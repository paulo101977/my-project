param (
    [string]$branchName = $( Read-Host "Favor informar uma branch válida." )
 )

# Baixando choco for windows
Set-ExecutionPolicy Bypass -Scope Process -Force; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))
# Instalando o git via choco
choco install git -params '"/GitAndUnixToolsOnPath"'
# Instalando docker via choco
choco install docker -y
# Instalando docker-machine via choco
choco install docker-machine -y
# Instalando docker-compose via choco
choco install docker-compose -y
# Iniciando Docker
restart-service *docker*
# Criando pasta projetos caso nao exista
New-Item -ItemType Directory -Force -Path C:\Projetos
# Entrando no novo diretório
cd C:\Projetos
# Baixando projeto
git clone --single-branch --branch $branchName https://totvshoteis.visualstudio.com/all/_git/thex 
# Entrando no projeto
cd thex\
# Mudando de branch caso já tenha clonado o diretório anteriormente
git checkout $branchName
# Mudando pra pasta docker
cd .docker\
# Executando docker-compose
docker-compose up -d
# Verificando se sql server subiu
docker logs -f sql-data
# Mudando de diretório para rodar importação
cd sh
# Rodando importação
import-db-windows.cmd
# cheers
