# Introduction 
Projeto de gestão hoteleira da Totvs.

# Getting Started
Abrir o arquivo install-thex.ps1

# Build and Test
O arquivo install-thex.ps1 irá baixar o Git, Docker, instalar, baixar o repositório do Thex, qual irá requisitar o usuário e senha
do Azure DevOps, e irá executar o docker-compose up.

Para rodar o Thex, basta acessar o endereço http://localhost:4200, após o termino da execução do script powershell.