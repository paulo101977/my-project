﻿using Shouldly;
using Tnf.AspNetCore.TestBase;
using Tnf.Notifications;
using Xunit;
using Microsoft.Extensions.DependencyInjection;
using Thex.SuperAdmin.Infra.Interfaces.ReadRepositories;
using Thex.SuperAdmin.Infra.Interfaces;
using Thex.SuperAdmin.Application.Adapters;
using NSubstitute;
using Thex.SuperAdmin.Infra.Entities;
using Thex.SuperAdmin.Web.Tests.Mocks;
using System;
using Thex.SuperAdmin.Application.Services;
using Thex.SuperAdmin.Dto;

namespace Thex.SuperAdmin.Web.Tests.Application
{
    public class TenantAppServiceTests : TnfAspNetCoreIntegratedTestBase<StartupUnitTest>
    {
        public INotificationHandler _notificationHandler;

        public TenantAppServiceTests()
        {
            _notificationHandler = new NotificationHandler(ServiceProvider);
        }

        [Fact]
        public void Should_Resolve_All()
        {
            TnfSession.ShouldNotBeNull();
            ServiceProvider.GetService<INotificationHandler>().ShouldNotBeNull();
            ServiceProvider.GetService<ITenantReadRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<ITenantRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<ITenantAdapter>().ShouldNotBeNull();
            ServiceProvider.GetService<ISimpleUnitOfWork>().ShouldNotBeNull();
        }

        [Fact]
        public void Should_Get_All()
        {
            var tenantReadRepository = Substitute.For<ITenantReadRepository>();
            var uow = Substitute.For<ISimpleUnitOfWork>();

            tenantReadRepository.GetAllDtoByFilter(new GetAllTenantDto()).ReturnsForAnyArgs(x =>
            {
                return TenantMock.GetDtoList();
            });

            new TenantAppService(tenantReadRepository, null, null, _notificationHandler, uow)
               .GetAllByFilter(new GetAllTenantDto());

            Assert.False(_notificationHandler.HasNotification());
        }

        [Fact]
        public void Should_Get_All_Without_Association()
        {
            var tenantReadRepository = Substitute.For<ITenantReadRepository>();
            var uow = Substitute.For<ISimpleUnitOfWork>();

            tenantReadRepository.GetAllDtoWithoutAssociation(Guid.NewGuid()).ReturnsForAnyArgs(x =>
            {
                return TenantMock.GetDtoList();
            });

            new TenantAppService(tenantReadRepository, null, null, _notificationHandler, uow)
               .GetAllWithoutAssociation(Guid.NewGuid());

            Assert.False(_notificationHandler.HasNotification());
        }

        [Fact]
        public void Should_Get()
        {
            var tenantReadRepository = Substitute.For<ITenantReadRepository>();
            var uow = Substitute.For<ISimpleUnitOfWork>();

            tenantReadRepository.GetDtoByid(Guid.NewGuid()).ReturnsForAnyArgs(x =>
            {
                return TenantMock.GetDtoWithoutAssociation();
            });

            new TenantAppService(tenantReadRepository, null, null, _notificationHandler, uow)
              .Get(Guid.NewGuid());
        }

        [Fact]
        public void Should_Create()
        {
            var tenantRepository = Substitute.For<ITenantRepository>();
            var uow = Substitute.For<ISimpleUnitOfWork>();
            var tenantAdapter = Substitute.For<ITenantAdapter>();

            tenantRepository.Insert(new Tenant()).ReturnsForAnyArgs(x =>
            {
                return new Tenant();
            });

            tenantAdapter.Map(TenantMock.GetDtoWithoutAssociation()).ReturnsForAnyArgs(x =>
            {
                return Tenant.Create(_notificationHandler)
                .WithId(Guid.NewGuid())
                .WithName("Tenant teste");               
            });

            new TenantAppService(null, tenantRepository, tenantAdapter, _notificationHandler, uow)
                .Create(TenantMock.GetDtoWithoutAssociation());

            Assert.False(_notificationHandler.HasNotification());

        }

        [Fact]
        public void Should_Update()
        {
            var tenantRepository = Substitute.For<ITenantRepository>();
            var tenantReadRepository = Substitute.For<ITenantReadRepository>();
            var uow = Substitute.For<ISimpleUnitOfWork>();
            var tenantAdapter = Substitute.For<ITenantAdapter>();

            tenantRepository.Update(new Tenant()).ReturnsForAnyArgs(x =>
            {
                return new Tenant();
            });

            tenantAdapter.MapToUpdate(new Tenant(), TenantMock.GetDtoWithoutAssociation()).ReturnsForAnyArgs(x =>
            {
                return Tenant.Create(_notificationHandler)
                .WithId(Guid.NewGuid())
                .WithName("Tenant teste");
            });

            tenantReadRepository.Exists(Guid.NewGuid()).ReturnsForAnyArgs(x =>
            {
                return true;
            });

            new TenantAppService(tenantReadRepository, tenantRepository, tenantAdapter, _notificationHandler, uow)
                .Update(TenantMock.GetDtoWithoutAssociation(), Guid.NewGuid());

            Assert.False(_notificationHandler.HasNotification());
        }
    }
}
