﻿using System;
using System.Collections.Generic;
using System.Text;
using Thex.SuperAdmin.Infra.Interfaces;
using Tnf.AspNetCore.TestBase;
using Tnf.Notifications;
using Microsoft.Extensions.DependencyInjection;
using Xunit;
using Shouldly;
using Thex.SuperAdmin.Infra.Interfaces.Services;
using Thex.SuperAdmin.Application.Adapters;
using Thex.SuperAdmin.Infra.Interfaces.ReadRepositories;
using Thex.Location.Application.Interfaces;
using Thex.Storage;
using System.Threading.Tasks;
using NSubstitute;
using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Web.Tests.Mocks;
using Thex.SuperAdmin.Application.Services;
using Microsoft.Extensions.Configuration;
using Thex.SuperAdmin.Infra.Context;
using System.IO;
using Thex.SuperAdmin.Infra.Entities;
using Thex.Common.Dto;
using Microsoft.AspNetCore.Http.Internal;
using Thex.Kernel;
using Thex.SuperAdmin.Dto.Dto;

namespace Thex.SuperAdmin.Web.Tests.Application
{
    public class PropertyAppServiceTests : TnfAspNetCoreIntegratedTestBase<StartupUnitTest>
    {
        public INotificationHandler _notificationHandler;
        public IApplicationUser _applicationUser;

        public PropertyAppServiceTests()
        {
            _notificationHandler = new NotificationHandler(ServiceProvider);
        }

        [Fact]
        public void Should_Resolve_All()
        {
            TnfSession.ShouldNotBeNull();
            ServiceProvider.GetService<INotificationHandler>().ShouldNotBeNull();
            ServiceProvider.GetService<IPropertyDomainService>().ShouldNotBeNull();
            ServiceProvider.GetService<IPropertyAdapter>().ShouldNotBeNull();
            ServiceProvider.GetService<ITenantReadRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<ITenantRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<IPropertyReadRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<ILocationAppService>().ShouldNotBeNull();
            ServiceProvider.GetService<IBrandReadRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<IAzureStorage>().ShouldNotBeNull();
            ServiceProvider.GetService<IConfiguration>().ShouldNotBeNull();
            ServiceProvider.GetService<ISimpleUnitOfWork>().ShouldNotBeNull();
        }

        [Fact]
        public async Task Should_Get_All()
        {
            var propertyReadRepository = Substitute.For<IPropertyReadRepository>();
            var uow = Substitute.For<ISimpleUnitOfWork>();

            propertyReadRepository.GetAllDtoAsync(new GetAllPropertyDto()).ReturnsForAnyArgs(x =>
            {
                return PropertyMock.GetListDtoAsync();
            });

            await new PropertyAppService(null, null, null, null, propertyReadRepository,
                null, null, null, null, _notificationHandler, uow, null, null, null, null,null, null, null, null, null ,null, null, null)
               .GetAll(new GetAllPropertyDto());

            Assert.False(_notificationHandler.HasNotification());
        }

        [Fact]
        public async Task Should_Get()
        {
            var propertyReadRepository = Substitute.For<IPropertyReadRepository>();
            var uow = Substitute.For<ISimpleUnitOfWork>();

            propertyReadRepository.GetDtoByIdAsync(new DefaultIntRequestDto(1)).ReturnsForAnyArgs(x =>
            {
                return PropertyMock.GetDto();
            });

            await new PropertyAppService(null, null, null, null, propertyReadRepository,
               null, null, null, null, _notificationHandler, uow, null, null, null, null, null, null, null, null, null, null, null, null)
              .GetAll(new GetAllPropertyDto());

            Assert.False(_notificationHandler.HasNotification());
        }

        [Fact]
        public async Task Should_Create()
        {
            var uow = Substitute.For<ISimpleUnitOfWork>();
            var tenantReadRepository = Substitute.For<ITenantReadRepository>();
            var propertyReadRepository = Substitute.For<IPropertyReadRepository>();
            var configuration = Substitute.For<IConfiguration>();
            var azureStorage = Substitute.For<IAzureStorage>();
            var propertyAdapter = Substitute.For<IPropertyAdapter>();
            var propertyDomainService = Substitute.For<IPropertyDomainService>();
            var brandReadRepository = Substitute.For<IBrandReadRepository>();
            var tenantRepository = Substitute.For<ITenantRepository>();
            var locationAppService = Substitute.For<ILocationAppService>();
            var propertyRepository = Substitute.For<IPropertyRepository>();
            var propertyContractAdapter = Substitute.For<IPropertyContractAdapter>();
            var integrationPartnerPropertyReadRepository = Substitute.For<IIntegrationPartnerPropertyReadRepository>();
            var integrationPartnerPropertyRepository = Substitute.For<IIntegrationPartnerPropertyRepository>();
            var integrationPartnerPropertyAdapter = Substitute.For<IIntegrationPartnerPropertyAdapter>();
            var integrationPartnerReadRepository = Substitute.For<IIntegrationPartnerReadRepository>();
            var integrationPartnerRepository = Substitute.For<IIntegrationPartnerRepository>();
            var propertyMealPlanTypeRepository = Substitute.For<IPropertyMealPlanTypeRepository>();
            var houseKeepingStatusPropertyRepository = Substitute.For<IHouseKeepingStatusPropertyRepository>();
            var propertyParameterRepository = Substitute.For<IPropertyParameterRepository>();
            var plasticBrandPropertyRepository = Substitute.For<IPlasticBrandPropertyRepository>();

            tenantRepository.GenerateTenantAutomaticallyAndSaveChanges("Tenant teste").ReturnsForAnyArgs(x =>
            {
                return Guid.NewGuid();
            });

            propertyReadRepository.Context.Returns(x =>
            {
                return ServiceProvider.GetService<ThexIdentityDbContext>();
            });

            configuration.GetValue<string>("").ReturnsForAnyArgs(x =>
            {
                return "teste";
            });

            var image = new FileStream("teste", FileMode.Create);
            azureStorage.SendAsync(Guid.NewGuid(), "", "", image).ReturnsForAnyArgs(x =>
            {
                return "teste";
            });
            image.Dispose();

            propertyAdapter.MapToCreate(PropertyMock.GetCreateDtoAsync().Result).ReturnsForAnyArgs(x =>
            {
                return Property.Create(_notificationHandler)
                .WithBrandId(1)
                .WithCompanyId(1)
                .WithId(1)
                .WithName("Hotel teste")
                .WithPropertyTypeId(Common.Enumerations.PropertyTypeEnum.Hotel)
                .WithPropertyUId(Guid.NewGuid());
            });

            propertyDomainService.InsertAndSaveChangesAsync(Property.Create(_notificationHandler)).ReturnsForAnyArgs(x =>
            {
                return new Property();
            });

            brandReadRepository.GetChainId(1).ReturnsForAnyArgs(x =>
            {
                return 1;
            });

            tenantRepository.SetProperty(Guid.NewGuid(), 1, 1, 1);

            locationAppService.CreateLocationList(new List<LocationDto>(), Guid.NewGuid());

            propertyContractAdapter.Map(new PropertyContractDto()).ReturnsForAnyArgs(x =>
            {
                return PropertyContract.Create(_notificationHandler)
               .WithId(Guid.NewGuid())
               .WithPropertyId(1)
               .WithMaxUh(11);
            });

            integrationPartnerPropertyReadRepository.IntegrationCodesAvailableAsync(new List<IntegrationPartnerPropertyDto>(), 1).ReturnsForAnyArgs(x =>
            {
                return true;
            });

            integrationPartnerReadRepository.GetPartnerNameByIdAsync(1).ReturnsForAnyArgs(x =>
            {
                return "teste";
            });

            integrationPartnerRepository.InsertAndSaveChangesAsync(new IntegrationPartner()).ReturnsForAnyArgs(x =>
            {
                return new IntegrationPartner();
            });

            integrationPartnerPropertyAdapter.Map(new IntegrationPartnerPropertyDto()).ReturnsForAnyArgs(x =>
            {
                return IntegrationPartnerProperty.Create(_notificationHandler)
                     .WithId(Guid.NewGuid())
                     .WithPropertyId(1)
                     .WithIntegrationCode("eeee")
                     .WithIntegrationPartnerId(1)
                     .WithIsActive(true)
                     .WithPartnerId(1);             
            });

            await integrationPartnerPropertyRepository.InsertRangeAndSaveChangesAsync(new List<IntegrationPartnerProperty>());

            await propertyMealPlanTypeRepository.CreatePropertyMealPlanTypesDefaultAsync(1, new Guid(), "br");

            await houseKeepingStatusPropertyRepository.CreateHouseKeepingStatusPropertyDefaultAsync(1, new Guid(), "br");

            await propertyParameterRepository.CreatePropertyParametersDefaultAsync(1, new Guid());

            await plasticBrandPropertyRepository.CreatePlasticBrandPropertiesDefault(1, new Guid());


            await new PropertyAppService(propertyDomainService, propertyAdapter, tenantReadRepository, tenantRepository, propertyReadRepository,
               locationAppService, brandReadRepository, azureStorage, configuration, _notificationHandler, uow, propertyRepository, _applicationUser, propertyContractAdapter, integrationPartnerPropertyReadRepository, integrationPartnerPropertyRepository, integrationPartnerPropertyAdapter, propertyMealPlanTypeRepository, houseKeepingStatusPropertyRepository, propertyParameterRepository, plasticBrandPropertyRepository, integrationPartnerReadRepository, integrationPartnerRepository)
              .CreateAsync(PropertyMock.GetCreateDtoAsync().Result, null);

            Assert.False(_notificationHandler.HasNotification());
        }

        [Fact]
        public async Task Should_Update()
        {
            var uow = Substitute.For<ISimpleUnitOfWork>();
            var tenantReadRepository = Substitute.For<ITenantReadRepository>();
            var propertyReadRepository = Substitute.For<IPropertyReadRepository>();
            var configuration = Substitute.For<IConfiguration>();
            var azureStorage = Substitute.For<IAzureStorage>();
            var propertyAdapter = Substitute.For<IPropertyAdapter>();
            var propertyDomainService = Substitute.For<IPropertyDomainService>();
            var brandReadRepository = Substitute.For<IBrandReadRepository>();
            var tenantRepository = Substitute.For<ITenantRepository>();
            var locationAppService = Substitute.For<ILocationAppService>();
            var propertyRepository = Substitute.For<IPropertyRepository>();
            var propertyContractAdapter = Substitute.For<IPropertyContractAdapter>();
            var integrationPartnerPropertyReadRepository = Substitute.For<IIntegrationPartnerPropertyReadRepository>();
            var integrationPartnerPropertyRepository = Substitute.For<IIntegrationPartnerPropertyRepository>();
            var integrationPartnerPropertyAdapter = Substitute.For<IIntegrationPartnerPropertyAdapter>();
            var integrationPartnerReadRepository = Substitute.For<IIntegrationPartnerReadRepository>();
            var integrationPartnerRepository = Substitute.For<IIntegrationPartnerRepository>();


            propertyReadRepository.GetByIdAsync(new DefaultIntRequestDto(1)).ReturnsForAnyArgs(x =>
            {
                return new Property();
            });

            brandReadRepository.GetChainId(1).ReturnsForAnyArgs(x =>
            {
                return 1;
            });

            propertyReadRepository.Context.Returns(x =>
            {
                return ServiceProvider.GetService<ThexIdentityDbContext>();
            });

            configuration.GetValue<string>("").ReturnsForAnyArgs(x =>
            {
                return "teste";
            });

            var image = new FileStream("teste", FileMode.Create);
            azureStorage.SendAsync(Guid.NewGuid(), "", "", image).ReturnsForAnyArgs(x =>
            {
                return "";
            });
            image.Dispose();

            propertyContractAdapter.MapToUpdate(new PropertyContract(), new PropertyContractDto()).ReturnsForAnyArgs(x =>
            {
                return PropertyContract.Create(_notificationHandler)
               .WithId(Guid.NewGuid())
               .WithPropertyId(1)
               .WithMaxUh(11);
            });

            propertyReadRepository.GetPropertyContractByPropertyIdAsync(1).ReturnsForAnyArgs(x =>
            {
                return new PropertyContract(); 
            });

            propertyAdapter.MapToUpdate(new Property(), PropertyMock.GetCreateDtoAsync().Result).ReturnsForAnyArgs(x =>
            {
                return Property.Create(_notificationHandler)
                .WithBrandId(1)
                .WithCompanyId(1)
                .WithId(1)
                .WithName("Hotel teste")
                .WithPropertyTypeId(Common.Enumerations.PropertyTypeEnum.Hotel)
                .WithPropertyUId(Guid.NewGuid());
            });

            propertyDomainService.UpdateAndSaveChangesAsync(Property.Create(_notificationHandler)).ReturnsForAnyArgs(x =>
            {
                return new Property();
            });

            tenantRepository.SetProperty(Guid.NewGuid(), 1, 1, 1);

            locationAppService.UpdateLocationList(new List<LocationDto>(), Guid.NewGuid().ToString());

            integrationPartnerPropertyReadRepository.IntegrationCodesAvailableAsync(new List<IntegrationPartnerPropertyDto>(), 1).ReturnsForAnyArgs(x =>
            {
                return true;
            });

            integrationPartnerReadRepository.GetPartnerNameByIdAsync(1).ReturnsForAnyArgs(x =>
            {
                return "teste";
            });

            integrationPartnerRepository.InsertAndSaveChangesAsync(new IntegrationPartner()).ReturnsForAnyArgs(x =>
            {
                return new IntegrationPartner();
            });

            integrationPartnerPropertyAdapter.Map(new IntegrationPartnerPropertyDto()).ReturnsForAnyArgs(x =>
            {
                return IntegrationPartnerProperty.Create(_notificationHandler)
                     .WithId(Guid.NewGuid())
                     .WithPropertyId(1)
                     .WithIntegrationCode("eeee")
                     .WithIntegrationPartnerId(1)
                     .WithIsActive(true)
                     .WithPartnerId(1);
            });

            await integrationPartnerPropertyRepository.InsertRangeAndSaveChangesAsync(new List<IntegrationPartnerProperty>());

            integrationPartnerPropertyReadRepository.GetAllByPropertyIdAsync(1).ReturnsForAnyArgs(x =>
            {
                return new List<IntegrationPartnerProperty>();
            });

            await integrationPartnerPropertyRepository.RemoveRangeAndSaveChangesAsync(new List<Guid>());

            await integrationPartnerPropertyRepository.UpdateRangeAndSaveChangesAsync(new List<IntegrationPartnerProperty>());

            await new PropertyAppService(propertyDomainService, propertyAdapter, tenantReadRepository, tenantRepository, propertyReadRepository,
                locationAppService, brandReadRepository, azureStorage, configuration, _notificationHandler, uow, propertyRepository, _applicationUser, propertyContractAdapter, integrationPartnerPropertyReadRepository, integrationPartnerPropertyRepository, integrationPartnerPropertyAdapter, null, null, null, null, integrationPartnerReadRepository, integrationPartnerRepository)
               .UpdateAsync(1, PropertyMock.GetCreateDtoAsync().Result, null);

            Assert.False(_notificationHandler.HasNotification());
        }
    }
}
