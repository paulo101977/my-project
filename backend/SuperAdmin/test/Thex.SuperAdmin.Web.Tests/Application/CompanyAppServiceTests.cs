﻿using System;
using System.Collections.Generic;
using System.Text;
using Tnf.AspNetCore.TestBase;
using Tnf.Notifications;
using Xunit;
using Microsoft.Extensions.DependencyInjection;
using Shouldly;
using Thex.SuperAdmin.Infra.Interfaces;
using Thex.SuperAdmin.Infra.Interfaces.ReadRepositories;
using Thex.SuperAdmin.Application.Adapters;
using Thex.Location.Application.Interfaces;
using Thex.AspNetCore.Security.Interfaces;
using System.Threading.Tasks;
using NSubstitute;
using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Web.Tests.Mocks;
using Thex.SuperAdmin.Application.Services;
using Thex.SuperAdmin.Dto.Dto;
using Thex.Common.Dto;
using Thex.SuperAdmin.Infra.Context;
using Thex.SuperAdmin.Infra.Entities;
using Thex.Kernel;

namespace Thex.SuperAdmin.Web.Tests.Application
{
    public class CompanyAppServiceTests : TnfAspNetCoreIntegratedTestBase<StartupUnitTest>
    {
        public INotificationHandler _notificationHandler;

        public CompanyAppServiceTests()
        {
            _notificationHandler = new NotificationHandler(ServiceProvider);
        }

        [Fact]
        public void Should_Resolve_All()
        {
            TnfSession.ShouldNotBeNull();
            ServiceProvider.GetService<ICompanyReadRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<ICompanyRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<IPersonRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<ICompanyAdapter>().ShouldNotBeNull();
            ServiceProvider.GetService<IPersonAdapter>().ShouldNotBeNull();
            ServiceProvider.GetService<IDocumentReadRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<IPropertyReadRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<IDocumentAdapter>().ShouldNotBeNull();
            ServiceProvider.GetService<IDocumentRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<ILocationAppService>().ShouldNotBeNull();
            ServiceProvider.GetService<IApplicationUser>().ShouldNotBeNull();
            ServiceProvider.GetService<IContactInformationRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<IContactInformationAdapter>().ShouldNotBeNull();
            ServiceProvider.GetService<INotificationHandler>().ShouldNotBeNull();
            ServiceProvider.GetService<ISimpleUnitOfWork>().ShouldNotBeNull();
        }


        [Fact]
        public async Task Should_Get_All()
        {
            var companyReadRepository = Substitute.For<ICompanyReadRepository>();
            var uow = Substitute.For<ISimpleUnitOfWork>();

            companyReadRepository.GetAllByFilterAsync(new GetAllCompanyDto()).ReturnsForAnyArgs(x =>
            {
                return CompanyMock.GetListDtoAsync();
            });

            await new CompanyAppService(companyReadRepository, null, null, null, null, null, null , null, null,
                null, null, null, null, _notificationHandler, uow)
               .GetAllByFilterAsync(new GetAllCompanyDto());

            Assert.False(_notificationHandler.HasNotification());
        }

        [Fact]
        public async Task Should_Get()
        {
            var companyReadRepository = Substitute.For<ICompanyReadRepository>();
            var documentReadRepository = Substitute.For<IDocumentReadRepository>();
            var locationAppService = Substitute.For<ILocationAppService>();
            var propertyReadRepository = Substitute.For<IPropertyReadRepository>();
            var applicationuser = Substitute.For<IApplicationUser>();
            var uow = Substitute.For<ISimpleUnitOfWork>();

            companyReadRepository.GetCompanyDtoByIdAsync(1).ReturnsForAnyArgs(x =>
            {
                return CompanyMock.GetDtoAsync();
            });

            locationAppService.GetAllByOwnerId(Guid.NewGuid(), "pt-BR").ReturnsForAnyArgs(x =>
            {
                return new List<LocationDto>();
            });

            propertyReadRepository.GetPropertiesAssociateByCompanyId(1).ReturnsForAnyArgs(x =>
            {
                return new List<PropertyDto>();
            });

            await new CompanyAppService(companyReadRepository, null, null, null, null, documentReadRepository, propertyReadRepository, null, null,
                 locationAppService, applicationuser, null, null, _notificationHandler, uow)
                .GetAsync(1);

            Assert.False(_notificationHandler.HasNotification());
        }

        [Fact]
        public async Task Should_Get_All_Location_From_Company()
        {
            var companyReadRepository = Substitute.For<ICompanyReadRepository>();
            var locationAppService = Substitute.For<ILocationAppService>();
            var applicationUser = Substitute.For<IApplicationUser>();
            var uow = Substitute.For<ISimpleUnitOfWork>();

            companyReadRepository.GetCompanyDtoByIdAsync(1).ReturnsForAnyArgs(x =>
            {
                return CompanyMock.GetDtoAsync();
            });

            locationAppService.GetAllByOwnerId(Guid.NewGuid(), "pt-BR").ReturnsForAnyArgs(x =>
            {
                return new List<LocationDto>();
            });

            await new CompanyAppService(companyReadRepository, null, null, null, null, null, null, null, null,
               locationAppService, applicationUser, null, null, _notificationHandler, uow)
              .GetLocationsFromCompanyAsync(1);
        }

        [Fact]
        public async Task Should_Create()
        {
            var companyReadRepository = Substitute.For<ICompanyReadRepository>();
            var documentReadRepository = Substitute.For<IDocumentReadRepository>();
            var locationAppService = Substitute.For<ILocationAppService>();
            var propertyReadRepository = Substitute.For<IPropertyReadRepository>();
            var applicationuser = Substitute.For<IApplicationUser>();
            var documentAdapter = Substitute.For<IDocumentAdapter>();
            var personAdapter = Substitute.For<IPersonAdapter>();
            var personRepository = Substitute.For<IPersonRepository>();
            var companyRepository = Substitute.For<ICompanyRepository>();
            var companyAdapter = Substitute.For<ICompanyAdapter>();
            var contactInformationRepository = Substitute.For<IContactInformationRepository>();
            var contactInformationAdater = Substitute.For<IContactInformationAdapter>();
            var documentRepository = Substitute.For<IDocumentRepository>();
            var uow = Substitute.For<ISimpleUnitOfWork>();

            documentAdapter.Map(new DocumentDto()).ReturnsForAnyArgs(x =>
            {
                return Document.Create(_notificationHandler)
                    .WithDocumentInformation("13.004.734/0001-90")
                    .WithDocumentTypeId(2)
                    .WithOwnerId(Guid.NewGuid());
            });

            companyReadRepository.Context.Returns(x =>
            {
                return ServiceProvider.GetService<ThexIdentityDbContext>();
            });

            personAdapter.Map(new PersonDto()).ReturnsForAnyArgs(x =>
            {
                return Person.Create(_notificationHandler)
                .WithFirstName("Company")
                .WithLastName("Teste")
                .WithId(Guid.NewGuid());
            });

            personRepository.Insert(new Person()).ReturnsForAnyArgs(x =>
            {
                return new Person();
            });

            companyAdapter.Map(CompanyMock.GetDto()).ReturnsForAnyArgs(x =>
            {
                return Company.Create(_notificationHandler)
                            .WithShortName("company teste")
                            .WithTradeName("company teste")
                            .WithCompanyUid(Guid.NewGuid())
                            .WithId(1);
            });

            contactInformationAdater.Map(new ContactInformationDto()).ReturnsForAnyArgs(x =>
            {
                return ContactInformation.Create(_notificationHandler)
                .WithContactInformationTypeId(1)
                .WithInformation("222222222222")
                .WithOwnerId(Guid.NewGuid());
            });

            companyRepository.InsertAndSaveChangesAsync(new Company()).ReturnsForAnyArgs(x =>
            {
                return new Company();
            });

            contactInformationRepository.AddRangeAndSaveChangesAsync(new List<ContactInformation>()).ReturnsForAnyArgs(x =>
            {
                return Task.FromResult("");
            });

            await new CompanyAppService(companyReadRepository, companyRepository, personRepository, companyAdapter, personAdapter, documentReadRepository, propertyReadRepository, documentAdapter, documentRepository,
                locationAppService, applicationuser, contactInformationRepository, contactInformationAdater, _notificationHandler, uow)
               .CreateAsync(CompanyMock.GetDto());

            Assert.False(_notificationHandler.HasNotification());
        }

        [Fact]
        public async void Should_Update()
        {
            var companyReadRepository = Substitute.For<ICompanyReadRepository>();
            var documentReadRepository = Substitute.For<IDocumentReadRepository>();
            var locationAppService = Substitute.For<ILocationAppService>();
            var propertyReadRepository = Substitute.For<IPropertyReadRepository>();
            var applicationuser = Substitute.For<IApplicationUser>();
            var documentAdapter = Substitute.For<IDocumentAdapter>();
            var personAdapter = Substitute.For<IPersonAdapter>();
            var personRepository = Substitute.For<IPersonRepository>();
            var companyRepository = Substitute.For<ICompanyRepository>();
            var companyAdapter = Substitute.For<ICompanyAdapter>();
            var contactInformationRepository = Substitute.For<IContactInformationRepository>();
            var contactInformationAdater = Substitute.For<IContactInformationAdapter>();
            var documentRepository = Substitute.For<IDocumentRepository>();
            var uow = Substitute.For<ISimpleUnitOfWork>();

            documentAdapter.Map(new DocumentDto()).ReturnsForAnyArgs(x =>
            {
                return Document.Create(_notificationHandler)
                    .WithDocumentInformation("13.004.734/0001-90")
                    .WithDocumentTypeId(2)
                    .WithOwnerId(Guid.NewGuid());
            });

            companyReadRepository.GetCompanyByIdAsync(default(int)).ReturnsForAnyArgs(x =>
            {
                return new Company();
            });

            companyReadRepository.Context.Returns(x =>
            {
                return ServiceProvider.GetService<ThexIdentityDbContext>();
            });

            companyReadRepository.Exists(1).ReturnsForAnyArgs(x =>
            {
                return true;
            });

            personAdapter.Map(new PersonDto()).ReturnsForAnyArgs(x =>
            {
                return Person.Create(_notificationHandler)
                .WithFirstName("Company")
                .WithLastName("Teste")
                .WithId(Guid.NewGuid());
            });

            companyAdapter.Map(CompanyMock.GetDto()).ReturnsForAnyArgs(x =>
            {
                return Company.Create(_notificationHandler)
                            .WithShortName("company teste")
                            .WithTradeName("company teste")
                            .WithCompanyUid(Guid.NewGuid())
                            .WithId(1);
            });

            companyAdapter.Map(new Company(), CompanyMock.GetDto()).ReturnsForAnyArgs(x =>
            {
                return Company.Create(_notificationHandler)
                            .WithShortName("company teste")
                            .WithTradeName("company teste")
                            .WithCompanyUid(Guid.NewGuid())
                            .WithId(1);
            });

            companyRepository.UpdateAndSaveChangesAsync(new Company()).ReturnsForAnyArgs(x =>
            {
                return new Company();
            });

            locationAppService.UpdateLocationList(new List<LocationDto>(), Guid.NewGuid().ToString());

            contactInformationAdater.Map(new ContactInformationDto()).ReturnsForAnyArgs(x =>
            {
                return ContactInformation.Create(_notificationHandler)
                .WithContactInformationTypeId(1)
                .WithInformation("222222222222")
                .WithOwnerId(Guid.NewGuid());
            });

            await new CompanyAppService(companyReadRepository, companyRepository, personRepository, companyAdapter, personAdapter, documentReadRepository, propertyReadRepository, documentAdapter, documentRepository,
               locationAppService, applicationuser, contactInformationRepository, contactInformationAdater, _notificationHandler, uow)
              .UpdateAsync(1, CompanyMock.GetDto());

            Assert.False(_notificationHandler.HasNotification());
        }
    }
}
