﻿using Shouldly;
using Tnf.AspNetCore.TestBase;
using Tnf.Notifications;
using Xunit;
using Microsoft.Extensions.DependencyInjection;
using Thex.SuperAdmin.Infra.Interfaces;
using Thex.SuperAdmin.Infra.Interfaces.ReadRepositories;
using System.Threading.Tasks;
using Thex.SuperAdmin.Application.Services;
using NSubstitute;
using Thex.SuperAdmin.Web.Tests.Mocks;

namespace Thex.SuperAdmin.Web.Tests.Application
{
    public class DashboardAppServiceTests : TnfAspNetCoreIntegratedTestBase<StartupUnitTest>
    {
        public INotificationHandler _notificationHandler;

        public DashboardAppServiceTests()
        {
            _notificationHandler = new NotificationHandler(ServiceProvider);
        }

        [Fact]
        public void Should_Resolve_All()
        {
            TnfSession.ShouldNotBeNull();
            ServiceProvider.GetService<INotificationHandler>().ShouldNotBeNull();
            ServiceProvider.GetService<ISimpleUnitOfWork>().ShouldNotBeNull();
            ServiceProvider.GetService<IPropertyReadRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<IPropertyRepository>().ShouldNotBeNull();
        }

        [Fact]
        public async Task Should_Get_All_Properties()
        {
            var propertyReadRepository = Substitute.For<IPropertyReadRepository>();
            var propertyRepository = Substitute.For<IPropertyRepository>();
            var uow = Substitute.For<ISimpleUnitOfWork>();

            propertyReadRepository.GetAllDtoForDashboardAsync().ReturnsForAnyArgs(x =>
            {
                return DashboardMock.GetAllPropertiesAsync();
            });

            await new DashboardAppService(uow, _notificationHandler, propertyReadRepository, propertyRepository)
               .GetAllPropertiesAsync();

            Assert.False(_notificationHandler.HasNotification());
        }

        [Theory]
        [InlineData(13)]
        [InlineData(14)]
        [InlineData(15)]
        public async Task Should_Next_PropertyStatus(int propertyStatus)
        {
            var propertyReadRepository = Substitute.For<IPropertyReadRepository>();
            var propertyRepository = Substitute.For<IPropertyRepository>();
            var uow = Substitute.For<ISimpleUnitOfWork>();

            propertyReadRepository.GetPropertyStatusByIdAsync(1).ReturnsForAnyArgs(x =>
            {
                return propertyStatus;
            });

            await propertyRepository.ChangeStatusAndSaveHistoryAsync(1, 1);

            await new DashboardAppService(uow, _notificationHandler, propertyReadRepository, propertyRepository)
               .NextPropertyStatusAsync(1);

            Assert.False(_notificationHandler.HasNotification());
        }

        [Theory]
        [InlineData(19)]
        [InlineData(20)]
        [InlineData(21)]
        public async Task Should_Not_Next_PropertyStatus(int propertyId)
        {
            var propertyReadRepository = Substitute.For<IPropertyReadRepository>();
            var propertyRepository = Substitute.For<IPropertyRepository>();
            var uow = Substitute.For<ISimpleUnitOfWork>();

            propertyReadRepository.GetPropertyStatusByIdAsync(1).ReturnsForAnyArgs(x =>
            {
                return propertyId;
            });

            await propertyRepository.ChangeStatusAndSaveHistoryAsync(1, 1);

            await new DashboardAppService(uow, _notificationHandler, propertyReadRepository, propertyRepository)
               .NextPropertyStatusAsync(1);

            Assert.True(_notificationHandler.HasNotification());
        }

        [Fact]
        public async Task Should_Block_Property()
        {
            var propertyReadRepository = Substitute.For<IPropertyReadRepository>();
            var propertyRepository = Substitute.For<IPropertyRepository>();
            var uow = Substitute.For<ISimpleUnitOfWork>();

            propertyReadRepository.PropertyIsBlocked(1).ReturnsForAnyArgs(x =>
            {
                return false;
            });

            propertyReadRepository.CanBlockPropertyAsync(1, string.Empty).ReturnsForAnyArgs(x =>
            {
                return true;
            });

            await new DashboardAppService(uow, _notificationHandler, propertyReadRepository, propertyRepository)
               .ToggleBlockPropertyAsync(1);

            Assert.False(_notificationHandler.HasNotification());
        }

        [Fact]
        public async Task Should_UnBlock_WithoutValidate_Property()
        {
            var propertyReadRepository = Substitute.For<IPropertyReadRepository>();
            var propertyRepository = Substitute.For<IPropertyRepository>();
            var uow = Substitute.For<ISimpleUnitOfWork>();

            propertyReadRepository.PropertyIsBlocked(1).ReturnsForAnyArgs(x =>
            {
                return true;
            });

            await new DashboardAppService(uow, _notificationHandler, propertyReadRepository, propertyRepository)
               .ToggleBlockPropertyAsync(1);

            Assert.False(_notificationHandler.HasNotification());
        }
    }
}
