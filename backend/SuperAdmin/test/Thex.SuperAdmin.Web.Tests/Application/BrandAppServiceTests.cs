﻿using Shouldly;
using System;
using System.Collections.Generic;
using System.Text;
using Tnf.AspNetCore.TestBase;
using Tnf.Notifications;
using Xunit;
using Microsoft.Extensions.DependencyInjection;
using Thex.SuperAdmin.Infra.Interfaces.ReadRepositories;
using Thex.SuperAdmin.Infra.Interfaces;
using Thex.SuperAdmin.Application.Adapters;
using NSubstitute;
using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Web.Tests.Mocks;
using System.Threading.Tasks;
using Thex.SuperAdmin.Application.Services;
using Thex.SuperAdmin.Infra.Entities;
using Thex.SuperAdmin.Infra.Context;

namespace Thex.SuperAdmin.Web.Tests.Application
{
    public class BrandAppServiceTests : TnfAspNetCoreIntegratedTestBase<StartupUnitTest>
    {
        public INotificationHandler _notificationHandler;

        public BrandAppServiceTests()
        {
            _notificationHandler = new NotificationHandler(ServiceProvider);
        }

        [Fact]
        public void Should_Resolve_All()
        {
            TnfSession.ShouldNotBeNull();
            ServiceProvider.GetService<INotificationHandler>().ShouldNotBeNull();
            ServiceProvider.GetService<IBrandReadRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<IBrandRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<IBrandAdapter>().ShouldNotBeNull();
            ServiceProvider.GetService<ITenantReadRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<ITenantRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<ISimpleUnitOfWork>().ShouldNotBeNull();
        }

        [Fact]
        public async Task Should_Get_All()
        {
            var brandReadRepository = Substitute.For<IBrandReadRepository>();
            var uow = Substitute.For<ISimpleUnitOfWork>();

            brandReadRepository.GetAllDtoByFilterAsync(new GetAllBrandDto()).ReturnsForAnyArgs(x =>
            {
                return BrandMock.GetDtoListAsync();
            });

            await new BrandAppService(brandReadRepository, null, null, null, null, _notificationHandler, uow)
               .GetAllByFilterAsync(new GetAllBrandDto());

            Assert.False(_notificationHandler.HasNotification());
        }

        [Fact]
        public async Task Should_Get()
        {
            var brandReadRepository = Substitute.For<IBrandReadRepository>();
            var uow = Substitute.For<ISimpleUnitOfWork>();

            brandReadRepository.GetDtoByIdAsync(1).ReturnsForAnyArgs(x =>
            {
                return BrandMock.GetDto();
            });

            await new BrandAppService(brandReadRepository, null, null, null, null, _notificationHandler, uow)
               .GetAsync(1);

            Assert.False(_notificationHandler.HasNotification());
        }

        [Fact]
        public async Task Should_Create()
        {
            var brandReadRepository = Substitute.For<IBrandReadRepository>();
            var uow = Substitute.For<ISimpleUnitOfWork>();
            var tenantAdapter = Substitute.For<ITenantAdapter>();
            var brandRepository = Substitute.For<IBrandRepository>();
            var brandAdapter = Substitute.For<IBrandAdapter>();
            var tenantReadRepository = Substitute.For<ITenantReadRepository>();
            var tenantRepository = Substitute.For<ITenantRepository>();

            brandReadRepository.Context.Returns(x =>
            {
                var context = ServiceProvider.GetService<ThexIdentityDbContext>();
                return context;
            });

            brandReadRepository.NameIsAvailable("Brand").ReturnsForAnyArgs(x =>
            {
                return Task.FromResult(true);
            });

            tenantRepository.GenerateTenantAutomaticallyAndSaveChanges("Tenant teste").ReturnsForAnyArgs(x =>
            {
                return Guid.NewGuid();
            });
           
            brandRepository.InsertAndSaveChangesAsync(new Brand()).ReturnsForAnyArgs(x =>
            {
                return new Brand();
            });

            brandAdapter.Map(BrandMock.GetDto()).ReturnsForAnyArgs(x =>
            {
                return Brand.Create(_notificationHandler)
                .WithId(1)
                .WithName("Brand teste")
                .WithChainId(1)
                .WithTenant(Guid.NewGuid());
            });

            await new BrandAppService(brandReadRepository, brandRepository, brandAdapter, tenantReadRepository, tenantRepository, _notificationHandler, uow)
              .CreateAsync(BrandMock.GetDto());

            Assert.False(_notificationHandler.HasNotification());
        }

        [Fact]
        public async void Should_Update()
        {
            var brandReadRepository = Substitute.For<IBrandReadRepository>();
            var uow = Substitute.For<ISimpleUnitOfWork>();
            var tenantAdapter = Substitute.For<ITenantAdapter>();
            var brandRepository = Substitute.For<IBrandRepository>();
            var brandAdapter = Substitute.For<IBrandAdapter>();

            brandReadRepository.NameIsAvailable("Brand").ReturnsForAnyArgs(x =>
            {
                return Task.FromResult(true);
            });

            brandRepository.UpdateAndSaveChangesAsync(new Brand()).ReturnsForAnyArgs(x =>
            {
                return new Brand();
            });

            brandAdapter.Map(new Brand(), BrandMock.GetDto()).ReturnsForAnyArgs(x =>
            {
                return Brand.Create(_notificationHandler)
                .WithId(1)
                .WithName("Brand teste")
                .WithChainId(1)
                .WithTenant(Guid.NewGuid());
            });

            brandReadRepository.GetById(1).ReturnsForAnyArgs(x =>
            {
                return Task.FromResult(new Brand());
            });

            await new BrandAppService(brandReadRepository, brandRepository, brandAdapter, null, null, _notificationHandler, uow)
             .UpdateAsync(1, BrandMock.GetDto());

            Assert.False(_notificationHandler.HasNotification());
        }
    }
}
