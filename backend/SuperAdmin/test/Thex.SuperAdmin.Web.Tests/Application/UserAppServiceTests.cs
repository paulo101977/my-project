﻿using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using NSubstitute;
using Shouldly;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.Kernel;
using Thex.Storage;
using Thex.SuperAdmin.Application.Adapters;
using Thex.SuperAdmin.Application.Interfaces;
using Thex.SuperAdmin.Application.Services;
using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Infra.Entities;
using Thex.SuperAdmin.Infra.Interfaces;
using Thex.SuperAdmin.Infra.Interfaces.ReadRepositories;
using Thex.SuperAdmin.Web.Tests.Mocks;
using Tnf.AspNetCore.TestBase;
using Tnf.Notifications;
using Tnf.Repositories.Uow;
using Xunit;

namespace Thex.SuperAdmin.Web.Tests.Application
{
    public class UserAppServiceTests : TnfAspNetCoreIntegratedTestBase<StartupUnitTest>
    {
        public INotificationHandler _notificationHandler;


        public UserAppServiceTests()
        {
            _notificationHandler = new NotificationHandler(ServiceProvider);

        }

        [Fact]
        public void Should_Resolve_All()
        {
            TnfSession.ShouldNotBeNull();
            ServiceProvider.GetService<INotificationHandler>().ShouldNotBeNull();
            ServiceProvider.GetService<IUserRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<IUserReadRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<IApplicationUser>().ShouldNotBeNull();
            ServiceProvider.GetService<UserManager<User>>().ShouldNotBeNull();
            ServiceProvider.GetService<IUnitOfWorkManager>().ShouldNotBeNull();

        }


        [Fact]
        public async Task Should_ChangePwd_Person()
        {
            var userStoreMock = Substitute.For<IUserStore<User>>();
            var passwordHasher = Substitute.For<IPasswordHasher<User>>();
            var userReadRepository = Substitute.For<IUserReadRepository>();
            var IApplicationUser = Substitute.For<IApplicationUser>();
            var userRepository = Substitute.For<IUserRepository>();
            var userAppService = Substitute.For<IUserAppService>();
            var userManager = Substitute.For<UserManager<User>>(userStoreMock, null, passwordHasher, null, null, null, null, null, null);
            var simpleUnitOfWork = Substitute.For<ISimpleUnitOfWork>();


            var user = UserMock.GetUser().Result;

            var dto = userReadRepository.GetAndValidateUser(default(Guid), default(string)).ReturnsForAnyArgs(x =>
            {
                return UserMock.GetUserDto();
            });

            userManager.FindByEmailAsync(default(string)).ReturnsForAnyArgs(x =>
            {
                return UserMock.GetUser().Result;
            });

            userManager.PasswordHasher.HashPassword(user, default(string)).ReturnsForAnyArgs(x =>
            {
                return "teste";

            });

            userManager.UpdateAsync(user).ReturnsForAnyArgs(x =>
            {
                return new IdentityResult();
            });

            simpleUnitOfWork.Commit().ReturnsForAnyArgs(x => { return true; });


            await new UserAppService(null, IApplicationUser, null, userReadRepository, null, _notificationHandler, null, null, null, null, null,
                null, null, userRepository, null, userManager, null, null, null, simpleUnitOfWork, null, null, null, null, null, null, null)
               .ChangePasswordPersonAsync(UserMock.GetChangePwtDto());

            Assert.False(_notificationHandler.HasNotification());
        }

        [Fact]
        public async Task Should_Update_User()
        {

            var unitOfWorkManager = ServiceProvider.GetService<IUnitOfWorkManager>();
            var applicationUser = ServiceProvider.GetService<IApplicationUser>();

            var userStoreMock = Substitute.For<IUserStore<User>>();
            var passwordHasher = Substitute.For<IPasswordHasher<User>>();
            var userReadRepository = Substitute.For<IUserReadRepository>();
            var userRepository = Substitute.For<IUserRepository>();
            var userAppService = Substitute.For<IUserAppService>();
            var userManager = Substitute.For<UserManager<User>>(userStoreMock, null, passwordHasher, null, null, null, null, null, null);
            var personReadRepository = Substitute.For<IPersonReadRepository>();
            var personAppService = Substitute.For<IPersonAppService>();
            var userRoleAppService = Substitute.For<IUserRoleAppService>();

            userReadRepository.GetByIdAsync(default(Guid)).ReturnsForAnyArgs(x =>
            {
                return UserMock.GetUser();

            });

            personReadRepository.Get(default(Guid)).ReturnsForAnyArgs(x =>
            {
                return PersonMock.GetPerson();
            });


            personAppService.UpdateAsync(default(Person), default(PersonDto));

            userManager.UpdateAsync(default(User)).ReturnsForAnyArgs(x =>
            {
                return new IdentityResult();
            });

            await userRoleAppService.CreateorUpdateAsync(default(Guid), default(List<Guid>), default(int));




            await new UserAppService(
                null, applicationUser, null, userReadRepository, null, _notificationHandler, null, null, null, null, null,
                null, null, userRepository, null, userManager, personAppService, null, null, null, null, null, null, userRoleAppService, null, personReadRepository,
                unitOfWorkManager: ServiceProvider.GetService<IUnitOfWorkManager>())
               .UpdateUserAsync(UserMock.GetUpdatedUserDto());

            Assert.False(_notificationHandler.HasNotification());
        }

        [Fact]
        public async Task Should_Change_Photo()
        {
            var unitOfWorkManager = ServiceProvider.GetService<IUnitOfWorkManager>();
            var applicationUser = ServiceProvider.GetService<IApplicationUser>();

            var userReadRepository = Substitute.For<IUserReadRepository>();
            var personRepository = Substitute.For<IPersonRepository>();
            var personReadRepository = Substitute.For<IPersonReadRepository>();
            var personAdapter = Substitute.For<IPersonAdapter>();
            var azureStorage = Substitute.For<IAzureStorage>();
            var unitOfWork = Substitute.For<IUnitOfWorkManager>();

            azureStorage.SendAsync(Guid.NewGuid(), "test", "test", new Byte[2]).ReturnsForAnyArgs(x =>
            {
                return ("UrlTest").AsTask();
            });

            userReadRepository.GetUserById(Guid.NewGuid()).ReturnsForAnyArgs(x =>
            {
                return new UserDto();
            });

            personReadRepository.Get(Guid.NewGuid()).ReturnsForAnyArgs(x =>
            {
                return new Person();
            });

            personAdapter.MapUpdatePhotoUrlPerson(new Person(), "urlTeste").ReturnsForAnyArgs(x =>
            {
                return Person.Create(_notificationHandler)
                  .WithId(Guid.NewGuid())
                  .WithFirstName("Test")
                  .WithPersonType("N");
            });

            personRepository.UpdateAsync(new Person()).ReturnsForAnyArgs(x =>
            {
                return new Person();
            });

            await new UserAppService(
              azureStorage, applicationUser, null, userReadRepository, null, _notificationHandler, null, personRepository, personAdapter, null, null,
              null, null, null, null, null, null, null, null, null, null, null, null, null, null, personReadRepository,
              unitOfWork)
             .ChangePhoto(Guid.NewGuid(), new UserDto { PhotoBase64 = "dGVzdA==" });

            Assert.False(_notificationHandler.HasNotification());
        }
    }
}
