﻿using System;
using System.Collections.Generic;
using System.Text;
using Thex.SuperAdmin.Infra.Interfaces.ReadRepositories;
using Microsoft.Extensions.DependencyInjection;
using Tnf.AspNetCore.TestBase;
using Tnf.Notifications;
using Xunit;
using Shouldly;
using Thex.SuperAdmin.Application.Adapters;
using Thex.SuperAdmin.Infra.Interfaces.Services;
using Thex.SuperAdmin.Infra.Interfaces;
using System.Threading.Tasks;
using NSubstitute;
using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Web.Tests.Mocks;
using Thex.SuperAdmin.Application.Services;
using Thex.SuperAdmin.Infra.Context;
using Thex.SuperAdmin.Infra.Entities;

namespace Thex.SuperAdmin.Web.Tests.Application
{
    public class ChainAppServiceTests : TnfAspNetCoreIntegratedTestBase<StartupUnitTest>
    {
        public INotificationHandler _notificationHandler;

        public ChainAppServiceTests()
        {
            _notificationHandler = new NotificationHandler(ServiceProvider);
        }

        [Fact]
        public void Should_Resolve_All()
        {
            TnfSession.ShouldNotBeNull();
            ServiceProvider.GetService<INotificationHandler>().ShouldNotBeNull();
            ServiceProvider.GetService<IChainAdapter>().ShouldNotBeNull();
            ServiceProvider.GetService<IChainReadRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<IChainDomainService>().ShouldNotBeNull();
            ServiceProvider.GetService<ITenantReadRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<ITenantRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<ISimpleUnitOfWork>().ShouldNotBeNull();
        }

        [Fact]
        public async Task Should_Get_All()
        {
            var chainReadRepository = Substitute.For<IChainReadRepository>();
            var uow = Substitute.For<ISimpleUnitOfWork>();

            chainReadRepository.GetAllDtoAsync(new GetAllChainDto()).ReturnsForAnyArgs(x =>
            {
                return ChainMock.GetListDtoAsync();
            });

            await new ChainAppService(null, chainReadRepository, null, null, null, _notificationHandler, uow, null, null)
               .GetAll(new GetAllChainDto());

            Assert.False(_notificationHandler.HasNotification());
        }

        [Fact]
        public async Task Should_Get()
        {
            var chainReadRepository = Substitute.For<IChainReadRepository>();
            var uow = Substitute.For<ISimpleUnitOfWork>();
            var brandReadRepository = Substitute.For<IBrandReadRepository>();
            var propertyReadRepository = Substitute.For<IPropertyReadRepository>();


            chainReadRepository.GetDtoByIdAsync(new DefaultIntRequestDto(1)).ReturnsForAnyArgs(x =>
            {
                return ChainMock.GetDtoAsync();
            });

            brandReadRepository.GetAllDtoByChainIdAsync(1).ReturnsForAnyArgs(x =>
            {
                return new List<BrandDto>();
            });

            propertyReadRepository.GetAllDtoByChainIdAsync(1).ReturnsForAnyArgs(x =>
            {
                return new List<PropertyDto>();
            });

            await new ChainAppService(null, chainReadRepository, null, null, null, _notificationHandler, uow, brandReadRepository, propertyReadRepository)
                .Get(new DefaultIntRequestDto(1));

            Assert.False(_notificationHandler.HasNotification());
        }

        [Fact]
        public async Task Should_Create()
        {
            var brandReadRepository = Substitute.For<IBrandReadRepository>();
            var uow = Substitute.For<ISimpleUnitOfWork>();
            var chainRepository = Substitute.For<IChainRepository>();
            var chainAdapter = Substitute.For<IChainAdapter>();
            var chainReadRepository = Substitute.For<IChainReadRepository>();
            var tenantReadRepository = Substitute.For<ITenantReadRepository>();
            var tenantRepository = Substitute.For<ITenantRepository>();
            var chainDomainService = Substitute.For<IChainDomainService>();

            chainReadRepository.Context.Returns(x =>
            {
                var context = ServiceProvider.GetService<ThexIdentityDbContext>();
                return context;
            });

            tenantRepository.GenerateTenantAutomaticallyAndSaveChanges("Tenant teste").ReturnsForAnyArgs(x =>
            {
                return Guid.NewGuid();
            });

            chainRepository.InsertAndSaveChangesAsync(new Chain()).ReturnsForAnyArgs(x =>
            {
                return new Chain();
            });

            chainAdapter.MapToCreate(ChainMock.GetDto()).ReturnsForAnyArgs(x =>
            {
                return Chain.Create(_notificationHandler)
                .WithId(1)
                .WithName("Chain teste")              
                .WithTenantId(Guid.NewGuid());
            });

            chainDomainService.InsertAndSaveChangesAsync(new Chain.Builder(_notificationHandler)).ReturnsForAnyArgs(x =>
            {
                return new Chain();
            });

            await new ChainAppService(chainAdapter, chainReadRepository, chainDomainService, tenantReadRepository, tenantRepository, _notificationHandler, uow, null, null)
                .Create(ChainMock.GetDto());

            Assert.False(_notificationHandler.HasNotification());
        }

        [Fact]
        public async void Should_Update()
        {
            var brandReadRepository = Substitute.For<IBrandReadRepository>();
            var uow = Substitute.For<ISimpleUnitOfWork>();
            var chainRepository = Substitute.For<IChainRepository>();
            var chainAdapter = Substitute.For<IChainAdapter>();
            var chainReadRepository = Substitute.For<IChainReadRepository>();
            var chainDomainService = Substitute.For<IChainDomainService>();

            chainReadRepository.GetByIdAsync(new DefaultIntRequestDto(1)).ReturnsForAnyArgs(x =>
            {
                return new Chain();
            });

            chainRepository.UpdateAndSaveChangesAsync(new Chain()).ReturnsForAnyArgs(x =>
            {
                return new Chain();
            });

            chainAdapter.MapToUpdate(new Chain(), ChainMock.GetDto()).ReturnsForAnyArgs(x =>
            {
                return Chain.Create(_notificationHandler)
                .WithId(1)
                .WithName("Chain teste")
                .WithTenantId(Guid.NewGuid());
            });

            chainDomainService.UpdateAndSaveChangesAsync(new Chain.Builder(_notificationHandler)).ReturnsForAnyArgs(x =>
            {
                return new Chain();
            });

            await new ChainAppService(chainAdapter, chainReadRepository, chainDomainService, null, null, _notificationHandler, uow, null, null)
                .Update(1, new ChainDto());

            Assert.False(_notificationHandler.HasNotification());
        }
    }
}
