﻿using Tnf.AspNetCore.TestBase;
using Tnf.Notifications;
using Microsoft.Extensions.DependencyInjection;
using Xunit;
using Shouldly;
using Thex.SuperAdmin.Infra.Interfaces;
using Thex.SuperAdmin.Infra.Interfaces.ReadRepositories;
using Microsoft.AspNetCore.Identity;
using Thex.SuperAdmin.Application.Adapters;
using Thex.Kernel;
using Thex.SuperAdmin.Application.Interfaces;
using Thex.SuperAdmin.Infra.Entities;
using NSubstitute;
using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Application.Services;
using System.Threading.Tasks;
using System;
using Thex.SuperAdmin.Web.Tests.Mocks;

namespace Thex.SuperAdmin.Web.Tests.Application
{
    public class UserTotvsAppServiceTests : TnfAspNetCoreIntegratedTestBase<StartupUnitTest>
    {
        public INotificationHandler _notificationHandler;

        public UserTotvsAppServiceTests()
        {
            _notificationHandler = new NotificationHandler(ServiceProvider);
        }

        [Fact]
        public void Should_Resolve_All()
        {
            TnfSession.ShouldNotBeNull();
            ServiceProvider.GetService<INotificationHandler>().ShouldNotBeNull();
            ServiceProvider.GetService<ISimpleUnitOfWork>().ShouldNotBeNull();
            ServiceProvider.GetService<IUserRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<IUserReadRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<IUserTenantRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<IBrandReadRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<IChainReadRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<IUserAdapter>().ShouldNotBeNull();
            ServiceProvider.GetService<IUserTenantAdapter>().ShouldNotBeNull();
            ServiceProvider.GetService<IPropertyReadRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<IApplicationUser>().ShouldNotBeNull();
            ServiceProvider.GetService<IPersonAppService>().ShouldNotBeNull();
        }

        [Fact]
        public async Task Should_Get_All()
        {
            var userReadRepository = Substitute.For<IUserReadRepository>();
            var uow = Substitute.For<ISimpleUnitOfWork>();

            userReadRepository.GetAllAsync(new GetAllUserDto()).ReturnsForAnyArgs(x =>
            {
                return UserMock.GetDtoListAsync();
            });

            await new UserTotvsAppService(uow, null, userReadRepository, null, null, null, null, null, null, _notificationHandler)
               .GetAllAsync(new GetAllUserDto());

            Assert.False(_notificationHandler.HasNotification());
        }

        [Fact]
        public async Task Should_Get()
        {
            var userReadRepository = Substitute.For<IUserReadRepository>();
            var uow = Substitute.For<ISimpleUnitOfWork>();

            userReadRepository.GetDtoByIdAsync(Guid.NewGuid()).ReturnsForAnyArgs(x=> 
            {
                return UserMock.GetDto();
            });

            await new UserTotvsAppService(uow, null, userReadRepository, null, null, null, null, null, null, _notificationHandler)
               .GetAsync(Guid.NewGuid());

            Assert.False(_notificationHandler.HasNotification());
        }

        //[Fact]
        //public async Task Should_Create()
        //{
        //    var userStoreMock = Substitute.For<IUserRoleStore<User>>();
        //    var userRepository = Substitute.For<IUserRepository>();
        //    var userReadRepository = Substitute.For<IUserReadRepository>();
        //    var userTenantRepository = Substitute.For<IUserTenantRepository>();
        //    var brandReadRepository = Substitute.For<IBrandReadRepository>();
        //    var chainReadRepository = Substitute.For<IChainReadRepository>();
        //    var userManager = new UserManager<User>(userStoreMock, null, null, null, null, null, null, null, null);
        //    var userAdapter = Substitute.For<IUserAdapter>();
        //    var userTenantAdapter = Substitute.For<IUserTenantAdapter>();
        //    var propertyReadRepository = Substitute.For<IPropertyReadRepository>();
        //    var applicationUser = Substitute.For<IApplicationUser>();
        //    var personAppService = Substitute.For<IPersonAppService>();
        //    var uow = Substitute.For<ISimpleUnitOfWork>();

        //    userReadRepository.Context.Returns(x =>
        //    {
        //        return ServiceProvider.GetService<ThexIdentityDbContext>();
        //    });

        //    personAppService.Create(new PersonDto()).ReturnsForAnyArgs(x =>
        //    {
        //        return new PersonDto();
        //    });

        //    userManager.CreateAsync(new User(), "teste").ReturnsForAnyArgs( x => 
        //    {
        //        return IdentityResult.Success;
        //    });

        //    chainReadRepository.GetByIdAsync(new DefaultIntRequestDto(1)).ReturnsForAnyArgs(x =>
        //    {
        //        return new Chain();
        //    });

        //    userTenantAdapter.Map(new UserTenantDto()).ReturnsForAnyArgs(x =>
        //    {
        //        return UserTenant.Create(_notificationHandler)
        //       .WithTenantId(Guid.NewGuid())
        //       .WithUserId(Guid.NewGuid())
        //       .WithId(Guid.NewGuid());
        //    });

        //    userTenantRepository.InsertAndSaveChangesAsync(new UserTenant()).ReturnsForAnyArgs(x =>
        //    {
        //        return new UserTenant();
        //    });

        //    await userTenantRepository.InsertRangeAndSaveChangesAsync(new List<UserTenant>());

        //    brandReadRepository.GetAllDtoByChainIdAsync(1).ReturnsForAnyArgs(x =>
        //    {
        //        return new List<BrandDto>();
        //    });

        //    propertyReadRepository.GetAllDtoByBrandIdListAsync(new List<int>()).ReturnsForAnyArgs(x =>
        //    {
        //        return new List<int>();
        //    });

        //    brandReadRepository.GetById(1).ReturnsForAnyArgs(x =>
        //    {
        //        return new Brand();
        //    });

        //    await new UserTotvsAppService(uow, userRepository, userReadRepository, userTenantRepository, brandReadRepository, chainReadRepository, userManager, userAdapter, userTenantAdapter, propertyReadRepository,
        //        applicationUser, personAppService, _notificationHandler)
        //       .CreateAsync(UserMock.GetDtoAsync().Result);

        //    Assert.False(_notificationHandler.HasNotification());

        //}

        //[Fact]
        //public async Task Should_Update()
        //{
        //    var userRepository = Substitute.For<IUserRepository>();
        //    var userReadRepository = Substitute.For<IUserReadRepository>();
        //    var userTenantRepository = Substitute.For<IUserTenantRepository>();
        //    var brandReadRepository = Substitute.For<IBrandReadRepository>();
        //    var chainReadRepository = Substitute.For<IChainReadRepository>();
        //    var userManager = Substitute.For<UserManager<User>>();
        //    var userAdapter = Substitute.For<IUserAdapter>();
        //    var userTenantAdapter = Substitute.For<IUserTenantAdapter>();
        //    var propertyReadRepository = Substitute.For<IPropertyReadRepository>();
        //    var applicationUser = Substitute.For<IApplicationUser>();
        //    var personAppService = Substitute.For<IPersonAppService>();
        //    var uow = Substitute.For<ISimpleUnitOfWork>();

        //    await new UserTotvsAppService(uow, userRepository, userReadRepository, userTenantRepository, brandReadRepository, chainReadRepository, userManager, userAdapter, userTenantAdapter, propertyReadRepository,
        //       applicationUser, personAppService, _notificationHandler)
        //      .UpdateAsync(UserMock.GetDtoAsync().Result, Guid.NewGuid());

        //    Assert.False(_notificationHandler.HasNotification());
        //}

        [Fact]
        public async Task Should_Toggle_IsActive()
        {
            var userRepository = Substitute.For<IUserRepository>();
            var uow = Substitute.For<ISimpleUnitOfWork>();

            await userRepository.ToggleIsActiveAsync(Guid.NewGuid());

            await new UserTotvsAppService(uow, userRepository, null, null, null, null, null, null, null, _notificationHandler)
                .ToggleIsActiveAsync(Guid.NewGuid());

            Assert.False(_notificationHandler.HasNotification());
        }
    }
}
