﻿using Shouldly;
using System.Threading.Tasks;
using Thex.SuperAdmin.Web.Controllers;
using Tnf.AspNetCore.TestBase;
using Xunit;
using Microsoft.Extensions.DependencyInjection;
using Thex.SuperAdmin.Application.Interfaces;
using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Web.Tests.Mocks;
using Thex.SuperAdmin.Dto.Dto;

namespace Thex.SuperAdmin.Web.Tests.Controllers
{
    public class CompanyControllerTests : TnfAspNetCoreIntegratedTestBase<StartupControllerTest>
    {
        [Fact]
        public void Should_Resolve_All()
        {
            TnfSession.ShouldNotBeNull();
            ServiceProvider.GetService<CompanyController>().ShouldNotBeNull();
            ServiceProvider.GetService<ICompanyAppService>().ShouldNotBeNull();
        }

        [Fact]
        public async Task Should_Post()
        {
            var company = await PostResponseAsObjectAsync<CompanyDto, CompanyDto>(
            $"{RouteConsts.Company}", CompanyMock.GetDto());

            Assert.NotNull(company);
        }

        [Fact]
        public async Task Should_GetAll()
        {
            var response = await GetResponseAsObjectAsync<ThexListDto<CompanyDto>>(
                $"{RouteConsts.Company}");

            Assert.Equal(3, response.TotalItems);
        }

        [Fact]
        public async Task Should_GetById()
        {
            var response = await GetResponseAsObjectAsync<CompanyDto>(
                $"{RouteConsts.Company}/{1}");

            Assert.NotNull(response);
        }

        [Fact]
        public async Task Should_GetLocationFromCompany()
        {
            var response = await GetResponseAsObjectAsync<CompanyDto>(
                $"{RouteConsts.Company}/{1}/locations");

            Assert.NotNull(response);
        }

        [Fact]
        public async Task Should_Put()
        {
            var company = await PutResponseAsObjectAsync<CompanyDto, CompanyDto>(
            $"{RouteConsts.Company}/{1}", CompanyMock.GetDto());

            Assert.NotNull(company);
        }
    }
}
