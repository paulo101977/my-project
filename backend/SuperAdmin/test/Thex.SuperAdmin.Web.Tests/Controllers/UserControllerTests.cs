﻿using Shouldly;
using Thex.SuperAdmin.Web.Controllers;
using Tnf.AspNetCore.TestBase;
using Xunit;
using Microsoft.Extensions.DependencyInjection;
using Thex.SuperAdmin.Application.Interfaces;
using System.Threading.Tasks;
using Thex.SuperAdmin.Dto.Dto;
using Thex.SuperAdmin.Web.Tests.Mocks;
using Thex.Kernel;
using Microsoft.Extensions.Caching.Distributed;
using Thex.AspNetCore.Security.Interfaces;
using Thex.SuperAdmin.Dto;

namespace Thex.SuperAdmin.Web.Tests.Controllers
{
    public class UserControllerTests : TnfAspNetCoreIntegratedTestBase<StartupControllerTest>
    {
        [Fact]
        public void Should_Resolve_All()
        {
            TnfSession.ShouldNotBeNull();
            ServiceProvider.GetService<UserController>().ShouldNotBeNull();
            ServiceProvider.GetService<IUserAppService>().ShouldNotBeNull();
            ServiceProvider.GetService<IApplicationUser>().ShouldNotBeNull();
            ServiceProvider.GetService<IUserRoleAppService>().ShouldNotBeNull();
            ServiceProvider.GetService<IDistributedCache>().ShouldNotBeNull();
            ServiceProvider.GetService<ITokenConfiguration>().ShouldNotBeNull();

        }

        [Fact]
        public Task Should_Path_UpdateUser()
        {
            var url = $"{RouteConsts.User}/{"UpdateUser"}";

            return PatchResponseAsObjectAsync<UpdateUserDto, UpdateUserDto>(
                    url, null);
        }


        [Fact]
        public Task Should_Path_ChangePersonPwd()
        {
            var url = $"{RouteConsts.User}/{"person/changepassword"}";

            return PatchResponseAsObjectAsync<ChangePwdPersonDto, ChangePwdPersonDto>(
                    url, null);
        }


    }
}
