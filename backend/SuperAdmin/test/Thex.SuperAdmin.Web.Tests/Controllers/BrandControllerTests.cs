﻿using Shouldly;
using System.Threading.Tasks;
using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Dto.Dto;
using Thex.SuperAdmin.Web.Tests.Mocks;
using Tnf.AspNetCore.TestBase;
using Xunit;
using Microsoft.Extensions.DependencyInjection;
using Thex.SuperAdmin.Web.Controllers;
using Thex.SuperAdmin.Application.Interfaces;

namespace Thex.SuperAdmin.Web.Tests.Controllers
{
    public class BrandControllerTests : TnfAspNetCoreIntegratedTestBase<StartupControllerTest>
    {
        [Fact]
        public void Should_Resolve_All()
        {
            TnfSession.ShouldNotBeNull();
            ServiceProvider.GetService<BrandController>().ShouldNotBeNull();
            ServiceProvider.GetService<IBrandAppService>().ShouldNotBeNull();
        }

        [Fact]
        public async Task Should_Post()
        {
            var company = await PostResponseAsObjectAsync<BrandDto, BrandDto>(
            $"{RouteConsts.Brand}", BrandMock.GetDto());

            Assert.NotNull(company);
        }

        [Fact]
        public async Task Should_GetAll()
        {
            var response = await GetResponseAsObjectAsync<ThexListDto<BrandDto>>(
                $"{RouteConsts.Brand}");

            Assert.Equal(3, response.TotalItems);
        }

        [Fact]
        public async Task Should_GetById()
        {
            var response = await GetResponseAsObjectAsync<BrandDto>(
                $"{RouteConsts.Brand}/{"d7db9f3c-bacf-467a-b08c-d9303227cc88"}");

            Assert.NotNull(response);
        }

        [Fact]
        public async Task Should_Put()
        {
            var brand = await PutResponseAsObjectAsync<BrandDto, BrandDto>(
            $"{RouteConsts.Brand}/{"d7db9f3c-bacf-467a-b08c-d9303227cc88"}", BrandMock.GetDto());

            Assert.NotNull(brand);
        }
    }
}
