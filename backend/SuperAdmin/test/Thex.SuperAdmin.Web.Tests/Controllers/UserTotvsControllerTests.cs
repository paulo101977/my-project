﻿using Shouldly;
using Thex.SuperAdmin.Web.Controllers;
using Tnf.AspNetCore.TestBase;
using Xunit;
using Microsoft.Extensions.DependencyInjection;
using Thex.SuperAdmin.Application.Interfaces;
using System.Threading.Tasks;
using Thex.SuperAdmin.Dto.Dto;
using Thex.SuperAdmin.Web.Tests.Mocks;

namespace Thex.SuperAdmin.Web.Tests.Controllers
{
    public class UserTotvsControllerTests : TnfAspNetCoreIntegratedTestBase<StartupControllerTest>
    {
        [Fact]
        public void Should_Resolve_All()
        {
            TnfSession.ShouldNotBeNull();
            ServiceProvider.GetService<UserTotvsController>().ShouldNotBeNull();
            ServiceProvider.GetService<IUserTotvsAppService>().ShouldNotBeNull();
        }

        [Fact]
        public async Task Should_Post()
        {
            var user = await PostResponseAsObjectAsync<CreateOrUpdateUserDto, CreateOrUpdateUserDto>(
            $"{RouteConsts.UserTotvs}", UserMock.GetDtoAsync().Result);

            Assert.NotNull(user);
        }

        [Fact]
        public async Task Should_GetAll()
        {
            var response = await GetResponseAsObjectAsync<ThexListDto<UsersDto>>(
                $"{RouteConsts.UserTotvs}");

            Assert.Equal(3, response.TotalItems);
        }

        [Fact]
        public async Task Should_GetById()
        {
            var response = await GetResponseAsObjectAsync<UsersDto>(
                $"{RouteConsts.UserTotvs}/{"d7db9f3c-bacf-467a-b08c-d9303227cc88"}");

            Assert.NotNull(response);
        }

        [Fact]
        public async Task Should_Put()
        {
            var user = await PutResponseAsObjectAsync<CreateOrUpdateUserDto, CreateOrUpdateUserDto>(
            $"{RouteConsts.UserTotvs}/{"d7db9f3c-bacf-467a-b08c-d9303227cc88"}", UserMock.GetDtoAsync().Result);

            Assert.NotNull(user);
        }

        [Fact]
        public async Task Should_ToggleIsActive()
        {
            var brand = await PatchResponseAsync(
            $"{RouteConsts.UserTotvs}/{"d7db9f3c-bacf-467a-b08c-d9303227cc88"}/toggleisactive", null);

            Assert.NotNull(brand);
        }
    }
}
