﻿using Shouldly;
using System;
using System.Collections.Generic;
using System.Text;
using Tnf.AspNetCore.TestBase;
using Microsoft.Extensions.DependencyInjection;
using Thex.SuperAdmin.Web.Controllers;
using Thex.SuperAdmin.Application.Interfaces;
using Xunit;
using System.Threading.Tasks;
using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Web.Tests.Mocks;
using Tnf.Dto;
using Thex.SuperAdmin.Dto.Dto;

namespace Thex.SuperAdmin.Web.Tests.Controllers
{
    public class ChainControllerTests : TnfAspNetCoreIntegratedTestBase<StartupControllerTest>
    {
        public void Should_Resolve_All()
        {
            TnfSession.ShouldNotBeNull();
            ServiceProvider.GetService<ChainController>().ShouldNotBeNull();
            ServiceProvider.GetService<IChainAppService>().ShouldNotBeNull();
        }

        [Fact]
        public async Task Should_Post()
        {
            var brand = await PostResponseAsObjectAsync<ChainDto, ChainDto>(
            $"{RouteConsts.Chain}", ChainMock.GetDto());

            Assert.NotNull(brand);
        }

        [Fact]
        public async Task Should_GetAll()
        {
            var response = await GetResponseAsObjectAsync<ThexListDto<ChainDto>>(
                $"{RouteConsts.Chain}");

            Assert.Equal(3, response.TotalItems);
        }

        [Fact]
        public async Task Should_GetById()
        {
            var response = await GetResponseAsObjectAsync<ChainDto>(
                $"{RouteConsts.Chain}/{1}");

            Assert.NotNull(response);
        }

        [Fact]
        public async Task Should_Put()
        {
            var chain = await PutResponseAsObjectAsync<ChainDto, ChainDto>(
            $"{RouteConsts.Chain}/{1}", ChainMock.GetDto());

            Assert.NotNull(chain);
        }
    }
}
