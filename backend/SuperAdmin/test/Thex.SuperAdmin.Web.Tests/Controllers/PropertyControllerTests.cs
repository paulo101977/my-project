﻿using Shouldly;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Tnf.AspNetCore.TestBase;
using Xunit;
using Microsoft.Extensions.DependencyInjection;
using Thex.SuperAdmin.Web.Controllers;
using Thex.SuperAdmin.Application.Interfaces;
using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Web.Tests.Mocks;
using Thex.SuperAdmin.Dto.Dto;

namespace Thex.SuperAdmin.Web.Tests.Controllers
{
    public class PropertyControllerTests : TnfAspNetCoreIntegratedTestBase<StartupControllerTest>
    {
        [Fact]
        public void Should_Resolve_All()
        {
            TnfSession.ShouldNotBeNull();
            ServiceProvider.GetService<PropertyController>().ShouldNotBeNull();
            ServiceProvider.GetService<IPropertyAppService>().ShouldNotBeNull();
        }

        [Fact]
        public async Task Should_Post()
        {
            var property = await PostResponseAsObjectAsync<CreateOrUpdatePropertyDto, CreateOrUpdatePropertyDto>(
            $"{RouteConsts.Property}", PropertyMock.GetCreateDtoAsync().Result);

            Assert.NotNull(property);
        }

        [Fact]
        public async Task Should_GetAll()
        {
            var response = await GetResponseAsObjectAsync<ThexListDto<PropertyDto>>(
                $"{RouteConsts.Property}");

            Assert.Equal(3, response.TotalItems);
        }

        [Fact]
        public async Task Should_GetById()
        {
            var response = await GetResponseAsObjectAsync<PropertyDto>(
                $"{RouteConsts.Property}/{1}");

            Assert.NotNull(response);
        }

        [Fact]
        public async Task Should_Put()
        {
            var property = await PutResponseAsObjectAsync<PropertyDto, PropertyDto>(
            $"{RouteConsts.Property}/{1}", PropertyMock.GetDto());

            Assert.NotNull(property);
        }
    }
}
