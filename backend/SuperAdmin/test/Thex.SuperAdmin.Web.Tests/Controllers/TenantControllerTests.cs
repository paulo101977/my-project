﻿using Shouldly;
using Tnf.AspNetCore.TestBase;
using Xunit;
using Microsoft.Extensions.DependencyInjection;
using System.Threading.Tasks;
using System.Collections.Generic;
using Thex.SuperAdmin.Web.Controllers;
using Thex.SuperAdmin.Application.Interfaces;
using Thex.SuperAdmin.Web.Tests.Mocks;
using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Dto.Dto;

namespace Thex.SuperAdmin.Web.Tests.Controllers
{
    public class TenantControllerTests : TnfAspNetCoreIntegratedTestBase<StartupControllerTest>
    {
        [Fact]
        public void Should_Resolve_All()
        {
            TnfSession.ShouldNotBeNull();
            ServiceProvider.GetService<TenantController>().ShouldNotBeNull();
            ServiceProvider.GetService<ITenantAppService>().ShouldNotBeNull();
        }

        [Fact]
        public async Task Should_Post()
        {
            var tenant = await PostResponseAsObjectAsync<TenantDto, TenantDto>(
            $"{RouteConsts.Tenant}", TenantMock.GetDtoWithoutAssociation());

            Assert.NotNull(tenant);
        }

        [Fact]
        public async Task Should_GetAll()
        {
            var response = await GetResponseAsObjectAsync<ThexListDto<TenantDto>>(
                $"{RouteConsts.Tenant}");

            Assert.Equal(3, response.TotalItems);
        }

        [Fact]
        public async Task Should_GetById()
        {
            var response = await GetResponseAsObjectAsync<TenantDto>(
                $"{RouteConsts.Tenant}/{"d7db9f3c-bacf-467a-b08c-d9303227cc88"}");

            Assert.NotNull(response);
        }

        [Fact]
        public async Task Should_Put()
        {
            var tenant = await PutResponseAsObjectAsync<TenantDto, TenantDto>(
            $"{RouteConsts.Tenant}/{"d7db9f3c-bacf-467a-b08c-d9303227cc88"}", TenantMock.GetDtoWithoutAssociation());

            Assert.NotNull(tenant);
        }
    }
}
