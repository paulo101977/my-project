﻿using Shouldly;
using Thex.SuperAdmin.Web.Controllers;
using Tnf.AspNetCore.TestBase;
using Xunit;
using Microsoft.Extensions.DependencyInjection;
using Thex.SuperAdmin.Application.Interfaces;
using System.Threading.Tasks;
using Thex.SuperAdmin.Dto.Dto;
using Thex.SuperAdmin.Dto;
using System.Net.Http;

namespace Thex.SuperAdmin.Web.Tests.Controllers
{
    public class DashboardControllerTests : TnfAspNetCoreIntegratedTestBase<StartupControllerTest>
    {
        [Fact]
        public void Should_Resolve_All()
        {
            TnfSession.ShouldNotBeNull();
            ServiceProvider.GetService<DashboardController>().ShouldNotBeNull();
            ServiceProvider.GetService<IDashboardAppService>().ShouldNotBeNull();
        }

        [Fact]
        public async Task Should_GetAll()
        {
            var response = await GetResponseAsObjectAsync<ThexListDto<DashboardPropertyDto>>(
                $"{RouteConsts.Dashboard}");

            Assert.Equal(3, response.TotalItems);
        }

        [Fact]
        public async Task Should_NextPropertyStatus()
        {
           await PatchResponseAsync(
               $"{RouteConsts.Dashboard}/next/1", new StringContent(""));
        }

        [Fact]
        public async Task Should_toggleBlockProperty()
        {
            await PatchResponseAsync(
                $"{RouteConsts.Dashboard}/1/toggleblockproperty", null);
        }
    }
}
