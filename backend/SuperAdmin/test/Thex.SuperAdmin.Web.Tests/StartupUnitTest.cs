﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;
using Thex.AspNetCore.Security;
using Thex.AspNetCore.Security.Interfaces;
using Thex.Common;
using Thex.Kernel;
using Thex.Kernel.Dto;
using Thex.Location.Application;
using Thex.Maps.Geocode;
using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Infra;
using Thex.SuperAdmin.Infra.Context;
using Thex.SuperAdmin.Infra.Entities;
using Thex.SuperAdmin.Infra.Interfaces;
using Thex.SuperAdmin.Infra.Uow;
using Thex.SuperAdmin.Web.Tests.Mocks;
using Tnf.Configuration;
using Tnf.Localization;

namespace Thex.SuperAdmin.Web.Tests
{
    public class StartupUnitTest
    {
        private IConfiguration Configuration { get; set; }

        public StartupUnitTest(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            services
                .AddApplicationServiceDependency()
                .AddApplicationServiceLocationDependency(options =>
                {
                    var settingsSection = Configuration.GetSection("MapsGeocodeConfig");
                    var settings = settingsSection.Get<MapsGeocodeConfig>();
                }, Configuration)
                .AddInfraDependency()
                .AddTnfAspNetCoreSetupTest()
                .AddTnfEfCoreSqliteInMemory();

            services.AddDbContext<ThexIdentityDbContext>(context => {
                context.UseInMemoryDatabase("ThexIdentityDbContext")
                .ConfigureWarnings(w => w.Ignore(InMemoryEventId.TransactionIgnoredWarning));
            });

            services
                .AddIdentity<User, Roles>(options => { })
                .AddEntityFrameworkStores<ThexIdentityDbContext>();

            services.AddSingleton<ITokenConfiguration>(e => new TokenConfiguration());
            services.AddSingleton<ISignConfiguration>(e => new SignConfiguration());
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddScoped<IApplicationUser, ApplicationUser>();
            services.AddScoped<ITokenManager, TokenManager>();
            services.AddScoped<IUserManagerMock, UserManagerMock>();
         

            services.AddScoped<IApplicationUser>(a => new ApplicationUserMock());
            services.AddScoped<Storage.IAzureStorage, Storage.AzureStorage>();
            var serviceProvider = services.BuildServiceProvider();

            ServiceLocator.SetLocatorProvider(serviceProvider);

            //Register Services
            services.AddTransient<ISimpleUnitOfWork, SimpleUnitOfWork>();

            return services.BuildServiceProvider();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app)
        {
            // Configura o uso do teste
            app.UseTnfAspNetCoreSetupTest(options =>
            {
                options.UseDomainLocalization();

                options.Repository(repositoryConfig =>
                {
                    repositoryConfig.Entity<IEntityGuid>(entity =>
                        entity.RequestDto<IDefaultGuidRequestDto>((e, d) => e.Id == d.Id));

                    repositoryConfig.Entity<IEntityInt>(entity =>
                        entity.RequestDto<IDefaultIntRequestDto>((e, d) => e.Id == d.Id));

                    repositoryConfig.Entity<IEntityLong>(entity =>
                        entity.RequestDto<IDefaultLongRequestDto>((e, d) => e.Id == d.Id));

                });
            });

            // Habilita o uso do UnitOfWork em todo o request
            app.UseTnfUnitOfWork();

            app.UseMvc(routes =>
            {
                routes.MapRoute("default", "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }

    public class ApplicationUserMock : IApplicationUser
    {
        public List<LoggedUserPropertyDto> PropertyList => throw new NotImplementedException();

        public string Name => "Teste";

        public string UserEmail => "teste@email.com";

        public Guid UserUid => new Guid("215bc8a0-07db-4428-9955-fa53b58b4ded");

        public string UserName => "Teste";

        public Guid TenantId => new Guid("9bece27c-cf67-4ea3-8ae8-357b3080475d");       

        public string PropertyId => "1";

        public string ChainId => "1";

        public bool IsAdmin => false;

        public string PreferredLanguage => "pt-br";

        public string PreferredCulture => "pt-br";

        public string PropertyLanguage => "pt-br";

        public string PropertyCulture => "pt-br";

        public string TimeZoneName => "America/Sao_Paulo";

        public string PropertyCountryCode => throw new NotImplementedException();

        public string PropertyDistrictCode => throw new NotImplementedException();

        public string PropertyUid => "5663f003-4557-4086-91f7-e9f6d3cf044a";

        string IApplicationUser.UserName { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

        public Dictionary<string, string> GetClaimsIdentity()
        {
            var claims = new Dictionary<string, string>();
            claims.Add("admin", "true");
            return claims;
        }

        public bool IsAuthenticated()
        {
            return true;
        }

        public void SetProperties(string userUid, string tenantId, string propertyId, string timeZoneName)
        {
            throw new NotImplementedException();
        }
    }
}
