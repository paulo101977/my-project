﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using Thex.AspNetCore.Security.Interfaces;
using Thex.Kernel;
using Thex.Location.Application;
using Thex.Location.Application.Interfaces;
using Thex.Location.Application.Services;
using Thex.Maps.Geocode;
using Thex.SuperAdmin.Application.Interfaces;
using Thex.SuperAdmin.Application.Services;
using Thex.SuperAdmin.Web.Tests.Mocks;

namespace Thex.SuperAdmin.Web.Tests
{
    public class StartupControllerTest
    {
        private IConfiguration Configuration { get; set; }

        public StartupControllerTest(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            services.AddTnfAspNetCoreSetupTest();
            services.AddApplicationServiceLocationDependency(options =>
            {
                var settingsSection = Configuration.GetSection("MapsGeocodeConfig");
                var settings = settingsSection.Get<MapsGeocodeConfig>();
            }, Configuration);
            services.AddTransient<ITenantAppService, TenantAppServiceMock>();
            services.AddTransient<ILocationAppService, LocationAppService>();
            services.AddTransient<IBrandAppService, BrandAppServiceMock>();
            services.AddTransient<IChainAppService, ChainAppServiceMock>();
            services.AddTransient<ICompanyAppService, CompanyAppServiceMock>();
            services.AddTransient<IPropertyAppService, PropertyAppServiceMock>();
            services.AddTransient<IDashboardAppService, DashboardAppServiceMock>();
            services.AddTransient<IUserTotvsAppService, UserTotvsAppServiceMock>();
            services.AddTransient<IUserAppService, UserAppServiceMock>();
            services.AddTransient<IUserRoleAppService, UserRoleAppServiceMock>();
            services.AddTransient<IDistributedCache, DistributedCacheMock>();
            services.AddTransient<ITokenConfiguration, TokenConfigurationMock>();


            

            services.AddScoped<IApplicationUser>(a => new ApplicationUserMock());
            var serviceProvider = services.BuildServiceProvider();

            ServiceLocator.SetLocatorProvider(serviceProvider);

            return services.BuildServiceProvider();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app)
        {
            // Configura o uso do teste
            app.UseTnfAspNetCoreSetupTest();

            app.UseMvc(routes =>
            {
                routes.MapRoute("default", "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
