﻿using System;
using System.Collections.Generic;
using System.Text;
using Thex.SuperAdmin.Application.Interfaces;
using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Dto.Dto;
using Tnf.Dto;

namespace Thex.SuperAdmin.Web.Tests.Mocks
{
    public class TenantAppServiceMock : ITenantAppService
    {
        public TenantDto Create(TenantDto dto)
        {
            return TenantMock.GetDtoWithoutAssociation();
        }

        public TenantDto Get(Guid id)
        {
            return TenantMock.GetDtoWithoutAssociation();
        }

        public ThexListDto<TenantDto> GetAllByFilter(GetAllTenantDto dto)
        {
            return TenantMock.GetDtoList();
        }

        public IListDto<TenantDto> GetAllWithoutAssociation(Guid? tenantId)
        {
            return TenantMock.GetDtoList();
        }

        public TenantDto Update(TenantDto dto, Guid id)
        {
            return TenantMock.GetDtoWithoutAssociation();
        }
    }
}
