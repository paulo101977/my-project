﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Dto.Dto;

namespace Thex.SuperAdmin.Web.Tests.Mocks
{
    public class TenantMock
    {
        public static TenantDto GetDtoWithChain(string name = "Tenant teste")
        {
            return new TenantDto()
            {
                Id = new Guid("d7db9f3c-bacf-467a-b08c-d9303227cc88"),
                TenantName = name,
                ChainId = 1,
                IsActive = true,
                ChainName = "Rede da Tenant"
            };
        }

        public static TenantDto GetDtoWithBrand(string name = "Tenant teste")
        {
            return new TenantDto()
            {
                Id = new Guid("d7db9f3c-bacf-467a-b08c-d9303227cc88"),
                TenantName = name,
                BrandId = 1,
                BrandName = "Bandeira da Tenant",
                ChainId = 1,
                IsActive = true,
                ChainName = "Rede da Tenant"
            };
        }

        public static TenantDto GetDtoWithProperty(string name = "Tenant teste")
        {
            return new TenantDto()
            {
                Id = new Guid("d7db9f3c-bacf-467a-b08c-d9303227cc88"),
                TenantName = name,
                BrandId = 1,
                BrandName = "Bandeira da Tenant",
                ChainId = 1,
                IsActive = true,
                ChainName = "Rede da Tenant",
                PropertyId = 1,
                PropertyName = "Property da Tenant"
            };
        }

        public static TenantDto GetDtoWithoutAssociation(string name = "Tenant teste")
        {
            return new TenantDto()
            {
                Id = new Guid("d7db9f3c-bacf-467a-b08c-d9303227cc88"),
                TenantName = name,
                IsActive = true,
            };
        }

        public static ThexListDto<TenantDto> GetDtoList()
        {
            var list = new List<TenantDto>();
            list.Add(TenantMock.GetDtoWithProperty("Tenant 1"));
            list.Add(TenantMock.GetDtoWithProperty("Tenant 2"));
            list.Add(TenantMock.GetDtoWithProperty("Tenant 3"));

            return new ThexListDto<TenantDto>()
            {
                HasNext = false,
                Items = list,
                TotalItems = list.Count
            };
        }
    }
}
