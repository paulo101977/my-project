﻿using Microsoft.AspNetCore.Identity;
using System.Threading.Tasks;
using Thex.SuperAdmin.Infra.Entities;

namespace Thex.SuperAdmin.Web.Tests.Mocks
{
    public interface IUserManagerMock
    {
        Task<IdentityResult> CreateAsync(User user, string password);
        Task<User> FindByEmailAsync(string email);
    }
}