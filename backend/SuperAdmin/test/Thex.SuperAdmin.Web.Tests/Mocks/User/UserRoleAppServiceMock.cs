﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.AspNetCore.Security;
using Thex.SuperAdmin.Application.Interfaces;
using Thex.SuperAdmin.Dto;
using Tnf.Dto;

namespace Thex.SuperAdmin.Web.Tests.Mocks
{
    public class UserRoleAppServiceMock : IUserRoleAppService
    {
        public Task CreateAsync(Guid userId, Guid roleId, List<int> propertyIdListDto)
        {
            throw new NotImplementedException();
        }

        public Task CreateorUpdateAsync(Guid userId, List<Guid> roleIdList, int? propertyId = null, int? brandId = null, int? chainId = null)
        {
            throw new NotImplementedException();
        }
    }
}
