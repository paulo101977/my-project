﻿using System;
using System.Threading.Tasks;
using Thex.SuperAdmin.Application.Interfaces;
using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Dto.Dto;

namespace Thex.SuperAdmin.Web.Tests.Mocks
{
    public class UserTotvsAppServiceMock : IUserTotvsAppService
    {
        public async Task<CreateOrUpdateUserDto> CreateAsync(CreateOrUpdateUserDto dto)
        {
            return await UserMock.GetDtoAsync();
        }

        public async Task<ThexListDto<UsersDto>> GetAllAsync(GetAllUserDto request)
        {
            return await UserMock.GetDtoListAsync();
        }

        public async Task<UsersDto> GetAsync(Guid id)
        {
            return await UserMock.GetDto().AsTask();
        }

        public async Task ToggleIsActiveAsync(Guid id)
        {
            
        }

        public async Task<CreateOrUpdateUserDto> UpdateAsync(CreateOrUpdateUserDto dto, Guid id)
        {
            return await UserMock.GetDtoAsync();
        }
    }
}
