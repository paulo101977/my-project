﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.AspNetCore.Security;
using Thex.SuperAdmin.Application.Interfaces;
using Thex.SuperAdmin.Dto;
using Tnf.Dto;

namespace Thex.SuperAdmin.Web.Tests.Mocks
{
    public class UserAppServiceMock : IUserAppService
    {
        public void ChangeCulture(Guid id, string culture)
        {
            throw new NotImplementedException();
        }

        public Task ChangePassword(string token, string oldPassword, string newPassword)
        {
            throw new NotImplementedException();
        }

        public Task ChangePasswordPerson(ChangePwdPersonDto dto)
        {
            throw new NotImplementedException();
        }

        public Task ChangePasswordPersonAsync(ChangePwdPersonDto dto)
        {
           return Task.FromResult("");
        }

        public Task<UserDto> ChangePhoto(Guid id, UserDto dto)
        {
            throw new NotImplementedException();
        }

        public Task<UserDto> CreateNewUser(UserInvitationDto dto)
        {
            throw new NotImplementedException();
        }

        public Task<UserDto> CreateNewUser(UserInvitationDto dto, List<Guid> roleIdList)
        {
            throw new NotImplementedException();
        }

        public Task<UserDto> CreateNewUserPassword(string token, string password)
        {
            throw new NotImplementedException();
        }

        public void ForgotPassword(string email, string frontUrl = null)
        {
            throw new NotImplementedException();
        }

        public void ForgotPasswordTotvs(string email)
        {
            throw new NotImplementedException();
        }

        public Task<UserDto> Get(DefaultGuidRequestDto id)
        {
            throw new NotImplementedException();
        }

        public Task<IListDto<UserDto>> GetAll(GetAllUserDto request)
        {
            throw new NotImplementedException();
        }

        public Task<IListDto<UserDto>> GetAllByLoggedTenantIdAsync(GetAllUserDto request)
        {
            throw new NotImplementedException();
        }

        public Task<UserDto> GetByLoggedUserIdAsync()
        {
            throw new NotImplementedException();
        }

        public AspNetCore.Security.UserTokenDto GetTokenByPropertyId(int propertyId)
        {
            throw new NotImplementedException();
        }

        public AspNetCore.Security.UserTokenDto Login(LoginDto login)
        {
            throw new NotImplementedException();
        }

        public AspNetCore.Security.UserTokenDto LoginByPropertyId(int propertyId)
        {
            throw new NotImplementedException();
        }

        public AspNetCore.Security.UserTokenDto LoginTotvs(LoginDto login)
        {
            throw new NotImplementedException();
        }

        public Task<string> NewInvitationLinkAsync(Guid id)
        {
            throw new NotImplementedException();
        }

        public Task NewPassword(string token, string password)
        {
            throw new NotImplementedException();
        }

        public void SetIsAdmin(Guid id, bool isAdmin)
        {
            throw new NotImplementedException();
        }

        public void ToggleActivation(Guid id)
        {
            throw new NotImplementedException();
        }

        public void ToggleActivationByLoggedPropertyId(Guid userId, int propertyId)
        {
            throw new NotImplementedException();
        }

        public Task UpdateUserAsync(UpdateUserDto dto)
        {
            return Task.FromResult("");
        }
    }
}
