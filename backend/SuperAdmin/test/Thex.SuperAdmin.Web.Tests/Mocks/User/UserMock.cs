﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Dto.Dto;
using Thex.SuperAdmin.Infra.Entities;

namespace Thex.SuperAdmin.Web.Tests.Mocks
{
    public static class UserMock
    {
        public async static Task<CreateOrUpdateUserDto> GetDtoAsync(string name = "Usuário teste")
        {
            return await Task.FromResult(new CreateOrUpdateUserDto()
            {
                PreferredLanguage = "pt-BR",
                Name = name,
                Email = "email@gmail.com",
                IsActive = true,
                UserName = "teste teste"
            });
        }

        public static UsersDto GetDto(string name = "Usuário teste")
        {
            return new UsersDto()
            {
                PreferredCulture = "pt-BR",
                PreferredLanguage = "pt-BR",
                Name = name,
                Email = "email@gmail.com",
                IsActive = true,
                UserName = "teste teste"
            };
        }
               

        public async static Task<ThexListDto<UsersDto>> GetDtoListAsync()
        {
            var list = new List<UsersDto>();
            list.Add(GetDto("Usuário 1"));
            list.Add(GetDto("Usuário 2"));
            list.Add(GetDto("Usuário 3"));

            return await Task.FromResult(new ThexListDto<UsersDto>()
            {
                HasNext = false,
                Items = list,
                TotalItems = list.Count
            });
        }

        public async static Task<User> GetUser()
        {
           return await Task.FromResult(new User()
            {
                Email = "teste@totvs.com"
            });
        }

        public static UserDto GetUserDto()
        {
            return new UserDto()
            {
                Name = "teste",
                Email = "teste@totvs.com"              
                
            };
        }


        public static ChangePwdPersonDto GetChangePwtDto()
        {
            return new ChangePwdPersonDto()
            {
              UserId = Guid.NewGuid(),
              NewPassword = "123",
              OldPassword = "321"

            };
        }



        public static UpdateUserDto GetUpdatedUserDto()
        {
            return new UpdateUserDto()
            {
                RoleIdList = new List<Guid>{Guid.NewGuid()},
                Name = "Teste 2"
            };
        }
          
    }
}
