﻿using Microsoft.Extensions.Caching.Distributed;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Thex.AspNetCore.Security;
using Thex.AspNetCore.Security.Interfaces;
using Thex.SuperAdmin.Application.Interfaces;
using Thex.SuperAdmin.Dto;
using Tnf.Dto;

namespace Thex.SuperAdmin.Web.Tests.Mocks
{
    public class TokenConfigurationMock : ITokenConfiguration
    {
        public string Audience { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public string Issuer { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public int Seconds { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public int FinalExpiration { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
    }
}
