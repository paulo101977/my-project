﻿
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Dto.Dto;

namespace Thex.SuperAdmin.Web.Tests.Mocks
{
    public class DashboardMock
    {
        public static DashboardPropertyDto GetPropertyDto(string name = "hotel teste")
        {
            return new DashboardPropertyDto()
            {
                Id = 1,
                CreationDate = DateTime.Now,
                InitialsName = "HT",
                IsBlocked = false,
                Name = name,
                PropertyStatusId = 1,
                PropertyStatusName = "Registro"
            };
        }

        public async static Task<ThexListDto<DashboardPropertyDto>> GetAllPropertiesAsync()
        {
            var dashboardPropertyList = new List<DashboardPropertyDto>();
            dashboardPropertyList.Add(GetPropertyDto("Hotel teste 1"));
            dashboardPropertyList.Add(GetPropertyDto("Hotel teste 2"));
            dashboardPropertyList.Add(GetPropertyDto("Hotel teste 3"));

            return await Task.FromResult(new ThexListDto<DashboardPropertyDto>()
            {
                Items = dashboardPropertyList,
                TotalItems = dashboardPropertyList.Count
            });
        }
    }
}
