﻿using System.Threading.Tasks;
using Thex.SuperAdmin.Application.Interfaces;
using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Dto.Dto;

namespace Thex.SuperAdmin.Web.Tests.Mocks
{
    public class DashboardAppServiceMock : IDashboardAppService
    {
        public async Task<ThexListDto<DashboardPropertyDto>> GetAllPropertiesAsync()
        {
            return await DashboardMock.GetAllPropertiesAsync();
        }

        public async Task NextPropertyStatusAsync(int propertyId)
        {
            await Task.FromResult("");
        }

        public async Task ToggleBlockPropertyAsync(int propertyId)
        {
            await Task.FromResult("");
        }
    }
}
