﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Dto.Dto;
using Thex.SuperAdmin.Infra.Entities;

namespace Thex.SuperAdmin.Web.Tests.Mocks
{
    public class BrandMock
    {
        public async static Task<BrandDto> GetDtoAsync(string name = "Brand teste")
        {
            return await Task.FromResult(new BrandDto()
            {
                Name = name,
                Id = 1,
                ChainId = 1,
                ChainName = "Rede da Brand",
                TenantId = Guid.NewGuid(),
                TenantName = "Tenant da Brand"
            });            
        }

        public static BrandDto GetDto(string name = "Brand teste")
        {
            return new BrandDto()
            {
                Name = name,
                Id = 1,
                ChainId = 1,
                ChainName = "Rede da Brand",
                TenantId = Guid.NewGuid(),
                TenantName = "Tenant da Brand"
            };
        }

        public async static Task<ThexListDto<BrandDto>> GetDtoListAsync()
        {
            var list = new List<BrandDto>();
            list.Add(GetDto("Brand 1"));
            list.Add(GetDto("Brand 2"));
            list.Add(GetDto("Brand 3"));

            return await Task.FromResult(new ThexListDto<BrandDto>()
            {
                TotalItems = list.Count,
                HasNext = false,
                Items = list
            });
        }

    }
}
