﻿using System;
using System.Threading.Tasks;
using Thex.SuperAdmin.Application.Interfaces;
using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Dto.Dto;

namespace Thex.SuperAdmin.Web.Tests.Mocks
{
    public class BrandAppServiceMock : IBrandAppService
    {
        public async Task<BrandDto> CreateAsync(BrandDto dto)
        {
            return await BrandMock.GetDtoAsync();
        }

        public BrandDto FirstOrDefault()
        {
            throw new NotImplementedException();
        }

        public async Task<ThexListDto<BrandDto>> GetAllByFilterAsync(GetAllBrandDto dto)
        {
            return await BrandMock.GetDtoListAsync();
        }

        public async Task<BrandDto> GetAsync(int id)
        {
            return await BrandMock.GetDtoAsync();
        }

        public async Task<BrandDto> UpdateAsync(int id, BrandDto dto)
        {
            return await BrandMock.GetDtoAsync();
        }
    }
}
