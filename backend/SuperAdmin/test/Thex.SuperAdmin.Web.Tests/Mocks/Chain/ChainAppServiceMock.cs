﻿using System.Threading.Tasks;
using Thex.SuperAdmin.Application.Interfaces;
using Thex.SuperAdmin.Dto;
using Tnf.Dto;

namespace Thex.SuperAdmin.Web.Tests.Mocks
{
    public class ChainAppServiceMock : IChainAppService
    {
        public async Task<ChainDto> Create(ChainDto dto)
        {
            return await ChainMock.GetDtoAsync();
        }

        public async Task<ChainDto> Get(DefaultIntRequestDto id)
        {
            return await ChainMock.GetDtoAsync();
        }

        public async Task<IListDto<ChainDto>> GetAll(GetAllChainDto request)
        {
            return await ChainMock.GetListDtoAsync();
        }

        public async Task<ChainDto> Update(int id, ChainDto dto)
        {
            return await ChainMock.GetDtoAsync();
        }
    }
}
