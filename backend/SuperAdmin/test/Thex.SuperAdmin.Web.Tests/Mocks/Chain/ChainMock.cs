﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Dto.Dto;
using Tnf.Dto;

namespace Thex.SuperAdmin.Web.Tests.Mocks
{
    public class ChainMock
    {
        public async static Task<ChainDto> GetDtoAsync(string name = "Chain teste")
        {
            return await Task.FromResult(new ChainDto()
            {
                Name = name,
                Id = 1,
                TenantId = Guid.NewGuid(),
                TenantName = "Tenant da Chain"
            });
        }

        public static ChainDto GetDto(string name = "Chain teste")
        {
            return new ChainDto()
            {
                Name = name,
                Id = 1,
                TenantId = Guid.NewGuid(),
                TenantName = "Tenant da Chain"
            };
        }

        public async static Task<IListDto<ChainDto>> GetListDtoAsync()
        {
            var list = new List<ChainDto>();
            list.Add(GetDto("Chain 1"));
            list.Add(GetDto("Chain 2"));
            list.Add(GetDto("Chain 3"));

            return await Task.FromResult(new ThexListDto<ChainDto>()
            {
                HasNext = false,
                Items = list,
                TotalItems = list.Count
            });
        }
    }
}
