﻿using System.Threading.Tasks;
using Thex.Common.Dto;
using Thex.SuperAdmin.Application.Interfaces;
using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Dto.Dto;

namespace Thex.SuperAdmin.Web.Tests.Mocks
{
    public class CompanyAppServiceMock : ICompanyAppService
    {
        public Task<CompanyDto> CreateAsync(CompanyDto dto)
        {
            return CompanyMock.GetDtoAsync();
        }

        public Task<ThexListDto<CompanyDto>> GetAllByFilterAsync(GetAllCompanyDto request)
        {
            return CompanyMock.GetListDtoAsync();
        }

        public Task<CompanyDto> GetAsync(int id)
        {
            return CompanyMock.GetDtoAsync();
        }

        public Task<ThexListDto<LocationDto>> GetLocationsFromCompanyAsync(int id)
        {
            return CompanyMock.GetLocationListAsync();
        }

        public Task<CompanyDto> UpdateAsync(int id, CompanyDto dto)
        {
            return CompanyMock.GetDtoAsync();
        }
    }
}
