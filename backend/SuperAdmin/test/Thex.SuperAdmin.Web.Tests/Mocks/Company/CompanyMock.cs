﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.Common.Dto;
using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Dto.Dto;

namespace Thex.SuperAdmin.Web.Tests.Mocks
{
    public class CompanyMock
    {
        public async static Task<CompanyDto> GetDtoAsync(string name = "company teste")
        {
            var documentDtoList = new List<DocumentDto>();
            documentDtoList.Add(new DocumentDto()
            {
                OwnerId = Guid.NewGuid(),
                DocumentTypeId = 2,
                DocumentInformation = "13.004.734/0001-90"
            });

            var locationList = new List<LocationDto>();
            locationList.Add(new LocationDto()
            {
                LocationCategoryId = 1,
                Latitude = -22.899903m,
                Longitude = -43.179114m,
                StreetName = "Rua Visconde de Inhauma",
                StreetNumber = "83",
                Neighborhood = "Centro",
                PostalCode = "20091",
                CountryCode = "BR",
                BrowserLanguage = "pt-BR"
            });

            return await Task.FromResult(new CompanyDto()
            {
                DocumentList = documentDtoList,
                LocationList = locationList,
                Email = "joao@gmail.com",
                HomePage = "joao@joao.com",
                ShortName = name,
                TradeName = name,
                PersonType = 'L'
            });
        }

        public static CompanyDto GetDto(string name = "company teste")
        {
            var documentDtoList = new List<DocumentDto>();
            documentDtoList.Add(new DocumentDto()
            {
                OwnerId = Guid.NewGuid(),
                DocumentTypeId = 2,
                DocumentInformation = "13.004.734/0001-90"
            });

            var locationList = new List<LocationDto>();
            locationList.Add(new LocationDto()
            {
                LocationCategoryId = 1,
                Latitude = -22.899903m,
                Longitude = -43.179114m,
                StreetName = "Rua Visconde de Inhauma",
                StreetNumber = "83",
                Neighborhood = "Centro",
                PostalCode = "20091",
                CountryCode = "BR",
                BrowserLanguage = "pt-BR"
            });

            return new CompanyDto()
            {
                DocumentList = documentDtoList,
                LocationList = locationList,
                Email = "joao@gmail.com",
                HomePage = "joao@joao.com",
                ShortName = name,
                TradeName = name,
                PersonType = 'L'
            };
        }

        public async static Task<ThexListDto<CompanyDto>> GetListDtoAsync()
        {
            var list = new List<CompanyDto>();
            list.Add(GetDto("company 1"));
            list.Add(GetDto("company 2"));
            list.Add(GetDto("company 3"));

            return await Task.FromResult(new ThexListDto<CompanyDto>()
            {
                HasNext = false,
                Items = list,
                TotalItems = list.Count
            });
        }

        public async static Task<ThexListDto<LocationDto>> GetLocationListAsync()
        {
            var locationList = new List<LocationDto>();
            locationList.Add(new LocationDto()
            {
                LocationCategoryId = 1,
                Latitude = -22.899903m,
                Longitude = -43.179114m,
                StreetName = "Rua Visconde de Inhauma",
                StreetNumber = "83",
                Neighborhood = "Centro",
                PostalCode = "20091",
                CountryCode = "BR",
                BrowserLanguage = "pt-BR"
            });

            locationList.Add(new LocationDto()
            {
                LocationCategoryId = 1,
                Latitude = -22.899903m,
                Longitude = -43.179114m,
                StreetName = "Rua Visconde de Inhauma",
                StreetNumber = "83",
                Neighborhood = "Centro",
                PostalCode = "20091",
                CountryCode = "BR",
                BrowserLanguage = "pt-BR"
            });

            locationList.Add(new LocationDto()
            {
                LocationCategoryId = 1,
                Latitude = -22.899903m,
                Longitude = -43.179114m,
                StreetName = "Rua Visconde de Inhauma",
                StreetNumber = "83",
                Neighborhood = "Centro",
                PostalCode = "20091",
                CountryCode = "BR",
                BrowserLanguage = "pt-BR"
            });

            return await Task.FromResult(new ThexListDto<LocationDto>()
            {
                HasNext = false,
                Items = locationList,
                TotalItems = locationList.Count
            });
        }
    }
}
