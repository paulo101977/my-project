﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Thex.SuperAdmin.Application.Interfaces;
using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Dto.Dto;

namespace Thex.SuperAdmin.Web.Tests.Mocks
{
    public class PropertyAppServiceMock : IPropertyAppService
    {
        public async Task<CreateOrUpdatePropertyDto> CreateAsync(CreateOrUpdatePropertyDto dto, IFormFile file)
        {
            return await PropertyMock.GetCreateDtoAsync();
        }

        public async Task<PropertyDto> Get(DefaultIntRequestDto id)
        {
            return await Task.FromResult(PropertyMock.GetDto());
        }

        public async Task<ThexListDto<PropertyDto>> GetAll(GetAllPropertyDto request)
        {
            return await PropertyMock.GetListDtoAsync();
        }

        public async Task<CreateOrUpdatePropertyDto> UpdateAsync(int id, CreateOrUpdatePropertyDto dto, IFormFile file)
        {
            return await PropertyMock.GetCreateDtoAsync();
        }
    }
}
