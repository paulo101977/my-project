﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Thex.Common.Dto;
using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Dto.Dto;

namespace Thex.SuperAdmin.Web.Tests.Mocks
{
    public class PropertyMock
    {
        public async static Task<CreateOrUpdatePropertyDto> GetCreateDtoAsync(string name = "Hotel teste")
        {
            var locationList = new List<LocationDto>();
            locationList.Add(new LocationDto
            {
                CountryCode = "br"
            });

            return await Task.FromResult(new CreateOrUpdatePropertyDto()
            {
                Name = name,
                Id = 1,
                TenantId = Guid.NewGuid(),
                BrandId = 1,
                CompanyId = 1,
                LocationList = locationList
            });
        }

        public static PropertyDto GetDto(string name = "Hotel teste")
        {
            return new PropertyDto()
            {
                Name = name,
                Id = 1,
                TenantId = Guid.NewGuid(),
                TenantName = "Tenant do Hotel",
                ChainId = 1,
                ChainName = "Rede do Hotel",
                BrandId = 1,
                BrandName = "Bandeira do Hotel",
                CompanyId = 1
            };
        }

        public static Task<ThexListDto<PropertyDto>> GetListDtoAsync()
        {
            var list = new List<PropertyDto>();
            list.Add(GetDto("hotel 1"));
            list.Add(GetDto("hotel 2"));
            list.Add(GetDto("hotel 3"));

            return Task.FromResult(new ThexListDto<PropertyDto>()
            {
                HasNext = false,
                Items = list,
                TotalItems = list.Count
            });
        }
    }
}
