﻿namespace Thex.SuperAdmin.Web
{
    public class RouteConsts
    {
        public const string User = "api/User";
        public const string Roles = "api/Role";
        public const string Chain = "api/Chain";
        public const string Dashboard = "api/Dashboard";
        public const string Tenant = "api/Tenant";
        public const string Brand = "api/Brand";
        public const string UserTotvs = "api/UserTotvs";
        public const string Company = "api/Company";
        public const string Property = "api/Property";
        public const string Feature = "api/Feature";
        public const string Permissions = "api/Permission";
        public const string Menu = "api/Menu";
        public const string IdentityProduct = "api/IdentityProduct";
        public const string IdentityModule = "api/IdentityModule";
        public const string UserPermissions = "api/UserPermission";
        public const string IntegrationPartner = "api/IntegrationPartner";
        public const string UserInvitation = "api/UserInvitation";
    }
}
