﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Thex.SuperAdmin.Application.Interfaces;
using Thex.SuperAdmin.Infra;
using Tnf.AspNetCore.Mvc.Response;

namespace Thex.SuperAdmin.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [AllowAnonymous]
    public class HealthController : TnfController
    {
        private readonly IBrandAppService _brandAppService;

        public HealthController(IBrandAppService brandAppService)
        {
            _brandAppService = brandAppService;
        }

        [HttpGet("check")]
        [ProducesResponseType(typeof(bool), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult Get()
        {
            var response = _brandAppService.FirstOrDefault();

            return CreateResponseOnGetAll(response != null ? true : false, EntityNames.Health);
        }
    }
}