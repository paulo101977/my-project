﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.Dto;
using Thex.Kernel;
using Thex.SuperAdmin.Application.Interfaces;
using Thex.SuperAdmin.Dto;
using Tnf.AspNetCore.Mvc.Response;
using Tnf.Dto;

namespace Thex.SuperAdmin.Web.Controllers
{
    [Route(RouteConsts.Roles)]
    [Authorize]
    public class RoleController : TnfController
    {
        private readonly IRolesAppService _roleAppService;

        public RoleController(IRolesAppService roleAppService, IApplicationUser applicationUser)
        {
            _roleAppService = roleAppService;
        }


        [HttpGet]
        [ProducesResponseType(typeof(IListDto<RolesDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Get([FromQuery] GetAllRoleDto requestDto)
        {
            var response = await _roleAppService.GetAllByFilterAsync(requestDto);

            return CreateResponseOnGetAll(response, RouteConsts.Roles);
        }

        [HttpGet("{id}")]
        [ProducesResponseType(typeof(RolesDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Get(Guid id, RequestDto requestDto)
        {
            var request = new DefaultGuidRequestDto(id, requestDto);

            var response = await _roleAppService.GetAsync(id);

            return CreateResponseOnGet(response, RouteConsts.Roles);
        }

        [HttpPut("{id}")]
        [ProducesResponseType(typeof(RolesDto), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Put(Guid id, [FromBody] RolesDto dto)
        {
            var response = await _roleAppService.UpdateAsync(id, dto);

            return CreateResponseOnPut(response, RouteConsts.Roles);
        }

        [HttpPost]
        [ProducesResponseType(typeof(IdentityPermissionDto), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Post([FromBody] RolesDto dto)
        {
            var response = await _roleAppService.CreateAsync(dto);

            return CreateResponseOnPost(response, RouteConsts.Roles);
        }

        [HttpDelete("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Delete(Guid id)
        {
            await _roleAppService.DeleteAsync(id);

            return CreateResponseOnDelete(RouteConsts.Roles);
        }

        [HttpPatch("{id}/toggleactivation")]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult ToggleActivation(Guid id)
        {
            _roleAppService.ToggleActivation(id);

            return CreateResponseOnPatch(null, RouteConsts.Roles);
        }
    }
}
