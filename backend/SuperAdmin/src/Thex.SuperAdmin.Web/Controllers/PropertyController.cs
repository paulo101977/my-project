﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Thex.SuperAdmin.Application.Interfaces;
using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Dto.Dto;
using Thex.SuperAdmin.Infra;
using Tnf.AspNetCore.Mvc.Response;
using Tnf.Dto;

namespace Thex.SuperAdmin.Web.Controllers
{
    [Route(RouteConsts.Property)]
    [Authorize]
    public class PropertyController : TnfController
    {
        private IPropertyAppService _propertyAppService;

        public PropertyController(IPropertyAppService propertyAppService)
        {
            _propertyAppService = propertyAppService;
        }

        [HttpPost]
        [ProducesResponseType(typeof(CreateOrUpdatePropertyDto), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Create([FromBody] CreateOrUpdatePropertyDto propertyDto, IFormFile file)
        {
            var response = await _propertyAppService.CreateAsync(propertyDto, file);

            return CreateResponseOnPost(response, EntityNames.Property);
        }

        [HttpGet]
        [ProducesResponseType(typeof(ThexListDto<PropertyDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> GetAll([FromQuery] GetAllPropertyDto requestDto)
        {
            var response = await _propertyAppService.GetAll(requestDto);

            return CreateResponseOnGetAll(response, EntityNames.Property);
        }

        [HttpGet("{id}")]
        [ProducesResponseType(typeof(PropertyDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Get(int id, RequestDto requestDto)
        {
            var request = new DefaultIntRequestDto(id, requestDto);

            var response = await _propertyAppService.Get(request);

            return CreateResponseOnGet(response, RouteConsts.Property);
        }

        [HttpPut("{id}")]
        [ProducesResponseType(typeof(CreateOrUpdatePropertyDto), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Put(int id, [FromBody] CreateOrUpdatePropertyDto propertyDto, IFormFile file)
        {
            var response = await _propertyAppService.UpdateAsync(id, propertyDto, file);

            return CreateResponseOnPut(response, RouteConsts.Property);
        }

    }
}