﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Primitives;
using System;
using System.Threading.Tasks;
using Thex.Dto;
using Thex.Kernel;
using Thex.SuperAdmin.Application.Interfaces;
using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Infra;
using Tnf.AspNetCore.Mvc.Response;
using Tnf.Dto;
using ThexSecurity = Thex.AspNetCore.Security;

namespace Thex.SuperAdmin.Web.Controllers
{
    [Route(RouteConsts.Feature)]
    [Authorize]
    public class FeatureController : TnfController
    {
        private readonly IFeatureAppService _featureAppService;
        private readonly IApplicationUser _applicationUser;

        public FeatureController(IApplicationUser applicationUser, IFeatureAppService featureAppService)
        {
            _featureAppService = featureAppService;
            _applicationUser = applicationUser;
        }

        [HttpGet]
        [ProducesResponseType(typeof(IListDto<IdentityFeatureDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Get([FromQuery] GetAllIdentityFeatureDto requestDto)
        {
            var response = await _featureAppService.GetAllByFilterAsync(requestDto);

            return CreateResponseOnGetAll(response, RouteConsts.Feature);
        }

        [HttpGet("{id}")]
        [ProducesResponseType(typeof(IdentityFeatureDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Get(Guid id, RequestDto requestDto)
        {
            var request = new DefaultGuidRequestDto(id, requestDto);

            var response = await _featureAppService.GetAsync(id);

            return CreateResponseOnGet(response, RouteConsts.Feature);
        }

        [HttpPut("{id}")]
        [ProducesResponseType(typeof(IdentityFeatureDto), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Put(Guid id, [FromBody] IdentityFeatureDto dto)
        {
            var response = await _featureAppService.UpdateAsync(id, dto);

            return CreateResponseOnPut(response, RouteConsts.Feature);
        }

        [HttpPost]
        [ProducesResponseType(typeof(IdentityFeatureDto), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Post([FromBody] IdentityFeatureDto dto)
        {
            var response = await _featureAppService.CreateAsync(dto);

            return CreateResponseOnPost(response, RouteConsts.Feature);
        }

        [HttpDelete("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Delete(Guid id)
        {
            await _featureAppService.DeleteAsync(id);

            return CreateResponseOnDelete(RouteConsts.Feature);
        }

    }
}
