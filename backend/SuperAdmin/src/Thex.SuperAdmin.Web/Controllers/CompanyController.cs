﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Thex.Common.Dto;
using Thex.SuperAdmin.Application.Interfaces;
using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Dto.Dto;
using Thex.SuperAdmin.Infra;
using Tnf.AspNetCore.Mvc.Response;
using Tnf.Dto;

namespace Thex.SuperAdmin.Web.Controllers
{
    [Route(RouteConsts.Company)]
    [Authorize]
    public class CompanyController : TnfController
    {
        private readonly ICompanyAppService _companyAppService;

        public CompanyController(ICompanyAppService companyAppService)
        {
            _companyAppService = companyAppService;
        }

        [HttpGet]
        [ProducesResponseType(typeof(ThexListDto<CompanyDto>), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> GetAll([FromQuery] GetAllCompanyDto requestDto)
        {
            var response = await _companyAppService.GetAllByFilterAsync(requestDto);

            return CreateResponseOnGetAll(response, EntityNames.Company);
        }

        [HttpGet("{id}")]
        [ProducesResponseType(typeof(CompanyDto), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Get(int id)
        {
            var response = await _companyAppService.GetAsync(id);

            return CreateResponseOnGet(response, EntityNames.Company);
        }

        [HttpGet("{id}/locations")]
        [ProducesResponseType(typeof(ThexListDto<LocationDto>), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> GetLocations(int id)
        {
            var response = await _companyAppService.GetLocationsFromCompanyAsync(id);

            return CreateResponseOnGetAll(response, EntityNames.Company);
        }

        [HttpPost]
        [ProducesResponseType(typeof(CompanyDto), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Create([FromBody] CompanyDto companyDto)
        {
            var response = await _companyAppService.CreateAsync(companyDto);

            return CreateResponseOnPost(response, EntityNames.Company);
        }

        [HttpPut("{id}")]
        [ProducesResponseType(typeof(CompanyDto), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Put([FromBody] CompanyDto companyDto, int id)
        {
            var response = await _companyAppService.UpdateAsync(id, companyDto);

            return CreateResponseOnPut(response, EntityNames.Company);
        }
    }
}