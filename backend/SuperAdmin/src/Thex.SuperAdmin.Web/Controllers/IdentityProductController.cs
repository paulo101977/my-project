﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Thex.SuperAdmin.Application.Interfaces;
using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Dto.Dto;
using Thex.SuperAdmin.Infra;
using Tnf.AspNetCore.Mvc.Response;

namespace Thex.SuperAdmin.Web.Controllers
{
    [Route(RouteConsts.IdentityProduct)]
    [Authorize]
    public class IdentityProductController : TnfController
    {
        private IIdentityProductAppService _identityProductAppService;

        public IdentityProductController(IIdentityProductAppService identityProductAppService)
        {
            _identityProductAppService = identityProductAppService;
        }

        [HttpGet]
        [ProducesResponseType(typeof(ThexListDto<IdentityProductDto>), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> GetAll()
        {
            var response = await _identityProductAppService.GetAllAsync();

            return CreateResponseOnGetAll(response, EntityNames.IdentityProduct);
        }
    }
}