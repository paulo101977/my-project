﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Primitives;
using System;
using System.Threading.Tasks;
using Thex.Kernel;
using Thex.SuperAdmin.Application.Interfaces;
using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Dto.Dto;
using Thex.SuperAdmin.Infra;
using Tnf.AspNetCore.Mvc.Response;
using Tnf.Dto;
using ThexSecurity = Thex.AspNetCore.Security;

namespace Thex.SuperAdmin.Web.Controllers
{
    [Route(RouteConsts.UserTotvs)]
    [Authorize]
    public class UserTotvsController : TnfController
    {
        private readonly IUserAppService _userAppService;
        private readonly IApplicationUser _applicationUser;
        private readonly IUserTotvsAppService _userTotvsAppService;

        public UserTotvsController(
            IUserAppService userAppService,
            IUserTotvsAppService userTotvsAppService,
            IApplicationUser applicationUser)
        {
            _userAppService = userAppService;
            _applicationUser = applicationUser;
            _userTotvsAppService = userTotvsAppService;
        }

        [AllowAnonymous]
        [HttpPost("login")]
        [ProducesResponseType(typeof(ThexSecurity.UserTokenDto), 201)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult Login([FromBody] LoginDto login)
        {
            ThexSecurity.UserTokenDto token = _userAppService.LoginTotvs(login);

            if (!Notification.HasNotification() && token == null)
                return Forbid();

            return CreateResponseOnPost(token, "Usertoken");
        }

        [AllowAnonymous]
        [HttpPost("forgotpassword/{email}")]
        [ProducesResponseType(201)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult ForgotPassword(string email)
        {
            _userAppService.ForgotPasswordTotvs(email);
            return CreateResponseOnPost(null, EntityNames.User);
        }

        [HttpGet]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> GetAll([FromQuery] GetAllUserDto request)
        {
            var response = await _userTotvsAppService.GetAllAsync(request);
            return CreateResponseOnGetAll(response, EntityNames.User);
        }

        [HttpPost]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Post([FromBody] CreateOrUpdateUserDto dto)
        {
            var response = await _userTotvsAppService.CreateAsync(dto);
            return CreateResponseOnPost(response, EntityNames.User);
        }

        [HttpPut("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Put([FromBody] CreateOrUpdateUserDto dto, Guid id)
        {
            var response = await _userTotvsAppService.UpdateAsync(dto, id);
            return CreateResponseOnPut(response, EntityNames.User);
        }

        [HttpGet("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Get(Guid id)
        {
            var response = await _userTotvsAppService.GetAsync(id);
            return CreateResponseOnGet(response, EntityNames.User);
        }

        [HttpPatch("{id}/toggleisactive")]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> ToggleIsActive(Guid id)
        {
            await _userTotvsAppService.ToggleIsActiveAsync(id);
            return CreateResponseOnPatch(null, EntityNames.User);
        }
    }
}
