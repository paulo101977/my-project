﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Thex.SuperAdmin.Application.Interfaces;
using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Dto.Dto;
using Thex.SuperAdmin.Infra;
using Tnf.AspNetCore.Mvc.Response;

namespace Thex.SuperAdmin.Web.Controllers
{
    [Route(RouteConsts.Brand)]
    [Authorize]
    public class BrandController : TnfController
    {
        private IBrandAppService _brandAppService;

        public BrandController(IBrandAppService brandAppService)
        {
            _brandAppService = brandAppService;
        }

        [HttpGet]
        [ProducesResponseType(typeof(ThexListDto<BrandDto>), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> GetAll([FromQuery] GetAllBrandDto requestDto)
        {
            var response = await _brandAppService.GetAllByFilterAsync(requestDto);

            return CreateResponseOnGetAll(response, EntityNames.Brand);
        }

        [HttpGet("{id}")]
        [ProducesResponseType(typeof(BrandDto), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Get(int id)
        {
            var response = await _brandAppService.GetAsync(id);

            return CreateResponseOnGet(response, EntityNames.Brand);
        }

        [HttpPost]
        [ProducesResponseType(typeof(BrandDto), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Create([FromBody] BrandDto brandDto)
        {
            var response = await _brandAppService.CreateAsync(brandDto);

            return CreateResponseOnPost(response, EntityNames.Brand);
        }

        [HttpPut("{id}")]
        [ProducesResponseType(typeof(BrandDto), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Put([FromBody] BrandDto brandDto, int id)
        {
            var response = await _brandAppService.UpdateAsync(id, brandDto);

            return CreateResponseOnPut(response, EntityNames.Brand);
        }
    }
}