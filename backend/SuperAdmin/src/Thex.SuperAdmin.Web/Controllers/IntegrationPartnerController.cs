﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Thex.AspNetCore.Security;
using Thex.SuperAdmin.Application.Interfaces;
using Thex.SuperAdmin.Dto.Dto;
using Thex.SuperAdmin.Infra;
using Tnf.AspNetCore.Mvc.Response;

namespace Thex.SuperAdmin.Web.Controllers
{
    [Route(RouteConsts.IntegrationPartner)]
    public class IntegrationPartnerController : TnfController
    {
        private readonly IIntegrationPartnerAppService _integrationPartnerAppService;

        public IntegrationPartnerController(IIntegrationPartnerAppService integrationPartnerAppService)
        {
            _integrationPartnerAppService = integrationPartnerAppService;
        }

        [AllowAnonymous]
        [HttpPost("authenticate")]
        [ProducesResponseType(typeof(IntegrationPartnerTokenDto), 201)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Authenticate([FromBody] AuthenticateIntegrationPartnerDto authenticateIntegrationPartner)
        {
            var token = await _integrationPartnerAppService.AuthenticateAndGenerateToken(authenticateIntegrationPartner);

            return CreateResponseOnPost(token, EntityNames.IntegrationPartner);
        }

        [HttpGet("getallpartners")]
        [ProducesResponseType(typeof(PartnerDto), 201)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> GetAllPartners()
        {
            var response = await _integrationPartnerAppService.GetAllPartnersAsync();
            return CreateResponseOnGetAll(response, EntityNames.IntegrationPartner);
        }
    }
}