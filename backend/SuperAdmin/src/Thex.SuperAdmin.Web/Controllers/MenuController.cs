﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.Dto;
using Thex.Kernel;
using Thex.SuperAdmin.Application.Interfaces;
using Thex.SuperAdmin.Dto;
using Tnf.AspNetCore.Mvc.Response;
using Tnf.Dto;

namespace Thex.SuperAdmin.Web.Controllers
{
    [Route(RouteConsts.Menu)]
    [Authorize]
    public class MenuController : TnfController
    {
        private readonly IMenuAppService _menuAppService;

        public MenuController(IMenuAppService menuAppService)
        {
            _menuAppService = menuAppService;
        }

        [HttpGet]
        [ProducesResponseType(typeof(IListDto<IdentityMenuFeatureDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> GetHierarchy([FromQuery] GetAllIdentityMenuFeatureDto requestDto)
        {
            var response = await _menuAppService.GetHierarchyAsync(requestDto);

            return CreateResponseOnGetAll(response, RouteConsts.Menu);
        }

        [HttpGet("{id}")]
        [ProducesResponseType(typeof(IdentityMenuFeatureDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Get(Guid id, RequestDto requestDto)
        {
            var request = new DefaultGuidRequestDto(id, requestDto);

            var response = await _menuAppService.GetAsync(id);

            return CreateResponseOnGet(response, RouteConsts.Menu);
        }

        [HttpPut("{id}")]
        [ProducesResponseType(typeof(IdentityMenuFeatureDto), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Put(Guid id, [FromBody] IdentityMenuFeatureDto dto)
        {
            var response = await _menuAppService.UpdateAsync(id, dto);

            return CreateResponseOnPut(response, RouteConsts.Menu);
        }

        [HttpPost]
        [ProducesResponseType(typeof(IdentityMenuFeatureDto), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Post([FromBody] IdentityMenuFeatureDto dto)
        {
            var response = await _menuAppService.CreateAsync(dto);

            return CreateResponseOnPost(response, RouteConsts.Menu);
        }

        [HttpPatch("{id}/toggleactivation")]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult ToggleActivation(Guid id)
        {
            _menuAppService.ToggleActivation(id);

            return CreateResponseOnPatch(null, RouteConsts.Menu);
        }
    }
}
