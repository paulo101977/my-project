﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.Kernel;
using Thex.SuperAdmin.Application.Interfaces;
using Thex.SuperAdmin.Dto;
using Tnf.AspNetCore.Mvc.Response;
using Tnf.Dto;

namespace Thex.SuperAdmin.Web.Controllers
{
    [Route(RouteConsts.Chain)]
    [Authorize]
    public class ChainController : TnfController
    {
        private readonly IChainAppService _chainAppService;
        private readonly IApplicationUser _applicationUser;

        public ChainController(IChainAppService chainAppService, IApplicationUser applicationUser)
        {
            _chainAppService = chainAppService;
            _applicationUser = applicationUser;
        }

        [HttpGet]
        [ProducesResponseType(typeof(IListDto<ChainDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Get([FromQuery] GetAllChainDto requestDto)
        {
            var response = await _chainAppService.GetAll(requestDto);

            return CreateResponseOnGetAll(response, RouteConsts.Chain);
        }

        [HttpGet("{id}")]
        [ProducesResponseType(typeof(ChainDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Get(int id, RequestDto requestDto)
        {
            var request = new DefaultIntRequestDto(id, requestDto);

            var response = await _chainAppService.Get(request);

            return CreateResponseOnGet(response, RouteConsts.Chain);
        }

        [HttpPut("{id}")]
        [ProducesResponseType(typeof(ChainDto), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Put(int id, [FromBody] ChainDto dto)
        {
            var response = await _chainAppService.Update(id, dto);

            return CreateResponseOnPut(response, RouteConsts.Chain);
        }

        [HttpPost]
        [ProducesResponseType(typeof(ChainDto), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Post([FromBody] ChainDto dto)
        {
            var response = await _chainAppService.Create(dto);

            return CreateResponseOnPost(response, RouteConsts.Chain);
        }
    }
}
