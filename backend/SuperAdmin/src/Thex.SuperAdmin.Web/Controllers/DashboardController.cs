﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Thex.SuperAdmin.Application.Interfaces;
using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Dto.Dto;
using Thex.SuperAdmin.Infra;
using Tnf.AspNetCore.Mvc.Response;

namespace Thex.SuperAdmin.Web.Controllers
{
    [Route(RouteConsts.Dashboard)]
    [Authorize]
    public class DashboardController : TnfController
    {
        private readonly IDashboardAppService _dashboardAppService;

        public DashboardController(IDashboardAppService dashboardAppService)
        {
            _dashboardAppService = dashboardAppService;
        }

        [HttpGet]
        [ProducesResponseType(typeof(ThexListDto<DashboardPropertyDto>), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> GetAllProperties()
        {
            var response = await _dashboardAppService.GetAllPropertiesAsync();

            return CreateResponseOnGetAll(response, EntityNames.Property);
        }

        [HttpPatch("next/{id}")]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> NextPropertyStatus(int id)
        {
            await _dashboardAppService.NextPropertyStatusAsync(id);

            return CreateResponseOnPatch(null, EntityNames.Property);
        }

        [HttpPatch("{id}/toggleblockproperty")]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> ToggleBlockProperty(int id)
        {
            await _dashboardAppService.ToggleBlockPropertyAsync(id);

            return CreateResponseOnPatch(null, EntityNames.Property);
        }
    }
}