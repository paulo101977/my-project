﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Thex.SuperAdmin.Application.Interfaces;
using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Dto.Dto;
using Thex.SuperAdmin.Infra;
using Tnf.AspNetCore.Mvc.Response;
using Tnf.Dto;

namespace Thex.SuperAdmin.Web.Controllers
{
    [Route(RouteConsts.Tenant)]
    [Authorize]
    public class TenantController : TnfController
    {
        private readonly ITenantAppService _tenantAppService;

        public TenantController(ITenantAppService tenantAppService)
        {
            _tenantAppService = tenantAppService;
        }

        [HttpGet]
        [ProducesResponseType(typeof(ThexListDto<TenantDto>), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult GetAll([FromQuery] GetAllTenantDto requestDto)
        {
            var response = _tenantAppService.GetAllByFilter(requestDto);

            return CreateResponseOnGetAll(response, EntityNames.Tenant);
        }

        [HttpGet("{id}")]
        [ProducesResponseType(typeof(TenantDto), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult Get(Guid id)
        {
            var response = _tenantAppService.Get(id);

            return CreateResponseOnGet(response, EntityNames.Tenant);
        }

        [HttpGet("withoutassociation")]
        [ProducesResponseType(typeof(IListDto<TenantDto>), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult GetAllWithoutAssociation([FromQuery] Guid? tenantId)
        {
            var response = _tenantAppService.GetAllWithoutAssociation(tenantId);

            return CreateResponseOnGetAll(response, EntityNames.Tenant);
        }

        [HttpPost]
        [ProducesResponseType(typeof(TenantDto), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult Create([FromBody] TenantDto dto)
        {
            var response = _tenantAppService.Create(dto);

            return CreateResponseOnPost(response, EntityNames.Tenant);
        }

        [HttpPut("{id}")]
        [ProducesResponseType(typeof(TenantDto), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult Update([FromBody] TenantDto dto, Guid id)
        {
            var response = _tenantAppService.Update(dto, id);

            return CreateResponseOnPut(response, EntityNames.Tenant);
        }
    }
}