﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.Dto;
using Thex.Kernel;
using Thex.SuperAdmin.Application.Interfaces;
using Thex.SuperAdmin.Dto;
using Tnf.AspNetCore.Mvc.Response;
using Tnf.Dto;

namespace Thex.SuperAdmin.Web.Controllers
{
    [Route(RouteConsts.Permissions)]
    [Authorize]
    public class PermissionController : TnfController
    {
        private readonly IPermissionAppService _permissionAppService;
        private readonly IUserPermissionAppService _userPermissionAppService;

        public PermissionController(IPermissionAppService permissionAppService, IUserPermissionAppService userPermissionAppService)
        {
            _userPermissionAppService = userPermissionAppService;
            _permissionAppService = permissionAppService;
        }

        [HttpGet]
        [ProducesResponseType(typeof(IListDto<IdentityPermissionDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Get([FromQuery] GetAllIdentityPermissionDto requestDto)
        {
            var response = await _permissionAppService.GetAllByFilterAsync(requestDto);

            return CreateResponseOnGetAll(response, RouteConsts.Permissions);
        }

        [HttpGet("UserId")]
        [ThexAuthorize("SA_Get_By_UserId")]
        [ProducesResponseType(typeof(IListDto<IdentityPermissionDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> GetByUser([FromQuery] GetAllIdentityPermissionDto requestDto)
        {
            var response = await _permissionAppService.GetAllByUserIdAsync(requestDto, true);

            return CreateResponseOnGetAll(response, RouteConsts.Permissions);
        }

        [HttpGet("{id}")]
        [ProducesResponseType(typeof(IdentityPermissionDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Get(Guid id, RequestDto requestDto)
        {
            var request = new DefaultGuidRequestDto(id, requestDto);

            var response = await _permissionAppService.GetAsync(id);

            return CreateResponseOnGet(response, RouteConsts.Permissions);
        }

        [HttpPut("{id}")]
        [ProducesResponseType(typeof(IdentityPermissionDto), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Put(Guid id, [FromBody] IdentityPermissionDto dto)
        {
            var response = await _permissionAppService.UpdateAsync(id, dto);

            return CreateResponseOnPut(response, RouteConsts.Permissions);
        }

        [HttpPost]
        [ProducesResponseType(typeof(IdentityPermissionDto), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Post([FromBody] IdentityPermissionDto dto)
        {
            var response = await _permissionAppService.CreateAsync(dto);

            return CreateResponseOnPost(response, RouteConsts.Permissions);
        }

        [HttpDelete("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Delete(Guid id)
        {
            await _permissionAppService.DeleteAsync(id);

            return CreateResponseOnDelete(RouteConsts.Permissions);
        }

        [HttpPatch("{id}/toggleactivation")]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult ToggleActivation(Guid id)
        {
            _permissionAppService.ToggleActivation(id);

            return CreateResponseOnPatch(null, RouteConsts.Permissions);
        }

        [HttpPost("{userId}")]
        [ProducesResponseType(typeof(bool), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> VerifyPermission(Guid userId, [FromBody]List<string> permissions)
            => Ok(await _userPermissionAppService.VerifyUserPermission(userId, permissions));

        [HttpPost("externalAccess")]
        [ProducesResponseType(typeof(bool), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> VerifyExternalAccessPermission([FromBody]List<string> permissions)
            => Ok(await _userPermissionAppService.VerifyExternalUserPermission(permissions));
    }
}
