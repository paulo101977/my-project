﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Thex.SuperAdmin.Application.Interfaces;
using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Dto.Dto;
using Thex.SuperAdmin.Infra;
using Tnf.AspNetCore.Mvc.Response;

namespace Thex.SuperAdmin.Web.Controllers
{
    [Route(RouteConsts.IdentityModule)]
    [Authorize]
    public class IdentityModuleController : TnfController
    {
        private IIdentityModuleAppService _identityModuleAppService;

        public IdentityModuleController(IIdentityModuleAppService identityModuleAppService)
        {
            _identityModuleAppService = identityModuleAppService;
        }

        [HttpGet]
        [ProducesResponseType(typeof(ThexListDto<IdentityModuleDto>), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> GetAll()
        {
            var response = await _identityModuleAppService.GetAllAsync();

            return CreateResponseOnGetAll(response, EntityNames.IdentityModule);
        }
    }
}