﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.Dto;
using Thex.Kernel;
using Thex.SuperAdmin.Application.Interfaces;
using Thex.SuperAdmin.Dto;
using Tnf.AspNetCore.Mvc.Response;
using Tnf.Dto;

namespace Thex.SuperAdmin.Web.Controllers
{
    [Route(RouteConsts.UserPermissions)]
    [Authorize]
    public class UserPermissionController : TnfController
    {
        private readonly IUserPermissionAppService _userPermissionAppService;

        public UserPermissionController(IUserPermissionAppService userPermissionAppService)
        {
            _userPermissionAppService = userPermissionAppService;
        }

        [HttpGet("productId/{productId}/menu/{withMenu}")]
        [ProducesResponseType(typeof(UserPermissionWithMenuDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Get(int productId, bool withMenu)
        {
            var response = await _userPermissionAppService.GetAllWithMenu(productId, withMenu);

            return CreateResponseOnGet(response, RouteConsts.UserPermissions);
        }

        [HttpGet("v2/propertyId/{propertyId}")]
        [ProducesResponseType(typeof(UserProductPermissionWithMenuDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> GetPermissionByProperty(int propertyId)
        {
            var response = await _userPermissionAppService.GetPermissionByProperty(propertyId);

            return CreateResponseOnGet(response, RouteConsts.UserPermissions);
        }
    }
}
