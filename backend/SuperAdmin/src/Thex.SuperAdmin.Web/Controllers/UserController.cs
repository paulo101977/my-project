﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Primitives;
using System;
using System.Threading.Tasks;
using Thex.AspNetCore.Security.Interfaces;
using Thex.Kernel;
using Thex.SuperAdmin.Application.Interfaces;
using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Infra;
using Tnf.AspNetCore.Mvc.Response;
using Tnf.Dto;
using ThexSecurity = Thex.AspNetCore.Security;

namespace Thex.SuperAdmin.Web.Controllers
{
    [Route(RouteConsts.User)]
    [Authorize]
    public class UserController : TnfController
    {
        private readonly IUserAppService _userAppService;
        private readonly IApplicationUser _applicationUser;
        private readonly IUserRoleAppService _userRoleAppService;
        private readonly IDistributedCache _cache;
        private readonly ITokenConfiguration _tokenConfigurations;

        public UserController(
            IUserAppService userAppService,
            IApplicationUser applicationUser,
            IUserRoleAppService userRoleAppService,
            IDistributedCache cache,
            ITokenConfiguration tokenConfigurations)
        {
            _userAppService = userAppService;
            _applicationUser = applicationUser;
            _userRoleAppService = userRoleAppService;
            _cache = cache;
            _tokenConfigurations = tokenConfigurations;
        }

        [HttpGet]
        [ThexAuthorize("SA_User_Get")]
        [ProducesResponseType(typeof(IListDto<UserDto>), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Get([FromQuery] GetAllUserDto requestDto)
        {
            var response = await _userAppService.GetAllByLoggedTenantIdAsync(requestDto);

            return CreateResponseOnGetAll(response, EntityNames.User);
        }

        [HttpGet("currentUser")]
        [ProducesResponseType(typeof(UserDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Get()
        {
            var response = await _userAppService.GetByLoggedUserIdAsync();

            return CreateResponseOnGet(response, EntityNames.User);
        }

        [AllowAnonymous]
        [HttpPost("login")]
        [ProducesResponseType(typeof(ThexSecurity.UserTokenDto), 201)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        [ProducesResponseType(401)]
        public IActionResult Login([FromBody] LoginDto login)
        {
            ThexSecurity.UserTokenDto token = _userAppService.Login(login);

            if (!Notification.HasNotification() && token == null)
                return Unauthorized();

            return CreateResponseOnPost(token, "Usertoken");
        }

        [HttpGet("property/{propertyId}/token")]
        [ProducesResponseType(typeof(ThexSecurity.UserTokenDto), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        [ProducesResponseType(403)]
        [ProducesResponseType(401)]
        public IActionResult GetTokenByPropertyId(int propertyId)
        {
            ThexSecurity.UserTokenDto token = _userAppService.LoginByPropertyId(propertyId);

            if (token == null)
                return Unauthorized();

            return CreateResponseOnGet(token, "Usertoken");
        }

        [AllowAnonymous]
        [HttpPost("forgotpassword/{email}")]
        [ProducesResponseType(201)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult ForgotPassword(string email)
        {
            _userAppService.ForgotPassword(email);
            return CreateResponseOnPost(null, EntityNames.User);
        }

        [HttpPatch("userId/{userId}/propertyid/{propertyid}/toggleactivation")]
        [ThexAuthorize("SA_User_Patch_ToggleActivation")]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult ToggleActivation(Guid userId, int propertyId)
        {
            _userAppService.ToggleActivationByLoggedPropertyId(userId, propertyId);

            return CreateResponseOnPatch(null, EntityNames.User);
        }

        [HttpPatch("{id}/{culture}/culture")]
        [ThexAuthorize("SA_User_Patch_Culture")]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult ChangeCulture(Guid id, string culture)
        {
            _userAppService.ChangeCulture(id, culture);

            return CreateResponseOnPatch(null, EntityNames.User);
        }

        [HttpPost("newpassword")]
        [ProducesResponseType(201)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> NewPassword([FromBody] PwdDto pwd)
        {
            var token = HttpContext.GetBearerToken();
            await _userAppService.NewPassword(token, pwd.Password);

            return CreateResponseOnPost(null, EntityNames.User);
        }

        [HttpPatch("changepassword")]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> ChangePassword([FromBody] ChangePwdDto pwd)
        {
            var token = HttpContext.GetBearerToken();
            await _userAppService.ChangePassword(token, pwd.OldPassword, pwd.NewPassword);

            return CreateResponseOnPatch(null, EntityNames.User);
        }

        [HttpPost("createnewuser")]
        [ThexAuthorize("SA_User_Post_CreateNewUser")]
        [ProducesResponseType(typeof(UserDto), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> CreateNewUser([FromBody] UserInvitationDto dto)
        {
            var response = await _userAppService.CreateNewUser(dto, dto.RoleIdList);
            return CreateResponseOnPost(response, EntityNames.User);
        }


        [HttpPatch("UpdateUser")]
        [ThexAuthorize("SA_User_Patch_UpdateUser")]
        [ProducesResponseType(typeof(UserDto), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> UpdateUser([FromBody] UpdateUserDto dto)
        {
            await _userAppService.UpdateUserAsync(dto);
            return CreateResponseOnPatch(null, EntityNames.User);
        }


        [HttpPost("newuserpassword")]
        [ProducesResponseType(typeof(UserDto), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> NewUserPassword([FromBody] PwdDto pwd)
        {
            var token = HttpContext.GetBearerToken();

            var response = await _userAppService.CreateNewUserPassword(token, pwd.Password);

            return CreateResponseOnPost(response, EntityNames.User);
        }

        [HttpPatch("{id}/newInvitationLink")]
        [ThexAuthorize("SA_User_Patch_NewInvitationLink")]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> NewInvitationLink(Guid id)
        {
            var response = await _userAppService.NewInvitationLinkAsync(id);

            return CreateResponseOnPatch(response, EntityNames.User);
        }

        [HttpPut("changephoto")]
        [ThexAuthorize("SA_User_Change_Photo")]
        [ProducesResponseType(typeof(UserDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> SendPhoto(Guid id, [FromBody] UserDto userDto)
        {
            var response = await _userAppService.ChangePhoto(_applicationUser.UserUid, userDto);

            return CreateResponseOnPut(response);
        }


        [HttpPatch("person/changepassword")]
        [ThexAuthorize("SA_User_Patch_PersonChangePwd")]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> ChangePasswordPerson([FromBody] ChangePwdPersonDto dto)
        {
            await _userAppService.ChangePasswordPersonAsync(dto);

            return CreateResponseOnPatch(null, EntityNames.User);
        }
    }
}
