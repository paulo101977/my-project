﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.ResponseCompression;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Thex.Common;
using Thex.Kernel;
using Thex.Location.Application;
using Thex.Maps.Geocode;
using Thex.SuperAdmin.Infra;
using Thex.SuperAdmin.Infra.Context;
using Thex.SuperAdmin.Infra.Entities;
using Thex.SuperAdmin.Infra.Interfaces;
using Thex.SuperAdmin.Infra.Uow;
using Tnf.Configuration;

namespace Thex.SuperAdmin.Web
{
    public class Startup
    {
        private IConfiguration Configuration { get; set; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            services
                .AddCorsAll("AllowAll")
                .AddApplicationServiceDependency()
                .AddApplicationServiceLocationDependency(options =>
                {
                    var settingsSection = Configuration.GetSection("MapsGeocodeConfig");
                    var settings = settingsSection.Get<MapsGeocodeConfig>();
                }, Configuration)
                .AddTnfAspNetCore()
                .AddRefreshTokenCacheConfig(Configuration)
                .AddPermissionsCacheConfig(Configuration)
                .AddResponseCompression(options =>
                {
                    options.Providers.Add<GzipCompressionProvider>();
                    options.EnableForHttps = true;
                });

            services.AddScoped<ISimpleUnitOfWork, SimpleUnitOfWork>();

            var connectionString = Configuration.GetSection("ConnectionStrings").GetValue<string>("SqlServer");
            services.AddDbContext<ThexIdentityDbContext>(options => options.UseSqlServer(connectionString, db => db.UseRowNumberForPaging()));

            services
                .AddIdentity<User, Roles>(options => { })
                .AddEntityFrameworkStores<ThexIdentityDbContext>();

            services.Configure<IdentityOptions>(options =>
            {
                options.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes(1);
                options.Lockout.MaxFailedAccessAttempts = 5;
                options.Lockout.AllowedForNewUsers = true;
            });

            services
                .AddThexAspNetCoreSecurity(options =>
                {
                    var settingsSection = Configuration.GetSection("TokenConfiguration");
                    var settings = settingsSection.Get<AspNetCore.Security.TokenConfiguration>();

                    options.TokenConfiguration = settings;
                    options.FrontEndpoint = Configuration.GetValue<string>("UrlFront");
                    options.SuperAdminEndpoint = Configuration.GetValue<string>("SuperAdminEndpoint");

                });

            services.AddScoped<Storage.IAzureStorage, Storage.AzureStorage>();

            services.AddMemoryCache();

            services.AddSwaggerDocumentation();

            services.AddAntiforgery(options =>
            {
                options.Cookie.Name = "X-CSRF-TOKEN-GOTNEXT-COOKIE";
                options.HeaderName = "X-CSRF-TOKEN-GOTNEXT-HEADER";
                options.SuppressXFrameOptionsHeader = false;
            })
            .AddMvc()
            .AddJsonOptions(opts =>
            {
                opts.SerializerSettings.NullValueHandling =
                    Newtonsoft.Json.NullValueHandling.Ignore;
            });

            var serviceProvider = services.BuildServiceProvider();

            ServiceLocator.SetLocatorProvider(serviceProvider);

            return serviceProvider;
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILogger<Startup> logger)
        {
            app.UseCors("AllowAll");

            // Configura o use do AspNetCore do Tnf
            app.UseTnfAspNetCore(options =>
            {
                options.UseDomainLocalization();
                options.DefaultPageSize(1000, 1000);
            });

            if (env.IsDevelopment())
                app.UseDeveloperExceptionPage();

            app.UseSwaggerDocumentation();
            app.UseThexAspNetCoreSecurity();
            app.UseMvcWithDefaultRoute();
            app.UseResponseCompression();

            app.Run(context =>
            {
                context.Response.Redirect("/swagger");
                return Task.CompletedTask;
            });

            logger.LogInformation("Start application ...");
        }

    }
}
