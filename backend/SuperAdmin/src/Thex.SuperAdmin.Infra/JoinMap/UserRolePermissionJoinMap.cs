﻿using Thex.SuperAdmin.Infra.Entities;

namespace Thex.SuperAdmin.Infra.JoinMap
{
    public class UserRolePermissionJoinMap
    {
        public User User { get; set; }
        public UserRole UserRole { get; set; }
        public Roles Role { get; set; }
        public IdentityRolePermission IdentityRolePermission { get; set; }
        public IdentityPermission IdentityPermission { get; set; }
        public IdentityPermissionFeature IdentityPermissionFeature { get; set; }
        public IdentityFeature IdentityFeature { get; set; }
        public IdentityProductProperty IdentityProductProperty { get; set; }
        public IdentityProduct IdentityProduct { get; set; }
        public IdentityMenuFeature IdentityMenuFeature { get; set; }

    }
}
