﻿using Thex.SuperAdmin.Infra.Entities;

namespace Thex.SuperAdmin.Infra.JoinMap
{
    public class TenantPropertyParameterJoinMap
    {
        public Tenant Tenant { get; set; }
        public PropertyParameter PropertyParameter { get; set; }
    }
}
