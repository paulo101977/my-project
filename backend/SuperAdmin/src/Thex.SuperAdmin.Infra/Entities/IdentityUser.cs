﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using Tnf.Notifications;

namespace Thex.SuperAdmin.Infra.Entities
{
    public partial class User : ThexIdentityUserEntity
    {
        public Guid? PersonId { get; set; }
        public string PreferredLanguage { get; set; }
        public string PreferredCulture { get; set; }
        public string Name { get; set; }
        public bool IsActive { get; set; }
        public bool? IsAdmin { get; set; }
        public Guid? TenantId { get; set; }

        public virtual ICollection<UserTenant> UserTenantList { get; internal set; }

        public User()
        {
            UserTenantList = new HashSet<UserTenant>();
        }

        public enum EntityError
        {
            UserMustHavePersonId,
            UserMustHaveTenantId,
            UserMustHaveUserLogin,
            UserOutOfBoundUserLogin,
            UserMustHavePreferredLanguage,
            UserOutOfBoundPreferredLanguage,
            UserMustHavePreferredCulture,
            UserOutOfBoundPreferredCulture,
            UserMustHaveName,
            UserOutOfBoundName,
            UserOutOfBoundEmail,
            UserNotFound,
            UserMustHaveAssociationBrand,
            UserMustNotHaveBrandDuplicateAssociation,
            UserMustNotHavePropertyDuplicateAssociation,
            UserMustHaveBrandPermissionForAssociateProperty,
            UserMustHaveChainPermissionForAssociateBrand,
            UserAlreadyAssociatedToAnotherChain,
            UserUpdateError
        }

        public static Builder Create(INotificationHandler handler)
            => new Builder(handler);

        public static Builder Create(INotificationHandler handler, User instance)
            => new Builder(handler, instance);

        public void Unlock()
        {
            LockoutEnd = null;
            AccessFailedCount = 0;
        }
    }
}
