﻿using System;

namespace Thex.SuperAdmin.Infra.Entities
{
    public class PropertyMealPlanType : ThexMultiTenantFullAuditedEntity
    {
        public Guid Id { get; set; }
        public string PropertyMealPlanTypeCode { get; internal set; }
        public string PropertyMealPlanTypeName { get; internal set; }
        public int PropertyId { get; internal set; }
        public int MealPlanTypeId { get; internal set; }
        public bool? IsActive { get; internal set; }

        public virtual Property Property { get; internal set; }
    }
}
