﻿
namespace Thex.SuperAdmin.Infra
{
	public partial class EntityNames
    {
        public const string Health = "Health check";
        public const string ApplicationModule = "ApplicationModule";
		public const string ApplicationParameter = "ApplicationParameter";
		public const string Brand = "Brand";
		public const string Chain = "Chain";
		public const string Company = "Company";
		public const string ContactInformation = "ContactInformation";
		public const string ContactInformationType = "ContactInformationType";
		public const string CountryLanguage = "CountryLanguage";
		public const string CountrySubdivision = "CountrySubdivision";
		public const string CountrySubdivisionTranslation = "CountrySubdivisionTranslation";
		public const string Document = "Document";
		public const string DocumentType = "DocumentType";
		public const string FeatureGroup = "FeatureGroup";
		public const string Location = "Location";
		public const string LocationCategory = "LocationCategory";
		public const string ParameterType = "ParameterType";
		public const string Person = "Person";
		public const string Property = "Property";
		public const string PropertyParameter = "PropertyParameter";
		public const string Room = "Room";
		public const string Status = "Status";
		public const string StatusCategory = "StatusCategory";
		public const string Tenant = "Tenant";
        public const string IdentityFeature = "IdentityFeature";
        public const string IdentityMenuFeature = "IdentityMenuFeature";
        public const string IdentityModule = "IdentityModule";
        public const string IdentityPermission = "IdentityPermission";
        public const string IdentityPermissionFeature = "IdentityPermissionFeature";
        public const string IdentityProduct = "IdentityProduct";
        public const string IdentityRolePermission = "IdentityRolePermission";
        public const string User = "User";
        public const string IntegrationPartner = "IngrationPartner";
    }
}
