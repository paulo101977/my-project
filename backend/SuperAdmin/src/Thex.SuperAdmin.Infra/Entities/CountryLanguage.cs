﻿using System;
using System.Collections.Generic;
using Tnf.Notifications;

namespace Thex.SuperAdmin.Infra.Entities
{
    public partial class CountryLanguage : IEntityInt
    {
        public int Id { get; set; }
        public int CountrySubdivisionId { get; internal set; }
        public string Language { get; internal set; }
        public string LanguageTwoLetterIsoCode { get; internal set; }
        public string LanguageThreeLetterIsoCode { get; internal set; }
        public string CultureInfoCode { get; internal set; }

        public virtual CountrySubdivision CountrySubdivision { get; internal set; }

        public enum EntityError
        {
            CountryLanguageMustHaveCountrySubdivisionId,
            CountryLanguageMustHaveLanguage,
            CountryLanguageOutOfBoundLanguage,
            CountryLanguageOutOfBoundLanguageTwoLetterIsoCode,
            CountryLanguageOutOfBoundLanguageThreeLetterIsoCode,
            CountryLanguageOutOfBoundCultureInfoCode
        }

        public static Builder Create(INotificationHandler handler)
            => new Builder(handler);

        public static Builder Create(INotificationHandler handler, CountryLanguage instance)
            => new Builder(handler, instance);
    }
}
