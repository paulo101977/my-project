﻿using System;
using System.Collections.Generic;
using Tnf.Notifications;

namespace Thex.SuperAdmin.Infra.Entities
{
    public partial class CountrySubdivision : IEntityInt
    {
        public CountrySubdivision()
        {
            CountrySubdivisionTranslationList = new HashSet<CountrySubdivisionTranslation>();
            CountryLanguageList = new HashSet<CountryLanguage>();
            InverseParentSubdivisionList = new HashSet<CountrySubdivision>();
            LocationList = new HashSet<Location>();
            LocationStateList = new HashSet<Location>();
            LocationCountryList = new HashSet<Location>();
            PersonList = new HashSet<Person>();
        }

        public int Id { get; set; }
        public string TwoLetterIsoCode { get; internal set; }
        public string ThreeLetterIsoCode { get; internal set; }
        public string CountryTwoLetterIsoCode { get; internal set; }
        public string CountryThreeLetterIsoCode { get; internal set; }
        public string ExternalCode { get; internal set; }
        public int? ParentSubdivisionId { get; internal set; }
        public int SubdivisionTypeId { get; internal set; }

        public virtual CountrySubdivision ParentSubdivision { get; internal set; }
        public virtual ICollection<CountryLanguage> CountryLanguageList { get; internal set; }
        public virtual ICollection<CountrySubdivisionTranslation> CountrySubdivisionTranslationList { get; internal set; }
        public virtual ICollection<CountrySubdivision> InverseParentSubdivisionList { get; internal set; }
        public virtual ICollection<Location> LocationList { get; internal set; }
        public virtual ICollection<Location> LocationStateList { get; internal set; }
        public virtual ICollection<Location> LocationCountryList { get; internal set; }
        public virtual ICollection<Person> PersonList { get; internal set; }

        public enum EntityError
        {
            CountrySubdivisionOutOfBoundTwoLetterIsoCode,
            CountrySubdivisionOutOfBoundThreeLetterIsoCode,
            CountrySubdivisionOutOfBoundCountryTwoLetterIsoCode,
            CountrySubdivisionOutOfBoundCountryThreeLetterIsoCode,
            CountrySubdivisionOutOfBoundExternalCode,
            CountrySubdivisionOutOfBoundOfficialNatural,
            CountrySubdivisionOutOfBoundOfficialLegal
        }

        public static Builder Create(INotificationHandler handler)
            => new Builder(handler);

        public static Builder Create(INotificationHandler handler, CountrySubdivision instance)
            => new Builder(handler, instance);
    }
}
