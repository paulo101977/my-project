﻿using System;
using Tnf.Notifications;

namespace Thex.SuperAdmin.Infra.Entities
{
    public partial class UserTenant : ThexFullAuditedEntity, IEntityGuid
    {
        public Guid Id {get; set;}
        public Guid UserId { get; internal set; }
        public Guid TenantId { get; internal set; }
        public bool IsActive { get; internal set; }

        public virtual User User { get; set; }
        public virtual Tenant Tenant { get; set; }

        public enum EntityError
        {
            UserTenantMustHaveUserId,
            UserTenantMustHaveTenantId
        }

        public static Builder Create(INotificationHandler handler)
            => new Builder(handler);

        public static Builder Create(INotificationHandler handler, UserTenant instance)
            => new Builder(handler, instance);
    }
}
