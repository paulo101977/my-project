﻿using System;
using System.Collections.Generic;
using Tnf.Notifications;

namespace Thex.SuperAdmin.Infra.Entities
{
    public partial class CountrySubdivisionTranslation : IEntityInt
    {
        public int Id { get; set; }
        public string LanguageIsoCode { get; internal set; }
        public string Name { get; internal set; }
        public int CountrySubdivisionId { get; internal set; }
        public string Nationality { get; internal set; }

        public virtual CountrySubdivision CountrySubdivision { get; internal set; }

        public enum EntityError
        {
            CountrySubdivisionTranslationMustHaveLanguageIsoCode,
            CountrySubdivisionTranslationOutOfBoundLanguageIsoCode,
            CountrySubdivisionTranslationMustHaveName,
            CountrySubdivisionTranslationOutOfBoundName,
            CountrySubdivisionTranslationMustHaveCountrySubdivisionId,
            CountrySubdivisionTranslationOutOfBoundNationality,
            CountrySubdivisionTranslationMustHaveCountryCode,
            CountrySubdivisionTranslationInvalidLanguage,
            CountrySubdivisionTranslationGeocodeApiReturnZeroResults,
            CountrySubdivisionTranslationGeocodeApiReturnError,
            CountrySubdivisionTranslationCountryCodeNotEqualGeocodeCountryShortName,
            CountrySubdivisionTranslationStateNameNotEqualGeocodeStateShortName
        }

        public static Builder Create(INotificationHandler handler)
            => new Builder(handler);

        public static Builder Create(INotificationHandler handler, CountrySubdivisionTranslation instance)
            => new Builder(handler, instance);
    }
}
