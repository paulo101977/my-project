﻿using Microsoft.AspNetCore.Identity;
using System;
using Tnf.Notifications;

namespace Thex.SuperAdmin.Infra.Entities
{
    public partial class UserLogin : ThexIdentityUserLoginEntity
    {
        public enum EntityError
        {
            UserLoginsMustHaveUserId
        }

        public static Builder Create(INotificationHandler handler)
            => new Builder(handler);

        public static Builder Create(INotificationHandler handler, UserLogin instance)
            => new Builder(handler, instance);
    }

}
