﻿using System;

namespace Thex.SuperAdmin.Infra.Entities
{
    public class PlasticBrandProperty : ThexMultiTenantFullAuditedEntity, IEntityGuid
    {
        public Guid Id { get; set; }
        public int PlasticBrandId { get; internal set; }
        public int PropertyId { get; internal set; }
        public short InstallmentsQuantity { get; internal set; }
        public bool IsActive { get; internal set; }

        public virtual PlasticBrand PlasticBrand { get; internal set; }
        public virtual Property Property { get; internal set; }
        
    }
}
