﻿using System;

namespace Thex.SuperAdmin.Infra.Entities
{
    public class PropertyStatusHistory : ThexFullAuditedEntity
    {
        public Guid Id { get; set; }
        public int PropertyId { get; internal set; }
        public int StatusId { get; internal set; }

        public virtual Property Property { get; internal set; }
        public virtual Status Status { get; internal set; }
    }
}
