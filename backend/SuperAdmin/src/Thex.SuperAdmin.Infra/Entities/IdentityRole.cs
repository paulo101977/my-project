﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using Tnf.Notifications;

namespace Thex.SuperAdmin.Infra.Entities
{
    public partial class Roles : ThexIdentityRoleEntity
    {
        public Roles()
        {
            IdentityRolePermissionList = new HashSet<IdentityRolePermission>();
        }

        public virtual bool IsActive { get; set; }
        public int IdentityProductId { get; internal set; }

        public virtual ICollection<IdentityRolePermission> IdentityRolePermissionList { get; internal set; }
        public virtual IdentityProduct IdentityProduct { get; internal set; }

        public enum EntityError
        {
            RolesOutOfBoundName,
            RolesOutOfBoundNormalizedName
        }

        public static Builder Create(INotificationHandler handler)
            => new Builder(handler);

        public static Builder Create(INotificationHandler handler, Roles instance)
            => new Builder(handler, instance);
    }
}
