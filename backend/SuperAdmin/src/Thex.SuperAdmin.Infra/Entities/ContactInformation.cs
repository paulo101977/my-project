﻿using System;
using System.Collections.Generic;
using Tnf.Notifications;

namespace Thex.SuperAdmin.Infra.Entities
{
    public partial class ContactInformation : IEntityInt
    {
        public int Id { get; set; }
        public Guid OwnerId { get; internal set; }
        public int ContactInformationTypeId { get; internal set; }
        public string Information { get; internal set; }

        public virtual ContactInformationType ContactInformationType { get; internal set; }
        public virtual Person Owner { get; internal set; }

        public enum EntityError
        {
            ContactInformationMustHaveOwnerId,
            ContactInformationMustHaveContactInformationTypeId,
            ContactInformationMustHaveInformation,
            ContactInformationOutOfBoundInformation
        }

        public static Builder Create(INotificationHandler handler)
            => new Builder(handler);

        public static Builder Create(INotificationHandler handler, ContactInformation instance)
            => new Builder(handler, instance);
    }
}
