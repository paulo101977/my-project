﻿using System;

namespace Thex.SuperAdmin.Infra.Entities
{
    public partial class Room : ThexMultiTenantFullAuditedEntity, IEntityInt
    {
        public int Id { get; set; }
        public int? ParentRoomId { get; internal set; }
        public int PropertyId { get; internal set; }
        public int RoomTypeId { get; internal set; }
        public string Building { get; internal set; }
        public string Wing { get; internal set; }
        public string Floor { get; internal set; }
        public string RoomNumber { get; internal set; }
        public string Remarks { get; internal set; }
        public bool IsActive { get; internal set; }
        public Guid HousekeepingStatusPropertyId { get; internal set; }


        public enum EntityError
        {
            RoomMustHavePropertyId,
            RoomMustHaveRoomTypeId,
            RoomOutOfBoundBuilding,
            RoomOutOfBoundWing,
            RoomOutOfBoundFloor,
            RoomMustHaveRoomNumber,
            RoomOutOfBoundRoomNumber,
            RoomOutOfBoundRemarks,
            RoomTypeHasReserveForward
        }
    }
}
