﻿using System;
using System.Collections.Generic;
using Thex.Common.Enumerations;
using Tnf.Notifications;

namespace Thex.SuperAdmin.Infra.Entities
{
    public partial class UserInvitation : ThexMultiTenantFullAuditedEntity, IEntityGuid
    {
        public Guid Id { get; set; }
        public int? ChainId { get; internal set; }
        public int? PropertyId { get; internal set; }
        public int? BrandId { get; internal set; }
        public string Email { get; internal set; }
        public string Name { get; internal set; }
        public string InvitationLink { get; internal set; }
        public DateTime InvitationDate { get; internal set; }
        public DateTime? InvitationAcceptanceDate { get; internal set; }
        public bool IsActive { get; internal set; }
        public Guid? UserId { get; internal set; }

        public virtual Chain Chain { get; internal set; }
        public virtual Property Property { get; internal set; }
        public virtual Brand Brand { get; internal set; }

        public enum EntityError
        {
            UserInvitationMustHaveEmail,
            UserInvitationOutOfBoundEmail,
            UserInvitationMustHaveName,
            UserInvitationOutOfBoundName,
            UserInvitationMustHaveInvitationLink,
            UserInvitationOutOfBoundInvitationLink,
            UserInvitationInvalidInvitationDate,
            UserInvitationInvalidInvitationAcceptanceDate
        }

        public static Builder Create(INotificationHandler handler)
            => new Builder(handler);

        public static Builder Create(INotificationHandler handler, UserInvitation instance)
            => new Builder(handler, instance);

        
        public void setNewLink(string newLink)
        {
            InvitationLink = newLink;
        }
    }
}
