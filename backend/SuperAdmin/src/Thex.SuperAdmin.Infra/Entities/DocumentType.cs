﻿using Tnf.Notifications;

namespace Thex.SuperAdmin.Infra.Entities
{
    public partial class DocumentType
    {
        public int Id { get; set; }
        public string Name { get; internal set; }

        public enum EntityError
        {
            DocumentTypeMustHavePersonType,
            DocumentTypeOutOfBoundPersonType,
            DocumentTypeMustHaveCountrySubdivisionId,
            DocumentTypeMustHaveName,
            DocumentTypeOutOfBoundName,
            DocumentTypeOutOfBoundStringFormatMask,
            DocumentTypeOutOfBoundRegexValidationExpression
        }
    }
}
