﻿using System;
using System.Collections.Generic;
using System.Text;
using Tnf.Notifications;

namespace Thex.SuperAdmin.Infra.Entities
{
    public partial class Document : IEntity
    {
        public int Id { get; set; }
        public Guid OwnerId { get; internal set; }
        public int DocumentTypeId { get; internal set; }
        public string DocumentInformation { get; internal set; }

        public virtual DocumentType DocumentType { get; internal set; }
        public virtual Person Owner { get; set; }

        public enum EntityError
        {
            DocumentMustHaveDocumentTypeId,
            DocumentMustHaveDocumentInformation,
            DocumentOutOfBoundDocumentInformation,
            DocumentInvalid,
            DocumentCpfInvalid,
            DocumentCnpjInvalid,
            DocumentListHasDuplicates
        }

        public static Builder Create(INotificationHandler handler)
            => new Builder(handler);

        public static Builder Create(INotificationHandler handler, Document instance)
            => new Builder(handler, instance);
    }
}
