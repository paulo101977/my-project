﻿using Microsoft.AspNetCore.Identity;
using System;
using Tnf.Notifications;

namespace Thex.SuperAdmin.Infra.Entities
{
    public partial class RoleClaim : ThexIdentityRoleClaimEntity
    {
        public enum EntityError
        {
            RoleClaimsMustHaveRoleId
        }

        public static Builder Create(INotificationHandler handler)
            => new Builder(handler);

        public static Builder Create(INotificationHandler handler, RoleClaim instance)
            => new Builder(handler, instance);
    }
}
