﻿using System;
using System.Collections.Generic;
using Tnf.Notifications;

namespace Thex.SuperAdmin.Infra.Entities
{
    public partial class Tenant : ThexFullAuditedEntity, IEntityGuid
    {
        public Tenant()
        {
            UserList = new HashSet<User>();
            ChildList = new HashSet<Tenant>();
            UserTenantList = new HashSet<UserTenant>();
        }

        public Guid Id { get; set; }
        public string TenantName { get; set; }
        public Guid? ParentId { get; set; }
        public bool IsActive { get; set; }

        public virtual Tenant Parent { get; set; }
        public virtual ICollection<Tenant> ChildList { get; set; }

        public virtual ICollection<User> UserList { get; internal set; }
        public virtual ICollection<UserTenant> UserTenantList { get; internal set; }

        public int? ChainId { get; internal set; }
        public int? PropertyId { get; internal set; }
        public int? BrandId { get; internal set; }

        public virtual Chain Chain { get; internal set; }
        public virtual Property Property { get; internal set; }
        public virtual Brand Brand { get; internal set; }

        public enum EntityError
        {
            TenantMustHaveTenantName,
            TenantOutOfBoundTenantName,
            TenantIsNotAvailable
        }

        public static Builder Create(INotificationHandler handler)
            => new Builder(handler);

        public static Builder Create(INotificationHandler handler, Tenant instance)
            => new Builder(handler, instance);
    }
}
