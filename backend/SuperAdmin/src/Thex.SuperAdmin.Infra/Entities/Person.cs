﻿using System;
using System.Collections.Generic;
using Thex.Common.Enumerations;
using Tnf.Notifications;
using Thex.Common.Extensions;

namespace Thex.SuperAdmin.Infra.Entities
{
    public partial class Person : IEntityGuid
    {
        internal Person(Guid id, string firstName, string lastName, string fullname, PersonTypeEnum personType) : this()
        {
            Id = id;
            FirstName = firstName;
            LastName = lastName;
            FullName = fullname;
            PersonTypeValue = personType;
        }

        internal Person(string firstName, string lastname, string fullname, PersonTypeEnum personType)
            : this(Guid.NewGuid(), firstName, lastname, fullname, personType)
        {
        }

        public Person()
        {
            CompanyList = new HashSet<Company>();
            ContactInformationList = new HashSet<ContactInformation>();
            DocumentList = new HashSet<Document>();
        }
        public Guid Id { get; set; }

        // public string PersonType { get; internal set; }
        public string FirstName { get; internal set; }
        public string LastName { get; internal set; }
        public string FullName { get; internal set; }
        public DateTime? DateOfBirth { get; internal set; }
        public int? CountrySubdivisionId { get; internal set; }
        public string PhotoUrl { get; internal set; }
        public string NickName { get; set; }

        public virtual Company Company { get; set; }
        public virtual ICollection<Company> CompanyList { get; internal set; }
        public virtual ICollection<ContactInformation> ContactInformationList { get; internal set; }
        public virtual ICollection<Document> DocumentList { get; internal set; }

        public enum EntityError
        {
            PersonMustHavePersonType,
            PersonOutOfBoundPersonType,
            PersonMustHaveFirstName,
            PersonOutOfBoundFirstName,
            PersonOutOfBoundLastName,
            PersonOutOfBoundFullName,
            PersonInvalidDateOfBirth
        }

        public static Builder Create(INotificationHandler handler)
            => new Builder(handler);

        public static Builder Create(INotificationHandler handler, Person instance)
            => new Builder(handler, instance);


        private PersonTypeEnum _personType;
        public PersonTypeEnum PersonTypeValue
        {
            get => _personType;
            internal set => _personType = value;
        }

        public string PersonType
        {
            get => ((char)_personType).ToString();
            internal set => _personType = EnumExtensions.ToEnum<PersonTypeEnum>(value);
        }
    }
}
