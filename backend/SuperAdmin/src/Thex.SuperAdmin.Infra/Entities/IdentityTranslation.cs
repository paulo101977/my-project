﻿using System;
using System.Collections.Generic;
using Tnf.Notifications;

namespace Thex.SuperAdmin.Infra.Entities
{
    public partial class IdentityTranslation : IEntityGuid
    {
        public Guid Id { get; set; }
        public string LanguageIsoCode { get; internal set; }
        public string Name { get; internal set; }
        public string Description { get; internal set; }
        public Guid IdentityOwnerId { get; internal set; }

        public enum EntityError
        {
            IdentityTranslationMustHaveLanguageIsoCode,
            IdentityTranslationOutOfBoundLanguageIsoCode,
            IdentityTranslationMustHaveIdentityOwnerId,
        }

        public static Builder Create(INotificationHandler handler)
            => new Builder(handler);

        public static Builder Create(INotificationHandler handler, IdentityTranslation instance)
            => new Builder(handler, instance);
    }
}
