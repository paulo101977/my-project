﻿using System;
using System.Collections.Generic;
using Tnf.Notifications;

namespace Thex.SuperAdmin.Infra.Entities
{
    public partial class ParameterType : IEntityInt
    {
        public ParameterType()
        {
            ApplicationParameterList = new HashSet<ApplicationParameter>();
        }

        public int Id { get; set; }
        public string ParameterTypeName { get; internal set; }

        public virtual ICollection<ApplicationParameter> ApplicationParameterList { get; internal set; }

        public enum EntityError
        {
            ParameterTypeMustHaveParameterTypeName,
            ParameterTypeOutOfBoundParameterTypeName
        }

        public static Builder Create(INotificationHandler handler)
            => new Builder(handler);

        public static Builder Create(INotificationHandler handler, ParameterType instance)
            => new Builder(handler, instance);
    }
}
