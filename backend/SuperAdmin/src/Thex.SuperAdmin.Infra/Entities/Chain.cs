﻿using System;
using System.Collections.Generic;
using Tnf.Notifications;

namespace Thex.SuperAdmin.Infra.Entities
{
    public partial class Chain : ThexMultiTenantFullAuditedEntity, IEntityInt
    {
        public Chain()
        {
            BrandList = new HashSet<Brand>();
            UserInvitationList = new HashSet<UserInvitation>();
        }
        public int Id { get; set; }
        public string Name { get; internal set; }
        public bool IsTemporary { get; internal set; }

        public virtual ICollection<Brand> BrandList { get; internal set; }
        public virtual ICollection<UserInvitation> UserInvitationList { get; internal set; }

        public enum EntityError
        {
            ChainMustHaveName,
            ChainOutOfBoundName
        }

        public static Builder Create(INotificationHandler handler)
            => new Builder(handler);

        public static Builder Create(INotificationHandler handler, Chain instance)
            => new Builder(handler, instance);
    }
}
