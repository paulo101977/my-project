﻿using Microsoft.AspNetCore.Identity;
using System;
using Tnf.Notifications;

namespace Thex.SuperAdmin.Infra.Entities
{
    public partial class UserToken : ThexIdentityUserTokenEntity
    {
        public enum EntityError
        {
            UserTokensMustHaveUserId,
            UserTokensMustHaveName,
            UserTokensOutOfBoundName
        }

        public static Builder Create(INotificationHandler handler)
            => new Builder(handler);

        public static Builder Create(INotificationHandler handler, UserToken instance)
            => new Builder(handler, instance);
    }
}
