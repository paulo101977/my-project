﻿using System;

namespace Thex.SuperAdmin.Infra.Entities
{
    public class HouseKeepingStatusProperty : ThexMultiTenantFullAuditedEntity
    {
        public Guid Id { get; set; }
        public int PropertyId { get; internal set; }
        public int HousekeepingStatusId { get; internal set; }
        public string StatusName { get; internal set; }
        public string Color { get; internal set; }

        public virtual Property Property { get; internal set; }
    }
}
