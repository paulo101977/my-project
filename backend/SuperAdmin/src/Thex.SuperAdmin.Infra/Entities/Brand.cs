﻿using System;
using System.Collections.Generic;
using Tnf.Notifications;

namespace Thex.SuperAdmin.Infra.Entities
{
    public partial class Brand : ThexMultiTenantFullAuditedEntity, IEntityInt
    {
        public Brand()
        {
            PropertyList = new HashSet<Property>();
            UserInvitationList = new HashSet<UserInvitation>();
        }
        public int Id { get; set; }
        public int ChainId { get; internal set; }
        public string Name { get; internal set; }
        public bool IsTemporary { get; internal set; }

        public virtual Chain Chain { get; internal set; }
        public virtual ICollection<Property> PropertyList { get; internal set; }
        public virtual ICollection<UserInvitation> UserInvitationList { get; internal set; }

        public enum EntityError
        {
            BrandMustHaveChainId,
            BrandMustHaveName,
            BrandOutOfBoundName,
            BrandMustHaveTenantId,
            AlreadyExistsBrandWithSameName
        }

        public static Builder Create(INotificationHandler handler)
            => new Builder(handler);

        public static Builder Create(INotificationHandler handler, Brand instance)
            => new Builder(handler, instance);
    }
}
