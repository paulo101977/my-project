﻿using System;
using System.Collections.Generic;
using Tnf.Notifications;

namespace Thex.SuperAdmin.Infra.Entities
{
    public partial class LocationCategory : IEntityInt
    {
        public LocationCategory()
        {
            LocationList = new HashSet<Location>();
        }

        public int Id { get; set; }
        public string Name { get; internal set; }
        public string RecordScope { get; internal set; }

        public virtual ICollection<Location> LocationList { get; internal set; }

        public enum EntityError
        {
            LocationCategoryMustHaveName,
            LocationCategoryOutOfBoundName,
            LocationCategoryMustHaveRecordScope,
            LocationCategoryOutOfBoundRecordScope
        }

        public static Builder Create(INotificationHandler handler)
            => new Builder(handler);

        public static Builder Create(INotificationHandler handler, LocationCategory instance)
            => new Builder(handler, instance);
    }
}
