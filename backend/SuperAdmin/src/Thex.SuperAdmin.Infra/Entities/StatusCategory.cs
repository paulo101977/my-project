﻿using System;
using System.Collections.Generic;
using Thex.Common.Enumerations;
using Tnf.Notifications;

namespace Thex.SuperAdmin.Infra.Entities
{
    public partial class StatusCategory : IEntityInt
    {
        public StatusCategory()
        {
            StatusList = new HashSet<Status>();
        }

        public int Id { get; set; }
        public string Name { get; internal set; }

        public virtual ICollection<Status> StatusList { get; internal set; }

        public enum EntityError
        {
            StatusCategoryMustHaveName,
            StatusCategoryOutOfBoundName
        }

        public static Builder Create(INotificationHandler handler)
            => new Builder(handler);

        public static Builder Create(INotificationHandler handler, StatusCategory instance)
            => new Builder(handler, instance);
    }
}
