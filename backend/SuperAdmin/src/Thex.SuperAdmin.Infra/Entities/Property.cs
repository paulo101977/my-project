﻿using System;
using System.Collections.Generic;
using Thex.Common.Enumerations;
using Tnf.Notifications;

namespace Thex.SuperAdmin.Infra.Entities
{
    public partial class Property : ThexMultiTenantFullAuditedEntity, IEntityInt
    {
        public Property()
        {
            UserInvitationList = new HashSet<UserInvitation>();
            PropertyStatusHistoryList = new HashSet<PropertyStatusHistory>();
            IntegrationPartnerPropertyList = new HashSet<IntegrationPartnerProperty>();
            IdentityProductPropertyList = new HashSet<IdentityProductProperty>();
        }

        public int Id { get; set; }
        public int CompanyId { get; internal set; }
        public int BrandId { get; internal set; }
        public string Name { get; internal set; }
        public Guid PropertyUId { get; internal set; }
        public int PropertyStatusId { get; internal set; }
        public bool? IsBlocked { get; internal set; }
        //public string Photo { get; internal set; }

        public virtual Status PropertyStatus { get; internal set; }
        public virtual Brand Brand { get; internal set; }
        public virtual Company Company { get; internal set; }
        public virtual PropertyType PropertyType { get; internal set; }
        public virtual ICollection<UserInvitation> UserInvitationList { get; internal set; }
        public virtual ICollection<PropertyStatusHistory> PropertyStatusHistoryList { get; internal set; }
        public virtual ICollection<PropertyContract> PropertyContractList { get; internal set; }
        public virtual ICollection<IntegrationPartnerProperty> IntegrationPartnerPropertyList { get; internal set; }
        public virtual ICollection<IdentityProductProperty> IdentityProductPropertyList { get; internal set; }

        public enum EntityError
        {
            PropertyMustHaveCompanyId,
            PropertyMustHavePropertyTypeId,
            PropertyMustHaveBrandId,
            PropertyMustHaveName,
            PropertyOutOfBoundName,
            PropertyInvalidCreationTime,
            PropertyInvalidLastModificationTime,
            PropertyInvalidDeletionTime,
            PropertyMustHaveTypeId,
            KeyForBlockPropertyInvalid,
        }

        public static Builder Create(INotificationHandler handler)
            => new Builder(handler);

        public static Builder Create(INotificationHandler handler, Property instance)
            => new Builder(handler, instance);


        private PropertyTypeEnum _propertyTypeId;
        public PropertyTypeEnum PropertyTypeIdValue
        {
            get
            {
                _propertyTypeId = ((PropertyTypeEnum)PropertyTypeIdValue);
                return _propertyTypeId;
            }
            internal set
            {
                _propertyTypeId = value;
                PropertyTypeId = ((int)value);
            }
        }

        public int PropertyTypeId
        {
            get;
            internal set;
        }
    }
}
