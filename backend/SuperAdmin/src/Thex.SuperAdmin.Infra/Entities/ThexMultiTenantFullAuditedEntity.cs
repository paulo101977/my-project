﻿using System;

namespace Thex.SuperAdmin.Infra.Entities
{
    [Serializable]
    public abstract class ThexMultiTenantFullAuditedEntity : ThexFullAuditedEntity
    {
        public Guid TenantId { get; set; }

        public Tenant Tenant { get; set; }
    }
}
