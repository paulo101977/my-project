﻿using System;
using System.Collections.Generic;
using Tnf.Notifications;

namespace Thex.SuperAdmin.Infra.Entities
{
    public partial class Company : IEntityInt
    {
        public Company()
        {
            InverseParentCompanyList = new HashSet<Company>();
            PropertyList = new HashSet<Property>();
        }

        public int Id { get; set; }
        public Guid PersonId { get; internal set; }
        public int? ParentCompanyId { get; internal set; }
        public string ShortName { get; internal set; }
        public string TradeName { get; internal set; }
        public Guid CompanyUid { get; internal set; }

        public virtual Company ParentCompany { get; internal set; }
        public virtual Person Person { get; internal set; }
        public virtual ICollection<Company> InverseParentCompanyList { get; internal set; }
        public virtual ICollection<Property> PropertyList { get; internal set; }

        public enum EntityError
        {
            CompanyMustHavePersonId,
            CompanyMustHaveShortName,
            CompanyOutOfBoundShortName,
            CompanyMustHaveTradeName,
            CompanyOutOfBoundTradeName,
            CompanyMustHaveEmail,
            CompanyHasInvalidPersonType,
            PersonNaturalDoesNotHaveMainDocument,
            PersonLegalDoesNotHaveMainDocument,
            CompanyLocationListGreaterThanAllowed,
            CompanyLocationListHasDuplicates,
            CompanyMustHaveCommercialLocation
        }

        public static Builder Create(INotificationHandler handler)
            => new Builder(handler);

        public static Builder Create(INotificationHandler handler, Company instance)
            => new Builder(handler, instance);
    }
}
