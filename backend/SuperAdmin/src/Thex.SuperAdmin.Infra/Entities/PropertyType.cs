﻿using System;
using System.Collections.Generic;
using Thex.Common.Enumerations;
using Tnf.Notifications;

namespace Thex.SuperAdmin.Infra.Entities
{
    public partial class PropertyType : IEntityInt
    {
        public PropertyType()
        {
            PropertyList = new HashSet<Property>();
        }

        public int Id { get; set; }
        public string Name { get; internal set; }

        public virtual ICollection<Property> PropertyList { get; internal set; }

        public enum EntityError
        {
            PropertyTypeMustHaveName,
            PropertyTypeOutOfBoundName
        }

        public static Builder Create(INotificationHandler handler)
            => new Builder(handler);

        public static Builder Create(INotificationHandler handler, PropertyType instance)
            => new Builder(handler, instance);
    }
}
