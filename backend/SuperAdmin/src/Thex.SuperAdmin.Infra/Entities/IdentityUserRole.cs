﻿using System;
using Tnf.Notifications;

namespace Thex.SuperAdmin.Infra.Entities
{
    public partial class UserRole : ThexIdentityUserRoleEntity
    {
        public Guid UserRolesId { get; set; }
        public int? ChainId { get; internal set; }
        public int? PropertyId { get; internal set; }
        public int? BrandId { get; internal set; }

        public virtual Chain Chain { get; internal set; }
        public virtual Property Property { get; internal set; }
        public virtual Brand Brand { get; internal set; }

        public enum EntityError
        {
            UserRolesMustHaveUserId,
            UserRolesMustHaveRoleId
        }

        public static Builder Create(INotificationHandler handler)
            => new Builder(handler);

        public static Builder Create(INotificationHandler handler, UserRole instance)
            => new Builder(handler, instance);
    }
}
