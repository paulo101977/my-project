﻿using System;
using System.Collections.Generic;
using Thex.Common.Enumerations;
using Tnf.Notifications;

namespace Thex.SuperAdmin.Infra.Entities
{
    public partial class Status : IEntityInt
    {
        public Status()
        {
            PropertyList = new HashSet<Property>();
        }

        public int Id { get; set; }
        public int StatusCategoryId { get; internal set; }
        public string Name { get; internal set; }

        public virtual StatusCategory StatusCategory { get; internal set; }
        public virtual ICollection<Property> PropertyList { get; internal set; }

        public enum EntityError
        {
            StatusMustHaveStatusCategoryId,
            StatusMustHaveName,
            StatusOutOfBoundName,
            PropertyStatusInvalid
        }

        public static Builder Create(INotificationHandler handler)
            => new Builder(handler);

        public static Builder Create(INotificationHandler handler, Status instance)
            => new Builder(handler, instance);
    }
}
