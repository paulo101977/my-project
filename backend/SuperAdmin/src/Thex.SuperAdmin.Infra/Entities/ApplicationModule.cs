﻿using System;
using System.Collections.Generic;
using Tnf.Notifications;

namespace Thex.SuperAdmin.Infra.Entities
{
    public partial class ApplicationModule : IEntityInt
    {
        public ApplicationModule()
        {
            ApplicationParameterList = new HashSet<ApplicationParameter>();
        }
        public int Id { get; set; }

        public string ApplicationModuleName { get; internal set; }
        public string ApplicationModuleDescription { get; internal set; }

        public virtual ICollection<ApplicationParameter> ApplicationParameterList { get; internal set; }

        public enum EntityError
        {
            ApplicationModuleMustHaveApplicationModuleName,
            ApplicationModuleOutOfBoundApplicationModuleName,
            ApplicationModuleMustHaveApplicationModuleDescription,
            ApplicationModuleOutOfBoundApplicationModuleDescription
        }

        public static Builder Create(INotificationHandler handler)
            => new Builder(handler);

        public static Builder Create(INotificationHandler handler, ApplicationModule instance)
            => new Builder(handler, instance);
    }
}
