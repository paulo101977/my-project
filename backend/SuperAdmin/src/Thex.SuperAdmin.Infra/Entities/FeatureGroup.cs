﻿using System;
using System.Collections.Generic;
using Tnf.Notifications;

namespace Thex.SuperAdmin.Infra.Entities
{
    public partial class FeatureGroup : IEntityInt
    {
        public FeatureGroup()
        {
            ApplicationParameterList = new HashSet<ApplicationParameter>();
        }

        public int Id { get; set; }
        public string FeatureGroupName { get; internal set; }
        public string Description { get; internal set; }

        public virtual ICollection<ApplicationParameter> ApplicationParameterList { get; internal set; }

        public enum EntityError
        {
            FeatureGroupMustHaveFeatureGroupName,
            FeatureGroupOutOfBoundFeatureGroupName,
            FeatureGroupMustHaveDescription,
            FeatureGroupOutOfBoundDescription
        }

        public static Builder Create(INotificationHandler handler)
            => new Builder(handler);

        public static Builder Create(INotificationHandler handler, FeatureGroup instance)
            => new Builder(handler, instance);
    }
}
