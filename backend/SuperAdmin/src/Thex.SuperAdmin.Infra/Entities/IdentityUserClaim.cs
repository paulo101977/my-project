﻿using Microsoft.AspNetCore.Identity;
using System;
using Tnf.Notifications;

namespace Thex.SuperAdmin.Infra.Entities
{
    public partial class UserClaim : ThexIdentityUserClaimEntity
    {
        public enum EntityError
        {
            UserClaimsMustHaveUserId
        }

        public static Builder Create(INotificationHandler handler)
            => new Builder(handler);

        public static Builder Create(INotificationHandler handler, UserClaim instance)
            => new Builder(handler, instance);
    }
}
