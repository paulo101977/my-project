﻿using System;
using System.Collections.Generic;
using Tnf.Notifications;

namespace Thex.SuperAdmin.Infra.Entities
{
    public partial class PropertyParameter : ThexMultiTenantFullAuditedEntity, IEntityGuid
    {
        public Guid Id { get; set; }
        public int PropertyId { get; internal set; }
        public int ApplicationParameterId { get; internal set; }
        public string PropertyParameterValue { get; internal set; }
        public string PropertyParameterMinValue { get; internal set; }
        public string PropertyParameterMaxValue { get; internal set; }
        public string PropertyParameterPossibleValues { get; internal set; }
        public bool IsActive { get; internal set; }

        public virtual ApplicationParameter ApplicationParameter { get; internal set; }
        public virtual Property Property { get; internal set; }

        public enum EntityError
        {
            PropertyParameterMustHavePropertyId,
            PropertyParameterMustHaveApplicationParameterId,
            PropertyParameterOutOfBoundPropertyParameterMinValue,
            PropertyParameterOutOfBoundPropertyParameterMaxValue,
            PropertyParameterHourFormatInvalid,
            PropertyParameterCheckoutLargerCheckin,
            PropertyParameterAgeRangeInvalidad,
            PropertyParameterIsNotDeactivateChildren_2,
            PropertyParameterMustHaveDailyBillingItem,
            PropertyParameterMustHaveTimeZone
        }

        public static Builder Create(INotificationHandler handler)
            => new Builder(handler);

        public static Builder Create(INotificationHandler handler, PropertyParameter instance)
            => new Builder(handler, instance);
    }
}
