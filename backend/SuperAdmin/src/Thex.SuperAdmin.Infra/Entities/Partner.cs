﻿
using System.Collections.Generic;

namespace Thex.SuperAdmin.Infra.Entities
{
    public class Partner : IEntityInt
    {
        public int Id { get; set; }
        public string PartnerName { get; internal set; }

        public virtual ICollection<IntegrationPartnerProperty> IntegrationPartnerPropertyList { get; internal set; }

        public Partner()
        {
            IntegrationPartnerPropertyList = new HashSet<IntegrationPartnerProperty>();
        }
    }
}
