﻿
namespace Thex.SuperAdmin.Infra.Entities
{
    public class PlasticBrand : IEntityInt
    {
        public int Id { get; set; }

        public string PlasticBrandName { get; internal set; }

        public string IconName { get; internal set; }
    }
}
