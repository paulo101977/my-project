﻿using Thex.SuperAdmin.Infra.Entities;
using Tnf.Notifications;
using Thex.SuperAdmin.Infra.Interfaces.Services;
using System.Threading.Tasks;
using Thex.SuperAdmin.Infra.Interfaces;
using Tnf;
using System.Collections.Generic;
using Thex.SuperAdmin.Dto;
using System;
using System.Linq;
using Tnf.Dto;

namespace Thex.SuperAdmin.Infra.Services
{
    public class MenuDomainService : IMenuDomainService
    {
        private const int INITIALLEVEL = 1;
        private readonly INotificationHandler _notificationHandler;

        public MenuDomainService(INotificationHandler notificationHandler)
        {
            _notificationHandler = notificationHandler;
        }

        public async Task<ListDto<MenuItemDto>> GetMenuItemHierarchy(List<IdentityMenuFeatureDto> menuList)
        {
            if (menuList == null || menuList.Count == 0)
                return null;
            
            var items = new List<MenuItemDto>();

            foreach (var menu in menuList.Where(e => e.Level == INITIALLEVEL).OrderBy(e => e.OrderNumber))
                items.Add(await CreateMenuItemHierarchy(menuList, menu));

            return new ListDto<MenuItemDto>
            {
                HasNext = false,
                Items = items
            };
        }

        public async Task<MenuItemDto> CreateMenuItemHierarchy(List<IdentityMenuFeatureDto> menuList, IdentityMenuFeatureDto menu)
        {
            var result = new MenuItemDto
            {
                Id = menu.Id,
                ProductId = menu.IdentityProductId,
                ModuleId = menu.IdentityModuleId,
                IsActive = menu.IsActive,
                OrderNumber = menu.OrderNumber,
                Description = menu.Description,
                FeatureId = menu.IdentityFeature.Id,
                FeatureName = menu.IdentityFeature.Name,
                IsInternal = menu.IdentityFeature.IsInternal,
                Key = menu.IdentityFeature.Key,
                ParentId = menu.IdentityFeatureParentId,
                Children = new List<MenuItemDto>()
            };

            foreach (var menuChild in menuList.Where(e => e.IdentityFeatureParentId == menu.IdentityFeatureId).OrderBy(e => e.OrderNumber))
                result.Children.Add(await CreateMenuItemHierarchy(menuList, menuChild));

            return result;
        }

        public async Task<List<string>> GetParentKeyList(List<IdentityMenuFeatureDto> menuFeatureList, List<string> permissionKeyList)
        {
            var keyList = new List<string>();

            foreach (var menu in menuFeatureList.Where(e => permissionKeyList.Contains(e.IdentityFeature.Key)))
                await GetRecursiveParentKey(keyList, menu, menuFeatureList);

            return keyList;
        }

        public async Task GetRecursiveParentKey(List<string> keyList, IdentityMenuFeatureDto menu, List<IdentityMenuFeatureDto> menuFeatureList)
        {
            if (menu == null)
                return;

            keyList.Add(menu.IdentityFeature.Key);

            if (menu.IdentityFeatureParentId.HasValue)
                await GetRecursiveParentKey(keyList, menuFeatureList.FirstOrDefault(e => e.IdentityFeatureId == menu.IdentityFeatureParentId.Value), menuFeatureList);
        }
    }
}
