﻿using Thex.SuperAdmin.Infra.Entities;
using Tnf.Notifications;
using Thex.SuperAdmin.Infra.Interfaces.Services;
using System.Threading.Tasks;
using Thex.SuperAdmin.Infra.Interfaces;
using Tnf;

namespace Thex.SuperAdmin.Infra.Services
{
    public class ChainDomainService : IChainDomainService
    {
        private readonly IChainRepository _repository;
        private readonly INotificationHandler _notificationHandler;

        public ChainDomainService(IChainRepository repository,
            INotificationHandler notificationHandler) 
        {
            _repository = repository;
            _notificationHandler = notificationHandler;
        }

        public async Task<Chain> InsertAndSaveChangesAsync(Chain.Builder builder)
        {
            Check.NotNull(builder, nameof(builder));

            var chain = builder.Build();

            if (!_notificationHandler.HasNotification())
                await _repository.InsertAndSaveChangesAsync(chain);

            return chain;
        }

        public async Task<Chain> UpdateAndSaveChangesAsync(Chain.Builder builder)
        {
            Check.NotNull(builder, nameof(builder));

            var chain = builder.Build();

            if (_notificationHandler.HasNotification())
                return null;

            return await _repository.UpdateAndSaveChangesAsync(chain);
        }
    }
}
