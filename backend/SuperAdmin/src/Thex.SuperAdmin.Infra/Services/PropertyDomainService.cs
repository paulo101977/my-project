﻿using Thex.SuperAdmin.Infra.Entities;
using Tnf.Notifications;
using Thex.SuperAdmin.Infra.Interfaces.Services;
using System.Threading.Tasks;
using Thex.SuperAdmin.Infra.Interfaces;
using Tnf;

namespace Thex.SuperAdmin.Infra.Services
{
    public class PropertyDomainService : IPropertyDomainService
    {
        private readonly IPropertyRepository _repository;
        private readonly INotificationHandler _notificationHandler;

        public PropertyDomainService(IPropertyRepository repository,
            INotificationHandler notificationHandler) 
        {
            _repository = repository;
            _notificationHandler = notificationHandler;
        }

        public async Task<Property> InsertAndSaveChangesAsync(Property.Builder builder)
        {
            Check.NotNull(builder, nameof(builder));

            var property = builder.Build();

            if (!_notificationHandler.HasNotification())
                await _repository.InsertAndSaveChangesAsync(property);

            return property;
        }

        public async Task<Property> UpdateAndSaveChangesAsync(Property.Builder builder)
        {
            Check.NotNull(builder, nameof(builder));

            var property = builder.Build();

            if (_notificationHandler.HasNotification())
                return null;

            return await _repository.UpdateAndSaveChangesAsync(property);
        }
    }
}
