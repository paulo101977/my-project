﻿using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Thex.SuperAdmin.Infra.Entities;
using Tnf.Domain.Services;

namespace Thex.SuperAdmin.Infra.Services
{
    public class ThexIdentityRoleManager : RoleManager<Roles>, IDomainService
    {
        public ThexIdentityRoleManager(
            IRoleStore<Roles> store,
            IEnumerable<IRoleValidator<Roles>> roleValidators,
            ILookupNormalizer keyNormalizer,
            IdentityErrorDescriber errors,
            ILogger<RoleManager<Roles>> logger) : base(store, roleValidators, keyNormalizer, errors, logger)
        {
        }

        public override async Task<IdentityResult> CreateAsync(Roles role)
        {
            ThrowIfDisposed();
            if (role == null)
            {
                throw new ArgumentNullException("role");
            }
            
            return await Store.CreateAsync(role, CancellationToken.None);
        }
    }
}
