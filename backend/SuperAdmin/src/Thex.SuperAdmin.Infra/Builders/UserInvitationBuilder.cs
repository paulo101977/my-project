﻿using System;
using System.Collections.Generic;
using Thex.Common;
using Tnf.Builder;
using Tnf.Notifications;
using Tnf.Specifications;

namespace Thex.SuperAdmin.Infra.Entities
{
    public partial class UserInvitation
    {
        public class Builder : Builder<UserInvitation>
        {
            public Builder(INotificationHandler handler) : base(handler)
            {
            }

            public Builder(INotificationHandler handler, UserInvitation instance) : base(handler, instance)
            {
            }

            public virtual Builder WithId(Guid id)
            {
                Instance.Id = id;
                return this;
            }
            public virtual Builder WithChainId(int? chainId)
            {
                Instance.ChainId = chainId;
                return this;
            }
            public virtual Builder WithPropertyId(int? propertyId)
            {
                Instance.PropertyId = propertyId;
                return this;
            }
            public virtual Builder WithBrandId(int? brandId)
            {
                Instance.BrandId = brandId;
                return this;
            }
            public virtual Builder WithEmail(string email)
            {
                Instance.Email = email;
                return this;
            }
            public virtual Builder WithName(string name)
            {
                Instance.Name = name;
                return this;
            }
            public virtual Builder WithInvitationLink(string invitationLink)
            {
                Instance.InvitationLink = invitationLink;
                return this;
            }
            public virtual Builder WithInvitationDate(DateTime invitationDate)
            {
                Instance.InvitationDate = invitationDate;
                return this;
            }
            public virtual Builder WithInvitationAcceptanceDate(DateTime? invitationAcceptanceDate)
            {
                Instance.InvitationAcceptanceDate = invitationAcceptanceDate;
                return this;
            }
            public virtual Builder WithIsActive(bool isActive)
            {
                Instance.IsActive = isActive;
                return this;
            }

            public virtual Builder WithUserId(Guid? userId)
            {
                Instance.UserId = userId;
                return this;
            }

            public virtual Builder WithTenantId(Guid tenantId)
            {
                Instance.TenantId = tenantId;
                return this;
            }

            protected override void Specifications()
            {
                AddSpecification(new ExpressionSpecification<UserInvitation>(
                    AppConsts.LocalizationSourceName,
                    UserInvitation.EntityError.UserInvitationMustHaveEmail,
                    w => !string.IsNullOrWhiteSpace(w.Email)));

                AddSpecification(new ExpressionSpecification<UserInvitation>(
                    AppConsts.LocalizationSourceName,
                    UserInvitation.EntityError.UserInvitationOutOfBoundEmail,
                    w => string.IsNullOrWhiteSpace(w.Email) || w.Email.Length > 0 && w.Email.Length <= 600));

                AddSpecification(new ExpressionSpecification<UserInvitation>(
                    AppConsts.LocalizationSourceName,
                    UserInvitation.EntityError.UserInvitationMustHaveName,
                    w => !string.IsNullOrWhiteSpace(w.Name)));

                AddSpecification(new ExpressionSpecification<UserInvitation>(
                    AppConsts.LocalizationSourceName,
                    UserInvitation.EntityError.UserInvitationOutOfBoundName,
                    w => string.IsNullOrWhiteSpace(w.Name) || w.Name.Length > 0 && w.Name.Length <= 300));

                AddSpecification(new ExpressionSpecification<UserInvitation>(
                    AppConsts.LocalizationSourceName,
                    UserInvitation.EntityError.UserInvitationMustHaveInvitationLink,
                    w => !string.IsNullOrWhiteSpace(w.InvitationLink)));

                AddSpecification(new ExpressionSpecification<UserInvitation>(
                    AppConsts.LocalizationSourceName,
                    UserInvitation.EntityError.UserInvitationOutOfBoundInvitationLink,
                    w => string.IsNullOrWhiteSpace(w.InvitationLink) || w.InvitationLink.Length > 0 && w.InvitationLink.Length <= 4000));

                AddSpecification(new ExpressionSpecification<UserInvitation>(
                    AppConsts.LocalizationSourceName,
                    UserInvitation.EntityError.UserInvitationInvalidInvitationDate,
                    w => w.InvitationDate >= new DateTime(1753, 1, 1) && w.InvitationDate <= DateTime.MaxValue));

                AddSpecification(new ExpressionSpecification<UserInvitation>(
                    AppConsts.LocalizationSourceName,
                    UserInvitation.EntityError.UserInvitationInvalidInvitationAcceptanceDate,
                    w => w.InvitationAcceptanceDate == null || w.InvitationAcceptanceDate >= new DateTime(1753, 1, 1) && w.InvitationAcceptanceDate <= DateTime.MaxValue));

            }
        }
    }

}