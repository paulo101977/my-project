﻿using System;
using System.Collections.Generic;
using Thex.Common;
using Tnf.Builder;
using Tnf.Notifications;
using Tnf.Specifications;

namespace Thex.SuperAdmin.Infra.Entities
{
    public partial class UserToken
    {
        public class Builder : Builder<UserToken>
        {
            public Builder(INotificationHandler handler) : base(handler)
            {
            }

            public Builder(INotificationHandler handler, UserToken instance) : base(handler, instance)
            {
            }

            public virtual Builder WithUserId(Guid userId)
            {
                Instance.UserId = userId;
                return this;
            }
            public virtual Builder WithLoginProvider(Guid loginProvider)
            {
                Instance.LoginProvider = loginProvider.ToString();
                return this;
            }
            public virtual Builder WithName(string name)
            {
                Instance.Name = name;
                return this;
            }
            public virtual Builder WithValue(string value)
            {
                Instance.Value = value;
                return this;
            }

            protected override void Specifications()
            {
                AddSpecification(new ExpressionSpecification<UserToken>(
                AppConsts.LocalizationSourceName,
                UserToken.EntityError.UserTokensMustHaveUserId,
                w => w.UserId != default(Guid)));

                AddSpecification(new ExpressionSpecification<UserToken>(
                AppConsts.LocalizationSourceName,
                UserToken.EntityError.UserTokensMustHaveName,
                w => !string.IsNullOrWhiteSpace(w.Name)));

                AddSpecification(new ExpressionSpecification<UserToken>(
                AppConsts.LocalizationSourceName,
                UserToken.EntityError.UserTokensOutOfBoundName,
                w => string.IsNullOrWhiteSpace(w.Name) || w.Name.Length > 0 && w.Name.Length <= 128));

            }
        }
    }

}