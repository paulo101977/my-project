﻿using System;
using System.Collections.Generic;
using Thex.Common;
using Tnf.Builder;
using Tnf.Notifications;
using Tnf.Specifications;

namespace Thex.SuperAdmin.Infra.Entities
{
    public partial class UserClaim
    {
        public class Builder : Builder<UserClaim>
        {
            public Builder(INotificationHandler handler) : base(handler)
            {
            }

            public Builder(INotificationHandler handler, UserClaim instance) : base(handler, instance)
            {
            }

            public virtual Builder WithUserId(Guid userId)
            {
                Instance.UserId = userId;
                return this;
            }
            public virtual Builder WithClaimType(string claimType)
            {
                Instance.ClaimType = claimType;
                return this;
            }
            public virtual Builder WithClaimValue(string claimValue)
            {
                Instance.ClaimValue = claimValue;
                return this;
            }

            protected override void Specifications()
            {
                AddSpecification(new ExpressionSpecification<UserClaim>(
                AppConsts.LocalizationSourceName,
                UserClaim.EntityError.UserClaimsMustHaveUserId,
                w => w.UserId != default(Guid)));
            }
        }
    }

}