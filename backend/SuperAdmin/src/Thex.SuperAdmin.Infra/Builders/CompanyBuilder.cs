﻿using System;
using System.Collections.Generic;
using Thex.Common;
using Tnf.Builder;
using Tnf.Notifications;
using Tnf.Specifications;

namespace Thex.SuperAdmin.Infra.Entities
{
    public partial class Company
    {
        public class Builder : Builder<Company>
        {
            public Builder(INotificationHandler handler) : base(handler)
            {
            }

            public Builder(INotificationHandler handler, Company instance) : base(handler, instance)
            {
            }

            public virtual Builder WithId(int id)
            {
                Instance.Id = id;
                return this;
            }
            public virtual Builder WithCompanyUid(Guid uId)
            {
                Instance.CompanyUid = uId;
                return this;
            }
            public virtual Builder WithPersonId(Guid personId)
            {
                Instance.PersonId = personId;
                return this;
            }
            public virtual Builder WithParentCompanyId(int? parentCompanyId)
            {
                Instance.ParentCompanyId = parentCompanyId;
                return this;
            }
            public virtual Builder WithShortName(string shortName)
            {
                Instance.ShortName = shortName;
                return this;
            }
            public virtual Builder WithTradeName(string tradeName)
            {
                Instance.TradeName = tradeName;
                return this;
            }
            public virtual Builder WithPerson(Person person)
            {
                Instance.Person = person;
                return this;
            }

            protected override void Specifications()
            {

                AddSpecification(new ExpressionSpecification<Company>(
                    AppConsts.LocalizationSourceName,
                    Company.EntityError.CompanyMustHaveShortName,
                    w => !string.IsNullOrWhiteSpace(w.ShortName)));

                AddSpecification(new ExpressionSpecification<Company>(
                    AppConsts.LocalizationSourceName,
                    Company.EntityError.CompanyOutOfBoundShortName,
                    w => string.IsNullOrWhiteSpace(w.ShortName) || w.ShortName.Length > 0 && w.ShortName.Length <= 50));

                AddSpecification(new ExpressionSpecification<Company>(
                    AppConsts.LocalizationSourceName,
                    Company.EntityError.CompanyMustHaveTradeName,
                    w => !string.IsNullOrWhiteSpace(w.TradeName)));

                AddSpecification(new ExpressionSpecification<Company>(
                    AppConsts.LocalizationSourceName,
                    Company.EntityError.CompanyOutOfBoundTradeName,
                    w => string.IsNullOrWhiteSpace(w.TradeName) || w.TradeName.Length > 0 && w.TradeName.Length <= 200));
            }
        }
    }
}
