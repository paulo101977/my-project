﻿using System;
using System.Collections.Generic;
using Thex.Common;
using Tnf.Builder;
using Tnf.Notifications;
using Tnf.Specifications;

namespace Thex.SuperAdmin.Infra.Entities
{
    public partial class Status
    {
        public class Builder : Builder<Status>
        {
            public Builder(INotificationHandler handler) : base(handler)
            {
            }

            public Builder(INotificationHandler handler, Status instance) : base(handler, instance)
            {
            }

            public virtual Builder WithId(int id)
            {
                Instance.Id = id;
                return this;
            }
            public virtual Builder WithStatusCategoryId(int statusCategoryId)
            {
                Instance.StatusCategoryId = statusCategoryId;
                return this;
            }
            public virtual Builder WithName(string name)
            {
                Instance.Name = name;
                return this;
            }

            protected override void Specifications()
            {
                AddSpecification(new ExpressionSpecification<Status>(
                    AppConsts.LocalizationSourceName,
                    Status.EntityError.StatusMustHaveStatusCategoryId,
                    w => w.StatusCategoryId != default(int)));

                AddSpecification(new ExpressionSpecification<Status>(
                    AppConsts.LocalizationSourceName,
                    Status.EntityError.StatusMustHaveName,
                    w => !string.IsNullOrWhiteSpace(w.Name)));

                AddSpecification(new ExpressionSpecification<Status>(
                    AppConsts.LocalizationSourceName,
                    Status.EntityError.StatusOutOfBoundName,
                    w => string.IsNullOrWhiteSpace(w.Name) || w.Name.Length > 0 && w.Name.Length <= 20));

            }
        }
    }
}
