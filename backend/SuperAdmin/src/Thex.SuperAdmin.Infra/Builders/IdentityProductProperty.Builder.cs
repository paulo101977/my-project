﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a Crudzilla tool.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using Tnf.Builder;
using Tnf.Notifications;
using Tnf.Specifications;
using Thex.Common;

namespace Thex.SuperAdmin.Infra.Entities
{
    public partial class IdentityProductProperty
    {
        public class Builder : Builder<IdentityProductProperty>
        {
            public Builder(INotificationHandler handler) : base(handler)
            {
            }
            public Builder(INotificationHandler handler, IdentityProductProperty instance) : base(handler, instance)
            {
            }

            public virtual Builder WithId(int id)
            {
                Instance.Id = id;
                return this;
            }
            public virtual Builder WithProductId(int id)
            {
                Instance.IdentityProductId = id;
                return this;
            }
            public virtual Builder WithPropertyId(int propertyId)
            {
                Instance.PropertyId = propertyId;
                return this;
            }
            public virtual Builder WithIsDeleted(bool isDeleted)
            {
                Instance.IsDeleted = isDeleted;
                return this;
            }
            public virtual Builder WithCreationTime(DateTime creationTime)
            {
                Instance.CreationTime = creationTime;
                return this;
            }
            public virtual Builder WithCreatorUserId(Guid? creatorUserId)
            {
                Instance.CreatorUserId = creatorUserId;
                return this;
            }
            public virtual Builder WithLastModificationTime(DateTime? lastModificationTime)
            {
                Instance.LastModificationTime = lastModificationTime;
                return this;
            }
            public virtual Builder WithLastModifierUserId(Guid? lastModifierUserId)
            {
                Instance.LastModifierUserId = lastModifierUserId;
                return this;
            }
            public virtual Builder WithDeletionTime(DateTime? deletionTime)
            {
                Instance.DeletionTime = deletionTime;
                return this;
            }
            public virtual Builder WithDeleterUserId(Guid? deleterUserId)
            {
                Instance.DeleterUserId = deleterUserId;
                return this;
            }

            protected override void Specifications()
            {

            }
        }
    }
}
