﻿using System;
using System.Collections.Generic;
using Thex.Common;
using Tnf.Builder;
using Tnf.Notifications;
using Tnf.Specifications;

namespace Thex.SuperAdmin.Infra.Entities
{
    public partial class PropertyType
    {
        public class Builder : Builder<PropertyType>
        {
            public Builder(INotificationHandler handler) : base(handler)
            {
            }

            public Builder(INotificationHandler handler, PropertyType instance) : base(handler, instance)
            {
            }

            public virtual Builder WithId(int id)
            {
                Instance.Id = id;
                return this;
            }
            public virtual Builder WithName(string name)
            {
                Instance.Name = name;
                return this;
            }

            protected override void Specifications()
            {
                AddSpecification(new ExpressionSpecification<PropertyType>(
                    AppConsts.LocalizationSourceName,
                    PropertyType.EntityError.PropertyTypeMustHaveName,
                    w => !string.IsNullOrWhiteSpace(w.Name)));

                AddSpecification(new ExpressionSpecification<PropertyType>(
                    AppConsts.LocalizationSourceName,
                    PropertyType.EntityError.PropertyTypeOutOfBoundName,
                    w => string.IsNullOrWhiteSpace(w.Name) || w.Name.Length > 0 && w.Name.Length <= 50));

            }
        }
    }
}
