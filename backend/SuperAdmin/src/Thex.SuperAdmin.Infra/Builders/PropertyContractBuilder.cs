﻿using System;
using Thex.Common;
using Tnf.Builder;
using Tnf.Notifications;
using Tnf.Specifications;

namespace Thex.SuperAdmin.Infra.Entities
{
    public partial class PropertyContract
    {
        public class Builder : Builder<PropertyContract>
        {
            public Builder(INotificationHandler handler) : base(handler)
            {
            }

            public Builder(INotificationHandler handler, PropertyContract instance) : base(handler, instance)
            {
            }

            public virtual Builder WithId(Guid id)
            {
                Instance.Id = id;
                return this;
            }
            public virtual Builder WithPropertyId(int propertyId)
            {
                Instance.PropertyId = propertyId;
                return this;
            }
            public virtual Builder WithMaxUh(int maxUh)
            {
                Instance.MaxUh = maxUh;
                return this;
            }

            protected override void Specifications()
            {
                AddSpecification(new ExpressionSpecification<PropertyContract>(
                   AppConsts.LocalizationSourceName,
                   PropertyContract.EntityError.PropertyContractMustHavePropertyId,
                   w => w.PropertyId != 0));

                AddSpecification(new ExpressionSpecification<PropertyContract>(
                    AppConsts.LocalizationSourceName,
                    PropertyContract.EntityError.PropertyContractMustHaveMaxUh,
                    w => w.MaxUh != 0));
            }
        }
    }
}
