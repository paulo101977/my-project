﻿using System;
using Thex.Common;
using Tnf.Builder;
using Tnf.Notifications;
using Tnf.Specifications;

namespace Thex.SuperAdmin.Infra.Entities
{
    public partial class UserTenant
    {
        public class Builder : Builder<UserTenant>
        {
            public Builder(INotificationHandler handler) : base(handler)
            {
            }

            public Builder(INotificationHandler handler, UserTenant instance) : base(handler, instance)
            {
            }

            public virtual Builder WithId(Guid id)
            {
                Instance.Id = id;
                return this;
            }
            public virtual Builder WithUserId(Guid userId)
            {
                Instance.UserId = userId;
                return this;
            }
            public virtual Builder WithTenantId(Guid tenantId)
            {
                Instance.TenantId = tenantId;
                return this;
            }

            public virtual Builder WithIsActive(bool isActive)
            {
                Instance.IsActive = isActive;
                return this;
            }

            protected override void Specifications()
            {
                AddSpecification(new ExpressionSpecification<UserTenant>(
                    AppConsts.LocalizationSourceName,
                    UserTenant.EntityError.UserTenantMustHaveTenantId,
                    w => w.TenantId != Guid.Empty));

                AddSpecification(new ExpressionSpecification<UserTenant>(
                    AppConsts.LocalizationSourceName,
                    UserTenant.EntityError.UserTenantMustHaveUserId,
                    w => w.UserId != Guid.Empty));
            }
        }
    }
}
