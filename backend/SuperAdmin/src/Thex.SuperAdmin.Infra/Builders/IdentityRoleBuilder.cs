﻿using System;
using System.Collections.Generic;
using Thex.Common;
using Tnf.Builder;
using Tnf.Notifications;
using Tnf.Specifications;

namespace Thex.SuperAdmin.Infra.Entities
{
    public partial class Roles
    {
        public class Builder : Builder<Roles>
        {
            public Builder(INotificationHandler handler) : base(handler)
            {
            }

            public Builder(INotificationHandler handler, Roles instance) : base(handler, instance)
            {
            }

            public virtual Builder WithId(Guid id)
            {
                Instance.Id = id;
                return this;
            }
            public virtual Builder WithName(string name)
            {
                Instance.Name = name;
                return this;
            }
            public virtual Builder WithNormalizedName(string normalizedName)
            {
                Instance.NormalizedName = normalizedName;
                return this;
            }
            public virtual Builder WithConcurrencyStamp(string concurrencyStamp)
            {
                Instance.ConcurrencyStamp = concurrencyStamp;
                return this;
            }

            public virtual Builder WithIsActive(bool isActive)
            {
                Instance.IsActive = isActive;
                return this;
            }

            public virtual Builder WithIdentityProductId(int identityProductId)
            {
                Instance.IdentityProductId = identityProductId;
                return this;
            }

            protected override void Specifications()
            {
                AddSpecification(new ExpressionSpecification<Roles>(
                AppConsts.LocalizationSourceName,
                Roles.EntityError.RolesOutOfBoundName,
                w => string.IsNullOrWhiteSpace(w.Name) || w.Name.Length > 0 && w.Name.Length <= 256));

                AddSpecification(new ExpressionSpecification<Roles>(
                AppConsts.LocalizationSourceName,
                Roles.EntityError.RolesOutOfBoundNormalizedName,
                w => string.IsNullOrWhiteSpace(w.NormalizedName) || w.NormalizedName.Length > 0 && w.NormalizedName.Length <= 256));
            }
        }
    }

}