﻿using System;
using System.Collections.Generic;
using Thex.Common;
using Tnf.Builder;
using Tnf.Notifications;
using Tnf.Specifications;

namespace Thex.SuperAdmin.Infra.Entities
{
    public partial class ContactInformationType
    {
        public class Builder : Builder<ContactInformationType>
        {
            public Builder(INotificationHandler handler) : base(handler)
            {
            }

            public Builder(INotificationHandler handler, ContactInformationType instance) : base(handler, instance)
            {
            }

            public virtual Builder WithId(int id)
            {
                Instance.Id = id;
                return this;
            }
            public virtual Builder WithName(string name)
            {
                Instance.Name = name;
                return this;
            }
            public virtual Builder WithStringFormatMask(string stringFormatMask)
            {
                Instance.StringFormatMask = stringFormatMask;
                return this;
            }
            public virtual Builder WithRegexValidationExpression(string regexValidationExpression)
            {
                Instance.RegexValidationExpression = regexValidationExpression;
                return this;
            }

            protected override void Specifications()
            {
                AddSpecification(new ExpressionSpecification<ContactInformationType>(
                    AppConsts.LocalizationSourceName,
                    ContactInformationType.EntityError.ContactInformationTypeMustHaveName,
                    w => !string.IsNullOrWhiteSpace(w.Name)));

                AddSpecification(new ExpressionSpecification<ContactInformationType>(
                    AppConsts.LocalizationSourceName,
                    ContactInformationType.EntityError.ContactInformationTypeOutOfBoundName,
                    w => string.IsNullOrWhiteSpace(w.Name) || w.Name.Length > 0 && w.Name.Length <= 50));

                AddSpecification(new ExpressionSpecification<ContactInformationType>(
                    AppConsts.LocalizationSourceName,
                    ContactInformationType.EntityError.ContactInformationTypeOutOfBoundStringFormatMask,
                    w => string.IsNullOrWhiteSpace(w.StringFormatMask) || w.StringFormatMask.Length > 0 && w.StringFormatMask.Length <= 100));

                AddSpecification(new ExpressionSpecification<ContactInformationType>(
                    AppConsts.LocalizationSourceName,
                    ContactInformationType.EntityError.ContactInformationTypeOutOfBoundRegexValidationExpression,
                    w => string.IsNullOrWhiteSpace(w.RegexValidationExpression) || w.RegexValidationExpression.Length > 0 && w.RegexValidationExpression.Length <= 500));

            }
        }
    }
}
