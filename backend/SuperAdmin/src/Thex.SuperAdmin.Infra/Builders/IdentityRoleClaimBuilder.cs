﻿using System;
using System.Collections.Generic;
using Thex.Common;
using Tnf.Builder;
using Tnf.Notifications;
using Tnf.Specifications;

namespace Thex.SuperAdmin.Infra.Entities
{
    public partial class RoleClaim
    {
        public class Builder : Builder<RoleClaim>
        {
            public Builder(INotificationHandler handler) : base(handler)
            {
            }

            public Builder(INotificationHandler handler, RoleClaim instance) : base(handler, instance)
            {
            }

            public virtual Builder WithRoleId(Guid roleId)
            {
                Instance.RoleId = roleId;
                return this;
            }
            public virtual Builder WithClaimType(string claimType)
            {
                Instance.ClaimType = claimType;
                return this;
            }
            public virtual Builder WithClaimValue(string claimValue)
            {
                Instance.ClaimValue = claimValue;
                return this;
            }

            protected override void Specifications()
            {
                AddSpecification(new ExpressionSpecification<RoleClaim>(
                AppConsts.LocalizationSourceName,
                RoleClaim.EntityError.RoleClaimsMustHaveRoleId,
                w => w.RoleId != default(Guid)));
            }
        }
    }

}