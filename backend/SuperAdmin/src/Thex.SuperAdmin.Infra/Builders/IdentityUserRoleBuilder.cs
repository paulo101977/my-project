﻿using System;
using System.Collections.Generic;
using Thex.Common;
using Tnf.Builder;
using Tnf.Notifications;
using Tnf.Specifications;

namespace Thex.SuperAdmin.Infra.Entities
{
    public partial class UserRole
    {
        public class Builder : Builder<UserRole>
        {
            public Builder(INotificationHandler handler) : base(handler)
            {
            }

            public Builder(INotificationHandler handler, UserRole instance) : base(handler, instance)
            {
            }

            public virtual Builder WithUserRolesId(Guid userRolesId)
            {
                Instance.UserRolesId = userRolesId;
                return this;
            }

            public virtual Builder WithUserId(Guid userId)
            {
                Instance.UserId = userId;
                return this;
            }
            public virtual Builder WithRoleId(Guid roleId)
            {
                Instance.RoleId = roleId;
                return this;
            }

            public virtual Builder WithChainId(int? chainId)
            {
                Instance.ChainId = chainId;
                return this;
            }

            public virtual Builder WithBrandId(int? brandId)
            {
                Instance.BrandId = brandId;
                return this;
            }

            public virtual Builder WithPropertyId(int? propertyId)
            {
                Instance.PropertyId = propertyId;
                return this;
            }

            protected override void Specifications()
            {
                AddSpecification(new ExpressionSpecification<UserRole>(
                AppConsts.LocalizationSourceName,
                UserRole.EntityError.UserRolesMustHaveUserId,
                w => w.UserId != default(Guid)));

                AddSpecification(new ExpressionSpecification<UserRole>(
                AppConsts.LocalizationSourceName,
                UserRole.EntityError.UserRolesMustHaveRoleId,
                w => w.RoleId != default(Guid)));
            }
        }
    }

}