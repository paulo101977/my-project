﻿using System;
using System.Collections.Generic;
using Thex.Common;
using Tnf.Builder;
using Tnf.Notifications;
using Tnf.Specifications;

namespace Thex.SuperAdmin.Infra.Entities
{
    public partial class UserLogin
    {
        public class Builder : Builder<UserLogin>
        {
            public Builder(INotificationHandler handler) : base(handler)
            {
            }

            public Builder(INotificationHandler handler, UserLogin instance) : base(handler, instance)
            {
            }

            public virtual Builder WithLoginProvider(string loginProvider)
            {
                Instance.LoginProvider = loginProvider;
                return this;
            }
            public virtual Builder WithProviderKey(string providerKey)
            {
                Instance.ProviderKey = providerKey;
                return this;
            }
            public virtual Builder WithProviderDisplayName(string providerDisplayName)
            {
                Instance.ProviderDisplayName = providerDisplayName;
                return this;
            }
            public virtual Builder WithUserId(Guid userId)
            {
                Instance.UserId = userId;
                return this;
            }

            protected override void Specifications()
            {
                AddSpecification(new ExpressionSpecification<UserLogin>(
                AppConsts.LocalizationSourceName,
                UserLogin.EntityError.UserLoginsMustHaveUserId,
                w => w.UserId != default(Guid)));
            }
        }
    }

}