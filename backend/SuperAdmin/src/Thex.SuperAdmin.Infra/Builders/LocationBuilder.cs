﻿using System;
using System.Collections.Generic;
using Thex.Common;
using Tnf.Builder;
using Tnf.Notifications;
using Tnf.Specifications;

namespace Thex.SuperAdmin.Infra.Entities
{
    public partial class Location
    {
    public class Builder : Builder<Location>
    {
		public Builder(INotificationHandler handler) : base(handler)
		{
		}

		public Builder(INotificationHandler handler, Location instance) : base(handler, instance)
		{
		}
		
        public virtual Builder WithId(int id)
        {
            Instance.Id = id;
            return this;
        }
        public virtual Builder WithOwnerId(Guid ownerId)
        {
            Instance.OwnerId = ownerId;
            return this;
        }
        public virtual Builder WithLocationCategoryId(int locationCategoryId)
        {
            Instance.LocationCategoryId = locationCategoryId;
            return this;
        }
        public virtual Builder WithLatitude(decimal latitude)
        {
            Instance.Latitude = latitude;
            return this;
        }
        public virtual Builder WithLongitude(decimal longitude)
        {
            Instance.Longitude = longitude;
            return this;
        }
        public virtual Builder WithStreetName(string streetName)
        {
            Instance.StreetName = streetName;
            return this;
        }
        public virtual Builder WithStreetNumber(string streetNumber)
        {
            Instance.StreetNumber = streetNumber;
            return this;
        }
        public virtual Builder WithAdditionalAddressDetails(string additionalAddressDetails)
        {
            Instance.AdditionalAddressDetails = additionalAddressDetails;
            return this;
        }
        public virtual Builder WithNeighborhood(string neighborhood)
        {
            Instance.Neighborhood = neighborhood;
            return this;
        }
        public virtual Builder WithCityId(int cityId)
        {
            Instance.CityId = cityId;
            return this;
        }
        public virtual Builder WithPostalCode(string postalCode)
        {
            Instance.PostalCode = postalCode;
            return this;
        }
        public virtual Builder WithCountryCode(string countryCode)
        {
            Instance.CountryCode = countryCode;
            return this;
        }

        public virtual Builder WithStateId(int? stateId)
        {
            Instance.StateId = stateId;
            return this;
        }

        public virtual Builder WithCountryId(int? countryId)
        {
            Instance.CountryId = countryId;
            return this;
        }

            protected override void Specifications()
        {
            AddSpecification(new ExpressionSpecification<Location>(
				AppConsts.LocalizationSourceName, 
				Location.EntityError.LocationMustHaveLocationCategoryId, 
				w => w.LocationCategoryId != default(int)));

            AddSpecification(new ExpressionSpecification<Location>(
				AppConsts.LocalizationSourceName, 
				Location.EntityError.LocationMustHaveStreetName, 
				w => !string.IsNullOrWhiteSpace(w.StreetName)));

            AddSpecification(new ExpressionSpecification<Location>(
				AppConsts.LocalizationSourceName, 
				Location.EntityError.LocationOutOfBoundStreetName, 
				w => string.IsNullOrWhiteSpace(w.StreetName) || w.StreetName.Length > 0 && w.StreetName.Length <= 200));

            AddSpecification(new ExpressionSpecification<Location>(
				AppConsts.LocalizationSourceName, 
				Location.EntityError.LocationMustHaveStreetNumber, 
				w => !string.IsNullOrWhiteSpace(w.StreetNumber)));

            AddSpecification(new ExpressionSpecification<Location>(
				AppConsts.LocalizationSourceName, 
				Location.EntityError.LocationOutOfBoundStreetNumber, 
				w => string.IsNullOrWhiteSpace(w.StreetNumber) || w.StreetNumber.Length > 0 && w.StreetNumber.Length <= 50));

            AddSpecification(new ExpressionSpecification<Location>(
				AppConsts.LocalizationSourceName, 
				Location.EntityError.LocationOutOfBoundAdditionalAddressDetails, 
				w => string.IsNullOrWhiteSpace(w.AdditionalAddressDetails) || w.AdditionalAddressDetails.Length > 0 && w.AdditionalAddressDetails.Length <= 50));

            AddSpecification(new ExpressionSpecification<Location>(
				AppConsts.LocalizationSourceName, 
				Location.EntityError.LocationOutOfBoundNeighborhood, 
				w => string.IsNullOrWhiteSpace(w.Neighborhood) || w.Neighborhood.Length > 0 && w.Neighborhood.Length <= 100));

            AddSpecification(new ExpressionSpecification<Location>(
				AppConsts.LocalizationSourceName, 
				Location.EntityError.LocationMustHaveCityId, 
				w => w.CityId != default(int)));

            AddSpecification(new ExpressionSpecification<Location>(
				AppConsts.LocalizationSourceName, 
				Location.EntityError.LocationMustHaveCountryCode, 
				w => !string.IsNullOrWhiteSpace(w.CountryCode)));

            AddSpecification(new ExpressionSpecification<Location>(
				AppConsts.LocalizationSourceName, 
				Location.EntityError.LocationOutOfBoundCountryCode, 
				w => string.IsNullOrWhiteSpace(w.CountryCode) || w.CountryCode.Length > 0 && w.CountryCode.Length <= 2));

        }
    }
}
}
