﻿using System;
using System.Collections.Generic;
using Thex.Common;
using Tnf.Builder;
using Tnf.Notifications;
using Tnf.Specifications;

namespace Thex.SuperAdmin.Infra.Entities
{
    public partial class LocationCategory
    {
        public class Builder : Builder<LocationCategory>
        {
            public Builder(INotificationHandler handler) : base(handler)
            {
            }

            public Builder(INotificationHandler handler, LocationCategory instance) : base(handler, instance)
            {
            }

            public virtual Builder WithId(int id)
            {
                Instance.Id = id;
                return this;
            }
            public virtual Builder WithName(string name)
            {
                Instance.Name = name;
                return this;
            }
            public virtual Builder WithRecordScope(string recordScope)
            {
                Instance.RecordScope = recordScope;
                return this;
            }

            protected override void Specifications()
            {
                AddSpecification(new ExpressionSpecification<LocationCategory>(
                    AppConsts.LocalizationSourceName,
                    LocationCategory.EntityError.LocationCategoryMustHaveName,
                    w => !string.IsNullOrWhiteSpace(w.Name)));

                AddSpecification(new ExpressionSpecification<LocationCategory>(
                    AppConsts.LocalizationSourceName,
                    LocationCategory.EntityError.LocationCategoryOutOfBoundName,
                    w => string.IsNullOrWhiteSpace(w.Name) || w.Name.Length > 0 && w.Name.Length <= 50));

                AddSpecification(new ExpressionSpecification<LocationCategory>(
                    AppConsts.LocalizationSourceName,
                    LocationCategory.EntityError.LocationCategoryMustHaveRecordScope,
                    w => !string.IsNullOrWhiteSpace(w.RecordScope)));

                AddSpecification(new ExpressionSpecification<LocationCategory>(
                    AppConsts.LocalizationSourceName,
                    LocationCategory.EntityError.LocationCategoryOutOfBoundRecordScope,
                    w => string.IsNullOrWhiteSpace(w.RecordScope) || w.RecordScope.Length > 0 && w.RecordScope.Length <= 1));

            }
        }
    }
}
