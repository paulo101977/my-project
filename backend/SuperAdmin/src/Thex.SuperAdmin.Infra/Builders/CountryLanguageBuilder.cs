﻿using System;
using System.Collections.Generic;
using Thex.Common;
using Tnf.Builder;
using Tnf.Notifications;
using Tnf.Specifications;

namespace Thex.SuperAdmin.Infra.Entities
{
    public partial class CountryLanguage
    {
        public class Builder : Builder<CountryLanguage>
        {
            public Builder(INotificationHandler handler) : base(handler)
            {
            }

            public Builder(INotificationHandler handler, CountryLanguage instance) : base(handler, instance)
            {
            }

            public virtual Builder WithId(int id)
            {
                Instance.Id = id;
                return this;
            }
            public virtual Builder WithCountrySubdivisionId(int countrySubdivisionId)
            {
                Instance.CountrySubdivisionId = countrySubdivisionId;
                return this;
            }
            public virtual Builder WithLanguage(string language)
            {
                Instance.Language = language;
                return this;
            }
            public virtual Builder WithLanguageTwoLetterIsoCode(string languageTwoLetterIsoCode)
            {
                Instance.LanguageTwoLetterIsoCode = languageTwoLetterIsoCode;
                return this;
            }
            public virtual Builder WithLanguageThreeLetterIsoCode(string languageThreeLetterIsoCode)
            {
                Instance.LanguageThreeLetterIsoCode = languageThreeLetterIsoCode;
                return this;
            }
            public virtual Builder WithCultureInfoCode(string cultureInfoCode)
            {
                Instance.CultureInfoCode = cultureInfoCode;
                return this;
            }

            protected override void Specifications()
            {
                AddSpecification(new ExpressionSpecification<CountryLanguage>(
                    AppConsts.LocalizationSourceName,
                    CountryLanguage.EntityError.CountryLanguageMustHaveCountrySubdivisionId,
                    w => w.CountrySubdivisionId != default(int)));

                AddSpecification(new ExpressionSpecification<CountryLanguage>(
                    AppConsts.LocalizationSourceName,
                    CountryLanguage.EntityError.CountryLanguageMustHaveLanguage,
                    w => !string.IsNullOrWhiteSpace(w.Language)));

                AddSpecification(new ExpressionSpecification<CountryLanguage>(
                    AppConsts.LocalizationSourceName,
                    CountryLanguage.EntityError.CountryLanguageOutOfBoundLanguage,
                    w => string.IsNullOrWhiteSpace(w.Language) || w.Language.Length > 0 && w.Language.Length <= 60));

                AddSpecification(new ExpressionSpecification<CountryLanguage>(
                    AppConsts.LocalizationSourceName,
                    CountryLanguage.EntityError.CountryLanguageOutOfBoundLanguageTwoLetterIsoCode,
                    w => string.IsNullOrWhiteSpace(w.LanguageTwoLetterIsoCode) || w.LanguageTwoLetterIsoCode.Length > 0 && w.LanguageTwoLetterIsoCode.Length <= 2));

                AddSpecification(new ExpressionSpecification<CountryLanguage>(
                    AppConsts.LocalizationSourceName,
                    CountryLanguage.EntityError.CountryLanguageOutOfBoundLanguageThreeLetterIsoCode,
                    w => string.IsNullOrWhiteSpace(w.LanguageThreeLetterIsoCode) || w.LanguageThreeLetterIsoCode.Length > 0 && w.LanguageThreeLetterIsoCode.Length <= 3));

                AddSpecification(new ExpressionSpecification<CountryLanguage>(
                    AppConsts.LocalizationSourceName,
                    CountryLanguage.EntityError.CountryLanguageOutOfBoundCultureInfoCode,
                    w => string.IsNullOrWhiteSpace(w.CultureInfoCode) || w.CultureInfoCode.Length > 0 && w.CultureInfoCode.Length <= 20));

            }
        }
    }
}

