﻿using System;
using Thex.Common;
using Tnf.Builder;
using Tnf.Notifications;
using Tnf.Specifications;

namespace Thex.SuperAdmin.Infra.Entities
{
    public partial class IntegrationPartnerProperty
    {
        public class Builder : Builder<IntegrationPartnerProperty>
        {
            public Builder(INotificationHandler handler) : base(handler)
            {
            }

            public Builder(INotificationHandler handler, IntegrationPartnerProperty instance) : base(handler, instance)
            {
            }

            public virtual Builder WithId(Guid id)
            {
                Instance.Id = id;
                return this;
            }
            public virtual Builder WithPropertyId(int propertyId)
            {
                Instance.PropertyId = propertyId;
                return this;
            }
            public virtual Builder WithIsActive(bool isActive)
            {
                Instance.IsActive = isActive;
                return this;
            }
            public virtual Builder WithIntegrationCode(string integrationCode)
            {
                Instance.IntegrationCode = integrationCode;
                return this;
            }
            public virtual Builder WithPartnerId(int partnerId)
            {
                Instance.PartnerId = partnerId;
                return this;
            }
            public virtual Builder WithIntegrationPartnerId(int? integrationPartnerId)
            {
                Instance.IntegrationPartnerId = integrationPartnerId;
                return this;
            }

            protected override void Specifications()
            {
                AddSpecification(new ExpressionSpecification<IntegrationPartnerProperty>(
                   AppConsts.LocalizationSourceName,
                   IntegrationPartnerProperty.EntityError.IntegrationPartnerPropertyMustHavePartnerId,
                   w => w.PartnerId != default(int)));

                AddSpecification(new ExpressionSpecification<IntegrationPartnerProperty>(
                    AppConsts.LocalizationSourceName,
                    IntegrationPartnerProperty.EntityError.IntegrationPartnerPropertyMustHavePropertyId,
                    w => w.PropertyId != default(int)));
            }
        }
    }
}
