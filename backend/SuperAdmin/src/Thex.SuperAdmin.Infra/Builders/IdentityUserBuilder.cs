﻿using System;
using System.Collections.Generic;
using Thex.Common;
using Tnf.Builder;
using Tnf.Notifications;
using Tnf.Specifications;

namespace Thex.SuperAdmin.Infra.Entities
{
    public partial class User
    {
        public class Builder : Builder<User>
        {
            public Builder(INotificationHandler handler) : base(handler)
            {
            }

            public Builder(INotificationHandler handler, User instance) : base(handler, instance)
            {
            }

            public virtual Builder WithId(Guid id)
            {
                Instance.Id = id;
                return this;
            }

            public virtual Builder WithPersonId(Guid personId)
            {
                Instance.PersonId = personId;
                return this;
            }
            public virtual Builder WithTenantId(Guid tenantId)
            {
                Instance.TenantId = tenantId;
                return this;
            }
            public virtual Builder WithPreferredLanguage(string preferredLanguage)
            {
                Instance.PreferredLanguage = preferredLanguage;
                return this;
            }
            public virtual Builder WithPreferredCulture(string preferredCulture)
            {
                Instance.PreferredCulture = preferredCulture;
                return this;
            }
            public virtual Builder WithName(string name)
            {
                Instance.Name = name;
                return this;
            }
            public virtual Builder WithEmail(string email)
            {
                Instance.Email = email;
                return this;
            }
            public virtual Builder WithIsActive(bool isActive)
            {
                Instance.IsActive = isActive;
                return this;
            }
            public virtual Builder WithUserName(string userName)
            {
                Instance.UserName = userName;
                return this;
            }

            public virtual Builder WithIsAdmin(bool? isAdmin)
            {
                Instance.IsAdmin = isAdmin.HasValue ? isAdmin.Value : false;
                return this;
            }

            protected override void Specifications()
            {
                AddSpecification(new ExpressionSpecification<User>(
                    AppConsts.LocalizationSourceName,
                    User.EntityError.UserMustHaveUserLogin,
                    w => !string.IsNullOrWhiteSpace(w.UserName)));

                AddSpecification(new ExpressionSpecification<User>(
                    AppConsts.LocalizationSourceName,
                    User.EntityError.UserOutOfBoundUserLogin,
                    w => string.IsNullOrWhiteSpace(w.UserName) || w.UserName.Length > 0 && w.UserName.Length <= 256));

                AddSpecification(new ExpressionSpecification<User>(
                    AppConsts.LocalizationSourceName,
                    User.EntityError.UserMustHavePreferredLanguage,
                    w => !string.IsNullOrWhiteSpace(w.PreferredLanguage)));

                AddSpecification(new ExpressionSpecification<User>(
                    AppConsts.LocalizationSourceName,
                    User.EntityError.UserOutOfBoundPreferredLanguage,
                    w => string.IsNullOrWhiteSpace(w.PreferredLanguage) || w.PreferredLanguage.Length > 0 && w.PreferredLanguage.Length <= 5));

                AddSpecification(new ExpressionSpecification<User>(
                    AppConsts.LocalizationSourceName,
                    User.EntityError.UserMustHavePreferredCulture,
                    w => !string.IsNullOrWhiteSpace(w.PreferredCulture)));

                AddSpecification(new ExpressionSpecification<User>(
                    AppConsts.LocalizationSourceName,
                    User.EntityError.UserOutOfBoundPreferredCulture,
                    w => string.IsNullOrWhiteSpace(w.PreferredCulture) || w.PreferredCulture.Length > 0 && w.PreferredCulture.Length <= 5));

                AddSpecification(new ExpressionSpecification<User>(
                    AppConsts.LocalizationSourceName,
                    User.EntityError.UserMustHaveName,
                    w => !string.IsNullOrWhiteSpace(w.Name)));

                AddSpecification(new ExpressionSpecification<User>(
                    AppConsts.LocalizationSourceName,
                    User.EntityError.UserOutOfBoundName,
                    w => string.IsNullOrWhiteSpace(w.Name) || w.Name.Length > 0 && w.Name.Length <= 300));

                AddSpecification(new ExpressionSpecification<User>(
                    AppConsts.LocalizationSourceName,
                    User.EntityError.UserOutOfBoundEmail,
                    w => string.IsNullOrWhiteSpace(w.Email) || w.Email.Length > 0 && w.Email.Length <= 600));

            }
        }
    }

}