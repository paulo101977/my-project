﻿using System;
using Thex.Common;
using Tnf.Builder;
using Tnf.Notifications;
using Tnf.Specifications;

namespace Thex.SuperAdmin.Infra.Entities
{
    public partial class Tenant
    {
        public class Builder : Builder<Tenant>
        {
            public Builder(INotificationHandler handler) : base(handler)
            {
            }

            public Builder(INotificationHandler handler, Tenant instance) : base(handler, instance)
            {
            }

            public virtual Builder WithId(Guid id)
            {
                Instance.Id = id;
                return this;
            }
            public virtual Builder WithName(string name)
            {
                Instance.TenantName = name;
                return this;
            }
            public virtual Builder WithIsActive(bool isActive)
            {
                Instance.IsActive = isActive;
                return this;
            }

            protected override void Specifications()
            {
                AddSpecification(new ExpressionSpecification<Tenant>(
                    AppConsts.LocalizationSourceName,
                    Tenant.EntityError.TenantMustHaveTenantName,
                    w => !string.IsNullOrWhiteSpace(w.TenantName)));

                AddSpecification(new ExpressionSpecification<Tenant>(
                    AppConsts.LocalizationSourceName,
                    Tenant.EntityError.TenantOutOfBoundTenantName,
                    w => string.IsNullOrWhiteSpace(w.TenantName) || w.TenantName.Length > 0 && w.TenantName.Length <= 50));

            }
        }
    }
}
