﻿using System;
using System.Collections.Generic;
using Thex.Common;
using Thex.Common.Enumerations;
using Tnf.Builder;
using Tnf.Notifications;
using Tnf.Specifications;

namespace Thex.SuperAdmin.Infra.Entities
{
    public partial class Person
    {
        public class Builder : Builder<Person>
        {
            public Builder(INotificationHandler handler) : base(handler)
            {
            }

            public Builder(INotificationHandler handler, Person instance) : base(handler, instance)
            {
            }

            public virtual Builder WithId(Guid id)
            {
                Instance.Id = (id == Guid.Empty) ? Guid.NewGuid() : id;
                return this;
            }
            public virtual Builder WithPersonType(string personType)
            {
                Instance.PersonType = personType;
                return this;
            }
            public virtual Builder WithPersonType(PersonTypeEnum personType)
            {
                Instance.PersonTypeValue = personType;
                return this;
            }
            public virtual Builder WithFirstName(string firstName)
            {
                Instance.FirstName = firstName;
                return this;
            }
            public virtual Builder WithLastName(string lastName)
            {
                Instance.LastName = lastName;
                return this;
            }
            public virtual Builder WithFullName(string fullName)
            {
                Instance.FullName = fullName;
                return this;
            }
            public virtual Builder WithDateOfBirth(DateTime? dateOfBirth)
            {
                Instance.DateOfBirth = dateOfBirth;
                return this;
            }
            public virtual Builder WithCountrySubdivisionId(int? countrySubdivisionId)
            {
                Instance.CountrySubdivisionId = countrySubdivisionId;
                return this;
            }

            public virtual Builder WithNickName(string nickName)
            {
                Instance.NickName = nickName;
                return this;
            }

            public virtual Builder WithPhotoUrl(string photoUrl)
            {
                Instance.PhotoUrl = photoUrl;
                return this;
            }

            public virtual Builder WithContactInformationList(ICollection<ContactInformation> contactInformationList)
            {
                Instance.ContactInformationList = contactInformationList;
                return this;
            }

            public virtual Builder WithDocumentList(ICollection<Document> documentList)
            {
                Instance.DocumentList = documentList;
                return this;
            }

            public DateTime? DateOfBirth { get; internal set; }
            protected override void Specifications()
            {

                AddSpecification(new ExpressionSpecification<Person>(
                    AppConsts.LocalizationSourceName,
                    Person.EntityError.PersonMustHavePersonType,
                    w => !string.IsNullOrWhiteSpace(w.PersonType)));

                AddSpecification(new ExpressionSpecification<Person>(
                    AppConsts.LocalizationSourceName,
                    Person.EntityError.PersonOutOfBoundPersonType,
                    w => string.IsNullOrWhiteSpace(w.PersonType) || w.PersonType.Length > 0 && w.PersonType.Length <= 1));

                AddSpecification(new ExpressionSpecification<Person>(
                    AppConsts.LocalizationSourceName,
                    Person.EntityError.PersonMustHaveFirstName,
                    w => !string.IsNullOrWhiteSpace(w.FirstName)));

                AddSpecification(new ExpressionSpecification<Person>(
                    AppConsts.LocalizationSourceName,
                    Person.EntityError.PersonOutOfBoundFirstName,
                    w => string.IsNullOrWhiteSpace(w.FirstName) || w.FirstName.Length > 0 && w.FirstName.Length <= 200));

                AddSpecification(new ExpressionSpecification<Person>(
                    AppConsts.LocalizationSourceName,
                    Person.EntityError.PersonOutOfBoundLastName,
                    w => string.IsNullOrWhiteSpace(w.LastName) || w.LastName.Length > 0 && w.LastName.Length <= 200));

                AddSpecification(new ExpressionSpecification<Person>(
                    AppConsts.LocalizationSourceName,
                    Person.EntityError.PersonOutOfBoundFullName,
                    w => string.IsNullOrWhiteSpace(w.FullName) || w.FullName.Length > 0 && w.FullName.Length <= 500));

                AddSpecification(new ExpressionSpecification<Person>(
                    AppConsts.LocalizationSourceName,
                    Person.EntityError.PersonInvalidDateOfBirth,
                    w => w.DateOfBirth == null || w.DateOfBirth >= new DateTime(1753, 1, 1) && w.DateOfBirth <= DateTime.MaxValue));

            }
        }
    }
}

