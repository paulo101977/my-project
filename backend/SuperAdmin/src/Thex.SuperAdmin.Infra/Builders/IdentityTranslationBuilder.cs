﻿using System;
using System.Collections.Generic;
using Thex.Common;
using Tnf.Builder;
using Tnf.Notifications;
using Tnf.Specifications;

namespace Thex.SuperAdmin.Infra.Entities
{
    public partial class IdentityTranslation
    {
    public class Builder : Builder<IdentityTranslation>
    {
		public Builder(INotificationHandler handler) : base(handler)
		{
		}

		public Builder(INotificationHandler handler, IdentityTranslation instance) : base(handler, instance)
		{
		}
		
        public virtual Builder WithId(Guid id)
        {
            Instance.Id = id;
            return this;
        }
        public virtual Builder WithLanguageIsoCode(string languageIsoCode)
        {
            Instance.LanguageIsoCode = languageIsoCode;
            return this;
        }
        public virtual Builder WithName(string name)
        {
            Instance.Name = name;
            return this;
        }
        public virtual Builder WithDescription(string description)
        {
            Instance.Description = description;
            return this;
        }
        public virtual Builder WithIdentityOwnerId(Guid identityOwnerId)
        {
            Instance.IdentityOwnerId = identityOwnerId;
            return this;
        }

        protected override void Specifications()
        {
            AddSpecification(new ExpressionSpecification<IdentityTranslation>(
				AppConsts.LocalizationSourceName,
                IdentityTranslation.EntityError.IdentityTranslationMustHaveLanguageIsoCode, 
				w => !string.IsNullOrWhiteSpace(w.LanguageIsoCode)));

            AddSpecification(new ExpressionSpecification<IdentityTranslation>(
				AppConsts.LocalizationSourceName,
                IdentityTranslation.EntityError.IdentityTranslationOutOfBoundLanguageIsoCode, 
				w => string.IsNullOrWhiteSpace(w.LanguageIsoCode) || w.LanguageIsoCode.Length > 0 && w.LanguageIsoCode.Length <= 5));
        }
    }
}
}
