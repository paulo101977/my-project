﻿using System;
using System.Collections.Generic;
using Thex.Common;
using Tnf.Builder;
using Tnf.Notifications;
using Tnf.Specifications;

namespace Thex.SuperAdmin.Infra.Entities
{
    public partial class Brand
    {
        public class Builder : Builder<Brand>
        {
            public Builder(INotificationHandler handler) : base(handler)
            {
            }

            public Builder(INotificationHandler handler, Brand instance) : base(handler, instance)
            {
            }

            public virtual Builder WithId(int id)
            {
                Instance.Id = id;
                return this;
            }
            public virtual Builder WithChainId(int chainId)
            {
                Instance.ChainId = chainId;
                return this;
            }
            public virtual Builder WithName(string name)
            {
                Instance.Name = name;
                return this;
            }
            public virtual Builder WithIsTemporary(bool isTemporary)
            {
                Instance.IsTemporary = isTemporary;
                return this;
            }
            public virtual Builder WithTenant(Guid tenantId)
            {
                Instance.TenantId = tenantId;
                return this;
            }

            protected override void Specifications()
            {
                AddSpecification(new ExpressionSpecification<Brand>(
                    AppConsts.LocalizationSourceName,
                    Brand.EntityError.BrandMustHaveChainId,
                    w => w.ChainId != default(int)));

                AddSpecification(new ExpressionSpecification<Brand>(
                   AppConsts.LocalizationSourceName,
                   Brand.EntityError.BrandMustHaveTenantId,
                   w => w.TenantId != Guid.Empty));

                AddSpecification(new ExpressionSpecification<Brand>(
                    AppConsts.LocalizationSourceName,
                    Brand.EntityError.BrandMustHaveName,
                    w => !string.IsNullOrWhiteSpace(w.Name)));

                AddSpecification(new ExpressionSpecification<Brand>(
                    AppConsts.LocalizationSourceName,
                    Brand.EntityError.BrandOutOfBoundName,
                    w => string.IsNullOrWhiteSpace(w.Name) || w.Name.Length > 0 && w.Name.Length <= 50));

            }
        }
    }
}
