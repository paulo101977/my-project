﻿using System;
using System.Collections.Generic;
using Thex.Common;
using Thex.Common.Enumerations;
using Tnf.Builder;
using Tnf.Notifications;
using Tnf.Specifications;

namespace Thex.SuperAdmin.Infra.Entities
{
    public partial class Property
    {
        public class Builder : Builder<Property>
        {
            public Builder(INotificationHandler handler) : base(handler)
            {
            }

            public Builder(INotificationHandler handler, Property instance) : base(handler, instance)
            {
            }

            public virtual Builder WithId(int id)
            {
                Instance.Id = id;
                return this;
            }
            public virtual Builder WithCompanyId(int companyId)
            {
                Instance.CompanyId = companyId;
                return this;
            }
            public virtual Builder WithPropertyTypeId(PropertyTypeEnum propertyTypeId)
            {
                Instance.PropertyTypeIdValue = propertyTypeId;
                return this;
            }
            public virtual Builder WithBrandId(int brandId)
            {
                Instance.BrandId = brandId;
                return this;
            }
            public virtual Builder WithName(string name)
            {
                Instance.Name = name;
                return this;
            }
            public virtual Builder WithPropertyUId(Guid propertyUId)
            {
                Instance.PropertyUId = propertyUId;
                return this;
            }
            public virtual Builder GeneratePropertyUId()
            {
                Instance.PropertyUId = Guid.NewGuid();
                return this;
            }

            public virtual Builder WithPropertyStatusId(int propertyStatusId)
            {
                Instance.PropertyStatusId = propertyStatusId;
                return this;
            }

            public virtual Builder WithTenantId(Guid tenantId)
            {
                Instance.TenantId = tenantId;
                return this;
            }

            //public virtual Builder WithPhoto(string photo)
            //{
            //    Instance.Photo = photo;
            //    return this;
            //}

            public virtual Builder WithIsBlocked(bool isBlocked)
            {
                Instance.IsBlocked = isBlocked;
                return this;
            }

            protected override void Specifications()
            {
                AddSpecification(new ExpressionSpecification<Property>(
                    AppConsts.LocalizationSourceName,
                    Property.EntityError.PropertyMustHaveCompanyId,
                    w => w.CompanyId != default(int)));

                AddSpecification(new ExpressionSpecification<Property>(
                    AppConsts.LocalizationSourceName,
                    Property.EntityError.PropertyMustHavePropertyTypeId,
                    w => w.PropertyTypeId != default(int)));

                AddSpecification(new ExpressionSpecification<Property>(
                    AppConsts.LocalizationSourceName,
                    Property.EntityError.PropertyMustHaveBrandId,
                    w => w.BrandId != default(int)));

                AddSpecification(new ExpressionSpecification<Property>(
                    AppConsts.LocalizationSourceName,
                    Property.EntityError.PropertyMustHaveName,
                    w => !string.IsNullOrWhiteSpace(w.Name)));

                AddSpecification(new ExpressionSpecification<Property>(
                    AppConsts.LocalizationSourceName,
                    Property.EntityError.PropertyOutOfBoundName,
                    w => string.IsNullOrWhiteSpace(w.Name) || w.Name.Length > 0 && w.Name.Length <= 200));

            }
        }
    }
}
