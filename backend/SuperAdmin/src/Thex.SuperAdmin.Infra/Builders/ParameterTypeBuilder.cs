﻿using System;
using System.Collections.Generic;
using Thex.Common;
using Tnf.Builder;
using Tnf.Notifications;
using Tnf.Specifications;

namespace Thex.SuperAdmin.Infra.Entities
{
    public partial class ParameterType
    {
        public class Builder : Builder<ParameterType>
        {
            public Builder(INotificationHandler handler) : base(handler)
            {
            }

            public Builder(INotificationHandler handler, ParameterType instance) : base(handler, instance)
            {
            }

            public virtual Builder WithId(int id)
            {
                Instance.Id = id;
                return this;
            }
            public virtual Builder WithParameterTypeName(string parameterTypeName)
            {
                Instance.ParameterTypeName = parameterTypeName;
                return this;
            }

            protected override void Specifications()
            {
                AddSpecification(new ExpressionSpecification<ParameterType>(
                    AppConsts.LocalizationSourceName,
                    ParameterType.EntityError.ParameterTypeMustHaveParameterTypeName,
                    w => !string.IsNullOrWhiteSpace(w.ParameterTypeName)));

                AddSpecification(new ExpressionSpecification<ParameterType>(
                    AppConsts.LocalizationSourceName,
                    ParameterType.EntityError.ParameterTypeOutOfBoundParameterTypeName,
                    w => string.IsNullOrWhiteSpace(w.ParameterTypeName) || w.ParameterTypeName.Length > 0 && w.ParameterTypeName.Length <= 100));

            }
        }
    }
}
