﻿using Thex.SuperAdmin.Infra.Context;
using Thex.SuperAdmin.Infra.Interfaces;

namespace Thex.SuperAdmin.Infra.Uow
{
    public class SimpleUnitOfWork : ISimpleUnitOfWork
    {
        private readonly ThexIdentityDbContext _context;

        public SimpleUnitOfWork(ThexIdentityDbContext context)
        {
            _context = context;
        }

        public bool Commit()
        {
            return _context.SaveChanges() > 0;
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}