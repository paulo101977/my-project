﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Thex.SuperAdmin.Infra.Entities;

namespace Thex.SuperAdmin.Infra.Mappers
{
    public class PropertyStatusHistoryMapper : IEntityTypeConfiguration<PropertyStatusHistory>
    {
        public void Configure(EntityTypeBuilder<PropertyStatusHistory> builder)
        {
            builder.ToTable("PropertyStatusHistory");

            builder.HasAnnotation("Relational:TableName", "PropertyStatusHistory");

            builder.HasKey(e => e.Id);
            builder.Property(e => e.Id).HasAnnotation("Relational:ColumnName", "PropertyStatusHistoryId");

            builder.HasOne(p=> p.Property)
                .WithMany(property => property.PropertyStatusHistoryList)
                .HasForeignKey(d => d.PropertyId)
                .HasConstraintName("FK_PropertyStatusHistory_Property");
        }
    }
}
