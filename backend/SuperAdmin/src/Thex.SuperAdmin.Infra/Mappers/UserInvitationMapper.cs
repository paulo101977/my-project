﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Thex.SuperAdmin.Infra.Entities;

namespace Thex.SuperAdmin.Infra.Mappers
{

    public class UserInvitationMapper : IEntityTypeConfiguration<UserInvitation>
    {
        public void Configure(EntityTypeBuilder<UserInvitation> builder)
        {

            builder.ToTable("UserInvitation");

            builder.HasAnnotation("Relational:TableName", "UserInvitation");

            builder.HasIndex(e => e.TenantId)
                .HasName("x_UserInvitation_TenantId");

            builder.Property(e => e.Id)
                .HasAnnotation("Relational:ColumnName", "UserInvitationId");

            builder.Property(e => e.ChainId).HasAnnotation("Relational:ColumnName", "ChainId");

            builder.Property(e => e.CreationTime)
                .HasColumnType("datetime")
                .HasDefaultValueSql("(getutcdate())")
                .HasAnnotation("Relational:ColumnName", "CreationTime");

            builder.Property(e => e.CreatorUserId).HasAnnotation("Relational:ColumnName", "CreatorUserId");

            builder.Property(e => e.DeleterUserId).HasAnnotation("Relational:ColumnName", "DeleterUserId");

            builder.Property(e => e.DeletionTime)
                .HasColumnType("datetime")
                .HasAnnotation("Relational:ColumnName", "DeletionTime");

            builder.Property(e => e.Email)
                .IsRequired()
                .HasMaxLength(600)
                .IsUnicode(false)
                .HasAnnotation("Relational:ColumnName", "Email");

            builder.Property(e => e.InvitationAcceptanceDate)
                .HasColumnType("datetime")
                .HasAnnotation("Relational:ColumnName", "InvitationAcceptanceDate");

            builder.Property(e => e.InvitationDate)
                .HasColumnType("datetime")
                .HasAnnotation("Relational:ColumnName", "InvitationDate");

            builder.Property(e => e.InvitationLink)
                .IsRequired()
                .HasMaxLength(4000)
                .IsUnicode(false)
                .HasAnnotation("Relational:ColumnName", "InvitationLink");

            builder.Property(e => e.IsActive).HasAnnotation("Relational:ColumnName", "IsActive").HasDefaultValue(true);

            builder.Property(e => e.IsDeleted)
                .IsRequired()
                .HasAnnotation("Relational:ColumnName", "IsDeleted")
                .HasDefaultValue(false);

            builder.Property(e => e.LastModificationTime)
                .HasColumnType("datetime")
                .HasDefaultValueSql("(getutcdate())")
                .HasAnnotation("Relational:ColumnName", "LastModificationTime");

            builder.Property(e => e.LastModifierUserId).HasAnnotation("Relational:ColumnName", "LastModifierUserId");

            builder.Property(e => e.Name)
                .IsRequired()
                .HasMaxLength(300)
                .IsUnicode(false)
                .HasAnnotation("Relational:ColumnName", "Name");

            builder.Property(e => e.PropertyId).HasAnnotation("Relational:ColumnName", "PropertyId");

            builder.Property(e => e.TenantId).HasAnnotation("Relational:ColumnName", "TenantId");

            builder.HasOne(d => d.Chain)
                .WithMany(p => p.UserInvitationList)
                .HasForeignKey(d => d.ChainId)
                .HasConstraintName("FK_UserInvitation_Chain");

            builder.HasOne(d => d.Property)
                .WithMany(p => p.UserInvitationList)
                .HasForeignKey(d => d.PropertyId)
                .HasConstraintName("FK_UserInvitation_Property");

            builder.HasOne(d => d.Brand)
                .WithMany(p => p.UserInvitationList)
                .HasForeignKey(d => d.BrandId)
                .HasConstraintName("FK_UserInvitation_Brand");

            builder.Property(e => e.UserId).HasAnnotation("Relational:ColumnName", "UserId");

        }
    }
}
