﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Thex.SuperAdmin.Infra.Entities;

namespace Thex.SuperAdmin.Infra.Mappers
{
    public class PersonMapper : IEntityTypeConfiguration<Person>
    {
        public void Configure(EntityTypeBuilder<Person> builder)
        {
            
            builder.ToTable("Person");

            builder.HasAnnotation("Relational:TableName", "Person");

            builder.HasIndex(e => e.PersonType)
                .HasName("x_Person_PersonType");

            builder.HasKey(e => e.Id);

            builder.Property(e => e.Id)
                .HasAnnotation("Relational:ColumnName", "PersonId");

            builder.Property(e => e.CountrySubdivisionId).HasAnnotation("Relational:ColumnName", "CountrySubdivisionId");

            builder.Property(e => e.DateOfBirth)
                .HasColumnType("date")
                .HasAnnotation("Relational:ColumnName", "DateOfBirth");

            builder.Property(e => e.FirstName)
                .IsRequired()
                .HasMaxLength(200)
                .IsUnicode(false)
                .HasAnnotation("Relational:ColumnName", "FirstName");

            builder.Property(e => e.FullName)
                .HasMaxLength(500)
                .IsUnicode(false)
                .HasAnnotation("Relational:ColumnName", "FullName");

            builder.Property(e => e.LastName)
                .HasMaxLength(200)
                .IsUnicode(false)
                .HasAnnotation("Relational:ColumnName", "LastName");

            builder.Property(e => e.PersonType)
                .IsRequired()
                .HasColumnType("char(1)")
                .HasAnnotation("Relational:ColumnName", "PersonType");

            builder.HasMany(x => x.DocumentList);

            builder.Ignore(e => e.PersonTypeValue);
            builder.Ignore(e => e.Company);
            
        }
    }
}
