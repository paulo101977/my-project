﻿using AutoMapper;
using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Infra.Entities;

namespace Thex.SuperAdmin.Infra.Mappers.Profiles
{
    public class MenuProfile : Profile
    {
        public MenuProfile()
        {
            CreateMap<MenuItemDto, MenuItemSimpleDto>()
                .AfterMap((menuItemEntity, menuItemDto) =>
                {
                    menuItemDto.Name = menuItemEntity.FeatureName;
                    menuItemDto.Expandables = null;

                    if(menuItemDto.Children != null && menuItemDto.Children.Count == 0)
                        menuItemDto.Children = null;
                });
        }
    }
}