﻿using AutoMapper;
using System;
using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Infra.Entities;

namespace Thex.SuperAdmin.Infra.Mappers.Profiles
{
    public class IdentityRolePermissionProfile : Profile
    {
        public IdentityRolePermissionProfile()
        {
            CreateMap<IdentityRolePermission, IdentityRolePermissionDto>()
                .ForMember(d => d.IdentityPermission, s => s.Ignore())
                .AfterMap((rolePermissionEntity, rolePermissionDto) =>
                {
                    if (rolePermissionEntity.IdentityPermission != null)
                        rolePermissionDto.IdentityPermission = rolePermissionEntity.IdentityPermission.MapTo<IdentityPermissionDto>();
                });
        }

    }
}
