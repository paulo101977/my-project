﻿using AutoMapper;
using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Infra.Entities;

namespace Thex.SuperAdmin.Infra.Mappers.Profiles
{
    public class IdentityMenuFeatureProfile : Profile
    {
        public IdentityMenuFeatureProfile()
        {
            CreateMap<IdentityMenuFeature, IdentityMenuFeatureDto>()
                .ForMember(d => d.ParentMenuList, s => s.Ignore());
        }

    }
}
