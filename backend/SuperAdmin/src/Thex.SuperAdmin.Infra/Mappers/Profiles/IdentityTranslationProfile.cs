﻿using AutoMapper;
using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Infra.Entities;

namespace Thex.SuperAdmin.Infra.Mappers.Profiles
{
    public class IdentityTranslationProfile : Profile
    {
        public IdentityTranslationProfile()
        {
            CreateMap<IdentityTranslation, IdentityTranslationDto>();
        }
    }
}