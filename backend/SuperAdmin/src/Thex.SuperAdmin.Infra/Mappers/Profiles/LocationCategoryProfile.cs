﻿using AutoMapper;
using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Infra.Entities;

namespace Thex.SuperAdmin.Infra.Mappers.Profiles
{
    public class LocationCategoryProfile : Profile
    {
        public LocationCategoryProfile()
        {
            CreateMap<LocationCategory, LocationCategoryDto>();
        }
    }
}