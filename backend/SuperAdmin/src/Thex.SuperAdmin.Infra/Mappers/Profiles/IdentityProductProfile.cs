﻿using AutoMapper;
using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Infra.Entities;

namespace Thex.SuperAdmin.Infra.Mappers.Profiles
{
    public class IdentityProductProfile : Profile
    {
        public IdentityProductProfile()
        {
            CreateMap<IdentityProduct, IdentityProductDto>();
        }

    }
}
