﻿using AutoMapper;
using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Infra.Entities;

namespace Thex.SuperAdmin.Infra.Mappers.Profiles
{
    public class ContactInformationTypeProfile : Profile
    {
        public ContactInformationTypeProfile()
        {
            CreateMap<ContactInformationType, ContactInformationTypeDto>();
        }
    }
}