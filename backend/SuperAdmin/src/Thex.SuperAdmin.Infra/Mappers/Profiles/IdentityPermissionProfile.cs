﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Infra.Entities;

namespace Thex.SuperAdmin.Infra.Mappers.Profiles
{
    public class IdentityPermissionProfile : Profile
    {
        public IdentityPermissionProfile()
        {
            CreateMap<IdentityPermission, IdentityPermissionDto>()
                .ForMember(d => d.IdentityPermissionFeatureList, s => s.Ignore())
                .AfterMap((permissionEntity, permissionDto) =>
                {
                    if (permissionEntity.IdentityModule != null)
                        permissionDto.IdentityModuleName = permissionEntity.IdentityModule.Name;

                    if (permissionEntity.IdentityProduct != null)
                        permissionDto.IdentityProductName = permissionEntity.IdentityProduct.Name;

                    if (permissionEntity.IdentityPermissionFeatureList.ContainsElement())
                    {
                        permissionDto.IdentityPermissionFeatureList = new List<IdentityPermissionFeatureDto>();

                        foreach (var permissionFeatureEntity in permissionEntity.IdentityPermissionFeatureList)
                            permissionDto.IdentityPermissionFeatureList.Add(permissionFeatureEntity.MapTo<IdentityPermissionFeatureDto>());
                    }
                });
        }
    }
}
