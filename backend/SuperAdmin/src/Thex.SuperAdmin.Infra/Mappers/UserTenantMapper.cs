﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Thex.SuperAdmin.Infra.Entities;

namespace Thex.SuperAdmin.Infra.Mappers
{
    public class UserTenantMapper : IEntityTypeConfiguration<UserTenant>
    {
        public void Configure(EntityTypeBuilder<UserTenant> builder)
        {
            builder.ToTable("UserTenant");

            builder.HasAnnotation("Relational:TableName", "UserTenant");

            builder.HasIndex(e => e.TenantId)
                .HasName("x_UserTenant_TenantId");

            builder.HasIndex(e => e.UserId)
                .HasName("x_UserTenant_UserId");

            builder.HasKey(e => e.Id);
            builder.Property(e => e.Id).HasAnnotation("Relational:ColumnName", "UserTenantId");

            builder.Property(e => e.IsActive).HasAnnotation("Relational:ColumnName", "IsActive");

            builder.HasOne(u => u.Tenant)
              .WithMany(t => t.UserTenantList)
              .HasForeignKey(u => u.TenantId)
              .HasConstraintName("FK_UserTenant_Tenant");

            builder.HasOne(u => u.User)
             .WithMany(t => t.UserTenantList)
             .HasForeignKey(u => u.UserId)
             .HasConstraintName("FK_UserTenant_Users");
        }
    }
}