﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Thex.SuperAdmin.Infra.Entities;

namespace Thex.SuperAdmin.Infra.Mappers
{
    public class CompanyMapper : IEntityTypeConfiguration<Company>
    {
        public void Configure(EntityTypeBuilder<Company> builder)
        {
            
            builder.ToTable("Company");

            builder.HasAnnotation("Relational:TableName", "Company");

            builder.HasIndex(e => e.ParentCompanyId)
                .HasName("x_Company_ParentCompanyId");

            builder.HasIndex(e => e.PersonId)
                .HasName("x_Company_PersonId");

            builder.HasKey(e => e.Id);
            builder.Property(e => e.Id).HasAnnotation("Relational:ColumnName", "CompanyId").ValueGeneratedOnAdd();

            builder.Property(e => e.ParentCompanyId).HasAnnotation("Relational:ColumnName", "ParentCompanyId");

            builder.Property(e => e.PersonId).HasAnnotation("Relational:ColumnName", "PersonId");

            builder.Property(e => e.CompanyUid).HasAnnotation("Relational:ColumnName", "CompanyUid");

            builder.Property(e => e.ShortName)
                .IsRequired()
                .HasMaxLength(50)
                .IsUnicode(false)
                .HasAnnotation("Relational:ColumnName", "ShortName");

            builder.Property(e => e.TradeName)
                .IsRequired()
                .HasMaxLength(200)
                .IsUnicode(false)
                .HasAnnotation("Relational:ColumnName", "TradeName");

            builder.HasOne(d => d.ParentCompany)
                .WithMany(p => p.InverseParentCompanyList)
                .HasForeignKey(d => d.ParentCompanyId)
                .HasConstraintName("FK_Company_Company");

            builder.HasOne(d => d.Person)
                .WithMany(p => p.CompanyList)
                .HasForeignKey(d => d.PersonId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_Company_Person");

        }
    }
}
