﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Thex.SuperAdmin.Infra.Entities;

namespace Thex.SuperAdmin.Infra.Mappers
{
    public class RoomMapper : IEntityTypeConfiguration<Room>
    {
        public void Configure(EntityTypeBuilder<Room> builder)
        {
            builder.ToTable("Room");

            builder.HasAnnotation("Relational:TableName", "Room");

            builder.HasKey(e => e.Id);
            builder.Property(e => e.Id).HasAnnotation("Relational:ColumnName", "Room");
        }
    }
}
