﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Thex.SuperAdmin.Infra.Entities;

namespace Thex.SuperAdmin.Infra.Mappers
{
    public class StatusCategoryMapper : IEntityTypeConfiguration<StatusCategory>
    {
        public void Configure(EntityTypeBuilder<StatusCategory> builder)
        {
            
            builder.ToTable("StatusCategory");

            builder.HasAnnotation("Relational:TableName", "StatusCategory");

            builder.Property(e => e.Id)
                .ValueGeneratedNever()
                .HasAnnotation("Relational:ColumnName", "StatusCategoryId");

            builder.Property(e => e.Name)
                .IsRequired()
                .HasMaxLength(40)
                .IsUnicode(false)
                .HasAnnotation("Relational:ColumnName", "Name");

        }
    }
}
