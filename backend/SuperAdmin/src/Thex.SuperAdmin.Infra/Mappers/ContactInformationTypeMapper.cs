﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Thex.SuperAdmin.Infra.Entities;

namespace Thex.SuperAdmin.Infra.Mappers
{
    public class ContactInformationTypeMapper : IEntityTypeConfiguration<ContactInformationType>
    {
        public void Configure(EntityTypeBuilder<ContactInformationType> builder)
        {
            
            builder.ToTable("ContactInformationType");

            builder.HasAnnotation("Relational:TableName", "ContactInformationType");

            builder.Property(e => e.Id)
                .ValueGeneratedNever()
                .HasAnnotation("Relational:ColumnName", "ContactInformationTypeId");

            builder.Property(e => e.Name)
                .IsRequired()
                .HasMaxLength(50)
                .IsUnicode(false)
                .HasAnnotation("Relational:ColumnName", "Name");

            builder.Property(e => e.RegexValidationExpression)
                .HasMaxLength(500)
                .IsUnicode(false)
                .HasAnnotation("Relational:ColumnName", "RegexValidationExpression");

            builder.Property(e => e.StringFormatMask)
                .HasMaxLength(100)
                .IsUnicode(false)
                .HasAnnotation("Relational:ColumnName", "StringFormatMask");

        }
    }
}
