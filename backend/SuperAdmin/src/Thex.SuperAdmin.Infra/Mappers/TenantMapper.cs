﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Thex.SuperAdmin.Infra.Entities;

namespace Thex.SuperAdmin.Infra.Mappers
{

    public class TenantMapper : IEntityTypeConfiguration<Tenant>
    {
        public void Configure(EntityTypeBuilder<Tenant> builder)
        {

            builder.ToTable("Tenant");

            builder.HasAnnotation("Relational:TableName", "Tenant");

            builder.HasIndex(e => e.Id)
                .HasName("x_Tenant_ParentId");

            builder.HasIndex(e => e.TenantName)
                .HasName("UK_Tenant")
                .IsUnique();

            builder.Property(e => e.Id)
                .HasAnnotation("Relational:ColumnName", "TenantId");

            //builder.Property(e => e.ChainId).HasAnnotation("Relational:ColumnName", "ChainId");

            //builder.Property(e => e.PropertyId).HasAnnotation("Relational:ColumnName", "PropertyId");

            builder.Property(e => e.CreationTime)
                .HasColumnType("datetime")
                .HasDefaultValueSql("(getutcdate())")
                .HasAnnotation("Relational:ColumnName", "CreationTime");

            builder.Property(e => e.CreatorUserId).HasAnnotation("Relational:ColumnName", "CreatorUserId");

            builder.Property(e => e.DeleterUserId).HasAnnotation("Relational:ColumnName", "DeleterUserId");

            builder.Property(e => e.DeletionTime)
                .HasColumnType("datetime")
                .HasAnnotation("Relational:ColumnName", "DeletionTime");

            builder.Property(e => e.IsActive).HasAnnotation("Relational:ColumnName", "IsActive");

            builder.Property(e => e.IsDeleted).HasAnnotation("Relational:ColumnName", "IsDeleted");

            builder.Property(e => e.LastModificationTime)
                .HasColumnType("datetime")
                .HasDefaultValueSql("(getutcdate())")
                .HasAnnotation("Relational:ColumnName", "LastModificationTime");

            builder.Property(e => e.LastModifierUserId).HasAnnotation("Relational:ColumnName", "LastModifierUserId");

            builder.Property(e => e.ParentId).HasAnnotation("Relational:ColumnName", "ParentId");

            builder.Property(e => e.TenantName)
                .IsRequired()
                .HasMaxLength(50)
                .IsUnicode(false)
                .HasAnnotation("Relational:ColumnName", "TenantName");


            builder.HasOne(d => d.Parent)
                .WithMany(p => p.ChildList)
                .HasForeignKey(d => d.ParentId)
                .HasConstraintName("FK_Tenant_Tenant");

            builder.HasOne(d => d.Chain)
                .WithOne(p => p.Tenant)
                .HasForeignKey<Chain>(d => d.TenantId)
                .HasConstraintName("FK_Tenant_Chain");

            builder.HasOne(d => d.Property)
                .WithOne(p => p.Tenant)
                .HasForeignKey<Property>(d => d.TenantId)
                .HasConstraintName("FK_Tenant_Property");

            builder.HasOne(d => d.Brand)
                .WithOne(p => p.Tenant)
                .HasForeignKey<Brand>(d => d.TenantId)
                .HasConstraintName("FK_Tenant_Brand");
        }
    }
}
