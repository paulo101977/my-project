﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Thex.SuperAdmin.Infra.Entities;

namespace Thex.SuperAdmin.Infra.Mappers
{
    public class PropertyContractMapper : IEntityTypeConfiguration<PropertyContract>
    {
        public void Configure(EntityTypeBuilder<PropertyContract> builder)
        {
            builder.ToTable("PropertyContract");

            builder.HasAnnotation("Relational:TableName", "PropertyContract");

            builder.HasKey(e => e.Id);
            builder.Property(e => e.Id).HasAnnotation("Relational:ColumnName", "PropertyContractId");

            builder.HasOne(p => p.Property)
                .WithMany(property => property.PropertyContractList)
                .HasForeignKey(d => d.PropertyId)
                .HasConstraintName("FK_PropertyContract_Property");
        }
    }
}
