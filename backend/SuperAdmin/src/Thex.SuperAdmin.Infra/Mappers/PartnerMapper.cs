﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Thex.SuperAdmin.Infra.Entities;

namespace Thex.SuperAdmin.Infra.Mappers
{
    public class PartnerMapper : IEntityTypeConfiguration<Partner>
    {
        public void Configure(EntityTypeBuilder<Partner> builder)
        {
            builder.ToTable("Partner");

            builder.HasAnnotation("Relational:TableName", "Partner");

            builder.HasKey(e => e.Id);

            builder.Property(e => e.Id)
              .HasAnnotation("Relational:ColumnName", "PartnerId");
        }
    }
}
