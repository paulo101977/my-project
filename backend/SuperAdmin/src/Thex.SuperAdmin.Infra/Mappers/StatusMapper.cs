﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Thex.SuperAdmin.Infra.Entities;

namespace Thex.SuperAdmin.Infra.Mappers
{
    public class StatusMapper : IEntityTypeConfiguration<Status>
    {
        public void Configure(EntityTypeBuilder<Status> builder)
        {
            builder.ToTable("Status");

            builder.HasAnnotation("Relational:TableName", "Status");

            builder.HasIndex(e => e.StatusCategoryId)
                .HasName("x_Status_StatusCategoryId");

            builder.Property(e => e.Id)
                .ValueGeneratedNever()
                .HasAnnotation("Relational:ColumnName", "StatusId");

            builder.Property(e => e.Name)
                .IsRequired()
                .HasMaxLength(20)
                .IsUnicode(false)
                .HasAnnotation("Relational:ColumnName", "Name");

            builder.Property(e => e.StatusCategoryId).HasAnnotation("Relational:ColumnName", "StatusCategoryId");

            builder.HasOne(d => d.StatusCategory)
                .WithMany(p => p.StatusList)
                .HasForeignKey(d => d.StatusCategoryId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_Status_StatusCategory");

        }
    }
}
