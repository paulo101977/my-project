﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Thex.SuperAdmin.Infra.Entities;

namespace Thex.SuperAdmin.Infra.Mappers
{
    class HouseKeepingStatusPropertyMapper : IEntityTypeConfiguration<HouseKeepingStatusProperty>
    {
        public void Configure(EntityTypeBuilder<HouseKeepingStatusProperty> builder)
        {
            builder.ToTable("HouseKeepingStatusProperty");

            builder.HasAnnotation("Relational:TableName", "HouseKeepingStatusProperty");

            builder.Property(e => e.Id)
                .HasAnnotation("Relational:ColumnName", "HouseKeepingStatusPropertyId");

        }
    }
}
