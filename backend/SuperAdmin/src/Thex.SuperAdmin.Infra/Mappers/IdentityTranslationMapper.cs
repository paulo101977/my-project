﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Thex.SuperAdmin.Infra.Entities;

namespace Thex.SuperAdmin.Infra.Mappers
{
    public class IdentityTranslationMapper : IEntityTypeConfiguration<IdentityTranslation>
    {
        public void Configure(EntityTypeBuilder<IdentityTranslation> builder)
        {
            
            builder.ToTable("IdentityTranslation");

            builder.HasAnnotation("Relational:TableName", "IdentityTranslation");

            builder.Property(e => e.Id).HasAnnotation("Relational:ColumnName", "IdentityTranslationId");

            builder.Property(e => e.LanguageIsoCode)
                .IsRequired()
                .HasMaxLength(5)
                .IsUnicode(false)
                .HasAnnotation("Relational:ColumnName", "LanguageIsoCode");

            builder.Property(e => e.Name)
                .IsRequired()
                .HasMaxLength(500)
                .IsUnicode(false)
                .HasAnnotation("Relational:ColumnName", "Name");

            builder.Property(e => e.Description)
                .IsRequired()
                .HasMaxLength(5000)
                .IsUnicode(false)
                .HasAnnotation("Relational:ColumnName", "Description");

            builder.Property(e => e.IdentityOwnerId)
                .HasAnnotation("Relational:ColumnName", "IdentityOwnerId");
        }
    }
}
