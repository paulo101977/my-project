﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Thex.SuperAdmin.Infra.Entities;

namespace Thex.SuperAdmin.Infra.Mappers
{
    public class PlasticBrandPropertyMapper : IEntityTypeConfiguration<PlasticBrandProperty>
    {
        public void Configure(EntityTypeBuilder<PlasticBrandProperty> builder)
        {
            builder.ToTable("PlasticBrandProperty");

            builder.HasAnnotation("Relational:TableName", "PlasticBrandProperty");

            builder.HasKey(e => e.Id);

            builder.Property(e => e.Id)
              .HasAnnotation("Relational:ColumnName", "PlasticBrandPropertyId");
        }
    }
}
