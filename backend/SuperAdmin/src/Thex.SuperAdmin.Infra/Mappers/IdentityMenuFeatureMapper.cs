﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a Crudzilla tool.
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;
using Thex.SuperAdmin.Infra.Entities;

namespace Thex.SuperAdmin.Infra.Mappers
{
    public class IdentityMenuFeatureMapper : IEntityTypeConfiguration<IdentityMenuFeature>
    {
        public void Configure(EntityTypeBuilder<IdentityMenuFeature> builder)
        {
            
            builder.ToTable("IdentityMenuFeature");

            builder.HasAnnotation("Relational:TableName", "IdentityMenuFeature");

            builder.Property(e => e.Id)
                .ValueGeneratedNever()
                .HasAnnotation("Relational:ColumnName", "IdentityMenuFeatureId");

            builder.Property(e => e.CreationTime)
                .HasColumnType("datetime")
                .HasDefaultValueSql("(getutcdate())")
                .HasAnnotation("Relational:ColumnName", "CreationTime");

            builder.Property(e => e.CreatorUserId).HasAnnotation("Relational:ColumnName", "CreatorUserId");

            builder.Property(e => e.DeleterUserId).HasAnnotation("Relational:ColumnName", "DeleterUserId");

            builder.Property(e => e.DeletionTime)
                .HasColumnType("datetime")
                .HasAnnotation("Relational:ColumnName", "DeletionTime");

            builder.Property(e => e.Description)
                .HasMaxLength(400)
                .IsUnicode(false)
                .HasAnnotation("Relational:ColumnName", "Description");

            builder.Property(e => e.IdentityFeatureId).HasAnnotation("Relational:ColumnName", "IdentityFeatureId");

            builder.Property(e => e.IdentityModuleId).HasAnnotation("Relational:ColumnName", "IdentityModuleId");

            builder.Property(e => e.IdentityFeatureParentId).HasAnnotation("Relational:ColumnName", "IdentityFeatureParentId");

            builder.Property(e => e.IdentityProductId).HasAnnotation("Relational:ColumnName", "IdentityProductId");

            builder.Property(e => e.IsActive).HasAnnotation("Relational:ColumnName", "IsActive");

            builder.Property(e => e.IsDeleted).HasAnnotation("Relational:ColumnName", "IsDeleted");

            builder.Property(e => e.LastModificationTime)
                .HasColumnType("datetime")
                .HasDefaultValueSql("(getutcdate())")
                .HasAnnotation("Relational:ColumnName", "LastModificationTime");

            builder.Property(e => e.LastModifierUserId).HasAnnotation("Relational:ColumnName", "LastModifierUserId");

            builder.Property(e => e.OrderNumber).HasAnnotation("Relational:ColumnName", "OrderNumber");

            builder.Property(e => e.Level).HasAnnotation("Relational:ColumnName", "Level");

            builder.HasOne(d => d.IdentityFeature)
                .WithMany(p => p.IdentityMenuFeatureList)
                .HasForeignKey(d => d.IdentityFeatureId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_IdentityMenuFeature_IdentityFeature");

            builder.HasOne(d => d.IdentityModule)
                .WithMany(p => p.IdentityMenuFeatureList)
                .HasForeignKey(d => d.IdentityModuleId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_IdentityMenuFeature_IdentityModule");

            builder.HasOne(d => d.IdentityFeatureParent)
                .WithMany(p => p.IdentityMenuFeatureIdentityFeatureParentList)
                .HasForeignKey(d => d.IdentityFeatureParentId)
                .HasConstraintName("FK_IdentityMenuFeature_IdentityFeatureParent");

            builder.HasOne(d => d.IdentityProduct)
                .WithMany(p => p.IdentityMenuFeatureList)
                .HasForeignKey(d => d.IdentityProductId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_IdentityMenuFeature_IdentityProduct");

        }
    }
}
