﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Thex.SuperAdmin.Infra.Entities;

namespace Thex.SuperAdmin.Infra.Mappers
{
    public class FeatureGroupMapper : IEntityTypeConfiguration<FeatureGroup>
    {
        public void Configure(EntityTypeBuilder<FeatureGroup> builder)
        {
            
            builder.ToTable("FeatureGroup");

            builder.HasAnnotation("Relational:TableName", "FeatureGroup");

            builder.Property(e => e.Id)
                .ValueGeneratedNever()
                .HasAnnotation("Relational:ColumnName", "FeatureGroupId");

            builder.Property(e => e.Description)
                .IsRequired()
                .HasMaxLength(300)
                .IsUnicode(false)
                .HasAnnotation("Relational:ColumnName", "Description");

            builder.Property(e => e.FeatureGroupName)
                .IsRequired()
                .HasMaxLength(100)
                .IsUnicode(false)
                .HasAnnotation("Relational:ColumnName", "FeatureGroupName");

        }
    }
}
