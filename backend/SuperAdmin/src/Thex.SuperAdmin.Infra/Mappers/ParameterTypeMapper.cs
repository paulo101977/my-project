﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Thex.SuperAdmin.Infra.Entities;

namespace Thex.SuperAdmin.Infra.Mappers
{
    public class ParameterTypeMapper : IEntityTypeConfiguration<ParameterType>
    {
        public void Configure(EntityTypeBuilder<ParameterType> builder)
        {
            
            builder.ToTable("ParameterType");

            builder.HasAnnotation("Relational:TableName", "ParameterType");

            builder.Property(e => e.Id)
                .ValueGeneratedNever()
                .HasAnnotation("Relational:ColumnName", "ParameterTypeId");

            builder.Property(e => e.ParameterTypeName)
                .IsRequired()
                .HasMaxLength(100)
                .IsUnicode(false)
                .HasAnnotation("Relational:ColumnName", "ParameterTypeName");

        }
    }
}
