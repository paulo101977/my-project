﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Thex.SuperAdmin.Infra.Entities;

namespace Thex.SuperAdmin.Infra.Mappers
{
    public class PlasticBrandMapper : IEntityTypeConfiguration<PlasticBrand>
    {
        public void Configure(EntityTypeBuilder<PlasticBrand> builder)
        {
            builder.ToTable("PlasticBrand");

            builder.HasAnnotation("Relational:TableName", "PlasticBrand");

            builder.HasKey(e => e.Id);

            builder.Property(e => e.Id)
              .HasAnnotation("Relational:ColumnName", "PlasticBrandId");
        }
    }
}
