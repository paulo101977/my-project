﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Thex.SuperAdmin.Infra.Entities;

namespace Thex.SuperAdmin.Infra.Mappers
{
    public class IntegrationPartnerPropertyMapper : IEntityTypeConfiguration<IntegrationPartnerProperty>
    {
        public void Configure(EntityTypeBuilder<IntegrationPartnerProperty> builder)
        {

            builder.ToTable("IntegrationPartnerProperty");

            builder.HasAnnotation("Relational:TableName", "IntegrationPartnerProperty");

            builder.HasKey(e => e.Id);

            builder.Property(e => e.Id)
              .HasAnnotation("Relational:ColumnName", "IntegrationPartnerPropertyId");

            builder.HasOne(ipp => ipp.Property)
                .WithMany(p => p.IntegrationPartnerPropertyList)
                .HasForeignKey(ipp => ipp.PropertyId);

            builder.HasOne(ipp => ipp.Partner)
              .WithMany(p => p.IntegrationPartnerPropertyList)
              .HasForeignKey(ipp => ipp.PartnerId);

            builder.HasOne(ipp => ipp.IntegrationPartner)
             .WithOne(ip => ip.IntegrationPartnerProperty);
        }
    }
}
