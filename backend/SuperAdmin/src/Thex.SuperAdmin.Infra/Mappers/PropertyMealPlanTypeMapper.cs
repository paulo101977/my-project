﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Thex.SuperAdmin.Infra.Entities;

namespace Thex.SuperAdmin.Infra.Mappers
{
    public class PropertyMealPlanTypeMapper : IEntityTypeConfiguration<PropertyMealPlanType>
    {
        public void Configure(EntityTypeBuilder<PropertyMealPlanType> builder)
        {

            builder.ToTable("PropertyMealPlanType");

            builder.HasAnnotation("Relational:TableName", "PropertyMealPlanType");

            builder.Property(e => e.Id)
                .HasAnnotation("Relational:ColumnName", "PropertyMealPlanTypeId");

        }
    }
}
