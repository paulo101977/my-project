﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Thex.SuperAdmin.Infra.Entities;

namespace Thex.SuperAdmin.Infra.Mappers
{
    public class ApplicationModuleMapper : IEntityTypeConfiguration<ApplicationModule>
    {
        public void Configure(EntityTypeBuilder<ApplicationModule> builder)
        {
            
            builder.ToTable("ApplicationModule");

            builder.HasAnnotation("Relational:TableName", "ApplicationModule");

            builder.Property(e => e.Id)
                .ValueGeneratedNever()
                .HasAnnotation("Relational:ColumnName", "ApplicationModuleId");

            builder.Property(e => e.ApplicationModuleDescription)
                .IsRequired()
                .HasMaxLength(300)
                .IsUnicode(false)
                .HasAnnotation("Relational:ColumnName", "ApplicationModuleDescription");

            builder.Property(e => e.ApplicationModuleName)
                .IsRequired()
                .HasMaxLength(100)
                .IsUnicode(false)
                .HasAnnotation("Relational:ColumnName", "ApplicationModuleName");

        }
    }
}
