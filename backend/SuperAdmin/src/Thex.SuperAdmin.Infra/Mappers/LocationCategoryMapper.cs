﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Thex.SuperAdmin.Infra.Entities;

namespace Thex.SuperAdmin.Infra.Mappers
{
    public class LocationCategoryMapper : IEntityTypeConfiguration<LocationCategory>
    {
        public void Configure(EntityTypeBuilder<LocationCategory> builder)
        {
            
            builder.ToTable("LocationCategory");

            builder.HasAnnotation("Relational:TableName", "LocationCategory");

            builder.Property(e => e.Id)
                .ValueGeneratedNever()
                .HasAnnotation("Relational:ColumnName", "LocationCategoryId");

            builder.Property(e => e.Name)
                .IsRequired()
                .HasMaxLength(50)
                .IsUnicode(false)
                .HasAnnotation("Relational:ColumnName", "Name");

            builder.Property(e => e.RecordScope)
                .IsRequired()
                .HasColumnType("char(1)")
                .HasDefaultValueSql("('U')")
                .HasAnnotation("Relational:ColumnName", "RecordScope");

        }
    }
}
