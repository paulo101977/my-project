﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Microsoft.EntityFrameworkCore
{
    static class Extensions
    {
        public static Expression<Func<TEntity, bool>> FilterByTenant<TEntity>(List<Guid> tenantList)
        {
            var methodInfo = typeof(List<Guid>).GetMethod("Contains",
                new Type[] { typeof(Guid) });

            var list = Expression.Constant(tenantList);

            var param = Expression.Parameter(typeof(TEntity), "j");
            var value = Expression.Property(param, "TenantId");
            var body = Expression.Call(list, methodInfo, value);

            // j => tenantList.Contains(j.TenantId)
            return Expression.Lambda<Func<TEntity, bool>>(body, param);
        }


        public static IQueryable<TEntity> FilterByTenant<TEntity>(this IQueryable<TEntity> source, List<Guid> tenantList)
        {
            if (tenantList == null || !tenantList.Any())
                return source;

            return source.Where(FilterByTenant<TEntity>(tenantList));
        }
    }
}
