﻿using System.Threading.Tasks;
using Thex.SuperAdmin.Infra.Entities;

namespace Thex.SuperAdmin.Infra.Interfaces
{
    public interface IChainRepository
    {
        Task<Chain> InsertAndSaveChangesAsync(Chain entity);
        Task<Chain> UpdateAndSaveChangesAsync(Chain entity);
    }
}
