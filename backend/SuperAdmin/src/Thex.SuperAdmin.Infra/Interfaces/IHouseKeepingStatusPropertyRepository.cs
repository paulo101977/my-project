﻿using System;
using System.Threading.Tasks;

namespace Thex.SuperAdmin.Infra.Interfaces
{
    public interface IHouseKeepingStatusPropertyRepository
    {
        Task CreateHouseKeepingStatusPropertyDefaultAsync(int propertyId, Guid tenantId, string countryCode);
    }
}
