﻿using System;
using System.Threading.Tasks;

namespace Thex.SuperAdmin.Infra.Interfaces
{
    public interface IPropertyMealPlanTypeRepository
    {
        Task CreatePropertyMealPlanTypesDefaultAsync(int propertyId, Guid tenantId, string countryCode);
    }
}
