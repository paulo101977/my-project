﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.SuperAdmin.Infra.Entities;

namespace Thex.SuperAdmin.Infra.Interfaces
{
    public interface IUserTenantRepository
    {
        Task<UserTenant> InsertAndSaveChangesAsync(UserTenant userTenant);
        Task InsertRangeAndSaveChangesAsync(ICollection<UserTenant> userTenantList);
        Task RemoveAllByUserIdAsync(Guid id);
        void ToggleAndSaveIsActive(Guid userId, int propertyId);
    }
}
