﻿using System.Threading.Tasks;
using Thex.SuperAdmin.Infra.Entities;

namespace Thex.SuperAdmin.Infra.Interfaces
{
    public interface IPropertyRepository
    {
        Task<Property> InsertAndSaveChangesAsync(Property entity);
        Task<Property> UpdateAndSaveChangesAsync(Property entity);
        Task ChangeStatusAndSaveHistoryAsync(int propertyId, int propertyStatusId);
        Task ToggleIsBlockedPropertyAndSaveHistoryAsync(int propertyId);
        Task SaveHistoryPropertyBlockedAsync(int propertyId);
        Task InsertPropertyContractAsync(PropertyContract propertyContract);
        Task UpdatePropertyContractAsync(PropertyContract propertyContract);
    }
}
