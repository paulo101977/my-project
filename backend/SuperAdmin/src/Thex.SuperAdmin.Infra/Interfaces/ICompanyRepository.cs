﻿using System.Threading.Tasks;
using Thex.SuperAdmin.Infra.Entities;

namespace Thex.SuperAdmin.Infra.Interfaces
{
    public interface ICompanyRepository
    {
        Task<Company> InsertAndSaveChangesAsync(Company company);
        Task<Company> UpdateAndSaveChangesAsync(Company company);
    }
}
