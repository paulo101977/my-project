﻿using System;
using System.Threading.Tasks;

namespace Thex.SuperAdmin.Infra.Interfaces
{
    public interface IPlasticBrandPropertyRepository
    {
        Task CreatePlasticBrandPropertiesDefault(int propertyId, Guid tenantId);
    }
}
