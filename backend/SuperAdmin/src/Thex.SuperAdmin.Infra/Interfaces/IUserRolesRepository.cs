﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Infra.Entities;

namespace Thex.SuperAdmin.Infra.Interfaces
{
    public interface IUserRolesRepository
    {
        Task CreateOrUpdateUserRoleAsync(Guid userId, List<UserRoleDto> userRoleDtoList, int? propertyId = null, int? brandId = null, int? chainId = null);
        Task AddUserRoleRangeAsync(List<UserRole> userRoleList);
    }
}
