﻿using System;
using System.Threading.Tasks;
using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Infra.Entities;
using Tnf.Dto;

namespace Thex.SuperAdmin.Infra.Interfaces
{
    public interface IPersonRepository
    {
        Person Insert(Person person);
        void Update(Person person);
        Task<Person> UpdateAsync(Person person);
    }
}
