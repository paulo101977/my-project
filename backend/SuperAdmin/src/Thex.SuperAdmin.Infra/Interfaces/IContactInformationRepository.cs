﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Thex.SuperAdmin.Infra.Entities;

namespace Thex.SuperAdmin.Infra.Interfaces
{
    public interface IContactInformationRepository
    {
        Task AddRangeAndSaveChangesAsync(IList<ContactInformation> contactInformations);
    }
}
