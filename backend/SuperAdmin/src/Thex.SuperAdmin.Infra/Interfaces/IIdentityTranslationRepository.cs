﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Infra.Entities;

namespace Thex.SuperAdmin.Infra.Interfaces
{
    public interface IIdentityTranslationRepository
    {
        Task InsertOrUpdateAndSaveChangesAsync(Guid ownerId, List<SimpleTranslationDto> translationList);
    }
}
