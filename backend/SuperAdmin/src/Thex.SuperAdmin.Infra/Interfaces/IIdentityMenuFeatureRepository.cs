﻿using System;
using System.Threading.Tasks;
using Thex.SuperAdmin.Infra.Entities;

namespace Thex.SuperAdmin.Infra.Interfaces
{
    public interface IIdentityMenuFeatureRepository
    {
        Task<IdentityMenuFeature> InsertAndSaveChangesAsync(IdentityMenuFeature identityMenuFeature);
        Task<IdentityMenuFeature> UpdateAndSaveChangesAsync(IdentityMenuFeature identityMenuFeature);
        void ToggleAndSaveIsActive(Guid id);
    }
}
