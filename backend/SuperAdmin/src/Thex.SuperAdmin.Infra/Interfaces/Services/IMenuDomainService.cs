﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.SuperAdmin.Dto;
using Tnf.Dto;

namespace Thex.SuperAdmin.Infra.Interfaces.Services
{
    public interface IMenuDomainService
    {
        Task<ListDto<MenuItemDto>> GetMenuItemHierarchy(List<IdentityMenuFeatureDto> menuList);
        Task<List<string>> GetParentKeyList(List<IdentityMenuFeatureDto> menuFeatureList, List<string> permissionKeyList);
    }
}
