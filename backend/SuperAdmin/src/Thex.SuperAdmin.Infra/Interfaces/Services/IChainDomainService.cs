﻿using System.Threading.Tasks;
using Thex.SuperAdmin.Infra.Entities;

namespace Thex.SuperAdmin.Infra.Interfaces.Services
{
    public interface IChainDomainService
    {
        Task<Chain> InsertAndSaveChangesAsync(Chain.Builder builder);
        Task<Chain> UpdateAndSaveChangesAsync(Chain.Builder builder);
    }
}
