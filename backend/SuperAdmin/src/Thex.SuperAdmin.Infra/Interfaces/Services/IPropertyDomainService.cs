﻿using System.Threading.Tasks;
using Thex.SuperAdmin.Infra.Entities;

namespace Thex.SuperAdmin.Infra.Interfaces.Services
{
    public interface IPropertyDomainService
    {
        Task<Property> InsertAndSaveChangesAsync(Property.Builder builder);
        Task<Property> UpdateAndSaveChangesAsync(Property.Builder builder);
    }
}
