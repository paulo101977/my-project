﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.SuperAdmin.Infra.Entities;

namespace Thex.SuperAdmin.Infra.Interfaces
{
    public interface IIntegrationPartnerPropertyRepository
    {
        Task InsertAndSaveChangesAsync(IntegrationPartnerProperty integrationPartnerProperty);
        Task InsertRangeAndSaveChangesAsync(ICollection<IntegrationPartnerProperty> integrationPartnerPropertyList);
        Task UpdateAndSaveChangesAsync(IntegrationPartnerProperty integrationPartnerProperty);
        Task UpdateRangeAndSaveChangesAsync(ICollection<IntegrationPartnerProperty> integrationPartnerPropertyList);
        Task RemoveAndSaveChangesAsync(IntegrationPartnerProperty integrationPartnerProperty);
        Task RemoveRangeAndSaveChangesAsync(ICollection<Guid> integrationPartnerPropertyIdList);
    }
}
