﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Infra.Entities;

namespace Thex.SuperAdmin.Infra.Interfaces
{
    public interface IRolesRepository
    {
        Task AddRolePermissionRangeAsync(List<IdentityRolePermission> rolePermissionList);
        Task CreateOrUpdateRolePermissionAsync(Guid roleId, List<IdentityRolePermissionDto> rolePermissionDtoList);
        Task RemoveAsync(Guid id);
        void ToggleAndSaveIsActive(Guid id);
    }
}
