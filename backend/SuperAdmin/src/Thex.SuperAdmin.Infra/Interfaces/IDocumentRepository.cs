﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Thex.SuperAdmin.Infra.Entities;

namespace Thex.SuperAdmin.Infra.Interfaces
{
    public interface IDocumentRepository
    {
        Task AddRangeAsync(ICollection<Document> documentList);
        Task UpdateRangeAsync(ICollection<Document> documentList);
        Task RemoveRangeAsync(ICollection<Document> documentList);

    }
}
