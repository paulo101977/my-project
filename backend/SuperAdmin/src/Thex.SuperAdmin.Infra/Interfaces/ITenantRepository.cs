﻿using System;
using Thex.SuperAdmin.Infra.Entities;

namespace Thex.SuperAdmin.Infra.Interfaces
{
    public interface ITenantRepository 
    {
        Tenant Insert(Tenant tenant);
        Tenant Update(Tenant tenant);
        void SetBrand(Guid tenantId, int brandId, int chainId);
        void SetChain(Guid tenantId, int chainId);
        void SetProperty(Guid tenantId, int brandId, int chainId, int propertyId);
        Guid GenerateTenantAutomaticallyAndSaveChanges(string tenantName);
    }
}
