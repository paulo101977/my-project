﻿using System.Threading.Tasks;
using Thex.SuperAdmin.Infra.Entities;

namespace Thex.SuperAdmin.Infra.Interfaces
{
    public interface IBrandRepository
    {
        Task<Brand> InsertAndSaveChangesAsync(Brand brand);
        Task<Brand> UpdateAndSaveChangesAsync(Brand brand);
    }
}
