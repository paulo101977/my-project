﻿using System;
using System.Threading.Tasks;

namespace Thex.SuperAdmin.Infra.Interfaces
{
    public interface IPropertyParameterRepository
    {
        Task CreatePropertyParametersDefaultAsync(int propertyId, Guid tenantId);
    }
}
