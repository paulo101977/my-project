﻿using System;
using System.Threading.Tasks;
using Thex.SuperAdmin.Infra.Entities;

namespace Thex.SuperAdmin.Infra.Interfaces
{
    public interface IIdentityFeatureRepository
    {
        Task<IdentityFeature> InsertAndSaveChangesAsync(IdentityFeature identityFeature);
        Task<IdentityFeature> UpdateAndSaveChangesAsync(IdentityFeature identityFeature);
        Task RemoveAsync(Guid id);
    }
}
