﻿using System;

namespace Thex.SuperAdmin.Infra.Interfaces
{
    public interface ISimpleUnitOfWork : IDisposable
    {
        bool Commit();
    }
}
