﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Infra.Entities;
using Tnf.Dto;

namespace Thex.SuperAdmin.Infra.Interfaces
{
    public interface IUserInvitationRepository
    {
        UserInvitation Insert(UserInvitation userInvitation);
        void ToggleAndSaveIsActive(Guid id);
        void InvitationAccepted(Guid id);
        Task<UserInvitation> UpdateInvitationLinkAsync(Guid userId, string newLink);
        Task AddRangeAsync(List<UserInvitation> userInvitationList);
    }
}
