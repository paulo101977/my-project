﻿using System;
using System.Threading.Tasks;
using Thex.SuperAdmin.Infra.Entities;

namespace Thex.SuperAdmin.Infra.Interfaces
{
    public interface IIdentityPermissionRepository
    {
        Task<IdentityPermission> InsertAndSaveChangesAsync(IdentityPermission identityPermission);
        Task<IdentityPermission> UpdateAndSaveChangesAsync(IdentityPermission newPermission, IdentityPermission oldPermission);
        Task RemoveAsync(Guid id);
        void ToggleAndSaveIsActive(Guid id);
    }
}
