﻿using System;
using System.Threading.Tasks;
using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Infra.Entities;
using Tnf.Dto;

namespace Thex.SuperAdmin.Infra.Interfaces
{
    public interface IUserRepository
    {
        void ToggleAndSaveIsActive(Guid id);
        void ChangeCulture(Guid id, string culture);
        Task<User> InsertAndSaveChangesAsync(User entity);
        Task ToggleIsActiveAsync(Guid id);
        Task UpdateAndSaveChangesAsync(User user);
    }
}
