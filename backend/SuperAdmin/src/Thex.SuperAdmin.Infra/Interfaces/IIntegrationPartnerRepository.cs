﻿using System.Threading.Tasks;
using Thex.SuperAdmin.Infra.Entities;

namespace Thex.SuperAdmin.Infra.Interfaces
{
    public interface IIntegrationPartnerRepository
    {
        Task<IntegrationPartner> InsertAndSaveChangesAsync(IntegrationPartner integrationPartner);
    }
}
