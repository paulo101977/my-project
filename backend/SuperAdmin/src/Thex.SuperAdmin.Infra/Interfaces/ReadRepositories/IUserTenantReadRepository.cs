﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.SuperAdmin.Infra.Entities;

namespace Thex.SuperAdmin.Infra.Interfaces.ReadRepositories
{
    public interface IUserTenantReadRepository
    {
        Task<ICollection<UserTenant>> GetAllByUserIdAsync(Guid id);
        Task<bool> Any(Guid userId, Guid tenantId);
        int GetChainIdByUserId(Guid userId);
    }
}
