﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Thex.Dto;
using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Dto.Dto;
using Thex.SuperAdmin.Infra.Context;
using Thex.SuperAdmin.Infra.Entities;

namespace Thex.SuperAdmin.Infra.Interfaces.ReadRepositories
{
    public interface IIdentityMenuFeatureReadRepository
    {
        ThexIdentityDbContext Context { get; }

        Task<MenuItemDto> GetMenuItemWithParents(Guid featureId);
        Task<bool> Exists(Guid id);
        Task<IdentityMenuFeature> GetById(Guid id);
        Task<IdentityMenuFeatureDto> GetDtoAsync(Guid id);
        Task<List<IdentityMenuFeatureDto>> GetAll();
        Task<List<IdentityMenuFeatureDto>> GetAllWithFiltersAsync(Expression<Func<IdentityMenuFeatureDto, bool>> filters);
    }
}
