﻿using System;
using System.Threading.Tasks;
using Thex.SuperAdmin.Dto;
using Tnf.Dto;

namespace Thex.SuperAdmin.Infra.Interfaces.ReadRepositories
{
    public interface IUserInvitationReadRepository
    {
        UserInvitationDto GetWithPropertyAndChainNames(Guid id);
        IListDto<UserInvitationDto> GetAllFilterByTenant(GetAllUserInvitationDto request);
        Task<UserInvitationDto> GetDtoByUserIdAndTenantIdAsync(Guid userId, Guid tenantId);
    }
}
