﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Dto.Dto;
using Thex.SuperAdmin.Infra.Entities;
using Thex.SuperAdmin.Infra.JoinMap;
using Tnf.Dto;

namespace Thex.SuperAdmin.Infra.Interfaces.ReadRepositories
{
    public interface ITenantReadRepository
    {
        bool Exists(Guid id);
        ThexListDto<TenantDto> GetAllDtoByFilter(GetAllTenantDto request);
        TenantDto GetDtoByid(Guid id);
        IListDto<TenantDto> GetAllDtoWithoutAssociation(Guid? tenantId);
        Expression<Func<T, bool>> FilterByTenant<T>(List<Guid> tenantList);
        Tenant GetCurrentTenant();
        Tenant GetTenantById(Guid id);
        bool IsAvailable(Guid tenantId);
        List<Guid> GetIdListByLoggedTenantId();
        List<Guid> GetIdListByLoggedUserId();
        TenantPropertyParameterJoinMap GetTenantWithPropertyParameterById(Guid id);
    }
}
