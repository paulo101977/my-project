﻿using System;
using System.Threading.Tasks;
using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Infra.Context;
using Thex.SuperAdmin.Infra.Entities;
using Tnf.Dto;

namespace Thex.SuperAdmin.Infra.Interfaces.ReadRepositories
{
    public interface IChainReadRepository
    {
        Task<IListDto<ChainDto>> GetAllDtoAsync(GetAllChainDto request);
        Task<ChainDto> GetDtoByIdAsync(DefaultIntRequestDto request);
        Task<Chain> GetByIdAsync(DefaultIntRequestDto request);
        Task<Guid> GetTenantId(int chainId);

        ThexIdentityDbContext Context { get; }
    }
}
