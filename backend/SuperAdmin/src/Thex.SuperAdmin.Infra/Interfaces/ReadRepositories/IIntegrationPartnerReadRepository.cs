﻿using System;
using System.Threading.Tasks;
using Thex.Common.Enumerations;
using Thex.Dto;
using Thex.SuperAdmin.Dto.Dto;
using Thex.SuperAdmin.Infra.Context;

namespace Thex.SuperAdmin.Infra.Interfaces.ReadRepositories
{
    public interface IIntegrationPartnerReadRepository
    {
        IntegrationPartnerDto GetDtoByIntegrationpartnerType(IntegrationPartnerTypeEnum integrationPartnerTypeEnum);
        IntegrationPartnerDto GetDtoByTokenClientAndTokenApplication(string tokenClient, string tokenApplication);
        Task<bool> AnyByTokenApplication(Guid tokenApplicationId);
        Task<string> GetPartnerNameByIdAsync(int id);
        Task<ThexListDto<PartnerDto>> GetAllPartnersAsync();
        ThexIdentityDbContext Context { get; }
    }
}
