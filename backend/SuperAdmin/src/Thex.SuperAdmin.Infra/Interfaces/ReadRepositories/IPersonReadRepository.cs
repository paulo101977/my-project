﻿using System;
using System.Collections.Generic;
using System.Text;
using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Infra.Entities;

namespace Thex.SuperAdmin.Infra.Interfaces.ReadRepositories
{
    public interface IPersonReadRepository
    {
        Person Get(Guid personId);
        PersonDto GetDtoById(Guid id);
    }
}
