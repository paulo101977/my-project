﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Thex.SuperAdmin.Dto.Dto;
using Thex.SuperAdmin.Infra.Entities;

namespace Thex.SuperAdmin.Infra.Interfaces.ReadRepositories
{
    public interface IIntegrationPartnerPropertyReadRepository
    {
        Task<bool> IntegrationCodesAvailableAsync(IList<IntegrationPartnerPropertyDto> integrationPartnerPropertyDtoList, int propertyId);
        Task<ICollection<IntegrationPartnerProperty>> GetAllByPropertyIdAsync(int propertyId);
        Task<IntegrationPartnerPropertyDto> Get(string tokenClient, string tokenApplication);
    }
}
