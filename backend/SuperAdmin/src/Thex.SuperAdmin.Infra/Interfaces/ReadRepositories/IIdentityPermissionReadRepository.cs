﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.Dto;
using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Dto.Dto;
using Thex.SuperAdmin.Infra.Context;
using Thex.SuperAdmin.Infra.Entities;

namespace Thex.SuperAdmin.Infra.Interfaces.ReadRepositories
{
    public interface IIdentityPermissionReadRepository
    {
        ThexIdentityDbContext Context { get; }

        Task<ThexListDto<IdentityPermissionDto>> GetAllDtoByFilterAsync(GetAllIdentityPermissionDto request, bool isUserId = false);
        Task<IdentityPermissionDto> GetDtoAsync(Guid id);
        Task<IdentityPermission> GetById(Guid id, bool? include = false, bool? tracking = true);
        Task<List<IdentityPermissionDto>> GetAllDtoByRoleIdAsync(Guid id);
    }
}
