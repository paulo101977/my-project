﻿using System;
using System.Collections.Generic;
using System.Text;
using Thex.SuperAdmin.Dto.Dto;
using Thex.SuperAdmin.Infra.Entities;

namespace Thex.SuperAdmin.Infra.Interfaces.ReadRepositories
{
    public interface IDocumentReadRepository
    {
        ICollection<DocumentDto> GetAllDtoByOwnerId(Guid owerId);
        ICollection<Document> GetAllByOwnerId(Guid ownerId);
    }
}
