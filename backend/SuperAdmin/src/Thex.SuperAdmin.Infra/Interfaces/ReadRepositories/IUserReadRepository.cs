﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Dto.Dto;
using Thex.SuperAdmin.Infra.Context;
using Thex.SuperAdmin.Infra.Entities;
using Tnf.Dto;

namespace Thex.SuperAdmin.Infra.Interfaces.ReadRepositories
{
    public interface IUserReadRepository
    {
        Task<UserDto> GetByLoggedUserIdAsync();
        Task<IListDto<UserDto>> GetAllByLoggedTenantIdAsync(GetAllUserDto request);
        bool AnyUserByEmail(string email);
        UserDto GetUserByEmail(string email);
        UserDto GetUserById(Guid userUid);
        UserDto GetAndValidateUser(Guid UserIid, string password);
        Task<ThexListDto<UsersDto>> GetAllAsync(GetAllUserDto request);
        ThexIdentityDbContext Context { get; }
        Task<UsersDto> GetDtoByIdAsync(Guid id);
        Task<User> GetByIdAsync(Guid id);
        bool HasPermissionByFilters(Guid id, int productId);
        Task<User> GetByIdWithoutIncludeAsync(Guid id);
    }
}
