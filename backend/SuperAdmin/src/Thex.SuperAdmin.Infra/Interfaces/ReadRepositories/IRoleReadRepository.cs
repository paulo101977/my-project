﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.Dto;
using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Dto.Dto;
using Thex.SuperAdmin.Infra.Context;

namespace Thex.SuperAdmin.Infra.Interfaces.ReadRepositories
{
    public interface IRolesReadRepository
    {
        ThexIdentityDbContext Context { get; }

        Task<RolesDto> GetByIdAsync(Guid id);

        Task<ThexListDto<RolesDto>> GetAllDtoByFilterAsync(GetAllRoleDto dto);

        Task<List<IdentityRolePermissionDto>> GetAllRolePermissionDtoByRoleIdAsync(Guid roleId);

        Task<int> Count(List<Guid> roleIdList);
    }
}
