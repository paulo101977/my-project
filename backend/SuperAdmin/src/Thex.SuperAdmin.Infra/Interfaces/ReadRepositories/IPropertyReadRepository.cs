﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Thex.Kernel.Dto;
using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Dto.Dto;
using Thex.SuperAdmin.Infra.Context;
using Thex.SuperAdmin.Infra.Entities;

namespace Thex.SuperAdmin.Infra.Interfaces.ReadRepositories
{
    public interface IPropertyReadRepository
    {
        Property GetByPropertyIdWithTenant(int propertyId, bool ignoreQueryFilters = false);

        CountryLanguageDto GetPropertyCulture(int propertyId);

        ThexIdentityDbContext Context { get; }

        ICollection<PropertyDto> GetPropertiesAssociateByCompanyId(int id);

        Task<ThexListDto<PropertyDto>> GetAllDtoAsync(GetAllPropertyDto request);

        Task<PropertyDto> GetDtoByIdAsync(DefaultIntRequestDto request);

        Task<Property> GetByIdAsync(DefaultIntRequestDto request);

        Task<ThexListDto<DashboardPropertyDto>> GetAllDtoForDashboardAsync();

        Task<int> GetPropertyStatusByIdAsync(int propertyId);

        Task<bool> CanBlockPropertyAsync(int propertyId, string key);

        Task<bool> PropertyIsBlocked(int propertyId);

        Task<PropertyContract> GetPropertyContractByPropertyIdAsync(int propertyId);

        Task<IList<PropertyDto>> GetAllDtoByChainIdAsync(int chainId);

        Task<Guid?> GetTenantIdByPropertyIdAsync(int propertyId);

        Task<List<int>> GetAllDtoByBrandIdListAsync(List<int> brandIdList);

        Task<List<PropertyIdAndTenantDto>> GetAllTenantIdListAsync(List<int> idList);

        Task<List<PropertyIdAndTenantDto>> GetAllTenantIdListByBrandIdListAsync(List<int> brandIdList);

        Task<List<PropertyIdAndTenantDto>> GetAllTenantIdListByChainIdAsync(int chainId);

        PropertyDto GetWithBrandAndChain(int id);

        Task<int> GetRoomsByPropertyIdAsync(int propertyId);

        List<LoggedUserPropertyDto> GetAllDtoByUserId(Guid userId, int productId);
    }
}