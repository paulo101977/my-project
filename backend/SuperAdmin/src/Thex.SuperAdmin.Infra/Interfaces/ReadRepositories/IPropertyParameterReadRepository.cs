﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.SuperAdmin.Dto;

namespace Thex.SuperAdmin.Infra.Interfaces.ReadRepositories
{
    public  interface IPropertyParameterReadRepository
    {
        string GetTimeZoneByPropertyId(int propertyId);
    }
}
