﻿using System.Threading.Tasks;
using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Dto.Dto;
using Thex.SuperAdmin.Infra.Context;
using Thex.SuperAdmin.Infra.Entities;

namespace Thex.SuperAdmin.Infra.Interfaces.ReadRepositories
{
    public interface ICompanyReadRepository
    {
        Task<ThexListDto<CompanyDto>> GetAllByFilterAsync(GetAllCompanyDto request);
        Task<CompanyDto> GetCompanyDtoByIdAsync(int id);
        Task<Company> GetCompanyByIdAsync(int id);
        Task<bool> Exists(int id);

        ThexIdentityDbContext Context { get; }
    }
}
