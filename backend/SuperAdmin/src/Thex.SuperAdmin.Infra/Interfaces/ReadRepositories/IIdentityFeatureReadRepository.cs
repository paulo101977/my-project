﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.Dto;
using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Dto.Dto;
using Thex.SuperAdmin.Infra.Context;
using Thex.SuperAdmin.Infra.Entities;

namespace Thex.SuperAdmin.Infra.Interfaces.ReadRepositories
{
    public interface IIdentityFeatureReadRepository
    {
        ThexIdentityDbContext Context { get; }

        Task<bool> Exists(Guid id);
        Task<IdentityFeature> GetById(Guid id);
        Task<ThexListDto<IdentityFeatureDto>> GetAllDtoByFilterAsync(GetAllIdentityFeatureDto dto);
        Task<IdentityFeatureDto> GetDtoAsync(Guid id);
    }
}
