﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Dto.Dto;
using Thex.SuperAdmin.Infra.Context;
using Thex.SuperAdmin.Infra.Entities;

namespace Thex.SuperAdmin.Infra.Interfaces.ReadRepositories
{
    public interface IBrandReadRepository
    {
        Task<ThexListDto<BrandDto>> GetAllDtoByFilterAsync(GetAllBrandDto request);
        Task<BrandDto> GetDtoByIdAsync(int id);
        Task<bool> Exists(int id);
        Task<Brand> GetById(int id);
        Task<bool> NameIsAvailable(string name);
        Task<int?> GetChainId(int brandId);
        Task<IList<BrandDto>> GetAllDtoByChainIdAsync(int chainId);
        Task<List<Guid>> GetAllTenantIdListAsync(List<int> brandIdList);
        Task<List<Guid>> GetAllTenantIdListByChainIdAsync(int chainId);
        BrandDto FirstOrDefault();

        ThexIdentityDbContext Context { get; }
    }
}
