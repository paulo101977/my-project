﻿using System.Threading.Tasks;
using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Dto.Dto;
using Thex.SuperAdmin.Infra.Context;

namespace Thex.SuperAdmin.Infra.Interfaces.ReadRepositories
{
    public interface IIdentityModuleReadRepository
    {
        Task<ThexListDto<IdentityModuleDto>> GetAllAsync();

        ThexIdentityDbContext Context { get; }
    }
}
