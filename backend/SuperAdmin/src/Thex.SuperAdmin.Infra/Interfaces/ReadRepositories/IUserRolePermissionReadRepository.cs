﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Dto.Dto;
using Thex.SuperAdmin.Infra.Context;

namespace Thex.SuperAdmin.Infra.Interfaces.ReadRepositories
{
    public interface IUserRolePermissionReadRepository
    {
        ThexIdentityDbContext Context { get; }

        Task<List<KeyValuePair<bool, string>>> GetAllUserKey(int productId, List<int?> propertyIds);

        Task<List<ProductPermissionDto>> GetAllUserKeyByProduct(int propertyId);

        Task<bool> AnyKeyAsync(Guid userId, List<string> permissionList);

        Task<bool> KeyIsEnabledForExternalAccess(List<string> permissionList);
    }
}
