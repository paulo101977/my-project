﻿using Thex.Common.Enumerations;
using Thex.SuperAdmin.Dto.Email;

namespace Thex.SuperAdmin.Infra.Factories
{
    public interface IEmailFactory
    {
        string GenerateEmail(EmailTemplateEnum emailTemplate, EmailBaseDto emailDto);
    }
}
