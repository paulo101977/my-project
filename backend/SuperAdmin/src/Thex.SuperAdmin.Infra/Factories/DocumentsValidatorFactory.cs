﻿using System;
using Thex.Common.Enumerations;
using Thex.SuperAdmin.Infra.Entities;
using Tnf.Specifications;

namespace Thex.SuperAdmin.Infra.Factories
{
    public class DocumentsValidatorFactory<T> where T : IEntity
    {
        public DocumentsValidatorFactory()
        {

        }

        private bool ValidateCNPJ(string document)
        {
            if (String.IsNullOrWhiteSpace(document))
                return false;

            document = document.Trim();
            document = document.Replace(".", "").Replace("-", "").Replace("/", "");

            if (document.Length != 14)
                return false;

            int[] multiplicador1 = new[] { 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2 };
            int[] multiplicador2 = new[] { 6, 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2 };
            int soma;
            int resto;
            string digito;
            string tempCnpj;

            // Verifica os Patterns mais Comuns para CNPJ's Inválidos
            if (document.Equals("00000000000000") ||
                document.Equals("11111111111111") ||
                document.Equals("22222222222222") ||
                document.Equals("33333333333333") ||
                document.Equals("44444444444444") ||
                document.Equals("55555555555555") ||
                document.Equals("66666666666666") ||
                document.Equals("77777777777777") ||
                document.Equals("88888888888888") ||
                document.Equals("99999999999999"))
            {
                return false;
            }

            tempCnpj = document.Substring(0, 12);
            soma = 0;

            for (int i = 0; i < 12; i++)
                soma += int.Parse(tempCnpj[i].ToString()) * multiplicador1[i];

            resto = (soma % 11);

            if (resto < 2)
                resto = 0;
            else
                resto = 11 - resto;

            digito = resto.ToString();
            tempCnpj = tempCnpj + digito;
            soma = 0;

            for (int i = 0; i < 13; i++)
                soma += int.Parse(tempCnpj[i].ToString()) * multiplicador2[i];

            resto = (soma % 11);

            if (resto < 2)
                resto = 0;
            else
                resto = 11 - resto;

            digito = digito + resto.ToString();

            return document.EndsWith(digito);
        }

        private bool ValidateCPF(string document)
        {
            if (String.IsNullOrWhiteSpace(document))
                return false;

            string clearCPF;
            clearCPF = document.Trim();
            clearCPF = clearCPF.Replace("-", "");
            clearCPF = clearCPF.Replace(".", "");

            if (clearCPF.Length != 11)
            {
                return false;
            }

            int[] cpfArray;
            
            if (clearCPF.Equals("00000000000") ||
                clearCPF.Equals("11111111111") ||
                clearCPF.Equals("22222222222") ||
                clearCPF.Equals("33333333333") ||
                clearCPF.Equals("44444444444") ||
                clearCPF.Equals("55555555555") ||
                clearCPF.Equals("66666666666") ||
                clearCPF.Equals("77777777777") ||
                clearCPF.Equals("88888888888") ||
                clearCPF.Equals("99999999999"))
            {
                return false;
            }

            foreach (char c in clearCPF)
            {
                if (!char.IsNumber(c))
                {
                    return false;
                }
            }

            cpfArray = new int[11];
            for (int i = 0; i < clearCPF.Length; i++)
            {
                cpfArray[i] = int.Parse(clearCPF[i].ToString());
            }

            // CPF Válido!
            return true;
        }

        public ExpressionSpecification<T> ValidateDocument(string LocalizationSourceName, int documentTypeId, string document)
        {
            switch (documentTypeId)
            {
                case (int)DocumentTypeEnum.BrNaturalPersonCPF:
                    
                    return new ExpressionSpecification<T>(
                      LocalizationSourceName,
                      Document.EntityError.DocumentCpfInvalid,
                      w => ValidateCPF(document)
                      );

                case (int)DocumentTypeEnum.BrLegalPersonCNPJ:
                    return new ExpressionSpecification<T>(
                      LocalizationSourceName,
                      Document.EntityError.DocumentCnpjInvalid,
                      w => ValidateCNPJ(document));

            }

            return new ExpressionSpecification<T>(
                       LocalizationSourceName,
                       Document.EntityError.DocumentInvalid,
                       w => w != null);

        }
    }
}
