﻿using Thex.Common;
using Thex.Location.Infra.Interfaces.Read;
using Thex.Location.Infra.Repositories.Read;
using Thex.SuperAdmin.Infra.Factories;
using Thex.SuperAdmin.Infra.Interfaces;
using Thex.SuperAdmin.Infra.Interfaces.ReadRepositories;
using Thex.SuperAdmin.Infra.Interfaces.Services;
using Thex.SuperAdmin.Infra.Mappers.Profiles;
using Thex.SuperAdmin.Infra.Repositories;
using Thex.SuperAdmin.Infra.Repositories.ReadRepositories;
using Thex.SuperAdmin.Infra.Services;

namespace Microsoft.Extensions.DependencyInjection
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddInfraDependency(this IServiceCollection services)
        {
            // Configura o uso do Dapper registrando os contextos que serão
            // usados pela aplicação
            services
                .AddCommonDependency();

            services.AddTnfDefaultConventionalRegistrations();

            services.AddTnfAutoMapper(config =>
            {
                config.AddProfile<ApplicationModuleProfile>();
                config.AddProfile<ApplicationParameterProfile>();
                config.AddProfile<BrandProfile>();
                config.AddProfile<ChainProfile>();
                config.AddProfile<CompanyProfile>();
                config.AddProfile<ContactInformationProfile>();
                config.AddProfile<ContactInformationTypeProfile>();
                config.AddProfile<CountryLanguageProfile>();
                config.AddProfile<CountrySubdivisionProfile>();

                config.AddProfile<CountrySubdivisionTranslationProfile>();
                config.AddProfile<FeatureGroupProfile>();
                config.AddProfile<LocationCategoryProfile>();
                config.AddProfile<LocationProfile>();
                config.AddProfile<ParameterTypeProfile>();
                config.AddProfile<PersonProfile>();
                config.AddProfile<PropertyParameterProfile>();
                config.AddProfile<PropertyProfile>();
                config.AddProfile<PropertyTypeProfile>();
                config.AddProfile<StatusCategoryProfile>();
                config.AddProfile<StatusProfile>();
                config.AddProfile<TenantProfile>();
                config.AddProfile<UserInvitationProfile>();
                config.AddProfile<IdentityFeatureProfile>();
                config.AddProfile<IdentityMenuFeatureProfile>();
                config.AddProfile<IdentityModuleProfile>();
                config.AddProfile<IdentityPermissionFeatureProfile>();
                config.AddProfile<IdentityPermissionProfile>();
                config.AddProfile<IdentityProductProfile>();
                config.AddProfile<IdentityRolePermissionProfile>();
                config.AddProfile<RolesProfile>();
                config.AddProfile<MenuProfile>();
                config.AddProfile<IdentityTranslationProfile>();
            });

            services.AddTransient<IUserReadRepository, UserReadRepository>();
            services.AddTransient<ITenantReadRepository, TenantReadRepository>();
            services.AddTransient<IPersonReadRepository, PersonReadRepository>();
            services.AddTransient<IPropertyParameterReadRepository, PropertyParameterReadRepository>();
            services.AddTransient<IPropertyReadRepository, PropertyReadRepository>();
            services.AddTransient<IUserInvitationReadRepository, UserInvitationReadRepository>();
            services.AddTransient<IUserReadRepository, UserReadRepository>();
            services.AddTransient<IChainReadRepository, ChainReadRepository>();
            services.AddTransient<IBrandReadRepository, BrandReadRepository>();
            services.AddTransient<ICompanyReadRepository, CompanyReadRepository>();
            services.AddTransient<IDocumentReadRepository, DocumentReadRepository>();
            services.AddTransient<IIdentityPermissionReadRepository, IdentityPermissionReadRepository>();
            services.AddTransient<IIdentityMenuFeatureReadRepository, IdentityMenuFeatureReadRepository>();
            services.AddTransient<IIdentityFeatureReadRepository, IdentityFeatureReadRepository>();
            services.AddTransient<IIdentityModuleReadRepository, IdentityModuleReadRepository>();
            services.AddTransient<IIdentityProductReadRepository, IdentityProductReadRepository>();
            services.AddTransient<IIntegrationPartnerPropertyReadRepository, IntegrationPartnerPropertyReadRepository>();
            services.AddTransient<IRolesReadRepository, RolesReadRepository>();
            services.AddTransient<IUserRolePermissionReadRepository, UserRolePermissionReadRepository>();
            services.AddTransient<IIntegrationPartnerReadRepository, IntegrationPartnerReadRepository>();
            
            services.AddTransient<IUserTenantReadRepository, UserTenantReadRepository>();

            services.AddTransient<IPersonRepository, PersonRepository>();
            services.AddTransient<IUserInvitationRepository, UserInvitationRepository>();
            services.AddTransient<IUserRepository, UserRepository>();
            services.AddTransient<IChainRepository, ChainRepository>();
            services.AddTransient<ITenantRepository, TenantRepository>();
            services.AddTransient<IBrandRepository, BrandRepository>();
            services.AddTransient<ICompanyRepository, CompanyRepository>();
            services.AddTransient<IDocumentRepository, DocumentRepository>();
            services.AddTransient<IContactInformationRepository, ContactInformationRepository>();
            services.AddTransient<IHouseKeepingStatusPropertyRepository, HouseKeepingStatusPropertyRepository>();
            services.AddTransient<IPlasticBrandPropertyRepository, PlasticBrandPropertyRepository>();
            services.AddTransient<IPropertyMealPlanTypeRepository, PropertyMealPlanTypeRepository>();
            services.AddTransient<IPropertyParameterRepository, PropertyParameterRepository>();        
            services.AddTransient<IPropertyRepository, PropertyRepository>();
            services.AddTransient<IIdentityTranslationRepository, IdentityTranslationRepository>();
            services.AddTransient<IUserTenantRepository, UserTenantRepository>();
            services.AddTransient<IIntegrationPartnerRepository, IntegrationPartnerRepository>();
            services.AddTransient<IIntegrationPartnerPropertyRepository, IntegrationPartnerPropertyRepository>();

            services.AddTransient<IChainDomainService, ChainDomainService>();
            services.AddTransient<ITenantRepository, TenantRepository>();
            services.AddTransient<IPropertyDomainService, PropertyDomainService>();
            services.AddTransient<IMenuDomainService, MenuDomainService>();
            
            services.AddTransient<IIdentityPermissionRepository, IdentityPermissionRepository>();
            services.AddTransient<IIdentityFeatureRepository, IdentityFeatureRepository>();
            services.AddTransient<IIdentityMenuFeatureRepository, IdentityMenuFeatureRepository>();
            services.AddTransient<IRolesRepository, RolesRepository>();
            services.AddTransient<IUserRolesRepository, UserRolesRepository>();

            services.AddTransient<IEmailFactory, EmailFactory>();

            services.AddTransient<ThexIdentityRoleManager>();

            return services;
        }
    }
}
