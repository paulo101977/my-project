﻿using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Thex.SuperAdmin.Infra.Entities;
using Thex.SuperAdmin.Infra.Mappers;
using System;
using System.Linq;
using Thex.Common.Extensions;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using System.Threading;
using System.Threading.Tasks;
using Thex.Kernel;
using System.Collections.Generic;

namespace Thex.SuperAdmin.Infra.Context
{
    public class ThexIdentityDbContext : IdentityDbContext<User, Roles, Guid, UserClaim, UserRole, UserLogin, RoleClaim, UserToken>
    {
        protected IApplicationUser _applicationUser;
        protected Guid _tenantId = Guid.Empty;
        protected Guid _userUid = Guid.Empty;

        public virtual DbSet<ApplicationModule> ApplicationModules { get; set; }
        public virtual DbSet<ApplicationParameter> ApplicationParameters { get; set; }
        public virtual DbSet<Brand> Brands { get; set; }
        public virtual DbSet<Chain> Chains { get; set; }
        public virtual DbSet<Company> Companies { get; set; }
        public virtual DbSet<ContactInformation> ContactInformations { get; set; }
        public virtual DbSet<ContactInformationType> ContactInformationTypes { get; set; }
        public virtual DbSet<CountryLanguage> CountryLanguages { get; set; }
        public virtual DbSet<CountrySubdivision> CountrySubdivisions { get; set; }
        public virtual DbSet<CountrySubdivisionTranslation> CountrySubdivisionTranslations { get; set; }
        public virtual DbSet<Document> Documents { get; set; }
        public virtual DbSet<DocumentType> DocumentTypes { get; set; }
        public virtual DbSet<HouseKeepingStatusProperty> HouseKeepingStatusProperties { get; set; }
        public virtual DbSet<FeatureGroup> FeatureGroups { get; set; }
        public virtual DbSet<IntegrationPartner> IntegrationPartners { get; set; }
        public virtual DbSet<Entities.Location> Locations { get; set; }
        public virtual DbSet<LocationCategory> LocationCategories { get; set; }
        public virtual DbSet<IntegrationPartnerProperty> IntegrationPartnerProperties { get; set; }
        public virtual DbSet<ParameterType> ParameterTypes { get; set; }
        public virtual DbSet<Partner> Partners { get; set; }
        public virtual DbSet<PropertyParameter> PropertyParameters { get; set; }
        public virtual DbSet<Person> Persons { get; set; }
        public virtual DbSet<PlasticBrand> PlasticBrands { get; set; }
        public virtual DbSet<PlasticBrandProperty> PlasticBrandProperties { get; set; }
        public virtual DbSet<Property> Properties { get; set; }
        public virtual DbSet<PropertyContract> PropertyContracts { get; set; }
        public virtual DbSet<PropertyMealPlanType> PropertyMealPlanTypes { get; set; }
        public virtual DbSet<PropertyStatusHistory> PropertyStatusHistories { get; set; }
        public virtual DbSet<Room> Rooms { get; set; }
        public virtual DbSet<Status> Statuss { get; set; }
        public virtual DbSet<UserInvitation> UserInvitations { get; set; }
        public virtual DbSet<Tenant> Tenants { get; set; }
        public virtual DbSet<UserTenant> UserTenants { get; set; }


        public virtual DbSet<IdentityFeature> IdentityFeatures { get; set; }
        public virtual DbSet<IdentityMenuFeature> IdentityMenuFeatures { get; set; }
        public virtual DbSet<IdentityModule> IdentityModules { get; set; }
        public virtual DbSet<IdentityPermission> IdentityPermissions { get; set; }
        public virtual DbSet<IdentityPermissionFeature> IdentityPermissionFeatures { get; set; }
        public virtual DbSet<IdentityProduct> IdentityProducts { get; set; }
        public virtual DbSet<IdentityRolePermission> IdentityRolePermissions { get; set; }
        public virtual DbSet<IdentityTranslation> IdentityTranslations { get; set; }
        public virtual DbSet<IdentityProductProperty> IdentityProductProperties { get; set; }

        public ThexIdentityDbContext(DbContextOptions<ThexIdentityDbContext> options, IApplicationUser applicationUser)
            : base(options)
        {
            _applicationUser = applicationUser;
            _tenantId = applicationUser.TenantId != Guid.Empty ? applicationUser.TenantId : Guid.Empty;
            _userUid = applicationUser.UserUid != Guid.Empty ? applicationUser.UserUid : Guid.Empty;
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.ApplyConfiguration(new ApplicationModuleMapper());
            modelBuilder.ApplyConfiguration(new ApplicationParameterMapper());
            modelBuilder.ApplyConfiguration(new BrandMapper());
            modelBuilder.ApplyConfiguration(new ChainMapper());
            modelBuilder.ApplyConfiguration(new CompanyMapper());
            modelBuilder.ApplyConfiguration(new ContactInformationMapper());
            modelBuilder.ApplyConfiguration(new ContactInformationTypeMapper());
            modelBuilder.ApplyConfiguration(new CountryLanguageMapper());
            modelBuilder.ApplyConfiguration(new CountrySubdivisionMapper());
            modelBuilder.ApplyConfiguration(new CountrySubdivisionTranslationMapper());
            modelBuilder.ApplyConfiguration(new DocumentMapper());
            modelBuilder.ApplyConfiguration(new DocumentTypeMapper());
            modelBuilder.ApplyConfiguration(new FeatureGroupMapper());
            modelBuilder.ApplyConfiguration(new HouseKeepingStatusPropertyMapper());
            modelBuilder.ApplyConfiguration(new LocationMapper());
            modelBuilder.ApplyConfiguration(new LocationCategoryMapper());
            modelBuilder.ApplyConfiguration(new IntegrationPartnerMapper());
            modelBuilder.ApplyConfiguration(new IntegrationPartnerPropertyMapper());

            modelBuilder.ApplyConfiguration(new PartnerMapper());
            modelBuilder.ApplyConfiguration(new PlasticBrandMapper());
            modelBuilder.ApplyConfiguration(new PlasticBrandPropertyMapper());
            modelBuilder.ApplyConfiguration(new PropertyMealPlanTypeMapper());
            modelBuilder.ApplyConfiguration(new ParameterTypeMapper());
            modelBuilder.ApplyConfiguration(new PersonMapper());
            modelBuilder.ApplyConfiguration(new PropertyMapper());
            modelBuilder.ApplyConfiguration(new PropertyContractMapper());
            modelBuilder.ApplyConfiguration(new PropertyParameterMapper());
            modelBuilder.ApplyConfiguration(new PropertyStatusHistoryMapper());
            modelBuilder.ApplyConfiguration(new RoomMapper());
            modelBuilder.ApplyConfiguration(new StatusMapper());
            modelBuilder.ApplyConfiguration(new TenantMapper());
            modelBuilder.ApplyConfiguration(new UserInvitationMapper());
            modelBuilder.ApplyConfiguration(new UserTenantMapper());

            modelBuilder.Entity<UserTenant>().HasQueryFilter(b => !b.IsDeleted);

            modelBuilder.ApplyConfiguration(new IdentityFeatureMapper());
            modelBuilder.ApplyConfiguration(new IdentityMenuFeatureMapper());
            modelBuilder.ApplyConfiguration(new IdentityModuleMapper());
            modelBuilder.ApplyConfiguration(new IdentityPermissionMapper());
            modelBuilder.ApplyConfiguration(new IdentityPermissionFeatureMapper());
            modelBuilder.ApplyConfiguration(new IdentityProductMapper());
            modelBuilder.ApplyConfiguration(new IdentityRolePermissionMapper());
            modelBuilder.ApplyConfiguration(new IdentityTranslationMapper());
            modelBuilder.ApplyConfiguration(new IdentityProductPropertyMapper());

            modelBuilder.Entity<IdentityFeature>().HasQueryFilter(b => !b.IsDeleted);
            modelBuilder.Entity<IdentityMenuFeature>().HasQueryFilter(b => !b.IsDeleted);
            modelBuilder.Entity<IdentityModule>().HasQueryFilter(b => !b.IsDeleted);
            modelBuilder.Entity<IdentityPermission>().HasQueryFilter(b => !b.IsDeleted);
            modelBuilder.Entity<IdentityPermissionFeature>().HasQueryFilter(b => !b.IsDeleted);
            modelBuilder.Entity<IdentityProduct>().HasQueryFilter(b => !b.IsDeleted);
            modelBuilder.Entity<IdentityRolePermission>().HasQueryFilter(b => !b.IsDeleted);
            modelBuilder.Entity<UserRole>().HasQueryFilter(b => !b.IsDeleted);
            modelBuilder.Entity<IntegrationPartner>().HasQueryFilter(b => b.IsActive);
            modelBuilder.Entity<IntegrationPartnerProperty>().HasQueryFilter(b => !b.IsDeleted);
            modelBuilder.Entity<IdentityProductProperty>().HasQueryFilter(b => !b.IsDeleted);

            modelBuilder.Entity<User>(b =>
            {
                b.ToTable("Users");
            });

            modelBuilder.Entity<UserClaim>(b =>
            {
                b.ToTable("UserClaims");
            });

            modelBuilder.Entity<UserLogin>(b =>
            {
                b.ToTable("UserLogins");
            });

            modelBuilder.Entity<UserToken>(b =>
            {
                b.ToTable("UserTokens");
            });

            modelBuilder.Entity<Roles>(b =>
            {
                b.ToTable("Roles");
            });

            modelBuilder.Entity<RoleClaim>(b =>
            {
                b.ToTable("RoleClaims");
            });

            modelBuilder.Entity<UserRole>(b =>
            {
                b.ToTable("UserRoles");
            });
        }


        public virtual void FillAuditFiedls()
        {
            var utcNowAuditDate = DateTime.UtcNow.ToZonedDateTimeLoggedUser();

            var auditedEntities = new List<string>
            {
                "ThexIdentityUserEntity",
                "ThexIdentityUserClaimEntity",
                "ThexIdentityUserLoginEntity",
                "ThexIdentityUserTokenEntity",
                "ThexIdentityRoleEntity",
                "ThexIdentityRoleClaimEntity",
                "ThexIdentityUserRoleEntity",
                "ThexMultiTenantFullAuditedEntity",
                "ThexFullAuditedEntity"
            };

            var entriesWithBase = ChangeTracker.Entries();

            var FullAuditedEntries = entriesWithBase
                .Where(entry => (entry.Metadata.ClrType.BaseType != null && auditedEntities.Contains(entry.Metadata.ClrType.BaseType.Name)) ||// entry.Metadata.ClrType.BaseType.Name.Contains("ThexFullAuditedEntity")) ||
                                (entry.Metadata.ClrType.BaseType.BaseType != null && auditedEntities.Contains(entry.Metadata.ClrType.BaseType.BaseType.Name)));//entry.Metadata.ClrType.BaseType.BaseType.Name.Contains("ThexFullAuditedEntity"))

            if (FullAuditedEntries != null)
                foreach (EntityEntry entityEntry in FullAuditedEntries)
                {
                    var entity = entityEntry.Entity;
                    switch (entityEntry.State)
                    {
                        case EntityState.Added:
                            entity.GetType().GetProperty("CreationTime").SetValue(entity, utcNowAuditDate, null);
                            entity.GetType().GetProperty("LastModificationTime").SetValue(entity, utcNowAuditDate, null);
                            entity.GetType().GetProperty("CreatorUserId").SetValue(entity, _userUid, null);
                            break;
                        case EntityState.Modified:
                            entityEntry.Property("CreationTime").IsModified = false;
                            entityEntry.Property("CreatorUserId").IsModified = false;

                            entity.GetType().GetProperty("LastModificationTime").SetValue(entity, utcNowAuditDate, null);
                            entity.GetType().GetProperty("LastModifierUserId").SetValue(entity, _userUid, null);
                            break;
                        case EntityState.Deleted:
                            entityEntry.State = EntityState.Modified;

                            entityEntry.Property("CreationTime").IsModified = false;
                            entityEntry.Property("CreatorUserId").IsModified = false;
                            entityEntry.Property("LastModificationTime").IsModified = false;
                            entityEntry.Property("LastModifierUserId").IsModified = false;

                            entity.GetType().GetProperty("DeletionTime").SetValue(entity, utcNowAuditDate, null);
                            entity.GetType().GetProperty("DeleterUserId").SetValue(entity, _userUid, null);
                            entity.GetType().GetProperty("IsDeleted").SetValue(entity, true);
                            break;
                    }
                }
        }

        public override int SaveChanges()
        {
            FillAuditFiedls();
            return base.SaveChanges();
        }

        public override async Task<int> SaveChangesAsync(CancellationToken cancellationToken = default(CancellationToken))
        {
            FillAuditFiedls();
            return (await base.SaveChangesAsync(true, cancellationToken));
        }
    }
}
