﻿using System;
using System.Linq.Expressions;
using Thex.Common;
using Thex.SuperAdmin.Infra.Entities;
using Tnf.Specifications;
using ThexEntities = Thex.SuperAdmin.Infra.Entities;

namespace Thex.SuperAdmin.Infra.Specifications.Tenant
{
    public class TenantIsAvailableSpecification : Specification<ThexEntities.Tenant>
    {
        private Guid _tenantId { get; }

        public TenantIsAvailableSpecification(Guid tenantId)
        {
            _tenantId = tenantId;
        }

        public override string LocalizationSource { get; protected set; } = AppConsts.LocalizationSourceName;

        public override Enum LocalizationKey { get; protected set; } = ThexEntities.Tenant.EntityError.TenantIsNotAvailable;

        public override Expression<Func<ThexEntities.Tenant, bool>> ToExpression()
        {
            return x => x.Id == _tenantId &&
                        !x.PropertyId.HasValue &&
                        !x.BrandId.HasValue &&
                        !x.ChainId.HasValue;
        }

    }
}
