﻿using System;
using System.Threading.Tasks;
using Thex.SuperAdmin.Infra.Context;
using Thex.SuperAdmin.Infra.Context.Repositories;
using Thex.SuperAdmin.Infra.Entities;
using Thex.SuperAdmin.Infra.Interfaces;

namespace Thex.SuperAdmin.Infra.Repositories
{
    public class PlasticBrandPropertyRepository : SimpleRepository<PlasticBrandProperty>, IPlasticBrandPropertyRepository
    {
        private readonly ThexIdentityDbContext _context;

        public PlasticBrandPropertyRepository(ThexIdentityDbContext context)
            : base(context)
        {
            _context = context;
        }

        public async Task CreatePlasticBrandPropertiesDefault(int propertyId, Guid tenantId)
        {
            for (int i = 1; i <= 7; i++)
            {
                await _context.PlasticBrandProperties.AddAsync(new PlasticBrandProperty()
                {
                    Id = Guid.NewGuid(),
                    PlasticBrandId = i,
                    PropertyId = propertyId,
                    TenantId = tenantId,
                    InstallmentsQuantity = 0,
                    IsActive = true
                });
            }

            await _context.SaveChangesAsync();
        }
    }
}
