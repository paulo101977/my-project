﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Threading.Tasks;
using Thex.Common;
using Thex.Common.Enumerations;
using Thex.SuperAdmin.Infra.Context;
using Thex.SuperAdmin.Infra.Context.Repositories;
using Thex.SuperAdmin.Infra.Entities;
using Thex.SuperAdmin.Infra.Interfaces;
using Tnf.Localization;

namespace Thex.SuperAdmin.Infra.Repositories
{
    public class HouseKeepingStatusPropertyRepository : SimpleRepository<HouseKeepingStatusProperty>, IHouseKeepingStatusPropertyRepository
    {
        private readonly ThexIdentityDbContext _context;
        private readonly ILocalizationManager _localizationManager;

        public HouseKeepingStatusPropertyRepository(
            ThexIdentityDbContext context,
            ILocalizationManager localizationManager)
            : base(context)
        {
            _context = context;
            _localizationManager = localizationManager;
        }

        public async Task CreateHouseKeepingStatusPropertyDefaultAsync(int propertyId, Guid tenantId, string countryCode)
        {
            var cultureInfo = GetCultureInfoByCountryCode(countryCode);

            await _context.HouseKeepingStatusProperties.AddAsync(new HouseKeepingStatusProperty()
            {
                Id = Guid.NewGuid(),
                PropertyId = propertyId,
                Color = "1DC29D",
                HousekeepingStatusId = (int)HousekeepingStatusEnum.Clean,
                StatusName = _localizationManager.GetString(AppConsts.LocalizationSourceName, HousekeepingStatusEnum.Clean.ToString(), cultureInfo),
                TenantId = tenantId
            });

            await _context.HouseKeepingStatusProperties.AddAsync(new HouseKeepingStatusProperty()
            {
                Id = Guid.NewGuid(),
                PropertyId = propertyId,
                Color = "FF6969",
                HousekeepingStatusId = (int)HousekeepingStatusEnum.Dirty,
                StatusName = _localizationManager.GetString(AppConsts.LocalizationSourceName, HousekeepingStatusEnum.Dirty.ToString(), cultureInfo),
                TenantId = tenantId
            });

            await _context.HouseKeepingStatusProperties.AddAsync(new HouseKeepingStatusProperty()
            {
                Id = Guid.NewGuid(),
                PropertyId = propertyId,
                Color = "A497E3",
                HousekeepingStatusId = (int)HousekeepingStatusEnum.Maintenance,
                StatusName = _localizationManager.GetString(AppConsts.LocalizationSourceName, HousekeepingStatusEnum.Maintenance.ToString(), cultureInfo),
                TenantId = tenantId
            });

            await _context.HouseKeepingStatusProperties.AddAsync(new HouseKeepingStatusProperty()
            {
                Id = Guid.NewGuid(),
                PropertyId = propertyId,
                Color = "F8E71C",
                HousekeepingStatusId = (int)HousekeepingStatusEnum.Inspection,
                StatusName = _localizationManager.GetString(AppConsts.LocalizationSourceName, HousekeepingStatusEnum.Inspection.ToString(), cultureInfo),
                TenantId = tenantId
            });

            await _context.HouseKeepingStatusProperties.AddAsync(new HouseKeepingStatusProperty()
            {
                Id = Guid.NewGuid(),
                PropertyId = propertyId,
                Color = "00C5FF",
                HousekeepingStatusId = (int)HousekeepingStatusEnum.Stowage,
                StatusName = _localizationManager.GetString(AppConsts.LocalizationSourceName, HousekeepingStatusEnum.Stowage.ToString(), cultureInfo),
                TenantId = tenantId
            });

            await _context.HouseKeepingStatusProperties.AddAsync(new HouseKeepingStatusProperty()
            {
                Id = Guid.NewGuid(),
                PropertyId = propertyId,
                Color = "8B572A",
                HousekeepingStatusId = (int)HousekeepingStatusEnum.CreatedByHotel,
                StatusName = _localizationManager.GetString(AppConsts.LocalizationSourceName, HousekeepingStatusEnum.CreatedByHotel.ToString(), cultureInfo),
                TenantId = tenantId
            });

            await _context.SaveChangesAsync();
        }

        private CultureInfo GetCultureInfoByCountryCode(string countryCode)
        {
            if (string.IsNullOrWhiteSpace(countryCode))
                return new CultureInfo("en-US");

            switch (countryCode.ToLower())
            {
                case "br":
                    return new CultureInfo("pt-BR");
                case "ar":
                    return new CultureInfo("es-AR");
                case "cl":
                    return new CultureInfo("es-CL");
                case "pt":
                    return new CultureInfo("pt-PT");
                default:
                    return new CultureInfo("en-US"); ;
            }
        }
    }
}
