﻿using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;
using Thex.SuperAdmin.Infra.Context;
using Thex.SuperAdmin.Infra.Context.Repositories;
using Thex.SuperAdmin.Infra.Entities;
using Thex.SuperAdmin.Infra.Interfaces;

namespace Thex.SuperAdmin.Infra.Repositories
{
    public class CompanyRepository : SimpleRepository<Company>, ICompanyRepository
    {
        private readonly ThexIdentityDbContext _context;

        public CompanyRepository(
          ThexIdentityDbContext context)
          : base(context)
        {
            _context = context;
        }

        public async Task<Company> InsertAndSaveChangesAsync(Company company)
        {
            var entityEntry = await _context.Companies.AddAsync(company);

            var entity = entityEntry.Entity;

            await _context.SaveChangesAsync();

            return entity;
        }

        public async Task<Company> UpdateAndSaveChangesAsync(Company company)
        {
            var existingCompany = _context.Companies
                .Where(exp => exp.Id == company.Id)
                .Include(exp => exp.Person)
                .ThenInclude(exp => exp.DocumentList)
                .Include(exp => exp.Person)
                .ThenInclude(exp => exp.ContactInformationList)
                .SingleOrDefault();

            if (existingCompany != null)
            {
                _context.Entry(existingCompany).State = EntityState.Modified;
                _context.Entry(existingCompany).CurrentValues.SetValues(company);
                existingCompany.TradeName = company.TradeName;
                existingCompany.ShortName = company.ShortName;
                existingCompany.Person.PersonType = company.Person.PersonType;
                existingCompany.Person.FirstName = company.Person.FirstName;
                existingCompany.Person.LastName = company.Person.LastName;
                existingCompany.Person.FullName = company.Person.FullName;

                RemoveDocumentListFromCompanyClient(existingCompany, company);
                RemoveContactInformationListFromCompanyClient(existingCompany, company);

                await _context.SaveChangesAsync();

                foreach (var contactInformation in company.Person.ContactInformationList)
                {
                    AddContactInformationListFromCompanyClient(existingCompany, contactInformation);
                }

                foreach (var document in company.Person.DocumentList)
                {
                    AddDocumentListFromCompanyClient(existingCompany, document);
                }

                await _context.SaveChangesAsync();                
            }

            return existingCompany;
        }

        #region Private methods
        private void RemoveContactInformationListFromCompanyClient(Company existingCompany, Company company)
        {
            var contactInformationListToExclude = existingCompany.Person.ContactInformationList.Where(c => c.OwnerId == company.PersonId).ToList();
            if (contactInformationListToExclude.Count > 0)
                _context.RemoveRange(contactInformationListToExclude);
        }

        private void AddContactInformationListFromCompanyClient(Company existingCompanyClient,
                                                                ContactInformation contactInformation)
        {
            existingCompanyClient.Person.ContactInformationList.Add(contactInformation);
        }

        private void RemoveDocumentListFromCompanyClient(Company existingCompany, Company company)
        {
            var documentListToExclude = existingCompany.Person.DocumentList.Where(d => d.OwnerId == company.PersonId).ToList();

            if (documentListToExclude.Any())
            {
                if (documentListToExclude.Count() > 1)
                {
                    foreach (var document in documentListToExclude)
                    {
                        _context.RemoveRange(document);
                        _context.SaveChanges();
                    }
                }
                else
                    _context.RemoveRange(documentListToExclude);
            }
        }

        private void AddDocumentListFromCompanyClient(Company existingCompany, Document document)
        {
            existingCompany.Person.DocumentList.Add(document);
        }

        #endregion
    }
}
