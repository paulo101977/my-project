﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Thex.SuperAdmin.Infra.Context;
using Thex.SuperAdmin.Infra.Context.Repositories;
using Thex.SuperAdmin.Infra.Entities;
using Thex.SuperAdmin.Infra.Interfaces;

namespace Thex.SuperAdmin.Infra.Repositories
{
    public class TenantRepository : SimpleRepository<Tenant>, ITenantRepository
    {
        private readonly ThexIdentityDbContext _context;

        public TenantRepository(
           ThexIdentityDbContext context) : base(context)
        {
            _context = context;
        }

        public new Tenant Update(Tenant tenant)
        {
            tenant = _context.Tenants.Update(tenant).Entity;
            return tenant;
        }

        public Tenant Insert(Tenant tenant)
        {
            tenant = _context.Tenants.Add(tenant).Entity;

            return tenant;
        }

        public Guid GenerateTenantAutomaticallyAndSaveChanges(string tenantName)
        {
            var tenant = new Tenant()
            {
                Id = Guid.NewGuid(),
                TenantName = tenantName,
                IsActive = true
            };

            return _context.Tenants.Add(tenant).Entity.Id;
        }

        public void SetBrand(Guid tenantId, int brandId, int chainId)
        {
            var tenant = _context.Tenants.FirstOrDefault(x => x.Id == tenantId);
            if(tenant != null)
            {
                tenant.BrandId = brandId;
                tenant.ChainId = chainId;
                tenant.ParentId = _context.Chains.FirstOrDefault(e => e.Id == chainId)?.TenantId;
                _context.Tenants.Update(tenant).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                _context.SaveChanges();
            }
        }

        public void SetChain(Guid tenantId, int chainId)
        {
            var tenant = _context.Tenants.FirstOrDefault(x => x.Id == tenantId);
            if (tenant != null)
            {
                tenant.ChainId = chainId;
                _context.Tenants.Update(tenant).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                _context.SaveChanges();
            }
        }

        public void SetProperty(Guid tenantId, int brandId, int chainId, int propertyId)
        {
            var tenant = _context.Tenants.FirstOrDefault(x => x.Id == tenantId);
            if (tenant != null)
            {
                tenant.BrandId = brandId;
                tenant.ChainId = chainId;
                tenant.PropertyId = propertyId;
                tenant.ParentId = _context.Brands.FirstOrDefault(e => e.Id == brandId)?.TenantId;

                _context.Tenants.Update(tenant).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                _context.SaveChanges();
            }
        }
    }
}
