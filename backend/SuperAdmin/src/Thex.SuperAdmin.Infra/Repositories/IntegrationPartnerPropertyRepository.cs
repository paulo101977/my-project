﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Thex.SuperAdmin.Infra.Context;
using Thex.SuperAdmin.Infra.Context.Repositories;
using Thex.SuperAdmin.Infra.Entities;
using Thex.SuperAdmin.Infra.Interfaces;

namespace Thex.SuperAdmin.Infra.Repositories
{
    public class IntegrationPartnerPropertyRepository : SimpleRepository<IntegrationPartnerProperty>, IIntegrationPartnerPropertyRepository
    {
        private readonly ThexIdentityDbContext _context;

        public IntegrationPartnerPropertyRepository(ThexIdentityDbContext context)
            : base(context)
        {
            _context = context;
        }

        public async Task InsertAndSaveChangesAsync(IntegrationPartnerProperty integrationPartnerProperty)
        {
            await _context.IntegrationPartnerProperties.AddAsync(integrationPartnerProperty);

            await _context.SaveChangesAsync();
        }

        public async Task InsertRangeAndSaveChangesAsync(ICollection<IntegrationPartnerProperty> integrationPartnerPropertyList)
        {
            await _context.IntegrationPartnerProperties.AddRangeAsync(integrationPartnerPropertyList);

            await _context.SaveChangesAsync();
        }

        public async Task UpdateAndSaveChangesAsync(IntegrationPartnerProperty integrationPartnerProperty)
        {
            _context.Entry(integrationPartnerProperty).State = Microsoft.EntityFrameworkCore.EntityState.Modified;

            _context.IntegrationPartnerProperties.Update(integrationPartnerProperty);

            await _context.SaveChangesAsync();
        }

        public async Task UpdateRangeAndSaveChangesAsync(ICollection<IntegrationPartnerProperty> integrationPartnerPropertyList)
        {
            _context.IntegrationPartnerProperties.UpdateRange(integrationPartnerPropertyList);

            await _context.SaveChangesAsync();
        }

        public async Task RemoveAndSaveChangesAsync(IntegrationPartnerProperty integrationPartnerProperty)
        {
            _context.IntegrationPartnerProperties.Remove(integrationPartnerProperty);

            await _context.SaveChangesAsync();
        }

        public async Task RemoveRangeAndSaveChangesAsync(ICollection<Guid> integrationPartnerPropertyIdList)
        {
            var integrationPartnerPropertyListToRemove = new List<IntegrationPartnerProperty>();
            var integrationPartnerListToRemove = new List<IntegrationPartner>();
            foreach (var integrationPartnerPropertyId in integrationPartnerPropertyIdList)
            {
                var integrationPartnerProperty = _context.IntegrationPartnerProperties
                    .FirstOrDefault(ipp => ipp.Id == integrationPartnerPropertyId);

                integrationPartnerPropertyListToRemove.Add(integrationPartnerProperty);
                if(integrationPartnerProperty.IntegrationPartnerId.HasValue)
                    integrationPartnerListToRemove.Add(new IntegrationPartner { Id = integrationPartnerProperty.IntegrationPartnerId.Value });
            }

            _context.IntegrationPartners.RemoveRange(integrationPartnerListToRemove);
            _context.IntegrationPartnerProperties.RemoveRange(integrationPartnerPropertyListToRemove);

            await _context.SaveChangesAsync();
        }
    }
}
