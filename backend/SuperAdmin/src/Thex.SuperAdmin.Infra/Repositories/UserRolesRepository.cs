﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Threading.Tasks;
using Thex.Kernel;
using Thex.SuperAdmin.Infra.Context;
using Thex.SuperAdmin.Infra.Context.Repositories;
using Thex.SuperAdmin.Infra.Entities;
using Thex.SuperAdmin.Infra.Interfaces;
using Tnf.Localization;
using System.Linq;
using System.Collections.Generic;
using Thex.SuperAdmin.Dto;
using System.Data.SqlClient;

namespace Thex.SuperAdmin.Infra.Repositories
{
    public class UserRolesRepository : SimpleRepository<UserRole>, IUserRolesRepository
    {
        private IApplicationUser _applicationUser;
        private ILocalizationManager _localizationManager;
        private readonly ThexIdentityDbContext _context;

        public UserRolesRepository(
         ThexIdentityDbContext context,
         IApplicationUser applicationUser,
         ILocalizationManager localizationManager) : base(context)
        {
            _applicationUser = applicationUser;
            _localizationManager = localizationManager;
            _context = context;
        }

        private async Task<List<UserRole>> GetUserRolesIdList(Guid userId, int? propertyId, int? brandId, int? chainId)
            => await _context.UserRoles
                        .Where(e => e.UserId == userId && !e.IsDeleted &&
                                    (propertyId.HasValue && propertyId.Value == e.PropertyId ||
                                    brandId.HasValue && brandId.Value == e.BrandId ||
                                    chainId.HasValue && chainId.Value == e.ChainId))
                        .ToListAsync();

        public async Task CreateOrUpdateUserRoleAsync(Guid userId, List<UserRoleDto> userRoleDtoList, int? propertyId = null, int? brandId = null, int? chainId = null)
        {
            if (userRoleDtoList == null)
                return;

            var userRoleList = await GetUserRolesIdList(userId, propertyId, brandId, chainId) ?? new List<UserRole>();

            var roleIdDtoList = userRoleDtoList.Select(e => e.RoleId).ToList();
            var roleIdList = userRoleList.Select(e => e.RoleId).ToList();

            UserRoleToExclude(userRoleList, roleIdDtoList);

            await UserRoleToAdd(userId, userRoleDtoList, roleIdList, propertyId, brandId, chainId);
        }

        private async Task UserRoleToAdd(Guid userId, List<UserRoleDto> userRoleDtoList, List<Guid> roleIdList, int? propertyId = null, int? brandId = null, int? chainId = null)
        {
            var userRoleToAdd = userRoleDtoList.Where(e => !roleIdList.Contains(e.RoleId)).ToList();
            var userRoleListToAdd = new List<UserRole>();
            foreach (var userRole in userRoleToAdd)
            {
                userRoleListToAdd.Add(new UserRole
                {
                    UserRolesId = Guid.NewGuid(),
                    UserId = userId,
                    RoleId = userRole.RoleId,
                    PropertyId = propertyId,
                    BrandId = brandId,
                    ChainId = chainId
                });
            }
            if (userRoleListToAdd.Count > 0)
                await AddUserRoleRangeAsync(userRoleListToAdd);
        }

        public async Task AddUserRoleRangeAsync(List<UserRole> userRoleList)
        {
            if (userRoleList.Count > 0)
            {
                await _context.AddRangeAsync(userRoleList);
                await _context.SaveChangesAsync();
            }
        }

        private void UserRoleToExclude(List<UserRole> userRoleList, List<Guid> roleIdDtoList)
        {
            
            var userRoleToExcludeList = userRoleList.Where(exp => !roleIdDtoList.Contains(exp.RoleId)).ToList();

            if (userRoleToExcludeList.Count > 0)
            {
                var currentDate = DateTime.UtcNow.ToZonedDateTimeLoggedUser();
                foreach (var userRoleToExclude in userRoleToExcludeList)
                    _context.Database
                           .ExecuteSqlCommand(
                           " UPDATE UserRoles SET                           " +
                           " LastModificationTime = @LastModificationTime, " +
                           " LastModifierUserId = @LastModifierUserId,     " +
                           " DeletionTime = @DeletionTime,                 " +
                           " DeleterUserId = @DeleterUserId,               " +
                           " IsDeleted = @IsDeleted                        " +
                           " WHERE UserRolesId = @UserRolesId"
                           , new SqlParameter("@LastModificationTime", currentDate)
                           , new SqlParameter("@LastModifierUserId", _applicationUser.UserUid)
                           , new SqlParameter("@DeletionTime", currentDate)
                           , new SqlParameter("@DeleterUserId", _applicationUser.UserUid)
                           , new SqlParameter("@IsDeleted", true)
                           , new SqlParameter("@UserRolesId", userRoleToExclude.UserRolesId));
            }
                
            _context.SaveChanges();
        }
    }
}
