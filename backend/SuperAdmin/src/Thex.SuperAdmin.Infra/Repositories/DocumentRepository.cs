﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Thex.SuperAdmin.Infra.Context;
using Thex.SuperAdmin.Infra.Context.Repositories;
using Thex.SuperAdmin.Infra.Entities;
using Thex.SuperAdmin.Infra.Interfaces;

namespace Thex.SuperAdmin.Infra.Repositories
{
    public class DocumentRepository : SimpleRepository<Document>, IDocumentRepository
    {
        private readonly ThexIdentityDbContext _context;

        public DocumentRepository(ThexIdentityDbContext context)
            :base(context)
        {
            _context = context;
        }

        public async Task AddRangeAsync(ICollection<Document> documentList)
        {
            await _context.Documents.AddRangeAsync(documentList);
            await _context.SaveChangesAsync();
        }

        public async Task UpdateRangeAsync(ICollection<Document> documentList)
        {
            _context.Documents.UpdateRange(documentList);

            await _context.SaveChangesAsync();
        }

        public async Task RemoveRangeAsync(ICollection<Document> documentList)
        {
            _context.Documents.RemoveRange(documentList);

            await _context.SaveChangesAsync();
        }
    }
}
