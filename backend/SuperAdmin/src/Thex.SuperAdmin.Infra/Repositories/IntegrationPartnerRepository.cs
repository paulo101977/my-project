﻿using System.Threading.Tasks;
using Thex.SuperAdmin.Infra.Context;
using Thex.SuperAdmin.Infra.Context.Repositories;
using Thex.SuperAdmin.Infra.Entities;
using Thex.SuperAdmin.Infra.Interfaces;

namespace Thex.SuperAdmin.Infra.Repositories
{
    public class IntegrationPartnerRepository : SimpleRepository<IntegrationPartner>, IIntegrationPartnerRepository
    {
        private readonly ThexIdentityDbContext _context;

        public IntegrationPartnerRepository(ThexIdentityDbContext context) : base(context)
        {
            _context = context;
        }

        public async Task<IntegrationPartner> InsertAndSaveChangesAsync(IntegrationPartner integrationPartner)
        {
            await _context.AddAsync(integrationPartner);

            await _context.SaveChangesAsync();

            return integrationPartner;
        }
    }
}
