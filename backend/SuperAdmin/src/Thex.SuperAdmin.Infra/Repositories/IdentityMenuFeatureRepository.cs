﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Threading.Tasks;
using Thex.Kernel;
using Thex.SuperAdmin.Infra.Context;
using Thex.SuperAdmin.Infra.Context.Repositories;
using Thex.SuperAdmin.Infra.Entities;
using Thex.SuperAdmin.Infra.Interfaces;
using Tnf.Localization;
using System.Linq;
using System.Collections.Generic;

namespace Thex.SuperAdmin.Infra.Repositories
{
    public class IdentityMenuFeatureRepository : SimpleRepository<IdentityMenuFeature>, IIdentityMenuFeatureRepository
    {
        private IApplicationUser _applicationUser;
        private ILocalizationManager _localizationManager;
        private readonly ThexIdentityDbContext _context;

        public IdentityMenuFeatureRepository(
         ThexIdentityDbContext context,
         IApplicationUser applicationUser,
         ILocalizationManager localizationManager) : base(context)
        {
            _applicationUser = applicationUser;
            _localizationManager = localizationManager;
            _context = context;
        }

        public async Task<IdentityMenuFeature> InsertAndSaveChangesAsync(IdentityMenuFeature entity)
        {
            var entityEntry = await _context.IdentityMenuFeatures.AddAsync(entity);
            await _context.SaveChangesAsync();
            entity = entityEntry.Entity;

            return entity;
        }

        public async Task<IdentityMenuFeature> UpdateAndSaveChangesAsync(IdentityMenuFeature entity)
        {
            AttachIfNot(entity);

            _context.Entry(entity).State = EntityState.Modified;

            await _context.SaveChangesAsync();

            return entity;
        }


        public void ToggleAndSaveIsActive(Guid id)
        {
            var permission = _context
                .IdentityMenuFeatures
                .FirstOrDefault(exp => exp.Id == id);

            var toogle = !permission.IsActive;

            permission.IsActive = toogle;
            _context.SaveChanges();
        }

    }
}
