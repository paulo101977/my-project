﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Thex.SuperAdmin.Infra.Context;
using Thex.SuperAdmin.Infra.Context.Repositories;
using Thex.SuperAdmin.Infra.Entities;
using Thex.SuperAdmin.Infra.Interfaces;

namespace Thex.SuperAdmin.Infra.Repositories
{
    public class UserTenantRepository : SimpleRepository<UserTenant>, IUserTenantRepository
    {
        private readonly ThexIdentityDbContext _context;

        public UserTenantRepository(ThexIdentityDbContext context)
            : base(context)
        {
            _context = context;
        }

        public async Task<UserTenant> InsertAndSaveChangesAsync(UserTenant userTenant)
        {
            var entity = await _context.UserTenants.AddAsync(userTenant);

            userTenant.Id = entity.Entity.Id;

            await _context.SaveChangesAsync();

            return userTenant;
        }

        public async Task InsertRangeAndSaveChangesAsync(ICollection<UserTenant> userTenantList)
        {
            await _context.UserTenants.AddRangeAsync(userTenantList);

            await _context.SaveChangesAsync();
        }
        
        public async Task RemoveAllByUserIdAsync(Guid id)
        {
            var userTenants = _context.UserTenants.Where(e => e.UserId == id).ToList();

            if(userTenants.Count > 0)
                _context.UserTenants.RemoveRange(userTenants);

            await _context.SaveChangesAsync();
        }

        public void ToggleAndSaveIsActive(Guid userId, int propertyId)
        {
            var userTenant = (from userTenants in _context.UserTenants
                        join tenants in _context.Tenants
                        on userTenants.TenantId equals tenants.Id
                        where userTenants.UserId == userId && tenants.PropertyId == propertyId
                        select userTenants)
                        .FirstOrDefault();

            var toogle = !userTenant.IsActive;

            userTenant.IsActive = toogle;

            _context.SaveChanges();
        }
    }
}
