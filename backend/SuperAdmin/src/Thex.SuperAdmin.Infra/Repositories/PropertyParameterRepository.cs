﻿using System;
using System.Threading.Tasks;
using Thex.Common.Enumerations;
using Thex.SuperAdmin.Infra.Context;
using Thex.SuperAdmin.Infra.Context.Repositories;
using Thex.SuperAdmin.Infra.Entities;
using Thex.SuperAdmin.Infra.Interfaces;

namespace Thex.SuperAdmin.Infra.Repositories
{
    public class PropertyParameterRepository : SimpleRepository<PropertyParameter>, IPropertyParameterRepository
    {
        private readonly ThexIdentityDbContext _context;

        public PropertyParameterRepository(ThexIdentityDbContext context)
            : base(context)
        {
            _context = context;
        }

        public async Task CreatePropertyParametersDefaultAsync(int propertyId, Guid tenantId)
        {
            await _context.PropertyParameters.AddAsync(new PropertyParameter()
            {
                Id = Guid.NewGuid(),
                IsActive = true,
                ApplicationParameterId = (int)ApplicationParameterEnum.ParameterCheckInTime,
                PropertyParameterValue = "14:00",
                PropertyParameterMinValue = "00:00",
                PropertyParameterMaxValue = "23:59",
                PropertyId = propertyId,
                TenantId = tenantId
            });

            await _context.PropertyParameters.AddAsync(new PropertyParameter()
            {
                Id = Guid.NewGuid(),
                IsActive = true,
                ApplicationParameterId = (int)ApplicationParameterEnum.ParameterCheckOutTime,
                PropertyParameterValue = "12:00",
                PropertyParameterMinValue = "00:00",
                PropertyParameterMaxValue = "23:59",
                PropertyId = propertyId,
                TenantId = tenantId
            });

            await _context.PropertyParameters.AddAsync(new PropertyParameter()
            {
                Id = Guid.NewGuid(),
                IsActive = true,
                ApplicationParameterId = (int)ApplicationParameterEnum.ParameterDailyLaunch,
                PropertyParameterValue = "Check-in",
                PropertyParameterMaxValue = "Check-in",
                PropertyParameterPossibleValues = "[{\"value\":\"Check -in\",\"title\":\"Check -in\"},{\"value\":\"Auditoria\",\"title\":\"Auditoria\"}]",
                PropertyId = propertyId,
                TenantId = tenantId
            });

            await _context.PropertyParameters.AddAsync(new PropertyParameter()
            {
                Id = Guid.NewGuid(),
                IsActive = true,
                ApplicationParameterId = (int)ApplicationParameterEnum.ParameterDefaultCurrency,
                PropertyId = propertyId,
                TenantId = tenantId
            });

            await _context.PropertyParameters.AddAsync(new PropertyParameter()
            {
                Id = Guid.NewGuid(),
                IsActive = true,
                ApplicationParameterId = (int)ApplicationParameterEnum.ParameterDaily,
                PropertyId = propertyId,
                TenantId = tenantId
            });

            await _context.PropertyParameters.AddAsync(new PropertyParameter()
            {
                Id = Guid.NewGuid(),
                IsActive = true,
                ApplicationParameterId = (int)ApplicationParameterEnum.ParameterDifferenceDaily,
                PropertyId = propertyId,
                TenantId = tenantId
            });

            await _context.PropertyParameters.AddAsync(new PropertyParameter()
            {
                Id = Guid.NewGuid(),
                IsActive = true,
                ApplicationParameterId = (int)ApplicationParameterEnum.ParameterChildren_1,
                PropertyParameterValue = "6",
                PropertyParameterMinValue = "0",
                PropertyParameterMaxValue = "9",
                PropertyId = propertyId,
                TenantId = tenantId
            });

            await _context.PropertyParameters.AddAsync(new PropertyParameter()
            {
                Id = Guid.NewGuid(),
                IsActive = true,
                ApplicationParameterId = (int)ApplicationParameterEnum.ParameterChildren_2,
                PropertyParameterValue = "10",
                PropertyParameterMinValue = "7",
                PropertyParameterMaxValue = "11",
                PropertyId = propertyId,
                TenantId = tenantId
            });

            await _context.PropertyParameters.AddAsync(new PropertyParameter()
            {
                Id = Guid.NewGuid(),
                IsActive = true,
                ApplicationParameterId = (int)ApplicationParameterEnum.ParameterChildren_3,
                PropertyParameterValue = "12",
                PropertyParameterMinValue = "11",
                PropertyParameterMaxValue = "17",
                PropertyId = propertyId,
                TenantId = tenantId
            });

            await _context.PropertyParameters.AddAsync(new PropertyParameter()
            {
                Id = Guid.NewGuid(),
                IsActive = true,
                ApplicationParameterId = 10,
                PropertyParameterValue = DateTime.UtcNow.Date.ToString("yyyy'-'MM'-'dd"),
                PropertyId = propertyId,
                TenantId = tenantId
            });

            await _context.PropertyParameters.AddAsync(new PropertyParameter()
            {
                Id = Guid.NewGuid(),
                IsActive = true,
                ApplicationParameterId = 11,
                PropertyId = propertyId,
                TenantId = tenantId
            });

            await _context.PropertyParameters.AddAsync(new PropertyParameter()
            {
                Id = Guid.NewGuid(),
                IsActive = true,
                ApplicationParameterId = (int)ApplicationParameterEnum.ParameterTimeZone,
                PropertyParameterValue = "America/Sao_Paulo",
                PropertyId = propertyId,
                TenantId = tenantId
            });

            await _context.PropertyParameters.AddAsync(new PropertyParameter()
            {
                Id = Guid.NewGuid(),
                IsActive = true,
                ApplicationParameterId = 13,
                PropertyParameterValue = "3",
                PropertyId = propertyId,
                TenantId = tenantId
            });

            await _context.SaveChangesAsync();
        }
    }
}
