﻿using System;
using System.Linq;
using Tnf.Localization;
using Thex.SuperAdmin.Infra.Context;
using Thex.SuperAdmin.Infra.Interfaces.ReadRepositories;
using Thex.SuperAdmin.Infra.Interfaces;
using Thex.SuperAdmin.Infra.Context.Repositories;
using Thex.SuperAdmin.Infra.Entities;
using Thex.Kernel;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Thex.SuperAdmin.Infra.Repositories
{
    public class UserRepository : SimpleRepository<User>, IUserRepository
    {
        private readonly ILocalizationManager _localizationManager;
        private readonly ITenantReadRepository _tenantReadRepository;
        private readonly ThexIdentityDbContext _context;
        private readonly IApplicationUser _applicationUser;

        public UserRepository(
            ThexIdentityDbContext context,
            ITenantReadRepository tenantReadRepository,
            IApplicationUser applicationUser,
            ILocalizationManager localizationManager)
            : base(context)
        {
            _tenantReadRepository = tenantReadRepository;
            _localizationManager = localizationManager;
            _applicationUser = applicationUser;
            _context = context;
        }

        public void ChangeCulture(Guid id, string culture)
        {
            var user = _context
                .Users
                .FirstOrDefault(exp => exp.Id == id);
                        
            if(user != null)
            {
                var tenantIdList = _context
                    .Tenants
                    .Where(e => e.Id == user.TenantId)
                    .SelectMany(e => e.ChildList)
                    .Select(e => e.Id).ToList();

                if (user.TenantId == _applicationUser.TenantId ||
                    tenantIdList.Contains(_applicationUser.TenantId))
                {
                    user.PreferredCulture = culture;
                    user.PreferredLanguage = culture;
                }
            }

        }

        public void ToggleAndSaveIsActive(Guid id)
        {
            var user = _context
                .Users
                .FirstOrDefault(exp => exp.Id == id && exp.TenantId == _applicationUser.TenantId);

            var toogle = !user.IsActive;

            user.IsActive = toogle;

            _context.SaveChanges();
        }

        public async Task<User> InsertAndSaveChangesAsync(User user)
        {
            var entity = await _context.Users.AddAsync(user);

            user.Id = entity.Entity.Id;

            await _context.SaveChangesAsync();

            return user;
        }

        public async Task ToggleIsActiveAsync(Guid id)
        {
            var user = await _context.Users.FirstOrDefaultAsync(u => u.Id == id);
            if (user == null) return;

            user.IsActive = !user.IsActive;

            await _context.SaveChangesAsync();
        }

        public async Task UpdateAndSaveChangesAsync(User user)
        {
            AttachIfNot(user);

            _context.Entry(user).State = EntityState.Modified;
            _context.Users.Update(user);

            await _context.SaveChangesAsync();
        }
    }
}