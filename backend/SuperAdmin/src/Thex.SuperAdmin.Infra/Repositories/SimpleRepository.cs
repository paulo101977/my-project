﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Thex.SuperAdmin.Infra.Interfaces;

namespace Thex.SuperAdmin.Infra.Context.Repositories
{
    public class SimpleRepository<TEntity> : ISimpleRepository<TEntity> where TEntity : class
    {
        protected readonly ThexIdentityDbContext Db;
        protected readonly DbSet<TEntity> DbSet;

        protected virtual DbSet<TEntity> Table => Db.Set<TEntity>();

        public SimpleRepository(ThexIdentityDbContext context)
        {
            Db = context;
            DbSet = Db.Set<TEntity>();
        }

        public virtual void Add(TEntity obj)
        {
            DbSet.Add(obj);
        }

        public virtual TEntity GetById(Guid id)
        {
            return DbSet.Find(id);
        }

        public virtual IQueryable<TEntity> GetAll()
        {
            return DbSet;
        }

        public virtual void Update(TEntity obj)
        {
            DbSet.Update(obj);
        }

        public virtual void Remove(Guid id)
        {
            DbSet.Remove(DbSet.Find(id));
        }

        public int SaveChanges()
        {
            return Db.SaveChanges();
        }

        protected virtual void AttachIfNot(TEntity entity)
        {
            var entry = Db.ChangeTracker.Entries().FirstOrDefault(ent => ent.Entity == entity);

            // Este cenário quer dizer que já existe um objeto do mesmo tipo com tracking (não a mesma instância).
            // Se existe ele deve ser removido do ChangeTracker
            if (entry == null)
            {
                var otherEntry = Db.ChangeTracker.Entries().FirstOrDefault(ent => ent.Entity.GetType().Name == entity.GetType().Name);
                if (otherEntry != null)
                    otherEntry.State = EntityState.Detached;
            }
            else
            {
                return;
            }

            Table.Attach(entity);
        }

        public void Dispose()
        {
            Db.Dispose();
            GC.SuppressFinalize(this);
        }
    }
}