﻿using System;
using System.Linq;
using Tnf.Localization;
using Thex.SuperAdmin.Infra.Context;
using Thex.SuperAdmin.Infra.Interfaces.ReadRepositories;
using Thex.SuperAdmin.Infra.Interfaces;
using Thex.SuperAdmin.Infra.Context.Repositories;
using Thex.SuperAdmin.Infra.Entities;
using Thex.Kernel;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Thex.Common.Enumerations;

namespace Thex.SuperAdmin.Infra.Repositories
{
    public class PropertyRepository : SimpleRepository<Property>, IPropertyRepository
    {
        private readonly ILocalizationManager _localizationManager;
        private readonly ITenantReadRepository _tenantReadRepository;
        private readonly ThexIdentityDbContext _context;
        private readonly IApplicationUser _applicationUser;

        public PropertyRepository(
            ThexIdentityDbContext context,
            ITenantReadRepository tenantReadRepository,
            IApplicationUser applicationUser,
            ILocalizationManager localizationManager)
            : base(context)
        {
            _tenantReadRepository = tenantReadRepository;
            _localizationManager = localizationManager;
            _applicationUser = applicationUser;
            _context = context;
        }

        public async Task<Property> InsertAndSaveChangesAsync(Property property)
        {
            var entityEntry = await _context.Properties.AddAsync(property);

            property = entityEntry.Entity;
            
            await _context.SaveChangesAsync();

            return property;
        }

        public async Task<Property> UpdateAndSaveChangesAsync(Property entity)
        {
            AttachIfNot(entity);

            _context.Entry(entity).State = EntityState.Modified;

            await _context.SaveChangesAsync();
            return entity;
        }

        #region PropertyStatusHistory
        public async Task ChangeStatusAndSaveHistoryAsync(int propertyId, int propertyStatusId)
        {
            var property = await _context.Properties.FirstOrDefaultAsync(x => x.Id == propertyId);
            if (property == null) return;

            _context.Entry(property).State = EntityState.Modified;
            property.PropertyStatusId = propertyStatusId;

            await _context.PropertyStatusHistories.AddAsync(new PropertyStatusHistory()
            {
                Id = Guid.NewGuid(),
                StatusId = propertyStatusId,
                PropertyId = propertyId
            });

            await _context.SaveChangesAsync();
        }

        public async Task ToggleIsBlockedPropertyAndSaveHistoryAsync(int propertyId)
        {
            var property = await _context.Properties.FirstOrDefaultAsync(x => x.Id == propertyId);
            if (property == null) return;

            _context.Entry(property).State = EntityState.Modified;
            property.IsBlocked = property.IsBlocked.HasValue ? !property.IsBlocked : true;

            await _context.PropertyStatusHistories.AddAsync(new PropertyStatusHistory()
            {
                Id = Guid.NewGuid(),
                StatusId = (int)(property.IsBlocked.Value ? PropertyStatusEnum.Blocked : PropertyStatusEnum.Unblocked),
                PropertyId = propertyId
            });

            await _context.SaveChangesAsync();
        }

        public async Task SaveHistoryPropertyBlockedAsync(int propertyId)
        {
            var property = await _context.Properties.FirstOrDefaultAsync(x => x.Id == propertyId);
            if (property == null) return;

            await _context.PropertyStatusHistories.AddAsync(new PropertyStatusHistory()
            {
                Id = Guid.NewGuid(),
                StatusId = (int)(property.IsBlocked.Value ? PropertyStatusEnum.Blocked : PropertyStatusEnum.Unblocked),
                PropertyId = propertyId
            });

            await _context.SaveChangesAsync();
        }
        #endregion

        #region PropertyContract
        public async Task InsertPropertyContractAsync(PropertyContract propertyContract)
        {
            await _context.PropertyContracts.AddAsync(propertyContract);

            await _context.SaveChangesAsync();
        }

        public async Task UpdatePropertyContractAsync(PropertyContract propertyContract)
        {
            AttachPropertyContract(propertyContract);

            _context.Entry(propertyContract).State = EntityState.Modified;

            await _context.SaveChangesAsync();
        }

        private void AttachPropertyContract(PropertyContract entity)
        {
            var entry = Db.ChangeTracker.Entries().FirstOrDefault(ent => ent.Entity == entity);

            // Este cenário quer dizer que já existe um objeto do mesmo tipo com tracking (não a mesma instância).
            // Se existe ele deve ser removido do ChangeTracker
            if (entry == null)
            {
                var otherEntry = Db.ChangeTracker.Entries().FirstOrDefault(ent => ent.Entity.GetType().Name == entity.GetType().Name);
                if (otherEntry != null)
                    otherEntry.State = EntityState.Detached;
            }
            else
            {
                return;
            }
        }
        #endregion

    }
}