﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Threading.Tasks;
using Thex.Kernel;
using Thex.SuperAdmin.Infra.Context;
using Thex.SuperAdmin.Infra.Context.Repositories;
using Thex.SuperAdmin.Infra.Entities;
using Thex.SuperAdmin.Infra.Interfaces;
using Tnf.Localization;
using System.Linq;
using System.Collections.Generic;
using Thex.SuperAdmin.Dto;

namespace Thex.SuperAdmin.Infra.Repositories
{
    public class RolesRepository : SimpleRepository<Roles>, IRolesRepository
    {
        private IApplicationUser _applicationUser;
        private ILocalizationManager _localizationManager;
        private readonly ThexIdentityDbContext _context;

        public RolesRepository(
         ThexIdentityDbContext context,
         IApplicationUser applicationUser,
         ILocalizationManager localizationManager) : base(context)
        {
            _applicationUser = applicationUser;
            _localizationManager = localizationManager;
            _context = context;
        }

        public async Task CreateOrUpdateRolePermissionAsync(Guid roleId, List<IdentityRolePermissionDto> rolePermissionDtoList)
        {
            if (rolePermissionDtoList == null || rolePermissionDtoList.Count == 0)
                return;

            var rolePermissionList = await _context.IdentityRolePermissions.Where(e => e.RoleId == roleId).ToListAsync();
            var permissionIdDtoList = rolePermissionDtoList.Select(e => e.IdentityPermissionId).ToList();
            var permissionIdList = rolePermissionList.Select(e => e.IdentityPermissionId).ToList();

            RolePermissionToExclude(rolePermissionList, permissionIdDtoList);

            RolePermissionToUpdate(rolePermissionDtoList, rolePermissionList, permissionIdDtoList);

            await RolePermissionToAdd(roleId, rolePermissionDtoList, permissionIdList);
        }

        private async Task RolePermissionToAdd(Guid roleId, List<IdentityRolePermissionDto> rolePermissionDtoList, List<Guid> permissionIdList)
        {

            //adicionar 
            var rolePermissionToAdd = rolePermissionDtoList.Where(e => !permissionIdList.Contains(e.IdentityPermissionId)).ToList();
            var rolePermissionListToAdd = new List<IdentityRolePermission>();
            foreach (var rolePermission in rolePermissionToAdd)
            {
                rolePermissionListToAdd.Add(new IdentityRolePermission
                {
                    Id = Guid.NewGuid(),
                    IdentityPermissionId = rolePermission.IdentityPermissionId,
                    RoleId = roleId,
                    IsActive = rolePermission.IsActive
                });
            }
            if (rolePermissionListToAdd.Count > 0)
                await AddRolePermissionRangeAsync(rolePermissionListToAdd);
        }

        private void RolePermissionToExclude(List<IdentityRolePermission> rolePermissionList, List<Guid> permissionIdDtoList)
        {
            //exclusão
            var rolePermissionToExcludeList = rolePermissionList.Where(exp => !permissionIdDtoList.Contains(exp.IdentityPermissionId)).ToList();
            //efetuo a exclusão
            if (rolePermissionToExcludeList.Count > 0)
                _context.RemoveRange(rolePermissionToExcludeList);

            _context.SaveChanges();
        }

        private void RolePermissionToUpdate(List<IdentityRolePermissionDto> rolePermissionDtoList, List<IdentityRolePermission> rolePermissionList, List<Guid> permissionIdDtoList)
        {

            //atualizar
            var rolePermissionToUpdateList = rolePermissionList.Where(exp => permissionIdDtoList.Contains(exp.IdentityPermissionId)).ToList(); ;
            foreach (var rolePermissionToUpdate in rolePermissionToUpdateList)
                rolePermissionToUpdate.IsActive = rolePermissionDtoList
                    .FirstOrDefault(e => e.IdentityPermissionId == rolePermissionToUpdate.IdentityPermissionId)?
                    .IsActive ?? false;

            _context.UpdateRange(rolePermissionToUpdateList);

            _context.SaveChanges();
        }

        public async Task AddRolePermissionRangeAsync(List<IdentityRolePermission> rolePermissionList)
        {
            if (rolePermissionList.Count > 0)
            {
                await _context.AddRangeAsync(rolePermissionList);
                await _context.SaveChangesAsync();
            }
        }

        public async Task RemoveAsync(Guid id)
        {
            base.Remove(id);

            await _context.SaveChangesAsync();
        }

        public void ToggleAndSaveIsActive(Guid id)
        {
            var role = _context
                .Roles
                .FirstOrDefault(exp => exp.Id == id);

            var toogle = !role.IsActive;

            role.IsActive = toogle;

            _context.SaveChanges();
        }

    }
}
