﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Threading.Tasks;
using Thex.Kernel;
using Thex.SuperAdmin.Infra.Context;
using Thex.SuperAdmin.Infra.Context.Repositories;
using Thex.SuperAdmin.Infra.Entities;
using Thex.SuperAdmin.Infra.Interfaces;
using Tnf.Localization;
using System.Linq;
using System.Collections.Generic;

namespace Thex.SuperAdmin.Infra.Repositories
{
    public class IdentityPermissionRepository : SimpleRepository<IdentityPermission>, IIdentityPermissionRepository
    {
        private IApplicationUser _applicationUser;
        private ILocalizationManager _localizationManager;
        private readonly ThexIdentityDbContext _context;

        public IdentityPermissionRepository(
         ThexIdentityDbContext context,
         IApplicationUser applicationUser,
         ILocalizationManager localizationManager) : base(context)
        {
            _applicationUser = applicationUser;
            _localizationManager = localizationManager;
            _context = context;
        }

        public async Task<IdentityPermission> InsertAndSaveChangesAsync(IdentityPermission entity)
        {
            var entityEntry = await _context.IdentityPermissions.AddAsync(entity);
            await _context.SaveChangesAsync();
            entity = entityEntry.Entity;

            return entity;
        }

        public async Task<IdentityPermission> UpdateAndSaveChangesAsync(IdentityPermission newPermission, IdentityPermission oldPermission)
        {
            if (oldPermission != null)
            {
                RemovePermissionFeatures(newPermission, oldPermission);

                await _context.SaveChangesAsync();

                newPermission.Id = oldPermission.Id;

                _context.Entry(oldPermission).CurrentValues.SetValues(newPermission);
                
                foreach (var permissionFeature in newPermission.IdentityPermissionFeatureList)
                    InsertOrUpdatePermissionFeatureList(newPermission, oldPermission, permissionFeature);

                await _context.SaveChangesAsync();
            }

            return oldPermission;
        }

        private void InsertOrUpdatePermissionFeatureList(IdentityPermission newPermission, IdentityPermission oldPermission, IdentityPermissionFeature permissionFeature)
        {
            var oldPermissionFeature = oldPermission.IdentityPermissionFeatureList.Where(e => e.IdentityFeatureId == permissionFeature.IdentityFeatureId).FirstOrDefault();

            if (oldPermissionFeature != null)
            {
                permissionFeature.Id = oldPermissionFeature.Id;
                permissionFeature.IdentityFeatureId = oldPermissionFeature.IdentityFeatureId;
                permissionFeature.IdentityPermissionId = oldPermissionFeature.IdentityPermissionId;

                _context.Entry(oldPermissionFeature).CurrentValues.SetValues(permissionFeature);
            }
            else
                oldPermission.IdentityPermissionFeatureList.Add(permissionFeature);
        }

        private void RemovePermissionFeatures(IdentityPermission newPermission, IdentityPermission oldPermission)
        {
            List<Guid> featureIdsExceptList = GetFeatureIds(newPermission);

            RemovePermissionFeaturesExceptListOfFeaturesIds(oldPermission, featureIdsExceptList);
        }

        private void RemovePermissionFeaturesExceptListOfFeaturesIds(IdentityPermission oldPermission, List<Guid> featureIdsExceptList)
        {
            var permissionFeatureToExclude = oldPermission.IdentityPermissionFeatureList.Where(exp => !featureIdsExceptList.Contains(exp.IdentityFeatureId)).ToList();

            if (permissionFeatureToExclude.Count > 0)
                _context.RemoveRange(permissionFeatureToExclude);
        }

        private List<Guid> GetFeatureIds(IdentityPermission permission)
        {
            return permission.IdentityPermissionFeatureList.Select(r => r.IdentityFeatureId).ToList();
        }

        public async Task RemoveAsync(Guid id)
        {
            var permissionFeatureList = await _context
                .IdentityPermissionFeatures
                .Where(e => e.IdentityPermissionId == id).ToListAsync();

            if (permissionFeatureList != null &&
                permissionFeatureList.Count() > 0)
                _context.RemoveRange(permissionFeatureList);

            base.Remove(id);

            await _context.SaveChangesAsync();
        }

        public void ToggleAndSaveIsActive(Guid id)
        {
            var permission = _context
                .IdentityPermissions
                .FirstOrDefault(exp => exp.Id == id);

            var toogle = !permission.IsActive;

            permission.IsActive = toogle;

            _context.SaveChanges();
        }

    }
}
