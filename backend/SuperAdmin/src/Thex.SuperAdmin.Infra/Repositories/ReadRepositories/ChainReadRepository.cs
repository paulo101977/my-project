﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Thex.Kernel;
using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Dto.Dto;
using Thex.SuperAdmin.Infra.Context;
using Thex.SuperAdmin.Infra.Context.Repositories;
using Thex.SuperAdmin.Infra.Entities;
using Thex.SuperAdmin.Infra.Interfaces.ReadRepositories;
using Tnf.Dto;
using Tnf.Localization;
using Tnf.Specifications;

namespace Thex.SuperAdmin.Infra.Repositories.ReadRepositories
{ 
    public class ChainReadRepository : SimpleRepository<Property>, IChainReadRepository
    {
        private readonly ILocalizationManager _localizationManager;
        private readonly ThexIdentityDbContext _context;
        private readonly IApplicationUser _applicationUser;

        public ChainReadRepository(
            ThexIdentityDbContext context,
            IApplicationUser applicationUser,
            ILocalizationManager localizationManager) : base(context)
        {
            _applicationUser = applicationUser;
            _localizationManager = localizationManager;
            _context = context;
        }

        public ThexIdentityDbContext Context
        {
            get
            {
                return _context;
            }
        }

        public async Task<IListDto<ChainDto>> GetAllDtoAsync(GetAllChainDto request)
        {
            var dbBaseQuery = GetBaseQuery(GetAllFilters(request.SearchData));

            var totalItems = await dbBaseQuery.CountAsync();

            dbBaseQuery = GetAllPaging(request, dbBaseQuery);

            var totalItemsFiltered = await dbBaseQuery.CountAsync();
            var result = await dbBaseQuery.OrderBy(x=>x.Name).ToListAsync();

            return new ThexListDto<ChainDto>
            {
                Items = result,
                HasNext = GetAllHasNext(request, totalItems, totalItemsFiltered),
                TotalItems = totalItems
            };
        }

        public async Task<ChainDto> GetDtoByIdAsync(DefaultIntRequestDto request)
        {
            return await GetBaseQuery(e => e.Id == request.Id).FirstOrDefaultAsync();
        }

        public async Task<Chain> GetByIdAsync(DefaultIntRequestDto request)
        {
            return await _context.Chains.FirstOrDefaultAsync(e => e.Id == request.Id);
        }

        #region private methods

        private Expression<Func<ChainDto, bool>> GetAllFilters(string searchData)
        {
            Expression<Func<ChainDto, bool>> filters = null;

            if (!string.IsNullOrEmpty(searchData))
            {
                //chain
                filters = (e => e.Name.ToLower().Contains(searchData));
                //brand
                filters = filters.Or((e => e.BrandName.ToLower().Contains(searchData)));
                //property
                filters = filters.Or((e => e.PropertyName.ToLower().Contains(searchData)));
                //company
                filters = filters.Or((e => e.TradeName.ToLower().Contains(searchData)));
                filters = filters.Or((e => e.ShortName.ToLower().Contains(searchData)));
            }

            return filters;
        }

        private IQueryable<ChainDto> GetBaseQuery(Expression<Func<ChainDto, bool>> filters)
            => (from c in _context.Chains.AsNoTracking().IgnoreQueryFilters()
                join t in _context.Tenants.AsNoTracking().IgnoreQueryFilters() on c.TenantId equals t.Id
                join b in _context.Brands.AsNoTracking().IgnoreQueryFilters() on c.Id equals b.ChainId
                into tb from b in tb.DefaultIfEmpty()
                join p in _context.Properties.AsNoTracking().IgnoreQueryFilters() on b.Id equals p.BrandId
                into bp from p in bp.DefaultIfEmpty()
                join co in _context.Companies.AsNoTracking().IgnoreQueryFilters() on p.CompanyId equals co.Id
                into pc from co in pc.DefaultIfEmpty()
                select new ChainDto
                {
                    Id = c.Id,
                    Name = c.Name,
                    TenantId = t.Id,
                    TenantName = t.TenantName,
                    PropertyId = p != null ? p.Id : 0,
                    PropertyUid = p != null ? p.PropertyUId : Guid.Empty,
                    PropertyName = p != null ? p.Name : string.Empty,
                    BrandId = b != null ? b.Id : 0,
                    BrandName = b != null ? b.Name : string.Empty,
                    CompanyId = co != null ? co.Id : 0,
                    CompanyUid = co != null ? co.CompanyUid : Guid.Empty,
                    ShortName = co != null ? co.ShortName : string.Empty,
                    TradeName = co != null ? co.TradeName : string.Empty
                })
                .Where(filters ?? (e => true))
                .GroupBy(e => new { e.Id, e.Name, e.TenantId, e.TenantName })
                .OrderBy(x => x.Key.Name)
                .Select(e => new ChainDto { Id = e.Key.Id, Name = e.Key.Name, TenantId = e.Key.TenantId, TenantName = e.Key.TenantName });


        private bool GetAllHasNext(GetAllChainDto request, int totalItems, int totalItemsFiltered)
            => totalItems > ((request.Page - 1) * request.PageSize) + totalItemsFiltered;

        private IQueryable<ChainDto> GetAllPaging(GetAllChainDto request, IQueryable<ChainDto> dbBaseQuery)
        {
            var pageSize = request.PageSize ?? 1000;
            var page = request.Page.HasValue ? request.Page.Value : 1;

            dbBaseQuery = dbBaseQuery.Skip(pageSize * (page - 1)).Take(pageSize);

            return dbBaseQuery;
        }
        #endregion

        public async Task<Guid> GetTenantId(int chainId)
            => await _context.Chains.AsNoTracking()
                    .Where(x => x.Id == chainId)
                    .Select(x => x.TenantId)
                    .FirstOrDefaultAsync();
    }
}