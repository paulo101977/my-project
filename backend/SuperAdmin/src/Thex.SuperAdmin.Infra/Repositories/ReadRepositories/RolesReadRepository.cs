﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Thex.Kernel;
using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Dto.Dto;
using Thex.SuperAdmin.Infra.Context;
using Thex.SuperAdmin.Infra.Context.Repositories;
using Thex.SuperAdmin.Infra.Entities;
using Thex.SuperAdmin.Infra.Interfaces.ReadRepositories;
using Tnf.Localization;
using System.Linq;
using X.PagedList;
using Thex.Common;
using Thex.SuperAdmin.Dto.Enumerations;
using Thex.Dto;
using System.Linq.Expressions;
using System.Globalization;
using Tnf.Specifications;

namespace Thex.SuperAdmin.Infra.Repositories.ReadRepositories
{
    public class RolesReadRepository : SimpleRepository<Roles>, IRolesReadRepository
    {
        private IApplicationUser _applicationUser;
        private ILocalizationManager _localizationManager;
        private readonly ThexIdentityDbContext _context;
        private readonly IConfiguration _configuration;

        public ThexIdentityDbContext Context { get { return _context; } }

        public RolesReadRepository(
         ThexIdentityDbContext context,
         IApplicationUser applicationUser,
         IConfiguration configuration,
         ILocalizationManager localizationManager) : base(context)
        {
            _applicationUser = applicationUser;
            _localizationManager = localizationManager;
            _context = context;
            _configuration = configuration;
        }

        public async Task<RolesDto> GetByIdAsync(Guid id)
        {
            var roleDto = await GetBaseQuery(x => x.Id == id).FirstOrDefaultAsync();

            if (roleDto != null)
                roleDto.TranslationList = await Context
                    .IdentityTranslations
                    .Where(e => e.IdentityOwnerId == roleDto.Id)
                    .Select(e => new SimpleTranslationDto
                    {
                        Name = e.Name,
                        LanguageIsoCode = e.LanguageIsoCode
                    }).ToListAsync();

            return roleDto;
        }

        public async Task<ThexListDto<RolesDto>> GetAllDtoByFilterAsync(GetAllRoleDto request)
        {
            var dbBaseQuery = GetBaseQuery(GetAllFilters(request));

            var totalItems = await dbBaseQuery.CountAsync();

            dbBaseQuery = GetAllPaging(request, dbBaseQuery);

            var totalItemsFiltered = await dbBaseQuery.CountAsync();
            var result = await dbBaseQuery.ToListAsync();

            return new ThexListDto<RolesDto>
            {
                Items = result,
                HasNext = GetAllHasNext(request, totalItems, totalItemsFiltered),
                TotalItems = totalItems
            };
        }

        public async Task<List<IdentityRolePermissionDto>> GetAllRolePermissionDtoByRoleIdAsync(Guid roleId)
        {
            var result = new List<IdentityRolePermissionDto>();
            var dbResult = await _context
                .IdentityRolePermissions.Where(e => e.RoleId == roleId).ToListAsync();

            foreach (var role in dbResult)
                result.Add(role.MapTo<IdentityRolePermissionDto>());

            return result;
        }

        public async Task<int> Count(List<Guid> roleIdList)
            => await _context.Roles.AsNoTracking().CountAsync(e => roleIdList.Contains(e.Id));


        #region private methods

        private Expression<Func<RolesDto, bool>> GetAllFilters(GetAllRoleDto request)
        {
            Expression<Func<RolesDto, bool>> filters = null;

            if (!string.IsNullOrEmpty(request.SearchData))
            {
                filters = (e => e.Name.ToLower().Contains(request.SearchData));
                filters = filters.Or(e => e.IdentityProductName.ToLower().Contains(request.SearchData));
            }

            if(request.IsActive.HasValue)
                filters = filters.And(e => e.IsActive == request.IsActive.Value);

            return filters;
        }

        private IQueryable<RolesDto> GetBaseQuery(Expression<Func<RolesDto, bool>> filters)
            => (from role in _context.Roles.AsNoTracking()
                join product in _context.IdentityProducts.AsNoTracking() on role.IdentityProductId equals product.Id

                join roleLanguage in _context.IdentityTranslations.AsNoTracking() on
                new { p1 = role.Id, p2 = CultureInfo.CurrentCulture.Name.ToLower() } equals new { p1 = roleLanguage.IdentityOwnerId, p2 = roleLanguage.LanguageIsoCode.ToLower() }
                into dl
                from roleLanguage in dl.DefaultIfEmpty()

                select new RolesDto
                {
                    Id = role.Id,
                    Name = roleLanguage != null ? roleLanguage.Name : role.Name,
                    IsActive = role.IsActive,
                    IdentityProductId = product.Id,
                    IdentityProductName = product.Name
                })
                .Where(filters ?? (e => true));

        private bool GetAllHasNext(GetAllRoleDto request, int totalItems, int totalItemsFiltered)
            => totalItems > ((request.Page - 1) * request.PageSize) + totalItemsFiltered;

        private IQueryable<RolesDto> GetAllPaging(GetAllRoleDto request, IQueryable<RolesDto> dbBaseQuery)
        {
            var pageSize = request.PageSize ?? 1000;
            var page = request.Page.HasValue ? request.Page.Value : 1;

            dbBaseQuery = dbBaseQuery.Skip(pageSize * (page - 1)).Take(pageSize);

            return dbBaseQuery;
        }

        #endregion
    }
}
