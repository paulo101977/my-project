﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Thex.Kernel;
using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Dto.Dto;
using Thex.SuperAdmin.Infra.Context;
using Thex.SuperAdmin.Infra.Context.Repositories;
using Thex.SuperAdmin.Infra.Entities;
using Thex.SuperAdmin.Infra.Interfaces.ReadRepositories;
using Tnf.Localization;
using System.Linq;
using X.PagedList;
using Thex.Dto;
using System.Linq.Expressions;
using Tnf.Specifications;
using System.Globalization;

namespace Thex.SuperAdmin.Infra.Repositories.ReadRepositories
{
    public class IdentityMenuFeatureReadRepository : SimpleRepository<IdentityMenuFeature>, IIdentityMenuFeatureReadRepository
    {
        private IApplicationUser _applicationUser;
        private ILocalizationManager _localizationManager;
        private readonly ThexIdentityDbContext _context;
        private readonly IConfiguration _configuration;

        public ThexIdentityDbContext Context { get { return _context; } }

        public IdentityMenuFeatureReadRepository(
         ThexIdentityDbContext context,
         IApplicationUser applicationUser,
         IConfiguration configuration,
         ILocalizationManager localizationManager) : base(context)
        {
            _applicationUser = applicationUser;
            _localizationManager = localizationManager;
            _context = context;
            _configuration = configuration;
        }

        private IQueryable<IdentityMenuFeatureDto> GetBaseQuery(Expression<Func<IdentityMenuFeatureDto, bool>> filters)
            => (from menuFeature in _context.IdentityMenuFeatures.AsNoTracking()

                join menuLanguage in _context.IdentityTranslations.AsNoTracking() on
                new { p1 = menuFeature.Id, p2 = CultureInfo.CurrentCulture.Name.ToLower() } equals new { p1 = menuLanguage.IdentityOwnerId, p2 = menuLanguage.LanguageIsoCode.ToLower() }
                into ml
                from menuLanguage in ml.DefaultIfEmpty()

                join feature in _context.IdentityFeatures.AsNoTracking() on menuFeature.IdentityFeatureId equals feature.Id
                join featureLanguage in _context.IdentityTranslations.AsNoTracking() on
                new { p1 = feature.Id, p2 = CultureInfo.CurrentCulture.Name.ToLower() } equals new { p1 = featureLanguage.IdentityOwnerId, p2 = featureLanguage.LanguageIsoCode.ToLower() }
                into dl
                from featureLanguage in dl.DefaultIfEmpty()
                select new IdentityMenuFeatureDto
                {
                    Id = menuFeature.Id,
                    IdentityProductId = menuFeature.IdentityProductId,
                    IdentityModuleId = menuFeature.IdentityModuleId,
                    IdentityFeatureParentId = menuFeature.IdentityFeatureParentId,
                    IsActive = menuFeature.IsActive,
                    OrderNumber = menuFeature.OrderNumber,
                    Level = menuFeature.Level,
                    Description = menuLanguage != null ? menuLanguage.Description : menuFeature.Description,
                    IdentityFeatureId = menuFeature.IdentityFeatureId,
                    IdentityFeature = new IdentityFeatureDto
                    {
                        Id = feature.Id,
                        IdentityProductId = feature.IdentityProductId,
                        IdentityModuleId = feature.IdentityModuleId,
                        Name = featureLanguage != null ? featureLanguage.Name : feature.Name,
                        Description = featureLanguage != null ? featureLanguage.Description : feature.Description,
                        IsInternal = feature.IsInternal,
                        Key = feature.Key
                    }
                })
                .Where(filters ?? (e => true));

        public async Task<MenuItemDto> GetMenuItemWithParents(Guid featureId)
            => await FillParentRecursive(featureId);

        private async Task<MenuItemDto> FillParentRecursive(Guid featureId)
        {
            var menuFeature = await GetBaseQuery(
                e => e.IdentityFeature.Id == featureId && 
                !e.IdentityFeature.IsInternal)
                .Select(e => new MenuItemDto
                {
                    Id = e.Id,
                    ProductId = e.IdentityProductId,
                    ModuleId = e.IdentityModuleId,
                    IsActive = e.IsActive,
                    OrderNumber = e.OrderNumber,
                    Description = e.Description,
                    FeatureId = e.IdentityFeature.Id,
                    FeatureName = e.IdentityFeature.Name,
                    IsInternal = e.IdentityFeature.IsInternal,
                    ParentId = e.IdentityFeatureParentId
                }).FirstOrDefaultAsync();

            if (menuFeature != null && menuFeature.ParentId.HasValue)
                menuFeature.Parent = await FillParentRecursive(menuFeature.ParentId.Value);

            return menuFeature;
        }

        public async Task<bool> Exists(Guid id)
            => await _context.IdentityMenuFeatures.AnyAsync(x => x.Id == id);

        public new async Task<IdentityMenuFeature> GetById(Guid id)
            => await _context.IdentityMenuFeatures.FirstOrDefaultAsync(x => x.Id == id);

        public new async Task<List<IdentityMenuFeatureDto>> GetAll()
            => await GetBaseQuery(null).ToListAsync();

        public async Task<IdentityMenuFeatureDto> GetDtoAsync(Guid id)
            => await GetBaseQuery(x => x.Id == id).FirstOrDefaultAsync();

        public new async Task<List<IdentityMenuFeatureDto>> GetAllWithFiltersAsync(Expression<Func<IdentityMenuFeatureDto, bool>> filters)
            => await GetBaseQuery(filters).ToListAsync();
    }
}
