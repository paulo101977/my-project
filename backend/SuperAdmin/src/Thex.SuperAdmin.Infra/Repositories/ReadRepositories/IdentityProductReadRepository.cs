﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Thex.Kernel;
using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Dto.Dto;
using Thex.SuperAdmin.Infra.Context;
using Thex.SuperAdmin.Infra.Context.Repositories;
using Thex.SuperAdmin.Infra.Entities;
using Thex.SuperAdmin.Infra.Interfaces.ReadRepositories;
using Tnf.Localization;
using System.Linq;
using X.PagedList;
using Thex.Common;
using Thex.SuperAdmin.Dto.Enumerations;

namespace Thex.SuperAdmin.Infra.Repositories.ReadRepositories
{
    public class IdentityProductReadRepository : SimpleRepository<IdentityProduct>, IIdentityProductReadRepository
    {
        private IApplicationUser _applicationUser;
        private ILocalizationManager _localizationManager;
        private readonly ThexIdentityDbContext _context;
        private readonly IConfiguration _configuration;

        public ThexIdentityDbContext Context { get { return _context; } }

        public IdentityProductReadRepository(
         ThexIdentityDbContext context,
         IApplicationUser applicationUser,
         IConfiguration configuration,
         ILocalizationManager localizationManager) : base(context)
        {
            _applicationUser = applicationUser;
            _localizationManager = localizationManager;
            _context = context;
            _configuration = configuration;
        }

        public async Task<ThexListDto<IdentityProductDto>> GetAllAsync()
        {
            var result = new List<IdentityProductDto>();
            var dbResult = await base.GetAll().ToListAsync();

            foreach (var identityProduct in dbResult)
            {
                identityProduct.Name = _localizationManager.GetString(AppConsts.LocalizationSourceName, ((IdentityProductEnum)identityProduct.Id).ToString());
                result.Add(identityProduct.MapTo<IdentityProductDto>());
            }

            return new ThexListDto<IdentityProductDto>()
            {
                HasNext = false,
                Items = result,
                TotalItems = result.Count()
            };
        }
    }
}
