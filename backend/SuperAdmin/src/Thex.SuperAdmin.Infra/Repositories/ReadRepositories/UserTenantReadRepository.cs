﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Thex.SuperAdmin.Infra.Context;
using Thex.SuperAdmin.Infra.Context.Repositories;
using Thex.SuperAdmin.Infra.Entities;
using Thex.SuperAdmin.Infra.Interfaces.ReadRepositories;

namespace Thex.SuperAdmin.Infra.Repositories.ReadRepositories
{
    public class UserTenantReadRepository : SimpleRepository<UserTenant>, IUserTenantReadRepository
    {
        private readonly ThexIdentityDbContext _context;

        public UserTenantReadRepository(ThexIdentityDbContext context) : base(context)
        {
            _context = context;
        }

        public async Task<ICollection<UserTenant>> GetAllByUserIdAsync(Guid id)
        {
            return await _context.UserTenants.Include(ut => ut.Tenant)                 
                            .Include(us => us.Tenant).ThenInclude(t => t.Property)
                            .Include(us => us.Tenant).ThenInclude(t => t.Brand)
                            .Include(us => us.Tenant).ThenInclude(t => t.Chain)
                            .Where(x => x.UserId == id).ToListAsync();
        }
        
        public async Task<bool> Any(Guid userId, Guid tenantId)
            => await _context
                .UserTenants
                .IgnoreQueryFilters()
                .AsNoTracking()
                .AnyAsync(e => e.UserId == userId && e.TenantId == tenantId);

        public int GetChainIdByUserId(Guid userId)
        {
            var userTenantList = _context.UserTenants
                                      .Include(t => t.Tenant).ThenInclude(p => p.Property).ThenInclude(p => p.Brand).ThenInclude(b => b.Chain)
                                      .Include(t => t.Tenant).ThenInclude(p => p.Chain)
                                      .Include(t => t.Tenant).ThenInclude(p => p.Brand).ThenInclude(p => p.Chain)
                                    .Where(ut => ut.UserId == userId);

            return userTenantList.FirstOrDefault(ut => ut.Tenant.Chain != null) != null ? userTenantList.FirstOrDefault(ut => ut.Tenant.Chain != null).Tenant.ChainId.Value :
                            userTenantList.FirstOrDefault(ut => ut.Tenant.Brand != null) != null ? userTenantList.FirstOrDefault(ut => ut.Tenant.Brand != null).Tenant.Brand.ChainId :
                            userTenantList.FirstOrDefault(ut => ut.Tenant.Property != null) != null ? userTenantList.FirstOrDefault(ut => ut.Tenant.Property != null).Tenant.Property.Brand.Chain.Id : default(int);
        }
    }
}
