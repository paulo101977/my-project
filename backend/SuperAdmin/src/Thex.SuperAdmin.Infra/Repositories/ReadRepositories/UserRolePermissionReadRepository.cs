﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.Kernel;
using Thex.SuperAdmin.Infra.Context;
using Thex.SuperAdmin.Infra.Context.Repositories;
using Thex.SuperAdmin.Infra.Entities;
using Thex.SuperAdmin.Infra.Interfaces.ReadRepositories;
using Tnf.Localization;
using System.Linq;
using Thex.SuperAdmin.Dto.Cache;
using Microsoft.Extensions.Caching.Memory;
using Thex.SuperAdmin.Infra.JoinMap;
using Thex.SuperAdmin.Dto;

namespace Thex.SuperAdmin.Infra.Repositories.ReadRepositories
{
    public class UserRolePermissionReadRepository : SimpleRepository<Roles>, IUserRolePermissionReadRepository
    {
        private IApplicationUser _applicationUser;
        private ILocalizationManager _localizationManager;
        private readonly ThexIdentityDbContext _context;
        private readonly IConfiguration _configuration;
        private readonly IMemoryCache _cache;
        private readonly IIntegrationPartnerReadRepository _integrationPartnerReadRepository;

        public ThexIdentityDbContext Context { get { return _context; } }

        public UserRolePermissionReadRepository(
         ThexIdentityDbContext context,
         IApplicationUser applicationUser,
         IConfiguration configuration,
         IMemoryCache cache,
         IIntegrationPartnerReadRepository integrationPartnerReadRepository,
         ILocalizationManager localizationManager) : base(context)
        {
            _applicationUser = applicationUser;
            _localizationManager = localizationManager;
            _context = context;
            _configuration = configuration;
            _cache = cache;
            _integrationPartnerReadRepository = integrationPartnerReadRepository;
        }

        public IQueryable<UserRolePermissionJoinMap> GetUserRolePermissionbaseQuery()
            => (from u in _context.Users.AsNoTracking()
                join ur in _context.UserRoles.AsNoTracking() on u.Id equals ur.UserId
                join r in _context.Roles.AsNoTracking() on ur.RoleId equals r.Id
                join rp in _context.IdentityRolePermissions.AsNoTracking() on r.Id equals rp.RoleId
                join p in _context.IdentityPermissions.AsNoTracking() on rp.IdentityPermissionId equals p.Id
                join pf in _context.IdentityPermissionFeatures.AsNoTracking() on p.Id equals pf.IdentityPermissionId
                join f in _context.IdentityFeatures.AsNoTracking() on pf.IdentityFeatureId equals f.Id
                join im in _context.IdentityMenuFeatures.AsNoTracking() on f.Id equals im.IdentityFeatureId into imf
                from identityMenu in imf.DefaultIfEmpty()
                select new UserRolePermissionJoinMap
                {
                    User = u,
                    UserRole = ur,
                    Role = r,
                    IdentityRolePermission = rp,
                    IdentityPermission = p,
                    IdentityPermissionFeature = pf,
                    IdentityFeature = f,
                    IdentityMenuFeature = identityMenu
                });

        public IQueryable<UserRolePermissionJoinMap> GetUserRoleWithProductPermissionBaseQuery()
            => (from u in _context.Users.AsNoTracking()
                join ur in _context.UserRoles.AsNoTracking() on u.Id equals ur.UserId
                join r in _context.Roles.AsNoTracking() on ur.RoleId equals r.Id
                join rp in _context.IdentityRolePermissions.AsNoTracking() on r.Id equals rp.RoleId
                join p in _context.IdentityPermissions.AsNoTracking() on rp.IdentityPermissionId equals p.Id
                join pf in _context.IdentityPermissionFeatures.AsNoTracking() on p.Id equals pf.IdentityPermissionId
                join f in _context.IdentityFeatures.AsNoTracking() on pf.IdentityFeatureId equals f.Id
                join pr in _context.IdentityProducts.AsNoTracking() on f.IdentityProductId equals pr.Id
                join pp in _context.IdentityProductProperties.AsNoTracking() on f.IdentityProductId equals pp.IdentityProductId into perm
                from pp in perm.DefaultIfEmpty()
                select new UserRolePermissionJoinMap
                {
                    User = u,
                    UserRole = ur,
                    Role = r,
                    IdentityRolePermission = rp,
                    IdentityPermission = p,
                    IdentityPermissionFeature = pf,
                    IdentityFeature = f,
                    IdentityProductProperty = pp,
                    IdentityProduct = pr
                });

        public async Task<List<KeyValuePair<bool, string>>> GetAllUserKey(int productId, List<int?> propertyIds)
            => await (from q in GetUserRolePermissionbaseQuery()
                      where propertyIds.Contains(q.UserRole.PropertyId) && !q.User.IsDeleted && q.User.IsActive &&
                      !q.UserRole.IsDeleted && !q.Role.IsDeleted && q.Role.IsActive && !q.IdentityRolePermission.IsDeleted &&
                      q.IdentityRolePermission.IsActive && !q.IdentityPermission.IsDeleted && q.IdentityPermission.IsActive &&
                      !q.IdentityPermissionFeature.IsDeleted && q.IdentityPermissionFeature.IsActive &&
                      !q.IdentityFeature.IsDeleted && q.IdentityPermission.IdentityProductId == productId &&
                      q.UserRole.UserId == _applicationUser.UserUid && (q.IdentityMenuFeature == null || q.IdentityMenuFeature.IsActive)
                      select new KeyValuePair<bool, string>(q.IdentityFeature.IsInternal, q.IdentityFeature.Key))
                .Distinct()
                .ToListAsync();


        public async Task<List<ProductPermissionDto>> GetAllUserKeyByProduct(int propertyId)
        {
            var propertyPermission = await (from q in GetUserRoleWithProductPermissionBaseQuery()
                                            where q.UserRole.PropertyId == propertyId && !q.User.IsDeleted && q.User.IsActive &&
                                            !q.UserRole.IsDeleted && !q.Role.IsDeleted && q.Role.IsActive && !q.IdentityRolePermission.IsDeleted &&
                                            q.IdentityRolePermission.IsActive && !q.IdentityPermission.IsDeleted && q.IdentityPermission.IsActive &&
                                            !q.IdentityPermissionFeature.IsDeleted && q.IdentityPermissionFeature.IsActive &&
                                            !q.IdentityFeature.IsDeleted &&
                                            q.UserRole.UserId == _applicationUser.UserUid
                                            select new Tuple<int, string, bool, bool, string, string, string>
                                                           (q.IdentityProduct.Id, q.IdentityProduct.Name,
                                                           (q.IdentityProductProperty != null), q.IdentityFeature.IsInternal,
                                                            q.IdentityFeature.Key, q.IdentityPermission.Name, q.IdentityPermission.Id.ToString()))
                            .Distinct()
                            .ToListAsync();

            return propertyPermission
                          .GroupBy(u => u.Item1)
                          .Select(grp => grp.ToList())
                          .Select(x => new ProductPermissionDto
                          {
                              ProductId = x.Select(a => a.Item1).FirstOrDefault(),
                              ProductName = x.Select(a => a.Item2).FirstOrDefault(),
                              Active = x.Select(a => a.Item3).FirstOrDefault(),
                              KeyList = x.Where(k => k.Item4).DistinctBy(k => k.Item7).Select(a => new KeyListDto
                              {
                                  Key = a.Item6?.Replace(" ", "_")
                              }).ToList(),
                              MenuList = x.Where(k => !k.Item4).Select(a => new MenuItemSimpleDto
                              {
                                  Key = a.Item5,
                                  Name = a.Item5,
                              }).ToList(),
                          }).ToList();
        }

        public async Task<List<string>> GetAllUserKeyListByPropertyOrChainAsync(Guid userId)
        {
            var propertyId = 0;
            var chainId = 0;

            int.TryParse(_applicationUser.PropertyId, out propertyId);
            int.TryParse(_applicationUser.ChainId, out chainId);

            var baseQuery = GetUserRolePermissionbaseQuery()
                            .Where(q =>
                                !q.User.IsDeleted && q.User.IsActive &&
                                !q.UserRole.IsDeleted && !q.Role.IsDeleted &&
                                q.Role.IsActive && !q.IdentityRolePermission.IsDeleted &&
                                q.IdentityRolePermission.IsActive && !q.IdentityPermission.IsDeleted &&
                                q.IdentityPermission.IsActive && !q.IdentityPermissionFeature.IsDeleted &&
                                q.IdentityPermissionFeature.IsActive && !q.IdentityFeature.IsDeleted &&
                                q.UserRole.UserId == userId);

            if (propertyId != 0)
                baseQuery = baseQuery.Where(q => q.UserRole.PropertyId == propertyId);
            else if (chainId != 0)
                baseQuery = baseQuery.Where(q => q.UserRole.ChainId == chainId);

            return await baseQuery.Select(q => q.IdentityFeature.Key).Distinct().ToListAsync();
        }

        public async Task<bool> IsIntegrationPartner(List<string> keyList)
            => (keyList == null || keyList.Count == 0) &&
                await _integrationPartnerReadRepository.AnyByTokenApplication(_applicationUser.UserUid);

        public async Task<bool> AnyKeyAsync(Guid userId, List<string> permissionList)
        {
            var cacheKey = UserPermissionCacheItem.BuildKey(userId, _applicationUser.TenantId);
            var cacheItem = GetUserPermissionsCache(cacheKey);

            if (cacheItem != null && cacheItem.PermissionKeyList.Count > 0)
                return AnyKeyInCache(cacheItem, permissionList);

            var userPermissionKeyList = await GetAllUserKeyListByPropertyOrChainAsync(userId);

            if (await IsIntegrationPartner(userPermissionKeyList))
                return true;

            return CreateAndCheckUserPermissionsCache(cacheKey, userId, userPermissionKeyList, permissionList);
        }

        public bool AnyKeyInCache(UserPermissionCacheItem cacheItem, List<string> permissionList)
            => cacheItem.PermissionKeyList.Any(p => permissionList.Contains(p));

        public bool CreateAndCheckUserPermissionsCache(string cacheKey, Guid userId, List<string> userPermissionKeyList, List<string> permissionToCheckList)
        {
            if (userPermissionKeyList.Count == 0 || permissionToCheckList.Count == 0)
                return false;

            var newCacheItem = new UserPermissionCacheItem(userId, userPermissionKeyList);

            _cache.Set(cacheKey, newCacheItem, TimeSpan.FromMinutes(value: UserPermissionCacheItem.ExpireInMinutes));

            return AnyKeyInCache(newCacheItem, permissionToCheckList);
        }

        public UserPermissionCacheItem GetUserPermissionsCache(string cacheKey)
            => _cache.Get<UserPermissionCacheItem>(cacheKey);

        public async Task<bool> KeyIsEnabledForExternalAccess(List<string> permissionList)
            => await (from f in _context.IdentityFeatures.AsNoTracking()
                      where !f.IsDeleted && f.IsInternal &&
                            f.ExternalAccess.HasValue &&
                            f.ExternalAccess.Value &&
                            permissionList.Contains(f.Key)
                      select f.Id).AnyAsync();
    }
}
