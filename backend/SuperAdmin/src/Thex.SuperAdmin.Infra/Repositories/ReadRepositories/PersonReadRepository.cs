﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Infra.Context;
using Thex.SuperAdmin.Infra.Context.Repositories;
using Thex.SuperAdmin.Infra.Entities;
using Thex.SuperAdmin.Infra.Interfaces.ReadRepositories;

namespace Thex.SuperAdmin.Infra.Repositories.ReadRepositories
{
    public class PersonReadRepository : SimpleRepository<Person>, IPersonReadRepository
    {
        private readonly ThexIdentityDbContext _context;

        public PersonReadRepository(ThexIdentityDbContext context)
            : base(context)
        {
            _context = context;
        }

        public Person Get(Guid personId)
        {
            return _context.Persons.FirstOrDefault(p => p.Id == personId);
        }

        public PersonDto GetDtoById(Guid id)
            => _context.Persons.FirstOrDefault(p => p.Id == id).MapTo<PersonDto>();
    }
}
