﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Thex.Common.Enumerations;
using Thex.Kernel;
using Thex.Kernel.Dto;
using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Dto.Dto;
using Thex.SuperAdmin.Dto.Enumerations;
using Thex.SuperAdmin.Infra.Context;
using Thex.SuperAdmin.Infra.Context.Repositories;
using Thex.SuperAdmin.Infra.Entities;
using Thex.SuperAdmin.Infra.Interfaces.ReadRepositories;
using Tnf.Dto;
using Tnf.Localization;
using Tnf.Specifications;

namespace Thex.SuperAdmin.Infra.Repositories.ReadRepositories
{
    public class PropertyReadRepository : SimpleRepository<Property>, IPropertyReadRepository
    {
        private readonly ILocalizationManager _localizationManager;
        private readonly ThexIdentityDbContext _context;
        private readonly IApplicationUser _applicationUser;

        public PropertyReadRepository(
            ThexIdentityDbContext context,
            IApplicationUser applicationUser,
            ILocalizationManager localizationManager) : base(context)
        {
            _applicationUser = applicationUser;
            _localizationManager = localizationManager;
            _context = context;
        }

        public ThexIdentityDbContext Context
        {
            get
            {
                return _context;
            }
        }

        public Property GetByPropertyIdWithTenant(int propertyId, bool ignoreQueryFilters = false)
        {
            IQueryable<Property> dbBaseQuery;

            if (!ignoreQueryFilters)
                dbBaseQuery = _context.Properties.IgnoreQueryFilters();
            else
                dbBaseQuery = _context.Properties;

            dbBaseQuery = dbBaseQuery.Include(e => e.Tenant).Where(e => e.Id == propertyId);

            return dbBaseQuery.FirstOrDefault();
        }

        public CountryLanguageDto GetPropertyCulture(int propertyId)
        {
            var result = (from p in _context.Properties.AsNoTracking().IgnoreQueryFilters()

                          join l in _context.Locations.AsNoTracking().IgnoreQueryFilters()
                          on p.PropertyUId equals l.OwnerId

                          join c in _context.CountrySubdivisions.AsNoTracking().IgnoreQueryFilters()
                          on l.CountryCode equals c.TwoLetterIsoCode

                          join cl in _context.CountryLanguages.AsNoTracking().IgnoreQueryFilters()
                          on c.Id equals cl.CountrySubdivisionId

                          where p.Id == propertyId && c.ParentSubdivisionId == null

                          select cl).FirstOrDefault();

            return result.MapTo<CountryLanguageDto>();
        }

        public ICollection<PropertyDto> GetPropertiesAssociateByCompanyId(int id)
        {
            var properties = _context.Properties.Include(x => x.Brand).Where(x => x.CompanyId == id);

            var propertiesDto = new List<PropertyDto>();
            foreach (var property in properties)
            {
                propertiesDto.Add(new PropertyDto()
                {
                    Id = property.Id,
                    Name = property.Name,
                    Brand = new BrandDto()
                    {
                        Id = property.Brand.Id,
                        Name = property.Brand.Name
                    }
                });
            }

            return propertiesDto;
        }

        public async Task<ThexListDto<PropertyDto>> GetAllDtoAsync(GetAllPropertyDto request)
        {
            var dbBaseQuery = GetBaseQuery(GetAllFilters(request));

            var totalItems = await dbBaseQuery.CountAsync();

            dbBaseQuery = GetAllPaging(request, dbBaseQuery);

            var totalItemsFiltered = await dbBaseQuery.CountAsync();
            var result = await dbBaseQuery.ToListAsync();

            return new ThexListDto<PropertyDto>
            {
                Items = result,
                HasNext = GetAllHasNext(request, totalItems, totalItemsFiltered),
                TotalItems = totalItems
            };
        }

        public async Task<ThexListDto<DashboardPropertyDto>> GetAllDtoForDashboardAsync()
        {
            var dashboardPropertyDtoList = await (from property in Context.Properties
                                                  join propertyStatus in Context.Statuss
                                                  on property.PropertyStatusId equals propertyStatus.Id
                                                  select new DashboardPropertyDto()
                                                  {
                                                      Id = property.Id,
                                                      CreationDate = property.CreationTime,
                                                      IsBlocked = property.IsBlocked ?? false,
                                                      Name = property.Name,
                                                      PropertyStatusId = propertyStatus.Id,
                                                      PropertyStatusName = propertyStatus.Name,
                                                      InitialsName = GetAbbreviationPropertyName(property.Name),
                                                  }).OrderBy(p => p.Name).ToListAsync();

            var propertyStatusHistoryList = await GetPropertyStatusHistoryByProperty(dashboardPropertyDtoList.Select(x => x.Id)
                .ToList())
                .ToListAsync();

            foreach (var dashboardPropertyDto in dashboardPropertyDtoList)
                dashboardPropertyDto.PropertyStatusHistoryDtoList = propertyStatusHistoryList.Where(x => x.PropertyId == dashboardPropertyDto.Id).ToList();

            return new ThexListDto<DashboardPropertyDto>()
            {
                Items = dashboardPropertyDtoList,
                TotalItems = dashboardPropertyDtoList.Count()
            };
        }

        public async Task<int> GetPropertyStatusByIdAsync(int propertyId)
        {
            var property = await _context.Properties.
                                    Select(x => new { x.Id, x.PropertyStatusId })
                                    .FirstOrDefaultAsync(x => x.Id == propertyId);

            if (property == null)
                return 0;

            return property.PropertyStatusId;
        }

        public async Task<PropertyDto> GetDtoByIdAsync(DefaultIntRequestDto request)
        {
            var baseQuery = await GetBaseQuery(e => e.Id == request.Id).FirstOrDefaultAsync();

            baseQuery.IntegrationPartnerPropertyList = await
                  (from ipp in _context.IntegrationPartnerProperties
                   join partner in _context.Partners
                   on ipp.PartnerId equals partner.Id
                   join p in _context.IntegrationPartners on ipp.PartnerId equals p.Id into ip
                   from integrationPartner in ip.DefaultIfEmpty()
                   where ipp.PropertyId == request.Id
                   select new IntegrationPartnerPropertyDto()
                   {
                       Id = ipp.Id,
                       PropertyId = ipp.PropertyId,
                       IntegrationCode = ipp.IntegrationCode,
                       IsActive = ipp.IsActive,
                       PartnerId = ipp.PartnerId,
                       PartnerName = partner.PartnerName,
                       TokenClient = integrationPartner.TokenClient
                   }).ToListAsync();

            return baseQuery;
        }

        public async Task<Property> GetByIdAsync(DefaultIntRequestDto request)
        {
            return await _context.Properties.FirstOrDefaultAsync(e => e.Id == request.Id);
        }

        public async Task<bool> CanBlockPropertyAsync(int propertyId, string key)
        {
            return await _context.Properties.AnyAsync(x => x.Id == propertyId && x.Name.ToLower().Equals(key.ToLower()));
        }

        public async Task<bool> PropertyIsBlocked(int propertyId)
        {
            var property = await _context.Properties
                                    .Select(x => new { x.Id, x.IsBlocked })
                                    .FirstOrDefaultAsync(x => x.Id == propertyId);

            if (property == null) return true;

            return property.IsBlocked.HasValue ? property.IsBlocked.Value : true;
        }

        public async Task<IList<PropertyDto>> GetAllDtoByChainIdAsync(int chainId)
        {
            return await _context.Properties
                .Include(x => x.Brand)
                .Where(x => x.Brand.ChainId == chainId)
                .Select(x => new PropertyDto
                {
                    Id = x.Id,
                    Name = x.Name
                }).ToListAsync();
        }

        public async Task<Guid?> GetTenantIdByPropertyIdAsync(int propertyId)
        {
            return (await _context.Properties.FirstOrDefaultAsync(p => p.Id == propertyId))?.TenantId;
        }

        public async Task<List<int>> GetAllDtoByBrandIdListAsync(List<int> brandIdList)
            => await _context.Properties.AsNoTracking()
                    .Where(p => brandIdList.Contains(p.BrandId))
                    .Select(p => p.Id)
                    .ToListAsync();

        public async Task<List<PropertyIdAndTenantDto>> GetAllTenantIdListAsync(List<int> idList)
            => await Context
                .Properties.AsNoTracking()
                .Where(e => idList.Contains(e.Id))
                .Select(x => new PropertyIdAndTenantDto { Id = x.Id, TenantId = x.TenantId })
                .ToListAsync();

        public async Task<List<PropertyIdAndTenantDto>> GetAllTenantIdListByBrandIdListAsync(List<int> brandIdList)
            => await _context.Properties.AsNoTracking()
                    .Where(p => brandIdList.Contains(p.BrandId))
                    .Select(x => new PropertyIdAndTenantDto { Id = x.Id, TenantId = x.TenantId })
                    .ToListAsync();

        public async Task<int> GetRoomsByPropertyIdAsync(int propertyId)
        {
            return await _context.Rooms.CountAsync(r => r.PropertyId == propertyId && !r.IsDeleted);
        }

        public async Task<List<PropertyIdAndTenantDto>> GetAllTenantIdListByChainIdAsync(int chainId)
            => await _context.Properties.AsNoTracking()
                    .Include(x => x.Brand).AsNoTracking()
                    .Where(x => x.Brand.ChainId == chainId)
                    .Select(x => new PropertyIdAndTenantDto { Id = x.Id, TenantId = x.TenantId })
                    .ToListAsync();


        public PropertyDto GetWithBrandAndChain(int id)
            => (from p in _context.Properties
                              join b in _context.Brands on p.BrandId equals b.Id
                              join c in _context.Chains on b.ChainId equals c.Id
                              where p.Id == id
                              select new PropertyDto
                              {
                                  Id = p.Id,
                                  Name = p.Name,
                                  TenantId = p.TenantId,
                                  Brand = new BrandDto
                                  {
                                      Id = p.Brand.Id,
                                      Name = p.Brand.Name,
                                      Chain = new ChainDto
                                      {
                                          Id = c.Id,
                                          Name = c.Name
                                      }
                                  }
                              }).FirstOrDefault();

        public List<LoggedUserPropertyDto> GetAllDtoByUserId(Guid userId, int productId)
            => (from u in _context.Users.AsNoTracking()
                join ut in _context.UserTenants.AsNoTracking() on u.Id equals ut.UserId
                join t in _context.Tenants.AsNoTracking() on ut.TenantId equals t.Id
                join p in _context.Properties.AsNoTracking() on t.Id equals p.TenantId
                join b in _context.Brands.AsNoTracking() on p.BrandId equals b.Id
                join pp in _context.PropertyParameters.Where(pp => pp.ApplicationParameterId == (int)ApplicationParameterEnum.ParameterTimeZone).AsNoTracking()
                on p.Id equals pp.PropertyId into pp
                from lpp in pp.DefaultIfEmpty()
                join l in _context.Locations.AsNoTracking() on p.PropertyUId equals l.OwnerId
                join cs in _context.CountrySubdivisions.AsNoTracking() on l.CountryCode equals cs.TwoLetterIsoCode
                join cl in _context.CountryLanguages.AsNoTracking() on cs.Id equals cl.CountrySubdivisionId
                join s in _context.CountrySubdivisions.AsNoTracking() on l.StateId equals s.Id into s
                from ls in s.DefaultIfEmpty()
                where ut.UserId == userId && ((p.PropertyStatusId == (int)PropertyStatusEnum.Production && (!p.IsBlocked.HasValue || !p.IsBlocked.Value)) ||
                                                        productId != (int)IdentityProductEnum.Central)
                select new LoggedUserPropertyDto
                {
                    PropertyId = p.Id.ToString(),
                    TenantId = p.TenantId.ToString(),
                    BrandId = p.BrandId.ToString(),
                    ChainId = b.ChainId.ToString(),
                    Name = p.Name,
                    TimeZoneName = lpp.PropertyParameterValue,
                    PropertyCulture = cl.CultureInfoCode.ToLower(),
                    PropertyLanguage = cl.CultureInfoCode.ToLower(),
                    PropertyCountryCode = l.CountryCode,
                    PropertyDistrictCode = ls.TwoLetterIsoCode
                }).ToList();         
        

        #region PropertyContract
        public async Task<PropertyContract> GetPropertyContractByPropertyIdAsync(int propertyId)
        {
            return await _context.PropertyContracts.FirstOrDefaultAsync(x => x.PropertyId == propertyId);
        }


        #endregion

        #region private methods

        private Expression<Func<PropertyDto, bool>> GetAllFilters(GetAllPropertyDto request)
        {
            var searchData = request.SearchData;
            Expression<Func<PropertyDto, bool>> filters = null;

            if (!string.IsNullOrEmpty(searchData))
            {
                //property
                filters = (e => e.Name.ToLower().Contains(searchData));
                //brand
                filters = filters.Or((e => e.BrandName.ToLower().Contains(searchData)));
                //chain
                filters = filters.Or((e => e.ChainName.ToLower().Contains(searchData)));
                //company
                filters = filters.Or((e => e.TradeName.ToLower().Contains(searchData)));
                filters = filters.Or((e => e.ShortName.ToLower().Contains(searchData)));
            }

            if (request.BrandIdList.Any())
                filters = filters == null ? (p => request.BrandIdList.Contains(p.BrandId)) : filters.And(p => request.BrandIdList.Contains(p.BrandId));

            return filters;
        }

        private IQueryable<PropertyDto> GetBaseQuery(Expression<Func<PropertyDto, bool>> filters)
            => (from c in _context.Chains.AsNoTracking().IgnoreQueryFilters()
                join t in _context.Tenants.AsNoTracking().IgnoreQueryFilters() on c.TenantId equals t.Id
                join b in _context.Brands.AsNoTracking().IgnoreQueryFilters() on c.Id equals b.ChainId
                join p in _context.Properties.AsNoTracking().IgnoreQueryFilters() on b.Id equals p.BrandId
                join co in _context.Companies.AsNoTracking().IgnoreQueryFilters() on p.CompanyId equals co.Id
                join pc in _context.PropertyContracts.AsNoTracking().IgnoreQueryFilters() on p.Id equals pc.PropertyId
                into pcp
                from pc in pcp.DefaultIfEmpty()
                select new PropertyDto
                {
                    Id = p.Id,
                    PropertyUId = p.PropertyUId,
                    Name = p.Name,
                    TenantId = p.TenantId,
                    TenantName = p.Tenant != null ? p.Tenant.TenantName: string.Empty,
                    ChainId = c.Id,
                    ChainName = c.Name,
                    BrandId = b.Id,
                    BrandName = b.Name,
                    CompanyId = co.Id,
                    CompanyUid = co.CompanyUid,
                    ShortName = co.ShortName,
                    TradeName = co.TradeName,
                    MaxUh = pc != null ? pc.MaxUh : default(int),
                    IsBlocked = p.IsBlocked ?? false
                    //PhotoUrl = p.Photo
                })
                .Where(filters ?? (e => true))
                .Select(p => p);

        private bool GetAllHasNext(GetAllPropertyDto request, int totalItems, int totalItemsFiltered)
            => totalItems > ((request.Page - 1) * request.PageSize) + totalItemsFiltered;

        private IQueryable<PropertyDto> GetAllPaging(GetAllPropertyDto request, IQueryable<PropertyDto> dbBaseQuery)
        {
            var pageSize = request.PageSize ?? 1000;
            var page = request.Page.HasValue ? request.Page.Value : 1;

            dbBaseQuery = dbBaseQuery.Skip(pageSize * (page - 1)).Take(pageSize);

            return dbBaseQuery;
        }

        private string GetAbbreviationPropertyName(string name)
        {
            var abbreviation = string.Empty;
            var names = name.Split(' ');
            if (names.Length > 1)
                abbreviation = $"{names[0].Substring(0, 1)}{names[1].Substring(0, 1)}";
            else
                abbreviation = name.Substring(0, 2);

            return abbreviation;
        }

        private IQueryable<PropertyStatusHistoryDto> GetPropertyStatusHistoryByProperty(List<int> listPropertyId)
        {
            var propertyStatusHistoryList = Context.PropertyStatusHistories
                                            .Include(x => x.Status)
                                            .AsNoTracking()
                                            .Where(x => listPropertyId.Contains(x.PropertyId))
                                            .GroupBy(x => new { x.StatusId, x.PropertyId })
                                            .Select(x => x.OrderByDescending(o => o.CreationTime).First())
                                            .AsQueryable()
                                            .OrderBy(x => x.CreationTime)
                                            .Select(x => new PropertyStatusHistoryDto()
                                            {
                                                Id = x.Id,
                                                CreationDate = x.CreationTime,
                                                PropertyStatusId = x.Status.Id,
                                                PropertyStatusName = x.Status.Name,
                                                PropertyId = x.PropertyId
                                            }).AsQueryable();

            return propertyStatusHistoryList;
        }
        
        #endregion
    }
}