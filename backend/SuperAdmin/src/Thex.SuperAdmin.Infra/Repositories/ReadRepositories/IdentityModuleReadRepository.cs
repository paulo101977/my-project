﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Thex.Kernel;
using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Dto.Dto;
using Thex.SuperAdmin.Infra.Context;
using Thex.SuperAdmin.Infra.Context.Repositories;
using Thex.SuperAdmin.Infra.Entities;
using Thex.SuperAdmin.Infra.Interfaces.ReadRepositories;
using Tnf.Localization;
using System.Linq;
using X.PagedList;
using Thex.Common;
using Thex.SuperAdmin.Dto.Enumerations;

namespace Thex.SuperAdmin.Infra.Repositories.ReadRepositories
{
    public class IdentityModuleReadRepository : SimpleRepository<IdentityModule>, IIdentityModuleReadRepository
    {
        private IApplicationUser _applicationUser;
        private ILocalizationManager _localizationManager;
        private readonly ThexIdentityDbContext _context;
        private readonly IConfiguration _configuration;

        public ThexIdentityDbContext Context { get { return _context; } }

        public IdentityModuleReadRepository(
         ThexIdentityDbContext context,
         IApplicationUser applicationUser,
         IConfiguration configuration,
         ILocalizationManager localizationManager) : base(context)
        {
            _applicationUser = applicationUser;
            _localizationManager = localizationManager;
            _context = context;
            _configuration = configuration;
        }

        public async Task<ThexListDto<IdentityModuleDto>> GetAllAsync()
        {
            var result = new List<IdentityModuleDto>();
            var dbResult = await base.GetAll().ToListAsync();

            foreach (var identityModule in dbResult)
            {
                identityModule.Name = _localizationManager.GetString(AppConsts.LocalizationSourceName, ((IdentityModuleEnum)identityModule.Id).ToString());
                result.Add(identityModule.MapTo<IdentityModuleDto>());
            }

            return new ThexListDto<IdentityModuleDto>()
            {
                HasNext = false,
                Items = result,
                TotalItems = result.Count()
            };
        }
    }
}
