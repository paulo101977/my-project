﻿using System;
using System.Threading.Tasks;
using System.Linq;
using Tnf.Localization;
using Thex.SuperAdmin.Infra.Context;
using Thex.SuperAdmin.Infra.Interfaces.ReadRepositories;
using Thex.SuperAdmin.Dto;
using Microsoft.AspNetCore.Identity;
using Tnf.Dto;
using Microsoft.EntityFrameworkCore;
using Thex.SuperAdmin.Infra.Context.Repositories;
using Thex.SuperAdmin.Infra.Entities;
using System.Linq.Expressions;
using Tnf.Specifications;
using X.PagedList;
using Thex.SuperAdmin.Dto.Dto;
using Thex.Kernel;
using System.Collections.Generic;

namespace Thex.SuperAdmin.Infra.Repositories.ReadRepositories
{
    public class ValidUser
    {
        public Guid Id { get; set; }
        public Guid PersonId { get; set; }
        public Guid TenantId { get; set; }
        public string UserName { get; set; }
        public string PreferredLanguage { get; set; }
        public string PreferredCulture { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public bool IsActive { get; set; }
        public bool? IsAdmin { get; set; }
        public string PasswordHash { get; set; }
    }

    public class UserReadRepository : SimpleRepository<User>, IUserReadRepository
    {
        private readonly ILocalizationManager _localizationManager;
        protected readonly ITenantReadRepository _tenantReadRepository;
        private readonly ThexIdentityDbContext _context;
        private readonly IApplicationUser _applicationUser;

        public ThexIdentityDbContext Context => _context;

        public UserReadRepository(
            ThexIdentityDbContext context,
            ITenantReadRepository tenantReadRepository,
            IApplicationUser applicationUser,
            ILocalizationManager localizationManager) : base(context)
        {
            _tenantReadRepository = tenantReadRepository;
            _applicationUser = applicationUser;
            _localizationManager = localizationManager;

            _context = context;
        }

        private UserDto MapToSimpleUserDto(User users, Person person)
        {
            return new UserDto
            {
                Id = users.Id,
                PersonId = users.PersonId.Value,
                UserName = users.UserName,
                PreferredLanguage = users.PreferredLanguage,
                PreferredCulture = users.PreferredCulture,
                IsActive = users.IsActive,
                IsAdmin = users.IsAdmin,
                Name = users.Name,
                Email = users.Email,
                PhoneNumber = users.PhoneNumber,
                DateOfBirth = person.DateOfBirth,
                NickName = person.NickName,
                PhotoUrl = person.PhotoUrl
                
            };
        }

        public async Task<IListDto<UserDto>> GetAllByLoggedTenantIdAsync(GetAllUserDto request)
        {
            var result = await (from users in _context.Users.AsNoTracking()
                          join userTenant in _context.UserTenants.AsNoTracking() on users.Id equals userTenant.UserId
                          join tenant in _context.Tenants.AsNoTracking() on userTenant.TenantId equals tenant.Id
                          join person in _context.Persons.AsNoTracking() on users.PersonId equals person.Id

                          join userInvitation in _context.UserInvitations.AsNoTracking() on
                          new { p1 = userTenant.TenantId, p2 = userTenant.UserId } equals
                          new { p1 = userInvitation.TenantId, p2 = userInvitation.UserId.Value }
                          into ui
                          from userInvitation in ui.DefaultIfEmpty()

                          where userInvitation.UserId.HasValue && userTenant.TenantId == _applicationUser.TenantId
                          select new UserDto
                          {
                              Id = users.Id,
                              PersonId = users.PersonId.Value,
                              TenantId = userTenant.TenantId,
                              UserName = users.UserName,
                              PreferredLanguage = users.PreferredLanguage,
                              PreferredCulture = users.PreferredCulture,
                              IsActive = userTenant.IsActive,
                              IsAdmin = users.IsAdmin,
                              Name = users.Name,
                              Email = users.Email,
                              LastLink = userInvitation != null ? userInvitation.InvitationLink : null,
                              PropertyId = tenant.PropertyId,
                              ChainId = tenant.ChainId,
                              BrandId = tenant.BrandId,
                              PhoneNumber = users.PhoneNumber,
                              DateOfBirth = person.DateOfBirth,
                              NickName = person.NickName
                          })
                         .ToListAsync();

            if (result.Any())
            {
                var userIdList = result.Select(e => e.Id).ToList();
                var propertyId = result.Where(e => e.PropertyId.HasValue).Select(e => e.PropertyId.Value).ToList();
                var roles = _context.UserRoles
                                .Where(e => userIdList.Contains(e.UserId) && e.PropertyId.HasValue && propertyId.Contains(e.PropertyId.Value))
                                .Select(e => new { e.RoleId, e.PropertyId, e.UserId })
                                .ToList();
                foreach (var user in result)
                    user.RoleIdList = roles.Where(e => e.PropertyId == user.PropertyId && e.UserId == user.Id).Select(e => e.RoleId).ToList();
            }

            return new ListDto<UserDto>
            {
                HasNext = false,
                Items = result
            };
        }

        public async Task<UserDto> GetByLoggedUserIdAsync()
        {
            return await (from users in _context.Users.AsNoTracking()
                          join person in _context.Persons.AsNoTracking() on users.PersonId equals person.Id
                          where users.Id == _applicationUser.UserUid
                          select MapToSimpleUserDto(users, person)).FirstOrDefaultAsync();
        }

        public bool AnyUserByEmail(string email)
            => _context.Users.Any(u => u.Email.ToLower().Trim() == email.ToLower().Trim());

        public UserDto GetUserByEmail(string email)
            => (from users in _context.Users
                where users.Email == email
                select new UserDto
                {
                    Id = users.Id,
                    PersonId = users.PersonId.Value,
                    TenantId = users.TenantId ?? Guid.Empty,
                    UserName = users.UserName,
                    PreferredLanguage = users.PreferredLanguage,
                    PreferredCulture = users.PreferredCulture,
                    Name = users.Name,
                    Email = users.Email,
                    IsActive = users.IsActive,
                    IsAdmin = users.IsAdmin
                }).FirstOrDefault().MapTo<UserDto>();

        public UserDto GetUserById(Guid userUid)
            => (from users in _context.Users
                join person in _context.Persons.AsNoTracking() on users.PersonId equals person.Id
                where users.Id == userUid
                select MapToSimpleUserDto(users, person))
            .FirstOrDefault()
            .MapTo<UserDto>();

        protected IQueryable<ValidUser> GetValidUser()
        {
            var userQuery = (from users in _context.Users
                             where users.IsActive
                             select new ValidUser
                             {
                                 Id = users.Id,
                                 PersonId = users.PersonId.Value,
                                 UserName = users.UserName,
                                 PreferredLanguage = users.PreferredLanguage,
                                 PreferredCulture = users.PreferredCulture,
                                 Name = users.Name,
                                 Email = users.Email,
                                 IsActive = users.IsActive,
                                 IsAdmin = users.IsAdmin,
                                 PasswordHash = users.PasswordHash,
                             });
            return userQuery;
        }

        public UserDto GetAndValidateUser(Guid userId, string password)
        {
            var validUser = GetValidUser().Where(u => u.Id == userId).FirstOrDefault();

            return GetAndValidateUser(validUser, password);
        }

        private UserDto GetAndValidateUser(ValidUser user, string password)
        {
            var _passwordHasher = new PasswordHasher<ValidUser>();

            if (user != null && _passwordHasher.VerifyHashedPassword(user, user.PasswordHash, password) != PasswordVerificationResult.Failed)
            {
                UserDto userDto = new UserDto()
                {
                    Id = user.Id,
                    PersonId = user.PersonId,
                    TenantId = user.TenantId,
                    UserName = user.UserName,
                    PreferredLanguage = user.PreferredLanguage,
                    PreferredCulture = user.PreferredCulture,
                    IsActive = user.IsActive,
                    IsAdmin = user.IsAdmin,
                    Name = user.Name,
                    Email = user.Email
                };
                return userDto;
            }
            return null;
        }

        public bool HasPermissionByFilters(Guid id, int productId)
        {
            return (from users in _context.Users.AsNoTracking()
                    join userRoles in _context.UserRoles.AsNoTracking()
                    on users.Id equals userRoles.UserId
                    join roles in _context.Roles.AsNoTracking()
                    on userRoles.RoleId equals roles.Id
                    where users.Id == id && roles.IdentityProductId == productId
                    select users).Any();
        }


        #region Crud Super ADM

        public async Task<ThexListDto<UsersDto>> GetAllAsync(GetAllUserDto request)
        {
            var query = _context.Users.Include(ut => ut.UserTenantList).ThenInclude(t => t.Tenant).ThenInclude(p => p.Property).ThenInclude(p => p.Brand).ThenInclude(b => b.Chain)
                                      .Include(ut => ut.UserTenantList).ThenInclude(t => t.Tenant).ThenInclude(p => p.Chain)
                                      .Include(ut => ut.UserTenantList).ThenInclude(t => t.Tenant).ThenInclude(p => p.Brand).ThenInclude(p => p.Chain)
                                      .AsQueryable();

            if (!string.IsNullOrWhiteSpace(request.SearchData))
            {
                var search = request.SearchData.ToLower();

                Expression<Func<User, bool>> filters = null;
                filters = (x => x.Name.ToLower().Contains(search));
                filters = filters.Or(x => x.UserTenantList.FirstOrDefault().Tenant.TenantName.ToLower().Contains(search));
                filters = filters.Or(x => x.UserTenantList.FirstOrDefault().Tenant.Property != null && x.UserTenantList.FirstOrDefault().Tenant.Property.Name.ToLower().Contains(search));
                filters = filters.Or(x => x.UserTenantList.FirstOrDefault().Tenant.Property != null && x.UserTenantList.FirstOrDefault().Tenant.Property.Company.TradeName.ToLower().Contains(search));

                query = query.Where(filters);
            }

            var pagination = query
                      .Distinct()
                      .OrderBy(x => x.Name)
                      .ToPagedList(request.Page.Value, request.PageSize.Value);

            return new ThexListDto<UsersDto>()
            {
                HasNext = pagination.HasNextPage,
                Items = await pagination.Select(x => new UsersDto()
                {
                    Id = x.Id,
                    Name = x.Name,
                    ChainId = x.UserTenantList.FirstOrDefault(ut => ut.Tenant.Chain != null) != null ? x.UserTenantList.FirstOrDefault(ut => ut.Tenant.Chain != null).Tenant.ChainId.Value :
                              x.UserTenantList.FirstOrDefault(ut => ut.Tenant.Brand != null) != null ? x.UserTenantList.FirstOrDefault(ut => ut.Tenant.Brand != null).Tenant.Brand.ChainId :
                              x.UserTenantList.FirstOrDefault(ut => ut.Tenant.Property != null) != null ? x.UserTenantList.FirstOrDefault(ut => ut.Tenant.Property != null).Tenant.Property.Brand.Chain.Id : 0,
                    ChainName = x.UserTenantList.FirstOrDefault(ut => ut.Tenant.Chain != null) != null ? x.UserTenantList.FirstOrDefault(ut => ut.Tenant.Chain != null).Tenant.Chain.Name :
                              x.UserTenantList.FirstOrDefault(ut => ut.Tenant.Brand != null) != null ? x.UserTenantList.FirstOrDefault(ut => ut.Tenant.Brand != null).Tenant.Brand.Chain.Name :
                              x.UserTenantList.FirstOrDefault(ut => ut.Tenant.Property != null) != null ? x.UserTenantList.FirstOrDefault(ut => ut.Tenant.Property != null).Tenant.Property.Brand.Chain.Name : string.Empty,
                    UserName = x.UserName,
                    Email = x.Email,
                    IsActive = x.IsActive
                }).ToListAsync(),
                TotalItems = query.Count()
            };
        }

        public async Task<UsersDto> GetDtoByIdAsync(Guid id)
        {
            var userDto = await _context.Users.Select(u => new UsersDto()
            {
                Id = u.Id,
                UserName = u.UserName,
                Email = u.Email,
                Name = u.Name,
                IsActive = u.IsActive,
                PreferredCulture = u.PreferredCulture,
                PreferredLanguage = u.PreferredLanguage
            }).FirstOrDefaultAsync(u => u.Id == id);

            if (userDto == null)
                return null;

            var userTenants = await _context.UserTenants
                .Include(us => us.Tenant).ThenInclude(t => t.Property).ThenInclude(p => p.Brand).ThenInclude(b => b.Chain)
                .Include(us => us.Tenant).ThenInclude(t => t.Brand).ThenInclude(b => b.Chain)
                .Include(us => us.Tenant).ThenInclude(t => t.Chain)
                .Where(x => x.UserId == id).ToListAsync();

            if (!userTenants.Any(ut => ut.Tenant.ChainId.HasValue && !ut.Tenant.BrandId.HasValue && !ut.Tenant.PropertyId.HasValue))
            {
                userDto.BrandList = userTenants.Where(ut => ut.Tenant.BrandId.HasValue && !ut.Tenant.PropertyId.HasValue)
             .Select(b => new BrandDto()
             {
                 Id = b.Tenant.Brand.Id,
                 ChainId = b.Tenant.Brand.ChainId,
                 Name = b.Tenant.Brand.Name
             })
             .OrderBy(b => b.Name)
             .ToList();

                userDto.PropertyList = userTenants.Where(ut => ut.Tenant.PropertyId != null &&
                    !userDto.BrandList.Select(b => b.Id).Contains(ut.Tenant.Property.BrandId))
                        .Select(b => new PropertyDto()
                        {
                            Id = b.Tenant.Property.Id,
                            BrandId = b.Tenant.Property.BrandId,
                            Name = b.Tenant.Property.Name
                        })
                        .OrderBy(b => b.Name)
                        .ToList();
            }
            var chain = userTenants.FirstOrDefault(ut => ut.Tenant.Chain != null) != null ? userTenants.FirstOrDefault(ut => ut.Tenant.Chain != null).Tenant.Chain :
                            userTenants.FirstOrDefault(ut => ut.Tenant.Brand != null) != null ? userTenants.FirstOrDefault(ut => ut.Tenant.Brand != null).Tenant.Brand.Chain :
                              userTenants.FirstOrDefault(ut => ut.Tenant.Property != null) != null ? userTenants.FirstOrDefault(ut => ut.Tenant.Property != null).Tenant.Property.Brand.Chain :
                              null;

            userDto.ChainId = chain != null ? chain.Id : 0;
            userDto.ChainName = chain != null ? chain.Name : string.Empty;
            return userDto;
        }

        public async Task<User> GetByIdAsync(Guid id)
        {
            return await _context.Users.Include(u => u.UserTenantList)
                                    .ThenInclude(ut => ut.Tenant)
                                    .ThenInclude(t => t.Brand)
                                    .Include(u => u.UserTenantList).ThenInclude(ut => ut.Tenant).ThenInclude(t => t.Property)
                                    .Include(u => u.UserTenantList).ThenInclude(ut => ut.Tenant).ThenInclude(t => t.Chain)
                                    .FirstOrDefaultAsync(u => u.Id == id);
        }

        public async Task<User> GetByIdWithoutIncludeAsync(Guid id)
            => await _context.Users.FirstOrDefaultAsync(u => u.Id == id);

        #endregion

    }
}
