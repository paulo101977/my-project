﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;
using Thex.Kernel;
using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Infra.Context;
using Thex.SuperAdmin.Infra.Context.Repositories;
using Thex.SuperAdmin.Infra.Entities;
using Thex.SuperAdmin.Infra.Interfaces.ReadRepositories;
using Tnf.Dto;
using Tnf.Localization;

namespace Thex.SuperAdmin.Infra.Repositories.ReadRepositories
{
    public class UserInvitationReadRepository : SimpleRepository<UserInvitation>, IUserInvitationReadRepository
    {
        private readonly ILocalizationManager _localizationManager;
        private readonly ThexIdentityDbContext _context;
        private readonly IApplicationUser _applicationUser;
        private readonly ITenantReadRepository _tenantReadRepository;

        public UserInvitationReadRepository(
            ThexIdentityDbContext context,
            IApplicationUser applicationUser,
            ITenantReadRepository tenantReadRepository,
            ILocalizationManager localizationManager) : base(context)
        {
            _applicationUser = applicationUser;
            _localizationManager = localizationManager;
            _tenantReadRepository = tenantReadRepository;
            _context = context;
        }

        public IListDto<UserInvitationDto> GetAllFilterByTenant(GetAllUserInvitationDto request)
        {
            var baseQuery = _context
            .UserInvitations
            .FilterByTenant(_tenantReadRepository.GetIdListByLoggedTenantId());

            var result = baseQuery;

            return result.ToListDto<UserInvitation, UserInvitationDto>(request);
        }

        public async Task<UserInvitationDto> GetDtoByUserIdAndTenantIdAsync(Guid userId, Guid tenantId)
        {
            var userInvitation = await _context
                    .UserInvitations.IgnoreQueryFilters().AsNoTracking()
                    .FirstOrDefaultAsync(e => e.UserId == userId && e.TenantId == tenantId);

            return userInvitation != null ? userInvitation.MapTo<UserInvitationDto>() : null;
        }

        public UserInvitationDto GetWithPropertyAndChainNames(Guid id)
        {
            return (from invitations in _context.UserInvitations
                    join chains in _context.Chains on invitations.ChainId equals chains.Id into leftChainsTmp
                    from leftChains in leftChainsTmp.DefaultIfEmpty()
                    join properties in _context.Properties on invitations.PropertyId equals properties.Id into leftPropertiesTmp
                    from leftProperties in leftPropertiesTmp.DefaultIfEmpty()
                    where invitations.Id == id
                    select new UserInvitationDto()
                    {
                        ChainId = invitations.ChainId,
                        PropertyId = invitations.PropertyId,
                        Email = invitations.Email,
                        Name = invitations.Name,
                        InvitationLink = invitations.InvitationLink,
                        InvitationDate = invitations.InvitationDate,
                        InvitationAcceptanceDate = invitations.InvitationAcceptanceDate,
                        IsActive = invitations.IsActive,
                        ChainName = leftChains.Name,
                        PropertyName = leftProperties.Name
                    }).FirstOrDefault();
        }
    }
}