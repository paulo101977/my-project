﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Thex.Kernel;
using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Dto.Dto;
using Thex.SuperAdmin.Infra.Context;
using Thex.SuperAdmin.Infra.Context.Repositories;
using Thex.SuperAdmin.Infra.Entities;
using Thex.SuperAdmin.Infra.Interfaces.ReadRepositories;
using Tnf.Localization;
using System.Linq;
using X.PagedList;
using Thex.Dto;
using Thex.Common.Enumerations;

namespace Thex.SuperAdmin.Infra.Repositories.ReadRepositories
{
    public class IntegrationPartnerReadRepository : SimpleRepository<IntegrationPartner>, IIntegrationPartnerReadRepository
    {
        private IApplicationUser _applicationUser;
        private ILocalizationManager _localizationManager;
        private readonly ThexIdentityDbContext _context;
        private readonly IConfiguration _configuration;

        public ThexIdentityDbContext Context { get { return _context; } }

        public IntegrationPartnerReadRepository(
         ThexIdentityDbContext context,
         IApplicationUser applicationUser,
         IConfiguration configuration,
         ILocalizationManager localizationManager) : base(context)
        {
            _applicationUser = applicationUser;
            _localizationManager = localizationManager;
            _context = context;
            _configuration = configuration;
        }

        public IntegrationPartnerDto GetDtoByIntegrationpartnerType(IntegrationPartnerTypeEnum integrationPartnerTypeEnum)
        {
            var integrationPartner = Context.IntegrationPartners.FirstOrDefault(x => x.IntegrationPartnerType == (int)integrationPartnerTypeEnum);
            if (integrationPartner != null)
            {
                return new IntegrationPartnerDto()
                {
                    IntegrationPartnerId = integrationPartner.Id,
                    IntegrationPartnerName = integrationPartner.IntegrationPartnerName,
                    TokenApplication = integrationPartner.TokenApplication,
                    TokenClient = integrationPartner.TokenClient,
                    IntegrationPartnerType = integrationPartner.IntegrationPartnerType
                };
            }

            return null;
        }

        public IntegrationPartnerDto GetDtoByTokenClientAndTokenApplication(string tokenClient, string tokenApplication)
        {
            var integrationPartner = Context.IntegrationPartners.FirstOrDefault(x => x.TokenApplication.Equals(tokenApplication) && x.TokenClient.Equals(tokenClient));
            if (integrationPartner == null) return null;

            return new IntegrationPartnerDto()
            {
                IntegrationPartnerId = integrationPartner.Id,
                IntegrationPartnerName = integrationPartner.IntegrationPartnerName,
                IntegrationPartnerType = integrationPartner.IntegrationPartnerType,
                TokenApplication = integrationPartner.TokenApplication,
                TokenClient = integrationPartner.TokenClient,
                IsActive = integrationPartner.IsActive
            };
        }

        public async Task<ThexListDto<PartnerDto>> GetAllPartnersAsync()
        {
            var partnerDtoList = _context.Partners
                .Select(p => new PartnerDto
                {
                   Id = p.Id,
                   PartnerName = p.PartnerName
                });

            return new ThexListDto<PartnerDto>()
            {
                Items = await partnerDtoList.ToListAsync(),
                HasNext = false
            };
        }

        public async Task<bool> AnyByTokenApplication(Guid tokenApplicationId)
            => await _context.IntegrationPartners.AsNoTracking().AnyAsync(e => e.TokenApplication == tokenApplicationId.ToString());

        public async Task<string> GetPartnerNameByIdAsync(int id)
            => (await _context.Partners.FirstOrDefaultAsync(p => p.Id == id))?.PartnerName;

    }
}
