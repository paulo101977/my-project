﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Thex.Kernel;
using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Dto.Dto;
using Thex.SuperAdmin.Infra.Context;
using Thex.SuperAdmin.Infra.Context.Repositories;
using Thex.SuperAdmin.Infra.Entities;
using Thex.SuperAdmin.Infra.Interfaces.ReadRepositories;
using Tnf.Localization;
using System.Linq;
using X.PagedList;

namespace Thex.SuperAdmin.Infra.Repositories.ReadRepositories
{
    public class BrandReadRepository : SimpleRepository<Brand>, IBrandReadRepository
    {
        private IApplicationUser _applicationUser;
        private ILocalizationManager _localizationManager;
        private readonly ThexIdentityDbContext _context;
        private readonly IConfiguration _configuration;

        public ThexIdentityDbContext Context { get { return _context; } }

        public BrandReadRepository(
         ThexIdentityDbContext context,
         IApplicationUser applicationUser,
         IConfiguration configuration,
         ILocalizationManager localizationManager) : base(context)
        {
            _applicationUser = applicationUser;
            _localizationManager = localizationManager;
            _context = context;
            _configuration = configuration;
        }

        public async Task<ThexListDto<BrandDto>> GetAllDtoByFilterAsync(GetAllBrandDto request)
        {
            request.Page = request.Page ?? 1;
            request.PageSize = request.PageSize ?? _configuration.GetValue<int>("DefaultPageSizeValue");

            var query = Context.Brands
                            .Include(x => x.Chain)
                            .Include(x => x.Tenant).AsQueryable();

            if (!string.IsNullOrWhiteSpace(request.SearchData))
            {
                var search = request.SearchData.ToLower();

                query = from brand in query.Include(x=>x.PropertyList)
                         where brand.Name.ToLower().Contains(search) ||
                                (brand.Chain != null && brand.Chain.Name.ToLower().Contains(search)) ||
                                (brand.Tenant != null && brand.Tenant.TenantName.ToLower().Contains(search)) ||
                                (brand.PropertyList != null && brand.PropertyList.Any(x => x.Name.ToLower().Contains(search)))
                          select brand;
            }

            var pagination = query
                       .Distinct()
                       .OrderBy(x => x.Name)
                       .ToPagedList(request.Page.Value, request.PageSize.Value);

            return new ThexListDto<BrandDto>()
            {
                HasNext = pagination.HasNextPage,
                Items = await pagination.Select(x => new BrandDto()
                {
                    Id = x.Id,
                    Name = x.Name,
                    ChainName = x.Chain != null ? x.Chain.Name : string.Empty,
                    ChainId = x.Chain != null ? x.Chain.Id: 0,
                    TenantId = x.Tenant != null ? x.Tenant.Id : Guid.Empty,
                    TenantName = x.Tenant != null ? x.Tenant.TenantName : string.Empty
                }).Distinct().ToListAsync(),
                TotalItems = query.Count()
            };
        }

        public async Task<BrandDto> GetDtoByIdAsync(int id)
        {
            var brand = await _context.Brands
                              .Include(x=>x.Tenant)
                              .Include(x=>x.Chain)
                              .Include(x=>x.PropertyList)
                              .FirstOrDefaultAsync(x => x.Id == id);            
            
            if (brand != null)
            {
                var propertiesDto = new List<PropertyDto>();
                if (brand.PropertyList != null)
                {
                    foreach (var property in brand.PropertyList)
                    {
                        propertiesDto.Add(new PropertyDto()
                        {
                            Id = property.Id,
                            Name = property.Name
                        });
                    }
                }

                return new BrandDto()
                {
                    Id = brand.Id,
                    Name = brand.Name,
                    ChainId = brand.Chain != null ? brand.Chain.Id : 0,
                    ChainName = brand.Chain != null ? brand.Chain.Name : string.Empty,
                    TenantId = brand.Tenant != null ? brand.Tenant.Id : Guid.Empty,
                    TenantName = brand.Tenant != null ? brand.Tenant.TenantName : string.Empty,
                    PropertyList = propertiesDto
                };
            }

            return null;
        }

        public async Task<bool> Exists(int id)
        {
            return await _context.Brands.AnyAsync(x => x.Id == id);
        }

        public async Task<Brand> GetById(int id)
        {
            var brand = await _context.Brands.FirstOrDefaultAsync(x => x.Id == id);
            return brand;
        }

        public async Task<bool> NameIsAvailable(string name)
        {
            return !(await _context.Brands.AnyAsync(x => x.Name.ToLower().Equals(name.ToLower())));
        }

        public async Task<int?> GetChainId(int id)
            => (await _context.Brands.Where(e => e.Id == id).Select(e => e.ChainId).FirstOrDefaultAsync());

        public async Task<IList<BrandDto>> GetAllDtoByChainIdAsync(int chainId)
        {
            return await _context.Brands.Where(x => x.ChainId == chainId)
                .Select(x => new BrandDto
                {
                    Id = x.Id,
                    Name = x.Name
                }).ToListAsync();
        }

        public async Task<List<Guid>> GetAllTenantIdListAsync(List<int> brandIdList)
            => await Context
                .Brands.AsNoTracking()
                .Where(e => brandIdList.Contains(e.Id))
                .Select(p => p.TenantId)
                .ToListAsync();

        public async Task<List<Guid>> GetAllTenantIdListByChainIdAsync(int chainId)
            => await _context.Brands.AsNoTracking()
                    .Where(x => x.ChainId == chainId)
                    .Select(x => x.TenantId)
                    .ToListAsync();

        public BrandDto FirstOrDefault()
            => Context.Brands.Select(e => new BrandDto
            {
                Id = e.Id,
                Name = e.Name
            }).FirstOrDefault();
    }
}
