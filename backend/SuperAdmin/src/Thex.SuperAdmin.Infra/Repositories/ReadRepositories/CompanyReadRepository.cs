﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Thex.Kernel;
using Thex.Common.Enumerations;
using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Dto.Dto;
using Thex.SuperAdmin.Infra.Context;
using Thex.SuperAdmin.Infra.Context.Repositories;
using Thex.SuperAdmin.Infra.Entities;
using Thex.SuperAdmin.Infra.Interfaces.ReadRepositories;
using Tnf.Localization;
using X.PagedList;

namespace Thex.SuperAdmin.Infra.Repositories.ReadRepositories
{
    public class CompanyReadRepository : SimpleRepository<Company>, ICompanyReadRepository
    {
        private readonly IApplicationUser _applicationUser;
        private readonly ILocalizationManager _localizationManager;
        private readonly ThexIdentityDbContext _context;
        private readonly IConfiguration _configuration;

        public ThexIdentityDbContext Context
        {
            get
            {
                return _context;
            }
        }

        public CompanyReadRepository(
       ThexIdentityDbContext context,
       IApplicationUser applicationUser,
       IConfiguration configuration,
       ILocalizationManager localizationManager) : base(context)
        {
            _applicationUser = applicationUser;
            _localizationManager = localizationManager;
            _context = context;
            _configuration = configuration;
        }

        public async Task<ThexListDto<CompanyDto>> GetAllByFilterAsync(GetAllCompanyDto request)
        {
            request.Page = request.Page ?? 1;
            request.PageSize = request.PageSize ?? _configuration.GetValue<int>("DefaultPageSizeValue");

            var companies = from company in _context.Companies
                            join property in _context.Properties on company.Id equals property.CompanyId into pc
                            from property in pc.DefaultIfEmpty()
                            join brand in _context.Brands on property.BrandId equals brand.Id into pb
                            from brand in pb.DefaultIfEmpty()
                            join chain in _context.Chains on brand.ChainId equals chain.Id into bc
                            from chain in bc.DefaultIfEmpty()
                            join tenant in _context.Tenants on property.TenantId equals tenant.Id into ct
                            from tenant in ct.DefaultIfEmpty()
                            select company;

            if (!string.IsNullOrWhiteSpace(request.SearchData))
            {
                var search = request.SearchData.ToLower();

                companies = from company in companies
                            where (company.ShortName.ToLower().Contains(search) || company.TradeName.ToLower().Contains(search)) ||
                                  (company.PropertyList != null && company.PropertyList.Any(x => x.Name.ToLower().Contains(search))) ||
                                  (company.PropertyList != null && company.PropertyList.Any(x => x.Brand != null && x.Brand.Name.ToLower().Contains(search))) ||
                                  (company.PropertyList != null && company.PropertyList.Any(x => x.Brand != null && x.Brand.Chain != null && x.Brand.Chain.Name.ToLower().Contains(search))) ||
                                  (company.PropertyList != null && company.PropertyList.Any(x => x.Tenant != null && x.Tenant.TenantName.ToLower().Contains(search)))
                            select company;
            }

            var pagination = companies
                      .Distinct()
                      .OrderBy(x => x.TradeName)
                      .ToPagedList(request.Page.Value, request.PageSize.Value);

            var companiesPagination = await pagination.Select(x => new CompanyDto()
            {
                TradeName = x.TradeName,
                ShortName = x.ShortName,
                Id = x.Id
            }).Distinct().ToListAsync();

            return new ThexListDto<CompanyDto>()
            {
                HasNext = pagination.HasNextPage,
                Items = companiesPagination,
                TotalItems = companies.Distinct().Count()
            };
        }

        public async Task<CompanyDto> GetCompanyDtoByIdAsync(int id)
        {
            var companyDto = await SearchCompanyClientByIdBaseQuery(id).FirstOrDefaultAsync();
            return companyDto;
        }

        public async Task<Company> GetCompanyByIdAsync(int id)
        {
            var company = await _context.Companies.FirstOrDefaultAsync(x => x.Id == id);
            return company;
        }

        public async Task<bool> Exists(int id)
        {
            return await _context.Companies.AnyAsync(x => x.Id == id);
        }

        #region private methods
        private IQueryable<CompanyDto> SearchCompanyClientByIdBaseQuery(int id)
        {
            return from company in _context.Companies
                   join person in _context.Persons on company.PersonId equals person.Id

                   join emailInformation in _context.ContactInformations on
                   new { p1 = person.Id, p2 = Convert.ToInt32(ContactInformationTypeEnum.Email) } equals
                   new { p1 = emailInformation.OwnerId, p2 = emailInformation.ContactInformationTypeId } into ri
                   from emailInformationLeft in ri.DefaultIfEmpty()

                   join phoneInformation in _context.ContactInformations on
                   new { p3 = person.Id, p4 = Convert.ToInt32(ContactInformationTypeEnum.PhoneNumber) } equals
                   new { p3 = phoneInformation.OwnerId, p4 = phoneInformation.ContactInformationTypeId } into ri2
                   from phoneInformationLeft in ri2.DefaultIfEmpty()

                   join homePageInformation in _context.ContactInformations on
                   new { p5 = person.Id, p6 = Convert.ToInt32(ContactInformationTypeEnum.Website) } equals
                   new { p5 = homePageInformation.OwnerId, p6 = homePageInformation.ContactInformationTypeId } into ri3
                   from homePageInformationLeft in ri3.DefaultIfEmpty()

                   join cellPhoneInformation in _context.ContactInformations on
                   new { p7 = person.Id, p8 = Convert.ToInt32(ContactInformationTypeEnum.CellPhoneNumber) } equals
                   new { p7 = cellPhoneInformation.OwnerId, p8 = cellPhoneInformation.ContactInformationTypeId } into
                   ri4

                   from cellPhoneInformationLeft in ri4.DefaultIfEmpty()

                   where company.Id == id
                   select new CompanyDto
                   {
                       Id = company.Id,
                       ShortName = company.ShortName,
                       TradeName = company.TradeName,
                       PersonType = person.PersonType[0],
                       Email = emailInformationLeft != null ? emailInformationLeft.Information : null,
                       HomePage = homePageInformationLeft != null ? homePageInformationLeft.Information : null,
                       PersonId = person.Id,
                       CompanyUid = company.CompanyUid
                   };
        }
        #endregion
    }
}
