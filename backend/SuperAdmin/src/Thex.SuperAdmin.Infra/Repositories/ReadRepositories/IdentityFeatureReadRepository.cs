﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Thex.Kernel;
using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Dto.Dto;
using Thex.SuperAdmin.Infra.Context;
using Thex.SuperAdmin.Infra.Context.Repositories;
using Thex.SuperAdmin.Infra.Entities;
using Thex.SuperAdmin.Infra.Interfaces.ReadRepositories;
using Tnf.Localization;
using System.Linq;
using X.PagedList;
using System.Linq.Expressions;
using Thex.Dto;
using Tnf.Specifications;
using System.Globalization;

namespace Thex.SuperAdmin.Infra.Repositories.ReadRepositories
{
    public class IdentityFeatureReadRepository : SimpleRepository<IdentityFeature>, IIdentityFeatureReadRepository
    {
        private IApplicationUser _applicationUser;
        private ILocalizationManager _localizationManager;
        private readonly ThexIdentityDbContext _context;
        private readonly IConfiguration _configuration;

        public ThexIdentityDbContext Context { get { return _context; } }

        public IdentityFeatureReadRepository(
         ThexIdentityDbContext context,
         IApplicationUser applicationUser,
         IConfiguration configuration,
         ILocalizationManager localizationManager) : base(context)
        {
            _applicationUser = applicationUser;
            _localizationManager = localizationManager;
            _context = context;
            _configuration = configuration;
        }

        public async Task<bool> Exists(Guid id) 
            => await _context.IdentityFeatures.AnyAsync(x => x.Id == id);

        public new async Task<IdentityFeature> GetById(Guid id) 
            => await _context.IdentityFeatures.FirstOrDefaultAsync(x => x.Id == id);

        public async Task<ThexListDto<IdentityFeatureDto>> GetAllDtoByFilterAsync(GetAllIdentityFeatureDto request)
        {
            var dbBaseQuery = GetBaseQuery(GetAllFilters(request.SearchData));

            var totalItems = await dbBaseQuery.CountAsync();

            dbBaseQuery = GetAllPaging(request, dbBaseQuery);

            var totalItemsFiltered = await dbBaseQuery.CountAsync();
            var result = await dbBaseQuery.ToListAsync();

            return new ThexListDto<IdentityFeatureDto>
            {
                Items = result,
                HasNext = GetAllHasNext(request, totalItems, totalItemsFiltered),
                TotalItems = totalItems
            };
        }

        public async Task<IdentityFeatureDto> GetDtoAsync(Guid id)
            => await GetBaseQuery(e => e.Id == id).FirstOrDefaultAsync();

        #region private methods

        private Expression<Func<IdentityFeatureDto, bool>> GetAllFilters(string searchData)
        {
            Expression<Func<IdentityFeatureDto, bool>> filters = null;
            
            if (!string.IsNullOrEmpty(searchData))
            {
                filters = (e => e.Name.ToLower().Contains(searchData));
                filters = filters.Or(e => e.IdentityProductName.ToLower().Contains(searchData));
                filters = filters.Or(e => e.IdentityModuleName.ToLower().Contains(searchData));
            }

            return filters;
        }

        private IQueryable<IdentityFeatureDto> GetBaseQuery(Expression<Func<IdentityFeatureDto, bool>> filters)
            => (from feature in _context.IdentityFeatures.AsNoTracking()
                join product in _context.IdentityProducts.AsNoTracking() on feature.IdentityProductId equals product.Id
                join module in _context.IdentityModules.AsNoTracking() on feature.IdentityModuleId equals module.Id

                join featureLanguage in _context.IdentityTranslations.AsNoTracking() on 
                new { p1 = feature.Id, p2 = CultureInfo.CurrentCulture.Name.ToLower() } equals new { p1 = featureLanguage.IdentityOwnerId, p2 = featureLanguage.LanguageIsoCode.ToLower() } 
                into dl from featureLanguage in dl.DefaultIfEmpty()

                select new IdentityFeatureDto
                {
                    Id = feature.Id,
                    IdentityProductId = product.Id,
                    IdentityProductName = product.Name,
                    IdentityModuleId = module.Id,
                    IdentityModuleName = module.Name,
                    Name = featureLanguage != null ? featureLanguage.Name : feature.Name,
                    Description = featureLanguage != null ? featureLanguage.Description : feature.Description,
                    IsInternal = feature.IsInternal,
                    Key = feature.Key
                })
                .Where(filters ?? (e => true));

        private bool GetAllHasNext(GetAllIdentityFeatureDto request, int totalItems, int totalItemsFiltered)
            => totalItems > ((request.Page - 1) * request.PageSize) + totalItemsFiltered;

        private IQueryable<IdentityFeatureDto> GetAllPaging(GetAllIdentityFeatureDto request, IQueryable<IdentityFeatureDto> dbBaseQuery)
        {
            var pageSize = request.PageSize ?? 1000;
            var page = request.Page.HasValue ? request.Page.Value : 1;

            dbBaseQuery = dbBaseQuery.Skip(pageSize * (page - 1)).Take(pageSize);

            return dbBaseQuery;
        }

        #endregion
    }
}
