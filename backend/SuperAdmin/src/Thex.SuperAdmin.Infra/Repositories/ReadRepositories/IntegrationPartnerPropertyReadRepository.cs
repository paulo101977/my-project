﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Thex.SuperAdmin.Dto.Dto;
using Thex.SuperAdmin.Infra.Context;
using Thex.SuperAdmin.Infra.Context.Repositories;
using Thex.SuperAdmin.Infra.Entities;
using Thex.SuperAdmin.Infra.Interfaces.ReadRepositories;

namespace Thex.SuperAdmin.Infra.Repositories.ReadRepositories
{
    public class IntegrationPartnerPropertyReadRepository : SimpleRepository<IntegrationPartnerProperty>, IIntegrationPartnerPropertyReadRepository
    {
        private readonly ThexIdentityDbContext _context;

        public IntegrationPartnerPropertyReadRepository(ThexIdentityDbContext context)
            : base(context)
        {
            _context = context;
        }

        public async Task<bool> IntegrationCodesAvailableAsync(IList<IntegrationPartnerPropertyDto> integrationPartnerPropertyDtoList, int propertyId) =>
            !(await _context.IntegrationPartnerProperties.AsNoTracking().AnyAsync(ipp => (integrationPartnerPropertyDtoList.Select(ippd => new { ippd.IntegrationCode, ippd.PartnerId })
                                                                  .ToList().Contains(new { ipp.IntegrationCode, ipp.PartnerId}) && ipp.PropertyId != propertyId).AsTask()));


        public async Task<ICollection<IntegrationPartnerProperty>> GetAllByPropertyIdAsync(int propertyId) =>
            await _context.IntegrationPartnerProperties.AsNoTracking().Where(ipp => ipp.PropertyId == propertyId).ToListAsync();

        public async Task<IntegrationPartnerPropertyDto> Get(string tokenClient, string tokenApplication) =>
            await (from integrationProperty in _context.IntegrationPartnerProperties.AsNoTracking()
                   join integration in _context.IntegrationPartners.AsNoTracking()
                   on integrationProperty.IntegrationPartnerId equals integration.Id
                   where integration.TokenClient == tokenClient && integration.TokenApplication == tokenApplication
                   select new IntegrationPartnerPropertyDto
                   {
                       Id = integrationProperty.Id,
                       PropertyId = integrationProperty.PropertyId
                   })
                    .FirstOrDefaultAsync();

    }
}
