﻿using Microsoft.EntityFrameworkCore;
using System.Linq;
using Thex.Kernel;
using Thex.SuperAdmin.Infra.Context;
using Thex.SuperAdmin.Infra.Context.Repositories;
using Thex.SuperAdmin.Infra.Entities;
using Thex.SuperAdmin.Infra.Interfaces.ReadRepositories;
using Tnf.Localization;

namespace Thex.SuperAdmin.Infra.Repositories.ReadRepositories
{
    public class PropertyParameterReadRepository : SimpleRepository<PropertyParameter>, IPropertyParameterReadRepository
    {
        private readonly ILocalizationManager _localizationManager;
        private readonly ThexIdentityDbContext _context;
        private readonly IApplicationUser _applicationUser;

        public PropertyParameterReadRepository(
            ThexIdentityDbContext context,
            IApplicationUser applicationUser,
            ILocalizationManager localizationManager) : base(context)
        {
            _applicationUser = applicationUser;
            _localizationManager = localizationManager;
            _context = context;
        }

        public string GetTimeZoneByPropertyId(int propertyId)
        {
            const int applicationParameterId = 12;

            var propertyParameter = GetByPropertyIdAndApplicationParameterId(propertyId, applicationParameterId);
            if (propertyParameter == null || (propertyParameter != null && propertyParameter.PropertyParameterValue == null))
            {
                return null;
            }
            else
            {
                return propertyParameter.PropertyParameterValue;
            }
        }

        private PropertyParameter GetByPropertyIdAndApplicationParameterId(int propertyId, int applicationParameterId)
        {
            var result = _context.PropertyParameters.IgnoreQueryFilters().Where(p => p.PropertyId == propertyId && p.ApplicationParameterId == applicationParameterId).FirstOrDefault();

            return result;
        }
    }
}
