﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;
using Thex.Common;
using Thex.Kernel;
using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Dto.Dto;
using Thex.SuperAdmin.Infra.Context;
using Thex.SuperAdmin.Infra.Context.Repositories;
using Thex.SuperAdmin.Infra.Entities;
using Thex.SuperAdmin.Infra.Interfaces.ReadRepositories;
using Thex.SuperAdmin.Infra.JoinMap;
using Thex.SuperAdmin.Infra.Specifications.Tenant;
using Tnf.Dto;
using Tnf.Localization;
using X.PagedList;

namespace Thex.SuperAdmin.Infra.Repositories.ReadRepositories
{
    public class TenantReadRepository : SimpleRepository<Tenant>, ITenantReadRepository
    {
        private readonly ILocalizationManager _localizationManager;
        private readonly ThexIdentityDbContext _context;
        private readonly IApplicationUser _applicationUser;
        private IConfiguration _configuration;

        private Guid _tenantId = Guid.Empty;
        private Guid _userUid = Guid.Empty;
        private List<Guid> _tenantIds = null;

        public TenantReadRepository(
            ThexIdentityDbContext context,
            IApplicationUser applicationUser,
            IConfiguration configuration,
            ILocalizationManager localizationManager) : base(context)
        {
            _configuration = configuration;
            _applicationUser = applicationUser;
            _localizationManager = localizationManager;
            _context = context;
            _tenantIds = null;
            _tenantId = applicationUser.TenantId;
            _userUid = applicationUser.UserUid;
        }

        public List<Guid> GetIdListByLoggedTenantId()
        {
            if (_tenantIds != null)
                return _tenantIds;

            var connectionString = _configuration.GetSection("ConnectionStrings").GetValue<string>("SqlServer");
            var connection = new SqlConnection(connectionString);
            connection.Open();
            var cmd = connection.CreateCommand();

            List<Guid> result = new List<Guid>();
            using (var transaction = connection.BeginTransaction(IsolationLevel.ReadCommitted))
            {
                cmd.Transaction = transaction;

                cmd.CommandText = "GetTenantIds";
                cmd.CommandType = CommandType.StoredProcedure;

                var param = cmd.CreateParameter();
                param.ParameterName = "@MainTenantId";
                param.Direction = ParameterDirection.Input;
                param.DbType = DbType.Guid;
                cmd.Parameters.Add(param);
                param.Value = _tenantId;

                using (var reader = cmd.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            Guid tenantId = reader.GetGuid(0);
                            result.Add(tenantId);
                        }
                    }
                    reader.Close();
                }

                if (transaction != null) transaction.Commit();


            }

            connection.Close();

            _tenantIds = result.ToList();
            return _tenantIds;
        }

        public List<Guid> GetIdListByLoggedUserId()
        {
            if (_tenantIds != null)
                return _tenantIds;

            var connectionString = _configuration.GetSection("ConnectionStrings").GetValue<string>("SqlServer");
            var connection = new SqlConnection(connectionString);
            connection.Open();
            var cmd = connection.CreateCommand();

            List<Guid> result = new List<Guid>();
            using (var transaction = connection.BeginTransaction(IsolationLevel.ReadCommitted))
            {
                cmd.Transaction = transaction;

                cmd.CommandText = "GetTenantIdListByUserId";
                cmd.CommandType = CommandType.StoredProcedure;

                var param = cmd.CreateParameter();
                param.ParameterName = "@UserId";
                param.Direction = ParameterDirection.Input;
                param.DbType = DbType.Guid;
                cmd.Parameters.Add(param);
                param.Value = _userUid;

                using (var reader = cmd.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            Guid tenantId = reader.GetGuid(0);
                            result.Add(tenantId);
                        }
                    }
                    reader.Close();
                }

                if (transaction != null) transaction.Commit();


            }
            connection.Close();

            _tenantIds = result.ToList();
            return _tenantIds;
        }

        public Expression<Func<T, bool>> FilterByTenant<T>(List<Guid> tenantList)
        {
            var methodInfo = typeof(List<string>).GetMethod("Contains",
                new Type[] { typeof(string) });

            var list = Expression.Constant(tenantList);

            var param = Expression.Parameter(typeof(T), "j");
            var value = Expression.Property(param, "TenantId");
            var body = Expression.Call(list, methodInfo, value);

            // j => codes.Contains(j.Code)
            return Expression.Lambda<Func<T, bool>>(body, param);
        }

        public Tenant GetCurrentTenant()
        {
            return _context.Tenants.Where(e => e.Id == _tenantId).FirstOrDefault();
        }

        public Tenant GetTenantById(Guid id)
        {
            var tenant = _context.Tenants.FirstOrDefault(x => x.Id == id);
            return tenant;
        }

        public ThexListDto<TenantDto> GetAllDtoByFilter(GetAllTenantDto request)
        {
            request.Page = request.Page ?? 1;
            request.PageSize = request.PageSize ?? _configuration.GetValue<int>("DefaultPageSizeValue");

            var query = _context.Tenants.Include(x => x.Chain)
                                          .Include(x => x.Brand)
                                          .Include(x => x.Property)
                                          .AsQueryable();

            if (!string.IsNullOrWhiteSpace(request.SearchData))
            {
                var search = request.SearchData.ToLower();

                query = from tenant in query
                        where (tenant.TenantName.ToLower().Contains(search)) ||
                                (tenant.Chain != null && tenant.Chain.Name.ToLower().Contains(search)) ||
                                (tenant.Brand != null && tenant.Brand.Name.ToLower().Contains(search)) ||
                                (tenant.Property != null && tenant.Property.Name.ToLower().Contains(search))
                          select tenant;
            }

            var pagination = query
                       .Distinct()
                       .OrderBy(x => x.TenantName)
                       .ToPagedList(request.Page.Value, request.PageSize.Value);

            return new ThexListDto<TenantDto>()
            {
                HasNext = pagination.HasNextPage,
                Items = pagination.Select(x => new TenantDto()
                {
                    Id = x.Id,
                    TenantName = x.TenantName,
                    ChainName = x.Chain != null ? x.Chain.Name : string.Empty,
                    ChainId = x.Chain != null ? x.Chain.Id : 0,
                    BrandId = x.Brand != null ? x.Brand.Id : 0,
                    BrandName = x.Brand != null ? x.Brand.Name : string.Empty,
                    PropertyId = x.Property != null ? x.Property.Id : 0,
                    PropertyName = x.Property != null ? x.Property.Name : string.Empty

                }).Distinct().ToList(),
                TotalItems = query.Count()
            };
        }

        public TenantDto GetDtoByid(Guid id)
        {
            var tenant = _context.Tenants
                        .Include(x => x.Brand)
                        .Include(x => x.Chain)
                        .Include(x => x.Property)
                        .FirstOrDefault(x => x.Id == id);

            if (tenant != null)
            {
                return new TenantDto()
                {
                    Id = tenant.Id,
                    TenantName = tenant.TenantName,
                    ChainName = tenant.Chain != null ? tenant.Chain.Name : string.Empty,
                    ChainId = tenant.Chain != null ? tenant.Chain.Id : 0,
                    BrandId = tenant.Brand != null ? tenant.Brand.Id : 0,
                    BrandName = tenant.Brand != null ? tenant.Brand.Name : string.Empty,
                    PropertyId = tenant.Property != null ? tenant.Property.Id : 0,
                    PropertyName = tenant.Property != null ? tenant.Property.Name : string.Empty
                };
            }

            return null;
        }

        public IListDto<TenantDto> GetAllDtoWithoutAssociation(Guid? tenantId)
        {
            var tenants = _context.Tenants.Where(x => !x.BrandId.HasValue &&
                                                !x.ChainId.HasValue &&
                                                !x.PropertyId.HasValue)
                                                .ToList();

            if(tenantId.HasValue)
            {
                var tenant = _context.Tenants.FirstOrDefault(x => x.Id == tenantId);
                if (tenant != null && !tenants.Contains(tenant))
                    tenants.Add(tenant);
            }

            var tenantsDto = new List<TenantDto>();
            foreach (var tenant in tenants)
            {
                tenantsDto.Add(new TenantDto()
                {
                    Id = tenant.Id,
                    TenantName = tenant.TenantName
                });
            }

            return new ListDto<TenantDto>()
            {
                HasNext = false,
                Items = tenantsDto.OrderBy(x=>x.TenantName)
                                  .ToList()
            };
        }

        public bool Exists(Guid id)
        {
            return _context.Tenants.Any(x => x.Id == id);
        }

        public bool IsAvailable(Guid tenantId)
        {
            return _context.Tenants.Any(new TenantIsAvailableSpecification(tenantId));
        }

        public TenantPropertyParameterJoinMap GetTenantWithPropertyParameterById(Guid id)
        {
            return (from tenant in _context.Tenants.IgnoreQueryFilters()
                    join propertyParameter in this._context.PropertyParameters.IgnoreQueryFilters() on
                    tenant.Id equals propertyParameter.TenantId into tpl
                    from tenantPropertyParameterLeft in tpl.DefaultIfEmpty()
                    where (tenant.Id == id && tenantPropertyParameterLeft == null)
                    || (tenant.Id == id && tenantPropertyParameterLeft != null && tenantPropertyParameterLeft.ApplicationParameterId == 12)
                    select new TenantPropertyParameterJoinMap
                    {
                        PropertyParameter = tenantPropertyParameterLeft,
                        Tenant = tenant
                    }).FirstOrDefault();
        }
    }
}
