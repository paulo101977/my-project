﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Thex.Kernel;
using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Dto.Dto;
using Thex.SuperAdmin.Infra.Context;
using Thex.SuperAdmin.Infra.Context.Repositories;
using Thex.SuperAdmin.Infra.Entities;
using Thex.SuperAdmin.Infra.Interfaces.ReadRepositories;
using Tnf.Localization;
using System.Linq;
using X.PagedList;
using Thex.Dto;
using System.Linq.Expressions;
using Tnf.Specifications;
using System.Globalization;

namespace Thex.SuperAdmin.Infra.Repositories.ReadRepositories
{
    public class IdentityPermissionReadRepository : SimpleRepository<IdentityPermission>, IIdentityPermissionReadRepository
    {
        private IApplicationUser _applicationUser;
        private ILocalizationManager _localizationManager;
        private readonly ThexIdentityDbContext _context;
        private readonly IConfiguration _configuration;

        public ThexIdentityDbContext Context { get { return _context; } }

        public IdentityPermissionReadRepository(
         ThexIdentityDbContext context,
         IApplicationUser applicationUser,
         IConfiguration configuration,
         ILocalizationManager localizationManager) : base(context)
        {
            _applicationUser = applicationUser;
            _localizationManager = localizationManager;
            _context = context;
            _configuration = configuration;
        }

        public async Task<IdentityPermission> GetById(Guid id, bool? include = false, bool? tracking = true)
        {
            if (include.HasValue && include.Value)
                return tracking.HasValue && tracking.Value ? await _context
                    .IdentityPermissions
                    .Include(e => e.IdentityPermissionFeatureList)
                    .FirstOrDefaultAsync(x => x.Id == id) :
                    await _context
                    .IdentityPermissions.AsNoTracking()
                    .Include(e => e.IdentityPermissionFeatureList)
                    .FirstOrDefaultAsync(x => x.Id == id);

            return
                tracking.HasValue && tracking.Value ? await _context.IdentityPermissions.FirstOrDefaultAsync(x => x.Id == id) :
                await _context.IdentityPermissions.AsNoTracking().FirstOrDefaultAsync(x => x.Id == id);
        }

        public async Task<ThexListDto<IdentityPermissionDto>> GetAllDtoByFilterAsync(GetAllIdentityPermissionDto request, bool isUserId = false)
        {

            IQueryable<IdentityPermissionDto> dbBaseQuery = null;

            if (isUserId)
                dbBaseQuery = GetQueryWithUserId();
            else
                dbBaseQuery = GetBaseQuery(GetAllFilters(request));


            var totalItems = await dbBaseQuery.CountAsync();

            dbBaseQuery = GetAllPaging(request, dbBaseQuery);

            var totalItemsFiltered = await dbBaseQuery.CountAsync();
            var result = await dbBaseQuery.ToListAsync();

            return new ThexListDto<IdentityPermissionDto>
            {
                Items = result,
                HasNext = GetAllHasNext(request, totalItems, totalItemsFiltered),
                TotalItems = totalItems
            };
        }

        public async Task<IdentityPermissionDto> GetDtoAsync(Guid id)
        {
            var dto = await GetBaseQuery(x => x.Id == id).FirstOrDefaultAsync();

            if (dto != null)
                dto.IdentityPermissionFeatureList = await GetPermissionFeatureBaseQuery(x => x.IdentityPermissionId == id).ToListAsync();

            return dto;
        }

        #region private methods

        private Expression<Func<IdentityPermissionDto, bool>> GetAllFilters(GetAllIdentityPermissionDto request)
        {
            Expression<Func<IdentityPermissionDto, bool>> filters = null;

            if (!string.IsNullOrEmpty(request.SearchData))
            {
                filters = (e => e.Name.ToLower().Contains(request.SearchData));
                filters = filters.Or((e => e.IdentityProductName.ToLower().Contains(request.SearchData)));
                filters = filters.Or((e => e.IdentityModuleName.ToLower().Contains(request.SearchData)));
            }

            if (request.ProductId.HasValue)
                filters = (e => e.IdentityProductId == request.ProductId.Value);

            if (request.ModuleId.HasValue)
                filters = (e => e.IdentityModuleId == request.ModuleId.Value);

            if (request.IsActive.HasValue)
                filters = (e => e.IsActive == request.IsActive.Value);
           

            return filters;
        }

        private IQueryable<IdentityPermissionDto> GetBaseQuery(Expression<Func<IdentityPermissionDto, bool>> filters)
            => (from permission in _context.IdentityPermissions.AsNoTracking()
                join product in _context.IdentityProducts.AsNoTracking() on permission.IdentityProductId equals product.Id
                join module in _context.IdentityModules.AsNoTracking() on permission.IdentityModuleId equals module.Id
                join permissionLanguage in _context.IdentityTranslations.AsNoTracking() on
                new { p1 = permission.Id, p2 = CultureInfo.CurrentCulture.Name.ToLower() } equals new { p1 = permissionLanguage.IdentityOwnerId, p2 = permissionLanguage.LanguageIsoCode.ToLower() }
                into dl
                from permissionLanguage in dl.DefaultIfEmpty()
                select new IdentityPermissionDto
                {
                    Id = permission.Id,
                    IdentityProductId = product.Id,
                    IdentityProductName = product.Name,
                    IdentityModuleId = module.Id,
                    IdentityModuleName = module.Name,
                    Name = permissionLanguage != null ? permissionLanguage.Name : permission.Name,
                    Description = permissionLanguage != null ? permissionLanguage.Description : permission.Description,
                    IsActive = permission.IsActive
                })
                .Where(filters ?? (e => true));

        private IQueryable<IdentityPermissionDto> GetQueryWithUserId()
           => (from permission in _context.IdentityPermissions.AsNoTracking()
               join product in _context.IdentityProducts.AsNoTracking() on permission.IdentityProductId equals product.Id
               join module in _context.IdentityModules.AsNoTracking() on permission.IdentityModuleId equals module.Id
               join permissionLanguage in _context.IdentityTranslations.AsNoTracking() on
               new { p1 = permission.Id, p2 = CultureInfo.CurrentCulture.Name.ToLower() } equals new { p1 = permissionLanguage.IdentityOwnerId, p2 = permissionLanguage.LanguageIsoCode.ToLower() }
               into dl
               join identityRolePermition in _context.IdentityRolePermissions.AsNoTracking() on permission.Id equals identityRolePermition.IdentityPermissionId
               join userRoles in _context.UserRoles.AsNoTracking() on identityRolePermition.RoleId equals userRoles.RoleId
               join roles in _context.Roles.AsNoTracking() on userRoles.RoleId equals roles.Id
               join rolesLanguage in _context.IdentityTranslations.AsNoTracking() on
               new {r1 = roles.Id, r2 = CultureInfo.CurrentCulture.Name.ToLower()} equals new {r1 = rolesLanguage.IdentityOwnerId, r2 = rolesLanguage.LanguageIsoCode.ToLower()}
               into rl
               from permissionLanguage in dl.DefaultIfEmpty()
               from rolesLanguage in rl.DefaultIfEmpty()
               where userRoles.UserId.Equals(_applicationUser.UserUid) && userRoles.PropertyId.Equals(int.Parse(_applicationUser.PropertyId))
               select new IdentityPermissionDto
               {
                   Id = permission.Id,
                   IdentityProductId = product.Id,
                   IdentityProductName = product.Name,
                   IdentityModuleId = module.Id,
                   IdentityModuleName = module.Name,
                   Name = permissionLanguage != null ? permissionLanguage.Name : permission.Name,
                   Description = permissionLanguage != null ? permissionLanguage.Description : permission.Description,
                   IsActive = permission.IsActive,
                   Role = rolesLanguage != null? rolesLanguage.Name : roles.Name,
                   RoleId = roles.Id
               });
             

        private bool GetAllHasNext(GetAllIdentityPermissionDto request, int totalItems, int totalItemsFiltered)
            => totalItems > ((request.Page - 1) * request.PageSize) + totalItemsFiltered;

        private IQueryable<IdentityPermissionDto> GetAllPaging(GetAllIdentityPermissionDto request, IQueryable<IdentityPermissionDto> dbBaseQuery)
        {
            var pageSize = request.PageSize ?? 1000;
            var page = request.Page.HasValue ? request.Page.Value : 1;

            dbBaseQuery = dbBaseQuery.Skip(pageSize * (page - 1)).Take(pageSize);

            return dbBaseQuery;
        }

        private IQueryable<IdentityPermissionFeatureDto> GetPermissionFeatureBaseQuery(Expression<Func<IdentityPermissionFeatureDto, bool>> filters)
            => (from permissionFeature in _context.IdentityPermissionFeatures.AsNoTracking()

                join feature in _context.IdentityFeatures.AsNoTracking() on permissionFeature.IdentityFeatureId equals feature.Id
                join product in _context.IdentityProducts.AsNoTracking() on feature.IdentityProductId equals product.Id
                join module in _context.IdentityModules.AsNoTracking() on feature.IdentityModuleId equals module.Id

                join featureLanguage in _context.IdentityTranslations.AsNoTracking() on
                new { p1 = feature.Id, p2 = CultureInfo.CurrentCulture.Name.ToLower() } equals new { p1 = featureLanguage.IdentityOwnerId, p2 = featureLanguage.LanguageIsoCode.ToLower() }
                into dl
                from featureLanguage in dl.DefaultIfEmpty()
                select new IdentityPermissionFeatureDto
                {
                    Id = permissionFeature.Id,
                    IdentityPermissionId = permissionFeature.IdentityPermissionId,
                    IdentityFeatureId = permissionFeature.IdentityFeatureId,
                    IsActive = permissionFeature.IsActive,
                    IdentityFeature = new IdentityFeatureDto
                    {
                        Id = feature.Id,
                        IdentityProductId = product.Id,
                        IdentityProductName = product.Name,
                        IdentityModuleId = module.Id,
                        IdentityModuleName = module.Name,
                        Name = featureLanguage != null ? featureLanguage.Name : feature.Name,
                        Description = featureLanguage != null ? featureLanguage.Description : feature.Description,
                        IsInternal = feature.IsInternal,
                        Key = feature.Key
                    }
                })
                .Where(filters ?? (e => true));


        #endregion

        public async Task<List<IdentityPermissionDto>> GetAllDtoByRoleIdAsync(Guid id)
         => await (from permission in _context.IdentityPermissions.AsNoTracking()
                   join rolePermission in _context.IdentityRolePermissions.AsNoTracking() on permission.Id equals rolePermission.IdentityPermissionId
                   join product in _context.IdentityProducts.AsNoTracking() on permission.IdentityProductId equals product.Id
                   join module in _context.IdentityModules.AsNoTracking() on permission.IdentityModuleId equals module.Id

                   join permissionLanguage in _context.IdentityTranslations.AsNoTracking() on
                   new { p1 = permission.Id, p2 = CultureInfo.CurrentCulture.Name.ToLower() } equals new { p1 = permissionLanguage.IdentityOwnerId, p2 = permissionLanguage.LanguageIsoCode.ToLower() }
                   into dl
                   from permissionLanguage in dl.DefaultIfEmpty()

                   where rolePermission.RoleId == id
                   select new IdentityPermissionDto
                   {
                       Id = permission.Id,
                       IdentityProductId = product.Id,
                       IdentityProductName = product.Name,
                       IdentityModuleId = module.Id,
                       IdentityModuleName = module.Name,
                       Name = permissionLanguage != null ? permissionLanguage.Name : permission.Name,
                       Description = permissionLanguage != null ? permissionLanguage.Description : permission.Description,
                       IsActive = permission.IsActive
                   }).ToListAsync();
    }
}
