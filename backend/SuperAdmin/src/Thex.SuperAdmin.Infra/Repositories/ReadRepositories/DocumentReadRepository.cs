﻿using System;
using System.Collections.Generic;
using System.Text;
using Thex.SuperAdmin.Dto.Dto;
using Thex.SuperAdmin.Infra.Context;
using Thex.SuperAdmin.Infra.Context.Repositories;
using Thex.SuperAdmin.Infra.Entities;
using Thex.SuperAdmin.Infra.Interfaces.ReadRepositories;
using System.Linq;

namespace Thex.SuperAdmin.Infra.Repositories.ReadRepositories
{
    public class DocumentReadRepository : SimpleRepository<Document>, IDocumentReadRepository
    {
        private readonly ThexIdentityDbContext _context;

        public DocumentReadRepository(ThexIdentityDbContext context)
            : base(context)
        {
            _context = context;
        }

        public ICollection<DocumentDto> GetAllDtoByOwnerId(Guid ownerId)
        {
            var documentDtoListBaseQuery = from document in _context.Documents
                                           join documentType in _context.DocumentTypes on document.DocumentTypeId equals documentType.Id
                                           where document.OwnerId == ownerId
                                           select new DocumentDto
                                           {
                                               OwnerId = document.OwnerId,
                                               DocumentInformation = document.DocumentInformation,
                                               DocumentTypeId = document.DocumentTypeId,
                                               DocumentType = new DocumentTypeDto
                                               {
                                                   Name = documentType.Name,
                                                   Id = documentType.Id
                                               }
                                           };
            return documentDtoListBaseQuery.ToList();
        }

        public ICollection<Document> GetAllByOwnerId(Guid ownerId)
            => _context.Documents.Where(d => d.OwnerId == ownerId).ToList();
    }
}
