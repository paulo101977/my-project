﻿using System;
using System.Linq;
using Tnf.Localization;
using Thex.SuperAdmin.Infra.Context;
using Thex.SuperAdmin.Infra.Interfaces.ReadRepositories;
using Thex.SuperAdmin.Infra.Interfaces;
using Thex.SuperAdmin.Infra.Context.Repositories;
using Thex.SuperAdmin.Infra.Entities;
using Thex.Kernel;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Thex.SuperAdmin.Infra.Repositories
{
    public class ChainRepository : SimpleRepository<Chain>, IChainRepository
    {
        private readonly ILocalizationManager _localizationManager;
        private readonly ITenantReadRepository _tenantReadRepository;
        private readonly ThexIdentityDbContext _context;
        private readonly IApplicationUser _applicationUser;

        public ChainRepository(
            ThexIdentityDbContext context,
            ITenantReadRepository tenantReadRepository,
            IApplicationUser applicationUser,
            ILocalizationManager localizationManager)
            : base(context)
        {
            _tenantReadRepository = tenantReadRepository;
            _localizationManager = localizationManager;
            _applicationUser = applicationUser;
            _context = context;
        }

        public async Task<Chain> InsertAndSaveChangesAsync(Chain chain)
        {
            var entityEntry = await _context.Chains.AddAsync(chain);

            chain = entityEntry.Entity;

            await _context.SaveChangesAsync();
            return chain;
        }

        public async Task<Chain> UpdateAndSaveChangesAsync(Chain entity)
        {
            AttachIfNot(entity);

            _context.Entry(entity).State = EntityState.Modified;

            await _context.SaveChangesAsync();
            return entity;
        }
    }
}