﻿using System;
using System.Globalization;
using System.Threading.Tasks;
using Thex.Common;
using Thex.Common.Enumerations;
using Thex.SuperAdmin.Infra.Context;
using Thex.SuperAdmin.Infra.Context.Repositories;
using Thex.SuperAdmin.Infra.Entities;
using Thex.SuperAdmin.Infra.Interfaces;
using Tnf.Localization;

namespace Thex.SuperAdmin.Infra.Repositories
{
    public class PropertyMealPlanTypeRepository : SimpleRepository<PropertyMealPlanType>, IPropertyMealPlanTypeRepository
    {
        private readonly ThexIdentityDbContext _context;
        private readonly ILocalizationManager _localizationManager;

        public PropertyMealPlanTypeRepository(
            ThexIdentityDbContext context,
            ILocalizationManager localizationManager)
            : base(context)
        {
            _context = context;
            _localizationManager = localizationManager;
        }

        public async Task CreatePropertyMealPlanTypesDefaultAsync(int propertyId, Guid tenantId, string countryCode)
        {
            var cultureInfo = GetCultureInfoByCountryCode(countryCode);

            await _context.PropertyMealPlanTypes.AddAsync(
                new PropertyMealPlanType()
                {
                    Id = Guid.NewGuid(),
                    IsActive = true,
                    PropertyId = propertyId,
                    MealPlanTypeId = (int)MealPlanTypeEnum.None,
                    PropertyMealPlanTypeCode = "SAP",
                    PropertyMealPlanTypeName = _localizationManager.GetString(AppConsts.LocalizationSourceName, MealPlanTypeEnum.None.ToString(), cultureInfo),
                    TenantId = tenantId
                });

            await _context.PropertyMealPlanTypes.AddAsync(
               new PropertyMealPlanType()
               {
                   Id = Guid.NewGuid(),
                   IsActive = true,
                   PropertyId = propertyId,
                   MealPlanTypeId = (int)MealPlanTypeEnum.Breakfast,
                   PropertyMealPlanTypeCode = "CM",
                   PropertyMealPlanTypeName = _localizationManager.GetString(AppConsts.LocalizationSourceName, MealPlanTypeEnum.Breakfast.ToString(), cultureInfo),
                   TenantId = tenantId
               });

            await _context.PropertyMealPlanTypes.AddAsync(
               new PropertyMealPlanType()
               {
                   Id = Guid.NewGuid(),
                   IsActive = true,
                   PropertyId = propertyId,
                   MealPlanTypeId = (int)MealPlanTypeEnum.Halfboard,
                   PropertyMealPlanTypeCode = "MAP",
                   PropertyMealPlanTypeName = _localizationManager.GetString(AppConsts.LocalizationSourceName, MealPlanTypeEnum.Halfboard.ToString(), cultureInfo),
                   TenantId = tenantId
               });

            await _context.PropertyMealPlanTypes.AddAsync(
               new PropertyMealPlanType()
               {
                   Id = Guid.NewGuid(),
                   IsActive = true,
                   PropertyId = propertyId,
                   MealPlanTypeId = (int)MealPlanTypeEnum.Halfboardlunch,
                   PropertyMealPlanTypeCode = "MAPA",
                   PropertyMealPlanTypeName = _localizationManager.GetString(AppConsts.LocalizationSourceName, MealPlanTypeEnum.Halfboardlunch.ToString(), cultureInfo),
                   TenantId = tenantId
               });

            await _context.PropertyMealPlanTypes.AddAsync(
               new PropertyMealPlanType()
               {
                   Id = Guid.NewGuid(),
                   IsActive = true,
                   PropertyId = propertyId,
                   MealPlanTypeId = (int)MealPlanTypeEnum.Halfboardlunchdinner,
                   PropertyMealPlanTypeCode = "MAPJ",
                   PropertyMealPlanTypeName = _localizationManager.GetString(AppConsts.LocalizationSourceName, MealPlanTypeEnum.Halfboardlunchdinner.ToString(), cultureInfo),
                   TenantId = tenantId
               });

            await _context.PropertyMealPlanTypes.AddAsync(
               new PropertyMealPlanType()
               {
                   Id = Guid.NewGuid(),
                   IsActive = true,
                   PropertyId = propertyId,
                   MealPlanTypeId = (int)MealPlanTypeEnum.Fullboard,
                   PropertyMealPlanTypeCode = "FAP",
                   PropertyMealPlanTypeName = _localizationManager.GetString(AppConsts.LocalizationSourceName, MealPlanTypeEnum.Fullboard.ToString(), cultureInfo),
                   TenantId = tenantId
               });

            await _context.PropertyMealPlanTypes.AddAsync(
               new PropertyMealPlanType()
               {
                   Id = Guid.NewGuid(),
                   IsActive = true,
                   PropertyId = propertyId,
                   MealPlanTypeId = (int)MealPlanTypeEnum.Allinclusive,
                   PropertyMealPlanTypeCode = "TI",
                   PropertyMealPlanTypeName = _localizationManager.GetString(AppConsts.LocalizationSourceName, MealPlanTypeEnum.Allinclusive.ToString(), cultureInfo),
                   TenantId = tenantId
               });

            await _context.SaveChangesAsync();
        }

        private CultureInfo GetCultureInfoByCountryCode(string countryCode)
        {
            if (string.IsNullOrWhiteSpace(countryCode))
                return new CultureInfo("en-US");

            switch (countryCode.ToLower())
            {
                case "br":
                    return new CultureInfo("pt-BR");
                case "ar":
                    return new CultureInfo("es-AR");
                case "cl":
                    return new CultureInfo("es-CL");
                case "pt":
                    return new CultureInfo("pt-PT");
                default:
                    return new CultureInfo("en-US"); ;
            }
        }
    }
}
