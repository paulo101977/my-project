﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Threading.Tasks;
using Thex.Kernel;
using Thex.SuperAdmin.Infra.Context;
using Thex.SuperAdmin.Infra.Context.Repositories;
using Thex.SuperAdmin.Infra.Entities;
using Thex.SuperAdmin.Infra.Interfaces;
using Tnf.Localization;
using System.Linq;

namespace Thex.SuperAdmin.Infra.Repositories
{
    public class IdentityFeatureRepository : SimpleRepository<IdentityFeature>, IIdentityFeatureRepository
    {
        private IApplicationUser _applicationUser;
        private ILocalizationManager _localizationManager;
        private readonly ThexIdentityDbContext _context;

        public IdentityFeatureRepository(
         ThexIdentityDbContext context,
         IApplicationUser applicationUser,
         ILocalizationManager localizationManager) : base(context)
        {
            _applicationUser = applicationUser;
            _localizationManager = localizationManager;
            _context = context;
        }

        public async Task<IdentityFeature> InsertAndSaveChangesAsync(IdentityFeature entity)
        {
            var entityEntry = await _context.IdentityFeatures.AddAsync(entity);
            await _context.SaveChangesAsync();
            entity = entityEntry.Entity;

            return entity;
        }

        public async Task<IdentityFeature> UpdateAndSaveChangesAsync(IdentityFeature entity)
        {
            AttachIfNot(entity);

            _context.Entry(entity).State = EntityState.Modified;

            await _context.SaveChangesAsync();

            return entity;
        }

        public async Task RemoveAsync(Guid id)
        {
            var permissionFeatureList = await _context
                .IdentityPermissionFeatures
                .Where(e => e.IdentityFeatureId == id).ToListAsync();

            if (permissionFeatureList != null &&
                permissionFeatureList.Count() > 0)
                _context.RemoveRange(permissionFeatureList);

            base.Remove(id);

            await _context.SaveChangesAsync();
        }

    }
}
