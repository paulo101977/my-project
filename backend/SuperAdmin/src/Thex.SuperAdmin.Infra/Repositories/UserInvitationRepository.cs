﻿using System;
using System.Threading.Tasks;
using System.Linq;
using Tnf.Localization;
using Thex.SuperAdmin.Infra.Context;
using Thex.SuperAdmin.Infra.Interfaces.ReadRepositories;
using Thex.SuperAdmin.Dto;
using Microsoft.AspNetCore.Identity;
using Tnf.Dto;
using Microsoft.EntityFrameworkCore;
using Thex.SuperAdmin.Infra.Interfaces;
using Thex.SuperAdmin.Infra.Entities;
using Thex.SuperAdmin.Infra.Context.Repositories;
using Thex.Kernel;
using System.Collections.Generic;

namespace Thex.SuperAdmin.Infra.Repositories
{
    public class UserInvitationRepository : SimpleRepository<UserInvitation>, IUserInvitationRepository
    {
        private readonly ILocalizationManager _localizationManager;
        private readonly ITenantReadRepository _tenantReadRepository;
        private readonly ThexIdentityDbContext _context;
        private readonly IApplicationUser _applicationUser;

        public UserInvitationRepository(
            ThexIdentityDbContext context,
            ITenantReadRepository tenantReadRepository,
            IApplicationUser applicationUser,
            ILocalizationManager localizationManager) : base(context)
        {
            _tenantReadRepository = tenantReadRepository;
            _localizationManager = localizationManager;
            _context = context;
            _applicationUser = applicationUser;
        }

        public void ToggleAndSaveIsActive(Guid id)
        {
            var UserInvitation = _context
                .UserInvitations
                .FirstOrDefault(exp => exp.Id == id);

            var toogle = !UserInvitation.IsActive;

            UserInvitation.IsActive = toogle;
        }

        public void InvitationAccepted(Guid id)
        {
            var UserInvitation = _context
                .UserInvitations
                .FirstOrDefault(exp => exp.Id == id);

            UserInvitation.InvitationAcceptanceDate = DateTime.UtcNow;
        }

        public UserInvitation Insert(UserInvitation userInvitation)
        {
            userInvitation = _context.Add(userInvitation).Entity;
            _context.SaveChanges();
            return userInvitation;
        }

        public async Task<UserInvitation> UpdateInvitationLinkAsync(Guid userId, string newLink)
        {
            var userInvitation = await _context.UserInvitations
                                        .FirstOrDefaultAsync(exp => exp.UserId == userId && 
                                                                    exp.TenantId == _applicationUser.TenantId);

            if (userInvitation == null)
                return userInvitation;

            userInvitation.setNewLink(newLink);

            _context.SaveChanges();

            return userInvitation;
        }

        public async Task AddRangeAsync(List<UserInvitation> userInvitationList)
        {
            if (userInvitationList.Count > 0)
            {
                await _context.AddRangeAsync(userInvitationList);
                await _context.SaveChangesAsync();
            }
        }
    }
}