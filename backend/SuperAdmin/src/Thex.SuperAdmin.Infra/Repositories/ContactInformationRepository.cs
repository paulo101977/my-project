﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Thex.SuperAdmin.Infra.Context;
using Thex.SuperAdmin.Infra.Context.Repositories;
using Thex.SuperAdmin.Infra.Entities;
using Thex.SuperAdmin.Infra.Interfaces;

namespace Thex.SuperAdmin.Infra.Repositories
{
    public class ContactInformationRepository : SimpleRepository<ContactInformation>, IContactInformationRepository
    {
        private readonly ThexIdentityDbContext _context;

        public ContactInformationRepository(
       ThexIdentityDbContext context) : base(context)
        {
            _context = context;
        }

        public async Task AddRangeAndSaveChangesAsync(IList<ContactInformation> contactInformations)
        {
            await _context.AddRangeAsync(contactInformations);

            await _context.SaveChangesAsync();
        }
    }
}
