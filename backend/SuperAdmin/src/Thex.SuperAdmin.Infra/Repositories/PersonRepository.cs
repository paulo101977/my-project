﻿using System;
using System.Threading.Tasks;
using System.Linq;
using Tnf.Localization;
using Thex.SuperAdmin.Infra.Context;
using Thex.SuperAdmin.Infra.Interfaces.ReadRepositories;
using Thex.SuperAdmin.Dto;
using Microsoft.AspNetCore.Identity;
using Tnf.Dto;
using Microsoft.EntityFrameworkCore;
using Thex.SuperAdmin.Infra.Interfaces;
using Thex.SuperAdmin.Infra.Entities;
using Thex.SuperAdmin.Infra.Context.Repositories;

namespace Thex.SuperAdmin.Infra.Repositories
{
    public class PersonRepository : SimpleRepository<Person>, IPersonRepository
    {
        private readonly ILocalizationManager _localizationManager;
        private readonly ITenantReadRepository _tenantReadRepository;
        private readonly ThexIdentityDbContext _context;

        public PersonRepository(
            ThexIdentityDbContext context,
            ITenantReadRepository tenantReadRepository,
            ILocalizationManager localizationManager) : base(context)
        {
            _tenantReadRepository = tenantReadRepository;
            _localizationManager = localizationManager;
            _context = context;
        }

        public Person Insert(Person person)
        {
            person = _context.Add(person).Entity;
            _context.SaveChanges();
            return person;
        }

        public async Task<Person> UpdateAsync(Person person)
        {
            AttachIfNot(person);

            _context.Entry(person).State = EntityState.Modified;

            _context.Persons.Update(person);

            await _context.SaveChangesAsync();

            return person;
        }
    }
}