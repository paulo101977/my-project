﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Threading.Tasks;
using Thex.Kernel;
using Thex.SuperAdmin.Infra.Context;
using Thex.SuperAdmin.Infra.Context.Repositories;
using Thex.SuperAdmin.Infra.Entities;
using Thex.SuperAdmin.Infra.Interfaces;
using Tnf.Localization;
using System.Linq;
using System.Collections.Generic;
using Thex.SuperAdmin.Dto;

namespace Thex.SuperAdmin.Infra.Repositories
{
    public class IdentityTranslationRepository : SimpleRepository<IdentityTranslation>, IIdentityTranslationRepository
    {
        private IApplicationUser _applicationUser;
        private ILocalizationManager _localizationManager;
        private readonly ThexIdentityDbContext _context;

        public IdentityTranslationRepository(
         ThexIdentityDbContext context,
         IApplicationUser applicationUser,
         ILocalizationManager localizationManager) : base(context)
        {
            _applicationUser = applicationUser;
            _localizationManager = localizationManager;
            _context = context;
        }

        public async Task InsertOrUpdateAndSaveChangesAsync(Guid ownerId, List<SimpleTranslationDto> translationList)
        {
            if (translationList == null || translationList.Count == 0)
                return;

            var translationToRemoveList = await _context.IdentityTranslations.Where(e => e.IdentityOwnerId == ownerId).ToListAsync();

            if (translationToRemoveList.Count > 0)
                _context.RemoveRange(translationToRemoveList);

            var translationToAddList = new List<IdentityTranslation>();
            foreach (var translation in translationList)
                translationToAddList.Add(
                    new IdentityTranslation {
                        Id = Guid.NewGuid(),
                        Name = translation.Name,
                        Description = translation.Description,
                        LanguageIsoCode = translation.LanguageIsoCode,
                        IdentityOwnerId = ownerId
                    });

            await _context.IdentityTranslations.AddRangeAsync(translationToAddList);
            await _context.SaveChangesAsync();
        }
    }
}
