﻿using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using Thex.Kernel;
using Thex.SuperAdmin.Infra.Context;
using Thex.SuperAdmin.Infra.Context.Repositories;
using Thex.SuperAdmin.Infra.Entities;
using Thex.SuperAdmin.Infra.Interfaces;
using Tnf.Localization;

namespace Thex.SuperAdmin.Infra.Repositories
{
    public class BrandRepository : SimpleRepository<Brand>, IBrandRepository
    {
        private IApplicationUser _applicationUser;
        private ILocalizationManager _localizationManager;
        private readonly ThexIdentityDbContext _context;

        public BrandRepository(
         ThexIdentityDbContext context,
         IApplicationUser applicationUser,
         ILocalizationManager localizationManager) : base(context)
        {
            _applicationUser = applicationUser;
            _localizationManager = localizationManager;
            _context = context;
        }

        public async Task<Brand> InsertAndSaveChangesAsync(Brand brand)
        {
            var entityEntry = await _context.Brands.AddAsync(brand);
            await _context.SaveChangesAsync();
            brand = entityEntry.Entity;

            return brand;
        }

        public async Task<Brand> UpdateAndSaveChangesAsync(Brand brand)
        {
            AttachIfNot(brand);

            _context.Entry(brand).State = EntityState.Modified;

            await _context.SaveChangesAsync();

            return brand;
        }
    }
}
