﻿
namespace Thex.SuperAdmin.Dto.Email
{
    public class EmailForgotPasswordDto : EmailBaseDto
    {
        public string UrlForgotPassword { get; set; }
    }
}
