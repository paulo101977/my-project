﻿
namespace Thex.SuperAdmin.Dto.Email
{
    public class EmailIntegrationDto : EmailBaseDto
    {
        public string ErrorMessage { get; set; }
    }
}
