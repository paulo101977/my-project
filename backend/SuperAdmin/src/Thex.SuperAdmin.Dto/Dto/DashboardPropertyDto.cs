﻿using System;
using System.Collections.Generic;
using Tnf.Dto;

namespace Thex.SuperAdmin.Dto
{
    public class DashboardPropertyDto : BaseDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime CreationDate { get; set; }
        public string InitialsName { get; set; }
        public int PropertyStatusId { get; set; }
        public string PropertyStatusName { get; set; }
        public bool IsBlocked { get; set; }

        public IList<PropertyStatusHistoryDto> PropertyStatusHistoryDtoList { get; set; }

        public DashboardPropertyDto()
        {
            PropertyStatusHistoryDtoList = new List<PropertyStatusHistoryDto>();
        }
    }
}
