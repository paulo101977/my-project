﻿using System;
using System.Collections.Generic;
using Tnf.Dto;

namespace Thex.SuperAdmin.Dto
{
    public partial class IdentityMenuFeatureDto : BaseDto
    {
        public static IdentityMenuFeatureDto NullInstance = null;

        public Guid Id { get; set; }
        public int IdentityProductId { get; set; }
        public int IdentityModuleId { get; set; }
        public Guid? IdentityFeatureParentId { get; set; }
        public Guid IdentityFeatureId { get; set; }
        public bool IsActive { get; set; }
        public int OrderNumber { get; set; }
        public int Level { get; set; }
        public string Description { get; set; }
        public bool IsDeleted { get; set; }
        //public DateTime CreationTime { get; set; }
        //public Guid? CreatorUserId { get; set; }
        //public DateTime? LastModificationTime { get; set; }
        //public Guid? LastModifierUserId { get; set; }
        //public DateTime? DeletionTime { get; set; }
        //public Guid? DeleterUserId { get; set; }

        public virtual IdentityFeatureDto IdentityFeature { get; set; }
        //public virtual IdentityModuleDto IdentityModule { get; set; }
        //public virtual IdentityFeatureDto IdentityFeatureParent { get; set; }
        //public virtual IdentityProductDto IdentityProduct { get; set; }
        public MenuItemDto ParentMenuList { get; set; }

        public string IdentityProductName { get; set; }
        public string IdentityModuleName { get; set; }

    }
}
