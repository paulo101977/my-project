﻿using System;
using System.Collections.Generic;
using Tnf.Dto;

namespace Thex.SuperAdmin.Dto
{
    public partial class SimpleTranslationDto
    {
        public string LanguageIsoCode { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
