﻿using System;
using System.Collections.Generic;
using Tnf.Dto;

namespace Thex.SuperAdmin.Dto
{
    public partial class IdentityRolePermissionDto : BaseDto
    {
        public static IdentityRolePermissionDto NullInstance = null;

        public Guid Id { get; set; }
        public Guid IdentityPermissionId { get; set; }
        public Guid RoleId { get; set; }
        public bool IsActive { get; set; }
        //public bool IsDeleted { get; set; }
        //public DateTime CreationTime { get; set; }
        //public Guid? CreatorUserId { get; set; }
        //public DateTime? LastModificationTime { get; set; }
        //public Guid? LastModifierUserId { get; set; }
        //public DateTime? DeletionTime { get; set; }
        //public Guid? DeleterUserId { get; set; }

        public virtual IdentityPermissionDto IdentityPermission { get; set; }
    }
}
