﻿using System;
using System.Collections.Generic;
using Tnf.Dto;

namespace Thex.SuperAdmin.Dto
{
    public partial class UpdateUserDto : BaseDto
    { 
        public Guid Id { get; set; }
        public static UpdateUserDto NullInstance = null;
        public List<Guid> RoleIdList { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public string Name { get; set; }        
        public string PhoneNumber { get; set; }
        public string NickName { get; set; }
    }

}
