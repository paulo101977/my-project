﻿using System;
using System.Collections.Generic;
using Tnf.Dto;

namespace Thex.SuperAdmin.Dto
{
    public partial class CountryLanguageDto : BaseDto
    {
        public int Id { get; set; }
        public static CountryLanguageDto NullInstance = null;

        public int CountrySubdivisionId { get; set; }
        public string Language { get; set; }
        public string LanguageTwoLetterIsoCode { get; set; }
        public string LanguageThreeLetterIsoCode { get; set; }
        public string CultureInfoCode { get; set; }

        public virtual CountrySubdivisionDto CountrySubdivision { get; set; }
    }
}
