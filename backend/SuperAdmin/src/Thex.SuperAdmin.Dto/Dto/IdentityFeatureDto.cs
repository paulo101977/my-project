﻿using System;
using System.Collections.Generic;
using Tnf.Dto;

namespace Thex.SuperAdmin.Dto
{
    public partial class IdentityFeatureDto : BaseDto
    {
        public static IdentityFeatureDto NullInstance = null;

        //public IdentityFeatureDto()
        //{
        //    IdentityMenuFeatureList = new HashSet<IdentityMenuFeatureDto>();
        //    IdentityPermissionFeatureList = new HashSet<IdentityPermissionFeatureDto>();
        //}

        public Guid Id { get; set; }
        public int IdentityProductId { get; set; }
        public int IdentityModuleId { get; set; }
        public string IdentityProductName { get; set; }
        public string IdentityModuleName { get; set; }
        public string Key { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsInternal { get; set; }
        //public bool IsDeleted { get; set; }
        //public DateTime CreationTime { get; set; }
        //public Guid? CreatorUserId { get; set; }
        //public DateTime? LastModificationTime { get; set; }
        //public Guid? LastModifierUserId { get; set; }
        //public DateTime? DeletionTime { get; set; }
        //public Guid? DeleterUserId { get; set; }

        //public virtual IdentityModuleDto IdentityModule { get; set; }
        //public virtual IdentityProductDto IdentityProduct { get; set; }
        //public virtual ICollection<IdentityMenuFeatureDto> IdentityMenuFeatureList { get; set; }
        //public virtual ICollection<IdentityPermissionFeatureDto> IdentityPermissionFeatureList { get; set; }
    }
}
