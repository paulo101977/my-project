﻿using System;
using System.Collections.Generic;
using Tnf.Dto;

namespace Thex.SuperAdmin.Dto
{
    public partial class ContactInformationTypeDto : BaseDto
    {
        public int Id { get; set; }
        public static ContactInformationTypeDto NullInstance = null;

        public ContactInformationTypeDto()
        {
            ContactInformationList = new HashSet<ContactInformationDto>();
        }

        public string Name { get; set; }
        public string StringFormatMask { get; set; }
        public string RegexValidationExpression { get; set; }

        public virtual ICollection<ContactInformationDto> ContactInformationList { get; set; }
    }
}
