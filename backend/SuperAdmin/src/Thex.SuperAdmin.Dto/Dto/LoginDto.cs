﻿using System;
using System.Collections.Generic;
using Tnf.Dto;

namespace Thex.SuperAdmin.Dto
{
    public class LoginDto
    {
        public string Login { get; set; }
        public string Password { get; set; }
        public string GrantType { get; set; }
        public int ProductId { get; set; }

        public string RefreshToken { get; set; }
        public Guid UserId { get; set; }
    }
}

