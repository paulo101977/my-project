﻿using System;
using System.Collections.Generic;
using Tnf.Dto;

namespace Thex.SuperAdmin.Dto
{
    public partial class BrandDto : BaseDto
    {
        public int Id { get; set; }
        public static BrandDto NullInstance = null;

        public BrandDto()
        {
            PropertyList = new HashSet<PropertyDto>();
        }

        public int ChainId { get; set; }
        public string ChainName { get; set; }
        public Guid TenantId { get; set; }
        public string TenantName { get; set; }
        public string Name { get; set; }
        public bool IsTemporary { get; set; }

        public virtual ChainDto Chain { get; set; }
        public virtual ICollection<PropertyDto> PropertyList { get; set; }
    }
}
