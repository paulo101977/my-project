﻿using System;
using System.Collections.Generic;
using Thex.Common.Dto;
using Thex.Common.Enumerations;
using Thex.SuperAdmin.Dto.Dto;
using Tnf.Dto;

namespace Thex.SuperAdmin.Dto
{
    public partial class PropertyDto : BaseDto
    {
        public int Id { get; set; }
        public static PropertyDto NullInstance = null;
        public PropertyDto()
        {
            IntegrationPartnerPropertyList = new List<IntegrationPartnerPropertyDto>();
        }

        public PropertyTypeEnum PropertyTypeId { get; set; }
        public string Name { get; set; }
        public string ContactFirstName { get; set; }
        public string ContactLastName { get; set; }
        public string ContactEmail { get; set; }
        public string ContactPhone { get; set; }
        public string ContactWebSite { get; set; }
        public string ShortName { get; set; }
        public string TradeName { get; set; }
        public Guid PropertyUId { get; set; }
        public Guid TenantId { get; set; }
        public int PropertyStatusId { get; set; }
        public string PhotoUrl { get; set; }
        public bool IsBlocked { get; set; }
        public int MaxUh { get; set; }
        public int RegisteredUhs { get; set; }

        public virtual PropertyTypeDto PropertyType { get; set; }
        public virtual CompanyDto Company { get; set; }
        public virtual BrandDto Brand { get; set; }

        public IList<LocationDto> LocationList { get; set; }
        public IList<IntegrationPartnerPropertyDto> IntegrationPartnerPropertyList { get; set; }

        public int CompanyId { get; set; }
        public int BrandId { get; set; }
        public int ChainId { get; set; }
        public string ChainName { get; set; }
        public string TenantName { get; set; }
        public string BrandName { get; set; }
        public Guid CompanyUid { get; set; }
    }

    public class CreateOrUpdatePropertyDto : BaseDto
    {
        public static CreateOrUpdatePropertyDto NullInstance = null;

        public int Id { get; set; }
        public int CompanyId { get; set; }
        public int BrandId { get; set; }
        public string Name { get; set; }
        public Guid PropertyUId { get; private set; }
        public Guid TenantId { get; set; }
        public string PhotoUrl { get; set; }
        public bool IsBlocked { get; set; }
        public int MaxUh { get; set; }

        public IList<LocationDto> LocationList { get; set; }
        public IList<IntegrationPartnerPropertyDto> IntegrationPartnerPropertyList { get; set; }

        public CreateOrUpdatePropertyDto()
        {
            IntegrationPartnerPropertyList = new List<IntegrationPartnerPropertyDto>();
        }

        public void GeneratePropertyUid()
        {
            this.PropertyUId = Guid.NewGuid();
        }
    }
}
