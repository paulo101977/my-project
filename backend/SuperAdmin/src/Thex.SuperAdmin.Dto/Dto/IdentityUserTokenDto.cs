﻿using System;
using System.Collections.Generic;
using Tnf.Dto;

namespace Thex.SuperAdmin.Dto
{
    public partial class UserTokenDto : BaseDto
    {
        public static UserTokenDto NullInstance = null;
        
        public Guid UserId { get; set; }
        public Guid LoginProvider { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
        public bool? IsDeleted { get; set; }
        public DateTime? CreationTime { get; set; }
        public Guid? CreatorUserId { get; set; }
        public DateTime? LastModificationTime { get; set; }
        public Guid? LastModifierUserId { get; set; }
        public DateTime? DeletionTime { get; set; }
        public Guid? DeleterUserId { get; set; }
    }
}
