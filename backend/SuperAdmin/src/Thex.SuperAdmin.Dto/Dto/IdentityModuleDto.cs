﻿using System;
using System.Collections.Generic;
using Tnf.Dto;

namespace Thex.SuperAdmin.Dto
{
    public partial class IdentityModuleDto : BaseDto
    {
        public static IdentityModuleDto NullInstance = null;

        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
