﻿using System;
using System.Collections.Generic;
using Tnf.Dto;

namespace Thex.SuperAdmin.Dto
{
    public partial class MenuItemDto : BaseDto
    {
        public Guid Id { get; set; }
        public int ProductId { get; set; }
        public int ModuleId { get; set; }
        public bool IsActive { get; set; }
        public int OrderNumber { get; set; }
        public string Description { get; set; }

        public bool IsInternal { get; set; }

        public Guid FeatureId { get; set; }
        public string FeatureName { get; set; }
        public string Key { get; set; }

        public Guid? ParentId { get; set; }
        public MenuItemDto Parent { get; set; }
        public List<MenuItemDto> Children { get; set; }
    }
}
