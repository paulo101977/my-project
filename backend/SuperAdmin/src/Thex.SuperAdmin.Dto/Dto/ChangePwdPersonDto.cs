﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Thex.SuperAdmin.Dto
{
    public class ChangePwdPersonDto
    {
        public Guid UserId { get; set; }
        public string OldPassword { get; set; }
        public string NewPassword { get; set; }
    }
}
