﻿using System;
using System.Collections.Generic;
using Thex.Common.Enumerations;
using Tnf.Dto;

namespace Thex.SuperAdmin.Dto
{
    public class LegalPersonDto : PersonDto
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LegalPersonDto"/> class.
        /// </summary>
        public LegalPersonDto() : base()
        {
            _personType = PersonTypeEnum.Legal;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="LegalPersonDto"/> class.
        /// </summary>
        /// <param name="tradeName">Trade name</param>
        /// <param name="shortName">Short name</param>
        public LegalPersonDto(string tradeName, string shortName) : this()
        {
            FirstName = tradeName;
            LastName = shortName;
        }
    }
}