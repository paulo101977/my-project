﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Thex.SuperAdmin.Dto.Dto
{
    public class DocumentTypeDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
