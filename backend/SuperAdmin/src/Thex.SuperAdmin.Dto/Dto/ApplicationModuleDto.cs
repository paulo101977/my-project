﻿using System;
using System.Collections.Generic;
using Tnf.Dto;

namespace Thex.SuperAdmin.Dto
{
    public partial class ApplicationModuleDto : BaseDto
    {
        public int Id { get; set; }
        public static ApplicationModuleDto NullInstance = null;

        public ApplicationModuleDto()
        {
            ApplicationParameterList = new HashSet<ApplicationParameterDto>();
        }

        public string ApplicationModuleName { get; set; }
        public string ApplicationModuleDescription { get; set; }

        public virtual ICollection<ApplicationParameterDto> ApplicationParameterList { get; set; }
    }
}
