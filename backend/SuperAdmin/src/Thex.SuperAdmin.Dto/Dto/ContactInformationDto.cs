﻿using System;
using System.Collections.Generic;
using Tnf.Dto;

namespace Thex.SuperAdmin.Dto
{
    public partial class ContactInformationDto : BaseDto
    {
        public int Id { get; set; }
        public static ContactInformationDto NullInstance = null;

        public Guid OwnerId { get; set; }
        public int ContactInformationTypeId { get; set; }
        public string Information { get; set; }

        public virtual ContactInformationTypeDto ContactInformationType { get; set; }
        public virtual PersonDto Owner { get; set; }
    }
}
