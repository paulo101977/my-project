﻿using System;
using System.Collections.Generic;
using Thex.Common.Dto;
using Thex.SuperAdmin.Dto.Dto;
using Tnf.Dto;

namespace Thex.SuperAdmin.Dto
{
    public partial class CompanyDto : BaseDto
    {
        public int Id { get; set; }
        public static CompanyDto NullInstance = null;

        public CompanyDto()
        {
            InverseParentCompanyList = new HashSet<CompanyDto>();
            PropertyList = new HashSet<PropertyDto>();
            DocumentList = new HashSet<DocumentDto>();
            LocationList = new List<LocationDto>();
        }

        public string ShortName { get; set; }
        public string TradeName { get; set; }
        public string CompanyTypeName { get; set; }
        public Guid CompanyUid { get; set; }

        public virtual CompanyDto ParentCompany { get; set; }
        public virtual ICollection<CompanyDto> InverseParentCompanyList { get; set; }
        public virtual ICollection<PropertyDto> PropertyList { get; set; }
        public string Email { get; set; }
        public string HomePage { get; set; }
        public Guid PersonId { get; set; }
        public ICollection<DocumentDto> DocumentList { get; set; }
        public IList<LocationDto> LocationList { get; set; }
        public char PersonType { get; set; }
    }
}
