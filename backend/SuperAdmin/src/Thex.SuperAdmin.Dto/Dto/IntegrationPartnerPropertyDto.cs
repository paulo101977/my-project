﻿using System;

namespace Thex.SuperAdmin.Dto.Dto
{
    public class IntegrationPartnerPropertyDto
    {
        public Guid Id { get; set; }

        public int PropertyId { get; set; }
        public bool IsActive { get; set; }
        public string IntegrationCode { get; set; }
        public int PartnerId { get; set; }
        public string PartnerName { get; set; }
        public int? IntegrationPartnerId { get; set; }
        public string TokenApplication { get; set; }
        public string TokenClient { get; set; }
    }
}
