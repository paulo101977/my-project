﻿using System;
using System.Collections.Generic;
using Tnf.Dto;

namespace Thex.SuperAdmin.Dto
{
    public partial class LocationCategoryDto : BaseDto
    {
        public int Id { get; set; }
        public static LocationCategoryDto NullInstance = null;

        public string Name { get; set; }
        public string RecordScope { get; set; }
    }
}
