﻿using System;
using System.Collections.Generic;
using Thex.Common.Enumerations;
using Tnf.Dto;

namespace Thex.SuperAdmin.Dto
{
    public partial class PersonDto : BaseDto
    {
        public Guid Id { get; set; }
        public static PersonDto NullInstance = null;

        protected PersonTypeEnum _personType;

        public PersonDto()
        {
            ContactInformationList = new HashSet<ContactInformationDto>();
            UserList = new HashSet<UserDto>();
        }
        

        public string PersonType { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FullName { get; set; }
        public string PhotoUrl { get; set; }

        public DateTime? DateOfBirth { get; set; }
        public int? CountrySubdivisionId { get; set; }

        public string NickName { get; set; }


        public virtual CompanyDto Company { get; set; }
        public virtual ICollection<ContactInformationDto> ContactInformationList { get; set; }
        public virtual ICollection<UserDto> UserList { get; set; }
    }
}
