﻿using System;
using System.Collections.Generic;
using Thex.Kernel.Dto;
using Tnf.Dto;

namespace Thex.SuperAdmin.Dto
{
    public partial class UserDto : BaseDto
    {
        public Guid Id { get; set; }
        public static UserDto NullInstance = null;

        public UserDto()
        {
        }

        public Guid PersonId { get; set; }
        public Guid TenantId { get; set; }
        public string UserName { get; set; }
        public string PreferredLanguage { get; set; }
        public string PreferredCulture { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string LastLink { get; set; }
        public bool IsActive { get; set; }
        public bool? IsAdmin { get; set; }
        public int? PropertyId { get; set; }
        public Guid? PropertyUId { get; set; }

        public int? ChainId { get; set; }
        public int? BrandId { get; set; }
        public string PhotoUrl { get; set; }
        public string PhotoBase64 { get; set; }


        public string NormalizedUserName { get; set; }
        public string NormalizedEmail { get; set; }
        public bool EmailConfirmed { get; set; }
        public string PasswordHash { get; set; }
        public string SecurityStamp { get; set; }
        public string ConcurrencyStamp { get; set; }
        public string PhoneNumber { get; set; }
        public bool PhoneNumberConfirmed { get; set; }
        public bool TwoFactorEnabled { get; set; }
        public DateTimeOffset? LockoutEnd { get; set; }
        public bool LockoutEnabled { get; set; }
        public int AccessFailedCount { get; set; }
        public bool? IsDeleted { get; set; }
        public DateTime? CreationTime { get; set; }
        public Guid? CreatorUserId { get; set; }
        public DateTime? LastModificationTime { get; set; }
        public Guid? LastModifierUserId { get; set; }
        public DateTime? DeletionTime { get; set; }
        public Guid? DeleterUserId { get; set; }

        public List<LoggedUserPropertyDto> PropertyList { get; set; }
        public List<Guid> RoleIdList { get; set; }

        public DateTime? DateOfBirth { get; set; }        
        public string NickName { get; set; }

    }
}
