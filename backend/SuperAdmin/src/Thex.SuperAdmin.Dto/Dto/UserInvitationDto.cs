﻿using System;
using System.Collections.Generic;
using Tnf.Dto;

namespace Thex.SuperAdmin.Dto
{
    public partial class UserInvitationDto : BaseDto
    {
        public Guid Id { get; set; }
        public static UserInvitationDto NullInstance = null;

        public int? ChainId { get; set; }
        public int? PropertyId { get; set; }
        public int? BrandId { get; set; }
        public string Email { get; set; }
        public string Name { get; set; }
        public string InvitationLink { get; set; }

        public DateTime InvitationDate { get; set; }

        public DateTime? InvitationAcceptanceDate { get; set; }
        public bool IsActive { get; set; }
        public bool IsAdmin { get; set; }
        public string PreferredCulture { get; set; }
        public string PreferredLanguage { get; set; }

        public string ChainName { get; set; }
        public string PropertyName { get; set; }
        public string BrandName { get; set; }

        public Guid? UserId { get; set; }

        public List<Guid> RoleIdList { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public string PhoneNumber { get; set; }
        public string NickName { get; set; }




    }
}
