﻿using System;
using System.Collections.Generic;
using Tnf.Dto;

namespace Thex.SuperAdmin.Dto
{
    public partial class FeatureGroupDto : BaseDto
    {
        public int Id { get; set; }
        public static FeatureGroupDto NullInstance = null;

        public FeatureGroupDto()
        {
            ApplicationParameterList = new HashSet<ApplicationParameterDto>();
        }

        public string FeatureGroupName { get; set; }
        public string Description { get; set; }

        public virtual ICollection<ApplicationParameterDto> ApplicationParameterList { get; set; }
    }
}
