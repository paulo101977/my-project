﻿using System;
using System.Collections.Generic;
using Tnf.Dto;

namespace Thex.SuperAdmin.Dto
{
    public partial class IdentityPermissionuserDto : BaseDto
    {
        public static IdentityPermissionuserDto NullInstance = null;

        public string Permission { get; set; }
        public Guid PermissionId { get; set; }
        public List<IdentityPermissionDto> PermissionList { get; set; }


    }
}
