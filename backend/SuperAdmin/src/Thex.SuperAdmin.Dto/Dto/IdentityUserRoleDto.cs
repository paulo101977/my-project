﻿using System;
using System.Collections.Generic;
using Tnf.Dto;

namespace Thex.SuperAdmin.Dto
{
    public partial class UserRoleDto : BaseDto
    {
        public static UserRoleDto NullInstance = null;
        
        public Guid UserId { get; set; }
        public Guid RoleId { get; set; }

        public int? PropertyId { get; set; }
        public int? ChainId { get; set; }
        public int? BrandId { get; set; }

        public bool? IsDeleted { get; set; }
        public DateTime? CreationTime { get; set; }
        public Guid? CreatorUserId { get; set; }
        public DateTime? LastModificationTime { get; set; }
        public Guid? LastModifierUserId { get; set; }
        public DateTime? DeletionTime { get; set; }
        public Guid? DeleterUserId { get; set; }
    }
}
