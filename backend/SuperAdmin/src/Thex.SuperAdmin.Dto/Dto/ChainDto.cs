﻿using System;
using System.Collections.Generic;
using Tnf.Dto;

namespace Thex.SuperAdmin.Dto
{
	public partial class ChainDto : BaseDto
	{
        public static ChainDto NullInstance = null;

        public int Id { get; set; }
		public string Name { get; set; }
        public bool IsTemporary { get; set; }

        public Guid TenantId { get; set; }
        public string TenantName { get; set; }

        public int PropertyId { get; set; }
        public Guid PropertyUid { get; set; }
        public string PropertyName { get; set; }

        public int BrandId { get; set; }
        public string BrandName { get; set; }

        public int CompanyId { get; set; }
        public Guid CompanyUid { get; set; }
        public string ShortName { get; set; }
        public string TradeName { get; set; }

        public ICollection<PropertyDto> PropertyDtoList { get; set; }
        public ICollection<BrandDto> BrandDtoList { get; set; }

        public ChainDto()
        {
            PropertyDtoList = new HashSet<PropertyDto>();
            BrandDtoList = new HashSet<BrandDto>();
        }
    }
}
