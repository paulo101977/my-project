﻿using System;
using System.Collections.Generic;
using Tnf.Dto;

namespace Thex.SuperAdmin.Dto
{
    public partial class MenuItemSimpleDto : BaseDto
    {
        public int OrderNumber { get; set; }
        public string Name { get; set; }
        public string Key { get; set; }

        public List<MenuItemSimpleDto> Children { get; set; }
    }
}
