﻿
namespace Thex.SuperAdmin.Dto.Dto
{
    public class PartnerDto
    {
        public int Id { get; set; }
        public string PartnerName { get; set; }
    }
}
