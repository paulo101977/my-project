﻿using System;
using System.Collections.Generic;
using Tnf.Dto;

namespace Thex.SuperAdmin.Dto
{
    public partial class UserProductPermissionWithMenuDto : BaseDto
    {
        public UserProductPermissionWithMenuDto()
        {
            this.PermissionList = new List<ProductPermissionDto>();
        }

        public List<ProductPermissionDto> PermissionList { get; set; }
    }

    public class ProductPermissionDto
    {
        public ProductPermissionDto()
        {
            this.KeyList = new List<KeyListDto>();
            this.MenuList = new List<MenuItemSimpleDto>();
        }

        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public bool Active { get; set; }
        public List<KeyListDto> KeyList { get; set; }
        public List<MenuItemSimpleDto> MenuList { get; set; }
    }

    public class KeyListDto
    {
        public string Key { get; set; }
    }
}
