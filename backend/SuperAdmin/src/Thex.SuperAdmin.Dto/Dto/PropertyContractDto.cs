﻿using System;
using Tnf.Dto;

namespace Thex.SuperAdmin.Dto.Dto
{
    public class PropertyContractDto : BaseDto
    {
        public Guid Id { get; set; }
        public int PropertyId { get; set; }
        public int MaxUh { get; set; }
        public int RegisteredUhs { get; set; }
    }
}
