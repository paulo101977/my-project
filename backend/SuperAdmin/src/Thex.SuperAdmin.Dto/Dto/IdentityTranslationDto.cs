﻿using System;
using System.Collections.Generic;
using Tnf.Dto;

namespace Thex.SuperAdmin.Dto
{
    public partial class IdentityTranslationDto : BaseDto
    {
        public Guid Id { get; set; }
        public static IdentityTranslationDto NullInstance = null;

        public string LanguageIsoCode { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public Guid IdentityOwnerId { get; set; }
    }
}
