﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Thex.SuperAdmin.Dto.Dto
{
    public class DocumentDto
    {
        public int Id { get; set; }
        public static DocumentDto NullInstance = null;

        public Guid OwnerId { get; set; }
        public int DocumentTypeId { get; set; }
        public string DocumentInformation { get; set; }
        public DocumentTypeDto DocumentType { get; set; }
    }
}
