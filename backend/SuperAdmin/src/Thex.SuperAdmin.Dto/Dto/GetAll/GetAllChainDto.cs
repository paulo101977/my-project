﻿using Tnf.Dto;

namespace Thex.SuperAdmin.Dto
{
    public class GetAllChainDto : RequestAllDto
    {
        public string SearchData { get; set; }
    }
}
