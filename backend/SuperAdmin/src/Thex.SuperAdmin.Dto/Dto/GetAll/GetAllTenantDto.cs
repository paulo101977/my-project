﻿using System;
using Tnf.Dto;

namespace Thex.SuperAdmin.Dto
{
    public class GetAllTenantDto : RequestAllDto
    {
        public string SearchData { get; set; }
    }
}
