﻿using System.Collections.Generic;
using Tnf.Dto;

namespace Thex.SuperAdmin.Dto
{
    public class GetAllPropertyDto : RequestAllDto
    {
        public string SearchData { get; set; }
        public ICollection<int> BrandIdList { get; set; }

        public GetAllPropertyDto()
        {
            BrandIdList = new HashSet<int>();
        }
    }
}
