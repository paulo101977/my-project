﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a Crudzilla tool.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using Tnf.Dto;

namespace Thex.Dto
{
    public partial class GetAllIdentityPermissionDto : RequestAllDto
    {
        public string SearchData { get; set; }
        
        public int? ProductId { get; set; }

        public int? ModuleId { get; set; }

        public bool? IsActive { get; set; }
    }
}
