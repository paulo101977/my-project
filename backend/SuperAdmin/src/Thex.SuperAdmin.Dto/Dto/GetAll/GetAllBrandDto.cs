﻿using Tnf.Dto;

namespace Thex.SuperAdmin.Dto
{
    public class GetAllBrandDto : RequestAllDto
    {
        public string SearchData { get; set; }
    }
}
