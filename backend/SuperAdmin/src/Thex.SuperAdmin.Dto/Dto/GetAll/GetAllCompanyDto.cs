﻿using Tnf.Dto;

namespace Thex.SuperAdmin.Dto
{
    public class GetAllCompanyDto : RequestAllDto
    {
        public string SearchData { get; set; }
    }
}
