﻿using Tnf.Dto;

namespace Thex.SuperAdmin.Dto
{
    public class GetAllUserDto : RequestAllDto
    {
        public string SearchData { get; set; }
    }
}
