﻿using System;
using Tnf.Dto;

namespace Thex.SuperAdmin.Dto.Dto
{
    public class UserTenantDto : BaseDto
    {
        public Guid Id { get; set; }
        public Guid UserId { get; set; }
        public Guid TenantId { get; set; }
        public bool IsActive { get; set; }
    }
}
