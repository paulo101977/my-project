﻿using System;
using System.Collections.Generic;
using Tnf.Dto;

namespace Thex.SuperAdmin.Dto
{
    using Thex.Common.Enumerations;

    public class NaturalPersonDto : PersonDto
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="NaturalPersonDto"/> class.
        /// </summary>
        public NaturalPersonDto() : base()
        {
            this._personType = PersonTypeEnum.Natural;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="NaturalPersonDto"/> class.
        /// </summary>
        /// <param name="firstName">First name</param>
        /// <param name="lastName">Second name</param>
        public NaturalPersonDto(string firstName, string lastName) : this()
        {
            this.FirstName = firstName;

            this.LastName = lastName;
        }
    }
}