﻿using System;
using System.Collections.Generic;
using Tnf.Dto;

namespace Thex.SuperAdmin.Dto
{
    public partial class ParameterTypeDto : BaseDto
    {
        public int Id { get; set; }
        public static ParameterTypeDto NullInstance = null;

        public ParameterTypeDto()
        {
            ApplicationParameterList = new HashSet<ApplicationParameterDto>();
        }

        public string ParameterTypeName { get; set; }

        public virtual ICollection<ApplicationParameterDto> ApplicationParameterList { get; set; }
    }
}
