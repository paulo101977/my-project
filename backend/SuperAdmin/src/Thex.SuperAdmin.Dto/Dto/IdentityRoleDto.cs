﻿using System;
using System.Collections.Generic;
using Tnf.Dto;

namespace Thex.SuperAdmin.Dto
{
    public partial class RolesDto : BaseDto
    {
        public static RolesDto NullInstance = null;

        public Guid Id { get; set; }
        public string Name { get; set; }
        public string NormalizedName { get; set; }
        public string ConcurrencyStamp { get; set; }
        public bool IsActive { get; set; }

        public int IdentityProductId { get; set; }
        public string IdentityProductName { get; set; }

        public List<IdentityRolePermissionDto> RolePermissionList { get; set; }
        public List<SimpleTranslationDto> TranslationList { get; set; }
    }
}
