﻿using System;
using System.Collections.Generic;
using Tnf.Dto;

namespace Thex.SuperAdmin.Dto
{
    public partial class StatusDto : BaseDto
    {
        public int Id { get; set; }
        public static StatusDto NullInstance = null;

        public StatusDto()
        {
        }

        public int StatusCategoryId { get; set; }
        public string Name { get; set; }

        public virtual StatusCategoryDto StatusCategory { get; set; }
    }
}
