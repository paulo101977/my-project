﻿using System;
using System.Collections.Generic;
using Tnf.Dto;

namespace Thex.SuperAdmin.Dto
{
    public partial class PropertyTypeDto : BaseDto
    {
        public int Id { get; set; }
        public static PropertyTypeDto NullInstance = null;

        public PropertyTypeDto()
        {
            PropertyList = new HashSet<PropertyDto>();
        }

        public string Name { get; set; }

        public virtual ICollection<PropertyDto> PropertyList { get; set; }
    }
}
