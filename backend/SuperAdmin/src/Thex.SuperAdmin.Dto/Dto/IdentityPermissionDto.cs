﻿using System;
using System.Collections.Generic;
using Tnf.Dto;

namespace Thex.SuperAdmin.Dto
{
    public partial class IdentityPermissionDto : BaseDto
    {
        public static IdentityPermissionDto NullInstance = null;

        public Guid Id { get; set; }
        public int IdentityProductId { get; set; }
        public int IdentityModuleId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }

        public MenuItemDto ParentMenuList { get; set; }

        public virtual IList<IdentityPermissionFeatureDto> IdentityPermissionFeatureList { get; set; }

        public string IdentityProductName { get; set; }
        public string IdentityModuleName { get; set; }

        public string Role { get; set; }
        public Guid RoleId { get; set; }

    }
}
