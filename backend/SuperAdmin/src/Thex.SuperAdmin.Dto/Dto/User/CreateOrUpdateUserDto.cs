﻿using System;
using System.Collections.Generic;
using Tnf.Dto;

namespace Thex.SuperAdmin.Dto.Dto
{
    public class CreateOrUpdateUserDto : BaseDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string UserName { get; set; }
        public bool IsActive { get; set; }
        public string Email { get; set; }
        public int? ChainId { get; set; }
        public string PreferredLanguage { get; set; }
        public bool IsAdmin { get; set; }

        public ICollection<BrandDto> BrandList { get; set; }
        public ICollection<PropertyDto> PropertyList { get; set; }

        public CreateOrUpdateUserDto()
        {
            BrandList = new HashSet<BrandDto>();
            PropertyList = new HashSet<PropertyDto>();
        }
    }
}
