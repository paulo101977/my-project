﻿using System;

namespace Thex.SuperAdmin.Dto.Dto
{
    public class AuthenticateIntegrationPartnerDto
    {
        public string TokenClient { get; set; }
        public string TokenApplication { get; set; }
        public int? PropertyId { get; set; }
        public int? ChainId { get; set; }
        public int? BrandId { get; set; }
        public Guid? TenantId { get; set; }
    }
}
