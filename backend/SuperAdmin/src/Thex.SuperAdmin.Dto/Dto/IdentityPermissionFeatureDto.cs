﻿using System;
using System.Collections.Generic;
using Tnf.Dto;

namespace Thex.SuperAdmin.Dto
{
    public partial class IdentityPermissionFeatureDto : BaseDto
    {
        public static IdentityPermissionFeatureDto NullInstance = null;

        public Guid Id { get; set; }
        public Guid IdentityPermissionId { get; set; }
        public Guid IdentityFeatureId { get; set; }
        public bool IsActive { get; set; }
        //public bool IsDeleted { get; set; }
        //public DateTime CreationTime { get; set; }
        //public Guid? CreatorUserId { get; set; }
        //public DateTime? LastModificationTime { get; set; }
        //public Guid? LastModifierUserId { get; set; }
        //public DateTime? DeletionTime { get; set; }
        //public Guid? DeleterUserId { get; set; }

        public virtual IdentityFeatureDto IdentityFeature { get; set; }
        //public virtual IdentityPermissionDto IdentityPermission { get; set; }
    }
}
