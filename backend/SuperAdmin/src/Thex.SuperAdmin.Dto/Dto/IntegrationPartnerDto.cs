﻿using Tnf.Dto;

namespace Thex.Dto
{
    public partial class IntegrationPartnerDto : BaseDto
    {
        public static IntegrationPartnerDto NullInstance = null;

        public int IntegrationPartnerId { get; set; }
        public string TokenClient { get; set; }
        public string TokenApplication { get; set; }
        public string IntegrationPartnerName { get; set; }
        public int IntegrationPartnerType { get; set; }
        public bool IsActive { get; set; }
    }
}
