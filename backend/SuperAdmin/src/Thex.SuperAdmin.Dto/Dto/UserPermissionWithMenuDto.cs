﻿using System;
using System.Collections.Generic;
using Tnf.Dto;

namespace Thex.SuperAdmin.Dto
{
    public partial class UserPermissionWithMenuDto : BaseDto
    {
        public List<string> KeyList { get; set; }
        public List<MenuItemSimpleDto> MenuList { get; set; }
    }
}
