﻿using System;
using System.Collections.Generic;
using Tnf.Dto;

namespace Thex.SuperAdmin.Dto
{
    public partial class StatusCategoryDto : BaseDto
    {
        public int Id { get; set; }
        public static StatusCategoryDto NullInstance = null;

        public StatusCategoryDto()
        {
            StatusList = new HashSet<StatusDto>();
        }

        public string Name { get; set; }

        public virtual ICollection<StatusDto> StatusList { get; set; }
    }
}
