﻿using System;
using Tnf.Dto;

namespace Thex.SuperAdmin.Dto
{
    public class PropertyStatusHistoryDto : BaseDto
    {
        public Guid Id { get; set; }
        public DateTime CreationDate { get; set; }
        public int PropertyStatusId { get; set; }
        public string PropertyStatusName { get; set; }
        public int PropertyId { get; set; }
    }
}
