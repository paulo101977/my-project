﻿using System;
using Tnf.Dto;

namespace Thex.SuperAdmin.Dto
{
    public class PropertyIdAndTenantDto : BaseDto
    {
        public int Id { get; set; }
        public Guid TenantId { get; set; }
    }
}
