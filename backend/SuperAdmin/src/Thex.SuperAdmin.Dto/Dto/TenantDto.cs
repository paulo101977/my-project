﻿using System;
using System.Collections.Generic;
using Tnf.Dto;

namespace Thex.SuperAdmin.Dto
{
    public partial class TenantDto : BaseDto
    {
        public Guid Id { get; set; }
        public static TenantDto NullInstance = null;

        public TenantDto()
        {
            InverseParentList = new HashSet<TenantDto>();
            UserList = new HashSet<UserDto>();
        }

        public int? ChainId { get; set; }
        public string ChainName { get; set; }
        public int BrandId { get; set; }
        public string BrandName { get; set; }
        public string PropertyName { get; set; }
        public int? PropertyId { get; set; }
        public string TenantName { get; set; }
        public Guid? ParentId { get; set; }
        public bool IsActive { get; set; }

        public virtual TenantDto Parent { get; set; }
        public virtual ICollection<TenantDto> InverseParentList { get; set; }
        public virtual ICollection<UserDto> UserList { get; set; }
    }
}
