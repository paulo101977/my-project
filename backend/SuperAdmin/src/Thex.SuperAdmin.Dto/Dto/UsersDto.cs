﻿using System;
using System.Collections.Generic;
using Tnf.Dto;

namespace Thex.SuperAdmin.Dto.Dto
{
    public class UsersDto : BaseDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public int ChainId { get; set; }
        public string ChainName { get; set; }
        public int PropertyId { get; set; }
        public string PropertyName { get; set; }
        public int CompanyId { get; set; }
        public string CompanyName { get; set; }
        public Guid TenantId { get; set; }
        public string TenantName { get; set; }
        public bool IsActive { get; set; }
        public string PreferredLanguage { get; set; }
        public string PreferredCulture { get; set; }

        public ICollection<BrandDto> BrandList { get; set; }
        public ICollection<PropertyDto> PropertyList { get; set; }

        public UsersDto()
        {
            BrandList = new HashSet<BrandDto>();
            PropertyList = new HashSet<PropertyDto>();
        }
    }
}
