﻿using System.Collections.Generic;
using Tnf.Dto;

namespace Thex.SuperAdmin.Dto.Dto
{
    public class ThexListDto<IDto> : ListDto<IDto>
    {
        public int TotalItems { get; set; }
    }
}
