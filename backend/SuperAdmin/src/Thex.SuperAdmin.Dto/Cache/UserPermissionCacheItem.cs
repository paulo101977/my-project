﻿using System;
using System.Collections.Generic;

namespace Thex.SuperAdmin.Dto.Cache
{
    [Serializable]
    public class UserPermissionCacheItem
    {
        public const string CacheStoreName = "UserRolePermission";
        
        public const int ExpireInMinutes = 10;

        public Guid UserId { get; set; }

        public List<string> PermissionKeyList { get; set; }

        public UserPermissionCacheItem()
        {
            PermissionKeyList = new List<string>();
        }

        public UserPermissionCacheItem(Guid userId)
            : this()
        {
            UserId = userId;
        }

        public UserPermissionCacheItem(Guid userId, List<string> PermissionKeyList)
            : this()
        {
            UserId = userId;

            if (PermissionKeyList != null && PermissionKeyList.Count > 0)
                this.PermissionKeyList.AddRange(PermissionKeyList);
        }

        public static string BuildKey(Guid userId, Guid tenantId)
           => $"{CacheStoreName}-{tenantId}-{userId}".ToUpper();
    }
}
