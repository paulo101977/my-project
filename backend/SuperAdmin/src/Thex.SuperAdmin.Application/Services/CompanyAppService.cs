﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Thex.Kernel;
using Thex.Common;
using Thex.Common.Dto;
using Thex.Common.Enumerations;
using Thex.Location.Application.Interfaces;
using Thex.SuperAdmin.Application.Adapters;
using Thex.SuperAdmin.Application.Interfaces;
using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Dto.Dto;
using Thex.SuperAdmin.Infra.Entities;
using Thex.SuperAdmin.Infra.Interfaces;
using Thex.SuperAdmin.Infra.Interfaces.ReadRepositories;
using Tnf.Notifications;

namespace Thex.SuperAdmin.Application.Services
{
    public class CompanyAppService : ApplicationServiceBase, ICompanyAppService
    {
        private readonly ICompanyReadRepository _companyReadRepository;
        private readonly IDocumentReadRepository _documentReadRepository;
        private readonly IPropertyReadRepository _propertyReadRepository;
        private readonly ILocationAppService _locationAppService;
        private readonly IApplicationUser _applicationUser;
        private readonly IDocumentAdapter _documentAdapter;
        private readonly ICompanyRepository _companyRepository;
        private readonly ICompanyAdapter _companyAdapter;
        private readonly IPersonAdapter _personAdapter;
        private readonly IPersonRepository _personRepository;
        private readonly IDocumentRepository _documentRepository;
        private readonly IContactInformationRepository _contactInformationRepository;
        private readonly IContactInformationAdapter _contactInformationAdapter;

        public CompanyAppService(
           ICompanyReadRepository companyReadRepository,
           ICompanyRepository companyRepository,
           IPersonRepository personRepository,
           ICompanyAdapter companyAdapter,
           IPersonAdapter personAdapter,
           IDocumentReadRepository documentReadRepository,
           IPropertyReadRepository propertyReadRepository,
           IDocumentAdapter documentAdapter,
           IDocumentRepository documentRepository,
           ILocationAppService locationAppService,
           IApplicationUser applicationUser,
           IContactInformationRepository contactInformationRepository,
           IContactInformationAdapter contactInformationAdapter,
           INotificationHandler notificationHandler,
           ISimpleUnitOfWork simpleUnitOfWork)
            :base(simpleUnitOfWork, notificationHandler)
        {
            _companyReadRepository = companyReadRepository;
            _documentReadRepository = documentReadRepository;
            _propertyReadRepository = propertyReadRepository;
            _locationAppService = locationAppService;
            _applicationUser = applicationUser;
            _documentAdapter = documentAdapter;
            _companyRepository = companyRepository;
            _companyAdapter = companyAdapter;
            _personAdapter = personAdapter;
            _personRepository = personRepository;
            _documentRepository = documentRepository;
            _contactInformationRepository = contactInformationRepository;
            _contactInformationAdapter = contactInformationAdapter;
        }

        public async Task<ThexListDto<CompanyDto>> GetAllByFilterAsync(GetAllCompanyDto request)
        {
            var companyDtoList = await _companyReadRepository.GetAllByFilterAsync(request);
            return companyDtoList;
        }

        public async Task<CompanyDto> GetAsync(int id)
        {
            var companyDto = await _companyReadRepository.GetCompanyDtoByIdAsync(id);

            if(companyDto == null)
            {
                NotifyNullParameter();
                return null;
            }

            if (companyDto != null)
            {
                companyDto.DocumentList =
                    _documentReadRepository.GetAllDtoByOwnerId(companyDto.CompanyUid);
                companyDto.LocationList =
                    _locationAppService.GetAllByOwnerId(companyDto.CompanyUid, _applicationUser.PreferredLanguage);
                companyDto.PropertyList = _propertyReadRepository.GetPropertiesAssociateByCompanyId(id);
            }

            return companyDto;
        }

        public async Task<ThexListDto<LocationDto>> GetLocationsFromCompanyAsync(int id)
        {
            var companyDto = await _companyReadRepository.GetCompanyDtoByIdAsync(id);
            if(companyDto == null)
            {
                NotifyParameterInvalid();
                return null;
            }

            var locationList = _locationAppService.GetAllByOwnerId(companyDto.CompanyUid, _applicationUser.PreferredLanguage);
            return new ThexListDto<LocationDto>()
            {
                HasNext = false,
                Items = locationList
            };
        }

        public async Task<CompanyDto> CreateAsync(CompanyDto dto)
        {
            if(dto == null)
            {
                NotifyNullParameter();
                return null;
            }
            dto.PersonType = 'L';
            ValidateCompany(dto);
            if (Notification.HasNotification())
                return null;

            using (var trans = _companyReadRepository.Context.Database.BeginTransaction())
            {
                var companyPerson = SetFirstAndLastNameBasedOnFullName(dto.TradeName);
                Guid personId = await this.CreatePersonAndGetId(companyPerson.FirstName, companyPerson.LastName, companyPerson.FullName, (PersonTypeEnum)dto.PersonType);

                if (Notification.HasNotification()) return null;

                dto.PersonId = personId;

                var company = _companyAdapter.Map(dto)
                                .WithCompanyUid(Guid.NewGuid())
                                .Build();

                if (Notification.HasNotification()) return null;

                dto.Id = (await _companyRepository.InsertAndSaveChangesAsync(company)).Id;

                IList<ContactInformation> contactInformationList = GetContactInformationListFromDto(personId, dto.Email, dto.HomePage);

                if (Notification.HasNotification()) return null;

                await SaveContactInformationList(contactInformationList);

                if (Notification.HasNotification()) return null;

                await SaveDocumentList(dto.DocumentList, company.CompanyUid);

                if (Notification.HasNotification()) return null;

                _locationAppService.CreateLocationList(dto.LocationList, company.CompanyUid);

                trans.Commit();
            }

            return dto;
        }

        public async Task<CompanyDto> UpdateAsync(int id, CompanyDto dto)
        {
            ValidateDtoAndId(dto, id, nameof(dto), nameof(id));
            if (Notification.HasNotification())
                return CompanyDto.NullInstance;

            dto.PersonType = 'L';
            ValidateCompany(dto);
            if (Notification.HasNotification())
                return CompanyDto.NullInstance;

            using (var trans = _companyReadRepository.Context.Database.BeginTransaction())
            {
                var dbCompany = await _companyReadRepository.GetCompanyByIdAsync(dto.Id);
                if (dbCompany == null)
                {
                    NotifyParameterInvalid();
                    return CompanyDto.NullInstance;
                }

                dto.Id = id;

                var companyPerson = SetFirstAndLastNameBasedOnFullName(dto.TradeName);

                PersonDto personClientDto = new PersonDto
                {
                    Id = dto.PersonId,
                    FirstName = companyPerson.FirstName,
                    LastName = companyPerson.LastName,
                    FullName = companyPerson.FullName,
                    PersonType = (dto.PersonType).ToString()
                };

                if (Notification.HasNotification())
                    return CompanyDto.NullInstance;

                ICollection<ContactInformation> contactInformationList = GetContactInformationListFromDto(
                  personId: personClientDto.Id,
                  email: dto.Email,
                  homePage: dto.HomePage);

                await UpdateDocumentList(dto.DocumentList, dbCompany.CompanyUid);

                if (Notification.HasNotification())
                    return CompanyDto.NullInstance;

                Person personClient = this._personAdapter.Map(personClientDto)
                    .WithContactInformationList(contactInformationList)
                    .Build();

                var company = _companyAdapter.Map(dto)
                   .WithPerson(personClient)
                   .WithCompanyUid(dbCompany.CompanyUid)
                   .Build();

                if (Notification.HasNotification()) return null;

                await _companyRepository.UpdateAndSaveChangesAsync(_companyAdapter.Map(company, dto).Build());

                UpdateLocation(dto);

                trans.Commit();
            }

            return dto;
        }

        private void UpdateLocation(CompanyDto dto)
        {
            if (dto.LocationList != null)
            {
                var locationDtoList = _locationAppService.SetCountrySubdivision(dto.LocationList);
                
                _locationAppService.UpdateLocationList(locationDtoList.ToList(), dto.CompanyUid.ToString());
            }
        }

        private async Task UpdateDocumentList(ICollection<DocumentDto> documentDtoList, Guid ownerId)
        {
            ValidateDocumentDuplicated(documentDtoList);

            if (Notification.HasNotification()) return;

            var documentListToInsert = new List<Document>();
            var documentListToUpdate = new List<Document>();
            var documentList = _documentReadRepository.GetAllByOwnerId(ownerId);
            var documentListToRemove = documentList
                                                    .Where(d => !documentDtoList
                                                    .Select(dd => dd.DocumentTypeId)
                                                    .Contains(d.DocumentTypeId))
                                                    .ToList();

            foreach (var documentDto in documentDtoList)
            {
                if (documentDto.OwnerId == Guid.Empty || !documentList.Any(d => d.DocumentTypeId == documentDto.DocumentTypeId))
                    documentListToInsert.Add(_documentAdapter.Map(documentDto)
                                        .WithOwnerId(ownerId)
                                        .Build());
                else
                {
                    var document = documentList.FirstOrDefault(d => d.DocumentTypeId == documentDto.DocumentTypeId);
                    documentListToUpdate.Add(_documentAdapter.Map(document, documentDto).Build());
                }

            }

            if (Notification.HasNotification())
                return;

            if (documentListToInsert.Any())
                await _documentRepository.AddRangeAsync(documentListToInsert);

            if (documentListToUpdate.Any())
                await _documentRepository.UpdateRangeAsync(documentListToUpdate);

            if (documentListToRemove.Any())
                await _documentRepository.RemoveRangeAsync(documentListToRemove);

        }

        #region Validations
        private void ValidateCompany(CompanyDto dto)
        {
            ValidateBasicDtoProperties(dto);
            if (Notification.HasNotification())
                return;

            DocumentListIsValid(dto);
            if (Notification.HasNotification())
                return;

            LocationListTypesIsValid(dto.LocationList, dto.PersonType);
            if (Notification.HasNotification())
                return;

            LocationListQuantityIsValid(dto.LocationList);
            if (Notification.HasNotification())
                return;
          
            // valida se existe mais de um documento com o mesmo tipo
            ValidateDocumentDuplicated(dto.DocumentList);
        }

        private void ValidateDocumentDuplicated(ICollection<DocumentDto> documentList)
        {
            var hasDuplicates = documentList
                 .Select(exp => exp.DocumentTypeId)
                 .GroupBy(exp => exp)
                 .Any(exp => exp.Count() > 1);

            if (hasDuplicates)
            {
                Notification.Raise(Notification.DefaultBuilder
                .WithMessage(AppConsts.LocalizationSourceName, Document.EntityError.DocumentListHasDuplicates)
                .WithDetailedMessage(AppConsts.LocalizationSourceName, Document.EntityError.DocumentListHasDuplicates)
                .Build());
            }
        }

        private void ValidateBasicDtoProperties(CompanyDto company)
        {
            var validTypes = new List<PersonTypeEnum>
            {
                PersonTypeEnum.Natural,
                PersonTypeEnum.Legal
            };

            if (!validTypes.Contains((PersonTypeEnum)company.PersonType))
            {
                Notification.Raise(Notification.DefaultBuilder
               .WithMessage(AppConsts.LocalizationSourceName, Company.EntityError.CompanyHasInvalidPersonType)
               .WithDetailedMessage(AppConsts.LocalizationSourceName, Company.EntityError.CompanyHasInvalidPersonType)
               .Build());
            }

            if (string.IsNullOrEmpty(company.Email))
            {
                Notification.Raise(Notification.DefaultBuilder
               .WithMessage(AppConsts.LocalizationSourceName, Company.EntityError.CompanyMustHaveEmail)
               .WithDetailedMessage(AppConsts.LocalizationSourceName, Company.EntityError.CompanyMustHaveEmail)
               .Build());
            }
        }
        #endregion

        #region Document private methods
        private void DocumentListIsValid(CompanyDto company)
        {
            var documentDtoList = company.DocumentList;

            if ((PersonTypeEnum)company.PersonType == PersonTypeEnum.Natural && !PersonHasMainDocument(documentDtoList, DocumentTypeEnum.BrNaturalPersonCPF))
                Notification.Raise(Notification.DefaultBuilder
              .WithMessage(AppConsts.LocalizationSourceName, Company.EntityError.PersonNaturalDoesNotHaveMainDocument)
              .WithDetailedMessage(AppConsts.LocalizationSourceName, Company.EntityError.PersonNaturalDoesNotHaveMainDocument)
              .Build());
            else if ((PersonTypeEnum)company.PersonType == PersonTypeEnum.Legal && !PersonHasMainDocument(documentDtoList, DocumentTypeEnum.BrLegalPersonCNPJ))
                Notification.Raise(Notification.DefaultBuilder
              .WithMessage(AppConsts.LocalizationSourceName, Company.EntityError.PersonLegalDoesNotHaveMainDocument)
              .WithDetailedMessage(AppConsts.LocalizationSourceName, Company.EntityError.PersonLegalDoesNotHaveMainDocument)
              .Build());
        }

        private bool PersonHasMainDocument(ICollection<DocumentDto> documentDtoList, DocumentTypeEnum t)
        {
            return documentDtoList != null && documentDtoList.Any(d => d.DocumentTypeId == Convert.ToInt32(t));
        }

        private async Task SaveDocumentList(ICollection<DocumentDto> documentListDto, Guid ownerId)
        {
            List<Document> documentList = new List<Document>();
            foreach (var documentDto in documentListDto)
            {
                documentDto.OwnerId = ownerId;
                documentList.Add(_documentAdapter.Map(documentDto).Build());
            }

            await _documentRepository.AddRangeAsync(documentList);
        }

        private PersonDto SetFirstAndLastNameBasedOnFullName(string FullName)
        {

            var personDto = new PersonDto();

            if (!string.IsNullOrEmpty(FullName))
            {

                personDto.FullName = FullName;

                var separator = " ";

                var FullNameList = FullName.Split(separator).ToList();


                personDto.FirstName = FullNameList.First();


                if (FullNameList.Count() > 1)
                {
                    FullNameList.RemoveAt(0);

                    personDto.LastName = FullNameList.JoinAsString(separator);
                }
            }


            return personDto;
        }

        private async Task<Guid> CreatePersonAndGetId(string firstName, string lastName, string fullName, PersonTypeEnum personType)
        {
            PersonDto personDto = new PersonDto()
            {
                FirstName = firstName,
                LastName = lastName,
                FullName = fullName,
                PersonType = ((char)personType).ToString()
            };

            var personBuilder = _personAdapter.Map(personDto);
            Guid personId = _personRepository.Insert(personBuilder.Build()).Id;

            return personId;
        }
        #endregion

        #region ContactInformation private methods
        private async Task SaveContactInformationList(IList<ContactInformation> contactInformationList)
        {
            if (contactInformationList.Any())
            {
                await _contactInformationRepository.AddRangeAndSaveChangesAsync(contactInformationList);
            }
        }

        private IList<ContactInformation> GetContactInformationListFromDto(Guid personId, string email = "", string homePage = "",
          string phoneNumber = "", string cellPhoneNumber = "")
        {
            List<ContactInformation> contactInformationList = new List<ContactInformation>();

            if (!String.IsNullOrEmpty(email))
            {
                contactInformationList.Add(GetContactInformationDto(ContactInformationTypeEnum.Email, email, personId).Build());
            }

            if (!String.IsNullOrEmpty(homePage))
            {
                contactInformationList.Add(GetContactInformationDto(ContactInformationTypeEnum.Website, homePage, personId).Build());
            }

            if (!String.IsNullOrEmpty(phoneNumber))
            {
                contactInformationList.Add(GetContactInformationDto(ContactInformationTypeEnum.PhoneNumber, phoneNumber, personId).Build());
            }

            if (!String.IsNullOrEmpty(cellPhoneNumber))
            {
                contactInformationList.Add(GetContactInformationDto(ContactInformationTypeEnum.CellPhoneNumber, cellPhoneNumber, personId).Build());
            }

            return contactInformationList;
        }

        private ContactInformation.Builder GetContactInformationDto(ContactInformationTypeEnum contactInformationTypeEnum, string contactInformation, Guid personId)
        {
            ContactInformationDto contactInformationDto = new ContactInformationDto();
            contactInformationDto.OwnerId = personId;
            contactInformationDto.Information = contactInformation;

            switch (contactInformationTypeEnum)
            {
                case ContactInformationTypeEnum.Email:
                    contactInformationDto.ContactInformationTypeId = (int)ContactInformationTypeEnum.Email;
                    break;
                case ContactInformationTypeEnum.PhoneNumber:
                    contactInformationDto.ContactInformationTypeId = (int)ContactInformationTypeEnum.PhoneNumber;
                    break;
                case ContactInformationTypeEnum.Website:
                    contactInformationDto.ContactInformationTypeId = (int)ContactInformationTypeEnum.Website;
                    break;
                case ContactInformationTypeEnum.CellPhoneNumber:
                    contactInformationDto.ContactInformationTypeId = (int)ContactInformationTypeEnum.CellPhoneNumber;
                    break;
            }
            return _contactInformationAdapter.Map(contactInformationDto);
        }
        #endregion

        #region Location private methods
        private void LocationListQuantityIsValid(ICollection<LocationDto> locationList)
        {
            var maxLocations = 5;

            if (locationList.Count > maxLocations)
            {
                Notification.Raise(Notification.DefaultBuilder
              .WithMessage(AppConsts.LocalizationSourceName, Company.EntityError.CompanyLocationListGreaterThanAllowed)
              .WithDetailedMessage(AppConsts.LocalizationSourceName, Company.EntityError.CompanyLocationListGreaterThanAllowed)
              .Build());
            }
        }

        private void LocationListTypesIsValid(ICollection<LocationDto> locationList, char personType)
        {
            if (locationList == null)
            {
                Notification.Raise(Notification.DefaultBuilder
                       .WithMessage(AppConsts.LocalizationSourceName, Company.EntityError.CompanyMustHaveCommercialLocation)
                       .WithDetailedMessage(AppConsts.LocalizationSourceName, Company.EntityError.CompanyMustHaveCommercialLocation)
                       .Build());

                return;
            }

            var hasDuplicates = locationList
                .Select(exp => exp.LocationCategoryId)
                .GroupBy(exp => exp)
                .Any(exp => exp.Count() > 1);

            if (hasDuplicates)
            {
                Notification.Raise(Notification.DefaultBuilder
                    .WithMessage(AppConsts.LocalizationSourceName, Company.EntityError.CompanyLocationListHasDuplicates)
                    .WithDetailedMessage(AppConsts.LocalizationSourceName, Company.EntityError.CompanyLocationListHasDuplicates)
                    .Build());
            }
            else
            {
                var hasCoomercialType = locationList.Any(l => l.LocationCategoryId == (int)LocationCategoryEnum.Commercial);

                if (!hasCoomercialType && personType == (char)PersonTypeEnum.Legal)
                {
                    Notification.Raise(Notification.DefaultBuilder
                       .WithMessage(AppConsts.LocalizationSourceName, Company.EntityError.CompanyMustHaveCommercialLocation)
                       .WithDetailedMessage(AppConsts.LocalizationSourceName, Company.EntityError.CompanyMustHaveCommercialLocation)
                       .Build());
                }
            }
        }
        #endregion
    }
}
