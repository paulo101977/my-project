﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Thex.Common;
using Thex.Dto;
using Thex.SuperAdmin.Application.Adapters;
using Thex.SuperAdmin.Application.Interfaces;
using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Dto.Dto;
using Thex.SuperAdmin.Dto.Enumerations;
using Thex.SuperAdmin.Infra.Entities;
using Thex.SuperAdmin.Infra.Interfaces;
using Thex.SuperAdmin.Infra.Interfaces.ReadRepositories;
using Thex.SuperAdmin.Infra.Interfaces.Services;
using Tnf.Dto;
using Tnf.Notifications;
using Tnf.Specifications;

namespace Thex.SuperAdmin.Application.Services
{
    public class MenuAppService : ApplicationServiceBase, IMenuAppService
    {
        private readonly IIdentityMenuFeatureAdapter _menuAdapter;
        private readonly IIdentityMenuFeatureRepository _menuRepository;
        private readonly IIdentityMenuFeatureReadRepository _menuReadRepository;
        private readonly IMenuDomainService _menuDomainService;
        private Expression<Func<IdentityMenuFeatureDto, bool>> filters = (e => true);

        public MenuAppService(
           IIdentityMenuFeatureAdapter menuAdapter,
           IIdentityMenuFeatureRepository menuRepository,
           IIdentityMenuFeatureReadRepository menuReadRepository,
           IMenuDomainService menuDomainService,
           INotificationHandler notificationHandler,
           ISimpleUnitOfWork simpleUnitOfWork)
           : base(simpleUnitOfWork, notificationHandler)
        {
            _menuAdapter = menuAdapter;
            _menuRepository = menuRepository;
            _menuReadRepository = menuReadRepository;
            _menuDomainService = menuDomainService;
        }

        public async Task<ListDto<MenuItemDto>> GetHierarchyAsync(GetAllIdentityMenuFeatureDto dto)
        {
            var menuList = await _menuReadRepository.GetAllWithFiltersAsync(GetFilterExpression(dto));

            if (menuList == null || menuList.Count == 0)
                return null;

            return await _menuDomainService.GetMenuItemHierarchy(menuList);
        }

        public async Task<ListDto<MenuItemDto>> GetHierarchyByPermissionListAsync(GetAllIdentityMenuFeatureDto dto, List<string> permissionKeyList)
        {
            if (permissionKeyList == null || permissionKeyList.Count == 0)
                return null;

            var menuList = await _menuReadRepository.GetAllWithFiltersAsync(GetFilterExpression(dto));

            if (menuList == null || menuList.Count == 0)
                return null;

            permissionKeyList.AddRange(await _menuDomainService.GetParentKeyList(menuList, permissionKeyList));

            menuList = menuList.Where(e => permissionKeyList.Contains(e.IdentityFeature.Key)).ToList();

            return await _menuDomainService.GetMenuItemHierarchy(menuList);
        }

        private Expression<Func<IdentityMenuFeatureDto, bool>> GetFilterExpression(GetAllIdentityMenuFeatureDto dto)
        {
            if (dto.ProductId.HasValue)
                filters = filters.And(e => e.IdentityProductId == dto.ProductId.Value);
            if (dto.ModuleId.HasValue)
                filters = filters.And(e => e.IdentityModuleId == dto.ModuleId.Value);

            return filters;
        }

        public async Task<IdentityMenuFeatureDto> GetAsync(Guid id)
        {
            var menuFeature = await _menuReadRepository.GetDtoAsync(id);

            if (menuFeature == null)
                return null;

            menuFeature.ParentMenuList = await _menuReadRepository.GetMenuItemWithParents(menuFeature.IdentityFeatureId);

            return menuFeature;
        }

        public async Task<IdentityMenuFeatureDto> CreateAsync(IdentityMenuFeatureDto dto)
        {
            ValidateDto(dto, nameof(dto));

            if (!Enum.IsDefined(typeof(IdentityProductEnum), dto.IdentityProductId) ||
                !Enum.IsDefined(typeof(IdentityModuleEnum), dto.IdentityModuleId))
                NotifyParameterInvalid();

            if (Notification.HasNotification())
                return IdentityMenuFeatureDto.NullInstance;

            //não pode linkar a feature com ela mesma
            //tem que pegar o modulo do pai
            //tem que ser uma feature publica
            //tem que ser uma feature que não tenha sido utilizada em outro menu

            var menuFeature = _menuAdapter.Map(dto).Build();

            if (Notification.HasNotification())
                return null;

            using (var trans = _menuReadRepository.Context.Database.BeginTransaction())
            {
                dto.Id = (await _menuRepository.InsertAndSaveChangesAsync(menuFeature)).Id;

                trans.Commit();
            }

            return dto;
        }

        public async Task<IdentityMenuFeatureDto> UpdateAsync(Guid id, IdentityMenuFeatureDto dto)
        {
            if (!Enum.IsDefined(typeof(IdentityProductEnum), dto.IdentityProductId) ||
                !Enum.IsDefined(typeof(IdentityModuleEnum), dto.IdentityModuleId))
                NotifyParameterInvalid();

            if (Notification.HasNotification())
                return null;

            dto.Id = id;

            var entity = await _menuReadRepository.GetById(dto.Id);

            if (entity == null)
            {
                NotifyNullParameter();
                return null;
            }

            //se o modulo do pai for diferente 
            //ou se o modulo foi alterado 
            //atualizar todos os filhos

            var menu = _menuAdapter.Map(entity, dto).Build();

            if (Notification.HasNotification())
                return null;

            using (var trans = _menuReadRepository.Context.Database.BeginTransaction())
            {
                await _menuRepository.UpdateAndSaveChangesAsync(menu);

                trans.Commit();
            }
            return dto;
        }

        public void ToggleActivation(Guid id)
        {
            if (id == Guid.Empty)
            {
                NotifyIdIsMissing();
                return;
            }

            _menuRepository.ToggleAndSaveIsActive(id);

            Commit();
        }

        public async Task<ListDto<MenuItemDto>> GetHierarchyByProductPropertyPermissionListAsync(GetAllIdentityMenuFeatureDto dto, List<string> permissionKeyList)
        {
            if (permissionKeyList == null || permissionKeyList.Count == 0)
                return null;

            var menuList = await _menuReadRepository.GetAllWithFiltersAsync(GetFilterExpression(dto));

            if (menuList == null || menuList.Count == 0)
                return null;

            var parentKeyList = (await _menuDomainService.GetParentKeyList(menuList, permissionKeyList));

            menuList = menuList.Where(e => parentKeyList.Contains(e.IdentityFeature.Key)).ToList();

            return await _menuDomainService.GetMenuItemHierarchy(menuList);
        }
    }
}
