﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Thex.SuperAdmin.Application.Adapters;
using Thex.SuperAdmin.Application.Interfaces;
using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Infra.Entities;
using Thex.SuperAdmin.Infra.Interfaces;
using Thex.SuperAdmin.Infra.Interfaces.ReadRepositories;
using Tnf.Notifications;

namespace Thex.SuperAdmin.Application.Services
{
    public class UserRoleAppService : ApplicationServiceBase, IUserRoleAppService
    {
        private readonly IRolesReadRepository _roleReadRepository;
        private readonly IUserRolesRepository _userRolesRepository;
        private readonly IUserRoleAdapter _userRoleAdapter;

        public UserRoleAppService(
           IRolesReadRepository roleReadRepository,
           IUserRolesRepository userRolesRepository,
           IUserRoleAdapter userRoleAdapter,
           INotificationHandler notificationHandler,
           ISimpleUnitOfWork simpleUnitOfWork)
           : base(simpleUnitOfWork, notificationHandler)
        {
            _roleReadRepository = roleReadRepository;
            _userRolesRepository = userRolesRepository;
            _userRoleAdapter = userRoleAdapter;
        }

        public async Task CreateorUpdateAsync(Guid userId, List<Guid> roleIdList, int? propertyId = null, int? brandId = null, int? chainId = null)
        {
            if (userId == Guid.Empty || (roleIdList.Count > 0 && await _roleReadRepository.Count(roleIdList) != roleIdList.Count))
                NotifyParameterInvalid();

            if (Notification.HasNotification())
                return;

            var userRoleIdList = roleIdList.Distinct().Select(e => new UserRoleDto { RoleId = e }).ToList();

            await _userRolesRepository.CreateOrUpdateUserRoleAsync(userId, userRoleIdList, propertyId, brandId, chainId);
        }

        public async Task CreateAsync(Guid userId, Guid roleId, List<int> propertyIdListDto)
        {
            if (userId == Guid.Empty || propertyIdListDto.Count == 0)
                NotifyParameterInvalid();

            if (Notification.HasNotification())
                return;

            var userRoleList = CreateMap(userId, roleId, propertyIdListDto);

            await _userRolesRepository.AddUserRoleRangeAsync(userRoleList);
        }

        private List<UserRole> CreateMap(Guid userId, Guid roleId, List<int> propertyIdListDto)
        {
            var result = new List<UserRole>();
            foreach (var propertyId in propertyIdListDto)
            {
                var builder = _userRoleAdapter.Map(new UserRoleDto
                {
                    PropertyId = propertyId,
                    RoleId = roleId,
                    UserId = userId
                });
                var entity = builder.Build();

                if (Notification.HasNotification())
                    return null;

                result.Add(entity);
            }

            return result;
        }

    }
}
