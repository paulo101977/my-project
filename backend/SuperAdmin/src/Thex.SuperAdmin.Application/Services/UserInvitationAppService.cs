﻿using System;
using Thex.Common.Enumerations;
using System.Threading.Tasks;
using Thex.Common;
using Microsoft.Extensions.Configuration;
using Tnf.Dto;
using Tnf.Notifications;
using Tnf.Localization;
using Thex.SuperAdmin.Application.Interfaces;
using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Dto.Email;
using Thex.SuperAdmin.Infra.Factories;
using Thex.SuperAdmin.Application.Adapters;
using Thex.SuperAdmin.Infra.Interfaces.ReadRepositories;
using Thex.SuperAdmin.Infra.Interfaces;
using Thex.AspNetCore.Security.Interfaces;
using Thex.Common.Emails.AwsSes;
using Thex.Common.Message;
using System.Collections.Generic;
using Thex.SuperAdmin.Dto.Dto;
using Thex.SuperAdmin.Infra.Entities;

namespace Thex.SuperAdmin.Application.Services
{
    public class UserInvitationAppService : ApplicationServiceBase, IUserInvitationAppService
    {
        private readonly ITokenManager _tokenManager;
        private readonly IConfiguration _configuration;
        private readonly ILocalizationManager _localizationManager;
        private readonly IEmailFactory _emailFactory;
        private readonly IUserInvitationAdapter _userInvitationAdapter;
        private readonly IUserInvitationReadRepository _userInvitationReadRepository;
        private readonly IUserInvitationRepository _userInvitationRepository;

        public UserInvitationAppService(
            IUserInvitationAdapter userInvitationAdapter,
            ITokenManager tokenManager,
            IConfiguration configuration,
            ILocalizationManager localizationManager,
            IEmailFactory emailFactory,
            IUserInvitationReadRepository userInvitationReadRepository,
            IUserInvitationRepository userInvitationRepository,
            INotificationHandler notificationHandler,
            ISimpleUnitOfWork simpleUnitOfWork)
            : base(simpleUnitOfWork, notificationHandler)
        {
            _tokenManager = tokenManager;
            _configuration = configuration;
            _localizationManager = localizationManager;
            _emailFactory = emailFactory;
            _userInvitationAdapter = userInvitationAdapter;
            _userInvitationReadRepository = userInvitationReadRepository;
            _userInvitationRepository = userInvitationRepository;
        }

        public async Task<UserInvitationDto> Create(UserInvitationDto dto, Guid tenantId, Guid userId)
        {
            ValidateDto<UserInvitationDto>(dto, nameof(dto));

            if (Notification.HasNotification())
                return UserInvitationDto.NullInstance;

            var oldInvitation = await _userInvitationReadRepository.GetDtoByUserIdAndTenantIdAsync(userId, tenantId);

            if (oldInvitation != null)
                return oldInvitation;

            dto.InvitationDate = DateTime.UtcNow;
            dto.InvitationAcceptanceDate = null;
            dto.IsActive = true;
            dto.UserId = userId;
            dto.Id = Guid.NewGuid();

            var userInvitationBuilder = _userInvitationAdapter.Map(dto);
            var userInvitation = userInvitationBuilder.Build();
            userInvitation.TenantId = tenantId;

            if (Notification.HasNotification())
                return null;

            dto.Id = _userInvitationRepository.Insert(userInvitation).Id;

            return dto;
        }

        public void SendMailNewUser(string email, string link)
        {
            var emailDto = new EmailUserInvitationDto()
            {
                Title = _localizationManager.GetString(AppConsts.LocalizationSourceName, EmailEnum.EmailSubjectUserInvitation.ToString()),
                Body = _localizationManager.GetString(AppConsts.LocalizationSourceName, EmailEnum.EmailBodyUserInvitation.ToString()),
                UrlInvitation = link
            };

            var host = _configuration.GetValue<string>("AwsEmailHost");
            var port = Convert.ToInt32(_configuration.GetValue<string>("AwsEmailPort"));
            var userName = _configuration.GetValue<string>("AwsEmailUserName");
            var passsword = _configuration.GetValue<string>("AwsEmailPassword");

            var subject = emailDto.Title;
            var from = _configuration.GetValue<string>("EmailNoReply");
            var body = _emailFactory.GenerateEmail(EmailTemplateEnum.EmailUserInvitation, emailDto);
            var to = email;

            // Prepare Email Message
            var message = EmailMessageBuilder
                            .Init()
                            .AddSubject(subject)
                            .AddFrom(from)
                            .AddBody(body)
                            .AddTo(to)
                            .Build();

            // Send Email Message
            IAwsEmailSender sender = new AwsEmailSender(new AwsEmailSettings(host, port, userName, passsword));
            sender.Send(message, true);
        }

        public async Task<UserInvitationDto> Get(DefaultGuidRequestDto id)
        {
            if (!ValidateRequestDto(id) || !ValidateId<Guid>(id.Id)) return null;

            if (Notification.HasNotification())
                return UserInvitationDto.NullInstance;

            return _userInvitationReadRepository.GetWithPropertyAndChainNames(id.Id);
        }

        public async Task<IListDto<UserInvitationDto>> GetAll(GetAllUserInvitationDto request)
        {
            ValidateRequestAllDto(request, nameof(request));

            if (Notification.HasNotification())
                return null;

            var response = _userInvitationReadRepository.GetAllFilterByTenant(request);
            return response;

        }

        public void ToggleActivation(Guid id)
        {
            if (id == Guid.Empty)
            {
                NotifyIdIsMissing();
                return;
            }

            _userInvitationRepository.ToggleAndSaveIsActive(id);
        }

        public void InvitationAccepted(Guid id)
        {
            if (id == Guid.Empty)
            {
                NotifyIdIsMissing();
                return;
            }

            _userInvitationRepository.InvitationAccepted(id);
        }

        public async Task UpdateInvitationLinkAsync(Guid userId, string newLink)
        {
            var userInvitation = await _userInvitationRepository.UpdateInvitationLinkAsync(userId, newLink);

            if (userInvitation == null)
                NotifyParameterInvalid();
        }

        private string CreateUserInvitationLink(string email, string name, Guid id)
        {
            string frontUrl = _configuration.GetValue<string>("UrlFront");

            return _tokenManager.CreateUserLink(frontUrl, email, name, id);
        }

        public async Task CreateAsync(CreateOrUpdateUserDto userDto, List<PropertyIdAndTenantDto> dtoList)
        {
            if (userDto == null || dtoList.Count == 0)
                NotifyParameterInvalid();

            if (Notification.HasNotification())
                return;

            var userInvitationLink = CreateUserInvitationLink(userDto.Email, userDto.Name, userDto.Id);
            var userInvitationList = new List<UserInvitation>();

            foreach (var propertyDto in dtoList)
            {
                var builder = _userInvitationAdapter.Map(userDto.Id, propertyDto.Id, propertyDto.TenantId, userDto.Email, userDto.Name, userInvitationLink);
                var entity = builder.Build();

                if (Notification.HasNotification())
                    return;

                userInvitationList.Add(entity);
            }

            await _userInvitationRepository.AddRangeAsync(userInvitationList);
        }

    }
}
