﻿using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Tnf.Notifications;
using Tnf.Localization;
using Thex.SuperAdmin.Application.Interfaces;
using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Infra.Factories;
using Thex.SuperAdmin.Application.Adapters;
using Thex.SuperAdmin.Infra.Interfaces;
using Thex.AspNetCore.Security.Interfaces;
using Thex.SuperAdmin.Infra.Entities;

namespace Thex.SuperAdmin.Application.Services
{
    public class PersonAppService : ApplicationServiceBase, IPersonAppService
    {
        private readonly ITokenManager _tokenManager;
        private readonly IConfiguration _configuration;
        private readonly ILocalizationManager _localizationManager;
        private readonly IEmailFactory _emailFactory;
        private readonly IPersonAdapter _personAdapter;
        private readonly IPersonRepository _personRepository;

        public PersonAppService(
            IPersonAdapter personAdapter,
            ITokenManager tokenManager,
            IConfiguration configuration,
            ILocalizationManager localizationManager,
            IEmailFactory emailFactory,
            IPersonRepository personRepository,
            INotificationHandler notificationHandler,
            ISimpleUnitOfWork simpleUnitOfWork)
            : base(simpleUnitOfWork, notificationHandler)
        {
            _tokenManager = tokenManager;
            _configuration = configuration;
            _localizationManager = localizationManager;
            _emailFactory = emailFactory;
            _personAdapter = personAdapter;
            _personRepository = personRepository;
        }

        public PersonDto Create(PersonDto dto)
        {
            ValidateDto<PersonDto>(dto, nameof(dto));

            if (Notification.HasNotification())
                return null;

            var personBuilder = _personAdapter.Map(dto);
            var person = personBuilder.Build();

            if (Notification.HasNotification())
                return null;

            dto.Id = _personRepository.Insert(person).Id;

            return dto;
        }

        public PersonDto UpdateAsync(Person entity, PersonDto dto)
        {
            ValidateDto<PersonDto>(dto, nameof(dto));

            if (Notification.HasNotification())
                return null;

            var personBuilder = _personAdapter.Map(entity, dto);
            var person = personBuilder.Build();

            _personRepository.Update(person);

            return dto;
        }

        public async Task UpdateProfilePersonAsync(Person entity, PersonDto personDto)
        => await _personRepository.UpdateAsync(_personAdapter.MapUpdateProfilePerson(entity, personDto).Build());
    }
}
