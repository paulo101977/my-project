﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Thex.Common;
using Thex.Common.Enumerations;
using Thex.Kernel;
using Thex.Location.Application.Interfaces;
using Thex.Storage;
using Thex.SuperAdmin.Application.Adapters;
using Thex.SuperAdmin.Application.Interfaces;
using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Dto.Dto;
using Thex.SuperAdmin.Infra.Entities;
using Thex.SuperAdmin.Infra.Interfaces;
using Thex.SuperAdmin.Infra.Interfaces.ReadRepositories;
using Thex.SuperAdmin.Infra.Interfaces.Services;
using Tnf.Notifications;

namespace Thex.SuperAdmin.Application.Services
{
    public class PropertyAppService : ApplicationServiceBase, IPropertyAppService
    {
        private readonly IPropertyDomainService _propertyDomainService;
        private readonly IPropertyAdapter _propertyAdapter;
        private readonly ITenantReadRepository _tenantReadRepository;
        private readonly ITenantRepository _tenantRepository;
        private readonly IPropertyReadRepository _propertyReadRepository;
        private readonly ILocationAppService _locationAppService;
        private readonly IBrandReadRepository _brandReadRepository;

        private readonly IAzureStorage _azureStorage;
        private readonly IConfiguration _configuration;
        private readonly IPropertyRepository _propertyRepository;
        private readonly IApplicationUser _applicationUser;
        private readonly IPropertyContractAdapter _propertyContractAdapter;
        private readonly IIntegrationPartnerPropertyReadRepository _integrationPartnerPropertyReadRepository;
        private readonly IIntegrationPartnerPropertyRepository _integrationPartnerPropertyRepository;
        private readonly IIntegrationPartnerPropertyAdapter _integrationPartnerPropertyAdapter;
        private readonly IPropertyMealPlanTypeRepository _propertyMealPlanTypeRepository;
        private readonly IHouseKeepingStatusPropertyRepository _houseKeepingStatusPropertyRepository;
        private readonly IPropertyParameterRepository _propertyParameterRepository;
        private readonly IPlasticBrandPropertyRepository _plasticBrandPropertyRepository;
        private readonly IIntegrationPartnerRepository _integrationPartnerRepository;
        private readonly IIntegrationPartnerReadRepository _integrationPartnerReadRepository;

        public PropertyAppService(
           IPropertyDomainService propertyDomainService,
           IPropertyAdapter propertyAdapter,
           ITenantReadRepository tenantReadRepository,
           ITenantRepository tenantRepository,
           IPropertyReadRepository propertyReadRepository,
           ILocationAppService locationAppService,
           IBrandReadRepository brandReadRepository,
           IAzureStorage azureStorage,
           IConfiguration configuration,
           INotificationHandler notificationHandler,
           ISimpleUnitOfWork simpleUnitOfWork,
           IPropertyRepository propertyRepository,
           IApplicationUser applicationUser,
           IPropertyContractAdapter propertyContractAdapter,
           IIntegrationPartnerPropertyReadRepository integrationPartnerPropertyReadRepository,
           IIntegrationPartnerPropertyRepository integrationPartnerPropertyRepository,
           IIntegrationPartnerPropertyAdapter integrationPartnerPropertyAdapter,
           IPropertyMealPlanTypeRepository propertyMealPlanTypeRepository,
           IHouseKeepingStatusPropertyRepository houseKeepingStatusPropertyRepository,
           IPropertyParameterRepository propertyParameterRepository,
           IPlasticBrandPropertyRepository plasticBrandPropertyRepository,
           IIntegrationPartnerReadRepository integrationPartnerReadRepository,
           IIntegrationPartnerRepository integrationPartnerRepository)
           : base(simpleUnitOfWork, notificationHandler)
        {
            _propertyDomainService = propertyDomainService;
            _propertyAdapter = propertyAdapter;
            _tenantReadRepository = tenantReadRepository;
            _tenantRepository = tenantRepository;
            _propertyReadRepository = propertyReadRepository;
            _locationAppService = locationAppService;
            _brandReadRepository = brandReadRepository;
            _azureStorage = azureStorage;
            _configuration = configuration;
            _propertyRepository = propertyRepository;
            _applicationUser = applicationUser;
            _propertyContractAdapter = propertyContractAdapter;
            _integrationPartnerPropertyReadRepository = integrationPartnerPropertyReadRepository;
            _integrationPartnerPropertyRepository = integrationPartnerPropertyRepository;
            _integrationPartnerPropertyAdapter = integrationPartnerPropertyAdapter;
            _propertyMealPlanTypeRepository = propertyMealPlanTypeRepository;
            _houseKeepingStatusPropertyRepository = houseKeepingStatusPropertyRepository;
            _propertyParameterRepository = propertyParameterRepository;
            _plasticBrandPropertyRepository = plasticBrandPropertyRepository;
            _integrationPartnerRepository = integrationPartnerRepository;
            _integrationPartnerReadRepository = integrationPartnerReadRepository;
        }

        public async Task<CreateOrUpdatePropertyDto> CreateAsync(CreateOrUpdatePropertyDto dto, IFormFile file)
        {
            ValidateDto<CreateOrUpdatePropertyDto>(dto, nameof(dto));

            if (Notification.HasNotification())
                return CreateOrUpdatePropertyDto.NullInstance;            

            using (var trans = _propertyReadRepository.Context.Database.BeginTransaction())
            {                
                dto.TenantId = dto.TenantId = _tenantRepository.GenerateTenantAutomaticallyAndSaveChanges($"{dto.Name} Property");

                if (Notification.HasNotification())
                    return null;

                dto.GeneratePropertyUid();

                // dto.PhotoUrl = await CreatePhoto(dto.TenantId, file);

                var propertyBuilder = _propertyAdapter.MapToCreate(dto);

                dto.Id = (await _propertyDomainService.InsertAndSaveChangesAsync(propertyBuilder)).Id;

                if (Notification.HasNotification()) return null;

                var propertyContract = _propertyContractAdapter.Map(new PropertyContractDto() { PropertyId = dto.Id, MaxUh = dto.MaxUh}).Build();

                if (Notification.HasNotification()) return null;

                await _propertyRepository.InsertPropertyContractAsync(propertyContract);

                if (Notification.HasNotification()) return null;

                await _propertyRepository.ChangeStatusAndSaveHistoryAsync(dto.Id, (int)PropertyStatusEnum.Register);

                if (dto.IsBlocked)
                    await _propertyRepository.SaveHistoryPropertyBlockedAsync(dto.Id);

                var chainId = await GetAndValidateChainId(dto.BrandId);

                if (Notification.HasNotification())
                    return CreateOrUpdatePropertyDto.NullInstance;

                _tenantRepository.SetProperty(dto.TenantId, dto.BrandId, chainId.Value, dto.Id);

                if (Notification.HasNotification()) return null;

                ClearLocationId(dto);

                _locationAppService.CreateLocationList(dto.LocationList, dto.PropertyUId);

                await SaveIntegrationPartnerPropertyList(dto.IntegrationPartnerPropertyList, dto.Id, dto.Name);

                if (Notification.HasNotification()) return null;

                await InsertRegistersDefault(dto.Id, dto.TenantId, dto.LocationList.FirstOrDefault()?.CountryCode);

                trans.Commit();
            }

            return dto;
        }

        public async Task<string> CreatePhoto(Guid ownerId, IFormFile file)
        {
            var propertyFolderName = _configuration.GetValue<string>("PropertyFolderName");
            var fileExt = Path.GetExtension(file.FileName);
            var fileName = Guid.NewGuid().ToString();

            return await _azureStorage.SendAsync(ownerId, propertyFolderName, $"{fileName}{fileExt}", file.OpenReadStream());
        }

        public async Task<ThexListDto<PropertyDto>> GetAll(GetAllPropertyDto request)
        {
            ValidateRequestAllDto(request, nameof(request));

            if (Notification.HasNotification())
                return null;

            return await _propertyReadRepository.GetAllDtoAsync(request);
        }

        public async Task<PropertyDto> Get(DefaultIntRequestDto id)
        {
            if (!ValidateRequestDto(id) || !ValidateId<int>(id.Id)) return null;

            if (Notification.HasNotification())
                return PropertyDto.NullInstance;

            var propertyDto = await _propertyReadRepository.GetDtoByIdAsync(id);
            propertyDto.RegisteredUhs = await _propertyReadRepository.GetRoomsByPropertyIdAsync(id.Id);
            propertyDto.LocationList = await _locationAppService.GetAllByOwnerId(propertyDto.PropertyUId, _applicationUser.PreferredCulture).AsTask();
            return propertyDto;
        }

        public async Task<CreateOrUpdatePropertyDto> UpdateAsync(int id, CreateOrUpdatePropertyDto dto, IFormFile file)
        {
            ValidateDtoAndId(dto, id, nameof(dto), nameof(id));

            if (Notification.HasNotification())
                return CreateOrUpdatePropertyDto.NullInstance;

            dto.Id = id;

            var property = await GetAndValidateProperty(id);

            if (Notification.HasNotification())
                return CreateOrUpdatePropertyDto.NullInstance;

            var chainId = await GetAndValidateChainId(dto.BrandId);

            if (Notification.HasNotification())
                return CreateOrUpdatePropertyDto.NullInstance;

            using (var trans = _propertyReadRepository.Context.Database.BeginTransaction())
            {
                if (file != null)
                    dto.PhotoUrl = await CreatePhoto(dto.TenantId, file);

                if (Notification.HasNotification()) return null;

                var propertyBuilder = _propertyAdapter.MapToUpdate(property, dto);

                await _propertyDomainService.UpdateAndSaveChangesAsync(propertyBuilder);

                if (Notification.HasNotification()) return null;

                if(await _propertyReadRepository.GetRoomsByPropertyIdAsync(dto.Id) > dto.MaxUh)
                {
                    Notification.Raise(Notification
                       .DefaultBuilder
                       .WithDetailedMessage(AppConsts.LocalizationSourceName, PropertyContract.EntityError.PropertyContractMaxUhNotMustBeLessRegisteredUh)
                       .WithMessage(AppConsts.LocalizationSourceName, PropertyContract.EntityError.PropertyContractMaxUhNotMustBeLessRegisteredUh)
                       .Build());

                    return null;
                }

                var propertyContract = _propertyContractAdapter.MapToUpdate
                    (await _propertyReadRepository.GetPropertyContractByPropertyIdAsync(id), new PropertyContractDto() { PropertyId = dto.Id, MaxUh = dto.MaxUh }).Build();

                if (Notification.HasNotification()) return null;

                await _propertyRepository.UpdatePropertyContractAsync(propertyContract);

                if (dto.IsBlocked != property.IsBlocked)
                    await _propertyRepository.SaveHistoryPropertyBlockedAsync(dto.Id);

                if (Notification.HasNotification()) return null;

                _tenantRepository.SetProperty(property.TenantId, dto.BrandId, chainId.Value, dto.Id);

                if (Notification.HasNotification()) return null;

                UpdateLocation(dto, property.PropertyUId.ToString());

                if (Notification.HasNotification()) return null;

                await UpdateIntegrationPartnerPropertyList(dto.IntegrationPartnerPropertyList, dto.Id, dto.Name);

                if (Notification.HasNotification()) return null;

                trans.Commit();
            }

            return dto;
        }

        private void UpdateLocation(CreateOrUpdatePropertyDto dto, string ownerId)
        {
            if (dto.LocationList != null)
            {
                var locationDtoList = _locationAppService.SetCountrySubdivision(dto.LocationList);

                _locationAppService.UpdateLocationList(locationDtoList.ToList(), ownerId);
            }
        }

        private void ClearLocationId(CreateOrUpdatePropertyDto dto)
        {
            if (dto.LocationList != null)
            {
                foreach (var locationDto in dto.LocationList)
                    locationDto.Id = 0;
            }
        }

        private async Task<Property> GetAndValidateProperty(int id)
        {
            var property = await _propertyReadRepository.GetByIdAsync(new DefaultIntRequestDto(id));

            if (property == null)
                NotifyNullParameter();

            return property;
        }

        private async Task<int?> GetAndValidateChainId(int brandId)
        {
            var chainId = await _brandReadRepository.GetChainId(brandId);

            if (!chainId.HasValue)
                NotifyIdIsMissing();

            return chainId;
        }

        #region IntegrationPartnerPropery
        public async Task SaveIntegrationPartnerPropertyList(IList<IntegrationPartnerPropertyDto> integrationPartnerPropertyDtoList, int propertyId, string propertyName)
        {
            await ValidateIntegrationPartnerPropertyList(integrationPartnerPropertyDtoList, propertyId);

            if (Notification.HasNotification()) return;

            await SaveIntegrationPartnerFromCMNETIntegrations(integrationPartnerPropertyDtoList, propertyName);

            var integrationPartnerPropertyList = new List<IntegrationPartnerProperty>();

            foreach (var integrationPartnerPropertyDto in integrationPartnerPropertyDtoList)
                integrationPartnerPropertyList.Add(_integrationPartnerPropertyAdapter
                    .Map(integrationPartnerPropertyDto)
                    .WithPropertyId(propertyId)
                    .Build());

            if (Notification.HasNotification()) return;

            await _integrationPartnerPropertyRepository.InsertRangeAndSaveChangesAsync(integrationPartnerPropertyList);
        }

        private async Task ValidateIntegrationPartnerPropertyList(IList<IntegrationPartnerPropertyDto> integrationPartnerPropertyDtoList, int propertyId)
        {
            var hasDuplicates = integrationPartnerPropertyDtoList
                .Select(ipp => ipp.PartnerId)
                .GroupBy(ipp => ipp)
                .Any(ipp => ipp.Count() > 1);

            if(hasDuplicates)
            {
               Notification.Raise(Notification
              .DefaultBuilder
              .WithDetailedMessage(AppConsts.LocalizationSourceName, IntegrationPartnerProperty.EntityError.ExistsDuplicateIntegrationPartnerProperties)
              .WithMessage(AppConsts.LocalizationSourceName, IntegrationPartnerProperty.EntityError.ExistsDuplicateIntegrationPartnerProperties)
              .Build());

                return;
            }

            var integrationCodesAreAvailable = await _integrationPartnerPropertyReadRepository.IntegrationCodesAvailableAsync(integrationPartnerPropertyDtoList, propertyId);
            if(!integrationCodesAreAvailable)
            {
              Notification.Raise(Notification
             .DefaultBuilder
             .WithDetailedMessage(AppConsts.LocalizationSourceName, IntegrationPartnerProperty.EntityError.AlreadyExistsIntegrationPartnerPropertyWithSameIntegrationCode)
             .WithMessage(AppConsts.LocalizationSourceName, IntegrationPartnerProperty.EntityError.AlreadyExistsIntegrationPartnerPropertyWithSameIntegrationCode)
             .Build());
            }
        }

        private async Task UpdateIntegrationPartnerPropertyList(IList<IntegrationPartnerPropertyDto> integrationPartnerPropertyDtoList, int propertyId, string propertyName)
        {
            await ValidateIntegrationPartnerPropertyList(integrationPartnerPropertyDtoList, propertyId);

            if (Notification.HasNotification()) return;

            var integrationPartnerPropertyListToInsert = new List<IntegrationPartnerProperty>();
            var integrationPartnerPropertyListToUpdate = new List<IntegrationPartnerProperty>();
            var integrationPartnerPropertyList = await _integrationPartnerPropertyReadRepository.GetAllByPropertyIdAsync(propertyId);
            var integrationPartnerPropertyIdListToRemove = integrationPartnerPropertyList.Select(ipp => ipp.Id).Where(ipp =>
                                                                !integrationPartnerPropertyDtoList.Select(ippd => ippd.Id).Contains(ipp))
                                                                .ToList();

            await SaveIntegrationPartnerFromCMNETIntegrations(
                integrationPartnerPropertyDtoList.Where(ipp => ipp.Id == default(Guid) && ipp.PartnerId == (int)PartnerEnum.BackCMNET).ToList(), propertyName);

            foreach (var integrationPartnerPropertyDto in integrationPartnerPropertyDtoList)
            {
                if(integrationPartnerPropertyDto.Id == default(Guid))
                    integrationPartnerPropertyListToInsert.Add(_integrationPartnerPropertyAdapter
                        .Map(integrationPartnerPropertyDto)
                        .WithPropertyId(propertyId)
                        .Build());
                else
                {
                    var integrationPartnerProperty = integrationPartnerPropertyList.FirstOrDefault(ipp => ipp.Id == integrationPartnerPropertyDto.Id);

                        integrationPartnerPropertyListToUpdate.Add(IntegrationPartnerProperty.Create(Notification)
                            .WithId(integrationPartnerProperty.Id)
                            .WithIntegrationCode(integrationPartnerPropertyDto.IntegrationCode)
                            .WithIntegrationPartnerId(integrationPartnerProperty.IntegrationPartnerId)
                            .WithIsActive(integrationPartnerPropertyDto.IsActive)
                            .WithPartnerId(integrationPartnerProperty.PartnerId)
                            .WithPropertyId(integrationPartnerProperty.PropertyId)
                            .Build());
                }
            }

            if (integrationPartnerPropertyListToInsert.Any())
                await _integrationPartnerPropertyRepository.InsertRangeAndSaveChangesAsync(integrationPartnerPropertyListToInsert);

            if(integrationPartnerPropertyListToUpdate.Any())
                await _integrationPartnerPropertyRepository.UpdateRangeAndSaveChangesAsync(integrationPartnerPropertyListToUpdate);

            if (integrationPartnerPropertyIdListToRemove.Any())
                await _integrationPartnerPropertyRepository.RemoveRangeAndSaveChangesAsync(integrationPartnerPropertyIdListToRemove);
                
        }

        private async Task SaveIntegrationPartnerFromCMNETIntegrations(IList<IntegrationPartnerPropertyDto> integrationPartnerPropertyDtoList, string propertyName)
        {
            foreach (var integration in integrationPartnerPropertyDtoList.Where(ipp => ipp.PartnerId == (int)PartnerEnum.BackCMNET))
            {
                var integrationName = $"{propertyName} {await _integrationPartnerReadRepository.GetPartnerNameByIdAsync(integration.PartnerId)}";

                var integrationPartner = IntegrationPartner.Create(Notification)
                                            .WithIntegrationPartnerName(integrationName)
                                            .WithIntegrationPartnerType((int)IntegrationPartnerTypeEnum.Internal)
                                            .WithIsActive(true)
                                            .WithTokenApplication(Guid.NewGuid().ToString())
                                            .WithTokenClient(Guid.NewGuid().ToString())
                                            .Build();

                integration.IntegrationPartnerId = (await _integrationPartnerRepository.InsertAndSaveChangesAsync(integrationPartner)).Id;
            }
        }

        #endregion

        #region DefaultInserts
        private async Task InsertRegistersDefault(int propertyId, Guid tenantId, string countryCode)
        {
            await _propertyMealPlanTypeRepository.CreatePropertyMealPlanTypesDefaultAsync(propertyId, tenantId, countryCode);
            await _houseKeepingStatusPropertyRepository.CreateHouseKeepingStatusPropertyDefaultAsync(propertyId, tenantId, countryCode);
            await _propertyParameterRepository.CreatePropertyParametersDefaultAsync(propertyId, tenantId);
            await _plasticBrandPropertyRepository.CreatePlasticBrandPropertiesDefault(propertyId, tenantId);
        }
        #endregion
    }
}
