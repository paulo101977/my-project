﻿using System;
using System.Threading.Tasks;
using Thex.Common;
using Thex.Common.Enumerations;
using Thex.SuperAdmin.Application.Interfaces;
using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Dto.Dto;
using Thex.SuperAdmin.Infra.Entities;
using Thex.SuperAdmin.Infra.Interfaces;
using Thex.SuperAdmin.Infra.Interfaces.ReadRepositories;
using Tnf.Notifications;

namespace Thex.SuperAdmin.Application.Services
{
    public class DashboardAppService : ApplicationServiceBase, IDashboardAppService
    {
        private readonly IPropertyReadRepository _propertyReadRepository;
        private readonly IPropertyRepository _propertyRepository;

        public DashboardAppService
            (ISimpleUnitOfWork uow,
            INotificationHandler notification,
            IPropertyReadRepository propertyReadRepository,
            IPropertyRepository propertyRepository)
            : base(uow, notification)
        {
            _propertyReadRepository = propertyReadRepository;
            _propertyRepository = propertyRepository;
        }

        public async Task<ThexListDto<DashboardPropertyDto>> GetAllPropertiesAsync()
        {
            return await _propertyReadRepository.GetAllDtoForDashboardAsync();
        }

        public async Task NextPropertyStatusAsync(int propertyId)
        {
            var propertyStatusId = await _propertyReadRepository.GetPropertyStatusByIdAsync(propertyId);
            if (propertyStatusId == 0)
            {
                NotifyNullParameter();
                return;
            }

            var statusEnum = (PropertyStatusEnum)Enum.ToObject(typeof(PropertyStatusEnum), propertyStatusId);
            switch (statusEnum)
            {
                case PropertyStatusEnum.Register:
                    await _propertyRepository.ChangeStatusAndSaveHistoryAsync(propertyId, (int)PropertyStatusEnum.Contract);
                    break;
                case PropertyStatusEnum.Contract:
                    await _propertyRepository.ChangeStatusAndSaveHistoryAsync(propertyId, (int)PropertyStatusEnum.Training);
                    break;
                case PropertyStatusEnum.Training:
                    await _propertyRepository.ChangeStatusAndSaveHistoryAsync(propertyId, (int)PropertyStatusEnum.Production);
                    break;
               
                case PropertyStatusEnum.Production:
                case PropertyStatusEnum.Blocked:
                case PropertyStatusEnum.Unblocked:
                default:
                    Notification.Raise(Notification.DefaultBuilder
                        .WithMessage(AppConsts.LocalizationSourceName, Status.EntityError.PropertyStatusInvalid)
                        .WithDetailedMessage(AppConsts.LocalizationSourceName, Status.EntityError.PropertyStatusInvalid)
                        .Build());
                    break;
            }
        }

        public async Task ToggleBlockPropertyAsync(int propertyId)
        {
            await _propertyRepository.ToggleIsBlockedPropertyAndSaveHistoryAsync(propertyId);
        }
    }
}
