﻿using System;
using System.Collections.Generic;
using System.Text;
using Thex.Common;
using Thex.Common.Enumerations;
using Thex.SuperAdmin.Application.Adapters;
using Thex.SuperAdmin.Application.Interfaces;
using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Dto.Dto;
using Thex.SuperAdmin.Infra.Entities;
using Thex.SuperAdmin.Infra.Interfaces;
using Thex.SuperAdmin.Infra.Interfaces.ReadRepositories;
using Tnf.Dto;
using Tnf.Notifications;

namespace Thex.SuperAdmin.Application.Services
{
    public class TenantAppService : ApplicationServiceBase, ITenantAppService
    {
        private readonly ITenantReadRepository _tenantReadRepository;
        private readonly ITenantRepository _tenantRepository;
        private readonly ITenantAdapter _tenantAdapter;

        public TenantAppService(
           ITenantReadRepository tenantReadRepository,
           ITenantRepository tenantRepository,
           ITenantAdapter tenantAdapter,
           INotificationHandler notificationHandler,
           ISimpleUnitOfWork simpleUnitOfWork)
           : base(simpleUnitOfWork, notificationHandler)
        {
            _tenantReadRepository = tenantReadRepository;
            _tenantRepository = tenantRepository;
            _tenantAdapter = tenantAdapter;
        }

        public TenantDto Create(TenantDto dto)
        {
            ValidateTenant(dto);

            if (Notification.HasNotification())
                return null;

            var tenant = _tenantAdapter.Map(dto).Build();

            if (Notification.HasNotification())
                return null;

            dto.Id = _tenantRepository.Insert(tenant).Id;

            Commit();

            return dto;
        }

        public TenantDto Update(TenantDto dto, Guid id)
        {
            ValidateTenant(dto);

            if (Notification.HasNotification())
                return null;

            dto.Id = id;

            ValidateUpdateTenant(id);

            if (Notification.HasNotification())
                return null;

            var entity = _tenantReadRepository.GetTenantById(id);

            var tenant = _tenantAdapter.MapToUpdate(entity, dto).Build();

            if (Notification.HasNotification())
                return null;

            _tenantRepository.Update(tenant);

            Commit();

            return dto;
        }

        public TenantDto Get(Guid id)
        {
            var tenant = _tenantReadRepository.GetDtoByid(id);
            if(tenant == null)
            {
                NotifyNullParameter();
                return null;
            }

            return tenant;
        }

        public ThexListDto<TenantDto> GetAllByFilter(GetAllTenantDto dto)
        {
            var tenants = _tenantReadRepository.GetAllDtoByFilter(dto);
            return tenants;
        }

        public IListDto<TenantDto> GetAllWithoutAssociation(Guid? tenantId)
        {
            var tenants = _tenantReadRepository.GetAllDtoWithoutAssociation(tenantId);
            return tenants;
        }

        private void ValidateTenant(TenantDto dto)
        {
            if(dto == null)
                NotifyNullParameter();
        }

        private void ValidateUpdateTenant(Guid id)
        {
            if (!_tenantReadRepository.Exists(id))
                NotifyNullParameter();
        }
    }
}
