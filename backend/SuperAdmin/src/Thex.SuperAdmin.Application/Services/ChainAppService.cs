﻿using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Tnf.Notifications;
using Tnf.Localization;
using Thex.SuperAdmin.Application.Interfaces;
using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Infra.Factories;
using Thex.SuperAdmin.Application.Adapters;
using Thex.SuperAdmin.Infra.Interfaces;
using Thex.Kernel;
using Tnf.Dto;
using Thex.SuperAdmin.Infra.Interfaces.ReadRepositories;
using Thex.SuperAdmin.Infra.Interfaces.Services;
using Thex.SuperAdmin.Infra.Entities;
using System;

namespace Thex.SuperAdmin.Application.Services
{
    public class ChainAppService : ApplicationServiceBase, IChainAppService
    {
        private readonly IChainAdapter _chainAdapter;
        private readonly IChainReadRepository _chainReadRepository;
        private readonly ITenantReadRepository _tenantReadRepository;
        private readonly IChainDomainService _chainDomainService;
        private readonly ITenantRepository _tenantRepository;
        private readonly IBrandReadRepository _brandReadRepository;
        private readonly IPropertyReadRepository _propertyReadRepository;

        public ChainAppService(
            IChainAdapter chainAdapter,
            IChainReadRepository chainReadRepository,
            IChainDomainService chainDomainService,
            ITenantReadRepository tenantReadRepository,
            ITenantRepository tenantRepository,
            INotificationHandler notificationHandler,
            ISimpleUnitOfWork simpleUnitOfWork,
            IBrandReadRepository brandReadRepository,
            IPropertyReadRepository propertyReadRepository)
            : base(simpleUnitOfWork, notificationHandler)
        {
            _chainAdapter = chainAdapter;
            _chainReadRepository = chainReadRepository;
            _chainDomainService = chainDomainService;
            _tenantReadRepository = tenantReadRepository;
            _tenantRepository = tenantRepository;
            _brandReadRepository = brandReadRepository;
            _propertyReadRepository = propertyReadRepository;
        }

        public async Task<IListDto<ChainDto>> GetAll(GetAllChainDto request)
        {
            ValidateRequestAllDto(request, nameof(request));

            if (Notification.HasNotification())
                return null;

            var response = await _chainReadRepository.GetAllDtoAsync(request);

            return response;
        }

        public async Task<ChainDto> Get(DefaultIntRequestDto id)
        {
            if (!ValidateRequestDto(id) || !ValidateId<int>(id.Id)) return null;

            if (Notification.HasNotification())
                return ChainDto.NullInstance;

            var chainDto = await _chainReadRepository.GetDtoByIdAsync(id);
            chainDto.BrandDtoList = await _brandReadRepository.GetAllDtoByChainIdAsync(id.Id);
            chainDto.PropertyDtoList = await _propertyReadRepository.GetAllDtoByChainIdAsync(id.Id);
            return chainDto;
        }

        public async Task<ChainDto> Create(ChainDto dto)
        {
            ValidateDto<ChainDto>(dto, nameof(dto));

            if (Notification.HasNotification())
                return ChainDto.NullInstance;
                      
            using (var trans = _chainReadRepository.Context.Database.BeginTransaction())
            {              
                dto.TenantId = dto.TenantId = _tenantRepository.GenerateTenantAutomaticallyAndSaveChanges($"{dto.Name} Chain");

                if (Notification.HasNotification())
                    return ChainDto.NullInstance;

                var chainBuilder = _chainAdapter.MapToCreate(dto);

                dto.Id = (await _chainDomainService.InsertAndSaveChangesAsync(chainBuilder)).Id;

                _tenantRepository.SetChain(dto.TenantId, dto.Id);

                trans.Commit();
            }
            return dto;
        }

        public async Task<ChainDto> Update(int id, ChainDto dto)
        {
            ValidateDtoAndId(dto, id, nameof(dto), nameof(id));

            if (Notification.HasNotification())
                return ChainDto.NullInstance;

            dto.Id = id;

            var chain = await _chainReadRepository.GetByIdAsync(new DefaultIntRequestDto(id));

            if (chain == null)
            {
                NotifyNullParameter();
                return ChainDto.NullInstance;
            }

            var chainBuilder = _chainAdapter.MapToUpdate(chain, dto);

            await _chainDomainService.UpdateAndSaveChangesAsync(chainBuilder);

            return dto;
        }
    }
}
