﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Thex.Common;
using Thex.SuperAdmin.Application.Adapters;
using Thex.SuperAdmin.Application.Interfaces;
using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Dto.Dto;
using Thex.SuperAdmin.Infra.Entities;
using Thex.SuperAdmin.Infra.Interfaces;
using Thex.SuperAdmin.Infra.Interfaces.ReadRepositories;
using Tnf.Notifications;

namespace Thex.SuperAdmin.Application.Services
{
    public class IdentityModuleAppService : ApplicationServiceBase, IIdentityModuleAppService
    {
        private readonly IIdentityModuleReadRepository _identityModuleReadRepository;

        public IdentityModuleAppService(
           IIdentityModuleReadRepository identityModuleReadRepository,
           INotificationHandler notificationHandler,
           ISimpleUnitOfWork simpleUnitOfWork)
           : base(simpleUnitOfWork, notificationHandler)
        {
            _identityModuleReadRepository = identityModuleReadRepository;
        }

        public async Task<ThexListDto<IdentityModuleDto>> GetAllAsync()
            => await _identityModuleReadRepository.GetAllAsync();
    }
}
