﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Thex.Common;
using Thex.Dto;
using Thex.SuperAdmin.Application.Adapters;
using Thex.SuperAdmin.Application.Interfaces;
using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Dto.Dto;
using Thex.SuperAdmin.Dto.Enumerations;
using Thex.SuperAdmin.Infra.Entities;
using Thex.SuperAdmin.Infra.Interfaces;
using Thex.SuperAdmin.Infra.Interfaces.ReadRepositories;
using Tnf.Notifications;

namespace Thex.SuperAdmin.Application.Services
{
    public class FeatureAppService : ApplicationServiceBase, IFeatureAppService
    {
        private readonly IIdentityFeatureAdapter _featureAdapter;
        private readonly IIdentityFeatureRepository _featureRepository;
        private readonly IIdentityFeatureReadRepository _featureReadRepository;

        public FeatureAppService(
           IIdentityFeatureAdapter featureAdapter,
           IIdentityFeatureRepository featureRepository,
           IIdentityFeatureReadRepository featureReadRepository,
           INotificationHandler notificationHandler,
           ISimpleUnitOfWork simpleUnitOfWork)
           : base(simpleUnitOfWork, notificationHandler)
        {
            _featureAdapter = featureAdapter;
            _featureRepository = featureRepository;
            _featureReadRepository = featureReadRepository;
        }
        
        public async Task<ThexListDto<IdentityFeatureDto>> GetAllByFilterAsync(GetAllIdentityFeatureDto dto)
            =>  await _featureReadRepository.GetAllDtoByFilterAsync(dto);

        public async Task<IdentityFeatureDto> GetAsync(Guid id)
            => await _featureReadRepository.GetDtoAsync(id);

        public async Task<IdentityFeatureDto> CreateAsync(IdentityFeatureDto dto)
        {
            ValidateDto(dto, nameof(dto));

            if (!Enum.IsDefined(typeof(IdentityProductEnum), dto.IdentityProductId) ||
                !Enum.IsDefined(typeof(IdentityModuleEnum), dto.IdentityModuleId))
                NotifyParameterInvalid();

            if (Notification.HasNotification())
                return IdentityFeatureDto.NullInstance;

            var feature = _featureAdapter.Map(dto).Build();

            if (Notification.HasNotification())
                return null;

            using (var trans = _featureReadRepository.Context.Database.BeginTransaction())
            {
                dto.Id = (await _featureRepository.InsertAndSaveChangesAsync(feature)).Id;

                trans.Commit();
            }

            return dto;
        }

        public async Task<IdentityFeatureDto> UpdateAsync(Guid id, IdentityFeatureDto dto)
        {
            if (Notification.HasNotification())
                return null;

            dto.Id = id;

            var entity = await _featureReadRepository.GetById(dto.Id);

            if(entity == null)
            {
                NotifyNullParameter();
                return null;
            }

            var feature = _featureAdapter.Map(entity, dto).Build();

            if (Notification.HasNotification())
                return null;

            using (var trans = _featureReadRepository.Context.Database.BeginTransaction())
            {
                await _featureRepository.UpdateAndSaveChangesAsync(feature);

                if (Notification.HasNotification())
                    return null;

                trans.Commit();
            }

            return dto;
        }
        
        public async Task DeleteAsync(Guid id)
        {
            if (id == Guid.Empty)
                NotifyIdIsMissing();

            if (!ValidateId(id)) return;

            using (var trans = _featureReadRepository.Context.Database.BeginTransaction())
            {
                await _featureRepository.RemoveAsync(id);

                if (Notification.HasNotification())
                    return;

                trans.Commit();
            }

        }
    }
}
