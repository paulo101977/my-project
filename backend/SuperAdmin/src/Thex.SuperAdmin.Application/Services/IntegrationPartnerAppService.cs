﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Thex.AspNetCore.Security;
using Thex.AspNetCore.Security.Interfaces;
using Thex.Common;
using Thex.Common.Enumerations;
using Thex.Dto;
using Thex.Kernel;
using Thex.Location.Infra.Interfaces.Read;
using Thex.SuperAdmin.Application.Adapters;
using Thex.SuperAdmin.Application.Interfaces;
using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Dto.Dto;
using Thex.SuperAdmin.Dto.Enumerations;
using Thex.SuperAdmin.Infra.Entities;
using Thex.SuperAdmin.Infra.Interfaces;
using Thex.SuperAdmin.Infra.Interfaces.ReadRepositories;
using Tnf.Notifications;

namespace Thex.SuperAdmin.Application.Services
{
    public class IntegrationPartnerAppService : ApplicationServiceBase, IIntegrationPartnerAppService
    {
        private readonly IApplicationUser _applicationUser;
        private readonly IIntegrationPartnerReadRepository _integrationPartnerReadRepository;
        private readonly ITokenManager _tokenManager;
        private readonly ITenantReadRepository _tenantReadRepository;
        private readonly IPropertyReadRepository _propertyReadRepository;
        private readonly IBrandReadRepository _brandReadRepository;
        private readonly IChainReadRepository _chainReadRepository;
        private readonly IIntegrationPartnerPropertyReadRepository _integrationPartnerPropertyReadRepository;
        private readonly ILocationReadRepository _locationReadRepository;

        public IntegrationPartnerAppService(
          IApplicationUser applicationUser,
          IIntegrationPartnerReadRepository integrationPartnerReadRepository,
          ITokenManager tokenManager,
          ITenantReadRepository tenantReadRepository,
          IPropertyReadRepository propertyReadRepository,
          IBrandReadRepository brandReadRepository,
          IChainReadRepository chainReadRepository,
          INotificationHandler notificationHandler,
          IIntegrationPartnerPropertyReadRepository integrationPartnerPropertyReadRepository,
          ISimpleUnitOfWork simpleUnitOfWork,
          ILocationReadRepository locationReadRepository)
            : base(simpleUnitOfWork, notificationHandler)
        {
            _applicationUser = applicationUser;
            _integrationPartnerReadRepository = integrationPartnerReadRepository;
            _tokenManager = tokenManager;
            _tenantReadRepository = tenantReadRepository;
            _propertyReadRepository = propertyReadRepository;
            _brandReadRepository = brandReadRepository;
            _chainReadRepository = chainReadRepository;
            _integrationPartnerPropertyReadRepository = integrationPartnerPropertyReadRepository;
            _locationReadRepository = locationReadRepository;
        }

        public async Task<IntegrationPartnerTokenDto> AuthenticateAndGenerateToken(AuthenticateIntegrationPartnerDto authenticateIntegrationPartnerDto)
        {
            ValidateAuthentication(authenticateIntegrationPartnerDto);
            if (Notification.HasNotification()) return null;

            var integrationPartner = _integrationPartnerReadRepository.GetDtoByTokenClientAndTokenApplication(authenticateIntegrationPartnerDto.TokenClient, authenticateIntegrationPartnerDto.TokenApplication);
            ValidateIntegrationPartner(integrationPartner);

            if (Notification.HasNotification()) return null;

            var token = await GetTokenByAuthenticate(authenticateIntegrationPartnerDto, integrationPartner);

            if(token  == null)
                Notification.Raise(Notification.DefaultBuilder
                  .WithMessage(AppConsts.LocalizationSourceName, CommonsEnum.Error.NullOrEmptyObject)
                  .WithForbiddenStatus()
                 .Build());

            return token;
        }

        public async Task<ThexListDto<PartnerDto>> GetAllPartnersAsync()
        {
            return await _integrationPartnerReadRepository.GetAllPartnersAsync();
        }

        private async Task<IntegrationPartnerTokenDto> GetTokenByAuthenticate(AuthenticateIntegrationPartnerDto authenticateIntegrationPartnerDto, IntegrationPartnerDto integrationPartnerDto)
        {
            var tokenInfo = new TokenInfo();
            tokenInfo.IsAdmin = false;
            tokenInfo.LoggedUserEmail = "";
            tokenInfo.LoggedUserName = integrationPartnerDto.IntegrationPartnerName;
            tokenInfo.LoggedUserUid = Guid.Parse(integrationPartnerDto.TokenApplication);

            if (authenticateIntegrationPartnerDto.TenantId.HasValue)
            {
                var tenantPropertyParameterJoinMap = _tenantReadRepository.GetTenantWithPropertyParameterById(authenticateIntegrationPartnerDto.TenantId.Value);
                if (tenantPropertyParameterJoinMap == null)
                    return null;

                tokenInfo.TenantId = tenantPropertyParameterJoinMap.Tenant.Id;
                tokenInfo.PropertyId = tenantPropertyParameterJoinMap.Tenant.PropertyId;
                tokenInfo.BrandId = tenantPropertyParameterJoinMap.Tenant.BrandId;
                tokenInfo.ChainId = tenantPropertyParameterJoinMap.Tenant.ChainId;

                if (tenantPropertyParameterJoinMap.PropertyParameter != null)
                    tokenInfo.TimeZoneName = tenantPropertyParameterJoinMap.PropertyParameter.PropertyParameterValue;
            }
            else if (authenticateIntegrationPartnerDto.PropertyId.HasValue && authenticateIntegrationPartnerDto.PropertyId.Value != 0)
            {
                var property = _propertyReadRepository.GetWithBrandAndChain(authenticateIntegrationPartnerDto.PropertyId.Value);
                if (property == null)
                    return null;

                tokenInfo.TenantId = property.TenantId;
                tokenInfo.PropertyId = property.Id;
                tokenInfo.ChainId = property.ChainId;
                tokenInfo.BrandId = property.BrandId;
            }
            else if (authenticateIntegrationPartnerDto.BrandId.HasValue && authenticateIntegrationPartnerDto.BrandId.Value != 0)
            {
                var brand = await _brandReadRepository.GetById(authenticateIntegrationPartnerDto.BrandId.Value);
                if (brand == null)
                    return null;

                tokenInfo.TenantId = brand.TenantId;
                tokenInfo.BrandId = brand.Id;
                tokenInfo.ChainId = brand.ChainId;
            }
            else if (authenticateIntegrationPartnerDto.ChainId.HasValue && authenticateIntegrationPartnerDto.ChainId.Value != 0)
            {
                var tenantChainId = await _chainReadRepository.GetTenantId(authenticateIntegrationPartnerDto.ChainId.Value);

                tokenInfo.TenantId = tenantChainId;
                tokenInfo.ChainId = integrationPartnerDto.IntegrationPartnerId;                                
            }
            else
            {
                var integrationDto = await _integrationPartnerPropertyReadRepository.Get(authenticateIntegrationPartnerDto.TokenClient, authenticateIntegrationPartnerDto.TokenApplication);

                if (integrationDto == null)
                    return null;

                var property = _propertyReadRepository.GetWithBrandAndChain(integrationDto.PropertyId);

                if (property == null)
                    return null;

                tokenInfo.TenantId = property.TenantId;
                tokenInfo.PropertyId = property.Id;
                tokenInfo.ChainId = property.ChainId;
                tokenInfo.BrandId = property.BrandId;

                var dto = _locationReadRepository.GetCountryAndDistrictCodeByPropertyId(property.Id);
                if (dto != null)
                    tokenInfo.PropertyCountryCode = dto.PropertyCountryCode;
            }

            var identity = _tokenManager.CreateClaimIdentity(integrationPartnerDto.IntegrationPartnerId, tokenInfo);
            return _tokenManager.CreateIntegrationPartnerToken(identity);
        }
        
        private void ValidateAuthentication(AuthenticateIntegrationPartnerDto authenticateIntegrationPartnerDto)
        {
            if (authenticateIntegrationPartnerDto == null || string.IsNullOrWhiteSpace(authenticateIntegrationPartnerDto.TokenApplication) || string.IsNullOrWhiteSpace(authenticateIntegrationPartnerDto.TokenClient))
            {
                Notification.Raise(Notification.DefaultBuilder
                  .WithMessage(AppConsts.LocalizationSourceName, IntegrationPartner.EntityError.IntegrationPartnerTypeNotFound)
                  .WithDetailedMessage(AppConsts.LocalizationSourceName, IntegrationPartner.EntityError.IntegrationPartnerTypeNotFound)
                  .Build());
            };
        }

        private void ValidateIntegrationPartner(IntegrationPartnerDto integrationPartner)
        {
            if (integrationPartner == null)
            {
                Notification.Raise(Notification.DefaultBuilder
                   .WithMessage(AppConsts.LocalizationSourceName, IntegrationPartner.EntityError.IntegrationPartnerTypeNotFound)
                   .WithDetailedMessage(AppConsts.LocalizationSourceName, IntegrationPartner.EntityError.IntegrationPartnerTypeNotFound)
                   .Build());
            }
        }
    }
}
