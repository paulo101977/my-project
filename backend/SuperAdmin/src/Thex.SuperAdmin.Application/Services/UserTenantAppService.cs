﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Thex.Common;
using Thex.Kernel;
using Thex.Location.Application;
using Thex.SuperAdmin.Application.Adapters;
using Thex.SuperAdmin.Application.Interfaces;
using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Dto.Dto;
using Thex.SuperAdmin.Infra.Entities;
using Thex.SuperAdmin.Infra.Interfaces;
using Thex.SuperAdmin.Infra.Interfaces.ReadRepositories;
using Tnf.Notifications;

namespace Thex.SuperAdmin.Application.Services
{
    public class UserTenantAppService : ApplicationServiceBase, IUserTenantAppService
    {
        private readonly IChainReadRepository _chainReadRepository;
        private readonly IUserTenantAdapter _userTenantAdapter;
        private readonly IUserTenantRepository _userTenantRepository;
        private readonly IBrandReadRepository _brandReadRepository;
        private readonly IPropertyReadRepository _propertyReadRepository;
        private readonly IApplicationUser _applicationUser;
        private readonly IUserTenantReadRepository _userTenantReadRepository;
        private readonly IUserRoleAppService _userRoleAppService;
        private readonly IConfiguration _configuration;
        private readonly IUserInvitationAppService _userInvitationAppService;

        public UserTenantAppService
            (INotificationHandler notification,
            ISimpleUnitOfWork uow,
            IChainReadRepository chainReadRepository,
            IUserTenantAdapter userTenantAdapter,
            IUserTenantRepository userTenantRepository,
            IBrandReadRepository brandReadRepository,
            IPropertyReadRepository propertyReadRepository,
            IUserRoleAppService userRoleAppService,
            IApplicationUser applicationUser,
            IConfiguration configuration,
            IUserTenantReadRepository userTenantReadRepository,
            IUserInvitationAppService userInvitationAppService
            ) : base(uow, notification)
        {
            _chainReadRepository = chainReadRepository;
            _userTenantAdapter = userTenantAdapter;
            _userTenantRepository = userTenantRepository;
            _brandReadRepository = brandReadRepository;
            _propertyReadRepository = propertyReadRepository;
            _applicationUser = applicationUser;
            _userTenantReadRepository = userTenantReadRepository;
            _userRoleAppService = userRoleAppService;
            _configuration = configuration;
            _userInvitationAppService = userInvitationAppService;
        }

        #region associations

        private async Task AssociateChain(CreateOrUpdateUserDto dto)
        {
            if (dto.ChainId.HasValue)
            {
                var chainTenantId = await _chainReadRepository.GetTenantId(dto.ChainId.Value);
                var userTenant = CreateMap(dto.Id, chainTenantId);
                await _userTenantRepository.InsertAndSaveChangesAsync(userTenant);
            }
        }

        private async Task AssociateBrands(CreateOrUpdateUserDto dto)
        {
            var brandTenantToAdd = new List<Guid>();
            var userTenantListToAdd = new List<UserTenant>();

            if (dto.BrandList != null && dto.BrandList.Count > 0)
                brandTenantToAdd.AddRange(await _brandReadRepository.GetAllTenantIdListAsync(dto.BrandList.Select(e => e.Id).ToList()));

            if (dto.ChainId.HasValue)
                brandTenantToAdd.AddRange(await _brandReadRepository.GetAllTenantIdListByChainIdAsync(dto.ChainId.Value));

            foreach (var brandTenantId in brandTenantToAdd.Distinct().ToList())
                userTenantListToAdd.Add(CreateMap(dto.Id, brandTenantId));

            if (userTenantListToAdd.Count > 0)
                await _userTenantRepository.InsertRangeAndSaveChangesAsync(userTenantListToAdd);
        }

        private async Task AssociateProperties(CreateOrUpdateUserDto dto)
        {
            var propertyIdAndTenantToAdd = new List<PropertyIdAndTenantDto>();
            var userTenantListToAdd = new List<UserTenant>();
            var adminRole = _configuration.GetValue<Guid>("AdminRole");

            if (dto.PropertyList != null && dto.PropertyList.Count > 0)
            {
                var result = await _propertyReadRepository.GetAllTenantIdListAsync(dto.PropertyList.Select(e => e.Id).ToList());

                if (result.Count != 0)
                    propertyIdAndTenantToAdd.AddRange(result);
            }

            if (dto.BrandList != null && dto.BrandList.Count > 0)
            {
                var result = await _propertyReadRepository.GetAllTenantIdListByBrandIdListAsync(dto.BrandList.Select(e => e.Id).ToList());

                if(result.Count != 0)
                    propertyIdAndTenantToAdd.AddRange(result);
            }

            if (dto.ChainId.HasValue)
            {
                var result = await _propertyReadRepository.GetAllTenantIdListByChainIdAsync(dto.ChainId.Value);

                if (result.Count != 0)
                    propertyIdAndTenantToAdd.AddRange(result);
            }

            foreach (var propertyTenantId in propertyIdAndTenantToAdd.Select(e => e.TenantId).Distinct().ToList())
                userTenantListToAdd.Add(CreateMap(dto.Id, propertyTenantId));


            if(userTenantListToAdd.Count > 0)
            {
                await _userTenantRepository.InsertRangeAndSaveChangesAsync(userTenantListToAdd);

                await _userRoleAppService.CreateAsync(dto.Id, adminRole, propertyIdAndTenantToAdd.Select(e => e.Id).ToList());

                await _userInvitationAppService.CreateAsync(dto, propertyIdAndTenantToAdd);
            }
        }

        public UserTenant CreateMap(Guid userId, Guid tenantId)
            => _userTenantAdapter.Map(new UserTenantDto
            {
                TenantId = tenantId,
                UserId = userId,
                IsActive = true
            }).Build();

        #endregion

        public async Task AssociateUserTenant(CreateOrUpdateUserDto dto)
        {
            await AssociateChain(dto);

            await AssociateBrands(dto);

            await AssociateProperties(dto);
        }

        public async Task UpdateAssociationUserTenant(CreateOrUpdateUserDto dto, User user)
        {
            await _userTenantRepository.RemoveAllByUserIdAsync(user.Id);
            await AssociateUserTenant(dto);
        }

        public async Task AssociateUserToLoggedTenant(Guid userId, bool isActive = false)
        {
            var loggedTenantId = _applicationUser.TenantId;

            if(loggedTenantId == Guid.Empty || userId == Guid.Empty)
            {
                NotifyParameterInvalid();
                return;
            }

            if (await _userTenantReadRepository.Any(userId, loggedTenantId))
                return;

            var userTenant = _userTenantAdapter.Map(new UserTenantDto {
                    TenantId = loggedTenantId,
                    UserId = userId,
                    IsActive = isActive
            }).Build();

            await _userTenantRepository.InsertAndSaveChangesAsync(userTenant);
        }

        public void ToggleActivation(Guid userId, int propertyId)
        {
            if (userId == Guid.Empty || propertyId == 0)
            {
                NotifyIdIsMissing();
                return;
            }

            var loggedPropertyId = 0;
            int.TryParse(_applicationUser.PropertyId, out loggedPropertyId);

            if (propertyId != loggedPropertyId)
            {
                NotifyParameterInvalid();
                return;
            }

            _userTenantRepository.ToggleAndSaveIsActive(userId, propertyId);

            Commit();

        }
    }
}
