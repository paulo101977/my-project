﻿using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Thex.AspNetCore.Security.Interfaces;
using Thex.Common;
using Thex.Common.Enumerations;
using Thex.Kernel;
using Thex.SuperAdmin.Application.Adapters;
using Thex.SuperAdmin.Application.Interfaces;
using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Dto.Dto;
using Thex.SuperAdmin.Infra.Entities;
using Thex.SuperAdmin.Infra.Interfaces;
using Thex.SuperAdmin.Infra.Interfaces.ReadRepositories;
using Tnf.Notifications;

namespace Thex.SuperAdmin.Application.Services
{
    public class UserTotvsAppService : ApplicationServiceBase, IUserTotvsAppService
    {
        private readonly IUserRepository _userRepository;
        private readonly IUserReadRepository _userReadRepository;
        private readonly IUserAdapter _userAdapter;
        private readonly UserManager<User> _userManager;
        private readonly IApplicationUser _applicationUser;
        private readonly IPersonAppService _personAppService;
        private readonly IUserTenantAppService _userTenantAppService;
        private readonly IPersonReadRepository _personReadRepository;

        public UserTotvsAppService(
            ISimpleUnitOfWork uow,
            IUserRepository userRepository,
            IUserReadRepository userReadRepository,
            UserManager<User> userManager,
            IUserAdapter userAdapter,
            IApplicationUser applicationUser,
            IPersonAppService personAppService,
            IUserTenantAppService userTenantAppService,
            IPersonReadRepository personReadRepository,
            INotificationHandler notification)
            : base(uow, notification)
        {
            _userRepository = userRepository;
            _userReadRepository = userReadRepository;
            _userAdapter = userAdapter;
            _userManager = userManager;
            _applicationUser = applicationUser;
            _personAppService = personAppService;
            _userTenantAppService = userTenantAppService;
            _personReadRepository = personReadRepository;
        }

        public async Task<ThexListDto<UsersDto>> GetAllAsync(GetAllUserDto request)
        {
            return await _userReadRepository.GetAllAsync(request);
        }

        public async Task<CreateOrUpdateUserDto> CreateAsync(CreateOrUpdateUserDto dto)
        {
            ValidateDto(dto, nameof(dto));

            if (Notification.HasNotification()) return null;

            using (var transaction = _userReadRepository.Context.Database.BeginTransaction())
            {
                dto.Id = (await CreateNewUser(dto, Guid.NewGuid())).Id;

                if (Notification.HasNotification()) return null;

                await _userTenantAppService.AssociateUserTenant(dto);

                if (Notification.HasNotification()) return null;

                transaction.Commit();
            }

            return dto;
        }

        public async Task<UsersDto> GetAsync(Guid id)
        {
            var userDto = await _userReadRepository.GetDtoByIdAsync(id);
            if (userDto == null)
            {
                NotifyNullParameter();
                return null;
            }

            return userDto;
        }

        public async Task ToggleIsActiveAsync(Guid id)
        {
            await _userRepository.ToggleIsActiveAsync(id);
        }

        public async Task<CreateOrUpdateUserDto> UpdateAsync(CreateOrUpdateUserDto dto, Guid id)
        {
            ValidateDto(dto, nameof(dto));

            if (Notification.HasNotification()) return null;

            dto.Id = id;

            var entity = await _userReadRepository.GetByIdAsync(id);
            if (entity == null)
            {
                NotifyNullParameter();
                return null;
            }

            using (var transaction = _userReadRepository.Context.Database.BeginTransaction())
            {                
                var user = _userAdapter.MapUpdateUserAdmin(entity, dto).Build();
                if (Notification.HasNotification()) return null;

                await _userRepository.UpdateAndSaveChangesAsync(user);

                if (Notification.HasNotification()) return null;

                UpdatePersonUser(dto, user.PersonId.Value);

                if (Notification.HasNotification()) return null;

                await _userTenantAppService.UpdateAssociationUserTenant(dto, entity);

                if (Notification.HasNotification()) return null;

                transaction.Commit();
            }

            return dto;
        }

        #region Private Methods
        private async Task<UserDto> CreateNewUser(CreateOrUpdateUserDto dto, Guid userId)
        {
            User userDto = null;

            try
            {
                if (!string.IsNullOrEmpty(dto.Email) && !string.IsNullOrEmpty(dto.Name))
                {
                    // Creating Person as contact binded to User
                    Guid personId = Guid.NewGuid();

                    var contactInformationList = new HashSet<ContactInformationDto>();
                    var contactEmail = new ContactInformationDto()
                    {
                        ContactInformationTypeId = (int)ContactInformationTypeEnum.Email,
                        Information = dto.Email,
                        OwnerId = personId
                    };
                    contactInformationList.Add(contactEmail);

                    var personDto = GeneratePersonDto(dto, personId);
                    personDto.ContactInformationList = contactInformationList;
                    personDto = _personAppService.Create(personDto);

                    if (personDto != null && !Notification.HasNotification())
                    {
                        // Creating User
                        userDto = new User()
                        {
                            Id = userId,
                            Name = dto.Name,
                            Email = dto.Email,
                            PersonId = personId,
                            PreferredCulture = dto.PreferredLanguage,
                            PreferredLanguage = dto.PreferredLanguage,
                            IsAdmin = true,
                            IsActive = true,
                            UserName = dto.Email,
                            CreationTime = DateTime.Now,
                            CreatorUserId = _applicationUser.UserUid,
                            LastModificationTime = DateTime.Now,
                            LastModifierUserId = _applicationUser.UserUid
                        };
                        var result = await _userManager.CreateAsync(userDto, GenerateRandomPassword());

                        NotifyIdentityErros(result);
                    }
                }
            }
            catch (Exception e)
            {
                Notification.Raise(Notification.DefaultBuilder
                        .WithMessage(AppConsts.LocalizationSourceName, UserEnum.Error.CreateNewUser)
                        .WithDetailedMessage(e.InnerException.Message)
                        .Build());
            }

            return new UserDto
            {
                Id = userDto.Id,
                UserName = userDto.UserName,
                Name = userDto.Name,
                Email = userDto.Email,
                PersonId = userDto.PersonId.Value,
                PreferredCulture = userDto.PreferredLanguage,
                PreferredLanguage = userDto.PreferredLanguage,
                IsAdmin = userDto.IsAdmin,
                IsActive = userDto.IsActive,
                TenantId = userDto.TenantId ?? Guid.Empty
            };
        }

        private void UpdatePersonUser(CreateOrUpdateUserDto dto, Guid personId)
        {
            var person = _personReadRepository.Get(personId);

            var contactInformationList = new HashSet<ContactInformationDto>();
            var contactEmail = new ContactInformationDto()
            {
                ContactInformationTypeId = (int)ContactInformationTypeEnum.Email,
                Information = dto.Email,
                OwnerId = personId
            };

            contactInformationList.Add(contactEmail);
            var personDto = GeneratePersonDto(dto, personId);
            personDto.ContactInformationList = contactInformationList;

            _personAppService.UpdateAsync(person, personDto);
        }

        private PersonDto GeneratePersonDto(CreateOrUpdateUserDto dto, Guid personId)
        {
            var names = dto.Name.Split(new char[] { ' ' }, 2);
            var firstName = names.FirstOrDefault();
            var lastName = names.FirstOrDefault().Equals(names.LastOrDefault()) ? string.Empty : names.LastOrDefault();
            return new ContactPersonDto(firstName, lastName)
            {
                Id = personId,
                PersonType = "C"
            };
        }

        private string GenerateRandomPassword(PasswordOptions opts = null)
        {
            if (opts == null) opts = new PasswordOptions()
            {
                RequiredLength = 8,
                RequiredUniqueChars = 4,
                RequireDigit = true,
                RequireLowercase = true,
                RequireNonAlphanumeric = true,
                RequireUppercase = true
            };

            string[] randomChars = new[] {
                "ABCDEFGHJKLMNOPQRSTUVWXYZ",    // uppercase 
                "abcdefghijkmnopqrstuvwxyz",    // lowercase
                "0123456789",                   // digits
                "!@$?_-"                        // non-alphanumeric
            };
            Random rand = new Random(Environment.TickCount);
            System.Collections.Generic.List<char> chars = new System.Collections.Generic.List<char>();

            if (opts.RequireUppercase)
                chars.Insert(rand.Next(0, chars.Count),
                    randomChars[0][rand.Next(0, randomChars[0].Length)]);

            if (opts.RequireLowercase)
                chars.Insert(rand.Next(0, chars.Count),
                    randomChars[1][rand.Next(0, randomChars[1].Length)]);

            if (opts.RequireDigit)
                chars.Insert(rand.Next(0, chars.Count),
                    randomChars[2][rand.Next(0, randomChars[2].Length)]);

            if (opts.RequireNonAlphanumeric)
                chars.Insert(rand.Next(0, chars.Count),
                    randomChars[3][rand.Next(0, randomChars[3].Length)]);

            for (int i = chars.Count; i < opts.RequiredLength
                || chars.Distinct().Count() < opts.RequiredUniqueChars; i++)
            {
                string rcs = randomChars[rand.Next(0, randomChars.Length)];
                chars.Insert(rand.Next(0, chars.Count),
                    rcs[rand.Next(0, rcs.Length)]);
            }

            return new string(chars.ToArray());
        }
        #endregion
    }
}
