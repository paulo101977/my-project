﻿using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Thex.Common;
using Thex.Dto;
using Thex.SuperAdmin.Application.Adapters;
using Thex.SuperAdmin.Application.Interfaces;
using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Dto.Dto;
using Thex.SuperAdmin.Dto.Enumerations;
using Thex.SuperAdmin.Infra.Entities;
using Thex.SuperAdmin.Infra.Interfaces;
using Thex.SuperAdmin.Infra.Interfaces.ReadRepositories;
using Thex.SuperAdmin.Infra.Repositories;
using Thex.SuperAdmin.Infra.Services;
using Tnf.Localization;
using Tnf.Notifications;

namespace Thex.SuperAdmin.Application.Services
{
    public class RolesAppService : ApplicationServiceBase, IRolesAppService
    {
        private readonly IRolesAdapter _roleAdapter;
        private readonly IRolesRepository _roleRepository;
        private readonly IRolesReadRepository _roleReadRepository;
        private ThexIdentityRoleManager _roleManager;
        private readonly IIdentityPermissionReadRepository _identityPermissionReadRepository;
        private readonly IIdentityRolePermissionAdapter _identityRolePermissionAdapter;
        private readonly IIdentityTranslationRepository _identityTranslationRepository;
        private readonly ILocalizationManager _localizationManager;
        private readonly IConfiguration _configuration;

        public RolesAppService(
           ThexIdentityRoleManager roleManager,
           IRolesAdapter rolesAdapter,
           IRolesRepository rolesRepository,
           IRolesReadRepository rolesReadRepository,
           IIdentityPermissionReadRepository identityPermissionReadRepository,
           IIdentityRolePermissionAdapter identityRolePermissionAdapter,
           INotificationHandler notificationHandler,
           IIdentityTranslationRepository identityTranslationRepository,
           ILocalizationManager localizationManager,
           ISimpleUnitOfWork simpleUnitOfWork,
           IConfiguration configuration)
           : base(simpleUnitOfWork, notificationHandler)
        {
            _roleAdapter = rolesAdapter;
            _roleRepository = rolesRepository;
            _roleReadRepository = rolesReadRepository;
            _identityPermissionReadRepository = identityPermissionReadRepository;
            _identityRolePermissionAdapter = identityRolePermissionAdapter;
            _roleManager = roleManager;
            _identityTranslationRepository = identityTranslationRepository;
            _localizationManager = localizationManager;
            _configuration = configuration;
        }

        public async Task<ThexListDto<RolesDto>> GetAllByFilterAsync(GetAllRoleDto dto)
        {
            var roleDtoList = await _roleReadRepository.GetAllDtoByFilterAsync(dto);

            //TODO: Fix Central Roles
            roleDtoList.Items = roleDtoList.Items.Where(r => r.Id != Guid.Parse("12BC5291-C1CA-4E34-BA8B-C59870182FE3")).ToList();

            return roleDtoList;
        }

        public async Task<RolesDto> GetAsync(Guid id)
        {
            var role = await _roleReadRepository.GetByIdAsync(id);

            if (role == null)
                return RolesDto.NullInstance;

            var roleDto = role.MapTo<RolesDto>();

            roleDto.RolePermissionList = await _roleReadRepository.GetAllRolePermissionDtoByRoleIdAsync(id);

            return roleDto;
        }

        public async Task<RolesDto> CreateAsync(RolesDto dto)
        {
            ValidateDto(dto, nameof(dto));

            if (Notification.HasNotification())
                return RolesDto.NullInstance;

            if (dto == null || !Enum.IsDefined(typeof(IdentityProductEnum), dto.IdentityProductId))
                NotifyParameterInvalid();

            if (Notification.HasNotification())
                return RolesDto.NullInstance;

            var role = _roleAdapter.Map(dto).Build();

            if (Notification.HasNotification())
                return null;

            using (var trans = _roleReadRepository.Context.Database.BeginTransaction())
            {
                var roleResult = await _roleManager.CreateAsync(role);

                CreateDefaultRoles(dto);

                //mapear erros do Identity
                if (roleResult.Succeeded)
                {
                    dto.Id = role.Id;

                    if (dto.RolePermissionList != null && dto.RolePermissionList.Count > 0)
                        await CreateRolePermissionAsync(role.Id, dto.RolePermissionList);

                    await _identityTranslationRepository.InsertOrUpdateAndSaveChangesAsync(dto.Id, dto.TranslationList);
                }
                else
                {
                    foreach (var error in roleResult.Errors)
                    {
                        var notificationBuilder = Notification.DefaultBuilder;
                        IdentityErrosEnum enumValue;
                        if (Enum.TryParse(error.Code, true, out enumValue))
                            notificationBuilder.WithMessage(AppConsts.LocalizationSourceName, enumValue);
                        else
                            notificationBuilder.WithDetailedMessage(error.Description);

                        Notification.Raise(notificationBuilder.Build());
                    }
                }

                trans.Commit();
            }

            return dto;
        }

        private void CreateDefaultRoles(RolesDto dto)
        {
            List<string> defaultPermissions = _configuration.GetSection("DefaultPermissions:PermissionList").Get<List<string>>();

            foreach (var perm in defaultPermissions)
                if (dto.RolePermissionList.FindAll(permission => perm.ToLower() == permission.IdentityPermissionId.ToString().ToLower()).Count == 0)
                {
                    dto.RolePermissionList.Add(new IdentityRolePermissionDto()
                    {
                        IdentityPermissionId = Guid.Parse(perm),
                        IsActive = true
                    });
                }

        }

        public async Task CreateRolePermissionAsync(Guid roleId, List<IdentityRolePermissionDto> rolePermissionDtoList)
        {
            var rolePermissionList = new List<IdentityRolePermission>();

            foreach (var rolePermissionDto in rolePermissionDtoList)
            {
                var rolePermission = _identityRolePermissionAdapter.Map(rolePermissionDto, roleId).Build();

                if (Notification.HasNotification())
                    continue;

                rolePermissionList.Add(rolePermission);
            }

            await _roleRepository.AddRolePermissionRangeAsync(rolePermissionList);
        }

        public async Task<RolesDto> UpdateAsync(Guid id, RolesDto dto)
        {
            if (dto == null || !Enum.IsDefined(typeof(IdentityProductEnum), dto.IdentityProductId))
                NotifyParameterInvalid();

            if (Notification.HasNotification())
                return RolesDto.NullInstance;

            dto.Id = id;

            var entity = await _roleManager.FindByIdAsync(id.ToString());

            if (entity == null)
            {
                NotifyNullParameter();
                return null;
            }

            var role = _roleAdapter.Map(entity, dto).Build();

            if (Notification.HasNotification())
                return null;

            using (var trans = _roleReadRepository.Context.Database.BeginTransaction())
            {
                await _roleManager.UpdateAsync(role);

                if (Notification.HasNotification())
                    return null;

                CreateDefaultRoles(dto);

                await _roleRepository.CreateOrUpdateRolePermissionAsync(role.Id, dto.RolePermissionList);

                await _identityTranslationRepository.InsertOrUpdateAndSaveChangesAsync(role.Id, dto.TranslationList);

                trans.Commit();
            }
            return dto;
        }

        public async Task DeleteAsync(Guid id)
        {
            if (id == Guid.Empty)
                NotifyIdIsMissing();

            if (!ValidateId(id)) return;

            using (var trans = _roleReadRepository.Context.Database.BeginTransaction())
            {
                await _roleRepository.RemoveAsync(id);

                if (Notification.HasNotification())
                    return;

                trans.Commit();
            }

        }

        public void ToggleActivation(Guid id)
        {
            if (id == Guid.Empty)
            {
                NotifyIdIsMissing();
                return;
            }

            _roleRepository.ToggleAndSaveIsActive(id);

            Commit();
        }
    }

    public class ThexRoleValidator : RoleValidator<Roles>
    {
        public override async Task<IdentityResult> ValidateAsync(RoleManager<Roles> manager, Roles role)
        {
            //return base.ValidateAsync(manager, role);
            return IdentityResult.Success;
        }
    }
}
