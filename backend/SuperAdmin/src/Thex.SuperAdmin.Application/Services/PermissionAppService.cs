﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Thex.Common;
using Thex.Dto;
using Thex.SuperAdmin.Application.Adapters;
using Thex.SuperAdmin.Application.Interfaces;
using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Dto.Dto;
using Thex.SuperAdmin.Dto.Enumerations;
using Thex.SuperAdmin.Infra.Entities;
using Thex.SuperAdmin.Infra.Interfaces;
using Thex.SuperAdmin.Infra.Interfaces.ReadRepositories;
using Tnf.Notifications;

namespace Thex.SuperAdmin.Application.Services
{
    public class PermissionAppService : ApplicationServiceBase, IPermissionAppService
    {
        private readonly IIdentityPermissionAdapter _permissionAdapter;
        private readonly IIdentityPermissionRepository _permissionRepository;
        private readonly IIdentityPermissionReadRepository _permissionReadRepository;
        private readonly IIdentityMenuFeatureReadRepository _identityMenuFeatureReadRepository;

        public PermissionAppService(
           IIdentityPermissionAdapter permissionAdapter,
           IIdentityPermissionRepository permissionRepository,
           IIdentityPermissionReadRepository permissionReadRepository,
           IIdentityMenuFeatureReadRepository identityMenuFeatureReadRepository,
           INotificationHandler notificationHandler,
           ISimpleUnitOfWork simpleUnitOfWork)
           : base(simpleUnitOfWork, notificationHandler)
        {
            _permissionAdapter = permissionAdapter;
            _permissionRepository = permissionRepository;
            _permissionReadRepository = permissionReadRepository;
            _identityMenuFeatureReadRepository = identityMenuFeatureReadRepository;
        }

        public async Task<ThexListDto<IdentityPermissionDto>> GetAllByFilterAsync(GetAllIdentityPermissionDto dto)
        {
            dto.PageSize = 10000;

            return await _permissionReadRepository.GetAllDtoByFilterAsync(dto);

        }


        public async Task<IdentityPermissionDto> GetAsync(Guid id)
        {
            var permission = await _permissionReadRepository.GetDtoAsync(id);
            if (permission != null)
            {
                var publicFeature = permission.IdentityPermissionFeatureList.Select(e => e.IdentityFeature).FirstOrDefault(e => !e.IsInternal);

                if (publicFeature != null)
                    permission.ParentMenuList = await _identityMenuFeatureReadRepository.GetMenuItemWithParents(publicFeature.Id);
            }

            return permission;
        }

        public async Task<IdentityPermissionDto> CreateAsync(IdentityPermissionDto dto)
        {
            ValidateDto(dto, nameof(dto));

            if (!Enum.IsDefined(typeof(IdentityProductEnum), dto.IdentityProductId) ||
                !Enum.IsDefined(typeof(IdentityModuleEnum), dto.IdentityModuleId))
                NotifyParameterInvalid();

            if (Notification.HasNotification())
                return IdentityPermissionDto.NullInstance;

            var permission = _permissionAdapter.Map(dto).Build();

            if (Notification.HasNotification())
                return null;

            using (var trans = _permissionReadRepository.Context.Database.BeginTransaction())
            {
                dto.Id = (await _permissionRepository.InsertAndSaveChangesAsync(permission)).Id;

                trans.Commit();
            }

            return dto;
        }

        public async Task<IdentityPermissionDto> UpdateAsync(Guid id, IdentityPermissionDto dto)
        {
            if (Notification.HasNotification())
                return null;

            dto.Id = id;

            if (!Enum.IsDefined(typeof(IdentityProductEnum), dto.IdentityProductId) ||
                !Enum.IsDefined(typeof(IdentityModuleEnum), dto.IdentityModuleId))
                NotifyParameterInvalid();

            var entity = await _permissionReadRepository.GetById(dto.Id, true);

            if (entity == null)
            {
                NotifyNullParameter();
                return null;
            }

            var permission = _permissionAdapter.Map(dto).Build();

            if (Notification.HasNotification())
                return null;

            using (var trans = _permissionReadRepository.Context.Database.BeginTransaction())
            {
                await _permissionRepository.UpdateAndSaveChangesAsync(permission, entity);

                trans.Commit();
            }
            return dto;
        }

        public async Task DeleteAsync(Guid id)
        {
            if (id == Guid.Empty)
                NotifyIdIsMissing();

            if (!ValidateId(id)) return;

            using (var trans = _permissionReadRepository.Context.Database.BeginTransaction())
            {
                await _permissionRepository.RemoveAsync(id);

                if (Notification.HasNotification())
                    return;

                trans.Commit();
            }

        }

        public void ToggleActivation(Guid id)
        {
            if (id == Guid.Empty)
            {
                NotifyIdIsMissing();
                return;
            }

            using (var trans = _permissionReadRepository.Context.Database.BeginTransaction())
            {
                _permissionRepository.ToggleAndSaveIsActive(id);

                if (Notification.HasNotification())
                    return;

                trans.Commit();

            }
        }

        public async Task<ThexListDto<IdentityPermissionuserDto>> GetAllByUserIdAsync(GetAllIdentityPermissionDto dto, bool isUserId)
        {
            dto.PageSize = 10000;

            var listItemPermission = new List<IdentityPermissionuserDto>();

            var result = await _permissionReadRepository.GetAllDtoByFilterAsync(dto, isUserId);

            var roles = result.Items.FirstOrDefault(x => x.Role != null).Role;

            var list = new IdentityPermissionuserDto
            {
                PermissionList = new List<IdentityPermissionDto>()
            };

            list.Permission = roles;
            list.PermissionId = result.Items.FirstOrDefault(x => x.Role == roles).RoleId;
            list.PermissionList = result.Items.Where(x => x.Role == roles).DistinctBy(c => c.Name).OrderBy(o => o.Name).ToList();

            listItemPermission.Add(list);

            return new ThexListDto<IdentityPermissionuserDto>
            {
                Items = listItemPermission,
                HasNext = result.HasNext,
                TotalItems = result.TotalItems
            };
        }
    }
}
