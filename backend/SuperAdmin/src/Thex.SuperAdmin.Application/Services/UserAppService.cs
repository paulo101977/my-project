﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Collections.Generic;
using Thex.Common.Enumerations;
using System.Threading.Tasks;
using Thex.Common;
using Newtonsoft.Json.Linq;
using Thex.Common.Message;
using Microsoft.Extensions.Configuration;
using Tnf.Dto;
using Tnf.Notifications;
using Tnf.Localization;
using Thex.SuperAdmin.Application.Interfaces;
using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Infra.Interfaces.ReadRepositories;
using Thex.SuperAdmin.Dto.Email;
using Thex.SuperAdmin.Infra.Entities;
using Thex.SuperAdmin.Infra.Interfaces;
using Thex.SuperAdmin.Infra.Factories;
using Microsoft.AspNetCore.Identity;
using System.Linq;
using Thex.AspNetCore.Security;
using ThexSecurity = Thex.AspNetCore.Security;
using Thex.AspNetCore.Security.Interfaces;
using Thex.Common.Emails.AwsSes;
using Microsoft.Extensions.Caching.Distributed;
using Thex.AspNetCore.Security.SecurityCache.Repository;
using Thex.SuperAdmin.Dto.Enumerations;
using Thex.Location.Infra.Interfaces.Read;
using Thex.Kernel;
using Thex.Kernel.Dto;
using Thex.SuperAdmin.Application.Adapters;
using Thex.Storage;
using Tnf.Repositories.Uow;
using Tnf.Repositories.Uow;

namespace Thex.SuperAdmin.Application.Services
{
    public class UserAppService : ApplicationServiceBase, IUserAppService
    {
        private IApplicationUser _applicationUser;
        private readonly ITokenManager _tokenManager;
        private readonly IUserReadRepository _userReadRepository;
        private readonly IConfiguration _configuration;
        private readonly ITenantReadRepository _tenantReadRepository;
        private readonly ILocalizationManager _localizationManager;
        private readonly IUserRepository _userRepository;
        private readonly IEmailFactory _emailFactory;
        private readonly IPropertyReadRepository _propertyReadRepository;
        private readonly IPropertyParameterReadRepository _propertyParameterReadRepository;
        private readonly IUserInvitationAppService _userInvitationAppService;
        private SignInManager<User> _signManager;
        private UserManager<User> _userManager;
        private readonly IRefreshTokenCacheRepository _cache;
        private readonly ITokenConfiguration _tokenConfigurations;
        private readonly IUserTenantAppService _userTenantAppService;
        private readonly IPersonAppService _personAppService;
        private readonly IUserRoleAppService _userRoleAppService;
        private readonly ILocationReadRepository _locationReadRepository;
        private readonly IPersonRepository _personRepository;
        private readonly IPersonReadRepository _personReadRepository;
        private readonly IUnitOfWorkManager _unitOfWork;
        private readonly IAzureStorage _azureStorage;
        private readonly IPersonAdapter _personAdapter;
        private readonly IUserTenantReadRepository _userTenantReadRepository;
        private readonly IUnitOfWorkManager _unitOfWorkManager;

        public UserAppService(
            IAzureStorage azureStorage,
            IApplicationUser applicationUser,
            ITokenManager tokenManager,
            IUserReadRepository userReadRepository,
            IConfiguration configuration,
            INotificationHandler notificationHandler,
            ITenantReadRepository tenantReadRepository,
            IPersonRepository personRepository,
            IPersonAdapter personAdapter,
            IPropertyReadRepository propertyReadRepository,
            IPropertyParameterReadRepository propertyParameterReadRepository,
            ILocalizationManager localizationManager,
            IEmailFactory emailFactory,
            IUserRepository userRepository,
            SignInManager<User> signManager,
            UserManager<User> userManager,
            IPersonAppService personAppService,
            IUserInvitationAppService userInvitationAppService,
            IUserTenantAppService userTenantAppService,
            ISimpleUnitOfWork simpleUnitOfWork,
            IRefreshTokenCacheRepository cache,
            ITokenConfiguration tokenConfigurations,
            ILocationReadRepository locationReadRepository,
            IUserRoleAppService userRoleAppService,
            IUserTenantReadRepository userTenantReadRepository,
            IPersonReadRepository personReadRepository,
            IUnitOfWorkManager unitOfWorkManager
            )
            : base(simpleUnitOfWork, notificationHandler)
        {
            _applicationUser = applicationUser;
            _tokenManager = tokenManager;
            _userReadRepository = userReadRepository;
            _configuration = configuration;
            _tenantReadRepository = tenantReadRepository;
            _localizationManager = localizationManager;
            _userRepository = userRepository;
            _emailFactory = emailFactory;
            _propertyReadRepository = propertyReadRepository;
            _propertyParameterReadRepository = propertyParameterReadRepository;
            _signManager = signManager;
            _userManager = userManager;
            _personAppService = personAppService;
            _userInvitationAppService = userInvitationAppService;
            _cache = cache;
            _tokenConfigurations = tokenConfigurations;
            _userTenantAppService = userTenantAppService;
            _userRoleAppService = userRoleAppService;
            _userTenantReadRepository = userTenantReadRepository;
            _locationReadRepository = locationReadRepository;
            _personRepository = personRepository;
            _personReadRepository = personReadRepository;
            _unitOfWork = unitOfWorkManager;
            _azureStorage = azureStorage;
            _personAdapter = personAdapter;
            _personReadRepository = personReadRepository;
            _unitOfWorkManager = unitOfWorkManager;
        }

        #region Get by Logged User Id

        public virtual async Task<UserDto> GetByLoggedUserIdAsync()
            => await _userReadRepository.GetByLoggedUserIdAsync();

        #endregion

        #region Get All by Logged Tenant

        public virtual async Task<IListDto<UserDto>> GetAllByLoggedTenantIdAsync(GetAllUserDto request)
        {
            ValidateRequestAllDto(request, nameof(request));

            if (Notification.HasNotification())
                return null;

            var response = await _userReadRepository.GetAllByLoggedTenantIdAsync(request);
            return response;
        }

        #endregion

        #region Login and RefreshToken

        public ThexSecurity.UserTokenDto Login(LoginDto login)
        {
            bool validCredentials = false;
            var userDto = new UserDto();

            if (login == null)
            {
                NotifyNullParameter();
                return null;
            }

            if (!Enum.IsDefined(typeof(IdentityProductEnum), login.ProductId))
            {
                NotifyParameterInvalid();
                return null;
            }

            switch (login.GrantType)
            {
                case "password":
                    if (login != null && !String.IsNullOrWhiteSpace(login.Login) && !string.IsNullOrWhiteSpace(login.Password))
                        validCredentials = ValidateUser(login.Login, login.Password, out userDto);
                    break;
                case "refresh_token":
                    if (login != null && !string.IsNullOrEmpty(login.RefreshToken) && login.UserId != Guid.Empty)
                        validCredentials = ValidateUserByRefreshToken(login.UserId, login.RefreshToken, out userDto);
                    break;
                default:
                    NotifyNullParameter();
                    return null;
            }

            // TODO: Create a SuperAdmin validation
            if (validCredentials && login.ProductId != (int)IdentityProductEnum.SuperAdmin)
                validCredentials = _userReadRepository.HasPermissionByFilters(userDto.Id, login.ProductId);

            return GenerateToken(validCredentials, userDto, login.ProductId);
        }

        public ThexSecurity.UserTokenDto LoginTotvs(LoginDto login)
        {
            var domainList = _configuration
                                .GetSection("SuperAdminLoginDomains")
                                .AsEnumerable()
                                .Where(e => e.Value != null)
                                .Select(e => e.Value)
                                .ToArray();

            if (!login.Login.ContainsAny(domainList))
            {
                Notification.Raise(Notification
                    .DefaultBuilder
                    .WithMessage(AppConsts.LocalizationSourceName, UserEnum.Error.SuperAdminUserNoAuthorizedAccess)
                    .Build());

                return null;
            }

            return Login(login);
        }

        public ThexSecurity.UserTokenDto LoginByPropertyId(int propertyId)
        {
            bool validCredentials = false;
            var userDto = new UserDto();

            var property = _propertyReadRepository.GetByPropertyIdWithTenant(propertyId);

            validCredentials = ValidatePropertyByTenantId(property, out userDto);

            return GeneratePropertyToken(validCredentials, userDto, property);
        }

        private ThexSecurity.UserTokenDto GenerateToken(bool validCredentials, UserDto userDto, int productId)
        {
            if (validCredentials)
            {
                CountryLanguageDto countryLanguageDto = null;
                string propertyCountryCode = string.Empty;

                userDto.PropertyList = _propertyReadRepository.GetAllDtoByUserId(userDto.Id, productId);

                if (userDto.PropertyId.HasValue)
                {
                    countryLanguageDto = _propertyReadRepository.GetPropertyCulture(userDto.PropertyId.Value);
                    var dto = _locationReadRepository.GetCountryAndDistrictCodeByPropertyId(userDto.PropertyId.Value);
                    propertyCountryCode = dto.PropertyCountryCode;
                }

                var tokenInfo = new TokenInfo()
                {
                    LoggedUserEmail = userDto.Email,
                    LoggedUserName = userDto.Name,
                    LoggedUserUid = userDto.Id,
                    TenantId = userDto.TenantId,
                    PropertyId = userDto.PropertyId,
                    ChainId = userDto.ChainId,
                    BrandId = userDto.BrandId,
                    IsAdmin = userDto.IsAdmin,
                    PreferredCulture = userDto.PreferredCulture,
                    PreferredLanguage = userDto.PreferredLanguage,
                    PropertyCulture = countryLanguageDto != null ? countryLanguageDto.CultureInfoCode.ToLower() : null,
                    PropertyLanguage = countryLanguageDto != null ? countryLanguageDto.CultureInfoCode.ToLower() : null,
                    TimeZoneName = userDto != null &&
                                        userDto.PropertyId.HasValue &&
                                        userDto.PropertyId.Value != 0 ? _propertyParameterReadRepository.GetTimeZoneByPropertyId(userDto.PropertyId.Value) : null,
                    PropertyCountryCode = propertyCountryCode,
                    PropertyList = userDto.PropertyList
                };
                var identity = _tokenManager.CreateClaimIdentity(userDto.Id, tokenInfo);

                var userToken = _tokenManager.CreateUserToken(identity);

                CreateTokenCache(userDto, userToken);

                return userToken;
            }
            else
                return null;
        }

        private ThexSecurity.UserTokenDto GeneratePropertyToken(bool validCredentials, UserDto userDto, Property property)
        {
            if (validCredentials && property != null)
            {
                CountryLanguageDto countryLanguageDto = null;
                string propertyCountryCode = string.Empty;
                string propertyDistrictCode = string.Empty;

                if (userDto.PropertyId.HasValue)
                {
                    countryLanguageDto = _propertyReadRepository.GetPropertyCulture(userDto.PropertyId.Value);
                    var dto = _locationReadRepository.GetCountryAndDistrictCodeByPropertyId(userDto.PropertyId.Value);
                    propertyCountryCode = dto.PropertyCountryCode;
                    propertyDistrictCode = dto.PropertyDistrictCode;
                }

                var tokenInfo = new TokenInfo()
                {
                    LoggedUserEmail = userDto.Email,
                    LoggedUserName = userDto.Name,
                    LoggedUserUid = userDto.Id,
                    TenantId = property.TenantId,
                    PropertyId = property.Tenant?.PropertyId,
                    PropertyUid = property.PropertyUId,
                    ChainId = property.Tenant?.ChainId,
                    BrandId = property.Tenant?.BrandId,
                    IsAdmin = userDto.IsAdmin,
                    PreferredCulture = userDto.PreferredCulture,
                    PreferredLanguage = userDto.PreferredLanguage,
                    PropertyCulture = countryLanguageDto != null ? countryLanguageDto.CultureInfoCode.ToLower() : null,
                    PropertyLanguage = countryLanguageDto != null ? countryLanguageDto.CultureInfoCode.ToLower() : null,
                    TimeZoneName = property.Tenant != null &&
                                    property.Tenant.PropertyId.HasValue &&
                                    property.Tenant.PropertyId.Value != 0 ? _propertyParameterReadRepository.GetTimeZoneByPropertyId(property.Tenant.PropertyId.Value) : null,
                    PropertyCountryCode = propertyCountryCode,
                    PropertyDistrictCode = propertyDistrictCode,
                };
                var identity = _tokenManager.CreateClaimIdentity(_applicationUser.UserUid, tokenInfo);

                var userToken = _tokenManager.CreateUserToken(identity);

                CreateTokenCache(userDto, userToken);

                return userToken;
            }

            return null;
        }

        private void CreateTokenCache(UserDto userDto, ThexSecurity.UserTokenDto userToken)
        {
            if (userToken != null && userToken.RefreshToken != null)
                _cache.Set(new RefreshTokenData
                {
                    RefreshToken = userToken.RefreshToken,
                    UserId = userDto.Id,
                    PropertyId = userDto.PropertyId,
                    PropertyUid = userDto.PropertyUId,
                    BrandId = userDto.BrandId,
                    ChainId = userDto.ChainId,
                    TenantId = userDto.TenantId,
                    PropertyList = userDto.PropertyList
                }, _tokenConfigurations.FinalExpiration);
        }

        private bool ValidatePropertyByTenantId(Property property, out UserDto userDto)
        {
            userDto = null;

            var tenantIdList = _tenantReadRepository.GetIdListByLoggedUserId();

            if (property != null && tenantIdList.Contains(property.TenantId))
            {
                userDto = _userReadRepository.GetUserById(_applicationUser.UserUid);

                if (property.TenantId != null && userDto != null)
                {
                    userDto.PropertyUId = property.PropertyUId;
                    userDto.PropertyId = property.Tenant.PropertyId;
                    userDto.BrandId = property.Tenant.BrandId;
                    userDto.ChainId = property.Tenant.ChainId;
                    userDto.TenantId = property.TenantId;
                }
            }

            return userDto != null;
        }

        private bool ValidateUserByRefreshToken(Guid userId, string refreshToken, out UserDto userDto)
        {
            userDto = null;

            var token = _cache.GetById(refreshToken);

            if (token != null && token.UserId == userId)
            {
                userDto = _userReadRepository.GetUserById(userId);
                userDto.PropertyId = token.PropertyId;
                userDto.BrandId = token.BrandId;
                userDto.ChainId = token.ChainId;
                userDto.TenantId = token.TenantId;

                _cache.RemoveById(refreshToken);
            }

            return userDto != null;
        }

        private bool ValidateUser(string login, string password, out UserDto userDto)
        {
            userDto = null;
            var result = _signManager.PasswordSignInAsync(login, password, false, lockoutOnFailure: true).GetAwaiter().GetResult();

            if (result.Succeeded)
                userDto = _userReadRepository.GetUserByEmail(login);

            //TODO: send email to the user stating it was blocked
            if (result.IsLockedOut)
                NotifyUserLocked();

            return userDto != null;
        }

        #endregion

        #region Forgot Password

        public void ForgotPassword(string email, string frontUrl = null)
        {
            AnyUserByEmail(email);

            if (Notification.HasNotification()) return;

            frontUrl = frontUrl ?? _configuration.GetValue<string>("UrlFront");

            string link = _tokenManager.CreateForgotPasswordLink(frontUrl, email);

            SendMailForgotPassword(email, link);
        }

        public void ForgotPasswordTotvs(string email)
        {
            string superAdmfrontUrl = _configuration.GetValue<string>("UrlFrontSuperAdmin");

            ForgotPassword(email, superAdmfrontUrl);
        }

        private void SendMailForgotPassword(string email, string link)
        {
            var emailDto = new EmailForgotPasswordDto()
            {
                Title = _localizationManager.GetString(AppConsts.LocalizationSourceName, EmailEnum.EmailSubjectForgotPassword.ToString()),
                Body = _localizationManager.GetString(AppConsts.LocalizationSourceName, EmailEnum.EmailBodyForgotPassword.ToString()),
                UrlForgotPassword = link
            };

            var host = _configuration.GetValue<string>("AwsEmailHost");
            var port = Convert.ToInt32(_configuration.GetValue<string>("AwsEmailPort"));
            var userName = _configuration.GetValue<string>("AwsEmailUserName");
            var passsword = _configuration.GetValue<string>("AwsEmailPassword");

            var subject = emailDto.Title;
            var from = _configuration.GetValue<string>("EmailNoReply");
            var body = _emailFactory.GenerateEmail(EmailTemplateEnum.EmailForgotPassword, emailDto);
            var to = email;


            // Prepare Email Message
            var message = EmailMessageBuilder
                            .Init()
                            .AddSubject(subject)
                            .AddFrom(from)
                            .AddBody(body)
                            .AddTo(to)
                            .Build();

            // Send Email Message
            IAwsEmailSender sender = new AwsEmailSender(new AwsEmailSettings(host, port, userName, passsword));
            sender.Send(message, true);
        }

        private void AnyUserByEmail(string email)
        {
            var anyUser = _userReadRepository.AnyUserByEmail(email);
            if (!anyUser)
            {
                Notification.Raise(Notification.DefaultBuilder
                  .WithMessage(AppConsts.LocalizationSourceName, User.EntityError.UserNotFound)
                  .WithDetailedMessage(AppConsts.LocalizationSourceName, User.EntityError.UserNotFound)
                  .Build());
            }
        }

        #endregion

        #region Toggle Activation

        public void ToggleActivationByLoggedPropertyId(Guid userId, int propertyId)
        {
            _userTenantAppService.ToggleActivation(userId, propertyId);
        }

        #endregion

        #region Change Culture

        public void ChangeCulture(Guid id, string culture)
        {
            if (id == Guid.Empty)
            {
                NotifyIdIsMissing();
                return;
            }

            _userRepository.ChangeCulture(id, culture);

            Commit();
        }

        #endregion

        #region New Password

        public async Task NewPassword(string token, string password)
        {
            try
            {
                var validToken = _tokenManager.VerifyToken(token);

                if (validToken != null)
                {
                    object email = null;

                    validToken.Payload.TryGetValue(JwtRegisteredClaimNames.Email, out email);

                    string strEmail = (email as string);

                    var user = await _userManager.FindByEmailAsync(strEmail);

                    if (user != null)
                    {
                        await NewPassword(user, password);

                        Commit();
                    }
                    else
                        NotifyInvalidUser();
                }
                else
                    NotifyInvalidToken();
            }
            catch (Exception e)
            {
                NotifyInvalidToken();
            }
        }

        #endregion

        #region ChangePassword

        public async Task ChangePassword(string token, string oldPassword, string newPassword)
        {
            var validToken = _tokenManager.VerifyToken(token);
            if (validToken != null)
            {
                JArray uniqueName = null;

                validToken.Payload.TryGetValue("unique_name", out uniqueName);
                Guid userId = Guid.Parse(uniqueName[0].ToString());

                UserDto userDto = _userReadRepository.GetAndValidateUser(userId, oldPassword);

                if (userDto != null)
                {
                    var user = await _userManager.FindByEmailAsync(userDto.Email);
                    await NewPassword(user, newPassword);

                    Commit();
                }
                else
                {
                    NotifyInvalidUserOrPassword();
                }
            }
            else
            {
                NotifyInvalidToken();
            }
        }

        public async Task<UserDto> CreateNewUserPassword(string token, string password)
        {
            User user = null;

            try
            {
                var validToken = _tokenManager.VerifyToken(token);
                if (validToken != null)
                {
                    string email;
                    string tenantId;
                    validToken.Payload.TryGetValue("email", out email);
                    validToken.Payload.TryGetValue("TenantId", out tenantId);

                    user = await _userManager.FindByEmailAsync(email);
                    if (user != null)
                    {
                        user.Unlock();

                        await NewPassword(user, password);

                        Commit();
                    }
                }
            }
            catch (Exception e)
            {
                Notification.Raise(Notification.DefaultBuilder
                        .WithMessage(AppConsts.LocalizationSourceName, UserEnum.Error.CreateNewUser)
                        .WithDetailedMessage(e.InnerException.Message)
                        .Build());
            }

            return new UserDto
            {
                UserName = user.UserName,
                Name = user.Name,
                Email = user.Email,
                PersonId = user.PersonId.Value,
                PreferredCulture = user.PreferredCulture,
                PreferredLanguage = user.PreferredLanguage,
                IsAdmin = user.IsAdmin,
                IsActive = user.IsActive,
                TenantId = user.TenantId ?? Guid.Empty
            };
        }

        public async Task ChangePasswordPersonAsync(ChangePwdPersonDto dto)
        {

            var userDto = _userReadRepository.GetAndValidateUser(dto.UserId, dto.OldPassword);

            if (userDto != null)
            {
                var user = _userManager.FindByEmailAsync(userDto.Email).Result;
                await NewPassword(user, dto.NewPassword);

                Commit();
            }
            else
            {
                NotifyInvalidUserOrPassword();
            }

        }

        #endregion

        #region New User

        public virtual async Task<UserDto> CreateNewUser(UserInvitationDto invitationDto, List<Guid> roleIdList)
        {
            var userDto = _userReadRepository.GetUserByEmail(invitationDto.Email);
            var tenant = GetCurrentTenantAndSetInvitationDto(invitationDto);

            var isNewUser = userDto == null;

            if (!isNewUser)
                ValidateUser(userDto);

            if (Notification.HasNotification()) return UserDto.NullInstance;

            using (var trans = _propertyReadRepository.Context.Database.BeginTransaction())
            {
                userDto = userDto ?? await CreateNewUser(invitationDto, Guid.NewGuid(), tenant.Id);

                if (Notification.HasNotification())
                    return UserDto.NullInstance;

                CreateUserLink(invitationDto, userDto);

                if (Notification.HasNotification())
                    return UserDto.NullInstance;

                userDto.LastLink = invitationDto.InvitationLink;

                await _userInvitationAppService.Create(invitationDto, tenant.Id, userDto.Id);

                await _userTenantAppService.AssociateUserToLoggedTenant(userDto.Id, invitationDto.IsActive);

                if (roleIdList != null)
                    await _userRoleAppService.CreateorUpdateAsync(userDto.Id, roleIdList, invitationDto.PropertyId);

                trans.Commit();
            }

            if (isNewUser && !Notification.HasNotification())
                _userInvitationAppService.SendMailNewUser(userDto.Email, userDto.LastLink);

            return userDto;
        }

        private void ValidateUser(UserDto userDto)
        {
            var domainList = _configuration
                              .GetSection("SuperAdminLoginDomains")
                              .AsEnumerable()
                              .Where(e => e.Value != null)
                              .Select(e => e.Value)
                              .ToArray();

            userDto.ChainId = _userTenantReadRepository.GetChainIdByUserId(userDto.Id);
            var chainId = int.Parse(_applicationUser.ChainId);
            if ((userDto.ChainId != default(int) && userDto.ChainId != chainId) && !userDto.Email.ContainsAny(domainList))
            {
                Notification.Raise(Notification
                  .DefaultBuilder
                  .WithMessage(AppConsts.LocalizationSourceName, User.EntityError.UserAlreadyAssociatedToAnotherChain)
                  .WithDetailedMessage(AppConsts.LocalizationSourceName, User.EntityError.UserAlreadyAssociatedToAnotherChain)
                  .Build());
            }
        }

        private Tenant GetCurrentTenantAndSetInvitationDto(UserInvitationDto invitationDto)
        {
            var tenant = _tenantReadRepository.GetCurrentTenant();

            invitationDto.BrandId = tenant.BrandId;
            invitationDto.ChainId = tenant.ChainId;
            invitationDto.PropertyId = tenant.PropertyId;

            return tenant;
        }

        private void CreateUserLink(UserInvitationDto invitationDto, UserDto userDto)
        {
            string frontUrl = _configuration.GetValue<string>("UrlFront");

            invitationDto.InvitationLink = _tokenManager.CreateUserLink(frontUrl, userDto.Email, userDto.Name, userDto.Id);
        }

        private async Task<UserDto> CreateNewUser(UserInvitationDto dto, Guid userId, Guid tenant)
        {
            User userDto = null;

            try
            {
                if (!string.IsNullOrEmpty(dto.Email) && !string.IsNullOrWhiteSpace(dto.Email) && !string.IsNullOrEmpty(dto.Name) && !string.IsNullOrWhiteSpace(dto.Name))
                {
                    // Creating Person as contact binded to User
                    Guid personId = Guid.NewGuid();

                    var contactInformationList = new HashSet<ContactInformationDto>();
                    var contactEmail = new ContactInformationDto()
                    {
                        ContactInformationTypeId = (int)ContactInformationTypeEnum.Email,
                        Information = dto.Email,
                        OwnerId = personId
                    };
                    contactInformationList.Add(contactEmail);

                    var nameList = SetFirstAndLastNameBasedOnFullName(dto.Name);

                    var firstName = nameList.First();

                    var lastName = nameList.Last();

                    PersonDto personDto = new ContactPersonDto(firstName, lastName)
                    {
                        Id = personId,
                        PersonType = "C",
                        ContactInformationList = contactInformationList,
                        NickName = dto.NickName,
                        DateOfBirth = dto.DateOfBirth

                    };
                    personDto = _personAppService.Create(personDto);

                    if (personDto != null && !Notification.HasNotification())
                    {
                        // Creating User
                        userDto = new User()
                        {
                            Id = userId,
                            Name = dto.Name,
                            Email = dto.Email,
                            PersonId = personId,
                            PreferredCulture = dto.PreferredCulture,
                            PreferredLanguage = dto.PreferredLanguage,
                            IsAdmin = dto.IsAdmin,
                            IsActive = true,
                            TenantId = tenant,
                            UserName = dto.Email,
                            CreationTime = DateTime.Now,
                            CreatorUserId = _applicationUser.UserUid,
                            LastModificationTime = DateTime.Now,
                            LastModifierUserId = _applicationUser.UserUid,
                            PhoneNumber = dto.PhoneNumber
                        };
                        var result = await _userManager.CreateAsync(userDto, GenerateRandomPassword());

                        NotifyIdentityErros(result);
                    }
                }
            }
            catch (Exception e)
            {
                Notification.Raise(Notification.DefaultBuilder
                        .WithMessage(AppConsts.LocalizationSourceName, UserEnum.Error.CreateNewUser)
                        .WithDetailedMessage(e.InnerException.Message)
                        .Build());
            }

            return new UserDto
            {
                Id = userId,
                UserName = userDto.UserName,
                Name = userDto.Name,
                Email = userDto.Email,
                PersonId = userDto.PersonId.Value,
                PreferredCulture = userDto.PreferredCulture,
                PreferredLanguage = userDto.PreferredLanguage,
                IsAdmin = userDto.IsAdmin,
                IsActive = userDto.IsActive,
                TenantId = userDto.TenantId ?? Guid.Empty
            };
        }

        private string GenerateRandomPassword(PasswordOptions opts = null)
        {
            if (opts == null) opts = new PasswordOptions()
            {
                RequiredLength = 8,
                RequiredUniqueChars = 4,
                RequireDigit = true,
                RequireLowercase = true,
                RequireNonAlphanumeric = true,
                RequireUppercase = true
            };

            string[] randomChars = new[] {
                "ABCDEFGHJKLMNOPQRSTUVWXYZ",    // uppercase 
                "abcdefghijkmnopqrstuvwxyz",    // lowercase
                "0123456789",                   // digits
                "!@$?_-"                        // non-alphanumeric
            };
            Random rand = new Random(Environment.TickCount);
            List<char> chars = new List<char>();

            if (opts.RequireUppercase)
                chars.Insert(rand.Next(0, chars.Count),
                    randomChars[0][rand.Next(0, randomChars[0].Length)]);

            if (opts.RequireLowercase)
                chars.Insert(rand.Next(0, chars.Count),
                    randomChars[1][rand.Next(0, randomChars[1].Length)]);

            if (opts.RequireDigit)
                chars.Insert(rand.Next(0, chars.Count),
                    randomChars[2][rand.Next(0, randomChars[2].Length)]);

            if (opts.RequireNonAlphanumeric)
                chars.Insert(rand.Next(0, chars.Count),
                    randomChars[3][rand.Next(0, randomChars[3].Length)]);

            for (int i = chars.Count; i < opts.RequiredLength
                || chars.Distinct().Count() < opts.RequiredUniqueChars; i++)
            {
                string rcs = randomChars[rand.Next(0, randomChars.Length)];
                chars.Insert(rand.Next(0, chars.Count),
                    rcs[rand.Next(0, rcs.Length)]);
            }

            return new string(chars.ToArray());
        }

        #endregion

        #region Shared Methods

        private async Task NewPassword(User user, string password)
        {
            if ((user == null) || string.IsNullOrEmpty(password))
            {
                NotifyInvalidUserOrPassword();
            }

            var newPassword = _userManager.PasswordHasher.HashPassword(user, password);
            user.PasswordHash = newPassword;
            var res = await _userManager.UpdateAsync(user);
        }

        private void NotifyInvalidUserOrPassword()
        {
            Notification.Raise(Notification
                .DefaultBuilder
                .WithMessage(AppConsts.LocalizationSourceName, UserEnum.Error.InvalidUserOrPassword)
                .Build());
        }

        private void NotifyInvalidToken()
        {
            Notification.Raise(Notification
                .DefaultBuilder
                .WithMessage(AppConsts.LocalizationSourceName, UserEnum.Error.InvalidToken)
                .Build());
        }

        private void NotifyInvalidUser()
        {
            Notification.Raise(Notification.DefaultBuilder
            .WithMessage(AppConsts.LocalizationSourceName, UserEnum.Error.InvalidUser)
                                    .WithDetailedMessage(AppConsts.LocalizationSourceName, UserEnum.Error.InvalidUser)
                                    .Build());
        }

        #endregion

        public async Task<string> NewInvitationLinkAsync(Guid id)
        {
            if (id == Guid.Empty)
            {
                NotifyNullParameter();
                return null;
            }

            var userDto = _userReadRepository.GetUserById(id);

            if (userDto == null)
            {
                NotifyInvalidUser();
                return null;
            }

            var newLink = _tokenManager.CreateUserLink(_configuration.GetValue<string>("UrlFront"), userDto.Email, userDto.Name, userDto.Id);

            await _userInvitationAppService.UpdateInvitationLinkAsync(userDto.Id, newLink);

            if (Notification.HasNotification())
                return null;

            return newLink;
        }

        public async Task UpdateUserAsync(UpdateUserDto dto)
        {
            try
            {
                ValidateUpdateDto(dto);

                if (Notification.HasNotification()) return;

                using (var trans = _unitOfWorkManager.Begin())
                {
                    var user = await _userReadRepository.GetByIdWithoutIncludeAsync(dto.Id);

                    if (user != null)
                    {
                        var person = _personReadRepository.Get(user.PersonId.GetValueOrDefault());

                        var personDto = GeneratePersonDto(dto);

                        if (person != null)
                            await _personAppService.UpdateProfilePersonAsync(person, personDto);

                       await ChangeUsers(dto, user);

                        if (dto.RoleIdList != null && dto.RoleIdList.Any())
                            await _userRoleAppService.CreateorUpdateAsync(user.Id, dto.RoleIdList, int.Parse(_applicationUser.PropertyId));
                    }

                    trans.Complete();
                }
            }
            catch (Exception e)
            {
                Notification.Raise(Notification.DefaultBuilder
                        .WithMessage(AppConsts.LocalizationSourceName, User.EntityError.UserUpdateError)
                        .WithDetailedMessage(e.InnerException.Message)
                        .Build());
            }
        }

        private void ValidateUpdateDto(UpdateUserDto dto)
        {
            ValidateDto(dto, nameof(dto));

            if (Notification.HasNotification()) return;

            if (dto == null)
                Notification.Raise(Notification
                    .DefaultBuilder
                    .WithMessage(AppConsts.LocalizationSourceName, CommonsEnum.Error.NullOrEmptyObject)
                    .Build());

        }

        private async Task ChangeUsers(UpdateUserDto dto, User user)
        {
            try
            {
                if (user != null)
                {
                    user.PhoneNumber = dto.PhoneNumber;
                    user.Name = dto.Name;

                  await  _userManager.UpdateAsync(user);
                }

            }
            catch (Exception e)
            {
                Notification.Raise(Notification.DefaultBuilder
                       .WithMessage(AppConsts.LocalizationSourceName, User.EntityError.UserUpdateError)
                       .WithDetailedMessage(e.InnerException.Message)
                       .Build());
            }
        }

        public async Task<UserDto> ChangePhoto(Guid id, UserDto dto)
        {
            var _bytes = Convert.FromBase64String(dto.PhotoBase64);
            var url = await _azureStorage.SendAsync(id, id.ToString(), id.ToString(), _bytes);

            if (Notification.HasNotification())
                return null;

            using (var uow = _unitOfWork.Begin())
            {
                var userDto = _userReadRepository.GetUserById(id);
                if (userDto == null)
                {
                    NotifyNullParameter();
                    return null;
                }

                var entity = _personReadRepository.Get(userDto.PersonId);
                if (entity == null)
                {
                    NotifyNullParameter();
                    return null;
                }

                var person = _personAdapter.MapUpdatePhotoUrlPerson(entity, url).Build();

                if (Notification.HasNotification())
                    return null;

                await _personRepository.UpdateAsync(person);

                if (Notification.HasNotification())
                    return null;

                await uow.CompleteAsync();
            }

            return new UserDto() { PhotoUrl = url };
        }


        private PersonDto GeneratePersonDto(UpdateUserDto dto)
        {
            var names = dto.Name.Split(new char[] { ' ' }, 2);
            var firstName = names.FirstOrDefault();
            var lastName = names.FirstOrDefault().Equals(names.LastOrDefault()) ? string.Empty : names.LastOrDefault();
            return new ContactPersonDto(firstName, lastName)
            {
                NickName = dto.NickName,
                DateOfBirth = dto.DateOfBirth
            };
        }

        private List<string> SetFirstAndLastNameBasedOnFullName(string fullName)
        {
            var stringList = new List<string>();

            if (!string.IsNullOrEmpty(fullName))
            {
                var separator = " ";

                var fullNameList = fullName.Split(separator).ToList();

                var firstName = fullNameList.First();

                stringList.Add(firstName);

                if (fullNameList.Count() > 1)
                {
                    fullNameList.RemoveAt(0);

                    var lastName = fullNameList.JoinAsString(separator);

                    stringList.Add(lastName);
                }
            }

            return stringList;
        }
    }
}
