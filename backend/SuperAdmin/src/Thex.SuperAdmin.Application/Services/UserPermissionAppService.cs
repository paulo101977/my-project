﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Thex.Common;
using Thex.Dto;
using Thex.Kernel;
using Thex.SuperAdmin.Application.Adapters;
using Thex.SuperAdmin.Application.Interfaces;
using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Dto.Dto;
using Thex.SuperAdmin.Dto.Enumerations;
using Thex.SuperAdmin.Infra.Entities;
using Thex.SuperAdmin.Infra.Interfaces;
using Thex.SuperAdmin.Infra.Interfaces.ReadRepositories;
using Thex.SuperAdmin.Infra.Interfaces.Services;
using Tnf.Dto;
using Tnf.Notifications;

namespace Thex.SuperAdmin.Application.Services
{
    public class UserPermissionAppService : ApplicationServiceBase, IUserPermissionAppService
    {
        private readonly IMenuAppService _menuAppService;
        private readonly IUserRolePermissionReadRepository _userRolePermissionReadRepository;
        private readonly IApplicationUser _applicationUser;

        public UserPermissionAppService(
           IMenuAppService menuAppService,
           IUserRolePermissionReadRepository userRolePermissionReadRepository,
           INotificationHandler notificationHandler,
           IApplicationUser applicationUser,
           ISimpleUnitOfWork simpleUnitOfWork)
           : base(simpleUnitOfWork, notificationHandler)
        {
            _menuAppService = menuAppService;
            _applicationUser = applicationUser;
            _userRolePermissionReadRepository = userRolePermissionReadRepository;
        }

        public async Task<UserProductPermissionWithMenuDto> GetPermissionByProperty(int propertyId)
        {
            var productPermission = await _userRolePermissionReadRepository.GetAllUserKeyByProduct(propertyId);

            return new UserProductPermissionWithMenuDto
            {
                PermissionList = GetMenuHierarchyAsync(productPermission)
            };
        }


        public async Task<UserPermissionWithMenuDto> GetAllWithMenu(int productId, bool withMenu)
        {
            int.TryParse(_applicationUser.PropertyId, out int propertyId);

            if (!Enum.IsDefined(typeof(IdentityProductEnum), productId) || propertyId < 0)
                NotifyParameterInvalid();

            var propertyIds = new List<int?>();

            if (propertyId != 0)
                propertyIds.Add(propertyId);
            else
                propertyIds.AddRange(_applicationUser.PropertyList.Select(x => (int?)int.Parse(x.PropertyId)).ToList());

            var userKeyList = await _userRolePermissionReadRepository.GetAllUserKey(productId, propertyIds);
            var publicKeyList = userKeyList.Where(e => !e.Key).Select(e => e.Value).ToList();

            return new UserPermissionWithMenuDto
            {
                KeyList = userKeyList.Select(e => e.Value).ToList(),
                MenuList = withMenu ? await GetMenuHierarchyAsync(productId, publicKeyList) : null
            };
        }

        public async Task<bool> VerifyUserPermission(Guid userId, List<string> permissions)
        {
            if (permissions == null || permissions.Count == 0 || userId == Guid.Empty)
                return false;

            return await _userRolePermissionReadRepository.AnyKeyAsync(userId, permissions);
        }

        /// <summary>
        /// V2
        /// </summary>
        /// <param name="productId"></param>
        /// <param name="publicKeyList"></param>
        /// <returns></returns>
        private async Task<List<MenuItemSimpleDto>> GetMenuHierarchyAsync(int productId, List<string> publicKeyList)
        {
            if (publicKeyList == null || publicKeyList.Count == 0)
                return null;

            var menuList = await _menuAppService.GetHierarchyByPermissionListAsync(new GetAllIdentityMenuFeatureDto { ProductId = productId }, publicKeyList);

            var result = new List<MenuItemSimpleDto>();
            foreach (var menu in menuList.Items)
                result.Add(menu.MapTo<MenuItemSimpleDto>());

            return result.OrderBy(e => e.OrderNumber).ToList();
        }

        /// <summary>
        /// V2
        /// </summary>
        /// <param name="productPermissionList"></param>
        /// <returns></returns>
        private List<ProductPermissionDto> GetMenuHierarchyAsync(List<ProductPermissionDto> productPermissionList)
        {
            var result = new List<ProductPermissionDto>();

            if (productPermissionList == null || productPermissionList.Count == 0)
                return null;

            foreach (var permision in productPermissionList)
            {
                var menuList = _menuAppService.GetHierarchyByProductPropertyPermissionListAsync(new GetAllIdentityMenuFeatureDto { ProductId = permision.ProductId }, permision.MenuList.Select(p => p.Key).ToList()).Result;

                if (menuList != null)
                {
                    permision.MenuList.Clear();

                    foreach (var menu in menuList?.Items)
                        permision.MenuList.Add(menu.MapTo<MenuItemSimpleDto>());
                }
            }

            return productPermissionList.ToList();
        }

        public async Task<bool> VerifyExternalUserPermission(List<string> permissions)
        {
            if (permissions == null || permissions.Count == 0)
                return false;

            return await _userRolePermissionReadRepository.KeyIsEnabledForExternalAccess(permissions);
        }
    }
}
