﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Thex.Common;
using Thex.SuperAdmin.Application.Adapters;
using Thex.SuperAdmin.Application.Interfaces;
using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Dto.Dto;
using Thex.SuperAdmin.Infra.Entities;
using Thex.SuperAdmin.Infra.Interfaces;
using Thex.SuperAdmin.Infra.Interfaces.ReadRepositories;
using Tnf.Notifications;

namespace Thex.SuperAdmin.Application.Services
{
    public class BrandAppService : ApplicationServiceBase, IBrandAppService
    {
        private readonly IBrandReadRepository _brandReadRepository;
        private readonly IBrandRepository _brandRepository;
        private readonly IBrandAdapter _brandAdapter;
        private readonly ITenantReadRepository _tenantReadRepository;
        private readonly ITenantRepository _tenantRepository;

        public BrandAppService(
           IBrandReadRepository brandReadRepository,
           IBrandRepository brandRepository,
           IBrandAdapter brandAdapter,
           ITenantReadRepository tenantReadRepository,
           ITenantRepository tenantRepository,
           INotificationHandler notificationHandler,
           ISimpleUnitOfWork simpleUnitOfWork)
           : base(simpleUnitOfWork, notificationHandler)
        {
            _brandReadRepository = brandReadRepository;
            _brandRepository = brandRepository;
            _brandAdapter = brandAdapter;
            _tenantReadRepository = tenantReadRepository;
            _tenantRepository = tenantRepository;
        }

        public async Task<ThexListDto<BrandDto>> GetAllByFilterAsync(GetAllBrandDto dto)
        {
            var brands = await _brandReadRepository.GetAllDtoByFilterAsync(dto);
            return brands;
        }

        public async Task<BrandDto> GetAsync(int id)
        {
            var brandDto = await  _brandReadRepository.GetDtoByIdAsync(id);
            if(brandDto == null)
            {
                NotifyNullParameter();
                return null;
            }

            return brandDto;
        }

        public async Task<BrandDto> CreateAsync(BrandDto dto)
        {
            await ValidateBrand(dto);
            if (Notification.HasNotification())
                return null;           
            
            using (var trans = _brandReadRepository.Context.Database.BeginTransaction())
            {
                dto.TenantId = _tenantRepository.GenerateTenantAutomaticallyAndSaveChanges($"{dto.Name} Brand");

                if (Notification.HasNotification())
                    return null;

                var brand = _brandAdapter.Map(dto).Build();
                if (Notification.HasNotification())
                    return null;

                dto.Id = (await _brandRepository.InsertAndSaveChangesAsync(brand)).Id;

                _tenantRepository.SetBrand(dto.TenantId, dto.Id, dto.ChainId);

                trans.Commit();
            }
            return dto;
        }

        public async Task<BrandDto> UpdateAsync(int id, BrandDto dto)
        {
            await ValidateBrand(dto);
            if (Notification.HasNotification())
                return null;

            dto.Id = id;

            var entity = await _brandReadRepository.GetById(dto.Id);
            if(entity == null)
            {
                NotifyNullParameter();
                return null;
            }

            var brand = _brandAdapter.Map(entity, dto).Build();
            if (Notification.HasNotification())
                return null;

            await _brandRepository.UpdateAndSaveChangesAsync(brand);

            return dto;
        }

        public virtual BrandDto FirstOrDefault()
            => _brandReadRepository.FirstOrDefault();

        #region Private methods
        private async Task ValidateBrand(BrandDto dto)
        {
            if (dto == null)
                NotifyNullParameter();

            var nameIsAvailable = await _brandReadRepository.NameIsAvailable(dto.Name);
            if(!nameIsAvailable)
            {
                Notification.Raise(Notification
              .DefaultBuilder
              .WithMessage(AppConsts.LocalizationSourceName, Brand.EntityError.AlreadyExistsBrandWithSameName)
              .Build());
            }
        }        
        #endregion
    }
}
