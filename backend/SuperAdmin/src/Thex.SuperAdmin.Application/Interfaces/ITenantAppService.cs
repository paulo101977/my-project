﻿using System;
using System.Collections.Generic;
using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Dto.Dto;
using Tnf.Dto;

namespace Thex.SuperAdmin.Application.Interfaces
{
    public interface ITenantAppService
    {
        TenantDto Create(TenantDto dto);
        TenantDto Update(TenantDto dto, Guid id);
        TenantDto Get(Guid id);
        ThexListDto<TenantDto> GetAllByFilter(GetAllTenantDto dto);
        IListDto<TenantDto> GetAllWithoutAssociation(Guid? tenantId);
    }
}
