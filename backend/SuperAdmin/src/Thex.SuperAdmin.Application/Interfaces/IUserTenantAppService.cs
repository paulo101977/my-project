﻿using System;
using System.Threading.Tasks;
using Thex.SuperAdmin.Dto.Dto;
using Thex.SuperAdmin.Infra.Entities;

namespace Thex.SuperAdmin.Application.Interfaces
{
    public interface IUserTenantAppService
    {
        Task AssociateUserTenant(CreateOrUpdateUserDto dto);
        Task UpdateAssociationUserTenant(CreateOrUpdateUserDto dto, User user);
        Task AssociateUserToLoggedTenant(Guid userId, bool isActive = false);
        void ToggleActivation(Guid userId, int propertyId);
    }
}
