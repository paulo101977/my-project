﻿using System.Threading.Tasks;
using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Dto.Dto;

namespace Thex.SuperAdmin.Application.Interfaces
{
    public interface IIdentityProductAppService
    {
        Task<ThexListDto<IdentityProductDto>> GetAllAsync();
    }
}
