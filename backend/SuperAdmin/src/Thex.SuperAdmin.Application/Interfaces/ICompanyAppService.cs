﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.Common.Dto;
using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Dto.Dto;

namespace Thex.SuperAdmin.Application.Interfaces
{
    public interface ICompanyAppService
    {
        Task<ThexListDto<CompanyDto>> GetAllByFilterAsync(GetAllCompanyDto request);
        Task<CompanyDto> GetAsync(int id);
        Task<ThexListDto<LocationDto>> GetLocationsFromCompanyAsync(int id);
        Task<CompanyDto> CreateAsync(CompanyDto dto);
        Task<CompanyDto> UpdateAsync(int id, CompanyDto dto);
    }
}
