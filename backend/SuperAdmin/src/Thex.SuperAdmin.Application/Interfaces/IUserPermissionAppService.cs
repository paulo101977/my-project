﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Infra.Entities;

namespace Thex.SuperAdmin.Application.Interfaces
{
    public interface IUserPermissionAppService
    {
        Task<UserPermissionWithMenuDto> GetAllWithMenu(int productId, bool withMenu);
        Task<UserProductPermissionWithMenuDto> GetPermissionByProperty(int propertyId);
        Task<bool> VerifyUserPermission(Guid userId, List<string> permissions);
        Task<bool> VerifyExternalUserPermission(List<string> permissions);
    }
}
