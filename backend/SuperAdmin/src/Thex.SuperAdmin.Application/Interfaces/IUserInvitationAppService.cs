﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Dto.Dto;

namespace Thex.SuperAdmin.Application.Interfaces
{
    public interface IUserInvitationAppService
    {
        void ToggleActivation(Guid id);
        void InvitationAccepted(Guid id);
        void SendMailNewUser(string email, string link);
        Task<UserInvitationDto> Create(UserInvitationDto dto, Guid tenantId, Guid userId);
        Task UpdateInvitationLinkAsync(Guid userId, string newLink);
        Task CreateAsync(CreateOrUpdateUserDto userDto, List<PropertyIdAndTenantDto> dtoList);
    }
}
