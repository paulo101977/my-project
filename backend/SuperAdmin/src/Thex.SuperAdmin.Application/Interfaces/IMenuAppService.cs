﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.Dto;
using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Dto.Dto;
using Tnf.Dto;

namespace Thex.SuperAdmin.Application.Interfaces
{
    public interface IMenuAppService
    {
        Task<IdentityMenuFeatureDto> GetAsync(Guid id);
        Task<IdentityMenuFeatureDto> CreateAsync(IdentityMenuFeatureDto dto);
        Task<IdentityMenuFeatureDto> UpdateAsync(Guid id, IdentityMenuFeatureDto dto);
        void ToggleActivation(Guid id);
        Task<ListDto<MenuItemDto>> GetHierarchyAsync(GetAllIdentityMenuFeatureDto dto);
        Task<ListDto<MenuItemDto>> GetHierarchyByPermissionListAsync(GetAllIdentityMenuFeatureDto dto, List<string> permissionKeyList);
        Task<ListDto<MenuItemDto>> GetHierarchyByProductPropertyPermissionListAsync(GetAllIdentityMenuFeatureDto dto, List<string> permissionKeyList);
    }
}
