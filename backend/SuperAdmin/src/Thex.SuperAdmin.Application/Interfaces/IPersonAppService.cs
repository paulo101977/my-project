﻿using System.Threading.Tasks;
using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Infra.Entities;

namespace Thex.SuperAdmin.Application.Interfaces
{
    public interface IPersonAppService
    {
        PersonDto Create(PersonDto dto);
        PersonDto UpdateAsync(Person entity, PersonDto dto);
        Task UpdateProfilePersonAsync(Person entity, PersonDto personDto);
    }
}
