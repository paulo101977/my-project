﻿using System;
using System.Threading.Tasks;
using Thex.Dto;
using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Dto.Dto;

namespace Thex.SuperAdmin.Application.Interfaces
{
    public interface IFeatureAppService
    {
        Task<ThexListDto<IdentityFeatureDto>> GetAllByFilterAsync(GetAllIdentityFeatureDto dto);
        Task<IdentityFeatureDto> GetAsync(Guid id);
        Task<IdentityFeatureDto> CreateAsync(IdentityFeatureDto dto);
        Task<IdentityFeatureDto> UpdateAsync(Guid id, IdentityFeatureDto dto);
        Task DeleteAsync(Guid id);
    }
}
