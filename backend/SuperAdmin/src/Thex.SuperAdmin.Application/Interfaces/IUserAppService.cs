﻿using System;
using System.Threading.Tasks;
using ThexSecurity = Thex.AspNetCore.Security;
using Thex.SuperAdmin.Dto;
using Tnf.Application.Services;
using Tnf.Dto;
using Thex.SuperAdmin.Dto.Dto;
using System.Collections.Generic;

namespace Thex.SuperAdmin.Application.Interfaces
{
    public interface IUserAppService : IApplicationService
    {
        Task<UserDto> GetByLoggedUserIdAsync();
        Task<IListDto<UserDto>> GetAllByLoggedTenantIdAsync(GetAllUserDto request);
        ThexSecurity.UserTokenDto Login(LoginDto login);
        ThexSecurity.UserTokenDto LoginByPropertyId(int propertyId);
        ThexSecurity.UserTokenDto LoginTotvs(LoginDto login);
        void ForgotPassword(string email, string frontUrl = null);
        void ForgotPasswordTotvs(string email);
        void ToggleActivationByLoggedPropertyId(Guid userId, int propertyId);
        void ChangeCulture(Guid id, string culture);
        Task NewPassword(string token, string password);
        Task ChangePassword(string token, string oldPassword, string newPassword);
        Task<UserDto> CreateNewUser(UserInvitationDto dto, List<Guid> roleIdList);
        Task<UserDto> CreateNewUserPassword(string token, string password);
        Task<string> NewInvitationLinkAsync(Guid id);
        Task<UserDto> ChangePhoto(Guid id, UserDto dto);
        Task ChangePasswordPersonAsync(ChangePwdPersonDto dto);
        Task UpdateUserAsync(UpdateUserDto dto);
    }
}
