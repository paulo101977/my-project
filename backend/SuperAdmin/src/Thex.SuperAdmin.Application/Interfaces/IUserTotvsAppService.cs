﻿using System;
using System.Threading.Tasks;
using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Dto.Dto;

namespace Thex.SuperAdmin.Application.Interfaces
{
    public interface IUserTotvsAppService
    {
        Task<CreateOrUpdateUserDto> CreateAsync(CreateOrUpdateUserDto dto);
        Task<ThexListDto<UsersDto>> GetAllAsync(GetAllUserDto request);
        Task<UsersDto> GetAsync(Guid id);
        Task ToggleIsActiveAsync(Guid id);
        Task<CreateOrUpdateUserDto> UpdateAsync(CreateOrUpdateUserDto dto, Guid id);
    }
}
