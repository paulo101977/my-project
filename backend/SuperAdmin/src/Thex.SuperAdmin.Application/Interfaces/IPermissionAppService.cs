﻿using System;
using System.Threading.Tasks;
using Thex.Dto;
using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Dto.Dto;

namespace Thex.SuperAdmin.Application.Interfaces
{
    public interface IPermissionAppService
    {
        Task<ThexListDto<IdentityPermissionDto>> GetAllByFilterAsync(GetAllIdentityPermissionDto dto);
        Task<IdentityPermissionDto> GetAsync(Guid id);
        Task<IdentityPermissionDto> CreateAsync(IdentityPermissionDto dto);
        Task<IdentityPermissionDto> UpdateAsync(Guid id, IdentityPermissionDto dto);
        Task DeleteAsync(Guid id);
        void ToggleActivation(Guid id);
        Task<ThexListDto<IdentityPermissionuserDto>> GetAllByUserIdAsync(GetAllIdentityPermissionDto dto, bool isUserId);
    }
}
