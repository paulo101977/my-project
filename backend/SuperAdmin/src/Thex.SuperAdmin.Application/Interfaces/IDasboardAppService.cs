﻿using System.Threading.Tasks;
using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Dto.Dto;

namespace Thex.SuperAdmin.Application.Interfaces
{
    public interface IDashboardAppService
    {
        Task<ThexListDto<DashboardPropertyDto>> GetAllPropertiesAsync();
        Task NextPropertyStatusAsync(int propertyId);
        Task ToggleBlockPropertyAsync(int propertyId);
    }
}
