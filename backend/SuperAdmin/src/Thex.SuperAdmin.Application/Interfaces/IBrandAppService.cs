﻿using System.Threading.Tasks;
using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Dto.Dto;

namespace Thex.SuperAdmin.Application.Interfaces
{
    public interface IBrandAppService
    {
        Task<ThexListDto<BrandDto>> GetAllByFilterAsync(GetAllBrandDto dto);
        Task<BrandDto> GetAsync(int id);
        Task<BrandDto> CreateAsync(BrandDto dto);
        Task<BrandDto> UpdateAsync(int id, BrandDto dto);
        BrandDto FirstOrDefault();
    }
}
