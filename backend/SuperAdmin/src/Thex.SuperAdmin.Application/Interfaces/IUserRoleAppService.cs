﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.SuperAdmin.Infra.Entities;

namespace Thex.SuperAdmin.Application.Interfaces
{
    public interface IUserRoleAppService
    {
        Task CreateorUpdateAsync(Guid userId, List<Guid> roleIdList, int? propertyId = null, int? brandId = null, int? chainId = null);
        Task CreateAsync(Guid userId, Guid roleId, List<int> propertyIdListDto);
    }
}
