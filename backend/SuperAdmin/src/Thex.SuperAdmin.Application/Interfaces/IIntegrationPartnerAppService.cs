﻿using Thex.SuperAdmin.Dto.Dto;
using Thex.AspNetCore.Security;
using System.Threading.Tasks;

namespace Thex.SuperAdmin.Application.Interfaces
{
    public interface IIntegrationPartnerAppService
    {
        Task<IntegrationPartnerTokenDto> AuthenticateAndGenerateToken(AuthenticateIntegrationPartnerDto authenticateIntegrationPartnerDto);
        Task<ThexListDto<PartnerDto>> GetAllPartnersAsync();
    }
}
