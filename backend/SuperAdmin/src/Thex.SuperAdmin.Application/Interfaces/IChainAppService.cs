﻿using System.Threading.Tasks;
using Thex.SuperAdmin.Dto;
using Tnf.Dto;

namespace Thex.SuperAdmin.Application.Interfaces
{
    public interface IChainAppService
    {
        Task<IListDto<ChainDto>> GetAll(GetAllChainDto request);
        Task<ChainDto> Get(DefaultIntRequestDto id);
        Task<ChainDto> Create(ChainDto dto);
        Task<ChainDto> Update(int id, ChainDto dto);
    }
}
