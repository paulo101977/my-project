﻿using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;
using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Dto.Dto;
using Tnf.Dto;

namespace Thex.SuperAdmin.Application.Interfaces
{
    public interface IPropertyAppService
    {
        Task<CreateOrUpdatePropertyDto> CreateAsync(CreateOrUpdatePropertyDto dto, IFormFile file);
        Task<ThexListDto<PropertyDto>> GetAll(GetAllPropertyDto request);
        Task<PropertyDto> Get(DefaultIntRequestDto id);
        Task<CreateOrUpdatePropertyDto> UpdateAsync(int id, CreateOrUpdatePropertyDto dto, IFormFile file);
    }
}
