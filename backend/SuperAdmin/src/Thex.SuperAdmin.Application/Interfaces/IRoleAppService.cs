﻿using System;
using System.Threading.Tasks;
using Thex.Dto;
using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Dto.Dto;

namespace Thex.SuperAdmin.Application.Interfaces
{
    public interface IRolesAppService
    {
        Task<ThexListDto<RolesDto>> GetAllByFilterAsync(GetAllRoleDto dto);
        Task<RolesDto> GetAsync(Guid id);
        Task<RolesDto> CreateAsync(RolesDto dto);
        Task<RolesDto> UpdateAsync(Guid id, RolesDto dto);
        Task DeleteAsync(Guid id);
        void ToggleActivation(Guid id);
    }
}
