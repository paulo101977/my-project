﻿using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Infra.Entities;

namespace Thex.SuperAdmin.Application.Adapters
{
    public interface IApplicationParameterAdapter
    {
        ApplicationParameter.Builder Map(ApplicationParameter entity, ApplicationParameterDto dto);
        ApplicationParameter.Builder Map(ApplicationParameterDto dto);
    }
}
