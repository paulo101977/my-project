﻿using System;
using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Infra.Entities;

namespace Thex.SuperAdmin.Application.Adapters
{
    public interface IUserInvitationAdapter
    {
        UserInvitation.Builder Map(UserInvitation entity, UserInvitationDto dto);
        UserInvitation.Builder Map(UserInvitationDto dto);
        UserInvitation.Builder Map(Guid userId, int propertyId, Guid tenantId, string email, string name, string link);
    }
}
