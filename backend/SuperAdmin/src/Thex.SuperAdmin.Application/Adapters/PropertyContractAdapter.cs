﻿using System;
using Thex.SuperAdmin.Dto.Dto;
using Thex.SuperAdmin.Infra.Entities;
using Tnf;
using Tnf.Notifications;

namespace Thex.SuperAdmin.Application.Adapters
{
    public class PropertyContractAdapter : IPropertyContractAdapter
    {
        public INotificationHandler NotificationHandler { get; }

        public PropertyContractAdapter(INotificationHandler notificationHandler)
        {
            NotificationHandler = notificationHandler;
        }

        public PropertyContract.Builder Map(PropertyContractDto dto)
        {
            Check.NotNull(dto, nameof(dto));

            var builder = new PropertyContract.Builder(NotificationHandler)
                .WithId(Guid.NewGuid())
                .WithPropertyId(dto.PropertyId)
                .WithMaxUh(dto.MaxUh);

            return builder;
        }

        public PropertyContract.Builder MapToUpdate(PropertyContract entity, PropertyContractDto dto)
        {
            Check.NotNull(entity, nameof(entity));
            Check.NotNull(dto, nameof(dto));

            var builder = new PropertyContract.Builder(NotificationHandler)
              .WithId(entity.Id)
              .WithPropertyId(entity.PropertyId)
              .WithMaxUh(dto.MaxUh);

            return builder;
        }
    }
}
