﻿using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Infra.Entities;

namespace Thex.SuperAdmin.Application.Adapters
{
    public interface IIdentityModuleAdapter
    {
        IdentityModule.Builder Map(IdentityModule entity, IdentityModuleDto dto);
        IdentityModule.Builder Map(IdentityModuleDto dto);
    }
}
