﻿using System;
using Thex.SuperAdmin.Dto.Dto;
using Thex.SuperAdmin.Infra.Entities;
using Tnf;
using Tnf.Notifications;

namespace Thex.SuperAdmin.Application.Adapters
{
    public class DocumentAdapter : IDocumentAdapter
    {
        public INotificationHandler NotificationHandler { get; }

        public DocumentAdapter(INotificationHandler notificationHandler)
        {
            NotificationHandler = notificationHandler;
        }

        public Document.Builder Map(Document entity, DocumentDto dto)
        {
            Check.NotNull(entity, nameof(entity));
            Check.NotNull(dto, nameof(dto));

            var builder = new Document.Builder(NotificationHandler, entity)
                .WithId(entity.Id)
                .WithOwnerId(entity.OwnerId)
                .WithDocumentTypeId(dto.DocumentTypeId)
                .WithDocumentInformation(dto.DocumentInformation);

            return builder;
        }

        public Document.Builder Map(DocumentDto dto)
        {
            Check.NotNull(dto, nameof(dto));

            var builder = new Document.Builder(NotificationHandler)
                .WithId(dto.Id)
                .WithOwnerId(dto.OwnerId)
                .WithDocumentTypeId(dto.DocumentTypeId)
                .WithDocumentInformation(dto.DocumentInformation);

            return builder;
        }
    }
}
