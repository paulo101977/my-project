﻿using System;
using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Infra.Entities;

namespace Thex.SuperAdmin.Application.Adapters
{
    public interface IIdentityRolePermissionAdapter
    {
        IdentityRolePermission.Builder Map(IdentityRolePermission entity, IdentityRolePermissionDto dto);
        IdentityRolePermission.Builder Map(IdentityRolePermissionDto dto, Guid roleId);
    }
}
