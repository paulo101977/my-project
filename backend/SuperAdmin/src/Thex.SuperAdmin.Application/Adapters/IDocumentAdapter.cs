﻿using Thex.SuperAdmin.Dto.Dto;
using Thex.SuperAdmin.Infra.Entities;

namespace Thex.SuperAdmin.Application.Adapters
{
    public interface IDocumentAdapter
    {
        Document.Builder Map(Document entity, DocumentDto dto);
        Document.Builder Map(DocumentDto dto);
    }
}
