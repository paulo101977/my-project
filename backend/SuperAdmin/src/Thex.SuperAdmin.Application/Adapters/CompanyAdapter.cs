﻿using System;
using System.Collections.Generic;
using System.Text;
using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Infra.Entities;
using Tnf;
using Tnf.Notifications;

namespace Thex.SuperAdmin.Application.Adapters
{
    public class CompanyAdapter : ICompanyAdapter
    {
        public INotificationHandler NotificationHandler { get; }

        public CompanyAdapter(INotificationHandler notificationHandler)
        {
            NotificationHandler = notificationHandler;
        }

        public virtual Company.Builder Map(Company entity, CompanyDto dto)
        {
            Check.NotNull(entity, nameof(entity));
            Check.NotNull(dto, nameof(dto));

            var builder = new Company.Builder(NotificationHandler, entity)
                .WithId(entity.Id)
                .WithPersonId(entity.PersonId)
                .WithShortName(dto.ShortName)
                .WithTradeName(dto.TradeName);

            return builder;
        }

        public virtual Company.Builder Map(CompanyDto dto)
        {
            Check.NotNull(dto, nameof(dto));

            var builder = new Company.Builder(NotificationHandler)                
                .WithId(dto.Id)
                .WithPersonId(dto.PersonId)
                .WithShortName(dto.ShortName)
                .WithTradeName(dto.TradeName);

            return builder;
        }
    }
}
