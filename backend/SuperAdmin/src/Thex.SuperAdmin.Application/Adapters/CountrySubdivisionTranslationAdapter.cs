﻿using System;
using System.Collections.Generic;
using System.Text;
using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Infra.Entities;
using Tnf;
using Tnf.Notifications;

namespace Thex.SuperAdmin.Application.Adapters
{
    public class CountrySubdivisionTranslationAdapter : ICountrySubdivisionTranslationAdapter
    {
        public INotificationHandler NotificationHandler { get; }

        public CountrySubdivisionTranslationAdapter(INotificationHandler notificationHandler)
        {
            NotificationHandler = notificationHandler;
        }

        public virtual CountrySubdivisionTranslation.Builder Map(CountrySubdivisionTranslation entity, CountrySubdivisionTranslationDto dto)
        {
            Check.NotNull(entity, nameof(entity));
            Check.NotNull(dto, nameof(dto));

            var builder = new CountrySubdivisionTranslation.Builder(NotificationHandler, entity)
                .WithId(dto.Id)
                .WithLanguageIsoCode(dto.LanguageIsoCode)
                .WithName(dto.Name)
                .WithCountrySubdivisionId(dto.CountrySubdivisionId)
                .WithNationality(dto.Nationality);

            return builder;
        }

        public virtual CountrySubdivisionTranslation.Builder Map(CountrySubdivisionTranslationDto dto)
        {
            Check.NotNull(dto, nameof(dto));

            var builder = new CountrySubdivisionTranslation.Builder(NotificationHandler)
                .WithId(dto.Id)
                .WithLanguageIsoCode(dto.LanguageIsoCode)
                .WithName(dto.Name)
                .WithCountrySubdivisionId(dto.CountrySubdivisionId)
                .WithNationality(dto.Nationality);

            return builder;
        }
    }
}
