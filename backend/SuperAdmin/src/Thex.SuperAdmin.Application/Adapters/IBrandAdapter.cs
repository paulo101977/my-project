﻿using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Infra.Entities;

namespace Thex.SuperAdmin.Application.Adapters
{
    public interface IBrandAdapter
    {
        Brand.Builder Map(Brand entity, BrandDto dto);
        Brand.Builder Map(BrandDto dto);
    }
}
