﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a Crudzilla tool.
// </auto-generated>
//------------------------------------------------------------------------------

using Thex.SuperAdmin.Infra.Entities;
using Thex.SuperAdmin.Dto;

namespace Thex.SuperAdmin.Application.Adapters
{
    public interface IUserLoginAdapter
    {
        UserLogin.Builder Map(UserLogin entity, UserLoginDto dto);
        UserLogin.Builder Map(UserLoginDto dto);
    }
}
