﻿using System;
using System.Collections.Generic;
using System.Text;
using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Infra.Entities;
using Tnf;
using Tnf.Notifications;

namespace Thex.SuperAdmin.Application.Adapters
{
    public class StatusCategoryAdapter : IStatusCategoryAdapter
    {
        public INotificationHandler NotificationHandler { get; }

        public StatusCategoryAdapter(INotificationHandler notificationHandler)
        {
            NotificationHandler = notificationHandler;
        }

        public virtual StatusCategory.Builder Map(StatusCategory entity, StatusCategoryDto dto)
        {
            Check.NotNull(entity, nameof(entity));
            Check.NotNull(dto, nameof(dto));

            var builder = new StatusCategory.Builder(NotificationHandler, entity)
                .WithId(dto.Id)
                .WithName(dto.Name);

            return builder;
        }

        public virtual StatusCategory.Builder Map(StatusCategoryDto dto)
        {
            Check.NotNull(dto, nameof(dto));

            var builder = new StatusCategory.Builder(NotificationHandler)
                .WithId(dto.Id)
                .WithName(dto.Name);

            return builder;
        }
    }
}
