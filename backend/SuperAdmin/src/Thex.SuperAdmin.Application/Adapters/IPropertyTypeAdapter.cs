﻿using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Infra.Entities;

namespace Thex.SuperAdmin.Application.Adapters
{
    public interface IPropertyTypeAdapter
    {
        PropertyType.Builder Map(PropertyType entity, PropertyTypeDto dto);
        PropertyType.Builder Map(PropertyTypeDto dto);
    }
}
