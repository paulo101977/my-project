﻿using System;
using System.Collections.Generic;
using System.Text;
using Thex.Common.Dto;
using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Infra.Entities;
using Tnf;
using Tnf.Notifications;

namespace Thex.SuperAdmin.Application.Adapters
{
    public class IdentityProductAdapter : IIdentityProductAdapter
    {
        public INotificationHandler NotificationHandler { get; }

        public IdentityProductAdapter(INotificationHandler notificationHandler)
        {
            NotificationHandler = notificationHandler;
        }

        public virtual IdentityProduct.Builder Map(IdentityProduct entity, IdentityProductDto dto)
        {
            Check.NotNull(entity, nameof(entity));
            Check.NotNull(dto, nameof(dto));

            var builder = new IdentityProduct.Builder(NotificationHandler, entity)
                .WithId(dto.Id)
                .WithName(dto.Name)
                .WithDescription(dto.Description)
                .WithIsDeleted(dto.IsDeleted)
                .WithCreationTime(dto.CreationTime)
                .WithCreatorUserId(dto.CreatorUserId)
                .WithLastModificationTime(dto.LastModificationTime)
                .WithLastModifierUserId(dto.LastModifierUserId)
                .WithDeletionTime(dto.DeletionTime)
                .WithDeleterUserId(dto.DeleterUserId);

            return builder;
        }

        public virtual IdentityProduct.Builder Map(IdentityProductDto dto)
        {
            Check.NotNull(dto, nameof(dto));

            var builder = new IdentityProduct.Builder(NotificationHandler)
                .WithId(dto.Id)
                .WithName(dto.Name)
                .WithDescription(dto.Description)
                .WithIsDeleted(dto.IsDeleted)
                .WithCreationTime(dto.CreationTime)
                .WithCreatorUserId(dto.CreatorUserId)
                .WithLastModificationTime(dto.LastModificationTime)
                .WithLastModifierUserId(dto.LastModifierUserId)
                .WithDeletionTime(dto.DeletionTime)
                .WithDeleterUserId(dto.DeleterUserId);

            return builder;
        }
    }
}
