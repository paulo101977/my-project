﻿using System;
using System.Collections.Generic;
using System.Text;
using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Infra.Entities;
using Tnf;
using Tnf.Notifications;

namespace Thex.SuperAdmin.Application.Adapters
{
    public class ContactInformationTypeAdapter : IContactInformationTypeAdapter
    {
        public INotificationHandler NotificationHandler { get; }

        public ContactInformationTypeAdapter(INotificationHandler notificationHandler)
        {
            NotificationHandler = notificationHandler;
        }

        public virtual ContactInformationType.Builder Map(ContactInformationType entity, ContactInformationTypeDto dto)
        {
            Check.NotNull(entity, nameof(entity));
            Check.NotNull(dto, nameof(dto));

            var builder = new ContactInformationType.Builder(NotificationHandler, entity)
                .WithId(dto.Id)
                .WithName(dto.Name)
                .WithStringFormatMask(dto.StringFormatMask)
                .WithRegexValidationExpression(dto.RegexValidationExpression);

            return builder;
        }

        public virtual ContactInformationType.Builder Map(ContactInformationTypeDto dto)
        {
            Check.NotNull(dto, nameof(dto));

            var builder = new ContactInformationType.Builder(NotificationHandler)
                .WithId(dto.Id)
                .WithName(dto.Name)
                .WithStringFormatMask(dto.StringFormatMask)
                .WithRegexValidationExpression(dto.RegexValidationExpression);

            return builder;
        }
    }
}
