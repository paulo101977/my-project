﻿using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Infra.Entities;

namespace Thex.SuperAdmin.Application.Adapters
{
    public interface ICompanyAdapter
    {
        Company.Builder Map(Company entity, CompanyDto dto);
        Company.Builder Map(CompanyDto dto);
    }
}
