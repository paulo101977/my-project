﻿using Thex.Common.Dto;
using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Infra.Entities;

namespace Thex.SuperAdmin.Application.Adapters
{
    public interface ILocationAdapter
    {
        Infra.Entities.Location.Builder Map(Infra.Entities.Location entity, LocationDto dto);
        Infra.Entities.Location.Builder Map(LocationDto dto);
    }
}
