﻿using System;
using System.Collections.Generic;
using System.Text;
using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Infra.Entities;
using Tnf;
using Tnf.Notifications;

namespace Thex.SuperAdmin.Application.Adapters
{
    public class ApplicationParameterAdapter : IApplicationParameterAdapter
    {
        public INotificationHandler NotificationHandler { get; }

        public ApplicationParameterAdapter(INotificationHandler notificationHandler)
        {
            NotificationHandler = notificationHandler;
        }

        public virtual ApplicationParameter.Builder Map(ApplicationParameter entity, ApplicationParameterDto dto)
        {
            Check.NotNull(entity, nameof(entity));
            Check.NotNull(dto, nameof(dto));

            var builder = new ApplicationParameter.Builder(NotificationHandler, entity)
                .WithId(dto.Id)
                .WithApplicationModuleId(dto.ApplicationModuleId)
                .WithFeatureGroupId(dto.FeatureGroupId)
                .WithParameterName(dto.ParameterName)
                .WithParameterDescription(dto.ParameterDescription)
                .WithParameterDefaultValue(dto.ParameterDefaultValue)
                .WithParameterTypeId(dto.ParameterTypeId)
                .WithParameterComponentType(dto.ParameterComponentType)
                .WithParameterMinValue(dto.ParameterMinValue)
                .WithParameterMaxValue(dto.ParameterMaxValue)
                .WithParameterPossibleValues(dto.ParameterPossibleValues)
                .WithIsNullable(dto.IsNullable);

            return builder;
        }

        public virtual ApplicationParameter.Builder Map(ApplicationParameterDto dto)
        {
            Check.NotNull(dto, nameof(dto));

            var builder = new ApplicationParameter.Builder(NotificationHandler)
                .WithId(dto.Id)
                .WithApplicationModuleId(dto.ApplicationModuleId)
                .WithFeatureGroupId(dto.FeatureGroupId)
                .WithParameterName(dto.ParameterName)
                .WithParameterDescription(dto.ParameterDescription)
                .WithParameterDefaultValue(dto.ParameterDefaultValue)
                .WithParameterTypeId(dto.ParameterTypeId)
                .WithParameterComponentType(dto.ParameterComponentType)
                .WithParameterMinValue(dto.ParameterMinValue)
                .WithParameterMaxValue(dto.ParameterMaxValue)
                .WithParameterPossibleValues(dto.ParameterPossibleValues)
                .WithIsNullable(dto.IsNullable);

            return builder;
        }
    }
}
