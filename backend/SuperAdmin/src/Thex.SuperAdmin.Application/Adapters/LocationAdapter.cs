﻿using System;
using System.Collections.Generic;
using System.Text;
using Thex.Common.Dto;
using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Infra.Entities;
using Tnf;
using Tnf.Notifications;

namespace Thex.SuperAdmin.Application.Adapters
{
    public class LocationAdapter : ILocationAdapter
    {
        public INotificationHandler NotificationHandler { get; }

        public LocationAdapter(INotificationHandler notificationHandler)
        {
            NotificationHandler = notificationHandler;
        }

        public virtual Infra.Entities.Location.Builder Map(Infra.Entities.Location entity, LocationDto dto)
        {
            Check.NotNull(entity, nameof(entity));
            Check.NotNull(dto, nameof(dto));

            var builder = new Infra.Entities.Location.Builder(NotificationHandler, entity)
                .WithId(dto.Id)
                .WithOwnerId(dto.OwnerId)
                .WithLocationCategoryId(dto.LocationCategoryId)
                .WithLatitude(dto.Latitude)
                .WithLongitude(dto.Longitude)
                .WithStreetName(dto.StreetName)
                .WithStreetNumber(dto.StreetNumber)
                .WithAdditionalAddressDetails(dto.AdditionalAddressDetails)
                .WithNeighborhood(dto.Neighborhood)
                .WithCityId(dto.CityId)
                .WithStateId(dto.StateId)
                .WithCountryId(dto.CountryId)
                .WithPostalCode(dto.PostalCode)
                .WithCountryCode(dto.CountryCode);

            return builder;
        }

        public virtual Infra.Entities.Location.Builder Map(LocationDto dto)
        {
            Check.NotNull(dto, nameof(dto));

            var builder = new Infra.Entities.Location.Builder(NotificationHandler)
                .WithId(dto.Id)
                .WithOwnerId(dto.OwnerId)
                .WithLocationCategoryId(dto.LocationCategoryId)
                .WithLatitude(dto.Latitude)
                .WithLongitude(dto.Longitude)
                .WithStreetName(dto.StreetName)
                .WithStreetNumber(dto.StreetNumber)
                .WithAdditionalAddressDetails(dto.AdditionalAddressDetails)
                .WithNeighborhood(dto.Neighborhood)
                .WithCityId(dto.CityId)
                .WithStateId(dto.StateId)
                .WithCountryId(dto.CountryId)
                .WithPostalCode(dto.PostalCode)
                .WithCountryCode(dto.CountryCode);

            return builder;
        }
    }
}
