﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Thex.Common.Dto;
using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Infra.Entities;
using Tnf;
using Tnf.Notifications;

namespace Thex.SuperAdmin.Application.Adapters
{
    public class IdentityPermissionAdapter : IIdentityPermissionAdapter
    {
        public INotificationHandler NotificationHandler { get; }
        public IIdentityPermissionFeatureAdapter IdentityPermissionFeatureAdapter { get; }
        

        public IdentityPermissionAdapter(
            INotificationHandler notificationHandler, 
            IIdentityPermissionFeatureAdapter identityPermissionFeatureAdapter)
        {
            NotificationHandler = notificationHandler;
            IdentityPermissionFeatureAdapter = identityPermissionFeatureAdapter;
        }

        public virtual IdentityPermission.Builder Map(IdentityPermission entity, IdentityPermissionDto dto)
        {
            Check.NotNull(entity, nameof(entity));
            Check.NotNull(dto, nameof(dto));

            var builder = new IdentityPermission.Builder(NotificationHandler, entity)
                .WithId(dto.Id)
                .WithIdentityProductId(dto.IdentityProductId)
                .WithIdentityModuleId(dto.IdentityModuleId)
                .WithName(dto.Name)
                .WithDescription(dto.Description)
                .WithIsActive(dto.IsActive)
                .WithPermissionFeatureList(CreateIdentityPermissionFeatureList(dto.Id, entity.IdentityPermissionFeatureList.ToList() ?? new List<IdentityPermissionFeature>(), dto.IdentityPermissionFeatureList));

            return builder;
        }

        public virtual IdentityPermission.Builder Map(IdentityPermissionDto dto)
        {
            Check.NotNull(dto, nameof(dto));
            var permissionId = Guid.NewGuid();

            var builder = new IdentityPermission.Builder(NotificationHandler)
                .WithId(dto.Id == Guid.Empty ? permissionId : dto.Id)
                .WithIdentityProductId(dto.IdentityProductId)
                .WithIdentityModuleId(dto.IdentityModuleId)
                .WithName(dto.Name)
                .WithDescription(dto.Description)
                .WithIsActive(dto.IsActive)
                .WithPermissionFeatureList(CreateIdentityPermissionFeatureList(permissionId, dto.IdentityPermissionFeatureList));

            return builder;
        }

        public List<IdentityPermissionFeature> CreateIdentityPermissionFeatureList(Guid permissionId, IList<IdentityPermissionFeatureDto> identityPermissionFeatureList)
        {
            var result = new List <IdentityPermissionFeature>();

            foreach (var identityPermissionFeatureDto in identityPermissionFeatureList)
            {
                identityPermissionFeatureDto.IdentityPermissionId = permissionId;
                result.Add(IdentityPermissionFeatureAdapter.Map(identityPermissionFeatureDto).Build());
            }
                

            return result;
        }

        public List<IdentityPermissionFeature> CreateIdentityPermissionFeatureList(Guid permissionId, List<IdentityPermissionFeature> oldIdentityPermissionFeatureList, IList<IdentityPermissionFeatureDto> identityPermissionFeatureList)
        {
            var result = new List<IdentityPermissionFeature>();

            foreach (var identityPermissionFeatureDto in identityPermissionFeatureList)
            {
                var oldItem = oldIdentityPermissionFeatureList.FirstOrDefault(e => e.Id == identityPermissionFeatureDto.Id);

                if(oldItem != null)
                {
                    identityPermissionFeatureDto.IdentityPermissionId = permissionId;
                    result.Add(IdentityPermissionFeatureAdapter.Map(oldItem, identityPermissionFeatureDto).Build());
                }
                else
                {
                    identityPermissionFeatureDto.IdentityPermissionId = permissionId;
                    result.Add(IdentityPermissionFeatureAdapter.Map(identityPermissionFeatureDto).Build());
                }
            }


            return result;
        }
    }
}
