﻿using System;
using System.Collections.Generic;
using System.Text;
using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Infra.Entities;
using Tnf;
using Tnf.Notifications;

namespace Thex.SuperAdmin.Application.Adapters
{
    public class CountryLanguageAdapter : ICountryLanguageAdapter
    {
        public INotificationHandler NotificationHandler { get; }

        public CountryLanguageAdapter(INotificationHandler notificationHandler)
        {
            NotificationHandler = notificationHandler;
        }

        public virtual CountryLanguage.Builder Map(CountryLanguage entity, CountryLanguageDto dto)
        {
            Check.NotNull(entity, nameof(entity));
            Check.NotNull(dto, nameof(dto));

            var builder = new CountryLanguage.Builder(NotificationHandler, entity)
                .WithId(dto.Id)
                .WithCountrySubdivisionId(dto.CountrySubdivisionId)
                .WithLanguage(dto.Language)
                .WithLanguageTwoLetterIsoCode(dto.LanguageTwoLetterIsoCode)
                .WithLanguageThreeLetterIsoCode(dto.LanguageThreeLetterIsoCode)
                .WithCultureInfoCode(dto.CultureInfoCode);

            return builder;
        }

        public virtual CountryLanguage.Builder Map(CountryLanguageDto dto)
        {
            Check.NotNull(dto, nameof(dto));

            var builder = new CountryLanguage.Builder(NotificationHandler)
                .WithId(dto.Id)
                .WithCountrySubdivisionId(dto.CountrySubdivisionId)
                .WithLanguage(dto.Language)
                .WithLanguageTwoLetterIsoCode(dto.LanguageTwoLetterIsoCode)
                .WithLanguageThreeLetterIsoCode(dto.LanguageThreeLetterIsoCode)
                .WithCultureInfoCode(dto.CultureInfoCode);

            return builder;
        }
    }
}
