﻿using System;
using System.Collections.Generic;
using System.Text;
using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Infra.Entities;
using Tnf;
using Tnf.Notifications;

namespace Thex.SuperAdmin.Application.Adapters
{
    public class UserInvitationAdapter : IUserInvitationAdapter
    {
        public INotificationHandler NotificationHandler { get; }

        public UserInvitationAdapter(INotificationHandler notificationHandler)
        {
            NotificationHandler = notificationHandler;
        }

        public virtual UserInvitation.Builder Map(UserInvitation entity, UserInvitationDto dto)
        {
            Check.NotNull(entity, nameof(entity));
            Check.NotNull(dto, nameof(dto));

            var builder = new UserInvitation.Builder(NotificationHandler, entity)
                .WithId(dto.Id)
                .WithChainId(dto.ChainId)
                .WithPropertyId(dto.PropertyId)
                .WithBrandId(dto.BrandId)
                .WithEmail(dto.Email)
                .WithName(dto.Name)
                .WithInvitationLink(dto.InvitationLink)
                .WithInvitationDate(dto.InvitationDate)
                .WithInvitationAcceptanceDate(dto.InvitationAcceptanceDate)
                .WithUserId(dto.UserId)
                .WithIsActive(dto.IsActive);

            return builder;
        }

        public virtual UserInvitation.Builder Map(UserInvitationDto dto)
        {
            Check.NotNull(dto, nameof(dto));

            var builder = new UserInvitation.Builder(NotificationHandler)
                .WithId(dto.Id)
                .WithChainId(dto.ChainId)
                .WithPropertyId(dto.PropertyId)
                .WithBrandId(dto.BrandId)
                .WithEmail(dto.Email)
                .WithName(dto.Name)
                .WithInvitationLink(dto.InvitationLink)
                .WithInvitationDate(dto.InvitationDate)
                .WithInvitationAcceptanceDate(dto.InvitationAcceptanceDate)
                .WithUserId(dto.UserId)
                .WithIsActive(dto.IsActive);

            return builder;
        }


        public virtual UserInvitation.Builder Map(Guid userId, int propertyId, Guid tenantId, string email, string name, string link)
        {
            var builder = new UserInvitation.Builder(NotificationHandler)
                .WithId(Guid.NewGuid())
                .WithPropertyId(propertyId)
                .WithEmail(email)
                .WithName(name)
                .WithInvitationLink(link)
                .WithInvitationDate(DateTime.UtcNow)
                .WithInvitationAcceptanceDate(null)
                .WithUserId(userId)
                .WithTenantId(tenantId)
                .WithIsActive(true);

            return builder;
        }
    }
}
