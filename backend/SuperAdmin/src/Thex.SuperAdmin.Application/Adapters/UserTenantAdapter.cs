﻿using Thex.SuperAdmin.Dto.Dto;
using Thex.SuperAdmin.Infra.Entities;
using Tnf;
using Tnf.Notifications;

namespace Thex.SuperAdmin.Application.Adapters
{
    public class UserTenantAdapter : IUserTenantAdapter
    {
        public INotificationHandler NotificationHandler { get; }

        public UserTenantAdapter(INotificationHandler notificationHandler)
        {
            NotificationHandler = notificationHandler;
        }

        public virtual UserTenant.Builder Map(UserTenant entity, UserTenantDto dto)
        {
            Check.NotNull(entity, nameof(entity));
            Check.NotNull(dto, nameof(dto));


            var builder = new UserTenant.Builder(NotificationHandler, entity)
                .WithId(entity.Id)
                .WithTenantId(entity.TenantId)
                .WithUserId(entity.UserId)
                .WithIsActive(entity.IsActive);

            return builder;
        }

        public virtual UserTenant.Builder Map(UserTenantDto dto)
        {
            Check.NotNull(dto, nameof(dto));

            var builder = new UserTenant.Builder(NotificationHandler)
                .WithId(dto.Id)
                .WithTenantId(dto.TenantId)
                .WithUserId(dto.UserId)
                .WithIsActive(dto.IsActive);

            return builder;
        }
    }
}
