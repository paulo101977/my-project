﻿using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Infra.Entities;

namespace Thex.SuperAdmin.Application.Adapters
{
    public interface IApplicationModuleAdapter
    {
        ApplicationModule.Builder Map(ApplicationModule entity, ApplicationModuleDto dto);
        ApplicationModule.Builder Map(ApplicationModuleDto dto);
    }
}
