﻿using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Infra.Entities;

namespace Thex.SuperAdmin.Application.Adapters
{
    public interface IIdentityTranslationAdapter
    {
        IdentityTranslation.Builder Map(IdentityTranslation entity, IdentityTranslationDto dto);
        IdentityTranslation.Builder Map(IdentityTranslationDto dto);
    }
}
