﻿using System;
using System.Collections.Generic;
using System.Text;
using Thex.Common.Dto;
using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Infra.Entities;
using Tnf;
using Tnf.Notifications;

namespace Thex.SuperAdmin.Application.Adapters
{
    public class IdentityPermissionFeatureAdapter : IIdentityPermissionFeatureAdapter
    {
        public INotificationHandler NotificationHandler { get; }

        public IdentityPermissionFeatureAdapter(INotificationHandler notificationHandler)
        {
            NotificationHandler = notificationHandler;
        }

        public virtual IdentityPermissionFeature.Builder Map(IdentityPermissionFeature entity, IdentityPermissionFeatureDto dto)
        {
            Check.NotNull(entity, nameof(entity));
            Check.NotNull(dto, nameof(dto));

            var builder = new IdentityPermissionFeature.Builder(NotificationHandler, entity)
                .WithId(dto.Id)
                .WithIdentityPermissionId(dto.IdentityPermissionId)
                .WithIdentityFeatureId(dto.IdentityFeatureId)
                .WithIsActive(dto.IsActive);
                //.WithIsDeleted(dto.IsDeleted)
                //.WithCreationTime(dto.CreationTime)
                //.WithCreatorUserId(dto.CreatorUserId)
                //.WithLastModificationTime(dto.LastModificationTime)
                //.WithLastModifierUserId(dto.LastModifierUserId)
                //.WithDeletionTime(dto.DeletionTime)
                //.WithDeleterUserId(dto.DeleterUserId);

            return builder;
        }

        public virtual IdentityPermissionFeature.Builder Map(IdentityPermissionFeatureDto dto)
        {
            Check.NotNull(dto, nameof(dto));

            var builder = new IdentityPermissionFeature.Builder(NotificationHandler)
                .WithId(dto.Id == Guid.Empty ? Guid.NewGuid() : dto.Id)
                .WithIdentityPermissionId(dto.IdentityPermissionId)
                .WithIdentityFeatureId(dto.IdentityFeatureId)
                .WithIsActive(dto.IsActive);
                //.WithIsDeleted(dto.IsDeleted)
                //.WithCreationTime(dto.CreationTime)
                //.WithCreatorUserId(dto.CreatorUserId)
                //.WithLastModificationTime(dto.LastModificationTime)
                //.WithLastModifierUserId(dto.LastModifierUserId)
                //.WithDeletionTime(dto.DeletionTime)
                //.WithDeleterUserId(dto.DeleterUserId);

            return builder;
        }
    }
}
