﻿using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Infra.Entities;

namespace Thex.SuperAdmin.Application.Adapters
{
    public interface IContactInformationAdapter
    {
        ContactInformation.Builder Map(ContactInformation entity, ContactInformationDto dto);
        ContactInformation.Builder Map(ContactInformationDto dto);
    }
}
