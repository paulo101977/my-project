﻿using System;
using System.Collections.Generic;
using System.Text;
using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Infra.Entities;
using Tnf;
using Tnf.Notifications;

namespace Thex.SuperAdmin.Application.Adapters
{
    public class ParameterTypeAdapter : IParameterTypeAdapter
    {
        public INotificationHandler NotificationHandler { get; }

        public ParameterTypeAdapter(INotificationHandler notificationHandler)
        {
            NotificationHandler = notificationHandler;
        }

        public virtual ParameterType.Builder Map(ParameterType entity, ParameterTypeDto dto)
        {
            Check.NotNull(entity, nameof(entity));
            Check.NotNull(dto, nameof(dto));

            var builder = new ParameterType.Builder(NotificationHandler, entity)
                .WithId(dto.Id)
                .WithParameterTypeName(dto.ParameterTypeName);

            return builder;
        }

        public virtual ParameterType.Builder Map(ParameterTypeDto dto)
        {
            Check.NotNull(dto, nameof(dto));

            var builder = new ParameterType.Builder(NotificationHandler)
                .WithId(dto.Id)
                .WithParameterTypeName(dto.ParameterTypeName);

            return builder;
        }
    }
}
