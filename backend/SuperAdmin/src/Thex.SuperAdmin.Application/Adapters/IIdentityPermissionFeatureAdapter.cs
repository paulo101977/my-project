﻿using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Infra.Entities;

namespace Thex.SuperAdmin.Application.Adapters
{
    public interface IIdentityPermissionFeatureAdapter
    {
        IdentityPermissionFeature.Builder Map(IdentityPermissionFeature entity, IdentityPermissionFeatureDto dto);
        IdentityPermissionFeature.Builder Map(IdentityPermissionFeatureDto dto);
    }
}
