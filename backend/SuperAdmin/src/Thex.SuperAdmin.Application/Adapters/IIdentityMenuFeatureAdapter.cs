﻿using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Infra.Entities;

namespace Thex.SuperAdmin.Application.Adapters
{
    public interface IIdentityMenuFeatureAdapter
    {
        IdentityMenuFeature.Builder Map(IdentityMenuFeature entity, IdentityMenuFeatureDto dto);
        IdentityMenuFeature.Builder Map(IdentityMenuFeatureDto dto);
    }
}
