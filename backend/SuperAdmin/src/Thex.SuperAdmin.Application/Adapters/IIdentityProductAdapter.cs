﻿using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Infra.Entities;

namespace Thex.SuperAdmin.Application.Adapters
{
    public interface IIdentityProductAdapter
    {
        IdentityProduct.Builder Map(IdentityProduct entity, IdentityProductDto dto);
        IdentityProduct.Builder Map(IdentityProductDto dto);
    }
}
