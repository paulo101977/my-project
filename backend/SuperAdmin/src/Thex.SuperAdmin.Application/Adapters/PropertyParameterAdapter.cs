﻿using System;
using System.Collections.Generic;
using System.Text;
using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Infra.Entities;
using Tnf;
using Tnf.Notifications;

namespace Thex.SuperAdmin.Application.Adapters
{
    public class PropertyParameterAdapter : IPropertyParameterAdapter
    {
        public INotificationHandler NotificationHandler { get; }
        private const int PARAMETERSYSTEMDATEID = 10;
        public PropertyParameterAdapter(INotificationHandler notificationHandler)
        {
            NotificationHandler = notificationHandler;
        }

        public virtual PropertyParameter.Builder Map(PropertyParameter entity, PropertyParameterDto dto)
        {
            Check.NotNull(entity, nameof(entity));
            Check.NotNull(dto, nameof(dto));

            if(dto.ApplicationParameterId == PARAMETERSYSTEMDATEID)
            {
                DateTime systemDateTime;
                DateTime.TryParse(dto.PropertyParameterValue, out systemDateTime);

                dto.PropertyParameterValue = systemDateTime.ToString("yyyy-MM-dd");
            }

            var builder = new PropertyParameter.Builder(NotificationHandler, entity)
                .WithId(dto.Id)
                .WithPropertyId(dto.PropertyId)
                .WithApplicationParameterId(dto.ApplicationParameterId)
                .WithPropertyParameterValue(dto.PropertyParameterValue)
                .WithPropertyParameterMinValue(dto.PropertyParameterMinValue)
                .WithPropertyParameterMaxValue(dto.PropertyParameterMaxValue)
                .WithPropertyParameterPossibleValues(dto.PropertyParameterPossibleValues);

            return builder;
        }

        public virtual PropertyParameter.Builder Map(PropertyParameterDto dto)
        {
            Check.NotNull(dto, nameof(dto));
            

            if (dto.ApplicationParameterId == PARAMETERSYSTEMDATEID)
            {
                DateTime systemDateTime;
                DateTime.TryParse(dto.PropertyParameterValue, out systemDateTime);

                dto.PropertyParameterValue = systemDateTime.ToString("yyyy-MM-dd");
            }
            //Guid newGuid = Guid.Empty;
            //if (dto.Id == Guid.Empty)
            //    newGuid = Guid.NewGuid();
            //else newGuid = dto.Id;


            var builder = new PropertyParameter.Builder(NotificationHandler)
                .WithId(dto.Id == Guid.Empty ? Guid.NewGuid() : dto.Id)
                .WithPropertyId(dto.PropertyId)
                .WithApplicationParameterId(dto.ApplicationParameterId)
                .WithPropertyParameterValue(dto.PropertyParameterValue)
                .WithPropertyParameterMinValue(dto.PropertyParameterMinValue)
                .WithPropertyParameterMaxValue(dto.PropertyParameterMaxValue)
                .WithPropertyParameterPossibleValues(dto.PropertyParameterPossibleValues)
                .WithPropertyParameterIsActive(dto.IsActive.Value)
                ;

            return builder;
        }

    }
}
