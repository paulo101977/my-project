﻿using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Infra.Entities;

namespace Thex.SuperAdmin.Application.Adapters
{
    public interface IIdentityPermissionAdapter
    {
        IdentityPermission.Builder Map(IdentityPermission entity, IdentityPermissionDto dto);
        IdentityPermission.Builder Map(IdentityPermissionDto dto);
    }
}
