﻿using System;
using System.Collections.Generic;
using System.Text;
using Thex.Common.Dto;
using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Infra.Entities;
using Tnf;
using Tnf.Notifications;

namespace Thex.SuperAdmin.Application.Adapters
{
    public class IdentityRolePermissionAdapter : IIdentityRolePermissionAdapter
    {
        public INotificationHandler NotificationHandler { get; }

        public IdentityRolePermissionAdapter(INotificationHandler notificationHandler)
        {
            NotificationHandler = notificationHandler;
        }

        public virtual IdentityRolePermission.Builder Map(IdentityRolePermission entity, IdentityRolePermissionDto dto)
        {
            Check.NotNull(entity, nameof(entity));
            Check.NotNull(dto, nameof(dto));

            var builder = new IdentityRolePermission.Builder(NotificationHandler, entity)
                .WithId(dto.Id)
                .WithIdentityPermissionId(dto.IdentityPermissionId)
                .WithIsActive(dto.IsActive);
                //.WithIsDeleted(dto.IsDeleted)
                //.WithCreationTime(dto.CreationTime)
                //.WithCreatorUserId(dto.CreatorUserId)
                //.WithLastModificationTime(dto.LastModificationTime)
                //.WithLastModifierUserId(dto.LastModifierUserId)
                //.WithDeletionTime(dto.DeletionTime)
                //.WithDeleterUserId(dto.DeleterUserId);

            return builder;
        }

        public virtual IdentityRolePermission.Builder Map(IdentityRolePermissionDto dto, Guid roleId)
        {
            Check.NotNull(dto, nameof(dto));

            var builder = new IdentityRolePermission.Builder(NotificationHandler)
                .WithId(Guid.NewGuid())
                .WithIdentityPermissionId(dto.IdentityPermissionId)
                .WithIsActive(dto.IsActive)
                .WithRoleId(roleId);
                //.WithIsDeleted(dto.IsDeleted)
                //.WithCreationTime(dto.CreationTime)
                //.WithCreatorUserId(dto.CreatorUserId)
                //.WithLastModificationTime(dto.LastModificationTime)
                //.WithLastModifierUserId(dto.LastModifierUserId)
                //.WithDeletionTime(dto.DeletionTime)
                //.WithDeleterUserId(dto.DeleterUserId);

            return builder;
        }
    }
}
