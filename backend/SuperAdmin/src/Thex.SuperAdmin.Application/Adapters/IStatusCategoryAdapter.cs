﻿using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Infra.Entities;

namespace Thex.SuperAdmin.Application.Adapters
{
    public interface IStatusCategoryAdapter
    {
        StatusCategory.Builder Map(StatusCategory entity, StatusCategoryDto dto);
        StatusCategory.Builder Map(StatusCategoryDto dto);
    }
}
