﻿using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Infra.Entities;
using Tnf;
using Tnf.Notifications;

namespace Thex.SuperAdmin.Application.Adapters
{
    public class TenantAdapter : ITenantAdapter
    {
        public INotificationHandler NotificationHandler { get; }

        public TenantAdapter(INotificationHandler notificationHandler)
        {
            NotificationHandler = notificationHandler;
        }

        public virtual Tenant.Builder MapToUpdate(Tenant entity, TenantDto dto)
        {
            Check.NotNull(entity, nameof(entity));
            Check.NotNull(dto, nameof(dto));

            var builder = new Tenant.Builder(NotificationHandler, entity)
                .WithId(entity.Id)
                .WithIsActive(true)
                .WithName(dto.TenantName);

            return builder;
        }

        public virtual Tenant.Builder Map(TenantDto dto)
        {
            Check.NotNull(dto, nameof(dto));

            var builder = new Tenant.Builder(NotificationHandler)
                .WithId(dto.Id)
                .WithIsActive(true)
                .WithName(dto.TenantName);

            return builder;
        }
    }
}
