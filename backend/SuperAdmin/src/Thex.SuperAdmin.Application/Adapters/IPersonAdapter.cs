﻿using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Infra.Entities;

namespace Thex.SuperAdmin.Application.Adapters
{
    public interface IPersonAdapter
    {
        Person.Builder Map(Person entity, PersonDto dto);
        Person.Builder Map(PersonDto dto);
        Person.Builder MapPerson(Person entity, PersonDto dto);
        Person.Builder MapUpdateProfilePerson(Person entity, PersonDto dto);
        Person.Builder MapUpdatePhotoUrlPerson(Person entity, string photoUrl);
    }
}
