﻿using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Infra.Entities;

namespace Thex.SuperAdmin.Application.Adapters
{
    public interface ICountrySubdivisionAdapter
    {
        CountrySubdivision.Builder Map(CountrySubdivision entity, CountrySubdivisionDto dto);
        CountrySubdivision.Builder Map(CountrySubdivisionDto dto);
    }
}
