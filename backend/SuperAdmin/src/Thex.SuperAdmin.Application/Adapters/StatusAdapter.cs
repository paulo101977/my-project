﻿using System;
using System.Collections.Generic;
using System.Text;
using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Infra.Entities;
using Tnf;
using Tnf.Notifications;

namespace Thex.SuperAdmin.Application.Adapters
{
    public class StatusAdapter : IStatusAdapter
    {
        public INotificationHandler NotificationHandler { get; }

        public StatusAdapter(INotificationHandler notificationHandler)
        {
            NotificationHandler = notificationHandler;
        }

        public virtual Status.Builder Map(Status entity, StatusDto dto)
        {
            Check.NotNull(entity, nameof(entity));
            Check.NotNull(dto, nameof(dto));

            var builder = new Status.Builder(NotificationHandler, entity)
                .WithId(dto.Id)
                .WithStatusCategoryId(dto.StatusCategoryId)
                .WithName(dto.Name);

            return builder;
        }


        public virtual Status.Builder Map(StatusDto dto)
        {
            Check.NotNull(dto, nameof(dto));

            var builder = new Status.Builder(NotificationHandler)
                .WithId(dto.Id)
                .WithStatusCategoryId(dto.StatusCategoryId)
                .WithName(dto.Name);

            return builder;
        }
    }
}
