﻿using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Infra.Entities;

namespace Thex.SuperAdmin.Application.Adapters
{
    public interface IPropertyParameterAdapter
    {
        PropertyParameter.Builder Map(PropertyParameter entity, PropertyParameterDto dto);
        PropertyParameter.Builder Map(PropertyParameterDto dto);
    }
}
