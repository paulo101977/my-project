﻿using Thex.SuperAdmin.Dto.Dto;
using Thex.SuperAdmin.Infra.Entities;

namespace Thex.SuperAdmin.Application.Adapters
{
    public interface IIntegrationPartnerPropertyAdapter
    {
        IntegrationPartnerProperty.Builder Map(IntegrationPartnerProperty entity, IntegrationPartnerPropertyDto dto);
        IntegrationPartnerProperty.Builder Map(IntegrationPartnerPropertyDto dto);
    }
}