﻿using System;
using System.Collections.Generic;
using System.Text;
using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Infra.Entities;
using Tnf;
using Tnf.Notifications;

namespace Thex.SuperAdmin.Application.Adapters
{
    public class ApplicationModuleAdapter : IApplicationModuleAdapter
    {
        public INotificationHandler NotificationHandler { get; }

        public ApplicationModuleAdapter(INotificationHandler notificationHandler)
        {
            NotificationHandler = notificationHandler;
        }

        public virtual ApplicationModule.Builder Map(ApplicationModule entity, ApplicationModuleDto dto)
        {
            Check.NotNull(entity, nameof(entity));
            Check.NotNull(dto, nameof(dto));

            var builder = new ApplicationModule.Builder(NotificationHandler, entity)
                .WithId(dto.Id)
                .WithApplicationModuleName(dto.ApplicationModuleName)
                .WithApplicationModuleDescription(dto.ApplicationModuleDescription);

            return builder;
        }

        public virtual ApplicationModule.Builder Map(ApplicationModuleDto dto)
        {
            Check.NotNull(dto, nameof(dto));

            var builder = new ApplicationModule.Builder(NotificationHandler)
                .WithId(dto.Id)
                .WithApplicationModuleName(dto.ApplicationModuleName)
                .WithApplicationModuleDescription(dto.ApplicationModuleDescription);

            return builder;
        }
    }
}
