﻿using System;
using System.Collections.Generic;
using System.Text;
using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Infra.Entities;
using Tnf;
using Tnf.Notifications;

namespace Thex.SuperAdmin.Application.Adapters
{
    public class IdentityTranslationAdapter : IIdentityTranslationAdapter
    {
        public INotificationHandler NotificationHandler { get; }

        public IdentityTranslationAdapter(INotificationHandler notificationHandler)
        {
            NotificationHandler = notificationHandler;
        }

        public virtual IdentityTranslation.Builder Map(IdentityTranslation entity, IdentityTranslationDto dto)
        {
            Check.NotNull(entity, nameof(entity));
            Check.NotNull(dto, nameof(dto));

            var builder = new IdentityTranslation.Builder(NotificationHandler, entity)
                .WithId(dto.Id)
                .WithLanguageIsoCode(dto.LanguageIsoCode)
                .WithName(dto.Name)
                .WithIdentityOwnerId(dto.IdentityOwnerId)
                .WithDescription(dto.Description);

            return builder;
        }

        public virtual IdentityTranslation.Builder Map(IdentityTranslationDto dto)
        {
            Check.NotNull(dto, nameof(dto));

            var builder = new IdentityTranslation.Builder(NotificationHandler)
                .WithId(dto.Id)
                .WithLanguageIsoCode(dto.LanguageIsoCode)
                .WithName(dto.Name)
                .WithIdentityOwnerId(dto.IdentityOwnerId)
                .WithDescription(dto.Description);

            return builder;
        }
    }
}
