﻿using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Infra.Entities;

namespace Thex.SuperAdmin.Application.Adapters
{
    public interface IFeatureGroupAdapter
    {
        FeatureGroup.Builder Map(FeatureGroup entity, FeatureGroupDto dto);
        FeatureGroup.Builder Map(FeatureGroupDto dto);
    }
}
