﻿using System;
using System.Collections.Generic;
using System.Text;
using Thex.Common.Enumerations;
using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Infra.Entities;
using Tnf;
using Tnf.Notifications;

namespace Thex.SuperAdmin.Application.Adapters
{
    public class PropertyAdapter : IPropertyAdapter
    {
        public INotificationHandler NotificationHandler { get; }

        public PropertyAdapter(INotificationHandler notificationHandler)
        {
            NotificationHandler = notificationHandler;
        }

        public virtual Property.Builder MapToUpdate(Property entity, CreateOrUpdatePropertyDto dto)
        {
            Check.NotNull(entity, nameof(entity));
            Check.NotNull(dto, nameof(dto));

            var builder = new Property.Builder(NotificationHandler, entity)
                .WithCompanyId(dto.CompanyId)
                .WithBrandId(dto.BrandId)
                .WithName(dto.Name)
                //.WithPhoto(dto.PhotoUrl)
                .WithPropertyTypeId(((PropertyTypeEnum)entity.PropertyTypeId))
                .WithTenantId(entity.TenantId)
                .WithPropertyUId(entity.PropertyUId)
                .WithIsBlocked(dto.IsBlocked)
                .WithPropertyStatusId(entity.PropertyStatusId);

            return builder;
        }

        public virtual Property.Builder MapToCreate(CreateOrUpdatePropertyDto dto)
        {
            Check.NotNull(dto, nameof(dto));

            var builder = new Property.Builder(NotificationHandler)
                .WithCompanyId(dto.CompanyId)
                .WithPropertyTypeId(PropertyTypeEnum.Hotel)
                .WithTenantId(dto.TenantId)
                .WithBrandId(dto.BrandId)
                .WithName(dto.Name)
                .WithIsBlocked(dto.IsBlocked)
                .WithPropertyUId(dto.PropertyUId)
                //.WithPhoto(dto.PhotoUrl)
                .WithPropertyStatusId((int)PropertyStatusEnum.Register);

            return builder;
        }
    }
}
