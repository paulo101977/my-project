﻿using System;
using System.Collections.Generic;
using System.Text;
using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Infra.Entities;
using Tnf;
using Tnf.Notifications;

namespace Thex.SuperAdmin.Application.Adapters
{
    public class ContactInformationAdapter : IContactInformationAdapter
    {
        public INotificationHandler NotificationHandler { get; }

        public ContactInformationAdapter(INotificationHandler notificationHandler)
        {
            NotificationHandler = notificationHandler;
        }

        public virtual ContactInformation.Builder Map(ContactInformation entity, ContactInformationDto dto)
        {
            Check.NotNull(entity, nameof(entity));
            Check.NotNull(dto, nameof(dto));

            var builder = new ContactInformation.Builder(NotificationHandler, entity)
                .WithOwnerId(dto.OwnerId)
                .WithContactInformationTypeId(dto.ContactInformationTypeId)
                .WithInformation(dto.Information);

            return builder;
        }

        public virtual ContactInformation.Builder Map(ContactInformationDto dto)
        {
            Check.NotNull(dto, nameof(dto));

            var builder = new ContactInformation.Builder(NotificationHandler)
                .WithOwnerId(dto.OwnerId)
                .WithContactInformationTypeId(dto.ContactInformationTypeId)
                .WithInformation(dto.Information);

            return builder;
        }
    }
}
