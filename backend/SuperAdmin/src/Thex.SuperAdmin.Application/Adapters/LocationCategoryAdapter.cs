﻿using System;
using System.Collections.Generic;
using System.Text;
using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Infra.Entities;
using Tnf;
using Tnf.Notifications;

namespace Thex.SuperAdmin.Application.Adapters
{
    public class LocationCategoryAdapter : ILocationCategoryAdapter
    {
        public INotificationHandler NotificationHandler { get; }

        public LocationCategoryAdapter(INotificationHandler notificationHandler)
        {
            NotificationHandler = notificationHandler;
        }

        public virtual LocationCategory.Builder Map(LocationCategory entity, LocationCategoryDto dto)
        {
            Check.NotNull(entity, nameof(entity));
            Check.NotNull(dto, nameof(dto));

            var builder = new LocationCategory.Builder(NotificationHandler, entity)
                .WithId(dto.Id)
                .WithName(dto.Name)
                .WithRecordScope(dto.RecordScope);

            return builder;
        }

        public virtual LocationCategory.Builder Map(LocationCategoryDto dto)
        {
            Check.NotNull(dto, nameof(dto));

            var builder = new LocationCategory.Builder(NotificationHandler)
                .WithId(dto.Id)
                .WithName(dto.Name)
                .WithRecordScope(dto.RecordScope);

            return builder;
        }
    }
}
