﻿using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Infra.Entities;

namespace Thex.SuperAdmin.Application.Adapters
{
    public interface ICountrySubdivisionTranslationAdapter
    {
        CountrySubdivisionTranslation.Builder Map(CountrySubdivisionTranslation entity, CountrySubdivisionTranslationDto dto);
        CountrySubdivisionTranslation.Builder Map(CountrySubdivisionTranslationDto dto);
    }
}
