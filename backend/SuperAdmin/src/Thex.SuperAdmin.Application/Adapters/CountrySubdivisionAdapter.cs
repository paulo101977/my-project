﻿using System;
using System.Collections.Generic;
using System.Text;
using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Infra.Entities;
using Tnf;
using Tnf.Notifications;

namespace Thex.SuperAdmin.Application.Adapters
{
    public class CountrySubdivisionAdapter : ICountrySubdivisionAdapter
    {
        public INotificationHandler NotificationHandler { get; }

        public CountrySubdivisionAdapter(INotificationHandler notificationHandler)
        {
            NotificationHandler = notificationHandler;
        }

        public virtual CountrySubdivision.Builder Map(CountrySubdivision entity, CountrySubdivisionDto dto)
        {
            Check.NotNull(entity, nameof(entity));
            Check.NotNull(dto, nameof(dto));

            var builder = CountrySubdivision.Create(NotificationHandler, entity)
                .WithId(dto.Id)
                .WithTwoLetterIsoCode(dto.TwoLetterIsoCode)
                .WithThreeLetterIsoCode(dto.ThreeLetterIsoCode)
                .WithCountryTwoLetterIsoCode(dto.CountryTwoLetterIsoCode)
                .WithCountryThreeLetterIsoCode(dto.CountryThreeLetterIsoCode)
                .WithExternalCode(dto.ExternalCode)
                .WithParentSubdivisionId(dto.ParentSubdivisionId)
                .WithSubdivisionTypeId(dto.SubdivisionTypeId);

            return builder;
        }

        public virtual CountrySubdivision.Builder Map(CountrySubdivisionDto dto)
        {
            Check.NotNull(dto, nameof(dto));

            var builder = CountrySubdivision.Create(NotificationHandler)
                .WithId(dto.Id)
                .WithTwoLetterIsoCode(dto.TwoLetterIsoCode)
                .WithThreeLetterIsoCode(dto.ThreeLetterIsoCode)
                .WithCountryTwoLetterIsoCode(dto.CountryTwoLetterIsoCode)
                .WithCountryThreeLetterIsoCode(dto.CountryThreeLetterIsoCode)
                .WithExternalCode(dto.ExternalCode)
                .WithParentSubdivisionId(dto.ParentSubdivisionId)
                .WithSubdivisionTypeId(dto.SubdivisionTypeId);

            return builder;
        }
    }
}
