﻿using Thex.SuperAdmin.Dto.Dto;
using Thex.SuperAdmin.Infra.Entities;
using Tnf;
using Tnf.Notifications;

namespace Thex.SuperAdmin.Application.Adapters
{
    public class IntegrationPartnerPropertyAdapter : IIntegrationPartnerPropertyAdapter
    {
        public INotificationHandler NotificationHandler { get; }

        public IntegrationPartnerPropertyAdapter(INotificationHandler notificationHandler)
        {
            NotificationHandler = notificationHandler;
        }

        public virtual IntegrationPartnerProperty.Builder Map(IntegrationPartnerProperty entity, IntegrationPartnerPropertyDto dto)
        {
            Check.NotNull(entity, nameof(entity));
            Check.NotNull(dto, nameof(dto));

            var builder = new IntegrationPartnerProperty.Builder(NotificationHandler, entity)
                .WithId(entity.Id)
                .WithPartnerId(entity.PartnerId)
                .WithPropertyId(entity.PropertyId)
                .WithIntegrationPartnerId(dto.IntegrationPartnerId)
                .WithIntegrationCode(dto.IntegrationCode)
                .WithIsActive(dto.IsActive);

            return builder;
        }

        public virtual IntegrationPartnerProperty.Builder Map(IntegrationPartnerPropertyDto dto)
        {
            Check.NotNull(dto, nameof(dto));

            var builder = new IntegrationPartnerProperty.Builder(NotificationHandler)
                .WithId(dto.Id)
                .WithPartnerId(dto.PartnerId)
                .WithPropertyId(dto.PropertyId)
                .WithIntegrationPartnerId(dto.IntegrationPartnerId)
                .WithIntegrationCode(dto.IntegrationCode)
                
                .WithIsActive(dto.IsActive);

            return builder;
        }

        
    }
}
