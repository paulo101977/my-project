﻿using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Infra.Entities;
using Tnf;
using Tnf.Notifications;

namespace Thex.SuperAdmin.Application.Adapters
{
    public class ChainAdapter : IChainAdapter
    {
        public INotificationHandler NotificationHandler { get; }

        public ChainAdapter(INotificationHandler notificationHandler)
        {
            NotificationHandler = notificationHandler;
        }

        public virtual Chain.Builder MapToUpdate(Chain entity, ChainDto dto)
        {
            Check.NotNull(entity, nameof(entity));
            Check.NotNull(dto, nameof(dto));

            var builder = new Chain.Builder(NotificationHandler, entity)
                .WithId(entity.Id)
                .WithTenantId(entity.TenantId)
                .WithName(dto.Name)
                .WithIsTemporary(entity.IsTemporary);

            return builder;
        }

        public virtual Chain.Builder MapToCreate(ChainDto dto)
        {
            Check.NotNull(dto, nameof(dto));

            var builder = new Chain.Builder(NotificationHandler)
                .WithName(dto.Name)
                .WithTenantId(dto.TenantId)
                .WithIsTemporary(false);

            return builder;
        }
    }
}
