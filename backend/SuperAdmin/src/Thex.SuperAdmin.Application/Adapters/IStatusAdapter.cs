﻿using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Infra.Entities;

namespace Thex.SuperAdmin.Application.Adapters
{
    public interface IStatusAdapter
    {
        Status.Builder Map(Status entity, StatusDto dto);
        Status.Builder Map(StatusDto dto);
    }
}
