﻿using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Infra.Entities;

namespace Thex.SuperAdmin.Application.Adapters
{
    public interface IIdentityFeatureAdapter
    {
        IdentityFeature.Builder Map(IdentityFeature entity, IdentityFeatureDto dto);
        IdentityFeature.Builder Map(IdentityFeatureDto dto);
    }
}
