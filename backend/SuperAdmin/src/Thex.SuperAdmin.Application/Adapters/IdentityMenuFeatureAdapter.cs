﻿using System;
using System.Collections.Generic;
using System.Text;
using Thex.Common.Dto;
using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Infra.Entities;
using Tnf;
using Tnf.Notifications;

namespace Thex.SuperAdmin.Application.Adapters
{
    public class IdentityMenuFeatureAdapter : IIdentityMenuFeatureAdapter
    {
        public INotificationHandler NotificationHandler { get; }

        public IdentityMenuFeatureAdapter(INotificationHandler notificationHandler)
        {
            NotificationHandler = notificationHandler;
        }

        public virtual IdentityMenuFeature.Builder Map(IdentityMenuFeature entity, IdentityMenuFeatureDto dto)
        {
            Check.NotNull(entity, nameof(entity));
            Check.NotNull(dto, nameof(dto));

            var builder = new IdentityMenuFeature.Builder(NotificationHandler, entity)
                .WithId(dto.Id)
                .WithIdentityProductId(dto.IdentityProductId)
                .WithIdentityModuleId(dto.IdentityModuleId)
                .WithIdentityFeatureParentId(dto.IdentityFeatureParentId)
                .WithIdentityFeatureId(dto.IdentityFeatureId)
                .WithIsActive(dto.IsActive)
                .WithOrderNumber(dto.OrderNumber)
                .WithLevel(dto.Level)
                .WithDescription(dto.Description);

            return builder;
        }

        public virtual IdentityMenuFeature.Builder Map(IdentityMenuFeatureDto dto)
        {
            Check.NotNull(dto, nameof(dto));

            var builder = new IdentityMenuFeature.Builder(NotificationHandler)
                .WithId(Guid.NewGuid())
                .WithIdentityProductId(dto.IdentityProductId)
                .WithIdentityModuleId(dto.IdentityModuleId)
                .WithIdentityFeatureParentId(dto.IdentityFeatureParentId)
                .WithIdentityFeatureId(dto.IdentityFeatureId)
                .WithIsActive(dto.IsActive)
                .WithOrderNumber(dto.OrderNumber)
                .WithLevel(dto.Level)
                .WithDescription(dto.Description)
                .WithIsDeleted(dto.IsDeleted);

            return builder;
        }
    }
}
