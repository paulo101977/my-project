﻿using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Infra.Entities;

namespace Thex.SuperAdmin.Application.Adapters
{
    public interface ILocationCategoryAdapter
    {
        LocationCategory.Builder Map(LocationCategory entity, LocationCategoryDto dto);
        LocationCategory.Builder Map(LocationCategoryDto dto);
    }
}
