﻿using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Infra.Entities;

namespace Thex.SuperAdmin.Application.Adapters
{
    public interface ITenantAdapter
    {
        Tenant.Builder MapToUpdate(Tenant entity, TenantDto dto);
        Tenant.Builder Map(TenantDto dto);
    }
}