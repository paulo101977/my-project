﻿using System;
using System.Collections.Generic;
using System.Text;
using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Infra.Entities;
using Tnf;
using Tnf.Notifications;

namespace Thex.SuperAdmin.Application.Adapters
{
    public class PropertyTypeAdapter : IPropertyTypeAdapter
    {
        public INotificationHandler NotificationHandler { get; }

        public PropertyTypeAdapter(INotificationHandler notificationHandler)
        {
            NotificationHandler = notificationHandler;
        }

        public virtual PropertyType.Builder Map(PropertyType entity, PropertyTypeDto dto)
        {
            Check.NotNull(entity, nameof(entity));
            Check.NotNull(dto, nameof(dto));

            var builder = new PropertyType.Builder(NotificationHandler, entity)
                .WithId(dto.Id)
                .WithName(dto.Name);

            return builder;
        }

        public virtual PropertyType.Builder Map(PropertyTypeDto dto)
        {
            Check.NotNull(dto, nameof(dto));

            var builder = new PropertyType.Builder(NotificationHandler)
                .WithId(dto.Id)
                .WithName(dto.Name);

            return builder;
        }
    }
}
