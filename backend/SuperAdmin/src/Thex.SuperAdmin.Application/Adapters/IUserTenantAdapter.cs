﻿using Thex.SuperAdmin.Dto.Dto;
using Thex.SuperAdmin.Infra.Entities;

namespace Thex.SuperAdmin.Application.Adapters
{
    public interface IUserTenantAdapter
    {
        UserTenant.Builder Map(UserTenant entity, UserTenantDto dto);
        UserTenant.Builder Map(UserTenantDto dto);
    }
}
