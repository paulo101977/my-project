﻿using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Infra.Entities;

namespace Thex.SuperAdmin.Application.Adapters
{
    public interface IContactInformationTypeAdapter
    {
        ContactInformationType.Builder Map(ContactInformationType entity, ContactInformationTypeDto dto);
        ContactInformationType.Builder Map(ContactInformationTypeDto dto);
    }
}
