﻿using System;
using System.Collections.Generic;
using System.Text;
using Thex.Common.Dto;
using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Infra.Entities;
using Tnf;
using Tnf.Notifications;

namespace Thex.SuperAdmin.Application.Adapters
{
    public class IdentityFeatureAdapter : IIdentityFeatureAdapter
    {
        public INotificationHandler NotificationHandler { get; }

        public IdentityFeatureAdapter(INotificationHandler notificationHandler)
        {
            NotificationHandler = notificationHandler;
        }

        public virtual IdentityFeature.Builder Map(IdentityFeature entity, IdentityFeatureDto dto)
        {
            Check.NotNull(entity, nameof(entity));
            Check.NotNull(dto, nameof(dto));

            var builder = new IdentityFeature.Builder(NotificationHandler, entity)
                .WithId(dto.Id)
                .WithIdentityProductId(dto.IdentityProductId)
                .WithIdentityModuleId(dto.IdentityModuleId)
                .WithKey(dto.Key)
                .WithName(dto.Name)
                .WithDescription(dto.Description)
                .WithIsInternal(dto.IsInternal);

            return builder;
        }

        public virtual IdentityFeature.Builder Map(IdentityFeatureDto dto)
        {
            Check.NotNull(dto, nameof(dto));

            var builder = new IdentityFeature.Builder(NotificationHandler)
                .WithId(Guid.NewGuid())
                .WithIdentityProductId(dto.IdentityProductId)
                .WithIdentityModuleId(dto.IdentityModuleId)
                .WithKey(dto.Key)
                .WithName(dto.Name)
                .WithDescription(dto.Description)
                .WithIsInternal(dto.IsInternal);

            return builder;
        }
    }
}
