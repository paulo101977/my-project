﻿using System;
using System.Collections.Generic;
using System.Text;
using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Infra.Entities;
using Tnf;
using Tnf.Notifications;

namespace Thex.SuperAdmin.Application.Adapters
{
    public class FeatureGroupAdapter : IFeatureGroupAdapter
    {
        public INotificationHandler NotificationHandler { get; }

        public FeatureGroupAdapter(INotificationHandler notificationHandler)
        {
            NotificationHandler = notificationHandler;
        }

        public virtual FeatureGroup.Builder Map(FeatureGroup entity, FeatureGroupDto dto)
        {
            Check.NotNull(entity, nameof(entity));
            Check.NotNull(dto, nameof(dto));

            var builder = new FeatureGroup.Builder(NotificationHandler, entity)
                .WithId(dto.Id)
                .WithFeatureGroupName(dto.FeatureGroupName)
                .WithDescription(dto.Description);

            return builder;
        }

        public virtual FeatureGroup.Builder Map(FeatureGroupDto dto)
        {
            Check.NotNull(dto, nameof(dto));

            var builder = new FeatureGroup.Builder(NotificationHandler)
                .WithId(dto.Id)
                .WithFeatureGroupName(dto.FeatureGroupName)
                .WithDescription(dto.Description);

            return builder;
        }
    }
}
