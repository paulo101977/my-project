﻿using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Infra.Entities;

namespace Thex.SuperAdmin.Application.Adapters
{
    public interface IParameterTypeAdapter
    {
        ParameterType.Builder Map(ParameterType entity, ParameterTypeDto dto);
        ParameterType.Builder Map(ParameterTypeDto dto);
    }
}
