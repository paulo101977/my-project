﻿using System;
using System.Collections.Generic;
using System.Text;
using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Infra.Entities;
using Tnf;
using Tnf.Notifications;

namespace Thex.SuperAdmin.Application.Adapters
{
    public class PersonAdapter : IPersonAdapter
    {
        public INotificationHandler NotificationHandler { get; }

        public PersonAdapter(INotificationHandler notificationHandler)
        {
            NotificationHandler = notificationHandler;
        }

        public virtual Person.Builder Map(Person entity, PersonDto dto)
        {
            Check.NotNull(entity, nameof(entity));
            Check.NotNull(dto, nameof(dto));

            var builder = new Person.Builder(NotificationHandler, entity)
                .WithId(dto.Id)
                .WithPersonType(dto.PersonType)
                .WithFirstName(dto.FirstName)
                .WithLastName(dto.LastName)
                .WithFullName(dto.FullName)
                .WithDateOfBirth(dto.DateOfBirth)
                .WithPhotoUrl(entity.PhotoUrl)
                .WithCountrySubdivisionId(dto.CountrySubdivisionId)
                .WithNickName(dto.NickName);


            return builder;
        }

        public virtual Person.Builder Map(PersonDto dto)
        {
            Check.NotNull(dto, nameof(dto));

            var builder = new Person.Builder(NotificationHandler)
                .WithId(dto.Id)
                .WithPersonType(dto.PersonType)
                .WithFirstName(dto.FirstName)
                .WithLastName(dto.LastName)
                .WithFullName(dto.FullName)
                .WithDateOfBirth(dto.DateOfBirth)
                .WithCountrySubdivisionId(dto.CountrySubdivisionId)
                .WithPhotoUrl(dto.PhotoUrl)
                .WithNickName(dto.NickName);

            return builder;
        }

        public Person.Builder MapPerson(Person entity, PersonDto dto)
        {
            Check.NotNull(entity, nameof(entity));
            Check.NotNull(dto, nameof(dto));

            var builder = new Person.Builder(NotificationHandler, entity)
                .WithId(entity.Id)
                .WithPersonType(dto.PersonType)
                .WithFirstName(dto.FirstName)
                .WithLastName(dto.LastName)
                .WithFullName(dto.FullName)
                .WithDateOfBirth(dto.DateOfBirth)
                .WithPhotoUrl(entity.PhotoUrl)
                .WithCountrySubdivisionId(entity.CountrySubdivisionId)
                .WithNickName(dto.NickName);

            return builder;
           
        }

        public Person.Builder MapUpdateProfilePerson(Person entity, PersonDto dto)
        {
            Check.NotNull(entity, nameof(entity));
            Check.NotNull(dto, nameof(dto));

            var builder = new Person.Builder(NotificationHandler, entity)
                .WithId(entity.Id)
                .WithPersonType(entity.PersonType)
                .WithFirstName(dto.FirstName)
                .WithLastName(dto.LastName)
                .WithFullName($"{dto.FirstName} {dto.LastName}")
                .WithDateOfBirth(dto.DateOfBirth)
                .WithPhotoUrl(entity.PhotoUrl)
                .WithCountrySubdivisionId(entity.CountrySubdivisionId)
                .WithNickName(dto.NickName);

            return builder;

        }

        public Person.Builder MapUpdatePhotoUrlPerson(Person entity, string photoUrl)
        {
            Check.NotNull(entity, nameof(entity));

            var builder = new Person.Builder(NotificationHandler, entity)
                .WithId(entity.Id)
                .WithPersonType(entity.PersonType)
                .WithFirstName(entity.FirstName)
                .WithLastName(entity.LastName)
                .WithFullName(entity.FullName)
                .WithDateOfBirth(entity.DateOfBirth)
                .WithPhotoUrl(photoUrl)
                .WithCountrySubdivisionId(entity.CountrySubdivisionId)
                .WithNickName(entity.NickName);

            return builder;

        }
    }
}
