﻿using Thex.SuperAdmin.Dto.Dto;
using Thex.SuperAdmin.Infra.Entities;

namespace Thex.SuperAdmin.Application.Adapters
{
    public interface IPropertyContractAdapter
    {
        PropertyContract.Builder Map(PropertyContractDto dto);
        PropertyContract.Builder MapToUpdate(PropertyContract entity, PropertyContractDto dto);
    }
}
