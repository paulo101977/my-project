﻿using System;
using System.Collections.Generic;
using System.Text;
using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Infra.Entities;
using Tnf;
using Tnf.Notifications;

namespace Thex.SuperAdmin.Application.Adapters
{
    public class BrandAdapter : IBrandAdapter
    {
        public INotificationHandler NotificationHandler { get; }

        public BrandAdapter(INotificationHandler notificationHandler)
        {
            NotificationHandler = notificationHandler;
        }

        public virtual Brand.Builder Map(Brand entity, BrandDto dto)
        {
            Check.NotNull(entity, nameof(entity));
            Check.NotNull(dto, nameof(dto));

            var builder = new Brand.Builder(NotificationHandler, entity)
                .WithId(entity.Id)
                .WithChainId(entity.ChainId)
                .WithName(dto.Name)
                .WithTenant(entity.TenantId)
                .WithIsTemporary(entity.IsTemporary);

            return builder;
        }

        public virtual Brand.Builder Map(BrandDto dto)
        {
            Check.NotNull(dto, nameof(dto));

            var builder = new Brand.Builder(NotificationHandler)
                .WithId(dto.Id)
                .WithChainId(dto.ChainId)
                .WithName(dto.Name)
                .WithTenant(dto.TenantId)
                
                .WithIsTemporary(dto.IsTemporary);

            return builder;
        }
    }
}
