﻿using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Infra.Entities;

namespace Thex.SuperAdmin.Application.Adapters
{
    public interface ICountryLanguageAdapter
    {
        CountryLanguage.Builder Map(CountryLanguage entity, CountryLanguageDto dto);
        CountryLanguage.Builder Map(CountryLanguageDto dto);
    }
}
