﻿using System;
using System.Collections.Generic;
using System.Text;
using Thex.Common.Dto;
using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Infra.Entities;
using Tnf;
using Tnf.Notifications;

namespace Thex.SuperAdmin.Application.Adapters
{
    public class IdentityModuleAdapter : IIdentityModuleAdapter
    {
        public INotificationHandler NotificationHandler { get; }

        public IdentityModuleAdapter(INotificationHandler notificationHandler)
        {
            NotificationHandler = notificationHandler;
        }

        public virtual IdentityModule.Builder Map(IdentityModule entity, IdentityModuleDto dto)
        {
            Check.NotNull(entity, nameof(entity));
            Check.NotNull(dto, nameof(dto));

            var builder = new IdentityModule.Builder(NotificationHandler, entity)
                .WithId(dto.Id)
                .WithName(dto.Name)
                .WithDescription(dto.Description);

            return builder;
        }

        public virtual IdentityModule.Builder Map(IdentityModuleDto dto)
        {
            Check.NotNull(dto, nameof(dto));

            var builder = new IdentityModule.Builder(NotificationHandler)
                .WithId(dto.Id)
                .WithName(dto.Name)
                .WithDescription(dto.Description);

            return builder;
        }
    }
}
