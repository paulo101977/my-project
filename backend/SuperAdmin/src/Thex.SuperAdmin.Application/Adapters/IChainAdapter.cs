﻿using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Infra.Entities;

namespace Thex.SuperAdmin.Application.Adapters
{
    public interface IChainAdapter
    {
        Chain.Builder MapToUpdate(Chain entity, ChainDto dto);
        Chain.Builder MapToCreate(ChainDto dto);
    }
}
