﻿using Thex.SuperAdmin.Dto;
using Thex.SuperAdmin.Infra.Entities;

namespace Thex.SuperAdmin.Application.Adapters
{
    public interface IPropertyAdapter
    {
        Property.Builder MapToUpdate(Property entity, CreateOrUpdatePropertyDto dto);
        Property.Builder MapToCreate(CreateOrUpdatePropertyDto dto);
    }
}
