﻿
using Thex.Location.Application.Interfaces;
using Thex.Location.Application.Services;
using Thex.SuperAdmin.Application.Adapters;
using Thex.SuperAdmin.Application.Interfaces;
using Thex.SuperAdmin.Application.Services;

namespace Microsoft.Extensions.DependencyInjection
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddApplicationServiceDependency(this IServiceCollection services)
        {
            services
                .AddInfraDependency();

            // Registro dos serviços
            services.AddTransient<IUserAppService, UserAppService>();
            services.AddTransient<IPersonAppService, PersonAppService>();
            services.AddTransient<IUserInvitationAppService, UserInvitationAppService>();
            services.AddTransient<IDashboardAppService, DashboardAppService>();
            services.AddTransient<ITenantAppService, TenantAppService>();
            services.AddTransient<IChainAppService, ChainAppService>();
            services.AddTransient<ICompanyAppService, CompanyAppService>();
            services.AddTransient<ILocationAppService, LocationAppService>();
            services.AddTransient<IPropertyAppService, PropertyAppService>();
            services.AddTransient<IUserTotvsAppService, UserTotvsAppService>();
            services.AddTransient<IUserTenantAppService, UserTenantAppService>();
            services.AddTransient<IBrandAppService, BrandAppService>();
            services.AddTransient<IPermissionAppService, PermissionAppService>();
            services.AddTransient<IFeatureAppService, FeatureAppService>();
            services.AddTransient<IMenuAppService, MenuAppService>();
            services.AddTransient<IIdentityModuleAppService, IdentityModuleAppService>();
            services.AddTransient<IIdentityProductAppService, IdentityProductAppService>();
            services.AddTransient<IRolesAppService, RolesAppService>();
            services.AddTransient<IUserRoleAppService, UserRoleAppService>();
            services.AddTransient<IIntegrationPartnerAppService, IntegrationPartnerAppService>();

            services.AddTransient<IApplicationModuleAdapter, ApplicationModuleAdapter>();
            services.AddTransient<IApplicationParameterAdapter, ApplicationParameterAdapter>();
            services.AddTransient<IBrandAdapter, BrandAdapter>();
            services.AddTransient<IChainAdapter, ChainAdapter>();
            services.AddTransient<ICompanyAdapter, CompanyAdapter>();
            services.AddTransient<IContactInformationAdapter, ContactInformationAdapter>();
            services.AddTransient<IContactInformationTypeAdapter, ContactInformationTypeAdapter>();
            services.AddTransient<ICountryLanguageAdapter, CountryLanguageAdapter>();
            services.AddTransient<ICountrySubdivisionAdapter, CountrySubdivisionAdapter>();
            services.AddTransient<ICountrySubdivisionTranslationAdapter, CountrySubdivisionTranslationAdapter>();
            services.AddTransient<IDocumentAdapter, DocumentAdapter>();
            services.AddTransient<IFeatureGroupAdapter, FeatureGroupAdapter>();
            services.AddTransient<IIntegrationPartnerPropertyAdapter, IntegrationPartnerPropertyAdapter>();
            services.AddTransient<ILocationAdapter, LocationAdapter>();
            services.AddTransient<ILocationCategoryAdapter, LocationCategoryAdapter>();
            services.AddTransient<IParameterTypeAdapter, ParameterTypeAdapter>();
            services.AddTransient<IPersonAdapter, PersonAdapter>();
            services.AddTransient<IPropertyAdapter, PropertyAdapter>();
            services.AddTransient<IPropertyContractAdapter, PropertyContractAdapter>();
            services.AddTransient<IPropertyParameterAdapter, PropertyParameterAdapter>();
            services.AddTransient<IPropertyTypeAdapter, PropertyTypeAdapter>();
            services.AddTransient<IStatusAdapter, StatusAdapter>();
            services.AddTransient<IStatusCategoryAdapter, StatusCategoryAdapter>();
            services.AddTransient<IUserInvitationAdapter, UserInvitationAdapter>();
            services.AddTransient<IUserTenantAdapter, UserTenantAdapter>();
            services.AddTransient<IUserAdapter, UserAdapter>();
            services.AddTransient<ITenantAdapter, TenantAdapter>();
            services.AddTransient<IRolesAdapter, RolesAdapter>();

            

            services.AddTransient<IIdentityProductAppService, IdentityProductAppService>();
            services.AddTransient<IIdentityModuleAppService, IdentityModuleAppService>();

            services.AddTransient<IIdentityFeatureAdapter, IdentityFeatureAdapter>();
            services.AddTransient<IIdentityMenuFeatureAdapter, IdentityMenuFeatureAdapter>();
            services.AddTransient<IIdentityModuleAdapter, IdentityModuleAdapter>();
            services.AddTransient<IIdentityPermissionAdapter, IdentityPermissionAdapter>();
            services.AddTransient<IIdentityPermissionFeatureAdapter, IdentityPermissionFeatureAdapter>();
            services.AddTransient<IIdentityProductAdapter, IdentityProductAdapter>();
            services.AddTransient<IIdentityRolePermissionAdapter, IdentityRolePermissionAdapter>();
            services.AddTransient<IUserPermissionAppService, UserPermissionAppService>();
            services.AddTransient<IIntegrationPartnerAppService, IntegrationPartnerAppService>();
            services.AddTransient<IUserRoleAdapter, UserRoleAdapter>();
            
            return services;
        }
    }
}