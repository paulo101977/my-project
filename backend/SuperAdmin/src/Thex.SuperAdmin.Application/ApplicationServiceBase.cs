﻿using Microsoft.AspNetCore.Identity;
using Thex.Common;
using Thex.Common.Enumerations;
using Thex.SuperAdmin.Dto.Enumerations;
using Thex.SuperAdmin.Infra.Entities;
using Thex.SuperAdmin.Infra.Interfaces;
using Tnf.Application.Services;
using Tnf.Notifications;
using Tnf.Repositories.Uow;

namespace Thex.SuperAdmin.Application.Services
{
    public abstract class ApplicationServiceBase : ApplicationService
    {
        private readonly ISimpleUnitOfWork _uow;

        protected ApplicationServiceBase(ISimpleUnitOfWork uow, INotificationHandler notification)
            : base(notification)
        {
            _uow = uow;
        }

        protected virtual void NotifyNullParameter()
        {
            Notification.Raise(Notification
                .DefaultBuilder
                .WithMessage(AppConsts.LocalizationSourceName, CommonsEnum.Error.NullOrEmptyObject)
                .Build());
        }

        protected virtual void NotifyIdIsMissing()
        {
            Notification.Raise(Notification
                .DefaultBuilder
                .WithMessage(AppConsts.LocalizationSourceName, CommonsEnum.Error.IdIsMissing)
                .Build());
        }

        protected virtual void NotifyParameterInvalid()
        {
            Notification.Raise(Notification
                .DefaultBuilder
                .WithMessage(AppConsts.LocalizationSourceName, CommonsEnum.Error.ParameterInvalid)
                .Build());
        }

        protected virtual void NotifyWhenEntityNotExist(string entityName)
        {
            Notification.Raise(Notification
                .DefaultBuilder
                .WithMessage(AppConsts.LocalizationSourceName, CommonsEnum.Error.EntityNotExist)
                .WithMessageFormat(entityName)
                .Build());
        }

        protected virtual void NotifyInvalidRangeDate()
        {
            Notification.Raise(Notification
                .DefaultBuilder
                .WithMessage(AppConsts.LocalizationSourceName, CommonsEnum.Error.DateGreaterThanAnotherDate)
                .Build());
        }

        protected virtual void NotifyMinimumCharacters(int quantity)
        {
            Notification.Raise(Notification
                .DefaultBuilder
                .WithMessage(AppConsts.LocalizationSourceName, CommonsEnum.Error.FieldMinimumOfCharacteres)
                .WithMessageFormat(quantity)
                .Build());
        }

        protected virtual void NotifyRequired(string field)
        {
            Notification.Raise(Notification
                .DefaultBuilder
                .WithMessage(AppConsts.LocalizationSourceName, CommonsEnum.Error.FieldRequired)
                .WithMessageFormat(field)
                .Build());
        }
        protected virtual void NotifyInvalidOrderBy()
        {
            Notification.Raise(Notification
                .DefaultBuilder
                .WithMessage(AppConsts.LocalizationSourceName, CommonsEnum.Error.InvalidOrderBy)
                .Build());
        }

        protected virtual void NotifyEmptyFilter()
        {
            Notification.Raise(Notification
                .DefaultBuilder
                .WithMessage(AppConsts.LocalizationSourceName, CommonsEnum.Error.EmptyFilter)
                .Build());
        }

        protected virtual void NotifyTenantIsNotAvailable()
        {
            Notification.Raise(Notification
                .DefaultBuilder
                .WithMessage(AppConsts.LocalizationSourceName, Tenant.EntityError.TenantIsNotAvailable)
                .Build());
        }

        protected virtual void NotifyIdentityErros(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                if (error.Code == IdentityErrosEnum.InvalidUserName.ToString())
                {
                    Notification.Raise(Notification.DefaultBuilder
                    .WithMessage(AppConsts.LocalizationSourceName, IdentityErrosEnum.InvalidUserName)
                    .Build());
                }
                else
                {
                    Notification.Raise(Notification.DefaultBuilder
                            .WithMessage(AppConsts.LocalizationSourceName, UserEnum.Error.CreateNewUser)
                    .WithDetailedMessage(error.Description)
                            .Build());
                }
            }
        }

        protected virtual void NotifyUserLocked()
        {
            Notification.Raise(Notification
                .DefaultBuilder
                .WithMessage(AppConsts.LocalizationSourceName, IdentityEnum.UserLocked)
                .Build());
        }

        public void Commit()
        {
            if (!Notification.HasNotification())
                _uow.Commit();
        }
    }
}
