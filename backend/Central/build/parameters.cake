public class BuildParameters
{
	public string SolutionTarget { get; private set; }
    public string ProjectTarget { get; private set; }
    public string Configuration { get; private set; }
    public string TargetFramework { get; private set; }
    public string TargetFrameworkFull { get; private set; }
    
    public static BuildParameters GetParameters(ICakeContext context)
    {
        if (context == null)
        {
            throw new ArgumentNullException("context");
        }

        var buildSystem = context.BuildSystem();

        return new BuildParameters {
            SolutionTarget = "../../Thex.Central.sln",
            ProjectTarget = "../test/Thex.Central.Web.Tests/Thex.Central.Web.Tests.csproj",
            Configuration = context.Argument("configuration", "Debug"),
            TargetFramework = "netcoreapp2.1",
            TargetFrameworkFull = "netcoreapp2.1"
        };
    }
}