﻿using Tnf.Dto;

namespace Thex.Central.Dto.RoomType
{
    public class GetAllRoomTypesDto : RequestAllDto
    {       
        public string SearchData { get; set; }
    }
}
