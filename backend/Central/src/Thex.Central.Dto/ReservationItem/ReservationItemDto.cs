﻿using System;
using Tnf.Dto;

namespace Thex.Central.Dto
{
    public partial class ReservationItemDto : BaseDto
    {
        public long Id { get; set; }
        public static ReservationItemDto NullInstance = null;

        public ReservationItemDto()
        {
        }

        public Guid ReservationItemUid { get; set; }
        public long ReservationId { get; set; }
        public int? ReasonId { get; set; }
        
        public DateTime EstimatedArrivalDate { get; set; }
        
        public DateTime? CheckInDate { get; set; }
        
        public DateTime EstimatedDepartureDate { get; set; }
        
        public DateTime? CheckOutDate { get; set; }

        public bool WalkIn { get; set; }

        public DateTime? CancellationDate { get; set; }
        public string CancellationDescription { get; set; }
        public string ReservationItemCode { get; set; }
        public int RequestedRoomTypeId { get; set; }
        public int ReceivedRoomTypeId { get; set; }
        public int? RoomId { get; set; }
        public byte AdultCount { get; set; }
        public byte ChildCount { get; set; }
        public int ReservationItemStatusId { get; set; }
        public Guid RoomLayoutId { get; set; }
        public byte ExtraBedCount { get; set; }
        public byte ExtraCribCount { get; set; }
        public Guid? RatePlanId { get; set; }
        public Guid? CurrencyId { get; set; }
        public int? MealPlanTypeId { get; set; }
        public int? GratuityTypeId { get; set; }
        public decimal? RateDaily { get; set; }

        public string CommercialBudgetName { get; set; }

        public DateTime? CheckInHour { get; set; }
        public DateTime? CheckOutHour { get; set; }

        public virtual RoomTypeDto ReceivedRoomType { get; set; }
        public virtual RoomTypeDto RequestedRoomType { get; set; }
        public virtual RoomDto Room { get; set; }
        public string ReservationItemStatusFormatted { get; set; }
    }
}
