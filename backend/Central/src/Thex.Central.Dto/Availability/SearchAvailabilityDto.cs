﻿namespace Thex.Central.Dto.Availability
{
    public class SearchAvailabilityDto
    {
        public string InitialDate { get; set; }

        public int Period { get; set; }
    }
}
