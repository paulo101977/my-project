﻿using System;
using Tnf.Dto;

namespace Thex.Central.Dto
{
    public class RoomBlockingDto : BaseDto
    {
        public Guid Id { get; set; }
        public static RoomBlockingDto NullInstance = null;

        public int PropertyId { get; set; }
        public int RoomId { get; set; }

        public DateTime BlockingStartDate { get; set; }

        public DateTime BlockingEndDate { get; set; }
        public int ReasonId { get; set; }
        public string Comments { get; set; }
        public int RoomTypeId { get; set; }
    }
}
