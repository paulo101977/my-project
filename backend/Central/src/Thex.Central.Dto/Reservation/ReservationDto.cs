﻿using System;
using System.Collections.Generic;
using Tnf.Dto;

namespace Thex.Central.Dto
{
    public partial class ReservationDto : BaseDto
    {
        public long Id { get; set; }
        public static ReservationDto NullInstance = null;

        public ReservationDto()
        {
            ReservationItemList = new HashSet<ReservationItemDto>();
        }

        public Guid ReservationUid { get; set; }
        public string ReservationCode { get; set; }
        public string ReservationVendorCode { get; set; }
        public int PropertyId { get; set; }
        public string ContactName { get; set; }
        public string ContactEmail { get; set; }
        public string ContactPhone { get; set; }
        public string InternalComments { get; set; }
        public string ExternalComments { get; set; }
        public string GroupName { get; set; }
        
        public DateTime? Deadline { get; set; }
        public bool UnifyAccounts { get; set; }
        public int? BusinessSourceId { get; set; }
        public int? MarketSegmentId { get; set; }
        public Guid? CompanyClientId { get; set; }
        public Guid? CompanyClientContactPersonId { get; set; }
        public Guid? RatePlanId { get; set; }

        public bool WalkIn { get; set; }

        public virtual ICollection<ReservationItemDto> ReservationItemList { get; set; }
    }
}
