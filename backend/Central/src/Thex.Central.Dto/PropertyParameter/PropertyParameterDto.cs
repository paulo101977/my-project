﻿using System;
using System.Collections.Generic;

namespace Thex.Central.Dto
{
    public class PropertyParameterDto
    {
        public Guid Id { get; set; }
        public static PropertyParameterDto NullInstance = null;

        public int PropertyId { get; set; }
        public int ApplicationParameterId { get; set; }
        public string PropertyParameterValue { get; set; }
        public string PropertyParameterMinValue { get; set; }
        public string PropertyParameterMaxValue { get; set; }
        public string PropertyParameterPossibleValues { get; set; }
        public bool? IsActive { get; set; }
        public IDictionary<string, string> Timezones;
    }
}
