﻿namespace Thex.Central.Dto.Room
{
    public class ParentRoomsWithTotalChildrens
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Total { get; set; }
    }
}
