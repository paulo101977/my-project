﻿using System;
using Thex.Central.Dto.Availability;
using Tnf.Application.Services;

namespace Thex.Central.Application.Interfaces
{
    public interface IAvailabilityAppService : IApplicationService
    {        
        AvailabilityDto GetAvailabilityPropertyList(SearchAvailabilityDto requestDto, DateTime? start = null, DateTime? final = null);
        AvailabilityRoomTypesDto GetAvailabilityRoomTypes(SearchAvailabilityDto requestDto, int propertyId, DateTime? start = null, DateTime? final = null);
    }
}
