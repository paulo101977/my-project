﻿using System;
using System.Collections.Generic;
using System.Linq;
using Thex.Central.Application.Interfaces;
using Thex.Central.Domain.Interfaces.Repositories;
using Thex.Central.Domain.Interfaces.Services;
using Thex.Central.Dto.Availability;
using Thex.Central.Dto.RoomType;
using Thex.Central.Infra.ReadInterfaces;
using Thex.Common;
using Thex.Common.Enumerations;
using Thex.Domain.Interfaces.Repositories;
using Thex.Kernel;
using Tnf.Notifications;

namespace Thex.Central.Application.Services.Availability
{
    public class AvailabilityAppService : ApplicationServiceBase, IAvailabilityAppService
    {
        protected readonly IRoomTypeReadRepository _roomTypeReadRepository;
        protected readonly IRoomReadRepository _roomReadRepository;
        protected readonly IRoomBlockingReadRepository _roomBlockingReadRepository;
        protected readonly IReservationItemReadRepository _reservationItemReadRepository;
        protected readonly IRoomTypeDomainService _roomTypeDomainService;
        protected readonly IRoomDomainService _roomDomainService;
        protected readonly IReservationDomainService _reservationDomainService;
        protected readonly IPropertyReadRepository _propertyReadRepository;
        private readonly IRoomTypeInventoryRepository _roomTypeInventoryRepository;
        private readonly IApplicationUser _applicationUser;

        public AvailabilityAppService(
        IRoomTypeReadRepository roomTypeReadRepository,
        IRoomReadRepository roomReadRepository,
        IReservationItemReadRepository reservationItemReadRepository,
        IRoomTypeDomainService roomTypeDomainService,
        IRoomDomainService roomDomainService,
        IReservationDomainService reservationDomainService,
        IRoomBlockingReadRepository roomBlockingReadRepository,
        INotificationHandler notificationHandler,
        IRoomTypeInventoryRepository roomTypeInventoryRepository,
        IApplicationUser applicationUser,
        IPropertyReadRepository propertyReadRepository)
        : base(notificationHandler)
        {
            _roomTypeReadRepository = roomTypeReadRepository;
            _roomReadRepository = roomReadRepository;
            _reservationItemReadRepository = reservationItemReadRepository;
            _roomTypeDomainService = roomTypeDomainService;
            _roomDomainService = roomDomainService;
            _reservationDomainService = reservationDomainService;
            _roomBlockingReadRepository = roomBlockingReadRepository;
            _propertyReadRepository = propertyReadRepository;
            _roomTypeInventoryRepository = roomTypeInventoryRepository;
            _applicationUser = applicationUser;
        }

        public AvailabilityAppService(
            INotificationHandler notificationHandler)
            : base(notificationHandler)
        {
        }

        public AvailabilityDto GetAvailabilityPropertyList(SearchAvailabilityDto requestDto, DateTime? start = null, DateTime? final = null)
        {
            if (requestDto != null && !start.HasValue && !final.HasValue)
                ValidateAvailability(requestDto);

            if (Notification.HasNotification())
                return null;

            var startDate = start ?? Convert.ToDateTime(requestDto.InitialDate);
            var finalDate = final ?? Convert.ToDateTime(requestDto.InitialDate).AddDays(requestDto.Period);

            var propertiesIds = _applicationUser.PropertyList.Select(p => int.Parse(p.PropertyId)).ToList();
            var propertyList = _propertyReadRepository.GetAvailabilityPropertyRowDto(propertiesIds);
            var roomTypeInventoryList = _roomTypeInventoryRepository.GetAllByFilters(startDate.AddDays(-1), finalDate.AddDays(1), propertiesIds).ToList();
            var availabilityList = _roomTypeDomainService.GetAvailabilityPropertyColumns(roomTypeInventoryList);

            var footerList = _roomTypeDomainService.GetAvailabilityTotals(availabilityList, propertyList, startDate, finalDate);        

            return new AvailabilityDto()
            {
                PropertyList = propertyList,
                AvailabilityList = availabilityList,
                FooterList = footerList
            };
        }

        private void ValidateAvailability(SearchAvailabilityDto requestDto)
        {
            var validPeriods = new int[] { 7, 15, 30, 31}.ToList();

            if (!validPeriods.Contains(requestDto.Period))
                Notification.Raise(Notification.DefaultBuilder.WithMessage(AppConsts.LocalizationSourceName, CommonsEnum.Error.InvalidPeriodParameterAvailability).Build());

            if (!DateTime.TryParse(requestDto.InitialDate, out DateTime dateTime))
                Notification.Raise(Notification.DefaultBuilder.WithMessage(AppConsts.LocalizationSourceName, CommonsEnum.Error.InvalidDateParameterAvailability).Build());
        }

        private void ValidateAvailability(SearchAvailabilityDto requestDto, int propertyId)
        {
            if (propertyId <= 0)
                NotifyIdIsMissing();

            var validPeriods = new int[] { 7, 15, 30 }.ToList();

            if (!validPeriods.Contains(requestDto.Period))
                Notification.Raise(Notification.DefaultBuilder.WithMessage(AppConsts.LocalizationSourceName, CommonsEnum.Error.InvalidPeriodParameterAvailability).Build());

            DateTime dateTime;
            if (!DateTime.TryParse(requestDto.InitialDate, out dateTime))
                Notification.Raise(Notification.DefaultBuilder.WithMessage(AppConsts.LocalizationSourceName, CommonsEnum.Error.InvalidDateParameterAvailability).Build());
        }

        private void ValidateAvailabilityWithProperty(SearchAvailabilityDto requestDto, int propertyId)
        {
            if (propertyId <= 0)
                NotifyIdIsMissing();

            ValidateAvailability(requestDto);
        }

        public AvailabilityRoomTypesDto GetAvailabilityRoomTypes(SearchAvailabilityDto requestDto, int propertyId, DateTime? start = null, DateTime? final = null)
        {
            if (requestDto != null && !start.HasValue && !final.HasValue)
                ValidateAvailability(requestDto, propertyId);

            if (Notification.HasNotification())
                return null;

            var startDate = start ?? Convert.ToDateTime(requestDto.InitialDate);
            var finalDate = final ?? Convert.ToDateTime(requestDto.InitialDate).AddDays(requestDto.Period);

            var parentRoomsWithTotals = _roomReadRepository.GetParentRoomsWithTotalChildrens(propertyId);
            var reservationItems = _reservationItemReadRepository.GetAvailabilityReservationsItemsByPeriodAndPropertyId(startDate, finalDate, propertyId, false);
            var roomTypesList = _roomTypeReadRepository.GetAvailabilityRoomTypeRow(propertyId);
            var roomBlockedList = _roomBlockingReadRepository.GetAllRoomsBlockedByPropertyId(propertyId, startDate.Date, finalDate.Date);

            var roomTypeInventoryList = _roomTypeInventoryRepository.GetAllByFilters(startDate.AddDays(-1), finalDate.AddDays(1), new List<int>() { propertyId }).ToList();
            var availabilityList = _roomTypeDomainService.GetAvailabilityRoomTypeColumns(parentRoomsWithTotals, reservationItems, roomTypesList, roomBlockedList, roomTypeInventoryList);

            var footerList = _roomTypeDomainService.GetAvailabilityTotalsByProperty(availabilityList, roomBlockedList, roomTypesList, startDate, finalDate);

            _roomTypeDomainService.FillRolesInAvailabilityRoomTypeColumns(roomTypesList, availabilityList, startDate, finalDate);

            availabilityList = availabilityList.OrderBy(a => a.DateFormatted).ThenBy(a => a.RoomTypeId).ToList();

            return new AvailabilityRoomTypesDto()
            {
                RoomTypeList = roomTypesList,
                AvailabilityList = availabilityList,
                FooterList = footerList
            };
        }       
    }
}
