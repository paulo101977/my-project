﻿using Microsoft.Extensions.DependencyInjection;
using Thex.Central.Application.Interfaces;
using Thex.Central.Application.Services.Availability;
using Thex.Central.Domain;

namespace Thex.Central.Application
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddCentralApplicationServiceDependency(this IServiceCollection services)
        {
            services
                .AddDomainDependency()
                .AddTnfDefaultConventionalRegistrations();

            // Registro dos serviços
            services.AddScoped<IAvailabilityAppService, AvailabilityAppService>();

            return services;
        }
    }
}
