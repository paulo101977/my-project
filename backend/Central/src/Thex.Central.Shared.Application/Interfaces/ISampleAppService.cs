﻿using System;
using Tnf.Dto;
using CentralDto = Thex.Central.Dto.Availability;
using ThexDto = Thex.Dto;

namespace Thex.Central.Shared.Application
{
    public interface ISampleAppService
    {
        CentralDto.AvailabilityDto GetAvailabilityPropertyList(CentralDto.SearchAvailabilityDto requestDto, DateTime? start = null, DateTime? final = null);
        CentralDto.AvailabilityRoomTypesDto GetAvailabilityRoomTypes(CentralDto.SearchAvailabilityDto requestDto, int propertyId, DateTime? start = null, DateTime? final = null);

        IListDto<ThexDto.BusinessSourceDto> GetAllByPropertyId(ThexDto.GetAllBusinessSourceDto request, int propertyId);
    }
}
