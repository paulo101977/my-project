﻿using Microsoft.Extensions.DependencyInjection;
using Thex.Application;
using Thex.Maps.Geocode;
using System;
using Thex.Central.Application;
using Thex.Infra.SqlServer;
using Thex.Central.Infra.SqlServer;
using Thex.Inventory;

namespace Thex.Central.Shared.Application
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddCentralSharedApplicationServiceDependency(this IServiceCollection services, Action<MapsGeocodeConfig> mapsGeocodeConfig)
        {
            services
                .AddTnfDefaultConventionalRegistrations();

            services.AddPMSApplicationServiceDependency(mapsGeocodeConfig);
            services.AddCentralApplicationServiceDependency();

            services.AddSqlServerDependency();
            services.AddCentralSqlServerDependency();

            services.AddInventoryDependency();

            return services;
        }
    }
}
