﻿using System;
using System.Linq;
using CentralDto = Thex.Central.Dto.Availability;
using Tnf.Notifications;
using CentrapApp = Thex.Central.Application.Interfaces;
using ThexApp = Thex.Application.Interfaces;
using ThexDto = Thex.Dto;
using Tnf.Dto;
using Thex.Kernel;
using System.Security.Principal;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;

namespace Thex.Central.Shared.Application.Services
{
    public class SampleAppService : ApplicationServiceBase, ISampleAppService
    {
        protected readonly CentrapApp.IAvailabilityAppService _availabilityAppService;
        protected readonly ThexApp.IBusinessSourceAppService _businessSourceAppService;
        protected readonly IApplicationUser _applicationUser;

        public SampleAppService(
            INotificationHandler notificationHandler,
            CentrapApp.IAvailabilityAppService availabilityAppService,
            ThexApp.IBusinessSourceAppService businessSourceAppService,
            IApplicationUser applicationUser)
        : base(notificationHandler)
        {
            _availabilityAppService = availabilityAppService;
            _businessSourceAppService = businessSourceAppService;
            _applicationUser = applicationUser;

            //alternativa para setar manualmente o tenant
            //(_applicationUser as ApplicationUserCentral).SetProperties(System.Guid.NewGuid().ToString(), "11", "11", "11", "pt-br", "pt-br", "America/Sao_Paulo", "BR", "BR");
        }

        public CentralDto.AvailabilityDto GetAvailabilityPropertyList(CentralDto.SearchAvailabilityDto requestDto, DateTime? start = null, DateTime? final = null)
         => _availabilityAppService.GetAvailabilityPropertyList(requestDto, start = null, final);

        public CentralDto.AvailabilityRoomTypesDto GetAvailabilityRoomTypes(CentralDto.SearchAvailabilityDto requestDto, int propertyId, DateTime? start = null, DateTime? final = null)
         => _availabilityAppService.GetAvailabilityRoomTypes(requestDto, propertyId, start, final);

        public IListDto<ThexDto.BusinessSourceDto> GetAllByPropertyId(ThexDto.GetAllBusinessSourceDto request, int propertyId)
         => _businessSourceAppService.GetAllByPropertyId(request, propertyId);
    }
}
