﻿using Microsoft.Extensions.DependencyInjection;
using Thex.Central.Domain.Interfaces.Repositories;
using Thex.Central.Infra.ReadInterfaces;
using Thex.Central.Infra.Repositories;
using Thex.Central.Infra.Repositories.Read;
using Thex.Domain.Interfaces.Repositories;

namespace Thex.Central.Infra
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddInfraDependency(this IServiceCollection services)
        {
            services
                .AddTnfEntityFrameworkCore();   // Configura o uso do EntityFrameworkCore registrando os contextos que serão usados pela aplicação

            services.AddTnfDefaultConventionalRegistrations();

            services.AddScoped<IPropertyParameterReadRepository, PropertyParameterReadRepository>();
            services.AddScoped<IReservationItemReadRepository, ReservationItemReadRepository>();
            services.AddScoped<IRoomReadRepository, RoomReadRepository>();
            services.AddScoped<IRoomBlockingReadRepository, RoomBlockingReadRepository>();
            services.AddScoped<IRoomTypeReadRepository, RoomTypeReadRepository>();
            services.AddScoped<ITenantReadRepository, TenantReadRepository>();
            services.AddScoped<IRoomTypeInventoryRepository, RoomTypeInventoryRepository>();

            return services;
        }
    }
}
