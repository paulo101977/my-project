﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using Thex.Central.Domain.Entities;
using Thex.Central.Domain.Interfaces.Repositories;
using Thex.Central.Infra.Context;
using Tnf.EntityFrameworkCore;
using Tnf.EntityFrameworkCore.Repositories;

namespace Thex.Central.Infra.Repositories
{
    public class RoomTypeInventoryRepository : EfCoreRepositoryBase<ThexContext, RoomTypeInventory>, IRoomTypeInventoryRepository
    {
        public RoomTypeInventoryRepository(IDbContextProvider<ThexContext> dbContextProvider)
            : base(dbContextProvider)
        {
        }

        public IEnumerable<RoomTypeInventory> GetAllByFilters(DateTime initialDate, DateTime endDate, List<int> propertiesIds)
        {
            return Context.RoomTypeInventories.Where(r => r.Date.Date >= initialDate.Date && 
                                                          r.Date.Date <= endDate.Date && 
                                                          propertiesIds.Contains(r.PropertyId) &&
                                                          !r.IsDeleted)
                                                          .IgnoreQueryFilters();
        }
    }
}
