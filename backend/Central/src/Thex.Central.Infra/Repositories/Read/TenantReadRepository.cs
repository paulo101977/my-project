﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using Thex.Central.Infra.Context;
using Thex.Central.Infra.ReadInterfaces;
using Thex.Kernel;
using Tnf.EntityFrameworkCore;

namespace Thex.Central.Infra.Repositories.Read
{
    public class TenantReadRepository : ITenantReadRepository
    {
        private IApplicationUser _applicationUser;
        private IConfiguration _configuration;

        private Guid _tenantId = Guid.Empty;
        private Guid _userUid = Guid.Empty;
        private List<Guid> _tenantIds = null;

        public List<Guid> TenantIdList { get => GetTenantIds(); }

        public TenantReadRepository(
            IDbContextProvider<ThexContext> dbContextProvider,
            IApplicationUser applicationUser,
            IConfiguration configuration)
            //: base(dbContextProvider)
        {
            _configuration = configuration;
            _applicationUser = applicationUser;

            _tenantId = applicationUser.TenantId;
            _userUid = applicationUser.UserUid;
            _tenantIds = null;
        }

        public List<Guid> GetTenantIds()
        {
            return new List<Guid> { Guid.NewGuid() };
        }
    }
}
