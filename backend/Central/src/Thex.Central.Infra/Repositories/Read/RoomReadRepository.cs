﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using Thex.Central.Domain.Entities;
using Thex.Central.Dto.Room;
using Thex.Central.Infra.Context;
using Thex.Domain.Interfaces.Repositories;
using Tnf.EntityFrameworkCore;
using Tnf.EntityFrameworkCore.Repositories;
using System.Linq;

namespace Thex.Central.Infra.Repositories.Read
{
    public class RoomReadRepository : EfCoreRepositoryBase<ThexContext, Room>, IRoomReadRepository
    {
        public RoomReadRepository(
            IDbContextProvider<ThexContext> dbContextProvider
            )
            : base(dbContextProvider)
        {
        }

        public List<ParentRoomsWithTotalChildrens> GetParentRoomsWithTotalChildrens()
        {
            var parentRooms = (from parentRoom in Context.Rooms.AsNoTracking()
                               where parentRoom.ParentRoomId.HasValue
                               select parentRoom.ParentRoomId);

            return (from r in Context.Rooms.AsNoTracking()
                    where parentRooms.Contains(r.Id)
                    select new ParentRoomsWithTotalChildrens
                    {
                        Id = r.Id,
                        Name = r.RoomNumber,
                        Total = (from rt in Context.Rooms
                                 where rt.ParentRoomId == r.Id
                                 select rt).Count()
                    })
                     .ToList();
        }

        public List<ParentRoomsWithTotalChildrens> GetParentRoomsWithTotalChildrens(int propertyId)
        {
            var parentRooms = (from parentRoom in Context.Rooms.AsNoTracking()
                               where parentRoom.PropertyId == propertyId
                               && parentRoom.ParentRoomId.HasValue
                               select parentRoom.ParentRoomId);

            return (from r in Context.Rooms.AsNoTracking()
                    where r.PropertyId == propertyId &&
                    parentRooms.Contains(r.Id)
                    select new ParentRoomsWithTotalChildrens
                    {
                        Id = r.Id,
                        Name = r.RoomNumber,
                        Total = (from rt in Context.Rooms
                                 where rt.ParentRoomId == r.Id
                                 select rt).Count()
                    })
                     .ToList();
        }
    }
}
