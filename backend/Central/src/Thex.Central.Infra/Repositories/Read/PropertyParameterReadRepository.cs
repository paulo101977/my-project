﻿using System;
using System.Linq;
using Thex.Central.Domain.Entities;
using Thex.Central.Dto;
using Thex.Central.Infra.Context;
using Thex.Central.Infra.ReadInterfaces;
using Tnf.EntityFrameworkCore;
using Tnf.EntityFrameworkCore.Repositories;

namespace Thex.Central.Infra.Repositories.Read
{
    public class PropertyParameterReadRepository : EfCoreRepositoryBase<ThexContext, PropertyParameter>, IPropertyParameterReadRepository
    {
        public PropertyParameterReadRepository(
            IDbContextProvider<ThexContext> dbContextProvider
            )
            : base(dbContextProvider)
        {
        }

        public PropertyParameterDto GetPropertyParameterById(Guid propertyParameterId)
        {
            return Context
                .PropertyParameters
                .Where(x => x.Id == propertyParameterId).FirstOrDefault().MapTo<PropertyParameterDto>();
        }
    }
}
