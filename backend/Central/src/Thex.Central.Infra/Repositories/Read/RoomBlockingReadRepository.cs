﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Collections.Generic;
using Thex.Central.Domain.Entities;
using Thex.Central.Dto;
using Thex.Central.Infra.Context;
using Thex.Central.Infra.ReadInterfaces;
using Tnf.EntityFrameworkCore;
using Tnf.EntityFrameworkCore.Repositories;

namespace Thex.Central.Infra.Repositories.Read
{
    public class RoomBlockingReadRepository : EfCoreRepositoryBase<ThexContext, RoomBlocking>, IRoomBlockingReadRepository
    {
        public RoomBlockingReadRepository(
            IDbContextProvider<ThexContext> dbContextProvider
            )
            : base(dbContextProvider)
        {
        }

        public List<RoomBlockingDto> GetAllRoomsBlockedByPropertyId(int propertyId, DateTime initialDate, DateTime finalDate)
        {
            return (from roomBlocking in Context.RoomBlockings.AsNoTracking()
                    join room in Context.Rooms.AsNoTracking() on roomBlocking.RoomId equals room.Id

                    where roomBlocking.PropertyId == propertyId && !roomBlocking.IsDeleted &&
                    roomBlocking.BlockingStartDate.Date <= finalDate.Date &&
                    roomBlocking.BlockingEndDate.Date >= initialDate.Date
                    select new RoomBlockingDto
                    {
                        RoomId = roomBlocking.RoomId,
                        BlockingStartDate = roomBlocking.BlockingStartDate,
                        BlockingEndDate = roomBlocking.BlockingEndDate,
                        Id = roomBlocking.Id,
                        RoomTypeId = room.RoomTypeId
                    }).ToList();
        }

        public List<RoomBlockingDto> GetAllRoomsBlocked(DateTime initialDate, DateTime finalDate)
        {
            return (from roomBlocking in Context.RoomBlockings.AsNoTracking()
                    join room in Context.Rooms.AsNoTracking() on roomBlocking.RoomId equals room.Id

                    where !roomBlocking.IsDeleted &&
                    roomBlocking.BlockingStartDate.Date <= finalDate.Date &&
                    roomBlocking.BlockingEndDate.Date >= initialDate.Date
                    select new RoomBlockingDto
                    {
                        PropertyId = roomBlocking.PropertyId,
                        RoomId = roomBlocking.RoomId,
                        BlockingStartDate = roomBlocking.BlockingStartDate,
                        BlockingEndDate = roomBlocking.BlockingEndDate,
                        Id = roomBlocking.Id,
                        RoomTypeId = room.RoomTypeId
                    }).ToList();
        }
    }
}
