﻿using System.Collections.Generic;
using Thex.Central.Domain.Entities;
using Thex.Central.Infra.Context;
using Thex.Central.Infra.ReadInterfaces;
using Tnf.EntityFrameworkCore;
using Tnf.EntityFrameworkCore.Repositories;
using System.Linq;
using Thex.Central.Dto.RoomType;
using Thex.Central.Dto.Availability;
using Microsoft.EntityFrameworkCore;

namespace Thex.Central.Infra.Repositories.Read
{
    public class RoomTypeReadRepository : EfCoreRepositoryBase<ThexContext, RoomType>, IRoomTypeReadRepository
    {
        public RoomTypeReadRepository(
            IDbContextProvider<ThexContext> dbContextProvider
            )
            : base(dbContextProvider)
        {
        }

        public List<RoomType> GetRoomTypesByPropertyId(GetAllRoomTypesDto request, int propertyId)
        {
            var baseQuery = QueryOfGetRoomTypesByPropertyId(propertyId);

            if (!DoesNotHaveFilter(request))
            {
                baseQuery = baseQuery
                    .Where(r => RoomTypeContainsRoomTypeAbbreviation(r, request.SearchData));
            }       

            return baseQuery.OrderBy(x => x.Order).ToList(); 
        }

        private IQueryable<RoomType> QueryOfGetRoomTypesByPropertyId(int propertyId)
        {
            var roomTypes = Context.RoomTypes
                .Where(r => r.PropertyId == propertyId);

            return roomTypes;
        }

        private bool DoesNotHaveFilter(GetAllRoomTypesDto request)
        {
            return request != null && string.IsNullOrEmpty(request.SearchData);
        }

        private bool RoomTypeContainsRoomTypeAbbreviation(RoomType roomTypeDto, string searchData)
        {
            return roomTypeDto.Abbreviation.Contains(searchData);
        }

        public List<AvailabilityRoomTypeRowDto> GetAvailabilityRoomTypeRow(int propertyId, int? roomTypeId = null)
        {
            var parentRooms = (from parentRoom in Context.Rooms.AsNoTracking()
                               where parentRoom.PropertyId == propertyId
                      && parentRoom.ParentRoomId.HasValue
                               select parentRoom.ParentRoomId);

            var dbBaseQuery = (from rt in Context.RoomTypes.AsNoTracking()
                               join r in Context.Rooms.AsNoTracking() on rt.Id equals r.RoomTypeId
                               where rt.PropertyId == propertyId &&
                               !parentRooms.Contains(r.Id)
                               group rt by new { rt.Id, rt.Name, rt.Order } into rtGroup
                               orderby rtGroup.Key.Order
                               select new AvailabilityRoomTypeRowDto
                               {
                                   Id = rtGroup.Key.Id,
                                   Name = rtGroup.Key.Name,
                                   Total = rtGroup.Count(),
                                   Order = rtGroup.Key.Order
                               });

            if (roomTypeId.HasValue)
                dbBaseQuery.Where(e => e.Id == roomTypeId);

            return dbBaseQuery.ToList();
        }
    }
}
