﻿using System.Collections.Generic;
using Thex.Central.Domain.Entities;
using Thex.Central.Infra.Context;
using Thex.Central.Infra.ReadInterfaces;
using Tnf.EntityFrameworkCore;
using Tnf.EntityFrameworkCore.Repositories;
using System.Linq;
using Thex.Central.Dto;
using Microsoft.EntityFrameworkCore;
using Thex.Central.Dto.Availability;
using Thex.Kernel;

namespace Thex.Central.Infra.Repositories.Read
{
    public class PropertyReadRepository : EfCoreRepositoryBase<ThexContext, Property>, IPropertyReadRepository
    {
        private readonly ITenantReadRepository _tenantReadRepository;
        private readonly IApplicationUser _applicationUser;

        public PropertyReadRepository(
            IDbContextProvider<ThexContext> dbContextProvider,
            ITenantReadRepository tenantReadRepository
            )
            : base(dbContextProvider)
        {
            _tenantReadRepository = tenantReadRepository;
        }

        public List<AvailabilityPropertyRowDto> GetAvailabilityPropertyRowDto(List<int> propertiesIds)
        {
            var parentRooms = (from parentRoom in Context.Rooms.AsNoTracking()
                               where parentRoom.ParentRoomId.HasValue
                               select parentRoom.ParentRoomId);

            var dbBaseQuery = (from p in Context.Properties.Where(p => !p.IsDeleted).AsNoTracking().IgnoreQueryFilters()
                               join r in Context.Rooms.Where(r => !r.IsDeleted).AsNoTracking().IgnoreQueryFilters() on p.Id equals r.PropertyId
                               where !parentRooms.Contains(r.Id) && propertiesIds.Contains(p.Id)
                               group p by new { p.Id, p.Name } into rtGroup
                               orderby rtGroup.Key.Name
                               select new AvailabilityPropertyRowDto
                               {
                                   Id = rtGroup.Key.Id,
                                   Name = rtGroup.Key.Name,
                                   Total = rtGroup.Count()                                   
                               });          

            return dbBaseQuery.ToList();
        }
    }
}
