﻿using System;
using System.Linq;
using System.Collections.Generic;
using Thex.Central.Domain.Entities;
using Thex.Central.Infra.Context;
using Thex.Central.Infra.ReadInterfaces;
using Thex.Common.Enumerations;
using Tnf.EntityFrameworkCore;
using Tnf.EntityFrameworkCore.Repositories;
using Microsoft.EntityFrameworkCore;

namespace Thex.Central.Infra.Repositories.Read
{
    public class ReservationItemReadRepository : EfCoreRepositoryBase<ThexContext, ReservationItem>, IReservationItemReadRepository
    {
        public ReservationItemReadRepository(
            IDbContextProvider<ThexContext> dbContextProvider
            )
            : base(dbContextProvider)
        {
        }

        public List<ReservationItem> GetAvailabilityReservationsItemsByPeriodAndPropertyId(DateTime initialDate, DateTime finalDate, int propertyId, bool? completeReservationItemInformations = true, int? roomTypeId = null)
        {
            var status = new int[] { (int)ReservationStatus.ToConfirm, (int)ReservationStatus.Confirmed, (int)ReservationStatus.Checkin }.ToList();

            var dbBaseQuery = (from ri in Context.ReservationItems.AsNoTracking()
                               join r in Context.Reservations.AsNoTracking()
                               on ri.ReservationId equals r.Id
                               where r.PropertyId == propertyId &&
                               (ri.CheckInDate ?? ri.EstimatedArrivalDate).Date <= finalDate.Date &&
                               status.Contains(ri.ReservationItemStatusId) &&
                               (ri.CheckOutDate ?? ri.EstimatedDepartureDate).Date >= initialDate.Date
                               select ri);

            if (roomTypeId.HasValue)
                dbBaseQuery = dbBaseQuery.Where(e => e.ReceivedRoomTypeId == roomTypeId.Value);

            if (completeReservationItemInformations.HasValue && !completeReservationItemInformations.Value)
                dbBaseQuery.Select(ri => new ReservationItem
                {
                    Id = ri.Id,
                    CheckInDate = ri.CheckInDate,
                    CheckOutDate = ri.CheckOutDate,
                    EstimatedArrivalDate = ri.EstimatedArrivalDate,
                    EstimatedDepartureDate = ri.EstimatedDepartureDate,
                    ReceivedRoomTypeId = ri.ReceivedRoomTypeId,
                    RoomId = ri.RoomId
                });

            return dbBaseQuery.ToList();
        }

        //used only in the unavailability grid display
        public List<ReservationItem> GetAvailabilityReservationsItemsByPeriod(DateTime initialDate, DateTime finalDate, bool? completeReservationItemInformations = true, int? roomTypeId = null)
        {
            var status = new int[] { (int)ReservationStatus.ToConfirm, (int)ReservationStatus.Confirmed, (int)ReservationStatus.Checkin }.ToList();        

            var dbBaseQuery = Context.ReservationItems.AsNoTracking()
                                           .Include(r => r.Reservation)                                         
                                           .Where(ri => (ri.CheckInDate ?? ri.EstimatedArrivalDate).Date <= finalDate.Date &&
                                           status.Contains(ri.ReservationItemStatusId) &&
                                           (ri.CheckOutDate ?? ri.EstimatedDepartureDate).Date >= initialDate.Date);

            if (roomTypeId.HasValue)
                dbBaseQuery = dbBaseQuery.Where(e => e.ReceivedRoomTypeId == roomTypeId.Value);

            if (completeReservationItemInformations.HasValue && !completeReservationItemInformations.Value)
                dbBaseQuery.Select(ri => new ReservationItem
                {
                    Id = ri.Id,
                    CheckInDate = ri.CheckInDate,
                    CheckOutDate = ri.CheckOutDate,
                    EstimatedArrivalDate = ri.EstimatedArrivalDate,
                    EstimatedDepartureDate = ri.EstimatedDepartureDate,
                    ReceivedRoomTypeId = ri.ReceivedRoomTypeId,
                    RoomId = ri.RoomId,
                    Reservation = ri.Reservation
                });

            return dbBaseQuery.ToList();
        }        
    }
}
