﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.Extensions.Configuration;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Thex.Central.Domain.Entities;
using Thex.Central.Infra.Context.Builders;
using Thex.Common;
using Thex.GenericLog;
using Thex.Kernel;
using Tnf.EntityFrameworkCore;
using Tnf.Runtime.Session;

namespace Thex.Central.Infra.Context
{
    public class ThexContext2 : TnfDbContext
    {
        private IGenericLogHandler _genericLog;
        protected IApplicationUser _applicationUser;
        private IConfiguration _configuration;
        protected Guid _tenantId = Guid.Empty;
        protected Guid _userUid = Guid.Empty;

        public ThexContext2(IApplicationUser applicationUser,
                           IConfiguration configuration,
                           DbContextOptions<ThexContext> options,
                           ITnfSession session,
                           IGenericLogHandler genericLog)
                : base(options, session)
        {
            _applicationUser = applicationUser;
            _configuration = configuration;
            _genericLog = genericLog;
            _tenantId = applicationUser.TenantId != Guid.Empty ? applicationUser.TenantId : Guid.Empty;
            _userUid = applicationUser.UserUid != Guid.Empty ? applicationUser.UserUid : Guid.Empty;
        }

        public override int SaveChanges()
        {
            return base.SaveChanges();
        }

        public override async Task<int> SaveChangesAsync(CancellationToken cancellationToken = default(CancellationToken))
        {
            return (await base.SaveChangesAsync(true, cancellationToken));
        }
    }

    public abstract class ThexContext : ThexContext2
    {
        private IGenericLogHandler _genericLog;
        protected IApplicationUser _applicationUser;
        private IConfiguration _configuration;
        protected Guid _tenantId = Guid.Empty;
        protected Guid _userUid = Guid.Empty;

        public ThexContext(IApplicationUser applicationUser,
                           IConfiguration configuration,
                           DbContextOptions<ThexContext> options,
                           ITnfSession session,
                           IGenericLogHandler genericLog)
                : base(applicationUser, configuration, options, session, genericLog)
        {
            _applicationUser = applicationUser;
            _configuration = configuration;
            _genericLog = genericLog;
            _tenantId = applicationUser.TenantId != Guid.Empty ? applicationUser.TenantId : Guid.Empty;
            _userUid = applicationUser.UserUid != Guid.Empty ? applicationUser.UserUid : Guid.Empty;
        }

        public virtual DbSet<Property> Properties { get; set; }
        public virtual DbSet<PropertyParameter> PropertyParameters { get; set; }
        public virtual DbSet<Reservation> Reservations { get; set; }
        public virtual DbSet<ReservationItem> ReservationItems { get; set; }
        public virtual DbSet<Room> Rooms { get; set; }
        public virtual DbSet<RoomBlocking> RoomBlockings { get; set; }
        public virtual DbSet<RoomType> RoomTypes { get; set; }
        public virtual DbSet<RoomTypeInventory> RoomTypeInventories { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new PropertyMapper());
            modelBuilder.ApplyConfiguration(new PropertyParameterMapper());
            modelBuilder.ApplyConfiguration(new ReservationMapper());
            modelBuilder.ApplyConfiguration(new ReservationItemMapper());
            modelBuilder.ApplyConfiguration(new RoomMapper());
            modelBuilder.ApplyConfiguration(new RoomBlockingMapper());
            modelBuilder.ApplyConfiguration(new RoomTypeMapper());
            modelBuilder.ApplyConfiguration(new RoomTypeInventoryMapper());

            modelBuilder.Entity<Property>().HasQueryFilter(b => EF.Property<Guid>(b, "TenantId") == _tenantId && !b.IsDeleted);
            modelBuilder.Entity<PropertyParameter>().HasQueryFilter(b => EF.Property<Guid>(b, "TenantId") == _tenantId && !b.IsDeleted);
            modelBuilder.Entity<Reservation>().HasQueryFilter(b => EF.Property<Guid>(b, "TenantId") == _tenantId && !b.IsDeleted);
            modelBuilder.Entity<ReservationItem>().HasQueryFilter(b => EF.Property<Guid>(b, "TenantId") == _tenantId && !b.IsDeleted);
            modelBuilder.Entity<Room>().HasQueryFilter(b => EF.Property<Guid>(b, "TenantId") == _tenantId && !b.IsDeleted);

            modelBuilder.Entity<RoomBlocking>().HasQueryFilter(b => EF.Property<Guid>(b, "TenantId") == _tenantId && !b.IsDeleted);
            modelBuilder.Entity<RoomTypeInventory>().HasQueryFilter(b => EF.Property<Guid>(b, "TenantId") == _tenantId && !b.IsDeleted);

            base.OnModelCreating(modelBuilder.EnableAutoHistory(null));
        }

        public virtual void FillAuditFiedls()
        {
            // custom code...
            var utcNowAuditDate = DateTime.UtcNow.ToZonedDateTimeLoggedUser();

            var entriesWithBase = ChangeTracker.Entries();

            var FullAuditedEntries = entriesWithBase
                .Where(entry => (entry.Metadata.ClrType.BaseType != null && entry.Metadata.ClrType.BaseType.Name.Contains("ThexFullAuditedEntity")) ||
                                (entry.Metadata.ClrType.BaseType.BaseType != null && entry.Metadata.ClrType.BaseType.BaseType.Name.Contains("ThexFullAuditedEntity")));

            if (FullAuditedEntries != null)
                foreach (EntityEntry entityEntry in FullAuditedEntries)
                {
                    var entity = entityEntry.Entity;
                    switch (entityEntry.State)
                    {
                        case EntityState.Added:
                            entity.GetType().GetProperty("CreationTime").SetValue(entity, utcNowAuditDate, null);
                            entity.GetType().GetProperty("LastModificationTime").SetValue(entity, utcNowAuditDate, null);
                            entity.GetType().GetProperty("CreatorUserId").SetValue(entity, _userUid, null);
                            break;
                        case EntityState.Modified:
                            entityEntry.Property("CreationTime").IsModified = false;
                            entityEntry.Property("CreatorUserId").IsModified = false;

                            entity.GetType().GetProperty("LastModificationTime").SetValue(entity, utcNowAuditDate, null);
                            entity.GetType().GetProperty("LastModifierUserId").SetValue(entity, _userUid, null);
                            break;
                        case EntityState.Deleted:
                            entityEntry.Property("CreationTime").IsModified = false;
                            entityEntry.Property("CreatorUserId").IsModified = false;
                            entityEntry.Property("LastModificationTime").IsModified = false;
                            entityEntry.Property("LastModifierUserId").IsModified = false;

                            entity.GetType().GetProperty("DeletionTime").SetValue(entity, utcNowAuditDate, null);
                            entity.GetType().GetProperty("DeleterUserId").SetValue(entity, _userUid, null);
                            entity.GetType().GetProperty("IsDeleted").SetValue(entity, true);
                            entityEntry.State = EntityState.Modified;
                            break;
                    }
                }


            var MultiTentantEntries = entriesWithBase
                .Where(entry => (entry.Metadata.ClrType.BaseType != null && entry.Metadata.ClrType.BaseType.Name.Contains("ThexMultiTenantFullAuditedEntity")) ||
                                (entry.Metadata.ClrType.BaseType.BaseType != null && entry.Metadata.ClrType.BaseType.BaseType.Name.Contains("ThexMultiTenantFullAuditedEntity")));

            if (MultiTentantEntries != null)
                foreach (EntityEntry entityEntry in MultiTentantEntries)
                {
                    var entity = entityEntry.Entity;
                    switch (entityEntry.State)
                    {
                        case EntityState.Added:
                            if (!entity.GetType().ToString().Contains("UserPassword"))
                                entity.GetType().GetProperty("TenantId").SetValue(entity, _tenantId, null);
                            break;
                        case EntityState.Modified:
                        case EntityState.Deleted:
                            entityEntry.Property("TenantId").IsModified = false;
                            break;
                    }
                }
        }

        public override int SaveChanges()
        {
            FillAuditFiedls();

            _genericLog.AddLog(_genericLog.DefaultBuilder
                       .WithMessage(this.EnsureAutoHistory())
                       .WithDetailedMessage(AppConsts.DatabaseModification)
                       .AsInformation()
                       .Build());

            return base.SaveChanges();
        }

        public override async Task<int> SaveChangesAsync(CancellationToken cancellationToken = default(CancellationToken))
        {
            //if(_startup.UsaThex)
            //    FillAuditFiedls();

            return (await base.SaveChangesAsync(true, cancellationToken));
        }
    }
}
