﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;
using Thex.Central.Domain.Entities;

namespace Thex.Central.Infra.Context.Builders
{
    public class RoomMapper : IEntityTypeConfiguration<Room>
    {
        public void Configure(EntityTypeBuilder<Room> builder)
        {

            builder.ToTable("Room");

            builder.HasAnnotation("Relational:TableName", "Room");

            builder.HasIndex(e => e.PropertyId)
                .HasName("x_Room_PropertyId");

            builder.HasIndex(e => e.RoomTypeId)
                .HasName("x_Room_RoomTypeId");

            builder.HasIndex(e => e.TenantId)
                .HasName("x_Room_TenantId");

            builder.HasIndex(e => new { e.PropertyId, e.RoomNumber })
                .HasName("UK_Room_PropertyId_RoomNumber")
                .IsUnique();

            builder.HasKey(e => e.Id);
            builder.Property(e => e.Id).HasAnnotation("Relational:ColumnName", "RoomId").ValueGeneratedOnAdd();

            builder.Property(e => e.Building)
                .HasMaxLength(20)
                .IsUnicode(false)
                .HasAnnotation("Relational:ColumnName", "Building");

            builder.Property(e => e.CreationTime)
                .HasColumnType("datetime")
                .HasDefaultValueSql("(getutcdate())")
                .HasAnnotation("Relational:ColumnName", "CreationTime");

            builder.Property(e => e.CreatorUserId).HasAnnotation("Relational:ColumnName", "CreatorUserId");

            builder.Property(e => e.DeleterUserId).HasAnnotation("Relational:ColumnName", "DeleterUserId");

            builder.Property(e => e.DeletionTime)
                .HasColumnType("datetime")
                .HasAnnotation("Relational:ColumnName", "DeletionTime");

            builder.Property(e => e.Floor)
                .HasMaxLength(20)
                .IsUnicode(false)
                .HasAnnotation("Relational:ColumnName", "Floor");

            builder.Property(e => e.IsActive).HasAnnotation("Relational:ColumnName", "IsActive");

            builder.Property(e => e.IsDeleted).HasAnnotation("Relational:ColumnName", "IsDeleted");

            builder.Property(e => e.LastModificationTime)
                .HasColumnType("datetime")
                .HasDefaultValueSql("(getutcdate())")
                .HasAnnotation("Relational:ColumnName", "LastModificationTime");

            builder.Property(e => e.LastModifierUserId).HasAnnotation("Relational:ColumnName", "LastModifierUserId");

            //builder.Property(e => e.ParentRoomId).HasAnnotation("Relational:ColumnName", "ParentRoomId");

            //builder.Property(e => e.PropertyId).HasAnnotation("Relational:ColumnName", "PropertyId");

            builder.Property(e => e.Remarks)
                .HasMaxLength(4000)
                .IsUnicode(false)
                .HasAnnotation("Relational:ColumnName", "Remarks");

            builder.Property(e => e.RoomNumber)
                .IsRequired()
                .HasMaxLength(10)
                .IsUnicode(false)
                .HasAnnotation("Relational:ColumnName", "RoomNumber");

            //builder.Property(e => e.RoomTypeId).HasAnnotation("Relational:ColumnName", "RoomTypeId");

            builder.Property(e => e.TenantId).HasAnnotation("Relational:ColumnName", "TenantId");

            builder.Property(e => e.Wing)
                .HasMaxLength(20)
                .IsUnicode(false)
                .HasAnnotation("Relational:ColumnName", "Wing");

            builder.HasOne(d => d.ParentRoom)
                .WithMany(p => p.ChildRoomList)
                .HasForeignKey(d => d.ParentRoomId)
                .HasConstraintName("FK_Room_Room");

            builder.HasOne(d => d.Property)
                .WithMany(p => p.RoomList)
                .HasForeignKey(d => d.PropertyId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_Room_Property");

            builder.HasOne(d => d.RoomType)
                .WithMany(p => p.RoomList)
                .HasForeignKey(d => d.RoomTypeId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_Room_RoomType");

            builder.HasIndex(e => e.HousekeepingStatusPropertyId)
                .HasName("x_Room_HousekeepingStatusPropertyId");

            builder.Property(e => e.HousekeepingStatusLastModificationTime)
                 .HasColumnType("datetime")
                 .HasDefaultValueSql("(getutcdate())")
                 .HasAnnotation("Relational:ColumnName", "HousekeepingStatusLastModificationTime");

            builder.Ignore(e => e.ChildRoomTotal);
        }
    }
}
