﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;
using Thex.Central.Domain.Entities;

namespace Thex.Central.Infra.Context.Builders
{
    public class RoomBlockingMapper : IEntityTypeConfiguration<RoomBlocking>
    {
        public void Configure(EntityTypeBuilder<RoomBlocking> builder)
        {
            
            builder.ToTable("RoomBlocking");

            builder.HasAnnotation("Relational:TableName", "RoomBlocking");

            builder.HasIndex(e => e.PropertyId)
                .HasName("x_RoomBlocking_PropertyId");

            builder.HasIndex(e => e.RoomId)
                .HasName("x_RoomBlocking_RoomId");

            builder.HasIndex(e => e.TenantId)
                .HasName("x_RoomBlocking_TenantId");

            builder.HasIndex(e => new { e.PropertyId, e.BlockingEndDate })
                .HasName("x_RoomBlocking_PropertyId_BlockingEndDate");

            builder.HasIndex(e => new { e.PropertyId, e.BlockingStartDate })
                .HasName("x_RoomBlocking_PropertyId_BlockingStartDate");

            builder.Property(e => e.Id)
                .HasAnnotation("Relational:ColumnName", "RoomBlockingId");

            builder.Property(e => e.BlockingEndDate)
                .HasColumnType("datetime")
                .HasAnnotation("Relational:ColumnName", "BlockingEndDate");

            builder.Property(e => e.BlockingStartDate)
                .HasColumnType("datetime")
                .HasAnnotation("Relational:ColumnName", "BlockingStartDate");

            builder.Property(e => e.Comments)
                .HasMaxLength(4000)
                .IsUnicode(false)
                .HasAnnotation("Relational:ColumnName", "Comments");

            builder.Property(e => e.CreationTime)
                .HasColumnType("datetime")
                .HasDefaultValueSql("(getutcdate())")
                .HasAnnotation("Relational:ColumnName", "CreationTime");

            builder.Property(e => e.CreatorUserId).HasAnnotation("Relational:ColumnName", "CreatorUserId");

            builder.Property(e => e.DeleterUserId).HasAnnotation("Relational:ColumnName", "DeleterUserId");

            builder.Property(e => e.DeletionTime)
                .HasColumnType("datetime")
                .HasAnnotation("Relational:ColumnName", "DeletionTime");

            builder.Property(e => e.IsDeleted).HasAnnotation("Relational:ColumnName", "IsDeleted");

            builder.Property(e => e.LastModificationTime)
                .HasColumnType("datetime")
                .HasDefaultValueSql("(getutcdate())")
                .HasAnnotation("Relational:ColumnName", "LastModificationTime");

            builder.Property(e => e.LastModifierUserId).HasAnnotation("Relational:ColumnName", "LastModifierUserId");

            builder.Property(e => e.PropertyId).HasAnnotation("Relational:ColumnName", "PropertyId");

            builder.Property(e => e.ReasonId).HasAnnotation("Relational:ColumnName", "ReasonId");

            builder.Property(e => e.RoomId).HasAnnotation("Relational:ColumnName", "RoomId");

            builder.Property(e => e.TenantId).HasAnnotation("Relational:ColumnName", "TenantId");
        }
    }
}
