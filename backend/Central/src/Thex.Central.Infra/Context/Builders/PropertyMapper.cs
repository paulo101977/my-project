﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;
using Thex.Central.Domain.Entities;

namespace Thex.Central.Infra.Context.Builders
{
    public class PropertyMapper : IEntityTypeConfiguration<Property>
    {
        public void Configure(EntityTypeBuilder<Property> builder)
        {
            
            builder.ToTable("Property");

            builder.HasAnnotation("Relational:TableName", "Property");

            builder.HasIndex(e => e.BrandId)
                .HasName("x_Property_BrandId");

            builder.HasIndex(e => e.CompanyId)
                .HasName("x_Property_CompanyId");

            //builder.HasIndex(e => e.PropertyTypeId)
            //    .HasName("x_Property_PropertyTypeId");

            builder.HasIndex(e => e.PropertyUId)
                .HasName("UK_Property_PropertyUId")
                .IsUnique();

            builder.HasKey(e => e.Id);
            builder.Property(e => e.Id).HasAnnotation("Relational:ColumnName", "PropertyId");

            //builder.Property(e => e.BrandId).HasAnnotation("Relational:ColumnName", "BrandId");

            //builder.Property(e => e.CompanyId).HasAnnotation("Relational:ColumnName", "CompanyId");

            builder.Property(e => e.PropertyStatusId)
                .IsRequired()
                .HasDefaultValueSql("((12))")
                .HasAnnotation("Relational:ColumnName", "PropertyStatusId");

            builder.Property(e => e.CreationTime)
                .HasColumnType("datetime")
                .HasDefaultValueSql("(getutcdate())")
                .HasAnnotation("Relational:ColumnName", "CreationTime");

            builder.Property(e => e.CreatorUserId).HasAnnotation("Relational:ColumnName", "CreatorUserId");

            builder.Property(e => e.DeleterUserId).HasAnnotation("Relational:ColumnName", "DeleterUserId");

            builder.Property(e => e.DeletionTime)
                .HasColumnType("datetime")
                .HasAnnotation("Relational:ColumnName", "DeletionTime");

            builder.Property(e => e.IsDeleted).HasAnnotation("Relational:ColumnName", "IsDeleted");

            builder.Property(e => e.LastModificationTime)
                .HasColumnType("datetime")
                .HasDefaultValueSql("(getutcdate())")
                .HasAnnotation("Relational:ColumnName", "LastModificationTime");

            builder.Property(e => e.LastModifierUserId).HasAnnotation("Relational:ColumnName", "LastModifierUserId");

            builder.Property(e => e.Name)
                .IsRequired()
                .HasMaxLength(200)
                .IsUnicode(false)
                .HasAnnotation("Relational:ColumnName", "Name");

            //builder.Property(e => e.PropertyTypeId).HasAnnotation("Relational:ColumnName", "PropertyTypeId");

            //builder.Ignore(e => e.PropertyTypeIdValue);

            builder.Property(e => e.PropertyUId)
                .HasDefaultValueSql("(newid())")
                .HasAnnotation("Relational:ColumnName", "PropertyUId");

            builder.Property(e => e.TenantId).HasAnnotation("Relational:ColumnName", "TenantId");
        }
    }
}
