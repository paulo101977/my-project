﻿namespace Thex.Central.Infra.ReadInterfaces
{
    using System;
    using System.Collections.Generic;
    using Thex.Central.Domain.Entities;
    using Tnf.Repositories;

    public interface IReservationItemReadRepository : IRepository
    {
        List<ReservationItem> GetAvailabilityReservationsItemsByPeriodAndPropertyId(DateTime initialDate, DateTime finalDate, int propertyId, bool? completeReservationItemInformations = true, int? roomTypeId = null);
        List<ReservationItem> GetAvailabilityReservationsItemsByPeriod(DateTime initialDate, DateTime finalDate, bool? completeReservationItemInformations = true, int? roomTypeId = null);        
    }
}