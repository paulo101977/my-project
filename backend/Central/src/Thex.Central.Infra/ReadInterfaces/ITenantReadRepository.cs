﻿using System;
using System.Collections.Generic;

namespace Thex.Central.Infra.ReadInterfaces
{
    public interface ITenantReadRepository
    {
        List<Guid> GetTenantIds();
    }
}
