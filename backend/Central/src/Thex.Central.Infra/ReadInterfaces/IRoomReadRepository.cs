﻿namespace Thex.Domain.Interfaces.Repositories
{
    using System.Collections.Generic;
    using Thex.Central.Dto.Room;
    using Tnf.Repositories;

    public interface IRoomReadRepository : IRepository
    {
        List<ParentRoomsWithTotalChildrens> GetParentRoomsWithTotalChildrens(int propertyId);
        List<ParentRoomsWithTotalChildrens> GetParentRoomsWithTotalChildrens();
    }
}