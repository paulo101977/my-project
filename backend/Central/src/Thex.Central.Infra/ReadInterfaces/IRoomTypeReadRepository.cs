﻿using System.Collections.Generic;
using Thex.Central.Domain.Entities;
using Thex.Central.Dto.Availability;
using Thex.Central.Dto.RoomType;
using Tnf.Repositories;

namespace Thex.Central.Infra.ReadInterfaces
{
    public interface IRoomTypeReadRepository : IRepository
    {
        List<RoomType> GetRoomTypesByPropertyId(GetAllRoomTypesDto request, int propertyId);
        List<AvailabilityRoomTypeRowDto> GetAvailabilityRoomTypeRow(int propertyId, int? roomTypeId = null);
    }
}