﻿using System.Collections.Generic;
using Thex.Central.Dto.Availability;
using Thex.Central.Dto;
using Tnf.Repositories;

namespace Thex.Central.Infra.ReadInterfaces
{
    public interface IPropertyReadRepository : IRepository
    {
        List<AvailabilityPropertyRowDto> GetAvailabilityPropertyRowDto(List<int> propertiesIds);
    }
}