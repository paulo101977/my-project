﻿using System;
using Thex.Central.Dto;
using Tnf.Repositories;

namespace Thex.Central.Infra.ReadInterfaces
{
    public  interface IPropertyParameterReadRepository : IRepository
    {
        PropertyParameterDto GetPropertyParameterById(Guid propertyParameterId);
    }
}
