﻿using System;
using System.Collections.Generic;
using Thex.Central.Dto;
using Tnf.Repositories;

namespace Thex.Central.Infra.ReadInterfaces
{
    public interface IRoomBlockingReadRepository : IRepository
    {
        List<RoomBlockingDto> GetAllRoomsBlockedByPropertyId(int propertyId, DateTime initialDate, DateTime finalDate);
        List<RoomBlockingDto> GetAllRoomsBlocked(DateTime initialDate, DateTime finalDate);
    }
}
