﻿namespace Thex.Central.Web
{
    public class RouteConsts
    {
        public const string Availability = "api/Availability";
        public const string Property = "api/Properties";
        public const string RateProposal = "api/RateProposal";
        public const string ReservationItem = "api/Reservationitems";
        public const string PropertyParameter = "api/PropertyParameter";
        public const string BusinessSource = "api/BusinessSource";
        public const string MarketSegment = "api/MarketSegment";
        public const string Reason = "api/reason";
        public const string PlasticBrandProperty = "api/PlasticBrandProperty";
        public const string CompanyClient = "api/CompanyClient";
        public const string RoomType = "api/Roomtypes";
        public const string ReservationBudget = "api/ReservationBudget";
        public const string PropertyMealPlanType = "api/PropertyMealPlanType";
        public const string RoomLayout = "api/RoomLayouts";
        public const string Guest = "api/Guest";
        public const string Reservation = "api/Reservations";
        public const string PropertyGuestType = "api/PropertyGuestType";
        public const string Country = "api/Country";
        public const string DocumentType = "api/Documenttypes";
        public const string GuestRegistration = "api/guestRegistration";
        public const string GuestReservationItem = "api/GuestReservationItem";
        public const string GeneralOccupation = "api/GeneralOccupation";
        public const string Nationality = "api/nationality";
        public const string TransportationType = "api/TransportationType";
        public const string RatePlan = "api/ratePlan";
    }
}
