﻿using Microsoft.AspNetCore.Mvc.Controllers;
using System.Linq;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Authentication;
using Thex.Kernel;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Mvc;
using Thex.AspNetCore.Security.Interfaces;
using System.Collections.Generic;
using Thex.Kernel.Dto;
using Newtonsoft.Json;

namespace Thex.Central.Web.Filters
{
    public class ValidatePropertyIdFilter : ActionFilterAttribute
    {
        private const string _propertyTokenList = "PropertyListDto";
        private const string _requestedPropertyId = "RequestedPropertyId";
        private string _requestedPropertyIdValue = null;
        private bool _authorized;

        public override void OnActionExecuting(ActionExecutingContext context)
        {
            _authorized = false;
            var _tokenManager = ResolveTokenManager(context);
            var serializedPropertyTokenList = GetLoggedPropertyTokenList(context, _tokenManager);
            List<LoggedUserPropertyDto> propertyTokenList = null;

            if (serializedPropertyTokenList != null)     
                propertyTokenList = JsonConvert.DeserializeObject<List<LoggedUserPropertyDto>>(serializedPropertyTokenList.ToString());                       

            if (propertyTokenList == null)
            {
                context.Result = new UnauthorizedResult();
                return;
            }

            CheckParameters(context, propertyTokenList);

            if (!_authorized)
                context.Result = new UnauthorizedResult();
            else
                SetApplicationUser(context, propertyTokenList);
        }

        private void CheckParameters(ActionExecutingContext context, List<LoggedUserPropertyDto> propertyTokenList)
        {
            foreach (ControllerParameterDescriptor param in context.ActionDescriptor.Parameters)
            {
                context.ActionArguments.TryGetValue(param.Name, out var modelValue);

                if (modelValue == null)
                    continue;

                if (param.Name.ToLower() == _requestedPropertyId.ToLower() && IsValid(propertyTokenList, modelValue.ToString()))
                    SetAuthorizedAndPropertyValue(modelValue);
                else
                {
                    var type = modelValue.GetType();

                    var propertyIdList = type.GetProperties().Where(x => x.Name.ToLower().Equals(_requestedPropertyId));

                    foreach (var prop in propertyIdList)
                    {
                        var value = prop.GetValue(modelValue);

                        if (value == null)
                            continue;

                        if (IsValid(propertyTokenList, value.ToString()))
                            SetAuthorizedAndPropertyValue(modelValue);
                    }
                }
            }
        }

        private void SetAuthorizedAndPropertyValue(object modelValue)
        {
            _authorized = true;
            _requestedPropertyIdValue = modelValue.ToString();
        }

        private void SetApplicationUser(ActionExecutingContext context, List<LoggedUserPropertyDto> propertyTokenList)
        {
            var propertyToken = propertyTokenList.FirstOrDefault(p => p.PropertyId == _requestedPropertyIdValue);

            (ResolveApplicationUser(context) as ApplicationUserCentral)
                .SetProperties(propertyToken.TenantId, propertyToken.PropertyId, propertyToken.BrandId, propertyToken.ChainId, 
                               propertyToken.PropertyCulture, propertyToken.PropertyLanguage, propertyToken.TimeZoneName, 
                               propertyToken.PropertyCountryCode, propertyToken.PropertyDistrictCode, propertyTokenList);
        }

        private bool IsValid(List<LoggedUserPropertyDto> tokenPropertyList, string requestedPropertyId)
            => tokenPropertyList.Any(e => e.PropertyId == requestedPropertyId);

        private object GetLoggedPropertyTokenList(ActionExecutingContext context, ITokenManager _tokenManager)
        {
            object propertyList = null;

            var accessToken = context.HttpContext.GetBearerToken();

            var validToken = _tokenManager.VerifyToken(accessToken);

            if (validToken != null)
                validToken.Payload.TryGetValue(_propertyTokenList, out propertyList);

            return propertyList;
        }

        private ITokenManager ResolveTokenManager(ActionExecutingContext context)
            => context.HttpContext.RequestServices.GetService<ITokenManager>();

        private IApplicationUser ResolveApplicationUser(ActionExecutingContext context)
            => context.HttpContext.RequestServices.GetService<IApplicationUser>();
       
    }
}
