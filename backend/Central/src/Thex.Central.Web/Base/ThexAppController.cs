﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Thex.AspNetCore.Filters;
using Thex.Kernel;

namespace Thex.Central.Web.Base
{
    [Authorize]
    //[ServiceFilter(typeof(ActionLogFilter))]
    public class ThexAppController : TnfController
    {
        protected IApplicationUser _applicationUser;

        public ThexAppController(IApplicationUser applicationUser)
        {
            _applicationUser = applicationUser;
        }

        public IApplicationUser ApplicationUser
        {
            get { return _applicationUser; }
            set { _applicationUser = value; }
        }
    }
}

