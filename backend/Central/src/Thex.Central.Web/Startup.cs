﻿using System;
using System.IO.Compression;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.ResponseCompression;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Thex.Common;
using Thex.Kernel;
using Tnf.Configuration;
using Thex.Central.Infra;
using Thex.AspNetCore.Filters;
using Thex.Central.Shared.Application;
using Thex.Maps.Geocode;
using PMSDto = Thex.Dto;
using Thex.Domain;
using Thex.Domain.Entities;
using Microsoft.AspNetCore.Http;
using System.Security.Principal;
using Microsoft.Extensions.DependencyInjection.Extensions;

namespace Thex.Central.Web
{
    public class Startup
    {
        DatabaseConfiguration DatabaseConfiguration { get; }
        private IConfiguration Configuration { get; set; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
            DatabaseConfiguration = new DatabaseConfiguration(configuration);
        }

        // Use this method to add services to the container.
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            services.Configure<GzipCompressionProviderOptions>(
                options => options.Level = CompressionLevel.Optimal);

            services
                .AddThexGenericLog()
                .AddCorsAll("AllowAll")
                .AddCentralSharedApplicationServiceDependency(options =>
                {
                    var settingsSection = Configuration.GetSection("MapsGeocodeConfig");
                    var settings = settingsSection.Get<MapsGeocodeConfig>();
                })
                .AddTnfAspNetCore()
                .AddResponseCompression(options =>
                {
                    options.Providers.Add<GzipCompressionProvider>();
                    options.EnableForHttps = true;
                });

            services
                .AddThexAspNetCoreSecurity(options =>
                {
                    var settingsSection = Configuration.GetSection("TokenConfiguration");
                    var settings = settingsSection.Get<AspNetCore.Security.TokenConfiguration>();

                    options.TokenConfiguration = settings;
                    options.FrontEndpoint = Configuration.GetValue<string>("UrlFront");
                    options.SuperAdminEndpoint = Configuration.GetValue<string>("SuperAdminEndpoint");
                });

            services.AddSingleton(Configuration.GetSection("ServicesConfiguration").Get<ServicesConfiguration>());
            services.AddScoped<ActionLogFilter>();

            services.AddSwaggerDocumentation();

            services.AddAntiforgery(options =>
            {
                options.Cookie.Name = "X-CSRF-TOKEN-GOTNEXT-COOKIE";
                options.HeaderName = "X-CSRF-TOKEN-GOTNEXT-HEADER";
                options.SuppressXFrameOptionsHeader = false;
            });

            services.AddMvc()
                .AddJsonOptions(opts =>
                {
                    opts.SerializerSettings.NullValueHandling =
                        Newtonsoft.Json.NullValueHandling.Ignore;
                });

            services.AddScoped<IPrincipal>((sp) => sp.GetService<IHttpContextAccessor>().HttpContext.User);

            var descriptor = new ServiceDescriptor(
                typeof(IApplicationUser),
                typeof(ApplicationUserCentral),
                ServiceLifetime.Scoped);

            services.Replace(descriptor);

            var serviceProvider = services.BuildServiceProvider();

            ServiceLocator.SetLocatorProvider(serviceProvider);

            return serviceProvider;
        }

        // Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseCors("AllowAll");

            // Configura o use do AspNetCore do Tnf
            app.UseTnfAspNetCore(options =>
            {
                //Adiciona as configurações de localização da aplicação
                options.UseDomainLocalization();

                options.Repository(repositoryConfig =>
                {
                    repositoryConfig.Entity<IEntityGuid>(entity =>
                        entity.RequestDto<PMSDto.IDefaultGuidRequestDto>((e, d) => e.Id == d.Id));

                    repositoryConfig.Entity<IEntityInt>(entity =>
                        entity.RequestDto<PMSDto.IDefaultIntRequestDto>((e, d) => e.Id == d.Id));

                    repositoryConfig.Entity<IEntityLong>(entity =>
                        entity.RequestDto<PMSDto.IDefaultLongRequestDto>((e, d) => e.Id == d.Id));
                });

                // Configura a connection string da aplicação
                options.DefaultNameOrConnectionString = DatabaseConfiguration.ConnectionString;
                options.DefaultPageSize(1000, 1000);

            });

            // Habilita o uso do UnitOfWork em todo o request
            app.UseTnfUnitOfWork();

            app.UseSwaggerDocumentation();

            app.UseThexAspNetCoreSecurity();

            app.UseMvcWithDefaultRoute();

            app.UseResponseCompression();

            app.Run(context =>
            {
                context.Response.Redirect("/swagger");
                return Task.CompletedTask;
            });
        }
    }
}
