﻿using Microsoft.AspNetCore.Mvc;
using Thex.Application.Interfaces;
using Thex.Central.Domain.Entities;
using Thex.Central.Web.Base;
using Thex.Central.Web.Filters;
using Thex.Dto;
using Thex.Dto.PlasticBrandProperty;
using Thex.Kernel;
using Tnf.AspNetCore.Mvc.Response;
using Tnf.Dto;

namespace Thex.Central.Web.Controllers
{
    [Route(RouteConsts.PlasticBrandProperty)]
    public class PlasticBrandPropertyController : ThexAppController
    {
        protected readonly IPlasticBrandPropertyAppService PlasticBrandPropertyAppService;

        public PlasticBrandPropertyController(
            IApplicationUser applicationUser,
            IPlasticBrandPropertyAppService plasticBrandPropertyAppService) : base(applicationUser)
        {
            PlasticBrandPropertyAppService = plasticBrandPropertyAppService;
        }


        [HttpGet]
        [ThexAuthorize("CENTRAL_PlasticBrandProperty_Get")]
        [ValidatePropertyIdFilter]
        [ProducesResponseType(typeof(IListDto<PlasticBrandPropertyResultDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public virtual IActionResult Get([FromQuery] GetAllPlasticBrandPropertyDto requestDto, [FromQuery] string requestedPropertyId)
        {
            var response = PlasticBrandPropertyAppService.GetAllByPropertyId(requestDto);

            return CreateResponseOnGetAll(response, EntityNames.PlasticBrandProperty);
        }
    }
}
