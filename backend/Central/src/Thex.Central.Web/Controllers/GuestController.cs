﻿using Microsoft.AspNetCore.Mvc;
using Thex.Application.Interfaces;
using Thex.Central.Domain.Entities;
using Thex.Central.Web.Base;
using Thex.Central.Web.Filters;
using Thex.Dto.Person;
using Thex.Kernel;
using Tnf.AspNetCore.Mvc.Response;

namespace Thex.Central.Web.Controllers
{
    [Route(RouteConsts.Guest)]
    public class GuestController : ThexAppController
    {
        private readonly IGuestAppService GuestAppService;

        public GuestController(
            IApplicationUser applicationUser,
            IGuestAppService guestAppService)
            : base(applicationUser)
        {
            GuestAppService = guestAppService;
        }


        [HttpGet("search")]
        [ThexAuthorize("CENTRAL_Guest_Get_Search")]
        [ValidatePropertyIdFilter]
        [ProducesResponseType(typeof(SearchPersonsResultDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult Search([FromQuery]SearchPersonsDto request, [FromQuery] string requestedPropertyId)
        {
            var response = GuestAppService.SearchBasedOnFilter(request);

            return CreateResponseOnGet(response, EntityNames.Guest);
        }
    }
}
