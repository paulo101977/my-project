﻿using Microsoft.AspNetCore.Mvc;
using Thex.Application.Interfaces;
using Thex.Central.Domain.Entities;
using Thex.Central.Web.Base;
using Thex.Central.Web.Filters;
using Thex.Dto.PropertyParameter;
using Thex.Kernel;
using Tnf.AspNetCore.Mvc.Response;

namespace Thex.Central.Web.Controllers
{
    [Route(RouteConsts.PropertyParameter)]
    public class PropertyParameterController : ThexAppController
    {
        protected readonly IPropertyParameterAppService PropertyParameterAppService;

        public PropertyParameterController(
            IApplicationUser applicationUser,
            IPropertyParameterAppService propertyParameterAppService) : base(applicationUser)
        {
            PropertyParameterAppService = propertyParameterAppService;
        }

        [HttpGet("{propertyId}/propertyparameters")]
        [ThexAuthorize("CENTRAL_PropertyParameter_Get_PropertyParameters")]
        [ValidatePropertyIdFilter]
        [ProducesResponseType(typeof(PropertyParameterDividedByGroupDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult GetPropertyParameterByPropertyId(int propertyId, [FromQuery] string requestedPropertyId)
        {
            var response = PropertyParameterAppService.GetPropertyParameterByPropertyId(propertyId);

            return CreateResponseOnGet(response, EntityNames.PropertyParameter);
        }
    }
}
