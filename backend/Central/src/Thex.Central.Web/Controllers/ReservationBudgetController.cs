﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.Application.Interfaces;
using Thex.Central.Domain.Entities;
using Thex.Central.Web.Base;
using Thex.Central.Web.Filters;
using Thex.Dto;
using Thex.Kernel;
using Tnf.AspNetCore.Mvc.Response;

namespace Thex.Central.Web.Controllers
{
    [Route(RouteConsts.ReservationBudget)]
    public class ReservationBudgetController : ThexAppController
    {
        private readonly IReservationBudgetAppService ReservationBudgetAppService;
        public ReservationBudgetController(IApplicationUser applicationUser, IReservationBudgetAppService reservationBudgetAppService) : base(applicationUser)
        {
            ReservationBudgetAppService = reservationBudgetAppService;
        }

        [HttpGet("{propertyId}")]
        [ThexAuthorize("CENTRAL_ReservationBudget_Get_Param")]
        [ValidatePropertyIdFilter]
        [ProducesResponseType(typeof(ReservationItemBudgetHeaderDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> GetWithBudgetOffer([FromQuery] ReservationItemBudgetRequestDto requestDto, int propertyId, [FromQuery] string requestedPropertyId)
        {
            if (requestDto.RoomTypeId.HasValue)
                requestDto.RoomTypeIdList = new List<int>() { requestDto.RoomTypeId.Value };

            var response = await ReservationBudgetAppService.GetWithBudgetOffer(requestDto, propertyId);

            return CreateResponseOnGet(response, EntityNames.ReservationBudget);
        }

        [HttpPut("{propertyId}/{reservationId}/accomodations")]
        [ThexAuthorize("CENTRAL_ReservationBudget_Update_Accomodations")]
        [ValidatePropertyIdFilter]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> UpdateByReservationId(int propertyId, [FromQuery] string requestedPropertyId, long reservationId)
        {
            await ReservationBudgetAppService.UpdateByReservationId(propertyId, reservationId);

            return CreateResponseOnPut(null, EntityNames.ReservationBudget);
        }
    }
}
