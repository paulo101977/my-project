﻿using Microsoft.AspNetCore.Mvc;
using Thex.Application.Interfaces;
using Thex.Central.Domain.Entities;
using Thex.Central.Web.Base;
using Thex.Central.Web.Filters;
using Thex.Dto;
using Thex.Kernel;
using Tnf.AspNetCore.Mvc.Response;
using Tnf.Dto;

namespace Thex.Central.Web.Controllers
{
    [Route(RouteConsts.MarketSegment)]
    public class MarketSegmentController : ThexAppController
    {
        private readonly IMarketSegmentAppService MarketSegmentAppService;
        public MarketSegmentController(
            IApplicationUser applicationUser,
            IMarketSegmentAppService marketSegmentAppService)
            : base(applicationUser)
        {
            MarketSegmentAppService = marketSegmentAppService;
        }

        [HttpGet]
        [ThexAuthorize("CENTRAL_MarketSegment_Get")]
        [ValidatePropertyIdFilter]
        [ProducesResponseType(typeof(IListDto<MarketSegmentDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult Get([FromQuery] GetAllMarketSegmentDto requestDto, [FromQuery] string requestedPropertyId)
        {
            int propertyId = requestDto.PropertyId;
            var response = MarketSegmentAppService.GetAllByPropertyId(requestDto, propertyId);

            return CreateResponseOnGetAll(response, EntityNames.MarketSegment);
        }
    }
}
