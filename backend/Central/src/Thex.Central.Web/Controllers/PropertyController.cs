﻿using Microsoft.AspNetCore.Mvc;
using Thex.Central.Web.Base;
using Thex.Kernel;
using System.Collections.Generic;
using Tnf.AspNetCore.Mvc.Response;
using Thex.Application.Interfaces;
using Thex.Central.Domain.Entities;
using Thex.Dto;
using Thex.Central.Web.Filters;
using Tnf.Dto;
using Thex.Dto.RoomTypes;

namespace Thex.Central.Web.Controllers
{
    [Route(RouteConsts.Property)]
    public class PropertyController : ThexAppController
    {
        private readonly IPropertyAppService _propertyAppService;
        private readonly IRoomTypeAppService RoomTypeAppService;

        public PropertyController(
            IApplicationUser applicationUser,
            IPropertyAppService propertyAppService,
            IRoomTypeAppService roomTypeAppService)
            : base(applicationUser)
        {
            _propertyAppService = propertyAppService;
            RoomTypeAppService = roomTypeAppService;
        }

        [HttpGet]
        [ThexAuthorize("CENTRAL_Property_GetAll")]
        [ProducesResponseType(typeof(IListDto<PropertyDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public virtual IActionResult GetProperties([FromQuery] GetAllPropertyDto requestDto)
        {
            var response = _propertyAppService.GetAllProperty(requestDto, true);

            return CreateResponseOnGetAll(response, EntityNames.Property);
        }

        [HttpGet("{id}/roomtypes")]
        [ThexAuthorize("CENTRAL_Property_Get_RoomTypes")]
        [ValidatePropertyIdFilter]
        [ProducesResponseType(typeof(IListDto<RoomTypeDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult Get([FromQuery] GetAllRoomTypesDto request, int id, [FromQuery] string requestedPropertyId)
        {
            var response = RoomTypeAppService.GetRoomTypesByPropertyId(request, id);

            return CreateResponseOnGetAll(response, EntityNames.Property);
        }
    }
}
