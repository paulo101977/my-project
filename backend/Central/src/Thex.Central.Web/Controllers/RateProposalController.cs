﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Thex.Application.Interfaces.RateProposal;
using Thex.Central.Domain.Entities;
using Thex.Central.Web.Base;
using Thex.Central.Web.Filters;
using Thex.Dto;
using Thex.Kernel;
using Tnf.AspNetCore.Mvc.Response;

namespace Thex.Central.Web.Controllers
{
    [Route(RouteConsts.RateProposal)]
    public class RateProposalController : ThexAppController
    {
        private readonly IRateProposalAppService RateProposalAppService;
        public RateProposalController(IRateProposalAppService rateProposalAppService,
            IApplicationUser applicationUser) : base(applicationUser)
        {
            RateProposalAppService = rateProposalAppService;
        }

        [HttpGet("{propertyId}")]
        [ThexAuthorize("CENTRAL_RateProposal_Get")]
        [ValidatePropertyIdFilter]
        [ProducesResponseType(typeof(ReservationItemBudgetHeaderDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> GetWithBudgetOffer([FromQuery] ReservationItemBudgetRequestDto requestDto, int propertyId, [FromQuery] string requestedPropertyId)
        {
            var response = await RateProposalAppService.GetAllhBudgetOfferWithRateProposal(requestDto, propertyId);

            return CreateResponseOnGet(response, EntityNames.RateProposal);
        }
    }
}
