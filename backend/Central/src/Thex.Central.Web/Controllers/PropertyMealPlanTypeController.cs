﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Thex.Application.Interfaces;
using Thex.Central.Domain.Entities;
using Thex.Central.Web.Base;
using Thex.Central.Web.Filters;
using Thex.Dto;
using Thex.Kernel;
using Tnf.AspNetCore.Mvc.Response;
using Tnf.Dto;

namespace Thex.Central.Web.Controllers
{
    [Route(RouteConsts.PropertyMealPlanType)]
    public class PropertyMealPlanTypeController : ThexAppController
    {
        private readonly IPropertyMealPlanTypeAppService PropertyMealPlanTypeAppService;

        public PropertyMealPlanTypeController(
            IApplicationUser applicationUser,
            IPropertyMealPlanTypeAppService propertyMealPlanTypeAppService)
            : base(applicationUser)
        {
            PropertyMealPlanTypeAppService = propertyMealPlanTypeAppService;
        }

        [HttpGet("{propertyId}/getallpropertymealplantype")]
        [ThexAuthorize("CENTRAL_PropertyMealPlanType_Get_GetAllPropertyMealPlanType")]
        [ValidatePropertyIdFilter]
        [ProducesResponseType(typeof(IListDto<PropertyMealPlanTypeDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult GetAll(int propertyId, [FromQuery] string requestedPropertyId, bool all = false)
        {
            var response = PropertyMealPlanTypeAppService.GetAllPropertyMealPlanTypeByProperty(propertyId, all);

            return CreateResponseOnGetAll(response, EntityNames.PropertyMealPlanType);
        }
    }
}
