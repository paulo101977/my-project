﻿using Microsoft.AspNetCore.Mvc;
using Thex.Application.Interfaces;
using Thex.Central.Domain.Entities;
using Thex.Central.Web.Base;
using Thex.Central.Web.Filters;
using Thex.Dto;
using Thex.Kernel;
using Tnf.AspNetCore.Mvc.Response;
using Tnf.Dto;

namespace Thex.Central.Web.Controllers
{
    [Route(RouteConsts.CompanyClient)]
    public class CompanyClientController : ThexAppController
    {
        private readonly ICompanyClientAppService CompanyClientAppService;

        public CompanyClientController(
            IApplicationUser applicationUser,
            ICompanyClientAppService companyClientAppService)
            : base(applicationUser)
        {
            CompanyClientAppService = companyClientAppService;
        }


        [HttpGet("search")]
        [ThexAuthorize("CENTRAL_CompanyClient_Get_Search")]
        [ValidatePropertyIdFilter]
        [ProducesResponseType(typeof(IListDto<GetAllCompanyClientSearchResultDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult Search([FromQuery] GetAllCompanyClientSearchDto requestDto, [FromQuery] string requestedPropertyId)
        {
            int propertyId = requestDto.PropertyId;
            var response = CompanyClientAppService.GetAllBasedOnFilter(requestDto, propertyId);

            return CreateResponseOnGetAll(response, EntityNames.CompanyClient);
        }

        [HttpGet("contactpersons")]
        [ThexAuthorize("CENTRAL_CompanyClient_Get_ContactPersons")]
        [ValidatePropertyIdFilter]
        [ProducesResponseType(typeof(IListDto<GetAllCompanyClientContactInformationSearchResultDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult ContactPerson([FromQuery] GetAllCompanyClientContactInformationSearchDto requestDto, [FromQuery] string requestedPropertyId)
        {
            var response = CompanyClientAppService.GetAllContactPersonByCompanyClientId(requestDto);

            return CreateResponseOnGetAll(response, EntityNames.CompanyClient);
        }
    }
}
