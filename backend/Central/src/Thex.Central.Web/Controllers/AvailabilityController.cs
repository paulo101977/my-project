﻿using Microsoft.AspNetCore.Mvc;
using CentralDto = Thex.Central.Dto.Availability;
using Thex.Central.Web.Base;
using Thex.Kernel;
using Thex.Central.Web.Filters;
using Thex.Central.Application.Interfaces;
using Tnf.AspNetCore.Mvc.Response;
using Thex.Central.Domain.Entities;

namespace Thex.Central.Web.Controllers
{
    [Route(RouteConsts.Availability)]
    public class AvailabilityController : ThexAppController
    {
        private readonly IAvailabilityAppService _availabilityAppService;

        public AvailabilityController(
            IApplicationUser applicationUser,
            IAvailabilityAppService availabilityAppService)
            : base(applicationUser)
        {
            _availabilityAppService = availabilityAppService;
        }

        [HttpGet("properties")]
        [ThexAuthorize("CENTRAL_Availability_Get_Properties")]
        [ProducesResponseType(typeof(CentralDto.AvailabilityDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public virtual IActionResult GetAvailabilityProperties([FromQuery] CentralDto.SearchAvailabilityDto requestDto)
        {
            var response = _availabilityAppService.GetAvailabilityPropertyList(requestDto);

            return CreateResponseOnGet(response, EntityNames.Availability);
        }

        [HttpGet("{propertyId}/{requestedPropertyId}/roomTypes")]
        [ThexAuthorize("CENTRAL_Availability_Get_RoomTypes")]
        [ValidatePropertyIdFilter]
        [ProducesResponseType(typeof(CentralDto.AvailabilityRoomTypesDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public virtual IActionResult GetAvailabilityRoomTypes([FromQuery] CentralDto.SearchAvailabilityDto requestDto, int propertyId, string requestedPropertyId)
        {
            var response = _availabilityAppService.GetAvailabilityRoomTypes(requestDto, propertyId);
            return CreateResponseOnGet(response, EntityNames.Availability);
        }


    }
}
