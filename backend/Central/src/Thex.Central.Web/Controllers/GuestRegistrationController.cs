﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Thex.Application.Interfaces;
using Thex.Central.Domain.Entities;
using Thex.Central.Web.Base;
using Thex.Central.Web.Filters;
using Thex.Dto;
using Thex.Dto.GuestRegistration;
using Thex.Kernel;
using Tnf.AspNetCore.Mvc.Response;
using Tnf.Dto;

namespace Thex.Central.Web.Controllers
{
    [Route(RouteConsts.GuestRegistration)]
    public class GuestRegistrationController : ThexAppController
    {
        private readonly IGuestRegistrationAppService GuestRegistrationAppService;
        public GuestRegistrationController(
            IApplicationUser applicationUser,
            IGuestRegistrationAppService guestRegistrationAppService)
            : base(applicationUser)
        {
            GuestRegistrationAppService = guestRegistrationAppService;
        }

        [HttpGet("search")]
        [ThexAuthorize("CENTRAL_GuestRegistration_Get_Search")]
        [ValidatePropertyIdFilter]
        [ProducesResponseType(typeof(IListDto<GuestRegistrationResultDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult Search([FromQuery] string requestedPropertyId, [FromQuery]SearchGuestRegistrationsDto request)
        {
            var response = GuestRegistrationAppService.GetAllGuestRegistrationBasedOnFilter(request);

            return CreateResponseOnGetAll(response, EntityNames.GuestRegistration);
        }

        [HttpGet("{propertyId}/{reservationId}/responsibles")]
        [ThexAuthorize("CENTRAL_GuestRegistration_Get_Responsibles")]
        [ValidatePropertyIdFilter]
        [ProducesResponseType(typeof(IListDto<GuestRegistrationResultDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult GetAllResponsibles(long propertyId, long reservationId, [FromQuery] string requestedPropertyId)
        {
            var response = GuestRegistrationAppService.GetAllResponsibles(propertyId, reservationId);
            return CreateResponseOnGetAll(response, EntityNames.GuestRegistration);
        }

        [HttpGet("ByGuestReservationItem/{guestReservationItemId}/property/{propertyId}/language/{languageIsoCode}")]
        [ThexAuthorize("CENTRAL_GuestRegistration_Get_ByGuestReservationItem")]
        [ValidatePropertyIdFilter]
        [ProducesResponseType(typeof(GuestRegistrationResultDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult GetGuestRegistrationByGuestReservationItem(long propertyId, long guestReservationItemId, string languageIsoCode, [FromQuery] string requestedPropertyId)
        {
            var response = GuestRegistrationAppService.GetGuestRegistrationByGuestReservationItem(propertyId, guestReservationItemId, languageIsoCode);
            return CreateResponseOnGet(response, EntityNames.GuestRegistration);

        }
    }
}
