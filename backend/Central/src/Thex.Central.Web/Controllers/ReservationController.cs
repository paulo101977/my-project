﻿using Microsoft.AspNetCore.Mvc;
using Thex.Application.Interfaces;
using Thex.Central.Domain.Entities;
using Thex.Central.Web.Base;
using Thex.Central.Web.Filters;
using Thex.Dto;
using Thex.Dto.Reservation;
using Thex.Kernel;
using Tnf.AspNetCore.Mvc.Response;
using Tnf.Dto;

namespace Thex.Central.Web.Controllers
{
    [Route(RouteConsts.Reservation)]
    public class ReservationController : ThexAppController
    {
        protected readonly IReservationAppService ReservationAppService;

        public ReservationController(
            IApplicationUser applicationUser,
            IReservationAppService reservationAppService) : base(applicationUser)
        {
            ReservationAppService = reservationAppService;
        }

        [HttpPost]
        [ThexAuthorize("CENTRAL_Reservation_Post")]
        [ValidatePropertyIdFilter]
        [ProducesResponseType(typeof(ReservationDto), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult Post([FromBody] ReservationDto dto, [FromQuery] string requestedPropertyId)
        {
            var response = ReservationAppService.CreateReservation(dto);

            return CreateResponseOnPost(response, EntityNames.Reservation);
        }

        [HttpGet("newReservationCode")]
        [ThexAuthorize("CENTRAL_Reservation_Get_NewReservationCode")]
        [ValidatePropertyIdFilter]
        [ProducesResponseType(typeof(NewReservationCodeDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult GetReservationCode([FromQuery] string requestedPropertyId)
           => CreateResponseOnGet(ReservationAppService.GetNewReservationCode(), EntityNames.Reservation);

        [HttpGet("{reservationId}/reservationHeader")]
        [ThexAuthorize("CENTRAL_Reservation_Get_ReservationHeader")]
        [ValidatePropertyIdFilter]
        [ProducesResponseType(typeof(ReservationHeaderResultDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult GetReservationId(long reservationId, [FromQuery] string requestedPropertyId)
        {
            var response = ReservationAppService.GetReservationHeader(reservationId);

            return CreateResponseOnGet(response, EntityNames.ReservationItem);
        }

        [HttpGet("{id}")]
        [ThexAuthorize("CENTRAL_Reservation_Get")]
        [ValidatePropertyIdFilter]
        [ProducesResponseType(typeof(ReservationDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult Get(long id, [FromQuery] string requestedPropertyId, [FromQuery] RequestDto requestDto)
        {
            var request = new DefaultLongRequestDto(id, requestDto);

            var response = ReservationAppService.GetReservation(request);

            return CreateResponseOnGet(response, EntityNames.Reservation);
        }

        [HttpPut("{id}")]
        [ThexAuthorize("CENTRAL_Reservation_Put")]
        [ValidatePropertyIdFilter]
        [ProducesResponseType(typeof(ReservationDto), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult Put(long id, [FromQuery] string requestedPropertyId, [FromBody] ReservationDto dto)
        {
            var response = ReservationAppService.UpdateReservationAsync(id, dto);

            return CreateResponseOnPut(response, EntityNames.Reservation);
        }

        [HttpPatch("{reservationId}/cancel")]
        [ThexAuthorize("CENTRAL_Reservation_Patch_Cancel")]
        [ValidatePropertyIdFilter]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult CancelReservation(long reservationId, [FromQuery] string requestedPropertyId, [FromBody] ReservationCancelDto dto)
        {
            this.ReservationAppService.CancelReservation(reservationId, dto);

            return CreateResponseOnPatch(null, EntityNames.ReservationItem);
        }

        [HttpPatch("{reservationId}/reactivate")]
        [ThexAuthorize("CENTRAL_Reservation_Patch_Reactivate")]
        [ValidatePropertyIdFilter]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult ReactivateReservation(long reservationId, [FromQuery] string requestedPropertyId)
        {
            ReservationAppService.ReactivateReservation(reservationId);

            return CreateResponseOnPatch(null, EntityNames.ReservationItem);
        }

        [HttpGet("{reservationItemId}/byreservationitem")]
        [ThexAuthorize("CENTRAL_Reservation_Get_ByReservationItem")]
        [ValidatePropertyIdFilter]
        [ProducesResponseType(typeof(ReservationDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult GetByReservationItemId(long reservationItemId, [FromQuery] string requestedPropertyId, [FromQuery] RequestDto requestDto)
        {
            var request = new DefaultLongRequestDto(reservationItemId, requestDto);

            var response = ReservationAppService.GetByReservationItemId(request);

            return CreateResponseOnGet(response, EntityNames.ReservationItem);
        }

        [HttpGet("{reservationItemId}/header")]
        [ThexAuthorize("CENTRAL_Reservation_Get_Header")]
        [ValidatePropertyIdFilter]
        [ProducesResponseType(typeof(ReservationHeaderResultDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult GetReservationHeaderByReservationItemId(long reservationItemId, [FromQuery] string requestedPropertyId)
        {
            var response = ReservationAppService.GetReservationHeaderByReservationItemId(reservationItemId);

            return CreateResponseOnGet(response, EntityNames.ReservationItem);
        }

        [HttpGet("{propertyId}/search")]
        [ThexAuthorize("CENTRAL_Reservation_Get_Search")]
        [ValidatePropertyIdFilter]
        [ProducesResponseType(typeof(IListDto<SearchReservationResultDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult GetAllByFilters([FromQuery] SearchReservationDto request, int propertyId, [FromQuery] string requestedPropertyId)
        {
            var response = ReservationAppService.GetAllByFilters(request, propertyId);

            return CreateResponseOnGetAll(response, EntityNames.Reservation);
        }

    }
}
