﻿using Microsoft.AspNetCore.Mvc;
using Thex.Application.Interfaces;
using Thex.Central.Domain.Entities;
using Thex.Central.Web.Base;
using Thex.Central.Web.Filters;
using Thex.Common;
using Thex.Dto;
using Thex.Kernel;
using Tnf.AspNetCore.Mvc.Response;
using Tnf.Dto;

namespace Thex.Central.Web.Controllers
{
    [Route(RouteConsts.Country)]
    public class CountryController : ThexAppController
    {
        protected readonly ICountrySubdivisionAppService CountrySubdivisionAppService;

        public CountryController(
            IApplicationUser applicationUser,
            ICountrySubdivisionAppService countrySubdivisionAppService) : base(applicationUser)
        {
            LocalizationSourceName = AppConsts.LocalizationSourceName;
            CountrySubdivisionAppService = countrySubdivisionAppService;
        }

        [HttpGet]
        [ThexAuthorize("CENTRAL_Country_Get")]
        [ValidatePropertyIdFilter]
        [ProducesResponseType(typeof(IListDto<CountryDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult Get([FromQuery] string requestedPropertyId, [FromQuery] GetAllCountryDto requestDto)
        {
            var response = CountrySubdivisionAppService.GetAllCountriesByLanguageIsoCode(requestDto);

            return CreateResponseOnGetAll(response, EntityNames.CountrySubdivision);
        }
    }
}
