﻿using Microsoft.AspNetCore.Mvc;
using Thex.Application.Interfaces;
using Thex.Central.Domain.Entities;
using Thex.Central.Web.Base;
using Thex.Central.Web.Filters;
using Thex.Dto;
using Thex.Kernel;
using Tnf.AspNetCore.Mvc.Response;
using Tnf.Dto;

namespace Thex.Central.Web.Controllers
{
    [Route(RouteConsts.RatePlan)]
    public class RatePlanController : ThexAppController
    {
        private readonly IRatePlanAppService RatePlanAppService;
        public RatePlanController(
            IApplicationUser applicationUser,
            IRatePlanAppService ratePlanAppService) : base(applicationUser)
        {
            RatePlanAppService = ratePlanAppService;
        }

        [HttpGet("{propertyId}/rateplans")]
        [ThexAuthorize("CENTRAL_RatePlan_Get_RatePlans")]
        [ValidatePropertyIdFilter]
        [ProducesResponseType(typeof(IListDto<RatePlanDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult GetAllSearch([FromQuery] GetAllRatePlanDto request, int propertyId, [FromQuery] string requestedPropertyId)
        {
            var response = RatePlanAppService.GetAllRatePlanSearch(request, propertyId);
            return CreateResponseOnGetAll(response, EntityNames.RatePlan);
        }
    }
}
