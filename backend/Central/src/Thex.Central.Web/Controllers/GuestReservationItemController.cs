﻿using Microsoft.AspNetCore.Mvc;
using Thex.Application.Interfaces;
using Thex.Central.Domain.Entities;
using Thex.Central.Web.Base;
using Thex.Central.Web.Filters;
using Thex.Dto;
using Thex.Kernel;
using Tnf.AspNetCore.Mvc.Response;

namespace Thex.Central.Web.Controllers
{
    [Route(RouteConsts.GuestReservationItem)]
    public class GuestReservationItemController : ThexAppController
    {
        private readonly IGuestReservationItemAppService GuestReservationItemAppService;

        public GuestReservationItemController(
            IApplicationUser applicationUser,
            IGuestReservationItemAppService guestReservationItemAppService)
            : base(applicationUser)
        {
            GuestReservationItemAppService = guestReservationItemAppService;
        }

        [HttpPatch("{id}/associate")]
        [ThexAuthorize("CENTRAL_GuestReservationItem_Patch_Associate")]
        [ValidatePropertyIdFilter]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult Associate(long id, [FromQuery] string requestedPropertyId, [FromBody] GuestReservationItemDto dto)
        {
            GuestReservationItemAppService.Associate(id, dto);

            return CreateResponseOnPatch(null, EntityNames.GuestReservationItem);
        }

        [HttpPatch("{id}/disassociate")]
        [ThexAuthorize("CENTRAL_GuestReservationItem_Patch_Disassociate")]
        [ValidatePropertyIdFilter]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult Disassociate(long id, [FromQuery] string requestedPropertyId)
        {
            GuestReservationItemAppService.Disassociate(id);

            return CreateResponseOnPatch(null, EntityNames.GuestReservationItem);
        }
    }
}
