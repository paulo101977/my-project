﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Net;
using Thex.Application.Interfaces;
using Thex.Central.Domain.Entities;
using Thex.Central.Web.Base;
using Thex.Central.Web.Filters;
using Thex.Dto;
using Thex.Dto.ReservationItem;
using Thex.Kernel;
using Tnf.AspNetCore.Mvc.Response;
using Tnf.Dto;

namespace Thex.Central.Web.Controllers
{
    [Route(RouteConsts.ReservationItem)]
    public class ReservationItemController : ThexAppController
    {
        private readonly IReservationItemAppService ReservationItemAppService;

        public ReservationItemController(
            IApplicationUser applicationUser,
            IReservationItemAppService reservationItemAppService)
            : base(applicationUser)
        {
            ReservationItemAppService = reservationItemAppService;
        }

        [HttpGet("property/{propertyId}/reservationId/{reservationId}/confirmationreservation")]
        [ThexAuthorize("CENTRAL_ReservationItem_Get_ConfirmationReservation")]
        [ValidatePropertyIdFilter]
        [ProducesResponseType(typeof(TemplateReservationSlipDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult GetInformationForSlip(int propertyId, [FromQuery] string requestedPropertyId, long reservationId)
        {
            var response = ReservationItemAppService.GenerateHtmlForSlipReservationAccommodation(propertyId, reservationId);

            return new ContentResult
            {
                ContentType = "text/html",
                StatusCode = (int)HttpStatusCode.OK,
                Content = response.SlipReservation
            };
        }

        [HttpPost("property/{propertyId}/reservation/{reservationId}/sendemail")]
        [ThexAuthorize("CENTRAL_ReservationItem_Post_SendEmail")]
        [ValidatePropertyIdFilter]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult SendEmailConfirmationReservation([FromBody] List<string> emailsList, int propertyId, [FromQuery] string requestedPropertyId, long reservationId)
        {
            ReservationItemAppService.SendMailConfirmationReservation(emailsList, propertyId, reservationId);

            return CreateResponseOnPost(null, EntityNames.ReservationItem);
        }

        [HttpGet("{propertyId}/{reservationId}/accommodations")]
        [ThexAuthorize("CENTRAL_ReservationItem_Get_Accommodations")]
        [ValidatePropertyIdFilter]
        [ProducesResponseType(typeof(IListDto<AccommodationsByReservationIdResultDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult GetAll([FromQuery] RequestAllDto requestDto, long propertyId, [FromQuery] string requestedPropertyId, long reservationId)
        {
            var response = ReservationItemAppService.GetAllAccommodations(requestDto, propertyId, reservationId);

            return CreateResponseOnGetAll(response, EntityNames.ReservationItem);
        }

        [HttpPatch("{propertyId}/{reservationItemId}/budget")]
        [ThexAuthorize("CENTRAL_ReservationItem_Patch_Budget")]
        [ValidatePropertyIdFilter]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult PatchReservationItemBudget(int propertyId, [FromQuery] string requestedPropertyId, long reservationItemId, [FromBody] ReservationItemWithBudgetDto dto)
        {
            ReservationItemAppService.PatchReservationItemBudget(propertyId, reservationItemId, dto);

            return CreateResponseOnPatch(null, EntityNames.ReservationItem);
        }

        [HttpPatch("{reservationItemId}/cancel")]
        [ThexAuthorize("CENTRAL_ReservationItem_Patch_Cancel")]
        [ValidatePropertyIdFilter]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult CancelReservationItem(long reservationItemId, [FromQuery] string requestedPropertyId, [FromBody] ReservationItemCancelDto dto)
        {
            this.ReservationItemAppService.CancelReservationItem(reservationItemId, dto);

            return CreateResponseOnPatch(null, EntityNames.ReservationItem);
        }


        [HttpPatch("{reservationItemId}/reactivate")]
        [ThexAuthorize("CENTRAL_ReservationItem_Patch_Reactivate")]
        [ValidatePropertyIdFilter]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult ReactivateReservationItem(long reservationItemId, [FromQuery] string requestedPropertyId)
        {
            ReservationItemAppService.ReactivateReservationItem(reservationItemId);

            return CreateResponseOnPatch(null, EntityNames.ReservationItem);
        }

        [HttpGet("{propertyId}/{reservationItemId}/{language}/guestguestRegistration")]
        [ThexAuthorize("CENTRAL_ReservationItem_Get_GuestGuestRegistration")]
        [ValidatePropertyIdFilter]
        [ProducesResponseType(typeof(IListDto<GuestGuestRegistrationResultDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult GetAll(long propertyId, long reservationItemId, string language, [FromQuery] string requestedPropertyId)
        {
            var response = ReservationItemAppService.GetAllGuestGuestRegistration(propertyId, reservationItemId, language);

            return CreateResponseOnGetAll(response, EntityNames.ReservationItem);
        }
    }
}
