﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Thex.Application.Interfaces;
using Thex.Central.Domain.Entities;
using Thex.Central.Web.Base;
using Thex.Central.Web.Filters;
using Thex.Dto;
using Thex.Kernel;
using Tnf.AspNetCore.Mvc.Response;
using Tnf.Dto;

namespace Thex.Central.Web.Controllers
{
    [Route(RouteConsts.PropertyGuestType)]
    public class PropertyGuestTypeController : ThexAppController
    {
        private readonly IPropertyGuestTypeAppService PropertyGuestTypeAppService;

        public PropertyGuestTypeController(
            IApplicationUser applicationUser,
            IPropertyGuestTypeAppService propertyGuestTypeAppService)
            : base(applicationUser)
        {
            PropertyGuestTypeAppService = propertyGuestTypeAppService;
        }

        [HttpGet("{propertyId}/byproperty")]
        [ThexAuthorize("CENTRAL_PropertyGuestType_Get_ByProperty")]
        [ValidatePropertyIdFilter]
        [ProducesResponseType(typeof(IListDto<PropertyGuestTypeDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> GetAllByPropertyId([FromQuery] string requestedPropertyId, [FromQuery] GetAllPropertyGuestTypeDto requestDto, int propertyId)
        {
            var response = await PropertyGuestTypeAppService.GetAllByPropertyId(requestDto, propertyId);

            return CreateResponseOnGetAll(response, EntityNames.PropertyGuestType);
        }
    }
}
