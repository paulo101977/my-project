﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Thex.Application.Interfaces;
using Thex.Central.Domain.Entities;
using Thex.Central.Web.Base;
using Thex.Central.Web.Filters;
using Thex.Dto;
using Thex.Kernel;
using Tnf.AspNetCore.Mvc.Response;
using Tnf.Dto;

namespace Thex.Central.Web.Controllers
{
    [Route(RouteConsts.GeneralOccupation)]
    public class GeneralOccupationController : ThexAppController
    {
        private readonly IGeneralOccupationAppService GeneralOccupationAppService;
        public GeneralOccupationController(
            IApplicationUser applicationUser,
            IGeneralOccupationAppService generalOccupationAppService) : base(applicationUser)
        {
            GeneralOccupationAppService = generalOccupationAppService;
        }

        [HttpGet]
        [ThexAuthorize("CENTRAL_GeneralOccupation_Get")]
        [ValidatePropertyIdFilter]
        [ProducesResponseType(typeof(IListDto<GeneralOccupationDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public virtual async Task<IActionResult> Get([FromQuery] GetAllGeneralOccupationDto requestDto, [FromQuery] string requestedPropertyId)
        {
            var response = await GeneralOccupationAppService.GetAll(requestDto);

            return CreateResponseOnGetAll(response, EntityNames.GeneralOccupation);
        }
    }
}
