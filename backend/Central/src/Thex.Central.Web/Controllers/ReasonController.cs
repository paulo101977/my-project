﻿using Microsoft.AspNetCore.Mvc;
using Thex.Application.Interfaces;
using Thex.Central.Domain.Entities;
using Thex.Central.Web.Base;
using Thex.Central.Web.Filters;
using Thex.Dto;
using Thex.Kernel;
using Tnf.AspNetCore.Mvc.Response;
using Tnf.Dto;

namespace Thex.Central.Web.Controllers
{
    [Route(RouteConsts.Reason)]
    public class ReasonController : ThexAppController
    {
        protected readonly IReasonAppService ReasonAppService; 

        public ReasonController(
            IApplicationUser applicationUser,
            IReasonAppService reasonAppService) : base(applicationUser)
        {
            ReasonAppService = reasonAppService;      
        }

        [HttpGet]
        [ThexAuthorize("CENTRAL_Reason_Get")]
        [ValidatePropertyIdFilter]
        [ProducesResponseType(typeof(IListDto<ReasonDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult Get([FromQuery] GetAllReasonDto requestDto, [FromQuery] string requestedPropertyId)
        {
            var response = ReasonAppService.GetAllByChainIdAndReasonCategory(requestDto);

            return CreateResponseOnGetAll(response, EntityNames.Reason);
        }
    }
}
