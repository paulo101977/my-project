﻿using Microsoft.AspNetCore.Mvc;
using Thex.Application.Interfaces;
using Thex.Central.Domain.Entities;
using Thex.Central.Web.Base;
using Thex.Central.Web.Filters;
using Thex.Common;
using Thex.Dto;
using Thex.Kernel;
using Tnf.AspNetCore.Mvc.Response;
using Tnf.Dto;

namespace Thex.Central.Web.Controllers
{
    [Route(RouteConsts.Nationality)]
    public class NationalityController : ThexAppController
    {
        protected readonly ICountrySubdivisionTranslationAppService CountrySubdivisionTranslationAppService;

        public NationalityController(
            IApplicationUser applicationUser,
            ICountrySubdivisionTranslationAppService countrySubdivisionTranslationAppService) : base(applicationUser)
        {
            LocalizationSourceName = AppConsts.LocalizationSourceName;
            CountrySubdivisionTranslationAppService = countrySubdivisionTranslationAppService;
        }

        [HttpGet]
        [ThexAuthorize("CENTRAL_Nationality_Get")]
        [ValidatePropertyIdFilter]
        [ProducesResponseType(typeof(IListDto<CountrySubdivisionTranslationDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public virtual IActionResult Get([FromQuery] GetAllCountrySubdivisionTranslationDto requestDto, [FromQuery] string requestedPropertyId)
        {
            var response = CountrySubdivisionTranslationAppService.GetAllNationalities(requestDto);

            return CreateResponseOnGetAll(response, EntityNames.CountrySubdivisionTranslation);
        }
    }
}
