﻿using Microsoft.AspNetCore.Mvc;
using Thex.Application.Interfaces;
using Thex.Central.Domain.Entities;
using Thex.Central.Web.Base;
using Thex.Central.Web.Filters;
using Thex.Dto;
using Thex.Kernel;
using Tnf.AspNetCore.Mvc.Response;
using Tnf.Dto;

namespace Thex.Central.Web.Controllers
{
    [Route(RouteConsts.RoomLayout)]
    public class RoomLayoutController : ThexAppController
    {
        private readonly IRoomLayoutAppService RoomLayoutAppService;

        public RoomLayoutController(
            IApplicationUser applicationUser,
            IRoomLayoutAppService roomLayoutAppService) : base(applicationUser)
        {
            RoomLayoutAppService = roomLayoutAppService;
        }

        [HttpGet("byroomtype/{id}")]
        [ThexAuthorize("CENTRAL_RoomLayout_Get_ByRoomType")]
        [ValidatePropertyIdFilter]
        [ProducesResponseType(typeof(IListDto<RoomLayoutDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult GetAllLayoutsByRoomTypeId([FromQuery] RequestDto request, int id, [FromQuery] string requestedPropertyId)
        {
            var requestDto = new DefaultIntRequestDto(id, request);

            var response = RoomLayoutAppService.GetAllLayoutsByRoomTypeId(requestDto);

            return CreateResponseOnGetAll(response, EntityNames.RoomLayout);
        }
    }
}
