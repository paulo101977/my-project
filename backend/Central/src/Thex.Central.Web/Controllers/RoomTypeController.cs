﻿using Microsoft.AspNetCore.Mvc;
using Thex.Application.Interfaces;
using Thex.Central.Domain.Entities;
using Thex.Central.Web.Base;
using Thex.Central.Web.Filters;
using Thex.Dto.RoomTypes;
using Thex.Kernel;
using Tnf.AspNetCore.Mvc.Response;
using Tnf.Dto;

namespace Thex.Central.Web.Controllers
{
    [Route(RouteConsts.RoomType)]
    public class RoomTypeController : ThexAppController
    {
        protected readonly IRoomTypeAppService RoomTypeAppService;

        public RoomTypeController(
            IApplicationUser applicationUser,
            IRoomTypeAppService roomTypeAppService) : base(applicationUser)
        {
            RoomTypeAppService = roomTypeAppService;
        }

        [HttpGet("{propertyId}/withoverbooking")]
        [ThexAuthorize("CENTRAL_RoomType_Get_WithOverbooking")]
        [ValidatePropertyIdFilter]
        [ProducesResponseType(typeof(IListDto<RoomTypeOverbookingDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult GetAll([FromQuery]GetAllRoomTypesByPropertyAndPeriodDto request, int propertyId, [FromQuery] string requestedPropertyId)
        {
            var response = RoomTypeAppService.GetAllRoomTypesByPropertyAndPeriodDto(request, propertyId);

            return CreateResponseOnGetAll(response, EntityNames.RoomType);
        }
    }
}
