﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Thex.Application.Interfaces;
using Thex.Central.Domain.Entities;
using Thex.Central.Web.Base;
using Thex.Central.Web.Filters;
using Thex.Dto;
using Thex.Kernel;
using Tnf.AspNetCore.Mvc.Response;
using Tnf.Dto;

namespace Thex.Central.Web.Controllers
{
    [Route(RouteConsts.BusinessSource)]
    public class BusinessSourceController : ThexAppController
    {
        private readonly IBusinessSourceAppService BusinessSourceAppService;

        public BusinessSourceController(
            IApplicationUser applicationUser,
            IBusinessSourceAppService businessSourceAppService)
            : base(applicationUser)
        {
            BusinessSourceAppService = businessSourceAppService;
        }

        [HttpGet]
        [ThexAuthorize("CENTRAL_BusinessSource_Get")]
        [ValidatePropertyIdFilter]
        [ProducesResponseType(typeof(IListDto<BusinessSourceDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public virtual async Task<IActionResult> Get([FromQuery] GetAllBusinessSourceDto requestDto, [FromQuery] string requestedPropertyId)
        {
            int propertyId = requestDto.PropertyId;
            var response = BusinessSourceAppService.GetAllByPropertyId(requestDto, propertyId);

            return CreateResponseOnGetAll(response, EntityNames.BusinessSource);
        }
    }
}
