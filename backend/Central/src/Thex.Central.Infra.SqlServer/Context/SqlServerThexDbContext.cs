﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Thex.Central.Infra.Context;
using Thex.GenericLog;
using Thex.Kernel;
using Tnf.Runtime.Session;

namespace Thex.Central.Infra.SqlServer.Context
{
    public class SqlServerCrudDbContext : ThexContext
    {
        public SqlServerCrudDbContext(IApplicationUser applicationUser,
                                      IConfiguration configuration,
                                      DbContextOptions<ThexContext> options,
                                      ITnfSession session,
                                      IGenericLogHandler genericLog)
            : base(applicationUser, configuration, options, session, genericLog)
        {
        }
    }
}
