﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System.IO;
using Thex.Central.Infra.Context;
using Thex.GenericLog;
using Thex.Kernel;
using Tnf.Runtime.Session;

namespace Thex.Central.Infra.SqlServer.Context
{
    public class SqlServerThexDbContextFactory
    {
        private IApplicationUser _applicationUser;
        private IConfiguration _configuration;
        private IGenericLogHandler _genericLog;

        public SqlServerThexDbContextFactory(IApplicationUser applicationUser,
                                             IConfiguration configuration,
                                             IGenericLogHandler genericLog)
            : base()
        {
            _applicationUser = applicationUser;
            _configuration = configuration;
            _genericLog = genericLog;
        }

        public SqlServerCrudDbContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<ThexContext>();

            var configuration = new ConfigurationBuilder()
                                    .SetBasePath(Directory.GetCurrentDirectory())
                                    .AddJsonFile($"appsettings.json", false)
                                    .Build();

            var databaseConfiguration = new DatabaseConfiguration(configuration);

            builder.UseSqlServer(databaseConfiguration.ConnectionString, db => db.UseRowNumberForPaging());

            return new SqlServerCrudDbContext(_applicationUser, _configuration, builder.Options, NullTnfSession.Instance, _genericLog);
        }
    }
}
