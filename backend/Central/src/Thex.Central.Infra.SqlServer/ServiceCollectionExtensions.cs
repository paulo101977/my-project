﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;
using Microsoft.Extensions.DependencyInjection;
using Thex.Central.Infra.Context;
using Thex.Central.Infra.SqlServer.Context;
using Thex.Common;

namespace Thex.Central.Infra.SqlServer
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddCentralSqlServerDependency(this IServiceCollection services)
        {
            services
                .AddInfraDependency()
                .AddTnfDbContext<ThexContext, SqlServerCrudDbContext>((config) =>
                {
                    if (config.ExistingConnection != null)
                        config.DbContextOptions.UseSqlServer(config.ExistingConnection, db => db.UseRowNumberForPaging());
                    else
                        config.DbContextOptions.UseSqlServer(config.ConnectionString, db => db.UseRowNumberForPaging());
                });

            return services;
        }
    }
}
