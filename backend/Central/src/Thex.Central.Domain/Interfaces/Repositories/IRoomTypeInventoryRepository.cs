﻿using System;
using System.Collections.Generic;
using Thex.Central.Domain.Entities;

namespace Thex.Central.Domain.Interfaces.Repositories
{
    public interface IRoomTypeInventoryRepository
    {
        IEnumerable<RoomTypeInventory> GetAllByFilters(DateTime initialDate, DateTime endDate, List<int> propertiesIds);
    }
}
