﻿using System;
using System.Collections.Generic;
using Thex.Central.Domain.Entities;
using Thex.Central.Dto;
using Thex.Central.Dto.Availability;
using Thex.Central.Dto.Room;

namespace Thex.Central.Domain.Interfaces.Services
{
    public interface IRoomTypeDomainService
    {
        List<AvailabilityPropertyColumnDto> GetAvailabilityPropertyColumns(List<RoomTypeInventory> roomTypeInventories);
        List<AvailabilityFooterRowDto> GetAvailabilityTotals(List<AvailabilityPropertyColumnDto> availabilityColumns, List<AvailabilityPropertyRowDto> propertiesRow, DateTime startDate, DateTime finalDate);
        void FillRolesInAvailabilityPropertyColumns(List<AvailabilityPropertyRowDto> propertyAvailability, List<AvailabilityPropertyColumnDto> availabilityList, DateTime startDate, DateTime finalDate);        
        List<AvailabilityRoomTypeFooterRowDto> GetAvailabilityTotalsByProperty(List<AvailabilityRoomTypeColumnDto> availabilityColumns, List<RoomBlockingDto> roomBlockings, List<AvailabilityRoomTypeRowDto> roomTypesRow, DateTime startDate, DateTime finalDate);
        void FillRolesInAvailabilityRoomTypeColumns(List<AvailabilityRoomTypeRowDto> roomTypeAvailability, List<AvailabilityRoomTypeColumnDto> availabilityList, DateTime startDate, DateTime finalDate);
        List<AvailabilityRoomTypeColumnDto> GetAvailabilityRoomTypeColumns(List<ParentRoomsWithTotalChildrens> parentRooms, List<ReservationItem> reservationItems, List<AvailabilityRoomTypeRowDto> roomTypesRow, List<RoomBlockingDto> roomBlockings, List<RoomTypeInventory> roomTypeInventoryList);
    }
}
