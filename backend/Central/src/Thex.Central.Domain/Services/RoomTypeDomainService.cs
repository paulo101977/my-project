﻿using System;
using System.Collections.Generic;
using System.Linq;
using Thex.Central.Domain.Entities;
using Thex.Central.Domain.Interfaces.Services;
using Thex.Central.Dto;
using Thex.Central.Dto.Availability;
using Thex.Central.Dto.Room;
using Thex.Common;
using Thex.Common.Enumerations;
using Tnf.Localization;

namespace Thex.Central.Domain.Services
{
    public class RoomTypeDomainService : IRoomTypeDomainService
    {
        private readonly ILocalizationManager _localizationManager;

        public RoomTypeDomainService(ILocalizationManager localizationManager)
        {
            _localizationManager = localizationManager;
        }

        public List<AvailabilityPropertyColumnDto> GetAvailabilityPropertyColumns(List<RoomTypeInventory> roomTypeInventories)
        {
            var availabilityPropertyList = new List<AvailabilityPropertyColumnDto>();

            var roomTypeInventoryPerDate = roomTypeInventories.Where(x => x.Total > 0)
                                                              .GroupBy(x => new { x.PropertyId, x.Date})
                                                              .Select(gp => new
                                                               {
                                                                   gp.Key.PropertyId,
                                                                   gp.Key.Date,
                                                                   Total = gp.Sum(x => x.Total),
                                                                   Balance = gp.Sum(x => x.Balance)
                                                               })
                                                               .OrderBy(x => x.Date)
                                                               .ThenBy(x => x.PropertyId);                                                        
           
            foreach (var inventory in roomTypeInventoryPerDate)
            {
                var availabilityDay = new AvailabilityPropertyColumnDto
                {
                    PropertyId = inventory.PropertyId,
                    Day = inventory.Date.Date,
                    Date = inventory.Date.ToString("dd/MM/yyyy"),
                    DateFormatted = inventory.Date.Date.ToString("s"),
                    Rooms = inventory.Total - inventory.Balance,
                    AvailableRooms = inventory.Balance,
                    RoomTotal = inventory.Total
                };

                availabilityPropertyList.Add(availabilityDay);
            }

            return availabilityPropertyList;
        }

        public List<AvailabilityFooterRowDto> GetAvailabilityTotals(List<AvailabilityPropertyColumnDto> availabilityColumns, List<AvailabilityPropertyRowDto> propertiesRow, DateTime startDate, DateTime finalDate)
        {
            var availabilityFooterList = new List<AvailabilityFooterRowDto>();
            var propertiesRowTotal = propertiesRow.Sum(e => e.Total);
            var footerValues = GetAvailabilityTotalsAndPercentageByDate(availabilityColumns, startDate, finalDate, propertiesRowTotal);

            var footerPercentage = new AvailabilityFooterRowDto
            {
                Id = (int)AvailabilityGridEnum.AvailabilityGridPercentage,
                Name = _localizationManager.GetString(AppConsts.LocalizationSourceName, AvailabilityGridEnum.AvailabilityGridPercentage.ToString()),
                Total = propertiesRowTotal,
                Values = footerValues.Value
            };


            var footerTotal = new AvailabilityFooterRowDto
            {
                Id = (int)AvailabilityGridEnum.AvailabilityGridTotal,
                Name = _localizationManager.GetString(AppConsts.LocalizationSourceName, AvailabilityGridEnum.AvailabilityGridTotal.ToString()),
                Total = propertiesRowTotal,
                Values = footerValues.Key
            };
           
            availabilityFooterList.Add(footerPercentage);
            availabilityFooterList.Add(footerTotal);

            return availabilityFooterList;
        }

        private KeyValuePair<List<AvailabilityFooterColumnDto>, List<AvailabilityFooterColumnDto>> GetAvailabilityTotalsAndPercentageByDate(List<AvailabilityPropertyColumnDto> availabilityColumns, DateTime startDate, DateTime finalDate, int total)
        {
            var totalList = new List<AvailabilityFooterColumnDto>();
            var percentageList = new List<AvailabilityFooterColumnDto>();
            var date = startDate;

            while (date <= finalDate.Date)
            {
                var occupiedDay = availabilityColumns.Where(exp => exp.Date == date.Date.ToString("dd/MM/yyyy")).Sum(exp => exp.Rooms);

                var totalResult = new AvailabilityFooterColumnDto();
                var percentageResult = new AvailabilityFooterColumnDto();

                totalResult.Date = percentageResult.Date = date.ToString("dd/MM/yyyy");
                totalResult.DateFormatted = percentageResult.DateFormatted = date.ToString("s");

                totalResult.Number = total - occupiedDay;

                percentageResult.Number = CalculateOverbookingPercentage(total, occupiedDay);

                if (totalResult.Number < 0)
                    totalResult.Number = 0;               

                totalList.Add(totalResult);
                percentageList.Add(percentageResult);


                date = date.AddDays(1);
            }

            return new KeyValuePair<List<AvailabilityFooterColumnDto>, List<AvailabilityFooterColumnDto>>(totalList, percentageList);
        }

        private decimal CalculateOverbookingPercentage(int totalRooms, int occupiedRooms)
        {
            if (totalRooms == 0)
                return 0;
            return Math.Round(100 * Convert.ToDecimal(occupiedRooms) / Convert.ToDecimal(totalRooms), 2);

        }

        private KeyValuePair<List<AvailabilityRoomTypeFooterColumnDto>, List<AvailabilityRoomTypeFooterColumnDto>> GetAllBlockingByDate(List<RoomBlockingDto> roomBlockings, DateTime startDate, DateTime finalDate)
        {
            var date = startDate;
            var blockingList = new List<AvailabilityRoomTypeFooterColumnDto>();
            while (date <= finalDate.Date)
            {
                var blockingDay = RoomBlockingCount(roomBlockings, date);
                var blockedResult = new AvailabilityRoomTypeFooterColumnDto();

                blockedResult.Number = blockingDay;
                blockedResult.Date = date.ToString("dd/MM/yyyy");
                blockedResult.DateFormatted = date.ToString("s");

                blockingList.Add(blockedResult);
                date = date.AddDays(1);
            }
            return new KeyValuePair<List<AvailabilityRoomTypeFooterColumnDto>, List<AvailabilityRoomTypeFooterColumnDto>>(null, blockingList);

        }

        private KeyValuePair<List<AvailabilityRoomTypeFooterColumnDto>, List<AvailabilityRoomTypeFooterColumnDto>> GetAllBlockingRoomTypeByDate(List<RoomBlockingDto> roomBlockings, DateTime startDate, DateTime finalDate)
        {
            var date = startDate;
            var blockingList = new List<AvailabilityRoomTypeFooterColumnDto>();
            while (date <= finalDate.Date)
            {
                var blockingDay = RoomBlockingCount(roomBlockings, date);
                var blockedResult = new AvailabilityRoomTypeFooterColumnDto();

                blockedResult.Number = blockingDay;
                blockedResult.Date = date.ToString("dd/MM/yyyy");
                blockedResult.DateFormatted = date.ToString("s");

                blockingList.Add(blockedResult);
                date = date.AddDays(1);
            }
            return new KeyValuePair<List<AvailabilityRoomTypeFooterColumnDto>, List<AvailabilityRoomTypeFooterColumnDto>>(null, blockingList);

        }

        private int RoomBlockingCount(List<RoomBlockingDto> roomBlockings, DateTime date)
        {
            return roomBlockings.Where(x => x.BlockingStartDate.Date <= date && x.BlockingEndDate.Date >= date).Count();
        }

        public void FillRolesInAvailabilityPropertyColumns(List<AvailabilityPropertyRowDto> propertyAvailability, List<AvailabilityPropertyColumnDto> availabilityList, DateTime startDate, DateTime finalDate)
        {            
            foreach (var property in propertyAvailability)
            {
                var date = startDate;
                while (date <= finalDate.Date)
                {
                    var dateString = date.ToString("dd/MM/yyyy");

                    if (availabilityList.Any(exp => exp.Date == dateString && exp.PropertyId == property.Id))
                    {
                        date = date.AddDays(1);
                        continue;
                    }

                    availabilityList.Add(new AvailabilityPropertyColumnDto
                    {
                        PropertyId = property.Id,
                        Date = dateString,
                        DateFormatted = date.ToString("s"),
                        AvailableRooms = property.Total,
                        Rooms = 0
                    });

                    date = date.AddDays(1);
                }
            }
        }       

        private KeyValuePair<List<AvailabilityRoomTypeFooterColumnDto>, List<AvailabilityRoomTypeFooterColumnDto>> GetAvailabilityTotalsAndPercentageRoomTypeByDate(List<AvailabilityRoomTypeColumnDto> availabilityColumns, List<RoomBlockingDto> roomBlockings, DateTime startDate, DateTime finalDate, int total)
        {
            var totalList = new List<AvailabilityRoomTypeFooterColumnDto>();
            var percentageList = new List<AvailabilityRoomTypeFooterColumnDto>();
            var date = startDate;

            while (date <= finalDate.Date)
            {
                var availabilityDay = availabilityColumns.Where(exp => exp.Date == date.Date.ToString("dd/MM/yyyy")).Sum(exp => exp.Rooms);

                var blockingDay = roomBlockings.Count(exp => exp.BlockingStartDate.Date <= date.Date && exp.BlockingEndDate.Date >= date.Date);

                var totalResult = new AvailabilityRoomTypeFooterColumnDto();
                var percentageResult = new AvailabilityRoomTypeFooterColumnDto();

                totalResult.Date = percentageResult.Date = date.ToString("dd/MM/yyyy");
                totalResult.DateFormatted = percentageResult.Date = date.ToString("s");

                totalResult.Number = total - availabilityDay;

                percentageResult.Number = CalculateOverbookingPercentage(total, (availabilityDay - blockingDay));

                totalList.Add(totalResult);
                percentageList.Add(percentageResult);


                date = date.AddDays(1);
            }

            return new KeyValuePair<List<AvailabilityRoomTypeFooterColumnDto>, List<AvailabilityRoomTypeFooterColumnDto>>(totalList, percentageList);
        }

        public List<AvailabilityRoomTypeFooterRowDto> GetAvailabilityTotalsByProperty(List<AvailabilityRoomTypeColumnDto> availabilityColumns, List<RoomBlockingDto> roomBlockings, List<AvailabilityRoomTypeRowDto> roomTypesRow, DateTime startDate, DateTime finalDate)
        {
            var availabilityFooterList = new List<AvailabilityRoomTypeFooterRowDto>();
            var roomTypesRowTotal = roomTypesRow.Sum(e => e.Total);
            var footerValues = GetAvailabilityTotalsAndPercentageRoomTypeByDate(availabilityColumns, roomBlockings, startDate, finalDate, roomTypesRowTotal);
            var footerValuesBlocked = GetAllBlockingByDate(roomBlockings, startDate, finalDate);

            var footerPercentage = new AvailabilityRoomTypeFooterRowDto
            {
                Id = (int)AvailabilityGridEnum.AvailabilityGridPercentage,
                Name = _localizationManager.GetString(AppConsts.LocalizationSourceName, AvailabilityGridEnum.AvailabilityGridPercentage.ToString()),
                Total = roomTypesRowTotal,
                Values = footerValues.Value
            };

            var footerTotal = new AvailabilityRoomTypeFooterRowDto
            {
                Id = (int)AvailabilityGridEnum.AvailabilityGridTotal,
                Name = _localizationManager.GetString(AppConsts.LocalizationSourceName, AvailabilityGridEnum.AvailabilityGridTotal.ToString()),
                Total = roomTypesRowTotal,
                Values = footerValues.Key
            };          
           
            availabilityFooterList.Add(footerTotal);
            availabilityFooterList.Add(footerPercentage);

            return availabilityFooterList;
        }

        public void FillRolesInAvailabilityRoomTypeColumns(List<AvailabilityRoomTypeRowDto> roomTypeAvailability, List<AvailabilityRoomTypeColumnDto> availabilityList, DateTime startDate, DateTime finalDate)
        {
            //preenche as datas que faltam para cada roomtype
            foreach (var roomType in roomTypeAvailability)
            {
                var date = startDate;
                while (date <= finalDate.Date)
                {
                    var dateString = date.ToString("dd/MM/yyyy");

                    if (availabilityList.Any(exp => exp.Date == dateString && exp.RoomTypeId == roomType.Id))
                    {
                        date = date.AddDays(1);
                        continue;
                    }

                    availabilityList.Add(new AvailabilityRoomTypeColumnDto
                    {
                        RoomTypeId = roomType.Id,
                        Date = dateString,
                        DateFormatted = date.ToString("s"),
                        AvailableRooms = roomType.Total,
                        Rooms = 0
                    });

                    date = date.AddDays(1);
                }
            }
        }

        public List<AvailabilityRoomTypeColumnDto> GetAvailabilityRoomTypeColumns(List<ParentRoomsWithTotalChildrens> parentRooms, List<ReservationItem> reservationItems, List<AvailabilityRoomTypeRowDto> roomTypesRow, List<RoomBlockingDto> roomBlockings, List<RoomTypeInventory> roomTypeInventoryList)
        {
            var availabilityRoomTypeList = new List<AvailabilityRoomTypeColumnDto>();

            var roomTypeInventoryPerDate = roomTypeInventoryList.Where(x => x.Total > 0)
                                                             .GroupBy(x => new { x.RoomTypeId, x.Date })
                                                             .Select(gp => new
                                                             {
                                                                 gp.Key.RoomTypeId,
                                                                 gp.Key.Date,
                                                                 Total = gp.Sum(x => x.Total),
                                                                 Balance = gp.Sum(x => x.Balance)
                                                             })
                                                              .OrderBy(x => x.Date)
                                                              .ThenBy(x => x.RoomTypeId);

            foreach (var inventory in roomTypeInventoryPerDate)
            {
                var availabilityDay = new AvailabilityRoomTypeColumnDto
                {
                    Day = inventory.Date.Date,
                    Date = inventory.Date.ToString("dd/MM/yyyy"),
                    DateFormatted = inventory.Date.Date.ToString("s"),
                    RoomTypeId = inventory.RoomTypeId,
                    Rooms = inventory.Total - inventory.Balance,
                    AvailableRooms = inventory.Balance,
                    RoomTotal = inventory.Total
                };

                availabilityRoomTypeList.Add(availabilityDay);
            }

            foreach (var blockedRoom in roomBlockings)
            {
                var startDate = blockedRoom.BlockingStartDate;
                var finalDate = blockedRoom.BlockingEndDate;

                if (startDate > finalDate)
                    continue;


                while (startDate.Date <= finalDate.Date)
                {
                    var availabilityDayInList = availabilityRoomTypeList.FirstOrDefault(exp => exp.Date == startDate.ToString("dd/MM/yyyy") && exp.RoomTypeId == blockedRoom.RoomTypeId);

                    var conjugated = parentRooms.FirstOrDefault(exp => exp.Id == blockedRoom.RoomId);
                    var roomType = roomTypesRow.FirstOrDefault(exp => exp.Id == blockedRoom.RoomTypeId);

                    if (availabilityDayInList == null)
                    {
                        availabilityDayInList = new AvailabilityRoomTypeColumnDto
                        {
                            Day = startDate.Date,
                            Date = startDate.Date.ToString("dd/MM/yyyy"),
                            DateFormatted = startDate.Date.ToString("s"),
                            RoomTypeId = blockedRoom.RoomTypeId
                        };

                        availabilityRoomTypeList.Add(availabilityDayInList);
                    }

                    var roomTypeInventory = roomTypeInventoryList.FirstOrDefault(r => r.Date.Date == availabilityDayInList.Day && r.RoomTypeId == availabilityDayInList.RoomTypeId);

                    if (roomTypeInventory != null)
                    {
                        availabilityDayInList.Rooms = roomTypeInventory.Total - roomTypeInventory.Balance;
                        availabilityDayInList.AvailableRooms = roomTypeInventory.Balance;
                    }

                    startDate = startDate.AddDays(1);
                }
            }

            return availabilityRoomTypeList;
        }
    }
}
