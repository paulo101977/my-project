﻿using System.Collections.Generic;
using Tnf.Notifications;

namespace Thex.Central.Domain.Entities
{
    public partial class RoomType : IEntityInt
    {
        public RoomType()
        {
            ReservationItemReceivedRoomTypeList = new HashSet<ReservationItem>();
            ReservationItemRequestedRoomTypeList = new HashSet<ReservationItem>();
            RoomList = new HashSet<Room>();
            RoomTypeInventoryList = new HashSet<RoomTypeInventory>();
        }

        public int Id { get; set; }
        public int PropertyId { get; internal set; }
        public int Order { get; internal set; }
        public string Name { get; internal set; }
        public string Abbreviation { get; internal set; }
        public int AdultCapacity { get; internal set; }
        public int ChildCapacity { get; internal set; }
        public int FreeChildQuantity1 { get; internal set; }
        public int FreeChildQuantity2 { get; internal set; }
        public int FreeChildQuantity3 { get; internal set; }
        public bool IsActive { get; internal set; }
        public decimal MaximumRate { get; internal set; }
        public decimal MinimumRate { get; internal set; }
        public string DistributionCode { get; internal set; }


        public virtual Property Property { get; internal set; }
        public virtual ICollection<ReservationItem> ReservationItemReceivedRoomTypeList { get; internal set; }
        public virtual ICollection<ReservationItem> ReservationItemRequestedRoomTypeList { get; internal set; }
        public virtual ICollection<Room> RoomList { get; internal set; }
        public virtual ICollection<RoomTypeInventory> RoomTypeInventoryList { get; internal set; }

        public enum EntityError
        {
            RoomTypeMustHavePropertyId,
            RoomTypeMustHaveName,
            RoomTypeOutOfBoundName,
            RoomTypeMustHaveAbbreviation,
            RoomTypeOutOfBoundAbbreviation,
            RoomTypeMustHaveOrder,
            RoomTypeMustHaveAdultCapacity,
            RoomTypeHasReserveForward,
            RoomTypeShouldNotDuplicateAbreviation,
            RoomTypeMustHaveBedTypes,
            RoomTypeMinimumRateLargerMaximumRate,
            RoomTypeRateInvalid,
            RoomTypeCountFreeChildrenLargerCapacity
        }

        public static Builder Create(INotificationHandler handler)
            => new Builder(handler);

        public static Builder Create(INotificationHandler handler, RoomType instance)
            => new Builder(handler, instance);
    }
}
