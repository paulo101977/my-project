﻿using System;
using System.Collections.Generic;
using Tnf.Notifications;

namespace Thex.Central.Domain.Entities
{
    public partial class Room: ThexMultiTenantFullAuditedEntity, IEntityInt
    {
        public Room()
        {
            ReservationItemList = new HashSet<ReservationItem>();
            ChildRoomList = new HashSet<Room>();
        }
        public int Id { get; set; }
        public int? ParentRoomId { get; internal set; }
        public int PropertyId { get; internal set; }
        public int RoomTypeId { get; internal set; }
        public string Building { get; internal set; }
        public string Wing { get; internal set; }
        public string Floor { get; internal set; }
        public string RoomNumber { get; internal set; }
        public string Remarks { get; internal set; }
        public bool IsActive { get; internal set; }
        public Guid HousekeepingStatusPropertyId { get; internal set; }
        public DateTime? HousekeepingStatusLastModificationTime { get; internal set; }

        public virtual Room ParentRoom { get; internal set; }
        public virtual ICollection<Room> ChildRoomList { get; internal set; }
        public virtual Property Property { get; internal set; }
        public virtual RoomType RoomType { get; internal set; }
        public virtual int? ChildRoomTotal { get; internal set; }
        public virtual ICollection<ReservationItem> ReservationItemList { get; internal set; }
        

        public enum EntityError
        {
            RoomMustHavePropertyId,
            RoomMustHaveRoomTypeId,
            RoomOutOfBoundBuilding,
            RoomOutOfBoundWing,
            RoomOutOfBoundFloor,
            RoomMustHaveRoomNumber,
            RoomOutOfBoundRoomNumber,
            RoomOutOfBoundRemarks,
            RoomTypeHasReserveForward
        }

        public static Builder Create(INotificationHandler handler)
            => new Builder(handler);

        public static Builder Create(INotificationHandler handler, Room instance)
            => new Builder(handler, instance);
    }
}
