﻿using System;

namespace Thex.Central.Domain.Entities
{
    public partial class RoomTypeInventory : ThexMultiTenantFullAuditedEntity, IEntityGuid
    {
        public Guid Id { get; set; }
        public int PropertyId { get; set; }
        public int RoomTypeId { get; set; }
        public int Total { get; set; }
        public int Balance { get; set; }
        public int BlockedQuantity { get; set; }
        public DateTime Date { get; set; }

        public virtual Property Property { get; internal set; }
        public virtual RoomType RoomType { get; internal set; }
    }
}
