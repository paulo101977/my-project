﻿using System;
using System.Collections.Generic;
using Tnf.Notifications;

namespace Thex.Central.Domain.Entities
{
	public partial class Reservation : ThexMultiTenantFullAuditedEntity, IEntityLong
	{
		public Reservation()
		{
			ReservationItemList = new HashSet<ReservationItem>();
		}
        public long Id { get; set; }
        public Guid ReservationUid { get; internal set; }
		public string ReservationCode { get; internal set; }
		public string ReservationVendorCode { get; internal set; }
		public int PropertyId { get; internal set; }
		public string ContactName { get; internal set; }
		public string ContactEmail { get; internal set; }
		public string ContactPhone { get; internal set; }
		public string InternalComments { get; internal set; }
		public string ExternalComments { get; internal set; }
		public string GroupName { get; internal set; }
		public DateTime? Deadline { get; internal set; }
		public bool UnifyAccounts { get; internal set; }
		public int? BusinessSourceId { get; internal set; }
		public int? MarketSegmentId { get; internal set; }
		public Guid? CompanyClientId { get; internal set; }
		public Guid? CompanyClientContactPersonId { get; internal set; }
		 public Guid? RatePlanId { get; internal set; }

		public virtual Property Property { get; internal set; }
		public virtual ICollection<ReservationItem> ReservationItemList { get; internal set; }

		public enum EntityError
		{
			ReservationMustHaveReservationCode,
			ReservationOutOfBoundReservationCode,
			ReservationOutOfBoundReservationVendorCode,
			ReservationMustHavePropertyId,
			ReservationOutOfBoundContactName,
			ReservationOutOfBoundContactEmail,
			ReservationOutOfBoundContactPhone,
			ReservationOutOfBoundInternalComments,
			ReservationOutOfBoundExternalComments,
			ReservationOutOfBoundGroupName,
			ReservationInvalidCreationTime,
			ReservationInvalidLastModificationTime,
			ReservationInvalidDeletionTime,
			ReservationInvalidDeadline,
			ReservationNotModify,
            ReportDateError,
            ReportDateInvalid,
            ReservationDateInvalidSystemDate,
            ReservationDeadlineCouldNotBeGreaterThanDateofEntry
        }

        public static Builder Create(INotificationHandler handler)
            => new Builder(handler);

        public static Builder Create(INotificationHandler handler, Reservation instance)
            => new Builder(handler, instance);
    }
}
