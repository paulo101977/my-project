﻿using System;
using System.Collections.Generic;
using Tnf.Notifications;

namespace Thex.Central.Domain.Entities
{
    public partial class ReservationItem : ThexMultiTenantFullAuditedEntity, IEntityLong
    {
        public ReservationItem()
        {
        }
        public long Id { get; set; }
        public Guid ReservationItemUid { get; internal set; }
        public long ReservationId { get; internal set; }
        public int? ReasonId { get; internal set; }
        public DateTime EstimatedArrivalDate { get; internal set; }
        public DateTime? CheckInDate { get; internal set; }
        public DateTime EstimatedDepartureDate { get; internal set; }
        public DateTime? CheckOutDate { get; internal set; }
        public DateTime? CancellationDate { get; internal set; }
        public string CancellationDescription { get; internal set; }
        public string ReservationItemCode { get; internal set; }
        public int RequestedRoomTypeId { get; internal set; }
        public int ReceivedRoomTypeId { get; internal set; }
        public int? RoomId { get; internal set; }
        public byte AdultCount { get; internal set; }
        public byte ChildCount { get; internal set; }
        public int ReservationItemStatusId { get; internal set; }
        public Guid RoomLayoutId { get; internal set; }
        public byte ExtraBedCount { get; internal set; }
        public byte ExtraCribCount { get; internal set; }
        public Guid? RatePlanId { get; internal set; }
        public Guid? CurrencyId { get; internal set; }
        public int? MealPlanTypeId { get; internal set; }
        public int? GratuityTypeId { get; internal set; }
        public bool WalkIn { get; internal set; }

        public DateTime? CheckInHour { get; internal set; }

        public DateTime? CheckOutHour { get; internal set; }
       
        public virtual RoomType ReceivedRoomType { get; internal set; }
        public virtual RoomType RequestedRoomType { get; internal set; }
        public virtual Reservation Reservation { get; internal set; }
        public virtual Room Room { get; internal set; }

        public enum EntityError
        {
            ReservationItemMustHaveReservationId,
            ReservationItemInvalidEstimatedArrivalDate,
            ReservationItemInvalidCheckInDate,
            ReservationItemInvalidEstimatedDepartureDate,
            ReservationItemInvalidCheckOutDate,
            ReservationItemInvalidCancellationDate,
            ReservationItemOutOfBoundCancellationDescription,
            ReservationItemMustHaveReservationItemCode,
            ReservationItemOutOfBoundReservationItemCode,
            ReservationItemMustHaveRequestedRoomTypeId,
            ReservationItemMustHaveReceivedRoomTypeId,
            ReservationItemMustHaveReservationItemStatusId,
            ReservationItemMustHaveRoomLayoutId,
            ReservationItemInvalidCreationTime,
            ReservationItemInvalidLastModificationTime,
            ReservationItemInvalidDeletionTime
        }

        public static Builder Create(INotificationHandler handler)
            => new Builder(handler);

        public static Builder Create(INotificationHandler handler, ReservationItem instance)
            => new Builder(handler, instance);
    }
}
