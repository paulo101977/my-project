﻿using System;
using System.Collections.Generic;
using Tnf.Notifications;

namespace Thex.Central.Domain.Entities
{
	public partial class Property : ThexMultiTenantFullAuditedEntity, IEntityInt
    {
		public Property()
		{
			ReservationList = new HashSet<Reservation>();
			RoomList = new HashSet<Room>();
			RoomTypeList = new HashSet<RoomType>();
            RoomTypeInventoryList = new HashSet<RoomTypeInventory>();
        }
        public int Id { get; set; }
        public int CompanyId { get; internal set; }
		public int BrandId { get; internal set; }
		public string Name { get; internal set; }
		public Guid PropertyUId { get; internal set; }
        public int PropertyStatusId { get; internal set; }


		public virtual ICollection<Reservation> ReservationList { get; internal set; }
		public virtual ICollection<Room> RoomList { get; internal set; }
		public virtual ICollection<RoomType> RoomTypeList { get; internal set; }
        public virtual ICollection<RoomTypeInventory> RoomTypeInventoryList { get; set; }

        public enum EntityError
		{
			PropertyMustHaveCompanyId,
			PropertyMustHavePropertyTypeId,
			PropertyMustHaveBrandId,
			PropertyMustHaveName,
			PropertyOutOfBoundName,
			PropertyInvalidCreationTime,
			PropertyInvalidLastModificationTime,
			PropertyInvalidDeletionTime,
			PropertyMustHaveTypeId
        }

        public static Builder Create(INotificationHandler handler)
            => new Builder(handler);

        public static Builder Create(INotificationHandler handler, Property instance)
            => new Builder(handler, instance);
    }
}
