﻿namespace Thex.Central.Domain.Entities
{
    public partial class EntityNames
    {
        public const string Property = "Property";
        public const string Availability = "Availability";
        public const string RateProposal = "RateProposal";
        public const string ReservationItem = "ReservationItem";
        public const string PropertyParameter = "PropertyParameter";
        public const string BusinessSource = "BusinessSource";
        public const string MarketSegment = "MarketSegment";
        public const string Reason = "Reason";
        public const string PlasticBrandProperty = "PlasticBrandProperty";
        public const string CompanyClient = "CompanyClient";
        public const string RoomType = "RoomType";
        public const string ReservationBudget = "ReservationBudget";
        public const string PropertyMealPlanType = "PropertyMealPlanType";
        public const string RoomLayout = "RoomLayout";
        public const string Guest = "Guest";
        public const string Reservation = "Reservation";
        public const string PropertyGuestType = "PropertyGuestType";
        public const string CountrySubdivision = "CountrySubdivision";
        public const string DocumentType = "DocumentType";
        public const string GuestRegistration = "GuestRegistration";
        public const string GuestReservationItem = "GuestReservationItem";
        public const string GeneralOccupation = "GeneralOccupation";
        public const string CountrySubdivisionTranslation = "CountrySubdivisionTranslation";
        public const string TransportationType = "TransportationType";
        public const string RatePlan = "RatePlan";
    }
}
