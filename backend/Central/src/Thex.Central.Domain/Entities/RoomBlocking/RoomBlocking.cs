﻿using System;
using Tnf.Notifications;

namespace Thex.Central.Domain.Entities
{
    public partial class RoomBlocking : ThexMultiTenantFullAuditedEntity, IEntityGuid
    {
        public Guid Id { get; set; }
        public int PropertyId { get; internal set; }
        public int RoomId { get; internal set; }
        public DateTime BlockingStartDate { get; internal set; }
        public DateTime BlockingEndDate { get; internal set; }
        public int ReasonId { get; internal set; }
        public string Comments { get; internal set; }

        public virtual Property Property { get; internal set; }
        public virtual Room Room { get; internal set; }

        public enum EntityError
        {
            RoomBlockingMustHavePropertyId,
            RoomBlockingMustHaveRoomId,
            RoomBlockingInvalidBlockingStartDate,
            RoomBlockingInvalidBlockingEndDate,
            RoomBlockingOutOfBoundComments,
            RoomBusyForPeriod,
            RoomBlockingIsBlockedForPeriod,
            RoomBlockingExistsReservationForPeriod,
            RoomBlockingIsBlockedForPeriodByRoomId,
            Commom

        }

        public static Builder Create(INotificationHandler handler)
            => new Builder(handler);

        public static Builder Create(INotificationHandler handler, RoomBlocking instance)
            => new Builder(handler, instance);
    }
}
