﻿using System;
using Thex.Kernel;

namespace Thex.Central.Domain.Entities
{
    [Serializable]
    public abstract class ThexMultiTenantFullAuditedEntity : ThexFullAuditedEntity
    {
        public Guid TenantId { get; set; }

        public void SetTenant(IApplicationUser applicationUser)
        {
            base.SetAudit(applicationUser);
            this.TenantId = applicationUser.TenantId;
        }
    }
}
