﻿using System;
using Tnf.Builder;
using Tnf.Notifications;

namespace Thex.Central.Domain.Entities
{
    public partial class RoomTypeInventory
    {
        public class Builder : Builder<RoomTypeInventory>
        {
            public Builder(INotificationHandler handler) : base(handler)
            {

            }

            public Builder(INotificationHandler handler, RoomTypeInventory instance) : base(handler, instance)
            {
            }

            public virtual Builder WithId(Guid id)
            {
                Instance.Id = id;
                return this;
            }

            public virtual Builder WithPropertyId(int propertyId)
            {
                Instance.PropertyId = propertyId;
                return this;
            }
            public virtual Builder WithRoomTypeId(int roomTypeId)
            {
                Instance.RoomTypeId = roomTypeId;
                return this;
            }
            public virtual Builder WithTotal(int total)
            {
                Instance.Total = total;
                return this;
            }
            public virtual Builder WithBalance(int balance)
            {
                Instance.Balance = balance;
                return this;
            }
            public virtual Builder WithBlockedQuantity(int BlockedQuantity)
            {
                Instance.BlockedQuantity = BlockedQuantity;
                return this;
            }
            public virtual Builder WithDate(DateTime date)
            {
                Instance.Date = date;
                return this;
            }
        }
    }
}
