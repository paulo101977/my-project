﻿using System;
using Tnf.Builder;
using Tnf.Notifications;
using Thex.Common;
using Tnf.Specifications;

namespace Thex.Central.Domain.Entities
{
    public partial class RoomBlocking
    {
        public class Builder : Builder<RoomBlocking>
        {
            public Builder(INotificationHandler handler) : base(handler)
            {
            }

            public Builder(INotificationHandler handler, RoomBlocking instance) : base(handler, instance)
            {
            }

            public virtual Builder WithId(Guid id)
            {
                Instance.Id = id;
                return this;
            }
            public virtual Builder WithPropertyId(int propertyId)
            {
                Instance.PropertyId = propertyId;
                return this;
            }
            public virtual Builder WithRoomId(int roomId)
            {
                Instance.RoomId = roomId;
                return this;
            }
            public virtual Builder WithBlockingStartDate(DateTime blockingStartDate)
            {
                Instance.BlockingStartDate = blockingStartDate;
                return this;
            }
            public virtual Builder WithBlockingEndDate(DateTime blockingEndDate)
            {
                Instance.BlockingEndDate = blockingEndDate;
                return this;
            }
            public virtual Builder WithReasonId(int reasonId)
            {
                Instance.ReasonId = reasonId;
                return this;
            }
            public virtual Builder WithComments(string comments)
            {
                Instance.Comments = comments;
                return this;
            }

            protected override void Specifications()
            {
                AddSpecification(new ExpressionSpecification<RoomBlocking>(
                    AppConsts.LocalizationSourceName,
                    RoomBlocking.EntityError.RoomBlockingMustHavePropertyId,
                    w => w.PropertyId != default(int)));

                AddSpecification(new ExpressionSpecification<RoomBlocking>(
                    AppConsts.LocalizationSourceName,
                    RoomBlocking.EntityError.RoomBlockingMustHaveRoomId,
                    w => w.RoomId != default(int)));

                AddSpecification(new ExpressionSpecification<RoomBlocking>(
                    AppConsts.LocalizationSourceName,
                    RoomBlocking.EntityError.RoomBlockingInvalidBlockingStartDate,
                    w => w.BlockingStartDate >= new DateTime(1753, 1, 1) && w.BlockingStartDate <= DateTime.MaxValue));

                AddSpecification(new ExpressionSpecification<RoomBlocking>(
                    AppConsts.LocalizationSourceName,
                    RoomBlocking.EntityError.RoomBlockingInvalidBlockingEndDate,
                    w => w.BlockingEndDate >= new DateTime(1753, 1, 1) && w.BlockingEndDate <= DateTime.MaxValue));

                AddSpecification(new ExpressionSpecification<RoomBlocking>(
                    AppConsts.LocalizationSourceName,
                    RoomBlocking.EntityError.RoomBlockingOutOfBoundComments,
                    w => string.IsNullOrWhiteSpace(w.Comments) || w.Comments.Length > 0 && w.Comments.Length <= 4000));



            }
        }
    }
}
