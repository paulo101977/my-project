﻿using System;
using System.Collections.Generic;
using Thex.Common;
using Tnf.Builder;
using Tnf.Notifications;
using Tnf.Specifications;

namespace Thex.Central.Domain.Entities
{
    public partial class Room
    {
	public class Builder : Builder<Room>
	{
		public Builder(INotificationHandler handler) : base(handler)
		{
		}

		public Builder(INotificationHandler handler, Room instance) : base(handler, instance)
		{
		}
		
		public virtual Builder WithId(int id)
		{
			Instance.Id = id;
			return this;
		}
		public virtual Builder WithParentRoomId(int? parentRoomId)
		{
			Instance.ParentRoomId = parentRoomId <= 0 ? null : parentRoomId;
			return this;
		}
		public virtual Builder WithPropertyId(int propertyId)
		{
			Instance.PropertyId = propertyId;
			return this;
		}
		public virtual Builder WithRoomTypeId(int roomTypeId)
		{
			Instance.RoomTypeId = roomTypeId;
			return this;
		}
		public virtual Builder WithBuilding(string building)
		{
			Instance.Building = building;
			return this;
		}
		public virtual Builder WithWing(string wing)
		{
			Instance.Wing = wing;
			return this;
		}
		public virtual Builder WithFloor(string floor)
		{
			Instance.Floor = floor;
			return this;
		}
		public virtual Builder WithRoomNumber(string roomNumber)
		{
			Instance.RoomNumber = roomNumber;
			return this;
		}
		public virtual Builder WithRemarks(string remarks)
		{
			Instance.Remarks = remarks;
			return this;
		}
		public virtual Builder WithIsActive(bool isActive)
		{
			Instance.IsActive = isActive;
			return this;
		}
        public virtual Builder WithHousekeepingStatusPropertyId(Guid housekeepingStatusPropertyId)
        {
            Instance.HousekeepingStatusPropertyId = housekeepingStatusPropertyId;
            return this;
        }

        public virtual Builder WithChildRoomList(IList<int> childRoomIdList)
		{
			foreach (int roomId in childRoomIdList)
				Instance.ChildRoomList.Add(new Room { Id = roomId });

			return this;
		}

        //public virtual Builder WithHousekeepingStatusLastModificationTime(DateTime? dateTime)
        //{
        //    Instance.HousekeepingStatusLastModificationTime = dateTime;
        //    return this;
        //}


        protected override void Specifications()
		{
			AddSpecification(new ExpressionSpecification<Room>(
				AppConsts.LocalizationSourceName, 
				Room.EntityError.RoomMustHavePropertyId, 
				w => w.PropertyId != default(int)));

			AddSpecification(new ExpressionSpecification<Room>(
				AppConsts.LocalizationSourceName, 
				Room.EntityError.RoomMustHaveRoomTypeId, 
				w => w.RoomTypeId != default(int)));

			AddSpecification(new ExpressionSpecification<Room>(
				AppConsts.LocalizationSourceName, 
				Room.EntityError.RoomOutOfBoundBuilding, 
				w => string.IsNullOrWhiteSpace(w.Building) || w.Building.Length > 0 && w.Building.Length <= 20));

			AddSpecification(new ExpressionSpecification<Room>(
				AppConsts.LocalizationSourceName, 
				Room.EntityError.RoomOutOfBoundWing, 
				w => string.IsNullOrWhiteSpace(w.Wing) || w.Wing.Length > 0 && w.Wing.Length <= 20));

			AddSpecification(new ExpressionSpecification<Room>(
				AppConsts.LocalizationSourceName, 
				Room.EntityError.RoomOutOfBoundFloor, 
				w => string.IsNullOrWhiteSpace(w.Floor) || w.Floor.Length > 0 && w.Floor.Length <= 20));

			AddSpecification(new ExpressionSpecification<Room>(
				AppConsts.LocalizationSourceName, 
				Room.EntityError.RoomMustHaveRoomNumber, 
				w => !string.IsNullOrWhiteSpace(w.RoomNumber)));

			AddSpecification(new ExpressionSpecification<Room>(
				AppConsts.LocalizationSourceName, 
				Room.EntityError.RoomOutOfBoundRoomNumber, 
				w => string.IsNullOrWhiteSpace(w.RoomNumber) || w.RoomNumber.Length > 0 && w.RoomNumber.Length <= 10));

			AddSpecification(new ExpressionSpecification<Room>(
				AppConsts.LocalizationSourceName, 
				Room.EntityError.RoomOutOfBoundRemarks, 
				w => string.IsNullOrWhiteSpace(w.Remarks) || w.Remarks.Length > 0 && w.Remarks.Length <= 4000));

		}
	}
}
}
