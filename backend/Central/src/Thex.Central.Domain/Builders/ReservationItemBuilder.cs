﻿using Tnf.Builder;
using Tnf.Notifications;
using Thex.Common.Enumerations;
using Thex.Common;

namespace Thex.Central.Domain.Entities
{
    public partial class ReservationItem
    {
        public class Builder : Builder<ReservationItem>
        {
            ReservationItem _oldReservationItem;
            int _propertyId;

            protected virtual void NotifyInvalidConstructor()
            {
                Notification.Raise(Notification
                    .DefaultBuilder
                    .WithMessage(AppConsts.LocalizationSourceName, CommonsEnum.Error.InvalidConstructor)
                    .Build());
            }

            public Builder(INotificationHandler handler) : base(handler)
            {
                NotifyInvalidConstructor();
            }

            public Builder(INotificationHandler handler, ReservationItem instance) : base(handler, instance)
            {
                NotifyInvalidConstructor();
            }
        }
    }
}
