﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace Thex.Central.Domain.Extensions
{
    public static class InfoValidationExtension
    {
        private const string EmailRegex = @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z";
        public static bool IsEmail(this string email)
        {
            return Regex.IsMatch(email, EmailRegex, RegexOptions.IgnoreCase);
        }
        public static bool IsWebSite(this string webSite)
        {
            return Uri.IsWellFormedUriString(webSite, UriKind.RelativeOrAbsolute);
        }

        public static bool IsBetween(this DateTime datetime, DateTime initialDate, DateTime finalDate)
        {
            return (datetime.CompareTo(initialDate) >= 0) && (datetime.CompareTo(finalDate) <= 0);
        }


    }
}
