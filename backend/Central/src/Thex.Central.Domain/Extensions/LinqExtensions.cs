﻿//  <copyright file="LinqExtensions.cs" company="TOTVS">
//  Copyright (c) TOTVS. All rights reserved.
//  </copyright>
using System;

namespace Thex.Central.Domain.Extensions
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;

    public static class LinqExtensions
    {
        public static IOrderedQueryable<TSource> OrderBy<TSource, TKey>(
            this IQueryable<TSource> source,
            Expression<Func<TSource, TKey>> keySelector,
            bool descending) =>
            descending ? source.OrderByDescending(keySelector) : source.OrderBy(keySelector);

        public static IOrderedQueryable<TSource> OrderBy<TSource, TKey>(
            this IQueryable<TSource> source,
            Expression<Func<TSource, TKey>> keySelector,
            IComparer<TKey> comparer,
            bool descending) => descending
                                    ? source.OrderByDescending(keySelector, comparer)
                                    : source.OrderBy(keySelector, comparer);

        public static IOrderedQueryable<TSource> ThenBy<TSource, TKey>(
            this IOrderedQueryable<TSource> source,
            Expression<Func<TSource, TKey>> keySelector,
            bool descending) =>
            descending ? source.ThenByDescending(keySelector) : source.ThenBy(keySelector);

        public static IOrderedQueryable<TSource> ThenBy<TSource, TKey>(
            this IOrderedQueryable<TSource> source,
            Expression<Func<TSource, TKey>> keySelector,
            IComparer<TKey> comparer,
            bool descending) => descending
                                    ? source.ThenByDescending(keySelector, comparer)
                                    : source.ThenBy(keySelector, comparer);

        public static IOrderedQueryable<TSource> OrderByDynamic<TSource, TKey>(
            this IQueryable<TSource> source,
            Expression<Func<TSource, TKey>> keySelector,
            bool descending) => source.Expression.Type == typeof(IOrderedQueryable<TSource>)
                                    ? ((IOrderedQueryable<TSource>)source).ThenBy(keySelector, descending)
                                    : source.OrderBy(keySelector, descending);
    }
}