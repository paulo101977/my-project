﻿// //  <copyright file="EnumerableExtensions.cs" company="TOTVS">
// //  Copyright (c) TOTVS. All rights reserved.
// //  </copyright>
namespace Thex.Central.Domain.Extensions
{
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// A<see cref="EnumerableExtensions"/>
    /// </summary>
    public static class EnumerableExtensions
    {
        /// <summary>
        /// Determines whether the collection contains elements.
        /// </summary>
        /// <typeparam name="T">The IEnumerable type.</typeparam>
        /// <param name="enumerable">The enumerable.</param>
        /// <returns><c>true</c> if the IEnumerable contains element; otherwise, <c>false</c>.</returns>
        public static bool ContainsElement<T>(this IEnumerable<T> enumerable)
        {
            if (enumerable == null)
                return false;

            return enumerable.Any();
        }

        /// <summary>
        /// Determines whether the collection is null or contains no elements.
        /// </summary>
        /// <typeparam name="T">The IEnumerable type.</typeparam>
        /// <param name="enumerable">The enumerable.</param>
        /// <returns><c>true</c> if the IEnumerable is null or empty; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsNullOrEmpty<T>(this IEnumerable<T> enumerable)
        {
            if (enumerable == null)
                return true;

            if (enumerable is ICollection<T> collection)
                return collection.Count < 1;

            return enumerable.Any();
        }
    }
}