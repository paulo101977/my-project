﻿using Microsoft.Extensions.DependencyInjection;
using Thex.Central.Domain.Interfaces.Services;
using Thex.Central.Domain.Services;
using Thex.Common;

namespace Thex.Central.Domain
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddDomainDependency(this IServiceCollection services)
        {
            // Adiciona as dependencias para utilização dos serviços de crud generico do Tnf
            services
                .AddTnfDomain()
                .AddCommonDependency();

            // Para habilitar as convenções do Tnf para Injeção de dependência (ITransientDependency, IScopedDependency, ISingletonDependency)
            // descomente a linha abaixo:
            services.AddTnfDefaultConventionalRegistrations();

            // Registro dos serviços
            services.AddScoped<IReservationDomainService, ReservationDomainService>();
            services.AddScoped<IRoomDomainService, RoomDomainService>();
            services.AddScoped<IRoomTypeDomainService, RoomTypeDomainService>();

            return services;
        }
    }
}
