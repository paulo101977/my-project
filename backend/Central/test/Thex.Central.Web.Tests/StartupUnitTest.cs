﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using System;
using System.IO;
using Thex.Common;
using Thex.Kernel;
using Thex.AspNetCore.Security;
using Thex.AspNetCore.Security.Interfaces;
using Tnf.Configuration;
using Thex.AspNetCore.Filters;
using Thex.GenericLog;
using Thex.Central.Application;
using Thex.Central.Infra.Context;
using Thex.Central.Web.Tests.Context;
using Thex.Central.Infra;
using Thex.Central.Domain;
using System.Configuration;
using Thex.Central.Web.Tests.Mocks.GenericLogEventBuilder;
using Thex.Central.Web.Tests.Mocks.GenericLogHandler;
using Thex.Central.Domain.Entities;
using Thex.Central.Dto;
using System.Collections.Generic;
using Thex.Central.Web.Tests.Middlewares;
using Thex.Central.Web.Tests.Mocks;

namespace Thex.Central.Web.Tests
{
    public class StartupUnitTest
    {
        private IConfiguration Configuration { get; set; }

        public StartupUnitTest(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            var builder = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.json");

            Configuration = builder.Build();

            ITokenConfiguration tokenConfiguration = new TokenConfiguration();
            new ConfigureFromConfigurationOptions<ITokenConfiguration>(
                Configuration.GetSection("TokenConfiguration"))
                    .Configure(tokenConfiguration);
            services.AddSingleton(tokenConfiguration);

            // Configura o setup de teste para AspNetCore
            services
                     .AddApplicationServiceDependency()
                     .AddTnfAspNetCoreSetupTest()
                     .AddTnfEfCoreSqliteInMemory()
                     .RegisterDbContextToSqliteInMemory<ThexContext, FakeThexContext>();  // Configura o cotexto a ser usado em memória pelo EntityFrameworkCore       

            services.AddInfraDependency();
            services.AddDomainDependency();

            services.AddSingleton(Configuration.GetSection("ServicesConfiguration").Get<ServicesConfiguration>());
            services.AddScoped<ActionLogFilter>();

            // Registro dos serviços de Mock
            services.AddTransient<IApplicationUser, ApplicationUserMock>();

            services.AddTransient<ITokenManager, TokenManager>();
            services.AddTransient<ISignConfiguration, SignConfiguration>();
            services.AddScoped<IApplicationUser>(a => new ApplicationUserMock());

            services.AddScoped<IGenericLogEventBuilder, GenericLogEventBuilderMock>();
            services.AddScoped<IGenericLogHandler, GenericLogHandlerMock>();

            return services.BuildServiceProvider();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app)
        {
            app.UseMiddleware<AttachTokenTest>();
            // Configura o uso do teste
            app.UseTnfAspNetCoreSetupTest(options =>
            {
                options.Repository(repositoryConfig =>
                {
                    repositoryConfig.Entity<IEntityGuid>(entity =>
                        entity.RequestDto<IDefaultGuidRequestDto>((e, d) => e.Id == d.Id));

                    repositoryConfig.Entity<IEntityInt>(entity =>
                        entity.RequestDto<IDefaultIntRequestDto>((e, d) => e.Id == d.Id));

                    repositoryConfig.Entity<IEntityLong>(entity =>
                        entity.RequestDto<IDefaultLongRequestDto>((e, d) => e.Id == d.Id));

                });
            });

            // Habilita o uso do UnitOfWork em todo o request
            app.UseTnfUnitOfWork();

            app.UseMvc(routes =>
            {
                routes.MapRoute("default", "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
