﻿using System;
using Thex.GenericLog;

namespace Thex.Central.Web.Tests.Mocks.GenericLogEvent
{
    public class GenericLogEventMock : IGenericLogEvent
    {
        public Guid Guid => throw new NotImplementedException();

        public GenericLogType GenericLogType => throw new NotImplementedException();

        public string Code { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

        public string Message => throw new NotImplementedException();

        public string DetailedMessage => throw new NotImplementedException();

        public ResultStatus Status => throw new NotImplementedException();

        public bool Equals(IGenericLogEvent other)
        {
            throw new NotImplementedException();
        }

        public bool Equals(IGenericLogEvent x, IGenericLogEvent y)
        {
            throw new NotImplementedException();
        }

        public int GetHashCode(IGenericLogEvent obj)
        {
            throw new NotImplementedException();
        }
    }
}
