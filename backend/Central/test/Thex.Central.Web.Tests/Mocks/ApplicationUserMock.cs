﻿using System;
using System.Collections.Generic;
using System.Text;
using Thex.Kernel;

namespace Thex.Central.Web.Tests.Mocks
{
    public class ApplicationUserMock : IApplicationUser
    {
        public string Name => "Teste";

        public string UserEmail => "teste@email.com";

        public Guid UserUid => new Guid("215bc8a0-07db-4428-9955-fa53b58b4ded");

        public string UserName => "Teste";

        public Guid TenantId => new Guid("9bece27c-cf67-4ea3-8ae8-357b3080475d");

        public string PropertyId => "1";

        public string ChainId => "1";

        public bool IsAdmin => false;

        public string PreferredLanguage => "pt-br";

        public string PreferredCulture => "pt-br";

        public string PropertyLanguage => "pt-br";

        public string PropertyCulture => "pt-br";

        public string TimeZoneName => "America/Sao_Paulo";

        public string PropertyCountryCode => "BR";

        public string PropertyDistrictCode => "RJ";

        string IApplicationUser.UserName { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

        public Dictionary<string, string> GetClaimsIdentity()
        {
            var claims = new Dictionary<string, string>();
            claims.Add("admin", "true");
            return claims;
        }

        public bool IsAuthenticated()
        {
            return true;
        }

        public void SetProperties(string userUid, string tenantId, string propertyId, string timeZoneName)
        {
            throw new NotImplementedException();
        }
    }
}
