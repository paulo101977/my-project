﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using System;
using System.Linq;
using Thex.Central.Infra.Context;
using Thex.Central.Web.Tests.Mocks;
using Tnf.Runtime.Session;
using Thex.Common.Extensions;

namespace Thex.Central.Web.Tests.Context
{
    public class FakeThexContext : ThexContext
    {
        //public FakeThexContext(IApplicationUser applicationUser, IConfiguration configuration, DbContextOptions<ThexContext> options, ITnfSession session)
        //    : base(applicationUser, configuration, options, session)
        //{
        //}

        public FakeThexContext(DbContextOptions<ThexContext> options, ITnfSession session)
            : base(new ApplicationUserMock(), null, options, session, null)
        {
        }

        public override void FillAuditFiedls()
        {
            // custom code...
            var utcNowAuditDate = DateTime.UtcNow.ToZonedDateTimeLoggedUser(_applicationUser);


            var entriesWithBase = ChangeTracker.Entries();

            var FullAuditedEntries = entriesWithBase
                .Where(entry => (entry.Metadata.ClrType.BaseType != null && entry.Metadata.ClrType.BaseType.Name.Contains("ThexFullAuditedEntity")) ||
                                (entry.Metadata.ClrType.BaseType.BaseType != null && entry.Metadata.ClrType.BaseType.BaseType.Name.Contains("ThexFullAuditedEntity")));

            if (FullAuditedEntries != null)
                foreach (EntityEntry entityEntry in FullAuditedEntries)
                {
                    var entity = entityEntry.Entity;
                    switch (entityEntry.State)
                    {
                        case EntityState.Added:
                            entity.GetType().GetProperty("CreationTime").SetValue(entity, utcNowAuditDate, null);
                            entity.GetType().GetProperty("LastModificationTime").SetValue(entity, utcNowAuditDate, null);
                            entity.GetType().GetProperty("CreatorUserId").SetValue(entity, _userUid, null);
                            break;
                        case EntityState.Modified:
                            entityEntry.Property("CreationTime").IsModified = false;
                            entityEntry.Property("CreatorUserId").IsModified = false;

                            entity.GetType().GetProperty("LastModificationTime").SetValue(entity, utcNowAuditDate, null);
                            entity.GetType().GetProperty("LastModifierUserId").SetValue(entity, _userUid, null);
                            break;
                        case EntityState.Deleted:
                            entityEntry.Property("CreationTime").IsModified = false;
                            entityEntry.Property("CreatorUserId").IsModified = false;
                            entityEntry.Property("LastModificationTime").IsModified = false;
                            entityEntry.Property("LastModifierUserId").IsModified = false;

                            entity.GetType().GetProperty("DeletionTime").SetValue(entity, utcNowAuditDate, null);
                            entity.GetType().GetProperty("DeleterUserId").SetValue(entity, _userUid, null);
                            entity.GetType().GetProperty("IsDeleted").SetValue(entity, true);
                            entityEntry.State = EntityState.Modified;
                            break;
                    }
                }


            var MultiTentantEntries = entriesWithBase
                .Where(entry => (entry.Metadata.ClrType.BaseType != null && entry.Metadata.ClrType.BaseType.Name.Contains("ThexMultiTenantFullAuditedEntity")) ||
                                (entry.Metadata.ClrType.BaseType.BaseType != null && entry.Metadata.ClrType.BaseType.BaseType.Name.Contains("ThexMultiTenantFullAuditedEntity")));

            if (MultiTentantEntries != null)
                foreach (EntityEntry entityEntry in MultiTentantEntries)
                {
                    var entity = entityEntry.Entity;
                    switch (entityEntry.State)
                    {
                        case EntityState.Added:
                            if (!entity.GetType().ToString().Contains("UserPassword"))
                                entity.GetType().GetProperty("TenantId").SetValue(entity, _tenantId, null);
                            break;
                        case EntityState.Modified:
                        case EntityState.Deleted:
                            entityEntry.Property("TenantId").IsModified = false;
                            break;
                    }
                }
        }
    }
}
