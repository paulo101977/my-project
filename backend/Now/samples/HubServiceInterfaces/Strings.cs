namespace HubServiceInterfaces
{
    public static class Strings
    {
        public static string HubUrl => "http://localhost:65351/hubs/clock";

        public static class Events
        {
            public static string TimeSent => nameof(IClock.ShowTime);
        }
    }
}