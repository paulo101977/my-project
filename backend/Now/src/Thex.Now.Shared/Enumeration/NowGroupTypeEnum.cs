﻿namespace Thex.Now.Shared.Enumeration
{
    public enum NowGroupTypeEnum
    {
        Message = 1,
        Notification = 2,
        Data = 3
    }
}
