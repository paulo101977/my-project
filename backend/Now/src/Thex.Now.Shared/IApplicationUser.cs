﻿namespace Thex.Now.Shared
{
    public interface IApplicationUser
    {
        string ProjectName { get; }
        string PublicKey { get; }
        string PrivateKey { get; }
        string ProjectId { get; }
        string ClientId { get; }

        bool IsInRole(string role);
        bool IsAuthenticated();
    }
}