﻿namespace Thex.Now.Shared
{
    public class RouteConsts
    {
        public const string Group = "api/group";
        public const string Project = "api/project";
        public const string Client = "api/client";
        public const string Hub = "api/now";
        
    }
}
