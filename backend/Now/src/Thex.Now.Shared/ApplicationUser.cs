﻿using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.Security.Claims;

namespace Thex.Now.Shared
{
    public class ApplicationUser : IApplicationUser
    {
        private readonly IHttpContextAccessor _accessor;
        private Dictionary<string, string> _claims;

        private const string PUBLICKEYFIELD = "PublicKey";
        private const string PRIVATEKEYFIELD = "PrivateKey";
        private const string PROJECTIDFIELD = "ProjectId";
        private const string CLIENTIDFIELD = "ClientId";
        private const string ROLEFIELD = ClaimTypes.Role;
        

        public ApplicationUser(IHttpContextAccessor accessor)
        {
            _accessor = accessor;
            _claims = null;
        }

        public string ProjectName => _accessor.HttpContext.User.Identity.Name;

        public string PublicKey
        {
            get
            {
                if (_claims == null)
                    _claims = GetClaimsIdentity();
                if (_claims.ContainsKey(PUBLICKEYFIELD))
                    return _claims[PUBLICKEYFIELD];
                else return null;
            }
        }

        public string PrivateKey
        {
            get
            {
                if (_claims == null)
                    _claims = GetClaimsIdentity();
                if (_claims.ContainsKey(PRIVATEKEYFIELD))
                    return _claims[PRIVATEKEYFIELD];
                else return null;
            }
        }

        public string ProjectId
        {
            get
            {
                if (_claims == null)
                    _claims = GetClaimsIdentity();
                if (_claims.ContainsKey(PROJECTIDFIELD))
                    return _claims[PROJECTIDFIELD];
                else return null;
            }
        }

        public string ClientId
        {
            get
            {
                if (_claims == null)
                    _claims = GetClaimsIdentity();
                if (_claims.ContainsKey(CLIENTIDFIELD))
                    return _claims[CLIENTIDFIELD];
                else return null;
            }
        }

        public bool IsInRole(string role)
            => _accessor.HttpContext.User.IsInRole(role);

        public bool IsAuthenticated()
            => _accessor.HttpContext.User.Identity.IsAuthenticated;

        private Dictionary<string, string> GetClaimsIdentity()
        {
            Dictionary<string, string> result = new Dictionary<string, string>();

            foreach (var claim in _accessor.HttpContext.User.Claims)
                result.AddIfNotContains(new KeyValuePair<string, string>(claim.Type, claim.Value));

            return result;
        }
    }
}