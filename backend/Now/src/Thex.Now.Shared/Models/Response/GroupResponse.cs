﻿using System;

namespace Thex.Now.Shared.Models.Response
{
    public class GroupResponse
    {
        public Guid GroupId { get; set; }
        public string GroupName { get; set; }
        public string PublicName { get; set; }
    }
}
