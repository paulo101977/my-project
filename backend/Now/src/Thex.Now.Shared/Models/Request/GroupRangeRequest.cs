﻿using Thex.Now.Shared.Enumeration;
using System.Collections.Generic;

namespace Thex.Now.Shared.Models.Request
{
    public class CreateGroupRangeRequest
    {
        public string ProjectId { get; set; }
        public string ClientId { get; set; }
        public List<GroupRangeRequest> GroupList { get; set; }
    }

    public class GroupRangeRequest
    {
        public string PublicName { get; set; }
        public NowGroupTypeEnum GroupType { get; set; }
    }
}
