﻿using Thex.Now.Shared.Enumeration;

namespace Thex.Now.Shared.Models.Request
{
    public class CreateGroupRequest
    {
        public string ProjectId { get; set; }
        public string ClientId { get; set; }
        public string PublicName { get; set; }
        public NowGroupTypeEnum GroupType { get; set; }
    }
}
