﻿namespace Thex.Now.Shared.Models.Request
{
    public class CreateClientRequest 
    {
        public string ProjectId { get; set; }
        public string OwnerName { get; set; }
    }
}
