﻿namespace System.Collections.Generic
{
    public static class EnumerableExtensions
    {
        public static bool AddIfNotContains<T>(this ICollection<T> source, T item)
        {
            if (source.Contains(item))
                return false;

            source.Add(item);
            return true;
        }
    }
}
