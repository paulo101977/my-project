﻿using Tnf.Application.Services;
using Tnf.Notifications;

namespace Thex.Now.Application
{
    public abstract class ApplicationServiceBase: ApplicationService
    {
        protected ApplicationServiceBase(INotificationHandler notification) : base(notification)
        {
        }
    }
}
