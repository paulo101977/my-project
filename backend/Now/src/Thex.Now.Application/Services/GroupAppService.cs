﻿using MediatR;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.Now.Application.Interfaces;
using Thex.Now.Domain;
using Thex.Now.Shared;
using Thex.Now.Shared.Models.Response;
using Tnf.Notifications;

namespace Thex.Now.Application
{
    public class GroupAppService : ApplicationServiceBase, IGroupAppService
    {
        private readonly INotificationHandler _notification;
        private readonly IMediator _mediator;
        private readonly IApplicationUser _applicationUser;

        public GroupAppService(
           INotificationHandler notification,
           IApplicationUser applicationUser,
            IMediator mediator) : base(notification)
        {
            _notification = notification;
            _mediator = mediator;
            _applicationUser = applicationUser;
        }

        public async Task<GroupResponse> CreateAsync(CreateGroupCommand command)
        {
            var response = await _mediator.Send(command);

            if (Notification.HasNotification())
                return null;

            return response;
        }

        public async Task<List<GroupResponse>> CreateRangeAsync(CreateGroupRangeCommand command)
        {
            var response = await _mediator.Send(command);

            if (Notification.HasNotification())
                return null;

            return response;
        }
    }
}
