﻿using MediatR;
using System;
using System.Linq;
using System.Threading.Tasks;
using Thex.Now.Application.Interfaces;
using Thex.Now.Client;
using Thex.Now.Domain;
using Thex.Now.Domain.Interfaces.ReadRepositories;
using Tnf.Notifications;

namespace Thex.Now.Application
{
    public class ProjectAppService : ApplicationServiceBase, IProjectAppService
    {
        private readonly INotificationHandler _notification;
        private readonly IMediator _mediator;

        private readonly INowClient _nowClient;
        private readonly IProjectReadRepository _projectReadRepository;

        public ProjectAppService(
           INotificationHandler notification,
           INowClient nowClient,
           IProjectReadRepository projectReadRepository,
            IMediator mediator) : base(notification)
        {
            _notification = notification;
            _mediator = mediator;
            _nowClient = nowClient;
            _projectReadRepository = projectReadRepository;
        }

        public async Task<CreateProjectCommand> CreateAsync(CreateProjectCommand command)
        {
            var response = await _mediator.Send(command);

            if (Notification.HasNotification())
                return null;

            return response;
        }

        public async Task<UpdateProjectNameCommand> UpdateProjectNameAsync(UpdateProjectNameCommand command)
        {
            var response = await _mediator.Send(command);

            if (Notification.HasNotification())
                return null;

            return response;
        }

        public async Task<RefreshKeyCommand> RefreshKeyAsync(RefreshKeyCommand command)
        {
            var response = await _mediator.Send(command);

            if (Notification.HasNotification())
                return null;

            return response;
        }

        public async Task<bool> AnyByPublicKeyAsync(string apiKey)
            => await _projectReadRepository.AnyByPublicKeyAsync(apiKey);

        public async Task<bool> AnyByApiKeyAndGroupNameAsync(string apiKey, string groupName)
        {
            var clientId = groupName.Split(new[] { "__" }, StringSplitOptions.None).FirstOrDefault();
            return await _projectReadRepository.AnyByFiltersAsync(apiKey, clientId, groupName);
        }
    }
}
