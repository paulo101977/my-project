﻿using MediatR;
using System.Threading.Tasks;
using Thex.Now.Application.Interfaces;
using Thex.Now.Domain;
using Tnf.Notifications;

namespace Thex.Now.Application
{
    public class ClientAppService : ApplicationServiceBase, IClientAppService
    {
        private readonly INotificationHandler _notification;
        private readonly IMediator _mediator;

        public ClientAppService(
           INotificationHandler notification,
            IMediator mediator) : base(notification)
        {
            _notification = notification;
            _mediator = mediator;
        }

        public async Task<CreateOrUpdateClientCommand> CreateOrUpdateAsync(CreateOrUpdateClientCommand command)
        {
            var response = await _mediator.Send(command);

            if (Notification.HasNotification())
                return null;

            return response;
        }
    }
}
