﻿using MediatR;
using Microsoft.Extensions.Configuration;
using System;
using System.Threading.Tasks;
using Thex.Now.Application.Interfaces;
using Thex.Now.Client;
using Tnf.Notifications;

namespace Thex.Now.Application
{
    public class TestAppService : ApplicationServiceBase, ITestAppService
    {
        private readonly INotificationHandler _notification;
        private readonly INowClient _nowClient;
        private readonly IConfiguration _configuration;

        public TestAppService(
           INotificationHandler notification,
           INowClient nowClient,
           IConfiguration configuration,
            IMediator mediator) : base(notification)
        {
            _configuration = configuration;
            _notification = notification;
            _nowClient = nowClient;
        }

        public async Task TestDataAsync()
        {
            string publicKey = _configuration["NowClient:PublicKey"];
            string privateKey = _configuration["NowClient:PrivateKey"];
            
            var rnd = new Random();

            await _nowClient.SendDataAsync($"{publicKey}__SampleData", "SampleData", "backend api", rnd.NextDouble());
        }

        public async Task TestNotificationAsync()
        {
            string publicKey = _configuration["NowClient:PublicKey"];
            string privateKey = _configuration["NowClient:PrivateKey"];

            await _nowClient.CreateGroupAsync(new Thex.Now.Client.Models.Request.CreateGroupRequest
            {
                ClientId = publicKey,
                GroupType = Thex.Now.Client.Enumeration.NowGroupTypeEnum.Notification,
                ProjectId = publicKey,
                PublicName = "Sample Create"
            });

            await _nowClient.SendMessageAsync($"{publicKey}__SampleNotification", "SampleNotification", "backend api", $"mensagem {Guid.NewGuid()}");
        }
    }
}
