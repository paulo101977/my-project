﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.Now.Domain;
using Thex.Now.Shared.Models.Response;
using Tnf.Application.Services;

namespace Thex.Now.Application.Interfaces
{
    public interface IGroupAppService : IApplicationService
    {
        Task<GroupResponse> CreateAsync(CreateGroupCommand command);
        Task<List<GroupResponse>> CreateRangeAsync(CreateGroupRangeCommand command);
    }
}
