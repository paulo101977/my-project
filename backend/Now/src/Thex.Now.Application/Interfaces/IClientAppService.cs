﻿using System.Threading.Tasks;
using Thex.Now.Domain;
using Tnf.Application.Services;

namespace Thex.Now.Application.Interfaces
{
    public interface IClientAppService : IApplicationService
    {
        Task<CreateOrUpdateClientCommand> CreateOrUpdateAsync(CreateOrUpdateClientCommand command);
    }
}
