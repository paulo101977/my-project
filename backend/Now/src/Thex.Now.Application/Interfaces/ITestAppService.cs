﻿using System.Threading.Tasks;
using Tnf.Application.Services;

namespace Thex.Now.Application.Interfaces
{
    public interface ITestAppService : IApplicationService
    {
        Task TestNotificationAsync();
        Task TestDataAsync();
    }
}
