﻿using System.Threading.Tasks;
using Thex.Now.Domain;
using Tnf.Application.Services;

namespace Thex.Now.Application.Interfaces
{
    public interface IProjectAppService : IApplicationService
    {
        Task<CreateProjectCommand> CreateAsync(CreateProjectCommand command);
        Task<UpdateProjectNameCommand> UpdateProjectNameAsync(UpdateProjectNameCommand command);
        Task<RefreshKeyCommand> RefreshKeyAsync(RefreshKeyCommand command);
        Task<bool> AnyByPublicKeyAsync(string apiKey);
        Task<bool> AnyByApiKeyAndGroupNameAsync(string apiKey, string groupName);
    }
}
