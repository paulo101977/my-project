﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Thex.Now.Application.Interfaces;

namespace Thex.Now.Application
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddNewApplicationServiceDependency(this IServiceCollection services, IConfiguration configuration)
        {
            services
                .AddNewDomainDependency(configuration["ConnectionStrings:MongoDb:New:MongoDBServer"], configuration["ConnectionStrings:MongoDb:New:MongoDBName"])
                .AddNowService(configuration);

            services.AddScoped<IProjectAppService, ProjectAppService>();
            services.AddScoped<IGroupAppService, GroupAppService>();
            services.AddScoped<IClientAppService, ClientAppService>();
            
            services.AddScoped<ITestAppService, TestAppService>();

            return services;
        }
    }
}
