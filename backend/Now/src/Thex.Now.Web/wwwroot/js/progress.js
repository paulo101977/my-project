﻿"use strict";

var apiKey = "c2c9f081-cfa0-43e9-88ad-9cd41defd518";
var connection = new signalR.HubConnectionBuilder().withUrl("/api/now?ApiKey=" + apiKey).build();
var group = "c2c9f081-cfa0-43e9-88ad-9cd41defd518__SampleProgress";


connection.on('taskStarted', data => {
    console.log('task started');
});

connection.on('taskProgressChanged', data => {
    console.log(data);
    var element = document.getElementById("sampleProgress");
    element.textContent = data;
    element.style.width = data + "%";
    element.setAttribute("aria-valuenow", data);
});

connection.on('taskEnded', data => {
    console.log('task ended');
});

connection.start().then(function () {

    connection.invoke("AddToGroupAsync", group).catch(function (err) {
        return console.error(err.toString());
    });

}).catch(function (err) {
    return console.error(err.toString());
});


$("#startButton").on("click", function () {

    $.get("/api/test/lengthy");

    return false;
});