﻿"use strict";

var apiKey = "c2c9f081-cfa0-43e9-88ad-9cd41defd518";
var connection = new signalR.HubConnectionBuilder().withUrl("/api/now?ApiKey=" + apiKey).build();
var group = "c2c9f081-cfa0-43e9-88ad-9cd41defd518__SampleData";

var chart = Highcharts.chart('container', {
    chart: {
        type: 'spline',
        animation: Highcharts.svg, // don't animate in old IE
        marginRight: 10
    },
    time: {
        useUTC: false
    },

    title: {
        text: 'Live random data'
    },
    xAxis: {
        type: 'datetime',
        tickPixelInterval: 150
    },
    yAxis: {
        title: {
            text: 'Value'
        },
        plotLines: [{
            value: 0,
            width: 1,
            color: '#808080'
        }]
    },
    tooltip: {
        headerFormat: '<b>{series.name}</b><br/>',
        pointFormat: '{point.x:%Y-%m-%d %H:%M:%S}<br/>{point.y:.2f}'
    },
    legend: {
        enabled: false
    },
    exporting: {
        enabled: false
    },
    series: [{
        name: 'Random data',
        data: (function () {
            var data = [],
                time = (new Date()).getTime(),
                i;

            for (i = -19; i <= 0; i += 1) {
                data.push({
                    x: time + i * 1000,
                    y: Math.random()
                });
            }
            return data;
        }())
    }]
});


connection.on("SampleData", function (user, message) {
    console.log(user);

    var series = chart.series[0];
    var x = (new Date()).getTime(),
        y = message;
    series.addPoint([x, y], true, true);
});

connection.start().then(function () {

    connection.invoke("AddToGroupAsync", group).catch(function (err) {
        return console.error(err.toString());
    });

}).catch(function (err) {
    return console.error(err.toString());
});
