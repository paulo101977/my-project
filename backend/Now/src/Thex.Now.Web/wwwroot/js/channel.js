﻿"use strict";

var apiKey = "c2c9f081-cfa0-43e9-88ad-9cd41defd518";
var connection = new signalR.HubConnectionBuilder().withUrl("/api/now?ApiKey=" + apiKey).build();
var group = apiKey + "__SampleNotification";
var method = "SampleNotification";

document.getElementById("sendButton").disabled = true;

connection.on(method, function (user, message) {
    var msg = message.replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;");
    var encodedMsg = user + " says " + msg;
    var li = document.createElement("li");
    li.textContent = encodedMsg;
    document.getElementById("messagesList").appendChild(li);
});

connection.start().then(function () {

    connection.invoke("AddToGroupAsync", group).catch(function (err) {
        return console.error(err.toString());
    });

    document.getElementById("sendButton").disabled = false;

}).catch(function (err) {
    document.getElementById("sendButton").disabled = true;
    return console.error(err.toString());
});


$("#sendButton").on("click", function () {

    var user = document.getElementById("userInput").value;
    var message = document.getElementById("messageInput").value;
    connection.invoke("SendMessageAsync", apiKey, group, method, user, message).catch(function (err) {
        return console.error(err.toString());
    });
    event.preventDefault();
});

//"use strict";

//var apiKey = "c2c9f081-cfa0-43e9-88ad-9cd41defd518";
//var connection = new signalR.HubConnectionBuilder().withUrl("/api/now?ApiKey=" + apiKey).build();

//document.getElementById("sendButton").disabled = true;

//connection.on("SampleNotification", function (user, message) {
//    var msg = message.replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;");
//    var encodedMsg = user + " says " + msg;
//    var li = document.createElement("li");
//    li.textContent = encodedMsg;
//    document.getElementById("messagesList").appendChild(li);
//});

//connection.start().then(function () {
//    document.getElementById("sendButton").disabled = false;
//}).catch(function (err) {
//    return console.error(err.toString());
//});

//document.getElementById("sendButton").addEventListener("click", function (event) {
//    var group = apiKey + "__" + document.getElementById("group").value;
//    var method = document.getElementById("method").value;
//    var user = document.getElementById("userInput").value;
//    var message = document.getElementById("messageInput").value;
//    connection.invoke("SendMessageAsync", apiKey, group, method, user, message).catch(function (err) {
//        return console.error(err.toString());
//    });
//    event.preventDefault();
//});


//document.getElementById("connGroupButton").addEventListener("click", function (event) {

//    var group = apiKey + "__" + document.getElementById("group").value;

//    connection.invoke("AddToGroupAsync", group).catch(function (err) {
//        return console.error(err.toString());
//    });

//    alert("connected");

//    event.preventDefault();
//});
