﻿using Microsoft.AspNetCore.Builder;
using Thex.Now.Web.Middleware;

namespace Thex.Now.Web.Extensions
{
    public static class CorsMiddlewareExtensions
    {
        public static IApplicationBuilder UseCorsMiddleware(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<CorsMiddleware>();
        }
    }
}
