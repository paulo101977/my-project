﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Thex.Now.Application.Interfaces;
using Thex.Now.Domain;
using Thex.Now.Shared;
using Tnf.AspNetCore.Mvc.Response;

namespace Thex.Now.Web.Controllers.API
{
    [Route(RouteConsts.Client)]
    [ApiController]
    public class ClientController : TnfController
    {
        private readonly IClientAppService _clientAppService;

        public ClientController(IClientAppService clientAppService)
        {
            _clientAppService = clientAppService;
        }

        [HttpPost]
        [Authorize]
        [ProducesResponseType(typeof(CreateOrUpdateClientCommand), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> PostAsync([FromBody] CreateOrUpdateClientCommand command)
        {
            var response = await _clientAppService.CreateOrUpdateAsync(command);

            return CreateResponseOnPost(response, RouteResponseConsts.Project);
        }
    }
}