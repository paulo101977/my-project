﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Thex.Now.Application.Interfaces;
using Thex.Now.Domain;
using Thex.Now.Shared;
using Thex.Now.Shared.Models.Response;
using Tnf.AspNetCore.Mvc.Response;

namespace Thex.Now.Web.Controllers.API
{
    [Route(RouteConsts.Group)]
    [ApiController]
    public class GroupController : TnfController
    {
        private readonly IGroupAppService _groupAppService;

        public GroupController(IGroupAppService groupAppService)
        {
            _groupAppService = groupAppService;
        }

        [HttpPost]
        [Authorize]
        [ProducesResponseType(typeof(GroupResponse), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> PostAsync([FromBody] CreateGroupCommand command)
        {
            var response = await _groupAppService.CreateAsync(command);

            return CreateResponseOnPost(response, RouteResponseConsts.Group);
        }

        [HttpPost("range")]
        [Authorize]
        [ProducesResponseType(typeof(List<GroupResponse>), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> PostRangeAsync([FromBody] CreateGroupRangeCommand command)
        {
            var response = await _groupAppService.CreateRangeAsync(command);

            return CreateResponseOnPost(response, RouteResponseConsts.Group);
        }
    }
}