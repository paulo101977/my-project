﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using TestSocketWeb;
using Thex.Now.Application.Interfaces;
using Tnf.AspNetCore.Mvc.Response;

namespace Thex.Now.Web.Controllers.API
{
    [Route("api/test")]
    [ApiController]
    public class TestController : TnfController
    {
        private readonly ITestAppService _testAppService;
        private readonly IHubContext<NowHub> _hubContext;

        public TestController(
            ITestAppService testAppService, 
            IHubContext<NowHub> hubContext)
        {
            _testAppService = testAppService;
            _hubContext = hubContext;
        }

        [HttpGet("notification")]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> GetNotification()
        {
            await _testAppService.TestNotificationAsync();

            return CreateResponseOnGet(null, "test");
        }

        [HttpGet("data")]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> GetData()
        {
            await _testAppService.TestDataAsync();

            return CreateResponseOnGet(null, "test");
        }

        [HttpGet("lengthy")]
        public async Task<IActionResult> Lengthy()
        {
            var group = "c2c9f081-cfa0-43e9-88ad-9cd41defd518__SampleProgress";

            await _hubContext
                .Clients
                .Group(group)
                .SendAsync("taskStarted");

            for (int i = 0; i < 100; i++)
            {
                System.Threading.Thread.Sleep(100);
                await _hubContext
                    .Clients
                    .Group(group)
                    .SendAsync("taskProgressChanged", i + 1);
            }

            await _hubContext
                .Clients
                .Group(group)
                .SendAsync("taskEnded");

            return Ok();
        }
    }
}