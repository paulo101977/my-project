﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Thex.Now.Application.Interfaces;
using Thex.Now.Core.Authorization;
using Thex.Now.Domain;
using Thex.Now.Shared;
using Tnf.AspNetCore.Mvc.Response;

namespace Thex.Now.Web.Controllers.API
{
    [Route(RouteConsts.Project)]
    [ApiController]
    public class ProjectController : TnfController
    {
        private readonly IProjectAppService _projectAppService;

        public ProjectController(IProjectAppService projectAppService)
        {
            _projectAppService = projectAppService;
        }

        [HttpPost]
        [Authorize(Policy = Policies.OnlyManagers)]
        [ProducesResponseType(typeof(CreateProjectCommand), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Post([FromBody] CreateProjectCommand command)
        {
            var response = await _projectAppService.CreateAsync(command);

            return CreateResponseOnPost(response, RouteResponseConsts.Project);
        }
        
        [HttpPatch("updateName")]
        [Authorize(Policy = Policies.OnlyManagers)]
        [ProducesResponseType(typeof(UpdateProjectNameCommand), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> UpdateNamePatch([FromBody] UpdateProjectNameCommand command)
        {
            var response = await _projectAppService.UpdateProjectNameAsync(command);

            return CreateResponseOnPatch(response, RouteResponseConsts.Project);
        }
        
        [HttpPatch("refreshKey")]
        [Authorize(Policy = Policies.OnlyManagers)]
        [ProducesResponseType(typeof(RefreshKeyCommand), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> RefreshKeyPut([FromBody] RefreshKeyCommand command)
        {
            var response = await _projectAppService.RefreshKeyAsync(command);

            return CreateResponseOnPatch(response, RouteResponseConsts.Project);
        }
    }
}