﻿using Microsoft.AspNetCore.SignalR;
using System.Threading.Tasks;
using Thex.Now.Application.Interfaces;
using Thex.Now.Shared;

namespace TestSocketWeb
{
    public class NowHub : Hub
    {
        private readonly IProjectAppService _projectAppService;

        public NowHub(
            IProjectAppService projectAppService)
        {
            _projectAppService = projectAppService;
        }

        public override async Task OnConnectedAsync()
        {
            var context = Context.GetHttpContext();
            var apiKey = context.Request.Query[NowConsts.QueryStringPublicAPI];

            if (!await _projectAppService.AnyByPublicKeyAsync(apiKey)) { 
                Context.Abort();
                return;
            }

            await base.OnConnectedAsync();
        }

        public async Task SendMessageAsync(string apiKey, string groupName, string method, string senderId, object message)
        {
            if (!await _projectAppService.AnyByApiKeyAndGroupNameAsync(apiKey, groupName))
                return;

            await Clients.Group(groupName).SendAsync(method, senderId, message);
        }

        public async Task SendNotificationAsync(string apiKey, string groupName, string method, string senderId, object notification)
        {
            if (!await _projectAppService.AnyByApiKeyAndGroupNameAsync(apiKey, groupName))
                return;

            await Clients.Group(groupName).SendAsync(method, senderId, notification);
        }

        public async Task SendDataAsync(string apiKey, string groupName, string method, string senderId, object data)
        {
            if (!await _projectAppService.AnyByApiKeyAndGroupNameAsync(apiKey, groupName))
                return;

            await Clients.Group(groupName).SendAsync(method, senderId, data);
        }

        public async Task AddToGroupAsync(string groupName)
        {
            await Groups.AddToGroupAsync(Context.ConnectionId, groupName);
        }

        public async Task RemoveFromGroupAsync(string groupName)
        {
            await Groups.RemoveFromGroupAsync(Context.ConnectionId, groupName);
        }
    }
}
