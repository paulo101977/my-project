﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.ResponseCompression;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System.Net;
using Thex.Now.Application;
using Thex.Now.Web.Extensions;

namespace TestSocketWeb
{
    public class Startup
    {
        public Startup(IConfiguration configuration, IHostingEnvironment environment)
        {
            Configuration = configuration;
            HostingEnvironment = environment;
        }

        public IConfiguration Configuration { get; }
        public IHostingEnvironment HostingEnvironment { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services
                .AddCorsAll("AllowAll")
                .AddTnfAspNetCore()
                .AddResponseCompression(options =>
                {
                    options.Providers.Add<GzipCompressionProvider>();
                    options.EnableForHttps = true;
                });

            services.AddNewApplicationServiceDependency(Configuration);

            services.AddNowAuthentication();

            services.AddNowAuthorization();

            if (HostingEnvironment.IsDevelopment())
            {
                services.AddSignalR();
            }
            else
            {
                services
                    .AddSignalR()
                    .AddAzureSignalR();
            }

            services.AddSwaggerDocumentation();

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseAuthentication();

            app.UseStaticFiles();

            app.UseCorsMiddleware();

            app.UseSwaggerDocumentation();

            app.UseResponseCompression();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });

            if (env.IsDevelopment())
            {
                app.UseSignalR(routes =>
                {
                    routes.MapHub<NowHub>("/api/now");
                });
            }
            else
            {
                app.UseAzureSignalR(routes =>
                {
                    routes.MapHub<NowHub>("/api/now");
                });
            }
        }
    }
}
