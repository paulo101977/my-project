﻿using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Serilog;
using Microsoft.Extensions.DependencyInjection;

namespace TestSocketWeb
{
    public class Program
    {
        public static async Task Main(string[] args)
        {
            var webHost = CreateWebHostBuilder(args).Build();

            await CreateDefaultData(webHost);

            await webHost.RunAsync();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .ConfigureLogging((hostingContext, logging) =>
                {
                    Log.Logger = new LoggerConfiguration()
                        .Enrich.WithMachineName()
                        .ReadFrom.Configuration(hostingContext.Configuration)
                        .CreateLogger();
                })
                .UseSerilog()
                .UseStartup<Startup>();

        private static async Task CreateDefaultData(IWebHost webHost)
        {
            using (var scope = webHost.Services.CreateScope())
            {
                var services = scope.ServiceProvider;
                var domainService = services.GetRequiredService<Thex.Now.Domain.Interfaces.Services.IDefaultProjectService>();

                await domainService.CreateDefaultProject();
            };
        }
    }
}

//4- implementar modo debug
////modo debug signalr local
////modo online vai pra azure
