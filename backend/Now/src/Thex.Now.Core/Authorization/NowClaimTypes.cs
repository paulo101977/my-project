﻿namespace Thex.Now.Core.Authorization
{
    public static class NowClaimTypes
    {
        public const string PublicKey = "PublicKey";
        public const string PrivateKey = "PrivateKey";
        public const string ProjectId = "ProjectId";
        public const string ClientId = "ProjectId";
    }
}
