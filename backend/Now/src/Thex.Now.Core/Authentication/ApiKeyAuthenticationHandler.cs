﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Thex.Now.Core.Authorization;
using Thex.Now.Domain.Interfaces.ReadRepositories;
using Thex.Now.Shared;

namespace Thex.Now.Core.Authentication
{
    public class ApiKeyAuthenticationHandler : AuthenticationHandler<ApiKeyAuthenticationOptions>
    {
        private const string PublicKeyHeaderName = NowConsts.PublicAPI;
        private const string PrivateKeyHeaderName = NowConsts.PrivateAPI;

        private const string ProblemDetailsContentType = "application/problem+json";

        private readonly IProjectReadRepository _projectReadRepository;

        public ApiKeyAuthenticationHandler(
            IOptionsMonitor<ApiKeyAuthenticationOptions> options,
            ILoggerFactory logger,
            UrlEncoder encoder,
            ISystemClock clock,
            IProjectReadRepository projectReadRepository) : base(options, logger, encoder, clock)
        {
            _projectReadRepository = projectReadRepository ?? throw new ArgumentNullException(nameof(projectReadRepository));
        }

        protected override async Task<AuthenticateResult> HandleAuthenticateAsync()
        {
            if (!Request.Headers.TryGetValue(PublicKeyHeaderName, out var publicKeyHeaderValues) ||
                !Request.Headers.TryGetValue(PrivateKeyHeaderName, out var privateKeyHeaderValues))
                return AuthenticateResult.NoResult();

             var publicKey = publicKeyHeaderValues.FirstOrDefault();
             var privateKey = privateKeyHeaderValues.FirstOrDefault();

            if (publicKeyHeaderValues.Count == 0 || string.IsNullOrWhiteSpace(publicKey) ||
                privateKeyHeaderValues.Count == 0 || string.IsNullOrWhiteSpace(privateKey))
                return AuthenticateResult.NoResult();

            var project = await _projectReadRepository.GetByApiKeys(publicKey, privateKey);

            if (project != null)
            {
                var claims = new List<Claim>
                {
                    new Claim(ClaimTypes.Name, project.ProjectName),
                    new Claim(NowClaimTypes.ProjectId, project.Id),
                    new Claim(NowClaimTypes.PublicKey, project.PublicKey),
                    new Claim(NowClaimTypes.PrivateKey, project.PrivateKey)
                };

                if(project.Roles != null)
                    claims.AddRange(project.Roles.Select(role => new Claim(ClaimTypes.Role, role)));

                var identity = new ClaimsIdentity(claims, Options.AuthenticationType);
                var identities = new List<ClaimsIdentity> { identity };
                var principal = new ClaimsPrincipal(identities);
                var ticket = new AuthenticationTicket(principal, Options.Scheme);

                return AuthenticateResult.Success(ticket);
            }

            return AuthenticateResult.Fail("Invalid API Key provided.");
        }

        protected override async Task HandleChallengeAsync(AuthenticationProperties properties)
        {
            Response.StatusCode = 401;
            Response.ContentType = ProblemDetailsContentType;
            var problemDetails = new UnauthorizedProblemDetails();

            await Response.WriteAsync(JsonConvert.SerializeObject(problemDetails));
        }

        protected override async Task HandleForbiddenAsync(AuthenticationProperties properties)
        {
            Response.StatusCode = 403;
            Response.ContentType = ProblemDetailsContentType;
            var problemDetails = new ForbiddenProblemDetails();

            await Response.WriteAsync(JsonConvert.SerializeObject(problemDetails));
        }
    }
}
