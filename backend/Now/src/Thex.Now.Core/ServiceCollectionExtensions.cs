﻿using Microsoft.AspNetCore.Authorization;
using Thex.Now.Core.Authentication;
using Thex.Now.Core.Authentication.Extensions;
using Thex.Now.Core.Authorization;
using Thex.Now.Shared;
using Tnf;

namespace Microsoft.Extensions.DependencyInjection
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddNowAuthentication(this IServiceCollection services)
        {
            Check.NotNull(services, nameof(services));

            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = ApiKeyAuthenticationOptions.DefaultScheme;
                options.DefaultChallengeScheme = ApiKeyAuthenticationOptions.DefaultScheme;
            })
            .AddApiKeySupport(options => { });

            services.AddScoped<IApplicationUser, ApplicationUser>();

            return services;
        }

        public static IServiceCollection AddNowAuthorization(this IServiceCollection services)
        {
            Check.NotNull(services, nameof(services));

            services.AddAuthorization(options =>
            {
                options.AddPolicy(Policies.OnlyManagers, policy => policy.Requirements.Add(new OnlyManagersRequirement()));
            });

            services.AddSingleton<IAuthorizationHandler, OnlyManagersAuthorizationHandler>();

            return services;
        }
    }
}
