﻿using MediatR;

namespace Thex.Now.Domain
{
    public class CreateProjectCommand : IRequest<CreateProjectCommand>
    {
        public string ProjectName { get; set; }
    }
}