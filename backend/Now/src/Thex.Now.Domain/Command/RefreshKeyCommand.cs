﻿using MediatR;
using System;

namespace Thex.Now.Domain
{
    public class RefreshKeyCommand : IRequest<RefreshKeyCommand>
    {
        public string ProjectId { get; set; }
    }
}