﻿using MediatR;
using System;
using Thex.Now.Domain.Enumeration;
using Thex.Now.Shared.Models.Response;

namespace Thex.Now.Domain
{
    public class CreateGroupCommand : BaseClientCommand, IRequest<GroupResponse>
    {
        public string PublicName { get; set; }
        public GroupTypeEnum GroupType { get; set; }
    }
}