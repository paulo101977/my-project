﻿namespace Thex.Now.Domain
{
    public class BaseClientCommand
    {
        public string ProjectId { get; set; }
        public string ClientId { get; set; }
    }
}