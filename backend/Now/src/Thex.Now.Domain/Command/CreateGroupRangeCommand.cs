﻿using MediatR;
using System.Collections.Generic;
using Thex.Now.Domain.Enumeration;
using Thex.Now.Shared.Models.Response;

namespace Thex.Now.Domain
{
    public class CreateGroupRangeCommand : BaseClientCommand, IRequest<List<GroupResponse>>
    {
        public List<GroupRangeCommand> GroupList { get; set; }
    }

    public class GroupRangeCommand
    {
        public string PublicName { get; set; }
        public GroupTypeEnum GroupType { get; set; }
    }
}