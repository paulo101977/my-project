﻿using MediatR;
using System;
using System.Collections.Generic;
using Thex.Now.Domain.Enumeration;

namespace Thex.Now.Domain
{
    public class CreateOrUpdateClientCommand : IRequest<CreateOrUpdateClientCommand>
    {
        public string ProjectId { get; set; }

        public string OwnerId { get; set; }
        public string OwnerName { get; set; }
        public List<CreateOrUpdateGroupCommand> GroupList { get; set; }
    }

    public class CreateOrUpdateGroupCommand
    {
        public Guid? Id { get; set; }
        public string PublicName { get; set; }
        public GroupTypeEnum GroupType { get; set; }
    }
}