﻿using MediatR;
using System;

namespace Thex.Now.Domain
{
    public class UpdateProjectNameCommand : IRequest<UpdateProjectNameCommand>
    {
        public string ProjectId { get; set; }
        public string Name { get; set; }
    }
}