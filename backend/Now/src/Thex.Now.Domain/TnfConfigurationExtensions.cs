﻿using Tnf.Configuration;
using Tnf.Localization;
using Tnf.Localization.Dictionaries;

namespace Thex.Now.Domain
{
    public static class TnfConfigurationExtensions
    {
        public static void UseDomainLocalization(this ITnfConfiguration configuration)
        {
            configuration.Localization.Sources.Add(
                new DictionaryBasedLocalizationSource("Thex",
                new JsonEmbeddedFileLocalizationDictionaryProvider(
                    typeof(TnfConfigurationExtensions).Assembly,
                    "Thex.Now.Domain.Localization.SourceFiles")));

            configuration.Localization.Languages.Add(new LanguageInfo("pt-BR", "Português", isDefault: true));
            configuration.Localization.Languages.Add(new LanguageInfo("en-US", "English"));
            configuration.Localization.Languages.Add(new LanguageInfo("es-AR", "Espanhol"));
            configuration.Localization.Languages.Add(new LanguageInfo("es-CL", "Espanhol"));
            configuration.Localization.Languages.Add(new LanguageInfo("pt-PT", "Português"));
        }
    }
}
