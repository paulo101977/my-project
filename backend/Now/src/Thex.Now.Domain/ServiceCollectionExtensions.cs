﻿using MediatR;
using System.Reflection;
using Thex.Now.Domain;
using Thex.Now.Domain.Interfaces;
using Thex.Now.Domain.Interfaces.ReadRepositories;
using Thex.Now.Domain.Repositories;
using Thex.Now.Domain.Repositories.ReadRepositories;
using Thex.Now.Domain.Services;
using Thex.Now.Domain.Interfaces.Services;

namespace Microsoft.Extensions.DependencyInjection
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddNewDomainDependency(this IServiceCollection services, string mongoConnectionString, string mongoDatabaseName)
        {
            services.AddTnfMongoDb(builder => builder
                 .WithConnectionString(mongoConnectionString)
                 .WithDatabaseName(mongoDatabaseName));

            services.ConfigureMediatR();

            services.AddScoped<IProjectReadRepository, ProjectReadRepository>();

            services.AddScoped<IProjectRepository, ProjectRepository>();

            services.AddScoped<IDefaultProjectService, DefaultProjectService>();
            
            return services;
        }

        private static void ConfigureMediatR(this IServiceCollection services)
        {
            services.AddMediatR(typeof(CreateProjectCommand).GetTypeInfo().Assembly);
        }
    }
}
