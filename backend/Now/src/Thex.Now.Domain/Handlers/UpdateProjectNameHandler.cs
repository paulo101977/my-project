﻿using MediatR;
using Microsoft.Extensions.Configuration;
using System;
using System.Threading;
using System.Threading.Tasks;
using Thex.Now.Domain.Entities;
using Thex.Now.Domain.Interfaces;
using Thex.Now.Domain.Interfaces.ReadRepositories;
using Tnf.Notifications;

namespace Thex.Now.Domain.Handlers
{
    internal class UpdateProjectNameHandler : BaseHandler, IRequestHandler<UpdateProjectNameCommand, UpdateProjectNameCommand>
    {
        private readonly INotificationHandler _notification;
        private readonly IConfiguration _configuration;
        private readonly IProjectRepository _projectRepository;
        private readonly IProjectReadRepository _projectReadRepository;

        public UpdateProjectNameHandler(
            INotificationHandler notification,
            IConfiguration configuration,
            IProjectRepository projectRepository,
            IProjectReadRepository projectReadRepository,
            IMediator mediator) : base(mediator)
        {
            _notification = notification;
            _configuration = configuration;
            _projectRepository = projectRepository;
            _projectReadRepository = projectReadRepository;
        }

        public async Task<UpdateProjectNameCommand> Handle(UpdateProjectNameCommand command, CancellationToken cancellationToken)
        {
            var project = await _projectReadRepository.GetById(command.ProjectId);

            if (project == null)
                throw new NullReferenceException();

            project.ChangeName(command.Name);

            await _projectRepository.UpdateAsync(project);

            return command;
        }
    }
}
