﻿using MediatR;
using Microsoft.Extensions.Configuration;
using System;
using System.Threading;
using System.Threading.Tasks;
using Thex.Now.Domain.Interfaces;
using Thex.Now.Domain.Interfaces.ReadRepositories;
using Thex.Now.Shared.Models.Response;
using Tnf.Notifications;

namespace Thex.Now.Domain.Handlers
{
    internal class CreateGroupHandler : BaseHandler, IRequestHandler<CreateGroupCommand, GroupResponse>
    {
        private readonly INotificationHandler _notification;
        private readonly IConfiguration _configuration;
        private readonly IProjectRepository _projectRepository;
        private readonly IProjectReadRepository _projectReadRepository;

        public CreateGroupHandler(
            INotificationHandler notification,
            IConfiguration configuration,
            IProjectRepository projectRepository,
            IProjectReadRepository projectReadRepository,
            IMediator mediator) : base(mediator)
        {
            _notification = notification;
            _configuration = configuration;
            _projectRepository = projectRepository;
            _projectReadRepository = projectReadRepository;
        }

        public async Task<GroupResponse> Handle(CreateGroupCommand command, CancellationToken cancellationToken)
        {
            var project = await _projectReadRepository.GetByFilters(command.ProjectId, command.ClientId);

            if (project == null)
                throw new NullReferenceException();

            var client = project.GetClient(command.ClientId);

            if (client == null)
                throw new NullReferenceException();

            var group = client.AddGroup(command.PublicName, command.GroupType);

            await _projectRepository.UpdateAsync(project);

            return new GroupResponse
            {
                GroupId = group.Id,
                GroupName = group.GroupName,
                PublicName = group.PublicName
            };
        }
    }
}
