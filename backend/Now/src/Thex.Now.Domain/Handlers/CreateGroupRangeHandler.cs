﻿using MediatR;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Thex.Now.Domain.Entities;
using Thex.Now.Domain.Interfaces;
using Thex.Now.Domain.Interfaces.ReadRepositories;
using Thex.Now.Shared.Models.Response;
using Tnf.Notifications;

namespace Thex.Now.Domain.Handlers
{
    internal class CreateGroupRangeHandler : BaseHandler, IRequestHandler<CreateGroupRangeCommand, List<GroupResponse>>
    {
        private readonly INotificationHandler _notification;
        private readonly IConfiguration _configuration;
        private readonly IProjectRepository _projectRepository;
        private readonly IProjectReadRepository _projectReadRepository;

        public CreateGroupRangeHandler(
            INotificationHandler notification,
            IConfiguration configuration,
            IProjectRepository projectRepository,
            IProjectReadRepository projectReadRepository,
            IMediator mediator) : base(mediator)
        {
            _notification = notification;
            _configuration = configuration;
            _projectRepository = projectRepository;
            _projectReadRepository = projectReadRepository;
        }

        public async Task<List<GroupResponse>> Handle(CreateGroupRangeCommand command, CancellationToken cancellationToken)
        {
            var project = await _projectReadRepository.GetByFilters(command.ProjectId, command.ClientId);
            var groupList = new List<Group>();

            if (project == null)
                throw new NullReferenceException();

            var client = project.GetClient(command.ClientId);

            if (client == null)
                throw new NullReferenceException();

            foreach (var group in command.GroupList)
                groupList.Add(client.AddGroup(group.PublicName, group.GroupType));

            await _projectRepository.UpdateAsync(project);

            return groupList.Select(e => new GroupResponse
            {
                GroupId = e.Id,
                GroupName = e.GroupName,
                PublicName = e.PublicName
            }).ToList();
        }
    }
}
