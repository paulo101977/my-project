﻿using MediatR;
using Microsoft.Extensions.Configuration;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Thex.Now.Domain.Entities;
using Thex.Now.Domain.Interfaces;
using Thex.Now.Domain.Interfaces.ReadRepositories;
using Tnf.Notifications;

namespace Thex.Now.Domain.Handlers
{
    internal class CreateOrUpdateClientHandler : BaseHandler, IRequestHandler<CreateOrUpdateClientCommand, CreateOrUpdateClientCommand>
    {
        private readonly INotificationHandler _notification;
        private readonly IConfiguration _configuration;
        private readonly IProjectRepository _projectRepository;
        private readonly IProjectReadRepository _projectReadRepository;

        public CreateOrUpdateClientHandler(
            INotificationHandler notification,
            IConfiguration configuration,
            IProjectRepository projectRepository,
            IProjectReadRepository projectReadRepository,
            IMediator mediator) : base(mediator)
        {
            _notification = notification;
            _configuration = configuration;
            _projectRepository = projectRepository;
            _projectReadRepository = projectReadRepository;
        }

        public async Task<CreateOrUpdateClientCommand> Handle(CreateOrUpdateClientCommand command, CancellationToken cancellationToken)
        {
            var project = await _projectReadRepository.GetById(command.ProjectId);

            if (project == null)
                throw new NullReferenceException();

            var client = project.GetClient(command.OwnerId);

            if (client == null)
                Create(command, project);
            else
                Update(command, project, client);

            await _projectRepository.UpdateAsync(project);

            return command;
        }

        private void Update(CreateOrUpdateClientCommand command, Project project, Client client)
        {
            client.ChangeName(command.OwnerName);

            foreach (var group in command.GroupList)
                client.AddOrUpdateGroup(group.Id, group.PublicName, group.GroupType);
        }

        private void Create(CreateOrUpdateClientCommand command, Project project)
        {
            Client client;
            command.OwnerId = project.AddClient(command.OwnerName);

            client = project.GetClient(command.OwnerId);

            if(command.GroupList != null)
                foreach (var group in command.GroupList)
                    client.AddGroup(group.PublicName, group.GroupType);
        }
    }
}
