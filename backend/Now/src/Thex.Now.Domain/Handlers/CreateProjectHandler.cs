﻿using MediatR;
using Microsoft.Extensions.Configuration;
using System;
using System.Threading;
using System.Threading.Tasks;
using Thex.Now.Domain.Entities;
using Thex.Now.Domain.Interfaces;
using Tnf.Notifications;

namespace Thex.Now.Domain.Handlers
{
    internal class CreateProjectHandler : BaseHandler, IRequestHandler<CreateProjectCommand, CreateProjectCommand>
    {
        private readonly INotificationHandler _notification;
        private readonly IConfiguration _configuration;
        private readonly IProjectRepository _projectRepository;

        public CreateProjectHandler(
            INotificationHandler notification,
            IConfiguration configuration,
            IProjectRepository projectRepository,
            IMediator mediator) : base(mediator)
        {
            _notification = notification;
            _configuration = configuration;
            _projectRepository = projectRepository;
        }

        public async Task<CreateProjectCommand> Handle(CreateProjectCommand command, CancellationToken cancellationToken)
        {
            var project = new Project(command.ProjectName);

            project.GenerateKeys();

            await _projectRepository.CreateAsync(project);

            return command;
        }
    }
}
