﻿using System;
using System.Collections.Generic;
using Thex.Now.Domain.Base;
using Tnf.MongoDb.Entities;
using System.Linq;
using Thex.Now.Domain.Enumeration;

namespace Thex.Now.Domain.Entities
{
    public partial class Project : BaseEntity, IMongoDbEntity<string>
    {
        public string Id { get; private set; }
        public string ProjectName { get; private set; }
        public string PublicKey { get; private set; }
        public string PrivateKey { get; private set; }

        public Project(string name)
        {
            Id = GenerateId().ToString();
            ProjectName = name;

            AddCreationAudit();
        }

        public List<string> Roles { get; private set; }

        public List<Client> ClientList { get; set; }

        internal void GenerateKeys()
        {
            PublicKey = Guid.NewGuid().ToString();
            PrivateKey = Guid.NewGuid().ToString();
        }

        internal void ChangeName(string projectName)
        {
            ProjectName = projectName;

            AddModificationAudit();
        }

        internal string AddClient(string ownerName)
        {
            if (ClientList == null)
                ClientList = new List<Client>();
            var client = new Client(ownerName);

            ClientList.Add(client);

            return client.OwnerId; 
        }

        internal Client GetClient(string ownerId)
            => ClientList != null ? ClientList.FirstOrDefault(e => e.OwnerId == ownerId) : null;


        public void GenerateDefaultProject(string defaultId, string name, string publicKey, string privateKey, List<KeyValuePair<string, GroupTypeEnum>> groupList)
        {
            Id = defaultId;
            ProjectName = name;
            PublicKey = publicKey;
            PrivateKey = privateKey;
            Roles = new List<string>
            {
                 "Manager"
            };

            AddCreationAudit();

            AddClient(name);

            var client = ClientList.First();

            client.GenerateDefaultClient(defaultId, name, groupList);
        }
    }
}
