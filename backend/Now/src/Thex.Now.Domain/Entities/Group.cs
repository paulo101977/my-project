﻿using System;
using Thex.Now.Domain.Base;
using Thex.Now.Domain.Enumeration;

namespace Thex.Now.Domain.Entities
{
    public class Group : BaseEntity
    {
        public Guid Id { get; private set; }
        public string GroupName { get; private set; }
        public string PublicName { get; private set; }
        public GroupTypeEnum GroupType { get; private set; }
        public bool IsPrivate { get; private set; }

        public Group(string clientId, string publicName, GroupTypeEnum groupType)
        {
            Id = GenerateId();

            GroupName = $"{clientId}__{Id}";
            PublicName = publicName;
            GroupType = groupType;
            IsPrivate = false;

            AddCreationAudit();
        }

        public void Update(string publicName, GroupTypeEnum groupType)
        {
            PublicName = publicName;
            GroupType = groupType;

            AddModificationAudit();
        }

        public void OverrideDefaultGroup()
            => GroupName = $"{GroupName.Split("__")[0]}__{PublicName}";
    }
}
