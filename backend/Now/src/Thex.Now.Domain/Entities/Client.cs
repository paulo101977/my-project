﻿using System;
using System.Collections.Generic;
using Thex.Now.Domain.Base;
using Thex.Now.Domain.Enumeration;
using System.Linq;

namespace Thex.Now.Domain.Entities
{
    public class Client : BaseEntity
    {
        public Client(string ownerName)
        {
            OwnerId = GenerateId().ToString();
            OwnerName = ownerName;

            AddCreationAudit();
        }

        public string OwnerId { get; private set; }
        public string OwnerName { get; private set; }

        public List<Group> GroupList { get; private set; }

        internal Group AddGroup(string publicName, GroupTypeEnum groupType)
        {
            if (GroupList == null)
                GroupList = new List<Group>();

            var group = new Group(OwnerId, publicName, groupType);

            GroupList.Add(group);

            AddModificationAudit();

            return group;
        }

        internal Group AddOrUpdateGroup(Guid? id, string publicName, GroupTypeEnum groupType)
        {
            if (id.HasValue)
            {
                var group = GetGroupById(id.Value);
                group.Update(publicName, groupType);

                return group;
            }
            else
                return AddGroup(publicName, groupType);
        }

        internal void ChangeName(string ownerName)
        {
            OwnerName = ownerName;
            AddModificationAudit();
        }

        internal Group GetGroupById(Guid id)
            => GroupList != null ? GroupList.FirstOrDefault(e => e.Id == id) : null;

        public void GenerateDefaultClient(string defaultId, string name, List<KeyValuePair<string, GroupTypeEnum>> groupList)
        {
            OwnerId = defaultId;
            OwnerName = name;

            AddCreationAudit();

            foreach (var group in groupList)
                AddGroup(group.Key, group.Value);

            foreach (var group in GroupList)
                group.OverrideDefaultGroup();
        }
    }
}
