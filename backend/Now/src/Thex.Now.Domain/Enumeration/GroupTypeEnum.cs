﻿namespace Thex.Now.Domain.Enumeration
{
    public enum GroupTypeEnum
    {
        Message = 1,
        Notification = 2,
        Data = 3,
        Progress = 4
    }
}
