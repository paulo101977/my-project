﻿using MediatR;
using System;
using System.Collections.Generic;

namespace Thex.Now.Domain.Base
{
    public class BaseEntity
    {
        public DateTime CreationTime { get; private set; }
        public DateTime LastModificationTime { get; private set; }

        private readonly List<INotification> _events = new List<INotification>();

        public IReadOnlyList<INotification> Events => _events;

        protected void AddEvent(INotification @event)
        {
            _events.Add(@event);
        }

        protected void AddCreationAudit()
        {
            CreationTime = DateTime.UtcNow;
            LastModificationTime = DateTime.UtcNow;
        }

        protected void AddModificationAudit()
        {
            LastModificationTime = DateTime.UtcNow;
        }

        static internal Guid GenerateId()
         => Guid.NewGuid();
    }
}
