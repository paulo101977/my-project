﻿using MediatR;
using System.Threading.Tasks;
using Thex.Now.Domain.Base;

namespace Thex.Now.Domain
{
    internal class BaseHandler
    {
        private readonly IMediator _mediator;

        public BaseHandler(IMediator mediator)
        {
            _mediator = mediator;
        }

        protected async Task PublishEvents<T>(T entity) where T : BaseEntity
        {
            foreach (var @event in entity.Events)
                await _mediator.Publish((dynamic)@event);
        }
    }
}
