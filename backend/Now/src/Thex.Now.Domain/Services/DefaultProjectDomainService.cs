﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Thex.Now.Domain.Entities;
using Thex.Now.Domain.Enumeration;
using Thex.Now.Domain.Interfaces;
using Thex.Now.Domain.Interfaces.ReadRepositories;
using Thex.Now.Domain.Interfaces.Services;

namespace Thex.Now.Domain.Services
{
    public class DefaultProjectService : IDefaultProjectService
    {
        private readonly IConfiguration _configuration;
        private readonly IProjectRepository _projectRepository;
        private readonly IProjectReadRepository _projectReadRepository;

        public DefaultProjectService(
            IConfiguration configuration,
            IProjectRepository projectRepository,
            IProjectReadRepository projectReadRepository)
        {
            _configuration = configuration;
            _projectRepository = projectRepository;
            _projectReadRepository = projectReadRepository;
        }

        public async Task CreateDefaultProject()
        {
            string publicKey = _configuration["NowClient:PublicKey"];
            string privateKey = _configuration["NowClient:PrivateKey"];
            string defaultId = publicKey;
            string name = "ThexNowAdmin";

            var groups = new List<KeyValuePair<string, GroupTypeEnum>>
            {
                new KeyValuePair<string, GroupTypeEnum>("SampleNotification", GroupTypeEnum.Notification),
                new KeyValuePair<string, GroupTypeEnum>("SampleMessage", GroupTypeEnum.Message),
                new KeyValuePair<string, GroupTypeEnum>("SampleData", GroupTypeEnum.Data),
                new KeyValuePair<string, GroupTypeEnum>("SampleProgress", GroupTypeEnum.Progress)
            };

            var dbProject  = await _projectReadRepository.GetById(publicKey);

            if (dbProject != null) return;

            var project = new Project(name);

            project.GenerateDefaultProject(defaultId, name, publicKey, privateKey, groups);

            await _projectRepository.CreateAsync(project);

        }
    }
}
