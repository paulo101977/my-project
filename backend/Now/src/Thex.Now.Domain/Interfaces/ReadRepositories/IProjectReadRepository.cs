﻿using System;
using System.Threading.Tasks;
using Thex.Now.Domain.Entities;

namespace Thex.Now.Domain.Interfaces.ReadRepositories
{
    public interface IProjectReadRepository
    {
        Task<Project> GetById(string id);
        Task<Project> GetByFilters(string id, string clientOwnerId);
        Task<Project> GetByApiKeys(string apiKey, string privateApiKey);
        Task<bool> AnyByPublicKeyAsync(string apiKey);
        Task<bool> AnyByFiltersAsync(string apiKey, string ownerid, string groupName);
    }
}

