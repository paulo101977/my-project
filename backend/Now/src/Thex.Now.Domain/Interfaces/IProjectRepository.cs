﻿using System.Threading.Tasks;
using Thex.Now.Domain.Entities;

namespace Thex.Now.Domain.Interfaces
{
    public interface IProjectRepository
    {
        Task CreateAsync(Project project);
        Task UpdateAsync(Project project);
    }
}
