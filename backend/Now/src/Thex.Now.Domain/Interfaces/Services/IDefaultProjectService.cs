﻿using System.Threading.Tasks;

namespace Thex.Now.Domain.Interfaces.Services
{
    public interface IDefaultProjectService
    {
        Task CreateDefaultProject();
    }
}

