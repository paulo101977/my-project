﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Thex.Now.Domain.Entities;
using Thex.Now.Domain.Interfaces.ReadRepositories;
using Tnf.MongoDb.Provider;
using Tnf.MongoDb.Repositories;

namespace Thex.Now.Domain.Repositories.ReadRepositories
{
    public class ProjectReadRepository : MongoDbRepository<Project, string>, IProjectReadRepository
    {
        public ProjectReadRepository(IMongoDbProvider provider) : base(provider)
        {
        }

        public async Task<Project> GetById(string id)
            => await FirstOrDefaultAsync(e => e.Id == id);

        public async Task<Project> GetByFilters(string id, string clientOwnerId)
            => await FirstOrDefaultAsync(e => e.Id == id && e.ClientList.Any(c => c.OwnerId == clientOwnerId));

        public async Task<Project> GetByApiKeys(string apiKey, string privateApiKey)
            => await FirstOrDefaultAsync(e => e.PublicKey == apiKey && e.PrivateKey == privateApiKey);

        public async Task<bool> AnyByPublicKeyAsync(string apiKey)
        {
            var project = await FirstOrDefaultAsync(e => e.PublicKey == apiKey);

            return project != null;
        }

        public async Task<bool> AnyByFiltersAsync(string apiKey, string ownerid, string groupName)
        {
            var project = await FirstOrDefaultAsync(e => e.PublicKey == apiKey);

            return  project != null && 
                    project.ClientList != null && 
                    project.ClientList.Any(e => e.OwnerId == ownerid && e.GroupList != null && e.GroupList.Any(g => g.GroupName == groupName));
        }
    }
}
