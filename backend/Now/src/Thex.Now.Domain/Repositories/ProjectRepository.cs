﻿using System;
using System.Threading.Tasks;
using Thex.Now.Domain.Entities;
using Thex.Now.Domain.Interfaces;
using Tnf.MongoDb.Provider;
using Tnf.MongoDb.Repositories;

namespace Thex.Now.Domain.Repositories
{
    public class ProjectRepository : MongoDbRepository<Project, string>, IProjectRepository
    {
        public ProjectRepository(IMongoDbProvider provider) : base(provider)
        {
        }

        public async Task CreateAsync(Project project)
        {
            await InsertAsync(project);
        }

        public async Task UpdateAsync(Project project)
        {
            await base.UpdateAsync(project);
        }
    }
}
