﻿namespace Thex.Now.Client
{
    public class NowConsts
    {
        public const string QueryStringPublicAPI = "ApiKey";
        
        public const string PublicAPI = "X--Public-Api-Key";
        public const string PrivateAPI = "X--Private-Api-Key";

        public const string SendNotification = "SendNotificationAsync";
        public const string SendData = "SendDataAsync";
        public const string SendMessage = "SendMessageAsync";
    }
}
