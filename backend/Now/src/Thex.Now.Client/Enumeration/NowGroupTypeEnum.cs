﻿namespace Thex.Now.Client.Enumeration
{
    public enum NowGroupTypeEnum
    {
        Message = 1,
        Notification = 2,
        Data = 3
    }
}
