﻿using Thex.Now.Client.Framework.Options;

namespace Thex.Now.Client
{
    public class ApplicationOptions
    {
        public PolicyOptions Policies { get; set; }
        public NowClientOptions NowClient { get; set; }
    }
}
