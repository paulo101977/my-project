﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.Now.Client.Models.Request;
using Thex.Now.Client.Models.Response;

namespace Thex.Now.Client
{
    public interface INowClient
    {
        Task<ClientResponse> CreateClientAsync(CreateClientRequest client);
        Task<GroupResponse> CreateGroupAsync(CreateGroupRequest group);
        Task<List<GroupResponse>> CreateRangeGroupAsync(CreateGroupRangeRequest groupRange);
        Task SendMessageAsync(string groupName, string method, string senderId, object message);
        Task SendNotificationAsync(string groupName, string method, string senderId, object notification);
        Task SendDataAsync(string groupName, string method, string senderId, object data);
    }
}