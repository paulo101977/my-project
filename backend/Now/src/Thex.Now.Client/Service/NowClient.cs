﻿using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR.Client;
using System.Linq;
using Thex.Now.Client.Models.Response;
using Thex.Now.Client.Models.Request;

namespace Thex.Now.Client
{
    public class NowClient : INowClient
    {
        private readonly HttpClient _httpClient;
        private readonly HubConnection _connection;

        public NowClient(HttpClient httpClient)
        {
            _httpClient = httpClient;
            _connection = CreateHubConnection();
        }

        public async Task<ClientResponse> CreateClientAsync(CreateClientRequest client)
        {
            var response = await _httpClient.PostAsJsonAsync(RouteConsts.Client, client);

            response.EnsureSuccessStatusCode();

            return await response.Content.ReadAsAsync<ClientResponse>();
        }

        public async Task<GroupResponse> CreateGroupAsync(CreateGroupRequest group)
        {
            var response = await _httpClient.PostAsJsonAsync(RouteConsts.Group, group);

            response.EnsureSuccessStatusCode();

            return await response.Content.ReadAsAsync<GroupResponse>();
        }

        public async Task<List<GroupResponse>> CreateRangeGroupAsync(CreateGroupRangeRequest groupRange)
        {
            var response = await _httpClient.PostAsJsonAsync($"{RouteConsts.Group}/range", groupRange);

            response.EnsureSuccessStatusCode();

            return await response.Content.ReadAsAsync<List<GroupResponse>>();
        }

        public async Task SendMessageAsync(string groupName, string method, string senderId, object message)
        {
            await _connection.StartAsync();
            await _connection.InvokeAsync(NowConsts.SendMessage, GetPublicApiKey(), groupName, method, senderId, message).ContinueWith(_task =>
            {
                if (_task.IsFaulted)
                {
                    //error => _task.Exception.GetBaseException();
                }
            });
            await _connection.DisposeAsync();
        }

        public async Task SendNotificationAsync(string groupName, string method, string senderId, object notification)
        {
            await _connection.StartAsync();
            await _connection.InvokeAsync(NowConsts.SendNotification, GetPublicApiKey(), groupName, method, senderId, notification).ContinueWith(_task =>
            {
                if (_task.IsFaulted)
                {
                    //error => _task.Exception.GetBaseException();
                }
            });
            await _connection.DisposeAsync();
        }

        public async Task SendDataAsync(string groupName, string method, string senderId, object data)
        {
            await _connection.StartAsync();
            await _connection.InvokeAsync(NowConsts.SendData, GetPublicApiKey(), groupName, method, senderId, data).ContinueWith(_task =>
            {
                if (_task.IsFaulted)
                {
                    //error => _task.Exception.GetBaseException();
                }
            });
            await _connection.DisposeAsync();
        }

        private string GetPublicApiKey()
            => _httpClient.DefaultRequestHeaders.GetValues(NowConsts.PublicAPI).FirstOrDefault();

        private HubConnection CreateHubConnection()
            => new HubConnectionBuilder()
                    .WithUrl($"{_httpClient.BaseAddress}{RouteConsts.Hub}?{NowConsts.QueryStringPublicAPI}={GetPublicApiKey()}")
                    .Build();
    }
}