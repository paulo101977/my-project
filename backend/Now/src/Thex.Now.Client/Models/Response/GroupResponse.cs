﻿using System;

namespace Thex.Now.Client.Models.Response
{
    public class GroupResponse
    {
        public Guid GroupId { get; set; }
        public string GroupName { get; set; }
        public string PublicName { get; set; }
    }
}
