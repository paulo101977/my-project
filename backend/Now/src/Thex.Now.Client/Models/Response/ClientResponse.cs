﻿using System;

namespace Thex.Now.Client.Models.Response
{
    public class ClientResponse
    {
        public string ProjectId { get; set; }
        public string OwnerId { get; set; }
        public string OwnerName { get; set; }
    }
}
