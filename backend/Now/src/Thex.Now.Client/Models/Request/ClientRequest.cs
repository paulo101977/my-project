﻿namespace Thex.Now.Client.Models.Request
{
    public class CreateClientRequest 
    {
        public string ProjectId { get; set; }
        public string OwnerName { get; set; }
    }
}
