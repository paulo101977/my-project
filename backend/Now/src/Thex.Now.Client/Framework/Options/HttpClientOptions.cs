﻿using System;

namespace Thex.Now.Client.Framework.Options
{
    public class HttpClientOptions
    {
        public Uri BaseAddress { get; set; }
        public TimeSpan Timeout { get; set; }
        public string PublicKey { get; set; }
        public string PrivateKey { get; set; }
    }
}
