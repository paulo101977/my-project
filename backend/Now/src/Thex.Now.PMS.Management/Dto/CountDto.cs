﻿using System;

namespace Thex.Now.PMS.Management.Dto
{
    public class CountDto
    {
        public int Total { get; set; }
    }
}
