﻿using System;

namespace Thex.Now.PMS.Management.Dto
{
    public class NowNotificationDto
    {
        public long NowNotificationId { get; set; }
        public string Version { get; set; }
        public string Data { get; set; }
        public string NotificationType { get; set; }
        public int GroupType { get; set; }
        public DateTime CreationTime { get; set; }
        public bool? Read { get; set; }
    }
}
