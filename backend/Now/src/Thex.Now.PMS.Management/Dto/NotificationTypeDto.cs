﻿using System;

namespace Thex.Now.PMS.Management.Dto
{
    public class NowNotificationTypeDto
    {
        public string NotificationType { get; set; }
        public string GroupName { get; set; }
        public string PublicName { get; set; }
        public string ClientId { get; set; }
        public string Method { get; set; }
        public string PublicKey { get; set; }
        public string ProjectId { get; set; }
    }
}
