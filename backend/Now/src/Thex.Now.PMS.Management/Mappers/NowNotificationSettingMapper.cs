﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Thex.Now.PMS.Management.Mappers
{
    public class NowNotificationSettingMapper : IEntityTypeConfiguration<NowNotificationSetting>
    {
        public void Configure(EntityTypeBuilder<NowNotificationSetting> builder)
        {
            builder.ToTable("NowNotificationSetting");

            builder.HasAnnotation("Relational:TableName", "NowNotificationSetting");

            builder.HasKey(e => e.Id);

            builder.Property(e => e.Id).HasAnnotation("Relational:ColumnName", "NowNotificationSettingId").ValueGeneratedNever();

            builder.Property(e => e.PublicKey).HasAnnotation("Relational:ColumnName", "PublicKey");

            builder.Property(e => e.PrivateKey).HasAnnotation("Relational:ColumnName", "PrivateKey");

            builder.Property(e => e.ProjectId).HasAnnotation("Relational:ColumnName", "ProjectId");

            builder.Property(e => e.ClientId).HasAnnotation("Relational:ColumnName", "ClientId");

            builder.Property(e => e.CreationTime)
                .HasColumnType("datetime")
                .HasDefaultValueSql("(getutcdate())")
                .HasAnnotation("Relational:ColumnName", "CreationTime");

            builder.Property(e => e.CreatorUserId).HasAnnotation("Relational:ColumnName", "CreatorUserId");

            builder.Property(e => e.DeleterUserId).HasAnnotation("Relational:ColumnName", "DeleterUserId");

            builder.Property(e => e.DeletionTime)
                .HasColumnType("datetime")
                .HasAnnotation("Relational:ColumnName", "DeletionTime");

            builder.Property(e => e.IsDeleted).HasAnnotation("Relational:ColumnName", "IsDeleted");

            builder.Property(e => e.LastModificationTime)
                .HasColumnType("datetime")
                .HasDefaultValueSql("(getutcdate())")
                .HasAnnotation("Relational:ColumnName", "LastModificationTime");

            builder.Property(e => e.LastModifierUserId).HasAnnotation("Relational:ColumnName", "LastModifierUserId");

            builder.Property(e => e.TenantId).HasAnnotation("Relational:ColumnName", "TenantId");
        }
    }
}
