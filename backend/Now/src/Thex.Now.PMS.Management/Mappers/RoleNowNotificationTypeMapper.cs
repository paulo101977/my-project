﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Thex.Now.PMS.Management.Mappers
{
    public class RoleNowNotificationTypeMapper : IEntityTypeConfiguration<RoleNowNotificationType>
    {
        public void Configure(EntityTypeBuilder<RoleNowNotificationType> builder)
        {
            builder.ToTable("RoleNowNotificationType");

            builder.HasAnnotation("Relational:TableName", "RoleNowNotificationType");

            builder.HasKey(e => e.Id);

            builder.Property(e => e.Id).HasAnnotation("Relational:ColumnName", "RoleNowNotificationTypeId").ValueGeneratedNever();

            builder.Property(e => e.NowNotificationTypeId).HasAnnotation("Relational:ColumnName", "NowNotificationTypeId");

            builder.Property(e => e.NotificationType).HasAnnotation("Relational:ColumnName", "NotificationType");

            builder.Property(e => e.RoleId).HasAnnotation("Relational:ColumnName", "RoleId");

            builder.Property(e => e.Method).HasAnnotation("Relational:ColumnName", "Method");

            builder.Property(e => e.GroupId).HasAnnotation("Relational:ColumnName", "GroupId");

            builder.Property(e => e.GroupName).HasAnnotation("Relational:ColumnName", "GroupName");

            builder.Property(e => e.PublicName).HasAnnotation("Relational:ColumnName", "PublicName");

            builder.Property(e => e.ClientId).HasAnnotation("Relational:ColumnName", "ClientId");

            builder.Property(e => e.NowNotificationSettingId).HasAnnotation("Relational:ColumnName", "NowNotificationSettingId");

            builder.Property(e => e.CreationTime)
                .HasColumnType("datetime")
                .HasDefaultValueSql("(getutcdate())")
                .HasAnnotation("Relational:ColumnName", "CreationTime");

            builder.Property(e => e.CreatorUserId).HasAnnotation("Relational:ColumnName", "CreatorUserId");

            builder.Property(e => e.DeleterUserId).HasAnnotation("Relational:ColumnName", "DeleterUserId");

            builder.Property(e => e.DeletionTime)
                .HasColumnType("datetime")
                .HasAnnotation("Relational:ColumnName", "DeletionTime");

            builder.Property(e => e.IsDeleted).HasAnnotation("Relational:ColumnName", "IsDeleted");

            builder.Property(e => e.LastModificationTime)
                .HasColumnType("datetime")
                .HasDefaultValueSql("(getutcdate())")
                .HasAnnotation("Relational:ColumnName", "LastModificationTime");

            builder.Property(e => e.LastModifierUserId).HasAnnotation("Relational:ColumnName", "LastModifierUserId");

            builder.Property(e => e.TenantId).HasAnnotation("Relational:ColumnName", "TenantId");

            builder.HasOne(d => d.NotificationSetting)
                .WithMany(p => p.RoleNotificationTypeList)
                .HasForeignKey(d => d.NowNotificationSettingId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_RoleNowNotificationType_NowNotificationSetting");
        }
    }
}
