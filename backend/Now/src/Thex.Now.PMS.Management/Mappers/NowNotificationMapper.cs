﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Thex.Now.PMS.Management.Mappers
{
    public class NowNotificationMapper : IEntityTypeConfiguration<NowNotification>
    {
        public void Configure(EntityTypeBuilder<NowNotification> builder)
        {
            builder.ToTable("NowNotification");

            builder.HasAnnotation("Relational:TableName", "NowNotification");

            builder.HasKey(e => e.Id);

            builder.Property(e => e.Id).HasAnnotation("Relational:ColumnName", "NowNotificationId").ValueGeneratedOnAdd();
            
            builder.Property(e => e.GroupType).HasAnnotation("Relational:ColumnName", "GroupType");

            builder.Property(e => e.Version).HasAnnotation("Relational:ColumnName", "Version");

            builder.Property(e => e.Data).HasAnnotation("Relational:ColumnName", "Data");

            builder.Property(e => e.NotificationType).HasAnnotation("Relational:ColumnName", "NotificationType");

            builder.Property(e => e.GroupName).HasAnnotation("Relational:ColumnName", "GroupName");

            builder.Property(e => e.PublicName).HasAnnotation("Relational:ColumnName", "PublicName");

            builder.Property(e => e.ClientId).HasAnnotation("Relational:ColumnName", "ClientId");

            builder.Property(e => e.UserId).HasAnnotation("Relational:ColumnName", "UserId");

            builder.Property(e => e.CreationTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getutcdate())")
                    .HasAnnotation("Relational:ColumnName", "CreationTime");

            builder.Property(e => e.CreatorUserId).HasAnnotation("Relational:ColumnName", "CreatorUserId");

            builder.Property(e => e.DeleterUserId).HasAnnotation("Relational:ColumnName", "DeleterUserId");

            builder.Property(e => e.DeletionTime)
                .HasColumnType("datetime")
                .HasAnnotation("Relational:ColumnName", "DeletionTime");

            builder.Property(e => e.IsDeleted).HasAnnotation("Relational:ColumnName", "IsDeleted");

            builder.Property(e => e.LastModificationTime)
                .HasColumnType("datetime")
                .HasDefaultValueSql("(getutcdate())")
                .HasAnnotation("Relational:ColumnName", "LastModificationTime");

            builder.Property(e => e.LastModifierUserId).HasAnnotation("Relational:ColumnName", "LastModifierUserId");

            builder.Property(e => e.TenantId).HasAnnotation("Relational:ColumnName", "TenantId");
        }
    }
}
