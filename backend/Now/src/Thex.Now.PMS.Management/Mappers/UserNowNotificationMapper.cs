﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Thex.Now.PMS.Management.Mappers
{
    public class UserNowNotificationMapper : IEntityTypeConfiguration<UserNowNotification>
    {
        public void Configure(EntityTypeBuilder<UserNowNotification> builder)
        {
            builder.ToTable("UserNowNotification");

            builder.HasAnnotation("Relational:TableName", "UserNowNotification");

            builder.HasKey(e => e.Id);

            builder.Property(e => e.Id).HasAnnotation("Relational:ColumnName", "UserNowNotificationId").ValueGeneratedOnAdd();
            
            builder.Property(e => e.NowNotificationId).HasAnnotation("Relational:ColumnName", "NowNotificationId");

            builder.Property(e => e.UserId).HasAnnotation("Relational:ColumnName", "UserId");

            builder.Property(e => e.Read).HasAnnotation("Relational:ColumnName", "Read");

            builder.Property(e => e.CreationTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getutcdate())")
                    .HasAnnotation("Relational:ColumnName", "CreationTime");

            builder.Property(e => e.CreatorUserId).HasAnnotation("Relational:ColumnName", "CreatorUserId");

            builder.Property(e => e.DeleterUserId).HasAnnotation("Relational:ColumnName", "DeleterUserId");

            builder.Property(e => e.DeletionTime)
                .HasColumnType("datetime")
                .HasAnnotation("Relational:ColumnName", "DeletionTime");

            builder.Property(e => e.IsDeleted).HasAnnotation("Relational:ColumnName", "IsDeleted");

            builder.Property(e => e.LastModificationTime)
                .HasColumnType("datetime")
                .HasDefaultValueSql("(getutcdate())")
                .HasAnnotation("Relational:ColumnName", "LastModificationTime");

            builder.Property(e => e.LastModifierUserId).HasAnnotation("Relational:ColumnName", "LastModifierUserId");

            builder.Property(e => e.TenantId).HasAnnotation("Relational:ColumnName", "TenantId");
        }
    }
}
