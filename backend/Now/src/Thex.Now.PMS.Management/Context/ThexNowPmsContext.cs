﻿using Microsoft.EntityFrameworkCore;
using Thex.Now.PMS.Management.Mappers;
using Tnf.EntityFrameworkCore;
using Tnf.Runtime.Session;

namespace Thex.Now.PMS.Management.Context
{
    public class ThexNowPmsContext : TnfDbContext
    {
        public DbSet<NowNotification> Notifications { get; set; }
        public DbSet<NowNotificationSetting> NotificationSettings { get; set; }
        public DbSet<RoleNowNotificationType> RoleNotificationTypes { get; set; }
        public DbSet<UserNowNotification> UserNowNotifications { get; set; }

        public ThexNowPmsContext(DbContextOptions<ThexNowPmsContext> options, ITnfSession session)
            : base(options, session)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new NowNotificationMapper());
            modelBuilder.ApplyConfiguration(new NowNotificationSettingMapper());
            modelBuilder.ApplyConfiguration(new RoleNowNotificationTypeMapper());
            modelBuilder.ApplyConfiguration(new UserNowNotificationMapper());
        }
    }
}