﻿using System.Threading.Tasks;
using Thex.Now.PMS.Management.Context;
using Thex.Now.PMS.Management.Interfaces;
using Tnf.EntityFrameworkCore;
using Tnf.EntityFrameworkCore.Repositories;

namespace Thex.Now.PMS.Management.Repositories
{
    public class PMSNotificationRepository : EfCoreRepositoryBase<ThexNowPmsContext, NowNotificationSetting>, IPMSNotificationRepository
    {
        public PMSNotificationRepository(IDbContextProvider<ThexNowPmsContext> dbContextProvider)
            : base(dbContextProvider)
        {
        }

        public async Task<NowNotificationSetting> CreateNotificationSettingAsync(NowNotificationSetting entity)
        {
            await Context.AddAsync(entity);
            await Context.SaveChangesAsync();

            return entity;
        }

        public async Task<NowNotification> CreateNotificationAsync(NowNotification entity)
        {
            await Context.AddAsync(entity);
            await Context.SaveChangesAsync();

            return entity;
        }

        public async Task<UserNowNotification> CreateUserNotificationSettingAsync(UserNowNotification entity)
        {
            await Context.AddAsync(entity);
            await Context.SaveChangesAsync();

            return entity;
        }

    }
}
