﻿using Microsoft.Extensions.Configuration;
using System;
using System.Threading.Tasks;
using Thex.Now.PMS.Management.Dto;
using Thex.Now.PMS.Management.Interfaces;
using Dapper;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;
using Tnf.Dto;

namespace Thex.Now.PMS.Management.Repositories
{
    public class PMSNotificationReadRepository : IPMSNotificationReadRepository
    {
        protected readonly string ConnectionStringName;
        protected readonly IConfiguration _configuration;

        public PMSNotificationReadRepository(IConfiguration configuration)
        {
            _configuration = configuration;
            ConnectionStringName = _configuration["ConnectionStrings:SqlServer"];
        }

        public async Task<NowNotificationTypeDto> GetByFiltersAsync(Guid tenantId, string notificationType)
        {
            var sql = @"
                        SELECT distinct
                            nt.NotificationType as NotificationType,
                            nt.GroupName as GroupName,
                            nt.PublicName as PublicName,
                            nt.ClientId as ClientId,
                            nt.Method as Method
                        FROM RoleNowNotificationType  nt
                        WHERE nt.NotificationType = @notificationType
                        AND nt.TenantId = @tenantId
                        AND nt.IsDeleted = 0
                        ";

            using (SqlConnection connection = new SqlConnection(ConnectionStringName))
                return await connection.QueryFirstAsync<NowNotificationTypeDto>(sql, new { notificationType, tenantId });
        }

        public async Task<List<NowNotificationTypeDto>> GetByFiltersAsync(Guid tenantId, List<Guid> roleIdList)
        {
            var sql = @"
                        SELECT distinct
                            nt.NotificationType as NotificationType,
                            nt.GroupName as GroupName,
                            nt.PublicName as PublicName,
                            nt.ClientId as ClientId,
                            nt.Method as Method,
							PublicKey as PublicKey,
							ProjectId as ProjectId
                        FROM RoleNowNotificationType  nt
						INNER JOIN NowNotificationSetting ns on nt.ClientId = ns.ClientId
                        WHERE nt.RoleId IN @roleIdList
                        AND nt.TenantId = @tenantId
                        AND nt.IsDeleted = 0
                        ";

            using (SqlConnection connection = new SqlConnection(ConnectionStringName))
                return (await connection
                    .QueryAsync<NowNotificationTypeDto>(sql, new { roleIdList, tenantId }))
                    .ToList();
        }

        public async Task<List<NowNotificationDto>> GetNotificationListAsync(Guid tenantId, List<string> notificationTypeList, Guid userId)
        {
               var sql = @"
                        select
                            n.NowNotificationId as NowNotificationId,
                            n.Version as Version,
                            n.Data as Data,
                            n.NotificationType as NotificationType,
                            n.CreationTime as CreationTime,
                            n.GroupType as GroupType,
	                        un.[Read] as [Read]
                        from NowNotification n
                        inner join PropertyParameter p on n.TenantId = p.TenantId
                        left join UserNowNotification un on (un.NowNotificationId = n.NowNotificationId and un.userid = @userId)
                        where NotificationType in @notificationTypeList
                        and n.tenantid = @tenantId
                        and(n.userid is null or n.userid = @userId)
                        and n.IsDeleted = 0
                        and p.ApplicationParameterId = 10
                        and cast(n.CreationTime as date) >= DATEADD(day, -5, cast(p.propertyparametervalue as date)) 
                        order by n.CreationTime desc
                        ";

            using (SqlConnection connection = new SqlConnection(ConnectionStringName))
                return (await connection
                    .QueryAsync<NowNotificationDto>(sql, new { notificationTypeList, tenantId, userId }))
                    .ToList();
        }
        
        public async Task<IListDto<NowNotificationDto>> GetNotificationListAsync(Guid tenantId, List<string> notificationTypeList, Guid userId, int? page, int? pageSize)
        {
            page = page ?? 1;
            pageSize = pageSize ?? 20;

            var sql = @"
                             select 
	                            n.NowNotificationId as NowNotificationId,
	                            n.Version as Version,
	                            n.Data as Data,
	                            n.NotificationType as NotificationType,
                                n.CreationTime as CreationTime,
	                            un.[Read] as [Read]
                            from NowNotification n
                            left join UserNowNotification un on (un.NowNotificationId = n.NowNotificationId and un.userid = @userId)
                            where NotificationType in @notificationTypeList
                            and n.tenantid = @tenantId
                            and(n.userid is null or n.userid = @userId)
                            and n.IsDeleted = 0
                            ORDER BY n.CreationTime desc
                            OFFSET (@page - 1) * @pageSize ROWS
                            FETCH NEXT @pageSize  ROWS ONLY
                        ";
 
            var sqlCount = @"select count(*) as Total
                            from NowNotification
                            where NotificationType in @notificationTypeList
                            and tenantid = @tenantId
                            and(userid is null or userid = @userId)
                            and IsDeleted = 0";

            using (SqlConnection connection = new SqlConnection(ConnectionStringName))
            {
                var total = (await connection.QueryFirstAsync<CountDto>(sqlCount, new { notificationTypeList, tenantId, userId, page, pageSize })).Total;
                var result =  (await connection
                                    .QueryAsync<NowNotificationDto>(sql, new { notificationTypeList, tenantId, userId, page, pageSize })).ToList();


                return new ListDto<NowNotificationDto>
                {
                    HasNext = HasNext(page.Value, pageSize.Value, result.Count(), total),
                    Items = result
                };
            }
        }

        public async Task<bool> AnyUserNotificationByFiltersdAsync(Guid tenantId, Guid userId)
        {
            var sql = @"
                            SELECT 1 
                            FROM NowNotification n
	                        INNER JOIN UserNowNotification un 
	                        ON n.NowNotificationId = un.NowNotificationId
                            WHERE un.UserId = @userId
                            AND TenantId = @tenantId
                        ";

            using (SqlConnection conexao = new SqlConnection(ConnectionStringName))
                return (await conexao.QueryAsync<object>(sql, new { tenantId, userId })).Any();
        }

        private bool HasNext(int page, int pageSize, int pagingTotal, int total)
            => total > ((page - 1) * pageSize) + pagingTotal;

    }
}
