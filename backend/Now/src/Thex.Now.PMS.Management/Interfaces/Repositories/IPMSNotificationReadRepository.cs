﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.Now.PMS.Management.Dto;
using Tnf.Dto;

namespace Thex.Now.PMS.Management.Interfaces
{
    public interface IPMSNotificationReadRepository
    {
        Task<NowNotificationTypeDto> GetByFiltersAsync(Guid tenantId, string notificationType);
        Task<List<NowNotificationTypeDto>> GetByFiltersAsync(Guid tenantId, List<Guid> roleIdList);
        Task<List<NowNotificationDto>> GetNotificationListAsync(Guid tenantId, List<string> notificationTypeList, Guid userId);
        Task<IListDto<NowNotificationDto>> GetNotificationListAsync(Guid tenantId, List<string> notificationTypeList, Guid userId, int? page, int? pageSize);
    }
}
