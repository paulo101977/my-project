﻿using System.Threading.Tasks;
using Tnf.Repositories;

namespace Thex.Now.PMS.Management.Interfaces
{
    public interface IPMSNotificationRepository : IRepository<NowNotificationSetting>
    {
        Task<NowNotificationSetting> CreateNotificationSettingAsync(NowNotificationSetting entity);
        Task<NowNotification> CreateNotificationAsync(NowNotification entity);
        Task<UserNowNotification> CreateUserNotificationSettingAsync(UserNowNotification entity);
    }
}
