﻿using System;
using System.Threading.Tasks;
using Thex.Now.PMS.Management.Entities;

namespace Thex.Now.PMS.Management.Interfaces
{
    public interface IPMSNotificationService
    {
        Task CreateClientAsync(Guid tenantId, string propertyName);
        Task SendNotificationAsync<T>(Guid tenantId, T notification, Guid? userId = null) where T : NowBaseNotificationEntity;
        Task SendMessageAsync<T>(Guid tenantId, T message, Guid? userId = null) where T : NowBaseMessageEntity;
        Task SendDataAsync<T>(Guid tenantId, T data, Guid? userId = null) where T : NowBaseDataEntity;
        Task MarkAsReadAsync(Guid tenantId, Guid userId, long notifiationId);
    }
}
