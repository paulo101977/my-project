﻿using System;
using System.Collections.Generic;
using System.Text;
using Thex.Now.Client.Enumeration;
using Thex.Now.PMS.Management.Enumerations;

namespace Thex.Now.PMS.Management
{
    public class RoleGroupNotificationData
    {
        public Guid RoleId { get; set; }
        public List<RoleGroupNotificationMethod> RoleGroupNotificationList { get; set; }

        public RoleGroupNotificationData(Guid roleId, List<RoleGroupNotificationMethod> notificationTypeList)
        {
            RoleId = roleId;
            RoleGroupNotificationList = notificationTypeList;
        }
    }

    public class RoleGroupNotificationMethod
    {
        public string PMSType { get; set; }
        public NowGroupTypeEnum NowGroupType { get; set; }
        public string Method { get; set; }

        public RoleGroupNotificationMethod(string pmsType, NowGroupTypeEnum nowGroupType, string method)
        {
            PMSType = pmsType;
            NowGroupType = nowGroupType;
            Method = method;
        }
    }

    public static class DefaultRoleGroupNotification
    {
        private static RoleGroupNotificationData AdminRole
        {
            get
            {
                return new RoleGroupNotificationData(
                    Guid.Parse("CFA00683-9308-448C-9BED-D2FF29259AD5"),
                    new List<RoleGroupNotificationMethod> {
                        new RoleGroupNotificationMethod(DefaultNotificationNowPMSEnum.NewMigratedReservationPMSNow.ToString(), NowGroupTypeEnum.Notification, MethodNotificationEnum.ReceivedMessage.ToString())
                    });
            }
        }

        private static RoleGroupNotificationData EmployeeRole
        {
            get
            {
                return new RoleGroupNotificationData(
                    Guid.Parse("84B76BD5-6E78-4A64-9AFC-D972C9488850"),
                                        new List<RoleGroupNotificationMethod> {
                        new RoleGroupNotificationMethod(DefaultNotificationNowPMSEnum.NewMigratedReservationPMSNow.ToString(), NowGroupTypeEnum.Notification, MethodNotificationEnum.ReceivedMessage.ToString())
                    });
            }
        }

        public static List<RoleGroupNotificationData> GetDefaultRoleGroupNotification()
        {
            return new List<RoleGroupNotificationData>
            {
                AdminRole,
                EmployeeRole
            };
        }
    }
}
