﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.Now.Client;
using Thex.Now.PMS.Management.Enumerations;
using Thex.Now.PMS.Management.Interfaces;
using Newtonsoft.Json;
using Thex.Now.Client.Models.Response;
using Thex.Now.Client.Models.Request;
using Thex.Now.Client.Enumeration;
using Thex.Now.PMS.Management.Entities;

namespace Thex.Now.PMS.Management.Services
{
    public class PMSNotificationService : IPMSNotificationService
    {
        private readonly IConfiguration _configuration;
        private readonly IPMSNotificationReadRepository _PMSNotificationReadRepository;
        private readonly IPMSNotificationRepository _PMSNotificationRepository;
        private readonly INowClient _nowClient;

        public PMSNotificationService(
            IConfiguration configuration,
            IPMSNotificationReadRepository PMSNotificationReadRepository,
            IPMSNotificationRepository PMSNotificationRepository,
            INowClient nowClient)
        {
            _configuration = configuration;
            _PMSNotificationReadRepository = PMSNotificationReadRepository;
            _PMSNotificationRepository = PMSNotificationRepository;
            _nowClient = nowClient;
        }

        public async Task CreateClientAsync(Guid tenantId, string propertyName)
        {
            var projectId = _configuration["NowClient:ProjectId"];
            var publicKey = _configuration["NowClient:PublicKey"];
            var privateKey = _configuration["NowClient:PrivateKey"];

            var nowClient = await CreateNowClient(propertyName, projectId);
            var nowGroups = await CreateNowGroups(nowClient, projectId);

            var notifSettings = new NowNotificationSetting(publicKey, privateKey, projectId, nowClient.OwnerId, tenantId);

            foreach (var roleNotif in DefaultRoleGroupNotification.GetDefaultRoleGroupNotification())
                notifSettings.AddRangeRoleNotificationType(roleNotif, nowGroups);

            await _PMSNotificationRepository.CreateNotificationSettingAsync(notifSettings);
        }

        public async Task MarkAsReadAsync(Guid tenantId, Guid userId, long notifiationId)
        {
            var userNotification = new UserNowNotification(tenantId, notifiationId, userId);

            userNotification.MaskAsRead();

            await _PMSNotificationRepository.CreateUserNotificationSettingAsync(userNotification);
        }

        public async Task SendNotificationAsync<T>(Guid tenantId, T notification, Guid? userId = null)
            where T : NowBaseNotificationEntity
        {
            var notifTypeDto = await GetNowNotificationType(tenantId, notification.NotificationType);
            
            var serializedNotification = JsonConvert.SerializeObject(notification);

            var notificationEntity = new NowNotification(serializedNotification, notification.GroupType, notification.NotificationType, notifTypeDto.GroupName, notifTypeDto.PublicName, notifTypeDto.ClientId, tenantId, userId);

            await _nowClient.SendNotificationAsync(notifTypeDto.GroupName, notifTypeDto.Method, notification.SenderId.ToString(), serializedNotification);

            await _PMSNotificationRepository.CreateNotificationAsync(notificationEntity);
        }

        public async Task SendMessageAsync<T>(Guid tenantId, T message, Guid? userId = null)
            where T : NowBaseMessageEntity
        {
            var notifTypeDto = await GetNowNotificationType(tenantId, message.NotificationType);

            var serializedMessage = JsonConvert.SerializeObject(message);

            var notificationEntity = new NowNotification(serializedMessage, message.GroupType, message.NotificationType, notifTypeDto.GroupName, notifTypeDto.PublicName, notifTypeDto.ClientId, tenantId, userId);

            await _nowClient.SendMessageAsync(notifTypeDto.GroupName, notifTypeDto.Method, message.SenderId.ToString(), serializedMessage);

            await _PMSNotificationRepository.CreateNotificationAsync(notificationEntity);
        }

        public async Task SendDataAsync<T>(Guid tenantId, T data, Guid? userId = null)
            where T : NowBaseDataEntity
        {
            var notifTypeDto = await GetNowNotificationType(tenantId, data.NotificationType);

            var serializedData = JsonConvert.SerializeObject(data);

            var notificationEntity = new NowNotification(serializedData, data.GroupType, data.NotificationType, notifTypeDto.GroupName, notifTypeDto.PublicName, notifTypeDto.ClientId, tenantId, userId);

            await _nowClient.SendDataAsync(notifTypeDto.GroupName, notifTypeDto.Method, data.SenderId.ToString(), serializedData);

            await _PMSNotificationRepository.CreateNotificationAsync(notificationEntity);
        }

        private async Task<Dto.NowNotificationTypeDto> GetNowNotificationType(Guid tenantId, string notificationType)
            => await _PMSNotificationReadRepository.GetByFiltersAsync(tenantId, notificationType);

        private async Task<ClientResponse> CreateNowClient(string propertyName, string projectId)
            => await _nowClient.CreateClientAsync(new CreateClientRequest
            {
                OwnerName = propertyName,
                ProjectId = projectId
            });

        private async Task<List<GroupResponse>> CreateNowGroups(ClientResponse nowClient, string projectId)
        {
            var groupList = new List<GroupRangeRequest>();

            foreach (DefaultNotificationNowPMSEnum notifEnum in Enum.GetValues(typeof(DefaultNotificationNowPMSEnum)))
                groupList.Add(new GroupRangeRequest
                {
                    GroupType = NowGroupTypeEnum.Notification,
                    PublicName = notifEnum.ToString()
                });

            return await _nowClient.CreateRangeGroupAsync(new CreateGroupRangeRequest
            {
                ClientId = nowClient.OwnerId,
                ProjectId = projectId,
                GroupList = groupList
            });
        }
    }
}
