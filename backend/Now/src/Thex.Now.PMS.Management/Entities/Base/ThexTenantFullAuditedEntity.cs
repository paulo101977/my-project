﻿using System;

namespace Thex.Now.PMS.Management.Entities
{
    [Serializable]
    public abstract class ThexTenantFullAuditedEntity
    {
        public Guid TenantId { get; set; }

        public virtual bool IsDeleted { get; set; }

        public virtual DateTime CreationTime { get; set; }

        public virtual Guid? CreatorUserId { get; set; }

        public virtual DateTime? LastModificationTime { get; set; }

        public virtual Guid? LastModifierUserId { get; set; }

        public virtual DateTime? DeletionTime { get; set; }

        public virtual Guid? DeleterUserId { get; set; }
    }
}
