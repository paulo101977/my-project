﻿using System;

namespace Thex.Now.PMS.Management.Entities
{
    public class NowBaseNotificationEntity
    {
        public int TemplateVersion { get; set; }
        public string NotificationType { get; set; }
        public int GroupType { get; set; }
        public Guid SenderId { get; set; }
        public string NotificationData { get; set; }
    }
}
