﻿using System;

namespace Thex.Now.PMS.Management.Entities
{
    public class NowBaseDataEntity
    {
        public int TemplateVersion { get; set; }
        public string NotificationType { get; set; }
        public int GroupType { get; set; }
        public Guid SenderId { get; set; }
    }
}
