﻿using System;
using System.Collections.Generic;
using System.Linq;
using Thex.Now.Client.Models.Response;
using Thex.Now.PMS.Management.Entities;

namespace Thex.Now.PMS.Management
{
    public class NowNotificationSetting : ThexTenantFullAuditedEntity
    {
        public virtual Guid Id { get; private set; }
        public virtual string PublicKey { get; private set; }
        public virtual string PrivateKey { get; private set; }
        public virtual string ProjectId { get; private set; }
        public virtual string ClientId { get; private set; }

        public virtual ICollection<RoleNowNotificationType> RoleNotificationTypeList { get; private set; }

        protected NowNotificationSetting()
        {
        }

        public NowNotificationSetting(string publicKey, string privateKey, string projectId, string clientId, Guid tenantId)
        {
            Id = Guid.NewGuid();
            PublicKey = publicKey;
            PrivateKey = privateKey;
            ProjectId = projectId;
            ClientId = clientId;
            TenantId = tenantId;
        }

        internal void AddRangeRoleNotificationType(RoleGroupNotificationData roleGroupData, List<GroupResponse> nowGroupList)
        {
            if (RoleNotificationTypeList == null)
                RoleNotificationTypeList = new List<RoleNowNotificationType>();

            foreach (var notifType in roleGroupData.RoleGroupNotificationList)
            {
                var nowGroup = nowGroupList.FirstOrDefault(e => e.PublicName == notifType.PMSType);

                if (nowGroup == null)
                    continue;

                RoleNotificationTypeList.Add(new RoleNowNotificationType(
                    (int)notifType.NowGroupType, notifType.PMSType, 
                    roleGroupData.RoleId, nowGroup.GroupId, 
                    nowGroup.GroupName, nowGroup.PublicName,
                    ClientId, Id,
                    notifType.Method, TenantId));
            }
        }
    }
}
