﻿using System;
using Thex.Now.PMS.Management.Entities;

namespace Thex.Now.PMS.Management
{
    public class RoleNowNotificationType : ThexTenantFullAuditedEntity
    {
        public Guid Id { get; private set; }
        public int NowNotificationTypeId { get; private set; }
        public string NotificationType { get; private set; }
        public Guid RoleId { get; private set; }
        public Guid GroupId { get; private set; }
        public string GroupName { get; private set; }
        public string PublicName { get; private set; }
        public string ClientId { get; private set; }
        public Guid NowNotificationSettingId { get; private set; }
        public string Method { get; private set; }

        public NowNotificationSetting NotificationSetting { get; private set; }

        protected RoleNowNotificationType()
        {
        }

        public RoleNowNotificationType(
            int nowNotificationTypeId, string notificationType, Guid roleId, 
            Guid groupId, string groupName, string publicName, 
            string clientId, Guid notificationSettingId,
            string method, Guid tenantId)
        {
            Id = Guid.NewGuid();

            NowNotificationTypeId = nowNotificationTypeId;
            NotificationType = notificationType;
            RoleId = roleId;
            GroupId = groupId;
            GroupName = groupName;
            PublicName = publicName;
            ClientId = clientId;
            NowNotificationSettingId = notificationSettingId;
            Method = method;
            TenantId = tenantId;
        }
    }
}
