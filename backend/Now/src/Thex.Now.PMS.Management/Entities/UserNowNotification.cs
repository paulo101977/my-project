﻿using System;
using Thex.Now.PMS.Management.Entities;

namespace Thex.Now.PMS.Management
{
    public class UserNowNotification : ThexTenantFullAuditedEntity
    {
        public long Id { get; private set; }
        public long NowNotificationId { get; private set; }
        public Guid UserId { get; private set; }
        public bool Read { get; private set; }

        protected UserNowNotification()
        {
        }

        public UserNowNotification(Guid tenantId, long notificationId, Guid userId)
        {
            TenantId = tenantId;
            UserId = userId;
            NowNotificationId = notificationId;
        }

        public void MaskAsRead()
        {
            Read = true;
        }
    }
}
