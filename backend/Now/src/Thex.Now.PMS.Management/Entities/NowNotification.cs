﻿using System;
using Thex.Now.PMS.Management.Entities;
using Thex.Now.PMS.Management.Enumerations;

namespace Thex.Now.PMS.Management
{
    public class NowNotification : ThexTenantFullAuditedEntity
    {
        public long Id { get; private set; }
        public int GroupType { get; private set; }
        public int Version { get; private set; }
        public string Data { get; private set; }
        public string NotificationType { get; private set; }
        public string GroupName { get; private set; }
        public string PublicName { get; private set; }
        public string ClientId { get; private set; }
        public Guid? UserId { get; private set; }

        protected NowNotification()
        {
        }

        public NowNotification(string data, int groupType, string notificationType, string groupName, string publicName, string clientId, Guid tenantId, Guid? userId)
        {
            GroupType = groupType;
            Version = (int)VersionEnum.V1;
            Data = data;
            NotificationType = notificationType;
            GroupName = groupName;
            PublicName = publicName;
            ClientId = clientId;
            TenantId = tenantId;
            UserId = UserId;
        }
    }
}
