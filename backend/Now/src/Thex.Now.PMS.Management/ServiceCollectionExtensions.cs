﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Thex.Now.PMS.Management.Context;
using Thex.Now.PMS.Management.Interfaces;
using Thex.Now.PMS.Management.Repositories;
using Thex.Now.PMS.Management.Services;

namespace Microsoft.Extensions.DependencyInjection
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddNowPMSService(this IServiceCollection services, IConfiguration configuration)
        {
            services
                .AddNowService(configuration)
                .AddTnfEntityFrameworkCore()
                .AddTnfDbContext<ThexNowPmsContext>((config) =>
                {
                    config.DbContextOptions.UseSqlServer(configuration.GetSection("ConnectionStrings").GetValue<string>("SqlServer"));
                });

            services.AddTransient<IPMSNotificationReadRepository, PMSNotificationReadRepository>();

            services.AddTransient<IPMSNotificationRepository, PMSNotificationRepository>();

            services.AddTransient<IPMSNotificationService, PMSNotificationService>();

            return services;
        }
    }
}
