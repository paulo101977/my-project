﻿using Tnf.Dto;

namespace Thex.Location.Dto.GetAll
{
    public class GetAllCountryDto : RequestAllDto
    {
        public string LanguageIsoCode { get; set; }
    }
}
