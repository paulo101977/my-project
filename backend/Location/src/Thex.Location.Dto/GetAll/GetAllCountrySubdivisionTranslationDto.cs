﻿using Tnf.Dto;

namespace Thex.Location.Dto.GetAll
{
    public class GetAllCountrySubdivisionTranslationDto : RequestAllDto
    {
        public string Language { get; set; }
    }
}
