﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Diagnostics;
using Thex.Common;
using Thex.Location.Application.Adapters;
using Thex.Location.Application.Interfaces;
using Thex.Location.Application.Services;
using Thex.Location.Infra;
using Thex.Location.Infra.Context;
using Thex.Maps.Geocode;

namespace Thex.Location.Application
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddApplicationServiceLocationDependency(this IServiceCollection services, Action<MapsGeocodeConfig> mapsGeocodeConfig, IConfiguration configuration = null)
        {
            // Dependencia do projeto Thex.Domain
            services
                .AddInfraDependency()
                .AddMapsGeocodeDependency(mapsGeocodeConfig)
                .AddEntityFrameworkSqlServer()
                .AddDbContext<ThexLocationContext>(options =>
                    options.UseSqlServer(configuration.GetConnectionString("SqlServer")));

            services.AddTnfDefaultConventionalRegistrations();

            // Registro dos serviços
            services.AddTransient<ILocationAdapter, LocationAdapter>();
            services.AddTransient<ICountrySubdivisionAdapter, CountrySubdivisionAdapter>();
            services.AddTransient<ICountrySubdivisionTranslationAdapter, CountrySubdivisionTranslationAdapter>();

            services.AddTransient<ILocationAppService, LocationAppService>();
            services.AddTransient<ICountrySubdivisionAppService, CountrySubdivisionAppService>();
            return services;
        }
    }
}
