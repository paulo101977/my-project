﻿using System;
using System.Collections.Generic;
using Thex.Common.Dto;
using Thex.Location.Application.Adapters;
using Thex.Location.Application.Interfaces;
using Thex.Location.Infra.Entities;
using Thex.Location.Infra.Interfaces;
using Thex.Location.Infra.Interfaces.Read;
using Tnf.Notifications;

namespace Thex.Location.Application.Services
{
    public class LocationAppService : ApplicationServiceBase, ILocationAppService
    {
        private readonly ILocationRepository _locationRepository;
        private readonly ILocationReadRepository _locationReadRepository;
        private readonly ILocationAdapter _locationAdapter;
        private readonly ICountrySubdivisionAppService _countrySubdivisionAppService;

        public LocationAppService(
            ILocationRepository locationRepository,
            ILocationReadRepository locationReadRepository,
            ILocationAdapter locationAdapter,
            ICountrySubdivisionAppService countrySubdivisionAppService,
            INotificationHandler notificationHandler)
            : base(notificationHandler)
        {
            _locationRepository = locationRepository;
            _locationReadRepository = locationReadRepository;
            _locationAdapter = locationAdapter;
            _countrySubdivisionAppService = countrySubdivisionAppService;
        }

        public LocationDto Get(int id)
        {
            if (id < 0)
            {
                NotifyIdIsMissing();
                return LocationDto.NullInstance;
            }

            LocationEntity location = _locationReadRepository.GetById(id);
            return location.MapTo<LocationDto>();
        }

        public IList<LocationDto> GetAllByOwnerId(Guid id, string language)
        {
            var location = _locationReadRepository.GetLocationDtoListByPersonId(id, language);
            return location;
        }

        public ICollection<LocationEntity> ToDomain(ICollection<LocationDto> locations)
        {
            var locationList = new HashSet<LocationEntity>();
            if (locations != null)
            {
                foreach (var locationDto in locations)
                {
                    var locationBuilder = this.GetBuilder(locationDto);
                    var location = locationBuilder.Build();
                    locationList.Add(location);
                }
            }
            return locationList;
        }

        /// <summary>
        /// Creates a <see cref="LocationBuilder"/> passing a dto with parents
        /// </summary>
        /// <param name="LocationDto">An instance of the <see cref="LocationDto"/> class</param>
        /// <returns>This <see cref="LocationBuilder"/> instance</returns>
        public LocationEntity.Builder GetBuilder(LocationDto locationDto)
        {
            var builder = new LocationEntity.Builder(Notification);

            builder
                .WithId(locationDto.Id)
                .WithOwnerId(locationDto.OwnerId)
                .WithLatitude(locationDto.Latitude)
                .WithLongitude(locationDto.Longitude)
                .WithStreetName(locationDto.StreetName)
                .WithStreetNumber(locationDto.StreetNumber)
                .WithCountryCode(locationDto.CountryCode)
                .WithAdditionalAddressDetails(locationDto.AdditionalAddressDetails);

            return builder;
        }

        public LocationEntity.Builder GetBuilder(LocationDto locationDto, Guid personId)
        {
            var builder = new LocationEntity.Builder(Notification);

            builder
                .WithId(locationDto.Id)
                .WithOwnerId(personId)
                .WithLatitude(locationDto.Latitude)
                .WithLongitude(locationDto.Longitude)
                .WithStreetName(locationDto.StreetName)
                .WithStreetNumber(locationDto.StreetNumber)
                .WithCountryCode(locationDto.CountryCode)
                .WithAdditionalAddressDetails(locationDto.AdditionalAddressDetails);

            return builder;
        }

        public LocationDto Create(LocationDto location)
        {
            if (location == null)
            {
                NotifyNullParameter();
                return LocationDto.NullInstance;
            }

            location.Id = _locationRepository.InsertLocation(_locationAdapter.Map(location).Build()).Id;

            return location;
        }

        public void CreateLocationList(ICollection<LocationDto> locationDtoList, Guid personId)
        {
            locationDtoList = SetCountrySubdivision(locationDtoList);
            IList<LocationEntity> locationList = new List<LocationEntity>();

            foreach (var locationDto in locationDtoList)
            {
                LocationEntity location = GetBuilder((LocationDto)locationDto, personId)
                    .WithLocationCategoryId(locationDto.LocationCategoryId)
                    .WithNeighborhood(locationDto.Neighborhood)
                    .WithPostalCode(locationDto.PostalCode)
                    .WithCityId(locationDto.CityId)
                    .WithCountryId(locationDto.CountryId)
                    .WithStateId(locationDto.StateId)
                    .WithCountryCode(locationDto.CountryCode).Build();

                locationList.Add(location);
            }

            if (!Notification.HasNotification())
                _locationRepository.AddRange(locationList);
        }

        public LocationDto Update(LocationDto location)
        {
            if (location == null)
            {
                NotifyNullParameter();
                return LocationDto.NullInstance;
            }

            if (location.Id <= 0)
                NotifyIdIsMissing();

            if (Notification.HasNotification())
                return LocationDto.NullInstance;

            _locationRepository.UpdateLocation(_locationAdapter.Map(location).Build());

            return location;
        }

        public void Delete(int id)
        {
            if (id < 0)
            {
                NotifyIdIsMissing();
                return;
            }

            var location = _locationReadRepository.GetById(id);
            if (location == null)
            {
                NotifyWhenEntityNotExist("Location");
                return;
            }

            _locationRepository.DeleteLocation(location);
        }

        public void UpdateLocationList(IList<LocationDto> locationDtoList, string ownerId)
        {
            var locationList = new List<LocationEntity>();

            foreach (var locationDto in locationDtoList)
                locationList.Add(GetLocationForUpdated(locationDto, ownerId));

            if (Notification.HasNotification() || locationList.Count == 0)
                return;

            _locationRepository.UpdateLocationList(locationList, Guid.Parse(ownerId));
        }

        public LocationEntity GetLocationForUpdated(LocationDto locationDto, string personId)
        {
            LocationEntity location = GetBuilder((LocationDto)locationDto, Guid.Parse(personId))
                .WithLocationCategoryId(locationDto.LocationCategoryId)
                .WithNeighborhood(locationDto.Neighborhood)
                .WithPostalCode(locationDto.PostalCode)
                .WithCityId(locationDto.CityId)
                .WithCountryId(locationDto.CountryId)
                .WithStateId(locationDto.StateId)
                .WithStreetName(locationDto.StreetName)
                .WithStreetNumber(locationDto.StreetNumber)
                .WithAdditionalAddressDetails(locationDto.AdditionalAddressDetails)
                .WithCountryCode(locationDto.CountryCode).Build();

            return location;
        }

        public void CreateOrUpdateGuestRegistrationLocation(LocationDto locationDto, string ownerId)
        {
            var location = GetLocationForUpdated(locationDto, ownerId);

            if (Notification.HasNotification())
                return;

            _locationRepository.CreateorUpdateLocationGuestRegistration(location, Guid.Parse(ownerId));
        }


        public ICollection<LocationDto> SetCountrySubdivision(ICollection<LocationDto> locationDtoList)
        {
            foreach (var locationDto in locationDtoList)
            {
                var countrySubdivision = this._countrySubdivisionAppService.CreateCountrySubdivisionTranslateAndGetCityId(locationDto, locationDto.BrowserLanguage);
                locationDto.CityId = countrySubdivision.Item1;
                locationDto.StateId = countrySubdivision.Item2;
                locationDto.CountryId = countrySubdivision.Item3;
            }

            return locationDtoList;
        }
    }
}
