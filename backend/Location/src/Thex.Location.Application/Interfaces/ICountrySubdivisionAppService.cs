﻿using Thex.Common.Dto;
using Thex.Location.Dto.GetAll;
using Tnf.Dto;

namespace Thex.Location.Application.Interfaces
{
    public interface ICountrySubdivisionAppService
    {
        IListDto<CountryDto> GetAllCountriesByLanguageIsoCode(GetAllCountryDto request);
        (int, int, int) CreateCountrySubdivisionTranslateAndGetCityId(LocationDto location, string language);
    }
}
