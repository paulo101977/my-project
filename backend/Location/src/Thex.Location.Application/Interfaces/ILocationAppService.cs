﻿using System;
using System.Collections.Generic;
using Thex.Common.Dto;
using Thex.Location.Infra.Entities;

namespace Thex.Location.Application.Interfaces
{
    public interface ILocationAppService
    {
        void CreateLocationList(ICollection<LocationDto> locationDtoList, Guid personId);
        LocationDto Get(int id);
        LocationEntity.Builder GetBuilder(LocationDto locationDto);
        LocationEntity.Builder GetBuilder(LocationDto locationDto, Guid personId);
        ICollection<LocationEntity> ToDomain(ICollection<LocationDto> locations);
        LocationDto Update(LocationDto location);
        void UpdateLocationList(IList<LocationDto> locationDtoList, string ownerId);
        LocationEntity GetLocationForUpdated(LocationDto locationDto, string personId);
        void CreateOrUpdateGuestRegistrationLocation(LocationDto locationDto, string ownerId);
        IList<LocationDto> GetAllByOwnerId(Guid id, string language);
        ICollection<LocationDto> SetCountrySubdivision(ICollection<LocationDto> locationDtoList);
    }
}
