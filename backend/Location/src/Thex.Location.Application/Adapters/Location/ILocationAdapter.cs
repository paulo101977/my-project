﻿using Thex.Common.Dto;
using Thex.Location.Infra.Entities;

namespace Thex.Location.Application.Adapters
{
    public interface ILocationAdapter
    {
        LocationEntity.Builder Map(LocationEntity entity, LocationDto dto);
        LocationEntity.Builder Map(LocationDto dto);
    }
}
