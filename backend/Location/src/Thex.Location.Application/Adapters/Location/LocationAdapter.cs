﻿using Thex.Common.Dto;
using Thex.Location.Infra.Entities;
using Tnf;
using Tnf.Notifications;

namespace Thex.Location.Application.Adapters
{
    public class LocationAdapter : ILocationAdapter
    {
        public INotificationHandler NotificationHandler { get; }

        public LocationAdapter(INotificationHandler notificationHandler)
        {
            NotificationHandler = notificationHandler;
        }

        public virtual LocationEntity.Builder Map(LocationEntity entity, LocationDto dto)
        {
            Check.NotNull(entity, nameof(entity));
            Check.NotNull(dto, nameof(dto));

            var builder = new LocationEntity.Builder(NotificationHandler, entity)
                .WithId(dto.Id)
                .WithOwnerId(dto.OwnerId)
                .WithLocationCategoryId(dto.LocationCategoryId)
                .WithLatitude(dto.Latitude)
                .WithLongitude(dto.Longitude)
                .WithStreetName(dto.StreetName)
                .WithStreetNumber(dto.StreetNumber)
                .WithAdditionalAddressDetails(dto.AdditionalAddressDetails)
                .WithNeighborhood(dto.Neighborhood)
                .WithCityId(dto.CityId)
                .WithStateId(dto.StateId)
                .WithCountryId(dto.CountryId)
                .WithPostalCode(dto.PostalCode)
                .WithCountryCode(dto.CountryCode);

            return builder;
        }

        public virtual LocationEntity.Builder Map(LocationDto dto)
        {
            Check.NotNull(dto, nameof(dto));

            var builder = new LocationEntity.Builder(NotificationHandler)
                .WithId(dto.Id)
                .WithOwnerId(dto.OwnerId)
                .WithLocationCategoryId(dto.LocationCategoryId)
                .WithLatitude(dto.Latitude)
                .WithLongitude(dto.Longitude)
                .WithStreetName(dto.StreetName)
                .WithStreetNumber(dto.StreetNumber)
                .WithAdditionalAddressDetails(dto.AdditionalAddressDetails)
                .WithNeighborhood(dto.Neighborhood)
                .WithCityId(dto.CityId)
                .WithStateId(dto.StateId)
                .WithCountryId(dto.CountryId)
                .WithPostalCode(dto.PostalCode)
                .WithCountryCode(dto.CountryCode);

            return builder;
        }
    }
}
