﻿using Thex.Common.Dto;
using Thex.Location.Infra.Entities;

namespace Thex.Location.Application.Adapters
{
    public interface ICountrySubdivisionTranslationAdapter
    {
        CountrySubdivisionTranslation.Builder Map(CountrySubdivisionTranslation entity, CountrySubdivisionTranslationDto dto);
        CountrySubdivisionTranslation.Builder Map(CountrySubdivisionTranslationDto dto);
    }
}
