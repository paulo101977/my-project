﻿using System;
using Thex.Common.Dto;
using Thex.Location.Infra.Entities;
using Tnf;
using Tnf.Notifications;

namespace Thex.Location.Application.Adapters
{
    public class CountrySubdivisionTranslationAdapter : ICountrySubdivisionTranslationAdapter
    {
        public INotificationHandler NotificationHandler { get; }

        public CountrySubdivisionTranslationAdapter(INotificationHandler notificationHandler)
        {
            NotificationHandler = notificationHandler;
        }

        public CountrySubdivisionTranslation.Builder Map(CountrySubdivisionTranslation entity, CountrySubdivisionTranslationDto dto)
        {
            Check.NotNull(entity, nameof(entity));
            Check.NotNull(dto, nameof(dto));

            var builder = new CountrySubdivisionTranslation.Builder(NotificationHandler, entity)
                .WithId(dto.Id)
                .WithLanguageIsoCode(dto.LanguageIsoCode)
                .WithName(dto.Name)
                .WithCountrySubdivisionId(dto.CountrySubdivisionId)
                .WithNationality(dto.Nationality);

            return builder;
        }

        public CountrySubdivisionTranslation.Builder Map(CountrySubdivisionTranslationDto dto)
        {
            Check.NotNull(dto, nameof(dto));

            var builder = new CountrySubdivisionTranslation.Builder(NotificationHandler)
                .WithId(dto.Id)
                .WithLanguageIsoCode(dto.LanguageIsoCode)
                .WithName(dto.Name)
                .WithCountrySubdivisionId(dto.CountrySubdivisionId)
                .WithNationality(dto.Nationality);

            return builder;
        }
    }
}
