﻿using Thex.Common.Dto;
using Thex.Location.Infra.Entities;

namespace Thex.Location.Application.Adapters
{
    public interface ICountrySubdivisionAdapter
    {
        CountrySubdivision.Builder Map(CountrySubdivision entity, CountrySubdivisionDto dto);
        CountrySubdivision.Builder Map(CountrySubdivisionDto dto);
    }
}
