﻿using Microsoft.EntityFrameworkCore;
using Tnf.EntityFrameworkCore;
using Tnf.Runtime.Session;
using Thex.Location.Infra.Entities;
using Thex.Location.Infra.Mappers;

namespace Thex.Location.Infra.Context
{
    public class ThexLocationContext : TnfDbContext
    {
        public DbSet<LocationCategory> LocationCategories { get; set; }
        public DbSet<CountrySubdivision> CountrySubdivisions { get; set; }
        public DbSet<CountryLanguage> CountryLanguages { get; set; }
        public DbSet<LocationEntity> Locations { get; set; }
        public DbSet<CountrySubdivisionTranslation> CountrySubdivisionTranslations { get; set; }
        public DbSet<Property> Properties { get; set; }

        public ThexLocationContext(DbContextOptions<ThexLocationContext> options, ITnfSession session)
            : base(options, session)
        {

            
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new PropertyMapper());
            modelBuilder.ApplyConfiguration(new CountrySubdivisionTranslationMapper());
            modelBuilder.ApplyConfiguration(new CountryLanguageMapper());
            modelBuilder.ApplyConfiguration(new LocationMapper());
            modelBuilder.ApplyConfiguration(new LocationCategoryMapper());
            modelBuilder.ApplyConfiguration(new CountrySubdivisionMapper());
        }
    }
}