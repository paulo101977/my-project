﻿using Microsoft.Extensions.DependencyInjection;
using Thex.Location.Infra.Interfaces;
using Thex.Location.Infra.Interfaces.Read;
using Thex.Location.Infra.Repositories;
using Thex.Location.Infra.Repositories.Read;

namespace Thex.Location.Infra
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddInfraDependency(this IServiceCollection services)
        {
            services
              .AddTnfEntityFrameworkCore();    // Configura o uso do EntityFrameworkCore registrando os contextos que serão usados pela aplicação

            services.AddTnfDefaultConventionalRegistrations();

            // Registro dos repositórios
            services.AddTransient<ILocationRepository, LocationRepository>();
            services.AddTransient<ICountrySubdivisionRepository, CountrySubdivisionRepository>();
            services.AddTransient<ICountrySubdivisionTranslationRepository, CountrySubdivisionTranslationRepository>();

            services.AddTransient<ILocationReadRepository, LocationReadRepository>();
            services.AddTransient<ICountrySubdivisionTranslationReadRepository, CountrySubdivisionTranslationReadRepository>();

            return services;
        }
    }
}
