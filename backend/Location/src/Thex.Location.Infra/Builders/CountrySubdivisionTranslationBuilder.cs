﻿using Thex.Common;
using Tnf.Builder;
using Tnf.Notifications;
using Tnf.Specifications;

namespace Thex.Location.Infra.Entities
{
    public partial class CountrySubdivisionTranslation
    {
        public class Builder : Builder<CountrySubdivisionTranslation>
        {
            public Builder(INotificationHandler handler) : base(handler)
            {
            }

            public Builder(INotificationHandler handler, CountrySubdivisionTranslation instance) : base(handler, instance)
            {
            }

            public virtual Builder WithId(int id)
            {
                Instance.Id = id;
                return this;
            }
            public virtual Builder WithLanguageIsoCode(string languageIsoCode)
            {
                Instance.LanguageIsoCode = languageIsoCode;
                return this;
            }
            public virtual Builder WithName(string name)
            {
                Instance.Name = name;
                return this;
            }
            public virtual Builder WithCountrySubdivisionId(int countrySubdivisionId)
            {
                Instance.CountrySubdivisionId = countrySubdivisionId;
                return this;
            }
            public virtual Builder WithNationality(string nationality)
            {
                Instance.Nationality = nationality;
                return this;
            }

            protected override void Specifications()
            {
                AddSpecification(new ExpressionSpecification<CountrySubdivisionTranslation>(
                    AppConsts.LocalizationSourceName,
                    CountrySubdivisionTranslation.EntityError.CountrySubdivisionTranslationMustHaveLanguageIsoCode,
                    w => !string.IsNullOrWhiteSpace(w.LanguageIsoCode)));

                AddSpecification(new ExpressionSpecification<CountrySubdivisionTranslation>(
                    AppConsts.LocalizationSourceName,
                    CountrySubdivisionTranslation.EntityError.CountrySubdivisionTranslationOutOfBoundLanguageIsoCode,
                    w => string.IsNullOrWhiteSpace(w.LanguageIsoCode) || w.LanguageIsoCode.Length > 0 && w.LanguageIsoCode.Length <= 5));

                AddSpecification(new ExpressionSpecification<CountrySubdivisionTranslation>(
                    AppConsts.LocalizationSourceName,
                    CountrySubdivisionTranslation.EntityError.CountrySubdivisionTranslationMustHaveName,
                    w => !string.IsNullOrWhiteSpace(w.Name)));

                AddSpecification(new ExpressionSpecification<CountrySubdivisionTranslation>(
                    AppConsts.LocalizationSourceName,
                    CountrySubdivisionTranslation.EntityError.CountrySubdivisionTranslationOutOfBoundName,
                    w => string.IsNullOrWhiteSpace(w.Name) || w.Name.Length > 0 && w.Name.Length <= 50));

                AddSpecification(new ExpressionSpecification<CountrySubdivisionTranslation>(
                    AppConsts.LocalizationSourceName,
                    CountrySubdivisionTranslation.EntityError.CountrySubdivisionTranslationMustHaveCountrySubdivisionId,
                    w => w.CountrySubdivisionId != default(int)));

                AddSpecification(new ExpressionSpecification<CountrySubdivisionTranslation>(
                    AppConsts.LocalizationSourceName,
                    CountrySubdivisionTranslation.EntityError.CountrySubdivisionTranslationOutOfBoundNationality,
                    w => string.IsNullOrWhiteSpace(w.Nationality) || w.Nationality.Length > 0 && w.Nationality.Length <= 100));

            }
        }
    }
}
