﻿using System;
using Tnf.Notifications;

namespace Thex.Location.Infra.Entities
{
    public partial class LocationEntity : IEntityInt
    {
        public int Id { get; set; }
        public Guid OwnerId { get; internal set; }
        public int LocationCategoryId { get; internal set; }
        public decimal Latitude { get; internal set; }
        public decimal Longitude { get; internal set; }
        public string StreetName { get; internal set; }
        public string StreetNumber { get; internal set; }
        public string AdditionalAddressDetails { get; internal set; }
        public string Neighborhood { get; internal set; }
        public int CityId { get; internal set; }
        public int? StateId { get; internal set; }
        public int? CountryId { get; internal set; }
        public string PostalCode { get; internal set; }
        public string CountryCode { get; internal set; }

        public virtual Property Property { get; internal set; }

        public virtual CountrySubdivision City { get; internal set; }
        public virtual CountrySubdivision State { get; internal set; }
        public virtual CountrySubdivision CountryEntity { get; internal set; }
        public virtual LocationCategory LocationCategory { get; internal set; }

        public enum EntityError
        {
            LocationMustHaveLocationCategoryId,
            LocationMustHaveStreetName,
            LocationOutOfBoundStreetName,
            LocationMustHaveStreetNumber,
            LocationOutOfBoundStreetNumber,
            LocationOutOfBoundAdditionalAddressDetails,
            LocationOutOfBoundNeighborhood,
            LocationMustHaveCityId,
            LocationMustHaveCountryCode,
            LocationOutOfBoundCountryCode
        }

        public static Builder Create(INotificationHandler handler)
            => new Builder(handler);

        public static Builder Create(INotificationHandler handler, LocationEntity instance)
            => new Builder(handler, instance);
    }
}
