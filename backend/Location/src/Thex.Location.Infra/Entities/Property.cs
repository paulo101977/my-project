﻿using System;
using System.Collections.Generic;
using System.Text;
using Thex.Common.Enumerations;
using Tnf.Notifications;

namespace Thex.Location.Infra.Entities
{
    public partial class Property
    {
        public int Id { get; set; }
        public int CompanyId { get; internal set; }
        public int BrandId { get; internal set; }
        public string Name { get; internal set; }
        public Guid PropertyUId { get; internal set; }
        public int PropertyStatusId { get; internal set; }

        public virtual ICollection<LocationEntity> LocationList { get; internal set; }

        private PropertyTypeEnum _propertyTypeId;
        public PropertyTypeEnum PropertyTypeIdValue
        {
            get
            {
                _propertyTypeId = ((PropertyTypeEnum)PropertyTypeIdValue);
                return _propertyTypeId;
            }
            internal set
            {
                _propertyTypeId = value;
                PropertyTypeId = ((int)value);
            }
        }

        public int PropertyTypeId
        {
            get;
            internal set;
        }

        public enum EntityError
        {
            PropertyMustHaveCompanyId,
            PropertyMustHavePropertyTypeId,
            PropertyMustHaveBrandId,
            PropertyMustHaveName,
            PropertyOutOfBoundName,
            PropertyInvalidCreationTime,
            PropertyInvalidLastModificationTime,
            PropertyInvalidDeletionTime,
            PropertyMustHaveTypeId
        }

        public static Builder Create(INotificationHandler handler)
            => new Builder(handler);

        public static Builder Create(INotificationHandler handler, Property instance)
            => new Builder(handler, instance);
    }
}
