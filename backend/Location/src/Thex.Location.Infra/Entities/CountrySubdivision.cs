﻿using System.Collections.Generic;
using Tnf.Notifications;

namespace Thex.Location.Infra.Entities
{
    public partial class CountrySubdivision : IEntityInt
    {
        public CountrySubdivision()
        {
            CountrySubdivisionTranslationList = new HashSet<CountrySubdivisionTranslation>();
        }

        public int Id { get; set; }
        public string TwoLetterIsoCode { get; internal set; }
        public string ThreeLetterIsoCode { get; internal set; }
        public string CountryTwoLetterIsoCode { get; internal set; }
        public string CountryThreeLetterIsoCode { get; internal set; }
        public string ExternalCode { get; internal set; }
        public int? ParentSubdivisionId { get; internal set; }
        public int SubdivisionTypeId { get; internal set; }

        public virtual CountrySubdivision ParentSubdivision { get; internal set; }
        public virtual ICollection<CountrySubdivisionTranslation> CountrySubdivisionTranslationList { get; internal set; }

        public virtual ICollection<LocationEntity> LocationList { get; internal set; }
        public virtual ICollection<LocationEntity> LocationStateList { get; internal set; }
        public virtual ICollection<LocationEntity> LocationCountryList { get; internal set; }

        public enum EntityError
        {
            CountrySubdivisionOutOfBoundTwoLetterIsoCode,
            CountrySubdivisionOutOfBoundThreeLetterIsoCode,
            CountrySubdivisionOutOfBoundCountryTwoLetterIsoCode,
            CountrySubdivisionOutOfBoundCountryThreeLetterIsoCode,
            CountrySubdivisionOutOfBoundExternalCode,
            CountrySubdivisionOutOfBoundOfficialNatural,
            CountrySubdivisionOutOfBoundOfficialLegal
        }

        public static Builder Create(INotificationHandler handler)
            => new Builder(handler);

        public static Builder Create(INotificationHandler handler, CountrySubdivision instance)
            => new Builder(handler, instance);
    }
}
