﻿using Thex.Location.Infra.Context;
using Thex.Location.Infra.Entities;
using Thex.Location.Infra.Interfaces;
using Tnf.EntityFrameworkCore;
using Tnf.EntityFrameworkCore.Repositories;

namespace Thex.Location.Infra.Repositories
{
    public class CountrySubdivisionTranslationRepository : EfCoreRepositoryBase<ThexLocationContext, CountrySubdivisionTranslation>, ICountrySubdivisionTranslationRepository
    {
        public CountrySubdivisionTranslationRepository(IDbContextProvider<ThexLocationContext> dbContextProvider)
          : base(dbContextProvider)
        {
        }

        public new CountrySubdivisionTranslation InsertAndSaveChanges(CountrySubdivisionTranslation countrySubdivisionTranslation)
        {
            Context.CountrySubdivisionTranslations.Add(countrySubdivisionTranslation);
            Context.SaveChanges();

            return countrySubdivisionTranslation;
        }
    }
}
