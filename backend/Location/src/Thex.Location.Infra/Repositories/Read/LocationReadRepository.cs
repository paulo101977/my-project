﻿using System;
using System.Collections.Generic;
using System.Linq;
using Thex.Common;
using Thex.Common.Dto;
using Thex.Common.Dto.Location;
using Thex.Common.Enumerations;
using Thex.Location.Infra.Context;
using Thex.Location.Infra.Entities;
using Thex.Location.Infra.Interfaces.Read;
using Tnf.EntityFrameworkCore;
using Tnf.EntityFrameworkCore.Repositories;
using Tnf.Localization;

namespace Thex.Location.Infra.Repositories.Read
{
    public class LocationReadRepository : EfCoreRepositoryBase<ThexLocationContext, LocationEntity>, ILocationReadRepository
    {
        private readonly ILocalizationManager _localizationManager;

        public LocationReadRepository(IDbContextProvider<ThexLocationContext> dbContextProvider, ILocalizationManager localizationManager)
            : base(dbContextProvider)
        {
            _localizationManager = localizationManager;
        }

        public IList<LocationDto> GetLocationDtoListByPersonId(Guid owerId, string languageIsoCode)
        {
            var locationDtoListBaseQuery = (from location in Context.Locations
                                            join city in Context.CountrySubdivisions on location.CityId equals city.Id
                                            join state in Context.CountrySubdivisions on city.ParentSubdivision.Id equals state.Id
                                            join country in Context.CountrySubdivisions on state.ParentSubdivision.Id equals country.Id
                                            join cityTranslation in Context.CountrySubdivisionTranslations on city.Id equals cityTranslation.CountrySubdivision.Id
                                            join stateTranslation in Context.CountrySubdivisionTranslations on state.Id equals stateTranslation.CountrySubdivision.Id
                                            join countryTranslation in Context.CountrySubdivisionTranslations on country.Id equals countryTranslation.CountrySubdivision.Id
                                            join locationCategory in Context.LocationCategories on location.LocationCategoryId equals locationCategory.Id
                                            where
                                            cityTranslation.LanguageIsoCode.ToLower() == languageIsoCode &&
                                            stateTranslation.LanguageIsoCode.ToLower() == languageIsoCode &&
                                            countryTranslation.LanguageIsoCode.ToLower() == languageIsoCode &&
                                            location.OwnerId == owerId
                                            select new LocationDto
                                            {
                                                OwnerId = location.OwnerId,
                                                LocationCategoryId = location.LocationCategoryId,
                                                Latitude = location.Latitude,
                                                Longitude = location.Longitude,
                                                StreetName = location.StreetName,
                                                StreetNumber = location.StreetNumber,
                                                AdditionalAddressDetails = location.AdditionalAddressDetails,
                                                PostalCode = location.PostalCode,
                                                CountryCode = location.CountryCode,
                                                Neighborhood = location.Neighborhood,
                                                Division = stateTranslation.Name,
                                                Subdivision = cityTranslation.Name,
                                                CityId = cityTranslation.Id,
                                                Country = countryTranslation.Name,
                                                Id = location.Id,

                                                LocationCategory = new LocationCategoryDto
                                                {
                                                    Id = locationCategory.Id
                                                }
                                            }).ToList();

            foreach (var location in locationDtoListBaseQuery)
                location.LocationCategory.Name = _localizationManager.GetString(AppConsts.LocalizationSourceName, ((LocationCategoryEnum)location.LocationCategory.Id).ToString());

            return locationDtoListBaseQuery.ToList();
        }

        public LocationDto GetLocationDtoListByPropertyUId(int pPropertyId)
        {
            var locationDtoListBaseQuery = (from location in Context.Locations
                                            join properties in Context.Properties on location.OwnerId equals properties.PropertyUId
                                            where
                                            properties.Id == pPropertyId
                                            select new LocationDto
                                            {
                                                OwnerId = location.OwnerId,
                                                LocationCategoryId = location.LocationCategoryId,
                                                Latitude = location.Latitude,
                                                Longitude = location.Longitude,
                                                StreetName = location.StreetName,
                                                StreetNumber = location.StreetNumber,
                                                AdditionalAddressDetails = location.AdditionalAddressDetails,
                                                PostalCode = location.PostalCode,
                                                CountryCode = location.CountryCode,
                                                Neighborhood = location.Neighborhood,
                                                Country = location.CountryCode,
                                                Id = location.Id
                                            }).FirstOrDefault();

            return locationDtoListBaseQuery;
        }

        public LocationEntity GetById(int id)
        {
            var location = Context.Locations.FirstOrDefault(x => x.Id == id);

            return location;
        }

        public LocationPropertyDto GetCountryAndDistrictCodeByPropertyId(int propertyId)
        {
            return (from property in Context.Properties
                    join location in Context.Locations on property.PropertyUId equals location.OwnerId
                    join countrySubdivision in Context.CountrySubdivisions on location.StateId equals countrySubdivision.Id into
                    lfCountrySubdivision
                    from countrySubdivisionLeft in lfCountrySubdivision.DefaultIfEmpty()
                    where property.Id == propertyId
                    select new LocationPropertyDto
                    {
                        PropertyCountryCode = location.CountryCode,
                        PropertyDistrictCode = countrySubdivisionLeft == null ? string.Empty : countrySubdivisionLeft.TwoLetterIsoCode
                    }
                ).FirstOrDefault();
        }
    }
}
