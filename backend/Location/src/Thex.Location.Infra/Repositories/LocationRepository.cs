﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using Thex.Location.Infra.Context;
using Thex.Location.Infra.Entities;
using Thex.Location.Infra.Interfaces;
using Tnf.EntityFrameworkCore;
using Tnf.EntityFrameworkCore.Repositories;

namespace Thex.Location.Infra.Repositories
{
    public class LocationRepository : EfCoreRepositoryBase<ThexLocationContext, LocationEntity>, ILocationRepository
    {
        public LocationRepository(IDbContextProvider<ThexLocationContext> dbContextProvider)
           : base(dbContextProvider)
        {
        }

        public void AddRange(IList<LocationEntity> locationList)
        {
            Context.Locations.AddRange(locationList);
            Context.SaveChanges();
        }

        public void UpdateLocationList(IList<LocationEntity> locationList, Guid ownerId)
        {
            var existingLocations = Context.Locations
                           .Where(exp => exp.OwnerId == ownerId)
                           .ToList();
             
            if (existingLocations != null)
            {
                RemoveLocations(locationList, existingLocations);

                foreach (var location in locationList)
                    InsertOrUpdateLocation(ownerId, existingLocations, location);
            }

            Context.SaveChanges();
        }

        private void InsertOrUpdateLocation(Guid ownerId, List<LocationEntity> existingLocations, LocationEntity location)
        {
            var existingLocation = existingLocations
                                   .Where(d => d.Id == location.Id && d.Id != 0)
                                   .FirstOrDefault();

            location.OwnerId = ownerId;

            if (existingLocation != null)
            {
                Context.Entry(existingLocation).State = EntityState.Modified;
                Context.Entry(existingLocation).CurrentValues.SetValues(location);
                Context.Locations.Update(existingLocation);
            }else
                Context.Locations.Add(location);

            Context.SaveChanges();

        }

        private void RemoveLocations(IList<LocationEntity> locationList, List<LocationEntity> existingLocations)
        {
            var locationIds = locationList.Select(e => e.Id).ToList();
            var locationListToExclude = existingLocations.Where(d => !locationIds.Contains(d.Id)).ToList();

            if (locationListToExclude.Count > 0)
                Context.RemoveRange(locationListToExclude);

            Context.SaveChanges();
        }

        public void CreateorUpdateLocationGuestRegistration(LocationEntity location, Guid ownerId)
        {
            var existingLocation = Context.Locations
                           .Where(exp => exp.OwnerId == ownerId)
                           .FirstOrDefault();


            location.OwnerId = ownerId;

            if (existingLocation != null)
            {
                location.Id = existingLocation.Id;
                Context.Entry(existingLocation).State = EntityState.Modified;
                Context.Entry(existingLocation).CurrentValues.SetValues(location);
            }
            else
                Context.Locations.Add(location);

            Context.SaveChanges();
        }

        public LocationEntity InsertLocation(LocationEntity location)
        {
            Context.Locations.Add(location);
            Context.SaveChanges();

            return location;
        }

        public void UpdateLocation(LocationEntity location)
        {
            var locationOld = Context.Locations.FirstOrDefault(x => x.Id == location.Id);
            if (locationOld != null)
            {
                Context.Entry(locationOld).State = EntityState.Modified;
                Context.Entry(locationOld).CurrentValues.SetValues(location);
            }

            Context.SaveChanges();
        }

        public void DeleteLocation(LocationEntity location)
        {
            Context.Locations.Remove(location);
            Context.SaveChanges();
        }
    }
}
