﻿using Thex.Location.Infra.Context;
using Thex.Location.Infra.Entities;
using Thex.Location.Infra.Interfaces;
using Tnf.EntityFrameworkCore;
using Tnf.EntityFrameworkCore.Repositories;

namespace Thex.Location.Infra.Repositories
{
    public class CountrySubdivisionRepository : EfCoreRepositoryBase<ThexLocationContext, CountrySubdivision>, ICountrySubdivisionRepository
    {
        public CountrySubdivisionRepository(IDbContextProvider<ThexLocationContext> dbContextProvider)
          : base(dbContextProvider)
        {
        }

        public new CountrySubdivision InsertAndSaveChanges(CountrySubdivision countrySubdivision)
        {
            Context.CountrySubdivisions.Add(countrySubdivision);
            Context.SaveChanges();

            return countrySubdivision;
        }
    }
}
