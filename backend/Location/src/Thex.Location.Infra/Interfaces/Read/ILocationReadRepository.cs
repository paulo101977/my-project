﻿using System;
using System.Collections.Generic;
using Thex.Common.Dto;
using Thex.Common.Dto.Location;
using Thex.Location.Infra.Entities;

namespace Thex.Location.Infra.Interfaces.Read
{
    public interface ILocationReadRepository
    {
        LocationEntity GetById(int id);
        LocationPropertyDto GetCountryAndDistrictCodeByPropertyId(int propertyId);
        IList<LocationDto> GetLocationDtoListByPersonId(Guid owerId, string languageIsoCode);
    }
}
