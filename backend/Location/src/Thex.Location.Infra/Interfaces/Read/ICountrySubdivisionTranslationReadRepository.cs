﻿using System.Collections.Generic;
using Thex.Common.Dto;
using Thex.Location.Dto.GetAll;
using Tnf.Dto;

namespace Thex.Location.Infra.Interfaces.Read
{
    public interface ICountrySubdivisionTranslationReadRepository
    {
        AddressTranslationDto GetAddressTranslationByCityId(int cityId, string languageIsoCode);

        IListDto<CountryDto> GetAllCountriesByLanguageIsoCode(GetAllCountryDto request);

        List<BaseCountrySubdivisionTranslationDto> GetCountryWithTranslations(string countryTwoLetterIsoCode);

        List<BaseCountrySubdivisionTranslationDto> GetStateWithTranslations(string translationName, int parentSubdivionId);
        List<BaseCountrySubdivisionTranslationDto> GetStateWithTranslationsByCode(string twoLetterIsoCode, int parentSubdivionId);

        List<BaseCountrySubdivisionTranslationDto> GetCityWithTranslations(string translationName, int parentSubdivionId);

        List<BaseCountrySubdivisionTranslationDto> GetCountrySubdivisionWithTranslations(int subdivisionId);

        IListDto<CountrySubdivisionTranslationDto> GetNationalities(GetAllCountrySubdivisionTranslationDto request);

        CountrySubdivisionTranslationDto GetNationalityByIdAndProperty(int nationalityId, int propertyId);
    }
}
