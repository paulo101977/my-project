﻿using Thex.Location.Infra.Entities;

namespace Thex.Location.Infra.Interfaces
{
    public interface ICountrySubdivisionRepository
    {
        CountrySubdivision InsertAndSaveChanges(CountrySubdivision countrySubdivision);
    }
}
