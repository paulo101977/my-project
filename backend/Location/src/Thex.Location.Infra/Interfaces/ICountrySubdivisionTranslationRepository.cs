﻿using Thex.Location.Infra.Entities;

namespace Thex.Location.Infra.Interfaces
{
    public interface ICountrySubdivisionTranslationRepository
    {
        CountrySubdivisionTranslation InsertAndSaveChanges(CountrySubdivisionTranslation countrySubdivisionTranslation);
    }
}
