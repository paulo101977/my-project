﻿using System;
using System.Collections.Generic;
using System.Text;
using Thex.Location.Infra.Entities;

namespace Thex.Location.Infra.Interfaces
{
    public interface ILocationRepository
    {
        void AddRange(IList<LocationEntity> locationList);
        void UpdateLocationList(IList<LocationEntity> locationList, Guid ownerId);
        void CreateorUpdateLocationGuestRegistration(LocationEntity location, Guid ownerId);
        LocationEntity InsertLocation(LocationEntity location);
        void UpdateLocation(LocationEntity location);
        void DeleteLocation(LocationEntity location);
    }
}
