﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using Thex.Location.Infra.Entities;

namespace Thex.Location.Infra.Mappers
{
    public class PropertyMapper : IEntityTypeConfiguration<Property>
    {
        public void Configure(EntityTypeBuilder<Property> builder)
        {

            builder.ToTable("Property");

            builder.HasAnnotation("Relational:TableName", "Property");

            builder.HasIndex(e => e.BrandId)
                .HasName("x_Property_BrandId");

            builder.HasIndex(e => e.CompanyId)
                .HasName("x_Property_CompanyId");

            builder.HasIndex(e => e.PropertyTypeId)
                .HasName("x_Property_PropertyTypeId");

            builder.HasIndex(e => e.PropertyUId)
                .HasName("UK_Property_PropertyUId")
                .IsUnique();

            builder.HasKey(e => e.Id);
            builder.Property(e => e.Id).HasAnnotation("Relational:ColumnName", "PropertyId");

            //builder.Property(e => e.BrandId).HasAnnotation("Relational:ColumnName", "BrandId");

            //builder.Property(e => e.CompanyId).HasAnnotation("Relational:ColumnName", "CompanyId");

            builder.Property(e => e.PropertyStatusId)
                .IsRequired()
                .HasDefaultValueSql("((12))")
                .HasAnnotation("Relational:ColumnName", "PropertyStatusId");

            builder.Property(e => e.Name)
                .IsRequired()
                .HasMaxLength(200)
                .IsUnicode(false)
                .HasAnnotation("Relational:ColumnName", "Name");

            //builder.Property(e => e.PropertyTypeId).HasAnnotation("Relational:ColumnName", "PropertyTypeId");

            builder.Ignore(e => e.PropertyTypeIdValue);

            builder.Property(e => e.PropertyUId)
                .HasDefaultValueSql("(newid())")
                .HasAnnotation("Relational:ColumnName", "PropertyUId");

        }
    }
}
