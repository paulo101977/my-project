﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Thex.Location.Infra.Entities;

namespace Thex.Location.Infra.Mappers
{
    public class CountrySubdivisionMapper : IEntityTypeConfiguration<CountrySubdivision>
    {
        public void Configure(EntityTypeBuilder<CountrySubdivision> builder)
        {

            builder.ToTable("CountrySubdivision");

            builder.HasAnnotation("Relational:TableName", "CountrySubdivision");

            builder.HasIndex(e => e.ParentSubdivisionId)
                .HasName("x_CountrySubdivision_ParentSubdivisionId");

            builder.Property(e => e.Id).HasAnnotation("Relational:ColumnName", "CountrySubdivisionId");

            builder.Property(e => e.CountryThreeLetterIsoCode)
                .HasColumnType("char(3)")
                .HasAnnotation("Relational:ColumnName", "CountryThreeLetterIsoCode");

            builder.Property(e => e.CountryTwoLetterIsoCode)
                .HasColumnType("char(2)")
                .HasAnnotation("Relational:ColumnName", "CountryTwoLetterIsoCode");

            builder.Property(e => e.ExternalCode)
                .HasMaxLength(20)
                .IsUnicode(false)
                .HasAnnotation("Relational:ColumnName", "ExternalCode");

            builder.Property(e => e.ParentSubdivisionId).HasAnnotation("Relational:ColumnName", "ParentSubdivisionId");

            builder.Property(e => e.SubdivisionTypeId).HasAnnotation("Relational:ColumnName", "SubdivisionTypeId");

            builder.Property(e => e.ThreeLetterIsoCode)
                .HasColumnType("char(3)")
                .HasAnnotation("Relational:ColumnName", "ThreeLetterIsoCode");

            builder.Property(e => e.TwoLetterIsoCode)
                .HasColumnType("char(2)")
                .HasAnnotation("Relational:ColumnName", "TwoLetterIsoCode");

        }
    }    
}
