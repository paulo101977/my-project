﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Thex.Location.Infra.Entities;

namespace Thex.Location.Infra.Mappers
{
    public class CountryLanguageMapper : IEntityTypeConfiguration<CountryLanguage>
    {
        public void Configure(EntityTypeBuilder<CountryLanguage> builder)
        {

            builder.ToTable("CountryLanguage");

            builder.HasAnnotation("Relational:TableName", "CountryLanguage");

            builder.HasIndex(e => e.CountrySubdivisionId)
                .HasName("x_CountryLanguage_CountrySubdivisionId");

            builder.Property(e => e.Id).HasAnnotation("Relational:ColumnName", "CountryLanguageId");

            builder.Property(e => e.CountrySubdivisionId).HasAnnotation("Relational:ColumnName", "CountrySubdivisionId");

            builder.Property(e => e.CultureInfoCode)
                .HasMaxLength(20)
                .IsUnicode(false)
                .HasAnnotation("Relational:ColumnName", "CultureInfoCode");

            builder.Property(e => e.Language)
                .IsRequired()
                .HasMaxLength(60)
                .IsUnicode(false)
                .HasAnnotation("Relational:ColumnName", "Language");

            builder.Property(e => e.LanguageThreeLetterIsoCode)
                .HasColumnType("char(3)")
                .HasAnnotation("Relational:ColumnName", "LanguageThreeLetterIsoCode");

            builder.Property(e => e.LanguageTwoLetterIsoCode)
                .HasColumnType("char(2)")
                .HasAnnotation("Relational:ColumnName", "LanguageTwoLetterIsoCode");
        }
    }
}
