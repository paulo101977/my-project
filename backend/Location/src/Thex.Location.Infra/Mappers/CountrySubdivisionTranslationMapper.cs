﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Thex.Location.Infra.Entities;

namespace Thex.Location.Infra.Mappers
{
    public class CountrySubdivisionTranslationMapper : IEntityTypeConfiguration<CountrySubdivisionTranslation>
    {
        public void Configure(EntityTypeBuilder<CountrySubdivisionTranslation> builder)
        {

            builder.ToTable("CountrySubdivisionTranslation");

            builder.HasAnnotation("Relational:TableName", "CountrySubdivisionTranslation");

            builder.HasIndex(e => e.CountrySubdivisionId)
                .HasName("x_CountrySubdivisionTranslation_CountrySubdivisionId");

            builder.Property(e => e.Id).HasAnnotation("Relational:ColumnName", "CountrySubdivisionTranslationId");

            builder.Property(e => e.CountrySubdivisionId).HasAnnotation("Relational:ColumnName", "CountrySubdivisionId");

            builder.Property(e => e.LanguageIsoCode)
                .IsRequired()
                .HasMaxLength(5)
                .IsUnicode(false)
                .HasAnnotation("Relational:ColumnName", "LanguageIsoCode");

            builder.Property(e => e.Name)
                .IsRequired()
                .HasMaxLength(50)
                .IsUnicode(false)
                .HasAnnotation("Relational:ColumnName", "Name");

            builder.Property(e => e.Nationality)
                .HasMaxLength(100)
                .IsUnicode(false)
                .HasAnnotation("Relational:ColumnName", "Nationality");

            builder.HasOne(d => d.CountrySubdivision)
                .WithMany(p => p.CountrySubdivisionTranslationList)
                .HasForeignKey(d => d.CountrySubdivisionId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_CountrySubdivisionTranslation_CountrySubdivision");

        }
    }
}
