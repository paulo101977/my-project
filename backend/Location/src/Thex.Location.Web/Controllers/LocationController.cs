﻿using Microsoft.AspNetCore.Mvc;
using Thex.Application.Interfaces;
using Thex.Common.Security;

namespace Thex.Location.Web.Controllers
{
    [Route(RouteConsts.Location)]
    public class LocationController : ThexAppController
    {
        private readonly ILocationAppService _locationAppService;

        public LocationController(
            IApplicationUser applicationUser,
            ILocationAppService locationAppService)
            : base(applicationUser)
        {
            _locationAppService = locationAppService;
        }


    }
}