﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Thex.Common.Security;

namespace Thex.Location.Web.Controllers
{
    [Authorize("Bearer")]
    public class ThexAppController : TnfController
    {
        protected IApplicationUser _applicationUser;

        public ThexAppController(IApplicationUser applicationUser)
        {
            _applicationUser = applicationUser;
        }

        public IApplicationUser ApplicationUser
        {
            get { return _applicationUser; }
            set { _applicationUser = value; }
        }
    }
}