﻿using System;
using System.Threading.Tasks;
using CurrencyExchangeWebJob.Infra.Application;
using Microsoft.Azure.WebJobs;

namespace CurrencyExchangeWebJob
{
    // To learn more about Microsoft Azure WebJobs SDK, please see https://go.microsoft.com/fwlink/?LinkID=320976
    class Program
    {
        // Please set the following connection strings in app.config for this WebJob to run:
        // AzureWebJobsDashboard and AzureWebJobsStorage
        static void Main()
        {
            Console.WriteLine($"Importação iniciada {DateTime.UtcNow}");

            try
            {
                var rates = new CurrencyExchangeAppService();
                rates.GetCurrencyExchangeRate().ConfigureAwait(false).GetAwaiter().GetResult();

                Console.WriteLine("Importação finalizada");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine("Importação finalizada com erros");
            }
        }
    }
}
