﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CurrencyExchangeWebJob.Infra.Entities
{
    [Table("Currency")]
    public class Currency
    {
        [Key]
        public Guid CurrencyId { get; set; }

        public int CountrySubdivisionId { get; set; }

        public string TwoLetterIsoCode { get; set; }

        public string CurrencyName { get; set; }

        public string AlphabeticCode { get; set; }

        public string NumnericCode { get; set; }

        public int MinorUnit { get; set; }

        public string Symbol { get; set; }

        public decimal ExchangeRate { get; set; }

        public bool IsDeleted { get; set; }

        public DateTime CreationTime { get; set; }

        public Guid? CreatorUserId { get; set; }

        public DateTime? LastModificationTime { get; set; }

        public Guid? LastModifierUserId { get; set; }

        public DateTime? DeletionTime { get; set; }

        public Guid? DeleterUserId { get; set; }

        public ICollection<CurrencyExchange> CurrencyExchanges { get; set; }
    }
}
