﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CurrencyExchangeWebJob.Infra.Entities
{
    [Table("CurrencyExchange")]
    public class CurrencyExchange
    {
        [Key]
        public Guid CurrencyExchangeId { get; set; }

        public DateTime CurrencyExchangeDate { get; set; }

        public decimal ExchangeRate { get; set; }

        public bool IsDeleted { get; set; }

        public DateTime CreationTime { get; set; }

        public int? CreatorUserId { get; set; }

        public DateTime? LastModificationTime { get; set; }

        public int? LastModifierUserId { get; set; }

        public DateTime? DeletionTime { get; set; }

        public int? DeleterUserId { get; set; }

        [ForeignKey("Currency")]
        public Guid CurrencyId { get; set; }

        public Currency Currency { get; set; }

    }
}
