﻿using CurrencyExchangeWebJob.Infra.Entities;
using System;
using System.Data.Entity;

namespace CurrencyExchangeWebJob.Infra
{
    public class CurrencyExchangeDbContext : DbContext
    {
        public CurrencyExchangeDbContext(): base("DB")
        {
            Database.SetInitializer<CurrencyExchangeDbContext>(null);
        }

        public virtual DbSet<CurrencyExchange> CurrencyExchange { get; set; }
        public virtual DbSet<Currency> Currency { get; set; }
        
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            var instance = System.Data.Entity.SqlServer.SqlProviderServices.Instance;
        }
    }
}
