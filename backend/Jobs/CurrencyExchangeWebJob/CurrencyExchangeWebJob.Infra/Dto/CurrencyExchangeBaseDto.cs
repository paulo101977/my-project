﻿namespace CurrencyExchangeWebJob.Infra.Dto
{
    public class CurrencyExchangeBaseDto
    {
        public string Base { get; set; }
        public RatesDto Rates { get; set; }
    }
}
