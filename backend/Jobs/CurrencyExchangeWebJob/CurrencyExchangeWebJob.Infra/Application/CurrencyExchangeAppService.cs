﻿using CurrencyExchangeWebJob.Infra.Dto;
using CurrencyExchangeWebJob.Infra.Entities;
using Newtonsoft.Json;
using System;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace CurrencyExchangeWebJob.Infra.Application
{
    public class CurrencyExchangeAppService
    {
        public async Task GetCurrencyExchangeRate()
        {
            var client = new HttpClient();

            var serviceUrl = ConfigurationManager.AppSettings["ServiceUrl"];

            var response = await client.GetStringAsync(serviceUrl);

            if (response != null)
            {
                var cotation = JsonConvert.DeserializeObject<CurrencyExchangeBaseDto>(response);

                GetCurrencyByCode(cotation);
            }
        }

        private void GetCurrencyByCode(CurrencyExchangeBaseDto cotation)
        {
            using (var context = new CurrencyExchangeDbContext())
            {
                var currencyList = context.Currency.ToList();

                foreach (var currency in currencyList)
                {
                    var currencyExchange = new CurrencyExchange()
                    {
                        CurrencyExchangeId = Guid.NewGuid(),
                        CurrencyExchangeDate = DateTime.UtcNow,
                        CurrencyId = currency.CurrencyId,
                        CreationTime = DateTime.UtcNow,
                        IsDeleted = false
                    };

                    try
                    {
                        currencyExchange.ExchangeRate = (decimal)cotation.Rates[currency.AlphabeticCode];
                    }
                    catch
                    {
                        Console.WriteLine($"Moeda não existente {currency.AlphabeticCode}");
                    }

                    currency.ExchangeRate = currencyExchange.ExchangeRate;

                    context.CurrencyExchange.Add(currencyExchange);
                }

                context.SaveChanges();
            }
        }

    }
}
