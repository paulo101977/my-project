﻿using ChannelCodeListUpdateWebJob.Infra.Dto;
using ChannelCodeListUpdateWebJob.Infra.Entities;
using ChannelCodeListUpdateWebJob.Infra.Enum;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace ChannelCodeListUpdateWebJob.Infra.Application
{
    public class ChannelCodeAppService
    {
        public async Task GetChannelCodeList()
        {
            var authorization = GetAuthorization();
            if (authorization != null)
            {
                HttpClientHandler handler = new HttpClientHandler() { AutomaticDecompression = System.Net.DecompressionMethods.GZip | System.Net.DecompressionMethods.Deflate };
                var client = new HttpClient(handler);

                client.DefaultRequestHeaders.TryAddWithoutValidation("token-client", authorization.TokenClient);
                client.DefaultRequestHeaders.TryAddWithoutValidation("token-application", authorization.TokenApplication);
                client.DefaultRequestHeaders.TryAddWithoutValidation("Accept-Encoding", "gzip,deflate");
                client.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "application/json;charset=utf-8");
                var serviceUrl = ConfigurationManager.AppSettings["ServiceUrl"];
                var response = await client.GetStringAsync(serviceUrl);
                if (response != null)
                {
                    try
                    {
                        var channelCodeList = JsonConvert.DeserializeObject<ChannelCodeDto>(response);
                        if (channelCodeList != null && channelCodeList.SourceOfBusinessList.Any())
                            UpdateChannelCodeList(channelCodeList);
                    }
                    catch (Exception ex)
                    {
                    }
                }
            }
        }

        private void UpdateChannelCodeList(ChannelCodeDto channelCodeDtoList)
        {
            using (var context = new ChannelCodeListUpdateWebJobDbContext())
            {
                var channelCodeOriginal = context.ChannelCode.AsNoTracking().ToList();
                int countNewChannelCode = 0;
                var channelCodeList = new List<ChannelCode>();
                foreach (var channelCodeDto in channelCodeDtoList.SourceOfBusinessList)
                {
                    if (!channelCodeOriginal.Any(x => x.ChannelCode1.Equals(channelCodeDto.Source)) &&
                        !channelCodeList.Any(x => x.ChannelCode1.Equals(channelCodeDto.Source)))
                    {
                        channelCodeList.Add(new ChannelCode()
                        {
                            ChannelCode1 = channelCodeDto.Source,
                            ChannelCodeDescription = channelCodeDto.Name
                        });
                        countNewChannelCode++;
                    }
                }

                if(channelCodeList.Count > 0)
                    context.ChannelCode.AddRange(channelCodeList);

                context.SaveChanges();
                Console.WriteLine($"Channel codes adicionados: {countNewChannelCode}");
            }
        }

        private IntegrationPartner GetAuthorization()
        {
            using (var context = new ChannelCodeListUpdateWebJobDbContext())
            {
                var authorization = context.IntegrationPartner.FirstOrDefault(x => x.IntegrationPartnerType == (int)IntegrationPartnerPartnerType.Higs);
                return authorization;
            }
        }
    }
}
