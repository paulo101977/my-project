﻿using ChannelCodeListUpdateWebJob.Infra.Entities;
using System;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace ChannelCodeListUpdateWebJob.Infra
{
    public class ChannelCodeListUpdateWebJobDbContext : DbContext
    {
        public ChannelCodeListUpdateWebJobDbContext(): base("DB")
        {
            Database.SetInitializer<ChannelCodeListUpdateWebJobDbContext>(null);
        }

        public virtual DbSet<ChannelCode> ChannelCode { get; set; }
        public virtual DbSet<IntegrationPartner> IntegrationPartner { get; set; }
        
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            var instance = System.Data.Entity.SqlServer.SqlProviderServices.Instance;
        }
    }
}
