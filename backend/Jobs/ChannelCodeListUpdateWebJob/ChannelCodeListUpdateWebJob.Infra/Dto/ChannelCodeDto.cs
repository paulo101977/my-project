﻿using System.Collections.Generic;

namespace ChannelCodeListUpdateWebJob.Infra.Dto
{
    public class ChannelCodeDto
    {
        public List<SourceOfBusinessList> SourceOfBusinessList { get; set; }
    }

    public class SourceOfBusinessList
    {
        public string Source { get; set; }
        public string Name { get; set; }
    }
}
