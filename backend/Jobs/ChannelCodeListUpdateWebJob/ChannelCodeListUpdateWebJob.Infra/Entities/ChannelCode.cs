﻿
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ChannelCodeListUpdateWebJob.Infra.Entities
{
    public class ChannelCode
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ChannelCodeId { get; set; }
        [Column("ChannelCode")]
        public string ChannelCode1 { get; set; }
        public string ChannelCodeDescription { get; set; }
    }
}
