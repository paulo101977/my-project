﻿using System.ComponentModel.DataAnnotations;


namespace ChannelCodeListUpdateWebJob.Infra.Entities
{
    public class IntegrationPartner
    {
        [Key]
        public int IntegrationPartnerID { get; set; }
        public string TokenClient { get; set; }
        public string TokenApplication { get; set; }
        public string IntegrationPartnerName { get; set; }
        public int IntegrationPartnerType { get; set; }

    }
}
