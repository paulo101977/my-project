﻿using System;
using UpdateInventoryWebJob.Infra.Application;

namespace UpdateInventoryWebJob
{
    // To learn more about Microsoft Azure WebJobs SDK, please see https://go.microsoft.com/fwlink/?LinkID=320976
    class Program
    {
        // Please set the following connection strings in app.config for this WebJob to run:
        // AzureWebJobsDashboard and AzureWebJobsStorage
        static void Main()
        {
            Console.WriteLine($"Iniciando atualização de inventário {DateTime.UtcNow}");

            try
            {
                new UpdateInventoryAppService().Execute();

                Console.WriteLine("Inventário atualizado");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine("Inventário finalizado com erros");
            }
        }
    }
}
