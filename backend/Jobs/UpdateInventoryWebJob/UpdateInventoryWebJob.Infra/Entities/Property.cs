﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace UpdateInventoryWebJob.Infra.Entities
{
    [Table("Property")]
    public class Property
    {
        [Key]
        public int PropertyId { get; set; }

        public string Name { get; set; }
    }
}
