﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace UpdateInventoryWebJob.Infra.Entities
{
    [Table("RoomTypeInventory")]
    public class RoomTypeInventory
    {
        [Key]
        public Guid RoomTypeInventoryId { get; set; }
        public int PropertyId { get; set; }
        public int RoomTypeId { get; set; }
        public int Total { get; set; }
        public int Balance { get; set; }
        public int BlockedQuantity { get; set; }
        public DateTime Date { get; set; }
        public bool IsDeleted { get; set; }
    }
}
