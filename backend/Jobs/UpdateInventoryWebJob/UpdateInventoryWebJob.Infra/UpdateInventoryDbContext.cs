﻿using System.Data.Entity;
using UpdateInventoryWebJob.Infra.Entities;

namespace UpdateInventoryWebJob.Infra
{
    public class UpdateInventoryDbContext : DbContext
    {
        public UpdateInventoryDbContext(): base("DB")
        {
            Database.SetInitializer<UpdateInventoryDbContext>(null);
        }

        public virtual DbSet<Property> Properties { get; set; }
        public virtual DbSet<RoomTypeInventory> RoomTypeInventories { get; set; }
        
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            var instance = System.Data.Entity.SqlServer.SqlProviderServices.Instance;
        }
    }
}
