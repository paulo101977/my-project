﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using UpdateInventoryWebJob.Infra.Entities;

namespace UpdateInventoryWebJob.Infra.Application
{
    public class UpdateInventoryAppService
    {
        public void Execute()
        {
            using (var context = new UpdateInventoryDbContext())
            {
                var ignoredIdList = new int[] { 11, 22 }.ToList();
                var propertyList = context.Properties.Where(e => !ignoredIdList.Contains(e.PropertyId)).ToList();

                Console.WriteLine("Obteve lista com todos hotéis");

                var oldInventory = context.RoomTypeInventories.AsNoTracking().Where(e => !e.IsDeleted && !ignoredIdList.Contains(e.PropertyId)).ToList();

                Console.WriteLine("Obteve o inventário antigo");

                foreach (var property in propertyList)
                {
                    var exec = context.Database.ExecuteSqlCommand("exec PROC_UPDATE_INVENTORY @propertyId",
                        new SqlParameter("@propertyId", property.PropertyId));

                    Console.WriteLine(string.Format("Inventário atualizado do Hotel {0} : {1}", property.PropertyId, property.Name));
                }
                
                var newInventory = context.RoomTypeInventories.AsNoTracking()
                    .Where(e => !e.IsDeleted && !ignoredIdList.Contains(e.PropertyId))
                    .OrderBy(e => e.PropertyId)
                    .ThenBy(e => e.RoomTypeId)
                    .ThenBy(e => e.Date)
                    .ToList();

                Console.WriteLine("Obteve o inventário atual");

                ProcessDiff(oldInventory, newInventory);
            }
        }

        private void ProcessDiff(List<RoomTypeInventory> oldInventory, List<RoomTypeInventory> newInventory)
        {
            var text = new StringBuilder("<html><body>");
            var hasDiff = false;
            foreach (var newInvLine in newInventory)
            {
                var oldInvLine = oldInventory.FirstOrDefault(e => e.PropertyId == newInvLine.PropertyId &&
                                                  e.RoomTypeId == newInvLine.RoomTypeId &&
                                                  e.Date == newInvLine.Date);

                if (oldInvLine == null)
                    continue;

                if(newInvLine.Total != oldInvLine.Total || 
                   newInvLine.Balance != oldInvLine.Balance ||
                   newInvLine.BlockedQuantity != oldInvLine.BlockedQuantity)
                {
                    hasDiff = true;
                    InventoryEmail(text, newInvLine, oldInvLine);
                }
            }

            text.AppendFormat("</body></html>");

            if (hasDiff)
                SendMail("DIFERENÇA no Inventário", text.ToString(), true);

            if (CheckControl())
                SendMail("Inventário OK", ControlEmail());

            Console.WriteLine("Processou a diferença do inventário");
        }

        public bool CheckControl()
        {
            var now = DateTime.Now;
            var controlDate = new DateTime(now.Year, now.Month, now.Day, 1, 0, 0);
            var d1 = controlDate.AddMinutes(-30);
            var d2 = controlDate.AddMinutes(+30);
            return now > d1 && now < d2;
        }

        private string ControlEmail()
            => "<html><body><p>tudo OK</p></body></html>";

        private void InventoryEmail(StringBuilder text, RoomTypeInventory newInvLine, RoomTypeInventory oldInvLine)
        {
            text.AppendFormat("<br />");
            text.AppendFormat("<p>Divergência: dia {0}, hotelId {1}, tipo de quarto: {2}</p>", newInvLine.Date.ToString("dd/MM/yyyy"), newInvLine.PropertyId, newInvLine.RoomTypeId);
            text.AppendFormat("<p>(antigo): Total {0}, Balance {1}, BlockedQuantity: {2}</p>", oldInvLine.Total, oldInvLine.Balance, oldInvLine.BlockedQuantity);
            text.AppendFormat("<p>(novo): Total {0}, Balance {1}, BlockedQuantity: {2}</p>", newInvLine.Total, newInvLine.Balance, newInvLine.BlockedQuantity);
        }

        public void SendMail(string subject, string body, bool? sendToAll = false)
        {
            var host = ConfigurationManager.AppSettings["host"];
            int port = Convert.ToInt32(ConfigurationManager.AppSettings["port"]);
            var userName = ConfigurationManager.AppSettings["userName"];
            var passsword = ConfigurationManager.AppSettings["passsword"];

            MailMessage message = new MailMessage();
            message.IsBodyHtml = true;
            message.From = new MailAddress("noreply@thex.cloud", "noreply@thex.cloud");
            message.To.Add(new MailAddress("saulo.brito@totvs.com.br"));
            message.To.Add(new MailAddress("luiz.queiroz@totvs.com.br"));

            if(sendToAll.HasValue && sendToAll.Value)
                message.To.Add(new MailAddress("jorge.canales@totvs.com.br"));
            
            message.Subject = subject;
            message.Body = body;

            using (var client = new SmtpClient(host, port))
            {
                client.Credentials = new NetworkCredential(userName, passsword);
                client.EnableSsl = true;

                try
                {
                    Console.WriteLine("Email com diferença de inventário será enviado...");
                    client.Send(message);
                    Console.WriteLine("Email enviado!");
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Email não foi enviado.");
                    Console.WriteLine("Error: " + ex.Message);
                }
            }
        }
    }
}
