﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Thex.Common.Enumerations
{
    public enum MealPlanTypeEnum
    {
        None = 1,
        Breakfast = 2,
        Halfboard = 3,
        Halfboardlunch = 4,
        Halfboardlunchdinner = 5,
        Fullboard = 6,
        Allinclusive = 7

    }
}
