﻿namespace Thex.Common.Enumerations
{
    public class BillingAccountEnum
    {
        /// <summary>
        /// Error enumerations of <see cref="BillingAccountEnum"/>/>
        /// </summary>
        public enum Error
        {
            MainBillingAccountCanNotBeDeleted = 1,
            BillingAccountWithBillingAccountItemsCanNotBeDeleted = 2,
            BillingAccountWithBillingAccountItemsCanNotBeClosedOrBlocked = 3,
            CanNotReopenAnBillingAccountAlreadyOpen = 4,
            OwnerMustHaveEmail = 5,
            OwnerMustHaveDocument = 6,
            OwnerMustHaveLocation = 7,
			OwnerMustHaveNationality = 8,
            PersonHeaderAlreadyAssociateWithOtherAccount = 9
        }

        public enum Default
        {
            BillingAccountDefaultName = 1,
            BillingAccountDefaultNameConsolidated = 2,
            BillingAccountDefaultNameMain = 3
        }      
    }
}
