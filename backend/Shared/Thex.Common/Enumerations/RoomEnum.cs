﻿namespace Thex.Common.Enumerations
{
    public class RoomEnum
    {
        public enum Error
        {
            RoomForeignKeyError = 1,
            RoomUKKeyError = 2
        }
    }
}
