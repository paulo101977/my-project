﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Thex.Common.Enumerations
{
    public class UnitMeasurementEnum
    {
        public enum UnitMeasurementType
        {
            UnitMeasurementKilo = 1,
            UnitMeasurementUnity = 2,
            UnitMeasurementLiters = 3,
            UnitMeasurementTonne = 4
        }
    }
}
