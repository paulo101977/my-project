﻿using System;
using System.Collections.Generic;
using System.Text;
using Thex.Common.Helpers;

namespace Thex.Common.Enumerations
{
    public enum EnvironmentEnum
    {
        [StringValueEnumAttribute("Dev")]
        Development,
        [StringValueEnumAttribute("Tst")]
        Test,
        [StringValueEnumAttribute("Stg")]
        Staging,
        [StringValueEnumAttribute("Uat")]
        UAT,
        [StringValueEnumAttribute("Bugfix")]
        BugFix,
        [StringValueEnumAttribute("Prod")]
        Production
    }
}
