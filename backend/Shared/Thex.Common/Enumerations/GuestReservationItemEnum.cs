﻿namespace Thex.Common.Enumerations
{
    public class GuestReservationItemEnum
    {
        /// <summary>
        /// Error enumerations of <see cref="GuestReservationItemEnum"/>/>
        /// </summary>
        public enum Error
        {
            GuestReservationItemModifyOrDeleteNotAllowed = 1
        }

        public enum Info
        {
            GuestReservationItemEntityDescription = 1
        }

        public enum RequiredFields
        {
            GuestReservationItemId = 1,
            ReservationItemId = 2,
            GuestName = 3,
            GuestEmail = 4,
            GuestDocument = 5,
            EstimatedArrivalDate = 6,
            CheckInDate = 7,
            EstimatedDepartureDate = 8,
            CheckOutDate = 9,
            GuestStatusId = 10,
            GuestTypeId = 11,
            PreferredName = 12,
            GuestCitizenship = 13,
            GuestLanguage = 14,
            PctDailyRate = 15,
            IsIncognito = 16,
            IsChild = 17,
            IsMain = 18
        }


        public enum GuestStatus
        {
            GuestReservationItemEnumMustHave = 1
        }
    }
}