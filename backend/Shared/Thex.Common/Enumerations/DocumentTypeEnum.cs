﻿// //  <copyright file="BedTypeEnum.cs" company="TOTVS">
// //  Copyright (c) TOTVS. All rights reserved.
// //  </copyright>
namespace Thex.Common.Enumerations
{
    public enum DocumentTypeEnum
    {
        Unknown = 0,
        BrNaturalPersonCPF = 1,
        BrLegalPersonCNPJ = 2,
        ArNaturalDNI = 3,
        ArLegalCUIT = 4,
        UyNaturalCI = 5,
        UyLegalRUT = 6,
        ClNaturalRUT = 7,
        ClLegalRUT = 8,
        PyNaturalRUC = 9,
        PyLegalCI = 10,
        BoNaturalNIT = 11,
        BoLegalNIT = 12,
        BrLegalPersonInscEst = 13,
        BrLegalPersonInscMun = 14,
        Passport = 15,
        BrNaturalPersonRG = 16,
        NaturalNIF = 17,
        UsNaturalSSN = 18,
        LegalENI = 19,
        PtNaturalCartaoCidadao = 20,
        LegalNIF = 21,
        PtNaturalCConducao = 22,
        PtNaturalAResidencia = 23
    }
}