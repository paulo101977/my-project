﻿namespace Thex.Common.Enumerations
{
    public class PropertyBaseRateEnum
    {
        public enum Info
        {
            PropertyBaseRateName = 1
        }

        /// <summary>
        /// Error enumerations of <see cref="PropertyBaseRateEnum"/>/>
        /// </summary>
        public enum Error
        {
            PropertyBaseRatePaxCanNotHaveAValue = 1,
            PropertyBaseRateChildCanNotHaveValue = 2,
            PropertyBaseRateHasInvalidMinOrMaxValue = 3,
            PropertyBaseRatePaxMustHaveValue = 4,
            PropertyBaseRateChildMustHaveValue = 5,
            PropertyBaseRateInvalidPeriod = 6,
            PropertyBaseRateWasNotFound = 7
        }
    }
}
