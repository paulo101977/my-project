﻿namespace Thex.Common.Enumerations
{
    public class CompanyEnum
    {

        /// <summary>
        /// Error enumerations of <see cref="Company"/>/>
        /// </summary>
        public enum Error
        {
            CompanyMustHaveShortName = 1,
            CompanyMustHaveTradeName = 2,
            CompanyMustHavePerson    = 3
        }
    }
}
