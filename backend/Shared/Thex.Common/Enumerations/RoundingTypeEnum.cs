﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Thex.Common.Enumerations
{
    public enum RoundingTypeEnum
    {
        NotRound = 1,
        Default = 2,
        RoundUp = 3,
        RoundDown = 4
    }
}
