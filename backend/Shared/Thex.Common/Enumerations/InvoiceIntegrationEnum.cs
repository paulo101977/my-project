﻿using System;
using System.Collections.Generic;
using System.Text;
using Thex.Common.Helpers;

namespace Thex.Common.Enumerations
{
    public enum InvoiceIntegrationEnum
    {
        [StringValueEnumAttribute("Autorizada")]
        Authorized,
        [StringValueEnumAttribute("Negada")]
        Denied,
        [StringValueEnumAttribute("Cancelada")]
        Canceled,
        [StringValueEnumAttribute("CancelamentoNegado")]
        CancelDenied
    }
}
