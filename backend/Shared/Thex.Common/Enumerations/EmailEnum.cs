﻿
namespace Thex.Common.Enumerations
{
    public enum EmailEnum
    {
        EmailSubjectForgotPassword,
        EmailBodyForgotPassword,
        EmailSubjectUserInvitation,
        EmailBodyUserInvitation,
        ClickHere,
        ExpirationForgotPassword,

        //Slip Reservation
        SlipReservationConfirmationReservationLabel,
        SlipReservationReservationConfirmedSuccessfullyLabel,
        SlipReservationWelcomeLabel,
        SlipReservationReservationCodeLabel,
        SlipReservationCompanyNameLabel,
        SlipReservationContactNameLabel,
        SlipReservationContactPhoneLabel,
        SlipReservationContactEmailLabel,
        SlipReservationStayInformationLabel,
        SlipReservationNightLabel,
        SlipReservationNightsLabel,
        SlipReservationAccommodationLabel,
        SlipReservationReservationPaymentLabel,
        SlipReservationDescriptionLabel,
        SlipReservationValueLabel,
        SlipReservationPriceRoomNightLabel,
        SlipReservationHotelPoliciesLabel,
        EmailSubjectFiscalDocument,
        EmailBodyFiscalDocument,
        EmailBodyPaymentType,
        EmailSubjectExternalAccessReservation,
        EmailBodyExternalAccessReservation
    }
}
