﻿namespace Thex.Common.Enumerations
{
    public class PropertyRateStrategyEnum
    {
        public enum Error
        {
            InvalidPeriod = 1,
            DuplicateRoomTypeAndOccupation = 2,
            RatePlanListEmpty = 3,
            InvalidRangeOccupation = 4,
            InvalidMinOccupation = 5,
            InvalidMaxOccupation = 6,
            InvalidOccupation = 7,
            InvalidAmount = 8,
            InvalidAmountNotNull = 9,
            InvalidAmountNull = 10,
            InvalidPropertyRateStrategyRoomTypes = 11
        }
    }
}
