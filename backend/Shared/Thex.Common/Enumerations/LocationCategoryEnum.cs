﻿// //  <copyright file="LocationCategoryEnum.cs" company="TOTVS">
// //  Copyright (c) TOTVS. All rights reserved.
// //  </copyright>
namespace Thex.Common.Enumerations
{
    public enum LocationCategoryEnum
    {
        Commercial = 1,
        Delivery = 2,
        Billing = 3,
        Residential = 4,
        Correspondence = 5
    }
}