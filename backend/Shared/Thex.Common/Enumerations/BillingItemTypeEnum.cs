﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Thex.Common.Enumerations
{
    public enum BillingItemTypeEnum
    {
        Service = 1,
        Tax = 2,
        PaymentType = 3,
        PointOfSale = 4
    }
}
