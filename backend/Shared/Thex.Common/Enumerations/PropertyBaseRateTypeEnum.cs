﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Thex.Common.Enumerations
{
    public enum PropertyBaseRateTypeEnum
    {
        Amount = 1,
        Variation = 2,
        Premise = 3,
        Level = 4
    }
}
