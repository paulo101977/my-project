﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Thex.Common.Enumerations
{
    public enum MostSelectedTypeEnum
    {
        MostSelectedService = 0,
        MostSelectedProduct = 1,
        MostSelectedCredit = 2,
        BillingItemFav = 3
    }
}
