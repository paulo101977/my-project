﻿//  <copyright file="ReservationStatus.cs" company="TOTVS">
//  Copyright (c) TOTVS. All rights reserved.
//  </copyright>
namespace Thex.Common.Enumerations
{
    public enum ReservationStatus
    {
        ToConfirm = 0,
        Confirmed = 1,
        Checkin = 2,
        Checkout = 3,
        Pending = 4,
        NoShow = 5,
        Canceled = 6,
        WaitList = 7,
        ReservationProposal = 8
    }
}