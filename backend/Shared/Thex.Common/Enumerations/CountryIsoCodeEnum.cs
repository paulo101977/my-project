﻿using System;
using System.Collections.Generic;
using System.Text;
using Thex.Common.Helpers;

namespace Thex.Common.Enumerations
{
    public enum CountryIsoCodeEnum
    {
        NotFound,
        [StringValueEnumAttribute("BR")]
        Brasil,
        [StringValueEnumAttribute("AR,CH,BO")]
        Latam,
        [StringValueEnumAttribute("PT,ES")]
        Europe
    }
}
