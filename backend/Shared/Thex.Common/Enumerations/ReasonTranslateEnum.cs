﻿// //  <copyright file="ReasonTranslateEnum.cs" company="TOTVS">
// //  Copyright (c) TOTVS. All rights reserved.
// //  </copyright>
namespace Thex.Common.Enumerations
{
    public enum ReasonTranslateEnum
    {

        PurposeOfTripShopping = 3,
        PurposeOfTripConventionOrFair = 4,
        PurposeOfTripStudiesOrCourses = 5,
        PurposeOfTripLeisureOrVacation = 6,
        PurposeOfTripBusiness = 7,
        PurposeOfTripRelativesOrFriends = 8,
        PurposeOfTripReligion = 9,
        PurposeOfTripHealth = 10,
        PurposeOfTripOther = 11
    }
}