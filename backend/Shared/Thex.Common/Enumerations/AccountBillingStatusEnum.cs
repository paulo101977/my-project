﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Thex.Common.Enumerations
{
    public enum AccountBillingStatusEnum
    {
        Opened = 1,
        Closed = 2
    }
}
