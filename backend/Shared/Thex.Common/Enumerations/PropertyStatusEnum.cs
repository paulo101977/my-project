﻿namespace Thex.Common.Enumerations
{
    public enum PropertyStatusEnum
    {
        Register = 13,
        Contract = 14,
        Training = 15,
        Production = 19,
        Blocked = 20,
        Unblocked = 21
    }
}




