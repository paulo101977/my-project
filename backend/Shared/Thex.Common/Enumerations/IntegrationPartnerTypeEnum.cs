﻿
namespace Thex.Common.Enumerations
{
    public enum IntegrationPartnerTypeEnum
    {
        Internal = 1,
        External = 2,
        ENotas = 4,
        InvoiceXpress = 5
    }
}
