﻿namespace Thex.Common.Enumerations
{
    public class PersonEnum
    {
        /// <summary>
        /// Error enumerations of <see cref="Person"/>/>
        /// </summary>
        public enum Error
        {
            PersonMustHaveName = 1
        }

        public enum Common
        {
            PersonAndGuestRegistrationSearchByGlobalPerson = 1,
            PersonAndGuestRegistrationSearchByGuest = 2
        }
    }
}