﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Thex.Common.Enumerations
{
    public class UserEnum
    {
        public enum Error
        {
            CreateNewUser = 1,
            InvalidUser = 2,
            InvalidUserOrPassword = 3,
            InvalidToken = 4,
            SuperAdminUserNoAuthorizedAccess = 5
        }
    }
}
