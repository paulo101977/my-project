﻿namespace Thex.Common.Enumerations
{
    public class LocationEnum
    {
        /// <summary>
        /// Error enumerations of <see cref="LocationEnum"/>/>
        /// </summary>
        public enum Error
        {
            LocationMustHave = 1
        }

        public enum Info
        {
            LocationEntityDescription = 1
        }

        public enum RequiredFields
        {
            LocationId = 1,
            CityId = 2
        }
    }
}