﻿namespace Thex.Common.Enumerations
{
    public class BillingInvoicePropertyEnum
    {
        public enum Error
        {
            BillingInvoicePropertySeriesLength,
            BillingInvoicePropertySeriesAlreadyExistsInCompany
        }
    }
}
