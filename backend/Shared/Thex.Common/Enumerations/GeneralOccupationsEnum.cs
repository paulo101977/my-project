﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Thex.Common.Enumerations
{

        public enum GeneralOccupationsEnum
        {
            GeneralOccupationAccountant = 1,
            GeneralOccupationArchitect = 2,
            GeneralOccupationBaker = 3,
            GeneralOccupationBanker = 4,
            GeneralOccupationBarber = 5,
            GeneralOccupationBricklayer = 6,
            GeneralOccupationBusdriver = 7,
            GeneralOccupationButcher = 8,
            GeneralOccupationCaretaker = 9,
            GeneralOccupationCarpenter = 10,
            GeneralOccupationChef = 11,
            GeneralOccupationDriver = 12,
            GeneralOccupationEconomist = 13,
            GeneralOccupationElectrician = 14,
            GeneralOccupationEngineer = 15,
            GeneralOccupationFirefighter = 16,
            GeneralOccupationFlightattendant = 17,
            GeneralOccupationFlorist = 18,
            GeneralOccupationGardener = 19,
            GeneralOccupationGoldsmith = 20,
            GeneralOccupationGraphicDesigner = 21,
            GeneralOccupationHairdresser = 22,
            GeneralOccupationJournalist = 23,
            GeneralOccupationJudge = 24,
            GeneralOccupationLawyer = 25,
            GeneralOccupationLibrarian = 26,
            GeneralOccupationLifeguard = 27,
            GeneralOccupationMechanic = 28,
            GeneralOccupationPhotographer = 29,
            GeneralOccupationPilot = 30,
            GeneralOccupationPlumber = 31,
            GeneralOccupationPoliceOfficer = 32,
            GeneralOccupationPostman = 33,
            GeneralOccupationScientist = 34,
            GeneralOccupationSoldier = 35,
            GeneralOccupationStudent = 36,
            GeneralOccupationTailor = 37,
            GeneralOccupationTaxidriver = 38,
            GeneralOccupationTeacher = 39,
            GeneralOccupationTranslator = 40,
            GeneralOccupationTravelagent = 41,
            GeneralOccupationWaiter = 42
        
    }
}
