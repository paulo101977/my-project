﻿using System.Threading.Tasks;
using Thex.Common.Emails.SendGrid.Message;
using Thex.Common.Message;

namespace Thex.Common.Emails.SendGrid
{
    public interface IAzureEmailSender
    {
        Task<ResponseMessage> SendAsync(EmailMessage message, bool IsHtml = false);
    }
}