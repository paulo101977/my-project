﻿using System;

namespace Thex.Common.Emails.AwsSes
{
    public class AwsEmailSettings
    {
        public AwsEmailSettings(string host, int port, string userName, string password)
        {
            if (string.IsNullOrEmpty(host))
                throw new ArgumentException("Host must be set");

            if (port <= 0)
                throw new ArgumentException("Port must be set");

            if (string.IsNullOrEmpty(userName))
                throw new ArgumentException("UserName must be set");

            if (string.IsNullOrEmpty(password))
                throw new ArgumentException("Password must be set");

            Host = host;
            Port = port;
            UserName = userName;
            Password = password;
        }

        public string Host { get; }
        public int Port { get; }
        public string UserName { get; }
        public string Password { get; }
    }
}
