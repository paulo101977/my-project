﻿using Microsoft.Extensions.Configuration;
using MimeKit;
using System;
using System.Linq;
using Thex.Common.Message;

namespace Thex.Common.Emails.AwsSes
{
    public class AwsEmailSender : IAwsEmailSender
    {
        private readonly AwsEmailSettings _settings;
        private readonly IConfiguration _configuration;

        public AwsEmailSender(AwsEmailSettings settings, IConfiguration configuration = null)
        {
            _settings = settings;

            if(configuration != null)
                _configuration = configuration;
        }

        public void Send(EmailMessage emailMessage, bool isHtml = false)
        {
            if (_configuration != null && !string.IsNullOrEmpty(_configuration.GetValue<string>("TestEnvironment")))
                return;

            // Create and build a new MailMessage object
            MimeMessage mimeMessage = new MimeMessage();
            mimeMessage.Subject = emailMessage.Subject;
            mimeMessage.From.Add(new MailboxAddress("Thex", emailMessage.From));
            mimeMessage.To.AddRange(emailMessage.To.Select(s => new MailboxAddress(s)));

            if (emailMessage.CC.Count > 0)
                mimeMessage.Cc.AddRange(emailMessage.CC.Select(s => new MailboxAddress(s)));

            if (emailMessage.BCC.Count > 0)
                mimeMessage.Bcc.AddRange(emailMessage.BCC.Select(s => new MailboxAddress(s)).ToList());

            mimeMessage.Body = GetBody(emailMessage, isHtml);

            using (var client = new MailKit.Net.Smtp.SmtpClient())
            {
                client.Connect(_settings.Host, _settings.Port, true);

                client.Authenticate(_settings.UserName, _settings.Password);

                client.Send(mimeMessage);

                client.Disconnect(true);
            }
        }

        private MimeEntity GetBody(EmailMessage emailMessage, bool isHtml)
        {
            var bodyBuilder = new BodyBuilder();

            if (isHtml)
                bodyBuilder.HtmlBody = emailMessage.Body;
            else
                bodyBuilder.TextBody = emailMessage.Body;

            return bodyBuilder.ToMessageBody();
        }
    }
}
