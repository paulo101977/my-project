﻿using Thex.Common.Message;

namespace Thex.Common.Emails.AwsSes
{
    public interface IAwsEmailSender
    {
        void Send(EmailMessage message, bool IsHtml = false);
    }
}
