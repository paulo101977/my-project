﻿using System;

namespace Thex.Common.Helpers
{
    public static class PrecisionHelper
    {
        public static decimal TruncateTwoDecimalPlaces(this decimal value)
        {
            return Math.Truncate(100 * value) / 100;
        }
    }
}
