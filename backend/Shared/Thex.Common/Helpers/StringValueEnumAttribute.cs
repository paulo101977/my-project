﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Thex.Common.Helpers
{
    public class StringValueEnumAttribute : Attribute
    {
        public string Value
        {
            get;
            protected set;
        }

        public StringValueEnumAttribute(string value)
        {
            this.Value = value;
        }
    }
}
