﻿using Newtonsoft.Json;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Sockets;
using System.Text;

namespace Thex.Common.Helpers
{
    public static class HttpClientHelper
    {
        private delegate bool execDelegate(HttpClient cliente, out bool autorizado);

        private static int _currentAttemptQuantity;

        public const int TimeoutRequest = 120;
        public const int MaxAttempt = 3;
        public const int AttemptInterval = 5;

        public static bool SendRequest<T>(string serviceUrl, string param, string body, ref T objReturn, string token = "")
        {
            var ret = false;
            var url = string.Format(serviceUrl, param);
            T internalReturn = default(T);

            _currentAttemptQuantity = 1;

            var conexaoOK = Execute(url,
                (HttpClient cliente, out bool auth) =>
                {
                    var retServico = false;
                    auth = true;
                    if (!string.IsNullOrEmpty(token))
                    {
                        cliente.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", token);
                    }

                    using (var resposta = cliente.PostAsync(url, new StringContent(body, Encoding.UTF8, "application/json")).Result)
                    {
                        if (resposta.IsSuccessStatusCode)
                        {
                            retServico = true;
                            internalReturn = JsonConvert.DeserializeObject<T>(resposta.Content.ReadAsStringAsync().Result, new JsonSerializerSettings { TypeNameHandling = TypeNameHandling.Auto });

                            ret = (internalReturn != null);
                        }
                        else
                        {
                            auth = (resposta.StatusCode != HttpStatusCode.Unauthorized);
                        }
                    }

                    return retServico;
                });

            objReturn = internalReturn;

            return ret;
        }

        private static bool Execute(string url, execDelegate execDelegate)
        {
            var ret = true;
            var auth = false;
            var serverRet = false;

            try
            {
                using (var cliente = new HttpClient())
                {
                    cliente.DefaultRequestHeaders.TryAddWithoutValidation("Accept", "application/json");
                    cliente.DefaultRequestHeaders.TryAddWithoutValidation("Accept-Charset", "utf-8");
                    cliente.DefaultRequestHeaders.TryAddWithoutValidation("User-Agent", "Bematech PDV");

                    //cliente.DefaultRequestHeaders.Add("Token", _token); 
                    cliente.Timeout = TimeSpan.FromSeconds(TimeoutRequest);
                    serverRet = execDelegate(cliente, out auth);
                }
            }
            catch (Exception ex)
            {
                while (ex.InnerException != null || ex.GetType() == typeof(SocketException))
                {
                    if (ex.GetType() == typeof(SocketException))
                    {
                        if ((ex as SocketException).SocketErrorCode == SocketError.ConnectionRefused)
                        {
                            ret = false;
                        }
                        break;
                    }
                    else
                    {
                        ex = ex.InnerException;
                    }
                }
            }


            if (!serverRet && _currentAttemptQuantity < MaxAttempt)
            {
                _currentAttemptQuantity++;
                if (!auth)
                {
                    // GetToken(true);
                }

                System.Threading.Thread.Sleep(AttemptInterval);

                ret = Execute(url, execDelegate);
            }

            return ret;
        }
    }
}
