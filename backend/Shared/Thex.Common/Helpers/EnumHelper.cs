﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace Thex.Common.Helpers
{
    public static class EnumHelper
    {
        private const string _defaultEnumValue = "-1";

        private static StringValueEnumAttribute[] GetStringValueEnumAttributes(FieldInfo field)
        {
            return (field.GetCustomAttributes(typeof(StringValueEnumAttribute), false) as StringValueEnumAttribute[]);
        }

        public static string LocalizaValorEnum<TEnum>(string nome)
        {
            foreach (var field in typeof(TEnum).GetFields())
            {
                if (!field.IsSpecialName)
                {
                    var attr = EnumHelper.GetStringValueEnumAttributes(field);
                    if (attr.Length > 0)
                    {
                        if (field.Name.ToUpper().Equals(nome.ToUpper()))
                        {
                            return attr[0].Value.ToUpper();
                        }

                    }
                }
            }

            return null;
        }

        public static List<Enum> ToList(Type type)
        {
            var ret = new List<Enum>();

            if (type != null && type.IsEnum)
            {
                var enumeration = Activator.CreateInstance(type);
                foreach (var fieldInfo in type.GetFields(BindingFlags.Static | BindingFlags.Public))
                {
                    ret.Add((Enum)fieldInfo.GetValue(enumeration));
                }
            }

            return ret;
        }

        public static string GetStringValue(Enum value)
        {
            var field = value.GetType().GetField(value.ToString());
            var attribs = GetStringValueEnumAttributes(field);
            return (attribs.Length > 0 ? attribs[0].Value : string.Empty);
        }
        public static Dictionary<int, string> GetList(Type type)
        {
            var ret = new Dictionary<int, string>();

            if (type != null && type.IsEnum)
            {
                var enumeration = Activator.CreateInstance(type);
                foreach (var fieldInfo in type.GetFields(BindingFlags.Static | BindingFlags.Public))
                {
                    var id = fieldInfo.GetValue(enumeration).TryParseToInt32();
                    var val = GetStringValue((Enum)fieldInfo.GetValue(enumeration));
                    ret.Add(id, val);
                }
            }

            return ret;
        }

        public static object GetEnumValue(Type tipo, string value, bool contain = false)
        {
            object ret = null;
            var found = false;

            if (tipo.IsEnum && !string.IsNullOrEmpty(value))
            {
                value = value.Trim().ToLower();

                foreach (var field in tipo.GetFields())
                {
                    if (!field.IsSpecialName)
                    {
                        if (contain)
                        {
                            var attr = GetStringValueEnumAttributes(field);
                            if (attr.Length > 0)
                            {
                                if (attr[0].Value.ToLower().Contains(value.ToLower()))
                                {
                                    ret = Enum.Parse(tipo, field.Name, true);
                                    found = true;
                                    break;
                                }
                            }
                        }
                        else
                        {
                            var attribs = GetStringValueEnumAttributes(field);
                            if (attribs.Length > 0)
                            {
                                if (value == attribs[0].Value.ToLower())
                                {
                                    ret = Enum.Parse(tipo, field.Name, true);
                                    found = true;
                                    break;
                                }
                            }

                            if (!found)
                            {
                                var val = (Enum)Enum.Parse(tipo, field.Name, true);

                                if (value == (val).ToString().ToLower())
                                {
                                    found = true;
                                    ret = val;
                                    break;
                                }
                            }
                        }
                    }
                }

                if (!found)
                {
                    var val = ToList(tipo).Find(item => item.TryParseToInt32().ToString() == value);
                    found = (val != null);
                    if (found)
                    {
                        ret = Enum.Parse(tipo, val.ToString(), true);
                    }
                }
            }

            return ret;
        }

        public static TEnum GetEnumValue<TEnum>(string value, bool contain = false)
        {
            var ret = default(TEnum);
            var found = false;

            if (typeof(TEnum).IsEnum && !string.IsNullOrEmpty(value))
            {
                value = value.Trim().ToLower();

                foreach (var field in typeof(TEnum).GetFields())
                {
                    if (!field.IsSpecialName)
                    {
                        if (contain)
                        {
                            var attr = GetStringValueEnumAttributes(field);
                            if (attr.Length > 0)
                            {
                                if (attr[0].Value.ToLower().Contains(value.ToLower()))
                                {
                                    ret = (TEnum)Enum.Parse(typeof(TEnum), field.Name, true);
                                    found = true;
                                    break;
                                }
                            }
                        }
                        else
                        {
                            var attribs = GetStringValueEnumAttributes(field);
                            if (attribs.Length > 0)
                            {
                                var lista = attribs[0].Value.ToLower().Split(',');
                                if (lista != null)
                                {
                                    foreach (var item in lista)
                                    {
                                        if (value == item)
                                        {
                                            ret = (TEnum)Enum.Parse(typeof(TEnum), field.Name, true);
                                            found = true;
                                            break;
                                        }
                                    }
                                }
                            }
                            else
                            {
                                var val = (TEnum)Enum.Parse(typeof(TEnum), field.Name, true);

                                if (value == (val).ToString().ToLower())
                                {
                                    found = true;
                                    ret = val;
                                    break;
                                }
                            }
                        }
                    }
                }

                if (!found)
                {
                    var val = ToList(typeof(TEnum)).Find(item => item.TryParseToInt32().ToString() == value);
                    found = (val != null);
                    if (found)
                    {
                        ret = (TEnum)Enum.Parse(typeof(TEnum), val.ToString(), true);
                    }
                }
            }

            if (!found)
            {
                ret = GetDefaultValue<TEnum>();
            }

            return ret;
        }

        public static TEnum GetDefaultValue<TEnum>()
        {
            var ret = default(TEnum);

            foreach (var field in typeof(TEnum).GetFields())
            {
                if (!field.IsSpecialName)
                {
                    var val = (TEnum)Enum.Parse(typeof(TEnum), field.Name, true);

                    if (val.TryParseToInt32().ToString() == _defaultEnumValue)
                    {
                        ret = val;
                        break;
                    }
                }
            }

            return ret;
        }

        private static int TryParseToInt32(this object obj)
        {
            int ret = 0;

            try
            {
                ret = Convert.ToInt32(obj);
            }
            catch (Exception)
            {
            }

            return ret;
        }
    }    
}
