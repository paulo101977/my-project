﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using Thex.Common.Enumerations;

namespace Thex.Common.Helpers
{
    public static class DocumentValidatorHelper
    {
        private static string _message;
        private const string cnpj = "00000000000000";

        /// <summary>
        /// Mensagem de erro retornada pelas validações.
        /// </summary>
        public static string Message
        {
            get
            {
                return _message ?? string.Empty;
            }
            set
            {
                _message = value;
            }
        }

        /// <summary>
        /// Valida um CPF.
        /// </summary>
        /// <param name="value">Valor a ser validado. Pode conter os caracteres de formatação ("." e "-").</param>
        /// <returns>Se o número é um CPF válido.</returns>
        public static bool ValidateCPF(string value)
        {
            var ret = true;
            const int numCharCPF = 11;

            try
            {
                // Retira caracteres especiais do número.
                value = value.Replace(".", "").Replace("-", "");

                if (value.Length != numCharCPF)
                {
                    ret = false;
                }
                else
                {
                    // Verifica se todos os caracteres são iguais.
                    var allCharEquals = true;
                    for (var i = 1; i < numCharCPF; i++)
                    {
                        if (value[i] != value[0])
                        {
                            allCharEquals = false;
                            break;
                        }
                    }

                    if (allCharEquals || value == "12345678909")
                    {
                        ret = false;
                    }
                    else
                    {
                        // Monta uma lista com cada número.
                        var numbers = new int[numCharCPF];
                        for (int i = 0; i < numCharCPF; i++)
                        {
                            numbers[i] = int.Parse(value[i].ToString(), CultureInfo.InvariantCulture);
                        }

                        var sum = 0;
                        for (var i = 0; i < 9; i++)
                        {
                            sum += (10 - i) * numbers[i];
                        }

                        var result = (sum % numCharCPF);
                        if (result == 1 || result == 0)
                        {
                            if (numbers[9] != 0)
                            {
                                ret = false;
                            }
                        }
                        else if (numbers[9] != numCharCPF - result)
                        {
                            ret = false;
                        }

                        if (ret)
                        {
                            sum = 0;
                            for (int i = 0; i < 10; i++)
                            {
                                sum += (numCharCPF - i) * numbers[i];
                            }

                            result = (sum % numCharCPF);
                            if (result == 1 || result == 0)
                            {
                                if (numbers[10] != 0)
                                {
                                    ret = false;
                                }
                            }
                            else if (numbers[10] != numCharCPF - result)
                            {
                                ret = false;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Message = ex.Message;
                ret = false;
            }

            return ret;
        }

        /// <summary>
        /// Valida um CNPJ.
        /// </summary>
        /// <param name="value">Valor a ser validado. Pode conter os caracteres de formatação (".", "/" e "-").</param>
        /// <returns>Se o número é um CNPJ válido.</returns>
        public static bool ValidateCNPJ(string value)
        {
            var ret = true;

            try
            {
                value = value.Replace(".", "").Replace("/", "").Replace("-", "");

                if (value == cnpj)
                {
                    return false;
                }

                var ftmt = "6543298765432";
                var cnpjOk = new bool[2];
                var digits = new int[14];
                var sum = new int[2];
                var result = new int[2];

                sum[0] = 0;
                sum[1] = 0;

                result[0] = 0;
                result[1] = 0;

                cnpjOk[0] = false;
                cnpjOk[1] = false;

                for (var i = 0; i < 14; i++)
                {
                    digits[i] = int.Parse(value.Substring(i, 1), CultureInfo.InvariantCulture);
                    if (i <= 11)
                    {
                        sum[0] += (digits[i] * int.Parse(ftmt.Substring(i + 1, 1), CultureInfo.InvariantCulture));
                    }

                    if (i <= 12)
                    {
                        sum[1] += (digits[i] * int.Parse(ftmt.Substring(i, 1), CultureInfo.InvariantCulture));
                    }
                }

                for (var i = 0; i < 2; i++)
                {
                    result[i] = (sum[i] % 11);
                    if (result[i] == 0 || result[i] == 1)
                    {
                        cnpjOk[i] = (digits[12 + i] == 0);
                    }
                    else
                    {
                        cnpjOk[i] = (digits[12 + i] == (11 - result[i]));
                    }
                }

                ret = (cnpjOk[0] && cnpjOk[1]);
            }
            catch (Exception ex)
            {
                Message = ex.Message;
                ret = false;
            }

            return ret;
        }

        /// <summary>
        /// Valida um CUIT.
        /// </summary>
        /// <param name="value">Valor a ser validado somente com dígitos.</param>
        /// <returns>Se o número é um CUIT válido.</returns>
        public static bool ValidateCUIT(string value)
        {
            var ret = false;

            try
            {
                var digits = new int[11];
                var sum = 0;

                if (value.Trim().Length == 11)
                {
                    digits[0] = Convert.ToInt32(value[10]) * 1;
                    digits[1] = Convert.ToInt32(value[9]) * 2;
                    digits[2] = Convert.ToInt32(value[8]) * 3;
                    digits[3] = Convert.ToInt32(value[7]) * 4;
                    digits[4] = Convert.ToInt32(value[6]) * 5;
                    digits[5] = Convert.ToInt32(value[5]) * 6;
                    digits[6] = Convert.ToInt32(value[4]) * 7;
                    digits[7] = Convert.ToInt32(value[0]) * 5;
                    digits[8] = Convert.ToInt32(value[1]) * 4;
                    digits[9] = Convert.ToInt32(value[2]) * 3;
                    digits[10] = Convert.ToInt32(value[3]) * 2;

                    foreach (var digit in digits)
                    {
                        sum += digit;
                    }

                    ret = (sum % 11 == 0 || value == new string('9', 11));
                }
            }
            catch (Exception ex)
            {
                Message = ex.Message;
            }

            return ret;
        }

        /// <summary>
        /// Valida um RUT.
        /// </summary>
        /// <param name="value">Valor a ser validado somente com dígitos.</param>
        /// <returns>Se o número é um RUT válido.</returns>
        public static bool ValidateRUT(string value)
        {
            var ret = false;

            try
            {
                var iTam = value.Trim().Length - 1;

                if (iTam == 8)
                {
                    var originalDv = value.Trim().Substring(iTam + 1, 1).ToUpper(CultureInfo.InvariantCulture);
                    var numberWithoutDv = value.Trim().Substring(1, iTam);
                    var sum = 0;
                    var factor = 2;

                    for (int x = iTam; x == 1; x--)
                    {
                        sum += Convert.ToInt32(numberWithoutDv[x]) * factor;
                        factor++;
                        if (factor == 8)
                        {
                            factor = 2;
                        }
                    }

                    var dvCalc = (sum % 11 != 0) ? (11 - (sum % 11)).ToString(CultureInfo.InvariantCulture) : "0";

                    if (dvCalc == "10")
                    {
                        dvCalc = "K";
                    }
                    else if (dvCalc == "11")
                    {
                        dvCalc = "0";
                    }

                    ret = (originalDv == dvCalc);
                }
            }
            catch (Exception ex)
            {
                Message = ex.Message;
            }

            return ret;
        }

        /// <summary>
        /// Valida um número conforme o tipo de documento especificado.
        /// </summary>
        /// <param name="type">Tipo de documento.</param>
        /// <param name="value">Valor a ser validado. Segue a mesma regra dos métodos correspondentes às validações específicas.</param>
        /// <returns>Se o número é um documento válido.</returns>
        public static bool ValidateDocument(DocumentTypeEnum type, string value)
        {
            var ret = false;

            switch (type)
            {
                case DocumentTypeEnum.BrLegalPersonCNPJ: ret = ValidateCNPJ(value); break;
                case DocumentTypeEnum.BrNaturalPersonCPF: ret = ValidateCPF(value); break;
                case DocumentTypeEnum.ArLegalCUIT: ret = ValidateCUIT(value); break;
                case DocumentTypeEnum.ClNaturalRUT: ret = ValidateRUT(value); break;
                default: ret = true; break;
            }

            return ret;
        }

        /// <summary>
        /// Valida o tipo de documento conforme o numero digitado
        /// </summary>
        /// <param name="value">Valor a ser validado. Segue a mesma regra dos métodos correspondentes às validações específicas.</param>
        /// <returns>O tipo do documento.</returns>
        public static DocumentTypeEnum ValidateDocumentType(string value)
        {
            var ret = DocumentTypeEnum.Unknown;

            if (ValidateCNPJ(value))
            {
                ret = DocumentTypeEnum.BrLegalPersonCNPJ;
            }
            else if (ValidateCPF(value))
            {
                ret = DocumentTypeEnum.BrNaturalPersonCPF;
            }
            else if (ValidateCUIT(value))
            {
                ret = DocumentTypeEnum.ArLegalCUIT;
            }
            else if (ValidateRUT(value))
            {
                ret = DocumentTypeEnum.ClNaturalRUT;
            }

            return ret;
        }
    }
}
