﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Thex.Common.Queries
{
    public static class PropertyQueries
    {
        public static string FiltersByPropertyId(string alias, int propertyId)
           => $"{alias}.propertyId={propertyId}";

        public static object FiltersByIsActive(string alias, string active)
          => $"{alias}.isactive = {active}";

    }
    public static class ReservationQueries
    {
        public static string IsDeleted(string alias, string deleted)
          => $"{alias}.Isdeleted = {deleted}";
    }

    public static class ReservationItemQueries
    {
        public static string CheckInOrEstimatedArrivalDate(string alias, string paramName)
           => $"cast(coalesce({alias}.CheckInDate, {alias}.EstimatedArrivalDate) as Date) as {paramName}";
        public static string CheckOutOrEstimatedDepartureDate(string alias, string paramName)
           => $"cast(coalesce({alias}.CheckOutDate, {alias}.EstimatedDepartureDate) as Date) as {paramName}";

        public static string CoalesceCheckDateEstimatedDate(string alias, string dateStart, string dateEnd)
           => $"(cast(coalesce({alias}.CheckInDate, {alias}.EstimatedArrivalDate) as date) >= '{dateStart}' " +
              $"and cast(coalesce({alias}.CheckInDate, {alias}.EstimatedArrivalDate) as date) <= '{dateEnd}') or" +
              $"(cast(coalesce({alias}.CheckOutDate, {alias}.EstimatedDepartureDate) as date) >= '{dateStart}'" +
              $"and cast(coalesce({alias}.CheckOutDate, {alias}.EstimatedDepartureDate) as date) <= '{dateEnd}')";
    }

    public static class GuestReservationItemQueries
    {
        public static string CoalesceChildAge(string alias, string paramName)
        => $"coalesce({alias}.childAge, 0) as {paramName}";

    }
}
