﻿namespace Thex.Common.Dto
{
    public class MainGuestHigsDto
    {
        /// <summary>
        /// Name of accompanying guests.
        /// </summary>
        public string GivenName { get; set; }

        /// <summary>
        /// Last Name of accompanying guests.
        /// </summary>
        public string Surename { get; set; }

        /// <summary>
        /// Phone number of accompanyng guests.
        /// </summary>
        //[Phone(ErrorMessage = "317")]
        public string Telephone { get; set; }

        /// <summary>
        /// Object of address
        /// </summary>
        public AddressHigsDto Address { get; set; }

        /// <summary>
        /// Guest's email.
        /// </summary>
        public string Email { get; set; }
    }
}
