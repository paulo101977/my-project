﻿using System.Collections.Generic;

namespace Thex.Common.Dto
{
    public class ReservationDownloadDto
    {
        public string Message { get; set; }

        public List<ReservationHeaderHigsDto> ReservationsList { get; set; }
    }
}
