﻿namespace Thex.Common.Dto
{
    public class AddressHigsDto
    {
        /// <summary>
        /// Address of the guest.
        /// </summary>
        public string AdressLine { get; set; }

        /// <summary>
        /// Name of the city.
        /// </summary>
        public string CityName { get; set; }

        /// <summary>
        /// Name of the state or the province.
        /// </summary>
        public string StateProv { get; set; }

        /// <summary>
        /// ZipCode of the guest.
        /// </summary>
        public string ZipCode { get; set; }

        /// <summary>
        /// Country of the guest.
        /// </summary>
        public string CountryName { get; set; }

        /// <summary>
        /// Name of the guest company.
        /// </summary>
        public string CompanyName { get; set; }
    }
}
