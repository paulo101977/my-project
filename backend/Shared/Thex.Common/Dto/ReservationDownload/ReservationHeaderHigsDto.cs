﻿using Thex.Common.Enumerations;

namespace Thex.Common.Dto
{
    public class ReservationHeaderHigsDto
    {
        /// <summary>
        /// Numero da reserva do hotel no Integrador.
        /// </summary>
        public string ReservationNumber { get; set; }

        /// <summary>
        /// NUmero da reserva do hotel no HIGS
        /// </summary>
        public string ReservationNumberHigs { get; set; }

        /// <summary>
        /// Tipo de operação da reserva, que pode ser: 1 - Insert, Modify e Cancel.
        /// </summary>
        public SituationTypeHigsEnum OperationType { get; set; }

        /// <summary>
        /// Bilhete Json da reserva.
        /// </summary>
        public string Request { get; set; }


        public ReservationHigsDto RequesDto { get; set; }
    }
}
