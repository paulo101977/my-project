﻿using System;
using System.Collections.Generic;
using Thex.Common.Enumerations;

namespace Thex.Common.Dto
{
    public class ReservationHigsDto
    {
        /// <summary>
        /// Partner name/system name information.
        /// </summary>
        public string CreatorId { get; set; }

        /// <summary>
        /// A string code that identifies reservation source. This field is used to link roomtypes and rateplans so you need to insert the correct code. See the list bellow.
        /// </summary>
        public string SourceOfBusiness { get; set; }

        /// <summary>
        /// Code that identifies the hotel in the API. You can get this code in the response JSON of the Search Method.
        /// </summary>
        public int IdHotel { get; set; }

        /// <summary>
        /// Checkin date of reservation.
        /// </summary>
        public DateTime Checkin { get; set; }

        /// <summary>
        /// Checkout date of reservation.
        /// </summary>
        public DateTime Checkout { get; set; }

        /// <summary>
        /// Date and time of inclusion of the reservation.
        /// </summary>
        public DateTime DateTimeInclusion { get; set; }

        /// <summary>
        /// Number of adults guests.
        /// </summary>
        public int GuestCount { get; set; }

        /// <summary>
        /// Number of children guests.
        /// </summary>
        public int NumberChildren { get; set; }

        /// <summary>
        /// Set of main guest informations.
        /// </summary>
        public MainGuestHigsDto MainGuest { get; set; }

        /// <summary>
        /// Set of information of accompanying guests.
        /// </summary>
        public List<GuestHigsDto> Guests { get; set; }

        /// <summary>
        /// Notes of reservation.
        /// </summary>        
        public string Comment { get; set; }

        /// <summary>
        /// Identification of payment type.CreditCard = 1, DirectHotel = 2, Invoiced = 3
        /// </summary>
        public TypePaymentHigsEnum? TypePayment { get; set; }

        /// <summary>
        /// List of invoiced payment.
        /// </summary>
        public List<string> TypeInvoice { get; set; }

        /// <summary>
        /// Set of information of credit card.
        /// </summary>
        public CreditCardDataHigsDto CreditCardData { get; set; }

        /// <summary>
        /// Code that identifies the rate plan.
        /// </summary>
        public string RatePlanCode { get; set; }

        /// <summary>
        /// Commission value.
        /// </summary>     
        public int? Commission { get; set; }

        /// <summary>
        /// Code that identifies the room type. You need to map this information with room types in the Bematech CMNet Systems, this is a mandatory.
        /// </summary>
        public string RoomTypeCode { get; set; }

        /// <summary>
        /// List of hotels that are been reserved.
        /// </summary>
        public List<HotelReservationHigsDto> HotelReservationIds { get; set; }

        /// <summary>
        /// Information list of taxes.
        /// </summary>
        public List<TaxesHigsDto> Taxes { get; set; }

        /// <summary>
        /// Information list of rates.
        /// </summary>
        public List<RatesHigsDto> Rates { get; set; }

        /// <summary>
        /// Total amount without tax. 
        /// </summary>
        public decimal TotalAmountBeforeTax { get; set; }

        /// <summary>
        /// Code of the currency used.
        /// </summary>
        public string CurrencyCode { get; set; }


        public CompanyInfoHigsDto CompanyInfo { get; set; }
    }
}
