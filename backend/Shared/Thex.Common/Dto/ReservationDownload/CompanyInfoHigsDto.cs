﻿namespace Thex.Common.Dto
{
    public class CompanyInfoHigsDto
    {
        public string CompanyId { get; set; }

        public string CompanyName { get; set; }

        public CompanyAddressHigsDto CompanyAddress { get; set; }

        public string CompanyEmail { get; set; }

        public string CompanyPhoneNumber { get; set; }
    }
}
