﻿
using Tnf.Dto;

namespace Thex.Common.Dto
{
    public class LocationCategoryDto : BaseDto
    {
        public int Id { get; set; }
        public static LocationCategoryDto NullInstance = null;

        public string Name { get; set; }
        public string RecordScope { get; set; }
    }
}
