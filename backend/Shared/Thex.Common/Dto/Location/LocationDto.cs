﻿using System;
using System.Text;
using Thex.Common.Dto;
using Tnf.Dto;

namespace Thex.Common.Dto
{
    public class LocationDto
    {
        public int Id { get; set; }

        public LocationDto()
        {
        }

        public LocationDto(int id)
        {
            Id = id;
        }

        public Guid OwnerId { get; set; }
        public string BrowserLanguage { get; set; }
        public int LocationCategoryId { get; set; }
        public decimal Latitude { get; set; }
        public decimal Longitude { get; set; }
        public string StreetName { get; set; }
        public string StreetNumber { get; set; }
        public string AdditionalAddressDetails { get; set; }
        public string Neighborhood { get; set; }
        public int CityId { get; set; }
        public string PostalCode { get; set; }
        public string CountryCode { get; set; }

        public int? StateId { get; set; }
        public int? CountryId { get; set; }
        public string Subdivision { get; set; }
        public string Division { get; set; }
        public string Country { get; set; }

        public static LocationDto NullInstance = null;

        public virtual CountrySubdivisionDto City { get; set; }
        public virtual CountrySubdivisionDto State { get; set; }
        public virtual CountrySubdivisionDto CountryEntity { get; set; }

        public virtual LocationCategoryDto LocationCategory { get; set; }

        public AddressTranslationDto AddressTranslation { get; set; }

        public string CompleteAddress
        {
            get
            {
                var completeAddress = new StringBuilder();

                completeAddress.Append($"{StreetName}, {StreetNumber} - {AdditionalAddressDetails}");

                if (!string.IsNullOrEmpty(Neighborhood))
                    completeAddress.Append($" - {Neighborhood}");

                if (AddressTranslation != null)
                    completeAddress.Append($" - {AddressTranslation.CityName} - {AddressTranslation.TwoLetterIsoCode} - {AddressTranslation.CountryName}");

                return completeAddress.ToString();
            }
        }
    }
}
