﻿using Tnf.Dto;

namespace Thex.Common.Dto
{
    public class CountrySubdivisionTranslationDto : BaseDto
    {
        public int Id { get; set; }
        public static CountrySubdivisionTranslationDto NullInstance = null;

        public string LanguageIsoCode { get; set; }
        public string Name { get; set; }
        public int CountrySubdivisionId { get; set; }
        public string Nationality { get; set; }
    }
}
