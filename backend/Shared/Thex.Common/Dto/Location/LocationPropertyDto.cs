﻿namespace Thex.Common.Dto.Location
{
    public class LocationPropertyDto
    {
        public string PropertyCountryCode { get; set; }
        public string PropertyDistrictCode { get; set; }
    }
}
