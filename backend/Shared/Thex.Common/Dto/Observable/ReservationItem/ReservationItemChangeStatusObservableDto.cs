﻿using System;
using Thex.Common.Enumerations;

namespace Thex.Common.Dto.Observable
{
    public class ReservationItemChangeStatusObservableDto
    {
        public int RoomTypeId { get; set; }
        public DateTime InitialDate { get; set; }
        public DateTime EndDate { get; set; }
        public ReservationStatus ReservationItemStatusIdNew { get; set; }
        public ReservationStatus? ReservationItemStatusIdOld { get; set; }
    }
}
