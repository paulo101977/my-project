﻿using System;

namespace Thex.Common.Dto.Observable
{
    public class ReservationItemChangeRoomTypeOrPeriodObservableDto
    {
        public int OldRoomTypeId { get; set; }
        public int NewRoomTypeId { get; set; }
        public DateTime OldInitialDate { get; set; }
        public DateTime OldEndDate { get; set; }
        public DateTime? NewInitialDate { get; set; }
        public DateTime? NewEndDate { get; set; }
    }
}
