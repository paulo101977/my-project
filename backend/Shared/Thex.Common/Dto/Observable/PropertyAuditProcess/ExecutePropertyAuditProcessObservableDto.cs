﻿using System.Collections.Generic;

namespace Thex.Common.Dto.Observable
{
    public class ExecutePropertyAuditProcessObservableDto
    {
        public IEnumerable<int> RoomTypeIdList { get; set; }
    }
}
