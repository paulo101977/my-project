﻿using System;

namespace Thex.Common.Dto.Observable
{
    public class RoomChangeRoomTypeObservableDto
    {
        public DateTime InitialDate { get; set; }
        public int PropertyId { get; set; }
        public int RoomTypeIdOld { get; set; }
        public int RoomTypeIdNew { get; set; }
    }
}
