﻿using System;

namespace Thex.Common.Dto.Observable
{
    public class CreateRoomObservableDto
    {
        public DateTime InitialDate { get; set; }
        public int PropertyId { get; set; }
        public int RoomTypeId { get; set; }
    }
}
