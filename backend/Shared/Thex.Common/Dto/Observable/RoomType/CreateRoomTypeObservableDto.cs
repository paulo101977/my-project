﻿using System;

namespace Thex.Common.Dto.Observable
{
    public class CreateRoomTypeObservableDto
    {
        public int PropertyId { get; set; }
        public int RoomTypeId { get; set; }
        public DateTime InitialDate { get; set; }
    }
}
