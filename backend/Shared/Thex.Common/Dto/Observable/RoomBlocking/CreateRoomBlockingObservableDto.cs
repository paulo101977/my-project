﻿using System;
using System.Collections.Generic;

namespace Thex.Common.Dto.Observable
{
    public class CreateRoomBlockingObservableDto
    {
        public IDictionary<int, int> RoomTypeIdWithRoomQuantityDictionary { get; set; }
        public DateTime InitialDate { get; set; }
        public DateTime EndDate { get; set; }

        public CreateRoomBlockingObservableDto()
        {
            RoomTypeIdWithRoomQuantityDictionary = new Dictionary<int, int>();
        }
    }
}
