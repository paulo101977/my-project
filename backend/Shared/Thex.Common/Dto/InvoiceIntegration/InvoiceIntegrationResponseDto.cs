﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Thex.Common.Dto.InvoiceIntegration
{
    public class InvoiceIntegrationResponseDto
    {
        [JsonProperty("tipo")]
        public string Type { get; set; }

        [JsonProperty("nfeChaveAcesso")]
        public string Key { get; set; }

        [JsonProperty("nfeNumeroProtocolo")]
        public string Protocol { get; set; }

        [JsonProperty("nfeId")]
        public string InternalId { get; set; }

        [JsonProperty("nfeIdExterno")]
        public string ExternalId { get; set; }

        [JsonProperty("nfeStatus")]
        public string Status { get; set; }

        [JsonProperty("nfeMotivoStatus")]
        public string Reason { get; set; }

        [JsonProperty("nfeLinkPdf")]
        public string UrlPdf { get; set; }

        [JsonProperty("nfeLinkXml")]
        public string UrlXml { get; set; }

        [JsonProperty("nfeNumeroRps")]
        public string RpsNumber { get; set; }

        [JsonProperty("nfeSerieRps")]
        public string RpsSerial { get; set; }

        [JsonProperty("nfeNumero")]
        public string Number { get; set; }

        [JsonProperty("numero")]
        public string InvoiceNumber { get; set; }

        [JsonProperty("nfeDataCompetencia")]
        public string EmissionDate { get; set; }

        [JsonProperty("nfeCodigoVerificacao")]
        public string VerificationCode { get; set; }
    }
}
