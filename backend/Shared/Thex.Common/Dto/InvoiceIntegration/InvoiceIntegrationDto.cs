﻿using System;
using System.Collections.Generic;
using System.Text;
using Tnf.Dto;

namespace Thex.Dto.Integration
{
    public class InvoiceIntegrationDto : BaseDto
    {
        public InvoiceIntegrationDto()
        {
            this.ServiceInvoice = new List<IntegrationServiceInvoiceDTO>();
            this.ProductInvoice = new List<IntegrationProductInvoiceDTO>();
            this.Client = new IntegrationClientDto();
        }

        public string PropertyUUId { get; set; }
        public string TenantId { get; set; }
        public string ExternalId { get; set; }
        public string ExternalTypeId { get; set; }
        public string Environment { get; set; }
        public string PropertyDistrict { get; set; }
        public string IntegrationCode { get; set; }
        public string TokenClient { get; set; }
        public int? Number { get; set; }
        public string SerialNumber { get; set; }
        public string FacturaReferenceId { get; set; }
        public string FacturaPaymentReferenceId { get; set; }
        public string FacturaSeriesId { get; set; }
        public string ReasonName { get; set; }
        public string Observations { get; set; }
        public List<IntegrationServiceInvoiceDTO> ServiceInvoice { get; set; }
        public List<IntegrationProductInvoiceDTO> ProductInvoice { get; set; }
        public IntegrationClientDto Client { get; set; }
    }

    public class IntegrationClientDto : BaseDto
    {
        public IntegrationClientDto()
        {
            this.Address = new IntegrationAddressDto();
        }

        public IntegrationAddressDto Address { get; set; }

        public string Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Document { get; set; }
        public string Phone { get; set; }
        public int DocumentTypeId { get; set; }
    }

    public class IntegrationAddressDto : BaseDto
    {
        public IntegrationAddressDto()
        {
            this.City = new IntegrationCityDTO();
        }

        public string Country { get; set; }
        public string District { get; set; }
        public string StreetName { get; set; }
        public string StreetNumber { get; set; }
        public string AdditionalInformation { get; set; }
        public string PostalCode { get; set; }
        public string Neighborhood { get; set; }
        public IntegrationCityDTO City { get; set; }
    }

    public class IntegrationCityDTO : BaseDto
    {
        public string Code { get; set; }
        public string Name { get; set; }
    }

    public class IntegrationServiceInvoiceDTO : BaseDto
    {
        public string Nbs { get; set; }
        public decimal Amount { get; set; }
        public string Description { get; set; }

        /// <summary>
        /// Apenas para Portugal
        /// </summary>
        public string ProductId { get; set; }

        /// <summary>
        /// Apenas para Portugal
        /// </summary>
        public string ServiceId { get; set; }
    }

    public class IntegrationProductInvoicePaymentsDto
    {
        public decimal Amount { get; set; }
        public string PaymentDescription { get; set; }

        public IntegrationProductInvoicePaymentCreditDetailsDto CreditDetails { get; set; }
    }

    public class IntegrationProductInvoicePaymentCreditDetailsDto
    {
        private string _defaultPaymentType = "IntegradoAoSistemaDeGestao";

        public IntegrationProductInvoicePaymentCreditDetailsDto()
        {
            this.Type = _defaultPaymentType;
        }

        public string Type { get; private set; }
        public string Document { get; set; }
        public string PlasticBrand { get; set; }
        public string NSU { get; set; }
    }

    public class IntegrationProductInvoiceDTO : BaseDto
    {
        public IntegrationProductInvoiceDTO()
        {
            this.PaymentDetails = new List<IntegrationProductInvoicePaymentsDto>();
        }

        public string ProductId { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public string Ncm { get; set; }
        public string Barcode { get; set; }
        public decimal Quantity { get; set; }
        public decimal Amount { get; set; }
        public decimal Rate { get; set; }
        public string Cest { get; set; }

        public List<IntegrationProductInvoicePaymentsDto> PaymentDetails { get; set; }
    }
}
