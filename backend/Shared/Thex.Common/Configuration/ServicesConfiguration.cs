﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;

namespace Thex.Common
{
    public class ServicesConfiguration
    {
        public ServicesConfiguration()
        {
            this.ElasticSearch = new ElasticSearch();
        }

        public string PreferenceAPI { get; set; }
        public string BarCodeAPI { get; set; }
        public ElasticSearch ElasticSearch { get; set; }
        public string DatabaseLog { get; set; }
    }

    public class ElasticSearch
    {
        public string Uri { get; set; }
        public string Accesskey { get; set; }
        public string Secretkey { get; set; }
    }
}