﻿using System;
using Tnf.Notifications;

namespace Thex.Common.Extensions
{
    public static class EnumExtensions
    {
        public static readonly INotificationHandler _notification;

        static EnumExtensions()
        {
            _notification = null;
        }


        private static void RaiseNotification(Error error, params object[] formats)
        {
            _notification.Raise(_notification
                                .DefaultBuilder
                                .AsError()
                                .WithResultStatus(ResultStatus.BadRequest)
                                .FromErrorEnum(error)
                                .WithMessage(AppConsts.LocalizationSourceName, error)
                                .WithDetailedMessage(AppConsts.LocalizationSourceName, error)
                                .WithMessageFormat(formats)
                                .Build());
        }

        #region String to Enum
        public static T ParseEnum<T>(string inString, bool ignoreCase = true, bool isChar = true, bool throwNotification = true) where T : struct
        {
            return (T)ParseEnum<T>(inString, default(T), isChar, ignoreCase, throwNotification);
        }

        private static T ParseEnum<T>(string inString, T defaultValue,
                                      bool isChar = true, bool ignoreCase = true, bool throwException = false) where T : struct
        {
            T returnEnum = defaultValue;

            if (!typeof(T).IsEnum)
            {
                RaiseNotification(Error.InvalidEnumType, typeof(T).ToString());
            }
            if (String.IsNullOrEmpty(inString))
            {
                RaiseNotification(Error.InvalidInputString, inString, typeof(T).ToString());
            }
            try
            {
                inString = inString.Trim();
                if (isChar)
                {
                    char charTmp = inString.Trim()[0];
                    if (Enum.IsDefined(typeof(T), (int)charTmp))
                    {
                        returnEnum = (T)Enum.ToObject(typeof(T), charTmp);
                    }
                }
                else
                {
                    bool success = Enum.TryParse<T>(inString, ignoreCase, out returnEnum);
                    if (!success && throwException)
                    {
                        RaiseNotification(Error.InvalidCast, typeof(T).ToString());
                    }
                }
            }
            catch (Exception)
            {
                RaiseNotification(Error.InvalidCast, typeof(T).ToString());
            }

            return returnEnum;
        }
        #endregion

        #region Int to Enum
        public static T ParseEnum<T>(int input, bool throwException = true) where T : struct
        {
            return (T)ParseEnum<T>(input, default(T), throwException);
        }
        public static T ParseEnum<T>(int input, T defaultValue, bool throwException = false) where T : struct
        {
            T returnEnum = defaultValue;
            if (!typeof(T).IsEnum)
            {
                RaiseNotification(Error.InvalidEnumType, typeof(T).ToString());
            }
            if (Enum.IsDefined(typeof(T), input))
            {
                returnEnum = (T)Enum.ToObject(typeof(T), input);
            }
            else
            {
                if (throwException)
                {
                    RaiseNotification(Error.InvalidCast);
                }
            }

            return returnEnum;

        }
        #endregion

        #region String Extension Methods for Enum Parsing
        public static T ToEnum<T>(this string inString, bool ignoreCase = true, bool isChar = true, bool throwException = true) where T : struct
        {
            return (T)ParseEnum<T>(inString, default(T), ignoreCase, isChar, throwException);
        }
        public static T ToEnum<T>(this string inString, T defaultValue, bool ignoreCase = true, bool isChar = true, bool throwException = false) where T : struct
        {
            return (T)ParseEnum<T>(inString, defaultValue, ignoreCase, isChar, throwException);
        }
        #endregion

        #region Int Extension Methods for Enum Parsing
        public static T ToEnum<T>(this int input, bool throwException = true) where T : struct
        {
            return (T)ParseEnum<T>(input, default(T), throwException);
        }

        public static T ToEnum<T>(this int input, T defaultValue, bool throwException = false) where T : struct
        {
            return (T)ParseEnum<T>(input, defaultValue, throwException);
        }
        #endregion


        public enum Error
        {
            InvalidEnumType,
            InvalidCast,
            InvalidInputString
        }
    }
}
