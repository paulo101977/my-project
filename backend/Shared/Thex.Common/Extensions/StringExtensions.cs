﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace Thex.Common.Extensions
{
    public static class StringExtensions
    {
        public static string RemoveAccentuation(this string texto)
        {
            const string letrasComAcentos = "ÄÅÁÂÀÃäáâàãÉÊËÈéêëèÍÎÏÌíîïìÖÓÔÒÕöóôòõÜÚÛüúûùÇçº&@";
            const string letrasSemAcentos = "AAAAAAaaaaaEEEEeeeeIIIIiiiiOOOOOoooooUUUuuuuCcoea";
            var ret = texto;

            for (var i = 0; i < letrasComAcentos.Length; i++)
            {
                ret = ret.Replace(letrasComAcentos[i], letrasSemAcentos[i]);
            }

            return ret;
        }

        public static string RemoveSpecialChars(this string valor)
        {
            Regex r = new Regex("(?:[^a-z0-9 ]|(?<=['\"])s)", RegexOptions.IgnoreCase | RegexOptions.CultureInvariant | RegexOptions.Compiled);
            return r.Replace(valor, String.Empty);
        }

        public static string RemoveAccentuationAndSpecialChars(this string texto)
        {
            const string letrasComAcentos = "ÄÅÁÂÀÃäáâàãÉÊËÈéêëèÍÎÏÌíîïìÖÓÔÒÕöóôòõÜÚÛüúûùÇçº&@";
            const string letrasSemAcentos = "AAAAAAaaaaaEEEEeeeeIIIIiiiiOOOOOoooooUUUuuuuCcoea";
            var ret = texto;

            for (var i = 0; i < letrasComAcentos.Length; i++)
            {
                ret = ret.Replace(letrasComAcentos[i], letrasSemAcentos[i]);
            }

            return ret.RemoveSpecialChars();
        }
    }
}
