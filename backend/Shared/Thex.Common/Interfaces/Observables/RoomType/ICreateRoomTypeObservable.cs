﻿using System;
using Thex.Common.Dto.Observable;

namespace Thex.Common.Interfaces.Observables
{
    public interface ICreateRoomTypeObservable
    {
        void ExecuteActionAfterCreateRoomType(CreateRoomTypeObservableDto dto);
    }
}
