﻿using Thex.Common.Dto.Observable;

namespace Thex.Common.Interfaces.Observables
{
    public interface IDeleteRoomObservable
    {
        void ExecuteActionAfterDeleteRoom(DeleteRoomObservableDto dto);
    }
}
