﻿using Thex.Common.Dto.Observable;

namespace Thex.Common.Interfaces.Observables
{
    public interface IRoomChangeRoomTypeObservable
    {
        void ExecuteActionAfterRoomChangeRoomType(RoomChangeRoomTypeObservableDto dto);
    }
}
