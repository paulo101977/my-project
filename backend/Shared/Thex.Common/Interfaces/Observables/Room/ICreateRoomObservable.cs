﻿using Thex.Common.Dto.Observable;

namespace Thex.Common.Interfaces.Observables
{
    public interface ICreateRoomObservable
    {
        void ExecuteActionAfterCreateRoom(CreateRoomObservableDto dto);
    }
}
