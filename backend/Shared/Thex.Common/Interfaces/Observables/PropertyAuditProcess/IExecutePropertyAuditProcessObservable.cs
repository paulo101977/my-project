﻿using Thex.Common.Dto.Observable;

namespace Thex.Common.Interfaces.Observables
{
    public interface IExecutePropertyAuditProcessObservable
    {
        void ExecuteActionAfterExecutePropertyAuditProcess(ExecutePropertyAuditProcessObservableDto dto);
    }
}
