﻿using Thex.Common.Dto.Observable;

namespace Thex.Common.Interfaces.Observables
{
    public interface IReservationItemChangeRoomTypeOrPeriodObservable
    {
        void ExecuteActionAfterReservationItemChangeRoomTypeOrPeriod(ReservationItemChangeRoomTypeOrPeriodObservableDto dto);
    }
}
