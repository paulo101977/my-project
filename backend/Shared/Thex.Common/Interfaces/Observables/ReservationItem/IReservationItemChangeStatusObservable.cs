﻿using Thex.Common.Dto.Observable;

namespace Thex.Common.Interfaces.Observables
{
    public interface IReservationItemChangeStatusObservable
    {
        void ExecuteActionAfterChangeReservationItemStatus(ReservationItemChangeStatusObservableDto dto);
    }
}
