﻿using Thex.Common.Dto.Observable;

namespace Thex.Common.Interfaces.Observables
{
    public interface ICreateRoomBlockingObservable
    {
        void ExecuteActionAfterCreateRoomBlocking(CreateRoomBlockingObservableDto dto);
    }
}
