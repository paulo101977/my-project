﻿using Thex.Common.Dto.Observable;

namespace Thex.Common.Interfaces.Observables
{
    public interface IDeleteRoomBlockingObservable
    {
        void ExecuteActionAfterDeleteRoomBlocking(DeleteRoomBlockingObservableDto dto);
    }
}
