﻿using System;
using System.Collections.Generic;
using System.Text;
using Tnf.Dto;

namespace Thex.Log.Dto
{
    public class ThexListDto<IDto> : ListDto<IDto>
    {
        public int TotalItems { get; set; }
        public DateTime? LastProcessDate { get; set; }
    }
}
