﻿using System;
using System.Collections.Generic;
using System.Text;
using Thex.Common.Dto;

namespace Thex.Log.Dto.ReservationDownload
{
    public class ReservationDownloadRequestDto 
    {
        public int PropertyId { get; set; }
        public string PropertyTimeZone { get; set; }
        public string BusinessSourceId { get; set; }
        public string BusinessSourceName { get; set; }
        public string PartnerId { get; set; }
        public string PartnerName { get; set; }
        public bool Status { get; set; }
        public string ReservationCode { get; set; }
        public ReservationHeaderHigsDto ReservationHeaderHigsDto { get; set; }

        public ICollection<ReservationDownloadSituationDto> SituationList { get; set; }

        public ReservationDownloadRequestDto()
        {
            this.SituationList = new HashSet<ReservationDownloadSituationDto>();
        }
    }
}
