﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Thex.Log.Dto.ReservationDownload
{
    public class ReservationDownloadDetailsDto
    {
        public string Id { get; set; }
        public string BusinessSourceId { get; set; }
        public string BusinessSourceName { get; set; }
        public string PartnerReservationNumber { get; set; }
        public string ReservationCode { get; set; }
        public string PartnerId { get; set; }
        public string PartnerName { get; set; }
        public string Response { get; set; }

        public ICollection<ReservationDownloadSituationDto> SituationList { get; set; }

        public ReservationDownloadDetailsDto()
        {
            this.SituationList = new HashSet<ReservationDownloadSituationDto>();
        }
    }
}
