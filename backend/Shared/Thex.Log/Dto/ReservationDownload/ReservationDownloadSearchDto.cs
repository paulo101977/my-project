﻿using System;

namespace Thex.Log.Dto.ReservationDownload
{
    public class ReservationDownloadSearchDto
    {
        public string ReservationDownloadId { get; set; }
        public DateTime? Date { get; set; }
        public string ReservationNumber { get; set; }
        public string BusinessSourceId { get; set; }
        public string PartnerId { get; set; }
        public int? Page { get; set; }
        public int? PageSize { get; set; }
    }
}
