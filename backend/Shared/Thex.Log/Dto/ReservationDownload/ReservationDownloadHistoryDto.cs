﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Thex.Log.Dto.ReservationDownload
{
    public class ReservationDownloadHistoryDto
    {
        public string Id { get; set; }
        public int OperationType { get; set; }
        public string OperationName { get; set; }
        public DateTime LastUpdateDate { get; set; }
        public bool Status { get; set; }
        public string Response { get; set; }
        public string ResponseHash { get; set; }
        public string Message { get; set; }

        public ReservationDownloadDetailsDto ReservationDownloadDetailsDto { get; set; }
    }
}
