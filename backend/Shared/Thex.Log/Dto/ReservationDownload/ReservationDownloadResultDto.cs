﻿using System;
using System.Collections.Generic;
using Tnf.Dto;

namespace Thex.Log.Dto.ReservationDownload
{
    public class ReservationDownloadResultDto : IDto
    {
        public string Id { get; set; }
        public string LastHistoryId { get; set; }
        public bool Status { get; set; }
        public int OperationType { get; set; }
        public string OperationName { get; set; }
        public string BusinessSourceId { get; set; }
        public string BusinessSourceName { get; set; }
        public string PartnerReservationNumber { get; set; }
        public string ReservationCode { get; set; }
        public string PartnerId { get; set; }
        public string PartnerName { get; set; }
        public string Message { get; set; }
        public DateTime? LastUpdateDate { get; set; }

        public IList<string> Expandables { get ; set ; }
    }
}
