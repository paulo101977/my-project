﻿using System;

namespace Thex.Log.Dto.ReservationDownload
{
    public class ReservationDownloadSituationDto
    {
        public string Id { get; set; }
        public bool Status { get; set; }
        public DateTime FirstAttemptDate { get; set; }
        public DateTime LastAttemptDate { get; set; }
        public DateTime? ResolutionDate { get; set; }
        public int StatusCode { get; set; }
        public string Message { get; set; }
        public DateTime? OperationDate { get; set; }
        public string Key { get; set; }
        public int? ErrorsNumber { get; set; }
    }
}
