﻿using System;

namespace Thex.Log.Dto
{
    public class ProcessingHistoryDto
    {
        public DateTime ProcessingDate { get; set; }
        public int ProcessingType { get; set; }
        public int PropertyId { get; set; }
    }
}
