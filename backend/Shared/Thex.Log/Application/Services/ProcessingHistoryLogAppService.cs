﻿using System.Threading.Tasks;
using Thex.Log.Application.Adapter.ProcessingHistoryLog;
using Thex.Log.Application.Interfaces;
using Thex.Log.Dto;
using Thex.Log.Infra.Entities;
using Thex.Log.Infra.Interfaces;
using Thex.Log.Infra.Interfaces.Read;
using Tnf.Application.Services;
using Tnf.Notifications;

namespace Thex.Log.Application.Services
{
    public class ProcessingHistoryLogAppService : ApplicationService, IProcessingHistoryLogAppService
    {
        private readonly IProcessingHistoryRepository _processingHistoryRepository;
        private readonly IProcessingHistoryAdapter _proccesingHistoryAdapter;
        private readonly IProcessingHistoryReadRepository _processingHistoryReadRepository;

        public ProcessingHistoryLogAppService(
            INotificationHandler notification,
            IProcessingHistoryRepository processingHistoryRepository,
            IProcessingHistoryReadRepository processingHistoryReadRepository,
            IProcessingHistoryAdapter proccesingHistoryAdapter
            )
            : base(notification)
        {
            _processingHistoryRepository = processingHistoryRepository;
            _proccesingHistoryAdapter = proccesingHistoryAdapter;
            _processingHistoryReadRepository = processingHistoryReadRepository;
        }

        public async Task SendLogAsync(ProcessingHistoryDto dto)
        {
            if (dto == null) return;

            var entity = await _processingHistoryReadRepository.GetByProcessingTypeAndPropertyIdAsync(dto.ProcessingType, dto.PropertyId);
            var processingHistory = new ProcessingHistory();

            if(entity == null)
            {
                processingHistory = _proccesingHistoryAdapter.MapCreate(dto).Build();

                await _processingHistoryRepository.InsertAsync(processingHistory);
            }
            else
            {
                processingHistory = _proccesingHistoryAdapter.MapUpdate(entity, dto).Build();

                await _processingHistoryRepository.UpdateAsync(processingHistory);
            }
        }
    }
}
