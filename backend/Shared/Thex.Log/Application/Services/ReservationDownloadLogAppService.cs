﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Thex.Common.Dto;
using Thex.Kernel;
using Thex.Log.Application.Adapter.ReservationDownloadLog;
using Thex.Log.Application.Interfaces;
using Thex.Log.Dto;
using Thex.Log.Dto.ReservationDownload;
using Thex.Log.Infra.Entities.ReservationDownloadLog;
using Thex.Log.Infra.Enumerations;
using Thex.Log.Infra.Interfaces;
using Thex.Log.Infra.Interfaces.Read;
using Tnf.Application.Services;
using Tnf.Notifications;

namespace Thex.Log.Application
{
    public class ReservationDownloadLogAppService : ApplicationService, IReservationDownloadLogAppService
    {
        private readonly IReservationDownloadRepository _reservationDownloadRepository;
        private readonly IReservationDownloadReadRepository _reservationDownloadReadRepository;
        private readonly IReservationDownloadAdapter _reservationDownloadAdapter;
        private readonly IProcessingHistoryReadRepository _processingHistoryReadRepository;
        private readonly IApplicationUser _applicationUser;

        public ReservationDownloadLogAppService(
            IReservationDownloadAdapter reservationDownloadAdapter,
            IReservationDownloadRepository reservationDownloadRepository,
            IProcessingHistoryReadRepository processingHistoryReadRepository,
            IApplicationUser applicationUser,
            IReservationDownloadReadRepository reservationDownloadReadRepository,
            INotificationHandler notification)
            : base(notification)
        {
            _reservationDownloadRepository = reservationDownloadRepository;
            _reservationDownloadReadRepository = reservationDownloadReadRepository;
            _reservationDownloadAdapter = reservationDownloadAdapter;
            _processingHistoryReadRepository = processingHistoryReadRepository;
            _applicationUser = applicationUser;
        }

        public async Task SendLogAsync(ReservationDownloadRequestDto dto)
        {
            if (dto == null) return;

            var entityId = await _reservationDownloadReadRepository.GetByParametersAsync(dto.ReservationHeaderHigsDto.ReservationNumberHigs, dto.PropertyId);
            if (!string.IsNullOrWhiteSpace(entityId))
                await UpdateAsync(dto, entityId);
            else
                await CreateAsync(dto);
        }

        public async Task<ThexListDto<ReservationDownloadResultDto>> GetAllByFiltersAsync(ReservationDownloadSearchDto request, int propertyId)
        {
            var result = await _reservationDownloadReadRepository.GetAllByFiltersAsync(request, propertyId);
            result.LastProcessDate = (await _processingHistoryReadRepository.FirstOrDefaultAsync(ph => ph.PropertyId == int.Parse(_applicationUser.PropertyId)
                                                                                && ph.ProcessingType == (int)ProcessingTypeEnum.ReservationDownload))?.ProcessingDate.ToZonedDateTimeLoggedUser(_applicationUser);

            return result;
        }
            

        public async Task<ThexListDto<ReservationDownloadHistoryDto>> GetAllHistoryByIdAsync(string reservDownloadId)
            => await _reservationDownloadReadRepository.GetAllHistoryByReservationDownloadIdAsync(reservDownloadId);

        #region Private Methods
        private async Task CreateAsync(ReservationDownloadRequestDto dto)
        {
            ResetCreditCardData(dto);

            var reservationDownloadHistoryList = new List<ReservationDownloadHistory>();
            var situationList = new List<ReservationDownloadSituation>();

            situationList.Add(
                _reservationDownloadAdapter
                    .MapCreateReservationDownloadSituation(GenerateSituationReservationProcessed())
                    .Build());

            foreach (var situationDto in dto.SituationList)
                situationList.Add(_reservationDownloadAdapter.MapCreateReservationDownloadSituation(situationDto).Build());

            reservationDownloadHistoryList.Add(_reservationDownloadAdapter.MapCreateReservationDownloadHistory(dto, situationList).Build());

            var reservationDownload = _reservationDownloadAdapter.MapCreateReservationDownload(dto, reservationDownloadHistoryList).Build();

            await _reservationDownloadRepository.InsertAsync(reservationDownload);
        }

        private async Task UpdateAsync(ReservationDownloadRequestDto dto, string entityId)
        {
            ResetCreditCardData(dto);

            var entity = await _reservationDownloadReadRepository.FirstOrDefaultAsync(rd => rd.Id == entityId);
            if (entity == null) return;

            var reservationDownloadHistoryList = entity.ReservationDownloadHistoryList.ToList();

            var entityHistory = entity.ReservationDownloadHistoryList.FirstOrDefault(rdh => rdh.OperationType == (int)dto.ReservationHeaderHigsDto.OperationType && rdh.ResponseHash.Equals(GenerateHash(dto.ReservationHeaderHigsDto.RequesDto)));

            if (entityHistory != null && entityHistory.ReservationDownloadSituationList.Any(rds => rds.Status && rds.StatusCode == (int)ReservationDownloadSituationEnum.ProcessedReservation))
                return;

            var situationList = entityHistory != null ? entityHistory.ReservationDownloadSituationList.ToList() : new List<ReservationDownloadSituation>();

            foreach (var situationDto in dto.SituationList)
            {
                var entitySituation = entityHistory?.ReservationDownloadSituationList.FirstOrDefault(rds => rds.StatusCode == situationDto.StatusCode);
                if (entityHistory == null || entitySituation == null)
                    situationList.Add(_reservationDownloadAdapter.MapCreateReservationDownloadSituation(situationDto).Build());
                else
                {
                    if (!situationDto.Status || !situationList.Any(s => s.StatusCode == situationDto.StatusCode && s.Status == situationDto.Status))
                    {
                        var index = entityHistory.ReservationDownloadSituationList.ToList().FindIndex(rds => rds.StatusCode == situationDto.StatusCode);
                        situationList[index] = _reservationDownloadAdapter.MapUpdateReservationDownloadSituation(entitySituation, situationDto).Build();
                    }
                }
            }

            if (entityHistory != null)
            {
                var index = entity.ReservationDownloadHistoryList.ToList().FindIndex(rdh => rdh.Id == entityHistory.Id);
                reservationDownloadHistoryList[index] = _reservationDownloadAdapter.MapUpdateReservationDownloadHistory(entityHistory, dto, situationList).Build();
            }
            else
            {
                situationList.Add(
                _reservationDownloadAdapter
                    .MapCreateReservationDownloadSituation(GenerateSituationReservationProcessed())
                    .Build());

                reservationDownloadHistoryList.Add(_reservationDownloadAdapter.MapCreateReservationDownloadHistory(dto, situationList).Build());
            }
                

            var reservationDownload = _reservationDownloadAdapter.MapUpdateReservationDownload(entity, dto, reservationDownloadHistoryList).Build();

            await _reservationDownloadRepository.UpdateAsync(reservationDownload);
        }

        private string GenerateHash(ReservationHigsDto dto)
        {
            var json = JsonConvert.SerializeObject(dto);
            return Convert.ToBase64String(SHA384.Create().ComputeHash(Encoding.UTF8.GetBytes(json)));
        }

        private void ResetCreditCardData(ReservationDownloadRequestDto dto)
            => dto.ReservationHeaderHigsDto.RequesDto.CreditCardData = null;

        private ReservationDownloadSituationDto GenerateSituationReservationProcessed()
           => new ReservationDownloadSituationDto()
           {
               OperationDate = DateTime.UtcNow,
               StatusCode = (int)ReservationDownloadSituationEnum.ProcessedReservation,
               Status = false
           };
        #endregion
    }
}
