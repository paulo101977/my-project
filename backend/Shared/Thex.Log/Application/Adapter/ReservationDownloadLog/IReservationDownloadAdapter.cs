﻿using System.Collections.Generic;
using Thex.Log.Dto;
using Thex.Log.Dto.ReservationDownload;
using Thex.Log.Infra.Entities.ReservationDownloadLog;

namespace Thex.Log.Application.Adapter.ReservationDownloadLog
{
    public interface IReservationDownloadAdapter
    {
        ReservationDownload.Builder MapCreateReservationDownload(ReservationDownloadRequestDto dto, ICollection<ReservationDownloadHistory> historyList);
        ReservationDownloadHistory.Builder MapCreateReservationDownloadHistory(ReservationDownloadRequestDto dto, ICollection<ReservationDownloadSituation> errorList);
        ReservationDownloadHistory.Builder MapUpdateReservationDownloadHistory(ReservationDownloadHistory entity, ReservationDownloadRequestDto dto, ICollection<ReservationDownloadSituation> situationList);
        ReservationDownloadSituation.Builder MapCreateReservationDownloadSituation(ReservationDownloadSituationDto dto);
        ReservationDownloadSituation.Builder MapUpdateReservationDownloadSituation(ReservationDownloadSituation entity, ReservationDownloadSituationDto dto);
        ReservationDownload.Builder MapUpdateReservationDownload(ReservationDownload entity, ReservationDownloadRequestDto dto, ICollection<ReservationDownloadHistory> historyList);
    }
}