﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using Thex.Log.Dto.ReservationDownload;
using Thex.Log.Infra.Entities.ReservationDownloadLog;
using Thex.Log.Infra.Enumerations;
using Tnf.Notifications;

namespace Thex.Log.Application.Adapter.ReservationDownloadLog
{
    public class ReservationDownloadAdapter : IReservationDownloadAdapter
    {
        public INotificationHandler NotificationHandler { get; }

        public ReservationDownloadAdapter(INotificationHandler notificationHandler)
        {
            NotificationHandler = notificationHandler;
        }

        public ReservationDownload.Builder MapCreateReservationDownload(ReservationDownloadRequestDto dto, ICollection<ReservationDownloadHistory> historyList)
        {
            var builder = new ReservationDownload.Builder(NotificationHandler)
                  .WithId(Guid.NewGuid().ToString())
                  .WithBusinessSourceId(dto.BusinessSourceId)
                  .WithBusinessSourceName(dto.BusinessSourceName)
                  .WithPartnerId(dto.PartnerId)
                  .WithPartnerName(dto.PartnerName)
                  .WithPropertyId(dto.PropertyId)
                  .WithPartnerReservationNumber(dto.ReservationHeaderHigsDto.ReservationNumber)
                  .WithExternalReservationNumber(dto.ReservationHeaderHigsDto.ReservationNumberHigs)
                  .WithReservationCode(dto.ReservationCode)
                  .WithReservationDownloadHistoryList(historyList);

            return builder;
        }

        public ReservationDownload.Builder MapUpdateReservationDownload(ReservationDownload entity, ReservationDownloadRequestDto dto, ICollection<ReservationDownloadHistory> historyList)
        {
            var builder = new ReservationDownload.Builder(NotificationHandler)
                 .WithId(entity.Id)  
                 .WithBusinessSourceId(dto.BusinessSourceId)
                 .WithBusinessSourceName(dto.BusinessSourceName)
                 .WithPartnerId(dto.PartnerId)
                 .WithPartnerName(dto.PartnerName)
                 .WithPropertyId(entity.PropertyId)
                 .WithExternalReservationNumber(entity.ExternalReservationNumber)
                 .WithPartnerReservationNumber(entity.PartnerReservationNumber)
                 .WithReservationCode(dto.ReservationCode)
                 .WithReservationDownloadHistoryList(historyList);

            return builder;
        }


        public ReservationDownloadHistory.Builder MapCreateReservationDownloadHistory(ReservationDownloadRequestDto dto, ICollection<ReservationDownloadSituation> situationList)
        {
            var builder = new ReservationDownloadHistory.Builder(NotificationHandler)
                  .WithId(Guid.NewGuid().ToString())
                  .WithLastUpdate(situationList.LastOrDefault().LastOperationDate)
                  .WithOperationType((int)dto.ReservationHeaderHigsDto.OperationType)
                  .WithResponse(JsonConvert.SerializeObject(dto.ReservationHeaderHigsDto.RequesDto))
                  .WithReservationDownloadSituationList(situationList);

            return builder;
        }

        public ReservationDownloadHistory.Builder MapUpdateReservationDownloadHistory(ReservationDownloadHistory entity, ReservationDownloadRequestDto dto, ICollection<ReservationDownloadSituation> situationList)
        {
            var builder = new ReservationDownloadHistory.Builder(NotificationHandler)
                  .WithId(entity.Id)
                  .WithLastUpdate(situationList.Max(s => s.LastOperationDate))
                  .WithOperationType(entity.OperationType)
                  .WithResponse(entity.Response)
                  .WithReservationDownloadSituationList(situationList);

            return builder;
        }

        public ReservationDownloadSituation.Builder MapCreateReservationDownloadSituation(ReservationDownloadSituationDto dto)
        {
            var builder = new ReservationDownloadSituation.Builder(NotificationHandler)
                  .WithId(Guid.NewGuid().ToString())
                  .WithFirstOperationDate(dto.OperationDate.Value)
                  .WithLastOperationDate(dto.OperationDate.Value)
                  .WithResolutionDate(dto.Status ? dto.OperationDate : (DateTime?)null)
                  .WithStatus(dto.Status)
                  .WithStatusCode(dto.StatusCode)
                  .WithError(!dto.Status && dto.StatusCode != (int)ReservationDownloadSituationEnum.ProcessedReservation ? 1 : default(int))
                  .WithKey(dto.Key);

            return builder;
        }

        public ReservationDownloadSituation.Builder MapUpdateReservationDownloadSituation(ReservationDownloadSituation entity, ReservationDownloadSituationDto dto)
        {
            var builder = new ReservationDownloadSituation.Builder(NotificationHandler)
                  .WithId(entity.Id)
                  .WithFirstOperationDate(entity.FirstOperationDate)
                  .WithLastOperationDate(dto.Status ? entity.LastOperationDate : dto.OperationDate.Value)
                  .WithResolutionDate(dto.Status ? dto.OperationDate : (DateTime?)null)
                  .WithStatus(dto.Status)
                  .WithStatusCode(entity.StatusCode)
                  .WithError(!dto.Status && dto.StatusCode != (int)ReservationDownloadSituationEnum.ProcessedReservation ? entity.Errors + 1 : entity.Errors)
                  .WithKey(entity.Key);
                  

            return builder;
        }
    }
}
