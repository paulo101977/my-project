﻿using Thex.Log.Dto;
using Thex.Log.Infra.Entities;

namespace Thex.Log.Application.Adapter.ProcessingHistoryLog
{
    public interface IProcessingHistoryAdapter
    {
        ProcessingHistory.Builder MapCreate(ProcessingHistoryDto dto);
        ProcessingHistory.Builder MapUpdate(ProcessingHistory entity, ProcessingHistoryDto dto);
    }
}
