﻿using System;
using Thex.Log.Dto;
using Thex.Log.Infra.Entities;
using Tnf.Notifications;

namespace Thex.Log.Application.Adapter.ProcessingHistoryLog
{
    public class ProcessingHistoryAdapter : IProcessingHistoryAdapter
    {
        public INotificationHandler NotificationHandler { get; }

        public ProcessingHistoryAdapter(INotificationHandler notificationHandler)
        {
            NotificationHandler = notificationHandler;
        }

        public ProcessingHistory.Builder MapCreate(ProcessingHistoryDto dto)
        {
            var builder = new ProcessingHistory.Builder(NotificationHandler)
                  .WithId(Guid.NewGuid().ToString())
                  .WithPropertyId(dto.PropertyId)
                  .WithProcessingType(dto.ProcessingType)
                  .WithProcessingDate(dto.ProcessingDate);

            return builder;
        }

        public ProcessingHistory.Builder MapUpdate(ProcessingHistory entity, ProcessingHistoryDto dto)
        {
            var builder = new ProcessingHistory.Builder(NotificationHandler)
                  .WithId(entity.Id)
                  .WithPropertyId(entity.PropertyId)
                  .WithProcessingType(entity.ProcessingType)
                  .WithProcessingDate(dto.ProcessingDate);

            return builder;
        }
    }
}
