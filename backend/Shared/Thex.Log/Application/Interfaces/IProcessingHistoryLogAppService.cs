﻿using System.Threading.Tasks;
using Thex.Log.Dto;

namespace Thex.Log.Application.Interfaces
{
    public interface IProcessingHistoryLogAppService
    {
        Task SendLogAsync(ProcessingHistoryDto dto);
    }
}
