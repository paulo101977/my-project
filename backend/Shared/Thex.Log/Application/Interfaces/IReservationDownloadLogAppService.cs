﻿using System.Threading.Tasks;
using Thex.Log.Dto;
using Thex.Log.Dto.ReservationDownload;

namespace Thex.Log.Application.Interfaces
{
    public interface IReservationDownloadLogAppService
    {
        Task SendLogAsync(ReservationDownloadRequestDto dto);
        Task<ThexListDto<ReservationDownloadResultDto>> GetAllByFiltersAsync(ReservationDownloadSearchDto request, int propertyId);
        Task<ThexListDto<ReservationDownloadHistoryDto>> GetAllHistoryByIdAsync(string reservDownloadId);
    }
}
