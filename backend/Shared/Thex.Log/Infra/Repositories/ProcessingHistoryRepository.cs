﻿using Thex.Log.Infra.Entities;
using Thex.Log.Infra.Interfaces;
using Tnf.MongoDb.Provider;
using Tnf.MongoDb.Repositories;

namespace Thex.Log.Infra.Repositories
{
    public class ProcessingHistoryRepository : MongoDbRepository<ProcessingHistory, string>, IProcessingHistoryRepository
    {
        public ProcessingHistoryRepository(IMongoDbProvider provider)
            : base(provider)
        {
        }
    }
}
