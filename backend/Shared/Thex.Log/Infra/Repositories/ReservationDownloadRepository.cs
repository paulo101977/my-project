﻿using System;
using Thex.Log.Infra.Entities.ReservationDownloadLog;
using Thex.Log.Infra.Interfaces;
using Tnf.MongoDb.Provider;
using Tnf.MongoDb.Repositories;

namespace Thex.Log.Infra.Repositories
{
    public class ReservationDownloadRepository : MongoDbRepository<ReservationDownload, string>, IReservationDownloadRepository
    {
        public ReservationDownloadRepository(IMongoDbProvider provider)
            : base(provider)
        {
        }


    }
}
