﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Thex.Common;
using Thex.Common.Enumerations;
using Thex.Kernel;
using Thex.Log.Dto;
using Thex.Log.Dto.ReservationDownload;
using Thex.Log.Infra.Entities.ReservationDownloadLog;
using Thex.Log.Infra.Enumerations;
using Thex.Log.Infra.Interfaces.Read;
using Tnf.Localization;
using Tnf.MongoDb.Provider;
using Tnf.MongoDb.Repositories;
using X.PagedList;

namespace Thex.Log.Infra.Repositories.Read
{
    public class ReservationDownloadReadRepository : MongoDbRepository<ReservationDownload, string>, IReservationDownloadReadRepository
    {
        private readonly ILocalizationManager _localizationManager;
        private readonly IApplicationUser _applicationUser;

        public ReservationDownloadReadRepository(
            IMongoDbProvider provider,
            IApplicationUser applicationUser,
            ILocalizationManager localizationManager)
            : base(provider)
        {
            _localizationManager = localizationManager;
            _applicationUser = applicationUser;
        }

        public async Task<string> GetByParametersAsync(string externalReservationNumber, int propertyId)
        {
            return (await FirstOrDefaultAsync(rd => rd.ExternalReservationNumber.Equals(externalReservationNumber)
              && rd.PropertyId == propertyId))
         ?.Id;
        }

        public async Task<ThexListDto<ReservationDownloadResultDto>> GetAllByFiltersAsync(ReservationDownloadSearchDto request, int propertyId)
        {
            var baseQuery = await WhereAsync(rd => rd.PropertyId == propertyId);

            baseQuery = FilterByBusinessSourceId(request, baseQuery);
            baseQuery = FilterByDate(request, baseQuery);
            baseQuery = FilterByPartnerId(request, baseQuery);
            baseQuery = FilterByReservationNumber(request, baseQuery);

            var baseQueryPagination = baseQuery.OrderByDescending(x => x.ReservationDownloadHistoryList.LastOrDefault().LastUpdateDate).ToPagedList(request.Page.Value, request.PageSize.Value);

            var resultDtoList = new List<ReservationDownloadResultDto>();
            foreach (var reservDownload in baseQueryPagination)
            {
                var reservHistory = reservDownload.ReservationDownloadHistoryList.LastOrDefault();
                var message = string.Empty;
                if (reservHistory.ReservationDownloadSituationList.Any(rds => rds.Status && rds.StatusCode == (int)ReservationDownloadSituationEnum.ProcessedReservation))
                    message = _localizationManager.GetString(AppConsts.LocalizationSourceName, "SuccessToMigratedReservation");
                else
                    message = _localizationManager.GetString(AppConsts.LocalizationSourceName, "ErrorToMigratedReservation");


                resultDtoList.Add(new ReservationDownloadResultDto()
                {
                    Id = reservDownload.Id,
                    LastHistoryId = reservHistory.Id,
                    BusinessSourceId = reservDownload.BusinessSourceId,
                    BusinessSourceName = reservDownload.BusinessSourceName,
                    PartnerId = reservDownload.PartnerId,
                    PartnerName = reservDownload.PartnerName,
                    OperationName = _localizationManager.GetString(AppConsts.LocalizationSourceName, ((SituationTypeHigsEnum)reservHistory.OperationType).ToString()),
                    OperationType = reservHistory.OperationType,
                    Status = reservHistory.ReservationDownloadSituationList.Any(rds => rds.Status && rds.StatusCode == (int)ReservationDownloadSituationEnum.ProcessedReservation),
                    ReservationCode = reservDownload.ReservationCode,
                    LastUpdateDate = reservHistory.LastUpdateDate.ToZonedDateTimeLoggedUser(_applicationUser),
                    PartnerReservationNumber = reservDownload.PartnerReservationNumber,
                    Message = message
                });
            }


            return new ThexListDto<ReservationDownloadResultDto>()
            {
                HasNext = baseQueryPagination.HasNextPage,
                Items = resultDtoList.OrderByDescending(rd => rd.LastUpdateDate).ToList(),
                TotalItems = baseQuery.Count()
            };
        }

        public async Task<ThexListDto<ReservationDownloadHistoryDto>> GetAllHistoryByReservationDownloadIdAsync(string reservDownloadId)
        {
            var reservDownload = await FirstOrDefaultAsync(rd => rd.Id.Equals(reservDownloadId));

            if (reservDownload == null) return new ThexListDto<ReservationDownloadHistoryDto>();

            var resultDtoList = new List<ReservationDownloadHistoryDto>();
            foreach (var reservDownHistory in reservDownload.ReservationDownloadHistoryList)
            {
                var status = reservDownHistory.ReservationDownloadSituationList.All(rds => rds.Errors == 0);

                var message = string.Empty;
                if (status)
                    message = _localizationManager.GetString(AppConsts.LocalizationSourceName, "SuccessToMigratedReservation");
                else
                    message = _localizationManager.GetString(AppConsts.LocalizationSourceName, "ErrorToMigratedReservation");

                var reservationDownloadDetailsDto = new ReservationDownloadDetailsDto()
                {
                    BusinessSourceName = reservDownload.BusinessSourceName,
                    BusinessSourceId = reservDownload.BusinessSourceId,
                    PartnerReservationNumber = reservDownload.PartnerReservationNumber,
                    ReservationCode = reservDownload.ReservationCode,
                    Id = reservDownHistory.Id,
                    Response = reservDownHistory.Response,
                    PartnerName = reservDownload.PartnerName,
                    PartnerId = reservDownload.PartnerId,
                    SituationList = reservDownHistory.ReservationDownloadSituationList.Where(rds => rds.StatusCode != (int)ReservationDownloadSituationEnum.ProcessedReservation)
                        .Select(rds => new ReservationDownloadSituationDto
                        {
                            FirstAttemptDate = rds.FirstOperationDate.ToZonedDateTimeLoggedUser(_applicationUser),
                            LastAttemptDate = rds.LastOperationDate.ToZonedDateTimeLoggedUser(_applicationUser),
                            Status = rds.Status,
                            StatusCode = rds.StatusCode,
                            Id = rds.Id,
                            ResolutionDate = rds.ResolutionDate.HasValue ? rds.ResolutionDate.Value.ToZonedDateTimeLoggedUser(_applicationUser) : (DateTime?)null,
                            Key = rds.Key,
                            ErrorsNumber = rds.Errors > 0 ? rds.Errors : (int?)null,
                            Message = _localizationManager.GetString(AppConsts.LocalizationSourceName, $"{rds.StatusCode}{(rds.Status ? "Success" : "Error")}")
                        }).OrderBy(rds => rds.StatusCode).ToList()
                };

                resultDtoList.Add(new ReservationDownloadHistoryDto()
                {
                    Id = reservDownHistory.Id,
                    OperationName = _localizationManager.GetString(AppConsts.LocalizationSourceName, ((SituationTypeHigsEnum)reservDownHistory.OperationType).ToString()),
                    OperationType = reservDownHistory.OperationType,
                    Status = status,
                    LastUpdateDate = reservDownHistory.LastUpdateDate.ToZonedDateTimeLoggedUser(_applicationUser),
                    Message = message,
                    ReservationDownloadDetailsDto = reservationDownloadDetailsDto
                });

                if (reservDownHistory.ReservationDownloadSituationList.Any(rds => rds.Status && rds.StatusCode == (int)ReservationDownloadSituationEnum.ProcessedReservation)
                            && !resultDtoList.Any(rdh => rdh.Status))
                {
                    var reservationDownloadSuccessDetailsDto = new ReservationDownloadDetailsDto()
                    {
                        BusinessSourceName = reservDownload.BusinessSourceName,
                        BusinessSourceId = reservDownload.BusinessSourceId,
                        PartnerReservationNumber = reservDownload.PartnerReservationNumber,
                        ReservationCode = reservDownload.ReservationCode,
                        Id = reservDownHistory.Id,
                        Response = reservDownHistory.Response,
                        PartnerName = reservDownload.PartnerName,
                        PartnerId = reservDownload.PartnerId,
                        SituationList = reservDownHistory.ReservationDownloadSituationList.Where(rds => rds.StatusCode != (int)ReservationDownloadSituationEnum.ProcessedReservation)
                        .Select(rds => new ReservationDownloadSituationDto
                        {
                            FirstAttemptDate = rds.ResolutionDate.Value.ToZonedDateTimeLoggedUser(_applicationUser),
                            LastAttemptDate = rds.ResolutionDate.Value.ToZonedDateTimeLoggedUser(_applicationUser),
                            Status = true,
                            StatusCode = rds.StatusCode,
                            Id = rds.Id,
                            ResolutionDate = rds.ResolutionDate.Value.ToZonedDateTimeLoggedUser(_applicationUser),
                            Key = rds.Key,
                            Message = _localizationManager.GetString(AppConsts.LocalizationSourceName, $"{rds.StatusCode}Success")
                        }).OrderBy(rds => rds.StatusCode).ToList()
                    };

                    resultDtoList.Add(new ReservationDownloadHistoryDto()
                    {
                        Id = reservDownHistory.Id,
                        OperationName = _localizationManager.GetString(AppConsts.LocalizationSourceName, ((SituationTypeHigsEnum)reservDownHistory.OperationType).ToString()),
                        OperationType = reservDownHistory.OperationType,
                        Status = true,
                        LastUpdateDate = reservDownHistory.LastUpdateDate.AddSeconds(1).ToZonedDateTimeLoggedUser(_applicationUser),
                        Message = _localizationManager.GetString(AppConsts.LocalizationSourceName, "SuccessToMigratedReservation"),
                        ReservationDownloadDetailsDto = reservationDownloadSuccessDetailsDto
                    });
                }
            }

            return new ThexListDto<ReservationDownloadHistoryDto>()
            {
                HasNext = false,
                Items = resultDtoList.OrderByDescending(rd => rd.LastUpdateDate).ToList()
            };
        }

        #region Filters
        private List<ReservationDownload> FilterByDate(ReservationDownloadSearchDto request, List<ReservationDownload> basequery)
        {
            if (request.Date.HasValue)
                basequery = basequery.Where(rd => rd.ReservationDownloadHistoryList.LastOrDefault().LastUpdateDate.ToZonedDateTimeLoggedUser(_applicationUser) >= request.Date.Value.Date && rd.ReservationDownloadHistoryList.LastOrDefault().LastUpdateDate.ToZonedDateTimeLoggedUser(_applicationUser) <= request.Date.Value.Date.AddHours(23).AddMinutes(59).AddSeconds(59)).ToList();

            return basequery;
        }

        private List<ReservationDownload> FilterByPartnerId(ReservationDownloadSearchDto request, List<ReservationDownload> basequery)
        {
            if (!string.IsNullOrWhiteSpace(request.PartnerId))
                basequery = basequery.Where(rd => !string.IsNullOrWhiteSpace(rd.PartnerId) && rd.PartnerId.Equals(request.PartnerId)).ToList();

            return basequery;
        }

        private List<ReservationDownload> FilterByBusinessSourceId(ReservationDownloadSearchDto request, List<ReservationDownload> basequery)
        {
            if (!string.IsNullOrWhiteSpace(request.BusinessSourceId))
                basequery = basequery.Where(rd => !string.IsNullOrWhiteSpace(rd.BusinessSourceId) && rd.BusinessSourceId.Equals(request.BusinessSourceId)).ToList();

            return basequery;
        }

        private List<ReservationDownload> FilterByReservationNumber(ReservationDownloadSearchDto request, List<ReservationDownload> basequery)
        {
            if (!string.IsNullOrWhiteSpace(request.ReservationNumber))
                basequery = basequery.Where(rd => (!string.IsNullOrWhiteSpace(rd.ReservationCode) && rd.ReservationCode.Contains(request.ReservationNumber)) || rd.PartnerReservationNumber.Contains(request.ReservationNumber)).ToList();

            return basequery;
        }
        #endregion
    }
}
