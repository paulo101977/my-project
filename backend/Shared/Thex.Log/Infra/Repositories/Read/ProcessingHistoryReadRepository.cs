﻿using System.Threading.Tasks;
using Thex.Log.Infra.Entities;
using Thex.Log.Infra.Interfaces.Read;
using Tnf.MongoDb.Provider;
using Tnf.MongoDb.Repositories;

namespace Thex.Log.Infra.Repositories.Read
{
    public class ProcessingHistoryReadRepository : MongoDbRepository<ProcessingHistory, string>, IProcessingHistoryReadRepository
    {
        public ProcessingHistoryReadRepository(IMongoDbProvider provider)
            : base(provider)
        {
        }

        public async Task<ProcessingHistory> GetByProcessingTypeAndPropertyIdAsync(int processingType, int propertyId)
            => await FirstOrDefaultAsync(ph => ph.PropertyId == propertyId && ph.ProcessingType == processingType);
    }
}
