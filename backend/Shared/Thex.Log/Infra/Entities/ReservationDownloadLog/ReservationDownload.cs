﻿using System.Collections.Generic;
using Tnf.MongoDb.Entities;

namespace Thex.Log.Infra.Entities.ReservationDownloadLog
{
    public partial class ReservationDownload : IMongoDbEntity<string>
    {
        public string Id { get; set; }
        public int PropertyId { get; set; }
        public string BusinessSourceId { get; internal set; }
        public string BusinessSourceName { get; internal set; }
        public string PartnerReservationNumber { get; internal set; }
        public string ExternalReservationNumber { get; internal set; }
        public string ReservationCode { get; internal set; }
        public string PartnerId { get; internal set; }
        public string PartnerName { get; internal set; }

        public ICollection<ReservationDownloadHistory> ReservationDownloadHistoryList { get; set; }

        public ReservationDownload()
        {
            this.ReservationDownloadHistoryList = new HashSet<ReservationDownloadHistory>();
        }
    }
}
