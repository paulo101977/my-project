﻿using System;
using Thex.Kernel;
using Tnf.MongoDb.Entities;

namespace Thex.Log.Infra.Entities.ReservationDownloadLog
{
    public partial class ReservationDownloadSituation : IMongoDbEntity<string>
    {
        public string Id { get; set; }
        public bool Status { get; internal set; }
        public int StatusCode { get; internal set; }
        public DateTime FirstOperationDate { get; internal set; }
        public DateTime LastOperationDate { get; internal set; }
        public DateTime? ResolutionDate { get; internal set; }
        public int Errors { get; internal set; }
        public string Key { get; internal set; }
    }
}
