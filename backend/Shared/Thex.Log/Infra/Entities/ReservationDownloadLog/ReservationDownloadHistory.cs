﻿using System;
using System.Collections.Generic;
using System.Text;
using Thex.Kernel;
using Tnf.MongoDb.Entities;

namespace Thex.Log.Infra.Entities.ReservationDownloadLog
{
    public partial class ReservationDownloadHistory : IMongoDbEntity<string>
    {
        public string Id { get; set; }
        public int OperationType { get; internal set; }
        public DateTime LastUpdateDate { get; internal set; }
        public string Response { get; internal set; }
        public string ResponseHash { get; internal set; }

        public ICollection<ReservationDownloadSituation> ReservationDownloadSituationList { get; internal set; }

        public ReservationDownloadHistory()
        {
            this.ReservationDownloadSituationList = new HashSet<ReservationDownloadSituation>();
        }
    }
}
