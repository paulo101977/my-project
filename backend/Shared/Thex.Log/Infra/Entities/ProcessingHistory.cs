﻿using System;
using System.Collections.Generic;
using System.Text;
using Tnf.MongoDb.Entities;

namespace Thex.Log.Infra.Entities
{
    public partial class ProcessingHistory : IMongoDbEntity<string>
    {
        public string Id { get; set; }
        public int PropertyId { get; set; }
        public int ProcessingType { get; set; }
        public DateTime ProcessingDate { get; set; }
    }
}
