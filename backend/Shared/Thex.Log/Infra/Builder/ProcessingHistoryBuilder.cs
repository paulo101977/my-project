﻿using System;
using System.Collections.Generic;
using System.Text;
using Tnf.Builder;
using Tnf.Notifications;

namespace Thex.Log.Infra.Entities
{
    public partial class ProcessingHistory
    {
        public class Builder : Builder<ProcessingHistory>
        {
            public Builder(INotificationHandler notificationHandler)
                : base(notificationHandler)
            {
            }

            public Builder(INotificationHandler notificationHandler, ProcessingHistory instance)
                : base(notificationHandler, instance)
            {
            }

            public virtual Builder WithId(string id)
            {
                Instance.Id = id;
                return this;
            }

            public virtual Builder WithPropertyId(int propertyId)
            {
                Instance.PropertyId = propertyId;
                return this;
            }

            public virtual Builder WithProcessingType(int processingType)
            {
                Instance.ProcessingType = processingType;
                return this;
            }

            public virtual Builder WithProcessingDate(DateTime processingDate)
            {
                Instance.ProcessingDate = processingDate;
                return this;
            }
        }
    }
}
