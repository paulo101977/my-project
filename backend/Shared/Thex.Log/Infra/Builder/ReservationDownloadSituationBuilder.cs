﻿using System;
using Tnf.Builder;
using Tnf.Notifications;

namespace Thex.Log.Infra.Entities.ReservationDownloadLog
{
    public partial class ReservationDownloadSituation
    {
        public class Builder : Builder<ReservationDownloadSituation>
        {
            public Builder(INotificationHandler notificationHandler)
                : base(notificationHandler)
            {
            }

            public Builder(INotificationHandler notificationHandler, ReservationDownloadSituation instance)
                : base(notificationHandler, instance)
            {
            }

            public virtual Builder WithId(string id)
            {
                Instance.Id = id;
                return this;
            }

            public virtual Builder WithStatus(bool status)
            {
                Instance.Status = status;
                return this;
            }

            public virtual Builder WithStatusCode(int statusCode)
            {
                Instance.StatusCode = statusCode;
                return this;
            }

            public virtual Builder WithFirstOperationDate(DateTime firstOperationDate)
            {
                Instance.FirstOperationDate = firstOperationDate;
                return this;
            }

            public virtual Builder WithLastOperationDate(DateTime lastOperationDate)
            {
                Instance.LastOperationDate = lastOperationDate;
                return this;
            }

            public virtual Builder WithResolutionDate(DateTime? resolutionDate)
            {
                Instance.ResolutionDate = resolutionDate;
                return this;
            }

            public virtual Builder WithError(int errors)
            {
                Instance.Errors = errors;
                return this;
            }

            public virtual Builder WithKey(string key)
            {
                Instance.Key = key;
                return this;
            }
        }
    }
}
