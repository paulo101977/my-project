﻿using System;
using System.Collections.Generic;
using Tnf.Builder;
using Tnf.Notifications;

namespace Thex.Log.Infra.Entities.ReservationDownloadLog
{
    public partial class ReservationDownload
    {
        public class Builder : Builder<ReservationDownload>
        {
            public Builder(INotificationHandler notificationHandler)
                : base(notificationHandler)
            {
            }

            public Builder(INotificationHandler notificationHandler, ReservationDownload instance)
                : base(notificationHandler, instance)
            {
            }

            public virtual Builder WithId(string id)
            {
                Instance.Id = id;
                return this;
            }

            public virtual Builder WithPropertyId(int propertyId)
            {
                Instance.PropertyId = propertyId;
                return this;
            }

            public virtual Builder WithBusinessSourceId(string businessSourceId)
            {
                Instance.BusinessSourceId = businessSourceId;
                return this;
            }

            public virtual Builder WithBusinessSourceName(string businessSourceName)
            {
                Instance.BusinessSourceName = businessSourceName;
                return this;
            }

            public virtual Builder WithExternalReservationNumber(string externalReservationNumber)
            {
                Instance.ExternalReservationNumber = externalReservationNumber;
                return this;
            }

            public virtual Builder WithPartnerReservationNumber(string partnerReservationNumber)
            {
                Instance.PartnerReservationNumber = partnerReservationNumber;
                return this;
            }

            public virtual Builder WithReservationCode(string reservationCode)
            {
                Instance.ReservationCode = reservationCode;
                return this;
            }

            public virtual Builder WithPartnerId(string partnerId)
            {
                Instance.PartnerId = partnerId;
                return this;
            }

            public virtual Builder WithPartnerName(string partnerName)
            {
                Instance.PartnerName = partnerName;
                return this;
            }

            public virtual Builder WithReservationDownloadHistoryList(ICollection<ReservationDownloadHistory> history)
            {
                Instance.ReservationDownloadHistoryList = history;
                return this;
            }
        }
    }
}
