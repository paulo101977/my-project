﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using Tnf.Builder;
using Tnf.Notifications;

namespace Thex.Log.Infra.Entities.ReservationDownloadLog
{
    public partial class ReservationDownloadHistory
    {
        public class Builder : Builder<ReservationDownloadHistory>
        {
            public Builder(INotificationHandler notificationHandler)
                : base(notificationHandler)
            {
            }

            public Builder(INotificationHandler notificationHandler, ReservationDownloadHistory instance)
                : base(notificationHandler, instance)
            {
            }

            public virtual Builder WithId(string id)
            {
                Instance.Id = id;
                return this;
            }

            public virtual Builder WithOperationType(int operationType)
            {
                Instance.OperationType = operationType;
                return this;
            }

            public virtual Builder WithLastUpdate(DateTime lastUpdate)
            {
                Instance.LastUpdateDate = lastUpdate;
                return this;
            }

            public virtual Builder WithReservationDownloadSituationList(ICollection<ReservationDownloadSituation> situationList)
            {
                Instance.ReservationDownloadSituationList = situationList;
                return this;
            }

            public virtual Builder WithResponse(string response)
            {
                Instance.Response = response;
                Instance.ResponseHash = Convert.ToBase64String(SHA384.Create().ComputeHash(Encoding.UTF8.GetBytes(response)));
                return this;
            }
        }
    }
}
