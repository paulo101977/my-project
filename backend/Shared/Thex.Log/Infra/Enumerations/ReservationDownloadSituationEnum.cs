﻿namespace Thex.Log.Infra.Enumerations
{
    public enum ReservationDownloadSituationEnum
    {
        ProcessedReservation = 100,
        ReservationItem = 101,
        Channel = 102,
        RatePlan = 103,
        RoomType = 104,
        Currency = 105,
        PropertyGuestType = 106,
        RoomLayout = 107
    }
}
