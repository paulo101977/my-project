﻿using System.Threading.Tasks;
using Thex.Log.Infra.Entities;
using Tnf.MongoDb.Repositories.Interfaces;

namespace Thex.Log.Infra.Interfaces.Read
{
    public interface IProcessingHistoryReadRepository : IMongoDbRepository<ProcessingHistory, string>
    {
        Task<ProcessingHistory> GetByProcessingTypeAndPropertyIdAsync(int processingType, int propertyId);
    }
}
