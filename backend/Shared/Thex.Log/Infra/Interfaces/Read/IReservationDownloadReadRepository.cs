﻿using System.Threading.Tasks;
using Thex.Log.Dto;
using Thex.Log.Dto.ReservationDownload;
using Thex.Log.Infra.Entities.ReservationDownloadLog;
using Tnf.MongoDb.Repositories.Interfaces;

namespace Thex.Log.Infra.Interfaces.Read
{
    public interface IReservationDownloadReadRepository : IMongoDbAsyncRepository<ReservationDownload, string>
    {
        Task<string> GetByParametersAsync(string externalReservationNumber, int propertyId);
        Task<ThexListDto<ReservationDownloadResultDto>> GetAllByFiltersAsync(ReservationDownloadSearchDto request, int propertyId);
        Task<ThexListDto<ReservationDownloadHistoryDto>> GetAllHistoryByReservationDownloadIdAsync(string reservDownloadId);
    }
}
