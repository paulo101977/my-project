﻿using System;
using System.Collections.Generic;
using System.Text;
using Thex.Log.Infra.Entities.ReservationDownloadLog;
using Tnf.MongoDb.Repositories.Interfaces;

namespace Thex.Log.Infra.Interfaces
{
    public interface IReservationDownloadRepository : IMongoDbRepository<ReservationDownload, string>
    {
    }
}
