﻿using Microsoft.Extensions.DependencyInjection;
using Thex.Log.Application;
using Thex.Log.Application.Adapter.ProcessingHistoryLog;
using Thex.Log.Application.Adapter.ReservationDownloadLog;
using Thex.Log.Application.Interfaces;
using Thex.Log.Application.Services;
using Thex.Log.Infra.Interfaces;
using Thex.Log.Infra.Interfaces.Read;
using Thex.Log.Infra.Repositories;
using Thex.Log.Infra.Repositories.Read;

namespace Thex.Log
{
    public static class ServiceCollections
    {
        public static IServiceCollection AddLogApplicationServiceDependency(this IServiceCollection services, string mongoConnectionString, string mongoDatabaseName)
        {
            services.AddTnfMongoDb(builder => builder
             .WithConnectionString(mongoConnectionString)
             .WithDatabaseName(mongoDatabaseName));

            #region Application
            services.AddScoped<IReservationDownloadAdapter, ReservationDownloadAdapter>();
            services.AddScoped<IProcessingHistoryAdapter, ProcessingHistoryAdapter>();

            services.AddScoped<IReservationDownloadLogAppService, ReservationDownloadLogAppService>();
            services.AddScoped<IProcessingHistoryLogAppService, ProcessingHistoryLogAppService>();
            #endregion

            #region Repositories
            services.AddScoped<IReservationDownloadReadRepository, ReservationDownloadReadRepository>();
            services.AddScoped<IProcessingHistoryReadRepository, ProcessingHistoryReadRepository>();

            services.AddScoped<IReservationDownloadRepository, ReservationDownloadRepository>();
            services.AddScoped<IProcessingHistoryRepository, ProcessingHistoryRepository>();
            #endregion

            return services;
        }
    }
}
