﻿using System;
using System.Collections.Generic;
using System.Linq;
using Thex.Common.Dto.Observable;
using Thex.Common.Enumerations;
using Thex.Common.Interfaces.Observables;
using Thex.Inventory.Domain.Entities;
using Thex.Inventory.Domain.Repositories.Interfaces;
using Thex.Kernel;

namespace Thex.Inventory.Domain.Services
{
    public class RoomTypeInventoryDomainService : ICreateRoomTypeObservable, ICreateRoomObservable, 
        IRoomChangeRoomTypeObservable, IDeleteRoomObservable, IExecutePropertyAuditProcessObservable,
        ICreateRoomBlockingObservable, IDeleteRoomBlockingObservable, IReservationItemChangeStatusObservable,
        IReservationItemChangeRoomTypeOrPeriodObservable
    {
        private IRoomTypeInventoryRepository _roomTypeInventoryRepository { get; set; }
        private IApplicationUser _applicationUser { get; set; }
        private IPropertyParameterRepository _propertyParameterRepository { get; set; }

        public RoomTypeInventoryDomainService(
            IRoomTypeInventoryRepository roomTypeInventoryRepository,
            IApplicationUser applicationUser,
            IPropertyParameterRepository propertyParameterRepository
            )
        {
            _roomTypeInventoryRepository = roomTypeInventoryRepository;
            _applicationUser = applicationUser;
            _propertyParameterRepository = propertyParameterRepository;
        }

        public void ExecuteActionAfterCreateRoomType(CreateRoomTypeObservableDto dto)
        {
            AddInitialPeriod(dto.PropertyId, dto.RoomTypeId, dto.InitialDate);
        }

        private void AddInitialPeriod(int propertyId, int roomTypeId, DateTime initialDate)
        {
            var roomTypeInventoryList = new List<RoomTypeInventory>();

            var date = initialDate;

            for (int i = 0; i < 1000; i++)
            {
                var roomTypeInventory = new RoomTypeInventory
                {
                    Id = Guid.NewGuid(),
                    PropertyId = propertyId,
                    RoomTypeId = roomTypeId,
                    Total = 0,
                    Balance = 0,
                    BlockedQuantity = 0,
                    Date = date.Date
                };

                roomTypeInventory.SetTenant(_applicationUser);

                roomTypeInventoryList.Add(roomTypeInventory);
                date = date.AddDays(1);
            }

            _roomTypeInventoryRepository.CreateList(roomTypeInventoryList);
        }

        public void ExecuteActionAfterCreateRoom(CreateRoomObservableDto dto)
        {
            var roomTypeInventoryList = _roomTypeInventoryRepository.GetAllByRoomTypeIdAndDate(dto.RoomTypeId, dto.InitialDate).ToList();

            foreach (var roomTypeInventory in roomTypeInventoryList)
            {
                roomTypeInventory.Balance++;
                roomTypeInventory.Total++;
            }

            _roomTypeInventoryRepository.UpdateList(roomTypeInventoryList);
        }

        public void ExecuteActionAfterRoomChangeRoomType(RoomChangeRoomTypeObservableDto dto)
        {
            var roomTypeInventoryToDecrementList = _roomTypeInventoryRepository.GetAllByRoomTypeIdAndDate(dto.RoomTypeIdOld, dto.InitialDate).ToList();
            foreach (var roomTypeInventory in roomTypeInventoryToDecrementList)
            {
                roomTypeInventory.Balance--;
                roomTypeInventory.Total--;
            }
             _roomTypeInventoryRepository.UpdateList(roomTypeInventoryToDecrementList);

            var roomTypeInventoryToIncrementList = _roomTypeInventoryRepository.GetAllByRoomTypeIdAndDate(dto.RoomTypeIdNew, dto.InitialDate).ToList();
            foreach (var roomTypeInventory in roomTypeInventoryToIncrementList)
            {
                roomTypeInventory.Balance++;
                roomTypeInventory.Total++;
            }
            _roomTypeInventoryRepository.UpdateList(roomTypeInventoryToIncrementList);
        }

        public void ExecuteActionAfterDeleteRoom(DeleteRoomObservableDto dto)
        {
            var roomTypeInventoryList = _roomTypeInventoryRepository.GetAllByRoomTypeIdAndDate(dto.RoomTypeId, dto.InitialDate).ToList();

            foreach (var roomTypeInventory in roomTypeInventoryList)
            {
                roomTypeInventory.Balance--;
                roomTypeInventory.Total--;
            }

            _roomTypeInventoryRepository.UpdateList(roomTypeInventoryList);
        }

        public void ExecuteActionAfterExecutePropertyAuditProcess(ExecutePropertyAuditProcessObservableDto dto)
        {
            var roomTypeInventoryList = new List<RoomTypeInventory>();

            foreach (var roomTypeId in dto.RoomTypeIdList)
            {
                var lastRoomTypeInventory = _roomTypeInventoryRepository.GetLastByRoomTypeId(roomTypeId);

                if (lastRoomTypeInventory != null)
                {
                    var roomTypeInventory = new RoomTypeInventory
                    {
                        Id = Guid.NewGuid(),
                        PropertyId = lastRoomTypeInventory.PropertyId,
                        RoomTypeId = lastRoomTypeInventory.RoomTypeId,
                        Total = lastRoomTypeInventory.Total,
                        Balance = lastRoomTypeInventory.Balance,
                        BlockedQuantity = 0,
                        Date = lastRoomTypeInventory.Date.AddDays(1)
                    };

                    roomTypeInventory.SetTenant(_applicationUser);

                    roomTypeInventoryList.Add(roomTypeInventory);
                }  
            }

            _roomTypeInventoryRepository.CreateList(roomTypeInventoryList);
        }

        public void ExecuteActionAfterCreateRoomBlocking(CreateRoomBlockingObservableDto dto)
        {
            var roomTypeIdList = dto.RoomTypeIdWithRoomQuantityDictionary.Keys;
            var roomTypeInventoryList = _roomTypeInventoryRepository.GetAllByRoomTypeIdListAndPeriod(roomTypeIdList, dto.InitialDate, dto.EndDate).ToList();

            foreach (var roomTypeInventory in roomTypeInventoryList)
            {
                var roomQuantity = dto.RoomTypeIdWithRoomQuantityDictionary[roomTypeInventory.RoomTypeId];
                roomTypeInventory.Balance -= roomQuantity;
                roomTypeInventory.BlockedQuantity += roomQuantity;
            }

            _roomTypeInventoryRepository.UpdateList(roomTypeInventoryList);
        }

        public void ExecuteActionAfterDeleteRoomBlocking(DeleteRoomBlockingObservableDto dto)
        {
            var systemDate = _propertyParameterRepository.GetSystemDate(int.Parse(_applicationUser.PropertyId));

            if (dto.InitialDate.Date < systemDate.Value.Date)
                dto.InitialDate = systemDate.Value;

            var roomTypeIdList = dto.RoomTypeIdWithRoomQuantityDictionary.Keys;
            var roomTypeInventoryList = _roomTypeInventoryRepository.GetAllByRoomTypeIdListAndPeriod(roomTypeIdList, dto.InitialDate, dto.EndDate).ToList();

            foreach (var roomTypeInventory in roomTypeInventoryList)
            {
                var roomQuantity = dto.RoomTypeIdWithRoomQuantityDictionary[roomTypeInventory.RoomTypeId];
                roomTypeInventory.Balance += roomQuantity;
                roomTypeInventory.BlockedQuantity -= roomQuantity;
            }

            _roomTypeInventoryRepository.UpdateList(roomTypeInventoryList);
        }

        public void ExecuteActionAfterChangeReservationItemStatus(ReservationItemChangeStatusObservableDto dto)
        {
            var systemDate = _propertyParameterRepository.GetSystemDate(int.Parse(_applicationUser.PropertyId));

            if (dto.InitialDate.Date < systemDate.Value.Date)
                dto.InitialDate = systemDate.Value;

            var endDate = GetEstimatedDepartureOrCheckoutDate(dto.EndDate);
            var roomTypeInventoryList = _roomTypeInventoryRepository.GetAllByRoomTypeIdAndPeriod(dto.RoomTypeId, dto.InitialDate, endDate).ToList();

            foreach (var roomTypeInventory in roomTypeInventoryList)
            {
                if (dto.ReservationItemStatusIdNew == ReservationStatus.ToConfirm
                    || dto.ReservationItemStatusIdNew == ReservationStatus.Confirmed)
                {
                    roomTypeInventory.Balance--;
                }
                else if (dto.ReservationItemStatusIdNew == ReservationStatus.Canceled
                    || dto.ReservationItemStatusIdNew == ReservationStatus.NoShow)
                {
                    roomTypeInventory.Balance++;
                }
                else if (dto.ReservationItemStatusIdOld.HasValue 
                    && dto.ReservationItemStatusIdOld.Value == ReservationStatus.NoShow
                    && dto.ReservationItemStatusIdNew == ReservationStatus.Checkin)
                {
                    roomTypeInventory.Balance--;
                }  
                else if (dto.ReservationItemStatusIdOld.HasValue 
                    && dto.ReservationItemStatusIdOld.Value == ReservationStatus.Checkin
                     && dto.ReservationItemStatusIdNew == ReservationStatus.Checkout)
                {
                    roomTypeInventory.Balance++;
                }
                else if (dto.ReservationItemStatusIdOld.HasValue 
                    &&
                    (dto.ReservationItemStatusIdOld.Value == ReservationStatus.ToConfirm
                    || dto.ReservationItemStatusIdOld.Value == ReservationStatus.Confirmed
                    || dto.ReservationItemStatusIdOld.Value == ReservationStatus.Checkin
                    )
                     && dto.ReservationItemStatusIdNew == ReservationStatus.Pending)
                {
                    roomTypeInventory.Balance++;
                }
            }

            _roomTypeInventoryRepository.UpdateList(roomTypeInventoryList);
        }

        public void ExecuteActionAfterReservationItemChangeRoomTypeOrPeriod(ReservationItemChangeRoomTypeOrPeriodObservableDto dto)
        {
            var systemDate = _propertyParameterRepository.GetSystemDate(int.Parse(_applicationUser.PropertyId));

            if (dto.OldInitialDate.Date < systemDate.Value.Date)
                dto.OldInitialDate = systemDate.Value;

            if (dto.NewInitialDate.HasValue && dto.NewInitialDate.Value.Date < systemDate.Value.Date)
                dto.NewInitialDate = systemDate.Value;

            var oldEstimatedDeparture = GetEstimatedDepartureOrCheckoutDate(dto.OldEndDate);

            var roomTypeInventoryToIncrementList = _roomTypeInventoryRepository.GetAllByRoomTypeIdAndPeriod(dto.OldRoomTypeId, dto.OldInitialDate, oldEstimatedDeparture).ToList();
            foreach (var roomTypeInventory in roomTypeInventoryToIncrementList)
                roomTypeInventory.Balance++;
            _roomTypeInventoryRepository.UpdateList(roomTypeInventoryToIncrementList);

            var roomTypeInventoryToDecrementList = new List<RoomTypeInventory>();
            if (dto.NewInitialDate == null || dto.NewEndDate == null)
            {
                roomTypeInventoryToDecrementList = _roomTypeInventoryRepository.GetAllByRoomTypeIdAndPeriod(dto.NewRoomTypeId, dto.OldInitialDate, oldEstimatedDeparture).ToList();
                foreach (var roomTypeInventory in roomTypeInventoryToDecrementList)
                    roomTypeInventory.Balance--;
            }
            else
            {
                roomTypeInventoryToDecrementList = _roomTypeInventoryRepository.GetAllByRoomTypeIdAndPeriod(dto.NewRoomTypeId, dto.NewInitialDate.Value, GetEstimatedDepartureOrCheckoutDate(dto.NewEndDate.Value)).ToList();
                foreach (var roomTypeInventory in roomTypeInventoryToDecrementList)
                    roomTypeInventory.Balance--;
            }
            _roomTypeInventoryRepository.UpdateList(roomTypeInventoryToDecrementList);
        }

        private DateTime GetEstimatedDepartureOrCheckoutDate(DateTime oldEndDate)
        {
            return oldEndDate.AddDays(-1);
        }
    }
}
