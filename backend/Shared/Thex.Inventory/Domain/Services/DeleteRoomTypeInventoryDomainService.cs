﻿using Thex.Inventory.Domain.Repositories.Interfaces;
using Thex.Kernel;

namespace Thex.Inventory.Domain.Services
{
    public class DeleteRoomTypeInventoryDomainService : IDeleteRoomTypeInventoryRepository
    {
        private IRoomTypeInventoryRepository _roomTypeInventoryRepository { get; set; }
        private IApplicationUser _applicationUser { get; set; }
        private IPropertyParameterRepository _propertyParameterRepository { get; set; }

        public DeleteRoomTypeInventoryDomainService(IRoomTypeInventoryRepository roomTypeInventoryRepository)
        {
            _roomTypeInventoryRepository = roomTypeInventoryRepository;
        }


        public void Delete(int roomTypeId)
        => _roomTypeInventoryRepository.Delete(roomTypeId);

    }
}
