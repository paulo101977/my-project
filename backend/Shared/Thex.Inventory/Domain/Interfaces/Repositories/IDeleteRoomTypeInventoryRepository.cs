﻿using System;
using System.Collections.Generic;
using Thex.Inventory.Domain.Entities;

namespace Thex.Inventory.Domain.Repositories.Interfaces
{
    public interface IDeleteRoomTypeInventoryRepository
    {
        void Delete(int id);       
        
    }
}
