﻿using System;

namespace Thex.Inventory.Domain.Repositories.Interfaces
{
    public interface IPropertyParameterRepository
    {
        DateTime? GetSystemDate(int propertyId);
    }
}
