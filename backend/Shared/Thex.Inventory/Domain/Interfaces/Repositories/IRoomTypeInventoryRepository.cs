﻿using System;
using System.Collections.Generic;
using Thex.Inventory.Domain.Entities;

namespace Thex.Inventory.Domain.Repositories.Interfaces
{
    public interface IRoomTypeInventoryRepository
    {
        void CreateList(IList<RoomTypeInventory> roomTypeInventoryList);
        void UpdateList(IList<RoomTypeInventory> roomTypeInventoryList);
        IEnumerable<RoomTypeInventory> GetAllByRoomTypeIdAndDate(int roomTypeId, DateTime date);
        IEnumerable<RoomTypeInventory> GetAllByPropertyIdAndDate(int propertyId, DateTime date);
        IEnumerable<RoomTypeInventory> GetAllByRoomTypeIdAndPeriod(int roomTypeId, DateTime initialDate, DateTime endDate);
        IEnumerable<RoomTypeInventory> GetAllByRoomTypeIdListAndPeriod(IEnumerable<int> roomTypeIdList, DateTime initialDate, DateTime endDate);
        RoomTypeInventory GetLastByRoomTypeId(int roomTypeId);
        void Delete(int roomTypeId);
    }
}
