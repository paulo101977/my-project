﻿namespace Thex.Inventory.Domain.Entities
{
    //SÓ PARA TESTAR A TRANSAÇÃO ENTRE DIFERENTES CONTEXTOS
    public class RoomType : IEntityInt
    {
        public int Id { get; set; }
        public int PropertyId { get;  set; }
        public int Order { get;  set; }
        public string Name { get;  set; }
        public string Abbreviation { get;  set; }
        public int AdultCapacity { get;  set; }
        public int ChildCapacity { get;  set; }
        public int FreeChildQuantity1 { get;  set; }
        public int FreeChildQuantity2 { get;  set; }
        public int FreeChildQuantity3 { get;  set; }
        public bool IsActive { get;  set; }
        public decimal MaximumRate { get;  set; }
        public decimal MinimumRate { get;  set; }
        public string DistributionCode { get;  set; }
    }
}
