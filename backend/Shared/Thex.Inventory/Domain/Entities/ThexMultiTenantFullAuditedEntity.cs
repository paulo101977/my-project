﻿using System;
using Thex.Kernel;

namespace Thex.Inventory.Domain.Entities
{
    public class ThexMultiTenantFullAuditedEntity : ThexFullAuditedEntity
    {
        public Guid TenantId { get; set; }

        public void SetTenant(IApplicationUser applicationUser)
        {
            base.SetAudit(applicationUser);
            this.TenantId = applicationUser.TenantId;
        }
    }
}
