﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using Thex.Inventory.Domain.Entities;
using Thex.Inventory.Domain.Repositories.Interfaces;
using Thex.Inventory.Infra.Context;
using Tnf.EntityFrameworkCore;
using Tnf.EntityFrameworkCore.Repositories;

namespace Thex.Inventory.Infra.Repositories
{
    public class PropertyParameterRepository : EfCoreRepositoryBase<ThexInventoryContext, PropertyParameter>, IPropertyParameterRepository
    {
        public PropertyParameterRepository(IDbContextProvider<ThexInventoryContext> dbContextProvider)
            : base(dbContextProvider)
        {
        }

        public DateTime? GetSystemDate(int propertyId)
        {
            const int applicationParameterId = 10;

            var propertyParameter = GetByPropertyIdAndApplicationParameterId(propertyId, applicationParameterId);
            if (propertyParameter == null || (propertyParameter != null && propertyParameter.PropertyParameterValue == null))
            {
                return null;
            }
            else
            {
                try
                {
                    var value = propertyParameter.PropertyParameterValue.Split("/");
                    return new DateTime(Convert.ToInt32(value[2]), Convert.ToInt32(value[1]), Convert.ToInt32(value[0]));
                }
                catch
                {
                    var value = propertyParameter.PropertyParameterValue.Split("-");
                    return new DateTime(Convert.ToInt32(value[0]), Convert.ToInt32(value[1]), Convert.ToInt32(value[2]));
                }
            }
        }

        private PropertyParameter GetByPropertyIdAndApplicationParameterId(int propertyId, int applicationParameterId)
        {
            var result = Context.PropertyParameters
                .IgnoreQueryFilters()
                .Where(p => p.PropertyId == propertyId && p.ApplicationParameterId == applicationParameterId).FirstOrDefault();

            return result;
        }
    }
}
