﻿using EFCore.BulkExtensions;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using Thex.Common;
using Thex.Common.Enumerations;
using Thex.Inventory.Domain.Entities;
using Thex.Inventory.Domain.Repositories.Interfaces;
using Thex.Inventory.Infra.Context;
using Tnf.EntityFrameworkCore;
using Tnf.EntityFrameworkCore.Repositories;
using Tnf.Notifications;

namespace Thex.Inventory.Infra.Repositories
{
    public class RoomTypeInventoryRepository : EfCoreRepositoryBase<ThexInventoryContext, RoomTypeInventory>, IRoomTypeInventoryRepository
    {

        private readonly INotificationHandler Notification;





        public RoomTypeInventoryRepository(
            IDbContextProvider<ThexInventoryContext> dbContextProvider,
            INotificationHandler notificationHandler)
            : base(dbContextProvider)
        {
            Notification = notificationHandler;

        }

        public void CreateList(IList<RoomTypeInventory> roomTypeInventoryList)
        {
            if (roomTypeInventoryList.Any())
            {
                Context.BulkInsert(roomTypeInventoryList);
                Context.SaveChanges();
            }
        }

        public void UpdateList(IList<RoomTypeInventory> roomTypeInventoryList)
        {
            if (roomTypeInventoryList.Any())
            {
                Context.BulkUpdate(roomTypeInventoryList);
                Context.SaveChanges();
            }
        }

        public IEnumerable<RoomTypeInventory> GetAllByPropertyIdAndDate(int propertyId, DateTime date)
        {
            return Context.RoomTypeInventories.AsNoTracking().Where(r => r.PropertyId == propertyId
                && r.Date.Date == date.Date).ToList();
        }

        public IEnumerable<RoomTypeInventory> GetAllByRoomTypeIdAndDate(int roomTypeId, DateTime date)
        {
            return Context.RoomTypeInventories.AsNoTracking().Where(r => r.RoomTypeId == roomTypeId
                && r.Date.Date >= date.Date).ToList();
        }

        public IEnumerable<RoomTypeInventory> GetAllByRoomTypeIdAndPeriod(int roomTypeId, DateTime initialDate, DateTime endDate)
        {
            return Context.RoomTypeInventories.AsNoTracking().Where(r => r.RoomTypeId == roomTypeId
                && r.Date.Date >= initialDate.Date && r.Date.Date <= endDate.Date).ToList();
        }

        public IEnumerable<RoomTypeInventory> GetAllByRoomTypeIdListAndPeriod(IEnumerable<int> roomTypeIdList, DateTime initialDate, DateTime endDate)
        {
            return Context.RoomTypeInventories.AsNoTracking().Where(r => roomTypeIdList.Contains(r.RoomTypeId)
                && r.Date.Date >= initialDate.Date && r.Date.Date <= endDate.Date).ToList();
        }

        public RoomTypeInventory GetLastByRoomTypeId(int roomTypeId)
        {
            return Context.RoomTypeInventories.AsNoTracking().Where(r => r.RoomTypeId == roomTypeId)
                .OrderByDescending(r => r.Date).FirstOrDefault();
        }

        public void Delete(int id)
        {
            try
            { 
                var roomTypeInventory = Context
                    .RoomTypeInventories
                    .Where(x => x.RoomTypeId == id).ToList();

                Context.RemoveRange(roomTypeInventory);

                Context.SaveChanges();
            }
            catch
            {

                Notification.Raise(Notification.DefaultBuilder
                        .WithMessage(AppConsts.LocalizationSourceName, RoomTypeEnum.Error.RoomTypeErrorRemove)
                        .WithDetailedMessage(AppConsts.LocalizationSourceName, RoomTypeEnum.Error.RoomTypeErrorRemove)
                        .Build());
            }
        }
    }
}
