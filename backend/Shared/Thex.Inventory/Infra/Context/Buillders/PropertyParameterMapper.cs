﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Thex.Inventory.Domain.Entities;

namespace Thex.Inventory.Infra.Context.Builders
{
    public class PropertyParameterMapper : IEntityTypeConfiguration<PropertyParameter>
    {
        public void Configure(EntityTypeBuilder<PropertyParameter> builder)
        {

            builder.ToTable("PropertyParameter");

            builder.HasAnnotation("Relational:TableName", "PropertyParameter");

            builder.HasIndex(e => e.ApplicationParameterId)
                .HasName("x_PropertyParameter_ApplicationParameterId");

            builder.HasIndex(e => e.PropertyId)
                .HasName("x_PropertyParameter_PropertyId");

            builder.HasIndex(e => e.TenantId)
                .HasName("x_PropertyParameter_TenantId");

            builder.Property(e => e.Id)
                .HasAnnotation("Relational:ColumnName", "PropertyParameterId");

            builder.Property(e => e.ApplicationParameterId).HasAnnotation("Relational:ColumnName", "ApplicationParameterId");

            builder.Property(e => e.CreationTime)
                .HasColumnType("datetime")
                .HasDefaultValueSql("(getutcdate())")
                .HasAnnotation("Relational:ColumnName", "CreationTime");

            builder.Property(e => e.CreatorUserId).HasAnnotation("Relational:ColumnName", "CreatorUserId");

            builder.Property(e => e.DeleterUserId).HasAnnotation("Relational:ColumnName", "DeleterUserId");

            builder.Property(e => e.DeletionTime)
                .HasColumnType("datetime")
                .HasAnnotation("Relational:ColumnName", "DeletionTime");

            builder.Property(e => e.IsActive).HasAnnotation("Relational:ColumnName", "IsActive");

            builder.Property(e => e.IsDeleted).HasAnnotation("Relational:ColumnName", "IsDeleted");

            builder.Property(e => e.LastModificationTime)
                .HasColumnType("datetime")
                .HasDefaultValueSql("(getutcdate())")
                .HasAnnotation("Relational:ColumnName", "LastModificationTime");

            builder.Property(e => e.LastModifierUserId).HasAnnotation("Relational:ColumnName", "LastModifierUserId");

            builder.Property(e => e.PropertyId).HasAnnotation("Relational:ColumnName", "PropertyId");

            builder.Property(e => e.PropertyParameterMaxValue)
                .HasMaxLength(100)
                .IsUnicode(false)
                .HasAnnotation("Relational:ColumnName", "PropertyParameterMaxValue");

            builder.Property(e => e.PropertyParameterMinValue)
                .HasMaxLength(100)
                .IsUnicode(false)
                .HasAnnotation("Relational:ColumnName", "PropertyParameterMinValue");

            builder.Property(e => e.PropertyParameterPossibleValues)
                .IsUnicode(false)
                .HasAnnotation("Relational:ColumnName", "PropertyParameterPossibleValues");

            builder.Property(e => e.PropertyParameterValue)
                .IsUnicode(false)
                .HasAnnotation("Relational:ColumnName", "PropertyParameterValue");

            builder.Property(e => e.TenantId).HasAnnotation("Relational:ColumnName", "TenantId");
        }
    }
}
