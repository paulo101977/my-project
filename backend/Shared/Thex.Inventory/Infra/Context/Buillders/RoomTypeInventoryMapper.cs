﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Thex.Inventory.Domain.Entities;

namespace Thex.Inventory.Infra.Context.Builders
{
    public class RoomTypeInventoryMapper : IEntityTypeConfiguration<RoomTypeInventory>
    {
        public void Configure(EntityTypeBuilder<RoomTypeInventory> builder)
        {
            builder.ToTable("RatePlanCommission");

            builder.HasAnnotation("Relational:TableName", "RoomTypeInventory");

            //-----------------

            builder.HasIndex(e => e.PropertyId)
                .HasName("x_RoomTypeInventory_PropertyId");

            builder.HasIndex(e => e.RoomTypeId)
                .HasName("x_RoomTypeInventory_RoomTypeId");

            //------------------

            builder.Property(e => e.Id)
                .HasAnnotation("Relational:ColumnName", "RoomTypeInventoryId");

            builder.Property(e => e.PropertyId)
                .HasAnnotation("Relational:ColumnName", "PropertyId");

            builder.Property(e => e.RoomTypeId)
                .HasAnnotation("Relational:ColumnName", "RoomTypeId");

            builder.Property(e => e.Total)
                .HasAnnotation("Relational:ColumnName", "Total");

            builder.Property(e => e.Balance)
                .HasAnnotation("Relational:ColumnName", "Balance");

            builder.Property(e => e.BlockedQuantity)
                .HasAnnotation("Relational:ColumnName", "BlockedQuantity");

            builder.Property(e => e.Date)
                .HasColumnType("datetime")
                .HasAnnotation("Relational:ColumnName", "Date");
        }
    }
}
