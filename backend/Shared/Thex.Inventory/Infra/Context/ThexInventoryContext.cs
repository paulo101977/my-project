﻿using Microsoft.EntityFrameworkCore;
using Thex.Inventory.Infra.Context.Builders;
using Thex.Inventory.Domain.Entities;
using System;
using System.Linq;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Thex.GenericLog;
using Thex.Kernel;
using Thex.Common;
using Tnf.Runtime.Session;
using Tnf.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Thex.EntityFrameworkCore.Utils;

namespace Thex.Inventory.Infra.Context
{
    public class ThexInventoryContext : TnfDbContext
    {
        private readonly IApplicationUser _applicationUser;
        private IGenericLogHandler _genericLog;
        protected Guid _tenantId = Guid.Empty;
        protected Guid _userUid = Guid.Empty;
        private readonly IServiceProvider _serviceProvider;

        public ThexInventoryContext(
            DbContextOptions<ThexInventoryContext> options,
            IApplicationUser applicationUser,
            ITnfSession session,
            IGenericLogHandler genericLog,
            IServiceProvider serviceProvider)
            : base(options, session)
        {
            _applicationUser = applicationUser;
            _genericLog = genericLog;
            _tenantId = applicationUser.TenantId != Guid.Empty ? applicationUser.TenantId : Guid.Empty;
            _userUid = applicationUser.UserUid != Guid.Empty ? applicationUser.UserUid : Guid.Empty;
            _serviceProvider = serviceProvider;
        }

        public DbSet<RoomTypeInventory> RoomTypeInventories { get; set; }
        public DbSet<PropertyParameter> PropertyParameters { get; set; }

        public virtual void FillAuditFiedls()
        {
            // custom code...
            var utcNowAuditDate = DateTime.UtcNow.ToZonedDateTimeLoggedUser(_applicationUser);

            var ignoreAuditedEntity = _serviceProvider.GetRequiredService<IIgnoreAuditedEntity>();

            var entriesWithBase = ChangeTracker.Entries();

            var FullAuditedEntries = entriesWithBase
                .Where(entry => (entry.Metadata.ClrType.BaseType != null && entry.Metadata.ClrType.BaseType.Name.Contains("ThexFullAuditedEntity")) ||
                                (entry.Metadata.ClrType.BaseType.BaseType != null && entry.Metadata.ClrType.BaseType.BaseType.Name.Contains("ThexFullAuditedEntity")));

            if (FullAuditedEntries != null)
                foreach (EntityEntry entityEntry in FullAuditedEntries)
                {
                    var entity = entityEntry.Entity;
                    if (ignoreAuditedEntity.Any(entity))
                        continue;

                    switch (entityEntry.State)
                    {
                        case EntityState.Added:
                            entity.GetType().GetProperty("CreationTime").SetValue(entity, utcNowAuditDate, null);
                            entity.GetType().GetProperty("LastModificationTime").SetValue(entity, utcNowAuditDate, null);
                            entity.GetType().GetProperty("CreatorUserId").SetValue(entity, _userUid, null);
                            break;
                        case EntityState.Modified:
                            entityEntry.Property("CreationTime").IsModified = false;
                            entityEntry.Property("CreatorUserId").IsModified = false;

                            entity.GetType().GetProperty("LastModificationTime").SetValue(entity, utcNowAuditDate, null);
                            entity.GetType().GetProperty("LastModifierUserId").SetValue(entity, _userUid, null);
                            break;
                        case EntityState.Deleted:
                            entityEntry.Property("CreationTime").IsModified = false;
                            entityEntry.Property("CreatorUserId").IsModified = false;
                            entityEntry.Property("LastModificationTime").IsModified = false;
                            entityEntry.Property("LastModifierUserId").IsModified = false;

                            entity.GetType().GetProperty("DeletionTime").SetValue(entity, utcNowAuditDate, null);
                            entity.GetType().GetProperty("DeleterUserId").SetValue(entity, _userUid, null);
                            entity.GetType().GetProperty("IsDeleted").SetValue(entity, true);
                            entityEntry.State = EntityState.Modified;
                            break;
                    }
                }


            var MultiTentantEntries = entriesWithBase
                .Where(entry => (entry.Metadata.ClrType.BaseType != null && entry.Metadata.ClrType.BaseType.Name.Contains("ThexMultiTenantFullAuditedEntity")) ||
                                (entry.Metadata.ClrType.BaseType.BaseType != null && entry.Metadata.ClrType.BaseType.BaseType.Name.Contains("ThexMultiTenantFullAuditedEntity")));

            if (MultiTentantEntries != null)
                foreach (EntityEntry entityEntry in MultiTentantEntries)
                {
                    var entity = entityEntry.Entity;
                    if (ignoreAuditedEntity.Any(entity))
                        continue;

                    switch (entityEntry.State)
                    {
                        case EntityState.Added:
                            if (!entity.GetType().ToString().Contains("UserPassword"))
                                entity.GetType().GetProperty("TenantId").SetValue(entity, _tenantId, null);
                            break;
                        case EntityState.Modified:
                        case EntityState.Deleted:
                            entityEntry.Property("TenantId").IsModified = false;
                            break;
                    }
                }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new RoomTypeMapper());
            modelBuilder.ApplyConfiguration(new RoomTypeInventoryMapper());
            modelBuilder.ApplyConfiguration(new PropertyParameterMapper());

            base.OnModelCreating(modelBuilder.EnableAutoHistory(null));
        }

        public override int SaveChanges()
        {
            FillAuditFiedls();

            _genericLog.AddLog(_genericLog.DefaultBuilder
                       .WithMessage(this.EnsureAutoHistory())
                       .WithDetailedMessage(AppConsts.DatabaseModification)
                       .AsInformation()
                       .Build());

            return base.SaveChanges();
        }
    }
}