﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Thex.Inventory.Domain.Repositories.Interfaces;
using Thex.Inventory.Infra.Context;
using Thex.Inventory.Infra.Repositories;

namespace Thex.Inventory
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddInventoryDependency(this IServiceCollection services, IConfiguration configuration = null)
        {
            services
                .AddTnfEntityFrameworkCore()
                .AddTnfDbContext<ThexInventoryContext>((config) =>
                {
                    if (config.ExistingConnection != null)
                        config.DbContextOptions.UseSqlServer(config.ExistingConnection);
                    else
                        config.DbContextOptions.UseSqlServer(config.ConnectionString);
                });

            services.AddTnfDefaultConventionalRegistrations();

            // Registro dos repositórios
            services.AddTransient<IRoomTypeInventoryRepository, RoomTypeInventoryRepository>();
            services.AddTransient<IPropertyParameterRepository, PropertyParameterRepository>();

            return services;
        }

        public static IServiceCollection AddInventoryDependencyFunction(this IServiceCollection services, string connectionString)
        {
            services
              .AddTnfEntityFrameworkCore()
                .AddTnfDbContext<ThexInventoryContext>((config) =>
                {
                    if (config.ExistingConnection != null)
                        config.DbContextOptions.UseSqlServer(config.ExistingConnection);
                    else
                        config.DbContextOptions.UseSqlServer(connectionString);
                });

            services.AddTnfDefaultConventionalRegistrations();

            // Registro dos repositórios
            services.AddTransient<IRoomTypeInventoryRepository, RoomTypeInventoryRepository>();
            services.AddTransient<IPropertyParameterRepository, PropertyParameterRepository>();

            return services;
        }
    }
}
