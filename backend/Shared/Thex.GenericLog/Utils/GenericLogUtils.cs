﻿using System.Linq;
using System.Reflection;
using Thex.GenericLog.Extensions;

namespace Thex.GenericLog.Utils
{
    public static class GenericLogUtils
    {
        public static string GenerateCode(string error)
            => error;

        public static string GenerateMessage(string error)
            => InternalGenerateMessage(error.GetType(), error);

        public static string GenerateDetailedMessage(string error)
            => InternalGenerateMessage(error.GetType().GetMember(error.ToString()).FirstOrDefault(), error);

        private static string InternalGenerateMessage(MemberInfo memberInfo, string error)
        {
            var attribute = memberInfo. GetSingleAttributeOrNull<DescriptionAttribute>();
            return attribute != null ? attribute.Description : error.ToString();
        }
    }
}
