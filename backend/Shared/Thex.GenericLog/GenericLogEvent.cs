﻿using Newtonsoft.Json;
using System;

namespace Thex.GenericLog
{
    /// <inheritdoc />
    [Serializable]
    public class GenericLogEvent : IGenericLogEvent
    {
        #region Properties

        /// <inheritdoc />
        public Guid Guid { get; }
        /// <inheritdoc />
        [JsonIgnore]
        public GenericLogType GenericLogType { get; }
        /// <inheritdoc />
        public string Code { get; set; }
        /// <inheritdoc />
        public string Message { get; }
        /// <inheritdoc />
        public string DetailedMessage { get; }
        /// <inheritdoc />
        [JsonIgnore]
        public ResultStatus Status { get; }

        #endregion

        #region Constructors

        /// <summary>
        /// Private Ctor
        /// </summary>
        /// <param name="guid">Receive a Guid</param>
        /// <param name="GenericLogType">Receive a GenericLogType</param>
        /// <param name="code">Receive a Code</param>
        /// <param name="message">Receive a Message</param>
        /// <param name="detailedMessage">Receive a Detailed Message</param>
        /// <param name="status">Receive a Status</param>
        [JsonConstructor]
        private GenericLogEvent(Guid guid, GenericLogType GenericLogType, string code, string message, string detailedMessage, ResultStatus status)
        {
            this.Guid = guid;
            this.Code = code;
            this.GenericLogType = GenericLogType;
            this.Message = message;
            this.DetailedMessage = detailedMessage;
            this.Status = status;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Create a new IGenericLogEvent using a factory.
        /// </summary>
        /// <param name="GenericLogType">Type of GenericLog</param>
        /// <param name="code">Set a friendly message code</param>
        /// <param name="message">Set a message</param>
        /// <param name="detailedMessage">Set the detailed message</param>
        /// <param name="status">Set the status</param>
        /// <returns>An IGenericLogEvent instance</returns>
        internal static GenericLogEvent New(GenericLogType GenericLogType, string code, string message, string detailedMessage, ResultStatus status)
            => new GenericLogEvent(Guid.NewGuid(), GenericLogType, code, message, detailedMessage, status);

        public override string ToString()
            => $"{Guid} - {GenericLogType} => {Message} - {DetailedMessage}";

        public int GetHashCode(IGenericLogEvent obj)
            => obj.Guid.GetHashCode();

        public override int GetHashCode()
            => GetHashCode(this);

        public override bool Equals(object obj)
            => Equals(this, (IGenericLogEvent)obj);

        public bool Equals(IGenericLogEvent other)
            => Equals(this, other);

        public bool Equals(IGenericLogEvent x, IGenericLogEvent y)
            => x?.Guid == y?.Guid;

        public static bool operator ==(GenericLogEvent x, IGenericLogEvent y)
            => !(x is null) ? x.Equals(y) : y is null;

        public static bool operator !=(GenericLogEvent x, IGenericLogEvent y)
            => !(x == y);

        #endregion
    }
}
