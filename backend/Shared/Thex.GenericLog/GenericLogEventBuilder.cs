﻿using System;
using System.Diagnostics;
using Tnf.Localization;
using Thex.GenericLog.Utils;

namespace Thex.GenericLog
{
    public class GenericLogEventBuilder : IGenericLogEventBuilder
    {
        private string _codeEnum { get; set; }
        private string _code { get; set; } = string.Empty;
        private string _message { get; set; } = string.Empty;
        private string _detailedMessage { get; set; } = string.Empty;
        private ResultStatus _status { get; set; } = ResultStatus.Undefined;
        private object[] _messageFormat { get; set; }
        private GenericLogType _genericLogType { get; set; } = GenericLogType.Information;

        private readonly ILocalizationManager _localizationManager;
        private readonly IGenericLogHandler _GenericLogHandler;

        public GenericLogEventBuilder(
            IGenericLogHandler GenericLogHandler,
            ILocalizationManager localizationManager)
        {
            _GenericLogHandler = GenericLogHandler;
            _localizationManager = localizationManager;
        }

        public IGenericLogEventBuilder FromException(Exception ex, string error)
        {
            AsError();
            WithMessage(error);

            if (Debugger.IsAttached)
                WithDetailedMessage(ex.Message + " - " + ex.StackTrace);

            return this;
        }

        public IGenericLogEventBuilder WithMessage(string str)
        {
            var GenericLogMessage = str;
            _message = GenericLogMessage;
            return this;
        }

        public IGenericLogEventBuilder WithDetailedMessage(string detailedMessage)
        {
            _detailedMessage = detailedMessage;
            return this;
        }

        public IGenericLogEventBuilder WithType(GenericLogType type)
        {
            _genericLogType = type;
            return this;
        }

        public IGenericLogEventBuilder AsWarning() => WithType(GenericLogType.Warning);

        public IGenericLogEventBuilder AsError() => WithType(GenericLogType.Error);

        public IGenericLogEventBuilder AsSpecification() => WithType(GenericLogType.Specification);

        public IGenericLogEventBuilder AsInformation() => WithType(GenericLogType.Information);

        public IGenericLogEvent Build()
        {
            if (_codeEnum != null)
            {
                if (_message.IsNullOrEmpty())
                    _message = GenericLogUtils.GenerateMessage(_codeEnum);
                if (_detailedMessage.IsNullOrEmpty())
                    WithDetailedMessage(GenericLogUtils.GenerateDetailedMessage(_codeEnum));
            }

            if (_messageFormat?.Length > 0 && !_message.IsNullOrEmpty() && !_detailedMessage.IsNullOrEmpty())
            {
                _message = string.Format(_message, _messageFormat);
                WithDetailedMessage(string.Format(_detailedMessage, _messageFormat));
            }

            return GenericLogEvent.New(_genericLogType, _code, _message, _detailedMessage, _status);
        }

        public void Raise()
        {
            var @event = Build();
            _GenericLogHandler.AddLog(@event);
        }

        public void Throw()
        {
            Raise();
            throw new Exception("Force throw GenericLog");
        }

        #region IDisposable Support

        private bool disposedValue = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // dispose managed state (managed objects).
                }

                // free unmanaged resources (unmanaged objects) and override a finalizer below.
                // set large fields to null.

                disposedValue = true;
            }
        }

        ~GenericLogEventBuilder() => Dispose(false);

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}