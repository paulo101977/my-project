﻿namespace Thex.GenericLog
{
    /// <summary>
    /// Define GenericLog type and criteria for each kind of event
    /// </summary>
    public enum GenericLogType
    {
        /// <summary>
        /// Critical - Means that process can not be ended or a break happen
        /// </summary>
        Error,
        /// <summary>
        /// Information - Create messages to inform sucess or important events
        /// </summary>
        Information,
        /// <summary>
        /// Pre-Critical - Means something happen and a process ended.
        /// It can become Critical very fast or in the next steps ahead.
        /// </summary>
        Warning,
        /// <summary>
        /// Specification - Means that specification rule event trigger.
        /// </summary>
        Specification
    }
}
