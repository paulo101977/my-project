﻿using System;

namespace Thex.GenericLog
{
    public interface IGenericLogEventBuilder : IDisposable
    {

        IGenericLogEventBuilder FromException(Exception ex, string error);

        IGenericLogEventBuilder WithMessage(string enumeration);

        IGenericLogEventBuilder WithDetailedMessage(string detailedMessage);

        IGenericLogEventBuilder AsWarning();

        IGenericLogEventBuilder AsError();

        IGenericLogEventBuilder AsSpecification();

        IGenericLogEventBuilder AsInformation();

        IGenericLogEvent Build();

        void Raise();

        void Throw();
    }
}