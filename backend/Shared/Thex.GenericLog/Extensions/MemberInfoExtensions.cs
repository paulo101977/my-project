﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Thex.GenericLog.Extensions
{
    public static class MemberInfoExtensions
    {
        public static TAttribute GetSingleAttributeOrNull<TAttribute>(this MemberInfo memberInfo, bool inherit = true)
          where TAttribute : Attribute
        {
            var attrs = memberInfo.GetAttributes<TAttribute>(inherit);
            return attrs.FirstOrDefault();
        }

        public static IEnumerable<TAttribute> GetAttributes<TAttribute>(this MemberInfo memberInfo, bool inherit = true)
         where TAttribute : Attribute
        {
            foreach (var attr in memberInfo.GetCustomAttributes(typeof(TAttribute), inherit))
                yield return (TAttribute)attr;
        }
    }
}
