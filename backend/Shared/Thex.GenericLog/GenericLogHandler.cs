﻿using Elasticsearch.Net;
using Elasticsearch.Net.Aws;
using Microsoft.Extensions.DependencyInjection;
using Nest;
using Newtonsoft.Json;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Thex.Common;
using Thex.GenericLog.AutoHistory.Dto;
using Thex.Kernel;

namespace Thex.GenericLog
{
    /// <inheritdoc />
    public class GenericLogHandler : IGenericLogHandler
    {
        #region Properties

        private readonly Guid _instanceId = Guid.NewGuid();

        /// <summary>
        /// GenericLog List - Hold an IGenericLogEvent during a
        /// given lifestyle
        /// </summary>
        public readonly ConcurrentDictionary<Guid, IGenericLogEvent> _genericLog;
        private readonly IServiceProvider _provider;
        private readonly string _secretKey;
        private readonly string _accessKey;
        private readonly string _databaseLogName;

        /// <inheritdoc />
        public IGenericLogEventBuilder DefaultBuilder => _provider.GetRequiredService<IGenericLogEventBuilder>();
        public IApplicationUser ApplicationUser => _provider.GetRequiredService<IApplicationUser>();
        public ServicesConfiguration ServicesConfiguration => _provider.GetRequiredService<ServicesConfiguration>();

        #endregion

        #region Constructors

        /// <summary>
        /// Default Ctor
        /// </summary>
        public GenericLogHandler(IServiceProvider provider)
        {
            _genericLog = new ConcurrentDictionary<Guid, IGenericLogEvent>();
            _provider = provider;
            _secretKey = GenericLogHashManager.GenerateMD5Decrypto(ServicesConfiguration.ElasticSearch.Secretkey);
            _accessKey = GenericLogHashManager.GenerateMD5Decrypto(ServicesConfiguration.ElasticSearch.Accesskey);
            _databaseLogName = ServicesConfiguration.DatabaseLog;
        }

        #endregion

        #region Methods

        /// <inheritdoc />
        public void AddLog(params IGenericLogEvent[] GenericLogEvents)
        {
            foreach (var GenericLogEvent in GenericLogEvents)
            {
                GenericLogEvent.Code += _genericLog.Count.ToString();

                if (!_genericLog.ContainsKey(GenericLogEvent.Guid))
                    _genericLog.TryAdd(GenericLogEvent.Guid, GenericLogEvent);
            }
        }

        /// <inheritdoc />
        public void AddLog(IEnumerable<IGenericLogEvent> GenericLogEvents)
            => AddLog(GenericLogEvents.ToArray());

        public void AddLogInformation(string message)
        {
            DefaultBuilder
                .WithMessage(message)
                .AsInformation()
                .Raise();
        }

        public void AddLogError(string message)
        {
            DefaultBuilder
                .WithMessage(message)
                .AsError()
                .Raise();
        }

        public void AddLogWarning(string message)
        {
            DefaultBuilder
                .WithMessage(message)
                .AsWarning()
                .Raise();
        }

        public void AddLogSpecification(string message)
        {
            DefaultBuilder
                .WithMessage(message)
                .AsSpecification()
                .Raise();
        }

        /// <inheritdoc />
        public void Delete(IGenericLogEvent GenericLogEvent)
            => _genericLog.TryRemove(GenericLogEvent.Guid, out GenericLogEvent);

        /// <inheritdoc />
        public ResultStatus? GetStatus()
            => GetAll()
                .FirstOrDefault(x => x.Status != ResultStatus.Undefined)
                ?.Status;

        /// <inheritdoc />
        public IEnumerable<IGenericLogEvent> GetAll()
        {
            foreach (var notificaton in _genericLog)
                yield return notificaton.Value;
        }

        /// <inheritdoc />
        public IEnumerable<IGenericLogEvent> GetAll(Func<IGenericLogEvent, bool> predicate)
        {
            foreach (var notificaton in _genericLog)
            {
                if (predicate(notificaton.Value))
                    yield return notificaton.Value;
            }
        }

        /// <inheritdoc />
        public bool HasGenericLog()
            => !_genericLog.IsNullOrEmpty();

        /// <inheritdoc />
        public bool HasErrorGenericLog()
            => GetAll()
                .Any(x => x.GenericLogType == GenericLogType.Error);

        /// <inheritdoc />
        public bool HasInformationGenericLog()
            => GetAll()
                .Any(x => x.GenericLogType == GenericLogType.Information);

        /// <inheritdoc />
        public bool HasWarningGenericLog()
            => GetAll()
                .Any(x => x.GenericLogType == GenericLogType.Warning);

        /// <inheritdoc />
        public bool HasSpecificationGenericLog()
            => GetAll()
                .Any(x => x.GenericLogType == GenericLogType.Specification);

        /// <inheritdoc />
        public bool HasGenericLog(GenericLogType type)
            => GetAll()
                .Any(x => x.GenericLogType == type);

        /// <inheritdoc />
        public IEnumerable<IGenericLogEvent> GetByCode(string code)
            => GetAll(x => x.Code == code);

        /// <inheritdoc />
        public IEnumerable<IGenericLogEvent> GetByMessage(string message)
            => GetAll(x => x.Message.Contains(message));

        /// <inheritdoc />
        public IEnumerable<IGenericLogEvent> GetByGuid(Guid guid)
            => GetAll(x => x.Guid == guid);

        /// <inheritdoc />
        public IEnumerable<IGenericLogEvent> GetByType(GenericLogType type)
            => GetAll(x => x.GenericLogType == type);

        [ExcludeFromCodeCoverage]
        public override string ToString()
            => _instanceId.ToString();

        public void Commit(string indexName = "database-log")
        {
            indexName = _databaseLogName ?? indexName;

            if (HasGenericLog())
            {
                var awsCredentials = new Amazon.Runtime.BasicAWSCredentials(_accessKey, _secretKey);
                var httpConnection = new AwsHttpConnection(awsCredentials, Amazon.RegionEndpoint.USEast1);
                var pool = new SingleNodeConnectionPool(new Uri(ServicesConfiguration.ElasticSearch.Uri));
                var config = new ConnectionSettings(pool, httpConnection);

                var client = new ElasticClient(config);

                client.IndexAsync(CreateElasticSearchLog(_genericLog), i => i.Index(indexName));
            }
        }

        private GenericLogElasticSearchDto CreateElasticSearchLog(ConcurrentDictionary<Guid, IGenericLogEvent> genericLog)
        {
            var log = new GenericLogElasticSearchDto()
            {
                Creation = DateTime.Now,
                UserId = ApplicationUser.UserUid.ToString(),
                TennantId = ApplicationUser.TenantId.ToString(),
                PropertyId = ApplicationUser.PropertyId,
                PropertyUid = ApplicationUser.PropertyUid,
                Details = new Details()
            };

            foreach (var gLog in genericLog.OrderBy(x => x.Value.Code))
            {
                var operations = new Operations()
                {
                    Code = gLog.Value.Code,
                    DetailedMessage = gLog.Value.DetailedMessage,
                    Messsage = gLog.Value.Message
                };

                if (operations.DetailedMessage.Contains(AppConsts.DatabaseModification))
                {
                    var autoHistory = JsonConvert.DeserializeObject<Microsoft.EntityFrameworkCore.AutoHistory>(gLog.Value.Message);

                    if (autoHistory != null)
                    {
                        // Change message value
                        operations.Messsage = operations.DetailedMessage;
                        operations.DataBase.Kind = autoHistory.Kind.ToString();
                        operations.DataBase.TableName = autoHistory.TableName;

                        foreach (var property in JsonConvert.DeserializeObject<Dictionary<string, Object>>(autoHistory.Changed))
                        {
                            if (property.Key.ToUpper() == "AFTER")
                            {
                                var detail = string.Empty;

                                foreach (var prop in JsonConvert.DeserializeObject<Dictionary<string, Object>>(property.Value.ToString()))
                                {
                                    detail += $"Field: {prop.Key} - Value: {prop.Value} |";
                                }

                                operations.DataBase.After = detail;
                            }

                            if (property.Key == "before")
                            {
                                var detail = string.Empty;

                                foreach (var prop in JsonConvert.DeserializeObject<Dictionary<string, Object>>(property.Value.ToString()))
                                {
                                    detail += $"Field: {prop.Key} Value: {prop.Value} |";
                                }

                                operations.DataBase.Before = detail;
                            }
                        }
                    }
                }

                if (operations.DetailedMessage.Contains(AppConsts.Exception))
                {
                    operations.Exception.Trace = "";
                }

                log.Details.Operations.Add(operations);
            }

            return log;
        }

        #endregion
    }
}
