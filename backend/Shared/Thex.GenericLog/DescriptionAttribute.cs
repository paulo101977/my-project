﻿using System;

namespace Thex.GenericLog
{
    [AttributeUsage(AttributeTargets.All)]
    public class DescriptionAttribute : Attribute
    {
        public DescriptionAttribute(string description)
            => Description = description;

        public string Description { get; set; }
    }
}
