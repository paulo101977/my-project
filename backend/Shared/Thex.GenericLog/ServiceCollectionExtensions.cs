﻿using Thex.GenericLog;
using Tnf;
using Tnf.Notifications;

// ReSharper disable once CheckNamespace
namespace Microsoft.Extensions.DependencyInjection
{
    public static class ServiceCollectionExtensions
    {
        /// <summary>
        /// Add Tnf Notifications
        /// </summary>
        /// <param name="services">The <see cref="IServiceCollection"/> to add the service</param>
        public static IServiceCollection AddThexGenericLog(this IServiceCollection services)
        {
            services.AddScoped<IGenericLogEventBuilder, GenericLogEventBuilder>();
            services.AddScoped<IGenericLogHandler, GenericLogHandler>();

            return services;
        }
    }
}
