﻿using Nest;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Text;

namespace Thex.GenericLog.AutoHistory.Dto
{
    public class GenericLogElasticSearchDto
    {
        public DateTime Creation { get; set; }

        public string UserId { get; set; }

        public string TennantId { get; set; }

        public string PropertyId { get; set; }
        public string PropertyUid { get; set; }

        public Details Details { get; set; }
    }


    public class Details
    {
        public Details()
        {
            this.Operations = new List<Operations>();
        }

        public List<Operations> Operations { get; set; }
    }

    public class Operations
    {
        public Operations()
        {
            this.DataBase = new DataBase();
            this.Exception = new Exception();
        }

        public string Code { get; set; }

        public string DetailedMessage { get; set; }

        public string Messsage { get; set; }

        public DataBase DataBase { get; set; }

        public Exception Exception { get; set; }
    }

    public class DataBase
    {
        public string Kind { get; set; }

        public string TableName { get; set; }

        public string Before { get; set; }

        public string After { get; set; }
    }

    public class Changed
    {
        public string Before { get; set; }

        public string After { get; set; }
    }


    public class Exception
    {
        public string Trace { get; set; }
    }
}
