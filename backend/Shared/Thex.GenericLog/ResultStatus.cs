﻿namespace Thex.GenericLog
{
    //
    // Summary:
    //     Contains the values of status codes.
    public enum ResultStatus
    {
        //
        // Summary:
        //     Indicates the status haven't been set.
        Undefined = 0,
        //
        // Summary:
        //     Indicates that the client can continue with its request.
        Continue = 100,
        //
        // Summary:
        //     Indicates that the protocol version or protocol is being changed.
        SwitchingProtocols = 101,
        //
        // Summary:
        //     Indicates that the server has received and is processing the request, but no response is
        //     available yet.
        Processing = 102,
        //
        // Summary:
        //     Indicates that the request succeeded and that the requested information is in the response. This
        //     is the most common status code to receive.
        Ok = 200,
        //
        // Summary:
        //     Indicates that the request resulted in a new resource created before the response was sent.
        Created = 201,
        //
        // Summary:
        //     Indicates that the request has been accepted for further processing.
        Accepted = 202,
        //
        // Summary:
        //     Indicates that the returned metainformation is from a cached copy instead of
        //     the origin server and therefore may be incorrect.
        NonAuthoritativeInformation = 203,
        //
        // Summary:
        //     Indicates that the request has been successfully processed and that the response is intentionally
        //     blank.
        NoContent = 204,
        //
        // Summary:
        //     Indicates that the client should reset (not reload) the current resource.
        ResetContent = 205,
        //
        // Summary:
        //     Indicates that the response is a partial response as requested by a GET request that includes
        //     a byte range.
        PartialContent = 206,
        //
        // Summary:
        //     Indicates that the The message body that follows is an XML message and can contain a number of
        //     separate response codes, depending on how many sub-requests were made.
        MultiStatus = 207,
        //
        // Summary:
        //     Indicates that the members of a DAV binding have already been enumerated in a preceding part of
        //     the (multistatus) response, and are not being included again.
        AlreadyReported = 208,
        //
        // Summary:
        //     Indicates that the server has fulfilled a request for the resource, and the response is a
        //     representation of the result of one or more instance-manipulations applied to the current
        //     instance.
        ImUsed = 226,
        //
        // Summary:
        //     Indicates that the requested information has multiple representations. The default action
        //     is to treat this status as a redirect and follow the contents of the Location
        //     header associated with this response.
        Ambiguous = 300,
        //
        // Summary:
        //     Indicates that the requested information has multiple representations. The default action
        //     is to treat this status as a redirect and follow the contents of the Location
        //     header associated with this response.
        MultipleChoices = 300,
        //
        // Summary:
        //     Indicates that the requested information has been moved to the URI specified in the Location
        //     header. The default action when this status is received is to follow the Location
        //     header associated with the response. When the original request method was POST,
        //     the redirected request will use the GET method.
        Moved = 301,
        //
        // Summary:
        //     Indicates that the requested information has been moved to the URI specified in the Location
        //     header. The default action when this status is received is to follow the Location
        //     header associated with the response.
        MovedPermanently = 301,
        //
        // Summary:
        //     Indicates that the requested information is located at the URI specified in the Location header.
        //     The default action when this status is received is to follow the Location header
        //     associated with the response. When the original request method was POST, the
        //     redirected request will use the GET method.
        Found = 302,
        //
        // Summary:
        //     Indicates that the requested information is located at the URI specified in the Location header.
        //     The default action when this status is received is to follow the Location header
        //     associated with the response. When the original request method was POST, the
        //     redirected request will use the GET method.
        Redirect = 302,
        //
        // Summary:
        //     Automatically redirects the client to the URI specified in the Location header as the result
        //     of a POST. The request to the resource specified by the Location header will
        //     be made with a GET.
        RedirectMethod = 303,
        //
        // Summary:
        //     Automatically redirects the client to the URI specified in the Location header as the result
        //     of a POST. The request to the resource specified by the Location header will
        //     be made with a GET.
        SeeOther = 303,
        //
        // Summary:
        //     Indicates that the client's cached copy is up to date. The contents of the resource are
        //     not transferred.
        NotModified = 304,
        //
        // Summary:
        //     Indicates that the request should use the proxy server at the URI specified in the Location
        //     header.
        UseProxy = 305,
        //
        // Summary:
        //     Is a proposed extension to the specification that is not fully specified.
        Unused = 306,
        //
        // Summary:
        //     Indicates that the request information is located at the URI specified in the Location
        //     header. The default action when this status is received is to follow the Location
        //     header associated with the response. When the original request method was POST,
        //     the redirected request will also use the POST method.
        RedirectKeepVerb = 307,
        //
        // Summary:
        //     Indicates that the request information is located at the URI specified in the Location
        //     header. The default action when this status is received is to follow the Location
        //     header associated with the response. When the original request method was POST,
        //     the redirected request will also use the POST method.
        TemporaryRedirect = 307,
        //
        // Summary:
        //     Indicates that the request and all future requests should be repeated using another URI. 307 and
        //     308 parallel the behaviors of 302 and 301, but do not allow the method to change. So, for
        //     example, submitting a form to a permanently redirected resource may continue smoothly.
        PermanentRedirect = 308,
        //
        // Summary:
        //     Indicates that the request could not be understood by the server. Is sent when no other error is
        //     applicable, or if the exact error is unknown or does not have its own error code.
        BadRequest = 400,
        //
        // Summary:
        //     Indicates that the requested resource requires authentication.
        Unauthorized = 401,
        //
        // Summary:
        //     Is reserved for future use.
        PaymentRequired = 402,
        //
        // Summary:
        //     Indicates that the server refuses to fulfill the request.
        Forbidden = 403,
        //
        // Summary:
        //     Indicates that the requested resource does not exist on the server.
        NotFound = 404,
        //
        // Summary:
        //     Indicates that the request method (POST or GET) is not allowed on the requested resource.
        MethodNotAllowed = 405,
        //
        // Summary:
        //     Indicates that the client has indicated with Accept headers that it will not accept any
        //     of the available representations of the resource.
        NotAcceptable = 406,
        //
        // Summary:
        //     Indicates that the requested proxy requires authentication. The Proxy-authenticate
        //     header contains the details of how to perform the authentication.
        ProxyAuthenticationRequired = 407,
        //
        // Summary:
        //     Indicates that the client did not send a request within the time the server was expecting
        //     the request.
        RequestTimeout = 408,
        //
        // Summary:
        //     Indicates that the request could not be carried out because of a conflict on the server.
        Conflict = 409,
        //
        // Summary:
        //     Indicates that the requested resource is no longer available.
        Gone = 410,
        //
        // Summary:
        //     Indicates that the required Content-length header is missing.
        LengthRequired = 411,
        //
        // Summary:
        //     Indicates that a condition set for this request failed, and the request cannot be carried
        //     out. Conditions are set with conditional request headers like If-Match, If-None-Match,
        //     or If-Unmodified-Since.
        PreconditionFailed = 412,
        //
        // Summary:
        //     Indicates that the request is too large for the server to process.
        RequestEntityTooLarge = 413,
        //
        // Summary:
        //     Indicates that the URI is too long.
        RequestUriTooLong = 414,
        //
        // Summary:
        //     Indicates that the request is an unsupported type.
        UnsupportedMediaType = 415,
        //
        // Summary:
        //     Indicates that the range of data requested from the resource cannot be returned,
        //     either because the beginning of the range is before the beginning of the resource,
        //     or the end of the range is after the end of the resource.
        RequestedRangeNotSatisfiable = 416,
        //
        // Summary: 
        //     Indicates that an expectation given in an Expect header could not be met by the server.
        ExpectationFailed = 417,
        //
        // Summary:
        //     This code should be returned by teapots requested to brew coffee.
        Teapot = 418,
        //
        // Summary:
        //     Indicates that the request was directed at a server that is not able to produce a response (for
        //     example because a connection reuse).
        MisdirectedRequest = 421,
        //
        // Summary:
        //     Indicates that the request was well-formed but was unable to be followed due to semantic errors.
        UnprocessableEntity = 422,
        //
        // Summary:
        //     Indicates that the resource that is being accessed is locked.
        Locked = 423,
        //
        // Summary:
        //     Indicates that the request failed due to failure of a previous request (e.g., a PROPPATCH).
        FailedDependency = 424,
        //
        // Summary:
        //     Indicates that the client should switch to a different protocol such as TLS/1.0.
        UpgradeRequired = 426,
        //
        // Summary:
        //     Indicates that the origin server requires the request to be conditional.
        PreconditionRequired = 428,
        //
        // Summary:
        //     Indicates that the user has sent too many requests in a given amount of time.
        TooManyRequests = 429,
        //
        // Summary:
        //     Indicates that the server is unwilling to process the request because either an individual header
        //     field, or all the header fields collectively, are too large.
        RequestHeaderFieldsTooLarge = 431,
        //
        // Summary:
        //     Indicates that the server operator has received a legal demand to deny access to a resource or to
        //     a set of resources that includes the requested resource.
        UnavailableForLegalReasons = 451,
        //
        // Summary:
        //     Indicates that a generic error has occurred on the server.
        InternalServerError = 500,
        //
        // Summary: 
        //     Indicates that the server does not support the requested function.
        NotImplemented = 501,
        //
        // Summary: 
        //     Indicates that an intermediate proxy server received a bad response from another proxy
        //     or the origin server.
        BadGateway = 502,
        //
        // Summary: 
        //     Indicates that the server is temporarily unavailable, usually due to high load or maintenance.
        ServiceUnavailable = 503,
        //
        // Summary:
        //     Indicates that an intermediate proxy server timed out while waiting for a response from
        //     another proxy or the origin server.
        GatewayTimeout = 504,
        //
        // Summary:
        //     Indicates that the version is not supported by the server.
        VersionNotSupported = 505,
        //
        // Summary:
        //     Indicates that transparent content negotiation for the request results in a circular reference.
        VariantAlsoNegotiates = 506,
        //
        // Summary:
        //     Indicates that the server is unable to store the representation needed to complete the request.
        InsufficientStorage = 507,
        //
        // Summary:
        //     Indicates that the server detected an infinite loop while processing the request (sent in lieu of
        //     208 Already Reported).
        LoopDetected = 508,
        //
        // Summary:
        //     Indicates that further extensions to the request are required for the server to fulfil it.
        NotExtended = 510,
        //
        // Summary:
        //     Indicates that the client needs to authenticate to gain network access. Intended for use by
        //     intercepting proxies used to control access to the network (e.g., "captive portals" used to
        //     require agreement to Terms of Service before granting full Internet access via a Wi-Fi hotspot).
        NetworkAuthenticationRequired = 511
    }
}
