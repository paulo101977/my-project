﻿using System;
using System.Collections.Generic;

namespace Thex.GenericLog
{
    /// <summary>
    /// Define GenericLog handler to handle IGenericLogEvents
    /// </summary>
    public interface IGenericLogHandler
    {
        /// <summary>
        /// Raise a new IGenericLogEvent, adding the IGenericLogEvent
        /// instance to IGenericLogEvent List
        /// </summary>
        /// <param name="GenericLogEvents">IGenericLogEvent instance</param>
        void AddLog(params IGenericLogEvent[] GenericLogEvents);

        /// <summary>
        /// Raise a new Error GenericLog
        /// </summary>
        /// <param name="sourceName">Localization source name.</param>
        /// <param name="enumeration">Localization enum key.</param>
        //void AddLogError(string enumeration);

        /// <summary>
        /// Delete a IGenericLogEvent object from List
        /// </summary>
        /// <param name="GenericLogEvent">IGenericLogEvent to be deleted</param>
        void Delete(IGenericLogEvent GenericLogEvent);

        /// <summary>
        /// Holds a list of IGenericLogEvent during the IGenericLogHandler lifetime
        /// </summary>
        /// <returns>GenericLog List</returns>
        IEnumerable<IGenericLogEvent> GetAll();

        /// <summary>
        /// Get all IGenericLogEvent objects from List filtering by a given predicate
        /// </summary>
        /// <param name="predicate">Predicate func</param>
        /// <returns>GenericLog list</returns>
        IEnumerable<IGenericLogEvent> GetAll(Func<IGenericLogEvent, bool> predicate);

        /// <summary>
        /// Query IGenericLogList to check if have any IGenericLogEvent object
        /// </summary>
        /// <returns>True if has any type of GenericLog</returns>
        bool HasGenericLog();

        /// <summary>
        /// Query IGenericLogList by Type
        /// </summary>
        /// <param name="type">GenericLog type for query</param>
        /// <returns>True if has any type of a given notificaton exists</returns>
        bool HasGenericLog(GenericLogType type);

        /// <summary>
        /// Query IGenericLogList by Guid
        /// </summary>
        /// <param name="guid">GenericLogEvent Guid</param>
        /// <returns>GenericLog list filtered by guid</returns>
        IEnumerable<IGenericLogEvent> GetByGuid(Guid guid);

        /// <summary>
        /// Builder to set the GenericLog fields
        /// </summary>
        IGenericLogEventBuilder DefaultBuilder { get; }

        void Commit(string indexName = "database-log");
    }
}
