﻿using System;
using System.Collections.Generic;

namespace Thex.GenericLog
{
    /// <summary>
    /// Define a IGenericLogEvent object
    /// </summary>
    public interface IGenericLogEvent : IEquatable<IGenericLogEvent>, IEqualityComparer<IGenericLogEvent>
    {
        /// <summary>
        /// GenericLog Guid. An universal and unique identification
        /// </summary>
        Guid Guid { get; }
        /// <summary>
        /// Define GenericLog type to an IGenericLogEvent instance
        /// </summary>
        GenericLogType GenericLogType { get; }
        /// <summary>
        /// Set Message code. It is a friendly name to an IGenericLogEvent instance
        /// </summary>
        string Code { get; set; }
        /// <summary>
        /// Set message to GenericLog
        /// </summary>
        string Message { get; }
        /// <summary>
        /// Set the detailed message to GenericLog. It is a technical message
        /// </summary>
        string DetailedMessage { get; }
        /// <summary>
        /// Status of the GenericLog
        /// </summary>
        ResultStatus Status { get; }
    }
}
