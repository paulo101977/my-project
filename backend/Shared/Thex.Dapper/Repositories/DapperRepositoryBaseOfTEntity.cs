﻿using Dapper;
using DapperExtensions;
using DapperExtensions.Mapper;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Thex.GenericLog;
using Tnf.Dapper.Extensions;
using Tnf.Dapper.Filters.Action;
using Tnf.Dapper.Filters.Query;
using Tnf.Dto;
using Tnf.Repositories;
using Tnf.Repositories.Entities;

namespace Tnf.Dapper.Repositories
{
    public class DapperRepositoryBase<TEntity> : IDapperRepository<TEntity>
        where TEntity : class
    {
        private static string NoMapperFound;

        public static string EntityName { get; private set; }

        private readonly IActiveTransactionProvider _activeTransactionProvider;
        private readonly IRepositoryConfiguration _repositoryConfiguration;
        private readonly IGenericLogHandler _genericLogHandler;

        static DapperRepositoryBase()
        {
            EntityName = typeof(TEntity).Name;
            NoMapperFound = $@"Nenhum mapeamento encontrado para a entidade {EntityName}. Verifique se existe o mapeamento para ClassMapper<TEntity>";
        }

        public DapperRepositoryBase(IActiveTransactionProvider activeTransactionProvider,
                                    IGenericLogHandler genericLogHandler)
        {
            _activeTransactionProvider = activeTransactionProvider;
            _genericLogHandler = genericLogHandler;
            DapperQueryFilterExecuter = activeTransactionProvider.GetService<IDapperQueryFilterExecuter>();
            DapperActionFilterExecuter = activeTransactionProvider.GetService<IDapperActionFilterExecuter>();
            _repositoryConfiguration = activeTransactionProvider.GetService<IRepositoryConfiguration>();

            DapperExtensions.DapperExtensions.SetLogHandler(_genericLogHandler);
        }

        public IDapperQueryFilterExecuter DapperQueryFilterExecuter { get; set; }

        public IDapperActionFilterExecuter DapperActionFilterExecuter { get; set; }

        public virtual DbConnection Connection
        {
            get { return (DbConnection)_activeTransactionProvider.GetActiveConnection(ActiveTransactionProviderArgs.Empty); }
        }

        /// <summary>
        ///     Gets the active transaction. If Dapper is active then <see cref="Tnf.Repositories.Uow.IUnitOfWork" /> should be started before
        ///     and there must be an active transaction.
        /// </summary>
        /// <value>
        ///     The active transaction.
        /// </value>
        public virtual DbTransaction ActiveTransaction
        {
            get { return (DbTransaction)_activeTransactionProvider.GetActiveTransaction(ActiveTransactionProviderArgs.Empty); }
        }

        public virtual TEntity Single(Expression<Func<TEntity, bool>> predicate)
        {
            Check.NotNull(predicate, nameof(predicate));

            IPredicate pg = DapperQueryFilterExecuter.ExecuteFilter(predicate);

            return Connection.GetList<TEntity>(pg, transaction: ActiveTransaction).Single();
        }

        public virtual Task<TEntity> SingleAsync(Expression<Func<TEntity, bool>> predicate)
        {
            return Task.FromResult(Single(predicate));
        }

        public virtual TEntity FirstOrDefault(Expression<Func<TEntity, bool>> predicate)
        {
            Check.NotNull(predicate, nameof(predicate));

            IPredicate pg = DapperQueryFilterExecuter.ExecuteFilter(predicate);

            return Connection.GetList<TEntity>(pg, transaction: ActiveTransaction).FirstOrDefault();
        }

        public virtual Task<TEntity> FirstOrDefaultAsync(Expression<Func<TEntity, bool>> predicate)
        {
            return Task.FromResult(FirstOrDefault(predicate));
        }

        public virtual IEnumerable<TEntity> GetAll()
        {
            PredicateGroup predicateGroup = DapperQueryFilterExecuter.ExecuteFilter<TEntity>();

            return Connection.GetList<TEntity>(predicateGroup, transaction: ActiveTransaction);
        }

        public virtual Task<IEnumerable<TEntity>> GetAllAsync()
        {
            return Task.FromResult(GetAll());
        }

        public virtual IEnumerable<TEntity> Query(string query, object parameters = null)
        {
            return Query<TEntity>(query, parameters);
        }

        public virtual Task<IEnumerable<TEntity>> QueryAsync(string query, object parameters = null)
        {
            return QueryAsync<TEntity>(query, parameters);
        }

        public virtual IEnumerable<TAny> Query<TAny>(string query, object parameters = null)
            where TAny : class
        {
            Check.NotNullOrWhiteSpace(query, nameof(query));

            return Connection.Query<TAny>(query, parameters, ActiveTransaction);
        }

        public virtual Task<IEnumerable<TAny>> QueryAsync<TAny>(string query, object parameters = null)
            where TAny : class
        {
            Check.NotNullOrWhiteSpace(query, nameof(query));

            return Connection.QueryAsync<TAny>(query, parameters, ActiveTransaction);
        }

        public virtual int Execute(string query, object parameters = null)
        {
            Check.NotNullOrWhiteSpace(query, nameof(query));

            return Connection.Execute(query, parameters, ActiveTransaction);
        }

        public virtual Task<int> ExecuteAsync(string query, object parameters = null)
        {
            Check.NotNullOrWhiteSpace(query, nameof(query));

            return Connection.ExecuteAsync(query, parameters, ActiveTransaction);
        }

        public virtual IEnumerable<TEntity> GetAllPaged(Expression<Func<TEntity, bool>> predicate, int pageNumber, int itemsPerPage, string sortingProperty, bool ascending = true)
        {
            Check.NotNull(predicate, nameof(predicate));

            IPredicate filteredPredicate = DapperQueryFilterExecuter.ExecuteFilter<TEntity>(predicate);

            return Connection.GetPage<TEntity>(
                filteredPredicate,
                new List<ISort> { new Sort { Ascending = ascending, PropertyName = sortingProperty } },
                pageNumber,
                itemsPerPage,
                ActiveTransaction);
        }

        public virtual Task<IEnumerable<TEntity>> GetAllPagedAsync(Expression<Func<TEntity, bool>> predicate, int pageNumber, int itemsPerPage, string sortingProperty, bool ascending = true)
        {
            return Task.FromResult(GetAllPaged(predicate, pageNumber, itemsPerPage, sortingProperty, ascending));
        }

        public virtual int Count(Expression<Func<TEntity, bool>> predicate)
        {
            Check.NotNull(predicate, nameof(predicate));

            IPredicate filteredPredicate = DapperQueryFilterExecuter.ExecuteFilter(predicate);

            return Connection.Count<TEntity>(filteredPredicate, ActiveTransaction);
        }

        public virtual Task<int> CountAsync(Expression<Func<TEntity, bool>> predicate)
        {
            return Task.FromResult(Count(predicate));
        }

        public virtual IEnumerable<TEntity> GetAll(Expression<Func<TEntity, bool>> predicate)
        {
            Check.NotNull(predicate, nameof(predicate));

            IPredicate filteredPredicate = DapperQueryFilterExecuter.ExecuteFilter(predicate);

            return Connection.GetList<TEntity>(filteredPredicate, transaction: ActiveTransaction);
        }

        public virtual Task<IEnumerable<TEntity>> GetAllAsync(Expression<Func<TEntity, bool>> predicate)
        {
            return Task.FromResult(GetAll(predicate));
        }

        public virtual IEnumerable<TEntity> GetAllPaged(Expression<Func<TEntity, bool>> predicate, int pageNumber, int itemsPerPage, bool ascending = true, params Expression<Func<TEntity, object>>[] sortingExpression)
        {
            Check.NotNull(predicate, nameof(predicate));

            IPredicate filteredPredicate = DapperQueryFilterExecuter.ExecuteFilter(predicate);

            return Connection.GetPage<TEntity>(filteredPredicate, sortingExpression.ToSortable(ascending), pageNumber, itemsPerPage, ActiveTransaction);
        }

        public virtual Task<IEnumerable<TEntity>> GetAllPagedAsync(Expression<Func<TEntity, bool>> predicate, int pageNumber, int itemsPerPage, bool ascending = true, params Expression<Func<TEntity, object>>[] sortingExpression)
        {
            return Task.FromResult(GetAllPaged(predicate, pageNumber, itemsPerPage, ascending, sortingExpression));
        }

        public virtual void Insert(TEntity entity)
        {
            Check.NotNull(entity, nameof(entity));

            DapperActionFilterExecuter.ExecuteCreationAuditFilter(entity);

            Connection.Insert(entity, ActiveTransaction);
        }

        public virtual Task InsertAsync(TEntity entity)
        {
            Insert(entity);
            return Task.CompletedTask;
        }

        public virtual void Insert(params TEntity[] entities)
        {
            Check.NotNull(entities, nameof(entities));

            foreach (var entity in entities)
            {
                Insert(entity);
            }
        }

        public virtual async Task InsertAsync(params TEntity[] entities)
        {
            Check.NotNull(entities, nameof(entities));

            foreach (var entity in entities)
            {
                await InsertAsync(entity).ForAwait();
            }
        }

        public virtual void Insert(IEnumerable<TEntity> entities)
        {
            Check.NotNull(entities, nameof(entities));

            foreach (var entity in entities)
            {
                Insert(entity);
            }
        }

        public virtual async Task InsertAsync(IEnumerable<TEntity> entities)
        {
            Check.NotNull(entities, nameof(entities));

            foreach (var entity in entities)
            {
                await InsertAsync(entity).ForAwait();
            }
        }

        public virtual void Update(TEntity entity)
        {
            Check.NotNull(entity, nameof(entity));

            DapperActionFilterExecuter.ExecuteModificationAuditFilter(entity);

            Connection.Update(entity, ActiveTransaction);
        }

        public virtual Task UpdateAsync(TEntity entity)
        {
            Update(entity);
            return Task.CompletedTask;
        }

        public virtual void Delete(TEntity entity)
        {
            Check.NotNull(entity, nameof(entity));

            if (entity is ISoftDelete)
            {
                DapperActionFilterExecuter.ExecuteDeletionAuditFilter(entity);
                Connection.Update(entity, ActiveTransaction);
            }
            else
            {
                Connection.Delete(entity, ActiveTransaction);
            }
        }

        public virtual Task DeleteAsync(TEntity entity)
        {
            Delete(entity);
            return Task.CompletedTask;
        }

        public virtual void Delete(Expression<Func<TEntity, bool>> predicate)
        {
            Check.NotNull(predicate, nameof(predicate));

            IEnumerable<TEntity> items = GetAll(predicate);
            foreach (TEntity entity in items)
            {
                Delete(entity);
            }
        }

        public virtual Task DeleteAsync(Expression<Func<TEntity, bool>> predicate)
        {
            Delete(predicate);
            return Task.CompletedTask;
        }

        public virtual TEntity Get<TRequestDto>(TRequestDto key)
            where TRequestDto : IRequestDto
        {
            Check.NotNull(key, nameof(key));

            var requestDtoQueryFilter = _repositoryConfiguration.GetRequestDtoQueryFilter<TEntity, TRequestDto>();

            var func = requestDtoQueryFilter(key);

            if (func == null)
                return default(TEntity);

            return FirstOrDefault(func);
        }

        public virtual Task<TEntity> GetAsync<TRequestDto>(TRequestDto key)
            where TRequestDto : IRequestDto
        {
            return Task.FromResult(Get(key));
        }

        public virtual IListDto<TDto> GetAll<TDto>(IRequestAllDto key, Expression<Func<TEntity, bool>> func = null, bool paginated = true, bool sorted = true)
        {
            Check.NotNull(key, nameof(key));

            var predicate = func == null ?
                DapperQueryFilterExecuter.ExecuteFilter<TEntity>() :
                DapperQueryFilterExecuter.ExecuteFilter(func);

            var sortList = new List<ISort>();

            if (sorted)
            {
                sortList = key.GetPropertiesOrder().Select(order => new Sort
                {
                    Ascending = order.Value == SortOrder.Ascending,
                    PropertyName = order.Key.FirstLetterToUpperCase()
                })
                .Cast<ISort>()
                .ToList();
            }

            if (sortList.Count == 0)
            {
                var mapp = DapperExtensions.DapperExtensions.GetMap<TEntity>();
                if (mapp == null)
                    throw new TnfException(NoMapperFound);

                var keys = mapp.Properties
                    .Where(w => w.KeyType != KeyType.NotAKey && w.Ignored == false)
                    .Select(s => s.Name);

                foreach (var item in keys)
                {
                    sortList.Add(new Sort
                    {
                        Ascending = true,
                        PropertyName = item
                    });
                }
            }

            var finalList = paginated ?
                Connection.GetPage<TEntity>(predicate, sortList, key.Page.Value - 1, key.PageSize.Value + 1, ActiveTransaction) :
                Connection.GetList<TEntity>(predicate, sortList, ActiveTransaction);

            //return paginated ?
            //    finalList.ToListDto<TEntity, TDto>(key) :
            //    finalList.ToListDto<TEntity, TDto>(false);

            return null;
        }

        public virtual Task<IListDto<TDto>> GetAllAsync<TDto>(IRequestAllDto key, Expression<Func<TEntity, bool>> func = null, bool paginated = true, bool sorted = true)
        {
            return Task.FromResult(GetAll<TDto>(key, func, paginated, sorted));
        }
    }
}
