﻿using System.Data.Common;
using Thex.GenericLog;
using Tnf.Repositories;

namespace Tnf.Dapper.Repositories
{
    public class DapperEfRepositoryBase<TDbContext, TEntity> : DapperRepositoryBase<TEntity>
        where TEntity : class
    {
        private readonly IActiveTransactionProvider _activeTransactionProvider;

        public DapperEfRepositoryBase(IActiveTransactionProvider activeTransactionProvider,
                                      IGenericLogHandler genericLogHandler)
            : base(activeTransactionProvider, genericLogHandler)
        {
            _activeTransactionProvider = activeTransactionProvider;
        }

        public ActiveTransactionProviderArgs ActiveTransactionProviderArgs
        {
            get
            {
                return new ActiveTransactionProviderArgs(typeof(TDbContext));
            }
        }

        public override DbConnection Connection => (DbConnection)_activeTransactionProvider.GetActiveConnection(ActiveTransactionProviderArgs);

        public override DbTransaction ActiveTransaction => (DbTransaction)_activeTransactionProvider.GetActiveTransaction(ActiveTransactionProviderArgs);
    }
}
