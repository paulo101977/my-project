﻿using Tnf.Dapper.Repositories;

namespace Tnf.Dapper
{
    public static class DapperAutoRepositoryTypes
    {
        static DapperAutoRepositoryTypes()
        {
            DefaultForEntityFrameworkCore = new DapperAutoRepositoryTypeAttribute(
                typeof(IDapperRepository<>),
                typeof(DapperEfRepositoryBase<,>));

            Default = new DapperAutoRepositoryTypeAttribute(
                typeof(IDapperRepository<>),
                typeof(DapperRepositoryBase<>));
        }

        public static DapperAutoRepositoryTypeAttribute DefaultForEntityFrameworkCore { get; }
        public static DapperAutoRepositoryTypeAttribute Default { get; }
    }
}
