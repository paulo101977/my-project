﻿using DapperExtensions;
using System;
using System.Linq.Expressions;

namespace Tnf.Dapper.Internal
{
    internal static class DapperExpressionExtensions
    {
        public static IPredicate ToPredicateGroup<TEntity>(this Expression<Func<TEntity, bool>> expression)
            where TEntity : class
        {
            Check.NotNull(expression, nameof(expression));

            var dev = new DapperExpressionVisitor<TEntity>();
            IPredicate pg = dev.Process(expression);

            return pg;
        }
    }
}
