﻿using System;
using System.Diagnostics;
using System.Reflection;
using Tnf;
using Tnf.Repositories;
using Tnf.Repositories.Entities;
using Tnf.Repositories.Uow;

// ReSharper disable once CheckNamespace
namespace Microsoft.Extensions.DependencyInjection
{
    internal static class TnfRepositoriesServiceCollectionExtensions
    {
        public static IServiceCollection AddTnfRepository(this IServiceCollection services)
        {
            Check.NotNull(services, nameof(services));

            if (services.IsRegistered<IRepositoryConfiguration>())
                return services;

            // Dependencies
            services.AddTnfRuntime();
            services.AddTnfNotifications();

            // Uow
           // services.AddSingleton<IUnitOfWorkDefaultOptions, UnitOfWorkDefaultOptions>();
            services.AddSingleton<IUnitOfWorkFilterExecuter, NullUnitOfWorkFilterExecuter>();
           // services.AddTransient<IConnectionStringResolver, DefaultConnectionStringResolver>();
            //services.AddTransient<ICurrentUnitOfWorkProvider, AsyncLocalCurrentUnitOfWorkProvider>();
            //services.AddTransient<IUnitOfWorkManager, UnitOfWorkManager>();

            // Configuration
            //services.AddSingleton<IRepositoryConfiguration, RepositoryConfiguration>();

            return services;
        }

        public static IServiceCollection AddTnfRepository(
           this IServiceCollection services,
           Type dbContextType,
           Type dbSetType,
           AutoRepositoryTypesAttribute defaultAutoRepositoryTypesAttribute)
        {
            Check.NotNull(services, nameof(services));
            Check.NotNull(dbContextType, nameof(dbContextType));
            Check.NotNull(dbSetType, nameof(dbSetType));
            Check.NotNull(defaultAutoRepositoryTypesAttribute, nameof(defaultAutoRepositoryTypesAttribute));

            // Dependencies
            //services.AddTnfRepository();

            // Register repositories
            var autoRepositoryAttr = dbContextType.GetSingleAttributeOrNull<AutoRepositoryTypesAttribute>() ?? defaultAutoRepositoryTypesAttribute;

            RegisterForDbContext(
                services,
                dbContextType,
                dbSetType,
                autoRepositoryAttr.RepositoryInterface,
                autoRepositoryAttr.RepositoryImplementation
            );

            if (autoRepositoryAttr.WithDefaultRepositoryInterfaces)
            {
                RegisterForDbContext(
                    services,
                    dbContextType,
                    dbSetType,
                    defaultAutoRepositoryTypesAttribute.RepositoryInterface,
                    autoRepositoryAttr.RepositoryImplementation
                );
            }

            return services;
        }

        public static IServiceCollection AddTnfRepository(
            this IServiceCollection services,
            Type entityType,
            AutoRepositoryTypesAttribute defaultAutoRepositoryTypesAttribute)
        {
            Check.NotNull(services, nameof(services));
            Check.NotNull(entityType, nameof(entityType));
            Check.NotNull(defaultAutoRepositoryTypesAttribute, nameof(defaultAutoRepositoryTypesAttribute));

            // Dependencies
           // services.AddTnfRepository();

            RegisterForType(
                services,
                entityType,
                defaultAutoRepositoryTypesAttribute.RepositoryInterface,
                defaultAutoRepositoryTypesAttribute.RepositoryImplementation);

            return services;
        }

        private static void RegisterForDbContext(
            IServiceCollection services,
            Type dbContextType,
            Type dbSetType,
            Type repositoryInterface,
            Type repositoryImplementation)
        {
            foreach (var entityTypeInfo in dbContextType.GetEntityTypeInfos(dbSetType))
            {
                var genericRepositoryType = repositoryInterface.MakeGenericType(entityTypeInfo.EntityType);
                var isRegistered = services.IsRegistered(genericRepositoryType);
         
                Debug.WriteLine($"[Tnf.Repositories]: Verificando se o repositório genérico {genericRepositoryType} está registrado: {isRegistered}");

                if (!isRegistered)
                {
                    var implType = repositoryImplementation.GetGenericArguments().Length == 1
                        ? repositoryImplementation.MakeGenericType(entityTypeInfo.EntityType)
                        : repositoryImplementation.MakeGenericType(entityTypeInfo.DeclaringType,
                            entityTypeInfo.EntityType);

                    services.AddTransient(
                        genericRepositoryType,
                        implType);

                    Debug.WriteLine($"[Tnf.Repositories]: {genericRepositoryType} registrado para o tipo {implType}");
                }
            }
        }

        private static void RegisterForType(
            IServiceCollection services,
            Type dbEntityType,
            Type repositoryInterface,
            Type repositoryImplementation)
        {
            var genericRepositoryType = repositoryInterface.MakeGenericType(dbEntityType);
            var isRegistered = services.IsRegistered(genericRepositoryType);

            Debug.WriteLine($"[Tnf.Repositories]: Verificando se o repositório genérico {genericRepositoryType} está registrado: {isRegistered}");

            if (!isRegistered)
            {
                var implType = repositoryImplementation.MakeGenericType(dbEntityType);

                services.AddTransient(genericRepositoryType, implType);

                Debug.WriteLine($"[Tnf.Repositories]: {genericRepositoryType} registrado para o tipo {implType}");
            }
        }
    }
}
