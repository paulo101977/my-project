﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Tnf.Dto;
using Tnf.ObjectMapping;

namespace DapperExtensions
{
    public static class ToListDtoExtensions
    {
        /// <summary>
        /// Convert To <see cref="IListDto{T}"/>
        /// </summary>
        /// <param name="list">List.</param>
        /// <param name="requestDto">RequestAllDto.</param>
        /// <typeparam name="T">The element type of the <see cref="IEnumerable{T}"/></typeparam>
        /// <returns> <see cref="IListDto{T}"/> that contains elements from the input sequence.</returns>
        public static IListDto<T> ToListDto<T>(this IEnumerable<T> list, IRequestAllDto requestDto)
        {
            var hasNext = list.Count() > requestDto.PageSize;

            if (hasNext)
                list = list.WithoutLast();

            return list.ToListDto(hasNext);
        }

        /// <summary>
        /// Convert To <see cref="IListDto{TIDto}"/>
        /// </summary>
        /// <param name="list">List.</param>
        /// <param name="requestDto">RequestAllDto.</param>
        /// <typeparam name="T">The element type of the <see cref="IEnumerable{T}"/></typeparam>
        /// <typeparam name="TIDto">The return dto type</typeparam>
        /// <returns> <see cref="IListDto{TIDto}"/> that contains elements from the input sequence.</returns>
        public static IListDto<TIDto> ToListDto<T, TIDto>(this IEnumerable<T> list, IRequestAllDto requestDto)
        {
            var hasNext = list.Count() > requestDto.PageSize;

            if (hasNext)
                list = list.WithoutLast();

            if (typeof(T) == typeof(TIDto))
                return list.ToListDto(hasNext) as IListDto<TIDto>;

            return list.ToListDto<T, TIDto>(hasNext);
        }

        /// <summary>
        /// Convert To <see cref="IListDto{T}"/>
        /// </summary>
        /// <param name="list">List.</param>
        /// <param name="requestDto">RequestAllDto.</param>
        /// <typeparam name="T">The element type of the <see cref="IEnumerable{T}"/></typeparam>
        /// <returns> <see cref="IListDto{TIDto}"/> that contains elements from the input sequence.</returns>
        public static async Task<IListDto<T>> ToListDtoAsync<T>(this IEnumerable<T> list, IRequestAllDto requestDto)
            => await list.ToListDto<T>(requestDto).AsTask().ForAwait();

        /// <summary>
        /// Convert To <see cref="IListDto{TIDto}"/>
        /// </summary>
        /// <param name="list">List.</param>
        /// <param name="requestDto">RequestAllDto.</param>
        /// <typeparam name="T">The element type of the <see cref="IEnumerable{T}"/></typeparam>
        /// <typeparam name="TIDto">The return dto type</typeparam>
        /// <returns> <see cref="IListDto{TIDto}"/> that contains elements from the input sequence.</returns>
        public static async Task<IListDto<TIDto>> ToListDtoAsync<T, TIDto>(this IEnumerable<T> list, IRequestAllDto requestDto)
            => await list.ToListDto<T, TIDto>(requestDto).AsTask().ForAwait();

        /// <summary>
        /// Convert To <see cref="IListDto{T}"/>
        /// </summary>
        /// <param name="list">List.</param>
        /// <param name="hasNext">If the list has next page.</param>
        /// <typeparam name="T">The element type of the <see cref="IEnumerable{T}"/></typeparam>
        /// <returns> <see cref="IListDto{TIDto}"/> that contains elements from the input sequence.</returns>
        public static IListDto<T> ToListDto<T>(this IEnumerable<T> list, bool hasNext)
            => new ListDto<T> { HasNext = hasNext, Items = new List<T>(list) };

        /// <summary>
        /// Convert To <see cref="IListDto{TIDto}"/>
        /// </summary>
        /// <param name="list">List.</param>
        /// <param name="hasNext">If the list has next page.</param>
        /// <typeparam name="T">The element type of the <see cref="IEnumerable{T}"/></typeparam>
        /// <typeparam name="TIDto">The return dto type</typeparam>
        /// <returns> <see cref="IListDto{TIDto}"/> that contains elements from the input sequence.</returns>
        public static IListDto<TIDto> ToListDto<T, TIDto>(this IEnumerable<T> list, bool hasNext)
            => new ListDto<TIDto> { HasNext = hasNext, Items = ObjectMapper.Mapper.Map<List<TIDto>>(list) };

        /// <summary>
        /// Convert To <see cref="IListDto{T}"/>
        /// </summary>
        /// <param name="list">List.</param>
        /// <param name="hasNext">If the list has next page.</param>
        /// <typeparam name="T">The element type of the <see cref="IEnumerable{T}"/></typeparam>
        /// <returns> <see cref="IListDto{TIDto}"/> that contains elements from the input sequence.</returns>
        public static async Task<IListDto<T>> ToListDtoAsync<T>(this IEnumerable<T> list, bool hasNext)
            => await list.ToListDto(hasNext).AsTask().ForAwait();

        /// <summary>
        /// Convert To <see cref="IListDto{TIDto}"/>
        /// </summary>
        /// <param name="list">List.</param>
        /// <param name="hasNext">If the list has next page.</param>
        /// <typeparam name="T">The element type of the <see cref="IEnumerable{T}"/></typeparam>
        /// <typeparam name="TIDto">The return dto type</typeparam>
        /// <returns> <see cref="IListDto{TIDto}"/> that contains elements from the input sequence.</returns>
        public static async Task<IListDto<TIDto>> ToListDtoAsync<T, TIDto>(this IEnumerable<T> list, bool hasNext)
            => await list.ToListDto<T, TIDto>(hasNext).AsTask().ForAwait();
    }
}
