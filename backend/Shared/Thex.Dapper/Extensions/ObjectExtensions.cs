﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Text;

namespace DapperExtensions
{
    public static class ObjectExtensions
    {
        public static T As<T>(this object obj)
            where T : class
            => (T)obj;

        public static T To<T>(this object obj)
            where T : struct
        {
            if (typeof(T) == typeof(Guid))
                return (T)TypeDescriptor.GetConverter(typeof(T)).ConvertFromInvariantString(obj.ToString());

            return (T)Convert.ChangeType(obj, typeof(T), CultureInfo.InvariantCulture);
        }

        public static bool IsFunc(this object obj)
        {
            if (obj == null)
                return false;

            var type = obj.GetType();
            if (!type.IsGenericType)
                return false;

            return type.GetGenericTypeDefinition() == typeof(Func<>);
        }

        public static bool IsFunc<TReturn>(this object obj)
            => obj != null && obj is Func<TReturn>;
    }
}
