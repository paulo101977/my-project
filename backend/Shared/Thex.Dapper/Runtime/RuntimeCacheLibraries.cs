﻿using Microsoft.Extensions.DependencyModel;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Thex.Dapper.Runtime
{
    public static class RuntimeCacheLibraries
    {
        private static IReadOnlyCollection<Type> _allTypesCache;
        private static List<string> ListOfTypesOrNamespacesToExclude = new List<string>()
        {
            "microsoft",
            "xunit",
            "system",
            "newtonsoft",
            "runtime",
            "netstandard",
            "bogus",
            "moq",
            "nsubstitute",
            "shouldly",
            "jetbrains",
            "automapper",
            "castle",
            "remotion",
            "identitymodel",
            "identityserver",
            "serilog",
            "swashbuckle"
        };

        internal static IEnumerable<Type> TryGetTypesFromAssembly(Assembly assembly)
        {
            IEnumerable<Type> _types = Enumerable.Empty<Type>();

            try
            {
                bool ReturnValidType(Type type)
                {
                    if (ListOfTypesOrNamespacesToExclude.Any(a => type.AssemblyQualifiedName.ToLowerInvariant().Contains(a)))
                        return false;

                    return true;
                };

                _types = assembly.GetTypes().Where(ReturnValidType).ToArray();
            }
            catch (Exception ex)
            {
                Debug.WriteLine($"Tnf: Can't load types from assembly {assembly.FullName} in RuntimeAssemblyHelper");
                Debug.WriteLine(ex.GetBaseException().Message);
            }

            return _types;
        }

        internal static IEnumerable<Assembly> MergeAssemblies()
        {
            var assemblies = new List<Assembly>();

            void LoadAssemblyFromFile(string library)
            {
                try
                {
                    if (ListOfTypesOrNamespacesToExclude.Any(a => library.ToLowerInvariant().Contains(a)))
                        return;

                    if (!assemblies.Any(a => a.Location.Contains(library)))
                    {
                        var assembly = Assembly.LoadFile(library);
                        assemblies.Add(assembly);
                    }
                }
                catch (Exception ex)
                {
                    Debug.WriteLine($"Tnf: Can't load assembly from file {library} in RuntimeAssemblyHelper");
                    Debug.WriteLine(ex.GetBaseException().Message);
                }
            };

            void LoadAssemblyFromName(string assemblyName)
            {
                try
                {
                    if (ListOfTypesOrNamespacesToExclude.Any(a => assemblyName.ToLowerInvariant().Contains(a)))
                        return;

                    if (!assemblies.Any(a => a.FullName == assemblyName))
                    {
                        var assembly = Assembly.Load(new AssemblyName(assemblyName));
                        assemblies.Add(assembly);
                    }
                }
                catch (Exception ex)
                {
                    Debug.WriteLine($"Tnf: Can't load assembly from assembly name {assemblyName} in RuntimeAssemblyHelper");
                    Debug.WriteLine(ex.GetBaseException().Message);
                }
            }

#if NETSTANDARD2_0
            DependencyContext.Default.RuntimeLibraries
                    .ToList()
                    .ForEach(library => LoadAssemblyFromName(library.Name));
#endif

            Directory.GetFiles(AppContext.BaseDirectory)
                    .Where(file => Path.GetExtension(file).Equals(".dll", StringComparison.OrdinalIgnoreCase))
                    .ToList()
                    .ForEach(LoadAssemblyFromFile);

            return assemblies.Distinct().ToList();
        }

        /// <summary>
        /// Get all types from runtime libraries from current context. (Except: Microsoft, Xunit, System, NewtonSoft, Runtime, etc ...)
        /// </summary>
        public static IReadOnlyCollection<Type> GetAllTypes()
        {
            if (_allTypesCache != null)
                return _allTypesCache;

            _allTypesCache = MergeAssemblies()
                .SelectMany(TryGetTypesFromAssembly)
                .Distinct()
                .ToList()
                .AsReadOnly();

            return _allTypesCache;
        }

        public static IEnumerable<Type> GetDerivedTypes(this Type type)
        {
            return GetAllTypes().Where(w =>
                w != type &&
                type.IsAssignableFrom(w) &&
                !w.IsAbstract &&
                w.IsClass);
        }
    }

}
