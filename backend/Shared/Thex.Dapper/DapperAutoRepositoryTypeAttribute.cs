﻿using System;
using Tnf.Repositories;

namespace Tnf.Dapper
{
    public class DapperAutoRepositoryTypeAttribute : AutoRepositoryTypesAttribute
    {
        public DapperAutoRepositoryTypeAttribute(
            Type repositoryInterface,
            Type repositoryImplementation)
            : base(repositoryInterface, repositoryImplementation)
        {
        }
    }
}
