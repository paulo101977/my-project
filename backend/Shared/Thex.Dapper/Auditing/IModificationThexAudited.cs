﻿using System;
using System.Collections.Generic;
using System.Text;
using Tnf.Repositories.Entities.Auditing;

namespace Tnf.Repositories.Entities.Auditing
{
    public interface IModificationThexAudited: IHasModificationTime
    {
        Guid? LastModifierUserId { get; set; }
    }
}