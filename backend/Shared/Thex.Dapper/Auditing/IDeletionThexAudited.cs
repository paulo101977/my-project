﻿using System;
using System.Collections.Generic;
using System.Text;
using Tnf.Repositories.Entities;
using Tnf.Repositories.Entities.Auditing;

namespace Thex.Dapper.Auditing
{
    public interface IDeletionThexAudited : IHasDeletionTime, ISoftDelete
    {
        Guid? DeleterUserId { get; set; }
    }
}



