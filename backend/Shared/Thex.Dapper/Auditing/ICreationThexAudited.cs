﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tnf.Repositories.Entities.Auditing
{
    public interface ICreationThexAudited
    {
        Guid CreatorUserId { get; set; }
    }
}
