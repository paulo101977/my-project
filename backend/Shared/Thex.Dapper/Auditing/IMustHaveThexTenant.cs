﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tnf.Repositories.Entities.Auditing
{
    public interface IMustHaveThexTenant
    {
        Guid? TenantId { get; set; }
        int? PropertyId { get; set; }
        int? ChainId { get; set; }
    }
}
