﻿using DapperExtensions;
using DapperExtensions.Mapper;
using DapperExtensions.Sql;
using System;
using System.Diagnostics;
using System.Linq;
using Thex.Dapper.Runtime;
using Thex.GenericLog;
using Tnf.Dapper;
using Tnf.Dapper.Filters.Action;
using Tnf.Dapper.Filters.Action.Internal;
using Tnf.Dapper.Filters.Query;
using Tnf.Dapper.Filters.Query.Internal;
using Tnf.Dapper.Repositories;

// ReSharper disable once CheckNamespace
namespace Microsoft.Extensions.DependencyInjection
{
    public static class ServiceCollectionExtensions
    {
        /// <summary>
        /// Add Tnf Dapper infraestructure
        /// </summary>
        /// <param name="services">Service collection</param>
        /// <param name="options">Dapper options</param>
        public static IServiceCollection AddTnfDapper(this IServiceCollection services, Action<DapperOptions> options)
        {
            if (services.IsRegistered(typeof(IDapperRepository<>)))
                return services;

            // Dependencies
            //services.AddTnfRepository();

            services.AddTransient<IDapperActionFilterExecuter, DapperActionFilterExecuter>();
            services.AddTransient<CreationAuditDapperActionFilter>();
            services.AddTransient<ModificationAuditDapperActionFilter>();
            services.AddTransient<DeletionAuditDapperActionFilter>();

            services.AddTransient<IDapperQueryFilterExecuter, DapperQueryFilterExecuter>();
            services.AddTransient<IDapperQueryFilter, MayHaveTenantDapperQueryFilter>();
            services.AddTransient<IDapperQueryFilter, MustHaveTenantDapperQueryFilter>();
            services.AddTransient<IDapperQueryFilter, SoftDeleteDapperQueryFilter>();

            services.AddScoped<IGenericLogHandler, GenericLogHandler>();

            SearchForEntityFrameworkCoreSupport(services);
            SearchForDapperClassMappings(services);
            ConfigureDapperExtensions(options);

            return services;
        }

        private static void SearchForDapperClassMappings(IServiceCollection services)
        {
            Debug.WriteLine("[Tnf.Dapper]: Procurando por todas as classes concretas que herdam do tipo IClassMapper<> para registro do repositório genérico");

            var classMapperTypeRef = typeof(IClassMapper<>);

            var classMappersRefs = RuntimeCacheLibraries
                .GetAllTypes()
                .Where(w => w.IsAssignableToGenericType(classMapperTypeRef))
                .Where(w => !w.FullName.Contains(classMapperTypeRef.Namespace))
                .Where(w => !w.IsAbstract)
                .ToArray();

            Debug.WriteLine("[Tnf.Dapper]: Procurando por todas as classes concretas que herdam do tipo IClassMapper<> para registro do repositório genérico");

            foreach (var mapper in classMappersRefs)
            {
                var classMapper = Activator.CreateInstance(mapper) as IClassMapper;

                services.AddTnfRepository(classMapper.EntityType, DapperAutoRepositoryTypes.Default);

                classMapper = null;
            }
        }

        /// <summary>
        /// Checks whether <paramref name="givenType"/> implements/inherits <paramref name="genericType"/>.
        /// </summary>
        /// <param name="givenType">Type to check</param>
        /// <param name="genericType">Generic type</param>
        private static bool IsAssignableToGenericType(this Type givenType, Type genericType)
        {
            var givenTypeInfo = givenType;

            if (givenTypeInfo.IsGenericType && givenType.GetGenericTypeDefinition() == genericType)
                return true;

            if (givenType.GetInterfaces().Any(interfaceType => interfaceType.IsGenericType && interfaceType.GetGenericTypeDefinition() == genericType))
                return true;

            if (givenTypeInfo.BaseType == null)
                return false;

            return IsAssignableToGenericType(givenTypeInfo.BaseType, genericType);
        }

        private static void SearchForEntityFrameworkCoreSupport(IServiceCollection services)
        {
            Debug.WriteLine("[Tnf.Dapper]: Procurando suporte ao EntityFrameworkCore (Tnf.EntityFrameworkCore.TnfDbContext)");

            var dbContextTypeRef = RuntimeCacheLibraries
                .GetAllTypes()
                .FirstOrDefault(w => w.FullName == "Tnf.EntityFrameworkCore.TnfDbContext");

            if (dbContextTypeRef != null)
            {
                Debug.WriteLine("[Tnf.Dapper]: Existe suporte ao EntityFrameworkCore na aplicação");
                Debug.WriteLine("[Tnf.Dapper]: Para todo contexto será registrado um repositório genérico (IDapperRepository<TEntity>), para cada entidade encontrada (DbSet<TEntity>)");

                var dbSetTypeRef = dbContextTypeRef.BaseType.Assembly.GetType("Microsoft.EntityFrameworkCore.DbSet`1");

                var dbContextTypes = services
                    .Where(w => dbContextTypeRef.IsAssignableFrom(w.ServiceType))
                    .Select(s => s.ServiceType)
                    .ToArray();

                foreach (var dbContextType in dbContextTypes)
                    services.AddTnfRepository(dbContextType, dbSetTypeRef, DapperAutoRepositoryTypes.DefaultForEntityFrameworkCore);

                return;
            }

            Debug.WriteLine("[Tnf.Dapper]: Não existe suporte ao EntityFrameworkCore na aplicação");
        }

        private static void ConfigureDapperExtensions(Action<DapperOptions> options)
        {
            var internalOptions = new DapperOptions();

            options(internalOptions);

            ISqlDialect sqlDialect = null;

            switch (internalOptions.DbType)
            {
                case DapperDbType.Sqlite:
                    sqlDialect = new SqliteDialect();
                    break;
                case DapperDbType.Oracle:
                    sqlDialect = new OracleDialect();
                    break;
                default:
                    sqlDialect = new SqlServerDialect();
                    break;
            }

            Debug.WriteLine($"[Tnf.Dapper]: Configurado suporte a {internalOptions.DbType}");

            var dapperExtensionsConfiguration = new DapperExtensionsConfiguration(
                typeof(AutoClassMapper<>),
                internalOptions.MapperAssemblies,
                sqlDialect);

            DapperExtensions.DapperExtensions.Configure(dapperExtensionsConfiguration);
        }
    }
}