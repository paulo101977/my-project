﻿using DapperExtensions;
using Tnf.Repositories.Entities;
using Tnf.Repositories.Uow;
using Tnf.Runtime.MultiTenancy;
using Tnf.Runtime.Session;

namespace Tnf.Dapper.Filters.Query.Internal
{
    internal class MustHaveTenantDapperQueryFilter : DapperQueryFilterBase<IMustHaveTenant>
    {
        public MustHaveTenantDapperQueryFilter(
            ICurrentUnitOfWorkProvider currentUnitOfWorkProvider, 
            IUnitOfWorkDefaultOptions unitOfWorkDefaultOptions,
            ITnfSession tnfSession)
            : base(currentUnitOfWorkProvider, unitOfWorkDefaultOptions, tnfSession)
        {
        }

        public override TnfDataFilter FilterName { get; } = TnfDataFilter.MustHaveTenant;

        private int TenantId
        {
            get
            {
                var tenantId = CurrentUnitOfWorkProvider.Current.GetTenantId();
                if (tenantId.HasValue)
                    return tenantId.Value;

                if (TnfSession.TenantId.HasValue)
                    return TnfSession.TenantId.Value;

                return MultiTenancyConsts.DefaultTenantId;
            }
        }

        protected override object FilterObject => TenantId;

        protected override string FilterObjectName { get; } = nameof(IMustHaveTenant.TenantId);

        public override IFieldPredicate ExecuteFilter<TEntity>()
        {
            IFieldPredicate predicate = null;

            if (IsFilterable<TEntity>())
                predicate = Predicates.Field<TEntity>(f => (f as IMustHaveTenant).TenantId, Operator.Eq, FilterObject);

            return predicate;
        }
    }
}
