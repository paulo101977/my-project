using DapperExtensions;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Tnf.Dapper.Internal;

namespace Tnf.Dapper.Filters.Query.Internal
{
    internal class DapperQueryFilterExecuter : IDapperQueryFilterExecuter
    {
        private IEnumerable<IDapperQueryFilter> _queryFilters;

        public DapperQueryFilterExecuter(IServiceProvider serviceProvider)
        {
            _queryFilters = serviceProvider.GetServices<IDapperQueryFilter>();
        }

        public IPredicate ExecuteFilter<TEntity>(Expression<Func<TEntity, bool>> predicate)
            where TEntity : class
        {
            ICollection<IDapperQueryFilter> filters = _queryFilters.ToList();

            foreach (IDapperQueryFilter filter in filters)
            {
                predicate = filter.ExecuteFilter(predicate);
            }

            IPredicate pg = predicate.ToPredicateGroup();
            return pg;
        }

        public PredicateGroup ExecuteFilter<TEntity>()
            where TEntity : class
        {
            ICollection<IDapperQueryFilter> filters = _queryFilters.ToList();
            var groups = new PredicateGroup
            {
                Operator = GroupOperator.And,
                Predicates = new List<IPredicate>()
            };

            foreach (IDapperQueryFilter filter in filters)
            {
                IFieldPredicate predicate = filter.ExecuteFilter<TEntity>();
                if (predicate != null)
                {
                    groups.Predicates.Add(predicate);
                }
            }

            return groups;
        }

        #region IDisposable Support

        private bool disposedValue = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // dispose managed state (managed objects).
                }

                // free unmanaged resources (unmanaged objects) and override a finalizer below.
                // set large fields to null.

                _queryFilters = null;

                disposedValue = true;
            }
        }

        ~DapperQueryFilterExecuter() => Dispose(false);

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        #endregion
    }
}
