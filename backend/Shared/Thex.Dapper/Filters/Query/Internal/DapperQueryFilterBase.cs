﻿using DapperExtensions;
using System;
using System.Linq.Expressions;
using System.Reflection;
using Tnf.Repositories.Uow;
using Tnf.Runtime.Session;

namespace Tnf.Dapper.Filters.Query.Internal
{
    internal abstract class DapperQueryFilterBase<TFilter> : IDapperQueryFilter
        where TFilter : class
    {
        protected readonly ICurrentUnitOfWorkProvider CurrentUnitOfWorkProvider;
        protected readonly IUnitOfWorkDefaultOptions UnitOfWorkDefaultOptions;
        protected readonly ITnfSession TnfSession;

        protected DapperQueryFilterBase(
            ICurrentUnitOfWorkProvider currentUnitOfWorkProvider, 
            IUnitOfWorkDefaultOptions unitOfWorkDefaultOptions, 
            ITnfSession tnfSession)
        {
            CurrentUnitOfWorkProvider = currentUnitOfWorkProvider;
            UnitOfWorkDefaultOptions = unitOfWorkDefaultOptions;
            TnfSession = tnfSession;
        }

        public abstract TnfDataFilter FilterName { get; }

        public virtual bool IsEnabled
        {
            get
            {
                if (CurrentUnitOfWorkProvider.Current != null)
                    return CurrentUnitOfWorkProvider.Current.IsFilterEnabled(FilterName);

                return UnitOfWorkDefaultOptions.IsFilterEnabled(FilterName);
            }
        }

        protected abstract object FilterObject { get; }

        protected abstract string FilterObjectName { get; }

        protected virtual bool IsFilterable<TEntity>() where TEntity : class
            => typeof(TFilter).IsAssignableFrom(typeof(TEntity)) && IsEnabled;

        public abstract IFieldPredicate ExecuteFilter<TEntity>()
            where TEntity : class;

        public Expression<Func<TEntity, bool>> ExecuteFilter<TEntity>(Expression<Func<TEntity, bool>> predicate)
            where TEntity : class
        {
            if (IsFilterable<TEntity>())
            {
                Type type = typeof(TEntity);
                PropertyInfo propType = type.GetProperty(FilterObjectName);
                ParameterExpression param = predicate == null ? Expression.Parameter(type, type.Name) : predicate.Parameters[0];
                MemberExpression memberExp = Expression.Property(param, FilterObjectName);
                BinaryExpression body = Expression.Equal(memberExp, Expression.Constant(FilterObject, propType.PropertyType));

                if (predicate != null)
                    body = Expression.AndAlso(predicate.Body, body);

                predicate = Expression.Lambda<Func<TEntity, bool>>(body, param);
            }

            return predicate;
        }
    }
}
