﻿using DapperExtensions;
using Tnf.Repositories.Entities;
using Tnf.Repositories.Uow;
using Tnf.Runtime.Session;

namespace Tnf.Dapper.Filters.Query.Internal
{
    internal class MayHaveTenantDapperQueryFilter : DapperQueryFilterBase<IMayHaveTenant>
    {
        public MayHaveTenantDapperQueryFilter(
            ICurrentUnitOfWorkProvider currentUnitOfWorkProvider,
            IUnitOfWorkDefaultOptions unitOfWorkDefaultOptions,
            ITnfSession tnfSession)
            : base(currentUnitOfWorkProvider, unitOfWorkDefaultOptions, tnfSession)
        {
        }

        public override TnfDataFilter FilterName { get; } = TnfDataFilter.MayHaveTenant;

        private int? TenantId
        {
            get
            {
                var tenantId = CurrentUnitOfWorkProvider.Current.GetTenantId();
                if (tenantId.HasValue)
                    return tenantId.Value;

                return TnfSession.TenantId;
            }
        }

        protected override object FilterObject => TenantId;

        protected override string FilterObjectName { get; } = nameof(IMayHaveTenant.TenantId);

        public override IFieldPredicate ExecuteFilter<TEntity>()
        {
            IFieldPredicate predicate = null;

            if (IsFilterable<TEntity>())
                predicate = Predicates.Field<TEntity>(f => (f as IMayHaveTenant).TenantId, Operator.Eq, FilterObject);

            return predicate;
        }
    }
}
