﻿using DapperExtensions;
using Tnf.Repositories.Entities;
using Tnf.Repositories.Uow;
using Tnf.Runtime.Session;

namespace Tnf.Dapper.Filters.Query.Internal
{
    internal class SoftDeleteDapperQueryFilter : DapperQueryFilterBase<ISoftDelete>
    {
        public SoftDeleteDapperQueryFilter(
            ICurrentUnitOfWorkProvider currentUnitOfWorkProvider,
            IUnitOfWorkDefaultOptions unitOfWorkDefaultOptions,
            ITnfSession tnfSession)
            : base(currentUnitOfWorkProvider, unitOfWorkDefaultOptions, tnfSession)
        {
        }

        public override TnfDataFilter FilterName { get; } = TnfDataFilter.SoftDelete;

        public bool IsDeleted => false;

        protected override object FilterObject => IsDeleted;

        protected override string FilterObjectName { get; } = nameof(ISoftDelete.IsDeleted);

        public override IFieldPredicate ExecuteFilter<TEntity>()
        {
            IFieldPredicate predicate = null;

            if (IsFilterable<TEntity>())
                predicate = Predicates.Field<TEntity>(f => (f as ISoftDelete).IsDeleted, Operator.Eq, FilterObject);

            return predicate;
        }
    }
}
