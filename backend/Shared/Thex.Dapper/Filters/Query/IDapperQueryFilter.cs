﻿using DapperExtensions;
using System;
using System.Linq.Expressions;
using Tnf.Repositories.Uow;

namespace Tnf.Dapper.Filters.Query
{
    public interface IDapperQueryFilter
    {
        TnfDataFilter FilterName { get; }

        bool IsEnabled { get; }

        IFieldPredicate ExecuteFilter<TEntity>()
            where TEntity : class;

        Expression<Func<TEntity, bool>> ExecuteFilter<TEntity>(Expression<Func<TEntity, bool>> predicate)
            where TEntity : class;
    }
}
