﻿using DapperExtensions;
using System;
using System.Linq.Expressions;

namespace Tnf.Dapper.Filters.Query
{
    public interface IDapperQueryFilterExecuter : IDisposable
    {
        IPredicate ExecuteFilter<TEntity>(Expression<Func<TEntity, bool>> predicate)
            where TEntity : class;

        PredicateGroup ExecuteFilter<TEntity>() 
            where TEntity : class;
    }
}
