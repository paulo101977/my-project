﻿using System;

namespace Tnf.Dapper.Filters.Action
{
    public interface IDapperActionFilter : IDisposable
    {
        void ExecuteFilter<TEntity>(TEntity entity)
            where TEntity : class;
    }
}
