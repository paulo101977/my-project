﻿using DapperExtensions;
using System;
using Thex.Kernel;
using Thex.Dapper.Auditing;
using Tnf.Repositories.Entities;
using Tnf.Repositories.Entities.Auditing;
using Tnf.Repositories.Uow;
using Tnf.Runtime.Session;
using Tnf.Timing;

namespace Tnf.Dapper.Filters.Action.Internal
{
    internal class DeletionAuditDapperActionFilter : DapperActionFilterBase, IDapperActionFilter
    {
        public IApplicationUser _applicationUser { get; set; }

        public DeletionAuditDapperActionFilter(
            ITnfSession tnfSession,
            ICurrentUnitOfWorkProvider currentUnitOfWorkProvider,
            IApplicationUser applicationUser)
            : base(tnfSession, currentUnitOfWorkProvider)
        {
            _applicationUser = applicationUser;
        }

        public void ExecuteFilter<TEntity>(TEntity entity)
            where TEntity : class
        {
            CheckAndSetMustHaveTenantIdProperty(entity);

            if (entity is ISoftDelete entitySoftDelete)
                entitySoftDelete.IsDeleted = true;

            if (entity is IHasDeletionTime entityDeletionTime && entityDeletionTime.DeletionTime == null)
                entityDeletionTime.DeletionTime = Clock.Now;

            if (entity is IDeletionAudited entityDeletionAudited && entityDeletionAudited.DeleterUserId == null)
                entityDeletionAudited.DeleterUserId = GetUserId(entity);

            if (entity is IDeletionThexAudited entityDeletionThexAudited && entityDeletionThexAudited.DeleterUserId == null)
                entityDeletionThexAudited.DeleterUserId = _applicationUser.UserUid;
        }

        protected virtual void CheckAndSetMustHaveTenantIdProperty(object entityAsObj)
        {
            if (entityAsObj is IMustHaveTenant entity && entity.TenantId == 0)
            {
                int? currentTenantId = GetCurrentTenantIdOrNull();

                if (currentTenantId != null)
                    entity.TenantId = currentTenantId.Value;
                else
                    throw new TnfException("Can not set TenantId to 0 for IMustHaveTenant entities!");
            }

            if (entityAsObj is IMustHaveThexTenant entityThex)
            {
                if (entityThex.TenantId == null || entityThex.TenantId == Guid.Empty)
                    entityThex.TenantId = _applicationUser.TenantId;

                if (entityThex.PropertyId == null)
                    entityThex.PropertyId = _applicationUser.PropertyId.TryParseToInt32();

                if (entityThex.ChainId == null)
                    entityThex.ChainId = _applicationUser.ChainId.TryParseToInt32();
            }
        }


        #region IDisposable Support

        private bool disposedValue = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // dispose managed state (managed objects).
                }

                // free unmanaged resources (unmanaged objects) and override a finalizer below.
                // set large fields to null.

                disposedValue = true;
            }
        }

        ~DeletionAuditDapperActionFilter() => Dispose(false);

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        #endregion
    }
}
