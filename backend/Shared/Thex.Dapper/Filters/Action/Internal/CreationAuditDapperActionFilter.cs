﻿using System;
using Thex.Common.Extensions;
using Thex.Kernel;
using Tnf.Repositories.Entities;
using Tnf.Repositories.Entities.Auditing;
using Tnf.Repositories.Uow;
using Tnf.Runtime.MultiTenancy;
using Tnf.Runtime.Session;
using Tnf.Timing;

namespace Tnf.Dapper.Filters.Action.Internal
{
    internal class CreationAuditDapperActionFilter : DapperActionFilterBase, IDapperActionFilter
    {
        private readonly IMultiTenancyConfig _multiTenancyConfig;
        private readonly IApplicationUser _applicationUser;

        public CreationAuditDapperActionFilter(
           IMultiTenancyConfig multiTenancyConfig,
           ITnfSession tnfSession,
           ICurrentUnitOfWorkProvider currentUnitOfWorkProvider,
           IApplicationUser applicationUser)
           : base(tnfSession, currentUnitOfWorkProvider)
        {
            _multiTenancyConfig = multiTenancyConfig;
            _applicationUser = applicationUser;
        }

        public void ExecuteFilter<TEntity>(TEntity entity)
            where TEntity : class
        {
            if (entity is IHasCreationTime entityWithCreationTime && entityWithCreationTime.CreationTime == default(DateTime))
                entityWithCreationTime.CreationTime = Clock.Now;

            CheckAndSetMustHaveTenantIdProperty(entity);
            CheckAndSetMayHaveTenantIdProperty(entity);

            if (entity is ICreationAudited entityCreationAudited && entityCreationAudited.CreatorUserId == null)
                entityCreationAudited.CreatorUserId = GetUserId(entity);

            if (entity is ICreationThexAudited entityCreationThexAudited && entityCreationThexAudited.CreatorUserId == Guid.Empty)
                entityCreationThexAudited.CreatorUserId = _applicationUser.UserUid;

            if (entity is IHasModificationTime entityModificationTime)
                entityModificationTime.LastModificationTime = null;

            if (entity is IModificationAudited entityModificationAudited)
                entityModificationAudited.LastModifierUserId = null;

            if (entity is IModificationThexAudited entityModificationThexAudited)
                entityModificationThexAudited.LastModifierUserId = null;
        }

        protected virtual void CheckAndSetMustHaveTenantIdProperty(object entityAsObj)
        {
            if (entityAsObj is IMustHaveTenant entity && entity.TenantId == 0)
            {
                int? currentTenantId = GetCurrentTenantIdOrNull();

                if (currentTenantId != null)
                    entity.TenantId = currentTenantId.Value;
                else
                    throw new TnfException("Can not set TenantId to 0 for IMustHaveTenant entities!");
            }

            if (entityAsObj is IMustHaveThexTenant entityThex)
            {
                if (entityThex.TenantId == null || entityThex.TenantId == Guid.Empty)
                    entityThex.TenantId = _applicationUser.TenantId;

                if (entityThex.PropertyId == null)
                    entityThex.PropertyId = _applicationUser.PropertyId.TryParseToInt32();

                if (entityThex.ChainId == null)
                    entityThex.ChainId = _applicationUser.ChainId.TryParseToInt32();
            }
        }

        protected virtual void CheckAndSetMayHaveTenantIdProperty(object entityAsObj)
        {
            if (entityAsObj is IMayHaveTenant entity && entity.TenantId == null && _multiTenancyConfig.IsEnabled)
                entity.TenantId = GetCurrentTenantIdOrNull();
        }

        #region IDisposable Support

        private bool disposedValue = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // dispose managed state (managed objects).
                }

                // free unmanaged resources (unmanaged objects) and override a finalizer below.
                // set large fields to null.

                disposedValue = true;
            }
        }

        ~CreationAuditDapperActionFilter() => Dispose(false);

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        #endregion
    }
}
