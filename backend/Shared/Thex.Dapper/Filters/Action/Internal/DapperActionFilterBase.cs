﻿using System;
using Tnf.Repositories.Entities;
using Tnf.Repositories.Uow;
using Tnf.Runtime.Session;

namespace Tnf.Dapper.Filters.Action.Internal
{
    internal abstract class DapperActionFilterBase
    {
        protected DapperActionFilterBase(
            ITnfSession tnfSession,
            ICurrentUnitOfWorkProvider currentUnitOfWorkProvider)
        {
            TnfSession = tnfSession;
            CurrentUnitOfWorkProvider = currentUnitOfWorkProvider;
        }

        public ITnfSession TnfSession { get; set; }

        public ICurrentUnitOfWorkProvider CurrentUnitOfWorkProvider { get; set; }

        protected virtual long? GetUserId<TEntity>(TEntity entity)
            where TEntity : class
        {
            long? userId = GetAuditUserId();

            if (userId == null)
                return null;

            // Special check for multi-tenant entities
            if (entity is IMayHaveTenant entityMayHaveTenant)
            {
                // Sets LastModifierUserId only if current user is in same tenant/host with the given entity
                if (entityMayHaveTenant.TenantId == TnfSession.TenantId)
                    return userId;

                return null;
            }

            // Special check for multi-tenant entities
            if (entity is IMustHaveTenant entityMustHaveTenant)
            {
                // Sets LastModifierUserId only if current user is in same tenant/host with the given entity
                if (entityMustHaveTenant.TenantId == TnfSession.TenantId)
                    return userId;

                return null;
            }

            return userId;
        }

        protected virtual long? GetAuditUserId()
            => TnfSession.UserId;

        protected virtual int? GetCurrentTenantIdOrNull()
        {
            if (CurrentUnitOfWorkProvider?.Current != null)
                return CurrentUnitOfWorkProvider.Current.GetTenantId();

            return TnfSession.TenantId;
        }
    }
}
