﻿using Microsoft.Extensions.DependencyInjection;
using System;

namespace Tnf.Dapper.Filters.Action.Internal
{
    internal class DapperActionFilterExecuter : IDapperActionFilterExecuter
    {
        private readonly IServiceProvider _serviceProvider;

        public DapperActionFilterExecuter(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public void ExecuteCreationAuditFilter<TEntity>(TEntity entity)
            where TEntity : class
        {
            _serviceProvider.GetRequiredService<CreationAuditDapperActionFilter>().ExecuteFilter(entity);
        }

        public void ExecuteModificationAuditFilter<TEntity>(TEntity entity)
            where TEntity : class
        {
            _serviceProvider.GetRequiredService<ModificationAuditDapperActionFilter>().ExecuteFilter(entity);
        }

        public void ExecuteDeletionAuditFilter<TEntity>(TEntity entity)
            where TEntity : class
        {
            _serviceProvider.GetRequiredService<DeletionAuditDapperActionFilter>().ExecuteFilter(entity);
        }

        #region IDisposable Support

        private bool disposedValue = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // dispose managed state (managed objects).
                }

                // free unmanaged resources (unmanaged objects) and override a finalizer below.
                // set large fields to null.

                disposedValue = true;
            }
        }

        ~DapperActionFilterExecuter() => Dispose(false);

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        #endregion
    }
}
