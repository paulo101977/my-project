﻿using System;

namespace Tnf.Dapper.Filters.Action
{
    public interface IDapperActionFilterExecuter : IDisposable
    {
        void ExecuteCreationAuditFilter<TEntity>(TEntity entity)
            where TEntity : class;

        void ExecuteModificationAuditFilter<TEntity>(TEntity entity)
            where TEntity : class;

        void ExecuteDeletionAuditFilter<TEntity>(TEntity entity)
            where TEntity : class;
    }
}
