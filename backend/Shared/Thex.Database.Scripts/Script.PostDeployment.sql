﻿/*
Post-Deployment Script Template
------------------------------------------------------------------------------------
 This file contains SQL statements that will be appended to the build script.
 Use SQLCMD syntax to include a file in the post-deployment script.
 Example:      :r .\myfile.sql
 Use SQLCMD syntax to reference a variable in the post-deployment script.
 Example:      :setvar TableName MyTable
               SELECT * FROM [$(TableName)]
------------------------------------------------------------------------------------
*/
:r .\Alters\AlterBillingAccountItem.sql
:r .\Alters\AlterPropertyCompanyClientCategory.sql
:r .\Alters\AlterReservationItem.sql
:r .\Inserts\DocumentType.sql
:r .\Inserts\HousekeepingRoomInspection.sql
:r .\Inserts\IntegrationPartner.sql
:r .\Inserts\ReasonCategory.sql
:r .\Inserts\Reason.sql
:r .\Inserts\StatusCategory.sql
:r .\Inserts\Status.sql
:r .\Inserts\PropertyBaseRateType.sql
--:r .\Inserts\ReservationItem.sql
:r .\Alters\AlterTenant.sql
:r .\Inserts\ProductCategory.sql
:r .\Inserts\UnitMeasurement.sql
:r .\Alters\AlterUserClaims.sql
:r .\Alters\AlterUserLogins.sql
:r .\Alters\AlterUserTokens.sql
:r .\Alters\AlterUserRoles.sql
:r .\Inserts\PropertyContract.sql
:r .\Alters\AlterUserInvitation.sql
:r .\Alters\AlterPlasticBrandProperty.sql
:r .\Alters\AlterProductCategory.sql
:r .\Alters\AlterRoom.sql
:r .\Alters\AlterPlasticBrand.sql
:r .\Alters\AlterBillingItemCategory.sql
:r .\Inserts\Partner.sql
:r .\Inserts\BillingInvoiceType.sql
:r .\Updates\BillingInvoiceType.sql
:r .\Updates\PropertyBaseRate.sql
:r .\Alters\AlterPerson.sql
:r .\Alters\AlterBillingInvoice.sql

:r .\Alters\AlterBillingInvoiceModel.sql
:r .\Inserts\BillingInvoiceModel.sql

:r .\Alters\AlterBillingInvoiceProperty.sql

:r .\Alters\AlterPropertyPremisse.sql
:r .\Alters\AlterPropertyHeaderPremisse.sql
:r .\Alters\AlterIntegrationPartnerProperty.sql

:r .\Alters\AlterDocumentType.sql
:r .\Inserts\Parameters.sql
:r .\Inserts\CountrySubdivisionBCB.sql
:r .\Alters\AlterReservationBudget.sql
:r .\Alters\AlterReservation.sql
:r .\Inserts\BillingAccountItemType.sql

:r .\Alters\AlterPropertyGuestType.sql
:r .\Alters\AlterTourismTax.sql

:r .\Alters\AlterTourismTax.sql
:r .\Inserts\Currency.sql

:r .\Updates\ProductCategory.sql
:r .\Inserts\ApplicationParameter.sql
:r .\Inserts\PropertyParameter.sql
:r .\Alters\AlterRoomOccupied.sql

:r .\Updates\PropertyParameter.sql
:r .\Inserts\PolicyType.sql
:r .\Updates\LevelRateHeader.sql
:r .\Alters\AlterLevelRateHeader.sql
:r .\Alters\AlterReservationConfirmation.sql
:r .\Updates\CountrySubdivision.sql


:r .\Updates\ExternalFeatures.sql

:r .\Identity\UpdateMenu.sql
:r .\Alters\AlterBillingInvoicePropertySupportedType.sql