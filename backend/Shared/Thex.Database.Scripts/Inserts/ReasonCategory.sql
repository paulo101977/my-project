﻿IF NOT EXISTS (SELECT * FROM ReasonCategory WHERE ReasonCategoryId = 12)
BEGIN
	INSERT INTO [dbo].[ReasonCategory] ([ReasonCategoryId], [ReasonType]) VALUES (12, 'Housekeeping Review');
END

IF NOT EXISTS (SELECT * FROM ReasonCategory WHERE ReasonCategoryId = 13)
BEGIN
	INSERT INTO [dbo].[ReasonCategory] ([ReasonCategoryId], [ReasonType]) VALUES (13, 'Housekeeping Stop');
END

IF NOT EXISTS (SELECT * FROM ReasonCategory WHERE ReasonCategoryId = 14)
BEGIN
	INSERT INTO [dbo].[ReasonCategory] ([ReasonCategoryId], [ReasonType]) VALUES (14, 'Credit Note Issue');
END
GO



