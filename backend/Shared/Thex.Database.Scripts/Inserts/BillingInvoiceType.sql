﻿IF NOT EXISTS (SELECT * FROM BillingInvoiceType WHERE BillingInvoiceTypeId = 1)
BEGIN
	INSERT INTO BillingInvoiceType VALUES (1, 'NFS-e')
END

IF NOT EXISTS (SELECT * FROM BillingInvoiceType WHERE BillingInvoiceTypeId = 2)
BEGIN
	INSERT INTO BillingInvoiceType VALUES (2, 'NFC-e')
END

IF NOT EXISTS (SELECT * FROM BillingInvoiceType WHERE BillingInvoiceTypeId = 3)
BEGIN
	INSERT INTO BillingInvoiceType VALUES (3, 'NF-e')
END

IF NOT EXISTS (SELECT * FROM BillingInvoiceType WHERE BillingInvoiceTypeId = 4)
BEGIN
	INSERT INTO BillingInvoiceType VALUES (4, 'Factura')
END

IF NOT EXISTS (SELECT * FROM BillingInvoiceType WHERE BillingInvoiceTypeId = 5)
BEGIN
	INSERT INTO BillingInvoiceType VALUES (5, 'Nota de Crédito Total')
END

IF NOT EXISTS (SELECT * FROM BillingInvoiceType WHERE BillingInvoiceTypeId = 6)
BEGIN
	INSERT INTO BillingInvoiceType VALUES (6, 'Nota de Débito')
END

IF NOT EXISTS (SELECT * FROM BillingInvoiceType WHERE BillingInvoiceTypeId = 7)
BEGIN
	INSERT INTO BillingInvoiceType VALUES (7, 'Nota de Crédito Parcial')
END

IF NOT EXISTS (SELECT * FROM BillingInvoiceType WHERE BillingInvoiceTypeId = 8)
BEGIN
	INSERT INTO BillingInvoiceType VALUES (8, 'SAT-CFe')
END


GO