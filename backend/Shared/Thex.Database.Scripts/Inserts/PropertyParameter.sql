﻿INSERT INTO PropertyParameter
(PropertyParameterId, PropertyId, ApplicationParameterId, PropertyParameterValue, PropertyParameterMinValue, PropertyParameterMaxValue, PropertyParameterPossibleValues, IsActive, TenantId, IsDeleted)
SELECT NEWID(), property.PropertyId, 15, 'false', NULL, 'false', '[{"value":"true","title":"Sim"},{"value":"false","title":"Não"}]', 1, property.TenantId, 0
FROM Property LEFT JOIN PropertyParameter
ON (PropertyParameter.PropertyId = Property.PropertyId AND PropertyParameter.ApplicationParameterId = 15)
WHERE PropertyParameter.PropertyId is null
GO