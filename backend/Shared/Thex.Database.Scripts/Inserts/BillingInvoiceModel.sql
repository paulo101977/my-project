﻿IF NOT EXISTS (SELECT * FROM BillingInvoiceModel WHERE BillingInvoiceModelId = 1)
BEGIN
	INSERT [dbo].[BillingInvoiceModel] ([BillingInvoiceModelId], [Description], [TaxModel], [IsVisible], [BillingInvoiceTypeId]) VALUES (1, N'NFS-e', N'NFS-e', 1, 1)
END

IF NOT EXISTS (SELECT * FROM BillingInvoiceModel WHERE BillingInvoiceModelId = 2)
BEGIN
	INSERT [dbo].[BillingInvoiceModel] ([BillingInvoiceModelId], [Description], [TaxModel], [IsVisible], [BillingInvoiceTypeId]) VALUES (2, N'NFC-e', N'NFC-e', 1, 2)
END

IF NOT EXISTS (SELECT * FROM BillingInvoiceModel WHERE BillingInvoiceModelId = 3)
BEGIN
	INSERT [dbo].[BillingInvoiceModel] ([BillingInvoiceModelId], [Description], [TaxModel], [IsVisible], [BillingInvoiceTypeId]) VALUES (3, N'NF-e', N'NF-e', 1, 3)
END

IF NOT EXISTS (SELECT * FROM BillingInvoiceModel WHERE BillingInvoiceModelId = 4)
BEGIN
	INSERT [dbo].[BillingInvoiceModel] ([BillingInvoiceModelId], [Description], [TaxModel], [IsVisible], [BillingInvoiceTypeId]) VALUES (4, N'Factura', N'Factura', 1, 4)
END

IF NOT EXISTS (SELECT * FROM BillingInvoiceModel WHERE BillingInvoiceModelId = 5)
BEGIN
	INSERT [dbo].[BillingInvoiceModel] ([BillingInvoiceModelId], [Description], [TaxModel], [IsVisible], [BillingInvoiceTypeId]) VALUES (5, N'Nota de Crédito', N'Nota de Crédito', 1, 5)
END

IF NOT EXISTS (SELECT * FROM BillingInvoiceModel WHERE BillingInvoiceModelId = 6)
BEGIN
	INSERT [dbo].[BillingInvoiceModel] ([BillingInvoiceModelId], [Description], [TaxModel], [IsVisible], [BillingInvoiceTypeId]) VALUES (6, N'Nota de Débito', N'Nota de Débito', 0, 6)
END

IF NOT EXISTS (SELECT * FROM BillingInvoiceModel WHERE BillingInvoiceModelId = 7)
BEGIN
	INSERT [dbo].[BillingInvoiceModel] ([BillingInvoiceModelId], [Description], [TaxModel], [IsVisible], [BillingInvoiceTypeId]) VALUES (7, N'Nota de Crédito Parcial', N'Nota de Crédito Parcial', 0, 5)
END

IF NOT EXISTS (SELECT * FROM BillingInvoiceModel WHERE BillingInvoiceModelId = 8)
BEGIN
	INSERT [dbo].[BillingInvoiceModel] ([BillingInvoiceModelId], [Description], [TaxModel], [IsVisible], [BillingInvoiceTypeId]) VALUES (8, N'SAT-CFe', N'SAT-CFe', 1, 8)
END

GO