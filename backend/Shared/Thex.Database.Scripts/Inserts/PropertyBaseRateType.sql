﻿IF NOT EXISTS(SELECT * FROM PropertyBaseRateType WHERE PropertyBaseRateTypeId = 1)
BEGIN 
	INSERT INTO [PropertyBaseRateType] ([PropertyBaseRateTypeId], [Name]) VALUES (1, 'Amount');
END

IF NOT EXISTS(SELECT * FROM PropertyBaseRateType WHERE PropertyBaseRateTypeId = 2)
BEGIN 
	INSERT INTO [PropertyBaseRateType] ([PropertyBaseRateTypeId], [Name]) VALUES (2, 'Variation');
END

IF NOT EXISTS(SELECT * FROM PropertyBaseRateType WHERE PropertyBaseRateTypeId = 3)
BEGIN 
	INSERT INTO [PropertyBaseRateType] ([PropertyBaseRateTypeId], [Name]) VALUES (3, 'Premise');
END

IF NOT EXISTS(SELECT * FROM PropertyBaseRateType WHERE PropertyBaseRateTypeId = 4)
BEGIN 
	INSERT INTO [PropertyBaseRateType] ([PropertyBaseRateTypeId], [Name]) VALUES (4, 'Level');
END