﻿IF NOT EXISTS (SELECT * FROM ApplicationParameter WHERE ApplicationParameterId = 15)
BEGIN
	INSERT INTO [dbo].[ApplicationParameter]
			([ApplicationParameterId], [ApplicationModuleId], [FeatureGroupId], [ParameterName], [ParameterDescription], [ParameterDefaultValue], [ParameterTypeId], [ParameterComponentType], [ParameterMinValue], [ParameterMaxValue], [ParameterPossibleValues], [IsNullable], [IsDeleted], [CreationTime], [CreatorUserId], [LastModificationTime], [LastModifierUserId], [DeletionTime], [DeleterUserId])
	VALUES 
			(15, 1, 1, 'Permitir edição de reservas migradas', 'Permitir edição de reservas migradas', 'Não', 2, 'Configuração do Hotel', NULL, 'Sim', '[{"value":"Sim","title":"Sim"},{"value":"Não","title":"Não"}]', 0, 0, GETDATE(), NULL, GETDATE(), NULL, NULL, NULL)
END
