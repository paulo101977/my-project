﻿IF NOT EXISTS (SELECT * FROM DocumentType WHERE DocumentTypeId = 17)
BEGIN
	INSERT INTO DocumentType VALUES (17, 'N', 5630, 'NIF', null, null, 1, null)
END

IF NOT EXISTS (SELECT * FROM DocumentType WHERE DocumentTypeId = 18)
BEGIN
	INSERT INTO DocumentType VALUES (18, 'N', 5630, 'SSN', null, null, 1, null)
END

IF NOT EXISTS (SELECT * FROM DocumentType WHERE DocumentTypeId = 19)
BEGIN
	INSERT INTO DocumentType VALUES (19, 'L', 5630, 'ENI', null, null, 1, null)
END

IF NOT EXISTS (SELECT * FROM DocumentType WHERE DocumentTypeId = 20)
BEGIN
	INSERT INTO DocumentType VALUES (20, 'N', 5593, 'Cartão de Cidadão', null, null, 1, null)
END

IF NOT EXISTS (SELECT * FROM DocumentType WHERE DocumentTypeId = 21)
BEGIN
	INSERT INTO DocumentType VALUES (21, 'L', NULL, 'NIF', null, null, 1, null)
END

IF NOT EXISTS (SELECT * FROM DocumentType WHERE DocumentTypeId = 22)
BEGIN
	INSERT INTO DocumentType VALUES (22, 'N', 5593, 'C. Condução', null, null, 1, null)
END

IF NOT EXISTS (SELECT * FROM DocumentType WHERE DocumentTypeId = 23)
BEGIN
	INSERT INTO DocumentType VALUES (23, 'N', 5593, 'A. de Residência', null, null, 1, null)
END

GO