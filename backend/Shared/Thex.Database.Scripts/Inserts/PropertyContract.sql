﻿INSERT INTO propertycontract
(PropertyContractId, PropertyId, MaxUh, IsDeleted, CreationTime, CreatorUserId)
SELECT NEWID(), property.PropertyId, 100, 0, getdate(),
'ABEE3FAE-FF86-4EB3-A333-3E757DD01F53'
FROM property left join
propertyContract on propertycontract.PropertyId = property.PropertyId 
WHERE propertyContract.PropertyId is null
GO