﻿IF NOT EXISTS (SELECT * FROM BillingAccountItemType WHERE BillingAccountItemTypeId = 6)
BEGIN
	INSERT INTO [dbo].[BillingAccountItemType] ([BillingAccountItemTypeId] ,[BillingAccountItemTypeName]) VALUES (6 ,'Credit Note');
END

GO