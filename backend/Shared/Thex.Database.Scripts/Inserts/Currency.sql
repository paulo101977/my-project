﻿IF NOT EXISTS (SELECT * FROM Currency WHERE CurrencyId = 'C5EF2CFA-A05E-44ED-AD2E-089B26471432')
BEGIN
	INSERT [dbo].[Currency] ([CurrencyId], [CountrySubdivisionId], [TwoLetterIsoCode], [CurrencyName], [AlphabeticCode], [NumnericCode], [MinorUnit], [Symbol], [ExchangeRate], [IsDeleted], [CreationTime], [CreatorUserId], [LastModificationTime], [LastModifierUserId], [DeletionTime], [DeleterUserId]) VALUES (N'C5EF2CFA-A05E-44ED-AD2E-089B26471432', 1, NULL, N'Dólar', N'USD', N'2', 2, N'$', CAST(1.0000 AS Numeric(18, 4)), 0, CAST(N'2018-03-28T18:15:59.203' AS DateTime), NULL, CAST(N'2018-03-28T18:15:59.203' AS DateTime), NULL, NULL, NULL)
END

IF NOT EXISTS (SELECT * FROM Currency WHERE CurrencyId = '2FB77BEB-DD9D-4E0B-8319-3482C77E60C6')
BEGIN
	INSERT [dbo].[Currency] ([CurrencyId], [CountrySubdivisionId], [TwoLetterIsoCode], [CurrencyName], [AlphabeticCode], [NumnericCode], [MinorUnit], [Symbol], [ExchangeRate], [IsDeleted], [CreationTime], [CreatorUserId], [LastModificationTime], [LastModifierUserId], [DeletionTime], [DeleterUserId]) VALUES (N'2FB77BEB-DD9D-4E0B-8319-3482C77E60C6', 1, NULL, N'Euro', N'EUR', N'2', 2, N'€', CAST(0.8800 AS Numeric(18, 4)), 0, CAST(N'2018-03-28T18:15:59.203' AS DateTime), NULL, CAST(N'2018-03-28T18:15:59.203' AS DateTime), NULL, NULL, NULL)
END

IF NOT EXISTS (SELECT * FROM Currency WHERE CurrencyId = '100326FD-450D-48AF-A756-F28AAF7D87DA')
BEGIN
	INSERT [dbo].[Currency] ([CurrencyId], [CountrySubdivisionId], [TwoLetterIsoCode], [CurrencyName], [AlphabeticCode], [NumnericCode], [MinorUnit], [Symbol], [ExchangeRate], [IsDeleted], [CreationTime], [CreatorUserId], [LastModificationTime], [LastModifierUserId], [DeletionTime], [DeleterUserId]) VALUES (N'100326FD-450D-48AF-A756-F28AAF7D87DA', 1, NULL, N'Peso Argentino', N'ARS', N'2', 2, N'$', CAST(41.6800 AS Numeric(18, 4)), 0, CAST(N'2018-03-28T18:15:59.203' AS DateTime), NULL, CAST(N'2018-03-28T18:15:59.203' AS DateTime), NULL, NULL, NULL)
END

GO
