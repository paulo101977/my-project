﻿IF NOT EXISTS (SELECT * FROM Reason WHERE ReasonName = 'Abrir Chamado' and ReasonCategoryId = 12)
BEGIN
	INSERT INTO [dbo].[Reason] ([ReasonName], [ReasonCategoryId], [IsActive], [ChainId]) VALUES (N'Abrir Chamado', 12, 1, 1);
END

IF NOT EXISTS (SELECT * FROM Reason WHERE ReasonName = 'Manutenção Rápida' and ReasonCategoryId = 12)
BEGIN
	INSERT INTO [dbo].[Reason] ([ReasonName], [ReasonCategoryId], [IsActive], [ChainId]) VALUES (N'Manutenção Rápida', 12, 1, 1);
END

IF NOT EXISTS (SELECT * FROM Reason WHERE ReasonName = 'Roubo ou Furto' and ReasonCategoryId = 12)
BEGIN
	INSERT INTO [dbo].[Reason] ([ReasonName], [ReasonCategoryId], [IsActive], [ChainId]) VALUES (N'Roubo ou Furto', 12, 1, 1);
END

IF NOT EXISTS (SELECT * FROM Reason WHERE ReasonName = 'Acidente' and ReasonCategoryId = 12)
BEGIN
	INSERT INTO [dbo].[Reason] ([ReasonName], [ReasonCategoryId], [IsActive], [ChainId]) VALUES (N'Acidente', 12, 1, 1);
END

IF NOT EXISTS (SELECT * FROM Reason WHERE ReasonName = 'Outro' and ReasonCategoryId = 12)
BEGIN
	INSERT INTO [dbo].[Reason] ([ReasonName], [ReasonCategoryId], [IsActive], [ChainId]) VALUES (N'Outro', 12, 1, 1);
END

IF NOT EXISTS (SELECT * FROM Reason WHERE ReasonName = 'Acidente' and ReasonCategoryId = 13)
BEGIN
	INSERT INTO [dbo].[Reason] ([ReasonName], [ReasonCategoryId], [IsActive], [ChainId]) VALUES (N'Acidente', 13, 1, 1);
END

IF NOT EXISTS (SELECT * FROM Reason WHERE ReasonName = 'Outro' and ReasonCategoryId = 13)
BEGIN
	INSERT INTO [dbo].[Reason] ([ReasonName], [ReasonCategoryId], [IsActive], [ChainId]) VALUES (N'Outro', 13, 1, 1);
END
GO









