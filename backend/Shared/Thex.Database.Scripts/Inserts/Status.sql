﻿IF NOT EXISTS (SELECT * FROM Status WHERE StatusId = 16)
BEGIN
	insert into status values (16,4,'Vago')
END

IF NOT EXISTS (SELECT * FROM Status WHERE StatusId = 17)
BEGIN
	insert into status values (17,4,'Ocupado')
END

IF NOT EXISTS (SELECT * FROM Status WHERE StatusId = 18)
BEGIN
	insert into status values (18,4,'Bloqueado')
END

IF NOT EXISTS (SELECT * FROM Status WHERE StatusId = 19)
BEGIN
	insert into status values (19,3,'Production')
END

IF NOT EXISTS (SELECT * FROM Status WHERE StatusId = 20)
BEGIN
	insert into status values (20,3,'Blocked')
END

IF NOT EXISTS (SELECT * FROM Status WHERE StatusId = 21)
BEGIN
	insert into status values (21,3,'Unblocked')
END



if EXISTS (SELECT * FROM Status where Name = 'Complete' and StatusCategoryId = 3)
BEGIN
	update Status 
	set Name = 'Register'
	where Name = 'Complete' and StatusCategoryId = 3
END

if EXISTS (SELECT * FROM Status where Name = 'Registering' and StatusCategoryId = 3)
BEGIN
	update Status 
	set Name = 'Contract'
	where Name = 'Registering' and StatusCategoryId = 3
END

if EXISTS (SELECT * FROM Status where Name = 'Setting' and StatusCategoryId = 3)
BEGIN
	update Status 
	set Name = 'Training'
	where Name = 'Setting' and StatusCategoryId = 3
END
GO





