﻿update ReservationItem
set IsMigrated = 0
where IsMigrated is null

GO

UPDATE ReservationItem
SET ReservationItem.RatePlanId = R.RatePlanId
FROM ReservationItem RI
INNER JOIN Reservation R
ON RI.ReservationId = R.ReservationId
WHERE RI.IsMigrated = 1 and RI.RatePlanId is null