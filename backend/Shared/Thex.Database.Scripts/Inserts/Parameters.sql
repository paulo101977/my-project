﻿IF NOT EXISTS (SELECT * FROM [ApplicationParameter] WHERE [ApplicationParameterId] = 14)
BEGIN
	INSERT [dbo].[ApplicationParameter] ([ApplicationParameterId], [ApplicationModuleId], [FeatureGroupId], [ParameterName], [ParameterDescription], [ParameterDefaultValue], [ParameterTypeId], [ParameterComponentType], [ParameterMinValue], [ParameterMaxValue], [ParameterPossibleValues], [IsNullable], [IsDeleted], [CreationTime], [CreatorUserId], [LastModificationTime], [LastModifierUserId], [DeletionTime], [DeleterUserId]) 
	VALUES (14, 1, 2, N'Diferença de Diária', N'Diferença de Diária', NULL, 2, N'Tipos de Serviço', NULL, NULL, NULL, 0, 0, CAST(N'2018-04-09T14:09:11.010' AS DateTime), NULL, CAST(N'2018-04-09T14:09:11.010' AS DateTime), NULL, NULL, NULL)
END
GO