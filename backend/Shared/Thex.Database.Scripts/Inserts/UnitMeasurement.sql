﻿IF NOT EXISTS (SELECT * FROM UnitMeasurement WHERE [UnitMeasurementId] = '8e6671c3-35bf-4e26-9a37-517256143566')
BEGIN
	INSERT INTO [dbo].[UnitMeasurement]
			   ([UnitMeasurementId]
			   ,[Name]
			   ,[Initials])
		 VALUES
			   ('8e6671c3-35bf-4e26-9a37-517256143566'
			   ,'Unidade'
			   ,'Un')
END

IF NOT EXISTS (SELECT * FROM UnitMeasurement WHERE [UnitMeasurementId] = '5ec1cc08-c6ed-49db-af23-4357b90795ec')
BEGIN
	INSERT INTO [dbo].[UnitMeasurement]
			   ([UnitMeasurementId]
			   ,[Name]
			   ,[Initials])
		 VALUES
			   ('5ec1cc08-c6ed-49db-af23-4357b90795ec'
			   ,'Kilo'
			   ,'Kg')
END
GO

