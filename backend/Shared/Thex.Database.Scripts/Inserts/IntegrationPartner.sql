﻿IF NOT EXISTS (SELECT * FROM IntegrationPartner WHERE TokenClient = '1796a5e2-54b7-408d-91e1-dad557d68086')
BEGIN
	INSERT [dbo].[IntegrationPartner] ([TokenClient], [TokenApplication], [IntegrationPartnerName], [IntegrationPartnerType], [IsActive]) VALUES ('1796a5e2-54b7-408d-91e1-dad557d68086', 'a865fddd-cc86-4983-a9ee-3ae0d5669f2a', 'NfeioFunction', 2, 1)
END

IF NOT EXISTS (SELECT * FROM IntegrationPartner WHERE TokenClient = '85daad68-f075-4572-a0e4-2a157c2a08d6')
BEGIN
	INSERT [dbo].[IntegrationPartner] ([TokenClient], [TokenApplication], [IntegrationPartnerName], [IntegrationPartnerType], [IsActive]) VALUES ('85daad68-f075-4572-a0e4-2a157c2a08d6', '4731f49e-86e2-4274-822d-e6c1f0db1ec1', 'PdvFunction', 2, 1)
END

IF NOT EXISTS (SELECT * FROM IntegrationPartner WHERE TokenClient = '04e3d3c1-193a-47ab-a69e-b129550f4550')
BEGIN
	INSERT [dbo].[IntegrationPartner] ([TokenClient], [TokenApplication], [IntegrationPartnerName], [IntegrationPartnerType], [IsActive]) VALUES ('04e3d3c1-193a-47ab-a69e-b129550f4550', 'c636fca5-0597-478e-80f4-7244623456f6', 'CMNETIntegration', 2, 1)
END

IF NOT EXISTS (SELECT * FROM IntegrationPartner WHERE TokenClient = 'OTUxMmI3YzItZGQ5Ny00YjE5LTg1ZTctZWRmNzJkYWIwNDAw')
BEGIN
	INSERT [dbo].[IntegrationPartner] ([TokenClient], [TokenApplication], [IntegrationPartnerName], [IntegrationPartnerType], [IsActive]) VALUES ('OTUxMmI3YzItZGQ5Ny00YjE5LTg1ZTctZWRmNzJkYWIwNDAw', null, 'ENOTAS', 4, 1)
END

IF NOT EXISTS (SELECT * FROM IntegrationPartner WHERE IntegrationPartnerName = 'INVOICEXPRESS')
BEGIN
	INSERT [dbo].[IntegrationPartner] ([TokenClient], [TokenApplication], [IntegrationPartnerName], [IntegrationPartnerType], [IsActive]) VALUES (null, null, 'INVOICEXPRESS', 5, 1)
END

IF NOT EXISTS (SELECT * FROM IntegrationPartner WHERE IntegrationPartnerName = 'SIBA')
BEGIN
	INSERT [dbo].[IntegrationPartner] ([TokenClient], [TokenApplication], [IntegrationPartnerName], [IntegrationPartnerType], [IsActive]) VALUES (null, null, 'SIBA', 6, 1)
END

GO