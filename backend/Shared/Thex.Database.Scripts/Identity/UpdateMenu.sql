﻿-- Delete Menu Property Panel
IF EXISTS (SELECT * FROM IdentityMenuFeature WHERE IdentityMenuFeatureId = '21c6d5d4-07b0-4c88-b202-c5fb337b878f')
BEGIN
	DELETE IdentityMenuFeature WHERE IdentityMenuFeatureId = '21c6d5d4-07b0-4c88-b202-c5fb337b878f'
	DELETE IdentityFeature WHERE IdentityFeatureId = '1a95b5f4-6335-4479-95e3-6191da18ccfa'
	DELETE IdentityPermissionFeature WHERE IdentityFeatureId = '1a95b5f4-6335-4479-95e3-6191da18ccfa'
END

-- Delete Menu Financial
IF EXISTS (SELECT * FROM IdentityMenuFeature WHERE IdentityMenuFeatureId = '48f61bff-fab3-4951-b328-5fe8255a432c')
BEGIN
	DELETE IdentityMenuFeature WHERE IdentityMenuFeatureId = '48f61bff-fab3-4951-b328-5fe8255a432c'
	DELETE IdentityFeature WHERE IdentityFeatureId = 'c88ca851-a013-45cf-9f6b-aefaf5b94ad4'
	DELETE IdentityPermissionFeature WHERE IdentityFeatureId = 'c88ca851-a013-45cf-9f6b-aefaf5b94ad4'
END