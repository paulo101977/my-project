﻿UPDATE PropertyBaseRate
SET PropertyBaseRateTypeId = 1
WHERE PropertyBaseRateTypeId IS NULL

GO

UPDATE PropertyBaseRate
SET PropertyMealPlanTypeRate = 0
WHERE PropertyMealPlanTypeRate IS NULL