﻿
   UPDATE IdentityFeature SET ExternalAccess = 1 WHERE [Key] IN (
	'Booking_ExternalRegistration_RoomList_Get',
	'Booking_ExternalRegistration_RegistrationList_Get',
	'Booking_ExternalRegistration_Registration_Get',
	'Booking_ExternalRegistration_Put',
	'PMS_Country_Get',
	'PMS_Nationality_Get',
	'PMS_DocumentType_Get'
   )

GO

