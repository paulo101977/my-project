﻿IF NOT EXISTS(SELECT 1 FROM sys.columns 
          WHERE Name = N'ReservationId'
          AND Object_ID = Object_ID(N'dbo.RoomOccupied'))
	BEGIN
		ALTER TABLE RoomOccupied
		ADD [ReservationId] BIGINT NOT NULL DEFAULT 0;
	END	 
GO


IF (OBJECT_ID('UK_RoomId') IS NOT NULL)
	BEGIN
	
		ALTER TABLE RoomOccupied
		DROP CONSTRAINT UK_RoomId;	   

	END
GO


IF (OBJECT_ID('FK_RoomAvailable_PropertyId') IS NOT NULL)
	BEGIN
		
		ALTER TABLE RoomOccupied
		DROP CONSTRAINT FK_RoomAvailable_PropertyId;	 

	END
GO


IF (OBJECT_ID('FK_RoomAvailable_Tenant') IS NOT NULL)
	BEGIN
		
		ALTER TABLE RoomOccupied
		DROP CONSTRAINT FK_RoomAvailable_Tenant;	 

	END
GO


