﻿declare @datatype as varchar(100)

SELECT @datatype = DATA_TYPE 
FROM INFORMATION_SCHEMA.COLUMNS
WHERE 
     TABLE_NAME = 'plasticbrandproperty' AND 
     COLUMN_NAME = 'creatoruserid'

IF @datatype = 'bigint'
BEGIN
    ALTER TABLE plasticbrandproperty
	ALTER COLUMN creatoruserid varchar(200)

	ALTER TABLE plasticbrandproperty
	ALTER COLUMN creatoruserid UNIQUEIDENTIFIER

	ALTER TABLE plasticbrandproperty
	ALTER COLUMN lastmodifieruserid varchar(200)

	ALTER TABLE plasticbrandproperty
	ALTER COLUMN lastmodifieruserid UNIQUEIDENTIFIER

	ALTER TABLE plasticbrandproperty
	ALTER COLUMN deleteruserid varchar(200)

	ALTER TABLE plasticbrandproperty
	ALTER COLUMN deleteruserid UNIQUEIDENTIFIER
END
GO

