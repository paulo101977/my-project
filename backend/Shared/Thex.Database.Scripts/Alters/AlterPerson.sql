﻿if EXISTS(SELECT * FROM INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS WHERE CONSTRAINT_NAME ='CK_Person_PersonType')
BEGIN
ALTER TABLE Person
DROP CONSTRAINT CK_Person_PersonType
ALTER TABLE Person
ADD CONSTRAINT [CK_Person_PersonType] CHECK ([PersonType] IN ('L', 'N', 'C', 'H'))
END

IF NOT EXISTS(SELECT 1 FROM sys.columns 
          WHERE Name = N'NickName'
          AND Object_ID = Object_ID(N'dbo.Person'))
BEGIN
	ALTER TABLE Person
	ADD NickName Varchar(1000) NULL;	
END

GO