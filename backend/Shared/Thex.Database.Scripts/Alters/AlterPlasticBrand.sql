﻿IF NOT EXISTS(SELECT 1 FROM sys.columns 
          WHERE Name = N'IconName'
          AND Object_ID = Object_ID(N'dbo.PlasticBrand'))
BEGIN
	ALTER TABLE PlasticBrand
	ADD IconName VARCHAR(50) NULL
END
GO

IF NOT EXISTS(SELECT 1 FROM sys.columns 
          WHERE Name = N'IconName'
          AND Object_ID = Object_ID(N'dbo.PlasticBrand') 
		  AND N'PlasticBrandName' = 'VISA')
BEGIN
	UPDATE PlasticBrand SET IconName = N'icon_visa.svg' 
	WHERE PlasticBrandName = 'VISA' 
END

IF NOT EXISTS(SELECT 1 FROM sys.columns 
          WHERE Name = N'IconName'
          AND Object_ID = Object_ID(N'dbo.PlasticBrand') 
		  AND N'PlasticBrandName' = 'MASTERCARD')
BEGIN
	UPDATE PlasticBrand SET IconName = N'icon_master.svg' 
	WHERE PlasticBrandName = 'MASTERCARD' 
END

IF NOT EXISTS(SELECT 1 FROM sys.columns 
          WHERE Name = N'IconName'
          AND Object_ID = Object_ID(N'dbo.PlasticBrand') 
		  AND N'PlasticBrandName' = 'AMERICAN EXPRESS')
BEGIN
	UPDATE PlasticBrand SET IconName = N'icon_amex.svg' 
	WHERE PlasticBrandName = 'AMERICAN EXPRESS' 
END

IF NOT EXISTS(SELECT 1 FROM sys.columns 
          WHERE Name = N'IconName'
          AND Object_ID = Object_ID(N'dbo.PlasticBrand') 
		  AND N'PlasticBrandName' = 'ELO')
BEGIN
	UPDATE PlasticBrand SET IconName = N'elo_icon.svg' 
	WHERE PlasticBrandName = 'ELO' 
END

IF NOT EXISTS(SELECT 1 FROM sys.columns 
          WHERE Name = N'IconName'
          AND Object_ID = Object_ID(N'dbo.PlasticBrand') 
		  AND N'PlasticBrandName' = 'HIPERCARD')
BEGIN
	UPDATE PlasticBrand SET IconName = N'hipercard_icon.svg' 
	WHERE PlasticBrandName = 'HIPERCARD' 
END

IF NOT EXISTS(SELECT 1 FROM sys.columns 
          WHERE Name = N'IconName'
          AND Object_ID = Object_ID(N'dbo.PlasticBrand') 
		  AND N'PlasticBrandName' = 'DINERS CLUB')
BEGIN
	UPDATE PlasticBrand SET IconName = N'icon_diners.svg' 
	WHERE PlasticBrandName = 'DINERS CLUB' 
END

IF NOT EXISTS(SELECT 1 FROM sys.columns 
          WHERE Name = N'IconName'
          AND Object_ID = Object_ID(N'dbo.PlasticBrand') 
		  AND N'PlasticBrandName' = 'SOROCRED')
BEGIN
	UPDATE PlasticBrand SET IconName = N'sorocred_icon.svg' 
	WHERE PlasticBrandName = 'SOROCRED' 
END

IF NOT EXISTS(SELECT 1 FROM sys.columns 
          WHERE Name = N'PlasticBrandUid'
          AND Object_ID = Object_ID(N'dbo.PlasticBrand'))
BEGIN
	ALTER TABLE PlasticBrand ADD PlasticBrandUid UNIQUEIDENTIFIER NULL
END
GO

IF NOT EXISTS (SELECT 1 FROM PlasticBrand WHERE PlasticBrandName = 'SOROCRED' AND  PlasticBrandUid = N'825c8b2d-db71-493c-a408-b5ff22f95edf')
BEGIN
PRINT('UPDATE')
	UPDATE PlasticBrand SET PlasticBrandUid =  N'825c8b2d-db71-493c-a408-b5ff22f95edf' WHERE PlasticBrandName = 'SOROCRED'

	IF NOT EXISTS (SELECT 1 FROM Document WHERE OwnerId = N'825c8b2d-db71-493c-a408-b5ff22f95edf')
	BEGIN
		INSERT INTO Document VALUES (N'825c8b2d-db71-493c-a408-b5ff22f95edf', 2,  N'04814563000174')
	END
END

IF NOT EXISTS (SELECT 1 FROM PlasticBrand WHERE PlasticBrandName = 'DINERS CLUB' AND  PlasticBrandUid = N'3f2a4e1e-5d55-4dfa-a0f5-2043e6f72cdc')
BEGIN
	UPDATE PlasticBrand SET PlasticBrandUid = N'3f2a4e1e-5d55-4dfa-a0f5-2043e6f72cdc' WHERE PlasticBrandName = 'DINERS CLUB'

	IF NOT EXISTS (SELECT 1 FROM Document WHERE OwnerId = N'3f2a4e1e-5d55-4dfa-a0f5-2043e6f72cdc')
	BEGIN
		INSERT INTO Document VALUES (N'3f2a4e1e-5d55-4dfa-a0f5-2043e6f72cdc', 2,  N'01425787000101')
	END
END

IF NOT EXISTS (SELECT 1 FROM PlasticBrand WHERE PlasticBrandName = 'HIPERCARD' AND  PlasticBrandUid = N'37a54ab7-67fc-40be-8ce2-3ab7f9fd77a9')
BEGIN
	UPDATE PlasticBrand SET PlasticBrandUid = N'37a54ab7-67fc-40be-8ce2-3ab7f9fd77a9' WHERE PlasticBrandName = 'HIPERCARD'

	IF NOT EXISTS (SELECT 1 FROM Document WHERE OwnerId = N'37a54ab7-67fc-40be-8ce2-3ab7f9fd77a9')
	BEGIN
		INSERT INTO Document VALUES (N'37a54ab7-67fc-40be-8ce2-3ab7f9fd77a9', 2,  N'03012230000169')
	END
END

IF NOT EXISTS (SELECT 1 FROM PlasticBrand WHERE PlasticBrandName = 'ELO' AND  PlasticBrandUid = N'26061746-7240-482e-86a6-55bf1a0ba88d')
BEGIN
	UPDATE PlasticBrand SET PlasticBrandUid = N'26061746-7240-482e-86a6-55bf1a0ba88d' WHERE PlasticBrandName = 'ELO'

	IF NOT EXISTS (SELECT 1 FROM Document WHERE OwnerId = N'26061746-7240-482e-86a6-55bf1a0ba88d')
	BEGIN
		INSERT INTO Document VALUES (N'26061746-7240-482e-86a6-55bf1a0ba88d', 2,  N'01027058000191')
	END
END

IF NOT EXISTS (SELECT 1 FROM PlasticBrand WHERE PlasticBrandName = 'MASTERCARD' AND  PlasticBrandUid = N'9f28ce40-c9da-407e-be8b-fea2cd5eb389')
BEGIN
	UPDATE PlasticBrand SET PlasticBrandUid = N'9f28ce40-c9da-407e-be8b-fea2cd5eb389' WHERE PlasticBrandName = 'MASTERCARD'

	IF NOT EXISTS (SELECT 1 FROM Document WHERE OwnerId = N'9f28ce40-c9da-407e-be8b-fea2cd5eb389')
	BEGIN
		INSERT INTO Document VALUES (N'9f28ce40-c9da-407e-be8b-fea2cd5eb389', 2,  N'01425787000101')
	END
END

IF NOT EXISTS (SELECT 1 FROM PlasticBrand WHERE PlasticBrandName = 'VISA' AND  PlasticBrandUid = N'3c2b3a34-40c3-4fe0-bbb6-4ab7aed1af47')
BEGIN
	UPDATE PlasticBrand SET PlasticBrandUid = N'3c2b3a34-40c3-4fe0-bbb6-4ab7aed1af47' WHERE PlasticBrandName = 'VISA'

	IF NOT EXISTS (SELECT 1 FROM Document WHERE OwnerId = N'3c2b3a34-40c3-4fe0-bbb6-4ab7aed1af47')
	BEGIN
		INSERT INTO Document VALUES (N'3c2b3a34-40c3-4fe0-bbb6-4ab7aed1af47', 2,  N'01027058000191')
	END
END

IF NOT EXISTS (SELECT 1 FROM PlasticBrand WHERE PlasticBrandName = 'AMERICAN EXPRESS' AND  PlasticBrandUid = N'13ed18db-7eb6-4e15-95a0-c33bb8ca4163')
BEGIN
	UPDATE PlasticBrand SET PlasticBrandUid = N'13ed18db-7eb6-4e15-95a0-c33bb8ca4163' WHERE PlasticBrandName = 'AMERICAN EXPRESS'

	IF NOT EXISTS (SELECT 1 FROM Document WHERE OwnerId = N'13ed18db-7eb6-4e15-95a0-c33bb8ca4163')
	BEGIN
		INSERT INTO Document VALUES (N'13ed18db-7eb6-4e15-95a0-c33bb8ca4163', 2,  N'0796547900014')
	END
END