﻿declare @datatype as varchar(100)

SELECT @datatype = DATA_TYPE 
FROM INFORMATION_SCHEMA.COLUMNS
WHERE 
     LOWER(TABLE_NAME) = 'userlogins' AND 
     LOWER(COLUMN_NAME) = 'loginprovider'

IF @datatype = 'uniqueidentifier'
BEGIN
	ALTER TABLE [UserLogins]
	DROP CONSTRAINT [PK_UserLogins]

	ALTER TABLE [UserLogins]
	ALTER COLUMN [LoginProvider] nvarchar(128) NOT NULL

	ALTER TABLE [UserLogins] 
	ALTER COLUMN [LoginProvider] nvarchar(128) NOT NULL 

	ALTER TABLE [UserLogins]
	ADD CONSTRAINT [PK_UserLogins] PRIMARY KEY CLUSTERED 
		(
			[LoginProvider] ASC,
			[ProviderKey] ASC
	)
END
GO

