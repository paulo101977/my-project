﻿IF EXISTS(SELECT 1 FROM sys.columns 
          WHERE Name = N'CategoryName'
          AND Object_ID = Object_ID(N'dbo.ProductCategory')
		  )
BEGIN
	UPDATE ProductCategory SET CategoryName = N'AlcoholicDrinks' 
	WHERE CategoryName = 'Bebidas Alcoólicas' 
	AND StandardCategoryId IS NULL
END

IF EXISTS(SELECT 1 FROM sys.columns 
          WHERE Name = N'CategoryName'
          AND Object_ID = Object_ID(N'dbo.ProductCategory')
		  )
BEGIN
	UPDATE ProductCategory SET CategoryName = N'Others' 
	WHERE CategoryName = 'Outros' 
	AND StandardCategoryId IS NULL
END

IF EXISTS(SELECT 1 FROM sys.columns 
          WHERE Name = N'CategoryName'
          AND Object_ID = Object_ID(N'dbo.ProductCategory')
		  )
BEGIN
	UPDATE ProductCategory SET CategoryName = N'Wines' 
	WHERE CategoryName = 'Vinhos' 
	AND StandardCategoryId IS NULL
END

IF EXISTS(SELECT 1 FROM sys.columns 
          WHERE Name = N'CategoryName'
          AND Object_ID = Object_ID(N'dbo.ProductCategory')
		  )
BEGIN
	UPDATE ProductCategory SET CategoryName = N'Drinks' 
	WHERE CategoryName = 'Bebidas' 
	AND StandardCategoryId IS NULL
END

IF EXISTS(SELECT 1 FROM sys.columns 
          WHERE Name = N'CategoryName'
          AND Object_ID = Object_ID(N'dbo.ProductCategory')
		  )
BEGIN
	UPDATE ProductCategory SET CategoryName = N'Foods' 
	WHERE CategoryName = 'Alimentos' 
	AND StandardCategoryId IS NULL
END