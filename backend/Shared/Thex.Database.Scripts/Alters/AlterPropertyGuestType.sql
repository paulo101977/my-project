﻿IF NOT EXISTS(SELECT 1 FROM sys.columns 
          WHERE Name = N'IsExemptOfTourismTax'
          AND Object_ID = Object_ID(N'dbo.PropertyGuestType'))
BEGIN
	ALTER TABLE PropertyGuestType
	ADD IsExemptOfTourismTax BIT NULL DEFAULT 0
END

GO


