﻿declare @datatype as varchar(100)

SELECT @datatype = DATA_TYPE 
FROM INFORMATION_SCHEMA.COLUMNS
WHERE 
     TABLE_NAME = 'tenant' AND 
     COLUMN_NAME = 'creatoruserid'

IF @datatype = 'bigint'
BEGIN
    ALTER TABLE tenant
	ALTER COLUMN creatoruserid varchar(200)

	ALTER TABLE tenant
	ALTER COLUMN creatoruserid UNIQUEIDENTIFIER

	ALTER TABLE tenant
	ALTER COLUMN lastmodifieruserid varchar(200)

	ALTER TABLE tenant
	ALTER COLUMN lastmodifieruserid UNIQUEIDENTIFIER

	ALTER TABLE tenant
	ALTER COLUMN deleteruserid varchar(200)

	ALTER TABLE tenant
	ALTER COLUMN deleteruserid UNIQUEIDENTIFIER
END
GO

