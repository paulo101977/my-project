﻿IF NOT EXISTS(SELECT 1 FROM sys.columns 
          WHERE Name = N'IsVisible'
          AND Object_ID = Object_ID(N'dbo.BillingInvoiceModel'))
BEGIN
	ALTER TABLE BillingInvoiceModel
	ADD IsVisible BIT NULL
END

IF NOT EXISTS(SELECT 1 FROM sys.columns 
          WHERE Name = N'BillingInvoiceTypeId'
          AND Object_ID = Object_ID(N'dbo.BillingInvoiceModel'))
BEGIN
	ALTER TABLE BillingInvoiceModel
	ADD BillingInvoiceTypeId BIT NULL
END

BEGIN
	UPDATE BillingInvoiceModel SET Description = 'NFS-e', TaxModel = 'NFS-e', isVisible = 1 WHERE BillingInvoiceModelId = 1
	UPDATE BillingInvoiceModel SET Description = 'NFC-e', TaxModel = 'NFC-e', isVisible = 1, BillingInvoiceTypeId = 2 WHERE BillingInvoiceModelId = 2
END

BEGIN
	UPDATE BillingInvoiceModel SET BillingInvoiceTypeId = 1
	WHERE BillingInvoiceTypeId IS NULL
END

GO


