﻿if EXISTS(SELECT * FROM INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS WHERE CONSTRAINT_NAME ='UK_PropertyCompanyClientCategory')
BEGIN
	ALTER TABLE PropertyCompanyClientCategory
	DROP CONSTRAINT UK_PropertyCompanyClientCategory

	ALTER TABLE PropertyCompanyClientCategory
	ADD CONSTRAINT [UK_PropertyCompanyClientCategory] UNIQUE ( [PropertyId], [PropertyCompanyClientCategoryName], [IsDeleted], [DeletionTime] )
END

GO
