﻿if EXISTS(SELECT * FROM INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS WHERE CONSTRAINT_NAME ='UK_BillingInvoicePropertySupportedType')
BEGIN
ALTER TABLE BillingInvoicePropertySupportedType
DROP CONSTRAINT UK_BillingInvoicePropertySupportedType
ALTER TABLE BillingInvoicePropertySupportedType
ADD CONSTRAINT UK_BillingInvoicePropertySupportedType UNIQUE ( [BillingItemTypeId], [BillingInvoicePropertyId], [PropertyId],[BillingItemId],[CountrySubdvisionServiceId],[IsDeleted],[DeletionTime])
END
GO