﻿IF NOT EXISTS(SELECT 1 FROM sys.columns 
          WHERE Name = N'IconName'
          AND Object_ID = Object_ID(N'dbo.BillingItemCategory'))
BEGIN
	ALTER TABLE BillingItemCategory
	ADD IconName VARCHAR(50) NULL
END

IF NOT EXISTS(SELECT 1 FROM sys.columns 
          WHERE Name = N'IconName'
          AND Object_ID = Object_ID(N'dbo.BillingItemCategory') 
		  AND N'CategoryName' = 'Restaurante')
BEGIN
	UPDATE BillingItemCategory SET IconName = N'restaurant.svg' 
	WHERE CategoryName = 'Restaurante' 
	AND StandardCategoryId IS NULL
END

IF NOT EXISTS(SELECT 1 FROM sys.columns 
          WHERE Name = N'IconName'
          AND Object_ID = Object_ID(N'dbo.BillingItemCategory') 
		  AND N'CategoryName' = 'Outros')
BEGIN
	UPDATE BillingItemCategory SET IconName = N'others.svg' 
	WHERE CategoryName = 'Outros' 
	AND StandardCategoryId IS NULL
END

IF NOT EXISTS(SELECT 1 FROM sys.columns 
          WHERE Name = N'IconName'
          AND Object_ID = Object_ID(N'dbo.BillingItemCategory') 
		  AND N'CategoryName' = 'Bares')
BEGIN
	UPDATE BillingItemCategory SET IconName = N'bars.svg' 
	WHERE CategoryName = 'Bares' 
	AND StandardCategoryId IS NULL
END

IF NOT EXISTS(SELECT 1 FROM sys.columns 
          WHERE Name = N'IconName'
          AND Object_ID = Object_ID(N'dbo.BillingItemCategory') 
		  AND N'CategoryName' = 'Diária')
BEGIN
	UPDATE BillingItemCategory SET IconName = N'daily.svg' 
	WHERE CategoryName = 'Diária' 
	AND StandardCategoryId IS NULL
END

IF NOT EXISTS(SELECT 1 FROM sys.columns 
          WHERE Name = N'IconName'
          AND Object_ID = Object_ID(N'dbo.BillingItemCategory') 
		  AND N'CategoryName' = 'Eventos')
BEGIN
	UPDATE BillingItemCategory SET IconName = N'events.svg' 
	WHERE CategoryName = 'Eventos' 
	AND StandardCategoryId IS NULL
END

IF NOT EXISTS(SELECT 1 FROM sys.columns 
          WHERE Name = N'IconName'
          AND Object_ID = Object_ID(N'dbo.BillingItemCategory') 
		  AND N'CategoryName' = 'Banquetes')
BEGIN
	UPDATE BillingItemCategory SET IconName = N'banquet.svg' 
	WHERE CategoryName = 'Banquetes' 
	AND StandardCategoryId IS NULL
END

IF NOT EXISTS(SELECT 1 FROM sys.columns 
          WHERE Name = N'IconName'
          AND Object_ID = Object_ID(N'dbo.BillingItemCategory') 
		  AND N'CategoryName' = 'Lavanderia')
BEGIN
	UPDATE BillingItemCategory SET IconName = N'laundry.svg' 
	WHERE CategoryName = 'Lavanderia' 
	AND StandardCategoryId IS NULL
END

IF NOT EXISTS(SELECT 1 FROM sys.columns 
          WHERE Name = N'IconName'
          AND Object_ID = Object_ID(N'dbo.BillingItemCategory') 
		  AND N'CategoryName' = 'Telecomunicações')
BEGIN
	UPDATE BillingItemCategory SET IconName = N'telecommunications.svg' 
	WHERE CategoryName = 'Telecomunicações' 
	AND StandardCategoryId IS NULL
END

IF NOT EXISTS(SELECT 1 FROM sys.columns 
          WHERE Name = N'IconName'
          AND Object_ID = Object_ID(N'dbo.BillingItemCategory') 
		  AND N'CategoryName' = 'ISS')
BEGIN
	UPDATE BillingItemCategory SET IconName = N'iss.svg' 
	WHERE CategoryName = 'ISS' 
	AND StandardCategoryId IS NULL
END

IF NOT EXISTS(SELECT 1 FROM sys.columns 
          WHERE Name = N'IconName'
          AND Object_ID = Object_ID(N'dbo.BillingItemCategory') 
		  AND N'CategoryName' = 'Taxa de Serviços')
BEGIN
	UPDATE BillingItemCategory SET IconName = N'serviceTax.svg' 
	WHERE CategoryName = 'Taxa de Serviços' 
	AND StandardCategoryId IS NULL
END
GO


