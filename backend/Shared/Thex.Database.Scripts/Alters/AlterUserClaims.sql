﻿declare @datatype as varchar(100)

SELECT @datatype = DATA_TYPE 
FROM INFORMATION_SCHEMA.COLUMNS
WHERE 
     lower(TABLE_NAME) = 'userclaims' AND 
     lower(COLUMN_NAME) = 'id'

IF @datatype = 'uniqueidentifier'
BEGIN
	ALTER TABLE [UserClaims]
	DROP CONSTRAINT [PK_UserClaims]

	ALTER TABLE [UserClaims] 
	ADD [Id1] INT IDENTITY(1,1) NOT NULL

	ALTER TABLE [UserClaims] 
	DROP COLUMN [Id]

	exec sp_rename 'UserClaims.Id1', 'Id', 'column'

	ALTER TABLE [UserClaims]
	ADD CONSTRAINT [PK_UserClaims] PRIMARY KEY CLUSTERED 
		(
			[Id] ASC
		)

END
GO

