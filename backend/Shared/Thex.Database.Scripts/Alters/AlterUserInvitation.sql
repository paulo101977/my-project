﻿IF NOT EXISTS(SELECT 1 FROM sys.columns 
          WHERE Name = N'UserId'
          AND Object_ID = Object_ID(N'dbo.UserInvitation'))
BEGIN

	ALTER TABLE USERINVITATION
	ADD UserId UNIQUEIDENTIFIER NULL

	ALTER TABLE USERINVITATION
	ADD CONSTRAINT [FK_UserInvitation_Users] FOREIGN KEY ([UserId]) REFERENCES [dbo].[Users] ([Id])

	EXEC sp_executesql
        N'
		UPDATE
			Userinvitation
		SET
			Userinvitation.UserId = USERS.ID
		FROM
			Userinvitation
			INNER JOIN Users
				ON Userinvitation.UserInvitationId = Users.UserInvitationId';
		
	EXEC sp_executesql
        N'UPDATE
			Users
		SET
			UserInvitationId = NULL';
	
	ALTER TABLE [dbo].[Users] DROP CONSTRAINT [FK_Users_UserInvitation]

	ALTER TABLE [dbo].[Users] DROP COLUMN UserInvitationId;

END
GO

