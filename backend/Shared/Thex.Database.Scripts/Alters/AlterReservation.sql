﻿IF (OBJECT_ID('UK_Reservation') IS NOT NULL)
	BEGIN
		
		ALTER TABLE Reservation
		DROP CONSTRAINT UK_Reservation

	END
GO

IF (OBJECT_ID('UK_Reservation_Soft_Delete') IS NULL)
	BEGIN
		ALTER TABLE Reservation
		ADD CONSTRAINT [UK_Reservation_Soft_Delete] UNIQUE ( [ReservationCode], [PropertyId], [IsDeleted], [DeletionTime] )

	END

GO