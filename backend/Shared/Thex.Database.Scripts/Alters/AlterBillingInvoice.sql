﻿IF NOT EXISTS(SELECT 1 FROM sys.columns 
          WHERE Name = N'BillingInvoiceReferenceId'
          AND Object_ID = Object_ID(N'dbo.BillingInvoice'))
BEGIN
	ALTER TABLE BillingInvoice
	ADD BillingInvoiceReferenceId UNIQUEIDENTIFIER NULL,
	FOREIGN KEY(BillingInvoiceReferenceId) REFERENCES BillingInvoice(BillingInvoiceId);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns 
          WHERE Name = N'PersonHeaderId'
          AND Object_ID = Object_ID(N'dbo.BillingInvoice'))
BEGIN
	ALTER TABLE BillingInvoice
	ADD PersonHeaderId UNIQUEIDENTIFIER NULL
	FOREIGN KEY(PersonHeaderId) REFERENCES Person(PersonId);
END

IF NOT EXISTS(SELECT 1 FROM sys.columns 
          WHERE Name = N'BillingInvoiceTypeId'
          AND Object_ID = Object_ID(N'dbo.BillingInvoice'))
BEGIN
	ALTER TABLE BillingInvoice
	ADD BillingInvoiceTypeId INT NULL
	FOREIGN KEY(BillingInvoiceTypeId) REFERENCES BillingInvoiceType(BillingInvoiceTypeId);
END


BEGIN
	UPDATE BillingInvoice SET BillingInvoiceTypeId = 1
	WHERE BillingInvoiceTypeId IS NULL
END

GO


