﻿IF NOT EXISTS(SELECT 1 FROM sys.columns 
          WHERE Name = N'CreditAmount'
          AND Object_ID = Object_ID(N'dbo.BillingAccountItem'))
BEGIN
	ALTER TABLE BillingAccountItem
	ADD CreditAmount NUMERIC(18, 4) NULL
END

IF NOT EXISTS(SELECT 1 FROM sys.columns 
          WHERE Name = N'IsTourismTax'
          AND Object_ID = Object_ID(N'dbo.BillingAccountItem'))
BEGIN
	ALTER TABLE BillingAccountItem
	ADD IsTourismTax BIT NULL
END

IF NOT EXISTS(SELECT 1 FROM sys.columns 
          WHERE Name = N'IsTourismTax'
          AND Object_ID = Object_ID(N'dbo.BillingAccountItem'))
BEGIN
	ALTER TABLE BillingAccountItem
	ADD IsTourismTax BIT NULL
END

GO


