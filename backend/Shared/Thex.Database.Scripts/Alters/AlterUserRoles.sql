﻿declare @datatype as varchar(100)

SELECT @datatype = DATA_TYPE 
FROM INFORMATION_SCHEMA.COLUMNS
WHERE 
     LOWER(TABLE_NAME) = 'alteruserroles' AND 
     LOWER(COLUMN_NAME) = 'userrolesid'

IF @datatype = 'uniqueidentifier'
BEGIN
	ALTER TABLE [UserRoles]
	DROP CONSTRAINT [PK_UserRoles]

	ALTER TABLE [UserRoles]
	ALTER COLUMN [UserRolesId] uniqueidentifier NOT NULL

	ALTER TABLE [UserRoles]
	ADD CONSTRAINT [PK_UserRoles] PRIMARY KEY CLUSTERED 
		(
			[UserRolesId]
		)
END
GO

