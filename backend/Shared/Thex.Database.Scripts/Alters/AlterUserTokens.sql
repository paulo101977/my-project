﻿declare @datatype as varchar(100)

SELECT @datatype = DATA_TYPE 
FROM INFORMATION_SCHEMA.COLUMNS
WHERE 
     LOWER(TABLE_NAME) = 'usertokens' AND 
     LOWER(COLUMN_NAME) = 'loginprovider'

IF @datatype = 'uniqueidentifier'
BEGIN
	ALTER TABLE [UserTokens]
	DROP CONSTRAINT [PK_UserTokens]

	ALTER TABLE [UserTokens]
	ALTER COLUMN [LoginProvider] nvarchar(128) NOT NULL

	ALTER TABLE [UserTokens]
	ADD CONSTRAINT [PK_UserTokens] PRIMARY KEY CLUSTERED 
		(
			[UserId],
			[LoginProvider],
			[Name] ASC
		)
END

GO
