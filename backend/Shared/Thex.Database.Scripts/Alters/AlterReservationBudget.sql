﻿IF (OBJECT_ID('UK_ReservationBudget') IS NOT NULL)
	BEGIN
		ALTER TABLE ReservationBudget
		ADD CONSTRAINT [UK_ReservationBudget] UNIQUE ( [ReservationItemId], [BudgetDay], [IsDeleted], [DeletionTime] )
	END

GO