﻿if EXISTS(SELECT * FROM INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS WHERE CONSTRAINT_NAME ='UK_IntegrationPartnerProperty_Soft_Delete')
BEGIN
ALTER TABLE IntegrationPartnerProperty
DROP CONSTRAINT UK_IntegrationPartnerProperty_Soft_Delete
ALTER TABLE IntegrationPartnerProperty
ADD CONSTRAINT [UK_IntegrationPartnerProperty_Soft_Delete] UNIQUE ( [PropertyId], [PartnerId], [IntegrationCode], [IsDeleted], [DeletionTime])
END

IF NOT EXISTS(SELECT 1 FROM sys.columns 
          WHERE Name = N'IntegrationNumber'
          AND Object_ID = Object_ID(N'dbo.IntegrationPartnerProperty'))
BEGIN
	ALTER TABLE IntegrationPartnerProperty
	ADD IntegrationNumber VARCHAR(100) NULL
END

GO