﻿UPDATE ReservationItem SET WalkIn = 0 WHERE WalkIn IS NULL
GO

UPDATE ReservationItem
SET ExternalReservationNumber = ReservationItemUid
WHERE ExternalReservationNumber IS NULL AND IsMigrated = 0

GO

IF (OBJECT_ID('UK_ReservationItem_HigsIntegration') IS NOT NULL)
	BEGIN
		ALTER TABLE ReservationItem
		DROP CONSTRAINT UK_ReservationItem_HigsIntegration
	END

GO

IF (OBJECT_ID('UK_ReservationItem_HigsIntegration_Soft_Delete') IS NULL)
	BEGIN
		ALTER TABLE ReservationItem
		ADD CONSTRAINT [UK_ReservationItem_HigsIntegration_Soft_Delete] UNIQUE ( [ExternalReservationNumber], [PartnerReservationNumber], [TenantId], [IsDeleted], [DeletionTime] )
	END

GO

IF (OBJECT_ID('UK_ReservationItem_Code') IS NOT NULL)
	BEGIN
		ALTER TABLE ReservationItem
		DROP CONSTRAINT UK_ReservationItem_Code
	END

GO


IF (OBJECT_ID('UK_ReservationItem_Code_Soft_Delete') IS NULL)
	BEGIN
		ALTER TABLE ReservationItem
		ADD CONSTRAINT [UK_ReservationItem_Code_Soft_Delete] UNIQUE ( [ReservationItemCode], [ReservationId], [IsDeleted], [DeletionTime] )
	END

GO