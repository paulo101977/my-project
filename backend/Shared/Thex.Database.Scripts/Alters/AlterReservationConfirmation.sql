﻿IF NOT EXISTS(SELECT 1 FROM sys.columns 
          WHERE Name = N'LaunchDaily'
          AND Object_ID = Object_ID(N'dbo.ReservationConfirmation'))
BEGIN
	ALTER TABLE ReservationConfirmation
	ADD LaunchDaily BIT NULL
END

GO