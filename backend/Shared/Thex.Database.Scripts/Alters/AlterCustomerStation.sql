﻿IF EXISTS(SELECT 1 FROM sys.columns 
          WHERE Name = N'SocialReason'
          AND Object_ID = Object_ID(N'dbo.CustomerStation'))
BEGIN
    UPDATE CustomerStation
	set SocialReason = ''
	ALTER TABLE CustomerStation
	DROP COLUMN SocialReason
END
GO