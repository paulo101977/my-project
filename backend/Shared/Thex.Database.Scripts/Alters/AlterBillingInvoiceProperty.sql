﻿IF NOT EXISTS(SELECT 1 FROM sys.columns 
          WHERE Name = N'BillingInvoicePropertyReferenceId'
          AND Object_ID = Object_ID(N'dbo.BillingInvoiceProperty'))
BEGIN
	ALTER TABLE BillingInvoicePropertyReference
	ADD BillingInvoicePropertyReferenceId UNIQUEIDENTIFIER NULL
END

IF NOT EXISTS(SELECT 1 FROM sys.columns 
          WHERE Name = N'AllowsCancel'
          AND Object_ID = Object_ID(N'dbo.BillingInvoiceProperty'))
BEGIN
	ALTER TABLE BillingInvoicePropertyReference
	ADD AllowsCancel BIT NULL
END

IF NOT EXISTS(SELECT 1 FROM sys.columns 
          WHERE Name = N'DaysForCancel'
          AND Object_ID = Object_ID(N'dbo.BillingInvoiceProperty'))
BEGIN
	ALTER TABLE BillingInvoicePropertyReference
	ADD DaysForCancel INT NULL
END

BEGIN
	UPDATE BillingInvoiceProperty SET AllowsCancel = 0
	WHERE AllowsCancel IS NULL
END

GO


