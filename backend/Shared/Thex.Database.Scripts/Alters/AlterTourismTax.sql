﻿IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS WHERE CONSTRAINT_NAME ='FK_TourismTax_BillingItem')
BEGIN
	ALTER TABLE TourismTax
	ADD CONSTRAINT FK_TourismTax_BillingItem FOREIGN KEY ([BillingItemId]) REFERENCES [dbo].[BillingItem] ([BillingItemId])
END

IF EXISTS(SELECT 1 FROM sys.columns 
          WHERE Name = N'LaunchType'
          AND Object_ID = Object_ID(N'dbo.TourismTax'))
BEGIN
	ALTER TABLE TourismTax
	ALTER COLUMN LaunchType INT NULL
END

GO




