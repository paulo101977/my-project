﻿if EXISTS(SELECT * FROM INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS WHERE CONSTRAINT_NAME ='UK_Room_PropertyId_RoomNumber')
BEGIN
	ALTER TABLE Room
	DROP CONSTRAINT UK_Room_PropertyId_RoomNumber

	ALTER TABLE Room
	ADD CONSTRAINT [UK_Room_PropertyId_RoomNumber_IsDeleted_DeletionTime] UNIQUE ( [PropertyId], [RoomNumber], [IsDeleted], [DeletionTime] )
END

GO