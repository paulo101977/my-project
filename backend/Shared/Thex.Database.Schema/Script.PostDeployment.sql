﻿/*
Post-Deployment Script Template							
------------------------------------------------------------------------------------
 This file contains SQL statements that will be appended to the build script.		
 Use SQLCMD syntax to include a file in the post-deployment script.			
 Example:      :r .\myfile.sql								
 Use SQLCMD syntax to reference a variable in the post-deployment script.		
 Example:      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
------------------------------------------------------------------------------------
*/
--products
:r .\Identity\Products.sql

--modules
:r .\Identity\Modules.sql

--features -- PMS
:r .\Identity\PMS\Features\AvailabilityFeatures.sql
:r .\Identity\PMS\Features\BillingAccountClosureFeatures.sql
:r .\Identity\PMS\Features\BillingAccountFeatures.sql
:r .\Identity\PMS\Features\BillingAccountItemFeatures.sql
:r .\Identity\PMS\Features\BillingInvoiceFeatures.sql
:r .\Identity\PMS\Features\BillingInvoiceIntegrationFeatures.sql
:r .\Identity\PMS\Features\BillingInvoiceModelFeatures.sql
:r .\Identity\PMS\Features\BillingInvoicePropertyFeatures.sql
:r .\Identity\PMS\Features\BilllingItemCategoryFeatures.sql
:r .\Identity\PMS\Features\BilllingItemFeatures.sql
:r .\Identity\PMS\Features\BilllingItemPDVFeatures.sql
:r .\Identity\PMS\Features\BrandFeatures.sql
:r .\Identity\PMS\Features\BusinessSourceFeatures.sql
:r .\Identity\PMS\Features\ChainFeatures.sql
:r .\Identity\PMS\Features\CompanyClientFeatures.sql
:r .\Identity\PMS\Features\CountryFeatures.sql
:r .\Identity\PMS\Features\CountrySubdivisionServiceFeatures.sql
:r .\Identity\PMS\Features\CurrencyFeatures.sql
:r .\Identity\PMS\Features\DashboardReportFeatures.sql
:r .\Identity\PMS\Features\DocumentTypesFeatures.sql
:r .\Identity\PMS\Features\GeneralOccupationFeatures.sql
:r .\Identity\PMS\Features\GuestFeatures.sql
:r .\Identity\PMS\Features\GuestRegistrationFeatures.sql
:r .\Identity\PMS\Features\GuestReservationItemFeatures.sql
:r .\Identity\PMS\Features\HouseKeepingStatusPropertyFeatures.sql
:r .\Identity\PMS\Features\IntegrationFeatures.sql
:r .\Identity\PMS\Features\LevelFeatures.sql
:r .\Identity\PMS\Features\LevelRateFeatures.sql
:r .\Identity\PMS\Features\MarketSegmentFeatures.sql
:r .\Identity\PMS\Features\NationalityFeatures.sql
:r .\Identity\PMS\Features\PaymentTypeFeatures.sql
:r .\Identity\PMS\Features\PlasticBrandPropertyFeatures.sql
:r .\Identity\PMS\Features\PropertyAuditProcessFeatures.sql
:r .\Identity\PMS\Features\PropertyBaseRateFeatures.sql
:r .\Identity\PMS\Features\PropertyCompanyClientCategoryFeatures.sql
:r .\Identity\PMS\Features\PropertyCurrencyExchangeFeatures.sql
:r .\Identity\PMS\Features\PropertyFeatures.sql
:r .\Identity\PMS\Features\PropertyGuestPolicyFeatures.sql
:r .\Identity\PMS\Features\PropertyGuestTypeFeatures.sql
:r .\Identity\PMS\Features\PropertyMealPlanTypeFeatures.sql
:r .\Identity\PMS\Features\PropertyMealPlanTypeRateFeatures.sql
:r .\Identity\PMS\Features\PropertyParameterFeatures.sql
:r .\Identity\PMS\Features\PropertyPolicyFeatures.sql
:r .\Identity\PMS\Features\RateCalendarFeatures.sql
:r .\Identity\PMS\Features\RatePlanFeatures.sql
:r .\Identity\PMS\Features\RateProposalFeatures.sql
:r .\Identity\PMS\Features\ReasonFeatures.sql
:r .\Identity\PMS\Features\ReservationBudgetFeatures.sql
:r .\Identity\PMS\Features\ReservationCompanyClientFeatures.sql
:r .\Identity\PMS\Features\ReservationFeatures.sql
:r .\Identity\PMS\Features\ReservationGuestFeatures.sql
:r .\Identity\PMS\Features\ReservationItemFeatures.sql
:r .\Identity\PMS\Features\ReservationReportFeatures.sql
:r .\Identity\PMS\Features\RoomFeatures.sql
:r .\Identity\PMS\Features\RoomLayoutFeatures.sql
:r .\Identity\PMS\Features\RoomTypeFeatures.sql
:r .\Identity\PMS\Features\TransportationTypeFeatures.sql
:r .\Identity\PMS\Features\PropertyRateStrategyFeatures.sql
:r .\Identity\PMS\Features\PropertyPremiseHeaderFeatures.sql
:r .\Identity\PMS\Features\GuestEntrance.sql
:r .\Identity\PMS\Features\TourismTaxFeatures.sql
:r .\Identity\PMS\Features\SibaIntegrationFeatures.sql
:r .\Identity\PMS\Features\ProformaFeatures.sql

--features -- PDV
:r .\Identity\PDV\Features\BillingAccountItemFeatures.sql
:r .\Identity\PDV\Features\BillingItemFeatures.sql
:r .\Identity\PDV\Features\NcmFeatures.sql
:r .\Identity\PDV\Features\ProductBillingItemBillingAccountItemFeatures.sql
:r .\Identity\PDV\Features\ProductCategoryFeatures.sql
:r .\Identity\PDV\Features\ProductFeatures.sql
:r .\Identity\PDV\Features\UnitMeasurementFeatures.sql

--features -- HouseKeeping
:r .\Identity\HouseKeeping\Features\HousekeepingRoomFeatures.sql
:r .\Identity\HouseKeeping\Features\UserFeatures.sql
:r .\Identity\HouseKeeping\Features\HouseKeepingGeneratorFeatures.sql

--features -- SuperAdmin
:r .\Identity\SuperAdmin\Features\UserFeatures.sql

--features -- Preferences
:r .\Identity\UserPreference\Features\UserFeatures.sql

--features -- TaxRule
:r .\Identity\TaxRule\Features\BrasilFeatures.sql
:r .\Identity\TaxRule\Features\TaxRuleFeatures.sql
:r .\Identity\TaxRule\Features\PortugalFeatures.sql

--features -- HigsIntegration
:r .\Identity\HigsIntegration\Features\IntegrationFeatures.sql

--features -- Booking
:r .\Identity\Booking\Features\ExternalFeatures.sql

--features -- CENTRAL
:r .\Identity\CENTRAL\Features\AvailabilityFeatures.sql
:r .\Identity\CENTRAL\Features\BusinessSourceFeatures.sql
:r .\Identity\CENTRAL\Features\CompanyClientFeatures.sql
:r .\Identity\CENTRAL\Features\CountryFeatures.sql
:r .\Identity\CENTRAL\Features\DocumentTypesFeatures.sql
:r .\Identity\CENTRAL\Features\GeneralOccupationFeatures.sql
:r .\Identity\CENTRAL\Features\GuestFeatures.sql
:r .\Identity\CENTRAL\Features\GuestRegistrationFeatures.sql
:r .\Identity\CENTRAL\Features\GuestReservationItemFeatures.sql
:r .\Identity\CENTRAL\Features\MarketSegmentFeatures.sql
:r .\Identity\CENTRAL\Features\NationalityFeatures.sql
:r .\Identity\CENTRAL\Features\PlasticBrandPropertyFeatures.sql
:r .\Identity\CENTRAL\Features\PropertyFeatures.sql
:r .\Identity\CENTRAL\Features\PropertyGuestTypeFeatures.sql
:r .\Identity\CENTRAL\Features\PropertyMealPlanTypeFeatures.sql
:r .\Identity\CENTRAL\Features\PropertyParameterFeatures.sql
:r .\Identity\CENTRAL\Features\RatePlanFeatures.sql
:r .\Identity\CENTRAL\Features\RateProposalFeatures.sql
:r .\Identity\CENTRAL\Features\ReasonFeatures.sql
:r .\Identity\CENTRAL\Features\ReservationBudgetFeatures.sql
:r .\Identity\CENTRAL\Features\ReservationFeatures.sql
:r .\Identity\CENTRAL\Features\ReservationItemFeatures.sql
:r .\Identity\CENTRAL\Features\RoomLayoutFeatures.sql
:r .\Identity\CENTRAL\Features\RoomTypeFeatures.sql
:r .\Identity\CENTRAL\Features\TransportationTypeFeatures.sql


--public features - MENU - PMS
--features -- Repor
:r .\Identity\Reports\Features\PensionPrevisionFeatures.sql
:r .\Identity\Reports\Features\BillingAccountTransferFeatures.sql

--public features - MENU
:r .\Identity\PMS\_Menu\PublicFeatures.sql

--public features - MENU - CENTRAL
:r .\Identity\CENTRAL\_Menu\PublicFeatures.sql

--menu feature - PMS
:r .\Identity\PMS\_Menu\MenuFeature.sql

--menu feature - CENTRAL
:r .\Identity\CENTRAL\_Menu\MenuFeature.sql

--permissions - PMS
:r .\Identity\PMS\Permissions\CashOperationPermissions.sql
:r .\Identity\PMS\Permissions\HostingPermissions.sql
:r .\Identity\PMS\Permissions\IntegrationPermissions.sql
:r .\Identity\PMS\Permissions\HouseKeepingPermissions.sql
:r .\Identity\PMS\Permissions\ReportPermissions.sql
:r .\Identity\PMS\Permissions\ReservationPermissions.sql
:r .\Identity\PMS\Permissions\RMPermissions.sql
:r .\Identity\PMS\Permissions\SettingsPermissions.sql
:r .\Identity\PMS\Permissions\HomePermissions.sql
:r .\Identity\Reports\Permissions\ReportPermissions.sql
:r .\Identity\HouseKeeping\Permissions\RoomPermissions.sql

--permissions - CENTRAL
:r .\Identity\CENTRAL\Permissions\HomePermissions.sql
:r .\Identity\CENTRAL\Permissions\ReservationPermissions.sql

--roles - PMS
:r .\Identity\PMS\Roles\Administrator.sql
:r .\Identity\PMS\Roles\Employee.sql
:r .\Identity\HouseKeeping\Roles\RoomMaid.sql

--roles - CENTRAL
:r .\Identity\CENTRAL\Roles\Administrator.sql




