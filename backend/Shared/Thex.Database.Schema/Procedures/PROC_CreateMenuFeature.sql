﻿CREATE PROCEDURE [dbo].[PROC_CreateMenuFeature]
(
	@identityMenuFeatureId UNIQUEIDENTIFIER,
	@identityProductId int,
	@identityModuleId int,
	@identityFeatureParentId UNIQUEIDENTIFIER,
	@identityFeatureId UNIQUEIDENTIFIER,
	@isActive int,
	@orderNumber int,
	@level int,
	@description varchar(MAX)
)
AS
Begin

	if(not exists(select 1 from IdentityMenuFeature where IdentityMenuFeatureId = @identityMenuFeatureId))
		INSERT INTO [dbo].[IdentityMenuFeature]
           ([IdentityMenuFeatureId],[IdentityProductId],[IdentityModuleId]
           ,[IdentityFeatureParentId],[IdentityFeatureId],[IsActive]
           ,[OrderNumber],[Level],[Description]
           ,[IsDeleted],[CreationTime],[CreatorUserId],[LastModificationTime],[LastModifierUserId],[DeletionTime],[DeleterUserId])
     VALUES
           (@identityMenuFeatureId,@identityProductId,@identityModuleId
           ,@identityFeatureParentId,@identityFeatureId,@isActive
           ,@orderNumber,@level,@description
           ,0,GETDATE(),NULL,GETDATE(),NULL,NULL,NULL)
	else
		update [IdentityMenuFeature] set 
			[IdentityMenuFeatureId] = @identityMenuFeatureId ,
			[IdentityProductId] = @identityProductId,
			[IdentityModuleId] = @identityModuleId,
			[IdentityFeatureParentId] = @identityFeatureParentId,
			[IdentityFeatureId] = @identityFeatureId,
			[IsActive] = @isActive,
			[OrderNumber] = @orderNumber,
			[Level] = @level,
			[Description] = @description
		where IdentityMenuFeatureId = @identityMenuFeatureId;

End