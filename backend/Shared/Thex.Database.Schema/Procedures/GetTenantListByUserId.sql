﻿CREATE PROCEDURE [dbo].[GetTenantIdListByUserId]
		@UserId UNIQUEIDENTIFIER
	AS
	BEGIN
		WITH Tenantlist AS
		(
		SELECT T.[TenantId] AS Id
		  FROM [dbo].[Tenant] as T
		  JOIN [dbo].[UserTenant] as UT on UT.TenantId = T.TenantId
		  WHERE T.[IsActive] = 1 AND  UT.[IsActive] = 1 AND T.IsDeleted = 0 AND UT.IsDeleted = 0 AND UT.UserId = @UserId
		UNION ALL
		SELECT TN.[TenantId] AS Id
		  FROM [dbo].[Tenant] AS TN
			   INNER JOIN TenantList AS TL
			   ON TN.ParentId = TL.Id
		  WHERE TN.[IsActive] = 1 AND TN.IsDeleted = 0 AND TN.[ParentId] IS NOT NULL
		) SELECT * FROM TenantList;
	END
GO
