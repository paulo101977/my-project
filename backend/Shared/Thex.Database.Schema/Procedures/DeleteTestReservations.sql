﻿CREATE PROCEDURE [dbo].[DeleteTestReservations]
AS
BEGIN

	DELETE GUESTRESERV
		FROM [dbo].[GuestReservationItem] GUESTRESERV
		  WHERE GUESTRESERV.GuestReservationItemId BETWEEN 2 AND 17;

	DELETE BUDG
		FROM [dbo].[ReservationBudget] BUDG
			 INNER JOIN [dbo].[ReservationItem] ITEM
				ON BUDG.ReservationItemId = ITEM.ReservationItemId
			 INNER JOIN [dbo].[Reservation] RESERV ON
				  ITEM.ReservationId = RESERV.ReservationId
		  WHERE RESERV.ReservationId BETWEEN 2 AND 9;

	DELETE ITEM
		FROM [dbo].[ReservationItem] ITEM
			 INNER JOIN [dbo].[Reservation] RESERV ON
				  ITEM.ReservationId = RESERV.ReservationId
		  WHERE RESERV.ReservationId BETWEEN 2 AND 9;

	DELETE FROM [dbo].[Reservation]
		  WHERE ReservationId BETWEEN 2 AND 9;
END;