﻿CREATE PROCEDURE [dbo].[PROC_CreateModule]
(
	@identityModuleId int,
	@identityModuleName varchar(200)
)
WITH EXECUTE AS OWNER
AS
Begin
	SET IDENTITY_INSERT [dbo].[IdentityModule] ON;

	if(not exists(select 1 from IdentityModule where IdentityModuleId = @identityModuleId))
		INSERT INTO [dbo].[IdentityModule]
			   ([IdentityModuleId],[Name],[Description],[IsDeleted],[CreationTime],[CreatorUserId],[LastModificationTime],[LastModifierUserId],[DeletionTime],[DeleterUserId])
		 VALUES
			   (@identityModuleId, @identityModuleName, NULL, 0, GETDATE(), NULL, GETDATE(), NULL, NULL, NULL);
	else
		update [IdentityModule] set [Name] = @identityModuleName where IdentityModuleId = @identityModuleId;

	SET IDENTITY_INSERT [dbo].[IdentityModule] OFF;
End