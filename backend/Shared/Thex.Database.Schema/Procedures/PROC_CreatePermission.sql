﻿CREATE PROCEDURE dbo.PROC_CreatePermission
(
	@identityPermissionId UNIQUEIDENTIFIER,
	@identityProductId int,
	@identityModuleId int,
	@PermissionNamePtBr varchar(150),
	@PermissionNamePtPt varchar(150),
	@PermissionNameEnUs varchar(150),
	@PermissionNameEsAr varchar(150),
	@permissionKeyList varchar(MAX)
)
AS
Begin

	Declare @permissionKey varchar(200);

	Declare @PermissionList_Cursor as CURSOR;

	BEGIN TRANSACTION;

	 BEGIN TRY
	 
		if(not exists(select 1 from [IdentityPermission] where [IdentityPermissionId] = @identityPermissionId))
			INSERT INTO [dbo].[IdentityPermission]
					([IdentityPermissionId],[IdentityProductId],[IdentityModuleId]
					,[Name],[Description]
					,[IsActive],[IsDeleted],[CreationTime],[CreatorUserId],[LastModificationTime],[LastModifierUserId],[DeletionTime],[DeleterUserId])
				VALUES
					(@identityPermissionId,@identityProductId,@identityModuleId
					,@PermissionNameEnUs,''
					,1,0,GETDATE(),NULL,GETDATE(),NULL,NULL,NULL);
		ELSE
			UPDATE [dbo].[IdentityPermission] SET 
				[Name] = @PermissionNameEnUs
			WHERE [IdentityPermissionId] = @identityPermissionId;


		SET @PermissionList_Cursor = CURSOR FOR
		SELECT * FROM dbo.splitstring(@permissionKeyList);
		OPEN @PermissionList_Cursor; 

		FETCH NEXT FROM @PermissionList_Cursor INTO @permissionKey;  
		WHILE @@FETCH_STATUS = 0  
		   BEGIN  

		   if(not exists(
				select 1 from [IdentityPermissionFeature] ipf 
				join IdentityFeature ife on ife.IdentityFeatureId = ipf.IdentityFeatureId 
				where ife.[Key] = @permissionKey and ipf.[IdentityPermissionId] = @identityPermissionId))
					INSERT INTO [dbo].[IdentityPermissionFeature]
					   ([IdentityPermissionFeatureId],[IdentityPermissionId],[IdentityFeatureId]
					   ,[IsActive],[IsDeleted],[CreationTime],[CreatorUserId],[LastModificationTime],[LastModifierUserId],[DeletionTime],[DeleterUserId])
					SELECT newid(),@identityPermissionId,F.IdentityFeatureId
					   ,1,0,GETDATE(),NULL,GETDATE(),NULL,NULL,NULL
					FROM IdentityFeature F
					WHERE F.[Key] = @permissionKey;

			  FETCH NEXT FROM @PermissionList_Cursor INTO @permissionKey;  
		   END;  
		CLOSE @PermissionList_Cursor;  
		DEALLOCATE @PermissionList_Cursor; 

		--TRANSLATIONS
		 if(not exists(select 1 from [IdentityTranslation] where [LanguageIsoCode] = 'pt-BR' and [IdentityOwnerId] = @identityPermissionId))
			 INSERT INTO [dbo].[IdentityTranslation]
					   ([IdentityTranslationId],[LanguageIsoCode],[Name],[Description],[IdentityOwnerId])
				 VALUES
					   (NEWID(),'pt-BR',@PermissionNamePtBr,NULL,@identityPermissionId)
		 ELSE
			 UPDATE [IdentityTranslation] SET [Name] = @PermissionNamePtBr
					WHERE [LanguageIsoCode] = 'pt-BR' and [IdentityOwnerId] = @identityPermissionId

		if(not exists(select 1 from [IdentityTranslation] where [LanguageIsoCode] = 'pt-PT' and [IdentityOwnerId] = @identityPermissionId))
			 INSERT INTO [dbo].[IdentityTranslation]
					   ([IdentityTranslationId],[LanguageIsoCode],[Name],[Description],[IdentityOwnerId])
				 VALUES
					   (NEWID(),'pt-PT',@PermissionNamePtPt,NULL,@identityPermissionId)
		ELSE
			 UPDATE [IdentityTranslation] SET [Name] = @PermissionNamePtPt
					WHERE [LanguageIsoCode] = 'pt-PT' and [IdentityOwnerId] = @identityPermissionId
			   	
		 if(not exists(select 1 from [IdentityTranslation] where [LanguageIsoCode] = 'es-AR' and [IdentityOwnerId] = @identityPermissionId))
			INSERT INTO [dbo].[IdentityTranslation]
				   ([IdentityTranslationId],[LanguageIsoCode],[Name],[Description],[IdentityOwnerId])
			 VALUES
				   (NEWID(),'es-AR',@PermissionNameEsAr,NULL,@identityPermissionId)
		 ELSE
			 UPDATE [IdentityTranslation] SET [Name] = @PermissionNameEsAr
					WHERE [LanguageIsoCode] = 'es-AR' and [IdentityOwnerId] = @identityPermissionId

		 if(not exists(select 1 from [IdentityTranslation] where [LanguageIsoCode] = 'en-US' and [IdentityOwnerId] = @identityPermissionId))
			INSERT INTO [dbo].[IdentityTranslation]
					   ([IdentityTranslationId],[LanguageIsoCode],[Name],[Description],[IdentityOwnerId])
				 VALUES
					   (NEWID(),'en-US',@PermissionNameEnUs,NULL,@identityPermissionId);
		 ELSE
			 UPDATE [IdentityTranslation] SET [Name] = @PermissionNameEnUs
					WHERE [LanguageIsoCode] = 'en-US' and [IdentityOwnerId] = @identityPermissionId
		
		COMMIT TRANSACTION

	END TRY
    BEGIN CATCH
        IF @@TRANCOUNT > 0
        BEGIN
            ROLLBACK TRANSACTION
        END
    END CATCH
End