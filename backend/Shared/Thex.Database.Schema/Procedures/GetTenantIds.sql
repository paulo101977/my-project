﻿CREATE PROCEDURE [dbo].[GetTenantIds]
	@MainTenantId UNIQUEIDENTIFIER
AS
BEGIN
	WITH Tenantlist AS
	(
	SELECT [TenantId] AS Id
	  FROM [dbo].[Tenant]
	  WHERE [IsActive] = 1 AND [TenantId] = @MainTenantId
	UNION ALL
	SELECT TN.[TenantId] AS Id
	  FROM [dbo].[Tenant] AS TN
		   INNER JOIN TenantList AS TL
		   ON TN.ParentId = TL.Id
	  WHERE TN.[IsActive] = 1 AND TN.[ParentId] IS NOT NULL
	) SELECT * FROM TenantList;
END;