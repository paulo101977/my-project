﻿CREATE PROCEDURE [dbo].[PROC_CreateProduct]
(
	@identityProductId int,
	@identityProductName varchar(200)
)
WITH EXECUTE AS OWNER
AS
Begin
	SET IDENTITY_INSERT [dbo].[IdentityProduct] ON;

	if(not exists(select 1 from IdentityProduct where IdentityProductId = @identityProductId))
		INSERT INTO [dbo].[IdentityProduct]
           ([IdentityProductId],[Name],[Description],[IsDeleted],[CreationTime],[CreatorUserId],[LastModificationTime],[LastModifierUserId],[DeletionTime],[DeleterUserId])
     VALUES
           (@identityProductId, @identityProductName, NULL, 0, GETDATE(), NULL, GETDATE(), NULL, NULL, NULL);
	else
		update [IdentityProduct] set [Name] = @identityProductName where IdentityProductId = @identityProductId;

	SET IDENTITY_INSERT [dbo].[IdentityProduct] OFF;
End