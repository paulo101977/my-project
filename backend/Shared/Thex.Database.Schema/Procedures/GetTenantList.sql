﻿CREATE PROCEDURE [dbo].[GetTenantList]
	@MainTenantId UNIQUEIDENTIFIER
AS
BEGIN
	WITH Tenantlist AS
	(
	SELECT [TenantId]
		  ,[TenantName]
		  ,[ParentId]
		  ,[IsActive]
		  ,1 AS Level
	  FROM [dbo].[Tenant]
	  WHERE [TenantId] = @MainTenantId
	UNION ALL
	SELECT TN.[TenantId]
		  ,TN.[TenantName]
		  ,TN.[ParentId]
		  ,TN.[IsActive]
		  ,TL.Level + 1 AS Level
	  FROM [dbo].[Tenant] AS TN
		   INNER JOIN TenantList AS TL
		   ON TN.ParentId = TL.TenantId
	  WHERE TN.[ParentId] IS NOT NULL
	) SELECT * FROM TenantList;
END;