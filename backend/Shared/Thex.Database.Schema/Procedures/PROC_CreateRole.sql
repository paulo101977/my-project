﻿
CREATE PROCEDURE [dbo].[PROC_Role]
(
	@roleId UNIQUEIDENTIFIER,
	@RoleNamePtBr varchar(150),
	@RoleNamePtPt varchar(150),
	@RoleNameEnUs varchar(150),
	@RoleNameEsAr varchar(150),
	@identityProductId int,
	@permissionIdList varchar(MAX)
)
AS
Begin

	Declare @permissionId varchar(200);
	Declare @PermissionList_Cursor as CURSOR;

	BEGIN TRANSACTION;

	 BEGIN TRY

	 
	if(not exists(select 1 from [dbo].[Roles] where [Id] = @roleId))
		INSERT INTO [dbo].[Roles]
				   ([Id],[Name],[NormalizedName],[ConcurrencyStamp],[IdentityProductId]
				   ,[IsActive],[IsDeleted],[CreationTime],[CreatorUserId],[LastModificationTime],[LastModifierUserId],[DeletionTime],[DeleterUserId])
			 VALUES
				   (@roleId,@RoleNameEnUs,UPPER(@RoleNameEnUs),@roleId,@identityProductId
				   ,1,0,GETDATE(),NULL,GETDATE(),NULL,NULL,NULL)
	ELSE
			UPDATE [dbo].[Roles] SET 
				[Name] = @RoleNameEnUs, [NormalizedName] = UPPER(@RoleNameEnUs)
			WHERE [Id] = @roleId;


		SET @PermissionList_Cursor = CURSOR FOR
		SELECT * FROM dbo.splitstring(@permissionIdList);
		OPEN @PermissionList_Cursor; 

		FETCH NEXT FROM @PermissionList_Cursor INTO @permissionId;  
		WHILE @@FETCH_STATUS = 0  
		   BEGIN  
				
			if(not exists(
				select 1 from [IdentityRolePermission] 
				where [IdentityPermissionId] = @permissionId and [RoleId] = @roleId))
					INSERT INTO [dbo].[IdentityRolePermission]
				   ([IdentityRolePermissionId],[IdentityPermissionId],[RoleId]
				   ,[IsActive],[IsDeleted],[CreationTime],[CreatorUserId],[LastModificationTime],[LastModifierUserId],[DeletionTime],[DeleterUserId])
					 VALUES
						   (NEWID(),@permissionId,@roleId
						   ,1,0,GETDATE(),NULL,GETDATE(),NULL,NULL,NULL)


			  FETCH NEXT FROM @PermissionList_Cursor INTO @permissionId;  
		   END;  
		CLOSE @PermissionList_Cursor;  
		DEALLOCATE @PermissionList_Cursor; 

		--TRANSLATIONS
		if(not exists(select 1 from [IdentityTranslation] where [LanguageIsoCode] = 'pt-BR' and [IdentityOwnerId] = @roleId))
			INSERT INTO [dbo].[IdentityTranslation]
				   ([IdentityTranslationId],[LanguageIsoCode],[Name],[Description],[IdentityOwnerId])
			 VALUES
				   (NEWID(),'pt-BR',@RoleNamePtBr,NULL,@roleId)
		  ELSE
			UPDATE [IdentityTranslation] SET [Name] = @RoleNamePtBr
				WHERE [LanguageIsoCode] = 'pt-BR' and [IdentityOwnerId] = @roleId
			   
		  if(not exists(select 1 from [IdentityTranslation] where [LanguageIsoCode] = 'pt-PT' and [IdentityOwnerId] = @roleId))
			INSERT INTO [dbo].[IdentityTranslation]
					   ([IdentityTranslationId],[LanguageIsoCode],[Name],[Description],[IdentityOwnerId])
				 VALUES
					   (NEWID(),'pt-PT',@RoleNamePtPt,NULL,@roleId)
		  ELSE
			 UPDATE [IdentityTranslation] SET [Name] = @RoleNamePtPt
					WHERE [LanguageIsoCode] = 'pt-PT' and [IdentityOwnerId] = @roleId
			   	
		  if(not exists(select 1 from [IdentityTranslation] where [LanguageIsoCode] = 'es-AR' and [IdentityOwnerId] = @roleId))
			INSERT INTO [dbo].[IdentityTranslation]
				   ([IdentityTranslationId],[LanguageIsoCode],[Name],[Description],[IdentityOwnerId])
			 VALUES
				   (NEWID(),'es-AR',@RoleNameEsAr,NULL,@roleId)
		  ELSE
			 UPDATE [IdentityTranslation] SET [Name] = @RoleNameEsAr
					WHERE [LanguageIsoCode] = 'es-AR' and [IdentityOwnerId] = @roleId
			   	
		  if(not exists(select 1 from [IdentityTranslation] where [LanguageIsoCode] = 'en-US' and [IdentityOwnerId] = @roleId))
			INSERT INTO [dbo].[IdentityTranslation]
				   ([IdentityTranslationId],[LanguageIsoCode],[Name],[Description],[IdentityOwnerId])
			 VALUES
				   (NEWID(),'en-US',@RoleNameEnUs,NULL,@roleId)
		  ELSE
			 UPDATE [IdentityTranslation] SET [Name] = @RoleNameEnUs
					WHERE [LanguageIsoCode] = 'en-US' and [IdentityOwnerId] = @roleId
		
		COMMIT TRANSACTION

	END TRY
    BEGIN CATCH
        IF @@TRANCOUNT > 0
        BEGIN
            ROLLBACK TRANSACTION
        END
    END CATCH
END
