﻿CREATE PROCEDURE dbo.PROC_CreateFeature
(
	@featureId UNIQUEIDENTIFIER,
	@identityProductId int,
	@identityModuleId int,
	@featureKey varchar(200)
)
AS
Begin
	
	if(not exists(select 1 from IdentityFeature where IdentityFeatureId = @featureId))
	  INSERT [dbo].[IdentityFeature] 
	([IdentityFeatureId], [IdentityProductId], [IdentityModuleId], [Key], 
	 [Name], [Description], [IsInternal], 
	 [IsDeleted], [CreationTime], [CreatorUserId], [LastModificationTime], [LastModifierUserId], [DeletionTime], [DeleterUserId]) VALUES 
	(@featureId, @identityProductId, @identityModuleId, @featureKey, 
	 @featureKey, N'backend', 1, 
	 0, GETDATE(), NULL, GETDATE(), NULL, NULL, NULL)
End