﻿CREATE PROCEDURE [dbo].[CreateTestReservations]
	@DateRef as DATETIME
AS
BEGIN
	SET IDENTITY_INSERT [dbo].[Reservation] ON
	INSERT 
	[dbo].[Reservation] (
	[ReservationId], [ReservationUid], [ReservationCode], [ReservationVendorCode], [PropertyId],
	[ContactName], [ContactEmail], [ContactPhone], [InternalComments], [ExternalComments], 
	[GroupName], [UnifyAccounts], [IsDeleted],[CreationTime],[CreatorUserId], 
	[LastModificationTime], [LastModifierUserId], [DeletionTime], [DeleterUserId],[Deadline],
	 [BusinessSourceId], [MarketSegmentId], [CompanyClientId]) 
	VALUES 
	(2, N'576ca054-8570-494f-bbe0-8d44b57ea434', N'BR-20170814-XSDSDSDFRZX2', N'00002', 1, 
		N'Saulo', N'saulo.brito@email.com', N'11111111', N'', N'', 
		N'grTotvs', 1, 0, CAST(N'2017-08-14T20:07:37.210' AS DateTime), NULL, 
		NULL, NULL, NULL, NULL, @DateRef,
		1,1, N'6a27e16a-f227-4256-a62f-96e7b39d0658'),
	(3, N'576ca054-8570-494f-bbe0-8d44b57ea435', N'BR-20170814-XSDSDSDFRZX3', N'00002', 1,
		N'Brito', N'brito@email.com', N'11111111', N'', N'', 
		N'grTotvs', 1, 0, CAST(N'2017-08-14T20:07:37.210' AS DateTime), NULL, 
		NULL, NULL, NULL, NULL, NULL,
		1,1, N'6a27e16a-f227-4256-a62f-96e7b39d0658')
	SET IDENTITY_INSERT [dbo].[Reservation] OFF

	SET IDENTITY_INSERT [dbo].[ReservationItem] ON

	declare @DateTimeRef datetime;
	set @DateTimeRef = DATEADD(MINUTE, 20*60, CAST(@DateRef AS DATETIME));

	INSERT [dbo].[ReservationItem] (
		[ReservationItemId], [ReservationItemUid], [ReservationId], [EstimatedArrivalDate], [CheckInDate],
		[EstimatedDepartureDate], [CheckOutDate], [ReservationItemCode], [RequestedRoomTypeId], [ReceivedRoomTypeId], 
		[RoomId], [AdultCount], [ChildCount], [ReservationItemStatusId], [RoomLayoutId], 
		[ExtraBedCount], [ExtraCribCount], [IsDeleted], [CreationTime], [CreatorUserId], 
		[LastModificationTime], [LastModifierUserId], [DeletionTime], [DeleterUserId],  [CancellationDate]) 
	VALUES 
	--RESERVA 2
	(2, N'59B04332-0D58-4F83-B075-ABD8C8728BE1', 2, DATEADD(DAY, -3, @DateRef), NULL, 
	@DateRef, NULL, N'0002', 1, 1, 
	3, 2, 0, 0, N'74BC15D2-25CB-44E9-B43C-036FE8E5207E', 
	0, 0, 0, CAST(N'2017-11-17T20:07:37.203' AS DateTime), NULL, 
	NULL, NULL, NULL, NULL, NULL),

	(3, N'22CAA745-7C11-4D02-B806-F6E9738CDBA9', 2, DATEADD(DAY, -3, @DateRef), DATEADD(DAY, -3, @DateRef), 
	@DateRef, @DateRef, N'0003', 1, 1, 
	5, 2, 0, 6, N'74BC15D2-25CB-44E9-B43C-036FE8E5207E', 
	0, 0, 0, CAST(N'2017-08-17T20:07:37.203' AS DateTime), NULL, 
	NULL, NULL, NULL, NULL, NULL),

	(4, N'528B50DC-8D02-47B5-8922-43665B0C76F7', 2, DATEADD(DAY, -1, @DateRef), DATEADD(DAY, -1, @DateTimeRef), 
	@DateRef, GETUTCDATE(), N'0004', 1, 1, 
	11, 3, 0, 2, N'74BC15D2-25CB-44E9-B43C-036FE8E5207E', 
	0, 0, 0, CAST(N'2017-08-17T20:07:37.203' AS DateTime), NULL, 
	NULL, NULL, NULL, NULL, NULL),

	--RESERVA 3
	(5, N'572F6C3F-9198-47A1-A4AD-6F221C4ED336', 3, @DateRef, @DateTimeRef, 
	DATEADD(DAY, 4, @DateRef), DATEADD(DAY, 4, @DateTimeRef), N'0005', 1, 1, 
	4, 1, 1, 3, N'74BC15D2-25CB-44E9-B43C-036FE8E5207E', 
	0, 0, 0, CAST(N'2017-08-17T20:07:37.203' AS DateTime), NULL, 
	NULL, NULL, NULL, NULL, @DateRef),

	(6, N'839F2316-F9B0-4DE8-88B5-EF6F2D01060B', 3, DATEADD(DAY, -3, @DateRef), NULL, 
	DATEADD(DAY, 4, @DateRef), NULL, N'0006', 1, 1, 
	6, 1, 0, 1, N'74BC15D2-25CB-44E9-B43C-036FE8E5207E', 
	0, 0, 0, CAST(N'2017-08-17T20:07:37.203' AS DateTime), NULL, 
	NULL, NULL, NULL, NULL, @DateRef),

	(7, N'CC82DDF8-E56C-4A8B-A1E5-07B012DBD5DF', 3, DATEADD(DAY, 3, @DateRef), DATEADD(DAY, 3, @DateRef), 
	DATEADD(DAY, 4, @DateRef), DATEADD(DAY, 4, @DateTimeRef), N'0007', 1, 1, 
	12, 1, 0, 4, N'74BC15D2-25CB-44E9-B43C-036FE8E5207E', 
	0, 0, 0, CAST(N'2017-08-17T20:07:37.203' AS DateTime), NULL, 
	NULL, NULL, NULL, NULL, NULL)

	SET IDENTITY_INSERT [dbo].[ReservationItem] OFF

	SET IDENTITY_INSERT [dbo].[GuestReservationItem] ON
	INSERT [dbo].[GuestReservationItem] 
	([GuestReservationItemId], [ReservationItemId], [GuestId], [GuestName], [GuestEmail], 
	[GuestDocument], [EstimatedArrivalDate], [CheckInDate], [EstimatedDepartureDate], [CheckOutDate], 
	[GuestStatusId], [GuestTypeId], [PreferredName], [GuestCitizenship], [GuestLanguage], 
	[PctDailyRate], [IsIncognito], [IsChild], [ChildAge], [IsMain]) VALUES 
	(2, 1, NULL, N'Hudson', N'hudson@email.com.br', 
	N'111111111111', CAST(N'2017-08-15' AS Date), NULL, CAST(N'2017-08-18' AS Date), NULL, 
	1, 1, N'Hudson', N'Brasileira', N'Portugues', 
	0, 0, 0, 0, 1),

	(3, 1, NULL, N'Saulo', N'saulo@email.com.br', 
	N'111111111111', CAST(N'2017-08-15' AS Date), NULL, CAST(N'2017-08-18' AS Date), NULL, 
	1, 1, N'Saulo', N'Brasileira', N'Portugues', 
	0, 1, 0, 0, 0),

	(4, 2, 2, N'Ricardo', N'ricardo@email.com.br', 
	N'111111111111', DATEADD(DAY, -3, @DateRef) , NULL, @DateRef, NULL, 
	1, 1, N'Ricardo', N'Brasileira', N'Portugues', 
	100, 0, 0, 0, 1),

	(5, 2, 3, N'Marcelo', N'marcelo@email.com.br', 
	N'111111111111', DATEADD(DAY, -3, @DateRef) , NULL, @DateTimeRef, NULL, 
	1, 1, N'Marcelo', N'Brasileira', N'Portugues', 
	0, 0, 0, 0, 0),

	(6, 3, NULL, N'Arthur', N'arthur@email.com.br', 
	N'111111111111', DATEADD(DAY, -3, @DateRef), NULL, @DateRef, NULL, 
	1, 1, N'Arthur', N'Brasileira', N'Portugues', 
	100, 1, 0, 0, 1),

	(7, 3, NULL, N'Yoseph', N'yoseph@email.com.br', 
	N'111111111111', DATEADD(DAY, 3, @DateRef), NULL, @DateRef, NULL, 
	1, 1, N'Yoseph', N'Brasileira', N'Portugues', 
	0, 1, 0, 0, 0),

	(8, 4, NULL, N'Caio', N'caio@email.com.br', N'111111111111', 
	DATEADD(DAY, -1, @DateRef), DATEADD(DAY, -1, @DateRef), @DateRef, GETUTCDATE(), 
	1, 1, N'Caio', N'Brasileira', N'Portugues', 
	100, 0, 0, 0, 1),

	(9, 4, NULL, N'Fabio', N'fabio@email.com.br', N'111111111111', 
	DATEADD(DAY, -1, @DateRef), NULL, @DateRef, NULL, 
	1, 1, N'Fabio', N'Brasileira', N'Portugues', 
	0, 0, 0, 0, 0),

	(10, 4, NULL, N'Amanda', N'amanda@email.com.br', N'111111111111', 
	DATEADD(DAY, -1, @DateRef), NULL, @DateRef, NULL, 
	1, 1, N'Amanda', N'Brasileira', N'Portugues', 
	0, 0, 0, 0, 0),

	(11, 5, NULL, N'Carol', N'carol@email.com.br', N'111111111111', 
	@DateRef, NULL, DATEADD(DAY, 4, @DateRef), NULL, 
	1, 1, N'Carol', N'Brasileira', N'Portugues', 
	100, 0, 0, 0, 1),

	(12, 5, NULL, N'Vanessa', N'vanessa@email.com.br', N'111111111111', 
	@DateRef, NULL, DATEADD(DAY, 4, @DateRef), NULL, 
	1, 1, N'Vanessa', N'Brasileira', N'Portugues', 
	0, 0, 1, 10, 0),

	(13, 6, NULL, N'Julia', N'julia@email.com.br', N'111111111111', 
	DATEADD(DAY, -3, @DateRef), NULL, DATEADD(DAY, 4, @DateRef), NULL, 
	1, 1, N'Julia', N'Brasileira', N'Portugues', 
	100, 0, 0, 0, 1),

	(14, 7, NULL, N'Isabela', N'isabela@email.com.br', N'111111111111', 
	DATEADD(DAY, 3, @DateRef), NULL, DATEADD(DAY, 4, @DateRef), NULL, 
	1, 1, N'Isabela', N'Brasileira', N'Portugues', 
	100, 0, 0, 0, 1)
	
	SET IDENTITY_INSERT [dbo].[GuestReservationItem] OFF

	SET IDENTITY_INSERT [dbo].[Plastic] ON 
	INSERT [dbo].[Plastic] ([PlasticId], [PersonId], [Holder], [PlasticNumber], [ExpirationDate], [CSC], [PlasticBrandId]) VALUES (1, NULL, N'USUARIO TESTE', 1234, N'05/2018', 1, 1);
	SET IDENTITY_INSERT [dbo].[Plastic] OFF
	
	INSERT [dbo].[ReservationConfirmation] ([ReservationConfirmationId], [ReservationId], [PaymentTypeId], [PlasticId], [BillingClientId], [EnsuresNoShow], [Value], [Deadline]) VALUES (N'576ca054-8570-494f-bbe0-8d44b57ea477', 1, 4, NULL, NULL, 1, NULL, NULL);
	INSERT [dbo].[ReservationConfirmation] ([ReservationConfirmationId], [ReservationId], [PaymentTypeId], [PlasticId], [BillingClientId], [EnsuresNoShow], [Value], [Deadline]) VALUES (N'576ca054-8570-494f-bbe0-8d44b57ea488', 2, 5, 1, NULL, 0, CAST(100 AS Numeric(18, 0)), CAST(N'2017-11-24' AS Date));
	INSERT [dbo].[ReservationConfirmation] ([ReservationConfirmationId], [ReservationId], [PaymentTypeId], [PlasticId], [BillingClientId], [EnsuresNoShow], [Value], [Deadline]) VALUES (N'576ca054-8570-494f-bbe0-8d44b57ea499', 3, 6, NULL, N'6a27e16a-f227-4256-a62f-96e7b39d0658', 1, NULL, NULL);

END;