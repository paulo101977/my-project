﻿CREATE PROCEDURE dbo.PROC_CreatePublicFeature
(
	@featureId UNIQUEIDENTIFIER,
	@identityProductId int,
	@identityModuleId int,
	@featureKey varchar(200),
	@PermissionNamePtBr varchar(150),
	@PermissionNamePtPt varchar(150),
	@PermissionNameEnUs varchar(150),
	@PermissionNameEsAr varchar(150)
)
AS
Begin

	BEGIN TRANSACTION;

	 BEGIN TRY

	 
	if(not exists(select 1 from IdentityFeature where IdentityFeatureId = @featureId))
		 INSERT [dbo].[IdentityFeature] 
		([IdentityFeatureId], [IdentityProductId], [IdentityModuleId], [Key], 
		 [Name], [Description], [IsInternal], 
		 [IsDeleted], [CreationTime], [CreatorUserId], [LastModificationTime], [LastModifierUserId], [DeletionTime], [DeleterUserId]) VALUES 
		(@featureId, @identityProductId, @identityModuleId, @featureKey, 
		 @PermissionNameEnUs, N'menu', 0, 
		 0, GETDATE(), NULL, GETDATE(), NULL, NULL, NULL)
	ELSE
			UPDATE [dbo].[IdentityFeature] SET 
				[Name] = @PermissionNameEnUs
			WHERE IdentityFeatureId = @featureId;

	--TRANSLATIONS
	 if(not exists(select 1 from [IdentityTranslation] where [LanguageIsoCode] = 'pt-BR' and [IdentityOwnerId] = @featureId))
		INSERT INTO [dbo].[IdentityTranslation]
			   ([IdentityTranslationId],[LanguageIsoCode],[Name],[Description],[IdentityOwnerId])
		 VALUES
			   (NEWID(),'pt-BR',@PermissionNamePtBr,NULL,@featureId)
	  ELSE
			 UPDATE [IdentityTranslation] SET [Name] = @PermissionNamePtBr
					WHERE [LanguageIsoCode] = 'pt-BR' and [IdentityOwnerId] = @featureId

	 if(not exists(select 1 from [IdentityTranslation] where [LanguageIsoCode] = 'pt-PT' and [IdentityOwnerId] = @featureId))
		INSERT INTO [dbo].[IdentityTranslation]
			   ([IdentityTranslationId],[LanguageIsoCode],[Name],[Description],[IdentityOwnerId])
		 VALUES
			   (NEWID(),'pt-PT',@PermissionNamePtPt,NULL,@featureId)
	ELSE
		UPDATE [IdentityTranslation] SET [Name] = @PermissionNamePtPt
			WHERE [LanguageIsoCode] = 'pt-PT' and [IdentityOwnerId] = @featureId
					
	 if(not exists(select 1 from [IdentityTranslation] where [LanguageIsoCode] = 'es-AR' and [IdentityOwnerId] = @featureId))
		INSERT INTO [dbo].[IdentityTranslation]
			   ([IdentityTranslationId],[LanguageIsoCode],[Name],[Description],[IdentityOwnerId])
		 VALUES
			   (NEWID(),'es-AR',@PermissionNameEsAr,NULL,@featureId)
	ELSE
		UPDATE [IdentityTranslation] SET [Name] = @PermissionNameEsAr
			WHERE [LanguageIsoCode] = 'es-AR' and [IdentityOwnerId] = @featureId
			   	
	 if(not exists(select 1 from [IdentityTranslation] where [LanguageIsoCode] = 'en-US' and [IdentityOwnerId] = @featureId))
		INSERT INTO [dbo].[IdentityTranslation]
			   ([IdentityTranslationId],[LanguageIsoCode],[Name],[Description],[IdentityOwnerId])
		 VALUES
			   (NEWID(),'en-US',@PermissionNameEnUs,NULL,@featureId)
	ELSE
		UPDATE [IdentityTranslation] SET [Name] = @PermissionNameEnUs
			WHERE [LanguageIsoCode] = 'en-US' and [IdentityOwnerId] = @featureId
			   
		COMMIT TRANSACTION

	END TRY
    BEGIN CATCH
        IF @@TRANCOUNT > 0
        BEGIN
            ROLLBACK TRANSACTION
        END
    END CATCH
End