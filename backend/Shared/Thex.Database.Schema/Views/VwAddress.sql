﻿CREATE VIEW [dbo].[VWADDRESS]
AS
SELECT loc.OwnerId,
       loc.StreetName,
       loc.StreetNumber,
       loc.AdditionalAddressDetails,
       loc.Neighborhood,
       loc.PostalCode,
       loc.LocationCategoryId,
       cityt.Name AS cityname,
       cityt.LanguageIsoCode,
       city.ExternalCode,
       city.CountrySubdivisionId AS CityId,
       sta.TwoLetterIsoCode AS isocodelevel2,
       stat.Name AS statename,
       stat.CountrySubdivisionId AS StateID,
       ctry.CountrySubdivisionId AS CountryId,
       ctryt.Name AS CountryName,
       ctry.CountryTwoLetterIsoCode,
       ctryt.Nationality,
       loc.latitude,
       loc.longitude
FROM LOCATION loc 
INNER JOIN
	CountrySubdivision ctry
INNER JOIN CountrySubdivisionTranslation ctryt 
	ON ctry.CountrySubdivisionId=ctryt.CountrySubdivisionId
	AND ctry.SubdivisionTypeId=1
INNER JOIN CountrySubdivision sta
	INNER JOIN CountrySubdivisionTranslation stat 
		ON sta.CountrySubdivisionId=stat.CountrySubdivisionId
		AND sta.SubdivisionTypeId=2 
	ON ctry.CountrySubdivisionId=sta.ParentSubdivisionId 
	AND ctryt.LanguageIsoCode=stat.LanguageIsoCode
LEFT JOIN CountrySubdivision city
	INNER JOIN CountrySubdivisionTranslation cityt 
		ON city.CountrySubdivisionId=cityt.CountrySubdivisionId
		AND city.SubdivisionTypeId=3 
	ON stat.CountrySubdivisionId=city.ParentSubdivisionId
	AND cityt.LanguageIsoCode = stat.LanguageIsoCode
	ON loc.CityId=coalesce(city.CountrySubdivisionId,stat.CountrySubdivisionId)
	AND loc.CountryCode=ctry.CountryTwoLetterIsoCode
GO