﻿CREATE VIEW [dbo].[VWGUEST]
AS
SELECT
  COALESCE(GI.GuestName, GR.FirstName) as FisrtName,
	GR.LastName,
  GR.FullName,
	ADDRH.STREETNAME,
	ADDRH.CITYNAME,
	ADDRH.POSTALCODE,
	ADDRH.COUNTRYNAME,
	ADDRH.CountryTwoLetterIsoCode,
  ADDRH.isocodelevel2 AS STATE,
	ADDRH.Neighborhood,
	ADDRH.AdditionalAddressDetails,
	ADDRH.LocationCategoryId,
	COALESCE(GI.GuestEmail,EMAIL.Information) AS EMAIL,
	RS.InternalComments, 
	RS.ExternalComments,
	room.RoomNumber,
	ADDRH.CountryId,
	RI.ReservationItemStatusId,
	COALESCE(DG.DocumentInformation, GI.GuestDocument ) AS Document,
	DGT.Name AS DocumentTypeName,
	GR.Gender,
	GT.GuestTypeName,
	GT.PropertyGuestTypeId,
	GT.IsVIP,
	GR.BirthDate,
	GI.IsChild,
	GI.IsMain,
	GI.IsIncognito,
	COALESCE(ADDRH.Nationality, GI.GuestCitizenship) AS Nationality,
	RI.ReservationItemCode AS ReservationCode, 
	ADDRH.Latitude,
	ADDRH.Longitude,
	RT.RoomTypeId,
	RT.Name AS RoomTypeName,
  GR.GuestRegistrationId,
  GR.ResponsibleGuestRegistrationId,
  GR.GuestId,     
  GI.GuestReservationItemId,
  RI.ReservationItemId,
  RS.ReservationId,
  RS.PropertyId,
  ADDRH.LanguageIsoCode
FROM Property PY
	INNER JOIN
	Reservation RS
	ON PY.PropertyId = RS.PropertyId
	INNER JOIN
	ReservationItem RI
	ON RS.ReservationId = RI.ReservationId
	INNER JOIN
	RoomType RT
	ON RI.ReceivedRoomTypeId=rt.RoomTypeId
	LEFT JOIN
	ROOM 
	ON ROOM.RoomId=RI.RoomId
	LEFT JOIN
	GuestReservationItem GI
		INNER JOIN
		PropertyGuestType GT
		ON GI.GuestTypeId = GT.PropertyGuestTypeId
	ON RI.ReservationItemId=GI.ReservationItemId
	LEFT JOIN
	GuestRegistration GR
	ON GI.GuestRegistrationId = GR.GuestRegistrationId
	LEFT JOIN
	VWADDRESS ADDRH
	ON ADDRH.OwnerId=GR.GuestRegistrationId AND ADDRH.LocationCategoryId=4
	LEFT JOIN
	ContactInformation EMAIL
	ON GI.GuestRegistrationId = EMAIL.OwnerId AND EMAIL.ContactInformationTypeId=1
	LEFT JOIN
	DOCUMENT DG
	ON GR.GuestRegistrationId = DG.OwnerId
	LEFT JOIN
	DocumentType DGT
	ON COALESCE(DG.DocumentTypeId, GI.GuestDocumentTypeId) =DGT.DocumentTypeId
GO