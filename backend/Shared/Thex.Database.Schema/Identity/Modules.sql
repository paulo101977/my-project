﻿Declare @identityModuleId int;
Declare @identityModuleName varchar(200);

BEGIN
	SET  @identityModuleId = 1;
	SET  @identityModuleName = 'Panel';

	EXEC PROC_CreateModule
		@identityModuleId,
		@identityModuleName;
END

BEGIN
	SET  @identityModuleId = 2;
	SET  @identityModuleName = 'Home';

	EXEC PROC_CreateModule
		@identityModuleId,
		@identityModuleName;
END

BEGIN
	SET  @identityModuleId = 3;
	SET  @identityModuleName = 'Hosting';

	EXEC PROC_CreateModule
		@identityModuleId,
		@identityModuleName;
END

BEGIN
	SET  @identityModuleId = 4;
	SET  @identityModuleName = 'Financial';

	EXEC PROC_CreateModule
		@identityModuleId,
		@identityModuleName;
END

BEGIN
	SET  @identityModuleId = 5;
	SET  @identityModuleName = 'Reservation';

	EXEC PROC_CreateModule
		@identityModuleId,
		@identityModuleName;
END

BEGIN
	SET  @identityModuleId = 6;
	SET  @identityModuleName = 'Governance';

	EXEC PROC_CreateModule
		@identityModuleId,
		@identityModuleName;
END

BEGIN
	SET  @identityModuleId = 7;
	SET  @identityModuleName = 'Settings';

	EXEC PROC_CreateModule
		@identityModuleId,
		@identityModuleName;
END

BEGIN
	SET  @identityModuleId = 8;
	SET  @identityModuleName = 'Revenue Management';

	EXEC PROC_CreateModule
		@identityModuleId,
		@identityModuleName;
END

BEGIN
	SET  @identityModuleId = 9;
	SET  @identityModuleName = 'Interactive Report';

	EXEC PROC_CreateModule
		@identityModuleId,
		@identityModuleName;
END

BEGIN
	SET  @identityModuleId = 10;
	SET  @identityModuleName = 'Integration';

	EXEC PROC_CreateModule
		@identityModuleId,
		@identityModuleName;
END

GO