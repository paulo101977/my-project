Declare @identityPermissionId UNIQUEIDENTIFIER;
	
	Declare @PermissionNamePtBr varchar(150);
	Declare @PermissionNamePtPt varchar(150);
	Declare @PermissionNameEnUs varchar(150);
	Declare @PermissionNameEsAr varchar(150);

	Declare @identityProductId int;
	Declare @identityModuleId int;
	
	Declare @permissionKeyList varchar(MAX);

-- Settings - View, Register, Edit, Delete and Activate UH
BEGIN
	SET @identityPermissionId = N'2d97ee64-7c09-422d-b012-cef97b675341';
	
	SET @PermissionNamePtBr = 'Editar UH';
	SET @PermissionNamePtPt = 'Editar UH';
	SET @PermissionNameEnUs = 'Edit UH';
	SET @PermissionNameEsAr = 'Editar HAB';

	SET @identityProductId = 1;
	SET @identityModuleId = 7;

	SET @permissionKeyList = 
	'PMS_Room_Put';


	EXEC PROC_CreatePermission
		@identityPermissionId,
		@identityProductId,
		@identityModuleId,
		@PermissionNamePtBr,
		@PermissionNamePtPt,
		@PermissionNameEnUs,
		@PermissionNameEsAr,
		@permissionKeyList;
END

GO