﻿  Declare @featureId UNIQUEIDENTIFIER;
 Declare @identityProductId int;
 Declare @identityModuleId int;
 Declare @featureKey varchar(200);

 BEGIN
	SET @featureId = N'e099a1e1-ef2e-469f-9b7a-f60f741f1ee8';
	SET @identityProductId = 3;
	SET @identityModuleId = 6;
	SET @featureKey = 'HK_User_Get_GetUserRoomHistory';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END

 BEGIN
	SET @featureId = N'e4c5967b-8b1d-4e5f-890f-0a379490ec52';
	SET @identityProductId = 3;
	SET @identityModuleId = 6;
	SET @featureKey = 'HK_User_Get_GetUserRoomByUserId';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END
GO
 
 