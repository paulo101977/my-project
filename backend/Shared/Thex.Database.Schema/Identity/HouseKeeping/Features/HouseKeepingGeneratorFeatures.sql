﻿ Declare @featureId UNIQUEIDENTIFIER;
 Declare @identityProductId int;
 Declare @identityModuleId int;
 Declare @featureKey varchar(200);

 BEGIN
	SET @featureId = N'2db532a8-0540-4667-8937-d24ec60c73c4';
	SET @identityProductId = 1;
	SET @identityModuleId = 1;
	SET @featureKey = 'HK_HouseKeepingGenerator_Post';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END

 BEGIN
	SET @featureId = N'15b38594-a275-4fed-a668-3bf112b60a8b';
	SET @identityProductId = 1;
	SET @identityModuleId = 1;
	SET @featureKey = 'HK_HouseKeepingGenerator_Post_CreateHouseKeeping';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END
 
  

GO