﻿  Declare @featureId UNIQUEIDENTIFIER;
 Declare @identityProductId int;
 Declare @identityModuleId int;
 Declare @featureKey varchar(200);

 BEGIN
	SET @featureId = N'8db2594e-3fd8-455d-b1d0-9fcece1fcacc';
	SET @identityProductId = 3;
	SET @identityModuleId = 6;
	SET @featureKey = 'HK_HousekeepingRoom_Get_GetAllGrid';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END
GO

 