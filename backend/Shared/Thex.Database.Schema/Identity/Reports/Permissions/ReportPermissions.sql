﻿	Declare @identityPermissionId UNIQUEIDENTIFIER;
	
	Declare @PermissionNamePtBr varchar(150);
	Declare @PermissionNamePtPt varchar(150);
	Declare @PermissionNameEnUs varchar(150);
	Declare @PermissionNameEsAr varchar(150);

	Declare @identityProductId int;
	Declare @identityModuleId int;
	
	Declare @permissionKeyList varchar(MAX);

--INTERACTIVE REPORT

-- Report - Pension Prevision
BEGIN
	SET @identityPermissionId = N'e175c1a6-dbf3-447c-b401-98e2719e004b';
	
	SET @PermissionNamePtBr = 'Previsão de Pensão';
	SET @PermissionNamePtPt = 'Previsão de Pensão';
	SET @PermissionNameEnUs = 'Pension Prevision';
	SET @PermissionNameEsAr = 'Previsión de Pensión';

	SET @identityProductId = 1;
	SET @identityModuleId = 9;

	SET @permissionKeyList = 
	'PMS_Menu_Reports,' +
	'Reports_PensionPrevision_Get';
	

	EXEC PROC_CreatePermission
		@identityPermissionId,
		@identityProductId,
		@identityModuleId,
		@PermissionNamePtBr,
		@PermissionNamePtPt,
		@PermissionNameEnUs,
		@PermissionNameEsAr,
		@permissionKeyList;
END

-- Report - Billing Account Transfer
BEGIN
	SET @identityPermissionId = N'85438618-0619-4e2f-b61b-0a149e49f5b1';
	
	SET @PermissionNamePtBr = 'Transferência entre contas';
	SET @PermissionNamePtPt = 'Transferência entre contas';
	SET @PermissionNameEnUs = 'Account Transfer';
	SET @PermissionNameEsAr = 'Transferencia de Cuenta';

	SET @identityProductId = 1;
	SET @identityModuleId = 9;

	SET @permissionKeyList = 
	'PMS_Menu_Reports,' +
	'Reports_BillingAccountTransfer_Get';
	

	EXEC PROC_CreatePermission
		@identityPermissionId,
		@identityProductId,
		@identityModuleId,
		@PermissionNamePtBr,
		@PermissionNamePtPt,
		@PermissionNameEnUs,
		@PermissionNameEsAr,
		@permissionKeyList;
END

GO