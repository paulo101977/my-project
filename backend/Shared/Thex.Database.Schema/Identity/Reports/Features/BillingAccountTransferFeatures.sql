﻿ Declare @featureId UNIQUEIDENTIFIER;
 Declare @identityProductId int;
 Declare @identityModuleId int;
 Declare @featureKey varchar(200);

 BEGIN
	SET @featureId = N'796eeb3b-300f-46a8-9bab-0bd19a0ca4de';
	SET @identityProductId = 1;
	SET @identityModuleId = 1;
	SET @featureKey = 'Reports_BillingAccountTransfer_Get';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END

GO