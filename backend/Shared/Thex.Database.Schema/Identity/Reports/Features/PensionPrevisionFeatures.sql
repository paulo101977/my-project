﻿ Declare @featureId UNIQUEIDENTIFIER;
 Declare @identityProductId int;
 Declare @identityModuleId int;
 Declare @featureKey varchar(200);

 BEGIN
	SET @featureId = N'cb8333e9-4d2d-476c-9a8c-01a864d04de2';
	SET @identityProductId = 1;
	SET @identityModuleId = 1;
	SET @featureKey = 'Reports_PensionPrevision_Get';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END

GO