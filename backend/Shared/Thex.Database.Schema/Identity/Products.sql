﻿Declare @identityProductId int;
Declare @identityProductName varchar(200);

BEGIN
	SET  @identityProductId = 1;
	SET  @identityProductName = 'PMS';

	EXEC PROC_CreateProduct
		@identityProductId,
		@identityProductName;
END

BEGIN
	SET  @identityProductId = 2;
	SET  @identityProductName = 'PDV';

	EXEC PROC_CreateProduct
		@identityProductId,
		@identityProductName;
END

BEGIN
	SET  @identityProductId = 3;
	SET  @identityProductName = 'HOUSEKEEPING';

	EXEC PROC_CreateProduct
		@identityProductId,
		@identityProductName;
END

BEGIN
	SET  @identityProductId = 4;
	SET  @identityProductName = 'SUPERADM';

	EXEC PROC_CreateProduct
		@identityProductId,
		@identityProductName;
END

BEGIN
	SET  @identityProductId = 5;
	SET  @identityProductName = 'PREFERENCES';

	EXEC PROC_CreateProduct
		@identityProductId,
		@identityProductName;
END

BEGIN
	SET  @identityProductId = 6;
	SET  @identityProductName = 'TRIBUTES';

	EXEC PROC_CreateProduct
		@identityProductId,
		@identityProductName;
END

BEGIN
	SET  @identityProductId = 7;
	SET  @identityProductName = 'HIGSINTEGRATION';

	EXEC PROC_CreateProduct
		@identityProductId,
		@identityProductName;
END

BEGIN
	SET  @identityProductId = 8;
	SET  @identityProductName = 'CENTRAL';

	EXEC PROC_CreateProduct
		@identityProductId,
		@identityProductName;
END


GO
