﻿ Declare @featureId UNIQUEIDENTIFIER;
 Declare @identityProductId int;
 Declare @identityModuleId int;
 Declare @featureKey varchar(200);

 BEGIN
	SET @featureId = N'6ed4ac37-2dcc-4879-b776-66dad51eb906';
	SET @identityProductId = 6;
	SET @identityModuleId = 7;
	SET @featureKey = 'Tax_Get_GetById';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END

 BEGIN
	SET @featureId = N'b758b3a9-e0b4-4db9-a9e1-e3cc34b7f1da';
	SET @identityProductId = 6;
	SET @identityModuleId = 7;
	SET @featureKey = 'Tax_Get_GetAllForProducts';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END

 BEGIN
	SET @featureId = N'32211e86-19f4-4d86-8df1-456c8473a290';
	SET @identityProductId = 6;
	SET @identityModuleId = 7;
	SET @featureKey = 'Tax_Get_GetAllForServices';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END


 BEGIN
	SET @featureId = N'7cfb1236-3ea3-4924-8a53-ae39606ac411';
	SET @identityProductId = 6;
	SET @identityModuleId = 7;
	SET @featureKey = 'Tax_Post_Post';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END

 BEGIN
	SET @featureId = N'98dc5caa-7a72-487c-9d8a-1f1cfeb2a997';
	SET @identityProductId = 6;
	SET @identityModuleId = 7;
	SET @featureKey = 'Tax_Patch_ToggleActivation';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END

 BEGIN
	SET @featureId = N'115f2ca8-7895-435f-bc7c-9695b5d96f87';
	SET @identityProductId = 6;
	SET @identityModuleId = 7;
	SET @featureKey = 'Tax_Get_GetTributes';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END
 
GO
 
 