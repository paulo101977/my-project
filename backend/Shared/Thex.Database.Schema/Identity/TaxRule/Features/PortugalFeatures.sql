﻿ Declare @featureId UNIQUEIDENTIFIER;
 Declare @identityProductId int;
 Declare @identityModuleId int;
 Declare @featureKey varchar(200);

 BEGIN
	SET @featureId = N'f05f6f9b-6199-43e9-b416-c0d9e6f0366c';
	SET @identityProductId = 6;
	SET @identityModuleId = 7;
	SET @featureKey = 'Tax_Get_Iva';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END

BEGIN
	SET @featureId = N'76248e54-3ab1-41b6-aae5-becf054d2863';
	SET @identityProductId = 6;
	SET @identityModuleId = 7;
	SET @featureKey = 'Tax_Get_GetAllTaxRulesServices';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END

BEGIN
	SET @featureId = N'c1e7adb9-3364-4ccb-869d-93cd13406411';
	SET @identityProductId = 6;
	SET @identityModuleId = 7;
	SET @featureKey = 'Tax_Get_GetAllTaxRulesProducts';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END

BEGIN
	SET @featureId = N'04cc4274-43b7-4feb-815d-a0e639ea2b0f';
	SET @identityProductId = 6;
	SET @identityModuleId = 7;
	SET @featureKey = 'Tax_PUT_AssociateServicesAndIva';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END

GO
 
 