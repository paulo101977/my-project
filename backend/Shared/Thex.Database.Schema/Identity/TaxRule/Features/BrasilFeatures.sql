﻿ Declare @featureId UNIQUEIDENTIFIER;
 Declare @identityProductId int;
 Declare @identityModuleId int;
 Declare @featureKey varchar(200);

 BEGIN
	SET @featureId = N'0e4fe3ba-bccc-4e8e-93d6-2da872fdf4d7';
	SET @identityProductId = 6;
	SET @identityModuleId = 7;
	SET @featureKey = 'Tax_Get_Cfop_GetAll';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END

 BEGIN
	SET @featureId = N'c3c4a7ab-d720-4257-b310-299e6e8a53d9';
	SET @identityProductId = 6;
	SET @identityModuleId = 7;
	SET @featureKey = 'Tax_Get_Cfop_GetAllByFilters';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END

 BEGIN
	SET @featureId = N'3c7ddc86-da80-40f9-9849-136daa5a5734';
	SET @identityProductId = 6;
	SET @identityModuleId = 7;
	SET @featureKey = 'Tax_Get_Cfop_GetByCode';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END

 BEGIN
	SET @featureId = N'd4e706f6-2dd3-4ddb-a721-3236e8280f1c';
	SET @identityProductId = 6;
	SET @identityModuleId = 7;
	SET @featureKey = 'Tax_Get_Csosn_GetAll';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END

 BEGIN
	SET @featureId = N'61f66080-bf6c-4202-b0bf-4449becacb5f';
	SET @identityProductId = 6;
	SET @identityModuleId = 7;
	SET @featureKey = 'Tax_Get_Csosn_GetByCode';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END

 BEGIN
	SET @featureId = N'67d60a21-9bd7-4f9c-9594-6b228daf3ab2';
	SET @identityProductId = 6;
	SET @identityModuleId = 7;
	SET @featureKey = 'Tax_Get_Cst_GetAll';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END

 BEGIN
	SET @featureId = N'5340d26c-61c1-40fe-ab6a-bdce6031f7ab';
	SET @identityProductId = 6;
	SET @identityModuleId = 7;
	SET @featureKey = 'Tax_Get_Cst_GetByCode';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END

 BEGIN
	SET @featureId = N'4d1e1dd8-21c6-42ac-8b1b-106a43fab9ff';
	SET @identityProductId = 6;
	SET @identityModuleId = 7;
	SET @featureKey = 'Tax_Get_Cst_GetByType';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END

 BEGIN
	SET @featureId = N'0f8b7592-76c2-4dc5-b8cc-2b503bbb4684';
	SET @identityProductId = 6;
	SET @identityModuleId = 7;
	SET @featureKey = 'Tax_Get_Revenue_GetAll';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END

 BEGIN
	SET @featureId = N'306ce17a-3d4a-4ef5-8a5c-08550413413c';
	SET @identityProductId = 6;
	SET @identityModuleId = 7;
	SET @featureKey = 'Tax_Get_Revenue_GetByCode';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END

 BEGIN
	SET @featureId = N'1b5f00a3-7be5-411b-9fa6-645f575a8c5d';
	SET @identityProductId = 6;
	SET @identityModuleId = 7;
	SET @featureKey = 'Tax_Get_Revenue_GetByNcm';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END

 BEGIN
	SET @featureId = N'483c2d8e-e7d6-4427-ab1b-060ec298ee52';
	SET @identityProductId = 6;
	SET @identityModuleId = 7;
	SET @featureKey = 'Tax_Get_Nbs_GetAll';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END

 BEGIN
	SET @featureId = N'95a4b931-ce0b-428f-85e7-7a08112beb0f';
	SET @identityProductId = 6;
	SET @identityModuleId = 7;
	SET @featureKey = 'Tax_Get_Nbs_GetAllByFilters';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END

 BEGIN
	SET @featureId = N'02983091-fd46-4d50-bf03-7b8c0e22fed6';
	SET @identityProductId = 6;
	SET @identityModuleId = 7;
	SET @featureKey = 'Tax_Get_Nbs_GetByCode';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END

 BEGIN
	SET @featureId = N'8406b819-becc-4a55-9cdd-60bd2c3ae7b3';
	SET @identityProductId = 6;
	SET @identityModuleId = 7;
	SET @featureKey = 'Tax_Get_Ncm_GetAll';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END

 BEGIN
	SET @featureId = N'91d91b72-653d-4505-882d-e42dcc93252d';
	SET @identityProductId = 6;
	SET @identityModuleId = 7;
	SET @featureKey = 'Tax_Get_Ncm_GetAllByFilters';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END

 BEGIN
	SET @featureId = N'6da7f583-4460-40cb-beb7-e6c8d8eed32d';
	SET @identityProductId = 6;
	SET @identityModuleId = 7;
	SET @featureKey = 'Tax_Get_Ncm_GetByCode';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END

 BEGIN
	SET @featureId = N'd9bfe9e4-ca53-4c7f-ac5e-5e8abb0eeb72';
	SET @identityProductId = 6;
	SET @identityModuleId = 7;
	SET @featureKey = 'Tax_Get_TaxCalculation_GetAll';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END

 BEGIN
	SET @featureId = N'fe1f7d0c-76ca-43ae-866f-a85237ba4aa1';
	SET @identityProductId = 6;
	SET @identityModuleId = 7;
	SET @featureKey = 'Tax_Get_TaxCalculation_GetById';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END

 BEGIN
	SET @featureId = N'982ed2b6-eef8-4deb-aab8-0d276746609a';
	SET @identityProductId = 6;
	SET @identityModuleId = 7;
	SET @featureKey = 'Tax_Get_ServiceType_GetAll';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END

 BEGIN
	SET @featureId = N'c715ffcb-74bb-49be-8d26-cdc32e4958fe';
	SET @identityProductId = 6;
	SET @identityModuleId = 7;
	SET @featureKey = 'Tax_Get_ServiceType_GetByCode';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END

 BEGIN
	SET @featureId = N'01ac6ac9-f872-4586-bd88-9896e7ddbb11';
	SET @identityProductId = 6;
	SET @identityModuleId = 7;
	SET @featureKey = 'Tax_Get_UF_GetAll';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END

 BEGIN
	SET @featureId = N'11d07518-115d-42b0-9788-331e44ee3c64';
	SET @identityProductId = 6;
	SET @identityModuleId = 7;
	SET @featureKey = 'Tax_Get_UF_GetByCode';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END

BEGIN
	SET @featureId = N'6c21062d-111d-4c17-8595-ea25ed22317b';
	SET @identityProductId = 6;
	SET @identityModuleId = 7;
	SET @featureKey = 'Tax_Post_Imposto_Retido';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END

BEGIN
	SET @featureId = N'd33c44a8-8656-483b-a27c-162cfa118c71';
	SET @identityProductId = 6;
	SET @identityModuleId = 7;
	SET @featureKey = 'Tax_Get_All_Imposto_Retido';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END

BEGIN
	SET @featureId = N'ac1b68c5-0ab9-4c26-ac9c-94d45870ff61';
	SET @identityProductId = 6;
	SET @identityModuleId = 7;
	SET @featureKey = 'Tax_Delete_Imposto_Retido';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END
GO
 
 