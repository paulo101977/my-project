﻿  Declare @featureId UNIQUEIDENTIFIER;
 Declare @identityProductId int;
 Declare @identityModuleId int;
 Declare @featureKey varchar(200);

 BEGIN
	SET @featureId = N'1e628afb-f7ae-42be-bc87-c4e6e767f646';
	SET @identityProductId = 5;
	SET @identityModuleId = 7;
	SET @featureKey = 'SA_User_Patch_ToggleActivation';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END

 BEGIN
	SET @featureId = N'88b728df-a486-48d5-8311-2af8dad96035';
	SET @identityProductId = 5;
	SET @identityModuleId = 7;
	SET @featureKey = 'SA_User_Patch_Culture';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END

 BEGIN
	SET @featureId = N'75488797-3739-4fcb-a2da-614d388fd48a';
	SET @identityProductId = 5;
	SET @identityModuleId = 7;
	SET @featureKey = 'SA_User_Get';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END
 
 BEGIN
	SET @featureId = N'a3998bbe-65e3-448d-9abf-5c9343001a49';
	SET @identityProductId = 5;
	SET @identityModuleId = 7;
	SET @featureKey = 'SA_User_Post_CreateNewUser';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END

 BEGIN
	SET @featureId = N'590c29f4-6250-4be9-a9f6-db2110fa7b24';
	SET @identityProductId = 1;
	SET @identityModuleId = 7;
	SET @featureKey = 'SA_User_Patch_NewInvitationLink';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
 END

  BEGIN
	SET @featureId = N'3f547f72-8f80-4d09-9e24-878069baa144';
	SET @identityProductId = 1;
	SET @identityModuleId = 7;
	SET @featureKey = 'SA_Get_By_UserId';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
 END

  BEGIN
	SET @featureId = N'be0e79b5-293a-4400-a983-4ab038dd58ab';
	SET @identityProductId = 1;
	SET @identityModuleId = 7;
	SET @featureKey = 'SA_User_Patch_PersonChangePwd';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
 END

   BEGIN
	SET @featureId = N'e1422cf2-461e-461f-b549-66abe32dcf74';
	SET @identityProductId = 1;
	SET @identityModuleId = 7;
	SET @featureKey = 'SA_User_Patch_UpdateUser';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
 END

  BEGIN
	SET @featureId = N'e0c930fa-213e-476b-a12b-dabae6a19232';
	SET @identityProductId = 1;
	SET @identityModuleId = 7;
	SET @featureKey = 'SA_User_Change_Photo';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
 END

GO