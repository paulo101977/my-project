﻿ Declare @featureId UNIQUEIDENTIFIER;
 Declare @identityProductId int;
 Declare @identityModuleId int;
 Declare @featureKey varchar(200);

 BEGIN
	SET @featureId = N'31c559a1-f97e-4d0e-99d1-825eea36d422';
	SET @identityProductId = 2;
	SET @identityModuleId = 7;
	SET @featureKey = 'PDV_Product_Get';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END

 BEGIN
	SET @featureId = N'd3d986a7-e61a-4650-8a18-2670c01cb8fa';
	SET @identityProductId = 2;
	SET @identityModuleId = 7;
	SET @featureKey = 'PDV_Product_Patch_ToggleActivation';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END
 
 BEGIN
	SET @featureId = N'78f238ff-a111-4bb2-b8b4-212e69c57424';
	SET @identityProductId = 2;
	SET @identityModuleId = 7;
	SET @featureKey = 'PDV_Product_Get_Param';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END

 BEGIN
	SET @featureId = N'5dc7ccd9-26d8-4910-b84c-beb71956fa5f';
	SET @identityProductId = 2;
	SET @identityModuleId = 7;
	SET @featureKey = 'PDV_Product_Post';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END
 
 BEGIN
	SET @featureId = N'd03d78d6-c73e-49d9-b3a0-35da6138c82d';
	SET @identityProductId = 2;
	SET @identityModuleId = 7;
	SET @featureKey = 'PDV_Product_Get_Search';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END
 
 BEGIN
	SET @featureId = N'1a59aecd-b4bc-40fc-a7cb-e4237f00e26d';
	SET @identityProductId = 2;
	SET @identityModuleId = 7;
	SET @featureKey = 'PDV_Product_Get_Category';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END

 BEGIN
	SET @featureId = N'3afcc04c-69cd-461e-9853-5cd8079456c8';
	SET @identityProductId = 2;
	SET @identityModuleId = 7;
	SET @featureKey = 'PDV_Product_Put';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END

 BEGIN
	SET @featureId = N'ad38d418-ff66-4f4b-a5bf-26e95148e46f';
	SET @identityProductId = 2;
	SET @identityModuleId = 7;
	SET @featureKey = 'PDV_Product_Delete';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END

 BEGIN
	SET @featureId = N'6c706081-ea3f-44c1-909f-5c6588ad6cb0';
	SET @identityProductId = 2;
	SET @identityModuleId = 7;
	SET @featureKey = 'PDV_Product_GetImageBarCode';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END

BEGIN
	SET @featureId = N'9055e83e-e338-4819-a4d8-52ab9a42961b';
	SET @identityProductId = 2;
	SET @identityModuleId = 7;
	SET @featureKey = 'PDV_Product_GetAllIdAndName';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END
GO
 
 