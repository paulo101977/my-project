﻿  Declare @featureId UNIQUEIDENTIFIER;
 Declare @identityProductId int;
 Declare @identityModuleId int;
 Declare @featureKey varchar(200);

 BEGIN
	SET @featureId = N'cad2c978-54f4-4316-966c-5f949b8e47de';
	SET @identityProductId = 2;
	SET @identityModuleId = 7;
	SET @featureKey = 'PDV_Ncm_Get';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END
GO

 