﻿  Declare @featureId UNIQUEIDENTIFIER;
 Declare @identityProductId int;
 Declare @identityModuleId int;
 Declare @featureKey varchar(200);

 BEGIN
	SET @featureId = N'75bc0fb1-6c39-41d4-aa49-dc3e6e27ad0a';
	SET @identityProductId = 2;
	SET @identityModuleId = 7;
	SET @featureKey = 'PDV_ProductBillingItem_Get';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END
GO
 