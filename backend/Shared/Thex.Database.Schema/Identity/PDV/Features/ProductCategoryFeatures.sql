﻿ Declare @featureId UNIQUEIDENTIFIER;
 Declare @identityProductId int;
 Declare @identityModuleId int;
 Declare @featureKey varchar(200);

 BEGIN
	SET @featureId = N'8fe78570-e3e2-4196-8ec5-ef2617091166';
	SET @identityProductId = 2;
	SET @identityModuleId = 7;
	SET @featureKey = 'PDV_ProductCategory_Get_GroupFixed';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END

 BEGIN
	SET @featureId = N'b8efc431-bff4-4296-ac89-5420a807f6b9';
	SET @identityProductId = 2;
	SET @identityModuleId = 7;
	SET @featureKey = 'PDV_ProductCategory_Get';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END
 
 BEGIN
	SET @featureId = N'848a41b5-387b-43c8-bb95-5ba08e0d40bc';
	SET @identityProductId = 2;
	SET @identityModuleId = 7;
	SET @featureKey = 'PDV_ProductCategory_Patch_ToggleActivation';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END
 
 BEGIN
	SET @featureId = N'21e1e637-174e-403f-97bf-7f766a23527a';
	SET @identityProductId = 2;
	SET @identityModuleId = 7;
	SET @featureKey = 'PDV_ProductCategory_Put';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END
 
 BEGIN
	SET @featureId = N'1417c065-41b8-43dd-8e24-64a1feceded6';
	SET @identityProductId = 2;
	SET @identityModuleId = 7;
	SET @featureKey = 'PDV_ProductCategory_Post';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END
 
 BEGIN
	SET @featureId = N'8cf7f6de-2fdf-4636-aa41-141d4548f6d1';
	SET @identityProductId = 2;
	SET @identityModuleId = 7;
	SET @featureKey = 'PDV_ProductCategory_Delete';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END

 BEGIN
	SET @featureId = N'b72123e1-a27e-4c0e-839c-a1a5afd5154e';
	SET @identityProductId = 2;
	SET @identityModuleId = 7;
	SET @featureKey = 'PDV_ProductCategory_Get_Param';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END
 
 BEGIN
	SET @featureId = N'ff9e8971-fedd-4fac-8b4a-4eac09507492';
	SET @identityProductId = 2;
	SET @identityModuleId = 7;
	SET @featureKey = 'PDV_ProductCategory_Get_GetAllByBillingItemId';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END
GO

 