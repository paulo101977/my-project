﻿  Declare @featureId UNIQUEIDENTIFIER;
 Declare @identityProductId int;
 Declare @identityModuleId int;
 Declare @featureKey varchar(200);

 BEGIN
	SET @featureId = N'0c99fbc1-2580-4dac-876c-053ea7cc60f3';
	SET @identityProductId = 2;
	SET @identityModuleId = 7;
	SET @featureKey = 'PDV_BillingAccountItem_Post_Debit';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END
GO

 