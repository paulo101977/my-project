﻿  Declare @featureId UNIQUEIDENTIFIER;
 Declare @identityProductId int;
 Declare @identityModuleId int;
 Declare @featureKey varchar(200);

 BEGIN
	SET @featureId = N'ed2d6f23-9d0a-4567-b5f5-34fddf9516c7';
	SET @identityProductId = 2;
	SET @identityModuleId = 1;
	SET @featureKey = 'PDV_UnitMeasurement_Get';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END
GO

