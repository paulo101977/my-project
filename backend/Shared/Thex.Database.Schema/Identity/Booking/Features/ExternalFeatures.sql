﻿ Declare @featureId UNIQUEIDENTIFIER;
 Declare @identityProductId int;
 Declare @identityModuleId int;
 Declare @featureKey varchar(200);

 BEGIN
	SET @featureId = N'f1255b69-154b-4a54-8c09-19f3f17c9bbf';
	SET @identityProductId = 1;
	SET @identityModuleId = 5;
	SET @featureKey = 'Booking_ExternalRegistration_Post';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END

BEGIN
	SET  @featureId = N'1d5b7ed6-1dd6-460b-88cd-a97a4ee9b69e';
	SET  @identityProductId = 1;
	SET  @identityModuleId = 5;
	SET  @featureKey = 'Booking_ExternalRegistration_RoomList_Get';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END


BEGIN
	SET  @featureId = N'b3b9e090-8cb7-4756-b959-2694ea370ae9';
	SET  @identityProductId = 1;
	SET  @identityModuleId = 5;
	SET  @featureKey = 'Booking_ExternalRegistration_RegistrationList_Get';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END


BEGIN
	SET  @featureId = N'65f0f307-298d-47f4-849a-d70aa686d527';
	SET  @identityProductId = 1;
	SET  @identityModuleId = 5;
	SET  @featureKey = 'Booking_ExternalRegistration_Registration_Get';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END

BEGIN
	SET  @featureId = N'bf55f182-680d-4b80-9d11-9be1de114d56';
	SET  @identityProductId = 1;
	SET  @identityModuleId = 5;
	SET  @featureKey = 'Booking_ExternalRegistration_Put';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END
GO