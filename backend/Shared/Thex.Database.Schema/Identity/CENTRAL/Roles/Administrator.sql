﻿   --Central Administrator ROLE
	Declare @roleId UNIQUEIDENTIFIER;

	Declare @RoleNamePtBr varchar(150);
	Declare @RoleNamePtPt varchar(150);
	Declare @RoleNameEnUs varchar(150);
	Declare @RoleNameEsAr varchar(150);
	Declare @identityProductId int;

	Declare @permissionIdList varchar(MAX);

BEGIN
	SET @roleId = N'12bc5291-c1ca-4e34-ba8b-c59870182fe3';

	SET @RoleNamePtBr = 'Central - Administrador';
	SET @RoleNamePtPt = 'Central - Administrador';
	SET @RoleNameEnUs = 'Central - Administrator';
	SET @RoleNameEsAr = 'Central - Administrador';

	SET @identityProductId = 8;

	SET @permissionIdList =
	
	'cf4fd204-5b89-4255-b404-47e91a772666,'+
	'ce238283-7936-437d-97f3-d6e6fdb7e71f,'+
	'a7967d17-39ad-4409-ad62-334e643bdea1,'+
	'345d7b0e-bc52-4bc8-8b75-af25b1876270,'+
	'87c317f9-022b-4d5c-bfa8-8d99fe09a86a,'+
	'ef69eef9-5985-4cce-9e63-7c897da98ff2,'+
	'f2b0b1f2-b691-485b-916e-20a739d6ad89,'+
	'8798d024-5ad2-4ed5-8546-620b26f99008,'+
	'6f055a3d-ed3b-4c7c-a656-fc4878ccc84f,'+
	'159a6f10-b402-41bc-b45c-2e39e459c4b6,'+
	'280b42da-06b5-4ccf-8a1f-8b7b8e9add37,'+
	'a9a1cd04-f6f1-42b4-9d3c-7c7312996f13,'+
	'98df927b-4c36-4200-bd1c-a634c998c934,'+
	'0003e6cb-56fc-4a38-b949-fa1f1eaa70d7';


	EXEC PROC_Role
		@roleId,
		@RoleNamePtBr,
		@RoleNamePtPt,
		@RoleNameEnUs,
		@RoleNameEsAr,
		@identityProductId,
		@permissionIdList;
END

GO
