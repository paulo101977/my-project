﻿Declare @identityMenuFeatureId UNIQUEIDENTIFIER;
Declare @identityProductId int;
Declare @identityModuleId int;
Declare @identityFeatureParentId UNIQUEIDENTIFIER;
Declare @identityFeatureId UNIQUEIDENTIFIER;
Declare @isActive int;
Declare @orderNumber int;
Declare @level int;
Declare @description varchar(MAX);

--RESERVE
BEGIN
	SET @identityMenuFeatureId = N'57c41e29-e66d-4bb3-b0ca-1d418b36cfb4';
	SET @identityProductId = 8;
	SET @identityModuleId = 5;
	SET @identityFeatureParentId = NULL;
	SET @identityFeatureId = N'6c1ae928-1f17-41c0-8d59-33b966321433';
	SET @isActive = 1;
	SET @orderNumber = 4;
	SET @level = 1;
	SET @description = 'Menu - Reserve';

	EXEC PROC_CreateMenuFeature
		@identityMenuFeatureId,
		@identityProductId,
		@identityModuleId,
		@identityFeatureParentId,
		@identityFeatureId,
		@isActive,
		@orderNumber,
		@level,
		@description;

END

 --RESERVE -- ITEMS 
BEGIN
	SET @identityMenuFeatureId = N'afc2c8bf-b500-4adb-8a2f-978223db6c7c';
	SET @identityProductId = 8;
	SET @identityModuleId = 5;
	SET @identityFeatureParentId = '6c1ae928-1f17-41c0-8d59-33b966321433';
	SET @identityFeatureId = N'4e4558b6-c387-4791-866c-7a148e9fdb23';
	SET @isActive = 1;
	SET @orderNumber = 1;
	SET @level = 2;
	SET @description = 'Menu - Reserve - Consult Reservation';

	EXEC PROC_CreateMenuFeature
		@identityMenuFeatureId,
		@identityProductId,
		@identityModuleId,
		@identityFeatureParentId,
		@identityFeatureId,
		@isActive,
		@orderNumber,
		@level,
		@description;

END
 
BEGIN
	SET @identityMenuFeatureId = N'8a517b49-2a1d-4dab-a9fe-fe70154b3e2b';
	SET @identityProductId = 8;
	SET @identityModuleId = 5;
	SET @identityFeatureParentId = '6c1ae928-1f17-41c0-8d59-33b966321433';
	SET @identityFeatureId = N'b37e55d1-49a6-4cb4-859f-4e74065c6ac5';
	SET @isActive = 1;
	SET @orderNumber = 2;
	SET @level = 2;
	SET @description = 'Menu - Reserve - New Booking';

	EXEC PROC_CreateMenuFeature
		@identityMenuFeatureId,
		@identityProductId,
		@identityModuleId,
		@identityFeatureParentId,
		@identityFeatureId,
		@isActive,
		@orderNumber,
		@level,
		@description;

END

 
BEGIN
	SET @identityMenuFeatureId = N'b98b3739-afe8-4501-b403-edee1d5797ce';
	SET @identityProductId = 8;
	SET @identityModuleId = 5;
	SET @identityFeatureParentId = '6c1ae928-1f17-41c0-8d59-33b966321433';
	SET @identityFeatureId = N'95836461-ddd8-4032-ad72-3909004716e0';
	SET @isActive = 1;
	SET @orderNumber = 3;
	SET @level = 2;
	SET @description = 'Menu - Reserve - Availability';

	EXEC PROC_CreateMenuFeature
		@identityMenuFeatureId,
		@identityProductId,
		@identityModuleId,
		@identityFeatureParentId,
		@identityFeatureId,
		@isActive,
		@orderNumber,
		@level,
		@description;

END
 
BEGIN
	SET @identityMenuFeatureId = N'68b4cc6c-4a7b-4d8b-9aa0-faac86653949';
	SET @identityProductId = 8;
	SET @identityModuleId = 5;
	SET @identityFeatureParentId = '6c1ae928-1f17-41c0-8d59-33b966321433';
	SET @identityFeatureId = N'ea709c78-723c-4e22-afc5-3b2b7389a2b3';
	SET @isActive = 1;
	SET @orderNumber = 4;
	SET @level = 2;
	SET @description = 'Menu - Reserve - Tariff Suggestion';

	EXEC PROC_CreateMenuFeature
		@identityMenuFeatureId,
		@identityProductId,
		@identityModuleId,
		@identityFeatureParentId,
		@identityFeatureId,
		@isActive,
		@orderNumber,
		@level,
		@description;

END

GO