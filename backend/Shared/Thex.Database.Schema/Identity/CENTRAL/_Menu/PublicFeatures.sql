﻿ --HOME

 	DECLARE @featureId UNIQUEIDENTIFIER;
	DECLARE @identityProductId int;
	DECLARE @identityModuleId int;
	DECLARE @featureKey varchar(200);
	DECLARE @PermissionNamePtBr varchar(150);
	DECLARE @PermissionNamePtPt varchar(150);
	DECLARE @PermissionNameEnUs varchar(150);
	DECLARE @PermissionNameEsAr varchar(150);
	
 BEGIN
	SET @featureId = N'6c1ae928-1f17-41c0-8d59-33b966321433';
	SET @identityProductId = 8;
	SET @identityModuleId = 5;
	SET @featureKey = 'CENTRAL_Menu_Reserve';
	SET @PermissionNamePtBr = 'Reservas';
	SET @PermissionNamePtPt = 'Reservas';
	SET @PermissionNameEsAr = 'Reservas';
	SET @PermissionNameEnUs = 'Reservation';

	EXEC PROC_CreatePublicFeature
		@featureId, @identityProductId,@identityModuleId,@featureKey,@PermissionNamePtBr,@PermissionNamePtPt,@PermissionNameEnUs,@PermissionNameEsAr
		
  END

 BEGIN
	SET @featureId = N'ea709c78-723c-4e22-afc5-3b2b7389a2b3';
	SET @identityProductId = 8;
	SET @identityModuleId = 5;
	SET @featureKey = 'CENTRAL_Menu_Tariff_Suggestion';
	SET @PermissionNamePtBr = 'Sugestão Tarifária';
	SET @PermissionNamePtPt = 'Sugestão Tarifária';
	SET @PermissionNameEsAr = 'Sugestão Tarifária';
	SET @PermissionNameEnUs = 'Tariff Suggestion';

	EXEC PROC_CreatePublicFeature
		@featureId, @identityProductId,@identityModuleId,@featureKey,@PermissionNamePtBr,@PermissionNamePtPt,@PermissionNameEnUs,@PermissionNameEsAr
		
END

 BEGIN
	SET @featureId = N'4e4558b6-c387-4791-866c-7a148e9fdb23';
	SET @identityProductId = 8;
	SET @identityModuleId = 5;
	SET @featureKey = 'CENTRAL_Menu_Consult_Reservation';
	SET @PermissionNamePtBr = 'Consultar Reserva';
	SET @PermissionNamePtPt = 'Consultar Reserva';
	SET @PermissionNameEsAr = 'Consultar Reserva';
	SET @PermissionNameEnUs = 'Consult Reservation';

	EXEC PROC_CreatePublicFeature
		@featureId, @identityProductId,@identityModuleId,@featureKey,@PermissionNamePtBr,@PermissionNamePtPt,@PermissionNameEnUs,@PermissionNameEsAr
		
END


 BEGIN
	SET @featureId = N'95836461-ddd8-4032-ad72-3909004716e0';
	SET @identityProductId = 8;
	SET @identityModuleId = 5;
	SET @featureKey = 'CENTRAL_Menu_Availability';
	SET @PermissionNamePtBr = 'Disponibilidade';
	SET @PermissionNamePtPt = 'Disponibilidade';
	SET @PermissionNameEsAr = 'Disponibilidad';
	SET @PermissionNameEnUs = 'Availability';

	EXEC PROC_CreatePublicFeature
		@featureId, @identityProductId,@identityModuleId,@featureKey,@PermissionNamePtBr,@PermissionNamePtPt,@PermissionNameEnUs,@PermissionNameEsAr
		
END

 BEGIN
	SET @featureId = N'b37e55d1-49a6-4cb4-859f-4e74065c6ac5';
	SET @identityProductId = 8;
	SET @identityModuleId = 5;
	SET @featureKey = 'CENTRAL_Menu_New_Booking';
	SET @PermissionNamePtBr = 'Nova Reserva';
	SET @PermissionNamePtPt = 'Nova Reserva';
	SET @PermissionNameEsAr = 'Nueva Reserva';
	SET @PermissionNameEnUs = 'New Booking';

	EXEC PROC_CreatePublicFeature
		@featureId, @identityProductId,@identityModuleId,@featureKey,@PermissionNamePtBr,@PermissionNamePtPt,@PermissionNameEnUs,@PermissionNameEsAr
		
END

GO
