﻿	Declare @identityPermissionId UNIQUEIDENTIFIER;
	
	Declare @PermissionNamePtBr varchar(150);
	Declare @PermissionNamePtPt varchar(150);
	Declare @PermissionNameEnUs varchar(150);
	Declare @PermissionNameEsAr varchar(150);

	Declare @identityProductId int;
	Declare @identityModuleId int;
	
	Declare @permissionKeyList varchar(MAX);

-- PMS - Consult Reservation
BEGIN
	SET @identityPermissionId = N'ce238283-7936-437d-97f3-d6e6fdb7e71f';
	
	SET @PermissionNamePtBr = 'Consultar Reserva';
	SET @PermissionNamePtPt = 'Consultar Reserva';
	SET @PermissionNameEnUs = 'Consultar Reserva';
	SET @PermissionNameEsAr = 'Consult Reservation';

	SET @identityProductId = 8;
	SET @identityModuleId = 5;

	SET @permissionKeyList = 
	'CENTRAL_Menu_Consult_Reservation,' +
	'CENTRAL_PropertyMealPlanType_Get_GetAllPropertyMealPlanType,' +
	'CENTRAL_MarketSegment_Get,' +
	'CENTRAL_BusinessSource_Get,' +
	'CENTRAL_RatePlan_Get_RatePlans,' +
	'CENTRAL_Reason_Get,' +
	'CENTRAL_Reservation_Get_Search,' +
	'CENTRAL_Property_Get_RoomTypes';


	EXEC PROC_CreatePermission
		@identityPermissionId,
		@identityProductId,
		@identityModuleId,
		@PermissionNamePtBr,
		@PermissionNamePtPt,
		@PermissionNameEnUs,
		@PermissionNameEsAr,
		@permissionKeyList;
END


-- PMS - reservation slip
BEGIN
	SET @identityPermissionId = N'a7967d17-39ad-4409-ad62-334e643bdea1';
	
	SET @PermissionNamePtBr = 'Slip da reserva';
	SET @PermissionNamePtPt = 'Slip da reserva';
	SET @PermissionNameEnUs = 'Reservation Slip';
	SET @PermissionNameEsAr = 'Carta de Confirmacíon de Reserva';

	SET @identityProductId = 8;
	SET @identityModuleId = 5;

	SET @permissionKeyList = 
	'CENTRAL_ReservationItem_Get_ConfirmationReservation,' +
	'CENTRAL_ReservationItem_Post_SendEmail';


	EXEC PROC_CreatePermission
		@identityPermissionId,
		@identityProductId,
		@identityModuleId,
		@PermissionNamePtBr,
		@PermissionNamePtPt,
		@PermissionNameEnUs,
		@PermissionNameEsAr,
		@permissionKeyList;
END


-- PMS - Reservation Budget
BEGIN
	SET @identityPermissionId = N'345d7b0e-bc52-4bc8-8b75-af25b1876270';
	
	SET @PermissionNamePtBr = 'Orçamento da Reserva';
	SET @PermissionNamePtPt = 'Orçamento da Reserva';
	SET @PermissionNameEnUs = 'Reservation Budget';
	SET @PermissionNameEsAr = 'Presupuesto de reserva';

	SET @identityProductId = 8;
	SET @identityModuleId = 5;

	SET @permissionKeyList = 
		'CENTRAL_PropertyMealPlanType_Get_GetAllPropertyMealPlanType,' +
		'CENTRAL_ReservationBudget_Get_Param,' +
		'CENTRAL_ReservationItem_Get_Accommodations,' +
		'CENTRAL_Reservation_Get_ReservationHeader,' +
		'CENTRAL_ReservationBudget_Update_Accomodations,' +
		'CENTRAL_ReservationItem_Patch_Budget';

	EXEC PROC_CreatePermission
		@identityPermissionId,
		@identityProductId,
		@identityModuleId,
		@PermissionNamePtBr,
		@PermissionNamePtPt,
		@PermissionNameEnUs,
		@PermissionNameEsAr,
		@permissionKeyList;
END



-- PMS - New Booking
BEGIN
	SET @identityPermissionId = N'87c317f9-022b-4d5c-bfa8-8d99fe09a86a';
	
	SET @PermissionNamePtBr = 'Nova Reserva';
	SET @PermissionNamePtPt = 'Nova Reserva';
	SET @PermissionNameEnUs = 'New Booking';
	SET @PermissionNameEsAr = 'Nueva Reserva';

	SET @identityProductId = 8;
	SET @identityModuleId = 5;

	SET @permissionKeyList = 
		'CENTRAL_Menu_New_Booking,'+
		'CENTRAL_PropertyParameter_Get_PropertyParameters,'+
		'CENTRAL_BusinessSource_Get,'+
		'CENTRAL_MarketSegment_Get,'+
		'CENTRAL_Reason_Get,'+
		'CENTRAL_PlasticBrandProperty_Get,'+
		'CENTRAL_CompanyClient_Get_Search,'+
		'CENTRAL_CompanyClient_Get_ContactPersons,'+
		'CENTRAL_RoomType_Get_WithOverbooking,'+
		'CENTRAL_ReservationBudget_Get_Param,'+
		'CENTRAL_PropertyMealPlanType_Get_GetAllPropertyMealPlanType,'+
		'CENTRAL_RoomLayout_Get_ByRoomType,'+
		'CENTRAL_Guest_Get_Search,'+
		'CENTRAL_Reservation_Get_NewReservationCode,' +
		'CENTRAL_Reservation_Post';


	EXEC PROC_CreatePermission
		@identityPermissionId,
		@identityProductId,
		@identityModuleId,
		@PermissionNamePtBr,
		@PermissionNamePtPt,
		@PermissionNameEnUs,
		@PermissionNameEsAr,
		@permissionKeyList;
END




-- PMS - Cancel Accommodation
BEGIN
	SET @identityPermissionId = N'ef69eef9-5985-4cce-9e63-7c897da98ff2';
	
	SET @PermissionNamePtBr = 'Cancelar Acomodação';
	SET @PermissionNamePtPt = 'Cancelar Acomodação';
	SET @PermissionNameEnUs = 'Cancel Accommodation';
	SET @PermissionNameEsAr = 'Cancelar Alojamiento';

	SET @identityProductId = 8;
	SET @identityModuleId = 5;

	SET @permissionKeyList = 
		'CENTRAL_Reason_Get,'+
		'CENTRAL_ReservationItem_Patch_Cancel';


	EXEC PROC_CreatePermission
		@identityPermissionId,
		@identityProductId,
		@identityModuleId,
		@PermissionNamePtBr,
		@PermissionNamePtPt,
		@PermissionNameEnUs,
		@PermissionNameEsAr,
		@permissionKeyList;
END




-- PMS - Room List
BEGIN
	SET @identityPermissionId = N'f2b0b1f2-b691-485b-916e-20a739d6ad89';
	
	SET @PermissionNamePtBr = 'Lista de Acomodações/Room List';
	SET @PermissionNamePtPt = 'Lista de Acomodações/Room List';
	SET @PermissionNameEnUs = 'Room List';
	SET @PermissionNameEsAr = 'Lista de Acomodación/Room List';

	SET @identityProductId = 8;
	SET @identityModuleId = 5;

	SET @permissionKeyList = 
		'CENTRAL_ReservationItem_Get_Accommodations,'+
		'CENTRAL_Reservation_Get_ByReservationItem,'+
		'CENTRAL_PropertyGuestType_Get_ByProperty,'+
		'CENTRAL_Country_Get,'+
		'CENTRAL_DocumentType_Get_AllByNaturalPersonType,'+
		'CENTRAL_GuestRegistration_Get_Search,'+
		'CENTRAL_GuestReservationItem_Patch_Associate,'+
		'CENTRAL_GuestReservationItem_Patch_Disassociate';


	EXEC PROC_CreatePermission
		@identityPermissionId,
		@identityProductId,
		@identityModuleId,
		@PermissionNamePtBr,
		@PermissionNamePtPt,
		@PermissionNameEnUs,
		@PermissionNameEsAr,
		@permissionKeyList;
END




-- PMS - Booking Edition
BEGIN
	SET @identityPermissionId = N'8798d024-5ad2-4ed5-8546-620b26f99008';
	
	SET @PermissionNamePtBr = 'Edição de Reserva';
	SET @PermissionNamePtPt = 'Edição de Reserva';
	SET @PermissionNameEnUs = 'Booking Edition';
	SET @PermissionNameEsAr = 'Edición de Reserva';

	SET @identityProductId = 8;
	SET @identityModuleId = 5;

	SET @permissionKeyList = 
		'CENTRAL_PropertyParameter_Get_PropertyParameters,'+
		'CENTRAL_BusinessSource_Get,'+
		'CENTRAL_MarketSegment_Get,'+
		'CENTRAL_Reason_Get,'+
		'CENTRAL_PlasticBrandProperty_Get,'+
		'CENTRAL_CompanyClient_Get_Search,'+
		'CENTRAL_CompanyClient_Get_ContactPersons,'+
		'CENTRAL_RoomType_Get_WithOverbooking,'+
		'CENTRAL_ReservationBudget_Get_Param,'+
		'CENTRAL_PropertyMealPlanType_Get_GetAllPropertyMealPlanType,'+
		'CENTRAL_RoomLayout_Get_ByRoomType,'+
		'CENTRAL_Guest_Get_Search,'+
		'CENTRAL_Reservation_Put,'+
		'CENTRAL_Reservation_Get';


	EXEC PROC_CreatePermission
		@identityPermissionId,
		@identityProductId,
		@identityModuleId,
		@PermissionNamePtBr,
		@PermissionNamePtPt,
		@PermissionNameEnUs,
		@PermissionNameEsAr,
		@permissionKeyList;
END



-- PMS - Reactivate Accommodation
BEGIN
	SET @identityPermissionId = N'6f055a3d-ed3b-4c7c-a656-fc4878ccc84f';
	
	SET @PermissionNamePtBr = 'Reativar Acomodação';
	SET @PermissionNamePtPt = 'Reativar Acomodação';
	SET @PermissionNameEnUs = 'Reactivate Accommodation';
	SET @PermissionNameEsAr = 'Reactivar Alojamiento';

	SET @identityProductId = 8;
	SET @identityModuleId = 5;

	SET @permissionKeyList = 
		'CENTRAL_ReservationItem_Patch_Reactivate';

	EXEC PROC_CreatePermission
		@identityPermissionId,
		@identityProductId,
		@identityModuleId,
		@PermissionNamePtBr,
		@PermissionNamePtPt,
		@PermissionNameEnUs,
		@PermissionNameEsAr,
		@permissionKeyList;
END


-- PMS - Cancel Reservation
BEGIN
	SET @identityPermissionId = N'159a6f10-b402-41bc-b45c-2e39e459c4b6';
	
	SET @PermissionNamePtBr = 'Cancelar Reserva';
	SET @PermissionNamePtPt = 'Cancelar Reserva';
	SET @PermissionNameEnUs = 'Cancel Reservation';
	SET @PermissionNameEsAr = 'Cancelar Reserva';

	SET @identityProductId = 8;
	SET @identityModuleId = 5;

	SET @permissionKeyList = 
		'CENTRAL_Reason_Get,'+
		'CENTRAL_Reservation_Patch_Cancel';

	EXEC PROC_CreatePermission
		@identityPermissionId,
		@identityProductId,
		@identityModuleId,
		@PermissionNamePtBr,
		@PermissionNamePtPt,
		@PermissionNameEnUs,
		@PermissionNameEsAr,
		@permissionKeyList;
END



-- PMS - Reactivate Reservation
BEGIN
	SET @identityPermissionId = N'280b42da-06b5-4ccf-8a1f-8b7b8e9add37';
	
	SET @PermissionNamePtBr = 'Reativar Reserva';
	SET @PermissionNamePtPt = 'Reativar Reserva';
	SET @PermissionNameEnUs = 'Reactivate Reservation';
	SET @PermissionNameEsAr = 'Reativar Reserva';

	SET @identityProductId = 8;
	SET @identityModuleId = 5;

	SET @permissionKeyList = 
		'CENTRAL_Reservation_Patch_Reactivate';

	EXEC PROC_CreatePermission
		@identityPermissionId,
		@identityProductId,
		@identityModuleId,
		@PermissionNamePtBr,
		@PermissionNamePtPt,
		@PermissionNameEnUs,
		@PermissionNameEsAr,
		@permissionKeyList;
END


-- PMS - Rate Suggestion
BEGIN
	SET @identityPermissionId = N'a9a1cd04-f6f1-42b4-9d3c-7c7312996f13';
	
	SET @PermissionNamePtBr = 'Disponibilidade';
	SET @PermissionNamePtPt = 'Disponibilidade';
	SET @PermissionNameEnUs = 'Disponibilidad';
	SET @PermissionNameEsAr = 'Availability';

	SET @identityProductId = 8;
	SET @identityModuleId = 5;

	SET @permissionKeyList = 
		'CENTRAL_Menu_Availability,'+
		'CENTRAL_Availability_Get_RoomTypes,'+
		'CENTRAL_Reason_Get,'+
		'CENTRAL_Availability_Get_Properties';


	EXEC PROC_CreatePermission
		@identityPermissionId,
		@identityProductId,
		@identityModuleId,
		@PermissionNamePtBr,
		@PermissionNamePtPt,
		@PermissionNameEnUs,
		@PermissionNameEsAr,
		@permissionKeyList;
END



-- PMS - Rate Suggestion
BEGIN
	SET @identityPermissionId = N'98df927b-4c36-4200-bd1c-a634c998c934';
	
	SET @PermissionNamePtBr = 'Sugestão Tarifária';
	SET @PermissionNamePtPt = 'Sugestão Tarifária';
	SET @PermissionNameEnUs = 'Sugestão Tarifária';
	SET @PermissionNameEsAr = 'Rate Suggestion';

	SET @identityProductId = 8;
	SET @identityModuleId = 5;

	SET @permissionKeyList = 
		'CENTRAL_Menu_Tariff_Suggestion,'+
		'CENTRAL_PropertyMealPlanType_Get_GetAllPropertyMealPlanType,'+
		'CENTRAL_Property_Get_RoomTypes,'+
		'CENTRAL_CompanyClient_Get_Search,'+
		'CENTRAL_RateProposal_Get';


	EXEC PROC_CreatePermission
		@identityPermissionId,
		@identityProductId,
		@identityModuleId,
		@PermissionNamePtBr,
		@PermissionNamePtPt,
		@PermissionNameEnUs,
		@PermissionNameEsAr,
		@permissionKeyList;
END


-- PMS - Guest record
BEGIN
	SET @identityPermissionId = N'0003e6cb-56fc-4a38-b949-fa1f1eaa70d7';
	
	SET @PermissionNamePtBr = 'Ficha de Hóspede';
	SET @PermissionNamePtPt = 'Ficha de Hóspede';
	SET @PermissionNameEnUs = 'Guest record';
	SET @PermissionNameEsAr = 'Ficha del Huésped';

	SET @identityProductId = 8;
	SET @identityModuleId = 5;

	SET @permissionKeyList = 
		'CENTRAL_GuestRegistration_Get_Responsibles,' +
		'CENTRAL_GuestRegistration_Get_ByGuestReservationItem,' +
		'CENTRAL_Country_Get,' +
		'CENTRAL_ReservationItem_Get_GuestGuestRegistration,' +
		'CENTRAL_Reservation_Get_Header,' +
		'CENTRAL_Reason_Get,' +
		'CENTRAL_GeneralOccupation_Get,' +
		'CENTRAL_Nationality_Get,' +
		'CENTRAL_DocumentType_Get_AllByNaturalPersonType,' +
		'CENTRAL_PropertyGuestType_Get_ByProperty,' +
		'CENTRAL_TransportationType_Get';

	EXEC PROC_CreatePermission
		@identityPermissionId,
		@identityProductId,
		@identityModuleId,
		@PermissionNamePtBr,
		@PermissionNamePtPt,
		@PermissionNameEnUs,
		@PermissionNameEsAr,
		@permissionKeyList;
END

GO