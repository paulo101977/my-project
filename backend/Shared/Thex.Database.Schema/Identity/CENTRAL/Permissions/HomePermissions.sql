﻿	Declare @identityPermissionId UNIQUEIDENTIFIER;
	
	Declare @PermissionNamePtBr varchar(150);
	Declare @PermissionNamePtPt varchar(150);
	Declare @PermissionNameEnUs varchar(150);
	Declare @PermissionNameEsAr varchar(150);

	Declare @identityProductId int;
	Declare @identityModuleId int;
	
	Declare @permissionKeyList varchar(MAX);
-- Hotel Panel
BEGIN
	SET @identityPermissionId = N'cf4fd204-5b89-4255-b404-47e91a772666';
	
	SET @PermissionNamePtBr = 'Painél de Hotéis';
	SET @PermissionNamePtPt = 'Painél de Hotéis';
	SET @PermissionNameEnUs = 'Hotel Panel';
	SET @PermissionNameEsAr = 'Painél de Hotéis';

	SET @identityProductId = 8;
	SET @identityModuleId = 1;

	SET @permissionKeyList = 
	'CENTRAL_Property_GetAll';

	EXEC PROC_CreatePermission
		@identityPermissionId,
		@identityProductId,
		@identityModuleId,
		@PermissionNamePtBr,
		@PermissionNamePtPt,
		@PermissionNameEnUs,
		@PermissionNameEsAr,
		@permissionKeyList;
END

GO