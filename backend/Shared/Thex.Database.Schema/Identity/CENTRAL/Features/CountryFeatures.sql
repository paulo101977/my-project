﻿ Declare @featureId UNIQUEIDENTIFIER;
 Declare @identityProductId int;
 Declare @identityModuleId int;
 Declare @featureKey varchar(200);

 BEGIN
	SET @featureId = N'4d11b1a0-5d9c-441e-a5e4-c22d9e8fbe36';
	SET @identityProductId = 8;
	SET @identityModuleId = 1;
	SET @featureKey = 'CENTRAL_Country_Get';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END

GO