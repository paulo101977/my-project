﻿  Declare @featureId UNIQUEIDENTIFIER;
 Declare @identityProductId int;
 Declare @identityModuleId int;
 Declare @featureKey varchar(200);

 BEGIN
	SET @featureId = N'e425515c-84d3-465f-bf3d-7a3c1be4bf3d';
	SET @identityProductId = 8;
	SET @identityModuleId = 1;
	SET @featureKey = 'CENTRAL_RatePlan_Get_RatePlans';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END

GO