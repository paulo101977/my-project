﻿  Declare @featureId UNIQUEIDENTIFIER;
 Declare @identityProductId int;
 Declare @identityModuleId int;
 Declare @featureKey varchar(200);

 BEGIN
	SET @featureId = N'b656a177-ac86-48a0-b8ca-51a51459e001';
	SET @identityProductId = 8;
	SET @identityModuleId = 1;
	SET @featureKey = 'CENTRAL_PropertyMealPlanType_Get_GetAllPropertyMealPlanType';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END


GO