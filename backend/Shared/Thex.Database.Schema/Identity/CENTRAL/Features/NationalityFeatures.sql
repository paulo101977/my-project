﻿ Declare @featureId UNIQUEIDENTIFIER;
 Declare @identityProductId int;
 Declare @identityModuleId int;
 Declare @featureKey varchar(200);

 BEGIN
	SET @featureId = N'd3e1fdee-d324-4769-9065-c9caae126638';
	SET @identityProductId = 8;
	SET @identityModuleId = 1;
	SET @featureKey = 'CENTRAL_Nationality_Get';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END

GO