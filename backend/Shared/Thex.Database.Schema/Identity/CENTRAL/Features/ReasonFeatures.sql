﻿  Declare @featureId UNIQUEIDENTIFIER;
 Declare @identityProductId int;
 Declare @identityModuleId int;
 Declare @featureKey varchar(200);

 BEGIN
	SET @featureId = N'2169064b-091a-4293-befa-e44612630b9f';
	SET @identityProductId = 8;
	SET @identityModuleId = 1;
	SET @featureKey = 'CENTRAL_Reason_Get';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END

GO