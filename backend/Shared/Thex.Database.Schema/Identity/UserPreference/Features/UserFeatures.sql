﻿  Declare @featureId UNIQUEIDENTIFIER;
 Declare @identityProductId int;
 Declare @identityModuleId int;
 Declare @featureKey varchar(200);

 BEGIN
	SET @featureId = N'68079199-444e-4fa6-80a7-8611e395c226';
	SET @identityProductId = 5;
	SET @identityModuleId = 7;
	SET @featureKey = 'PREF_UserPreference_Post_MostSelected';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END

 BEGIN
	SET @featureId = N'0a785623-ef3a-4a65-8d3d-8a74c9487d26';
	SET @identityProductId = 5;
	SET @identityModuleId = 7;
	SET @featureKey = 'PREF_UserPreference_Post_BillingItemFav';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END

 BEGIN
	SET @featureId = N'a7c2f042-3603-4a84-bda2-08af76f1ac9d';
	SET @identityProductId = 5;
	SET @identityModuleId = 7;
	SET @featureKey = 'PREF_UserPreference_Get_BillingItemFav';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END

 BEGIN
	SET @featureId = N'010be664-6f95-4d36-96f2-aa7781fce1a1';
	SET @identityProductId = 5;
	SET @identityModuleId = 7;
	SET @featureKey = 'PREF_UserPreference_Get_MostSelected';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END
 
GO