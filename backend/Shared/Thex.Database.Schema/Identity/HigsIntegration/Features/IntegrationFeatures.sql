﻿ Declare @featureId UNIQUEIDENTIFIER;
 Declare @identityProductId int;
 Declare @identityModuleId int;
 Declare @featureKey varchar(200);

 BEGIN
	SET @featureId = N'524ca094-d351-403b-a093-a485576add08';
	SET @identityProductId = 7;
	SET @identityModuleId = 10;
	SET @featureKey = 'HIGSINTEGRATION_Get_All_Reservation_Logs';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END

 BEGIN
	SET @featureId = N'd6f7365d-61ff-4d1a-8a5f-75a773540542';
	SET @identityProductId = 7;
	SET @identityModuleId = 10;
	SET @featureKey = 'HIGSINTEGRATION_Get_All_Reservation_Logs_History';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END
GO
 
 