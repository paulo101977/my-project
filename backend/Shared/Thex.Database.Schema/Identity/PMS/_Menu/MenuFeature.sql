﻿Declare @identityMenuFeatureId UNIQUEIDENTIFIER;
Declare @identityProductId int;
Declare @identityModuleId int;
Declare @identityFeatureParentId UNIQUEIDENTIFIER;
Declare @identityFeatureId UNIQUEIDENTIFIER;
Declare @isActive int;
Declare @orderNumber int;
Declare @level int;
Declare @description varchar(MAX);

 --HOME
BEGIN
	SET @identityMenuFeatureId = N'd4eac2d9-859f-4f00-aa47-9d1c2aa6a695';
	SET @identityProductId = 1;
	SET @identityModuleId = 1;
	SET @identityFeatureParentId = NULL;
	SET @identityFeatureId = N'fbc4b2ef-a5ac-4ad5-9abe-057245d4d1d1';
	SET @isActive = 1;
	SET @orderNumber = 1;
	SET @level = 1;
	SET @description = 'Menu - Home';

	EXEC PROC_CreateMenuFeature
		@identityMenuFeatureId,
		@identityProductId,
		@identityModuleId,
		@identityFeatureParentId,
		@identityFeatureId,
		@isActive,
		@orderNumber,
		@level,
		@description;

END

--HOSTING
BEGIN
	SET @identityMenuFeatureId = N'12dbc84d-97f6-4a7c-b4ba-90aee2d98333';
	SET @identityProductId = 1;
	SET @identityModuleId = 3;
	SET @identityFeatureParentId = NULL;
	SET @identityFeatureId = N'a90f35b5-4948-4bd5-b8c5-06d0febc18aa';
	SET @isActive = 1;
	SET @orderNumber = 2;
	SET @level = 1;
	SET @description = 'Menu - Hosting';

	EXEC PROC_CreateMenuFeature
		@identityMenuFeatureId,
		@identityProductId,
		@identityModuleId,
		@identityFeatureParentId,
		@identityFeatureId,
		@isActive,
		@orderNumber,
		@level,
		@description;

END


--HOSTING -- ITEMS 
BEGIN
	SET @identityMenuFeatureId = N'c912db2a-ac26-4624-9e43-37a645250026';
	SET @identityProductId = 1;
	SET @identityModuleId = 3;
	SET @identityFeatureParentId = N'a90f35b5-4948-4bd5-b8c5-06d0febc18aa';
	SET @identityFeatureId = N'7baa48c8-fb1b-46b7-b0da-4f6a63003804';
	SET @isActive = 1;
	SET @orderNumber = 1;
	SET @level = 2;
	SET @description = 'Menu - Hosting - Check-in';

	EXEC PROC_CreateMenuFeature
		@identityMenuFeatureId,
		@identityProductId,
		@identityModuleId,
		@identityFeatureParentId,
		@identityFeatureId,
		@isActive,
		@orderNumber,
		@level,
		@description;

END

BEGIN
	SET @identityMenuFeatureId = N'368b754b-14ec-49d7-809b-d867ccd89c78';
	SET @identityProductId = 1;
	SET @identityModuleId = 3;
	SET @identityFeatureParentId = N'a90f35b5-4948-4bd5-b8c5-06d0febc18aa';
	SET @identityFeatureId = N'a2a82619-4f81-48b7-bcea-7daa486de6b9';
	SET @isActive = 1;
	SET @orderNumber = 2;
	SET @level = 2;
	SET @description = 'Menu - Hosting - Walk-in';

	EXEC PROC_CreateMenuFeature
		@identityMenuFeatureId,
		@identityProductId,
		@identityModuleId,
		@identityFeatureParentId,
		@identityFeatureId,
		@isActive,
		@orderNumber,
		@level,
		@description;

END

BEGIN
	SET @identityMenuFeatureId = N'9b1c50e7-6699-41a7-b9ec-3ef29461eeef';
	SET @identityProductId = 1;
	SET @identityModuleId = 3;
	SET @identityFeatureParentId = N'a90f35b5-4948-4bd5-b8c5-06d0febc18aa';
	SET @identityFeatureId = N'02cb046e-68e4-4347-ae8a-2d2c5b68160c';
	SET @isActive = 1;
	SET @orderNumber = 3;
	SET @level = 2;
	SET @description = 'Menu - Hosting - Change of UH';

	EXEC PROC_CreateMenuFeature
		@identityMenuFeatureId,
		@identityProductId,
		@identityModuleId,
		@identityFeatureParentId,
		@identityFeatureId,
		@isActive,
		@orderNumber,
		@level,
		@description;

END

BEGIN
	SET @identityMenuFeatureId = N'91f8772d-f226-492c-8d49-fb4804aec031';
	SET @identityProductId = 1;
	SET @identityModuleId = 3;
	SET @identityFeatureParentId = N'a90f35b5-4948-4bd5-b8c5-06d0febc18aa';
	SET @identityFeatureId = N'69ae172a-8525-46f5-a80e-b2f463d9d4c5';
	SET @isActive = 1;
	SET @orderNumber = 4;
	SET @level = 2;
	SET @description = 'Menu - Hosting - Audit';

	EXEC PROC_CreateMenuFeature
		@identityMenuFeatureId,
		@identityProductId,
		@identityModuleId,
		@identityFeatureParentId,
		@identityFeatureId,
		@isActive,
		@orderNumber,
		@level,
		@description;

END

--FINANCIAL -- ITEMS 
BEGIN
	SET @identityMenuFeatureId = N'ee5d98d7-fb43-4ae2-bbb0-b01c9a9002ac';
	SET @identityProductId = 1;
	SET @identityModuleId = 4;
	SET @identityFeatureParentId = NULL;
	SET @identityFeatureId = N'e82caa5b-bc0a-4a85-9c6b-44a2e9b3505c';
	SET @isActive = 1;
	SET @orderNumber = 3;
	SET @level = 1;
	SET @description = 'Menu - Cash Operation';

	EXEC PROC_CreateMenuFeature
		@identityMenuFeatureId,
		@identityProductId,
		@identityModuleId,
		@identityFeatureParentId,
		@identityFeatureId,
		@isActive,
		@orderNumber,
		@level,
		@description;

END

--RESERVE
BEGIN
	SET @identityMenuFeatureId = N'c9138eb3-bbed-4744-83a7-2e5e92b30926';
	SET @identityProductId = 1;
	SET @identityModuleId = 5;
	SET @identityFeatureParentId = NULL;
	SET @identityFeatureId = N'3bcb16f2-e068-4628-9e51-e49353a329ed';
	SET @isActive = 1;
	SET @orderNumber = 4;
	SET @level = 1;
	SET @description = 'Menu - Reserve';

	EXEC PROC_CreateMenuFeature
		@identityMenuFeatureId,
		@identityProductId,
		@identityModuleId,
		@identityFeatureParentId,
		@identityFeatureId,
		@isActive,
		@orderNumber,
		@level,
		@description;

END

 --RESERVE -- ITEMS 
BEGIN
	SET @identityMenuFeatureId = N'65c14aee-8a21-481e-9dba-dfa707a02a4a';
	SET @identityProductId = 1;
	SET @identityModuleId = 5;
	SET @identityFeatureParentId = '3bcb16f2-e068-4628-9e51-e49353a329ed';
	SET @identityFeatureId = N'c0a43700-bfc3-4b44-a94c-e2d8b27d14b7';
	SET @isActive = 1;
	SET @orderNumber = 1;
	SET @level = 2;
	SET @description = 'Menu - Reserve - Consult Reservation';

	EXEC PROC_CreateMenuFeature
		@identityMenuFeatureId,
		@identityProductId,
		@identityModuleId,
		@identityFeatureParentId,
		@identityFeatureId,
		@isActive,
		@orderNumber,
		@level,
		@description;

END
 
BEGIN
	SET @identityMenuFeatureId = N'4f18f28f-8b4d-497c-9b25-434e919fa98a';
	SET @identityProductId = 1;
	SET @identityModuleId = 5;
	SET @identityFeatureParentId = '3bcb16f2-e068-4628-9e51-e49353a329ed';
	SET @identityFeatureId = N'e55ee818-c5f4-444c-9ea0-6c6a6182a0d3';
	SET @isActive = 1;
	SET @orderNumber = 2;
	SET @level = 2;
	SET @description = 'Menu - Reserve - New Booking';

	EXEC PROC_CreateMenuFeature
		@identityMenuFeatureId,
		@identityProductId,
		@identityModuleId,
		@identityFeatureParentId,
		@identityFeatureId,
		@isActive,
		@orderNumber,
		@level,
		@description;

END

 
BEGIN
	SET @identityMenuFeatureId = N'6ea804cf-3a42-4d3c-b4c7-f15b7da40645';
	SET @identityProductId = 1;
	SET @identityModuleId = 5;
	SET @identityFeatureParentId = '3bcb16f2-e068-4628-9e51-e49353a329ed';
	SET @identityFeatureId = N'3c564556-dea6-4e24-9b4f-5bde8bccacc7';
	SET @isActive = 1;
	SET @orderNumber = 3;
	SET @level = 2;
	SET @description = 'Menu - Reserve - Availability';

	EXEC PROC_CreateMenuFeature
		@identityMenuFeatureId,
		@identityProductId,
		@identityModuleId,
		@identityFeatureParentId,
		@identityFeatureId,
		@isActive,
		@orderNumber,
		@level,
		@description;

END


 
BEGIN
	SET @identityMenuFeatureId = N'6b749bdd-ac68-46c6-9fc4-eb297c9a8ac7';
	SET @identityProductId = 1;
	SET @identityModuleId = 5;
	SET @identityFeatureParentId = '3bcb16f2-e068-4628-9e51-e49353a329ed';
	SET @identityFeatureId = N'94bcfe83-8d99-4cd6-b0ab-2e760798108c';
	SET @isActive = 1;
	SET @orderNumber = 4;
	SET @level = 2;
	SET @description = 'Menu - Reserve - Tariff Suggestion';

	EXEC PROC_CreateMenuFeature
		@identityMenuFeatureId,
		@identityProductId,
		@identityModuleId,
		@identityFeatureParentId,
		@identityFeatureId,
		@isActive,
		@orderNumber,
		@level,
		@description;

END

  --GOVERNANCE
BEGIN
	SET @identityMenuFeatureId = N'9bbe45e6-f91d-40ff-a8da-1567c5654bf2';
	SET @identityProductId = 1;
	SET @identityModuleId = 6;
	SET @identityFeatureParentId = NULL;
	SET @identityFeatureId = N'21f65b27-a692-4603-a536-b292395f729d';
	SET @isActive = 1;
	SET @orderNumber = 5;
	SET @level = 1;
	SET @description = 'Menu - Governance';

	EXEC PROC_CreateMenuFeature
		@identityMenuFeatureId,
		@identityProductId,
		@identityModuleId,
		@identityFeatureParentId,
		@identityFeatureId,
		@isActive,
		@orderNumber,
		@level,
		@description;

END

--GOVERNANCE -- ITEMS 
BEGIN
	SET @identityMenuFeatureId = N'8b4932ca-1e61-4a32-a313-1a1a1f5327ea';
	SET @identityProductId = 1;
	SET @identityModuleId = 6;
	SET @identityFeatureParentId = N'21f65b27-a692-4603-a536-b292395f729d';
	SET @identityFeatureId = N'c004b742-145d-4876-8109-357f2f7358bf';
	SET @isActive = 1;
	SET @orderNumber = 1;
	SET @level = 2;
	SET @description = 'Menu - Governance - See Governance';

	EXEC PROC_CreateMenuFeature
		@identityMenuFeatureId,
		@identityProductId,
		@identityModuleId,
		@identityFeatureParentId,
		@identityFeatureId,
		@isActive,
		@orderNumber,
		@level,
		@description;

END

BEGIN
	SET @identityMenuFeatureId = N'5bc0c27b-43de-4ac5-8877-7869882c580a';
	SET @identityProductId = 1;
	SET @identityModuleId = 6;
	SET @identityFeatureParentId = N'21f65b27-a692-4603-a536-b292395f729d';
	SET @identityFeatureId = N'cb4957a0-ee64-4153-ad3b-abb9a256d565';
	SET @isActive = 1;
	SET @orderNumber = 2;
	SET @level = 2;
	SET @description = 'Menu - Governance - Associate UH';

	EXEC PROC_CreateMenuFeature
		@identityMenuFeatureId,
		@identityProductId,
		@identityModuleId,
		@identityFeatureParentId,
		@identityFeatureId,
		@isActive,
		@orderNumber,
		@level,
		@description;

END


--SETTINGS
BEGIN
	SET @identityMenuFeatureId = N'0e3380ed-9167-4096-a1b4-19fe996eb2ba';
	SET @identityProductId = 1;
	SET @identityModuleId = 7;
	SET @identityFeatureParentId = NULL;
	SET @identityFeatureId = N'606b78da-9996-4829-8789-9aac2b297da1';
	SET @isActive = 1;
	SET @orderNumber = 6;
	SET @level = 1;
	SET @description = 'Menu - Settings';

	EXEC PROC_CreateMenuFeature
		@identityMenuFeatureId,
		@identityProductId,
		@identityModuleId,
		@identityFeatureParentId,
		@identityFeatureId,
		@isActive,
		@orderNumber,
		@level,
		@description;

END

 --Settings -- ITEMS 
BEGIN
	SET @identityMenuFeatureId = N'd7b54655-86db-4ab8-b842-5c1f32913b06';
	SET @identityProductId = 1;
	SET @identityModuleId = 7;
	SET @identityFeatureParentId = N'606b78da-9996-4829-8789-9aac2b297da1';
	SET @identityFeatureId = N'7b0e7ad5-8bea-4544-b208-5fa25dabc6e8';
	SET @isActive = 1;
	SET @orderNumber = 1;
	SET @level = 2;
	SET @description = 'Menu - Registration - Hotel Management';

	EXEC PROC_CreateMenuFeature
		@identityMenuFeatureId,
		@identityProductId,
		@identityModuleId,
		@identityFeatureParentId,
		@identityFeatureId,
		@isActive,
		@orderNumber,
		@level,
		@description;

END

BEGIN
	SET @identityMenuFeatureId = N'e358ecfe-90a2-446c-bfd2-2492391b3ef1';
	SET @identityProductId = 1;
	SET @identityModuleId = 7;
	SET @identityFeatureParentId = N'606b78da-9996-4829-8789-9aac2b297da1';
	SET @identityFeatureId = N'3b5f7e8b-d8b1-4877-9794-ec5cc568800a';
	SET @isActive = 1;
	SET @orderNumber = 2;
	SET @level = 2;
	SET @description = 'Menu - Settings - Network Management';

	EXEC PROC_CreateMenuFeature
		@identityMenuFeatureId,
		@identityProductId,
		@identityModuleId,
		@identityFeatureParentId,
		@identityFeatureId,
		@isActive,
		@orderNumber,
		@level,
		@description;

END

BEGIN
	SET @identityMenuFeatureId = N'aca713f7-731c-43c6-a3c1-0f84f3aeb166';
	SET @identityProductId = 1;
	SET @identityModuleId = 7;
	SET @identityFeatureParentId = N'606b78da-9996-4829-8789-9aac2b297da1';
	SET @identityFeatureId = N'f8ac64b4-7f4e-4059-abe9-c39de8cc1b2d';
	SET @isActive = 1;
	SET @orderNumber = 3;
	SET @level = 2;
	SET @description = 'Menu - Settings - Release type';

	EXEC PROC_CreateMenuFeature
		@identityMenuFeatureId,
		@identityProductId,
		@identityModuleId,
		@identityFeatureParentId,
		@identityFeatureId,
		@isActive,
		@orderNumber,
		@level,
		@description;

END

BEGIN
	SET @identityMenuFeatureId = N'43ef773e-7db6-4a49-adac-6a81abf3e422';
	SET @identityProductId = 1;
	SET @identityModuleId = 7;
	SET @identityFeatureParentId = N'606b78da-9996-4829-8789-9aac2b297da1';
	SET @identityFeatureId = N'90581cbf-f6bf-4fe7-ad35-d31a907e614f';
	SET @isActive = 1;
	SET @orderNumber = 4;
	SET @level = 2;
	SET @description = 'Menu - Settings - Note Emissions';

	EXEC PROC_CreateMenuFeature
		@identityMenuFeatureId,
		@identityProductId,
		@identityModuleId,
		@identityFeatureParentId,
		@identityFeatureId,
		@isActive,
		@orderNumber,
		@level,
		@description;

END

BEGIN
	SET @identityMenuFeatureId = N'ace6ab42-110f-4ade-a6df-7daa2b49f6fd';
	SET @identityProductId = 1;
	SET @identityModuleId = 7;
	SET @identityFeatureParentId = N'606b78da-9996-4829-8789-9aac2b297da1';
	SET @identityFeatureId = N'5f1060c3-ad19-40e5-ad4e-edbb6562c8f2';
	SET @isActive = 1;
	SET @orderNumber = 5;
	SET @level = 2;
	SET @description = 'Menu - Settings - Forms of payment';

	EXEC PROC_CreateMenuFeature
		@identityMenuFeatureId,
		@identityProductId,
		@identityModuleId,
		@identityFeatureParentId,
		@identityFeatureId,
		@isActive,
		@orderNumber,
		@level,
		@description;

END

  --Settings -- SUBITEMS -- Hotel Management


BEGIN
	SET @identityMenuFeatureId = N'11f4ce4c-8c59-4e39-8f41-a0a2e75c703e';
	SET @identityProductId = 1;
	SET @identityModuleId = 7;
	SET @identityFeatureParentId = N'7b0e7ad5-8bea-4544-b208-5fa25dabc6e8';
	SET @identityFeatureId = N'f5412d6f-135d-4441-a3b8-cb68bcf89e99';
	SET @isActive = 1;
	SET @orderNumber = 1;
	SET @level = 2;
	SET @description = 'Menu - Register - Hotel Management - Contract';

	EXEC PROC_CreateMenuFeature
		@identityMenuFeatureId,
		@identityProductId,
		@identityModuleId,
		@identityFeatureParentId,
		@identityFeatureId,
		@isActive,
		@orderNumber,
		@level,
		@description;

END

BEGIN
	SET @identityMenuFeatureId = N'7cef2da8-6148-4cba-bf2b-6f9cf4325f0f';
	SET @identityProductId = 1;
	SET @identityModuleId = 7;
	SET @identityFeatureParentId = N'7b0e7ad5-8bea-4544-b208-5fa25dabc6e8';
	SET @identityFeatureId = N'a3e76c39-c3fb-48e6-b46e-d73fee081a31';
	SET @isActive = 1;
	SET @orderNumber = 2;
	SET @level = 2;
	SET @description = 'Menu - Register - Hotel Management - Parameters';

	EXEC PROC_CreateMenuFeature
		@identityMenuFeatureId,
		@identityProductId,
		@identityModuleId,
		@identityFeatureParentId,
		@identityFeatureId,
		@isActive,
		@orderNumber,
		@level,
		@description;

END

BEGIN
	SET @identityMenuFeatureId = N'fac3a336-db97-4234-a6b5-b8c9bdad910e';
	SET @identityProductId = 1;
	SET @identityModuleId = 7;
	SET @identityFeatureParentId = N'7b0e7ad5-8bea-4544-b208-5fa25dabc6e8';
	SET @identityFeatureId = N'dd68b842-68df-4f94-95d9-2e63b5cf491f';
	SET @isActive = 1;
	SET @orderNumber = 3;
	SET @level = 2;
	SET @description = 'Menu - Register - Hotel Management - Policies';

	EXEC PROC_CreateMenuFeature
		@identityMenuFeatureId,
		@identityProductId,
		@identityModuleId,
		@identityFeatureParentId,
		@identityFeatureId,
		@isActive,
		@orderNumber,
		@level,
		@description;

END

BEGIN
	SET @identityMenuFeatureId = N'fdb56e6f-a56f-4963-b54b-71069cbc84ae';
	SET @identityProductId = 1;
	SET @identityModuleId = 7;
	SET @identityFeatureParentId = N'7b0e7ad5-8bea-4544-b208-5fa25dabc6e8';
	SET @identityFeatureId = N'32c7ce25-4f30-4f99-a443-e6fa12698e14';
	SET @isActive = 1;
	SET @orderNumber = 4;
	SET @level = 2;
	SET @description = 'Menu - Register - Hotel Management - Kind of UH';

	EXEC PROC_CreateMenuFeature
		@identityMenuFeatureId,
		@identityProductId,
		@identityModuleId,
		@identityFeatureParentId,
		@identityFeatureId,
		@isActive,
		@orderNumber,
		@level,
		@description;

END


BEGIN
	SET @identityMenuFeatureId = N'db0b9975-706a-47f6-bccc-2bf5c7e14fb5';
	SET @identityProductId = 1;
	SET @identityModuleId = 7;
	SET @identityFeatureParentId = N'7b0e7ad5-8bea-4544-b208-5fa25dabc6e8';
	SET @identityFeatureId = N'22662ccb-7773-4aaa-ab81-f9e080047112';
	SET @isActive = 1;
	SET @orderNumber = 5;
	SET @level = 2;
	SET @description = 'Menu - Register - Hotel Management - UH';

	EXEC PROC_CreateMenuFeature
		@identityMenuFeatureId,
		@identityProductId,
		@identityModuleId,
		@identityFeatureParentId,
		@identityFeatureId,
		@isActive,
		@orderNumber,
		@level,
		@description;

END




BEGIN
	SET @identityMenuFeatureId = N'3587a638-1025-4c30-a1d2-978fab1a5c4b';
	SET @identityProductId = 1;
	SET @identityModuleId = 7;
	SET @identityFeatureParentId = N'7b0e7ad5-8bea-4544-b208-5fa25dabc6e8';
	SET @identityFeatureId = N'ade9723b-194d-4e73-a32e-ade39bcea8a9';
	SET @isActive = 1;
	SET @orderNumber = 6;
	SET @level = 2;
	SET @description = 'Menu - Register - Hotel Management - User Management';

	EXEC PROC_CreateMenuFeature
		@identityMenuFeatureId,
		@identityProductId,
		@identityModuleId,
		@identityFeatureParentId,
		@identityFeatureId,
		@isActive,
		@orderNumber,
		@level,
		@description;

END

BEGIN
	SET @identityMenuFeatureId = N'780c7671-3e7c-46e0-a9bf-682b86e26361';
	SET @identityProductId = 1;
	SET @identityModuleId = 7;
	SET @identityFeatureParentId = N'7b0e7ad5-8bea-4544-b208-5fa25dabc6e8';
	SET @identityFeatureId = N'4d9ad143-4667-4fff-952b-df137e22939e';
	SET @isActive = 1;
	SET @orderNumber = 7;
	SET @level = 2;
	SET @description = 'Menu - Register - Hotel Management - Type of Guest';

	EXEC PROC_CreateMenuFeature
		@identityMenuFeatureId,
		@identityProductId,
		@identityModuleId,
		@identityFeatureParentId,
		@identityFeatureId,
		@isActive,
		@orderNumber,
		@level,
		@description;

END

BEGIN
	SET @identityMenuFeatureId = N'3b0d2680-7c0f-4e1b-bc10-2cebd5fa1da2';
	SET @identityProductId = 1;
	SET @identityModuleId = 7;
	SET @identityFeatureParentId = N'7b0e7ad5-8bea-4544-b208-5fa25dabc6e8';
	SET @identityFeatureId = N'162df2b3-21f9-468f-965d-669fc87ff81e';
	SET @isActive = 1;
	SET @orderNumber = 8;
	SET @level = 2;
	SET @description = 'Menu - Register - Hotel Management - Governance Status';

	EXEC PROC_CreateMenuFeature
		@identityMenuFeatureId,
		@identityProductId,
		@identityModuleId,
		@identityFeatureParentId,
		@identityFeatureId,
		@isActive,
		@orderNumber,
		@level,
		@description;

END

BEGIN
	SET @identityMenuFeatureId = N'83ce2389-a85c-46d7-a247-6dcc08c6be6e';
	SET @identityProductId = 1;
	SET @identityModuleId = 7;
	SET @identityFeatureParentId = N'7b0e7ad5-8bea-4544-b208-5fa25dabc6e8';
	SET @identityFeatureId = N'd9682a67-f090-40b3-b7d4-ca9b2ef02483';
	SET @isActive = 1;
	SET @orderNumber = 9;
	SET @level = 2;
	SET @description = 'Menu - Register - Hotel Management - Currency Quotation';

	EXEC PROC_CreateMenuFeature
		@identityMenuFeatureId,
		@identityProductId,
		@identityModuleId,
		@identityFeatureParentId,
		@identityFeatureId,
		@isActive,
		@orderNumber,
		@level,
		@description;

END

-- Inative
BEGIN
	SET @identityMenuFeatureId = N'3346cfe8-7403-4700-97f0-024e70dd6dff';
	SET @identityProductId = 1;
	SET @identityModuleId = 7;
	SET @identityFeatureParentId = N'7b0e7ad5-8bea-4544-b208-5fa25dabc6e8';
	SET @identityFeatureId = N'171b2dfc-7b18-4b2c-b817-8198cfa939df';
	SET @isActive = 0;
	SET @orderNumber = 1;
	SET @level = 3;
	SET @description = 'Menu - Register - Hotel Management - Hotel';

	EXEC PROC_CreateMenuFeature
		@identityMenuFeatureId,
		@identityProductId,
		@identityModuleId,
		@identityFeatureParentId,
		@identityFeatureId,
		@isActive,
		@orderNumber,
		@level,
		@description;

END

   --Settings -- SUBITEMS -- Network management
BEGIN
	SET @identityMenuFeatureId = N'480719c3-03c9-4fae-9c93-139f0f098089';
	SET @identityProductId = 1;
	SET @identityModuleId = 7;
	SET @identityFeatureParentId = N'3b5f7e8b-d8b1-4877-9794-ec5cc568800a';
	SET @identityFeatureId = N'46a4a363-abab-47e3-ae89-4d96f22c8e33';
	SET @isActive = 1;
	SET @orderNumber = 1;
	SET @level = 3;
	SET @description = 'Menu - Settings - Network Management - Reason';

	EXEC PROC_CreateMenuFeature
		@identityMenuFeatureId,
		@identityProductId,
		@identityModuleId,
		@identityFeatureParentId,
		@identityFeatureId,
		@isActive,
		@orderNumber,
		@level,
		@description;

END

BEGIN
	SET @identityMenuFeatureId = N'bddd0908-fd50-4896-ba27-60711dc442ef';
	SET @identityProductId = 1;
	SET @identityModuleId = 7;
	SET @identityFeatureParentId = N'3b5f7e8b-d8b1-4877-9794-ec5cc568800a';
	SET @identityFeatureId = N'ea57a045-4017-4646-9a18-9b046b5685d8';
	SET @isActive = 1;
	SET @orderNumber = 2;
	SET @level = 3;
	SET @description = 'Menu - Settings - Network Management - Segment';

	EXEC PROC_CreateMenuFeature
		@identityMenuFeatureId,
		@identityProductId,
		@identityModuleId,
		@identityFeatureParentId,
		@identityFeatureId,
		@isActive,
		@orderNumber,
		@level,
		@description;

END


BEGIN
	SET @identityMenuFeatureId = N'83238aaa-568d-4d6e-9d1c-e82f24bc7f4a';
	SET @identityProductId = 1;
	SET @identityModuleId = 7;
	SET @identityFeatureParentId = N'3b5f7e8b-d8b1-4877-9794-ec5cc568800a';
	SET @identityFeatureId = N'b1eaf7b2-e16c-4618-af35-076c61ac970a';
	SET @isActive = 1;
	SET @orderNumber = 3;
	SET @level = 3;
	SET @description = 'Menu - Settings - Network Management - Origin';

	EXEC PROC_CreateMenuFeature
		@identityMenuFeatureId,
		@identityProductId,
		@identityModuleId,
		@identityFeatureParentId,
		@identityFeatureId,
		@isActive,
		@orderNumber,
		@level,
		@description;

END

BEGIN
	SET @identityMenuFeatureId = N'53d93f03-6598-445d-b278-9ae45e154f42';
	SET @identityProductId = 1;
	SET @identityModuleId = 7;
	SET @identityFeatureParentId = N'3b5f7e8b-d8b1-4877-9794-ec5cc568800a';
	SET @identityFeatureId = N'ba0e79d9-9b1a-4d94-87b2-874ac90dd644';
	SET @isActive = 1;
	SET @orderNumber = 4;
	SET @level = 3;
	SET @description = 'Menu - Settings - Network Management - Client';

	EXEC PROC_CreateMenuFeature
		@identityMenuFeatureId,
		@identityProductId,
		@identityModuleId,
		@identityFeatureParentId,
		@identityFeatureId,
		@isActive,
		@orderNumber,
		@level,
		@description;

END

BEGIN
	SET @identityMenuFeatureId = N'5c270d62-cf34-42f4-9230-2c5aa28ff840';
	SET @identityProductId = 1;
	SET @identityModuleId = 7;
	SET @identityFeatureParentId = N'3b5f7e8b-d8b1-4877-9794-ec5cc568800a';
	SET @identityFeatureId = N'e3dba002-e32d-400e-97df-df41b1912f88';
	SET @isActive = 1;
	SET @orderNumber = 5;
	SET @level = 3;
	SET @description = 'Menu - Settings - Network Management - Customer Category';

	EXEC PROC_CreateMenuFeature
		@identityMenuFeatureId,
		@identityProductId,
		@identityModuleId,
		@identityFeatureParentId,
		@identityFeatureId,
		@isActive,
		@orderNumber,
		@level,
		@description;

END

   --Settings -- SUBITEMS -- Release Type
BEGIN
	SET @identityMenuFeatureId = N'69a193f6-ab3a-405e-8208-9a6b134d54d6';
	SET @identityProductId = 1;
	SET @identityModuleId = 7;
	SET @identityFeatureParentId = N'f8ac64b4-7f4e-4059-abe9-c39de8cc1b2d';
	SET @identityFeatureId = N'f8ab19d2-5b97-423f-a383-5d7bb6cfb01e';
	SET @isActive = 1;
	SET @orderNumber = 1;
	SET @level = 3;
	SET @description = 'Menu - Settings - Release Type - Service';

	EXEC PROC_CreateMenuFeature
		@identityMenuFeatureId,
		@identityProductId,
		@identityModuleId,
		@identityFeatureParentId,
		@identityFeatureId,
		@isActive,
		@orderNumber,
		@level,
		@description;

END

BEGIN
	SET @identityMenuFeatureId = N'f7bed83f-4d03-4c78-a681-f98b3b68d061';
	SET @identityProductId = 1;
	SET @identityModuleId = 7;
	SET @identityFeatureParentId = N'f8ac64b4-7f4e-4059-abe9-c39de8cc1b2d';
	SET @identityFeatureId = N'c10b63f1-0ae0-4551-856e-7b41e826069d';
	SET @isActive = 1;
	SET @orderNumber = 2;
	SET @level = 3;
	SET @description = 'Menu - Settings - Release Type - Rate';

	EXEC PROC_CreateMenuFeature
		@identityMenuFeatureId,
		@identityProductId,
		@identityModuleId,
		@identityFeatureParentId,
		@identityFeatureId,
		@isActive,
		@orderNumber,
		@level,
		@description;

END

BEGIN
	SET @identityMenuFeatureId = N'a015da5a-4577-4390-a374-2d077a10a87d';
	SET @identityProductId = 1;
	SET @identityModuleId = 7;
	SET @identityFeatureParentId = N'f8ac64b4-7f4e-4059-abe9-c39de8cc1b2d';
	SET @identityFeatureId = N'01988158-2be7-449e-ac85-d962ddb1dbd3';
	SET @isActive = 1;
	SET @orderNumber = 3;
	SET @level = 3;
	SET @description = 'Menu - Settings - Release Type - Product';

	EXEC PROC_CreateMenuFeature
		@identityMenuFeatureId,
		@identityProductId,
		@identityModuleId,
		@identityFeatureParentId,
		@identityFeatureId,
		@isActive,
		@orderNumber,
		@level,
		@description;

END

--Settings -- SUBITEMS -- Release Type -- Service
 
BEGIN
	SET @identityMenuFeatureId = N'2900ed90-d80f-403c-97ae-f4249316143f';
	SET @identityProductId = 1;
	SET @identityModuleId = 7;
	SET @identityFeatureParentId = N'f8ab19d2-5b97-423f-a383-5d7bb6cfb01e';
	SET @identityFeatureId = N'f8f50ca7-62de-4cf5-8f6a-d6305c1fefce';
	SET @isActive = 1;
	SET @orderNumber = 1;
	SET @level = 4;
	SET @description = 'Menu - Settings - Release Type - Service - Service Group Registration';

	EXEC PROC_CreateMenuFeature
		@identityMenuFeatureId,
		@identityProductId,
		@identityModuleId,
		@identityFeatureParentId,
		@identityFeatureId,
		@isActive,
		@orderNumber,
		@level,
		@description;

END

BEGIN
	SET @identityMenuFeatureId = N'c3323fc8-e7d2-4f36-becf-cd5b0bcc0827';
	SET @identityProductId = 1;
	SET @identityModuleId = 7;
	SET @identityFeatureParentId = N'f8ab19d2-5b97-423f-a383-5d7bb6cfb01e';
	SET @identityFeatureId = N'65dcd507-8d29-489e-83ad-5f2a6efd10cc';
	SET @isActive = 1;
	SET @orderNumber = 2;
	SET @level = 4;
	SET @description = 'Menu - Settings - Release Type - Service - Service Registration';

	EXEC PROC_CreateMenuFeature
		@identityMenuFeatureId,
		@identityProductId,
		@identityModuleId,
		@identityFeatureParentId,
		@identityFeatureId,
		@isActive,
		@orderNumber,
		@level,
		@description;

END

--Settings -- SUBITEMS -- Release Type -- Tax
BEGIN
	SET @identityMenuFeatureId = N'6160f4c7-7036-40e0-a102-515cb416d785';
	SET @identityProductId = 1;
	SET @identityModuleId = 7;
	SET @identityFeatureParentId = N'c10b63f1-0ae0-4551-856e-7b41e826069d';
	SET @identityFeatureId = N'ebbc054c-7102-492e-b09a-b910422f36ef';
	SET @isActive = 1;
	SET @orderNumber = 1;
	SET @level = 4;
	SET @description = 'Menu - Settings - Release Type - Rate - Create Rate';

	EXEC PROC_CreateMenuFeature
		@identityMenuFeatureId,
		@identityProductId,
		@identityModuleId,
		@identityFeatureParentId,
		@identityFeatureId,
		@isActive,
		@orderNumber,
		@level,
		@description;

END


--Settings -- SUBITEMS -- Release Type -- Product
BEGIN
	SET @identityMenuFeatureId = N'a587936a-868b-459f-929d-2ee55bfa41a3';
	SET @identityProductId = 1;
	SET @identityModuleId = 7;
	SET @identityFeatureParentId = N'01988158-2be7-449e-ac85-d962ddb1dbd3';
	SET @identityFeatureId = N'9bbbdbc3-8b74-4ef7-a935-0a0242764449';
	SET @isActive = 1;
	SET @orderNumber = 1;
	SET @level = 4;
	SET @description = 'Menu - Settings - Release Type - POS - POS Register';

	EXEC PROC_CreateMenuFeature
		@identityMenuFeatureId,
		@identityProductId,
		@identityModuleId,
		@identityFeatureParentId,
		@identityFeatureId,
		@isActive,
		@orderNumber,
		@level,
		@description;

END

BEGIN
	SET @identityMenuFeatureId = N'd79263f4-7459-4fd4-9dfb-21f184f7613f';
	SET @identityProductId = 1;
	SET @identityModuleId = 7;
	SET @identityFeatureParentId = N'01988158-2be7-449e-ac85-d962ddb1dbd3';
	SET @identityFeatureId = N'53a35dc8-8898-4581-88f8-3a8666b1b014';
	SET @isActive = 1;
	SET @orderNumber = 2;
	SET @level = 4;
	SET @description = 'Menu - Settings - Release Type - POS - Product Group Registration';

	EXEC PROC_CreateMenuFeature
		@identityMenuFeatureId,
		@identityProductId,
		@identityModuleId,
		@identityFeatureParentId,
		@identityFeatureId,
		@isActive,
		@orderNumber,
		@level,
		@description;

END

BEGIN
	SET @identityMenuFeatureId = N'8573f7cf-6d6b-4a8a-8e93-de5cb0445654';
	SET @identityProductId = 1;
	SET @identityModuleId = 7;
	SET @identityFeatureParentId = N'01988158-2be7-449e-ac85-d962ddb1dbd3';
	SET @identityFeatureId = N'c366829a-8ac5-4fda-9af1-82b291ef7705';
	SET @isActive = 1;
	SET @orderNumber = 3;
	SET @level = 4;
	SET @description = 'Menu - Settings - Release Type - POS - Product Registration';

	EXEC PROC_CreateMenuFeature
		@identityMenuFeatureId,
		@identityProductId,
		@identityModuleId,
		@identityFeatureParentId,
		@identityFeatureId,
		@isActive,
		@orderNumber,
		@level,
		@description;

END

--Settings -- SUBITEMS -- Note Issuance
BEGIN
	SET @identityMenuFeatureId = N'4ab67dc1-0c19-4f18-a8bf-d9337de14c49';
	SET @identityProductId = 1;
	SET @identityModuleId = 7;
	SET @identityFeatureParentId = N'90581cbf-f6bf-4fe7-ad35-d31a907e614f';
	SET @identityFeatureId = N'e25b7351-0f4c-4f5b-9400-c0250b4b959d';
	SET @isActive = 1;
	SET @orderNumber = 1;
	SET @level = 4;
	SET @description = 'Menu - Settings - Note Issuance - Tributes';

	EXEC PROC_CreateMenuFeature
		@identityMenuFeatureId,
		@identityProductId,
		@identityModuleId,
		@identityFeatureParentId,
		@identityFeatureId,
		@isActive,
		@orderNumber,
		@level,
		@description;

END

BEGIN
	SET @identityMenuFeatureId = N'c3fd55ac-8b46-4161-b3fb-0fdef52361e9';
	SET @identityProductId = 1;
	SET @identityModuleId = 7;
	SET @identityFeatureParentId = N'90581cbf-f6bf-4fe7-ad35-d31a907e614f';
	SET @identityFeatureId = N'b6d2c380-6999-4e06-8eec-9570e84503c3';
	SET @isActive = 1;
	SET @orderNumber = 2;
	SET @level = 4;
	SET @description = 'Menu - Settings - Note Issuance - Fiscal Documents';

	EXEC PROC_CreateMenuFeature
		@identityMenuFeatureId,
		@identityProductId,
		@identityModuleId,
		@identityFeatureParentId,
		@identityFeatureId,
		@isActive,
		@orderNumber,
		@level,
		@description;

END

BEGIN
	SET @identityMenuFeatureId = N'0f491313-1a43-4b7e-a15a-26bada0d8430';
	SET @identityProductId = 1;
	SET @identityModuleId = 7;
	SET @identityFeatureParentId = N'90581cbf-f6bf-4fe7-ad35-d31a907e614f';
	SET @identityFeatureId = N'30a32502-c930-4792-9996-6362f38322ee';
	SET @isActive = 1;
	SET @orderNumber = 3;
	SET @level = 4;
	SET @description = 'Menu - Settings - Note Issuance - Download XML NFC-e';

	EXEC PROC_CreateMenuFeature
		@identityMenuFeatureId,
		@identityProductId,
		@identityModuleId,
		@identityFeatureParentId,
		@identityFeatureId,
		@isActive,
		@orderNumber,
		@level,
		@description;

END

--Settings -- SUBITEMS -- Note Issuance -- Tributes
BEGIN
	SET @identityMenuFeatureId = N'e9b227ef-4e36-436c-902d-e74f579a3882';
	SET @identityProductId = 1;
	SET @identityModuleId = 7;
	SET @identityFeatureParentId = N'e25b7351-0f4c-4f5b-9400-c0250b4b959d';
	SET @identityFeatureId = N'a0f446ca-871c-413b-b2c1-e3d9b5f98b33';
	SET @isActive = 0;
	SET @orderNumber = 1;
	SET @level = 5;
	SET @description = 'Menu - Settings - Note Issuance - Tributes - Service';

	EXEC PROC_CreateMenuFeature
		@identityMenuFeatureId,
		@identityProductId,
		@identityModuleId,
		@identityFeatureParentId,
		@identityFeatureId,
		@isActive,
		@orderNumber,
		@level,
		@description;

END

BEGIN
	SET @identityMenuFeatureId = N'2569e97a-8b6e-4b2b-9b2b-933df8d0d8f5';
	SET @identityProductId = 1;
	SET @identityModuleId = 7;
	SET @identityFeatureParentId = N'e25b7351-0f4c-4f5b-9400-c0250b4b959d';
	SET @identityFeatureId = N'16565880-9243-43f4-92a7-7b3a07502b91';
	SET @isActive = 1;
	SET @orderNumber = 2;
	SET @level = 5;
	SET @description = 'Menu - Settings - Note Issuance - Tributes - Product';

	EXEC PROC_CreateMenuFeature
		@identityMenuFeatureId,
		@identityProductId,
		@identityModuleId,
		@identityFeatureParentId,
		@identityFeatureId,
		@isActive,
		@orderNumber,
		@level,
		@description;

END

BEGIN
	SET @identityMenuFeatureId = N'8b97d56c-a419-46fe-9c24-274c61d41483';
	SET @identityProductId = 1;
	SET @identityModuleId = 7;
	SET @identityFeatureParentId = N'e25b7351-0f4c-4f5b-9400-c0250b4b959d';
	SET @identityFeatureId = N'619d3bc4-fa50-4844-9ad0-57f05d2b28a4';
	SET @isActive = 1;
	SET @orderNumber = 3;
	SET @level = 5;
	SET @description = 'Menu - Settings - Note Issuance - Tributes - ISS withheld';

	EXEC PROC_CreateMenuFeature
		@identityMenuFeatureId,
		@identityProductId,
		@identityModuleId,
		@identityFeatureParentId,
		@identityFeatureId,
		@isActive,
		@orderNumber,
		@level,
		@description;

END

BEGIN
	SET @identityMenuFeatureId = N'aafa7cec-cacd-4414-9c51-80d857eb84e6';
	SET @identityProductId = 1;
	SET @identityModuleId = 7;
	SET @identityFeatureParentId = N'e25b7351-0f4c-4f5b-9400-c0250b4b959d';
	SET @identityFeatureId = N'ed38e655-d94a-4d93-900f-f4d30ea71ee1';
	SET @isActive = 1;
	SET @orderNumber = 4;
	SET @level = 5;
	SET @description = 'Menu - Settings - Note Issuance - Tributes - IVA x Serviços/Produtos';

	EXEC PROC_CreateMenuFeature
		@identityMenuFeatureId,
		@identityProductId,
		@identityModuleId,
		@identityFeatureParentId,
		@identityFeatureId,
		@isActive,
		@orderNumber,
		@level,
		@description;

END

--REVENUE MANAGEMENT
BEGIN
	SET @identityMenuFeatureId = N'1a849cc4-676a-4cd2-99e1-61ad544c9472';
	SET @identityProductId = 1;
	SET @identityModuleId = 8;
	SET @identityFeatureParentId = NULL;
	SET @identityFeatureId = N'667bfaa6-b614-49c2-8b81-26eb24d79b21';
	SET @isActive = 1;
	SET @orderNumber = 7;
	SET @level = 1;
	SET @description = 'Menu - Revenue Management';

	EXEC PROC_CreateMenuFeature
		@identityMenuFeatureId,
		@identityProductId,
		@identityModuleId,
		@identityFeatureParentId,
		@identityFeatureId,
		@isActive,
		@orderNumber,
		@level,
		@description;

END

--REVENUE MANAGEMENT - Items
BEGIN
	SET @identityMenuFeatureId = N'15f87fda-ecae-410e-819f-dc3eb2d3a58a';
	SET @identityProductId = 1;
	SET @identityModuleId = 8;
	SET @identityFeatureParentId = N'667bfaa6-b614-49c2-8b81-26eb24d79b21';
	SET @identityFeatureId = N'c7280153-b05a-4a0d-9f23-a62b67c32e9c';
	SET @isActive = 1;
	SET @orderNumber = 1;
	SET @level = 2;
	SET @description = 'Menu - Revenue Management - Pension Register';

	EXEC PROC_CreateMenuFeature
		@identityMenuFeatureId,
		@identityProductId,
		@identityModuleId,
		@identityFeatureParentId,
		@identityFeatureId,
		@isActive,
		@orderNumber,
		@level,
		@description;

END

BEGIN
	SET @identityMenuFeatureId = N'c44d0757-6175-4917-82c2-4cd2adc12fe0';
	SET @identityProductId = 1;
	SET @identityModuleId = 8;
	SET @identityFeatureParentId = N'667bfaa6-b614-49c2-8b81-26eb24d79b21';
	SET @identityFeatureId = N'8bcbfbe6-5f90-42f7-9b5b-c5db88e43b0a';
	SET @isActive = 1;
	SET @orderNumber = 2;
	SET @level = 2;
	SET @description = 'Menu - Revenue Management - Insert Base Rate';

	EXEC PROC_CreateMenuFeature
		@identityMenuFeatureId,
		@identityProductId,
		@identityModuleId,
		@identityFeatureParentId,
		@identityFeatureId,
		@isActive,
		@orderNumber,
		@level,
		@description;

END

BEGIN
	SET @identityMenuFeatureId = N'7b098e71-f999-493f-b884-16ab2db6f29b';
	SET @identityProductId = 1;
	SET @identityModuleId = 8;
	SET @identityFeatureParentId = N'667bfaa6-b614-49c2-8b81-26eb24d79b21';
	SET @identityFeatureId = N'9e799d48-9250-4c20-90b3-5fa40c45f784';
	SET @isActive = 1;
	SET @orderNumber = 3;
	SET @level = 2;
	SET @description = 'Menu - Revenue Management - Trade Agreement';

	EXEC PROC_CreateMenuFeature
		@identityMenuFeatureId,
		@identityProductId,
		@identityModuleId,
		@identityFeatureParentId,
		@identityFeatureId,
		@isActive,
		@orderNumber,
		@level,
		@description;

END

BEGIN
	SET @identityMenuFeatureId = N'd5533065-15d5-4497-8efb-a0bc673e321f';
	SET @identityProductId = 1;
	SET @identityModuleId = 8;
	SET @identityFeatureParentId = N'667bfaa6-b614-49c2-8b81-26eb24d79b21';
	SET @identityFeatureId = N'443b8cf7-8872-47eb-8215-97b09f1e05c0';
	SET @isActive = 1;
	SET @orderNumber = 4;
	SET @level = 2;
	SET @description = 'Menu - Revenue Management - Tariff Schedule';

	EXEC PROC_CreateMenuFeature
		@identityMenuFeatureId,
		@identityProductId,
		@identityModuleId,
		@identityFeatureParentId,
		@identityFeatureId,
		@isActive,
		@orderNumber,
		@level,
		@description;

END



BEGIN
	SET @identityMenuFeatureId = N'639ab6c4-b812-45b8-a01e-6963e824f2c0';
	SET @identityProductId = 1;
	SET @identityModuleId = 8;
	SET @identityFeatureParentId = N'667bfaa6-b614-49c2-8b81-26eb24d79b21';
	SET @identityFeatureId = N'd4a38d6d-83e3-4792-ae33-e2dce6d9f7de';
	SET @isActive = 1;
	SET @orderNumber = 5;
	SET @level = 2;
	SET @description = 'Menu - Revenue Management - Cadastro de Premissas';

	EXEC PROC_CreateMenuFeature
		@identityMenuFeatureId,
		@identityProductId,
		@identityModuleId,
		@identityFeatureParentId,
		@identityFeatureId,
		@isActive,
		@orderNumber,
		@level,
		@description;

END

BEGIN
	SET @identityMenuFeatureId = N'c8e77e25-40d2-405f-9f4f-490cfa22b704';
	SET @identityProductId = 1;
	SET @identityModuleId = 8;
	SET @identityFeatureParentId = N'667bfaa6-b614-49c2-8b81-26eb24d79b21';
	SET @identityFeatureId = N'ac64e720-f26e-4e72-9f79-23921667cf83';
	SET @isActive = 1;
	SET @orderNumber = 6;
	SET @level = 2;
	SET @description = 'Menu - Revenue Management - Rate Strategy';

	EXEC PROC_CreateMenuFeature
		@identityMenuFeatureId,
		@identityProductId,
		@identityModuleId,
		@identityFeatureParentId,
		@identityFeatureId,
		@isActive,
		@orderNumber,
		@level,
		@description;

END

BEGIN
	SET @identityMenuFeatureId = N'afc283ec-c19b-4670-a35b-a504b846e62d';
	SET @identityProductId = 1;
	SET @identityModuleId = 8;
	SET @identityFeatureParentId = N'667bfaa6-b614-49c2-8b81-26eb24d79b21';
	SET @identityFeatureId = N'dece3ff3-ecb2-434f-bb8a-4c3ced260daf';
	SET @isActive = 1;
	SET @orderNumber = 8;
	SET @level = 2;
	SET @description = 'Menu - Revenue Management - Mealplan Type Rate';

	EXEC PROC_CreateMenuFeature
		@identityMenuFeatureId,
		@identityProductId,
		@identityModuleId,
		@identityFeatureParentId,
		@identityFeatureId,
		@isActive,
		@orderNumber,
		@level,
		@description;

END



BEGIN
	SET @identityMenuFeatureId = N'6FB3CFA3-94B0-4593-B540-A213779E2127';
	SET @identityProductId = 1;
	SET @identityModuleId = 8;
	SET @identityFeatureParentId = '667bfaa6-b614-49c2-8b81-26eb24d79b21';
	SET @identityFeatureId = N'DFD55FC5-0539-49B7-93A9-4243D8840188';
	SET @isActive = 1;
	SET @orderNumber = 8;
	SET @level = 2;
	SET @description = 'Menu - Tariff Level';

	EXEC PROC_CreateMenuFeature
		@identityMenuFeatureId,
		@identityProductId,
		@identityModuleId,
		@identityFeatureParentId,
		@identityFeatureId,
		@isActive,
		@orderNumber,
		@level,
		@description;

END

BEGIN
	SET @identityMenuFeatureId = N'A5DC39B9-986C-46DB-A01C-8D69DCC86273';
	SET @identityProductId = 1;
	SET @identityModuleId = 8;
	SET @identityFeatureParentId = 'DFD55FC5-0539-49B7-93A9-4243D8840188';
	SET @identityFeatureId = N'04f04c5c-4b89-43cc-9750-b60f8ed63bc1';
	SET @isActive = 1;
	SET @orderNumber = 2;
	SET @level = 3;
	SET @description = 'Menu - Tariff Level - Register Rate by Level';

	EXEC PROC_CreateMenuFeature
		@identityMenuFeatureId,
		@identityProductId,
		@identityModuleId,
		@identityFeatureParentId,
		@identityFeatureId,
		@isActive,
		@orderNumber,
		@level,
		@description;

END

BEGIN
	SET @identityMenuFeatureId = N'2d4cc67d-aff7-4bca-b3af-6c77dcc6ed5f';
	SET @identityProductId = 1;
	SET @identityModuleId = 8;
	SET @identityFeatureParentId = 'DFD55FC5-0539-49B7-93A9-4243D8840188';
	SET @identityFeatureId = N'7AA9A04C-2701-4248-91DD-5E5B998F5EB9';
	SET @isActive = 1;
	SET @orderNumber = 1;
	SET @level = 3;
	SET @description = 'Menu - Tariff Level - Rate Level Register';

	EXEC PROC_CreateMenuFeature
		@identityMenuFeatureId,
		@identityProductId,
		@identityModuleId,
		@identityFeatureParentId,
		@identityFeatureId,
		@isActive,
		@orderNumber,
		@level,
		@description;

END


--INTERACTIVE REPORT
BEGIN
	SET @identityMenuFeatureId = N'de157d5e-f476-47f1-b05c-f8b6f2ebbf6d';
	SET @identityProductId = 1;
	SET @identityModuleId = 9;
	SET @identityFeatureParentId = NULL;
	SET @identityFeatureId = N'6a7df67d-bce6-4425-9ce8-352e1864d5b9';
	SET @isActive = 1;
	SET @orderNumber = 9;
	SET @level = 1;
	SET @description = 'Menu - Interactive Report';

	EXEC PROC_CreateMenuFeature
		@identityMenuFeatureId,
		@identityProductId,
		@identityModuleId,
		@identityFeatureParentId,
		@identityFeatureId,
		@isActive,
		@orderNumber,
		@level,
		@description;

END

 --INTERACTIVE REPORT - ITEM
BEGIN
	SET @identityMenuFeatureId = N'47b8c78a-19a0-4c29-a1b4-2c9ff75ecbe0';
	SET @identityProductId = 1;
	SET @identityModuleId = 9;
	SET @identityFeatureParentId = N'6a7df67d-bce6-4425-9ce8-352e1864d5b9';
	SET @identityFeatureId = N'0b28fe2d-52ea-4743-bad4-0ffaee7f73cc';
	SET @isActive = 1;
	SET @orderNumber = 1;
	SET @level = 2;
	SET @description = 'Menu - Interactive Report';

	EXEC PROC_CreateMenuFeature
		@identityMenuFeatureId,
		@identityProductId,
		@identityModuleId,
		@identityFeatureParentId,
		@identityFeatureId,
		@isActive,
		@orderNumber,
		@level,
		@description;

END

 --INTERACTIVE REPORT - DASHBOARD
BEGIN
	SET @identityMenuFeatureId = N'11039cd6-4e53-4499-95e0-6127a48f410b';
	SET @identityProductId = 1;
	SET @identityModuleId = 9;
	SET @identityFeatureParentId = N'6a7df67d-bce6-4425-9ce8-352e1864d5b9';
	SET @identityFeatureId = N'34dddb52-95aa-484e-9108-69c71a2e120e';
	SET @isActive = 1;
	SET @orderNumber = 2;
	SET @level = 2;
	SET @description = 'Menu - Interactive Dashboard';

	EXEC PROC_CreateMenuFeature
		@identityMenuFeatureId,
		@identityProductId,
		@identityModuleId,
		@identityFeatureParentId,
		@identityFeatureId,
		@isActive,
		@orderNumber,
		@level,
		@description;

END

 --INTERACTIVE REPORT - SIBA INTEGRATION
BEGIN
	SET @identityMenuFeatureId = N'bf218815-3a03-4179-8ab8-e4390063b9c4';
	SET @identityProductId = 1;
	SET @identityModuleId = 9;
	SET @identityFeatureParentId = N'6a7df67d-bce6-4425-9ce8-352e1864d5b9';
	SET @identityFeatureId = N'ef53cc7e-9777-40c9-9938-837380dd9d14';
	SET @isActive = 1;
	SET @orderNumber = 3;
	SET @level = 2;
	SET @description = 'Menu - Siba Integration';

	EXEC PROC_CreateMenuFeature
		@identityMenuFeatureId,
		@identityProductId,
		@identityModuleId,
		@identityFeatureParentId,
		@identityFeatureId,
		@isActive,
		@orderNumber,
		@level,
		@description;

END

--INTEGRATION
BEGIN
	SET @identityMenuFeatureId = N'72d97423-0431-479b-9289-7c9317e0780e';
	SET @identityProductId = 1;
	SET @identityModuleId = 10;
	SET @identityFeatureParentId = NULL;
	SET @identityFeatureId = N'589f4583-abad-4db2-b3aa-6085fec95022';
	SET @isActive = 1;
	SET @orderNumber = 8;
	SET @level = 1;
	SET @description = 'Menu - Integration';

	EXEC PROC_CreateMenuFeature
		@identityMenuFeatureId,
		@identityProductId,
		@identityModuleId,
		@identityFeatureParentId,
		@identityFeatureId,
		@isActive,
		@orderNumber,
		@level,
		@description;

END

--INTEGRATION - Items
BEGIN
	SET @identityMenuFeatureId = N'767febb1-5148-4e71-a3ae-87a0b29635d0';
	SET @identityProductId = 1;
	SET @identityModuleId = 10;
	SET @identityFeatureParentId = '589f4583-abad-4db2-b3aa-6085fec95022';
	SET @identityFeatureId = N'42cb11d9-1892-40d0-9bb7-d766de0caa29';
	SET @isActive = 1;
	SET @orderNumber = 1;
	SET @level = 2;
	SET @description = 'Menu - Channel';

	EXEC PROC_CreateMenuFeature
		@identityMenuFeatureId,
		@identityProductId,
		@identityModuleId,
		@identityFeatureParentId,
		@identityFeatureId,
		@isActive,
		@orderNumber,
		@level,
		@description;

END

BEGIN
	SET @identityMenuFeatureId = N'734076dd-6345-4d5b-b4aa-8f303d4791c4';
	SET @identityProductId = 1;
	SET @identityModuleId = 10;
	SET @identityFeatureParentId = '589f4583-abad-4db2-b3aa-6085fec95022';
	SET @identityFeatureId = N'ef5cf032-27b0-45a1-b6ea-4a71c22f599a';
	SET @isActive = 1;
	SET @orderNumber = 2;
	SET @level = 2;
	SET @description = 'Menu - Reservation Integration Panel';

	EXEC PROC_CreateMenuFeature
		@identityMenuFeatureId,
		@identityProductId,
		@identityModuleId,
		@identityFeatureParentId,
		@identityFeatureId,
		@isActive,
		@orderNumber,
		@level,
		@description;

END



--DELETE - MenuFeature
BEGIN
	SET @identityMenuFeatureId = N'551d3e4b-aafa-4e1e-a60b-a92e5668d58a';

	EXEC PROC_DeleteMenuFeature
		@identityMenuFeatureId;

END

BEGIN
	SET @identityMenuFeatureId = N'5afe796a-9d5b-4000-9871-636d98ae91f8';

	EXEC PROC_DeleteMenuFeature
		@identityMenuFeatureId;

END

BEGIN
	SET @identityMenuFeatureId = N'b4f28db0-d061-4948-b079-27de4e2a212f';

	EXEC PROC_DeleteMenuFeature
		@identityMenuFeatureId;

END
GO