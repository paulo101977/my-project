﻿ --1 -> Painel
 --2 -> Home
 --3 -> Hospedagem
 --4 -> Financeiro
 --5 -> Reserva
 --6 -> Governança
 --7 -> Cadastro
 --8 -> Revenue Management
 --9 -> Relatório Interativo
 
 --pt-BR
 --es-AR
 --en-US
 --pt-PT

 --HOME

 	DECLARE @featureId UNIQUEIDENTIFIER;
	DECLARE @identityProductId int;
	DECLARE @identityModuleId int;
	DECLARE @featureKey varchar(200);
	DECLARE @PermissionNamePtBr varchar(150);
	DECLARE @PermissionNamePtPt varchar(150);
	DECLARE @PermissionNameEnUs varchar(150);
	DECLARE @PermissionNameEsAr varchar(150);

 BEGIN
	SET @featureId = N'fbc4b2ef-a5ac-4ad5-9abe-057245d4d1d1';
	SET @identityProductId = 1;
	SET @identityModuleId = 2;
	SET @featureKey = 'PMS_Menu_Home';
	SET @PermissionNamePtBr = 'Home';
	SET @PermissionNamePtPt = 'Home';
	SET @PermissionNameEsAr = 'Página de Inicio';
	SET @PermissionNameEnUs = 'Home';

	EXEC PROC_CreatePublicFeature
		@featureId, @identityProductId,@identityModuleId,@featureKey,@PermissionNamePtBr,@PermissionNamePtPt,@PermissionNameEnUs,@PermissionNameEsAr
		
END

BEGIN
	SET @featureId = N'a90f35b5-4948-4bd5-b8c5-06d0febc18aa';
	SET @identityProductId = 1;
	SET @identityModuleId = 3;
	SET @featureKey = 'PMS_Menu_Hosting';
	SET @PermissionNamePtBr = 'Hospedagem';
	SET @PermissionNamePtPt = 'Hospedagem';
	SET @PermissionNameEsAr = 'Hospedagem';
	SET @PermissionNameEnUs = 'Hosting';

	EXEC PROC_CreatePublicFeature
		@featureId, @identityProductId,@identityModuleId,@featureKey,@PermissionNamePtBr,@PermissionNamePtPt,@PermissionNameEnUs,@PermissionNameEsAr
		
END

 BEGIN
	SET @featureId = N'7baa48c8-fb1b-46b7-b0da-4f6a63003804';
	SET @identityProductId = 1;
	SET @identityModuleId = 3;
	SET @featureKey = 'PMS_Menu_Checkin';
	SET @PermissionNamePtBr = 'Check-in';
	SET @PermissionNamePtPt = 'Check-in';
	SET @PermissionNameEsAr = 'Check-in';
	SET @PermissionNameEnUs = 'Check-in';

	EXEC PROC_CreatePublicFeature
		@featureId, @identityProductId,@identityModuleId,@featureKey,@PermissionNamePtBr,@PermissionNamePtPt,@PermissionNameEnUs,@PermissionNameEsAr
		
END

 BEGIN
	SET @featureId = N'a2a82619-4f81-48b7-bcea-7daa486de6b9';
	SET @identityProductId = 1;
	SET @identityModuleId = 3;
	SET @featureKey = 'PMS_Menu_Walkin';
	SET @PermissionNamePtBr = 'Walk-in';
	SET @PermissionNamePtPt = 'Walk-in';
	SET @PermissionNameEsAr = 'Walk-in';
	SET @PermissionNameEnUs = 'Walk-in';

	EXEC PROC_CreatePublicFeature
		@featureId, @identityProductId,@identityModuleId,@featureKey,@PermissionNamePtBr,@PermissionNamePtPt,@PermissionNameEnUs,@PermissionNameEsAr
		
END

 BEGIN
	SET @featureId = N'02cb046e-68e4-4347-ae8a-2d2c5b68160c';
	SET @identityProductId = 1;
	SET @identityModuleId = 3;
	SET @featureKey = 'PMS_Menu_Change_Of_UH';
	SET @PermissionNamePtBr = 'Mudança de UH';
	SET @PermissionNamePtPt = 'Mudança de UH';
	SET @PermissionNameEsAr = 'Cambio de habitacíon';
	SET @PermissionNameEnUs = 'Change of UH';

	EXEC PROC_CreatePublicFeature
		@featureId, @identityProductId,@identityModuleId,@featureKey,@PermissionNamePtBr,@PermissionNamePtPt,@PermissionNameEnUs,@PermissionNameEsAr
		
END

 BEGIN
	SET @featureId = N'69ae172a-8525-46f5-a80e-b2f463d9d4c5';
	SET @identityProductId = 1;
	SET @identityModuleId = 3;
	SET @featureKey = 'PMS_Menu_Audit';
	SET @PermissionNamePtBr = 'Auditoria';
	SET @PermissionNamePtPt = 'Auditoria';
	SET @PermissionNameEsAr = 'Auditoría';
	SET @PermissionNameEnUs = 'Audit';

	EXEC PROC_CreatePublicFeature
		@featureId, @identityProductId,@identityModuleId,@featureKey,@PermissionNamePtBr,@PermissionNamePtPt,@PermissionNameEnUs,@PermissionNameEsAr
		
END

 BEGIN
	SET @featureId = N'e82caa5b-bc0a-4a85-9c6b-44a2e9b3505c';
	SET @identityProductId = 1;
	SET @identityModuleId = 4;
	SET @featureKey = 'PMS_Menu_Cash_Operation';
	SET @PermissionNamePtBr = 'Operação de Caixa';
	SET @PermissionNamePtPt = 'Operação de Caixa';
	SET @PermissionNameEsAr = 'Operación de Caja';
	SET @PermissionNameEnUs = 'Cash Operation';

	EXEC PROC_CreatePublicFeature
		@featureId, @identityProductId,@identityModuleId,@featureKey,@PermissionNamePtBr,@PermissionNamePtPt,@PermissionNameEnUs,@PermissionNameEsAr
		
END

 BEGIN
	SET @featureId = N'3bcb16f2-e068-4628-9e51-e49353a329ed';
	SET @identityProductId = 1;
	SET @identityModuleId = 5;
	SET @featureKey = 'PMS_Menu_Reserve';
	SET @PermissionNamePtBr = 'Reservas';
	SET @PermissionNamePtPt = 'Reservas';
	SET @PermissionNameEsAr = 'Reservas';
	SET @PermissionNameEnUs = 'Reservation';

	EXEC PROC_CreatePublicFeature
		@featureId, @identityProductId,@identityModuleId,@featureKey,@PermissionNamePtBr,@PermissionNamePtPt,@PermissionNameEnUs,@PermissionNameEsAr
		
END

 BEGIN
	SET @featureId = N'c0a43700-bfc3-4b44-a94c-e2d8b27d14b7';
	SET @identityProductId = 1;
	SET @identityModuleId = 5;
	SET @featureKey = 'PMS_Menu_Consult_Reservation';
	SET @PermissionNamePtBr = 'Consultar Reserva';
	SET @PermissionNamePtPt = 'Consultar Reserva';
	SET @PermissionNameEsAr = 'Consultar Reserva';
	SET @PermissionNameEnUs = 'Consult Reservation';

	EXEC PROC_CreatePublicFeature
		@featureId, @identityProductId,@identityModuleId,@featureKey,@PermissionNamePtBr,@PermissionNamePtPt,@PermissionNameEnUs,@PermissionNameEsAr
		
END

 BEGIN
	SET @featureId = N'e55ee818-c5f4-444c-9ea0-6c6a6182a0d3';
	SET @identityProductId = 1;
	SET @identityModuleId = 5;
	SET @featureKey = 'PMS_Menu_New_Booking';
	SET @PermissionNamePtBr = 'Nova Reserva';
	SET @PermissionNamePtPt = 'Nova Reserva';
	SET @PermissionNameEsAr = 'Nueva Reserva';
	SET @PermissionNameEnUs = 'New Booking';

	EXEC PROC_CreatePublicFeature
		@featureId, @identityProductId,@identityModuleId,@featureKey,@PermissionNamePtBr,@PermissionNamePtPt,@PermissionNameEnUs,@PermissionNameEsAr
		
END

 BEGIN
	SET @featureId = N'3c564556-dea6-4e24-9b4f-5bde8bccacc7';
	SET @identityProductId = 1;
	SET @identityModuleId = 5;
	SET @featureKey = 'PMS_Menu_Availability';
	SET @PermissionNamePtBr = 'Disponibilidade';
	SET @PermissionNamePtPt = 'Disponibilidade';
	SET @PermissionNameEsAr = 'Disponibilidad';
	SET @PermissionNameEnUs = 'Availability';

	EXEC PROC_CreatePublicFeature
		@featureId, @identityProductId,@identityModuleId,@featureKey,@PermissionNamePtBr,@PermissionNamePtPt,@PermissionNameEnUs,@PermissionNameEsAr
		
END

 BEGIN
	SET @featureId = N'94bcfe83-8d99-4cd6-b0ab-2e760798108c';
	SET @identityProductId = 1;
	SET @identityModuleId = 5;
	SET @featureKey = 'PMS_Menu_Tariff_Suggestion';
	SET @PermissionNamePtBr = 'Sugestão Tarifária';
	SET @PermissionNamePtPt = 'Sugestão Tarifária';
	SET @PermissionNameEsAr = 'Sugestão Tarifária';
	SET @PermissionNameEnUs = 'Tariff Suggestion';

	EXEC PROC_CreatePublicFeature
		@featureId, @identityProductId,@identityModuleId,@featureKey,@PermissionNamePtBr,@PermissionNamePtPt,@PermissionNameEnUs,@PermissionNameEsAr
		
END

 BEGIN
	SET @featureId = N'21f65b27-a692-4603-a536-b292395f729d';
	SET @identityProductId = 1;
	SET @identityModuleId = 6;
	SET @featureKey = 'PMS_Menu_Governance';
	SET @PermissionNamePtBr = 'Governança';
	SET @PermissionNamePtPt = 'Governanta';
	SET @PermissionNameEsAr = 'Ama de Llaves';
	SET @PermissionNameEnUs = 'Governance';

	EXEC PROC_CreatePublicFeature
		@featureId, @identityProductId,@identityModuleId,@featureKey,@PermissionNamePtBr,@PermissionNamePtPt,@PermissionNameEnUs,@PermissionNameEsAr
		
END

 BEGIN
	SET @featureId = N'c004b742-145d-4876-8109-357f2f7358bf';
	SET @identityProductId = 1;
	SET @identityModuleId = 6;
	SET @featureKey = 'PMS_Menu_See_Governance';
	SET @PermissionNamePtBr = 'Consultar Governança';
	SET @PermissionNamePtPt = 'Consultar Governanta';
	SET @PermissionNameEsAr = 'Consulta Ama de llaves';
	SET @PermissionNameEnUs = 'See Governance';

	EXEC PROC_CreatePublicFeature
		@featureId, @identityProductId,@identityModuleId,@featureKey,@PermissionNamePtBr,@PermissionNamePtPt,@PermissionNameEnUs,@PermissionNameEsAr
		
END

 BEGIN
	SET @featureId = N'cb4957a0-ee64-4153-ad3b-abb9a256d565';
	SET @identityProductId = 1;
	SET @identityModuleId = 6;
	SET @featureKey = 'PMS_Menu_See_Management_of_UH';
	SET @PermissionNamePtBr = 'Associar UH';
	SET @PermissionNamePtPt = 'Associar UH';
	SET @PermissionNameEsAr = 'Asociar HAB';
	SET @PermissionNameEnUs = 'Associate UH';

	EXEC PROC_CreatePublicFeature
		@featureId, @identityProductId,@identityModuleId,@featureKey,@PermissionNamePtBr,@PermissionNamePtPt,@PermissionNameEnUs,@PermissionNameEsAr
		
END


 BEGIN
	SET @featureId = N'5efe2258-7269-448e-be73-10073765ae72';
	SET @identityProductId = 1;
	SET @identityModuleId = 6;
	SET @featureKey = 'PMS_Menu_See_Automatic_Management_of_UH';
	SET @PermissionNamePtBr = 'Associar UH Automatico';
	SET @PermissionNamePtPt = 'Associar UH Automatico';
	SET @PermissionNameEsAr = 'Asociacion Automatica de Hab';
	SET @PermissionNameEnUs = 'Automatic Room Association';

	EXEC PROC_CreatePublicFeature
		@featureId, @identityProductId,@identityModuleId,@featureKey,@PermissionNamePtBr,@PermissionNamePtPt,@PermissionNameEnUs,@PermissionNameEsAr
		
END

 BEGIN
	SET @featureId = N'606b78da-9996-4829-8789-9aac2b297da1';
	SET @identityProductId = 1;
	SET @identityModuleId = 7;
	SET @featureKey = 'PMS_Menu_Settings';
	SET @PermissionNamePtBr = 'Cadastros';
	SET @PermissionNamePtPt = 'Cadastros';
	SET @PermissionNameEsAr = 'Cadastros';
	SET @PermissionNameEnUs = 'Registration';

	EXEC PROC_CreatePublicFeature
		@featureId, @identityProductId,@identityModuleId,@featureKey,@PermissionNamePtBr,@PermissionNamePtPt,@PermissionNameEnUs,@PermissionNameEsAr
		
END

 BEGIN
	SET @featureId = N'7b0e7ad5-8bea-4544-b208-5fa25dabc6e8';
	SET @identityProductId = 1;
	SET @identityModuleId = 7;
	SET @featureKey = 'PMS_Menu_Hotel_Management';
	SET @PermissionNamePtBr = 'Gestão de Hotel';
	SET @PermissionNamePtPt = 'Gestão de Hotel';
	SET @PermissionNameEsAr = 'Dirección del Hotel';
	SET @PermissionNameEnUs = 'Hotel Management';

	EXEC PROC_CreatePublicFeature
		@featureId, @identityProductId,@identityModuleId,@featureKey,@PermissionNamePtBr,@PermissionNamePtPt,@PermissionNameEnUs,@PermissionNameEsAr
		
END

 BEGIN
	SET @featureId = N'3b5f7e8b-d8b1-4877-9794-ec5cc568800a';
	SET @identityProductId = 1;
	SET @identityModuleId = 7;
	SET @featureKey = 'PMS_Menu_Network_Management';
	SET @PermissionNamePtBr = 'Gestão de Rede';
	SET @PermissionNamePtPt = 'Gestão de Rede';
	SET @PermissionNameEsAr = 'Dirección del Rede';
	SET @PermissionNameEnUs = 'Network Management';

	EXEC PROC_CreatePublicFeature
		@featureId, @identityProductId,@identityModuleId,@featureKey,@PermissionNamePtBr,@PermissionNamePtPt,@PermissionNameEnUs,@PermissionNameEsAr
		
END

 BEGIN
	SET @featureId = N'f8ac64b4-7f4e-4059-abe9-c39de8cc1b2d';
	SET @identityProductId = 1;
	SET @identityModuleId = 7;
	SET @featureKey = 'PMS_Menu_Release_Type';
	SET @PermissionNamePtBr = 'Tipo de Lançamento';
	SET @PermissionNamePtPt = 'Tipo de Lançamento';
	SET @PermissionNameEsAr = 'Tipo de Lançamento';
	SET @PermissionNameEnUs = 'Release Type';

	EXEC PROC_CreatePublicFeature
		@featureId, @identityProductId,@identityModuleId,@featureKey,@PermissionNamePtBr,@PermissionNamePtPt,@PermissionNameEnUs,@PermissionNameEsAr
		
END

 BEGIN
	SET @featureId = N'5f1060c3-ad19-40e5-ad4e-edbb6562c8f2';
	SET @identityProductId = 1;
	SET @identityModuleId = 7;
	SET @featureKey = 'PMS_Menu_Forms_of_Payment';
	SET @PermissionNamePtBr = 'Formas de Pagamento';
	SET @PermissionNamePtPt = 'Formas de Pagamento';
	SET @PermissionNameEsAr = 'Formas de Pago';
	SET @PermissionNameEnUs = 'Payment Methods';

	EXEC PROC_CreatePublicFeature
		@featureId, @identityProductId,@identityModuleId,@featureKey,@PermissionNamePtBr,@PermissionNamePtPt,@PermissionNameEnUs,@PermissionNameEsAr
		
END

BEGIN
	SET @featureId = N'ade9723b-194d-4e73-a32e-ade39bcea8a9';
	SET @identityProductId = 1;
	SET @identityModuleId = 7;
	SET @featureKey = 'PMS_Menu_User_Management';
	SET @PermissionNamePtBr = 'Gestão do Usuário';
	SET @PermissionNamePtPt = 'Gestão do Usuário';
	SET @PermissionNameEsAr = 'Gestão do Usuário';
	SET @PermissionNameEnUs = 'User Management';

	EXEC PROC_CreatePublicFeature
		@featureId, @identityProductId,@identityModuleId,@featureKey,@PermissionNamePtBr,@PermissionNamePtPt,@PermissionNameEnUs,@PermissionNameEsAr
		
END

-- 6d4705fc-d664-4fe5-baf8-0a2ef073e8d8 -> Produtos
-- f0de89cd-3739-4fd0-8ac1-cb447fa7460c -> Serviços

BEGIN
	SET @featureId = N'90581cbf-f6bf-4fe7-ad35-d31a907e614f';
	SET @identityProductId = 1;
	SET @identityModuleId = 7;
	SET @featureKey = 'PMS_Menu_Invoice_Emissions';
	SET @PermissionNamePtBr = 'Emissões de Nota';
	SET @PermissionNamePtPt = 'Emissões de Factura';
	SET @PermissionNameEsAr = 'Emissões de Nota';
	SET @PermissionNameEnUs = 'Invoice Emissions';

	EXEC PROC_CreatePublicFeature
		@featureId, @identityProductId,@identityModuleId,@featureKey,@PermissionNamePtBr,@PermissionNamePtPt,@PermissionNameEnUs,@PermissionNameEsAr
		
END

BEGIN
	SET @featureId = N'619d3bc4-fa50-4844-9ad0-57f05d2b28a4';
	SET @identityProductId = 1;
	SET @identityModuleId = 7;
	SET @featureKey = 'PMS_Menu_ISS_Withheld';
	SET @PermissionNamePtBr = 'ISS Retido';
	SET @PermissionNamePtPt = 'ISS Retido';
	SET @PermissionNameEsAr = 'ISS Retido';
	SET @PermissionNameEnUs = 'ISS Withheld';

	EXEC PROC_CreatePublicFeature
		@featureId, @identityProductId,@identityModuleId,@featureKey,@PermissionNamePtBr,@PermissionNamePtPt,@PermissionNameEnUs,@PermissionNameEsAr
		
END

BEGIN
	SET @featureId = N'e25b7351-0f4c-4f5b-9400-c0250b4b959d';
	SET @identityProductId = 1;
	SET @identityModuleId = 7;
	SET @featureKey = 'PMS_Menu_Tributes';
	SET @PermissionNamePtBr = 'Tributos';
	SET @PermissionNamePtPt = 'Tributos';
	SET @PermissionNameEsAr = 'Tributos';
	SET @PermissionNameEnUs = 'Tax';

	EXEC PROC_CreatePublicFeature
		@featureId, @identityProductId,@identityModuleId,@featureKey,@PermissionNamePtBr,@PermissionNamePtPt,@PermissionNameEnUs,@PermissionNameEsAr
		
END

BEGIN
	SET @featureId = N'a0f446ca-871c-413b-b2c1-e3d9b5f98b33';
	SET @identityProductId = 1;
	SET @identityModuleId = 7;
	SET @featureKey = 'PMS_Menu_Tributes_Service';
	SET @PermissionNamePtBr = 'Tributo de serviço';
	SET @PermissionNamePtPt = 'Tributo de serviço';
	SET @PermissionNameEsAr = 'Tributo de serviço';
	SET @PermissionNameEnUs = 'Service tribute';

	EXEC PROC_CreatePublicFeature
		@featureId, @identityProductId,@identityModuleId,@featureKey,@PermissionNamePtBr,@PermissionNamePtPt,@PermissionNameEnUs,@PermissionNameEsAr
		
END

BEGIN
	SET @featureId = N'16565880-9243-43f4-92a7-7b3a07502b91';
	SET @identityProductId = 1;
	SET @identityModuleId = 7;
	SET @featureKey = 'PMS_Menu_Tributes_Product';
	SET @PermissionNamePtBr = 'Tributo de produto';
	SET @PermissionNamePtPt = 'Tributo de produto';
	SET @PermissionNameEsAr = 'Tributo de produto';
	SET @PermissionNameEnUs = 'Product tribute';

	EXEC PROC_CreatePublicFeature
		@featureId, @identityProductId,@identityModuleId,@featureKey,@PermissionNamePtBr,@PermissionNamePtPt,@PermissionNameEnUs,@PermissionNameEsAr
		
END

BEGIN
	SET @featureId = N'b6d2c380-6999-4e06-8eec-9570e84503c3';
	SET @identityProductId = 1;
	SET @identityModuleId = 7;
	SET @featureKey = 'PMS_Menu_Fiscal_Documents';
	SET @PermissionNamePtBr = 'Documentos Fiscais';
	SET @PermissionNamePtPt = 'Documentos Fiscais';
	SET @PermissionNameEsAr = 'Documentos Fiscais';
	SET @PermissionNameEnUs = 'Fiscal Documents';

	EXEC PROC_CreatePublicFeature
		@featureId, @identityProductId,@identityModuleId,@featureKey,@PermissionNamePtBr,@PermissionNamePtPt,@PermissionNameEnUs,@PermissionNameEsAr
		
END

BEGIN
	SET @featureId = N'30a32502-c930-4792-9996-6362f38322ee';
	SET @identityProductId = 1;
	SET @identityModuleId = 7;
	SET @featureKey = 'PMS_Menu_Download_XML_NFCE';
	SET @PermissionNamePtBr = 'Download XML NFC-e';
	SET @PermissionNamePtPt = 'Download XML NFC-e';
	SET @PermissionNameEsAr = 'Download XML NFC-e';
	SET @PermissionNameEnUs = 'Download XML NFC-e';

	EXEC PROC_CreatePublicFeature
		@featureId, @identityProductId,@identityModuleId,@featureKey,@PermissionNamePtBr,@PermissionNamePtPt,@PermissionNameEnUs,@PermissionNameEsAr
		
END

 BEGIN
	SET @featureId = N'171b2dfc-7b18-4b2c-b817-8198cfa939df';
	SET @identityProductId = 1;
	SET @identityModuleId = 7;
	SET @featureKey = 'PMS_Menu_Hotel';
	SET @PermissionNamePtBr = 'Hotel';
	SET @PermissionNamePtPt = 'Hotel';
	SET @PermissionNameEsAr = 'Hotel';
	SET @PermissionNameEnUs = 'Hotel';

	EXEC PROC_CreatePublicFeature
		@featureId, @identityProductId,@identityModuleId,@featureKey,@PermissionNamePtBr,@PermissionNamePtPt,@PermissionNameEnUs,@PermissionNameEsAr
		
END

 BEGIN
	SET @featureId = N'32c7ce25-4f30-4f99-a443-e6fa12698e14';
	SET @identityProductId = 1;
	SET @identityModuleId = 7;
	SET @featureKey = 'PMS_Menu_Kind_of_UH';
	SET @PermissionNamePtBr = 'Tipo de UH';
	SET @PermissionNamePtPt = 'Tipo de UH';
	SET @PermissionNameEsAr = 'Tipo de HAB';
	SET @PermissionNameEnUs = 'Type of UH';

	EXEC PROC_CreatePublicFeature
		@featureId, @identityProductId,@identityModuleId,@featureKey,@PermissionNamePtBr,@PermissionNamePtPt,@PermissionNameEnUs,@PermissionNameEsAr
		
END

 BEGIN
	SET @featureId = N'22662ccb-7773-4aaa-ab81-f9e080047112';
	SET @identityProductId = 1;
	SET @identityModuleId = 7;
	SET @featureKey = 'PMS_Menu_UH';
	SET @PermissionNamePtBr = 'UH';
	SET @PermissionNamePtPt = 'Quarto';
	SET @PermissionNameEsAr = 'HAB';
	SET @PermissionNameEnUs = 'UH';

	EXEC PROC_CreatePublicFeature
		@featureId, @identityProductId,@identityModuleId,@featureKey,@PermissionNamePtBr,@PermissionNamePtPt,@PermissionNameEnUs,@PermissionNameEsAr
		
END

 BEGIN
	SET @featureId = N'4d9ad143-4667-4fff-952b-df137e22939e';
	SET @identityProductId = 1;
	SET @identityModuleId = 7;
	SET @featureKey = 'PMS_Menu_Type_of_Guest';
	SET @PermissionNamePtBr = 'Tipo de Hóspede';
	SET @PermissionNamePtPt = 'Tipo de Hóspede';
	SET @PermissionNameEsAr = 'Tipo de Huésped';
	SET @PermissionNameEnUs = 'Type of Guest';

	EXEC PROC_CreatePublicFeature
		@featureId, @identityProductId,@identityModuleId,@featureKey,@PermissionNamePtBr,@PermissionNamePtPt,@PermissionNameEnUs,@PermissionNameEsAr
		
END

 BEGIN
	SET @featureId = N'd9682a67-f090-40b3-b7d4-ca9b2ef02483';
	SET @identityProductId = 1;
	SET @identityModuleId = 7;
	SET @featureKey = 'PMS_Menu_Currency_Quotation';
	SET @PermissionNamePtBr = 'Cotação de Moeda';
	SET @PermissionNamePtPt = 'Cotação de Moeda';
	SET @PermissionNameEsAr = 'Cotización de moneda';
	SET @PermissionNameEnUs = 'Currency Quotation';

	EXEC PROC_CreatePublicFeature
		@featureId, @identityProductId,@identityModuleId,@featureKey,@PermissionNamePtBr,@PermissionNamePtPt,@PermissionNameEnUs,@PermissionNameEsAr
		
END

 BEGIN
	SET @featureId = N'46a4a363-abab-47e3-ae89-4d96f22c8e33';
	SET @identityProductId = 1;
	SET @identityModuleId = 7;
	SET @featureKey = 'PMS_Menu_Reason';
	SET @PermissionNamePtBr = 'Motivo';
	SET @PermissionNamePtPt = 'Motivo';
	SET @PermissionNameEsAr = 'Razón';
	SET @PermissionNameEnUs = 'Reason';

	EXEC PROC_CreatePublicFeature
		@featureId, @identityProductId,@identityModuleId,@featureKey,@PermissionNamePtBr,@PermissionNamePtPt,@PermissionNameEnUs,@PermissionNameEsAr
		
END

 BEGIN
	SET @featureId = N'ea57a045-4017-4646-9a18-9b046b5685d8';
	SET @identityProductId = 1;
	SET @identityModuleId = 7;
	SET @featureKey = 'PMS_Menu_Segment';
	SET @PermissionNamePtBr = 'Segmento';
	SET @PermissionNamePtPt = 'Segmento';
	SET @PermissionNameEsAr = 'Segmento';
	SET @PermissionNameEnUs = 'Segment';

	EXEC PROC_CreatePublicFeature
		@featureId, @identityProductId,@identityModuleId,@featureKey,@PermissionNamePtBr,@PermissionNamePtPt,@PermissionNameEnUs,@PermissionNameEsAr
		
END

 BEGIN
	SET @featureId = N'b1eaf7b2-e16c-4618-af35-076c61ac970a';
	SET @identityProductId = 1;
	SET @identityModuleId = 7;
	SET @featureKey = 'PMS_Menu_Origin';
	SET @PermissionNamePtBr = 'Origem';
	SET @PermissionNamePtPt = 'Origem';
	SET @PermissionNameEsAr = 'Origen';
	SET @PermissionNameEnUs = 'Origin';

	EXEC PROC_CreatePublicFeature
		@featureId, @identityProductId,@identityModuleId,@featureKey,@PermissionNamePtBr,@PermissionNamePtPt,@PermissionNameEnUs,@PermissionNameEsAr
		
END

 BEGIN
	SET @featureId = N'ba0e79d9-9b1a-4d94-87b2-874ac90dd644';
	SET @identityProductId = 1;
	SET @identityModuleId = 7;
	SET @featureKey = 'PMS_Menu_Client';
	SET @PermissionNamePtBr = 'Cliente';
	SET @PermissionNamePtPt = 'Cliente';
	SET @PermissionNameEsAr = 'Cliente';
	SET @PermissionNameEnUs = 'Client';

	EXEC PROC_CreatePublicFeature
		@featureId, @identityProductId,@identityModuleId,@featureKey,@PermissionNamePtBr,@PermissionNamePtPt,@PermissionNameEnUs,@PermissionNameEsAr
		
END

 BEGIN
	SET @featureId = N'e3dba002-e32d-400e-97df-df41b1912f88';
	SET @identityProductId = 1;
	SET @identityModuleId = 7;
	SET @featureKey = 'PMS_Menu_Customer_Category';
	SET @PermissionNamePtBr = 'Categoria de Cliente';
	SET @PermissionNamePtPt = 'Categoria de Cliente';
	SET @PermissionNameEsAr = 'Categoría de Cliente';
	SET @PermissionNameEnUs = 'Customer Category';

	EXEC PROC_CreatePublicFeature
		@featureId, @identityProductId,@identityModuleId,@featureKey,@PermissionNamePtBr,@PermissionNamePtPt,@PermissionNameEnUs,@PermissionNameEsAr
		
END

 BEGIN
	SET @featureId = N'f8ab19d2-5b97-423f-a383-5d7bb6cfb01e';
	SET @identityProductId = 1;
	SET @identityModuleId = 7;
	SET @featureKey = 'PMS_Menu_Service';
	SET @PermissionNamePtBr = 'Serviço';
	SET @PermissionNamePtPt = 'Serviço';
	SET @PermissionNameEsAr = 'Servicios';
	SET @PermissionNameEnUs = 'Service';

	EXEC PROC_CreatePublicFeature
		@featureId, @identityProductId,@identityModuleId,@featureKey,@PermissionNamePtBr,@PermissionNamePtPt,@PermissionNameEnUs,@PermissionNameEsAr
		
END

 BEGIN
	SET @featureId = N'c10b63f1-0ae0-4551-856e-7b41e826069d';
	SET @identityProductId = 1;
	SET @identityModuleId = 7;
	SET @featureKey = 'PMS_Menu_Rate';
	SET @PermissionNamePtBr = 'Taxa';
	SET @PermissionNamePtPt = 'Taxa';
	SET @PermissionNameEsAr = 'Tasa';
	SET @PermissionNameEnUs = 'Rate';

	EXEC PROC_CreatePublicFeature
		@featureId, @identityProductId,@identityModuleId,@featureKey,@PermissionNamePtBr,@PermissionNamePtPt,@PermissionNameEnUs,@PermissionNameEsAr
		
END

 BEGIN
	SET @featureId = N'01988158-2be7-449e-ac85-d962ddb1dbd3';
	SET @identityProductId = 1;
	SET @identityModuleId = 7;
	SET @featureKey = 'PMS_Menu_Product';
	SET @PermissionNamePtBr = 'Produto';
	SET @PermissionNamePtPt = 'Produto';
	SET @PermissionNameEsAr = 'Producto';
	SET @PermissionNameEnUs = 'Product';

	EXEC PROC_CreatePublicFeature
		@featureId, @identityProductId,@identityModuleId,@featureKey,@PermissionNamePtBr,@PermissionNamePtPt,@PermissionNameEnUs,@PermissionNameEsAr
		
END

 BEGIN
	SET @featureId = N'f8f50ca7-62de-4cf5-8f6a-d6305c1fefce';
	SET @identityProductId = 1;
	SET @identityModuleId = 7;
	SET @featureKey = 'PMS_Menu_Service_Group_Registration';
	SET @PermissionNamePtBr = 'Cadastro de Grupo de Serviços';
	SET @PermissionNamePtPt = 'Cadastro de Grupo de Serviços';
	SET @PermissionNameEsAr = 'Registro de Grupo de Servicios';
	SET @PermissionNameEnUs = 'Service Group Registration';

	EXEC PROC_CreatePublicFeature
		@featureId, @identityProductId,@identityModuleId,@featureKey,@PermissionNamePtBr,@PermissionNamePtPt,@PermissionNameEnUs,@PermissionNameEsAr
		
END


 BEGIN
	SET @featureId = N'65dcd507-8d29-489e-83ad-5f2a6efd10cc';
	SET @identityProductId = 1;
	SET @identityModuleId = 7;
	SET @featureKey = 'PMS_Menu_Service_Registration';
	SET @PermissionNamePtBr = 'Cadastro de Serviço';
	SET @PermissionNamePtPt = 'Cadastro de Serviço';
	SET @PermissionNameEsAr = 'Registro de Servicio';
	SET @PermissionNameEnUs = 'Service Registration';

	EXEC PROC_CreatePublicFeature
		@featureId, @identityProductId,@identityModuleId,@featureKey,@PermissionNamePtBr,@PermissionNamePtPt,@PermissionNameEnUs,@PermissionNameEsAr
		
END

 BEGIN
	SET @featureId = N'ebbc054c-7102-492e-b09a-b910422f36ef';
	SET @identityProductId = 1;
	SET @identityModuleId = 7;
	SET @featureKey = 'PMS_Menu_Create_Rate';
	SET @PermissionNamePtBr = 'Cadastro de Taxa';
	SET @PermissionNamePtPt = 'Cadastro de Taxa';
	SET @PermissionNameEsAr = 'Registro de Tasa';
	SET @PermissionNameEnUs = 'Create Rate';

	EXEC PROC_CreatePublicFeature
		@featureId, @identityProductId,@identityModuleId,@featureKey,@PermissionNamePtBr,@PermissionNamePtPt,@PermissionNameEnUs,@PermissionNameEsAr
		
END

 BEGIN
	SET @featureId = N'9bbbdbc3-8b74-4ef7-a935-0a0242764449';
	SET @identityProductId = 1;
	SET @identityModuleId = 7;
	SET @featureKey = 'PMS_Menu_POS_Register';
	SET @PermissionNamePtBr = 'Cadastro de POS';
	SET @PermissionNamePtPt = 'Cadastro de POS';
	SET @PermissionNameEsAr = 'Registro de POS';
	SET @PermissionNameEnUs = 'POS Register';

	EXEC PROC_CreatePublicFeature
		@featureId, @identityProductId,@identityModuleId,@featureKey,@PermissionNamePtBr,@PermissionNamePtPt,@PermissionNameEnUs,@PermissionNameEsAr
		
END

 BEGIN
	SET @featureId = N'53a35dc8-8898-4581-88f8-3a8666b1b014';
	SET @identityProductId = 1;
	SET @identityModuleId = 7;
	SET @featureKey = 'PMS_Menu_Product_Group_Registration';
	SET @PermissionNamePtBr = 'Cadastro de Grupos de Produto';
	SET @PermissionNamePtPt = 'Cadastro de Grupos de Produto';
	SET @PermissionNameEsAr = 'Registro de Grupos de Producto';
	SET @PermissionNameEnUs = 'Product Group Registration';

	EXEC PROC_CreatePublicFeature
		@featureId, @identityProductId,@identityModuleId,@featureKey,@PermissionNamePtBr,@PermissionNamePtPt,@PermissionNameEnUs,@PermissionNameEsAr
		
END

 BEGIN
	SET @featureId = N'c366829a-8ac5-4fda-9af1-82b291ef7705';
	SET @identityProductId = 1;
	SET @identityModuleId = 7;
	SET @featureKey = 'PMS_Menu_Product_Registration';
	SET @PermissionNamePtBr = 'Cadastro de Produtos';
	SET @PermissionNamePtPt = 'Cadastro de Produtos';
	SET @PermissionNameEsAr = 'Registro Productos';
	SET @PermissionNameEnUs = 'Product Registration';

	EXEC PROC_CreatePublicFeature
		@featureId, @identityProductId,@identityModuleId,@featureKey,@PermissionNamePtBr,@PermissionNamePtPt,@PermissionNameEnUs,@PermissionNameEsAr
		
END

 BEGIN
	SET @featureId = N'f5412d6f-135d-4441-a3b8-cb68bcf89e99';
	SET @identityProductId = 1;
	SET @identityModuleId = 7;
	SET @featureKey = 'PMS_Menu_Hotel_Contract';
	SET @PermissionNamePtBr = 'Dados do Contrato';
	SET @PermissionNamePtPt = 'Dados do Contrato';
	SET @PermissionNameEsAr = 'Datos del contrato';
	SET @PermissionNameEnUs = 'Contract Data';

	EXEC PROC_CreatePublicFeature
		@featureId, @identityProductId,@identityModuleId,@featureKey,@PermissionNamePtBr,@PermissionNamePtPt,@PermissionNameEnUs,@PermissionNameEsAr
		
END

 BEGIN
	SET @featureId = N'a3e76c39-c3fb-48e6-b46e-d73fee081a31';
	SET @identityProductId = 1;
	SET @identityModuleId = 7;
	SET @featureKey = 'PMS_Menu_Hotel_Parameters';
	SET @PermissionNamePtBr = 'Parâmetros';
	SET @PermissionNamePtPt = 'Parâmetros';
	SET @PermissionNameEsAr = 'Parámetros';
	SET @PermissionNameEnUs = 'Parameters';

	EXEC PROC_CreatePublicFeature
		@featureId, @identityProductId,@identityModuleId,@featureKey,@PermissionNamePtBr,@PermissionNamePtPt,@PermissionNameEnUs,@PermissionNameEsAr
		
END

 BEGIN
	SET @featureId = N'dd68b842-68df-4f94-95d9-2e63b5cf491f';
	SET @identityProductId = 1;
	SET @identityModuleId = 7;
	SET @featureKey = 'PMS_Menu_Hotel_Policies';
	SET @PermissionNamePtBr = 'Políticas';
	SET @PermissionNamePtPt = 'Políticas';
	SET @PermissionNameEsAr = 'Políticas';
	SET @PermissionNameEnUs = 'Policies';

	EXEC PROC_CreatePublicFeature
		@featureId, @identityProductId,@identityModuleId,@featureKey,@PermissionNamePtBr,@PermissionNamePtPt,@PermissionNameEnUs,@PermissionNameEsAr
		
END

 BEGIN
	SET @featureId = N'162df2b3-21f9-468f-965d-669fc87ff81e';
	SET @identityProductId = 1;
	SET @identityModuleId = 7;
	SET @featureKey = 'PMS_Menu_Hotel_GovernanceStatus';
	SET @PermissionNamePtBr = 'Status da Governaça';
	SET @PermissionNamePtPt = 'Status da Governaça';
	SET @PermissionNameEsAr = 'Estado de gobierno';
	SET @PermissionNameEnUs = 'Governance Status';

	EXEC PROC_CreatePublicFeature
		@featureId, @identityProductId,@identityModuleId,@featureKey,@PermissionNamePtBr,@PermissionNamePtPt,@PermissionNameEnUs,@PermissionNameEsAr
		
END

 BEGIN
	SET @featureId = N'667bfaa6-b614-49c2-8b81-26eb24d79b21';
	SET @identityProductId = 1;
	SET @identityModuleId = 8;
	SET @featureKey = 'PMS_Menu_Revenue_Management';
	SET @PermissionNamePtBr = 'Gestão de Receita';
	SET @PermissionNamePtPt = 'Gestão de Receita';
	SET @PermissionNameEsAr = 'Gestão de Receita';
	SET @PermissionNameEnUs = 'Revenue Management';

	EXEC PROC_CreatePublicFeature
		@featureId, @identityProductId,@identityModuleId,@featureKey,@PermissionNamePtBr,@PermissionNamePtPt,@PermissionNameEnUs,@PermissionNameEsAr
		
END

 BEGIN
	SET @featureId = N'c7280153-b05a-4a0d-9f23-a62b67c32e9c';
	SET @identityProductId = 1;
	SET @identityModuleId = 8;
	SET @featureKey = 'PMS_Menu_Pension_Register';
	SET @PermissionNamePtBr = 'Cadastro de Pensão';
	SET @PermissionNamePtPt = 'Cadastro de Pensão';
	SET @PermissionNameEsAr = 'Registro de Pensión';
	SET @PermissionNameEnUs = 'Pension Register';

	EXEC PROC_CreatePublicFeature
		@featureId, @identityProductId,@identityModuleId,@featureKey,@PermissionNamePtBr,@PermissionNamePtPt,@PermissionNameEnUs,@PermissionNameEsAr
		
END

 BEGIN
	SET @featureId = N'8bcbfbe6-5f90-42f7-9b5b-c5db88e43b0a';
	SET @identityProductId = 1;
	SET @identityModuleId = 8;
	SET @featureKey = 'PMS_Menu_Insert_Base_Rate';
	SET @PermissionNamePtBr = 'Inserir Tarifa Base';
	SET @PermissionNamePtPt = 'Inserir Tarifa Base';
	SET @PermissionNameEsAr = 'Insertar Tarifa Base';
	SET @PermissionNameEnUs = 'Insert Base Rate';

	EXEC PROC_CreatePublicFeature
		@featureId, @identityProductId,@identityModuleId,@featureKey,@PermissionNamePtBr,@PermissionNamePtPt,@PermissionNameEnUs,@PermissionNameEsAr
		
END
	

 BEGIN
	SET @featureId = N'9e799d48-9250-4c20-90b3-5fa40c45f784';
	SET @identityProductId = 1;
	SET @identityModuleId = 8;
	SET @featureKey = 'PMS_Menu_Trade_Agreement';
	SET @PermissionNamePtBr = 'Acordo Comercial';
	SET @PermissionNamePtPt = 'Acordo Comercial';
	SET @PermissionNameEsAr = 'Acuerdo Comercial';
	SET @PermissionNameEnUs = 'Trade Agreement';

	EXEC PROC_CreatePublicFeature
		@featureId, @identityProductId,@identityModuleId,@featureKey,@PermissionNamePtBr,@PermissionNamePtPt,@PermissionNameEnUs,@PermissionNameEsAr
		
END	
	

 BEGIN
	SET @featureId = N'443b8cf7-8872-47eb-8215-97b09f1e05c0';
	SET @identityProductId = 1;
	SET @identityModuleId = 8;
	SET @featureKey = 'PMS_Menu_Tariff_Schedule';
	SET @PermissionNamePtBr = 'Calendário de Tarifas';
	SET @PermissionNamePtPt = 'Calendário de Tarifas';
	SET @PermissionNameEsAr = 'Calendario de Tarifas';
	SET @PermissionNameEnUs = 'Tariff Schedule';

	EXEC PROC_CreatePublicFeature
		@featureId, @identityProductId,@identityModuleId,@featureKey,@PermissionNamePtBr,@PermissionNamePtPt,@PermissionNameEnUs,@PermissionNameEsAr
		
END	
	
	
 BEGIN
	SET @featureId = N'ac64e720-f26e-4e72-9f79-23921667cf83';
	SET @identityProductId = 1;
	SET @identityModuleId = 8;
	SET @featureKey = 'PMS_Menu_Rate_Strategy';
	SET @PermissionNamePtBr = 'Estratégia Tarifária';
	SET @PermissionNamePtPt = 'Estratégia Tarifária';
	SET @PermissionNameEsAr = 'Estratégia Tarifária';
	SET @PermissionNameEnUs = 'Rate Strategy';

	EXEC PROC_CreatePublicFeature
		@featureId, @identityProductId,@identityModuleId,@featureKey,@PermissionNamePtBr,@PermissionNamePtPt,@PermissionNameEnUs,@PermissionNameEsAr
		
END	

 BEGIN
	SET @featureId = N'DFD55FC5-0539-49B7-93A9-4243D8840188';
	SET @identityProductId = 1;
	SET @identityModuleId = 8;
	SET @featureKey = 'PMS_Menu_Level_Tariff';
	SET @PermissionNamePtBr = 'Nível Tarifário';
	SET @PermissionNamePtPt = 'Nível Tarifário';
	SET @PermissionNameEsAr = 'Nível Tarifário';
	SET @PermissionNameEnUs = 'Tariff Level';

	EXEC PROC_CreatePublicFeature
		@featureId, @identityProductId,@identityModuleId,@featureKey,@PermissionNamePtBr,@PermissionNamePtPt,@PermissionNameEnUs,@PermissionNameEsAr
		
END	

 BEGIN
	SET @featureId = N'7AA9A04C-2701-4248-91DD-5E5B998F5EB9';
	SET @identityProductId = 1;
	SET @identityModuleId = 8;
	SET @featureKey = 'PMS_Menu_Level';
	SET @PermissionNamePtBr = 'Cadastro de Nível de Tarifa';
	SET @PermissionNamePtPt = 'Cadastro de Nível de Tarifa';
	SET @PermissionNameEsAr = 'Cadastro de Nível de Tarifa';
	SET @PermissionNameEnUs = 'Rate Level Register';

	EXEC PROC_CreatePublicFeature
		@featureId, @identityProductId,@identityModuleId,@featureKey,@PermissionNamePtBr,@PermissionNamePtPt,@PermissionNameEnUs,@PermissionNameEsAr
		
END	

BEGIN
	SET @featureId = N'04f04c5c-4b89-43cc-9750-b60f8ed63bc1';
	SET @identityProductId = 1;
	SET @identityModuleId = 8;
	SET @featureKey = 'PMS_Menu_Level_Rate';
	SET @PermissionNamePtBr = 'Cadastro de Tarifa por nível';
	SET @PermissionNamePtPt = 'Cadastro de Tarifa por nível';
	SET @PermissionNameEsAr = 'Registro de Tarifa por nivel';
	SET @PermissionNameEnUs = 'Rate register by Level';

	EXEC PROC_CreatePublicFeature
		@featureId, @identityProductId,@identityModuleId,@featureKey,@PermissionNamePtBr,@PermissionNamePtPt,@PermissionNameEnUs,@PermissionNameEsAr
		
END	



 BEGIN
	SET @featureId = N'6a7df67d-bce6-4425-9ce8-352e1864d5b9';
	SET @identityProductId = 1;
	SET @identityModuleId = 9;
	SET @featureKey = 'PMS_Menu_Interactive_Report';
	SET @PermissionNamePtBr = 'Relatório Interativo';
	SET @PermissionNamePtPt = 'Relatório Interativo';
	SET @PermissionNameEsAr = 'Informe Interactivo';
	SET @PermissionNameEnUs = 'Interactive Report';

	EXEC PROC_CreatePublicFeature
		@featureId, @identityProductId,@identityModuleId,@featureKey,@PermissionNamePtBr,@PermissionNamePtPt,@PermissionNameEnUs,@PermissionNameEsAr
		
END	

 BEGIN
	SET @featureId = N'0b28fe2d-52ea-4743-bad4-0ffaee7f73cc';
	SET @identityProductId = 1;
	SET @identityModuleId = 9;
	SET @featureKey = 'PMS_Menu_Reports';
	SET @PermissionNamePtBr = 'Relatórios';
	SET @PermissionNamePtPt = 'Relatórios';
	SET @PermissionNameEsAr = 'Informes';
	SET @PermissionNameEnUs = 'Reports';

	EXEC PROC_CreatePublicFeature
		@featureId, @identityProductId,@identityModuleId,@featureKey,@PermissionNamePtBr,@PermissionNamePtPt,@PermissionNameEnUs,@PermissionNameEsAr
		
END	

BEGIN
	SET @featureId = N'dece3ff3-ecb2-434f-bb8a-4c3ced260daf';
	SET @identityProductId = 1;
	SET @identityModuleId = 8;
	SET @featureKey = 'PMS_Menu_PropertyMealPlanTypeRate';
	SET @PermissionNamePtBr = 'Configurar Pensão';
	SET @PermissionNamePtPt = 'Configurar Pensão';
	SET @PermissionNameEsAr = 'Gestión Comidas';
	SET @PermissionNameEnUs = 'Mealplan Management';

	EXEC PROC_CreatePublicFeature
		@featureId, @identityProductId,@identityModuleId,@featureKey,@PermissionNamePtBr,@PermissionNamePtPt,@PermissionNameEnUs,@PermissionNameEsAr
		
END	

BEGIN
	SET @featureId = N'd4a38d6d-83e3-4792-ae33-e2dce6d9f7de';
	SET @identityProductId = 1;
	SET @identityModuleId = 8;
	SET @featureKey = 'PMS_Menu_PropertyPremise';
	SET @PermissionNamePtBr = 'Cadastro de Premissas';
	SET @PermissionNamePtPt = 'Cadastro de Premissas';
	SET @PermissionNameEsAr = 'Registro de premisas';
	SET @PermissionNameEnUs = 'Assumptions Register';

	EXEC PROC_CreatePublicFeature
		@featureId, @identityProductId,@identityModuleId,@featureKey,@PermissionNamePtBr,@PermissionNamePtPt,@PermissionNameEnUs,@PermissionNameEsAr
		
END	

 BEGIN
	SET @featureId = N'34dddb52-95aa-484e-9108-69c71a2e120e';
	SET @identityProductId = 1;
	SET @identityModuleId = 9;
	SET @featureKey = 'PMS_Menu_Dashboards';
	SET @PermissionNamePtBr = 'Dashboards';
	SET @PermissionNamePtPt = 'Dashboards';
	SET @PermissionNameEsAr = 'Dashboards';
	SET @PermissionNameEnUs = 'Dashboards';

	EXEC PROC_CreatePublicFeature
		@featureId, @identityProductId,@identityModuleId,@featureKey,@PermissionNamePtBr,@PermissionNamePtPt,@PermissionNameEnUs,@PermissionNameEsAr
		
END	

BEGIN
	SET @featureId = N'ef53cc7e-9777-40c9-9938-837380dd9d14';
	SET @identityProductId = 1;
	SET @identityModuleId = 9;
	SET @featureKey = 'PMS_Menu_SibaIntegration';
	SET @PermissionNamePtBr = 'Integração Siba';
	SET @PermissionNamePtPt = 'Integração Siba';
	SET @PermissionNameEsAr = 'Integração Siba';
	SET @PermissionNameEnUs = 'Integração Siba';

	EXEC PROC_CreatePublicFeature
		@featureId, @identityProductId,@identityModuleId,@featureKey,@PermissionNamePtBr,@PermissionNamePtPt,@PermissionNameEnUs,@PermissionNameEsAr
		
END	


 BEGIN
	SET @featureId = N'589f4583-abad-4db2-b3aa-6085fec95022';
	SET @identityProductId = 1;
	SET @identityModuleId = 10;
	SET @featureKey = 'PMS_Menu_Integration';
	SET @PermissionNamePtBr = 'Integração';
	SET @PermissionNamePtPt = 'Integração';
	SET @PermissionNameEsAr = 'Integración';
	SET @PermissionNameEnUs = 'Integration';

	EXEC PROC_CreatePublicFeature
		@featureId, @identityProductId,@identityModuleId,@featureKey,@PermissionNamePtBr,@PermissionNamePtPt,@PermissionNameEnUs,@PermissionNameEsAr
		
END	

 BEGIN
	SET @featureId = N'42cb11d9-1892-40d0-9bb7-d766de0caa29';
	SET @identityProductId = 1;
	SET @identityModuleId = 10;
	SET @featureKey = 'PMS_Menu_Channel';
	SET @PermissionNamePtBr = 'Channel';
	SET @PermissionNamePtPt = 'Channel';
	SET @PermissionNameEsAr = 'Channel';
	SET @PermissionNameEnUs = 'Channel';

	EXEC PROC_CreatePublicFeature
		@featureId, @identityProductId,@identityModuleId,@featureKey,@PermissionNamePtBr,@PermissionNamePtPt,@PermissionNameEnUs,@PermissionNameEsAr
		
END	

 BEGIN
	SET @featureId = N'ef5cf032-27b0-45a1-b6ea-4a71c22f599a';
	SET @identityProductId = 1;
	SET @identityModuleId = 10;
	SET @featureKey = 'HIGSINTEGRATION_Menu_Reservation_Integration_Panel';
	SET @PermissionNamePtBr = 'Painel de Integração de Reservas';
	SET @PermissionNamePtPt = 'Painel de Integração de Reservas';
	SET @PermissionNameEsAr = 'Panel de Integración de Reservas';
	SET @PermissionNameEnUs = 'Reservation Integration Panel';

	EXEC PROC_CreatePublicFeature
		@featureId, @identityProductId,@identityModuleId,@featureKey,@PermissionNamePtBr,@PermissionNamePtPt,@PermissionNameEnUs,@PermissionNameEsAr
		
END	

BEGIN
	SET @featureId = N'ed38e655-d94a-4d93-900f-f4d30ea71ee1';
	SET @identityProductId = 1;
	SET @identityModuleId = 7;
	SET @featureKey = 'PMS_Menu_Iva_Services_And_Products';
	SET @PermissionNamePtBr = 'IVA x Serviços/Produtos';
	SET @PermissionNamePtPt = 'IVA x Serviços/Produtos';
	SET @PermissionNameEsAr = 'IVA x Serviços/Produtos';
	SET @PermissionNameEnUs = 'IVA x Serviços/Produtos';

	EXEC PROC_CreatePublicFeature
		@featureId, @identityProductId,@identityModuleId,@featureKey,@PermissionNamePtBr,@PermissionNamePtPt,@PermissionNameEnUs,@PermissionNameEsAr
		
END
GO
