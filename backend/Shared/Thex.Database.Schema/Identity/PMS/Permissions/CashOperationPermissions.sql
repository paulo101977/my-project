﻿	Declare @identityPermissionId UNIQUEIDENTIFIER;
	
	Declare @PermissionNamePtBr varchar(150);
	Declare @PermissionNamePtPt varchar(150);
	Declare @PermissionNameEnUs varchar(150);
	Declare @PermissionNameEsAr varchar(150);

	Declare @identityProductId int;
	Declare @identityModuleId int;
	
	Declare @permissionKeyList varchar(MAX);

-- PMS - CashOperationPermissions
BEGIN
	SET @identityPermissionId = N'87b63d51-b1c7-45b8-9e27-ee6b9e660d44';
	
	SET @PermissionNamePtBr = 'Consultar Contas Operação de Caixa';
	SET @PermissionNamePtPt = 'Consultar Contas Operação de Caixa';
	SET @PermissionNameEnUs = 'Consult Cash Operation';
	SET @PermissionNameEsAr = 'Consultar Operación de Caja';

	SET @identityProductId = 1;
	SET @identityModuleId = 4;

	SET @permissionKeyList = 
	'PMS_Menu_Cash_Operation,' +
	'PMS_BillingAccount_Get_Search';

	EXEC PROC_CreatePermission
		@identityPermissionId,
		@identityProductId,
		@identityModuleId,
		@PermissionNamePtBr,
		@PermissionNamePtPt,
		@PermissionNameEnUs,
		@PermissionNameEsAr,
		@permissionKeyList;
END


-- PMS - New Account Sheet
BEGIN
	SET @identityPermissionId = N'176d9191-a331-4af1-a9f6-fbb46b7c7ae2';
	
	SET @PermissionNamePtBr = 'Nova Conta Avulsa';
	SET @PermissionNamePtPt = 'Nova Conta Avulsa';
	SET @PermissionNameEnUs = 'Nueva Cuenta Directa';
	SET @PermissionNameEsAr = 'New Account Sheet';

	SET @identityProductId = 1;
	SET @identityModuleId = 4;

	SET @permissionKeyList = 
		'PMS_DocumentType_Get_PersonType_CountryCode,'+
		'PMS_MarketSegment_Get,'+
		'PMS_BusinessSource_Get,'+
		'PMS_Country_Get,'+
		'PMS_CompanyClient_Get_SearchWithLocation,'+
		'PMS_BillingAccount_Post_Sparce';

	EXEC PROC_CreatePermission
		@identityPermissionId,
		@identityProductId,
		@identityModuleId,
		@PermissionNamePtBr,
		@PermissionNamePtPt,
		@PermissionNameEnUs,
		@PermissionNameEsAr,
		@permissionKeyList;
END



-- PMS - New Account Guest
BEGIN
	SET @identityPermissionId = N'ba12d505-3eaa-4a56-b7a3-e0f44b3fec0d';
	
	SET @PermissionNamePtBr = 'Nova Conta Hóspede';
	SET @PermissionNamePtPt = 'Nova Conta Hóspede';
	SET @PermissionNameEnUs = 'New Account Guest';
	SET @PermissionNameEsAr = 'Nueva Cuenta Huésped';

	SET @identityProductId = 1;
	SET @identityModuleId = 4;

	SET @permissionKeyList = 
		'PMS_ReservationGuest_Get_Param,'+
		'PMS_BillingAccount_Post_Add,'+
		'PMS_BillingAccount_Post';

	EXEC PROC_CreatePermission
		@identityPermissionId,
		@identityProductId,
		@identityModuleId,
		@PermissionNamePtBr,
		@PermissionNamePtPt,
		@PermissionNameEnUs,
		@PermissionNameEsAr,
		@permissionKeyList;
END


-- PMS - New Account Company
BEGIN
	SET @identityPermissionId = N'2147a05a-ff81-43fa-b8c1-0757974eb77f';
	
	SET @PermissionNamePtBr = 'Nova Conta Empresa';
	SET @PermissionNamePtPt = 'Nova Conta Empresa';
	SET @PermissionNameEnUs = 'New Account Company';
	SET @PermissionNameEsAr = 'Nueva Cuenta Empresa';

	SET @identityProductId = 1;
	SET @identityModuleId = 4;

	SET @permissionKeyList = 
		'PMS_ReservationCompanyClient_Get_Param,'+
		'PMS_BillingAccount_Post_Add,'+
		'PMS_BillingAccount_Post';


	EXEC PROC_CreatePermission
		@identityPermissionId,
		@identityProductId,
		@identityModuleId,
		@PermissionNamePtBr,
		@PermissionNamePtPt,
		@PermissionNameEnUs,
		@PermissionNameEsAr,
		@permissionKeyList;
END


-- PMS - View Account
BEGIN
	SET @identityPermissionId = N'2147a05a-ff81-43fa-b8c1-0757974eb77f';
	
	SET @PermissionNamePtBr = 'Visualizar Conta';
	SET @PermissionNamePtPt = 'Visualizar Conta';
	SET @PermissionNameEnUs = 'View Account';
	SET @PermissionNameEsAr = 'Ver Cuenta';

	SET @identityProductId = 1;
	SET @identityModuleId = 4;

	SET @permissionKeyList = 
		'PMS_BillingAccount_Get_AccountEntries,'+
		'PMS_BillingAccount_Get_Header,'+
		'PMS_BillingItem_Get_GetAllServiceForRealese,'+
		'PMS_BillingItem_Get_PaymentTypes,'+
		'PMS_Reason_Get_GetAllReason_Params,'+
		'PMS_Reason_Get,'+
		'PMS_BillingAccount_Get_Sidebar';

	EXEC PROC_CreatePermission
		@identityPermissionId,
		@identityProductId,
		@identityModuleId,
		@PermissionNamePtBr,
		@PermissionNamePtPt,
		@PermissionNameEnUs,
		@PermissionNameEsAr,
		@permissionKeyList;
END


-- PMS - Add Account
BEGIN
	SET @identityPermissionId = N'4d7e7d6f-030e-45df-84df-3747ea26de0f';
	
	SET @PermissionNamePtBr = 'Adicionar Conta';
	SET @PermissionNamePtPt = 'Adicionar Conta';
	SET @PermissionNameEnUs = 'Add Account';
	SET @PermissionNameEsAr = 'Añadir Cuenta';

	SET @identityProductId = 1;
	SET @identityModuleId = 4;

	SET @permissionKeyList = 
		'PMS_BillingAccount_Post_Add';

	EXEC PROC_CreatePermission
		@identityPermissionId,
		@identityProductId,
		@identityModuleId,
		@PermissionNamePtBr,
		@PermissionNamePtPt,
		@PermissionNameEnUs,
		@PermissionNameEsAr,
		@permissionKeyList;
END



-- PMS - Reopen Account
BEGIN
	SET @identityPermissionId = N'92262b6d-fc9d-441b-871e-afee2466c60b';
	
	SET @PermissionNamePtBr = 'Reabrir Conta';
	SET @PermissionNamePtPt = 'Reabrir Conta';
	SET @PermissionNameEnUs = 'Reopen Account';
	SET @PermissionNameEsAr = 'Reabrir Cuenta';

	SET @identityProductId = 1;
	SET @identityModuleId = 4;

	SET @permissionKeyList = 
		'PMS_BillingAccount_Post_Reopen,'+
		'PMS_Reason_Get_GetAllReason_Params,'+
		'PMS_Reason_Get';

	EXEC PROC_CreatePermission
		@identityPermissionId,
		@identityProductId,
		@identityModuleId,
		@PermissionNamePtBr,
		@PermissionNamePtPt,
		@PermissionNameEnUs,
		@PermissionNameEsAr,
		@permissionKeyList;
END


-- PMS - Account Statement
BEGIN
	SET @identityPermissionId = N'a9600f48-0937-4c06-9194-ea3bcdbd2e54';
	
	SET @PermissionNamePtBr = 'Extrato da Conta';
	SET @PermissionNamePtPt = 'Extrato da Conta';
	SET @PermissionNameEnUs = 'Account Statement';
	SET @PermissionNameEsAr = 'Extracto de la Cuenta';

	SET @identityProductId = 1;
	SET @identityModuleId = 4;

	SET @permissionKeyList = 
		'PMS_BillingAccount_Get_ForClosure,'+
		'PMS_BillingAccount_Get_AccountEntriesComplete,'+
		'PMS_ReservationItem_Get_Param';

	EXEC PROC_CreatePermission
		@identityPermissionId,
		@identityProductId,
		@identityModuleId,
		@PermissionNamePtBr,
		@PermissionNamePtPt,
		@PermissionNameEnUs,
		@PermissionNameEsAr,
		@permissionKeyList;
END



-- PMS - RPS printing
BEGIN
	SET @identityPermissionId = N'88a77db2-bff3-464f-a7a6-ea6a0dac59e5';
	
	SET @PermissionNamePtBr = 'Impressão do RPS';
	SET @PermissionNamePtPt = 'Impressão do RPS';
	SET @PermissionNameEnUs = 'RPS printing';
	SET @PermissionNameEsAr = 'Impresión del RPS';

	SET @identityProductId = 1;
	SET @identityModuleId = 4;

	SET @permissionKeyList = 
		'PMS_BillingInvoiceIntegration_Get_Invoice_Rps';

	EXEC PROC_CreatePermission
		@identityPermissionId,
		@identityProductId,
		@identityModuleId,
		@PermissionNamePtBr,
		@PermissionNamePtPt,
		@PermissionNameEnUs,
		@PermissionNameEsAr,
		@permissionKeyList;
END


-- PMS - account payment
BEGIN
	SET @identityPermissionId = N'2b4ec9c2-f293-4d0b-8734-5be2c5f3ead8';
	
	SET @PermissionNamePtBr = 'Pagamento de Conta';
	SET @PermissionNamePtPt = 'Pagamento de Conta';
	SET @PermissionNameEnUs = 'Account Payment';
	SET @PermissionNameEsAr = 'Pago de Cuenta';

	SET @identityProductId = 1;
	SET @identityModuleId = 4;

	SET @permissionKeyList = 
		'PMS_BillingAccount_Get_Statement,'+
		'PMS_BillingItem_Get_PaymentTypes,'+
		'PMS_BillingItem_Get_PlasticBrand,'+
		'PMS_BillingAccountClosure_Post,'+
		'PMS_BillingAccount_Post_AccountEntriesList,'+
		'PMS_BillingAccount_Get_ForClosure,'+
		'PMS_BillingAccount_Patch_Person_Header,'+
		'PMS_BillingAccount_Get_Person_Header,'+
		'PMS_BillingInvoice_Post_Create_Credit_Note,'+
		'PMS_BillingInvoice_Get_All_Credit_Note';


	EXEC PROC_CreatePermission
		@identityPermissionId,
		@identityProductId,
		@identityModuleId,
		@PermissionNamePtBr,
		@PermissionNamePtPt,
		@PermissionNameEnUs,
		@PermissionNameEsAr,
		@permissionKeyList;
END



-- PMS - Split Account Item
BEGIN
	SET @identityPermissionId = N'cd5c4764-7ea2-4976-a1c4-704fcc01fe8c';
	
	SET @PermissionNamePtBr = 'Dividir Item da Conta';
	SET @PermissionNamePtPt = 'Dividir Item da Conta';
	SET @PermissionNameEnUs = 'Split Account Item';
	SET @PermissionNameEsAr = 'Dividir elemento de la cuenta';

	SET @identityProductId = 1;
	SET @identityModuleId = 4;

	SET @permissionKeyList = 
		'PMS_BillingAccountItem_Post_Partial';

	EXEC PROC_CreatePermission
		@identityPermissionId,
		@identityProductId,
		@identityModuleId,
		@PermissionNamePtBr,
		@PermissionNamePtPt,
		@PermissionNameEnUs,
		@PermissionNameEsAr,
		@permissionKeyList;
END




-- PMS - Discounting Account Item
BEGIN
	SET @identityPermissionId = N'c1fd7230-55e2-4778-8dca-876bbf83cfef';
	
	SET @PermissionNamePtBr = 'Descontar Item da Conta';
	SET @PermissionNamePtPt = 'Descontar Item da Conta';
	SET @PermissionNameEnUs = 'Discounting Account Item';
	SET @PermissionNameEsAr = 'Descontar elemento de la cuenta';

	SET @identityProductId = 1;
	SET @identityModuleId = 4;

	SET @permissionKeyList = 
		'PMS_BillingAccountItem_Post_Discount,'+
		'PMS_Reason_Get_GetAllReason_Params';

	EXEC PROC_CreatePermission
		@identityPermissionId,
		@identityProductId,
		@identityModuleId,
		@PermissionNamePtBr,
		@PermissionNamePtPt,
		@PermissionNameEnUs,
		@PermissionNameEsAr,
		@permissionKeyList;
END




-- PMS -Unify Account Item
BEGIN
	SET @identityPermissionId = N'5c234273-c71e-41b0-9939-95003bf90ddc';
	
	SET @PermissionNamePtBr = 'Juntar Item da Conta';
	SET @PermissionNamePtPt = 'Unificar Item da Conta';
	SET @PermissionNameEnUs = 'Unify Account Item';
	SET @PermissionNameEsAr = 'Unificar elemento de la cuenta';

	SET @identityProductId = 1;
	SET @identityModuleId = 4;

	SET @permissionKeyList = 
		'PMS_BillingAccountItem_Post_UndoPartial';

	EXEC PROC_CreatePermission
		@identityPermissionId,
		@identityProductId,
		@identityModuleId,
		@PermissionNamePtBr,
		@PermissionNamePtPt,
		@PermissionNameEnUs,
		@PermissionNameEsAr,
		@permissionKeyList;
END



-- PMS -Reverse Account Item
BEGIN
	SET @identityPermissionId = N'56b30649-b343-4e53-b41a-ef8d79ff4734';
	
	SET @PermissionNamePtBr = 'Estornar Item da Conta';
	SET @PermissionNamePtPt = 'Estornar Item da Conta';
	SET @PermissionNameEnUs = 'Reverse Account Item';
	SET @PermissionNameEsAr = 'Estornar elemento de la cuenta';

	SET @identityProductId = 1;
	SET @identityModuleId = 4;

	SET @permissionKeyList = 
		'PMS_BillingAccountItem_Post_Reversal,'+
		'PMS_Reason_Get_GetAllReason_Params';

	EXEC PROC_CreatePermission
		@identityPermissionId,
		@identityProductId,
		@identityModuleId,
		@PermissionNamePtBr,
		@PermissionNamePtPt,
		@PermissionNameEnUs,
		@PermissionNameEsAr,
		@permissionKeyList;
END


-- PMS -Partial Payment
BEGIN
	SET @identityPermissionId = N'788a7e90-d456-4f7c-8611-6a08706471d1';
	
	SET @PermissionNamePtBr = 'Pagamento Parcial';
	SET @PermissionNamePtPt = 'Pagamento Parcial';
	SET @PermissionNameEnUs = 'Partial Payment';
	SET @PermissionNameEsAr = 'Pago Parcial';

	SET @identityProductId = 1;
	SET @identityModuleId = 4;

	SET @permissionKeyList = 
		'PMS_BillingAccountClosure_Post_Partial,'+
		'PMS_BillingItem_Get_PaymentTypes,'+
		'PMS_BillingItem_Get_PlasticBrand';

	EXEC PROC_CreatePermission
		@identityPermissionId,
		@identityProductId,
		@identityModuleId,
		@PermissionNamePtBr,
		@PermissionNamePtPt,
		@PermissionNameEnUs,
		@PermissionNameEsAr,
		@permissionKeyList;
END



-- PMS -Transfer Account Item
BEGIN
	SET @identityPermissionId = N'cbb6f6c9-15ee-4904-a5f9-0621e46e9e5c';
	
	SET @PermissionNamePtBr = 'Transferir Item da Conta';
	SET @PermissionNamePtPt = 'Transferir Item da Conta';
	SET @PermissionNameEnUs = 'Transfer Account Item';
	SET @PermissionNameEsAr = 'Transferencia de elementos de cuenta';

	SET @identityProductId = 1;
	SET @identityModuleId = 4;

	SET @permissionKeyList = 
		'PMS_BillingAccount_Get_AccountEntries,'+
		'PMS_BillingAccount_Get_TransferSearch,'+
		'PMS_BillingAccount_Get_UhOrSparceAccounts,'+
		'PMS_BillingAccountItem_Post_Transfer';


	EXEC PROC_CreatePermission
		@identityPermissionId,
		@identityProductId,
		@identityModuleId,
		@PermissionNamePtBr,
		@PermissionNamePtPt,
		@PermissionNameEnUs,
		@PermissionNameEsAr,
		@permissionKeyList;
END



-- PMS -Undo Transfer Account Item
BEGIN
	SET @identityPermissionId = N'4c432a1d-0671-4825-a514-2b9ba3e3ff63';
	
	SET @PermissionNamePtBr = 'Desfazer Transferir Item da Conta';
	SET @PermissionNamePtPt = 'Desfazer Transferir Item da Conta';
	SET @PermissionNameEnUs = 'Undo Transfer Account Item';
	SET @PermissionNameEsAr = 'Desfazer Transferencia de elementos de cuenta';

	SET @identityProductId = 1;
	SET @identityModuleId = 4;

	SET @permissionKeyList = 
		'PMS_BillingAccountItem_Post_UndoTransfer';

	EXEC PROC_CreatePermission
		@identityPermissionId,
		@identityProductId,
		@identityModuleId,
		@PermissionNamePtBr,
		@PermissionNamePtPt,
		@PermissionNameEnUs,
		@PermissionNameEsAr,
		@permissionKeyList;
END



-- PMS -Services Launching
BEGIN
	SET @identityPermissionId = N'62556ae8-5aba-47d1-bfe2-ef3768222b78';
	
	SET @PermissionNamePtBr = 'Lançamento de serviços ';
	SET @PermissionNamePtPt = 'Lançamento de serviços ';
	SET @PermissionNameEnUs = 'Services Launching';
	SET @PermissionNameEsAr = 'Lanzamiento de servicios';

	SET @identityProductId = 1;
	SET @identityModuleId = 4;

	SET @permissionKeyList = 
		'PMS_BillingItemCategory_Get_GroupFixed,'+
		'PMS_BillingItem_Get_GetAllServiceForRealese,'+
		'PMS_Currency_Get,'+
		'PMS_PropertyCurrencyExchange_Get_CurrentQuotationList,'+
		'PMS_BillingItem_Get_GetAllBillingItemService_Search,'+
		'PMS_BillingItem_Get_GetAllBillingItemService,'+
		'PMS_BillingAccountItem_Post_Debit,'+
		'PDV_ProductBillingItem_Get,'+
		'PMS_BillingItemCategory_Get_GetAllCategoriesForItem,'+
		'PREF_UserPreference_Get_MostSelected';

	EXEC PROC_CreatePermission
		@identityPermissionId,
		@identityProductId,
		@identityModuleId,
		@PermissionNamePtBr,
		@PermissionNamePtPt,
		@PermissionNameEnUs,
		@PermissionNameEsAr,
		@permissionKeyList;
END



-- PMS -Credits Launching
BEGIN
	SET @identityPermissionId = N'764d104f-0383-4d3d-9efd-3917e32b48db';
	
	SET @PermissionNamePtBr = 'Lançamento de Créditos';
	SET @PermissionNamePtPt = 'Lançamento de Créditos';
	SET @PermissionNameEnUs = 'Credits Launching';
	SET @PermissionNameEsAr = 'Lanzamiento de Créditos';

	SET @identityProductId = 1;
	SET @identityModuleId = 4;

	SET @permissionKeyList = 
		'PMS_PropertyCurrencyExchange_Get_CurrentQuotationList,'+
		'PMS_BillingItem_Get_PaymentTypes,'+
		'PMS_BillingItem_Get_AllPaymentTypes,'+
		'PMS_Currency_Get,'+
		'PMS_BillingItem_Get_PlasticBrand,'+
		'PMS_BillingAccountItem_Post_Credit,'+
		'PDV_ProductBillingItem_Get,'+
		'PMS_BillingItemCategory_Get_GetAllCategoriesForItem,'+
		'PREF_UserPreference_Get_MostSelected';

	EXEC PROC_CreatePermission
		@identityPermissionId,
		@identityProductId,
		@identityModuleId,
		@PermissionNamePtBr,
		@PermissionNamePtPt,
		@PermissionNameEnUs,
		@PermissionNameEsAr,
		@permissionKeyList;
END



-- PMS -Products Launching
BEGIN
	SET @identityPermissionId = N'aec8ba1f-2ca0-437b-9515-ad933c25d12a';
	
	SET @PermissionNamePtBr = 'Lançamento de Produtos';
	SET @PermissionNamePtPt = 'Lançamento de Produtos';
	SET @PermissionNameEnUs = 'Products Launching';
	SET @PermissionNameEsAr = 'Lanzamiento de Productos';

	SET @identityProductId = 1;
	SET @identityModuleId = 4;

	SET @permissionKeyList = 
		'PDV_ProductCategory_Get_GetAllByBillingItemId,'+
		'PDV_Product_Get_Category,'+
		'PDV_BillingAccountItem_Post_Debit,'+
		'PDV_Product_Get_Search,'+
		'PDV_BillingItem_Get,'+
		'PREF_UserPreference_Get_BillingItemFav,'+
		'PREF_UserPreference_Post_BillingItemFav,'+
		'PDV_ProductBillingItem_Get,'+
		'PMS_BillingItemCategory_Get_GetAllCategoriesForItem,'+
		'PREF_UserPreference_Get_MostSelected';


	EXEC PROC_CreatePermission
		@identityPermissionId,
		@identityProductId,
		@identityModuleId,
		@PermissionNamePtBr,
		@PermissionNamePtPt,
		@PermissionNameEnUs,
		@PermissionNameEsAr,
		@permissionKeyList;
END

-- PMS -Download Nfce
BEGIN
	SET @identityPermissionId = N'be88d6e7-768c-4a4b-a195-6120b19f9e1d';
	
	SET @PermissionNamePtBr = 'Download NFCE';
	SET @PermissionNamePtPt = 'Download NFCE';
	SET @PermissionNameEnUs = 'Download NFCE';
	SET @PermissionNameEsAr = 'Download NFCE';

	SET @identityProductId = 1;
	SET @identityModuleId = 4;

	SET @permissionKeyList = 
		'PMS_Menu_Download_XML_NFCE,'+
		'PMS_BillingInvoiceIntegration_Get_Invoice_Download';


	EXEC PROC_CreatePermission
		@identityPermissionId,
		@identityProductId,
		@identityModuleId,
		@PermissionNamePtBr,
		@PermissionNamePtPt,
		@PermissionNameEnUs,
		@PermissionNameEsAr,
		@permissionKeyList;
END




GO