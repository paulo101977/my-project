﻿	Declare @identityPermissionId UNIQUEIDENTIFIER;
	
	Declare @PermissionNamePtBr varchar(150);
	Declare @PermissionNamePtPt varchar(150);
	Declare @PermissionNameEnUs varchar(150);
	Declare @PermissionNameEsAr varchar(150);

	Declare @identityProductId int;
	Declare @identityModuleId int;
	
	Declare @permissionKeyList varchar(MAX);
-- Home - Hotel Panel
BEGIN
	SET @identityPermissionId = N'781ad618-e2f1-4cd8-b9d6-8646fb76e0b9';
	
	SET @PermissionNamePtBr = 'Painél de Hotéis';
	SET @PermissionNamePtPt = 'Painél de Hotéis';
	SET @PermissionNameEnUs = 'Hotel Panel';
	SET @PermissionNameEsAr = 'Painél de Hotéis';

	SET @identityProductId = 1;
	SET @identityModuleId = 1;

	SET @permissionKeyList = 
	'PMS_Menu_Home,' +
	'PMS_Property_Get_GetByPropertyId';

	EXEC PROC_CreatePermission
		@identityPermissionId,
		@identityProductId,
		@identityModuleId,
		@PermissionNamePtBr,
		@PermissionNamePtPt,
		@PermissionNameEnUs,
		@PermissionNameEsAr,
		@permissionKeyList;
END

GO