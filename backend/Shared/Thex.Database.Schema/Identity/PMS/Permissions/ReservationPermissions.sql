﻿	Declare @identityPermissionId UNIQUEIDENTIFIER;
	
	Declare @PermissionNamePtBr varchar(150);
	Declare @PermissionNamePtPt varchar(150);
	Declare @PermissionNameEnUs varchar(150);
	Declare @PermissionNameEsAr varchar(150);

	Declare @identityProductId int;
	Declare @identityModuleId int;
	
	Declare @permissionKeyList varchar(MAX);

-- PMS - Consult Reservation
BEGIN
	SET @identityPermissionId = N'87b63d51-b1c7-45b8-9e27-ee6b9e660d44';
	
	SET @PermissionNamePtBr = 'Consultar Reserva';
	SET @PermissionNamePtPt = 'Consultar Reserva';
	SET @PermissionNameEnUs = 'Consultar Reserva';
	SET @PermissionNameEsAr = 'Consult Reservation';

	SET @identityProductId = 1;
	SET @identityModuleId = 5;

	SET @permissionKeyList = 
	'PMS_Menu_Consult_Reservation,' +
	'PMS_PropertyMealPlanType_Get_GetAllPropertyMealPlanType,' +
	'PMS_MarketSegment_Get,' +
	'PMS_BusinessSource_Get,' +
	'PMS_RatePlan_Get_RatePlans,' +
	'PMS_Reason_Get,' +
	'PMS_Reservation_Get_Search,' +
	'PMS_Property_Get_RoomTypes';


	EXEC PROC_CreatePermission
		@identityPermissionId,
		@identityProductId,
		@identityModuleId,
		@PermissionNamePtBr,
		@PermissionNamePtPt,
		@PermissionNameEnUs,
		@PermissionNameEsAr,
		@permissionKeyList;
END


-- PMS - reservation slip
BEGIN
	SET @identityPermissionId = N'03a818e4-a58a-42ab-9641-c9d3fd8d6f3b';
	
	SET @PermissionNamePtBr = 'Slip da reserva';
	SET @PermissionNamePtPt = 'Slip da reserva';
	SET @PermissionNameEnUs = 'Reservation Slip';
	SET @PermissionNameEsAr = 'Carta de Confirmacíon de Reserva';

	SET @identityProductId = 1;
	SET @identityModuleId = 5;

	SET @permissionKeyList = 
	'PMS_ReservationItem_Get_ConfirmationReservation,' +
	'PMS_ReservationItem_Post_SendEmail';


	EXEC PROC_CreatePermission
		@identityPermissionId,
		@identityProductId,
		@identityModuleId,
		@PermissionNamePtBr,
		@PermissionNamePtPt,
		@PermissionNameEnUs,
		@PermissionNameEsAr,
		@permissionKeyList;
END


-- PMS - Guest record
BEGIN
	SET @identityPermissionId = N'69faef75-f9d4-4173-a87e-f790df8bd905';
	
	SET @PermissionNamePtBr = 'Ficha de Hóspede';
	SET @PermissionNamePtPt = 'Ficha de Hóspede';
	SET @PermissionNameEnUs = 'Guest record';
	SET @PermissionNameEsAr = 'Ficha del Huésped';

	SET @identityProductId = 1;
	SET @identityModuleId = 5;

	SET @permissionKeyList = 
		'PMS_GuestRegistration_Get_Responsibles,' +
		'PMS_GuestRegistration_Get_ByGuestReservationItem,' +
		'PMS_Country_Get,' +
		'PMS_ReservationItem_Get_GuestGuestRegistration,' +
		'PMS_Reservation_Get_Header,' +
		'PMS_Reason_Get,' +
		'PMS_GeneralOccupation_Get,' +
		'PMS_Nationality_Get,' +
		'PMS_DocumentType_Get_AllByNaturalPersonType,' +
		'PMS_PropertyGuestType_Get_ByProperty,' +
		'PMS_TransportationType_Get,' +
		'PMS_DocumentType_Get';

	EXEC PROC_CreatePermission
		@identityPermissionId,
		@identityProductId,
		@identityModuleId,
		@PermissionNamePtBr,
		@PermissionNamePtPt,
		@PermissionNameEnUs,
		@PermissionNameEsAr,
		@permissionKeyList;
END




-- PMS - Reservation Budget
BEGIN
	SET @identityPermissionId = N'60bd4cc6-fe46-46c7-89bf-4e02e44a57cd';
	
	SET @PermissionNamePtBr = 'Orçamento da Reserva';
	SET @PermissionNamePtPt = 'Orçamento da Reserva';
	SET @PermissionNameEnUs = 'Reservation Budget';
	SET @PermissionNameEsAr = 'Presupuesto de reserva';

	SET @identityProductId = 1;
	SET @identityModuleId = 5;

	SET @permissionKeyList = 
		'PMS_PropertyMealPlanType_Get_GetAllPropertyMealPlanType,' +
		'PMS_ReservationBudget_Get_Param,' +
		'PMS_ReservationItem_Get_Accommodations,' +
		'PMS_Reservation_Get_ReservationHeader,' +
		'PMS_ReservationBudget_Update_Accomodations,' +
		'PMS_ReservationItem_Patch_Budget';

	EXEC PROC_CreatePermission
		@identityPermissionId,
		@identityProductId,
		@identityModuleId,
		@PermissionNamePtBr,
		@PermissionNamePtPt,
		@PermissionNameEnUs,
		@PermissionNameEsAr,
		@permissionKeyList;
END



-- PMS - New Booking
BEGIN
	SET @identityPermissionId = N'90c74cc8-bd22-473c-9279-6cc15bd27a51';
	
	SET @PermissionNamePtBr = 'Nova Reserva';
	SET @PermissionNamePtPt = 'Nova Reserva';
	SET @PermissionNameEnUs = 'New Booking';
	SET @PermissionNameEsAr = 'Nueva Reserva';

	SET @identityProductId = 1;
	SET @identityModuleId = 5;

	SET @permissionKeyList = 
		'PMS_Menu_New_Booking,'+
		'PMS_PropertyParameter_Get_PropertyParameters,'+
		'PMS_BusinessSource_Get,'+
		'PMS_MarketSegment_Get,'+
		'PMS_Reason_Get,'+
		'PMS_PlasticBrandProperty_Get,'+
		'PMS_CompanyClient_Get_Search,'+
		'PMS_CompanyClient_Get_ContactPersons,'+
		'PMS_RoomType_Get_WithOverbooking,'+
		'PMS_ReservationBudget_Get_Param,'+
		'PMS_PropertyMealPlanType_Get_GetAllPropertyMealPlanType,'+
		'PMS_RoomLayout_Get_ByRoomType,'+
		'PMS_Guest_Get_Search,'+
		'PMS_Reservation_Get_NewReservationCode,' +
		'Booking_ExternalRegistration_Post,' +
		'PMS_Reservation_Post';


	EXEC PROC_CreatePermission
		@identityPermissionId,
		@identityProductId,
		@identityModuleId,
		@PermissionNamePtBr,
		@PermissionNamePtPt,
		@PermissionNameEnUs,
		@PermissionNameEsAr,
		@permissionKeyList;
END




-- PMS - Cancel Accommodation
BEGIN
	SET @identityPermissionId = N'67703c7a-3194-410e-aa0f-6819fa620cff';
	
	SET @PermissionNamePtBr = 'Cancelar Acomodação';
	SET @PermissionNamePtPt = 'Cancelar Acomodação';
	SET @PermissionNameEnUs = 'Cancel Accommodation';
	SET @PermissionNameEsAr = 'Cancelar Alojamiento';

	SET @identityProductId = 1;
	SET @identityModuleId = 5;

	SET @permissionKeyList = 
		'PMS_Reason_Get,'+
		'PMS_ReservationItem_Patch_Cancel';


	EXEC PROC_CreatePermission
		@identityPermissionId,
		@identityProductId,
		@identityModuleId,
		@PermissionNamePtBr,
		@PermissionNamePtPt,
		@PermissionNameEnUs,
		@PermissionNameEsAr,
		@permissionKeyList;
END




-- PMS - Room List
BEGIN
	SET @identityPermissionId = N'53d966f9-b9e7-40ed-b545-76eedbe5f7c7';
	
	SET @PermissionNamePtBr = 'Lista de Acomodações/Room List';
	SET @PermissionNamePtPt = 'Lista de Acomodações/Room List';
	SET @PermissionNameEnUs = 'Room List';
	SET @PermissionNameEsAr = 'Lista de Acomodación/Room List';

	SET @identityProductId = 1;
	SET @identityModuleId = 5;

	SET @permissionKeyList = 
		'PMS_ReservationItem_Get_Accommodations,'+
		'PMS_Reservation_Get_ByReservationItem,'+
		'PMS_PropertyGuestType_Get_ByProperty,'+
		'PMS_Country_Get,'+
		'PMS_DocumentType_Get_AllByNaturalPersonType,'+
		'PMS_GuestRegistration_Get_Search,'+
		'PMS_GuestReservationItem_Patch_Associate,'+
		'PMS_GuestReservationItem_Patch_Disassociate';


	EXEC PROC_CreatePermission
		@identityPermissionId,
		@identityProductId,
		@identityModuleId,
		@PermissionNamePtBr,
		@PermissionNamePtPt,
		@PermissionNameEnUs,
		@PermissionNameEsAr,
		@permissionKeyList;
END




-- PMS - Booking Edition
BEGIN
	SET @identityPermissionId = N'684b6a78-daae-4d97-b4d7-9e5cc3a1877c';
	
	SET @PermissionNamePtBr = 'Edição de Reserva';
	SET @PermissionNamePtPt = 'Edição de Reserva';
	SET @PermissionNameEnUs = 'Booking Edition';
	SET @PermissionNameEsAr = 'Edición de Reserva';

	SET @identityProductId = 1;
	SET @identityModuleId = 5;

	SET @permissionKeyList = 
		'PMS_PropertyParameter_Get_PropertyParameters,'+
		'PMS_BusinessSource_Get,'+
		'PMS_MarketSegment_Get,'+
		'PMS_Reason_Get,'+
		'PMS_PlasticBrandProperty_Get,'+
		'PMS_CompanyClient_Get_Search,'+
		'PMS_CompanyClient_Get_ContactPersons,'+
		'PMS_RoomType_Get_WithOverbooking,'+
		'PMS_ReservationBudget_Get_Param,'+
		'PMS_PropertyMealPlanType_Get_GetAllPropertyMealPlanType,'+
		'PMS_RoomLayout_ByRoomType,'+
		'PMS_Guest_Get_Search,'+
		'PMS_Reservation_Put,'+
		'PMS_Reservation_Get,'+
		'PMS_GuestEntranceBudget_Get_Param,'+
		'PMS_GuestEntranceConfirm_Post,'+
		'PMS_GuestEntrance_Post,'+
		'PMS_GuestEntrance_Put';


	EXEC PROC_CreatePermission
		@identityPermissionId,
		@identityProductId,
		@identityModuleId,
		@PermissionNamePtBr,
		@PermissionNamePtPt,
		@PermissionNameEnUs,
		@PermissionNameEsAr,
		@permissionKeyList;
END



-- PMS - Reactivate Accommodation
BEGIN
	SET @identityPermissionId = N'eb1b548e-ebb3-4a91-8673-8098a6e2b5cf';
	
	SET @PermissionNamePtBr = 'Reativar Acomodação';
	SET @PermissionNamePtPt = 'Reativar Acomodação';
	SET @PermissionNameEnUs = 'Reactivate Accommodation';
	SET @PermissionNameEsAr = 'Reactivar Alojamiento';

	SET @identityProductId = 1;
	SET @identityModuleId = 5;

	SET @permissionKeyList = 
		'PMS_ReservationItem_Patch_Reactivate';

	EXEC PROC_CreatePermission
		@identityPermissionId,
		@identityProductId,
		@identityModuleId,
		@PermissionNamePtBr,
		@PermissionNamePtPt,
		@PermissionNameEnUs,
		@PermissionNameEsAr,
		@permissionKeyList;
END


-- PMS - Cancel Reservation
BEGIN
	SET @identityPermissionId = N'd304cee2-3263-4f5b-a348-e8bf249dd00c';
	
	SET @PermissionNamePtBr = 'Cancelar Reserva';
	SET @PermissionNamePtPt = 'Cancelar Reserva';
	SET @PermissionNameEnUs = 'Cancel Reservation';
	SET @PermissionNameEsAr = 'Cancelar Reserva';

	SET @identityProductId = 1;
	SET @identityModuleId = 5;

	SET @permissionKeyList = 
		'PMS_Reason_Get,'+
		'PMS_Reservation_Patch_Cancel';

	EXEC PROC_CreatePermission
		@identityPermissionId,
		@identityProductId,
		@identityModuleId,
		@PermissionNamePtBr,
		@PermissionNamePtPt,
		@PermissionNameEnUs,
		@PermissionNameEsAr,
		@permissionKeyList;
END



-- PMS - Reactivate Reservation
BEGIN
	SET @identityPermissionId = N'88c3bca5-f29e-4796-83c3-c0b8b8301b89';
	
	SET @PermissionNamePtBr = 'Reativar Reserva';
	SET @PermissionNamePtPt = 'Reativar Reserva';
	SET @PermissionNameEnUs = 'Reactivate Reservation';
	SET @PermissionNameEsAr = 'Reativar Reserva';

	SET @identityProductId = 1;
	SET @identityModuleId = 5;

	SET @permissionKeyList = 
		'PMS_Reservation_Patch_Reactivate';

	EXEC PROC_CreatePermission
		@identityPermissionId,
		@identityProductId,
		@identityModuleId,
		@PermissionNamePtBr,
		@PermissionNamePtPt,
		@PermissionNameEnUs,
		@PermissionNameEsAr,
		@permissionKeyList;
END


-- PMS - Rate Suggestion
BEGIN
	SET @identityPermissionId = N'05a0869f-39c4-476b-a2c6-cb9871090eca';
	
	SET @PermissionNamePtBr = 'Disponibilidade';
	SET @PermissionNamePtPt = 'Disponibilidade';
	SET @PermissionNameEnUs = 'Disponibilidad';
	SET @PermissionNameEsAr = 'Availability';

	SET @identityProductId = 1;
	SET @identityModuleId = 5;

	SET @permissionKeyList = 
		'PMS_Menu_Availability,'+
		'PMS_Availability_Get_RoomTypes,'+
		'PMS_ReservationReport_Get_Grafics,'+
		'PMS_Reason_Get,'+
		'PMS_Availability_Get_Rooms';


	EXEC PROC_CreatePermission
		@identityPermissionId,
		@identityProductId,
		@identityModuleId,
		@PermissionNamePtBr,
		@PermissionNamePtPt,
		@PermissionNameEnUs,
		@PermissionNameEsAr,
		@permissionKeyList;
END



-- PMS - Rate Suggestion
BEGIN
	SET @identityPermissionId = N'7d77c3e8-6618-4382-8b82-ff7cdc418fbb';
	
	SET @PermissionNamePtBr = 'Sugestão Tarifária';
	SET @PermissionNamePtPt = 'Sugestão Tarifária';
	SET @PermissionNameEnUs = 'Sugestão Tarifária';
	SET @PermissionNameEsAr = 'Rate Suggestion';

	SET @identityProductId = 1;
	SET @identityModuleId = 5;

	SET @permissionKeyList = 
		'PMS_Menu_Tariff_Suggestion,'+
		'PMS_PropertyMealPlanType_Get_GetAllPropertyMealPlanType,'+
		'PMS_Property_Get_RoomTypes,'+
		'PMS_CompanyClient_Get_Search,'+
		'PMS_RateProposal_Get';


	EXEC PROC_CreatePermission
		@identityPermissionId,
		@identityProductId,
		@identityModuleId,
		@PermissionNamePtBr,
		@PermissionNamePtPt,
		@PermissionNameEnUs,
		@PermissionNameEsAr,
		@permissionKeyList;
END




-- PMS - Early Checkout
BEGIN
	SET @identityPermissionId = N'911e9eef-f657-4d53-a714-89c0e738b860';
	
	SET @PermissionNamePtBr = 'Checkout Antecipado';
	SET @PermissionNamePtPt = 'Checkout Antecipado';
	SET @PermissionNameEnUs = 'Early Checkout';
	SET @PermissionNameEsAr = 'Checkout Anticipado';
	
	SET @identityProductId = 1;
	SET @identityModuleId = 5;

	SET @permissionKeyList = 
		'PMS_ReservationItem_Get_EarlyCheckout,'+
		'PMS_ReservationItem_Patch_Checkout';


	EXEC PROC_CreatePermission
		@identityPermissionId,
		@identityProductId,
		@identityModuleId,
		@PermissionNamePtBr,
		@PermissionNamePtPt,
		@PermissionNameEnUs,
		@PermissionNameEsAr,
		@permissionKeyList;
END


-- PMS - Release UH
BEGIN
	SET @identityPermissionId = N'654cfc52-d346-48aa-b6b1-d0da9a7afd34';
	
	SET @PermissionNamePtBr = 'Liberar UH';
	SET @PermissionNamePtPt = 'Liberar UH';
	SET @PermissionNameEnUs = 'Release UH';
	SET @PermissionNameEsAr = 'Liberar HAB';
	
	SET @identityProductId = 1;
	SET @identityModuleId = 5;

	SET @permissionKeyList = 
		'PMS_ReservationItem_Patch_Realease';


	EXEC PROC_CreatePermission
		@identityPermissionId,
		@identityProductId,
		@identityModuleId,
		@PermissionNamePtBr,
		@PermissionNamePtPt,
		@PermissionNameEnUs,
		@PermissionNameEsAr,
		@permissionKeyList;
END

-- PMS - Reactivate Accommodation
BEGIN
	SET @identityPermissionId = N'c312b3be-f607-4dbc-81a1-0f0def9401a8';

	SET @PermissionNamePtBr = 'Reativar No-Show';
	SET @PermissionNamePtPt = 'Reativar No-Show';
	SET @PermissionNameEnUs = 'Reactivate No-Show';
	SET @PermissionNameEsAr = 'Reactivar No-Show';

	SET @identityProductId = 1;
	SET @identityModuleId = 5;

	SET @permissionKeyList =
		'PMS_ReservationItem_Patch_ReactivateNoShow';

	EXEC PROC_CreatePermission
		@identityPermissionId,
		@identityProductId,
		@identityModuleId,
		@PermissionNamePtBr,
		@PermissionNamePtPt,
		@PermissionNameEnUs,
		@PermissionNameEsAr,
		@permissionKeyList;
END

-- PMS - Proforma

BEGIN
	SET @identityPermissionId = N'fd445cf5-907f-4a16-8785-ce2369ddf721';

	SET @PermissionNamePtBr = 'Proforma';
	SET @PermissionNamePtPt = 'Proforma';
	SET @PermissionNameEnUs = 'Proforma';
	SET @PermissionNameEsAr = 'Proforma';

	SET @identityProductId = 1;
	SET @identityModuleId = 5;

	SET @permissionKeyList =
		'PMS_Proforma_Post';

	EXEC PROC_CreatePermission
		@identityPermissionId,
		@identityProductId,
		@identityModuleId,
		@PermissionNamePtBr,
		@PermissionNamePtPt,
		@PermissionNameEnUs,
		@PermissionNameEsAr,
		@permissionKeyList;
END


GO