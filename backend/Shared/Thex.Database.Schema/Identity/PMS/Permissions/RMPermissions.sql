﻿	Declare @identityPermissionId UNIQUEIDENTIFIER;

	Declare @PermissionNamePtBr varchar(150);
	Declare @PermissionNamePtPt varchar(150);
	Declare @PermissionNameEnUs varchar(150);
	Declare @PermissionNameEsAr varchar(150);

	Declare @identityProductId int;
	Declare @identityModuleId int;

	Declare @permissionKeyList varchar(MAX);

--RM
-- RM - Register, Edit and Activate Pension Type
BEGIN
	SET @identityPermissionId = N'6ef57ac7-ab9b-46f9-a4c9-de73ddfa1fb4';

	SET @PermissionNamePtBr = 'Cadastrar, Editar e Ativar Tipo de Pensão';
	SET @PermissionNamePtPt = 'Cadastrar, Editar e Ativar Tipo de Pensão';
	SET @PermissionNameEnUs = 'Register, Edit and Activate Pension Type';
	SET @PermissionNameEsAr = 'Registrarse, Editar y Activar Tipo de Pensión';

	SET @identityProductId = 1;
	SET @identityModuleId = 8;

	SET @permissionKeyList =
	'PMS_Menu_Pension_Register,' +
	'PMS_PropertyMealPlanType_Get_GetAllPropertyMealPlanType,' +
	'PMS_PropertyMealPlanType_Update,' +
	'PMS_PropertyMealPlanType_Patch_ToggleActivation';

	EXEC PROC_CreatePermission
		@identityPermissionId,
		@identityProductId,
		@identityModuleId,
		@PermissionNamePtBr,
		@PermissionNamePtPt,
		@PermissionNameEnUs,
		@PermissionNameEsAr,
		@permissionKeyList;
END


-- RM - Create and view base rate history
BEGIN
	SET @identityPermissionId = N'ef82b8d1-4a0e-4931-82a8-06697e909db8';

	SET @PermissionNamePtBr = 'Criar Tarifa Base e Visualizar Histórico';
	SET @PermissionNamePtPt = 'Criar Tarifa Base e Visualizar Histórico';
	SET @PermissionNameEnUs = 'Create Base Rate and View History';
	SET @PermissionNameEsAr = 'Crear Tarifa Base y Ver Historial';

	SET @identityProductId = 1;
	SET @identityModuleId = 8;

	SET @permissionKeyList =
	'PMS_Menu_Insert_Base_Rate,' +
	'PMS_PropertyBaseRate_Post_By_Premise,' +
	'PMS_PropertyBaseRate_Get_PropertyBaseRateHeaderHistory,' +
	'PMS_PropertyMealPlanType_Get_GetAllPropertyMealPlanType,' +
	'PMS_RatePlan_Get_RatePlanHistoryId,' +
	'PMS_PropertyBaseRate_Get_ConfigurationDataParameters,' +
	'PMS_PropertyBaseRate_Post,' +
	'PMS_PropertyBaseRate_Post_By_Level';

	EXEC PROC_CreatePermission
		@identityPermissionId,
		@identityProductId,
		@identityModuleId,
		@PermissionNamePtBr,
		@PermissionNamePtPt,
		@PermissionNameEnUs,
		@PermissionNameEsAr,
		@permissionKeyList;
END


-- RM - Register, Edit and Activate Trade Agreement
BEGIN
	SET @identityPermissionId = N'244b91d9-8000-4de6-8286-69c42f8b8f45';

	SET @PermissionNamePtBr = 'Cadastrar, Editar e Ativar Acordo Comercial';
	SET @PermissionNamePtPt = 'Cadastrar, Editar e Ativar Acordo Comercial';
	SET @PermissionNameEnUs = 'Register, Edit and Activate Trade Agreement';
	SET @PermissionNameEsAr = 'Registrarse, Editar y Activar Acuerdo Comercial';

	SET @identityProductId = 1;
	SET @identityModuleId = 8;

	SET @permissionKeyList =
	'PMS_Menu_Trade_Agreement,' +
	'PMS_RatePlan_Get_RatePlans,' +
	'PMS_RatePlan_Patch_ToggleActivation,' +
	'PMS_RatePlan_Get_HeaderRatePlan,' +
	'PMS_PropertyCompanyClientCategory_Get_GetAllCompanyClientCategory,' +
	'PMS_RatePlan_Get_BillingItens,' +
	'PMS_Property_Get_RoomTypes,' +
	'PMS_RatePlan_Get_Param,' +
	'PMS_CompanyClient_Get,' +
	'PMS_RatePlan_Put,' +
	'PMS_RatePlan_Post';

	EXEC PROC_CreatePermission
		@identityPermissionId,
		@identityProductId,
		@identityModuleId,
		@PermissionNamePtBr,
		@PermissionNamePtPt,
		@PermissionNameEnUs,
		@PermissionNameEsAr,
		@permissionKeyList;
END


-- RM - Rate Calendar
BEGIN
	SET @identityPermissionId = N'3ad25f22-071e-4495-be59-45996b6c8d1b';

	SET @PermissionNamePtBr = 'Calendário de Tarifas';
	SET @PermissionNamePtPt = 'Calendário de Tarifas';
	SET @PermissionNameEnUs = 'Rate Calendar';
	SET @PermissionNameEsAr = 'Calendario de Tarifas';

	SET @identityProductId = 1;
	SET @identityModuleId = 8;

	SET @permissionKeyList =
	'PMS_Menu_Tariff_Schedule,' +
	'PMS_RateCalendar_Get_CalendarParameters,' +
	'PMS_RatePlan_Get_RatePlans,' +
	'PMS_RateCalendar_Get_Param';

	EXEC PROC_CreatePermission
		@identityPermissionId,
		@identityProductId,
		@identityModuleId,
		@PermissionNamePtBr,
		@PermissionNamePtPt,
		@PermissionNameEnUs,
		@PermissionNameEsAr,
		@permissionKeyList;
END


-- RM - Rate Strategy
BEGIN
	SET @identityPermissionId = N'280c95ac-9b74-415e-a075-50ff122e7979';

	SET @PermissionNamePtBr = 'Estratégia Tarifária';
	SET @PermissionNamePtPt = 'Estratégia Tarifária';
	SET @PermissionNameEnUs = 'Rate Strategy';
	SET @PermissionNameEsAr = 'Estratégia Tarifária';

	SET @identityProductId = 1;
	SET @identityModuleId = 8;

	SET @permissionKeyList =
	'PMS_Menu_Rate_Strategy,' +
	'PMS_PropertyRateStrategy_Patch_ToggleActivation,' +
	'PMS_PropertyRateStrategy_Put,' +
	'PMS_PropertyRateStrategy_Post,' +
	'PMS_PropertyRateStrategy_Get_Param,' +
	'PMS_PropertyRateStrategy_Get_GetAllByPropertyId';

	EXEC PROC_CreatePermission
		@identityPermissionId,
		@identityProductId,
		@identityModuleId,
		@PermissionNamePtBr,
		@PermissionNamePtPt,
		@PermissionNameEnUs,
		@PermissionNameEsAr,
		@permissionKeyList;
END


-- RM - Premissas
BEGIN
	SET @identityPermissionId = N'754792e2-d5a1-4c60-a07a-a5ce6f42f30d';

	SET @PermissionNamePtBr = 'Configurar Pensão';
	SET @PermissionNamePtPt = 'Configurar Pensão';
	SET @PermissionNameEsAr = 'Gestión Comidas';
	SET @PermissionNameEnUs = 'Mealplan Management';

	SET @identityProductId = 1;
	SET @identityModuleId = 8;

	SET @permissionKeyList = 
	'PMS_Menu_PropertyMealPlanTypeRate,' +
	'PMS_PropertyMealPlanTypeRate_Patch_Toggle,' +
	'PMS_PropertyMealPlanTypeRate_Put,' +
	'PMS_PropertyMealPlanTypeRate_Post,' +
	'PMS_PropertyMealPlanTypeRate_GetAll,' +
	'PMS_PropertyMealPlanTypeRate_Get';

	EXEC PROC_CreatePermission
		@identityPermissionId,
		@identityProductId,
		@identityModuleId,
		@PermissionNamePtBr,
		@PermissionNamePtPt,
		@PermissionNameEnUs,
		@PermissionNameEsAr,
		@permissionKeyList;
END

-- RM - MealPlanTypeRate
BEGIN
	SET @identityPermissionId = N'501777f1-1394-4a49-a13d-59af1aa0ca99';
	
	SET @PermissionNamePtBr = 'Cadastro de Premissas';
	SET @PermissionNamePtPt = 'Cadastro de Premissas';
	SET @PermissionNameEnUs = 'Assumptions Register';
	SET @PermissionNameEsAr = 'Registro de premisas';
	
	SET @permissionKeyList =
	'PMS_Menu_PropertyPremise,' +
	'PMS_PropertyPremise_Get,' +
	'PMS_PropertyPremise_Get_Param,' +
	'PMS_PropertyPremise_Patch_ToggleActivation,' +
	'PMS_PropertyPremise_Post,' +
	'PMS_PropertyPremise_Put,' +
	'PMS_PropertyPremise_Delete';

	EXEC PROC_CreatePermission
		@identityPermissionId,
		@identityProductId,
		@identityModuleId,
		@PermissionNamePtBr,
		@PermissionNamePtPt,
		@PermissionNameEnUs,
		@PermissionNameEsAr,
		@permissionKeyList;
END

-- RM - Level
BEGIN
	SET @identityPermissionId = N'7C694699-7F95-4220-878C-0603049A4F73';
	
	SET @PermissionNamePtBr = 'Cadastro de Nível de Tarifa';
	SET @PermissionNamePtPt = 'Cadastro de Nível de Tarifa';
	SET @PermissionNameEsAr = 'Registro de Nivel de Tarifa';
	SET @PermissionNameEnUs = 'Rate Level Register';
	
	SET @permissionKeyList =
	'PMS_Level_Get_All,' +
	'PMS_Level_Get,' +
	'PMS_Level_Put,' +
	'PMS_Level_Post,' +
	'PMS_Level_Delete,' +
	'PMS_Level_Patch_ToggleActivation,' +
	'PMS_Menu_Level_Tariff,' +
	'PMS_Menu_Level';

	EXEC PROC_CreatePermission
		@identityPermissionId,
		@identityProductId,
		@identityModuleId,
		@PermissionNamePtBr,
		@PermissionNamePtPt,
		@PermissionNameEnUs,
		@PermissionNameEsAr,
		@permissionKeyList;
END

-- RM - Level Rate
BEGIN
	SET @identityPermissionId = N'5c54cb6e-0cce-4030-b8da-1b1230345b6b';
	
	SET @PermissionNamePtBr = 'Cadastro de Tarifa por nível';
	SET @PermissionNamePtPt = 'Cadastro de Tarifa por nível';
	SET @PermissionNameEsAr = 'Registro de Tarifa por nivel';
	SET @PermissionNameEnUs = 'Rate register by Level';
	
	SET @permissionKeyList =
	'PMS_Level_Rate_Get_All,' +
	'PMS_Level_Rate_Get,' +
	'PMS_Level_Rate_Post,' +
	'PMS_Level_Rate_Put,' +
	'PMS_Level_Rate_Delete,' +
	'PMS_Level_Rate_Toggle,' +
	'PMS_Level_Rate_Get_All_Levels_Available,' +
	'PMS_Menu_Level_Tariff,' +
	'PMS_Menu_Level_Rate';

	EXEC PROC_CreatePermission
		@identityPermissionId,
		@identityProductId,
		@identityModuleId,
		@PermissionNamePtBr,
		@PermissionNamePtPt,
		@PermissionNameEnUs,
		@PermissionNameEsAr,
		@permissionKeyList;
END


GO
