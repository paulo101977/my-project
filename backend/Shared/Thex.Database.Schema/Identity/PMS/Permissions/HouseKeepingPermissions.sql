﻿	Declare @identityPermissionId UNIQUEIDENTIFIER;
	
	Declare @PermissionNamePtBr varchar(150);
	Declare @PermissionNamePtPt varchar(150);
	Declare @PermissionNameEnUs varchar(150);
	Declare @PermissionNameEsAr varchar(150);

	Declare @identityProductId int;
	Declare @identityModuleId int;
	
	Declare @permissionKeyList varchar(MAX);

--Housekeeping
-- Housekeeping - Manage Housekeeping
BEGIN
	SET @identityPermissionId = N'a28c2d16-e9b6-4f5e-bc0f-ee27d3c89be7';
	
	SET @PermissionNamePtBr = 'Gerenciar Governança';
	SET @PermissionNamePtPt = 'Gerenciar Governança';
	SET @PermissionNameEnUs = 'Manage Governance';
	SET @PermissionNameEsAr = 'Gestionar Ama de llaves';

	SET @identityProductId = 1;
	SET @identityModuleId = 6;

	SET @permissionKeyList = 
	'PMS_Menu_See_Governance,' +
	'PMS_Reason_Get,' +
	'PMS_HouseKeepingStatusProperty_Get_GetAllhouseKeepingAlterStatusRoom,' +
	'PMS_HouseKeepingStatusProperty_Get_GetAllHousekeepingStatusProperty,' +
	'PMS_HouseKeepingStatusProperty_Update_AlterManyRoom,' +
	'PMS_HouseKeepingStatusProperty_Post_BlockingRoom,' +
	'PMS_HouseKeepingStatusProperty_Delete_Unlockroom';


	EXEC PROC_CreatePermission
		@identityPermissionId,
		@identityProductId,
		@identityModuleId,
		@PermissionNamePtBr,
		@PermissionNamePtPt,
		@PermissionNameEnUs,
		@PermissionNameEsAr,
		@permissionKeyList;
END


-- Housekeeping - Management of UH
BEGIN
	SET @identityPermissionId = N'637f756e-fcc5-4835-824d-dc9af0650d56';
	
	SET @PermissionNamePtBr = 'Gestão de UH';
	SET @PermissionNamePtPt = 'Gestão de UH';
	SET @PermissionNameEnUs = 'Management of UH';
	SET @PermissionNameEsAr = 'Gestão de UH';

	SET @identityProductId = 1;
	SET @identityModuleId = 6;

	SET @permissionKeyList = 
	'PMS_Menu_See_Management_of_UH,' +
	'HK_User_Get_GetUserRoomHistory,' +
	'HK_HousekeepingRoom_Get_GetAllGrid,' +
	'HK_User_Get_GetUserRoomByUserId';



	EXEC PROC_CreatePermission
		@identityPermissionId,
		@identityProductId,
		@identityModuleId,
		@PermissionNamePtBr,
		@PermissionNamePtPt,
		@PermissionNameEnUs,
		@PermissionNameEsAr,
		@permissionKeyList;
END


-- Housekeeping - Automatic Room Association
BEGIN
	SET @identityPermissionId = N'4d399132-83df-4ee0-a7fd-ff8cbf5f13fc';
	
	SET @PermissionNamePtBr = 'Associação Automática de UH';
	SET @PermissionNamePtPt = 'Associação Automática de UH';
	SET @PermissionNameEnUs = 'Automatic Room Association';
	SET @PermissionNameEsAr = 'Asociación Automática de Habitaciones';

	SET @identityProductId = 1;
	SET @identityModuleId = 6;

	SET @permissionKeyList = 
	'HK_HouseKeepingGenerator_Post,' +
	'HK_HouseKeepingGenerator_Post_CreateHouseKeeping';



	EXEC PROC_CreatePermission
		@identityPermissionId,
		@identityProductId,
		@identityModuleId,
		@PermissionNamePtBr,
		@PermissionNamePtPt,
		@PermissionNameEnUs,
		@PermissionNameEsAr,
		@permissionKeyList;
END




GO