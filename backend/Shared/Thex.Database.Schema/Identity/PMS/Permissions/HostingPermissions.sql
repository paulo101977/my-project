﻿	Declare @identityPermissionId UNIQUEIDENTIFIER;
	
	Declare @PermissionNamePtBr varchar(150);
	Declare @PermissionNamePtPt varchar(150);
	Declare @PermissionNameEnUs varchar(150);
	Declare @PermissionNameEsAr varchar(150);

	Declare @identityProductId int;
	Declare @identityModuleId int;
	
	Declare @permissionKeyList varchar(MAX);

-- PMS - Check-in
BEGIN
	SET @identityPermissionId = N'12cf2cd2-c16f-4fae-b2f6-98b14192745f';
	
	SET @PermissionNamePtBr = 'Check-in';
	SET @PermissionNamePtPt = 'Check-in';
	SET @PermissionNameEnUs = 'Check-in';
	SET @PermissionNameEsAr = 'Check-in';

	SET @identityProductId = 1;
	SET @identityModuleId = 3;

	SET @permissionKeyList = 
	'PMS_Menu_Checkin,' +

	'PMS_DocumentType_Get_AllByNaturalPersonType,' +
	'PMS_PropertyGuestType_Get_ByProperty,' +
	'PMS_TransportationType_Get,' +
	'PMS_Nationality_Get,' +
	'PMS_Reason_Get,' +
	'PMS_GeneralOccupation_Get,' +
	'PMS_Reservation_Get_Header,' +
	'PMS_ReservationItem_Get_GuestGuestRegistration,' +
	'PMS_GuestRegistration_Get_Responsibles,' +
	'PMS_Country_Get,' +
	'PMS_RoomType_Get_WithOverbooking,' +
	'PMS_GuestRegistration_Get_ByGuestReservationItem,' +
	'PMS_Room_Get_ByPeriodOfRoomType,' +
	'PMS_ReservationItem_Patch_AssociateRoom,' +
	'PMS_GuestRegistration_Get_Search,' +
	'PMS_GuestRegistration_Post,' +
	'PMS_Reservation_Patch_SaveNote,' +
	'PMS_GuestRegistration_Put,' +
	'PMS_GuestReservationItem_Patch_ConfirmCheckin,' +
	'PMS_Reservation_Get,' +
	'PMS_PropertyMealPlanType_Get_GetAllPropertyMealPlanType,' +
	'PMS_MarketSegment_Get,' +
	'PMS_BusinessSource_Get,' +
	'PMS_RatePlan_Get_RatePlans,' +
	'PMS_Reservation_Get_Search,' +
	'PMS_Property_Get_RoomTypes,' +
	'PMS_ReservationItem_Patch_CancelCheckin';


	EXEC PROC_CreatePermission
		@identityPermissionId,
		@identityProductId,
		@identityModuleId,
		@PermissionNamePtBr,
		@PermissionNamePtPt,
		@PermissionNameEnUs,
		@PermissionNameEsAr,
		@permissionKeyList;
END



-- PMS - Walk-in
BEGIN
	SET @identityPermissionId = N'5765e2a2-45b2-4e20-9020-be2809b832b4';
	
	SET @PermissionNamePtBr = 'Walk-in';
	SET @PermissionNamePtPt = 'Walk-in';
	SET @PermissionNameEnUs = 'Walk-in';
	SET @PermissionNameEsAr = 'Walk-in';

	SET @identityProductId = 1;
	SET @identityModuleId = 3;

	SET @permissionKeyList = 
	'PMS_Menu_Walkin,' +
	'PMS_PropertyParameter_Get_PropertyParameters,' +
	'PMS_BusinessSource_Get,' +
	'PMS_MarketSegment_Get,' +
	'PMS_Reason_Get,' +
	'PMS_PlasticBrandProperty_Get,' +
	'PMS_CompanyClient_Get_Search,' +
	'PMS_CompanyClient_Get_ContactPersons,' +
	'PMS_RoomType_Get_WithOverbooking,' +
	'PMS_ReservationBudget_Get_Param,' +
	'PMS_PropertyMealPlanType_Get_GetAllPropertyMealPlanType,' +
	'PMS_RoomLayout_Get_ByRoomType,' +
	'PMS_Guest_Get_Search,' +
	'PMS_Reservation_Post,' +
	'PMS_DocumentType_Get_AllByNaturalPersonType,' +
	'PMS_PropertyGuestType_Get_ByProperty,' +
	'PMS_TransportationType_Get,' +
	'PMS_Nationality_Get,' +
	'PMS_GeneralOccupation_Get,' +
	'PMS_Reservation_Get_Header,' +
	'PMS_ReservationItem_Get_GuestGuestRegistration,' +
	'PMS_GuestRegistration_Get_Responsibles,' +
	'PMS_Country_Get,' +
	'PMS_GuestRegistration_Get_ByGuestReservationItem,' +
	'PMS_Room_Get_ByPeriodOfRoomType,' +
	'PMS_ReservationItem_Patch_AssociateRoom,' +
	'PMS_GuestRegistration_Get_Search,' +
	'PMS_GuestRegistration_Post,' +
	'PMS_Reservation_Patch_SaveNote,' +
	'PMS_GuestRegistration_Put,' +
	'PMS_GuestReservationItem_Patch_ConfirmCheckin,' +
	'PMS_Reservation_Get_NewReservationCode,' +
	'PMS_Reservation_Get,' +
	'PMS_ReservationItem_Patch_CancelCheckin';


	EXEC PROC_CreatePermission
		@identityPermissionId,
		@identityProductId,
		@identityModuleId,
		@PermissionNamePtBr,
		@PermissionNamePtPt,
		@PermissionNameEnUs,
		@PermissionNameEsAr,
		@permissionKeyList;
END


-- PMS - Change of UH
BEGIN
	SET @identityPermissionId = N'7d900201-c636-403d-abab-e59f3ba36d2c';
	
	SET @PermissionNamePtBr = 'Mudança de UH';
	SET @PermissionNamePtPt = 'Mudança de UH';
	SET @PermissionNameEnUs = 'Change of UH';
	SET @PermissionNameEsAr = 'Cambio de Habitacíon';

	SET @identityProductId = 1;
	SET @identityModuleId = 3;

	SET @permissionKeyList = 
	'PMS_Menu_Change_Of_UH,'+
	'PMS_RoomType_Get_WithOverbooking,'+
	'PMS_Reason_Get_GetAllReason_Params,'+
	'PMS_Room_Get_StatusCheckinRooms,'+
	'PMS_GuestReservationItem_Get_RoomId,'+
	'PMS_Room_Get_ByPeriodOfRoomType,'+
	'PMS_Room_Get_ByPeriodOfRoomType_ChangeOfRoom,'+
	'PMS_ReservationBudget_Get_Param,'+
	'PMS_ReservationItem_Post_AccommodationChange';

	EXEC PROC_CreatePermission
		@identityPermissionId,
		@identityProductId,
		@identityModuleId,
		@PermissionNamePtBr,
		@PermissionNamePtPt,
		@PermissionNameEnUs,
		@PermissionNameEsAr,
		@permissionKeyList;
END



-- PMS - Change of UH
BEGIN
	SET @identityPermissionId = N'135f0b68-f3ba-4ba4-b461-ddff8c72e824';
	
	SET @PermissionNamePtBr = 'Auditoria';
	SET @PermissionNamePtPt = 'Auditoria';
	SET @PermissionNameEnUs = 'Audit';
	SET @PermissionNameEsAr = 'Auditoría';

	SET @identityProductId = 1;
	SET @identityModuleId = 3;

	SET @permissionKeyList = 
	'PMS_Menu_Audit,'+
	'PMS_PropertyAuditProcess_Get_GetAuditStep,'+
	'PMS_PropertyAuditProcess_Get_GetTimeForAudit,'+
	'PMS_PropertyAuditProcess_Patch_Params';


	EXEC PROC_CreatePermission
		@identityPermissionId,
		@identityProductId,
		@identityModuleId,
		@PermissionNamePtBr,
		@PermissionNamePtPt,
		@PermissionNameEnUs,
		@PermissionNameEsAr,
		@permissionKeyList;
END
GO