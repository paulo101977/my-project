﻿	Declare @identityPermissionId UNIQUEIDENTIFIER;
	
	Declare @PermissionNamePtBr varchar(150);
	Declare @PermissionNamePtPt varchar(150);
	Declare @PermissionNameEnUs varchar(150);
	Declare @PermissionNameEsAr varchar(150);

	Declare @identityProductId int;
	Declare @identityModuleId int;
	
	Declare @permissionKeyList varchar(MAX);

--Settings
-- Settings - Register, Edit and Activate Payment Type
BEGIN
	SET @identityPermissionId = N'2d614d0a-dce2-42c3-b03f-931302ccc6fb';
	
	SET @PermissionNamePtBr = 'Visualizar, Cadastrar, Editar, Deletar e Ativar Formas de Pagamento';
	SET @PermissionNamePtPt = 'Visualizar, Cadastrar, Editar, Deletar e Ativar Formas de Pagamento';
	SET @PermissionNameEnUs = 'View, Register, Edit, Delete, and Activate Payment Methods';
	SET @PermissionNameEsAr = 'Ver, Registrar, Editar, Eliminar y Activar Formas de Pago';

	SET @identityProductId = 1;
	SET @identityModuleId = 7;

	SET @permissionKeyList = 
	'PMS_Menu_Forms_of_Payment,'+
	'PMS_BillingItem_Get_AllPaymentTypes,'+
	'PMS_BillingItem_Patch_PaymentType_Params,'+
	'PMS_PaymentType_Get_ForCreation,'+
	'PMS_CompanyClient_Get_IsAcquirer,'+
	'PMS_BillingItem_Get_GetPaymentType,'+
	'PMS_BillingItem_Update_PaymentType,'+
	'PMS_BillingItem_Post_PaymentType,'+
	'PMS_BillingItem_Delete_PaymentType_Params';

	EXEC PROC_CreatePermission
		@identityPermissionId,
		@identityProductId,
		@identityModuleId,
		@PermissionNamePtBr,
		@PermissionNamePtPt,
		@PermissionNameEnUs,
		@PermissionNameEsAr,
		@permissionKeyList;
END

-- Settings - Register, Edit and Activate Tax
BEGIN
	SET @identityPermissionId = N'f0d481ba-132c-4f3a-84f0-ac6a0e2bcfbe';
	
	SET @PermissionNamePtBr = 'Visualizar, Cadastrar, Editar, Deletar e Ativar Tributos';
	SET @PermissionNamePtPt = 'Visualizar, Cadastrar, Editar, Deletar e Ativar Tributos';
	SET @PermissionNameEnUs = 'View, Register, Edit, Delete, and Activate Taxes';
	SET @PermissionNameEsAr = 'Ver, Registrar, Editar, Eliminar y Activar Tributos';

	SET @identityProductId = 1;
	SET @identityModuleId = 7;

	SET @permissionKeyList = 
	'PMS_Menu_Invoice_Emissions,'+
	'PMS_Menu_Tributes,'+
	'PMS_Menu_Tributes_Service,' +
	'PMS_Menu_Tributes_Product,' +
	'PMS_Menu_ISS_Withheld,' +
	'PMS_Menu_Fiscal_Documents,' +
	'PMS_CompanyClient_Patch_Associate_With_Tributs_Withheld,'+
	'PMS_CompanyClient_Get_Associate_With_Tributs_Withheld,'+
	'PMS_CompanyClient_Delete_Association_With_Tribut_Withheld,'+
	'Tax_Get_Cfop_GetAll,'+
	'Tax_Get_Cfop_GetAllByFilters,'+
	'Tax_Get_Cfop_GetByCode,'+
	'Tax_Get_Csosn_GetAll,'+
	'Tax_Get_Csosn_GetByCode,'+
	'Tax_Get_Cst_GetAll,'+
	'Tax_Get_Cst_GetByCode,'+
	'Tax_Get_Cst_GetByType,'+
	'Tax_Get_Revenue_GetAll,'+
	'Tax_Get_Revenue_GetByCode,'+
	'Tax_Get_Revenue_GetByNcm,'+
	'Tax_Get_Nbs_GetAll,'+
	'Tax_Get_Nbs_GetAllByFilters,'+
	'Tax_Get_Nbs_GetByCode,'+
	'Tax_Get_Ncm_GetAll,'+
	'Tax_Get_Ncm_GetAllByFilters,'+
	'Tax_Get_Ncm_GetByCode,'+
	'Tax_Get_TaxCalculation_GetAll,'+
	'Tax_Get_TaxCalculation_GetById,'+
	'Tax_Get_ServiceType_GetAll,'+
	'Tax_Get_ServiceType_GetByCode,'+
	'Tax_Get_UF_GetAll,'+
	'Tax_Get_UF_GetByCode,' +
    'Tax_Get_GetById,' +
	'Tax_Get_GetAllForServices,' +
	'Tax_Get_GetAllForProducts,' +
	'Tax_Post_Post,' +
	'Tax_Patch_ToggleActivation,' +
	'Tax_Get_GetTributes,' +
	'Tax_Get_GetAllForProducts,' +
	'Tax_Post_Imposto_Retido,' +
	'Tax_Get_All_Imposto_Retido,' +
	'Tax_Delete_Imposto_Retido,' +
	'PMS_Menu_Iva_Services_And_Products';

	EXEC PROC_CreatePermission
		@identityPermissionId,
		@identityProductId,
		@identityModuleId,
		@PermissionNamePtBr,
		@PermissionNamePtPt,
		@PermissionNameEnUs,
		@PermissionNameEsAr,
		@permissionKeyList;
END

-- Settings - Register, Edit and Activate Product
BEGIN
	SET @identityPermissionId = N'162fd44b-ad3c-426e-bb3a-d4155395c652';
	
	SET @PermissionNamePtBr = 'Visualizar, Cadastrar, Editar e Ativar Produto';
	SET @PermissionNamePtPt = 'Visualizar, Cadastrar, Editar e Ativar Produto';
	SET @PermissionNameEnUs = 'View, Register, Edit and Activate Product';
	SET @PermissionNameEsAr = 'Ver, Registrar, Editar y Activar Producto';

	SET @identityProductId = 1;
	SET @identityModuleId = 7;

	SET @permissionKeyList = 
	'PMS_Menu_Product_Registration,'+
	'PDV_Product_Get,'+
	'PDV_Product_Patch_ToggleActivation,'+
	'PDV_ProductCategory_Get,'+
	'PDV_UnitMeasurement_Get,'+
	'PDV_Ncm_Get,'+
	'PDV_BillingItem_Get,'+
	'PDV_Product_Post,' + 
	'PDV_Product_Put,' +
	'PDV_Product_Delete,' +
	'PDV_Product_GetImageBarCode,' +
	'PDV_Product_Get_Param';

	EXEC PROC_CreatePermission
		@identityPermissionId,
		@identityProductId,
		@identityModuleId,
		@PermissionNamePtBr,
		@PermissionNamePtPt,
		@PermissionNameEnUs,
		@PermissionNameEsAr,
		@permissionKeyList;
END



-- Settings - Register, Edit and Activate Product Group
BEGIN
	SET @identityPermissionId = N'ac677600-ba05-4123-a592-42b57e51f958';
	
	SET @PermissionNamePtBr = 'Visualizar, Cadastrar, Editar e Ativar Grupos de Produto';
	SET @PermissionNamePtPt = 'Visualizar, Cadastrar, Editar e Ativar Grupos de Produto';
	SET @PermissionNameEnUs = 'View, Register, Edit and Activate Product Groups';
	SET @PermissionNameEsAr = 'Ver, Registrar, Editar y Activar Grupos de Producto';

	SET @identityProductId = 1;
	SET @identityModuleId = 7;

	SET @permissionKeyList = 
	'PMS_Menu_Product_Group_Registration,'+
	'PDV_ProductCategory_Get_GroupFixed,'+
	'PDV_ProductCategory_Get,'+
	'PDV_ProductCategory_Patch_ToggleActivation,'+
	'PDV_ProductCategory_Get_Param,'+
	'PDV_ProductCategory_Put,'+
	'PDV_ProductCategory_Post,'+
	'PDV_ProductCategory_Delete';

	EXEC PROC_CreatePermission
		@identityPermissionId,
		@identityProductId,
		@identityModuleId,
		@PermissionNamePtBr,
		@PermissionNamePtPt,
		@PermissionNameEnUs,
		@PermissionNameEsAr,
		@permissionKeyList;
END



-- Settings - View, Register, Edit, Delete and Activate POS
BEGIN
	SET @identityPermissionId = N'c7ba4339-dabd-4142-b1f3-594746540e5f';
	
	SET @PermissionNamePtBr = 'Visualizar, Cadastrar, Editar, Excluir e Ativar PDV';
	SET @PermissionNamePtPt = 'Visualizar, Cadastrar, Editar, Excluir e Ativar PDV';
	SET @PermissionNameEnUs = 'View, Register, Edit, Delete and Activate POS';
	SET @PermissionNameEsAr = 'Ver, Registrar, Editar, Eliminar y Activar PDV';

	SET @identityProductId = 1;
	SET @identityModuleId = 7;

	SET @permissionKeyList = 
	'PMS_Menu_POS_Register,'+
	'PMS_BillingItemPDV_Get_GetAllBillingItemService,'+
	'PMS_BillingItemCategory_Get_GetAllCategoriesForItem,'+
	'PMS_BillingItem_Get_GetAllBillingItemForAssociateTax,'+
	'PMS_BillingItemPDV_Post_CreateServiceWithTax,'+
	'PMS_BillingItemPDV_Patch_ToggleActivation,'+
	'PMS_BillingItem_Get_Params_BillingItem,'+
	'PMS_BillingItemPDV_Update_UpdateBillingItemServiceWithTax,'+
	'PMS_BillingItemPDV_Get_Params_BillingItem,'+
	'PMS_BillingItemPDV_Delete_Params';

	EXEC PROC_CreatePermission
		@identityPermissionId,
		@identityProductId,
		@identityModuleId,
		@PermissionNamePtBr,
		@PermissionNamePtPt,
		@PermissionNameEnUs,
		@PermissionNameEsAr,
		@permissionKeyList;
END



-- Settings - View, Register, Edit, Delete and Activate Rate
BEGIN
	SET @identityPermissionId = N'cecddaa0-d6b9-4a35-a5fb-ca95573bc615';
	
	SET @PermissionNamePtBr = 'Visualizar, Cadastrar, Editar, Excluir e Ativar Taxa';
	SET @PermissionNamePtPt = 'Visualizar, Cadastrar, Editar, Excluir e Ativar Taxa';
	SET @PermissionNameEnUs = 'View, Register, Edit, Delete and Activate Rate';
	SET @PermissionNameEsAr = 'Ver, Registrar, Editar, Eliminar y Activar Tasa';

	SET @identityProductId = 1;
	SET @identityModuleId = 7;

	SET @permissionKeyList = 
	'PMS_Menu_Create_Rate,'+
	'PMS_BillingItem_Get_GetAllBillingItemTax,'+
	'PMS_BillingItem_Patch_ToggleActivation,'+
	'PMS_BillingItem_Get_BillingItem_Params,'+
	'PMS_BillingItem_Update_UpdateBillingItemTaxWithService,'+
	'PMS_BillingItem_Post_CreateTaxWithService,'+
	'PMS_BillingItem_Get_GetAllBillingItemForAssociateService,'+
	'PMS_BillingItemCategory_Get_GetAllCategoriesForItem,'+
	'PMS_BillingItem_Delete_Params';


	EXEC PROC_CreatePermission
		@identityPermissionId,
		@identityProductId,
		@identityModuleId,
		@PermissionNamePtBr,
		@PermissionNamePtPt,
		@PermissionNameEnUs,
		@PermissionNameEsAr,
		@permissionKeyList;
END




-- Settings - View, Register, Edit, Delete and Activate Service
BEGIN
	SET @identityPermissionId = N'b0c91d3b-c6a0-4a6e-9adc-5683a93b878b';
	
	SET @PermissionNamePtBr = 'Visualizar, Cadastrar, Editar, Excluir e Ativar Serviço';
	SET @PermissionNamePtPt = 'Visualizar, Cadastrar, Editar, Excluir e Ativar Serviço';
	SET @PermissionNameEnUs = 'View, Register, Edit, Delete and Activate Service';
	SET @PermissionNameEsAr = 'Ver, Registrar, Editar, Eliminar y Activar Servicio';

	SET @identityProductId = 1;
	SET @identityModuleId = 7;

	SET @permissionKeyList = 
	'PMS_Menu_Service_Registration,'+
	'PMS_BillingItem_Get_GetAllBillingItemService,'+
	'PMS_BillingItem_Patch_ToggleActivation,'+
	'PMS_BillingItem_Get_BillingItem_Params,'+
	'PMS_BillingItem_Update_UpdateBillingItemServiceWithTax,'+
	'PMS_BillingItem_Get_GetAllBillingItemForAssociateTax,'+
	'PMS_BillingItem_Get_GetAllBillingItemTax,'+
	'PMS_BillingItemCategory_Get_GetAllCategoriesForItem,'+
	'PMS_BillingItem_Post_CreateServiceWithTax,'+
	'PMS_BillingItem_Delete_Params';


	EXEC PROC_CreatePermission
		@identityPermissionId,
		@identityProductId,
		@identityModuleId,
		@PermissionNamePtBr,
		@PermissionNamePtPt,
		@PermissionNameEnUs,
		@PermissionNameEsAr,
		@permissionKeyList;
END


-- Settings - View, Register, Edit, Delete and Activate Service Group
BEGIN
	SET @identityPermissionId = N'95a13e80-cc27-4bf5-b0ac-45878e5d1a09';
	
	SET @PermissionNamePtBr = 'Visualizar, Cadastrar, Editar, Excluir e Ativar Grupo de Serviço';
	SET @PermissionNamePtPt = 'Visualizar, Cadastrar, Editar, Excluir e Ativar Grupo de Serviço';
	SET @PermissionNameEnUs = 'View, Register, Edit, Delete and Activate Services Group';
	SET @PermissionNameEsAr = 'Ver, Registrar, Editar, Eliminar y Activar Grupo de Servicio';

	SET @identityProductId = 1;
	SET @identityModuleId = 7;

	SET @permissionKeyList = 
	'PMS_Menu_Service_Group_Registration,'+
	'PMS_BillingItemCategory_Get_BillingItemCategory,'+
	'PMS_BillingItemCategory_Patch_ToggleActivation,'+
	'PMS_BillingItemCategory_Get_Params,'+
	'PMS_BillingItemCategory_Get_GroupFixed,'+
	'PMS_BillingItemCategory_Put,'+
	'PMS_BillingItemCategory_Post,'+
	'PMS_BillingItemCategory_Delete';


	EXEC PROC_CreatePermission
		@identityPermissionId,
		@identityProductId,
		@identityModuleId,
		@PermissionNamePtBr,
		@PermissionNamePtPt,
		@PermissionNameEnUs,
		@PermissionNameEsAr,
		@permissionKeyList;
END



-- Settings - View, Register, Edit, Delete and Activate Customer Category
BEGIN
	SET @identityPermissionId = N'28ce9f68-547b-4150-933f-1e14a7458a6c';
	
	SET @PermissionNamePtBr = 'Visualizar, Cadastrar, Editar, Excluir e Ativar Categoria de Cliente';
	SET @PermissionNamePtPt = 'Visualizar, Cadastrar, Editar, Excluir e Ativar Categoria de Cliente';
	SET @PermissionNameEnUs = 'View, Register, Edit, Delete and Activate Customer Category';
	SET @PermissionNameEsAr = 'Ver, Registrar, Editar, Eliminar y Activar Categoría de cliente';

	SET @identityProductId = 1;
	SET @identityModuleId = 7;

	SET @permissionKeyList = 
	'PMS_Menu_Customer_Category,'+
	'PMS_PropertyCompanyClientCategory_Get_GetAllCompanyClientCategory,'+
	'PMS_PropertyCompanyClientCategory_Patch_ToggleActivation,'+
	'PMS_PropertyCompanyClientCategory_Put,'+
	'PMS_PropertyCompanyClientCategory_Post,'+
	'PMS_PropertyCompanyClientCategory_Put,'+
	'PMS_PropertyCompanyClientCategory_Delete';


	EXEC PROC_CreatePermission
		@identityPermissionId,
		@identityProductId,
		@identityModuleId,
		@PermissionNamePtBr,
		@PermissionNamePtPt,
		@PermissionNameEnUs,
		@PermissionNameEsAr,
		@permissionKeyList;
END




-- Settings - View, Register, Edit, Delete and Activate Client
BEGIN
	SET @identityPermissionId = N'8ebfcb97-7257-48a8-8f5b-ed580f1f5ab5';
	
	SET @PermissionNamePtBr = 'Visualizar, Cadastrar, Editar, Excluir e Ativar Cliente';
	SET @PermissionNamePtPt = 'Visualizar, Cadastrar, Editar, Excluir e Ativar Cliente';
	SET @PermissionNameEnUs = 'View, Register, Edit, Delete and Activate Customer';
	SET @PermissionNameEsAr = 'Ver, Registrar, Editar, Eliminar y Activar Cliente';

	SET @identityProductId = 1;
	SET @identityModuleId = 7;

	SET @permissionKeyList = 
	'PMS_Menu_Client,'+
	'PMS_CompanyClient_Get,'+
	'PMS_CompanyClient_Patch_ToggleActivation,'+
	'PMS_DocumentType_Get,'+
	'PMS_PropertyCompanyClientCategory_Get_GetAllCompanyClientCategory,'+
	'PMS_CompanyClient_Get_Params,'+
	'PMS_Country_Get,'+
	'PMS_CompanyClient_Put,'+
	'PMS_CompanyClient_Post';


	EXEC PROC_CreatePermission
		@identityPermissionId,
		@identityProductId,
		@identityModuleId,
		@PermissionNamePtBr,
		@PermissionNamePtPt,
		@PermissionNameEnUs,
		@PermissionNameEsAr,
		@permissionKeyList;
END


-- Settings - View, Register, Edit, Delete and Activate Origin
BEGIN
	SET @identityPermissionId = N'8ebfcb97-7257-48a8-8f5b-ed580f1f5ab5';
	
	SET @PermissionNamePtBr = 'Visualizar, Cadastrar, Editar, Excluir e Ativar Origem';
	SET @PermissionNamePtPt = 'Visualizar, Cadastrar, Editar, Excluir e Ativar Origem';
	SET @PermissionNameEnUs = 'View, Register, Edit, Delete and Activate Origin';
	SET @PermissionNameEsAr = 'Ver, Registrar, Editar, Eliminar y Activar Origen';

	SET @identityProductId = 1;
	SET @identityModuleId = 7;

	SET @permissionKeyList = 
	'PMS_Menu_Origin,'+
	'PMS_BusinessSource_Get,'+
	'PMS_BusinessSource_Get_ToggleActivation,'+
	'PMS_BusinessSource_Update,'+
	'PMS_BusinessSource_Post,'+
	'PMS_BusinessSource_Delete';


	EXEC PROC_CreatePermission
		@identityPermissionId,
		@identityProductId,
		@identityModuleId,
		@PermissionNamePtBr,
		@PermissionNamePtPt,
		@PermissionNameEnUs,
		@PermissionNameEsAr,
		@permissionKeyList;
END


-- Settings - View, Register, Edit, Delete and Activate Segment
BEGIN
	SET @identityPermissionId = N'9b4e3999-3505-4456-a2b2-d10590b7d160';
	
	SET @PermissionNamePtBr = 'Visualizar, Cadastrar, Editar, Excluir e Ativar Segmento';
	SET @PermissionNamePtPt = 'Visualizar, Cadastrar, Editar, Excluir e Ativar Segmento';
	SET @PermissionNameEnUs = 'View, Register, Edit, Delete and Activate Segmento';
	SET @PermissionNameEsAr = 'Ver, Registrar, Editar, Eliminar y Activar Origen';

	SET @identityProductId = 1;
	SET @identityModuleId = 7;

	SET @permissionKeyList = 
	'PMS_Menu_Segment,'+
	'PMS_MarketSegment_Get,'+
	'PMS_MarketSegment_Post,'+
	'PMS_MarketSegment_Patch_ToggleActivation,'+
	'PMS_MarketSegment_Put,'+
	'PMS_MarketSegment_Delete';

	EXEC PROC_CreatePermission
		@identityPermissionId,
		@identityProductId,
		@identityModuleId,
		@PermissionNamePtBr,
		@PermissionNamePtPt,
		@PermissionNameEnUs,
		@PermissionNameEsAr,
		@permissionKeyList;
END



-- Settings - View, Register, Edit, Delete and Activate Reason
BEGIN
	SET @identityPermissionId = N'cded8d29-cab5-482d-a04a-a68d18d973b1';
	
	SET @PermissionNamePtBr = 'Visualizar, Cadastrar, Editar, Excluir e Ativar Motivo';
	SET @PermissionNamePtPt = 'Visualizar, Cadastrar, Editar, Excluir e Ativar Motivo';
	SET @PermissionNameEnUs = 'View, Register, Edit, Delete and Activate Reason';
	SET @PermissionNameEsAr = 'Ver, Registrar, Editar, Eliminar y Activar Razón';

	SET @identityProductId = 1;
	SET @identityModuleId = 7;

	SET @permissionKeyList = 
	'PMS_Menu_Reason,'+
	'PMS_Reason_Get_GetAllReasonCategory,'+
	'PMS_Reason_Get_GetAllReason,'+
	'PMS_Reason_Patch_ToggleActivation,'+
	'PMS_Reason_Post,'+
	'PMS_Reason_Put,'+
	'PMS_Reason_Delete';


	EXEC PROC_CreatePermission
		@identityPermissionId,
		@identityProductId,
		@identityModuleId,
		@PermissionNamePtBr,
		@PermissionNamePtPt,
		@PermissionNameEnUs,
		@PermissionNameEsAr,
		@permissionKeyList;
END




-- Settings - View, Register, Edit, Delete and Activate Currency Quotation
BEGIN
	SET @identityPermissionId = N'1cc17a0e-af50-4f03-9427-5602410739ac';
	
	SET @PermissionNamePtBr = 'Visualizar, Cadastrar, Editar Cotação de Moeda';
	SET @PermissionNamePtPt = 'Visualizar, Cadastrar, Editar Cotação de Moeda';
	SET @PermissionNameEnUs = 'View, Register, Edit Currency Quotation';
	SET @PermissionNameEsAr = 'Ver, Registrar, Editar Cotización de Moneda';

	SET @identityProductId = 1;
	SET @identityModuleId = 7;

	SET @permissionKeyList = 
	'PMS_Menu_Currency_Quotation,'+
	'PMS_Currency_Get,'+
	'PMS_PropertyCurrencyExchange_Get_QuotationCurrent,'+
	'PMS_PropertyCurrencyExchange_Get_QuotationByPeriod,'+
	'PMS_PropertyCurrencyExchange_Get_InsertQuotation,'+
	'PMS_PropertyCurrencyExchange_Patch_CurrentQuotation,'+
	'PMS_PropertyCurrencyExchange_Get_UpdateQuotationFuture';



	EXEC PROC_CreatePermission
		@identityPermissionId,
		@identityProductId,
		@identityModuleId,
		@PermissionNamePtBr,
		@PermissionNamePtPt,
		@PermissionNameEnUs,
		@PermissionNameEsAr,
		@permissionKeyList;
END




-- Settings - View, Register, Edit, Delete and Activate Type of Guest
BEGIN
	SET @identityPermissionId = N'b96579d9-cdbf-4286-a99e-b7125c3b3365';
	
	SET @PermissionNamePtBr = 'Visualizar, Cadastrar, Editar, Excluir e Ativar Tipo de Hóspede';
	SET @PermissionNamePtPt = 'Visualizar, Cadastrar, Editar, Excluir e Ativar Tipo de Hóspede';
	SET @PermissionNameEnUs = 'View, Register, Edit, Delete and Activate Type of Guest';
	SET @PermissionNameEsAr = 'Ver, Registrar, Editar, Eliminar y Activar Tipo de Huésped';

	SET @identityProductId = 1;
	SET @identityModuleId = 7;

	SET @permissionKeyList = 
	'PMS_Menu_Type_of_Guest,'+
	'PMS_PropertyGuestType_Get_ByProperty,'+
	'PMS_PropertyGuestType_Patch_ToggleActivation,'+
	'PMS_PropertyCurrencyExchange_Put,'+
	'PMS_PropertyGuestType_Post,'+
	'PMS_PropertyGuestType_Delete,' +
	'PMS_PropertyGuestType_Put';


	EXEC PROC_CreatePermission
		@identityPermissionId,
		@identityProductId,
		@identityModuleId,
		@PermissionNamePtBr,
		@PermissionNamePtPt,
		@PermissionNameEnUs,
		@PermissionNameEsAr,
		@permissionKeyList;
END


-- Settings - View, Register, Edit, Delete and Activate UH
BEGIN
	SET @identityPermissionId = N'149a6baa-0290-4d99-aa4c-2662296fe868';
	
	SET @PermissionNamePtBr = 'Visualizar, Cadastrar, Editar, Excluir e Ativar UH';
	SET @PermissionNamePtPt = 'Visualizar, Cadastrar, Editar, Excluir e Ativar UH';
	SET @PermissionNameEnUs = 'View, Register, Edit, Delete and Activate UH';
	SET @PermissionNameEsAr = 'Ver, Registrar, Editar, Eliminar y Activar HAB';

	SET @identityProductId = 1;
	SET @identityModuleId = 7;

	SET @permissionKeyList = 
	'PMS_Menu_UH,'+
	'PMS_Property_Get_Rooms,'+
	'PMS_Property_Get_ParentRooms,'+
	'PMS_Room_Patch_ToggleActivation,'+
	'PMS_Room_Get_Param,'+
	'PMS_Room_Put,'+
	'PMS_Property_Get_RoomTypes,'+
	'PMS_Room_Post,'+
	'PMS_Room_Get_Total_And_Available_Rooms,' +
	'PMS_Room_Delete';


	EXEC PROC_CreatePermission
		@identityPermissionId,
		@identityProductId,
		@identityModuleId,
		@PermissionNamePtBr,
		@PermissionNamePtPt,
		@PermissionNameEnUs,
		@PermissionNameEsAr,
		@permissionKeyList;
END


-- Settings - View, Register, Edit, Delete and Activate Type of UH
BEGIN
	SET @identityPermissionId = N'854240e4-2566-4864-9639-85989f38e365';
	
	SET @PermissionNamePtBr = 'Visualizar, Cadastrar, Editar, Excluir e Ativar Tipo de UH';
	SET @PermissionNamePtPt = 'Visualizar, Cadastrar, Editar, Excluir e Ativar Tipo de UH';
	SET @PermissionNameEnUs = 'View, Register, Edit, Delete and Activate Type of UH';
	SET @PermissionNameEsAr = 'Ver, Registrar, Editar, Eliminar y Activar Tipo de HAB';

	SET @identityProductId = 1;
	SET @identityModuleId = 7;

	SET @permissionKeyList = 
	'PMS_Menu_Kind_of_UH,'+
	'PMS_Property_Get_RoomTypes,'+
	'PMS_RoomType_Patch_ToggleActivation,'+
	'PMS_RoomType_Get,'+
	'PMS_RoomType_Put,'+
	'PMS_RoomType_Post,'+
	'PMS_PropertyParameter_Get_PropertyParametersAgeList,'+
	'PMS_RoomType_Delete';

	EXEC PROC_CreatePermission
		@identityPermissionId,
		@identityProductId,
		@identityModuleId,
		@PermissionNamePtBr,
		@PermissionNamePtPt,
		@PermissionNameEnUs,
		@PermissionNameEsAr,
		@permissionKeyList;
END




-- Settings - View, Register and Edit Parameterization of Fiscal Documents
BEGIN
	SET @identityPermissionId = N'5bb95907-76a7-4f2a-b0db-dc77c7f2858c';
	
	SET @PermissionNamePtBr = 'Visualizar, Cadastrar e Editar Parametrização de Documentos Fiscais';
	SET @PermissionNamePtPt = 'Visualizar, Cadastrar e Editar Parametrização de Documentos Fiscais';
	SET @PermissionNameEnUs = 'View, Register and Edit Parameterization of Fiscal Documents';
	SET @PermissionNameEsAr = 'Ver, Registrar y Editar Parametrización de Documentos Fiscales';

	SET @identityProductId = 1;
	SET @identityModuleId = 7;

	SET @permissionKeyList = 
	'PMS_BillingInvoice_Get_GetAll,'+
	'PMS_BillingItem_Get_BillingItemForParametersDocuments,'+
	'PMS_CountrySubdvisionService_Get_GetAll,'+
	'PMS_BillingInvoiceProperty_Get_Param,'+
	'PMS_BillingInvoiceModel_Get,'+
	'PMS_BillingInvoiceProperty_Post_ParametersDocument';

	EXEC PROC_CreatePermission
		@identityPermissionId,
		@identityProductId,
		@identityModuleId,
		@PermissionNamePtBr,
		@PermissionNamePtPt,
		@PermissionNameEnUs,
		@PermissionNameEsAr,
		@permissionKeyList;
END


--Settings - View, Register, Edit, Delete and Activate Housekeeping Status
BEGIN
	SET @identityPermissionId = N'79637711-1656-48b3-81a3-cb6286653ad6';
	
	SET @PermissionNamePtBr = 'Visualizar, Cadastrar, Editar, Deletar e Ativar Status da Governança';
	SET @PermissionNamePtPt = 'Visualizar, Cadastrar, Editar, Deletar e Ativar Status da Governanta';
	SET @PermissionNameEnUs = 'View, Register, Edit, Delete and Activate Housekeeping Status';
	SET @PermissionNameEsAr = 'Ver, Registrar, Editar, Eliminar y Activar Status de Mucama';

	SET @identityProductId = 1;
	SET @identityModuleId = 7;

	SET @permissionKeyList = 
	'PMS_HouseKeepingStatusProperty_Get_GetAllHousekeepingStatusProperty,'+
	'PMS_HouseKeepingStatusProperty_Get_ToggleActivation,'+
	'PMS_HouseKeepingStatusProperty_Put,'+
	'PMS_HouseKeepingStatusProperty_Post,'+
	'PMS_HouseKeepingStatusProperty_Delete,'+
	'PMS_Menu_Hotel_GovernanceStatus';



	EXEC PROC_CreatePermission
		@identityPermissionId,
		@identityProductId,
		@identityModuleId,
		@PermissionNamePtBr,
		@PermissionNamePtPt,
		@PermissionNameEnUs,
		@PermissionNameEsAr,
		@permissionKeyList;
END


--Settings - View, Register, Edit, Delete and Activate Hotel Policies
BEGIN
	SET @identityPermissionId = N'fa16a620-9256-45b8-a16c-098e604fff9a';
	
	SET @PermissionNamePtBr = 'Visualizar, Cadastrar, Editar, Deletar e Ativar Políticas do Hotel';
	SET @PermissionNamePtPt = 'Visualizar, Cadastrar, Editar, Deletar e Ativar Políticas do Hotel';
	SET @PermissionNameEnUs = 'View, Register, Edit, Delete and Activate Hotel Policies';
	SET @PermissionNameEsAr = 'Ver, Registrar, Editar, Eliminar y Activar Políticas del Hotel';

	SET @identityProductId = 1;
	SET @identityModuleId = 7;

	SET @permissionKeyList = 
		'PMS_PropertyPolicy_Get_GetAllPropertyPolicy,'+
		'PMS_PropertyPolicy_Post,'+
		'PMS_PropertyPolicy_Patch_ToggleActivation,'+
		'PMS_PropertyPolicy_Update,'+
		'PMS_PropertyPolicy_Delete';

	EXEC PROC_CreatePermission
		@identityPermissionId,
		@identityProductId,
		@identityModuleId,
		@PermissionNamePtBr,
		@PermissionNamePtPt,
		@PermissionNameEnUs,
		@PermissionNameEsAr,
		@permissionKeyList;
END

--Settings - View, Register, Edit, Delete and Activate Guest Policies
BEGIN
	SET @identityPermissionId = N'57b45e33-30bc-4636-b3c1-fa55f98b1ff0';
	
	SET @PermissionNamePtBr = 'Visualizar, Cadastrar, Editar, Deletar e Ativar Políticas do Hotel';
	SET @PermissionNamePtPt = 'Visualizar, Cadastrar, Editar, Deletar e Ativar Políticas do Hotel';
	SET @PermissionNameEnUs = 'View, Register, Edit, Delete and Activate Hotel Policies';
	SET @PermissionNameEsAr = 'Ver, Registrar, Editar, Eliminar y Activar Políticas del Hotel';

	SET @identityProductId = 1;
	SET @identityModuleId = 7;

	SET @permissionKeyList = 
		'PMS_PropertyGuestPolicy_Post,' +
		'PMS_PropertyGuestPolicy_GetAll,' +
		'PMS_PropertyGuestPolicy_Get,' +
		'PMS_PropertyGuestPolicy_Update,' +
		'PMS_PropertyGuestPolicy_Delete';

	EXEC PROC_CreatePermission
		@identityPermissionId,
		@identityProductId,
		@identityModuleId,
		@PermissionNamePtBr,
		@PermissionNamePtPt,
		@PermissionNameEnUs,
		@PermissionNameEsAr,
		@permissionKeyList;
END


--Settings - View and Edit Property Parameters
BEGIN
	SET @identityPermissionId = N'6c61526a-47b8-414b-a79e-69d5f8e79036';
	
	SET @PermissionNamePtBr = 'Visualizar e Editar Parâmetros do Hotel';
	SET @PermissionNamePtPt = 'Visualizar, Cadastrar, Editar, Deletar e Ativar Políticas do Hotel';
	SET @PermissionNameEnUs = 'View and Edit Property Parameters';
	SET @PermissionNameEsAr = 'Ver y Editar Parámetros del Hotel.';

	SET @identityProductId = 1;
	SET @identityModuleId = 7;

	SET @permissionKeyList = 
		'PMS_PropertyParameter_Get_PropertyParameters,'+
		'PMS_PropertyParameter_Update_UpdateSingle,'+
		'PMS_Menu_Hotel_Parameters';

	EXEC PROC_CreatePermission
		@identityPermissionId,
		@identityProductId,
		@identityModuleId,
		@PermissionNamePtBr,
		@PermissionNamePtPt,
		@PermissionNameEnUs,
		@PermissionNameEsAr,
		@permissionKeyList;
END




--Settings - View, Register, Edit, Delete and Activate Hotel Policies
BEGIN
	SET @identityPermissionId = N'fa16a620-9256-45b8-a16c-098e604fff9a';
	
	SET @PermissionNamePtBr = 'Visualizar, Cadastrar, Editar, Deletar e Ativar Políticas do Hotel';
	SET @PermissionNamePtPt = 'Visualizar, Cadastrar, Editar, Deletar e Ativar Políticas do Hotel';
	SET @PermissionNameEnUs = 'View, Register, Edit, Delete and Activate Hotel Policies';
	SET @PermissionNameEsAr = 'Ver, Registrar, Editar, Eliminar y Activar Políticas del Hotel';

	SET @identityProductId = 1;
	SET @identityModuleId = 7;

	SET @permissionKeyList = 
		'PMS_PropertyPolicy_Get_GetAllPropertyPolicy,'+
		'PMS_PropertyPolicy_Post,'+
		'PMS_PropertyPolicy_Patch_ToggleActivation,'+
		'PMS_PropertyPolicy_Update,'+
		'PMS_PropertyPolicy_Delete,'+
		'PMS_Menu_Hotel_Policies';

	EXEC PROC_CreatePermission
		@identityPermissionId,
		@identityProductId,
		@identityModuleId,
		@PermissionNamePtBr,
		@PermissionNamePtPt,
		@PermissionNameEnUs,
		@PermissionNameEsAr,
		@permissionKeyList;
END


--Settings - Hotel - View Contract Data
BEGIN
	SET @identityPermissionId = N'6c61526a-47b8-414b-a79e-69d5f8e79036';
	
	SET @PermissionNamePtBr = 'Hotel - Visualizar Dados do Contrato';
	SET @PermissionNamePtPt = 'Hotel - Visualizar Dados do Contrato';
	SET @PermissionNameEnUs = 'Hotel - View Contract Data';
	SET @PermissionNameEsAr = 'Hotel - Ver Dados do Contrato.';

	SET @identityProductId = 1;
	SET @identityModuleId = 7;

	SET @permissionKeyList = 
		'PMS_Menu_Hotel,'+
		'PMS_Menu_Hotel_Contract,'+
		'PMS_Brand_Get,'+
		'PMS_Property_Get_GetByPropertyId,'+
		'PMS_Chain_Get,'+
		'PMS_Country_Get,'+
		'PMS_Property_ChangePhoto,'+
		'PMS_Property_Update';


	EXEC PROC_CreatePermission
		@identityPermissionId,
		@identityProductId,
		@identityModuleId,
		@PermissionNamePtBr,
		@PermissionNamePtPt,
		@PermissionNameEnUs,
		@PermissionNameEsAr,
		@permissionKeyList;
END


--Settings - User Management
BEGIN
	SET @identityPermissionId = N'49ee4360-0ddd-4e1a-995b-b4d1f249e343';
	
	SET @PermissionNamePtBr = 'Gestão do Usuário';
	SET @PermissionNamePtPt = 'Gestão do Usuário';
	SET @PermissionNameEnUs = 'User Management';
	SET @PermissionNameEsAr = 'Hotel - Ver Dados do Contrato.';

	SET @identityProductId = 1;
	SET @identityModuleId = 7;

	SET @permissionKeyList = 
		'PMS_Menu_User_Management,'+
		'SA_User_Get,'+
		'SA_User_Patch_ToggleActivation,'+
		'SA_User_Patch_Culture,'+
		'SA_Get_By_UserId,'+
		'SA_User_Patch_NewInvitationLink,'+
		'SA_User_Post_CreateNewUser';

	EXEC PROC_CreatePermission
		@identityPermissionId,
		@identityProductId,
		@identityModuleId,
		@PermissionNamePtBr,
		@PermissionNamePtPt,
		@PermissionNameEnUs,
		@PermissionNameEsAr,
		@permissionKeyList;
END

BEGIN
	SET @identityPermissionId = N'5feca9f6-ce45-4aed-8839-3cfa4030c0cd';
	
	SET @PermissionNamePtBr = 'Gestão do Perfil';
	SET @PermissionNamePtPt = 'Gestão do Perfil';
	SET @PermissionNameEnUs = 'Profile Management';
	SET @PermissionNameEsAr = 'Gestión de perfiles';

	SET @identityProductId = 1;
	SET @identityModuleId = 7;

	SET @permissionKeyList = 
		'SA_User_Get,'+
		'SA_User_Patch_PersonChangePwd,'+
		'SA_User_Patch_UpdateUser,'+
		'SA_User_Change_Photo';

	EXEC PROC_CreatePermission
		@identityPermissionId,
		@identityProductId,
		@identityModuleId,
		@PermissionNamePtBr,
		@PermissionNamePtPt,
		@PermissionNameEnUs,
		@PermissionNameEsAr,
		@permissionKeyList;
END


--Settings - TourismTax
BEGIN
	SET @identityPermissionId = N'd17e7f6b-fef2-4c98-b449-f4de69fee295';
	
	SET @PermissionNamePtBr = 'Taxa de Turismo';
	SET @PermissionNamePtPt = 'Taxa de Turismo';
	SET @PermissionNameEnUs = 'Tourism Tax';
	SET @PermissionNameEsAr = 'Tasa de Turismo';

	SET @identityProductId = 1;
	SET @identityModuleId = 7;

	SET @permissionKeyList = 
		'PMS_TourismTax_Get,'+
		'PMS_TourismTax_Post,'+
		'PMS_TourismTax_Put,'+
		'PMS_TourismTax_ToggleActivation,'+
		'PMS_TourismTax_Launch_Patch';

	EXEC PROC_CreatePermission
		@identityPermissionId,
		@identityProductId,
		@identityModuleId,
		@PermissionNamePtBr,
		@PermissionNamePtPt,
		@PermissionNameEnUs,
		@PermissionNameEsAr,
		@permissionKeyList;
END

-- Settings - Associate Services To Iva
BEGIN
	SET @identityPermissionId = N'4c2170f4-12cf-498a-90dd-a488c816a7d9';
	
	SET @PermissionNamePtBr = 'Associar Serviços ao Iva';
	SET @PermissionNamePtPt = 'Associar Serviços ao Iva';
	SET @PermissionNameEnUs = 'Associar Serviços ao Iva';
	SET @PermissionNameEsAr = 'Associar Serviços ao Iva';

	SET @identityProductId = 1;
	SET @identityModuleId = 7;

	SET @permissionKeyList = 
	'PMS_BillingItem_Get_BillingItemService,'+
	'PDV_Product_GetAllIdAndName,'+
	'Tax_Get_Iva,'+
	'Tax_Get_GetAllTaxRulesServices,'+
	'Tax_Get_GetAllTaxRulesProducts,'+
	'Tax_PUT_AssociateServicesAndIva';


	EXEC PROC_CreatePermission
		@identityPermissionId,
		@identityProductId,
		@identityModuleId,
		@PermissionNamePtBr,
		@PermissionNamePtPt,
		@PermissionNameEnUs,
		@PermissionNameEsAr,
		@permissionKeyList;
END


GO