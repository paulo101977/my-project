﻿	Declare @identityPermissionId UNIQUEIDENTIFIER;
	
	Declare @PermissionNamePtBr varchar(150);
	Declare @PermissionNamePtPt varchar(150);
	Declare @PermissionNameEnUs varchar(150);
	Declare @PermissionNameEsAr varchar(150);

	Declare @identityProductId int;
	Declare @identityModuleId int;
	
	Declare @permissionKeyList varchar(MAX);

--INTERACTIVE REPORT

-- Report - Guest in House
BEGIN
	SET @identityPermissionId = N'c7081c2a-bf5d-4af4-bd80-f4e84563e879';
	
	SET @PermissionNamePtBr = 'Hóspede na Casa';
	SET @PermissionNamePtPt = 'Hóspede na Casa';
	SET @PermissionNameEnUs = 'Guest in House';
	SET @PermissionNameEsAr = 'Huésped en la Casa';

	SET @identityProductId = 1;
	SET @identityModuleId = 9;

	SET @permissionKeyList = 
	'PMS_Menu_Reports,' +
	'PMS_PropertyGuestType_Get_ByProperty,' +
	'PMS_ReservationReport_Get_SearchMeanPlan,' +
	'PMS_ReservationReport_Get_Search,'+
	'PMS_ReservationReport_Post_MeanPlan';

	EXEC PROC_CreatePermission
		@identityPermissionId,
		@identityProductId,
		@identityModuleId,
		@PermissionNamePtBr,
		@PermissionNamePtPt,
		@PermissionNameEnUs,
		@PermissionNameEsAr,
		@permissionKeyList;
END



-- Report - Checkout
BEGIN
	SET @identityPermissionId = N'96d38dd3-807a-41e2-98be-ee41150c42f1';
	
	SET @PermissionNamePtBr = 'Checkout';
	SET @PermissionNamePtPt = 'Checkout';
	SET @PermissionNameEnUs = 'Checkout';
	SET @PermissionNameEsAr = 'Checkout';

	SET @identityProductId = 1;
	SET @identityModuleId = 9;

	SET @permissionKeyList = 
	'PMS_Menu_Reports,' +
	'PMS_ReservationReport_Get_SearchCheck';

	EXEC PROC_CreatePermission
		@identityPermissionId,
		@identityProductId,
		@identityModuleId,
		@PermissionNamePtBr,
		@PermissionNamePtPt,
		@PermissionNameEnUs,
		@PermissionNameEsAr,
		@permissionKeyList;
END


-- Report - Check-in
BEGIN
	SET @identityPermissionId = N'acb3e53c-ba0a-468b-be15-2fe410386849';
	
	SET @PermissionNamePtBr = 'Check-in';
	SET @PermissionNamePtPt = 'Check-in';
	SET @PermissionNameEnUs = 'Check-in';
	SET @PermissionNameEsAr = 'Check-in';

	SET @identityProductId = 1;
	SET @identityModuleId = 9;

	SET @permissionKeyList = 
	'PMS_Menu_Reports,' +
	'PMS_ReservationReport_Get_SearchCheck';

	EXEC PROC_CreatePermission
		@identityPermissionId,
		@identityProductId,
		@identityModuleId,
		@PermissionNamePtBr,
		@PermissionNamePtPt,
		@PermissionNameEnUs,
		@PermissionNameEsAr,
		@permissionKeyList;
END


-- Report - Balance of accounts
BEGIN
	SET @identityPermissionId = N'd1ec762b-e2e9-45ac-ba82-5c11626c2af9';
	
	SET @PermissionNamePtBr = 'Saldo das contas';
	SET @PermissionNamePtPt = 'Saldo das contas';
	SET @PermissionNameEnUs = 'Balance of accounts';
	SET @PermissionNameEsAr = 'Saldo de las cuentas';

	SET @identityProductId = 1;
	SET @identityModuleId = 9;

	SET @permissionKeyList = 
	'PMS_Menu_Reports,' +
	'PMS_ReservationReport_Get_SearchGuestSale';

	EXEC PROC_CreatePermission
		@identityPermissionId,
		@identityProductId,
		@identityModuleId,
		@PermissionNamePtBr,
		@PermissionNamePtPt,
		@PermissionNameEnUs,
		@PermissionNameEsAr,
		@permissionKeyList;
END


-- Report - Issued notes
BEGIN
	SET @identityPermissionId = N'28151ae7-74d6-437c-8de4-269ee21a86a9';
	
	SET @PermissionNamePtBr = 'Notas emitidas';
	SET @PermissionNamePtPt = 'Notas emitidas';
	SET @PermissionNameEnUs = 'Issued notes';
	SET @PermissionNameEsAr = 'Notas emitidas';

	SET @identityProductId = 1;
	SET @identityModuleId = 9;

	SET @permissionKeyList = 
	'PMS_Menu_Reports,' +
	'PMS_Currency_Get,' +
	'PMS_ReservationReport_Get_IssuedNotes';

	EXEC PROC_CreatePermission
		@identityPermissionId,
		@identityProductId,
		@identityModuleId,
		@PermissionNamePtBr,
		@PermissionNamePtPt,
		@PermissionNameEnUs,
		@PermissionNameEsAr,
		@permissionKeyList;
END



-- Report - Situation of UH
BEGIN
	SET @identityPermissionId = N'55125c74-aa9b-448b-b986-f975ef574882';
	
	SET @PermissionNamePtBr = 'Situação de UH';
	SET @PermissionNamePtPt = 'Situação de UH';
	SET @PermissionNameEnUs = 'Situation of UH';
	SET @PermissionNameEsAr = 'Situación de HAB';
	
	SET @identityProductId = 1;
	SET @identityModuleId = 9;

	SET @permissionKeyList = 
	'PMS_Menu_Reports,' +
	'PMS_HouseKeepingStatusProperty_Get_GetAllHousekeepingStatusProperty,' +
	'PMS_ReservationReport_Get_UhSituation,' +
	'PMS_Property_Get_RoomTypes';

	EXEC PROC_CreatePermission
		@identityPermissionId,
		@identityProductId,
		@identityModuleId,
		@PermissionNamePtBr,
		@PermissionNamePtPt,
		@PermissionNameEnUs,
		@PermissionNameEsAr,
		@permissionKeyList;
END



-- Report - Report - Bordereau
BEGIN
	SET @identityPermissionId = N'44eb2719-2d3d-466c-a95a-11bd9b9202ec';
	
	SET @PermissionNamePtBr = 'Borderô';
	SET @PermissionNamePtPt = 'Borderô';
	SET @PermissionNameEnUs = 'Bordereau';
	SET @PermissionNameEsAr = 'Borderô';
	
	SET @identityProductId = 1;
	SET @identityModuleId = 9;

	SET @permissionKeyList = 
	'PMS_Menu_Reports,' +
	'PMS_ReservationReport_Get_Bordereau';

	EXEC PROC_CreatePermission
		@identityPermissionId,
		@identityProductId,
		@identityModuleId,
		@PermissionNamePtBr,
		@PermissionNamePtPt,
		@PermissionNameEnUs,
		@PermissionNameEsAr,
		@permissionKeyList;
END


-- Report - Occupation
BEGIN
	SET @identityPermissionId = N'2fa5a137-51fb-4703-894f-a391bf85693b';
	
	SET @PermissionNamePtBr = 'Ocupação';
	SET @PermissionNamePtPt = 'Ocupação';
	SET @PermissionNameEnUs = 'Occupation';
	SET @PermissionNameEsAr = 'Ocupación';
	
	SET @identityProductId = 1;
	SET @identityModuleId = 9;

	SET @permissionKeyList = 
	'PMS_Menu_Reports,' +
	'PMS_Availability_Get_RoomTypes';

	EXEC PROC_CreatePermission
		@identityPermissionId,
		@identityProductId,
		@identityModuleId,
		@PermissionNamePtBr,
		@PermissionNamePtPt,
		@PermissionNameEnUs,
		@PermissionNameEsAr,
		@permissionKeyList;
END



-- Report - Reservations Made
BEGIN
	SET @identityPermissionId = N'56727e32-9b9f-4394-8ac8-06baabf27457';
	
	SET @PermissionNamePtBr = 'Reservas Efetuadas';
	SET @PermissionNamePtPt = 'Reservas Efetuadas';
	SET @PermissionNameEnUs = 'Reservations Made';
	SET @PermissionNameEsAr = 'Reservas Efectuadas';
	
	SET @identityProductId = 1;
	SET @identityModuleId = 9;

	SET @permissionKeyList = 
	'PMS_Menu_Reports,' +
	'PMS_ReservationReport_Get_ReservationsMade';

	EXEC PROC_CreatePermission
		@identityPermissionId,
		@identityProductId,
		@identityModuleId,
		@PermissionNamePtBr,
		@PermissionNamePtPt,
		@PermissionNameEnUs,
		@PermissionNameEsAr,
		@permissionKeyList;
END

-- Report - Reservation With Deposits
BEGIN
	SET @identityPermissionId = N'14592ed6-bb9f-4f7c-b678-f5e26f10dd5a';
	
	SET @PermissionNamePtBr = 'Reservas com depósitos';
	SET @PermissionNamePtPt = 'Reservas com depósitos';
	SET @PermissionNameEnUs = 'Reservation With Deposits';
	SET @PermissionNameEsAr = 'Reservas con depósitos';
	
	SET @identityProductId = 1;
	SET @identityModuleId = 9;

	SET @permissionKeyList = 
	'PMS_Menu_Reports,' +
	'PMS_ReservationReport_Get_ReservationsInAdvance';

	EXEC PROC_CreatePermission
		@identityPermissionId,
		@identityProductId,
		@identityModuleId,
		@PermissionNamePtBr,
		@PermissionNamePtPt,
		@PermissionNameEnUs,
		@PermissionNameEsAr,
		@permissionKeyList;
END


-- Report - Governance and Discrepancy
BEGIN
	SET @identityPermissionId = N'9551ef4f-d24b-4071-b508-8cfbedf5c1c7';
	
	SET @PermissionNamePtBr = 'Governança e Discrepância';
	SET @PermissionNamePtPt = 'Governança e Discrepância';
	SET @PermissionNameEnUs = 'Governance and Discrepancy';
	SET @PermissionNameEsAr = 'Gobernanza y Discrepancia';
	
	SET @identityProductId = 1;
	SET @identityModuleId = 9;

	SET @permissionKeyList = 
	'PMS_Menu_Reports,' +
	'PMS_Property_Get_RoomTypes,' +
	'PMS_HouseKeepingStatusProperty_Get_GetAllHousekeepingStatusProperty,' +
	'PMS_ReservationReport_Get_HousekeepingStatusDisagreement,' +
	'PMS_ReservationReport_Post_HousekeepingRoomDisagreement';

	EXEC PROC_CreatePermission
		@identityPermissionId,
		@identityProductId,
		@identityModuleId,
		@PermissionNamePtBr,
		@PermissionNamePtPt,
		@PermissionNameEnUs,
		@PermissionNameEsAr,
		@permissionKeyList;
END

-- Report - Governance and Discrepancy
BEGIN
	SET @identityPermissionId = N'88762d32-f78f-48bd-8a80-e181a328dc59';
	
	SET @PermissionNamePtBr = 'Dashboard';
	SET @PermissionNamePtPt = 'Dashboard';
	SET @PermissionNameEnUs = 'Dashboard';
	SET @PermissionNameEsAr = 'Dashboard';
	
	SET @identityProductId = 1;
	SET @identityModuleId = 9;

	SET @permissionKeyList = 
	'PMS_Menu_Dashboards,' +
	'PMS_DashboardReport_Get';

	EXEC PROC_CreatePermission
		@identityPermissionId,
		@identityProductId,
		@identityModuleId,
		@PermissionNamePtBr,
		@PermissionNamePtPt,
		@PermissionNameEnUs,
		@PermissionNameEsAr,
		@permissionKeyList;
END

-- Report - Siba Integration
BEGIN
	SET @identityPermissionId = N'd3cfc497-01b9-4bd5-b01f-43cb17e8ccc6';
	
	SET @PermissionNamePtBr = 'Integração Siba';
	SET @PermissionNamePtPt = 'Integração Siba';
	SET @PermissionNameEnUs = 'Integração Siba';
	SET @PermissionNameEsAr = 'Integração Siba';
	
	SET @identityProductId = 1;
	SET @identityModuleId = 9;

	SET @permissionKeyList = 
	'PMS_Menu_SibaIntegration,' +
	'PMS_SibaIntegration_Post';

	EXEC PROC_CreatePermission
		@identityPermissionId,
		@identityProductId,
		@identityModuleId,
		@PermissionNamePtBr,
		@PermissionNamePtPt,
		@PermissionNameEnUs,
		@PermissionNameEsAr,
		@permissionKeyList;
END

-- Dashboard - Property
BEGIN
	SET @identityPermissionId = N'9ac86026-116a-4a11-ad55-4d2ca64b438e';
	
	SET @PermissionNamePtBr = 'Dashboard do hotel';
	SET @PermissionNamePtPt = 'Dashboard do hotel';
	SET @PermissionNameEnUs = 'Dashboard do hotel';
	SET @PermissionNameEsAr = 'Dashboard do hotel';
	
	SET @identityProductId = 1;
	SET @identityModuleId = 9;

	SET @permissionKeyList = 
	'PMS_ReservationReport_Get_PropertyDashboard,' +
	'PMS_Availability_Get_RoomTypes,' +
	'PMS_ReservationReport_Get_Grafics,' +
	'PMS_Property_Get_GetByPropertyId,';

	EXEC PROC_CreatePermission
		@identityPermissionId,
		@identityProductId,
		@identityModuleId,
		@PermissionNamePtBr,
		@PermissionNamePtPt,
		@PermissionNameEnUs,
		@PermissionNameEsAr,
		@permissionKeyList;
END

GO