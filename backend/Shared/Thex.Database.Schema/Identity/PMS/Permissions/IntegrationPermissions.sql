﻿	Declare @identityPermissionId UNIQUEIDENTIFIER;
	
	Declare @PermissionNamePtBr varchar(150);
	Declare @PermissionNamePtPt varchar(150);
	Declare @PermissionNameEnUs varchar(150);
	Declare @PermissionNameEsAr varchar(150);

	Declare @identityProductId int;
	Declare @identityModuleId int;
	
	Declare @permissionKeyList varchar(MAX);

-- INTEGRATION 

-- Integration - Channel
BEGIN
	SET @identityPermissionId = N'ff794d98-781f-4037-9272-21f34f270247';
	
	SET @PermissionNamePtBr = 'Channel';
	SET @PermissionNamePtPt = 'Channel';
	SET @PermissionNameEnUs = 'Channel';
	SET @PermissionNameEsAr = 'Channel';

	SET @identityProductId = 1;
	SET @identityModuleId = 10;

	SET @permissionKeyList = 
	'PMS_Menu_Channel,' +
	'PMS_Channel_GetAll,' +
	'PMS_Channel_Get,' +
	'PMS_Channel_Put,'+
	'PMS_Channel_Post,'+
	'PMS_Channel_Delete,'+
	'PMS_Channel_Patch_ToggleActivation,'+
	'PMS_Channel_Get_Search,' +
	'PMS_MarketSegment_Get';

	EXEC PROC_CreatePermission
		@identityPermissionId,
		@identityProductId,
		@identityModuleId,
		@PermissionNamePtBr,
		@PermissionNamePtPt,
		@PermissionNameEnUs,
		@PermissionNameEsAr,
		@permissionKeyList;
END

BEGIN
	SET @identityPermissionId = N'156b0eb1-fc9e-4fcb-b4bf-208de87c1352';
	
	SET @PermissionNamePtBr = 'Painel de Integração de Reservas';
	SET @PermissionNamePtPt = 'Painel de Integração de Reservas';
	SET @PermissionNameEnUs = 'Reservation Integration Panel';
	SET @PermissionNameEsAr = 'Panel de Integración de Reservas';

	SET @identityProductId = 1;
	SET @identityModuleId = 10;

	SET @permissionKeyList = 
	'PMS_Channel_GetAll,' +
	'PMS_BusinessSource_Get,' +
	'HIGSINTEGRATION_Menu_Reservation_Integration_Panel,' +
	'HIGSINTEGRATION_Get_All_Reservation_Logs,' +
	'HIGSINTEGRATION_Get_All_Reservation_Logs_History';

	EXEC PROC_CreatePermission
		@identityPermissionId,
		@identityProductId,
		@identityModuleId,
		@PermissionNamePtBr,
		@PermissionNamePtPt,
		@PermissionNameEnUs,
		@PermissionNameEsAr,
		@permissionKeyList;
END
GO