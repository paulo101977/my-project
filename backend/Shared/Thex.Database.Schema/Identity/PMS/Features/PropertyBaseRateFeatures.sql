﻿ Declare @featureId UNIQUEIDENTIFIER;
 Declare @identityProductId int;
 Declare @identityModuleId int;
 Declare @featureKey varchar(200);

 BEGIN
	SET @featureId = N'02af0e7d-0a11-4327-a9d7-857a11e53317';
	SET @identityProductId = 1;
	SET @identityModuleId = 1;
	SET @featureKey = 'PMS_PropertyBaseRate_Get_ConfigurationDataParameters';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END

 BEGIN
	SET @featureId = N'3e3b8d49-9ac7-4853-9789-b69c295f7c15';
	SET @identityProductId = 1;
	SET @identityModuleId = 1;
	SET @featureKey = 'PMS_PropertyBaseRate_Get_PropertyBaseRateHeaderHistory';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END

 BEGIN
	SET @featureId = N'bab76259-8bf6-424a-9e8c-3b7f15b9e0f3';
	SET @identityProductId = 1;
	SET @identityModuleId = 1;
	SET @featureKey = 'PMS_PropertyBaseRate_Post';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END

 BEGIN
	SET @featureId = N'fe8791f5-3e19-456c-a22e-c7e3399eb795';
	SET @identityProductId = 1;
	SET @identityModuleId = 1;
	SET @featureKey = 'PMS_PropertyBaseRate_Post_By_Level';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END
 	
 	BEGIN
	SET @featureId = N'cdf75b39-91de-4e74-82a6-7a3ce3ac104c';
	SET @identityProductId = 1;
	SET @identityModuleId = 1;
	SET @featureKey = 'PMS_PropertyBaseRate_Post_By_Premise';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END

GO