﻿  Declare @featureId UNIQUEIDENTIFIER;
 Declare @identityProductId int;
 Declare @identityModuleId int;
 Declare @featureKey varchar(200);

 BEGIN
	SET @featureId = N'942d2719-651e-427c-84cb-3a0b424d41da';
	SET @identityProductId = 1;
	SET @identityModuleId = 1;
	SET @featureKey = 'PMS_ReservationReport_Get_Grafics';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END

 BEGIN
	SET @featureId = N'58c58dc3-640c-4f39-9e02-3003fef18d31';
	SET @identityProductId = 1;
	SET @identityModuleId = 1;
	SET @featureKey = 'PMS_ReservationReport_Get_Search';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END
 
 BEGIN
	SET @featureId = N'd0d9fca4-04a5-4c81-ba95-6d10965ccae5';
	SET @identityProductId = 1;
	SET @identityModuleId = 1;
	SET @featureKey = 'PMS_ReservationReport_Get_SearchCheck';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END
 
 BEGIN
	SET @featureId = N'31dc7a75-04a5-4e08-acab-e828881917c4';
	SET @identityProductId = 1;
	SET @identityModuleId = 1;
	SET @featureKey = 'PMS_ReservationReport_Get_SearchMeanPlan';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END

 BEGIN
	SET @featureId = N'5044fd87-af02-4776-9c90-8b4730b9fe92';
	SET @identityProductId = 1;
	SET @identityModuleId = 1;
	SET @featureKey = 'PMS_ReservationReport_Get_SearchGuestSale';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END

 BEGIN
	SET @featureId = N'1572467d-ae55-4304-a40e-af29054ae9c5';
	SET @identityProductId = 1;
	SET @identityModuleId = 1;
	SET @featureKey = 'PMS_ReservationReport_Post_MeanPlan';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END

 BEGIN
	SET @featureId = N'a91f927b-454d-4cf5-ba1d-836e5775722f';
	SET @identityProductId = 1;
	SET @identityModuleId = 1;
	SET @featureKey = 'PMS_ReservationReport_Get_IssuedNotes';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END
 
 BEGIN
	SET @featureId = N'15030a23-560a-417f-a8f8-af96dad5e95a';
	SET @identityProductId = 1;
	SET @identityModuleId = 1;
	SET @featureKey = 'PMS_ReservationReport_Get_UhSituation';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END
 
 BEGIN
	SET @featureId = N'372440bc-d87e-461c-95a3-54a586f86182';
	SET @identityProductId = 1;
	SET @identityModuleId = 1;
	SET @featureKey = 'PMS_ReservationReport_Get_Bordereau';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END
 
 BEGIN
	SET @featureId = N'cef8bd56-3da2-4b79-a713-61e0f032685d';
	SET @identityProductId = 1;
	SET @identityModuleId = 1;
	SET @featureKey = 'PMS_ReservationReport_Get_HousekeepingStatusDisagreement';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END

 BEGIN
	SET @featureId = N'f81b1238-ddda-4a1e-92e7-820a72139ee3';
	SET @identityProductId = 1;
	SET @identityModuleId = 1;
	SET @featureKey = 'PMS_ReservationReport_Post_HousekeepingRoomDisagreement';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END
 
 BEGIN
	SET @featureId = N'215f659d-8c6e-46e7-93ad-433bd505fa19';
	SET @identityProductId = 1;
	SET @identityModuleId = 1;
	SET @featureKey = 'PMS_ReservationReport_Get_ReservationsMade';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END

 BEGIN
	SET @featureId = N'2f2e04fb-9d8c-428d-96fa-08fb9d2344a0';
	SET @identityProductId = 1;
	SET @identityModuleId = 1;
	SET @featureKey = 'PMS_ReservationReport_Get_ReservationsInAdvance';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END

BEGIN
	SET @featureId = N'38fbc985-ee4c-4eeb-b31c-3a2b9d0adbcc';
	SET @identityProductId = 1;
	SET @identityModuleId = 1;
	SET @featureKey = 'PMS_ReservationReport_Get_PropertyDashboard';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END

GO