﻿ Declare @featureId UNIQUEIDENTIFIER;
 Declare @identityProductId int;
 Declare @identityModuleId int;
 Declare @featureKey varchar(200);

 BEGIN
	SET @featureId = N'0005192a-6484-4d9a-99ce-6e009f23d8ea';
	SET @identityProductId = 1;
	SET @identityModuleId = 1;
	SET @featureKey = 'PMS_BillingItem_Update_PaymentType';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END
 
 BEGIN
	SET @featureId = N'ff5b11fd-aa44-4acc-9436-34d4d1f6d5c5';
	SET @identityProductId = 1;
	SET @identityModuleId = 1;
	SET @featureKey = 'PMS_BillingItem_Post_PaymentType';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END

 BEGIN
	SET @featureId = N'7524ea02-5a01-4f59-8328-5cebbebbeeac';
	SET @identityProductId = 1;
	SET @identityModuleId = 1;
	SET @featureKey = 'PMS_BillingItem_Delete_PaymentType_Params';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END

 BEGIN
	SET @featureId = N'c80c34b0-2b37-4045-9cc3-a86eae6e5cdb';
	SET @identityProductId = 1;
	SET @identityModuleId = 1;
	SET @featureKey = 'PMS_BillingItem_Get_GetPaymentType';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END
 
 BEGIN
	SET @featureId = N'fdebec04-f01a-4899-b154-1783d0898e4a';
	SET @identityProductId = 1;
	SET @identityModuleId = 1;
	SET @featureKey = 'PMS_BillingItem_Patch_PaymentType_Params';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END
 
 BEGIN
	SET @featureId = N'7319666e-c9e6-4b81-abf2-9bb13e150e79';
	SET @identityProductId = 1;
	SET @identityModuleId = 1;
	SET @featureKey = 'PMS_BillingItem_Get_AllPaymentTypes';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END
 
 BEGIN
	SET @featureId = N'5949a517-3277-438d-8249-d87602f86cf4';
	SET @identityProductId = 1;
	SET @identityModuleId = 1;
	SET @featureKey = 'PMS_BillingItem_Get_GetAllBillingItemService';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END

 BEGIN
	SET @featureId = N'95b581c6-09cc-46f8-b7fe-411e19c940b8';
	SET @identityProductId = 1;
	SET @identityModuleId = 1;
	SET @featureKey = 'PMS_BillingItem_Patch_ToggleActivation';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END
 
 BEGIN
	SET @featureId = N'78895654-4043-4fea-b638-c63431cc4db7';
	SET @identityProductId = 1;
	SET @identityModuleId = 1;
	SET @featureKey = 'PMS_BillingItem_Get_GetAllBillingItemForAssociateTax';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END
 
 BEGIN
	SET @featureId = N'238ecce9-1e7c-4629-b28c-1f594a38266c';
	SET @identityProductId = 1;
	SET @identityModuleId = 1;
	SET @featureKey = 'PMS_BillingItem_Get_GetAllBillingItemTax';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END

 BEGIN
	SET @featureId = N'99f8e65b-c611-4757-a733-65b94e235d97';
	SET @identityProductId = 1;
	SET @identityModuleId = 1;
	SET @featureKey = 'PMS_BillingItem_Get_BillingItem_Params';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END
 
 BEGIN
	SET @featureId = N'bb7d7738-2dce-42d7-9c77-657f7cdf90bb';
	SET @identityProductId = 1;
	SET @identityModuleId = 1;
	SET @featureKey = 'PMS_BillingItem_Update_UpdateBillingItemServiceWithTax';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END

 BEGIN
	SET @featureId = N'dd4712f6-41b4-4588-ba61-73eed9efc64b';
	SET @identityProductId = 1;
	SET @identityModuleId = 1;
	SET @featureKey = 'PMS_BillingItem_Post_CreateServiceWithTax';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END
 
 BEGIN
	SET @featureId = N'279e41fc-d8cd-4f9c-a75b-0770d3651371';
	SET @identityProductId = 1;
	SET @identityModuleId = 1;
	SET @featureKey = 'PMS_BillingItem_Delete_Params';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END
 
 BEGIN
	SET @featureId = N'42e4cb90-6774-4a43-9f18-9fe7e79713b4';
	SET @identityProductId = 1;
	SET @identityModuleId = 1;
	SET @featureKey = 'PMS_BillingItem_Get_GetAllBillingItemForAssociateService';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END

 BEGIN
	SET @featureId = N'44d54ff3-a9ec-4203-a0ee-5db32f7a4ca2';
	SET @identityProductId = 1;
	SET @identityModuleId = 1;
	SET @featureKey = 'PMS_BillingItem_Update_UpdateBillingItemTaxWithService';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END

BEGIN
	SET @featureId = N'eead0498-bd65-42d8-a359-c3e7b61a652e';
	SET @identityProductId = 1;
	SET @identityModuleId = 1;
	SET @featureKey = 'PMS_BillingItem_Post_CreateTaxWithService';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END
 
 BEGIN
	SET @featureId = N'357233c0-e8ae-414e-8023-ac1351508b43';
	SET @identityProductId = 1;
	SET @identityModuleId = 1;
	SET @featureKey = 'PMS_BillingItem_Get_BillingItemForParametersDocuments';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END
 
 BEGIN
	SET @featureId = N'8ce4e61e-7df4-473a-9ba8-0ed5df02373f';
	SET @identityProductId = 1;
	SET @identityModuleId = 1;
	SET @featureKey = 'PMS_BillingItem_Get_GetAllServiceForRealese';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END
 
 BEGIN
	SET @featureId = N'74bf6949-d971-482d-adca-bb7b19ac9090';
	SET @identityProductId = 1;
	SET @identityModuleId = 1;
	SET @featureKey = 'PMS_BillingItem_Get_PaymentTypes';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END

 BEGIN
	SET @featureId = N'e3547f1e-07de-458c-abdd-cfc0eef18718';
	SET @identityProductId = 1;
	SET @identityModuleId = 1;
	SET @featureKey = 'PMS_BillingItem_Get_PlasticBrand';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END
 
 BEGIN
	SET @featureId = N'cef805c4-3119-47ee-bc90-e5222fe8f5df';
	SET @identityProductId = 1;
	SET @identityModuleId = 1;
	SET @featureKey = 'PMS_BillingItem_Get_GetAllBillingItemService_Search';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END

BEGIN
	SET @featureId = N'bff608b7-437c-4c07-9373-6a1b02c04a76';
	SET @identityProductId = 1;
	SET @identityModuleId = 7;
	SET @featureKey = 'PMS_BillingItem_Get_BillingItemService';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END
 
GO