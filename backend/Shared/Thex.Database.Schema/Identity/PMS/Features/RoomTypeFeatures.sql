﻿ Declare @featureId UNIQUEIDENTIFIER;
 Declare @identityProductId int;
 Declare @identityModuleId int;
 Declare @featureKey varchar(200);

 BEGIN
	SET @featureId = N'd82ce7c1-1907-41ea-a589-bcf051d18831';
	SET @identityProductId = 1;
	SET @identityModuleId = 1;
	SET @featureKey = 'PMS_RoomType_Patch_ToggleActivation';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END

 BEGIN
	SET @featureId = N'da9a0f92-6fa8-4ba3-9c9f-8a146d55d117';
	SET @identityProductId = 1;
	SET @identityModuleId = 1;
	SET @featureKey = 'PMS_RoomType_Get';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END
 
 BEGIN
	SET @featureId = N'c34d1e19-00d0-448f-a96f-0a018715f100';
	SET @identityProductId = 1;
	SET @identityModuleId = 1;
	SET @featureKey = 'PMS_RoomType_Put';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END

 BEGIN
	SET @featureId = N'51f40593-b802-432e-98d2-352a57f4b23b';
	SET @identityProductId = 1;
	SET @identityModuleId = 1;
	SET @featureKey = 'PMS_RoomType_Post';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END

 BEGIN
	SET @featureId = N'da5ed5f3-12d9-4399-b2f6-01f4a85f7b1f';
	SET @identityProductId = 1;
	SET @identityModuleId = 1;
	SET @featureKey = 'PMS_RoomType_Delete';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END

 BEGIN
	SET @featureId = N'21f90820-c6e7-40df-a44b-37a552451df6';
	SET @identityProductId = 1;
	SET @identityModuleId = 1;
	SET @featureKey = 'PMS_RoomType_Get_WithOverbooking';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END
GO


 