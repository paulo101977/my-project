﻿ Declare @featureId UNIQUEIDENTIFIER;
 Declare @identityProductId int;
 Declare @identityModuleId int;
 Declare @featureKey varchar(200);

 BEGIN
	SET @featureId = N'19987a1c-0519-4b99-9f14-bd25f3115ffd';
	SET @identityProductId = 1;
	SET @identityModuleId = 1;
	SET @featureKey = 'PMS_Brand_Get';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END



GO