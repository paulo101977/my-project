﻿ Declare @featureId UNIQUEIDENTIFIER;
 Declare @identityProductId int;
 Declare @identityModuleId int;
 Declare @featureKey varchar(200);

 BEGIN
	SET @featureId = N'B915415D-E32C-49E0-9C9B-80F05A3421F7';
	SET @identityProductId = 1;
	SET @identityModuleId = 8;
	SET @featureKey = 'PMS_Level_Get_All';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END

 BEGIN
	SET @featureId = N'2CE6F6D8-6966-455F-8DE2-898AAC59DCF0';
	SET @identityProductId = 1;
	SET @identityModuleId = 8;
	SET @featureKey = 'PMS_Level_Get';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END

 BEGIN
	SET @featureId = N'02F678A1-CA10-4BDD-A605-F73C2264C72C';
	SET @identityProductId = 1;
	SET @identityModuleId = 8;
	SET @featureKey = 'PMS_Level_Put';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END

 BEGIN
	SET @featureId = N'6920AC62-C731-4B9E-85F5-075FFAC3DB75';
	SET @identityProductId = 1;
	SET @identityModuleId = 8;
	SET @featureKey = 'PMS_Level_Post';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END

 BEGIN
	SET @featureId = N'CAB65A94-7A8D-47DB-AF41-CE039A9FE2B9';
	SET @identityProductId = 1;
	SET @identityModuleId = 8;
	SET @featureKey = 'PMS_Level_Delete';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END

 BEGIN
	SET @featureId = N'13648385-75E9-447B-A2CA-6C281E196ABE';
	SET @identityProductId = 1;
	SET @identityModuleId = 8;
	SET @featureKey = 'PMS_Level_Patch_ToggleActivation';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END

GO