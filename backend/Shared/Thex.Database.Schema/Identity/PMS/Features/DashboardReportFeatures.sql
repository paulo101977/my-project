﻿  Declare @featureId UNIQUEIDENTIFIER;
 Declare @identityProductId int;
 Declare @identityModuleId int;
 Declare @featureKey varchar(200);

 BEGIN
	SET @featureId = N'8385bde9-9863-40fa-9ea0-295e0427b4ed';
	SET @identityProductId = 1;
	SET @identityModuleId = 9;
	SET @featureKey = 'PMS_DashboardReport_Get';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END

GO