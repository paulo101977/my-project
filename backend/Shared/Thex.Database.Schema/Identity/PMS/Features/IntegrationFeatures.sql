﻿ Declare @featureId UNIQUEIDENTIFIER;
 Declare @identityProductId int;
 Declare @identityModuleId int;
 Declare @featureKey varchar(200);

 BEGIN
	SET @featureId = N'239e035e-fe5b-4eac-af92-3d078b13f9e2';
	SET @identityProductId = 1;
	SET @identityModuleId = 10;
	SET @featureKey = 'PMS_Channel_GetAll';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END


 BEGIN
	SET @featureId = N'26437ff0-7a63-4c1d-a9db-a8dfb06d7124';
	SET @identityProductId = 1;
	SET @identityModuleId = 10;
	SET @featureKey = 'PMS_Channel_Get';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END


 BEGIN
	SET @featureId = N'45c22ac8-fa80-45a0-9a36-24d6c7454962';
	SET @identityProductId = 1;
	SET @identityModuleId = 10;
	SET @featureKey = 'PMS_Channel_Put';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END


 BEGIN
	SET @featureId = N'bb31fbf0-8e3a-44e7-98b5-0dc08b61c1b3';
	SET @identityProductId = 1;
	SET @identityModuleId = 10;
	SET @featureKey = 'PMS_Channel_Post';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END


 BEGIN
	SET @featureId = N'8b2d89a1-9a7e-4dfc-b669-e1673bbeb3c6';
	SET @identityProductId = 1;
	SET @identityModuleId = 10;
	SET @featureKey = 'PMS_Channel_Delete';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END


 BEGIN
	SET @featureId = N'13b83fdd-83dd-4b9a-9a42-70d06cd3e8c2';
	SET @identityProductId = 1;
	SET @identityModuleId = 10;
	SET @featureKey = 'PMS_Channel_Patch_ToggleActivation';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END


 BEGIN
	SET @featureId = N'f54a5ee0-23af-49ed-b90b-d8719159f392';
	SET @identityProductId = 1;
	SET @identityModuleId = 10;
	SET @featureKey = 'PMS_Channel_Get_Search';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END

GO