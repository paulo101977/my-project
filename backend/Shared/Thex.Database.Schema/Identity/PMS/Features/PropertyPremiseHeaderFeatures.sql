﻿ Declare @featureId UNIQUEIDENTIFIER;
 Declare @identityProductId int;
 Declare @identityModuleId int;
 Declare @featureKey varchar(200);

 BEGIN
	SET @featureId = N'0dd0ad7c-fdc6-467b-b68b-3b6d95ac8ece';
	SET @identityProductId = 1;
	SET @identityModuleId = 8;
	SET @featureKey = 'PMS_PropertyPremise_Get';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END

 BEGIN
	SET @featureId = N'4f88a968-6b73-44e0-9a13-5726d0b73a19';
	SET @identityProductId = 1;
	SET @identityModuleId = 8;
	SET @featureKey = 'PMS_PropertyPremise_Patch_ToggleActivation';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END
 
 BEGIN
	SET @featureId = N'59a2f5ea-df68-4b0e-9c7f-bccfe72aa865';
	SET @identityProductId = 1;
	SET @identityModuleId = 8;
	SET @featureKey = 'PMS_PropertyPremise_Get_Param';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END

 BEGIN
	SET @featureId = N'2d249a03-a1d3-4b2c-bd6f-b28242d5cce5';
	SET @identityProductId = 1;
	SET @identityModuleId = 8;
	SET @featureKey = 'PMS_PropertyPremise_Post';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END

 BEGIN
	SET @featureId = N'6c087ece-bd73-4b84-bb9d-6715e6366775';
	SET @identityProductId = 1;
	SET @identityModuleId = 8;
	SET @featureKey = 'PMS_PropertyPremise_Put';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END

 BEGIN
	SET @featureId = N'c739c50e-59cc-4677-aaf1-c211c30f0e8a';
	SET @identityProductId = 1;
	SET @identityModuleId = 8;
	SET @featureKey = 'PMS_PropertyPremise_Delete';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END

GO 
 