﻿ Declare @featureId UNIQUEIDENTIFIER;
 Declare @identityProductId int;
 Declare @identityModuleId int;
 Declare @featureKey varchar(200);

 BEGIN
	SET @featureId = N'a217cefd-1602-42e7-9457-801f6398701e';
	SET @identityProductId = 1;
	SET @identityModuleId = 1;
	SET @featureKey = 'PMS_Availability_Get_RoomTypes';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END

BEGIN
	SET  @featureId = N'c9611137-0e70-4ec7-a155-04e00f6196c7';
	SET  @identityProductId = 1;
	SET  @identityModuleId = 1;
	SET  @featureKey = 'PMS_Availability_Get_Rooms';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END
GO