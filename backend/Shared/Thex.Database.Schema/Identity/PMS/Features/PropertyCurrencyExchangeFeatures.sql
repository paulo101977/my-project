﻿ Declare @featureId UNIQUEIDENTIFIER;
 Declare @identityProductId int;
 Declare @identityModuleId int;
 Declare @featureKey varchar(200);

 BEGIN
	SET @featureId = N'db72f45c-0425-4327-9fd0-2b5492df2587';
	SET @identityProductId = 1;
	SET @identityModuleId = 1;
	SET @featureKey = 'PMS_PropertyCurrencyExchange_Get_QuotationCurrent';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END

 BEGIN
	SET @featureId = N'18a30eba-1c56-45fe-bb8d-4f8d2264d951';
	SET @identityProductId = 1;
	SET @identityModuleId = 1;
	SET @featureKey = 'PMS_PropertyCurrencyExchange_Get_QuotationByPeriod';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END
 
 BEGIN
	SET @featureId = N'653c6c49-2c4b-4c93-813b-6fe18d9e7bd9';
	SET @identityProductId = 1;
	SET @identityModuleId = 1;
	SET @featureKey = 'PMS_PropertyCurrencyExchange_Get_InsertQuotation';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END
 
 BEGIN
	SET @featureId = N'a3eaf313-085c-4006-aa94-3411e499e368';
	SET @identityProductId = 1;
	SET @identityModuleId = 1;
	SET @featureKey = 'PMS_PropertyCurrencyExchange_Get_UpdateQuotationFuture';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END
 
 BEGIN
	SET @featureId = N'9c15c980-b249-4d74-96fb-d7ecc159a2cb';
	SET @identityProductId = 1;
	SET @identityModuleId = 1;
	SET @featureKey = 'PMS_PropertyCurrencyExchange_Get_QuotationByCurrencyId';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
  END

 
 BEGIN
	SET @featureId = N'9edb6cb8-c092-4400-a269-950683a1e13b';
	SET @identityProductId = 1;
	SET @identityModuleId = 1;
	SET @featureKey = 'PMS_PropertyCurrencyExchange_Get_CurrentQuotationList';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END

 BEGIN
	SET @featureId = N'0bdd38d5-afc7-48f3-bad2-c617b4a51417';
	SET @identityProductId = 1;
	SET @identityModuleId = 1;
	SET @featureKey = 'PMS_PropertyCurrencyExchange_Patch_CurrentQuotation';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END


GO