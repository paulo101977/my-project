﻿ Declare @featureId UNIQUEIDENTIFIER;
 Declare @identityProductId int;
 Declare @identityModuleId int;
 Declare @featureKey varchar(200);

 BEGIN
	SET @featureId = N'8e4668ac-1fef-4417-bec5-581ac049a768';
	SET @identityProductId = 1;
	SET @identityModuleId = 8;
	SET @featureKey = 'PMS_PropertyRateStrategy_Patch_ToggleActivation';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END

 BEGIN
	SET @featureId = N'ca71598e-9048-4530-94a2-3889b3c7b4b8';
	SET @identityProductId = 1;
	SET @identityModuleId = 8;
	SET @featureKey = 'PMS_PropertyRateStrategy_Put';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END

 BEGIN
	SET @featureId = N'b278d093-fd89-4229-829f-eddc6339fae9';
	SET @identityProductId = 1;
	SET @identityModuleId = 8;
	SET @featureKey = 'PMS_PropertyRateStrategy_Post';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END

 BEGIN
	SET @featureId = N'062483a5-554f-4f60-a35b-2f9cd685aed6';
	SET @identityProductId = 1;
	SET @identityModuleId = 8;
	SET @featureKey = 'PMS_PropertyRateStrategy_Get_Param';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END

 BEGIN
	SET @featureId = N'6462db2f-9b0f-41f6-8162-efacb587f073';
	SET @identityProductId = 1;
	SET @identityModuleId = 8;
	SET @featureKey = 'PMS_PropertyRateStrategy_Get_GetAllByPropertyId';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END



GO