﻿  Declare @featureId UNIQUEIDENTIFIER;
 Declare @identityProductId int;
 Declare @identityModuleId int;
 Declare @featureKey varchar(200);

 BEGIN
	SET @featureId = N'b61c011f-6e82-46a3-82e8-484b1fe80065';
	SET @identityProductId = 1;
	SET @identityModuleId = 1;
	SET @featureKey = 'PMS_CompanyClient_Get_IsAcquirer';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END

 BEGIN
	SET @featureId = N'8c2d764c-3b61-4167-93f0-d254bb40656d';
	SET @identityProductId = 1;
	SET @identityModuleId = 1;
	SET @featureKey = 'PMS_CompanyClient_Get';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END
 
 BEGIN
	SET @featureId = N'46e854ee-f857-4448-89e4-8d22526daedd';
	SET @identityProductId = 1;
	SET @identityModuleId = 1;
	SET @featureKey = 'PMS_CompanyClient_Patch_ToggleActivation';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END
 
 BEGIN
	SET @featureId = N'099cf70b-e301-4269-9178-87ced8a9f03c';
	SET @identityProductId = 1;
	SET @identityModuleId = 1;
	SET @featureKey = 'PMS_CompanyClient_Get_Params';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END

 BEGIN
	SET @featureId = N'93f9c19a-1f28-4a34-b8fa-512ff18033d2';
	SET @identityProductId = 1;
	SET @identityModuleId = 1;
	SET @featureKey = 'PMS_CompanyClient_Put';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END
 
 BEGIN
	SET @featureId = N'7c5a65ca-4d08-4a61-b667-94dd7d1ea33d';
	SET @identityProductId = 1;
	SET @identityModuleId = 1;
	SET @featureKey = 'PMS_CompanyClient_Post';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END
 
 BEGIN
	SET @featureId = N'73ec48e7-447b-41b1-9367-4adc37b945f0';
	SET @identityProductId = 1;
	SET @identityModuleId = 1;
	SET @featureKey = 'PMS_CompanyClient_Get_SearchWithLocation';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END
 
 BEGIN
	SET @featureId = N'6c42d590-1380-4d89-a91f-c228dd8a95a5';
	SET @identityProductId = 1;
	SET @identityModuleId = 1;
	SET @featureKey = 'PMS_CompanyClient_Get_Search';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END
 
 BEGIN
	SET @featureId = N'2de0fbc5-da8c-42dc-a102-f0343d2cea7f';
	SET @identityProductId = 1;
	SET @identityModuleId = 1;
	SET @featureKey = 'PMS_CompanyClient_Get_ContactPersons';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END

BEGIN
	SET @featureId = N'f8cb924c-23ff-45b0-8b56-0b3de65d090c';
	SET @identityProductId = 1;
	SET @identityModuleId = 1;
	SET @featureKey = 'PMS_CompanyClient_Patch_Associate_With_Tributs_Withheld';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END

BEGIN
	SET @featureId = N'b7504c24-df4b-4c84-9abf-65d7d40a4d28';
	SET @identityProductId = 1;
	SET @identityModuleId = 1;
	SET @featureKey = 'PMS_CompanyClient_Get_Associate_With_Tributs_Withheld';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END

BEGIN
	SET @featureId = N'c7c92eaa-610a-4f24-bff7-7169646bac00';
	SET @identityProductId = 1;
	SET @identityModuleId = 1;
	SET @featureKey = 'PMS_CompanyClient_Delete_Association_With_Tribut_Withheld';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END
 
GO