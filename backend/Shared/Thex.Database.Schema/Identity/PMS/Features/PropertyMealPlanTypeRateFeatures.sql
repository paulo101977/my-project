﻿  Declare @featureId UNIQUEIDENTIFIER;
 Declare @identityProductId int;
 Declare @identityModuleId int;
 Declare @featureKey varchar(200);
 
 BEGIN
	SET @featureId = N'28cffa88-7734-4e55-8438-342fae61bd17';
	SET @identityProductId = 1;
	SET @identityModuleId = 8;
	SET @featureKey = 'PMS_PropertyMealPlanTypeRate_GetAll';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END
 
 BEGIN
	SET @featureId = N'7747592b-73eb-456f-80e2-f935b4cf4fde';
	SET @identityProductId = 1;
	SET @identityModuleId = 8;
	SET @featureKey = 'PMS_PropertyMealPlanTypeRate_Get';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END

 BEGIN
	SET @featureId = N'5a3b73ce-7b1d-46ca-9d34-28bb5cdd65af';
	SET @identityProductId = 1;
	SET @identityModuleId = 8;
	SET @featureKey = 'PMS_PropertyMealPlanTypeRate_Patch_Toggle';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END

 BEGIN
	SET @featureId = N'd40baa04-15dd-46ce-bb6e-88f1fbaf5167';
	SET @identityProductId = 1;
	SET @identityModuleId = 8;
	SET @featureKey = 'PMS_PropertyMealPlanTypeRate_Post';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END
 
 BEGIN
	SET @featureId = N'd3f42af0-4cbf-4c8b-ad85-9c30768a8867';
	SET @identityProductId = 1;
	SET @identityModuleId = 8;
	SET @featureKey = 'PMS_PropertyMealPlanTypeRate_Put';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END
 
 BEGIN
	SET @featureId = N'37281327-1cd5-4571-b29a-3cd2434fbcd4';
	SET @identityProductId = 1;
	SET @identityModuleId = 8;
	SET @featureKey = 'PMS_PropertyMealPlanTypeRate_Delete';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END

GO