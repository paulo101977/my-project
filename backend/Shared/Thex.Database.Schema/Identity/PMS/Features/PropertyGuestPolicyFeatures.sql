﻿  Declare @featureId UNIQUEIDENTIFIER;
 Declare @identityProductId int;
 Declare @identityModuleId int;
 Declare @featureKey varchar(200);

 BEGIN
	SET @featureId = N'8fbc259a-f2f4-411f-bb9a-4439b7352fd3';
	SET @identityProductId = 1;
	SET @identityModuleId = 1;
	SET @featureKey = 'PMS_PropertyGuestPolicy_Post';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END 

 BEGIN
	SET @featureId = N'7f646547-c61e-4786-a4f5-7af6d773910d';
	SET @identityProductId = 1;
	SET @identityModuleId = 1;
	SET @featureKey = 'PMS_PropertyGuestPolicy_GetAll';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END 

 BEGIN
	SET @featureId = N'd000ba14-47f9-492b-9925-9f431aba618e';
	SET @identityProductId = 1;
	SET @identityModuleId = 1;
	SET @featureKey = 'PMS_PropertyGuestPolicy_Get';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END 

 BEGIN
	SET @featureId = N'f64f700a-a784-4a59-ba15-051e524e91c0';
	SET @identityProductId = 1;
	SET @identityModuleId = 1;
	SET @featureKey = 'PMS_PropertyGuestPolicy_Update';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END 

 BEGIN
	SET @featureId = N'e26e6dd1-6722-49af-894d-6cb0cc015346';
	SET @identityProductId = 1;
	SET @identityModuleId = 1;
	SET @featureKey = 'PMS_PropertyGuestPolicy_Delete';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END 

GO