﻿  Declare @featureId UNIQUEIDENTIFIER;
 Declare @identityProductId int;
 Declare @identityModuleId int;
 Declare @featureKey varchar(200);

 BEGIN
	SET @featureId = N'da4778cd-0052-498d-95ca-883f36130987';
	SET @identityProductId = 1;
	SET @identityModuleId = 1;
	SET @featureKey = 'PMS_ReservationItem_Get_Param';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END

 BEGIN
	SET @featureId = N'005b1115-ab9a-4047-9e9b-3bf9600baa62';
	SET @identityProductId = 1;
	SET @identityModuleId = 1;
	SET @featureKey = 'PMS_ReservationItem_Patch_Realease';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END
 
 BEGIN
	SET @featureId = N'f5c8f955-880b-49c1-a40f-b900514ff16f';
	SET @identityProductId = 1;
	SET @identityModuleId = 1;
	SET @featureKey = 'PMS_ReservationItem_Get_EarlyCheckout';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END
 
 BEGIN
	SET @featureId = N'ec8340da-82b6-4e59-bdf8-fb2f6ab4033e';
	SET @identityProductId = 1;
	SET @identityModuleId = 1;
	SET @featureKey = 'PMS_ReservationItem_Patch_Checkout';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END

 BEGIN
	SET @featureId = N'cf349995-de47-489a-b5fa-d7de37c43505';
	SET @identityProductId = 1;
	SET @identityModuleId = 1;
	SET @featureKey = 'PMS_ReservationItem_Patch_Reactivate';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END

 BEGIN
	SET @featureId = N'3d2aff8c-ff9c-4ee4-8dc6-d34447aec771';
	SET @identityProductId = 1;
	SET @identityModuleId = 1;
	SET @featureKey = 'PMS_ReservationItem_Get_Accommodations';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END

 BEGIN
	SET @featureId = N'98abdad5-fa29-43ae-9223-3ebe522eeb14';
	SET @identityProductId = 1;
	SET @identityModuleId = 1;
	SET @featureKey = 'PMS_ReservationItem_Patch_Cancel';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END
 
 BEGIN
	SET @featureId = N'2bebfa65-67d5-4508-a5f5-66cca0e9a6da';
	SET @identityProductId = 1;
	SET @identityModuleId = 1;
	SET @featureKey = 'PMS_ReservationItem_Patch_Budget';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END
 
 BEGIN
	SET @featureId = N'98b70c11-6823-41d4-8daf-9fd230104cbe';
	SET @identityProductId = 1;
	SET @identityModuleId = 1;
	SET @featureKey = 'PMS_ReservationItem_Get_GuestGuestRegistration';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END

 BEGIN
	SET @featureId = N'536ca42d-1f66-4f6a-abc7-1985c8b68384';
	SET @identityProductId = 1;
	SET @identityModuleId = 1;
	SET @featureKey = 'PMS_ReservationItem_Post_SendEmail';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END
 
 BEGIN
	SET @featureId = N'd9d5b397-a446-4779-bb6b-d671f913a3ce';
	SET @identityProductId = 1;
	SET @identityModuleId = 1;
	SET @featureKey = 'PMS_ReservationItem_Get_ConfirmationReservation';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END
 
 BEGIN
	SET @featureId = N'e3303391-4d41-4b72-bb8e-41df51ed78b7';
	SET @identityProductId = 1;
	SET @identityModuleId = 1;
	SET @featureKey = 'PMS_ReservationItem_Patch_AssociateRoom';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END

 BEGIN
	SET @featureId = N'35ba52ba-0a5e-4d70-b9ed-69956551248e';
	SET @identityProductId = 1;
	SET @identityModuleId = 1;
	SET @featureKey = 'PMS_ReservationItem_Post_AccommodationChange';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END
 
 BEGIN
	SET @featureId = N'c83e0b47-3317-4471-a9fd-3baec3a543a8';
	SET @identityProductId = 1;
	SET @identityModuleId = 1;
	SET @featureKey = 'PMS_ReservationItem_Patch_CancelCheckin';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END

  BEGIN
	SET @featureId = N'2f40a659-3815-493a-bc82-f0d8ffda1f59';
	SET @identityProductId = 1;
	SET @identityModuleId = 1;
	SET @featureKey = 'PMS_ReservationItem_Patch_ReactivateNoShow';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END
 

GO