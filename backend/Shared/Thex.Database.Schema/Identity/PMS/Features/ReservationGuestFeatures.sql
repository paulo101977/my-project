﻿ Declare @featureId UNIQUEIDENTIFIER;
 Declare @identityProductId int;
 Declare @identityModuleId int;
 Declare @featureKey varchar(200);

 BEGIN
	SET @featureId = N'0ec8bb6b-338a-45ec-b930-ce5707c4d019';
	SET @identityProductId = 1;
	SET @identityModuleId = 1;
	SET @featureKey = 'PMS_ReservationGuest_Get_Param';

	EXEC PROC_CreateFeature
		@featureId,
		@identityProductId,
		@identityModuleId,
		@featureKey;
END

GO