﻿CREATE TABLE [dbo].[PropertyDocumentType](

    [Id]                        UNIQUEIDENTIFIER    NOT NULL,
	[PropertyId]			    INT					NOT NULL,
	[DocumentTypeId]			INT					NOT NULL,
		

	CONSTRAINT [PK_PropertyDocumentType_ID] PRIMARY KEY ( [Id] ),
	CONSTRAINT [FK_PropertyDocumentType_PropertyId] FOREIGN KEY ([PropertyId]) REFERENCES [dbo].[Property]([PropertyId]),
    CONSTRAINT [FK_PropertyDocumentType_DocumentTypeId] FOREIGN KEY ([DocumentTypeId]) REFERENCES [dbo].[DocumentType]([DocumentTypeId]),
    CONSTRAINT [UK_PropertyDocumentType_PropertyId_DocumentTypeId] UNIQUE ([PropertyId], [DocumentTypeId]),
);
GO

CREATE INDEX 
	x_PropertyDocumentType_PropertyId ON dbo.Property( [PropertyId] )
GO

CREATE INDEX 
	x_PropertyDocumentType_DocumentTypeId ON dbo.DocumentType( [DocumentTypeId] )
GO