﻿CREATE TABLE [dbo].[PropertyAuditProcessStep]
(
	[PropertyAuditProcessStepId] UNIQUEIDENTIFIER NOT NULL PRIMARY KEY,
	[PropertyAuditProcessId] UNIQUEIDENTIFIER	NOT NULL,
	[StartDate]				DateTime			NOT NULL	DEFAULT GETUTCDATE(),
	[EndDate]				DateTime			NULL,
	[Description]			varchar(500)	    NULL,
	[RowTotal]				INT					NOT NULL,
	[CurrentRow]			INT					NOT NULL,
	[PropertyId]			INT					NOT NULL,
	[AuditStepTypeId]		INT					NOT NULL,
	[TenantId]									UNIQUEIDENTIFIER	NOT NULL,
	[IsDeleted]									BIT					NOT NULL	DEFAULT 0,
	[CreationTime]								DATETIME			NOT NULL	DEFAULT GETUTCDATE(),
	[CreatorUserId]								UNIQUEIDENTIFIER	NULL,
	[LastModificationTime]						DATETIME			NULL		DEFAULT GETUTCDATE(),
	[LastModifierUserId]						UNIQUEIDENTIFIER	NULL,
	[DeletionTime]								DATETIME			NULL,
	[DeleterUserId]								UNIQUEIDENTIFIER	NULL,

	CONSTRAINT [FK_PropertyAuditProcessStep_Property] FOREIGN KEY ([PropertyId]) REFERENCES [dbo].[Property] ([PropertyId]),
	CONSTRAINT [FK_PropertyAuditProcessStep_PropertyAuditProcess] FOREIGN KEY ([PropertyAuditProcessId]) REFERENCES [dbo].[PropertyAuditProcess] ([PropertyAuditProcessId]),
	CONSTRAINT [FK_PropertyAuditProcessStep_AuditStepType] FOREIGN KEY ([AuditStepTypeId]) REFERENCES [dbo].[AuditStepType] ([AuditStepTypeId]),
	CONSTRAINT [FK_PropertyAuditProcessStep_Tenant] FOREIGN KEY ([TenantId]) REFERENCES [dbo].[Tenant] ([TenantId]),
	CONSTRAINT [UK_PropertyAuditProcessStep] UNIQUE ([PropertyId], [PropertyAuditProcessId], [AuditStepTypeId], [IsDeleted], [DeletionTime])
)
GO

CREATE INDEX 
	x_PropertyAuditProcessStep_PropertyId ON [dbo].[PropertyAuditProcessStep] ( [PropertyId] ) 
GO

CREATE INDEX 
	x_PropertyAuditProcessStep_PropertyAuditProcessId ON [dbo].[PropertyAuditProcessStep] ( [PropertyAuditProcessId] ) 
GO

CREATE INDEX 
	x_PropertyAuditProcessStep_AuditStepTypeId ON [dbo].[PropertyAuditProcessStep] ( [AuditStepTypeId] ) 
GO

CREATE INDEX 
	x_PropertyAuditProcessStep_TenantId ON [dbo].[PropertyAuditProcessStep] ( [TenantId] ) 
GO
