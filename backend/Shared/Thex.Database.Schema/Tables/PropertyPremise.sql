﻿CREATE TABLE [dbo].[PropertyPremise]
(
	[PropertyPremiseId]				UNIQUEIDENTIFIER	NOT NULL,
	[PropertyId]					INT					NOT NULL,
	[RoomTypeId]					INT					NOT NULL,
	[Pax_1]							NUMERIC (18,4)		NULL,
	[Pax_2]							NUMERIC (18,4)		NULL,
	[Pax_3]							NUMERIC (18,4)		NULL,
	[Pax_4]							NUMERIC (18,4)		NULL,
	[Pax_5]							NUMERIC (18,4)		NULL,
	[Pax_6]							NUMERIC (18,4)		NULL,
	[Pax_7]							NUMERIC (18,4)		NULL,
	[Pax_8]							NUMERIC (18,4)		NULL,
	[Pax_9]							NUMERIC (18,4)		NULL,
	[Pax_10]						NUMERIC (18,4)		NULL,
	[Pax_1_Operator]				nvarchar(1)  		NULL,
	[Pax_2_Operator]				nvarchar(1)		NULL,
	[Pax_3_Operator]				nvarchar(1)		NULL,
	[Pax_4_Operator]				nvarchar(1)		NULL,
	[Pax_5_Operator]				nvarchar(1)		NULL,
	[Pax_6_Operator]				nvarchar(1)		NULL,
	[Pax_7_Operator]				nvarchar(1)		NULL,
	[Pax_8_Operator]				nvarchar(1)		NULL,
	[Pax_9_Operator]				nvarchar(1)		NULL,
	[Pax_10_Operator]				nvarchar(1)		NULL,
	[Pax_Additional]			    NUMERIC (18,4)	NULL,
	[Pax_Additional_Operator]		nvarchar(1)		NULL,
	[Child_1]				NUMERIC (18,4)		NULL,
	[Child_2]				NUMERIC (18,4)		NULL,
	[Child_3]				NUMERIC (18,4)		NULL,
	[Child_1_Operator]				nvarchar(1)		NULL,
	[Child_2_Operator]				nvarchar(1)		NULL,
	[Child_3_Operator]				nvarchar(1)		NULL,
	[TenantId]						UNIQUEIDENTIFIER	NOT NULL,
	[PropertyPremiseHeaderId] UNIQUEIDENTIFIER	NOT NULL,
	[IsDeleted]						BIT					NOT NULL	DEFAULT 0,
	[CreationTime]					DATETIME			NOT NULL	DEFAULT GETUTCDATE(),
	[CreatorUserId]					UNIQUEIDENTIFIER	NULL,
	[LastModificationTime]			DATETIME			NULL		DEFAULT GETUTCDATE(),
	[LastModifierUserId]			UNIQUEIDENTIFIER	NULL,
	[DeletionTime]					DATETIME			NULL,
	[DeleterUserId]					UNIQUEIDENTIFIER	NULL,

	CONSTRAINT [PK_PropertyPremise] PRIMARY KEY ([PropertyPremiseId]),
	CONSTRAINT [UK_PropertyPremise_1] UNIQUE ([PropertyPremiseHeaderId], [PropertyId], [RoomTypeId], [IsDeleted], [DeletionTime]),
	CONSTRAINT [FK_PropertyPremise_Property] FOREIGN KEY ([PropertyId]) REFERENCES [dbo].[Property] ([PropertyId]),
	CONSTRAINT [FK_PropertyPremise_RoomType] FOREIGN KEY ([RoomTypeId]) REFERENCES [dbo].[RoomType] ([RoomTypeId]),
	CONSTRAINT [FK_PropertyPremise_PropertyPremiseHeader] FOREIGN KEY ([PropertyPremiseHeaderId]) REFERENCES [dbo].[PropertyPremiseHeader] ([PropertyPremiseHeaderId]),
	CONSTRAINT [FK_PropertyPremise_Tenant] FOREIGN KEY ([TenantId]) REFERENCES [dbo].[Tenant] ([TenantId]),
)
GO

CREATE INDEX 
	x_PropertyPremise_PropertyId ON dbo.[PropertyPremise] ( [PropertyId] )
GO

CREATE INDEX 
	x_PropertyPremise_RoomTypeId ON dbo.[PropertyPremise] ( [RoomTypeId] )
GO

CREATE INDEX 
	x_PropertyPremise_TenantId ON [dbo].[PropertyPremise]( [TenantId] )
GO

CREATE INDEX 
	x_PropertyPremise_PropertyPremiseHeaderId ON dbo.[PropertyPremiseHeader] ( [PropertyPremiseHeaderId] )
GO