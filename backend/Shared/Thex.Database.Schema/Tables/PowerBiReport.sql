﻿CREATE TABLE [dbo].[PowerBiReport]
(
	[PowerBiReportId]						UNIQUEIDENTIFIER	NOT NULL,
	[PowerBiReportKey]						VARCHAR(100)		NOT NULL,
	[PowerBiReportDescription]				VARCHAR(200)		NULL,

	CONSTRAINT [PK_PowerBiReport] PRIMARY KEY ([PowerBiReportId])
)
