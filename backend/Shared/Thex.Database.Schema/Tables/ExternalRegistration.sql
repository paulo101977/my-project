﻿CREATE TABLE [dbo].[ExternalRegistration]
(
	[ExternalRegistrationId]			UNIQUEIDENTIFIER				NOT NULL,
	[ReservationUid]					UNIQUEIDENTIFIER				NOT NULL,
	[Email]								VARCHAR(200)					NOT NULL,
	[Link]								VARCHAR(4000)					NOT	NULL,
	[TenantId]							UNIQUEIDENTIFIER				NOT NULL,
	[IsDeleted]							BIT								NOT NULL	DEFAULT 0,
	[CreationTime]						DATETIME						NOT NULL	DEFAULT GETUTCDATE(),
	[CreatorUserId]						UNIQUEIDENTIFIER				NULL,
	[LastModificationTime]				DATETIME						NULL		DEFAULT GETUTCDATE(),
	[LastModifierUserId]				UNIQUEIDENTIFIER				NULL,
	[DeletionTime]						DATETIME						NULL,
	[DeleterUserId]						UNIQUEIDENTIFIER				NULL,
	
	CONSTRAINT [PK_ExternalRegistration] PRIMARY KEY ([ExternalRegistrationId]),
	CONSTRAINT [FK_ExternalRegistration_ReservationUid] FOREIGN KEY ([ReservationUid]) REFERENCES [dbo].[Reservation] ([ReservationUid]),
	CONSTRAINT [FK_ExternalRegistration_Tenant] FOREIGN KEY ([TenantId]) REFERENCES [dbo].[Tenant] ([TenantId])
)
GO
