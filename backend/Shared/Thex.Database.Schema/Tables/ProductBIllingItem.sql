﻿CREATE TABLE [dbo].[ProductBillingItem]
(
	[ProductBillingItemId]			UNIQUEIDENTIFIER	NOT NULL,
	[ProductId]						UNIQUEIDENTIFIER	NOT NULL,
	[BillingItemId]					INTEGER				NOT NULL,
	[UnitPrice]						NUMERIC (18,4)		NULL,
	[IsActive]						BIT					NOT NULL		DEFAULT 1,

	[TenantId]						UNIQUEIDENTIFIER	NOT NULL,

	[IsDeleted]						BIT					NOT NULL	DEFAULT 0,
	[CreationTime]					DATETIME			NOT NULL	DEFAULT GETUTCDATE(),
	[CreatorUserId]					UNIQUEIDENTIFIER	NULL,
	[LastModificationTime]			DATETIME			NULL		DEFAULT GETUTCDATE(),
	[LastModifierUserId]			UNIQUEIDENTIFIER	NULL,
	[DeletionTime]					DATETIME			NULL,
	[DeleterUserId]					UNIQUEIDENTIFIER	NULL,

	CONSTRAINT [PK_ProductBillingItem] PRIMARY KEY ([ProductBillingItemId]),
	CONSTRAINT [FK_ProductBillingItem_Product] FOREIGN KEY ([ProductId]) REFERENCES [dbo].[Product] ([ProductId]),
	CONSTRAINT [FK_ProductBillingItem_BillingItem] FOREIGN KEY ([BillingItemId]) REFERENCES [dbo].[BillingItem] ([BillingItemId]),
	CONSTRAINT [FK_ProductBillingItem_Tenant] FOREIGN KEY ([TenantId]) REFERENCES [dbo].[Tenant] ([TenantId]),

);
GO

CREATE INDEX 
	x_ProductBillingItem_ProductId ON dbo.ProductBillingItem( [ProductId] )
GO

CREATE INDEX 
	x_ProductBillingItem_BillingItemId ON dbo.ProductBillingItem( [BillingItemId] )
GO

CREATE INDEX 
	x_ProductBillingItem_TenantId ON dbo.ProductBillingItem( [TenantId] )
GO