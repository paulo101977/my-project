﻿CREATE TABLE [dbo].[PropertyFacility]
(
	[PropertyId]				INT					NOT NULL,
	[FacilityId]				UNIQUEIDENTIFIER	NOT NULL,
	[TenantId]					UNIQUEIDENTIFIER	NOT NULL,

	[IsDeleted]					BIT					NOT NULL	DEFAULT 0,
	[CreationTime]				DATETIME			NOT NULL	DEFAULT GETUTCDATE(),
	[CreatorUserId]				UNIQUEIDENTIFIER	NULL,
	[LastModificationTime]		DATETIME			NULL		DEFAULT GETUTCDATE(),
	[LastModifierUserId]		UNIQUEIDENTIFIER	NULL,
	[DeletionTime]				DATETIME			NULL,
	[DeleterUserId]				UNIQUEIDENTIFIER	NULL,

	CONSTRAINT [PK_PropertyFacility] PRIMARY KEY ([PropertyId], [FacilityId]),
	CONSTRAINT [FK_PropertyFacility_Property] FOREIGN KEY ([PropertyId]) REFERENCES [dbo].[Property] ([PropertyId]),
	CONSTRAINT [FK_PropertyFacility_Facility] FOREIGN KEY ([FacilityId]) REFERENCES [dbo].[Facility] ([FacilityId]),
	CONSTRAINT [FK_PropertyFacility_Tenant] FOREIGN KEY ([TenantId]) REFERENCES [dbo].[Tenant] ([TenantId])
);
GO

CREATE INDEX 
	x_PropertyFacility_TenantId ON dbo.PropertyFacility( [TenantId] )
GO