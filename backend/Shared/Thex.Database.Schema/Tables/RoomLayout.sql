﻿CREATE TABLE [dbo].[RoomLayout]
(
	[RoomLayoutId]	UNIQUEIDENTIFIER	NOT NULL,
	[QuantitySingle]		TINYINT				NOT NULL,
	[QuantityDouble]		TINYINT				NOT NULL,
	CONSTRAINT [PK_RoomLayout] PRIMARY KEY ([RoomLayoutId])
);
GO


