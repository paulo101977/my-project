﻿CREATE TABLE [dbo].[Users](
	[Id] UNIQUEIDENTIFIER NOT NULL,
	[UserName] [nvarchar](256) NULL,
	[NormalizedUserName] [nvarchar](256) NULL,
	[Email] [nvarchar](256) NULL,
	[NormalizedEmail] [nvarchar](256) NULL,
	[EmailConfirmed] [bit] NOT NULL,
	[PasswordHash] [nvarchar](max) NULL,
	[SecurityStamp] [nvarchar](max) NULL,
	[ConcurrencyStamp] [nvarchar](max) NULL,
	[PhoneNumber] [nvarchar](max) NULL,
	[PhoneNumberConfirmed] [bit] NOT NULL,
	[TwoFactorEnabled] [bit] NOT NULL,
	[LockoutEnd] [datetimeoffset](7) NULL,
	[LockoutEnabled] [bit] NOT NULL,
	[AccessFailedCount] [int] NOT NULL,
	[PersonId] [uniqueidentifier] NULL,
	[TenantId] [uniqueidentifier] NULL,
	[PreferredLanguage] [char](5) NULL,
	[PreferredCulture] [char](5) NULL,
	[Name] [varchar](300) NULL,
	[IsAdmin] [bit] NULL,
	[IsActive] [bit] NULL,
	[IsDeleted] [bit] NULL,
	[CreationTime] [datetime] NULL,
	[CreatorUserId] [uniqueidentifier] NULL,
	[LastModificationTime] [datetime] NULL,
	[LastModifierUserId] [uniqueidentifier] NULL,
	[DeletionTime] [datetime] NULL,
	[DeleterUserId] [uniqueidentifier] NULL,

	CONSTRAINT [PK_Users] PRIMARY KEY CLUSTERED 
	(
		[Id]
	),
	CONSTRAINT [UK_UserName]    UNIQUE ([UserName]),
	
	CONSTRAINT [FK_Users_Person]	FOREIGN KEY ([PersonId]) REFERENCES [dbo].[Person] ([PersonId]),
	CONSTRAINT [FK_Users_Tenant]	FOREIGN KEY ([TenantId]) REFERENCES [dbo].[Tenant] ([TenantId])
);
GO
