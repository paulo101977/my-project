﻿CREATE TABLE [dbo].[Room]
(
	[RoomId]									INT					NOT NULL	IDENTITY,
	[ParentRoomId]								INT					NULL,
	[PropertyId]								INT					NOT NULL,
	[RoomTypeId]								INT					NOT NULL,
	[Building]									VARCHAR (20)		NULL,
	[Wing]										VARCHAR (20)		NULL,
	[Floor]										VARCHAR (20)		NULL,
	[RoomNumber]								VARCHAR (10)		NOT NULL,
	[Remarks]									VARCHAR (4000)		NULL,		
	[IsActive]									BIT					NOT NULL,
	[HousekeepingStatusPropertyId]				UNIQUEIDENTIFIER	NOT NULL,
	[HousekeepingStatusLastModificationTime]    DATETIME            NULL,
	[TenantId]									UNIQUEIDENTIFIER	NOT NULL,

	[IsDeleted]									BIT					NOT NULL	DEFAULT 0,
	[CreationTime]								DATETIME			NOT NULL	DEFAULT GETUTCDATE(),
	[CreatorUserId]								UNIQUEIDENTIFIER	NULL,
	[LastModificationTime]						DATETIME			NULL		DEFAULT GETUTCDATE(),
	[LastModifierUserId]						UNIQUEIDENTIFIER	NULL,
	[DeletionTime]								DATETIME			NULL,
	[DeleterUserId]								UNIQUEIDENTIFIER	NULL,


	CONSTRAINT [PK_Room] PRIMARY KEY ([RoomId]),
	CONSTRAINT [FK_Room_Property] FOREIGN KEY ([PropertyId]) REFERENCES [dbo].[Property] ([PropertyId]),
	CONSTRAINT [FK_Room_RoomType] FOREIGN KEY ([RoomTypeId]) REFERENCES [dbo].[RoomType] ([RoomTypeId]),
	CONSTRAINT [FK_Room_Room] FOREIGN KEY ([ParentRoomId]) REFERENCES [dbo].[Room] ([RoomId]),
	CONSTRAINT [UK_Room_PropertyId_RoomNumber_IsDeleted_DeletionTime] UNIQUE ([PropertyId],[RoomNumber], [IsDeleted], [DeletionTime]),
	CONSTRAINT [FK_Room_HousekeepingStatusProperty] FOREIGN KEY ([HousekeepingStatusPropertyId]) REFERENCES [dbo].[HousekeepingStatusProperty] ([HousekeepingStatusPropertyId]),
	CONSTRAINT [FK_Room_Tenant] FOREIGN KEY ([TenantId]) REFERENCES [dbo].[Tenant] ([TenantId])
);
GO

CREATE INDEX [x_Room_PropertyId] ON [dbo].[Room] ([PropertyId]);
GO

CREATE INDEX [x_Room_HousekeepingStatusPropertyId] ON [dbo].[Room] ([HousekeepingStatusPropertyId]);
GO

CREATE INDEX [x_Room_RoomTypeId] ON [dbo].[Room] ([RoomTypeId]);
GO

CREATE INDEX 
	x_Room_TenantId ON [dbo].[Room]( [TenantId] )
GO