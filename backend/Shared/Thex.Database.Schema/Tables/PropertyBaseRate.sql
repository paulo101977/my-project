﻿CREATE TABLE [dbo].[PropertyBaseRate]
(
	[PropertyBaseRateId]			UNIQUEIDENTIFIER	NOT NULL,
	[PropertyId]					INT					NOT NULL,
	[Date]							DATETIME			NOT NULL,
	[MealPlanTypeDefault]			INT					NOT NULL	DEFAULT 1,
	[MealPlanTypeId]				INT					NOT NULL,
	[RoomTypeId]					INT					NOT NULL,
	[Pax_1_Amount]					NUMERIC (18,4)		NULL,
	[Pax_2_Amount]					NUMERIC (18,4)		NULL,
	[Pax_3_Amount]					NUMERIC (18,4)		NULL,
	[Pax_4_Amount]					NUMERIC (18,4)		NULL,
	[Pax_5_Amount]					NUMERIC (18,4)		NULL,
	[Pax_6_Amount]					NUMERIC (18,4)		NULL,
	[Pax_7_Amount]					NUMERIC (18,4)		NULL,
	[Pax_8_Amount]					NUMERIC (18,4)		NULL,
	[Pax_9_Amount]					NUMERIC (18,4)		NULL,
	[Pax_10_Amount]					NUMERIC (18,4)		NULL,
	[Pax_11_Amount]					NUMERIC (18,4)		NULL,
	[Pax_12_Amount]					NUMERIC (18,4)		NULL,
	[Pax_13_Amount]					NUMERIC (18,4)		NULL,
	[Pax_14_Amount]					NUMERIC (18,4)		NULL,
	[Pax_15_Amount]					NUMERIC (18,4)		NULL,
	[Pax_16_Amount]					NUMERIC (18,4)		NULL,
	[Pax_17_Amount]					NUMERIC (18,4)		NULL,
	[Pax_18_Amount]					NUMERIC (18,4)		NULL,
	[Pax_19_Amount]					NUMERIC (18,4)		NULL,
	[Pax_20_Amount]					NUMERIC (18,4)		NULL,
	[Pax_Additional_Amount]			NUMERIC (18,4)		NULL,
	[Child_1_Amount]				NUMERIC (18,4)		NULL,
	[Child_2_Amount]				NUMERIC (18,4)		NULL,
	[Child_3_Amount]				NUMERIC (18,4)		NULL,
	[Adult_MealPlan_Amount]			NUMERIC (18,4)		NULL,
	[Child_1_MealPlan_Amount]		NUMERIC (18,4)		NULL,
	[Child_2_MealPlan_Amount]		NUMERIC (18,4)		NULL,
	[Child_3_MealPlan_Amount]		NUMERIC (18,4)		NULL,
	[CurrencyId]					UNIQUEIDENTIFIER	NOT NULL,
	[CurrencySymbol]				VARCHAR(10)			NULL,
	[RatePlanId]					UNIQUEIDENTIFIER	NULL,
	[TenantId]						UNIQUEIDENTIFIER	NOT NULL,
	[PropertyBaseRateHeaderHistoryId] UNIQUEIDENTIFIER	NULL,
	[Level]							INT					NOT NULL DEFAULT 1,
	[LevelId]						UNIQUEIDENTIFIER	NULL,
	[LevelRateId]					UNIQUEIDENTIFIER	NULL,
	[PropertyBaseRateTypeId]		INT					NULL,
	[PropertyMealPlanTypeRate]		BIT					NULL,

	[IsDeleted]						BIT					NOT NULL	DEFAULT 0,
	[CreationTime]					DATETIME			NOT NULL	DEFAULT GETUTCDATE(),
	[CreatorUserId]					UNIQUEIDENTIFIER	NULL,
	[LastModificationTime]			DATETIME			NULL		DEFAULT GETUTCDATE(),
	[LastModifierUserId]			UNIQUEIDENTIFIER	NULL,
	[DeletionTime]					DATETIME			NULL,
	[DeleterUserId]					UNIQUEIDENTIFIER	NULL,

	CONSTRAINT [PK_PropertyBaseRate] PRIMARY KEY ([PropertyBaseRateId]),
	CONSTRAINT [UK_PropertyBaseRate] UNIQUE ( [PropertyId], [Date], [MealPlanTypeId], [RoomTypeId], [CurrencyId], [RatePlanId] ),
	CONSTRAINT [FK_PropertyBaseRate_Property] FOREIGN KEY ([PropertyId]) REFERENCES [dbo].[Property] ([PropertyId]),
	CONSTRAINT [FK_PropertyBaseRate_RoomType] FOREIGN KEY ([RoomTypeId]) REFERENCES [dbo].[RoomType] ([RoomTypeId]),
	CONSTRAINT [FK_PropertyBaseRate_MealPlanType] FOREIGN KEY ([MealPlanTypeId]) REFERENCES [dbo].[MealPlanType] ([MealPlanTypeId]),
	CONSTRAINT [FK_PropertyBaseRate_Currency] FOREIGN KEY ([CurrencyId]) REFERENCES [dbo].[Currency] ([CurrencyId]),
	CONSTRAINT [FK_PropertyBaseRate_RatePlan] FOREIGN KEY ([RatePlanId]) REFERENCES [dbo].[RatePlan] ([RatePlanId]),
	CONSTRAINT [FK_PropertyBaseRate_PropertyBaseRateHeaderHistory] FOREIGN KEY ([PropertyBaseRateHeaderHistoryId]) REFERENCES [dbo].[PropertyBaseRateHeaderHistory] ([PropertyBaseRateHeaderHistoryId]),
	CONSTRAINT [FK_PropertyBaseRate_Tenant] FOREIGN KEY ([TenantId]) REFERENCES [dbo].[Tenant] ([TenantId]),
	CONSTRAINT [FK_PropertyBaseRate_Level] FOREIGN KEY ([LevelId]) REFERENCES [dbo].[Level] ([LevelId]),
	CONSTRAINT [FK_PropertyBaseRate_LevelRateHeader] FOREIGN KEY ([LevelRateId]) REFERENCES [dbo].[LevelRateHeader] ([LevelRateHeaderId]),
	CONSTRAINT [FK_PropertyBaseRate_PropertyBaseRateType] FOREIGN KEY ([PropertyBaseRateTypeId]) REFERENCES [dbo].[PropertyBaseRateType] ([PropertyBaseRateTypeId]),
)
GO

CREATE INDEX 
	x_PropertyBaseRate_PropertyId ON dbo.[PropertyBaseRate] ( [PropertyId] )
GO

CREATE INDEX 
	x_PropertyBaseRate_RoomTypeId ON dbo.[PropertyBaseRate] ( [RoomTypeId] )
GO

CREATE INDEX 
	x_PropertyBaseRate_MealPlanTypeId ON dbo.[PropertyBaseRate] ( [MealPlanTypeId] )
GO

CREATE INDEX 
	x_PropertyBaseRate_CurrencyId ON dbo.[PropertyBaseRate] ( [CurrencyId] )
GO

CREATE INDEX 
	x_PropertyBaseRate_TenantId ON [dbo].[PropertyBaseRate]( [TenantId] )
GO

CREATE INDEX 
	x_PropertyBaseRate_RatePlanId ON dbo.[PropertyBaseRate] ( [RatePlanId] )
GO

CREATE INDEX 
	x_PropertyBaseRate_PropertyBaseRateHeaderHistoryId ON dbo.[PropertyBaseRateHeaderHistory] ( [PropertyBaseRateHeaderHistoryId] )
GO

CREATE INDEX 
	x_PropertyBaseRate_LevelId ON dbo.[Level] ( [LevelId] )
GO

CREATE INDEX 
	x_PropertyBaseRate_LevelRateId ON dbo.[LevelRateHeader] ( [LevelRateHeaderId] )
GO

CREATE INDEX 
	x_PropertyBaseRate_PropertyBaseRateTypeId ON dbo.[PropertyBaseRateType] ( [PropertyBaseRateTypeId] )
GO

