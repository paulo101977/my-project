﻿CREATE TABLE [dbo].[PropertyContactPerson]
(
	[PersonId]					UNIQUEIDENTIFIER	NOT NULL,
	[PropertyId]				INTEGER				NOT NULL,
	[OccupationId]				INTEGER				NULL,
	[TenantId]					UNIQUEIDENTIFIER	NOT NULL,

	[IsDeleted]					BIT					NOT NULL	DEFAULT 0,
	[CreationTime]				DATETIME			NOT NULL	DEFAULT GETUTCDATE(),
	[CreatorUserId]				UNIQUEIDENTIFIER	NULL,
	[LastModificationTime]		DATETIME			NULL		DEFAULT GETUTCDATE(),
	[LastModifierUserId]		UNIQUEIDENTIFIER	NULL,
	[DeletionTime]				DATETIME			NULL,
	[DeleterUserId]				UNIQUEIDENTIFIER	NULL,

	CONSTRAINT [PK_PropertyContactPerson] PRIMARY KEY ([PersonId], [PropertyId]),
	CONSTRAINT [FK_PropertyContactPerson_Person] FOREIGN KEY ([PersonId]) REFERENCES [dbo].[Person] (PersonId),
	CONSTRAINT [FK_PropertyContactPerson_Property] FOREIGN KEY ([PropertyId]) REFERENCES [dbo].[Property] (PropertyId),
	CONSTRAINT [FK_PropertyContactPerson_Occupation] FOREIGN KEY ([OccupationId]) REFERENCES [dbo].[Occupation] (OccupationId),
	CONSTRAINT [FK_PropertyContactPerson_Tenant] FOREIGN KEY ([TenantId]) REFERENCES [dbo].[Tenant] ([TenantId])
)
GO

CREATE INDEX 
	x_PropertyContactPerson_Occupation ON dbo.Occupation( [OccupationId] )
GO

CREATE INDEX 
	x_PropertyContactPerson_TenantId ON [dbo].[PropertyContactPerson]( [TenantId] )
GO