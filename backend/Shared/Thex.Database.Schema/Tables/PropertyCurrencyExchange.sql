﻿CREATE TABLE [dbo].[PropertyCurrencyExchange]
(
	[PropertyCurrencyExchangeId]		UNIQUEIDENTIFIER	NOT NULL,
	[PropertyId]						INT					NOT NULL,
	[PropertyCurrencyExchangeDate]		DATETIME			NOT NULL,
	[CurrencyId]						UNIQUEIDENTIFIER	NOT NULL,
	[ExchangeRate]						NUMERIC(18,4)		NOT NULL,
	[TenantId]							UNIQUEIDENTIFIER	NOT NULL,

	[IsDeleted]							BIT					NOT NULL	DEFAULT 0,
	[CreationTime]						DATETIME			NOT NULL	DEFAULT GETUTCDATE(),
	[CreatorUserId]						UNIQUEIDENTIFIER	NULL,
	[LastModificationTime]				DATETIME			NULL		DEFAULT GETUTCDATE(),
	[LastModifierUserId]				UNIQUEIDENTIFIER	NULL,
	[DeletionTime]						DATETIME			NULL,
	[DeleterUserId]						UNIQUEIDENTIFIER	NULL,

	CONSTRAINT [PK_PropertyCurrencyExchange] PRIMARY KEY ([PropertyCurrencyExchangeId]),
	CONSTRAINT [FK_PropertyCurrencyExchange_Currency] FOREIGN KEY ([CurrencyId]) REFERENCES [dbo].[Currency] ([CurrencyId]),
	CONSTRAINT [FK_PropertyCurrencyExchange_Property] FOREIGN KEY ([PropertyId]) REFERENCES [dbo].[Property] ([PropertyId]),
	CONSTRAINT [FK_PropertyCurrencyExchange_Tenant] FOREIGN KEY ([TenantId]) REFERENCES [dbo].[Tenant] ([TenantId])
)
GO

CREATE INDEX 
	x_PropertyCurrencyExchange_CurrencyId ON dbo.PropertyCurrencyExchange( [CurrencyId] )
GO

CREATE INDEX 
	x_PropertyCurrencyExchange_PropertyId ON dbo.PropertyCurrencyExchange( [PropertyId] )
GO

CREATE INDEX 
	x_PropertyCurrencyExchange_TenantId ON [dbo].[PropertyCurrencyExchange]( [TenantId] )
GO