﻿CREATE TABLE [dbo].[Company]
(
	[CompanyId] 		INT					NOT NULL	IDENTITY,
	[CompanyUid] 		UNIQUEIDENTIFIER	NOT NULL	DEFAULT NEWID(),
	[PersonId] 			UNIQUEIDENTIFIER	NOT NULL,
	[ParentCompanyId]	INT 				NULL,
	[ShortName] 		VARCHAR (50) 		NOT NULL,
	[TradeName]			VARCHAR (200)		NOT NULL,

	CONSTRAINT [PK_Company] PRIMARY KEY ([CompanyId]),
	CONSTRAINT [FK_Company_Person] FOREIGN KEY ([PersonId]) REFERENCES [dbo].[Person] (PersonId),
	CONSTRAINT [FK_Company_Company] FOREIGN KEY ([ParentCompanyId]) REFERENCES [dbo].[Company] (CompanyId),
);
GO

CREATE INDEX 
	x_Company_ParentCompanyId ON dbo.Company( [ParentCompanyId] )
GO

CREATE INDEX 
	x_Company_PersonId ON dbo.Company( [PersonId] )
GO
