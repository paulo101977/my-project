﻿CREATE TABLE [dbo].[ChainMarketSegment]
(
	[ChainMarketSegmentId]		UNIQUEIDENTIFIER	NOT NULL,
	[ChainId]					INT		NOT NULL,
	[MarketSegmentId]			INT		NOT NULL,
	[IsActive]					BIT		NOT NULL	DEFAULT 1,	

	[IsDeleted]					BIT					NOT NULL	DEFAULT 0,
	[CreationTime]				DATETIME			NOT NULL	DEFAULT GETUTCDATE(),
	[CreatorUserId]				UNIQUEIDENTIFIER	NULL,
	[LastModificationTime]		DATETIME			NULL		DEFAULT GETUTCDATE(),
	[LastModifierUserId]		UNIQUEIDENTIFIER	NULL,
	[DeletionTime]				DATETIME			NULL,
	[DeleterUserId]				UNIQUEIDENTIFIER	NULL,

	CONSTRAINT [PK_ChainMarketSegment] PRIMARY KEY ([ChainMarketSegmentId]),
	CONSTRAINT [UK_ChainMarketSegment] UNIQUE ([ChainId], [MarketSegmentId]),
	CONSTRAINT [FK_ChainMarketSegment_Chain] FOREIGN KEY ([ChainId]) REFERENCES [Chain]([ChainId]),
	CONSTRAINT [FK_ChainMarketSegment_MarketSegment] FOREIGN KEY ([MarketSegmentId]) REFERENCES [MarketSegment]([MarketSegmentId])
)
GO

CREATE INDEX 
	x_ChainMarketSegment_MarketSegmentId ON dbo.[ChainMarketSegment]( [MarketSegmentId] )
GO
