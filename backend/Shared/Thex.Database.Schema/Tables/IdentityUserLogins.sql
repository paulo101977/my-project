﻿
CREATE TABLE [dbo].[UserLogins](
	[LoginProvider]			NVARCHAR(128)	NOT NULL,
	[ProviderKey]			NVARCHAR(128)	NOT NULL,
	[ProviderDisplayName]	[nvarchar](max)		NULL,
	[UserId]				UNIQUEIDENTIFIER	NOT NULL,
	
	[IsDeleted]					BIT					NOT NULL	DEFAULT 0,
	[CreationTime]				DATETIME			NOT NULL	DEFAULT GETUTCDATE(),
	[CreatorUserId]				UNIQUEIDENTIFIER	NULL,
	[LastModificationTime]		DATETIME			NULL		DEFAULT GETUTCDATE(),
	[LastModifierUserId]		UNIQUEIDENTIFIER	NULL,
	[DeletionTime]				DATETIME			NULL,
	[DeleterUserId]				UNIQUEIDENTIFIER	NULL,
	CONSTRAINT [PK_UserLogins] PRIMARY KEY CLUSTERED 
	(
		[LoginProvider] ASC,
		[ProviderKey] ASC
	),
	CONSTRAINT [FK_AspNetUserLogins_AspNetUsers_UserId] FOREIGN KEY([UserId]) REFERENCES [dbo].[Users] ([Id])
)