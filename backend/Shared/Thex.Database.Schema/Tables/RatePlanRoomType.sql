﻿CREATE TABLE [dbo].[RatePlanRoomType]
(
	[RatePlanRoomTypeId]			UNIQUEIDENTIFIER	NOT NULL,
	[RatePlanId]					UNIQUEIDENTIFIER	NOT NULL,
	[RoomTypeId]					INT					NOT NULL,
	[PercentualAmmount]				NUMERIC(18,4)		NOT NULL,
	[IsDecreased]					BIT					NOT NULL,
	[PublishOnline]					BIT					NOT NULL	DEFAULT 0,
	[TenantId]					UNIQUEIDENTIFIER	NOT NULL,

	[IsDeleted]					BIT					NOT NULL	DEFAULT 0,
	[CreationTime]				DATETIME			NOT NULL	DEFAULT GETUTCDATE(),
	[CreatorUserId]				UNIQUEIDENTIFIER	NULL,
	[LastModificationTime]		DATETIME			NULL		DEFAULT GETUTCDATE(),
	[LastModifierUserId]		UNIQUEIDENTIFIER	NULL,
	[DeletionTime]				DATETIME			NULL,
	[DeleterUserId]				UNIQUEIDENTIFIER	NULL,
	
	CONSTRAINT [PK_RatePlanRoomType] PRIMARY KEY ( [RatePlanRoomTypeId] ),
	CONSTRAINT [FK_RatePlanRoomType_RoomType] FOREIGN KEY ( [RoomTypeId] ) REFERENCES [dbo].[RoomType] ( [RoomTypeId] ),
	CONSTRAINT [FK_RatePlanRoomType_RatePlanId] FOREIGN KEY ( [RatePlanId] ) REFERENCES [dbo].[RatePlan] ( [RatePlanId] ),
	CONSTRAINT [FK_RatePlanRoomType_Tenant] FOREIGN KEY ([TenantId]) REFERENCES [dbo].[Tenant] ([TenantId])
)
GO

CREATE INDEX 
	x_RatePlanRoomType_RoomTypeId ON dbo.[RatePlanRoomType] ( [RoomTypeId] )
GO

CREATE INDEX 
	x_RatePlanRoomType_RatePlanId ON dbo.[RatePlanRoomType] ( [RatePlanId] )
GO

CREATE INDEX 
	x_RatePlanRoomType_TenantId ON [dbo].[RatePlanRoomType]( [TenantId] )
GO