﻿CREATE TABLE [dbo].[PropertyGuestType]
(
	[PropertyGuestTypeId]		INTEGER			IDENTITY				NOT NULL,
	[PropertyId]				INTEGER									NOT NULL,
	[GuestTypeName]				VARCHAR(100)							NOT NULL,
	[IsVIP]						BIT				DEFAULT 0				NOT NULL,
	
	[Code] VARCHAR(20) NULL, 
    [IsIncognito] BIT NULL DEFAULT 0 , 
    [IsActive] BIT NULL DEFAULT 1 , 
	[IsExemptOfTourismTax] BIT NULL DEFAULT 0, 
    CONSTRAINT [PK_PropertyGuestType] PRIMARY KEY ([PropertyGuestTypeId]),
	CONSTRAINT [FK_PropertyGuestType_Property] FOREIGN KEY ([PropertyId]) REFERENCES [dbo].[Property] ([PropertyId])
)
GO

CREATE INDEX 
    x_PropertyGuestType_PropertyId ON dbo.PropertyGuestType( [PropertyId] )
GO

