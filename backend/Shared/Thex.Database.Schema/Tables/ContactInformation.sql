﻿CREATE TABLE [dbo].[ContactInformation] (
	[OwnerId]							UNIQUEIDENTIFIER 	NOT NULL,	
	[ContactInformationTypeId]			INT		  			NOT NULL,
	[Information]						VARCHAR (500)		NOT NULL,

	CONSTRAINT [PK_ContactInformation] PRIMARY KEY ([OwnerId], [ContactInformationTypeId], [Information]),
	CONSTRAINT [FK_ContactInformation_Person] FOREIGN KEY ([OwnerId]) REFERENCES [dbo].[Person] ([PersonId]),
	CONSTRAINT [FK_ContactInformation_ContactInformationType] FOREIGN KEY ([ContactInformationTypeId]) REFERENCES [dbo].[ContactInformationType] ([ContactInformationTypeId])
);
GO