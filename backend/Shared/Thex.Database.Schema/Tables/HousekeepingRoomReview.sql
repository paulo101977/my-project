﻿CREATE TABLE [dbo].[HousekeepingRoomReview]
(
	[HousekeepingRoomReviewId]			UNIQUEIDENTIFIER	NOT NULL,
	[PropertyId]						INT					NOT NULL,
	[HousekeepingRoomId]				UNIQUEIDENTIFIER	NOT NULL,
	[ReasonId]							INT					NOT NULL,
	[Review]							VARCHAR(4000)		NULL,
	[IsDeleted]							BIT					NOT NULL	DEFAULT 0,
	[CreationTime]						DATETIME			NOT NULL	DEFAULT GETUTCDATE(),
	[CreatorUserId]						UNIQUEIDENTIFIER	NULL,
	[LastModificationTime]				DATETIME			NULL		DEFAULT GETUTCDATE(),
	[LastModifierUserId]				UNIQUEIDENTIFIER	NULL,
	[DeletionTime]						DATETIME			NULL,
	[DeleterUserId]						UNIQUEIDENTIFIER	NULL,
	

	CONSTRAINT [PK_HousekeepingRoomReview] PRIMARY KEY ( [HousekeepingRoomReviewId] ),
	CONSTRAINT [FK_HousekeepingRoomReview_HousekeepingRoom] FOREIGN KEY ( [HousekeepingRoomId] ) REFERENCES [dbo].[HousekeepingRoom] ( [HousekeepingRoomId] ),
	CONSTRAINT [FK_HousekeepingRoomReview_Property] FOREIGN KEY ( [PropertyId] ) REFERENCES [dbo].[Property] ( [PropertyId] ),
	CONSTRAINT [FK_HousekeepingRoomReview_Reason] FOREIGN KEY ( [ReasonId] ) REFERENCES [dbo].[Reason] ( [ReasonId] ),
)
GO

CREATE INDEX 
	x_HousekeepingRoomReview_HousekeepingRoomId ON [HousekeepingRoomReview] ( [HousekeepingRoomId] )
GO

CREATE INDEX 
	x_HousekeepingRoomReview_PropertyId ON [HousekeepingRoomReview] ( [PropertyId] )
GO

CREATE INDEX 
	x_HousekeepingRoomReview_ReasonId ON [HousekeepingRoomReview] ( [ReasonId] )
GO