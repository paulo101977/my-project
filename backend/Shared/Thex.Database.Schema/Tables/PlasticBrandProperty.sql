﻿CREATE TABLE [dbo].[PlasticBrandProperty]
(
	[PlasticBrandPropertyId]				UNIQUEIDENTIFIER			NOT NULL,
	[PlasticBrandId]						INT							NOT NULL,
	[PropertyId]							INT							NOT NULL,
	[InstallmentsQuantity]					SMALLINT					NOT NULL,
	[IsActive]								BIT							NOT NULL	DEFAULT 1,
	[TenantId]								UNIQUEIDENTIFIER	NOT NULL,

	[IsDeleted]								BIT					NOT NULL	DEFAULT 0,
	[CreationTime]							DATETIME			NOT NULL	DEFAULT GETUTCDATE(),
	[CreatorUserId]							UNIQUEIDENTIFIER	NULL,
	[LastModificationTime]					DATETIME			NULL		DEFAULT GETUTCDATE(),
	[LastModifierUserId]					UNIQUEIDENTIFIER	NULL,
	[DeletionTime]							DATETIME			NULL,
	[DeleterUserId]							UNIQUEIDENTIFIER	NULL,

	CONSTRAINT [PK_PlasticBrandProperty] PRIMARY KEY ( [PlasticBrandPropertyId] ),
	CONSTRAINT [UK_PlasticBrandProperty_01] UNIQUE ( [PlasticBrandId],[PropertyId] ),
	CONSTRAINT [FK_PlasticBrandProperty_PlasticBrand] FOREIGN KEY ( [PlasticBrandId] ) REFERENCES [dbo].[PlasticBrand] ( [PlasticBrandId] ),
	CONSTRAINT [FK_PlasticBrandProperty_Property] FOREIGN KEY ( [PropertyId] ) REFERENCES [dbo].[Property] ( [PropertyId] ),
	CONSTRAINT [FK_PlasticBrandProperty_Tenant] FOREIGN KEY ([TenantId]) REFERENCES [dbo].[Tenant] ([TenantId])
)
GO

CREATE INDEX 
	x_PlasticBrandProperty_PlasticBrandId ON PlasticBrandProperty ( [PlasticBrandId] )
GO

CREATE INDEX 
	x_PlasticBrandProperty_PropertyId ON PlasticBrandProperty ( [PropertyId] )
GO

CREATE INDEX 
	x_PlasticBrandProperty_TenantId ON [dbo].[PlasticBrandProperty]( [TenantId] )
GO