﻿CREATE TABLE [dbo].[UserRoles](
	[UserRolesId]	UNIQUEIDENTIFIER	NOT NULL,
	[UserId]		UNIQUEIDENTIFIER	NOT NULL,
	[RoleId]		UNIQUEIDENTIFIER	NOT NULL,

	[ChainId]					INT					NULL,
	[PropertyId]				INT					NULL,
	[BrandId]					INT					NULL,

	[IsDeleted]					BIT					NOT NULL	DEFAULT 0,
	[CreationTime]				DATETIME			NOT NULL	DEFAULT GETUTCDATE(),
	[CreatorUserId]				UNIQUEIDENTIFIER	NULL,
	[LastModificationTime]		DATETIME			NULL		DEFAULT GETUTCDATE(),
	[LastModifierUserId]		UNIQUEIDENTIFIER	NULL,
	[DeletionTime]				DATETIME			NULL,
	[DeleterUserId]				UNIQUEIDENTIFIER	NULL,

	CONSTRAINT [PK_UserRoles] PRIMARY KEY CLUSTERED 
	(
		[UserRolesId]
	),
	CONSTRAINT [FK_AspNetUserRoles_AspNetRoles_RoleId] FOREIGN KEY([RoleId]) REFERENCES [dbo].[Roles] ([Id]),
	CONSTRAINT [FK_AspNetUserRoles_AspNetUsers_UserId] FOREIGN KEY([UserId]) REFERENCES [dbo].[Users] ([Id]),
	
	CONSTRAINT [FK_AspNetUserRoles_Property] FOREIGN KEY ([PropertyId]) REFERENCES [dbo].[Property] ([PropertyId]),
	CONSTRAINT [FK_AspNetUserRoles_Chain] FOREIGN KEY ([ChainId]) REFERENCES [dbo].[Chain] ([ChainId]),
	CONSTRAINT [FK_AspNetUserRoles_Brand] FOREIGN KEY ([BrandId]) REFERENCES [dbo].[Brand] ([BrandId])
);
GO
