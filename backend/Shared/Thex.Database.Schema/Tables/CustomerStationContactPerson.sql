﻿CREATE TABLE [dbo].[CustomerStationContactPerson]
(
	[CustomerStationContactPersonId]	UNIQUEIDENTIFIER	NOT NULL,
	[PersonId]						UNIQUEIDENTIFIER	NOT NULL,
	[CustomerStationId]				UNIQUEIDENTIFIER	NOT NULL,
	[OccupationId]					INTEGER				NULL,

	CONSTRAINT [PK_CustomerStationContactPerson] PRIMARY KEY ([CustomerStationContactPersonId]),
	CONSTRAINT [UK_CustomerStationContactPerson] UNIQUE ([PersonId], [CustomerStationId]),
	CONSTRAINT [FK_CustomerStationContactPerson_Person] FOREIGN KEY ([PersonId]) REFERENCES [dbo].[Person] ([PersonId]),
	CONSTRAINT [FK_CustomerStationContactPerson_Company] FOREIGN KEY ([CustomerStationId]) REFERENCES [dbo].[CustomerStation] ([CustomerStationId]),
	CONSTRAINT [FK_CustomerStationContactPerson_Occupation] FOREIGN KEY ([OccupationId]) REFERENCES [dbo].[Occupation] ([OccupationId])
)
GO

CREATE INDEX 
	x_CustomerStationContactPerson_Occupation ON dbo.Occupation( [OccupationId] )
GO

CREATE INDEX 
	x_CustomerStationContactPerson_CustomerStationId ON dbo.[CustomerStation]( [CustomerStationId] )
GO