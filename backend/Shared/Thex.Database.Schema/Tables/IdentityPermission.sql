﻿CREATE TABLE [dbo].[IdentityPermission](
	[IdentityPermissionId]	UNIQUEIDENTIFIER	NOT NULL,
	[IdentityProductId]		INT					NOT NULL,
	[IdentityModuleId]		INT					NOT NULL,
	[Name]					VARCHAR(150)		NOT NULL,
	[Description]			VARCHAR(400)		NOT NULL,
	[IsActive]				BIT					NOT NULL	DEFAULT 1,
	
	[IsDeleted]					BIT					NOT NULL	DEFAULT 0,
	[CreationTime]				DATETIME			NOT NULL	DEFAULT GETUTCDATE(),
	[CreatorUserId]				UNIQUEIDENTIFIER	NULL,
	[LastModificationTime]		DATETIME			NULL		DEFAULT GETUTCDATE(),
	[LastModifierUserId]		UNIQUEIDENTIFIER	NULL,
	[DeletionTime]				DATETIME			NULL,
	[DeleterUserId]				UNIQUEIDENTIFIER	NULL,
	
    CONSTRAINT [PK_IdentityPermission] PRIMARY KEY ([IdentityPermissionId]),
	CONSTRAINT [FK_IdentityPermission_IdentityProduct] FOREIGN KEY ([IdentityProductId]) REFERENCES [dbo].[IdentityProduct] ([IdentityProductId]),
	CONSTRAINT [FK_IdentityPermission_IdentityModule] FOREIGN KEY ([IdentityModuleId]) REFERENCES [dbo].[IdentityModule] ([IdentityModuleId])
)