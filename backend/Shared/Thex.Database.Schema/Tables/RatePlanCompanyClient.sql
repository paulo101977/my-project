﻿CREATE TABLE [dbo].[RatePlanCompanyClient]
(
	[RatePlanCompanyClientId]		UNIQUEIDENTIFIER	NOT NULL,
	[CompanyClientId]				UNIQUEIDENTIFIER	NOT NULL,
	[RatePlanId]					UNIQUEIDENTIFIER	NOT NULL,
	[TenantId]						UNIQUEIDENTIFIER	NOT NULL,

	[IsDeleted]						BIT					NOT NULL	DEFAULT 0,
	[CreationTime]					DATETIME			NOT NULL	DEFAULT GETUTCDATE(),
	[CreatorUserId]					UNIQUEIDENTIFIER	NULL,
	[LastModificationTime]			DATETIME			NULL		DEFAULT GETUTCDATE(),
	[LastModifierUserId]			UNIQUEIDENTIFIER	NULL,
	[DeletionTime]					DATETIME			NULL,
	[DeleterUserId]					UNIQUEIDENTIFIER	NULL,

	CONSTRAINT [PK_RatePlanCompanyClient] PRIMARY KEY ( [RatePlanCompanyClientId] ),
	CONSTRAINT [FK_RatePlanCompanyClient_CompanyClient] FOREIGN KEY ( [CompanyClientId] ) REFERENCES [dbo].[CompanyClient] ( [CompanyClientId] ),
	CONSTRAINT [FK_RatePlanCompanyClient_RatePlan] FOREIGN KEY ( [RatePlanId] ) REFERENCES [dbo].[RatePlan] ( [RatePlanId] ),
	CONSTRAINT [FK_RatePlanCompanyClient_Tenant] FOREIGN KEY ([TenantId]) REFERENCES [dbo].[Tenant] ([TenantId])
)
GO

CREATE INDEX 
	x_RatePlanCompanyClient_CompanyClientId ON dbo.[RatePlanCompanyClient] ( [CompanyClientId] )
GO

CREATE INDEX 
	x_RatePlanCompanyClient_RatePlanId ON dbo.[RatePlanCompanyClient] ( [RatePlanId] )
GO

CREATE INDEX 
	x_RatePlanCompanyClient_TenantId ON [dbo].[RatePlanCompanyClient]( [TenantId] )
GO