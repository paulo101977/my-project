﻿CREATE TABLE [dbo].[PropertyParameter]
(
	[PropertyParameterId]				UNIQUEIDENTIFIER	NOT NULL,
	[PropertyId]						INT					NOT NULL,
	[ApplicationParameterId]			INT					NOT NULL,
	[PropertyParameterValue]			VARCHAR(MAX)		NULL,
	[PropertyParameterMinValue]			VARCHAR(100)		NULL,
	[PropertyParameterMaxValue]			VARCHAR(100)		NULL,
	[PropertyParameterPossibleValues]	VARCHAR(MAX)		NULL,
	[IsActive]							BIT					NULL DEFAULT 0,
	[TenantId]							UNIQUEIDENTIFIER	NOT NULL,

	[IsDeleted]							BIT					NOT NULL	DEFAULT 0,
	[CreationTime]						DATETIME			NOT NULL	DEFAULT GETUTCDATE(),
	[CreatorUserId]						UNIQUEIDENTIFIER	NULL,
	[LastModificationTime]				DATETIME			NULL		DEFAULT GETUTCDATE(),
	[LastModifierUserId]				UNIQUEIDENTIFIER	NULL,
	[DeletionTime]						DATETIME			NULL,
	[DeleterUserId]						UNIQUEIDENTIFIER	NULL,
	
	CONSTRAINT [PK_PropertyParameter]	PRIMARY KEY ([PropertyParameterId]),
	CONSTRAINT [UK_PropertyParameter] UNIQUE ( [PropertyId], [ApplicationParameterId]),
	CONSTRAINT [FK_PropertyParameter_Property] FOREIGN KEY ([PropertyId]) REFERENCES [dbo].[Property] ([PropertyId]),
	CONSTRAINT [FK_PropertyParameter_PropertyParameter] FOREIGN KEY ([ApplicationParameterId]) REFERENCES [dbo].[ApplicationParameter] ([ApplicationParameterId]),
	CONSTRAINT [FK_PropertyParameter_Tenant] FOREIGN KEY ([TenantId]) REFERENCES [dbo].[Tenant] ([TenantId])
)
GO

CREATE INDEX 
	x_PropertyParameter_PropertyId ON [dbo].[PropertyParameter] ( [PropertyId] )
GO

CREATE INDEX 
	x_PropertyParameter_ApplicationParameterId ON [dbo].[PropertyParameter] ( [ApplicationParameterId] )
GO

CREATE INDEX 
	x_PropertyParameter_TenantId ON [dbo].[PropertyParameter]( [TenantId] )
GO