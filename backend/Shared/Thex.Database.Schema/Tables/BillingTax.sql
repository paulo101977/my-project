﻿CREATE TABLE [dbo].[BillingTax]
(
	[BillingTaxId]					UNIQUEIDENTIFIER	NOT NULL,
	[BillingItemId]					INTEGER				NOT NULL,
	[BillingItemTaxId]				INTEGER				NOT NULL,
	[TaxPercentage]					NUMERIC(18,4)		NOT NULL,
	[BeginDate]						DATE				NOT NULL,
	[EndDate]						DATE				NULL,
	[IsActive]						BIT					NOT NULL,
	[TenantId]						UNIQUEIDENTIFIER	NOT NULL,

	[IsDeleted]						BIT					NOT NULL	DEFAULT 0,
	[CreationTime]					DATETIME			NOT NULL	DEFAULT GETUTCDATE(),
	[CreatorUserId]					UNIQUEIDENTIFIER	NULL,
	[LastModificationTime]			DATETIME			NULL		DEFAULT GETUTCDATE(),
	[LastModifierUserId]			UNIQUEIDENTIFIER	NULL,
	[DeletionTime]					DATETIME			NULL,
	[DeleterUserId]					UNIQUEIDENTIFIER	NULL,

	CONSTRAINT [PK_BillingTax] PRIMARY KEY ( [BillingTaxId] ),
	CONSTRAINT [FK_BillingTax_BillingItem] FOREIGN KEY ( [BillingItemId] ) REFERENCES [dbo].[BillingItem] ( [BillingItemId] ),
	CONSTRAINT [FK_BillingTax_BillingItem_Tax] FOREIGN KEY ( [BillingItemTaxId] ) REFERENCES [dbo].[BillingItem] ( [BillingItemId] ),
	CONSTRAINT [FK_BillingTax_Tenant] FOREIGN KEY ([TenantId]) REFERENCES [dbo].[Tenant] ([TenantId])
)
GO

CREATE INDEX 
	x_BillingTax_BillingItemId ON BillingTax ( [BillingItemId] ) 
GO

CREATE INDEX 
	x_BillingTax_BillingItemTaxId ON BillingTax ( [BillingItemTaxId] ) 
GO

CREATE INDEX 
	x_BillingTax_TenantId ON [dbo].[BillingTax]( [TenantId] )
GO