﻿CREATE TABLE [dbo].[CompanyClient]
(
	[CompanyClientId]						UNIQUEIDENTIFIER	NOT NULL,
	[CompanyId]								INT					NOT NULL,
	[PersonId]								UNIQUEIDENTIFIER	NOT NULL,
	[PersonType]							CHAR (1)			NOT NULL,
	[ShortName]								VARCHAR(100)		NOT NULL,
	[TradeName]								VARCHAR(200)		NOT NULL,
	[DateOfBirth]							DATE			 	NULL,
	[CountrySubdivisionId]					INTEGER				NOT NULL,
	[PropertyCompanyClientCategoryId]		UNIQUEIDENTIFIER	NULL,
	[IsActive]								BIT					NOT NULL	DEFAULT 1,
	[IsAcquirer]							BIT					NOT NULL	DEFAULT 0,
	[ExternalCode]							VARCHAR(100)	NULL,

	CONSTRAINT [PK_CompanyClient] PRIMARY KEY ([CompanyClientId]),
	CONSTRAINT [FK_CompanyClient_Person] FOREIGN KEY ([PersonId]) REFERENCES [dbo].[Person] (PersonId),
	CONSTRAINT [FK_CompanyClient_Company] FOREIGN KEY ([CompanyId]) REFERENCES [dbo].[Company] (CompanyId),
	CONSTRAINT [FK_CompanyClient_PropertyCompanyClientCategory] FOREIGN KEY ([PropertyCompanyClientCategoryId]) REFERENCES [dbo].[PropertyCompanyClientCategory] ([PropertyCompanyClientCategoryId]),
	CONSTRAINT [CK_CompanyClient_PersonType] CHECK ([PersonType] IN ('L', 'N'))
);
GO

CREATE INDEX 
	x_CompanyClient_PersonId ON dbo.CompanyClient( [PersonId] )
GO

CREATE INDEX 
	x_CompanyClient_CompanyId ON dbo.CompanyClient( [CompanyId] )
GO

CREATE INDEX 
	x_CompanyClient_PropertyCompanyClientCategoryId ON dbo.CompanyClient( [PropertyCompanyClientCategoryId] )
GO