﻿CREATE TABLE [dbo].[ReservationItemLaunch]
(
	[ReservationItemLaunchId]			UNIQUEIDENTIFIER					NOT NULL,
	[ReservationItemId]					BIGINT								NOT NULL,
	[GuestReservationItemId]			BIGINT								NULL,
	[QuantityOfTourismTaxLaunched]		INT									NULL,

	[TenantId]					UNIQUEIDENTIFIER	NOT NULL,
	[IsDeleted]					BIT					NOT NULL	DEFAULT 0,
	[CreationTime]				DATETIME			NOT NULL	DEFAULT GETUTCDATE(),
	[CreatorUserId]				UNIQUEIDENTIFIER	NULL,
	[LastModificationTime]		DATETIME			NULL		DEFAULT GETUTCDATE(),
	[LastModifierUserId]		UNIQUEIDENTIFIER	NULL,
	[DeletionTime]				DATETIME			NULL,
	[DeleterUserId]				UNIQUEIDENTIFIER	NULL,

	CONSTRAINT [PK_ReservationItemLaunch] PRIMARY KEY ([ReservationItemLaunchId]), 
	CONSTRAINT [FK_ReservationItemLaunch_ReservationItem] FOREIGN KEY ([ReservationItemId]) REFERENCES [ReservationItem]([ReservationItemId]),
	CONSTRAINT [FK_ReservationItemLaunch_GuestReservationItem] FOREIGN KEY ([GuestReservationItemId]) REFERENCES [GuestReservationItem]([GuestReservationItemId]),
	CONSTRAINT [FK_ReservationItemLaunch_Tenant] FOREIGN KEY ([TenantId]) REFERENCES [dbo].[Tenant] ([TenantId])
);
GO

CREATE INDEX 
	x_ReservationItemLaunch_ReservationItemId ON [dbo].[ReservationItemLaunch]( [ReservationItemId] )
GO

CREATE INDEX 
	x_ReservationItemLaunch_GuestReservationItemId ON [dbo].[ReservationItemLaunch]( [GuestReservationItemId] )
GO

CREATE INDEX 
	x_ReservationItemLaunch_TenantId ON [dbo].[ReservationItemLaunch]( [TenantId] )
GO