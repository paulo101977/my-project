﻿CREATE TABLE [dbo].[UserTenant]
(
	[UserTenantId]					UNIQUEIDENTIFIER	NOT NULL,
	[UserId]						UNIQUEIDENTIFIER	NOT NULL,
	[TenantId]						UNIQUEIDENTIFIER	NOT NULL,
	[IsActive]						BIT					NOT NULL	DEFAULT 1,

	[IsDeleted]						BIT					NOT NULL	DEFAULT 0,
	[CreationTime]					DATETIME			NOT NULL	DEFAULT GETUTCDATE(),
	[CreatorUserId]					UNIQUEIDENTIFIER	NULL,
	[LastModificationTime]			DATETIME			NULL		DEFAULT GETUTCDATE(),
	[LastModifierUserId]			UNIQUEIDENTIFIER	NULL,
	[DeletionTime]					DATETIME			NULL,
	[DeleterUserId]					UNIQUEIDENTIFIER	NULL,

	CONSTRAINT [PK_UserTenant] PRIMARY KEY ([UserTenantId]),
	CONSTRAINT [UK_UserTenant] UNIQUE ( [UserId], [TenantId], [IsDeleted], [DeletionTime] ),
	CONSTRAINT [FK_UserTenant_Users] FOREIGN KEY ([UserId]) REFERENCES [Users]([Id]),
	CONSTRAINT [FK_UserTenant_Tenant] FOREIGN KEY ([TenantId]) REFERENCES [Tenant]([TenantId]),
);

GO

CREATE INDEX 
	x_UserTenant_UserId ON dbo.UserTenant( [UserId] )

GO

CREATE INDEX 
	x_UserTenant_TenantId ON dbo.UserTenant( [TenantId] )