﻿CREATE TABLE [dbo].[GuestMealPlanControl]
(
	 [GuestMealPlanControlId]  UNIQUEIDENTIFIER NOT NULL
	,[GuestReservationItemId]  BIGINT NOT NULL
	,[Coffee] BIT NOT NULL DEFAULT 0
	,[Lunch] BIT NOT NULL DEFAULT 0
	,[Dinner] BIT NOT NULL DEFAULT 0
	,[GuestMealPlanControlDate] DATETIME NOT NULL DEFAULT GETUTCDATE()
	,[PropertyId] INT NOT NULL
	,[TenantId] UNIQUEIDENTIFIER NOT NULL
	,[IsDeleted] BIT NOT NULL DEFAULT 0
	,[CreationTime] DATETIME DEFAULT GETUTCDATE()
	,[CreatorUserId] UNIQUEIDENTIFIER
	,[LastModificationTime]  DATETIME DEFAULT GETUTCDATE()
	,[LastModifierUserId] UNIQUEIDENTIFIER
	,[DeletionTime] DATETIME DEFAULT GETUTCDATE()
	,[DeleterUserId] UNIQUEIDENTIFIER

	CONSTRAINT [PK_GuestMealPlanControl] PRIMARY KEY ([GuestMealPlanControlId]),
	CONSTRAINT [UK_GuestMealPlanControl] UNIQUE ([GuestMealPlanControlId], [GuestMealPlanControlDate]),
	CONSTRAINT [FK_GuestMealPlanControl_GuestReservationItem] FOREIGN KEY ([GuestReservationItemId]) REFERENCES [GuestReservationItem]([GuestReservationItemId]),
	CONSTRAINT [FK_GuestMealPlanControl_Property] FOREIGN KEY ([PropertyId]) REFERENCES [Property]([PropertyId]),
	CONSTRAINT [FK_GuestMealPlanControl_Tenant] FOREIGN KEY ([TenantId]) REFERENCES [dbo].[Tenant] ([TenantId])
);
GO

CREATE INDEX 
	x_GuestMealPlanControl_GuestReservationItemId ON dbo.GuestMealPlanControl( [GuestReservationItemId] )
GO

CREATE INDEX 
	x_GuestMealPlanControl_Coffee ON dbo.GuestMealPlanControl( [Coffee] )
GO

CREATE INDEX 
	x_GuestMealPlanControl_Lunch ON dbo.GuestMealPlanControl( [Lunch] )
GO

CREATE INDEX 
	x_GuestMealPlanControl_Dinner ON dbo.GuestMealPlanControl( [Dinner] )
GO

CREATE INDEX 
	x_GuestMealPlanControl_PropertyId ON dbo.GuestMealPlanControl( [PropertyId] )
GO

CREATE INDEX 
	x_GuestMealPlanControl_TenantId ON dbo.GuestMealPlanControl( [TenantId] )
GO

CREATE INDEX 
	x_GuestMealPlanControl_GuestMealPlanControlDate ON dbo.GuestMealPlanControl( [GuestMealPlanControlDate] )
GO