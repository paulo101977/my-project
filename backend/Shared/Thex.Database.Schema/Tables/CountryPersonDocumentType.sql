﻿CREATE TABLE [dbo].[CountryPersonDocumentType]
(
	[CountryPersonDocumentTypeId]	INTEGER				NOT NULL,
	[PersonType]					CHAR(1)				NOT NULL,
	[DocumentTypeId]				INTEGER				NOT NULL,
	[CountrySubdivisionId]			INTEGER				NOT NULL,

	CONSTRAINT [PK_CountryPersonDocumentType] PRIMARY KEY ([CountryPersonDocumentTypeId]),
	CONSTRAINT [UK_CountryPersonDocumentType] UNIQUE ([DocumentTypeId],[PersonType],[CountrySubdivisionId]),
	CONSTRAINT [FK_CountryPersonDocumentType_DocumentType] FOREIGN KEY ( [DocumentTypeId] ) REFERENCES [dbo].[DocumentType] ( [DocumentTypeId] )
	
)
GO

CREATE INDEX 
	x_CountryPersonDocumentType_DocumentTypeId ON dbo.CountryPersonDocumentType( [DocumentTypeId] )
GO

CREATE INDEX 
	x_CountryPersonDocumentType_CountrySubdivisionId ON dbo.CountryPersonDocumentType( [CountrySubdivisionId] )
GO
