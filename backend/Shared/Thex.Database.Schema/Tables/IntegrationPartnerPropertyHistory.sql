﻿CREATE TABLE [dbo].[IntegrationPartnerPropertyHistory]
(
	[IntegrationPartnerPropertyHistoryId] UNIQUEIDENTIFIER NOT NULL, 
    [ReservationItemId] BIGINT NOT NULL, 
    [IntegrationPartnerPropertyId] UNIQUEIDENTIFIER NOT NULL, 
    [Response] NVARCHAR(MAX) NOT NULL, 
    [IntegrationOperationType] INT NOT NULL,

	[IsDeleted]							BIT					NOT NULL	DEFAULT 0,
	[CreationTime]						DATETIME			NOT NULL	DEFAULT GETUTCDATE(),
	[CreatorUserId]						UNIQUEIDENTIFIER	NULL,
	[LastModificationTime]				DATETIME			NULL		DEFAULT GETUTCDATE(),
	[LastModifierUserId]				UNIQUEIDENTIFIER	NULL,
	[DeletionTime]						DATETIME			NULL,
	[DeleterUserId]						UNIQUEIDENTIFIER	NULL,

	CONSTRAINT [PK_IntegrationPartnerPropertyHistory] PRIMARY KEY ( [IntegrationPartnerPropertyHistoryId] ),
	CONSTRAINT [FK_IntegrationPartnerPropertyHistory_ReservationItem] FOREIGN KEY ( [ReservationItemId] ) REFERENCES [dbo].[ReservationItem] ( [ReservationItemId] ),
	CONSTRAINT [FK_IntegrationPartnerPropertyHistory_InterationPartnerProperty] FOREIGN KEY ( [IntegrationPartnerPropertyId] ) REFERENCES [dbo].[IntegrationPartnerProperty] ( [IntegrationPartnerPropertyId] ),
)
GO

CREATE INDEX 
	x_IntegrationPartnerPropertyHistory_ReservationItemId ON [ReservationItem] ( [ReservationItemId] )
GO

CREATE INDEX 
	x_IntegrationPartnerPropertyHistory_IntegrationPartnerPropertyId ON [IntegrationPartnerProperty] ( [IntegrationPartnerPropertyId] )
GO
