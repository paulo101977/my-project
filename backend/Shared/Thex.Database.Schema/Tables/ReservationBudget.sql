﻿CREATE TABLE [dbo].[ReservationBudget]
(
	[ReservationBudgetId]		BIGINT				NOT NULL	IDENTITY(1,1),
	[ReservationBudgetUid]		UNIQUEIDENTIFIER	NOT NULL,
	[ReservationItemId]			BIGINT				NOT NULL,
	[BudgetDay]					DATE				NOT NULL,
	[BaseRate]					NUMERIC(18, 4)		NOT NULL,
	[ChildRate]					NUMERIC(18, 4)		NOT NULL,
	[MealPlanTypeBaseRate]		NUMERIC(18, 4)		NULL,
	[ChildMealPlanTypeRate]		NUMERIC(18, 4)		NULL,
	[MealPlanTypeId]			INT					NOT NULL,
	[RateVariation]				NUMERIC(18, 4)		NOT NULL,
	[AgreementRate]				NUMERIC(18, 4)		NOT NULL,
	[ChildAgreementRate]		NUMERIC(18, 4)		NULL,
	[MealPlanTypeAgreementRate]			NUMERIC(18, 4)		NULL,
	[ChildMealPlanTypeAgreementRate]	NUMERIC(18, 4)		NULL,
	[ManualRate]				NUMERIC(18, 4)		NOT NULL,
	[SeparatedRate]				NUMERIC(18, 4)		NULL DEFAULT 0,
	[Discount]					NUMERIC(18, 4)		NOT NULL,
	[CurrencyId]				UNIQUEIDENTIFIER	NOT NULL,
	[CurrencySymbol]			VARCHAR(10)			NULL,
	[TenantId]					UNIQUEIDENTIFIER	NOT NULL,
	[BillingAccountItemId]		UNIQUEIDENTIFIER	NULL, 

	[IsDeleted]					BIT					NOT NULL	DEFAULT 0,
	[CreationTime]				DATETIME			NOT NULL	DEFAULT GETUTCDATE(),
	[CreatorUserId]				UNIQUEIDENTIFIER	NULL,
	[LastModificationTime]		DATETIME			NULL		DEFAULT GETUTCDATE(),
	[LastModifierUserId]		UNIQUEIDENTIFIER	NULL,
	[DeletionTime]				DATETIME			NULL,
	[DeleterUserId]				UNIQUEIDENTIFIER	NULL,
	[PropertyBaseRateHeaderHistoryId] UNIQUEIDENTIFIER	NULL,

    CONSTRAINT [PK_ReservationBudget] PRIMARY KEY ([ReservationBudgetId]),
	CONSTRAINT [AK_ReservationBudget_Uid] UNIQUE ([ReservationBudgetUid]),
	CONSTRAINT [FK_ReservationBudget_ReservationItem] FOREIGN KEY ([ReservationItemId]) REFERENCES [dbo].[ReservationItem] ([ReservationItemId]),
	CONSTRAINT [FK_ReservationBudget_Currency] FOREIGN KEY ([CurrencyId]) REFERENCES [dbo].[Currency] ([CurrencyId]),
	CONSTRAINT [FK_ReservationBudget_Tenant] FOREIGN KEY ([TenantId]) REFERENCES [dbo].[Tenant] ([TenantId]),
	CONSTRAINT [FK_ReservationBudget_PropertyBaseRateHeaderHistory] FOREIGN KEY ([PropertyBaseRateHeaderHistoryId]) REFERENCES [dbo].[PropertyBaseRateHeaderHistory] ([PropertyBaseRateHeaderHistoryId]),
	CONSTRAINT [FK_ReservationBudget_BillingAccountItem] FOREIGN KEY ( [BillingAccountItemId] ) REFERENCES [dbo].[BillingAccountItem] ( [BillingAccountItemId] )
);
GO

CREATE INDEX 
	x_ReservationBudget_ReservationItemId ON dbo.ReservationBudget( [ReservationItemId] )
GO

CREATE INDEX 
	x_ReservationBudget_CurrencyId ON dbo.ReservationBudget( [CurrencyId] )
GO

CREATE INDEX 
	x_ReservationBudget_TenantId ON [dbo].[ReservationBudget]( [TenantId] )
GO

CREATE INDEX 
	x_ReservationBudget_BillingAccountItemId ON [dbo].[ReservationBudget]( [BillingAccountItemId] )
GO