﻿CREATE TABLE [dbo].[BillingAccountItemInvoiceHistory]
(
	[BillingAccountItemInvoiceHistoryId]		UNIQUEIDENTIFIER		NOT NULL,
	[BillingInvoiceId]							UNIQUEIDENTIFIER		NOT NULL,
	[BillingAccountItemId]						UNIQUEIDENTIFIER		NOT NULL,
	[TenantId]									UNIQUEIDENTIFIER		NOT NULL,
	[IsDeleted]									BIT						NOT NULL	DEFAULT 0,
	[CreationTime]								DATETIME				NOT NULL	DEFAULT GETUTCDATE(),
	[CreatorUserId]								UNIQUEIDENTIFIER		NULL,
	[LastModificationTime]						DATETIME				NULL		DEFAULT GETUTCDATE(),
	[LastModifierUserId]						UNIQUEIDENTIFIER		NULL,
	[DeletionTime]								DATETIME				NULL,
	[DeleterUserId]								UNIQUEIDENTIFIER		NULL,

    CONSTRAINT [PK_BillingAccountItemInvoiceHistory] PRIMARY KEY ( [BillingAccountItemInvoiceHistoryId] ),
	CONSTRAINT [FK_BillingAccountItemInvoiceHistory_BillingAccountItem] FOREIGN KEY ( [BillingAccountItemId] ) REFERENCES [dbo].[BillingAccountItem] ( [BillingAccountItemId] ),
	CONSTRAINT [FK_BillingAccountItemInvoiceHistory_BillingInvoice] FOREIGN KEY ( [BillingInvoiceId] ) REFERENCES [dbo].[BillingInvoice] ( [BillingInvoiceId] ),
	CONSTRAINT [FK_BillingAccountItemInvoiceHistory_Tenant] FOREIGN KEY ([TenantId]) REFERENCES [dbo].[Tenant] ([TenantId])
)
GO

CREATE INDEX 
	x_BillingAccountItemInvoiceHistory_BillingAccountItemInvoiceHistoryId ON [dbo].[BillingAccountItemInvoiceHistory] ( [BillingAccountItemInvoiceHistoryId] ) 
GO 

CREATE INDEX 
	x_BillingAccountItemInvoiceHistory_BillingInvoiceId ON [dbo].[BillingAccountItemInvoiceHistory] ( [BillingInvoiceId] ) 
GO 

CREATE INDEX 
	x_BillingAccountItemInvoiceHistory_BillingAccountItemId ON [dbo].[BillingAccountItemInvoiceHistory] ( [BillingAccountItemId] ) 
GO 

CREATE INDEX 
	x_BillingAccountItemInvoiceHistory_TenantId ON [dbo].[BillingAccountItemInvoiceHistory]( [TenantId] )
GO
