﻿CREATE TABLE [dbo].[Person] (
	[PersonId]					UNIQUEIDENTIFIER	NOT NULL,
	[PersonType]				CHAR (1)			NOT NULL,
	[FirstName]					VARCHAR (200)		NOT NULL,
	[LastName]					VARCHAR (400)		NULL,
	[FullName]					VARCHAR (1000)		NULL,
	[NickName]					VARCHAR (1000)		NULL,
	[DateOfBirth]				DATE				NULL,
	[CountrySubdivisionId]		INTEGER				NULL,
	[PhotoUrl]					VARCHAR(200)		NULL,
	[ReceiveOffers]				BIT		DEFAULT 0	NULL,
	[SharePersonData]			BIT		DEFAULT 0	NULL,
	[AllowContact]				BIT		DEFAULT 0	NULL,

	CONSTRAINT [PK_Person] PRIMARY KEY ([PersonId]),
	CONSTRAINT [CK_Person_PersonType] CHECK ([PersonType] IN ('L', 'N', 'C', 'H'))
);
GO

CREATE INDEX 
	x_Person_PersonType ON dbo.Person( [PersonType] )
GO