﻿CREATE TABLE [dbo].[UserInvitation]
(
	[UserInvitationId]				UNIQUEIDENTIFIER	NOT NULL,
	[ChainId]						INT					NULL,
	[PropertyId]					INT					NULL,
	[BrandId]						INT					NULL,
	[Email]							VARCHAR(600)		NOT NULL,
	[Name]							VARCHAR(300)		NOT NULL,
	[InvitationLink]				VARCHAR(4000)		NOT NULL,
	[InvitationDate]				DATETIME			NOT NULL,
	[InvitationAcceptanceDate]		DATETIME			NULL,
	[IsActive]						BIT					NOT NULL,
	[TenantId]						UNIQUEIDENTIFIER	NOT NULL,
	[UserId]						UNIQUEIDENTIFIER	NULL,

	[IsDeleted]						BIT					NOT NULL	DEFAULT 0,
	[CreationTime]					DATETIME			NOT NULL	DEFAULT GETUTCDATE(),
	[CreatorUserId]					UNIQUEIDENTIFIER	NULL,
	[LastModificationTime]			DATETIME			NULL		DEFAULT GETUTCDATE(),
	[LastModifierUserId]			UNIQUEIDENTIFIER	NULL,
	[DeletionTime]					DATETIME			NULL,
	[DeleterUserId]					UNIQUEIDENTIFIER	NULL,

	CONSTRAINT [PK_UserInvitation] PRIMARY KEY ( [UserInvitationId] ),
	CONSTRAINT [FK_UserInvitation_Property] FOREIGN KEY ([PropertyId]) REFERENCES [dbo].[Property] ([PropertyId]),
	CONSTRAINT [FK_UserInvitation_Chain] FOREIGN KEY ([ChainId]) REFERENCES [dbo].[Chain] ([ChainId]),
	CONSTRAINT [FK_UserInvitation_Brand] FOREIGN KEY ([BrandId]) REFERENCES [dbo].[Brand] ([BrandId]),
	CONSTRAINT [FK_UserInvitation_Tenant] FOREIGN KEY ([TenantId]) REFERENCES [dbo].[Tenant] ([TenantId]),
	CONSTRAINT [FK_UserInvitation_Users] FOREIGN KEY ([UserId]) REFERENCES [dbo].[Users] ([Id])
);
GO

CREATE INDEX 
	x_UserInvitation_PropertyId ON dbo.Property( [PropertyId] )
GO

CREATE INDEX 
	x_UserInvitation_ChainId ON dbo.Chain( [ChainId] )
GO

CREATE INDEX 
	x_UserInvitation_TenantId ON [dbo].[UserInvitation]( [TenantId] )
GO

CREATE INDEX 
	x_UserInvitation_BrandId ON dbo.Brand( [BrandId] )
GO