﻿CREATE TABLE [dbo].[ApplicationParameter]
(
	[ApplicationParameterId]		INT						NOT NULL,
	[ApplicationModuleId]			INT						NOT NULL,
	[FeatureGroupId]				INT						NOT NULL,
	[ParameterName]					VARCHAR(100)			NOT NULL,
	[ParameterDescription]			VARCHAR(500)			NULL,
	[ParameterDefaultValue]			VARCHAR(MAX)			NULL,
	[ParameterTypeId]				INT						NOT NULL,
	[ParameterComponentType]		VARCHAR(100)			NULL,
	[ParameterMinValue]				VARCHAR(100)			NULL,
	[ParameterMaxValue]				VARCHAR(100)			NULL,
	[ParameterPossibleValues]		VARCHAR(MAX)			NULL,
	[IsNullable]					BIT						NOT NULL	DEFAULT 0,

	[IsDeleted]					BIT					NOT NULL	DEFAULT 0,
	[CreationTime]				DATETIME			NOT NULL	DEFAULT GETUTCDATE(),
	[CreatorUserId]				UNIQUEIDENTIFIER	NULL,
	[LastModificationTime]		DATETIME			NULL		DEFAULT GETUTCDATE(),
	[LastModifierUserId]		UNIQUEIDENTIFIER	NULL,
	[DeletionTime]				DATETIME			NULL,
	[DeleterUserId]				UNIQUEIDENTIFIER	NULL,

	
	CONSTRAINT [PK_ApplicationParameter] PRIMARY KEY ([ApplicationParameterId]),
	CONSTRAINT [UK_ApplicationParameter] UNIQUE ([ApplicationModuleId], [FeatureGroupId], [ParameterName]),

	CONSTRAINT [FK_ApplicationParameter_ParameterType] FOREIGN KEY ([ParameterTypeId]) REFERENCES [dbo].[ParameterType] ([ParameterTypeId]),
	CONSTRAINT [FK_ApplicationParameter_ApplicationModuleId] FOREIGN KEY ([ApplicationModuleId]) REFERENCES [dbo].[ApplicationModule] ([ApplicationModuleId]),
	CONSTRAINT [FK_ApplicationParameter_FeatureGroupId] FOREIGN KEY ([FeatureGroupId]) REFERENCES [dbo].[FeatureGroup] ([FeatureGroupId])
)
GO

CREATE INDEX 
	x_ApplicationParameter_ParameterTypeId ON [dbo].[ApplicationParameter] ( [ParameterTypeId] )
GO

CREATE INDEX 
	x_ApplicationParameter_ApplicationModuleId ON [dbo].[ApplicationParameter] ( [ApplicationModuleId] )
GO

CREATE INDEX 
	x_ApplicationParameter_FeatureGroupId ON [dbo].[ApplicationParameter] ( [FeatureGroupId] )
GO

