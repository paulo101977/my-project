﻿CREATE TABLE [dbo].[PropertyPremiseHeader]
(
	[PropertyPremiseHeaderId]		UNIQUEIDENTIFIER	NOT NULL,
	[PropertyId]					INT					NOT NULL,
	[InitialDate]					DATETIME			NOT NULL,
	[FinalDate]						DATETIME			NOT NULL,
	[CurrencyId]					UNIQUEIDENTIFIER	NOT NULL,	
	[TenantId]						UNIQUEIDENTIFIER	NOT NULL,
	[IsActive]						BIT					NOT NULL	DEFAULT 0,
	[IsDeleted]						BIT					NOT NULL	DEFAULT 0,
	[CreationTime]					DATETIME			NOT NULL	DEFAULT GETUTCDATE(),
	[CreatorUserId]					UNIQUEIDENTIFIER	NULL,
	[LastModificationTime]			DATETIME			NULL		DEFAULT GETUTCDATE(),
	[LastModifierUserId]			UNIQUEIDENTIFIER	NULL,
	[DeletionTime]					DATETIME			NULL,
	[DeleterUserId]					UNIQUEIDENTIFIER	NULL,
	[Name] VARCHAR(200) NOT NULL, 
    [PaxReference] INT NOT NULL, 
    [RoomTypeReferenceId] int NOT NULL, 
    CONSTRAINT [PK_PropertyPremiseHeader] PRIMARY KEY ([PropertyPremiseHeaderId]),
    CONSTRAINT [UK_PropertyPremiseHeader_1] UNIQUE ([PropertyId], [Name], [IsActive], [IsDeleted], [DeletionTime]),
	CONSTRAINT [FK_PropertyPremiseHeader_Property] FOREIGN KEY ([PropertyId]) REFERENCES [dbo].[Property] ([PropertyId]),
	CONSTRAINT [FK_PropertyPremiseHeader_Currency] FOREIGN KEY ([CurrencyId]) REFERENCES [dbo].[Currency] ([CurrencyId]),
	CONSTRAINT [FK_PropertyPremiseHeader_Tenant] FOREIGN KEY ([TenantId]) REFERENCES [dbo].[Tenant] ([TenantId]),
	CONSTRAINT [FK_PropertyPremiseHeader_RoomType] FOREIGN KEY ([RoomTypeReferenceId]) REFERENCES [dbo].[RoomType] ([RoomTypeId])
)
GO

CREATE INDEX
	x_PropertyPremiseHeader_PropertyId ON dbo.[PropertyPremiseHeader] ( [PropertyId] )
GO

CREATE INDEX 
	x_PropertyPremiseHeader_CurrencyId ON dbo.[PropertyPremiseHeader] ( [CurrencyId] )
GO

CREATE INDEX 
	x_PropertyPremiseHeader_TenantId ON [dbo].[PropertyPremiseHeader]( [TenantId] )
GO

CREATE INDEX 
	x_PropertyPremiseHeader_RoomTypeReferenceId ON [dbo].[PropertyPremiseHeader]( [RoomTypeReferenceId] )
GO