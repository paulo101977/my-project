﻿CREATE TABLE [dbo].[ReservationConfirmation]
(
	[ReservationConfirmationId]			UNIQUEIDENTIFIER					NOT NULL,
	[ReservationId]						BIGINT								NOT NULL,
	[PaymentTypeId]						INT									NOT NULL,
	[PlasticId]							BIGINT								NULL,
	[BillingClientId]					UNIQUEIDENTIFIER					NULL,					
	[EnsuresNoShow]						BIT									NOT NULL,
	[Value]								NUMERIC(18, 4)						NULL,
	[Deadline]							DATE								NULL,
	[LaunchDaily]						BIT									NULL,
	[TenantId]					UNIQUEIDENTIFIER	NOT NULL,

	[IsDeleted]					BIT					NOT NULL	DEFAULT 0,
	[CreationTime]				DATETIME			NOT NULL	DEFAULT GETUTCDATE(),
	[CreatorUserId]				UNIQUEIDENTIFIER	NULL,
	[LastModificationTime]		DATETIME			NULL		DEFAULT GETUTCDATE(),
	[LastModifierUserId]		UNIQUEIDENTIFIER	NULL,
	[DeletionTime]				DATETIME			NULL,
	[DeleterUserId]				UNIQUEIDENTIFIER	NULL,

	
	CONSTRAINT [PK_ReservationConfirmation] PRIMARY KEY ([ReservationConfirmationId]), 
	CONSTRAINT [FK_ReservationConfirmation_Reservation] FOREIGN KEY ([ReservationId]) REFERENCES [Reservation]([ReservationId]),
	CONSTRAINT [FK_ReservationConfirmation_PaymentType] FOREIGN KEY ([PaymentTypeId]) REFERENCES [PaymentType]([PaymentTypeId]),
	CONSTRAINT [FK_ReservationConfirmation_CompanyClient] FOREIGN KEY ([BillingClientId]) REFERENCES [CompanyClient]([CompanyClientId]),
	CONSTRAINT [FK_ReservationConfirmation_Plastic] FOREIGN KEY ([PlasticId]) REFERENCES [Plastic]([PlasticId]),
	CONSTRAINT [FK_ReservationConfirmation_Tenant] FOREIGN KEY ([TenantId]) REFERENCES [dbo].[Tenant] ([TenantId])
);
GO

CREATE INDEX 
	x_ReservationConfirmation_ReservationId ON dbo.ReservationConfirmation( [ReservationId] )
GO

CREATE INDEX 
	x_ReservationConfirmation_PaymentTypeId ON dbo.ReservationConfirmation( [PaymentTypeId] )
GO

CREATE INDEX 
	x_ReservationConfirmation_BillingClientId ON dbo.ReservationConfirmation( [BillingClientId] )
GO

CREATE INDEX 
	x_ReservationConfirmation_PlasticId ON dbo.ReservationConfirmation( [PlasticId] )
GO

CREATE INDEX 
	x_ReservationConfirmation_TenantId ON [dbo].[ReservationConfirmation]( [TenantId] )
GO