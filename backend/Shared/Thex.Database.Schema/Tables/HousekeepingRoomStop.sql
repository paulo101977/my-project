﻿CREATE TABLE [dbo].[HousekeepingRoomStop]
(
	[HousekeepingRoomStopId]			UNIQUEIDENTIFIER	NOT NULL,
	[PropertyId]						INT					NOT NULL,
	[HousekeepingRoomId]				UNIQUEIDENTIFIER	NOT NULL,
	[ReasonId]							INT					NOT NULL,
	[Reason]							VARCHAR(4000)		NULL,
	[IsDeleted]							BIT					NOT NULL	DEFAULT 0,
	[CreationTime]						DATETIME			NOT NULL	DEFAULT GETUTCDATE(),
	[CreatorUserId]						UNIQUEIDENTIFIER	NULL,
	[LastModificationTime]				DATETIME			NULL		DEFAULT GETUTCDATE(),
	[LastModifierUserId]				UNIQUEIDENTIFIER	NULL,
	[DeletionTime]						DATETIME			NULL,
	[DeleterUserId]						UNIQUEIDENTIFIER	NULL,
	

	CONSTRAINT [PK_HousekeepingRoomStop] PRIMARY KEY ( [HousekeepingRoomStopId] ),
	CONSTRAINT [FK_HousekeepingRoomStop_HousekeepingRoom] FOREIGN KEY ( [HousekeepingRoomId] ) REFERENCES [dbo].[HousekeepingRoom] ( [HousekeepingRoomId] ),
	CONSTRAINT [FK_HousekeepingRoomStop_Property] FOREIGN KEY ( [PropertyId] ) REFERENCES [dbo].[Property] ( [PropertyId] ),
	CONSTRAINT [FK_HousekeepingRoomStop_Reason] FOREIGN KEY ( [ReasonId] ) REFERENCES [dbo].[Reason] ( [ReasonId] ),
)
GO

CREATE INDEX 
	x_HousekeepingRoomStop_HousekeepingRoomId ON [HousekeepingRoomStop] ( [HousekeepingRoomId] )
GO

CREATE INDEX 
	x_HousekeepingRoomStop_PropertyId ON [HousekeepingRoomStop] ( [PropertyId] )
GO

CREATE INDEX 
	x_HousekeepingRoomStop_ReasonId ON [HousekeepingRoomStop] ( [ReasonId] )
GO