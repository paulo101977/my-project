﻿CREATE TABLE [dbo].[BillingInvoiceProperty]
(
	[BillingInvoicePropertyId]		UNIQUEIDENTIFIER	NOT NULL,
	[BillingInvoicePropertySeries]	VARCHAR(5)			NOT NULL,
	[BillingInvoiceModelId]			INT	NOT NULL,
	[LastNumber]						VARCHAR(100)			NOT NULL,
	[IsIntegrated]						BIT						NOT NULL		DEFAULT 0,
	[PropertyId]						INT						NOT NULL, 
    [IsActive]							BIT						NOT NULL DEFAULT 1, 
	[EmailAlert]							VARCHAR(150)			NOT NULL,
	[Description]						VARCHAR(5000)		NULL,
	[BillingInvoicePropertyReferenceId] UNIQUEIDENTIFIER NULL, 
	[AllowsCancel]						BIT						NULL		DEFAULT 0,
	[DaysForCancel]				INT						NULL,
	[TenantId]									UNIQUEIDENTIFIER		NOT NULL,

	[IsDeleted]									BIT						NOT NULL	DEFAULT 0,
	[CreationTime]								DATETIME				NOT NULL	DEFAULT GETUTCDATE(),
	[CreatorUserId]								UNIQUEIDENTIFIER		NULL,
	[LastModificationTime]						DATETIME				NULL		DEFAULT GETUTCDATE(),
	[LastModifierUserId]						UNIQUEIDENTIFIER		NULL,
	[DeletionTime]								DATETIME				NULL,
	[DeleterUserId]								UNIQUEIDENTIFIER		NULL,
    
    [IntegrationId] NVARCHAR(50) NULL, 
    CONSTRAINT PK_BillingInvoiceProperty PRIMARY KEY ([BillingInvoicePropertyId]),
	CONSTRAINT FK_BillingInvoiceModelId  FOREIGN KEY ([BillingInvoiceModelId]) REFERENCES dbo.[BillingInvoiceModel] ([BillingInvoiceModelId]),
	CONSTRAINT [FK_BillingInvoiceProperty_Property] FOREIGN KEY ([PropertyId]) REFERENCES [Property]([PropertyId]),
	CONSTRAINT [UK_BillingInvoiceProperty] UNIQUE ( [BillingInvoicePropertySeries], [BillingInvoiceModelId], [PropertyId]),
	CONSTRAINT [FK_BillingInvoiceProperty_Tenant] FOREIGN KEY ([TenantId]) REFERENCES [dbo].[Tenant] ([TenantId]),
	CONSTRAINT [FK_BillingInvoiceProperty_BillingInvoiceProperty] FOREIGN KEY ([BillingInvoicePropertyReferenceId]) REFERENCES [dbo].[BillingInvoiceProperty] ([BillingInvoicePropertyId])
)
GO

CREATE INDEX 
	x_BillingInvoiceProperty_BillingInvoiceModelId ON [dbo].[BillingInvoiceProperty] ( [BillingInvoiceModelId] ) 
GO

CREATE INDEX 
	x_BillingInvoiceProperty_PropertyId ON [dbo].[BillingInvoiceProperty] ( [PropertyId] ) 
GO

CREATE INDEX 
	x_BillingInvoiceProperty_TenantId ON [dbo].[BillingInvoiceProperty]( [TenantId] )
GO