﻿CREATE TABLE [dbo].[IdentityTranslation]
(
	[IdentityTranslationId]			UNIQUEIDENTIFIER	NOT NULL,
	[LanguageIsoCode]				VARCHAR(5) 			NOT NULL,
	[Name]							VARCHAR(500) 		NULL,
	[Description]					VARCHAR(5000) 		NULL,
	[IdentityOwnerId]				UNIQUEIDENTIFIER 	NOT NULL,

	CONSTRAINT [PK_IdentityTranslation] PRIMARY KEY ([IdentityTranslationId])
);
GO
