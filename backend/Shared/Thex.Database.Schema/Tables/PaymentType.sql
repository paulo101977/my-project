﻿CREATE TABLE [dbo].[PaymentType]
(
	[PaymentTypeId]			INT							NOT NULL,
	[PaymentTypeName]		VARCHAR(50)					NOT NULL,

	CONSTRAINT [PK_PaymentType] PRIMARY KEY ([PaymentTypeId])
);
GO
