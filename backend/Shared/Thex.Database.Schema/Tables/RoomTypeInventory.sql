﻿CREATE TABLE [dbo].[RoomTypeInventory]
(
	[RoomTypeInventoryId]			UNIQUEIDENTIFIER	NOT NULL, 
    [PropertyId]					INT					NOT NULL, 
    [RoomTypeId]					INT					NOT NULL, 
    [Total]							INT					NOT NULL, 
    [Balance]						INT					NOT NULL, 
    [BlockedQuantity]				INT					NOT NULL, 
    [Date]							DATETIME			NOT NULL, 
	[TenantId]						UNIQUEIDENTIFIER	NOT NULL,

	[IsDeleted]						BIT					NOT NULL	DEFAULT 0,
	[CreationTime]					DATETIME			NOT NULL	DEFAULT GETUTCDATE(),
	[CreatorUserId]					UNIQUEIDENTIFIER	NULL,
	[LastModificationTime]			DATETIME			NULL		DEFAULT GETUTCDATE(),
	[LastModifierUserId]			UNIQUEIDENTIFIER	NULL,
	[DeletionTime]					DATETIME			NULL,
	[DeleterUserId]					UNIQUEIDENTIFIER	NULL,

	CONSTRAINT [PK_RoomTypeInventory] PRIMARY KEY ([RoomTypeInventoryId]),
	CONSTRAINT [UK_RoomTypeInventory] UNIQUE ( [RoomTypeId], [Date]),
	CONSTRAINT [FK_RoomTypeInventory_Property] FOREIGN KEY ([PropertyId]) REFERENCES [dbo].[Property] ([PropertyId]),
	CONSTRAINT [FK_RoomTypeInventory_RoomType] FOREIGN KEY ([RoomTypeId]) REFERENCES [dbo].[RoomType] ([RoomTypeId]),
	CONSTRAINT [FK_RoomTypeInventory_Tenant] FOREIGN KEY ([TenantId]) REFERENCES [dbo].[Tenant] ([TenantId])
)
GO

CREATE INDEX 
	x_RoomTypeInventory_PropertyId ON [dbo].[RoomTypeInventory] ( [PropertyId] )
GO

CREATE INDEX 
	x_RoomTypeInventory_RoomTypeId ON [dbo].[RoomTypeInventory] ( [RoomTypeId] )
GO

CREATE INDEX 
	x_RoomTypeInventory_TenantId ON [dbo].[RoomTypeInventory] ( [TenantId] )
GO