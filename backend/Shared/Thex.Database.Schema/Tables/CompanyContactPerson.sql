﻿CREATE TABLE [dbo].[CompanyContactPerson]
(
	[PersonId]			UNIQUEIDENTIFIER NOT NULL,
	[CompanyId]			INTEGER			 NOT NULL,
	[OccupationId]		INTEGER			 NULL,

	CONSTRAINT [PK_CompanyContractPerson] PRIMARY KEY ([PersonId], [CompanyId]),
	CONSTRAINT [FK_CompanyContractPerson_Person] FOREIGN KEY ([PersonId]) REFERENCES [dbo].[Person] ([PersonId]),
	CONSTRAINT [FK_CompanyContractPerson_Company] FOREIGN KEY ([CompanyId]) REFERENCES [dbo].[Company] ([CompanyId]),
	CONSTRAINT [FK_CompanyContractPerson_Occupation] FOREIGN KEY ([OccupationId]) REFERENCES [dbo].[Occupation] ([OccupationId])
)
GO

CREATE INDEX 
	x_CompanyContactPerson_Occupation ON dbo.Occupation( [OccupationId] )
GO