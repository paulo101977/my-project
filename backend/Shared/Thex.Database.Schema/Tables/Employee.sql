﻿CREATE TABLE [dbo].[Employee]
(
	[EmployeeId]	INT IDENTITY		NOT NULL,
	[PersonId]		UNIQUEIDENTIFIER	NOT NULL,
	[OccupationId]	INT					NOT NULL,

	CONSTRAINT [PK_Employee] PRIMARY KEY ([EmployeeId]),
	CONSTRAINT [FK_Employee_Occupation] FOREIGN KEY ([OccupationId]) REFERENCES [dbo].[Occupation] ([OccupationId]),
	CONSTRAINT [FK_Employee_Person]	FOREIGN KEY ([PersonId]) REFERENCES [dbo].[Person] ([PersonId])
);
GO

CREATE INDEX 
	x_Employee_OccupationId ON dbo.Employee( [OccupationId] )
GO

CREATE INDEX 
	x_Employee_PersonId ON dbo.Employee( [PersonId] )
GO