﻿CREATE TABLE [dbo].[StatusCategory]
(
	[StatusCategoryId]	INT				NOT NULL,
	[Name]				VARCHAR (40)	NOT NULL,

	CONSTRAINT [PK_StatusCategory] PRIMARY KEY ([StatusCategoryId])
)
