﻿CREATE TABLE [dbo].[RateType]
(
	[RateTypeId]			INT	NOT NULL,
	[RateTypeName]			VARCHAR(100)	NOT NULL,

	CONSTRAINT [PK_RateType] PRIMARY KEY ( [RateTypeId] ),
)
GO