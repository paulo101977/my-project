﻿CREATE TABLE [dbo].[BillingAccountItemTransfer]
(
	[BillingAccountItemTransferId]				UNIQUEIDENTIFIER		NOT NULL, 
	[BillingAccountItemId]						UNIQUEIDENTIFIER		NOT NULL, 
	[BillingAccountIdSource]					UNIQUEIDENTIFIER		NOT NULL, 
	[BillingAccountIdDestination]				UNIQUEIDENTIFIER		NOT NULL, 
	[TransferDate]								DATETIME				NOT NULL,
	[TenantId]									UNIQUEIDENTIFIER		NOT NULL,

	[IsDeleted]									BIT					NOT NULL	DEFAULT 0,
	[CreationTime]								DATETIME			NOT NULL	DEFAULT GETUTCDATE(),
	[CreatorUserId]								UNIQUEIDENTIFIER	NULL,
	[LastModificationTime]						DATETIME			NULL		DEFAULT GETUTCDATE(),
	[LastModifierUserId]						UNIQUEIDENTIFIER	NULL,
	[DeletionTime]								DATETIME			NULL,
	[DeleterUserId]								UNIQUEIDENTIFIER	NULL,

	CONSTRAINT [PK_BillingAccountItemTransfer] PRIMARY KEY ( [BillingAccountItemTransferId] ),
	CONSTRAINT [FK_BillingAccountItemTransfer_BillingAccount] FOREIGN KEY ( [BillingAccountIdSource] ) REFERENCES [dbo].[BillingAccount] ( [BillingAccountId] ),
	CONSTRAINT [FK_BillingAccountItemTransfer_BillingAccountItem] FOREIGN KEY ( [BillingAccountItemId] ) REFERENCES [dbo].[BillingAccountItem] ( [BillingAccountItemId] ),
	CONSTRAINT [FK_BillingAccountItemTransfer_BillingAccount_1] FOREIGN KEY ( [BillingAccountIdDestination] ) REFERENCES [dbo].[BillingAccount] ( [BillingAccountId] ),
	CONSTRAINT [FK_BillingAccountItemTransfer_Tenant] FOREIGN KEY ([TenantId]) REFERENCES [dbo].[Tenant] ([TenantId])
)
GO

CREATE INDEX 
	x_BillingAccountItemTransfer_BillingAccountIdSource ON BillingAccountItemTransfer ( [BillingAccountIdSource] ) 
GO

CREATE INDEX 
	x_BillingAccountItemTransfer_BillingAccountItemId ON BillingAccountItemTransfer ( [BillingAccountItemId] ) 
GO

CREATE INDEX 
	x_BillingAccountItemTransfer_BillingAccountIdDestination ON BillingAccountItemTransfer ( [BillingAccountIdDestination] ) 
GO

CREATE INDEX 
	x_BillingAccountItemTransfer_TenantId ON [dbo].[BillingAccountItemTransfer]( [TenantId] )
GO