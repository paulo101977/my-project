﻿CREATE TABLE [dbo].[IdentityRolePermission](
	[IdentityRolePermissionId]	UNIQUEIDENTIFIER	NOT NULL,
	[IdentityPermissionId]		UNIQUEIDENTIFIER	NOT NULL,
	[RoleId]					UNIQUEIDENTIFIER	NOT NULL,
	[IsActive]					BIT					NOT NULL	DEFAULT 1,
	[IsDeleted]					BIT					NOT NULL	DEFAULT 0,
	[CreationTime]				DATETIME			NOT NULL	DEFAULT GETUTCDATE(),
	[CreatorUserId]				UNIQUEIDENTIFIER	NULL,
	[LastModificationTime]		DATETIME			NULL		DEFAULT GETUTCDATE(),
	[LastModifierUserId]		UNIQUEIDENTIFIER	NULL,
	[DeletionTime]				DATETIME			NULL,
	[DeleterUserId]				UNIQUEIDENTIFIER	NULL,
	
    CONSTRAINT [PK_IdentityRolePermission] PRIMARY KEY ([IdentityRolePermissionId]),
	CONSTRAINT [FK_IdentityRolePermission_IdentityPermission] FOREIGN KEY ([IdentityPermissionId]) REFERENCES [dbo].[IdentityPermission] ([IdentityPermissionId]),
	CONSTRAINT [FK_IdentityRolePermission_IdentityRoles] FOREIGN KEY ([RoleId]) REFERENCES [dbo].[Roles] ([Id])
)