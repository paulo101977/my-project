﻿CREATE TABLE [dbo].[DocumentType] (
	[DocumentTypeId]			INT				NOT NULL,
	[PersonType]				CHAR(1)			NOT NULL,
	[CountrySubdivisionId]		INTEGER			NULL,
	[Name]						VARCHAR(50)		NOT NULL,
	[StringFormatMask]			VARCHAR(100)	NULL,
	[RegexValidationExpression]	VARCHAR(500)	NULL,
	[IsMandatory]				BIT				NOT NULL	DEFAULT 0,

	[HigsCode] INT NULL, 
    CONSTRAINT [PK_DocumentType] PRIMARY KEY ([DocumentTypeId]),
	CONSTRAINT [CK_DocumentType_PersonType] CHECK ([PersonType] IN ('L', 'N')),
	CONSTRAINT [FK_DocumentType_CountrySubdivision] FOREIGN KEY ( [CountrySubdivisionId] ) REFERENCES [dbo].[CountrySubdivision]( [CountrySubdivisionId] )
);
GO

CREATE INDEX 
	x_DocumentType_CountrySubdivisionId ON dbo.DocumentType( [CountrySubdivisionId] )
GO