﻿CREATE TABLE [dbo].[PropertyRateStrategy]
(
	[PropertyRateStrategyId] UNIQUEIDENTIFIER NOT NULL, 
    [Name] VARCHAR(100) NOT NULL, 
    [StartDate] DATETIME NOT NULL, 
    [EndDate] DATETIME NOT NULL, 
    [IsActive] BIT NOT NULL,
	[IsDeleted]						BIT					NOT NULL	DEFAULT 0,
	[PropertyId]					INT			NOT NULL	,
	[CreatorUserId]					UNIQUEIDENTIFIER	NULL,
	[LastModificationTime]			DATETIME			NULL		DEFAULT GETUTCDATE(),
	[LastModifierUserId]			UNIQUEIDENTIFIER	NULL,
	[DeletionTime]					DATETIME			NULL,
	[DeleterUserId]					UNIQUEIDENTIFIER	NULL,
	[CreationTime] DATETIME NOT NULL DEFAULT GETUTCDATE(), 
	[TenantId]						UNIQUEIDENTIFIER		NOT NULL,

    CONSTRAINT [PK_PropertyRateStrategy] PRIMARY KEY ( [PropertyRateStrategyId] ),
	CONSTRAINT [FK_PropertyRateStrategy_Property] FOREIGN KEY ( [PropertyId] ) REFERENCES [dbo].[Property] ( [PropertyId] ),
	CONSTRAINT [FK_PropertyRateStrategy_Tenant] FOREIGN KEY ([TenantId]) REFERENCES [dbo].[Tenant] ([TenantId])
)
GO

CREATE INDEX 
	x_PropertyRateStrategy_PropertyId ON dbo.[PropertyRateStrategy] ( [PropertyId] )
GO

CREATE INDEX 
	x_PropertyRateStrategy_TenantId ON [dbo].[PropertyRateStrategy]( [TenantId] )
GO

	
