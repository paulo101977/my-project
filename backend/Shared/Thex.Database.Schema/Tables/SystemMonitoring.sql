﻿CREATE TABLE [dbo].[SystemMonitoring]
(
	[SystemId] 					UNIQUEIDENTIFIER	NOT NULL,	
	[PropertyUid]			    INT		            NOT NULL,
	[Routine]			        VARCHAR(60) 		NOT NULL,
	[IsWizard]                  BIT					NOT NULL	DEFAULT 0,
	[IsCompletedWizard]         BIT					NOT NULL	DEFAULT 0,
	[FinishedUserId]			INT          		NOT NULL,
	
	[TenantId]					UNIQUEIDENTIFIER	NOT NULL,

	[IsDeleted]					BIT					NOT NULL	DEFAULT 0,
	[CreationTime]				DATETIME			NOT NULL	DEFAULT GETUTCDATE(),
	[CreatorUserId]				UNIQUEIDENTIFIER	NULL,
	[LastModificationTime]		DATETIME			NULL		DEFAULT GETUTCDATE(),
	[LastModifierUserId]		UNIQUEIDENTIFIER	NULL,
	[DeletionTime]				DATETIME			NULL,
	[DeleterUserId]				UNIQUEIDENTIFIER	NULL,

	CONSTRAINT [PK_SystemId] PRIMARY KEY ([SystemId]),	
	CONSTRAINT [FK_SystemMonitoring_PropertyId] FOREIGN KEY ([PropertyUid]) REFERENCES [dbo].[Property]([PropertyId]),
	CONSTRAINT [FK_SystemMonitoring_Tenant] FOREIGN KEY ([TenantId]) REFERENCES [dbo].[Tenant] ([TenantId])
	
	
);
GO

CREATE INDEX 
	x_SystemMonitoring_SystemId ON dbo.SystemMonitoring( [SystemId] )
GO

CREATE INDEX 
	x_Routine ON dbo.SystemMonitoring( [Routine] )
GO

CREATE INDEX 
	x_SystemMonitoring_PropertyId ON dbo.Property( [PropertyId] )
GO

CREATE INDEX 
	x_SystemMonitoring_TenantId ON dbo.Tenant( [TenantId] )
GO