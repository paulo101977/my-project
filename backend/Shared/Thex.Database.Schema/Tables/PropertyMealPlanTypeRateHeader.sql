﻿CREATE TABLE [dbo].[PropertyMealPlanTypeRateHeader]
(
	[PropertyMealPlanTypeRateHeaderId] UNIQUEIDENTIFIER NOT NULL,
	[PropertyId] INT NOT NULL,
	[InitialDate] DATETIME NOT NULL,
	[FinalDate] DATETIME NOT NULL,
	[CurrencyId] UNIQUEIDENTIFIER NOT NULL,

	[TenantId]                      UNIQUEIDENTIFIER    NOT NULL,
	[IsDeleted]                     BIT                 NOT NULL    DEFAULT 0,
	[CreationTime]                  DATETIME            NOT NULL    DEFAULT GETUTCDATE(),
	[CreatorUserId]                 UNIQUEIDENTIFIER    NULL,
	[LastModificationTime]          DATETIME            NULL        DEFAULT GETUTCDATE(),
	[LastModifierUserId]            UNIQUEIDENTIFIER    NULL,
	[DeletionTime]                  DATETIME            NULL,
	[DeleterUserId]                 UNIQUEIDENTIFIER    NULL,

	CONSTRAINT [PK_PropertyMealPlanTypeRateHeader] PRIMARY KEY ([PropertyMealPlanTypeRateHeaderId]),
	CONSTRAINT [FK_PropertyMealPlanTypeRateHeader_Property] FOREIGN KEY ([PropertyId]) REFERENCES [dbo].[Property] ([PropertyId]),
	CONSTRAINT [FK_PropertyMealPlanTypeRateHeader_Currency] FOREIGN KEY ([CurrencyId]) REFERENCES [dbo].[Currency] ([CurrencyId]),
	CONSTRAINT [FK_PropertyMealPlanTypeRateHeader_Tenant] FOREIGN KEY ([TenantId]) REFERENCES [dbo].[Tenant] ([TenantId])
)
GO

CREATE INDEX
x_PropertyMealPlanTypeRateHeader_PropertyId ON dbo.[PropertyMealPlanTypeRateHeader] ( [PropertyId] )
GO

CREATE INDEX
x_PropertyMealPlanTypeRateHeader_CurrencyId ON dbo.[PropertyMealPlanTypeRateHeader] ( [CurrencyId] )
GO

CREATE INDEX
x_PropertyMealPlanTypeRateHeader_TenantId ON dbo.[PropertyMealPlanTypeRateHeader] ( [TenantId] )
GO

CREATE INDEX
x_PropertyMealPlanTypeRateHeader_InitialDate ON dbo.[PropertyMealPlanTypeRateHeader] ( [InitialDate] )
GO

CREATE INDEX
x_PropertyMealPlanTypeRateHeader_FinalDate ON dbo.[PropertyMealPlanTypeRateHeader] ( [FinalDate] )
GO