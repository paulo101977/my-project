﻿CREATE TABLE [dbo].[CountryCurrency]
(
	[CountryCurrencyId]			UNIQUEIDENTIFIER	NOT NULL,
	[CountrySubdivisionId]		INT					NOT NULL,
	[CurrencyId]				UNIQUEIDENTIFIER	NOT NULL,

	[IsDeleted]					BIT					NOT NULL	DEFAULT 0,
	[CreationTime]				DATETIME			NOT NULL	DEFAULT GETUTCDATE(),
	[CreatorUserId]				BIGINT				NULL,
	[LastModificationTime]		DATETIME			NULL		DEFAULT GETUTCDATE(),
	[LastModifierUserId]		BIGINT				NULL,
	[DeletionTime]				DATETIME			NULL,
	[DeleterUserId]				BIGINT				NULL,

	CONSTRAINT [PK_CountryCurrency] PRIMARY KEY ([CountryCurrencyId]),
	CONSTRAINT [FK_CountryCurrency_CountrySubdivison] FOREIGN KEY ([CountrySubdivisionId]) REFERENCES [dbo].[CountrySubdivision] ([CountrySubdivisionId]),
	CONSTRAINT [FK_CountryCurrency_Currency] FOREIGN KEY ([CurrencyId]) REFERENCES [dbo].[Currency] ([CurrencyId])
)
GO

CREATE INDEX 
	x_CountryCurrency_CountrySubdivisionId ON dbo.CountryCurrency( [CountrySubdivisionId] );
GO

CREATE INDEX 
	x_CountryCurrency_CurrencyId ON dbo.CountryCurrency( [CurrencyId] );
GO