﻿CREATE TABLE [dbo].[PropertyMarketSegment]
(
	[PropertyMarketSegmentId]	UNIQUEIDENTIFIER	NOT NULL,
	[ChainIdMarketSegmentId]	UNIQUEIDENTIFIER	NOT NULL,
	[ChainId]					INT					NOT NULL,
	[PropertyId]				INT					NOT NULL,
	[MarketSegmentId]			INT					NOT NULL,
	[IsActive]					BIT					NOT NULL	DEFAULT 1,	
	[TenantId]					UNIQUEIDENTIFIER	NOT NULL,

	[IsDeleted]					BIT					NOT NULL	DEFAULT 0,
	[CreationTime]				DATETIME			NOT NULL	DEFAULT GETUTCDATE(),
	[CreatorUserId]				UNIQUEIDENTIFIER	NULL,
	[LastModificationTime]		DATETIME			NULL		DEFAULT GETUTCDATE(),
	[LastModifierUserId]		UNIQUEIDENTIFIER	NULL,
	[DeletionTime]				DATETIME			NULL,
	[DeleterUserId]				UNIQUEIDENTIFIER	NULL,

	CONSTRAINT [PK_PropertyMarketSegment] PRIMARY KEY ([PropertyMarketSegmentId]),
	CONSTRAINT [UK_PropertyMarketSegment] UNIQUE ([ChainId], [PropertyId], [MarketSegmentId]),
	CONSTRAINT [FK_PropertyMarketSegmente_ChainMarketSegment_1] FOREIGN KEY ([ChainIdMarketSegmentId]) REFERENCES [ChainMarketSegment]([ChainMarketSegmentId]),
	CONSTRAINT [FK_PropertyMarketSegmente_ChainMarketSegment_2] FOREIGN KEY ([ChainId], [MarketSegmentId]) REFERENCES [ChainMarketSegment]([ChainId], [MarketSegmentId]),
	CONSTRAINT [FK_PropertyMarketSegment_Property] FOREIGN KEY ([PropertyId]) REFERENCES [Property]([PropertyId]),
	CONSTRAINT [FK_PropertyMarketSegment_Tenant] FOREIGN KEY ([TenantId]) REFERENCES [dbo].[Tenant] ([TenantId])
)
GO

CREATE INDEX 
	x_PropertyMarketSegment_ChainIdMarketSegmentId ON dbo.[PropertyMarketSegment]( ChainIdMarketSegmentId )
GO

CREATE INDEX 
	x_PropertyMarketSegment_ChainId_MarketSegmentId ON dbo.[PropertyMarketSegment]( [ChainId], [MarketSegmentId] )
GO

CREATE INDEX 
	x_PropertyMarketSegment_PropertyId ON dbo.[PropertyMarketSegment]( [PropertyId] )
GO

CREATE INDEX 
	x_PropertyMarketSegment_TenantId ON [dbo].[PropertyMarketSegment]( [TenantId] )
GO