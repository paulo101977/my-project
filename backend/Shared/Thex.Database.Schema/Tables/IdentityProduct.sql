﻿CREATE TABLE [dbo].[IdentityProduct](
	[IdentityProductId]	INT	 				NOT NULL	IDENTITY(1,1),
	[Name]				VARCHAR(50)		NOT NULL,
	[Description]		VARCHAR(256)		NULL,
	
	[IsDeleted]					BIT					NOT NULL	DEFAULT 0,
	[CreationTime]				DATETIME			NOT NULL	DEFAULT GETUTCDATE(),
	[CreatorUserId]				UNIQUEIDENTIFIER	NULL,
	[LastModificationTime]		DATETIME			NULL		DEFAULT GETUTCDATE(),
	[LastModifierUserId]		UNIQUEIDENTIFIER	NULL,
	[DeletionTime]				DATETIME			NULL,
	[DeleterUserId]				UNIQUEIDENTIFIER	NULL,
	
    CONSTRAINT [PK_IdentityProduct] PRIMARY KEY ([IdentityProductId]),
)