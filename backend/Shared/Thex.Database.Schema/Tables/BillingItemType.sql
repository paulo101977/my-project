﻿CREATE TABLE [dbo].[BillingItemType]
(
	[BillingItemTypeId]				INTEGER				NOT NULL,
	[BillingItemTypeName]			VARCHAR(100)		NOT NULL,

	CONSTRAINT [PK_BillingItemType] PRIMARY KEY ( [BillingItemTypeId] )
)
GO