﻿CREATE TABLE [dbo].[HousekeepingRoomDisagreement]
(
	[HousekeepingRoomDisagreementId]	UNIQUEIDENTIFIER	NOT NULL,
	[PropertyId]						INT					NOT NULL,
	[HousekeepingRoomId]				UNIQUEIDENTIFIER	NULL,
	[AdultCount]						INT					NULL,
	[ChildrenCount]						INT					NULL,
	[BagCount]							INT					NULL,
	[IsDeleted]							BIT					NOT NULL	DEFAULT 0,
	[CreationTime]						DATETIME			NOT NULL	DEFAULT GETUTCDATE(),
	[CreatorUserId]						UNIQUEIDENTIFIER	NULL,
	[LastModificationTime]				DATETIME			NULL		DEFAULT GETUTCDATE(),
	[LastModifierUserId]				UNIQUEIDENTIFIER	NULL,
	[DeletionTime]						DATETIME			NULL,
	[DeleterUserId]						UNIQUEIDENTIFIER	NULL,	
	[Observation] TEXT NULL, 
    [HousekeepingRoomDisagreementStatusId] INT NULL, 
    [OwnerId] UNIQUEIDENTIFIER NULL, 
    [RoomId] INT NULL, 
    [HousekeepingRoomInspectionId] INT NULL, 
    CONSTRAINT [PK_HousekeepingRoomDisagreement] PRIMARY KEY ( [HousekeepingRoomDisagreementId] ),
	CONSTRAINT [FK_HousekeepingRoomDisagreement_HousekeepingRoom] FOREIGN KEY ( [HousekeepingRoomId] ) REFERENCES [dbo].[HousekeepingRoom] ( [HousekeepingRoomId] ),
	CONSTRAINT [FK_HousekeepingRoomDisagreement_Property] FOREIGN KEY ( [PropertyId] ) REFERENCES [dbo].[Property] ( [PropertyId] ),
	CONSTRAINT [FK_HousekeepingRoomDisagreement_Status] FOREIGN KEY([HousekeepingRoomDisagreementStatusId]) REFERENCES [dbo].[Status] ([StatusId]),
	CONSTRAINT [FK_HousekeepingRoomDisagreement_Room] FOREIGN KEY ( [RoomId] ) REFERENCES [dbo].[Room] ( [RoomId] ),	
	CONSTRAINT [FK_HousekeepingRoomDisagreement_Users] FOREIGN KEY ( [OwnerId] ) REFERENCES [dbo].[Users] ( [Id] ),
	CONSTRAINT [FK_HousekeepingRoomDisagreement_HousekeepingRoomInspectionId] FOREIGN KEY ( [HousekeepingRoomInspectionId] ) REFERENCES [dbo].[HousekeepingRoomInspection] ( [HousekeepingRoomInspectionId] ),
)
GO

CREATE INDEX 
	x_HousekeepingRoomDisagreement_HousekeepingRoomId ON [HousekeepingRoomDisagreement] ( [HousekeepingRoomId] )
GO

CREATE INDEX 
	x_HousekeepingRoomDisagreement_PropertyId ON [HousekeepingRoomDisagreement] ( [PropertyId] )
GO

