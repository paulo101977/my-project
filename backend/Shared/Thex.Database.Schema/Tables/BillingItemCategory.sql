﻿CREATE TABLE [dbo].[BillingItemCategory]
(
	[BillingItemCategoryId]			INT				NOT NULL	IDENTITY(1,1),
	[CategoryName]					VARCHAR(100)	NOT NULL,
	[PropertyId]					INT				NULL,
	[IsActive]						BIT				NOT NULL	,
	[StandardCategoryId]			INT				NULL,
	[TenantId]						UNIQUEIDENTIFIER	NULL,

	[IsDeleted]						BIT					NOT NULL	DEFAULT 0,
	[CreationTime]					DATETIME			NOT NULL	DEFAULT GETUTCDATE(),
	[CreatorUserId]					UNIQUEIDENTIFIER	NULL,
	[LastModificationTime]			DATETIME			NULL		DEFAULT GETUTCDATE(),
	[LastModifierUserId]			UNIQUEIDENTIFIER	NULL,
	[DeletionTime]					DATETIME			NULL,
	[DeleterUserId]					UNIQUEIDENTIFIER	NULL,
	[IconName]						VARCHAR(50)			NULL,

	CONSTRAINT [PK_BillingItemCategory] PRIMARY KEY ( [BillingItemCategoryId] ),
	CONSTRAINT [FK_BillingItemCategory_Property] FOREIGN KEY ( [PropertyId] ) REFERENCES [dbo].[Property] ( [PropertyId] ),
	CONSTRAINT [FK_BillingItemCategory_BillingItemCategory] FOREIGN KEY ( [StandardCategoryId] ) REFERENCES [dbo].[BillingItemCategory] ( [BillingItemCategoryId] ),
	CONSTRAINT [FK_BillingItemCategory_Tenant] FOREIGN KEY ([TenantId]) REFERENCES [dbo].[Tenant] ([TenantId])
)
GO

CREATE INDEX 
	x_BillingItemCategory_PropertyId ON BillingItemCategory ( [PropertyId] ) 
GO 

CREATE INDEX 
	x_BillingItemCategory_StandardCategoryId ON BillingItemCategory ( [StandardCategoryId] ) 
GO

CREATE INDEX 
	x_BillingItemCategory_TenantId ON [dbo].[BillingItemCategory]( [TenantId] )
GO
