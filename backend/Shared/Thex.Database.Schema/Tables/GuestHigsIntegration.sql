﻿CREATE TABLE [dbo].[GuestHigsIntegration]
(
	[GuestHigsIntegrationId]	UNIQUEIDENTIFIER					NOT NULL,
	[GuestId] 					BIGINT								NOT NULL,
	[HigsCode]					VARCHAR(100)						NOT NULL, 
	[PropertyId]				INT									NOT NULL,

	CONSTRAINT [PK_GuestHigsIntegration] PRIMARY KEY ([GuestHigsIntegrationId]),
	CONSTRAINT [UK_GuestHigsIntegration] UNIQUE ([GuestId], [HigsCode], [PropertyId]),
	CONSTRAINT [FK_GuestHigsIntegration_Guest] FOREIGN KEY ([GuestId]) REFERENCES [dbo].[Guest] ([GuestId]),
	CONSTRAINT [FK_GuestHigsIntegration_Property] FOREIGN KEY ([PropertyId]) REFERENCES [dbo].[Property] ([PropertyId])
);
GO