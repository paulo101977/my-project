﻿CREATE TABLE [dbo].[RatePlanCommission]
(
	[RatePlanCommissionId]			UNIQUEIDENTIFIER	NOT NULL,
	[RatePlanId]					UNIQUEIDENTIFIER	NOT NULL,
	[BillingItemId]					INTEGER				NOT NULL,
	[CommissionPercentualAmount]	NUMERIC (18,4)		NOT NULL,
	[TenantId]					UNIQUEIDENTIFIER	NOT NULL,

	[IsDeleted]					BIT					NOT NULL	DEFAULT 0,
	[CreationTime]				DATETIME			NOT NULL	DEFAULT GETUTCDATE(),
	[CreatorUserId]				UNIQUEIDENTIFIER	NULL,
	[LastModificationTime]		DATETIME			NULL		DEFAULT GETUTCDATE(),
	[LastModifierUserId]		UNIQUEIDENTIFIER	NULL,
	[DeletionTime]				DATETIME			NULL,
	[DeleterUserId]				UNIQUEIDENTIFIER	NULL,

	CONSTRAINT [PK_RatePlanCommission] PRIMARY KEY ( [RatePlanCommissionId] ),
	CONSTRAINT [UK_RatePlanCommission] UNIQUE ( [RatePlanId], [BillingItemId] ),
	CONSTRAINT [FK_RatePlanCommission_BillingItem] FOREIGN KEY ( [BillingItemId] ) REFERENCES [dbo].[BillingItem] ( [BillingItemId] ),
	CONSTRAINT [FK_RatePlanCommission_RatePlan] FOREIGN KEY ( [RatePlanId] ) REFERENCES [dbo].[RatePlan] ( [RatePlanId] ),
	CONSTRAINT [FK_RatePlanCommission_Tenant] FOREIGN KEY ([TenantId]) REFERENCES [dbo].[Tenant] ([TenantId])
)
GO

CREATE INDEX 
	x_RatePlanCommission_BillingItemId ON dbo.[RatePlanCommission] ( [BillingItemId] )
GO

CREATE INDEX 
	x_RatePlanCommission_RatePlanId ON dbo.[RatePlanCommission] ( [RatePlanId] )
GO

CREATE INDEX 
	x_RatePlanCommission_TenantId ON [dbo].[RatePlanCommission]( [TenantId] )
GO