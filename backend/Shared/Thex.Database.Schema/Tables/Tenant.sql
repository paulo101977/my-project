﻿CREATE TABLE [dbo].[Tenant]
(
	[TenantId]					UNIQUEIDENTIFIER	NOT NULL,
	[TenantName]				VARCHAR(50)			NOT NULL,
	[ChainId]					INT					NULL,
	[PropertyId]				INT					NULL,
	[BrandId]					INT					NULL,
	[ParentId]					UNIQUEIDENTIFIER	NULL,
	[IsActive]					BIT					NOT NULL,

	[IsDeleted]					BIT					NOT NULL	DEFAULT 0,
	[CreationTime]				DATETIME			NOT NULL	DEFAULT GETUTCDATE(),
	[CreatorUserId]				UNIQUEIDENTIFIER				NULL,
	[LastModificationTime]		DATETIME			NULL		DEFAULT GETUTCDATE(),
	[LastModifierUserId]		UNIQUEIDENTIFIER				NULL,
	[DeletionTime]				DATETIME			NULL,
	[DeleterUserId]				UNIQUEIDENTIFIER				NULL,
	
	CONSTRAINT [PK_Tenant] PRIMARY KEY ([TenantId]),
	CONSTRAINT [FK_Tenant_Tenant] FOREIGN KEY ([ParentId]) REFERENCES [dbo].[Tenant] ([TenantId]),
	CONSTRAINT [UK_Tenant] UNIQUE ([TenantName]),
	CONSTRAINT [FK_Tenant_Property] FOREIGN KEY ([PropertyId]) REFERENCES [dbo].[Property] ([PropertyId]),
	CONSTRAINT [FK_Tenant_Chain] FOREIGN KEY ([ChainId]) REFERENCES [dbo].[Chain] ([ChainId]),
	CONSTRAINT [FK_Tenant_Brand] FOREIGN KEY ([BrandId]) REFERENCES [dbo].[Brand] ([BrandId])
);
GO

CREATE INDEX 
	x_Tenant_ParentId ON dbo."Tenant"( [TenantId] )
GO

CREATE INDEX 
	x_Tenant_PropertyId ON dbo.Property( [PropertyId] )
GO

CREATE INDEX 
	x_Tenant_ChainId ON dbo.Chain( [ChainId] )
GO

CREATE INDEX 
	x_Tenant_BrandId ON dbo.Brand( [BrandId] )
GO