﻿CREATE TABLE [dbo].[Document] (
	[OwnerId]				UNIQUEIDENTIFIER	NOT NULL,
    [DocumentTypeId]		INT		  			NOT NULL,
    [DocumentInformation]	VARCHAR (20)		NOT NULL,

    CONSTRAINT [PK_Document] PRIMARY KEY ([OwnerId], [DocumentTypeId]),
	CONSTRAINT [FK_Document_DocumentType] FOREIGN KEY ([DocumentTypeId]) REFERENCES [dbo].[DocumentType] ([DocumentTypeId]),
);
GO

CREATE INDEX 
    x_Document_DocumentTypeIdId ON dbo.Document( [DocumentTypeId] )
GO