﻿CREATE TABLE [dbo].[AuditStepType]
(
	[AuditStepTypeId] INT NOT NULL PRIMARY KEY,
	[Name] VARCHAR (100) NOT NULL,
)
