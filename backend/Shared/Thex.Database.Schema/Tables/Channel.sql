﻿CREATE TABLE [dbo].[Channel]
(
	[ChannelId] 					UNIQUEIDENTIFIER	NOT NULL,
	[ChannelCodeId]					INT 				NOT NULL,
	[Description]					VARCHAR(60) 		NOT NULL,
	[BusinessSourceId]				INT					NULL,
	[DistributionAmount]			NUMERIC (18,4)		NULL,
	[IsActive]						BIT					NOT NULL	DEFAULT 0,
	[TenantId]						UNIQUEIDENTIFIER	NOT NULL,

	[IsDeleted]						BIT					NOT NULL	DEFAULT 0,
	[CreationTime]					DATETIME			NOT NULL	DEFAULT GETUTCDATE(),
	[CreatorUserId]					UNIQUEIDENTIFIER	NULL,
	[LastModificationTime]			DATETIME			NULL		DEFAULT GETUTCDATE(),
	[LastModifierUserId]			UNIQUEIDENTIFIER	NULL,
	[DeletionTime]					DATETIME			NULL,
	[DeleterUserId]					UNIQUEIDENTIFIER	NULL,
	[MarketSegmentId]				INT					NULL,

	CONSTRAINT [PK_Channel] PRIMARY KEY ([ChannelId]),
	CONSTRAINT [FK_Channel_BusinessSource] FOREIGN KEY ([BusinessSourceId]) REFERENCES [BusinessSource]([BusinessSourceId]),
	CONSTRAINT [FK_Channel_ChannelCode] FOREIGN KEY ([ChannelCodeId]) REFERENCES [ChannelCode]([ChannelCodeId]),
	CONSTRAINT [FK_Channel_Tenant] FOREIGN KEY ([TenantId]) REFERENCES [dbo].[Tenant] ([TenantId]),
	CONSTRAINT [FK_Channel_MarketSegment] FOREIGN KEY ( [MarketSegmentId] ) REFERENCES [dbo].[MarketSegment] ( [MarketSegmentId] )
);
GO

CREATE INDEX 
	x_Channel_BusinessSourceId ON dbo.Channel( [BusinessSourceId] )
GO

CREATE INDEX 
	x_Channel_ChannelCodeId ON dbo.Channel( [ChannelCodeId] )
GO

CREATE INDEX 
	x_Channel_TenantId ON [dbo].[Channel]( [TenantId] )
GO

CREATE INDEX 
	x_Channel_MarketSegmentId ON [dbo].[Channel]( [MarketSegmentId] )
GO