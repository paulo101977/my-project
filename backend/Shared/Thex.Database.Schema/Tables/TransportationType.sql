﻿CREATE TABLE [dbo].[TransportationType]
(
	[TransportationTypeId]						INT			NOT NULL,
	[TransportationTypeName]					VARCHAR(20)	NOT NULL,
	[HigsCode]				INT							NULL, 

	CONSTRAINT [PK_TransportationType] PRIMARY KEY ([TransportationTypeId])
)
GO
