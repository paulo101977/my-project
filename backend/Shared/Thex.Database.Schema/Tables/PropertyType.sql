﻿CREATE TABLE [dbo].[PropertyType]
(
	[PropertyTypeId]	INT 			NOT NULL,
	[Name]				VARCHAR (50)	NOT NULL,

	CONSTRAINT [PK_PropertyType] PRIMARY KEY ([PropertyTypeId])
)