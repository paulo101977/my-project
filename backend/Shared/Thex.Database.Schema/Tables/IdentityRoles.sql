﻿CREATE TABLE [dbo].[Roles](
	[Id]				UNIQUEIDENTIFIER	NOT NULL,
	[Name]				[nvarchar](256)		NULL,
	[NormalizedName]	[nvarchar](256)		NULL,
	[ConcurrencyStamp]	[nvarchar](max)		NULL,
	[IsActive]			BIT					NOT NULL	DEFAULT 1,
	[IdentityProductId]	INT					NULL,
	
	[IsDeleted]					BIT					NOT NULL	DEFAULT 0,
	[CreationTime]				DATETIME			NOT NULL	DEFAULT GETUTCDATE(),
	[CreatorUserId]				UNIQUEIDENTIFIER	NULL,
	[LastModificationTime]		DATETIME			NULL		DEFAULT GETUTCDATE(),
	[LastModifierUserId]		UNIQUEIDENTIFIER	NULL,
	[DeletionTime]				DATETIME			NULL,
	[DeleterUserId]				UNIQUEIDENTIFIER	NULL,
	

	CONSTRAINT [PK_Roles] PRIMARY KEY CLUSTERED ([Id]),
	CONSTRAINT [FK_Roles_IdentityProduct] FOREIGN KEY ([IdentityProductId]) REFERENCES [dbo].[IdentityProduct] ([IdentityProductId]),
	
)