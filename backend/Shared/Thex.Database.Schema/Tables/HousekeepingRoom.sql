﻿CREATE TABLE [dbo].[HousekeepingRoom]
(
	[HousekeepingRoomId]				UNIQUEIDENTIFIER	NOT NULL,
	[PropertyId]						INT					NOT NULL,
	[OwnerId]							UNIQUEIDENTIFIER	NOT NULL,
	[RoomId]							INT					NOT NULL,
	[IsStarted]							BIT					NOT NULL,
	[StartDate]							DATETIME			NULL,
	[EndDate]							DATETIME			NULL,
	[ElapsedTime]						INT					NULL		DEFAULT 2400,
	[ServiceDate]						DATETIME			NULL		DEFAULT GETUTCDATE(),
	[IsDeleted]							BIT					NOT NULL	DEFAULT 0,
	[CreationTime]						DATETIME			NOT NULL	DEFAULT GETUTCDATE(),
	[CreatorUserId]						UNIQUEIDENTIFIER	NULL,
	[LastModificationTime]				DATETIME			NULL		DEFAULT GETUTCDATE(),
	[LastModifierUserId]				UNIQUEIDENTIFIER	NULL,
	[DeletionTime]						DATETIME			NULL,
	[DeleterUserId]						UNIQUEIDENTIFIER	NULL,
	
	CONSTRAINT [PK_HousekeepingRoom] PRIMARY KEY ( [HousekeepingRoomId] ),
	CONSTRAINT [FK_HousekeepingRoom_Room] FOREIGN KEY ( [RoomId] ) REFERENCES [dbo].[Room] ( [RoomId] ),
	CONSTRAINT [FK_HousekeepingRoom_Property] FOREIGN KEY ( [PropertyId] ) REFERENCES [dbo].[Property] ( [PropertyId] ),
	CONSTRAINT [FK_HousekeepingRoom_Users] FOREIGN KEY ( [OwnerId] ) REFERENCES [dbo].[Users] ( [Id] ),
)
GO

CREATE INDEX 
	x_HousekeepingRoom_RoomId ON [HousekeepingRoom] ( [RoomId] )
GO

CREATE INDEX 
	x_HousekeepingRoom_PropertyId ON [HousekeepingRoom] ( [PropertyId] )
GO

CREATE INDEX 
	x_HousekeepingRoom_OwnerId ON [HousekeepingRoom] ( [OwnerId] )
GO