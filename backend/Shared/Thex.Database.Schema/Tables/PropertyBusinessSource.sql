﻿CREATE TABLE [dbo].[PropertyBusinessSource]
(
	[PropertyBusinessSourceId]	UNIQUEIDENTIFIER	NOT NULL,
	[ChainIdBusinessSourceId]	UNIQUEIDENTIFIER	NOT NULL,
	[ChainId]					INT					NOT NULL,
	[PropertyId]				INT					NOT NULL,
	[BusinessSourceId]			INT					NOT NULL,
	[IsActive]					BIT					NOT NULL	DEFAULT 1,	
	[TenantId]					UNIQUEIDENTIFIER	NOT NULL,

	[IsDeleted]					BIT					NOT NULL	DEFAULT 0,
	[CreationTime]				DATETIME			NOT NULL	DEFAULT GETUTCDATE(),
	[CreatorUserId]				UNIQUEIDENTIFIER	NULL,
	[LastModificationTime]		DATETIME			NULL		DEFAULT GETUTCDATE(),
	[LastModifierUserId]		UNIQUEIDENTIFIER	NULL,
	[DeletionTime]				DATETIME			NULL,
	[DeleterUserId]				UNIQUEIDENTIFIER	NULL,

	CONSTRAINT [PK_PropertyBusinessSource] PRIMARY KEY ([PropertyBusinessSourceId]),
	CONSTRAINT [UK_PropertyBusinessSource] UNIQUE ([ChainId], [PropertyId], [BusinessSourceId]),
	CONSTRAINT [FK_PropertyBusinessSourcee_ChainBusinessSource_1] FOREIGN KEY ([ChainIdBusinessSourceId]) REFERENCES [ChainBusinessSource]([ChainBusinessSourceId]),
	CONSTRAINT [FK_PropertyBusinessSourcee_ChainBusinessSource_2] FOREIGN KEY ([ChainId], [BusinessSourceId]) REFERENCES [ChainBusinessSource]([ChainId], [BusinessSourceId]),
	CONSTRAINT [FK_PropertyBusinessSource_Property] FOREIGN KEY ([PropertyId]) REFERENCES [Property]([PropertyId]),
	CONSTRAINT [FK_PropertyBusinessSource_Tenant] FOREIGN KEY ([TenantId]) REFERENCES [dbo].[Tenant] ([TenantId])
)
GO

CREATE INDEX 
	x_PropertyBusinessSource_ChainIdBusinessSourceId ON dbo.[PropertyBusinessSource]( ChainIdBusinessSourceId )
GO

CREATE INDEX 
	x_PropertyBusinessSource_ChainId_BusinessSourceId ON dbo.[PropertyBusinessSource]( [ChainId], [BusinessSourceId] )
GO

CREATE INDEX 
	x_PropertyBusinessSource_PropertyId ON dbo.[PropertyBusinessSource]( [PropertyId] )
GO

CREATE INDEX 
	x_PropertyBusinessSource_TenantId ON [dbo].[PropertyBusinessSource]( [TenantId] )
GO