﻿CREATE TABLE [dbo].[HousekeepingStatusProperty]
(
	[HousekeepingStatusPropertyId]		UNIQUEIDENTIFIER	NOT NULL,
	[PropertyId]						INT					NOT NULL,
	[HousekeepingStatusId]				INT					NOT NULL,
	[StatusName]						VARCHAR(100)		NOT NULL,
	[Color]								VARCHAR(6)			NOT NULL,
	[TenantId]							UNIQUEIDENTIFIER	NOT NULL,
	[IsActive]							BIT					NOT NULL	DEFAULT 1,
	[IsDeleted]							BIT					NOT NULL	DEFAULT 0,
	[CreationTime]						DATETIME			NOT NULL	DEFAULT GETUTCDATE(),
	[CreatorUserId]						UNIQUEIDENTIFIER	NULL,
	[LastModificationTime]				DATETIME			NULL		DEFAULT GETUTCDATE(),
	[LastModifierUserId]				UNIQUEIDENTIFIER	NULL,
	[DeletionTime]						DATETIME			NULL,
	[DeleterUserId]						UNIQUEIDENTIFIER	NULL,
	

	CONSTRAINT [PK_HousekeepingStatusProperty] PRIMARY KEY ( [HousekeepingStatusPropertyId] ),
	CONSTRAINT [FK_HousekeepingStatusProperty_HousekeepingStatus] FOREIGN KEY ( [HousekeepingStatusId] ) REFERENCES [dbo].[HousekeepingStatus] ( [HousekeepingStatusId] ),
	CONSTRAINT [FK_HousekeepingStatusProperty_Property] FOREIGN KEY ( [PropertyId] ) REFERENCES [dbo].[Property] ( [PropertyId] )
)
GO

CREATE INDEX 
	x_HousekeepingStatusProperty_HousekeepingStatusId ON [HousekeepingStatusProperty] ( [HousekeepingStatusId] )
GO

CREATE INDEX 
	x_HousekeepingStatusProperty_PropertyId ON [HousekeepingStatusProperty] ( [PropertyId] )
GO