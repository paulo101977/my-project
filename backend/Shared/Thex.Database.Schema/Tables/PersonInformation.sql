﻿CREATE TABLE [dbo].[PersonInformation]
(
	[OwnerId]					UNIQUEIDENTIFIER		NOT NULL,
	[PersonInformationTypeId]	INTEGER					NOT NULL,
	[Information]				VARCHAR(500)			NOT NULL,

	CONSTRAINT [PK_PersonInformation] PRIMARY KEY ([OwnerId], [PersonInformationTypeId], [Information]),
	CONSTRAINT [FK_PersonInformation_Person] FOREIGN KEY ([OwnerId]) REFERENCES [dbo].[Person] ([PersonId]),
	CONSTRAINT [FK_PersonInformation_PersonInformationType] FOREIGN KEY ([PersonInformationTypeId]) REFERENCES [dbo].[PersonInformationType] ([PersonInformationTypeId])
);
GO
