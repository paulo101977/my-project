﻿CREATE TABLE [dbo].[Level]
(
	[LevelId] 					UNIQUEIDENTIFIER	NOT NULL,
	[LevelCode]					INT 				NOT NULL,
	[PropertyId]			    INT		            NOT NULL,
	[Description]			    VARCHAR(60) 		NOT NULL,
	[Order]                     INT					NOT NULL,
	[IsActive]					BIT					NOT NULL	DEFAULT 0,
	
	[TenantId]					UNIQUEIDENTIFIER	NOT NULL,

	[IsDeleted]					BIT					NOT NULL	DEFAULT 0,
	[CreationTime]				DATETIME			NOT NULL	DEFAULT GETUTCDATE(),
	[CreatorUserId]				UNIQUEIDENTIFIER	NULL,
	[LastModificationTime]		DATETIME			NULL		DEFAULT GETUTCDATE(),
	[LastModifierUserId]		UNIQUEIDENTIFIER	NULL,
	[DeletionTime]				DATETIME			NULL,
	[DeleterUserId]				UNIQUEIDENTIFIER	NULL,

	CONSTRAINT [PK_Level] PRIMARY KEY ([LevelId]),
	CONSTRAINT [UK_Level_Code] UNIQUE ([LevelCode],[PropertyId], [IsDeleted],[DeletionTime]),
	CONSTRAINT [FK_Level_PropertyId] FOREIGN KEY ([PropertyId]) REFERENCES [dbo].[Property]([PropertyId]),
	CONSTRAINT [FK_Level_Tenant] FOREIGN KEY ([TenantId]) REFERENCES [dbo].[Tenant] ([TenantId])
	
	
);
GO

CREATE INDEX 
	x_Level_LevelId ON dbo.Level( [LevelId] )
GO

CREATE INDEX 
	x_Level_Code ON dbo.Level( [LevelCode] )
GO

CREATE INDEX 
	x_Level_PropertyId ON dbo.Property( [PropertyId] )
GO

CREATE INDEX 
	x_Level_TenantId ON [dbo].[Level]( [TenantId] )
GO