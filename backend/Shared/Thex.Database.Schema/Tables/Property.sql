﻿CREATE TABLE [dbo].[Property]
(
	[PropertyId]				INT IDENTITY		NOT NULL,
	[CompanyId]					INT					NOT NULL,
	[PropertyTypeId]			INT					NOT NULL,
	[BrandId]					INT					NOT NULL,
	[Name]						VARCHAR (200)		NOT NULL,
	[PropertyUId]				UNIQUEIDENTIFIER	NOT NULL	DEFAULT NEWID(),
	[PropertyStatusId]			INT					NOT NULL		DEFAULT 13,
	[TenantId]					UNIQUEIDENTIFIER	NOT NULL,
	[IsBlocked]					BIT					NULL			DEFAULT 0,

	[Photo]						VARCHAR (300)		NULL,
	[IsDeleted]					BIT					NOT NULL	DEFAULT 0,
	[CreationTime]				DATETIME			NOT NULL	DEFAULT GETUTCDATE(),
	[CreatorUserId]				UNIQUEIDENTIFIER	NULL,
	[LastModificationTime]		DATETIME			NULL		DEFAULT GETUTCDATE(),
	[LastModifierUserId]		UNIQUEIDENTIFIER	NULL,
	[DeletionTime]				DATETIME			NULL,
	[DeleterUserId]				UNIQUEIDENTIFIER	NULL,

	CONSTRAINT [PK_Property] PRIMARY KEY ([PropertyId]),
	CONSTRAINT [FK_Property_Company] FOREIGN KEY ([CompanyId]) REFERENCES [dbo].[Company] ([CompanyId]),
	CONSTRAINT [FK_Property_PropertyType] FOREIGN KEY ([PropertyTypeId]) REFERENCES [dbo].[PropertyType] ([PropertyTypeId]),
	CONSTRAINT [FK_Property_Brand] FOREIGN KEY ([BrandId]) REFERENCES [dbo].[Brand] ([BrandId]),
	CONSTRAINT [FK_Property_Status] FOREIGN KEY ([PropertyStatusId]) REFERENCES [dbo].[Status] ([StatusId]),
	CONSTRAINT [UK_Property_PropertyUId] UNIQUE ([PropertyUId])
);
GO

CREATE INDEX 
	x_Property_BrandId ON dbo.Property( [BrandId] )
GO

CREATE INDEX 
	x_Property_CompanyId ON dbo.Property( [CompanyId] )
GO

CREATE INDEX 
	x_Property_PropertyTypeId ON dbo.Property( [PropertyTypeId] )
GO

CREATE INDEX 
	x_Property_StatusId ON dbo.Property( [PropertyStatusId] )
GO