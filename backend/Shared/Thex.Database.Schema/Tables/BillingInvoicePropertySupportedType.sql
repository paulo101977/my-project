﻿CREATE TABLE [dbo].[BillingInvoicePropertySupportedType]
(
	[BillingInvoicePropertySupportedTypeId]			UNIQUEIDENTIFIER		NOT NULL,
	[BillingItemTypeId]										INT		NOT NULL,
	[BillingInvoicePropertyId]								UNIQUEIDENTIFIER		NOT NULL,
	[PropertyId]												INT		NOT NULL, 
	[BillingItemId]												INT	    NOT NULL,
	[CountrySubdvisionServiceId]							UNIQUEIDENTIFIER	NULL ,
	[TenantId]													UNIQUEIDENTIFIER	NOT NULL,

	[IsDeleted]													BIT					NOT NULL	DEFAULT 0,
	[CreationTime]												DATETIME			NOT NULL	DEFAULT GETUTCDATE(),
	[CreatorUserId]											UNIQUEIDENTIFIER	NULL,
	[LastModificationTime]									DATETIME			NULL		DEFAULT GETUTCDATE(),
	[LastModifierUserId]										UNIQUEIDENTIFIER	NULL,
	[DeletionTime]												DATETIME			NULL,
	[DeleterUserId]											UNIQUEIDENTIFIER	NULL,
    CONSTRAINT [PK_BillingInvoicePropertySupportedType] PRIMARY KEY ( [BillingInvoicePropertySupportedTypeId] ),
	CONSTRAINT [FK_BillingInvoicePropertySupportedType_Property] FOREIGN KEY ([PropertyId]) REFERENCES [Property]([PropertyId]),
	CONSTRAINT [FK_BillingInvoicePropertySupportedType_BillingItemType] FOREIGN KEY ( [BillingItemTypeId] ) REFERENCES [dbo].[BillingItemType] ( [BillingItemTypeId] ),
	CONSTRAINT [FK_BillingInvoicePropertySupportedType_BillingInvoiceProperty] FOREIGN KEY ([BillingInvoicePropertyId]) REFERENCES [BillingInvoiceProperty]([BillingInvoicePropertyId]),	
	CONSTRAINT [FK_BillingInvoicePropertySupportedType_BillingItem] FOREIGN KEY ([BillingItemId]) REFERENCES BillingItem([BillingItemId]),
	CONSTRAINT [FK_BillingInvoicePropertySupportedType_CountrySubdvisionService] FOREIGN KEY ([CountrySubdvisionServiceId]) REFERENCES CountrySubdvisionService([CountrySubdvisionServiceId]),
	CONSTRAINT [UK_BillingInvoicePropertySupportedType] UNIQUE ( [BillingItemTypeId], [BillingInvoicePropertyId], [PropertyId],[BillingItemId],[CountrySubdvisionServiceId],[IsDeleted],[DeletionTime]),
	CONSTRAINT [FK_BillingInvoicePropertySupportedType_Tenant] FOREIGN KEY ([TenantId]) REFERENCES [dbo].[Tenant] ([TenantId])

)
GO

CREATE INDEX 
	x_BillingInvoicePropertySupportedType_PropertyId ON [dbo].[BillingInvoicePropertySupportedType]( [PropertyId] )
GO

CREATE INDEX 
	x_BillingInvoicePropertySupportedType_BillingItemTypeId ON [dbo].[BillingInvoicePropertySupportedType]( [BillingItemTypeId] )
GO

CREATE INDEX 
	x_BillingInvoicePropertySupportedType_BillingInvoicePropertyId ON [dbo].[BillingInvoicePropertySupportedType]( [BillingInvoicePropertyId] )
GO

CREATE INDEX 
	x_BillingInvoicePropertySupportedType_BillingItemId ON [dbo].[BillingInvoicePropertySupportedType]( [BillingItemId] )
GO

CREATE INDEX 
	x_BillingInvoicePropertySupportedType_CountrySubdivisionServiceId ON [dbo].[BillingInvoicePropertySupportedType]( [CountrySubdvisionServiceId] )
GO


CREATE INDEX 
	x_BillingInvoicePropertySupportedType_TenantId ON [dbo].[BillingInvoicePropertySupportedType]( [TenantId] )
GO
