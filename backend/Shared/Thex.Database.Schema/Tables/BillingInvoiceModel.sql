﻿CREATE TABLE [dbo].[BillingInvoiceModel]
(
	[BillingInvoiceModelId]			INT	NOT NULL,
	[Description]						VARCHAR (100)		NOT NULL,

	[TaxModel] VARCHAR(100) NOT NULL, 
    [IsVisible] BIT NULL, 
    [BillingInvoiceTypeId] INT NULL, 
    CONSTRAINT PK_BillingInvoiceModel PRIMARY KEY ([BillingInvoiceModelId])
)
GO

CREATE INDEX 
	x_BillingInvoiceModel_BillingInvoiceTypeId ON [dbo].[BillingInvoiceModel] ( [BillingInvoiceTypeId] ) 
GO