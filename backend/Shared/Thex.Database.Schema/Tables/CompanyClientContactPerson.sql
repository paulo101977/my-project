﻿CREATE TABLE [dbo].[CompanyClientContactPerson]
(
	[CompanyClientContactPersonId]	UNIQUEIDENTIFIER	NOT NULL,
	[PersonId]						UNIQUEIDENTIFIER	NOT NULL,
	[CompanyClientId]				UNIQUEIDENTIFIER	NOT NULL,
	[OccupationId]					INTEGER				NULL,

	CONSTRAINT [PK_CompanyClientContractPerson] PRIMARY KEY ([CompanyClientContactPersonId]),
	CONSTRAINT [UK_CompanyClientContractPerson] UNIQUE ([PersonId], [CompanyClientId]),
	CONSTRAINT [FK_CompanyClientContractPerson_Person] FOREIGN KEY ([PersonId]) REFERENCES [dbo].[Person] ([PersonId]),
	CONSTRAINT [FK_CompanyClientContractPerson_Company] FOREIGN KEY ([CompanyClientId]) REFERENCES [dbo].[CompanyClient] ([CompanyClientId]),
	CONSTRAINT [FK_CompanyClientContractPerson_Occupation] FOREIGN KEY ([OccupationId]) REFERENCES [dbo].[Occupation] ([OccupationId])
)
GO

CREATE INDEX 
	x_CompanyClientContactPerson_Occupation ON dbo.Occupation( [OccupationId] )
GO

CREATE INDEX 
	x_CompanyClientContactPerson_CompanyClientId ON dbo.[CompanyClient]( [CompanyClientId] )
GO