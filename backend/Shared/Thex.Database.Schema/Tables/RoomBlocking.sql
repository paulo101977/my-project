﻿CREATE TABLE [dbo].[RoomBlocking]
(
	[RoomBlockingId]			UNIQUEIDENTIFIER	NOT NULL,
	[PropertyId]				INT					NOT NULL,	
	[RoomId]					INT					NOT NULL,
	[BlockingStartDate]			DATETIME			NOT NULL,
	[BlockingEndDate]			DATETIME			NOT NULL,
	[ReasonId]					INT					NOT NULL,
	[Comments]					VARCHAR(4000)		NULL,
	[TenantId]					UNIQUEIDENTIFIER	NOT NULL,

	[IsDeleted]					BIT					NOT NULL	DEFAULT 0,
	[CreationTime]				DATETIME			NOT NULL	DEFAULT GETUTCDATE(),
	[CreatorUserId]				UNIQUEIDENTIFIER	NULL,
	[LastModificationTime]		DATETIME			NULL		DEFAULT GETUTCDATE(),
	[LastModifierUserId]		UNIQUEIDENTIFIER	NULL,
	[DeletionTime]				DATETIME			NULL,
	[DeleterUserId]				UNIQUEIDENTIFIER	NULL,

	CONSTRAINT [PK_RoomBlocking] PRIMARY KEY ( [RoomBlockingId] ),
	CONSTRAINT [FK_RoomBlocking_Room] FOREIGN KEY ( [RoomId] ) REFERENCES [dbo].[Room] ( [RoomId] ),
	CONSTRAINT [FK_RoomBlocking_Property] FOREIGN KEY ( [PropertyId] ) REFERENCES [dbo].[Property] ( [PropertyId] ),
	CONSTRAINT [FK_RoomBlocking_Tenant] FOREIGN KEY ([TenantId]) REFERENCES [dbo].[Tenant] ([TenantId])
)
GO

CREATE INDEX 
	x_RoomBlocking_PropertyId ON [RoomBlocking] ( [PropertyId] )
GO

CREATE INDEX 
	x_RoomBlocking_RoomId ON [RoomBlocking] ( [RoomId] )
GO

CREATE INDEX 
	x_RoomBlocking_PropertyId_BlockingStartDate ON [RoomBlocking] ( [PropertyId], [BlockingStartDate] )
GO

CREATE INDEX 
	x_RoomBlocking_PropertyId_BlockingEndDate ON [RoomBlocking] ( [PropertyId], [BlockingEndDate] )
GO

CREATE INDEX 
	x_RoomBlocking_TenantId ON [dbo].[RoomBlocking]( [TenantId] )
GO