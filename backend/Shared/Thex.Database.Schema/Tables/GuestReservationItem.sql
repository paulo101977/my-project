CREATE TABLE [dbo].[GuestReservationItem](
	[GuestReservationItemId]		BIGINT				NOT NULL	IDENTITY(1,1),
	[ReservationItemId]				BIGINT				NOT NULL,
	[GuestId]						BIGINT				NULL,
	[GuestName]						VARCHAR(100)		NULL,
	[GuestEmail]					VARCHAR(100)		NULL,
	[GuestDocumentTypeId]			INT					NULL,
	[GuestDocument]					VARCHAR(20)			NULL,
	[EstimatedArrivalDate]			DATETIME			NOT NULL,
	[CheckInDate]					DATETIME			NULL,
	[EstimatedDepartureDate]		DATETIME			NOT NULL,
	[CheckOutDate]					DATETIME			NULL,
	[GuestStatusId]					INT					NOT NULL	DEFAULT 0,
	[GuestTypeId]					INT					NOT NULL,
	[PreferredName]					VARCHAR(100)		NULL,
	[GuestCitizenship]				VARCHAR(50)			NULL,
	[GuestLanguage]					VARCHAR(50)			NULL,
	[PctDailyRate]					SMALLINT			NOT NULL,
	[IsIncognito]					BIT					NOT NULL	DEFAULT 0,
	[IsChild]						BIT					NOT NULL	DEFAULT 0,
	[ChildAge]						SMALLINT			NULL		DEFAULT 0,
	[IsMain]						BIT					NOT NULL	DEFAULT 0,
	[IsGratuity]					BIT					NULL		DEFAULT 0,
	[IsSeparated]					BIT					NULL		DEFAULT 0,
	[PaxPosition]					SMALLINT			NULL		DEFAULT 0,
	[GuestRegistrationId]			UNIQUEIDENTIFIER	NULL,
	[PropertyId]					INT					NOT NULL,
	[TenantId]						UNIQUEIDENTIFIER	NOT NULL,

	[IsDeleted]						BIT					NOT NULL	DEFAULT 0,
	[CreationTime]					DATETIME			NOT NULL	DEFAULT GETUTCDATE(),
	[CreatorUserId]					UNIQUEIDENTIFIER	NULL,
	[LastModificationTime]			DATETIME			NULL		DEFAULT GETUTCDATE(),
	[LastModifierUserId]			UNIQUEIDENTIFIER	NULL,
	[DeletionTime]					DATETIME			NULL,
	[DeleterUserId]					UNIQUEIDENTIFIER	NULL,
	
	[CheckInHour] DATETIME NULL, 
    [CheckOutHour] DATETIME NULL, 
    CONSTRAINT [PK_GuestReservationItem] PRIMARY KEY CLUSTERED ([GuestReservationItemId] ASC),
	CONSTRAINT [FK_GuestReservationItem_Guest] FOREIGN KEY([GuestId]) REFERENCES [dbo].[Guest] ([GuestId]),
	CONSTRAINT [FK_GuestReservationItem_PropertyGuestType] FOREIGN KEY([GuestTypeId]) REFERENCES [dbo].[PropertyGuestType] ([PropertyGuestTypeId]),
	CONSTRAINT [FK_GuestReservationItem_ReservationItem] FOREIGN KEY([ReservationItemId]) REFERENCES [dbo].[ReservationItem] ([ReservationItemId]),
	CONSTRAINT [FK_GuestReservationItem_Status] FOREIGN KEY([GuestStatusId]) REFERENCES [dbo].[Status] ([StatusId]),
	CONSTRAINT [FK_GuestReservationItem_DocumentType] FOREIGN KEY([GuestDocumentTypeId]) REFERENCES [dbo].[DocumentType] ([DocumentTypeId]),
	CONSTRAINT [FK_GuestReservationItem_GuestRegistration] FOREIGN KEY([GuestRegistrationId]) REFERENCES [dbo].[GuestRegistration] ([GuestRegistrationId])
)
GO

ALTER TABLE [dbo].[GuestReservationItem] CHECK CONSTRAINT [FK_GuestReservationItem_Guest]
GO

ALTER TABLE [dbo].[GuestReservationItem] CHECK CONSTRAINT [FK_GuestReservationItem_PropertyGuestType]
GO

ALTER TABLE [dbo].[GuestReservationItem] CHECK CONSTRAINT [FK_GuestReservationItem_ReservationItem]
GO

ALTER TABLE [dbo].[GuestReservationItem] CHECK CONSTRAINT [FK_GuestReservationItem_Status]
GO

CREATE INDEX 
	x_GuestReservationItem_GuestId ON dbo.GuestReservationItem( [GuestId] )
GO

CREATE INDEX 
	x_GuestReservationItem_PropertyGuestTypeId ON dbo.GuestReservationItem( [GuestTypeId] )
GO

CREATE INDEX 
	x_GuestReservationItem_ReservationItemId ON dbo.GuestReservationItem( [ReservationItemId] )
GO

CREATE INDEX 
	x_GuestReservationItem_StatusId ON dbo.GuestReservationItem( [GuestStatusId] )
GO

CREATE INDEX 
	x_GuestReservationItem_GuestRegistrationId ON dbo.GuestReservationItem( [GuestRegistrationId] )
GO