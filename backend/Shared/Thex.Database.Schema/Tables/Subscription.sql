﻿CREATE TABLE [dbo].[Subscription]
(
	[SubscriptionId]			INT					NOT NULL	IDENTITY,
	[AccountId]					INT					NOT NULL,
	[RoomCountLimit]			INT					NOT NULL,
	[IsActive]					BIT 				NOT NULL,

	[IsDeleted]					BIT					NOT NULL	DEFAULT 0,
	[CreationTime]				DATETIME			NOT NULL	DEFAULT GETUTCDATE(),
	[CreatorUserId]				BIGINT				NULL,
	[LastModificationTime]		DATETIME			NULL		DEFAULT GETUTCDATE(),
	[LastModifierUserId]		BIGINT				NULL,
	[DeletionTime]				DATETIME			NULL,
	[DeleterUserId]				BIGINT				NULL,

	CONSTRAINT [PK_Subscription] PRIMARY KEY ([SubscriptionId])
);
GO

CREATE INDEX 
	x_Subscription_AccountId ON dbo.Subscription( [AccountId] )
GO