﻿CREATE TABLE [dbo].[UserClaims](
	[Id]			[int] IDENTITY(1,1) NOT NULL,
	[UserId]		UNIQUEIDENTIFIER	NOT NULL,
	[ClaimType]		[nvarchar](max)		NULL,
	[ClaimValue]	[nvarchar](max)		NULL,
	
	[IsDeleted]					BIT					NOT NULL	DEFAULT 0,
	[CreationTime]				DATETIME			NOT NULL	DEFAULT GETUTCDATE(),
	[CreatorUserId]				UNIQUEIDENTIFIER	NULL,
	[LastModificationTime]		DATETIME			NULL		DEFAULT GETUTCDATE(),
	[LastModifierUserId]		UNIQUEIDENTIFIER	NULL,
	[DeletionTime]				DATETIME			NULL,
	[DeleterUserId]				UNIQUEIDENTIFIER	NULL,

	CONSTRAINT [PK_UserClaims] PRIMARY KEY CLUSTERED ([Id]),
	CONSTRAINT [FK_AspNetUserClaims_AspNetUsers_UserId] FOREIGN KEY([UserId]) REFERENCES [dbo].[Users] ([Id])
)