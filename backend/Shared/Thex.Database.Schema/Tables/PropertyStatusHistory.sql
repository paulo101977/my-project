﻿CREATE TABLE [dbo].[PropertyStatusHistory]
(
	[PropertyStatusHistoryId]		UNIQUEIDENTIFIER	NOT NULL, 
    [PropertyId] INT NOT NULL, 
    [StatusId] INT NOT NULL, 
    [IsDeleted]						BIT					NOT NULL	DEFAULT 0,
	[CreationTime]					DATETIME			NOT NULL	DEFAULT GETUTCDATE(),
	[CreatorUserId]					UNIQUEIDENTIFIER	NULL,
	[LastModificationTime]			DATETIME			NULL		DEFAULT GETUTCDATE(),
	[LastModifierUserId]			UNIQUEIDENTIFIER	NULL,
	[DeletionTime]					DATETIME			NULL,
	[DeleterUserId]					UNIQUEIDENTIFIER	NULL,

	CONSTRAINT [PK_PropertyStatusHistory] PRIMARY KEY ([PropertyStatusHistoryId]),
	CONSTRAINT [FK_PropertyStatusHistory_Property] FOREIGN KEY ( [PropertyId] ) REFERENCES [dbo].[Property] ( [PropertyId] ),
	CONSTRAINT [FK_PropertyStatusHistory_PropertyStatus] FOREIGN KEY ( [StatusId] ) REFERENCES [dbo].[Status] ( [StatusId] )
)

GO

CREATE INDEX 
	x_PropertyStatusHistory_PropertyId ON dbo.[PropertyStatusHistory] ( [PropertyId] )
GO

CREATE INDEX 
	x_PropertyStatusHistory_StatusId ON dbo.[PropertyStatusHistory] ( [StatusId] )