﻿CREATE TABLE [dbo].[IdentityFeature](
	[IdentityFeatureId]	UNIQUEIDENTIFIER	NOT NULL,
	[IdentityProductId]	INT					NOT NULL,
	[IdentityModuleId]	INT					NOT NULL,
	[Key]				VARCHAR(200)		NOT NULL,
	[Name]				VARCHAR(300)		NOT NULL,
	[Description]		VARCHAR(400)		NOT NULL,
	[IsInternal]		BIT					NOT NULL	DEFAULT 0,
	[ExternalAccess]		BIT					NULL,
	
	[IsDeleted]					BIT					NOT NULL	DEFAULT 0,
	[CreationTime]				DATETIME			NOT NULL	DEFAULT GETUTCDATE(),
	[CreatorUserId]				UNIQUEIDENTIFIER	NULL,
	[LastModificationTime]		DATETIME			NULL		DEFAULT GETUTCDATE(),
	[LastModifierUserId]		UNIQUEIDENTIFIER	NULL,
	[DeletionTime]				DATETIME			NULL,
	[DeleterUserId]				UNIQUEIDENTIFIER	NULL,
	
    CONSTRAINT [PK_IdentityFeature] PRIMARY KEY ([IdentityFeatureId]),
	CONSTRAINT [UK_IdentityFeature_Key] UNIQUE ([Key], [IsDeleted], [DeletionTime]),
	CONSTRAINT [FK_IdentityFeature_IdentityProduct] FOREIGN KEY ([IdentityProductId]) REFERENCES [dbo].[IdentityProduct] ([IdentityProductId]),
	CONSTRAINT [FK_IdentityFeature_IdentityModule] FOREIGN KEY ([IdentityModuleId]) REFERENCES [dbo].[IdentityModule] ([IdentityModuleId])
)