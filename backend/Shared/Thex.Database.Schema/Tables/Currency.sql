﻿CREATE TABLE [dbo].[Currency]
(
	[CurrencyId]				UNIQUEIDENTIFIER	NOT NULL,
	[CountrySubdivisionId]		INT					NULL,
	[TwoLetterIsoCode]			CHAR(2)				NULL,
	[CurrencyName]				VARCHAR(200)		NOT NULL,
	[AlphabeticCode]			VARCHAR(10)			NOT NULL,
	[NumnericCode]				VARCHAR(10)			NOT NULL,
	[MinorUnit]					INT					NOT NULL,
	[Symbol]					VARCHAR(10)			NOT NULL,
	[ExchangeRate]				NUMERIC(18,4)		NOT NULL,

	[IsDeleted]					BIT					NOT NULL	DEFAULT 0,
	[CreationTime]				DATETIME			NOT NULL	DEFAULT GETUTCDATE(),
	[CreatorUserId]				BIGINT				NULL,
	[LastModificationTime]		DATETIME			NULL		DEFAULT GETUTCDATE(),
	[LastModifierUserId]		BIGINT				NULL,
	[DeletionTime]				DATETIME			NULL,
	[DeleterUserId]				BIGINT				NULL,

	CONSTRAINT [PK_Currency] PRIMARY KEY ([CurrencyId]),
	CONSTRAINT [FK_Currency_CountrySubdivison] FOREIGN KEY ([CountrySubdivisionId]) REFERENCES [dbo].[CountrySubdivision] ([CountrySubdivisionId])
)
GO

CREATE INDEX 
	x_Currency_CountrySubdivisionId ON dbo.Currency( [CountrySubdivisionId] )
GO