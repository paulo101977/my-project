﻿CREATE TABLE [dbo].[ParameterType]
(
	[ParameterTypeId]		INT				NOT NULL,
	[ParameterTypeName]		VARCHAR(100)	NOT NULL,

	CONSTRAINT [PK_ParameterType] PRIMARY KEY ([ParameterTypeId])
)