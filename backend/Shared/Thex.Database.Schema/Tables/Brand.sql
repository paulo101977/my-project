﻿CREATE TABLE [dbo].[Brand]
(
	[BrandId]		INT IDENTITY	NOT NULL,
	[ChainId]		INT				NOT NULL,
	[Name]			VARCHAR (50)	NOT NULL,
	[IsTemporary]	BIT				NOT NULL	DEFAULT 0,
	[TenantId]					UNIQUEIDENTIFIER	NOT NULL,

	[IsDeleted]					BIT					NOT NULL	DEFAULT 0,
	[CreationTime]				DATETIME			NOT NULL	DEFAULT GETUTCDATE(),
	[CreatorUserId]				UNIQUEIDENTIFIER	NULL,
	[LastModificationTime]		DATETIME			NULL		DEFAULT GETUTCDATE(),
	[LastModifierUserId]		UNIQUEIDENTIFIER	NULL,
	[DeletionTime]				DATETIME			NULL,
	[DeleterUserId]				UNIQUEIDENTIFIER	NULL,

	CONSTRAINT [PK_Brand] PRIMARY KEY ([BrandId]),
	CONSTRAINT [FK_Brand_Chain] FOREIGN KEY ([ChainId]) REFERENCES [dbo].[Chain] ([ChainId]),
	CONSTRAINT [UK_Brand_Name] UNIQUE ([Name])
);
GO

CREATE INDEX 
	x_Brand_ChainId ON dbo.Brand( [ChainId] )
GO
