﻿CREATE TABLE [dbo].[GuestRegistrationDocument]
(
	[GuestRegistrationDocumentId]		UNIQUEIDENTIFIER	NOT NULL,
	[DocumentTypeId]					INT					NOT NULL,
	[DocumentInformation]				VARCHAR (20)		NOT NULL,
	[GuestRegistrationId]				UNIQUEIDENTIFIER	NOT NULL,
	
	[IsDeleted]							BIT					NOT NULL	DEFAULT 0,
	[CreationTime]						DATETIME			NOT NULL	DEFAULT GETUTCDATE(),
	[CreatorUserId]						UNIQUEIDENTIFIER	NULL,
	[LastModificationTime]				DATETIME			NULL		DEFAULT GETUTCDATE(),
	[LastModifierUserId]				UNIQUEIDENTIFIER	NULL,
	[DeletionTime]						DATETIME			NULL,
	[DeleterUserId]						UNIQUEIDENTIFIER	NULL,

	CONSTRAINT [PK_GuestRegistrationDocument] PRIMARY KEY ([GuestRegistrationDocumentId]),
	CONSTRAINT [UK_GuestRegistrationDocument] UNIQUE ([GuestRegistrationId], [DocumentTypeId], [IsDeleted], [DeletionTime]),
	CONSTRAINT [FK_GuestRegistrationDocument_DocumentType] FOREIGN KEY ([DocumentTypeId]) REFERENCES [dbo].[DocumentType] ([DocumentTypeId]),
	CONSTRAINT [FK_GuestRegistrationDocument_GuestRegistration] FOREIGN KEY ([GuestRegistrationId]) REFERENCES [dbo].[GuestRegistration] ([GuestRegistrationId]),
);

GO

CREATE INDEX 
    x_GuestRegistrationDocument_DocumentTypeId ON dbo.Document( [DocumentTypeId] )
GO

CREATE INDEX 
    x_GuestRegistrationDocument_GuestRegistrationId ON dbo.GuestRegistration( [GuestRegistrationId] )
GO
