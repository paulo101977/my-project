﻿CREATE TABLE [dbo].[PersonInformationType]
(
	[PersonInformationTypeId] INTEGER		NOT NULL,
	[Name]					  VARCHAR(50)	NOT NULL,

	CONSTRAINT [PK_PersonInformationType] PRIMARY KEY ([PersonInformationTypeId])
)
