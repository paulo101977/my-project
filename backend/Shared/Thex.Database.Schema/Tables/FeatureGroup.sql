﻿CREATE TABLE [dbo].[FeatureGroup]
(
	[FeatureGroupId]		INT				NOT NULL,
	[FeatureGroupName]		VARCHAR(100)	NOT NULL,
	[Description]			VARCHAR(300)	NOT NULL,

	CONSTRAINT PK_FeatureGroup PRIMARY KEY ( [FeatureGroupId] )
)
