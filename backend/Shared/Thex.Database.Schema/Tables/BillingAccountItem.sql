﻿CREATE TABLE [dbo].[BillingAccountItem]
(
	[BillingAccountItemId]						UNIQUEIDENTIFIER		NOT NULL,
	[BillingAccountId]							UNIQUEIDENTIFIER		NOT NULL,
	[BillingAccountIdLastSource]				UNIQUEIDENTIFIER		NULL,
	[BillingItemId]								INTEGER					NULL,
	[BillingAccountItemTypeId]					INTEGER					NOT NULL,
	[BillingAccountItemComments]				VARCHAR (500)			NULL, 
	[Amount]									NUMERIC (18,2)			NOT NULL,
	[OriginalAmount]							NUMERIC (18,2)			NULL,
	[PercentualAmount]							NUMERIC (18,2)			NULL,
	[OldAmount]									NUMERIC (18,2)			NULL,
	[OldOriginalAmount]							NUMERIC (18,2)			NULL,
	[OldPercentualAmount]						NUMERIC (18,2)			NULL,
	[CheckNumber]								VARCHAR(100)			NULL,
	[NSU]										VARCHAR (100)			NULL,
	[BillingAccountItemDate]					DATETIME				NOT NULL,
	[BillingAccountItemReasonId]				INTEGER					NULL,
	[BillingAccountItemObservation]		VARCHAR(50)				NULL,
	[BillingAccountItemParentId]				UNIQUEIDENTIFIER		NULL,
	[IntegrationDetail]							VARCHAR(MAX)			NULL,
	[WasReversed]								BIT						NOT NULL DEFAULT 0, 
	[BillingInvoiceId]							UNIQUEIDENTIFIER		NULL, 
	[BillingAccountItemTypeIdLastSource]		INT						NULL, 
	[Order]										INT						NULL, 
	[PartialParentBillingAccountItemId]			UNIQUEIDENTIFIER		NULL, 
	[InstallmentsQuantity]						INT						NULL, 
	[IsOriginal]								BIT						NULL,
	[CurrencyId]								UNIQUEIDENTIFIER		NOT NULL,
	[CurrencyExchangeReferenceId]				UNIQUEIDENTIFIER		NULL,
	[CurrencyExchangeReferenceSecId]			UNIQUEIDENTIFIER		NULL,
	[CurrencySymbol]							VARCHAR(10)				NULL,
	[TenantId]									UNIQUEIDENTIFIER		NOT NULL,
	[ServiceDescription]						VARCHAR(MAX)			NULL,
	[IsDeleted]									BIT						NOT NULL	DEFAULT 0,
	[CreationTime]								DATETIME				NOT NULL	DEFAULT GETUTCDATE(),
	[CreatorUserId]								UNIQUEIDENTIFIER		NULL,
	[LastModificationTime]						DATETIME				NULL		DEFAULT GETUTCDATE(),
	[LastModifierUserId]						UNIQUEIDENTIFIER		NULL,
	[DeletionTime]								DATETIME				NULL,
	[DeleterUserId]								UNIQUEIDENTIFIER		NULL,

	[DateParameter] DATETIME NULL, 
    [Daily] BIT NULL, 
	[IsTourismTax] BIT NULL, 
    [IntegrationCode] VARCHAR(100) NULL, 
    [CreditAmount] NUMERIC(18, 4) NULL, 
    [IntegrationPaymentId] NVARCHAR(50) NULL, 
    CONSTRAINT [PK_BillingAccountItem] PRIMARY KEY ( [BillingAccountItemId] ),
	CONSTRAINT [FK_BillingAccountItem_BillingAccount] FOREIGN KEY ( [BillingAccountId] ) REFERENCES [dbo].[BillingAccount] ( [BillingAccountId] ),
	CONSTRAINT [FK_BillingAccountItem_BillingAccountItem] FOREIGN KEY ( [BillingAccountItemParentId] ) REFERENCES [dbo].[BillingAccountItem] ( [BillingAccountItemId] ),
	CONSTRAINT [FK_BillingAccountItem_BillingAccountItemType] FOREIGN KEY ( [BillingAccountItemTypeId] ) REFERENCES [dbo].[BillingAccountItemType] ( [BillingAccountItemTypeId] ),
	CONSTRAINT [FK_BillingAccountItem_Reason] FOREIGN KEY ( [BillingAccountItemReasonId] ) REFERENCES [dbo].[Reason] ( [ReasonId] ),
	CONSTRAINT [FK_BillingAccountItem_BillingItem] FOREIGN KEY ( [BillingItemId] ) REFERENCES [dbo].[BillingItem] ( [BillingItemId] ),
	CONSTRAINT [FK_BillingAccountItem_BillingAccount_LastSource] FOREIGN KEY ( [BillingAccountIdLastSource] ) REFERENCES [dbo].[BillingAccount] ( [BillingAccountId] ),
	CONSTRAINT [FK_BillingAccountItem_BillingInvoice] FOREIGN KEY ( [BillingInvoiceId] ) REFERENCES [dbo].[BillingInvoice] ( [BillingInvoiceId] ),
	CONSTRAINT [FK_BillingAccountItem_BillingAccountItem2] FOREIGN KEY ( [PartialParentBillingAccountItemId] ) REFERENCES [dbo].[BillingAccountItem] ( [BillingAccountItemId] ),
	CONSTRAINT [FK_BillingAccountItem_Tenant] FOREIGN KEY ([TenantId]) REFERENCES [dbo].[Tenant] ([TenantId]),
	CONSTRAINT [FK_BillingAccountItem_Currency] FOREIGN KEY ( [CurrencyId] ) REFERENCES [dbo].[Currency] ( [CurrencyId] )
)
GO

CREATE INDEX 
	x_BillingAccountItem_BillingAccountId ON BillingAccountItem ( [BillingAccountId] ) 
GO 

CREATE INDEX 
	x_BillingAccountItem_BillingAccountItemParentId ON BillingAccountItem ( [BillingAccountItemParentId] ) 
GO

CREATE INDEX 
	x_BillingAccountItem_BillingAccountItemReasonId ON BillingAccountItem ( [BillingAccountItemReasonId] ) 
GO

CREATE INDEX 
	x_BillingAccountItem_BillingItemId ON BillingAccountItem ( [BillingItemId] ) 
GO

CREATE INDEX 
	x_BillingAccountItem_BillingAccountItemTypeId ON BillingAccountItem ( [BillingAccountItemTypeId] ) 
GO

CREATE INDEX 
	x_BillingAccountItem_BillingAccountIdLastSource ON BillingAccountItem ( [BillingAccountIdLastSource] ) 
GO

CREATE INDEX 
	x_BillingAccountItem_CurrencyExchangeReferenceId ON BillingAccountItem ( [CurrencyExchangeReferenceId] ) 
GO

CREATE INDEX 
	x_BillingAccountItem_CurrencyId ON BillingAccountItem ( [CurrencyId] ) 
GO
GO

CREATE INDEX 
	x_BillingAccountItem_TenantId ON [dbo].[BillingAccountItem]( [TenantId] )
GO