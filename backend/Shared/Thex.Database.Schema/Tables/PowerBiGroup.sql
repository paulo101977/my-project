﻿CREATE TABLE [dbo].[PowerBiGroup]
(
	[PowerBiGroupId]						UNIQUEIDENTIFIER	NOT NULL,
	[PowerBiGroupKey]						VARCHAR(100)		NOT NULL,
	[PowerBiGroupDescription]				VARCHAR(200)		NULL,

	CONSTRAINT [PK_PowerBiGroup] PRIMARY KEY ([PowerBiGroupId])
)
