﻿CREATE TABLE [dbo].[BillingItem]
(
	[BillingItemId]					INTEGER				NOT NULL		IDENTITY(1,1),
	[BillingItemTypeId]				INTEGER				NOT NULL,
	[BillingItemName]				VARCHAR(100)		NULL,
	[BillingItemCategoryId]			INT					NULL,
	[Description]					VARCHAR(200)		NULL,
	[PropertyId]					INT					NULL,
	[PaymentTypeId]					INT					NULL,
	[PlasticBrandPropertyId]		UNIQUEIDENTIFIER	NULL,
	[AcquirerId]					UNIQUEIDENTIFIER	NULL,
	[MaximumInstallmentsQuantity]	SMALLINT			NULL,
	[IntegrationCode]				VARCHAR(100)		NULL,
	[IsActive]						BIT					NOT NULL,
	[TenantId]						UNIQUEIDENTIFIER	NOT NULL,

	[IsDeleted]						BIT					NOT NULL	DEFAULT 0,
	[CreationTime]					DATETIME			NOT NULL	DEFAULT GETUTCDATE(),
	[CreatorUserId]					UNIQUEIDENTIFIER	NULL,
	[LastModificationTime]			DATETIME			NULL		DEFAULT GETUTCDATE(),
	[LastModifierUserId]			UNIQUEIDENTIFIER	NULL,
	[DeletionTime]					DATETIME			NULL,
	[DeleterUserId]					UNIQUEIDENTIFIER	NULL,

	CONSTRAINT [PK_BillingItem] PRIMARY KEY ( [BillingItemId] ),
	CONSTRAINT [FK_BillingItem_BillingItemType] FOREIGN KEY ( [BillingItemTypeId] ) REFERENCES [dbo].[BillingItemType] ( [BillingItemTypeId] ),
	CONSTRAINT [FK_BillingItem_BillingItemCategory] FOREIGN KEY ( [BillingItemCategoryId] ) REFERENCES [dbo].[BillingItemCategory] ( [BillingItemCategoryId] ),
	CONSTRAINT [FK_BillingItem_PaymentType] FOREIGN KEY ( [PaymentTypeId] ) REFERENCES [dbo].[PaymentType] ( [PaymentTypeId] ),
	CONSTRAINT [FK_BillingItem_PlasticBrandProperty] FOREIGN KEY ( [PlasticBrandPropertyId] ) REFERENCES [dbo].[PlasticBrandProperty] ( [PlasticBrandPropertyId] ),
	CONSTRAINT [FK_BillingItem_CompanyClient] FOREIGN KEY ( [AcquirerId] ) REFERENCES [dbo].[CompanyClient] ( [CompanyClientId] ),
	CONSTRAINT [FK_BillingItem_Tenant] FOREIGN KEY ([TenantId]) REFERENCES [dbo].[Tenant] ([TenantId])
)
GO

CREATE INDEX 
	x_BillingItem_BillingItemTypeId ON BillingItem ( [BillingItemTypeId] ) 
GO 

CREATE INDEX 
	x_BillingItem_BillingItemCategoryId ON BillingItem ( [BillingItemCategoryId] ) 
GO 

CREATE INDEX 
	x_BillingItem_PaymentTypeId ON BillingItem ( [PaymentTypeId] ) 
GO

CREATE INDEX 
	x_BillingItem_PlasticBrandPropertyId ON BillingItem ( [PlasticBrandPropertyId] )
GO

CREATE INDEX 
	x_BillingItem_CompanyClientId ON BillingItem  ( [AcquirerId] )
GO

CREATE INDEX 
	x_BillingItem_TenantId ON [dbo].[BillingItem]( [TenantId] )
GO