﻿CREATE TABLE [dbo].[PropertyMealPlanTypeRateHistory]
(
	[PropertyMealPlanTypeRateHistoryId] UNIQUEIDENTIFIER NOT NULL,
	[PropertyId] INT NOT NULL,
	[MealPlanTypeId] INT NOT NULL,
	[MealPlanTypeDefault] BIT NULL,
	[Adult_MealPlan_Amount] NUMERIC (18,4) NULL,
	[Child_1_MealPlan_Amount] NUMERIC (18,4) NULL,
	[Child_2_MealPlan_Amount] NUMERIC (18,4) NULL,
	[Child_3_MealPlan_Amount] NUMERIC (18,4) NULL,
	[TenantId] UNIQUEIDENTIFIER NOT NULL,
	[PropertyMealPlanTypeRateHeaderId] UNIQUEIDENTIFIER NULL,

	[Sunday]                        BIT                 NOT NULL    DEFAULT 0,
	[Monday]                        BIT                 NOT NULL    DEFAULT 0,
	[Tuesday]                       BIT                 NOT NULL    DEFAULT 0,
	[Wednesday]                     BIT                 NOT NULL    DEFAULT 0,
	[Thursday]                      BIT                 NOT NULL    DEFAULT 0,
	[Friday]                        BIT                 NOT NULL    DEFAULT 0,
	[Saturday]                      BIT                 NOT NULL    DEFAULT 0,
    
	[IsDeleted]                     BIT                 NOT NULL    DEFAULT 0,
	[CreationTime]                  DATETIME            NOT NULL    DEFAULT GETUTCDATE(),
	[CreatorUserId]                 UNIQUEIDENTIFIER    NULL,
	[LastModificationTime]          DATETIME            NULL        DEFAULT GETUTCDATE(),
	[LastModifierUserId]            UNIQUEIDENTIFIER    NULL,
	[DeletionTime]                  DATETIME            NULL,
	[DeleterUserId]                 UNIQUEIDENTIFIER    NULL,

	CONSTRAINT [PK_PropertyMealPlanTypeRateHistory] PRIMARY KEY ([PropertyMealPlanTypeRateHistoryId]),
	CONSTRAINT [FK_PropertyMealPlanTypeRateHistory_Property] FOREIGN KEY ([PropertyId]) REFERENCES [dbo].[Property] ([PropertyId]),
	CONSTRAINT [FK_PropertyMealPlanTypeRateHistory_MealPlanType] FOREIGN KEY ([MealPlanTypeId]) REFERENCES [dbo].[MealPlanType] ([MealPlanTypeId]),
	CONSTRAINT [FK_PropertyMealPlanTypeRateHistory_PropertyMealPlanTypeRateHeader] FOREIGN KEY ([PropertyMealPlanTypeRateHeaderId]) REFERENCES [dbo].[PropertyMealPlanTypeRateHeader] ([PropertyMealPlanTypeRateHeaderId]),
	CONSTRAINT [FK_PropertyMealPlanTypeRateHistory_Tenant] FOREIGN KEY ([TenantId]) REFERENCES [dbo].[Tenant] ([TenantId]),
)
GO

CREATE INDEX
x_PropertyMealPlanTypeRateHistory_PropertyId ON dbo.[PropertyMealPlanTypeRateHistory] ( [PropertyId] )
GO

CREATE INDEX
x_PropertyMealPlanTypeRateHistory_MealPlanTypeId ON dbo.[PropertyMealPlanTypeRateHistory] ( [MealPlanTypeId] )
GO

CREATE INDEX
x_PropertyMealPlanTypeRateHistory_TenantId ON dbo.[PropertyMealPlanTypeRateHeader] ( [TenantId] )
GO

CREATE INDEX
x_PropertyMealPlanTypeRateHistory_PropertyMealPlanTypeRateHeaderId ON dbo.[PropertyMealPlanTypeRateHeader] ( [PropertyMealPlanTypeRateHeaderId] )
GO