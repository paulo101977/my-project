﻿CREATE TABLE [dbo].[ProductCategory]
(
	[ProductCategoryId]				UNIQUEIDENTIFIER		NOT NULL,
	[CategoryName]					VARCHAR(100)			NOT NULL,
	[PropertyId]					INT						NULL,
	[ChainId]						INT						NULL,
	[IsActive]						BIT						NOT NULL		DEFAULT 1,
	[StandardCategoryId]			UNIQUEIDENTIFIER		NULL,
	[IconName]						VARCHAR(50)				NULL,
	[TenantId]						UNIQUEIDENTIFIER		NULL,

	[IsDeleted]						BIT					NOT NULL	DEFAULT 0,
	[CreationTime]					DATETIME			NOT NULL	DEFAULT GETUTCDATE(),
	[CreatorUserId]					UNIQUEIDENTIFIER	NULL,
	[LastModificationTime]			DATETIME			NULL		DEFAULT GETUTCDATE(),
	[LastModifierUserId]			UNIQUEIDENTIFIER	NULL,
	[DeletionTime]					DATETIME			NULL,
	[DeleterUserId]					UNIQUEIDENTIFIER	NULL,
	[IntegrationCode]				VARCHAR(40)			NULL, 
    CONSTRAINT [PK_ProductCategory] PRIMARY KEY ( [ProductCategoryId] ),
	CONSTRAINT [FK_ProductCategory_Property] FOREIGN KEY ( [PropertyId] ) REFERENCES [dbo].[Property] ( [PropertyId] ),
	CONSTRAINT [FK_ProductCategory_ProductCategory] FOREIGN KEY ( [StandardCategoryId] ) REFERENCES [dbo].[ProductCategory] ( [ProductCategoryId] ),
	CONSTRAINT [FK_ProductCategory_Chain] FOREIGN KEY ( [ChainId] ) REFERENCES [dbo].[Chain] ( [ChainId] ),
	CONSTRAINT [FK_ProductCategory_Tenant] FOREIGN KEY ([TenantId]) REFERENCES [dbo].[Tenant] ([TenantId])
)
GO

CREATE INDEX 
	x_ProductCategory_PropertyId ON ProductCategory ( [PropertyId] ) 
GO 

CREATE INDEX 
	x_ProductCategory_ChainId ON [dbo].[Chain]( [ChainId] )
GO

CREATE INDEX 
	x_ProductCategory_StandardCategoryId ON ProductCategory ( [StandardCategoryId] ) 
GO

CREATE INDEX 
	x_ProductCategory_TenantId ON [dbo].[ProductCategory]( [TenantId] )
GO
