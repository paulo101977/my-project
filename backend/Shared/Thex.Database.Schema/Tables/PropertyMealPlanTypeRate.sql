﻿CREATE TABLE [dbo].[PropertyMealPlanTypeRate]
(
	[PropertyMealPlanTypeRateId] UNIQUEIDENTIFIER NOT NULL,
	[PropertyId] INT NOT NULL,
	[Date] DATETIME NOT NULL,
	[MealPlanTypeDefault] BIT NULL,
	[MealPlanTypeId] INT NOT NULL,
	[Adult_MealPlan_Amount] NUMERIC (18,4) NULL,
	[Child_1_MealPlan_Amount] NUMERIC (18,4) NULL,
	[Child_2_MealPlan_Amount] NUMERIC (18,4) NULL,
	[Child_3_MealPlan_Amount] NUMERIC (18,4) NULL,
	[CurrencyId] UNIQUEIDENTIFIER NOT NULL,
	[TenantId] UNIQUEIDENTIFIER NOT NULL,
	[PropertyMealPlanTypeRateHeaderId] UNIQUEIDENTIFIER NULL,

	[Sunday]                        BIT                 NOT NULL    DEFAULT 0,
	[Monday]                        BIT                 NOT NULL    DEFAULT 0,
	[Tuesday]                       BIT                 NOT NULL    DEFAULT 0,
	[Wednesday]                     BIT                 NOT NULL    DEFAULT 0,
	[Thursday]                      BIT                 NOT NULL    DEFAULT 0,
	[Friday]                        BIT                 NOT NULL    DEFAULT 0,
	[Saturday]                      BIT                 NOT NULL    DEFAULT 0,
	
	[IsActive]                      BIT                 NOT NULL    DEFAULT 1,
	[IsDeleted]                     BIT                 NOT NULL    DEFAULT 0,
	[CreationTime]                  DATETIME            NOT NULL    DEFAULT GETUTCDATE(),
	[CreatorUserId]                 UNIQUEIDENTIFIER    NULL,
	[LastModificationTime]          DATETIME            NULL        DEFAULT GETUTCDATE(),
	[LastModifierUserId]            UNIQUEIDENTIFIER    NULL,
	[DeletionTime]                  DATETIME            NULL,
	[DeleterUserId]                 UNIQUEIDENTIFIER    NULL,

	CONSTRAINT [PK_PropertyMealPlanTypeRate] PRIMARY KEY ([PropertyMealPlanTypeRateId]),
	CONSTRAINT [UK_PropertyMealPlanTypeRate] UNIQUE ( [PropertyId], [Date], [MealPlanTypeId], [CurrencyId], [IsActive]),
	CONSTRAINT [FK_PropertyMealPlanTypeRate_Property] FOREIGN KEY ([PropertyId]) REFERENCES [dbo].[Property] ([PropertyId]),
	CONSTRAINT [FK_PropertyMealPlanTypeRate_MealPlanType] FOREIGN KEY ([MealPlanTypeId]) REFERENCES [dbo].[MealPlanType] ([MealPlanTypeId]),
	CONSTRAINT [FK_PropertyMealPlanTypeRate_Currency] FOREIGN KEY ([CurrencyId]) REFERENCES [dbo].[Currency] ([CurrencyId]),
	CONSTRAINT [FK_PropertyMealPlanTypeRate_PropertyMealPlanTypeRateHeader] FOREIGN KEY ([PropertyMealPlanTypeRateHeaderId]) REFERENCES [dbo].[PropertyMealPlanTypeRateHeader] ([PropertyMealPlanTypeRateHeaderId]),
	CONSTRAINT [FK_PropertyMealPlanTypeRate_Tenant] FOREIGN KEY ([TenantId]) REFERENCES [dbo].[Tenant] ([TenantId]),
)
GO

CREATE INDEX
x_PropertyMealPlanTypeRate_PropertyId ON dbo.[PropertyMealPlanTypeRate] ( [PropertyId] )
GO

CREATE INDEX
x_PropertyMealPlanTypeRate_MealPlanTypeId ON dbo.[PropertyMealPlanTypeRate] ( [MealPlanTypeId] )
GO

CREATE INDEX
x_PropertyMealPlanTypeRate_CurrencyId ON dbo.[PropertyMealPlanTypeRate] ( [CurrencyId] )
GO

CREATE INDEX
x_PropertyMealPlanTypeRate_TenantId ON dbo.[PropertyMealPlanTypeRate] ( [TenantId] )
GO

CREATE INDEX
x_PropertyMealPlanTypeRate_PropertyMealPlanTypeRateHeaderId ON dbo.[PropertyMealPlanTypeRateHeader] ( [PropertyMealPlanTypeRateHeaderId] )
GO