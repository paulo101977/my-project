﻿CREATE TABLE [dbo].[PropertyBaseRateType]
(
	[PropertyBaseRateTypeId]	 INT			NOT NULL,
	[Name]						 VARCHAR(50)	NOT NULL

	CONSTRAINT [PK_PropertyBaseRateType] PRIMARY KEY ( [PropertyBaseRateTypeId] )
)
