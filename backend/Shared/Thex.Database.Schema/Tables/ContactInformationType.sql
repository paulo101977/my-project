﻿CREATE TABLE [dbo].[ContactInformationType]
(
	[ContactInformationTypeId]	INT 				NOT NULL,
	[Name]						VARCHAR (50)		NOT NULL,
	[StringFormatMask]			VARCHAR (100)		NULL,
	[RegexValidationExpression]	VARCHAR (500)		NULL,

	CONSTRAINT [PK_ContactInformationType] PRIMARY KEY ([ContactInformationTypeId]),
)