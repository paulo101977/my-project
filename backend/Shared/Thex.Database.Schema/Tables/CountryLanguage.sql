﻿CREATE TABLE [dbo].[CountryLanguage]
(
	[CountryLanguageId]				INT			NOT NULL	IDENTITY,
	[CountrySubdivisionId]			INT			NOT NULL,
	[Language]						VARCHAR(60)	NOT NULL,
	[LanguageTwoLetterIsoCode]		CHAR(2)		NULL,
	[LanguageThreeLetterIsoCode]	CHAR(3)		NULL,
	[CultureInfoCode]				VARCHAR(20)	NULL,
	CONSTRAINT [PK_CountryLanguage] PRIMARY KEY ([CountryLanguageId]),
	CONSTRAINT [FK_CountryLanguage_CountrySubdivision] FOREIGN KEY([CountrySubdivisionId]) REFERENCES [dbo].[CountrySubdivision] ([CountrySubdivisionId])
)
GO

CREATE INDEX 
	x_CountryLanguage_CountrySubdivisionId ON dbo.CountryLanguage( [CountrySubdivisionId] )
GO