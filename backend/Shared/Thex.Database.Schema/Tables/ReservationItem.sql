﻿CREATE TABLE [dbo].[ReservationItem]
(    
	[ReservationItemId]			BIGINT				NOT NULL	IDENTITY(1,1),
	[ReservationItemUid]		UNIQUEIDENTIFIER	NOT NULL,
	[ReservationId]				BIGINT				NOT NULL,
	[ReasonId]					INT					NULL,
	[EstimatedArrivalDate]		DATETIME				NOT NULL,
	[CheckInDate]				DATETIME			NULL,
	[EstimatedDepartureDate]	DATETIME				NOT NULL,
	[CheckOutDate]				DATETIME			NULL,
	[CancellationDate]			DATETIME			NULL,
	[CancellationDescription]	VARCHAR(1000)		NULL,
	[ReservationItemCode]		VARCHAR (50)		NOT NULL, 
	[RequestedRoomTypeId]		INT					NOT NULL, 
	[ReceivedRoomTypeId]		INT					NOT NULL, 
	[RoomId]					INT					NULL, 
	[AdultCount]				TINYINT				NOT NULL, 
	[ChildCount]				TINYINT				NOT NULL,
	[ReservationItemStatusId]	INT					NOT NULL,
	[RoomLayoutId]				UNIQUEIDENTIFIER	NOT NULL,
	[ExtraBedCount]				TINYINT				NOT NULL	DEFAULT 0,
	[ExtraCribCount]			TINYINT				NOT NULL	DEFAULT 0,
	[RatePlanId]				UNIQUEIDENTIFIER	NULL,
	[CurrencyId]				UNIQUEIDENTIFIER	NULL,
	[MealPlanTypeId]			INT					NULL,
	[GratuityTypeId]			INT					NULL,
	[TenantId]					UNIQUEIDENTIFIER	NOT NULL,
	[ExternalReservationNumber] VARCHAR(100)		NULL,
	[PartnerReservationNumber]	VARCHAR(100)		NULL,
	[IsMigrated]				BIT					NULL		DEFAULT 0,
	[IntegrationId]				VARCHAR(100)		NULL,

	[IsDeleted]					BIT					NOT NULL	DEFAULT 0,
	[CreationTime]				DATETIME			NOT NULL	DEFAULT GETUTCDATE(),
	[CreatorUserId]				UNIQUEIDENTIFIER	NULL,
	[LastModificationTime]		DATETIME			NULL		DEFAULT GETUTCDATE(),
	[LastModifierUserId]		UNIQUEIDENTIFIER	NULL,
	[DeletionTime]				DATETIME			NULL,
	[DeleterUserId]				UNIQUEIDENTIFIER	NULL,

	[CheckInHour] DATETIME NULL, 
    [CheckOutHour] DATETIME NULL, 
    [WalkIn] BIT NULL DEFAULT 0, 
    CONSTRAINT [PK_ReservationItem] PRIMARY KEY ([ReservationItemId]),
	CONSTRAINT [FK_ReservationItem_Reservation] FOREIGN KEY ([ReservationId]) REFERENCES [dbo].[Reservation] ([ReservationId]),
	CONSTRAINT [FK_ReservationItem_RequestedRoomType] FOREIGN KEY ([RequestedRoomTypeId]) REFERENCES [dbo].[RoomType] ([RoomTypeId]),
	CONSTRAINT [FK_ReservationItem_ReceivedRoomType] FOREIGN KEY ([ReceivedRoomTypeId]) REFERENCES [dbo].[RoomType] ([RoomTypeId]),
	CONSTRAINT [FK_RoomReservation_Room] FOREIGN KEY ([RoomId]) REFERENCES [dbo].[Room] ([RoomId]),
	CONSTRAINT [FK_RoomReservation_Status] FOREIGN KEY ([ReservationItemStatusId]) REFERENCES [dbo].[Status] ([StatusId]),
	CONSTRAINT [FK_RoomReservation_RoomLayout] FOREIGN KEY ([RoomLayoutId]) REFERENCES [dbo].[RoomLayout] ([RoomLayoutId]),
	CONSTRAINT [FK_ReservationItem_RatePlan] FOREIGN KEY ( [RatePlanId] ) REFERENCES [dbo].[RatePlan] ( [RatePlanId] ),
	CONSTRAINT [FK_ReservationItem_Currency] FOREIGN KEY ( [CurrencyId] ) REFERENCES [dbo].[Currency] ( [CurrencyId] ),
	CONSTRAINT [FK_ReservationItem_MealPlanType] FOREIGN KEY ( [MealPlanTypeId] ) REFERENCES [dbo].[MealPlanType] ( [MealPlanTypeId] ),
	CONSTRAINT [FK_ReservationItem_GratuityType] FOREIGN KEY ( [GratuityTypeId] ) REFERENCES [dbo].[GratuityType] ( [GratuityTypeId] ),
	CONSTRAINT [FK_ReservationItem_Reason] FOREIGN KEY ([ReasonId]) REFERENCES [Reason]([ReasonId]),
	CONSTRAINT [FK_ReservationItem_Tenant] FOREIGN KEY ([TenantId]) REFERENCES [dbo].[Tenant] ([TenantId])
);
GO

CREATE INDEX 
	x_ReservationItem_ReceivedRoomTypeId ON dbo.ReservationItem( [ReceivedRoomTypeId] )
GO

CREATE INDEX 
	x_ReservationItem_RequestedRoomTypeId ON dbo.ReservationItem( [RequestedRoomTypeId] )
GO

CREATE INDEX 
	x_ReservationItem_ReservationId ON dbo.ReservationItem( [ReservationId] )
GO

CREATE INDEX 
	x_ReservationItem_RoomId ON dbo.ReservationItem( [RoomId] )
GO

CREATE INDEX 
	x_ReservationItem_ReservationItemStatusId ON dbo.ReservationItem( [ReservationItemStatusId] )
GO

CREATE INDEX 
	x_ReservationItem_RoomLayoutId ON dbo.ReservationItem( [RoomLayoutId] )
GO

CREATE INDEX 
	x_ReservationItem_TenantId ON [dbo].[ReservationItem]( [TenantId] )
GO