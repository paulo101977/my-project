﻿CREATE TABLE [dbo].[CurrencyExchange]
(
	[CurrencyExchangeId]		UNIQUEIDENTIFIER	NOT NULL,
	[CurrencyExchangeDate]		DATETIME			NOT NULL,
	[CurrencyId]				UNIQUEIDENTIFIER	NOT NULL,
	[ExchangeRate]				NUMERIC(18,4)		NOT NULL,

	[IsDeleted]					BIT					NOT NULL	DEFAULT 0,
	[CreationTime]				DATETIME			NOT NULL	DEFAULT GETUTCDATE(),
	[CreatorUserId]				BIGINT				NULL,
	[LastModificationTime]		DATETIME			NULL		DEFAULT GETUTCDATE(),
	[LastModifierUserId]		BIGINT				NULL,
	[DeletionTime]				DATETIME			NULL,
	[DeleterUserId]				BIGINT				NULL,

	CONSTRAINT [PK_CurrencyExchange] PRIMARY KEY ([CurrencyExchangeId]),
	CONSTRAINT [FK_CurrencyExchange_Currency] FOREIGN KEY ([CurrencyId]) REFERENCES [dbo].[Currency] ([CurrencyId])
)
GO

CREATE INDEX 
	x_CurrencyExchange_CurrencyId ON dbo.CurrencyExchange( [CurrencyId] )
GO