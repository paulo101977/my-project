﻿CREATE TABLE [dbo].[PropertyBaseRateHeaderHistory]
(
	[PropertyBaseRateHeaderHistoryId]		UNIQUEIDENTIFIER	NOT NULL,
	[PropertyId]					INT					NOT NULL,
	[InitialDate]					DATETIME			NOT NULL,
	[FinalDate]						DATETIME			NOT NULL,
	[CurrencyId]					UNIQUEIDENTIFIER	NOT NULL,
	[MealPlanTypeDefaultId]			INT					NOT NULL,
	[RatePlanId]					UNIQUEIDENTIFIER	NULL,
	[LevelId]						UNIQUEIDENTIFIER	NULL,
	[LevelRateId]					UNIQUEIDENTIFIER	NULL,

	[Sunday]						BIT					NOT NULL	DEFAULT 0,
	[Monday]						BIT					NOT NULL	DEFAULT 0,
	[Tuesday]						BIT					NOT NULL	DEFAULT 0,
	[Wednesday]						BIT					NOT NULL	DEFAULT 0,
	[Thursday]						BIT					NOT NULL	DEFAULT 0,
	[Friday]						BIT					NOT NULL	DEFAULT 0,
	[Saturday]						BIT					NOT NULL	DEFAULT 0,
		
	[TenantId]						UNIQUEIDENTIFIER	NOT NULL,
	[IsDeleted]						BIT					NOT NULL	DEFAULT 0,
	[CreationTime]					DATETIME			NOT NULL	DEFAULT GETUTCDATE(),
	[CreatorUserId]					UNIQUEIDENTIFIER	NULL,
	[LastModificationTime]			DATETIME			NULL		DEFAULT GETUTCDATE(),
	[LastModifierUserId]			UNIQUEIDENTIFIER	NULL,
	[DeletionTime]					DATETIME			NULL,
	[DeleterUserId]					UNIQUEIDENTIFIER	NULL,
	
	CONSTRAINT [PK_PropertyBaseRateHeaderHistory] PRIMARY KEY ([PropertyBaseRateHeaderHistoryId]),
	CONSTRAINT [FK_PropertyBaseRateHeaderHistory_Property] FOREIGN KEY ([PropertyId]) REFERENCES [dbo].[Property] ([PropertyId]),
	CONSTRAINT [FK_PropertyBaseRateHeaderHistory_MealPlanType] FOREIGN KEY ([MealPlanTypeDefaultId]) REFERENCES [dbo].[MealPlanType] ([MealPlanTypeId]),
	CONSTRAINT [FK_PropertyBaseRateHeaderHistory_Currency] FOREIGN KEY ([CurrencyId]) REFERENCES [dbo].[Currency] ([CurrencyId]),
	CONSTRAINT [FK_PropertyBaseRateHeaderHistory_RatePlan] FOREIGN KEY ([RatePlanId]) REFERENCES [dbo].[RatePlan] ([RatePlanId]),
	CONSTRAINT [FK_PropertyBaseRateHeaderHistory_Tenant] FOREIGN KEY ([TenantId]) REFERENCES [dbo].[Tenant] ([TenantId]),
	CONSTRAINT [FK_PropertyBaseRateHeaderHistory_Level] FOREIGN KEY ([LevelId]) REFERENCES [dbo].[Level] ([LevelId]),
	CONSTRAINT [FK_PropertyBaseRateHeaderHistory_LevelRateHeader] FOREIGN KEY ([LevelRateId]) REFERENCES [dbo].[LevelRateHeader] ([LevelRateHeaderId]),
)
GO

CREATE INDEX 
	x_PropertyBaseRateHeaderHistory_PropertyId ON dbo.[PropertyBaseRateHeaderHistory] ( [PropertyId] )
GO

CREATE INDEX 
	x_PropertyBaseRateHeaderHistory_MealPlanTypeId ON dbo.[PropertyBaseRateHeaderHistory] ( [MealPlanTypeDefaultId] )
GO

CREATE INDEX 
	x_PropertyBaseRateHeaderHistory_CurrencyId ON dbo.[PropertyBaseRateHeaderHistory] ( [CurrencyId] )
GO

CREATE INDEX 
	x_PropertyBaseRateHeaderHistory_RatePlanId ON dbo.[PropertyBaseRateHeaderHistory] ( [RatePlanId] )
GO

CREATE INDEX 
	x_PropertyBaseRateHeaderHistory_TenantId ON [dbo].[PropertyBaseRateHeaderHistory]( [TenantId] )
GO

CREATE INDEX 
	x_PropertyBaseRateHeaderHistory_LevelId ON dbo.[Level] ( [LevelId] )
GO

CREATE INDEX 
	x_PropertyBaseRateHeaderHistory_LevelRateId ON dbo.[LevelRateHeader] ( [LevelRateHeaderId] )
GO