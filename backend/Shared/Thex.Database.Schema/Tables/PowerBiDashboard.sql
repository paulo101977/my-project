﻿CREATE TABLE [dbo].[PowerBiDashboard]
(
	[PowerBiDashboardId]						UNIQUEIDENTIFIER	NOT NULL,
	[PowerBiDashboardKey]						VARCHAR(100)		NOT NULL,
	[PowerBiDashboardDescription]				VARCHAR(200)		NULL,

	CONSTRAINT [PK_PowerBiDashboard] PRIMARY KEY ([PowerBiDashboardId])
)
