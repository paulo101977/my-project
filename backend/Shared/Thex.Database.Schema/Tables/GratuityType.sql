﻿CREATE TABLE [dbo].[GratuityType]
(
	[GratuityTypeId]		INT				NOT NULL,
	[GratuityTypeName]		VARCHAR(100)	NOT NULL,

	CONSTRAINT [PK_GratuityType] PRIMARY KEY ( [GratuityTypeId] ),
	CONSTRAINT [UK_GratuityType] UNIQUE ( [GratuityTypeName] )
)
