﻿CREATE TABLE [dbo].[PlasticBrand]
(
	[PlasticBrandId]					INT						NOT NULL	IDENTITY(1,1),
	[PlasticBrandName]					VARCHAR(100)			NOT NULL,
	[IconName]							VARCHAR(50)				NULL,

	[PlasticBrandUId] UNIQUEIDENTIFIER NULL, 
    CONSTRAINT [PK_PlasticBrand] PRIMARY KEY ( [PlasticBrandId] )
)
GO

