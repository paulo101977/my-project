﻿CREATE TABLE [dbo].[AgreementType]
(
	[AgreementTypeId]		INT				NOT NULL,
	[AgreementTypeName]		VARCHAR(100)	NOT NULL,

	CONSTRAINT [PK_AgreementType] PRIMARY KEY ( [AgreementTypeId] ),
	CONSTRAINT [UK_AgreementType] UNIQUE ( [AgreementTypeName] )
)
