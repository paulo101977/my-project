﻿CREATE TABLE [dbo].[IntegrationPartnerProperty]
(
	[IntegrationPartnerPropertyId]		UNIQUEIDENTIFIER,
	[PropertyId]						INT						NOT NULL,
	[IsActive]							BIT						NULL	   DEFAULT 1,
	[IntegrationCode] 					VARCHAR(100)			NULL,
	[PartnerId]							INT						NOT NULL,
	[IntegrationPartnerId]				INT						NULL,
	[IntegrationNumber] 				VARCHAR(100)			NULL,

	[IsDeleted]							BIT					NOT NULL	DEFAULT 0,
	[CreationTime]						DATETIME			NOT NULL	DEFAULT GETUTCDATE(),
	[CreatorUserId]						UNIQUEIDENTIFIER	NULL,
	[LastModificationTime]				DATETIME			NULL		DEFAULT GETUTCDATE(),
	[LastModifierUserId]				UNIQUEIDENTIFIER	NULL,
	[DeletionTime]						DATETIME			NULL,
	[DeleterUserId]						UNIQUEIDENTIFIER	NULL,
	
	CONSTRAINT [PK_IntegrationPartnerProperty] PRIMARY KEY ([IntegrationPartnerPropertyId]),
	CONSTRAINT [FK_IntegrationPartnerProperty_Property] FOREIGN KEY ( [PropertyId] ) REFERENCES [dbo].[Property] ( [PropertyId] ),
	CONSTRAINT [UK_IntegrationPartnerProperty_Soft_Delete] UNIQUE ( [PropertyId], [PartnerId], [IntegrationCode], [IsDeleted], [DeletionTime]),
	CONSTRAINT [FK_IntegrationPartnerProperty_Partner] FOREIGN KEY ( [PartnerId] ) REFERENCES [dbo].[Partner] ( [PartnerId] ),
	CONSTRAINT [FK_IntegrationPartnerProperty_IntegrationPartner] FOREIGN KEY ( [IntegrationPartnerId] ) REFERENCES [dbo].[IntegrationPartner] ( [IntegrationPartnerId] ),
);

GO

CREATE INDEX [x_IntegrationPartnerProperty_IntegrationPartnerId] ON [dbo].[IntegrationPartner] ([IntegrationPartnerId]);
