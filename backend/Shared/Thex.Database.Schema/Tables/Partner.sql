﻿CREATE TABLE [dbo].[Partner]
(
	[PartnerId]							INT			NOT NULL,
	[PartnerName] 						VARCHAR(100) NOT NULL,
	
	CONSTRAINT [PK_Partner] PRIMARY KEY ([PartnerId])
)
