﻿CREATE TABLE [dbo].[CountrySubdvisionService]
(
	[CountrySubdvisionServiceId] UNIQUEIDENTIFIER	NOT NULL,
	[Code]								VARCHAR(100)			NOT	NULL,
	[Description]						VARCHAR(500)			NULL,
	[CityId]								INTEGER					NOT NULL,


	CONSTRAINT [PK_CountrySubdvisionService] PRIMARY KEY ( [CountrySubdvisionServiceId] ),
	CONSTRAINT [FK_CountrySubdvisionService_CountrySubdivision] FOREIGN KEY ([CityId]) REFERENCES [dbo].[CountrySubdivision] ([CountrySubdivisionId]),
	CONSTRAINT [UK_CountrySubdvisionService] UNIQUE ([Code], [CityId])

)

GO
CREATE INDEX [x_CountrySubdvisionService_CityId] ON [dbo].[CountrySubdvisionService] ([CityId]);
