﻿CREATE TABLE [dbo].[BillingInvoice]
(
	[BillingInvoiceId]							UNIQUEIDENTIFIER	NOT NULL,
	[BillingInvoiceNumber]						VARCHAR(100)		NOT NULL,
	[BillingInvoiceSeries]						VARCHAR(5)			NOT NULL,
	[EmissionDate]								DATETIME			NOT NULL	DEFAULT GETUTCDATE(),
	[CancelDate]								DATETIME			NULL,
	[EmissionUserId]							UNIQUEIDENTIFIER				NULL,
	[CancelUserId]								UNIQUEIDENTIFIER				NULL,
	[PropertyId]								INT					NOT NULL,
	[ExternalNumber]							VARCHAR(100)		NULL,
	[ExternalSeries]							VARCHAR(5)			NULL,
	[ExternalEmissionDate]						DATETIME			NULL,
	[BillingAccountId]							UNIQUEIDENTIFIER	NOT NULL,
	[AditionalDetails]							VARCHAR(MAX)		NULL,
	[BillingInvoicePropertyId]					UNIQUEIDENTIFIER	NOT NULL,
	[BillingInvoiceCancelReasonId]				INTEGER				NULL,
	[TenantId]									UNIQUEIDENTIFIER	NOT NULL,
	[IntegratorLink]							VARCHAR(200)		NULL,
	[IntegratorXml]								VARCHAR(200)		NULL,
	[IntegratorId]								VARCHAR(50)			NULL,
	[IsDeleted]									BIT					NOT NULL	DEFAULT 0,
	[CreationTime]								DATETIME			NOT NULL	DEFAULT GETUTCDATE(),
	[CreatorUserId]								UNIQUEIDENTIFIER	NULL,
	[LastModificationTime]						DATETIME			NULL		DEFAULT GETUTCDATE(),
	[LastModifierUserId]						UNIQUEIDENTIFIER	NULL,
	[DeletionTime]								DATETIME			NULL,
	[DeleterUserId]								UNIQUEIDENTIFIER	NULL,
	[BillingInvoiceStatusId]					INT					NOT NULL DEFAULT 9,
	[ExternalCodeNumber]						VARCHAR(100)		NULL,
	[ErrorDate]									DATETIME			NULL	DEFAULT GETUTCDATE(),
	[BillingInvoiceReferenceId] UNIQUEIDENTIFIER NULL, 
	[BillingInvoiceCreditId] UNIQUEIDENTIFIER NULL, 
    [PersonHeaderId] UNIQUEIDENTIFIER NULL, 
    [BillingInvoiceTypeId] INT NULL , 
    [ExternalRps] NVARCHAR(100) NULL, 
    [IntegrationCode] VARCHAR(100) NULL, 
    CONSTRAINT [PK_BillingInvoice] PRIMARY KEY ( [BillingInvoiceId] ),
	CONSTRAINT [FK_BillingInvoice_Property] FOREIGN KEY ( [PropertyId] ) REFERENCES [dbo].[Property] ( [PropertyId] ),
	CONSTRAINT [FK_BillingInvoice_BillingAccount] FOREIGN KEY ( [BillingAccountId] ) REFERENCES [dbo].[BillingAccount] ( [BillingAccountId] ),
	CONSTRAINT [FK_BillingInvoice_Reason] FOREIGN KEY ( [BillingInvoiceCancelReasonId] ) REFERENCES [dbo].[Reason] ( [ReasonId] ),
	CONSTRAINT [FK_BillingInvoice_BillingInvoiceProperty] FOREIGN KEY ( [BillingInvoicePropertyId] ) REFERENCES [dbo].[BillingInvoiceProperty] ( [BillingInvoicePropertyId] ),
	CONSTRAINT [FK_BillingInvoice_Status] FOREIGN KEY ([BillingInvoiceStatusId]) REFERENCES [dbo].[Status] ([StatusId]),
	CONSTRAINT [FK_BillingInvoice_BillingInvoice] FOREIGN KEY ([BillingInvoiceReferenceId]) REFERENCES [dbo].[BillingInvoice] ([BillingInvoiceId]),
	CONSTRAINT [FK_BillingInvoice_BillingInvoiceCredit] FOREIGN KEY ([BillingInvoiceCreditId]) REFERENCES [dbo].[BillingInvoice] ([BillingInvoiceId]),
	CONSTRAINT [FK_BillingInvoice_Person] FOREIGN KEY ([PersonHeaderId]) REFERENCES [dbo].[Person] ([PersonId]),
	CONSTRAINT [FK_BillingInvoice_BillingInvoiceType] FOREIGN KEY ([BillingInvoiceTypeId]) REFERENCES [dbo].[BillingInvoiceType] ([BillingInvoiceTypeId]),
	CONSTRAINT [UK_BillingInvoice] UNIQUE ( [PropertyId],[BillingInvoicePropertyId], [BillingInvoiceNumber], [BillingInvoiceSeries])
)
GO

CREATE INDEX 
	x_BillingInvoice_PropertyId ON [dbo].[BillingInvoice] ( PropertyId ) 
GO

CREATE INDEX 
	x_BillingInvoice_BillingInvoicePropertyId ON [dbo].[BillingInvoice] ( BillingInvoicePropertyId ) 
GO

CREATE INDEX 
	x_BillingInvoice_BillingInvoiceStatusId ON [dbo].[BillingInvoice] ( BillingInvoiceStatusId ) 
GO

CREATE INDEX 
	x_BillingInvoice_BillingInvoiceNumber ON [dbo].[BillingInvoice] ( [BillingInvoiceNumber] ) 
GO


CREATE INDEX 
	x_BillingInvoice_EmissionDate ON [dbo].[BillingInvoice] ( [EmissionDate] ) 
GO

CREATE INDEX 
	x_BillingInvoice_CancelDate ON [dbo].[BillingInvoice] ( [CancelDate] ) 
GO

CREATE INDEX 
	x_BillingInvoice_ExternalNumber ON [dbo].[BillingInvoice] ( [PropertyId], [ExternalNumber] ) 
GO

CREATE INDEX 
	x_BillingInvoice_ExternalEmissionDate ON [dbo].[BillingInvoice] ( [ExternalEmissionDate] ) 
GO

CREATE INDEX 
	x_BillingInvoice_BillingAccountId ON [dbo].[BillingInvoice] ( [BillingAccountId] ) 
GO

CREATE INDEX
	x_BillingInvoice_BillingInvoiceCancelReasonId ON [dbo].[BillingInvoice] ([BillingInvoiceCancelReasonId])
GO

CREATE INDEX 
	x_BillingInvoice_PersonId ON [dbo].[BillingInvoice] ( [PersonHeaderId] ) 
GO

CREATE INDEX 
	x_BillingInvoice_BillingInvoiceTypeId ON [dbo].[BillingInvoice] ( [BillingInvoiceTypeId] ) 
GO