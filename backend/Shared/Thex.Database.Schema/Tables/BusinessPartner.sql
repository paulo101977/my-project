﻿CREATE TABLE [dbo].[BusinessPartner]
(
	[BusinessPartnerId]				UNIQUEIDENTIFIER	NOT NULL,
	[BusinessPartnerName]			VARCHAR(200)		NOT NULL,
	[Description]					VARCHAR(300)		NULL,
	[Url]							VARCHAR(4000)		NOT NULL,
	[CountrySubdivisionId]			INT					NOT NULL,
	[BusinessPartnerTypeId]			INT					NOT NULL,	

	[IsDeleted]						BIT					NOT NULL	DEFAULT 0,
	[CreationTime]					DATETIME			NOT NULL	DEFAULT GETUTCDATE(),
	[CreatorUserId]					UNIQUEIDENTIFIER	NULL,
	[LastModificationTime]			DATETIME			NULL		DEFAULT GETUTCDATE(),
	[LastModifierUserId]			UNIQUEIDENTIFIER	NULL,
	[DeletionTime]					DATETIME			NULL,
	[DeleterUserId]					UNIQUEIDENTIFIER	NULL,
	
	CONSTRAINT [PK_BusinessPartner] PRIMARY KEY ([BusinessPartnerId]),
	CONSTRAINT [FK_BusinessPartner_BusinessPartnerTypeId] FOREIGN KEY ([BusinessPartnerTypeId]) REFERENCES [dbo].[BusinessPartnerType] ([BusinessPartnerTypeId]),
	CONSTRAINT [FK_BusinessPartner_CountrySubdivisionId] FOREIGN KEY ([CountrySubdivisionId]) REFERENCES [dbo].[CountrySubdivision] ([CountrySubdivisionId])
)
GO

CREATE INDEX 
	x_BusinessPartner_BusinessPartnerTypeId ON [dbo].[BusinessPartner] ( [BusinessPartnerTypeId] )
GO

CREATE INDEX 
	x_BusinessPartner_CountrySubdivisionId ON [dbo].[BusinessPartner] ( [CountrySubdivisionId] )
GO