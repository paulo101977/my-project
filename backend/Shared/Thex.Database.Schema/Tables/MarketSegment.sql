﻿CREATE TABLE [dbo].[MarketSegment]
(
	[MarketSegmentId]			INT					NOT NULL	IDENTITY , 
    [Name]						VARCHAR(50)			NOT NULL, 
    [IsActive]					BIT					NOT NULL,
	[ParentMarketSegmentId]		INT					NULL,
	[Code]						VARCHAR(20)			NOT NULL,
	[SegmentAbbreviation]		VARCHAR(30)			NOT NULL,
	
	[IsDeleted]					BIT					NOT NULL	DEFAULT 0,
	[CreationTime]				DATETIME			NOT NULL	DEFAULT GETUTCDATE(),
	[CreatorUserId]				UNIQUEIDENTIFIER	NULL,
	[LastModificationTime]		DATETIME			NULL		DEFAULT GETUTCDATE(),
	[LastModifierUserId]		UNIQUEIDENTIFIER	NULL,
	[DeletionTime]				DATETIME			NULL,
	[DeleterUserId]				UNIQUEIDENTIFIER	NULL,

	CONSTRAINT [PK_MarketSegment] PRIMARY KEY ([MarketSegmentId]),
	CONSTRAINT [FK_MarketSegment_MarketSegment] FOREIGN KEY ([ParentMarketSegmentId]) REFERENCES [MarketSegment]([MarketSegmentId])
)
GO

CREATE INDEX 
	x_MarketSegment_ParentMarketSegmentId ON dbo.[MarketSegment]( [ParentMarketSegmentId] )
GO