﻿CREATE TABLE [dbo].[BusinessSource]
(
	[BusinessSourceId]	INT 			NOT NULL	IDENTITY, 
	[Description]		VARCHAR(50)		NOT NULL,
	[Code]                 VARCHAR(20)     NOT NULL,
	[IsActive]			     BIT				NOT NULL	DEFAULT 1,	
	
	[IsDeleted]					BIT					NOT NULL	DEFAULT 0,
	[CreationTime]				DATETIME			NOT NULL	DEFAULT GETUTCDATE(),
	[CreatorUserId]				UNIQUEIDENTIFIER	NULL,
	[LastModificationTime]		DATETIME			NULL		DEFAULT GETUTCDATE(),
	[LastModifierUserId]		UNIQUEIDENTIFIER	NULL,
	[DeletionTime]				DATETIME			NULL,
	[DeleterUserId]				UNIQUEIDENTIFIER	NULL,

	CONSTRAINT [PK_BusinessSource] PRIMARY KEY ([BusinessSourceId])
)
