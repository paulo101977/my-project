﻿CREATE TABLE [dbo].[HousekeepingStatus]
(
	[HousekeepingStatusId]		INT				NOT NULL,
	[StatusName]				VARCHAR(100)	NOT NULL,
	[DefaultColor]				VARCHAR(6)		NOT NULL,


	CONSTRAINT [PK_HousekeepingStatus] PRIMARY KEY ( [HousekeepingStatusId] )
)
