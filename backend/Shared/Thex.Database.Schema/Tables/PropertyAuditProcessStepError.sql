﻿CREATE TABLE [dbo].[PropertyAuditProcessStepError]
(
	[PropertyAuditProcessStepErrorId] UNIQUEIDENTIFIER NOT NULL PRIMARY KEY,
	[PropertyAuditProcessStepId] UNIQUEIDENTIFIER	NOT NULL,
	[Description]				varchar(2000)	NULL,
	[ReservationItemId]			BIGINT			NULL,
	[ReservationItemCode]		VARCHAR (50)	NULL,
	[ReservationItemStatusId]	INT				NULL,
	[ReservationItemStatusName]	varchar(20)		NULL,
	[EstimatedArrivalDate]		DATETIME		NULL,
	[EstimatedDepartureDate]	DATETIME		NULL,
	[ReceivedRoomTypeId]		INT				NULL,
	[RoomTypeName]				VARCHAR (100)	NULL,
	[RoomId]					INT				NULL, 
	[RoomNumber]				VARCHAR (10)	NULL,
	[GuestName]					VARCHAR (500)	NULL, 
	[HousekeepingStatusId]		INT				NULL,
	[HousekeepingStatusName]	VARCHAR (100)	NULL,
	[HousekeepingStatusColor]	VARCHAR (20)	NULL,
	[HousekeepingStatusPropertyId]		UNIQUEIDENTIFIER	NULL,
	[HousekeepingStatusPropertyName]	varchar(2000)	NULL,


	[PropertyId]			INT					NOT NULL,
	[TenantId]									UNIQUEIDENTIFIER		NOT NULL,
	[IsDeleted]									BIT					NOT NULL	DEFAULT 0,
	[CreationTime]								DATETIME			NOT NULL	DEFAULT GETUTCDATE(),
	[CreatorUserId]								UNIQUEIDENTIFIER	NULL,
	[LastModificationTime]						DATETIME			NULL		DEFAULT GETUTCDATE(),
	[LastModifierUserId]						UNIQUEIDENTIFIER	NULL,
	[DeletionTime]								DATETIME			NULL,
	[DeleterUserId]								UNIQUEIDENTIFIER	NULL,

	CONSTRAINT [FK_PropertyAuditProcessStepError_PropertyAuditProcessStep] FOREIGN KEY ([PropertyAuditProcessStepId]) REFERENCES [dbo].[PropertyAuditProcessStep] ([PropertyAuditProcessStepId]),
	CONSTRAINT [FK_PropertyAuditProcessStepError_ReservationItem] FOREIGN KEY ([ReservationItemId]) REFERENCES [dbo].[ReservationItem] ([ReservationItemId]),
	CONSTRAINT [FK_PropertyAuditProcessStepError_ReservationItemStatus] FOREIGN KEY ([ReservationItemStatusId]) REFERENCES [dbo].[Status] ([StatusId]),
	CONSTRAINT [FK_PropertyAuditProcessStepError_RoomType] FOREIGN KEY ([ReceivedRoomTypeId]) REFERENCES [dbo].[RoomType] ([RoomTypeId]),
	CONSTRAINT [FK_PropertyAuditProcessStepError_Room] FOREIGN KEY ([RoomId]) REFERENCES [dbo].[Room] ([RoomId]),
	CONSTRAINT [FK_PropertyAuditProcessStepError_HousekeepingStatus] FOREIGN KEY ([HousekeepingStatusId]) REFERENCES [dbo].[HousekeepingStatus] ([HousekeepingStatusId]),
	CONSTRAINT [FK_PropertyAuditProcessStepError_HousekeepingStatusProperty] FOREIGN KEY ([HousekeepingStatusPropertyId]) REFERENCES [dbo].[HousekeepingStatusProperty] ([HousekeepingStatusPropertyId]),
	CONSTRAINT [FK_PropertyAuditProcessStepError_Property] FOREIGN KEY ([PropertyId]) REFERENCES [dbo].[Property] ([PropertyId]),
	CONSTRAINT [FK_PropertyAuditProcessStepError_Tenant] FOREIGN KEY ([TenantId]) REFERENCES [dbo].[Tenant] ([TenantId])
)
GO

CREATE INDEX 
	x_PropertyAuditProcessStepError_PropertyAuditProcessStepId ON [dbo].[PropertyAuditProcessStepError] ( [PropertyAuditProcessStepId] ) 
GO

CREATE INDEX 
	x_PropertyAuditProcessStepError_ReservationItemId ON [dbo].[PropertyAuditProcessStepError] ( [ReservationItemId] ) 
GO

CREATE INDEX 
	x_PropertyAuditProcessStepError_ReservationItemStatusId ON [dbo].[PropertyAuditProcessStepError] ( [ReservationItemStatusId] ) 
GO

CREATE INDEX 
	x_PropertyAuditProcessStepError_ReceivedRoomTypeId ON [dbo].[PropertyAuditProcessStepError] ( [ReceivedRoomTypeId] ) 
GO

CREATE INDEX 
	x_PropertyAuditProcessStepError_RoomId ON [dbo].[PropertyAuditProcessStepError] ( [RoomId] ) 
GO

CREATE INDEX 
	x_PropertyAuditProcessStepError_HousekeepingStatusId ON [dbo].[PropertyAuditProcessStepError] ( [HousekeepingStatusId] ) 
GO

CREATE INDEX 
	x_PropertyAuditProcessStepError_HousekeepingStatusPropertyId ON [dbo].[PropertyAuditProcessStepError] ( [HousekeepingStatusPropertyId] ) 
GO

CREATE INDEX 
	x_PropertyAuditProcessStepError_PropertyId ON [dbo].[PropertyAuditProcessStepError] ( [PropertyId] ) 
GO

CREATE INDEX 
	x_PropertyAuditProcessStepError_TenantId ON [dbo].[PropertyAuditProcessStepError] ( [TenantId] ) 
GO