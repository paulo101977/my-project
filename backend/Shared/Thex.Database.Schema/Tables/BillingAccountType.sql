﻿CREATE TABLE [dbo].[BillingAccountType]
(
	 [BillingAccountTypeId]			INTEGER				NOT NULL , 
     [TypeName]						VARCHAR(100)		NOT NULL,

     CONSTRAINT [PK_BillingAccountType] PRIMARY KEY ( [BillingAccountTypeId] )
)
GO
