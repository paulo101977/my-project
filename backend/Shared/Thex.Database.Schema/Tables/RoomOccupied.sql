﻿CREATE TABLE [dbo].RoomOccupied
(
    [Id]     					UNIQUEIDENTIFIER	NOT NULL,
    [RoomId] 					INT	                NOT NULL,
	[ReservationId]				BIGINT              NOT NULL,
	[PropertyId]			    INT		            NOT NULL,
    [TenantId]					UNIQUEIDENTIFIER	NOT NULL,
	[IsDeleted]					BIT					NOT NULL	DEFAULT 0,
	[CreationTime]				DATETIME			NOT NULL	DEFAULT GETUTCDATE(),
	[CreatorUserId]				UNIQUEIDENTIFIER	NULL,
	[LastModificationTime]		DATETIME			NULL		DEFAULT GETUTCDATE(),
	[LastModifierUserId]		UNIQUEIDENTIFIER	NULL,
	[DeletionTime]				DATETIME			NULL,
	[DeleterUserId]				UNIQUEIDENTIFIER	NULL,

	CONSTRAINT [PK_Id] PRIMARY KEY ([Id]),	
	CONSTRAINT [FK_RoomRoomOccupied_ReservationId] FOREIGN KEY ([ReservationId]) REFERENCES [dbo].[Reservation]([ReservationId]),
	CONSTRAINT [UK_RoomId_RoomOccupied] UNIQUE ( [RoomId], [PropertyId], [IsDeleted], [DeletionTime] ),	
	CONSTRAINT [FK_RoomOccupied_PropertyId] FOREIGN KEY ([PropertyId]) REFERENCES [dbo].[Property]([PropertyId]),
	CONSTRAINT [FK_RoomOccupied_Tenant] FOREIGN KEY ([TenantId]) REFERENCES [dbo].[Tenant] ([TenantId])
);
GO

CREATE INDEX 
	x_RoomOccupied_RoomId ON dbo.RoomOccupied( [RoomId] )
GO

CREATE INDEX 
	x_RoomOccupied_PropertyId ON dbo.Property( [PropertyId] )
GO

CREATE INDEX 
	x_RoomOccupied_TenantId ON [dbo].[RoomOccupied]( [TenantId] )
GO