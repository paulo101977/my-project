﻿CREATE TABLE [dbo].[BillingAccountItemDetail]
(
	[BillingAccountItemDetailId]						UNIQUEIDENTIFIER		NOT NULL,
	[BillingAccountItemId]							    UNIQUEIDENTIFIER		NOT NULL,
	[Amount]											NUMERIC (18,4)			NOT NULL,
    [Quantity]											NUMERIC(18, 4) NOT NULL, 
    [ProductId]											UNIQUEIDENTIFIER NOT NULL, 
    CONSTRAINT [PK_BillingAccountItemDetail] PRIMARY KEY ( [BillingAccountItemDetailId] ),
	CONSTRAINT [FK_BillingAccountItemDetail_BillingAccountItem] FOREIGN KEY ( [BillingAccountItemId] ) REFERENCES [dbo].[BillingAccountItem] ( [BillingAccountItemId] ),
	CONSTRAINT [FK_BillingAccountItem_Product] FOREIGN KEY ( [ProductId] ) REFERENCES [dbo].[Product] ( [ProductId] )
)
GO

CREATE INDEX 
	x_BillingAccountItemDetail_BillingAccountItemDetailId ON BillingAccountItemDetail ( [BillingAccountItemDetailId] ) 
GO 

CREATE INDEX 
	x_BillingAccountItemDetail_BillingAccountItemId ON BillingAccountItemDetail ( [BillingAccountItemId] ) 
GO

CREATE INDEX 
	x_BillingAccountItemDetail_ProductId ON BillingAccountItemDetail ( [ProductId] ) 
GO