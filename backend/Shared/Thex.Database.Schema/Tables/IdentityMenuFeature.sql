﻿CREATE TABLE [dbo].[IdentityMenuFeature](
	[IdentityMenuFeatureId]			UNIQUEIDENTIFIER	NOT NULL,
	[IdentityProductId]				INT					NOT NULL,
	[IdentityModuleId]				INT					NOT NULL,
	[IdentityFeatureParentId]		UNIQUEIDENTIFIER	NULL,
	[IdentityFeatureId]				UNIQUEIDENTIFIER	NOT NULL,
	[IsActive]						BIT					NOT NULL	DEFAULT 1,
	[OrderNumber]					INT					NOT NULL,
	[Level]							INT					NOT NULL,
	[Description]					VARCHAR(400)		NULL,

	[IsDeleted]					BIT					NOT NULL	DEFAULT 0,
	[CreationTime]				DATETIME			NOT NULL	DEFAULT GETUTCDATE(),
	[CreatorUserId]				UNIQUEIDENTIFIER	NULL,
	[LastModificationTime]		DATETIME			NULL		DEFAULT GETUTCDATE(),
	[LastModifierUserId]		UNIQUEIDENTIFIER	NULL,
	[DeletionTime]				DATETIME			NULL,
	[DeleterUserId]				UNIQUEIDENTIFIER	NULL,
	
    CONSTRAINT [PK_IdentityMenuFeature] PRIMARY KEY ([IdentityMenuFeatureId]),
	CONSTRAINT [FK_IdentityMenuFeature_IdentityProduct] FOREIGN KEY ([IdentityProductId]) REFERENCES [dbo].[IdentityProduct] ([IdentityProductId]),
	CONSTRAINT [FK_IdentityMenuFeature_IdentityModule] FOREIGN KEY ([IdentityModuleId]) REFERENCES [dbo].[IdentityModule] ([IdentityModuleId]),
	CONSTRAINT [FK_IdentityMenuFeature_IdentityFeatureParent] FOREIGN KEY ([IdentityFeatureParentId]) REFERENCES [dbo].[IdentityFeature] ([IdentityFeatureId]),
	CONSTRAINT [FK_IdentityMenuFeature_IdentityFeature] FOREIGN KEY ([IdentityFeatureId]) REFERENCES [dbo].[IdentityFeature] ([IdentityFeatureId])
)