﻿CREATE TABLE [dbo].[BillingItemPaymentCondition]
(
	[BillingItemPaymentConditionId]		UNIQUEIDENTIFIER				NOT NULL,
	[BillingItemId]						INT								NOT NULL,
	[InstallmentNumber]					SMALLINT						NOT NULL,
	[CommissionPct]						NUMERIC(18, 2)					NOT NULL,
	[PaymentDeadline]					SMALLINT						NOT NULL,
	[TenantId]							UNIQUEIDENTIFIER	NOT NULL,

	[IsDeleted]							BIT					NOT NULL	DEFAULT 0,
	[CreationTime]						DATETIME			NOT NULL	DEFAULT GETUTCDATE(),
	[CreatorUserId]						UNIQUEIDENTIFIER	NULL,
	[LastModificationTime]				DATETIME			NULL		DEFAULT GETUTCDATE(),
	[LastModifierUserId]				UNIQUEIDENTIFIER	NULL,
	[DeletionTime]						DATETIME			NULL,
	[DeleterUserId]						UNIQUEIDENTIFIER	NULL,

	CONSTRAINT [PK_BillingItemPaymentCondition] PRIMARY KEY ( [BillingItemPaymentConditionId] ),
	CONSTRAINT [UK_BillingItemPaymentCondition] UNIQUE ( [BillingItemId], [InstallmentNumber]),
	CONSTRAINT [FK_BillingItemPaymentCondition_BillingItem] FOREIGN KEY ( [BillingItemId] ) REFERENCES [dbo].[BillingItem] ( [BillingItemId] ),
	CONSTRAINT [FK_BillingItemPaymentCondition_Tenant] FOREIGN KEY ([TenantId]) REFERENCES [dbo].[Tenant] ([TenantId])
)
GO

CREATE INDEX 
	x_BillingItemPaymentCondition_BillingItemId_InstallmentNumber ON [dbo].[BillingItemPaymentCondition] ( [BillingItemId], [InstallmentNumber] )
GO

CREATE INDEX 
	x_BillingItemPaymentCondition_TenantId ON [dbo].[BillingItemPaymentCondition]( [TenantId] )
GO