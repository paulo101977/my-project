﻿CREATE TABLE [dbo].[Plastic]
(
	[PlasticId]						BIGINT						NOT NULL	IDENTITY,
	[PersonId]						UNIQUEIDENTIFIER			NULL,
	[Holder]						VARCHAR(150)				NOT NULL,
	[PlasticNumber]					VARCHAR(20)					NOT NULL,
	[ExpirationDate]				VARCHAR(7)					NOT NULL,
	[CSC]							SMALLINT					NOT NULL,
	[PlasticBrandId]				INT							NOT NULL,

	CONSTRAINT [PK_Plastic] PRIMARY KEY ([PlasticId]),
	CONSTRAINT [FK_Plastic_PlasticBrand] FOREIGN KEY ( [PlasticBrandId] ) REFERENCES [dbo].[PlasticBrand] ( [PlasticBrandId] )
);
GO

CREATE INDEX 
	x_Plastic_PlasticBrandId ON Plastic ( [PlasticBrandId] )
GO

