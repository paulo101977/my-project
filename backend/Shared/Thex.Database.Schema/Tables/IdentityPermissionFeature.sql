﻿CREATE TABLE [dbo].[IdentityPermissionFeature](
	[IdentityPermissionFeatureId]	UNIQUEIDENTIFIER	NOT NULL,
	[IdentityPermissionId]			UNIQUEIDENTIFIER	NOT NULL,
	[IdentityFeatureId]				UNIQUEIDENTIFIER	NOT NULL,
	[IsActive]						BIT					NOT NULL	DEFAULT 1,
	[IsDeleted]					BIT					NOT NULL	DEFAULT 0,
	[CreationTime]				DATETIME			NOT NULL	DEFAULT GETUTCDATE(),
	[CreatorUserId]				UNIQUEIDENTIFIER	NULL,
	[LastModificationTime]		DATETIME			NULL		DEFAULT GETUTCDATE(),
	[LastModifierUserId]		UNIQUEIDENTIFIER	NULL,
	[DeletionTime]				DATETIME			NULL,
	[DeleterUserId]				UNIQUEIDENTIFIER	NULL,
	
    CONSTRAINT [PK_IdentityPermissionFeature] PRIMARY KEY ([IdentityPermissionFeatureId]),
	CONSTRAINT [FK_IdentityPermissionFeature_IdentityPermission] FOREIGN KEY ([IdentityPermissionId]) REFERENCES [dbo].[IdentityPermission] ([IdentityPermissionId]),
	CONSTRAINT [FK_IdentityPermissionFeature_IdentityFeature] FOREIGN KEY ([IdentityFeatureId]) REFERENCES [dbo].[IdentityFeature] ([IdentityFeatureId])
)