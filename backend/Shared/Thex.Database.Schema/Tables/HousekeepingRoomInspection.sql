﻿CREATE TABLE [dbo].[HousekeepingRoomInspection]
(
	[HousekeepingRoomInspectionId]	INT	NOT NULL,
	[HousekeepingRoomInspectionName] VARCHAR(20) NOT NULL,
	CONSTRAINT [PK_[HousekeepingRoomInspection] PRIMARY KEY ( [HousekeepingRoomInspectionId] )
)
GO


CREATE INDEX 
	x_HousekeepingRoomInspection_HousekeepingRoomInspectionId ON [HousekeepingRoomInspection] ( [HousekeepingRoomInspectionId] )
GO