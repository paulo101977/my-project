﻿CREATE TABLE [dbo].[PropertyRateStrategyRoomType]
(
	[PropertyRateStrategyRoomTypeId] UNIQUEIDENTIFIER NOT NULL, 
    [PropertyRateStrategyId] UNIQUEIDENTIFIER NOT NULL, 
    [RoomTypeId] INT NOT NULL, 
    [MinOccupationPercentual] NUMERIC(4, 1) NOT NULL, 
    [MaxOccupationPercentual] NUMERIC(4, 1) NOT NULL, 
    [IsDecreased] BIT NOT NULL, 
    [PercentualAmount] NUMERIC(16, 2) NULL, 
    [IncrementAmount] NUMERIC(16, 2) NULL,
	[IsDeleted]					BIT					NOT NULL	DEFAULT 0,
	[CreationTime]				DATETIME			NOT NULL	DEFAULT GETUTCDATE(),
	[CreatorUserId]				UNIQUEIDENTIFIER	NULL,
	[LastModificationTime]		DATETIME			NULL		DEFAULT GETUTCDATE(),
	[LastModifierUserId]		UNIQUEIDENTIFIER	NULL,
	[DeletionTime]				DATETIME			NULL,
	[DeleterUserId]				UNIQUEIDENTIFIER	NULL,
	[TenantId]						UNIQUEIDENTIFIER		NOT NULL,

	CONSTRAINT [PK_PropertyRateStrategyRoomType] PRIMARY KEY ( [PropertyRateStrategyRoomTypeId] ),
	CONSTRAINT [FK_PropertyRateStrategyRoomType_PropertyRateStrategy] FOREIGN KEY ( [PropertyRateStrategyId] ) REFERENCES [dbo].[PropertyRateStrategy] ( [PropertyRateStrategyId] ),
	CONSTRAINT [FK_PropertyRateStrategyRoomType_RoomType] FOREIGN KEY ( [RoomTypeId] ) REFERENCES [dbo].[RoomType] ( [RoomTypeId] ),
	CONSTRAINT [FK_PropertyRateStrategyRoomType_Tenant] FOREIGN KEY ([TenantId]) REFERENCES [dbo].[Tenant] ([TenantId])
)
GO

CREATE INDEX 
	x_PropertyRateStrategyRoomType_PropertyRateStrategy ON dbo.[PropertyRateStrategyRoomType] ( [PropertyRateStrategyId] )
GO
CREATE INDEX 
	x_PropertyRateStrategyRoomType_RoomType ON dbo.[PropertyRateStrategyRoomType] ( [RoomTypeId] )
GO
CREATE INDEX 
	x_PropertyRateStrategyRoomType_TenantId ON [dbo].[PropertyRateStrategy]( [TenantId] )
GO