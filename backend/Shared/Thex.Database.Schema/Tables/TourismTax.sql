﻿CREATE TABLE [dbo].[TourismTax]
(
	[TourismTaxId]		UNIQUEIDENTIFIER				NOT NULL,
	[PropertyId]		INT	NOT NULL,
	[IsActive] BIT NOT NULL, 
    [BillingItemId] INT NOT NULL, 
    [Amount] NUMERIC(18, 2) NOT NULL, 
    [NumberOfNights] INT NOT NULL, 
    [IsTotalOfNights] BIT NOT NULL, 
    [IsIntegratedFiscalDocument] BIT NOT NULL, 
    [LaunchType] INT NULL, 

	[TenantId]						UNIQUEIDENTIFIER		NOT NULL,
	[IsDeleted]						BIT						NOT NULL	DEFAULT 0,
	[CreationTime]					DATETIME				NOT NULL	DEFAULT GETUTCDATE(),
	[CreatorUserId]					UNIQUEIDENTIFIER		NULL,
	[LastModificationTime]			DATETIME				NULL		DEFAULT GETUTCDATE(),
	[LastModifierUserId]			UNIQUEIDENTIFIER		NULL,
	[DeletionTime]					DATETIME				NULL,
	[DeleterUserId]					UNIQUEIDENTIFIER		NULL,

    CONSTRAINT [PK_TourismTax] PRIMARY KEY ([TourismTaxId]),
	CONSTRAINT [UK_TourismTax_Property] UNIQUE ([PropertyId]),
	CONSTRAINT [FK_TourismTax_Property] FOREIGN KEY ([PropertyId]) REFERENCES [dbo].[Property] ([PropertyId]),
	CONSTRAINT [FK_TourismTax_BillingItem] FOREIGN KEY ([BillingItemId]) REFERENCES [dbo].[BillingItem] ([BillingItemId]),
	CONSTRAINT [FK_TourismTax_Tenant] FOREIGN KEY ([TenantId]) REFERENCES [dbo].[Tenant] ([TenantId]),
)
GO

CREATE INDEX 
	[x_TourismTax_TenantId] ON [dbo].[TourismTax]([TenantId])
GO

CREATE INDEX [x_TourismTax_PropertyId] ON [dbo].[TourismTax] ([PropertyId]);
GO