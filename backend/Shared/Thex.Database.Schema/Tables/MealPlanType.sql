﻿CREATE TABLE [dbo].[MealPlanType]
(
	[MealPlanTypeId]		INT				NOT NULL,
	[MealPlanTypeCode]		VARCHAR(10)		NULL,
	[MealPlanTypeName]		VARCHAR(100)	NOT NULL,

	CONSTRAINT [PK_MealPlanType] PRIMARY KEY ([MealPlanTypeId]),
	CONSTRAINT [UK_MealPlanType_1] UNIQUE ( [MealPlanTypeCode]),
	CONSTRAINT [UK_MealPlanType_2] UNIQUE ( [MealPlanTypeName])
)
GO
