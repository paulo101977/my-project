﻿CREATE TABLE [dbo].[PropertyCompanyClientCategory]
(
	[PropertyCompanyClientCategoryId]		UNIQUEIDENTIFIER	NOT NULL,
	[PropertyId]							INT					NOT NULL,
	[PropertyCompanyClientCategoryName]		VARCHAR(100)		NOT NULL,
	[IsActive]				BIT							NOT NULL,
	[TenantId]					UNIQUEIDENTIFIER	NOT NULL,

	[IsDeleted]					BIT					NOT NULL	DEFAULT 0,
	[CreationTime]				DATETIME			NOT NULL	DEFAULT GETUTCDATE(),
	[CreatorUserId]				UNIQUEIDENTIFIER	NULL,
	[LastModificationTime]		DATETIME			NULL		DEFAULT GETUTCDATE(),
	[LastModifierUserId]		UNIQUEIDENTIFIER	NULL,
	[DeletionTime]				DATETIME			NULL,
	[DeleterUserId]				UNIQUEIDENTIFIER	NULL,

	CONSTRAINT [PK_PropertyCompanyClientCategory] PRIMARY KEY ([PropertyCompanyClientCategoryId]),
	CONSTRAINT [UK_PropertyCompanyClientCategory] UNIQUE ( [PropertyId], [PropertyCompanyClientCategoryName], [IsDeleted], [DeletionTime] ),
	CONSTRAINT [FK_PropertyCompanyClientCategory_Property] FOREIGN KEY ([PropertyId]) REFERENCES [dbo].[Property] ([PropertyId]),
	CONSTRAINT [FK_PropertyCompanyClientCategory_Tenant] FOREIGN KEY ([TenantId]) REFERENCES [dbo].[Tenant] ([TenantId])
)
GO

CREATE INDEX 
	x_PropertyCompanyClientCategory_PropertyId ON dbo.[PropertyCompanyClientCategory] ( [PropertyId] )
GO

CREATE INDEX 
	x_PropertyCompanyClientCategory_TenantId ON [dbo].[PropertyCompanyClientCategory]( [TenantId] )
GO