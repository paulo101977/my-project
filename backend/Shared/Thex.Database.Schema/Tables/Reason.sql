﻿CREATE TABLE [dbo].[Reason]
(
	[ReasonId]				INT							NOT NULL  IDENTITY,
	[ReasonName]			VARCHAR(100)				NOT NULL,
	[ReasonCategoryId]		INT							NOT NULL,
	[IsActive]				BIT							NOT NULL,
	[ChainId]				INT							NOT NULL,
	[HigsCode]				INT							NULL, 

	CONSTRAINT [PK_Reason] PRIMARY KEY ([ReasonId]), 
	CONSTRAINT [FK_Reason_ReasonCategory] FOREIGN KEY ([ReasonCategoryId]) REFERENCES [ReasonCategory]([ReasonCategoryId]),
	CONSTRAINT [FK_Reason_Chain] FOREIGN KEY ([ChainId]) REFERENCES [Chain]([ChainId])
);
GO

CREATE INDEX 
	x_Reason_ReasonCategoryId ON dbo.Reason( [ReasonCategoryId] )
GO

CREATE INDEX 
	x_Reason_ChainId ON dbo.Reason( [ChainId] )
GO
