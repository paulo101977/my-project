﻿CREATE TABLE [dbo].[Reservation]
(
	[ReservationId]						BIGINT				NOT NULL	IDENTITY(1,1),
	[ReservationUid]					UNIQUEIDENTIFIER	NOT NULL,
	[ReservationCode]					VARCHAR(25)			NOT NULL,
	[ReservationVendorCode]				VARCHAR(20)			NULL,
	[PropertyId]						INT					NOT NULL,
	[ContactName]						VARCHAR(100)		NULL,
	[ContactEmail]						VARCHAR(100)		NULL,
	[ContactPhone]						VARCHAR(50)			NULL,
	[InternalComments]					VARCHAR(4000)		NULL,
	[ExternalComments]					VARCHAR(4000)		NULL,
	[PartnerComments]					VARCHAR(4000)		NULL,
	[GroupName]							VARCHAR(50)			NULL,
	[Deadline]							DATE				NULL,
	[UnifyAccounts]						BIT		 			NOT NULL	DEFAULT 0,
	[BusinessSourceId]					INT					NULL,
	[MarketSegmentId]					INT					NULL,
	[CompanyClientId]					UNIQUEIDENTIFIER	NULL,
	[CompanyClientContactPersonId]		UNIQUEIDENTIFIER	NULL,
	[RatePlanId]						UNIQUEIDENTIFIER	NULL,
	[TenantId]							UNIQUEIDENTIFIER	NOT NULL,

	[IsDeleted]							BIT					NOT NULL	DEFAULT 0,
	[CreationTime]						DATETIME			NOT NULL	DEFAULT GETUTCDATE(),
	[CreatorUserId]						UNIQUEIDENTIFIER	NULL,
	[LastModificationTime]				DATETIME			NULL		DEFAULT GETUTCDATE(),
	[LastModifierUserId]				UNIQUEIDENTIFIER	NULL,
	[DeletionTime]						DATETIME			NULL,
	[DeleterUserId]						UNIQUEIDENTIFIER	NULL,

	CONSTRAINT [PK_Reservation] PRIMARY KEY ([ReservationId]), 
	CONSTRAINT [AK_Reservation_Uid] UNIQUE ([ReservationUid]),
	CONSTRAINT [FK_Reservation_Property] FOREIGN KEY ([PropertyId]) REFERENCES [Property]([PropertyId]),
	CONSTRAINT [FK_Reservation_BusinessSource] FOREIGN KEY ([BusinessSourceId]) REFERENCES [BusinessSource]([BusinessSourceId]),
	CONSTRAINT [FK_Reservation_MarketSegment] FOREIGN KEY ([MarketSegmentId]) REFERENCES [MarketSegment]([MarketSegmentId]),
	CONSTRAINT [FK_Reservation_CompanyClient] FOREIGN KEY ([CompanyClientId]) REFERENCES [CompanyClient]([CompanyClientId]),
	CONSTRAINT [FK_Reservation_Person] FOREIGN KEY ([CompanyClientContactPersonId]) REFERENCES [Person]([PersonId]),
	CONSTRAINT [FK_eservation_RatePlan] FOREIGN KEY ( [RatePlanId] ) REFERENCES [dbo].[RatePlan] ( [RatePlanId] ),
	CONSTRAINT [FK_Reservation_Tenant] FOREIGN KEY ([TenantId]) REFERENCES [dbo].[Tenant] ([TenantId])
);
GO

CREATE INDEX 
	x_Reservation_PropertyId ON dbo.Reservation( [PropertyId] )
GO

CREATE INDEX 
	x_Reservation_BusinessSourceId ON dbo.Reservation( [BusinessSourceId] )
GO

CREATE INDEX 
	x_Reservation_MarketSegmentId ON dbo.Reservation( [MarketSegmentId] )
GO

CREATE INDEX 
	x_Reservation_CompanyClientContactPersonId ON dbo.Reservation( [CompanyClientContactPersonId] )
GO

CREATE INDEX 
	x_Reservation_CompanyClientId ON dbo.Reservation( [CompanyClientId] )
GO

CREATE INDEX 
	x_Reservation_RatePlanId ON dbo.Reservation( [RatePlanId] )
GO
GO

CREATE INDEX 
	x_Reservation_TenantId ON [dbo].[Reservation]( [TenantId] )
GO

CREATE INDEX 
	x_Reservation_MarketSegmentId_And_PropertyId ON [dbo].[Reservation]( [MarketSegmentId], [PropertyId])
GO

CREATE INDEX 
	x_Reservation_BusinessSourceId_And_PropertyId ON [dbo].[Reservation]( [BusinessSourceId], [PropertyId])
GO