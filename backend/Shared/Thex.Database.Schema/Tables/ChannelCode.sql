﻿CREATE TABLE [dbo].[ChannelCode]
(
	[ChannelCodeId]						INT				NOT NULL		IDENTITY(1,1),
	[ChannelCode]						VARCHAR(500)	NULL,
	[ChannelCodeDescription]			VARCHAR(500)	NULL,

	CONSTRAINT [PK_ChannelCode] PRIMARY KEY ([ChannelCodeId])
)
GO
