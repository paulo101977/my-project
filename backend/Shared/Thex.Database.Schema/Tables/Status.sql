﻿CREATE TABLE [dbo].[Status]
(
	[StatusId]			INT				NOT NULL,
	[StatusCategoryId]	INT				NOT NULL,
	[Name]				VARCHAR (20)	NOT NULL,

	CONSTRAINT [PK_Status] PRIMARY KEY ([StatusId]),
	CONSTRAINT [FK_Status_StatusCategory] FOREIGN KEY ([StatusCategoryId]) REFERENCES [dbo].[StatusCategory] ([StatusCategoryId])
);
GO

CREATE INDEX 
	x_Status_StatusCategoryId ON dbo.Status( [StatusCategoryId] )
GO
