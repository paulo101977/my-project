﻿CREATE TABLE [dbo].[UserTokens](
	[UserId]			UNIQUEIDENTIFIER		NOT NULL,
	[LoginProvider]		[nvarchar](128)			NOT NULL,
	[Name]				[nvarchar](128)			NOT NULL,
	[Value]				[nvarchar](max)			NULL,
	
	[IsDeleted]					BIT					NOT NULL	DEFAULT 0,
	[CreationTime]				DATETIME			NOT NULL	DEFAULT GETUTCDATE(),
	[CreatorUserId]				UNIQUEIDENTIFIER	NULL,
	[LastModificationTime]		DATETIME			NULL		DEFAULT GETUTCDATE(),
	[LastModifierUserId]		UNIQUEIDENTIFIER	NULL,
	[DeletionTime]				DATETIME			NULL,
	[DeleterUserId]				UNIQUEIDENTIFIER	NULL,

	CONSTRAINT [PK_UserTokens] PRIMARY KEY CLUSTERED 
	(
		[UserId],
		[LoginProvider],
		[Name] ASC
	),
	CONSTRAINT [FK_AspNetUserTokens_AspNetUsers_UserId] FOREIGN KEY([UserId]) REFERENCES [dbo].[Users] ([Id])
);
GO