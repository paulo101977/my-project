﻿CREATE TABLE [dbo].[Product]
(
	[ProductId]						UNIQUEIDENTIFIER		NOT NULL,
	[Code]							VARCHAR(20)				NOT NULL,
	[ProductName]					VARCHAR(200)			NOT NULL,
	[ProductCategoryId]				UNIQUEIDENTIFIER		NOT NULL,
	[UnitMeasurementId]				UNIQUEIDENTIFIER		NOT NULL,
	[NcmId]							UNIQUEIDENTIFIER		NULL,
	[BarCode]						VARCHAR(200)			NOT NULL,	
	[IsActive]						BIT						NOT NULL		DEFAULT 1,
	[PropertyId]					INT						NULL,
	[ChainId]						INT						NULL,
	[TenantId]						UNIQUEIDENTIFIER		NULL,
	[IsDeleted]						BIT					NOT NULL	DEFAULT 0,
	[CreationTime]					DATETIME			NOT NULL	DEFAULT GETUTCDATE(),
	[CreatorUserId]					UNIQUEIDENTIFIER	NULL,
	[LastModificationTime]			DATETIME			NULL		DEFAULT GETUTCDATE(),
	[LastModifierUserId]			UNIQUEIDENTIFIER	NULL,
	[DeletionTime]					DATETIME			NULL,
	[DeleterUserId]					UNIQUEIDENTIFIER	NULL,

	CONSTRAINT [PK_Product] PRIMARY KEY ( [ProductId] ),
	CONSTRAINT [FK_Product_ProductCategory] FOREIGN KEY ( [ProductCategoryId] ) REFERENCES [dbo].[ProductCategory] ( [ProductCategoryId] ),
	CONSTRAINT [FK_Product_Tenant] FOREIGN KEY ([TenantId]) REFERENCES [dbo].[Tenant] ([TenantId]),
	CONSTRAINT [FK_Product_Property] FOREIGN KEY ( [PropertyId] ) REFERENCES [dbo].[Property] ( [PropertyId] ),
	CONSTRAINT [FK_Product_Chain] FOREIGN KEY ( [ChainId] ) REFERENCES [dbo].[Chain] ( [ChainId] ),
	CONSTRAINT [FK_Product_UnitMeasurement] FOREIGN KEY ( [UnitMeasurementId] ) REFERENCES [dbo].[UnitMeasurement] ( [UnitMeasurementId] ),
	CONSTRAINT [FK_Product_Ncm] FOREIGN KEY ( [NcmId] ) REFERENCES [dbo].[Ncm] ( [NcmId] ),
)
GO

CREATE INDEX 
	x_Product_ProductCategoryId ON Product ( [ProductCategoryId] ) 
GO

CREATE INDEX 
	x_Product_PropertyId ON Product ( [PropertyId] ) 
GO 


CREATE INDEX 
	x_Product_ChainId ON [dbo].[Chain]( [ChainId] )
GO

CREATE INDEX 
	x_Product_TenantId ON [dbo].[Product]( [TenantId] )
GO

CREATE INDEX 
	x_Product_UnitMeasurementId ON [dbo].[UnitMeasurement]( [UnitMeasurementId] )
GO

CREATE INDEX 
	x_Product_NcmId ON [dbo].[Ncm]( [NcmId] )
GO
