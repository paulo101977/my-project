﻿CREATE TABLE [dbo].[Guest]
(
	[GuestId] 					BIGINT								NOT NULL	IDENTITY(1,1),
	[PersonId] 					UNIQUEIDENTIFIER					NULL,

	CONSTRAINT [PK_Guest] PRIMARY KEY ([GuestId]),
	CONSTRAINT [UK_Guest] UNIQUE ([PersonId]),
	CONSTRAINT [FK_Guest_Person] FOREIGN KEY ([PersonId]) REFERENCES [dbo].[Person] ([PersonId]),
);
GO

ALTER TABLE [dbo].[Guest] CHECK CONSTRAINT [FK_Guest_Person]
GO

CREATE INDEX 
    x_Guest_PersonId ON dbo.Guest( [PersonId] )
GO