﻿CREATE TABLE [dbo].[GuestRegistration]
(
	[GuestRegistrationId]				UNIQUEIDENTIFIER				NOT NULL,
	[FirstName]							VARCHAR(200)					NOT NULL,
	[LastName]							VARCHAR(400)					NULL,
	[FullName]							VARCHAR(1000)					NULL,
	[SocialName]						VARCHAR(1000)					NULL,
	[Email]								VARCHAR(200)					NULL,
	[BirthDate]							DATE							NULL,
	[Gender]							VARCHAR(1)						NULL,
	[OccupationId]						INT								NULL,
	[GuestTypeId]						INT								NULL,
	[PhoneNumber]						VARCHAR(50)						NULL,
	[MobilePhoneNumber]					VARCHAR(50)						NULL,
	[Nationality]						INTEGER							NULL,
	[PurposeOfTrip]						INTEGER							NULL,
	[ArrivingBy]						INTEGER							NULL,
	[ArrivingFrom]						INTEGER							NULL,
	[ArrivingFromText]					VARCHAR(100)					NULL,
	[NextDestination]					INTEGER							NULL,
	[NextDestinationText]				VARCHAR(100)					NULL,
	[AdditionalInformation]				TEXT							NULL,
	[HealthInsurance]					VARCHAR(200)					NULL,
	[PropertyId]						INT								NOT NULL,
	[TenantId]							UNIQUEIDENTIFIER				NOT NULL,
	[PersonId]							UNIQUEIDENTIFIER				NOT NULL,	-- Denormalizing
	[GuestId]							BIGINT							NULL,	-- Denormalizing
	[ResponsibleGuestRegistrationId]	UNIQUEIDENTIFIER				NULL,
	[IsLegallyIncompetent]				BIT								NULL	DEFAULT 0,
	[DocumentTypeId]					INT		  						NULL,
    [DocumentInformation]				VARCHAR (20)					NULL,
	
	[HigsCode]							VARCHAR(100)					NULL, 

	[IsDeleted]							BIT								NOT NULL	DEFAULT 0,
	[CreationTime]						DATETIME						NOT NULL	DEFAULT GETUTCDATE(),
	[CreatorUserId]						UNIQUEIDENTIFIER				NULL,
	[LastModificationTime]				DATETIME						NULL		DEFAULT GETUTCDATE(),
	[LastModifierUserId]				UNIQUEIDENTIFIER				NULL,
	[DeletionTime]						DATETIME						NULL,
	[DeleterUserId]						UNIQUEIDENTIFIER				NULL,
	
	CONSTRAINT [PK_GuestRegistration] PRIMARY KEY ([GuestRegistrationId]),
	CONSTRAINT [FK_GuestRegistration_CountrySubdivision_1] FOREIGN KEY ([Nationality]) REFERENCES [dbo].[CountrySubdivision] ([CountrySubdivisionId]),
	CONSTRAINT [FK_GuestRegistration_CountrySubdivision_2] FOREIGN KEY ([ArrivingFrom]) REFERENCES [dbo].[CountrySubdivision] ([CountrySubdivisionId]),
	CONSTRAINT [FK_GuestRegistration_CountrySubdivision_3] FOREIGN KEY ([NextDestination]) REFERENCES [dbo].[CountrySubdivision] ([CountrySubdivisionId]),
	CONSTRAINT [FK_GuestRegistration_PropertyGuestType] FOREIGN KEY ([GuestTypeId]) REFERENCES [dbo].[PropertyGuestType] ([PropertyGuestTypeId]),
	CONSTRAINT [FK_GuestRegistration_TransportationType] FOREIGN KEY ([ArrivingBy]) REFERENCES [dbo].[TransportationType] ([TransportationTypeId]),
	CONSTRAINT [FK_GuestRegistration_GeneralOccupation] FOREIGN KEY ([OccupationId]) REFERENCES [dbo].[GeneralOccupation] ([GeneralOccupationId]),
	CONSTRAINT [FK_GuestRegistration_Reason] FOREIGN KEY ([PurposeOfTrip]) REFERENCES [dbo].[Reason] ([ReasonId]),
	CONSTRAINT [FK_GuestRegistration_Property] FOREIGN KEY ([PropertyId]) REFERENCES [dbo].[Property]([PropertyId]),
	CONSTRAINT [FK_GuestRegistration_Tenant] FOREIGN KEY ([TenantId]) REFERENCES [dbo].[Tenant] ([TenantId]),
	CONSTRAINT [FK_GuestRegistration_Person] FOREIGN KEY ([PersonId]) REFERENCES [dbo].[Person]([PersonId]),
	CONSTRAINT [FK_GuestRegistration_Guest] FOREIGN KEY ([GuestId]) REFERENCES [dbo].[Guest] ([GuestId]),
	CONSTRAINT [FK_GuestRegistration_ResponsibleGuestRegistrationId] FOREIGN KEY ([ResponsibleGuestRegistrationId]) REFERENCES [dbo].GuestRegistration ([GuestRegistrationId]),
	CONSTRAINT [FK_GuestRegistration_DocumentType] FOREIGN KEY ([DocumentTypeId]) REFERENCES [dbo].[DocumentType] ([DocumentTypeId]),
)
GO

CREATE INDEX 
	x_GuestRegistration_Nationality ON dbo.GuestRegistration( [Nationality] )
GO

CREATE INDEX 
	x_GuestRegistration_ArrivingFrom ON dbo.GuestRegistration( [ArrivingFrom] )
GO

CREATE INDEX 
	x_GuestRegistration_NextDestination ON dbo.GuestRegistration( [NextDestination] )
GO

CREATE INDEX 
	x_GuestRegistration_GuestTypeId ON dbo.GuestRegistration( [GuestTypeId] )
GO

CREATE INDEX 
	x_GuestRegistration_ArrivingBy ON dbo.GuestRegistration( [ArrivingBy] )
GO

CREATE INDEX 
	x_GuestRegistration_OccupationId ON dbo.GuestRegistration( [OccupationId] )
GO

CREATE INDEX 
	x_GuestRegistration_PurposeOfTrip ON dbo.GuestRegistration( [PurposeOfTrip] )
GO

CREATE INDEX 
	x_GuestRegistration_PropertyId ON [dbo].[GuestRegistration]( [PropertyId] )
GO

CREATE INDEX 
	x_GuestRegistration_TenantId ON [dbo].[GuestRegistration]( [TenantId] )
GO

CREATE INDEX 
	x_GuestRegistration_PersonId ON [dbo].[GuestRegistration]( [PersonId] )
GO

CREATE INDEX 
	x_GuestRegistration_GuestId ON [dbo].[GuestRegistration]( [GuestId] )
GO

CREATE INDEX 
	x_GuestRegistration_ResponsibleGuestRegistrationId ON [dbo].[GuestRegistration]( [ResponsibleGuestRegistrationId] )
GO

CREATE INDEX 
	x_GuestRegistration_DocumentTypeId ON [dbo].[GuestRegistration]( [DocumentTypeId] )
GO