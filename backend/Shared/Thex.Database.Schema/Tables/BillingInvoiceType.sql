﻿CREATE TABLE [dbo].[BillingInvoiceType]
(
	[BillingInvoiceTypeId]							INT	NOT NULL,
	[BillingInvoiceTypeName]						VARCHAR(200)		NOT NULL,
    CONSTRAINT [PK_BillingInvoiceType] PRIMARY KEY ( [BillingInvoiceTypeId] ),
)
GO