﻿CREATE TABLE [dbo].[PowerBiGroupReportDashboard]
(
	[PowerBiGroupReportDashboardId]			UNIQUEIDENTIFIER			NOT NULL,
	[PowerBiGroupId]						UNIQUEIDENTIFIER			NOT NULL,
	[PowerBiDashboardId]					UNIQUEIDENTIFIER			NULL,
	[PowerBiReportId]						UNIQUEIDENTIFIER			NULL,

	CONSTRAINT [PK_PowerBiGroupReportDashboard] PRIMARY KEY ([PowerBiGroupReportDashboardId]),
	CONSTRAINT [FK_PowerBiGroupReportDashboard_PowerBiGroup] FOREIGN KEY ([PowerBiGroupId]) REFERENCES [PowerBiGroup]([PowerBiGroupId]),
	CONSTRAINT [FK_PowerBiGroupReportDashboard_PowerBiReport] FOREIGN KEY ([PowerBiReportId]) REFERENCES [PowerBiReport]([PowerBiReportId]),
	CONSTRAINT [FK_PowerBiGroupReportDashboard_PowerBiDashboard] FOREIGN KEY ([PowerBiDashboardId]) REFERENCES [dbo].[PowerBiDashboard] ([PowerBiDashboardId])
);
GO

CREATE INDEX 
	x_PowerBiGroupReportDashboard_PowerBiGroupId ON dbo.PowerBiGroupReportDashboard( [PowerBiGroupId] )
GO

CREATE INDEX 
	x_PowerBiGroupReportDashboard_PowerBiReportId ON dbo.PowerBiGroupReportDashboard( [PowerBiReportId] )
GO

CREATE INDEX 
	x_PowerBiGroupReportDashboard_PowerBiDashboardId ON [dbo].[PowerBiGroupReportDashboard]( [PowerBiDashboardId] )
GO
