﻿CREATE TABLE [dbo].[IntegrationPartner]
(
	[IntegrationPartnerId] INT NOT NULL IDENTITY(1,1),
	[TokenClient]						VARCHAR(150),
	[TokenApplication]					VARCHAR(150),
	[IntegrationPartnerName] 			VARCHAR(100) NOT NULL,
	[IntegrationPartnerType]			INT			 NOT NULL,
	[IsActive]							BIT			 NULL	   DEFAULT 1,
	
	CONSTRAINT [PK_IntegrationPartner] PRIMARY KEY ([IntegrationPartnerId])
)
