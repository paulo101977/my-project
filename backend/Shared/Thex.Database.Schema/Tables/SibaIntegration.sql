﻿CREATE TABLE [dbo].[SibaIntegration]
(
	[SibaIntegrationId]				UNIQUEIDENTIFIER   NOT NULL, 
	[PropertyUId]					uniqueidentifier	 NOT NULL, 
	[IntegrationFileNumber]			INTEGER	 NOT NULL,

	CONSTRAINT [PK_SibaIntegration] PRIMARY KEY ([SibaIntegrationId]),
	CONSTRAINT [UK_SibaIntegration] UNIQUE ([PropertyUId], [IntegrationFileNumber])
)
GO

CREATE INDEX 
	x_SibaIntegration_PropertyId ON SibaIntegration(PropertyUId)
GO
