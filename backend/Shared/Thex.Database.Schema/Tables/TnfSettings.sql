﻿CREATE TABLE [dbo].[TnfSettings]
(
	[Id] [bigint] IDENTITY(1,1) PRIMARY KEY NOT NULL,
	[TenantId] [int] NULL,
	[UserId] [bigint] NULL,
	[Name] [varchar](256) NULL,
	[Value] [varchar](2000) NULL,
	[IsDeleted] [bit] NOT NULL	DEFAULT 0,
	[DeleterUserId] [bigint] NULL,
	[DeletionTime] [datetime] NULL,
	[LastModificationTime] [datetime] NULL,
	[LastModifierUserId] [bigint] NULL,
	[CreationTime] [datetime] NOT NULL,
	[CreatorUserId] [bigint] NULL
)

