﻿CREATE TABLE [dbo].[Location]
(
	[LocationId]				INT IDENTITY		NOT NULL,
	[OwnerId]					UNIQUEIDENTIFIER	NOT NULL,
	[LocationCategoryId]		INT					NOT NULL	DEFAULT 1,
	[Latitude]					DECIMAL (9,6)		    NULL,
	[Longitude]					DECIMAL (9,6)		    NULL,
	[StreetName]				VARCHAR (200)		    NULL,
	[StreetNumber]				VARCHAR (50)		    NULL,
	[AdditionalAddressDetails]	VARCHAR (50)			NULL,
	[Neighborhood]				VARCHAR(100)			NULL,
	[CityId]					INT					    NULL,
	[PostalCode]				VARCHAR(20)			    NULL,
	[CountryCode]				CHAR (2)			NOT NULL,
	
	[StateId]					INT					NULL,
	[CountryId]					INT					NULL,

	CONSTRAINT [PK_Location] PRIMARY KEY ([LocationId]),
	CONSTRAINT [FK_Location_LocationCategory] FOREIGN KEY ([LocationCategoryId]) REFERENCES [dbo].[LocationCategory] ([LocationCategoryId]),
	CONSTRAINT [FK_Location_CountrySubdivision] FOREIGN KEY ([CityId]) REFERENCES [dbo].[CountrySubdivision] ([CountrySubdivisionId]),
	CONSTRAINT [FK_Location_CountrySubdivision_State] FOREIGN KEY ([StateId]) REFERENCES [dbo].[CountrySubdivision] ([CountrySubdivisionId]),
	CONSTRAINT [FK_Location_CountrySubdivision_Country] FOREIGN KEY ([CountryId]) REFERENCES [dbo].[CountrySubdivision] ([CountrySubdivisionId])
);

GO
CREATE INDEX [x_Location_LocationCategoryId] ON [dbo].[Location] ([LocationCategoryId]);

GO
CREATE INDEX [x_Location_CountrySubdivisionId] ON [dbo].[Location] ([CountryCode]);

GO
CREATE INDEX [x_Location_CityId] ON [dbo].[Location] ([CityId]);

GO
CREATE INDEX [x_Location_StateId] ON [dbo].[Location] ([StateId]);

GO
CREATE INDEX [x_Location_CountryId] ON [dbo].[Location] ([CountryId]);