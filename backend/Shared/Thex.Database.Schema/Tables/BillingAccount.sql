﻿CREATE TABLE [dbo].[BillingAccount]
(
	[BillingAccountId]				UNIQUEIDENTIFIER		NOT NULL, 
	[CompanyClientId]				UNIQUEIDENTIFIER		NULL, 
	[ReservationId]					BIGINT					NULL, 
	[ReservationItemId]				BIGINT					NULL,
	[GuestReservationItemId]		BIGINT					NULL,
	[BillingAccountTypeId]			INTEGER					NOT NULL, 
	[StartDate]						DATETIME				NOT NULL, 
	[EndDate]						DATETIME, 
	[StatusId]						INTEGER					NOT NULL,
	[IsMainAccount]					BIT						NOT NULL	DEFAULT 1,
	[MarketSegmentId]				INT						NOT NULL,
	[BusinessSourceId]				INT						NOT NULL,
	[PropertyId]					INT						NOT NULL,
	[Blocked]						BIT						NOT NULL	DEFAULT 0,
	[GroupKey]						UNIQUEIDENTIFIER		NOT NULL, 
	[BillingAccountName]			VARCHAR(150)			NULL, 
	[ReopeningReasonId]				INT						NULL,
	[ReopeningDate]					DATETIME				NULL,
	[PersonHeaderId] UNIQUEIDENTIFIER NULL, 
	[TenantId]						UNIQUEIDENTIFIER		NOT NULL,
	
	[IsDeleted]						BIT						NOT NULL	DEFAULT 0,
	[CreationTime]					DATETIME				NOT NULL	DEFAULT GETUTCDATE(),
	[CreatorUserId]					UNIQUEIDENTIFIER		NULL,
	[LastModificationTime]			DATETIME				NULL		DEFAULT GETUTCDATE(),
	[LastModifierUserId]			UNIQUEIDENTIFIER		NULL,
	[DeletionTime]					DATETIME				NULL,
	[DeleterUserId]					UNIQUEIDENTIFIER		NULL,


	CONSTRAINT [PK_BillingAccount] PRIMARY KEY ( [BillingAccountId] ),
	CONSTRAINT [FK_BillingAccount_BillingAccountType] FOREIGN KEY ( [BillingAccountTypeId] ) REFERENCES [dbo].[BillingAccountType] ( [BillingAccountTypeId] ),
	CONSTRAINT [FK_BillingAccount_CompanyClient] FOREIGN KEY ( [CompanyClientId] ) REFERENCES [dbo].[CompanyClient] ( [CompanyClientId] ),
	CONSTRAINT [FK_BillingAccount_Reservation] FOREIGN KEY ( [ReservationId] ) REFERENCES [dbo].[Reservation] ( [ReservationId] ),
	CONSTRAINT [FK_BillingAccount_ReservationItem] FOREIGN KEY ( [ReservationItemId] ) REFERENCES [dbo].[ReservationItem] ( [ReservationItemId] ),
	CONSTRAINT [FK_BillingAccount_Status] FOREIGN KEY ( [StatusId] ) REFERENCES [dbo].[Status] ( [StatusId] ),
	CONSTRAINT [FK_BillingAccount_GuestReservationItem] FOREIGN KEY ( [GuestReservationItemId] ) REFERENCES [dbo].[GuestReservationItem] ( [GuestReservationItemId] ),
	CONSTRAINT [FK_BillingAccount_BusinessSource] FOREIGN KEY ( [BusinessSourceId] ) REFERENCES [dbo].[BusinessSource] ( [BusinessSourceId] ),
	CONSTRAINT [FK_BillingAccount_MarketSegment] FOREIGN KEY ( [MarketSegmentId] ) REFERENCES [dbo].[MarketSegment] ( [MarketSegmentId] ),
	CONSTRAINT [FK_BillingAccount_Reason] FOREIGN KEY ( [ReopeningReasonId] ) REFERENCES [dbo].[Reason] ( [ReasonId] ),	
	CONSTRAINT [FK_BillingAccount_Tenant] FOREIGN KEY ([TenantId]) REFERENCES [dbo].[Tenant] ([TenantId]),
	CONSTRAINT [FK_BillingAccount_Person] FOREIGN KEY ([PersonHeaderId]) REFERENCES [dbo].[Person] ([PersonId]),
)
GO

CREATE INDEX 
	x_BillingAccount_CompanyClientId ON BillingAccount ( [CompanyClientId] )
GO

CREATE INDEX 
	x_BillingAccount_ReservationId ON BillingAccount ( [ReservationId] )
GO 

CREATE INDEX 
	x_BillingAccount_BillingAccountTypeId ON BillingAccount ( [BillingAccountTypeId] ) 
GO 

CREATE INDEX 
	x_BillingAccount_StatusId ON BillingAccount ( [StatusId] ) 
GO 

CREATE INDEX 
	x_BillingAccount_ReservationItemId ON BillingAccount ( [ReservationItemId] ) 
GO

CREATE INDEX 
	x_BillingAccount_GuestReservationItemId ON BillingAccount ( [GuestReservationItemId] ) 
GO

CREATE INDEX 
	x_BillingAccount_MarketSegmentId ON BillingAccount ( [MarketSegmentId] ) 
GO

CREATE INDEX 
	x_BillingAccount_BusinessSourceId ON BillingAccount ( [BusinessSourceId] ) 
GO

CREATE INDEX 
	x_BillingAccount_PropertyId ON BillingAccount ( PropertyId ) 
GO


CREATE INDEX 
	x_BillingAccount_GroupKey ON BillingAccount ( [GroupKey] ) 
GO

CREATE INDEX 
	x_BillingAccount_TenantId ON [dbo].[BillingAccount]( [TenantId] )
GO

CREATE INDEX 
	x_BillingAccount_PersonId ON [dbo].[BillingAccount] ( [PersonHeaderId] ) 
GO