﻿CREATE TABLE [dbo].[PolicyType]
(
	[PolicyTypeId]		INT NOT NULL,
	[PolicyTypeName]	VARCHAR(50)	NOT NULL,

	CONSTRAINT [PK_PolicyType] PRIMARY KEY ([PolicyTypeId])
)
GO