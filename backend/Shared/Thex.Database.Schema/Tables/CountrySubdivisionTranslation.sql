﻿CREATE TABLE [dbo].[CountrySubdivisionTranslation]
(
	[CountrySubdivisionTranslationId] INT IDENTITY  NOT NULL,
	[LanguageIsoCode]				  VARCHAR(5) 	NOT NULL,
	[Name]							  VARCHAR(50) 	NOT NULL,
	[CountrySubdivisionId]			  INT 			NOT NULL,
	[Nationality]					  VARCHAR(100)	NULL

	CONSTRAINT [PK_CountrySubdivisionTranslation] PRIMARY KEY ([CountrySubdivisionTranslationId]),
	CONSTRAINT [FK_CountrySubdivisionTranslation_CountrySubdivision] FOREIGN KEY ([CountrySubdivisionId]) REFERENCES [dbo].[CountrySubdivision] ([CountrySubdivisionId])
);
GO

CREATE INDEX 
    x_CountrySubdivisionTranslation_CountrySubdivisionId ON dbo.CountrySubdivisionTranslation( [CountrySubdivisionId] )
GO
