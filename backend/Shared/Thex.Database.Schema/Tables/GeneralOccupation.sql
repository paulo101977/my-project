﻿CREATE TABLE [dbo].[GeneralOccupation]
(
	[GeneralOccupationId]			INTEGER			NOT NULL,
	[Occupation]					VARCHAR(100)	NOT NULL,

	CONSTRAINT [PK_GeneralOccupation] PRIMARY KEY ( [GeneralOccupationId] )
)
GO

