﻿CREATE TABLE [dbo].[BedType]
(
	[BedTypeId]		INT				NOT NULL,
	[Name]			VARCHAR (20)	NOT NULL,
	[Capactity]		TINYINT			NOT NULL	DEFAULT 1,

    CONSTRAINT [PK_BedType] PRIMARY KEY ([BedTypeId])
);