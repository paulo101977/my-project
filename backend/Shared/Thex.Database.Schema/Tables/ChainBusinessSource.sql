﻿CREATE TABLE [dbo].[ChainBusinessSource]
(
	[ChainBusinessSourceId]		UNIQUEIDENTIFIER	NOT NULL,
	[ChainId]					INT		NOT NULL,
	[BusinessSourceId]			INT		NOT NULL,
	[IsActive]					BIT		NOT NULL	DEFAULT 1,	

	[IsDeleted]					BIT					NOT NULL	DEFAULT 0,
	[CreationTime]				DATETIME			NOT NULL	DEFAULT GETUTCDATE(),
	[CreatorUserId]				UNIQUEIDENTIFIER	NULL,
	[LastModificationTime]		DATETIME			NULL		DEFAULT GETUTCDATE(),
	[LastModifierUserId]		UNIQUEIDENTIFIER	NULL,
	[DeletionTime]				DATETIME			NULL,
	[DeleterUserId]				UNIQUEIDENTIFIER	NULL,

	CONSTRAINT [PK_ChainBusinessSource] PRIMARY KEY ([ChainBusinessSourceId]),
	CONSTRAINT [UK_ChainBusinessSource] UNIQUE ([ChainId], [BusinessSourceId]),
	CONSTRAINT [FK_ChainBusinessSource_Chain] FOREIGN KEY ([ChainId]) REFERENCES [Chain]([ChainId]),
	CONSTRAINT [FK_ChainBusinessSource_BusinessSource] FOREIGN KEY ([BusinessSourceId]) REFERENCES [BusinessSource]([BusinessSourceId])
)
GO

CREATE INDEX 
	x_ChainBusinessSource_BusinessSourceId ON dbo.[ChainBusinessSource]( [BusinessSourceId] )
GO