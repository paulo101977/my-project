﻿CREATE TABLE [dbo].[ReasonCategory]
(
	[ReasonCategoryId]			INT						NOT NULL,
	[ReasonType]				VARCHAR(100)			NOT NULL,

	CONSTRAINT [PK_ReasonCategory] PRIMARY KEY ([ReasonCategoryId])
	
);
GO
