﻿CREATE TABLE [dbo].[PowerBiTenantConfig]
(
	[PowerBiTenantConfigId]				UNIQUEIDENTIFIER	NOT NULL,
	[PowerBiGroupId]					UNIQUEIDENTIFIER	NOT NULL,
	[TenantId]							UNIQUEIDENTIFIER	NOT NULL,

	CONSTRAINT [PK_PowerBiTenantConfig] PRIMARY KEY ([PowerBiTenantConfigId]),
	CONSTRAINT [FK_PowerBiTenantConfig_PowerBiGroup] FOREIGN KEY ([PowerBiGroupId]) REFERENCES [PowerBiGroup]([PowerBiGroupId]),
	CONSTRAINT [FK_PowerBiTenantConfig_Tenant] FOREIGN KEY ([TenantId]) REFERENCES [dbo].[Tenant] ([TenantId])
);
GO

CREATE INDEX 
	x_PowerBiTenantConfig_PowerBiGroupId ON dbo.PowerBiGroup( [PowerBiGroupId] )
GO

CREATE INDEX 
	x_PowerBiTenantConfig_TenantId ON dbo.Tenant( [TenantId] )
GO
	