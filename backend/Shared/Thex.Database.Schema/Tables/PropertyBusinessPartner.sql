﻿CREATE TABLE [dbo].[PropertyBusinessPartner]
(
	[PropertyBusinessPartnerId]		UNIQUEIDENTIFIER	NOT NULL,
	[PropertyId]					INT					NOT NULL,
	[BusinessPartnerId]				UNIQUEIDENTIFIER	NOT NULL,
	[TenantId]						UNIQUEIDENTIFIER	NOT NULL,

	[IsDeleted]						BIT					NOT NULL	DEFAULT 0,
	[CreationTime]					DATETIME			NOT NULL	DEFAULT GETUTCDATE(),
	[CreatorUserId]					UNIQUEIDENTIFIER	NULL,
	[LastModificationTime]			DATETIME			NULL		DEFAULT GETUTCDATE(),
	[LastModifierUserId]			UNIQUEIDENTIFIER	NULL,
	[DeletionTime]					DATETIME			NULL,
	[DeleterUserId]					UNIQUEIDENTIFIER	NULL,
	
	CONSTRAINT [PK_PropertyBusinessPartner]	PRIMARY KEY ([PropertyBusinessPartnerId]),
	CONSTRAINT [FK_PropertyBusinessPartner_Property] FOREIGN KEY ([PropertyId]) REFERENCES [dbo].[Property] ([PropertyId]),
	CONSTRAINT [FK_PropertyBusinessPartner_BusinessPartnerId] FOREIGN KEY ([BusinessPartnerId]) REFERENCES [dbo].[BusinessPartner] ([BusinessPartnerId]),
	CONSTRAINT [FK_PropertyBusinessPartner_Tenant] FOREIGN KEY ([TenantId]) REFERENCES [dbo].[Tenant] ([TenantId])
)
GO

CREATE INDEX 
	x_PropertyBusinessPartner_PropertyId ON [dbo].[PropertyBusinessPartner] ( [PropertyId] )
GO

CREATE INDEX 
	x_PropertyBusinessPartner_BusinessPartnerId ON [dbo].[PropertyBusinessPartner] ( [BusinessPartnerId] )
GO

CREATE INDEX 
	x_PropertyBusinessPartner_TenantId ON [dbo].[PropertyBusinessPartner]( [TenantId] )
GO