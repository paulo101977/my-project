﻿CREATE TABLE [dbo].[CountrySubdivision]
(
	[CountrySubdivisionId]				INT				NOT NULL	IDENTITY,
	[TwoLetterIsoCode]					CHAR(2)			NULL,
	[ThreeLetterIsoCode]				CHAR(3)			NULL,
	[CountryTwoLetterIsoCode]			CHAR(2)			NULL,
	[CountryThreeLetterIsoCode]			CHAR(3)			NULL,
	[ExternalCode]						VARCHAR(20)		NULL,
	[ParentSubdivisionId]				INT				NULL,
	[SubdivisionTypeId]					INT				NOT NULL,
	[BR_CODIGOBCB]						NUMERIC			NULL,
	
	CONSTRAINT [PK_CountrySubdivision] PRIMARY KEY ([CountrySubdivisionId]),
	CONSTRAINT [FK_CountrySubdivision_CountrySubdivison] FOREIGN KEY ([ParentSubdivisionId]) REFERENCES [dbo].[CountrySubdivision] ([CountrySubdivisionId])
);
GO

CREATE INDEX 
	x_CountrySubdivision_ParentSubdivisionId ON dbo.CountrySubdivision( [ParentSubdivisionId] )
GO