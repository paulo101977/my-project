﻿CREATE TABLE [dbo].[ApplicationModule]
(
	[ApplicationModuleId]				INT				NOT NULL,
	[ApplicationModuleName]				VARCHAR(100)	NOT NULL,
	[ApplicationModuleDescription]		VARCHAR(300)	NOT NULL,

	CONSTRAINT	PK_ApplicationModule PRIMARY KEY ([ApplicationModuleId])
)
