﻿CREATE TABLE [dbo].[Occupation]
(
    [OccupationId]	INT IDENTITY	NOT NULL,
	[Name]			VARCHAR (50)	NOT NULL,
	[CompanyId]		INTEGER			NOT NULL

    CONSTRAINT [PK_Occupation] PRIMARY KEY ([OccupationId]),
	CONSTRAINT [FK_Occupation_Company] FOREIGN KEY ([CompanyId]) REFERENCES [dbo].[Company] (CompanyId)
);
GO

CREATE INDEX 
	x_Occupation_CompanyId ON dbo.Occupation( [CompanyId] )
GO