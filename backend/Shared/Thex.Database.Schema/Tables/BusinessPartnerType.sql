﻿CREATE TABLE [dbo].[BusinessPartnerType]
(
	[BusinessPartnerTypeId]			INT				NOT NULL,
	[BusinessPartnerTypeName]		VARCHAR(100)	NOT NULL,
	[Description]					VARCHAR(300)	NOT NULL,

	CONSTRAINT PK_BusinessPartnerType PRIMARY KEY ( [BusinessPartnerTypeId] )
)
