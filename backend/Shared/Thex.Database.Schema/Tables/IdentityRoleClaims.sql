﻿CREATE TABLE [dbo].[RoleClaims](
	[Id]			[int]				IDENTITY(1,1)	NOT NULL,
	[RoleId]		UNIQUEIDENTIFIER	NOT NULL,
	[ClaimType]		[nvarchar](max)		NULL,
	[ClaimValue]	[nvarchar](max)		NULL,
 
	[IsDeleted]					BIT					NOT NULL	DEFAULT 0,
	[CreationTime]				DATETIME			NOT NULL	DEFAULT GETUTCDATE(),
	[CreatorUserId]				UNIQUEIDENTIFIER	NULL,
	[LastModificationTime]		DATETIME			NULL		DEFAULT GETUTCDATE(),
	[LastModifierUserId]		UNIQUEIDENTIFIER	NULL,
	[DeletionTime]				DATETIME			NULL,
	[DeleterUserId]				UNIQUEIDENTIFIER	NULL,

	CONSTRAINT [PK_RoleClaims] PRIMARY KEY CLUSTERED ([Id] ASC),
	CONSTRAINT [FK_AspNetRoleClaims_AspNetRoles_RoleId] FOREIGN KEY([RoleId]) REFERENCES [dbo].[Roles] ([Id])
);
GO
