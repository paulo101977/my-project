﻿CREATE TABLE [dbo].[PropertyMealPlanType]
(
	[PropertyMealPlanTypeId]		UNIQUEIDENTIFIER	NOT NULL,
	[PropertyMealPlanTypeCode]		VARCHAR(10)			NULL,
	[PropertyMealPlanTypeName]		VARCHAR(100)		NULL,
	[PropertyId]					INT					NOT NULL,
	[MealPlanTypeId]				INT					NOT NULL,
	[IsActive]						BIT					NOT NULL	DEFAULT 1,
	[TenantId]						UNIQUEIDENTIFIER	NOT NULL,

	[IsDeleted]						BIT					NOT NULL	DEFAULT 0,
	[CreationTime]					DATETIME			NOT NULL	DEFAULT GETUTCDATE(),
	[CreatorUserId]					UNIQUEIDENTIFIER	NULL,
	[LastModificationTime]			DATETIME			NULL		DEFAULT GETUTCDATE(),
	[LastModifierUserId]			UNIQUEIDENTIFIER	NULL,
	[DeletionTime]					DATETIME			NULL,
	[DeleterUserId]					UNIQUEIDENTIFIER	NULL,

	CONSTRAINT [PK_PropertyMealPlanType] PRIMARY KEY ([PropertyMealPlanTypeId]),
	CONSTRAINT [UK_PropertyMealPlanType_1] UNIQUE ( [PropertyId], [MealPlanTypeId]),
	CONSTRAINT [UK_PropertyMealPlanType_2] UNIQUE ( [PropertyId], [PropertyMealPlanTypeCode]),
	CONSTRAINT [UK_PropertyMealPlanType_3] UNIQUE ( [PropertyId], [PropertyMealPlanTypeName]),
	CONSTRAINT [FK_PropertyMealPlanType_MealPlanType] FOREIGN KEY ( [MealPlanTypeId] ) REFERENCES [dbo].[MealPlanType] ( [MealPlanTypeId] ),
	CONSTRAINT [FK_PropertyMealPlanType_Property] FOREIGN KEY ( [PropertyId] ) REFERENCES [dbo].[Property] ( [PropertyId] ),
	CONSTRAINT [FK_PropertyMealPlanType_Tenant] FOREIGN KEY ([TenantId]) REFERENCES [dbo].[Tenant] ([TenantId])
)
GO

CREATE INDEX 
	x_PropertyMealPlanType_MealPlanTypeId ON PropertyMealPlanType ( [MealPlanTypeId] ) 
GO 

CREATE INDEX 
	x_PropertyMealPlanType_PropertyId ON PropertyMealPlanType ( [PropertyId] )
GO

CREATE INDEX 
	x_PropertyMealPlanType_TenantId ON [dbo].[PropertyMealPlanType]( [TenantId] )
GO