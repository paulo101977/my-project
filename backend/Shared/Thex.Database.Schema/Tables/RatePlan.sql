﻿CREATE TABLE [dbo].[RatePlan]
(
	[RatePlanId]					UNIQUEIDENTIFIER	NOT NULL,
	[AgreementTypeId]				INT					NOT NULL,
	[AgreementName]					VARCHAR(100)		NOT NULL,
	[StartDate]						DATETIME			NOT NULL, 
	[EndDate]						DATETIME			NULL,
	[PropertyId]					INT					NOT NULL,
	[MealPlanTypeId]				INT	NOT NULL,
	[IsActive]						BIT					NOT NULL	DEFAULT 1,
	[PublishOnline]					BIT					NOT NULL	DEFAULT 0,
	[CurrencyId]					UNIQUEIDENTIFIER	NOT NULL,
	[CurrencySymbol]				VARCHAR(10)			NULL,
	[RateNet]						BIT					NOT NULL	DEFAULT 0,
	[RateTypeId]					INT					NOT NULL	DEFAULT 0,
	[TenantId]						UNIQUEIDENTIFIER	NOT NULL,
	[MarketSegmentId]               INT    NULL,
	[IsDeleted]						BIT					NOT NULL	DEFAULT 0,
	[CreationTime]					DATETIME			NOT NULL	DEFAULT GETUTCDATE(),
	[CreatorUserId]					UNIQUEIDENTIFIER	NULL,
	[LastModificationTime]			DATETIME			NULL		DEFAULT GETUTCDATE(),
	[LastModifierUserId]			UNIQUEIDENTIFIER	NULL,
	[DeletionTime]					DATETIME			NULL,
	[DeleterUserId]					UNIQUEIDENTIFIER	NULL,
	[DistributionCode]				VARCHAR (100)		NULL, 
	[Description]					VARCHAR (4000)		NULL,
	[HigsCode]						VARCHAR(100)		NULL,
	[RoundingTypeId]				INT					NULL,

	CONSTRAINT [PK_RatePlan] PRIMARY KEY ( [RatePlanId] ),
	CONSTRAINT [UK_RatePlan] UNIQUE ( [PropertyId], [AgreementName] ),
	CONSTRAINT [FK_RatePlan_Property] FOREIGN KEY ( [PropertyId] ) REFERENCES [dbo].[Property] ( [PropertyId] ),
	CONSTRAINT [FK_RatePlan_MealPlanType] FOREIGN KEY ( [MealPlanTypeId] ) REFERENCES [dbo].[MealPlanType] ( [MealPlanTypeId] ),
	CONSTRAINT [FK_RatePlan_Currency] FOREIGN KEY ( [CurrencyId] ) REFERENCES [dbo].[Currency]( [CurrencyId] ),
	CONSTRAINT [FK_RatePlan_AgreementType] FOREIGN KEY ( [AgreementTypeId] ) REFERENCES [dbo].[AgreementType] ( [AgreementTypeId] ),
	CONSTRAINT [FK_RatePlan_RateType] FOREIGN KEY ( [RateTypeId] ) REFERENCES [dbo].[RateType]( [RateTypeId] ),
	CONSTRAINT [FK_RatePlan_Tenant] FOREIGN KEY ([TenantId]) REFERENCES [dbo].[Tenant] ([TenantId]),
	CONSTRAINT [FK_RatePlan_MarketSegment] FOREIGN KEY ( [MarketSegmentId] ) REFERENCES [dbo].[MarketSegment] ( [MarketSegmentId] )
)
GO

CREATE INDEX 
	x_RatePlan_PropertyId ON dbo.[RatePlan] ( [PropertyId] )
GO

CREATE INDEX 
	x_RatePlan_MealPlanTypeId ON dbo.[RatePlan] ( [MealPlanTypeId] )
GO

CREATE INDEX 
	x_RatePlan_CurrencyId ON dbo.[RatePlan] ( [CurrencyId] )
GO

CREATE INDEX 
	x_RatePlan_RateTypeId ON dbo.[RatePlan] ( [RateTypeId] )
GO

CREATE INDEX 
	x_RatePlan_AgreementTypeId ON dbo.[RatePlan] ( [AgreementTypeId] )
GO

CREATE INDEX 
	x_RatePlan_TenantId ON [dbo].[RatePlan]( [TenantId] )
GO