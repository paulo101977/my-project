﻿CREATE TABLE [dbo].[BillingAccountItemType]
(
	[BillingAccountItemTypeId]				INTEGER				NOT NULL,
	[BillingAccountItemTypeName]			VARCHAR(100)		NOT NULL,

	CONSTRAINT [PK_BillingAccountItemType] PRIMARY KEY ( [BillingAccountItemTypeId] )
)
GO