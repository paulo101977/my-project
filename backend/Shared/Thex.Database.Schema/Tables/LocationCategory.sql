﻿CREATE TABLE [dbo].[LocationCategory]
(
    [LocationCategoryId]	INT			NOT NULL,
    [Name]					VARCHAR(50)	NOT NULL,
	[RecordScope]			CHAR(1)		NOT NULL	DEFAULT 'U',

    CONSTRAINT [PK_LocationCategory] PRIMARY KEY ([LocationCategoryId]),

	CONSTRAINT [CK_LocationCategory_RecordScope] CHECK ([RecordScope] IN ('U', 'S'))
);
