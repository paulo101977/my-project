﻿CREATE TABLE [dbo].[RoomTypeBedType]
(
	[RoomTypeId]	INT				NOT NULL,
	[BedTypeId]		INT				NOT NULL,
	[Count]			INT DEFAULT 1	NOT NULL,
	[OptionalLimit]	INT DEFAULT 0	NOT NULL,

	CONSTRAINT [PK_RoomTypeBedType] PRIMARY KEY ([RoomTypeId], [BedTypeId]),
	CONSTRAINT [FK_RoomTypeBedType_RoomType] FOREIGN KEY([RoomTypeId]) REFERENCES [dbo].[RoomType] ([RoomTypeId]),
	CONSTRAINT [FK_RoomTypeBedType_BedType] FOREIGN KEY([BedTypeId]) REFERENCES [dbo].[BedType] ([BedTypeId])
)
GO