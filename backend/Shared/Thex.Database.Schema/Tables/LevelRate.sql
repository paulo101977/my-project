﻿CREATE TABLE [dbo].[LevelRate]
(
	[LevelRateId]					UNIQUEIDENTIFIER	NOT NULL,
	[PropertyId]					INT					NOT NULL,
	[MealPlanTypeDefault]			INT					NOT NULL	DEFAULT 1,
	[MealPlanTypeId]				INT					NOT NULL,
	[RoomTypeId]					INT					NOT NULL,
	[Pax_1_Amount]					NUMERIC (18,4)		NULL,
	[Pax_2_Amount]					NUMERIC (18,4)		NULL,
	[Pax_3_Amount]					NUMERIC (18,4)		NULL,
	[Pax_4_Amount]					NUMERIC (18,4)		NULL,
	[Pax_5_Amount]					NUMERIC (18,4)		NULL,
	[Pax_6_Amount]					NUMERIC (18,4)		NULL,
	[Pax_7_Amount]					NUMERIC (18,4)		NULL,
	[Pax_8_Amount]					NUMERIC (18,4)		NULL,
	[Pax_9_Amount]					NUMERIC (18,4)		NULL,
	[Pax_10_Amount]					NUMERIC (18,4)		NULL,
	[Pax_11_Amount]					NUMERIC (18,4)		NULL,
	[Pax_12_Amount]					NUMERIC (18,4)		NULL,
	[Pax_13_Amount]					NUMERIC (18,4)		NULL,
	[Pax_14_Amount]					NUMERIC (18,4)		NULL,
	[Pax_15_Amount]					NUMERIC (18,4)		NULL,
	[Pax_16_Amount]					NUMERIC (18,4)		NULL,
	[Pax_17_Amount]					NUMERIC (18,4)		NULL,
	[Pax_18_Amount]					NUMERIC (18,4)		NULL,
	[Pax_19_Amount]					NUMERIC (18,4)		NULL,
	[Pax_20_Amount]					NUMERIC (18,4)		NULL,
	[Pax_Additional_Amount]			NUMERIC (18,4)		NULL,
	[Child_1_Amount]				NUMERIC (18,4)		NULL,
	[Child_2_Amount]				NUMERIC (18,4)		NULL,
	[Child_3_Amount]				NUMERIC (18,4)		NULL,
	[Adult_MealPlan_Amount]			NUMERIC (18,4)		NULL,
	[Child_1_MealPlan_Amount]		NUMERIC (18,4)		NULL,
	[Child_2_MealPlan_Amount]		NUMERIC (18,4)		NULL,
	[Child_3_MealPlan_Amount]		NUMERIC (18,4)		NULL,
	[CurrencyId]					UNIQUEIDENTIFIER	NOT NULL,
	[CurrencySymbol]				VARCHAR(10)			NULL,
	[LevelRateHeaderId]				UNIQUEIDENTIFIER	NULL,

	[TenantId]						UNIQUEIDENTIFIER	NOT NULL,
	[IsDeleted]						BIT					NOT NULL	DEFAULT 0,
	[CreationTime]					DATETIME			NOT NULL	DEFAULT GETUTCDATE(),
	[CreatorUserId]					UNIQUEIDENTIFIER	NULL,
	[LastModificationTime]			DATETIME			NULL		DEFAULT GETUTCDATE(),
	[LastModifierUserId]			UNIQUEIDENTIFIER	NULL,
	[DeletionTime]					DATETIME			NULL,
	[DeleterUserId]					UNIQUEIDENTIFIER	NULL,

	CONSTRAINT [PK_LevelRate] PRIMARY KEY ([LevelRateId]),
	CONSTRAINT [FK_LevelRate_Property] FOREIGN KEY ([PropertyId]) REFERENCES [dbo].[Property] ([PropertyId]),
	CONSTRAINT [FK_LevelRate_RoomType] FOREIGN KEY ([RoomTypeId]) REFERENCES [dbo].[RoomType] ([RoomTypeId]),
	CONSTRAINT [FK_LevelRate_MealPlanType] FOREIGN KEY ([MealPlanTypeId]) REFERENCES [dbo].[MealPlanType] ([MealPlanTypeId]),
	CONSTRAINT [FK_LevelRate_Currency] FOREIGN KEY ([CurrencyId]) REFERENCES [dbo].[Currency] ([CurrencyId]),
	CONSTRAINT [FK_LevelRate_LevelRateHeader] FOREIGN KEY ([LevelRateHeaderId]) REFERENCES [dbo].[LevelRateHeader] ([LevelRateHeaderId]),
	CONSTRAINT [FK_LevelRate_Tenant] FOREIGN KEY ([TenantId]) REFERENCES [dbo].[Tenant] ([TenantId]),
)

GO

CREATE INDEX 
	x_LevelRate_PropertyId ON dbo.[PropertyBaseRateHistory] ( [PropertyId] )
GO

CREATE INDEX 
	x_LevelRate_RoomTypeId ON dbo.[PropertyBaseRateHistory] ( [RoomTypeId] )
GO

CREATE INDEX 
	x_LevelRate_MealPlanTypeId ON dbo.[PropertyBaseRateHistory] ( [MealPlanTypeId] )
GO

CREATE INDEX 
	x_LevelRate_CurrencyId ON dbo.[PropertyBaseRateHistory] ( [CurrencyId] )
GO

CREATE INDEX 
	x_LevelRate_TenantId ON [dbo].[PropertyBaseRateHistory]( [TenantId] )
GO

CREATE INDEX 
	x_LevelRate_LevelRateHeaderId ON dbo.[PropertyBaseRateHeaderHistory] ( [PropertyBaseRateHeaderHistoryId] )
GO
