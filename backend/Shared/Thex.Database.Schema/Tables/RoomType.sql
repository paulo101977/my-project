﻿CREATE TABLE [dbo].[RoomType]
(
	[RoomTypeId]			INT IDENTITY	NOT NULL,
	[PropertyId]			INT				NOT NULL,
	[Order]					INT				NOT NULL,
	[Name]					VARCHAR (100)	NOT NULL,
	[Abbreviation]			VARCHAR (64)	NOT NULL,
	[AdultCapacity]			INT				NOT NULL,
	[ChildCapacity]			INT				NOT NULL,
	[FreeChildQuantity_1]	INT				NOT NULL	DEFAULT 0,
	[FreeChildQuantity_2]	INT				NOT NULL	DEFAULT 0,
	[FreeChildQuantity_3]	INT				NOT NULL	DEFAULT 0,
	[IsActive]					BIT				NOT NULL,
	[MaximumRate]			Decimal(12,4)  NOT NULL,
	[MinimumRate]			    Decimal(12,4)  NOT NULL,
	[DistributionCode]        VARCHAR (100)	NOT NULL,      

	CONSTRAINT [PK_RoomType] PRIMARY KEY ([RoomTypeId]),
	CONSTRAINT [FK_RoomType_Property] FOREIGN KEY ([PropertyId]) REFERENCES [dbo].[Property] ([PropertyId]),
	CONSTRAINT [UK_RoomType_Abbreviation] UNIQUE ([PropertyId], [Abbreviation])
);
GO

CREATE INDEX 
	x_RoomType_PropertyId ON dbo.RoomType( [PropertyId] )
GO