﻿CREATE TABLE [dbo].[PropertyGuestPrefs]
(
	[GuestId]					BIGINT				NOT NULL,
	[PropertyId]				INTEGER				NOT NULL,
	[PreferredRoomId]			INTEGER				NULL,
	[LastRoomId]				INTEGER				NULL,
	[TenantId]					UNIQUEIDENTIFIER	NOT NULL,

	[IsDeleted]					BIT					NOT NULL	DEFAULT 0,
	[CreationTime]				DATETIME			NOT NULL	DEFAULT GETUTCDATE(),
	[CreatorUserId]				UNIQUEIDENTIFIER	NULL,
	[LastModificationTime]		DATETIME			NULL		DEFAULT GETUTCDATE(),
	[LastModifierUserId]		UNIQUEIDENTIFIER	NULL,
	[DeletionTime]				DATETIME			NULL,
	[DeleterUserId]				UNIQUEIDENTIFIER	NULL,

	CONSTRAINT [PK_PropertyGuestPrefs] PRIMARY KEY ([GuestId],[PropertyId]),
	CONSTRAINT [FK_PropertyGuestPrefs_Guest] FOREIGN KEY ([GuestId]) REFERENCES [dbo].[Guest] ([GuestId]),
	CONSTRAINT [FK_PropertyGuestPrefs_Property] FOREIGN KEY ([PropertyId]) REFERENCES [dbo].[Property] ([PropertyId]),
	CONSTRAINT [FK_PropertyGuestPrefs_PreferredRoomId_Room] FOREIGN KEY ([PreferredRoomId]) REFERENCES [dbo].[Room] ([RoomId]),
	CONSTRAINT [FK_PropertyGuestPrefs_LastRoomId_Room] FOREIGN KEY ([LastRoomId]) REFERENCES [dbo].[Room] ([RoomId]),
	CONSTRAINT [FK_PropertyGuestPrefs_Tenant] FOREIGN KEY ([TenantId]) REFERENCES [dbo].[Tenant] ([TenantId])
)
GO

ALTER TABLE [dbo].[PropertyGuestPrefs] CHECK CONSTRAINT [FK_PropertyGuestPrefs_PreferredRoomId_Room]
GO
ALTER TABLE [dbo].[PropertyGuestPrefs] CHECK CONSTRAINT [FK_PropertyGuestPrefs_Property]
GO

CREATE INDEX 
	x_PropertyGuestPrefs_PreferredRoomId ON dbo.PropertyGuestPrefs( [PreferredRoomId] )
GO

CREATE INDEX 
	x_PropertyGuestPrefs_LastRoomId ON dbo.PropertyGuestPrefs( [LastRoomId] )
GO

CREATE INDEX 
	x_PropertyGuestPrefs_TenantId ON [dbo].[PropertyGuestPrefs]( [TenantId] )
GO
