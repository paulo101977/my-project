﻿CREATE TABLE [dbo].[IdentityProductProperty]
(
	[IdentityProductPropertyId] INT IDENTITY(1,1) NOT NULL, 
    [IdentityProductId] INT NOT NULL, 
    [PropertyId] INT NOT NULL, 
    [IsActive] BIT NOT NULL, 
    [IsDeleted] BIT NOT NULL, 
    [CreationTime] DATETIME NOT NULL, 
    [CreatorUserId] UNIQUEIDENTIFIER NULL, 
    [LastModificationTime] DATETIME NULL, 
    [LastModifierUserId] UNIQUEIDENTIFIER NULL, 
    [DeletionTime] DATETIME NULL, 
    [DeleterUserId] UNIQUEIDENTIFIER NULL,
	
    CONSTRAINT [PK_IdentityProductProperty] PRIMARY KEY ([IdentityProductPropertyId]),
	CONSTRAINT [FK_IdentityProduct_Property] FOREIGN KEY ([IdentityProductId]) REFERENCES [dbo].[IdentityProduct] ( [IdentityProductId] ),
)
