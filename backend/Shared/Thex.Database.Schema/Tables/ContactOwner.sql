﻿CREATE TABLE [dbo].[ContactOwner]
(
	[ContactOwnerId]	UNIQUEIDENTIFIER	NOT NULL,
	[PersonId]			UNIQUEIDENTIFIER	NOT NULL,
	[OwnerId]			UNIQUEIDENTIFIER	NOT NULL,
	[OccupationId]		INTEGER				NULL,

	CONSTRAINT [PK_ContactOwner] PRIMARY KEY ([ContactOwnerId]),
	CONSTRAINT [UK_ContactOwnerPerson] UNIQUE ([PersonId], [OwnerId]),
	CONSTRAINT [FK_ContactOwner_Person] FOREIGN KEY ([PersonId]) REFERENCES [dbo].[Person] ([PersonId]),
	CONSTRAINT [FK_ContactOwner_Occupation] FOREIGN KEY ([OccupationId]) REFERENCES [dbo].[Occupation] ([OccupationId])
)
GO

CREATE INDEX 
	x_CompanyOwner_Occupation ON dbo.Occupation( [OccupationId] )
GO