﻿CREATE TABLE [dbo].[PropertyContract]
(
	[PropertycontractId]			UNIQUEIDENTIFIER	NOT NULL, 
    [PropertyId]					INT					NOT NULL, 
    [MaxUh]							INT					NOT NULL,
	
	[IsDeleted]						BIT					NOT NULL	DEFAULT 0,
	[CreationTime]					DATETIME			NOT NULL	DEFAULT GETUTCDATE(),
	[CreatorUserId]					UNIQUEIDENTIFIER	NOT NULL,
	[LastModificationTime]			DATETIME			NULL		DEFAULT GETUTCDATE(),
	[LastModifierUserId]			UNIQUEIDENTIFIER	NULL,
	[DeletionTime]					DATETIME			NULL,
	[DeleterUserId]					UNIQUEIDENTIFIER	NULL,

	CONSTRAINT [PK_PropertyContract] PRIMARY KEY ([PropertyContractId]),
	CONSTRAINT [FK_PropertyContract_Property] FOREIGN KEY ( [PropertyId] ) REFERENCES [dbo].[Property] ( [PropertyId] ),
	CONSTRAINT [UK_PropertyContract_PropertyId] UNIQUE ([PropertyId])
)

GO

CREATE INDEX 
	x_PropertyContract_PropertyId ON dbo.[PropertyContract] ( [PropertyId] )
