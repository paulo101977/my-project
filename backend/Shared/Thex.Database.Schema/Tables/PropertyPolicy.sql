﻿CREATE TABLE [dbo].[PropertyPolicy]
(
	[PropertyPolicyId]			UNIQUEIDENTIFIER	NOT NULL,
	[PropertyId]				INT 				NOT NULL,
	[PolicyTypeId]				INT					NOT NULL,
	[CountryId]				    INT					NULL,
	[Description]				VARCHAR(4000)		NULL,
	[IsActive]					BIT					NOT NULL	DEFAULT 1,
	[TenantId]					UNIQUEIDENTIFIER	NOT NULL,
	[IsDeleted]					BIT					NOT NULL	DEFAULT 0,
	[CreationTime]				DATETIME			NOT NULL	DEFAULT GETUTCDATE(),
	[CreatorUserId]				UNIQUEIDENTIFIER	NULL,
	[LastModificationTime]		DATETIME			NULL		DEFAULT GETUTCDATE(),
	[LastModifierUserId]		UNIQUEIDENTIFIER	NULL,
	[DeletionTime]				DATETIME			NULL,
	[DeleterUserId]				UNIQUEIDENTIFIER	NULL,
	
	CONSTRAINT [PK_PropertyPolicy] PRIMARY KEY ([PropertyPolicyId]),
	CONSTRAINT [FK_PropertyPolicy_PolicyType]	FOREIGN KEY ([PolicyTypeId]) REFERENCES [dbo].[PolicyType] ([PolicyTypeId]),
	CONSTRAINT [FK_PropertyPolicy_Property]	FOREIGN KEY ([PropertyId]) REFERENCES [dbo].[Property] ([PropertyId]),
	CONSTRAINT [FK_PropertyPolicy_Tenant] FOREIGN KEY ([TenantId]) REFERENCES [dbo].[Tenant] ([TenantId]),
	CONSTRAINT [FK_PropertyPolicy_CountrySubdivision] FOREIGN KEY ([CountryId]) REFERENCES [dbo].CountrySubdivision ([CountrySubdivisionId])
)
GO

CREATE INDEX 
	x_PropertyPolicy_PropertyId ON [dbo].[PropertyPolicy] ( [PropertyId] )
GO


CREATE INDEX 
	x_PropertyPolicy_PolicyTypeId ON [dbo].[PropertyPolicy] ( [PolicyTypeId] )
GO

CREATE INDEX 
	x_PropertyPolicy_TenantId ON [dbo].[PropertyPolicy]( [TenantId] )
GO

CREATE INDEX 
	x_PropertyPolicy_CountryId ON [dbo].[PropertyPolicy]( [CountryId] )
GO