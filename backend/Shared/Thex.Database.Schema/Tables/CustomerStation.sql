﻿CREATE TABLE [dbo].[CustomerStation]
(
	[CustomerStationId]				UNIQUEIDENTIFIER	NOT NULL,
	[CustomerStationName]			varchar(150)		NOT NULL,
	[SocialReason]					varchar(150)		NOT NULL,
	[PropertyCompanyClientCategoryId]		UNIQUEIDENTIFIER	NULL,
	[IsActive]						bit					NOT NULL	DEFAULT 1,
	[CompanyClientId]				UNIQUEIDENTIFIER	NOT NULL,
	[HomePage]						varchar(150)		NULL,

	[IsDeleted]						BIT					NOT NULL	DEFAULT 0,
	[CreationTime]					DATETIME			NOT NULL	DEFAULT GETUTCDATE(),
	[CreatorUserId]					UNIQUEIDENTIFIER	NULL,
	[LastModificationTime]			DATETIME			NULL		DEFAULT GETUTCDATE(),
	[LastModifierUserId]			UNIQUEIDENTIFIER	NULL,
	[DeletionTime]					DATETIME			NULL,
	[DeleterUserId]					UNIQUEIDENTIFIER	NULL,

	CONSTRAINT [PK_CustomerStation] PRIMARY KEY ([CustomerStationId]),
	CONSTRAINT [FK_CustomerStation_CompanyClient] FOREIGN KEY ([CompanyClientId]) REFERENCES [CompanyClient]([CompanyClientId]),
	CONSTRAINT [FK_CustomerStation_PropertyCompanyClientCategory] FOREIGN KEY ([PropertyCompanyClientCategoryId]) REFERENCES [dbo].[PropertyCompanyClientCategory] ([PropertyCompanyClientCategoryId]),

);
GO

CREATE INDEX 
	x_CustomerStation_CompanyClientId ON dbo.CustomerStation( [CompanyClientId] )

GO

CREATE INDEX 
	x_CustomerStation_PropertyCompanyClientCategoryId ON dbo.CompanyClient( [PropertyCompanyClientCategoryId] )
