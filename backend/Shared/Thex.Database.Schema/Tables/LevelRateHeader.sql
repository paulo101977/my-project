﻿CREATE TABLE [dbo].[LevelRateHeader]
(
	[LevelRateHeaderId]				UNIQUEIDENTIFIER	NOT NULL,
	[RateName]						VARCHAR(100)		NOT NULL,
	[InitialDate]					DATETIME			NOT NULL,
	[EndDate]						DATETIME			NOT NULL,
	[LevelId]						UNIQUEIDENTIFIER	NOT NULL,
	[PropertyMealPlanTypeRateHeaderId]	UNIQUEIDENTIFIER	NULL,
	[PropertyMealPlanTypeRate]		BIT						NULL,		
	[IsActive]						BIT					NOT NULL	DEFAULT 0,
	[CurrencyId]					UNIQUEIDENTIFIER	NOT NULL,

	[TenantId]						UNIQUEIDENTIFIER	NOT NULL,
	[IsDeleted]						BIT					NOT NULL	DEFAULT 0,
	[CreationTime]					DATETIME			NOT NULL	DEFAULT GETUTCDATE(),
	[CreatorUserId]					UNIQUEIDENTIFIER	NULL,
	[LastModificationTime]			DATETIME			NULL		DEFAULT GETUTCDATE(),
	[LastModifierUserId]			UNIQUEIDENTIFIER	NULL,
	[DeletionTime]					DATETIME			NULL,
	[DeleterUserId]					UNIQUEIDENTIFIER	NULL,

	CONSTRAINT [PK_LevelRateHeader] PRIMARY KEY ([LevelRateHeaderId]),
	CONSTRAINT [FK_LevelRateHeader_Level] FOREIGN KEY ([LevelId]) REFERENCES [dbo].[Level] ([LevelId]),
	CONSTRAINT [FK_LevelRateHeader_Currency] FOREIGN KEY ([CurrencyId]) REFERENCES [dbo].[Currency] ([CurrencyId]),
	CONSTRAINT [FK_LevelRateHeader_Tenant] FOREIGN KEY ([TenantId]) REFERENCES [dbo].[Tenant] ([TenantId]),
)

GO

CREATE INDEX 
	x_LevelRateHeader_LevelId ON [dbo].[LevelRateHeader]( [LevelId] )
GO

CREATE INDEX 
	x_LevelRateHeader_CurrencyId ON [dbo].[LevelRateHeader]( [CurrencyId] )
GO

CREATE INDEX 
	x_LevelRateHeader_TenantId ON [dbo].[LevelRateHeader]( [TenantId] )
GO
