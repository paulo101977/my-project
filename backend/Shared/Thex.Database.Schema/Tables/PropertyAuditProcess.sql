﻿CREATE TABLE [dbo].[PropertyAuditProcess]
(
	[PropertyAuditProcessId] UNIQUEIDENTIFIER	NOT NULL	PRIMARY KEY,
	[PropertySystemDate]	DATETIME			NOT NULL,
	[StartDate]				DateTime			NOT NULL	DEFAULT GETUTCDATE(),
	[EndDate]				DateTime			NULL,
	[PropertyId]			INT					NOT NULL,
	[AuditStepTypeId]		INT					NOT NULL,
	[TenantId]									UNIQUEIDENTIFIER	NOT NULL,
	[IsDeleted]									BIT					NOT NULL	DEFAULT 0,
	[CreationTime]								DATETIME			NOT NULL	DEFAULT GETUTCDATE(),
	[CreatorUserId]								UNIQUEIDENTIFIER	NULL,
	[LastModificationTime]						DATETIME			NULL		DEFAULT GETUTCDATE(),
	[LastModifierUserId]						UNIQUEIDENTIFIER	NULL,
	[DeletionTime]								DATETIME			NULL,
	[DeleterUserId]								UNIQUEIDENTIFIER	NULL,

	CONSTRAINT [FK_PropertyAuditProcessControl_Property] FOREIGN KEY ([PropertyId]) REFERENCES [dbo].[Property] ([PropertyId]),
	CONSTRAINT [FK_PropertyAuditProcessControl_AuditStepType] FOREIGN KEY ([AuditStepTypeId]) REFERENCES [dbo].[AuditStepType] ([AuditStepTypeId]),
	CONSTRAINT [FK_PropertyAuditProcessControl_Tenant] FOREIGN KEY ([TenantId]) REFERENCES [dbo].[Tenant] ([TenantId])
)
GO

CREATE INDEX 
	x_PropertyAuditProcess_PropertyId ON [dbo].[PropertyAuditProcess] ( [PropertyId] ) 
GO

CREATE INDEX 
	x_PropertyAuditProcess_AuditStepTypeId ON [dbo].[PropertyAuditProcess] ( [AuditStepTypeId] ) 
GO

CREATE INDEX 
	x_PropertyAuditProcess_TenantId ON [dbo].[PropertyAuditProcess] ( [TenantId] ) 
GO