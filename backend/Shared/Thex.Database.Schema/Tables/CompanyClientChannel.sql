﻿CREATE TABLE [dbo].[CompanyClientChannel]
(
	[CompanyClientChannelId]		UNIQUEIDENTIFIER	NOT NULL,
	[ChannelId]						UNIQUEIDENTIFIER	NOT NULL,
	[CompanyClientId]				UNIQUEIDENTIFIER	NOT NULL,
	[IsActive]						BIT					NOT NULL	DEFAULT 1,
	[CompanyId]						VARCHAR(80) 		NOT NULL,

	CONSTRAINT [PK_CompanyClientChannel] PRIMARY KEY ([CompanyClientChannelId]),
	CONSTRAINT [UK_CompanyClientChannel] UNIQUE ([ChannelId], [CompanyClientId]),
	CONSTRAINT [FK_CompanyClientChannel_Channel] FOREIGN KEY ([ChannelId]) REFERENCES [dbo].[Channel] ([ChannelId]),
	CONSTRAINT [FK_CompanyClientChannel_Company] FOREIGN KEY ([CompanyClientId]) REFERENCES [dbo].[CompanyClient] ([CompanyClientId]),
)

GO

CREATE INDEX 
	x_CompanyClientChannel_ChannelId ON dbo.Channel( [ChannelId] )
GO

CREATE INDEX 
	x_CompanyClientChannel_CompanyClientId ON dbo.[CompanyClient]( [CompanyClientId] )
GO
