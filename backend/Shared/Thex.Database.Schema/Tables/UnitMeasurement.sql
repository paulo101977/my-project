﻿CREATE TABLE [dbo].[UnitMeasurement]
(
	[UnitMeasurementId]			UNIQUEIDENTIFIER	NOT NULL,
	[Name]						VARCHAR (20)		NOT NULL,
	[Initials]					VARCHAR (5)		    NOT NULL	

	CONSTRAINT [PK_UnitMeasurement] PRIMARY KEY ([UnitMeasurementId]),
);
GO
