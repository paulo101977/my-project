﻿--CREATE TRIGGER dbo.BillingAccountItem_UpdateBillingAccountAmount
--    ON dbo.BillingAccountItem
--    AFTER INSERT, UPDATE, DELETE
--AS
--BEGIN
--    SET NOCOUNT ON;
	
--    DECLARE @billingaccountid as uniqueidentifier;
--	DECLARE @oldbillingaccountid as uniqueidentifier;
--    DECLARE @newaccountbalance as numeric(18,4);
    
--	SET @billingaccountid = '00000000-0000-0000-0000-000000000000';
--	SET @oldbillingaccountid = '00000000-0000-0000-0000-000000000000';

--    IF NOT EXISTS(SELECT 1 FROM INSERTED)
--		SELECT 
--			@billingaccountid = BillingAccountId, 
--			@oldbillingaccountid = ISNULL(BillingAccountIdLastSource, @oldbillingaccountid) 
--		FROM DELETED;
--    ELSE
--		SELECT 
--			@billingaccountid = BillingAccountId, 
--			@oldbillingaccountid = ISNULL(BillingAccountIdLastSource, @oldbillingaccountid)  
--		FROM INSERTED;
    
--	SELECT @newaccountbalance = SUM(BillingAccountItem.Amount) FROM BillingAccountItem WHERE BillingAccountId = @billingaccountid;
	
--	UPDATE BillingAccount
--	SET BillingAccount.AccountBalance = ISNULL(@newaccountbalance, 0)
--	WHERE BillingAccountId = @billingaccountid;

--	IF(@oldbillingaccountid <> '00000000-0000-0000-0000-000000000000')
--		BEGIN
--			SELECT @newaccountbalance = SUM(BillingAccountItem.Amount) FROM BillingAccountItem WHERE BillingAccountId = @oldbillingaccountid;
			
--			UPDATE BillingAccount
--			SET BillingAccount.AccountBalance = ISNULL(@newaccountbalance, 0)
--			WHERE BillingAccountId = @oldbillingaccountid;
--		END

--END;