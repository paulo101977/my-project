﻿namespace System
{
    public static class ObjectExtensions
    {
        public static Int64 TryParseToInt64(this object obj)
        {
            Int64 ret = 0;

            try
            {
                ret = Convert.ToInt64(obj);
            }
            catch (Exception)
            {
            }

            return ret;
        }

        public static int TryParseToInt32(this object obj)
        {
            int ret = 0;

            try
            {
                ret = Convert.ToInt32(obj);
            }
            catch (Exception)
            {
            }

            return ret;
        }

        public static decimal TryParseToDecimal(this object obj)
        {
            decimal ret = 0;

            try
            {
                ret = Convert.ToDecimal(obj);
            }
            catch (Exception)
            {
            }

            return ret;
        }
    }
}
