﻿using NodaTime;
using System;
using Microsoft.AspNetCore.Http;
using Thex.Kernel;

namespace System
{
    public static class DateTimeExtensions
    {
        public static int GetAge(this DateTime? birthdate)
        {
            if (birthdate == null)
                return 0;

            // Save today's date.
            DateTime today = DateTime.Today;
            // Calculate the age.
            int age = (int)(today.Year - birthdate?.Year);
            // Go back to the year the person was born in case of a leap year
            if (birthdate > today.AddYears(-age)) age--;
            return age;
        }

        public static int NumberOfNights(this DateTime date1, DateTime date2)
        {
            var frm = date1 < date2 ? date1 : date2;
            var to = date1 < date2 ? date2 : date1;

            return (int)(to.Date - frm.Date).TotalDays;
        }

        public static DateTime ToZonedDateTime(this DateTime dateTime, string timeZoneName)
        {
            if (dateTime == DateTime.MinValue)
                return dateTime;

            var timeZone = DateTimeZoneProviders.Tzdb[timeZoneName];

            var instant = Instant.FromDateTimeUtc(DateTime.SpecifyKind(dateTime, DateTimeKind.Utc));
            return instant.InZone(timeZone).ToDateTimeUnspecified();
        }

        public static DateTime Yestarday(this DateTime dateTime)
        {
            return dateTime.AddDays(-1); 
        }

        public static DateTime ToZonedDateTimeLoggedUser(this DateTime dateTime, IApplicationUser applicationUser = null)
        {
            var contextAccessor = ServiceLocator.Current.GetInstance<IHttpContextAccessor>();
            applicationUser = applicationUser ?? new ApplicationUser(contextAccessor);

            var timeZoneName = applicationUser != null && !string.IsNullOrEmpty(applicationUser.TimeZoneName) ?
                                applicationUser.TimeZoneName :
                                KernelConsts.DEFAULT_TIME_ZONE_NAME;

            return dateTime.ToZonedDateTime(timeZoneName);
        }
    }
}
