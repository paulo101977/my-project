﻿using System;
using System.Collections.Generic;

namespace System.Linq
{
    public static class EnumerableExtensions
    {
        /// <summary>
        /// Determines whether the collection contains elements.
        /// </summary>
        /// <typeparam name="T">The IEnumerable type.</typeparam>
        /// <param name="enumerable">The enumerable.</param>
        /// <returns><c>true</c> if the IEnumerable contains element; otherwise, <c>false</c>.</returns>
        public static bool ContainsElement<T>(this IEnumerable<T> enumerable)
        {
            if (enumerable == null)
                return false;

            return enumerable.Any();
        }

        public static IEnumerable<TSource> DistinctBy<TSource, TKey>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector)
        {
            HashSet<TKey> seenKeys = new HashSet<TKey>();
            foreach (TSource element in source)
            {
                if (seenKeys.Add(keySelector(element)))
                {
                    yield return element;
                }
            }
        }
    }
}
