﻿using System;
using System.Reflection;
using System.Threading.Tasks;

namespace Thex.Kernel
{
    public interface IThexAuthorize
    {
        Task<bool> AuthorizeAsync(MethodInfo methodInfo, Guid userId, Guid tenantId, bool externalAccess);

        bool RequiresAuthorization(MethodInfo methodInfo);
    }
}
