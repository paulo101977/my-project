﻿using System;
using System.Collections.Generic;
using Thex.Kernel.Dto;

namespace Thex.Kernel
{
    public interface IApplicationUser
    {
        string Name { get; }
        Dictionary<string, string> GetClaimsIdentity();
        bool IsAuthenticated();
        string UserEmail { get; }
        Guid UserUid { get; }
        string UserName { get; set; }
        Guid TenantId { get; }
        string PropertyId { get; }
        string PropertyUid { get; }
        List<LoggedUserPropertyDto> PropertyList { get; }
        string ChainId { get; }
        bool IsAdmin { get; }
        string PreferredLanguage { get; }
        string PreferredCulture { get; }
        string PropertyLanguage { get; }
        string PropertyCulture { get; }
        string TimeZoneName { get; }
        string PropertyCountryCode { get; }
        string PropertyDistrictCode { get; }

        void SetProperties(string userUid, string tenantId, string propertyId, string timeZoneName);
    }
}