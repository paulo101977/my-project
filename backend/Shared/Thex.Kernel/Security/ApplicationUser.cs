﻿using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using Thex.Kernel.Dto;

namespace Thex.Kernel
{
    public class ApplicationUser : IApplicationUser
    {
        private readonly IHttpContextAccessor _accessor;
        public Dictionary<string, string> _claims;
        private readonly string TenantIdField = "TenantId";
        private readonly string LoggedUserEmailField = "LoggedUserEmail";
        private readonly string LoggedUserUidField = "LoggedUserUid";
        private readonly string LoggedUserNameField = "LoggedUserName";
        private readonly string PropertyIdField = "PropertyId";
        private readonly string PropertyUidField = "PropertyUid";
        private readonly string BrandIdField = "BrandId";
        private readonly string ChainIdField = "ChainId";
        private readonly string IsAdminField = "IsAdmin";
        private readonly string PreferredCultureField = "PreferredCulture";
        private readonly string PreferredLanguageField = "PreferredLanguage";
        private readonly string PropertyCultureField = "PropertyCulture";
        private readonly string PropertyLanguageField = "PropertyLanguage";
        private readonly string TimeZoneNameField = "TimeZoneName";
        private readonly string PropertyCountryCodeField = "PropertyCountryCode";
        private readonly string PropertyDistrictCodeField = "PropertyDistrictCode";
        private readonly string PropertyListDtoField = "PropertyListDto";

        public ApplicationUser(IHttpContextAccessor accessor)
        {
            _accessor = accessor;
            _claims = null;
        }

        public string Name => _accessor.HttpContext.User.Identity.Name;

        public string UserEmail
        {
            get
            {
                if (_claims == null)
                    _claims = GetClaimsIdentity();
                if (_claims.ContainsKey(LoggedUserEmailField))
                    return _claims[LoggedUserEmailField];
                else return "";
            }
        }

        public Guid UserUid
        {
            get
            {
                if (_claims == null)
                    _claims = GetClaimsIdentity();
                if (_claims.ContainsKey(LoggedUserUidField))
                    return new Guid(_claims[LoggedUserUidField]);
                else return Guid.Empty;
            }
        }

        public string UserName
        {
            get
            {
                if (_claims == null)
                    _claims = GetClaimsIdentity();
                if (_claims.ContainsKey(LoggedUserNameField))
                    return _claims[LoggedUserNameField];
                else return "";
            }
        }

        public Guid TenantId
        {
            get
            {
                if (_claims == null)
                    _claims = GetClaimsIdentity();
                if (_claims.ContainsKey(TenantIdField))
                    return new Guid(_claims[TenantIdField]);
                else return Guid.Empty;
            }
        }

        public string PropertyId
        {
            get
            {
                if (_claims == null)
                    _claims = GetClaimsIdentity();
                if (_claims.ContainsKey(PropertyIdField))
                    return _claims[PropertyIdField];
                else return "";
            }
        }

        public string PropertyUid
        {
            get
            {
                if (_claims == null)
                    _claims = GetClaimsIdentity();
                if (_claims.ContainsKey(PropertyUidField))
                    return _claims[PropertyUidField];
                else return "";
            }
        }


        public List<LoggedUserPropertyDto> PropertyList
        {
            get
            {
                if (_claims == null)
                    _claims = GetClaimsIdentity();
                if (_claims.ContainsKey(PropertyListDtoField) && !string.IsNullOrWhiteSpace(_claims[PropertyListDtoField]))
                {
                    var response = JArray.Parse(_claims[PropertyListDtoField]);
                    return response.Select(r => JsonConvert.DeserializeObject<LoggedUserPropertyDto>(r.ToString())).ToList();
                }
                else return new List<LoggedUserPropertyDto>();
            }
        }

        public string BrandId
        {
            get
            {
                if (_claims == null)
                    _claims = GetClaimsIdentity();
                if (_claims.ContainsKey(BrandIdField))
                    return _claims[BrandIdField];
                else return "";
            }
        }

        public string ChainId
        {
            get
            {
                if (_claims == null)
                    _claims = GetClaimsIdentity();
                if (_claims.ContainsKey(ChainIdField))
                    return _claims[ChainIdField];
                else return "";
            }
        }


        public bool IsAdmin
        {
            get
            {
                bool isAdminValue = false;
                if (_claims == null)
                    _claims = GetClaimsIdentity();
                if (_claims.ContainsKey(IsAdminField))
                {
                    Boolean.TryParse(_claims[IsAdminField], out isAdminValue);

                    return isAdminValue;
                }
                else return isAdminValue;
            }
        }

        public string PreferredCulture
        {
            get
            {
                if (_claims == null)
                    _claims = GetClaimsIdentity();
                if (_claims.ContainsKey(PreferredCultureField))
                    return _claims[PreferredCultureField];
                else return "";
            }
        }

        public string PreferredLanguage
        {
            get
            {
                if (_claims == null)
                    _claims = GetClaimsIdentity();
                if (_claims.ContainsKey(PreferredLanguageField))
                    return _claims[PreferredLanguageField];
                else return "";
            }
        }

        public string PropertyCulture
        {
            get
            {
                if (_claims == null)
                    _claims = GetClaimsIdentity();
                if (_claims.ContainsKey(PropertyCultureField))
                    return _claims[PropertyCultureField];
                else return "";
            }
        }

        public string PropertyLanguage
        {
            get
            {
                if (_claims == null)
                    _claims = GetClaimsIdentity();
                if (_claims.ContainsKey(PropertyLanguageField))
                    return _claims[PropertyLanguageField];
                else return "";
            }
        }


        public string TimeZoneName
        {
            get
            {
                if (_claims == null)
                    _claims = GetClaimsIdentity();
                if (_claims.ContainsKey(TimeZoneNameField))
                    return _claims[TimeZoneNameField];
                else return "";
            }
        }

        public string PropertyCountryCode
        {
            get
            {
                if (_claims == null)
                    _claims = GetClaimsIdentity();
                if (_claims.ContainsKey(PropertyCountryCodeField))
                    return _claims[PropertyCountryCodeField];
                else return "";
            }
        }

        public string PropertyDistrictCode
        {
            get
            {
                if (_claims == null)
                    _claims = GetClaimsIdentity();
                if (_claims.ContainsKey(PropertyDistrictCodeField))
                    return _claims[PropertyDistrictCodeField];
                else return "";
            }
        }

        string IApplicationUser.UserName { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }        

        public bool IsAuthenticated()
        {
            return _accessor.HttpContext.User.Identity.IsAuthenticated;
        }

        public virtual Dictionary<string, string> GetClaimsIdentity()
        {
            Dictionary<string, string> result = new Dictionary<string, string>();

            foreach (var claim in _accessor.HttpContext.User.Claims)
            {
                result.AddIfNotContains<KeyValuePair<string, string>>(new KeyValuePair<string, string>(claim.Type, claim.Value));
            }
            return result;
        }

        public void SetProperties(string userUid, string tenantId, string propertyId, string timeZoneName)
        {
            throw new NotImplementedException();
        }
    }
}
