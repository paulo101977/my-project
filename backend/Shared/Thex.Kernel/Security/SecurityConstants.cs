﻿namespace Thex.Kernel
{
    public class ThexSecurityConstants
    {
        public const string DEFAULT_TIME_ZONE_NAME = "America/Sao_Paulo";
    }

    public class ThexLoggingEventsConstants
    {
        public const int Authorization = 10003;
    }

    public class ThexSecurityNames
    {
        public const string InvalidUserApiCommunication = "A resposta obtida da comunicação com a api de Usuários é inválida";
        public const string NoAuthorizedAccess = "O acesso ao recurso {0} não foi autorizado. Verifique se o cliente OAuth {1} permissão.";
        public const string ResourceNotFound = "O recurso {0} não foi encontrado. Verifique se o endereço {0} existe e está disponível";
        public const string UserApiEstablishingConnectionError = "Erro ao estabelecer comunicação com a api de Usuários (Consulte o log para maiores detalhes).";
        public const string AccessTokenFoundInAuthorizationHeader = "Token encontrado no Authorization Header";
        public const string AccessTokenNotFoundInAuthorizationHeader = "Token não encontrado no Authorization Header. Não existe usuário autenticado.";
        public const string AccessToken = "Token de acesso {0}";
        public const string CheckingForAuthorization = "Verificando se o método {0} precisa de autorização";
        public const string NoRequiredAuthorization = "Nenhuma autorização é necessária para {0}";
        public const string NoUserFoundToAuthorize = "Nenhum usuário encontrado para autorizar o método {0}";
        public const string CheckingPermissionsForUser = "Verificando as permissões {0} para o usuário ({1}) do Tenant ({2})";
        public const string UserIsAuthorizedForPermissions = "Usuário ({0}) do Tenant ({1}) está autorizado para as permissões {2}";
        public const string UserIsAuthorized = "Usuário ({0}) do Tenant ({1}) está autorizado";
        public const string UserIsNotAuthorizedForPermissions = "Usuário ({0}) do Tenant ({1}) não está autorizado para as todas permissões {2}";
    }
}
