﻿namespace Thex.Kernel.Dto
{
    public class LoggedUserPropertyDto
    {
        public string PropertyId { get; set; }
        public string TenantId { get; set; }
        public string BrandId { get; set; }
        public string ChainId { get; set; }
        public string Name { get; set; }
        public string TimeZoneName { get; set; }
        public string PropertyCulture { get; set; }
        public string PropertyLanguage { get; set; }
        public string PropertyCountryCode { get; set; }
        public string PropertyDistrictCode { get; set; }
    }
}
