﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Logging;
using System.Linq;
using System.Threading.Tasks;
using Tnf.AspNetCore.Mvc.Response;
using Tnf.Localization;
using Tnf.Notifications;
using Microsoft.AspNetCore.Http;
using Tnf;
using System;
using Thex.Kernel;

namespace Thex.AspNetCore.Mvc.Authorization
{
    public class ThexAuthorizationFilter : IAsyncAuthorizationFilter
    {
        private readonly ILogger _logger;
        private readonly IThexAuthorize _thexAuthorize;
        private readonly INotificationHandler _notification;
        private readonly ILocalizationManager _localizationManager;

        private const string AnonymousRequest = "Request anônimo, não verifica permissão";
        private const string NoUserAuthenticated = "Nenhum usuário está autenticado";
        private const string AuthorizationErroMethod = "Erro ao tentar autorizar o método {0}";

        public ThexAuthorizationFilter(
            ILogger<ThexAuthorizationFilter> logger,
            IThexAuthorize thexAuthorize,
            INotificationHandler notification,
            ILocalizationManager localizationManager)
        {
            _logger = logger;
            _thexAuthorize = thexAuthorize;
            _notification = notification;
            _localizationManager = localizationManager;
        }

        public async Task OnAuthorizationAsync(AuthorizationFilterContext context)
        {
            if (context.Filters.Any(item => item is IAllowAnonymousFilter))
            {
                _logger.LogDebug(ThexLoggingEventsConstants.Authorization, AnonymousRequest);
                return;
            }

            var methodInfo = context.ActionDescriptor.GetMethodInfo();

            if (!_thexAuthorize.RequiresAuthorization(methodInfo))
                return;

            try
            {
                Guid userId = context.HttpContext.GetThexUserId();
                Guid tenantId = context.HttpContext.GetThexTenantId();
                bool externalAccess = context.HttpContext.GetExternalAccess();
                
                if (!externalAccess && userId == Guid.Empty)
                {
                    _logger.LogInformation(ThexLoggingEventsConstants.Authorization, NoUserAuthenticated);
                    BuildResponseError(context, ResultStatus.Unauthorized);
                    return;
                }
                if (!await _thexAuthorize.AuthorizeAsync(methodInfo, userId, tenantId, externalAccess).ForAwait())
                    BuildResponseError(context, ResultStatus.Unauthorized);
            }
            catch (Exception ex)
            {
                _logger.LogError(ThexLoggingEventsConstants.Authorization, ex, AuthorizationErroMethod, methodInfo);

                BuildResponseError(context, ResultStatus.InternalServerError);
            }
        }

        private void BuildResponseError(AuthorizationFilterContext context, ResultStatus status)
        {
            var controllerMethod = context.HttpContext.Request.Method;
            var securityErrorEnum = TnfController.Error.AspNetCoreOnGetError;

            switch (controllerMethod)
            {
                case "POST":
                    securityErrorEnum = TnfController.Error.AspNetCoreOnPostError;
                    break;
                case "PUT":
                    securityErrorEnum = TnfController.Error.AspNetCoreOnPutError;
                    break;
                case "DELETE":
                    securityErrorEnum = TnfController.Error.AspNetCoreOnDeleteError;
                    break;
                case "PATCH":
                    securityErrorEnum = TnfController.Error.AspNetCoreOnPatchError;
                    break;
            }

            var response = ErrorResponse
                .DefaultBuilder
                .WithLocalizationManager(_localizationManager)
                .FromErrorEnum(securityErrorEnum)
                .WithMessage(TnfConsts.LocalizationSourceName, securityErrorEnum)
                .WithMessageFormat(controllerMethod)
                .WithDetails(_notification.GetAll())
                .Build();

            context.HttpContext.Response.StatusCode = (int)status;
            context.Result = new ObjectResult(response);
        }
    }
}
