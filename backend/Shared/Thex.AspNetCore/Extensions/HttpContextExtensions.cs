﻿using System;
using System.Linq;
using Thex.Kernel;

namespace Microsoft.AspNetCore.Http
{
    public static class HttpContextExtensions
    {
        public static Guid GetThexTenantId(this HttpContext context)
        {
            if (context == null)
                throw new ArgumentNullException(nameof(context));

            Guid tenantId = Guid.Empty;

            Guid.TryParse(context.User?.Claims?.FirstOrDefault(c => c.Type == TokenInfoClaims.TenantId)?.Value, out tenantId);

            return tenantId;
        }

        public static Guid GetThexUserId(this HttpContext context)
        {
            if (context == null)
                throw new ArgumentNullException(nameof(context));

            Guid userId = Guid.Empty;

            Guid.TryParse(context.User?.Claims?.FirstOrDefault(c => c.Type == TokenInfoClaims.LoggedUserUid)?.Value, out userId);

            return userId;
        }

        public static bool GetExternalAccess(this HttpContext context)
        {
            if (context == null)
                throw new ArgumentNullException(nameof(context));

            bool externalAccess = false;

            bool.TryParse(context.User?.Claims?.FirstOrDefault(c => c.Type == TokenInfoClaims.IsExternalAccess)?.Value, out externalAccess);

            return externalAccess;
        }

        public static Guid GetReservationUid(this HttpContext context)
        {
            if (context == null)
                throw new ArgumentNullException(nameof(context));

            Guid reservationUid = Guid.Empty;

            Guid.TryParse(context.User?.Claims?.FirstOrDefault(c => c.Type == TokenInfoClaims.ReservationUid)?.Value, out reservationUid);

            return reservationUid;
        }
    }
}
