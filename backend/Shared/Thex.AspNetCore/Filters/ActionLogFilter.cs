﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Thex.Common;
using Thex.GenericLog;

namespace Thex.AspNetCore.Filters
{
    public class ActionLogFilter : ActionFilterAttribute
    {
        private IGenericLogHandler _genericLog { get; set; }
        private IGenericLogEventBuilder _genericLogEventBuilder { get; set; }

        public ActionLogFilter(IGenericLogHandler genericLog,
                               IGenericLogEventBuilder genericLogEventBuilder)
        {
            _genericLog = genericLog;
            _genericLogEventBuilder = genericLogEventBuilder;
        }

        public override void OnActionExecuting(ActionExecutingContext context)
        {
            if (AppConsts.MethodsLog.Contains(context.HttpContext.Request.Method))
            {
                context.HttpContext.Items["StartTimestamp"] = Stopwatch.GetTimestamp();

                _genericLog.AddLog(_genericLog.DefaultBuilder
                  .WithMessage($"{context.HttpContext.Request.Protocol} {context.HttpContext.Request.Method} {GetPath(context.HttpContext)}") //string.Format(MessageTemplate, httpContext.Request.Method, GetPath(httpContext), statusCode, elapsedMs))
                  .WithDetailedMessage(AppConsts.StartRequest)
                  .AsInformation()
                  .Build());
            }

            base.OnActionExecuting(context);
        }

        public override void OnActionExecuted(ActionExecutedContext context)
        {
            base.OnActionExecuted(context);

            if (AppConsts.MethodsLog.Contains(context.HttpContext.Request.Method) && _genericLog.HasGenericLog())
            {
                var elapsedMs = GetElapsedMilliseconds((long)context.HttpContext.Items["StartTimestamp"], Stopwatch.GetTimestamp());
                var statusCode = context.HttpContext.Response?.StatusCode;

                _genericLog.AddLog(_genericLog.DefaultBuilder
                                              .WithMessage($"HTTP {context.HttpContext.Request.Method} {GetPath(context.HttpContext)} responded {statusCode} in {elapsedMs:0.0000} ms") //string.Format(MessageTemplate, httpContext.Request.Method, GetPath(httpContext), statusCode, elapsedMs))
                                              .WithDetailedMessage(AppConsts.EndRequest)
                                              .AsInformation()
                                              .Build());

                _genericLog.Commit();
            }
        }

        private static double GetElapsedMilliseconds(long start, long stop)
        {
            return (stop - start) * 1000 / (double)Stopwatch.Frequency;
        }

        private static string GetPath(HttpContext httpContext)
        {
            return httpContext.Features.Get<IHttpRequestFeature>()?.RawTarget ?? httpContext.Request.Path.ToString();
        }
    }
}
