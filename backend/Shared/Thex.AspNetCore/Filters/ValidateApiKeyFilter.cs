﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Primitives;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;

namespace Thex.Web
{
    public class ValidateApiKeyFilter : ActionFilterAttribute
    {
        private string _xToken = "x-token";
        private string _apiKey = "gWqqYgPUIEimDHjlBOuKhA==";

        public override void OnActionExecuting(ActionExecutingContext context)
        {
            if (IsValidRequestMethod(context))
            {
                if (!IsValid(context.HttpContext.Request.Headers))
                    context.Result = new UnauthorizedResult();
            }
        }

        private bool IsValid(IHeaderDictionary headers)
        {
            var ret = false;

            try
            {
                if (headers.TryGetValue(_xToken, out StringValues values))
                {
                    string token = values.First();

                    if (string.IsNullOrEmpty(token))
                        return false;

                    if (token != _apiKey)
                        return false;

                    return true;
                }
            }
            catch
            {
                return false;
            }

            return ret;
        }

        private bool IsValidRequestMethod(ActionExecutingContext context)
        {
            var methods = new string[] { "POST", "DELETE", "PATCH", "UPDATE" }.ToList();
            var request = context.HttpContext.Request;
            var currentMethod = request.Method;

            var isValidMethod = methods.Contains(currentMethod);
            return isValidMethod;
        }
    }
}
