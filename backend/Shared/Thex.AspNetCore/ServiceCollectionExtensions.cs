﻿using Microsoft.AspNetCore.Mvc;
using Thex.AspNetCore.Mvc.Authorization;
using Tnf;

// ReSharper disable once CheckNamespace
namespace Microsoft.Extensions.DependencyInjection
{
    public static class ServiceCollectionExtensions
    {
        /// <summary>
        /// Add Thex AspNetCore
        /// </summary>
        /// <param name="services">Services.</param>
        public static IServiceCollection AddThexAspNetCore(this IServiceCollection services)
        {
            Check.NotNull(services, nameof(services));

            // Filters
            services.AddTransient<ThexAuthorizationFilter>();

            ConfigureThexAspNetCore(services);

            return services;
        }

        private static void ConfigureThexAspNetCore(IServiceCollection services)
        {
            // Configure Thex Mvc
            services.Configure<MvcOptions>(mvcOptions =>
            {
                mvcOptions.Filters.AddService(typeof(ThexAuthorizationFilter));
            });
        }
    }
}
