﻿namespace Thex.Maps.Geocode.ReadInterfaces
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Thex.Maps.Geocode.Entities.GoogleAddress;
    using Tnf.Repositories;

    public interface IAddressMapsGeocodeReadRepository : IRepository
    {
        Task<List<GoogleAddress>> GetMapsGeocodeResultByAddress(string address, string language);

        Task<GoogleAddress> GetMapsGeocodeResultByPlaceId(string placeId, string language);

        List<string> GetValidLanguages();

        GoogleAddressCountryTypeLevels GetAddressComponentConfigByCountryTwoLetterIsoCode(string countryTwoLetterIsoCode, GoogleAddressComponent[] components);
    }
}
