﻿using Microsoft.Extensions.DependencyInjection;
using System;
using Thex.Maps.Geocode.ReadInterfaces;
using Thex.Maps.Geocode.Repositories;
using Tnf;

namespace Thex.Maps.Geocode
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddMapsGeocodeDependency(this IServiceCollection services, Action<MapsGeocodeConfig> configureOptions)
        {
            services.AddTransient<IAddressMapsGeocodeReadRepository, AddressMapsGeocodeReadRepository>();
            services.AddTransient<IMapsGeocodeBaseRepository, MapsGeocodeBaseRepository>();

            Check.NotNull(services, nameof(services));

            var options = new MapsGeocodeConfig();

            configureOptions(options);

            services.AddSingleton(e => options);

            return services;
        }       
    }
}