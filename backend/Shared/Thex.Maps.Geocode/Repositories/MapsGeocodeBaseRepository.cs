﻿namespace Thex.Maps.Geocode.Repositories
{
    using System.Net.Http;

    public class MapsGeocodeBaseRepository : IMapsGeocodeBaseRepository
    {
        public string ServiceUrl { get; } = MapsGeocodeConfig.ServiceUrl;

        public HttpClient GetClient()
        {
            return new HttpClient();
        }
    }
}
