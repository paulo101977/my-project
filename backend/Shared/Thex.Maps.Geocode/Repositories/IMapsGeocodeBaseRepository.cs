﻿namespace Thex.Maps.Geocode.Repositories
{
    using System.Net.Http;

    public interface IMapsGeocodeBaseRepository
    {
        HttpClient GetClient();
    }
}
