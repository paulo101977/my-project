﻿namespace Thex.Maps.Geocode.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    using Thex.Maps.Geocode.Entities;
    using Thex.Maps.Geocode.ReadInterfaces;

    using Newtonsoft.Json;
    using Thex.Maps.Geocode.Enumerations;
    using Thex.Maps.Geocode.Entities.GoogleAddress;
    using System.IO;
    using Thex.Maps.Geocode.Entities.Parser;

    public class AddressMapsGeocodeReadRepository : MapsGeocodeBaseRepository, IAddressMapsGeocodeReadRepository
    {
        public async Task<List<GoogleAddress>> GetMapsGeocodeResultByAddress(string address, string language)
        {
            var client = GetClient();

            var requestUri = $"{ServiceUrl}&address={address}&language={language}";

            var responseMessage = await client.GetAsync(requestUri);

            var resultData = await responseMessage.Content.ReadAsStringAsync();

            return GetMapsGeocodeResults(resultData);
        }

        public async Task<GoogleAddress> GetMapsGeocodeResultByPlaceId(string placeId, string language)
        {
            var client = GetClient();

            var requestUri = $"{ServiceUrl}&place_id={placeId}&language={language}";

            var responseMessage = await client.GetAsync(requestUri);

            var resultData = await responseMessage.Content.ReadAsStringAsync();

            var result = JsonConvert.DeserializeObject<MapsGeocodeResult>(resultData);

            return GetMapsGeocodeResults(resultData).FirstOrDefault();
        }


        private List<GoogleAddress> GetMapsGeocodeResults(string resultData)
        {
            var result = JsonConvert.DeserializeObject<MapsGeocodeResult>(resultData);

            var status = EvaluateStatus(result.status);

            if (status != GoogleStatus.Ok && status != GoogleStatus.ZeroResults)
                return null; //error

            if (status == GoogleStatus.Ok)
                return ParseAddresses(result.results);

            return new List<GoogleAddress>();
        }


        public List<string> GetValidLanguages()
        {
            return new string[]
            {
                "ar","bg","bn","ca","cs","da","de","el","en", "en-US", "en-AU","en-GB","es","eu","eu","fa","fi","fil","fr","gl",
                "gu","hi","hr","hu","id","it","iw","ja","kn","ko","lt","lv","ml","mr","nl","no","pl","pt","pt-BR","pt-PT",
                "ro","ru","sk","sl","sr","sv","ta","te","th","tl","tr","uk","vi","zh-CN","zh -TW"
            }.ToList();
        }
        
        public GoogleAddressCountryTypeLevels GetAddressComponentConfigByCountryTwoLetterIsoCode(string countryTwoLetterIsoCodeGoogle, GoogleAddressComponent[] Components)
        {
            var result = new GoogleAddressCountryTypeLevels();
            Components = Components.OrderBy(x => x.Order).ToArray();
            for (int i = 0; i < Components.ToArray().Length; i++)
            {
                var component = Components[i];
                var componentType = component.Types[0];

                if (componentType == GoogleAddressType.Country ||
                    componentType == GoogleAddressType.AdministrativeAreaLevel1 ||
                    componentType == GoogleAddressType.AdministrativeAreaLevel2 ||
                    componentType == GoogleAddressType.Locality ||
                    componentType == GoogleAddressType.SubLocality ||
                    componentType == GoogleAddressType.AdministrativeAreaLevel3)
                {
                    result = new GoogleAddressCountryTypeLevels
                    {
                        CountryTwoletterIsoCode = countryTwoLetterIsoCodeGoogle,
                        TypeLevel = componentType,
                        ChildrenLevel = result != null ? result : null
                    };
                }
            }

            return result;
        }

        private List<GoogleAddress> ParseAddresses(List<Result> geocodes)
        {
            var result = new List<GoogleAddress>();

            foreach (var geocode in geocodes)
            {

                GoogleAddressType type = EvaluateType(geocode.types.FirstOrDefault());
                string placeId = geocode.place_id;
                string formattedAddress = geocode.formatted_address;

                var components = ParseComponents(geocode.address_components).ToArray();

                var geoLocation = geocode.geometry.location;
                var geoViewport = geocode.geometry.viewport;
                var geoLocationType = geocode.geometry.location_type;

                GoogleLocation coordinates = new GoogleLocation(geoLocation.lat, geoLocation.lng);
                GoogleLocation neCoordinates = new GoogleLocation(geoViewport.northeast.lat, geoViewport.northeast.lng);
                GoogleLocation swCoordinates = new GoogleLocation(geoViewport.southwest.lat, geoViewport.southwest.lng);

                var viewport = new GoogleViewport { Northeast = neCoordinates, Southwest = swCoordinates };

                GoogleLocationType locationType = EvaluateLocationType(geoLocationType);

                result.Add(new GoogleAddress(type, formattedAddress, components, coordinates, viewport, locationType, placeId));
            }

            return result;
        }

        private IEnumerable<GoogleAddressComponent> ParseComponents(List<AddressComponent> addressesComponents)
        {
            foreach (var address in addressesComponents)
            {
                string longName = address.long_name;
                string shortName = address.short_name;
                var types = ParseComponentTypes(address.types).ToArray();

                if (types.Any()) //don't return an address component with no type
                    yield return new GoogleAddressComponent(types, longName, shortName);
            }
        }

        private IEnumerable<GoogleAddressType> ParseComponentTypes(List<string> componentTrpes)
        {
            foreach (var item in componentTrpes)
                yield return EvaluateType(item);
        }

        /// <remarks>
        /// http://code.google.com/apis/maps/documentation/geocoding/#StatusCodes
        /// </remarks>
        private GoogleStatus EvaluateStatus(string status)
        {
            switch (status)
            {
                case "OK": return GoogleStatus.Ok;
                case "ZERO_RESULTS": return GoogleStatus.ZeroResults;
                case "OVER_QUERY_LIMIT": return GoogleStatus.OverQueryLimit;
                case "REQUEST_DENIED": return GoogleStatus.RequestDenied;
                case "INVALID_REQUEST": return GoogleStatus.InvalidRequest;
                default: return GoogleStatus.Error;
            }
        }

        /// <remarks>
        /// http://code.google.com/apis/maps/documentation/geocoding/#Types
        /// </remarks>
        private GoogleAddressType EvaluateType(string type)
        {
            switch (type)
            {
                case "street_address": return GoogleAddressType.StreetAddress;
                case "route": return GoogleAddressType.Route;
                case "intersection": return GoogleAddressType.Intersection;
                case "political": return GoogleAddressType.Political;
                case "country": return GoogleAddressType.Country;
                case "administrative_area_level_1": return GoogleAddressType.AdministrativeAreaLevel1;
                case "administrative_area_level_2": return GoogleAddressType.AdministrativeAreaLevel2;
                case "administrative_area_level_3": return GoogleAddressType.AdministrativeAreaLevel3;
                case "colloquial_area": return GoogleAddressType.ColloquialArea;
                case "locality": return GoogleAddressType.Locality;
                case "sublocality": return GoogleAddressType.SubLocality;
                case "neighborhood": return GoogleAddressType.Neighborhood;
                case "premise": return GoogleAddressType.Premise;
                case "subpremise": return GoogleAddressType.Subpremise;
                case "postal_code": return GoogleAddressType.PostalCode;
                case "natural_feature": return GoogleAddressType.NaturalFeature;
                case "airport": return GoogleAddressType.Airport;
                case "park": return GoogleAddressType.Park;
                case "point_of_interest": return GoogleAddressType.PointOfInterest;
                case "post_box": return GoogleAddressType.PostBox;
                case "street_number": return GoogleAddressType.StreetNumber;
                case "floor": return GoogleAddressType.Floor;
                case "room": return GoogleAddressType.Room;
                case "postal_town": return GoogleAddressType.PostalTown;
                case "establishment": return GoogleAddressType.Establishment;
                case "sublocality_level_1": return GoogleAddressType.SubLocalityLevel1;
                case "sublocality_level_2": return GoogleAddressType.SubLocalityLevel2;
                case "sublocality_level_3": return GoogleAddressType.SubLocalityLevel3;
                case "sublocality_level_4": return GoogleAddressType.SubLocalityLevel4;
                case "sublocality_level_5": return GoogleAddressType.SubLocalityLevel5;
                case "postal_code_suffix": return GoogleAddressType.PostalCodeSuffix;

                default: return GoogleAddressType.Unknown;
            }
        }

        /// <remarks>
        /// https://developers.google.com/maps/documentation/geocoding/?csw=1#Results
        /// </remarks>
        private GoogleLocationType EvaluateLocationType(string type)
        {
            switch (type)
            {
                case "ROOFTOP": return GoogleLocationType.Rooftop;
                case "RANGE_INTERPOLATED": return GoogleLocationType.RangeInterpolated;
                case "GEOMETRIC_CENTER": return GoogleLocationType.GeometricCenter;
                case "APPROXIMATE": return GoogleLocationType.Approximate;

                default: return GoogleLocationType.Unknown;
            }
        }
    }
}
