﻿namespace Thex.Maps.Geocode.Entities.GoogleAddress
{
    public class GoogleViewport
    {
        public GoogleLocation Northeast { get; set; }
        public GoogleLocation Southwest { get; set; }
    }
}
