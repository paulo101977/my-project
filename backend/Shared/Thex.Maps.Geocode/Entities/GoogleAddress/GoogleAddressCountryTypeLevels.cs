﻿using Thex.Maps.Geocode.Enumerations;
using System;


namespace Thex.Maps.Geocode.Entities.GoogleAddress
{
    public class GoogleAddressCountryTypeLevels
    {
        public string CountryTwoletterIsoCode  { get; set; }
        public GoogleAddressType TypeLevel { get; set; }
        public GoogleAddressCountryTypeLevels ChildrenLevel { get; set; }
    }
}
