﻿using System.Collections.Generic;

namespace Thex.Maps.Geocode.Entities
{
    public class MapsGeocodeResult
    {
        public List<Result> results { get; set; }
        public string status { get; set; }
    }
}
