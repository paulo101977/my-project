﻿namespace Thex.Maps.Geocode.Entities
{
    public class Location
    {
        public double lat { get; set; }
        public double lng { get; set; }
    }
}
