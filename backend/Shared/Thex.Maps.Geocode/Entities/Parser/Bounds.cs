﻿namespace Thex.Maps.Geocode.Entities
{
    public class Bounds
    {
        public Northeast northeast { get; set; }
        public Southwest southwest { get; set; }
    }
}
