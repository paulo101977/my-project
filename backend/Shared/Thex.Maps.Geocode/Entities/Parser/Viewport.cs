﻿namespace Thex.Maps.Geocode.Entities
{
    public class Viewport
    {
        public Northeast northeast { get; set; }
        public Southwest southwest { get; set; }
    }
}
