﻿namespace Thex.Maps.Geocode.Entities
{
    public class Northeast
    {
        public double lat { get; set; }
        public double lng { get; set; }
    }
}
