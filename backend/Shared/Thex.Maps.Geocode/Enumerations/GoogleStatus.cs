﻿namespace Thex.Maps.Geocode.Enumerations
{
    public enum GoogleStatus
    {
        Error,
        Ok,
        ZeroResults,
        OverQueryLimit,
        RequestDenied,
        InvalidRequest
    }
}
