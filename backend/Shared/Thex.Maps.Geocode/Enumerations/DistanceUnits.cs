﻿namespace Thex.Maps.Geocode.Enumerations
{
    public enum DistanceUnits
    {
        Miles,
        Kilometers
    }
}
