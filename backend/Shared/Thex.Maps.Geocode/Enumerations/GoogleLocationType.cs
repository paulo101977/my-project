﻿namespace Thex.Maps.Geocode.Enumerations
{
    public enum GoogleLocationType
    {
        Unknown,
        Rooftop,
        RangeInterpolated,
        GeometricCenter,
        Approximate
    }
}
