﻿namespace Thex.Maps.Geocode
{
    public class MapsGeocodeConfig
    {
        public MapsGeocodeConfig()
        {

        }

        public MapsGeocodeConfig(string apiKey, string serviceUrl, string urlGoogleMaps)
        {
            ApiKey = apiKey;
            ServiceUrl = serviceUrl;
            UrlGoogleMaps = urlGoogleMaps;
        }

        public static string ApiKey { get; set; }
        public static string ServiceUrl { get; set; }
        public static string UrlGoogleMaps { get; set; }
    }
}