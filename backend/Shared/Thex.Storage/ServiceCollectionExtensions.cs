﻿using Microsoft.Extensions.DependencyInjection;
using Thex.Storage;

namespace Thex.Maps.Geocode
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddThexAzureStorage(this IServiceCollection services)
        {
            services.AddTransient<IAzureStorage, AzureStorage>();
            
            return services;
        }
    }
}