﻿using System;
using System.IO;
using System.Threading.Tasks;

namespace Thex.Storage
{
   public interface IAzureStorage
    {
        Task<string> SendAsync(Guid ownerId, string folderName, string fileName, Stream file);
        Task<string> SendAsync(Guid pPropertyGuid, string pNameFolder, string pNameFile, Byte[] pValue);
    }
}
