﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using System.IO;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;

namespace Thex.Storage
{
    public class AzureStorage : IAzureStorage
    {
        private readonly IConfiguration _configuration;

        public AzureStorage(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public async Task<string> SendAsync(Guid ownerId, string folderName, string fileName, Stream file)
        {

            CloudStorageAccount storageAccount = null;
            CloudBlobContainer cloudBlobContainer = null;

            string storageConnectionString = _configuration["ConnectionStrings:Storage:Thex"];
                
            // Check whether the connection string can be parsed.
            if (CloudStorageAccount.TryParse(storageConnectionString, out storageAccount))
            {
                // Create the CloudBlobClient that represents the Blob storage endpoint for the storage account.
                CloudBlobClient cloudBlobClient = storageAccount.CreateCloudBlobClient();

                cloudBlobContainer = cloudBlobClient.GetContainerReference(ownerId.ToString());

                await cloudBlobContainer.CreateIfNotExistsAsync();

                // Set the permissions so the blobs are public. 
                BlobContainerPermissions permissions = new BlobContainerPermissions
                {
                    PublicAccess = BlobContainerPublicAccessType.Blob
                };
                
                await cloudBlobContainer.SetPermissionsAsync(permissions);

                var filePath = string.IsNullOrEmpty(folderName) ? $"{fileName}" : $"{folderName}/{fileName}";

                CloudBlockBlob cloudBlockBlob = cloudBlobContainer.GetBlockBlobReference(filePath);

                await cloudBlockBlob.UploadFromStreamAsync(file);

                return cloudBlockBlob.Uri.AbsoluteUri;
            }
            else
            {
                throw new Exception("Não foi possível criar conexão com o Azure Storage.");
            }
        }

        public async Task<string> SendAsync(Guid pPropertyGuid, string pNameFolder, string pNameFile, Byte[] pValue)
        {

            CloudStorageAccount storageAccount = null;
            CloudBlobContainer cloudBlobContainer = null; ;

            string storageConnectionString = _configuration["ConnectionStrings:Storage:Thex"];

            // Check whether the connection string can be parsed.
            if (CloudStorageAccount.TryParse(storageConnectionString, out storageAccount))
            {
                try
                {
                    // Create the CloudBlobClient that represents the Blob storage endpoint for the storage account.
                    CloudBlobClient cloudBlobClient = storageAccount.CreateCloudBlobClient();

                    cloudBlobContainer = cloudBlobClient.GetContainerReference(pPropertyGuid.ToString());

                    await cloudBlobContainer.CreateIfNotExistsAsync();

                    // Set the permissions so the blobs are public. 
                    BlobContainerPermissions permissions = new BlobContainerPermissions
                    {
                        PublicAccess = BlobContainerPublicAccessType.Blob
                    };

                    await cloudBlobContainer.SetPermissionsAsync(permissions);

                    CloudBlockBlob cloudBlockBlob = cloudBlobContainer.GetBlockBlobReference(pNameFile);

                    await cloudBlockBlob.UploadFromByteArrayAsync(pValue, 0, pValue.Length);

                    BlobContinuationToken blobContinuationToken = null;

                    do
                    {
                        var results = await cloudBlobContainer.ListBlobsSegmentedAsync(null, blobContinuationToken);
                        // Get the value of the continuation token returned by the listing call.
                        blobContinuationToken = results.ContinuationToken;

                    } while (blobContinuationToken != null); // Loop while the continuation token is not null.      

                    return cloudBlockBlob.Uri.AbsoluteUri;

                }
                catch (StorageException ex)
                {
                    throw ex;
                }
            }
            else
            {
                throw new Exception("Não foi possível criar conexão com o Azure Storage.");
            }
        }
    }
}
