﻿CREATE LOGIN application_user WITH PASSWORD = 'Password#1234';

GO

CREATE USER application_user FOR LOGIN application_user;

GO

GRANT CONNECT TO application_user;

GO

GRANT SELECT, INSERT, UPDATE, DELETE, EXECUTE TO application_user;