﻿INSERT INTO TransportationType(TransportationTypeId,TransportationTypeName) VALUES (1, 'Plane');
INSERT INTO TransportationType(TransportationTypeId,TransportationTypeName) VALUES (2, 'Car');
INSERT INTO TransportationType(TransportationTypeId,TransportationTypeName) VALUES (3, 'Motorcycle');
INSERT INTO TransportationType(TransportationTypeId,TransportationTypeName) VALUES (4, 'Ship/Ferry Boat');
INSERT INTO TransportationType(TransportationTypeId,TransportationTypeName) VALUES (5, 'Bus');
INSERT INTO TransportationType(TransportationTypeId,TransportationTypeName) VALUES (6, 'Train');
INSERT INTO TransportationType(TransportationTypeId,TransportationTypeName) VALUES (7, 'Other');