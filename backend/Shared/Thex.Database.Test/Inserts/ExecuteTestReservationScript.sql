﻿declare @DateRef datetime;
set @DateRef = GETUTCDATE();
EXEC [DeleteTestReservations];
EXEC [CreateTestReservations] @DateRef;