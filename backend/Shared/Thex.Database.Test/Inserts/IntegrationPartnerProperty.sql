﻿declare @integrationPartnerId int

INSERT [dbo].[IntegrationPartnerProperty]
([IntegrationPartnerPropertyId], [PropertyId], [IsActive], [IntegrationCode], [PartnerId]) 
VALUES ('c3a1abcb-7364-43ba-a9a4-a3fe480cc3ca', 11, 1, '848CC06F-8717-4D44-9706-1B13E3AA0400', 4)

SET @integrationPartnerId =(select IntegrationPartnerId from IntegrationPartner where IntegrationPartnerName = 'ENOTAS')
UPDATE IntegrationPartnerProperty SET IntegrationPartnerId = @integrationPartnerId WHERE PartnerId = 4

INSERT [dbo].[IntegrationPartnerProperty]
([IntegrationPartnerPropertyId], [PropertyId], [IsActive], [IntegrationCode], [IntegrationNumber], [PartnerId]) 
VALUES (newid(), 33, 1, '228d48ba12e8f26e3ea55f423f88836d1f354b43', 'totvscmnet', 5)

SET @integrationPartnerId =(select IntegrationPartnerId from IntegrationPartner where IntegrationPartnerName = 'INVOICEXPRESS')
UPDATE IntegrationPartnerProperty SET IntegrationPartnerId = @integrationPartnerId WHERE PartnerId = 5
