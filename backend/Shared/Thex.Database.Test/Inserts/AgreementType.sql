﻿GO
INSERT [dbo].[AgreementType] ([AgreementTypeId], [AgreementTypeName]) VALUES (1, N'Direct Sale - B2C')
GO
INSERT [dbo].[AgreementType] ([AgreementTypeId], [AgreementTypeName]) VALUES (2, N'Business Sale - B2B')