﻿INSERT [dbo].[Person] ([PersonId], [PersonType], [FirstName], [LastName], [DateOfBirth], [CountrySubdivisionId]) VALUES (N'b6641c68-f250-47d2-94e3-14d580b8decd', N'N', N'Ricardo', N'Ricardo', CAST(N'1962-04-06' AS Date), 1)
GO
INSERT [dbo].[Person] ([PersonId], [PersonType], [FirstName], [LastName], [DateOfBirth], [CountrySubdivisionId]) VALUES (N'b909ad6a-eb2c-4dcf-a583-c297d71ed0cf', N'N', N'Joana', N'Joana', CAST(N'1996-04-05' AS Date), 1)
GO



SET IDENTITY_INSERT [dbo].[Reservation] ON 
GO
INSERT [dbo].[Reservation] ([ReservationId], [ReservationUid], [ReservationCode], [ReservationVendorCode], [PropertyId], [ContactName], [ContactEmail], [ContactPhone], [InternalComments], [ExternalComments], [GroupName], [Deadline], [UnifyAccounts], [BusinessSourceId], [MarketSegmentId], [CompanyClientId], [CompanyClientContactPersonId], [TenantId], [IsDeleted], [CreationTime], [CreatorUserId], [LastModificationTime], [LastModifierUserId], [DeletionTime], [DeleterUserId]) VALUES (4, N'1b45825e-8f06-4ed6-a5f7-25f0ef586e2c', N'1Z1EFH7F3', N'001', 11, N'Ricardo', N'ricardo@email.com', N'30174625', N'Observação 2', N'Observação 1', N'', NULL, 0, 1, 1, N'6a27e16a-f227-4256-a62f-96e7b39d0658', NULL, N'23eb803c-726a-4c7c-b25b-2c22a56793d9', 0, GETUTCDATE(), NULL, GETUTCDATE(), NULL, NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[Reservation] OFF
GO


SET IDENTITY_INSERT [dbo].[ReservationItem] ON 
GO
INSERT [dbo].[ReservationItem] ([ReservationItemId], [ReservationItemUid], [ReservationId], [ReasonId], [EstimatedArrivalDate], [CheckInDate], [EstimatedDepartureDate], [CheckOutDate], [CancellationDate], [CancellationDescription], [ReservationItemCode], [RequestedRoomTypeId], [ReceivedRoomTypeId], [RoomId], [AdultCount], [ChildCount], [ReservationItemStatusId], [RoomLayoutId], [ExtraBedCount], [ExtraCribCount], [TenantId], [IsDeleted], [CreationTime], [CreatorUserId], [LastModificationTime], [LastModifierUserId], [DeletionTime], [DeleterUserId]) VALUES (8, N'530c75c7-6deb-4ac0-9153-81163f0ad5d2', 4, NULL, GETUTCDATE(), GETUTCDATE(), GETUTCDATE()+5, GETUTCDATE()+5, NULL, NULL, N'1Z1EFH7F3-0001', 1, 1, 1, 2, 0, 2, N'88c38586-5c4b-489b-8bb9-0aa7d78dea99', 0, 0, N'23eb803c-726a-4c7c-b25b-2c22a56793d9', 0, GETUTCDATE(), NULL, GETUTCDATE(), NULL, NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[ReservationItem] OFF



GO
INSERT [dbo].[GuestRegistration] ([GuestRegistrationId], [FirstName], [LastName], [FullName], [SocialName], [Email], [BirthDate], [Gender], [OccupationId], [GuestTypeId], [PhoneNumber], [MobilePhoneNumber], [Nationality], [PurposeOfTrip], [ArrivingBy], [ArrivingFrom], [ArrivingFromText], [NextDestination], [NextDestinationText], [AdditionalInformation], [HealthInsurance],[PropertyId],[TenantId],[PersonId],[GuestId],[ResponsibleGuestRegistrationId],[IsLegallyIncompetent],[IsDeleted],[CreationTime],[CreatorUserId],[LastModificationTime],[LastModifierUserId],[DeletionTime],[DeleterUserId]) VALUES (N'60f9eba1-9eaa-422f-8cfc-eaba11e8e22f', N'Joana', N'Joana', N'Joana', NULL, N'joana@email.com', CAST(N'1996-04-05' AS Date), N'F', 17, 1, NULL, NULL, 1, 3, 1, NULL, N'RJ', NULL, N'SP', NULL, NULL,11,'23eb803c-726a-4c7c-b25b-2c22a56793d9','21ec6fa3-afd9-4889-9490-a4d50863eef5',2,'60f9eba1-9eaa-422f-8cfc-eaba11e8e22f',0,0,GETUTCDATE(),null,null,null,null,null)
GO
INSERT [dbo].[GuestRegistration] ([GuestRegistrationId], [FirstName], [LastName], [FullName], [SocialName], [Email], [BirthDate], [Gender], [OccupationId], [GuestTypeId], [PhoneNumber], [MobilePhoneNumber], [Nationality], [PurposeOfTrip], [ArrivingBy], [ArrivingFrom], [ArrivingFromText], [NextDestination], [NextDestinationText], [AdditionalInformation], [HealthInsurance],[PropertyId],[TenantId],[PersonId],[GuestId],[ResponsibleGuestRegistrationId],[IsLegallyIncompetent],[IsDeleted],[CreationTime],[CreatorUserId],[LastModificationTime],[LastModifierUserId],[DeletionTime],[DeleterUserId]) VALUES (N'a4c8836f-7995-4e86-82ef-ed1fc00fcf9c', N'Ricardo', N'Ricardo', N'Ricardo', NULL, N'ricardo@email.com', CAST(N'1962-04-06' AS Date), N'M', 6, 1, NULL, NULL, 1, 3, 4, NULL, N'RJ', NULL, N'SP', NULL, NULL,11,'23eb803c-726a-4c7c-b25b-2c22a56793d9','21ec6fa3-afd9-4889-9490-a4d50863eef5',2,'60f9eba1-9eaa-422f-8cfc-eaba11e8e22f',0,0,GETUTCDATE(),null,null,null,null,null)
GO


SET IDENTITY_INSERT [dbo].[Guest] ON 
GO
INSERT [dbo].[Guest] ([GuestId], [PersonId]) VALUES (8, N'b6641c68-f250-47d2-94e3-14d580b8decd')
GO
INSERT [dbo].[Guest] ([GuestId], [PersonId]) VALUES (9, N'b909ad6a-eb2c-4dcf-a583-c297d71ed0cf')
GO
SET IDENTITY_INSERT [dbo].[Guest] OFF
GO

SET IDENTITY_INSERT [dbo].[GuestReservationItem] ON 
GO
INSERT [dbo].[GuestReservationItem] ([GuestReservationItemId], [ReservationItemId], [GuestId], [GuestName], [GuestEmail], [GuestDocumentTypeId], [GuestDocument], [EstimatedArrivalDate], [CheckInDate], [EstimatedDepartureDate], [CheckOutDate], [GuestStatusId], [GuestTypeId], [PreferredName], [GuestCitizenship], [GuestLanguage], [PctDailyRate], [IsIncognito], [IsChild], [ChildAge], [IsMain], [GuestRegistrationId], [PropertyId], [TenantId]) VALUES (15, 8, 8, N'Ricardo', N'ricardo@email.com', 1, N'13427265706', GETUTCDATE(), GETUTCDATE(), GETUTCDATE()+5, NULL, 2, 1, N'Ricardo', NULL, NULL, 100, 0, 0, NULL, 1, N'a4c8836f-7995-4e86-82ef-ed1fc00fcf9c', 1, N'23eb803c-726a-4c7c-b25b-2c22a56793d9')
GO
INSERT [dbo].[GuestReservationItem] ([GuestReservationItemId], [ReservationItemId], [GuestId], [GuestName], [GuestEmail], [GuestDocumentTypeId], [GuestDocument], [EstimatedArrivalDate], [CheckInDate], [EstimatedDepartureDate], [CheckOutDate], [GuestStatusId], [GuestTypeId], [PreferredName], [GuestCitizenship], [GuestLanguage], [PctDailyRate], [IsIncognito], [IsChild], [ChildAge], [IsMain], [GuestRegistrationId], [PropertyId], [TenantId]) VALUES (16, 8, 9, N'Joana', N'joana@email.com', 1, N'13427265783', GETUTCDATE(), GETUTCDATE(), GETUTCDATE()+5, NULL, 2, 1, N'Joana', NULL, NULL, 0, 0, 0, NULL, 0, N'60f9eba1-9eaa-422f-8cfc-eaba11e8e22f', 1, N'23eb803c-726a-4c7c-b25b-2c22a56793d9')
GO
SET IDENTITY_INSERT [dbo].[GuestReservationItem] OFF
GO


INSERT [dbo].[BillingAccount] ([BillingAccountId], [CompanyClientId], [ReservationId], [ReservationItemId], [GuestReservationItemId], [BillingAccountTypeId], [StartDate], [EndDate], [StatusId], [IsMainAccount], [MarketSegmentId], [BusinessSourceId], [PropertyId], [Blocked], [GroupKey], [BillingAccountName], [ReopeningReasonId], [ReopeningDate], [TenantId]) VALUES (N'50a66538-dd94-4162-a1ed-83fc8311e574', NULL, 4, 8, 16, 2, GETUTCDATE(), NULL, 1, 1, 1, 1, 11, 0, N'50a66538-dd94-4162-a1ed-83fc8311e574', NULL, NULL, NULL, N'23eb803c-726a-4c7c-b25b-2c22a56793d9')
GO
INSERT [dbo].[BillingAccount] ([BillingAccountId], [CompanyClientId], [ReservationId], [ReservationItemId], [GuestReservationItemId], [BillingAccountTypeId], [StartDate], [EndDate], [StatusId], [IsMainAccount], [MarketSegmentId], [BusinessSourceId], [PropertyId], [Blocked], [GroupKey], [BillingAccountName], [ReopeningReasonId], [ReopeningDate], [TenantId]) VALUES (N'c3e5678d-815f-42af-bad0-c826d9bc7790', NULL, 4, 8, 15, 2, GETUTCDATE(), NULL, 1, 1, 1, 1, 11, 0, N'c3e5678d-815f-42af-bad0-c826d9bc7790', NULL, NULL, NULL, N'23eb803c-726a-4c7c-b25b-2c22a56793d9')
GO																																																																																					
INSERT [dbo].[BillingAccount] ([BillingAccountId], [CompanyClientId], [ReservationId], [ReservationItemId], [GuestReservationItemId], [BillingAccountTypeId], [StartDate], [EndDate], [StatusId], [IsMainAccount], [MarketSegmentId], [BusinessSourceId], [PropertyId], [Blocked], [GroupKey], [BillingAccountName], [ReopeningReasonId], [ReopeningDate], [TenantId]) VALUES (N'c976c0ef-0452-4b7d-8e85-d1ecbaa4bf6e', NULL, 4, 8, 15, 2, GETUTCDATE(), NULL, 1, 0, 1, 1, 11, 0, N'c3e5678d-815f-42af-bad0-c826d9bc7790', N'Bebidas', NULL, NULL, N'23eb803c-726a-4c7c-b25b-2c22a56793d9')
GO																																																																																				
INSERT [dbo].[BillingAccount] ([BillingAccountId], [CompanyClientId], [ReservationId], [ReservationItemId], [GuestReservationItemId], [BillingAccountTypeId], [StartDate], [EndDate], [StatusId], [IsMainAccount], [MarketSegmentId], [BusinessSourceId], [PropertyId], [Blocked], [GroupKey], [BillingAccountName], [ReopeningReasonId], [ReopeningDate], [TenantId]) VALUES (N'487aab2e-a841-48c0-8721-e71c27b93fd9', N'6a27e16a-f227-4256-a62f-96e7b39d0658', 4, 8, NULL, 3, GETUTCDATE(), NULL, 1, 1, 1, 1, 11, 0, N'487aab2e-a841-48c0-8721-e71c27b93fd9', NULL, NULL, NULL, N'23eb803c-726a-4c7c-b25b-2c22a56793d9')
GO
INSERT [dbo].[BillingAccountItem] ([BillingAccountItemId], [BillingAccountId], [BillingAccountIdLastSource], [BillingItemId], [BillingAccountItemTypeId], [BillingAccountItemComments], [Amount],[CurrencyId],[CurrencyExchangeReferenceId],[CurrencySymbol], [OriginalAmount], [PercentualAmount], [CheckNumber], [NSU], [BillingAccountItemDate], [BillingAccountItemReasonId], [BillingAccountItemParentId], [WasReversed], [BillingInvoiceId], [BillingAccountItemTypeIdLastSource], [Order], [PartialParentBillingAccountItemId], [InstallmentsQuantity], [IsOriginal], [TenantId]) VALUES (N'd5199ee7-b63e-47ec-a394-f2cc22c1b0ca', N'c3e5678d-815f-42af-bad0-c826d9bc7790', NULL, 6, 2, NULL, CAST(-1000.0000 AS Numeric(18, 4)),'67838acb-9525-4aeb-b0a6-127c1b986c48',null,null, CAST(-1000.0000 AS Numeric(18, 4)), NULL, NULL, NULL, GETUTCDATE(), NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, N'23eb803c-726a-4c7c-b25b-2c22a56793d9')
GO
INSERT [dbo].[BillingAccountItem] ([BillingAccountItemId], [BillingAccountId], [BillingAccountIdLastSource], [BillingItemId], [BillingAccountItemTypeId], [BillingAccountItemComments], [Amount],[CurrencyId],[CurrencyExchangeReferenceId],[CurrencySymbol],  [OriginalAmount], [PercentualAmount], [CheckNumber], [NSU], [BillingAccountItemDate], [BillingAccountItemReasonId], [BillingAccountItemParentId], [WasReversed], [BillingInvoiceId], [BillingAccountItemTypeIdLastSource], [Order], [PartialParentBillingAccountItemId], [InstallmentsQuantity], [IsOriginal], [TenantId]) VALUES (N'f3265fda-1161-4f85-8370-0061401f1ab0', N'50a66538-dd94-4162-a1ed-83fc8311e574', NULL, 6, 2, NULL, CAST(-520.0000 AS Numeric(18, 4)),'67838acb-9525-4aeb-b0a6-127c1b986c48',null,null, CAST(-520.0000 AS Numeric(18, 4)), NULL, NULL, NULL, GETUTCDATE(), NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, N'23eb803c-726a-4c7c-b25b-2c22a56793d9')
GO																																																																																																																																									
INSERT [dbo].[BillingAccountItem] ([BillingAccountItemId], [BillingAccountId], [BillingAccountIdLastSource], [BillingItemId], [BillingAccountItemTypeId], [BillingAccountItemComments], [Amount],[CurrencyId],[CurrencyExchangeReferenceId],[CurrencySymbol],  [OriginalAmount], [PercentualAmount], [CheckNumber], [NSU], [BillingAccountItemDate], [BillingAccountItemReasonId], [BillingAccountItemParentId], [WasReversed], [BillingInvoiceId], [BillingAccountItemTypeIdLastSource], [Order], [PartialParentBillingAccountItemId], [InstallmentsQuantity], [IsOriginal], [TenantId]) VALUES (N'd3158931-30a2-447c-bbca-5a1c97a7224e', N'c976c0ef-0452-4b7d-8e85-d1ecbaa4bf6e', NULL, 6, 2, NULL, CAST(-10.0000 AS Numeric(18, 4)),'67838acb-9525-4aeb-b0a6-127c1b986c48',null,null, CAST(-10.0000 AS Numeric(18, 4)), NULL, NULL, NULL, GETUTCDATE(), NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, N'23eb803c-726a-4c7c-b25b-2c22a56793d9')
GO																																																																																																																																								
INSERT [dbo].[BillingAccountItem] ([BillingAccountItemId], [BillingAccountId], [BillingAccountIdLastSource], [BillingItemId], [BillingAccountItemTypeId], [BillingAccountItemComments], [Amount], [CurrencyId],[CurrencyExchangeReferenceId],[CurrencySymbol], [OriginalAmount], [PercentualAmount], [CheckNumber], [NSU], [BillingAccountItemDate], [BillingAccountItemReasonId], [BillingAccountItemParentId], [WasReversed], [BillingInvoiceId], [BillingAccountItemTypeIdLastSource], [Order], [PartialParentBillingAccountItemId], [InstallmentsQuantity], [IsOriginal], [TenantId]) VALUES (N'70fed01a-2385-4af3-9f73-a5c366b9abd6', N'c3e5678d-815f-42af-bad0-c826d9bc7790', NULL, 7, 2, NULL, CAST(-40.0000 AS Numeric(18, 4)),'67838acb-9525-4aeb-b0a6-127c1b986c48',null,null, CAST(-40.0000 AS Numeric(18, 4)), NULL, NULL, NULL, GETUTCDATE(), NULL, N'd5199ee7-b63e-47ec-a394-f2cc22c1b0ca', 0, NULL, NULL, NULL, NULL, NULL, NULL, N'23eb803c-726a-4c7c-b25b-2c22a56793d9')
GO																																																																																																																																				
INSERT [dbo].[BillingAccountItem] ([BillingAccountItemId], [BillingAccountId], [BillingAccountIdLastSource], [BillingItemId], [BillingAccountItemTypeId], [BillingAccountItemComments], [Amount], [CurrencyId],[CurrencyExchangeReferenceId],[CurrencySymbol], [OriginalAmount], [PercentualAmount], [CheckNumber], [NSU], [BillingAccountItemDate], [BillingAccountItemReasonId], [BillingAccountItemParentId], [WasReversed], [BillingInvoiceId], [BillingAccountItemTypeIdLastSource], [Order], [PartialParentBillingAccountItemId], [InstallmentsQuantity], [IsOriginal], [TenantId]) VALUES (N'948b0450-2d01-4451-8ff1-a8d18267eb4a', N'c976c0ef-0452-4b7d-8e85-d1ecbaa4bf6e', NULL, 7, 2, NULL, CAST(-0.4000 AS Numeric(18, 4)),'67838acb-9525-4aeb-b0a6-127c1b986c48',null,null, CAST(-0.4000 AS Numeric(18, 4)), NULL, NULL, NULL, GETUTCDATE(), NULL, N'd3158931-30a2-447c-bbca-5a1c97a7224e', 0, NULL, NULL, NULL, NULL, NULL, NULL, N'23eb803c-726a-4c7c-b25b-2c22a56793d9')
GO																																																																																																																																		
INSERT [dbo].[BillingAccountItem] ([BillingAccountItemId], [BillingAccountId], [BillingAccountIdLastSource], [BillingItemId], [BillingAccountItemTypeId], [BillingAccountItemComments], [Amount],[CurrencyId],[CurrencyExchangeReferenceId],[CurrencySymbol],  [OriginalAmount], [PercentualAmount], [CheckNumber], [NSU], [BillingAccountItemDate], [BillingAccountItemReasonId], [BillingAccountItemParentId], [WasReversed], [BillingInvoiceId], [BillingAccountItemTypeIdLastSource], [Order], [PartialParentBillingAccountItemId], [InstallmentsQuantity], [IsOriginal], [TenantId]) VALUES (N'b28e8fa6-bb7b-47d5-b097-e37923c154f1', N'50a66538-dd94-4162-a1ed-83fc8311e574', NULL, 7, 2, NULL, CAST(-20.8000 AS Numeric(18, 4)),'67838acb-9525-4aeb-b0a6-127c1b986c48',null,null, CAST(-20.8000 AS Numeric(18, 4)), NULL, NULL, NULL, GETUTCDATE(), NULL, N'f3265fda-1161-4f85-8370-0061401f1ab0', 0, NULL, NULL, NULL, NULL, NULL, NULL, N'23eb803c-726a-4c7c-b25b-2c22a56793d9')
GO


INSERT [dbo].[ReservationConfirmation] ([ReservationConfirmationId], [ReservationId], [PaymentTypeId], [PlasticId], [BillingClientId], [EnsuresNoShow], [Value], [Deadline], [TenantId]) VALUES (N'8911dc12-a910-4fd3-8bb2-a4904394b3a2', 4, 1, NULL, NULL, 0, NULL, NULL, N'23eb803c-726a-4c7c-b25b-2c22a56793d9')
GO


SET IDENTITY_INSERT [dbo].[ReservationBudget] ON 
GO
INSERT [dbo].[ReservationBudget] ([ReservationBudgetId], [ReservationBudgetUid], [ReservationItemId], [BudgetDay], [BaseRate], [ChildRate], [MealPlanTypeId], [RateVariation], [AgreementRate], [ManualRate], [Discount], [CurrencyId], [CurrencySymbol], [TenantId]) VALUES (23, N'42c3c2a2-c64d-459e-9cc7-240ae26917c8', 8, CAST(N'2018-03-28' AS Date), CAST(300.0000 AS Numeric(18, 4)), CAST(150.0000 AS Numeric(18, 4)), 1, CAST(20.0000 AS Numeric(18, 4)), CAST(30.0000 AS Numeric(18, 4)), CAST(50.0000 AS Numeric(18, 4)), CAST(500.0000 AS Numeric(18, 4)), N'67838acb-9525-4aeb-b0a6-127c1b986c48', NULL, N'23eb803c-726a-4c7c-b25b-2c22a56793d9')
GO																																																																																																							 
INSERT [dbo].[ReservationBudget] ([ReservationBudgetId], [ReservationBudgetUid], [ReservationItemId], [BudgetDay], [BaseRate], [ChildRate], [MealPlanTypeId], [RateVariation], [AgreementRate], [ManualRate], [Discount], [CurrencyId], [CurrencySymbol], [TenantId]) VALUES (24, N'ef4c8589-34d5-42e6-b512-e320dad9d071', 8, CAST(N'2018-03-28' AS Date), CAST(300.0000 AS Numeric(18, 4)), CAST(150.0000 AS Numeric(18, 4)), 1, CAST(20.0000 AS Numeric(18, 4)), CAST(30.0000 AS Numeric(18, 4)), CAST(50.0000 AS Numeric(18, 4)), CAST(500.0000 AS Numeric(18, 4)), N'67838acb-9525-4aeb-b0a6-127c1b986c48', NULL, N'23eb803c-726a-4c7c-b25b-2c22a56793d9')
GO																																																																																																							 
INSERT [dbo].[ReservationBudget] ([ReservationBudgetId], [ReservationBudgetUid], [ReservationItemId], [BudgetDay], [BaseRate], [ChildRate], [MealPlanTypeId], [RateVariation], [AgreementRate], [ManualRate], [Discount], [CurrencyId], [CurrencySymbol], [TenantId]) VALUES (25, N'06526986-1b63-4ba3-b79c-6d681c601364', 8, CAST(N'2018-03-28' AS Date), CAST(300.0000 AS Numeric(18, 4)), CAST(150.0000 AS Numeric(18, 4)), 1, CAST(20.0000 AS Numeric(18, 4)), CAST(30.0000 AS Numeric(18, 4)), CAST(50.0000 AS Numeric(18, 4)), CAST(500.0000 AS Numeric(18, 4)), N'67838acb-9525-4aeb-b0a6-127c1b986c48', NULL, N'23eb803c-726a-4c7c-b25b-2c22a56793d9')
GO																																																																																																							 
INSERT [dbo].[ReservationBudget] ([ReservationBudgetId], [ReservationBudgetUid], [ReservationItemId], [BudgetDay], [BaseRate], [ChildRate], [MealPlanTypeId], [RateVariation], [AgreementRate], [ManualRate], [Discount], [CurrencyId], [CurrencySymbol], [TenantId]) VALUES (26, N'9c011546-acd9-4260-bc3a-1a3ca00a4382', 8, CAST(N'2018-03-28' AS Date), CAST(300.0000 AS Numeric(18, 4)), CAST(150.0000 AS Numeric(18, 4)), 1, CAST(20.0000 AS Numeric(18, 4)), CAST(30.0000 AS Numeric(18, 4)), CAST(50.0000 AS Numeric(18, 4)), CAST(500.0000 AS Numeric(18, 4)), N'67838acb-9525-4aeb-b0a6-127c1b986c48', NULL, N'23eb803c-726a-4c7c-b25b-2c22a56793d9')
GO
SET IDENTITY_INSERT [dbo].[ReservationBudget] OFF


GO
INSERT [dbo].[ContactInformation] ([OwnerId], [ContactInformationTypeId], [Information]) VALUES (N'b6641c68-f250-47d2-94e3-14d580b8decd', 1, N'ricardo@email.com')
GO
INSERT [dbo].[ContactInformation] ([OwnerId], [ContactInformationTypeId], [Information]) VALUES (N'b909ad6a-eb2c-4dcf-a583-c297d71ed0cf', 1, N'joana@email.com')
GO


INSERT [dbo].[Document] ([OwnerId], [DocumentTypeId], [DocumentInformation]) VALUES (N'b6641c68-f250-47d2-94e3-14d580b8decd', 1, N'13427265706')
GO
INSERT [dbo].[Document] ([OwnerId], [DocumentTypeId], [DocumentInformation]) VALUES (N'a4c8836f-7995-4e86-82ef-ed1fc00fcf9c', 1, N'13427265706')
GO
INSERT [dbo].[Document] ([OwnerId], [DocumentTypeId], [DocumentInformation]) VALUES (N'b909ad6a-eb2c-4dcf-a583-c297d71ed0cf', 1, N'13427265783')
GO
INSERT [dbo].[Document] ([OwnerId], [DocumentTypeId], [DocumentInformation]) VALUES (N'60f9eba1-9eaa-422f-8cfc-eaba11e8e22f', 1, N'13427265783')
GO


SET IDENTITY_INSERT [dbo].[Location] ON 
GO
INSERT [dbo].[Location] ([LocationId], [OwnerId], [LocationCategoryId], [Latitude], [Longitude], [StreetName], [StreetNumber], [AdditionalAddressDetails], [Neighborhood], [CityId], [PostalCode], [CountryCode]) VALUES (3002, N'a4c8836f-7995-4e86-82ef-ed1fc00fcf9c', 1, CAST(-22.900523 AS Decimal(9, 6)), CAST(-43.287372 AS Decimal(9, 6)), N'Rua Ajuratuba', N'121', N'bloco D', N'Todos os Santos', 3269, 20735050, N'BR')
GO
INSERT [dbo].[Location] ([LocationId], [OwnerId], [LocationCategoryId], [Latitude], [Longitude], [StreetName], [StreetNumber], [AdditionalAddressDetails], [Neighborhood], [CityId], [PostalCode], [CountryCode]) VALUES (3003, N'b6641c68-f250-47d2-94e3-14d580b8decd', 1, CAST(-22.900523 AS Decimal(9, 6)), CAST(-43.287372 AS Decimal(9, 6)), N'Rua Ajuratuba', N'121', N'bloco D', N'Todos os Santos', 3269, 20735050, N'BR')
GO
INSERT [dbo].[Location] ([LocationId], [OwnerId], [LocationCategoryId], [Latitude], [Longitude], [StreetName], [StreetNumber], [AdditionalAddressDetails], [Neighborhood], [CityId], [PostalCode], [CountryCode]) VALUES (3004, N'60f9eba1-9eaa-422f-8cfc-eaba11e8e22f', 1, CAST(-22.904976 AS Decimal(9, 6)), CAST(-43.177722 AS Decimal(9, 6)), N'Avenida Rio Branco', N'111', N'', N'Centro', 3269, 123123312, N'BR')
GO
INSERT [dbo].[Location] ([LocationId], [OwnerId], [LocationCategoryId], [Latitude], [Longitude], [StreetName], [StreetNumber], [AdditionalAddressDetails], [Neighborhood], [CityId], [PostalCode], [CountryCode]) VALUES (3005, N'b909ad6a-eb2c-4dcf-a583-c297d71ed0cf', 1, CAST(-22.904976 AS Decimal(9, 6)), CAST(-43.177722 AS Decimal(9, 6)), N'Avenida Rio Branco', N'111', N'', N'Centro', 3269, 123123312, N'BR')
GO
SET IDENTITY_INSERT [dbo].[Location] OFF
GO







