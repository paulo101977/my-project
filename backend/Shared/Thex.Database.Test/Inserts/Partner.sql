﻿IF NOT EXISTS (SELECT * FROM Partner WHERE PartnerName = 'HIGS')
BEGIN
	INSERT [dbo].[Partner] ([PartnerId], [PartnerName]) VALUES (1, 'HIGS')
END

IF NOT EXISTS (SELECT * FROM Partner WHERE PartnerName = 'ENOTAS')
BEGIN
	INSERT [dbo].[Partner] ([PartnerId], [PartnerName]) VALUES (4, 'ENOTAS')
END

IF NOT EXISTS (SELECT * FROM Partner WHERE PartnerName = 'INVOICEXPRESS')
BEGIN
	INSERT [dbo].[Partner] ([PartnerId], [PartnerName]) VALUES (5, 'INVOICEXPRESS')
END