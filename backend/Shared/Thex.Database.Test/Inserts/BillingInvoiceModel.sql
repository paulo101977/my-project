﻿INSERT [dbo].[BillingInvoiceModel] ([BillingInvoiceModelId], [Description], [TaxModel], [IsVisible], [BillingInvoiceTypeId]) VALUES (1, N'NFS-e', N'NFS-e', 1, 1)
INSERT [dbo].[BillingInvoiceModel] ([BillingInvoiceModelId], [Description], [TaxModel], [IsVisible], [BillingInvoiceTypeId]) VALUES (2, N'NFC-e', N'NFC-e', 1, 2)
INSERT [dbo].[BillingInvoiceModel] ([BillingInvoiceModelId], [Description], [TaxModel], [IsVisible], [BillingInvoiceTypeId]) VALUES (3, N'NF-e', N'NF-e', 1, 3)
INSERT [dbo].[BillingInvoiceModel] ([BillingInvoiceModelId], [Description], [TaxModel], [IsVisible], [BillingInvoiceTypeId]) VALUES (4, N'Factura', N'Factura', 1, 4)
INSERT [dbo].[BillingInvoiceModel] ([BillingInvoiceModelId], [Description], [TaxModel], [IsVisible], [BillingInvoiceTypeId]) VALUES (5, N'Nota de Crédito', N'NFS-e', 0, 5)
INSERT [dbo].[BillingInvoiceModel] ([BillingInvoiceModelId], [Description], [TaxModel], [IsVisible], [BillingInvoiceTypeId]) VALUES (6, N'Nota de Débito', N'Nota de Débito', 0, 6)
INSERT [dbo].[BillingInvoiceModel] ([BillingInvoiceModelId], [Description], [TaxModel], [IsVisible], [BillingInvoiceTypeId]) VALUES (8, N'SAT CF-e', N'SAT CF-e', 1, 8)