﻿INSERT [dbo].[ContactInformation] ([OwnerId], [ContactInformationTypeId], [Information]) VALUES (N'eeae6861-e4f9-498e-8066-1e77f5db2305', 1, N'email22@email.com');
INSERT [dbo].[ContactInformation] ([OwnerId], [ContactInformationTypeId], [Information]) VALUES (N'ef392e81-9c98-403e-93da-a648c32b36d4', 1, N'ricardosilva4@email.com');
INSERT [dbo].[ContactInformation] ([OwnerId], [ContactInformationTypeId], [Information]) VALUES (N'5608fd69-741a-4700-9d36-c1f5b899f1f4', 1, N'ricardomendes4@email.com');
INSERT [dbo].[ContactInformation] ([OwnerId], [ContactInformationTypeId], [Information]) VALUES (N'eeae6861-e4f9-498e-8066-1e77f5db2305', 2, N'22111122');
INSERT [dbo].[ContactInformation] ([OwnerId], [ContactInformationTypeId], [Information]) VALUES (N'ef392e81-9c98-403e-93da-a648c32b36d4', 2, N'22123342');
INSERT [dbo].[ContactInformation] ([OwnerId], [ContactInformationTypeId], [Information]) VALUES (N'5608fd69-741a-4700-9d36-c1f5b899f1f4', 2, N'22010111');
INSERT [dbo].[ContactInformation] ([OwnerId], [ContactInformationTypeId], [Information]) VALUES (N'eeae6861-e4f9-498e-8066-1e77f5db2305', 3, N'www.home22.com.br');

INSERT [dbo].[ContactInformation] ([OwnerId], [ContactInformationTypeId], [Information]) VALUES (N'5f797fb0-4645-4cea-8905-aa3160847d37', 1, N'saulo@email.com.br');
INSERT [dbo].[ContactInformation] ([OwnerId], [ContactInformationTypeId], [Information]) VALUES (N'5f797fb0-4645-4cea-8905-aa3160847d37', 2, N'21 9999 0000');
INSERT [dbo].[ContactInformation] ([OwnerId], [ContactInformationTypeId], [Information]) VALUES (N'5f797fb0-4645-4cea-8905-aa3160847d37', 3, N'www.saulo.com');

INSERT [dbo].[ContactInformation] ([OwnerId], [ContactInformationTypeId], [Information]) VALUES (N'5f797fb0-4645-4cea-8905-aa3160847d36', 1, N'yoseph@email.com');
INSERT [dbo].[ContactInformation] ([OwnerId], [ContactInformationTypeId], [Information]) VALUES (N'5f797fb0-4645-4cea-8905-aa3160847d36', 2, N'2199993333');
INSERT [dbo].[ContactInformation] ([OwnerId], [ContactInformationTypeId], [Information]) VALUES (N'5f797fb0-4645-4cea-8905-aa3160847d36', 3, N'yoseph.com');

INSERT [dbo].[ContactInformation] ([OwnerId], [ContactInformationTypeId], [Information]) VALUES (N'5f797fb0-4645-4cea-8905-aa3160847d33', 1, N'marcelo@email.com');
INSERT [dbo].[ContactInformation] ([OwnerId], [ContactInformationTypeId], [Information]) VALUES (N'5f797fb0-4645-4cea-8905-aa3160847d33', 2, N'21 30991111');
