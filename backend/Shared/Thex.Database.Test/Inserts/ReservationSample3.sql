﻿
SET IDENTITY_INSERT [dbo].[Reservation] ON 

GO
INSERT [dbo].[Reservation] ([ReservationId], [ReservationUid], [ReservationCode], [ReservationVendorCode], [PropertyId], [ContactName], [ContactEmail], [ContactPhone], [InternalComments], [ExternalComments], [GroupName], [Deadline], [UnifyAccounts], [BusinessSourceId], [MarketSegmentId], [CompanyClientId], [CompanyClientContactPersonId], [IsDeleted], [CreationTime], [CreatorUserId], [LastModificationTime], [LastModifierUserId], [DeletionTime], [DeleterUserId]) VALUES (6, N'0318096b-e7bd-4a57-89dc-75c0d81d88f4', N'XZ9CG71V7', N'001', 11, N'Arthur', N'arthur@email.com', N'3017283', N'', N'', N'', NULL, 0, 1, 1, NULL, NULL, 0, GETUTCDATE(), NULL, GETUTCDATE(), NULL, NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[Reservation] OFF
GO
INSERT [dbo].[ReservationConfirmation] ([ReservationConfirmationId], [ReservationId], [PaymentTypeId], [PlasticId], [BillingClientId], [EnsuresNoShow], [Value], [Deadline]) VALUES (N'5e5b197a-06fe-4b14-b1c7-73494dd4dd28', 6, 1, NULL, NULL, 0, NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[ReservationItem] ON 

GO
INSERT [dbo].[ReservationItem] ([ReservationItemId], [ReservationItemUid], [ReservationId], [ReasonId], [EstimatedArrivalDate], [CheckInDate], [EstimatedDepartureDate], [CheckOutDate], [CancellationDate], [CancellationDescription], [ReservationItemCode], [RequestedRoomTypeId], [ReceivedRoomTypeId], [RoomId], [AdultCount], [ChildCount], [ReservationItemStatusId], [RoomLayoutId], [ExtraBedCount], [ExtraCribCount], [IsDeleted], [CreationTime], [CreatorUserId], [LastModificationTime], [LastModifierUserId], [DeletionTime], [DeleterUserId]) VALUES (11, N'65adafb2-2037-4cf9-9f66-69dfb017980f', 6, NULL,  GETUTCDATE()+10, NULL,  GETUTCDATE()+11, NULL, NULL, NULL, N'XZ9CG71V7-0002', 1, 1, NULL, 2, 0, 1, N'88c38586-5c4b-489b-8bb9-0aa7d78dea99', 0, 0, 0, CAST(N'2018-04-02T16:25:18.663' AS DateTime), NULL, CAST(N'2018-04-02T19:25:19.640' AS DateTime), NULL, NULL, NULL)
GO
INSERT [dbo].[ReservationItem] ([ReservationItemId], [ReservationItemUid], [ReservationId], [ReasonId], [EstimatedArrivalDate], [CheckInDate], [EstimatedDepartureDate], [CheckOutDate], [CancellationDate], [CancellationDescription], [ReservationItemCode], [RequestedRoomTypeId], [ReceivedRoomTypeId], [RoomId], [AdultCount], [ChildCount], [ReservationItemStatusId], [RoomLayoutId], [ExtraBedCount], [ExtraCribCount], [IsDeleted], [CreationTime], [CreatorUserId], [LastModificationTime], [LastModifierUserId], [DeletionTime], [DeleterUserId]) VALUES (12, N'7cbf4489-f8fb-43a4-9d92-4ca021457c0f', 6, NULL,  GETUTCDATE()+10, NULL, GETUTCDATE()+11, NULL, NULL, NULL, N'XZ9CG71V7-0001', 1, 1, NULL, 1, 0, 1, N'88c38586-5c4b-489b-8bb9-0aa7d78dea99', 0, 0, 0, CAST(N'2018-04-02T16:25:18.707' AS DateTime), NULL, CAST(N'2018-04-02T19:25:19.640' AS DateTime), NULL, NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[ReservationItem] OFF
GO
SET IDENTITY_INSERT [dbo].[GuestReservationItem] ON 

GO
INSERT [dbo].[GuestReservationItem] ([GuestReservationItemId], [ReservationItemId], [GuestId], [GuestName], [GuestEmail], [GuestDocumentTypeId], [GuestDocument], [EstimatedArrivalDate], [CheckInDate], [EstimatedDepartureDate], [CheckOutDate], [GuestStatusId], [GuestTypeId], [PreferredName], [GuestCitizenship], [GuestLanguage], [PctDailyRate], [IsIncognito], [IsChild], [ChildAge], [IsMain], [GuestRegistrationId]) VALUES (20, 11, NULL, N'Arthur', NULL, NULL, NULL,  GETUTCDATE()+10, NULL, GETUTCDATE()+11, NULL, 0, 1, NULL, NULL, NULL, 100, 0, 0, 0, 1, NULL)
GO
INSERT [dbo].[GuestReservationItem] ([GuestReservationItemId], [ReservationItemId], [GuestId], [GuestName], [GuestEmail], [GuestDocumentTypeId], [GuestDocument], [EstimatedArrivalDate], [CheckInDate], [EstimatedDepartureDate], [CheckOutDate], [GuestStatusId], [GuestTypeId], [PreferredName], [GuestCitizenship], [GuestLanguage], [PctDailyRate], [IsIncognito], [IsChild], [ChildAge], [IsMain], [GuestRegistrationId]) VALUES (21, 11, NULL, N'Larissa', NULL, NULL, NULL,  GETUTCDATE()+10, NULL, GETUTCDATE()+11, NULL, 0, 1, NULL, NULL, NULL, 0, 0, 0, 0, 0, NULL)
GO
INSERT [dbo].[GuestReservationItem] ([GuestReservationItemId], [ReservationItemId], [GuestId], [GuestName], [GuestEmail], [GuestDocumentTypeId], [GuestDocument], [EstimatedArrivalDate], [CheckInDate], [EstimatedDepartureDate], [CheckOutDate], [GuestStatusId], [GuestTypeId], [PreferredName], [GuestCitizenship], [GuestLanguage], [PctDailyRate], [IsIncognito], [IsChild], [ChildAge], [IsMain], [GuestRegistrationId]) VALUES (22, 12, NULL, N'Yoseph', NULL, NULL, NULL,  GETUTCDATE()+10, NULL, GETUTCDATE()+11, NULL, 0, 1, NULL, NULL, NULL, 100, 0, 0, 0, 1, NULL)
GO
SET IDENTITY_INSERT [dbo].[GuestReservationItem] OFF
GO
SET IDENTITY_INSERT [dbo].[ReservationBudget] ON 

GO
INSERT [dbo].[ReservationBudget] ([ReservationBudgetId], [ReservationBudgetUid], [ReservationItemId], [BudgetDay], [BaseRate], [ChildRate], [MealPlanTypeId], [RateVariation], [AgreementRate], [ManualRate], [Discount], [CurrencyId], [CurrencySymbol]) VALUES (31, N'1113c2a2-c64d-459e-9cc7-240ae26917c8',11, CAST(N'2018-03-28' AS Date), CAST(300.0000 AS Numeric(18, 4)), CAST(150.0000 AS Numeric(18, 4)), 1, CAST(20.0000 AS Numeric(18, 4)), CAST(30.0000 AS Numeric(18, 4)), CAST(50.0000 AS Numeric(18, 4)), CAST(500.0000 AS Numeric(18, 4)), N'67838acb-9525-4aeb-b0a6-127c1b986c48', NULL)
GO
INSERT [dbo].[ReservationBudget] ([ReservationBudgetId], [ReservationBudgetUid], [ReservationItemId], [BudgetDay], [BaseRate], [ChildRate], [MealPlanTypeId], [RateVariation], [AgreementRate], [ManualRate], [Discount], [CurrencyId], [CurrencySymbol]) VALUES (32, N'333c8589-34d5-42e6-b512-e320dad9d071', 11, CAST(N'2018-03-28' AS Date), CAST(300.0000 AS Numeric(18, 4)), CAST(150.0000 AS Numeric(18, 4)), 1, CAST(20.0000 AS Numeric(18, 4)), CAST(30.0000 AS Numeric(18, 4)), CAST(50.0000 AS Numeric(18, 4)), CAST(500.0000 AS Numeric(18, 4)), N'67838acb-9525-4aeb-b0a6-127c1b986c48', NULL)
GO
SET IDENTITY_INSERT [dbo].[ReservationBudget] OFF