﻿
SET IDENTITY_INSERT [dbo].[Property] ON 
INSERT [dbo].[Property] ([PropertyId],[CompanyId], [PropertyTypeId], [BrandId], [Name], [PropertyUId], [IsDeleted], [TenantId], [CreationTime], [CreatorUserId], [LastModifierUserId], [DeletionTime], [DeleterUserId]) VALUES (11, 1, 1, 1, N'Hotel Totvs Resorts Rio de Janeiro', N'6146da81-9cc3-46ef-b855-825551594bfe', 0, N'23eb803c-726a-4c7c-b25b-2c22a56793d9', CAST(N'2017-08-12T18:14:32.740' AS DateTime), NULL, NULL, NULL, NULL);
INSERT [dbo].[Property] ([PropertyId], [PropertyStatusId],  [CompanyId], [PropertyTypeId], [BrandId], [Name], [PropertyUId], [IsDeleted], [TenantId], [CreationTime], [CreatorUserId], [LastModifierUserId], [DeletionTime], [DeleterUserId]) VALUES (22, 15, 2, 1, 5, N'Hotel Atlas Rio de Janeiro', N'6146da81-9cc3-46ef-b855-825551594b23', 0, N'23eb803c-726a-4c7c-b25b-2c22a5679323', CAST(N'2017-08-12T18:14:32.740' AS DateTime), NULL, NULL, NULL, NULL);
INSERT [dbo].[Property] ([PropertyId], [PropertyStatusId],  [CompanyId], [PropertyTypeId], [BrandId], [Name], [PropertyUId], [IsDeleted], [TenantId], [CreationTime], [CreatorUserId], [LastModifierUserId], [DeletionTime], [DeleterUserId]) VALUES (33, 19, 3, 1, 6, N'Hotel Portugal', N'6146da81-9cc3-46ef-b855-825551594b72', 0, N'23eb803c-726a-4c7c-b25b-2c22a5679375', GETDATE(), NULL, NULL, NULL, NULL);


SET IDENTITY_INSERT [dbo].[Property] OFF