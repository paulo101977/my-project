﻿INSERT INTO [PropertyType] ([PropertyTypeId], [Name]) VALUES (1, 'Hotel');
INSERT INTO [PropertyType] ([PropertyTypeId], [Name]) VALUES (2, 'Resort');
INSERT INTO [PropertyType] ([PropertyTypeId], [Name]) VALUES (3, 'Bed & Breakfast');
INSERT INTO [PropertyType] ([PropertyTypeId], [Name]) VALUES (4, 'Flat Hotel');
INSERT INTO [PropertyType] ([PropertyTypeId], [Name]) VALUES (5, 'Hostel');
INSERT INTO [PropertyType] ([PropertyTypeId], [Name]) VALUES (6, 'Motel');