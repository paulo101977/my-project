﻿INSERT INTO [ContactInformationType] ([ContactInformationTypeId], [Name]) VALUES (1, 'Email');
INSERT INTO [ContactInformationType] ([ContactInformationTypeId], [Name]) VALUES (2, 'Phone Number');
INSERT INTO [ContactInformationType] ([ContactInformationTypeId], [Name]) VALUES (3, 'Website');
INSERT INTO [ContactInformationType] ([ContactInformationTypeId], [Name]) VALUES (4, 'Cell Phone Number');