﻿IF NOT EXISTS (SELECT * FROM IntegrationPartner WHERE TokenClient = 'OTUxMmI3YzItZGQ5Ny00YjE5LTg1ZTctZWRmNzJkYWIwNDAw')
BEGIN
	INSERT [dbo].[IntegrationPartner] ([TokenClient], [TokenApplication], [IntegrationPartnerName], [IntegrationPartnerType], [IsActive]) VALUES ('OTUxMmI3YzItZGQ5Ny00YjE5LTg1ZTctZWRmNzJkYWIwNDAw', null, 'ENOTAS', 4, 1)
END

IF NOT EXISTS (SELECT * FROM IntegrationPartner WHERE IntegrationPartnerName = 'INVOICEXPRESS')
BEGIN
	INSERT [dbo].[IntegrationPartner] ([TokenClient], [TokenApplication], [IntegrationPartnerName], [IntegrationPartnerType], [IsActive]) VALUES (null, null, 'INVOICEXPRESS', 5, 1)
END

IF NOT EXISTS (SELECT * FROM IntegrationPartner WHERE IntegrationPartnerName = 'SIBA')
BEGIN
	INSERT [dbo].[IntegrationPartner] ([TokenClient], [TokenApplication], [IntegrationPartnerName], [IntegrationPartnerType], [IsActive]) VALUES (null, null, 'SIBA', 6, 1)
END
