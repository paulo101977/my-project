﻿INSERT INTO [dbo].[Person]
		   ([PersonId],[PersonType],[FirstName],[LastName],[DateOfBirth],[CountrySubdivisionId])
	 VALUES
		   ('1286F3D0-432C-4DE0-9A83-D6BCBE7F7722', 'C', 'Thex', 'Thex', NULL, 1);
GO

INSERT INTO [dbo].[ContactInformation]
		   ([OwnerId],[ContactInformationTypeId],[Information])
	 VALUES
		   ('1286F3D0-432C-4DE0-9A83-D6BCBE7F7722', 1, 'thex@totvscmnet.com.br');
GO


INSERT INTO [dbo].[Person]
		   ([PersonId],[PersonType],[FirstName],[LastName],[DateOfBirth],[CountrySubdivisionId])
	 VALUES
		   ('1286F3D0-432C-4DE0-9A83-D6BCBE7F7756', 'C', 'Totvs', 'Hotel', NULL, 0);
GO

INSERT INTO [dbo].[ContactInformation]
		   ([OwnerId],[ContactInformationTypeId],[Information])
	 VALUES
		   ('1286F3D0-432C-4DE0-9A83-D6BCBE7F7756', 1, 'totvs@totvscmnet.com.br');
GO



INSERT INTO [dbo].[Person]
		   ([PersonId],[PersonType],[FirstName],[LastName],[DateOfBirth],[CountrySubdivisionId])
	 VALUES
		   ('1286F3D0-432C-4DE0-9A83-D6BCBE7F7723', 'C', 'Atlas', 'Atlas', NULL, 0);
GO

INSERT INTO [dbo].[ContactInformation]
		   ([OwnerId],[ContactInformationTypeId],[Information])
	 VALUES
		   ('1286F3D0-432C-4DE0-9A83-D6BCBE7F7723', 1, 'atlas@totvscmnet.com.br');
GO



INSERT [dbo].[Users] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [PersonId], [TenantId], [PreferredLanguage], [PreferredCulture], [Name], [IsAdmin], [IsActive], [IsDeleted], [CreationTime], [CreatorUserId], [LastModificationTime], [LastModifierUserId], [DeletionTime], [DeleterUserId]) VALUES (N'6adab508-9e65-47d2-9466-b8fddeb25fb4', N'atlas@totvscmnet.com.br', N'ATLAS@TOTVSCMNET.COM.BR', N'atlas@totvscmnet.com.br', N'ATLAS@TOTVSCMNET.COM.BR', 0, N'AQAAAAEAACcQAAAAELGyI3o2locEHeC17U+Uy8Mhx6MDDvjfnO0eKjbUstIRvhHE60dHN30XgivoLkMvMw==', N'AWVWE4U2J3IKEFOWR5OT4VRF2NAGTV2D', N'cdf7177e-b9a2-4c19-a4fc-b6e829c09b42', NULL, 0, 0, NULL, 1, 0, N'1286f3d0-432c-4de0-9a83-d6bcbe7f7723', N'23eb803c-726a-4c7c-b25b-2c22a5679323', N'pt-br', N'pt-br', N'Atlas', 0, 1, 0, CAST(N'2018-12-21T11:36:11.750' AS DateTime), NULL, CAST(N'2019-01-10T13:26:38.313' AS DateTime), N'e86b85a4-4b07-4a14-8005-168af644c89a', NULL, NULL)
GO
INSERT [dbo].[Users] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [PersonId], [TenantId], [PreferredLanguage], [PreferredCulture], [Name], [IsAdmin], [IsActive], [IsDeleted], [CreationTime], [CreatorUserId], [LastModificationTime], [LastModifierUserId], [DeletionTime], [DeleterUserId]) VALUES (N'66133325-3ce6-44fb-8474-6ad0ef653ff4', N'thex@totvscmnet.com.br', N'THEX@TOTVSCMNET.COM.BR', N'thex@totvscmnet.com.br', N'THEX@TOTVSCMNET.COM.BR', 0, N'AQAAAAEAACcQAAAAELGyI3o2locEHeC17U+Uy8Mhx6MDDvjfnO0eKjbUstIRvhHE60dHN30XgivoLkMvMw==', N'AWVWE4U2J3IKEFOWR5OT4VRF2NAGTV2D', N'cdf7177e-b9a2-4c19-a4fc-b6e829c09b42', NULL, 0, 0, NULL, 1, 0, N'1286f3d0-432c-4de0-9a83-d6bcbe7f7722', N'23eb803c-726a-4c7c-b25b-2c22a5679322', N'pt-br', N'pt-br', N'Thex', 1, 1, 0, CAST(N'2018-12-21T11:36:11.750' AS DateTime), NULL, CAST(N'2018-12-21T11:36:11.750' AS DateTime), NULL, NULL, NULL)
GO
INSERT [dbo].[Users] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [PersonId], [TenantId], [PreferredLanguage], [PreferredCulture], [Name], [IsAdmin], [IsActive], [IsDeleted], [CreationTime], [CreatorUserId], [LastModificationTime], [LastModifierUserId], [DeletionTime], [DeleterUserId]) VALUES (N'94af4673-730e-43fb-9bed-73f9cf69c716', N'totvs@totvscmnet.com.br', N'TOTVS@TOTVSCMNET.COM.BR', N'totvs@totvscmnet.com.br', N'TOTVS@TOTVSCMNET.COM.BR', 0, N'AQAAAAEAACcQAAAAELGyI3o2locEHeC17U+Uy8Mhx6MDDvjfnO0eKjbUstIRvhHE60dHN30XgivoLkMvMw==', N'AWVWE4U2J3IKEFOWR5OT4VRF2NAGTV2D', N'cdf7177e-b9a2-4c19-a4fc-b6e829c09b42', NULL, 0, 0, NULL, 1, 0, N'1286f3d0-432c-4de0-9a83-d6bcbe7f7756', N'23eb803c-726a-4c7c-b25b-2c22a56793d9', N'pt-br', N'pt-br', N'Totvs', 0, 1, 0, CAST(N'2018-12-21T11:36:11.750' AS DateTime), NULL, CAST(N'2018-12-21T11:36:11.750' AS DateTime), NULL, NULL, NULL)
GO
INSERT [dbo].[UserInvitation] ([UserInvitationId], [ChainId], [PropertyId], [BrandId], [Email], [Name], [InvitationLink], [InvitationDate], [InvitationAcceptanceDate], [IsActive], [TenantId], [IsDeleted], [CreationTime], [CreatorUserId], [LastModificationTime], [LastModifierUserId], [DeletionTime], [DeleterUserId], [UserId]) VALUES (N'a8e90080-af60-4ab3-8845-8d0206d5d623', NULL, NULL, NULL, N'atlas@totvscmnet.com.br', N'Atlas', N'http://thsfront-dev.azurewebsites.net/auth/signup;token=eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1bmlxdWVfbmFtZSI6WyI4M2EwZWZlMC1lMTQ1LTRkZTYtOWNlYy1mZDY3NWY4YWZkZGMiLCJUaGV4Il0sImp0aSI6ImQwMmMwNTllNzFjYzQ1ZGE5YjdmNTE5NzAyZjlkMDg4IiwiZW1haWwiOiJ0aGV4QHRvdHZzY21uZXQuY29tLmJyIiwiTG9nZ2VyVXNlckVtYWlsIjoidGhleEB0b3R2cy5jb20uYnIiLCJMb2dnZWRVc2VyTmFtZSI6IlRoZXgiLCJMb2dnZWRVc2VyVWlkIjoiMjFlYzZmYTMtYWZkOS00ODg5LTk0OTAtYTRkNTA4NjNlZWY1IiwiUHJvcGVydHlJZCI6IjEiLCJDaGFpbklkIjoiMSIsIlRlbmFudElkIjoiMjNlYjgwM2MtNzI2YS00YzdjLWIyNWItMmMyMmE1Njc5M2Q5IiwibmJmIjoxNTE5MjMwMTQ3LCJleHAiOjE1MjE4MjIxNDcsImlhdCI6MTUxOTIzMDE0NywiaXNzIjoidGhleElzc3VlciIsImF1ZCI6InRoZXhBdWRpZW5jZSJ9.LC7QGWctbd6h1cmZFDz8l0hQdI1MEX8s2lqv19zdVlee1ch-vk0B5FGVyR1nVvHu3O3zPeHBznHIese2-aJ3rF9C563WwUHh7lErG-TNHXI0vDlg8K3fBJy8ZxHuKEAKmRD2u5uZjQC2ycqn9HI-8MWEoN7IZrBlVDEvw1d1ndiiCyc-nR_hDwRX0pRHPRoOEjRnVTPADIOiMOdWcWZ6yjwh9HLK9ilBa2ZT86xaX7HYpM3CdYB99La3cX5LiP1zDWXYvC1HN6bzd_JnELSlepguW2F6F_Mxrud6iL3_pzhsoaMvM82nqhPZ4XP38mKJVR3pjwce1t_4DDWl9ahlag', CAST(N'2018-12-21T13:36:06.250' AS DateTime), NULL, 1, N'23eb803c-726a-4c7c-b25b-2c22a5679323', 0, CAST(N'2018-12-21T13:36:06.250' AS DateTime), NULL, CAST(N'2018-12-21T13:36:06.250' AS DateTime), NULL, NULL, NULL, N'6adab508-9e65-47d2-9466-b8fddeb25fb4')
GO
INSERT [dbo].[UserInvitation] ([UserInvitationId], [ChainId], [PropertyId], [BrandId], [Email], [Name], [InvitationLink], [InvitationDate], [InvitationAcceptanceDate], [IsActive], [TenantId], [IsDeleted], [CreationTime], [CreatorUserId], [LastModificationTime], [LastModifierUserId], [DeletionTime], [DeleterUserId], [UserId]) VALUES (N'a8e90080-af60-4ab3-8845-8d0206d5d622', NULL, NULL, NULL, N'thex@totvscmnet.com.br', N'Thex', N'http://thsfront-dev.azurewebsites.net/auth/signup;token=eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1bmlxdWVfbmFtZSI6WyI4M2EwZWZlMC1lMTQ1LTRkZTYtOWNlYy1mZDY3NWY4YWZkZGMiLCJUaGV4Il0sImp0aSI6ImQwMmMwNTllNzFjYzQ1ZGE5YjdmNTE5NzAyZjlkMDg4IiwiZW1haWwiOiJ0aGV4QHRvdHZzY21uZXQuY29tLmJyIiwiTG9nZ2VyVXNlckVtYWlsIjoidGhleEB0b3R2cy5jb20uYnIiLCJMb2dnZWRVc2VyTmFtZSI6IlRoZXgiLCJMb2dnZWRVc2VyVWlkIjoiMjFlYzZmYTMtYWZkOS00ODg5LTk0OTAtYTRkNTA4NjNlZWY1IiwiUHJvcGVydHlJZCI6IjEiLCJDaGFpbklkIjoiMSIsIlRlbmFudElkIjoiMjNlYjgwM2MtNzI2YS00YzdjLWIyNWItMmMyMmE1Njc5M2Q5IiwibmJmIjoxNTE5MjMwMTQ3LCJleHAiOjE1MjE4MjIxNDcsImlhdCI6MTUxOTIzMDE0NywiaXNzIjoidGhleElzc3VlciIsImF1ZCI6InRoZXhBdWRpZW5jZSJ9.LC7QGWctbd6h1cmZFDz8l0hQdI1MEX8s2lqv19zdVlee1ch-vk0B5FGVyR1nVvHu3O3zPeHBznHIese2-aJ3rF9C563WwUHh7lErG-TNHXI0vDlg8K3fBJy8ZxHuKEAKmRD2u5uZjQC2ycqn9HI-8MWEoN7IZrBlVDEvw1d1ndiiCyc-nR_hDwRX0pRHPRoOEjRnVTPADIOiMOdWcWZ6yjwh9HLK9ilBa2ZT86xaX7HYpM3CdYB99La3cX5LiP1zDWXYvC1HN6bzd_JnELSlepguW2F6F_Mxrud6iL3_pzhsoaMvM82nqhPZ4XP38mKJVR3pjwce1t_4DDWl9ahlag', CAST(N'2018-12-21T13:36:06.227' AS DateTime), NULL, 1, N'23eb803c-726a-4c7c-b25b-2c22a5679322', 0, CAST(N'2018-12-21T13:36:06.227' AS DateTime), NULL, CAST(N'2018-12-21T13:36:06.227' AS DateTime), NULL, NULL, NULL, N'66133325-3ce6-44fb-8474-6ad0ef653ff4')
GO
INSERT [dbo].[UserInvitation] ([UserInvitationId], [ChainId], [PropertyId], [BrandId], [Email], [Name], [InvitationLink], [InvitationDate], [InvitationAcceptanceDate], [IsActive], [TenantId], [IsDeleted], [CreationTime], [CreatorUserId], [LastModificationTime], [LastModifierUserId], [DeletionTime], [DeleterUserId], [UserId]) VALUES (N'a8e90080-af60-4ab3-8845-8d0206d5d600', NULL, NULL, NULL, N'totvs@totvscmnet.com.br', N'Totvs', N'http://thsfront-dev.azurewebsites.net/auth/signup;token=eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1bmlxdWVfbmFtZSI6WyI4M2EwZWZlMC1lMTQ1LTRkZTYtOWNlYy1mZDY3NWY4YWZkZGMiLCJUaGV4Il0sImp0aSI6ImQwMmMwNTllNzFjYzQ1ZGE5YjdmNTE5NzAyZjlkMDg4IiwiZW1haWwiOiJ0aGV4QHRvdHZzY21uZXQuY29tLmJyIiwiTG9nZ2VyVXNlckVtYWlsIjoidGhleEB0b3R2cy5jb20uYnIiLCJMb2dnZWRVc2VyTmFtZSI6IlRoZXgiLCJMb2dnZWRVc2VyVWlkIjoiMjFlYzZmYTMtYWZkOS00ODg5LTk0OTAtYTRkNTA4NjNlZWY1IiwiUHJvcGVydHlJZCI6IjEiLCJDaGFpbklkIjoiMSIsIlRlbmFudElkIjoiMjNlYjgwM2MtNzI2YS00YzdjLWIyNWItMmMyMmE1Njc5M2Q5IiwibmJmIjoxNTE5MjMwMTQ3LCJleHAiOjE1MjE4MjIxNDcsImlhdCI6MTUxOTIzMDE0NywiaXNzIjoidGhleElzc3VlciIsImF1ZCI6InRoZXhBdWRpZW5jZSJ9.LC7QGWctbd6h1cmZFDz8l0hQdI1MEX8s2lqv19zdVlee1ch-vk0B5FGVyR1nVvHu3O3zPeHBznHIese2-aJ3rF9C563WwUHh7lErG-TNHXI0vDlg8K3fBJy8ZxHuKEAKmRD2u5uZjQC2ycqn9HI-8MWEoN7IZrBlVDEvw1d1ndiiCyc-nR_hDwRX0pRHPRoOEjRnVTPADIOiMOdWcWZ6yjwh9HLK9ilBa2ZT86xaX7HYpM3CdYB99La3cX5LiP1zDWXYvC1HN6bzd_JnELSlepguW2F6F_Mxrud6iL3_pzhsoaMvM82nqhPZ4XP38mKJVR3pjwce1t_4DDWl9ahlag', CAST(N'2018-12-21T13:36:06.247' AS DateTime), NULL, 1, N'23eb803c-726a-4c7c-b25b-2c22a56793d9', 0, CAST(N'2018-12-21T13:36:06.247' AS DateTime), NULL, CAST(N'2018-12-21T13:36:06.247' AS DateTime), NULL, NULL, NULL, N'94af4673-730e-43fb-9bed-73f9cf69c716')
GO


INSERT INTO [dbo].[UserTenant]
           ([UserTenantId]
           ,[UserId]
           ,[TenantId]
           ,[IsActive]
           ,[IsDeleted]
           ,[CreationTime]
           ,[CreatorUserId])
     VALUES
           ('563a1db5-1f9f-4532-a877-14a110e65f1e'
           ,'66133325-3ce6-44fb-8474-6ad0ef653ff4'
           ,'23eb803c-726a-4c7c-b25b-2c22a56793d9'
           ,1
           ,0
           ,SYSDATETIME()
           ,'66133325-3ce6-44fb-8474-6ad0ef653ff4')
GO




