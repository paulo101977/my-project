﻿SET IDENTITY_INSERT [dbo].[BillingItem] ON 
GO
INSERT [dbo].[BillingItem] ([BillingItemId], [BillingItemTypeId], [BillingItemName], [BillingItemCategoryId], [Description], [PropertyId], [PaymentTypeId], [PlasticBrandPropertyId], [AcquirerId], [MaximumInstallmentsQuantity], [IntegrationCode], [IsActive], [TenantId]) VALUES (1, 3, N'crédito 222', NULL, N'crédito 222', 11, 5, N'30ccbd8e-2da1-48f1-bf3e-10f81eb72503', N'6a27e16a-f227-4256-a62f-96e7b39d0658', 1, NULL, 1, N'23eb803c-726a-4c7c-b25b-2c22a56793d9')
GO
INSERT [dbo].[BillingItem] ([BillingItemId], [BillingItemTypeId], [BillingItemName], [BillingItemCategoryId], [Description], [PropertyId], [PaymentTypeId], [PlasticBrandPropertyId], [AcquirerId], [MaximumInstallmentsQuantity], [IntegrationCode], [IsActive], [TenantId]) VALUES (2, 3, N'crédito 222', NULL, N'crédito 222', 11, 5, N'30ccbd8e-2da1-48f1-bf3e-10f81eb72522', N'6a27e16a-f227-4256-a62f-96e7b39d0658', 2, NULL, 1, N'23eb803c-726a-4c7c-b25b-2c22a56793d9')
GO
INSERT [dbo].[BillingItem] ([BillingItemId], [BillingItemTypeId], [BillingItemName], [BillingItemCategoryId], [Description], [PropertyId], [PaymentTypeId], [PlasticBrandPropertyId], [AcquirerId], [MaximumInstallmentsQuantity], [IntegrationCode], [IsActive], [TenantId]) VALUES (3, 3, N'a faturar 111', NULL, N'a faturar 111', 11, 6, NULL, NULL, NULL, NULL, 1, N'23eb803c-726a-4c7c-b25b-2c22a56793d9')
GO																																																															
INSERT [dbo].[BillingItem] ([BillingItemId], [BillingItemTypeId], [BillingItemName], [BillingItemCategoryId], [Description], [PropertyId], [PaymentTypeId], [PlasticBrandPropertyId], [AcquirerId], [MaximumInstallmentsQuantity], [IntegrationCode], [IsActive], [TenantId]) VALUES (4, 3, N'cheque 111', NULL, N'cheque 111', 11, 7, NULL, NULL, NULL, NULL, 1, N'23eb803c-726a-4c7c-b25b-2c22a56793d9')
GO																																																															
INSERT [dbo].[BillingItem] ([BillingItemId], [BillingItemTypeId], [BillingItemName], [BillingItemCategoryId], [Description], [PropertyId], [PaymentTypeId], [PlasticBrandPropertyId], [AcquirerId], [MaximumInstallmentsQuantity], [IntegrationCode], [IsActive], [TenantId]) VALUES (5, 3, N'dinheiro 222', NULL, N'dinheiro 222', 11, 8, NULL, NULL, NULL, NULL, 1, N'23eb803c-726a-4c7c-b25b-2c22a56793d9')
GO																																																															
INSERT [dbo].[BillingItem] ([BillingItemId], [BillingItemTypeId], [BillingItemName], [BillingItemCategoryId], [Description], [PropertyId], [PaymentTypeId], [PlasticBrandPropertyId], [AcquirerId], [MaximumInstallmentsQuantity], [IntegrationCode], [IsActive], [TenantId]) VALUES (6, 1, N'Totvs serviço', 19, NULL, 11, NULL, NULL, NULL, NULL, NULL, 1, N'23eb803c-726a-4c7c-b25b-2c22a56793d9')
GO
INSERT [dbo].[BillingItem] ([BillingItemId], [BillingItemTypeId], [BillingItemName], [BillingItemCategoryId], [Description], [PropertyId], [PaymentTypeId], [PlasticBrandPropertyId], [AcquirerId], [MaximumInstallmentsQuantity], [IntegrationCode], [IsActive], [TenantId]) VALUES (7, 2, N'Totvs taxa', 20, NULL, 11, NULL, NULL, NULL, NULL, NULL, 1, N'23eb803c-726a-4c7c-b25b-2c22a56793d9')
GO
INSERT [dbo].[BillingItem] ([BillingItemId], [BillingItemTypeId], [BillingItemName], [BillingItemCategoryId], [Description], [PropertyId], [PaymentTypeId], [PlasticBrandPropertyId], [AcquirerId], [MaximumInstallmentsQuantity], [IntegrationCode], [IsActive], [TenantId]) VALUES (8, 4, N'PDV', null, N'PDV', 11, NULL, NULL, NULL, NULL, NULL, 1, N'23eb803c-726a-4c7c-b25b-2c22a56793d9')
GO


SET IDENTITY_INSERT [dbo].[BillingItem] OFF
GO
SET IDENTITY_INSERT [dbo].[BillingItem] OFF
GO
INSERT [dbo].[BillingItemPaymentCondition] ([BillingItemPaymentConditionId], [BillingItemId], [InstallmentNumber], [CommissionPct], [PaymentDeadline], [TenantId]) VALUES (N'88bc1875-1104-45fc-903d-1cd36b243123', 1, 1, CAST(3 AS Numeric(18, 0)), 30, N'23eb803c-726a-4c7c-b25b-2c22a56793d9')
GO
INSERT [dbo].[BillingItemPaymentCondition] ([BillingItemPaymentConditionId], [BillingItemId], [InstallmentNumber], [CommissionPct], [PaymentDeadline], [TenantId]) VALUES (N'995e217d-2d33-468a-b669-5e76650fa064', 2, 2, CAST(5 AS Numeric(18, 0)), 22, N'23eb803c-726a-4c7c-b25b-2c22a56793d9')
GO
INSERT [dbo].[BillingItemPaymentCondition] ([BillingItemPaymentConditionId], [BillingItemId], [InstallmentNumber], [CommissionPct], [PaymentDeadline], [TenantId]) VALUES (N'fa2680ba-fd4a-44d0-9632-c0ff5b6d62ad', 2, 1, CAST(4 AS Numeric(18, 0)), 22, N'23eb803c-726a-4c7c-b25b-2c22a56793d9')
GO