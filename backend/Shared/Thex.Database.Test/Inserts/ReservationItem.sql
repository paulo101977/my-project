﻿SET IDENTITY_INSERT [dbo].[ReservationItem] ON 
INSERT [dbo].[ReservationItem] (
	[ReservationItemId], [ReservationItemUid], [ReservationId], 
	[EstimatedArrivalDate], [CheckInDate], [EstimatedDepartureDate], 
	[CheckOutDate], [ReservationItemCode], [RequestedRoomTypeId], 
	[ReceivedRoomTypeId], [RoomId], [AdultCount], 
	[ChildCount], [ReservationItemStatusId], [RoomLayoutId], 
	[ExtraBedCount], [ExtraCribCount], [IsDeleted], 
	[CreationTime], [CreatorUserId], [LastModificationTime], 
	[LastModifierUserId], [DeletionTime], [DeleterUserId]) 
	VALUES (
	1, N'82c15ab0-8d25-4aaf-9047-ae636039d94f', 1, 
	CAST(N'2017-08-15T12:00:00.000' AS DateTime), NULL, CAST(N'2017-08-18T14:00:00.000' AS DateTime), 
	NULL, N'0001', 1, 
	1, 1, 3, 
	0, 1, N'74BC15D2-25CB-44E9-B43C-036FE8E5207E', 
	0, 0, 0, 
	CAST(N'2017-08-14T20:07:37.000' AS DateTime), NULL, NULL, 
	NULL, NULL, NULL);
SET IDENTITY_INSERT [dbo].[ReservationItem] OFF