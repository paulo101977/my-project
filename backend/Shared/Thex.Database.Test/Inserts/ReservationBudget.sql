﻿SET IDENTITY_INSERT [dbo].[ReservationBudget] ON 

GO
INSERT [dbo].[ReservationBudget] ([ReservationBudgetId], [ReservationBudgetUid], [ReservationItemId], [BudgetDay], [BaseRate], [ChildRate], [MealPlanTypeId], [RateVariation], [AgreementRate], [ManualRate], [Discount], [CurrencyId], [CurrencySymbol]) VALUES (6, N'277026ff-a4a6-49ae-ae49-2b07daf37d06', 3, CAST(N'2018-03-28' AS Date), CAST(300.0000 AS Numeric(18, 4)), CAST(150.0000 AS Numeric(18, 4)), 1, CAST(20.0000 AS Numeric(18, 4)), CAST(30.0000 AS Numeric(18, 4)), CAST(50.0000 AS Numeric(18, 4)), CAST(500.0000 AS Numeric(18, 4)), N'67838acb-9525-4aeb-b0a6-127c1b986c48', NULL)

SET IDENTITY_INSERT [dbo].[ReservationBudget] OFF
GO
