﻿GO
INSERT [dbo].[MealPlanType] ([MealPlanTypeId], [MealPlanTypeCode], [MealPlanTypeName]) VALUES (1, N'SAP', N'None')
GO
INSERT [dbo].[MealPlanType] ([MealPlanTypeId], [MealPlanTypeCode], [MealPlanTypeName]) VALUES (2, N'CM', N'Breakfast')
GO
INSERT [dbo].[MealPlanType] ([MealPlanTypeId], [MealPlanTypeCode], [MealPlanTypeName]) VALUES (3, N'MAP', N'Half Board')
GO
INSERT [dbo].[MealPlanType] ([MealPlanTypeId], [MealPlanTypeCode], [MealPlanTypeName]) VALUES (4, N'MAPA', N'Half Board Lunch')
GO
INSERT [dbo].[MealPlanType] ([MealPlanTypeId], [MealPlanTypeCode], [MealPlanTypeName]) VALUES (5, N'MAPJ', N'Half Board Dinner')
GO
INSERT [dbo].[MealPlanType] ([MealPlanTypeId], [MealPlanTypeCode], [MealPlanTypeName]) VALUES (6, N'FAP', N'Full Board')
GO
INSERT [dbo].[MealPlanType] ([MealPlanTypeId], [MealPlanTypeCode], [MealPlanTypeName]) VALUES (7, N'TI', N'All Inclusive')
GO