﻿SET IDENTITY_INSERT [dbo].[Company] ON 
INSERT [dbo].[Company] ([CompanyId], [CompanyUid], [PersonId], [ParentCompanyId], [ShortName], [TradeName]) VALUES (1, N'28E91463-9182-4CA5-9AD4-4339E544B2B5', N'21ec6fa3-afd9-4889-9490-a4d50863eef5', NULL, N'Hotel Totvs Resorts Rio de Janeiro', N'Hotelaria Accor Brasil S/A');
INSERT [dbo].[Company] ([CompanyId], [CompanyUid], [PersonId], [ParentCompanyId], [ShortName], [TradeName]) VALUES (2, N'28E91463-9182-4CA5-9AD4-4339E544B223', N'21ec6fa3-afd9-4889-9490-a4d50863ee23', NULL, N'Hotel Atlas Rio de Janeiro', N'Hotelaria Atlas S/A');
 INSERT [dbo].[Company] ([CompanyId], [CompanyUid], [PersonId], [ParentCompanyId], [ShortName], [TradeName]) VALUES (3, N'28E91463-9182-4CA5-9AD4-4339E544B225', N'21ec6fa3-afd9-4889-9490-a4d50863ee25', NULL, N'Hotel Portugal', N'Hotel Portugal S/A');
SET IDENTITY_INSERT [dbo].[Company] OFF
