﻿SET IDENTITY_INSERT [dbo].[Reservation] ON 

GO
INSERT [dbo].[Reservation] ([ReservationId], [ReservationUid], [ReservationCode], [ReservationVendorCode], [PropertyId], [ContactName], [ContactEmail], [ContactPhone], [InternalComments], [ExternalComments], [GroupName], [Deadline], [UnifyAccounts], [BusinessSourceId], [MarketSegmentId], [CompanyClientId], [CompanyClientContactPersonId], [IsDeleted], [CreationTime], [CreatorUserId], [LastModificationTime], [LastModifierUserId], [DeletionTime], [DeleterUserId]) VALUES (5, N'71502b8b-710c-4af5-8956-8852f92c786f', N'2RVCT97HV', N'001', 11, N'Felipe', N'felipe@email.com', N'30123455', N'Obs2', N'Obs1', N'', NULL, 0, 1, 1, NULL, NULL, 0, GETUTCDATE(), NULL, GETUTCDATE(), NULL, NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[Reservation] OFF
GO
INSERT [dbo].[ReservationConfirmation] ([ReservationConfirmationId], [ReservationId], [PaymentTypeId], [PlasticId], [BillingClientId], [EnsuresNoShow], [Value], [Deadline]) VALUES (N'c70f1075-40ab-4235-ad24-714983e81dbf', 5, 2, NULL, NULL, 0, CAST(1000.0000 AS Numeric(18, 4)), NULL)
GO
SET IDENTITY_INSERT [dbo].[ReservationItem] ON 

GO
INSERT [dbo].[ReservationItem] ([ReservationItemId], [ReservationItemUid], [ReservationId], [ReasonId], [EstimatedArrivalDate], [CheckInDate], [EstimatedDepartureDate], [CheckOutDate], [CancellationDate], [CancellationDescription], [ReservationItemCode], [RequestedRoomTypeId], [ReceivedRoomTypeId], [RoomId], [AdultCount], [ChildCount], [ReservationItemStatusId], [RoomLayoutId], [ExtraBedCount], [ExtraCribCount], [IsDeleted], [CreationTime], [CreatorUserId], [LastModificationTime], [LastModifierUserId], [DeletionTime], [DeleterUserId]) VALUES (9, N'1515bb1e-5098-4e97-ab79-6d55e39d4e00', 5, NULL, GETUTCDATE(), GETUTCDATE(), GETUTCDATE()+2, GETUTCDATE()+2, NULL, NULL, N'2RVCT97HV-0002', 2, 2, 8, 1, 0, 1, N'74bc15d2-25cb-44e9-b43c-036fe8e5207e', 0, 0, 0, GETUTCDATE(), NULL, GETUTCDATE(), NULL, NULL, NULL)
GO
INSERT [dbo].[ReservationItem] ([ReservationItemId], [ReservationItemUid], [ReservationId], [ReasonId], [EstimatedArrivalDate], [CheckInDate], [EstimatedDepartureDate], [CheckOutDate], [CancellationDate], [CancellationDescription], [ReservationItemCode], [RequestedRoomTypeId], [ReceivedRoomTypeId], [RoomId], [AdultCount], [ChildCount], [ReservationItemStatusId], [RoomLayoutId], [ExtraBedCount], [ExtraCribCount], [IsDeleted], [CreationTime], [CreatorUserId], [LastModificationTime], [LastModifierUserId], [DeletionTime], [DeleterUserId]) VALUES (10, N'b404dab1-3d85-423d-9d6e-f299fea18fe1', 5, NULL, GETUTCDATE(), GETUTCDATE(), GETUTCDATE()+2, GETUTCDATE()+2, NULL, NULL, N'2RVCT97HV-0001', 2, 2, 7, 1, 1, 1, N'74bc15d2-25cb-44e9-b43c-036fe8e5207e', 0, 0, 0, GETUTCDATE(), NULL, GETUTCDATE(), NULL, NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[ReservationItem] OFF
GO
SET IDENTITY_INSERT [dbo].[GuestReservationItem] ON 

GO
INSERT [dbo].[GuestReservationItem] ([GuestReservationItemId], [ReservationItemId], [GuestId], [GuestName], [GuestEmail], [GuestDocumentTypeId], [GuestDocument], [EstimatedArrivalDate], [CheckInDate], [EstimatedDepartureDate], [CheckOutDate], [GuestStatusId], [GuestTypeId], [PreferredName], [GuestCitizenship], [GuestLanguage], [PctDailyRate], [IsIncognito], [IsChild], [ChildAge], [IsMain], [GuestRegistrationId]) VALUES (17, 9, NULL, N'Felipe', NULL, NULL, NULL, GETUTCDATE(), NULL, GETUTCDATE()+2, NULL, 0, 1, NULL, NULL, NULL, 100, 0, 0, NULL, 1, NULL)
GO
INSERT [dbo].[GuestReservationItem] ([GuestReservationItemId], [ReservationItemId], [GuestId], [GuestName], [GuestEmail], [GuestDocumentTypeId], [GuestDocument], [EstimatedArrivalDate], [CheckInDate], [EstimatedDepartureDate], [CheckOutDate], [GuestStatusId], [GuestTypeId], [PreferredName], [GuestCitizenship], [GuestLanguage], [PctDailyRate], [IsIncognito], [IsChild], [ChildAge], [IsMain], [GuestRegistrationId]) VALUES (18, 10, NULL, N'Carol', NULL, NULL, NULL, GETUTCDATE(), NULL, GETUTCDATE()+2, NULL, 0, 1, NULL, NULL, NULL, 100, 0, 0, NULL, 1, NULL)
GO
INSERT [dbo].[GuestReservationItem] ([GuestReservationItemId], [ReservationItemId], [GuestId], [GuestName], [GuestEmail], [GuestDocumentTypeId], [GuestDocument], [EstimatedArrivalDate], [CheckInDate], [EstimatedDepartureDate], [CheckOutDate], [GuestStatusId], [GuestTypeId], [PreferredName], [GuestCitizenship], [GuestLanguage], [PctDailyRate], [IsIncognito], [IsChild], [ChildAge], [IsMain], [GuestRegistrationId]) VALUES (19, 10, NULL, N'Paulo', NULL, NULL, NULL, GETUTCDATE(), NULL, GETUTCDATE()+2, NULL, 0, 1, NULL, NULL, NULL, 0, 0, 1, 9, 0, NULL)
GO
SET IDENTITY_INSERT [dbo].[GuestReservationItem] OFF
GO
SET IDENTITY_INSERT [dbo].[ReservationBudget] ON 
GO
INSERT [dbo].[ReservationBudget] ([ReservationBudgetId], [ReservationBudgetUid], [ReservationItemId], [BudgetDay], [BaseRate], [ChildRate], [MealPlanTypeId], [RateVariation], [AgreementRate], [ManualRate], [Discount], [CurrencyId], [CurrencySymbol]) VALUES (28, N'C64F0FCD-005A-442D-9481-08C1A67F283C', 9, CAST(N'2018-03-28' AS Date), CAST(300.0000 AS Numeric(18, 4)), CAST(150.0000 AS Numeric(18, 4)), 1, CAST(20.0000 AS Numeric(18, 4)), CAST(30.0000 AS Numeric(18, 4)), CAST(50.0000 AS Numeric(18, 4)), CAST(500.0000 AS Numeric(18, 4)), N'67838acb-9525-4aeb-b0a6-127c1b986c48', NULL)
GO
INSERT [dbo].[ReservationBudget] ([ReservationBudgetId], [ReservationBudgetUid], [ReservationItemId], [BudgetDay], [BaseRate], [ChildRate], [MealPlanTypeId], [RateVariation], [AgreementRate], [ManualRate], [Discount], [CurrencyId], [CurrencySymbol]) VALUES (29, N'16526986-1b63-4ba3-b79c-6d681c601364', 9, CAST(N'2018-03-28' AS Date), CAST(300.0000 AS Numeric(18, 4)), CAST(150.0000 AS Numeric(18, 4)), 1, CAST(20.0000 AS Numeric(18, 4)), CAST(30.0000 AS Numeric(18, 4)), CAST(50.0000 AS Numeric(18, 4)), CAST(500.0000 AS Numeric(18, 4)), N'67838acb-9525-4aeb-b0a6-127c1b986c48', NULL)
GO
INSERT [dbo].[ReservationBudget] ([ReservationBudgetId], [ReservationBudgetUid], [ReservationItemId], [BudgetDay], [BaseRate], [ChildRate], [MealPlanTypeId], [RateVariation], [AgreementRate], [ManualRate], [Discount], [CurrencyId], [CurrencySymbol]) VALUES (30, N'00011546-acd9-4260-bc3a-1a3ca00a4382', 9, CAST(N'2018-03-28' AS Date), CAST(300.0000 AS Numeric(18, 4)), CAST(150.0000 AS Numeric(18, 4)), 1, CAST(20.0000 AS Numeric(18, 4)), CAST(30.0000 AS Numeric(18, 4)), CAST(50.0000 AS Numeric(18, 4)), CAST(500.0000 AS Numeric(18, 4)), N'67838acb-9525-4aeb-b0a6-127c1b986c48', NULL)
GO
SET IDENTITY_INSERT [dbo].[ReservationBudget] OFF