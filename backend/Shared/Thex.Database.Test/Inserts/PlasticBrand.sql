﻿SET IDENTITY_INSERT [dbo].[PlasticBrand] ON
INSERT [dbo].[PlasticBrand] ([PlasticBrandId], [PlasticBrandName]) VALUES (1, N'VISA');
INSERT [dbo].[PlasticBrand] ([PlasticBrandId], [PlasticBrandName]) VALUES (2, N'MASTERCARD');
INSERT [dbo].[PlasticBrand] ([PlasticBrandId], [PlasticBrandName]) VALUES (3, N'AMERICAN EXPRESS');
INSERT [dbo].[PlasticBrand] ([PlasticBrandId], [PlasticBrandName]) VALUES (4, N'ELO');
INSERT [dbo].[PlasticBrand] ([PlasticBrandId], [PlasticBrandName]) VALUES (5, N'HIPERCARD');
INSERT [dbo].[PlasticBrand] ([PlasticBrandId], [PlasticBrandName]) VALUES (6, N'DINERS CLUB');
INSERT [dbo].[PlasticBrand] ([PlasticBrandId], [PlasticBrandName]) VALUES (7, N'SOROCRED');
SET IDENTITY_INSERT [dbo].[PlasticBrand] OFF