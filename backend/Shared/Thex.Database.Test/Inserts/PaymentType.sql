﻿INSERT INTO [dbo].[PaymentType] ([PaymentTypeId], [PaymentTypeName]) VALUES (1, 'Internal Usage');
INSERT INTO [dbo].[PaymentType] ([PaymentTypeId], [PaymentTypeName]) VALUES (2, 'Deposit');
INSERT INTO [dbo].[PaymentType] ([PaymentTypeId], [PaymentTypeName]) VALUES (3, 'Courtesy');
INSERT INTO [dbo].[PaymentType] ([PaymentTypeId], [PaymentTypeName]) VALUES (4, 'Hotel');
INSERT INTO [dbo].[PaymentType] ([PaymentTypeId], [PaymentTypeName]) VALUES (5, 'Credit Card');
INSERT INTO [dbo].[PaymentType] ([PaymentTypeId], [PaymentTypeName]) VALUES (6, 'To be Billed');
INSERT INTO [dbo].[PaymentType] ([PaymentTypeId], [PaymentTypeName]) VALUES (7, 'Check');
INSERT INTO [dbo].[PaymentType] ([PaymentTypeId], [PaymentTypeName]) VALUES (8, 'Money');
INSERT INTO [dbo].[PaymentType] ([PaymentTypeId], [PaymentTypeName]) VALUES (9, 'Debit');