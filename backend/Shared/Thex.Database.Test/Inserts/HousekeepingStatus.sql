﻿INSERT INTO [HousekeepingStatus] ([HousekeepingStatusId],[StatusName],[DefaultColor]) VALUES ( 1, 'Clean', '1DC29D');
INSERT INTO [HousekeepingStatus] ([HousekeepingStatusId],[StatusName],[DefaultColor]) VALUES ( 2, 'Dirty', 'FF6969');
INSERT INTO [HousekeepingStatus] ([HousekeepingStatusId],[StatusName],[DefaultColor]) VALUES ( 3, 'Maintenance', 'A497E3');
INSERT INTO [HousekeepingStatus] ([HousekeepingStatusId],[StatusName],[DefaultColor]) VALUES ( 4, 'Inspection', 'F8E71C');
INSERT INTO [HousekeepingStatus] ([HousekeepingStatusId],[StatusName],[DefaultColor]) VALUES ( 5, 'Stowage', '00C5FF');
INSERT INTO [HousekeepingStatus] ([HousekeepingStatusId],[StatusName],[DefaultColor]) VALUES ( 6, 'Created By Hotel', '8B572A');
