﻿GO
INSERT [dbo].[PolicyType] ([PolicyTypeId], [PolicyTypeName]) VALUES (1, N'Check-in Policy')
GO
INSERT [dbo].[PolicyType] ([PolicyTypeId], [PolicyTypeName]) VALUES (2, N'Check-out Policy')
GO
INSERT [dbo].[PolicyType] ([PolicyTypeId], [PolicyTypeName]) VALUES (3, N'Modification Policy')
GO
INSERT [dbo].[PolicyType] ([PolicyTypeId], [PolicyTypeName]) VALUES (4, N'Prepayment and Warranty Policy')
GO
INSERT [dbo].[PolicyType] ([PolicyTypeId], [PolicyTypeName]) VALUES (5, N'Childrens Policy')
GO
INSERT [dbo].[PolicyType] ([PolicyTypeId], [PolicyTypeName]) VALUES (6, N'Other Policies')
GO
INSERT [dbo].[PolicyType] ([PolicyTypeId], [PolicyTypeName]) VALUES (7, N'Cancellation Policy')
GO






