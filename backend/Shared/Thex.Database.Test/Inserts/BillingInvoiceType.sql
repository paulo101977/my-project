﻿INSERT INTO [BillingInvoiceType] ([BillingInvoiceTypeId], [BillingInvoiceTypeName]) VALUES (1, 'NFS-e');
INSERT INTO [BillingInvoiceType] ([BillingInvoiceTypeId], [BillingInvoiceTypeName]) VALUES (2, 'NFC-e');
INSERT INTO [BillingInvoiceType] ([BillingInvoiceTypeId], [BillingInvoiceTypeName]) VALUES (3, 'NF-e');
INSERT INTO [BillingInvoiceType] ([BillingInvoiceTypeId], [BillingInvoiceTypeName]) VALUES (4, 'Factura');
INSERT INTO [BillingInvoiceType] ([BillingInvoiceTypeId], [BillingInvoiceTypeName]) VALUES (5, 'Nota de Crédito');
INSERT INTO [BillingInvoiceType] ([BillingInvoiceTypeId], [BillingInvoiceTypeName]) VALUES (6, 'Nota de Débito');