﻿INSERT INTO usertenant (usertenantid, userid, tenantid, isactive)
SELECT NEWID(), users.id, users.tenantid, 1
FROM users left join
usertenant on users.tenantid = usertenant.tenantid 
WHERE usertenant.tenantid is null
GO

INSERT INTO usertenant (usertenantid, userid, tenantid, isactive) VALUES (NEWID(), N'66133325-3ce6-44fb-8474-6ad0ef653ff4', N'23eb803c-726a-4c7c-b25b-2c22a5679355', 1)
GO
INSERT INTO usertenant (usertenantid, userid, tenantid, isactive) VALUES (NEWID(), N'66133325-3ce6-44fb-8474-6ad0ef653ff4', N'23eb803c-726a-4c7c-b25b-2c22a5679365', 1)
GO
INSERT INTO usertenant (usertenantid, userid, tenantid, isactive) VALUES (NEWID(), N'66133325-3ce6-44fb-8474-6ad0ef653ff4', N'23eb803c-726a-4c7c-b25b-2c22a5679375', 1)
GO