﻿declare 
@HotelId bigint,
@Hoteluuid uniqueidentifier,
@CompanyId bigint,
@ChainId bigint,
@BrandId bigint,
@HotelName nvarchar(100),
@TenantName nvarchar(100),
@Usuariouuid uniqueidentifier,
@PersonCompany uniqueidentifier,
@Companyuuid uniqueidentifier,
@BrandName nvarchar(100),
@ChainName nvarchar(100),
@Email nvarchar(200),
@MaxUh bigint,
@UsuarioEmail nvarchar(200),
@UsuarioNome nvarchar(200),
@Tenanteuuidproperty uniqueidentifier,
@Tenanteuuidbrand uniqueidentifier,
@Tenanteuuidchain uniqueidentifier,
@TenantNameproperty  nvarchar(200),
@TenantNamebrand  nvarchar(200),
@TenantNamechain  nvarchar(200),
@personid uniqueidentifier,
@cnpj nvarchar(50),
@telefone nvarchar(50),
@rua nvarchar(200),
@numero nvarchar(200),
@complemento nvarchar(200),
@bairro nvarchar(200),
@cidadeId int,
@cep nvarchar(20),
@estadoId int,
@scopoIdMarket int,
@scopoIdMarketSEC int,
@scopoIdSource int,
@DiariaCategoryId int,
@DiariaId int,
@DiferencaDiariaId int

set @HotelName = 'Hotel SAT/SP'
set @cnpj = '82.373.077/0001-71'
set @rua  = 'Avenida Paulista'
set @numero = '1000'
set @bairro  = 'Bela Vista'
set @cidadeId = '3856'
set @estadoId = '26'
set @cep  = '01310100'

set @BrandName = @HotelName + ' Bandeira'
set @ChainName = @HotelName + ' Rede'
set @TenantNameproperty = @HotelName + ' Hotel Tenant'
set @TenantNamebrand = @HotelName + ' Brand Tenant'
set @TenantNamechain = @HotelName + ' Chain Tenant'
set @Tenanteuuidproperty = newid()
set @Tenanteuuidbrand = newid()
set @Tenanteuuidchain = newid()
set @PersonCompany = newid()
set @Companyuuid = newid()
set @Hoteluuid = newid()
set @Usuariouuid = '66133325-3ce6-44fb-8474-6ad0ef653ff4'
set @personid = newid()

--CRIACAO DOS TENANTS
insert into tenant (tenantId, TenantName, IsActive)
values
(@Tenanteuuidchain,@TenantNamechain,1),
(@Tenanteuuidbrand,@TenantNamebrand,1),
(@Tenanteuuidproperty,@TenantNameproperty,1);

--CRIAÇÃO CHAIN, 
insert into Chain(Name, TenantId)
values (@ChainName, @Tenanteuuidchain);

select @ChainId = ChainId  from Chain where Name=@ChainName;

--CRIAÇÃO DA BRAND
insert into Brand(chainId,Name, IsTemporary,tenantid) 
values(@ChainId, @BrandName, 0, @Tenanteuuidbrand);

select @BrandId = BrandId  from Brand where Name=@BrandName;


--INSERÇÃO DA COMPANY
insert into Person(PersonId,PersonType, FirstName, LastName,CountrySubdivisionId)
values (@PersonCompany,'L', @HotelName, null, 1);

insert into company (companyUid, PersonId, ShortName,TradeName)
values (@Companyuuid, @PersonCompany, @HotelName, @HotelName + 'Ltda');

select @CompanyId = CompanyId from company where companyuid=@companyuuid;


--CRIAÇÃO DA PROPERTY
insert into Property(CompanyId, PropertyTypeId, BrandId, Name, PropertyUId,TenantId) 
values(@CompanyId, 1,@BrandId,@HotelName, @Hoteluuid,@Tenanteuuidproperty);

select @HotelId=PropertyId from property where PropertyUId=@Hoteluuid;

--INSERCAO PERSON DA PROPERTY
insert into Person(PersonId,PersonType, FirstName,CountrySubdivisionId)
values (@Hoteluuid,'L', @HotelName,  1);

----INSERCAO PERSON DO USUARIO
--insert into Person(PersonId,PersonType, FirstName,CountrySubdivisionId)
--values (@personid,'C', @UsuarioNome,  1);

--INSERCAO PERSON DA COMPANY
insert into Person(PersonId,PersonType, FirstName,CountrySubdivisionId)
values (@Companyuuid,'L', @HotelName,  1);

--Insert Parametros
insert into PropertyParameter(PropertyParameterId, PropertyId,ApplicationParameterId,
PropertyParameterValue, PropertyParameterMinValue, PropertyParameterMaxValue,
PropertyParameterPossibleValues, IsActive,TenantId)
select newid(), py.PropertyId,ApplicationParameterId,
ap.ParameterDefaultValue, ParameterMinValue, ParameterMaxValue,
ParameterPossibleValues, 1,py.TenantId
from ApplicationParameter ap, Property py
where  not exists(select 1 from PropertyParameter
where propertyid=py.PropertyId and ApplicationParameterId=ap.ApplicationParameterId)
and py.PropertyId = @HotelId;


PRINT '1';
--Insert Pensões
INSERT [dbo].[PropertyMealPlanType] 
([PropertyMealPlanTypeId], [PropertyMealPlanTypeCode], [PropertyMealPlanTypeName], [PropertyId], [MealPlanTypeId], [IsActive], 
[IsDeleted], [CreationTime], [CreatorUserId], [LastModificationTime], [LastModifierUserId], [DeletionTime], [DeleterUserId], [TenantId]) 
VALUES (newid(), N'MAPA', N'Meia Pensão Almoço', @HotelId, 4, 1, 0, GETDATE(), NULL, GETDATE(), NULL, NULL, NULL,@Tenanteuuidproperty),
(newid(), N'FAP', N'Pensão Completa', @HotelId, 6, 1, 0, GETDATE(), NULL, GETDATE(), NULL, NULL, NULL, @Tenanteuuidproperty),
(newid(), N'SAP', N'Sem Pensão', @HotelId, 1, 1, 0, GETDATE(), NULL, GETDATE(), NULL, NULL, NULL, @Tenanteuuidproperty),
(newid(), N'CM', N'Café da Manha', @HotelId, 2, 1, 0, GETDATE(), NULL, GETDATE(), NULL, NULL, NULL, @Tenanteuuidproperty),
(newid(), N'MAP', N'Meia Pensão', @HotelId, 3, 1, 0, GETDATE(), NULL, GETDATE(), NULL, NULL, NULL, @Tenanteuuidproperty),
(newid(), N'TI', N'Tudo Incluso', @HotelId, 7, 1, 0, GETDATE(), NULL, GETDATE(), NULL, NULL, NULL, @Tenanteuuidproperty),
(newid(), N'MAPJ', N'Meia Pensão Jantar', @HotelId, 5, 1, 0, GETDATE(), NULL, GETDATE(), NULL, NULL, NULL, @Tenanteuuidproperty)

--Insert QTD UHS
insert into propertycontract
(propertycontractid, propertyid, maxuh, isdeleted, creatoruserid)
values(newid(), @HotelId, 10, 0, @Usuariouuid)

PRINT '2';
 --Insert Status Governança
 insert into HousekeepingStatusProperty(HousekeepingStatusPropertyId,PropertyId, HousekeepingStatusId, StatusName,Color,
TenantId, IsActive)
select NEWID(), py.propertyId, hk.HousekeepingStatusId, hk.StatusName,hk.DefaultColor,py.tenantid,1
from Property py,
HousekeepingStatus hk
where not exists(select 1 from HousekeepingStatusProperty h1
where h1.PropertyId=py.PropertyId
and h1.HousekeepingStatusId=hk.HousekeepingStatusId);

PRINT '3';
--INSERT STATUS GOVERNAÇA
INSERT INTO [HousekeepingStatusProperty] ([HousekeepingStatusPropertyId], [PropertyId], [HousekeepingStatusId], [StatusName], [Color], [TenantId], [IsActive] )
VALUES (newid(), @HotelId, 1, 'Limpo', '1DC29D', @Tenanteuuidproperty, 1),
 (newid(), @HotelId, 2, 'Sujo', 'FF6969', @Tenanteuuidproperty, 1),
 (newid(), @HotelId, 3, 'Manutenção', 'A497E3', @Tenanteuuidproperty, 1),
 (newid(), @HotelId, 4, 'Inspeção', 'F8E71C', @Tenanteuuidproperty, 1),
 (newid(), @HotelId, 5, 'Arrumação', '00C5FF', @Tenanteuuidproperty, 1),
 (newid(), @HotelId, 6, 'Stand By', 'F5A623', @Tenanteuuidproperty, 1)


 --ATUALIZAR OS TENANTS COM OS IDS RESPECTIVOS
 --chain
 UPDATE Tenant SET ChainId=@ChainId where TenantId=@Tenanteuuidchain;
--brand
 UPDATE Tenant SET ChainId=@ChainId, BrandId=@BrandId where TenantId=@Tenanteuuidbrand;
 --property
 UPDATE Tenant SET PropertyId=@HotelId, ChainId=@ChainId, BrandId=@BrandId where TenantId=@Tenanteuuidproperty;

 --TIPOS DE CARTÔES
INSERT [dbo].[PlasticBrandProperty] ([PlasticBrandPropertyId], [PlasticBrandId], [PropertyId], [InstallmentsQuantity], [IsActive], [TenantId])
 VALUES (newid(), 1, @HotelId, 0, 1, @Tenanteuuidproperty),
(newid(), 2, @HotelId, 0, 1, @Tenanteuuidproperty),
(newid(), 3, @HotelId, 0, 1, @Tenanteuuidproperty),
(newid(), 4, @HotelId, 0, 1, @Tenanteuuidproperty),
(newid(), 5, @HotelId, 0, 1, @Tenanteuuidproperty),
(newid(), 6, @HotelId, 0, 1, @Tenanteuuidproperty),
(newid(), 7, @HotelId, 0, 1, @Tenanteuuidproperty)

PRINT '4';

-- INSERT USER TENANT
INSERT [dbo].[UserTenant] ([UserTenantId], [UserId], [TenantId], [IsActive], [IsDeleted], [CreationTime], [CreatorUserId], 
[LastModificationTime], [LastModifierUserId], [DeletionTime], [DeleterUserId]) VALUES 
(newid(),'66133325-3ce6-44fb-8474-6ad0ef653ff4' , @Tenanteuuidproperty, 1, 0, 
getdate(), @Usuariouuid, getdate(), NULL, NULL, NULL)

--INSERT DE PERMISSAO ADMIN
INSERT [dbo].[UserRoles] ([UserRolesId], [UserId], [RoleId], [ChainId], [PropertyId], [BrandId], [IsDeleted], [CreationTime],
 [CreatorUserId], [LastModificationTime], [LastModifierUserId], [DeletionTime], [DeleterUserId]) VALUES 
 (newid(), '66133325-3ce6-44fb-8474-6ad0ef653ff4', N'CFA00683-9308-448C-9BED-D2FF29259AD5', NULL, @HotelId, NULL, 0, getdate(), NULL, 
 getdate(), NULL, NULL, NULL)
 
PRINT '5';
 --INSERT DE RAZOES DEFAULT ADMIN
 INSERT [dbo].[Reason] ([ReasonName], [IsActive], [ReasonCategoryId], [ChainId], [HigsCode]) VALUES 
 (N'Cancelamento da Reserva', 1, 1, @ChainId, NULL),
 (N'Agendamento de Reserva', 1, 2, @ChainId, NULL),
 (N'Prazo do Evento', 1, 3, @ChainId, NULL),
 (N'Transferência de quarto', 1, 4, @ChainId, NULL),
 (N'Bloqueio do quarto', 1, 5, @ChainId, NULL),
 (N'Desconto Indevido', 1, 6, @ChainId, NULL),
 (N'Outros', 1, 7, @ChainId, NULL),
 (N'Outro', 1, 8, @ChainId, NULL),
 (N'Lançamento Indevido', 1, 9, @ChainId, NULL),
 (N'Encerramento Indevido', 1, 10, @ChainId, NULL),
 (N'Cancelamento de nota fiscal', 1, 11, @ChainId, NULL);
 
PRINT '6';
 --INSERCAO DO DOCUMENTO DA COMPANY
 insert into document (ownerid, documenttypeid, documentinformation) 
values (@Companyuuid, 2, @cnpj);

 insert into document (ownerid, documenttypeid, documentinformation) 
values (@PersonCompany, 2, @cnpj);


PRINT '9';

 --INSERCAO ENDEREÇO COMPANY
 INSERT [dbo].[Location] ([OwnerId], [LocationCategoryId], [Latitude], [Longitude], [StreetName], [StreetNumber], 
 [AdditionalAddressDetails], [Neighborhood], [CityId], [PostalCode], [CountryCode], [StateId], [CountryId]) VALUES 
 (@PersonCompany, 1, CAST(0.000000 AS Decimal(9, 6)), CAST(0.000000 AS Decimal(9, 6)), 
 @rua, @numero, @complemento, @bairro, @cidadeId, @cep, N'BR', @estadoId, 1)
 
 INSERT [dbo].[Location] ([OwnerId], [LocationCategoryId], [Latitude], [Longitude], [StreetName], [StreetNumber], 
 [AdditionalAddressDetails], [Neighborhood], [CityId], [PostalCode], [CountryCode], [StateId], [CountryId]) VALUES 
 (@Companyuuid, 1, CAST(0.000000 AS Decimal(9, 6)), CAST(0.000000 AS Decimal(9, 6)), 
 @rua, @numero, @complemento, @bairro, @cidadeId, @cep, N'BR', @estadoId, 1)
 
 INSERT [dbo].[Location] ([OwnerId], [LocationCategoryId], [Latitude], [Longitude], [StreetName], [StreetNumber], 
 [AdditionalAddressDetails], [Neighborhood], [CityId], [PostalCode], [CountryCode], [StateId], [CountryId]) VALUES 
 (@Hoteluuid, 1, CAST(0.000000 AS Decimal(9, 6)), CAST(0.000000 AS Decimal(9, 6)), 
 @rua, @numero, @complemento, @bairro, @cidadeId, @cep, N'BR', @estadoId, 1)
 
 
PRINT '1';


----SEGMENTO SECUNDARIO

INSERT INTO MARKETSEGMENT 
([Name], [IsActive],[ParentMarketSegmentId], [Code],[SegmentAbbreviation], [IsDeleted], [CreationTime], [CreatorUserId], [LastModificationTime], [LastModifierUserId], [DeletionTime], [DeleterUserId]) 
VALUES ('SEGMENTOS THEX', 1, NULL, '001', 'SEGMENTOSTHEX', 0, GETDATE(), NULL, NULL, NULL, NULL, NULL);

SET @scopoIdMarket = SCOPE_IDENTITY()

INSERT INTO [CHAINMARKETSEGMENT]
	( [ChainMarketSegmentId], [ChainId], [MarketSegmentId], [IsActive])
	VALUES (newid(), @ChainId,@scopoIdMarket,1);
	
PRINT '001';

----SEGMENTO SECUNDARIO

INSERT INTO MARKETSEGMENT 
([Name], [IsActive],[ParentMarketSegmentId], [Code],[SegmentAbbreviation], [IsDeleted], [CreationTime], [CreatorUserId], [LastModificationTime], [LastModifierUserId], [DeletionTime], [DeleterUserId]) 
VALUES ('PADRÃO', 1, @scopoIdMarket, '001-1', 'PADRÃO', 0, GETDATE(), NULL, NULL, NULL, NULL, NULL);

SET @scopoIdMarketSEC = SCOPE_IDENTITY()

INSERT INTO [CHAINMARKETSEGMENT]
	( [ChainMarketSegmentId], [ChainId], [MarketSegmentId], [IsActive], [IsDeleted], [CreationTime], [CreatorUserId], [LastModificationTime], [LastModifierUserId], [DeletionTime], [DeleterUserId])
	VALUES (newid(), @ChainId,@scopoIdMarketSEC,1, 0, GETDATE(), NULL, NULL, NULL, NULL, NULL);

	
PRINT '002';
----ORIGEM

INSERT INTO BUSINESSSOURCE 
([Description], [Code],[IsActive], [IsDeleted], [CreationTime], [CreatorUserId], [LastModificationTime], [LastModifierUserId], [DeletionTime], [DeleterUserId]) 
VALUES ('RESERVA BALCÃO','BALCAO', 1, 0, GETDATE(), NULL, NULL, NULL, NULL, NULL);

SET @scopoIdSource = SCOPE_IDENTITY()

INSERT INTO [CHAINBUSINESSSOURCE]
	( [ChainBusinessSourceId], [ChainId], [BusinessSourceId], [IsActive])
	VALUES (newid(),@ChainId,@scopoIdSource,1);

	
PRINT '003';

--GRUPOS

insert into BillingItemCategory ([CategoryName],[PropertyId],[IsActive],[StandardCategoryId],[TenantId]) values ('Restaurante',@HotelId,1,1, @Tenanteuuidproperty);
insert into BillingItemCategory ([CategoryName],[PropertyId],[IsActive],[StandardCategoryId],[TenantId]) values ('Outros',@HotelId,1,2, @Tenanteuuidproperty);
insert into BillingItemCategory ([CategoryName],[PropertyId],[IsActive],[StandardCategoryId],[TenantId]) values ('Bares',@HotelId,1,3, @Tenanteuuidproperty);
insert into BillingItemCategory ([CategoryName],[PropertyId],[IsActive],[StandardCategoryId],[TenantId]) values ('Eventos',@HotelId,1,5, @Tenanteuuidproperty);
insert into BillingItemCategory ([CategoryName],[PropertyId],[IsActive],[StandardCategoryId],[TenantId]) values ('Banquetes',@HotelId,1,6, @Tenanteuuidproperty);
insert into BillingItemCategory ([CategoryName],[PropertyId],[IsActive],[StandardCategoryId],[TenantId]) values ('Lavanderia',@HotelId,1,7, @Tenanteuuidproperty);
insert into BillingItemCategory ([CategoryName],[PropertyId],[IsActive],[StandardCategoryId],[TenantId]) values ('Telecomunicações',@HotelId,1,8, @Tenanteuuidproperty);
insert into BillingItemCategory ([CategoryName],[PropertyId],[IsActive],[StandardCategoryId],[TenantId]) values ('ISS',@HotelId,1,9, @Tenanteuuidproperty);
insert into BillingItemCategory ([CategoryName],[PropertyId],[IsActive],[StandardCategoryId],[TenantId]) values ('Taxa de Serviços',@HotelId,1,10, @Tenanteuuidproperty);
insert into BillingItemCategory ([CategoryName],[PropertyId],[IsActive],[StandardCategoryId],[TenantId]) values ('Diária',@HotelId,1,4, @Tenanteuuidproperty);


select @DiariaCategoryId = BillingItemCategoryId from BillingItemCategory where [CategoryName]='Diária' and [PropertyId] = @HotelId;

PRINT '004';

--ITENS
INSERT [dbo].[BillingItem] ([BillingItemTypeId], [BillingItemName], [BillingItemCategoryId], [Description], [PropertyId], [PaymentTypeId], [PlasticBrandPropertyId], [AcquirerId], 
[MaximumInstallmentsQuantity], [IntegrationCode], [IsActive], [TenantId]) VALUES 
--PAGAMENTO
(3, N'DINHEIRO', NULL, N'Pagamento no dinheiro', @HotelId, 8, NULL, NULL, NULL, NULL, 1, @Tenanteuuidproperty),
--SERVIÇO
(1, N'DIÁRIA', @DiariaCategoryId, NULL, @HotelId, NULL, NULL, NULL, NULL, NULL, 1, @Tenanteuuidproperty),
(1, N'DIFERENÇA DE DIÁRIA', @DiariaCategoryId, NULL, @HotelId, NULL, NULL, NULL, NULL, NULL, 1, @Tenanteuuidproperty)


select @DiariaId = [BillingItemId] from [BillingItem] where [BillingItemName]='DIÁRIA' and [PropertyId] = @HotelId;
select @DiferencaDiariaId = [BillingItemId] from [BillingItem] where [BillingItemName]='DIFERENÇA DE DIÁRIA' and [PropertyId] = @HotelId;


PRINT '005';

--PARAMETROS ATUALIZACAO
update propertyparameter set PropertyParameterValue = '14:00' where ApplicationParameterId = 1
update propertyparameter set PropertyParameterValue = '12:00' where ApplicationParameterId = 2
update propertyparameter set PropertyParameterValue = 'Check-in' where ApplicationParameterId = 3
update propertyparameter set PropertyParameterValue = '67838acb-9525-4aeb-b0a6-127c1b986c48' where ApplicationParameterId = 4   ---moeda
update propertyparameter set PropertyParameterValue = @DiariaId where ApplicationParameterId = 5 ---diária
update propertyparameter set PropertyParameterValue = '6' where ApplicationParameterId = 7 
update propertyparameter set PropertyParameterValue = '10' where ApplicationParameterId = 8
update propertyparameter set PropertyParameterValue = '12' where ApplicationParameterId = 9
update propertyparameter set PropertyParameterValue = convert(varchar, getdate(), 23) where ApplicationParameterId = 10
update propertyparameter set PropertyParameterValue = '0' where ApplicationParameterId = 11
update propertyparameter set PropertyParameterValue = 'America/Sao_Paulo' where ApplicationParameterId = 12
update propertyparameter set PropertyParameterValue = '3' where ApplicationParameterId = 13
update propertyparameter set PropertyParameterValue = @DiferencaDiariaId  where ApplicationParameterId = 14 -- diferença de diária



PRINT '006';

--TIPO DE HOSPEDE
INSERT [dbo].[PropertyGuestType] ([PropertyId], [GuestTypeName], [IsVIP]) VALUES (@HotelId, N'NORMAL', 0);
INSERT [dbo].[PropertyGuestType] ([PropertyId], [GuestTypeName], [IsVIP]) VALUES (@HotelId, N'VIP', 1);

-- TIPO DE QUARTO
SET IDENTITY_INSERT [dbo].[RoomType] ON
INSERT [dbo].[RoomType] ([RoomTypeId], [PropertyId], [Order], [Name], [Abbreviation], [AdultCapacity], [ChildCapacity],[FreeChildQuantity_1],[FreeChildQuantity_2],[FreeChildQuantity_3], [IsActive],[MaximumRate],[MinimumRate],[DistributionCode]) 
VALUES (4, @HotelId, 1, N'Standard', N'STD', 2, 1,1,1,1, 1,N'450.00',N'150.00',N'CD1')
INSERT [dbo].[RoomType] ([RoomTypeId], [PropertyId], [Order], [Name], [Abbreviation], [AdultCapacity], [ChildCapacity],[FreeChildQuantity_1],[FreeChildQuantity_2],[FreeChildQuantity_3], [IsActive],[MaximumRate],[MinimumRate],[DistributionCode]) 
VALUES (5, @HotelId, 2, N'Luxo', N'LXO', 2, 2, 1,1,1,1,N'450.00',N'150.00',N'CD2')
INSERT [dbo].[RoomType] ([RoomTypeId], [PropertyId], [Order], [Name], [Abbreviation], [AdultCapacity], [ChildCapacity],[FreeChildQuantity_1],[FreeChildQuantity_2],[FreeChildQuantity_3], [IsActive],[MaximumRate],[MinimumRate],[DistributionCode]) 
VALUES (6, @HotelId, 3, N'Presidential', N'PSD', 2, 2,1,1,1, 1,N'450.00',N'150.00',N'CD3')
SET IDENTITY_INSERT [dbo].[RoomType] OFF

-- QUARTO
SET IDENTITY_INSERT [dbo].[Room] ON
INSERT [dbo].[Room] ([RoomId], [ParentRoomId], [PropertyId], [RoomTypeId], [Building], [Wing], [Floor], [RoomNumber], [Remarks], [IsActive], [HousekeepingStatusPropertyId], [TenantId]) VALUES (13, NULL, @HotelId, 1, 'A', 'North', '1', '101', 'N', 1, '6e841a8e-87a6-4965-8a57-55c07b9bff50', @Tenanteuuidproperty)
INSERT [dbo].[Room] ([RoomId], [ParentRoomId], [PropertyId], [RoomTypeId], [Building], [Wing], [Floor], [RoomNumber], [Remarks], [IsActive], [HousekeepingStatusPropertyId], [TenantId]) VALUES (14, NULL, @HotelId, 1, 'A', 'North', '1', '102', 'N', 1, '6e841a8e-87a6-4965-8a57-55c07b9bff50', @Tenanteuuidproperty)
INSERT [dbo].[Room] ([RoomId], [ParentRoomId], [PropertyId], [RoomTypeId], [Building], [Wing], [Floor], [RoomNumber], [Remarks], [IsActive], [HousekeepingStatusPropertyId], [TenantId]) VALUES (15, NULL, @HotelId, 1, 'A', 'North', '1', '103', 'N', 1, '6e841a8e-87a6-4965-8a57-55c07b9bff50', @Tenanteuuidproperty)
INSERT [dbo].[Room] ([RoomId], [ParentRoomId], [PropertyId], [RoomTypeId], [Building], [Wing], [Floor], [RoomNumber], [Remarks], [IsActive], [HousekeepingStatusPropertyId], [TenantId]) VALUES (16, NULL, @HotelId, 1, 'A', 'North', '1', '104', 'N', 1, '6e841a8e-87a6-4965-8a57-55c07b9bff50', @Tenanteuuidproperty)
INSERT [dbo].[Room] ([RoomId], [ParentRoomId], [PropertyId], [RoomTypeId], [Building], [Wing], [Floor], [RoomNumber], [Remarks], [IsActive], [HousekeepingStatusPropertyId], [TenantId]) VALUES (17, NULL, @HotelId, 1, 'A', 'North', '1', '105', 'N', 1, '6e841a8e-87a6-4965-8a57-55c07b9bff50', @Tenanteuuidproperty)
INSERT [dbo].[Room] ([RoomId], [ParentRoomId], [PropertyId], [RoomTypeId], [Building], [Wing], [Floor], [RoomNumber], [Remarks], [IsActive], [HousekeepingStatusPropertyId], [TenantId]) VALUES (18, NULL, @HotelId, 1, 'A', 'North', '1', '106', 'N', 1, '6e841a8e-87a6-4965-8a57-55c07b9bff50', @Tenanteuuidproperty)
INSERT [dbo].[Room] ([RoomId], [ParentRoomId], [PropertyId], [RoomTypeId], [Building], [Wing], [Floor], [RoomNumber], [Remarks], [IsActive], [HousekeepingStatusPropertyId], [TenantId]) VALUES (19, NULL, @HotelId, 2, 'A', 'North', '5', '501', 'N', 1, '6e841a8e-87a6-4965-8a57-55c07b9bff50', @Tenanteuuidproperty)
INSERT [dbo].[Room] ([RoomId], [ParentRoomId], [PropertyId], [RoomTypeId], [Building], [Wing], [Floor], [RoomNumber], [Remarks], [IsActive], [HousekeepingStatusPropertyId], [TenantId]) VALUES (20, NULL, @HotelId, 2, 'A', 'North', '5', '502', 'N', 1, '6e841a8e-87a6-4965-8a57-55c07b9bff50', @Tenanteuuidproperty)
INSERT [dbo].[Room] ([RoomId], [ParentRoomId], [PropertyId], [RoomTypeId], [Building], [Wing], [Floor], [RoomNumber], [Remarks], [IsActive], [HousekeepingStatusPropertyId], [TenantId]) VALUES (21, NULL, @HotelId, 2, 'A', 'North', '5', '503', 'N', 1, '6e841a8e-87a6-4965-8a57-55c07b9bff50', @Tenanteuuidproperty)
INSERT [dbo].[Room] ([RoomId], [ParentRoomId], [PropertyId], [RoomTypeId], [Building], [Wing], [Floor], [RoomNumber], [Remarks], [IsActive], [HousekeepingStatusPropertyId], [TenantId]) VALUES (22, NULL,@HotelId, 2, 'A', 'North', '5', '504', 'N', 1, '42c17b94-2190-46a6-8cfc-3bcdd7d38019', @Tenanteuuidproperty)
INSERT [dbo].[Room] ([RoomId], [ParentRoomId], [PropertyId], [RoomTypeId], [Building], [Wing], [Floor], [RoomNumber], [Remarks], [IsActive], [HousekeepingStatusPropertyId], [TenantId]) VALUES (23, NULL, @HotelId, 3, 'A', 'North', '6', '601', 'N', 1, '62045743-1cd2-41f1-829b-fc8c0e3dca0d', @Tenanteuuidproperty)
INSERT [dbo].[Room] ([RoomId], [ParentRoomId], [PropertyId], [RoomTypeId], [Building], [Wing], [Floor], [RoomNumber], [Remarks], [IsActive], [HousekeepingStatusPropertyId], [TenantId]) VALUES (24, NULL, @HotelId, 3, 'A', 'North', '6', '602', 'N', 1, '560958d8-ab8c-4d38-8bfd-6e99cda67f05', @Tenanteuuidproperty)
SET IDENTITY_INSERT [dbo].[Room] OFF


INSERT [dbo].[IntegrationPartnerProperty]
([IntegrationPartnerPropertyId], [PropertyId], [IsActive], [IntegrationCode], [PartnerId]) 
VALUES (newid(), @HotelId, 1, 'F91745E1-E5DC-4AD8-AC3C-7670BEFF0400', 4)

declare @integrationPartnerId int
SET @integrationPartnerId =(select IntegrationPartnerId from IntegrationPartner where IntegrationPartnerName = 'ENOTAS')
UPDATE IntegrationPartnerProperty SET IntegrationPartnerId = @integrationPartnerId WHERE PartnerId = 4


INSERT [dbo].[BillingInvoiceProperty] ([BillingInvoicePropertyId], [BillingInvoicePropertySeries], [BillingInvoiceModelId], [LastNumber], [IsIntegrated], [PropertyId],[Description],[EmailAlert],[TenantId]) 
VALUES (newid(), N'4', 8, N'0', 1, @HotelId,N'SAT SP',N'luiz.queiroz@totvs.com.br', @Tenanteuuidproperty)

