﻿Declare @initialDate DateTime = convert(datetime,'2019-01-01 00:00:00', 111);
Declare @id uniqueidentifier;

WHILE(@initialDate <= convert(datetime,'2021-06-18 00:00:00', 111))
BEGIN
	SET @id = newid();

    INSERT INTO RoomTypeInventory 
	([RoomTypeInventoryId], [PropertyId], [RoomTypeId], [Total], [Balance], [BlockedQuantity], [Date], [TenantId])
	values (@id, 11, 1, 6, 6, 0, @initialDate, '23EB803C-726A-4C7C-B25B-2C22A56793D9')

	SET @id = newid();

	INSERT INTO RoomTypeInventory 
	([RoomTypeInventoryId], [PropertyId], [RoomTypeId], [Total], [Balance], [BlockedQuantity], [Date], [TenantId])
	values (@id, 11, 2, 4, 4, 0, @initialDate, '23EB803C-726A-4C7C-B25B-2C22A56793D9')

	SET @id = newid();

	INSERT INTO RoomTypeInventory 
	([RoomTypeInventoryId], [PropertyId], [RoomTypeId], [Total], [Balance], [BlockedQuantity], [Date], [TenantId])
	values (@id, 11, 3, 2, 2, 0, @initialDate, '23EB803C-726A-4C7C-B25B-2C22A56793D9')

	SET @initialDate = DATEADD(day, 1, @initialDate);
END
GO