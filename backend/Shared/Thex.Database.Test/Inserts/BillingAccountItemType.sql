﻿INSERT INTO [dbo].[BillingAccountItemType] ([BillingAccountItemTypeId] ,[BillingAccountItemTypeName]) VALUES (1 ,'Credit');
INSERT INTO [dbo].[BillingAccountItemType] ([BillingAccountItemTypeId] ,[BillingAccountItemTypeName]) VALUES (2 ,'Debit');
INSERT INTO [dbo].[BillingAccountItemType] ([BillingAccountItemTypeId] ,[BillingAccountItemTypeName]) VALUES (3 ,'Reversal');
INSERT INTO [dbo].[BillingAccountItemType] ([BillingAccountItemTypeId] ,[BillingAccountItemTypeName]) VALUES (4 ,'Discount');
INSERT INTO [dbo].[BillingAccountItemType] ([BillingAccountItemTypeId] ,[BillingAccountItemTypeName]) VALUES (5 ,'Partial');
