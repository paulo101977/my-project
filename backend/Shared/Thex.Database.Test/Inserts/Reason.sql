﻿SET IDENTITY_INSERT [dbo].[Reason] ON 
INSERT INTO [dbo].[Reason] ([ReasonId], [ReasonName], [ReasonCategoryId], [IsActive], [ChainId]) VALUES (1, N'Agência sem retorno do cliente', 1, 1, 1);
INSERT INTO [dbo].[Reason] ([ReasonId], [ReasonName], [ReasonCategoryId], [IsActive], [ChainId]) VALUES (2, N'Agência/empresa fechou através de operadora', 1, 1, 1);
INSERT INTO [dbo].[Reason] ([ReasonId], [ReasonName], [ReasonCategoryId], [IsActive], [ChainId]) VALUES (3, N'Shopping', 7, 1, 1);
INSERT INTO [dbo].[Reason] ([ReasonId], [ReasonName], [ReasonCategoryId], [IsActive], [ChainId]) VALUES (4, N'Convention/Fair', 7, 1, 1);
INSERT INTO [dbo].[Reason] ([ReasonId], [ReasonName], [ReasonCategoryId], [IsActive], [ChainId]) VALUES (5, N'Studies/Courses', 7, 1, 1);
INSERT INTO [dbo].[Reason] ([ReasonId], [ReasonName], [ReasonCategoryId], [IsActive], [ChainId]) VALUES (6, N'Leisure/Vacation', 7, 1, 1);
INSERT INTO [dbo].[Reason] ([ReasonId], [ReasonName], [ReasonCategoryId], [IsActive], [ChainId]) VALUES (7, N'Business', 7, 1, 1);
INSERT INTO [dbo].[Reason] ([ReasonId], [ReasonName], [ReasonCategoryId], [IsActive], [ChainId]) VALUES (8, N'Relatives/Friends', 7, 1, 1);
INSERT INTO [dbo].[Reason] ([ReasonId], [ReasonName], [ReasonCategoryId], [IsActive], [ChainId]) VALUES (9, N'Religion', 7, 1, 1);
INSERT INTO [dbo].[Reason] ([ReasonId], [ReasonName], [ReasonCategoryId], [IsActive], [ChainId]) VALUES (10, N'Health', 7, 1, 1);
INSERT INTO [dbo].[Reason] ([ReasonId], [ReasonName], [ReasonCategoryId], [IsActive], [ChainId]) VALUES (11, N'Other', 7, 1, 1);
INSERT INTO [dbo].[Reason] ([ReasonId], [ReasonName], [ReasonCategoryId], [IsActive], [ChainId]) VALUES (12, N'Lançamento Indevido', 9, 1, 1);
INSERT INTO [dbo].[Reason] ([ReasonId], [ReasonName], [ReasonCategoryId], [IsActive], [ChainId]) VALUES (13, N'Encerramento Indevido', 10, 1, 1);
INSERT INTO [dbo].[Reason] ([ReasonId], [ReasonName], [ReasonCategoryId], [IsActive], [ChainId]) VALUES (14, N'Desconto Indevido', 6, 1, 1);
INSERT INTO [dbo].[Reason] ([ReasonId], [ReasonName], [ReasonCategoryId], [IsActive], [ChainId]) VALUES (15, N'Cancelamento de nota fiscal', 11, 1, 1);
SET IDENTITY_INSERT [dbo].[Reason] OFF