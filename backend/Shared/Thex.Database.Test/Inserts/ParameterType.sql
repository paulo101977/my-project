﻿INSERT [dbo].[ParameterType] ([ParameterTypeId], [ParameterTypeName]) VALUES (1, N'Checkbox')
GO
INSERT [dbo].[ParameterType] ([ParameterTypeId], [ParameterTypeName]) VALUES (2, N'ComboBox')
GO
INSERT [dbo].[ParameterType] ([ParameterTypeId], [ParameterTypeName]) VALUES (3, N'Number')
GO
INSERT [dbo].[ParameterType] ([ParameterTypeId], [ParameterTypeName]) VALUES (4, N'Date')
GO
INSERT [dbo].[ParameterType] ([ParameterTypeId], [ParameterTypeName]) VALUES (5, N'Default')
GO
INSERT [dbo].[ParameterType] ([ParameterTypeId], [ParameterTypeName]) VALUES (6, N'Time')
GO