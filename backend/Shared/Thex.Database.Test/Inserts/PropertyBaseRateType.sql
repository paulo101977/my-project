﻿INSERT INTO [PropertyBaseRateType] ([PropertyBaseRateTypeId], [Name]) VALUES (1, 'Amount');
INSERT INTO [PropertyBaseRateType] ([PropertyBaseRateTypeId], [Name]) VALUES (2, 'Variation');
INSERT INTO [PropertyBaseRateType] ([PropertyBaseRateTypeId], [Name]) VALUES (3, 'Premise');
INSERT INTO [PropertyBaseRateType] ([PropertyBaseRateTypeId], [Name]) VALUES (4, 'Level');