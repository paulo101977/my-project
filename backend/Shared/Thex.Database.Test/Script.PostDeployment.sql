﻿/*
Post-Deployment Script Template							
--------------------------------------------------------------------------------------
 This file contains SQL statements that will be appended to the build script.		
 Use SQLCMD syntax to include a file in the post-deployment script.			
 Example:      :r .\myfile.sql								
 Use SQLCMD syntax to reference a variable in the post-deployment script.		
 Example:      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/

--:r .\Inserts\DatabaseLog.sql
--:r .\Trigger\DatabaseLogTrigger.sql


:r .\Inserts\GratuityType.sql
:r .\Inserts\RateType.sql
:r .\Inserts\CountrySubdivision.sql
:r .\Inserts\CountrySubdivisionTranslation.sql
:r .\Inserts\CountryLanguage.sql
:r .\Inserts\StatusCategory.sql
:r .\Inserts\Status.sql
:r .\Inserts\BedType.sql
:r .\Inserts\Chain.sql
:r .\Inserts\ContactInformationType.sql
:r .\Inserts\PersonInformationType.sql
:r .\Inserts\LocationCategory.sql
:r .\Inserts\DocumentType.sql
:r .\Inserts\CountryPersonDocumentType.sql
:r .\Inserts\Brand.sql
:r .\Inserts\PropertyType.sql
:r .\Inserts\PlasticBrand.sql
:r .\Inserts\Person.sql
:r .\Inserts\PersonGuestRegistration.sql
:r .\Inserts\Tenant.sql
:r .\Inserts\User.sql
:r .\Inserts\Guest.sql
:r .\Inserts\Company.sql
:r .\Inserts\Property.sql
:r .\Inserts\PropertyBaseRateType.sql
:r .\Inserts\PropertyCompanyClientCategory.sql
:r .\Inserts\PropertyGuestType.sql
:r .\Inserts\TransportationType.sql
:r .\Inserts\ReasonCategory.sql
:r .\Inserts\Reason.sql
:r .\Inserts\GuestRegistration.sql
:r .\Inserts\RoomType.sql
:r .\Inserts\RoomTypeBedType.sql
:r .\Inserts\RoomLayout.sql
:r .\Inserts\HousekeepingStatus.sql
:r .\Inserts\HousekeepingStatusProperty.sql
:r .\Inserts\Room.sql
:r .\Inserts\MarketSegment.sql
:r .\Inserts\BusinessSource.sql
:r .\Inserts\ChainMarketSegment.sql
:r .\Inserts\ChainBusinessSource.sql
:r .\Inserts\Occupation.sql
:r .\Inserts\CompanyClient.sql
:r .\Inserts\CompanyClientContactPerson.sql
:r .\Inserts\ContactInformation.sql
:r .\Inserts\Document.sql
:r .\Inserts\DocumentGuestRegistration.sql
:r .\Inserts\Location.sql
:r .\Inserts\LocationGuestRegistration.sql
--:r .\Inserts\CompanyClientCompleted.sql
:r .\Inserts\PaymentType.sql
--:r .\Inserts\Reservation.sql
--:r .\Inserts\ReservationItem.sql
--:r .\Inserts\GuestReservationItem.sql
--:r .\Inserts\ExecuteTestReservationScript.sql
:r .\Inserts\Currency.sql
--:r .\Inserts\ReservationBudget.sql
:r .\Inserts\GeneralOccupation.sql
:r .\Inserts\PlasticBrandProperty.sql
:r .\Inserts\MealPlanType.sql
:r .\Inserts\BillingItemType.sql
:r .\Inserts\BillingItemCategory.sql
:r .\Inserts\BillingItem.sql
:r .\Inserts\BillingTax.sql
:r .\Inserts\PropertyMealPlanType.sql
:r .\Inserts\BillingAccountItemType.sql
:r .\Inserts\BillingAccountType.sql
--:r .\Inserts\BillingAccount.sql
:r .\Inserts\PolicyType.sql
:r .\Inserts\FeatureGroup.sql
:r .\Inserts\ApplicationModule.sql
:r .\Inserts\ParameterType.sql
:r .\Inserts\ApplicationParameter.sql
:r .\Inserts\PropertyParameter.sql
:r .\Inserts\BillingInvoiceType.sql
:r .\Inserts\BillingInvoiceModel.sql
:r .\Inserts\BillingInvoiceProperty.sql
:r .\Inserts\CountrySubdivisionService.sql
:r .\Inserts\BillingInvoicePropertySupportedType.sql
:r .\Inserts\PropertyCurrencyExchange.sql
:r .\Inserts\CurrencyExchange.sql
:r .\Inserts\AgreementType.sql
--:r .\ForcedError\ForcedError.sql
:r .\Inserts\RatePlan.sql
:r .\Inserts\AuditStepType.sql
:r .\Inserts\TenantUpdate.sql
:r .\Inserts\Partner.sql
:r .\Inserts\IntegrationPartner.sql
:r .\Inserts\IntegrationPartnerProperty.sql
:r .\Inserts\UserIdentity.sql
:r .\Updates\ApplicationParameter.sql
:r .\Updates\UpdateClientContactName.sql
:r .\Updates\Tenant.sql
:r .\Inserts\UserRoles.sql
:r .\Inserts\RoomTypeInventory.sql
:r .\Inserts\UserTenant.sql
:r .\Inserts\Ncm.sql
:r .\Inserts\PropertyContract.sql
:r .\Inserts\CreatePropertySP.sql