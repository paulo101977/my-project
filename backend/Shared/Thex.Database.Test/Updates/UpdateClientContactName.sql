﻿UPDATE Person
SET FirstName =  A.NovoFirstName, LastName = A.NovoLastName, FullName = NovoFullName
FROM (
	SELECT A.*, 	
	(ltrim(rtrim(A.NovoFirstName)) + ' ' + ltrim(rtrim(isnull(A.NovoLastName,'')))) AS NovoFullName 
	FROM (
		SELECT PersonId,	
		FirstName,LastName,FullName,
		CASE charindex(' ',FirstName) WHEN 0 THEN FirstName 
		ELSE substring(FirstName,1,charindex(' ',FirstName)) END AS NovoFirstName, 
		CASE WHEN charindex(' ',FirstName) > 0 and isnull(LastName,'') = ''
		THEN RIGHT(FirstName,len(FirstName) - charindex(' ',FirstName)) 
		WHEN charindex(' ',FirstName) = 0 and  charindex(' ',LastName) = 0 THEN LastName 
		WHEN substring(FirstName,1,charindex(' ',FirstName)) = substring(LastName,1,charindex(' ',LastName))
		THEN RIGHT(LastName,len(LastName) - charindex(' ',LastName))
		WHEN charindex(' ',FirstName) > 0 
		THEN RIGHT(FirstName,len(FirstName) - charindex(' ',FirstName)) + ' ' + LastName		
		ELSE LastName
		END AS NovoLastName
		FROM Person		
		) A		
	) A
WHERE Person.PersonId = A.PersonId