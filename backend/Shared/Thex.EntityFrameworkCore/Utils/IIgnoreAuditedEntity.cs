﻿using System.Collections.Generic;

namespace Thex.EntityFrameworkCore.Utils
{
    public interface IIgnoreAuditedEntity
    {
        void SetEntityName(string entityName);
        void SetEntityNameList(List<string> entityNameList);
        bool Any(string entityName);
        bool Any(object entityNameObj);
    }
}
