﻿using System.Collections.Generic;
using System.Linq;

namespace Thex.EntityFrameworkCore.Utils
{
    public class IgnoreAuditedEntity : IIgnoreAuditedEntity
    {
        public List<string> EntityNameList { get; private set; }

        public IgnoreAuditedEntity()
        {
            EntityNameList = new List<string>();
        }

        public void SetEntityName(string entityName)
        {
            EntityNameList.Add(entityName);
        }

        public void SetEntityNameList(List<string> entityNameList)
        {
            EntityNameList.AddRange(entityNameList);
        }

        public bool Any(string entityName)
            => EntityNameList.Any(e => e.ToLower() == entityName.ToLower());

        public bool Any(object entityNameObj)
        {
            var entityName = entityNameObj.GetType().ToString().Split('.').Last();

            return Any(entityName);
        }
    }
}
