﻿using Thex.EntityFrameworkCore.Utils;
using Tnf;

// ReSharper disable once CheckNamespace
namespace Microsoft.Extensions.DependencyInjection
{
    public static class ServiceCollectionExtensions
    {
        /// <summary>
        /// Add Thex AspNetCore
        /// </summary>
        /// <param name="services">Services.</param>
        public static IServiceCollection AddThexEntityFrameworkCore(this IServiceCollection services)
        {
            Check.NotNull(services, nameof(services));

            services.AddScoped<IIgnoreAuditedEntity, IgnoreAuditedEntity>();

            return services;
        }
    }
}
