﻿using Microsoft.IdentityModel.Tokens;
using System;
using System.IO;
using System.Security.Cryptography;
using Thex.AspNetCore.Security.Interfaces;

namespace Thex.AspNetCore.Security
{
    public class SignConfiguration : ISignConfiguration
    {
        private string secPath = Path.Combine(Path.GetFullPath(Directory.GetCurrentDirectory()), "xml.sec");
        public SecurityKey Key { get; }
        public SigningCredentials SigningCredentials { get; }

        public void WriteXmlSec(string xmlSec)
        {
            if (!File.Exists(secPath))
            {
                File.WriteAllText(secPath, xmlSec);
            }
        }

        private string ReadXmlSec()
        {
            string readText = "";
            if (File.Exists(secPath))
            {
                readText = File.ReadAllText(secPath);
            }
            return readText;
        }

        public SignConfiguration()
        {
            string xmlSec = ReadXmlSec();
            var provider = new RSACryptoServiceProvider(2048);
            if (String.IsNullOrEmpty(xmlSec))
            {
                Key = new RsaSecurityKey(provider.ExportParameters(true));
                xmlSec = provider.ExportToXmlString(true);
                WriteXmlSec(xmlSec);
            }
            else
            {
                provider.ImportFromXmlString(xmlSec);
                Key = new RsaSecurityKey(provider.ExportParameters(true));
            }

            SigningCredentials = new SigningCredentials(
                Key, SecurityAlgorithms.RsaSha256Signature);
        }
    }
}
