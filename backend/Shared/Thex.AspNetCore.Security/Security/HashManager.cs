﻿using System;
using System.Security.Cryptography;
using System.Text;
using Thex.Common;

namespace Thex.AspNetCore.Security
{
    public static class HashManager
    {
        private static RNGCryptoServiceProvider _cryptoServiceProvider = null;
        private const int SALT_SIZE = 24;
        private static TripleDESCryptoServiceProvider des;
        private static MD5 hashMD5;

        public static TripleDESCryptoServiceProvider DES
        {
            get
            {
                if (des == null)
                {
                    des = new TripleDESCryptoServiceProvider();
                }
                return des;
            }
        }

        public static MD5 HashMD5
        {
            get
            {
                if (hashMD5 == null)
                {
                    hashMD5 = MD5.Create();
                }
                return hashMD5;
            }
        }

        // utilty function to convert string to byte[]        
        public static byte[] GetBytes(string str)
        {
            return Encoding.Unicode.GetBytes(str);
        }

        // utilty function to convert byte[] to string        
        public static string GetString(byte[] bytes)
        {
            return Encoding.Unicode.GetString(bytes);
        }

        static HashManager()
        {
            _cryptoServiceProvider = new RNGCryptoServiceProvider();
        }

        public static string GetSaltString()
        {
            byte[] saltBytes = new byte[SALT_SIZE];

            _cryptoServiceProvider.GetNonZeroBytes(saltBytes);
            string saltString = GetString(saltBytes);
            return saltString;
        }

        public static string GetPasswordHashAndSalt(string message)
        {
            // Use SHA256 algorithm to hash salted password
            SHA256 sha = new SHA256CryptoServiceProvider();
            byte[] dataBytes = GetBytes(message);
            byte[] resultBytes = sha.ComputeHash(dataBytes);

            return GetString(resultBytes);
        }

        public static string GeneratePasswordHash(string plainTextPassword, out string salt)
        {
            salt = GetSaltString();
            string finalString = plainTextPassword + salt;
            return GetPasswordHashAndSalt(finalString);
        }

        public static bool IsPasswordMatch(string password, string salt, string hash)
        {
            string finalString = password + salt;
            return hash == GetPasswordHashAndSalt(finalString);
        }

        public static string GenerateMD5Crypto(string text)
        {
            var ret = string.Empty;

            if (!string.IsNullOrEmpty(text))
            {
                try
                {
                    DES.Key = HashMD5.ComputeHash(Encoding.ASCII.GetBytes(AppConsts.Key));
                    DES.Mode = System.Security.Cryptography.CipherMode.ECB;
                    var transform = des.CreateEncryptor();
                    var bytes = Encoding.ASCII.GetBytes(text);
                    ret = Convert.ToBase64String(transform.TransformFinalBlock(bytes, 0, bytes.Length));
                }
                catch
                {
                    ret = string.Empty;
                }
            }

            return ret;
        }

        public static string GenerateMD5Decrypto(string text)
        {
            var ret = string.Empty;

            if (!string.IsNullOrEmpty(text))
            {
                try
                {
                    DES.Key = HashMD5.ComputeHash(Encoding.ASCII.GetBytes(AppConsts.Key));
                    DES.Mode = System.Security.Cryptography.CipherMode.ECB;
                    var transform = des.CreateDecryptor();
                    var inputBuffer = Convert.FromBase64String(text);
                    ret = Encoding.ASCII.GetString(transform.TransformFinalBlock(inputBuffer, 0, inputBuffer.Length));
                }
                catch
                {
                    ret = string.Empty;
                }
            }

            return ret;
        }
    }
}
