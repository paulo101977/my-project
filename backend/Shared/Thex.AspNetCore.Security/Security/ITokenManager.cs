﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;

namespace Thex.AspNetCore.Security.Interfaces
{
    public interface ITokenManager
    {
        ClaimsIdentity CreateClaimIdentity(Guid userId, TokenInfo tokenInfo);
        ClaimsIdentity CreateClaimIdentity(string email);
        ClaimsIdentity CreateClaimIdentity(int IntegrationPartnerId, TokenInfo tokenInfo);
        ClaimsIdentity CreateClaimIdentity(string email, string name, Guid id, TokenInfo tokenInfo);
        UserTokenDto CreateUserToken(ClaimsIdentity identity, int? expiration = null);
        IntegrationPartnerTokenDto CreateIntegrationPartnerToken(ClaimsIdentity identity);
        JwtSecurityToken VerifyToken(string token);
        string CreateUserLink(string frontUrl, string email, string name, Guid id);
        string CreateForgotPasswordLink(string frontUrl, string email);
        string CreateRoomListExternalLink(string frontUrl, string email, int expiration, Guid reservationUid, Guid? guestReservationItemUid);
    }
}