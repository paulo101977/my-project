﻿using System;
using System.Collections.Generic;
using Thex.AspNetCore.Security.Interfaces;
using Thex.Kernel.Dto;

namespace Thex.AspNetCore.Security
{
    public class TokenInfo : ITokenInfo
    {
        public Guid LoggedUserUid { get; set; }
        public string LoggedUserName { get; set; }
        public string LoggedUserEmail { get; set; }
        public Guid TenantId { get; set; }
        public int? PropertyId { get; set; }
        public Guid? PropertyUid { get; set; }
        public int? ChainId { get; set; }
        public int? BrandId { get; set; }
        public bool? IsAdmin { get; set; }
        public string PreferredLanguage { get; set; }
        public string PreferredCulture { get; set; }
        public string PropertyLanguage { get; set; }
        public string PropertyCulture { get; set; }
        public string TimeZoneName { get; set; }
        public string PropertyCountryCode { get; set; }
        public string PropertyDistrictCode { get; set; }
        public List<LoggedUserPropertyDto> PropertyList { get; set; }
    }
}
