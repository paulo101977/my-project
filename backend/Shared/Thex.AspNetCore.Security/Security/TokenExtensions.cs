﻿using Newtonsoft.Json;
using System.Linq;
using System.Security.Claims;
using Thex.Kernel;

namespace Thex.AspNetCore.Security
{
    public static class TokenExtensions
    {

        public static ClaimsIdentity AddTokenInfo(this ClaimsIdentity identity, TokenInfo tokenInfo)
        {
            identity.AddClaims(
                new[] {
                        new Claim(TokenInfoClaims.LoggedUserEmail, tokenInfo.LoggedUserEmail),
                        new Claim(TokenInfoClaims.LoggedUserName, tokenInfo.LoggedUserName),
                        new Claim(TokenInfoClaims.LoggedUserUid, tokenInfo.LoggedUserUid.ToString()),
                        new Claim(TokenInfoClaims.PropertyId, tokenInfo.PropertyId.HasValue ?  tokenInfo.PropertyId.Value.ToString() : ""),
                        new Claim(TokenInfoClaims.PropertyUid, tokenInfo.PropertyUid.HasValue ?  tokenInfo.PropertyUid.Value.ToString() : ""),
                        new Claim(TokenInfoClaims.BrandId, tokenInfo.BrandId.HasValue ? tokenInfo.BrandId.Value.ToString() : ""),
                        new Claim(TokenInfoClaims.ChainId, tokenInfo.ChainId.HasValue ? tokenInfo.ChainId.Value.ToString() : ""),
                        new Claim(TokenInfoClaims.TenantId, tokenInfo.TenantId.ToString()),
                        new Claim(TokenInfoClaims.IsAdmin, tokenInfo.IsAdmin.HasValue ? tokenInfo.IsAdmin.Value.ToString().ToLower() : "false"),
                        new Claim(TokenInfoClaims.PreferredCulture, tokenInfo.PreferredCulture != null ? tokenInfo.PreferredCulture : ""),
                        new Claim(TokenInfoClaims.PreferredLanguage, tokenInfo.PreferredLanguage != null ? tokenInfo.PreferredLanguage : ""),
                        new Claim(TokenInfoClaims.PropertyCulture, tokenInfo.PropertyCulture != null ? tokenInfo.PropertyCulture : ""),
                        new Claim(TokenInfoClaims.PropertyLanguage,tokenInfo.PropertyLanguage != null ? tokenInfo.PropertyLanguage : ""),
                        new Claim(TokenInfoClaims.TimeZoneName, tokenInfo.TimeZoneName != null ? tokenInfo.TimeZoneName : ThexSecurityConstants.DEFAULT_TIME_ZONE_NAME),
                        new Claim(TokenInfoClaims.PropertyCountryCode ,tokenInfo.PropertyCountryCode != null ? tokenInfo.PropertyCountryCode : ""),
                        new Claim(TokenInfoClaims.PropertyDistrictCode , tokenInfo.PropertyDistrictCode != null ? tokenInfo.PropertyDistrictCode : ""),
                        new Claim(TokenInfoClaims.PropertyListDto , tokenInfo.PropertyList != null && tokenInfo.PropertyList.Count > 0 ? JsonConvert.SerializeObject(tokenInfo.PropertyList) : "")
                }
            );
            return identity;
        }
    }
}
