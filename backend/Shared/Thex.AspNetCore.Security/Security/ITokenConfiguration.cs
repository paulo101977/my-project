﻿namespace Thex.AspNetCore.Security.Interfaces
{
    public interface ITokenConfiguration
    {
        string Audience { get; set; }
        string Issuer { get; set; }
        int Seconds { get; set; }
        int FinalExpiration { get; set; }
    }
}