﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Security.Claims;
using System.Security.Principal;
using System.IdentityModel.Tokens.Jwt;
using Tnf.Dto;
using Thex.Kernel;
using Thex.AspNetCore.Security.Interfaces;
using System.Collections.Generic;
using Thex.Kernel.Dto;
using Newtonsoft.Json;

namespace Thex.AspNetCore.Security
{
    public class TokenConfiguration : ITokenConfiguration
    {
        public string Audience { get; set; }
        public string Issuer { get; set; }
        public int Seconds { get; set; }
        public int FinalExpiration { get; set; }
    }

    public class UserTokenDto : BaseDto
    {
        public Guid Id { get; set; }
        public bool Authenticated { get; set; }
        public string Created { get; set; }
        public string Expiration { get; set; }
        public string NewPasswordToken { get; set; }

        public string RefreshToken { get; set; }
    }

    public class RefreshTokenData
    {
        public Guid UserId { get; set; }
        public string RefreshToken { get; set; }
        public int? PropertyId { get; set; }
        public Guid? PropertyUid { get; set; }
        public int? BrandId { get; set; }
        public int? ChainId { get; set; }
        public Guid TenantId { get; set; }
        public List<LoggedUserPropertyDto> PropertyList { get; set; }
    }

    public class IntegrationPartnerTokenDto : BaseDto
    {
        public Guid Id { get; set; }
        public bool Authenticated { get; set; }
        public string Created { get; set; }
        public string Expiration { get; set; }
        public string NewPasswordToken { get; set; }
    }

    public class TokenManager : ITokenManager
    {
        IApplicationUser _applicationUser;
        IConfiguration _configuration;
        ISignConfiguration _signConfiguration;
        ITokenConfiguration _tokenConfigurations;

        public TokenManager(IApplicationUser applicationUser, IConfiguration configuration, ISignConfiguration signConfiguration, ITokenConfiguration tokenConfiguration)
        {
            _applicationUser = applicationUser;
            _configuration = configuration;
            _signConfiguration = signConfiguration;
            _tokenConfigurations = tokenConfiguration;
        }

        //new user token
        public ClaimsIdentity CreateClaimIdentity(string email, string name, Guid id, TokenInfo tokenInfo)
        {
            ClaimsIdentity identity = new ClaimsIdentity(
                new GenericIdentity(id.ToString(), "NewUser"),
                new[] {
                        new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString("N")),
                        new Claim(TokenInfoClaims.Name, name),
                        new Claim(JwtRegisteredClaimNames.Email, email)
                }
            ).AddTokenInfo(tokenInfo);
            return identity;
        }

        //New user Claims
        public ClaimsIdentity CreateClaimIdentity(Guid id, string email, string name)
        {
            ClaimsIdentity identity = new ClaimsIdentity(
                 new GenericIdentity(id.ToString(), "NewUser"),
                 new[] {
                        new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString("N")),
                        new Claim(JwtRegisteredClaimNames.Email, email),
                        new Claim(TokenInfoClaims.PropertyId, _applicationUser.PropertyId),
                        new Claim(TokenInfoClaims.PropertyUid, _applicationUser.PropertyUid),
                        new Claim(TokenInfoClaims.TenantId, _applicationUser.TenantId.ToString()),
                 }
             );
            return identity;
        }

        // Forgot password Claims
        public ClaimsIdentity CreateClaimIdentity(string email)
        {
            string TempId = Guid.NewGuid().ToString();
            ClaimsIdentity identity = new ClaimsIdentity(
                new GenericIdentity(TempId, "TempId"),
                new[] {
                        new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString("N")),
                        new Claim(JwtRegisteredClaimNames.Email, email)
                }
            );
            return identity;
        }

        // Login Claims
        public ClaimsIdentity CreateClaimIdentity(Guid userId, TokenInfo tokenInfo)
        {
            string strUserId = userId.ToString();

            ClaimsIdentity identity = new ClaimsIdentity(
                new GenericIdentity(strUserId, "user"),
                new[] {
                        new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString("N")),
                        new Claim(JwtRegisteredClaimNames.Sub, strUserId)
                }
            ).AddTokenInfo(tokenInfo);
            return identity;
        }

        public ClaimsIdentity CreateClaimIdentity(int IntegrationPartnerId, TokenInfo tokenInfo)
        {
            string strIntegrationPartnerId = IntegrationPartnerId.ToString();

            ClaimsIdentity identity = new ClaimsIdentity(
                new GenericIdentity(strIntegrationPartnerId, "integrationPartner"),
                new[] {
                        new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString("N")),
                        new Claim(JwtRegisteredClaimNames.Sub, strIntegrationPartnerId)
                }
            ).AddTokenInfo(tokenInfo);

            return identity;
        }

        public static TokenInfo GetTokenInfo(IApplicationUser applicationUser)
        {
            TokenInfo tokenInfo = new TokenInfo();

            tokenInfo.LoggedUserName = applicationUser.UserName;
            tokenInfo.LoggedUserUid = applicationUser.UserUid;
            tokenInfo.LoggedUserEmail = applicationUser.UserEmail;
            tokenInfo.TenantId = applicationUser.TenantId;

            int propertyId = 0;
            Int32.TryParse(applicationUser.PropertyId, out propertyId);
            tokenInfo.PropertyId = propertyId;
            tokenInfo.PropertyUid = Guid.Parse(applicationUser.PropertyUid);

            int chainId = 0;
            Int32.TryParse(applicationUser.ChainId, out chainId);
            tokenInfo.ChainId = chainId;

            tokenInfo.IsAdmin = applicationUser.IsAdmin;
            tokenInfo.PreferredCulture = applicationUser.PreferredCulture;
            tokenInfo.PreferredLanguage = applicationUser.PreferredLanguage;
            tokenInfo.PropertyCulture = applicationUser.PropertyCulture;
            tokenInfo.PropertyLanguage = applicationUser.PropertyLanguage;
            tokenInfo.TimeZoneName = applicationUser.TimeZoneName;
            tokenInfo.PropertyCountryCode = applicationUser.PropertyCountryCode;
            tokenInfo.PropertyDistrictCode = applicationUser.PropertyDistrictCode;
            tokenInfo.PropertyList = applicationUser.PropertyList;

            return tokenInfo;
        }

        public string CreateUserLink(string frontUrl, string email, string name, Guid id)
        {
            var expirationMonth = 2592000;
            ClaimsIdentity identity = CreateClaimIdentity(id, email, name);
            UserTokenDto usertoken = CreateUserToken(identity, expirationMonth);
            Uri link;
            Uri.TryCreate(frontUrl + "/auth/signup;token=" + usertoken.NewPasswordToken, UriKind.Absolute, out link);

            return link.AbsoluteUri;
        }

        public string CreateForgotPasswordLink(string frontUrl, string email)
        {
            var expirationDay = 86400;
            ClaimsIdentity identity = CreateClaimIdentity(email);
            UserTokenDto usertoken = CreateUserToken(identity, expirationDay);
            Uri link;
            Uri.TryCreate(frontUrl + "/auth/reset-password;token=" + usertoken.NewPasswordToken, UriKind.Absolute, out link);

            return link.AbsoluteUri;
        }

        public UserTokenDto CreateUserToken(ClaimsIdentity identity, int? expiration = null)
        {
            if (identity == null)
            {
                return null;
            }

            var dataCriacao = DateTime.UtcNow;
            var dataExpiracao = dataCriacao + TimeSpan.FromSeconds(expiration ?? _tokenConfigurations.Seconds);

            var handler = new JwtSecurityTokenHandler();
            var securityToken = handler.CreateToken(new SecurityTokenDescriptor
            {
                Issuer = _tokenConfigurations.Issuer,
                Audience = _tokenConfigurations.Audience,
                SigningCredentials = _signConfiguration.SigningCredentials,
                Subject = identity,
                NotBefore = dataCriacao,
                Expires = dataExpiracao
            });
            var token = handler.WriteToken(securityToken);

            return new UserTokenDto
            {
                Authenticated = true,
                Created = dataCriacao.ToString("yyyy-MM-dd HH:mm:ss"),
                Expiration = dataExpiracao.ToString("yyyy-MM-dd HH:mm:ss"),
                NewPasswordToken = token,
                RefreshToken = Guid.NewGuid().ToString().Replace("-", string.Empty),
                Id = Guid.NewGuid()
            };
        }

        public IntegrationPartnerTokenDto CreateIntegrationPartnerToken(ClaimsIdentity identity)
        {
            if (identity == null)
            {
                return null;
            }

            DateTime dataCriacao = DateTime.UtcNow;
            DateTime dataExpiracao = dataCriacao +
                TimeSpan.FromSeconds(_tokenConfigurations.Seconds);

            var handler = new JwtSecurityTokenHandler();
            var securityToken = handler.CreateToken(new SecurityTokenDescriptor
            {
                Issuer = _tokenConfigurations.Issuer,
                Audience = _tokenConfigurations.Audience,
                SigningCredentials = _signConfiguration.SigningCredentials,
                Subject = identity,
                NotBefore = dataCriacao,
                Expires = dataExpiracao
            });
            var token = handler.WriteToken(securityToken);

            return new IntegrationPartnerTokenDto
            {
                Authenticated = true,
                Created = dataCriacao.ToString("yyyy-MM-dd HH:mm:ss"),
                Expiration = dataExpiracao.ToString("yyyy-MM-dd HH:mm:ss"),
                NewPasswordToken = token,
                Id = Guid.NewGuid()
            };
        }

        public JwtSecurityToken VerifyToken(string token)
        {
            var validationParameters = new TokenValidationParameters()
            {
                IssuerSigningKey = _signConfiguration.Key,
                ValidAudience = _tokenConfigurations.Audience,
                ValidIssuer = _tokenConfigurations.Issuer,
                ValidateLifetime = true,
                ValidateAudience = true,
                ValidateIssuer = true,
                ValidateIssuerSigningKey = true
            };

            var handler = new JwtSecurityTokenHandler();
            SecurityToken validatedToken = null;
            try
            {
                handler.ValidateToken(token, validationParameters, out validatedToken);
                return (validatedToken as JwtSecurityToken);
            }
            catch (SecurityTokenException)
            {
                return null;
            }
            catch
            {
                return null;
            }
        }


        public string CreateRoomListExternalLink(string frontUrl, string email, int expiration, Guid reservationUid, Guid? guestReservationItemUid)
        {
            var identity = CreateClaimIdentity(email, reservationUid, guestReservationItemUid);
            var token = CreateRoomListToken(identity, expiration);

            Uri link;
            Uri.TryCreate($"{frontUrl}/roomlist?token={token}", UriKind.Absolute, out link);

            return link.AbsoluteUri;
        }

        private string CreateRoomListToken(ClaimsIdentity identity, int expiration)
        {
            if (identity == null) return null;

            var creationDate = DateTime.UtcNow;
            var expirationDate = creationDate + TimeSpan.FromSeconds(expiration);

            var handler = new JwtSecurityTokenHandler();
            var securityToken = handler.CreateToken(new SecurityTokenDescriptor
            {
                Issuer = _tokenConfigurations.Issuer,
                Audience = _tokenConfigurations.Audience,
                SigningCredentials = _signConfiguration.SigningCredentials,
                Subject = identity,
                NotBefore = creationDate,
                Expires = expirationDate
            });
            return handler.WriteToken(securityToken);
        }

        private ClaimsIdentity CreateClaimIdentity(string email, Guid reservationUid, Guid? guestReservationItemUid)
        {
            ClaimsIdentity identity = new ClaimsIdentity(
                new GenericIdentity(Guid.Empty.ToString(), "ExternalUser"),
                new[] {
                        new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString("N")),
                        new Claim(JwtRegisteredClaimNames.Sub, Guid.NewGuid().ToString("N")),
                        new Claim(TokenInfoClaims.LoggedUserEmail, email),
                        new Claim(TokenInfoClaims.PropertyId, _applicationUser.PropertyId),
                        new Claim(TokenInfoClaims.TenantId, _applicationUser.TenantId.ToString()),
                        new Claim(TokenInfoClaims.PropertyLanguage, _applicationUser.PropertyLanguage.ToString()),
                        new Claim(TokenInfoClaims.LoggedUserUid, Guid.Empty.ToString()),
                        new Claim(TokenInfoClaims.ReservationUid, reservationUid.ToString()),
                        new Claim(TokenInfoClaims.GuestReservationItemUid, guestReservationItemUid.ToString()),
                        new Claim(TokenInfoClaims.IsExternalAccess, true.ToString().ToLower()),
                        new Claim(TokenInfoClaims.PreferredLanguage, _applicationUser.PreferredLanguage.ToString()),

                        
                }
            );
            return identity;
        }
    }
}