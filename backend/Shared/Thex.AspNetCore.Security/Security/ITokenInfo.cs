﻿using System;
using System.Collections.Generic;
using Thex.Kernel.Dto;

namespace Thex.AspNetCore.Security.Interfaces
{
    public interface ITokenInfo
    {
        string LoggedUserEmail { get; set; }
        string LoggedUserName { get; set; }
        Guid LoggedUserUid { get; set; }
        int? PropertyId { get; set; }
        Guid? PropertyUid { get; set; }
        int? ChainId { get; set; }
        Guid TenantId { get; set; }
        int? BrandId { get; set; }
        bool? IsAdmin { get; set; }
        string PreferredLanguage { get; set; }
        string PreferredCulture { get; set; }
        string PropertyLanguage { get; set; }
        string PropertyCulture { get; set; }
        string TimeZoneName { get; set; }
        string PropertyCountryCode { get; set; }
        string PropertyDistrictCode { get; set; }
        List<LoggedUserPropertyDto> PropertyList { get; set; }
    }
}