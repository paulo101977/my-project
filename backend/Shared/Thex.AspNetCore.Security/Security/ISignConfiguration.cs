﻿using Microsoft.IdentityModel.Tokens;

namespace Thex.AspNetCore.Security.Interfaces
{
    public interface ISignConfiguration
    {
        SecurityKey Key { get; }
        SigningCredentials SigningCredentials { get; }
    }
}