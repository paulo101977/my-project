﻿using System;
using System.Threading.Tasks;

namespace Thex.AspNetCore.Security.Interfaces
{
    /// <summary>
    /// This class is used to permissions for users.
    /// </summary>
    public interface IThexPermissionChecker
    {
        /// <summary>
        /// Checks if a user is granted for a permission.
        /// </summary>
        /// <param name="userId">User id to check</param>
        /// <param name="permissions">Permissions to check</param>
        Task<bool> IsGrantedAsync(Guid userId, bool externalAccess, params string[] permissions);
    }
}

