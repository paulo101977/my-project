﻿namespace Thex.AspNetCore.Security
{
    public class ThexSecurityConsts
    {
        public const string REFRESH_TOKEN_CACHE_CONNECTION_STRING = "ConnectionStrings:Redis:Token";
        public const string PERMISSIONS_CACHE_CONNECTION_STRING = "ConnectionStrings:Redis:Permissions";

        public const string REFRESH_TOKEN_CACHE_INSTANCE_NAME = "RefreshToken";
        public const string PERMISSIONS_CACHE_INSTANCE_NAME = "Permissions";
    }
}
