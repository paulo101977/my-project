﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Tnf.Notifications;
using Tnf.Runtime.Security;
using Thex.Kernel;
using Tnf;
using Thex.AspNetCore.Security.Interfaces;

namespace Thex.AspNetCore.Security
{
    public class ThexAuthorize : IThexAuthorize
    {
        private IEnumerable<ThexAuthorizeAttribute> _authorizeAttributes { get; set; } = new List<ThexAuthorizeAttribute>();

        private readonly ILogger<ThexAuthorize> _logger;
        private readonly IThexPermissionChecker _permissionChecker;
        private readonly INotificationHandler _notification;

        public ThexAuthorize(
            ILogger<ThexAuthorize> logger,
            IThexPermissionChecker permissionChecker,
            INotificationHandler notification)
        {
            _logger = logger;
            _permissionChecker = permissionChecker;
            _notification = notification;
        }

        public bool RequiresAuthorization(MethodInfo methodInfo)
        {
            _logger.LogInformation(ThexLoggingEventsConstants.Authorization, ThexSecurityNames.CheckingForAuthorization, methodInfo);

            if (!GetAttributes<AllowAnonymousAttribute>(methodInfo).IsNullOrEmpty())
            {
                _logger.LogInformation(ThexLoggingEventsConstants.Authorization, ThexSecurityNames.NoRequiredAuthorization, methodInfo);
                return false;
            }

            _authorizeAttributes = GetAttributes<ThexAuthorizeAttribute>(methodInfo);

            if (_authorizeAttributes.IsNullOrEmpty())
            {
                _logger.LogInformation(ThexLoggingEventsConstants.Authorization, ThexSecurityNames.NoRequiredAuthorization, methodInfo);
                return false;
            }

            return true;
        }

        public async Task<bool> AuthorizeAsync(MethodInfo methodInfo, Guid userId, Guid tenantId, bool externalAccess)
        {
            if (!externalAccess && userId == Guid.Empty)
            {
                _logger.LogInformation(ThexLoggingEventsConstants.Authorization, ThexSecurityNames.NoUserFoundToAuthorize, methodInfo);
                return false;
            }

            if (_authorizeAttributes.IsNullOrEmpty())
                _authorizeAttributes = GetAttributes<ThexAuthorizeAttribute>(methodInfo);

            var allPermissions = _authorizeAttributes.SelectMany(s => s.Permissions).Distinct().ToArray();

            if (allPermissions.Length <= 0)
            {
                _logger.LogInformation(ThexLoggingEventsConstants.Authorization, ThexSecurityNames.UserIsAuthorized, userId, tenantId);
                return true;
            }

            var permissionsMessage = $"{string.Join(", ", allPermissions)}";

            _logger.LogInformation(ThexLoggingEventsConstants.Authorization, ThexSecurityNames.CheckingPermissionsForUser, permissionsMessage, userId, tenantId);

            var authorized = await AuthorizeAsync(allPermissions, userId, externalAccess);

            if (authorized)
                _logger.LogInformation(ThexLoggingEventsConstants.Authorization, ThexSecurityNames.UserIsAuthorizedForPermissions, userId, tenantId, permissionsMessage);
            else
                _logger.LogInformation(ThexLoggingEventsConstants.Authorization, ThexSecurityNames.UserIsNotAuthorizedForPermissions, userId, tenantId, permissionsMessage);

            return authorized;
        }

        private IEnumerable<TAttribute> GetAttributes<TAttribute>(MethodInfo methodInfo)
            where TAttribute : Attribute
            => methodInfo.GetAttributesOfMemberAndDeclaringType<TAttribute>();

        private async Task<bool> AuthorizeAsync(string[] permissionNames, Guid userId, bool externalAccess)
        {
            if (await _permissionChecker.IsGrantedAsync(userId, externalAccess, permissionNames))
                return true;

            var error = TnfController.Error.AspNetCoreSecurityOnAllOfThesePermissionsMustBeGrantedError;

            _notification
                .DefaultBuilder
                .FromErrorEnum(error)
                .WithMessage(TnfConsts.LocalizationSourceName, error)
                .WithMessageFormat(permissionNames.JoinAsString(", "))
                .Raise();

            return false;
        }
    }
}

