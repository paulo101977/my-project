﻿using Microsoft.AspNetCore.Authorization;
using System;

// ReSharper disable once CheckNamespace
namespace Microsoft.AspNetCore.Mvc
{
    /// <summary>
    /// This attribute is used on a method of an <see cref="ThexController"/>
    /// to make that method usable only by authorized users.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = false)]
    public class ThexAuthorizeAttribute : AuthorizeAttribute
    {
        /// <inheritdoc/>
        public string[] Permissions { get; set; }

        /// <summary>
        /// Creates a new instance of <see cref="ThexAuthorizeAttribute"/> class.
        /// </summary>
        /// <param name="permissions">A list of permissions to authorize</param>
        public ThexAuthorizeAttribute(params string[] permissions)
        {
            Permissions = permissions;
        }
    }
}
