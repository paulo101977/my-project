﻿using Microsoft.AspNetCore.Builder;

namespace Microsoft.AspNetCore.Builder
{
    public static class ApplicationBuilderExtensions
    {
        public static void UseThexAspNetCoreSecurity(this IApplicationBuilder app)
        {
            app.UseAuthentication();
        }
    }
}
