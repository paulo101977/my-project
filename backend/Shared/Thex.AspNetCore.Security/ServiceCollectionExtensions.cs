﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using System;
using Thex.AspNetCore.Security;
using Thex.AspNetCore.Security.Interfaces;
using Thex.AspNetCore.Security.SecurityCache;
using Thex.AspNetCore.Security.SecurityCache.Repository;
using Thex.Kernel;
using Tnf;

namespace Microsoft.Extensions.DependencyInjection
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddThexAspNetCoreSecurity(this IServiceCollection services, Action<ThexSecurityConfiguration> configureOptions)
        {
            Check.NotNull(services, nameof(services));

            var options = new ThexSecurityConfiguration();

            configureOptions(options);

            services.AddThexAspNetCoreSecurity(options);

            return services;
        }

        public static IServiceCollection AddThexAspNetCoreSecurity(this IServiceCollection services, ThexSecurityConfiguration configuration)
        {
            Check.NotNull(services, nameof(services));
            Check.NotNull(configuration, nameof(configuration));
            var signConfiguration = new SignConfiguration();

            // Depêndencias
            services.AddThexAspNetCore();
            services.AddSingleton<ITokenConfiguration>(e => configuration.TokenConfiguration); 
            services.AddSingleton<ISignConfiguration>(e => signConfiguration);
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddScoped<IApplicationUser, ApplicationUser>();
            services.AddScoped<ITokenManager, TokenManager>();

            //Configura o Jwt Bearer Token
            services.AddJwtBearer(configuration.TokenConfiguration, signConfiguration);

            // Utilizado sempre que for verificar uma permissão de acesso
            services.AddSingleton<IThexPermissionChecker, ThexPermissionChecker>((provider) =>
            {
                var logger = provider.GetService<ILogger<ThexPermissionChecker>>();
                var httpContextAccessor = provider.GetService<IHttpContextAccessor>();

                var permissionChecker = new ThexPermissionChecker(logger, httpContextAccessor, configuration.SuperAdminEndpoint);
                return permissionChecker;
            });

            services.ReplaceTransient<IThexAuthorize, ThexAuthorize>();

            return services;
        }

        private static IServiceCollection AddJwtBearer(this IServiceCollection services, TokenConfiguration tokenConfiguration, SignConfiguration signConfiguration)
        {
            services.AddAuthentication(authOptions =>
            {
                authOptions.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
                authOptions.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(bearerOptions =>
            {
                bearerOptions.TokenValidationParameters = new TokenValidationParameters
                {
                    IssuerSigningKey = signConfiguration.Key,
                    ValidAudience = tokenConfiguration.Audience,
                    ValidIssuer = tokenConfiguration.Issuer,

                    // Valida a assinatura de um token recebido
                    ValidateIssuerSigningKey = true,

                    // Verifica se um token recebido ainda é válido
                    ValidateLifetime = true,

                    // Tempo de tolerância para a expiração de um token (utilizado
                    // caso haja problemas de sincronismo de horário entre diferentes
                    // computadores envolvidos no processo de comunicação)
                    ClockSkew = TimeSpan.Zero
                };
                #region Bearer Events
                bearerOptions.Events = new JwtBearerEvents
                {
                    OnTokenValidated = context =>
                    {
                        // Add the access_token as a claim, as we may actually need it
                        var accessToken = context.SecurityToken as System.IdentityModel.Tokens.Jwt.JwtSecurityToken;
                        if (accessToken != null)
                        {
                            // Todo : Get User and Tenant
                            //ITokenInfo tokenInfo = ExtractTokenInfo(accessToken);
                        }
                        return System.Threading.Tasks.Task.CompletedTask;
                    }
                };
                #endregion
                bearerOptions.SaveToken = true;
            });

            return services;
        }

        public static IServiceCollection AddRefreshTokenCacheConfig(this IServiceCollection services, IConfiguration configuration)
        {
            services.Configure<RefreshTokenCacheOptions>(config =>
            {
                config.Configuration = configuration[ThexSecurityConsts.REFRESH_TOKEN_CACHE_CONNECTION_STRING];
                config.InstanceName = ThexSecurityConsts.REFRESH_TOKEN_CACHE_INSTANCE_NAME;
            });

            services.Add(ServiceDescriptor.Singleton<IDistributedRefreshTokenCache, RefreshTokenCache>());

            services.AddTransient<IRefreshTokenCacheRepository, RefreshTokenCacheRepository>();
            
            return services;
        }

        public static IServiceCollection AddPermissionsCacheConfig(this IServiceCollection services, IConfiguration configuration)
        {
            services.Configure<PermissionsCacheOptions>(config =>
            {
                config.Configuration = configuration[ThexSecurityConsts.PERMISSIONS_CACHE_CONNECTION_STRING];
                config.InstanceName = ThexSecurityConsts.PERMISSIONS_CACHE_INSTANCE_NAME;
            });

            services.Add(ServiceDescriptor.Singleton<IDistributedPermissionsCache, PermissionsCache>());

            return services;
        }
    }
}
