﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Thex.AspNetCore.Security.Interfaces;
using Thex.Kernel;
using Tnf;
using Tnf.Runtime.Security;

namespace Thex.AspNetCore.Security
{
    internal class ThexPermissionChecker : IThexPermissionChecker, IDisposable
    {
        private readonly IHttpContextAccessor _httpContextAccessor;

        protected HttpClient Client;
        protected ILogger Logger;
        protected HttpMessageHandler Handler;
        protected string SuperAdminEndpoint;
        protected int retry;

        public ThexPermissionChecker(ILogger logger, IHttpContextAccessor httpContextAccessor, string superAdminEndpoint)
        {
            Logger = Check.NotNull(logger, nameof(logger));
            SuperAdminEndpoint = superAdminEndpoint;
            _httpContextAccessor = httpContextAccessor;
        }

        public Task<bool> IsGrantedAsync(Guid userId, bool externalAccess, params string[] permissions)
        {
            Check.NotNullOrEmpty(permissions, nameof(permissions));

            var url = externalAccess ? $"{SuperAdminEndpoint}/api/permission/externalAccess" : $"{SuperAdminEndpoint}/api/permission/{userId}";

            retry = 1;

            return SendRequest<bool>(url, HttpVerbs.Post, permissions);
        }
        
        protected async Task<T> SendRequest<T>(string url, HttpVerbs verb, object value = null)
        {
            Handler = new HttpClientHandler();
            Client = new HttpClient(Handler);
            
            SetBearerToken();
            
            HttpResponseMessage response = null;

            switch (verb)
            {
                case HttpVerbs.Get:
                    Logger.LogInformation(ThexLoggingEventsConstants.Authorization, $"[Thex Auth Client] - GET -> {url}");
                    response = await Client.GetAsync(url).ForAwait();
                    break;

                case HttpVerbs.Post:
                    var json = JsonConvert.SerializeObject(value);
                    var httpContent = new StringContent(json, Encoding.UTF8, "application/json");

                    Logger.LogInformation(ThexLoggingEventsConstants.Authorization, $"[Thex Auth Client] - POST -> {url}");

                    response = await Client.PostAsync(url, httpContent).ForAwait();
                    break;
            }

            var contentResponse = string.Empty;

            if (response?.Content != null)
                contentResponse = await response.Content.ReadAsStringAsync().ForAwait();

            switch (response?.StatusCode)
            {
                case HttpStatusCode.OK:
                    if (!contentResponse.IsNullOrWhiteSpace())
                    {
                        try
                        {
                            return JsonConvert.DeserializeObject<T>(contentResponse);
                        }
                        catch (Exception)
                        {
                            if(retry < 3)
                            {
                                retry++;
                                return await SendRequest<T>(url, verb, value);
                            }
                            else
                            {
                                Logger.LogError(ThexLoggingEventsConstants.Authorization, ThexSecurityNames.InvalidUserApiCommunication);

                                if (!contentResponse.IsNullOrWhiteSpace())
                                    Logger.LogError(ThexLoggingEventsConstants.Authorization, contentResponse);

                                throw new TnfException(ThexSecurityNames.InvalidUserApiCommunication);
                            }
                        }
                    }

                    return default(T);
                case HttpStatusCode.Unauthorized:
                    {
                        var errorMessage = string.Format(ThexSecurityNames.NoAuthorizedAccess, url, "");

                        Logger.LogError(ThexLoggingEventsConstants.Authorization, errorMessage);

                        if (!contentResponse.IsNullOrWhiteSpace())
                            Logger.LogError(ThexLoggingEventsConstants.Authorization, contentResponse);

                        throw new TnfException(errorMessage);
                    }
                case HttpStatusCode.NotFound:
                    {
                        var errorMessage = string.Format(ThexSecurityNames.ResourceNotFound, url);

                        Logger.LogError(ThexLoggingEventsConstants.Authorization, errorMessage);

                        throw new TnfException(errorMessage);
                    }
                default:
                    {
                        Logger.LogError(ThexLoggingEventsConstants.Authorization, ThexSecurityNames.UserApiEstablishingConnectionError);

                        if (!contentResponse.IsNullOrWhiteSpace())
                            Logger.LogError(ThexLoggingEventsConstants.Authorization, contentResponse);

                        throw new TnfException(ThexSecurityNames.UserApiEstablishingConnectionError);
                    }
            }
        }

        private void SetBearerToken()
        {
            if (_httpContextAccessor != null && _httpContextAccessor.HttpContext != null)
            {
                var accessToken = _httpContextAccessor.HttpContext.GetBearerToken();

                if (!string.IsNullOrWhiteSpace(accessToken))
                {
                    Logger.LogInformation(LoggingEvents.Rac, ThexSecurityNames.AccessTokenFoundInAuthorizationHeader);
                    Client.SetBearerToken(accessToken);
                    return;
                }

                Logger.LogInformation(LoggingEvents.Rac, ThexSecurityNames.AccessTokenNotFoundInAuthorizationHeader);
            }
        }

        #region IDisposable Support

        private bool disposedValue = false;

        protected virtual void Dispose(bool disposing)
        {
            if (disposedValue)
                return;

            if (disposing)
            {
                // dispose managed state (managed objects).
                Client.Dispose();
                Client = null;
            }

            // free unmanaged resources (unmanaged objects) and override a finalizer below.
            // set large fields to null.

            disposedValue = true;
        }

        ~ThexPermissionChecker() => Dispose(false);

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        #endregion

    }
}