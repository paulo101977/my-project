﻿namespace Thex.AspNetCore.Security
{
    public class ThexSecurityConfiguration
    {
        public TokenConfiguration TokenConfiguration { get; set; }

        public string FrontEndpoint { get; set; }
        public string SuperAdminEndpoint { get; set; }
    }
}
