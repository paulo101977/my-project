﻿using Microsoft.Extensions.Caching.Redis;
using Microsoft.Extensions.Options;

namespace Thex.AspNetCore.Security.SecurityCache
{
    public class RefreshTokenCache : RedisCache, IDistributedRefreshTokenCache
    {
        public RefreshTokenCache(IOptions<RefreshTokenCacheOptions> optionsAccessor) : base(optionsAccessor)
        {
        }
    }

    public class PermissionsCache : RedisCache, IDistributedPermissionsCache
    {
        public PermissionsCache(IOptions<PermissionsCacheOptions> optionsAccessor) : base(optionsAccessor)
        {
        }
    }
}
