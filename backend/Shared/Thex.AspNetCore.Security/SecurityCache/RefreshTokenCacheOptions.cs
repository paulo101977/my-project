﻿using Microsoft.Extensions.Caching.Redis;

namespace Thex.AspNetCore.Security.SecurityCache
{
    public class RefreshTokenCacheOptions : RedisCacheOptions
    {
    }

    public class PermissionsCacheOptions : RedisCacheOptions
    {
    }
}
