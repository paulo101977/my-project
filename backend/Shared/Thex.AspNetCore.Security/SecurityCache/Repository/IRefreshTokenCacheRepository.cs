﻿namespace Thex.AspNetCore.Security.SecurityCache.Repository
{
    public interface IRefreshTokenCacheRepository
    {
        RefreshTokenData GetById(string key);
        void RemoveById(string key);
        void Set(RefreshTokenData token, int expiration);
    }
}
