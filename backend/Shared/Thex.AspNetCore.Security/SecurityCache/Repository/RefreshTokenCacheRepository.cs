﻿using Microsoft.Extensions.Caching.Distributed;
using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace Thex.AspNetCore.Security.SecurityCache.Repository
{
    public class RefreshTokenCacheRepository : IRefreshTokenCacheRepository
    {
        private readonly IDistributedCache _refreshTokenCache;

        public RefreshTokenCacheRepository(IDistributedRefreshTokenCache refreshTokenCache)
        {
            _refreshTokenCache = refreshTokenCache;
        }

        public RefreshTokenData GetById(string key)
        {
            RefreshTokenData result = null;
            string token = _refreshTokenCache.GetString(key);

            if (!string.IsNullOrWhiteSpace(token))
                result = JsonConvert.DeserializeObject<RefreshTokenData>(token);

            return result;
        }

        public void RemoveById(string key)
            => _refreshTokenCache.Remove(key);

        public void Set(RefreshTokenData token, int expiration)
        {
            var options = new DistributedCacheEntryOptions();

            options.SetAbsoluteExpiration(TimeSpan.FromSeconds(expiration));

            _refreshTokenCache.SetString(token.RefreshToken, JsonConvert.SerializeObject(token), options);
        }
    }
}
