﻿using Microsoft.Extensions.Caching.Distributed;

namespace Thex.AspNetCore.Security.SecurityCache
{
    public interface IDistributedRefreshTokenCache : IDistributedCache
    {
    }

    public interface IDistributedPermissionsCache : IDistributedCache
    {
    }
}
