﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyConfiguration("Thex.Preference.Domain")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("Thex Preference")]
[assembly: AssemblyTrademark("")]

// Setting ComVisible to false makes the types in this assembly not visible
// to COM components.  If you need to access a type in this assembly from
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

[assembly: InternalsVisibleTo("Thex.Preference.Infra")]
[assembly: InternalsVisibleTo("Thex.Preference.Web.Tests")]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("92579a7d-6474-405e-acb7-a0787e5533f5")]
