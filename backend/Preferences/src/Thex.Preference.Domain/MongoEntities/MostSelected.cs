﻿using System;
using System.Collections.Generic;
using Thex.Common.Enumerations;
using Thex.Preference.Domain.Entities;
using Tnf.MongoDb.Entities;

namespace Thex.Preference.Domain.MongoEntities
{
    public partial class MostSelected : BaseEntity, IMongoDbEntity<string>
    {
        public MostSelected()
        {
            this.Details = new List<MostSelectedDetails>();
        }

        public MostSelectedTypeEnum Type { get; set; }

        public Guid UserId { get; set; }

        public IList<MostSelectedDetails> Details { get; set; }
    }

    public class MostSelectedDetails
    {
        public MostSelectedDetails()
        {
            this.Code = string.Empty;
            this.Description = string.Empty;
            this.Quantity = 0m;
            this.GroupDescription = string.Empty;
            this.ExternalId = string.Empty;
            this.BillingItemId = string.Empty;
            this.Type = 0;
        }

        public string GroupDescription { get; set; }
        public string Description { get; set; }
        public string Code { get; set; }
        public string ExternalId { get; set; }
        public string BillingItemId { get; set; }
        public decimal Quantity { get; set; }
        public int Type { get; set; }
    }
}
