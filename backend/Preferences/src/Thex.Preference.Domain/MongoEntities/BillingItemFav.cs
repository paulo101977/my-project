﻿using System;
using System.Collections.Generic;
using System.Text;
using Thex.Preference.Domain.Entities;
using Tnf.MongoDb.Entities;

namespace Thex.Preference.Domain.MongoEntities
{
    public class BillingItemFav : BaseEntity, IMongoDbEntity<string>
    {
        public BillingItemFav()
        {
            this.Details = new List<BillingItemFavDetails>();
        }

        public PreferenceType Type { get; set; }

        public Guid UserId { get; set; }

        public IList<BillingItemFavDetails> Details { get; set; }
    }

    public class BillingItemFavDetails
    {
        public int BillingItemId { get; set; }
    }
}
