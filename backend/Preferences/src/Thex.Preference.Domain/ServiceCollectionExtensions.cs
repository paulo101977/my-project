﻿using Microsoft.Extensions.DependencyInjection;
using Thex.Common;

namespace Thex.Preference.Domain
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddDomainDependency(this IServiceCollection services)
        {
            // Adiciona as dependencias para utilização dos serviços de crud generico do Tnf
            services
                .AddTnfDefaultConventionalRegistrations()
                .AddCommonDependency();
            
            return services;
        }
    }
}
