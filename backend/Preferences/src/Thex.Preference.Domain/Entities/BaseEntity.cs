﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Thex.Preference.Domain.Entities
{
    public class BaseEntity : ThexMultiTenantFullAuditedEntity, IEntityString
    {
        public string Id { get; set; }
        public bool IsActive { get; set; }
        public int? PropertyId { get; internal set; }
        public int? ChainId { get; internal set; }
    }
}
