﻿using System;

namespace Thex.Preference.Domain.Entities
{
    [Serializable]
    public abstract class ThexMultiTenantFullAuditedEntity : ThexFullAuditedEntity
    {
        public Guid? TenantId { get; set; }
    }
}
