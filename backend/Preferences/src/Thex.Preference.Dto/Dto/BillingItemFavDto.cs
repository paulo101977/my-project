﻿using System.Collections.Generic;

namespace Thex.Preference.Dto
{
    public class BillingItemFavDto : BaseUserFavDto
    {
        public BillingItemFavDto()
        {
            this.Details = new List<BillingItemFavDetailsDto>();
        }

        public static BillingItemFavDto NullInstance = null;

        public IList<BillingItemFavDetailsDto> Details { get; set; }
    }

    public class BillingItemFavDetailsDto
    {
        public int BillingItemId { get; set; }
    }
}
