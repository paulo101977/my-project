﻿using System;
using System.Collections.Generic;
using System.Text;
using Tnf.Dto;

namespace Thex.Preference.Dto
{
    public class BaseUserFavDto : BaseDto
    {
        public String Id { get; set; }
        public Guid UserId { get; set; }
        public int Type { get; set; }
    }
}
