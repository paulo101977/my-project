﻿using System.Collections.Generic;

namespace Thex.Preference.Dto
{
    public class MostSelectedDto : BaseUserFavDto
    {
        public static MostSelectedDto NullInstance = null;

        public MostSelectedDto()
        {
            this.Details = new List<MostSelectedDetailsDto>();
        }

        public IList<MostSelectedDetailsDto> Details { get; set; }
    }

    public class MostSelectedDetailsDto
    {
        public string Description { get; set; }
        public string GroupDescription { get; set; }
        public string Code { get; set; }
        public string NcmCode { get; set; }
        public string BarCode { get; set; }
        public string ExternalId { get; set; }
        public decimal Quantity { get; set; }
        public string BillingItemId { get; set; }
        public decimal Amount { get; set; }
        public int Type { get; set; }
    }
}
