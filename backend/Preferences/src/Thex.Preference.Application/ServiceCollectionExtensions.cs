﻿using Microsoft.Extensions.DependencyInjection;
using Thex.Preference.Application.Interfaces;
using Thex.Preference.Application.Services;
using Thex.Preference.Domain;

namespace Thex.Preference.Application
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddApplicationServiceDependency(this IServiceCollection services)
        {
            // Dependencia do projeto Thex.Domain
            services
                .AddDomainDependency()
                .AddTnfDefaultConventionalRegistrations();

            // Registro dos serviços
           services.AddTransient<IMostSelectedAppService, MostSelectedAppService>();
           services.AddTransient<IBillingItemFavAppService, BillingItemFavAppService>();

            return services;
        }
    }
}