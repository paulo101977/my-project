﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Thex.Kernel;
using Thex.Preference.Application.Interfaces;
using Thex.Preference.Domain.MongoEntities;
using Thex.Preference.Dto;
using Thex.Preference.Infra.Extensions;
using Thex.Preference.Infra.Interfaces;
using Thex.Preference.Infra.Interfaces.ReadRepositories;
using Tnf.Application.Services;
using Tnf.Dto;
using Tnf.Notifications;

namespace Thex.Preference.Application.Services
{
    public class BillingItemFavAppService : ApplicationService, IBillingItemFavAppService
    {
        private readonly IApplicationUser _applicationUser;
        private readonly IBillingItemFavRepository _billingItemRepository;
        private readonly IBillingItemFavReadRepository _billingItemReadRepository;

        public BillingItemFavAppService(IApplicationUser applicationUser,
                                        INotificationHandler notificationHandler,
                                        IBillingItemFavRepository billingItemRepository,
                                        IBillingItemFavReadRepository billingItemReadRepository) : base(notificationHandler)
        {
            _applicationUser = applicationUser;
            _billingItemRepository = billingItemRepository;
            _billingItemReadRepository = billingItemReadRepository;
        }

        public async Task<bool> Create(BillingItemFavDto dto)
        {
            if (!ValidateDto<MostSelectedDto>(dto))
                return false;

            var entity = dto.ToEntity();

            if (entity == null && Notification.HasNotification())
                return false;

            var dbEntity = await _billingItemReadRepository.GetEntityByUserId(entity.UserId);

            if (dbEntity.Count == 0)
            {
                await _billingItemRepository.Post(entity);
            }
            else
            {
                entity.Id = dbEntity.FirstOrDefault().Id;
                entity.IsActive = dbEntity.FirstOrDefault().IsActive;

                dbEntity.FirstOrDefault().Details.ToList().ForEach(x =>
                {
                    if (entity.Details.ToList().Find(e => e.BillingItemId == x.BillingItemId) == null)
                        entity.Details.Add(new BillingItemFavDetails()
                        {
                            BillingItemId = x.BillingItemId
                        });
                });

                await _billingItemRepository.Change(entity);
            }

            if (Notification.HasNotification())
                return false;

            return true;
        }

        public async Task<bool> Delete(BillingItemFavDto dto)
        {
            if (!ValidateDto<MostSelectedDto>(dto))
                return false;

            var entity = dto.ToEntity();

            if (entity == null && Notification.HasNotification())
                return false;

            var dbEntity = await _billingItemReadRepository.GetEntityByUserId(entity.UserId);

            if (dbEntity.FirstOrDefault().Details.Count == 1)
            {
                await _billingItemRepository.Delete(dbEntity.FirstOrDefault());
            }
            else
            {
                dbEntity.FirstOrDefault().Details.RemoveAt(dbEntity.FirstOrDefault().Details.ToList().FindIndex(x => x.BillingItemId == entity.Details.FirstOrDefault().BillingItemId));

                await _billingItemRepository.Change(dbEntity.FirstOrDefault());
            }

            if (Notification.HasNotification())
                return false;

            return true;
        }

        public async Task<IListDto<BillingItemFavDto>> GetAll(string userId)
        {
            return await _billingItemReadRepository.GetDtoByUserId(Guid.Parse(userId));
        }
    }
}
