﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Thex.Kernel;
using Thex.Preference.Application.Interfaces;
using Thex.Preference.Domain.MongoEntities;
using Thex.Preference.Dto;
using Thex.Preference.Infra.Extensions;
using Thex.Preference.Infra.Interfaces.ReadRepositories;
using Thex.Preference.Infra.Repositories;
using Tnf.Application.Services;
using Tnf.Dto;
using Tnf.Notifications;

namespace Thex.Preference.Application.Services
{
    public class MostSelectedAppService : ApplicationService, IMostSelectedAppService
    {
        private readonly IApplicationUser _applicationUser;
        private readonly IMostSelectedRepository _userPreferenceRepository;
        private readonly IMostSelectedReadRepository _userPreferenceReadRepository;

        public MostSelectedAppService(IApplicationUser applicationUser,
                                        INotificationHandler notificationHandler,
                                        IMostSelectedRepository userPreferenceRepository,
                                        IMostSelectedReadRepository userPreferenceReadRepository) : base(notificationHandler)
        {
            _applicationUser = applicationUser;
            _userPreferenceRepository = userPreferenceRepository;
            _userPreferenceReadRepository = userPreferenceReadRepository;
        }

        public async Task<bool> Create(MostSelectedDto dto)
        {
            dto.UserId = _applicationUser.UserUid;

            if (!ValidateDto<MostSelectedDto>(dto))
                return false;

            var entity = dto.ToEntity();

            if (entity == null && Notification.HasNotification())
                return false;

            var dbEntity = await _userPreferenceReadRepository.GetByUserId(entity.UserId, entity.Type);

            if (dbEntity.Count == 0)
            {
                await _userPreferenceRepository.Post(entity);
            }
            else
            {
                IncrementMostSelectByType(entity, dbEntity.FirstOrDefault());

                await _userPreferenceRepository.Change(entity);
            }

            if (Notification.HasNotification())
                return false;

            return true;
        }

        public async Task<MostSelectedDto> GetMostSelectedByUserId(string userId)
        {
            var dbEntity = await _userPreferenceReadRepository.GetByUserIdWithOrderedDetails(Guid.Parse(userId));

            if (dbEntity == null || dbEntity.Count == 0)
                return MostSelectedDto.NullInstance;

            return dbEntity.FirstOrDefault().ToDto();
        }

        public async Task<ListDto<MostSelectedDto>> GetAll()
        {
            var ret = new ListDto<MostSelectedDto>();

            var dbList = await _userPreferenceReadRepository.GetAll();

            foreach (var item in dbList)
            {
                ret.Items.Add(item.ToDto());
            }

            return ret;
        }

        private void IncrementMostSelectByType(MostSelected entity, MostSelected dbEntity)
        {
            entity.CreationTime = dbEntity.CreationTime;
            entity.CreatorUserId = dbEntity.CreatorUserId;
            entity.Id = dbEntity.Id;
            entity.IsActive = true;

            foreach (var preferenceDetails in dbEntity.Details)
            {
                MostSelectedDetails entityDetail = null;

                entityDetail = entity.Details.ToList().Find(x => x.ExternalId == preferenceDetails.ExternalId);

                if (entityDetail != null)
                {
                    entityDetail.Quantity += preferenceDetails.Quantity;
                }
                else
                {
                    entity.Details.Add(preferenceDetails);
                }
            }
        }
    }
}
