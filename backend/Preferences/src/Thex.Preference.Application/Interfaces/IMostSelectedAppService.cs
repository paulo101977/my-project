﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Thex.Preference.Dto;
using Tnf.Application.Services;
using Tnf.Dto;

namespace Thex.Preference.Application.Interfaces
{
    public interface IMostSelectedAppService: IApplicationService
    {
        Task<bool> Create(MostSelectedDto dto);
        Task<MostSelectedDto> GetMostSelectedByUserId(string userId);
        Task<ListDto<MostSelectedDto>> GetAll();
    }
}
