﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Thex.Preference.Dto;
using Tnf.Dto;

namespace Thex.Preference.Application.Interfaces
{
    public interface IBillingItemFavAppService
    {
        Task<bool> Create(BillingItemFavDto dto);
        Task<bool> Delete(BillingItemFavDto dto);
        Task<IListDto<BillingItemFavDto>> GetAll(string userId);
    }
}
