﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Thex.Preference.Application.Interfaces;
using Thex.Preference.Base;
using Thex.Preference.Domain.Entities;
using Thex.Preference.Dto;
using Thex.Preference.Dto.GetAll;
using Tnf.AspNetCore.Mvc.Response;
using Tnf.Dto;

namespace Thex.Preference.Web.Controllers
{
    [Route(RouteConsts.UserPreferenceRouteNames)]
    public class UserPreferenceController : ThexAppController
    {
        private readonly IMostSelectedAppService _mostSelectedAppService;
        private readonly IBillingItemFavAppService _favBillingItemAppService;

        public UserPreferenceController(IMostSelectedAppService userPreferenceAppService,
                                        IBillingItemFavAppService favBillingItemAppService)
        {
            _mostSelectedAppService = userPreferenceAppService;
            _favBillingItemAppService = favBillingItemAppService;
        }

        /// <summary>
        /// Insere novos itens na liste de mais selecionados do usuário
        /// </summary>
        [HttpPost("mostselected")]
        [ThexAuthorize("PREF_UserPreference_Post_MostSelected")]
        [ProducesResponseType(typeof(MostSelectedDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> PostMostSelected([FromBody] MostSelectedDto dto)
        {
            var response = await _mostSelectedAppService.Create(dto);

            return CreateResponseOnPost(response, EntityNames.MostSelected);
        }

        /// <summary>
        /// Retorna a lista de itens mais selecionados por usuário
        /// </summary>
        [HttpGet("mostselected/{userid}")]
        [ThexAuthorize("PREF_UserPreference_Get_MostSelected")]
        [ProducesResponseType(typeof(MostSelectedDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> GetMostSelectedByUserId(string userid, [FromQuery]RequestDto dto)
        {
            var response = await _mostSelectedAppService.GetMostSelectedByUserId(userid);

            return CreateResponseOnGet(response, EntityNames.MostSelected);
        }

        /// <summary>
        /// Insere novos pontos de venda na lista de favoritos para o usuário
        /// </summary>
        [HttpPost("billingitemfav")]
        [ThexAuthorize("PREF_UserPreference_Post_BillingItemFav")]
        [ProducesResponseType(typeof(BillingItemFavDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> PostFavBillingItem([FromBody] BillingItemFavDto dto)
        {
            var response = await _favBillingItemAppService.Create(dto);

            return CreateResponseOnPost(response, EntityNames.FavBillingItem);
        }

        /// <summary>
        /// Remove um ponto de venda na lista de favoritos para o usuário
        /// </summary>
        [HttpPost("billingitemfav/undo")]
        [ProducesResponseType(typeof(BillingItemFavDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> DeleteFavBillingItem([FromBody] BillingItemFavDto dto)
        {
            var response = await _favBillingItemAppService.Delete(dto);

            return CreateResponseOnDelete(response, EntityNames.FavBillingItem);
        }

        /// <summary>
        /// Insere novos pontos de venda na lista de favoritos para o usuário
        /// </summary>
        [HttpGet("billingitemfav/{userid}")]
        [ThexAuthorize("PREF_UserPreference_Get_BillingItemFav")]
        [ProducesResponseType(typeof(BillingItemFavDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> PostFavBillingItem(string userid, [FromQuery]RequestDto dto)
        {
            var response = await _favBillingItemAppService.GetAll(userid);

            return CreateResponseOnGet(response, EntityNames.FavBillingItem);
        }
    }
}