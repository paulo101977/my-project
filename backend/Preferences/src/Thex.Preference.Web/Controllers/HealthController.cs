﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Thex.Preference.Application.Interfaces;
using Thex.Preference.Domain.Entities;
using Tnf.AspNetCore.Mvc.Response;

namespace Thex.Preference.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [AllowAnonymous]
    public class HealthController : TnfController
    {
        private readonly IMostSelectedAppService _mostSelectedAppService;

        public HealthController(IMostSelectedAppService userPreferenceAppService)
        {
            _mostSelectedAppService = userPreferenceAppService;
        }

        /// <summary>
        /// Retorna a lista de itens mais selecionados por usuário
        /// </summary>
        [HttpGet("check")]
        [ProducesResponseType(typeof(bool), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> GetMostSelectedByUserId()
        {
            var response = await _mostSelectedAppService.GetAll();

            return CreateResponseOnGetAll(response != null ? true : false, EntityNames.Health);
        }
    }
}