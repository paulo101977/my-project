﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.ResponseCompression;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.IO.Compression;
using System.Threading.Tasks;
using Thex.AspNetCore.Security;
using Thex.Kernel;
using Thex.Preference.Application;
using Thex.Preference.Domain.Entities;
using Thex.Preference.Infra;

namespace Thex.Preference.Web
{
    public class Startup
    {
        private IConfiguration Configuration { get; set; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            services.Configure<GzipCompressionProviderOptions>(
                options => options.Level = CompressionLevel.Optimal);

            services
                .AddCorsAll("AllowAll")
                .AddApplicationServiceDependency()
                .AddInfraDependency()
                .AddTnfAspNetCore()
                .AddResponseCompression(options =>
                {
                    options.Providers.Add<GzipCompressionProvider>();
                    options.EnableForHttps = true;
                });

            services.AddTnfMongoDb(builder => builder
               .WithConnectionString(Configuration[$"ConnectionStrings:MongoDb:Preference:MongoDBServer"])
               .WithDatabaseName(Configuration[$"ConnectionStrings:MongoDb:Preference:MongoDBName"]));

            services
                .AddThexAspNetCoreSecurity(options =>
                {
                    var settingsSection = Configuration.GetSection("TokenConfiguration");
                    var settings = settingsSection.Get<TokenConfiguration>();

                    options.TokenConfiguration = settings;
                    options.FrontEndpoint = Configuration.GetValue<string>("UrlFront");
                    options.SuperAdminEndpoint = Configuration.GetValue<string>("SuperAdminEndpoint");

                });

            services.AddSwaggerDocumentation();

            services.AddAntiforgery(options =>
            {
                options.Cookie.Name = "X-CSRF-TOKEN-GOTNEXT-COOKIE";
                options.HeaderName = "X-CSRF-TOKEN-GOTNEXT-HEADER";
                options.SuppressXFrameOptionsHeader = false;
            })
            .AddMvc()
            .AddJsonOptions(opts =>
            {
                opts.SerializerSettings.NullValueHandling =
                    Newtonsoft.Json.NullValueHandling.Ignore;
            });

            var serviceProvider = services.BuildServiceProvider();

            ServiceLocator.SetLocatorProvider(serviceProvider);

            return serviceProvider;
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILogger<Startup> logger)
        {
            app.UseCors("AllowAll");

            // Configura o use do AspNetCore do Tnf
            app.UseTnfAspNetCore(options =>
            {
                options.UseDomainLocalization();
            });

            if (env.IsDevelopment())
                app.UseDeveloperExceptionPage();

            app.UseSwaggerDocumentation();

            app.UseThexAspNetCoreSecurity();

            app.UseMvcWithDefaultRoute();
            app.UseResponseCompression();

            app.Run(context =>
            {
                context.Response.Redirect("/swagger");
                return Task.CompletedTask;
            });

            logger.LogInformation("Start application ...");
        }
    }
}
