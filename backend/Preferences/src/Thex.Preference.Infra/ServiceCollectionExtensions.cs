﻿using Microsoft.Extensions.DependencyInjection;
using Thex.Preference.Infra.Interfaces;
using Thex.Preference.Infra.Interfaces.ReadRepositories;
using Thex.Preference.Infra.Repositories;
using Thex.Preference.Infra.Repositories.ReadRepositories;

namespace Thex.Preference.Infra
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddInfraDependency(this IServiceCollection services)
        {
            services.AddTransient<IMostSelectedReadRepository, MostSelectedReadRepository>();
            services.AddTransient<IMostSelectedRepository, MostSelectedRepository>();
            services.AddTransient<IBillingItemFavRepository, BillingItemRepository>();
            services.AddTransient<IBillingItemFavReadRepository, BillingItemFavReadRepository>();

            return services;
        }
    }
}
