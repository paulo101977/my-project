﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Thex.Preference.Domain.MongoEntities;

namespace Thex.Preference.Infra.Interfaces
{
    public interface IBillingItemFavRepository
    {
        Task Delete(BillingItemFav entity);
        Task Post(BillingItemFav entity);
        Task Change(BillingItemFav entity);
    }
}
