﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Thex.Preference.Domain.MongoEntities;
using Thex.Preference.Dto;

namespace Thex.Preference.Infra.Repositories
{
    public interface IMostSelectedRepository
    {
        Task Post(MostSelected dto);
        Task Change(MostSelected dto);
    }
}
