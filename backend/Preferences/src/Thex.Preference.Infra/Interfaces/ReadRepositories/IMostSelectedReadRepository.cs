﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Thex.Common.Enumerations;
using Thex.Preference.Domain.MongoEntities;
using Thex.Preference.Dto;
using Tnf.Dto;

namespace Thex.Preference.Infra.Interfaces.ReadRepositories
{
    public interface IMostSelectedReadRepository
    {
        Task<IList<MostSelected>> GetByUserId(Guid userId, MostSelectedTypeEnum type);
        
        Task<IList<MostSelected>> GetByUserIdWithOrderedDetails(Guid userId);

        Task<IList<MostSelected>> GetAll();
    }
}
