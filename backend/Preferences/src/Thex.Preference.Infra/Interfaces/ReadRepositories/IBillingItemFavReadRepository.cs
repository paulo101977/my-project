﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Thex.Preference.Domain.MongoEntities;
using Thex.Preference.Dto;
using Tnf.Dto;

namespace Thex.Preference.Infra.Interfaces.ReadRepositories
{
    public interface IBillingItemFavReadRepository
    {
        Task<IList<BillingItemFav>> GetEntityByUserId(Guid userId);
        Task<IListDto<BillingItemFavDto>> GetDtoByUserId(Guid userId);
    }
}
