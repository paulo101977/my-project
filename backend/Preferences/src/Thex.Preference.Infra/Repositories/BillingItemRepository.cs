﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Thex.Kernel;
using Thex.Preference.Domain.MongoEntities;
using Thex.Preference.Extensions;
using Thex.Preference.Infra.Interfaces;
using Thex.Preference.Infra.Interfaces.ReadRepositories;
using Tnf.MongoDb.Provider;
using Tnf.MongoDb.Repositories;
using Tnf.Notifications;

namespace Thex.Preference.Infra.Repositories
{
    public class BillingItemRepository : MongoDbRepository<BillingItemFav, string>, IBillingItemFavRepository
    {
        private INotificationHandler _notification { get; set; }
        private IApplicationUser _applicationUser { get; set; }
        private IBillingItemFavReadRepository _billingItemReadRepository { get; set; }

        public BillingItemRepository(
            IMongoDbProvider provider,
            INotificationHandler notification,
            IApplicationUser applicationUser,
            IBillingItemFavReadRepository billingItemReadRepository) : base(provider)
        {
            _notification = notification;
            _applicationUser = applicationUser;
            _billingItemReadRepository = billingItemReadRepository;
        }

        public async Task Post(BillingItemFav entity)
        {
            entity.SetOperationValues(Operation.Insert, _applicationUser);

            await InsertAsync(entity);
        }

        public async Task Delete(BillingItemFav entity)
        {
            entity.SetOperationValues(Operation.Delete, _applicationUser);

            await DeleteAsync(entity);
        }

        public async Task Change(BillingItemFav entity)
        {
            entity.SetOperationValues(Operation.Update, _applicationUser);

            await UpdateAsync(entity);
        }
    }
}
