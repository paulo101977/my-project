﻿using System.Threading.Tasks;
using Thex.Kernel;
using Thex.Preference.Domain.MongoEntities;
using Thex.Preference.Extensions;
using Thex.Preference.Infra.Interfaces.ReadRepositories;
using Tnf.MongoDb.Provider;
using Tnf.MongoDb.Repositories;
using Tnf.Notifications;

namespace Thex.Preference.Infra.Repositories
{
    public class MostSelectedRepository : MongoDbRepository<MostSelected, string>, IMostSelectedRepository
    {
        private INotificationHandler _notification { get; set; }
        private IApplicationUser _applicationUser { get; set; }
        private IMostSelectedReadRepository _userPreferenceReadRepopsitory { get; set; }

        public MostSelectedRepository(
            IMongoDbProvider provider,
            INotificationHandler notification,
            IApplicationUser applicationUser,
            IMostSelectedReadRepository userPreferenceReadRepopsitory) : base(provider)
        {
            _notification = notification;
            _applicationUser = applicationUser;
            _userPreferenceReadRepopsitory = userPreferenceReadRepopsitory;
        }

        public async Task Post(MostSelected entity)
        {
            entity.SetOperationValues(Operation.Insert, _applicationUser);

            await InsertAsync(entity);
        }

        public async Task Change(MostSelected entity)
        {
            entity.SetOperationValues(Operation.Update, _applicationUser);

            await UpdateAsync(entity);
        }
    }
}
