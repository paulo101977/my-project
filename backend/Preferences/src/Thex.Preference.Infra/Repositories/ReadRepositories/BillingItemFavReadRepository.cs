﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Thex.Kernel;
using Thex.Preference.Domain.MongoEntities;
using Thex.Preference.Dto;
using Thex.Preference.Infra.Extensions;
using Thex.Preference.Infra.Interfaces.ReadRepositories;
using Tnf.Dto;
using Tnf.MongoDb.Provider;
using Tnf.MongoDb.Repositories;
using Tnf.Notifications;

namespace Thex.Preference.Infra.Repositories.ReadRepositories
{
    public class BillingItemFavReadRepository : MongoDbRepository<BillingItemFav, string>, IBillingItemFavReadRepository
    {
        private INotificationHandler _notification { get; set; }
        private IApplicationUser _applicationUser { get; set; }

        public BillingItemFavReadRepository(
            IMongoDbProvider provider,
            INotificationHandler notification,
            IApplicationUser applicationUser) : base(provider)
        {
            _notification = notification;
            _applicationUser = applicationUser;
        }

        public async Task<IList<BillingItemFav>> GetEntityByUserId(Guid userId)
        {
            var tId = _applicationUser.TenantId;
            var pId = _applicationUser.PropertyId.TryParseToInt32();

            return await WhereAsync(x =>  x.PropertyId == pId &&
                                          x.TenantId == tId &&
                                          x.UserId == userId);
        }

        public async Task<IListDto<BillingItemFavDto>> GetDtoByUserId(Guid userId)
        {
            var ret = new ListDto<BillingItemFavDto>();
            ret.HasNext = false;

            var tId = _applicationUser.TenantId;
            var pId = _applicationUser.PropertyId.TryParseToInt32();

            var query = await WhereAsync(x => x.IsActive == true &&
                                              x.PropertyId == pId &&
                                              x.TenantId == tId &&
                                              x.UserId == userId);

            query.ToList().ForEach(billing =>
            {
                ret.Items.Add(billing.ToDto());
            });

            return ret;
        }
    }
}
