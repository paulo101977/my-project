﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Thex.Common.Enumerations;
using Thex.Kernel;
using Thex.Preference.Domain.MongoEntities;
using Thex.Preference.Infra.Interfaces.ReadRepositories;
using Tnf.MongoDb.Provider;
using Tnf.MongoDb.Repositories;
using Tnf.Notifications;

namespace Thex.Preference.Infra.Repositories.ReadRepositories
{
    public class MostSelectedReadRepository : MongoDbRepository<MostSelected, string>, IMostSelectedReadRepository
    {
        private INotificationHandler _notification { get; set; }
        private IApplicationUser _applicationUser { get; set; }

        public MostSelectedReadRepository(
            IMongoDbProvider provider,
            INotificationHandler notification,
            IApplicationUser applicationUser) : base(provider)
        {
            _notification = notification;
            _applicationUser = applicationUser;
        }

        public async Task<IList<MostSelected>> GetByUserId(Guid userId, MostSelectedTypeEnum type)
        {
            var tId = _applicationUser.TenantId;
            var pId = _applicationUser.PropertyId.TryParseToInt32();

            return await WhereAsync(x => x.IsActive == true &&
                                          x.PropertyId == pId &&
                                          x.TenantId == tId &&
                                          x.UserId == userId &&
                                          x.Type == type);
        }

        public Task<IList<MostSelected>> GetByUserIdWithOrderedDetails(Guid userId)
        {
            var tId = _applicationUser.TenantId;
            var pId = _applicationUser.PropertyId.TryParseToInt32();

            var mostSelected = WhereAsync(x => x.IsActive == true &&
                                          x.PropertyId == pId &&
                                          x.TenantId == tId &&
                                          x.UserId == userId).Result;

            var orderedProducts = mostSelected.Where(x => x.Type == MostSelectedTypeEnum.MostSelectedProduct)
                        .OrderBy(x => x.UserId).ThenBy(x => x.UserId == userId)
                        .Select(x => new MostSelected
                        {
                            Details = x.Details.OrderByDescending(d => d.Quantity).Take(12)
                            .Select(d => new MostSelectedDetails
                            {
                                GroupDescription = d.GroupDescription,
                                Description = d.Description,
                                Code = d.Code,
                                Quantity = d.Quantity,
                                ExternalId = d.ExternalId,
                                Type = (int)MostSelectedTypeEnum.MostSelectedProduct,
                                BillingItemId = d.BillingItemId
                            }).ToList()
                        }).FirstOrDefault();

            var orderedServices = mostSelected.Where(x => x.Type == MostSelectedTypeEnum.MostSelectedService)
                                     .OrderBy(x => x.UserId).ThenBy(x => x.UserId == userId)
                                     .Select(x => new MostSelected
                                     {
                                         Details = x.Details.OrderByDescending(d => d.Quantity).Take(12)
                                         .Select(d => new MostSelectedDetails
                                         {
                                             GroupDescription = d.GroupDescription,
                                             Description = d.Description,
                                             Code = d.Code,
                                             Quantity = d.Quantity,
                                             ExternalId = d.ExternalId,
                                             Type = (int)MostSelectedTypeEnum.MostSelectedService,
                                             BillingItemId = d.BillingItemId
                                         }).ToList()
                                     }).FirstOrDefault();

            var orderedCredit = mostSelected.Where(x => x.Type == MostSelectedTypeEnum.MostSelectedCredit)
                                     .OrderBy(x => x.UserId).ThenBy(x => x.UserId == userId)
                                     .Select(x => new MostSelected
                                     {
                                         Details = x.Details.OrderByDescending(d => d.Quantity).Take(12)
                                         .Select(d => new MostSelectedDetails
                                         {
                                             GroupDescription = d.GroupDescription,
                                             Description = d.Description,
                                             Code = d.Code,
                                             Quantity = d.Quantity,
                                             ExternalId = d.ExternalId,
                                             Type = (int)MostSelectedTypeEnum.MostSelectedCredit,
                                             BillingItemId = d.BillingItemId
                                         }).ToList()
                                     }).FirstOrDefault();

            var finalList = new List<MostSelected>();
            var finalDetails = new List<MostSelectedDetails>();

            if (orderedProducts != null)
                finalDetails.AddRange((orderedProducts as MostSelected).Details);

            if (orderedServices != null)
                finalDetails.AddRange((orderedServices as MostSelected).Details);

            if (orderedCredit != null)
                finalDetails.AddRange((orderedCredit as MostSelected).Details);

            if (mostSelected != null && mostSelected.Count > 0)
                finalList.Add(new MostSelected()
                {
                    Id = mostSelected.FirstOrDefault().Id,
                    ChainId = mostSelected.FirstOrDefault().ChainId,
                    CreationTime = mostSelected.FirstOrDefault().CreationTime,
                    CreatorUserId = mostSelected.FirstOrDefault().CreatorUserId,
                    DeleterUserId = mostSelected.FirstOrDefault().DeleterUserId,
                    DeletionTime = mostSelected.FirstOrDefault().DeletionTime,
                    IsActive = mostSelected.FirstOrDefault().IsActive,
                    IsDeleted = mostSelected.FirstOrDefault().IsDeleted,
                    LastModificationTime = mostSelected.FirstOrDefault().LastModificationTime,
                    LastModifierUserId = mostSelected.FirstOrDefault().LastModifierUserId,
                    PropertyId = mostSelected.FirstOrDefault().PropertyId,
                    TenantId = mostSelected.FirstOrDefault().TenantId,
                    UserId = mostSelected.FirstOrDefault().UserId,
                    Details = finalDetails
                });

            var ordered = finalList
                         .OrderBy(x => x.UserId).ThenBy(x => x.UserId == userId)
                         .Select(x => new MostSelected
                         {
                             Id = x.Id,
                             ChainId = x.ChainId,
                             CreationTime = x.CreationTime,
                             CreatorUserId = x.CreatorUserId,
                             DeleterUserId = x.DeleterUserId,
                             DeletionTime = x.DeletionTime,
                             IsActive = x.IsActive,
                             IsDeleted = x.IsDeleted,
                             LastModificationTime = x.LastModificationTime,
                             LastModifierUserId = x.LastModifierUserId,
                             PropertyId = x.PropertyId,
                             TenantId = x.TenantId,
                             Type = x.Type,
                             UserId = x.UserId,
                             Details = x.Details.OrderByDescending(d => d.Quantity).Take(12)
                             .Select(d => new MostSelectedDetails
                             {
                                 GroupDescription = d.GroupDescription,
                                 Description = d.Description,
                                 Code = d.Code,
                                 Quantity = d.Quantity,
                                 ExternalId = d.ExternalId,
                                 Type = d.Type,
                                 BillingItemId = d.BillingItemId
                             }).ToList()
                         }).ToList();

            return Task.FromResult<IList<MostSelected>>(ordered);
        }

        public async Task<IList<MostSelected>> GetAll()
        {
            var tId = _applicationUser.TenantId;
            var pId = _applicationUser.PropertyId.TryParseToInt32();

            var mostSelected = await WhereAsync(x => x.IsActive == false &&
                                                     x.PropertyId == pId &&
                                                     x.TenantId == tId);

            return mostSelected;
        }
    }
}
