﻿using System;

namespace Thex.Preference.Infra.Common
{
    [Serializable]
    public abstract class ThexMultiTenantNullableFullAuditedEntity : ThexFullAuditedEntity
    {
        public Guid? TenantId { get; set; }
        public Tenant Tenant { get; set; }
    }
}
