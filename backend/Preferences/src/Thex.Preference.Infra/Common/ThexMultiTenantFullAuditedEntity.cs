﻿using System;

namespace Thex.Preference.Infra.Common
{
    [Serializable]
    public abstract class ThexMultiTenantFullAuditedEntity : ThexFullAuditedEntity
    {
        public Guid TenantId { get; set; }

        public Tenant Tenant { get; set; }
    }
}
