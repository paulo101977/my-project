﻿using Thex.Common.Enumerations;
using Thex.Preference.Domain;
using Thex.Preference.Domain.MongoEntities;
using Thex.Preference.Dto;

namespace Thex.Preference.Infra.Extensions
{
    public static class PreferenceExtensions
    {
        public static MostSelected ToEntity(this MostSelectedDto dto)
        {
            var ret = new MostSelected()
            {
                Id = dto.Id,
                UserId = dto.UserId,
                Type = (MostSelectedTypeEnum)dto.Type
            };

            foreach (var dtoDetail in dto.Details)
            {
                ret.Details.Add(new MostSelectedDetails()
                {
                    Description = dtoDetail.Description,
                    Code = dtoDetail.Code,
                    Quantity = dtoDetail.Quantity,
                    ExternalId = dtoDetail.ExternalId,
                    Type = dto.Type,
                    BillingItemId = dtoDetail.BillingItemId,
                    GroupDescription = dtoDetail.GroupDescription
                });
            }

            return ret;
        }

        public static BillingItemFav ToEntity(this BillingItemFavDto dto)
        {
            var ret = new BillingItemFav()
            {
                Id = dto.Id,
                UserId = dto.UserId,
                Type = (PreferenceType)dto.Type
            };

            foreach (var dtoDetail in dto.Details)
            {
                ret.Details.Add(new BillingItemFavDetails()
                {
                    BillingItemId = dtoDetail.BillingItemId
                });
            }

            return ret;
        }

        public static MostSelectedDto ToDto(this MostSelected entity)
        {
            var ret = new MostSelectedDto()
            {
                Id = entity.Id,
                UserId = entity.UserId,
                Type = (int)entity.Type
            };

            foreach (var entityDetail in entity.Details)
            {
                ret.Details.Add(new MostSelectedDetailsDto()
                {
                    Description = entityDetail.Description,
                    Code = entityDetail.Code,
                    Quantity = entityDetail.Quantity,
                    ExternalId = entityDetail.ExternalId,
                    Type = entityDetail.Type,
                    BillingItemId = entityDetail.BillingItemId,
                    GroupDescription = entityDetail.GroupDescription
                });
            }

            return ret;
        }

        public static BillingItemFavDto ToDto(this BillingItemFav entity)
        {
            var ret = new BillingItemFavDto()
            {
                Id = entity.Id,
                UserId = entity.UserId,
                Type = (int)entity.Type
            };

            foreach (var entityDetail in entity.Details)
            {
                ret.Details.Add(new BillingItemFavDetailsDto()
                {
                    BillingItemId = entityDetail.BillingItemId
                });
            }

            return ret;
        }
    }
}
