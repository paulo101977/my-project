﻿using System;
using Thex.Kernel;
using Thex.Preference.Domain.Entities;
using Thex.Preference.Infra;

namespace Thex.Preference.Extensions
{
    public static class DtoEntityExtensions
    {
        // <summary>
        // Preenche os campos de acordo com a operação(Inserção, Edição e deleção)
        // </summary>
        public static BaseEntity SetOperationValues(this BaseEntity entity, Operation operation, IApplicationUser _applicationUser)
        {
            var timeZone = _applicationUser.TimeZoneName;
            var userId = _applicationUser.UserUid;

            entity.TenantId = _applicationUser.TenantId;
            entity.PropertyId = _applicationUser.PropertyId.TryParseToInt32();
            entity.ChainId = _applicationUser.ChainId.TryParseToInt32();

            switch (operation)
            {
                case Operation.Insert:
                    {
                        if (string.IsNullOrEmpty(entity.Id))
                            entity.Id = Guid.NewGuid().ToString();

                        entity.CreationTime = DateTime.UtcNow.ToZonedDateTime(timeZone);
                        entity.CreatorUserId = userId;
                        entity.IsActive = true;
                        entity.IsDeleted = false;
                    }
                    break;
                case Operation.Update:
                    {
                        entity.LastModificationTime = DateTime.UtcNow.ToZonedDateTime(timeZone);
                        entity.LastModifierUserId = userId;
                    }
                    break;
                case Operation.Delete:
                    {
                        entity.IsDeleted = true;
                        entity.IsActive = false;
                        entity.DeletionTime = DateTime.UtcNow.ToZonedDateTime(timeZone);
                        entity.DeleterUserId = userId;
                    }
                    break;
            }

            return entity;
        }
    }
}