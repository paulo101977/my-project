﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.HigsIntegration.Application.Interfaces;
using Thex.HigsIntegration.Dto.Dto;
using Thex.HigsIntegration.Dto.Dto.GetAll;

namespace Thex.HigsIntegration.Web.Tests.Mocks
{
    public class CustomerStationAppServiceMock : ICustomerStationAppService
    {
        public async Task<CustomerStationDto> CreateAsync(CustomerStationDto dto)
        {
            return await CustomerStationMock.GetDtoAsync();
        }

        public Task DeleteAsync(Guid id)
        {
            return Task.CompletedTask;
        }

        public async Task<IList<CustomerStationDto>> GetAllCustomerStationDto(GetAllCustomerStationDto requestDto)
        {
            return await CustomerStationMock.GetDtoListAsync();
        }

        public Task<IList<CustomerStationDto>> GetAllDtoByCompanyClient(Guid id)
        {
            throw new NotImplementedException();
        }

        public Task<CustomerStationDto> GetCustomerStationDtoById(Guid id)
        {
            return CustomerStationMock.GetDtoAsync();
        }

        public Task ToogleAsync(Guid id)
        {
            return Task.CompletedTask;
        }

        public Task<CustomerStationDto> UpdateAsync(Guid id, CustomerStationDto dto)
        {
            return CustomerStationMock.GetDtoAsync();
        }
    }
}
