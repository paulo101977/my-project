﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.Common.Dto;
using Thex.HigsIntegration.Dto.Dto;
using Thex.HigsIntegration.Infra.Entities;
using Tnf.Notifications;

namespace Thex.HigsIntegration.Web.Tests.Mocks
{
    public class CustomerStationMock
    {
        public static CustomerStationDto GetDto(string name = "Posto de cliente teste")
        {
            var contactPersonList = new List<CustomerStationContactPersonDto>();
            contactPersonList.Add(new CustomerStationContactPersonDto()
            {
                Name = "João da Silva",
                PhoneNumber = "24 2222-2222",
                Email = "joao@gmail.com",
                OccupationId = 1
            });

            var locationList = new List<LocationDto>();
            locationList.Add(new LocationDto()
            {
                LocationCategoryId = 1,
                Latitude = -22.899903m,
                Longitude = -43.179114m,
                StreetName = "Rua Visconde de Inhauma",
                StreetNumber = "83",
                Neighborhood = "Centro",
                PostalCode = "20091",
                CountryCode = "BR",
                BrowserLanguage = "pt-BR"
            });

            return new CustomerStationDto()
            {
                Id = Guid.Parse("23eb803c-726a-4c7c-b25b-2c22a56793d9"),
                IsActive = true,
                SocialReason = "Posto de cliente teste Social",
                CustomerStationCategoryName = name,
                CompanyClientId = Guid.Parse("6a27e16a-f227-4256-a62f-96e7b39d0658"),
                CustomerStationContactPersonList = contactPersonList,
                LocationList = locationList
            };
        }

        public static async Task<CustomerStationDto> GetDtoAsync(string name = "Posto de cliente teste")
        {
            var dto = GetDto();
            return await Task.FromResult(dto);
        }

        public static Task<IList<CustomerStationDto>> GetDtoListAsync()
        {
            var ret = new List<CustomerStationDto>();

            ret.Add(CustomerStationMock.GetDto("Posto de cliente 1"));
            ret.Add(CustomerStationMock.GetDto("Posto de cliente 2"));
            ret.Add(CustomerStationMock.GetDto("Posto de cliente 3"));

            return Task.FromResult<IList<CustomerStationDto>>(ret);
        }

        public static CustomerStation Get(INotificationHandler notificationHandler)
        {
            return new CustomerStation.Builder(notificationHandler)
                .WithCompanyClientId(Guid.NewGuid())
                .WithCustomerStationCategoryId(Guid.NewGuid())
                .WithCustomerStationName("Posto de cliente")
                .WithHomePage("www.hotel.com.br")
                .WithId(Guid.NewGuid())
                .WithIsActive(true)
                .WithSocialReason("Posto de cliente")
                .Build();
        }

        #region CustomerStationContactPerson
        public static List<CustomerStationContactPerson> GetListCustomerStationContactPerson(INotificationHandler notificationHandler)
        {
            var list = new List<CustomerStationContactPerson>();

            list.Add(CustomerStationContactPerson.Create(notificationHandler)
                .WithCustomerStationId(Guid.NewGuid())
                .WithId(Guid.NewGuid())
                .WithOccupationId(1)
                .WithPersonId(Guid.NewGuid())
                .Build());

            list.Add(CustomerStationContactPerson.Create(notificationHandler)
               .WithCustomerStationId(Guid.NewGuid())
               .WithId(Guid.NewGuid())
               .WithOccupationId(1)
               .WithPersonId(Guid.NewGuid())
               .Build());

            list.Add(CustomerStationContactPerson.Create(notificationHandler)
               .WithCustomerStationId(Guid.NewGuid())
               .WithId(Guid.NewGuid())
               .WithOccupationId(1)
               .WithPersonId(Guid.NewGuid())
               .Build());

            return list;
        }

        public static List<CustomerStationContactPersonDto> GetListCustomerStationContactPersonDto()
        {
            var list = new List<CustomerStationContactPersonDto>();
            list.Add(new CustomerStationContactPersonDto()
            {
                Id = Guid.NewGuid(),
                CustomerStationId = Guid.NewGuid(),
                PhoneNumber = "2122222222",
                Email = "joao@gmail.com",
                OccupationId = 1,
                Name = "João da Silva",
                PersonId = Guid.NewGuid(),
                OccupationName = "Ocupação"
            });

            list.Add(new CustomerStationContactPersonDto()
            {
                Id = Guid.NewGuid(),
                CustomerStationId = Guid.NewGuid(),
                PhoneNumber = "2122222222",
                Email = "joao@yahoo.com",
                OccupationId = 1,
                Name = "João da Silva",
                PersonId = Guid.NewGuid(),
                OccupationName = "Ocupação"
            });

            list.Add(new CustomerStationContactPersonDto()
            {
                Id = Guid.NewGuid(),
                CustomerStationId = Guid.NewGuid(),
                PhoneNumber = "2122222222",
                Email = "joao@outlook.com",
                OccupationId = 1,
                Name = "João da Silva",
                PersonId = Guid.NewGuid(),
                OccupationName = "Ocupação"
            });

            return list;
        }
        #endregion

        #region Person
        public static Person GetPerson(INotificationHandler notificationHandler)
        {
            return Person.Create(notificationHandler)
                .WithFirstName("João")
                .WithLastName("da Silva")
                .WithFullName("João da Silva")
                .WithId(Guid.NewGuid())
                .WithPersonType("N")
                .Build();
        }
        #endregion

        #region ContactInformation
        public static List<ContactInformation> GetListContactInformations(INotificationHandler notificationHandler)
        {
            var list = new List<ContactInformation>();

            list.Add(ContactInformation.Create(notificationHandler)
                .WithContactInformationTypeId(1)
                .WithInformation("joao@gmail.com")
                .WithOwnerId(Guid.NewGuid())
                .Build());

            list.Add(ContactInformation.Create(notificationHandler)
                .WithContactInformationTypeId(2)
                .WithInformation("2122222222")
                .WithOwnerId(Guid.NewGuid())
                .Build());

            list.Add(ContactInformation.Create(notificationHandler)
                .WithContactInformationTypeId(3)
                .WithInformation("www.joao.com")
                .WithOwnerId(Guid.NewGuid())
                .Build());

            return list;
        }
        #endregion

        #region Occupation
        public static Occupation GetOccupation(INotificationHandler notificationHandler)
        {
            return Occupation.Create(notificationHandler)
                .WithCompanyId(1)
                .WithId(1)
                .WithName("Ocupação")
                .Build();
        }
        #endregion
    }
}
