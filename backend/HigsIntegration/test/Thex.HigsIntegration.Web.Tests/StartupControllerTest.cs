﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using Thex.Common;
using Thex.HigsIntegration.Application.Interfaces;
using Thex.HigsIntegration.Web.Tests.Mocks;
using Thex.Kernel;
using Thex.Location.Application;
using Thex.Location.Application.Interfaces;
using Thex.Location.Application.Services;
using Thex.Maps.Geocode;

namespace Thex.HigsIntegration.Web.Tests
{
    public class StartupControllerTest
    {
        private IConfiguration Configuration { get; set; }

        public StartupControllerTest(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            services.AddTnfAspNetCoreSetupTest();
            services.AddApplicationServiceLocationDependency(options =>
            {
                var settingsSection = Configuration.GetSection("MapsGeocodeConfig");
                var settings = settingsSection.Get<MapsGeocodeConfig>();
            }, Configuration);
            services.AddTransient<ICustomerStationAppService, CustomerStationAppServiceMock>();
            services.AddTransient<ILocationAppService, LocationAppService>();

            services.AddScoped<IApplicationUser>(a => new ApplicationUserMock());
            var serviceProvider = services.BuildServiceProvider();

            ServiceLocator.SetLocatorProvider(serviceProvider);

            return services.BuildServiceProvider();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app)
        {
            // Configura o uso do teste
            app.UseTnfAspNetCoreSetupTest();

            app.UseMvc(routes =>
            {
                routes.MapRoute("default", "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
