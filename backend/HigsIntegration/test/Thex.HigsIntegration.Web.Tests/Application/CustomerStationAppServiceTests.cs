﻿using Shouldly;
using System;
using System.Collections.Generic;
using System.Text;
using Tnf.AspNetCore.TestBase;
using Tnf.Notifications;
using Xunit;
using Microsoft.Extensions.DependencyInjection;
using Thex.HigsIntegration.Infra.Interfaces.ReadRepositories;
using Thex.HigsIntegration.Infra.Interfaces;
using Thex.HigsIntegration.Application.Adapters;
using Thex.Location.Application.Interfaces;
using Tnf.Repositories.Uow;
using NSubstitute;
using System.Threading.Tasks;
using Thex.HigsIntegration.Application.Services;
using Thex.HigsIntegration.Infra.Entities;
using Thex.HigsIntegration.Web.Tests.Mocks;
using Thex.Common.Dto;
using Tnf.Dto;
using Thex.HigsIntegration.Dto.Dto.GetAll;
using Thex.HigsIntegration.Dto.Dto;
using Thex.Kernel;

namespace Thex.HigsIntegration.Web.Tests.Application
{
    public class CustomerStationAppServiceTests : TnfAspNetCoreIntegratedTestBase<StartupUnitTest>
    {
        public INotificationHandler _notificationHandler;

        public CustomerStationAppServiceTests()
        {
            _notificationHandler = new NotificationHandler(ServiceProvider);
        }

        [Fact]
        public void Should_Resolve_All()
        {
            TnfSession.ShouldNotBeNull();
            ServiceProvider.GetService<INotificationHandler>().ShouldNotBeNull();
            ServiceProvider.GetService<ICustomerStationRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<ICustomerStationReadRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<ICustomerStationAdapter>().ShouldNotBeNull();
            ServiceProvider.GetService<IPersonRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<IPersonAdapter>().ShouldNotBeNull();
            ServiceProvider.GetService<IContactInformationAdapter>().ShouldNotBeNull();
            ServiceProvider.GetService<ILocationAppService>().ShouldNotBeNull();
            ServiceProvider.GetService<IContactInformationRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<IContactInformationReadRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<ICustomerStationContactPersonRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<IPersonReadRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<IOccupationReadRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<ICustomerStationContactPersonReadRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<IApplicationUser>().ShouldNotBeNull();
            ServiceProvider.GetService<IUnitOfWorkManager>().ShouldNotBeNull();
        }

        [Fact]
        public async void Should_Create()
        {
            var customerStationReadRepository = Substitute.For<ICustomerStationReadRepository>();
            var customerStationRepository = Substitute.For<ICustomerStationRepository>();
            var customerStationAdapter = Substitute.For<ICustomerStationAdapter>();
            var locationAppService = Substitute.For<ILocationAppService>();
            var personAdapter = Substitute.For<IPersonAdapter>();
            var personRepository = Substitute.For<IPersonRepository>();
            var unitOfWork = ServiceProvider.GetService<IUnitOfWorkManager>();
            var contactInformationRepository = Substitute.For<IContactInformationRepository>();
            var occupationReadRepository = Substitute.For<IOccupationReadRepository>();
            var personReadRepository = Substitute.For<IPersonReadRepository>();
            var contactInformationReadRepository = Substitute.For<IContactInformationReadRepository>();
            var customerStationContactPersonReadRepository = Substitute.For<ICustomerStationContactPersonReadRepository>();
            var applicationUser = ServiceProvider.GetService<IApplicationUser>();
            var customerStationContactRepository = Substitute.For<ICustomerStationContactPersonRepository>();

            customerStationRepository.InsertAsync(new BaseEntity()).ReturnsForAnyArgs(x =>
            {
                return CustomerStationMock.GetDto();
            });

            customerStationAdapter.Map(CustomerStationMock.GetDto()).ReturnsForAnyArgs(x =>
            {
                return CustomerStation.Create(_notificationHandler)
                .WithId(Guid.NewGuid())
                .WithCustomerStationName("Posto de cliente")
                .WithSocialReason("Posto de cliente")
                .WithCompanyClientId(Guid.NewGuid());
            });

            personAdapter.Map(new PersonDto()).ReturnsForAnyArgs(x =>
            {
                return Person.Create(_notificationHandler)
               .WithFirstName("João")
               .WithLastName("da Silva")
               .WithFullName("João da Silva")
               .WithId(Guid.NewGuid())
               .WithPersonType("N");
            });

            customerStationReadRepository.GetCustomerStationDtoById(Guid.NewGuid()).ReturnsForAnyArgs(x =>
            {
                return CustomerStationMock.GetDto();
            });

            customerStationContactPersonReadRepository.GetAllByCustomerStationId(Guid.NewGuid()).ReturnsForAnyArgs(x =>
            {
                return CustomerStationMock.GetListCustomerStationContactPerson(_notificationHandler);
            });

            personReadRepository.GetById(Guid.NewGuid()).ReturnsForAnyArgs(x =>
            {
                return CustomerStationMock.GetPerson(_notificationHandler);
            });

            contactInformationReadRepository.GetContactInformationsByPerson(Guid.NewGuid()).ReturnsForAnyArgs(x =>
            {
                return CustomerStationMock.GetListContactInformations(_notificationHandler);
            });

            occupationReadRepository.GetById(1).ReturnsForAnyArgs(x =>
            {
                return CustomerStationMock.GetOccupation(_notificationHandler);
            });

            locationAppService.GetAllByOwnerId(Guid.NewGuid(), "pt-BR").ReturnsForAnyArgs(x =>
            {
                return new List<LocationDto>();
            });

            await new CustomerStationAppService(_notificationHandler,
                                                    customerStationRepository,
                                                    customerStationReadRepository,
                                                    customerStationAdapter, personRepository, personAdapter, null, locationAppService,
                                                    contactInformationRepository, contactInformationReadRepository, customerStationContactRepository, personReadRepository,
                                                    occupationReadRepository, customerStationContactPersonReadRepository, applicationUser, unitOfWork)
                                                    .CreateAsync(CustomerStationMock.GetDto());

            Assert.False(_notificationHandler.HasNotification());
        }

        [Fact]
        public async void Should_Update()
        {
            var customerStationReadRepository = Substitute.For<ICustomerStationReadRepository>();
            var customerStationRepository = Substitute.For<ICustomerStationRepository>();
            var customerStationAdapter = Substitute.For<ICustomerStationAdapter>();
            var locationAppService = Substitute.For<ILocationAppService>();
            var personAdapter = Substitute.For<IPersonAdapter>();
            var personRepository = Substitute.For<IPersonRepository>();
            var unitOfWork = ServiceProvider.GetService<IUnitOfWorkManager>();
            var contactInformationRepository = Substitute.For<IContactInformationRepository>();
            var occupationReadRepository = Substitute.For<IOccupationReadRepository>();
            var personReadRepository = Substitute.For<IPersonReadRepository>();
            var contactInformationReadRepository = Substitute.For<IContactInformationReadRepository>();
            var customerStationContactPersonReadRepository = Substitute.For<ICustomerStationContactPersonReadRepository>();
            var applicationUser = ServiceProvider.GetService<IApplicationUser>();
            var customerStationContactRepository = Substitute.For<ICustomerStationContactPersonRepository>();

            customerStationRepository.InsertAsync(new BaseEntity()).ReturnsForAnyArgs(x =>
            {
                return CustomerStationMock.GetDto();
            });

            customerStationAdapter.Map(CustomerStationMock.GetDto()).ReturnsForAnyArgs(x =>
            {
                return CustomerStation.Create(_notificationHandler)
                .WithId(Guid.NewGuid())
                .WithCustomerStationName("Posto de cliente")
                .WithSocialReason("Posto de cliente")
                .WithCompanyClientId(Guid.NewGuid());
            });

            customerStationRepository.UpdateAsync(Guid.NewGuid(), new BaseEntity()).ReturnsForAnyArgs(x =>
            {
                return CustomerStationMock.GetDto();
            });

            personAdapter.Map(new PersonDto()).ReturnsForAnyArgs(x =>
            {
                return Person.Create(_notificationHandler)
               .WithFirstName("João")
               .WithLastName("da Silva")
               .WithFullName("João da Silva")
               .WithId(Guid.NewGuid())
               .WithPersonType("N");
            });

            customerStationReadRepository.GetCustomerStationDtoById(Guid.NewGuid()).ReturnsForAnyArgs(x =>
            {
                return CustomerStationMock.GetDto();
            });

            customerStationContactPersonReadRepository.GetAllByCustomerStationId(Guid.NewGuid()).ReturnsForAnyArgs(x =>
            {
                return CustomerStationMock.GetListCustomerStationContactPerson(_notificationHandler);
            });

            personReadRepository.GetById(Guid.NewGuid()).ReturnsForAnyArgs(x =>
            {
                return CustomerStationMock.GetPerson(_notificationHandler);
            });

            contactInformationReadRepository.GetContactInformationsByPerson(Guid.NewGuid()).ReturnsForAnyArgs(x =>
            {
                return CustomerStationMock.GetListContactInformations(_notificationHandler);
            });

            occupationReadRepository.GetById(1).ReturnsForAnyArgs(x =>
            {
                return CustomerStationMock.GetOccupation(_notificationHandler);
            });

            locationAppService.GetAllByOwnerId(Guid.NewGuid(), "pt-BR").ReturnsForAnyArgs(x =>
            {
                return new List<LocationDto>();
            });

            await new CustomerStationAppService(_notificationHandler,
                                                    customerStationRepository,
                                                    customerStationReadRepository,
                                                    customerStationAdapter, personRepository, personAdapter, null, locationAppService,
                                                    contactInformationRepository, contactInformationReadRepository, customerStationContactRepository, personReadRepository,
                                                    occupationReadRepository, customerStationContactPersonReadRepository, applicationUser, unitOfWork)
                                                    .UpdateAsync(Guid.NewGuid(), CustomerStationMock.GetDto());

            Assert.False(_notificationHandler.HasNotification());
        }

        [Fact]
        public async void Should_Delete()
        {
            var customerStationReadRepository = Substitute.For<ICustomerStationReadRepository>();
            var customerStationRepository = Substitute.For<ICustomerStationRepository>();
            var customerStationAdapter = Substitute.For<ICustomerStationAdapter>();
            var unitOfWork = ServiceProvider.GetService<IUnitOfWorkManager>();

            customerStationAdapter.Map(CustomerStationMock.GetDto()).ReturnsForAnyArgs(x =>
            {
                return CustomerStation.Create(_notificationHandler)
                .WithId(Guid.NewGuid())
                .WithCustomerStationName("Posto de cliente")
                .WithSocialReason("Posto de cliente")
                .WithCompanyClientId(Guid.NewGuid());
            });

            customerStationRepository.RemoveAsync(Guid.NewGuid()).ReturnsForAnyArgs(x =>
            {
                return Task.CompletedTask;
            });

            customerStationReadRepository.GetCustomerStationDtoById(Guid.NewGuid()).ReturnsForAnyArgs(x =>
            {
                return CustomerStationMock.GetDto();
            });

           await new CustomerStationAppService(_notificationHandler,
                                                    customerStationRepository,
                                                    customerStationReadRepository,
                                                    customerStationAdapter, null, null, null, null,
                                                    null, null, null, null,
                                                    null, null, null, unitOfWork)
                                                    .DeleteAsync(Guid.Parse("7d220627-853d-466f-835f-a06b6d6b1922"));

            Assert.False(_notificationHandler.HasNotification());
        }

        [Fact]
        public async void Should_Toggle()
        {
            var customerStationReadRepository = Substitute.For<ICustomerStationReadRepository>();
            var customerStationRepository = Substitute.For<ICustomerStationRepository>();
   
            customerStationRepository.ToggleAsync(Guid.NewGuid()).ReturnsForAnyArgs(x =>
            {
                return Task.CompletedTask;
            });

            await new CustomerStationAppService(_notificationHandler,
                                                    customerStationRepository,
                                                    customerStationReadRepository,
                                                    null, null, null, null, null,
                                                    null, null, null, null,
                                                    null, null, null, null)
                                                    .ToogleAsync(Guid.Parse("7d220627-853d-466f-835f-a06b6d6b1922"));

            Assert.False(_notificationHandler.HasNotification());
        }

        [Fact]
        public async void Should_GetById()
        {
            var customerStationReadRepository = Substitute.For<ICustomerStationReadRepository>();
            var locationAppService = Substitute.For<ILocationAppService>();
            var occupationReadRepository = Substitute.For<IOccupationReadRepository>();
            var personReadRepository = Substitute.For<IPersonReadRepository>();
            var contactInformationReadRepository = Substitute.For<IContactInformationReadRepository>();
            var customerStationContactPersonReadRepository = Substitute.For<ICustomerStationContactPersonReadRepository>();
            var unitOfWork = ServiceProvider.GetService<IUnitOfWorkManager>();
            var applicationUser = ServiceProvider.GetService<IApplicationUser>();

            customerStationReadRepository.GetCustomerStationDtoById(Guid.NewGuid()).ReturnsForAnyArgs(x =>
            {
                return CustomerStationMock.GetDto();
            });

            customerStationContactPersonReadRepository.GetAllByCustomerStationId(Guid.NewGuid()).ReturnsForAnyArgs(x =>
            {
                return CustomerStationMock.GetListCustomerStationContactPerson(_notificationHandler);
            });

            personReadRepository.GetById(Guid.NewGuid()).ReturnsForAnyArgs(x =>
            {
                return CustomerStationMock.GetPerson(_notificationHandler);
            });

            contactInformationReadRepository.GetContactInformationsByPerson(Guid.NewGuid()).ReturnsForAnyArgs(x =>
            {
                return CustomerStationMock.GetListContactInformations(_notificationHandler);
            });

            occupationReadRepository.GetById(1).ReturnsForAnyArgs(x =>
            {
                return CustomerStationMock.GetOccupation(_notificationHandler);
            });

            locationAppService.GetAllByOwnerId(Guid.NewGuid(), "pt-BR").ReturnsForAnyArgs(x =>
            {
                return new List<LocationDto>();
            });

            await new CustomerStationAppService(_notificationHandler,
                                        null,
                                        customerStationReadRepository,
                                        null, null, null, null, locationAppService,
                                        null, contactInformationReadRepository, null, personReadRepository,
                                        occupationReadRepository, customerStationContactPersonReadRepository, applicationUser, unitOfWork)
                                        .GetCustomerStationDtoById(Guid.Parse("7d220627-853d-466f-835f-a06b6d6b1922"));

            Assert.False(_notificationHandler.HasNotification());
        }

        [Fact]
        public async void Should_GetAll()
        {
            var customerStationReadRepository = Substitute.For<ICustomerStationReadRepository>();
            var locationAppService = Substitute.For<ILocationAppService>();
            var occupationReadRepository = Substitute.For<IOccupationReadRepository>();
            var personReadRepository = Substitute.For<IPersonReadRepository>();
            var contactInformationReadRepository = Substitute.For<IContactInformationReadRepository>();
            var customerStationContactPersonReadRepository = Substitute.For<ICustomerStationContactPersonReadRepository>();
            var unitOfWork = ServiceProvider.GetService<IUnitOfWorkManager>();
            var applicationUser = ServiceProvider.GetService<IApplicationUser>();

            customerStationReadRepository.GetCustomerStationDtoById(Guid.NewGuid()).ReturnsForAnyArgs(x =>
            {
                return CustomerStationMock.GetDto();
            });

            customerStationContactPersonReadRepository.GetAllByCustomerStationId(Guid.NewGuid()).ReturnsForAnyArgs(x =>
            {
                return CustomerStationMock.GetListCustomerStationContactPerson(_notificationHandler);
            });

            personReadRepository.GetById(Guid.NewGuid()).ReturnsForAnyArgs(x =>
            {
                return CustomerStationMock.GetPerson(_notificationHandler);
            });

            contactInformationReadRepository.GetContactInformationsByPerson(Guid.NewGuid()).ReturnsForAnyArgs(x =>
            {
                return CustomerStationMock.GetListContactInformations(_notificationHandler);
            });

            occupationReadRepository.GetById(1).ReturnsForAnyArgs(x =>
            {
                return CustomerStationMock.GetOccupation(_notificationHandler);
            });

            locationAppService.GetAllByOwnerId(Guid.NewGuid(), "pt-BR").ReturnsForAnyArgs(x =>
            {
                return new List<LocationDto>();
            });

            await new CustomerStationAppService(_notificationHandler,
                                        null,
                                        customerStationReadRepository,
                                        null, null, null, null, locationAppService,
                                        null, contactInformationReadRepository, null, personReadRepository,
                                        occupationReadRepository, customerStationContactPersonReadRepository, applicationUser, unitOfWork)
                                        .GetAllCustomerStationDto(new GetAllCustomerStationDto());

            Assert.False(_notificationHandler.HasNotification());
        }

        [Fact]
        public async void Should_GetAllByCompanyClient()
        {
            var customerStationReadRepository = Substitute.For<ICustomerStationReadRepository>();
            var locationAppService = Substitute.For<ILocationAppService>();
            var occupationReadRepository = Substitute.For<IOccupationReadRepository>();
            var personReadRepository = Substitute.For<IPersonReadRepository>();
            var contactInformationReadRepository = Substitute.For<IContactInformationReadRepository>();
            var customerStationContactPersonReadRepository = Substitute.For<ICustomerStationContactPersonReadRepository>();
            var unitOfWork = ServiceProvider.GetService<IUnitOfWorkManager>();
            var applicationUser = ServiceProvider.GetService<IApplicationUser>();

            customerStationReadRepository.GetCustomerStationDtoById(Guid.NewGuid()).ReturnsForAnyArgs(x =>
            {
                return CustomerStationMock.GetDto();
            });

            customerStationContactPersonReadRepository.GetAllByCustomerStationId(Guid.NewGuid()).ReturnsForAnyArgs(x =>
            {
                return CustomerStationMock.GetListCustomerStationContactPerson(_notificationHandler);
            });

            personReadRepository.GetById(Guid.NewGuid()).ReturnsForAnyArgs(x =>
            {
                return CustomerStationMock.GetPerson(_notificationHandler);
            });

            contactInformationReadRepository.GetContactInformationsByPerson(Guid.NewGuid()).ReturnsForAnyArgs(x =>
            {
                return CustomerStationMock.GetListContactInformations(_notificationHandler);
            });

            occupationReadRepository.GetById(1).ReturnsForAnyArgs(x =>
            {
                return CustomerStationMock.GetOccupation(_notificationHandler);
            });

            locationAppService.GetAllByOwnerId(Guid.NewGuid(), "pt-BR").ReturnsForAnyArgs(x =>
            {
                return new List<LocationDto>();
            });

            await new CustomerStationAppService(_notificationHandler,
                                        null,
                                        customerStationReadRepository,
                                        null, null, null, null, locationAppService,
                                        null, contactInformationReadRepository, null, personReadRepository,
                                        occupationReadRepository, customerStationContactPersonReadRepository, applicationUser, unitOfWork)
                                        .GetAllDtoByCompanyClient(Guid.NewGuid());

            Assert.False(_notificationHandler.HasNotification());
        }
    }
}
