﻿
using Shouldly;
using Thex.HigsIntegration.Application.Interfaces;
using Thex.HigsIntegration.Web.Controllers;
using Tnf.AspNetCore.TestBase;
using Xunit;
using Microsoft.Extensions.DependencyInjection;
using System.Threading.Tasks;
using Thex.HigsIntegration.Dto.Dto;
using System.Collections.Generic;
using Thex.HigsIntegration.Web.Tests.Mocks;

namespace Thex.HigsIntegration.Web.Tests.Controllers
{
    public class CustomerStationControllerTests : TnfAspNetCoreIntegratedTestBase<StartupControllerTest>
    {
        [Fact]
        public void Should_Resolve_All()
        {
            TnfSession.ShouldNotBeNull();
            ServiceProvider.GetService<CustomerStationController>().ShouldNotBeNull();
            ServiceProvider.GetService<ICustomerStationAppService>().ShouldNotBeNull();
        }

        [Fact]
        public async Task Should_Post()
        {
            var customerStation = await PostResponseAsObjectAsync<CustomerStationDto, CustomerStationDto>(
            $"{RouteConsts.CustomerStationRouteName}", CustomerStationMock.GetDto());

            Assert.NotNull(customerStation);
        }

        [Fact]
        public async Task Should_GetAll()
        {
            var response = await GetResponseAsObjectAsync<List<CustomerStationDto>>(
                $"{RouteConsts.CustomerStationRouteName}");

            Assert.Equal(3, response.Count);
        }

        [Fact]
        public async Task Should_GetById()
        {
            var response = await GetResponseAsObjectAsync<CustomerStationDto>(
                $"{RouteConsts.CustomerStationRouteName}/{"23eb803c-726a-4c7c-b25b-2c22a56793d9"}");

            Assert.NotNull(response);
        }

        [Fact]
        public async Task Should_Put()
        {
            var billingAccountItem = await PutResponseAsObjectAsync<CustomerStationDto, CustomerStationDto>(
            $"{RouteConsts.CustomerStationRouteName}/{"50a66538-dd94-4162-a1ed-83fc8311e574"}", CustomerStationMock.GetDto());

            Assert.NotNull(billingAccountItem);
        }

        [Fact]
        public Task Should_Delete()
        {
            return DeleteResponseAsync(
                $"{RouteConsts.CustomerStationRouteName}/{"c3e5678d-815f-42af-bad0-c826d9bc7790"}"
            );
        }

        [Fact]
        public Task Should_Toggle()
        {
            return PatchResponseAsync(
                $"{RouteConsts.CustomerStationRouteName}/{"c3e5678d-815f-42af-bad0-c826d9bc7790"}/{"toogle"}",
                null
            );
        }
    }
}
