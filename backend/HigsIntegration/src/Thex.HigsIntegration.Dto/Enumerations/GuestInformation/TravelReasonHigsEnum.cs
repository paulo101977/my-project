﻿namespace Thex.HigsIntegration.Dto.Enumerations.GuestInformation
{
    public enum TravelReasonHigsEnum
    {
        None = 0,
        Tourism = 1,
        Business = 2,
        Leisure = 3,
        Convention = 4,
        Other = 5
    }

}
