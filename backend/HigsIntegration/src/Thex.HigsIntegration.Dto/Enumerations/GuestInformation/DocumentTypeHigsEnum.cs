﻿namespace Thex.HigsIntegration.Dto.Enumerations.GuestInformation
{
    public enum DocumentTypeHigsEnum
    {
        None = 0,
	    CPF = 1,
	    CivilIdentification = 2,
	    BirthCertificate = 3,
	    Passport = 4
    }       
}
