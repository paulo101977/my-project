﻿namespace Thex.HigsIntegration.Dto.Enumerations.GuestInformation
{
    public enum TransportTypeHigsEnum
    {
        None = 0,
        Airplane = 1,
        Ship = 2,
        Car = 3,
        Other = 4
    }

}
