﻿
namespace Thex.HigsIntegration.Dto.Enumerations
{
    public enum CustomerStationEnum
    {
        CustomerStationMustHaveCustomerStationName = 1,
        CustomerStationMustHaveSocialReason = 2,
        CustomerStationMustHaveAffiliateCattegoryId = 3,
        CustomerStationMustHaveCompanyClientId = 4
    }
}
