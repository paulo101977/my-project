﻿using System;

namespace Thex.HigsIntegration.Dto.Dto
{
    public class IntegrationPartnerPropertyDto
    {
        public Guid Id { get; set; }
        public int PropertyId { get; set; }
        public int? IsActive { get; set; }
        public string IntegrationCode { get; set; }
        public int PartnerId { get; set; }
        public Guid TenantId { get; set; }
    }
}
