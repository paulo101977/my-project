﻿namespace Thex.HigsIntegration.Dto.Dto.RatePlanList
{
    public class AdditionalGuestAmountHigsDto
    {
        public int AgeQualifyingCode { get; set; }
        public double Amount { get; set; }
    }
}
