﻿using Thex.HigsIntegration.Dto.Enumerations.RatePlanList;

namespace Thex.HigsIntegration.Dto.Dto.RatePlanList
{
    public class MealsIncludedHigsDto
    {
        public string MealPlanIndicator { get; set; }
        public MealPlanHigsEnum MealPlanCode { get; set; }
    }
}
