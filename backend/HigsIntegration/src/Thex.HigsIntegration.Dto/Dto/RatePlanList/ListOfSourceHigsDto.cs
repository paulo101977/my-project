﻿using System.Collections.Generic;

namespace Thex.HigsIntegration.Dto.Dto.RatePlanList
{
    public class ListOfSourceHigsDto
    {
        public string SourceOfBusiness { get; set; }
        public List<ListCompanyHigsDto> ListCompanyID { get; set; }
    }
}
