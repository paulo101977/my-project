﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Thex.HigsIntegration.Dto.Dto.RatePlanList
{
    public class RatePlanListDto
    {
        [JsonProperty(PropertyName = "ResponseList")]
        public List<RatePlanAndRateAmountHigsDto> RateplanList { get; set; }
        public string Page { get; set; }
        public bool NextPage { get; set; }
    }
}
