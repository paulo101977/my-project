﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Thex.HigsIntegration.Dto.Dto.RatePlanList
{
    public class BeforeArrivalHigsDto
    {
        public object TimeUnit { get; set; }
        public int Value { get; set; }
    }
}
