﻿using System.Collections.Generic;

namespace Thex.HigsIntegration.Dto.Dto.RatePlanList
{
    public class RateHigsDto
    {
        public List<BaseByGuestAmtHigsDto> BaseByGuestAmts { get; set; }
    }
}
