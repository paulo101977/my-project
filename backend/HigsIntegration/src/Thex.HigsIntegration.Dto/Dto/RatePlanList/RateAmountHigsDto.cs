﻿using System.Collections.Generic;

namespace Thex.HigsIntegration.Dto.Dto.RatePlanList
{
    public class RateAmountHigsDto
    {
        public int HotelCode { get; set; }
        public StatusApplicationControlHigsDto StatusApplicationControl { get; set; }
        public RateHigsDto Rate { get; set; }
        public List<AdditionalGuestAmountHigsDto> AdditionalGuestAmounts { get; set; }
        public string ID { get; set; }
    }
}
