﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Thex.HigsIntegration.Dto.Dto.RatePlanList
{
    public class CancellationPolicyHigsDto
    {
        public object TimeUnit { get; set; }
        public int Value { get; set; }
    }
}
