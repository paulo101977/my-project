﻿namespace Thex.HigsIntegration.Dto.Dto.RatePlanList
{
    public class BaseByGuestAmtHigsDto
    {
        public int AgeQualifyingCode { get; set; }
        public int NumberOfGuests { get; set; }
        public double AmountAfterTax { get; set; }
    }
}
