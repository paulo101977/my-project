﻿namespace Thex.HigsIntegration.Dto.Dto.RatePlanList
{
    public class StatusApplicationControlHigsDto
    {
        public string Start { get; set; }
        public string End { get; set; }
        public string RatePlanCode { get; set; }
        public string InvTypeCode { get; set; }
        public int Mon { get; set; }
        public int Tue { get; set; }
        public int Wed { get; set; }
        public int Thu { get; set; }
        public int Fri { get; set; }
        public int Sat { get; set; }
        public int Sun { get; set; }
    }
}
