﻿using System;
using System.Collections.Generic;

namespace Thex.HigsIntegration.Dto.Dto.RatePlanList
{
    public class RatePlanHigsDto
    {
        public int HotelCode { get; set; }
        public string LocatorId { get; set; }
        public string RatePlanCode { get; set; }
        public List<ListOfSourceHigsDto> ListOfSource { get; set; }
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
        public string CurrencyCode { get; set; }
        public bool IsCommissionable { get; set; }
        public int TypeComission { get; set; }
        public double ValueComission { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public MealsIncludedHigsDto MealsIncluded { get; set; }
        public List<SellableProductHigsDto> SellableProduct { get; set; }
        public bool IsNew { get; set; }
        public bool Active { get; set; }
        public List<object> Partners { get; set; }
        public string BillingPolicy { get; set; }
        public CancellationPolicyHigsDto CancellationPolicy { get; set; }
        public BeforeArrivalHigsDto BeforeArrival { get; set; }
        public string ID { get; set; }
    }
}
