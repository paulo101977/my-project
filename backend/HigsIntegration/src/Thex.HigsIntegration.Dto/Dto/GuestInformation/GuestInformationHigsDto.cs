﻿using Thex.HigsIntegration.Dto.Enumerations.GuestInformation;

namespace Thex.HigsIntegration.Dto.Dto.GuestInformation
{
    public class GuestInformationHigsDto
    {
        public string LocatorId { get; set; }
        public int HotelCode { get; set; }
        public GuestInfoHigsDto Guest { get; set; }
        public ReservationIdsHigsDto ReservationIds { get; set; }
        public string GivenName { get; set; }
        public string SurName { get; set; }
        public string Email { get; set; }
        public string Profession { get; set; }
        public string BirthDate { get; set; }
        public string Gender { get; set; }
        public DocumentTypeHigsEnum DocumentType { get; set; }
        public string DocumentNumber { get; set; }
        public GuestAddressHigsDto GuestAddress { get; set; }
        public string Nationality { get; set; }
        public string PhoneNumber { get; set; }
        public string CelPhoneNumber { get; set; }
        public string LastSource { get; set; }
        public string NextStop { get; set; }
        public TravelReasonHigsEnum TravelReason { get; set; }
        public TransportTypeHigsEnum Transport { get; set; }
        public string Checkin { get; set; }
        public string Checkout { get; set; }
    }
}
