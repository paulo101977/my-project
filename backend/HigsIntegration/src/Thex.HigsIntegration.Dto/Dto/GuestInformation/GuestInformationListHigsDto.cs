﻿using System.Collections.Generic;

namespace Thex.HigsIntegration.Dto.Dto.GuestInformation
{
    public class GuestInformationListHigsDto : HigsPaginationDto
    {
        public List<GuestInformationHigsDto> ResponseList { get; set; }
    }
}
