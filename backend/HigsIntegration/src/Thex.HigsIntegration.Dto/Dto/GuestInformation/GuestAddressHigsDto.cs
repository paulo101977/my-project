﻿namespace Thex.HigsIntegration.Dto.Dto.GuestInformation
{
    public class GuestAddressHigsDto
    {
        public string StreetName { get; set; }
        public string AddressNumber { get; set; }
        public string Complement { get; set; }
        public string PostalCode { get; set; }
        public string District { get; set; }
        public string CityName { get; set; }
        public string CityCode { get; set; }
        public string StateName { get; set; }
        public string StateCode { get; set; }
        public string CountryName { get; set; }
        public string CountryCode { get; set; }
    }
}
