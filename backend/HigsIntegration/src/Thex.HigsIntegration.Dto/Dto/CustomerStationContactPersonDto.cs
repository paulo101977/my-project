﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Thex.HigsIntegration.Dto.Dto
{
    public class CustomerStationContactPersonDto
    {
        public Guid Id { get; set; }
        public Guid PersonId { get; set; }
        public string OccupationName { get; set; }
        public string Name { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public Guid CustomerStationId { get; set; }
        public int? OccupationId { get; set; }
    }
}
