﻿using System;

namespace Thex.HigsIntegration.Dto.Dto
{
    public class ContactInformationDto
    {
        public Guid OwnerId { get; set; }
        public int ContactInformationTypeId { get; set; }
        public string Information { get; set; }
    }
}
