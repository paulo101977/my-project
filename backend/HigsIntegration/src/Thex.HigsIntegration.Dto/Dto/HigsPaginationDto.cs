﻿namespace Thex.HigsIntegration.Dto.Dto
{
    public class HigsPaginationDto
    {
        public bool NextPage { get; set; }
        public string Page { get; set; }
    }
}
