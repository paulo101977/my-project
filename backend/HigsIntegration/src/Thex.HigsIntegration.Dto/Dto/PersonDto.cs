﻿using System;

namespace Thex.HigsIntegration.Dto.Dto
{
    public class PersonDto
    {
        public Guid Id { get; set; }
        public string PersonType { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FullName { get; set; }
        public int? CountrySubdivisionId { get; set; }
    }
}
