﻿using System;
using System.Collections.Generic;
using Thex.Common.Dto;
using Tnf.Dto;

namespace Thex.HigsIntegration.Dto.Dto
{
    public class CustomerStationDto : BaseDto, IDto
    {
        public CustomerStationDto()
        {
            this.LocationList = new List<LocationDto>();
            this.CustomerStationContactPersonList = new List<CustomerStationContactPersonDto>();
        }

        public Guid Id { get; set; }
        public string CustomerStationName { get; set; }
        public string SocialReason { get; set; }
        public Guid? CustomerStationCategoryId { get; set; }
        public string CustomerStationCategoryName { get; set; }
        public bool IsActive { get; set; }
        public Guid CompanyClientId { get; set; }
        public string CompanyClientName { get; set; }
        public string HomePage { get; set; }

        public IList<LocationDto> LocationList { get; set; }
        public ICollection<CustomerStationContactPersonDto> CustomerStationContactPersonList { get; set; }
    }
}
