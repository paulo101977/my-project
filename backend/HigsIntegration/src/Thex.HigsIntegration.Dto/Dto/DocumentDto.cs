﻿
using System;

namespace Thex.HigsIntegration.Dto.Dto
{
    public class DocumentDto
    {
        public int Id { get; set; }
        public static DocumentDto NullInstance = null;

        public Guid OwnerId { get; set; }
        public int DocumentTypeId { get; set; }
        public string DocumentInformation { get; set; }
        public byte[] Value { get; set; }
    }
}
