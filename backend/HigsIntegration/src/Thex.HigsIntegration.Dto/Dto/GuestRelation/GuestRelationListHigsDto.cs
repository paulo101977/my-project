﻿using System.Collections.Generic;

namespace Thex.HigsIntegration.Dto.Dto.GuestRelation
{
    public class GuestRelationListHigsDto : HigsPaginationDto
    {
        public List<GuestRelationHigsDto> GuestRelations { get; set; }
    }}
