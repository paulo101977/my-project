﻿namespace Thex.HigsIntegration.Dto.Dto.GuestRelation
{
    public class GuestInfoRelationHigsDto
    {
        public int Code { get; set; }
        public string Name { get; set; }
    }
}
