﻿namespace Thex.HigsIntegration.Dto.Dto.GuestRelation
{
    public class GuestRelationHigsDto
    {
        public string LocatorId { get; set; }
        public int HotelCode { get; set; }
        public string RoomNumber { get; set; }
        public string Checkin { get; set; }
        public string Checkout { get; set; }
        public string EffectiveCheckin { get; set; }
        public string EffectiveCheckout { get; set; }
        public GuestInfoRelationHigsDto Guest { get; set; }
        public ReservationIdsHigsDto ReservationIds { get; set; }
    }
}
