﻿using Tnf.Dto;

namespace Thex.HigsIntegration.Dto.Dto.GetAll
{
    public class GetAllGuestBaseDto : RequestAllDto
    {
        public string HotelCode { get; set; }
        public string SourceReservationNumber { get; set; }
        public string GuestCod { get; set; }
        public string RoomNumber { get; set; }

        public bool HasSearchFilter()
         => (HotelCode ?? SourceReservationNumber ?? GuestCod ?? RoomNumber) != null;

        public object GetGuestRelationCriteriaHigs()
            => new
            {
                SelectionCriteria = new
                {
                    HotelCode,
                    SourceReservationNumber,
                    GuestCod,
                    RoomNumber,
                    Page
                }
            };

        public object GuestUrlParamHigs()
            => $"?hotelCode={HotelCode}&SourceReservationNumber={SourceReservationNumber}&GuestCode={GuestCod}&RoomCode={RoomNumber}&Page={Page}";

    }
}
