﻿
using Microsoft.Extensions.Configuration;
using Thex.HigsIntegration.Application.Adapters;
using Thex.HigsIntegration.Application.Interfaces;
using Thex.HigsIntegration.Application.Jobs;
using Thex.HigsIntegration.Application.Services;
using Thex.Location.Application.Interfaces;
using Thex.Location.Application.Services;
using Thex.Log;

namespace Microsoft.Extensions.DependencyInjection
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddApplicationServiceDependency(this IServiceCollection services, IConfiguration configuration)
        {
            services
                .AddLogApplicationServiceDependency(configuration["ConnectionStrings:MongoDb:Log:MongoDBServer"], configuration["ConnectionStrings:MongoDb:Log:MongoDBName"])
                .AddInfraDependency();

            services.AddTransient<ICustomerStationAdapter, CustomerStationAdapter>();
            services.AddTransient<ICustomerStationContactPersonAdapter, CustomerStationContactPersonAdapter>();
            services.AddTransient<IContactInformationAdapter, ContactInformationAdapter>();
            services.AddTransient<IPersonAdapter, PersonAdapter>();

            services.AddTransient<ICustomerStationAppService, CustomerStationAppService>();
            services.AddTransient<ILocationAppService, LocationAppService>();

            services.AddTransient<IReservationDownloadAppService, ReservationDownloadAppService>();
            services.AddTransient<IGuestRelationAppService, GuestRelationAppService>();
            services.AddTransient<IGuestInformationAppService, GuestInformationAppService>();
            services.AddTransient<IRatePlanListAppService, RatePlanListAppService>();

            services.AddScoped<ReservationDownloadJob>();
            services.AddScoped<RatePlanListJob>();
            services.AddScoped<GuestRelationJob>();
            services.AddScoped<GuestInformationJob>();

            return services;
        }
    }
}