﻿using System.Threading.Tasks;
using Tnf.Application.Services;

namespace Thex.HigsIntegration.Application.Interfaces
{
    public interface IRatePlanListAppService : IApplicationService
    {
        Task ProcessRatePlanListAsync();
    }
}
