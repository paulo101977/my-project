﻿using System.Threading.Tasks;
using Thex.HigsIntegration.Dto.Dto.GetAll;
using Thex.HigsIntegration.Dto.Dto.GuestInformation;
using Tnf.Application.Services;

namespace Thex.HigsIntegration.Application.Interfaces
{
    public interface IGuestInformationAppService : IApplicationService
    {
        Task<GuestInformationListHigsDto> GetAllAsync(GetAllGuestInformationDto requestDto);
        Task ProcessGuestInformationAsync();
    }
}
