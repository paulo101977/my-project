﻿using System.Threading.Tasks;

namespace Thex.HigsIntegration.Application.Interfaces.Jobs
{
    public interface IJob
    {
        Task Execute();
    }
}
