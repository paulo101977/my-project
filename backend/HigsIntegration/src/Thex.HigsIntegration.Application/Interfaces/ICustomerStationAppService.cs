﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.HigsIntegration.Dto.Dto;
using Thex.HigsIntegration.Dto.Dto.GetAll;
using Tnf.Application.Services;

namespace Thex.HigsIntegration.Application.Interfaces
{
    public interface ICustomerStationAppService : IApplicationService
    {
        Task<IList<CustomerStationDto>> GetAllCustomerStationDto(GetAllCustomerStationDto requestDto);
        Task<CustomerStationDto> GetCustomerStationDtoById(Guid id);
        Task<CustomerStationDto> CreateAsync(CustomerStationDto dto);
        Task<CustomerStationDto> UpdateAsync(Guid id, CustomerStationDto dto);
        Task<IList<CustomerStationDto>> GetAllDtoByCompanyClient(Guid id);
        Task DeleteAsync(Guid id);
        Task ToogleAsync(Guid id);
    }
}
