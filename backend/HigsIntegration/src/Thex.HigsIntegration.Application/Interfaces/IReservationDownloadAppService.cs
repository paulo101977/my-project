﻿using System.Threading.Tasks;
using Thex.Log.Dto;
using Thex.Log.Dto.ReservationDownload;
using Tnf.Application.Services;

namespace Thex.HigsIntegration.Application.Interfaces
{
    public interface IReservationDownloadAppService : IApplicationService
    {
        Task ProcessReservationDownloadAsync();
        Task<ThexListDto<ReservationDownloadResultDto>> GetAllByFiltersAsync(ReservationDownloadSearchDto request);
        Task<ThexListDto<ReservationDownloadHistoryDto>> GetAllHistoryByIdAsync(string reservDownloadId);
    }
}
