﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.HigsIntegration.Dto.Dto.GetAll;
using Thex.HigsIntegration.Dto.Dto.GuestRelation;
using Tnf.Application.Services;

namespace Thex.HigsIntegration.Application.Interfaces
{
    public interface IGuestRelationAppService : IApplicationService
    {
        Task<GuestRelationListHigsDto> GetAllAsync(GetAllGuestRelationDto requestDto);
        Task ProcessGuestRelationAsync();
    }
}
