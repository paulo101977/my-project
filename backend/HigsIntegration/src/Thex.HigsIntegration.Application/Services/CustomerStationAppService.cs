﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Thex.Common;
using Thex.Common.Enumerations;
using Thex.HigsIntegration.Application.Adapters;
using Thex.HigsIntegration.Application.Interfaces;
using Thex.HigsIntegration.Dto.Dto;
using Thex.HigsIntegration.Dto.Dto.GetAll;
using Thex.HigsIntegration.Infra.Entities;
using Thex.HigsIntegration.Infra.Interfaces;
using Thex.HigsIntegration.Infra.Interfaces.ReadRepositories;
using Thex.Kernel;
using Thex.Location.Application.Interfaces;
using Tnf.Notifications;
using Tnf.Repositories.Uow;

namespace Thex.HigsIntegration.Application.Services
{
    public class CustomerStationAppService : ApplicationServiceBase<CustomerStationDto, CustomerStation>, ICustomerStationAppService
    {
        private readonly ICustomerStationAdapter _customerStationAdapter;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly ICustomerStationRepository _customerStationRepository;
        private readonly ICustomerStationReadRepository _customerStationReadRepository;
        private readonly INotificationHandler _notificationHandler;
        private readonly ILocationAppService _locationAppService;
        private readonly IPersonAdapter _personAdapter;
        private readonly IPersonRepository _personRepository;
        private readonly ICustomerStationContactPersonRepository _customerStationContactPersonRepository;
        private readonly IContactInformationAdapter _contactInformationAdapter;
        private readonly IContactInformationRepository _contactInformationRepository;
        private readonly ICustomerStationContactPersonReadRepository _customerStationContactPersonReadRepository;
        private readonly IContactInformationReadRepository _contactInformationReadRepository;
        private readonly IPersonReadRepository _personReadRepository;
        private readonly IOccupationReadRepository _occupationReadRepository;
        private readonly IApplicationUser _applicationUser;

        public CustomerStationAppService(INotificationHandler notificationHandler,
                                ICustomerStationRepository customerStationRepository,
                                ICustomerStationReadRepository customerStationReadRepository,
                                ICustomerStationAdapter customerStationAdapter,
                                IPersonRepository personRepository,
                                IPersonAdapter personAdapter,
                                IContactInformationAdapter contactInformationAdapter,
                                ILocationAppService locationAppService,
                                IContactInformationRepository contactInformationRepository,
                                IContactInformationReadRepository contactInformationReadRepository,
                                ICustomerStationContactPersonRepository customerStationContactPersonRepository,
                                IPersonReadRepository personReadRepository,
                                IOccupationReadRepository occupationReadRepository,
                                ICustomerStationContactPersonReadRepository customerStationContactPersonReadRepository,
                                IApplicationUser applicationUser,
                                IUnitOfWorkManager unitOfWorkManager)
                                : base(notificationHandler, customerStationRepository)
        {
            _customerStationAdapter = customerStationAdapter;
            _unitOfWorkManager = unitOfWorkManager;
            _customerStationRepository = customerStationRepository;
            _customerStationReadRepository = customerStationReadRepository;
            _notificationHandler = notificationHandler;
            _locationAppService = locationAppService;
            _personAdapter = personAdapter;
            _personRepository = personRepository;
            _customerStationContactPersonRepository = customerStationContactPersonRepository;
            _contactInformationAdapter = contactInformationAdapter;
            _contactInformationRepository = contactInformationRepository;
            _customerStationContactPersonReadRepository = customerStationContactPersonReadRepository;
            _contactInformationReadRepository = contactInformationReadRepository;
            _personReadRepository = personReadRepository;
            _occupationReadRepository = occupationReadRepository;
            _applicationUser = applicationUser;
        }

        public async Task<CustomerStationDto> CreateAsync(CustomerStationDto dto)
        {
            ValidateCustomerStation(dto);
            if (_notificationHandler.HasNotification())
                return null;

            Guid customerStationId = Guid.Empty;
            using (var uow = _unitOfWorkManager.Begin())
            {
                var customerStationEntity = _customerStationAdapter.Map(dto).Build();
                if (_notificationHandler.HasNotification())
                    return null;

                var customerStationDto = await _customerStationRepository.InsertAsync(customerStationEntity);
                customerStationId = customerStationDto.MapTo<CustomerStationDto>().Id;
                dto.Id = customerStationId;

                if (_notificationHandler.HasNotification())
                    return null;

                await InsertContactPersons(dto.CustomerStationContactPersonList, customerStationId);

                if (_notificationHandler.HasNotification())
                    return null;

                uow.Complete();
            }

            _locationAppService.CreateLocationList(dto.LocationList, customerStationId);

            var result = await GetCustomerStationDtoById(customerStationId);
            result.CustomerStationContactPersonList = await GetCustomerStationContactPersonList(customerStationId);
            result.LocationList = _locationAppService.GetAllByOwnerId(customerStationId, _applicationUser.PreferredLanguage);
            return result;
        }

        private void ValidateCustomerStation(CustomerStationDto dto)
        {
            if (dto == null)
            {
                Notification.Raise(Notification
                    .DefaultBuilder
                    .WithMessage(AppConsts.LocalizationSourceName, CommonsEnum.Error.NullOrEmptyObject)
                    .Build());
            }

            if(dto != null && (dto.LocationList == null || !dto.LocationList.Any()))
            {
                Notification.Raise(Notification
                    .DefaultBuilder
                    .WithMessage(AppConsts.LocalizationSourceName, CustomerStation.EntityError.CustomerStationMustHaveAddress)
                    .Build());
            }
        }
        
        public async Task DeleteAsync(Guid id)
        {
            using (var uow = _unitOfWorkManager.Begin())
            {
                var customerStationDto = await _customerStationReadRepository.GetCustomerStationDtoById(id);
                var customerStation = _customerStationAdapter.Map(customerStationDto).Build();
                await _customerStationRepository.RemoveAsync(id);

                uow.Complete();
            }
        }

        public async Task ToogleAsync(Guid id)
        {
            await _customerStationRepository.ToggleAsync(id);
        }

        public async Task<CustomerStationDto> UpdateAsync(Guid id, CustomerStationDto dto)
        {
            ValidateCustomerStation(dto);
            if (_notificationHandler.HasNotification())
                return null;

            Guid customerStationId = Guid.Empty;

            using (var uow = _unitOfWorkManager.Begin())
            {
                var customerStationDto = await _customerStationReadRepository.GetCustomerStationDtoById(id);
                var customerStation = _customerStationAdapter.Map(dto).Build();
                if (_notificationHandler.HasNotification())
                    return null;

                var result = await _customerStationRepository.UpdateAsync(id, customerStation);
                customerStationId = result.MapTo<CustomerStationDto>().Id;

                if (_notificationHandler.HasNotification())
                    return null;

                await UpdateCustomerStationContactPerson(dto.CustomerStationContactPersonList, customerStationId);

                if (_notificationHandler.HasNotification())
                    return null;

               uow.Complete();
            }

            _locationAppService.UpdateLocationList(dto.LocationList, customerStationId.ToString());

            var customerStationDtoResult = await GetCustomerStationDtoById(customerStationId);
            customerStationDtoResult.CustomerStationContactPersonList = await GetCustomerStationContactPersonList(customerStationId);
            customerStationDtoResult.LocationList = _locationAppService.GetAllByOwnerId(customerStationId, _applicationUser.PreferredLanguage);
            return customerStationDtoResult;
        }

        public async Task<IList<CustomerStationDto>> GetAllCustomerStationDto(GetAllCustomerStationDto requestDto)
        {
            ValidateRequestAllDto(requestDto, nameof(requestDto));

            var customerStations = await _customerStationReadRepository.GetAllCustomerStationDto();
            foreach (var customerStation in customerStations)
            {
                customerStation.CustomerStationContactPersonList = await GetCustomerStationContactPersonList(customerStation.Id);
                customerStation.LocationList = _locationAppService.GetAllByOwnerId(customerStation.Id, _applicationUser.PreferredLanguage);
            }
            return customerStations;
        }

        public async Task<IList<CustomerStationDto>> GetAllDtoByCompanyClient(Guid id)
        {
            var customerStations = await _customerStationReadRepository.GetAllDtoByCompanyClient(id);
            foreach (var customerStation in customerStations)
            {
                customerStation.CustomerStationContactPersonList = await GetCustomerStationContactPersonList(customerStation.Id);
                customerStation.LocationList = _locationAppService.GetAllByOwnerId(customerStation.Id, _applicationUser.PreferredLanguage);
            }
            return customerStations;
        }

        public async Task<CustomerStationDto> GetCustomerStationDtoById(Guid id)
        {
            var customerStationDto = await _customerStationReadRepository.GetCustomerStationDtoById(id);
            if(customerStationDto == null)
            {
                Notification.Raise(Notification
                    .DefaultBuilder
                    .WithMessage(AppConsts.LocalizationSourceName, CommonsEnum.Error.NullOrEmptyObject)
                    .Build());
                 
                return null;
            }

            customerStationDto.CustomerStationContactPersonList = await GetCustomerStationContactPersonList(customerStationDto.Id);
            customerStationDto.LocationList = _locationAppService.GetAllByOwnerId(customerStationDto.Id, _applicationUser.PreferredLanguage);
            return customerStationDto;
        }

        public override CustomerStation GeneratedEntity(CustomerStationDto dto)
        {
            return _customerStationAdapter.Map(dto).Build();
        }


        #region CustomerStationContactPerson

        private async Task<IList<CustomerStationContactPersonDto>> GetCustomerStationContactPersonList(Guid customerStationId)
        {
            List<CustomerStationContactPersonDto> customerStationContactPersonDtos = new List<CustomerStationContactPersonDto>();

            var customerStationContactPersonList = await _customerStationContactPersonReadRepository.GetAllByCustomerStationId(customerStationId);
            if(customerStationContactPersonList != null && customerStationContactPersonList.Any())
            {
                foreach (var customerStationContactPerson in customerStationContactPersonList)
                {
                    var customerStationContactPersonDto = customerStationContactPerson.MapTo<CustomerStationContactPersonDto>();

                    var person = await _personReadRepository.GetById(customerStationContactPerson.PersonId);
                    if(person != null)
                    {
                        customerStationContactPersonDto.PersonId = person.Id;
                        customerStationContactPersonDto.Name = person.FullName;
                    }

                    var contactInformations = await _contactInformationReadRepository.GetContactInformationsByPerson(customerStationContactPerson.PersonId);

                    var contactInformationEmail = contactInformations.FirstOrDefault(x => x.ContactInformationTypeId == (int)ContactInformationTypeEnum.Email);
                    if (contactInformationEmail != null)
                        customerStationContactPersonDto.Email = contactInformationEmail.Information;

                    var contactInformationPhone = contactInformations.FirstOrDefault(x => x.ContactInformationTypeId == (int)ContactInformationTypeEnum.PhoneNumber);
                    if (contactInformationPhone != null)
                        customerStationContactPersonDto.PhoneNumber = contactInformationPhone.Information;

                    if (customerStationContactPersonDto.OccupationId.HasValue)
                    {
                        var occupation = await _occupationReadRepository.GetById(customerStationContactPersonDto.OccupationId.Value);
                        if (occupation != null)
                            customerStationContactPersonDto.OccupationName = occupation.Name;
                    }

                    customerStationContactPersonDtos.Add(customerStationContactPersonDto);
                }
            }

            return customerStationContactPersonDtos;
        }

        private async Task InsertContactPersons(ICollection<CustomerStationContactPersonDto> customerStationContactPersonDtos, Guid customerStationId)
        {
            IList<CustomerStationContactPerson> customerStationContactPersonList = new List<CustomerStationContactPerson>();
            IList<Person> personList = new List<Person>();

            foreach (var customerStationContactPersonDto in customerStationContactPersonDtos)
            {
                PersonDto personDto = SetFirstAndLastNameBasedOnFullName(customerStationContactPersonDto.Name);

                personDto.PersonType = ((char)PersonTypeEnum.Natural).ToString();

                Person person = _personAdapter.Map(personDto).Build();

                var customerStationContactPerson = new CustomerStationContactPerson.Builder(Notification);
                await _personRepository.Add(person);

                if (string.IsNullOrEmpty((customerStationContactPersonDto.OccupationName)))
                {
                    customerStationContactPerson
                        .WithId(Guid.NewGuid())
                        .WithPersonId(person.Id)
                        .WithCustomerStationId(customerStationId)
                        .WithOccupationId(customerStationContactPersonDto.OccupationId);
                }
                else
                {
                    customerStationContactPerson
                        .WithId(Guid.NewGuid())
                        .WithPersonId(person.Id)
                        .WithCustomerStationId(customerStationId)
                        .WithOccupationId(customerStationContactPersonDto.OccupationId);
                }

                customerStationContactPersonList.Add(customerStationContactPerson.Build());
                
                IList<ContactInformation> contactInformationList = GetContactInformationListFromDto(person.Id, customerStationContactPersonDto.PhoneNumber, customerStationContactPersonDto.Email);
                SaveContactInformationList(contactInformationList);
            }

            if (customerStationContactPersonList.Any())
                await _customerStationContactPersonRepository.InsertRangeAsync(customerStationContactPersonList);

        }

        private PersonDto SetFirstAndLastNameBasedOnFullName(string FullName)
        {

            var personDto = new PersonDto();

            if (!string.IsNullOrEmpty(FullName))
            {

                personDto.FullName = FullName;

                var separator = " ";

                var FullNameList = FullName.Split(separator).ToList();


                personDto.FirstName = FullNameList.First();


                if (FullNameList.Count() > 1)
                {
                    FullNameList.RemoveAt(0);

                    personDto.LastName = FullNameList.JoinAsString(separator);
                }
            }


            return personDto;
        }

        private IList<ContactInformation> GetContactInformationListFromDto(Guid personId, string phone, string email)
        {
            List<ContactInformation> contactInformationList = new List<ContactInformation>();

            contactInformationList.Add(new ContactInformation.Builder(Notification)
                .WithOwnerId(personId)
                .WithContactInformationTypeId((int)ContactInformationTypeEnum.PhoneNumber)
                .WithInformation(phone)
                .Build());

            contactInformationList.Add(new ContactInformation.Builder(Notification)
                .WithOwnerId(personId)
                .WithContactInformationTypeId((int)ContactInformationTypeEnum.Email)
                .WithInformation(email)
                .Build());


            return contactInformationList;
        }

        private async void SaveContactInformationList(IList<ContactInformation> contactInformationList)
        {
            if (contactInformationList.Any())
                await _contactInformationRepository.AddRangeAsync(contactInformationList);

        }

        private async Task UpdateCustomerStationContactPerson(ICollection<CustomerStationContactPersonDto> customerStationContactPersonDtoList, Guid customerStationId)
        {
            var customerStationContactPersonListForCreate = new List<CustomerStationContactPersonDto>();
            var customerStationContactPersonListForUpdate = new List<CustomerStationContactPerson>();
            var personList = new List<Person>();

            foreach (var customerStationContactPersonDto in customerStationContactPersonDtoList)
            {
                if (customerStationContactPersonDto.PersonId == Guid.Empty || string.IsNullOrEmpty(customerStationContactPersonDto.PersonId.ToString()))
                {
                    customerStationContactPersonListForCreate.Add(customerStationContactPersonDto);
                }
                else
                {
                    PersonDto personDto = SetFirstAndLastNameBasedOnFullName(customerStationContactPersonDto.Name);

                    personDto.PersonType = ((char)PersonTypeEnum.Natural).ToString();

                    personDto.Id = customerStationContactPersonDto.PersonId;

                    Person person = _personAdapter.Map(personDto).Build();

                    await _personRepository.Update(person);

                    var customerStationContactPerson = new CustomerStationContactPerson.Builder(Notification);

                    if (String.IsNullOrEmpty((customerStationContactPersonDto.OccupationName)))
                    {
                        customerStationContactPerson
                            .WithPersonId(person.Id)
                            .WithCustomerStationId(customerStationContactPersonDto.CustomerStationId)
                            .WithId(customerStationContactPersonDto.Id);
                    }
                    else
                    {
                        customerStationContactPerson
                            .WithPersonId(person.Id)
                            .WithCustomerStationId(customerStationContactPersonDto.CustomerStationId)
                            .WithOccupationId(customerStationContactPersonDto.OccupationId)
                            .WithId(customerStationContactPersonDto.Id);
                    }

                    customerStationContactPersonListForUpdate.Add(customerStationContactPerson.Build());
                    IList<ContactInformation> contactInformationList = GetContactInformationListFromDto(person.Id, customerStationContactPersonDto.PhoneNumber, customerStationContactPersonDto.Email);
                    await UpdateContactInformationList(contactInformationList, person.Id);
                }
            }

            await RemoveCustomerStationContactPersons(customerStationContactPersonListForUpdate, customerStationId);

            if (customerStationContactPersonListForCreate.Any())
                await InsertContactPersons(customerStationContactPersonListForCreate, customerStationId);
        }

        private async Task RemoveCustomerStationContactPersons(ICollection<CustomerStationContactPerson> customerStationContactPersonList, Guid customerStationId)
        {
            var customerStationContactInformationForExclude = await _customerStationContactPersonReadRepository.GetAllForExclude(customerStationContactPersonList, customerStationId);
            if (customerStationContactInformationForExclude != null && customerStationContactInformationForExclude.Any())
            {
                await _customerStationContactPersonRepository.RemoveRangeAsync(customerStationContactInformationForExclude);

                foreach (var item in customerStationContactInformationForExclude)
                {
                    await RemoveContactInformations(item.PersonId);
                    await RemovePerson(item.PersonId);
                }
            }
        }

        private async Task RemoveContactInformations(Guid personId)
        {
            var contactInformations = await _contactInformationReadRepository.GetContactInformationsByPerson(personId);
            if (contactInformations != null && contactInformations.Any())
                await _contactInformationRepository.RemoveRangeAsync(contactInformations);
        }

        private async Task RemovePerson(Guid personId)
        {
            var person = await _personReadRepository.GetById(personId);
            if (person != null)
                await _personRepository.Remove(person);
        }

        public async Task UpdateContactInformationList(IList<ContactInformation> contactInformationList, Guid personId)
        {
            var contactInformations = await _contactInformationReadRepository.GetContactInformationsByPerson(personId);
            if (contactInformations != null && contactInformations.Any())
            {
                await _contactInformationRepository.RemoveRangeAsync(contactInformations);
                SaveContactInformationList(contactInformationList);
            }
        }
        #endregion

    }
}
