﻿using Tnf.Notifications;
using Microsoft.Extensions.Configuration;
using Thex.HigsIntegration.Infra.Repositories.ReadRepositories;
using Thex.HigsIntegration.Application.Interfaces;
using System.Threading.Tasks;
using System.Collections.Generic;
using Thex.HigsIntegration.Dto.Dto.GuestRelation;
using Thex.HigsIntegration.Dto.Dto.GetAll;
using Tnf.Runtime.Security;
using Thex.Common.Enumerations;
using Thex.HigsIntegration.Dto.Dto;
using Microsoft.WindowsAzure.Storage.Queue;
using Thex.HigsIntegration.Infra.Interfaces.Queue;
using System.Linq;
using System;
using Newtonsoft.Json;

namespace Thex.HigsIntegration.Application.Services
{
    public class GuestRelationAppService : HigsApplicationServiceBase, IGuestRelationAppService
    {
        private readonly IntegrationPartnerPropertyReadRepository _integrationPartnerPropertyReadRepository;
        private readonly IGuestRelationListQueue _guestRelationListQueue;
        
        public GuestRelationAppService(
            IConfiguration configuration,
            INotificationHandler notificationHandler,
            IntegrationPartnerPropertyReadRepository integrationPartnerPropertyReadRepository,
            IGuestRelationListQueue guestRelationListQueue) 
            : base(configuration, notificationHandler)
        {
            _integrationPartnerPropertyReadRepository = integrationPartnerPropertyReadRepository;
            _guestRelationListQueue = guestRelationListQueue;
        }

        public async Task<GuestRelationListHigsDto> GetAllAsync(GetAllGuestRelationDto requestDto)
        {
            ValidateRequestAllDto(requestDto, nameof(requestDto));

            if (!requestDto.HasSearchFilter())
                return null;

            if (!string.IsNullOrEmpty(requestDto.HotelCode) &&
                !await _integrationPartnerPropertyReadRepository.AnyByIntegrationCode(requestDto.HotelCode))
                return null;

            return await SendRequest<GuestRelationListHigsDto>(GetGuestRelationListUrl() + requestDto.GuestUrlParamHigs(), HttpVerbs.Get);
        }

        public async Task ProcessGuestRelationAsync()
        {
            var propertyHigsList = await _integrationPartnerPropertyReadRepository.GetAllByPartner(PartnerEnum.Higs);

            if (propertyHigsList.Count == 0)
                return;

            var queue = await _guestRelationListQueue.GetQueue();

            var tasks = propertyHigsList
                .Select(p => SendGuestRelationToQueue(p, queue))
                .ToArray();

            try
            {
                Task.WaitAll(tasks);
            }
            catch (AggregateException ae)
            {
                throw ae.Flatten();
            }
        }

        #region Private Methods

        private async Task SendGuestRelationToQueue(IntegrationPartnerPropertyDto integrationPartnerProperty, CloudQueue queue)
        {
            try
            {
                var guestRelationListResult = await SendRequest<GuestRelationListHigsDto>(GetGuestRelationListUrl() + HotelUrlParamHigs(integrationPartnerProperty.IntegrationCode), HttpVerbs.Get);

                if (guestRelationListResult == null ||
                    guestRelationListResult.GuestRelations == null ||
                    guestRelationListResult.GuestRelations.Count == 0)
                    return;

                foreach (var guestRelation in guestRelationListResult.GuestRelations)
                    await _guestRelationListQueue.Insert(queue, JsonConvert.SerializeObject(guestRelation));
            }
            catch
            {
            }
        }

        #endregion
    }
}
