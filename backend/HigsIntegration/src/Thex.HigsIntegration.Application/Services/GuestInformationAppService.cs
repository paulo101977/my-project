﻿using System;
using System.Threading.Tasks;
using Thex.HigsIntegration.Application.Interfaces;
using Tnf.Notifications;
using Microsoft.Extensions.Configuration;
using Thex.HigsIntegration.Infra.Repositories.ReadRepositories;
using System.Collections.Generic;
using Thex.HigsIntegration.Dto.Dto.GetAll;
using Thex.HigsIntegration.Dto.Dto.GuestInformation;
using Tnf.Runtime.Security;
using Thex.HigsIntegration.Dto.Dto;
using Thex.Common.Enumerations;
using Microsoft.WindowsAzure.Storage.Queue;
using Newtonsoft.Json;
using Thex.HigsIntegration.Infra.Interfaces.Queue;
using System.Linq;

namespace Thex.HigsIntegration.Application.Services
{
    public class GuestInformationAppService : HigsApplicationServiceBase, IGuestInformationAppService
    {
        private readonly IntegrationPartnerPropertyReadRepository _integrationPartnerPropertyReadRepository;
        private readonly IGuestInformationListQueue _guestInformationListQueue;

        public GuestInformationAppService(
            IConfiguration configuration,
            INotificationHandler notificationHandler,
            IGuestInformationListQueue guestInformationListQueue,
            IntegrationPartnerPropertyReadRepository integrationPartnerPropertyReadRepository) 
            : base(configuration, notificationHandler)
        {
            _integrationPartnerPropertyReadRepository = integrationPartnerPropertyReadRepository;
            _guestInformationListQueue = guestInformationListQueue;
        }

        public async Task<GuestInformationListHigsDto> GetAllAsync(GetAllGuestInformationDto requestDto)
        {
            ValidateRequestAllDto(requestDto, nameof(requestDto));

            if (!requestDto.HasSearchFilter())
                return null;

            if (!string.IsNullOrEmpty(requestDto.HotelCode) &&
                !await _integrationPartnerPropertyReadRepository.AnyByIntegrationCode(requestDto.HotelCode))
                return null;

            return await SendRequest<GuestInformationListHigsDto>(GetGuestInformationListUrl() + requestDto.GuestUrlParamHigs(), HttpVerbs.Get);
        }

        public async Task ProcessGuestInformationAsync()
        {
            var propertyHigsList = await _integrationPartnerPropertyReadRepository.GetAllByPartner(PartnerEnum.Higs);

            if (propertyHigsList.Count == 0)
                return;

            var queue = await _guestInformationListQueue.GetQueue();

            var tasks = propertyHigsList
                .Select(p => SendGuestInformationToQueue(p, queue))
                .ToArray();

            try
            {
                Task.WaitAll(tasks);
            }
            catch (AggregateException ae)
            {
                throw ae.Flatten();
            }
        }

        #region Private Methods

        private async Task SendGuestInformationToQueue(IntegrationPartnerPropertyDto integrationPartnerProperty, CloudQueue queue)
        {
            try
            {
                var guestInformationListResult = await SendRequest<GuestInformationListHigsDto>(GetGuestInformationListUrl() + HotelUrlParamHigs(integrationPartnerProperty.IntegrationCode), HttpVerbs.Get);

                if (guestInformationListResult == null ||
                    guestInformationListResult.ResponseList == null ||
                    guestInformationListResult.ResponseList.Count == 0)
                    return;

                var primeiro = guestInformationListResult.ResponseList.FirstOrDefault();
                primeiro.GuestAddress.StreetName = "Rua Visconde de Pirajá";
                primeiro.GuestAddress.AddressNumber = "524";
                primeiro.GuestAddress.Complement = "bloco d apt 40404";
                primeiro.GuestAddress.PostalCode = "22410002";
                primeiro.GuestAddress.District = "Ipanema";
                primeiro.GuestAddress.CityName = "Rio de Janeiro";
                primeiro.GuestAddress.CityCode = "Rio de Janeiro";
                primeiro.GuestAddress.StateName = "RJ";
                primeiro.GuestAddress.StateCode = "RJ";
                primeiro.GuestAddress.CountryName = "Brasil";
                primeiro.GuestAddress.CountryCode = "BR";

                primeiro.Nationality = "BR";
                primeiro.DocumentNumber = "134.272.657-06";
                var teste = JsonConvert.SerializeObject(primeiro);
                await _guestInformationListQueue.Insert(queue, teste);

                //foreach (var guestInformation in guestInformationListResult.ResponseList)
                //    await _guestInformationListQueue.Insert(queue, JsonConvert.SerializeObject(guestInformation));
            }
            catch
            {
            }
        }

        #endregion
    }
}
