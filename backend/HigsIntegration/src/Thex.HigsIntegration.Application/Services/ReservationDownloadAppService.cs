﻿using Microsoft.Extensions.Configuration;
using Microsoft.WindowsAzure.Storage.Queue;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Thex.Common.Dto;
using Thex.Common.Enumerations;
using Thex.HigsIntegration.Application.Interfaces;
using Thex.HigsIntegration.Dto.Dto;
using Thex.HigsIntegration.Infra.Interfaces.Queue;
using Thex.HigsIntegration.Infra.Interfaces.ReadRepositories;
using Thex.HigsIntegration.Infra.Repositories.ReadRepositories;
using Thex.Kernel;
using Thex.Log.Application.Interfaces;
using Thex.Log.Dto;
using Thex.Log.Dto.ReservationDownload;
using Thex.Log.Infra.Enumerations;
using Tnf.Notifications;
using Tnf.Runtime.Security;

namespace Thex.HigsIntegration.Application.Services
{
    public class ReservationDownloadAppService : HigsApplicationServiceBase, IReservationDownloadAppService
    {
        private readonly IntegrationPartnerPropertyReadRepository _integrationPartnerPropertyReadRepository;
        private readonly IReservationDownloadQueue _reservationDownloadQueue;
        private readonly ICurrencyReadRepository _currencyReadRepository;
        private readonly IChannelReadRepository _channelReadRepository;
        private readonly ICompanyClientChannelReadRepository _companyClientChannelReadRepository;
        private readonly IRoomTypeReadRepository _roomTypeReadRepository;
        private readonly IRoomLayoutReadRepository _roomLayoutReadRepository;
        private readonly IRatePlanReadRepository _ratePlanReadRepository;
        private readonly IPropertyGuestTypeReadRepository _propertyGuestTypeReadRepository;
        private readonly IReservationItemReadRepository _reservationItemReadRepository;
        private readonly IReservationDownloadLogAppService _reservationDownloadLogAppService;
        private readonly IPropertyParameterReadRepository _propertyParameterReadRepository;
        private readonly IApplicationUser _applicationUser;
        private readonly IConfiguration _configuration;

        public ReservationDownloadAppService(
            IConfiguration configuration,
            INotificationHandler notificationHandler,
            ICurrencyReadRepository currencyReadRepository,
            IChannelReadRepository channelReadRepository,
            ICompanyClientChannelReadRepository companyClientChannelReadRepository,
            IRoomTypeReadRepository roomTypeReadRepository,
            IRoomLayoutReadRepository roomLayoutReadRepository,
            IRatePlanReadRepository ratePlanReadRepository,
            IPropertyGuestTypeReadRepository propertyGuestTypeReadRepository,
            IPropertyParameterReadRepository propertyParameterReadRepository,
            IReservationItemReadRepository reservationItemReadRepository,
            IntegrationPartnerPropertyReadRepository integrationPartnerPropertyReadRepository,
            IApplicationUser applicationUser,
            IReservationDownloadLogAppService reservationDownloadLogAppService,
            IReservationDownloadQueue reservationDownloadQueue) : base(configuration, notificationHandler)
        {
            _integrationPartnerPropertyReadRepository = integrationPartnerPropertyReadRepository;
            _reservationDownloadQueue = reservationDownloadQueue;
            _currencyReadRepository = currencyReadRepository;
            _channelReadRepository = channelReadRepository;
            _companyClientChannelReadRepository = companyClientChannelReadRepository;
            _roomTypeReadRepository = roomTypeReadRepository;
            _roomLayoutReadRepository = roomLayoutReadRepository;
            _ratePlanReadRepository = ratePlanReadRepository;
            _propertyGuestTypeReadRepository = propertyGuestTypeReadRepository;
            _reservationItemReadRepository = reservationItemReadRepository;
            _reservationDownloadLogAppService = reservationDownloadLogAppService;
            _propertyParameterReadRepository = propertyParameterReadRepository;
            _applicationUser = applicationUser;
            _configuration = configuration;
        }

        public async Task ProcessReservationDownloadAsync()
        {
            var reservationDownloadUrl = GetReservationDownloadUrl();
            var propertyHigsList = await _integrationPartnerPropertyReadRepository.GetAllByPartner(PartnerEnum.Higs);

            if (propertyHigsList.Count == 0)
                return;

            var queue = await _reservationDownloadQueue.GetQueue();

            var tasks = propertyHigsList
                .Select(p => SendReservationToQueue(p, queue, reservationDownloadUrl))
                .ToArray();

            try
            {
                Task.WaitAll(tasks);
            }
            catch (AggregateException ae)
            {
                throw ae.Flatten();
            }
        }

        public async Task<ThexListDto<ReservationDownloadResultDto>> GetAllByFiltersAsync(ReservationDownloadSearchDto request)
        {
            if (!request.PageSize.HasValue)
                request.PageSize = _configuration.GetValue<int>("DefaultPageSizeValue");

            if (!request.Page.HasValue)
                request.Page = 1;

            return await _reservationDownloadLogAppService.GetAllByFiltersAsync(request, int.Parse(_applicationUser.PropertyId));
        }

        public async Task<ThexListDto<ReservationDownloadHistoryDto>> GetAllHistoryByIdAsync(string reservDownloadId)
            => await _reservationDownloadLogAppService.GetAllHistoryByIdAsync(reservDownloadId);
        
        #region Private Methods

        private async Task SendReservationToQueue(IntegrationPartnerPropertyDto integrationPartnerProperty, CloudQueue queue, string url)
        {
            try
            {
                var reservationDownloadResult = await SendRequest<ReservationDownloadDto>(url, HttpVerbs.Post, HotelCriteriaHigs(integrationPartnerProperty.IntegrationCode));

                if (reservationDownloadResult == null ||
                    reservationDownloadResult.ReservationsList == null ||
                    reservationDownloadResult.ReservationsList.Count == 0)
                    return;

                foreach (var reservation in reservationDownloadResult.ReservationsList)
                {
                    var result = await ValidateReservationAsync(reservation, integrationPartnerProperty.PropertyId, integrationPartnerProperty.TenantId);
                    if(result.SituationList.All(s => s.Status))
                        await _reservationDownloadQueue.Insert(queue, JsonConvert.SerializeObject(reservation));
                }
            }
            catch (Exception ex)
            {
            }
        }

        private ReservationDownloadSituationDto GenerateSituationDto(int statusCode, bool status, string key = null)
        {
            var errorDto = new ReservationDownloadSituationDto();
            errorDto.OperationDate = DateTime.UtcNow;
            errorDto.Status = status;
            errorDto.StatusCode = statusCode;
            errorDto.Key = key;

            return errorDto;
        }
        #endregion

        #region Validations
        private async Task<ReservationDownloadRequestDto> ValidateReservationAsync(ReservationHeaderHigsDto dto, int propertyId, Guid tenantId)
        {
            dto.RequesDto = JsonConvert.DeserializeObject<ReservationHigsDto>(dto.Request);

            var reservationLogDto = new ReservationDownloadRequestDto();
            reservationLogDto.PropertyTimeZone = await _propertyParameterReadRepository.GetTimeZoneByPropertyIdAsync(propertyId);
            reservationLogDto.PropertyId = propertyId;
            reservationLogDto.ReservationHeaderHigsDto = dto;

            await ValidateReservationItemAsync(reservationLogDto);
            await ValidateChannelAsync(reservationLogDto, tenantId);
            await ValidateRoomTypeAsync(reservationLogDto);
            await ValidateCurrencyAsync(reservationLogDto);
            await ValidatePropertyGuestTypeAsync(reservationLogDto);
            await ValidateRoomLayoutAsync(reservationLogDto);

            await _reservationDownloadLogAppService.SendLogAsync(reservationLogDto);
            return reservationLogDto;
        }

        private async Task ValidateReservationItemAsync(ReservationDownloadRequestDto reservationLogDto)
        {
            if (reservationLogDto.ReservationHeaderHigsDto.OperationType == SituationTypeHigsEnum.Modify || reservationLogDto.ReservationHeaderHigsDto.OperationType == SituationTypeHigsEnum.Cancel)
            {
                var reservationItem = await _reservationItemReadRepository.GetByExternalReservationNumberAsync(reservationLogDto.ReservationHeaderHigsDto.ReservationNumberHigs);
                reservationLogDto.SituationList.Add(GenerateSituationDto((int)ReservationDownloadSituationEnum.ReservationItem, reservationItem != null, reservationLogDto.ReservationHeaderHigsDto.ReservationNumberHigs));
            }
        }

        private async Task ValidateChannelAsync(ReservationDownloadRequestDto reservationLogDto, Guid tenantId)
        {
            var channelCode = reservationLogDto.ReservationHeaderHigsDto.RequesDto.SourceOfBusiness;
            var channel = await _channelReadRepository.GetByIntegrationCodeAsync(channelCode, tenantId);
            reservationLogDto.SituationList.Add(GenerateSituationDto((int)ReservationDownloadSituationEnum.Channel, channel != null, channelCode));
        }

        private async Task ValidateRoomTypeAsync(ReservationDownloadRequestDto reservationLogDto)
        {
            var distributionCode = reservationLogDto.ReservationHeaderHigsDto.RequesDto.RoomTypeCode;
            var roomType = await _roomTypeReadRepository.GetByDistributionCodeAsync(distributionCode, reservationLogDto.PropertyId);
            reservationLogDto.SituationList.Add(GenerateSituationDto((int)ReservationDownloadSituationEnum.RoomType, roomType != null, distributionCode));
        }

        private async Task ValidateCurrencyAsync(ReservationDownloadRequestDto reservationLogDto)
        {
            var currencyCode = reservationLogDto.ReservationHeaderHigsDto.RequesDto.CurrencyCode;
            var currency = await _currencyReadRepository.GetByCurrencyCodeAsync(currencyCode);
            reservationLogDto.SituationList.Add(GenerateSituationDto((int)ReservationDownloadSituationEnum.Currency, currency != null, currencyCode));
        }

        private async Task ValidatePropertyGuestTypeAsync(ReservationDownloadRequestDto reservationLogDto)
        {
            var propertyGuestType = await _propertyGuestTypeReadRepository.GetDefaultByPropertyIdAsync(reservationLogDto.PropertyId);
            reservationLogDto.SituationList.Add(GenerateSituationDto((int)ReservationDownloadSituationEnum.PropertyGuestType, propertyGuestType != null));
        }

        private async Task ValidateRoomLayoutAsync(ReservationDownloadRequestDto reservationLogDto)
        {
            var roomLayout = await _roomLayoutReadRepository.GetDefaultAsync();
            reservationLogDto.SituationList.Add(GenerateSituationDto((int)ReservationDownloadSituationEnum.RoomLayout, roomLayout != null));
        }
        #endregion       
    }
}
