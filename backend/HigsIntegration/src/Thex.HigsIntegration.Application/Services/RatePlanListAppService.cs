﻿using System;
using System.Threading.Tasks;
using Thex.HigsIntegration.Application.Interfaces;
using Thex.HigsIntegration.Dto.Dto;
using Tnf.Notifications;
using Tnf.Runtime.Security;
using Microsoft.Extensions.Configuration;
using Thex.HigsIntegration.Infra.Repositories.ReadRepositories;
using Thex.Common.Enumerations;
using System.Linq;
using Thex.HigsIntegration.Infra.Interfaces.Queue;
using Microsoft.WindowsAzure.Storage.Queue;
using Newtonsoft.Json;
using Thex.HigsIntegration.Dto.Dto.RatePlanList;

namespace Thex.HigsIntegration.Application.Services
{
    public class RatePlanListAppService : HigsApplicationServiceBase, IRatePlanListAppService
    {
        private readonly IntegrationPartnerPropertyReadRepository _integrationPartnerPropertyReadRepository;
        private readonly IRatePlanListQueue _ratePlanListQueue;

        public RatePlanListAppService(
            IConfiguration configuration,
            INotificationHandler notificationHandler,
            IntegrationPartnerPropertyReadRepository integrationPartnerPropertyReadRepository,
            IRatePlanListQueue ratePlanListQueue) : base(configuration, notificationHandler)
        {
            _integrationPartnerPropertyReadRepository = integrationPartnerPropertyReadRepository;
            _ratePlanListQueue = ratePlanListQueue;
        }

        public async Task ProcessRatePlanListAsync()
        {
            var ratePlanListUrl = GetRatePlanListUrl();
            var propertyHigsList = await _integrationPartnerPropertyReadRepository.GetAllByPartner(PartnerEnum.Higs);

            if (propertyHigsList.Count == 0)
                return;

            var queue = await _ratePlanListQueue.GetQueue();

            var tasks = propertyHigsList
                .Select(p => SendRatePlanToQueue(p, queue, ratePlanListUrl))
                .ToArray();

            try
            {
                Task.WaitAll(tasks);
            }
            catch (AggregateException ae)
            {
                throw ae.Flatten();
            }
        }

        #region Private Methods

        private async Task SendRatePlanToQueue(IntegrationPartnerPropertyDto integrationPartnerProperty, CloudQueue queue, string url)
        {
            try
            {
                var ratePlanListResult = await SendRequest<RatePlanListDto>($"{url}/{integrationPartnerProperty.IntegrationCode}", HttpVerbs.Get);

                if (ratePlanListResult == null ||
                    ratePlanListResult.RateplanList == null ||
                    ratePlanListResult.RateplanList.Count == 0)
                    return;

                foreach (var ratePlan in ratePlanListResult.RateplanList)
                {
                    ratePlan.RatePlan = ratePlan.RatePlan.Take(1).ToList();
                    ratePlan.RateAmount = ratePlan.RateAmount.Take(1).ToList();
                    ratePlan.HotelCode = integrationPartnerProperty.IntegrationCode;
                    await _ratePlanListQueue.Insert(queue, JsonConvert.SerializeObject(ratePlan));
                }
            }
            catch
            {
            }
        }

        #endregion
    }
}
