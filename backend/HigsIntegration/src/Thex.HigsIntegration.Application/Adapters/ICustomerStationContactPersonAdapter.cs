﻿using Thex.HigsIntegration.Dto.Dto;
using Thex.HigsIntegration.Infra.Entities;

namespace Thex.HigsIntegration.Application.Adapters
{
    public interface ICustomerStationContactPersonAdapter
    {
        CustomerStationContactPerson.Builder Map(CustomerStationContactPerson entity, CustomerStationContactPersonDto dto);
        CustomerStationContactPerson.Builder Map(CustomerStationContactPersonDto dto);
    }
}
