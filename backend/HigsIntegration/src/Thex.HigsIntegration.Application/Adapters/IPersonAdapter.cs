﻿using Thex.HigsIntegration.Dto.Dto;
using Thex.HigsIntegration.Infra.Entities;

namespace Thex.HigsIntegration.Application.Adapters
{
    public interface IPersonAdapter
    {
        Person.Builder Map(Person entity, PersonDto dto);
        Person.Builder Map(PersonDto dto);
    }
}
