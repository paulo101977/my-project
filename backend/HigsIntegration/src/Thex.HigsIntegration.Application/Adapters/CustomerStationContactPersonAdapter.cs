﻿using System;
using System.Collections.Generic;
using System.Text;
using Thex.HigsIntegration.Dto.Dto;
using Thex.HigsIntegration.Infra.Entities;
using Tnf;
using Tnf.Notifications;

namespace Thex.HigsIntegration.Application.Adapters
{
    public class CustomerStationContactPersonAdapter : ICustomerStationContactPersonAdapter
    {
        public INotificationHandler NotificationHandler { get; }

        public CustomerStationContactPersonAdapter(INotificationHandler notificationHandler)
        {
            NotificationHandler = notificationHandler;
        }

        public CustomerStationContactPerson.Builder Map(CustomerStationContactPerson entity, CustomerStationContactPersonDto dto)
        {
            Check.NotNull(entity, nameof(entity));
            Check.NotNull(dto, nameof(dto));

            var builder = new CustomerStationContactPerson.Builder(NotificationHandler, entity)
                .WithId(dto.Id)
                .WithOccupationId(dto.OccupationId)
                .WithCustomerStationId(dto.CustomerStationId)
                .WithPersonId(dto.PersonId);

                 return builder;
        }

        public CustomerStationContactPerson.Builder Map(CustomerStationContactPersonDto dto)
        {
            Check.NotNull(dto, nameof(dto));

            var builder = new CustomerStationContactPerson.Builder(NotificationHandler)
                .WithId(dto.Id)
                .WithOccupationId(dto.OccupationId)
                .WithCustomerStationId(dto.CustomerStationId)
                .WithPersonId(dto.PersonId);

            return builder;
        }
    }
}
