﻿using Thex.HigsIntegration.Dto.Dto;
using Thex.HigsIntegration.Infra.Entities;

namespace Thex.HigsIntegration.Application.Adapters
{
    public interface IContactInformationAdapter
    {
        ContactInformation.Builder Map(ContactInformation entity, ContactInformationDto dto);
        ContactInformation.Builder Map(ContactInformationDto dto);
    }
}
