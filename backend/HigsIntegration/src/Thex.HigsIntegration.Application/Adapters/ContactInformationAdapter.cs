﻿using Thex.HigsIntegration.Dto.Dto;
using Thex.HigsIntegration.Infra.Entities;
using Tnf;
using Tnf.Notifications;

namespace Thex.HigsIntegration.Application.Adapters
{
    public class ContactInformationAdapter : IContactInformationAdapter
    {
        public INotificationHandler NotificationHandler { get; }

        public ContactInformationAdapter(INotificationHandler notificationHandler)
        {
            NotificationHandler = notificationHandler;
        }

        public virtual ContactInformation.Builder Map(ContactInformation entity, ContactInformationDto dto)
        {
            Check.NotNull(entity, nameof(entity));
            Check.NotNull(dto, nameof(dto));

            var builder = new ContactInformation.Builder(NotificationHandler, entity)
                .WithContactInformationTypeId(dto.ContactInformationTypeId)
                .WithInformation(dto.Information)
                .WithOwnerId(dto.OwnerId);


            return builder;
        }

        public virtual ContactInformation.Builder Map(ContactInformationDto dto)
        {
            Check.NotNull(dto, nameof(dto));

            var builder = new ContactInformation.Builder(NotificationHandler)
                .WithContactInformationTypeId(dto.ContactInformationTypeId)
                .WithInformation(dto.Information)
                .WithOwnerId(dto.OwnerId);

            return builder;
        }
    }
}
