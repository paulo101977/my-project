﻿using Thex.HigsIntegration.Dto.Dto;
using Thex.HigsIntegration.Infra.Entities;
using Tnf;
using Tnf.Notifications;

namespace Thex.HigsIntegration.Application.Adapters
{
    public class CustomerStationAdapter : ICustomerStationAdapter
    {
        public INotificationHandler NotificationHandler { get; }

        public CustomerStationAdapter(INotificationHandler notificationHandler)
        {
            NotificationHandler = notificationHandler;
        }

        public virtual CustomerStation.Builder Map(CustomerStation entity, CustomerStationDto dto)
        {
            Check.NotNull(entity, nameof(entity));
            Check.NotNull(dto, nameof(dto));

            var builder = new CustomerStation.Builder(NotificationHandler, entity)
                .WithId(dto.Id)
                .WithCustomerStationName(dto.CustomerStationName)
                .WithSocialReason(dto.SocialReason)
                .WithCompanyClientId(dto.CompanyClientId)
                .WithIsActive(dto.IsActive)
                .WithHomePage(dto.HomePage)
                .WithCustomerStationCategoryId(dto.CustomerStationCategoryId);


            return builder;
        }

        public virtual CustomerStation.Builder Map(CustomerStationDto dto)
        {
            Check.NotNull(dto, nameof(dto));

            var builder = new CustomerStation.Builder(NotificationHandler)
                .WithId(dto.Id)
                .WithCustomerStationName(dto.CustomerStationName)
                .WithSocialReason(dto.SocialReason)
                .WithCompanyClientId(dto.CompanyClientId)
                .WithIsActive(dto.IsActive)
                .WithHomePage(dto.HomePage)
                .WithCustomerStationCategoryId(dto.CustomerStationCategoryId);

            return builder;
        }
    }
}
