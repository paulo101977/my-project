﻿
using Thex.HigsIntegration.Dto.Dto;
using Thex.HigsIntegration.Infra.Entities;

namespace Thex.HigsIntegration.Application.Adapters
{
    public interface ICustomerStationAdapter
    {
        CustomerStation.Builder Map(CustomerStation entity, CustomerStationDto dto);
        CustomerStation.Builder Map(CustomerStationDto dto);
    }
}
