﻿using Thex.HigsIntegration.Dto.Dto;
using Thex.HigsIntegration.Infra.Entities;
using Tnf;
using Tnf.Notifications;

namespace Thex.HigsIntegration.Application.Adapters
{
    public class PersonAdapter : IPersonAdapter
    {
        public INotificationHandler NotificationHandler { get; }

        public PersonAdapter(INotificationHandler notificationHandler)
        {
            NotificationHandler = notificationHandler;
        }

        public Person.Builder Map(Person entity, PersonDto dto)
        {
            Check.NotNull(entity, nameof(entity));
            Check.NotNull(dto, nameof(dto));

            var builder = new Person.Builder(NotificationHandler, entity)
               .WithId(dto.Id)
               .WithFirstName(dto.FirstName)
               .WithLastName(dto.LastName)
               .WithFullName(dto.FullName)
               .WithPersonType(dto.PersonType);

            return builder;
        }

        public Person.Builder Map(PersonDto dto)
        {
            Check.NotNull(dto, nameof(dto));

            var builder = new Person.Builder(NotificationHandler)
               .WithId(dto.Id)
               .WithFirstName(dto.FirstName)
               .WithLastName(dto.LastName)
               .WithFullName(dto.FullName)
               .WithPersonType(dto.PersonType);

            return builder;
        }
    }
}
