﻿using System.Threading.Tasks;
using Thex.HigsIntegration.Application.Interfaces;
using Thex.HigsIntegration.Application.Interfaces.Jobs;

namespace Thex.HigsIntegration.Application.Jobs
{
    public class GuestRelationJob : IJob
    {
        private readonly IGuestRelationAppService _guestRelationAppService;

        public GuestRelationJob(IGuestRelationAppService guestRelationAppService)
        {
            _guestRelationAppService = guestRelationAppService;
        }

        public async Task Execute()
            => await _guestRelationAppService.ProcessGuestRelationAsync();
    }
}
