﻿using System.Threading.Tasks;
using Thex.HigsIntegration.Application.Interfaces;
using Thex.HigsIntegration.Application.Interfaces.Jobs;

namespace Thex.HigsIntegration.Application.Jobs
{
    public class ReservationDownloadJob : IJob
    {
        private readonly IReservationDownloadAppService _reservationDownloadAppService;

        public ReservationDownloadJob(IReservationDownloadAppService reservationDownloadAppService)
        {
            _reservationDownloadAppService = reservationDownloadAppService;
        }

        public async Task Execute()
            => await _reservationDownloadAppService.ProcessReservationDownloadAsync();
    }
}
