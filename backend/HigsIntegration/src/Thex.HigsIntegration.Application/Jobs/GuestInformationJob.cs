﻿using System.Threading.Tasks;
using Thex.HigsIntegration.Application.Interfaces;
using Thex.HigsIntegration.Application.Interfaces.Jobs;

namespace Thex.HigsIntegration.Application.Jobs
{
    public class GuestInformationJob : IJob
    {
        private readonly IGuestInformationAppService _guestInformationAppService;

        public GuestInformationJob(IGuestInformationAppService guestInformationAppService)
        {
            _guestInformationAppService = guestInformationAppService;
        }

        public async Task Execute()
            => await _guestInformationAppService.ProcessGuestInformationAsync();
    }
}
