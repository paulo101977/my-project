﻿using System.Threading.Tasks;
using Thex.HigsIntegration.Application.Interfaces;
using Thex.HigsIntegration.Application.Interfaces.Jobs;

namespace Thex.HigsIntegration.Application.Jobs
{
    public class RatePlanListJob : IJob
    {
        private readonly IRatePlanListAppService _ratePlanListAppService;

        public RatePlanListJob(IRatePlanListAppService ratePlanListAppService)
        {
            _ratePlanListAppService = ratePlanListAppService;
        }

        public async Task Execute()
            => await _ratePlanListAppService.ProcessRatePlanListAsync();
    }
}
