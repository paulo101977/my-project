﻿using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Thex.HigsIntegration.Infra.Constants;
using Tnf.Application.Services;
using Tnf.Notifications;
using Tnf.Runtime.Security;

namespace Thex.HigsIntegration.Application
{
    public class HigsApplicationServiceBase : ApplicationService
    {
        protected HttpClient Client;
        protected HttpClientHandler Handler;
        protected readonly IConfiguration Configuration;

        public HigsApplicationServiceBase(IConfiguration configuration, INotificationHandler notificationHandler)
            :base(notificationHandler)
        {
            Configuration = configuration;
            Handler = new HttpClientHandler();
            Handler.AutomaticDecompression = DecompressionMethods.Deflate | DecompressionMethods.GZip;
        }

        protected async Task<T> SendRequest<T>(string url, HttpVerbs verb, object value = null)
        {
            Client = new HttpClient(Handler);
            SetTokenClient();
            SetTokenApplication();

            HttpResponseMessage response = null;

            switch (verb)
            {
                case HttpVerbs.Get:
                    response = await Client.GetAsync(url).ForAwait();
                    break;

                case HttpVerbs.Post:
                    try
                    {
                        var json = JsonConvert.SerializeObject(value);
                        var httpContent = new StringContent(json, Encoding.UTF8, "application/json");
                        Client.DefaultRequestHeaders.Add("Accept-Encoding", "gzip,deflate");
                        response = await Client.PostAsync(url, httpContent).ForAwait();
                        break;
                    }
                    catch (Exception ex)
                    {

                        throw;
                    }

            }

            var contentResponse = string.Empty;

            if (response?.Content != null)
                contentResponse = await response.Content.ReadAsStringAsync().ForAwait();

            switch (response?.StatusCode)
            {
                case HttpStatusCode.OK:
                    if (!contentResponse.IsNullOrWhiteSpace())
                    {
                        try
                        {
                            return JsonConvert.DeserializeObject<T>(contentResponse);
                        }
                        catch (Exception)
                        {
                            //comunicação inválida
                        }
                    }

                    return default(T);
                case HttpStatusCode.Unauthorized:
                    {
                        return default(T);
                        //não autorizado
                    }
                case HttpStatusCode.NotFound:
                    {
                        //não existente
                        return default(T);
                    }
                default:
                    {
                        //erro ao tentar estabelecer a comunicação
                        return default(T);
                    }
            }
        }

        protected string GetReservationDownloadUrl()
            => $"{Configuration.GetValue<string>(HigsIntegrationConstants.HIGSURLAPI)}/{HigsIntegrationConstants.RESERVATIONDOWNLOAD}";

        protected string GetRatePlanListUrl()
            => $"{Configuration.GetValue<string>(HigsIntegrationConstants.HIGSURLAPI)}/{HigsIntegrationConstants.RATEPLANLIST}";

        protected string GetGuestRelationListUrl()
            => $"{Configuration.GetValue<string>(HigsIntegrationConstants.HIGSURLAPI)}/{HigsIntegrationConstants.GUESTRELATION}";

        protected string GetGuestInformationListUrl()
            => $"{Configuration.GetValue<string>(HigsIntegrationConstants.HIGSURLAPI)}/{HigsIntegrationConstants.GUESTINFORMATION}";

        private void SetTokenClient()
            => Client.DefaultRequestHeaders.Add(HigsIntegrationConstants.HIGSTOKENCLIENT, Configuration.GetValue<string>(HigsIntegrationConstants.HIGSTOKENCLIENTSETTINGS));

        private void SetTokenApplication()
            => Client.DefaultRequestHeaders.Add(HigsIntegrationConstants.HIGSTOKENAPPLICATION, Configuration.GetValue<string>(HigsIntegrationConstants.HIGSTOKENAPPLICATIONSETTINGS));

        protected object HotelUrlParamHigs(string integrationCode)
            => $"?hotelCode={integrationCode}";

        protected object HotelCriteriaHigs(string integrationCode)
            => new {
                SelectionCriteria = new
                {
                    HotelCode = integrationCode
                }
            };

        protected object GuestCriteriaHigs(string integrationCode,string sourceReservationNumber, string guestCod, string roomNumber, int? page)
            => new
            {
                SelectionCriteria = new
                {
                    HotelCode = integrationCode,
                    SourceReservationNumber = sourceReservationNumber,
                    GuestCod = guestCod,
                    RoomNumber = roomNumber,
                    Page = page
                }
            };

        protected object GuestUrlParamHigs(string integrationCode, string sourceReservationNumber, string guestCod, string roomNumber, int? page)
            => $"?hotelCode={integrationCode}&SourceReservationNumber={sourceReservationNumber}&GuestCode={guestCod}&RoomCode={roomNumber}&Page={page}";

    }
}
