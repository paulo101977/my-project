﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Thex.HigsIntegration.Application.Interfaces;
using Tnf.AspNetCore.Mvc.Response;
using Thex.HigsIntegration.Infra.Entities;
using Thex.HigsIntegration.Dto.Dto.GuestInformation;
using Thex.HigsIntegration.Dto.Dto.GetAll;
using Hangfire;
using Thex.HigsIntegration.Application.Jobs;

namespace Thex.HigsIntegration.Web.Controllers
{
    [Route(RouteConsts.GuestInformationRouteName)]
    [AllowAnonymous]
    public class GuestInformationController : TnfController
    {
        private readonly IGuestInformationAppService _guestInformationAppService;

        public GuestInformationController(IGuestInformationAppService guestInformationAppService)
        {
            _guestInformationAppService = guestInformationAppService;
        }

        [HttpGet]
        [ProducesResponseType(typeof(GuestInformationListHigsDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> GetAll([FromQuery] GetAllGuestInformationDto requestDto)
        {
            return CreateResponseOnGet(
                await _guestInformationAppService.GetAllAsync(requestDto),
                EntityNames.GuestInformation);
        }

        [HttpPost]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult ProcessGuestInformationListAsync()
        {
            BackgroundJob.Enqueue<GuestInformationJob>(j => j.Execute());

            return Ok();
        }
    }
}