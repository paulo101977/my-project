﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Thex.HigsIntegration.Application.Interfaces;
using Tnf.AspNetCore.Mvc.Response;
using Thex.HigsIntegration.Infra.Entities;
using Thex.HigsIntegration.Dto.Dto.GuestInformation;
using Thex.HigsIntegration.Dto.Dto.GetAll;
using Thex.HigsIntegration.Dto.Dto.GuestRelation;
using Hangfire;
using Thex.HigsIntegration.Application.Jobs;

namespace Thex.HigsIntegration.Web.Controllers
{
    [Route(RouteConsts.GuestRelationRouteName)]
    [AllowAnonymous]
    public class GuestRelationController : TnfController
    {
        private readonly IGuestRelationAppService _guestRelationAppService;

        public GuestRelationController(IGuestRelationAppService guestRelationAppService)
        {
            _guestRelationAppService = guestRelationAppService;
        }

        [HttpGet]
        [ProducesResponseType(typeof(GuestRelationListHigsDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> GetAll([FromQuery] GetAllGuestRelationDto requestDto)
        {
            return CreateResponseOnGet(
                await _guestRelationAppService.GetAllAsync(requestDto),
                EntityNames.GuestRelation);
        }

        [HttpPost]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult ProcessGuestRelationListAsync()
        {
            BackgroundJob.Enqueue<GuestRelationJob>(j => j.Execute());

            return Ok();
        }
    }
}