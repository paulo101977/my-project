﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Thex.HigsIntegration.Application.Interfaces;
using Thex.HigsIntegration.Dto.Dto;
using Thex.HigsIntegration.Dto.Dto.GetAll;
using Thex.HigsIntegration.Infra.Entities;
using Thex.Location.Application.Interfaces;
using Tnf.AspNetCore.Mvc.Response;


namespace Thex.HigsIntegration.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [AllowAnonymous]
    public class HealthController : TnfController
    {
        private readonly ICustomerStationAppService _customerStationAppService;
        private readonly ILocationAppService _locationAppService;

        public HealthController(ICustomerStationAppService customerStationAppService,
                                         ILocationAppService locationAppService)
        {
            _customerStationAppService = customerStationAppService;
            _locationAppService = locationAppService;
        }

        /// <summary>
        /// Verifica se a API está em funcionamento e conectada no banco de dados
        /// </summary>
        /// <returns></returns>
        [HttpGet("check")]
        [ProducesResponseType(typeof(bool), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> GetAll([FromQuery] GetAllCustomerStationDto requestDto)
        {
            var response = await _customerStationAppService.GetAllCustomerStationDto(requestDto);

            return CreateResponseOnGetAll(response != null ? true : false, EntityNames.Health);
        }
    }
}