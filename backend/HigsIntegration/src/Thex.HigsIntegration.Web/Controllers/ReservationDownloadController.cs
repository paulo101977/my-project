﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Thex.HigsIntegration.Application.Interfaces;
using Tnf.AspNetCore.Mvc.Response;
using Hangfire;
using Thex.HigsIntegration.Application.Jobs;
using Thex.Log.Dto.ReservationDownload;

namespace Thex.HigsIntegration.Web.Controllers
{
    [Route(RouteConsts.ReservationDownloadRouteName)]
    [Authorize]
    public class ReservationDownloadController : TnfController
    {
        private readonly IReservationDownloadAppService _reservationDownloadAppService;

        public ReservationDownloadController(IReservationDownloadAppService reservationDownloadAppService)
        {
            _reservationDownloadAppService = reservationDownloadAppService;
        }

        [AllowAnonymous]
        [HttpPost]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult ProcessReservationDownloadAsync()
        {
            BackgroundJob.Enqueue<ReservationDownloadJob>(j => j.Execute());

            return Ok();
        }

        [HttpGet]
        [ThexAuthorize("HIGSINTEGRATION_Get_All_Reservation_Logs")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> GetAllByFiltersAsync([FromQuery] ReservationDownloadSearchDto request)
        {
            var response = await _reservationDownloadAppService.GetAllByFiltersAsync(request);

            return CreateResponseOnGetAll(response);
        }

        [HttpGet("{reservDownloadId}/history")]
        [ThexAuthorize("HIGSINTEGRATION_Get_All_Reservation_Logs_History")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> GetAllHistoryByIdAsync(string reservDownloadId)
        {
            var response = await _reservationDownloadAppService.GetAllHistoryByIdAsync(reservDownloadId);

            return CreateResponseOnGetAll(response);
        }
    }
}