﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Thex.HigsIntegration.Application.Interfaces;
using Thex.HigsIntegration.Dto;
using Thex.HigsIntegration.Dto.Dto;
using Thex.HigsIntegration.Dto.Dto.GetAll;
using Thex.HigsIntegration.Infra.Entities;
using Thex.Location.Application.Interfaces;
using Tnf.AspNetCore.Mvc.Response;
using Tnf.Dto;

namespace Thex.HigsIntegration.Web.Controllers
{
    [Route(RouteConsts.CustomerStationRouteName)]
    [Authorize]
    public class CustomerStationController : TnfController
    {
        private readonly ICustomerStationAppService _customerStationAppService;
        private readonly ILocationAppService _locationAppService;

        public CustomerStationController(ICustomerStationAppService customerStationAppService,
                                         ILocationAppService locationAppService)
        {
            _customerStationAppService = customerStationAppService;
            _locationAppService = locationAppService;
        }

        [HttpGet("{id}")]
        [ProducesResponseType(typeof(CustomerStationDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> GetById(Guid id)
        {
            var response = await _customerStationAppService.GetCustomerStationDtoById(id);

            return CreateResponseOnGet(response, EntityNames.CustomerStation);
        }

        [HttpGet]
        [ProducesResponseType(typeof(CustomerStationDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> GetAll([FromQuery] GetAllCustomerStationDto requestDto)
        {
            var response = await _customerStationAppService.GetAllCustomerStationDto(requestDto);

            return CreateResponseOnGet(response, EntityNames.CustomerStation);
        }


        [HttpPost]
        [ProducesResponseType(typeof(CustomerStationDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Create([FromBody] CustomerStationDto requestDto)
        {
            var response = await _customerStationAppService.CreateAsync(requestDto);

            return CreateResponseOnPost(response, EntityNames.CustomerStation);
        }

        [HttpPut("{id}")]
        [ProducesResponseType(typeof(CustomerStationDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Update(Guid id, [FromBody] CustomerStationDto requestDto)
        {
            var response = await _customerStationAppService.UpdateAsync(id, requestDto);

            return CreateResponseOnPut(response, EntityNames.CustomerStation);
        }

        [HttpDelete("{id}")]
        [ProducesResponseType(typeof(CustomerStationDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Delete(Guid id)
        {
            await _customerStationAppService.DeleteAsync(id);

            return CreateResponseOnDelete(EntityNames.CustomerStation);
        }

        [HttpPatch("{id}/toogle")]
        [ProducesResponseType(typeof(CustomerStationDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Toogle(Guid id)
        {
            await _customerStationAppService.ToogleAsync(id);
            return CreateResponseOnGet(null, EntityNames.CustomerStation);
        }

        [HttpGet("getallbycompanyclient/{companyClientId}")]
        [ProducesResponseType(typeof(CustomerStationDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> GetAllByCompanyClient(Guid companyClientId)
        {
            var response = await _customerStationAppService.GetAllDtoByCompanyClient(companyClientId);

            return CreateResponseOnGet(response, EntityNames.CustomerStation);
        }
    }
}