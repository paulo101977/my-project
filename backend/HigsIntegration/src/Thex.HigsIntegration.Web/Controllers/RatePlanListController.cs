﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Thex.HigsIntegration.Application.Interfaces;
using Tnf.AspNetCore.Mvc.Response;
using Hangfire;
using Thex.HigsIntegration.Application.Jobs;

namespace Thex.HigsIntegration.Web.Controllers
{
    [Route(RouteConsts.RatePlanListRouteName)]
    [AllowAnonymous]
    public class RatePlanListController : TnfController
    {
        private readonly IRatePlanListAppService _ratePlanListAppService;

        public RatePlanListController(IRatePlanListAppService ratePlanListAppService)
        {
            _ratePlanListAppService = ratePlanListAppService;
        }

        [HttpPost]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult ProcessRatePlanListAsync()
        {
            BackgroundJob.Enqueue<RatePlanListJob>(j => j.Execute());

            return Ok();
        }
    }
}