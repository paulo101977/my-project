﻿using System;
using System.IO.Compression;
using System.Threading.Tasks;
using Hangfire;
using Hangfire.MemoryStorage;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.ResponseCompression;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Thex.HigsIntegration.Dto;
using Thex.HigsIntegration.Infra;
using Thex.HigsIntegration.Infra.Entities;
using Thex.Kernel;
using Thex.HigsIntegration.Web.Activator;
using Thex.Location.Application;
using Tnf.Configuration;
using Thex.Maps.Geocode;

namespace Thex.HigsIntegration.Web
{
    public class Startup
    {
        DatabaseConfiguration DatabaseConfiguration { get; }
        private IConfiguration Configuration { get; set; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
            DatabaseConfiguration = new DatabaseConfiguration(configuration);
        }

        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            services.Configure<GzipCompressionProviderOptions>(
                options => options.Level = CompressionLevel.Optimal);

            services
                .AddCorsAll("AllowAll")
                .AddApplicationServiceLocationDependency(options =>
                {
                    var settingsSection = Configuration.GetSection("MapsGeocodeConfig");
                    var settings = settingsSection.Get<MapsGeocodeConfig>();
                }, Configuration)
                .AddApplicationServiceDependency(Configuration)
                .AddTnfAspNetCore()
                .AddResponseCompression(options =>
                {
                    options.Providers.Add<GzipCompressionProvider>();
                    options.EnableForHttps = true;
                });

            services
                .AddThexAspNetCoreSecurity(options =>
                {
                    var settingsSection = Configuration.GetSection("TokenConfiguration");
                    var settings = settingsSection.Get<AspNetCore.Security.TokenConfiguration>();

                    options.TokenConfiguration = settings;
                    options.FrontEndpoint = Configuration.GetValue<string>("UrlFront");
                    options.SuperAdminEndpoint = Configuration.GetValue<string>("SuperAdminEndpoint");

                });

            services.AddHangfire(c => c.UseMemoryStorage());

            services.AddSwaggerDocumentation();

            services.AddAntiforgery(options =>
            {
                options.Cookie.Name = "X-CSRF-TOKEN-GOTNEXT-COOKIE";
                options.HeaderName = "X-CSRF-TOKEN-GOTNEXT-HEADER";
                options.SuppressXFrameOptionsHeader = false;
            })
            .AddMvc()
            .AddJsonOptions(opts =>
            {
                opts.SerializerSettings.NullValueHandling =
                    Newtonsoft.Json.NullValueHandling.Ignore;
            });

            var serviceProvider = services.BuildServiceProvider();

            ServiceLocator.SetLocatorProvider(serviceProvider);

            return serviceProvider;
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env, IServiceProvider serviceProvider, ILogger<Startup> logger)
        {
            app.UseCors("AllowAll");

            // Configura o use do AspNetCore do Tnf
            app.UseTnfAspNetCore(options =>
            {
                options.UseDomainLocalization();
                // Configura a connection string da aplicação
                options.DefaultNameOrConnectionString = DatabaseConfiguration.ConnectionString;

                options.Repository(repositoryConfig =>
                {
                    repositoryConfig.Entity<IEntityGuid>(entity =>
                        entity.RequestDto<IDefaultGuidRequestDto>((e, d) => e.Id == d.Id));

                    repositoryConfig.Entity<IEntityInt>(entity =>
                        entity.RequestDto<IDefaultIntRequestDto>((e, d) => e.Id == d.Id));

                    repositoryConfig.Entity<IEntityLong>(entity =>
                        entity.RequestDto<IDefaultLongRequestDto>((e, d) => e.Id == d.Id));
                });
            });

            if (env.IsDevelopment())
                app.UseDeveloperExceptionPage();

            app.UseSwaggerDocumentation();

            // Habilita o uso do UnitOfWork em todo o request
            //app.UseTnfUnitOfWork();

            app.UseThexAspNetCoreSecurity();

            SetGlobalConfigurations(serviceProvider);

            app.UseHangfireServer();

            app.UseMvcWithDefaultRoute();
            app.UseResponseCompression();

            app.Run(context =>
            {
                context.Response.Redirect("/swagger");
                return Task.CompletedTask;
            });

            logger.LogInformation("Start application ...");
        }

        public void SetGlobalConfigurations(IServiceProvider serviceProvider)
        {
            GlobalConfiguration.Configuration.UseActivator(new HangfireActivator(serviceProvider));
            GlobalJobFilters.Filters.Add(new AutomaticRetryAttribute { Attempts = 0 });
        }
    }
}