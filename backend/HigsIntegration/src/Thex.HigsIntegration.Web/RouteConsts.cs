﻿namespace Thex.HigsIntegration.Web
{
    public class RouteConsts
    {
        public const string CustomerStationRouteName = "api/customerStation";
        public const string ReservationDownloadRouteName = "api/reservationDownload";
        public const string RatePlanListRouteName = "api/ratePlanList";
        public const string GuestInformationRouteName = "api/guestInformation";
        public const string GuestRelationRouteName = "api/guestRelation";
    }
}
