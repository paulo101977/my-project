﻿using Hangfire;
using System;


namespace Thex.HigsIntegration.Web.Activator
{
    public class HangfireActivator : JobActivator
    {
        private IServiceProvider _serviceProvider;

        public HangfireActivator(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public override object ActivateJob(Type type) => _serviceProvider.GetService(type);
    }
}
