﻿using Microsoft.EntityFrameworkCore;
using Thex.HigsIntegration.Infra.Entities;
using Tnf.EntityFrameworkCore;
using Tnf.Runtime.Session;

namespace Thex.HigsIntegration.Infra.Context
{
    public class ThexHigsIntegrationContext : TnfDbContext
    {
        public DbSet<CustomerStation> CustomerStations { get; set; }
        public DbSet<ContactInformation> ContactInformations { get; set; }
        public DbSet<Person> Persons { get; set; }
        public DbSet<IntegrationPartnerProperty> IntegrationPartnerProperties { get; set; }

        // Importante o construtor do contexto receber as opções com o tipo generico definido: DbContextOptions<TDbContext>
        public ThexHigsIntegrationContext(DbContextOptions<ThexHigsIntegrationContext> options, ITnfSession session)
            : base(options, session)
        {
        }
    }
}
