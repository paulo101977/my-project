﻿using System;

namespace Thex.HigsIntegration.Infra.Entities
{
    [Serializable]
    public abstract class ThexMultiTenantNullableFullAuditedEntity : ThexFullAuditedEntity
    {
        public Guid? TenantId { get; set; }
        //public Tenant Tenant { get; set; }
    }
}
