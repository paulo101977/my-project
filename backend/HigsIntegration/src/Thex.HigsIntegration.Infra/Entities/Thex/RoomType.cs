﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Thex.HigsIntegration.Infra.Entities.Thex
{
    public class RoomType
    {
        public int Id { get; set; }
        public int PropertyId { get; internal set; }
        public int Order { get; internal set; }
        public string Name { get; internal set; }
        public string Abbreviation { get; internal set; }
        public int AdultCapacity { get; internal set; }
        public int ChildCapacity { get; internal set; }
        public int FreeChildQuantity1 { get; internal set; }
        public int FreeChildQuantity2 { get; internal set; }
        public int FreeChildQuantity3 { get; internal set; }
        public bool IsActive { get; internal set; }
        public decimal MaximumRate { get; internal set; }
        public decimal MinimumRate { get; internal set; }
        public string DistributionCode { get; internal set; }
    }
}
