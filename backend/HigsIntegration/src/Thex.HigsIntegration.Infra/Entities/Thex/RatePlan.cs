﻿using System;

namespace Thex.HigsIntegration.Infra.Entities.Thex
{
    public class RatePlan : ThexMultiTenantFullAuditedEntity
    {
        public Guid Id { get; set; }
        public int AgreementTypeId { get; internal set; }
        public string AgreementName { get; internal set; }
        public DateTime StartDate { get; internal set; }
        public DateTime? EndDate { get; internal set; }
        public int PropertyId { get; internal set; }
        public int MealPlanTypeId { get; internal set; }
        public bool? IsActive { get; internal set; }
        public Guid CurrencyId { get; internal set; }
        public string CurrencySymbol { get; internal set; }
        public bool RateNet { get; internal set; }
        public int RateTypeId { get; internal set; }
        public int? MarketSegmentId { get; internal set; }
        public string DistributionCode { get; internal set; }
        public string Description { get; internal set; }
        public string HigsCode { get; internal set; }
    }
}
