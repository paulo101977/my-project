﻿using System;
using Tnf.Notifications;

namespace Thex.HigsIntegration.Infra.Entities
{
    public partial class Person
    {
        public Guid Id { get; set; }
        public string FirstName { get; internal set; }
        public string LastName { get; internal set; }
        public string FullName { get; internal set; }
        public string PersonType { get; internal set; }
        public DateTime? DateOfBirth { get; internal set; }
        public int? CountrySubdivisionId { get; internal set; }

        public enum EntityError
        {
            PersonMustHavePersonType,
            PersonOutOfBoundPersonType,
            PersonMustHaveFirstName,
            PersonOutOfBoundFirstName,
            PersonOutOfBoundLastName,
            PersonOutOfBoundFullName,
            PersonInvalidDateOfBirth
        }

        public static Builder Create(INotificationHandler handler)
            => new Builder(handler);

        public static Builder Create(INotificationHandler handler, Person instance)
            => new Builder(handler, instance);
    }
}
