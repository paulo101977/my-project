﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Thex.HigsIntegration.Infra.Entities.Thex
{
     public class Channel : ThexMultiTenantFullAuditedEntity
    {
        public Guid Id { get; set; }
        public int ChannelCodeId { get; internal set; }
        public string Description { get; internal set; }
        public int? BusinessSourceId { get; internal set; }
        public decimal? DistributionAmount { get; internal set; }
        public int? MarketSegmentId { get; internal set; }
        public bool IsActive { get; internal set; }
    }
}
