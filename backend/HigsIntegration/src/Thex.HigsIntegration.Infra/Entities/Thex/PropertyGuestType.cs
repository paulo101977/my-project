﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Thex.HigsIntegration.Infra.Entities.Thex
{
    public class PropertyGuestType
    {
        public int Id { get; set; }
        public int PropertyId { get; internal set; }
        public string GuestTypeName { get; internal set; }
        public bool IsVip { get; internal set; }

        public string Code { get; internal set; }
        public bool? IsIncognito { get; internal set; }
        public bool? IsActive { get; internal set; }
    }
}
