﻿using Tnf.Notifications;

namespace Thex.HigsIntegration.Infra.Entities
{
    public partial class Occupation
    {
        public int Id { get; set; }
        public string Name { get; internal set; }
        public int CompanyId { get; internal set; }

        public enum EntityError
        {
            OccupationMustHaveName,
            OccupationOutOfBoundName,
            OccupationMustHaveCompanyId
        }

        public static Builder Create(INotificationHandler handler)
           => new Builder(handler);

        public static Builder Create(INotificationHandler handler, Occupation instance)
            => new Builder(handler, instance);
    }
}
