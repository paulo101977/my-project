﻿using System;

namespace Thex.HigsIntegration.Infra.Entities.Thex
{
    public class CompanyClientChannel
    {
        public Guid Id { get; set; }
        public Guid ChannelId { get; internal set; }
        public Guid CompanyClientId { get; internal set; }
        public bool IsActive { get; internal set; }
        public string CompanyId { get; internal set; }
    }
}
