﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Thex.HigsIntegration.Infra.Entities.Thex
{
    public class RoomLayout
    {
        public Guid Id { get; set; }
        public byte QuantitySingle { get; internal set; }
        public byte QuantityDouble { get; internal set; }
    }
}
