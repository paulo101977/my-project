﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Thex.HigsIntegration.Infra.Entities.Thex
{
    public class Currency : ThexFullAuditedEntity
    {
        public Guid Id { get; set; }
        public string TwoLetterIsoCode { get; internal set; }
        public string CurrencyName { get; internal set; }
        public string AlphabeticCode { get; internal set; }
        public string NumnericCode { get; internal set; }
        public int MinorUnit { get; internal set; }
        public string Symbol { get; internal set; }
        public decimal ExchangeRate { get; internal set; }
    }
}
