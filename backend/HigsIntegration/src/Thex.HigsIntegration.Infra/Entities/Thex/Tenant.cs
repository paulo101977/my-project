﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Thex.HigsIntegration.Infra.Entities.Thex
{
    public class Tenant
    {
        public Guid Id { get; set; }
        public string TenantName { get; set; }
        public Guid? ParentId { get; set; }
        public bool IsActive { get; set; }
        public virtual Tenant Parent { get; set; }
        public virtual ICollection<Tenant> ChildList { get; set; }
    }
}
