﻿using System;

namespace Thex.HigsIntegration.Infra.Entities
{
    public partial class IntegrationPartnerProperty
    {
        public Guid Id { get; internal set; }
        public int PropertyId { get; internal set; }
        public int? IsActive { get; internal set; }
        public string IntegrationCode { get; internal set; }
        public int PartnerId { get; internal set; }
    }
}
