﻿using System;

namespace Thex.HigsIntegration.Infra.Entities
{
    public class BaseEntity : ThexFullAuditedEntity, IEntityGuid
    {
        public Guid Id { get; set; }
    }
}
