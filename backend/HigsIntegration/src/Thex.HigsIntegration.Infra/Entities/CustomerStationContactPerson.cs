﻿using System;
using Tnf.Notifications;

namespace Thex.HigsIntegration.Infra.Entities
{
    public partial class CustomerStationContactPerson: IEntityGuid
    {
        public Guid Id { get; set; }
        public Guid PersonId { get; internal set; }
        public Guid CustomerStationId { get; internal set; }
        public int? OccupationId { get; internal set; }

        public enum EntityError
        {
            CustomerStationContactPersonMustHavePerson,
            CustomerStationContactPersonMustHaveCustomerStation
        }

        public static Builder Create(INotificationHandler handler)
            => new Builder(handler);

        public static Builder Create(INotificationHandler handler, CustomerStationContactPerson instance)
            => new Builder(handler, instance);
    }

}
