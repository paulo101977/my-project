﻿using System;
using Tnf.Notifications;

namespace Thex.HigsIntegration.Infra.Entities
{
    public partial class CustomerStation : BaseEntity, IEntityGuid
    {
        public string CustomerStationName { get; internal set; }
        public string SocialReason { get; internal set; }
        public bool IsActive { get; internal set; }
        public Guid? CustomerStationCategoryId { get; internal set; }
        public Guid CompanyClientId { get; internal set; }
        public string HomePage { get; internal set; }

        public enum EntityError
        {
            CustomerStationMustHaveCustomerStationName,
            CustomerStationMustHaveSocialReason,
            CustomerStationMustHaveCustomerStationCattegoryId,
            CustomerStationMustHaveCompanyClientId,
            CustomerStationMustHaveHomePage,
            CustomerStationMustHaveAddress
        }

        public static Builder Create(INotificationHandler handler)
            => new Builder(handler);

        public static Builder Create(INotificationHandler handler, CustomerStation instance)
            => new Builder(handler, instance);
    }
}
