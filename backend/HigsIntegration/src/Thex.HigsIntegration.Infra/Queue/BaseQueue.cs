﻿using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Queue;
using Microsoft.Extensions.Configuration;
using Thex.HigsIntegration.Infra.Constants;
using System.Threading.Tasks;
using Thex.HigsIntegration.Infra.Interfaces.Queue;

namespace Thex.HigsIntegration.Infra.Queue
{
    public class BaseQueue : IBaseQueue
    {
        protected readonly IConfiguration Configuration;

        public BaseQueue(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public virtual CloudQueueClient GetCloudQueueClient()
        {
            var storageAccount = CloudStorageAccount.Parse(Configuration[HigsIntegrationConstants.STORAGECONNECTIONSTRING]);

            return storageAccount.CreateCloudQueueClient();
        }

        public virtual CloudQueue GetQueue(CloudQueueClient queueClient, string queueName)
        {
            return queueClient.GetQueueReference(queueName);
        }

        public virtual async Task Insert(CloudQueue queue, string json)
        {
            await queue.AddMessageAsync(new CloudQueueMessage(json));
        }

        public virtual async Task CreateQueueIfNotExists(CloudQueue queue)
        {
            await queue.CreateIfNotExistsAsync();
        }

        public virtual async Task<CloudQueue> GetAndCreateQueueIfNotExists(string queueName)
        {
            var queueClient = GetCloudQueueClient();
            var queue = GetQueue(queueClient, queueName);

            await CreateQueueIfNotExists(queue);

            return queue;
        }
    }
}
