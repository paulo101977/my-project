﻿using Microsoft.WindowsAzure.Storage.Queue;
using Microsoft.Extensions.Configuration;
using Thex.HigsIntegration.Infra.Constants;
using System.Threading.Tasks;
using Thex.HigsIntegration.Infra.Interfaces.Queue;

namespace Thex.HigsIntegration.Infra.Queue
{
    public class GuestRelationListQueue : BaseQueue, IGuestRelationListQueue
    {
        private readonly IConfiguration _configuration;

        public GuestRelationListQueue(IConfiguration configuration)
            : base(configuration)
        {
            _configuration = configuration;
        }

        public async Task<CloudQueue> GetQueue()
            => await GetAndCreateQueueIfNotExists(_configuration.GetSection("Queues").GetValue<string>("GuestRelationListQueue"));
    }
}
