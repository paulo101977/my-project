﻿namespace Thex.HigsIntegration.Infra.Constants
{
    public class HigsIntegrationConstants
    {
        public const string HIGSURLAPI = "HigsUrlApi";

        public const string HIGSTOKENCLIENTSETTINGS = "HigsTokenClient";
        public const string HIGSTOKENAPPLICATIONSETTINGS = "HigsTokenApplication";

        public const string HIGSTOKENCLIENT = "Token-Client";
        public const string HIGSTOKENAPPLICATION = "Token-Application";

        public const string RESERVATIONDOWNLOAD = "ReservationDownload";
        public const string RATEPLANLIST = "RatePlanList";
        public const string GUESTRELATION = "GuestRelation";
        public const string GUESTINFORMATION = "GuestInformation";


        public const string STORAGECONNECTIONSTRING = "ConnectionStrings:Storage:Thex";

    }

    public class IntegrationQueueConstants
    {
        public const string RESERVATIONDOWNLOAD = "reservationdownload";
        public const string RATEPLANLIST = "rateplanlist";
        public const string GUESTINFORMATIONLIST = "guestinformation";
        public const string GUESTRELATIONLIST = "guestrelation";
    }
}
