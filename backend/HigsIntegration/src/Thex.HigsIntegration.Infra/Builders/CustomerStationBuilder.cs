﻿using System;
using Thex.Common;
using Tnf.Builder;
using Tnf.Notifications;
using Tnf.Specifications;

namespace Thex.HigsIntegration.Infra.Entities
{
    public partial class CustomerStation
    {
        public class Builder : Builder<CustomerStation>
        {
            public Builder(INotificationHandler handler) : base(handler)
            {
            }

            public Builder(INotificationHandler handler, CustomerStation instance) : base(handler, instance)
            {
            }

            public virtual Builder WithId(Guid id)
            {
                Instance.Id = id;
                return this;
            }

            public virtual Builder WithCustomerStationName(string customerStationName)
            {
                Instance.CustomerStationName = customerStationName;
                return this;
            }

            public virtual Builder WithSocialReason(string socialReason)
            {
                Instance.SocialReason = socialReason;
                return this;
            }

            public virtual Builder WithCustomerStationCategoryId(Guid? customerStationCategoryId)
            {
                Instance.CustomerStationCategoryId = customerStationCategoryId;
                return this;
            }

            public virtual Builder WithIsActive(bool isActive)
            {
                Instance.IsActive = isActive;
                return this;
            }

            public virtual Builder WithCompanyClientId(Guid companyClientId)
            {
                Instance.CompanyClientId = companyClientId;
                return this;
            }

            public virtual Builder WithHomePage(string homePage)
            {
                Instance.HomePage = homePage;
                return this;
            }

            protected override void Specifications()
            {
                AddSpecification(new ExpressionSpecification<CustomerStation>(
                    AppConsts.LocalizationSourceName,
                    CustomerStation.EntityError.CustomerStationMustHaveCustomerStationName,
                    w => !string.IsNullOrWhiteSpace(w.CustomerStationName)));

                AddSpecification(new ExpressionSpecification<CustomerStation>(
                    AppConsts.LocalizationSourceName,
                    CustomerStation.EntityError.CustomerStationMustHaveSocialReason,
                    w => !string.IsNullOrWhiteSpace(w.SocialReason)));

                AddSpecification(new ExpressionSpecification<CustomerStation>(
                AppConsts.LocalizationSourceName,
                CustomerStation.EntityError.CustomerStationMustHaveCompanyClientId,
                w => w.CompanyClientId != Guid.Empty));

            }
        }
    }
}
