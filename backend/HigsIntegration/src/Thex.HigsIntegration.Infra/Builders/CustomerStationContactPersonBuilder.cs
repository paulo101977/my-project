﻿using System;
using Thex.Common;
using Tnf.Builder;
using Tnf.Notifications;
using Tnf.Specifications;

namespace Thex.HigsIntegration.Infra.Entities
{
    public partial class CustomerStationContactPerson
    {
        public class Builder : Builder<CustomerStationContactPerson>
        {
            public Builder(INotificationHandler handler) : base(handler)
            {
            }

            public Builder(INotificationHandler handler, CustomerStationContactPerson instance) : base(handler, instance)
            {
            }

            public virtual Builder WithId(Guid id)
            {
                Instance.Id = id;
                return this;
            }

            public virtual Builder WithPersonId(Guid personId)
            {
                Instance.PersonId = personId;
                return this;
            }

            public virtual Builder WithCustomerStationId(Guid customerStationId)
            {
                Instance.CustomerStationId = customerStationId;
                return this;
            }

            public virtual Builder WithOccupationId(int? occupationId)
            {
                Instance.OccupationId = occupationId;
                return this;
            }

            protected override void Specifications()
            {
                AddSpecification(new ExpressionSpecification<CustomerStationContactPerson>(
                    AppConsts.LocalizationSourceName,
                    CustomerStationContactPerson.EntityError.CustomerStationContactPersonMustHavePerson,
                    w => w.PersonId != Guid.Empty));

                AddSpecification(new ExpressionSpecification<CustomerStationContactPerson>(
                    AppConsts.LocalizationSourceName,
                    CustomerStationContactPerson.EntityError.CustomerStationContactPersonMustHavePerson,
                    w => w.CustomerStationId != Guid.Empty));
            }
        }
    }
}
