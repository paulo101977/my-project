﻿using System;
using System.Collections.Generic;
using System.Text;
using Thex.Common;
using Tnf.Builder;
using Tnf.Notifications;
using Tnf.Specifications;

namespace Thex.HigsIntegration.Infra.Entities
{
    public partial class Occupation
    {
        public class Builder : Builder<Occupation>
        {
            public Builder(INotificationHandler handler) : base(handler)
            {
            }

            public Builder(INotificationHandler handler, Occupation instance) : base(handler, instance)
            {
            }

            public virtual Builder WithId(int id)
            {
                Instance.Id = id;
                return this;
            }
            public virtual Builder WithName(string name)
            {
                Instance.Name = name;
                return this;
            }
            public virtual Builder WithCompanyId(int companyId)
            {
                Instance.CompanyId = companyId;
                return this;
            }

            protected override void Specifications()
            {
                AddSpecification(new ExpressionSpecification<Occupation>(
                    AppConsts.LocalizationSourceName,
                    Occupation.EntityError.OccupationMustHaveName,
                    w => !string.IsNullOrWhiteSpace(w.Name)));

                AddSpecification(new ExpressionSpecification<Occupation>(
                    AppConsts.LocalizationSourceName,
                    Occupation.EntityError.OccupationOutOfBoundName,
                    w => string.IsNullOrWhiteSpace(w.Name) || w.Name.Length > 0 && w.Name.Length <= 50));

                AddSpecification(new ExpressionSpecification<Occupation>(
                    AppConsts.LocalizationSourceName,
                    Occupation.EntityError.OccupationMustHaveCompanyId,
                    w => w.CompanyId != default(int)));

            }
        }
    }
}
