﻿using System;
using System.Threading.Tasks;
using Thex.Common;
using Thex.Common.Enumerations;
using Thex.HigsIntegration.Dto.Dto;
using Thex.HigsIntegration.Infra.Context;
using Thex.HigsIntegration.Infra.Entities;
using Thex.HigsIntegration.Infra.Extensions;
using Thex.HigsIntegration.Infra.Infra;
using Thex.HigsIntegration.Infra.Interfaces;
using Thex.HigsIntegration.Infra.Interfaces.ReadRepositories;
using Thex.Kernel;
using Tnf.Dapper.Repositories;
using Tnf.Dto;
using Tnf.Notifications;
using Tnf.Repositories;

namespace Thex.HigsIntegration.Infra.Repositories
{
    public class CustomerStationRepository : DapperEfRepositoryBase<ThexHigsIntegrationContext, CustomerStation>, ICustomerStationRepository
    {
        private readonly IApplicationUser _applicationUser;
        private readonly INotificationHandler _notificationHandler;
        private readonly ICustomerStationReadRepository _customerStationReadRepository;

        public CustomerStationRepository(IActiveTransactionProvider activeTransactionProvider,
                                        ICustomerStationReadRepository customerStationReadRepository,
                                        INotificationHandler notificationHandler,
                                        IApplicationUser applicationUser)
            : base(activeTransactionProvider)
        {
            _applicationUser = applicationUser;
            _notificationHandler = notificationHandler;
            _customerStationReadRepository = customerStationReadRepository;
        }

        public async Task<IDto> InsertAsync(BaseEntity baseEntity)
        {
            await InsertAsync(entity: baseEntity.SetOperationValues(Operation.Insert, _applicationUser) as CustomerStation);
            var customerStationDto = baseEntity.MapTo<CustomerStationDto>();
            return customerStationDto;
        }

        public async Task RemoveAsync(Guid id)
        {
            var customerStation = await _customerStationReadRepository.GetCustomerStationById(id);

            if (customerStation == null)
            {
                _notificationHandler.Raise(_notificationHandler
                                    .DefaultBuilder
                                    .WithMessage(AppConsts.LocalizationSourceName, CommonsEnum.Error.ParameterInvalid)
                                    .Build());
            }

            await UpdateAsync(customerStation.SetOperationValues(Operation.Delete, _applicationUser) as CustomerStation);
        }

        public async Task ToggleAsync(Guid id)
        {
            var customerStation = await _customerStationReadRepository.GetCustomerStationById(id);

            if (customerStation == null)
            {
                _notificationHandler.Raise(_notificationHandler
                                    .DefaultBuilder
                                    .WithMessage(AppConsts.LocalizationSourceName, CommonsEnum.Error.ParameterInvalid)
                                    .Build());
            }

            customerStation.IsActive = !customerStation.IsActive;

            await UpdateAsync((customerStation.SetOperationValues(Operation.Update, _applicationUser) as CustomerStation));
        }

        public async Task<IDto> UpdateAsync(Guid id, BaseEntity baseEntity)
        {
            var customerStation = await _customerStationReadRepository.GetCustomerStationById(id);

            // Ajustes dapper
            baseEntity.Id = customerStation.Id;
            baseEntity.CreationTime = customerStation.CreationTime;
            baseEntity.CreatorUserId = customerStation.CreatorUserId;

            await UpdateAsync((baseEntity.SetOperationValues(Operation.Update, _applicationUser) as CustomerStation));

            return baseEntity.MapTo<CustomerStationDto>();
        }
    }
}
