﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.HigsIntegration.Infra.Context;
using Thex.HigsIntegration.Infra.Entities;
using Thex.HigsIntegration.Infra.Interfaces;
using Tnf.Dapper.Repositories;
using Tnf.Repositories;

namespace Thex.HigsIntegration.Infra.Repositories
{
    public class PersonRepository : DapperEfRepositoryBase<ThexHigsIntegrationContext, Person>, IPersonRepository
    {
        public PersonRepository(IActiveTransactionProvider activeTransactionProvider)
         : base(activeTransactionProvider)
        {

        }

        public async Task Add(Person person)
        {
            await InsertAsync(person);
        }

        public async Task AddRange(ICollection<Person> persons)
        {
            await InsertAsync(persons);
        }

        public new async Task Update(Person person)
        {
            await UpdateAsync(person);
        }

        public async Task Remove(Person person)
        {
            await DeleteAsync(person);
        }
    }
}
