﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.HigsIntegration.Infra.Context;
using Thex.HigsIntegration.Infra.Entities;
using Thex.HigsIntegration.Infra.Interfaces;
using Tnf.Dapper.Repositories;
using Tnf.Repositories;

namespace Thex.HigsIntegration.Infra.Repositories
{
    public class CustomerStationContactPersonRepository : DapperEfRepositoryBase<ThexHigsIntegrationContext, CustomerStationContactPerson>, ICustomerStationContactPersonRepository
    {
        public CustomerStationContactPersonRepository(IActiveTransactionProvider activeTransactionProvider)
          : base(activeTransactionProvider)
        {
        }

        public new async Task InsertAsync(CustomerStationContactPerson customerStationContactPerson)
        {
            await InsertAsync(customerStationContactPerson);
        }

        public async Task InsertRangeAsync(ICollection<CustomerStationContactPerson> customerStationContactPerson)
        {
            await InsertAsync(customerStationContactPerson);
        }

        public async Task RemoveAsync(CustomerStationContactPerson customerStationContactPerson)
        {
            await DeleteAsync(customerStationContactPerson);
        }

        public async Task RemoveRangeAsync(ICollection<CustomerStationContactPerson> customerStationContactPerson)
        {
            foreach (var item in customerStationContactPerson)
            {
                await DeleteAsync(item);
            }
        }

    }
}
