﻿using System.Linq;
using System.Threading.Tasks;
using Thex.HigsIntegration.Infra.Context;
using Thex.HigsIntegration.Infra.Entities.Thex;
using Thex.HigsIntegration.Infra.Interfaces.ReadRepositories;
using Tnf.Dapper.Repositories;
using Tnf.Repositories;

namespace Thex.HigsIntegration.Infra.Repositories.ReadRepositories
{
    public class PropertyParameterReadRepository : DapperEfRepositoryBase<ThexHigsIntegrationContext, PropertyParameter>, IPropertyParameterReadRepository
    {
        public PropertyParameterReadRepository(IActiveTransactionProvider activeTransactionProvider)
            : base(activeTransactionProvider)
        {
        }

        public async Task<string> GetTimeZoneByPropertyIdAsync(int propertyId)
         => (await QueryAsync<string>(@"
                SELECT      PP.PropertyParameterValue                         
                FROM        PropertyParameter PP 
                WHERE       PP.PropertyId = @propertyId AND PP.ApplicationParameterId = 12",
                new { propertyId}))
             .FirstOrDefault();
    }
}
