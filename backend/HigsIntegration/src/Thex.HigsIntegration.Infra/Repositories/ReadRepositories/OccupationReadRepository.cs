﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Thex.HigsIntegration.Infra.Context;
using Thex.HigsIntegration.Infra.Entities;
using Thex.HigsIntegration.Infra.Interfaces.ReadRepositories;
using Tnf.Dapper.Repositories;
using Tnf.Repositories;

namespace Thex.HigsIntegration.Infra.Repositories.ReadRepositories
{
    public class OccupationReadRepository : DapperEfRepositoryBase<ThexHigsIntegrationContext, Occupation>, IOccupationReadRepository
    {
        public OccupationReadRepository(IActiveTransactionProvider activeTransactionProvider)
        : base(activeTransactionProvider)
        {

        }

        public async Task<Occupation> GetById(int id)
        {
            var query = await QueryAsync<Occupation>(@"
                 SELECT OccupationId as Id,
                       Occupation.*
                FROM   Occupation
                WHERE  OccupationId = @id",
               new { Id = id });


            return query.FirstOrDefault();
        }
    }
}
