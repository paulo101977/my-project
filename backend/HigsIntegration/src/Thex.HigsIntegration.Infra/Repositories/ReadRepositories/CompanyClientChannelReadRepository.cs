﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Thex.HigsIntegration.Infra.Context;
using Thex.HigsIntegration.Infra.Entities.Thex;
using Thex.HigsIntegration.Infra.Interfaces.ReadRepositories;
using Tnf.Dapper.Repositories;
using Tnf.Repositories;

namespace Thex.HigsIntegration.Infra.Repositories.ReadRepositories
{
    public class CompanyClientChannelReadRepository : DapperEfRepositoryBase<ThexHigsIntegrationContext, CompanyClientChannel>, ICompanyClientChannelReadRepository
    {
        public CompanyClientChannelReadRepository(IActiveTransactionProvider activeTransactionProvider)
            : base(activeTransactionProvider)
        {
        }

        public async Task<CompanyClientChannel> GetByCompanyIdAsync(string companyId, Guid channelId, Guid tenantId)
           => (await QueryAsync<CompanyClientChannel>(@"
                SELECT      CCC.CompanyClientChannelId as Id                          
                FROM        CompanyClientChannel CCC 
                INNER JOIN  Channel C
                ON          CCC.ChannelId = C.ChannelId
                INNER JOIN  CompanyClient CC
                ON          CCC.CompanyClientId = CC.CompanyClientId
                WHERE       C.TenantId = @tenantId AND CCC.CompanyId = @companyId AND CCC.ChannelId = @channelId AND CCC.IsActive = 1",
               new { companyId, channelId, tenantId }))
               .FirstOrDefault();
    }
}
