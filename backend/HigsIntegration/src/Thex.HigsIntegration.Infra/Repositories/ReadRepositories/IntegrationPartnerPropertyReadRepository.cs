﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Thex.Common.Enumerations;
using Thex.HigsIntegration.Dto.Dto;
using Thex.HigsIntegration.Infra.Context;
using Thex.HigsIntegration.Infra.Entities;
using Thex.HigsIntegration.Infra.Interfaces.ReadRepositories;
using Tnf.Dapper.Repositories;
using Tnf.Repositories;

namespace Thex.HigsIntegration.Infra.Repositories.ReadRepositories
{
    public class IntegrationPartnerPropertyReadRepository : DapperEfRepositoryBase<ThexHigsIntegrationContext, IntegrationPartnerProperty>, IIntegrationPartnerPropertyReadRepository
    {
        public IntegrationPartnerPropertyReadRepository(IActiveTransactionProvider activeTransactionProvider)
        : base(activeTransactionProvider)
        {

        }

        public async Task<ICollection<IntegrationPartnerPropertyDto>> GetAllByPartner(PartnerEnum partnerEnum)
            => (await QueryAsync<IntegrationPartnerPropertyDto>(@"
                SELECT IPP.IntegrationPartnerPropertyId as Id,
                       P.TenantId as TenantId,
                       IPP.*
                FROM   IntegrationPartnerProperty IPP
                INNER JOIN Property P
                ON IPP.PropertyId = P.PropertyId
                WHERE  PartnerId = @Id",
                new { Id = (int)partnerEnum }))
            .ToList();

        public async Task<IntegrationPartnerPropertyDto> GetByIntegrationCode(string integrationCode)
        => (await QueryAsync<IntegrationPartnerPropertyDto>(@"
                SELECT IntegrationPartnerPropertyId as Id,
                       IntegrationPartnerProperty.*
                FROM   IntegrationPartnerProperty
                WHERE  IntegrationCode = @IntegrationCode",
                new { IntegrationCode = integrationCode }))
            .FirstOrDefault();

        public async Task<bool> AnyByIntegrationCode(string integrationCode)
        => (await QueryAsync<object>(@"
                SELECT 1
                FROM   IntegrationPartnerProperty
                WHERE  IntegrationCode = @IntegrationCode",
                new { IntegrationCode = integrationCode }))
            .Any();
    }
}
