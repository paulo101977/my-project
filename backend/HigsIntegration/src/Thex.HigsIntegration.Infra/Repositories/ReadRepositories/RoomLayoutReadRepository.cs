﻿using System.Linq;
using System.Threading.Tasks;
using Thex.HigsIntegration.Infra.Context;
using Thex.HigsIntegration.Infra.Entities.Thex;
using Thex.HigsIntegration.Infra.Interfaces.ReadRepositories;
using Tnf.Dapper.Repositories;
using Tnf.Repositories;

namespace Thex.HigsIntegration.Infra.Repositories.ReadRepositories
{
    public class RoomLayoutReadRepository : DapperEfRepositoryBase<ThexHigsIntegrationContext, RoomLayout>, IRoomLayoutReadRepository
    {
        public RoomLayoutReadRepository(IActiveTransactionProvider activeTransactionProvider)
            : base(activeTransactionProvider)
        {
        }

        public async Task<RoomLayout> GetDefaultAsync()
       => (await QueryAsync<RoomLayout>(@"
                SELECT      RL.RoomLayoutId as Id                          
                FROM        RoomLayout RL"))
           .FirstOrDefault();
    }
}
