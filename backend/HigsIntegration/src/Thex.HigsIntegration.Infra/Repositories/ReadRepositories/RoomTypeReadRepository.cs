﻿using System.Linq;
using System.Threading.Tasks;
using Thex.HigsIntegration.Infra.Context;
using Thex.HigsIntegration.Infra.Entities.Thex;
using Thex.HigsIntegration.Infra.Interfaces.ReadRepositories;
using Tnf.Dapper.Repositories;
using Tnf.Repositories;

namespace Thex.HigsIntegration.Infra.Repositories.ReadRepositories
{
    public class RoomTypeReadRepository : DapperEfRepositoryBase<ThexHigsIntegrationContext, RoomType>, IRoomTypeReadRepository
    {
        public RoomTypeReadRepository(IActiveTransactionProvider activeTransactionProvider)
            : base(activeTransactionProvider)
        {
        }

        public async Task<RoomType> GetByDistributionCodeAsync(string distributionCode, int propertyId)
       => (await QueryAsync<RoomType>(@"
                SELECT      RT.RoomTypeId as Id                          
                FROM        RoomType RT 
                WHERE       RT.PropertyId = @propertyId AND RT.DistributionCode = @distributionCode AND RT.IsActive = 1",
           new { propertyId, distributionCode }))
           .FirstOrDefault();
    }
}
