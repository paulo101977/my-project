﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Thex.HigsIntegration.Infra.Context;
using Thex.HigsIntegration.Infra.Entities;
using Thex.HigsIntegration.Infra.Interfaces.ReadRepositories;
using Tnf.Dapper.Repositories;
using Tnf.Repositories;

namespace Thex.HigsIntegration.Infra.Repositories.ReadRepositories
{
    public class ContactInformationReadRepository : DapperEfRepositoryBase<ThexHigsIntegrationContext, ContactInformation>, IContactInformationReadRepository
    {
        public ContactInformationReadRepository(IActiveTransactionProvider activeTransactionProvider)
       : base(activeTransactionProvider)
        {

        }

        public async Task<ICollection<ContactInformation>> GetContactInformationsByPerson(Guid id)
        {
            var query = await QueryAsync<ContactInformation>(@"
                SELECT *
                FROM  ContactInformation
                WHERE OwnerId = @Id",
                new { Id = id });

            return query.ToList();
        }

        public async Task<ContactInformation> GetContactInformationByTypeAndPerson(Guid id, int contactInformationType)
        {
            var query = await QueryAsync<ContactInformation>(@"
                SELECT *
                FROM  ContactInformation
                WHERE OwnerId = @Id && ContactInformationTypeId = @Type",
               new { Id = id, Type = contactInformationType });

            return query.FirstOrDefault();
        }
    }
}
