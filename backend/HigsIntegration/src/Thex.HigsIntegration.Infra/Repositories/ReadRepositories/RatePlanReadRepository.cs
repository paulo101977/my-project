﻿using System.Linq;
using System.Threading.Tasks;
using Thex.HigsIntegration.Infra.Context;
using Thex.HigsIntegration.Infra.Entities.Thex;
using Thex.HigsIntegration.Infra.Interfaces.ReadRepositories;
using Tnf.Dapper.Repositories;
using Tnf.Repositories;

namespace Thex.HigsIntegration.Infra.Repositories.ReadRepositories
{
    public class RatePlanReadRepository : DapperEfRepositoryBase<ThexHigsIntegrationContext, RatePlan>, IRatePlanReadRepository
    {
        public RatePlanReadRepository(IActiveTransactionProvider activeTransactionProvider)
            : base(activeTransactionProvider)
        {
        }

        public async Task<RatePlan> GetByDistributionCodeAsync(string distributionCode, int propertyId)
         => (await QueryAsync<RatePlan>(@"
                SELECT      RP.RatePlanId as Id                          
                FROM        RatePlan RP 
                WHERE       RP.PropertyId = @propertyId AND RP.DistributionCode = @distributionCode AND RP.IsActive = 1 AND RP.IsDeleted = 0",
             new { propertyId, distributionCode }))
             .FirstOrDefault();
    }
}
