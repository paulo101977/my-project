﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Thex.HigsIntegration.Dto.Dto;
using Thex.HigsIntegration.Infra.Context;
using Thex.HigsIntegration.Infra.Entities;
using Thex.HigsIntegration.Infra.Interfaces.ReadRepositories;
using Thex.Kernel;
using Tnf.Dapper.Repositories;
using Tnf.Repositories;

namespace Thex.HigsIntegration.Infra.Repositories.ReadRepositories
{
    public class CustomerStationReadRepository : DapperEfRepositoryBase<ThexHigsIntegrationContext, CustomerStation>, ICustomerStationReadRepository
    {
        private readonly IApplicationUser _applicationUser;

        public CustomerStationReadRepository(IActiveTransactionProvider activeTransactionProvider,
                                             IApplicationUser applicationUser)
            : base(activeTransactionProvider)
        {
            _applicationUser = applicationUser;
        }

        public async Task<IList<CustomerStation>> GetAllCustomerStation()
        {
            var query = await QueryAsync<CustomerStation>(@"
                SELECT CustomerStation.CustomerStationId as Id,
                       CustomerStation.*
                FROM   CustomerStation
                WHERE  IsDeleted = 0");

            return query.ToList();
        }

        public async Task<IList<CustomerStationDto>> GetAllCustomerStationDto()
        {
            var query = await QueryAsync<CustomerStationDto>(@"
                SELECT CustomerStation.CustomerStationId as Id,
                       CustomerStation.*,                    
                       PropertyCompanyClientCategory.PropertyCompanyClientCategoryName as CustomerStationCategoryName
                FROM   CustomerStation
                LEFT JOIN PropertyCompanyClientCategory
                ON CustomerStation.PropertyCompanyClientCategoryId = PropertyCompanyClientCategory.PropertyCompanyClientCategoryId
                WHERE CustomerStation.IsDeleted = 0");

            return query.ToList();
        }

        public async Task<CustomerStation> GetCustomerStationById(Guid id)
        {
            var query = await QueryAsync<CustomerStation>(@"
                 SELECT CustomerStation.CustomerStationId as Id,
                       CustomerStation.*
                FROM   CustomerStation
                WHERE  CustomerStation.CustomerStationId = @id and IsDeleted = 0",
                new { Id = id});


            var customerStation = query.FirstOrDefault();
            return customerStation;
        }

        public async Task<CustomerStationDto> GetCustomerStationDtoById(Guid id)
        {
            var query = await QueryAsync<CustomerStationDto>(@"
                 SELECT CustomerStation.CustomerStationId as Id,
                       CustomerStation.*,                    
                       PropertyCompanyClientCategory.PropertyCompanyClientCategoryName as CustomerStationCategoryName
                FROM   CustomerStation
                left JOIN PropertyCompanyClientCategory
                ON CustomerStation.PropertyCompanyClientCategoryId = PropertyCompanyClientCategory.PropertyCompanyClientCategoryId
                WHERE  CustomerStation.CustomerStationId = @id and CustomerStation.IsDeleted = 0",
                new { Id = id });


            var customerStation = query.FirstOrDefault();
            return customerStation;
        }

        public async Task<IList<CustomerStationDto>> GetAllDtoByCompanyClient(Guid id)
        {
            var query = await QueryAsync<CustomerStationDto>(@"
                SELECT CustomerStation.CustomerStationId as Id,
                       CustomerStation.*,                    
                       PropertyCompanyClientCategory.PropertyCompanyClientCategoryName as CustomerStationCategoryName
                FROM   CustomerStation
                LEFT JOIN PropertyCompanyClientCategory
                ON CustomerStation.PropertyCompanyClientCategoryId = PropertyCompanyClientCategory.PropertyCompanyClientCategoryId
                WHERE CustomerStation.IsDeleted = 0 AND CustomerStation.CompanyClientId = @Id",
                new { Id = id});

            return query.ToList();
        }
    }
}
