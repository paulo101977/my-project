﻿using System.Linq;
using System.Threading.Tasks;
using Thex.HigsIntegration.Infra.Context;
using Thex.HigsIntegration.Infra.Entities.Thex;
using Thex.HigsIntegration.Infra.Interfaces.ReadRepositories;
using Tnf.Dapper.Repositories;
using Tnf.Repositories;

namespace Thex.HigsIntegration.Infra.Repositories.ReadRepositories
{
    public class ReservationItemReadRepository : DapperEfRepositoryBase<ThexHigsIntegrationContext, ReservationItem>, IReservationItemReadRepository
    {
        public ReservationItemReadRepository(IActiveTransactionProvider activeTransactionProvider)
            : base(activeTransactionProvider)
        {
        }

        public async Task<ReservationItem> GetByExternalReservationNumberAsync(string externalReservationNumber)
         => (await QueryAsync<ReservationItem>(@"
                SELECT      RI.ReservationItemId as Id                          
                FROM        ReservationItem RI 
                WHERE       RI.ExternalReservationNumber = @externalReservationNumber",
             new { externalReservationNumber }))
             .FirstOrDefault();
    }
}
