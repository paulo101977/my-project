﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Thex.HigsIntegration.Infra.Context;
using Thex.HigsIntegration.Infra.Entities.Thex;
using Thex.HigsIntegration.Infra.Interfaces.ReadRepositories;
using Tnf.Dapper.Repositories;
using Tnf.Repositories;

namespace Thex.HigsIntegration.Infra.Repositories.ReadRepositories
{
    public class ChannelReadRepository : DapperEfRepositoryBase<ThexHigsIntegrationContext, Channel>, IChannelReadRepository
    {
        public ChannelReadRepository(IActiveTransactionProvider activeTransactionProvider)
            : base(activeTransactionProvider)
        {
        }

        public async Task<Channel> GetByIntegrationCodeAsync(string channelCode, Guid tenantId)
            => (await QueryAsync<Channel>(@"
                SELECT      C.ChannelId as Id                          
                FROM        Channel C 
                INNER JOIN  ChannelCode CC
                ON          C.ChannelCodeId  = CC.ChannelCodeId
                WHERE       C.TenantId = @tenantId AND CC.ChannelCode = @channelCode AND C.IsActive = 1 AND C.IsDeleted = 0",
                new { channelCode, tenantId }))
                .FirstOrDefault();

    }
}
