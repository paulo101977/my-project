﻿using System.Linq;
using System.Threading.Tasks;
using Thex.HigsIntegration.Infra.Context;
using Thex.HigsIntegration.Infra.Entities.Thex;
using Thex.HigsIntegration.Infra.Interfaces.ReadRepositories;
using Tnf.Dapper.Repositories;
using Tnf.Repositories;

namespace Thex.HigsIntegration.Infra.Repositories.ReadRepositories
{
    public class CurrencyReadRepository : DapperEfRepositoryBase<ThexHigsIntegrationContext, Currency>, ICurrencyReadRepository
    {
        public CurrencyReadRepository(IActiveTransactionProvider activeTransactionProvider)
            : base(activeTransactionProvider)
        {
        }

        public async Task<Currency> GetByCurrencyCodeAsync(string currencyCode)
           => (await QueryAsync<Currency>(@"
                SELECT      C.CurrencyId as Id                          
                FROM        Currency C 
                WHERE       C.AlphabeticCode = @currencyCode",
               new { currencyCode }))
               .FirstOrDefault();
    }
}
