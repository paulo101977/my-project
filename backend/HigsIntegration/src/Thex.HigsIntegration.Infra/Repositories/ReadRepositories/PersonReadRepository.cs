﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Thex.HigsIntegration.Infra.Context;
using Thex.HigsIntegration.Infra.Entities;
using Thex.HigsIntegration.Infra.Interfaces.ReadRepositories;
using Tnf.Dapper.Repositories;
using Tnf.Repositories;

namespace Thex.HigsIntegration.Infra.Repositories.ReadRepositories
{
    public class PersonReadRepository : DapperEfRepositoryBase<ThexHigsIntegrationContext, Person>, IPersonReadRepository
    {
        public PersonReadRepository(IActiveTransactionProvider activeTransactionProvider)
         : base(activeTransactionProvider)
        {

        }

        public async Task<Person> GetById(Guid id)
        {
            var query = await QueryAsync<Person>(@"
                 SELECT PersonId as Id,
                       Person.*
                FROM   Person
                WHERE  PersonId = @id",
               new { Id = id });


            return query.FirstOrDefault();
        }
    }
}
