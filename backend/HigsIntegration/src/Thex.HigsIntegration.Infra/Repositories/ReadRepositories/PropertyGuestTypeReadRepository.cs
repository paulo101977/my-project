﻿using System.Linq;
using System.Threading.Tasks;
using Thex.HigsIntegration.Infra.Context;
using Thex.HigsIntegration.Infra.Entities.Thex;
using Thex.HigsIntegration.Infra.Interfaces.ReadRepositories;
using Tnf.Dapper.Repositories;
using Tnf.Repositories;

namespace Thex.HigsIntegration.Infra.Repositories.ReadRepositories
{
    public class PropertyGuestTypeReadRepository : DapperEfRepositoryBase<ThexHigsIntegrationContext, PropertyGuestType>, IPropertyGuestTypeReadRepository
    {
        public PropertyGuestTypeReadRepository(IActiveTransactionProvider activeTransactionProvider)
            : base(activeTransactionProvider)
        {
        }

        public async Task<PropertyGuestType> GetDefaultByPropertyIdAsync(int propertyId)
           => (await QueryAsync<PropertyGuestType>(@"
                SELECT      PGT.PropertyGuestTypeId as Id                          
                FROM        PropertyGuestType PGT 
                WHERE       PGT.PropertyId = @propertyId AND PGT.IsVIP = 0",
               new { propertyId }))
               .FirstOrDefault();
    }
}
