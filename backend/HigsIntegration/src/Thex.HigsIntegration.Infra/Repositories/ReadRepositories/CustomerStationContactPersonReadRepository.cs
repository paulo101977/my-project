﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Thex.HigsIntegration.Infra.Context;
using Thex.HigsIntegration.Infra.Entities;
using Thex.HigsIntegration.Infra.Interfaces.ReadRepositories;
using Tnf.Dapper.Repositories;
using Tnf.Repositories;

namespace Thex.HigsIntegration.Infra.Repositories.ReadRepositories
{
    public class CustomerStationContactPersonReadRepository : DapperEfRepositoryBase<ThexHigsIntegrationContext, CustomerStationContactPerson>, ICustomerStationContactPersonReadRepository
    {
        public CustomerStationContactPersonReadRepository(IActiveTransactionProvider activeTransactionProvider)
           : base(activeTransactionProvider)
        {
        }

        public async Task<ICollection<CustomerStationContactPerson>> GetAllByCustomerStationId(Guid id)
        {
            var query = await QueryAsync<CustomerStationContactPerson>(@"
                SELECT CustomerStationContactPersonId as Id,
                       CustomerStationContactPerson.*
                FROM   CustomerStationContactPerson
                WHERE  CustomerStationId = @Id",
                new { Id = id});
            
            return query.ToList();
        }

        public async Task<CustomerStationContactPerson> GetById(Guid id)
        {
            var query = await QueryAsync<CustomerStationContactPerson>(@"
                SELECT CustomerStationContactPersonId as Id,
                       CustomerStationContactPerson.*
                FROM   CustomerStationContactPerson
                WHERE  CustomerStationContactPersonId = @Id",
                new { Id = id });

            return query.FirstOrDefault();
        }

        public async Task<ICollection<CustomerStationContactPerson>> GetAllForExclude(ICollection<CustomerStationContactPerson> customerStationContactPerson, Guid customerStationId)
        {
            var query = await QueryAsync<CustomerStationContactPerson>(@"
                SELECT CustomerStationContactPersonId as Id,
                       PersonId
                FROM   CustomerStationContactPerson
                WHERE  CustomerStationId = @Id AND CustomerStationContactPersonId NOT IN(@List)",
                new { Id = customerStationId, List = customerStationContactPerson.Select(x => x.Id) }
            );

            return query.ToList();
        }
    }
}
