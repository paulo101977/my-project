﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.HigsIntegration.Infra.Context;
using Thex.HigsIntegration.Infra.Entities;
using Thex.HigsIntegration.Infra.Interfaces;
using Tnf.Dapper.Repositories;
using Tnf.Repositories;

namespace Thex.HigsIntegration.Infra.Repositories
{
    public class ContactInformationRepository : DapperEfRepositoryBase<ThexHigsIntegrationContext, ContactInformation>, IContactInformationRepository
    {
        public ContactInformationRepository(IActiveTransactionProvider activeTransactionProvider)
         : base(activeTransactionProvider)
        {

        }

        public async Task AddAsync(ContactInformation contactInformation)
        {
            await InsertAsync(contactInformation);
        }

        public async Task AddRangeAsync(ICollection<ContactInformation> contactInformationList)
        {
            await InsertAsync(contactInformationList);
        }

        public async Task RemoveRangeAsync(ICollection<ContactInformation> contactInformationList)
        {
            foreach (var contactInformation in contactInformationList)
            {
                await DeleteAsync(contactInformation);
            }
        }

        public async Task RemoveAsync(ContactInformation contactInformation)
        {
            await DeleteAsync(contactInformation);
        }
    }
}
