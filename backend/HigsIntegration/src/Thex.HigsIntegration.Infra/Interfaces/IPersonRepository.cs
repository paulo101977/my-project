﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.HigsIntegration.Infra.Entities;

namespace Thex.HigsIntegration.Infra.Interfaces
{
    public interface IPersonRepository
    {
        Task Add(Person person);
        Task Remove(Person person);
        Task AddRange(ICollection<Person> persons);
        Task Update(Person person);
    }
}
