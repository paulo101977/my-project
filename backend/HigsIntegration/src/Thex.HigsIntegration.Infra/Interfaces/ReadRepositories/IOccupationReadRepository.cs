﻿using System.Threading.Tasks;
using Thex.HigsIntegration.Infra.Entities;

namespace Thex.HigsIntegration.Infra.Interfaces.ReadRepositories
{
    public interface IOccupationReadRepository
    {
        Task<Occupation> GetById(int id);
    }
}
