﻿
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.HigsIntegration.Dto.Dto;
using Thex.HigsIntegration.Infra.Entities;

namespace Thex.HigsIntegration.Infra.Interfaces.ReadRepositories
{
    public interface ICustomerStationReadRepository
    {
        Task<CustomerStation> GetCustomerStationById(Guid id);
        Task<CustomerStationDto> GetCustomerStationDtoById(Guid id);
        Task<IList<CustomerStation>> GetAllCustomerStation();
        Task<IList<CustomerStationDto>> GetAllCustomerStationDto();
        Task<IList<CustomerStationDto>> GetAllDtoByCompanyClient(Guid id);
    }
}
