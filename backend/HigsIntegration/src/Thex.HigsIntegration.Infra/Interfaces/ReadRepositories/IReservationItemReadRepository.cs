﻿using System.Threading.Tasks;
using Thex.HigsIntegration.Infra.Entities.Thex;

namespace Thex.HigsIntegration.Infra.Interfaces.ReadRepositories
{
    public interface IReservationItemReadRepository
    {
        Task<ReservationItem> GetByExternalReservationNumberAsync(string externalReservationNumber);
    }
}
