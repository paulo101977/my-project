﻿using System.Threading.Tasks;

namespace Thex.HigsIntegration.Infra.Interfaces.ReadRepositories
{
    public interface IPropertyParameterReadRepository
    {
        Task<string> GetTimeZoneByPropertyIdAsync(int propertyId);
    }
}
