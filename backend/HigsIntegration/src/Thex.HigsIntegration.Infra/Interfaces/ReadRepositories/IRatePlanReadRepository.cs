﻿using System.Threading.Tasks;
using Thex.HigsIntegration.Infra.Entities.Thex;

namespace Thex.HigsIntegration.Infra.Interfaces.ReadRepositories
{
    public interface IRatePlanReadRepository
    {
        Task<RatePlan> GetByDistributionCodeAsync(string distributionCode, int propertyId);
    }
}
