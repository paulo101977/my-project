﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.HigsIntegration.Infra.Entities;

namespace Thex.HigsIntegration.Infra.Interfaces.ReadRepositories
{
    public interface IContactInformationReadRepository
    {
        Task<ICollection<ContactInformation>> GetContactInformationsByPerson(Guid id);
        Task<ContactInformation> GetContactInformationByTypeAndPerson(Guid id, int contactInformationType);
    }
}
