﻿using System.Threading.Tasks;
using Thex.HigsIntegration.Infra.Entities.Thex;

namespace Thex.HigsIntegration.Infra.Interfaces.ReadRepositories
{
    public interface ICurrencyReadRepository
    {
        Task<Currency> GetByCurrencyCodeAsync(string currencyCode);
    }
}
