﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Thex.HigsIntegration.Infra.Entities.Thex;

namespace Thex.HigsIntegration.Infra.Interfaces.ReadRepositories
{
    public interface IPropertyGuestTypeReadRepository
    {
        Task<PropertyGuestType> GetDefaultByPropertyIdAsync(int propertyId);
    }
}
