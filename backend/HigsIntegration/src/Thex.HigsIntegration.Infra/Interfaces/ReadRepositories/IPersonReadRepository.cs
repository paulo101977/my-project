﻿using System;
using System.Threading.Tasks;
using Thex.HigsIntegration.Infra.Entities;

namespace Thex.HigsIntegration.Infra.Interfaces.ReadRepositories
{
    public interface IPersonReadRepository
    {
        Task<Person> GetById(Guid id);
    }
}
