﻿using System.Threading.Tasks;
using Thex.HigsIntegration.Infra.Entities.Thex;

namespace Thex.HigsIntegration.Infra.Interfaces.ReadRepositories
{
    public interface IRoomTypeReadRepository
    {
        Task<RoomType> GetByDistributionCodeAsync(string distributionCode, int propertyId);
    }
}
