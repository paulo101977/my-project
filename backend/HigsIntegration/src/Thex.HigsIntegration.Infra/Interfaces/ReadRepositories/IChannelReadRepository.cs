﻿using System;
using System.Threading.Tasks;
using Thex.HigsIntegration.Infra.Entities.Thex;

namespace Thex.HigsIntegration.Infra.Interfaces.ReadRepositories
{
    public interface IChannelReadRepository
    {
        Task<Channel> GetByIntegrationCodeAsync(string channelCode, Guid tenantId);
    }
}
