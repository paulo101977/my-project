﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.HigsIntegration.Infra.Entities;

namespace Thex.HigsIntegration.Infra.Interfaces.ReadRepositories
{
    public interface ICustomerStationContactPersonReadRepository
    {
        Task<ICollection<CustomerStationContactPerson>> GetAllByCustomerStationId(Guid id);
        Task<CustomerStationContactPerson> GetById(Guid id);
        Task<ICollection<CustomerStationContactPerson>> GetAllForExclude(ICollection<CustomerStationContactPerson> customerStationContactPerson, Guid customerStationId);
    }
}
