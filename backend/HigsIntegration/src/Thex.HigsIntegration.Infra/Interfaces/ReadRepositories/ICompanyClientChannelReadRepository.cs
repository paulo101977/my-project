﻿using System;
using System.Threading.Tasks;
using Thex.HigsIntegration.Infra.Entities.Thex;

namespace Thex.HigsIntegration.Infra.Interfaces.ReadRepositories
{
    public interface ICompanyClientChannelReadRepository
    {
        Task<CompanyClientChannel> GetByCompanyIdAsync(string companyId, Guid channelId, Guid tenantId);
    }
}
