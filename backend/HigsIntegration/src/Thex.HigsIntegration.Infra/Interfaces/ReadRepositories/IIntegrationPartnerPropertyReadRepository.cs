﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.Common.Enumerations;
using Thex.HigsIntegration.Dto.Dto;

namespace Thex.HigsIntegration.Infra.Interfaces.ReadRepositories
{
    public interface IIntegrationPartnerPropertyReadRepository
    {
        Task<ICollection<IntegrationPartnerPropertyDto>> GetAllByPartner(PartnerEnum partnerEnum);
        Task<IntegrationPartnerPropertyDto> GetByIntegrationCode(string integrationCode);
        Task<bool> AnyByIntegrationCode(string integrationCode);
    }
}
