﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Thex.HigsIntegration.Infra.Entities;

namespace Thex.HigsIntegration.Infra.Interfaces
{
    public interface ICustomerStationContactPersonRepository
    {
        Task InsertAsync(CustomerStationContactPerson customerStationContactPerson);
        Task InsertRangeAsync(ICollection<CustomerStationContactPerson> customerStationContactPerson);
        Task RemoveAsync(CustomerStationContactPerson customerStationContactPerson);
        Task RemoveRangeAsync(ICollection<CustomerStationContactPerson> customerStationContactPerson);
    }
}
