﻿using System;
using System.Threading.Tasks;
using Thex.HigsIntegration.Infra.Entities;
using Tnf.Dto;

namespace Thex.HigsIntegration.Infra.Interfaces
{
    public interface IBaseRepository
    {
        Task<IDto> InsertAsync(BaseEntity baseEntity);
        Task<IDto> UpdateAsync(Guid id, BaseEntity baseEntity);
        Task RemoveAsync(Guid id);
        Task ToggleAsync(Guid id);
    }
}
