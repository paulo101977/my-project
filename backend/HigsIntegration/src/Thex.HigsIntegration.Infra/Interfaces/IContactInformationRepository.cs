﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Thex.HigsIntegration.Infra.Entities;

namespace Thex.HigsIntegration.Infra.Interfaces
{
    public interface IContactInformationRepository
    {
        Task AddAsync(ContactInformation contactInformation);
        Task AddRangeAsync(ICollection<ContactInformation> contactInformationList);
        Task RemoveRangeAsync(ICollection<ContactInformation> contactInformationList);
        Task RemoveAsync(ContactInformation contactInformation);
    }
}
