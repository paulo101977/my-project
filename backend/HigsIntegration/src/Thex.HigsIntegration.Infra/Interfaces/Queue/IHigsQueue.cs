﻿using Microsoft.WindowsAzure.Storage.Queue;
using System.Threading.Tasks;

namespace Thex.HigsIntegration.Infra.Interfaces.Queue
{
    public interface IHigsQueue
    {
        Task<CloudQueue> GetQueue();
        Task Insert(CloudQueue queue, string json);
    }
}
