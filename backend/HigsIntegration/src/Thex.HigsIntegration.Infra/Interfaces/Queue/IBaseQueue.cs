﻿using Microsoft.WindowsAzure.Storage.Queue;
using System.Threading.Tasks;

namespace Thex.HigsIntegration.Infra.Interfaces.Queue
{
    public interface IBaseQueue
    {
        CloudQueueClient GetCloudQueueClient();
        CloudQueue GetQueue(CloudQueueClient queueClient, string queueName);
        Task Insert(CloudQueue queue, string json);
        Task CreateQueueIfNotExists(CloudQueue queue);
        Task<CloudQueue> GetAndCreateQueueIfNotExists(string queueName);
    }
}
