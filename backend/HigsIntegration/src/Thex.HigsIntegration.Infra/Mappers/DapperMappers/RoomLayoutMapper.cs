﻿using DapperExtensions.Mapper;
using Thex.HigsIntegration.Infra.Entities.Thex;

namespace Thex.HigsIntegration.Infra.Mappers.DapperMappers
{
    public class RoomLayoutMapper : ClassMapper<RoomLayout>
    {
        public RoomLayoutMapper()
        {
            Table("RoomLayout");
            Map(x => x.Id).Column("RoomLayoutId").Key(KeyType.Guid);
            AutoMap();
        }
    }
}
