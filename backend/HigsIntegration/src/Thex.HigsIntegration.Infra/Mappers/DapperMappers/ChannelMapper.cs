﻿using DapperExtensions.Mapper;
using System;
using System.Collections.Generic;
using System.Text;
using Thex.HigsIntegration.Infra.Entities.Thex;

namespace Thex.HigsIntegration.Infra.Mappers.DapperMappers
{
    public class ChannelMapper : ClassMapper<Channel>
    {
        public ChannelMapper()
        {
            Table("Channel");
            Map(x => x.Id).Column("ChannelId").Key(KeyType.Guid);
            AutoMap();
        }
    }
}
