﻿using DapperExtensions.Mapper;
using Thex.HigsIntegration.Infra.Entities.Thex;

namespace Thex.HigsIntegration.Infra.Mappers.DapperMappers
{
    public class ReservationItemMapper : ClassMapper<ReservationItem>
    {
        public ReservationItemMapper()
        {
            Table("ReservationItem");
            Map(x => x.Id).Column("ReservationItemId").Key(KeyType.Identity);
            AutoMap();
        }
    }
}
