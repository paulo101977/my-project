﻿using DapperExtensions.Mapper;
using Thex.HigsIntegration.Infra.Entities;

namespace Thex.HigsIntegration.Infra.Mappers.DapperMappers
{
   public class PersonMapper : ClassMapper<Person>
    {
        public PersonMapper()
        {
            Table("Person");
            Map(x => x.Id).Column("PersonId").Key(KeyType.Guid);
            AutoMap();
        }
    }
}
