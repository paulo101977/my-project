﻿using DapperExtensions.Mapper;
using Thex.HigsIntegration.Infra.Entities;

namespace Thex.HigsIntegration.Infra.Mappers.DapperMappers
{
    public class CustomerStationContactPersonMapper : ClassMapper<CustomerStationContactPerson>
    {
        public CustomerStationContactPersonMapper()
        {
            Table("CustomerStationContactPerson");
            Map(x => x.Id).Column("CustomerStationContactPersonId").Key(KeyType.Guid);
            AutoMap();
        }
    }
}
