﻿using DapperExtensions.Mapper;
using Thex.HigsIntegration.Infra.Entities.Thex;

namespace Thex.HigsIntegration.Infra.Mappers.DapperMappers
{
    public class CurrencyMapper : ClassMapper<Currency>
    {
        public CurrencyMapper()
        {
            Table("Currency");
            Map(x => x.Id).Column("CurrencyId").Key(KeyType.Guid);
            AutoMap();
        }
    }
}
