﻿using DapperExtensions.Mapper;
using System;
using System.Collections.Generic;
using System.Text;
using Thex.HigsIntegration.Infra.Entities.Thex;

namespace Thex.HigsIntegration.Infra.Mappers.DapperMappers
{
    public class PropertyGuestTypeMapper : ClassMapper<PropertyGuestType>
    {
        public PropertyGuestTypeMapper()
        {
            Table("PropertyGuestType");
            Map(x => x.Id).Column("PropertyGuestTypeId").Key(KeyType.Identity);
            AutoMap();
        }
    }
}
