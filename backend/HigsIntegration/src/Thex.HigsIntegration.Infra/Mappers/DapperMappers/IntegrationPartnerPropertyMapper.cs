﻿using DapperExtensions.Mapper;
using Thex.HigsIntegration.Infra.Entities;

namespace Thex.HigsIntegration.Infra.Mappers.DapperMappers
{
   public class IntegrationPartnerPropertyMapper : ClassMapper<IntegrationPartnerProperty>
    {
        public IntegrationPartnerPropertyMapper()
        {
            Table("IntegrationPartnerProperty");
            Map(x => x.Id).Column("IntegrationPartnerPropertyId").Key(KeyType.Guid);
            AutoMap();
        }
    }
}
