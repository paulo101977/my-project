﻿using DapperExtensions.Mapper;
using Thex.HigsIntegration.Infra.Entities.Thex;

namespace Thex.HigsIntegration.Infra.Mappers.DapperMappers
{
    public class RoomTypeMapper : ClassMapper<RoomType>
    {
        public RoomTypeMapper()
        {
            Table("RoomType");
            Map(x => x.Id).Column("RoomTypeId").Key(KeyType.Identity);
            AutoMap();
        }
    }
}
