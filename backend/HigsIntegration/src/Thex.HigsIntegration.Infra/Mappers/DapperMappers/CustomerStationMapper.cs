﻿using DapperExtensions.Mapper;
using Thex.HigsIntegration.Infra.Entities;

namespace Thex.HigsIntegration.Infra.Mappers.DapperMappers
{
    public class CustomerStationMapper : ClassMapper<CustomerStation>
    {
        public CustomerStationMapper()
        {
            Table("CustomerStation");
            Map(x => x.Id).Column("CustomerStationId").Key(KeyType.Guid);
            Map(x => x.CustomerStationCategoryId).Column("PropertyCompanyClientCategoryId");
            AutoMap();
        }
    }
}
