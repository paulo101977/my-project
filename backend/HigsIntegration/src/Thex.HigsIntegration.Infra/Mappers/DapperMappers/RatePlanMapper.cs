﻿using DapperExtensions.Mapper;
using Thex.HigsIntegration.Infra.Entities.Thex;

namespace Thex.HigsIntegration.Infra.Mappers.DapperMappers
{
    public class RatePlanMapper : ClassMapper<RatePlan>
    {
        public RatePlanMapper()
        {
            Table("RatePlan");
            Map(x => x.Id).Column("RatePlanId").Key(KeyType.Guid);
            AutoMap();
        }
    }
}
