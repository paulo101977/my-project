﻿using DapperExtensions.Mapper;
using Thex.HigsIntegration.Infra.Entities;

namespace Thex.HigsIntegration.Infra.Mappers.DapperMappers
{
    public class ContactInformationMapper : ClassMapper<ContactInformation>
    {
        public ContactInformationMapper()
        {
            Table("ContactInformation");
            Map(x => x.OwnerId).Key(KeyType.Assigned);
            Map(x => x.Information).Key(KeyType.Assigned);
            Map(x => x.ContactInformationTypeId).Key(KeyType.Assigned);
            Map(x => x.Id).Ignore();
            AutoMap();
        }
    }
}
