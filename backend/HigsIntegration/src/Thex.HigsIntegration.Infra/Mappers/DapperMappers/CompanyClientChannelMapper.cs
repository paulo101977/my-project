﻿using DapperExtensions.Mapper;
using Thex.HigsIntegration.Infra.Entities.Thex;

namespace Thex.HigsIntegration.Infra.Mappers.DapperMappers
{
    public class CompanyClientChannelMapper : ClassMapper<CompanyClientChannel>
    {
        public CompanyClientChannelMapper()
        {
            Table("CompanyClientChannel");
            Map(x => x.Id).Column("CompanyClientChannelId").Key(KeyType.Guid);
            AutoMap();
        }
    }
}
