﻿using AutoMapper;
using Thex.HigsIntegration.Dto.Dto;
using Thex.HigsIntegration.Infra.Entities;

namespace Thex.HigsIntegration.Infra.Mappers.Profiles
{
    class ContactInformationProfile : Profile
    {
        public ContactInformationProfile()
        {
            CreateMap<ContactInformation, ContactInformationDto>();
        }
    }
}
