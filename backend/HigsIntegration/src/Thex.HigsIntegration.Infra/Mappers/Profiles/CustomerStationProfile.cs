﻿using AutoMapper;
using Thex.HigsIntegration.Dto.Dto;
using Thex.HigsIntegration.Infra.Entities;

namespace Thex.HigsIntegration.Infra.Mappers.Profiles
{
    public class CustomerStationProfile : Profile
    {
        public CustomerStationProfile()
        {
            CreateMap<CustomerStation, CustomerStationDto>();
        }
    }
}
