﻿using Thex.Common;
using Thex.HigsIntegration.Infra.Context;
using Thex.HigsIntegration.Infra.Interfaces;
using Thex.HigsIntegration.Infra.Interfaces.Queue;
using Thex.HigsIntegration.Infra.Interfaces.ReadRepositories;
using Thex.HigsIntegration.Infra.Mappers.DapperMappers;
using Thex.HigsIntegration.Infra.Mappers.Profiles;
using Thex.HigsIntegration.Infra.Queue;
using Thex.HigsIntegration.Infra.Repositories;
using Thex.HigsIntegration.Infra.Repositories.ReadRepositories;
using Tnf.Dapper;

namespace Microsoft.Extensions.DependencyInjection
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddInfraDependency(this IServiceCollection services)
        {
            // Configura o uso do Dapper registrando os contextos que serão
            // usados pela aplicação
            services
                .AddTnfEntityFrameworkCore()
                .AddTnfDbContext<ThexHigsIntegrationContext>(config => DbContextConfigurer.Configure(config))
                .AddTnfDapper(options =>
                {
                    options.MapperAssemblies.Add(typeof(CustomerStationContactPersonMapper).Assembly);
                    options.MapperAssemblies.Add(typeof(CustomerStationMapper).Assembly);
                    options.MapperAssemblies.Add(typeof(PersonMapper).Assembly);
                    options.MapperAssemblies.Add(typeof(ContactInformationMapper).Assembly);
                    options.MapperAssemblies.Add(typeof(RatePlanMapper).Assembly);
                    options.MapperAssemblies.Add(typeof(CurrencyMapper).Assembly);
                    options.MapperAssemblies.Add(typeof(ChannelMapper).Assembly);
                    options.MapperAssemblies.Add(typeof(RoomTypeMapper).Assembly);
                    options.MapperAssemblies.Add(typeof(RoomLayoutMapper).Assembly);
                    options.MapperAssemblies.Add(typeof(PropertyGuestTypeMapper).Assembly);
                    options.MapperAssemblies.Add(typeof(CompanyClientChannelMapper).Assembly);
                    options.MapperAssemblies.Add(typeof(ReservationItemMapper).Assembly);
                    options.DbType = DapperDbType.SqlServer;
                });

            services.AddTnfAutoMapper(config =>
            {
                config.AddProfile<CustomerStationProfile>();
                config.AddProfile<CustomerStationContactPersonProfile>();
                config.AddProfile<PersonProfile>();
                config.AddProfile<ContactInformationProfile>();
            });

            services.AddTransient<IChannelReadRepository, ChannelReadRepository>();
            services.AddTransient<ICompanyClientChannelReadRepository, CompanyClientChannelReadRepository>();
            services.AddTransient<ICurrencyReadRepository, CurrencyReadRepository>();
            services.AddTransient<IPropertyGuestTypeReadRepository, PropertyGuestTypeReadRepository>();
            services.AddTransient<IPropertyParameterReadRepository, PropertyParameterReadRepository>();
            services.AddTransient<IRatePlanReadRepository, RatePlanReadRepository>();
            services.AddTransient<IRoomTypeReadRepository, RoomTypeReadRepository>();
            services.AddTransient<IRoomLayoutReadRepository, RoomLayoutReadRepository>();
            services.AddTransient<IReservationItemReadRepository, ReservationItemReadRepository>();

            services.AddTransient<ICustomerStationReadRepository, CustomerStationReadRepository>();
            services.AddTransient<IContactInformationReadRepository, ContactInformationReadRepository>();
            services.AddTransient<ICustomerStationContactPersonReadRepository, CustomerStationContactPersonReadRepository>();
            services.AddTransient<IPersonReadRepository, PersonReadRepository>();
            services.AddTransient<IOccupationReadRepository, OccupationReadRepository>();
            services.AddTransient<IIntegrationPartnerPropertyReadRepository, IntegrationPartnerPropertyReadRepository>();

            services.AddTransient<ICustomerStationRepository, CustomerStationRepository>();
            services.AddTransient<IContactInformationRepository, ContactInformationRepository>();
            services.AddTransient<ICustomerStationContactPersonRepository, CustomerStationContactPersonRepository>();
            services.AddTransient<IPersonRepository, PersonRepository>();

            services.AddTransient<IReservationDownloadQueue, ReservationDownloadQueue>();
            services.AddTransient<IRatePlanListQueue, RatePlanListQueue>();
            services.AddTransient<IGuestRelationListQueue, GuestRelationListQueue>();
            services.AddTransient<IGuestInformationListQueue, GuestInformationListQueue>();

            return services;
        }
    }
}
