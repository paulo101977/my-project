﻿using System;

namespace THEx.Integration.NFeIO.Commom
{
    public class ApisNfeIo
    {
        private static string UrlServer()
        {
            return Environment.GetEnvironmentVariable("apiNFeIO", EnvironmentVariableTarget.Process);
        }

        public static string UrlGetPropertyForDocument (string document)
        {
            string additionalUrl = String.Format(ApiNfeioValue.GetCompany, document);

            return UrlServer() + additionalUrl;
        }

        public static string UrlPostServiceInvoice(string companyId)
        {
            string additionalUrl = String.Format(ApiNfeioValue.SendInvoice, companyId);

            return UrlServer() + additionalUrl;
        }

        public static string UrlGetNfsePdf(string companyId, string invoiceId)
        {
            string additionalUrl = String.Format(ApiNfeioValue.GetPdf, companyId, invoiceId);

            return UrlServer() + additionalUrl;
        }

        public static string UrlGetNfseXml(string companyId, string invoiceId)
        {
            string additionalUrl = String.Format(ApiNfeioValue.GetXml, companyId, invoiceId);

            return UrlServer() + additionalUrl;
        }

        public static string UrlDeleteInvoice(string companyId, string invoiceId)
        {
            string additionalUrl = String.Format(ApiNfeioValue.CancelInvoice, companyId, invoiceId);

            return UrlServer() + additionalUrl;
        }
    }
}
