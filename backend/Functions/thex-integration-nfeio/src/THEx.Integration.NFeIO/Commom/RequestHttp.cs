﻿using System;
using System.Net;
using System.Threading.Tasks;
using Microsoft.Azure.WebJobs.Host;
using Newtonsoft.Json;
using RestSharp;
using THEx.Integration.NFeIO.DTO;
using THEx.Integration.NFeIO.DTO.PMS;

namespace THEx.Integration.NFeIO.Commom
{
    public class RequestHttp
    {
        ApiResultDto apiResultDto = new ApiResultDto();

        public ApiResultDto SendPost(string url, string messageBody)
        {
            var client = new RestClient(url);
            var request = new RestRequest(Method.POST);

            request.AddHeader("Authorization", Appkeys.NotaFiscal);
            request.RequestFormat = DataFormat.Json;
            request.AddParameter("application/json; charset=utf-8", messageBody, ParameterType.RequestBody);
            
            IRestResponse response = client.Execute(request);
            HttpStatusCode statusCode = response.StatusCode;

            apiResultDto.HttpStatusCode = (int)statusCode;
            apiResultDto.Message = response.Content;
            apiResultDto.Method = "POST";
            apiResultDto.Url = url;
            apiResultDto.Body = messageBody;
            
            return apiResultDto;
            
        }

        public ApiResultDto SendGet(string url)
        {
            var client = new RestClient(url);
            var request = new RestRequest(Method.GET);

            request.AddHeader("Authorization", Appkeys.NotaFiscal);
            request.RequestFormat = DataFormat.Json;
            
            IRestResponse response = client.Execute(request);
            HttpStatusCode statusCode = response.StatusCode;

            apiResultDto.HttpStatusCode = (int)statusCode;
            apiResultDto.Message = response.Content;
            apiResultDto.Method = "GET";
            apiResultDto.Url = url;
            
            return apiResultDto;
        }

        public ApiResultDto SendDelete(string url)
        {
            var client = new RestClient(url);
            var request = new RestRequest(Method.DELETE);

            request.AddHeader("Authorization", Appkeys.NotaFiscal);
            request.RequestFormat = DataFormat.Json;

            IRestResponse response = client.Execute(request);
            HttpStatusCode statusCode = response.StatusCode;

            apiResultDto.HttpStatusCode = (int)statusCode;
            apiResultDto.Message = response.Content;
            apiResultDto.Method = "DELETE";
            apiResultDto.Url = url;
            
            return apiResultDto;
        }

        public ApiResultDto SendPut(string url, string messageBody, TraceWriter log, bool isThex = false, string tenantId = "")
        {
            var client = new RestClient(url);
            var request = new RestRequest(Method.PUT);

            if (isThex)
            {
                //log.Info("SendPut == Antes de pegar o token == tenantId == " + tenantId);
                string token = GetThexToken(tenantId, log);
                //log.Info("SendPut == token == " + token);
                request.AddHeader("Authorization", token);
            }
            else
            {
                request.AddHeader("Authorization", Appkeys.NotaFiscal);
            }

            request.RequestFormat = DataFormat.Json;
            request.AddParameter("application/json; charset=utf-8", messageBody, ParameterType.RequestBody);

            //log.Info("SendPut == antes do client.Execute");

            IRestResponse response = client.Execute(request);

            //log.Info("SendPut == depoois do client.Execute == statusCode == " + response.StatusCode);

            HttpStatusCode statusCode = response.StatusCode;

            apiResultDto.HttpStatusCode = (int)statusCode;
            apiResultDto.Message = response.Content;
            apiResultDto.Method = "PUT";
            apiResultDto.Url = url;
            apiResultDto.Body = messageBody;

            return apiResultDto;
        }

        private string GetThexToken(string tenantId, TraceWriter log)
        {
            var client = new RestClient(ApisTHEx.UrlGetThexToken());

            var request = new RestRequest(Method.POST);

            //log.Info("GetThexToken == TokenApplication == " + Environment.GetEnvironmentVariable("TokenApplication", EnvironmentVariableTarget.Process));
            //log.Info("GetThexToken == TokenClient == " + Environment.GetEnvironmentVariable("TokenClient", EnvironmentVariableTarget.Process));

            var tokenInfo = new TokenInfoDto
            {
                TenantId = tenantId,
                TokenApplication = Environment.GetEnvironmentVariable("tokenApplicationThex", EnvironmentVariableTarget.Process),
                TokenClient = Environment.GetEnvironmentVariable("tokenClientThex", EnvironmentVariableTarget.Process),
            };

            var body = JsonConvert.SerializeObject(tokenInfo);

            //log.Info("GetThexToken == body == " + body);

            request.AddParameter("application/json; charset=utf-8", body, ParameterType.RequestBody);

            IRestResponse response = client.Execute(request);

            //log.Info("GetThexToken == client.Execute()");
            //log.Info("GetThexToken == response.IsSuccessful == " + response.IsSuccessful);

            if (response.IsSuccessful)
            {
                UserTokenDto userTokenDto = JsonConvert.DeserializeObject<UserTokenDto>(response.Content);
                var token = "Bearer " + userTokenDto.NewPasswordToken;
                return token;
            }
            else
                return string.Empty;
        }
    }
}
