﻿using Newtonsoft.Json;
using THEx.Integration.NFeIO.DTO.PMS;

namespace THEx.Integration.NFeIO.Commom
{
    public class Utils
    {

        public const string StorageName = "thexfunctionsdev";

        public enum StatusInvoice { New, Processing, Processed, Erro };


        public const string FunctionAppService = "function-app";
        public const string ExternalService = "external";
        public const string Issued = "Issued";
        public const string IssuedFailed = "IssueFailed";
        public const string Cancelled = "Cancelled";

    }
}
