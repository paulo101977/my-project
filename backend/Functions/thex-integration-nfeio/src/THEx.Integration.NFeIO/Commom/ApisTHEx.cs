﻿using System;

namespace THEx.Integration.NFeIO.Commom
{
    public class ApisTHEx
    {
        private static string UrlServer()
        {
            return Environment.GetEnvironmentVariable("apiTHEx", EnvironmentVariableTarget.Process);
        }

        private static string UrlServerSuperAdmin()
        {
            return Environment.GetEnvironmentVariable("apiTHExSuperAdmin", EnvironmentVariableTarget.Process);
        }

        public static string UrlGetThexToken()
        {
            return UrlServerSuperAdmin() + ApiTHExValue.ThexToken;
        }

        public static string UrlPutInvoiceSuccess (string propertyId)
        {
            string additionalUrl = String.Format(ApiTHExValue.InvoiceSuccess, propertyId);

            return UrlServer() + additionalUrl;
        }

        public static string UrlPutInvoiceCancel(string propertyId)
        {
            string additionalUrl = String.Format(ApiTHExValue.InvoiceCancel, propertyId);

            return UrlServer() + additionalUrl;
        }

        public static string UrlPutInvoiceError(string propertyId)
        {
            string additionalUrl = String.Format(ApiTHExValue.InvoiceError, propertyId);

            return UrlServer() + additionalUrl;
        }
    }
}
