using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Host;
using Newtonsoft.Json;
using THEx.Integration.NFeIO.Commom;
using THEx.Integration.NFeIO.DTO;
using THEx.Integration.NFeIO.Storage.Queue;

namespace THEx.Integration.NFeIO.Functions.WebHookTrigger
{
    public static class ResponseWh
    {
        [FunctionName("WebHookTriggerResponse")]
        public static async Task<object> Run(
            [HttpTrigger(WebHookType = "genericJson", Route =  "responseInvoice")]
        HttpRequestMessage req, TraceWriter log)
        {
            try
            {
                log.Info("Entrou em ResponseWh");

                string jsonContent = await req.Content.ReadAsStringAsync();

                log.Info("ResponseWh == jsonContent == " + jsonContent);

                var data = JsonConvert.DeserializeObject<ServiceInvoice>(jsonContent);

                log.Info("ResponseWh == data == " + data);
                log.Info("ResponseWh == data.FlowStatu == " + data.FlowStatus);

                switch (data.FlowStatus)
                {
                    case Utils.Issued:
                        {
                            log.Info("ResponseWh == antes da fila de IssuedInvoiceQueue");
                            new IssuedInvoiceQueue().Insert(jsonContent);
                            log.Info("ResponseWh == depois da fila de IssuedInvoiceQueue");
                            break;
                        }
                    case Utils.IssuedFailed:
                        {
                            log.Info("ResponseWh == antes da fila de FailedInvoiceQueue");
                            new FailedInvoiceQueue().Insert(jsonContent);
                            log.Info("ResponseWh == depois da fila de FailedInvoiceQueue");
                            break;
                        }
                    case Utils.Cancelled:
                        {
                            log.Info("ResponseWh == antes da fila de CancellationIssuedInvoiceQueue");
                            new CancellationIssuedInvoiceQueue().Insert(jsonContent);
                            log.Info("ResponseWh == depois da fila de CancellationIssuedInvoiceQueue");
                            break;
                        }
                    default:
                        log.Info("ResponseWh == antes da fila de DefaultInvoiceQueue");
                        new DefaultInvoiceQueue().Insert(jsonContent);
                        log.Info("ResponseWh == depois da fila de DefaultInvoiceQueue");
                        break;
                }

                return req.CreateResponse(HttpStatusCode.OK);
            }
            catch(Exception ex)
            {
                log.Info("ResponseWh == Exception == ", ex.Message);
                return req.CreateResponse(HttpStatusCode.BadRequest);
            }
        }

    }
}
