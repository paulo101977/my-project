using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Host;
using Newtonsoft.Json;
using System;
using System.Threading.Tasks;
using THEx.Integration.NFeIO.Commom;
using THEx.Integration.NFeIO.DTO;
using THEx.Integration.NFeIO.DTO.PMS;
using THEx.Integration.NFeIO.Service;
using THEx.Integration.NFeIO.Storage.Queue;

namespace THEx.Integration.NFeIO.Functions.QueueTrigger
{
    public static class IssuedInvoiceQt
    {
        [FunctionName("QueueTriggerIssuedInvoice")]
        public static async Task RunAsync(
            [ServiceBusTrigger("%QueueThexNfeioIssuedInvoice%", Connection = "ServiceBusConnection")]
            string sJson,
            Int32 deliveryCount,
            DateTime enqueuedTimeUtc,
            string messageId,
            TraceWriter log)
        {
            try
            {
                log.Info("Entrou em IssuedInvoiceQt == json == " + sJson);

                InvoiceNfeioDto invoiceDto = new InvoiceNfeioDto();

                invoiceDto.ServiceInvoice = JsonConvert.DeserializeObject<ServiceInvoice>(sJson);

                log.Info("IssuedInvoiceQt == invoiceDto.ServiceInvoice.RpsSerialNumber == " + invoiceDto.ServiceInvoice.RpsSerialNumber);
                log.Info("IssuedInvoiceQt == invoiceDto.ServiceInvoice.RpsNumber == " + invoiceDto.ServiceInvoice.RpsNumber);

                var propertyService = new PropertyService();
                var propertyMappingEntity = propertyService
                    .GetPropertyMappingByExternalId(invoiceDto.ServiceInvoice.Provider.id);

                log.Info("IssuedInvoiceQt == propertyMappingEntity == " + (propertyMappingEntity != null).ToString());

                invoiceDto.Document = propertyMappingEntity.Document;
                invoiceDto.ExternalPropertyId = propertyMappingEntity.ExternalId;
                invoiceDto.PropertyId = propertyMappingEntity.PropertyId;

                log.Info("IssuedInvoiceQt == invoiceDto.Document == " + invoiceDto.Document);
                log.Info("IssuedInvoiceQt == invoiceDto.ExternalPropertyId == " + invoiceDto.ExternalPropertyId);
                log.Info("IssuedInvoiceQt == invoiceDto.PropertyId == " + invoiceDto.PropertyId);

                var rpsPropertyService = new RpsPropertyService(invoiceDto);
                rpsPropertyService.UpdateControlRpsProperty((int)Utils.StatusInvoice.Processed);

                log.Info("Passou do update");

                var invoiceMappingService = new InvoiceMappingService();
                var invoiceMappingEntity = invoiceMappingService.GetByPropertyAndInvoiceExternalId(
                    invoiceDto.PropertyId, invoiceDto.ServiceInvoice.Id, log);

                log.Info("IssuedInvoiceQt == invoiceMappingEntity == " + (invoiceMappingEntity != null).ToString());

                var invoiceResultDto = new InvoiceResultDto()
                {
                    PropertyId = invoiceDto.PropertyId,
                    InvoiceId = invoiceMappingEntity.InvoiceId,
                    LinkPdf = ApisNfeIo.UrlGetNfsePdf(invoiceDto.ExternalPropertyId, invoiceDto.ServiceInvoice.Id),
                    LinkXml = ApisNfeIo.UrlGetNfseXml(invoiceDto.ExternalPropertyId, invoiceDto.ServiceInvoice.Id),
                    Number = invoiceDto.ServiceInvoice.Number,
                    RpsSerialNumber = invoiceDto.ServiceInvoice.RpsSerialNumber,
                    RpsNumber = invoiceDto.ServiceInvoice.RpsNumber,
                    TenantId = invoiceMappingEntity.TenantId,
                    IsCancel = false
                };

                var resultDto = new ResultDto()
                {
                    Service = Utils.FunctionAppService,
                    Message = invoiceResultDto
                };

                string invoiceJson = JsonConvert.SerializeObject(resultDto);

                log.Info("IssuedInvoiceQt == Antes da fila");
                await new ReturnStatusInvoiceQueue().Insert(invoiceJson);
                log.Info("IssuedInvoiceQt == Depois da fila");

                log.Info("IssuedInvoiceQt == invoiceDto.ServiceInvoice.RpsSerialNumber antes do SearchAndSendSuccessorInvoice == " + invoiceDto.ServiceInvoice.RpsSerialNumber);

                await new InvoiceAwaitingService().SearchAndSendSuccessorInvoiceAsync(invoiceDto, log);
            }
            catch(Exception ex)
            {
                log.Info("IssuedInvoiceQt == Exception == " + ex.Message);
            }
        }
    }
}
