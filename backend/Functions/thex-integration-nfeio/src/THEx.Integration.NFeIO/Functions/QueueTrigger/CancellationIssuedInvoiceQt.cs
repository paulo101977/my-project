using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Host;
using Newtonsoft.Json;
using System;
using System.Threading.Tasks;
using THEx.Integration.NFeIO.Commom;
using THEx.Integration.NFeIO.DTO;
using THEx.Integration.NFeIO.DTO.PMS;
using THEx.Integration.NFeIO.Service;
using THEx.Integration.NFeIO.Storage.Queue;

namespace THEx.Integration.NFeIO.Functions.QueueTrigger
{
    public static class CancellationIssuedInvoiceQt
    {
        [FunctionName("QueueTriggerCancellationIssuedInvoice")]
        public static async Task RunAsync(
            [ServiceBusTrigger("%QueueThexNfeioCancellationIssuedInvoice%", Connection = "ServiceBusConnection")]
            string sJson,
            Int32 deliveryCount,
            DateTime enqueuedTimeUtc,
            string messageId,
            TraceWriter log)
        {
            try
            {
                log.Info("Entrou em CancellationIssuedInvoiceQt");

                var invoiceDto = new InvoiceNfeioDto();

                invoiceDto.ServiceInvoice = JsonConvert.DeserializeObject<ServiceInvoice>(sJson);

                log.Info("CancellationIssuedInvoiceQt == invoiceDto.ServiceInvoice.RpsSerialNumber == " + invoiceDto.ServiceInvoice.RpsSerialNumber);
                log.Info("CancellationIssuedInvoiceQt == invoiceDto.ServiceInvoice.RpsNumber == " + invoiceDto.ServiceInvoice.RpsNumber);
                log.Info("CancellationIssuedInvoiceQt == invoiceDto.Document == " + invoiceDto.Document);

                var propertyService = new PropertyService();
                var propertyMappingEntity = propertyService
                    .GetPropertyMappingByExternalId(invoiceDto.ServiceInvoice.Provider.id);

                log.Info("CancellationIssuedInvoiceQt == propertyMappingEntity == " + (propertyMappingEntity != null).ToString());

                invoiceDto.Document = propertyMappingEntity.Document;
                invoiceDto.ExternalPropertyId = propertyMappingEntity.ExternalId;
                invoiceDto.PropertyId = propertyMappingEntity.PropertyId;

                var invoiceMappingService = new InvoiceMappingService();
                var invoiceMappingEntity = invoiceMappingService.GetByPropertyAndInvoiceExternalId(
                    invoiceDto.PropertyId, invoiceDto.ServiceInvoice.Id, log);

                log.Info("CancellationIssuedInvoiceQt == invoiceMappingEntity == " + (invoiceMappingEntity != null).ToString());

                var invoiceResultDto = new InvoiceResultDto()
                {
                    PropertyId = invoiceDto.PropertyId,
                    InvoiceId = invoiceMappingEntity.InvoiceId,
                    Number = invoiceDto.ServiceInvoice.Number,
                    RpsSerialNumber = invoiceDto.ServiceInvoice.RpsSerialNumber,
                    RpsNumber = invoiceDto.ServiceInvoice.RpsNumber,
                    TenantId = invoiceMappingEntity.TenantId,
                    IsCancel = true
                };

                var resultDto = new ResultDto()
                {
                    Service = Utils.FunctionAppService,
                    Message = invoiceResultDto
                };

                log.Info("CancellationIssuedInvoiceQt == Antes de enviar para a fila");
                string invoiceJson = JsonConvert.SerializeObject(resultDto);
                log.Info("CancellationIssuedInvoiceQt == Depois de enviar para a fila");
                await new ReturnStatusInvoiceQueue().Insert(invoiceJson);
            }
            catch(Exception ex)
            {
                log.Info("CancellationIssuedInvoiceQt == Exception == " + ex.Message);
            }
        }
    }
}
