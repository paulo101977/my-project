using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Host;
using Newtonsoft.Json;
using System;
using System.Net;
using THEx.Integration.NFeIO.Commom;
using THEx.Integration.NFeIO.DTO;
using THEx.Integration.NFeIO.DTO.PMS;
using THEx.Integration.NFeIO.Entity;
using THEx.Integration.NFeIO.Service;
using THEx.Integration.NFeIO.Storage.Queue;

namespace THEx.Integration.NFeIO.Functions.QueueTrigger
{
    public static class PrepareCancelInvoiceQt
    {
        [FunctionName("QueueTriggerPrepareCancelInvoice")]
        public static void Run(
            [ServiceBusTrigger("%QueueThexNfeioPrepareCancelInvoice%", Connection = "ServiceBusConnection")]
            string sJson,
            Int32 deliveryCount,
            DateTime enqueuedTimeUtc,
            string messageId,
            TraceWriter log)
        {
            try
            {
                log.Info("Entrou em PrepareCancelInvoiceQt");

                InvoiceNfeioDto invoiceDto = JsonConvert.DeserializeObject<InvoiceNfeioDto>(sJson);

                log.Info("PrepareCancelInvoiceQt == Entrou " + invoiceDto.Document);

                ErrorValidationService errorValidationService = new ErrorValidationService(invoiceDto.PropertyId);

                InvoiceMappingService invoiceMappingService = new InvoiceMappingService();
                InvoiceMappingEntity invoiceMappingEntity = invoiceMappingService.GetMappingByPropertyAndInvoice(
                    invoiceDto.PropertyId, invoiceDto.InvoiceId);

                log.Info("PrepareCancelInvoiceQt == invoiceMappingEntity " + (invoiceMappingEntity != null).ToString());

                if (invoiceMappingEntity == null)
                {
                    errorValidationService.CreateError(CodeMessages.Error002007, "InvoiceId: " + invoiceDto.InvoiceId, null, invoiceDto.InvoiceId, invoiceDto.TenantId);
                    errorValidationService.SendError(Utils.FunctionAppService);
                    return;
                }

                PropertyService propertyService = new PropertyService();
                invoiceDto.ExternalPropertyId = propertyService.GetPropertyMapping(invoiceDto.PropertyId);

                log.Info("PrepareCancelInvoiceQt == invoiceDto.ExternalPropertyId " + invoiceDto.ExternalPropertyId);

                if (string.IsNullOrEmpty(invoiceDto.ExternalPropertyId))
                {
                    errorValidationService.CreateError(CodeMessages.Error002003, invoiceDto.Document, null, invoiceDto.InvoiceId, invoiceDto.TenantId);
                    errorValidationService.SendError(Utils.FunctionAppService);
                    return;
                }

                InvoiceService invoiceService = new InvoiceService();
                ApiResultDto apiResultDto = invoiceService.CancelInvoice(
                    invoiceDto.ExternalPropertyId, invoiceMappingEntity.InvoiceExternalId);

                log.Info("PrepareCancelInvoiceQt == apiResultDto Status" + apiResultDto.HttpStatusCode);
                log.Info("PrepareCancelInvoiceQt == apiResultDto Message" + apiResultDto.Message);

                if ((apiResultDto.HttpStatusCode != (int)HttpStatusCode.OK) && (apiResultDto.HttpStatusCode != (int)HttpStatusCode.Created)
                    && (apiResultDto.HttpStatusCode != (int)HttpStatusCode.Accepted))
                {
                    string resultJson = JsonConvert.SerializeObject(apiResultDto);
                    errorValidationService.CreateError(CodeMessages.Error003001, null, resultJson, invoiceDto.InvoiceId, invoiceDto.TenantId);
                    errorValidationService.SendError(Utils.ExternalService);
                }
            }
            catch(Exception ex)
            {
                log.Info("PrepareCancelInvoiceQt == Exception " + ex.Message);
            }
        }
    }
}
