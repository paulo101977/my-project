using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Host;
using Newtonsoft.Json;
using System;
using System.Net;
using THEx.Integration.NFeIO.Commom;
using THEx.Integration.NFeIO.DTO;
using THEx.Integration.NFeIO.DTO.PMS;
using THEx.Integration.NFeIO.Service;

namespace THEx.Integration.NFeIO.Functions.QueueTrigger
{
    public static class SendInvoiceQt
    {
        [FunctionName("QueueTriggerSendInvoice")]
        public static void Run(
            [ServiceBusTrigger("%QueueThexNfeioInvoiceWaitingSent%", Connection = "ServiceBusConnection")]
            string sJson,
            Int32 deliveryCount,
            DateTime enqueuedTimeUtc,
            string messageId,
            TraceWriter log)
        {
            try
            {
                InvoiceNfeioDto invoiceDto = JsonConvert.DeserializeObject<InvoiceNfeioDto>(sJson);

                RpsPropertyService rpsPropertyService = new RpsPropertyService(invoiceDto);

                log.Info("SendInvoiceQt == invoiceDto.Document == " + invoiceDto.Document);
                log.Info("SendInvoiceQt == invoiceDto.ExternalPropertyId == " + invoiceDto.ExternalPropertyId);

                rpsPropertyService.UpdateControlRpsProperty((int)Utils.StatusInvoice.Processing);

                log.Info("Antes de enviar a s�rie: " + invoiceDto.ServiceInvoice.RpsSerialNumber + " com n�mero: " + invoiceDto.ServiceInvoice.RpsNumber);

                InvoiceService invoiceService = new InvoiceService();
                ApiResultDto apiResultDto = invoiceService.SendInvoice(
                    invoiceDto.ExternalPropertyId, invoiceDto.ServiceInvoice);

                if (apiResultDto.HttpStatusCode == (int)HttpStatusCode.Accepted)
                {
                    log.Info("Sucesso ao enviar a s�rie: " + invoiceDto.ServiceInvoice.RpsSerialNumber + " com n�mero: " + invoiceDto.ServiceInvoice.RpsNumber);
                    
                    invoiceDto.ServiceInvoice = JsonConvert.DeserializeObject<ServiceInvoice>(apiResultDto.Message);

                    InvoiceMappingService invoiceMappingService = new InvoiceMappingService();
                    invoiceMappingService.CreateMapping(invoiceDto);
                }
                else
                {
                    log.Info("Erro ao enviar para o Nfeio == " + apiResultDto.Message + " a nota com a s�rie: " +
                        invoiceDto.ServiceInvoice.RpsSerialNumber + " com n�mero: " + invoiceDto.ServiceInvoice.RpsNumber);

                    rpsPropertyService.UpdateControlRpsProperty((int)Utils.StatusInvoice.Erro);

                    ErrorValidationService errorValidationService = new ErrorValidationService(invoiceDto.PropertyId);
                    string resultJson = JsonConvert.SerializeObject(apiResultDto);

                    errorValidationService.CreateError(CodeMessages.Error003001, null, resultJson, invoiceDto.InvoiceId, invoiceDto.TenantId);
                    errorValidationService.SendError(Utils.ExternalService);
                }
            }
            catch(Exception ex)
            {
                log.Info("catch SendInvoiceQt== " + ex.Message);
            }
        }
    }
}
