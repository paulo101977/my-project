using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Host;
using System;
using THEx.Integration.NFeIO.Service;

namespace THEx.Integration.NFeIO.Functions.QueueTrigger
{
    public static class SendInvoiceResponseQt
    {
        [FunctionName("QueueTriggerSendInvoiceResponse")]
        public static void Run(
            [ServiceBusTrigger("%QueueThexNfeioReturnStatusInvoice%", Connection = "ServiceBusConnection")]
            string sJson,
            Int32 deliveryCount,
            DateTime enqueuedTimeUtc,
            string messageId,
            TraceWriter log)
        {
            log.Info("Entrou em SendInvoiceResponseQt");

            InvoiceService invoiceService = new InvoiceService();

            invoiceService.SendInvoiceResponseTHEx(sJson, log);
        }
    }
}
