using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Host;
using Newtonsoft.Json;
using System;
using System.Threading.Tasks;
using THEx.Integration.NFeIO.Commom;
using THEx.Integration.NFeIO.DTO;
using THEx.Integration.NFeIO.Entity;
using THEx.Integration.NFeIO.Service;
using THEx.Integration.NFeIO.Storage.Queue;

namespace THEx.Integration.NFeIO.Functions.QueueTrigger
{
    public static class PrepareInvoiceQt
    {
        [FunctionName("QueueTriggerPrepareInvoice")]
        public static async Task RunAsync(
            [ServiceBusTrigger("%QueueThexNfeioPrepareInvoice%", Connection = "ServiceBusConnection")]
            string sJson,
            Int32 deliveryCount,
            DateTime enqueuedTimeUtc,
            string messageId,
            TraceWriter log)
        {
            try
            {
                log.Info("PrepareInvoiceQt == Json == " + sJson);

                InvoiceNfeioDto invoiceDto = JsonConvert.DeserializeObject<InvoiceNfeioDto>(sJson);

                ErrorValidationService errorValidationService = new ErrorValidationService(invoiceDto.PropertyId);

                PropertyService propertyService = new PropertyService();

                invoiceDto.ExternalPropertyId = propertyService.GetPropertyMappingOrCreateMapping(
                    invoiceDto.PropertyId, invoiceDto.Document);

                log.Info("PrepareInvoiceQt == invoiceDto.ServiceInvoice.RpsSerialNumber " + invoiceDto.ServiceInvoice.RpsSerialNumber);
                log.Info("PrepareInvoiceQt == invoiceDto.ServiceInvoice.RpsNumber " + invoiceDto.ServiceInvoice.RpsNumber);

                if (string.IsNullOrEmpty(invoiceDto.ExternalPropertyId))
                {
                    errorValidationService.CreateError(CodeMessages.Error002003, invoiceDto.Document, null, invoiceDto.InvoiceId, invoiceDto.TenantId);
                    errorValidationService.SendError(Utils.FunctionAppService);
                    return;
                }

                RpsPropertyService rpsPropertyService = new RpsPropertyService(invoiceDto);

                RpsPropertyEntity rpsPropertyEntity = rpsPropertyService.FindControlRpsProperty(log);

                if (rpsPropertyService.ValidateAvailabilityForProcessing(rpsPropertyEntity))
                {
                    string invoiceJson = JsonConvert.SerializeObject(invoiceDto);

                    log.Info("PrepareInvoiceQt == Pode Processara s�rie: " + invoiceDto.ServiceInvoice.RpsSerialNumber + " com n�mero: " + invoiceDto.ServiceInvoice.RpsNumber);

                    await new InvoiceWatingSentQueue().Insert(invoiceJson);
                }
                else
                {
                    Boolean invoiceWasResent = errorValidationService.InvoiceWasResent(invoiceDto, log);

                    log.Info("PrepareInvoiceQt == invoiceWasResent " + invoiceWasResent.ToString() + "com a s�rie: " +
                        invoiceDto.ServiceInvoice.RpsSerialNumber + " com n�mero: " + invoiceDto.ServiceInvoice.RpsNumber);

                    if (invoiceWasResent)
                    {
                        errorValidationService.SendError(Utils.FunctionAppService);
                    }
                    else
                    {
                        InvoiceAwaitingService invoiceAwaitingService = new InvoiceAwaitingService();
                        invoiceAwaitingService.SaveInvoice(invoiceDto);
                        log.Info("PrepareInvoiceQt == Depois de invoiceAwaitingService" + "com a s�rie: " +
                        invoiceDto.ServiceInvoice.RpsSerialNumber + " com n�mero: " + invoiceDto.ServiceInvoice.RpsNumber);
                    }
                }
            }
            catch (Exception ex)
            {
                log.Info("catch PrepareInvoiceQt== " + ex.Message);
            }
        }
    }
}
