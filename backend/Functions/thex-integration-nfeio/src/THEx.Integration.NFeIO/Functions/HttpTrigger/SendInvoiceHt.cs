using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Azure.WebJobs.Host;
using Newtonsoft.Json;
using THEx.Integration.NFeIO.Commom;
using THEx.Integration.NFeIO.DTO;
using THEx.Integration.NFeIO.DTO.PMS;
using THEx.Integration.NFeIO.Service;
using THEx.Integration.NFeIO.Storage.Queue;

namespace THEx.Integration.NFeIO.Functions.HttpTrigger
{
    public static class SendInvoiceHt
    {
        [FunctionName("HttpTriggerSendInvoice")]
        public static async Task<HttpResponseMessage> Run(
            [HttpTrigger(AuthorizationLevel.Function, "post", Route = "v1/nfeio/invoice")]
        HttpRequestMessage req, TraceWriter log)
        {
            try
            {
                string sJson = await req.Content.ReadAsStringAsync();

                InvoiceLotDto invoiceLotDto = JsonConvert.DeserializeObject<InvoiceLotDto>(sJson);

                ErrorValidationService errorValidationService = new ErrorValidationService(null);

                if (errorValidationService.InvalidRequest(invoiceLotDto))
                {
                    log.Info("SendInvoiceHt == dentro de errorValidationService.InvalidRequest(invoiceLotDto)");

                    ResultDto resultDto = errorValidationService.GetResultErrors(Utils.FunctionAppService);
                    return req.CreateResponse(HttpStatusCode.BadRequest, resultDto);
                }

                errorValidationService.DefaultInformationValidation(invoiceLotDto);

                List<InvoiceNfeioDto> invoiceNfeioDtoList = new List<InvoiceNfeioDto>();

                foreach (InvoicePmsDto invoicePmsDto in invoiceLotDto.Invoices.OrderBy(b => b.RpsNumber))
                {
                    Boolean invoiceValidated = errorValidationService.InvoiceValidation(invoicePmsDto, invoiceLotDto.TenantId);

                    log.Info("SendInvoiceHt == invoiceLotDto.Invoices.Count() " + invoiceValidated);

                    if (invoiceValidated)
                    {
                        ServiceInvoice serviceInvoice = new ServiceInvoice()
                        {
                            Borrower = invoiceLotDto.Borrower,
                            CityServiceCode = invoicePmsDto.CityServiceCode,
                            Description = invoicePmsDto.Description,
                            ServicesAmount = invoicePmsDto.InvoiceAmount,
                            DeductionsAmount = invoicePmsDto.DeductionsAmount,
                            RpsNumber = invoicePmsDto.RpsNumber,
                            RpsSerialNumber = invoicePmsDto.RpsSerialNumber,
                        };

                        InvoiceNfeioDto invoiceDto = new InvoiceNfeioDto()
                        {
                            Document = invoiceLotDto.Document,
                            PropertyId = invoiceLotDto.PropertyId,
                            InvoiceId = invoicePmsDto.Id,
                            ServiceInvoice = serviceInvoice,
                            TenantId = invoiceLotDto.TenantId
                        };

                        invoiceNfeioDtoList.Add(invoiceDto);
                    }
                }

                if (errorValidationService.IsThereAnErro())
                {
                    ResultDto resultDto = errorValidationService.GetResultErrors(Utils.FunctionAppService);
                    return req.CreateResponse(HttpStatusCode.BadRequest, resultDto);
                }

                foreach (InvoiceNfeioDto invoiceDto in invoiceNfeioDtoList.OrderBy(b => b.ServiceInvoice.RpsNumber))
                {
                    sJson = JsonConvert.SerializeObject(invoiceDto);
                    log.Info("SendInvoiceHt == Antes da fila == RpsNumber == " + invoiceDto.ServiceInvoice.RpsNumber);
                    await new PrepareInvoiceQueue(log).Insert(sJson, log);
                    log.Info("SendInvoiceHt == Depois da fila == RpsNumber == " + invoiceDto.ServiceInvoice.RpsNumber);
                }
                return req.CreateResponse(HttpStatusCode.OK);
            }
            catch(Exception ex)
            {
                log.Info("catch sendInvoiceHt== " + ex.Message);
                return req.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
    }
}
