using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Azure.WebJobs.Host;
using Newtonsoft.Json;
using THEx.Integration.NFeIO.Commom;
using THEx.Integration.NFeIO.DTO;
using THEx.Integration.NFeIO.DTO.PMS;
using THEx.Integration.NFeIO.Service;
using THEx.Integration.NFeIO.Storage.Queue;

namespace THEx.Integration.NFeIO.Functions.HttpTrigger
{
    public static class CancelInvoinceHt
    {
        [FunctionName("HttpTriggerCancelInvoince")]
        public static async Task<HttpResponseMessage> Run(
            [HttpTrigger(AuthorizationLevel.Function, "delete", Route = "v1/nfeio/invoice")]
        HttpRequestMessage req, TraceWriter log)
        {
            try
            {
                log.Info("Entrou em CancelInvoinceHt");

                string sJson = await req.Content.ReadAsStringAsync();

                InvoiceNfeioDto invoiceDto = JsonConvert.DeserializeObject<InvoiceNfeioDto>(sJson);

                log.Info("CancelInvoinceHt == Entrou em SendInvoiceHt com document == " + invoiceDto.Document);

                ErrorValidationService errorValidationService = new ErrorValidationService(invoiceDto.PropertyId);
                errorValidationService.DefaultInformationValidation(invoiceDto);

                if (errorValidationService.IsThereAnErro())
                {
                    log.Info("CancelInvoinceHt == tem erro");
                    ResultDto resultDto = errorValidationService.GetResultErrors(Utils.FunctionAppService);
                    log.Info("CancelInvoinceHt == mensagem do erro == " + resultDto.Message);
                    return req.CreateResponse(HttpStatusCode.BadRequest, resultDto);
                }
                else
                {
                    log.Info("CancelInvoinceHt == sucesso");
                    sJson = JsonConvert.SerializeObject(invoiceDto);
                    await new PrepareCancelInvoiceQueue().Insert(sJson);

                    return req.CreateResponse(HttpStatusCode.OK);
                }
            }
            catch(Exception ex)
            {
                log.Info("CancelInvoinceHt == Exception == " + ex.Message);
                return req.CreateResponse(HttpStatusCode.BadRequest);
            }
        }
    }
}
