using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Azure.WebJobs.Host;
using Newtonsoft.Json;
using THEx.Integration.NFeIO.Commom;
using THEx.Integration.NFeIO.DTO;
using THEx.Integration.NFeIO.DTO.PMS;
using THEx.Integration.NFeIO.Service;
using THEx.Integration.NFeIO.Storage.Queue;

namespace THEx.Integration.NFeIO.Functions.HttpTrigger
{
    public static class SendInvoiceCalculatedHt
    {
        [FunctionName("HttpTriggerSendInvoiceCalculated")]
        public static async Task<HttpResponseMessage> Run(
            [HttpTrigger(AuthorizationLevel.Function, "post", Route = "v1/nfeio/invoice/calculated")]
        HttpRequestMessage req, TraceWriter log)
        {
            string sJson = await req.Content.ReadAsStringAsync();
            InvoiceLotDto invoiceLotDto = JsonConvert.DeserializeObject<InvoiceLotDto>(sJson);

            ErrorValidationService errorValidationService = new ErrorValidationService(null);

            if(errorValidationService.InvalidRequest(invoiceLotDto))
            {
                ResultDto resultDto = errorValidationService.GetResultErrors(Utils.FunctionAppService);
                return req.CreateResponse(HttpStatusCode.BadRequest, resultDto);
            }

            errorValidationService.DefaultInformationValidation(invoiceLotDto);

            List<InvoiceNfeioDto> invoiceNfeioDtoList = new List<InvoiceNfeioDto>();
            foreach (InvoicePmsDto invoicePmsDto in invoiceLotDto.Invoices)
            {
                Boolean invoiceValidated = errorValidationService.InvoiceValidationCalculated(invoicePmsDto, invoiceLotDto.TenantId);

                if (invoiceValidated) {
                    ServiceInvoice serviceInvoice = new ServiceInvoice()
                    {
                        Borrower = invoiceLotDto.Borrower,
                        CityServiceCode = invoicePmsDto.CityServiceCode,
                        Description = invoicePmsDto.Description,
                        ServicesAmount = invoicePmsDto.InvoiceAmount,
                        DeductionsAmount = invoicePmsDto.DeductionsAmount,
                        RpsNumber = invoicePmsDto.RpsNumber,
                        RpsSerialNumber = invoicePmsDto.RpsSerialNumber,
                        IssRate = invoicePmsDto.IssRatePercentage / 100,
                        IssTaxAmount = invoicePmsDto.IssTaxAmount,
                        IssAmountWithheld = (invoicePmsDto.IsIssWithheld) ? invoicePmsDto.IssTaxAmount : 0
                    };

                    InvoiceNfeioDto invoiceDto = new InvoiceNfeioDto()
                    {
                        Document = invoiceLotDto.Document,
                        PropertyId = invoiceLotDto.PropertyId,
                        InvoiceId = invoicePmsDto.Id,
                        ServiceInvoice = serviceInvoice
                    };

                    invoiceNfeioDtoList.Add(invoiceDto);
                }
            }

            if(errorValidationService.IsThereAnErro())
            {
                ResultDto resultDto = errorValidationService.GetResultErrors(Utils.FunctionAppService);
                return req.CreateResponse(HttpStatusCode.BadRequest, resultDto);
            }

            foreach (InvoiceNfeioDto invoiceDto in invoiceNfeioDtoList)
            {
                sJson = JsonConvert.SerializeObject(invoiceDto);
                await new PrepareInvoiceQueue(log).Insert(sJson);
            }
            return req.CreateResponse(HttpStatusCode.OK);
        }
    }
}
