﻿using Microsoft.WindowsAzure.Storage.Table;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using THEx.Integration.NFeIO.Entity;

namespace THEx.Integration.NFeIO.Storage.Table
{
    class RpsPropertyTable : BaseTable
    {
        string TableDescription = Environment.GetEnvironmentVariable("TableThexNfeioRpsProperty", EnvironmentVariableTarget.Process);

        public RpsPropertyTable()
        {
            CreateConnection(TableDescription);
        }

        public TableResult Insert(RpsPropertyEntity entity)
        {
            TableOperation insertOperation = TableOperation.Insert(entity);

            return ExecuteOperation(insertOperation);

        }

        public TableResult Update(RpsPropertyEntity entity)
        {
            TableOperation updateOperation = TableOperation.Replace(entity);

            return ExecuteOperation(updateOperation);

        }

        public List<RpsPropertyEntity> FindByPropertyAndRpsSerie(string propertyId, string rpsSerieNumber)
        {
            TableQuery<RpsPropertyEntity> query = new TableQuery<RpsPropertyEntity>()
                .Where(TableQuery.CombineFilters(
                    TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.Equal, propertyId),
                    TableOperators.And,
                    TableQuery.GenerateFilterCondition("RpsSerieNumber", QueryComparisons.Equal, rpsSerieNumber)));

            List<RpsPropertyEntity> identifierMappingEntityList = new List<RpsPropertyEntity>();

            TableContinuationToken token = null;

            TableQuerySegment<RpsPropertyEntity> resultSegment =
                    this.cloudTable.ExecuteQuerySegmented(query, token);
            token = resultSegment.ContinuationToken;


            foreach (RpsPropertyEntity entity in resultSegment.Results)
            {
                identifierMappingEntityList.Add(entity);

            }

            return identifierMappingEntityList;
        }
    }
}
