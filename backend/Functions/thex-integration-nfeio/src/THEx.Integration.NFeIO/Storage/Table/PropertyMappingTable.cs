﻿using Microsoft.WindowsAzure.Storage.Table;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using THEx.Integration.NFeIO.Entity;

namespace THEx.Integration.NFeIO.Storage.Table
{
    class PropertyMappingTable : BaseTable
    {
        string TableDescription = Environment.GetEnvironmentVariable("TableThexNfeioPropertyMapping", EnvironmentVariableTarget.Process);

        public PropertyMappingTable()
        {
            CreateConnection(TableDescription);
        }

        public TableResult Insert(PropertyMappingEntity entity)
        {
            TableOperation insertOperation = TableOperation.Insert(entity);

            return ExecuteOperation(insertOperation);

        }

        public PropertyMappingEntity FindByProperty(string propertyId)
        {
            TableQuery<PropertyMappingEntity> query = new TableQuery<PropertyMappingEntity>()
                .Where(TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.Equal, propertyId));

            PropertyMappingEntity propertyMappingEntity = SingleResult(query);
            return propertyMappingEntity;
        }

        public PropertyMappingEntity FindByPropertyExternalId(string externalId)
        {
            TableQuery<PropertyMappingEntity> query = new TableQuery<PropertyMappingEntity>()
                .Where(TableQuery.GenerateFilterCondition("RowKey", QueryComparisons.Equal, externalId));

            PropertyMappingEntity propertyMappingEntity = SingleResult(query);
            return propertyMappingEntity;
        }

        public PropertyMappingEntity FindByDocument(string document)
        {
            TableQuery<PropertyMappingEntity> query = new TableQuery<PropertyMappingEntity>()
                .Where(TableQuery.GenerateFilterCondition("Document", QueryComparisons.Equal, document));

            PropertyMappingEntity propertyMappingEntity = SingleResult(query);
            return propertyMappingEntity;
        }

    private PropertyMappingEntity SingleResult(TableQuery<PropertyMappingEntity> query)
        {
            List<PropertyMappingEntity> identifierMappingEntityList = ExecuteQuery(query);

            if (identifierMappingEntityList.Count == 0)
            {
                return null;
            }
            else
            {
                return identifierMappingEntityList[0];
            }
        }

        private List<PropertyMappingEntity> ListResult(TableQuery<PropertyMappingEntity> query)
        {
            return ExecuteQuery(query);
        }

        private List<PropertyMappingEntity> ExecuteQuery(TableQuery<PropertyMappingEntity> query)
        {
            List<PropertyMappingEntity> identifierMappingEntityList = new List<PropertyMappingEntity>();

            TableContinuationToken token = null;

            TableQuerySegment<PropertyMappingEntity> resultSegment =
                    this.cloudTable.ExecuteQuerySegmented(query, token);
            token = resultSegment.ContinuationToken;


            foreach (PropertyMappingEntity entity in resultSegment.Results)
            {
                identifierMappingEntityList.Add(entity);

            }


            //do
            //{
            //    TableQuerySegment<PropertyMappingEntity> resultSegment =
            //        this.cloudTable.ExecuteQuerySegmented(query, token);
            //    token = resultSegment.ContinuationToken;


            //    foreach (PropertyMappingEntity entity in resultSegment.Results)
            //    {
            //        identifierMappingEntityList.Add(entity);

            //    }
            //} while (token != null);

            return identifierMappingEntityList;
        }
    }
}
