﻿using Microsoft.WindowsAzure.Storage.Table;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using THEx.Integration.NFeIO.Entity;

namespace THEx.Integration.NFeIO.Storage.Table
{
    class InvoiceAwaitingTable : BaseTable
    {
        string TableDescription = Environment.GetEnvironmentVariable("TableThexNfeioRpsInvoiceAwaiting", EnvironmentVariableTarget.Process);

        public InvoiceAwaitingTable()
        {
            CreateConnection(TableDescription);
        }

        public TableResult Insert(InvoiceAwaitingEntity entity)
        {
            TableOperation insertOperation = TableOperation.Insert(entity);

            return ExecuteOperation(insertOperation);

        }

        public TableResult Delete(InvoiceAwaitingEntity entity)
        {
            TableOperation insertOperation = TableOperation.Delete(entity);

            return ExecuteOperation(insertOperation);

        }

        public InvoiceAwaitingEntity FindByPropertyAndRps(
            string propertyId, string serialNumber, int rpsNumber)
        {
            TableQuery<InvoiceAwaitingEntity> query = new TableQuery<InvoiceAwaitingEntity>()
                .Where(TableQuery.CombineFilters(
                    TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.Equal, propertyId),
                    TableOperators.And,
                    TableQuery.GenerateFilterCondition("RowKey", QueryComparisons.Equal, 
                        serialNumber + Convert.ToString(rpsNumber))));

            List<InvoiceAwaitingEntity> identifierMappingEntityList = new List<InvoiceAwaitingEntity>();

            TableContinuationToken token = null;
            
            TableQuerySegment<InvoiceAwaitingEntity> resultSegment =
                this.cloudTable.ExecuteQuerySegmented(query, token);

            token = resultSegment.ContinuationToken;


            foreach (InvoiceAwaitingEntity entity in resultSegment.Results)
            {
                identifierMappingEntityList.Add(entity);

            }
            //do
            //{
            //    TableQuerySegment<InvoiceAwaitingEntity> resultSegment =
            //        this.cloudTable.ExecuteQuerySegmented(query, token);
            //    token = resultSegment.ContinuationToken;


            //    foreach (InvoiceAwaitingEntity entity in resultSegment.Results)
            //    {
            //        identifierMappingEntityList.Add(entity);

            //    }
            //} while (token != null);

            if (identifierMappingEntityList.Count == 0)
            {
                return null;
            }

            return identifierMappingEntityList[0];
        }
    }
}
