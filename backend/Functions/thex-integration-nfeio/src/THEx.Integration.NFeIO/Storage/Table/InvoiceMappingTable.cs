﻿using Microsoft.WindowsAzure.Storage.Table;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using THEx.Integration.NFeIO.Entity;

namespace THEx.Integration.NFeIO.Storage.Table
{
    class InvoiceMappingTable : BaseTable
    {
        string TableDescription = Environment.GetEnvironmentVariable("TableThexNfeioInvoiceMapping", EnvironmentVariableTarget.Process);

        public InvoiceMappingTable()
        {
            CreateConnection(TableDescription);
        }

        public TableResult Insert(InvoiceMappingEntity entity)
        {
            TableOperation insertOperation = TableOperation.Insert(entity);

            return ExecuteOperation(insertOperation);

        }

        public TableResult Update(InvoiceMappingEntity entity)
        {
            TableOperation insertOperation = TableOperation.Replace(entity);

            return ExecuteOperation(insertOperation);

        }

        public InvoiceMappingEntity FindByPropertyAndInvoice(string propertyId, string invoiceId)
        {
            TableQuery<InvoiceMappingEntity> query = new TableQuery<InvoiceMappingEntity>()
                .Where(TableQuery.CombineFilters(
                    TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.Equal, propertyId),
                    TableOperators.And,
                    TableQuery.GenerateFilterCondition("RowKey", QueryComparisons.Equal, invoiceId)));

            InvoiceMappingEntity invoiceMappingEntity = SingleResult(query);
            return invoiceMappingEntity;
        }

        public InvoiceMappingEntity FindByPropertyAndInvoiceExternal(string propertyId, string invoiceExternalId)
        {
            TableQuery<InvoiceMappingEntity> query = new TableQuery<InvoiceMappingEntity>()
                .Where(TableQuery.CombineFilters(
                    TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.Equal, propertyId),
                    TableOperators.And,
                    TableQuery.GenerateFilterCondition("InvoiceExternalId", QueryComparisons.Equal, invoiceExternalId)));

            InvoiceMappingEntity invoiceMappingEntity = SingleResult(query);
            return invoiceMappingEntity;
        }



        private InvoiceMappingEntity SingleResult(TableQuery<InvoiceMappingEntity> query)
        {
            List<InvoiceMappingEntity> identifierMappingEntityList = ExecuteQuery(query);

            if (identifierMappingEntityList.Count == 0)
            {
                return null;
            }
            else
            {
                return identifierMappingEntityList[0];
            }
        }

        private List<InvoiceMappingEntity> ListResult(TableQuery<InvoiceMappingEntity> query)
        {
            return ExecuteQuery(query);
        }

        private List<InvoiceMappingEntity> ExecuteQuery(TableQuery<InvoiceMappingEntity> query)
        {
            List<InvoiceMappingEntity> identifierMappingEntityList = new List<InvoiceMappingEntity>();

            TableContinuationToken token = null;

            TableQuerySegment<InvoiceMappingEntity> resultSegment =
                    this.cloudTable.ExecuteQuerySegmented(query, token);
            token = resultSegment.ContinuationToken;


            foreach (InvoiceMappingEntity entity in resultSegment.Results)
            {
                identifierMappingEntityList.Add(entity);

            }


            //do
            //{
            //    TableQuerySegment<InvoiceMappingEntity> resultSegment =
            //        this.cloudTable.ExecuteQuerySegmented(query, token);
            //    token = resultSegment.ContinuationToken;


            //    foreach (InvoiceMappingEntity entity in resultSegment.Results)
            //    {
            //        identifierMappingEntityList.Add(entity);

            //    }
            //} while (token != null);

            return identifierMappingEntityList;
        }
    }
}
