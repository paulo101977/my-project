﻿using Microsoft.Azure;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace THEx.Integration.NFeIO.Storage.Table
{
    public class BaseTable
    {
        public CloudStorageAccount storageAccount;
        public CloudTable cloudTable;

        public void CreateConnection(string tableDescription)
        {
            var storageName = Environment.GetEnvironmentVariable("ThexFunctionStorage", EnvironmentVariableTarget.Process);

            storageAccount = CloudStorageAccount.Parse(storageName);

            CloudTableClient tableClient = storageAccount.CreateCloudTableClient();

            cloudTable = tableClient.GetTableReference(tableDescription);

            cloudTable.CreateIfNotExists();
        }

        public TableResult ExecuteOperation(TableOperation tableOperation)
        {
            TableResult tableResult = cloudTable.Execute(tableOperation);
            return tableResult;
        }

        public TableResult Delete(TableOperation tableOperation)
        {
            TableResult tableResult = cloudTable.Execute(tableOperation);
            return tableResult;
        }

    }
}
