﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace THEx.Integration.NFeIO.Storage.Queue
{
    public class ReturnStatusInvoiceQueue : BaseQueue
    {
        public string QueueDescription = Environment.GetEnvironmentVariable("QueueThexNfeioReturnStatusInvoice", EnvironmentVariableTarget.Process);

        public ReturnStatusInvoiceQueue()
        {
            CreateConnection(QueueDescription);
        }

    }
}
