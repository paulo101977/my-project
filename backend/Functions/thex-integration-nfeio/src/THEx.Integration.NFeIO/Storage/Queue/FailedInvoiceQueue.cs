﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace THEx.Integration.NFeIO.Storage.Queue
{
    public class FailedInvoiceQueue : BaseQueue
    {
        public const string QueueDescription = "thex-nfeio-failed-invoice";

        public FailedInvoiceQueue()
        {
            CreateConnection(QueueDescription);
        }

    }
}
