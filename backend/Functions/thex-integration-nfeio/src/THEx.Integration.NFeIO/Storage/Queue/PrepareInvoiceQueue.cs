﻿using Microsoft.Azure.WebJobs.Host;
using System;


namespace THEx.Integration.NFeIO.Storage.Queue
{
    public class PrepareInvoiceQueue : BaseQueue
    {
        public static string QueueDescription = Environment.GetEnvironmentVariable("QueueThexNfeioPrepareInvoice", EnvironmentVariableTarget.Process);

        public PrepareInvoiceQueue(TraceWriter log)
        {
            CreateConnection(QueueDescription, log);
        }

    }
}
