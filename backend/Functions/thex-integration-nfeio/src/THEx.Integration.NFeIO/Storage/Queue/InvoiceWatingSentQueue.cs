﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace THEx.Integration.NFeIO.Storage.Queue
{
    public class InvoiceWatingSentQueue : BaseQueue
    {
        public string QueueDescription = Environment.GetEnvironmentVariable("QueueThexNfeioInvoiceWaitingSent", EnvironmentVariableTarget.Process);

        public InvoiceWatingSentQueue()
        {
            CreateConnection(QueueDescription);
        }

    }
}
