﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace THEx.Integration.NFeIO.Storage.Queue
{
    public class IssuedInvoiceQueue : BaseQueue
    {
        public string QueueDescription = Environment.GetEnvironmentVariable("QueueThexNfeioIssuedInvoice", EnvironmentVariableTarget.Process);

        public IssuedInvoiceQueue()
        {
            CreateConnection(QueueDescription);
        }

    }
}
