﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace THEx.Integration.NFeIO.Storage.Queue
{
    public class CancellationIssuedInvoiceQueue : BaseQueue
    {
        public string QueueDescription = Environment.GetEnvironmentVariable("QueueThexNfeioCancellationIssuedInvoice", EnvironmentVariableTarget.Process);

        public CancellationIssuedInvoiceQueue()
        {
            CreateConnection(QueueDescription);
        }

    }
}
