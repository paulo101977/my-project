﻿using System;
using Microsoft.Azure.WebJobs.Host;
using Microsoft.Azure.ServiceBus;
using System.Text;
using System.Threading.Tasks;

namespace THEx.Integration.NFeIO.Storage.Queue
{
    public class BaseQueue
    {
        private QueueClient queueClient;

        public void CreateConnection(string queueName, TraceWriter log = null)
        {
            var serviceBusConnection = Environment.GetEnvironmentVariable("ServiceBusConnection", EnvironmentVariableTarget.Process);
            queueClient = new QueueClient(serviceBusConnection, queueName);
        }

        public async Task Insert(string json, TraceWriter log = null)
        {
            var message = new Message(Encoding.UTF8.GetBytes(json));
            await queueClient.SendAsync(message);
        }
    }
}
