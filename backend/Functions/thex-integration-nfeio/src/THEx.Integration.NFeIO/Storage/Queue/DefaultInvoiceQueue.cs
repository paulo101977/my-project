﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace THEx.Integration.NFeIO.Storage.Queue
{
    public class DefaultInvoiceQueue : BaseQueue
    {
        public const string QueueDescription = "thex-nfeio-default-invoice";

        public DefaultInvoiceQueue()
        {
            CreateConnection(QueueDescription);
        }

    }
}
