﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace THEx.Integration.NFeIO.Storage.Queue
{
    public class PrepareCancelInvoiceQueue : BaseQueue
    {
        public static string QueueDescription = Environment.GetEnvironmentVariable("QueueThexNfeioPrepareCancelInvoice", EnvironmentVariableTarget.Process);
    
        public PrepareCancelInvoiceQueue()
        {
            CreateConnection(QueueDescription);
        }

    }
}
