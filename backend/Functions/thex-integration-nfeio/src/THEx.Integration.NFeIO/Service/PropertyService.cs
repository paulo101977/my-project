﻿using Microsoft.WindowsAzure.Storage.Table;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using THEx.Integration.NFeIO.Commom;
using THEx.Integration.NFeIO.DTO;
using THEx.Integration.NFeIO.DTO.PMS;
using THEx.Integration.NFeIO.Entity;
using THEx.Integration.NFeIO.Storage.Table;

namespace THEx.Integration.NFeIO.Service
{
    class PropertyService
    {

        public string GetPropertyMappingOrCreateMapping(string propertyId, string document)
        {

            PropertyMappingTable propertyMappingTable = new PropertyMappingTable();

            PropertyMappingEntity propertyMappingEntity = propertyMappingTable.FindByProperty(propertyId);

            if (propertyMappingEntity == null)
            {
                RequestHttp requestHttp = new RequestHttp();
                ApiResultDto apiResultDto = requestHttp.SendGet(ApisNfeIo.UrlGetPropertyForDocument(document));

                if(apiResultDto.HttpStatusCode != (int)HttpStatusCode.OK)
                {
                    return null;
                }

                ResponseCompany responseCompany = JsonConvert.DeserializeObject<ResponseCompany>(apiResultDto.Message);
                propertyMappingEntity = new PropertyMappingEntity(propertyId, document, responseCompany.Companies.Id);
                propertyMappingTable.Insert(propertyMappingEntity);
            }

            return propertyMappingEntity.ExternalId;
        }

        public string GetPropertyMapping(string propertyId)
        {

            PropertyMappingTable propertyMappingTable = new PropertyMappingTable();

            PropertyMappingEntity propertyMappingEntity = propertyMappingTable.FindByProperty(propertyId);

            if (propertyMappingEntity == null)
            {
               return null;
            } else
            {
                return propertyMappingEntity.ExternalId;
            }      
        }

        public PropertyMappingEntity GetPropertyMappingByExternalId(string externalId)
        {
            PropertyMappingTable propertyMappingTable = new PropertyMappingTable();

            PropertyMappingEntity propertyMappingEntity = propertyMappingTable
                .FindByPropertyExternalId(externalId);

            return propertyMappingEntity;
        }
    }
}
