﻿using Microsoft.Azure.WebJobs.Host;
using Newtonsoft.Json;
using System;
using System.Net;
using THEx.Integration.NFeIO.Commom;
using THEx.Integration.NFeIO.DTO;
using THEx.Integration.NFeIO.DTO.PMS;

namespace THEx.Integration.NFeIO.Service
{
    public class InvoiceService
    {
        RequestHttp requestHttp = new RequestHttp();

        public ApiResultDto SendInvoice(string companyId, ServiceInvoice serviceInvoice)
        {
            var messageBody = JsonConvert.SerializeObject(serviceInvoice);

            ApiResultDto result = requestHttp.SendPost(ApisNfeIo.UrlPostServiceInvoice(companyId), messageBody);
            return result;
        }

        public ApiResultDto CancelInvoice(string propertyExternalId, string invoiceExternalId)
        {
            ApiResultDto result = requestHttp.SendDelete(ApisNfeIo.UrlDeleteInvoice(propertyExternalId, invoiceExternalId));
            return result;

        }

        public ApiResultDto SendInvoiceResponseTHEx(string sJson, TraceWriter log)
        {
            log.Info("SendInvoiceResponseTHEx == sJson == " + sJson);
            ResultDto resultDto = JsonConvert.DeserializeObject<ResultDto>(sJson);
            ApiResultDto result = null;

            log.Info("SendInvoiceResponseTHEx == sJson == resultDto.Message.TenantId.Value == " + resultDto.Message.TenantId.Value);

            if (sJson.Contains("LinkPdf") && !(Boolean)resultDto.Message.IsCancel)
            {
                result = requestHttp.SendPut(ApisTHEx.UrlPutInvoiceSuccess(resultDto.Message.PropertyId.Value), sJson, log, true, resultDto.Message.TenantId.Value);
            }
            else if (sJson.Contains("IsCancel") && (Boolean)resultDto.Message.IsCancel)
            {
                result = requestHttp.SendPut(ApisTHEx.UrlPutInvoiceCancel(resultDto.Message.PropertyId.Value), sJson, log, true, resultDto.Message.TenantId.Value);
            }
            else
            {
                result = requestHttp.SendPut(ApisTHEx.UrlPutInvoiceError(resultDto.Message.PropertyId.Value), sJson, log, true, resultDto.Message.TenantId.Value);
            }

            log.Info("SendInvoiceResponseTHEx == result.HttpStatusCode == " + result.HttpStatusCode);
            log.Info("SendInvoiceResponseTHEx == result.Message == " + result.Message);

            if ((result.HttpStatusCode != (int)HttpStatusCode.OK) && (result.HttpStatusCode != (int)HttpStatusCode.Created)
                && (result.HttpStatusCode != (int)HttpStatusCode.Accepted))
            {
                string messageError = String.Format("Erro ao enviar requisição para o THEx. ResponseStatusCode: '{0}'. Message: '{1}",
                    result.HttpStatusCode, result.Message);
                log.Info("Exception == " + messageError);
                throw new System.OperationCanceledException(messageError);
            }

            return result;
        }
    }
}
