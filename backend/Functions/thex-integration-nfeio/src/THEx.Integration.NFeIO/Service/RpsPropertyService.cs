﻿using Microsoft.Azure.WebJobs.Host;
using Microsoft.WindowsAzure.Storage.Table;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using THEx.Integration.NFeIO.Commom;
using THEx.Integration.NFeIO.DTO;
using THEx.Integration.NFeIO.Entity;
using THEx.Integration.NFeIO.Storage.Table;

namespace THEx.Integration.NFeIO.Service
{
    class RpsPropertyService
    {
   
        RpsPropertyTable rpsPropertyTable;
        InvoiceNfeioDto invoiceDto;

        public RpsPropertyService(InvoiceNfeioDto invoiceDto)
        {
            this.invoiceDto = invoiceDto;
            rpsPropertyTable = new RpsPropertyTable();
           
        }

        public RpsPropertyEntity FindControlRpsProperty(TraceWriter log)
        {
            RpsPropertyTable rpsPropertyTable = new RpsPropertyTable();

            List<RpsPropertyEntity> rpsPropertyEntities = rpsPropertyTable.FindByPropertyAndRpsSerie(
                invoiceDto.PropertyId, invoiceDto.ServiceInvoice.RpsSerialNumber);

            log.Info("RpsPropertyService == rpsPropertyEntities.Count == " + rpsPropertyEntities.Count);

            if (rpsPropertyEntities.Count == 0)
            {
                return CreateControlRpsProperty();
            }

            return rpsPropertyEntities.OrderBy(p => p.RpsNumber).FirstOrDefault();
        }

        public List<RpsPropertyEntity> FindControlRpsPropertyList()
        {
            RpsPropertyTable rpsPropertyTable = new RpsPropertyTable();

            List<RpsPropertyEntity> rpsPropertyEntities = rpsPropertyTable.FindByPropertyAndRpsSerie(
                invoiceDto.PropertyId, invoiceDto.ServiceInvoice.RpsSerialNumber);

            return rpsPropertyEntities;
        }

        public RpsPropertyEntity CreateControlRpsProperty()
        {
            RpsPropertyEntity rpsPropertyEntity = new RpsPropertyEntity(
                invoiceDto.PropertyId,
                invoiceDto.ServiceInvoice.RpsSerialNumber,
                invoiceDto.ServiceInvoice.RpsNumber,
                (int)Utils.StatusInvoice.New,
                invoiceDto.TenantId);

            TableResult result = rpsPropertyTable.Insert(rpsPropertyEntity);

            return rpsPropertyEntity;
        }

        public Boolean ValidateAvailabilityForProcessing(RpsPropertyEntity rpsPropertyEntity)
        {
            if (
                   (
                       (invoiceDto.ServiceInvoice.RpsNumber == rpsPropertyEntity.RpsNumber + 1
                          && rpsPropertyEntity.Status == (int)Utils.StatusInvoice.Processed) ||
                       (invoiceDto.ServiceInvoice.RpsNumber == rpsPropertyEntity.RpsNumber
                         && (rpsPropertyEntity.Status == (int)Utils.StatusInvoice.New ||
                            rpsPropertyEntity.Status == (int)Utils.StatusInvoice.Erro))
                    ))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void UpdateControlRpsProperty(int statusInvoice)
        {
            List<RpsPropertyEntity> rpsPropertyEntityList = rpsPropertyTable.FindByPropertyAndRpsSerie(
                invoiceDto.PropertyId, invoiceDto.ServiceInvoice.RpsSerialNumber);

            //Só posso ter uma property e série no rpsPropertyEntity, o RowKey é composto dessas duas variáveis
            var rpsPropertyEntity = rpsPropertyEntityList.FirstOrDefault();

            rpsPropertyEntity.RpsNumber = invoiceDto.ServiceInvoice.RpsNumber;
            rpsPropertyEntity.Status = statusInvoice;
            rpsPropertyEntity.TransactionId = invoiceDto.ServiceInvoice.Id;

            rpsPropertyTable.Update(rpsPropertyEntity);
        }
    }
}
