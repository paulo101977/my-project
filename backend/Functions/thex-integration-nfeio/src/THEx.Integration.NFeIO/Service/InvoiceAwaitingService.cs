﻿using Microsoft.Azure.WebJobs.Host;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using THEx.Integration.NFeIO.DTO;
using THEx.Integration.NFeIO.Entity;
using THEx.Integration.NFeIO.Storage.Queue;
using THEx.Integration.NFeIO.Storage.Table;

namespace THEx.Integration.NFeIO.Service
{
    class InvoiceAwaitingService
    {
        InvoiceAwaitingTable invoiceAwaitingTable;
        RpsPropertyService rpsPropertyService;

        public InvoiceAwaitingService()
        {
            invoiceAwaitingTable = new InvoiceAwaitingTable();
            rpsPropertyService = new RpsPropertyService(null);
        }

        public void SaveInvoice(InvoiceNfeioDto invoiceDto)
        {
            string invoiceJson = JsonConvert.SerializeObject(invoiceDto);
            InvoiceAwaitingEntity invoiceAwaitingEntity = new InvoiceAwaitingEntity(
                invoiceDto.PropertyId, invoiceDto.ServiceInvoice.RpsSerialNumber,
                invoiceDto.ServiceInvoice.RpsNumber, invoiceJson);

            invoiceAwaitingTable.Insert(invoiceAwaitingEntity);
        }

        public InvoiceAwaitingEntity GetEntityByPropertyAndRps(string propertyId, int rpsNumber, string serialNumber)
        {
            InvoiceAwaitingEntity invoiceAwaitingEntity = invoiceAwaitingTable.FindByPropertyAndRps(
                propertyId, serialNumber, rpsNumber);

            return invoiceAwaitingEntity;
        }

        public async Task SearchAndSendSuccessorInvoiceAsync(InvoiceNfeioDto invoiceDto, TraceWriter log)
        {
            log.Info("InvoiceAwaitingService == SearchAndSendSuccessorInvoice == " + invoiceDto.ServiceInvoice.RpsSerialNumber);

            var invoiceAwaitingEntity =
            invoiceAwaitingTable.FindByPropertyAndRps(
                invoiceDto.PropertyId, invoiceDto.ServiceInvoice.RpsSerialNumber,
                    invoiceDto.ServiceInvoice.RpsNumber + 1);

            log.Info("InvoiceAwaitingService == SearchAndSendSuccessorInvoice == " + (invoiceAwaitingEntity != null));

            if (invoiceAwaitingEntity == null)
            {
                return;
            }

            log.Info("InvoiceAwaitingService == SearchAndSendSuccessorInvoice == json == " + invoiceAwaitingEntity.SJson);

            invoiceAwaitingTable.Delete(invoiceAwaitingEntity);

            log.Info("InvoiceAwaitingService == Antes da fila de PrepareInvoiceQueue");
            await new PrepareInvoiceQueue(log).Insert(invoiceAwaitingEntity.SJson);
            log.Info("InvoiceAwaitingService == Depois da fila de PrepareInvoiceQueue");
        }
    }
}
