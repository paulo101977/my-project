﻿using Microsoft.Azure.WebJobs.Host;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using THEx.Integration.NFeIO.Commom;
using THEx.Integration.NFeIO.DTO;
using THEx.Integration.NFeIO.DTO.PMS;
using THEx.Integration.NFeIO.Entity;
using THEx.Integration.NFeIO.Storage.Queue;

namespace THEx.Integration.NFeIO.Service
{
    public class ErrorValidationService
    {

        List<ErrorDto> errors = new List<ErrorDto>();
        string PropertyId = null;

        public ErrorValidationService (string propertyId)
        {
            PropertyId = propertyId;
        }


        
        public void SendError(string typeService)
        {
            string sJson = JsonConvert.SerializeObject(GetResultErrors(typeService));
            new ReturnStatusInvoiceQueue().Insert(sJson);
        }

        public ResultDto GetResultErrors(string typeService)
        {
            ResultDto resultDto = new ResultDto()
            {
                Service = typeService
            };
            resultDto.Message = errors[0];
            return resultDto;
        }

        public void CreateError(string error, string auxiliaryMessage, string complement, string id, string tenantId)
        {
            ErrorDto errorDto = JsonConvert.DeserializeObject<ErrorDto>(error);
            if (auxiliaryMessage != null) errorDto.Message = string.Format(errorDto.Message, auxiliaryMessage);
            errorDto.Detail = complement;
            errorDto.InvoiceId = id;
            errorDto.PropertyId = PropertyId;
            errorDto.TenantId = tenantId;
            errors.Add(errorDto);
        }

        public Boolean InvalidRequest(InvoiceLotDto invoiceLotDto)
        {
            if(invoiceLotDto == null)
            {
                CreateError(CodeMessages.Error002001, null, null, null, invoiceLotDto.TenantId);
                return true;
            }
            
            return false;
        }

        public Boolean IsThereAnErro()
        {
            if(errors.Count == 0)
            {
                return false;
            } else
            {
                return true;
            }
        }

        public void DefaultInformationValidation(InvoiceLotDto invoiceLotDto)
        {
            if(invoiceLotDto.Borrower == null)
            {
                CreateError(CodeMessages.Error002002, "Borrower", null, null, invoiceLotDto.TenantId);
            }

            if(String.IsNullOrEmpty(invoiceLotDto.Document))
            {
                CreateError(CodeMessages.Error002002, "Document", null, null, invoiceLotDto.TenantId);
            }

            if (String.IsNullOrEmpty(invoiceLotDto.PropertyId))
            {
                CreateError(CodeMessages.Error002002, "PropertyId", null, null, invoiceLotDto.TenantId);
            }

        }

        public void DefaultInformationValidation(InvoiceNfeioDto invoiceDto)
        {
            if (String.IsNullOrEmpty(invoiceDto.PropertyId))
            {
                CreateError(CodeMessages.Error002002, "PropertyId", null, null, invoiceDto.TenantId);
            }
            if (String.IsNullOrEmpty(invoiceDto.InvoiceId))
            {
                CreateError(CodeMessages.Error002002, "InvoiceId", null, invoiceDto.InvoiceId, invoiceDto.TenantId);
            }
            if (String.IsNullOrEmpty(invoiceDto.TenantId))
            {
                CreateError(CodeMessages.Error002002, "TenantId", null, invoiceDto.TenantId, invoiceDto.TenantId);
            }
        }

        public Boolean InvoiceValidation(InvoicePmsDto invoicePmsDto, string tenantId)
        {
            Boolean validated = true;

            if (String.IsNullOrEmpty(invoicePmsDto.Id))
            {
                CreateError(CodeMessages.Error002002, "InvoiceId", null, invoicePmsDto.Id, tenantId);
                validated = false;
            }

            if (String.IsNullOrEmpty(invoicePmsDto.CityServiceCode))
            {
                CreateError(CodeMessages.Error002002, 
                    "CityServiceCode - (InvoiceId:" + invoicePmsDto.Id + ")", null, invoicePmsDto.Id, tenantId);
                validated = false;
            }

            if (String.IsNullOrEmpty(invoicePmsDto.RpsSerialNumber))
            {
                CreateError(CodeMessages.Error002002,
                    "RpsSerialNumber - (InvoiceId:" + invoicePmsDto.Id + ")", null, invoicePmsDto.Id, tenantId);
                validated = false;
            }

            if (invoicePmsDto.RpsNumber == 0)
            {
                CreateError(CodeMessages.Error002002,
                    "RpsNumber - (InvoiceId:" + invoicePmsDto.Id + ")", null, invoicePmsDto.Id, tenantId);
                validated = false;
            }

            return validated;

        }

        public Boolean InvoiceValidationCalculated(InvoicePmsDto invoicePmsDto, string tenantId)
        {
            Boolean validated = true;
            validated = InvoiceValidation(invoicePmsDto, tenantId);

            if (invoicePmsDto.IssRatePercentage == 0)
            {
                CreateError(CodeMessages.Error002002,
                    "IssRatePercentage - (InvoiceId:" + invoicePmsDto.Id + ")", null, invoicePmsDto.Id, tenantId);
                validated = false;
            }

            if (invoicePmsDto.IssTaxAmount == 0)
            {
                CreateError(CodeMessages.Error002002,
                    "IssTaxAmount - (InvoiceId:" + invoicePmsDto.Id + ")", null, invoicePmsDto.Id, tenantId);
                validated = false;
            }
            return validated;
        }

        public Boolean InvoiceWasResent(InvoiceNfeioDto invoiceNfeioDto, TraceWriter log)
        {
            InvoiceAwaitingService invoiceAwaitingService = new InvoiceAwaitingService();

            InvoiceAwaitingEntity invoiceAwaitingEntity = 
                invoiceAwaitingService.GetEntityByPropertyAndRps(
                    invoiceNfeioDto.PropertyId, invoiceNfeioDto.ServiceInvoice.RpsNumber, 
                    invoiceNfeioDto.ServiceInvoice.RpsSerialNumber);

            if(invoiceAwaitingEntity != null)
            {
                CreateError(CodeMessages.Error002004, null, invoiceAwaitingEntity.SJson, invoiceNfeioDto.InvoiceId, invoiceNfeioDto.TenantId);
                return true;
            }

            RpsPropertyService rpsPropertyService = new RpsPropertyService(invoiceNfeioDto);
            RpsPropertyEntity rpsPropertyEntity = rpsPropertyService.FindControlRpsProperty(log);

            if(rpsPropertyEntity != null &&
                rpsPropertyEntity.RpsNumber == invoiceNfeioDto.ServiceInvoice.RpsNumber &&
                rpsPropertyEntity.Status == (int)Utils.StatusInvoice.Processing)
            {
                CreateError(CodeMessages.Error002005, null, JsonConvert.SerializeObject(invoiceNfeioDto), invoiceNfeioDto.InvoiceId, invoiceNfeioDto.TenantId);
                return true;
            } else if (rpsPropertyEntity != null && 
                (
                    (rpsPropertyEntity.RpsNumber == invoiceNfeioDto.ServiceInvoice.RpsNumber && 
                    rpsPropertyEntity.Status == (int)Utils.StatusInvoice.Processed) ||
                rpsPropertyEntity.RpsNumber > invoiceNfeioDto.ServiceInvoice.RpsNumber))
            {
                CreateError(CodeMessages.Error002006, null, JsonConvert.SerializeObject(invoiceNfeioDto), invoiceNfeioDto.InvoiceId, invoiceNfeioDto.TenantId);
                return true;
            } 


            return false;
        }
    }
}
