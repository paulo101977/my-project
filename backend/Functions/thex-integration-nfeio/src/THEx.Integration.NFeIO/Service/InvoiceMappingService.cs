﻿using Microsoft.Azure.WebJobs.Host;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using THEx.Integration.NFeIO.DTO;
using THEx.Integration.NFeIO.Entity;
using THEx.Integration.NFeIO.Storage.Table;

namespace THEx.Integration.NFeIO.Service
{
    public class InvoiceMappingService
    {
        InvoiceMappingTable invoiceMappingTable;

        public InvoiceMappingService()
        {
            invoiceMappingTable = new InvoiceMappingTable();
        }

        public void CreateMapping(InvoiceNfeioDto invoiceNfeioDto)
        {
            InvoiceMappingEntity invoiceMappingEntity = invoiceMappingTable.FindByPropertyAndInvoice(
                invoiceNfeioDto.PropertyId, invoiceNfeioDto.InvoiceId);

            if(invoiceMappingEntity == null)
            {
                invoiceMappingEntity = new InvoiceMappingEntity(
                    invoiceNfeioDto.PropertyId, invoiceNfeioDto.InvoiceId, 
                    invoiceNfeioDto.ExternalPropertyId, invoiceNfeioDto.ServiceInvoice.Id, invoiceNfeioDto.TenantId);
                invoiceMappingTable.Insert(invoiceMappingEntity);
            } else
            {
                invoiceMappingTable.Update(invoiceMappingEntity);
            }
        }

        public InvoiceMappingEntity GetMappingByPropertyAndInvoice(string propertyId, string invoiceId)
        {
            return invoiceMappingTable.FindByPropertyAndInvoice(propertyId, invoiceId);
        }

        public InvoiceMappingEntity GetByPropertyAndInvoiceExternalId(string propertyId, string invoiceExternalId, TraceWriter log)
        {
            InvoiceMappingEntity invoiceMappingEntity = null;
            invoiceMappingEntity = invoiceMappingTable.FindByPropertyAndInvoiceExternal(propertyId, invoiceExternalId);

            //do
            //{
            //    invoiceMappingEntity = invoiceMappingTable.FindByPropertyAndInvoiceExternal(propertyId, invoiceExternalId);

            //    if (invoiceMappingEntity == null)
            //    {
            //        log.Info(Convert.ToString(++i));
            //        Thread.Sleep(1000);
            //    }
            //} while (invoiceMappingEntity == null);
            
            return invoiceMappingEntity;
        }
    }
}
