﻿using Microsoft.WindowsAzure.Storage.Table;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace THEx.Integration.NFeIO.Entity
{
    class InvoiceAwaitingEntity : TableEntity
    {
        public int RpsNumber { get; set; }
        public string RpsSerieNumber { get; set; }
        public String PropertyId { get; set; }
        public string SJson { get; set; }

        public InvoiceAwaitingEntity() { }

        public InvoiceAwaitingEntity(string propertyId, string rpsSerieNumber,
            int rpsNumber, string sJson)
        {
            this.PartitionKey = propertyId;
            this.RowKey = rpsSerieNumber + Convert.ToString(rpsNumber);

            RpsNumber = rpsNumber;
            RpsSerieNumber = rpsSerieNumber;
            PropertyId = propertyId;
            SJson = sJson;
        }

    }
}
