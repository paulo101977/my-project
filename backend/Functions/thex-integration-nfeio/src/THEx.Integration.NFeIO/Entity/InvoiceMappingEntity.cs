﻿using Microsoft.WindowsAzure.Storage.Table;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace THEx.Integration.NFeIO.Entity
{
    public class InvoiceMappingEntity : TableEntity
    {
        public String PropertyId { get; set; }

        public String PropertyExternalId { get; set; }

        public String InvoiceId {get;set;}

        public String InvoiceExternalId { get; set; }

        public String TenantId { get; set; }

        public InvoiceMappingEntity() { }

        public InvoiceMappingEntity(string propertyId, string invoiceId, string propertyExternalId, string invoiceExternalId, string tenantId)
        {
            this.PartitionKey = propertyId;
            this.RowKey = invoiceId;

            PropertyId = propertyId;
            InvoiceId = invoiceId;
            PropertyExternalId = propertyExternalId;
            InvoiceExternalId = invoiceExternalId;
            TenantId = tenantId;
        }

    }
}
