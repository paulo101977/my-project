﻿using Microsoft.WindowsAzure.Storage.Table;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace THEx.Integration.NFeIO.Entity
{
    public class PropertyMappingEntity : TableEntity
    {
        public String PropertyId { get; set; }
        public String Document { get; set; }
        public String ExternalId { get; set; }

        public PropertyMappingEntity() { }

        public PropertyMappingEntity(string propertyId, string document, string externalId)
        {
            this.PartitionKey = propertyId;
            this.RowKey = externalId;

            PropertyId = propertyId;
            Document = document;
            ExternalId = externalId;
            
        }

    }
}
