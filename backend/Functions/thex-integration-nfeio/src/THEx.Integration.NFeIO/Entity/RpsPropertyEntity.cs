﻿using Microsoft.WindowsAzure.Storage.Table;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace THEx.Integration.NFeIO.Entity
{
    public class RpsPropertyEntity : TableEntity
    {
        public int RpsNumber { get; set; }
        public string RpsSerieNumber { get; set; }
        public String PropertyId { get; set; }
        public int Status { get; set; }
        public string TenantId { get; set; }
        public string TransactionId { get; set; }

        public RpsPropertyEntity() { }

        public RpsPropertyEntity(string propertyId, string rpsSerieNumber, int rpsNumber, int status, string tenantId)
        {
            this.PartitionKey = propertyId;
            this.RowKey = propertyId + rpsSerieNumber;

            RpsNumber = rpsNumber;
            RpsSerieNumber = rpsSerieNumber;
            PropertyId = propertyId;
            Status = status;
            TenantId = tenantId;
        }

    }
}
