﻿using System.Collections.Generic;
using THEx.Integration.NFeIO.DTO.PMS;

namespace THEx.Integration.NFeIO.DTO
{
    public class InvoiceLotDto
    {
        public string Document { get; set; }
        public string PropertyId { get; set; }
        public string ExternalPropertyId { get; set; }
        public Borrower Borrower { get; set; }
        public string TenantId { get; set; }
        public List<InvoicePmsDto> Invoices { get; set; }
    }
}
