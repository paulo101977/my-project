﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace THEx.Integration.NFeIO.DTO.PMS
{
    public class ApiResultDto
    {
        public int HttpStatusCode { get; set; }
        public string Message { get; set; }
        public string Method { get; set; }
        public string Url { get; set; }
        public string Body { get; set; }
    }
}
