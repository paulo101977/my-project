﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace THEx.Integration.NFeIO.DTO.PMS
{
    public class ErrorDto
    {
        public string Code { get; set; }
        public string InvoiceId { get; set; }
        public string PropertyId { get; set; }
        public string Message { get; set; }
        public string Detail { get; set; }
        public string TenantId { get; set; }
    }
}
