﻿using System;

namespace THEx.Integration.NFeIO.DTO.PMS
{
    public class UserTokenDto
    {
        public Guid Id { get; set; }
        public bool Authenticated { get; set; }
        public string Created { get; set; }
        public string Expiration { get; set; }
        public string NewPasswordToken { get; set; }
    }
}
