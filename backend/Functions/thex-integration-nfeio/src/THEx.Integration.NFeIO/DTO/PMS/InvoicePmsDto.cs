﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace THEx.Integration.NFeIO.DTO.PMS
{
    public class InvoicePmsDto
    {
        public string Id { get; set; }
        public string CityServiceCode { get; set; }
        public string Description { get; set; }
        public double InvoiceAmount { get; set; }
        public double DeductionsAmount { get; set; }
        public int RpsNumber { get; set; }
        public string RpsSerialNumber { get; set; }
        public double IssRatePercentage { get; set; }
        public double IssTaxAmount { get; set; }
        public Boolean IsIssWithheld { get; set; }

    }
}
