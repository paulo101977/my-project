﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace THEx.Integration.NFeIO.DTO
{
    class InvoiceResultDto
    {
        public string PropertyId { get; set; }
        public string InvoiceId { get; set; }
        public string LinkPdf { get; set; }
        public string LinkXml { get; set; }
        public string Number { get; set; }
        public int RpsNumber { get; set; }
        public string RpsSerialNumber { get; set; }
        public string TenantId { get; set; }
        public bool IsCancel { get; set; }
    }
}
