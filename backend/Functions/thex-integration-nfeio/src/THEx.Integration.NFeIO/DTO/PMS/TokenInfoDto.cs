﻿namespace THEx.Integration.NFeIO.DTO.PMS
{
    public class TokenInfoDto
    {
        public string TokenClient { get; set; }
        public string TokenApplication { get; set; }
        public string TenantId { get; set; }
    }
}
