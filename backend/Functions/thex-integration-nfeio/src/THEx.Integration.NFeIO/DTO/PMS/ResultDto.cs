﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace THEx.Integration.NFeIO.DTO.PMS
{
    public class ResultDto
    {
        
        public string Service { get; set; }
        public dynamic Message { get; set; }
    }
}
