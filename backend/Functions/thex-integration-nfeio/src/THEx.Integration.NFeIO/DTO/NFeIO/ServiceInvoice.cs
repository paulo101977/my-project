﻿using System;

namespace THEx.Integration.NFeIO.DTO
{
    [Serializable]
    public class ServiceInvoice
    {
        public string Id { get; set; }
        public string Environment { get; set; }
        public string FlowStatus { get; set; }
        public string FlowMessage { get; set; }
        public Provider Provider { get; set; }
        public Borrower Borrower { get; set; }
        public string BatchNumber { get; set; }
        public string BatchCheckNumber { get; set; }
        public string Number { get; set; }
        public string CheckCode { get; set; }
        public string Status { get; set; }
        public string RpsType { get; set; }
        public string RpsStatus { get; set; }
        public string TaxationType { get; set; }
        public DateTime? IssuedOn { get; set; }
        public DateTime? CancelledOn { get; set; }
        public string RpsSerialNumber { get; set; }
        public int RpsNumber { get; set; }
        public string CityServiceCode { get; set; }
        public string FederalServiceCode { get; set; }
        public string Description { get; set; }
        public Double ServicesAmount { get; set; }
        public Double DeductionsAmount { get; set; }
        public Double? DiscountUnconditionedAmount { get; set; }
        public Double? DiscountConditionedAmount { get; set; }
        public Double? BaseTaxAmount { get; set; }
        public Double? IssRate { get; set; }
        public Double? IssTaxAmount { get; set; }
        public Double? IrAmountWithheld { get; set; }
        public Double? PisAmountWithheld { get; set; }
        public Double? CofinsAmountWithheld { get; set; }
        public Double? CsllAmountWithheld { get; set; }
        public Double? InssAmountWithheld { get; set; }
        public Double? IssAmountWithheld { get; set; }
        public Double? OthersAmountWithheld { get; set; }
        public Double? AmountWithheld { get; set; }
        public Double? AmountNet { get; set; }
        public ApproximateTax ApproximateTax { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? ModifiedOn { get; set; }
    }
}