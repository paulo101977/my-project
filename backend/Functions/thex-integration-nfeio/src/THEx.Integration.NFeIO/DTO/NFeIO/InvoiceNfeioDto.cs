﻿namespace THEx.Integration.NFeIO.DTO
{
    public class InvoiceNfeioDto
    {
        public string Document { get; set; }
        public string PropertyId { get; set; }
        public string ExternalPropertyId { get; set; }
        public string InvoiceId { get; set; }
        public string TenantId { get; set; }
        public ServiceInvoice ServiceInvoice { get; set; }
    }
}
