﻿using System;
using System.Collections.Generic;

namespace THEx.Integration.NFeIO.DTO
{
    [Serializable]
    public class NaturalPeopleList
    {
        public List<NaturalPeople> naturalPeople { get; set; }
    }
}