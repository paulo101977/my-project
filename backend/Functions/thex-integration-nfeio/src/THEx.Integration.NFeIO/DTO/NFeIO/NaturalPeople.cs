﻿using System;

namespace THEx.Integration.NFeIO.DTO
{


    [Serializable]
    public class NaturalPeople
    {
        public string id { get; set; }
        public string name { get; set; }
        public int federalTaxNumber { get; set; }
        public string email { get; set; }
        public Address address { get; set; }
        public DateTime birthDate { get; set; }
        public string idNumber { get; set; }
        public string status { get; set; }
        public DateTime createdOn { get; set; }
        public DateTime modifiedOn { get; set; }
    }
}
