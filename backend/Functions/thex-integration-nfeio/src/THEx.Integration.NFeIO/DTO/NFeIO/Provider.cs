﻿using System;
using System.Collections.Generic;

namespace THEx.Integration.NFeIO.DTO
{
   
    [Serializable]
    public class Provider
    {
        public string tradeName { get; set; }
        public DateTime? openningDate { get; set; }
        public string taxRegime { get; set; }
        public string specialTaxRegime { get; set; }
        public string legalNature { get; set; }
        public List<EconomicActivity> economicActivities { get; set; }
        public string companyRegistryNumber { get; set; }
        public string regionalTaxNumber { get; set; }
        public string municipalTaxNumber { get; set; }
        public Double issRate { get; set; }
        public string parentId { get; set; }
        public string id { get; set; }
        public string name { get; set; }
        public string federalTaxNumber { get; set; }
        public string email { get; set; }
        public Address address { get; set; }
        public string status { get; set; }
        public string type { get; set; }
        public DateTime? createdOn { get; set; }
        public DateTime? modifiedOn { get; set; }
    }
}
