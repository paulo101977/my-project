﻿namespace THEx.Integration.NFeIO.DTO
{
    public class Address
    {
        public string Country { get; set; }
        public string PostalCode { get; set; }
        public string Street { get; set; }
        public string Number { get; set; }
        public string AdditionalInformation { get; set; }
        public string District { get; set; }
        public City City { get; set; }
        public string State { get; set; }
    }
}
