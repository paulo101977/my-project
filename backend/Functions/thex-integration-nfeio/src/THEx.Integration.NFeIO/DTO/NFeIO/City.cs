﻿namespace THEx.Integration.NFeIO.DTO
{
    public class City
    {
        public string Code { get; set; }
        public string Name { get; set; }
    }
}
