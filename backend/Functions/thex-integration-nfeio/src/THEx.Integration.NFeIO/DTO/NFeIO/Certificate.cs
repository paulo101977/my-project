﻿namespace THEx.Integration.NFeIO.DTO
{
    public class Certificate
    {
        public string Thumbprint { get; set; }
        public string ModifiedOn { get; set; }
        public string ExpiresOn { get; set; }
        public string Status { get; set; }

    }
}
