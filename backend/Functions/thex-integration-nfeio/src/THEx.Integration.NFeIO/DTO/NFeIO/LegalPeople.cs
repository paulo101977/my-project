﻿using System;
using System.Collections.Generic;

namespace THEx.Integration.NFeIO.DTO
{

    [Serializable]
    public class LegalPeople
    {
        public string id { get; set; }
        public string name { get; set; }
        public string tradeName { get; set; }
        public int federalTaxNumber { get; set; }
        public string email { get; set; }
        public Address address { get; set; }
        public DateTime openningDate { get; set; }
        public string taxRegime { get; set; }
        public string legalNature { get; set; }
        public List<EconomicActivity> economicActivities { get; set; }
        public int companyRegistryNumber { get; set; }
        public int regionalTaxNumber { get; set; }
        public string municipalTaxNumber { get; set; }
        public string status { get; set; }
        public DateTime createdOn { get; set; }
        public DateTime modifiedOn { get; set; }
    }

}
