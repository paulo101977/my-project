﻿using System;
using System.Collections.Generic;

namespace THEx.Integration.NFeIO.DTO
{
    [Serializable]
    public class ServiceInvoices
    {
        public int totalResults { get; set; }
        public int totalPages { get; set; }
        public int page { get; set; }
        public List<ServiceInvoice> serviceInvoices { get; set; }
    }
}