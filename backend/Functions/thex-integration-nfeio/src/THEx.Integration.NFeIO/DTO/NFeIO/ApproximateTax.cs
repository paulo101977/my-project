﻿using System;

namespace THEx.Integration.NFeIO.DTO
{
    [Serializable]
    public class ApproximateTax
    {
        public string source { get; set; }
        public string version { get; set; }
        public Double totalRate { get; set; }
    }
}