﻿using System;
using System.Collections.Generic;

namespace THEx.Integration.NFeIO.DTO
{

    [Serializable]
    public class Companies
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string TradeName { get; set; }
        public Int64 FederalTaxNumber { get; set; }
        public string Email { get; set; }
        public Address Address { get; set; }
        public DateTime OpenningDate { get; set; }
        public string TaxRegime { get; set; }
        public string SpecialTaxRegime { get; set; }
        public string LegalNature { get; set; }
        public List<EconomicActivity> EconomicActivities { get; set; }
        public int CompanyRegistryNumber { get; set; }
        public int RegionalTaxNumber { get; set; }
        public string MunicipalTaxNumber { get; set; }
        public string RpsSerialNumber { get; set; }
        public int RpsNumber { get; set; }
        public string Environment { get; set; }
        public string FiscalStatus { get; set; }
        public Certificate Certificate { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime ModifiedOn { get; set; }
    }
}
