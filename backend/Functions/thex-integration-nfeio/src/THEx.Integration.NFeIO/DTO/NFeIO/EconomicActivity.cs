﻿namespace THEx.Integration.NFeIO.DTO
{
    public class EconomicActivity
    {
        public string type { get; set; }
        public int code { get; set; }
    }
}
