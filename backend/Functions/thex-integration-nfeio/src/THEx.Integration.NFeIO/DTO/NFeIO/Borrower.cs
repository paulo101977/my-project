﻿using System;

namespace THEx.Integration.NFeIO.DTO
{
    [Serializable]
    public class Borrower
    {
        public string parentId { get; set; }
        public string id { get; set; }
        public string name { get; set; }
        public string federalTaxNumber { get; set; }
        public string email { get; set; }
        public Address address { get; set; }
        public string status { get; set; }
        public string type { get; set; }
        public DateTime? createdOn { get; set; }
        public DateTime? modifiedOn { get; set; }
    }
}