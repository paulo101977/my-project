﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;
using Thex.Kernel;

namespace Thex.Integration.Function
{
    public class ApplicationUserFunction : ApplicationUser, IApplicationUser
    {
        private readonly IHttpContextAccessor _accessor;

        public ApplicationUserFunction(IHttpContextAccessor accessor) : base(accessor)
        {
            _accessor = accessor;

        }

        public override Dictionary<string, string> GetClaimsIdentity()
        {
            return this._claims ?? new Dictionary<string, string>();
        }

        public void SetProperties(string userUid, string tenantId, string propertyId, string timeZoneName)
        {
            this._claims = new Dictionary<string, string>();
            this._claims.Add("LoggedUserUid", userUid);
            this._claims.Add("TenantId", tenantId);
            this._claims.Add("PropertyId", propertyId);
            this._claims.Add("TimeZoneName", timeZoneName);
        }
    }
}
