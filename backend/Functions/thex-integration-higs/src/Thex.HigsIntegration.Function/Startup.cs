﻿using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using Thex.Common;
using Thex.HigsIntegration.Function.Service.Application;
using Thex.HigsIntegration.Function.Service.Infra.Context;
using Thex.Integration.Function;
using Thex.Inventory;
using Thex.Kernel;
using Thex.Log;
using Willezone.Azure.WebJobs.Extensions.DependencyInjection;

[assembly: WebJobsStartup(typeof(Startup))]
namespace Thex.Integration.Function
{
    internal class Startup : IWebJobsStartup
    {
        public void Configure(IWebJobsBuilder builder)
        {
            builder.AddDependencyInjection<ServiceProviderBuilder>();

        }
    }

    internal class ServiceProviderBuilder : IServiceProviderBuilder
    {
        private readonly ILoggerFactory _loggerFactory;

        public ServiceProviderBuilder(ILoggerFactory loggerFactory)
        {
            _loggerFactory = loggerFactory;
        }

        public IServiceProvider Build()
        {
            var services = new ServiceCollection();

            services
               .AddTnfEntityFrameworkCore()

            .AddTnfKernel()
            .AddTnfRuntime()
            .AddTnfNotifications()

               .AddTnfDbContext<ThexHigsIntegrationFunctionContext>(options =>
               {
                   options.DbContextOptions.UseSqlServer(Environment.GetEnvironmentVariable("ThexConnectionString"));
               });

            services
                .AddHttpContextAccessor()
                .AddInventoryDependencyFunction(Environment.GetEnvironmentVariable("ThexConnectionString"))
                .AddLogApplicationServiceDependency(Environment.GetEnvironmentVariable("MongoDBServer"), Environment.GetEnvironmentVariable("MongoDBName"))
                .AddApplicationDependency()
                .AddThexGenericLog();

            services.AddScoped<IApplicationUser, ApplicationUserFunction>();

            var servicesConfiguration = new ServicesConfiguration()
            {
                ElasticSearch = new ElasticSearch()
                {
                    Accesskey = Environment.GetEnvironmentVariable("GenericLogAccesskey"),
                    Secretkey = Environment.GetEnvironmentVariable("GenericLogSecretkey"),
                    Uri = Environment.GetEnvironmentVariable("GenericLogUri"),
                }
            };

            services.AddSingleton(servicesConfiguration);

            var serviceProvider = services.BuildServiceProvider();

            ServiceLocator.SetLocatorProvider(serviceProvider);

            return serviceProvider;
        }
    }
}
