using System;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Host;
using Newtonsoft.Json;
using Serilog;
using Thex.GenericLog;
using Thex.HigsIntegration.Function.Service.Application.Interfaces;
using Thex.Kernel;
using Willezone.Azure.WebJobs.Extensions.DependencyInjection;

namespace Thex.Integration.Function
{
    public static class QueueTriggerRatePlanList
    {
        private static IGenericLogHandler _genericLog;
        private static TraceWriter _logWriter;

        [FunctionName("QueueTriggerRatePlanList")]
        public async static Task Run(
            [QueueTrigger("%QueueRatePlanList%", Connection = "AzureWebJobsStorage")]string myQueueItem,
            [Inject]IRatePlanListAppService ratePlanListAppService,
            [Inject]IGenericLogHandler genericLog,
            [Inject]IApplicationUser applicationUser,
            TraceWriter log)
        {
            _genericLog = genericLog;
            _logWriter = log;

            try
            {
                applicationUser.SetProperties(Guid.Empty.ToString(), Guid.Empty.ToString(), "", "");
                log.Info($"C# Queue trigger function processed: {myQueueItem}");

                await ratePlanListAppService.ImportAsync(myQueueItem);
            }
            catch (Exception ex)
            {
                genericLog.AddLog(genericLog.DefaultBuilder
                 .WithMessage(ex.Message)
                 .WithDetailedMessage(JsonConvert.SerializeObject(myQueueItem))
                 .AsError()
                 .Build());
            }

            CommitLog();
        }

        private static void CommitLog()
        {
            foreach (var logError in _genericLog.GetAll().Where(g => g.GenericLogType == GenericLogType.Error))
            {
                _logWriter.Error(logError.Message);
            }

            if (!Debugger.IsAttached)
            {
                if (_genericLog.HasGenericLog())
                    _genericLog.Commit("rateplan-list-log");
            }
        }
    }
}
