using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Willezone.Azure.WebJobs.Extensions.DependencyInjection;
using Thex.HigsIntegration.Function.Service.Application.Interfaces;

namespace Thex.Integration.Function
{
    public class HttpHealth
    {
        [FunctionName("Health")]
        public async Task<IActionResult> Run(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", "post", Route = null)] HttpRequest req,
            [Inject]IHealthAppService _healthAppService,
            ILogger log)
        {
            return await _healthAppService.Check()
                ? (ActionResult)new OkResult()
                : new BadRequestResult();
        }
    }
}
