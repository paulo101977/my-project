﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Thex.HigsIntegration.Function.Service.Enumerations.RatePlanList
{
    public enum AgeQualifyingCodeEnum
    {
        Adult = 10,
        Child = 8,
        ChildUntil2Years = 6
    }
}
