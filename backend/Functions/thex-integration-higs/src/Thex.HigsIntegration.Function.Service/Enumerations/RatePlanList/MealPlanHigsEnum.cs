﻿namespace Thex.HigsIntegration.Dto.Enumerations.RatePlanList
{
    public enum MealPlanHigsEnum
    {
        Nenhum = 1,
        CafeDaManha = 2,
        Meia = 3,
        MeiaPensaoAlmoco = 4,
        MeiaPensaoJantar = 5,
        PensaoInteira = 6,
        TudoIncluso = 7
    }
}
