﻿namespace Thex.HigsIntegration.Dto.Enumerations.RatePlanList
{
    public enum TypeComissionHigsEnum
    {
        None = 0,
        Amount = 1,
        Percent = 2
    }
}
