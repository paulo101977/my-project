﻿namespace Thex.HigsIntegration.Function.Service.Dto
{
    public class GuestHigsDto
    {
        /// <summary>
        /// Name of accompanying guests.
        /// </summary>
        public string GivenName { get; set; }

        /// <summary>
        /// Last Name of accompanying guests.
        /// </summary>
        public string LastName { get; set; }
    }
}
