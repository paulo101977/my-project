﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Thex.HigsIntegration.Function.Service.Dto
{
    public class CreditCardDataHigsDto
    {
        /// <summary>
        /// Credit card type flag.
        /// </summary>        
        public string Flag { get; set; }

        /// <summary>
        /// Name of card holder.
        /// </summary>        
        public string Name { get; set; }

        /// <summary>
        /// Credit card number.
        /// </summary>        
        public string CardNumber { get; set; }

        /// <summary>
        /// Credit Card security code.
        /// </summary>        
        public string SecurityCode { get; set; }

        /// <summary>
        /// Data of the credit card expiration (mm/yy).
        /// </summary>        
        public string Expiration { get; set; }
    }
}
