﻿namespace Thex.HigsIntegration.Function.Service.Dto
{
    public partial class GetAllCountryDto
    {
        public string LanguageIsoCode { get; set; }
    }
}
