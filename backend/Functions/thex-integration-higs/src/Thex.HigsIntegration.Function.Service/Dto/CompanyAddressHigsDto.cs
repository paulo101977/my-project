﻿
namespace Thex.HigsIntegration.Function.Service.Dto
{
    public class CompanyAddressHigsDto
    {
        public string Address { get; set; }

        public string AddressNumber { get; set; }

        public string Complement { get; set; }

        public string CityName { get; set; }

        public string StateName { get; set; }

        public string CountryName { get; set; }
    }
}
