﻿namespace Thex.HigsIntegration.Function.Service.Dto
{
    public class ReservationIdsHigsDto
    {
        public string HIGSReservationNumber { get; set; }
        public string PMSReservationNumber { get; set; }
        public string SourceReservationNumber { get; set; }
    }
}
