﻿using System;
using Thex.Common.Dto;
using Thex.Common.Enumerations;
using Thex.HigsIntegration.Function.Service.Enumerations;

namespace Thex.HigsIntegration.Function.Service.Dto
{
    public class GuestInformationHigsDto
    {
        public string LocatorId { get; set; }
        public int HotelCode { get; set; }
        public GuestInfoHigsDto Guest { get; set; }
        public ReservationIdsHigsDto ReservationIds { get; set; }
        public string GivenName { get; set; }
        public string SurName { get; set; }
        public string Email { get; set; }
        public string Profession { get; set; }
        public string BirthDate { get; set; }
        public string Gender { get; set; }
        public DocumentTypeHigsEnum DocumentType { get; set; }
        public string DocumentNumber { get; set; }
        public GuestAddressHigsDto GuestAddress { get; set; }
        public string Nationality { get; set; }
        public string PhoneNumber { get; set; }
        public string CelPhoneNumber { get; set; }
        public string LastSource { get; set; }
        public string NextStop { get; set; }
        public TravelReasonHigsEnum TravelReason { get; set; }
        public TransportTypeHigsEnum Transport { get; set; }
        public string Checkin { get; set; }
        public string Checkout { get; set; }
        public string NationalityCode { get; set; }

        public string GetFullName()
            => !string.IsNullOrEmpty(SurName) ? $"{GivenName} {SurName}" : $"{GivenName}";

        public DateTime? GetBirthDate()
        {
            DateTime dateResult;

            if (string.IsNullOrEmpty(BirthDate) || !DateTime.TryParse(BirthDate, out dateResult))
                return null;
                        
            return dateResult;
        }

        public string GetGender()
        {
            if (string.IsNullOrEmpty(Gender))
                return null;

            return Gender.ToLower() == "male" ? "M" : "F";
        }


        public string GetHigsCode()
        {
            if (string.IsNullOrEmpty(LocatorId))
                return null;

            return $"{LocatorId}";
        }

        public LocationDto GetLocation()
        {
            if (GuestAddress == null)
                return null;

            return new LocationDto
            {
                BrowserLanguage = "pt-BR",
                StreetName = GuestAddress.StreetName,
                StreetNumber = GuestAddress.AddressNumber,
                AdditionalAddressDetails = GuestAddress.Complement,

                Country = GuestAddress.CountryName,
                CountryCode = GuestAddress.CountryCode,

                Neighborhood = GuestAddress.District,
                Subdivision = GuestAddress.CityName,
                Division = GuestAddress.CityCode,
                PostalCode = GuestAddress.PostalCode,

                LocationCategoryId = (int)LocationCategoryEnum.Residential
            };
        }
    }
}
