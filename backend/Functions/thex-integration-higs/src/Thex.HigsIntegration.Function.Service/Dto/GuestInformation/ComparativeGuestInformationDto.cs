﻿using System;
using Thex.Common.Dto;
using Thex.HigsIntegration.Function.Service.Enumerations;

namespace Thex.HigsIntegration.Function.Service.Dto
{
    public class ComparativeGuestInformationDto
    {
        public int? ThexDocumentTypeId { get; set; }
        public int? ThexNationalityId { get; set; }
        public int? ThexTravelReasonId { get; set; }
        public int? ThexTransportationTypeId { get; set; }
        public int? ThexOccupationId { get; set; }
        public string ThexPersonType { get; set; }
        

        public int PropertyId { get; private set; }
        public Guid PersonId { get; private set; }
        public long GuestId { get; private set; }

        public LocationDto location { get; private set; }

        public void SetPropertyId(int propertyId)
        {
            PropertyId = propertyId;
        }

        public void SetPersonId(Guid personid)
        {
            PersonId = personid;
        }

        public void SetGuestId(long guestid)
        {
            GuestId = guestid;
        }


        public void SetLocation(LocationDto locationDto)
        {
            if (location != null)
                location = locationDto;
        }
    }
}
