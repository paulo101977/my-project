﻿using System.Collections.Generic;
using Thex.Common.Dto;

namespace Thex.HigsIntegration.Function.Service.Dto
{
    public class ReservationDownloadDto
    {
        public string Message { get; set; }

        public List<ReservationHeaderHigsDto> ReservationsList { get; set; }
    }
}
