﻿using System.Collections.Generic;

namespace Thex.HigsIntegration.Function.Service.Dto.RatePlanList
{
    public class RateAmountHigsDto
    {
        public string ID { get; set; }
        public StatusApplicationControlHigsDto StatusApplicationControl { get; set; }
        public RateHigsDto Rate { get; set; }
        public List<AdditionalGuestAmountHigsDto> AdditionalGuestAmounts { get; set; }
    }
}
