﻿using System.Collections.Generic;
using Thex.HigsIntegration.Dto.Enumerations.RatePlanList;

namespace Thex.HigsIntegration.Function.Service.Dto.RatePlanList
{
    public class RatePlanHigsDto
    {
        public string ID { get; set; }
        public string RatePlanCode { get; set; }
        public List<ListOfSourceHigsDto> ListOfSource { get; set; }
        public string Start { get; set; }
        public string End { get; set; }
        public string CurrencyCode { get; set; }
        public TypeComissionHigsEnum TypeComission { get; set; }
        public decimal ValueComission { get; set; }
        public bool IsCommissionable { get; set; }
        public MealsIncludedHigsDto MealsIncluded { get; set; }
        public List<SellableProductHigsDto> SellableProduct { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
