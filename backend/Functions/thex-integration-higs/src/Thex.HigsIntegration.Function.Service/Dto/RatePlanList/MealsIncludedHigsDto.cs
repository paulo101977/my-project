﻿using Thex.HigsIntegration.Dto.Enumerations.RatePlanList;

namespace Thex.HigsIntegration.Function.Service.Dto.RatePlanList
{
    public class MealsIncludedHigsDto
    {
        public string MealPlanIndicator { get; set; }
        public MealPlanHigsEnum MealPlanCode { get; set; }
    }
}
