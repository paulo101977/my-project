﻿using Thex.HigsIntegration.Function.Service.Enumerations.RatePlanList;

namespace Thex.HigsIntegration.Function.Service.Dto.RatePlanList
{
    public class BaseByGuestAmtHigsDto
    {
        public int AgeQualifyingCode { get; set; }
        public int NumberOfGuests { get; set; }
        public decimal AmountAfterTax { get; set; }
    }
}
