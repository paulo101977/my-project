﻿using System.Collections.Generic;

namespace Thex.HigsIntegration.Function.Service.Dto.RatePlanList
{
    public class RatePlanListDto
    {
        public List<RatePlanAndRateAmountHigsDto> RateplanList { get; set; }
    }
}
