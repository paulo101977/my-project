﻿using System.Collections.Generic;

namespace Thex.HigsIntegration.Function.Service.Dto.RatePlanList
{
    public class RatePlanAndRateAmountHigsDto
    {
        public string HotelCode { get; set; }
        public List<RatePlanHigsDto> RatePlan { get; set; }
        public List<RateAmountHigsDto> RateAmount { get; set; }
    }
}
