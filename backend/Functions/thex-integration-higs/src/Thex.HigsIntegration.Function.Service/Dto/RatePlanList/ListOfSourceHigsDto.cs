﻿using System.Collections.Generic;

namespace Thex.HigsIntegration.Function.Service.Dto.RatePlanList
{
    public class ListOfSourceHigsDto
    {
        public string SourceOfBusiness { get; set; }
        public List<ListCompanyHigsDto> ListCompanyID { get; set; }
    }
}
