﻿using Thex.HigsIntegration.Function.Service.Enumerations.RatePlanList;

namespace Thex.HigsIntegration.Function.Service.Dto.RatePlanList
{
    public class AdditionalGuestAmountHigsDto
    {
        public int AgeQualifyingCode { get; set; }
        public decimal Amount { get; set; }
    }
}
