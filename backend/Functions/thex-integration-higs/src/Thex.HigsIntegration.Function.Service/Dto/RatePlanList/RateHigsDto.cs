﻿using System.Collections.Generic;

namespace Thex.HigsIntegration.Function.Service.Dto.RatePlanList
{
    public class RateHigsDto
    {
        public List<BaseByGuestAmtHigsDto> BaseByGuestAmts { get; set; }
    }
}
