﻿using Thex.Common.Enumerations;
using Thex.HigsIntegration.Function.Service.Dto;

namespace Thex.HigsIntegration.Dto.Dto.ReservationDownload
{
    public class HotelReservationHigsDto
    {
        /// <summary>
        /// Unique Confirmation Number.
        /// </summary>
        public string HotelReservationNumber { get; set; }

        /// <summary>
        /// 1- Number sent by partnet when you are inserting a new reservation; 2-  Bematech CMNet Reservas reservation identifier, is used to modify or cancel the reservation. 3- returned after a sucessfull cancelation. It is a Bematech CMNet cancelation number
        /// </summary>
        public TypeReservationHigsEnum TypeReservation { get; set; }
    }
}
