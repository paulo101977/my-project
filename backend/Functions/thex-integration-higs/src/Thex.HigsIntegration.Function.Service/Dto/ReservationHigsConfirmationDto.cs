﻿using System.Collections.Generic;

namespace Thex.HigsIntegration.Function.Service.Dto
{
    public class ReservationHigsConfirmationDto
    {
        public string HotelCode { get; set; }
        public ICollection<object> HotelReservationIds { get; set; }

        public ReservationHigsConfirmationDto()
        {
            HotelReservationIds = new List<object>();
        }
    }
}
