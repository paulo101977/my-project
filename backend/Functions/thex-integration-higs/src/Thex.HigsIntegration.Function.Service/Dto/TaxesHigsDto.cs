﻿namespace Thex.HigsIntegration.Function.Service.Dto
{
    public class TaxesHigsDto
    {
        /// <summary>
        /// Code that identifies the tax.
        /// </summary>
        public int CodeTax { get; set; }

        /// <summary>
        /// Tax amount.
        /// </summary>
        public decimal Value { get; set; }

        /// <summary>
        /// Description of the tax.
        /// </summary>
        public string Description { get; set; }
    }
}
