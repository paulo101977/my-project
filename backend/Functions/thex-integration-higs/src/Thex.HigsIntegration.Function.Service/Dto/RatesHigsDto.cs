﻿using System;

namespace Thex.HigsIntegration.Function.Service.Dto
{
    public class RatesHigsDto
    {
        /// <summary>
        /// Effective date that will be charged.
        /// </summary>
        public DateTime EffectiveDate { get; set; }

        /// <summary>
        /// Amount that will be charged linked to effective date.
        /// </summary>
        public decimal Amount { get; set; }
    }
}
