﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Thex.HigsIntegration.Function.Service.Entities
{
    public class GeneralOccupation
    {
        public GeneralOccupation()
        {
            GuestRegistrationList = new HashSet<GuestRegistration>();
        }
        public int Id { get; set; }

        public string Occupation { get; internal set; }

        public virtual ICollection<GuestRegistration> GuestRegistrationList { get; internal set; }
    }
}
