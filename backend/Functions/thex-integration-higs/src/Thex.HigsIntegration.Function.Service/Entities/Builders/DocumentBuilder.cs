﻿using System;
using System.Collections.Generic;
using System.Text;
using Thex.Common;
using Tnf.Builder;
using Tnf.Notifications;
using Tnf.Specifications;

namespace Thex.HigsIntegration.Function.Service.Entities
{
    public partial class Document
    {
        public class Builder : Builder<Document>
        {
            public Builder(INotificationHandler handler) : base(handler)
            {
            }

            public Builder(INotificationHandler handler, Document instance) : base(handler, instance)
            {
            }

            public virtual Builder WithId(int id)
            {
                Instance.Id = id;
                return this;
            }
            public virtual Builder WithOwnerId(Guid ownerId)
            {
                Instance.OwnerId = ownerId;
                return this;
            }
            public virtual Builder WithDocumentTypeId(int documentTypeId)
            {
                Instance.DocumentTypeId = documentTypeId;
                return this;
            }
            public virtual Builder WithDocumentInformation(string documentInformation)
            {
                Instance.DocumentInformation = documentInformation;
                return this;
            }

            protected override void Specifications()
            {
                AddSpecification(new ExpressionSpecification<Document>(
                    AppConsts.LocalizationSourceName,
                    Document.EntityError.DocumentMustHaveDocumentTypeId,
                    w => w.DocumentTypeId != default(int)));

                AddSpecification(new ExpressionSpecification<Document>(
                    AppConsts.LocalizationSourceName,
                    Document.EntityError.DocumentMustHaveDocumentInformation,
                    w => !string.IsNullOrWhiteSpace(w.DocumentInformation)));

                AddSpecification(new ExpressionSpecification<Document>(
                    AppConsts.LocalizationSourceName,
                    Document.EntityError.DocumentOutOfBoundDocumentInformation,
                    w => string.IsNullOrWhiteSpace(w.DocumentInformation) || w.DocumentInformation.Length > 0 && w.DocumentInformation.Length <= 20));
            }
        }
    }
}
