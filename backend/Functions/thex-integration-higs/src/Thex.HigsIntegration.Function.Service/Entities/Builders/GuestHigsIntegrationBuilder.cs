﻿using System;
using System.Collections.Generic;
using System.Text;
using Thex.Common;
using Tnf.Builder;
using Tnf.Notifications;
using Tnf.Specifications;

namespace Thex.HigsIntegration.Function.Service.Entities
{
    public partial class GuestHigsIntegration
    {
        public class Builder : Builder<GuestHigsIntegration>
        {
            public Builder(INotificationHandler handler) : base(handler)
            {
            }

            public Builder(INotificationHandler handler, GuestHigsIntegration instance) : base(handler, instance)
            {
            }

            public virtual Builder WithId(Guid id)
            {
                Instance.Id = (id == Guid.Empty) ? Guid.NewGuid() : id;
                return this;
            }

            public virtual Builder WithGuestId(long guestId)
            {
                Instance.GuestId = guestId;
                return this;
            }

            public virtual Builder WithPropertyId(int propertyId)
            {
                Instance.PropertyId = propertyId;
                return this;
            }

            public virtual Builder WithHigsCode(string higsCode)
            {
                Instance.HigsCode = higsCode;
                return this;
            }
        }
    }
}

