﻿using System;
using System.Collections.Generic;
using System.Text;
using Thex.Common;
using Tnf.Builder;
using Tnf.Notifications;
using Tnf.Specifications;

namespace Thex.HigsIntegration.Function.Service.Entities
{
    public partial class Guest
    {
        public class Builder : Builder<Guest>
        {
            public Builder(INotificationHandler handler) : base(handler)
            {
            }

            public Builder(INotificationHandler handler, Guest instance) : base(handler, instance)
            {
            }

            public virtual Builder WithId(long id)
            {
                Instance.Id = id;
                return this;
            }
            public virtual Builder WithPersonId(Guid? personId)
            {
                Instance.PersonId = personId;
                return this;
            }
        }
    }
}

