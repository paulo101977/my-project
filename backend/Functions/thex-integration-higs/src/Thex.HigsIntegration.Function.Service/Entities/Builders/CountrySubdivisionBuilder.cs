﻿using System;
using System.Collections.Generic;
using System.Text;
using Thex.Common;
using Tnf.Builder;
using Tnf.Notifications;
using Tnf.Specifications;

namespace Thex.HigsIntegration.Function.Service.Entities
{
    public partial class CountrySubdivision
    {
        public class Builder : Builder<CountrySubdivision>
        {
            public Builder(INotificationHandler handler) : base(handler)
            {
            }

            public Builder(INotificationHandler handler, CountrySubdivision instance) : base(handler, instance)
            {
            }

            public virtual Builder WithId(int id)
            {
                Instance.Id = id;
                return this;
            }
            public virtual Builder WithTwoLetterIsoCode(string twoLetterIsoCode)
            {
                Instance.TwoLetterIsoCode = twoLetterIsoCode;
                return this;
            }
            public virtual Builder WithThreeLetterIsoCode(string threeLetterIsoCode)
            {
                Instance.ThreeLetterIsoCode = threeLetterIsoCode;
                return this;
            }
            public virtual Builder WithCountryTwoLetterIsoCode(string countryTwoLetterIsoCode)
            {
                Instance.CountryTwoLetterIsoCode = countryTwoLetterIsoCode;
                return this;
            }
            public virtual Builder WithCountryThreeLetterIsoCode(string countryThreeLetterIsoCode)
            {
                Instance.CountryThreeLetterIsoCode = countryThreeLetterIsoCode;
                return this;
            }
            public virtual Builder WithExternalCode(string externalCode)
            {
                Instance.ExternalCode = externalCode;
                return this;
            }
            public virtual Builder WithParentSubdivisionId(int? parentSubdivisionId)
            {
                Instance.ParentSubdivisionId = parentSubdivisionId;
                return this;
            }
            public virtual Builder WithSubdivisionTypeId(int subdivisionTypeId)
            {
                Instance.SubdivisionTypeId = subdivisionTypeId;
                return this;
            }

            protected override void Specifications()
            {
                AddSpecification(new ExpressionSpecification<CountrySubdivision>(
                    AppConsts.LocalizationSourceName,
                    CountrySubdivision.EntityError.CountrySubdivisionOutOfBoundTwoLetterIsoCode,
                    w => string.IsNullOrWhiteSpace(w.TwoLetterIsoCode) || w.TwoLetterIsoCode.Length > 0 && w.TwoLetterIsoCode.Length <= 2));

                AddSpecification(new ExpressionSpecification<CountrySubdivision>(
                    AppConsts.LocalizationSourceName,
                    CountrySubdivision.EntityError.CountrySubdivisionOutOfBoundThreeLetterIsoCode,
                    w => string.IsNullOrWhiteSpace(w.ThreeLetterIsoCode) || w.ThreeLetterIsoCode.Length > 0 && w.ThreeLetterIsoCode.Length <= 3));

                AddSpecification(new ExpressionSpecification<CountrySubdivision>(
                    AppConsts.LocalizationSourceName,
                    CountrySubdivision.EntityError.CountrySubdivisionOutOfBoundCountryTwoLetterIsoCode,
                    w => string.IsNullOrWhiteSpace(w.CountryTwoLetterIsoCode) || w.CountryTwoLetterIsoCode.Length > 0 && w.CountryTwoLetterIsoCode.Length <= 2));

                AddSpecification(new ExpressionSpecification<CountrySubdivision>(
                    AppConsts.LocalizationSourceName,
                    CountrySubdivision.EntityError.CountrySubdivisionOutOfBoundCountryThreeLetterIsoCode,
                    w => string.IsNullOrWhiteSpace(w.CountryThreeLetterIsoCode) || w.CountryThreeLetterIsoCode.Length > 0 && w.CountryThreeLetterIsoCode.Length <= 3));

                AddSpecification(new ExpressionSpecification<CountrySubdivision>(
                    AppConsts.LocalizationSourceName,
                    CountrySubdivision.EntityError.CountrySubdivisionOutOfBoundExternalCode,
                    w => string.IsNullOrWhiteSpace(w.ExternalCode) || w.ExternalCode.Length > 0 && w.ExternalCode.Length <= 20));

            }
        }
    }
}
