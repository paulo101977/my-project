﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Thex.HigsIntegration.Function.Service.Entities
{
    public partial class Reservation
    {
        public class Builder
        {
            public Reservation Instance { get; set; }

            public Builder()
            {
                Instance = new Reservation();
            }

            public virtual Builder WithId(long id)
            {
                Instance.Id = id;
                return this;
            }

            public virtual Builder WithReservationUid(Guid reservationUid)
            {
                Instance.ReservationUid = reservationUid == Guid.Empty ? Guid.NewGuid() : reservationUid;
                return this;
            }

            public virtual Builder WithReservationCode(string reservationCode)
            {
                Instance.ReservationCode = reservationCode;
                return this;
            }

            public virtual Builder WithReservationVendorCode(string reservationVendorCode)
            {
                Instance.ReservationVendorCode = reservationVendorCode;
                return this;
            }

            public virtual Builder WithPropertyId(int propertyId)
            {
                Instance.PropertyId = propertyId;
                return this;
            }

            public virtual Builder WithContactName(string contactName)
            {
                Instance.ContactName = contactName;
                return this;
            }

            public virtual Builder WithContactEmail(string contactEmail)
            {
                Instance.ContactEmail = contactEmail;
                return this;
            }

            public virtual Builder WithContactPhone(string contactPhone)
            {
                Instance.ContactPhone = contactPhone;
                return this;
            }

            public virtual Builder WithInternalComments(string internalComments)
            {
                Instance.InternalComments = internalComments;
                return this;
            }

            public virtual Builder WithExternalComments(string externalComments)
            {
                Instance.ExternalComments = externalComments;
                return this;
            }

            public virtual Builder WithGroupName(string groupName)
            {
                Instance.GroupName = groupName;
                return this;
            }

            public virtual Builder WithDeadline(DateTime? deadline)
            {
                Instance.Deadline = deadline;
                return this;
            }

            public virtual Builder WithUnifyAccounts(bool unifyAccounts)
            {
                Instance.UnifyAccounts = unifyAccounts;
                return this;
            }

            public virtual Builder WithBusinessSourceId(int? businessSourceId)
            {
                Instance.BusinessSourceId = businessSourceId;
                return this;
            }

            public virtual Builder WithReservationItemList(IList<ReservationItem> ReservationItemList)
            {
                Instance.ReservationItemList = ReservationItemList;
                return this;
            }

            public virtual Builder WithMarketSegmentId(int? marketSegmentId)
            {
                Instance.MarketSegmentId = marketSegmentId;
                return this;
            }

            public virtual Builder WithCompanyClientId(Guid? companyClientId, Guid? reservationConfirmationBillingClientId)
            {
                if ((companyClientId == null) && (reservationConfirmationBillingClientId != null))
                    companyClientId = reservationConfirmationBillingClientId;

                if (companyClientId != null)
                {
                    Instance.CompanyClientId = companyClientId;
                }
                else
                {
                    Instance.CompanyClientId = null;
                }
                return this;
            }

            public virtual Builder WithCompanyClientContactPersonId(Guid? companyClientContactPersonId)
            {
                Instance.CompanyClientContactPersonId = companyClientContactPersonId;
                return this;
            }

            public virtual Builder WithRatePlanId(Guid? ratePlanId)
            {
                Instance.RatePlanId = ratePlanId;
                return this;
            }
        }
    }
}
