﻿using System.Collections.Generic;

namespace Thex.HigsIntegration.Function.Service.Entities
{
    public class TransportationType
    {
        public TransportationType()
        {
            GuestRegistrationList = new HashSet<GuestRegistration>();
        }

        public int Id { get; set; }
        public string TransportationTypeName { get; internal set; }
        public int? HigsCode { get; internal set; }

        public virtual ICollection<GuestRegistration> GuestRegistrationList { get; internal set; }
    }
}
