﻿using System;

namespace Thex.HigsIntegration.Function.Service.Entities
{
    public class PropertyParameter : ThexMultiTenantFullAuditedEntity
    {
        public Guid Id { get; set; }
        public int PropertyId { get; internal set; }
        public int ApplicationParameterId { get; internal set; }
        public string PropertyParameterValue { get; internal set; }
        public string PropertyParameterMinValue { get; internal set; }
        public string PropertyParameterMaxValue { get; internal set; }
        public string PropertyParameterPossibleValues { get; internal set; }
        public bool IsActive { get; internal set; }
    }
}
