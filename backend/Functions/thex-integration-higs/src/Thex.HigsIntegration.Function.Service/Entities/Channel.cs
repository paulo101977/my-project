﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Thex.HigsIntegration.Function.Service.Entities
{
    public class Channel : ThexMultiTenantFullAuditedEntity
    {
        public Guid Id { get; set; }
        public int ChannelCodeId { get; internal set; }
        public string Description { get; internal set; }
        public int? BusinessSourceId { get; internal set; }
        public decimal? DistributionAmount { get; internal set; }
        public int? MarketSegmentId { get; internal set; }
        public bool IsActive { get; internal set; }

        public virtual ChannelCode ChannelCode { get; internal set; }
        public virtual MarketSegment MarketSegment { get; internal set; }
        public virtual BusinessSource BusinessSource { get; internal set; }
        public virtual ICollection<CompanyClientChannel> CompanyClientChannelList { get; internal set; }
    }
}
