﻿using System;
using Thex.Inventory.Domain.Entities;

namespace Thex.HigsIntegration.Function.Service.Entities
{
    public class PropertyBaseRateHistory : ThexMultiTenantFullAuditedEntity, IEntityGuid
    {
        public object this[string propertyName]
        {
            get { return this.GetType().GetProperty(propertyName).GetValue(this, null); }
            set { this.GetType().GetProperty(propertyName).SetValue(this, value, null); }
        }

        public Guid Id { get; set; }

        public int PropertyId { get; internal set; }
        public int MealPlanTypeDefault { get; internal set; }
        public int MealPlanTypeId { get; internal set; }
        public int RoomTypeId { get; internal set; }
        public decimal? Pax1Amount { get; internal set; }
        public decimal? Pax2Amount { get; internal set; }
        public decimal? Pax3Amount { get; internal set; }
        public decimal? Pax4Amount { get; internal set; }
        public decimal? Pax5Amount { get; internal set; }
        public decimal? Pax6Amount { get; internal set; }
        public decimal? Pax7Amount { get; internal set; }
        public decimal? Pax8Amount { get; internal set; }
        public decimal? Pax9Amount { get; internal set; }
        public decimal? Pax10Amount { get; internal set; }
        public decimal? Pax11Amount { get; internal set; }
        public decimal? Pax12Amount { get; internal set; }
        public decimal? Pax13Amount { get; internal set; }
        public decimal? Pax14Amount { get; internal set; }
        public decimal? Pax15Amount { get; internal set; }
        public decimal? Pax16Amount { get; internal set; }
        public decimal? Pax17Amount { get; internal set; }
        public decimal? Pax18Amount { get; internal set; }
        public decimal? Pax19Amount { get; internal set; }
        public decimal? Pax20Amount { get; internal set; }
        public decimal? PaxAdditionalAmount { get; internal set; }
        public decimal? Child1Amount { get; internal set; }
        public decimal? Child2Amount { get; internal set; }
        public decimal? Child3Amount { get; internal set; }
        public decimal? AdultMealPlanAmount { get; internal set; }
        public decimal? Child1MealPlanAmount { get; internal set; }
        public decimal? Child2MealPlanAmount { get; internal set; }
        public decimal? Child3MealPlanAmount { get; internal set; }
        public Guid CurrencyId { get; internal set; }
        public string CurrencySymbol { get; internal set; }
        public int Level { get; internal set; }
        public Guid? RatePlanId { get; internal set; }
        public Guid? PropertyBaseRateHeaderHistoryId { get; internal set; }

        public virtual Property Property { get; internal set; }
        public virtual PropertyBaseRateHeaderHistory PropertyBaseRateHeaderHistory { get; internal set; }
        public virtual RoomType RoomType { get; internal set; }
        public virtual RatePlan RatePlan { get; internal set; }
        public virtual Currency Currency { get; internal set; }
    }
}
