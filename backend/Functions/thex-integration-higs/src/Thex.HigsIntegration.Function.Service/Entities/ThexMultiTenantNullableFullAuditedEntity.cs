﻿using System;

namespace Thex.HigsIntegration.Function.Service.Entities
{
    [Serializable]
    public abstract class ThexMultiTenantNullableFullAuditedEntity : ThexFullAuditedEntity
    {
        public Guid? TenantId { get; set; }

        public Tenant Tenant { get; set; }
    }
}
