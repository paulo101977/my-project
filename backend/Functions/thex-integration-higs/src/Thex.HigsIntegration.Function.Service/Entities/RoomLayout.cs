﻿using System;

namespace Thex.HigsIntegration.Function.Service.Entities
{
    public class RoomLayout
    {
        public Guid Id { get; set; }
        public byte QuantitySingle { get; internal set; }
        public byte QuantityDouble { get; internal set; }
    }
}
