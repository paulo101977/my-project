﻿using System;

namespace Thex.HigsIntegration.Function.Service.Entities
{
    public class ReservationConfirmation : ThexMultiTenantFullAuditedEntity
    {
        public Guid Id { get; set; }
        public long ReservationId { get; internal set; }
        public int PaymentTypeId { get; internal set; }
        public long? PlasticId { get; internal set; }
        public Guid? BillingClientId { get; internal set; }
        public bool EnsuresNoShow { get; internal set; }
        public decimal? Value { get; internal set; }
        public DateTime? Deadline { get; internal set; }
        public bool? LaunchDaily { get; internal set; }

        public virtual Reservation Reservation { get; set; }
    }
}
