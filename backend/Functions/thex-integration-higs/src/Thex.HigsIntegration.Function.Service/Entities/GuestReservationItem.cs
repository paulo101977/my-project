﻿using System;

namespace Thex.HigsIntegration.Function.Service.Entities
{
    public class GuestReservationItem : ThexMultiTenantFullAuditedEntity
    {
        public long Id { get; set; }
        public long ReservationItemId { get; internal set; }
        public long? GuestId { get; internal set; }
        public string GuestName { get; internal set; }
        public string GuestEmail { get; internal set; }
        public int? GuestDocumentTypeId { get; internal set; }
        public string GuestDocument { get; internal set; }
        public DateTime EstimatedArrivalDate { get; internal set; }
        public DateTime? CheckInDate { get; internal set; }
        public DateTime EstimatedDepartureDate { get; internal set; }
        public DateTime? CheckOutDate { get; internal set; }
        public int GuestStatusId { get; internal set; }
        public int GuestTypeId { get; internal set; }
        public string PreferredName { get; internal set; }
        public string GuestCitizenship { get; internal set; }
        public string GuestLanguage { get; internal set; }
        public short PctDailyRate { get; internal set; }
        public bool IsIncognito { get; internal set; }
        public bool IsChild { get; internal set; }
        public short? ChildAge { get; internal set; }
        public bool IsMain { get; internal set; }
        public Guid? GuestRegistrationId { get; internal set; }
        public int PropertyId { get; internal set; }

        public DateTime? CheckInHour { get; internal set; }

        public DateTime? CheckOutHour { get; internal set; }

        public virtual ReservationItem ReservationItem { get; internal set; }
        public virtual DocumentType GuestDocumentType { get; internal set; }
        public virtual PropertyGuestType GuestType { get; internal set; }
    }
}
