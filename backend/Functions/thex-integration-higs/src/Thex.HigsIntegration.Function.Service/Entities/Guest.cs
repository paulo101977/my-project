﻿using System;
using System.Collections.Generic;
using System.Text;
using Tnf.Notifications;

namespace Thex.HigsIntegration.Function.Service.Entities
{
    public partial class Guest
    {
        public Guest()
        {
            GuestRegistrationList = new HashSet<GuestRegistration>();
            GuestReservationItemList = new HashSet<GuestReservationItem>();
            GuestHigsIntegrationList = new HashSet<GuestHigsIntegration>();
        }
        public long Id { get; set; }
        public Guid? PersonId { get; internal set; }

        public virtual Person Person { get; internal set; }
        public virtual ICollection<GuestRegistration> GuestRegistrationList { get; internal set; }
        public virtual ICollection<GuestReservationItem> GuestReservationItemList { get; internal set; }
        public virtual ICollection<GuestHigsIntegration> GuestHigsIntegrationList { get; internal set; }

        public static Builder Create(INotificationHandler handler)
            => new Builder(handler);

        public static Builder Create(INotificationHandler handler, Guest instance)
            => new Builder(handler, instance);
    }
}
