﻿using Thex.Inventory.Domain.Entities;

namespace Thex.HigsIntegration.Function.Service.Entities
{
    public class MarketSegment : ThexFullAuditedEntity, IEntityInt
    {
        public int Id { get; set; }
        public string Name { get; internal set; }
        public bool IsActive { get; internal set; }
        public int? ParentMarketSegmentId { get; internal set; }
        public string Code { get; internal set; }
        public string SegmentAbbreviation { get; internal set; }
    }
}