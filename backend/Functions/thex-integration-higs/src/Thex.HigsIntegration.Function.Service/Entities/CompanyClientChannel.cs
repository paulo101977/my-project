﻿using System;

namespace Thex.HigsIntegration.Function.Service.Entities
{
    public class CompanyClientChannel
    {
        public Guid Id { get; set; }
        public Guid ChannelId { get; internal set; }
        public Guid CompanyClientId { get; internal set; }
        public bool IsActive { get; internal set; }
        public string CompanyId { get; internal set; }

        public Channel Channel { get; internal set; }
    }
}
