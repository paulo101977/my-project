﻿using System;
using System.Collections.Generic;

namespace Thex.HigsIntegration.Function.Service.Entities
{
    public partial class Reservation : ThexMultiTenantFullAuditedEntity
    {
        public long Id { get; set; }
        public Guid ReservationUid { get; internal set; }
        public string ReservationCode { get; internal set; }
        public string ReservationVendorCode { get; internal set; }
        public int PropertyId { get; internal set; }
        public string ContactName { get; internal set; }
        public string ContactEmail { get; internal set; }
        public string ContactPhone { get; internal set; }
        public string InternalComments { get; internal set; }
        public string ExternalComments { get; internal set; }
        public string PartnerComments { get; set; }
        public string GroupName { get; internal set; }
        public DateTime? Deadline { get; internal set; }
        public bool UnifyAccounts { get; internal set; }
        public int? BusinessSourceId { get; internal set; }
        public int? MarketSegmentId { get; internal set; }
        public Guid? CompanyClientId { get; internal set; }
        public Guid? CompanyClientContactPersonId { get; internal set; }
        public Guid? RatePlanId { get; internal set; }

        public virtual RatePlan RatePlan { get; internal set; }

        public virtual ICollection<ReservationItem> ReservationItemList { get; internal set; }
        public virtual ICollection<ReservationConfirmation> ReservationConfirmationList { get; internal set; }

        public Reservation()
        {
            ReservationItemList = new HashSet<ReservationItem>();
            ReservationConfirmationList = new HashSet<ReservationConfirmation>();
        }
    }
}
