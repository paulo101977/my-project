﻿using System;
using System.Collections.Generic;
using System.Text;
using Thex.Common.Enumerations;
using Tnf.Notifications;

namespace Thex.HigsIntegration.Function.Service.Entities
{
    public partial class Person
    {
        public Person()
        {
            DocumentList = new HashSet<Document>();
            LocationList = new HashSet<Location>();
            ContactInformationList = new HashSet<ContactInformation>();
            GuestList = new HashSet<Guest>();
            GuestRegistrationList = new HashSet<GuestRegistration>();
            PersonInformationList = new HashSet<PersonInformation>();
        }
        public Guid Id { get; set; }

        public string FirstName { get; internal set; }
        public string LastName { get; internal set; }
        public string FullName { get; internal set; }
        public DateTime? DateOfBirth { get; internal set; }
        public int? CountrySubdivisionId { get; internal set; }
        public string PersonType { get; set; }
        public CountrySubdivision CountrySubdivision { get; internal set; }

        public virtual Guest Guest { get; set; }
        public virtual ICollection<Document> DocumentList { get; set; }
        public virtual ICollection<Location> LocationList { get; set; }
        public virtual ICollection<ContactInformation> ContactInformationList { get; set; }
        public virtual ICollection<Guest> GuestList { get; set; }
        public virtual ICollection<GuestRegistration> GuestRegistrationList { get; internal set; }
        public virtual ICollection<PersonInformation> PersonInformationList { get; set; }

        public enum EntityError
        {
            PersonMustHavePersonType,
            PersonOutOfBoundPersonType,
            PersonMustHaveFirstName,
            PersonOutOfBoundFirstName,
            PersonOutOfBoundLastName,
            PersonOutOfBoundFullName,
            PersonInvalidDateOfBirth
        }

        public static Builder Create(INotificationHandler handler)
            => new Builder(handler);

        public static Builder Create(INotificationHandler handler, Person instance)
            => new Builder(handler, instance);
    }
}
