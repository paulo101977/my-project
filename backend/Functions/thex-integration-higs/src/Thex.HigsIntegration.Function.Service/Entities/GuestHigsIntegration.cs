﻿using System;
using System.Collections.Generic;
using System.Text;
using Tnf.Notifications;

namespace Thex.HigsIntegration.Function.Service.Entities
{
    public partial class GuestHigsIntegration
    {
        public Guid Id { get; set; }
        public int PropertyId { get; internal set; }
        public string HigsCode { get; internal set; }
        public long GuestId { get; internal set; }

        public virtual Property Property { get; internal set; }
        public virtual Guest Guest { get; internal set; }

        public static Builder Create(INotificationHandler handler)
            => new Builder(handler);

        public static Builder Create(INotificationHandler handler, GuestHigsIntegration instance)
            => new Builder(handler, instance);
    }
}
