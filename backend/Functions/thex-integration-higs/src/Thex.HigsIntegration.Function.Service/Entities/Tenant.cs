﻿using System;

namespace Thex.HigsIntegration.Function.Service.Entities
{
   public class Tenant : ThexFullAuditedEntity
    {
        public Guid Id { get; set; }
        public string TenantName { get; set; }
        public Guid? ParentId { get; set; }
        public bool IsActive { get; set; }
    }
}
