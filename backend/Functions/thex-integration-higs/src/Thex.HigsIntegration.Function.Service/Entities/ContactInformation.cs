﻿using System;
using System.Collections.Generic;
using System.Text;
using Tnf.Notifications;

namespace Thex.HigsIntegration.Function.Service.Entities
{
    public partial class ContactInformation
    {
        public int Id { get; set; }
        public Guid OwnerId { get; internal set; }
        public int ContactInformationTypeId { get; internal set; }
        public string Information { get; internal set; }
        
        public virtual Person Owner { get; internal set; }

        public enum EntityError
        {
            ContactInformationMustHaveOwnerId,
            ContactInformationMustHaveContactInformationTypeId,
            ContactInformationMustHaveInformation,
            ContactInformationOutOfBoundInformation
        }

        public static Builder Create(INotificationHandler handler)
            => new Builder(handler);

        public static Builder Create(INotificationHandler handler, ContactInformation instance)
            => new Builder(handler, instance);
    }
}
