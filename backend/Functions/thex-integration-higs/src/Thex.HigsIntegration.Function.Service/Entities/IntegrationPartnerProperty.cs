﻿using System;
using System.Collections.Generic;

namespace Thex.HigsIntegration.Function.Service.Entities
{
    public class IntegrationPartnerProperty
    {
        public Guid Id { get; set; }
        public int PropertyId { get; internal set; }
        public bool IsActive { get; internal set; }
        public string IntegrationCode { get; internal set; }
        public int PartnerId { get; internal set; }

        public virtual Property Property { get; set; }

        public virtual ICollection<IntegrationPartnerPropertyHistory> IntegrationPartnerPropertyHistoryList { get; internal set; }

        public IntegrationPartnerProperty()
        {
            IntegrationPartnerPropertyHistoryList = new HashSet<IntegrationPartnerPropertyHistory>();
        }
    }
}
