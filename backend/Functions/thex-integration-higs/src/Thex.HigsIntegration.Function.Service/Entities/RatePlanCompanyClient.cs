﻿using System;
using Thex.Inventory.Domain.Entities;

namespace Thex.HigsIntegration.Function.Service.Entities
{
    public class RatePlanCompanyClient : ThexMultiTenantFullAuditedEntity, IEntityGuid
    {
        public Guid Id { get; set; }
        public Guid CompanyClientId { get; internal set; }
        public Guid RatePlanId { get; internal set; }

        public virtual RatePlan RatePlan { get; internal set; }
    }
}
