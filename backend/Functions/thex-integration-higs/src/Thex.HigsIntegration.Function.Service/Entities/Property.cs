﻿using System;
using System.Collections.Generic;

namespace Thex.HigsIntegration.Function.Service.Entities
{
    public class Property : ThexMultiTenantFullAuditedEntity
    {
        public Property()
        {
            LocationList = new HashSet<Location>();
            GuestRegistrationList = new HashSet<GuestRegistration>();
            GuestHigsIntegrationList = new HashSet<GuestHigsIntegration>();
        }

        public int Id { get; set; }
        public int CompanyId { get; internal set; }
        public int BrandId { get; internal set; }
        public string Name { get; internal set; }
        public Guid PropertyUId { get; internal set; }
        public int PropertyStatusId { get; internal set; }

        public virtual ICollection<IntegrationPartnerProperty> IntegrationPartnerPropertyList { get; internal set; }
        public virtual ICollection<Location> LocationList { get; internal set; }
        public virtual ICollection<GuestRegistration> GuestRegistrationList { get; internal set; }
        public virtual ICollection<GuestHigsIntegration> GuestHigsIntegrationList { get; internal set; }
    }
}
