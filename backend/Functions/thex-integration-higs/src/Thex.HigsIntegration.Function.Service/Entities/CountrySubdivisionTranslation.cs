﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Thex.HigsIntegration.Function.Service.Entities
{
    public partial class CountrySubdivisionTranslation
    {
        public int Id { get; set; }
        public string LanguageIsoCode { get; internal set; }
        public string Name { get; internal set; }
        public int CountrySubdivisionId { get; internal set; }
        public string Nationality { get; internal set; }

        public virtual CountrySubdivision CountrySubdivision { get; internal set; }

        public enum EntityError
        {
            CountrySubdivisionTranslationMustHaveLanguageIsoCode,
            CountrySubdivisionTranslationOutOfBoundLanguageIsoCode,
            CountrySubdivisionTranslationMustHaveName,
            CountrySubdivisionTranslationOutOfBoundName,
            CountrySubdivisionTranslationMustHaveCountrySubdivisionId,
            CountrySubdivisionTranslationOutOfBoundNationality,
            CountrySubdivisionTranslationMustHaveCountryCode,
            CountrySubdivisionTranslationInvalidLanguage,
            CountrySubdivisionTranslationGeocodeApiReturnZeroResults,
            CountrySubdivisionTranslationGeocodeApiReturnError,
            CountrySubdivisionTranslationCountryCodeNotEqualGeocodeCountryShortName,
            CountrySubdivisionTranslationStateNameNotEqualGeocodeStateShortName
        }
    }
}
