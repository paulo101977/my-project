﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Thex.HigsIntegration.Function.Service.Entities
{
    public class BusinessSource : ThexFullAuditedEntity
    {
        public int Id { get; set; }
        public string Description { get; internal set; }
        public bool IsActive { get; internal set; }
        public string Code { get; set; }
    }
}
