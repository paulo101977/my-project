﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Thex.HigsIntegration.Function.Service.Entities
{
    public class PropertyGuestType
    {
        public PropertyGuestType()
        {
            GuestRegistrationList = new HashSet<GuestRegistration>();
            GuestReservationItemList = new HashSet<GuestReservationItem>();
        }

        public int Id { get; set; }
        public int PropertyId { get; internal set; }
        public string GuestTypeName { get; internal set; }
        public bool IsVip { get; internal set; }

        public string Code { get; internal set; }
        public bool? IsIncognito { get; internal set; }
        public bool? IsActive { get; internal set; }

        public virtual Property Property { get; internal set; }
        public virtual ICollection<GuestRegistration> GuestRegistrationList { get; internal set; }
        public virtual ICollection<GuestReservationItem> GuestReservationItemList { get; internal set; }
    }
}
