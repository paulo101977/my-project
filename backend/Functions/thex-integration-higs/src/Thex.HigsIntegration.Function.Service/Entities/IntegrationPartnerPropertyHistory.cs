﻿using System;

namespace Thex.HigsIntegration.Function.Service.Entities
{
    public class IntegrationPartnerPropertyHistory : ThexFullAuditedEntity
    {
        public Guid Id { get; set; }
        public long ReservationItemId { get; internal set; }
        public Guid IntegrationPartnerPropertyId { get; internal set; }
        public string Response { get; internal set; }
        public int IntegrationOperationType { get; internal set; }

        public virtual ReservationItem ReservationItem { get; internal set; }
        public virtual  IntegrationPartnerProperty IntegrationPartnerProperty { get; internal set; }
    }
}
