﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Thex.HigsIntegration.Function.Service.Entities
{
    public class Reason
    {
        public Reason()
        {
            GuestRegistrationList = new HashSet<GuestRegistration>();
            ReservationItemList = new HashSet<ReservationItem>();
        }

        public int Id { get; set; }
        public string ReasonName { get; internal set; }
        public int ReasonCategoryId { get; internal set; }
        public bool IsActive { get; internal set; }
        public int ChainId { get; internal set; }
        public int? HigsCode { get; internal set; }

        public virtual ICollection<GuestRegistration> GuestRegistrationList { get; internal set; }
        public virtual ICollection<ReservationItem> ReservationItemList { get; internal set; }
    }
}
