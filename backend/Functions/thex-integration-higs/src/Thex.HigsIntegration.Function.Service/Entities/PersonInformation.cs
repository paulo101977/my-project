﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Thex.HigsIntegration.Function.Service.Entities
{
    public class PersonInformation
    {
        public int Id { get; set; }
        public Guid OwnerId { get; internal set; }
        public int PersonInformationTypeId { get; internal set; }
        public string Information { get; internal set; }

        public virtual Person Owner { get; internal set; }
    }
}
