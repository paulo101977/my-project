﻿using System;
using Thex.Inventory.Domain.Entities;

namespace Thex.HigsIntegration.Function.Service.Entities
{
    public class RatePlanRoomType : ThexMultiTenantFullAuditedEntity, IEntityGuid
    {
        public Guid Id { get; set; }
        public Guid RatePlanId { get; internal set; }
        public int RoomTypeId { get; internal set; }
        public decimal PercentualAmmount { get; internal set; }
        public bool IsDecreased { get; internal set; }

        public virtual RatePlan RatePlan { get; internal set; }
        public virtual RoomType RoomType { get; internal set; }
    }
}
