﻿using System;
using System.Collections.Generic;
using System.Text;
using Tnf.Notifications;

namespace Thex.HigsIntegration.Function.Service.Entities
{
    public partial class Location
    {
        public int Id { get; set; }
        public Guid OwnerId { get; internal set; }
        public int LocationCategoryId { get; internal set; }
        public decimal Latitude { get; internal set; }
        public decimal Longitude { get; internal set; }
        public string StreetName { get; internal set; }
        public string StreetNumber { get; internal set; }
        public string AdditionalAddressDetails { get; internal set; }
        public string Neighborhood { get; internal set; }
        public int CityId { get; internal set; }
        public int? StateId { get; internal set; }
        public int? CountryId { get; internal set; }
        public string PostalCode { get; internal set; }
        public string CountryCode { get; internal set; }
        public virtual Person Person { get; internal set; }
        public virtual Property Property { get; internal set; }
        public virtual GuestRegistration GuestRegistration { get; internal set; }

        public virtual CountrySubdivision City { get; internal set; }
        public virtual CountrySubdivision State { get; internal set; }
        public virtual CountrySubdivision CountryEntity { get; internal set; }

        public enum EntityError
        {
            LocationMustHaveLocationCategoryId,
            LocationMustHaveStreetName,
            LocationOutOfBoundStreetName,
            LocationMustHaveStreetNumber,
            LocationOutOfBoundStreetNumber,
            LocationOutOfBoundAdditionalAddressDetails,
            LocationOutOfBoundNeighborhood,
            LocationMustHaveCityId,
            LocationMustHaveCountryCode,
            LocationOutOfBoundCountryCode
        }

        public static Builder Create(INotificationHandler handler)
            => new Builder(handler);

        public static Builder Create(INotificationHandler handler, Location instance)
            => new Builder(handler, instance);
    }
}
