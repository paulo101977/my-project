﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Thex.HigsIntegration.Function.Service.Entities
{
    public class ChannelCode
    {
        public int Id { get; set; }
        public string ChannelCode1 { get; internal set; }
        public string ChannelCodeDescription { get; internal set; }

        public virtual ICollection<Channel> ChannelList { get; internal set; }

        public ChannelCode()
        {
            ChannelList = new HashSet<Channel>();
        }
    }
}
