﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Thex.HigsIntegration.Function.Service.Entities
{
    public class ReasonCategory
    {
        public ReasonCategory()
        {
            ReasonList = new HashSet<Reason>();
        }

        public int Id { get; set; }
        public string ReasonType { get; internal set; }

        public virtual ICollection<Reason> ReasonList { get; internal set; }
    }
}
