﻿using System;
using Thex.Inventory.Domain.Entities;

namespace Thex.HigsIntegration.Function.Service.Entities
{
    public class RatePlanCommission : ThexMultiTenantFullAuditedEntity, IEntityGuid
    {
        public Guid Id { get; set; }
        public Guid RatePlanId { get; internal set; }
        public int BillingItemId { get; internal set; }
        public decimal CommissionPercentualAmount { get; internal set; }

        public virtual RatePlan RatePlan { get; internal set; }
    }
}
