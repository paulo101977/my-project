﻿using System;
using System.Collections.Generic;

namespace Thex.HigsIntegration.Function.Service.Entities
{
    public class ReservationItem : ThexMultiTenantFullAuditedEntity
    {
        public long Id { get; set; }
        public Guid ReservationItemUid { get; internal set; }
        public long ReservationId { get; internal set; }
        public int? ReasonId { get; internal set; }
        public DateTime EstimatedArrivalDate { get; internal set; }
        public DateTime? CheckInDate { get; internal set; }
        public DateTime EstimatedDepartureDate { get; internal set; }
        public DateTime? CheckOutDate { get; internal set; }
        public DateTime? CancellationDate { get; internal set; }
        public string CancellationDescription { get; internal set; }
        public string ReservationItemCode { get; internal set; }
        public int RequestedRoomTypeId { get; internal set; }
        public int ReceivedRoomTypeId { get; internal set; }
        public int? RoomId { get; internal set; }
        public byte AdultCount { get; internal set; }
        public byte ChildCount { get; internal set; }
        public int ReservationItemStatusId { get; internal set; }
        public Guid RoomLayoutId { get; internal set; }
        public byte ExtraBedCount { get; internal set; }
        public byte ExtraCribCount { get; internal set; }
        public Guid? RatePlanId { get; internal set; }
        public Guid? CurrencyId { get; internal set; }
        public int? MealPlanTypeId { get; internal set; }
        public int? GratuityTypeId { get; internal set; }
        public string ExternalReservationNumber { get; internal set; }
        public string PartnerReservationNumber { get; internal set; }
        public bool WalkIn { get; internal set; }
        public bool IsMigrated { get; internal set; }
        public DateTime? CheckInHour { get; internal set; }
        public DateTime? CheckOutHour { get; internal set; }

        public virtual Currency Currency { get; internal set; }
        public virtual Reservation Reservation { get; internal set; }

        public virtual ICollection<ReservationBudget> ReservationBudgetList { get; internal set; }
        public virtual ICollection<GuestReservationItem> GuestReservationItemList { get; internal set; }
        public virtual ICollection<IntegrationPartnerPropertyHistory> IntegrationPartnerPropertyHistoryList { get; internal set; }

        public ReservationItem()
        {
            ReservationBudgetList = new HashSet<ReservationBudget>();
            GuestReservationItemList = new HashSet<GuestReservationItem>();
            IntegrationPartnerPropertyHistoryList = new HashSet<IntegrationPartnerPropertyHistory>();
        }
    }
}
