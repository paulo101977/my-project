﻿using System;
using System.Collections.Generic;

namespace Thex.HigsIntegration.Function.Service.Entities
{
    public class RatePlan : ThexMultiTenantFullAuditedEntity
    {
        public Guid Id { get; set; }
        public int AgreementTypeId { get; set; }
        public string AgreementName { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int PropertyId { get; set; }
        public int MealPlanTypeId { get; set; }
        public bool? IsActive { get; set; }
        public Guid CurrencyId { get; set; }
        public string CurrencySymbol { get; set; }
        public bool RateNet { get; set; }
        public int RateTypeId { get; set; }
        public int? MarketSegmentId { get; set; }
        public string DistributionCode { get; set; }
        public string Description { get; set; }
        public string HigsCode { get; set; }

        public virtual Currency Currency { get; internal set; }
        public virtual ICollection<RatePlanCommission> RatePlanCommissionList { get; set; }
        public virtual ICollection<RatePlanCompanyClient> RatePlanCompanyClientList { get; set; }
        public virtual ICollection<RatePlanRoomType> RatePlanRoomTypeList { get; set; }
    }
}
