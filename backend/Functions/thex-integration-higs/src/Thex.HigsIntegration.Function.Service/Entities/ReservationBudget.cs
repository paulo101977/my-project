﻿using System;

namespace Thex.HigsIntegration.Function.Service.Entities
{
    public class ReservationBudget : ThexMultiTenantFullAuditedEntity
    {
        public long Id { get; set; }
        public Guid ReservationBudgetUid { get; internal set; }
        public long ReservationItemId { get; internal set; }
        public DateTime BudgetDay { get; internal set; }
        public decimal BaseRate { get; internal set; }
        public decimal ChildRate { get; internal set; }
        public int MealPlanTypeId { get; internal set; }
        public decimal RateVariation { get; internal set; }
        public decimal AgreementRate { get; internal set; }
        public decimal ManualRate { get; internal set; }
        public decimal Discount { get; internal set; }
        public Guid CurrencyId { get; internal set; }
        public string CurrencySymbol { get; internal set; }
        public Guid? BillingAccountItemId { get; internal set; }

        public virtual ReservationItem ReservationItem { get; internal set; }
    }
}
