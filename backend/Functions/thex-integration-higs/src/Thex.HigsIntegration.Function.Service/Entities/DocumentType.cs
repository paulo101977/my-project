﻿using System;
using System.Collections.Generic;
using System.Text;
using Thex.Common.Enumerations;

namespace Thex.HigsIntegration.Function.Service.Entities
{
    public class DocumentType
    {
        public DocumentType()
        {
            DocumentList = new HashSet<Document>();
            GuestReservationItemList = new HashSet<GuestReservationItem>();
        }

        public int Id { get; set; }

        private PersonTypeEnum _personType;
        public PersonTypeEnum PersonTypeValue
        {
            get
            {
                _personType = (PersonTypeEnum)Convert.ToChar(PersonTypeValue);
                return _personType;
            }
            internal set
            {
                _personType = value;
                PersonType = ((char)value).ToString();
            }
        }

        public string PersonType
        {
            get;
            internal set;
        }

        public int? CountrySubdivisionId { get; internal set; }
        public string Name { get; internal set; }
        public string StringFormatMask { get; internal set; }
        public string RegexValidationExpression { get; internal set; }
        public bool IsMandatory { get; internal set; }
        public int? HigsCode { get; internal set; }

        public virtual CountrySubdivision CountrySubdivision { get; internal set; }
        public virtual ICollection<GuestRegistration> GuestRegistration { get; internal set; }
        public virtual ICollection<Document> DocumentList { get; internal set; }
        public virtual ICollection<GuestReservationItem> GuestReservationItemList { get; internal set; }
    }
}
