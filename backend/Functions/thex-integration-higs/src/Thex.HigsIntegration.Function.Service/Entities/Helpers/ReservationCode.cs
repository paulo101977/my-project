﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Thex.HigsIntegration.Function.Service.Entities.Helpers
{
    public class ReservationCode
    {
        private const byte TimestampByte = 0;
        private const byte NodeByte = 4;
        private const int BitsToRotate = 5;
        private int _month = 1;
        private int _day = 1;

        private byte[] _timestamp = new byte[5];

        private readonly char[] characters = { '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'X', 'Z' };

        public int Year { get; set; } = 0;
        public int Month
        {
            get { return _month; }
            set
            {
                if (value > 0 && value <= 12)
                {
                    _month = value;
                };
            }
        }
        public int Day
        {
            get { return _day; }
            set
            {
                if (value > 0 && value <= 31)
                {
                    _day = value;
                };
            }
        }

        public byte[] Timestamp
        {
            get => _timestamp;
            set => _timestamp = value;
            //set => Array.Copy(value, 0, _timestamp, 0, _timestamp.Length);
        }
        public byte[] Node { get; set; } = new byte[2];


        public byte[] GetUid()
        {
            byte[] uid = new byte[6];

            // copy timestamp
            Array.Copy(Timestamp, 0, uid, TimestampByte, Math.Min(4, Timestamp.Length));

            // copy node
            Array.Copy(Node, 0, uid, NodeByte, Math.Min(2, Node.Length));

            return uid;
        }

        private byte[] ShiftRight(byte[] value, int bitcount)
        {
            byte[] temp = new byte[value.Length];
            if (bitcount >= 8)
            {
                Array.Copy(value, 0, temp, bitcount / 8, temp.Length - (bitcount / 8));
            }
            else
            {
                Array.Copy(value, temp, temp.Length);
            }
            if (bitcount % 8 != 0)
            {
                for (int i = temp.Length - 1; i >= 0; i--)
                {
                    temp[i] >>= bitcount % 8;
                    if (i > 0)
                    {
                        temp[i] |= (byte)(temp[i - 1] << 8 - bitcount % 8);
                    }
                }
            }
            return temp;
        }

        public string GetStringFromByteArray(byte[] bytes)
        {

            var sb = new StringBuilder();
            int rotations = (8 * bytes.Length) / BitsToRotate;
            for (int i = 0; i < rotations; i++)
            {
                int pos = bytes[bytes.Length - 1] & 0x1F;
                char c = characters[pos];
                sb.Append(c);
                bytes = ShiftRight(bytes, BitsToRotate);
            }
            return sb.ToString();
        }

        public string ToString(char? separator, string countryTwoLetterIsoCode)
        {
            string strSeparator = separator != null ? separator.ToString() : "";
            return countryTwoLetterIsoCode.Substring(0, 2) + strSeparator + Year.ToString("D4") + Month.ToString("D2") + Day.ToString("D2") + strSeparator + GetStringFromByteArray(GetUid());
        }
        public override string ToString()
        {
            return GetStringFromByteArray(GetUid());
        }


    }
}
