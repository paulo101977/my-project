﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Thex.HigsIntegration.Function.Service.Entities.Helpers
{
    public class FullDataReservation
    {
        public readonly Channel Channel;
        public readonly RatePlan RatePlan;
        public readonly Currency Currency;
        public readonly Guid? CompanyClientId;
        public readonly bool IsClientFromChannel;
        public readonly Guid RoomLayoutId;
        public readonly int RoomTypeId;
        public readonly int GuestTypeId;
        public readonly PropertyBaseRate PropertyBaseRate;
        public readonly string SystemComments;
        public readonly int MealPlanTypeId;

        public FullDataReservation
            (
             Channel channel,
             RatePlan ratePlan,
             Currency currency,
             Guid? companyClientId,
             bool isClientFromChannel,
             Guid roomLayoutId,
             int roomTypeId,
             int guestTypeId,
             PropertyBaseRate propertyBaseRate,
             string systemComments,
             int mealPlanTypeId
            )
        {
            this.Channel = channel;
            this.RatePlan = ratePlan;
            this.Currency = currency;
            this.CompanyClientId = companyClientId;
            this.IsClientFromChannel = isClientFromChannel;
            this.RoomLayoutId = roomLayoutId;
            this.RoomTypeId = roomTypeId;
            this.GuestTypeId = guestTypeId;
            this.PropertyBaseRate = propertyBaseRate;
            this.SystemComments = systemComments;
            this.MealPlanTypeId = mealPlanTypeId;
        }
    }
}
