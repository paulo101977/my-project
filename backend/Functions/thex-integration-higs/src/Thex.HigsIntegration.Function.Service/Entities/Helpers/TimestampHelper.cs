﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Thex.HigsIntegration.Function.Service.Entities.Helpers
{
    public static class TimestampHelper
    {
        public static readonly DateTimeOffset UnixStart;
        public static readonly long MaxUnixSeconds;
        public static readonly long MaxUnixMilliseconds;
        public static readonly long MaxUnixMicroseconds;

        public const long TicksInOneMicrosecond = 10L;
        public const long TicksInOneMillisecond = 10000L;
        public const long TicksInOneSecond = 10000000L;

        static TimestampHelper()
        {
            UnixStart = new DateTimeOffset(1970, 1, 1, 0, 0, 0, TimeSpan.Zero);
            MaxUnixSeconds = Convert.ToInt64((DateTimeOffset.MaxValue - UnixStart).TotalSeconds);
            MaxUnixMilliseconds = Convert.ToInt64((DateTimeOffset.MaxValue - UnixStart).TotalMilliseconds);
            MaxUnixMicroseconds = Convert.ToInt64((DateTimeOffset.MaxValue - UnixStart).Ticks / TicksInOneMicrosecond);
        }

        /// <summary>
        /// Allows for the use of alternative timestamp providers.
        /// </summary>
        public static Func<DateTimeOffset> UtcNow = () => DateTimePrecise.UtcNowOffset;
    }
}
