﻿using System;
using System.Collections.Generic;
using Thex.Inventory.Domain.Entities;

namespace Thex.HigsIntegration.Function.Service.Entities
{
   public class PropertyBaseRateHeaderHistory : ThexMultiTenantFullAuditedEntity, IEntityGuid
    {
        public Guid Id { get; set; }

        public int PropertyId { get; internal set; }
        public DateTime InitialDate { get; internal set; }
        public DateTime FinalDate { get; internal set; }
        public Guid CurrencyId { get; internal set; }
        public int MealPlanTypeDefaultId { get; internal set; }
        public Guid? RatePlanId { get; internal set; }
        public bool Sunday { get; internal set; }
        public bool Monday { get; internal set; }
        public bool Tuesday { get; internal set; }
        public bool Wednesday { get; internal set; }
        public bool Thursday { get; internal set; }
        public bool Friday { get; internal set; }
        public bool Saturday { get; internal set; }

        public virtual Property Property { get; internal set; }
        public virtual ICollection<PropertyBaseRate> PropertyBaseRateList { get; set; }
        public virtual Currency Currency { get; internal set; }
        public virtual RatePlan RatePlan { get; internal set; }
        public virtual ICollection<PropertyBaseRateHistory> PropertyBaseRateHistoryList { get; set; }

        public PropertyBaseRateHeaderHistory()
        {
            PropertyBaseRateHistoryList = new List<PropertyBaseRateHistory>();
        }
    }
}
