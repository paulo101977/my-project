﻿
using System;
using System.Threading.Tasks;
using Thex.HigsIntegration.Function.Service.Dto;

namespace Thex.HigsIntegration.Function.Service.Application.Interfaces
{
    public interface IContactInformationAppService
    {
        Task CreateAndSaveChangesAsync(Guid personId, GuestInformationHigsDto dto);
        Task UpdateAndSaveChangesAsync(Guid personId, GuestInformationHigsDto dto);
    }
}
