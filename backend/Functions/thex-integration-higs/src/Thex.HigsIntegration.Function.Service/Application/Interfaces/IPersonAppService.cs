﻿
using System;
using System.Threading.Tasks;
using Thex.HigsIntegration.Function.Service.Dto;

namespace Thex.HigsIntegration.Function.Service.Application.Interfaces
{
    public interface IPersonAppService
    {
        Task<Guid> CreateOrUpdatePersonGuestRegistrationAsync(GuestInformationHigsDto dto, ComparativeGuestInformationDto comparativeDto);
    }
}
