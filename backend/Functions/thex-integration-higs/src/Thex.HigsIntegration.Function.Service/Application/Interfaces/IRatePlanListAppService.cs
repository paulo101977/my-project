﻿
using System.Threading.Tasks;

namespace Thex.HigsIntegration.Function.Service.Application.Interfaces
{
    public interface IRatePlanListAppService
    {
        Task ImportAsync(string ratePlanListString);
    }
}
