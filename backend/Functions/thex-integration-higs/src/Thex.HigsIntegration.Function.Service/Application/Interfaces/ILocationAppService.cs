﻿
using System;
using System.Threading.Tasks;
using Thex.Common.Dto;
using Thex.Common.Enumerations;

namespace Thex.HigsIntegration.Function.Service.Application.Interfaces
{
    public interface ILocationAppService
    {
        Task CreateAndSaveChangesAsync(LocationDto locationDto, Guid ownerId);
        Task UpdateAndSaveChangesAsync(LocationDto locationDto, Guid ownerId);
        void SetCountryStateAndCity(LocationDto locationDto, LocationCategoryEnum categoryEnum);
        (int, int, int) CreateCountrySubdivisionTranslateAndGetCityId(LocationDto location, string language);
    }
}
