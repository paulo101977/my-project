﻿
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.Common.Dto;
using Thex.Common.Enumerations;

namespace Thex.HigsIntegration.Function.Service.Application.Interfaces
{
    public interface IDocumentAppService
    {
        Task CreateAndSaveChangesAsync(KeyValuePair<int, string> document, Guid ownerId);
        Task UpdateAndSaveChangesAsync(KeyValuePair<int, string> document, Guid ownerId);
    }
}
