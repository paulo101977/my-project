﻿using System.Threading.Tasks;
using Thex.HigsIntegration.Function.Service.Dto.RatePlanList;
using Thex.HigsIntegration.Function.Service.Entities;

namespace Thex.HigsIntegration.Function.Service.Application.Interfaces
{
    public interface IPropertyBaseRateAppService
    {
        Task CreateAsync(RateAmountHigsDto dto, RatePlan ratePlan);
    }
}
