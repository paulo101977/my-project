﻿
using System.Threading.Tasks;

namespace Thex.HigsIntegration.Function.Service.Application.Interfaces
{
    public interface IGuestRelationAppService
    {
        Task ImportAsync(string guestRelationString);
    }
}
