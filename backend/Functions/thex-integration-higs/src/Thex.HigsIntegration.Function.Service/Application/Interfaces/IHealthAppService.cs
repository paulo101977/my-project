﻿
using System.Threading.Tasks;

namespace Thex.HigsIntegration.Function.Service.Application.Interfaces
{
    public interface IHealthAppService
    {
        Task<bool> Check();
    }
}
