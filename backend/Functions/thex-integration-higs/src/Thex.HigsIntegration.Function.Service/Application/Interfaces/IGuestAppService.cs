﻿
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.Common.Dto;
using Thex.Common.Enumerations;
using Thex.HigsIntegration.Function.Service.Entities;

namespace Thex.HigsIntegration.Function.Service.Application.Interfaces
{
    public interface IGuestAppService
    {
        Task<Guest> CreateAndSaveChangesAsync(Guid personId, string higsCode, int propertyId);
        Task<GuestHigsIntegration> GetAsync(string guestCode, int propertyId);
    }
}
