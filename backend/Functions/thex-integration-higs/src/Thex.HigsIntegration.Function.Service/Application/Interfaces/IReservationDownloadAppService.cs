﻿
using System.Threading.Tasks;
using Microsoft.Azure.WebJobs.Host;

namespace Thex.HigsIntegration.Function.Service.Application.Interfaces
{
    public interface IReservationDownloadAppService
    {
        Task ImportReservationToThexAsync(string reservationString, TraceWriter log);
    }
}
