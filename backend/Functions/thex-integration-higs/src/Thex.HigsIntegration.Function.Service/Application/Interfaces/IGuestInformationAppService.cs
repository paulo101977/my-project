﻿
using System.Threading.Tasks;

namespace Thex.HigsIntegration.Function.Service.Application.Interfaces
{
    public interface IGuestInformationAppService
    {
        Task ImportAsync(string guestInformationString);
    }
}
