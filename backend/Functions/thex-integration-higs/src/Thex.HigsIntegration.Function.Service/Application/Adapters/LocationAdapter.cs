﻿using Newtonsoft.Json;
using System;
using Thex.Common.Dto;
using Thex.GenericLog;
using Thex.HigsIntegration.Function.Service.Entities;
using Thex.Kernel;
using Tnf;
using Tnf.Notifications;

namespace Thex.HigsIntegration.Function.Service.Application.Adapters
{
    public class LocationAdapter : ILocationAdapter
    {
        public INotificationHandler _notificationHandler { get; }
        private readonly IApplicationUser _applicationUser;
        private readonly IGenericLogHandler _genericLog;

        public LocationAdapter(
            INotificationHandler notificationHandler,
           IGenericLogHandler genericLog,
           IApplicationUser applicationUser)
        {
            _notificationHandler = notificationHandler;
            _applicationUser = applicationUser;
            _genericLog = genericLog;
        }

        public Location.Builder CreateMap(LocationDto dto, Guid ownerId)
        {
            var builder = new Location.Builder(_notificationHandler)
                .WithId(dto.Id)
                .WithOwnerId(ownerId)
                .WithLatitude(dto.Latitude)
                .WithLongitude(dto.Longitude)
                .WithStreetName(dto.StreetName)
                .WithStreetNumber(dto.StreetNumber)
                .WithCountryCode(dto.CountryCode)
                .WithAdditionalAddressDetails(dto.AdditionalAddressDetails)
                .WithLocationCategoryId(dto.LocationCategoryId)
                .WithNeighborhood(dto.Neighborhood)
                .WithPostalCode(dto.PostalCode)
                .WithCityId(dto.CityId)
                .WithCountryId(dto.CountryId)
                .WithStateId(dto.StateId)
                .WithCountryCode(dto.CountryCode);

            return builder;
        }
    }
}
