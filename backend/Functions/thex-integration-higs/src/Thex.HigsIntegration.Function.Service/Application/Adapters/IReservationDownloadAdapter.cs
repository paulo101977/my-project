﻿using System.Threading.Tasks;
using Thex.Common.Dto;
using Thex.HigsIntegration.Function.Service.Dto;
using Thex.HigsIntegration.Function.Service.Entities;

namespace Thex.HigsIntegration.Function.Service.Application.Adapters
{
    public interface IReservationDownloadAdapter
    {
        Task<Reservation> MapReservation(ReservationHeaderHigsDto dto);
        ReservationConfirmation MapReservationConfirmation(ReservationHeaderHigsDto dto, Reservation reservation);
        Task<Reservation> MapUpdateReservation(Reservation entity, ReservationHeaderHigsDto dto);
        ReservationConfirmation MapUpdateReservationConfirmation(Reservation reservation, ReservationHeaderHigsDto dto);
    }
}
