﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Thex.HigsIntegration.Function.Service.Dto.RatePlanList;
using Thex.HigsIntegration.Function.Service.Entities;

namespace Thex.HigsIntegration.Function.Service.Application.Adapters
{
    public interface IPropertyBaseRateAdapter
    {
        PropertyBaseRateHeaderHistory CreateBaseRateHeaderHistory(
            RateAmountHigsDto dto,
            RatePlan ratePlan,
            ICollection<int> roomTypeIdList,
            ICollection<int> mealPlanTypeIdList);

        Task CreateBaseRateList(
                    RateAmountHigsDto dto,
                    ICollection<RoomType> roomTypeList,
                    RatePlan ratePlan,
                    List<DateTime> rangeDateList,
                    List<int> roomTypeIdList,
                    Currency currency,
                    Guid headerHistoryId,
                    List<PropertyBaseRate> propertyBaseRateListToAdd,
                    List<PropertyBaseRate> propertyBaseRateListToUpdate,
                    int mealPlanTypeId);
    }
}
