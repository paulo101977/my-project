﻿using System;
using Thex.Common.Enumerations;
using Thex.HigsIntegration.Function.Service.Entities;

namespace Thex.HigsIntegration.Function.Service.Application.Adapters
{
    public interface IContactInformationAdapter
    {
        ContactInformation.Builder CreateMap(ContactInformationTypeEnum typeEnum, string contactInformation, Guid ownerId);
    }
}
