﻿using Newtonsoft.Json;
using System;
using Thex.Common.Dto;
using Thex.GenericLog;
using Thex.HigsIntegration.Function.Service.Entities;
using Thex.Kernel;
using Tnf;
using Tnf.Notifications;

namespace Thex.HigsIntegration.Function.Service.Application.Adapters
{
    public class GuestHigsIntegrationAdapter : IGuestHigsIntegrationAdapter
    {
        public INotificationHandler _notificationHandler { get; }
        private readonly IApplicationUser _applicationUser;
        private readonly IGenericLogHandler _genericLog;

        public GuestHigsIntegrationAdapter(
            INotificationHandler notificationHandler,
           IGenericLogHandler genericLog,
           IApplicationUser applicationUser)
        {
            _notificationHandler = notificationHandler;
            _applicationUser = applicationUser;
            _genericLog = genericLog;
        }

        public GuestHigsIntegration.Builder CreateMap(long guestId, string higsCode, int propertyId)
        {
            var builder = new GuestHigsIntegration.Builder(_notificationHandler)
                .WithId(Guid.Empty)
                .WithGuestId(guestId)
                .WithHigsCode(higsCode)
                .WithPropertyId(propertyId);

            return builder;
        }
    }
}
