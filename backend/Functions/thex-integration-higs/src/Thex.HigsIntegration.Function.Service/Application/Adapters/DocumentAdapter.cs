﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Thex.Common.Enumerations;
using Thex.GenericLog;
using Thex.HigsIntegration.Function.Service.Dto;
using Thex.HigsIntegration.Function.Service.Entities;
using Thex.HigsIntegration.Function.Service.Entities.Helpers;
using Thex.HigsIntegration.Function.Service.Infra.Interfaces;
using Thex.HigsIntegration.Function.Service.Infra.Interfaces.Read;
using Thex.Kernel;
using Tnf;
using Tnf.Notifications;

namespace Thex.HigsIntegration.Function.Service.Application.Adapters
{
    public class DocumentAdapter : IDocumentAdapter
    {
        public INotificationHandler _notificationHandler { get; }
        private readonly IApplicationUser _applicationUser;
        private readonly IGenericLogHandler _genericLog;

        public DocumentAdapter(
            INotificationHandler notificationHandler,
           IGenericLogHandler genericLog,
           IApplicationUser applicationUser)
        {
            _notificationHandler = notificationHandler;
            _applicationUser = applicationUser;
            _genericLog = genericLog;
        }

        public Document.Builder CreateMap(KeyValuePair<int, string> document, Guid ownerId)
        {
            var builder = new Document.Builder(_notificationHandler)
                .WithOwnerId(ownerId)
                .WithDocumentTypeId(document.Key)
                .WithDocumentInformation(document.Value);

            return builder;
        }

    }
}
