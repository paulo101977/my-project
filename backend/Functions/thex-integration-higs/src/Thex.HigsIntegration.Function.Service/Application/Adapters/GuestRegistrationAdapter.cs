﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Thex.Common.Enumerations;
using Thex.GenericLog;
using Thex.HigsIntegration.Function.Service.Dto;
using Thex.HigsIntegration.Function.Service.Entities;
using Thex.HigsIntegration.Function.Service.Entities.Helpers;
using Thex.HigsIntegration.Function.Service.Infra.Interfaces;
using Thex.HigsIntegration.Function.Service.Infra.Interfaces.Read;
using Thex.Kernel;
using Tnf;
using Tnf.Notifications;

namespace Thex.HigsIntegration.Function.Service.Application.Adapters
{
    public class GuestRegistrationAdapter : IGuestRegistrationAdapter
    {
        public INotificationHandler _notificationHandler { get; }
        private readonly IApplicationUser _applicationUser;
        private readonly IGenericLogHandler _genericLog;

        public GuestRegistrationAdapter(
            INotificationHandler notificationHandler,
           IGenericLogHandler genericLog,
           IApplicationUser applicationUser)
        {
            _notificationHandler = notificationHandler;
            _applicationUser = applicationUser;
            _genericLog = genericLog;
        }

        public GuestRegistration.Builder CreateMap(GuestInformationHigsDto dto, ComparativeGuestInformationDto comparativeDto)
        {
            Check.NotNull(dto, nameof(dto));

            var builder = new GuestRegistration.Builder(_notificationHandler)
                .WithId(Guid.NewGuid())
                .WithHigsCode(dto.GetHigsCode())
                .WithFirstName(dto.GivenName)
                .WithLastName(dto.SurName)
                .WithFullName(dto.GetFullName())
                .WithSocialName(dto.GetFullName())
                .WithEmail(dto.Email)
                .WithBirthDate(dto.GetBirthDate())
                .WithGender(dto.GetGender())
                .WithPhoneNumber(dto.PhoneNumber)
                .WithMobilePhoneNumber(dto.CelPhoneNumber)
                .WithDocumentInformation(dto.DocumentNumber)
                .WithArrivingFromText(dto.LastSource)
                .WithNextDestinationText(dto.NextStop)
                //comparative
                .WithPropertyId(comparativeDto.PropertyId)
                .WithPersonId(comparativeDto.PersonId)
                .WithGuestId(comparativeDto.GuestId)
                .WithDocumentTypeId(comparativeDto.ThexDocumentTypeId)
                .WithNationality(comparativeDto.ThexNationalityId)
                .WithPurposeOfTrip(comparativeDto.ThexTravelReasonId)
                .WithArrivingBy(comparativeDto.ThexTransportationTypeId)
                .WithOccupationId(comparativeDto.ThexOccupationId);
                
            return builder;
        }

    }
}
