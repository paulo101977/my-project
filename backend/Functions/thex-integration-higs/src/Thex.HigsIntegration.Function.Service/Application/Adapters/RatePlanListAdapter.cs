﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Thex.Common.Enumerations;
using Thex.HigsIntegration.Function.Service.Dto.RatePlanList;
using Thex.HigsIntegration.Function.Service.Entities;
using Thex.HigsIntegration.Function.Service.Infra.Interfaces.Read;
using Thex.Kernel;

namespace Thex.HigsIntegration.Function.Service.Application.Adapters
{
    public class RatePlanListAdapter : IRatePlanListAdapter
    {
        private readonly IRatePlanReadRepository _ratePlanReadRepository;
        private readonly IApplicationUser _applicationUser;
        private readonly ICurrencyReadRepository _currencyReadRepository;
        private readonly IRoomTypeReadRepository _roomTypeReadRepository;
        private readonly ICompanyClientChannelReadRepository _companyClientChannelReadRepository;
        private readonly IPropertyReadRepository _propertyReadRepository;

        public RatePlanListAdapter(
            IRatePlanReadRepository ratePlanReadRepository,
            IApplicationUser applicationUser,
            ICurrencyReadRepository currencyReadRepository,
            IRoomTypeReadRepository roomTypeReadRepository,
            ICompanyClientChannelReadRepository companyClientChannelReadRepository,
            IPropertyReadRepository propertyReadRepository
            )
        {
            _ratePlanReadRepository = ratePlanReadRepository;
            _applicationUser = applicationUser;
            _currencyReadRepository = currencyReadRepository;
            _roomTypeReadRepository = roomTypeReadRepository;
            _companyClientChannelReadRepository = companyClientChannelReadRepository;
            _propertyReadRepository = propertyReadRepository;
        }

        public async Task<ICollection<RatePlan>> MapCreateOrUpdateRatePlanList(RatePlanAndRateAmountHigsDto dto)
        {
            var ratePlanList = new List<RatePlan>();
            foreach (var ratePlanDto in dto.RatePlan)
                ratePlanList.Add(await MapCreateOrUpdateRatePlan(ratePlanDto));

            return ratePlanList;
        }

        public async Task<ICollection<PropertyBaseRate>> MapCreatePropertyBaseRateList(RatePlanAndRateAmountHigsDto dto, Guid ratePlanId)
        {
            var propertyBaseRateList = new List<PropertyBaseRate>();
            foreach (var rateAmount in dto.RateAmount)
                propertyBaseRateList.Add(await MapCreateOrUpdatePropertyBaseRate(rateAmount));

            return propertyBaseRateList;
        }

        #region PropertyBaseRate
        private async Task<PropertyBaseRate> MapCreateOrUpdatePropertyBaseRate(RateAmountHigsDto dto)
        {
            var propertyBaseRate = new PropertyBaseRate(_applicationUser);
            var ratePlan = await _ratePlanReadRepository.GetByDistributionCodeAsync(dto.StatusApplicationControl.RatePlanCode, int.Parse(_applicationUser.PropertyId));
            propertyBaseRate.Id = Guid.NewGuid();
            propertyBaseRate.CurrencyId = ratePlan.CurrencyId;
            propertyBaseRate.CurrencySymbol = ratePlan.CurrencySymbol;
            propertyBaseRate.PropertyId = ratePlan.PropertyId;
            propertyBaseRate.RatePlanId = ratePlan.Id;
            propertyBaseRate.MealPlanTypeDefault = ratePlan.MealPlanTypeId;
            propertyBaseRate.MealPlanTypeId = ratePlan.MealPlanTypeId;

            return propertyBaseRate;
        }
        #endregion

        #region RatePlan
        private async Task<RatePlan> MapCreateOrUpdateRatePlan(RatePlanHigsDto dto)
        {
            var ratePlan = await _ratePlanReadRepository.GetByDistributionCodeAsync(dto.RatePlanCode, int.Parse(_applicationUser.PropertyId));
            if (ratePlan != null)
                ratePlan = MapUpdateRatePlan(ratePlan, dto);
            else
                ratePlan = await MapCreateRatePlan(dto);

            return ratePlan;
        }

        private async Task<RatePlan> MapCreateRatePlan(RatePlanHigsDto dto)
        {
            var ratePlan = new RatePlan();
            ratePlan.Id = Guid.NewGuid();
            ratePlan.AgreementName = dto.Name;
            ratePlan.Description = dto.Description;
            ratePlan.AgreementTypeId = (int)AgreementTypeEnum.BusinessSale;
            ratePlan.RateTypeId = (int)RatePlanTypeEnum.FlatRate;
            ratePlan.DistributionCode = dto.RatePlanCode;
            ratePlan.StartDate = DateTime.Parse(dto.Start).ToZonedDateTime(_applicationUser.TimeZoneName);
            ratePlan.EndDate = DateTime.Parse(dto.End).ToZonedDateTime(_applicationUser.TimeZoneName);
            ratePlan.IsActive = true;
            ratePlan.RateNet = false;
            ratePlan.MealPlanTypeId = (int)dto.MealsIncluded.MealPlanCode;
            ratePlan.PropertyId = int.Parse(_applicationUser.PropertyId);

            var currency = await _currencyReadRepository.GetCurrencyByCurrencyNameAsync(dto.CurrencyCode);
            if (currency != null)
            {
                ratePlan.CurrencyId = currency.Id;
                ratePlan.CurrencySymbol = currency.Symbol;
            }

            ratePlan.RatePlanCompanyClientList = await GetRatePlanCompanyClient(dto, ratePlan.Id);

            if (dto.IsCommissionable)
                ratePlan.RatePlanCommissionList = await GetRatePlanComissionList(dto, ratePlan.Id);

            return ratePlan;
        }

        private RatePlan MapUpdateRatePlan(RatePlan ratePlan, RatePlanHigsDto dto)
        {
            return ratePlan;
        }

        private async Task<ICollection<RatePlanCompanyClient>> GetRatePlanCompanyClient(RatePlanHigsDto dto, Guid ratePlanId)
        {
            var ratePlanCompanyClientList = new List<RatePlanCompanyClient>();
            foreach (var businessOfSource in dto.ListOfSource)
            {
                foreach (var companyId in businessOfSource.ListCompanyID)
                {
                    ratePlanCompanyClientList.Add(new RatePlanCompanyClient()
                    {
                        Id = Guid.NewGuid(),
                        CompanyClientId = (await _companyClientChannelReadRepository.GetCompanyClientIdByCompanyIdAndChannelCodeAsync(
                            businessOfSource.SourceOfBusiness, companyId.CompanyId, _applicationUser.TenantId)).Value,
                        RatePlanId = ratePlanId
                    });
                }
            }

            return ratePlanCompanyClientList.GroupBy(rp => rp.CompanyClientId).Select(grp => grp.FirstOrDefault()).ToList();
        }

        private async Task<ICollection<RatePlanCommission>> GetRatePlanComissionList(RatePlanHigsDto dto, Guid ratePlanId)
        {
            var ratePlanComission = new RatePlanCommission();
            var billingItemId = await _propertyReadRepository.GetParameterByPropertyIdAsync(int.Parse(_applicationUser.PropertyId), (int)ApplicationParameterEnum.ParameterDaily);

            ratePlanComission.RatePlanId = ratePlanId;
            ratePlanComission.Id = Guid.NewGuid();
            ratePlanComission.CommissionPercentualAmount = dto.ValueComission;
            ratePlanComission.BillingItemId = int.Parse(billingItemId);

            return new List<RatePlanCommission> { ratePlanComission };
        }
        #endregion
    }
}
