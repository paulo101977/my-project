﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.HigsIntegration.Function.Service.Dto.RatePlanList;
using Thex.HigsIntegration.Function.Service.Entities;

namespace Thex.HigsIntegration.Function.Service.Application.Adapters
{
    public interface IRatePlanListAdapter
    {
        Task<ICollection<RatePlan>> MapCreateOrUpdateRatePlanList(RatePlanAndRateAmountHigsDto dto);
        Task<ICollection<PropertyBaseRate>> MapCreatePropertyBaseRateList(RatePlanAndRateAmountHigsDto dto, Guid ratePlanId);
    }
}
