﻿using System;
using Thex.HigsIntegration.Function.Service.Dto;
using Thex.HigsIntegration.Function.Service.Entities;

namespace Thex.HigsIntegration.Function.Service.Application.Adapters
{
    public interface IGuestHigsIntegrationAdapter
    {
        GuestHigsIntegration.Builder CreateMap(long guestId, string higsCode, int propertyId);
    }
}
