﻿using Newtonsoft.Json;
using System;
using Thex.Common.Dto;
using Thex.GenericLog;
using Thex.HigsIntegration.Function.Service.Entities;
using Thex.Kernel;
using Tnf;
using Tnf.Notifications;

namespace Thex.HigsIntegration.Function.Service.Application.Adapters
{
    public class GuestAdapter : IGuestAdapter
    {
        public INotificationHandler _notificationHandler { get; }
        private readonly IApplicationUser _applicationUser;
        private readonly IGenericLogHandler _genericLog;

        public GuestAdapter(
            INotificationHandler notificationHandler,
           IGenericLogHandler genericLog,
           IApplicationUser applicationUser)
        {
            _notificationHandler = notificationHandler;
            _applicationUser = applicationUser;
            _genericLog = genericLog;
        }

        public Guest.Builder CreateMap(Guid personId)
        {
            var builder = new Guest.Builder(_notificationHandler)
                .WithPersonId(personId);

            return builder;
        }
    }
}
