﻿using System;
using Thex.Common.Dto;
using Thex.HigsIntegration.Function.Service.Entities;

namespace Thex.HigsIntegration.Function.Service.Application.Adapters
{
    public interface IPersonAdapter
    {
        Person.Builder CreateMap(string firstName, string lastName, int? countrySubdivisionId, DateTime? birthDate, string personType);
        Person.Builder UpdateMap(Person person, string firstName, string lastName, int? countrySubdivisionId, DateTime? birthDate, string personType);
    }
}
