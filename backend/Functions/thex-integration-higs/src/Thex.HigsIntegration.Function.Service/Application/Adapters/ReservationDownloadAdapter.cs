﻿using Microsoft.Azure.WebJobs.Host;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Thex.Common.Dto;
using Thex.Common.Enumerations;
using Thex.GenericLog;
using Thex.HigsIntegration.Function.Service.Dto;
using Thex.HigsIntegration.Function.Service.Entities;
using Thex.HigsIntegration.Function.Service.Entities.Helpers;
using Thex.HigsIntegration.Function.Service.Infra.Interfaces;
using Thex.HigsIntegration.Function.Service.Infra.Interfaces.Read;
using Thex.Kernel;

namespace Thex.HigsIntegration.Function.Service.Application.Adapters
{
    public class ReservationDowloadAdapter : IReservationDownloadAdapter
    {
        private readonly IRoomTypeReadRepository _roomTypeReadRepository;
        private readonly IRoomLayoutReadRepository _roomLayoutReadRepository;
        private readonly ICurrencyReadRepository _currencyReadRepository;
        private readonly IChannelReadRepository _channelReadRepository;
        private readonly IRatePlanReadRepository _ratePlanReadRepository;
        private readonly IIntegrationPartnerPropertyReadRepository _integrationPartnerPropertyReadRepository;
        private readonly ICompanyClientChannelReadRepository _companyClientChannelReadRepository;
        private readonly IApplicationUser _applicationUser;
        private readonly IGuestReservationItemRepository _guestReservationRepository;
        private readonly IPropertyGuestTypeReadRepository _propertyGuestTypeReadRepository;
        private readonly IReservationBudgetRepository _reservationBudgetRepository;
        private readonly IGenericLogHandler _genericLog;
        private readonly IPropertyBaseRateReadRepository _propertyBaseRateReadRepository;
        private readonly IPropertyParameterReadRepository _propertyParameterReadRepository;

        public ReservationDowloadAdapter(
           IRoomTypeReadRepository roomTypeReadRepository,
           IRoomLayoutReadRepository roomLayoutReadRepository,
           ICurrencyReadRepository currencyReadRepository,
           IChannelReadRepository channelReadRepository,
           IRatePlanReadRepository ratePlanReadRepository,
           IGuestReservationItemRepository guestReservationRepository,
           IPropertyBaseRateReadRepository propertyBaseRateReadRepository,
           IIntegrationPartnerPropertyReadRepository integrationPartnerPropertyReadRepository,
           ICompanyClientChannelReadRepository companyClientChannelReadRepository,
           IReservationBudgetRepository reservationBudgetRepository,
           IPropertyGuestTypeReadRepository propertyGuestTypeReadRepository,
           IPropertyParameterReadRepository propertyParameterReadRepository,
           IGenericLogHandler genericLog,
           IApplicationUser applicationUser)
        {
            _roomTypeReadRepository = roomTypeReadRepository;
            _roomLayoutReadRepository = roomLayoutReadRepository;
            _currencyReadRepository = currencyReadRepository;
            _channelReadRepository = channelReadRepository;
            _ratePlanReadRepository = ratePlanReadRepository;
            _integrationPartnerPropertyReadRepository = integrationPartnerPropertyReadRepository;
            _companyClientChannelReadRepository = companyClientChannelReadRepository;
            _applicationUser = applicationUser;
            _guestReservationRepository = guestReservationRepository;
            _propertyGuestTypeReadRepository = propertyGuestTypeReadRepository;
            _reservationBudgetRepository = reservationBudgetRepository;
            _genericLog = genericLog;
            _propertyBaseRateReadRepository = propertyBaseRateReadRepository;
            _propertyParameterReadRepository = propertyParameterReadRepository;
        }

        #region Create Reservation
        public async Task<Reservation> MapReservation(ReservationHeaderHigsDto dto)
        {
            var fullDataReservation = await ValidateAndGetDataReservation(dto);
            if (fullDataReservation == null) return null;

            var reservation = new Reservation();
            reservation.PropertyId = int.Parse(_applicationUser.PropertyId);
            reservation.TenantId = _applicationUser.TenantId;
            reservation.ContactName = $"{dto.RequesDto.MainGuest.GivenName} {dto.RequesDto.MainGuest.Surename}";
            reservation.ContactEmail = dto.RequesDto.MainGuest.Email;
            reservation.ContactPhone = dto.RequesDto.MainGuest.Telephone;
            reservation.ReservationUid = Guid.NewGuid();
            reservation.PartnerComments = new StringBuilder().Append(fullDataReservation.SystemComments).Append(dto.RequesDto.Comment).ToString();
            reservation.ReservationCode = ReservationCodeGeneratorHelper.GenerateTimeBasedShortGuid().ToString();
            reservation.BusinessSourceId = fullDataReservation.Channel.BusinessSourceId;
            reservation.CompanyClientId = fullDataReservation.CompanyClientId;

            reservation.RatePlanId = fullDataReservation.RatePlan?.Id;
            reservation.MarketSegmentId = fullDataReservation.Channel.MarketSegmentId;

            reservation.ReservationItemList = await MapReservationItem(dto, reservation, fullDataReservation);
            return reservation;
        }

        private async Task<ICollection<ReservationItem>> MapReservationItem(ReservationHeaderHigsDto dto, Reservation reservation, FullDataReservation fullDataReservation)
        {
            var checkinAndCheckoutHours = await _propertyParameterReadRepository.GetCheckinAndCheckoutHourByPropertyId(int.Parse(_applicationUser.PropertyId));

            var reservationItem = new ReservationItem();
            reservationItem.ReservationId = reservation.Id;
            reservationItem.ReservationItemCode = $"{reservation.ReservationCode}-001";
            reservationItem.ReservationItemUid = Guid.NewGuid();
            reservationItem.EstimatedArrivalDate = dto.RequesDto.Checkin.Add(checkinAndCheckoutHours.Item1);
            reservationItem.EstimatedDepartureDate = dto.RequesDto.Checkout.Add(checkinAndCheckoutHours.Item2);
            reservationItem.AdultCount = Convert.ToByte(dto.RequesDto.GuestCount);
            reservationItem.ExternalReservationNumber = dto.ReservationNumberHigs;
            reservationItem.PartnerReservationNumber = dto.ReservationNumber;
            reservationItem.ChildCount = Convert.ToByte(dto.RequesDto.NumberChildren);
            reservationItem.ReceivedRoomTypeId = reservationItem.RequestedRoomTypeId = fullDataReservation.RoomTypeId;
            reservationItem.RoomLayoutId = fullDataReservation.RoomLayoutId;
            reservationItem.ReservationItemStatusId = (int)ReservationStatus.Confirmed;
            reservationItem.CurrencyId = fullDataReservation.Currency.Id;
            reservationItem.RatePlanId = fullDataReservation.RatePlan?.Id;
            reservationItem.MealPlanTypeId = fullDataReservation.MealPlanTypeId;
            reservationItem.IsMigrated = true;

            reservationItem.ReservationBudgetList = MapReservationBudgetList(dto, reservationItem, reservation.PropertyId, fullDataReservation.Currency.Symbol, fullDataReservation.PropertyBaseRate);
            reservationItem.GuestReservationItemList = await MapGuestReservationItem(dto, reservation, reservationItem.EstimatedArrivalDate, reservationItem.EstimatedDepartureDate);

            return new List<ReservationItem>() { reservationItem };
        }

        private async Task<ICollection<GuestReservationItem>> MapGuestReservationItem(ReservationHeaderHigsDto dto, Reservation reservation, DateTime estimatedArrivalDate, DateTime estimatedDepartureDate)
        {
            var guestReservationItemList = new List<GuestReservationItem>();
            var guestTypeIdDefault = await _propertyGuestTypeReadRepository.GetPropertyGuestTypeDefaultAsync(int.Parse(_applicationUser.PropertyId));
            guestReservationItemList.Add(new GuestReservationItem()
            {
                IsChild = false,
                IsMain = true,
                GuestName = $"{dto.RequesDto.MainGuest.GivenName} {dto.RequesDto.MainGuest.Surename}",
                EstimatedArrivalDate = estimatedArrivalDate,
                EstimatedDepartureDate = estimatedDepartureDate,
                GuestTypeId = guestTypeIdDefault,
                GuestStatusId = (int)ReservationStatus.Confirmed,
                PropertyId = reservation.PropertyId
            });

            for (var i = 0; i < dto.RequesDto.GuestCount - 1; i++)
            {
                var fullName = dto.RequesDto.Guests != null && dto.RequesDto.Guests.Count > i ? $"{dto.RequesDto.Guests[i].GivenName} {dto.RequesDto.Guests[i].LastName}" : string.Empty;
                guestReservationItemList.Add(new GuestReservationItem()
                {
                    IsChild = false,
                    IsMain = false,
                    GuestName = fullName,
                    EstimatedArrivalDate = dto.RequesDto.Checkin,
                    EstimatedDepartureDate = dto.RequesDto.Checkout,
                    GuestTypeId = guestTypeIdDefault,
                    GuestStatusId = (int)ReservationStatus.Confirmed,
                    PropertyId = reservation.PropertyId
                });
            }

            for (int i = 0; i < dto.RequesDto.NumberChildren; i++)
            {
                guestReservationItemList.Add(new GuestReservationItem()
                {
                    IsChild = true,
                    IsMain = false,
                    EstimatedArrivalDate = dto.RequesDto.Checkin,
                    EstimatedDepartureDate = dto.RequesDto.Checkout,
                    GuestTypeId = guestTypeIdDefault,
                    GuestStatusId = (int)ReservationStatus.Confirmed,
                    PropertyId = reservation.PropertyId
                });
            }

            return guestReservationItemList;
        }

        private ICollection<ReservationBudget> MapReservationBudgetList(ReservationHeaderHigsDto dto, ReservationItem reservationItem, int propertyId, string symbolCurrency, PropertyBaseRate propertyBaseRate)
        {
            var reservationBudgetList = new List<ReservationBudget>();
            decimal? paxAdultAmount = 0;
            decimal? paxChildAmount = 0;

            if (propertyBaseRate != null)
            {
                paxAdultAmount = (decimal?)(dto.RequesDto.GuestCount <= 20 ? propertyBaseRate[$"Pax{dto.RequesDto.GuestCount}Amount"] : propertyBaseRate.Pax20Amount);
                paxChildAmount = (decimal?)(dto.RequesDto.NumberChildren > 0 ? dto.RequesDto.NumberChildren <= 3 ? propertyBaseRate[$"Child{dto.RequesDto.NumberChildren}Amount"] : propertyBaseRate.PaxAdditionalAmount : null);
            }

            decimal totalAmount = 0;
            foreach (var date in dto.RequesDto.Rates.Select(x => x.EffectiveDate))
            {
                var manualRate = dto.RequesDto.Rates.FirstOrDefault(r => r.EffectiveDate.Date == date.Date)?.Amount;
                totalAmount += manualRate ?? 0;
                reservationBudgetList.Add(new ReservationBudget()
                {
                    AgreementRate = manualRate ?? 0,
                    BaseRate = propertyBaseRate != null ? paxAdultAmount ?? default(int) : manualRate ?? 0,
                    ManualRate = manualRate ?? 0,
                    Discount = 0,
                    ChildRate = propertyBaseRate != null ? paxChildAmount ?? default(int) : manualRate ?? 0,
                    RateVariation = 0,
                    BudgetDay = date,
                    CurrencyId = reservationItem.CurrencyId.Value,
                    CurrencySymbol = symbolCurrency,
                    ReservationItemId = reservationItem.Id,
                    MealPlanTypeId = reservationItem.MealPlanTypeId.Value,
                    ReservationBudgetUid = Guid.NewGuid()
                });
            }

            if (dto.RequesDto.Taxes != null && dto.RequesDto.Taxes.Count > 0 && (totalAmount == dto.RequesDto.TotalAmountBeforeTax || Math.Abs(totalAmount - dto.RequesDto.TotalAmountBeforeTax) < 1))
                this.IncrementTaxAmountInBudget(ref reservationBudgetList, dto.RequesDto.Taxes);

            return reservationBudgetList;
        }

        private void IncrementTaxAmountInBudget(ref List<ReservationBudget> reservationBudgetList, List<Thex.Common.Dto.TaxesHigsDto> taxesList)
        {
            reservationBudgetList.LastOrDefault().ManualRate += taxesList.Sum(x => x.Value);
        }

        public ReservationConfirmation MapReservationConfirmation(ReservationHeaderHigsDto dto, Reservation reservation)
        {
            var reservationConfirmation = new ReservationConfirmation();
            
            reservationConfirmation.EnsuresNoShow = false;
            reservationConfirmation.ReservationId = reservation.Id;

            if(dto.RequesDto.TypePayment == TypePaymentHigsEnum.CreditCard)
                reservationConfirmation.LaunchDaily = true;

            if (dto.RequesDto.TypePayment == TypePaymentHigsEnum.Invoiced)
            {
                if (reservation.CompanyClientId.HasValue)
                {
                    reservationConfirmation.PaymentTypeId = (int)PaymentTypeEnum.TobeBilled;
                    reservationConfirmation.BillingClientId = reservation.CompanyClientId.Value;
                }
                else
                {
                    reservationConfirmation.PaymentTypeId = (int)PaymentTypeEnum.Hotel;
                    reservationConfirmation.BillingClientId = null;
                }
            }
            else
            {
                reservationConfirmation.PaymentTypeId = GetTypePayment(dto.RequesDto.TypePayment.Value);
                reservationConfirmation.BillingClientId = reservation.CompanyClientId;
            }

            return reservationConfirmation;
        }
        #endregion
        
        #region Update Reservation
        public async Task<Reservation> MapUpdateReservation(Reservation entity, ReservationHeaderHigsDto dto)
        {
            var fullDataReservation = await ValidateAndGetDataReservation(dto);
            if (fullDataReservation == null) return null;

            entity.ContactName = $"{dto.RequesDto.MainGuest.GivenName} {dto.RequesDto.MainGuest.Surename}";
            entity.ContactEmail = dto.RequesDto.MainGuest.Email;
            entity.ContactPhone = dto.RequesDto.MainGuest.Telephone;
            entity.PartnerComments = new StringBuilder().Append(fullDataReservation.SystemComments).Append(dto.RequesDto.Comment).ToString();
            entity.BusinessSourceId = fullDataReservation.Channel.BusinessSourceId;
            entity.CompanyClientId = fullDataReservation.CompanyClientId;

            entity.RatePlanId = fullDataReservation.RatePlan?.Id;
            entity.MarketSegmentId = fullDataReservation.Channel.MarketSegmentId;

            entity.ReservationItemList = await MapUpdateReservationItem(entity, dto, fullDataReservation);
            return entity;
        }

        private async Task<ICollection<ReservationItem>> MapUpdateReservationItem(Reservation reservation, ReservationHeaderHigsDto dto, FullDataReservation fullDataReservation)
        {
            var checkinAndCheckoutHours = await _propertyParameterReadRepository.GetCheckinAndCheckoutHourByPropertyId(int.Parse(_applicationUser.PropertyId));

            var reservationItem = reservation.ReservationItemList.FirstOrDefault();
            reservationItem.EstimatedArrivalDate = dto.RequesDto.Checkin.Add(checkinAndCheckoutHours.Item1);
            reservationItem.EstimatedDepartureDate = dto.RequesDto.Checkout.Add(checkinAndCheckoutHours.Item2);
            reservationItem.AdultCount = Convert.ToByte(dto.RequesDto.GuestCount);
            reservationItem.ChildCount = Convert.ToByte(dto.RequesDto.NumberChildren);
            reservationItem.IsMigrated = true;
            reservationItem.ReceivedRoomTypeId = reservationItem.RequestedRoomTypeId = fullDataReservation.RoomTypeId;
            reservationItem.CurrencyId = fullDataReservation.Currency.Id;
            reservationItem.RatePlanId = fullDataReservation.RatePlan?.Id;
            reservationItem.MealPlanTypeId = fullDataReservation.MealPlanTypeId;

            reservationItem.ReservationBudgetList = await MapUpdateReservationBudgetList(dto, reservationItem, reservation.PropertyId, fullDataReservation.Currency.Symbol, fullDataReservation.PropertyBaseRate);
            reservationItem.GuestReservationItemList = await MapUpdateGuestReservationItem(dto, reservationItem.GuestReservationItemList.ToList(), reservation, reservationItem.EstimatedArrivalDate, reservationItem.EstimatedDepartureDate);

            return new List<ReservationItem>() { reservationItem };
        }

        private async Task<ICollection<GuestReservationItem>> MapUpdateGuestReservationItem(ReservationHeaderHigsDto dto, List<GuestReservationItem> guestReservationItemList, Reservation reservation, DateTime estimatedArrivalDate, DateTime estimatedDepartureDate)
        {
            guestReservationItemList = (await MapGuestReservationItem(dto, reservation, estimatedArrivalDate, estimatedDepartureDate)).ToList();
            return guestReservationItemList;
        }

        private async Task<ICollection<ReservationBudget>> MapUpdateReservationBudgetList(ReservationHeaderHigsDto dto, ReservationItem reservationItem, int propertyId, string symbolCurrency, PropertyBaseRate propertyBaseRate)
        {
            await _reservationBudgetRepository.RemoveRangedAsync(reservationItem.ReservationBudgetList);

            return MapReservationBudgetList(dto, reservationItem, propertyId, symbolCurrency, propertyBaseRate);
        }

        public ReservationConfirmation MapUpdateReservationConfirmation(Reservation reservation, ReservationHeaderHigsDto dto)
        {
            var reservationConfirmation = reservation.ReservationConfirmationList.FirstOrDefault();

            if (dto.RequesDto.TypePayment == TypePaymentHigsEnum.CreditCard)
                reservationConfirmation.LaunchDaily = true;

            if (dto.RequesDto.TypePayment == TypePaymentHigsEnum.Invoiced)
            {
                if (reservation.CompanyClientId.HasValue)
                {
                    reservationConfirmation.PaymentTypeId = (int)PaymentTypeEnum.TobeBilled;
                    reservationConfirmation.BillingClientId = reservation.CompanyClientId.Value;
                }
                else
                {
                    reservationConfirmation.PaymentTypeId = (int)PaymentTypeEnum.Hotel;
                    reservationConfirmation.BillingClientId = null;
                }
            }
            else
            {
                reservationConfirmation.PaymentTypeId = GetTypePayment(dto.RequesDto.TypePayment.Value);
                reservationConfirmation.BillingClientId = reservation.CompanyClientId;
            }

            return reservationConfirmation;
        }
        #endregion

        #region Private Methods
        private int GetTypePayment(TypePaymentHigsEnum typePaymentHigsEnum)
        {
            switch (typePaymentHigsEnum)
            {
                case TypePaymentHigsEnum.CreditCard:
                    return (int)Common.Enummerable.Thex.PaymentTypeEnum.CreditCard;
                case TypePaymentHigsEnum.DirectHotel:
                    return (int)Common.Enummerable.Thex.PaymentTypeEnum.Hotel;
                case TypePaymentHigsEnum.Invoiced:
                    return (int)Common.Enummerable.Thex.PaymentTypeEnum.TobeBilled;
                default:
                    return 0;
            }
        }

        private void NotifyDataNotFound(string message, string detailMessage)
        {
            _genericLog.AddLog(_genericLog.DefaultBuilder
            .WithMessage(message)
            .WithDetailedMessage(detailMessage)
            .AsError()
            .Build());
        }

        private async Task<FullDataReservation> ValidateAndGetDataReservation(ReservationHeaderHigsDto dto)
        {
            var request = dto.RequesDto;

            var channel = await _channelReadRepository.GetChannelByChannelCodeAsync(dto.RequesDto.SourceOfBusiness, _applicationUser.TenantId);
            if (channel == null)
            {
                dto.RequesDto = null;
                NotifyDataNotFound($"Channel {request.SourceOfBusiness} não encontrado", JsonConvert.SerializeObject(dto));
                return null;
            }

            var roomTypeId = await _roomTypeReadRepository.GetRoomTypeIdByDistributionCodeAsync(dto.RequesDto.RoomTypeCode, int.Parse(_applicationUser.PropertyId));
            if (roomTypeId == 0)
            {
                dto.RequesDto = null;
                NotifyDataNotFound($"Tipo de UH {request.RoomTypeCode} não encontrado", JsonConvert.SerializeObject(dto));
                return null;
            }

            var currency = await _currencyReadRepository.GetCurrencyByCurrencyNameAsync(dto.RequesDto.CurrencyCode);
            if (currency == null)
            {
                dto.RequesDto = null;
                NotifyDataNotFound($"Moeda {request.CurrencyCode} não encontrada", JsonConvert.SerializeObject(dto));
                return null;
            }

            var roomLayoutId = await _roomLayoutReadRepository.GetRoomLayoutDefaultAsync();
            if (roomLayoutId == Guid.Empty)
            {
                dto.RequesDto = null;
                NotifyDataNotFound("Layout de UH não encontrado", JsonConvert.SerializeObject(dto));
                return null;
            }

            var guestTypeId = await _propertyGuestTypeReadRepository.GetPropertyGuestTypeDefaultAsync(int.Parse(_applicationUser.PropertyId));
            if (guestTypeId == 0)
            {
                dto.RequesDto = null;
                NotifyDataNotFound("Tipo de Hóspede não encontrado", JsonConvert.SerializeObject(dto));
                return null;
            }

            var ratePlan = await _ratePlanReadRepository.GetByDistributionCodeAsync(dto.RequesDto.RatePlanCode, int.Parse(_applicationUser.PropertyId));
            var client = await GetCompanyClient(dto);
            var propertyBaseRate = (await _propertyBaseRateReadRepository.GetAllByPeriod(int.Parse(_applicationUser.PropertyId), new List<int> { roomTypeId },
                    currency.Id, null, dto.RequesDto.Checkin, dto.RequesDto.Checkin, null, null, null))?.FirstOrDefault();

            var mealPlanTypeId = ratePlan != null ? ratePlan.MealPlanTypeId : propertyBaseRate != null ? propertyBaseRate.MealPlanTypeDefault : (int)MealPlanTypeEnum.None;
            var systemComments = GenerateSystemComments(dto, ratePlan, propertyBaseRate, client.Item1, client.Item2);

            return new FullDataReservation(channel, ratePlan, currency, client.Item1, client.Item2,
                        roomLayoutId, roomTypeId, guestTypeId, propertyBaseRate, systemComments, mealPlanTypeId);
        }

        private async Task<(Guid?, bool)> GetCompanyClient(ReservationHeaderHigsDto dto)
        {
            Guid? companyClientId = null;
            bool IsClientFromChannel = false;
            if (dto.RequesDto.CompanyInfo != null)
                companyClientId = await _companyClientChannelReadRepository.GetCompanyClientIdByCompanyIdAndChannelCodeAsync(dto.RequesDto.SourceOfBusiness, dto.RequesDto.CompanyInfo.CompanyId, _applicationUser.TenantId);

            if (!companyClientId.HasValue)
            {
                companyClientId = await _companyClientChannelReadRepository.GetCompanyClientIdByCompanyIdAndChannelCodeAsync(dto.RequesDto.SourceOfBusiness, dto.RequesDto.SourceOfBusiness, _applicationUser.TenantId);
                IsClientFromChannel = companyClientId.HasValue;
            }

            return (companyClientId, IsClientFromChannel);
        }

        private string GenerateSystemComments(ReservationHeaderHigsDto dto, RatePlan ratePlan, PropertyBaseRate propertyBaseRate, Guid? companyClientId, bool isClientFromChannel)
        {
            var systemComments = new StringBuilder();
            systemComments.AppendLine();

            GenerateTaxComments(systemComments, dto);

            GenerateRatePlanComments(systemComments, dto, propertyBaseRate, ratePlan);

            GenerateCompanyClientComments(systemComments, dto, companyClientId, isClientFromChannel);

            if (dto.RequesDto.TypePayment == TypePaymentHigsEnum.Invoiced && !companyClientId.HasValue)
                systemComments.Append("- Esta reserva teve sua confirmação alterada de FATURAMENTO para DIRETO NO HOTEL pois NÃO possuía cliente não hora da migração.");

            if (!string.IsNullOrWhiteSpace(systemComments.ToString()))
            {
                systemComments.Insert(0, $"Observações de migração:");
                systemComments.AppendLine();
                systemComments.AppendLine();
            }

            if (!string.IsNullOrWhiteSpace(dto.RequesDto.Comment))
                systemComments.AppendLine("Observações HIGS:");

            return systemComments.ToString();
        }

        private void GenerateTaxComments(StringBuilder systemComments, ReservationHeaderHigsDto dto)
        {
            var totalAmount = dto.RequesDto.Rates.Sum(x => x.Amount);
            if (dto.RequesDto.Taxes != null && dto.RequesDto.Taxes.Count > 0 && (totalAmount == dto.RequesDto.TotalAmountBeforeTax || Math.Abs(totalAmount - dto.RequesDto.TotalAmountBeforeTax) < 1))
            {
                systemComments.Append($"Valor da taxa adicionada na diária do dia: {dto.RequesDto.Rates.LastOrDefault().EffectiveDate.Date} - Valor original: {dto.RequesDto.Rates.LastOrDefault().Amount}");
                systemComments.AppendLine();
                systemComments.Append($"Taxas: ");

                foreach (var tax in dto.RequesDto.Taxes)
                    systemComments.Append($"{tax.CodeTax.ToString() + " " + tax.Description + " " + tax.Value}");
            }
        }

        private void GenerateRatePlanComments(StringBuilder systemComments, ReservationHeaderHigsDto dto, PropertyBaseRate propertyBaseRate, RatePlan ratePlan)
        {
            if (ratePlan == null && propertyBaseRate != null)
                systemComments.AppendLine($"- Não foi encontrado um acordo comercial com o código de distribuição {dto.RequesDto.RatePlanCode}. A reserva foi migrada com a tarifa base.");
            else if (ratePlan == null && propertyBaseRate == null)
                systemComments.AppendLine($"- Não foi encontrado um acordo comercial com o código de distribuição {dto.RequesDto.RatePlanCode} e nenhuma tarifa base. A reserva foi migrada com o tipo de pensão: Sem pensão.");
        }

        private void GenerateCompanyClientComments(StringBuilder systemComments, ReservationHeaderHigsDto dto, Guid? companyClientId, bool isClientFromChannel)
        {
            if (!companyClientId.HasValue || (isClientFromChannel && dto.RequesDto.CompanyInfo != null))
            {
                systemComments.AppendLine();
                if (isClientFromChannel && dto.RequesDto.CompanyInfo != null)
                    systemComments.Append($"- Não foi encontrado um cliente com o companyId {dto.RequesDto.CompanyInfo.CompanyId}.");
                else
                {
                    if (dto.RequesDto.CompanyInfo != null)
                        systemComments.Append($"- Não foi encontrado um cliente com companyId {dto.RequesDto.CompanyInfo.CompanyId} ou {dto.RequesDto.SourceOfBusiness}.");
                    else
                        systemComments.Append($"- Não foi encontrado um cliente com o companyId {dto.RequesDto.SourceOfBusiness}.");
                }
            }
        }
        #endregion
    }
}
