﻿using System;
using Thex.HigsIntegration.Function.Service.Dto;
using Thex.HigsIntegration.Function.Service.Entities;

namespace Thex.HigsIntegration.Function.Service.Application.Adapters
{
    public interface IGuestAdapter
    {
        Guest.Builder CreateMap(Guid personId);
    }
}
