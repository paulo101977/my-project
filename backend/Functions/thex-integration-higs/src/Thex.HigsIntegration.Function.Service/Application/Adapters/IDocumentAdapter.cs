﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.HigsIntegration.Function.Service.Dto;
using Thex.HigsIntegration.Function.Service.Entities;

namespace Thex.HigsIntegration.Function.Service.Application.Adapters
{
    public interface IDocumentAdapter
    {
        Document.Builder CreateMap(KeyValuePair<int, string> document, Guid ownerId);
        
    }
}
