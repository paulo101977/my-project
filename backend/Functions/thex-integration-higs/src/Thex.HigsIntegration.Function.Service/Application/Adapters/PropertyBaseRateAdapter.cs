﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Thex.Common.Enumerations;
using Thex.HigsIntegration.Function.Service.Dto.RatePlanList;
using Thex.HigsIntegration.Function.Service.Entities;
using Thex.HigsIntegration.Function.Service.Enumerations.RatePlanList;
using Thex.HigsIntegration.Function.Service.Infra.Interfaces;
using Thex.HigsIntegration.Function.Service.Infra.Interfaces.Read;
using Thex.Kernel;

namespace Thex.HigsIntegration.Function.Service.Application.Adapters
{
    public class PropertyBaseRateAdapter : IPropertyBaseRateAdapter
    {
        private readonly IRoomTypeReadRepository _roomTypeReadRepository;
        private readonly IApplicationUser _applicationUser;
        private readonly ICurrencyReadRepository _currencyReadRepository;
        private readonly IPropertyBaseRateReadRepository _propertyBaseRateReadRepository;
        private readonly IPropertyBaseRateRepository _propertyBaseRateRepository;
        private readonly IPropertyBaseRateHeaderHistoryRepository _propertyBaseRateHeaderHistoryRepository;

        public PropertyBaseRateAdapter(
            IRoomTypeReadRepository roomTypeReadRepository,
            IApplicationUser applicationUser,
            ICurrencyReadRepository currencyReadRepository,
            IPropertyBaseRateReadRepository propertyBaseRateReadRepository,
            IPropertyBaseRateRepository propertyBaseRateRepository,
            IPropertyBaseRateHeaderHistoryRepository propertyBaseRateHeaderHistoryRepository)
        {
            _roomTypeReadRepository = roomTypeReadRepository;
            _applicationUser = applicationUser;
            _currencyReadRepository = currencyReadRepository;
            _propertyBaseRateReadRepository = propertyBaseRateReadRepository;
            _propertyBaseRateRepository = propertyBaseRateRepository;
            _propertyBaseRateHeaderHistoryRepository = propertyBaseRateHeaderHistoryRepository;
        }

      
        public async Task CreateBaseRateList(
                    RateAmountHigsDto dto,
                    ICollection<RoomType> roomTypeList,
                    RatePlan ratePlan,
                    List<DateTime> rangeDateList,
                    List<int> roomTypeIdList,
                    Currency currency,
                    Guid headerHistoryId,
                    List<PropertyBaseRate> propertyBaseRateListToAdd,
                    List<PropertyBaseRate> propertyBaseRateListToUpdate,
                    int mealPlanTypeId)
        {

            //obtem as tarifas que serão editadas
            var propertyBaseRate = await _propertyBaseRateReadRepository.GetAll(mealPlanTypeId, currency.Id, rangeDateList, roomTypeIdList, ratePlan.Id);


            //criar objetos de criação
            foreach (var date in rangeDateList)
            {
                //varrer cada linha
                foreach (var roomTypeId in roomTypeIdList)
                {
                    var propertyBaseRateToUpdate = propertyBaseRate.SingleOrDefault(e => e.RoomTypeId == roomTypeId && e.Date.Date == date);

                    if (propertyBaseRateToUpdate == null)
                        propertyBaseRateListToAdd.Add(
                            CreatePropertyBaseRate(dto, date, roomTypeId, ratePlan, currency, headerHistoryId));
                    else
                        propertyBaseRateListToUpdate.Add(
                            CreatePropertyBaseRate(propertyBaseRateToUpdate, dto, date, roomTypeId, ratePlan, currency, headerHistoryId));
                }
            }
        }

        public PropertyBaseRateHeaderHistory CreateBaseRateHeaderHistory(
           RateAmountHigsDto dto,
           RatePlan ratePlan,
           ICollection<int> roomTypeIdList,
           ICollection<int> mealPlanTypeIdList)
        {
            var propertyBaseRateHeaderHistory = new PropertyBaseRateHeaderHistory
            {
                Id = Guid.NewGuid(),
                PropertyId = ratePlan.PropertyId,
                InitialDate = ratePlan.StartDate,
                FinalDate = ratePlan.EndDate.Value,
                CurrencyId = ratePlan.CurrencyId,
                MealPlanTypeDefaultId = ratePlan.MealPlanTypeId,
                RatePlanId = ratePlan.Id,
                Monday = Convert.ToBoolean(dto.StatusApplicationControl.Mon),
                Tuesday = Convert.ToBoolean(dto.StatusApplicationControl.Tue),
                Wednesday = Convert.ToBoolean(dto.StatusApplicationControl.Wed),
                Thursday = Convert.ToBoolean(dto.StatusApplicationControl.Thu),
                Friday = Convert.ToBoolean(dto.StatusApplicationControl.Fri),
                Saturday = Convert.ToBoolean(dto.StatusApplicationControl.Sat),
                Sunday = Convert.ToBoolean(dto.StatusApplicationControl.Sun),
            };

            foreach (var roomTypeId in roomTypeIdList)
            {
                foreach (var mealPlanTypeId in mealPlanTypeIdList)
                {
                    var propertyBaseRateHistoryDto = CreatePropertyBaseRateHistory(dto, roomTypeId, mealPlanTypeId, ratePlan);

                    propertyBaseRateHeaderHistory.PropertyBaseRateHistoryList.Add(propertyBaseRateHistoryDto);
                }
            }

            return propertyBaseRateHeaderHistory;
        }

        private PropertyBaseRate CreatePropertyBaseRate(
            RateAmountHigsDto dto,
            DateTime date,
            int roomTypeId,
            RatePlan ratePlan,
            Currency currency, 
            Guid? propertyBaseRateHeaderHistoryId)
        {
            var propertyBaseRate = new PropertyBaseRate(_applicationUser)
            {
                Id = Guid.NewGuid(),
                PropertyId = int.Parse(_applicationUser.PropertyId),
                Date = date,
                MealPlanTypeDefault = ratePlan.MealPlanTypeId,
                RoomTypeId = roomTypeId,
                CurrencyId = currency.Id,
                CurrencySymbol = currency.Symbol,
                MealPlanTypeId = ratePlan.MealPlanTypeId,
                RatePlanId = ratePlan.Id,
                AdultMealPlanAmount = 0,
                Level = 0,
                PropertyBaseRateHeaderHistoryId = propertyBaseRateHeaderHistoryId
            };

            var adultBaseAmount = dto.Rate.BaseByGuestAmts.FirstOrDefault(x => x.AgeQualifyingCode == (int)AgeQualifyingCodeEnum.Adult);
            if(adultBaseAmount != null)
            {
                for (int i = 1; i <= adultBaseAmount.NumberOfGuests; i++)
                    propertyBaseRate[$"Pax{i}Amount"] = adultBaseAmount.AmountAfterTax;
            }

            var childUntil2YearsBaseAmount = dto.Rate.BaseByGuestAmts.FirstOrDefault(x => x.AgeQualifyingCode == (int)AgeQualifyingCodeEnum.ChildUntil2Years);
            if (childUntil2YearsBaseAmount != null)
                propertyBaseRate.Child1Amount = childUntil2YearsBaseAmount.AmountAfterTax;

            var childBaseAmount = dto.Rate.BaseByGuestAmts.FirstOrDefault(x => x.AgeQualifyingCode == (int)AgeQualifyingCodeEnum.Child);
            if (childBaseAmount != null)
                propertyBaseRate.Child2Amount = childUntil2YearsBaseAmount.AmountAfterTax;

            var adultAdditionalBaseAmount = dto.AdditionalGuestAmounts.FirstOrDefault(x => x.AgeQualifyingCode == (int)AgeQualifyingCodeEnum.Adult);
            if (adultAdditionalBaseAmount != null)
                propertyBaseRate.PaxAdditionalAmount = adultAdditionalBaseAmount.Amount;

            var childAdditionalBaseAmount = dto.AdditionalGuestAmounts.FirstOrDefault(x => x.AgeQualifyingCode == (int)AgeQualifyingCodeEnum.Child || x.AgeQualifyingCode == (int)AgeQualifyingCodeEnum.ChildUntil2Years);
            if (childAdditionalBaseAmount != null)
                propertyBaseRate.Child3Amount = childAdditionalBaseAmount.Amount;

            return propertyBaseRate;
        }

        private PropertyBaseRate CreatePropertyBaseRate(
            PropertyBaseRate entity,
            RateAmountHigsDto dto,
            DateTime date,
            int roomTypeId,
            RatePlan ratePlan,
            Currency currency,
            Guid? propertyBaseRateHeaderHistoryId)
        {
            var propertyBaseRate = new PropertyBaseRate(_applicationUser)
            {
                Id = entity.Id,
                PropertyId = int.Parse(_applicationUser.PropertyId),
                Date = date,
                MealPlanTypeDefault = ratePlan.MealPlanTypeId,
                RoomTypeId = roomTypeId,
                CurrencyId = currency.Id,
                CurrencySymbol = currency.Symbol,
                MealPlanTypeId = ratePlan.MealPlanTypeId,
                RatePlanId = ratePlan.Id,
                AdultMealPlanAmount = 0,
                Level = 0,
                PropertyBaseRateHeaderHistoryId = propertyBaseRateHeaderHistoryId
            };

            var adultBaseAmount = dto.Rate.BaseByGuestAmts.FirstOrDefault(x => x.AgeQualifyingCode == (int)AgeQualifyingCodeEnum.Adult);
            if (adultBaseAmount != null)
            {
                for (int i = 1; i <= adultBaseAmount.NumberOfGuests; i++)
                    propertyBaseRate[$"Pax{i}Amount"] = adultBaseAmount.AmountAfterTax;
            }

            var childUntil2YearsBaseAmount = dto.Rate.BaseByGuestAmts.FirstOrDefault(x => x.AgeQualifyingCode == (int)AgeQualifyingCodeEnum.ChildUntil2Years);
            if (childUntil2YearsBaseAmount != null)
                propertyBaseRate.Child1Amount = childUntil2YearsBaseAmount.AmountAfterTax;

            var childBaseAmount = dto.Rate.BaseByGuestAmts.FirstOrDefault(x => x.AgeQualifyingCode == (int)AgeQualifyingCodeEnum.Child);
            if (childBaseAmount != null)
                propertyBaseRate.Child2Amount = childUntil2YearsBaseAmount.AmountAfterTax;

            var adultAdditionalBaseAmount = dto.AdditionalGuestAmounts.FirstOrDefault(x => x.AgeQualifyingCode == (int)AgeQualifyingCodeEnum.Adult);
            if (adultAdditionalBaseAmount != null)
                propertyBaseRate.PaxAdditionalAmount = adultAdditionalBaseAmount.Amount;

            var childAdditionalBaseAmount = dto.AdditionalGuestAmounts.FirstOrDefault(x => x.AgeQualifyingCode == (int)AgeQualifyingCodeEnum.Child || x.AgeQualifyingCode == (int)AgeQualifyingCodeEnum.ChildUntil2Years);
            if (childAdditionalBaseAmount != null)
                propertyBaseRate.Child3Amount = childAdditionalBaseAmount.Amount;

            return propertyBaseRate;
        }

        private PropertyBaseRateHistory CreatePropertyBaseRateHistory(
            RateAmountHigsDto dto,
            int roomTypeId,
            int mealPlanTypeId,
            RatePlan ratePlan)
        {
            var propertyBaseRateHistory = new PropertyBaseRateHistory
            {
                Id = Guid.NewGuid(),
                PropertyId = int.Parse(_applicationUser.PropertyId),
                MealPlanTypeDefault = ratePlan.MealPlanTypeId,
                RoomTypeId = roomTypeId,
                CurrencyId = ratePlan.CurrencyId,
                CurrencySymbol = ratePlan.CurrencySymbol,
                MealPlanTypeId = mealPlanTypeId,
                RatePlanId = ratePlan.Id,
                AdultMealPlanAmount = 0,
                Level = 0,
            };

            var adultBaseAmount = dto.Rate.BaseByGuestAmts.FirstOrDefault(x => x.AgeQualifyingCode == (int)AgeQualifyingCodeEnum.Adult);
            if (adultBaseAmount != null)
            {
                for (int i = 1; i <= adultBaseAmount.NumberOfGuests; i++)
                    propertyBaseRateHistory[$"Pax{i}Amount"] = adultBaseAmount.AmountAfterTax;
            }

            var childUntil2YearsBaseAmount = dto.Rate.BaseByGuestAmts.FirstOrDefault(x => x.AgeQualifyingCode == (int)AgeQualifyingCodeEnum.ChildUntil2Years);
            if (childUntil2YearsBaseAmount != null)
                propertyBaseRateHistory.Child1Amount = childUntil2YearsBaseAmount.AmountAfterTax;

            var childBaseAmount = dto.Rate.BaseByGuestAmts.FirstOrDefault(x => x.AgeQualifyingCode == (int)AgeQualifyingCodeEnum.Child);
            if (childBaseAmount != null)
                propertyBaseRateHistory.Child2Amount = childUntil2YearsBaseAmount.AmountAfterTax;

            var adultAdditionalBaseAmount = dto.AdditionalGuestAmounts.FirstOrDefault(x => x.AgeQualifyingCode == (int)AgeQualifyingCodeEnum.Adult);
            if (adultAdditionalBaseAmount != null)
                propertyBaseRateHistory.PaxAdditionalAmount = adultAdditionalBaseAmount.Amount;

            var childAdditionalBaseAmount = dto.AdditionalGuestAmounts.FirstOrDefault(x => x.AgeQualifyingCode == (int)AgeQualifyingCodeEnum.Child || x.AgeQualifyingCode == (int)AgeQualifyingCodeEnum.ChildUntil2Years);
            if (childAdditionalBaseAmount != null)
                propertyBaseRateHistory.Child3Amount = childAdditionalBaseAmount.Amount;

            return propertyBaseRateHistory;
        }
    }
}
