﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Thex.Common.Enumerations;
using Thex.GenericLog;
using Thex.HigsIntegration.Function.Service.Dto;
using Thex.HigsIntegration.Function.Service.Entities;
using Thex.HigsIntegration.Function.Service.Entities.Helpers;
using Thex.HigsIntegration.Function.Service.Infra.Interfaces;
using Thex.HigsIntegration.Function.Service.Infra.Interfaces.Read;
using Thex.Kernel;
using Tnf;
using Tnf.Notifications;

namespace Thex.HigsIntegration.Function.Service.Application.Adapters
{
    public class ContactInformationAdapter : IContactInformationAdapter
    {
        public INotificationHandler _notificationHandler { get; }
        private readonly IApplicationUser _applicationUser;
        private readonly IGenericLogHandler _genericLog;

        public ContactInformationAdapter(
            INotificationHandler notificationHandler,
           IGenericLogHandler genericLog,
           IApplicationUser applicationUser)
        {
            _notificationHandler = notificationHandler;
            _applicationUser = applicationUser;
            _genericLog = genericLog;
        }

        public ContactInformation.Builder CreateMap(ContactInformationTypeEnum typeEnum, string contactInformation, Guid ownerId)
        {
            var builder = new ContactInformation.Builder(_notificationHandler);

            builder
                .WithOwnerId(ownerId)
                .WithContactInformationTypeId((int)typeEnum)
                .WithInformation(contactInformation);

            return builder;
        }
    }
}
