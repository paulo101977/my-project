﻿using Newtonsoft.Json;
using System;
using Thex.Common.Dto;
using Thex.GenericLog;
using Thex.HigsIntegration.Function.Service.Entities;
using Thex.Kernel;
using Tnf;
using Tnf.Notifications;

namespace Thex.HigsIntegration.Function.Service.Application.Adapters
{
    public class PersonAdapter : IPersonAdapter
    {
        public INotificationHandler _notificationHandler { get; }
        private readonly IApplicationUser _applicationUser;
        private readonly IGenericLogHandler _genericLog;

        public PersonAdapter(
            INotificationHandler notificationHandler,
           IGenericLogHandler genericLog,
           IApplicationUser applicationUser)
        {
            _notificationHandler = notificationHandler;
            _applicationUser = applicationUser;
            _genericLog = genericLog;
        }

        public Person.Builder CreateMap(string firstName, string lastName, int? countrySubdivisionId, DateTime? birthDate, string personType)
        {
            var builder = new Person.Builder(_notificationHandler)
                .WithId(Guid.Empty)
                .WithFirstName(firstName)
                .WithLastName(lastName)
                .WithPersonType(personType)
                .WithDateOfBirth(birthDate)
                .WithCountrySubdivisionId(countrySubdivisionId);

            return builder;
        }

        public Person.Builder UpdateMap(Person person, string firstName, string lastName, int? countrySubdivisionId, DateTime? birthDate, string personType)
        {
            var builder = new Person.Builder(_notificationHandler, person)
                .WithId(person.Id)
                .WithFirstName(firstName)
                .WithLastName(lastName)
                .WithPersonType(personType)
                .WithDateOfBirth(birthDate)
                .WithCountrySubdivisionId(countrySubdivisionId);

            return builder;
        }
    }
}
