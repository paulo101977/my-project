﻿using Thex.HigsIntegration.Function.Service.Dto;
using Thex.HigsIntegration.Function.Service.Entities;

namespace Thex.HigsIntegration.Function.Service.Application.Adapters
{
    public interface IGuestRegistrationAdapter
    {
        GuestRegistration.Builder CreateMap(GuestInformationHigsDto dto, ComparativeGuestInformationDto comparativeDto);
    }
}
