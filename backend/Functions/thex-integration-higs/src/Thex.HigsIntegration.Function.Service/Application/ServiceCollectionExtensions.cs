﻿using Microsoft.Extensions.DependencyInjection;
using System;
using Thex.Common.Interfaces.Observables;
using Thex.HigsIntegration.Function.Service.Application.Adapters;
using Thex.HigsIntegration.Function.Service.Application.Interfaces;
using Thex.HigsIntegration.Function.Service.Application.Services;
using Thex.HigsIntegration.Function.Service.Infra;
using Thex.Inventory;
using Thex.Inventory.Domain.Services;

namespace Thex.HigsIntegration.Function.Service.Application
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddApplicationDependency(this IServiceCollection services)
        {
            services.AddTnfNotifications();
            services.AddInfraDependency();

            services.AddScoped<IReservationItemChangeStatusObservable, RoomTypeInventoryDomainService>();
            services.AddScoped<IReservationItemChangeRoomTypeOrPeriodObservable, RoomTypeInventoryDomainService>();

            services.AddScoped<IReservationDownloadAdapter, ReservationDowloadAdapter>();
            services.AddScoped<IGuestRegistrationAdapter, GuestRegistrationAdapter>();
            services.AddScoped<IGuestHigsIntegrationAdapter, GuestHigsIntegrationAdapter>();
            services.AddScoped<IGuestAdapter, GuestAdapter>();
            services.AddScoped<IPersonAdapter, PersonAdapter>();
            services.AddScoped<ILocationAdapter, LocationAdapter>();
            services.AddScoped<IContactInformationAdapter, ContactInformationAdapter>(); 
            services.AddScoped<IDocumentAdapter, DocumentAdapter>();
            services.AddScoped<IPropertyBaseRateAdapter, PropertyBaseRateAdapter>();
            services.AddScoped<IRatePlanListAdapter, RatePlanListAdapter>();

            services.AddScoped<IReservationDownloadAppService, ReservationDownloadAppService>();
            services.AddScoped<IGuestInformationAppService, GuestInformationAppService>();
            services.AddScoped<IGuestRelationAppService, GuestRelationAppService>();
            services.AddScoped<ILocationAppService, LocationAppService>();
            services.AddScoped<IRatePlanListAppService, RatePlanListAppService>();
            services.AddScoped<IGuestAppService, GuestAppService>();
            services.AddScoped<IContactInformationAppService, ContactInformationAppService>();
            services.AddScoped<IDocumentAppService, DocumentAppService>();
            services.AddScoped<IPersonAppService, PersonAppService>();
            services.AddScoped<IPropertyBaseRateAppService, PropertyBaseRateAppService>();

            services.AddScoped<IHealthAppService, HealthAppService>();

            return services;
        }
    }
}
