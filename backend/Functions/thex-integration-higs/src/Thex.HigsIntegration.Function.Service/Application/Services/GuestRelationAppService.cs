﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Thex.GenericLog;
using Thex.HigsIntegration.Function.Service.Application.Interfaces;
using Thex.Kernel;
using Tnf.Notifications;
using Tnf.Repositories.Uow;

namespace Thex.HigsIntegration.Function.Service.Application.Services
{
    public class GuestRelationAppService : BaseFunctionAppService, IGuestRelationAppService
    {
        public GuestRelationAppService(
                INotificationHandler notificationHandler,
                IGenericLogHandler genericLog,
                IApplicationUser applicationUser,
                IHttpContextAccessor httpContext,
                IUnitOfWorkManager unitOfWork
            ) : base(notificationHandler, genericLog, applicationUser, httpContext, unitOfWork)
        {
        }

        public async Task ImportAsync(string guestRelationString)
        {
            throw new System.NotImplementedException();
        }
    }
}
