﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using Thex.Common.Enumerations;
using Thex.GenericLog;
using Thex.HigsIntegration.Function.Service.Application.Adapters;
using Thex.HigsIntegration.Function.Service.Application.Interfaces;
using Thex.HigsIntegration.Function.Service.Dto.RatePlanList;
using Thex.HigsIntegration.Function.Service.Entities;
using Thex.HigsIntegration.Function.Service.Infra.Interfaces;
using Thex.HigsIntegration.Function.Service.Infra.Interfaces.Read;
using Thex.Kernel;
using Tnf.Notifications;
using Tnf.Repositories.Uow;

namespace Thex.HigsIntegration.Function.Service.Application.Services
{
    public class RatePlanListAppService : BaseFunctionAppService, IRatePlanListAppService
    {
        private readonly IIntegrationPartnerPropertyReadRepository _integrationPartnerPropertyReadRepository;
        private readonly IPropertyReadRepository _propertyReadRepository;
        private readonly IRatePlanListAdapter _ratePlanListAdapter;
        private readonly IRatePlanRepository _ratePlanRepository;
        private readonly IPropertyBaseRateAdapter _propertyBaseRateAdapter;
        private readonly IPropertyBaseRateAppService _propertyBaseRateAppService;
        private readonly IRatePlanReadRepository _ratePlanReadRepository;

        public RatePlanListAppService(
                INotificationHandler notificationHandler,
                IGenericLogHandler genericLog,
                IApplicationUser applicationUser,
                IHttpContextAccessor httpContext,
                IUnitOfWorkManager unitOfWork,
                IIntegrationPartnerPropertyReadRepository integrationPartnerPropertyReadRepository,
                IPropertyReadRepository propertyReadRepository,
                IRatePlanListAdapter ratePlanListAdapter,
                IPropertyBaseRateAdapter propertyBaseRateAdapter,
                IRatePlanRepository ratePlanRepository,
                IRatePlanReadRepository ratePlanReadRepository,
                IPropertyBaseRateAppService propertyBaseRateAppService
            ) : base(notificationHandler, genericLog, applicationUser, httpContext, unitOfWork)
        {
            _integrationPartnerPropertyReadRepository = integrationPartnerPropertyReadRepository;
            _propertyReadRepository = propertyReadRepository;
            _ratePlanListAdapter = ratePlanListAdapter;
            _ratePlanRepository = ratePlanRepository;
            _propertyBaseRateAdapter = propertyBaseRateAdapter;
            _propertyBaseRateAppService = propertyBaseRateAppService;
            _ratePlanReadRepository = ratePlanReadRepository;
        }

        public async Task ImportAsync(string ratePlanListString)
        {
            var dto = JsonConvert.DeserializeObject<RatePlanAndRateAmountHigsDto>(ratePlanListString);

            if (dto == null) return;

            var property = await _integrationPartnerPropertyReadRepository.GetPropertyByIntegrationCodeAsync(dto.HotelCode);
            if (property == null)
            {
                _genericLog.AddLog(_genericLog.DefaultBuilder
                   .WithMessage("Hotel não encontrado")
                   .WithDetailedMessage(JsonConvert.SerializeObject(dto))
                   .AsError()
                   .Build());

                return;
            }

            await SetApplicationUser(property);

            await CreateOrUpdateRatePlan(dto);

            await CreateOrUpdatePropertyBaseRate(dto);
        }

        private async Task CreateOrUpdateRatePlan(RatePlanAndRateAmountHigsDto dto)
        {
            var ratePlanList = await _ratePlanListAdapter.MapCreateOrUpdateRatePlanList(dto);
            foreach (var ratePlan in ratePlanList)
            {
                await _ratePlanRepository.InsertAndSaveChangesAsync(ratePlan);
            }
        }

        private async Task CreateOrUpdatePropertyBaseRate(RatePlanAndRateAmountHigsDto dto)
        {
            foreach (var rateAmount in dto.RateAmount)
            {
                var ratePlan = await _ratePlanReadRepository.GetByDistributionCodeAsync(rateAmount.StatusApplicationControl.RatePlanCode, int.Parse(_applicationUser.PropertyId));
                await _propertyBaseRateAppService.CreateAsync(rateAmount, ratePlan);
            }
        }

        private async Task SetApplicationUser(Property property)
        {
            var timezoneName = await _propertyReadRepository.GetParameterByPropertyIdAsync(property.Id, (int)ApplicationParameterEnum.ParameterTimeZone);
            _applicationUser.SetProperties(Environment.GetEnvironmentVariable("HigsTokenApplication"), property.TenantId.ToString(), property.Id.ToString(), timezoneName);
        }

        private async Task SendConfirmationToHigs(List<Guid> ratePlanIdList, List<Guid> rateAmountIdList)
        {
            var body = JsonConvert.SerializeObject(new
            {
                RatePlan = ratePlanIdList,
                RateAmount = rateAmountIdList
            });

            HttpClient httpClient = new HttpClient();
            httpClient.DefaultRequestHeaders.Add("Token-Client", Environment.GetEnvironmentVariable("HigsTokenClient"));
            httpClient.DefaultRequestHeaders.Add("Token-Application", Environment.GetEnvironmentVariable("HigsTokenApplication"));
            httpClient.DefaultRequestHeaders.Add("Accept-Encoding", "gzip,deflate");

            var response = await httpClient.PostAsync(Environment.GetEnvironmentVariable("HigsConfirmationReservationUri"), new StringContent(body, Encoding.UTF8, "application/json"));
            var responseString = await response.Content.ReadAsStringAsync();
        }
    }
}
