﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Thex.Common.Enumerations;
using Thex.HigsIntegration.Function.Service.Application.Adapters;
using Thex.HigsIntegration.Function.Service.Application.Interfaces;
using Thex.HigsIntegration.Function.Service.Dto.RatePlanList;
using Thex.HigsIntegration.Function.Service.Entities;
using Thex.HigsIntegration.Function.Service.Infra.Interfaces;
using Thex.HigsIntegration.Function.Service.Infra.Interfaces.Read;
using Thex.Kernel;

namespace Thex.HigsIntegration.Function.Service.Application.Services
{
    public class PropertyBaseRateAppService : IPropertyBaseRateAppService
    {
        private readonly IRoomTypeReadRepository _roomTypeReadRepository;
        private readonly IApplicationUser _applicationUser;
        private readonly ICurrencyReadRepository _currencyReadRepository;
        private readonly IPropertyBaseRateReadRepository _propertyBaseRateReadRepository;
        private readonly IPropertyBaseRateRepository _propertyBaseRateRepository;
        private readonly IPropertyBaseRateHeaderHistoryRepository _propertyBaseRateHeaderHistoryRepository;
        private readonly IPropertyBaseRateAdapter _propertyBaseRateAdapter;

        public PropertyBaseRateAppService(
           IRoomTypeReadRepository roomTypeReadRepository,
           IApplicationUser applicationUser,
           ICurrencyReadRepository currencyReadRepository,
           IPropertyBaseRateReadRepository propertyBaseRateReadRepository,
           IPropertyBaseRateRepository propertyBaseRateRepository,
           IPropertyBaseRateHeaderHistoryRepository propertyBaseRateHeaderHistoryRepository,
           IPropertyBaseRateAdapter propertyBaseRateAdapter)
        {
            _roomTypeReadRepository = roomTypeReadRepository;
            _applicationUser = applicationUser;
            _currencyReadRepository = currencyReadRepository;
            _propertyBaseRateReadRepository = propertyBaseRateReadRepository;
            _propertyBaseRateRepository = propertyBaseRateRepository;
            _propertyBaseRateHeaderHistoryRepository = propertyBaseRateHeaderHistoryRepository;
            _propertyBaseRateAdapter = propertyBaseRateAdapter;
        }

        public async Task CreateAsync(RateAmountHigsDto dto, RatePlan ratePlan)
        {
            var roomTypeIdList = new List<int> { await _roomTypeReadRepository.GetRoomTypeIdByDistributionCodeAsync(dto.StatusApplicationControl.InvTypeCode, int.Parse(_applicationUser.PropertyId)) };
            var rangeDateList = CreateRangeOfDates(dto.StatusApplicationControl);
            var mealPlanTypeIdList = new List<int> { ratePlan.MealPlanTypeId };


            // VERIFICAR
            //if (!mealPlanTypeIdList.Any(m => m.Equals(1)))
                // mealPlanTypeIdList.Add((int)MealPlanTypeEnum.None);

            var headerHistory = _propertyBaseRateAdapter.CreateBaseRateHeaderHistory(dto, ratePlan, roomTypeIdList, mealPlanTypeIdList);
            var headerHistoryId = await _propertyBaseRateHeaderHistoryRepository.Create(headerHistory);
            var currency = await _currencyReadRepository.GetCurrencyByCurrencySymbolAsync(ratePlan.CurrencySymbol);
            var roomTypeList = await _roomTypeReadRepository.GetAllRoomTypesByPropertyIdAsync(int.Parse(_applicationUser.PropertyId));

            List<Task> tasks = new List<Task>();

            var propertyBaseRateListToAdd = new List<PropertyBaseRate>();
            var propertyBaseRateListToUpdate = new List<PropertyBaseRate>();

            // Atualizando o mealplantypedefault de todos os propertybaserates neste periodo no banco
            _propertyBaseRateRepository.GetAndUpdate(ratePlan.PropertyId, roomTypeIdList, currency.Id, ratePlan.StartDate, ratePlan.EndDate.Value, ratePlan.MealPlanTypeId);

            foreach (var mealPlanTypeId in mealPlanTypeIdList)
            {
                tasks.Add(_propertyBaseRateAdapter.CreateBaseRateList(
                 dto,
                 roomTypeList,
                 ratePlan,
                 rangeDateList,
                 roomTypeIdList,
                 currency,
                 headerHistoryId,
                 propertyBaseRateListToAdd,
                 propertyBaseRateListToUpdate,
                 mealPlanTypeId));
            }

            await Task.WhenAll(tasks);

            _propertyBaseRateRepository.CreateAndUpdate(propertyBaseRateListToAdd, propertyBaseRateListToUpdate);

        }

        private List<DateTime> CreateRangeOfDates(StatusApplicationControlHigsDto dto)
        {
            //monto as datas que serão salvas
            var startDate = DateTime.Parse(dto.Start).Date;
            var finalDate = DateTime.Parse(dto.End).Date;
            var rangeDates = new List<DateTime>();

            var daysOfWeek = new List<int>
           {
               dto.Mon,
               dto.Tue,
               dto.Wed,
               dto.Thu,
               dto.Fri,
               dto.Sat,
               dto.Sun
           };

            while (startDate <= finalDate)
            {
                if (daysOfWeek.Contains((int)startDate.DayOfWeek))
                    rangeDates.Add(startDate);

                startDate = startDate.AddDays(1);
            }

            return rangeDates;
        }
    }
}
