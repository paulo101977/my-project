﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Thex.Common;
using Thex.Common.Dto;
using Thex.Common.Enumerations;
using Thex.GenericLog;
using Thex.HigsIntegration.Function.Service.Application.Adapters;
using Thex.HigsIntegration.Function.Service.Application.Interfaces;
using Thex.HigsIntegration.Function.Service.Dto;
using Thex.HigsIntegration.Function.Service.Entities;
using Thex.HigsIntegration.Function.Service.Infra.Interfaces;
using Thex.HigsIntegration.Function.Service.Infra.Interfaces.Read;
using Thex.Kernel;
using Thex.Maps.Geocode.Entities.GoogleAddress;
using Thex.Maps.Geocode.ReadInterfaces;
using Tnf.Localization;
using Tnf.Notifications;
using Tnf.Repositories.Uow;

namespace Thex.HigsIntegration.Function.Service.Application.Services
{
    public class ContactInformationAppService : BaseFunctionAppService, IContactInformationAppService
    {
        private readonly IContactInformationAdapter _contactInformationAdapter;
        private readonly IContactInformationRepository _contactInformationRepository;
        
        public ContactInformationAppService(
                INotificationHandler notificationHandler,
                IGenericLogHandler genericLog,
                IApplicationUser applicationUser,
                IHttpContextAccessor httpContext,
                IUnitOfWorkManager unitOfWork,
                IContactInformationAdapter contactInformationAdapter,
                IContactInformationRepository contactInformationRepository
            ) : base(notificationHandler, genericLog, applicationUser, httpContext, unitOfWork)
        {
            _contactInformationAdapter = contactInformationAdapter;
            _contactInformationRepository = contactInformationRepository;
        }

        public async Task CreateAndSaveChangesAsync(Guid personId, GuestInformationHigsDto dto)
        {
            var contactInformationList = GetContactInformationListFromDto(personId, dto);

            if (Notification.HasNotification())
                return;

            await _contactInformationRepository.AddRangeAndSaveChangesAsync(contactInformationList);
        }

        public async Task UpdateAndSaveChangesAsync(Guid personId, GuestInformationHigsDto dto)
        {
            var contactInformationList = GetContactInformationListFromDto(personId, dto);

            if (Notification.HasNotification())
                return;

            await _contactInformationRepository.UpdateRangeAndSaveChangesAsync(personId, contactInformationList);
        }

        private IList<ContactInformation> GetContactInformationListFromDto(Guid personId, GuestInformationHigsDto dto)
        {
            List<ContactInformation> contactInformationList = new List<ContactInformation>();

            if (!String.IsNullOrEmpty(dto.Email))
                contactInformationList.Add(_contactInformationAdapter.CreateMap(ContactInformationTypeEnum.Email, dto.Email, personId).Build());

            if (!String.IsNullOrEmpty(dto.PhoneNumber))
                contactInformationList.Add(_contactInformationAdapter.CreateMap(ContactInformationTypeEnum.PhoneNumber, dto.PhoneNumber, personId).Build());

            if (!String.IsNullOrEmpty(dto.CelPhoneNumber))
                contactInformationList.Add(_contactInformationAdapter.CreateMap(ContactInformationTypeEnum.CellPhoneNumber, dto.CelPhoneNumber, personId).Build());

            return contactInformationList;
        }
    }
}
