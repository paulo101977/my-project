﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Thex.Common;
using Thex.Common.Dto;
using Thex.Common.Enumerations;
using Thex.GenericLog;
using Thex.HigsIntegration.Function.Service.Application.Adapters;
using Thex.HigsIntegration.Function.Service.Application.Interfaces;
using Thex.HigsIntegration.Function.Service.Dto;
using Thex.HigsIntegration.Function.Service.Entities;
using Thex.HigsIntegration.Function.Service.Infra.Interfaces;
using Thex.HigsIntegration.Function.Service.Infra.Interfaces.Read;
using Thex.Kernel;
using Thex.Maps.Geocode.Entities.GoogleAddress;
using Thex.Maps.Geocode.ReadInterfaces;
using Tnf.Localization;
using Tnf.Notifications;
using Tnf.Repositories.Uow;

namespace Thex.HigsIntegration.Function.Service.Application.Services
{
    public class DocumentAppService : BaseFunctionAppService, IDocumentAppService
    {
        private readonly IDocumentAdapter _documentAdapter;
        private readonly IDocumentRepository _documentRepository;
        
        public DocumentAppService(
                INotificationHandler notificationHandler,
                IGenericLogHandler genericLog,
                IApplicationUser applicationUser,
                IHttpContextAccessor httpContext,
                IUnitOfWorkManager unitOfWork,
                IDocumentAdapter documentAdapter,
                IDocumentRepository documentRepository
            ) : base(notificationHandler, genericLog, applicationUser, httpContext, unitOfWork)
        {
            _documentAdapter = documentAdapter;
            _documentRepository = documentRepository;
        }

        public async Task CreateAndSaveChangesAsync(KeyValuePair<int, string> documentKeyValue, Guid ownerId)
        {
            if (string.IsNullOrEmpty(documentKeyValue.Value))
                return;

            var documentBuilder = _documentAdapter.CreateMap(documentKeyValue, ownerId);
            var document = documentBuilder.Build();

            if (Notification.HasNotification())
                return;

            await _documentRepository.CreateAndSaveChangesAsync(document);
        }

        public async Task UpdateAndSaveChangesAsync(KeyValuePair<int, string> documentKeyValue, Guid ownerId)
        {
            if (string.IsNullOrEmpty(documentKeyValue.Value))
                return;

            var documentBuilder = _documentAdapter.CreateMap(documentKeyValue, ownerId);
            var document = documentBuilder.Build();

            if (Notification.HasNotification())
                return;

            await _documentRepository.UpdateAndSaveChangesAsync(document);
        }
    }
}
