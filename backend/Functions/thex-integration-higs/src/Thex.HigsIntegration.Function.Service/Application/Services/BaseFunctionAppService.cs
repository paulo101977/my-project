﻿using Microsoft.AspNetCore.Http;
using Thex.Common;
using Thex.Common.Enumerations;
using Thex.GenericLog;
using Thex.HigsIntegration.Function.Service.Application.Interfaces;
using Thex.Kernel;
using Tnf.Notifications;
using Tnf.Repositories.Uow;

namespace Thex.HigsIntegration.Function.Service.Application.Services
{
    public class BaseFunctionAppService
    {
        protected INotificationHandler Notification { get; }
        protected readonly IUnitOfWorkManager _unitOfWork;
        protected readonly IApplicationUser _applicationUser;
        protected readonly IGenericLogHandler _genericLog;
        protected readonly IHttpContextAccessor _httpContext;

        public BaseFunctionAppService(
                INotificationHandler notificationHandler,
                IGenericLogHandler genericLog,
                IApplicationUser applicationUser,
                IHttpContextAccessor httpContext,
                IUnitOfWorkManager unitOfWork
            )
        {
            Notification = notificationHandler;
            _unitOfWork = unitOfWork;
            _applicationUser = applicationUser;
            _genericLog = genericLog;
            _httpContext = httpContext;
        }

        protected virtual void NotifyRequired(string field)
        {
            Notification.Raise(Notification
                .DefaultBuilder
                .WithMessage(AppConsts.LocalizationSourceName, CommonsEnum.Error.FieldRequired)
                .WithMessageFormat(field)
                .Build());
        }
    }
}
