﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Thex.Common.Enumerations;
using Thex.GenericLog;
using Thex.HigsIntegration.Function.Service.Application.Adapters;
using Thex.HigsIntegration.Function.Service.Application.Interfaces;
using Thex.HigsIntegration.Function.Service.Dto;
using Thex.HigsIntegration.Function.Service.Entities;
using Thex.HigsIntegration.Function.Service.Infra.Interfaces;
using Thex.HigsIntegration.Function.Service.Infra.Interfaces.Read;
using Thex.Kernel;
using Tnf.Notifications;
using Tnf.Repositories.Uow;

namespace Thex.HigsIntegration.Function.Service.Application.Services
{
    public class PersonAppService : BaseFunctionAppService, IPersonAppService
    {
        private readonly IDocumentAppService _documentAppService;
        private readonly IContactInformationAppService _contactInformationAppService;
        private readonly ILocationAppService _locationAppService;
        private readonly IPersonAdapter _personAdapter;
        private readonly IPersonRepository _personRepository;
        private readonly IPersonReadRepository _personReadRepository;
        private readonly IGuestAppService _guestAppService;

        public PersonAppService(
                INotificationHandler notificationHandler,
                IGenericLogHandler genericLog,
                IApplicationUser applicationUser,
                IHttpContextAccessor httpContext,
                IUnitOfWorkManager unitOfWork,
                IDocumentAppService documentAppService,
                IContactInformationAppService contactInformationAppService,
                ILocationAppService locationAppService,
                IPersonAdapter personAdapter,
                IPersonRepository personRepository,
                IGuestAppService guestAppService,
                IPersonReadRepository personReadRepository
            ) : base(notificationHandler, genericLog, applicationUser, httpContext, unitOfWork)
        {
            _documentAppService = documentAppService;
            _contactInformationAppService = contactInformationAppService;
            _locationAppService = locationAppService;
            _personAdapter = personAdapter;
            _personRepository = personRepository;
            _guestAppService = guestAppService;
            _personReadRepository = personReadRepository;
        }

        public async Task<Guid> CreateOrUpdatePersonGuestRegistrationAsync(GuestInformationHigsDto dto, ComparativeGuestInformationDto comparativeDto)
        {
            if (!comparativeDto.ThexDocumentTypeId.HasValue)
                return Guid.Empty;

            var person = await GetPerson(dto, comparativeDto);

            if (person != null)
                return await UpdatePersonAsync(person, dto, comparativeDto);

            return await CreatePersonAsync(dto, comparativeDto);
        }

        private async Task<Person> GetPerson(GuestInformationHigsDto dto, ComparativeGuestInformationDto comparativeDto)
        {
            var person = await _personReadRepository.GetByDocument(dto.DocumentNumber, comparativeDto.ThexDocumentTypeId.Value);

            if (person == null)
                person = await _personReadRepository.GetByHigsCode(dto.Guest?.Code.ToString(), comparativeDto.PropertyId);

            return person;
        }

        private async Task<Guid> UpdatePersonAsync(Person person, GuestInformationHigsDto dto, ComparativeGuestInformationDto comparativeDto)
        {
            var personId = await this.CreateBuilderAndUpdatePerson(person, dto.GivenName, dto.SurName, comparativeDto.ThexNationalityId, dto.GetBirthDate(), comparativeDto.ThexPersonType);

            if (Notification.HasNotification())
                return default(Guid);

            //update document
            if (!string.IsNullOrEmpty(dto.DocumentNumber) && comparativeDto.ThexDocumentTypeId.HasValue)
                await _documentAppService.UpdateAndSaveChangesAsync(new KeyValuePair<int, string>(comparativeDto.ThexDocumentTypeId.Value, dto.DocumentNumber), personId);

            if (Notification.HasErrorNotification())
                return default(Guid);

            //update contact information
            await _contactInformationAppService.UpdateAndSaveChangesAsync(personId, dto);

            if (Notification.HasNotification())
                return default(Guid);

            //update location
            await _locationAppService.UpdateAndSaveChangesAsync(comparativeDto.location, personId);

            if (Notification.HasNotification())
                return default(Guid);

            var guest = await _guestAppService.CreateAndSaveChangesAsync(personId, dto.Guest?.Code.ToString(), comparativeDto.PropertyId);

            comparativeDto.SetPersonId(personId);
            comparativeDto.SetGuestId(guest.Id);

            return personId;
        }

        private async Task<Guid> CreatePersonAsync(GuestInformationHigsDto dto, ComparativeGuestInformationDto comparativeDto)
        {
            //create person
            var personId = await this.CreatePersonAndGetId(dto.GivenName, dto.SurName, comparativeDto.ThexNationalityId, dto.GetBirthDate(), comparativeDto.ThexPersonType);

            if (Notification.HasNotification())
                return default(Guid);

            //create document
            if (!string.IsNullOrEmpty(dto.DocumentNumber) && comparativeDto.ThexDocumentTypeId.HasValue)
                await _documentAppService.CreateAndSaveChangesAsync(new KeyValuePair<int, string>(comparativeDto.ThexDocumentTypeId.Value, dto.DocumentNumber), personId);

            if (Notification.HasErrorNotification())
                return default(Guid);

            //create contact information
            await _contactInformationAppService.CreateAndSaveChangesAsync(personId, dto);

            if (Notification.HasNotification())
                return default(Guid);

            //create location
            await _locationAppService.CreateAndSaveChangesAsync(comparativeDto.location, personId);

            if (Notification.HasNotification())
                return default(Guid);

            //create guest
            var guest = await _guestAppService.CreateAndSaveChangesAsync(personId, dto.Guest?.Code.ToString(), comparativeDto.PropertyId);

            comparativeDto.SetPersonId(personId);
            comparativeDto.SetGuestId(guest.Id);

            return personId;
        }


        private async Task<Guid> CreatePersonAndGetId(string firstName, string lastName, int? countrySubdivisionId, DateTime? birthDate, string personType)
        {
            var personBuilder = _personAdapter.CreateMap(firstName, lastName, countrySubdivisionId, birthDate, personType);

            var person = personBuilder.Build();

            if (Notification.HasNotification())
                return default(Guid);

            await _personRepository.CreateAndSaveChangesAsync(person);

            return person.Id;
        }


        private async Task<Guid> CreateBuilderAndUpdatePerson(Person person, string firstName, string lastName, int? countrySubdivisionId, DateTime? birthDate, string personType)
        {
            var personBuilder = _personAdapter.UpdateMap(person, firstName, lastName, countrySubdivisionId, birthDate, personType);

            var personObj = personBuilder.Build();

            if (Notification.HasNotification())
                return default(Guid);

            await _personRepository.UpdateAndSaveChangesAsync(personObj);

            return person.Id;
        }
    }
}
