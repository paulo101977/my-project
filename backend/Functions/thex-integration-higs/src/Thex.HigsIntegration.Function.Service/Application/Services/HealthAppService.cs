﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Queue;
using Thex.Common;
using Thex.Common.Dto;
using Thex.Common.Enumerations;
using Thex.GenericLog;
using Thex.HigsIntegration.Function.Service.Application.Adapters;
using Thex.HigsIntegration.Function.Service.Application.Interfaces;
using Thex.HigsIntegration.Function.Service.Dto;
using Thex.HigsIntegration.Function.Service.Entities;
using Thex.HigsIntegration.Function.Service.Infra.Interfaces;
using Thex.HigsIntegration.Function.Service.Infra.Interfaces.Read;
using Thex.Kernel;
using Thex.Maps.Geocode.Entities.GoogleAddress;
using Thex.Maps.Geocode.ReadInterfaces;
using Tnf.Localization;
using Tnf.Notifications;
using Tnf.Repositories.Uow;

namespace Thex.HigsIntegration.Function.Service.Application.Services
{
    public class HealthAppService : BaseFunctionAppService, IHealthAppService
    {
        private readonly IHealthReadRepository _healthReadRepository;

        public HealthAppService(
                INotificationHandler notificationHandler,
                IGenericLogHandler genericLog,
                IApplicationUser applicationUser,
                IHttpContextAccessor httpContext,
                IUnitOfWorkManager unitOfWork,
                IHealthReadRepository healthReadRepository
            ) : base(notificationHandler, genericLog, applicationUser, httpContext, unitOfWork)
        {
            _healthReadRepository = healthReadRepository;
        }

        public async Task<bool> CheckDatabase()
            => await _healthReadRepository.AnyAsync();

        public async Task<bool> CheckStorage()
        {
            try
            {
                var storageAccount = CloudStorageAccount.Parse(Environment.GetEnvironmentVariable("AzureWebJobsStorage"));

                var _queueClient = storageAccount.CreateCloudQueueClient();

                var queue = _queueClient.GetQueueReference(Environment.GetEnvironmentVariable("QueueGuestRelation"));
                await queue.CreateIfNotExistsAsync();

                var peek = await queue.PeekMessageAsync();

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }  
        }

        public async Task<bool> Check()
        {
            var databaseTask = CheckDatabase();
            var storageTask = CheckStorage();

            await Task.WhenAll(new Task[] { databaseTask, storageTask });

            return databaseTask.Result && storageTask.Result;
        }
           
    }
}
