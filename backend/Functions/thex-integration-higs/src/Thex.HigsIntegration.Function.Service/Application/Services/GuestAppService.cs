﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Thex.Common;
using Thex.Common.Dto;
using Thex.Common.Enumerations;
using Thex.GenericLog;
using Thex.HigsIntegration.Function.Service.Application.Adapters;
using Thex.HigsIntegration.Function.Service.Application.Interfaces;
using Thex.HigsIntegration.Function.Service.Dto;
using Thex.HigsIntegration.Function.Service.Entities;
using Thex.HigsIntegration.Function.Service.Infra.Interfaces;
using Thex.HigsIntegration.Function.Service.Infra.Interfaces.Read;
using Thex.Kernel;
using Thex.Maps.Geocode.Entities.GoogleAddress;
using Thex.Maps.Geocode.ReadInterfaces;
using Tnf.Localization;
using Tnf.Notifications;
using Tnf.Repositories.Uow;

namespace Thex.HigsIntegration.Function.Service.Application.Services
{
    public class GuestAppService : BaseFunctionAppService, IGuestAppService
    {
        private readonly IGuestAdapter _guestAdapter;
        private readonly IGuestRepository _guestRepository;
        private readonly IGuestHigsIntegrationAdapter _guestHigsIntegrationAdapter;
        private readonly IGuestHigsIntegrationReadRepository _guestHigsIntegrationReadRepository;
        private readonly IGuestReadRepository _guestReadRepository;

        public GuestAppService(
                INotificationHandler notificationHandler,
                IGenericLogHandler genericLog,
                IApplicationUser applicationUser,
                IHttpContextAccessor httpContext,
                IUnitOfWorkManager unitOfWork,
                IGuestAdapter guestAdapter,
                IGuestRepository guestRepository,
                IGuestHigsIntegrationReadRepository guestHigsIntegrationReadRepository,
                IGuestReadRepository guestReadRepository
            ) : base(notificationHandler, genericLog, applicationUser, httpContext, unitOfWork)
        {
            _guestAdapter = guestAdapter;
            _guestRepository = guestRepository;
            _guestHigsIntegrationReadRepository = guestHigsIntegrationReadRepository;
            _guestReadRepository = guestReadRepository;
        }

        public async Task<Guest> CreateAndSaveChangesAsync(Guid personId, string guestCode, int propertyId)
        {
            var guestbyPersonId = await _guestReadRepository.GetAsync(personId);

            if (guestbyPersonId != null)
                return guestbyPersonId;

            var guestHigs = await _guestHigsIntegrationReadRepository.GetByHigsCodeAsync(guestCode, propertyId);

            if (guestHigs != null && guestHigs.Guest != null)
                return guestHigs.Guest;

            var guestBuilder = _guestAdapter.CreateMap(personId);
            var guest = guestBuilder.Build();

            if (Notification.HasNotification())
                return null;

            return await _guestRepository.CreateAndSaveChangesAsync(guest);
        }

        public async Task<GuestHigsIntegration> GetAsync(string guestCode, int propertyId)
           => await _guestHigsIntegrationReadRepository.GetByHigsCodeAsync(guestCode, propertyId);

        public async Task<Guest> GetAsync(Guid personId)
           => await _guestReadRepository.GetAsync(personId);
    }
}
