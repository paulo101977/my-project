﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using Thex.Common.Dto;
using Thex.Common.Enumerations;
using Thex.GenericLog;
using Thex.HigsIntegration.Function.Service.Application.Adapters;
using Thex.HigsIntegration.Function.Service.Application.Interfaces;
using Thex.HigsIntegration.Function.Service.Dto;
using Thex.HigsIntegration.Function.Service.Entities;
using Thex.HigsIntegration.Function.Service.Infra.Interfaces.Read;
using Thex.Kernel;
using Tnf.Configuration;
using Tnf.Notifications;
using Tnf.Repositories.Uow;

namespace Thex.HigsIntegration.Function.Service.Application.Services
{
    public class GuestInformationAppService : BaseFunctionAppService, IGuestInformationAppService
    {
        protected readonly IIntegrationPartnerPropertyReadRepository _integrationPartnerPropertyReadRepository;
        protected readonly IPropertyReadRepository _propertyReadRepository;

        protected readonly IDocumentTypeReadRepository _documentTypeReadRepository;
        protected readonly INationalityReadRepository _nationalityReadRepository;
        protected readonly IReasonReadRepository _reasonReadRepository;
        protected readonly ITransportationTypeReadRepository _transportationTypeReadRepository;
        protected readonly IGeneralOccupationReadRepository _generalOccupationReadRepository;

        protected readonly IGuestRegistrationAdapter _guestRegistrationAdapter;
        
        protected readonly ILocationAppService _locationAppService;
        protected readonly IPersonAppService _personAppService;
        protected readonly ITnfConfiguration _configuration;
        
        public GuestInformationAppService(
                INotificationHandler notificationHandler,
                IGenericLogHandler genericLog,
                IApplicationUser applicationUser,
                IHttpContextAccessor httpContext,
                IUnitOfWorkManager unitOfWork,
                IIntegrationPartnerPropertyReadRepository integrationPartnerPropertyReadRepository,
                IPropertyReadRepository propertyReadRepository,
                IDocumentTypeReadRepository documentTypeReadRepository,
                INationalityReadRepository nationalityReadRepository,
                IGuestRegistrationAdapter guestRegistrationAdapter,
                IReasonReadRepository reasonReadRepository,
                ITransportationTypeReadRepository transportationTypeReadRepository,
                IGeneralOccupationReadRepository generalOccupationReadRepository,
                ILocationAppService locationAppService,
                ITnfConfiguration configuration,
                IPersonAppService personAppService
            ) : base(notificationHandler, genericLog, applicationUser, httpContext, unitOfWork)
        {
            _integrationPartnerPropertyReadRepository = integrationPartnerPropertyReadRepository;
            _propertyReadRepository = propertyReadRepository;
            _documentTypeReadRepository = documentTypeReadRepository;
            _guestRegistrationAdapter = guestRegistrationAdapter;
            _nationalityReadRepository = nationalityReadRepository;
            _reasonReadRepository = reasonReadRepository;
            _transportationTypeReadRepository = transportationTypeReadRepository;
            _generalOccupationReadRepository = generalOccupationReadRepository;
            _locationAppService = locationAppService;
            _personAppService = personAppService;
            _configuration = configuration;
        }

        public async Task ImportAsync(string guestInformationString)
        {
            var dto = JsonConvert.DeserializeObject<GuestInformationHigsDto>(guestInformationString);

            if (dto == null)
                return;

            var property = await _integrationPartnerPropertyReadRepository.GetPropertyByIntegrationCodeAsync(dto.HotelCode.ToString());
            if (property == null)
            {
                _genericLog.AddLog(_genericLog.DefaultBuilder
                   .WithMessage("Hotel não encontrado")
                   .WithDetailedMessage(JsonConvert.SerializeObject(dto))
                   .AsError()
                   .Build());

                return;
            }

            using (var uow = _unitOfWork.Begin())
            {
                await SetApplicationUser(property);

                await CreateOrUpdateGuestRegistration(dto);

                await uow.CompleteAsync();
            }
        }

        private async Task SetApplicationUser(Property property)
        {
            var timezoneName = await _propertyReadRepository.GetParameterByPropertyIdAsync(property.Id, (int)ApplicationParameterEnum.ParameterTimeZone);
            _applicationUser.SetProperties(Environment.GetEnvironmentVariable("HigsTokenApplication"), property.TenantId.ToString(), property.Id.ToString(), timezoneName);
        }

        private async Task CreateOrUpdateGuestRegistration(GuestInformationHigsDto dto)
        {
            var comparativeDto = await GetComparativeGuestInformationDto(dto);

            comparativeDto.SetPropertyId(Convert.ToInt32(_applicationUser.PropertyId));
            comparativeDto.SetLocation(GetAndSetLocation(dto));

            await _personAppService.CreateOrUpdatePersonGuestRegistrationAsync(dto, comparativeDto);

            //verificar se o Locator ID existe
            //caso exista atualizar o existente
            //caso não exista criar um novo

            //criar o location do guestregistration

            var guestMapper = _guestRegistrationAdapter.CreateMap(dto, comparativeDto);

            //acertar o GET do ChainId
            //acertar o Nationality Code 
            //verificar as razões

            var guest = guestMapper.Build();
        }

        private LocationDto GetAndSetLocation(GuestInformationHigsDto dto)
        {
            var locationDto = dto.GetLocation();

            if (locationDto != null)
                _locationAppService.SetCountryStateAndCity(locationDto, LocationCategoryEnum.Residential);

            return locationDto;
        }

        private async Task<ComparativeGuestInformationDto> GetComparativeGuestInformationDto(GuestInformationHigsDto dto)
        {
            var guestDocumentType = await _documentTypeReadRepository.GetByHigsCodeAsync((int)dto.DocumentType);
            var nationality = await _nationalityReadRepository.GetByIsoCodeAsync(dto.NationalityCode ?? "BR");
            var travelReason = await _reasonReadRepository.GetByHigsCodeAsync((int)dto.TravelReason, 1, (int)ReasonCategoryEnum.PurposeOfTrip);
            var transportationType = await _transportationTypeReadRepository.GetByHigsCodeAsync((int)dto.Transport);
            var occupation = await _generalOccupationReadRepository.GetByNameAsync(dto.Profession);

            return new ComparativeGuestInformationDto
            {
                ThexDocumentTypeId = guestDocumentType?.Id,
                ThexNationalityId = nationality?.Id,
                ThexTravelReasonId = travelReason?.Id,
                ThexTransportationTypeId = transportationType?.Id,
                ThexOccupationId = occupation?.Id,
                ThexPersonType = guestDocumentType?.PersonType,
                
            };
        }









    }
}
