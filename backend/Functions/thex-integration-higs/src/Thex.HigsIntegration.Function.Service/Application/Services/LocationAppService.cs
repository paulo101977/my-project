﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Thex.Common;
using Thex.Common.Dto;
using Thex.Common.Enumerations;
using Thex.GenericLog;
using Thex.HigsIntegration.Function.Service.Application.Adapters;
using Thex.HigsIntegration.Function.Service.Application.Interfaces;
using Thex.HigsIntegration.Function.Service.Dto;
using Thex.HigsIntegration.Function.Service.Entities;
using Thex.HigsIntegration.Function.Service.Infra.Interfaces;
using Thex.HigsIntegration.Function.Service.Infra.Interfaces.Read;
using Thex.Kernel;
using Thex.Maps.Geocode.Entities.GoogleAddress;
using Thex.Maps.Geocode.ReadInterfaces;
using Tnf.Localization;
using Tnf.Notifications;
using Tnf.Repositories.Uow;

namespace Thex.HigsIntegration.Function.Service.Application.Services
{
    public class LocationAppService : BaseFunctionAppService, ILocationAppService
    {
        private readonly ICountrySubdivisionTranslationReadRepository _countrySubdivisionTranslationReadRepository;
        private readonly IAddressMapsGeocodeReadRepository _addressMapsGeocodeReadRepository;
        private readonly ICountrySubdivisionRepository _countrySubdivisionRepository;
        private readonly ICountrySubdivisionTranslationRepository _countrySubdivisionTranslationRepository;
        private readonly ILocalizationManager _localizationManager;
        private readonly ILocationAdapter _locationAdapter;
        private readonly ILocationRepository _locationRepository;

        public LocationAppService(
                INotificationHandler notificationHandler,
                IGenericLogHandler genericLog,
                IApplicationUser applicationUser,
                IHttpContextAccessor httpContext,
                IUnitOfWorkManager unitOfWork,

                ICountrySubdivisionRepository countrySubdivisionRepository,
                ICountrySubdivisionTranslationReadRepository countrySubdivisionTranslationReadRepository,
                IAddressMapsGeocodeReadRepository addressMapsGeocodeReadRepository,
                ICountrySubdivisionTranslationRepository countrySubdivisionTranslationRepository,
                ILocalizationManager localizationManager,
                ILocationAdapter locationAdapter,
                ILocationRepository locationRepository
            ) : base(notificationHandler, genericLog, applicationUser, httpContext, unitOfWork)
        {
            _countrySubdivisionTranslationReadRepository = countrySubdivisionTranslationReadRepository;
            _addressMapsGeocodeReadRepository = addressMapsGeocodeReadRepository;
            _countrySubdivisionRepository = countrySubdivisionRepository;
            _countrySubdivisionTranslationRepository = countrySubdivisionTranslationRepository;
            _localizationManager = localizationManager;
            _locationAdapter = locationAdapter;
            _locationRepository = locationRepository;
        }

        public async Task CreateAndSaveChangesAsync(LocationDto locationDto, Guid ownerId)
        {
            if (locationDto == null)
                return;

            var locationBuilder = _locationAdapter.CreateMap(locationDto, ownerId);
            var location = locationBuilder.Build();

            if (Notification.HasNotification())
                return;

            await _locationRepository.CreateAndSaveChangesAsync(location);
        }

        public async Task UpdateAndSaveChangesAsync(LocationDto locationDto, Guid ownerId)
        {
            if (locationDto == null)
                return;

            var locationBuilder = _locationAdapter.CreateMap(locationDto, ownerId);
            var location = locationBuilder.Build();

            if (Notification.HasNotification())
                return;

            await _locationRepository.UpdateAndSaveChangesAsync(location, ownerId);
        }

        public void SetCountryStateAndCity(LocationDto locationDto, LocationCategoryEnum categoryEnum)
        {
            if (locationDto == null)
                return;

            locationDto.LocationCategoryId = (int)categoryEnum;
            var countrySubdivision = CreateCountrySubdivisionTranslateAndGetCityId(locationDto, locationDto.BrowserLanguage);
            locationDto.CityId = countrySubdivision.Item1;
            locationDto.StateId = countrySubdivision.Item2;
            locationDto.CountryId = countrySubdivision.Item3;
            locationDto.Id = 0;
        }

        public virtual (int, int, int) CreateCountrySubdivisionTranslateAndGetCityId(LocationDto location, string language)
        {
            //validar country code obrigatorio
            if (string.IsNullOrEmpty(location.CountryCode))
            {
                Notification.Raise(Notification
                    .DefaultBuilder
                    .WithMessage(AppConsts.LocalizationSourceName, CountrySubdivisionTranslation.EntityError.CountrySubdivisionTranslationMustHaveCountryCode)
                    .Build());
                return (0, 0, 0);
            }

            language = language.ToLower();
            var locationCountryCode = location.CountryCode.ToLower();
            var searchLanguages = new List<string>
                {
                    { "en-us" },
                    { "pt-br" }
                };

            //se o countrycode == br então linguagem principal = brasil e secondLanguage vira a estados unidos
            if (locationCountryCode == "br")
                searchLanguages.Reverse();

            var address = ParseAddress(location);

            //validar se está dentro das linguagens permitidas do google 
            if (!_addressMapsGeocodeReadRepository.GetValidLanguages().Any(l => l.ToLower() == language))
            {
                Notification.Raise(Notification
                    .DefaultBuilder
                    .WithMessage(AppConsts.LocalizationSourceName, CountrySubdivisionTranslation.EntityError.CountrySubdivisionTranslationInvalidLanguage)
                    .Build());
                return (0, 0, 0);
            }

            //passar a região tb
            var task = _addressMapsGeocodeReadRepository.GetMapsGeocodeResultByAddress(address, language);

            var result = task.Result;

            if (result == null)
            {
                Notification.Raise(Notification
                    .DefaultBuilder
                    .WithMessage(AppConsts.LocalizationSourceName, CountrySubdivisionTranslation.EntityError.CountrySubdivisionTranslationGeocodeApiReturnError)
                    .Build());
                return (0, 0, 0);
            }

            if (result.Count() == 0)
            {
                Notification.Raise(Notification
                    .DefaultBuilder
                    .WithMessage(AppConsts.LocalizationSourceName, CountrySubdivisionTranslation.EntityError.CountrySubdivisionTranslationGeocodeApiReturnZeroResults)
                    .Build());
                return (0, 0, 0);
            }

            //varrer cada resultado e pegar a melhor escolha
            var geocodeUserSearch = result.First();
            var geocodeUserPlaceIdSearch = geocodeUserSearch.PlaceId;

            //tradução principal
            var principalGeoCodeResult = searchLanguages.First() == language ? geocodeUserSearch : _addressMapsGeocodeReadRepository.GetMapsGeocodeResultByPlaceId(geocodeUserPlaceIdSearch, searchLanguages.First()).Result;
            var principalGeoCodeIds = CreateCountrySubdivisionDefaultLanguage(principalGeoCodeResult, searchLanguages.First(), locationCountryCode);

            var countryId = principalGeoCodeIds.Where(exp => exp.Key == "country").FirstOrDefault().Value;
            var stateId = principalGeoCodeIds.Where(exp => exp.Key == "state").FirstOrDefault().Value;
            var cityId = principalGeoCodeIds.Where(exp => exp.Key == "city").FirstOrDefault().Value;

            if (cityId <= 0)
            {
                NotifyRequired(_localizationManager.GetString(AppConsts.LocalizationSourceName, LocationEnum.RequiredFields.CityId.ToString()));
            }

            //tradução da 'property'
            var secondGeoCodeResult = searchLanguages.Last() == language ? geocodeUserSearch : _addressMapsGeocodeReadRepository.GetMapsGeocodeResultByPlaceId(geocodeUserPlaceIdSearch, searchLanguages.Last()).Result;
            CreateCountrySubdivisionTranslate(secondGeoCodeResult, searchLanguages.Last(), locationCountryCode, countryId, stateId, cityId);

            //tradução do usuário somente se linguagem passada NÃO está entre uma das principais
            if (!searchLanguages.Contains(language))
                CreateCountrySubdivisionTranslate(geocodeUserSearch, language, locationCountryCode, countryId, stateId, cityId);

            return (cityId, stateId, countryId);
        }


        #region private methods


        private string ParseAddress(LocationDto location)
        {
            var completeAddress = new StringBuilder();

            completeAddress.Append($"{location.StreetName}, {location.StreetNumber}");

            if (!string.IsNullOrEmpty(location.Neighborhood))
                completeAddress.Append($" - {location.Neighborhood}");

            if (!string.IsNullOrEmpty(location.Subdivision))
                completeAddress.Append($", {location.Subdivision}");

            if (!string.IsNullOrEmpty(location.Division))
                completeAddress.Append($" - {location.Division}");

            completeAddress.Append($", {location.PostalCode}");

            if (!string.IsNullOrEmpty(location.Country))
                completeAddress.Append($", {location.Country}");

            return completeAddress.ToString();
        }


        private void CreateCountrySubdivisionTranslate(GoogleAddress googleAddress, string language, string countryIsoCode, int countryId, int stateId, int cityId)
        {
            var levelsIds = new Dictionary<string, int>();

            language = language.ToLower();

            var components = googleAddress.Components;

            #region Get Address Component Levels

            var countryConfig = _addressMapsGeocodeReadRepository.GetAddressComponentConfigByCountryTwoLetterIsoCode(countryIsoCode, components);

            var country = components.FirstOrDefault(exp => exp.Types.Any(d => d == countryConfig.TypeLevel));
            var state = components.FirstOrDefault(exp => exp.Types.Any(d => d == countryConfig.ChildrenLevel.TypeLevel));
            var city = components.FirstOrDefault(exp => exp.Types.Any(d => d == countryConfig.ChildrenLevel.ChildrenLevel.TypeLevel));

            #endregion

            #region Country 

            if (country != null)
            {
                var countryDbResults = _countrySubdivisionTranslationReadRepository.GetCountryWithTranslations(country.ShortName);
                CountrySubdivisionLanguageProcess(levelsIds, language, country, countryDbResults, 1, "country");
            }

            #endregion

            #region State

            if (state != null)
            {
                var stateDbResults = _countrySubdivisionTranslationReadRepository.GetStateWithTranslations(state.LongName, levelsIds.First(exp => exp.Key == "country").Value);

                if (stateDbResults.Count == 0)
                    stateDbResults = _countrySubdivisionTranslationReadRepository.GetStateWithTranslationsByCode(state.ShortName, levelsIds.First(exp => exp.Key == "country").Value);

                if (stateDbResults.Count == 0)
                {
                    Notification.Raise(Notification
                        .DefaultBuilder
                        .WithMessage(AppConsts.LocalizationSourceName, CountrySubdivisionTranslation.EntityError.CountrySubdivisionTranslationStateNameNotEqualGeocodeStateShortName)
                        .Build());
                }

                CountrySubdivisionLanguageProcess(levelsIds, language, state, stateDbResults, 2, "state");
            }

            #endregion

            #region City

            if (city != null)
            {
                var cityDbResults = _countrySubdivisionTranslationReadRepository.GetCityWithTranslations(city.LongName, levelsIds.First(exp => exp.Key == "state").Value);
                CountrySubdivisionLanguageProcess(levelsIds, language, city, cityDbResults, 3, "city");
            }
            #endregion

        }


        private Dictionary<string, int> CreateCountrySubdivisionDefaultLanguage(GoogleAddress googleAddress, string language, string countryIsoCode)
        {
            var levelsIds = new Dictionary<string, int>();

            var components = googleAddress.Components;

            #region Get Address Component Levels

            //preparado para efetuar de forma genérica
            var countryConfig = _addressMapsGeocodeReadRepository.GetAddressComponentConfigByCountryTwoLetterIsoCode(countryIsoCode, components);

            var country = components.FirstOrDefault(exp => exp.Types.Any(d => d == countryConfig.TypeLevel));
            var state = components.FirstOrDefault(exp => exp.Types.Any(d => d == countryConfig.ChildrenLevel.TypeLevel));
            var city = components.FirstOrDefault(exp => exp.Types.Any(d => d == countryConfig.ChildrenLevel.ChildrenLevel.TypeLevel));

            #endregion

            //validar se o countryIsoCode é diferente do resultado!
            if (countryIsoCode.ToLower() != country.ShortName.ToLower())
            {
                Notification.Raise(Notification
                    .DefaultBuilder
                    .WithMessage(AppConsts.LocalizationSourceName, CountrySubdivisionTranslation.EntityError.CountrySubdivisionTranslationCountryCodeNotEqualGeocodeCountryShortName)
                    .Build());
                return null;
            }

            #region Country 

            if (country != null)
            {
                var countryDbResults = _countrySubdivisionTranslationReadRepository.GetCountryWithTranslations(country.ShortName);
                CountrySubdivisionProcess(levelsIds, language, country, countryDbResults, 1, "country");
            }

            #endregion

            #region State

            if (state != null)
            {
                var stateDbResults = _countrySubdivisionTranslationReadRepository.GetStateWithTranslations(state.LongName, levelsIds.First(exp => exp.Key == "country").Value);
                CountrySubdivisionProcess(levelsIds, language, state, stateDbResults, 2, "state", countryIsoCode);
            }

            #endregion

            #region City

            if (city != null)
            {
                var cityDbResults = _countrySubdivisionTranslationReadRepository.GetCityWithTranslations(city.LongName, levelsIds.First(exp => exp.Key == "state").Value);
                CountrySubdivisionProcess(levelsIds, language, city, cityDbResults, 3, "city", countryIsoCode);
            }

            #endregion

            return levelsIds;
        }


        private void CountrySubdivisionLanguageProcess(Dictionary<string, int> levelsIds, string language, GoogleAddressComponent component, List<BaseCountrySubdivisionTranslationDto> countryDbResults, int countrySubdivisionType, string countrySubdivisionLevel)
        {
            var countryDbResult = countryDbResults.FirstOrDefault();
            if (countryDbResult != null)
            {
                if (!countryDbResults.Any(exp => exp.Name.ToLower() == component.LongName.ToLower() && exp.LanguageIsoCode.ToLower() == language))
                    CreateCountrySubdivisionTranslation(language, countryDbResult.Id, component.LongName);

                SetCountrySubdivisionLevelsIds(levelsIds, countrySubdivisionLevel, countryDbResult.Id);
            }
        }

        private void CountrySubdivisionProcess(Dictionary<string, int> levelsIds, string language, GoogleAddressComponent component, List<BaseCountrySubdivisionTranslationDto> countryDbResults, int countrySubdivisionType, string countrySubdivisionLevel, string countryTwoLetterIsoCode = null)
        {
            if (countryDbResults == null || countryDbResults.Count == 0)
                CreateCountrySubdivision(levelsIds, language, component, countrySubdivisionLevel, countrySubdivisionType, countryTwoLetterIsoCode);
            else
            {
                var countryDbResult = countryDbResults.FirstOrDefault();
                if (countryDbResult != null)
                {

                    if (!countryDbResults.Any(exp => exp.Name.ToLower() == component.LongName.ToLower() && exp.LanguageIsoCode.ToLower() == language))
                        CreateCountrySubdivisionTranslation(language, countryDbResult.Id, component.LongName);

                    SetCountrySubdivisionLevelsIds(levelsIds, countrySubdivisionLevel, countryDbResult.Id);
                }
            }
        }

        private void CreateCountrySubdivision(Dictionary<string, int> levelsIds, string language, GoogleAddressComponent component, string countrySubdivisionLevel, int countrySubdivisionType, string countryTwoLetterIsoCode = null)
        {
            var countryBuilder = GetCountrySubdivisionBuilder(levelsIds, language, component, countrySubdivisionLevel, countrySubdivisionType, countryTwoLetterIsoCode);

            var country = countryBuilder.Build();

            if (Notification.HasNotification())
                return;

            var countrySubdivisionId = _countrySubdivisionRepository.InsertAndSaveChanges(country).Id;

            CreateCountrySubdivisionTranslation(language, countrySubdivisionId, component.LongName);

            SetCountrySubdivisionLevelsIds(levelsIds, countrySubdivisionLevel, countrySubdivisionId);
        }

        private void SetCountrySubdivisionLevelsIds(Dictionary<string, int> levelsIds, string countrySubdivisionLevel, int countrySubdivisionId)
        {
            levelsIds.Add(countrySubdivisionLevel, countrySubdivisionId);
        }

        private CountrySubdivision.Builder GetCountrySubdivisionBuilder(Dictionary<string, int> ret, string language, GoogleAddressComponent component, string countrySubdivisionLevel, int countrySubdivisionType, string countryTwoLetterIsoCode = null)
        {
            var countryBuilder = new CountrySubdivision.Builder(Notification);

            countryBuilder
                .WithCountryTwoLetterIsoCode(countryTwoLetterIsoCode != null ? countryTwoLetterIsoCode.ToUpper() : component.ShortName.ToUpper())
                .WithSubdivisionTypeId(countrySubdivisionType);

            switch (countrySubdivisionLevel)
            {
                case "country":
                    countryBuilder.WithTwoLetterIsoCode(component.ShortName.ToUpper());
                    break;
                case "state":
                    countryBuilder.WithParentSubdivisionId(ret.First(x => x.Key == "country").Value);
                    //fix states brazil
                    if (language.ToLower() == "pt-br")
                        countryBuilder.WithTwoLetterIsoCode(component.ShortName.ToUpper());
                    break;
                case "city":
                    countryBuilder.WithParentSubdivisionId(ret.First(x => x.Key == "state").Value);
                    break;
            }

            return countryBuilder;
        }

        private void CreateCountrySubdivisionTranslation(string language, int countrySubdivisionId, string longName)
        {
            CountrySubdivisionTranslation.Builder countryTranslationBuilder = GetCountrySubdivisionTranslationBuilder(language, countrySubdivisionId, longName);

            var countryTranslation = countryTranslationBuilder.Build();

            if (Notification.HasNotification())
                return;

            _countrySubdivisionTranslationRepository.InsertAndSaveChanges(countryTranslation);
        }

        private CountrySubdivisionTranslation.Builder GetCountrySubdivisionTranslationBuilder(string language, int countrySubdivisionId, string longName)
        {
            var countryTranslationBuilder = new CountrySubdivisionTranslation.Builder(Notification);

            countryTranslationBuilder
                .WithCountrySubdivisionId(countrySubdivisionId)
                .WithLanguageIsoCode(language)
                .WithName(longName);
            return countryTranslationBuilder;
        }

        #endregion

    }
}
