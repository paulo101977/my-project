﻿using Microsoft.AspNetCore.Http;
using Microsoft.Azure.WebJobs.Host;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Thex.Common.Dto;
using Thex.Common.Dto.Observable;
using Thex.Common.Enumerations;
using Thex.Common.Interfaces.Observables;
using Thex.GenericLog;
using Thex.HigsIntegration.Function.Service.Application.Adapters;
using Thex.HigsIntegration.Function.Service.Application.Interfaces;
using Thex.HigsIntegration.Function.Service.Dto;
using Thex.HigsIntegration.Function.Service.Entities;
using Thex.HigsIntegration.Function.Service.Infra.Interfaces;
using Thex.HigsIntegration.Function.Service.Infra.Interfaces.Read;
using Thex.Inventory.Domain.Repositories.Interfaces;
using Thex.Inventory.Domain.Services;
using Thex.Kernel;
using Thex.Log.Application.Interfaces;
using Thex.Log.Dto;
using Thex.Log.Dto.ReservationDownload;
using Thex.Log.Infra.Enumerations;
using Tnf.Notifications;
using Tnf.Repositories.Uow;

namespace Thex.HigsIntegration.Function.Service.Application.Services
{
    public class ReservationDownloadAppService : IReservationDownloadAppService
    {
        public INotificationHandler Notification { get; }

        private readonly IUnitOfWorkManager _unitOfWork;
        private readonly IIntegrationPartnerPropertyReadRepository _integrationPartnerPropertyReadRepository;
        private readonly IReservationItemReadRepository _reservationItemReadRepository;
        private readonly IReservationDownloadAdapter _reservationDownloadAdapter;
        private readonly IReservationRepository _reservationRepository;
        private readonly IReservationConfirmationRepository _reservationConfirmationRepository;
        private readonly IApplicationUser _applicationUser;
        private readonly IPropertyReadRepository _propertyReadRepository;
        private readonly IReservationItemRepository _reservationItemRepository;
        private readonly IGuestReservationItemRepository _guestReservationItemRepository;
        private readonly IReservationReadRepository _reservationReadRepository;
        private readonly IIntegrationPartnerPropertyHistoryRepository _integrationPartnerPropertyHistoryRepository;
        private readonly IGenericLogHandler _genericLog;
        private readonly IHttpContextAccessor _httpContext;
        private readonly IRoomTypeReadRepository _roomTypeReadRepository;
        private readonly IReservationDownloadLogAppService _reservationDownloadLogAppService;
        private readonly IProcessingHistoryLogAppService _processingHistoryLogAppService;
        private readonly IChannelReadRepository _channelReadRepository;
        private IList<IReservationItemChangeStatusObservable> _reservationItemChangeStatusObservableList;
        private IList<IReservationItemChangeRoomTypeOrPeriodObservable> _reservationItemChangeRoomTypeOrPeriodObservableList;

        public ReservationDownloadAppService(
                INotificationHandler notificationHandler,
                IReservationDownloadAdapter reservationDownloadAdapter,
                IIntegrationPartnerPropertyReadRepository integrationPartnerPropertyReadRepository,
                IIntegrationPartnerPropertyHistoryRepository integrationPartnerPropertyHistoryRepository,
                IReservationItemReadRepository reservationItemReadRepository,
                IReservationRepository reservationRepository,
                IReservationReadRepository reservationReadRepository,
                IReservationItemRepository reservationItemRepository,
                IRoomTypeReadRepository roomTypeReadRepository,
                IGuestReservationItemRepository guestReservationItemRepository,
                IReservationConfirmationRepository reservationConfirmationRepository,
                IPropertyReadRepository propertyReadRepository,
                IChannelReadRepository channelReadRepository,
                IReservationDownloadLogAppService reservationDownloadLogAppService,
                IProcessingHistoryLogAppService processingHistoryLogAppService,
                IGenericLogHandler genericLog,
                IApplicationUser applicationUser,
                IHttpContextAccessor httpContext,
                IServiceProvider serviceProvider,
                IUnitOfWorkManager unitOfWork
            )
        {
            Notification = notificationHandler;
            _unitOfWork = unitOfWork;
            _integrationPartnerPropertyReadRepository = integrationPartnerPropertyReadRepository;
            _reservationItemReadRepository = reservationItemReadRepository;
            _reservationDownloadAdapter = reservationDownloadAdapter;
            _reservationRepository = reservationRepository;
            _reservationConfirmationRepository = reservationConfirmationRepository;
            _applicationUser = applicationUser;
            _propertyReadRepository = propertyReadRepository;
            _reservationItemRepository = reservationItemRepository;
            _guestReservationItemRepository = guestReservationItemRepository;
            _reservationReadRepository = reservationReadRepository;
            _integrationPartnerPropertyHistoryRepository = integrationPartnerPropertyHistoryRepository;
            _genericLog = genericLog;
            _httpContext = httpContext;
            _roomTypeReadRepository = roomTypeReadRepository;
            _reservationDownloadLogAppService = reservationDownloadLogAppService;
            _processingHistoryLogAppService = processingHistoryLogAppService;
            _channelReadRepository = channelReadRepository;

            SetObservables(serviceProvider);
        }

        public async Task ImportReservationToThexAsync(string reservationString, TraceWriter log)
        {
            var dto = JsonConvert.DeserializeObject<ReservationHeaderHigsDto>(reservationString);
            dto.RequesDto = JsonConvert.DeserializeObject<ReservationHigsDto>(dto.Request);


            log.Info($"irá executar o método: _integrationPartnerPropertyReadRepository.GetPropertyByIntegrationCodeAsync");

            var property = await _integrationPartnerPropertyReadRepository.GetPropertyByIntegrationCodeAsync(dto.RequesDto.IdHotel.ToString());
            if (property == null)
            {
                _genericLog.AddLog(_genericLog.DefaultBuilder
                   .WithMessage("Hotel não encontrado")
                   .WithDetailedMessage(JsonConvert.SerializeObject(dto))
                   .AsError()
                   .Build());

                return;
            }

            log.Info($"irá executar o método: SetApplicationUser");

            await SetApplicationUser(property);

            log.Info($"irá executar o método: _reservationItemReadRepository.GetReservationItemByExternalReservationNumberAsync");

            var reservationItem = await _reservationItemReadRepository.GetReservationItemByExternalReservationNumberAsync(dto.ReservationNumberHigs);
            using (var transaction = _unitOfWork.Begin())
            {
                log.Info($"abriu a transação");

                if ((dto.OperationType == SituationTypeHigsEnum.Insert && reservationItem == null) ||
                   ((dto.OperationType == SituationTypeHigsEnum.Modify || dto.OperationType == SituationTypeHigsEnum.Cancel)
                   && reservationItem != null))
                {
                    switch (dto.OperationType)
                    {
                        case SituationTypeHigsEnum.Insert:
                            log.Info($"caiu no case de inserção SituationTypeHigsEnum.InsertReservationAsync");
                            reservationItem = await InsertReservationAsync(dto);
                            break;
                        case SituationTypeHigsEnum.Cancel:
                            log.Info($"caiu no case de cancelamento SituationTypeHigsEnum.Cancel");
                            await CancelationReservationAsync(dto);
                            break;
                        case SituationTypeHigsEnum.Modify:
                            log.Info($"caiu no case atualização SituationTypeHigsEnum.Modify");
                            await UpdateReservationAsync(dto);
                            break;
                        default:
                            break;
                    }

                    if (_genericLog.HasGenericLog(GenericLogType.Error)) return;

                    log.Info($"irá salvar o histórico: SaveIntegrationPartnerPropertyHistoryAsync");

                    await SaveIntegrationPartnerPropertyHistoryAsync(dto, reservationItem.Id);

                    log.Info($"irá completar a transação: transaction.Complete()");

                    transaction.Complete();
                }
            }

            if ((dto.OperationType == SituationTypeHigsEnum.Modify || dto.OperationType == SituationTypeHigsEnum.Cancel) && reservationItem == null)
            {
                var message = $"Reserva não encontrada para {(dto.OperationType == SituationTypeHigsEnum.Modify ? "modificação" : "cancelamento")}";
                dto.RequesDto = null;
                NotifyDataNotFound(message, JsonConvert.SerializeObject(dto));
                return;
            }

            if (_genericLog.HasGenericLog(GenericLogType.Error)) return;

            log.Info($"irá enviar o log de reservas");

            await SendLogAsync(dto, property.Id, reservationItem.Reservation.ReservationCode);

            log.Info($"irá enviar a confirmação pro HIGS: SendConfirmationToHigs");

            await SendConfirmationToHigs(dto.RequesDto.IdHotel.ToString(), dto.ReservationNumberHigs, reservationItem.Id);
        }

        #region Higs Operation Types
        private async Task<ReservationItem> InsertReservationAsync(ReservationHeaderHigsDto dto)
        {
            ValidateHigsDto(dto);

            var reservation = await _reservationDownloadAdapter.MapReservation(dto);
            if (_genericLog.HasGenericLog(GenericLogType.Error)) return null;

            reservation.Id = (await _reservationRepository.InsertAndSaveChangesAsync(reservation)).Id;
            var reservationConfirmation = _reservationDownloadAdapter.MapReservationConfirmation(dto, reservation);
            if (reservationConfirmation != null)
            {
                reservationConfirmation.ReservationId = reservation.Id;
                await _reservationConfirmationRepository.InsertAndSaveChangesAsync(reservationConfirmation);
            }

            RiseActionsAfterReservationItemChangeStatus(reservation.ReservationItemList.FirstOrDefault(), ReservationStatus.Confirmed);

            return reservation.ReservationItemList.FirstOrDefault();
        }

        private async Task UpdateReservationAsync(ReservationHeaderHigsDto dto)
        {
            ValidateHigsDto(dto);

            var reservationItem = await _reservationItemReadRepository.GetReservationItemByExternalReservationNumberAsync(dto.ReservationNumberHigs);
            if (reservationItem == null || reservationItem.ReservationItemStatusId == (int)ReservationStatus.Canceled
                || reservationItem.ReservationItemStatusId == (int)ReservationStatus.Checkin
                || reservationItem.ReservationItemStatusId == (int)ReservationStatus.Checkout)
                return;

            var reservationItemOld = new ReservationItem()
            {
                RequestedRoomTypeId = reservationItem.RequestedRoomTypeId,
                ReceivedRoomTypeId = reservationItem.ReceivedRoomTypeId,
                EstimatedArrivalDate = reservationItem.EstimatedArrivalDate,
                EstimatedDepartureDate = reservationItem.EstimatedDepartureDate
            };

            var entity = await _reservationReadRepository.GetByExternalReservationNumberAsync(dto.ReservationNumberHigs);

            var reservation = await _reservationDownloadAdapter.MapUpdateReservation(entity, dto);
            if (_genericLog.HasGenericLog(GenericLogType.Error)) return;

            await _reservationRepository.UpdateAndSaveChangesAsync(reservation);
            await _reservationItemRepository.UpdateAsync(reservation.ReservationItemList.FirstOrDefault());

            var reservationConfirmation = _reservationDownloadAdapter.MapUpdateReservationConfirmation(reservation, dto);
            if (reservationConfirmation != null)
                await _reservationConfirmationRepository.UpdateAndSaveChangesAsync(reservationConfirmation);

            if (reservationItem.EstimatedArrivalDate != dto.RequesDto.Checkin ||
               reservationItem.EstimatedDepartureDate != dto.RequesDto.Checkout ||
               reservationItem.RequestedRoomTypeId != reservationItemOld.RequestedRoomTypeId)
                RiseActionsAfterReservationItemChangeRoomTypeOrPeriod(reservationItemOld, reservation.ReservationItemList.FirstOrDefault());
        }

        private async Task CancelationReservationAsync(ReservationHeaderHigsDto dto)
        {
            var reservationItem = await _reservationItemReadRepository.GetReservationItemByExternalReservationNumberAsync(dto.ReservationNumberHigs);
            if (reservationItem == null || reservationItem.ReservationItemStatusId == (int)ReservationStatus.Canceled
                || reservationItem.ReservationItemStatusId == (int)ReservationStatus.Checkin
                || reservationItem.ReservationItemStatusId == (int)ReservationStatus.Checkout)
                return;

            await UpdateReservationItemForCancel(reservationItem);

            RiseActionsAfterReservationItemChangeStatus(reservationItem, ReservationStatus.Canceled, ReservationStatus.Confirmed);
        }
        #endregion

        #region Private Methods
        private async Task SetApplicationUser(Property property)
        {
            var timezoneName = await _propertyReadRepository.GetParameterByPropertyIdAsync(property.Id, (int)ApplicationParameterEnum.ParameterTimeZone);
            _applicationUser.SetProperties(Environment.GetEnvironmentVariable("HigsTokenApplication"), property.TenantId.ToString(), property.Id.ToString(), timezoneName);
        }

        private void ValidateHigsDto(ReservationHeaderHigsDto dto)
        {
            if (dto == null)
            {
                NotifyNullParameter();
                return;
            }

            if (string.IsNullOrWhiteSpace(dto.RequesDto.RatePlanCode))
            {
                NotifyNullParameter();
                return;
            }
        }

        private void NotifyNullParameter()
        {
            Notification.Raise(Notification
                .DefaultBuilder
                .WithMessage("teste", Common.Enummerable.CommonsEnum.NullOrEmptyObject)
                .Build());
        }

        private void NotifyDataNotFound(string message, string detailMessage)
        {
            _genericLog.AddLog(_genericLog.DefaultBuilder
            .WithMessage(message)
            .WithDetailedMessage(detailMessage)
            .AsError()
            .Build());
        }

        private async Task UpdateReservationItemForCancel(ReservationItem reservationItemExisting)
        {
            reservationItemExisting.CancellationDate = DateTime.UtcNow.ToZonedDateTime(_applicationUser.TimeZoneName);
            reservationItemExisting.ReservationItemStatusId = (int)ReservationStatus.Canceled;

            foreach (var guestReservationItem in reservationItemExisting.GuestReservationItemList)
                guestReservationItem.GuestStatusId = (int)ReservationStatus.Canceled;

            await _reservationItemRepository.UpdateAsync(reservationItemExisting);
        }

        private async Task SaveIntegrationPartnerPropertyHistoryAsync(ReservationHeaderHigsDto dto, long reservationItemId)
        {
            dto.RequesDto.CreditCardData = null;
            dto.Request = JsonConvert.SerializeObject(dto.RequesDto);
            await _integrationPartnerPropertyHistoryRepository.InsertAndSaveChangesAsync(new IntegrationPartnerPropertyHistory()
            {
                Response = dto.Request,
                IntegrationOperationType = (int)dto.OperationType,
                IntegrationPartnerPropertyId = await _integrationPartnerPropertyReadRepository.GetByIntegrationCodeAsync(dto.RequesDto.IdHotel.ToString()),
                ReservationItemId = reservationItemId
            });
        }

        private async Task SendConfirmationToHigs(string hotelCode, string reservationNumberHigs, long reservationItemId)
        {
            var body = JsonConvert.SerializeObject(new ReservationHigsConfirmationDto()
            {
                HotelCode = hotelCode,
                HotelReservationIds = new[]
                {
                    new
                    {
                        HigsReservationNumber = reservationNumberHigs,
                        HotelReservationNumber = reservationItemId
                    }
                }
            });

            HttpClient httpClient = new HttpClient();
            httpClient.DefaultRequestHeaders.Add("Token-Client", Environment.GetEnvironmentVariable("HigsTokenClient"));
            httpClient.DefaultRequestHeaders.Add("Token-Application", Environment.GetEnvironmentVariable("HigsTokenApplication"));
            httpClient.DefaultRequestHeaders.Add("Accept-Encoding", "gzip,deflate");

            var response = await httpClient.PostAsync(Environment.GetEnvironmentVariable("HigsConfirmationReservationUri"), new StringContent(body, Encoding.UTF8, "application/json"));
            var responseString = await response.Content.ReadAsStringAsync();
        }

        private async Task SendLogAsync(ReservationHeaderHigsDto dto, int propertyId, string reservationCode)
        {
            var channel = await _channelReadRepository.GetChannelByChannelCodeAsync(dto.RequesDto.SourceOfBusiness, _applicationUser.TenantId);
            if (channel == null) return;

            var RequesDto = new ReservationDownloadRequestDto();
            RequesDto.ReservationHeaderHigsDto = dto;
            RequesDto.PropertyId = propertyId;
            RequesDto.PartnerId = channel.Id.ToString();
            RequesDto.PartnerName = channel.Description;
            RequesDto.BusinessSourceId = channel.BusinessSourceId.ToString();
            RequesDto.BusinessSourceName = channel.BusinessSource.Description;
            RequesDto.ReservationCode = reservationCode;

            var situationList = new List<ReservationDownloadSituationDto>();
            situationList.Add(new ReservationDownloadSituationDto()
            {
                StatusCode = (int)ReservationDownloadSituationEnum.ProcessedReservation,
                Status = true,
                OperationDate = DateTime.UtcNow
            });

            RequesDto.SituationList = situationList;

            await _reservationDownloadLogAppService.SendLogAsync(RequesDto);

            var processingHistory = new ProcessingHistoryDto()
            {
                ProcessingDate = DateTime.UtcNow,
                ProcessingType = (int)ProcessingTypeEnum.ReservationDownload,
                PropertyId = propertyId
            };

            await _processingHistoryLogAppService.SendLogAsync(processingHistory);

        }
        #endregion

        #region Inventory Methods
        private void RiseActionsAfterReservationItemChangeStatus(ReservationItem reservationItem,
            ReservationStatus statusNew, ReservationStatus? statusOld = null)
        {
            foreach (var observable in _reservationItemChangeStatusObservableList)
            {
                var dtoObservable = new ReservationItemChangeStatusObservableDto
                {
                    RoomTypeId = reservationItem.ReceivedRoomTypeId,
                    ReservationItemStatusIdNew = statusNew,
                    ReservationItemStatusIdOld = statusOld,
                    InitialDate = reservationItem.EstimatedArrivalDate,
                    EndDate = reservationItem.EstimatedDepartureDate
                };

                observable.ExecuteActionAfterChangeReservationItemStatus(dtoObservable);
            }
        }

        private void RiseActionsAfterReservationItemChangeRoomTypeOrPeriod(ReservationItem existingReservationItem,
            ReservationItem reservationItem)
        {
            foreach (var observable in _reservationItemChangeRoomTypeOrPeriodObservableList)
            {
                var dtoObservable = new ReservationItemChangeRoomTypeOrPeriodObservableDto
                {
                    OldRoomTypeId = existingReservationItem.ReceivedRoomTypeId,
                    OldInitialDate = existingReservationItem.EstimatedArrivalDate,
                    OldEndDate = existingReservationItem.EstimatedDepartureDate,
                    NewRoomTypeId = reservationItem.ReceivedRoomTypeId,
                    NewInitialDate = reservationItem.EstimatedArrivalDate,
                    NewEndDate = reservationItem.EstimatedDepartureDate,
                };

                observable.ExecuteActionAfterReservationItemChangeRoomTypeOrPeriod(dtoObservable);
            }
        }

        private void SetObservables(IServiceProvider serviceProvider)
        {
            _reservationItemChangeStatusObservableList = new List<IReservationItemChangeStatusObservable>();
            var serviceForReservationItemChangeStatusObservable = serviceProvider.GetServices<IReservationItemChangeStatusObservable>().First(o => o.GetType() == typeof(RoomTypeInventoryDomainService));
            _reservationItemChangeStatusObservableList.Add(serviceForReservationItemChangeStatusObservable);

            _reservationItemChangeRoomTypeOrPeriodObservableList = new List<IReservationItemChangeRoomTypeOrPeriodObservable>();
            var serviceForReservationItemChangeRoomTypeOrPeriodObservable = serviceProvider.GetServices<IReservationItemChangeRoomTypeOrPeriodObservable>().First(o => o.GetType() == typeof(RoomTypeInventoryDomainService));
            _reservationItemChangeRoomTypeOrPeriodObservableList.Add(serviceForReservationItemChangeRoomTypeOrPeriodObservable);
        }
        #endregion

    }
}
