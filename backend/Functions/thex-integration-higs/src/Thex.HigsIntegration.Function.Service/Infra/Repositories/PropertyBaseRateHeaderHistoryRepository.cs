﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Thex.HigsIntegration.Function.Service.Entities;
using Thex.HigsIntegration.Function.Service.Infra.Context;
using Thex.HigsIntegration.Function.Service.Infra.Interfaces;
using Tnf.EntityFrameworkCore;
using Tnf.EntityFrameworkCore.Repositories;

namespace Thex.HigsIntegration.Function.Service.Infra.Repositories
{
    public class PropertyBaseRateHeaderHistoryRepository : EfCoreRepositoryBase<ThexHigsIntegrationFunctionContext, PropertyBaseRateHeaderHistory>, IPropertyBaseRateHeaderHistoryRepository
    {
        public PropertyBaseRateHeaderHistoryRepository(IDbContextProvider<ThexHigsIntegrationFunctionContext> dbContextProvider)
            : base(dbContextProvider)
        {
        }

        public async Task<Guid> Create(PropertyBaseRateHeaderHistory propertyBaseRateHeaderHistory)
        {
            await Context.PropertyBaseRateHeaderHistories.AddAsync(propertyBaseRateHeaderHistory);

            await Context.SaveChangesAsync();

            return propertyBaseRateHeaderHistory.Id;
        }
    }
}
