﻿using System.Threading.Tasks;
using Thex.HigsIntegration.Function.Service.Entities;
using Thex.HigsIntegration.Function.Service.Infra.Context;
using Thex.HigsIntegration.Function.Service.Infra.Interfaces;
using Tnf.EntityFrameworkCore;
using Tnf.EntityFrameworkCore.Repositories;

namespace Thex.HigsIntegration.Function.Service.Infra.Repositories
{
    public class CountrySubdivisionTranslationRepository : EfCoreRepositoryBase<ThexHigsIntegrationFunctionContext, CountrySubdivisionTranslation>, ICountrySubdivisionTranslationRepository
    {
        public CountrySubdivisionTranslationRepository(IDbContextProvider<ThexHigsIntegrationFunctionContext> dbContextProvider) : base(dbContextProvider)
        {
        }
         
        public override CountrySubdivisionTranslation InsertAndSaveChanges(CountrySubdivisionTranslation countrySubdivisionTranslation)
        {
            Context.CountrySubdivisionTranslations.Add(countrySubdivisionTranslation);

            //Context.SaveChanges();

            return countrySubdivisionTranslation;
        }
    }
}
