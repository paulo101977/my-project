﻿using EFCore.BulkExtensions;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using Thex.HigsIntegration.Function.Service.Entities;
using Thex.HigsIntegration.Function.Service.Infra.Context;
using Thex.HigsIntegration.Function.Service.Infra.Interfaces;
using Thex.Kernel;
using Tnf.EntityFrameworkCore;
using Tnf.EntityFrameworkCore.Repositories;

namespace Thex.HigsIntegration.Function.Service.Infra.Repositories
{
    public class PropertyBaseRateRepository : EfCoreRepositoryBase<ThexHigsIntegrationFunctionContext, PropertyBaseRate>, IPropertyBaseRateRepository
    {
        private readonly IApplicationUser _applicationUser;

        public PropertyBaseRateRepository(
            IDbContextProvider<ThexHigsIntegrationFunctionContext> dbContextProvider,
            IApplicationUser applicationUser) 
            : base(dbContextProvider)
        {
            _applicationUser = applicationUser;
        }

        public void CreateAndUpdate(List<PropertyBaseRate> propertyBaseRateToAddList, List<PropertyBaseRate> propertyBaseRateToUpdateList)
        {
            if (propertyBaseRateToUpdateList.Count > 0)
                Context.BulkUpdate(propertyBaseRateToUpdateList);

            if (propertyBaseRateToAddList.Count > 0)
                Context.BulkInsert(propertyBaseRateToAddList);
        }

        public void GetAndUpdate(int propertyId, List<int> roomTypeIdList, Guid currencyId, DateTime startDate, DateTime finalDate, int mealPlanTypeDefaultId)
        {
            Context.PropertyBaseRates
               .Where(p => p.PropertyId == propertyId &&
                           roomTypeIdList.Contains(p.RoomTypeId) &&
                           p.CurrencyId == currencyId &&
                           p.Date >= startDate &&
                           p.Date <= finalDate &&
                           p.TenantId == _applicationUser.TenantId)
               .IgnoreQueryFilters()
               .BatchUpdate(new PropertyBaseRate { MealPlanTypeDefault = mealPlanTypeDefaultId });
        }
    }
}
