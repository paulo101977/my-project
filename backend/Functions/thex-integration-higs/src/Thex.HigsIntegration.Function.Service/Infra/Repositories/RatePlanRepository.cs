﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Thex.HigsIntegration.Function.Service.Entities;
using Thex.HigsIntegration.Function.Service.Infra.Context;
using Thex.HigsIntegration.Function.Service.Infra.Interfaces;
using Tnf.EntityFrameworkCore;
using Tnf.EntityFrameworkCore.Repositories;

namespace Thex.HigsIntegration.Function.Service.Infra.Repositories
{
    public class RatePlanRepository : EfCoreRepositoryBase<ThexHigsIntegrationFunctionContext, RatePlan>, IRatePlanRepository
    {
        public RatePlanRepository(IDbContextProvider<ThexHigsIntegrationFunctionContext> dbContextProvider)
            : base(dbContextProvider)
        {
        }

        public async Task InsertRangeAndSaveChangesAsync(ICollection<RatePlan> ratePlanList)
        {
            await Context.RatePlans.AddRangeAsync(ratePlanList);

            await Context.SaveChangesAsync();
        }
    }
}
