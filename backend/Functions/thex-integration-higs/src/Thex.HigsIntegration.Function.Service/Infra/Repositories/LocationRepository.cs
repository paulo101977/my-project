﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Thex.HigsIntegration.Function.Service.Entities;
using Thex.HigsIntegration.Function.Service.Infra.Context;
using Thex.HigsIntegration.Function.Service.Infra.Interfaces;
using Tnf.EntityFrameworkCore;
using Tnf.EntityFrameworkCore.Repositories;

namespace Thex.HigsIntegration.Function.Service.Infra.Repositories
{
    public class LocationRepository : EfCoreRepositoryBase<ThexHigsIntegrationFunctionContext, Location>, ILocationRepository
    {
        public LocationRepository(IDbContextProvider<ThexHigsIntegrationFunctionContext> dbContextProvider) : base(dbContextProvider)
        {
        }

        public async Task CreateAndSaveChangesAsync(Location location)
        {
            if (location == null)
                return;

            await Context.Locations.AddAsync(location);

            await Context.SaveChangesAsync();
        }

        public async Task UpdateAndSaveChangesAsync(Location location, Guid ownerId)
        {
            if (location == null)
                return;

            RemoveLocation(location, ownerId);

            await CreateAndSaveChangesAsync(location);
        }

        private void RemoveLocation(Location location, Guid ownerId)
        {
            var locationListToExclude = Context.Locations.Where(e => e.OwnerId == ownerId && e.LocationCategoryId == location.LocationCategoryId).ToList();

            if (locationListToExclude.Count > 0)
            {
                Context.RemoveRange(locationListToExclude);
                Context.SaveChanges();
            }
        }
    }
}
