﻿using System.Threading.Tasks;
using Thex.HigsIntegration.Function.Service.Entities;
using Thex.HigsIntegration.Function.Service.Infra.Context;
using Thex.HigsIntegration.Function.Service.Infra.Interfaces;
using Tnf.EntityFrameworkCore;
using Tnf.EntityFrameworkCore.Repositories;

namespace Thex.HigsIntegration.Function.Service.Infra.Repositories
{
    public class GuestHigsIntegrationRepository : EfCoreRepositoryBase<ThexHigsIntegrationFunctionContext, GuestHigsIntegration>, IGuestHigsIntegrationRepository
    {
        public GuestHigsIntegrationRepository(IDbContextProvider<ThexHigsIntegrationFunctionContext> dbContextProvider) : base(dbContextProvider)
        {
        }

        public async Task<GuestHigsIntegration> CreateAndSaveChangesAsync(GuestHigsIntegration guestHigs)
        {
            Context.GuestHigsIntegrations.Update(guestHigs);

            await Context.SaveChangesAsync();

            return guestHigs;
        }
    }
}
