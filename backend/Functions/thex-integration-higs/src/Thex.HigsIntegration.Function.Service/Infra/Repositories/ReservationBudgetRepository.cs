﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.HigsIntegration.Function.Service.Entities;
using Thex.HigsIntegration.Function.Service.Infra.Context;
using Thex.HigsIntegration.Function.Service.Infra.Interfaces;
using Tnf.EntityFrameworkCore;
using Tnf.EntityFrameworkCore.Repositories;

namespace Thex.HigsIntegration.Function.Service.Infra.Repositories
{
    public class ReservationBudgetRepository : EfCoreRepositoryBase<ThexHigsIntegrationFunctionContext, ReservationBudget>, IReservationBudgetRepository
    {
        public ReservationBudgetRepository(IDbContextProvider<ThexHigsIntegrationFunctionContext> dbContextProvider) : base(dbContextProvider)
        {

        }

        public async Task RemoveRangedAsync(ICollection<ReservationBudget> reservationBudgetList)
        {
            Context.ReservationBudgets.RemoveRange(reservationBudgetList);

            await Context.SaveChangesAsync();
        }
    }
}
