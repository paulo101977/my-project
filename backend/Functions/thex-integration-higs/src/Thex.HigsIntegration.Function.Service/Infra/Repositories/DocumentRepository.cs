﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Thex.HigsIntegration.Function.Service.Entities;
using Thex.HigsIntegration.Function.Service.Infra.Context;
using Thex.HigsIntegration.Function.Service.Infra.Interfaces;
using Tnf.EntityFrameworkCore;
using Tnf.EntityFrameworkCore.Repositories;

namespace Thex.HigsIntegration.Function.Service.Infra.Repositories
{
    public class DocumentRepository : EfCoreRepositoryBase<ThexHigsIntegrationFunctionContext, Document>, IDocumentRepository
    {
        public DocumentRepository(IDbContextProvider<ThexHigsIntegrationFunctionContext> dbContextProvider) : base(dbContextProvider)
        {
           
        }

        public async Task<Document> CreateAndSaveChangesAsync(Document document)
        {
            Context.Documents.Add(document);

            await Context.SaveChangesAsync();

            return document;
        }

        public async Task<Document> UpdateAndSaveChangesAsync(Document document)
        {
            RemoveDocument(document.DocumentTypeId, document.OwnerId);

            return await CreateAndSaveChangesAsync(document);
        }

        private void RemoveDocument(int documentTypeId, Guid ownerId)
        {
            var documentListToExclude = Context.Documents.Where(e => e.OwnerId == ownerId && e.DocumentTypeId == documentTypeId).ToList();

            if (documentListToExclude.Count > 0)
            {
                Context.RemoveRange(documentListToExclude);
                Context.SaveChanges();
            }
        }
    }
}
