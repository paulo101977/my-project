﻿using System.Threading.Tasks;
using Thex.HigsIntegration.Function.Service.Infra.Context;

namespace Thex.HigsIntegration.Function.Service.Infra.Repositories
{
    public abstract class RepositoryBase<T> where T : class
    {
        private readonly ThexHigsIntegrationFunctionContext _context;

        public RepositoryBase(ThexHigsIntegrationFunctionContext context)
        {
            _context = context;
        }

        public async Task<T> InsertAndSaveChangesAsync(T entity)
        {
            await _context.AddAsync(entity);

            await _context.SaveChangesAsync();

            return entity;
        }
    }
}
