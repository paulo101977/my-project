﻿using System.Threading.Tasks;
using Thex.HigsIntegration.Function.Service.Entities;
using Thex.HigsIntegration.Function.Service.Infra.Context;
using Thex.HigsIntegration.Function.Service.Infra.Interfaces;
using Tnf.EntityFrameworkCore;
using Tnf.EntityFrameworkCore.Repositories;

namespace Thex.HigsIntegration.Function.Service.Infra.Repositories
{
    public class CountrySubdivisionRepository : EfCoreRepositoryBase<ThexHigsIntegrationFunctionContext, CountrySubdivision>, ICountrySubdivisionRepository
    {
        public CountrySubdivisionRepository(IDbContextProvider<ThexHigsIntegrationFunctionContext> dbContextProvider) : base(dbContextProvider)
        {
        }

        public override CountrySubdivision InsertAndSaveChanges(CountrySubdivision countrySubdivision)
        {
            Context.CountrySubdivisions.Add(countrySubdivision);

            //Context.SaveChanges();

            return countrySubdivision;
        }
    }
}
