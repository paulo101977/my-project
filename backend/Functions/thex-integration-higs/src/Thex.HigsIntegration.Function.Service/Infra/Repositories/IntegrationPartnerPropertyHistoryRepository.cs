﻿using System.Threading.Tasks;
using Thex.HigsIntegration.Function.Service.Entities;
using Thex.HigsIntegration.Function.Service.Infra.Context;
using Thex.HigsIntegration.Function.Service.Infra.Interfaces;
using Tnf.EntityFrameworkCore;
using Tnf.EntityFrameworkCore.Repositories;

namespace Thex.HigsIntegration.Function.Service.Infra.Repositories
{
    public class IntegrationPartnerPropertyHistoryRepository : EfCoreRepositoryBase<ThexHigsIntegrationFunctionContext, IntegrationPartnerPropertyHistory>, IIntegrationPartnerPropertyHistoryRepository
    {
        public IntegrationPartnerPropertyHistoryRepository(IDbContextProvider<ThexHigsIntegrationFunctionContext> dbContextProvider) : base(dbContextProvider)
        {

        }

        public async Task InsertAndSaveChangesAsync(IntegrationPartnerPropertyHistory history)
        {
            await Context.IntegrationPartnerPropertyHistories.AddAsync(history);

            await Context.SaveChangesAsync();
        }
    }
}
