﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Thex.HigsIntegration.Function.Service.Entities;
using Thex.HigsIntegration.Function.Service.Infra.Context;
using Thex.HigsIntegration.Function.Service.Infra.Interfaces;
using Tnf.EntityFrameworkCore;
using Tnf.EntityFrameworkCore.Repositories;

namespace Thex.HigsIntegration.Function.Service.Infra.Repositories
{
    public class ContactInformationRepository : EfCoreRepositoryBase<ThexHigsIntegrationFunctionContext, ContactInformation>, IContactInformationRepository
    {
        public ContactInformationRepository(IDbContextProvider<ThexHigsIntegrationFunctionContext> dbContextProvider) : base(dbContextProvider)
        {
        }

        public async Task AddRangeAndSaveChangesAsync(IList<ContactInformation> contactInformationList)
        {
            if (contactInformationList == null || contactInformationList.Count == 0)
                return;

            await Context.ContactInformations.AddRangeAsync(contactInformationList);

            await Context.SaveChangesAsync();
        }

        public async Task UpdateRangeAndSaveChangesAsync(Guid personId, IList<ContactInformation> contactInformationList)
        {
            if (contactInformationList == null || contactInformationList.Count == 0)
                return;

            RemoveContactInformationList(personId);

            await AddRangeAndSaveChangesAsync(contactInformationList);
        }

        private void RemoveContactInformationList(Guid ownerId)
        {
            var contactInformationListToExclude = Context.ContactInformations.Where(e => e.OwnerId == ownerId).ToList();

            if (contactInformationListToExclude.Count > 0)
            {
                Context.RemoveRange(contactInformationListToExclude);
                Context.SaveChanges();
            }
        }

    }
}
