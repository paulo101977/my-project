﻿using System.Threading.Tasks;
using Thex.HigsIntegration.Function.Service.Entities;
using Thex.HigsIntegration.Function.Service.Infra.Context;
using Thex.HigsIntegration.Function.Service.Infra.Interfaces;
using Tnf.EntityFrameworkCore;
using Tnf.EntityFrameworkCore.Repositories;

namespace Thex.HigsIntegration.Function.Service.Infra.Repositories
{
    public class ReservationRepository : EfCoreRepositoryBase<ThexHigsIntegrationFunctionContext, Reservation>, IReservationRepository
    {
        public ReservationRepository(IDbContextProvider<ThexHigsIntegrationFunctionContext> dbContextProvider) : base(dbContextProvider)
        {
        }

        public async Task<Reservation> UpdateAndSaveChangesAsync(Reservation reservation)
        {
            Context.Reservations.Update(reservation);

            await Context.SaveChangesAsync();

            return reservation;
        }
    }
}
