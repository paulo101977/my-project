﻿using System.Threading.Tasks;
using Thex.HigsIntegration.Function.Service.Entities;
using Thex.HigsIntegration.Function.Service.Infra.Context;
using Thex.HigsIntegration.Function.Service.Infra.Interfaces;
using Tnf.EntityFrameworkCore;
using Tnf.EntityFrameworkCore.Repositories;

namespace Thex.HigsIntegration.Function.Service.Infra.Repositories
{
    public class PersonRepository : EfCoreRepositoryBase<ThexHigsIntegrationFunctionContext, Person>, IPersonRepository
    {
        public PersonRepository(IDbContextProvider<ThexHigsIntegrationFunctionContext> dbContextProvider) : base(dbContextProvider)
        {
           
        }

        public async Task<Person> CreateAndSaveChangesAsync(Person person)
        {
            Context.Persons.Update(person);

            await Context.SaveChangesAsync();

            return person;
        }

        public async Task<Person> UpdateAndSaveChangesAsync(Person person)
        {
            Context.Persons.Update(person);

            await Context.SaveChangesAsync();

            return person;
        }
    }
}
