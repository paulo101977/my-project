﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Thex.Common.Enumerations;
using Thex.HigsIntegration.Function.Service.Entities;
using Thex.HigsIntegration.Function.Service.Infra.Context;
using Thex.HigsIntegration.Function.Service.Infra.Interfaces;
using Tnf.EntityFrameworkCore;
using Tnf.EntityFrameworkCore.Repositories;

namespace Thex.HigsIntegration.Function.Service.Infra.Repositories
{
    public class GuestReservationItemRepository : EfCoreRepositoryBase<ThexHigsIntegrationFunctionContext, GuestReservationItem>, IGuestReservationItemRepository
    {
        public GuestReservationItemRepository(IDbContextProvider<ThexHigsIntegrationFunctionContext> dbContextProvider) : base(dbContextProvider)
        {

        }

        public async Task UpdateAllGuestsToCanceledAsync(long reservationItemId)
        {
            var guestReservationItemList = await Context
                .GuestReservationItems
                .AsNoTracking()
                .Where(g => g.ReservationItemId == reservationItemId && g.GuestStatusId != (int)ReservationStatus.Canceled)
                .Select(g => new GuestReservationItem() {
                    Id = g.Id
                })
                .ToListAsync();

            foreach (var guestReservationItem in guestReservationItemList)
            {
                var entityAttached = Context.Set<GuestReservationItem>().Local.FirstOrDefault(e => e.Id == guestReservationItem.Id);
                if (entityAttached == null)
                    Context.GuestReservationItems.Attach(new GuestReservationItem() { Id = guestReservationItem.Id });

                guestReservationItem.GuestStatusId = (int)ReservationStatus.Canceled;
            }

            await Context.SaveChangesAsync();
        }

        public async Task RemoveRangeAsync(List<GuestReservationItem> guestReservationList)
        {
            Context.GuestReservationItems.RemoveRange(guestReservationList);

            await Context.SaveChangesAsync();
        }

        public async Task SaveRangeAndSaveChangesAsync(List<GuestReservationItem> guestReservationList)
        {
            await Context.GuestReservationItems.AddRangeAsync(guestReservationList);

            await Context.SaveChangesAsync();
        }

        public async Task UpdateRangeAndSaveChangesAsync(List<GuestReservationItem> guestReservationList)
        {
            Context.GuestReservationItems.UpdateRange(guestReservationList);

            await Context.SaveChangesAsync();
        }
    }
}
