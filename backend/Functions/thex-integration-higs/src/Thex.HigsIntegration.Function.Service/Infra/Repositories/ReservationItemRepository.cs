﻿using System.Threading.Tasks;
using Thex.HigsIntegration.Function.Service.Entities;
using Thex.HigsIntegration.Function.Service.Infra.Context;
using Thex.HigsIntegration.Function.Service.Infra.Interfaces;
using Tnf.EntityFrameworkCore;
using Tnf.EntityFrameworkCore.Repositories;

namespace Thex.HigsIntegration.Function.Service.Infra.Repositories
{
    public class ReservationItemRepository : EfCoreRepositoryBase<ThexHigsIntegrationFunctionContext, ReservationItem>, IReservationItemRepository
    {
        public ReservationItemRepository(IDbContextProvider<ThexHigsIntegrationFunctionContext> dbContextProvider) : base(dbContextProvider)
        {

        }

        public async Task UpdateAsync(ReservationItem reservationItem)
        {
            Context.ReservationItems.Update(reservationItem);

            await Context.SaveChangesAsync();
        }
    }
}
