﻿using System.Threading.Tasks;
using Thex.HigsIntegration.Function.Service.Entities;
using Thex.HigsIntegration.Function.Service.Infra.Context;
using Thex.HigsIntegration.Function.Service.Infra.Interfaces;
using Tnf.EntityFrameworkCore;
using Tnf.EntityFrameworkCore.Repositories;

namespace Thex.HigsIntegration.Function.Service.Infra.Repositories
{
    public class ReservationConfirmationRepository : EfCoreRepositoryBase<ThexHigsIntegrationFunctionContext, ReservationConfirmation>, IReservationConfirmationRepository
    {
        public ReservationConfirmationRepository(IDbContextProvider<ThexHigsIntegrationFunctionContext> dbContextProvider) : base(dbContextProvider)
        {

        }

        public async Task<ReservationConfirmation> UpdateAndSaveChangesAsync(ReservationConfirmation reservationConfirmation)
        {
            Context.ReservationConfirmations.Update(reservationConfirmation);

            await Context.SaveChangesAsync();

            return reservationConfirmation;
        }
    }
}
