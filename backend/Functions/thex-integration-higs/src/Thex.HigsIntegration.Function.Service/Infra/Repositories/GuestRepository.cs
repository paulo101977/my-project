﻿using System.Threading.Tasks;
using Thex.HigsIntegration.Function.Service.Entities;
using Thex.HigsIntegration.Function.Service.Infra.Context;
using Thex.HigsIntegration.Function.Service.Infra.Interfaces;
using Tnf.EntityFrameworkCore;
using Tnf.EntityFrameworkCore.Repositories;

namespace Thex.HigsIntegration.Function.Service.Infra.Repositories
{
    public class GuestRepository : EfCoreRepositoryBase<ThexHigsIntegrationFunctionContext, Guest>, IGuestRepository
    {
        public GuestRepository(IDbContextProvider<ThexHigsIntegrationFunctionContext> dbContextProvider) : base(dbContextProvider)
        {
           
        }

        public async Task<Guest> CreateAndSaveChangesAsync(Guest guest)
        {
            Context.Guests.Update(guest);

            await Context.SaveChangesAsync();

            return guest;
        }

        public async Task<Guest> UpdateAndSaveChangesAsync(Guest guest)
        {
            Context.Guests.Update(guest);

            await Context.SaveChangesAsync();

            return guest;
        }
    }
}
