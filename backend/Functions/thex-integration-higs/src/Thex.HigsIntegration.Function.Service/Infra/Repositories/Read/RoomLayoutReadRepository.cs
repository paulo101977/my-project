﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;
using Thex.HigsIntegration.Function.Service.Infra.Context;
using Thex.HigsIntegration.Function.Service.Infra.Interfaces.Read;

namespace Thex.HigsIntegration.Function.Service.Infra.Repositories.Read
{
    public class RoomLayoutReadRepository : IRoomLayoutReadRepository
    {
        private readonly ThexHigsIntegrationFunctionContext _context;

        public RoomLayoutReadRepository(ThexHigsIntegrationFunctionContext context)
        {
            _context = context;
        }

        public async Task<Guid> GetRoomLayoutDefaultAsync()
        {
            var roomLayout = await _context.RoomLayouts.FirstOrDefaultAsync(r => r.QuantityDouble == 1 && r.QuantitySingle == 1);
            if (roomLayout == null)
                return _context.RoomLayouts.FirstOrDefault().Id;

            return roomLayout.Id;
        }
    }
}
