﻿using System;
using System.Collections.Generic;
using Thex.HigsIntegration.Function.Service.Infra.Context;
using Thex.HigsIntegration.Function.Service.Infra.Interfaces.Read;
using System.Linq;
using Thex.HigsIntegration.Function.Service.Entities;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace Thex.HigsIntegration.Function.Service.Infra.Repositories.Read
{
    public class ReservationItemReadRepository : IReservationItemReadRepository
    {
        private readonly ThexHigsIntegrationFunctionContext _context;

        public ReservationItemReadRepository(ThexHigsIntegrationFunctionContext context)
        {
            _context = context;
        }

        public async Task<ReservationItem> GetReservationItemByExternalReservationNumberAsync(string externalReservationNumber)
        {
            return await _context.ReservationItems
                .Include(r => r.GuestReservationItemList)
                .Include(r => r.Reservation)
                .FirstOrDefaultAsync(r => r.ExternalReservationNumber.ToLower().Equals(externalReservationNumber.ToLower()));
        }
    }
}
