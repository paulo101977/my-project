﻿using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;
using Thex.HigsIntegration.Function.Service.Entities;
using Thex.HigsIntegration.Function.Service.Infra.Context;
using Thex.HigsIntegration.Function.Service.Infra.Interfaces.Read;

namespace Thex.HigsIntegration.Function.Service.Infra.Repositories.Read
{
    public class RatePlanReadRepository : IRatePlanReadRepository
    {
        private readonly ThexHigsIntegrationFunctionContext _context;

        public RatePlanReadRepository(ThexHigsIntegrationFunctionContext context)
        {
            _context = context;
        }

        public async Task<RatePlan> GetByDistributionCodeAsync(string distributionCode, int propertyId)
        {
            return await _context.RatePlans
               .FirstOrDefaultAsync(x => x.DistributionCode.ToUpper()
               .Equals(distributionCode.ToUpper()) && x.PropertyId == propertyId &&
               (x.IsActive.HasValue && x.IsActive.Value && !x.IsDeleted));
        }
    }
}
