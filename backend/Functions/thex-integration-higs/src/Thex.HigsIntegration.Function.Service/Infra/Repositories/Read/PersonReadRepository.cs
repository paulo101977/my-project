﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Thex.HigsIntegration.Function.Service.Entities;
using Thex.HigsIntegration.Function.Service.Infra.Context;
using Thex.HigsIntegration.Function.Service.Infra.Interfaces.Read;

namespace Thex.HigsIntegration.Function.Service.Infra.Repositories.Read
{
    public class PersonReadRepository : IPersonReadRepository
    {
        private readonly ThexHigsIntegrationFunctionContext _context;

        public PersonReadRepository(ThexHigsIntegrationFunctionContext context)
        {
            _context = context;
        }

        public async Task<Person> GetByDocument(string document, int documentType)
         => await (from p in _context.Persons.AsNoTracking()
                   join d in _context.Documents.AsNoTracking() on p.Id equals d.OwnerId
                   where d.DocumentInformation == document && d.DocumentTypeId == documentType
                   select p).FirstOrDefaultAsync();

        public async Task<Person> GetByHigsCode(string higsCode, int propertyId)
            => await (from p in _context.Persons.AsNoTracking()
               join g in _context.Guests.AsNoTracking() on p.Id equals g.PersonId
               join h in _context.GuestHigsIntegrations.AsNoTracking() on g.Id equals h.GuestId
               where h.HigsCode == higsCode && h.PropertyId == propertyId
               select p).FirstOrDefaultAsync();
    }
}
