﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Thex.HigsIntegration.Function.Service.Entities;
using Thex.HigsIntegration.Function.Service.Infra.Context;
using Thex.HigsIntegration.Function.Service.Infra.Interfaces.Read;

namespace Thex.HigsIntegration.Function.Service.Infra.Repositories.Read
{
    public class NationalityReadRepository : INationalityReadRepository
    {
        private readonly ThexHigsIntegrationFunctionContext _context;
        private const int COUNTRYSUBDIVISIONTYPEID = 1;

        public NationalityReadRepository(ThexHigsIntegrationFunctionContext context)
        {
            _context = context;
        }

        public async Task<CountrySubdivisionTranslation> GetByIsoCodeAsync(string isoCode)
            => await (from c in _context.CountrySubdivisions.AsNoTracking()
                      join ct in _context.CountrySubdivisionTranslations.AsNoTracking()
                      on c.Id equals ct.CountrySubdivisionId
                      where c.CountryTwoLetterIsoCode.ToLower() == isoCode.ToLower() &&
                      c.SubdivisionTypeId == COUNTRYSUBDIVISIONTYPEID
                      select ct).FirstOrDefaultAsync();
    }
}
