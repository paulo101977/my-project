﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Thex.HigsIntegration.Function.Service.Entities;
using Thex.HigsIntegration.Function.Service.Infra.Context;
using Thex.HigsIntegration.Function.Service.Infra.Interfaces.Read;

namespace Thex.HigsIntegration.Function.Service.Infra.Repositories.Read
{
    public class ReasonReadRepository : IReasonReadRepository
    {
        private readonly ThexHigsIntegrationFunctionContext _context;
        private const int COUNTRYSUBDIVISIONTYPEID = 1;

        public ReasonReadRepository(ThexHigsIntegrationFunctionContext context)
        {
            _context = context;
        }

        public async Task<Reason> GetByHigsCodeAsync(int higsCode, int chainId, int categoryId)
            => await _context.Reasons.AsNoTracking()
                        .FirstOrDefaultAsync(e => e.HigsCode == higsCode && 
                                                  e.ChainId == chainId && 
                                                  e.ReasonCategoryId == categoryId);
    }
}
