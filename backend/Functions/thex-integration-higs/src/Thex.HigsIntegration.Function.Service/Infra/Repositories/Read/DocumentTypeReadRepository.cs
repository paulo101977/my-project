﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Thex.HigsIntegration.Function.Service.Entities;
using Thex.HigsIntegration.Function.Service.Infra.Context;
using Thex.HigsIntegration.Function.Service.Infra.Interfaces.Read;

namespace Thex.HigsIntegration.Function.Service.Infra.Repositories.Read
{
    public class DocumentTypeReadRepository : IDocumentTypeReadRepository
    {
        private readonly ThexHigsIntegrationFunctionContext _context;

        public DocumentTypeReadRepository(ThexHigsIntegrationFunctionContext context)
        {
            _context = context;
        }

        public async Task<DocumentType> GetByHigsCodeAsync(int higsCode)
            => await _context.DocumentTypes.AsNoTracking().FirstOrDefaultAsync(e => e.HigsCode == higsCode);
    }
}
