﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Thex.HigsIntegration.Function.Service.Entities;
using Thex.HigsIntegration.Function.Service.Infra.Context;
using Thex.HigsIntegration.Function.Service.Infra.Interfaces.Read;

namespace Thex.HigsIntegration.Function.Service.Infra.Repositories.Read
{
    public class GuestReadRepository : IGuestReadRepository
    {
        private readonly ThexHigsIntegrationFunctionContext _context;

        public GuestReadRepository(ThexHigsIntegrationFunctionContext context)
        {
            _context = context;
        }

        public async Task<Guest> GetAsync(Guid personId)
         => await (from g in _context.Guests.AsNoTracking()
                   where g.PersonId == personId
                   select g).FirstOrDefaultAsync();
    }
}
