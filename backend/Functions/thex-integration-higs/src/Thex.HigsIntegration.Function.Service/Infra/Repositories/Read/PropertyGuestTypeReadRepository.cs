﻿using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using Thex.HigsIntegration.Function.Service.Infra.Context;
using Thex.HigsIntegration.Function.Service.Infra.Interfaces.Read;

namespace Thex.HigsIntegration.Function.Service.Infra.Repositories.Read
{
    public class PropertyGuestTypeReadRepository : IPropertyGuestTypeReadRepository
    {
        private readonly ThexHigsIntegrationFunctionContext _context;

        public PropertyGuestTypeReadRepository(ThexHigsIntegrationFunctionContext context)
        {
            _context = context;
        }

        public async Task<int> GetPropertyGuestTypeDefaultAsync(int propertyId)
        {
            var guesType = await _context.PropertyGuestTypes.FirstOrDefaultAsync(pgt => !pgt.IsVip && pgt.PropertyId == propertyId);

            return guesType != null ? guesType.Id : 0;
        }
    }
}
