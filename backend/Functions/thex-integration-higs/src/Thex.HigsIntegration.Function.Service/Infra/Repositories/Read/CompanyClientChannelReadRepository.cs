﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Threading.Tasks;
using Thex.HigsIntegration.Function.Service.Infra.Context;
using Thex.HigsIntegration.Function.Service.Infra.Interfaces.Read;

namespace Thex.HigsIntegration.Function.Service.Infra.Repositories.Read
{
    public class CompanyClientChannelReadRepository : ICompanyClientChannelReadRepository
    {
        private readonly ThexHigsIntegrationFunctionContext _context;

        public CompanyClientChannelReadRepository(ThexHigsIntegrationFunctionContext context)
        {
            _context = context;
        }

        public async Task<Guid?> GetCompanyClientIdByCompanyIdAsync(string companyId, Guid tenantId)
        {
            return (await _context.CompanyClientChannels
               .Include(ccc => ccc.Channel)
               .FirstOrDefaultAsync(ccc => ccc.CompanyId.ToLower().Equals(companyId.ToLower()) && ccc.Channel.TenantId == tenantId && ccc.IsActive))
               ?.CompanyClientId;
        }

        public async Task<Guid?> GetCompanyClientIdByCompanyIdAndChannelCodeAsync(string channelCode, string companyId, Guid tenantId)
        {
            return (await _context.CompanyClientChannels
               .Include(ccc => ccc.Channel)
               .ThenInclude(c => c.ChannelCode)
               .FirstOrDefaultAsync(ccc => ccc.CompanyId.ToLower().Equals(companyId.ToLower()) && ccc.Channel.TenantId == tenantId 
                                        && ccc.IsActive && ccc.Channel.ChannelCode.ChannelCode1.ToLower().Equals(channelCode.ToLower())))
               ?.CompanyClientId;
        }
    }
}
