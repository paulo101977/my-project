﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Thex.Common.Dto;
using Thex.HigsIntegration.Function.Service.Dto;
using Thex.HigsIntegration.Function.Service.Entities;
using Thex.HigsIntegration.Function.Service.Infra.Context;
using Thex.HigsIntegration.Function.Service.Infra.Interfaces.Read;
using Tnf.Dto;

namespace Thex.HigsIntegration.Function.Service.Infra.Repositories.Read
{
    public class CountrySubdivisionTranslationReadRepository : ICountrySubdivisionTranslationReadRepository
    {
        private readonly ThexHigsIntegrationFunctionContext _context;

        public CountrySubdivisionTranslationReadRepository(ThexHigsIntegrationFunctionContext context)
        {
            _context = context;
        }

        //public AddressTranslationDto GetAddressTranslationByCityId(int cityId, string languageIsoCode)
        //{
        //    return (from city in _context.CountrySubdivisions
        //            join state in _context.CountrySubdivisions on city.ParentSubdivision.Id equals state.Id
        //            join country in _context.CountrySubdivisions on state.ParentSubdivision.Id equals country.Id
        //            join cityTranslation in _context.CountrySubdivisionTranslations on city.Id equals cityTranslation.CountrySubdivision.Id
        //            join stateTranslation in _context.CountrySubdivisionTranslations on state.Id equals stateTranslation.CountrySubdivision.Id
        //            join countryTranslation in _context.CountrySubdivisionTranslations on country.Id equals countryTranslation.CountrySubdivision.Id
        //            where city.Id == cityId &&
        //            cityTranslation.LanguageIsoCode.ToLower() == languageIsoCode &&
        //            stateTranslation.LanguageIsoCode.ToLower() == languageIsoCode &&
        //            countryTranslation.LanguageIsoCode.ToLower() == languageIsoCode
        //            select new AddressTranslationDto
        //            {
        //                CityId = city.Id,
        //                CityName = cityTranslation.Name,
        //                StateId = state.Id,
        //                StateName = stateTranslation.Name,
        //                CountryId = country.Id,
        //                CountryName = countryTranslation.Name,
        //                LanguageIsoCode = cityTranslation.LanguageIsoCode,
        //                TwoLetterIsoCode = state.TwoLetterIsoCode,
        //                CountryTwoLetterIsoCode = state.CountryTwoLetterIsoCode
        //            })
        //                       .FirstOrDefault();
        //}

        //public IListDto<CountryDto> GetAllCountriesByLanguageIsoCode(GetAllCountryDto request)
        //{
        //    var result = (from country in _context.CountrySubdivisions
        //                  join translation in _context.CountrySubdivisionTranslations on country.Id equals translation.CountrySubdivision.Id
        //                  where translation.LanguageIsoCode.ToLower() == request.LanguageIsoCode.ToLower() &&
        //                  !country.ParentSubdivisionId.HasValue
        //                  select new CountryDto
        //                  {
        //                      Id = country.Id,
        //                      Name = translation.Name,
        //                      TranslationId = translation.Id,
        //                      TwoLetterIsoCode = country.TwoLetterIsoCode,
        //                      CountryTwoLetterIsoCode = country.CountryTwoLetterIsoCode
        //                  })
        //                     .OrderBy(exp => exp.Name).ToList();

        //    return new ListDto<CountryDto>
        //    {
        //        HasNext = false,
        //        Items = result,
        //    };
        //}

        public List<BaseCountrySubdivisionTranslationDto> GetCountryWithTranslations(string countryTwoLetterIsoCode)
        {
            return (from country in _context.CountrySubdivisions
                    join translation in _context.CountrySubdivisionTranslations on country.Id equals translation.CountrySubdivision.Id
                    where country.CountryTwoLetterIsoCode.ToLower() == countryTwoLetterIsoCode.ToLower() &&
                    !country.ParentSubdivisionId.HasValue
                    select new BaseCountrySubdivisionTranslationDto
                    {
                        Id = country.Id,
                        Name = translation.Name,
                        TranslationId = translation.Id,
                        TwoLetterIsoCode = country.TwoLetterIsoCode,
                        CountryTwoLetterIsoCode = country.CountryTwoLetterIsoCode,
                        LanguageIsoCode = translation.LanguageIsoCode
                    }).ToList();

        }

        public List<BaseCountrySubdivisionTranslationDto> GetStateWithTranslations(string translationName, int parentSubdivionId)
        {
            return (from country in _context.CountrySubdivisions
                    join translation in _context.CountrySubdivisionTranslations on country.Id equals translation.CountrySubdivision.Id
                    where translation.Name.ToLower() == translationName.ToLower() && country.ParentSubdivision.Id == parentSubdivionId
                    select new BaseCountrySubdivisionTranslationDto
                    {
                        Id = country.Id,
                        Name = translation.Name,
                        TranslationId = translation.Id,
                        TwoLetterIsoCode = country.TwoLetterIsoCode,
                        CountryTwoLetterIsoCode = country.CountryTwoLetterIsoCode,
                        LanguageIsoCode = translation.LanguageIsoCode
                    }).ToList();
        }


        public List<BaseCountrySubdivisionTranslationDto> GetStateWithTranslationsByCode(string twoLetterIsoCode, int parentSubdivionId)
        {
            return (from state in _context.CountrySubdivisions
                    join translation in _context.CountrySubdivisionTranslations on state.Id equals translation.CountrySubdivision.Id
                    where state.TwoLetterIsoCode.ToLower() == twoLetterIsoCode.Substring(0, 2).ToLower() && state.ParentSubdivision.Id == parentSubdivionId
                    select new BaseCountrySubdivisionTranslationDto
                    {
                        Id = state.Id,
                        Name = translation.Name,
                        TranslationId = translation.Id,
                        TwoLetterIsoCode = state.TwoLetterIsoCode,
                        CountryTwoLetterIsoCode = state.CountryTwoLetterIsoCode,
                        LanguageIsoCode = translation.LanguageIsoCode
                    }).ToList();
        }

        public List<BaseCountrySubdivisionTranslationDto> GetCityWithTranslations(string translationName, int parentSubdivionId)
        {
            return (from country in _context.CountrySubdivisions
                    join translation in _context.CountrySubdivisionTranslations on country.Id equals translation.CountrySubdivision.Id
                    where translation.Name.ToLower() == translationName.ToLower() && country.ParentSubdivision.Id == parentSubdivionId
                    select new BaseCountrySubdivisionTranslationDto
                    {
                        Id = country.Id,
                        Name = translation.Name,
                        TranslationId = translation.Id,
                        TwoLetterIsoCode = country.TwoLetterIsoCode,
                        CountryTwoLetterIsoCode = country.CountryTwoLetterIsoCode,
                        LanguageIsoCode = translation.LanguageIsoCode
                    }).ToList();
        }


        public List<BaseCountrySubdivisionTranslationDto> GetCountrySubdivisionWithTranslations(int subdivisionId)
        {
            return (from countrySubdivision in _context.CountrySubdivisions
                    join translation in _context.CountrySubdivisionTranslations on countrySubdivision.Id equals translation.CountrySubdivision.Id
                    where countrySubdivision.Id == subdivisionId
                    select new BaseCountrySubdivisionTranslationDto
                    {
                        Id = countrySubdivision.Id,
                        Name = translation.Name,
                        TranslationId = translation.Id,
                        TwoLetterIsoCode = countrySubdivision.TwoLetterIsoCode,
                        CountryTwoLetterIsoCode = countrySubdivision.CountryTwoLetterIsoCode,
                        LanguageIsoCode = translation.LanguageIsoCode
                    }).ToList();
        }
    }
}
