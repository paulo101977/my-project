﻿using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;
using Thex.HigsIntegration.Function.Service.Entities;
using Thex.HigsIntegration.Function.Service.Infra.Context;
using Thex.HigsIntegration.Function.Service.Infra.Interfaces.Read;

namespace Thex.HigsIntegration.Function.Service.Infra.Repositories.Read
{
    public class ReservationReadRepository : IReservationReadRepository
    {
        public ThexHigsIntegrationFunctionContext Context { get; }

        public ReservationReadRepository(ThexHigsIntegrationFunctionContext context)
        {
            Context = context;
        }

        public async Task<Reservation> GetByExternalReservationNumberAsync(string externalReservationNumber)
        {
            var reservationId = (await Context.ReservationItems.Select(ri => new { ri.ReservationId, ri.ExternalReservationNumber } ).FirstOrDefaultAsync(r => r.ExternalReservationNumber.ToLower().Equals(externalReservationNumber.ToLower())))?.ReservationId;

            if (!reservationId.HasValue) return null;

            return await Context.Reservations.Include(r => r.ReservationItemList).ThenInclude(ri => ri.ReservationBudgetList)
                                              .Include(r => r.ReservationItemList).ThenInclude(ri => ri.GuestReservationItemList)
                                              .Include(r => r.ReservationConfirmationList)
                                              .FirstOrDefaultAsync(r => r.Id == reservationId);
        }
    }
}
