﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Thex.HigsIntegration.Function.Service.Entities;
using Thex.HigsIntegration.Function.Service.Infra.Context;
using Thex.HigsIntegration.Function.Service.Infra.Interfaces.Read;

namespace Thex.HigsIntegration.Function.Service.Infra.Repositories.Read
{
    public class GuestHigsIntegrationReadRepository : IGuestHigsIntegrationReadRepository
    {
        private readonly ThexHigsIntegrationFunctionContext _context;

        public GuestHigsIntegrationReadRepository(ThexHigsIntegrationFunctionContext context)
        {
            _context = context;
        }

        public async Task<GuestHigsIntegration> GetByHigsCodeAsync(string higsCode, long propertyId)
            => await _context
                        .GuestHigsIntegrations.AsNoTracking()
                        .Include(e => e.Guest).AsNoTracking()
                        .FirstOrDefaultAsync(e => e.HigsCode == higsCode && e.PropertyId == propertyId);
    }
}
