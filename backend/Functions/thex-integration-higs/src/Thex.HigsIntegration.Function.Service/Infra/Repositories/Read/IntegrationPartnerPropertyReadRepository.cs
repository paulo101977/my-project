﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;
using Thex.HigsIntegration.Function.Service.Entities;
using Thex.HigsIntegration.Function.Service.Infra.Context;
using Thex.HigsIntegration.Function.Service.Infra.Interfaces.Read;

namespace Thex.HigsIntegration.Function.Service.Infra.Repositories.Read
{
    public class IntegrationPartnerPropertyReadRepository : IIntegrationPartnerPropertyReadRepository
    {
        private readonly ThexHigsIntegrationFunctionContext _context;

        public IntegrationPartnerPropertyReadRepository(ThexHigsIntegrationFunctionContext context)
        {
            _context = context;
        }

        public async Task<Property> GetPropertyByIntegrationCodeAsync(string integrationCode)
        {
            var integrationPartnerProperty = await _context.IntegrationPartnerProperties.AsNoTracking()
                .Include(x=>x.Property)
                .FirstOrDefaultAsync(x => x.IntegrationCode.ToUpper()
                .Equals(integrationCode.ToUpper()) && x.IsActive);

            return integrationPartnerProperty?.Property;
        }

        public async Task<Guid> GetByIntegrationCodeAsync(string integrationCode)
        {
            return (await _context.IntegrationPartnerProperties.FirstOrDefaultAsync(i => i.IntegrationCode.ToLower().Equals(integrationCode.ToLower()))).Id;
        }
    }
}
