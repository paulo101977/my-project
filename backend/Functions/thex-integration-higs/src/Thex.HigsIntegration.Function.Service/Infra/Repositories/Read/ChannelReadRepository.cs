﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Thex.HigsIntegration.Function.Service.Entities;
using Thex.HigsIntegration.Function.Service.Infra.Context;
using Thex.HigsIntegration.Function.Service.Infra.Interfaces.Read;

namespace Thex.HigsIntegration.Function.Service.Infra.Repositories.Read
{
    public class ChannelReadRepository : IChannelReadRepository
    {
        private readonly ThexHigsIntegrationFunctionContext _context;

        public ChannelReadRepository(ThexHigsIntegrationFunctionContext context)
        {
            _context = context;
        }

        public async Task<Channel> GetChannelByChannelCodeAsync(string channelCode, Guid tenantId)
        {
            var channel = await _context.Channels
                                        .Include(c => c.ChannelCode)
                                        .Include(c => c.MarketSegment)
                                        .Include(c => c.BusinessSource)
                                        .FirstOrDefaultAsync(x => x.ChannelCode.ChannelCode1.ToUpper().Equals(channelCode.ToUpper()) 
                                                                                            && x.IsActive && x.TenantId == tenantId && !x.IsDeleted);
            if (channel == null)
                return null;

            return channel;
        }
    }
}
