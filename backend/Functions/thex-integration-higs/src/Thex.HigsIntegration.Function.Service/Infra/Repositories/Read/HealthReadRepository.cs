﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.HigsIntegration.Function.Service.Infra.Context;
using Thex.HigsIntegration.Function.Service.Infra.Interfaces.Read;

namespace Thex.HigsIntegration.Function.Service.Infra.Repositories.Read
{
    public class HealthReadRepository : IHealthReadRepository
    {
        private readonly ThexHigsIntegrationFunctionContext _context;

        public HealthReadRepository(ThexHigsIntegrationFunctionContext context)
        {
            _context = context;
        }

        public async Task<bool> AnyAsync()
         => await _context.Guests.AnyAsync();
    }
}
