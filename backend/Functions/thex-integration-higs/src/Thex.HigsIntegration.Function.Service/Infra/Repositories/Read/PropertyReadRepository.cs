﻿using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;
using Thex.Common.Enumerations;
using Thex.HigsIntegration.Function.Service.Infra.Context;
using Thex.HigsIntegration.Function.Service.Infra.Interfaces.Read;

namespace Thex.HigsIntegration.Function.Service.Infra.Repositories.Read
{
    public class PropertyReadRepository : IPropertyReadRepository
    {
        private readonly ThexHigsIntegrationFunctionContext _context;

        public PropertyReadRepository(ThexHigsIntegrationFunctionContext context)
        {
            _context = context;
        }

        public async Task<string> GetParameterByPropertyIdAsync(int propertyId, int applicationParameterId)
        {
            return (await _context.PropertyParameters.FirstOrDefaultAsync(x => x.PropertyId == propertyId &&
                            x.ApplicationParameterId == applicationParameterId))
                            ?.PropertyParameterValue;
        }
    }
}
