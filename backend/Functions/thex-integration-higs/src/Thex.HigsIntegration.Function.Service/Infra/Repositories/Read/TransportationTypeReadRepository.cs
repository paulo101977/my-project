﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Thex.HigsIntegration.Function.Service.Entities;
using Thex.HigsIntegration.Function.Service.Infra.Context;
using Thex.HigsIntegration.Function.Service.Infra.Interfaces.Read;

namespace Thex.HigsIntegration.Function.Service.Infra.Repositories.Read
{
    public class TransportationTypeReadRepository : ITransportationTypeReadRepository
    {
        private readonly ThexHigsIntegrationFunctionContext _context;

        public TransportationTypeReadRepository(ThexHigsIntegrationFunctionContext context)
        {
            _context = context;
        }

        public async Task<TransportationType> GetByHigsCodeAsync(int higsCode)
            => await _context.TransportationTypes.AsNoTracking()
                        .FirstOrDefaultAsync(e => e.HigsCode == higsCode);
    }
}
