﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Thex.HigsIntegration.Function.Service.Entities;
using Thex.HigsIntegration.Function.Service.Infra.Context;
using Thex.HigsIntegration.Function.Service.Infra.Interfaces.Read;

namespace Thex.HigsIntegration.Function.Service.Infra.Repositories.Read
{
    public class PropertyBaseRateReadRepository : IPropertyBaseRateReadRepository
    {
        private readonly ThexHigsIntegrationFunctionContext _context;

        public PropertyBaseRateReadRepository(ThexHigsIntegrationFunctionContext context)
        {
            _context = context;
        }

        public async Task<List<PropertyBaseRate>> GetAll(int mealPlanTypeId, Guid currencyId, List<DateTime> dateList, List<int> roomTypeIdList, Guid? ratePlanId = null)
        {
            var dbBaseQuery = _context
                                .PropertyBaseRates
                                .AsNoTracking()
                                .Where(e => e.MealPlanTypeId == mealPlanTypeId &&
                                            roomTypeIdList.Contains(e.RoomTypeId) &&
                                            e.CurrencyId == currencyId);

            var minDate = dateList.Min(p => p);
            var maxDate = dateList.Max(p => p);

            dbBaseQuery = dbBaseQuery.Where(e => e.Date.Date >= minDate.Date);
            dbBaseQuery = dbBaseQuery.Where(e => e.Date.Date <= maxDate.Date);

            if (ratePlanId.HasValue)
                dbBaseQuery = dbBaseQuery.Where(e => e.RatePlanId == ratePlanId.Value);
            else
                dbBaseQuery = dbBaseQuery.Where(e => e.RatePlanId == null);

            return await dbBaseQuery.ToListAsync();
        }

        public async Task<List<PropertyBaseRate>> GetAllByPeriod(int propertyId, List<int> roomTypeId, Guid? currencyId, int? mealPlanTypeId, DateTime initialDate, DateTime finalDate, bool? include = null, bool? fixedRates = null, Guid? ratePlanId = null)
        {
            var dbBaseQuery = _context
                                .PropertyBaseRates
                                .AsNoTracking();

            if (include.HasValue && include.Value)
                dbBaseQuery = dbBaseQuery
                    .Include(e => e.Currency)
                    .Include(e => e.RoomType);

            dbBaseQuery = dbBaseQuery
                .Where(e => e.PropertyId == propertyId &&
                            roomTypeId.Contains(e.RoomTypeId) &&
                            e.Date.Date >= initialDate.Date &&
                            e.Date.Date <= finalDate.Date);

            if (mealPlanTypeId.HasValue)
                dbBaseQuery = dbBaseQuery.Where(e => e.MealPlanTypeId == mealPlanTypeId);

            if (currencyId.HasValue)
                dbBaseQuery = dbBaseQuery.Where(e => e.CurrencyId == currencyId);

            if (fixedRates.HasValue && fixedRates.Value)
                dbBaseQuery = dbBaseQuery.Where(e => e.RatePlanId != null);
            else
                dbBaseQuery = dbBaseQuery.Where(e => e.RatePlanId == null);

            if (ratePlanId.HasValue)
                dbBaseQuery = dbBaseQuery.Where(e => e.RatePlanId == ratePlanId.Value);

            return await dbBaseQuery.ToListAsync();
        }
    }
}
