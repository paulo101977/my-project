﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Thex.Common.Enumerations;
using Thex.HigsIntegration.Function.Service.Infra.Context;
using Thex.HigsIntegration.Function.Service.Infra.Interfaces.Read;

namespace Thex.HigsIntegration.Function.Service.Infra.Repositories.Read
{
    public class PropertyParameterReadRepository : IPropertyParameterReadRepository
    {
        private readonly ThexHigsIntegrationFunctionContext _context;

        public PropertyParameterReadRepository(ThexHigsIntegrationFunctionContext context)
        {
            _context = context;
        }

        public async Task<(TimeSpan, TimeSpan)> GetCheckinAndCheckoutHourByPropertyId(int propertyId)
        {
            var parameters = await _context.PropertyParameters.Where(pp => pp.PropertyId == propertyId &&
                                            (pp.ApplicationParameterId == (int)ApplicationParameterEnum.ParameterCheckInTime || 
                                                                pp.ApplicationParameterId == (int)ApplicationParameterEnum.ParameterCheckOutTime))
                                            .ToListAsync();

            return (TimeSpan.Parse(parameters.FirstOrDefault(pp => pp.ApplicationParameterId == (int)ApplicationParameterEnum.ParameterCheckInTime).PropertyParameterValue),
                   TimeSpan.Parse(parameters.FirstOrDefault(pp => pp.ApplicationParameterId == (int)ApplicationParameterEnum.ParameterCheckOutTime).PropertyParameterValue));
        }
    }
}
