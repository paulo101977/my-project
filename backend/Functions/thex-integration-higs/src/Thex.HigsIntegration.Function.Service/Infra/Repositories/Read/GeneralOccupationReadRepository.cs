﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Thex.HigsIntegration.Function.Service.Entities;
using Thex.HigsIntegration.Function.Service.Infra.Context;
using Thex.HigsIntegration.Function.Service.Infra.Interfaces.Read;

namespace Thex.HigsIntegration.Function.Service.Infra.Repositories.Read
{
    public class GeneralOccupationReadRepository : IGeneralOccupationReadRepository
    {
        private readonly ThexHigsIntegrationFunctionContext _context;
        private const int OTHERSOCCUPATIONID = 30;

        public GeneralOccupationReadRepository(ThexHigsIntegrationFunctionContext context)
        {
            _context = context;
        }

        public async Task<GeneralOccupation> GetByNameAsync(string occupationName)
        {
            if (string.IsNullOrEmpty(occupationName))
                return null;

           var occupation = await _context.GeneralOccupations.AsNoTracking()
                        .FirstOrDefaultAsync(e => e.Occupation.ToLower().Trim() == occupationName.ToLower());

            return occupation ?? new GeneralOccupation { Id = OTHERSOCCUPATIONID };
        }
            
    }
}
