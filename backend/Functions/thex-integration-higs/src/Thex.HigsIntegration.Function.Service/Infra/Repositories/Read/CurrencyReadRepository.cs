﻿using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;
using Thex.HigsIntegration.Function.Service.Entities;
using Thex.HigsIntegration.Function.Service.Infra.Context;
using Thex.HigsIntegration.Function.Service.Infra.Interfaces.Read;

namespace Thex.HigsIntegration.Function.Service.Infra.Repositories.Read
{
    public class CurrencyReadRepository : ICurrencyReadRepository
    {
        private readonly ThexHigsIntegrationFunctionContext _context;

        public CurrencyReadRepository(ThexHigsIntegrationFunctionContext context)
        {
            _context = context;
        }

        public async Task<Currency> GetCurrencyByCurrencyNameAsync(string currencyName)
        {
            return await _context.Currencies
                                    .AsNoTracking()
                                    .FirstOrDefaultAsync(c => c.AlphabeticCode.ToUpper().Equals(currencyName.ToUpper()) && !c.IsDeleted);
        }

        public async Task<Currency> GetCurrencyByCurrencySymbolAsync(string symbol)
        {
            return await _context.Currencies
                                    .AsNoTracking()
                                    .FirstOrDefaultAsync(c => c.Symbol.ToUpper().Equals(symbol.ToUpper()) && !c.IsDeleted);
        }
    }
}
