﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Thex.HigsIntegration.Function.Service.Entities;
using Thex.HigsIntegration.Function.Service.Infra.Context;
using Thex.HigsIntegration.Function.Service.Infra.Interfaces.Read;

namespace Thex.HigsIntegration.Function.Service.Infra.Repositories.Read
{
    public class RoomTypeReadRepository : IRoomTypeReadRepository
    {
        private readonly ThexHigsIntegrationFunctionContext _context;

        public RoomTypeReadRepository(ThexHigsIntegrationFunctionContext context)
        {
            _context = context;
        }

        public async Task<int> GetRoomTypeIdByDistributionCodeAsync(string code, int propertyId)
        {
            var roomType = await _context.RoomTypes
                .FirstOrDefaultAsync(r => r.DistributionCode.ToLower().Equals(code.ToLower())
                && r.PropertyId.Equals(propertyId) && r.IsActive);

            if (roomType == null) return 0;

            return roomType.Id;
        }

        public async Task<ICollection<RoomType>> GetAllRoomTypesByPropertyIdAsync(int propertyId)
        {
            var dbBaseQuery = await _context.RoomTypes.AsNoTracking()
                    .Where(r => r.PropertyId == propertyId)
                    .OrderBy(r => r.Name)
                    .ToListAsync();

            return dbBaseQuery;
        }
    }
}
