﻿using Microsoft.Extensions.DependencyInjection;
using Thex.HigsIntegration.Function.Service.Infra.Interfaces;
using Thex.HigsIntegration.Function.Service.Infra.Interfaces.Read;
using Thex.HigsIntegration.Function.Service.Infra.Repositories;
using Thex.HigsIntegration.Function.Service.Infra.Repositories.Read;
using Thex.Maps.Geocode.ReadInterfaces;
using Thex.Maps.Geocode.Repositories;
using Tnf.Notifications;

namespace Thex.HigsIntegration.Function.Service.Infra
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddInfraDependency(this IServiceCollection services)
        {
            services.AddScoped<INotificationHandler, NotificationHandler>();

            services.AddScoped<ICompanyClientChannelReadRepository, CompanyClientChannelReadRepository>();
            services.AddScoped<ICurrencyReadRepository, CurrencyReadRepository>();
            services.AddScoped<IIntegrationPartnerPropertyReadRepository, IntegrationPartnerPropertyReadRepository>();
            services.AddScoped<IRatePlanReadRepository, RatePlanReadRepository>();
            services.AddScoped<IReservationItemReadRepository, ReservationItemReadRepository>();
            services.AddScoped<IChannelReadRepository, ChannelReadRepository>();
            services.AddScoped<IRoomLayoutReadRepository, RoomLayoutReadRepository>();
            services.AddScoped<IRoomTypeReadRepository, RoomTypeReadRepository>();
            services.AddScoped<IPropertyReadRepository, PropertyReadRepository>();
            services.AddScoped<IPropertyParameterReadRepository, PropertyParameterReadRepository>();
            services.AddScoped<IReservationReadRepository, ReservationReadRepository>();
            services.AddScoped<IDocumentTypeReadRepository, DocumentTypeReadRepository>();
            services.AddScoped<ICountrySubdivisionTranslationReadRepository, CountrySubdivisionTranslationReadRepository>();
            services.AddScoped<IGeneralOccupationReadRepository, GeneralOccupationReadRepository>();
            services.AddScoped<INationalityReadRepository, NationalityReadRepository>();
            services.AddScoped<IReasonReadRepository, ReasonReadRepository>();
            services.AddScoped<ITransportationTypeReadRepository, TransportationTypeReadRepository>();
            services.AddScoped<IGuestHigsIntegrationReadRepository, GuestHigsIntegrationReadRepository>();
            services.AddScoped<IAddressMapsGeocodeReadRepository, AddressMapsGeocodeReadRepository>();
            services.AddScoped<IPropertyBaseRateReadRepository, PropertyBaseRateReadRepository>();
            services.AddScoped<IPropertyGuestTypeReadRepository, PropertyGuestTypeReadRepository>();
            

            services.AddScoped<IGuestReservationItemRepository, GuestReservationItemRepository>();
            services.AddScoped<IReservationBudgetRepository, ReservationBudgetRepository>();
            services.AddScoped<IReservationRepository, ReservationRepository>();
            services.AddScoped<IReservationConfirmationRepository, ReservationConfirmationRepository>();
            services.AddScoped<IReservationItemRepository, ReservationItemRepository>();
            services.AddScoped<IIntegrationPartnerPropertyHistoryRepository, IntegrationPartnerPropertyHistoryRepository>();
            services.AddScoped<ICountrySubdivisionRepository, CountrySubdivisionRepository>();
            services.AddScoped<ICountrySubdivisionTranslationRepository, CountrySubdivisionTranslationRepository>();
            services.AddScoped<IGuestHigsIntegrationRepository, GuestHigsIntegrationRepository>();
            services.AddScoped<IGuestRepository, GuestRepository>();
            services.AddScoped<IPersonRepository, PersonRepository>();
            services.AddScoped<ILocationRepository, LocationRepository>();
            services.AddScoped<IContactInformationRepository, ContactInformationRepository>();
            services.AddScoped<IDocumentRepository, DocumentRepository>();
            services.AddScoped<IPropertyBaseRateRepository, PropertyBaseRateRepository>();
            services.AddScoped<IPropertyBaseRateHeaderHistoryRepository, PropertyBaseRateHeaderHistoryRepository>();
            services.AddScoped<IRatePlanRepository, RatePlanRepository>();

            services.AddScoped<IHealthReadRepository, HealthReadRepository>();

            return services;
        }
    }
}
