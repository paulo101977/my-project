﻿using System.Threading.Tasks;
using Thex.HigsIntegration.Function.Service.Entities;

namespace Thex.HigsIntegration.Function.Service.Infra.Interfaces
{
    public interface IReservationConfirmationRepository : IRepositoryBase<ReservationConfirmation>
    {
        Task<ReservationConfirmation> UpdateAndSaveChangesAsync(ReservationConfirmation reservationConfirmation);
    }
}
