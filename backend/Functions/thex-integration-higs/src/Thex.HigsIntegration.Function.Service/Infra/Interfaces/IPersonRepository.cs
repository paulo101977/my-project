﻿using System.Threading.Tasks;
using Thex.HigsIntegration.Function.Service.Entities;

namespace Thex.HigsIntegration.Function.Service.Infra.Interfaces
{
    public interface IPersonRepository : IRepositoryBase<Person>
    {
        Task<Person> CreateAndSaveChangesAsync(Person person);
        Task<Person> UpdateAndSaveChangesAsync(Person person);
    }
}
