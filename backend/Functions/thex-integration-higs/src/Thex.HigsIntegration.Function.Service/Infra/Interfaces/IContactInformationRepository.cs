﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.HigsIntegration.Function.Service.Entities;

namespace Thex.HigsIntegration.Function.Service.Infra.Interfaces
{
    public interface IContactInformationRepository : IRepositoryBase<ContactInformation>
    {
        Task AddRangeAndSaveChangesAsync(IList<ContactInformation> contactInformationList);
        Task UpdateRangeAndSaveChangesAsync(Guid personId, IList<ContactInformation> contactInformationList);
    }
}
