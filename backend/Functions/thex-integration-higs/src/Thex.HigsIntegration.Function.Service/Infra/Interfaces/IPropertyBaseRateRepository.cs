﻿using System;
using System.Collections.Generic;
using System.Text;
using Thex.HigsIntegration.Function.Service.Entities;

namespace Thex.HigsIntegration.Function.Service.Infra.Interfaces
{
    public interface IPropertyBaseRateRepository
    {
        void CreateAndUpdate(List<PropertyBaseRate> propertyBaseRateToAddList, List<PropertyBaseRate> propertyBaseRateToUpdateList);
        void GetAndUpdate(int propertyId, List<int> roomTypeIdList, Guid currencyId, DateTime startDate, DateTime finalDate, int mealPlanTypeDefaultId);
    }
}
