﻿using System;
using System.Threading.Tasks;
using Thex.HigsIntegration.Function.Service.Entities;

namespace Thex.HigsIntegration.Function.Service.Infra.Interfaces
{
    public interface ILocationRepository : IRepositoryBase<Location>
    {
        Task CreateAndSaveChangesAsync(Location location);
        Task UpdateAndSaveChangesAsync(Location location, Guid ownerId);
    }
}
