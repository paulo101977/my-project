﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.HigsIntegration.Function.Service.Entities;

namespace Thex.HigsIntegration.Function.Service.Infra.Interfaces
{
    public interface ICountrySubdivisionRepository
    {
        CountrySubdivision InsertAndSaveChanges(CountrySubdivision entity);
    }
}
