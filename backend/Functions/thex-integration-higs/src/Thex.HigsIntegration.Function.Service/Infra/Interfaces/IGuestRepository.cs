﻿using System.Threading.Tasks;
using Thex.HigsIntegration.Function.Service.Entities;

namespace Thex.HigsIntegration.Function.Service.Infra.Interfaces
{
    public interface IGuestRepository : IRepositoryBase<Guest>
    {
        Task<Guest> CreateAndSaveChangesAsync(Guest guest);
        Task<Guest> UpdateAndSaveChangesAsync(Guest guest);
    }
}
