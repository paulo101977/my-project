﻿using System.Threading.Tasks;

namespace Thex.HigsIntegration.Function.Service.Infra.Interfaces
{
    public interface IRepositoryBase<TEntity> where TEntity : class
    {
        Task<TEntity> InsertAndSaveChangesAsync(TEntity entity);
    }
}
