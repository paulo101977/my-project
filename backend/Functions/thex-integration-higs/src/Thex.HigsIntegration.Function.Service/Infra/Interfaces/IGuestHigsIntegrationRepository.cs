﻿using System.Threading.Tasks;
using Thex.HigsIntegration.Function.Service.Entities;

namespace Thex.HigsIntegration.Function.Service.Infra.Interfaces
{
    public interface IGuestHigsIntegrationRepository : IRepositoryBase<GuestHigsIntegration>
    {
        Task<GuestHigsIntegration> CreateAndSaveChangesAsync(GuestHigsIntegration guestHigs);
    }
}
