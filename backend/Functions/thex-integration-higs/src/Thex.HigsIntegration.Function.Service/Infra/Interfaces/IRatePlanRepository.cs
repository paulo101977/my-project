﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.HigsIntegration.Function.Service.Entities;
using Tnf.Repositories;

namespace Thex.HigsIntegration.Function.Service.Infra.Interfaces
{
    public interface IRatePlanRepository : IRepository<RatePlan>
    {
        Task InsertRangeAndSaveChangesAsync(ICollection<RatePlan> ratePlanList);
    }
}
