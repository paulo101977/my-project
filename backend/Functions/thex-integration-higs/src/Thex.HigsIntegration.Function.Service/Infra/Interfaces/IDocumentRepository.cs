﻿using System.Threading.Tasks;
using Thex.HigsIntegration.Function.Service.Entities;

namespace Thex.HigsIntegration.Function.Service.Infra.Interfaces
{
    public interface IDocumentRepository : IRepositoryBase<Document>
    {
        Task<Document> CreateAndSaveChangesAsync(Document document);
        Task<Document> UpdateAndSaveChangesAsync(Document document);
    }
}
