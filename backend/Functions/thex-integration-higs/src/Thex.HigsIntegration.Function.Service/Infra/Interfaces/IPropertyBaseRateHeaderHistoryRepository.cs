﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Thex.HigsIntegration.Function.Service.Entities;

namespace Thex.HigsIntegration.Function.Service.Infra.Interfaces
{
    public interface IPropertyBaseRateHeaderHistoryRepository
    {
        Task<Guid> Create(PropertyBaseRateHeaderHistory propertyBaseRateHeaderHistory);
    }
}
