﻿using System.Threading.Tasks;
using Thex.HigsIntegration.Function.Service.Entities;

namespace Thex.HigsIntegration.Function.Service.Infra.Interfaces
{
    public interface IIntegrationPartnerPropertyHistoryRepository
    {
        Task InsertAndSaveChangesAsync(IntegrationPartnerPropertyHistory history);
    }
}
