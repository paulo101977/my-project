﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.HigsIntegration.Function.Service.Entities;

namespace Thex.HigsIntegration.Function.Service.Infra.Interfaces
{
    public interface IGuestReservationItemRepository
    {
        Task UpdateAllGuestsToCanceledAsync(long reservationItemId);
        Task RemoveRangeAsync(List<GuestReservationItem> guestReservationList);
        Task SaveRangeAndSaveChangesAsync(List<GuestReservationItem> guestReservationList);
        Task UpdateRangeAndSaveChangesAsync(List<GuestReservationItem> guestReservationList);
    }
}
