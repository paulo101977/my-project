﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Thex.HigsIntegration.Function.Service.Infra.Interfaces.Read
{
    public interface IPropertyParameterReadRepository
    {
        Task<(TimeSpan, TimeSpan)> GetCheckinAndCheckoutHourByPropertyId(int propertyId);
    }
}
