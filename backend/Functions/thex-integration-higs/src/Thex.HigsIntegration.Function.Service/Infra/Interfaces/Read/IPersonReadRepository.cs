﻿using System;
using System.Threading.Tasks;
using Thex.HigsIntegration.Function.Service.Entities;

namespace Thex.HigsIntegration.Function.Service.Infra.Interfaces.Read
{
    public interface IPersonReadRepository
    {
        Task<Person> GetByDocument(string document, int documentType);
        Task<Person> GetByHigsCode(string higsCode, int propertyId);
    }
}
