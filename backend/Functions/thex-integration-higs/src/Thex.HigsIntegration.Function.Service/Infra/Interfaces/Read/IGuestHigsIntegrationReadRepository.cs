﻿using System;
using System.Threading.Tasks;
using Thex.HigsIntegration.Function.Service.Entities;

namespace Thex.HigsIntegration.Function.Service.Infra.Interfaces.Read
{
    public interface IGuestHigsIntegrationReadRepository
    {
        Task<GuestHigsIntegration> GetByHigsCodeAsync(string higsCode, long propertyId);
    }
}
