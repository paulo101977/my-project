﻿
using System.Threading.Tasks;

namespace Thex.HigsIntegration.Function.Service.Infra.Interfaces.Read
{
    public interface IPropertyReadRepository
    {
        Task<string> GetParameterByPropertyIdAsync(int propertyId, int applicationParameterId);
    }
}
