﻿using System.Threading.Tasks;
using Thex.HigsIntegration.Function.Service.Entities;

namespace Thex.HigsIntegration.Function.Service.Infra.Interfaces.Read
{
    public interface ICurrencyReadRepository
    {
        Task<Currency> GetCurrencyByCurrencyNameAsync(string currencyName);
        Task<Currency> GetCurrencyByCurrencySymbolAsync(string symbol);
    }
}
