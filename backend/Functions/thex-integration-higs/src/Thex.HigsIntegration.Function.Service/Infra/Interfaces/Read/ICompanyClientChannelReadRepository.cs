﻿using System;
using System.Threading.Tasks;

namespace Thex.HigsIntegration.Function.Service.Infra.Interfaces.Read
{
    public interface ICompanyClientChannelReadRepository
    {
        Task<Guid?> GetCompanyClientIdByCompanyIdAsync(string companyId, Guid tenantId);
        Task<Guid?> GetCompanyClientIdByCompanyIdAndChannelCodeAsync(string channelCode, string companyId, Guid tenantId);
    }
}
