﻿
using System.Threading.Tasks;
using Thex.HigsIntegration.Function.Service.Entities;

namespace Thex.HigsIntegration.Function.Service.Infra.Interfaces.Read
{  
    public interface ITransportationTypeReadRepository
    {
        Task<TransportationType> GetByHigsCodeAsync(int higsCode);
    }
}