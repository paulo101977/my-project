﻿using System.Threading.Tasks;
using Thex.HigsIntegration.Function.Service.Entities;

namespace Thex.HigsIntegration.Function.Service.Infra.Interfaces.Read
{
    public interface IRatePlanReadRepository
    {
        Task<RatePlan> GetByDistributionCodeAsync(string distributionCode, int propertyId);
    }
    
}
