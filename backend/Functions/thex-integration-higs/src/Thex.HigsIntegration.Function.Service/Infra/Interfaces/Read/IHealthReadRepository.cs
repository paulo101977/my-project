﻿using System.Threading.Tasks;

namespace Thex.HigsIntegration.Function.Service.Infra.Interfaces.Read
{
    public interface IHealthReadRepository
    {
        Task<bool> AnyAsync();
    }
}
