﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.HigsIntegration.Function.Service.Entities;

namespace Thex.HigsIntegration.Function.Service.Infra.Interfaces.Read
{
    public interface IPropertyBaseRateReadRepository
    {
        Task<List<PropertyBaseRate>> GetAll(int mealPlanTypeId, Guid currencyId, List<DateTime> dateList, List<int> roomTypeIdList, Guid? ratePlanId = null);
        Task<List<PropertyBaseRate>> GetAllByPeriod(int propertyId, List<int> roomTypeId, Guid? currencyId, int? mealPlanTypeId, DateTime initialDate, DateTime finalDate, bool? include = null, bool? fixedRates = null, Guid? ratePlanId = null);
    }
}
