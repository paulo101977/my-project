﻿using System;
using System.Threading.Tasks;
using Thex.HigsIntegration.Function.Service.Entities;

namespace Thex.HigsIntegration.Function.Service.Infra.Interfaces.Read
{
    public interface IIntegrationPartnerPropertyReadRepository
    {
        Task<Property> GetPropertyByIntegrationCodeAsync(string integrationCode);
        Task<Guid> GetByIntegrationCodeAsync(string integrationCode);
    }
}
