﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.Common.Dto;
using Thex.HigsIntegration.Function.Service.Dto;
using Thex.HigsIntegration.Function.Service.Entities;
using Tnf.Dto;

namespace Thex.HigsIntegration.Function.Service.Infra.Interfaces.Read
{
    public interface ICountrySubdivisionTranslationReadRepository
    {
        //AddressTranslationDto GetAddressTranslationByCityId(int cityId, string languageIsoCode);

        //IListDto<CountryDto> GetAllCountriesByLanguageIsoCode(GetAllCountryDto request);

        List<BaseCountrySubdivisionTranslationDto> GetCountryWithTranslations(string countryTwoLetterIsoCode);

        List<BaseCountrySubdivisionTranslationDto> GetStateWithTranslations(string translationName, int parentSubdivionId);
        List<BaseCountrySubdivisionTranslationDto> GetStateWithTranslationsByCode(string twoLetterIsoCode, int parentSubdivionId);

        List<BaseCountrySubdivisionTranslationDto> GetCityWithTranslations(string translationName, int parentSubdivionId);
        
    }
}
