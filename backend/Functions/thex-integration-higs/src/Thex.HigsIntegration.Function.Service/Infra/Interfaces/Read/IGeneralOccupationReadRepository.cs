﻿
using System.Threading.Tasks;
using Thex.HigsIntegration.Function.Service.Entities;

namespace Thex.HigsIntegration.Function.Service.Infra.Interfaces.Read
{  
    public interface IGeneralOccupationReadRepository
    {
        Task<GeneralOccupation> GetByNameAsync(string occupationName);
    }
}