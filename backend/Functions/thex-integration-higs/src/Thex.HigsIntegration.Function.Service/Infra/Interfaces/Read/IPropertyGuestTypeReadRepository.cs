﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Thex.HigsIntegration.Function.Service.Infra.Interfaces.Read
{
    public interface IPropertyGuestTypeReadRepository
    {
        Task<int> GetPropertyGuestTypeDefaultAsync(int propertyId);
    }
}
