﻿using System.Threading.Tasks;
using Thex.HigsIntegration.Function.Service.Entities;
using Thex.HigsIntegration.Function.Service.Infra.Context;

namespace Thex.HigsIntegration.Function.Service.Infra.Interfaces.Read
{
    public interface IReservationReadRepository
    {
        Task<Reservation> GetByExternalReservationNumberAsync(string partnerReservationNumber);
        ThexHigsIntegrationFunctionContext Context { get; }
    }
}
