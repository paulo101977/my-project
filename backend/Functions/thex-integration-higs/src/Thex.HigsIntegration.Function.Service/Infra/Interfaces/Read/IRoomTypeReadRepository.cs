﻿
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.HigsIntegration.Function.Service.Entities;

namespace Thex.HigsIntegration.Function.Service.Infra.Interfaces.Read
{  
    public interface IRoomTypeReadRepository
    {
        Task<int> GetRoomTypeIdByDistributionCodeAsync(string code, int propertyId);
        Task<ICollection<RoomType>> GetAllRoomTypesByPropertyIdAsync(int propertyId);
    }
}