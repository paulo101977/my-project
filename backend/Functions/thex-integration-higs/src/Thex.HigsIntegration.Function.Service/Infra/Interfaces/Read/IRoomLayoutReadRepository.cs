﻿using System;
using System.Threading.Tasks;

namespace Thex.HigsIntegration.Function.Service.Infra.Interfaces.Read
{
    public interface IRoomLayoutReadRepository
    {
        Task<Guid> GetRoomLayoutDefaultAsync();
    }
}
