﻿using System;

namespace Thex.HigsIntegration.Function.Service.Infra
{
    public class DatabaseConfiguration
    {
        public DatabaseConfiguration()
        {
            DefaultSchema = "";
            ConnectionStringName = "";
            DatabaseType = DatabaseType.SqlServer;
            ConnectionString = System.Environment.GetEnvironmentVariable("ThexConnectionString", EnvironmentVariableTarget.Process);
        }

        public string DefaultSchema { get; }
        public string ConnectionStringName { get; }
        public string ConnectionString { get; }
        public DatabaseType DatabaseType { get; }
    }

    public enum DatabaseType
    {
        SqlServer,
        Sqlite,
        Oracle
    }
}
