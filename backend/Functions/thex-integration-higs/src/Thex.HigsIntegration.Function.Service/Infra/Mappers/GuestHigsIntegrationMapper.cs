﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Thex.HigsIntegration.Function.Service.Entities;

namespace Thex.HigsIntegration.Function.Service.Infra.Mappers
{
    public class GuestHigsIntegrationMapper : IEntityTypeConfiguration<GuestHigsIntegration>
    {
        public void Configure(EntityTypeBuilder<GuestHigsIntegration> builder)
        {
            builder.ToTable("GuestHigsIntegration");

            builder.HasAnnotation("Relational:TableName", "GuestHigsIntegration");

            builder.Property(e => e.Id)
                .HasAnnotation("Relational:ColumnName", "GuestHigsIntegrationId");

            builder.Property(e => e.HigsCode)
                .HasMaxLength(100)
                .IsUnicode(false)
                .HasAnnotation("Relational:ColumnName", "HigsCode");

            builder.HasOne(d => d.Guest)
                .WithMany(p => p.GuestHigsIntegrationList)
                .HasForeignKey(d => d.GuestId)
                .HasConstraintName("FK_GuestHigsIntegration_Guest");

            builder.HasOne(d => d.Property)
                .WithMany(p => p.GuestHigsIntegrationList)
                .HasForeignKey(d => d.PropertyId)
                .HasConstraintName("FK_GuestHigsIntegration_Property");
        }
    }
}
