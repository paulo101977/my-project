﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Thex.HigsIntegration.Function.Service.Entities;

namespace Thex.HigsIntegration.Function.Service.Infra.Mappers
{
    public class RatePlanMapper : IEntityTypeConfiguration<RatePlan>
    {
        public void Configure(EntityTypeBuilder<RatePlan> builder)
        {

            builder.ToTable("RatePlan");

            builder.HasAnnotation("Relational:TableName", "RatePlan");

            builder.HasIndex(e => e.AgreementTypeId)
                .HasName("x_RatePlan_AgreementTypeId");

            builder.HasIndex(e => e.CurrencyId)
                .HasName("x_RatePlan_CurrencyId");

            builder.HasIndex(e => e.PropertyId)
                .HasName("x_RatePlan_PropertyId");

            builder.HasIndex(e => e.MealPlanTypeId)
                .HasName("x_RatePlan_MealPlanTypeId");

            builder.HasIndex(e => new { e.PropertyId, e.AgreementName })
                .HasName("UK_RatePlan")
                .IsUnique();

            builder.Property(e => e.Id)
                .HasAnnotation("Relational:ColumnName", "RatePlanId");

            builder.Property(e => e.AgreementName)
                .IsRequired()
                .HasMaxLength(100)
                .IsUnicode(false)
                .HasAnnotation("Relational:ColumnName", "AgreementName");

            builder.Property(e => e.AgreementTypeId).HasAnnotation("Relational:ColumnName", "AgreementTypeId");

            builder.Property(e => e.CurrencyId).HasAnnotation("Relational:ColumnName", "CurrencyId");

            builder.Property(e => e.CurrencySymbol)
                .HasMaxLength(10)
                .IsUnicode(false)
                .HasAnnotation("Relational:ColumnName", "CurrencySymbol");

            builder.Property(e => e.EndDate)
                .HasColumnType("datetime")
                .HasAnnotation("Relational:ColumnName", "EndDate");

            builder.Property(e => e.IsActive)
                .HasDefaultValueSql("((1))")
                .HasAnnotation("Relational:ColumnName", "IsActive");

            builder.Property(e => e.PropertyId).HasAnnotation("Relational:ColumnName", "PropertyId");

            builder.Property(e => e.MealPlanTypeId).HasAnnotation("Relational:ColumnName", "MealPlanTypeId");

            builder.Property(e => e.RateNet).HasAnnotation("Relational:ColumnName", "RateNet");

            builder.Property(e => e.RateTypeId).HasAnnotation("Relational:ColumnName", "RateTypeId");

            builder.Property(e => e.StartDate)
                .HasColumnType("datetime")
                .HasAnnotation("Relational:ColumnName", "StartDate");

        }
    }
}
