﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Thex.HigsIntegration.Function.Service.Entities;

namespace Thex.HigsIntegration.Function.Service.Infra.Mappers
{
    public class LocationMapper : IEntityTypeConfiguration<Location>
    {
        public void Configure(EntityTypeBuilder<Location> builder)
        {
            
            builder.ToTable("Location");

            builder.HasAnnotation("Relational:TableName", "Location");

            builder.HasIndex(e => e.CityId)
                .HasName("x_Location_CityId");

            builder.HasIndex(e => e.StateId)
                .HasName("x_Location_StateId");

            builder.HasIndex(e => e.CountryId)
                .HasName("x_Location_CountryId");

            builder.HasIndex(e => e.CountryCode)
                .HasName("x_Location_CountrySubdivisionId");

            builder.HasIndex(e => e.LocationCategoryId)
                .HasName("x_Location_LocationCategoryId");

            builder.HasKey(e => e.Id);
            builder.Property(e => e.Id).HasAnnotation("Relational:ColumnName", "LocationId").ValueGeneratedOnAdd();

            builder.Property(e => e.AdditionalAddressDetails)
                .HasMaxLength(50)
                .IsUnicode(false)
                .HasAnnotation("Relational:ColumnName", "AdditionalAddressDetails");

            builder.Property(e => e.CityId).HasAnnotation("Relational:ColumnName", "CityId");
            builder.Property(e => e.StateId).HasAnnotation("Relational:ColumnName", "StateId");
            builder.Property(e => e.CountryId).HasAnnotation("Relational:ColumnName", "CountryId");

            builder.Property(e => e.CountryCode)
                .IsRequired()
                .HasColumnType("char(2)")
                .HasAnnotation("Relational:ColumnName", "CountryCode");

            builder.Property(e => e.Latitude)
                .HasColumnType("decimal(9, 6)")
                .HasAnnotation("Relational:ColumnName", "Latitude");

            builder.Property(e => e.LocationCategoryId)
                .HasDefaultValueSql("((1))")
                .HasAnnotation("Relational:ColumnName", "LocationCategoryId");

            builder.Property(e => e.Longitude)
                .HasColumnType("decimal(9, 6)")
                .HasAnnotation("Relational:ColumnName", "Longitude");

            builder.Property(e => e.Neighborhood)
                .HasMaxLength(100)
                .IsUnicode(false)
                .HasAnnotation("Relational:ColumnName", "Neighborhood");

            builder.Property(e => e.OwnerId).HasAnnotation("Relational:ColumnName", "OwnerId");

            builder.Property(e => e.PostalCode).HasAnnotation("Relational:ColumnName", "PostalCode");

            builder.Property(e => e.StreetName)
                .IsRequired()
                .HasMaxLength(200)
                .IsUnicode(false)
                .HasAnnotation("Relational:ColumnName", "StreetName");

            builder.Property(e => e.StreetNumber)
                .IsRequired()
                .HasMaxLength(50)
                .IsUnicode(false)
                .HasAnnotation("Relational:ColumnName", "StreetNumber");

            builder.HasOne(d => d.City)
                .WithMany(p => p.LocationList)
                .HasForeignKey(d => d.CityId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_Location_CountrySubdivision");

            builder.HasOne(d => d.State)
                .WithMany(p => p.LocationStateList)
                .HasForeignKey(d => d.StateId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_Location_CountrySubdivision_State");

            builder.HasOne(d => d.CountryEntity)
                .WithMany(p => p.LocationCountryList)
                .HasForeignKey(d => d.CountryId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_Location_CountrySubdivision_Country");
            
            builder.HasOne(d => d.Property)
                .WithMany(p => p.LocationList)
                .HasForeignKey(d => d.OwnerId)
                .HasPrincipalKey(d => d.PropertyUId)
                .OnDelete(DeleteBehavior.Restrict);

            builder.HasOne(d => d.Person)
                .WithMany(p => p.LocationList)
                .HasForeignKey(d => d.OwnerId)
                .HasPrincipalKey(d => d.Id)
                .OnDelete(DeleteBehavior.Restrict);

        }
    }
}
