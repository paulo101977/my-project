﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Thex.HigsIntegration.Function.Service.Entities;

namespace Thex.HigsIntegration.Function.Service.Infra.Mappers
{
    public class IntegrationPartnerPropertyMapper : IEntityTypeConfiguration<IntegrationPartnerProperty>
    {
        public void Configure(EntityTypeBuilder<IntegrationPartnerProperty> builder)
        {
            builder.ToTable("IntegrationPartnerProperty");

            builder.HasAnnotation("Relational:TableName", "IntegrationPartnerProperty");

            builder.Property(e => e.Id)
                .ValueGeneratedNever()
                .HasAnnotation("Relational:ColumnName", "IntegrationPartnerPropertyId");

            builder.HasOne(e => e.Property)
               .WithMany(p => p.IntegrationPartnerPropertyList)
               .HasForeignKey(e => e.PropertyId)
               .HasConstraintName("FK_IntegrationPartnerProperty_Property");
        }
    }
}
