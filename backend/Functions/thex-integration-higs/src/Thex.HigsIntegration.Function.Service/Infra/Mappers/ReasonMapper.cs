﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Thex.HigsIntegration.Function.Service.Entities;

namespace Thex.HigsIntegration.Function.Service.Infra.Mappers
{
    public class ReasonMapper : IEntityTypeConfiguration<Reason>
    {
        public void Configure(EntityTypeBuilder<Reason> builder)
        {
            
            builder.ToTable("Reason");

            builder.HasAnnotation("Relational:TableName", "Reason");

            builder.HasIndex(e => e.ChainId)
                .HasName("x_Reason_ChainId");

            builder.HasIndex(e => e.ReasonCategoryId)
                .HasName("x_Reason_ReasonCategoryId");

            builder.Property(e => e.Id)
                .ValueGeneratedOnAdd()
                .HasAnnotation("Relational:ColumnName", "ReasonId");

            builder.Property(e => e.ChainId).HasAnnotation("Relational:ColumnName", "ChainId");

            builder.Property(e => e.IsActive).HasAnnotation("Relational:ColumnName", "IsActive");

            builder.Property(e => e.ReasonCategoryId).HasAnnotation("Relational:ColumnName", "ReasonCategoryId");

            builder.Property(e => e.HigsCode).HasAnnotation("Relational:ColumnName", "HigsCode");

            builder.Property(e => e.ReasonName)
                .IsRequired()
                .HasMaxLength(100)
                .IsUnicode(false)
                .HasAnnotation("Relational:ColumnName", "ReasonName");
        }
    }
}
