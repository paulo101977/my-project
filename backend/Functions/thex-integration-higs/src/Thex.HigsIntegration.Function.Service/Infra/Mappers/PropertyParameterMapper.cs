﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Thex.HigsIntegration.Function.Service.Entities;

namespace Thex.HigsIntegration.Function.Service.Infra.Mappers
{
    public class PropertyParameterMapper : IEntityTypeConfiguration<PropertyParameter>
    {
        public void Configure(EntityTypeBuilder<PropertyParameter> builder)
        {

            builder.ToTable("PropertyParameter");

            builder.HasAnnotation("Relational:TableName", "PropertyParameter");

            builder.HasIndex(e => e.ApplicationParameterId)
                .HasName("x_PropertyParameter_ApplicationParameterId");

            builder.HasIndex(e => e.PropertyId)
                .HasName("x_PropertyParameter_PropertyId");

            builder.Property(e => e.Id)
                .HasAnnotation("Relational:ColumnName", "PropertyParameterId");

            builder.Property(e => e.ApplicationParameterId).HasAnnotation("Relational:ColumnName", "ApplicationParameterId");

            builder.Property(e => e.IsActive).HasAnnotation("Relational:ColumnName", "IsActive");

            builder.Property(e => e.PropertyId).HasAnnotation("Relational:ColumnName", "PropertyId");

            builder.Property(e => e.PropertyParameterMaxValue)
                .HasMaxLength(100)
                .IsUnicode(false)
                .HasAnnotation("Relational:ColumnName", "PropertyParameterMaxValue");

            builder.Property(e => e.PropertyParameterMinValue)
                .HasMaxLength(100)
                .IsUnicode(false)
                .HasAnnotation("Relational:ColumnName", "PropertyParameterMinValue");

            builder.Property(e => e.PropertyParameterPossibleValues)
                .IsUnicode(false)
                .HasAnnotation("Relational:ColumnName", "PropertyParameterPossibleValues");

            builder.Property(e => e.PropertyParameterValue)
                .IsUnicode(false)
                .HasAnnotation("Relational:ColumnName", "PropertyParameterValue");
        }
    }
}
