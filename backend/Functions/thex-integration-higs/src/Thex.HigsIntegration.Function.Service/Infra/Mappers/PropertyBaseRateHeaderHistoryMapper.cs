﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Thex.HigsIntegration.Function.Service.Entities;

namespace Thex.HigsIntegration.Function.Service.Infra.Mappers
{
    public class PropertyBaseRateHeaderHistoryMapper : IEntityTypeConfiguration<PropertyBaseRateHeaderHistory>
    {
        public void Configure(EntityTypeBuilder<PropertyBaseRateHeaderHistory> builder)
        {
            builder.ToTable("PropertyBaseRateHeaderHistory");

            builder.HasAnnotation("Relational:TableName", "PropertyBaseRateHeaderHistory");

            builder.HasIndex(e => e.CurrencyId)
                .HasName("x_PropertyBaseRateHeaderHistory_CurrencyId");

            builder.HasIndex(e => e.MealPlanTypeDefaultId)
                .HasName("x_PropertyBaseRateHeaderHistory_MealPlanTypeId");

            builder.HasIndex(e => e.Id)
                .HasName("x_PropertyBaseRateHistory_PropertyBaseRateHeaderHistoryId");

            builder.HasIndex(e => e.PropertyId)
                .HasName("x_PropertyBaseRateHeaderHistory_PropertyId");

            builder.HasIndex(e => e.RatePlanId)
                .HasName("x_PropertyBaseRateHeaderHistory_RatePlanId");

            builder.HasIndex(e => e.TenantId)
                .HasName("x_PropertyBaseRateHeaderHistory_TenantId");

            builder.Property(e => e.Id)
                .ValueGeneratedNever()
                .HasAnnotation("Relational:ColumnName", "PropertyBaseRateHeaderHistoryId");

            builder.Property(e => e.CreationTime)
                .HasColumnType("datetime")
                .HasDefaultValueSql("(getutcdate())")
                .HasAnnotation("Relational:ColumnName", "CreationTime");

            builder.Property(e => e.CreatorUserId).HasAnnotation("Relational:ColumnName", "CreatorUserId");

            builder.Property(e => e.CurrencyId).HasAnnotation("Relational:ColumnName", "CurrencyId");

            builder.Property(e => e.DeleterUserId).HasAnnotation("Relational:ColumnName", "DeleterUserId");

            builder.Property(e => e.DeletionTime)
                .HasColumnType("datetime")
                .HasAnnotation("Relational:ColumnName", "DeletionTime");

            builder.Property(e => e.FinalDate)
                .HasColumnType("datetime")
                .HasAnnotation("Relational:ColumnName", "FinalDate");

            builder.Property(e => e.Friday).HasAnnotation("Relational:ColumnName", "Friday");

            builder.Property(e => e.InitialDate)
                .HasColumnType("datetime")
                .HasAnnotation("Relational:ColumnName", "InitialDate");

            builder.Property(e => e.IsDeleted).HasAnnotation("Relational:ColumnName", "IsDeleted");

            builder.Property(e => e.LastModificationTime)
                .HasColumnType("datetime")
                .HasDefaultValueSql("(getutcdate())")
                .HasAnnotation("Relational:ColumnName", "LastModificationTime");

            builder.Property(e => e.LastModifierUserId).HasAnnotation("Relational:ColumnName", "LastModifierUserId");

            builder.Property(e => e.MealPlanTypeDefaultId).HasAnnotation("Relational:ColumnName", "MealPlanTypeDefaultId");

            builder.Property(e => e.Monday).HasAnnotation("Relational:ColumnName", "Monday");

            builder.Property(e => e.PropertyId).HasAnnotation("Relational:ColumnName", "PropertyId");

            builder.Property(e => e.RatePlanId).HasAnnotation("Relational:ColumnName", "RatePlanId");

            builder.Property(e => e.Saturday).HasAnnotation("Relational:ColumnName", "Saturday");

            builder.Property(e => e.Sunday).HasAnnotation("Relational:ColumnName", "Sunday");

            builder.Property(e => e.TenantId).HasAnnotation("Relational:ColumnName", "TenantId");

            builder.Property(e => e.Thursday).HasAnnotation("Relational:ColumnName", "Thursday");

            builder.Property(e => e.Tuesday).HasAnnotation("Relational:ColumnName", "Tuesday");

            builder.Property(e => e.Wednesday).HasAnnotation("Relational:ColumnName", "Wednesday");

        }
    }
}
