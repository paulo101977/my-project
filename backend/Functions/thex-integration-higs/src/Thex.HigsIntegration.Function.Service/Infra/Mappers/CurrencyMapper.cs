﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Thex.HigsIntegration.Function.Service.Entities;

namespace Thex.HigsIntegration.Function.Service.Infra.Mappers
{
    public class CurrencyMapper : IEntityTypeConfiguration<Currency>
    {
        public void Configure(EntityTypeBuilder<Currency> builder)
        {
            builder.HasKey(e => e.Id);

            builder.ToTable("Currency");

            builder.HasAnnotation("Relational:TableName", "Currency");

            builder.Property(e => e.Id)
                .HasAnnotation("Relational:ColumnName", "CurrencyId");

            builder.Property(e => e.AlphabeticCode)
                .IsRequired()
                .HasMaxLength(10)
                .IsUnicode(false)
                .HasAnnotation("Relational:ColumnName", "AlphabeticCode");

            builder.Property(e => e.MinorUnit).HasAnnotation("Relational:ColumnName", "MinorUnit");

            builder.Property(e => e.NumnericCode)
                .IsRequired()
                .HasMaxLength(10)
                .IsUnicode(false)
                .HasAnnotation("Relational:ColumnName", "NumnericCode");

            builder.Property(e => e.Symbol)
                .IsRequired()
                .HasMaxLength(10)
                .IsUnicode(false)
                .HasAnnotation("Relational:ColumnName", "Symbol");

            builder.Property(e => e.TwoLetterIsoCode)
                .HasColumnType("char(2)")
                .HasAnnotation("Relational:ColumnName", "TwoLetterIsoCode");
        }
    }
}
