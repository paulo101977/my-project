﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Thex.HigsIntegration.Function.Service.Entities;

namespace Thex.HigsIntegration.Function.Service.Infra.Mappers
{
    public class ReservationItemMapper : IEntityTypeConfiguration<ReservationItem>
    {
        public void Configure(EntityTypeBuilder<ReservationItem> builder)
        {
            builder.ToTable("ReservationItem");

            builder.HasAnnotation("Relational:TableName", "ReservationItem");

            builder.HasIndex(e => e.ReceivedRoomTypeId)
                .HasName("x_ReservationItem_ReceivedRoomTypeId");

            builder.HasIndex(e => e.RequestedRoomTypeId)
                .HasName("x_ReservationItem_RequestedRoomTypeId");

            builder.HasIndex(e => e.ReservationId)
                .HasName("x_ReservationItem_ReservationId");

            builder.HasIndex(e => e.ReservationItemStatusId)
                .HasName("x_ReservationItem_ReservationItemStatusId");

            builder.HasIndex(e => e.RoomId)
                .HasName("x_ReservationItem_RoomId");

            builder.HasIndex(e => e.RoomLayoutId)
                .HasName("x_ReservationItem_RoomLayoutId");

            builder.HasKey(e => e.Id);

            builder.Property(e => e.Id).HasAnnotation("Relational:ColumnName", "ReservationItemId").ValueGeneratedOnAdd();

            builder.Property(e => e.AdultCount).HasAnnotation("Relational:ColumnName", "AdultCount");

            builder.Property(e => e.CancellationDate)
                .HasColumnType("datetime")
                .HasAnnotation("Relational:ColumnName", "CancellationDate");

            builder.Property(e => e.CancellationDescription)
                .HasMaxLength(1000)
                .IsUnicode(false)
                .HasAnnotation("Relational:ColumnName", "CancellationDescription");

            builder.Property(e => e.CheckInDate)
                .HasColumnType("datetime")
                .HasAnnotation("Relational:ColumnName", "CheckInDate");

            builder.Property(e => e.CheckOutDate)
                .HasColumnType("datetime")
                .HasAnnotation("Relational:ColumnName", "CheckOutDate");

            builder.Property(e => e.ChildCount).HasAnnotation("Relational:ColumnName", "ChildCount");

            builder.Property(e => e.EstimatedArrivalDate)
                .HasColumnType("datetime")
                .HasAnnotation("Relational:ColumnName", "EstimatedArrivalDate");

            builder.Property(e => e.EstimatedDepartureDate)
                .HasColumnType("datetime")
                .HasAnnotation("Relational:ColumnName", "EstimatedDepartureDate");

            builder.Property(e => e.ExtraBedCount)
                .HasDefaultValueSql("((0))")
                .HasAnnotation("Relational:ColumnName", "ExtraBedCount");

            builder.Property(e => e.ExtraCribCount)
                .HasDefaultValueSql("((0))")
                .HasAnnotation("Relational:ColumnName", "ExtraCribCount");

            builder.Property(e => e.ReservationItemCode)
                .IsRequired()
                .HasMaxLength(50)
                .IsUnicode(false)
                .HasAnnotation("Relational:ColumnName", "ReservationItemCode");

            builder.Property(e => e.ReservationItemUid).HasAnnotation("Relational:ColumnName", "ReservationItemUid");

            builder.Property(e => e.RatePlanId).HasAnnotation("Relational:ColumnName", "RatePlanId");

            builder.Property(e => e.CurrencyId).HasAnnotation("Relational:ColumnName", "CurrencyId");

            builder.Property(e => e.MealPlanTypeId).HasAnnotation("Relational:ColumnName", "MealPlanTypeId");

            builder.HasOne(e => e.Reservation)
            .WithMany(p => p.ReservationItemList)
            .HasForeignKey(e => e.ReservationId)
            .HasConstraintName("FK_ReservationItem_Reservation");
        }
    }
}
