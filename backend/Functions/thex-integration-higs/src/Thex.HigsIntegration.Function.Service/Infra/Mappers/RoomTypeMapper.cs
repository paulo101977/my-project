﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Thex.HigsIntegration.Function.Service.Entities;

namespace Thex.HigsIntegration.Function.Service.Infra.Mappers
{
    public class RoomTypeMapper : IEntityTypeConfiguration<RoomType>
    {
        public void Configure(EntityTypeBuilder<RoomType> builder)
        {
            builder.ToTable("RoomType");

            builder.HasAnnotation("Relational:TableName", "RoomType");

            builder.HasIndex(e => e.PropertyId)
                .HasName("x_RoomType_PropertyId");

            builder.HasIndex(e => new { e.PropertyId, e.Abbreviation })
                .HasName("UK_RoomType_Abbreviation")
                .IsUnique();

            builder.Property(e => e.Id).HasAnnotation("Relational:ColumnName", "RoomTypeId");

            builder.Property(e => e.Abbreviation)
                .IsRequired()
                .HasMaxLength(64)
                .IsUnicode(false)
                .HasAnnotation("Relational:ColumnName", "Abbreviation");

            builder.Property(e => e.AdultCapacity).HasAnnotation("Relational:ColumnName", "AdultCapacity");

            builder.Property(e => e.ChildCapacity).HasAnnotation("Relational:ColumnName", "ChildCapacity");

            builder.Property(e => e.FreeChildQuantity1)
                .HasColumnName("FreeChildQuantity_1")
                .HasDefaultValueSql("((0))")
                .HasAnnotation("Relational:ColumnName", "FreeChildQuantity_1");

            builder.Property(e => e.FreeChildQuantity2)
                .HasColumnName("FreeChildQuantity_2")
                .HasDefaultValueSql("((0))")
                .HasAnnotation("Relational:ColumnName", "FreeChildQuantity_2");

            builder.Property(e => e.FreeChildQuantity3)
                .HasColumnName("FreeChildQuantity_3")
                .HasDefaultValueSql("((0))")
                .HasAnnotation("Relational:ColumnName", "FreeChildQuantity_3");

            builder.Property(e => e.IsActive).HasAnnotation("Relational:ColumnName", "IsActive");

            builder.Property(e => e.Name)
                .IsRequired()
                .HasMaxLength(100)
                .IsUnicode(false)
                .HasAnnotation("Relational:ColumnName", "Name");

            builder.Property(e => e.Order).HasAnnotation("Relational:ColumnName", "Order");

            builder.Property(e => e.PropertyId).HasAnnotation("Relational:ColumnName", "PropertyId");

            builder.Property(e => e.MaximumRate)
            .IsRequired()
            .HasColumnType("numeric(12, 4)")
            .IsUnicode(false)
            .HasAnnotation("Relational:ColumnName", "MaximumRate");

            builder.Property(e => e.MinimumRate)
            .IsRequired()
            .HasColumnType("numeric(12, 4)")
            .IsUnicode(false)
            .HasAnnotation("Relational:ColumnName", "MinimumRate");

            builder.Property(e => e.DistributionCode)
            .IsRequired()
            .HasMaxLength(100)
            .HasAnnotation("Relational:ColumnName", "DistributionCode");

        }
    }
}
