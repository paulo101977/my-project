﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Thex.HigsIntegration.Function.Service.Entities;

namespace Thex.HigsIntegration.Function.Service.Infra.Mappers
{
    public class RatePlanCompanyClientMapper : IEntityTypeConfiguration<RatePlanCompanyClient>
    {
        public void Configure(EntityTypeBuilder<RatePlanCompanyClient> builder)
        {
            builder.ToTable("RatePlanCompanyClient");

            builder.HasAnnotation("Relational:TableName", "RatePlanCompanyClient");

            builder.HasIndex(e => e.CompanyClientId)
                .HasName("x_RatePlanCompanyClient_CompanyClientId");

            builder.HasIndex(e => e.RatePlanId)
                .HasName("x_RatePlanCompanyClient_RatePlanId");

            builder.HasIndex(e => e.TenantId)
                .HasName("x_RatePlanCompanyClient_TenantId");

            builder.Property(e => e.Id)
                .HasAnnotation("Relational:ColumnName", "RatePlanCompanyClientId");

            builder.Property(e => e.CompanyClientId).HasAnnotation("Relational:ColumnName", "CompanyClientId");

            builder.Property(e => e.CreationTime)
                .HasColumnType("datetime")
                .HasDefaultValueSql("(getutcdate())")
                .HasAnnotation("Relational:ColumnName", "CreationTime");

            builder.Property(e => e.CreatorUserId).HasAnnotation("Relational:ColumnName", "CreatorUserId");

            builder.Property(e => e.DeleterUserId).HasAnnotation("Relational:ColumnName", "DeleterUserId");

            builder.Property(e => e.DeletionTime)
                .HasColumnType("datetime")
                .HasAnnotation("Relational:ColumnName", "DeletionTime");

            builder.Property(e => e.IsDeleted).HasAnnotation("Relational:ColumnName", "IsDeleted");

            builder.Property(e => e.LastModificationTime)
                .HasColumnType("datetime")
                .HasDefaultValueSql("(getutcdate())")
                .HasAnnotation("Relational:ColumnName", "LastModificationTime");

            builder.Property(e => e.LastModifierUserId).HasAnnotation("Relational:ColumnName", "LastModifierUserId");

            builder.Property(e => e.RatePlanId).HasAnnotation("Relational:ColumnName", "RatePlanId");

            builder.Property(e => e.TenantId).HasAnnotation("Relational:ColumnName", "TenantId");

            builder.HasOne(d => d.RatePlan)
                .WithMany(p => p.RatePlanCompanyClientList)
                .HasForeignKey(d => d.RatePlanId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_RatePlanCompanyClient_RatePlan");
        }
    }
}
