﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Thex.HigsIntegration.Function.Service.Entities;

namespace Thex.HigsIntegration.Function.Service.Infra.Mappers
{
    public class ChannelCodeMapper : IEntityTypeConfiguration<ChannelCode>
    {
        public void Configure(EntityTypeBuilder<ChannelCode> builder)
        {

            builder.ToTable("ChannelCode");

            builder.HasAnnotation("Relational:TableName", "ChannelCode");

            builder.Property(e => e.Id).HasAnnotation("Relational:ColumnName", "ChannelCodeId");

            builder.Property(e => e.ChannelCode1)
                .IsRequired()
                .HasColumnName("ChannelCode")
                .HasMaxLength(20)
                .IsUnicode(false)
                .HasAnnotation("Relational:ColumnName", "ChannelCode");

            builder.Property(e => e.ChannelCodeDescription)
                .HasMaxLength(200)
                .IsUnicode(false)
                .HasAnnotation("Relational:ColumnName", "ChannelCodeDescription");

        }
    }
}
