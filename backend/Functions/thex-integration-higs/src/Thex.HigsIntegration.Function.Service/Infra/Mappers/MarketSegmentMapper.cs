﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using Thex.HigsIntegration.Function.Service.Entities;

namespace Thex.HigsIntegration.Function.Service.Infra.Mappers
{
    public class MarketSegmentMapper : IEntityTypeConfiguration<MarketSegment>
    {
        public void Configure(EntityTypeBuilder<MarketSegment> builder)
        {
            builder.ToTable("MarketSegment");

            builder.HasAnnotation("Relational:TableName", "MarketSegment");

            builder.HasIndex(e => e.ParentMarketSegmentId)
                .HasName("x_MarketSegment_ParentMarketSegmentId");

            builder.Property(e => e.Id).HasAnnotation("Relational:ColumnName", "MarketSegmentId").ValueGeneratedOnAdd();

            builder.Property(e => e.Code)
                .IsRequired()
                .HasMaxLength(20)
                .IsUnicode(false)
                .HasAnnotation("Relational:ColumnName", "Code");

            builder.Property(e => e.Name)
                .IsRequired()
                .HasMaxLength(50)
                .IsUnicode(false)
                .HasAnnotation("Relational:ColumnName", "Name");

            builder.Property(e => e.ParentMarketSegmentId).HasAnnotation("Relational:ColumnName", "ParentMarketSegmentId");



            builder.Property(e => e.CreationTime)
                .HasColumnType("datetime")
                .HasDefaultValueSql("(getutcdate())")
                .HasAnnotation("Relational:ColumnName", "CreationTime");

            builder.Property(e => e.CreatorUserId).HasAnnotation("Relational:ColumnName", "CreatorUserId");

            builder.Property(e => e.DeleterUserId).HasAnnotation("Relational:ColumnName", "DeleterUserId");

            builder.Property(e => e.DeletionTime)
                .HasColumnType("datetime")
                .HasAnnotation("Relational:ColumnName", "DeletionTime");

            builder.Property(e => e.IsActive)
                .HasAnnotation("Relational:ColumnName", "IsActive");

            builder.Property(e => e.IsDeleted).HasAnnotation("Relational:ColumnName", "IsDeleted");

            builder.Property(e => e.LastModificationTime)
                .HasColumnType("datetime")
                .HasDefaultValueSql("(getutcdate())")
                .HasAnnotation("Relational:ColumnName", "LastModificationTime");

            builder.Property(e => e.LastModifierUserId).HasAnnotation("Relational:ColumnName", "LastModifierUserId");

            builder.Property(e => e.SegmentAbbreviation)
                .IsRequired()
                .HasMaxLength(30)
                .IsUnicode(false)
                .HasAnnotation("Relational:ColumnName", "SegmentAbbreviation");
        }
    }
}
