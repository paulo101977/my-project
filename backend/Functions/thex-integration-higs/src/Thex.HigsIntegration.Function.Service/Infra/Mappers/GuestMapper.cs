﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Thex.HigsIntegration.Function.Service.Entities;

namespace Thex.HigsIntegration.Function.Service.Infra.Mappers
{
    public class GuestMapper : IEntityTypeConfiguration<Guest>
    {
        public void Configure(EntityTypeBuilder<Guest> builder)
        {
            
            builder.ToTable("Guest");

            builder.HasAnnotation("Relational:TableName", "Guest");

            builder.HasIndex(e => e.PersonId)
                .HasName("x_Guest_PersonId");
            
            builder.Property(e => e.Id)
                .ValueGeneratedOnAdd()
                .HasAnnotation("Relational:ColumnName", "GuestId");

            builder.Property(e => e.PersonId).HasAnnotation("Relational:ColumnName", "PersonId");

            builder.HasOne(d => d.Person)
                .WithMany(p => p.GuestList)
                .HasForeignKey(d => d.PersonId)
                .HasConstraintName("FK_Guest_Person");

        }
    }
}
