﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Thex.HigsIntegration.Function.Service.Entities;

namespace Thex.HigsIntegration.Function.Service.Infra.Mappers
{
    public class ReservationConfirmationMapper : IEntityTypeConfiguration<ReservationConfirmation>
    {
        public void Configure(EntityTypeBuilder<ReservationConfirmation> builder)
        {
            builder.ToTable("ReservationConfirmation");

            builder.HasAnnotation("Relational:TableName", "ReservationConfirmation");

            builder.HasIndex(e => e.BillingClientId)
                .HasName("x_ReservationConfirmation_BillingClientId");

            builder.HasIndex(e => e.PaymentTypeId)
                .HasName("x_ReservationConfirmation_PaymentTypeId");

            builder.HasIndex(e => e.PlasticId)
                .HasName("x_ReservationConfirmation_PlasticId");

            builder.HasIndex(e => e.ReservationId)
                .HasName("x_ReservationConfirmation_ReservationId");

            builder.Property(e => e.Id)
                .HasAnnotation("Relational:ColumnName", "ReservationConfirmationId");

            builder.Property(e => e.BillingClientId).HasAnnotation("Relational:ColumnName", "BillingClientId");

            builder.Property(e => e.Deadline)
                .HasColumnType("date")
                .HasAnnotation("Relational:ColumnName", "Deadline");

            builder.Property(e => e.PaymentTypeId).HasAnnotation("Relational:ColumnName", "PaymentTypeId");

            builder.Property(e => e.PlasticId).HasAnnotation("Relational:ColumnName", "PlasticId");

            builder.Property(e => e.ReservationId).HasAnnotation("Relational:ColumnName", "ReservationId");

            builder.HasOne(e => e.Reservation)
             .WithMany(p => p.ReservationConfirmationList)
             .HasForeignKey(e => e.ReservationId)
             .HasConstraintName("FK_ReservationItem_ReservationConfirmation");
        }
    }
}
