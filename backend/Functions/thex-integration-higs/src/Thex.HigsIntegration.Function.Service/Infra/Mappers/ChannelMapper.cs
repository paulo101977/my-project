﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Thex.HigsIntegration.Function.Service.Entities;

namespace Thex.HigsIntegration.Function.Service.Infra.Mappers
{
    public class ChannelMapper : IEntityTypeConfiguration<Channel>
    {
        public void Configure(EntityTypeBuilder<Channel> builder)
        {
            builder.ToTable("Channel");

            builder.HasAnnotation("Relational:TableName", "Channel");

            builder.HasIndex(e => e.BusinessSourceId)
                .HasName("x_Channel_BusinessSourceId");

            builder.HasIndex(e => e.ChannelCodeId)
                .HasName("x_Channel_ChannelCodeId");

            builder.Property(e => e.Id)
                .ValueGeneratedNever()
                .HasAnnotation("Relational:ColumnName", "ChannelId");

            builder.Property(e => e.BusinessSourceId).HasAnnotation("Relational:ColumnName", "BusinessSourceId");

            builder.Property(e => e.ChannelCodeId).HasAnnotation("Relational:ColumnName", "ChannelCodeId");

            builder.Property(e => e.Description)
                .IsRequired()
                .HasMaxLength(60)
                .IsUnicode(false)
                .HasAnnotation("Relational:ColumnName", "Description");

            builder.Property(e => e.DistributionAmount)
                .HasColumnType("numeric(18, 4)")
                .HasAnnotation("Relational:ColumnName", "DistributionAmount");

            builder.Property(e => e.IsActive).HasAnnotation("Relational:ColumnName", "IsActive");

            builder.HasOne(d => d.ChannelCode)
                .WithMany(p => p.ChannelList)
                .HasForeignKey(d => d.ChannelCodeId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_Channel_ChannelCode");

        }
    }
}
