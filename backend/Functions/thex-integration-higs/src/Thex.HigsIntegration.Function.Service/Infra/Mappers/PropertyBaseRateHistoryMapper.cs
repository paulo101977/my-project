﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Thex.HigsIntegration.Function.Service.Entities;

namespace Thex.HigsIntegration.Function.Service.Infra.Mappers
{
    public class PropertyBaseRateHistoryMapper : IEntityTypeConfiguration<PropertyBaseRateHistory>
    {
        public void Configure(EntityTypeBuilder<PropertyBaseRateHistory> builder)
        {
            builder.ToTable("PropertyBaseRateHistory");

            builder.HasAnnotation("Relational:TableName", "PropertyBaseRateHistory");

            builder.HasIndex(e => e.CurrencyId)
                .HasName("x_PropertyBaseRateHistory_CurrencyId");

            builder.HasIndex(e => e.MealPlanTypeId)
                .HasName("x_PropertyBaseRateHistory_MealPlanTypeId");

            builder.HasIndex(e => e.PropertyId)
                .HasName("x_PropertyBaseRateHistory_PropertyId");

            builder.HasIndex(e => e.RatePlanId)
                .HasName("x_PropertyBaseRateHistory_RatePlanId");

            builder.HasIndex(e => e.RoomTypeId)
                .HasName("x_PropertyBaseRateHistory_RoomTypeId");

            builder.HasIndex(e => e.TenantId)
                .HasName("x_PropertyBaseRateHistory_TenantId");

            builder.Property(e => e.Id)
                .ValueGeneratedNever()
                .HasAnnotation("Relational:ColumnName", "PropertyBaseRateHistoryId");

            builder.Property(e => e.AdultMealPlanAmount)
                .HasColumnName("Adult_MealPlan_Amount")
                .HasColumnType("numeric(18, 4)")
                .HasAnnotation("Relational:ColumnName", "Adult_MealPlan_Amount");

            builder.Property(e => e.Child1Amount)
                .HasColumnName("Child_1_Amount")
                .HasColumnType("numeric(18, 4)")
                .HasAnnotation("Relational:ColumnName", "Child_1_Amount");

            builder.Property(e => e.Child1MealPlanAmount)
                .HasColumnName("Child_1_MealPlan_Amount")
                .HasColumnType("numeric(18, 4)")
                .HasAnnotation("Relational:ColumnName", "Child_1_MealPlan_Amount");

            builder.Property(e => e.Child2Amount)
                .HasColumnName("Child_2_Amount")
                .HasColumnType("numeric(18, 4)")
                .HasAnnotation("Relational:ColumnName", "Child_2_Amount");

            builder.Property(e => e.Child2MealPlanAmount)
                .HasColumnName("Child_2_MealPlan_Amount")
                .HasColumnType("numeric(18, 4)")
                .HasAnnotation("Relational:ColumnName", "Child_2_MealPlan_Amount");

            builder.Property(e => e.Child3Amount)
                .HasColumnName("Child_3_Amount")
                .HasColumnType("numeric(18, 4)")
                .HasAnnotation("Relational:ColumnName", "Child_3_Amount");

            builder.Property(e => e.Child3MealPlanAmount)
                .HasColumnName("Child_3_MealPlan_Amount")
                .HasColumnType("numeric(18, 4)")
                .HasAnnotation("Relational:ColumnName", "Child_3_MealPlan_Amount");

            builder.Property(e => e.CreationTime)
                .HasColumnType("datetime")
                .HasDefaultValueSql("(getutcdate())")
                .HasAnnotation("Relational:ColumnName", "CreationTime");

            builder.Property(e => e.CreatorUserId).HasAnnotation("Relational:ColumnName", "CreatorUserId");

            builder.Property(e => e.CurrencyId).HasAnnotation("Relational:ColumnName", "CurrencyId");

            builder.Property(e => e.CurrencySymbol)
                .HasMaxLength(10)
                .IsUnicode(false)
                .HasAnnotation("Relational:ColumnName", "CurrencySymbol");

            builder.Property(e => e.DeleterUserId).HasAnnotation("Relational:ColumnName", "DeleterUserId");

            builder.Property(e => e.DeletionTime)
                .HasColumnType("datetime")
                .HasAnnotation("Relational:ColumnName", "DeletionTime");

            builder.Property(e => e.IsDeleted).HasAnnotation("Relational:ColumnName", "IsDeleted");

            builder.Property(e => e.LastModificationTime)
                .HasColumnType("datetime")
                .HasDefaultValueSql("(getutcdate())")
                .HasAnnotation("Relational:ColumnName", "LastModificationTime");

            builder.Property(e => e.LastModifierUserId).HasAnnotation("Relational:ColumnName", "LastModifierUserId");

            builder.Property(e => e.MealPlanTypeDefault)
                .HasDefaultValueSql("((1))")
                .HasAnnotation("Relational:ColumnName", "MealPlanTypeDefault");

            builder.Property(e => e.MealPlanTypeId).HasAnnotation("Relational:ColumnName", "MealPlanTypeId");

            builder.Property(e => e.Pax10Amount)
                .HasColumnName("Pax_10_Amount")
                .HasColumnType("numeric(18, 4)")
                .HasAnnotation("Relational:ColumnName", "Pax_10_Amount");

            builder.Property(e => e.Pax11Amount)
                .HasColumnName("Pax_11_Amount")
                .HasColumnType("numeric(18, 4)")
                .HasAnnotation("Relational:ColumnName", "Pax_11_Amount");

            builder.Property(e => e.Pax12Amount)
                .HasColumnName("Pax_12_Amount")
                .HasColumnType("numeric(18, 4)")
                .HasAnnotation("Relational:ColumnName", "Pax_12_Amount");

            builder.Property(e => e.Pax13Amount)
                .HasColumnName("Pax_13_Amount")
                .HasColumnType("numeric(18, 4)")
                .HasAnnotation("Relational:ColumnName", "Pax_13_Amount");

            builder.Property(e => e.Pax14Amount)
                .HasColumnName("Pax_14_Amount")
                .HasColumnType("numeric(18, 4)")
                .HasAnnotation("Relational:ColumnName", "Pax_14_Amount");

            builder.Property(e => e.Pax15Amount)
                .HasColumnName("Pax_15_Amount")
                .HasColumnType("numeric(18, 4)")
                .HasAnnotation("Relational:ColumnName", "Pax_15_Amount");

            builder.Property(e => e.Pax16Amount)
                .HasColumnName("Pax_16_Amount")
                .HasColumnType("numeric(18, 4)")
                .HasAnnotation("Relational:ColumnName", "Pax_16_Amount");

            builder.Property(e => e.Pax17Amount)
                .HasColumnName("Pax_17_Amount")
                .HasColumnType("numeric(18, 4)")
                .HasAnnotation("Relational:ColumnName", "Pax_17_Amount");

            builder.Property(e => e.Pax18Amount)
                .HasColumnName("Pax_18_Amount")
                .HasColumnType("numeric(18, 4)")
                .HasAnnotation("Relational:ColumnName", "Pax_18_Amount");

            builder.Property(e => e.Pax19Amount)
                .HasColumnName("Pax_19_Amount")
                .HasColumnType("numeric(18, 4)")
                .HasAnnotation("Relational:ColumnName", "Pax_19_Amount");

            builder.Property(e => e.Pax1Amount)
                .HasColumnName("Pax_1_Amount")
                .HasColumnType("numeric(18, 4)")
                .HasAnnotation("Relational:ColumnName", "Pax_1_Amount");

            builder.Property(e => e.Pax20Amount)
                .HasColumnName("Pax_20_Amount")
                .HasColumnType("numeric(18, 4)")
                .HasAnnotation("Relational:ColumnName", "Pax_20_Amount");

            builder.Property(e => e.Pax2Amount)
                .HasColumnName("Pax_2_Amount")
                .HasColumnType("numeric(18, 4)")
                .HasAnnotation("Relational:ColumnName", "Pax_2_Amount");

            builder.Property(e => e.Pax3Amount)
                .HasColumnName("Pax_3_Amount")
                .HasColumnType("numeric(18, 4)")
                .HasAnnotation("Relational:ColumnName", "Pax_3_Amount");

            builder.Property(e => e.Pax4Amount)
                .HasColumnName("Pax_4_Amount")
                .HasColumnType("numeric(18, 4)")
                .HasAnnotation("Relational:ColumnName", "Pax_4_Amount");

            builder.Property(e => e.Pax5Amount)
                .HasColumnName("Pax_5_Amount")
                .HasColumnType("numeric(18, 4)")
                .HasAnnotation("Relational:ColumnName", "Pax_5_Amount");

            builder.Property(e => e.Pax6Amount)
                .HasColumnName("Pax_6_Amount")
                .HasColumnType("numeric(18, 4)")
                .HasAnnotation("Relational:ColumnName", "Pax_6_Amount");

            builder.Property(e => e.Pax7Amount)
                .HasColumnName("Pax_7_Amount")
                .HasColumnType("numeric(18, 4)")
                .HasAnnotation("Relational:ColumnName", "Pax_7_Amount");

            builder.Property(e => e.Pax8Amount)
                .HasColumnName("Pax_8_Amount")
                .HasColumnType("numeric(18, 4)")
                .HasAnnotation("Relational:ColumnName", "Pax_8_Amount");

            builder.Property(e => e.Pax9Amount)
                .HasColumnName("Pax_9_Amount")
                .HasColumnType("numeric(18, 4)")
                .HasAnnotation("Relational:ColumnName", "Pax_9_Amount");

            builder.Property(e => e.PaxAdditionalAmount)
                .HasColumnName("Pax_Additional_Amount")
                .HasColumnType("numeric(18, 4)")
                .HasAnnotation("Relational:ColumnName", "Pax_Additional_Amount");

            builder.Property(e => e.PropertyBaseRateHeaderHistoryId).HasAnnotation("Relational:ColumnName", "PropertyBaseRateHeaderHistoryId");

            builder.Property(e => e.PropertyId).HasAnnotation("Relational:ColumnName", "PropertyId");

            builder.Property(e => e.Level).HasAnnotation("Relational:ColumnName", "Level");

            builder.Property(e => e.RatePlanId).HasAnnotation("Relational:ColumnName", "RatePlanId");

            builder.Property(e => e.RoomTypeId).HasAnnotation("Relational:ColumnName", "RoomTypeId");

            builder.Property(e => e.TenantId).HasAnnotation("Relational:ColumnName", "TenantId");

            builder.HasOne(d => d.PropertyBaseRateHeaderHistory)
               .WithMany(p => p.PropertyBaseRateHistoryList)
               .HasForeignKey(d => d.PropertyBaseRateHeaderHistoryId)
               .HasConstraintName("FK_PropertyBaseRateHistory_PropertyBaseRateHeaderHistory");
        }
    }
}
