﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Thex.HigsIntegration.Function.Service.Entities;

namespace Thex.HigsIntegration.Function.Service.Infra.Mappers
{
    public class PropertyMapper : IEntityTypeConfiguration<Property>
    {
        public void Configure(EntityTypeBuilder<Property> builder)
        {
            builder.ToTable("Property");

            builder.HasAnnotation("Relational:TableName", "Property");

            builder.HasIndex(e => e.BrandId)
                .HasName("x_Property_BrandId");

            builder.HasIndex(e => e.CompanyId)
                .HasName("x_Property_CompanyId");

            builder.HasIndex(e => e.PropertyUId)
                .HasName("UK_Property_PropertyUId")
                .IsUnique();

            builder.HasKey(e => e.Id);
            builder.Property(e => e.Id).HasAnnotation("Relational:ColumnName", "PropertyId");

            builder.Property(e => e.PropertyStatusId)
                .IsRequired()
                .HasDefaultValueSql("((12))")
                .HasAnnotation("Relational:ColumnName", "PropertyStatusId");           
        }
    }
}
