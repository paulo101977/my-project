﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Thex.HigsIntegration.Function.Service.Entities;

namespace Thex.HigsIntegration.Function.Service.Infra.Mappers
{
    public class ReservationBudgetMapper : IEntityTypeConfiguration<ReservationBudget>
    {
        public void Configure(EntityTypeBuilder<ReservationBudget> builder)
        {

            builder.ToTable("ReservationBudget");

            builder.HasAnnotation("Relational:TableName", "ReservationBudget");

            builder.HasIndex(e => e.BillingAccountItemId)
                .HasName("x_ReservationBudget_BillingAccountItemId");

            builder.HasIndex(e => e.CurrencyId)
                .HasName("x_ReservationBudget_CurrencyId");

            builder.HasIndex(e => e.ReservationBudgetUid)
                .HasName("AK_ReservationBudget_Uid")
                .IsUnique();

            builder.HasIndex(e => e.ReservationItemId)
                .HasName("x_ReservationBudget_ReservationItemId");

            builder.Property(e => e.Id).HasAnnotation("Relational:ColumnName", "ReservationBudgetId").ValueGeneratedOnAdd();

            builder.Property(e => e.BillingAccountItemId).HasAnnotation("Relational:ColumnName", "BillingAccountItemId");

            builder.Property(e => e.BudgetDay)
                .HasColumnType("date")
                .HasAnnotation("Relational:ColumnName", "BudgetDay");

            builder.Property(e => e.CurrencyId).HasAnnotation("Relational:ColumnName", "CurrencyId");

            builder.Property(e => e.CurrencySymbol)
                .HasMaxLength(10)
                .IsUnicode(false)
                .HasAnnotation("Relational:ColumnName", "CurrencySymbol");

            builder.Property(e => e.ManualRate)
                .HasColumnType("numeric(18, 4)")
                .HasAnnotation("Relational:ColumnName", "ManualRate");

            builder.Property(e => e.MealPlanTypeId).HasAnnotation("Relational:ColumnName", "MealPlanTypeId");

            builder.Property(e => e.RateVariation)
                .HasColumnType("numeric(18, 4)")
                .HasAnnotation("Relational:ColumnName", "RateVariation");

            builder.Property(e => e.ReservationBudgetUid).HasAnnotation("Relational:ColumnName", "ReservationBudgetUid");

            builder.Property(e => e.BaseRate)
                .HasColumnType("numeric(18, 4)")
                .HasAnnotation("Relational:ColumnName", "BaseRate");

            builder.Property(e => e.ChildRate)
                .HasColumnType("numeric(18, 4)")
                .HasAnnotation("Relational:ColumnName", "ChildRate");

            builder.Property(e => e.AgreementRate)
                .HasColumnType("numeric(18, 4)")
                .HasAnnotation("Relational:ColumnName", "AgreementRate");

            builder.HasOne(e => e.ReservationItem)
            .WithMany(p => p.ReservationBudgetList)
            .HasForeignKey(e => e.ReservationItemId)
            .HasConstraintName("FK_ReservationBudget_ReservationItem");
        }
    }
}
