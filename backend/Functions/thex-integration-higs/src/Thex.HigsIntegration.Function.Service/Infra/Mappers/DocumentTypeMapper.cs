﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Thex.HigsIntegration.Function.Service.Entities;

namespace Thex.HigsIntegration.Function.Service.Infra.Mappers
{
    public class DocumentTypeMapper : IEntityTypeConfiguration<DocumentType>
    {
        public void Configure(EntityTypeBuilder<DocumentType> builder)
        {
            
            builder.ToTable("DocumentType");

            builder.HasAnnotation("Relational:TableName", "DocumentType");

            builder.HasIndex(e => e.CountrySubdivisionId)
                .HasName("x_DocumentType_CountrySubdivisionId");

            builder.HasIndex(e => new { e.Id, e.PersonType, e.CountrySubdivisionId })
                .HasName("UK_DocumentType")
                .IsUnique();

            builder.HasKey(e => e.Id);
            builder.Property(e => e.Id)
                .ValueGeneratedOnAdd()
                .HasAnnotation("Relational:ColumnName", "DocumentTypeId");

            builder.Property(e => e.CountrySubdivisionId).HasAnnotation("Relational:ColumnName", "CountrySubdivisionId");

            builder.Property(e => e.IsMandatory).HasAnnotation("Relational:ColumnName", "IsMandatory");

            builder.Property(e => e.Name)
                .IsRequired()
                .HasMaxLength(50)
                .IsUnicode(false)
                .HasAnnotation("Relational:ColumnName", "Name");

            builder.Property(e => e.HigsCode).HasAnnotation("Relational:ColumnName", "HigsCode");

            builder.Property(e => e.PersonType)
                .IsRequired()
                .HasColumnType("char(1)")
                .HasAnnotation("Relational:ColumnName", "PersonType");

            builder.Ignore(e => e.PersonTypeValue);

            builder.Property(e => e.RegexValidationExpression)
                .HasMaxLength(500)
                .IsUnicode(false)
                .HasAnnotation("Relational:ColumnName", "RegexValidationExpression");

            builder.Property(e => e.StringFormatMask)
                .HasMaxLength(100)
                .IsUnicode(false)
                .HasAnnotation("Relational:ColumnName", "StringFormatMask");

            builder.HasOne(d => d.CountrySubdivision)
                .WithMany(p => p.DocumentTypeList)
                .HasForeignKey(d => d.CountrySubdivisionId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_DocumentType_CountrySubdivision");

        }
    }
}
