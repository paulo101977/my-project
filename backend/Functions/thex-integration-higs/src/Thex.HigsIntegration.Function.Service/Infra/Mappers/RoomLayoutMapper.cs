﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Thex.HigsIntegration.Function.Service.Entities;

namespace Thex.HigsIntegration.Function.Service.Infra.Mappers
{
    public class RoomLayoutMapper : IEntityTypeConfiguration<RoomLayout>
    {
        public void Configure(EntityTypeBuilder<RoomLayout> builder)
        {
            builder.ToTable("RoomLayout");

            builder.HasAnnotation("Relational:TableName", "RoomLayout");

            builder.Property(e => e.Id)
                .HasAnnotation("Relational:ColumnName", "RoomLayoutId");

            builder.Property(e => e.QuantityDouble).HasAnnotation("Relational:ColumnName", "QuantityDouble");

            builder.Property(e => e.QuantitySingle).HasAnnotation("Relational:ColumnName", "QuantitySingle");

        }
    }
}
