﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Thex.HigsIntegration.Function.Service.Entities;

namespace Thex.HigsIntegration.Function.Service.Infra.Mappers
{
    public class GeneralOccupationMapper : IEntityTypeConfiguration<GeneralOccupation>
    {
        public void Configure(EntityTypeBuilder<GeneralOccupation> builder)
        {
            
            builder.ToTable("GeneralOccupation");

            builder.HasAnnotation("Relational:TableName", "GeneralOccupation");

            builder.Property(e => e.Id)
                .ValueGeneratedNever()
                .HasAnnotation("Relational:ColumnName", "GeneralOccupationId");

            builder.Property(e => e.Occupation)
                .IsRequired()
                .HasMaxLength(100)
                .IsUnicode(false)
                .HasAnnotation("Relational:ColumnName", "Occupation");

        }
    }
}
