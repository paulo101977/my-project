﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Thex.HigsIntegration.Function.Service.Entities;

namespace Thex.HigsIntegration.Function.Service.Infra.Mappers
{
    public class ReservationMapper : IEntityTypeConfiguration<Reservation>
    {
        public void Configure(EntityTypeBuilder<Reservation> builder)
        {

            builder.ToTable("Reservation");

            builder.HasAnnotation("Relational:TableName", "Reservation");

            builder.HasIndex(e => e.BusinessSourceId)
                .HasName("x_Reservation_BusinessSourceId");

            builder.HasIndex(e => e.CompanyClientContactPersonId)
                .HasName("x_Reservation_CompanyClientContactPersonId");

            builder.HasIndex(e => e.CompanyClientId)
                .HasName("x_Reservation_CompanyClientId");

            builder.HasIndex(e => e.MarketSegmentId)
                .HasName("x_Reservation_MarketSegmentId");

            builder.HasIndex(e => e.PropertyId)
                .HasName("x_Reservation_PropertyId");

            builder.HasIndex(e => e.RatePlanId)
                .HasName("x_Reservation_RatePlanId");

            builder.HasIndex(e => e.ReservationUid)
                .HasName("AK_Reservation_Uid")
                .IsUnique();

            builder.Property(e => e.Id).HasAnnotation("Relational:ColumnName", "ReservationId").ValueGeneratedOnAdd();

            builder.Property(e => e.ContactEmail)
                .HasMaxLength(100)
                .IsUnicode(false)
                .HasAnnotation("Relational:ColumnName", "ContactEmail");

            builder.Property(e => e.ContactName)
                .HasMaxLength(100)
                .IsUnicode(false)
                .HasAnnotation("Relational:ColumnName", "ContactName");

            builder.Property(e => e.ContactPhone)
                .HasMaxLength(50)
                .IsUnicode(false)
                .HasAnnotation("Relational:ColumnName", "ContactPhone");

            builder.Property(e => e.ExternalComments)
                .HasMaxLength(4000)
                .IsUnicode(false)
                .HasAnnotation("Relational:ColumnName", "ExternalComments");

            builder.Property(e => e.GroupName)
                .HasMaxLength(50)
                .IsUnicode(false)
                .HasAnnotation("Relational:ColumnName", "GroupName");

            builder.Property(e => e.InternalComments)
                .HasMaxLength(4000)
                .IsUnicode(false)
                .HasAnnotation("Relational:ColumnName", "InternalComments");

            builder.Property(e => e.RatePlanId).HasAnnotation("Relational:ColumnName", "RatePlanId");

            builder.Property(e => e.ReservationCode)
                .IsRequired()
                .HasMaxLength(25)
                .IsUnicode(false)
                .HasAnnotation("Relational:ColumnName", "ReservationCode");

            builder.Property(e => e.ReservationUid).HasAnnotation("Relational:ColumnName", "ReservationUid");

            builder.Property(e => e.ReservationVendorCode)
                .HasMaxLength(20)
                .IsUnicode(false)
                .HasAnnotation("Relational:ColumnName", "ReservationVendorCode");
        }
    }
}
