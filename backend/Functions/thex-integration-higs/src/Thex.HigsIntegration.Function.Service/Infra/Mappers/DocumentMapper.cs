﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Thex.HigsIntegration.Function.Service.Entities;

namespace Thex.HigsIntegration.Function.Service.Infra.Mappers
{
    public class DocumentMapper : IEntityTypeConfiguration<Document>
    {
        public void Configure(EntityTypeBuilder<Document> builder)
        {
            builder.Ignore(e => e.Id);

            builder.HasKey(e => new { e.OwnerId, e.DocumentTypeId });

            builder.ToTable("Document");

            builder.HasAnnotation("Relational:TableName", "Document");

            builder.HasIndex(e => e.DocumentTypeId)
                .HasName("x_Document_DocumentTypeIdId");

            builder.Property(e => e.OwnerId).HasAnnotation("Relational:ColumnName", "OwnerId");

            builder.Property(e => e.DocumentTypeId).HasAnnotation("Relational:ColumnName", "DocumentTypeId");

            builder.Property(e => e.DocumentInformation)
                .IsRequired()
                .HasMaxLength(20)
                .IsUnicode(false)
                .HasAnnotation("Relational:ColumnName", "DocumentInformation");

            builder.HasOne(d => d.Owner)
                .WithMany(p => p.DocumentList)
                .HasForeignKey(d => d.OwnerId)
                .OnDelete(DeleteBehavior.Restrict);

            builder.HasOne(d => d.DocumentType)
                .WithMany(p => p.DocumentList)
                .HasForeignKey(d => d.DocumentTypeId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_Document_DocumentType");

        }
    }
}
