﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Thex.HigsIntegration.Function.Service.Entities;

namespace Thex.HigsIntegration.Function.Service.Infra.Mappers
{
    public class GuestReservationItemMapper : IEntityTypeConfiguration<GuestReservationItem>
    {
        public void Configure(EntityTypeBuilder<GuestReservationItem> builder)
        {

            builder.ToTable("GuestReservationItem");

            builder.HasAnnotation("Relational:TableName", "GuestReservationItem");

            builder.HasIndex(e => e.GuestRegistrationId)
                .HasName("x_GuestReservationItem_GuestRegistrationId");

            builder.HasIndex(e => e.GuestId)
                .HasName("x_GuestReservationItem_GuestId");

            builder.HasIndex(e => e.GuestStatusId)
                .HasName("x_GuestReservationItem_StatusId");

            builder.HasIndex(e => e.GuestTypeId)
                .HasName("x_GuestReservationItem_PropertyGuestTypeId");

            builder.HasIndex(e => e.ReservationItemId)
                .HasName("x_GuestReservationItem_ReservationItemId");

            builder.Property(e => e.Id).HasAnnotation("Relational:ColumnName", "GuestReservationItemId").ValueGeneratedOnAdd();

            builder.Property(e => e.CheckInDate)
                .HasColumnType("datetime")
                .HasAnnotation("Relational:ColumnName", "CheckInDate");

            builder.Property(e => e.CheckOutDate)
                .HasColumnType("datetime")
                .HasAnnotation("Relational:ColumnName", "CheckOutDate");

            builder.Property(e => e.ChildAge)
                .HasDefaultValueSql("((0))")
                .HasAnnotation("Relational:ColumnName", "ChildAge");

            builder.Property(e => e.EstimatedArrivalDate)
                .HasColumnType("datetime")
                .HasAnnotation("Relational:ColumnName", "EstimatedArrivalDate");

            builder.Property(e => e.EstimatedDepartureDate)
                .HasColumnType("datetime")
                .HasAnnotation("Relational:ColumnName", "EstimatedDepartureDate");

            builder.Property(e => e.GuestCitizenship)
                .HasMaxLength(50)
                .IsUnicode(false)
                .HasAnnotation("Relational:ColumnName", "GuestCitizenship");

            builder.Property(e => e.GuestDocument)
                .HasMaxLength(20)
                .IsUnicode(false)
                .HasAnnotation("Relational:ColumnName", "GuestDocument");

            builder.Property(e => e.GuestEmail)
                .HasMaxLength(100)
                .IsUnicode(false)
                .HasAnnotation("Relational:ColumnName", "GuestEmail");

            builder.Property(e => e.GuestLanguage)
                .HasMaxLength(50)
                .IsUnicode(false)
                .HasAnnotation("Relational:ColumnName", "GuestLanguage");

            builder.Property(e => e.GuestName)
                .HasMaxLength(100)
                .IsUnicode(false)
                .HasAnnotation("Relational:ColumnName", "GuestName");

            builder.Property(e => e.IsChild).HasAnnotation("Relational:ColumnName", "IsChild");

            builder.Property(e => e.IsIncognito).HasAnnotation("Relational:ColumnName", "IsIncognito");

            builder.Property(e => e.IsMain).HasAnnotation("Relational:ColumnName", "IsMain");

            builder.Property(e => e.PctDailyRate).HasAnnotation("Relational:ColumnName", "PctDailyRate");

            builder.Property(e => e.PreferredName)
                .HasMaxLength(100)
                .IsUnicode(false)
                .HasAnnotation("Relational:ColumnName", "PreferredName");

            builder.Property(e => e.PropertyId).HasAnnotation("Relational:ColumnName", "PropertyId");

            builder.HasOne(e => e.ReservationItem)
           .WithMany(p => p.GuestReservationItemList)
           .HasForeignKey(e => e.ReservationItemId)
           .HasConstraintName("FK_GuestReservationItem_ReservationItem");

            builder.HasOne(d => d.GuestType)
               .WithMany(p => p.GuestReservationItemList)
               .HasForeignKey(d => d.GuestTypeId)
               .OnDelete(DeleteBehavior.ClientSetNull)
               .HasConstraintName("FK_GuestReservationItem_PropertyGuestType");

            builder.HasOne(d => d.GuestDocumentType)
                .WithMany(p => p.GuestReservationItemList)
                .HasForeignKey(d => d.GuestDocumentTypeId)
                .HasConstraintName("FK_GuestReservationItem_DocumentType");
        }
    }
}
