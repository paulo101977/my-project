﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Thex.HigsIntegration.Function.Service.Entities;

namespace Thex.HigsIntegration.Function.Service.Infra.Mappers
{
    public class RatePlanCommissionMapper : IEntityTypeConfiguration<RatePlanCommission>
    {
        public void Configure(EntityTypeBuilder<RatePlanCommission> builder)
        {
            builder.ToTable("RatePlanCommission");

            builder.HasAnnotation("Relational:TableName", "RatePlanCommission");

            builder.HasIndex(e => e.BillingItemId)
                .HasName("x_RatePlanCommission_BillingItemId");

            builder.HasIndex(e => e.RatePlanId)
                .HasName("x_RatePlanCommission_RatePlanId");

            builder.HasIndex(e => e.TenantId)
                .HasName("x_RatePlanCommission_TenantId");

            builder.HasIndex(e => new { e.RatePlanId, e.BillingItemId })
                .HasName("UK_RatePlanCommission")
                .IsUnique();

            builder.Property(e => e.Id)
                .HasAnnotation("Relational:ColumnName", "RatePlanCommissionId");

            builder.Property(e => e.BillingItemId).HasAnnotation("Relational:ColumnName", "BillingItemId");

            builder.Property(e => e.CommissionPercentualAmount)
                .HasColumnType("numeric(18, 4)")
                .HasAnnotation("Relational:ColumnName", "CommissionPercentualAmount");

            builder.Property(e => e.CreationTime)
                .HasColumnType("datetime")
                .HasDefaultValueSql("(getutcdate())")
                .HasAnnotation("Relational:ColumnName", "CreationTime");

            builder.Property(e => e.CreatorUserId).HasAnnotation("Relational:ColumnName", "CreatorUserId");

            builder.Property(e => e.DeleterUserId).HasAnnotation("Relational:ColumnName", "DeleterUserId");

            builder.Property(e => e.DeletionTime)
                .HasColumnType("datetime")
                .HasAnnotation("Relational:ColumnName", "DeletionTime");

            builder.Property(e => e.IsDeleted).HasAnnotation("Relational:ColumnName", "IsDeleted");

            builder.Property(e => e.LastModificationTime)
                .HasColumnType("datetime")
                .HasDefaultValueSql("(getutcdate())")
                .HasAnnotation("Relational:ColumnName", "LastModificationTime");

            builder.Property(e => e.LastModifierUserId).HasAnnotation("Relational:ColumnName", "LastModifierUserId");

            builder.Property(e => e.RatePlanId).HasAnnotation("Relational:ColumnName", "RatePlanId");

            builder.Property(e => e.TenantId).HasAnnotation("Relational:ColumnName", "TenantId");

            builder.HasOne(d => d.RatePlan)
                .WithMany(p => p.RatePlanCommissionList)
                .HasForeignKey(d => d.RatePlanId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_RatePlanCommission_RatePlan");
        }
    }
}
