﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Thex.HigsIntegration.Function.Service.Entities;

namespace Thex.HigsIntegration.Function.Service.Infra.Mappers
{
    public class PersonInformationMapper : IEntityTypeConfiguration<PersonInformation>
    {
        public void Configure(EntityTypeBuilder<PersonInformation> builder)
        {
            
            builder.HasKey(e => new { e.OwnerId, e.PersonInformationTypeId, e.Information });

            builder.ToTable("PersonInformation");

            builder.Ignore(e => e.Id);

            builder.HasAnnotation("Relational:TableName", "PersonInformation");

            builder.Property(e => e.OwnerId).HasAnnotation("Relational:ColumnName", "OwnerId");

            builder.Property(e => e.PersonInformationTypeId).HasAnnotation("Relational:ColumnName", "PersonInformationTypeId");

            builder.Property(e => e.Information)
                .HasMaxLength(500)
                .IsUnicode(false)
                .HasAnnotation("Relational:ColumnName", "Information");

            builder.HasOne(d => d.Owner)
                .WithMany(p => p.PersonInformationList)
                .HasForeignKey(d => d.OwnerId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_PersonInformation_Person");
        }
    }
}
