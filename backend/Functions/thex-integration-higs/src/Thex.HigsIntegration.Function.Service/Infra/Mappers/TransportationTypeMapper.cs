﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Thex.HigsIntegration.Function.Service.Entities;

namespace Thex.HigsIntegration.Function.Service.Infra.Mappers
{
    public class TransportationTypeMapper : IEntityTypeConfiguration<TransportationType>
    {
        public void Configure(EntityTypeBuilder<TransportationType> builder)
        {
            
            builder.ToTable("TransportationType");

            builder.HasAnnotation("Relational:TableName", "TransportationType");

            builder.Property(e => e.Id)
                .ValueGeneratedNever()
                .HasAnnotation("Relational:ColumnName", "TransportationTypeId");

            builder.Property(e => e.TransportationTypeName)
                .IsRequired()
                .HasMaxLength(20)
                .IsUnicode(false)
                .HasAnnotation("Relational:ColumnName", "TransportationTypeName");

            builder.Property(e => e.HigsCode).HasAnnotation("Relational:ColumnName", "HigsCode");
        }
    }
}
