﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Thex.HigsIntegration.Function.Service.Entities;

namespace Thex.HigsIntegration.Function.Service.Infra.Mappers
{
    public class CompanyClientChannelMapper : IEntityTypeConfiguration<CompanyClientChannel>
    {
        public void Configure(EntityTypeBuilder<CompanyClientChannel> builder)
        {
            builder.ToTable("CompanyClientChannel");

            builder.Property(e => e.Id)
                .ValueGeneratedNever()
                .HasAnnotation("Relational:ColumnName", "CompanyClientChannelId");

            builder.HasAnnotation("Relational:TableName", "CompanyClientChannel");

            builder.HasIndex(e => e.ChannelId)
               .HasName("x_CompanyClientChannel_ChannelId");

            builder.HasIndex(e => e.CompanyClientId)
                .HasName("x_CompanyClientChannel_CompanyClientId");

            builder.Property(e => e.IsActive).HasAnnotation("Relational:ColumnName", "IsActive");

            builder.HasOne(d => d.Channel)
                .WithMany(p => p.CompanyClientChannelList)
                .HasForeignKey(d => d.ChannelId)
                .HasConstraintName("CompanyClientChannel_Channel");
        }
    }
}
