﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using Thex.HigsIntegration.Function.Service.Entities;

namespace Thex.HigsIntegration.Function.Service.Infra.Mappers
{
    public class IntegrationPartnerPropertyHistoryMapper : IEntityTypeConfiguration<IntegrationPartnerPropertyHistory>
    {
        public void Configure(EntityTypeBuilder<IntegrationPartnerPropertyHistory> builder)
        {
            builder.ToTable("IntegrationPartnerPropertyHistory");

            builder.HasAnnotation("Relational:TableName", "IntegrationPartnerPropertyHistory");

            builder.Property(e => e.Id)
                .HasAnnotation("Relational:ColumnName", "IntegrationPartnerPropertyHistoryId");

            builder.HasOne(e => e.ReservationItem)
               .WithMany(p => p.IntegrationPartnerPropertyHistoryList)
               .HasForeignKey(e => e.ReservationItemId)
               .HasConstraintName("FK_IntegrationPartnerPropertyHistory_ReservationItem");

            builder.HasOne(e => e.IntegrationPartnerProperty)
             .WithMany(p => p.IntegrationPartnerPropertyHistoryList)
             .HasForeignKey(e => e.IntegrationPartnerPropertyId)
             .HasConstraintName("FK_IntegrationPartnerPropertyHistory_IntegrationPartnerProperty");
        }
    }
}
