﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Thex.HigsIntegration.Function.Service.Entities;

namespace Thex.HigsIntegration.Function.Service.Infra.Mappers
{
    public class PropertyGuestTypeMapper : IEntityTypeConfiguration<PropertyGuestType>
    {
        public void Configure(EntityTypeBuilder<PropertyGuestType> builder)
        {
            
            builder.ToTable("PropertyGuestType");

            builder.HasAnnotation("Relational:TableName", "PropertyGuestType");

            builder.HasIndex(e => e.PropertyId)
                .HasName("x_PropertyGuestType_PropertyId");

            builder.Property(e => e.Id).HasAnnotation("Relational:ColumnName", "PropertyGuestTypeId");

            builder.Property(e => e.GuestTypeName)
                .IsRequired()
                .HasMaxLength(100)
                .IsUnicode(false)
                .HasAnnotation("Relational:ColumnName", "GuestTypeName");

            builder.Property(e => e.IsVip)
                .HasColumnName("IsVIP")
                .HasAnnotation("Relational:ColumnName", "IsVIP");

            builder.Property(e => e.Code)
                .IsRequired()
                .HasMaxLength(20)
                .IsUnicode(false)
                .HasAnnotation("Relational:ColumnName", "Code");
            builder.Property(e => e.IsIncognito).HasAnnotation("Relational:ColumnName", "IsIncognito");
            builder.Property(e => e.IsActive).HasAnnotation("Relational:ColumnName", "IsActive");

            builder.Property(e => e.PropertyId).HasAnnotation("Relational:ColumnName", "PropertyId");
        }
    }
}
