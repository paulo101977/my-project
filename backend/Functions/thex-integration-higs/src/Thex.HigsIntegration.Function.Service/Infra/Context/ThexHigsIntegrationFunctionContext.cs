﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.Extensions.Configuration;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Thex.Common;
using Thex.GenericLog;
using Thex.HigsIntegration.Function.Service.Entities;
using Thex.HigsIntegration.Function.Service.Infra.Mappers;
using Thex.Kernel;
using Tnf.EntityFrameworkCore;
using Tnf.Runtime.Session;

namespace Thex.HigsIntegration.Function.Service.Infra.Context
{
    public class ThexHigsIntegrationFunctionContext : TnfDbContext
    {
        private readonly IConfiguration _config;
        private readonly IApplicationUser _applicationUser;
        private readonly IGenericLogHandler _genericLog;

        public DbSet<BusinessSource> BusinessSources { get; set; }
        public DbSet<Channel> Channels { get; set; }
        public DbSet<CompanyClientChannel> CompanyClientChannels { get; set; }
        public DbSet<Currency> Currencies { get; set; }
        public DbSet<GuestReservationItem> GuestReservationItems { get; set; }
        public DbSet<IntegrationPartnerProperty> IntegrationPartnerProperties { get; set; }
        public DbSet<IntegrationPartnerPropertyHistory> IntegrationPartnerPropertyHistories { get; set; }
        public DbSet<Property> Properties { get; set; }
        public DbSet<PropertyBaseRate> PropertyBaseRates { get; set; }
        public DbSet<PropertyBaseRateHistory> PropertyBaseRateHistories { get; set; }
        public DbSet<PropertyBaseRateHeaderHistory> PropertyBaseRateHeaderHistories { get; set; }
        public DbSet<PropertyParameter> PropertyParameters { get; set; }
        public DbSet<RatePlan> RatePlans { get; set; }
        public DbSet<Reservation> Reservations { get; set; }
        public DbSet<ReservationBudget> ReservationBudgets { get; set; }
        public DbSet<ReservationConfirmation> ReservationConfirmations { get; set; }
        public DbSet<ReservationItem> ReservationItems { get; set; }
        public DbSet<RoomLayout> RoomLayouts { get; set; }
        public DbSet<RoomType> RoomTypes { get; set; }

        public DbSet<Document> Documents { get; set; }
        public DbSet<DocumentType> DocumentTypes { get; set; }
        public DbSet<GuestRegistration> GuestRegistrations { get; set; }
        public DbSet<Guest> Guests { get; set; }
        public DbSet<ContactInformation> ContactInformations { get; set; }
        public DbSet<PersonInformation> PersonInformations { get; set; }
        public DbSet<Person> Persons { get; set; }

        public DbSet<TransportationType> TransportationTypes { get; set; }
        public DbSet<Reason> Reasons { get; set; }
        public DbSet<GeneralOccupation> GeneralOccupations { get; set; }
        public DbSet<PropertyGuestType> PropertyGuestTypes { get; set; }
        public DbSet<Location> Locations { get; set; }
        public DbSet<CountrySubdivision> CountrySubdivisions { get; set; }
        public DbSet<CountrySubdivisionTranslation> CountrySubdivisionTranslations { get; set; }
        public DbSet<GuestHigsIntegration> GuestHigsIntegrations { get; set; }

        public ThexHigsIntegrationFunctionContext(
            DbContextOptions<ThexHigsIntegrationFunctionContext> options,
            ITnfSession session,
            IApplicationUser applicationUser,
            IGenericLogHandler genericLog
            ) : base(options, session)
        {
            _applicationUser = applicationUser;
            _genericLog = genericLog;
        }

        public override async Task<int> SaveChangesAsync(CancellationToken cancellationToken = default(CancellationToken))
        {
            FillAuditFiedls();

            _genericLog.AddLog(_genericLog.DefaultBuilder
                .WithMessage(this.EnsureAutoHistory())
                .WithDetailedMessage(AppConsts.DatabaseModification)
                .AsInformation()
                .Build());

            return (await base.SaveChangesAsync(true, cancellationToken));
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new BusinessSourceMapper());
            modelBuilder.ApplyConfiguration(new ChannelCodeMapper());
            modelBuilder.ApplyConfiguration(new ChannelMapper());
            modelBuilder.ApplyConfiguration(new CompanyClientChannelMapper());
            modelBuilder.ApplyConfiguration(new CurrencyMapper());
            modelBuilder.ApplyConfiguration(new GuestReservationItemMapper());
            modelBuilder.ApplyConfiguration(new IntegrationPartnerPropertyMapper());
            modelBuilder.ApplyConfiguration(new IntegrationPartnerPropertyHistoryMapper());
            modelBuilder.ApplyConfiguration(new PropertyMapper());
            modelBuilder.ApplyConfiguration(new PropertyParameterMapper());
            modelBuilder.ApplyConfiguration(new RatePlanMapper());
            modelBuilder.ApplyConfiguration(new RatePlanCommissionMapper());
            modelBuilder.ApplyConfiguration(new RatePlanCompanyClientMapper());
            modelBuilder.ApplyConfiguration(new RatePlanMapper());
            modelBuilder.ApplyConfiguration(new RatePlanRoomTypeMapper());
            modelBuilder.ApplyConfiguration(new ReservationBudgetMapper());
            modelBuilder.ApplyConfiguration(new ReservationConfirmationMapper());
            modelBuilder.ApplyConfiguration(new ReservationItemMapper());
            modelBuilder.ApplyConfiguration(new ReservationMapper());
            modelBuilder.ApplyConfiguration(new RoomLayoutMapper());
            modelBuilder.ApplyConfiguration(new RoomTypeMapper());
            modelBuilder.ApplyConfiguration(new MarketSegmentMapper());
            modelBuilder.ApplyConfiguration(new DocumentMapper());
            modelBuilder.ApplyConfiguration(new DocumentTypeMapper());
            modelBuilder.ApplyConfiguration(new GuestRegistrationMapper());
            modelBuilder.ApplyConfiguration(new GuestMapper());
            modelBuilder.ApplyConfiguration(new PersonInformationMapper());
            modelBuilder.ApplyConfiguration(new PersonMapper());
            modelBuilder.ApplyConfiguration(new PropertyBaseRateMapper());
            modelBuilder.ApplyConfiguration(new PropertyBaseRateHeaderHistoryMapper());
            modelBuilder.ApplyConfiguration(new PropertyBaseRateHistoryMapper());
            modelBuilder.ApplyConfiguration(new TransportationTypeMapper());
            modelBuilder.ApplyConfiguration(new ReasonMapper());
            modelBuilder.ApplyConfiguration(new GeneralOccupationMapper());
            modelBuilder.ApplyConfiguration(new PropertyGuestTypeMapper());
            modelBuilder.ApplyConfiguration(new LocationMapper());
            modelBuilder.ApplyConfiguration(new CountrySubdivisionMapper());
            modelBuilder.ApplyConfiguration(new CountrySubdivisionTranslationMapper());
            modelBuilder.ApplyConfiguration(new GuestHigsIntegrationMapper());

            modelBuilder.HasDefaultSchema("dbo");

            base.OnModelCreating(modelBuilder);
        }

        private void FillAuditFiedls()
        {
            // custom code...
            var utcNowAuditDate = DateTime.UtcNow.ToZonedDateTimeLoggedUser(_applicationUser);

            var entriesWithBase = ChangeTracker.Entries();

            var FullAuditedEntries = entriesWithBase
                .Where(entry => (entry.Metadata.ClrType.BaseType != null && entry.Metadata.ClrType.BaseType.Name.Contains("ThexFullAuditedEntity")) ||
                                (entry.Metadata.ClrType.BaseType.BaseType != null && entry.Metadata.ClrType.BaseType.BaseType.Name.Contains("ThexFullAuditedEntity")));

            if (FullAuditedEntries != null)
                foreach (EntityEntry entityEntry in FullAuditedEntries)
                {
                    var entity = entityEntry.Entity;
                    switch (entityEntry.State)
                    {
                        case EntityState.Added:
                            entity.GetType().GetProperty("CreationTime").SetValue(entity, utcNowAuditDate, null);
                            entity.GetType().GetProperty("LastModificationTime").SetValue(entity, utcNowAuditDate, null);
                            entity.GetType().GetProperty("CreatorUserId").SetValue(entity, _applicationUser.UserUid, null);
                            break;
                        case EntityState.Modified:
                            entityEntry.Property("CreationTime").IsModified = false;
                            entityEntry.Property("CreatorUserId").IsModified = false;

                            entity.GetType().GetProperty("LastModificationTime").SetValue(entity, utcNowAuditDate, null);
                            entity.GetType().GetProperty("LastModifierUserId").SetValue(entity, _applicationUser.UserUid, null);
                            break;
                        case EntityState.Deleted:
                            entityEntry.Property("CreationTime").IsModified = false;
                            entityEntry.Property("CreatorUserId").IsModified = false;
                            entityEntry.Property("LastModificationTime").IsModified = false;
                            entityEntry.Property("LastModifierUserId").IsModified = false;

                            entity.GetType().GetProperty("DeletionTime").SetValue(entity, utcNowAuditDate, null);
                            entity.GetType().GetProperty("DeleterUserId").SetValue(entity, _applicationUser.UserUid, null);
                            entity.GetType().GetProperty("IsDeleted").SetValue(entity, true);
                            entityEntry.State = EntityState.Modified;
                            break;
                    }
                }


            var MultiTentantEntries = entriesWithBase
                .Where(entry => (entry.Metadata.ClrType.BaseType != null && entry.Metadata.ClrType.BaseType.Name.Contains("ThexMultiTenantFullAuditedEntity")) ||
                                (entry.Metadata.ClrType.BaseType.BaseType != null && entry.Metadata.ClrType.BaseType.BaseType.Name.Contains("ThexMultiTenantFullAuditedEntity")));

            if (MultiTentantEntries != null)
                foreach (EntityEntry entityEntry in MultiTentantEntries)
                {
                    var entity = entityEntry.Entity;
                    switch (entityEntry.State)
                    {
                        case EntityState.Added:
                            if (!entity.GetType().ToString().Contains("UserPassword"))
                                entity.GetType().GetProperty("TenantId").SetValue(entity, _applicationUser.TenantId, null);
                            break;
                        case EntityState.Modified:
                        case EntityState.Deleted:
                            entityEntry.Property("TenantId").IsModified = false;
                            break;
                    }
                }
        }
    }
}
