﻿
namespace Thex.HigsIntegration.Function.Service.Common.Enummerable
{
    public enum CommonsEnum
    {
        IdIsMissing = 1,
        NullOrEmptyObject = 2,
        DateGreaterThanAnotherDate = 3,
        EntityNotExist = 4,
        EntityMustHaveField = 5,
        FieldMinimumOfCharacteres = 6,
        FieldRequired = 7,
        EntityMustHaveValidField = 8,
        EntityDateMustBeBetween = 9,
        GuestReservationItemSumOfListOfPctDailyRateGreaterThanAllowed = 10,
        GuestReservationItemTotalOfListOfIsMainGreaterThanAllowed = 11,
        InvalidOrderBy = 12,
        InvalidConstructor = 13,
        ParameterInvalid = 14,
        InvalidPeriodParameterAvailability = 15,
        InvalidDateParameterAvailability = 16,
        EmptyFilter = 17
    }
}
