﻿
namespace Thex.HigsIntegration.Function.Service.Common.Enummerable.Thex
{
    public enum PaymentTypeEnum
    {
        InternalUsage = 1,
        Deposit = 2,
        Courtesy = 3,
        Hotel = 4,
        CreditCard = 5,
        TobeBilled = 6,
        Check = 7,
        Money = 8,
        Debit = 9
    }
}
