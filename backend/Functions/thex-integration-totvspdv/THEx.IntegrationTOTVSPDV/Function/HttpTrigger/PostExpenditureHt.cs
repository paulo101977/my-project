using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Azure.WebJobs.Host;
using Newtonsoft.Json;
using Polly;
using Thex.Common.Enumerations;
using Thex.Common.Helpers;
using THEx.IntegrationTOTVSPDV.Commom;
using THEx.IntegrationTOTVSPDV.DAO;
using THEx.IntegrationTOTVSPDV.DTO.PDV;
using THEx.IntegrationTOTVSPDV.DTO.PMS;
using THEx.IntegrationTOTVSPDV.Service;

namespace THEx.IntegrationTOTVSPDV.Function.HttpTrigger
{
    public static class PostExpenditureHt
    {
        #region Deprecated

        [FunctionName("HttpTriggerPostExpenditure")]
        public static async Task<HttpResponseMessage> Run(
          [HttpTrigger(AuthorizationLevel.Function, "post", Route = "expenditure")]
            HttpRequestMessage req, TraceWriter log)
        {
            dynamic data = await req.Content.ReadAsAsync<object>();

            string sjson = Convert.ToString(data);

            PostInquiryDto postInquiry =
                JsonConvert.DeserializeObject<PostInquiryDto>(sjson);

            if (!postInquiry.ServiceId.HasValue || postInquiry.ServiceId == default(int))
            {
                log.Error("O campo ServiceId � obrigrat�rio");
                return null;
            }

            if (postInquiry.Cancelled && (!postInquiry.ReasonId.HasValue || postInquiry.ReasonId == default(int)))
            {
                log.Error("O campo ReasonId � obrigat�rio para cancelamentos.");
                return null;
            }

            ExpenditureService expenditureService = new ExpenditureService();

            if (postInquiry.Cancelled == false)
            {
                var result = expenditureService.ProcessSale(postInquiry, log);

                if (result.HttpStatusCode == (int)HttpStatusCode.Created)
                {

                    expenditureService.SaveMapping(result.Body, postInquiry.Id, log);
                    return req.CreateResponse(HttpStatusCode.Created);
                }
                else
                {
                    MessageDto messageDto = new MessageDto();
                    messageDto.Errors.Add(Utils.CreateMessageError(ErrorMessages.Error003001, postInquiry.Id));
                    return req.CreateResponse(HttpStatusCode.BadRequest, messageDto);
                }
            }
            else
            {
                var result = expenditureService.DeleteSale(postInquiry, log);


                if (result.HttpStatusCode == (int)HttpStatusCode.Created)
                {
                    return req.CreateResponse(HttpStatusCode.OK);
                }
                else if (result.HttpStatusCode == 0)
                {
                    return req.CreateResponse(HttpStatusCode.BadRequest, result.Message);
                }
                else
                {
                    MessageDto messageDto = new MessageDto();
                    messageDto.Errors.Add(Utils.CreateMessageError(ErrorMessages.Error003001, postInquiry.Id));
                    return req.CreateResponse(HttpStatusCode.BadRequest, messageDto);
                }
            }

        }

        #endregion

        [FunctionName("PostExpenditure")]
        public static async Task<HttpResponseMessage> PostExpenditure(
            [HttpTrigger(AuthorizationLevel.Function, "post", Route = "v2/expenditure")]
            HttpRequestMessage req, TraceWriter log)
        {
            var messageList = new MessageDto();
            var postInquiry = await req.Content.ReadAsAsync<PostRequestDto>();

            // TODO: Validar par�metros
            ValidateInquiry(messageList, postInquiry);

            if (messageList.Errors.Count > 0)
                return req.CreateResponse(HttpStatusCode.BadRequest, messageList);

            if (!postInquiry.Cancelled)
            {
                var result = new ExpenditureDao().ProcessSale(log, postInquiry, messageList);

                if (messageList.Errors.Count > 0)
                    return req.CreateResponse(HttpStatusCode.BadRequest, messageList);
                else
                    return req.CreateResponse(HttpStatusCode.OK, result);
            }
            else
            {
                var result = new ExpenditureDao().DeleteSale(log, postInquiry, messageList);

                if (messageList.Errors.Count > 0)
                    return req.CreateResponse(HttpStatusCode.BadRequest, messageList);
                else
                    return req.CreateResponse(HttpStatusCode.OK, result);
            }
        }

        private static void ValidateInquiry(MessageDto message, PostRequestDto postInquiry)
        {
            if ((postInquiry.BillingItemId == 0) || (postInquiry.BillingItemId == default(int)))
            {
                message.Errors.Add(Utils.CreateMessageError(ErrorMessages.Error004001, postInquiry.BillingAccountId));
            }

            if ((postInquiry.BillingInvoiceTypeId == 0) || (postInquiry.BillingInvoiceTypeId == default(int)))
            {
                message.Errors.Add(Utils.CreateMessageError(ErrorMessages.Error005001, postInquiry.BillingInvoiceTypeId.ToString()));
            }

            if (postInquiry.InvoiceNumber.IsNullOrEmpty())
            {
                message.Errors.Add(Utils.CreateMessageError(ErrorMessages.Error006001, postInquiry.InvoiceNumber));
            }

            if (postInquiry.InvoiceSeries.IsNullOrEmpty())
            {
                message.Errors.Add(Utils.CreateMessageError(ErrorMessages.Error007001, postInquiry.InvoiceSeries));
            }

            if (postInquiry.Cancelled && (!postInquiry.ReasonId.HasValue || postInquiry.ReasonId == default(int)))
            {
                message.Errors.Add(Utils.CreateMessageError(ErrorMessages.Error008001, postInquiry.ReasonId.TryParseToInt32().ToString()));
            }
        }
    }
}