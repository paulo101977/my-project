using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Azure.WebJobs.Host;
using RestSharp.Extensions;
using THEx.IntegrationTOTVSPDV.Commom;
using THEx.IntegrationTOTVSPDV.DAO;
using THEx.IntegrationTOTVSPDV.DTO.PMS;

namespace THEx.IntegrationTOTVSPDV.Function.HttpTrigger
{
    public static class GetReasonHt
    {
        #region Deprecated 

        [FunctionName("HttpTriggerGetReason")]
        public static HttpResponseMessage GetReason(
            [HttpTrigger(AuthorizationLevel.Function, "get", Route = "reason")]
            HttpRequestMessage req, TraceWriter log)
        {
            log.Info("C# HTTP trigger function processed a request.");

            // parse query parameter
            string propertyId = req.GetQueryNameValuePairs()
                .FirstOrDefault(q => string.Compare(q.Key, Param.PropertyId, StringComparison.OrdinalIgnoreCase) == 0)
                .Value;

            if (propertyId == null)
            {
                MessageDto messageDto = new MessageDto();
                messageDto.Errors.Add(Utils.CreateMessageError(ErrorMessages.Error001001, Param.PropertyId));

                return req.CreateResponse(HttpStatusCode.BadRequest, messageDto);
            }

            ReasonDao reasonDao = new ReasonDao();

            List<ReasonDto> reasons = reasonDao.FindReasonByPropertyId(log, propertyId);

            return req.CreateResponse(HttpStatusCode.OK, reasons);
        }

        #endregion

        /// <summary>
        /// Find all reasons by property
        /// </summary>
        [FunctionName("GetReasons")]
        public static HttpResponseMessage GetReasonV2(
            [HttpTrigger(AuthorizationLevel.Function, "get", Route = "v2/reason")]
             HttpRequestMessage req, TraceWriter log)
        {
            var propertyId = req.GetQueryNameValuePairs()
                .FirstOrDefault(q => string.Compare(q.Key, Param.PropertyId, StringComparison.OrdinalIgnoreCase) == 0)
                .Value;

            if (!propertyId.HasValue())
                return req.CreateResponse(HttpStatusCode.BadRequest, new MessageDto(Utils.CreateMessageError(ErrorMessages.Error001001, Param.PropertyId)));

            return req.CreateResponse(HttpStatusCode.OK, new ReasonDao().FindReasonByPropertyId(log, propertyId));
        }
    }
}