using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Azure.WebJobs.Host;
using RestSharp.Extensions;
using THEx.IntegrationTOTVSPDV.Commom;
using THEx.IntegrationTOTVSPDV.DAO;
using THEx.IntegrationTOTVSPDV.DTO.PDV;
using THEx.IntegrationTOTVSPDV.DTO.PMS;

namespace THEx.IntegrationTOTVSPDV.Function.HttpTrigger
{
    public static class GetPropertyHt
    {
        #region Deprecated

        [FunctionName("HttpTriggerGetProperty")]
        public static HttpResponseMessage GetProperties(
            [HttpTrigger(AuthorizationLevel.Function, "get", Route = "propertyDetails")]
            HttpRequestMessage req, TraceWriter log)
        {
            string propertyDoc = req.GetQueryNameValuePairs()
                .FirstOrDefault(q => string.Compare(q.Key, Param.PropertyDoc, StringComparison.OrdinalIgnoreCase) == 0)
                .Value;

            string propertyid = req.GetQueryNameValuePairs()
                .FirstOrDefault(q => string.Compare(q.Key, Param.PropertyId, StringComparison.OrdinalIgnoreCase) == 0)
                .Value;

            if (propertyDoc == null && propertyid == null)
            {
                MessageDto messageDto = new MessageDto();
                messageDto.Errors.Add(Utils.CreateMessageError(ErrorMessages.Error001001, Param.PropertyDoc));
                messageDto.Errors.Add(Utils.CreateMessageError(ErrorMessages.Error001001, Param.PropertyId));

                return req.CreateResponse(HttpStatusCode.BadRequest, messageDto);
            }

            PropertyDao propertyDao = new PropertyDao();
            CompanyDto company = propertyDao.FindHotelByDocument(log, propertyDoc, propertyid);

            if (company != null) return req.CreateResponse(HttpStatusCode.OK, company);

            return req.CreateResponse(HttpStatusCode.OK);
        }

        #endregion

        /// <summary>
        /// Find all properties by document
        /// </summary>
        [FunctionName("GetProperties")]
        public static HttpResponseMessage GetPropertiesV2(
            [HttpTrigger(AuthorizationLevel.Function, "get", Route = "v2/property")]
             HttpRequestMessage req, TraceWriter log)
        {
            string propertyDoc = req.GetQueryNameValuePairs()
                .FirstOrDefault(q => string.Compare(q.Key, Param.PropertyDoc, StringComparison.OrdinalIgnoreCase) == 0)
                .Value;

            if (!propertyDoc.HasValue())
                return req.CreateResponse(HttpStatusCode.BadRequest, new MessageDto(Utils.CreateMessageError(ErrorMessages.Error001001, Param.PropertyDoc)));

            return req.CreateResponse(HttpStatusCode.OK, new PropertyDao().FindProperty(log, propertyDoc));
        }
    }
}