using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Azure.WebJobs.Host;

namespace THEx.IntegrationTOTVSPDV.Function.HttpTrigger
{
    public static class ConnectionTestHt
    {
        #region Deprecated

        [FunctionName("ConnectionTestHt")]
        public static HttpResponseMessage Run(
            [HttpTrigger(AuthorizationLevel.Function, "get", Route = "connectionTest")]
            HttpRequestMessage req, TraceWriter log)
        {
            return req.CreateResponse(HttpStatusCode.OK);
        }

        #endregion

        [FunctionName("GetStatus")]
        public static HttpResponseMessage GetStatusV2(
          [HttpTrigger(AuthorizationLevel.Function, "get", Route = "v2/status")]
           HttpRequestMessage req)
        {
            return req.CreateResponse(HttpStatusCode.OK);
        }
    }
}