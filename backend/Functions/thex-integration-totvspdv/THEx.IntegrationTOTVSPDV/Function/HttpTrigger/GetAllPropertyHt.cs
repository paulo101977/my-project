using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Azure.WebJobs.Host;
using THEx.IntegrationTOTVSPDV.Commom;
using THEx.IntegrationTOTVSPDV.DAO;
using THEx.IntegrationTOTVSPDV.DTO.PDV;
using THEx.IntegrationTOTVSPDV.DTO.PMS;

namespace THEx.IntegrationTOTVSPDV.Function.HttpTrigger
{
    public static class GetAllPropertyHt
    {
        [FunctionName("HttpTriggerGetAllProperty")]
        public static async Task<HttpResponseMessage> Run(
            [HttpTrigger(AuthorizationLevel.Function, "get", Route = "propertyList")]
            HttpRequestMessage req, TraceWriter log)
        {
            string propertyDoc = req.GetQueryNameValuePairs()
                .FirstOrDefault(q => string.Compare(q.Key, Param.PropertyDoc, StringComparison.OrdinalIgnoreCase) == 0)
                .Value;

            if (propertyDoc == null)
            {
                MessageDto messageDto = new MessageDto();
                messageDto.Errors.Add(Utils.CreateMessageError(ErrorMessages.Error001001, Param.PropertyDoc));

                return req.CreateResponse(HttpStatusCode.BadRequest, messageDto);
            }

            PropertyDao propertyDao = new PropertyDao();
            var companyList = propertyDao.FindAllHotelByDocument(log, propertyDoc);

            if (companyList != null && companyList.Count > 0) return req.CreateResponse(HttpStatusCode.OK, companyList);

            return req.CreateResponse(HttpStatusCode.OK);
        }
    }
}