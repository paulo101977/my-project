using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Azure.WebJobs.Host;
using RestSharp.Extensions;
using THEx.IntegrationTOTVSPDV.Commom;
using THEx.IntegrationTOTVSPDV.DAO;
using THEx.IntegrationTOTVSPDV.DTO.PMS;

namespace THEx.IntegrationTOTVSPDV.Function.HttpTrigger
{
    public static class GetServiceHt
    {
        #region Deprecated

        [FunctionName("HttpTriggerGetService")]
        public static HttpResponseMessage GetService(
        [HttpTrigger(AuthorizationLevel.Function, "get", Route = "service")]
            HttpRequestMessage req, TraceWriter log)
        {
            log.Info("C# HTTP trigger function processed a request.");

            // parse query parameter
            string propertyId = req.GetQueryNameValuePairs()
                .FirstOrDefault(q => string.Compare(q.Key, Param.PropertyId, StringComparison.OrdinalIgnoreCase) == 0)
                .Value;

            if (propertyId == null)
            {
                MessageDto messageDto = new MessageDto();
                messageDto.Errors.Add(Utils.CreateMessageError(ErrorMessages.Error001001, Param.PropertyId));

                return req.CreateResponse(HttpStatusCode.BadRequest, messageDto);
            }

            ServiceDao serviceDao = new ServiceDao();

            List<ServiceDto> services = serviceDao.FindServiceByPropertyId(log, propertyId);

            return req.CreateResponse(HttpStatusCode.OK, services);
        }

        #endregion

        /// <summary>
        /// Find all service by property
        /// </summary>
        [FunctionName("GetPos")]
        public static HttpResponseMessage GetServiceV2(
            [HttpTrigger(AuthorizationLevel.Function, "get", Route = "v2/pos")]
             HttpRequestMessage req, TraceWriter log)
        {
            string propertyId = req.GetQueryNameValuePairs()
                .FirstOrDefault(q => string.Compare(q.Key, Param.PropertyId, StringComparison.OrdinalIgnoreCase) == 0)
                .Value;

            if (!propertyId.HasValue())
                return req.CreateResponse(HttpStatusCode.BadRequest, new MessageDto(Utils.CreateMessageError(ErrorMessages.Error001001, Param.PropertyId)));

            return req.CreateResponse(HttpStatusCode.OK, new ServiceDao().FindServiceByPropertyId(log, propertyId));
        }

        /// <summary>
        /// Find all service by property
        /// </summary>
        [FunctionName("GetPayment")]
        public static HttpResponseMessage GetPaymentV2(
            [HttpTrigger(AuthorizationLevel.Function, "get", Route = "v2/payment")]
             HttpRequestMessage req, TraceWriter log)
        {
            string propertyId = req.GetQueryNameValuePairs()
                .FirstOrDefault(q => string.Compare(q.Key, Param.PropertyId, StringComparison.OrdinalIgnoreCase) == 0)
                .Value;

            if (!propertyId.HasValue())
                return req.CreateResponse(HttpStatusCode.BadRequest, new MessageDto(Utils.CreateMessageError(ErrorMessages.Error001001, Param.PropertyId)));

            return req.CreateResponse(HttpStatusCode.OK, new PaymentDao().FindPaymentByPropertyId(log, propertyId));
        }
    }
}