using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Azure.WebJobs.Host;
using THEx.IntegrationTOTVSPDV.Commom;
using THEx.IntegrationTOTVSPDV.DAO;
using THEx.IntegrationTOTVSPDV.DTO.PDV;
using THEx.IntegrationTOTVSPDV.DTO.PMS;
using THEx.IntegrationTOTVSPDV.VO;

namespace THEx.IntegrationTOTVSPDV.Function.HttpTrigger
{
    public static class GetGuestHt
    {
        #region Deprecated

        [FunctionName("HttpTriggerGetGuest")]
        public static HttpResponseMessage Run(
            [HttpTrigger(AuthorizationLevel.Function, "get", Route = "guest")]
            HttpRequestMessage req, TraceWriter log)
        {
            MessageDto messageDto = null;
            FilterParamVo filterParamVo = new FilterParamVo();

            filterParamVo.PropertyId = req.GetQueryNameValuePairs()
                .FirstOrDefault(q => string.Compare(q.Key, Param.PropertyId, StringComparison.OrdinalIgnoreCase) == 0)
                .Value;

            if (filterParamVo.PropertyId == null)
            {
                messageDto = new MessageDto();
                messageDto.Errors.Add(Utils.CreateMessageError(ErrorMessages.Error001001, Param.PropertyId));
                return req.CreateResponse(HttpStatusCode.BadRequest, messageDto);
            }

            filterParamVo.Type = req.GetQueryNameValuePairs()
                .FirstOrDefault(q => string.Compare(q.Key, Param.Type, StringComparison.OrdinalIgnoreCase) == 0)
                .Value.TryParseToInt32();

            filterParamVo.Name = req.GetQueryNameValuePairs()
                .FirstOrDefault(q => string.Compare(q.Key, Param.Name, StringComparison.OrdinalIgnoreCase) == 0)
                .Value;

            filterParamVo.GivenName = req.GetQueryNameValuePairs()
                .FirstOrDefault(q => string.Compare(q.Key, Param.GivenName, StringComparison.OrdinalIgnoreCase) == 0)
                .Value;

            filterParamVo.RoomNumber = req.GetQueryNameValuePairs()
                .FirstOrDefault(q => string.Compare(q.Key, Param.RoomNumber, StringComparison.OrdinalIgnoreCase) == 0)
                .Value;

            filterParamVo.Number = req.GetQueryNameValuePairs()
                .FirstOrDefault(q => string.Compare(q.Key, Param.Number, StringComparison.OrdinalIgnoreCase) == 0)
                .Value;

            GuestDao guestDao = new GuestDao();
            List<CustomerDto> customers = guestDao.FindGuestByFilter(log, filterParamVo);

            return req.CreateResponse(HttpStatusCode.OK, customers);
        }

        #endregion

        [FunctionName("GetGuest")]
        public static HttpResponseMessage GetGuest(
           [HttpTrigger(AuthorizationLevel.Function, "get", Route = "v2/guest")]
            HttpRequestMessage req, TraceWriter log)
        {
            FilterParamVo filterParamVo = new FilterParamVo();

            filterParamVo.PropertyId = req.GetQueryNameValuePairs()
                .FirstOrDefault(q => string.Compare(q.Key, Param.PropertyId, StringComparison.OrdinalIgnoreCase) == 0)
                .Value;

            if (filterParamVo.PropertyId == null)
                return req.CreateResponse(HttpStatusCode.BadRequest, new MessageDto(Utils.CreateMessageError(ErrorMessages.Error001001, Param.PropertyId)));

            filterParamVo.Type = req.GetQueryNameValuePairs()
                 .FirstOrDefault(q => string.Compare(q.Key, Param.Type, StringComparison.OrdinalIgnoreCase) == 0)
                 .Value.TryParseToInt32();

            filterParamVo.Name = req.GetQueryNameValuePairs()
                .FirstOrDefault(q => string.Compare(q.Key, Param.Name, StringComparison.OrdinalIgnoreCase) == 0)
                .Value;

            filterParamVo.GivenName = req.GetQueryNameValuePairs()
                .FirstOrDefault(q => string.Compare(q.Key, Param.GivenName, StringComparison.OrdinalIgnoreCase) == 0)
                .Value;

            filterParamVo.RoomNumber = req.GetQueryNameValuePairs()
                .FirstOrDefault(q => string.Compare(q.Key, Param.RoomNumber, StringComparison.OrdinalIgnoreCase) == 0)
                .Value;

            filterParamVo.Number = req.GetQueryNameValuePairs()
                .FirstOrDefault(q => string.Compare(q.Key, Param.Number, StringComparison.OrdinalIgnoreCase) == 0)
                .Value;

            return req.CreateResponse(HttpStatusCode.OK, new GuestDao().FindGuest(log, filterParamVo));
        }
    }
}