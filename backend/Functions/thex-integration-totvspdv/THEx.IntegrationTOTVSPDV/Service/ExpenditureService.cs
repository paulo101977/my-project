﻿using Microsoft.Azure.WebJobs.Host;
using Microsoft.WindowsAzure.Storage.Table;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.Common.Enumerations;
using Thex.Common.Helpers;
using THEx.IntegrationTOTVSPDV.Commom;
using THEx.IntegrationTOTVSPDV.DAO;
using THEx.IntegrationTOTVSPDV.DTO.PDV;
using THEx.IntegrationTOTVSPDV.DTO.PMS;
using THEx.IntegrationTOTVSPDV.Entity;
using THEx.IntegrationTOTVSPDV.Storage.Table;

namespace THEx.IntegrationTOTVSPDV.Service
{
    public class ExpenditureService
    {
        public string UrlBaseSA
        {
            get
            {
                switch (EnumHelper.GetEnumValue<EnvironmentEnum>(Environment.GetEnvironmentVariable("CloudEnvironment", EnvironmentVariableTarget.Process), true))
                {
                    case EnvironmentEnum.Production: return "https://thex-api-sa.totvs.com.br/api/";
                    case EnvironmentEnum.BugFix: return "https://thex-api-sa-fabric-bugfix.azurewebsites.net/api/";
                    case EnvironmentEnum.Test: return "https://thex-api-sa-fabric-tst.azurewebsites.net/api/";
                    case EnvironmentEnum.UAT: return "https://thex-api-sa-fabric-uat.azurewebsites.net/api/";
                    case EnvironmentEnum.Staging: return "https://thex-api-sa-fabric-stg.azurewebsites.net/api/";
                    case EnvironmentEnum.Development:
                    default: return "https://thex-api-sa-fabric.azurewebsites.net/api/";
                }
            }
        }

        public string UrlBasePMS
        {
            get
            {
                switch (EnumHelper.GetEnumValue<EnvironmentEnum>(Environment.GetEnvironmentVariable("CloudEnvironment", EnvironmentVariableTarget.Process), true))
                {
                    case EnvironmentEnum.Production: return "https://thex-api-pms.totvs.com.br/api/";
                    case EnvironmentEnum.BugFix: return "https://thex-api-pms-fabric-bugfix.azurewebsites.net/api/";
                    case EnvironmentEnum.Test: return "https://thex-api-pms-fabric-tst.azurewebsites.net/api/";
                    case EnvironmentEnum.UAT: return "https://thex-api-pms-fabric-uat.azurewebsites.net/api/";
                    case EnvironmentEnum.Staging: return "https://thex-api-pms-fabric-stg.azurewebsites.net/api/";
                    case EnvironmentEnum.Development:
                    default: return "https://thex-api-pms-fabric.azurewebsites.net/api/";
                }
            }
        }

        public ApiResultDto ProcessSale(PostInquiryDto postInquiryDto, TraceWriter log)
        {
            BillingAccountItem billingAccountItem = new BillingAccountItem
            {
                PropertyId = postInquiryDto.PropertyId,
                BillingAccountId = postInquiryDto.Number,
                BillingItemId = postInquiryDto.ServiceId ?? default(int),
                DetailList = postInquiryDto.SaleDetails,
                TenantId = postInquiryDto.TenantId,
                Amount = 0
            };

            foreach (var t in billingAccountItem.DetailList)
                billingAccountItem.Amount += t.Amount;

            string jsonBillingAccountItem = JsonConvert.SerializeObject(billingAccountItem);

            var result = RequestHttp.SendPost(ApiThEx.UrlPostSalePos(), jsonBillingAccountItem, postInquiryDto.TenantId, log);

            return result;
        }

        public ApiResultDto DeleteSale(PostInquiryDto postInquiryDto, TraceWriter log)
        {
            MessageDto messageDto = new MessageDto();

            IdentifierMappingEntity imEntity = FindByPosSale(postInquiryDto);

            log.Info("ExpenditureService == DeleteSale == imEntity == " + (imEntity != null).ToString());

            if (imEntity == null)
            {
                messageDto.Errors.Add(Utils.CreateMessageError(ErrorMessages.Error002001, postInquiryDto.Id));
            }

            ReasonDao reasonDao = new ReasonDao();
            ReasonDto reason = reasonDao.findReasonById(postInquiryDto.PropertyId, postInquiryDto.ReasonId ?? default(int));

            log.Info("ExpenditureService == DeleteSale == reason == " + (reason != null).ToString());

            if (reason == null)
            {
                messageDto.Errors.Add(Utils.CreateMessageError(ErrorMessages.Error002002, postInquiryDto.ReasonId.ToString()));
            }

            log.Info("ExpenditureService == DeleteSale == messageDto.Errors.Count == " + messageDto.Errors.Count);

            if (messageDto.Errors.Count > 0)
            {
                ApiResultDto apiResultDto = new ApiResultDto();
                apiResultDto.Message = JsonConvert.SerializeObject(messageDto);
                apiResultDto.HttpStatusCode = 0;
                return apiResultDto;
            }

            BillingAccountItemReversal reversal = new BillingAccountItemReversal();
            reversal.id = postInquiryDto.Number;
            reversal.tenantId = postInquiryDto.TenantId;

            BillingAccountItem billingAccountItem = new BillingAccountItem
            {
                PropertyId = postInquiryDto.PropertyId,
                BillingAccountId = postInquiryDto.Number,
                OriginalBillingAccountItemId = imEntity.ThexId,
                TenantId = postInquiryDto.TenantId,
                ReasonId = reason.Id
            };

            reversal.billingAccountItems = new List<BillingAccountItem>();
            reversal.billingAccountItems.Add(billingAccountItem);

            string jsonBillingAccountItemReversal = JsonConvert.SerializeObject(reversal);

            var result = RequestHttp.SendPost(ApiThEx.UrlDeleteSalePos(), jsonBillingAccountItemReversal, postInquiryDto.TenantId, log);

            return result;

        }

        public void SaveMapping(string sResult, string posId, TraceWriter log)
        {
            BillingAccountItem billingAccountItemResult =
                JsonConvert.DeserializeObject<BillingAccountItem>(sResult);

            IdentifierMappingEntity imEntity = new IdentifierMappingEntity(
                billingAccountItemResult.PropertyId, posId, billingAccountItemResult.Id);

            IdentifierMappingTable imTable = new IdentifierMappingTable();
            imTable.CreateConnection();

            Task<TableResult> resultTable = imTable.Insert(imEntity, log);
        }

        IdentifierMappingEntity FindByPosSale(PostInquiryDto postInquiryDto)
        {
            IdentifierMappingTable imTable = new IdentifierMappingTable();
            imTable.CreateConnection();

            Task<List<IdentifierMappingEntity>> result =
                    imTable.FindbyPosSale(postInquiryDto.PropertyId, postInquiryDto.Id);

            List<IdentifierMappingEntity> list = result.Result;

            if (list.Count != 0)
            {
                return list[0];
            }
            else
            {
                return null;
            }
        }










        public ApiResultDto ProcessSale(PostRequestDto postRequest, TraceWriter log)
        {
            BillingAccountItem billingAccountItem = new BillingAccountItem
            {
                PropertyId = postRequest.PropertyId,
                BillingAccountId = postRequest.BillingAccountId,
                BillingItemId = postRequest.BillingItemId,
                DetailList = postRequest.SaleDetails,
                TenantId = postRequest.TenantId,
                Amount = 0
            };

            foreach (var t in billingAccountItem.DetailList)
                billingAccountItem.Amount += t.Amount;

            string jsonBillingAccountItem = JsonConvert.SerializeObject(billingAccountItem);

            var result = RequestHttp.SendPost((UrlBasePMS + "BillingAccountItemIntegration/debit"), jsonBillingAccountItem, postRequest.TenantId, log);

            return result;
        }
    }
}