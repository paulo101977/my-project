﻿using Microsoft.Azure;
using Microsoft.Azure.WebJobs.Host;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace THEx.IntegrationTOTVSPDV.Storage.Table
{
    public class BaseTable
    {
        public CloudStorageAccount StorageAccount;
        public CloudTable CloudTable;

        public async Task<Boolean> CreateConnection(string tableDescription)
        {
            StorageAccount = CloudStorageAccount.Parse(Environment.GetEnvironmentVariable("connectionStorage", EnvironmentVariableTarget.Process));

            CloudTableClient tableClient = StorageAccount.CreateCloudTableClient();

            CloudTable = tableClient.GetTableReference(tableDescription);

            return await CloudTable.CreateIfNotExistsAsync();
        }

        public async Task<TableResult> Insert(TableOperation insertOperation, TraceWriter log)
        {
            log.Info("BaseTable == Entrou no Insert == Antes do ExecuteAsync");

            TableResult tableResult = null;

            try
            {
                tableResult = await CloudTable.ExecuteAsync(insertOperation);
                log.Info("BaseTable == Depois do ExecuteAsync");
            }
            catch(Exception ex)
            {
                log.Info("BaseTable == catch do ExecuteAsync == " + ex.Message);
            }

            return tableResult;
        }
    }
}
