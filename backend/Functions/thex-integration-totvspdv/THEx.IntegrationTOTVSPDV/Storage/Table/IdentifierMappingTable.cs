﻿using Microsoft.Azure.WebJobs.Host;
using Microsoft.WindowsAzure.Storage.Table;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using THEx.IntegrationTOTVSPDV.Entity;

namespace THEx.IntegrationTOTVSPDV.Storage.Table
{
    class IdentifierMappingTable : BaseTable
    {
        string TableDescription = "ThexIntegrationTotvsPdvIdentifierMapping";

        public async void CreateConnection()
        {
            await CreateConnection(TableDescription);
        }

        public async Task<TableResult> Insert(IdentifierMappingEntity entity, TraceWriter log)
        {
            TableOperation insertOperation = TableOperation.Insert(entity);

            return await Insert(insertOperation, log);

        }

        public async Task<List<IdentifierMappingEntity>> FindbyPosSale(int propertyId, string posId)
        {
            TableQuery<IdentifierMappingEntity> query = new TableQuery<IdentifierMappingEntity>()
                .Where(TableQuery.CombineFilters(
                    TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.Equal, 
                        Convert.ToString(propertyId)),
                    TableOperators.And,
                    TableQuery.GenerateFilterCondition("RowKey", QueryComparisons.Equal, posId)));

            List<IdentifierMappingEntity> identifierMappingEntityList = new List<IdentifierMappingEntity>();

            TableContinuationToken token = null;
            do
            {
                TableQuerySegment<IdentifierMappingEntity> resultSegment =
                    await this.CloudTable.ExecuteQuerySegmentedAsync(query, token);
                token = resultSegment.ContinuationToken;


                foreach (IdentifierMappingEntity entity in resultSegment.Results)
                {
                    identifierMappingEntityList.Add(entity);

                }
            } while (token != null);

            return identifierMappingEntityList;
        }


    }

    
}
