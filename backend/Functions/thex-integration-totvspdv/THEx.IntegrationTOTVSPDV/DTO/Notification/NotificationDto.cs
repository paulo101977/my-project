﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace THEx.IntegrationTOTVSPDV.DTO.Notification
{
    public class NotificationDto
    {
        public string Code { get; set; }
        public string Message { get; set; }
        public string DetailedMessage { get; set; }

        public List<NotificationDto> Details { get; set; }
    }
}
