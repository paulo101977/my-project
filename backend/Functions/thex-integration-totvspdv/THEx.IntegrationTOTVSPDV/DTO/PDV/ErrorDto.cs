﻿namespace THEx.IntegrationTOTVSPDV.DTO.PMS
{
    public class ErrorDto
    {
        public string Code { get; set; }
        public string Message { get; set; }
    }
}