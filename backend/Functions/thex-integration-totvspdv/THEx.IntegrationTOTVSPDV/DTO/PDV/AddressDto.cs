﻿namespace THEx.IntegrationTOTVSPDV.DTO.PDV
{
    public class AddressDto
    {
        public string StreetName { get; set; }
        public string StreetNumber { get; set; }
        public string Neighborhood { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public string ZipCode { get; set; }
        public string PostalCode { get; set; }
    }
}