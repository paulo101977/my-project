﻿namespace THEx.IntegrationTOTVSPDV.DTO.PDV
{
    public class PostingDetailDto
    {
        public double Amount { get; set; }
        public double DiscontAmount { get; set; }
        public string PmsCode { get; set; }
        public string Description { get; set; }
    }
}