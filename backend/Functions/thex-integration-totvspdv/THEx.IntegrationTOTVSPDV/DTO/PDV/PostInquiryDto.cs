﻿using System.Collections.Generic;
using THEx.IntegrationTOTVSPDV.DTO.PMS;

namespace THEx.IntegrationTOTVSPDV.DTO.PDV
{
    public class PostInquiryDto
    {
        public string Id { get; set; }
        public string TenantId { get; set; }
        public bool Cancelled { get; set; }
        public int? ServiceId { get; set; }
        public int PropertyId { get; set; }
        public string Number { get; set; }
        public string Ticket { get; set; }
        public int? ReasonId { get; set; }
        public List<SaleDatail> SaleDetails { get; set; }
    }
}