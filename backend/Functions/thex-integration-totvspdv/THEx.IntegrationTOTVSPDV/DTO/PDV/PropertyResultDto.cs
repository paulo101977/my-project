﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace THEx.IntegrationTOTVSPDV.DTO.PDV
{

    public class PropertyResulDto
    {
        [JsonProperty("list")]
        public List<PropertyResultDetailsDto> PropertyResultDetailsList { get; set; }

    }

    public class PropertyResultDetailsDto
    {
        public string id { get; set; }
        public string companyId { get; set; }
        public string nome { get; set; }
        public string tenantId { get; set; }
        public string razaosocial { get; set; }
        public string CNPJ { get; set; }
        public string IM { get; set; }
        public string IE { get; set; }
        public string logradouro { get; set; }
        public string bairro { get; set; }
        public string CEP { get; set; }
        public string numero { get; set; }
        public string cidade { get; set; }
        public string estado { get; set; }
        public string ibge { get; set; }
        public string sgPais { get; set; }
        public string pais { get; set; }
        public string sgEstado { get; set; }
    }
}
