﻿namespace THEx.IntegrationTOTVSPDV.DTO.PDV
{
    public class PaymentDetailDto
    {
        public string CurrencyCode { get; set; }
        public int Code { get; set; }
        public double Amount { get; set; }
    }
}