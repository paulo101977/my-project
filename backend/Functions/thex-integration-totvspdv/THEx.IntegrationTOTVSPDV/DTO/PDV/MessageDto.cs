﻿using System.Collections.Generic;

namespace THEx.IntegrationTOTVSPDV.DTO.PMS
{
    public class MessageDto
    {
        public MessageDto()
        {
            Errors = new List<ErrorDto>();
        }

        public MessageDto(ErrorDto error)
        {
            Errors = new List<ErrorDto>();
            Errors.Add(error);
        }

        public List<ErrorDto> Errors { get; set; }
    }
}