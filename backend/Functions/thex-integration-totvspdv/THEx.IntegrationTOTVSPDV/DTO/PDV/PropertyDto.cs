﻿namespace THEx.IntegrationTOTVSPDV.DTO.PDV
{
    public class PropertyDto
    {
        public string Id { get; set; }
        public string TenantId { get; set; }
        public string Name { get; set; }
        public string Cnpj { get; set; }
        public string CorparateName { get; set; }
        public string MunicipalRegistration { get; set; }
        public string StateRegistration { get; set; }
        public AddressDto Address { get; set; }
    }
}