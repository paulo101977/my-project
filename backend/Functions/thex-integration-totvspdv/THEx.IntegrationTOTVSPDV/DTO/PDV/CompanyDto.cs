﻿namespace THEx.IntegrationTOTVSPDV.DTO.PDV
{
    public class CompanyDto
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Cnpj { get; set; }
        public string CorparateName { get; set; }
        public PropertyDto Property { get; set; }
    }
}