﻿namespace THEx.IntegrationTOTVSPDV.DTO.PDV
{
    public class CustomerDto
    {
        public string NamePrefix { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string ZipCode { get; set; }
        public string Country { get; set; }
        public string Email { get; set; }
        public string Comment { get; set; }
        public string RoomNumber { get; set; }
        public int? ReservationItemStatusId { get; set; }
        public int? GuestStatusId { get; set; }
        public int? BillingAccountStatusId { get; set; }
        public int BillingAccountTypeId { get; set; }
        public string Number { get; set; }
        public string Pin { get; set; }
    }
}