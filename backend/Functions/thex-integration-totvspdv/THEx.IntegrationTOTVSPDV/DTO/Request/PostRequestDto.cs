﻿using System;
using System.Collections.Generic;
using THEx.IntegrationTOTVSPDV.DTO.PMS;

namespace THEx.IntegrationTOTVSPDV.DTO.PDV
{
    public class PostRequestDto
    {
        public string BillingAccountId { get; set; }
        public int PropertyId { get; set; }
        public string TenantId { get; set; }
        public bool Cancelled { get; set; }
        public int BillingItemId { get; set; }
        public DateTime InvoiceEmissionDate { get; set; }
        public string InvoiceNumber { get; set; }
        public string InvoiceSeries { get; set; }
        public int BillingInvoiceTypeId { get; set; }
        public int PaymentId { get; set; }
        public string IntegratorId { get; set; }
        public int? ReasonId { get; set; }
        public double? Amount { get; set; }

        public List<SaleDatail> SaleDetails { get; set; }
    }
}