﻿using Newtonsoft.Json;

namespace THEx.IntegrationTOTVSPDV.DTO.PMS
{
    public class ReasonDto
    {
        [JsonProperty("reasonId")]
        public int Id { get; set; }

        [JsonProperty("reasonName")]
        public string Description { get; set; }

        [JsonProperty("ReasonType")]
        public string Type { get; set; }
    }
}