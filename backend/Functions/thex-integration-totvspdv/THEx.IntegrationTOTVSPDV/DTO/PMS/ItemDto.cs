﻿namespace THEx.IntegrationTOTVSPDV.DTO.PMS
{
    public class ItemDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}