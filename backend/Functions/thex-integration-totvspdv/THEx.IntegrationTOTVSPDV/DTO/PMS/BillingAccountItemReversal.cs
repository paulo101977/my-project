﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace THEx.IntegrationTOTVSPDV.DTO.PMS
{
    public class BillingAccountItemReversal
    {
        public string id { get; set; }
        public List<BillingAccountItem> billingAccountItems { get; set; }

        public string tenantId { get; set; }
    }
}
