﻿using Newtonsoft.Json;

namespace THEx.IntegrationTOTVSPDV.DTO.PMS
{
    public class ServiceDto
    {
        [JsonProperty("BillingItemId")]
        public int Id { get; set; }

        [JsonProperty("BillingItemName")]
        public string Description { get; set; }
    }
}