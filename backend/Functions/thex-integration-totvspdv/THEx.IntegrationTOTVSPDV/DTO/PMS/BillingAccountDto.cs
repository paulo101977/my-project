﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace THEx.IntegrationTOTVSPDV.DTO.PMS
{
    public class BillingAccountPMSResponseDto
    {
        public bool hasNext { get; set; }

        public List<BillingAccountPMSDto> Items { get; set; }
    }

    public class BillingAccountPMSDto
    {
        public Guid Id { get; set; }
        public string ReservationCode { get; set; }
        public int AccountTypeId { get; set; }
        public string AccountTypeName { get; set; }
        public string BillingAccountName { get; set; }
        public string RoomTypeName { get; set; }
        public string RoomNumber { get; set; }
        public decimal Balance { get; set; }
        public string BillingAccountId { get; set; }
        public int StatusId { get; set; }
        public string HolderName { get; set; }
        public Guid GroupKey { get; set; }
        public Guid TenantId { get; set; }
        public string ReservationStatusName { get; set; }
        public int? ReservationStatusId { get; set; }

        public Guid CurrencyId { get; set; }
        public string CurrencySymbol { get; set; }

        public DateTime? ArrivalDate { get; set; }
        public DateTime? DepartureDate { get; set; }

        public string BillingAccountDefaultName { get; set; }

        public bool AnyOpenAccount { get; set; }
        public bool AnyClosedAccount { get; set; }
        public bool AnyPendingAccount { get; set; }

        public string BalanceFormatted
        {
            get
            {
                string resultFormatted;

                if (Balance < 0)
                    resultFormatted = $"- {CurrencySymbol} {(Balance * (-1)).ToString("N", CultureInfo.CurrentCulture)}";
                else
                    resultFormatted = $"{CurrencySymbol} {Balance.ToString("N", CultureInfo.CurrentCulture)}";

                return resultFormatted;
            }
        }


        public string ResultFormatted
        {
            get
            {
                var resultFormatted = new StringBuilder();

                resultFormatted.Append($"{HolderName} ");

                if (!string.IsNullOrEmpty(BillingAccountName))
                    resultFormatted.Append($"({BillingAccountName}) ");
                else
                {
                    BillingAccountName = BillingAccountDefaultName;
                    resultFormatted.Append($"({BillingAccountName}) ");
                }

                if (!string.IsNullOrEmpty(ReservationCode))
                    resultFormatted.Append($"| {ReservationCode} ");

                if (!string.IsNullOrEmpty(RoomTypeName) && !string.IsNullOrEmpty(RoomNumber))
                    resultFormatted.Append($"| {RoomTypeName}-{RoomNumber} ");

                resultFormatted.Append(BalanceFormatted);

                return resultFormatted.ToString();
            }
        }
    }
}
