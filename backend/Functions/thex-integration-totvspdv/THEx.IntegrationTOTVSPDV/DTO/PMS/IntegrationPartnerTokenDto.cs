﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace THEx.IntegrationTOTVSPDV.DTO.PMS
{
    public class IntegrationPartnerTokenDto 
    {
        public Guid Id { get; set; }
        public bool Authenticated { get; set; }
        public string Created { get; set; }
        public string Expiration { get; set; }
        public string NewPasswordToken { get; set; }
    }
}
