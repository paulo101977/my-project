﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace THEx.IntegrationTOTVSPDV.DTO.PMS
{
    public class SearchBillingAccountDto
    {
        public string ReservationCode { get; set; }
        public string AccountTypeName { get; set; }
        public string BillingAccountName { get; set; }
        public string RoomTypeName { get; set; }
        public string FreeTextSearch { get; set; }
        public int? AccountStatus { get; set; }

        public bool? OpenedAccounts { get; set; }
        public bool? ClosedAccounts { get; set; }
        public bool? PendingAccounts { get; set; }

        public Guid? BaseAccountIgnored { get; set; }

        public List<Guid> BillingAccountIgnoredList { get; set; }
    }
}
