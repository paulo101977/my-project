﻿using System.Collections.Generic;

namespace THEx.IntegrationTOTVSPDV.DTO.PMS
{
    public class BillingAccountItem
    {
        public string Id { get; set; }
        public string TenantId { get; set; }
        public int PropertyId { get; set; }
        public string BillingAccountId { get; set; }
        public int BillingItemId { get; set; }
        public string OriginalBillingAccountItemId { get; set; }
        public double Amount { get; set; }
        public int ReasonId { get; set; }
        public List<SaleDatail> DetailList { get; set; }
    }
}