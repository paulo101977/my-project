﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace THEx.IntegrationTOTVSPDV.DTO.PMS
{
    public class TokenInfoDto
    {
        public string TokenClient { get; set; }
        public string TokenApplication { get; set; }
        public string TenantId { get; set; }
    }
}
