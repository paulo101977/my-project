﻿namespace THEx.IntegrationTOTVSPDV.DTO.PMS
{
    public class SaleDatail
    {
        public decimal Quantity { get; set; }
        public string Description { get; set; }
        public double Amount { get; set; }
        public string Group { get; set; }
    }
}