﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace THEx.IntegrationTOTVSPDV.DTO.Response
{
    public class BillingInvoiceResponseDto
    {
        public Guid Id { get; set; }
        public string BillingInvoiceNumber { get; set; }
        public string BillingInvoiceSeries { get; set; }
        public DateTime EmissionDate { get; set; }
        public DateTime? CancelDate { get; set; }
        public Guid? EmissionUserId { get; set; }
        public Guid? CancelUserId { get; set; }
        public string ExternalRps { get; set; }
        public string ExternalNumber { get; set; }
        public string ExternalSeries { get; set; }
        public DateTime? ExternalEmissionDate { get; set; }
        public Guid BillingAccountId { get; set; }
        public Guid BillingInvoicePropertyId { get; set; }
        public int? BillingInvoiceCancelReasonId { get; set; }
        public string IntegratorLink { get; set; }
        public string IntegratorXml { get; set; }
        public string IntegratorId { get; set; }
        public int BillingInvoiceStatusId { get; set; }
        public string ExternalCodeNumber { get; set; }
        public DateTime? ErrorDate { get; set; }
        public int BillingInvoiceTypeId { get; set; }
        public string BillingInvoiceTypeName { get; set; }
    }
}
