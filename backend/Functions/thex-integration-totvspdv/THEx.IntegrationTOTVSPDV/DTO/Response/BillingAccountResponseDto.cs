﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace THEx.IntegrationTOTVSPDV.DTO.PMS
{
    public class BillingAccountResponseDto
    {
        public bool hasNext { get; set; }
        public List<BillingAccountDto> Items { get; set; }

        public static BillingAccountResponseDto NullInstance = null;
    }

    public class BillingAccountDto
    {
        [JsonProperty("billingAccountId")]
        public Guid BillingAccountId { get; set; }

        [JsonProperty("BillingAccountTypeId")]
        public int BillingAccountTypeId { get; set; }

        [JsonProperty("BillingAccountTypeName")]
        public string BillingAccountTypeName { get; set; }

        [JsonProperty("reservationCode")]
        public string ReservationCode { get; set; }

        [JsonProperty("holderName")]
        public string GuestName { get; set; }

        [JsonProperty("guestEmail")]
        public string GuestEmail { get; set; }

        [JsonProperty("guestTypeName")]
        public string GuestTypeName { get; set; }

        [JsonProperty("roomNumber")]
        public string RoomNumber { get; set; }

        [JsonProperty("arrivalDate")]
        public DateTime AarrivalDate { get; set; }

        [JsonProperty("departureDate")]
        public DateTime DepartureDate { get; set; }

        [JsonProperty("mealPlanTypeId")]
        public string MealPlanType { get; set; }
    }
}
