﻿using Newtonsoft.Json;
using System;

namespace THEx.IntegrationTOTVSPDV.DTO.PDV
{
    public class PropertyResponseDto
    {
        [JsonProperty("PropertyId")]
        public string Id { get; set; }

        [JsonProperty("TenantId")]
        public Guid TenantId { get; set; }

        [JsonProperty("BrandId")]
        public int BrandId { get; set; }

        [JsonProperty("Name")]
        public string Name { get; set; }

        [JsonProperty("DocumentInformation")]
        public string DocumentInformation { get; set; }

        [JsonProperty("IM")]
        public string MunicipalRegistration { get; set; }

        [JsonProperty("IE")]
        public string StateRegistration { get; set; }

        [JsonProperty("TradeName")]
        public string TradeName { get; set; }

        [JsonProperty("StreetName")]
        public string StreetName { get; set; }

        [JsonProperty("StreetNumber")]
        public string StreetNumber { get; set; }

        [JsonProperty("Neighborhood")]
        public string Neighborhood { get; set; }

        [JsonProperty("CityName")]
        public string City { get; set; }

        [JsonProperty("StateName")]
        public string State { get; set; }

        [JsonProperty("StateCode")]
        public string StateCode { get; set; }

        [JsonProperty("PostalCode")]
        public string PostalCode { get; set; }

        [JsonProperty("CountryName")]
        public string Country { get; set; }
    }
}