﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace THEx.IntegrationTOTVSPDV.DTO.Response
{
    public class PaymentResponseDto
    {
        [JsonProperty("PaymentId")]
        public int Id { get; set; }

        [JsonProperty("Description")]
        public string Description { get; set; }
    }
}
