﻿namespace THEx.IntegrationTOTVSPDV.VO
{
    public class FilterParamVo
    {
        public string PropertyId { get; set; }
        public int Type { get; set; }
        public string Name { get; set; }
        public string GivenName { get; set; }
        public string RoomNumber { get; set; }
        public string Number { get; set; }
        public string LanguageIsoCode { get; set; }        
    }
}