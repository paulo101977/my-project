﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Azure.WebJobs.Host;
using Newtonsoft.Json;
using THEx.IntegrationTOTVSPDV.DTO.PMS;

namespace THEx.IntegrationTOTVSPDV.DAO
{
    public class ReasonDao : GenericDao
    {
        /// <summary>
        /// Find all reasons by property
        /// </summary>
        public List<ReasonDto> FindReasonByPropertyId(TraceWriter log, string propertyId)
        {
            try
            {
                var query = QueryReasonByPropertyId(propertyId);

                return CreateConnection<List<ReasonDto>>(query);
            }
            catch (Exception ex)
            {
                log.Info(ex.Message);
            }

            return new List<ReasonDto>();
        }

        public ReasonDto findReasonById(int propertyId, int reasonId)
        {

            string sQuery = QueryReasonById(propertyId, reasonId);
            string sReader = CreateConnection(sQuery);
            dynamic jsonProperty = JsonConvert.DeserializeObject(sReader);

            if (jsonProperty == null || jsonProperty.list.Count == 0)
            {
                return null;
            }

            ReasonDto reason = new ReasonDto
            {
                Id = jsonProperty.list[0].reasonId,
                Description = jsonProperty.list[0].reasonName
            };

            return reason;
        }

        private static string QueryReasonByPropertyId(string propertyId)
        {
            StringBuilder sbSql = new StringBuilder();
            sbSql.Append("select										 ");
            sbSql.Append("r.reasonId,                                    ");
            sbSql.Append("r.reasonName,                                  ");
            sbSql.Append("rc.ReasonType                                  ");
            sbSql.Append("from reason r                                  ");
            sbSql.Append("inner join reasoncategory rc                   ");
            sbSql.Append("on r.reasoncategoryid = rc.reasoncategoryid    ");
            sbSql.Append("inner join chain c on c.chainid = r.chainid    ");
            sbSql.Append("inner join brand b on b.chainid = c.chainid    ");
            sbSql.Append("inner join property p on p.brandid = b.brandid ");
            sbSql.Append("and p.propertyid = {0}                         ");

            return string.Format(sbSql.ToString(), propertyId);
        }

        private static string QueryReasonById(int propertyId, int reasonId)
        {
            StringBuilder sbSql = new StringBuilder();
            sbSql.Append("select										 ");
            sbSql.Append("r.reasonId,                                    ");
            sbSql.Append("r.reasonName                                   ");
            sbSql.Append("from reason r                                  ");
            sbSql.Append("inner join reasoncategory rc                   ");
            sbSql.Append("on r.reasoncategoryid = rc.reasoncategoryid    ");
            sbSql.Append("inner join chain c on c.chainid = r.chainid    ");
            sbSql.Append("inner join brand b on b.chainid = c.chainid    ");
            sbSql.Append("inner join property p on p.brandid = b.brandid ");
            sbSql.Append("and p.propertyid = {0}                         ");
            sbSql.Append("and r.reasonId = {1}                           ");

            return string.Format(sbSql.ToString(), propertyId.ToString(), reasonId.ToString());
        }
    }
}