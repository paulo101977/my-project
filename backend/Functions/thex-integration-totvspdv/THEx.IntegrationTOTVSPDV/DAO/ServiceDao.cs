﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Azure.WebJobs.Host;
using Newtonsoft.Json;
using THEx.IntegrationTOTVSPDV.DTO.PMS;

namespace THEx.IntegrationTOTVSPDV.DAO
{
    public class ServiceDao : GenericDao
    {
        /// <summary>
        /// Find all service by property
        /// </summary>
        public List<ServiceDto> FindServiceByPropertyId(TraceWriter log, string propertyId)
        {
            try
            {
                var query = QueryItemByPropertyId(propertyId);

                return CreateConnection<List<ServiceDto>>(query);
            }
            catch (Exception ex)
            {
                log.Info(ex.Message);
            }

            return new List<ServiceDto>();
        }

        private static string QueryItemByPropertyId(string propertyId)
        {
            StringBuilder sbSql = new StringBuilder();

            sbSql.Append("SELECT                                              ");
            sbSql.Append("BI.BILLINGITEMNAME,                                 ");
            sbSql.Append("BI.BILLINGITEMID                                    ");
            sbSql.Append("FROM BILLINGITEM BI                                 ");
            sbSql.Append("JOIN BILLINGITEMTYPE BITY                           ");
            sbSql.Append("ON BI.BILLINGITEMTYPEID = BITY.BILLINGITEMTYPEID    ");
            sbSql.Append("WHERE BI.PROPERTYID = 1                             ");
            sbSql.Append("AND BI.BILLINGITEMTYPEID = 4                        ");

            return string.Format(sbSql.ToString(), propertyId);
        }
    }
}