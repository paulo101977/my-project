﻿using Microsoft.Azure.WebJobs.Host;
using Newtonsoft.Json;
using Polly;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Thex.Common.Enumerations;
using Thex.Common.Helpers;
using THEx.IntegrationTOTVSPDV.DTO.PDV;
using THEx.IntegrationTOTVSPDV.DTO.PMS;
using THEx.IntegrationTOTVSPDV.VO;

namespace THEx.IntegrationTOTVSPDV.DAO
{
    public class GuestDao : GenericDao
    {
        public string UrlBaseSA
        {
            get
            {
                switch (EnumHelper.GetEnumValue<EnvironmentEnum>(Environment.GetEnvironmentVariable("CloudEnvironment", EnvironmentVariableTarget.Process), true))
                {
                    case EnvironmentEnum.Production: return "https://thex-api-sa.totvs.com.br/api/";
                    case EnvironmentEnum.BugFix: return "https://thex-api-sa-fabric-bugfix.azurewebsites.net/api/";
                    case EnvironmentEnum.Test: return "https://thex-api-sa-fabric-tst.azurewebsites.net/api/";
                    case EnvironmentEnum.UAT: return "https://thex-api-sa-fabric-uat.azurewebsites.net/api/";
                    case EnvironmentEnum.Staging: return "https://thex-api-sa-fabric-stg.azurewebsites.net/api/";
                    case EnvironmentEnum.Development:
                    default: return "https://thex-api-sa-fabric.azurewebsites.net/api/";
                }
            }
        }

        public string UrlBasePMS
        {
            get
            {
                switch (EnumHelper.GetEnumValue<EnvironmentEnum>(Environment.GetEnvironmentVariable("CloudEnvironment", EnvironmentVariableTarget.Process), true))
                {
                    case EnvironmentEnum.Production: return "https://thex-api-pms.totvs.com.br/api/";
                    case EnvironmentEnum.BugFix: return "https://thex-api-pms-fabric-bugfix.azurewebsites.net/api/";
                    case EnvironmentEnum.Test: return "https://thex-api-pms-fabric-tst.azurewebsites.net/api/";
                    case EnvironmentEnum.UAT: return "https://thex-api-pms-fabric-uat.azurewebsites.net/api/";
                    case EnvironmentEnum.Staging: return "https://thex-api-pms-fabric-stg.azurewebsites.net/api/";
                    case EnvironmentEnum.Development:
                    default: return "https://thex-api-pms-fabric.azurewebsites.net/api/";
                }
            }
        }

        public string _tokenClient = "04e3d3c1-193a-47ab-a69e-b129550f4550";
        public string _tokenApplication = "c636fca5-0597-478e-80f4-7244623456f6";
        public string _grantTypeAuth = "password";

        public string _urlLogin = "";
        public string _urlPropertyLogin = "";

        private async Task<string> GetTokenByEnvironment(string urlBase, string propertyId)
        {
            var client = new HttpClient();

            try
            {
                var jsonUser = JsonConvert.SerializeObject(new
                {
                    tokenClient = _tokenClient,
                    tokenApplication = _tokenApplication,
                    propertyId
                }, Formatting.Indented);

                var response = await Policy
                               .HandleResult<HttpResponseMessage>(message => !message.IsSuccessStatusCode)
                               .WaitAndRetryAsync(1, i => TimeSpan.FromSeconds(5), (result, timeSpan, retryCount, context) =>
                               {
                                   Console.Write($"Request failed with {result.Result.StatusCode}. Waiting {timeSpan} before next retry. Retry attempt {retryCount}");
                               })
                               .ExecuteAsync(() => client.PostAsync(string.Concat(urlBase, @"IntegrationPartner/authenticate"), new StringContent(jsonUser, Encoding.UTF8, "application/json")));

                if (response.IsSuccessStatusCode)
                {
                    var ret = JsonConvert.DeserializeObject<IntegrationPartnerTokenDto>(await response.Content.ReadAsStringAsync());

                    return ret.NewPasswordToken;
                }
                else
                    throw new Exception(await response.Content.ReadAsStringAsync());
            }
            catch (Exception ex)
            {
                Console.Write($"Error {ex.Message}");
                throw;
            }
        }

        public List<CustomerDto> FindGuestByFilter(TraceWriter log, FilterParamVo filterParamVo)
        {
            List<CustomerDto> customers = new List<CustomerDto>();
            try
            {
                var billingAccountList = GetAccounts(filterParamVo).Result;

                foreach (var acount in billingAccountList)
                {
                    customers.Add(new CustomerDto()
                    {
                        FirstName = acount.HolderName,
                        LastName = acount.HolderName,
                        RoomNumber = acount.RoomNumber,
                        ReservationItemStatusId = acount.ReservationStatusId,
                        Number = acount.BillingAccountId,
                        BillingAccountStatusId = acount.StatusId,
                        BillingAccountTypeId = acount.AccountTypeId
                    });
                }
            }
            catch (Exception e)
            {
                log.Info("Error");
            }

            return customers;
        }

        private string GetUrlAccounts(string propertyId)
        {
            return string.Concat(UrlBasePMS, $"BillingAccount/{propertyId}/search?openedAccounts=true&culture=pt-BR");
        }

        private async Task<List<BillingAccountPMSDto>> GetAccounts(FilterParamVo filters)
        {
            var client = new HttpClient();
            client.DefaultRequestHeaders.Authorization =
                        new AuthenticationHeaderValue("Bearer", await GetTokenByEnvironment(UrlBaseSA, filters.PropertyId));


            var response = await Policy
                                  .HandleResult<HttpResponseMessage>(message => !message.IsSuccessStatusCode)
                                  .WaitAndRetryAsync(1, i => TimeSpan.FromSeconds(30), (result, timeSpan, retryCount, context) =>
                                  {
                                      Console.Write($"Request failed with {result.Result.StatusCode}. Waiting {timeSpan} before next retry. Retry attempt {retryCount}");
                                  })
                                  .ExecuteAsync(() =>
                                  {
                                      return client.GetAsync(GetUrlAccounts(filters.PropertyId));
                                  });

            if (response.IsSuccessStatusCode)
            {
                var ret = JsonConvert.DeserializeObject<BillingAccountPMSResponseDto>(await response.Content.ReadAsStringAsync());
                var billingAccountItems = ret.Items;

                if (!string.IsNullOrEmpty(filters.GivenName))
                    billingAccountItems = billingAccountItems.FindAll(x => x.HolderName.Contains(filters.GivenName));

                if (!string.IsNullOrEmpty(filters.Name))
                    billingAccountItems = billingAccountItems.FindAll(x => x.HolderName.Contains(filters.GivenName)).ToList();

                if (!string.IsNullOrEmpty(filters.Number))
                    billingAccountItems = billingAccountItems.FindAll(x => x.BillingAccountId.Contains(filters.Number)).ToList();

                if (!string.IsNullOrEmpty(filters.RoomNumber))
                    billingAccountItems = billingAccountItems.FindAll(x => x.RoomNumber != null && x.RoomNumber.Contains(filters.RoomNumber)).ToList();

                return billingAccountItems;
            }
            else
                throw new Exception(await response.Content.ReadAsStringAsync());
        }

        private static string QueryGuestByFilter(FilterParamVo filterParamVo)
        {
            StringBuilder sbSql = new StringBuilder();
            sbSql.Append(@"SELECT ' ' AS Prefixo, COALESCE(GR.FirstName, COALESCE(BA.BillingAccountName, 'Geral')) 
            as FirstName, GR.LastName, ADDRH.StreetName, ADDRH.CityName, ADDRH.PostalCode, ADDRH.CountryName, 
            ADDRH.CountryTwoLetterIsoCode, ADDRH.LanguageIsoCode, RS.ContactEmail AS Email, RS.InternalComments, 
            RS.ExternalComments, room.RoomNumber, GI.GuestStatusId, RI.ReservationItemStatusId, ba.BillingAccountId,  
            ba.StatusId as 'BillingAccountStatusId',bt.TypeName as AccountTypeName, ba.BillingAccountTypeId
            FROM Property PY 
            INNER JOIN BillingAccount BA ON PY.PropertyId = BA.PropertyId 
            INNER JOIN BillingAccountType BT ON BA.BillingAccountTypeId = bt.BillingAccountTypeId 
            LEFT JOIN ReservationItem RI ON BA.ReservationItemId = RI.ReservationItemId  AND(RI.ReservationItemStatusId = 2 OR RI.ReservationItemStatusId = 4) 
            LEFT JOIN Reservation RS ON RI.ReservationId = RS.ReservationId 
            LEFT JOIn GuestReservationItem GI ON GI.ReservationItemId = RI.ReservationItemId AND BA.GuestReservationItemId = GI.GuestReservationItemId
            LEFT JOIN GuestRegistration GR ON GR.GuestRegistrationId = GI.GuestRegistrationId 
            LEFT JOIN Room ON Room.RoomId = ri.RoomId 
            LEFT JOIN CompanyClient CC ON BA.CompanyClientId = cc.CompanyClientId 
            LEFT JOIN VWADDRESS ADDRH ON 
	            ((ADDRH.OwnerId = GI.GuestRegistrationId  AND ADDRH.LocationCategoryId = 4 ) OR (ADDRH.OwnerId = CC.Personid)) AND (ADDRH.LanguageIsoCode = 'pt-BR' )
            LEFT JOIN ContactInformation CI ON COALESCE(GI.GuestRegistrationId, CC.CompanyClientId) = CI.OwnerId AND CI.ContactInformationTypeId = 1 
            WHERE PY.PropertyId = {0} AND BA.StatusId = 1
            ");

            string sQuery = string.Format(sbSql.ToString(), filterParamVo.PropertyId);

            sbSql = new StringBuilder();
            sbSql.Append(sQuery);

            if (!string.IsNullOrWhiteSpace(filterParamVo.Number))
            {
                string queryAux = "AND ba.BillingAccountId = '{0}' ";
                sbSql.Append(string.Format(queryAux, filterParamVo.Number));
            }

            if (!string.IsNullOrWhiteSpace(filterParamVo.RoomNumber))
            {
                string queryAux = "AND room.RoomNumber = '{0}' ";
                sbSql.Append(string.Format(queryAux, filterParamVo.RoomNumber));
            }

            if (!string.IsNullOrWhiteSpace(filterParamVo.GivenName))
            {
                string queryAux = "AND GR.LastName like '%{0}%' ";
                sbSql.Append(string.Format(queryAux, filterParamVo.GivenName));
            }

            if (!string.IsNullOrWhiteSpace(filterParamVo.Name))
            {
                string queryAux = "AND GR.FirstName like '%{0}%' ";
                sbSql.Append(string.Format(queryAux, filterParamVo.Name));
            }

            if (!string.IsNullOrWhiteSpace(filterParamVo.Type.ToString()))
                sbSql.Append(filterParamVo.Type.Equals("0")
                    ? "AND bt.BillingAccountTypeId = 2 "
                    : "AND bt.BillingAccountTypeId in (1, 3, 4) ");
            if (!string.IsNullOrEmpty(filterParamVo.LanguageIsoCode))
                sbSql.Append(filterParamVo.LanguageIsoCode.Equals("pt-BR"));

            return sbSql.ToString();
        }

        private string GetUrlGuests(string propertyId)
        {
            return string.Concat(UrlBasePMS, $"reservations/{propertyId}/search?reservationItemStatus=2&guestReservationItemStatusId=2&culture=pt-BR");
        }

        private async Task<BillingAccountResponseDto> GetGuests(FilterParamVo filters)
        {
            var client = new HttpClient();
            client.DefaultRequestHeaders.Authorization =
                        new AuthenticationHeaderValue("Bearer", await GetTokenByEnvironment(UrlBaseSA, filters.PropertyId));

            var response = await Policy
                                  .HandleResult<HttpResponseMessage>(message => !message.IsSuccessStatusCode)
                                  .WaitAndRetryAsync(1, i => TimeSpan.FromSeconds(30), (result, timeSpan, retryCount, context) =>
                                  {
                                      Console.Write($"Request failed with {result.Result.StatusCode}. Waiting {timeSpan} before next retry. Retry attempt {retryCount}");
                                  })
                                  .ExecuteAsync(() =>
                                  {
                                      return client.GetAsync(GetUrlGuests(filters.PropertyId));
                                  });

            if (response.IsSuccessStatusCode)
            {
                var ret = JsonConvert.DeserializeObject<BillingAccountResponseDto>(await response.Content.ReadAsStringAsync());
                var billingAccountItems = ret.Items;

                if (filters.Type != 0)
                    billingAccountItems = billingAccountItems.FindAll(x => x.BillingAccountTypeId == filters.Type);

                if (!string.IsNullOrEmpty(filters.GivenName))
                    billingAccountItems = billingAccountItems.FindAll(x => x.GuestName.Contains(filters.GivenName));

                if (!string.IsNullOrEmpty(filters.Name))
                    billingAccountItems = billingAccountItems.FindAll(x => x.GuestName.Contains(filters.Name)).ToList();

                if (!string.IsNullOrEmpty(filters.Number))
                    billingAccountItems = billingAccountItems.FindAll(x => x.BillingAccountId.ToString().Contains(filters.Number)).ToList();

                if (!string.IsNullOrEmpty(filters.RoomNumber))
                    billingAccountItems = billingAccountItems.FindAll(x => x.RoomNumber != null && x.RoomNumber == filters.RoomNumber).ToList();

                ret.Items = billingAccountItems;
                return ret;
            }
            else
                throw new Exception(await response.Content.ReadAsStringAsync());
        }

        public BillingAccountResponseDto FindGuest(TraceWriter log, FilterParamVo filterParamVo)
        {
            try
            {
                return GetGuests(filterParamVo).Result;
            }
            catch (Exception ex)
            {
                log.Info(ex.Message);
            }

            return BillingAccountResponseDto.NullInstance;
        }
    }
}