﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Azure.WebJobs.Host;
using Newtonsoft.Json;
using Polly;
using Thex.Common.Enumerations;
using Thex.Common.Helpers;
using THEx.IntegrationTOTVSPDV.DTO.PMS;
using THEx.IntegrationTOTVSPDV.DTO.Response;

namespace THEx.IntegrationTOTVSPDV.DAO
{
    public class PaymentDao : GenericDao
    {
        /// <summary>
        /// Find all payments by property
        /// </summary>
        public List<PaymentResponseDto> FindPaymentByPropertyId(TraceWriter log, string propertyId)
        {
            try
            {
                var query = QueryPaymentByPropertyId(propertyId);

                return CreateConnection<List<PaymentResponseDto>>(query);
            }
            catch (Exception ex)
            {
                log.Info(ex.Message);
            }

            return new List<PaymentResponseDto>();
        }

        private static string QueryPaymentByPropertyId(string propertyId)
        {
            StringBuilder sbSql = new StringBuilder();

            sbSql.Append("SELECT                                              ");
            sbSql.Append("BI.BILLINGITEMNAME AS DESCRIPTION,                  ");
            sbSql.Append("BI.BILLINGITEMID AS PAYMENTID                       ");
            sbSql.Append("FROM BILLINGITEM BI                                 ");
            sbSql.Append("JOIN BILLINGITEMTYPE BITY                           ");
            sbSql.Append("ON BI.BILLINGITEMTYPEID = BITY.BILLINGITEMTYPEID    ");
            sbSql.Append("WHERE BI.PROPERTYID = {0}                           ");
            sbSql.Append("AND BI.BILLINGITEMTYPEID = 3                        ");

            return string.Format(sbSql.ToString(), propertyId);
        }
    }
}