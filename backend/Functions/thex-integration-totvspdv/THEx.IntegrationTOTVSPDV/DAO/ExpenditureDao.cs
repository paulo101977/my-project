﻿using Microsoft.Azure.WebJobs.Host;
using Newtonsoft.Json;
using Polly;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Thex.Common.Enumerations;
using Thex.Common.Helpers;
using THEx.IntegrationTOTVSPDV.DTO.Notification;
using THEx.IntegrationTOTVSPDV.DTO.PDV;
using THEx.IntegrationTOTVSPDV.DTO.PMS;
using THEx.IntegrationTOTVSPDV.DTO.Response;
using THEx.IntegrationTOTVSPDV.VO;

namespace THEx.IntegrationTOTVSPDV.DAO
{
    public class ExpenditureDao : GenericDao
    {
        public string UrlBaseSA
        {
            get
            {
                switch (EnumHelper.GetEnumValue<EnvironmentEnum>(Environment.GetEnvironmentVariable("CloudEnvironment", EnvironmentVariableTarget.Process), true))
                {
                    case EnvironmentEnum.Production: return "https://thex-api-sa.totvs.com.br/api/";
                    case EnvironmentEnum.BugFix: return "https://thex-api-sa-fabric-bugfix.azurewebsites.net/api/";
                    case EnvironmentEnum.Test: return "https://thex-api-sa-fabric-tst.azurewebsites.net/api/";
                    case EnvironmentEnum.UAT: return "https://thex-api-sa-fabric-uat.azurewebsites.net/api/";
                    case EnvironmentEnum.Staging: return "https://thex-api-sa-fabric-stg.azurewebsites.net/api/";
                    case EnvironmentEnum.Development:
                    default: return "https://thex-api-sa-fabric.azurewebsites.net/api/";
                }
            }
        }
        public string UrlBasePMS
        {
            get
            {
                switch (EnumHelper.GetEnumValue<EnvironmentEnum>(Environment.GetEnvironmentVariable("CloudEnvironment", EnvironmentVariableTarget.Process), true))
                {
                    case EnvironmentEnum.Production: return "https://thex-api-pms.totvs.com.br/api/";
                    case EnvironmentEnum.BugFix: return "https://thex-api-pms-fabric-bugfix.azurewebsites.net/api/";
                    case EnvironmentEnum.Test: return "https://thex-api-pms-fabric-tst.azurewebsites.net/api/";
                    case EnvironmentEnum.UAT: return "https://thex-api-pms-fabric-uat.azurewebsites.net/api/";
                    case EnvironmentEnum.Staging: return "https://thex-api-pms-fabric-stg.azurewebsites.net/api/";
                    case EnvironmentEnum.Development:
                    default: return "https://thex-api-pms-fabric.azurewebsites.net/api/";
                }
            }
        }

        public string _tokenClient = "04e3d3c1-193a-47ab-a69e-b129550f4550";
        public string _tokenApplication = "c636fca5-0597-478e-80f4-7244623456f6";
        public string _grantTypeAuth = "password";
        public string _urlLogin = "";
        public string _urlPropertyLogin = "";

        private async Task<string> GetTokenByEnvironment(string urlBase, string propertyId)
        {
            var client = new HttpClient();

            try
            {
                var jsonUser = JsonConvert.SerializeObject(new
                {
                    tokenClient = _tokenClient,
                    tokenApplication = _tokenApplication,
                    propertyId
                }, Formatting.Indented);

                var response = await Policy
                               .HandleResult<HttpResponseMessage>(message => !message.IsSuccessStatusCode)
                               .WaitAndRetryAsync(1, i => TimeSpan.FromSeconds(5), (result, timeSpan, retryCount, context) =>
                               {
                                   Console.Write($"Request failed with {result.Result.StatusCode}. Waiting {timeSpan} before next retry. Retry attempt {retryCount}");
                               })
                               .ExecuteAsync(() => client.PostAsync(string.Concat(urlBase, @"IntegrationPartner/authenticate"), new StringContent(jsonUser, Encoding.UTF8, "application/json")));

                if (response.IsSuccessStatusCode)
                {
                    var ret = JsonConvert.DeserializeObject<IntegrationPartnerTokenDto>(await response.Content.ReadAsStringAsync());

                    return ret.NewPasswordToken;
                }
                else
                    throw new Exception(await response.Content.ReadAsStringAsync());
            }
            catch (Exception ex)
            {
                Console.Write($"Error {ex.Message}");
                throw;
            }
        }

        private string GetUrlPostDebit(string propertyId)
        {
            return string.Concat(UrlBasePMS, $"BillingAccountItemIntegration/v2/expenditure");
        }

        private string GetUrlInvoicesByAccount(string billingAccountId)
        {
            return string.Concat(UrlBasePMS, $"BillingAccount/{billingAccountId}/invoices");
        }


        private async Task<BillingAccountItemReponseDto> ProcessSale(PostRequestDto postRequest, MessageDto messageList)
        {
            var jsonUser = JsonConvert.SerializeObject(postRequest, Formatting.Indented);
            var client = new HttpClient();
            client.DefaultRequestHeaders.Authorization =
                        new AuthenticationHeaderValue("Bearer", await GetTokenByEnvironment(UrlBaseSA, postRequest.PropertyId.ToString()));

            var response = await Policy
                                  .HandleResult<HttpResponseMessage>(message => !message.IsSuccessStatusCode)
                                  .WaitAndRetryAsync(1, i => TimeSpan.FromSeconds(5), (result, timeSpan, retryCount, context) =>
                                  {
                                      Console.Write($"Request failed with {result.Result.StatusCode}. Waiting {timeSpan} before next retry. Retry attempt {retryCount}");
                                  })
                                  .ExecuteAsync(() =>
                                  {
                                      return client.PostAsync(GetUrlPostDebit(postRequest.PropertyId.ToString()),
                                          new StringContent(jsonUser, Encoding.UTF8, "application/json"));
                                  });

            if (response.IsSuccessStatusCode)
            {
                var ret = await response.Content.ReadAsStringAsync();
            }
            else
            {
                var notification = JsonConvert.DeserializeObject<NotificationDto>(await response.Content.ReadAsStringAsync());
                messageList.Errors.Add(new ErrorDto()
                {
                    Code = "-99",
                    Message = notification.Message + " | " + notification.Details.FirstOrDefault().DetailedMessage
                });
            }

            return await Task.FromResult(BillingAccountItemReponseDto.NullInstance);
        }

        private async Task<List<BillingInvoiceResponseDto>> GetInvoiceByAccount(string billingInvoiceAccountId, int propertyId, MessageDto messageList)
        {
            var client = new HttpClient();
            client.DefaultRequestHeaders.Authorization =
                        new AuthenticationHeaderValue("Bearer", await GetTokenByEnvironment(UrlBaseSA, propertyId.ToString()));

            var response = await Policy
                                  .HandleResult<HttpResponseMessage>(message => !message.IsSuccessStatusCode)
                                  .WaitAndRetryAsync(1, i => TimeSpan.FromSeconds(5), (result, timeSpan, retryCount, context) =>
                                  {
                                      Console.Write($"Request failed with {result.Result.StatusCode}. Waiting {timeSpan} before next retry. Retry attempt {retryCount}");
                                  })
                                  .ExecuteAsync(() =>
                                  {
                                      return client.GetAsync(GetUrlInvoicesByAccount(billingInvoiceAccountId));
                                  });

            if (response.IsSuccessStatusCode)
            {
                var tst = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<List<BillingInvoiceResponseDto>>(await response.Content.ReadAsStringAsync());
            }
            else
            {
                var notification = JsonConvert.DeserializeObject<NotificationDto>(await response.Content.ReadAsStringAsync());
                messageList.Errors.Add(new ErrorDto()
                {
                    Code = "-98",
                    Message = notification.Message + " | " + notification.Details.FirstOrDefault().DetailedMessage
                });
            }

            return await Task.FromResult(new List<BillingInvoiceResponseDto>());
        }


        public BillingAccountItemReponseDto ProcessSale(TraceWriter log, PostRequestDto postRequest, MessageDto messageList)
        {
            try
            {
                postRequest.Amount = postRequest.SaleDetails.Sum(s => s.Amount);

                return ProcessSale(postRequest, messageList).Result;
            }
            catch (Exception ex)
            {
                log.Info(ex.Message);
            }

            return BillingAccountItemReponseDto.NullInstance;
        }

        public BillingAccountItemReponseDto DeleteSale(TraceWriter log, PostRequestDto postRequest, MessageDto messageList)
        {
            try
            {
                var invoice = GetInvoiceByAccount(postRequest.BillingAccountId, postRequest.PropertyId, messageList).Result;

                var request = new BillingAccountItem
                {
                    PropertyId = postRequest.PropertyId,
                    BillingAccountId = postRequest.BillingAccountId,
                    BillingItemId = postRequest.BillingItemId,
                    DetailList = postRequest.SaleDetails,
                    TenantId = postRequest.TenantId,
                    Amount = 0
                };

                foreach (var sale in postRequest.SaleDetails)
                    request.Amount += sale.Amount;

                return ProcessSale(postRequest, messageList).Result;
            }
            catch (Exception ex)
            {
                log.Info(ex.Message);
            }

            return BillingAccountItemReponseDto.NullInstance;
        }
    }
}