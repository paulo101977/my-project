﻿using System;
using System.Text;
using Microsoft.Azure.WebJobs.Host;
using Newtonsoft.Json;
using THEx.IntegrationTOTVSPDV.DTO.PDV;

namespace THEx.IntegrationTOTVSPDV.DAO
{
    public class CompanyDao : GenericDao
    {
        public CompanyDto FindCompanyByid(TraceWriter log, int id)
        {
            CompanyDto company = new CompanyDto();

            try
            {
                string sQuery = QueryCompanyParamId(id);

                string sReader = CreateConnection(sQuery);
                dynamic jsonProperty = JsonConvert.DeserializeObject(sReader);

                if (jsonProperty.list.Count == 0) return null;

                company.Id = jsonProperty.list[0].ID;
                company.Name = jsonProperty.list[0].NOME;
                company.Cnpj = jsonProperty.list[0].CNPJ;
                company.CorparateName = jsonProperty.list[0].RAZAOSOCIAL;
            }
            catch (Exception ex)
            {
                log.Info("Error");
            }

            return company;
        }

        private static string QueryCompanyParamId(int id)
        {
            StringBuilder sbSql = new StringBuilder();
            sbSql.Append("SELECT CY.CompanyId AS ID, CY.ShortName AS NOME, ");
            sbSql.Append("DC.DocumentInformation AS CNPJ, CY.TradeName AS RAZAOSOCIAL ");
            sbSql.Append("FROM Company CY, Document DC ");
            sbSql.Append("WHERE CY.CompanyUid = DC.OwnerId ");
            sbSql.Append("AND DC.DocumentTypeId = 2 ");
            sbSql.Append("AND CY.CompanyId = {0} ");

            return string.Format(sbSql.ToString(), id);
        }
    }
}