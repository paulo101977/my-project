﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using Newtonsoft.Json;

namespace THEx.IntegrationTOTVSPDV.DAO
{
    public class GenericDao
    {
        #region deprecated

        public string CreateConnection(string sQuery)
        {
            SqlCommand cmd = new SqlCommand();
            SqlConnection sqlConnection1 = null;
            try
            {
                var cnnString =
                    Environment.GetEnvironmentVariable("connectionDataBase", EnvironmentVariableTarget.Process);

                sqlConnection1 = new SqlConnection(cnnString ?? throw new InvalidOperationException());

                cmd.CommandText = string.Format(sQuery);
                cmd.CommandType = CommandType.Text;
                cmd.Connection = sqlConnection1;

                sqlConnection1.Open();

                var reader = cmd.ExecuteReader();


                string sReader = ToJson(reader);

                sqlConnection1.Close();

                return sReader;
            }
            catch (Exception e)
            {
                sqlConnection1?.Close();
                throw e;
            }
        }

        #endregion

        public T CreateConnection<T>(string sQuery)
        {
            var command = new SqlCommand();
            var cnnString =
                    Environment.GetEnvironmentVariable("connectionDataBase", EnvironmentVariableTarget.Process);

            var connection = new SqlConnection(cnnString ?? throw new InvalidOperationException());

            try
            {
                command.CommandText = string.Format(sQuery);
                command.CommandType = CommandType.Text;
                command.Connection = connection;

                connection.Open();
                var reader = command.ExecuteReader();
                var json = ToJson(reader);
                connection.Close();

                return JsonConvert.DeserializeObject<T>(json);
            }
            catch (Exception ex)
            {
                connection?.Close();
                throw ex;
            }
        }

        public string ToJson(SqlDataReader rdr)
        {
            StringBuilder sb = new StringBuilder();
            StringWriter sw = new StringWriter(sb);

            using (JsonWriter jsonWriter = new JsonTextWriter(sw))
            {
                jsonWriter.WriteStartArray();

                while (rdr.Read())
                {
                    jsonWriter.WriteStartObject();

                    int fields = rdr.FieldCount;

                    for (int i = 0; i < fields; i++)
                    {
                        jsonWriter.WritePropertyName(rdr.GetName(i));
                        jsonWriter.WriteValue(rdr[i]);
                    }

                    jsonWriter.WriteEndObject();
                }

                jsonWriter.WriteEndArray();

                return sw.ToString();
            }
        }
    }
}