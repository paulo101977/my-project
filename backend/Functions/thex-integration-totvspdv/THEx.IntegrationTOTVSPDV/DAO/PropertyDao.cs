﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Azure.WebJobs.Host;
using Newtonsoft.Json;
using THEx.IntegrationTOTVSPDV.DTO.PDV;

namespace THEx.IntegrationTOTVSPDV.DAO
{
    public class PropertyDao : GenericDao
    {
        #region Deprecated

        public List<CompanyDto> FindAllHotelByDocument(TraceWriter log, string propertyDoc)
        {
            List<CompanyDto> companyList = new List<CompanyDto>();

            try
            {
                string sQuery = QueryPropertyParamDocument(propertyDoc);

                string sReader = CreateConnection(sQuery);
                var propertyList = JsonConvert.DeserializeObject<PropertyResulDto>(sReader);

                if (propertyList.PropertyResultDetailsList.Count == 0) return null;

                foreach (var prop in propertyList.PropertyResultDetailsList)
                {
                    var address = new AddressDto();
                    address.StreetName = (string)prop.logradouro;
                    address.Neighborhood = prop.bairro;
                    address.PostalCode = prop.CEP;
                    address.StreetNumber = prop.numero;
                    address.City = prop.cidade;
                    address.State = prop.sgEstado;
                    address.ZipCode = prop.sgPais;
                    address.Country = prop.pais;

                    var property = new PropertyDto();
                    property.Address = address;

                    property.Id = prop.id.ToString();
                    property.Name = prop.nome;
                    property.CorparateName = prop.razaosocial;
                    property.Cnpj = prop.CNPJ;
                    property.StateRegistration = prop.IE;
                    property.MunicipalRegistration = prop.IM;
                    property.TenantId = prop.tenantId;

                    var company = new CompanyDto();
                    company.Id = prop.companyId.ToString();

                    int.TryParse(company.Id, out var companyId);
                    CompanyDao companyDao = new CompanyDao();

                    company = companyDao.FindCompanyByid(log, companyId);
                    company.Property = property;

                    companyList.Add(company);
                }
            }
            catch (Exception e)
            {
                log.Info("Error");
            }

            return companyList;
        }

        public CompanyDto FindHotelByDocument(TraceWriter log, string propertyDoc)
        {
            CompanyDto company = new CompanyDto();
            PropertyDto property = new PropertyDto();
            AddressDto address = new AddressDto();

            try
            {
                string sQuery = QueryPropertyParamDocument(propertyDoc);

                string sReader = CreateConnection(sQuery);
                dynamic jsonProperty = JsonConvert.DeserializeObject(sReader);


                if (jsonProperty.list.Count == 0) return null;

                address.StreetName = jsonProperty.list[0].logradouro;
                address.Neighborhood = jsonProperty.list[0].bairro;
                address.PostalCode = jsonProperty.list[0].CEP;
                address.StreetNumber = jsonProperty.list[0].numero;
                address.City = jsonProperty.list[0].cidade;
                address.State = jsonProperty.list[0].sgEstado;
                address.ZipCode = jsonProperty.list[0].sgPais;
                address.Country = jsonProperty.list[0].pais;

                property.Address = address;

                property.Id = jsonProperty.list[0].id.ToString();
                property.Name = jsonProperty.list[0].nome;
                property.CorparateName = jsonProperty.list[0].razaoSocial;
                property.Cnpj = jsonProperty.list[0].CNPJ;
                property.StateRegistration = jsonProperty.list[0].IE;
                property.MunicipalRegistration = jsonProperty.list[0].IM;
                property.TenantId = jsonProperty.list[0].tenantId;

                company.Id = jsonProperty.list[0].companyId.ToString();

                int.TryParse(company.Id, out var companyId);
                CompanyDao companyDao = new CompanyDao();

                company = companyDao.FindCompanyByid(log, companyId);
                company.Property = property;
            }
            catch (Exception e)
            {
                log.Info("Error");
            }

            return company;
        }

        public CompanyDto FindHotelByDocument(TraceWriter log, string propertyDoc, string propertyId)
        {
            CompanyDto company = new CompanyDto();
            PropertyDto property = new PropertyDto();
            AddressDto address = new AddressDto();

            try
            {
                string sQuery = QueryPropertyParamDocument(propertyDoc, propertyId);

                string sReader = CreateConnection(sQuery);
                dynamic jsonProperty = JsonConvert.DeserializeObject(sReader);


                if (jsonProperty.list.Count == 0) return null;

                address.StreetName = jsonProperty.list[0].logradouro;
                address.Neighborhood = jsonProperty.list[0].bairro;
                address.PostalCode = jsonProperty.list[0].CEP;
                address.StreetNumber = jsonProperty.list[0].numero;
                address.City = jsonProperty.list[0].cidade;
                address.State = jsonProperty.list[0].sgEstado;
                address.ZipCode = jsonProperty.list[0].sgPais;
                address.Country = jsonProperty.list[0].pais;

                property.Address = address;

                property.Id = jsonProperty.list[0].id.ToString();
                property.Name = jsonProperty.list[0].nome;
                property.CorparateName = jsonProperty.list[0].razaoSocial;
                property.Cnpj = jsonProperty.list[0].CNPJ;
                property.StateRegistration = jsonProperty.list[0].IE;
                property.MunicipalRegistration = jsonProperty.list[0].IM;
                property.TenantId = jsonProperty.list[0].tenantId;

                company.Id = jsonProperty.list[0].companyId.ToString();

                int.TryParse(company.Id, out var companyId);
                CompanyDao companyDao = new CompanyDao();

                company = companyDao.FindCompanyByid(log, companyId);
                company.Property = property;
            }
            catch (Exception e)
            {
                log.Info("Error");
            }

            return company;
        }

        private static string QueryPropertyParamDocument(string propertyDoc)
        {
            StringBuilder sbSql = new StringBuilder();
            sbSql.Append("SELECT py.PropertyId as id,					 ");
            sbSql.Append("py.CompanyId as companyId, 					 ");
            sbSql.Append("py.Name as nome, 							     ");
            sbSql.Append("py.TenantId as tenantId, 					     ");
            sbSql.Append("cy.TradeName as razaosocial, 				     ");
            sbSql.Append("cn.DocumentInformation as CNPJ, 			     ");
            sbSql.Append("im.DocumentInformation as IM, 				 ");
            sbSql.Append("ie.DocumentInformation as IE, 				 ");
            sbSql.Append("lo.StreetName as logradouro, 				     ");
            sbSql.Append("lo.Neighborhood as bairro, 					 ");
            sbSql.Append("lo.PostalCode as CEP, 						 ");
            sbSql.Append("lo.StreetNumber as numero, 					 ");
            sbSql.Append("lo.cityname as cidade, 						 ");
            sbSql.Append("lo.statename as estado, 					     ");
            sbSql.Append("lo.ExternalCode as ibge, 					     ");
            sbSql.Append("lo.CountryTwoLetterIsoCode as sgPais, 		 ");
            sbSql.Append("lo.CountryName as pais, 					     ");
            sbSql.Append("lo.isocodelevel2 as sgEstado 				     ");
            sbSql.Append("FROM Property PY 							     ");
            sbSql.Append("inner join Company cy 						 ");
            sbSql.Append("ON py.CompanyId = cy.CompanyId 				 ");
            sbSql.Append("inner join Person PE  						 ");
            sbSql.Append("ON cy.PersonId = PE.PersonId  				 ");
            sbSql.Append("LEFT JOIN 									 ");
            sbSql.Append("Document IM 								     ");
            sbSql.Append("ON cy.CompanyuId = 						         ");
            sbSql.Append("	IM.OwnerId AND IM.DocumentTypeId = 14 	     ");
            sbSql.Append("LEFT join Document IE 						 ");
            sbSql.Append("ON cy.CompanyuId = 						         ");
            sbSql.Append("	IE.OwnerId AND IE.DocumentTypeId = 13 	     ");
            sbSql.Append("LEFT JOIN 									 ");
            sbSql.Append("Document CN 								     ");
            sbSql.Append("ON cy.CompanyuId = 						         ");
            sbSql.Append("	CN.OwnerId AND CN.DocumentTypeId = 2 	     ");
            sbSql.Append("LEFT JOIN VWADDRESS lo 						 ");
            sbSql.Append("ON PY.PropertyUId =	                         ");
            sbSql.Append("	lo.OwnerId AND lo.locationcategoryid = 1     ");
            sbSql.Append("AND lo.LanguageIsoCode = 'pt-BR'               ");
            sbSql.Append("WHERE cn.DocumentInformation = '{0}'           ");

            return string.Format(sbSql.ToString(), propertyDoc);
        }

        private static string QueryPropertyParamDocument(string propertyDoc, string propertyId)
        {
            StringBuilder sbSql = new StringBuilder();
            sbSql.Append("SELECT py.PropertyId as id,					 ");
            sbSql.Append("py.CompanyId as companyId, 					 ");
            sbSql.Append("py.Name as nome, 							     ");
            sbSql.Append("py.TenantId as tenantId, 					     ");
            sbSql.Append("cy.TradeName as razaosocial, 				     ");
            sbSql.Append("cn.DocumentInformation as CNPJ, 			     ");
            sbSql.Append("im.DocumentInformation as IM, 				 ");
            sbSql.Append("ie.DocumentInformation as IE, 				 ");
            sbSql.Append("lo.StreetName as logradouro, 				     ");
            sbSql.Append("lo.Neighborhood as bairro, 					 ");
            sbSql.Append("lo.PostalCode as CEP, 						 ");
            sbSql.Append("lo.StreetNumber as numero, 					 ");
            sbSql.Append("lo.cityname as cidade, 						 ");
            sbSql.Append("lo.statename as estado, 					     ");
            sbSql.Append("lo.ExternalCode as ibge, 					     ");
            sbSql.Append("lo.CountryTwoLetterIsoCode as sgPais, 		 ");
            sbSql.Append("lo.CountryName as pais, 					     ");
            sbSql.Append("lo.isocodelevel2 as sgEstado 				     ");
            sbSql.Append("FROM Property PY 							     ");
            sbSql.Append("inner join Company cy 						 ");
            sbSql.Append("ON py.CompanyId = cy.CompanyId 				 ");
            sbSql.Append("inner join Person PE  						 ");
            sbSql.Append("ON cy.PersonId = PE.PersonId  				 ");
            sbSql.Append("LEFT JOIN 									 ");
            sbSql.Append("Document IM 								     ");
            sbSql.Append("ON cy.CompanyuId = 						         ");
            sbSql.Append("	IM.OwnerId AND IM.DocumentTypeId = 14 	     ");
            sbSql.Append("LEFT join Document IE 						 ");
            sbSql.Append("ON cy.CompanyuId = 						         ");
            sbSql.Append("	IE.OwnerId AND IE.DocumentTypeId = 13 	     ");
            sbSql.Append("LEFT JOIN 									 ");
            sbSql.Append("Document CN 								     ");
            sbSql.Append("ON cy.CompanyuId = 						         ");
            sbSql.Append("	CN.OwnerId AND CN.DocumentTypeId = 2 	     ");
            sbSql.Append("LEFT JOIN VWADDRESS lo 						 ");
            sbSql.Append("ON PY.PropertyUId =	                         ");
            sbSql.Append("	lo.OwnerId AND lo.locationcategoryid = 1     ");
            sbSql.Append("AND lo.LanguageIsoCode = 'pt-BR'               ");
            sbSql.Append("WHERE cn.DocumentInformation = '{0}'           ");
            sbSql.Append("and py.propertyid = {1}                      ");

            return string.Format(sbSql.ToString(), propertyDoc, propertyId);
        }

        #endregion

        public List<PropertyResponseDto> FindProperty(TraceWriter log, string propertyDoc)
        {
            try
            {
                string sQuery = PropertyQueryBase(propertyDoc);
                return CreateConnection<List<PropertyResponseDto>>(sQuery);
            }
            catch (Exception ex)
            {
                log.Info(ex.Message);
            }

            return new List<PropertyResponseDto>();
        }

        private static string PropertyQueryBase(string propertyDoc)
        {
            StringBuilder sbSql = new StringBuilder();
            sbSql.Append("SELECT PROPERTY.PROPERTYID,	                                                            ");
            sbSql.Append("       PROPERTY.TENANTID, 		                                                        ");
            sbSql.Append("       PROPERTY.BRANDID, 	                                                                ");
            sbSql.Append("       PROPERTY.NAME,                                                                     ");
            sbSql.Append("       CN.DOCUMENTINFORMATION,                                                            ");
            sbSql.Append("       IM.DOCUMENTINFORMATION AS IM, 				                                        ");
            sbSql.Append("       IE.DOCUMENTINFORMATION AS IE,                                                      ");
            sbSql.Append("       COMPANY.TRADENAME,                                                                 ");
            sbSql.Append("       LO.STREETNAME,                                                                     ");
            sbSql.Append("       LO.STREETNUMBER,                                                                   ");
            sbSql.Append("       LO.NEIGHBORHOOD,                                                                   ");
            sbSql.Append("       LO.CITYNAME,                                                                       ");
            sbSql.Append("       LO.STATENAME,                                                                      ");
            sbSql.Append("       LO.ISOCODELEVEL2 AS STATECODE,                                                     ");
            sbSql.Append("       LO.POSTALCODE,					                                                    ");
            sbSql.Append("       LO.COUNTRYNAME                                                                     ");
            sbSql.Append("FROM PROPERTY PROPERTY                                                                    ");
            sbSql.Append("INNER JOIN COMPANY ON PROPERTY.COMPANYID = COMPANY.COMPANYID                              ");
            sbSql.Append("INNER JOIN PERSON PE ON COMPANY.PERSONID = PE.PERSONID                                    ");
            sbSql.Append("LEFT JOIN DOCUMENT IM ON COMPANY.COMPANYUID = IM.OWNERID AND IM.DOCUMENTTYPEID = 14       ");
            sbSql.Append("LEFT JOIN DOCUMENT IE ON COMPANY.COMPANYUID = IE.OWNERID AND IE.DOCUMENTTYPEID = 13       ");
            sbSql.Append("LEFT JOIN DOCUMENT CN ON COMPANY.COMPANYUID = CN.OWNERID AND CN.DOCUMENTTYPEID = 2        ");
            sbSql.Append("LEFT JOIN VWADDRESS LO ON PROPERTY.PROPERTYUID = LO.OWNERID AND LO.LOCATIONCATEGORYID = 1 ");
            sbSql.Append("AND LO.LANGUAGEISOCODE = 'PT-BR'                                                          ");
            sbSql.Append("WHERE CN.DOCUMENTINFORMATION = '{0}'                                                      ");

            return string.Format(sbSql.ToString(), propertyDoc);
        }
    }
}