﻿using Microsoft.WindowsAzure.Storage.Table;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace THEx.IntegrationTOTVSPDV.Entity
{
    class IdentifierMappingEntity : TableEntity
    {
        public string ThexId { get; set; }
        public string PosId { get; set; }
        public int PropertyId { get; set; }

        public IdentifierMappingEntity() { }

        public IdentifierMappingEntity(int propertyId, string posId, string thexId)
        {
            this.PartitionKey = Convert.ToString(propertyId);
            this.RowKey = posId;

            this.ThexId = thexId;
            this.PosId = posId;
            this.PropertyId = propertyId;
        }
    }
}
