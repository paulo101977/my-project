﻿using Newtonsoft.Json;
using THEx.IntegrationTOTVSPDV.DTO.PMS;

namespace THEx.IntegrationTOTVSPDV.Commom
{
    public class Utils
    {
        public static ErrorDto CreateMessageError(string errorMessage, string auxiliaryMessage)
        {
            ErrorDto errorDto = JsonConvert.DeserializeObject<ErrorDto>(errorMessage);
            if (auxiliaryMessage != null) errorDto.Message = string.Format(errorDto.Message, auxiliaryMessage);

            return errorDto;
        }
    }
}