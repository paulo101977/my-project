﻿using Microsoft.Azure.WebJobs.Host;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Net;
using System.Threading.Tasks;
using THEx.IntegrationTOTVSPDV.DTO.PMS;

namespace THEx.IntegrationTOTVSPDV.Commom
{
    public class RequestHttp
    {
        public static ApiResultDto SendPost(string url, string messageBody, string tenantId, TraceWriter log)
        {
            var client = new RestClient(url);
            var request = new RestRequest(Method.POST);
            var apiResultDto = new ApiResultDto();

            string token = GetThexToken(tenantId, log);

            log.Info("RequestHttp == SendPost == Token esta vazio == " + (token == string.Empty).ToString());

            request.AddHeader("Authorization", token);
            request.RequestFormat = DataFormat.Json;
            request.AddParameter("application/json; charset=utf-8", messageBody, ParameterType.RequestBody);

            log.Info("RequestHttp == SendPost == Antes de executar");

            IRestResponse response = client.Execute(request);

            log.Info("RequestHttp == SendPost == Depois de executar");

            HttpStatusCode statusCode = response.StatusCode;

            apiResultDto.HttpStatusCode = (int)statusCode;
            apiResultDto.Message = response.StatusDescription + (!response.Content.IsNullOrEmpty() ? (" - " + response.Content.IsNullOrEmpty()) : string.Empty);
            apiResultDto.Method = "POST";
            apiResultDto.Url = url;
            apiResultDto.Body = response.Content;

            return apiResultDto;

            //var client = new RestClient(url);
            //var request = new RestRequest(Method.POST) { RequestFormat = DataFormat.Json };

            //request.AddParameter("application/json; charset=utf-8", messageBody, ParameterType.RequestBody);
            //var taskCompletionSource = new TaskCompletionSource<string>();
            //client.ExecuteAsync(request, response => taskCompletionSource.SetResult(response.Content));
            //var result = taskCompletionSource.Task.Result;

            //return result;
        }

        private static string GetThexToken(string tenantId, TraceWriter log)
        {
            log.Info("GetThexToken == " + ApiThEx.UrlGetThexToken());

            var client = new RestClient(ApiThEx.UrlGetThexToken());

            var request = new RestRequest(Method.POST);

            log.Info("GetThexToken == TokenApplication == " + Environment.GetEnvironmentVariable("tokenApplicationThex", EnvironmentVariableTarget.Process));
            log.Info("GetThexToken == TokenClient == " + Environment.GetEnvironmentVariable("tokenClientThex", EnvironmentVariableTarget.Process));

            var tokenInfo = new TokenInfoDto
            {
                TenantId = tenantId,
                TokenApplication = Environment.GetEnvironmentVariable("tokenApplicationThex", EnvironmentVariableTarget.Process),
                TokenClient = Environment.GetEnvironmentVariable("tokenClientThex", EnvironmentVariableTarget.Process),
            };

            var body = JsonConvert.SerializeObject(tokenInfo);

            log.Info("GetThexToken == body == " + body);

            request.AddParameter("application/json; charset=utf-8", body, ParameterType.RequestBody);

            IRestResponse response = client.Execute(request);

            log.Info("GetThexToken == client.Execute()");
            log.Info("GetThexToken == response.IsSuccessful == " + response.IsSuccessful);

            if (response.IsSuccessful)
            {
                UserTokenDto userTokenDto = JsonConvert.DeserializeObject<UserTokenDto>(response.Content);
                var token = "Bearer " + userTokenDto.NewPasswordToken;
                return token;
            }
            else
                return string.Empty;
        }

        public static string SendGet(string url)
        {
            var client = new RestClient(url);
            var request = new RestRequest(Method.GET) { RequestFormat = DataFormat.Json };

            var taskCompletionSource = new TaskCompletionSource<string>();
            client.ExecuteAsync(request, response => taskCompletionSource.SetResult(response.Content));
            var result = taskCompletionSource.Task.Result;

            return result;
        }
    }
}