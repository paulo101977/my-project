﻿using System;

namespace THEx.IntegrationTOTVSPDV.Commom
{
    public class ApiThEx
    {
        private static string UrlServer()
        {
            return Environment.GetEnvironmentVariable("apiTHEx", EnvironmentVariableTarget.Process);
        }

        private static string UrlServerSuperAdmin()
        {
            return Environment.GetEnvironmentVariable("apiTHExSuperAdmin", EnvironmentVariableTarget.Process);
        }

        public static string UrlGetThexToken()
        {
            return UrlServerSuperAdmin() + ApiTHExValue.ThexToken;
        }

        public static string UrlPostSalePos()
        {
            string url = UrlServer() + ApiTHExValue.PostBillingAccountItemDebit;
            return url;
        }

        public static string UrlDeleteSalePos()
        {
            string url = UrlServer() + ApiTHExValue.DeleteBillingAccountItemDebit;
            return url;
        }
    }
}