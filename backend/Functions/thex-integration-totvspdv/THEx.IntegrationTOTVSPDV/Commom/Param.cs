﻿namespace THEx.IntegrationTOTVSPDV.Commom
{
    public static class Param
    {
        public const string GivenName = "givenName";
        public const string Name = "name";
        public const string Number = "number";
        public const string PropertyId = "propertyId";
        public const string PropertyDoc = "propertyDoc";
        public const string RoomNumber = "roomNumber";
        public const string Type = "type";
    }
}