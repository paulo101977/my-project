
declare @propertyId int
Begin


	SET  @propertyId = 23;

	DECLARE @total int, @budgetday date, @roomtypeid int;  
	DECLARE @RoomType_Cursor as CURSOR;
	
	DECLARE @block_startdate date, @block_enddate date, @block_roomtypeid int;  
	DECLARE @BlockedRoomType_Cursor as CURSOR;

	BEGIN TRANSACTION;

	 BEGIN TRY

	    update roomtypeinventory set blockedQuantity = 0, balance = total where propertyid = @propertyId;

		SET @RoomType_Cursor = CURSOR FOR
			
			select count(*) as total, rb.budgetday as budgetday, receivedroomtypeid as roomtypeid
			from reservationitem ri 
			inner join reservationbudget rb on ri.reservationitemid = rb.reservationitemid 
			inner join roomtype rt on rt.roomtypeid = ri.receivedroomtypeid
			where rb.isdeleted = 0 and ri.reservationitemstatusid < 3 and rt.propertyid = @propertyId 
			group by rb.budgetday, receivedroomtypeid
			order by budgetday

		OPEN @RoomType_Cursor; 


		FETCH NEXT FROM @RoomType_Cursor INTO @total, @budgetday, @roomtypeid
		WHILE @@FETCH_STATUS = 0  
		   BEGIN  
			  
			  update roomtypeinventory
			  set balance = total - @total 
			  where cast(([date]) As Date) = @budgetday and roomtypeid = @roomtypeid

			  FETCH NEXT FROM @RoomType_Cursor INTO @total, @budgetday, @roomtypeid
		   END
		CLOSE @RoomType_Cursor;  
		DEALLOCATE @RoomType_Cursor; 


		
		SET @BlockedRoomType_Cursor = CURSOR FOR
			
			select cast((bloc.blockingStartDate) As Date) as block_startdate
			, cast((bloc.blockingEndDate) As Date) as block_enddate
			, bloc_rt.roomtypeid  as block_roomtypeid
			from roomblocking bloc 
			inner join room ro on ro.roomid = bloc.roomid 
			inner join roomtype bloc_rt on ro.roomtypeid = bloc_rt.roomtypeid
			where bloc_rt.propertyid = @propertyId  and bloc.isdeleted = 0

		OPEN @BlockedRoomType_Cursor; 

		FETCH NEXT FROM @BlockedRoomType_Cursor INTO @block_startdate, @block_enddate, @block_roomtypeid
		WHILE @@FETCH_STATUS = 0  
		   BEGIN  
		      
			  update roomtypeinventory
			  set balance = balance - 1, blockedquantity = blockedquantity + 1  
			  where cast(([date]) As Date) between @block_startdate and @block_enddate
			  and roomtypeid = @block_roomtypeid
			  
			  FETCH NEXT FROM @BlockedRoomType_Cursor INTO @block_startdate, @block_enddate, @block_roomtypeid
		   END
		CLOSE @BlockedRoomType_Cursor;  
		DEALLOCATE @BlockedRoomType_Cursor; 

		--ROLLBACK TRANSACTION
		COMMIT TRANSACTION

	END TRY
    BEGIN CATCH
        IF @@TRANCOUNT > 0
        BEGIN
            ROLLBACK TRANSACTION
        END
    END CATCH
END