using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace SqlProjChecker
{
    class Program
    {
        private static string[] IgnoredFolders => new string[] { "Debug" };
        private static string[] IgnoredFiles => new string[] { "Script.PostDeployment.sql" };

        static void Main(string[] args)
        {
            string path = args[0];

            var sqlFileNameList = GetSqlFileNames(path);

            var result = new List<string>();

            CheckSqlProj(path, sqlFileNameList, result);

            Console.WriteLine(result.Count == 0);

            result.ForEach(i => Console.Write("{0}\t", i));

            Console.ReadKey();
        }

        private static void CheckSqlProj(string path, List<string> sqlFileNameList, List<string> result)
        {
            var sqlProjeFile = GetFiles(path, GetSqlProjSearchPattern()).FirstOrDefault();
            var content = File.ReadAllText(sqlProjeFile.FullName).ToLower();

            foreach (var file in sqlFileNameList)
            {
                if (!content.Contains(file.ToLower()))
                    result.Add(file);
            }
        }

        private static List<string> GetSqlFileNames(string path)
        {
            var sqlFileNameList = new List<string>();
            foreach (var file in GetFiles(path, GetSqlSearchPattern()))
            {
                if (IgnoredFolders.Contains(file.Directory.Name) || IgnoredFiles.Contains(file.Name) || file.Extension.ToLower().Contains("sqlproj"))
                    continue;

                sqlFileNameList.Add($"{file.Directory.Name}\\{file.Name}");
            }

            return sqlFileNameList;
        }

        private static string GetSqlSearchPattern()
            => $"*.sql";

        private static string GetSqlProjSearchPattern()
            => $"*.sqlproj";

        private static ICollection<FileInfo> GetFiles(string path, string searchPattern)
        {
            if (!Directory.Exists(path))
                throw new DirectoryNotFoundException("Invalid directory");

            var objDirectoryInfo = new DirectoryInfo(path);

            return objDirectoryInfo.GetFiles(searchPattern, SearchOption.AllDirectories);
        }
    }
}
