
declare 
@Tenanteuuid uniqueidentifier,
@HotelId bigint,
@Hoteluuid uniqueidentifier,
@CompanyId bigint,
@ChainId bigint,
@BrandId bigint,
@HotelName nvarchar(100),
@TenantName nvarchar(100),
@Usuariouuid uniqueidentifier,
@PersonCompany uniqueidentifier,
@Companyuuid uniqueidentifier,
@BrandName nvarchar(100),
@ChainName nvarchar(100),
@Email nvarchar(200)

 begin
 set @TenantName = '55/RIO Hotel'
 set @HotelName = '55/RIO Hotel'
 set @Tenanteuuid = newid()
 set @PersonCompany = newid()
 set @Companyuuid = newid()
 set @Hoteluuid = newid()
 set @ChainName = '55/RIO Rede'
 set @BrandName = '55/RIO Bandeira'
 set @Email = 'saulo.brito@totvscmnet.com.br'
 select @Usuariouuid=id from [Users] where username = @Email

 insert into tenant (tenantId, TenantName, IsActive)
values
(@Tenanteuuid,@TenantName,1);

update [Users] set TenantId=@Tenanteuuid, IsAdmin = 1,
[PasswordHash] = 'AQAAAAEAACcQAAAAELGyI3o2locEHeC17U+Uy8Mhx6MDDvjfnO0eKjbUstIRvhHE60dHN30XgivoLkMvMw==', 
[SecurityStamp] = 'AWVWE4U2J3IKEFOWR5OT4VRF2NAGTV2D', 
[ConcurrencyStamp] = 'cdf7177e-b9a2-4c19-a4fc-b6e829c09b42' where Id=@Usuariouuid;

insert into Person(PersonId,PersonType, FirstName, LastName,CountrySubdivisionId)
values (@PersonCompany,'L', 'Hotel Portugal', 'Hotel', 1);

insert into company (companyUid, PersonId, ShortName,TradeName)
values (@Companyuuid, @PersonCompany, '55/RIO Hotel', 'Maluhia Participações Ltda.');

select @CompanyId = CompanyId from company where companyuid=@companyuuid;

insert into Chain(Name, TenantId)
values (@ChainName, @Tenanteuuid);

select @ChainId = ChainId  from Chain where Name=@ChainName;

insert into Brand(chainId,Name, IsTemporary,tenantid)
values(@ChainId, @BrandName, 0, @Tenanteuuid);

select @BrandId = BrandId  from Brand where Name=@BrandName;

insert into Property(CompanyId,PropertyTypeId, BrandId, Name, PropertyUId,TenantId)
values(@CompanyId, 1,@BrandId,@HotelName, @Hoteluuid,@Tenanteuuid);

select @HotelId=PropertyId from property where PropertyUId=@Hoteluuid;

--Insert Parametros
insert into PropertyParameter(PropertyParameterId, PropertyId,ApplicationParameterId,
PropertyParameterValue, PropertyParameterMinValue, PropertyParameterMaxValue,
PropertyParameterPossibleValues, IsActive,TenantId)
select newid(), py.PropertyId,ApplicationParameterId,
ap.ParameterDefaultValue, ParameterMinValue, ParameterMaxValue,
ParameterPossibleValues, 1,py.TenantId
from ApplicationParameter ap, Property py
where  not exists(select 1 from PropertyParameter
where propertyid=py.PropertyId and ApplicationParameterId=ap.ApplicationParameterId);

--Insert Pensões
insert into PropertyMealPlanType(PropertyMealPlanTypeId, PropertyMealPlanTypeCode, PropertyMealPlanTypeName,
PropertyId, MealPlanTypeId,TenantId,IsActive)
select newid(), mealplantypecode, mealplantypename, py.propertyid, mp.mealplantypeid, py.tenantid,
1
 from MealPlanType mp, Property py
 where not exists(select 1 from PropertyMealPlanType pm
 where pm.mealplantypeid=mp.MealPlanTypeId
 and py.PropertyId=pm.PropertyId);
 
 
--Insert PropertyContract
insert into propertycontract
(propertycontractid, propertyid, maxuh, isdeleted, creatoruserid)
values(newid(), @HotelId, 100, 0, @Usuariouuid)

 --Insert Status Governança
 insert into HousekeepingStatusProperty(HousekeepingStatusPropertyId,PropertyId, HousekeepingStatusId, StatusName,Color,
TenantId, IsActive)
select NEWID(), py.propertyId, hk.HousekeepingStatusId, hk.StatusName,hk.DefaultColor,py.tenantid,1
from Property py,
HousekeepingStatus hk
where not exists(select 1 from HousekeepingStatusProperty h1
where h1.PropertyId=py.PropertyId
and h1.HousekeepingStatusId=hk.HousekeepingStatusId);


 insert into plasticBrandProperty
 (PlasticBrandPropertyId, PlasticBrandId, PropertyId, InstallmentsQuantity, IsActive, TenantId)
 select newid(), PlasticBrandId, PropertyId, 0, 1, py.TenantId
  from PlasticBrand pb, property py
  where not exists(select 1 from plasticBrandProperty p1
 where p1.PlasticBrandId=pb.PlasticBrandId
 and py.PropertyId=p1.PropertyId);

 UPDATE Tenant SET PropertyId=@HotelId, ChainId=@ChainId, BrandId=@BrandId where TenantId=@Tenanteuuid;
  UPDATE [UserInvitation] SET PropertyId=@HotelId, ChainId=@ChainId, BrandId=@BrandId, TenantId=@Tenanteuuid where  email = @Email;
 	 
	insert into document 
	(ownerid, documenttypeid, documentinformation) 
	values 
	--2 - cnpj
	(@Companyuuid, 2, '13190617000169'),
	--13 - inscrição estadual
	(@Companyuuid, 13, '87.200.922'),
	--14 - inscrição municipal
	(@Companyuuid, 14, '0658320-2');

	
	insert into Person(PersonId,PersonType, FirstName, LastName,CountrySubdivisionId)
	values (@Hoteluuid,'L', 'Hotel Portugual', 'Hotel', 1);
	
	insert into ContactInformation 
	(ownerid, contactinformationtypeid, information)
	values 
	--type 2 = telefone
	(@Hoteluuid, 2, '(21) 3798-5699'),
	--type 3 = homepage
	(@Hoteluuid, 3, 'https://www.hotelportugal.com.br');

 end;