Begin
	DECLARE @roomtypeid INT;
	DECLARE @propertyid INT;
	DECLARE @tenantid UNIQUEIDENTIFIER;
	DECLARE @totalRooms INT;
	
	Declare @roomType_Cursor as CURSOR;

		SET @roomType_Cursor = CURSOR FOR SELECT r.roomtypeid, p.propertyid, p.tenantid, (select count(*) from room ro where ro.roomtypeid = r.roomtypeid) as totalRooms 
											FROM roomtype r 
											inner join property p on r.propertyid = p.propertyid
		OPEN @roomType_Cursor; 

		FETCH NEXT FROM @roomType_Cursor INTO @roomtypeid, @propertyid, @tenantid, @totalRooms;  
		WHILE @@FETCH_STATUS = 0  
		   BEGIN  

				Declare @initialDate DateTime = convert(datetime,'2018-05-01 00:00:00', 111);
				Declare @id uniqueidentifier;
				
				DECLARE @totalRoombBlocking INT;
				DECLARE @totalReservedRooms INT;

				WHILE(@initialDate <= convert(datetime,'2020-06-18 00:00:00', 111))
				BEGIN

					select @totalRoombBlocking = count(*) from RoomBlocking rb
					inner join room r on r.roomid = rb.roomid
					inner join roomtype rt on rt.roomtypeid = r.roomtypeid
					where 
					 CONVERT(date, rb.blockingstartdate) <= CONVERT(date, @initialDate)
					and CONVERT(date, rb.blockingenddate) >= CONVERT(date, @initialDate) 
					and rt.roomtypeid = @roomtypeid;

					
					select @totalReservedRooms = count(*) from reservationitem ri
					where 
					 (
					 (CONVERT(date, coalesce(ri.CheckInDate, ri.EstimatedArrivalDate)) <= CONVERT(date, @initialDate)
					and CONVERT(date, coalesce(ri.CheckOutDate, ri.EstimatedDepartureDate)) > CONVERT(date, @initialDate) )
					or 
					 (CONVERT(date, coalesce(ri.CheckInDate, ri.EstimatedArrivalDate)) = CONVERT(date, @initialDate)
					and CONVERT(date, coalesce(ri.CheckOutDate, ri.EstimatedDepartureDate)) = CONVERT(date, @initialDate) )
					)
					and ri.reservationitemstatusid <= 2
					and ri.receivedroomtypeid = @roomtypeid;


					SET @id = newid();

					INSERT INTO RoomTypeInventory 
					([RoomTypeInventoryId], [PropertyId], [RoomTypeId], [Total], [Balance], [BlockedQuantity], [Date], [TenantId])
					values (@id, @propertyid, @roomtypeid, @totalRooms, @totalRooms - coalesce(@totalRoombBlocking,0) - coalesce(@totalReservedRooms,0), @totalRoombBlocking, @initialDate, @tenantid)

					SET @initialDate = DATEADD(day, 1, @initialDate);



				END
					
			  FETCH NEXT FROM @roomType_Cursor INTO @roomtypeid, @propertyid, @tenantid, @totalRooms;  
		   END;  
		CLOSE @roomType_Cursor;  
		DEALLOCATE @roomType_Cursor; 
End

