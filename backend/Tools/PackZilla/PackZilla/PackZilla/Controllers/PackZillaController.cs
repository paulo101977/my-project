﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System.Linq;
using System.IO;
using IO = System.IO;
using System.Text.RegularExpressions;
using Buildalyzer;
using System.Threading.Tasks;
using System;

namespace PackZilla.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PackZillaController : ControllerBase
    {
        private readonly IConfiguration _configuration;

        public string _thexDirectory = "";
        public List<string> _thexPackages => _configuration.GetSection("ThexPackages").Get<string[]>().ToList();
        public List<ThexProject> _thexProjects => _configuration.GetSection("ThexProjects").Get<ThexProject[]>().ToList();
        public ThexProject _currentProject = null;
        public static List<ResultDto> _cachedResultList = null;

        public PackZillaController(IConfiguration configuration)
        {
            _configuration = configuration;
            _cachedResultList = _cachedResultList ?? new List<ResultDto>();

            _thexDirectory = _configuration.GetValue<string>(Microsoft.AspNetCore.Hosting.WebHostDefaults.ContentRootKey);
            _thexDirectory = _thexDirectory.Replace(String.Format("{0}Tools{0}PackZilla{0}PackZilla{0}PackZilla", Path.DirectorySeparatorChar), "");
        }

        [HttpGet]
        public ActionResult<IEnumerable<ResultDto>> Get()
            => _cachedResultList;

        [HttpPatch("id/{id}")]
        public ActionResult<ResultDto> Patch(Guid id)
        {
            var resultDto = _cachedResultList.FirstOrDefault(e => e.Id == id);

            SetCurrentProject(resultDto.Project);

            resultDto.Projects = new List<ResultProjectDto>();

            CheckSolutionBuild(resultDto.Projects);

            return resultDto;
        }

        [HttpPost]
        public ActionResult<ResultDto> Post([FromBody] PackZillaDto packzillaDto)
        {
            try
            {
                if (packzillaDto == null || string.IsNullOrEmpty(packzillaDto.Project) || string.IsNullOrEmpty(packzillaDto.Version))
                    throw new Exception("Invalid DTO");

                SetCurrentProject(packzillaDto.Project);

                return ProcessFiles(packzillaDto.Version);
            }
            catch (Exception ex)
            {
                return new ResultDto
                {
                    Success = false,
                    Message = ex.Message
                };
            }
        }

        private void SetCurrentProject(string projectName)
        {
            _currentProject = _thexProjects.FirstOrDefault(e => e.ProjectName.ToLower() == projectName.ToLower());
        }

        private void CheckSolutionBuild(List<ResultProjectDto> resultProject)
        {
            var manager = new AnalyzerManager($"{_thexDirectory}\\{_currentProject.ProjectSln}");
            var projects = manager.Projects.Select(e => e.Value).ToList();

            Parallel.ForEach(
                projects,
                new ParallelOptions { MaxDegreeOfParallelism = 4 },
                project => { CheckBuild(project, resultProject); }
            );
        }

        private void CheckBuild(ProjectAnalyzer analyzer, List<ResultProjectDto> resultProject)
        {

        }

        private ResultDto ProcessFiles(string version)
        {
            var files = GetFiles();

            var resultDto = new ResultDto
            {
                Id = Guid.NewGuid(),
                Project = _currentProject.ProjectName,
                ExecutionDate = DateTime.Now,
                Projects = new List<ResultProjectDto>(),
                Success = true
            };

            Parallel.ForEach(
                files,
                new ParallelOptions { MaxDegreeOfParallelism = 4 },
                file => { ReplacePackagesInFile(file, version); }
            );

            CheckSolutionBuild(resultDto.Projects);

            _cachedResultList.Add(resultDto);

            return resultDto;
        }

        private void ReplacePackagesInFile(FileInfo file, string version)
        {
            var content = IO.File.ReadAllText(file.FullName);

            if (!_thexPackages.Any(e => content.Contains($@"Include=""{e}""")))
                return;

            foreach (var packageName in _thexPackages)
            {
                var pattern = $@"<PackageReference Include=""{packageName}"" Version=""(.*)"" />";
                var replace = $@"<PackageReference Include=""{packageName}"" Version=""{version}"" />";

                content = Regex.Replace(content, pattern, replace);
            }

            IO.File.WriteAllText(file.FullName, content);
        }

        private ICollection<FileInfo> GetFiles()
        {
            var path = Path.Combine(_thexDirectory, _currentProject.ProjectFolder);

            if (!Directory.Exists(path))
                throw new DirectoryNotFoundException("Invalid directory");

            var objDirectoryInfo = new DirectoryInfo(path);

            return objDirectoryInfo.GetFiles(GetSearchPattern(), SearchOption.AllDirectories);
        }

        private string GetSearchPattern()
            => "*.csproj";
    }

    public class ThexProject
    {
        public string ProjectName { get; set; }
        public string ProjectFolder { get; set; }
        public string ProjectSln { get; set; }
    }

    public class PackZillaDto
    {
        public string Version { get; set; }
        public string Project { get; set; }
    }

    public class ResultDto
    {
        public Guid Id { get; set; }
        public string Project { get; set; }
        public DateTime ExecutionDate { get; set; }
        public List<ResultProjectDto> Projects { get; set; }

        public bool Success { get; set; }
        public string Message { get; set; }

        public string ExecutionDateFormatted
            => ExecutionDate.ToString("dd/MM/yyyy HH:mm");
    }

    public class ResultProjectDto
    {
        public string ProjectPath { get; set; }
        public bool BuildOK { get; set; }
    }
}
