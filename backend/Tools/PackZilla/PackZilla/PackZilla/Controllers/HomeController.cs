﻿using Microsoft.AspNetCore.Mvc;

namespace PackZilla.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
    }
}
