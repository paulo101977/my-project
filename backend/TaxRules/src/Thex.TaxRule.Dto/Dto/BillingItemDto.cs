﻿namespace Thex.TaxRule.Dto
{
    public class BillingItemDto
    {
        public int Id { get; set; }
        public string BillingItemName { get; set; }
    }
}
