﻿using System;
using Tnf.Dto;

namespace Thex.TaxRule.Dto
{
    public class IvaDto : BaseDto
    {
        public Guid Id { get; set; }
        public string Description { get; set; }
        public string Location { get; set; }
        public decimal? Tax { get; set; }
    }
}
