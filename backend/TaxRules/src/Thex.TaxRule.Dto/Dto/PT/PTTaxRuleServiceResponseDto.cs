﻿using System;

namespace Thex.TaxRule.Dto
{
    public class PTTaxRuleServiceResponseDto
    {
        public int ServiceId { get; set; }
        public string ServiceName { get; set; }
        public Guid IvaId { get; set; }
        public string ExemptionCode { get; set; }
    }
}
