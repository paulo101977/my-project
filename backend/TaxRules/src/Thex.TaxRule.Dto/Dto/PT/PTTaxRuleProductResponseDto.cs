﻿using System;

namespace Thex.TaxRule.Dto
{
    public class PTTaxRuleProductResponseDto
    {
        public Guid ProductId { get; set; }
        public string ProductName { get; set; }
        public Guid IvaId { get; set; }
    }
}
