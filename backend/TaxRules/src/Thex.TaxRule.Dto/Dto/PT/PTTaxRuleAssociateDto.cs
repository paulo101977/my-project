﻿using System;
using System.Collections.Generic;

namespace Thex.TaxRule.Dto
{
    public class PTTaxRuleAssociateDto
    {
        public Guid IvaId { get; set; }
        public List<PTTaxRuleAssociateServiceDto> ServiceList { get; set; }
        public List<PTTaxRuleAssociateProductDto> ProductList { get; set; }

        public PTTaxRuleAssociateDto()
        {
            ServiceList = new List<PTTaxRuleAssociateServiceDto>();
            ProductList = new List<PTTaxRuleAssociateProductDto>();
        }
    }

    public class PTTaxRuleAssociateServiceDto
    {
        public int ServiceId { get; set; }
        public string ExemptionCode { get; set; }
    }

    public class PTTaxRuleAssociateProductDto
    {
        public Guid ProductId { get; set; }
        public string ExemptionCode { get; set; }
    }
}
