﻿using Newtonsoft.Json;

namespace Thex.TaxRule.Dto.Dto.PT.InvoiceXpressIntegration.Proforma
{
    public class ProformaResponseDetailsDto
    {
        [JsonProperty("id")]
        public string Id { get; set; }
    }
}
