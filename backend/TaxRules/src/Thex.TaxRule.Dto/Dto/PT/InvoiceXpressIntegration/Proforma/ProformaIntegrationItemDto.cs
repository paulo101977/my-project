﻿using Newtonsoft.Json;
using Thex.TaxRule.Dto.InvoiceXpressIntegration;

namespace Thex.TaxRule.Dto.Dto.PT.InvoiceXpressIntegration.Proforma
{
    public class ProformaIntegrationItemDto
    {
        public int ServiceId { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("unit_price")]
        public decimal UnitPrice { get; set; }

        [JsonProperty("quantity")]
        public int Quantity { get; set; }

        [JsonProperty("unit")]
        public string UnitMeasurement { get; set; }

        [JsonProperty("tax")]
        public TaxInvoiceXpressDto TaxDetails { get; set; }

        public bool ShouldSerializeServiceId()
        {
            return false;
        }
    }
}
