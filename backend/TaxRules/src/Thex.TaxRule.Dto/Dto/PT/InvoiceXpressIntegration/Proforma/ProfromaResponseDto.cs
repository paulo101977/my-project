﻿using Newtonsoft.Json;

namespace Thex.TaxRule.Dto.Dto.PT.InvoiceXpressIntegration.Proforma
{
    public class ProformaResponseDto
    {
        public static ProformaResponseDto NullInstance = null;

        [JsonProperty("proforma")]
        public ProformaResponseDetailsDto ResponseDetails { get; set; }
    }
}
