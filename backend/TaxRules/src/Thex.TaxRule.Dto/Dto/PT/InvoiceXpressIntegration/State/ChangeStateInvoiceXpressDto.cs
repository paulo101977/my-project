﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using Thex.TaxRule.Dto.Dto.Integration.Payment;

namespace Thex.TaxRule.Dto.InvoiceXpressIntegration
{
    [JsonObject("invoice")]
    public class ChangeStateInvoiceXpressDto
    {
        [JsonProperty("state")]
        public string State { get; internal set; }
    }
}
