﻿using Newtonsoft.Json;

namespace Thex.TaxRule.Dto.InvoiceXpressIntegration
{
    public class TaxInvoiceXpressDto
    {
        /// <summary>
        /// The tax name to be used on this line item. If not found the default tax is applied to the line item.
        /// </summary>
        [JsonProperty("name")]
        public string TaxName { get; set; }
    }
}
