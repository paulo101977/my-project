﻿using Newtonsoft.Json;

namespace Thex.TaxRule.Dto.InvoiceXpressIntegration
{
    [JsonObject("cliente")]
    public class PersonInvoiceXpressDto
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        /// <summary>
        /// The document’s client. If the client doesn’t exist, a new one is created. 
        /// If it exists, the remaining client fields will be ignored.
        /// </summary>
        [JsonProperty("name")]
        public string Name { get; set; }

        /// <summary>
        /// The client’s unique code
        /// </summary>
        [JsonProperty("code")]
        public string Code { get; set; }

        /// <summary>
        /// Client email address. Must be a valid email address
        /// </summary>
        [JsonProperty("email")]
        public string Email { get; set; }

        /// <summary>
        /// Client company address.
        /// </summary>
        [JsonProperty("address")]
        public string StreetName { get; set; }

        /// <summary>
        /// Client’s city
        /// </summary>
        [JsonProperty("city")]
        public string City { get; set; }

        /// <summary>
        /// Client’s postal code for it’s company address.
        /// </summary>
        [JsonProperty("postal_code")]
        public string PostalCode { get; set; }

        /// <summary>
        ///  Country, normally used for a company country.
        /// </summary>
        [JsonProperty("country")]
        public string Country { get; set; }

        /// <summary>
        /// Client's phone number.
        /// </summary>
        [JsonProperty("phone")]
        public string Phone { get; set; }

        /// <summary>
        ///  Client's default observations. This is added to the observations field of all the invoices sent to this client.
        /// </summary>
        [JsonProperty("observations")]
        public string AdditionalInformation { get; set; }

        /// <summary>
        /// Client's fiscal ID (Número de Contribuinte).
        /// </summary>
        [JsonProperty("fiscal_id")]
        public string FiscalId { get; set; }
    }
}
