﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Thex.TaxRule.Dto.InvoiceXpressIntegration
{
    public class CancelPaymentoInvoiceXpressDto
    {
        [JsonProperty("state")]
        public string State { get; set; }

        [JsonProperty("message")]
        public string Message { get; set; }
    }
}
