﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Thex.TaxRule.Dto.InvoiceXpressIntegration
{
    public class PaymentoInvoiceXpressDto
    {
        [JsonProperty("amount")]
        public decimal Amount { get; set; }
    }
}
