﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Thex.TaxRule.Dto.InvoiceXpressIntegration.Error
{
    public class Error
    {
        [JsonProperty("error")]
        public string Message { get; set; }
    }

    public class InvoiceXpressErrorDto
    {
        public static InvoiceXpressErrorDto NullInstance { get; set; }

        [JsonProperty("errors")]
        public List<Error> ErrorsList { get; set; }
    }
}
