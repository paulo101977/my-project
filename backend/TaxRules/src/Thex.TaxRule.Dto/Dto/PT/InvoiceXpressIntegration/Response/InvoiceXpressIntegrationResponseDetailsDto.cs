﻿using System;
using System.Collections.Generic;
using System.Text;
using Thex.Common.Dto.InvoiceIntegration;

namespace Thex.TaxRule.Dto.Dto.PT.InvoiceXpressIntegration
{
    public class InvoiceXpressIntegrationResponseDetailsDto
    {
        public InvoiceXpressIntegrationResponseDetailsDto(InvoiceIntegrationResponseDto dto)
        {
            this.InternalId = dto.InternalId;
            this.ExternalId = dto.ExternalId;
            this.Status = dto.Status;
            this.Reason = dto.Reason;
            this.UrlPdf = dto.UrlPdf;
            this.UrlXml = dto.UrlXml;
            this.Number = !string.IsNullOrEmpty(dto.Number) ? dto.Number : dto.InvoiceNumber;
            this.RpsNumber = dto.RpsNumber;
            this.Serial = dto.RpsSerial;
            this.EmissionDate = dto.EmissionDate;
            this.VerificationCode = dto.VerificationCode;
        }

        public string InternalId { get; set; }

        public string ExternalId { get; set; }

        public string Status { get; set; }

        public string Reason { get; set; }

        public string UrlPdf { get; set; }

        public string UrlXml { get; set; }

        public string Number { get; set; }
        public string RpsNumber { get; set; }

        public string Serial { get; set; }

        public string EmissionDate { get; set; }

        public string VerificationCode { get; set; }
    }

}
