﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Thex.TaxRule.Dto.Dto.PT.InvoiceXpressIntegration.Response
{
    public class CreditNoteInvoiceResponseDto
    {
        public static CreditNoteInvoiceResponseDto NullInstance = null;

        [JsonProperty("credit_note")]
        public CreditNoteInvoiceResponseDetailsDto ResponseDetails { get; set; }
    }

    public class CreditNoteInvoiceResponseDetailsDto
    {
        [JsonProperty("total")]
        public decimal Amount { get; set; }

        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("sequence_number")]
        public string InvoiceNumber { get; set; }

        [JsonProperty("date")]
        public string EmissionDate { get; set; }

        [JsonProperty("permalink")]
        public string UrlPdf { get; set; }
    }
}
