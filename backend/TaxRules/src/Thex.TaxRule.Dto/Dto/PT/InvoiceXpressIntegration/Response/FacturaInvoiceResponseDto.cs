﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Thex.TaxRule.Dto.InvoiceXpressIntegration.Response
{
    public class FacturaInvoiceResponseDetailsDto
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("sequence_number")]
        public string InvoiceNumber { get; set; }

        [JsonProperty("date")]
        public string EmissionDate { get; set; }

        [JsonProperty("permalink")]
        public string UrlPdf { get; set; }

        [JsonProperty("total")]
        public decimal Amount { get; set; }

        [JsonProperty("client")]
        public FacturaClientResponseDetailsDto Client { get; set; }
    }

    public class FacturaPaymentResponseDetailsDto
    {
        [JsonProperty("id")]
        public string Id { get; set; }
    }

    public class FacturaInvoiceResponseDto
    {
        public static FacturaInvoiceResponseDto NullInstance = null;

        [JsonProperty("invoice")]
        public FacturaInvoiceResponseDetailsDto ResponseDetails { get; set; }
    }

    public class FacturaPaymentResponseDto
    {
        public static FacturaPaymentResponseDto NullInstance = null;

        [JsonProperty("receipt")]
        public FacturaPaymentResponseDetailsDto ResponseDetails { get; set; }
    }

    public class FacturaClientResponseDetailsDto
    {
        [JsonProperty("id")]
        public string Id { get; set; }
    }
}
