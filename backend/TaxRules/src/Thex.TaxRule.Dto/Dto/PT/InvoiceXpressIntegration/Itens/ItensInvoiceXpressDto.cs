﻿using Newtonsoft.Json;
using System.Collections.Generic;
using Thex.TaxRule.Dto.Dto.Integration.Tax;

namespace Thex.TaxRule.Dto.InvoiceXpressIntegration
{
    public class ItensInvoiceXpressDto
    {
        /// <summary>
        /// Name of the item. Must be unique.
        /// </summary>
        [JsonProperty("name")]
        public string Name { get; set; }

        /// <summary>
        /// Item’s description.
        /// </summary>
        [JsonProperty("description")]
        public string Description { get; set; }

        /// <summary>
        /// Item’s unit price. Must be a number equal or greater than 0.0
        /// </summary>
        [JsonProperty("unit_price")]
        public decimal UnitPrice { get; set; }

        /// <summary>
        /// Quantity
        /// </summary>
        [JsonProperty("quantity")]
        public decimal Quantity { get; set; }

        /// <summary>
        /// The unit of measure. Ex: hour, day, month, unit , service or other.
        /// </summary>
        [JsonProperty("unit")]
        public string UnitMeasurement { get; set; }

        /// <summary>
        /// The item discount percentage (%). Defaults to 0.0. Must be a value between 0.0 and 100.0 inclusive.
        /// </summary>
        [JsonProperty("discount")]
        public decimal DiscontValue { get; set; }

        /// <summary>
        /// The tax applied to the line item. If not present the default tax is applied to the line item.
        /// </summary>
        [JsonProperty("tax")]
        public TaxInvoiceXpressDto TaxDetails { get; set; }
    }
}
