﻿using Newtonsoft.Json;
using System.Collections.Generic;
using Thex.TaxRule.Dto.Dto.PT.InvoiceXpressIntegration;
using Tnf.MongoDb.Entities;

namespace Thex.TaxRule.Dto.InvoiceXpressIntegration
{
    public class FacturaInvoice : IMongoDbEntity<string>
    {
        private string _facturaType = "Factura";


        public FacturaInvoice(string date, string dueDate, string tokenClient, string integrationCode, 
                              decimal amount, string externalId, string serieId, string observations)
        {
            this.ItensDetails = new List<ItensInvoiceXpressDto>();
            this.Date = date;
            this.DueDate = dueDate;
            this.TokenClient = tokenClient;
            this.IntegrationCode = integrationCode;
            this.InvoiceType = _facturaType;
            this.Amount = amount;
            this.Id = externalId;
            this.SequenceId = serieId;
            this.Observations = observations;
        }

        public FacturaInvoice(decimal amount, string externalId)
        {
            this.ItensDetails = new List<ItensInvoiceXpressDto>();
            this.Amount = amount;
            this.Id = externalId;
        }

        [JsonIgnore]
        public decimal Amount { get; set; }

        [JsonIgnore]
        public string TokenClient { get; set; }

        [JsonIgnore]
        public string IntegrationCode { get; set; }

        [JsonIgnore]
        public string InvoiceType { get; internal set; }

        [JsonIgnore]
        public string Id { get; set; }

        /// <summary>
        ///  The (owner) invoice associated to this document. This option is only available for credit_notes or debit_notes. 
        ///  You can also send the (owner) guide id when creating an invoice to associate an invoice to a guide.
        /// </summary>
        [JsonProperty("owner_invoice_id")]
        public string FacturaId { get; set; }

        /// <summary>
        /// The document date. Must be in format dd/mm/yyyy ex.: 03/12/2017. 
        /// If format is invalid, date will be set to current date.
        /// </summary>
        [JsonProperty("date")]
        public string Date { get; set; }

        /// <summary>
        // ID of the sequence you want to use with this document.If missing, the default sequence will be used.
        /// </summary>
        [JsonProperty("sequence_id")]
        public string SequenceId { get; set; }

        /// <summary>
        /// The document due date. Must be in format dd/mm/yyyy ex.: 03/12/2017. 
        /// If format is invalid, date will be set to current date.
        /// </summary>
        [JsonProperty("due_date")]
        public string DueDate { get; set; }

        /// <summary>
        /// Required/Optional] Portuguese IVA exemption code. 
        /// Required for portuguese accounts on invoices with IVA exempt items (0%). 
        /// Must be one of the following: M01, M02, M03, M04, M05, M06, M07, M08, M09, M10, M11, M12, M13, M14, M15, M16, M99. 
        /// </summary>
        [JsonProperty("tax_exemption")]
        public string TaxExemption { get; set; }

        /// <summary>
        /// [Optional] Document observations, these will be printed with the invoice
        /// </summary>
        [JsonProperty("observations")]
        public string Observations { get; set; }

        /// <summary>
        /// Client details
        /// </summary>
        [JsonProperty("client")]
        public PersonInvoiceXpressDto PersonDetails { get; set; }

        /// <summary>
        /// An array of line items. If items with the given names do not exist, they are created. 
        /// If an item already exists, it is updated with the new values. At least one is required.
        /// </summary>
        [JsonProperty("items")]
        public List<ItensInvoiceXpressDto> ItensDetails { get; set; }

        public InvoiceXpressIntegrationResponseDetailsDto ResponseDetails { get; set; }
    }
}
