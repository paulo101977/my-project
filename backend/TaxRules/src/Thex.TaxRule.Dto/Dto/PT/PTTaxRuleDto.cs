﻿using System;
using Tnf.Dto;

namespace Thex.TaxRule.Dto
{
    public class PTTaxRuleDto : BaseDto
    {
        public Guid Id { get; set; }
        public int ServiceId { get; set; }
        public Guid ProductId { get; set; }
        public Guid IvaId { get; set; }
        public bool IsActive { get; set; }
        public string Description { get; set; }
        public string ExemptionCode { get; set; }
    }
}
