﻿using System;

namespace Thex.TaxRule.Dto
{
    public class ProductDto
    {
        public Guid Id { get; set; }
        public string ProductName { get; set; }
    }
}
