﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a Crudzilla tool.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using Tnf.Dto;

namespace Thex.TaxRule.Dto
{
    public partial class ImpostoRetidoDto : BaseDto
    {
        public static ImpostoRetidoDto NullInstance = null;
        public Guid Id { get; set; }
        public Guid OwnerId { get; set; }
        public Guid? PropertyUId { get; set; }
    }
}
