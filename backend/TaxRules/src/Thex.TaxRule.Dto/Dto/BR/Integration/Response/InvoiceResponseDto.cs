﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using Thex.TaxRule.Dto.InvoiceXpressIntegration.Response;

namespace Thex.TaxRule.Dto.Dto.Integration.Response
{
    public class InvoiceResponseDto
    {
        public static InvoiceResponseDto NullInstance = null;

        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonProperty("motivoStatus")]
        public string Reason { get; set; }

        [JsonProperty("linkDanfe")]
        public string UrlPdf { get; set; }

        [JsonProperty("linkDownloadXml")]
        public string UrlXml { get; set; }

        [JsonProperty("externalId")]
        public string ExternalId { get; set; }

        [JsonProperty("nfeId")]
        public string InvoiceId { get; set; }

        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("paymentId")]
        public string PaymentId { get; set; }

        [JsonProperty("dataCriacao")]
        public DateTime EmissionDate { get; set; }

        [JsonProperty("numero")]
        public string InvoiceNumber { get; set; }

        [JsonProperty("serie")]
        public string Series { get; set; }

        [JsonProperty("client")]
        public FacturaClientResponseDetailsDto Client { get; set; }
    }
}
