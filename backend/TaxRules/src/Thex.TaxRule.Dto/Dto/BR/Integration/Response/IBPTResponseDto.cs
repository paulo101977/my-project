﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Thex.TaxRule.Dto.Dto.Integration.Response
{
    public class IBPTResponseDto
    {
        [JsonProperty("ValorTributoNacional")]
        public decimal? FederalTax { get; set; }

        [JsonProperty("ValorTributoMunicipal")]
        public decimal? CityTax { get; set; }

        [JsonProperty("ValorTributoEstadual")]
        public decimal? StateTax { get; set; }

        [JsonProperty("Fonte")]
        public string Source { get; set; }
    }
}