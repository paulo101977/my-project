﻿using Newtonsoft.Json;

namespace Thex.TaxRule.Dto.Dto.Integration.Response
{
    public class InvoiceCancelResponseDto
    {
        public static InvoiceCancelResponseDto NullInstance = null;

        public InvoiceCancelResponseDto(bool success)
        {
            Success = Success;
        }

        [JsonProperty("success")]
        public string Success { get; }
    }
}
