﻿using Newtonsoft.Json;
using System.Collections.Generic;
using Thex.TaxRule.Dto.Dto.Integration.Itens;
using Thex.TaxRule.Dto.Dto.Integration.NFSe;
using Thex.TaxRule.Dto.Dto.Integration.Order;
using Tnf.MongoDb.Entities;

namespace Thex.TaxRule.Dto.Dto.Integration.NFCe
{
    public class SATInvoice : IntegrationInvoiceServiceBaseDto, IMongoDbEntity<string>
    {
        private string _enviromentDefault = "Produção";
        private string _nfceType = "SAT";

        public SATInvoice(string externalId, string propertyUUid, string enviroment, bool emailConfirmation,
                           string propertyIntegrationCode, int? number, string serialNumber)
            : base(propertyIntegrationCode)
        {
            this.NFceId = externalId;
            this.ExternalId = externalId;
            this.PropertyIntegrationCode = propertyIntegrationCode;
            this.Environment = enviroment == string.Empty ? _enviromentDefault : enviroment;
            this.EmailConfirmation = emailConfirmation;
            this.InvoiceType = _nfceType;
            this.Number = number;
            this.SerialNumber = serialNumber;
        }

        [JsonProperty("id")]
        public string NFceId { get; set; }

        [JsonProperty("numero")]
        public int? Number { get; set; }

        [JsonProperty("serie")]
        public string SerialNumber { get; set; }

        [JsonProperty("pedido")]
        public OrderDto OrderDetails { get; set; }

        [JsonProperty("itens")]
        public List<ItensDto> ItensDetails { get; set; }
    }
}
