﻿using Newtonsoft.Json;

namespace Thex.TaxRule.Dto.Dto.Integration
{
    [JsonObject("cliente")]
    public class PersonDto
    {
        [JsonProperty("endereco")]
        public AddressDto Address { get; set; }

        [JsonProperty("tipoPessoa")]
        public string Type { get; set; }

        [JsonProperty("nome")]
        public string Name { get; set; }

        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("cpfCnpj")]
        public string Document { get; set; }

        [JsonProperty("telefone")]
        public string Phone { get; set; }

        [JsonProperty("indicadorContribuinteICMS")]
        public string ICMSIndicator { get; set; }
    }
}
