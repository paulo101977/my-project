﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Thex.TaxRule.Dto.Dto.Integration.NFSe
{
    public class NFSeDetailsDto
    {
        [JsonProperty("descricao")]
        public string Description { get; set; }

        [JsonProperty("aliquotaIss")]
        public decimal? Tax { get; set; }

        [JsonProperty("issRetidoFonte")]
        public bool? TaxRetained { get; set; }

        [JsonProperty("cnae")]
        public string Cnae { get; set; }

        [JsonProperty("codigoServicoMunicipio")]
        public string Code { get; set; }

        [JsonProperty("valorPis")]
        public decimal? PisAmount { get; set; }

        [JsonProperty("valorCofins")]
        public decimal? CofinsAmount { get; set; }

        [JsonProperty("valorCsll")]
        public decimal? CsllAmount { get; set; }

        [JsonProperty("valorInss")]
        public decimal? InssAmount { get; set; }

        [JsonProperty("valorIr")]
        public decimal? IrAmount { get; set; }
    }
}
