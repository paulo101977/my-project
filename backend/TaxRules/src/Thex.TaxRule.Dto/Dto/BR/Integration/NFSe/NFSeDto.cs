﻿using Newtonsoft.Json;
using Tnf.MongoDb.Entities;

namespace Thex.TaxRule.Dto.Dto.Integration.NFSe
{
    public class NFSeInvoice : IntegrationInvoiceServiceBaseDto, IMongoDbEntity<string>
    {
        private string _enviromentDefault = "Homologação";
        private string _nfseType = "NFSe";

        public NFSeInvoice(string externalId, decimal amount, string propertyUUid, string enviroment, bool emailConfirmation, string propertyIntegrationCode)
            : base(propertyIntegrationCode)
        {
            this.ExternalId = externalId;
            this.Amount = amount;
            this.PropertyIntegrationCode = propertyUUid;
            this.Environment = enviroment == string.Empty ? _enviromentDefault : enviroment;
            this.EmailConfirmation = emailConfirmation;
            this.InvoiceType = _nfseType;
        }

        public NFSeInvoice(string externalId, string enviroment, string propertyIntegrationCode, string invoiceType)
            : base(propertyIntegrationCode)
        {
            this.ExternalId = externalId;
            this.PropertyIntegrationCode = propertyIntegrationCode;
            this.Environment = enviroment;
            this.InvoiceType = _nfseType;
        }

        [JsonProperty("valorTotal")]
        public decimal Amount { get; set; }

        [JsonProperty("servico")]
        public NFSeDetailsDto ServiceDetails { get; set; }

        [JsonProperty("observacoes")]
        public string AdditionalInformations { get; set; }
    }
}
