﻿using Newtonsoft.Json;

namespace Thex.TaxRule.Dto.Dto.Integration.Tax
{
    public class SimpleTaxPrediction
    {
        [JsonProperty("percentual")]
        public double Tax { get; set; }
    }

    public class DetailedTaxPrediction
    {
        [JsonProperty("percentualFederal")]
        public decimal? FederalTax { get; set; }

        [JsonProperty("percentualEstadual")]
        public decimal? StateTax { get; set; }

        [JsonProperty("percentualMunicipal")]
        public decimal? CityTax { get; set; }
    }

    public class TaxPrediction
    {
        [JsonProperty("simplificado")]
        public SimpleTaxPrediction SimpleTaxPredictionDetails { get; set; }

        [JsonProperty("detalhado")]
        public DetailedTaxPrediction DetailedTaxPredictionDetails { get; set; }

        [JsonProperty("fonte")]
        public string Source { get; set; }
    }

    public class TaxDto
    {
        [JsonProperty("percentualAproximadoTributos")]
        public TaxPrediction TaxPredictionDetails { get; set; }

        [JsonProperty("icms")]
        public IcmsTaxDto IcmsTaxDetails { get; set; }

        [JsonProperty("pis")]
        public PisTaxDto PisTaxDetails { get; set; }

        [JsonProperty("cofins")]
        public CofinsTaxDto CofinsTaxDetails { get; set; }

        [JsonProperty("iss")]
        public IssTaxDto IssTaxDetails { get; set; }
    }
}
