﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Thex.TaxRule.Dto.Dto.Integration.Tax
{
    public class IcmsTaxDto
    {
        [JsonProperty("situacaoTributaria")]
        public string Code { get; set; }

        [JsonProperty("origem")]
        public string Source { get; set; }

        [JsonProperty("aliquota")]
        public decimal? Tax { get; set; }

        [JsonProperty("baseCalculo")]
        public decimal? TaxBase { get; set; }

        [JsonProperty("modalidadeBaseCalculo")]
        public decimal? TaxBaseModality { get; set; }

        [JsonProperty("percentualReducaoBaseCalculo")]
        public decimal? TaxBaseReduction { get; set; }

        [JsonProperty("baseCalculoST")]
        public decimal? TaxBaseST { get; set; }

        [JsonProperty("aliquotaST")]
        public decimal? TaxST { get; set; }

        [JsonProperty("modalidadeBaseCalculoST")]
        public decimal? TaxBaseSTModality { get; set; }

        [JsonProperty("percentualReducaoBaseCalculoST")]
        public decimal? TaxBaseSTReduction { get; set; }

        [JsonProperty("percentualMargemValorAdicionadoST")]
        public decimal? TaxAddedST { get; set; }
    }
}
