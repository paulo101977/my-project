﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Thex.TaxRule.Dto.Dto.Integration.Tax
{
    public class CofinsTaxPercentageDto
    {
        [JsonProperty("aliquota")]
        public decimal Tax { get; set; }
    }

    public class CofinsTaxDto : TaxDto
    {
        [JsonProperty("situacaoTributaria")]
        public string Code { get; set; }

        [JsonProperty("porAliquota")]
        public CofinsTaxPercentageDto TaxPercentageDetails { get; set; }
    }
}
