﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Thex.TaxRule.Dto.Dto.Integration.Tax
{
    public class IssTaxDto 
    {
        [JsonProperty("itemListaServicoLC116")]
        public string LC116 { get; set; }

        [JsonProperty("aliquotaIss")]
        public double Tax { get; set; }
    }
}
