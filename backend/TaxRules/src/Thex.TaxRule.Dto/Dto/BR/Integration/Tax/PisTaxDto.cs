﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Thex.TaxRule.Dto.Dto.Integration.Tax
{
    public class PisTaxPercentageDto
    {
        [JsonProperty("aliquota")]
        public decimal Tax { get; set; }
    }

    public class PisTaxDto
    {
        [JsonProperty("situacaoTributaria")]
        public string Code { get; set; }

        [JsonProperty("porAliquota")]
        public PisTaxPercentageDto TaxPercentageDetails { get; set; }
    }
}
