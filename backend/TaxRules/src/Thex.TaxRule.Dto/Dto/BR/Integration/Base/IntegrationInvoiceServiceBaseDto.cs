﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Thex.TaxRule.Dto.Dto.Integration.NFSe
{
    public class IntegrationInvoiceServiceBaseDto : IntegrationInvoice
    {
        public IntegrationInvoiceServiceBaseDto(string propertyIntegrationCode) : base(propertyIntegrationCode)
        {
        }

        public IntegrationInvoiceServiceBaseDto(string externalId, string enviroment, string propertyIntegrationCode, string invoiceType)
             : base(externalId, enviroment, propertyIntegrationCode, invoiceType)
        {
        }
    }
}
