﻿using Newtonsoft.Json;
using Thex.Common.Dto.InvoiceIntegration;

namespace Thex.TaxRule.Dto.Dto.Integration
{
    public class IntegrationInvoice
    {
        public IntegrationInvoice(string propertyIntegrationCode)
        {
            this.PropertyIntegrationCode = propertyIntegrationCode;
        }

        public IntegrationInvoice(string externalId, string enviroment, string propertyIntegrationCode, 
                                  string invoiceType)
        {
            this.ExternalId = externalId;
            this.PropertyIntegrationCode = propertyIntegrationCode;
            this.Environment = enviroment;
            this.InvoiceType = invoiceType;
        }

        [JsonProperty("tipo")]
        public string InvoiceType { get; set; }

        [JsonProperty("empresaId")]
        public string PropertyIntegrationCode { get; set; }

        [JsonProperty("ambienteEmissao")]
        public string Environment { get; set; }

        [JsonProperty("enviarPorEmail")]
        public bool EmailConfirmation { get; set; }

        [JsonProperty("idExterno")]
        public string ExternalId { get; set; }

        [JsonProperty("cliente")]
        public PersonDto Person { get; set; }

        [JsonIgnore]
        public string Id
        {
            get
            {
                return this.ExternalId;
            }
        }

        public InvoiceIntegrationResponseDetailsDto ResponseDetails { get; set; }
    }

    public class InvoiceIntegrationResponseDetailsDto
    {
        public InvoiceIntegrationResponseDetailsDto(InvoiceIntegrationResponseDto dto)
        {
            this.InternalId = dto.InternalId;
            this.ExternalId = dto.ExternalId;
            this.Status = dto.Status;
            this.Reason = dto.Reason;
            this.UrlPdf = dto.UrlPdf;
            this.UrlXml = dto.UrlXml;
            this.Number = !string.IsNullOrEmpty(dto.Number)? dto.Number : dto.InvoiceNumber;
            this.RpsNumber = dto.RpsNumber;
            this.Serial = dto.RpsSerial;
            this.EmissionDate = dto.EmissionDate;
            this.VerificationCode = dto.VerificationCode;
        }

        public string InternalId { get; set; }

        public string ExternalId { get; set; }

        public string Status { get; set; }

        public string Reason { get; set; }

        public string UrlPdf { get; set; }

        public string UrlXml { get; set; }

        public string Number { get; set; }
        public string RpsNumber { get; set; }

        public string Serial { get; set; }

        public string EmissionDate { get; set; }

        public string VerificationCode { get; set; }
    }
}
