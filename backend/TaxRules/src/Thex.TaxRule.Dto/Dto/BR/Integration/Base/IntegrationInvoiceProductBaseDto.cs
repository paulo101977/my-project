﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Thex.TaxRule.Dto.Dto.Integration.NFSe
{
    public class IntegrationInvoiceProductBaseDto : IntegrationInvoice
    {
        public IntegrationInvoiceProductBaseDto(string propertyIntegrationCode) : base(propertyIntegrationCode)
        {

        }

        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("numero")]
        public string Number { get; set; }

        [JsonProperty("serie")]
        public string Series { get; set; }

        [JsonProperty("naturezaOperacao")]
        public string Operation { get; internal set; }

        [JsonProperty("tipoOperacao")]
        public string OutputOperation { get; internal set; }

        [JsonProperty("finalidade")]
        public string Purpose { get; internal set; }

        [JsonProperty("consumidorFinal")]
        public bool Destination { get; internal set; }

        [JsonProperty("indicadorPresencaConsumidor")]
        public string OperationType { get; internal set; }

        [JsonProperty("informacoesAdicionais")]
        public string AdditionalDetails { get; internal set; }
    }
}
