﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using Thex.TaxRule.Dto.Dto.Integration.Payment;

namespace Thex.TaxRule.Dto.Dto.Integration.Order
{
    public class OrderDto
    {
        private readonly string _defaultOperationType = "OperacaoPresencial";

        public OrderDto()
        {
            this.OperationType = _defaultOperationType;
        }

        [JsonProperty("presencaConsumidor")]
        public string OperationType { get; internal set; }

        [JsonProperty("pagamento")]
        public PaymentoDto PaymentDetails { get; set; }
    }
}
