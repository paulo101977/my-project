﻿using Newtonsoft.Json;
using System.Collections.Generic;
using Thex.TaxRule.Dto.Dto.Integration.Tax;

namespace Thex.TaxRule.Dto.Dto.Integration.Itens
{
    public class ItensDto
    {
        [JsonProperty("cfop")]
        public string Cfop { get; set; }

        [JsonProperty("codigo")]
        public string Code { get; set; }

        [JsonProperty("descricao")]
        public string Description { get; set; }

        [JsonProperty("ncm")]
        public string Ncm { get; set; }

        [JsonProperty("cest")]
        public string Cest { get; set; }

        [JsonProperty("sku")]
        public string Sku { get; set; }

        [JsonProperty("quantidade")]
        public decimal Quantity { get; set; }

        [JsonProperty("unidadeMedida")]
        public string UnitMeasurement { get; set; }

        [JsonProperty("valorUnitario")]
        public decimal UnitPrice { get; set; }

        [JsonProperty("descontos")]
        public decimal DiscontValue { get; set; }

        [JsonProperty("outrasDespesas")]
        public decimal AditionalValue { get; set; }

        [JsonProperty("impostos")]
        public TaxDto TaxDetails { get; set; }
    }
}
