﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Thex.TaxRule.Dto.Dto.Integration.NFSe
{
    public class IntegrationDownloadInvoiceDto
    {
        [JsonProperty("empresaId")]
        public string PropertyID { get; set; }

        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("idExterno")]
        public string ExternalId { get; set; }
    }
}
