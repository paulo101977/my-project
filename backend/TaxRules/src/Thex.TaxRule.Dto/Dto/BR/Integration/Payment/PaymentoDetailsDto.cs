﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Thex.TaxRule.Dto.Dto.Integration.Payment
{
    public class PaymentoDetailsDto
    {
        [JsonProperty("tipo")]
        public string Type { get; set; }

        [JsonProperty("valor")]
        public decimal Amount { get; set; }

        [JsonProperty("credenciadoraCartao")]
        public CreaditCardIssuerDto CreaditCardIssuerDetails { get; set; }
    }
}