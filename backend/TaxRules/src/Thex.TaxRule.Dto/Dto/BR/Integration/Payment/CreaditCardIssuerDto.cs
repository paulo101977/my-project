﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Thex.TaxRule.Dto.Dto.Integration.Payment
{
    public class CreaditCardIssuerDto
    {
        [JsonProperty("tipoIntegracaoPagamento")]
        public string Type { get; set; }

        [JsonProperty("cnpjCredenciadoraCartao")]
        public string Document { get; set; }

        [JsonProperty("bandeira")]
        public string PlasticBrand { get; set; }

        [JsonProperty("autorizacao")]
        public string AutorizationCode { get; set; }
    }
}
