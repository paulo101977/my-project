﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Thex.TaxRule.Dto.Dto.Integration.Payment
{
    public class PaymentoDto
    {
        private readonly string _defaultOperationType = "PagamentoAVista";

        public PaymentoDto()
        {
            this.OperationType = _defaultOperationType;
            this.Details = new List<PaymentoDetailsDto>();
        }

        [JsonProperty("tipo")]
        public string OperationType { get; set; }

        [JsonProperty("formas")]
        public List<PaymentoDetailsDto> Details { get; set; }
    }
}
