﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using Thex.TaxRule.Dto.Dto.Integration.Itens;
using Thex.TaxRule.Dto.Dto.Integration.NFSe;
using Thex.TaxRule.Dto.Dto.Integration.Order;

namespace Thex.TaxRule.Dto.Dto.Integration.NFe
{
    public class NfeDto : IntegrationInvoiceProductBaseDto
    {
        private string _nfeType = "NFe";

        public NfeDto(string propertyIntegrationCode) : base(propertyIntegrationCode)
        {
            this.InvoiceType = _nfeType;
        }

        [JsonProperty("pedido")]
        public OrderDto OrderDetails { get; set; }

        [JsonProperty("itens")]
        public List<ItensDto> ItensDetails { get; internal set; }
    }
}
