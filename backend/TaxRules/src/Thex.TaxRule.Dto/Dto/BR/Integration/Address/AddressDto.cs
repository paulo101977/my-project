﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Thex.TaxRule.Dto.Dto.Integration
{
    [JsonObject("endereco")]
    public class AddressDto
    {
        [JsonProperty("pais")]
        public string Country { get; set; }

        [JsonProperty("uf")]
        public string District { get; set; }

        [JsonProperty("cidade")]
        public string City { get; set; }

        [JsonProperty("logradouro")]
        public string StreetName { get; set; }

        [JsonProperty("numero")]
        public string StreetNumber { get; set; }

        [JsonProperty("complemento")]
        public string AdditionalInformation { get; set; }

        [JsonProperty("bairro")]
        public string Neighborhood { get; set; }

        [JsonProperty("cep")]
        public string PostalCode { get; set; }
    }
}
