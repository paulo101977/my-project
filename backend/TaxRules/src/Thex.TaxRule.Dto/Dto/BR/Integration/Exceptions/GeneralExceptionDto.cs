﻿using System;
using System.Collections.Generic;
using System.Text;
using Thex.TaxRule.Dto.Dto.Integration.Error;
using Thex.TaxRule.Dto.Dto.Integration.Exceptions.Base;

namespace Thex.TaxRule.Dto.Dto.Integration.Exceptions
{
    public class GeneralExceptionDto : BaseExceptionDto
    {
        private string _generalErrorMessage = "A general error occurred";

        public GeneralExceptionDto(string message)
                    : base(message)
        {
            this.Summary = message;
        }
        public GeneralExceptionDto(ErrorDto[] errors)
            : base(errors)
        {
            this.Summary = _generalErrorMessage;
        }

        public GeneralExceptionDto(string message, ErrorDto[] errors)
            : base(errors)
        {
            this.Summary = message;
        }

        public GeneralExceptionDto(string message, Exception inner)
            : base(message, inner)
        {
            this.Summary = message;
        }
    }
}
