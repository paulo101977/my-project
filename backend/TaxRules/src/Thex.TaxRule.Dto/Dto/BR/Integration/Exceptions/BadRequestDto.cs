﻿using System;
using System.Collections.Generic;
using System.Text;
using Thex.TaxRule.Dto.Dto.Integration.Exceptions.Base;

namespace Thex.TaxRule.Dto.Dto.Integration.Exceptions
{
    public class BadRequestDto : BaseExceptionDto
    {
        public BadRequestDto(string message) : base(message)
        {
        }
    }
}
