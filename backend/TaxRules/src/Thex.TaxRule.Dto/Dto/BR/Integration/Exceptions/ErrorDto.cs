﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Thex.TaxRule.Dto.Dto.Integration.Error
{
    public class ErrorDto
    {
        public static ErrorDto NullInstance { get; set; }

        [JsonProperty("codigo")]
        public string Code { get; set; }

        [JsonProperty("mensagem")]
        public string Message { get; set; }

        public override string ToString()
        {
            return string.Format("{0} - {1}", this.Code, this.Message);
        }
    }
}
