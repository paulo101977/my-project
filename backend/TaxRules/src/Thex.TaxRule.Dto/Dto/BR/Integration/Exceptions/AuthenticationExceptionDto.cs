﻿using System;
using System.Collections.Generic;
using System.Text;
using Thex.TaxRule.Dto.Dto.Integration.Error;
using Thex.TaxRule.Dto.Dto.Integration.Exceptions.Base;

namespace Thex.TaxRule.Dto.Dto.Integration.Exceptions
{
    public class AuthenticationExceptionDto: BaseExceptionDto
    {
        private string _authMessage = "You are not authenticated. Check your api key and try again";

        public AuthenticationExceptionDto(ErrorDto[] errors)
                    : base(errors)
        {
            this.Summary = _authMessage;
        }

        public AuthenticationExceptionDto(string message, ErrorDto[] errors)
            : base(errors)
        {
            this.Summary = message;
        }
    }
}
