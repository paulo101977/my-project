﻿using System;
using System.Collections.Generic;
using System.Text;
using Thex.TaxRule.Dto.Dto.Integration.Error;
using Thex.TaxRule.Dto.Dto.Integration.Exceptions.Base;

namespace Thex.TaxRule.Dto.Dto.Integration.Exceptions
{
    public class ValidationExceptionDto : BaseExceptionDto
    {
        private string _generalValidationMessage = @"The server cannot or will not process the request due to something that is perceived to be a client error (e.g., malformed request syntax, or deceptive request routing)";

        public ValidationExceptionDto(string message)
           : base(message)
        {
            this.Summary = message;
        }

        public ValidationExceptionDto(ErrorDto[] errors)
            : base(errors)
        {
            this.Summary = _generalValidationMessage;
        }

        public ValidationExceptionDto(string message, ErrorDto[] errors)
            : base(errors)
        {
            this.Summary = message;
        }
    }
}
