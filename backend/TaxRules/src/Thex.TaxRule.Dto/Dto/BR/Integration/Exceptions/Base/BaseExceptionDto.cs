﻿using System;
using System.Collections.Generic;
using System.Text;
using Thex.TaxRule.Dto.Dto.Integration.Error;

namespace Thex.TaxRule.Dto.Dto.Integration.Exceptions.Base
{
    public abstract class BaseExceptionDto : Exception
    {
        private string _errorMessage = "Errors";

        protected string Summary { get; set; }

        public BaseExceptionDto() { }

        public ErrorDto[] Errors { get; protected set; }

        public BaseExceptionDto(string message)
            : base(message)
        {
        }

        public BaseExceptionDto(string message, Exception inner)
            : base(message, inner)
        {
        }

        public BaseExceptionDto(ErrorDto[] errors)
        {
            this.Errors = errors;
        }

        public override string Message
        {
            get
            {
                var sb = new StringBuilder();

                sb.AppendLine(this.Summary);

                if (this.Errors != null)
                {
                    if (this.Errors.Length > 0)
                    {
                        sb.AppendFormat("\r\n{0}:\r\n", _errorMessage);

                        foreach (var error in this.Errors)
                        {
                            sb.AppendLine(error.ToString());
                        }
                    }
                }

                return sb.ToString();
            }
        }
    }
}
