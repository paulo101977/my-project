﻿using Thex.TaxRule.Dto.Dto.Integration.Error;
using Thex.TaxRule.Dto.Dto.Integration.Exceptions.Base;

namespace Thex.TaxRule.Dto.Dto.Integration.Exceptions
{
    public class AuthorizationExceptionDto : BaseExceptionDto
    {
        private string _authMessage = "You don't have necessary permissions for the resource";

        public AuthorizationExceptionDto(ErrorDto[] errors)
           : base(errors)
        {
            this.Summary = _authMessage;
        }

        public AuthorizationExceptionDto(string message, ErrorDto[] errors)
            : base(errors)
        {
            this.Summary = message;
        }
    }
}
