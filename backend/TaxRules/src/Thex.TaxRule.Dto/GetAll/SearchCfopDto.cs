﻿using System;
using System.Collections.Generic;
using System.Text;
using Tnf.Dto;

namespace Thex.TaxRule.Dto.GetAll
{
    public class SearchCfopDto : RequestAllDto
    {
        public string Param { get; set; }
    }
}
