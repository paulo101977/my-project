﻿using System;

namespace Thex.TaxRule.Dto
{
    public class GetAllPTTaxRuleServiceDto
    {
        public Guid? IvaId { get; set; }
    }
}
