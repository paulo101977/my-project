﻿using System;

namespace Thex.TaxRule.Dto
{
    public class GetAllPTTaxRuleProductDto
    {
        public Guid? IvaId { get; set; }
    }
}
