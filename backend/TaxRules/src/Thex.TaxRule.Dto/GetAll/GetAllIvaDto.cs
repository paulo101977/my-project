﻿using Tnf.Dto;

namespace Thex.TaxRule.Dto
{
    public class GetAllIvaDto : RequestAllDto
    {
        public bool IsActive { get; set; }
    }
}
