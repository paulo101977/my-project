﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Thex.Kernel;

namespace Thex.TaxRule.Base
{
    [Authorize]
    public class ThexAppController : TnfController
    {
        protected IApplicationUser _applicationUser;

        public ThexAppController(IApplicationUser applicationUser)
        {
            _applicationUser = applicationUser;
        }
    }
}

