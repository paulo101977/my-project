﻿namespace Thex.TaxRule.Web
{
    public class RouteConsts
    {
        public const string BrasilRouteName = "api/br";
        public const string TaxRuleRouteName = "api/taxrule";
        public const string PortugalRouteName = "api/pt";
    }
}
