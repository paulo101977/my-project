using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.Kernel;
using Thex.TaxRule.ApplicationEurope.Interfaces;
using Thex.TaxRule.Base;
using Thex.TaxRule.Domain.Entities;
using Thex.TaxRule.Dto;
using Thex.TaxRule.Hub.Application.Interface;
using Tnf.AspNetCore.Mvc.Response;
using Tnf.Dto;

namespace Thex.TaxRule.Web.Controllers
{
    [Route(RouteConsts.PortugalRouteName)]
    [ApiController]
    public class PortugalController : ThexAppController
    {
        private readonly IIvaAppService _ivaAppService;
        private readonly IPTTaxRuleAppService _pTTaxRuleAppService;

        public PortugalController(ITaxHub taxHubAppService,
                                 IApplicationUser applicationUser,
                                 IIvaAppService ivaAppService,
                                 IPTTaxRuleAppService pTTaxRuleAppService)
                                 : base(applicationUser)
        {
            _ivaAppService = ivaAppService;
            _pTTaxRuleAppService = pTTaxRuleAppService;
        }


        [HttpGet("iva")]
        [ThexAuthorize("Tax_Get_Iva")]
        [ProducesResponseType(typeof(IListDto<IvaDto>), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> GetAll([FromQuery] GetAllIvaDto requestDto)
        {
            var response = await _ivaAppService.GetAll(requestDto);

            return CreateResponseOnGetAll(response, EntityNames.Iva);
        }

        [HttpGet("getalltaxrulesservices")]
        [ThexAuthorize("Tax_Get_GetAllTaxRulesServices")]
        [ProducesResponseType(typeof(List<PTTaxRuleServiceResponseDto>), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> GetAllTaxRulesServices([FromQuery] GetAllPTTaxRuleServiceDto getAllTaxRuleService)
        {
            var response = await _pTTaxRuleAppService.GetAllTaxRulesServices(getAllTaxRuleService);

            return CreateResponseOnGet(response, EntityNames.PtTaxRule);
        }

        [HttpGet("getalltaxrulesproducts")]
        [ThexAuthorize("Tax_Get_GetAllTaxRulesProducts")]
        [ProducesResponseType(typeof(List<PTTaxRuleProductResponseDto>), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> GetAllTaxRulesProducts([FromQuery] GetAllPTTaxRuleProductDto getAllPTTaxRuleProductDto)
        {
            var response = await _pTTaxRuleAppService.GetAllTaxRulesProducts(getAllPTTaxRuleProductDto);

            return CreateResponseOnGet(response, EntityNames.PtTaxRule);
        }

        [HttpPut("associateservicesandiva")]
        [ThexAuthorize("Tax_PUT_AssociateServicesAndIva")]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult AssociateServicesAndIva([FromBody] PTTaxRuleAssociateDto pTTaxRuleAssociateDto)
        {
            _pTTaxRuleAppService.AssociateRange(pTTaxRuleAssociateDto);

            return CreateResponseOnGet(null, EntityNames.PtTaxRule);
        }
    }
}
