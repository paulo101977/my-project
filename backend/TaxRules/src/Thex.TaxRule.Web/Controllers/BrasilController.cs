﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.Kernel;
using Thex.TaxRule.Application.Brasil.Interfaces;
using Thex.TaxRule.Base;
using Thex.TaxRule.Domain.Entities;
using Thex.TaxRule.Dto;
using Thex.TaxRule.Dto.GetAll;
using Tnf.AspNetCore.Mvc.Response;
using Tnf.Dto;

namespace Thex.TaxRule.Web.Controllers
{
    [ApiController]
    [Route(RouteConsts.BrasilRouteName)]
    public class BrasilController : ThexAppController
    {
        private readonly ICFOPAppService _cfopAppService;
        private readonly ICSOSNAppService _csosnAppService;
        private readonly ICSTAppService _cstAppService;
        private readonly INaturezaReceitaAppService _naturezaReceitaAppService;
        private readonly INBSAppService _nbsAppService;
        private readonly INCMAppService _ncmAppService;
        private readonly IRegimeApuracaoAppService _regimeApuracaoAppService;
        private readonly ITipoServicoAppService _tipoServicoAppService;
        private readonly IUFAppService _ufAppService;
        private readonly ITaxRulesAppServiceBrazil _taxRulesAppService;
        private readonly IImpostoRetidoAppService _impostoRetidoAppService;

        public BrasilController(ICFOPAppService cfopAppService,
                                ICSOSNAppService csosnAppService,
                                ICSTAppService cstAppService,
                                INaturezaReceitaAppService naturezaReceitaAppService,
                                INBSAppService nbsAppService,
                                INCMAppService ncmAppService,
                                IRegimeApuracaoAppService regimeApuracaoAppService,
                                ITipoServicoAppService tipoServicoAppService,
                                IUFAppService ufAppService,
                                ITaxRulesAppServiceBrazil taxRulesAppService,
                                IApplicationUser applicationUser,
                                IImpostoRetidoAppService impostoRetidoAppService)
            : base(applicationUser)
        {
            _cfopAppService = cfopAppService;
            _csosnAppService = csosnAppService;
            _cstAppService = cstAppService;
            _naturezaReceitaAppService = naturezaReceitaAppService;
            _nbsAppService = nbsAppService;
            _ncmAppService = ncmAppService;
            _regimeApuracaoAppService = regimeApuracaoAppService;
            _tipoServicoAppService = tipoServicoAppService;
            _ufAppService = ufAppService;
            _taxRulesAppService = taxRulesAppService;
            _impostoRetidoAppService = impostoRetidoAppService;
        }

        /// <summary>
        /// Retorna a lista de CFOP
        /// </summary>
        /// <returns></returns>
        [HttpGet("cfop")]
        [ThexAuthorize("Tax_Get_Cfop_GetAll")]
        [ProducesResponseType(typeof(IListDto<CFOPDto>), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> GetAll([FromQuery]GetAllCFOPDto requestDto)
        {
            var response = await _cfopAppService.GetAll(requestDto);

            return CreateResponseOnGetAll(response, EntityNames.CFOP);
        }

        /// <summary>
        /// Retorna a lista de CFOP
        /// </summary>
        /// <returns></returns>
        [HttpGet("cfop/search")]
        [ThexAuthorize("Tax_Get_Cfop_GetAllByFilters")]
        [ProducesResponseType(typeof(IListDto<CFOPDto>), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> GetAllByFilters([FromQuery]SearchCfopDto requestDto)
        {
            var response = await _cfopAppService.GetAllByFilters(requestDto);

            return CreateResponseOnGetAll(response, EntityNames.CFOP);
        }

        /// <summary>
        /// Retorna a lista de CFOP pelo código
        /// </summary>
        /// <returns></returns>
        [HttpGet("cfop/{code}")]
        [ThexAuthorize("Tax_Get_Cfop_GetByCode")]
        [ProducesResponseType(typeof(IListDto<CFOPDto>), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> GetByCode(string code, [FromQuery]GetAllCFOPDto requestDto)
        {
            var response = await _cfopAppService.GetByCode(code, requestDto);

            return CreateResponseOnGet(response, EntityNames.CFOP);
        }

        /// <summary>
        /// Retorna a lista de CSOSN
        /// </summary>
        /// <returns></returns>
        [HttpGet("csosn")]
        [ThexAuthorize("Tax_Get_Csosn_GetAll")]
        [ProducesResponseType(typeof(IListDto<CSOSNDto>), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> GetAll([FromQuery]GetAllCSOSNDto requestDto)
        {
            var response = await _csosnAppService.GetAll(requestDto);

            return CreateResponseOnGetAll(response, EntityNames.CSOSN);
        }

        /// <summary>
        /// Retorna a lista de CSOSN pelo código
        /// </summary>
        /// <returns></returns>
        [HttpGet("csosn/{code}")]
        [ThexAuthorize("Tax_Get_Csosn_GetByCode")]
        [ProducesResponseType(typeof(IListDto<CSOSNDto>), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> GetByCode(string code, [FromQuery]GetAllCSOSNDto requestDto)
        {
            var response = await _csosnAppService.GetByCode(code, requestDto);

            return CreateResponseOnGet(response, EntityNames.CSOSN);
        }

        /// <summary>
        /// Retorna a lista de CST
        /// </summary>
        /// <returns></returns>
        [HttpGet("cst")]
        [ThexAuthorize("Tax_Get_Cst_GetAll")]
        [ProducesResponseType(typeof(IListDto<CSTDto>), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> GetAll([FromQuery]GetAllCSTDto requestDto)
        {
            var response = await _cstAppService.GetAll(requestDto);

            return CreateResponseOnGetAll(response, EntityNames.CST);
        }

        /// <summary>
        /// Retorna a lista de CST pelo código
        /// </summary>
        /// <returns></returns>
        [HttpGet("cst/{code}")]
        [ThexAuthorize("Tax_Get_Cst_GetByCode")]
        [ProducesResponseType(typeof(IListDto<CSTDto>), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> GetByCode(string code, [FromQuery]GetAllCSTDto requestDto)
        {
            var response = await _cstAppService.GetByCode(code, requestDto);

            return CreateResponseOnGet(response, EntityNames.CST);
        }

        /// <summary>
        /// Retorna a lista de CST pelo tipo
        /// </summary>
        /// <returns></returns>
        [HttpGet("cst/getallbytype/{type}")]
        [ThexAuthorize("Tax_Get_Cst_GetByType")]
        [ProducesResponseType(typeof(IListDto<CSTDto>), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> GetByType(int type, [FromQuery]GetAllCSTDto requestDto)
        {
            var response = await _cstAppService.GetAllByType(type, requestDto);

            return CreateResponseOnGetAll(response, EntityNames.CST);
        }

        /// <summary>
        /// Retorna a lista de Natureza da Receita
        /// </summary>
        /// <returns></returns>
        [HttpGet("naturezareceita")]
        [ThexAuthorize("Tax_Get_Revenue_GetAll")]
        [ProducesResponseType(typeof(IListDto<NaturezaReceitaDto>), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> GetAll([FromQuery]GetAllNaturezaReceitaDto requestDto)
        {
            var response = await _naturezaReceitaAppService.GetAll(requestDto);

            return CreateResponseOnGetAll(response, EntityNames.NaturezaReceita);
        }

        /// <summary>
        /// Retorna a lista de Natureza da Receita pelo código
        /// </summary>
        /// <returns></returns>
        [HttpGet("naturezareceita/{code}")]
        [ThexAuthorize("Tax_Get_Revenue_GetByCode")]
        [ProducesResponseType(typeof(IListDto<NaturezaReceitaDto>), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> GetByCode(string code, [FromQuery]GetAllNaturezaReceitaDto requestDto)
        {
            var response = await _naturezaReceitaAppService.GetByCode(code, requestDto);

            return CreateResponseOnGet(response, EntityNames.NaturezaReceita);
        }


        /// <summary>
        /// Retorna a lista de Natureza da Receita pelo NCM
        /// </summary>
        /// <returns></returns>
        [HttpGet("naturezareceita/getbyncm/{ncm}")]
        [ThexAuthorize("Tax_Get_Revenue_GetByNcm")]
        [ProducesResponseType(typeof(IListDto<NaturezaReceitaDto>), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> GetByNCM(string ncm, [FromQuery]GetAllNaturezaReceitaDto requestDto)
        {
            var response = await _naturezaReceitaAppService.GetByNcm(ncm, requestDto);

            return CreateResponseOnGetAll(response, EntityNames.NaturezaReceita);
        }

        /// <summary>
        /// Retorna a lista de NBS
        /// </summary>
        /// <returns></returns>
        [HttpGet("nbs")]
        [ThexAuthorize("Tax_Get_Nbs_GetAll")]
        [ProducesResponseType(typeof(IListDto<NBSDto>), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> GetAll([FromQuery]GetAllNBSDto requestDto)
        {
            var response = await _nbsAppService.GetAll(requestDto);

            return CreateResponseOnGetAll(response, EntityNames.NBS);
        }

        /// <summary>
        /// Retorna a lista de NBS
        /// </summary>
        /// <returns></returns>
        [HttpGet("nbs/search")]
        [ThexAuthorize("Tax_Get_Nbs_GetAllByFilters")]
        [ProducesResponseType(typeof(IListDto<NBSDto>), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> GetAllByFilters([FromQuery]SearchNbsDto searchDto)
        {
            var response = await _nbsAppService.GetAllNbsByFilters(searchDto);

            return CreateResponseOnGetAll(response, EntityNames.NBS);
        }

        /// <summary>
        /// Retorna a lista de NBS pelo código
        /// </summary>
        /// <returns></returns>
        [HttpGet("nbs/{code}")]
        [ThexAuthorize("Tax_Get_Nbs_GetByCode")]
        [ProducesResponseType(typeof(IListDto<NBSDto>), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> GetByCode(string code, [FromQuery]GetAllNBSDto requestDto)
        {
            var response = await _nbsAppService.GetByCode(code, requestDto);

            return CreateResponseOnGet(response, EntityNames.CST);
        }

        /// <summary>
        /// Retorna a lista de NCM
        /// </summary>
        /// <returns></returns>
        [HttpGet("ncm")]
        [ThexAuthorize("Tax_Get_Ncm_GetAll")]
        [ProducesResponseType(typeof(IListDto<NCMDto>), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> GetAll([FromQuery]GetAllNCMDto requestDto)
        {
            var response = await _ncmAppService.GetAll(requestDto);

            return CreateResponseOnGetAll(response, EntityNames.NCM);
        }

        /// <summary>
        /// Retorna a lista de NCM pelo código
        /// </summary>
        /// <returns></returns>
        [HttpGet("ncm/{code}")]
        [ThexAuthorize("Tax_Get_Ncm_GetByCode")]
        [ProducesResponseType(typeof(IListDto<NCMDto>), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> GetByCode(string code, [FromQuery]GetAllNCMDto requestDto)
        {
            var response = await _ncmAppService.GetByCode(code, requestDto);

            return CreateResponseOnGet(response, EntityNames.NCM);
        }

        /// <summary>
        /// Retorna a lista de NCM
        /// </summary>
        /// <returns></returns>
        [HttpGet("ncm/search")]
        [ThexAuthorize("Tax_Get_Ncm_GetAllByFilters")]
        [ProducesResponseType(typeof(IListDto<NBSDto>), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> GetAllByFilters([FromQuery]SearchNcmDto searchDto)
        {
            var response = await _ncmAppService.GetAllByFilters(searchDto);

            return CreateResponseOnGetAll(response, EntityNames.NCM);
        }

        /// <summary>
        /// Retorna a lista de Regime de apuração
        /// </summary>
        /// <returns></returns>
        [HttpGet("regimeapuracao")]
        [ThexAuthorize("Tax_Get_TaxCalculation_GetAll")]
        [ProducesResponseType(typeof(IListDto<RegimeApuracaoDto>), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> GetAll([FromQuery]GetAllRegimeApuracaoDto requestDto)
        {
            var response = await _regimeApuracaoAppService.GetAll(requestDto);

            return CreateResponseOnGetAll(response, EntityNames.RegimeApuracao);
        }

        /// <summary>
        /// Retorna a lista de Regime de apuração pelo identificador
        /// </summary>
        /// <returns></returns>
        [HttpGet("regimeapuracao/{id}")]
        [ThexAuthorize("Tax_Get_TaxCalculation_GetById")]
        [ProducesResponseType(typeof(IListDto<RegimeApuracaoDto>), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> GetById(string id, [FromQuery]GetAllRegimeApuracaoDto requestDto)
        {
            var response = await _regimeApuracaoAppService.GetByCode(Guid.Parse(id), requestDto);

            return CreateResponseOnGet(response, EntityNames.RegimeApuracao);
        }

        /// <summary>
        /// Retorna a lista de tipos de serviço
        /// </summary>
        /// <returns></returns>
        [HttpGet("tiposervico")]
        [ThexAuthorize("Tax_Get_ServiceType_GetAll")]
        [ProducesResponseType(typeof(IListDto<TipoServicoDto>), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> GetAll([FromQuery]GetAllTipoServicoDto requestDto)
        {
            var response = await _tipoServicoAppService.GetAll(requestDto);

            return CreateResponseOnGetAll(response, EntityNames.TipoServico);
        }

        /// <summary>
        /// Retorna a lista de tipos de serviço pelo código
        /// </summary>
        /// <returns></returns>
        [HttpGet("tiposervico/{code}")]
        [ThexAuthorize("Tax_Get_ServiceType_GetByCode")]
        [ProducesResponseType(typeof(IListDto<RegimeApuracaoDto>), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> GetByCode(string code, [FromQuery]GetAllTipoServicoDto requestDto)
        {
            var response = await _tipoServicoAppService.GetByCode(code, requestDto);

            return CreateResponseOnGet(response, EntityNames.TipoServico);
        }

        /// <summary>
        /// Retorna a lista de UF
        /// </summary>
        /// <returns></returns>
        [HttpGet("uf")]
        [ThexAuthorize("Tax_Get_UF_GetAll")]
        [ProducesResponseType(typeof(IListDto<UFDto>), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> GetAll([FromQuery]GetAllUFDto requestDto)
        {
            var response = await _ufAppService.GetAll(requestDto);

            return CreateResponseOnGetAll(response, EntityNames.UF);
        }

        /// <summary>
        /// Retorna a lista de UF pelo código
        /// </summary>
        /// <returns></returns>
        [HttpGet("uf/{uf}")]
        [ThexAuthorize("Tax_Get_UF_GetByCode")]
        [ProducesResponseType(typeof(IListDto<UFDto>), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> GetByCode([FromQuery]GetAllUFDto requestDto, string uf)
        {
            var response = await _ufAppService.GetByCode(requestDto);

            return CreateResponseOnGet(response, EntityNames.UF);
        }

        /// <summary>
        /// Associa o cliente a imposto retido
        /// </summary>
        /// <returns></returns>
        [HttpPost("createimpostoretidolist")]
        [ThexAuthorize("Tax_Post_Imposto_Retido")]
        [ProducesResponseType(typeof(ImpostoRetidoDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> CreateImpostoRetidoList([FromBody]List<ImpostoRetidoDto> impostoRetidoDtoList)
        {
            await _impostoRetidoAppService.CreateList(impostoRetidoDtoList);

            return CreateResponseOnGet(null, EntityNames.ImpostoRetido);
        }

        /// <summary>
        /// Retorna os clientes que possuem imposto retido
        /// </summary>
        /// <returns></returns>
        [HttpGet("getallclientwithimpostoretido/property/{propertyUId}")]
        [ThexAuthorize("Tax_Get_All_Imposto_Retido")]
        [ProducesResponseType(typeof(ImpostoRetidoDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> GetAllClientWithImpostoRetido(Guid propertyUId, [FromQuery]GetAllImpostoRetidoDto getAllImpostoRetidoDto)
        {
            var response = await _impostoRetidoAppService.GetAllByOwnerIdAndPropertyUid(getAllImpostoRetidoDto, propertyUId);

            return CreateResponseOnGet(response, EntityNames.ImpostoRetido);
        }

        /// <summary>
        /// Remove o cliente da associação com imposto retido
        /// </summary>
        /// <returns></returns>
        [HttpDelete("deleteassociationwithimpostoretido/owner/{ownerId}/property/{propertyUId}")]
        [ThexAuthorize("Tax_Delete_Imposto_Retido")]
        [ProducesResponseType(typeof(ImpostoRetidoDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> DeletAassociationWithImpostoRetido(Guid ownerId, Guid propertyUId)
        {
            await _impostoRetidoAppService.Delete(ownerId, propertyUId);

            return CreateResponseOnDelete();
        }
    }
}
