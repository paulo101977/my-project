﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.Common.Dto.InvoiceIntegration;
using Thex.Dto.Integration;
using Thex.Kernel;
using Thex.TaxRule.Base;
using Thex.TaxRule.Domain.Entities;
using Thex.TaxRule.Dto.Dto.Integration.Response;
using Thex.TaxRule.Dto.Dto.PT.InvoiceXpressIntegration.Proforma;
using Thex.TaxRule.Hub.Application.Interface;
using Thex.Web;
using Tnf.AspNetCore.Mvc.Response;

namespace Thex.TaxRule.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class InvoiceController : ThexAppController
    {
        private readonly ITaxHub _taxHubAppService;

        public InvoiceController(ITaxHub taxHubAppService,
                                 IApplicationUser applicationUser) : base(applicationUser)
        {
            _taxHubAppService = taxHubAppService;
        }

        [HttpPost]
        [ProducesResponseType(typeof(InvoiceResponseDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Send([FromBody]List<InvoiceIntegrationDto> invoiceListDto)
        {
            var response = await _taxHubAppService.Send(invoiceListDto);

            return CreateResponseOnPost(response, EntityNames.TaxRules);
        }

        [HttpPost("status/{invoiceType}")]
        [AllowAnonymous]
        [ValidateApiKeyFilter]
        [ProducesResponseType(typeof(InvoiceResponseDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> UpdateInvoiceResponse([FromBody]InvoiceIntegrationResponseDto invoiceDto, int invoiceType)
        {
            var response = await _taxHubAppService.UpdateInvoiceResponse(invoiceDto, invoiceType);

            return CreateResponseOnPost(response, EntityNames.TaxRules);
        }

        [HttpPost("cancel/{invoiceType}")]
        [ProducesResponseType(typeof(InvoiceCancelResponseDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Cancel([FromBody] InvoiceIntegrationDto invoiceDto, int invoiceType)
        {
            var response = await _taxHubAppService.Cancel(invoiceDto, invoiceType);

            return CreateResponseOnDelete(null, EntityNames.TaxRules);
        }

        [HttpGet("invoiceId/{id}")]
        [ProducesResponseType(typeof(InvoiceCancelResponseDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Get(Guid id)
        {
            var response = await _taxHubAppService.GetIntegratorId(id);

            return CreateResponseOnGet(response, EntityNames.TaxRules);
        }

        [HttpPost("proforma")]
        [ProducesResponseType(typeof(InvoiceResponseDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> SendProforma([FromBody]ProformaDto proformaDto)
        {
            var response = await _taxHubAppService.SendProforma(proformaDto);

            return CreateResponseOnPost(response, EntityNames.TaxRules);
        }
    }
}
