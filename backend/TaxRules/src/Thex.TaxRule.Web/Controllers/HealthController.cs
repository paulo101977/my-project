﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Thex.TaxRule.Application.Brasil.Interfaces;
using Thex.TaxRule.Domain.Entities;
using Thex.TaxRule.Dto;

namespace Thex.TaxRule.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [AllowAnonymous]
    public class HealthController : TnfController
    {
        private readonly IUFAppService _ufAppService;

        public HealthController(IUFAppService UFAppService)
        {
            _ufAppService = UFAppService;
        }

        /// <summary>
        /// Verifica a integridade da aplicação
        /// </summary>
        [HttpGet("check")]
        [ProducesResponseType(typeof(bool), 200)]
        [ProducesResponseType(404)]
        //[ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> GetMostSelectedByUserId()
        {
            var response = await _ufAppService.GetAll(new GetAllUFDto());

            return CreateResponseOnGetAll(response != null ? true : false, EntityNames.Health);
        }
    }
}