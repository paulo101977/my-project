﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using Thex.Kernel;
using Thex.TaxRule.Base;
using Thex.TaxRule.Domain;
using Thex.TaxRule.Domain.Entities;
using Thex.TaxRule.Dto;
using Thex.TaxRule.Hub.Application.Interface;
using Tnf.AspNetCore.Mvc.Response;
using Tnf.Dto;

namespace Thex.TaxRule.Web.Controllers
{
    [Route(RouteConsts.TaxRuleRouteName)]
    [ApiController]
    public class TaxRuleController : ThexAppController
    {
        private readonly ITaxHub _taxHubAppService;

        public TaxRuleController(ITaxHub taxHubAppService,
                                 IApplicationUser applicationUser)
                                 : base(applicationUser)
        {
            _taxHubAppService = taxHubAppService;
        }

        /// <summary>
        /// Retorna uma regra tributária pelo identificador
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id}")]
        [ThexAuthorize("Tax_Get_GetById")]
        [ProducesResponseType(typeof(TaxRulesDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> GetById(string id)
        {
            var response = await _taxHubAppService.Get(Guid.Parse(id));

            return CreateResponseOnGet(response, EntityNames.TaxRules);
        }


        /// <summary>
        /// Retorna todas as regras tributárias cadastradas para os produtos
        /// </summary>
        /// <returns></returns>
        [HttpGet("products")]
        [ThexAuthorize("Tax_Get_GetAllForProducts")]
        [ProducesResponseType(typeof(IListDto<TaxRulesDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> GetAllForProducts([FromQuery]GetAllTaxRulesDto requestDto)
        {
            var response = await _taxHubAppService.GetAll((int)TaxRuleType.Products, requestDto);

            return CreateResponseOnGet(response, EntityNames.TaxRules);
        }

        /// <summary>
        /// Retorna todas as regras tributárias cadastradas para os serviços
        /// </summary>
        /// <returns></returns>
        [HttpGet("services")]
        [ThexAuthorize("Tax_Get_GetAllForServices")]
        [ProducesResponseType(typeof(IListDto<TaxRulesDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> GetAllForServices([FromQuery]GetAllTaxRulesDto requestDto)
        {
            var response = await _taxHubAppService.GetAll((int)TaxRuleType.Services, requestDto);

            return CreateResponseOnGet(response, EntityNames.TaxRules);
        }

        /// <summary>
        /// Registra uma nova regra tributária
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ThexAuthorize("Tax_Post_Post")]
        [AllowAnonymous]
        [ProducesResponseType(typeof(TaxRulesDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Post([FromBody]TaxRulesDto dto)
        {
            var response = await _taxHubAppService.Create(dto);

            return CreateResponseOnPost(response, EntityNames.TaxRules);
        }

        /// <summary>
        /// Ativa ou desativa uma regra tributária
        /// </summary>
        /// <returns></returns>
        [HttpPatch("{id}/toggleactivation")]
        [ThexAuthorize("Tax_Patch_ToggleActivation")]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> ToggleActivation(string id)
        {
            await _taxHubAppService.Toggle(Guid.Parse(id));

            return CreateResponseOnPatch(null, EntityNames.TaxRules);
        }

        /// <summary>
        /// Retorna a tributação de um item específico
        /// </summary>
        /// <param name="propertyUUId">Identificador do hotel</param>
        /// <param name="requestDto">Demais parâmetros</param>
        /// <example>
        /// Nos demais parâmetros da consulta:
        /// TwoLetterUf - Obrigatório quando é Brasil, identifica a UF de tributação
        /// --------------------------
        /// BarCode - Código de barras
        /// Ncm - Código NCM
        /// Nbs - Código NBS
        /// --------------------------
        ///
        /// BarCode ou Ncm obrigatórios para buscar uma tributação de produtos
        /// NBS obrigatório para buscar uma tributação de serviços
        /// </example>
        /// <returns></returns>
        [HttpGet("tributes/{propertyId}")]
        [ThexAuthorize("Tax_Get_GetTributes")]
        [ProducesResponseType(typeof(TaxRulesDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> GetTributes(string propertyUUId, [FromQuery]GetAllTaxRulesWithParamsDto requestDto)
        {
            var response = await _taxHubAppService.GetTribute(Guid.Parse(propertyUUId), requestDto);

            return CreateResponseOnGet(response, EntityNames.TaxRules);
        }
    }
}
