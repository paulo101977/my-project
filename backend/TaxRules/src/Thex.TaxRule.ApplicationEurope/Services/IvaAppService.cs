using System.Threading.Tasks;
using Thex.Kernel;
using Thex.TaxRule.Dto;
using Thex.TaxRule.Infra.Interfaces.ReadRepositories;
using Tnf.Dto;
using Tnf.Notifications;
using Thex.TaxRule.ApplicationEurope.Services;
using Thex.TaxRule.ApplicationEurope.Interfaces;
using Thex.TaxRule.Domain.Entities;

namespace Thex.TaxRule.Application.Brasil.Services
{
    public class IvaAppService : ApplicationServiceBase<IvaDto, Iva>, IIvaAppService
    {
        private readonly IApplicationUser _applicationUser;
        private readonly IIvaReadRepository _ivaReadRepository;

        public IvaAppService(IApplicationUser applicationUser,
                              INotificationHandler notificationHandler,
                              IIvaReadRepository ivaReadRepository)
                              : base(notificationHandler, null)
        {
            _applicationUser = applicationUser;
            _ivaReadRepository = ivaReadRepository;
        }

        public Task<IListDto<IvaDto>> GetAll(GetAllIvaDto getAllIvaDto)
        {
            getAllIvaDto.IsActive = true;
            return _ivaReadRepository.GetAll(getAllIvaDto);
        }

        public override Iva GeneratedEntity(IvaDto dto)
        {
            throw new System.NotImplementedException();
        }
    }
}