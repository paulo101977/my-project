using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Thex.Common;
using Thex.Common.Dto.InvoiceIntegration;
using Thex.Common.Enumerations;
using Thex.Common.Helpers;
using Thex.Dto.Integration;
using Thex.GenericLog;
using Thex.Kernel;
using Thex.TaxRule.ApplicationEurope.Adapters;
using Thex.TaxRule.ApplicationEurope.Interfaces;
using Thex.TaxRule.ApplicationEurope.Services.Integration;
using Thex.TaxRule.Domain.Enumerations;
using Thex.TaxRule.Dto;
using Thex.TaxRule.Dto.Dto.Integration.Response;
using Thex.TaxRule.Dto.Dto.PT.InvoiceXpressIntegration;
using Thex.TaxRule.Dto.Dto.PT.InvoiceXpressIntegration.Proforma;
using Thex.TaxRule.Dto.InvoiceXpressIntegration;
using Thex.TaxRule.Infra.Interfaces;
using Thex.TaxRule.Infra.Interfaces.ReadRepositories;
using Thex.TaxRule.Infra.Repositories;
using Tnf.Dto;
using Tnf.Localization;
using Tnf.Notifications;
using Tnf.Repositories.Uow;

namespace Thex.TaxRule.ApplicationEurope.Services
{
    public class TaxRulesAppServiceEurope : ApplicationServiceBase<TaxRulesDto, Domain.Entities.TaxRule>, ITaxRulesAppServiceEurope
    {
        private readonly string _defaultParams = "Parametros n�o informados: ";
        private readonly string _unitMeasurementProductDefault = "unit";
        private readonly string _unitMeasurementServiceDefault = "service";
        private readonly string _enotasIntegrationIndexName = "enotas-log";
        private readonly string _ivaDescription = "Isento";
        private readonly string _taxExemptionCode = "M99";

        private readonly IApplicationUser _applicationUser;
        private readonly IConfiguration _configuration;
        private readonly INotificationHandler _notificationHandler;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IPTTaxRuleAdapter _taxRulesAdapter;
        private readonly ITaxRulesRepository _taxRulesRepository;
        private readonly ITaxRulesReadRepository _taxRulesReadRepository;
        private readonly IFacturaInvoiceRepository _facturaInvoiceRepository;
        private readonly IFacturaInvoiceReadRepository _facturaInvoiceReadRepository;
        private readonly IRetainedClientReadRepository _retainedClientReadRepository;
        private readonly IGenericLogHandler _genericLog;
        private readonly ILocalizationManager _localizationManager;
        private readonly ITaxRulesPTReadRepository _taxRulesPTReadRepository;
        private readonly ICreditNoteInvoiceRepository _creditNoteInvoiceRepository;
        private readonly ICreditNoteInvoiceReadRepository _creditNoteInvoiceReadRepository;

        public TaxRulesAppServiceEurope(INotificationHandler notificationHandler,
                          ITaxRulesRepository taxRulesRepository,
                          IUnitOfWorkManager unitOfWorkManager,
                          IPTTaxRuleAdapter taxRulesAdapter,
                          ITaxRulesReadRepository taxRulesReadRepository,
                          IConfiguration configuration,
                          IApplicationUser applicationUser,
                          IFacturaInvoiceRepository facturaInvoiceRepository,
                          IFacturaInvoiceReadRepository facturaInvoiceReadRepository,
                          IRetainedClientReadRepository retainedClientReadRepository,
                          IGenericLogHandler genericLogHandler,
                          ILocalizationManager localizationManager,
                          ITaxRulesPTReadRepository taxRulesPTReadRepository,
                          ICreditNoteInvoiceRepository creditNoteInvoiceRepository,
                          ICreditNoteInvoiceReadRepository creditNoteInvoiceReadRepository
                          ) : base(notificationHandler, taxRulesRepository)
        {
            _localizationManager = localizationManager;
            _genericLog = genericLogHandler;
            _unitOfWorkManager = unitOfWorkManager;
            _taxRulesAdapter = taxRulesAdapter;
            _notificationHandler = notificationHandler;
            _taxRulesRepository = taxRulesRepository;
            _taxRulesReadRepository = taxRulesReadRepository;
            _configuration = configuration;
            _applicationUser = applicationUser;
            _facturaInvoiceRepository = facturaInvoiceRepository;
            _facturaInvoiceReadRepository = facturaInvoiceReadRepository;
            _retainedClientReadRepository = retainedClientReadRepository;
            _taxRulesPTReadRepository = taxRulesPTReadRepository;
            _creditNoteInvoiceRepository = creditNoteInvoiceRepository;
            _creditNoteInvoiceReadRepository = creditNoteInvoiceReadRepository;
        }

        public async Task<InvoiceResponseDto> Send(List<InvoiceIntegrationDto> invoiceListDto)
        {

            if (_notificationHandler.HasNotification())
                return InvoiceResponseDto.NullInstance;

            if (_notificationHandler.HasNotification())
                return InvoiceResponseDto.NullInstance;

            var invoice = await GenetateInvoiceIntegration(invoiceListDto);

            return (invoice != null ? invoice : InvoiceResponseDto.NullInstance);
        }

        public async Task UpdateInvoiceResponse(InvoiceIntegrationResponseDto invoiceResponse, int invoiceType)
        {
            switch (EnumHelper.GetEnumValue<BillingInvoiceTypeEnum>(invoiceType.ToString()))
            {
                case BillingInvoiceTypeEnum.Factura:
                    {
                        await UpdateFactura(invoiceResponse);
                        break;
                    }
                default:
                    break;
            }

            if (_genericLog.HasGenericLog())
                _genericLog.Commit(_enotasIntegrationIndexName);
        }

        private async Task UpdateFactura(InvoiceIntegrationResponseDto invoiceResponse)
        {
            var facturaInvoice = await _facturaInvoiceReadRepository.GetServiceInvoice(invoiceResponse.InternalId);

            if (facturaInvoice == null)
                return;

            facturaInvoice.ResponseDetails = new InvoiceXpressIntegrationResponseDetailsDto(invoiceResponse);

            await _facturaInvoiceRepository.ChangeAsync(facturaInvoice);

            _genericLog.AddLog(_genericLog.DefaultBuilder
               .WithMessage(_localizationManager.GetString(AppConsts.LocalizationSourceName, TaxRuleEnum.Message.UpdateInvoice.ToString()))
               .WithDetailedMessage(JsonConvert.SerializeObject(invoiceResponse))
               .AsError().Build());
        }

        private async Task<InvoiceResponseDto> GenetateInvoiceIntegration(List<InvoiceIntegrationDto> invoiceListDto)
        {
            var ret = InvoiceResponseDto.NullInstance;

            if (invoiceListDto.Any(x => x.ServiceInvoice != null && x.ServiceInvoice.Count > 0) ||
                invoiceListDto.Any(x => x.ProductInvoice != null && x.ProductInvoice.Count > 0))
                ret = await CreateAndServiceOrProductInvoice(invoiceListDto);

            if (_notificationHandler.HasNotification())
                ret = await Task.FromResult(InvoiceResponseDto.NullInstance);

            return ret;
        }

        private async Task<ProformaResponseDto> GenerateProforma(ProformaDto proforma)
        {
            var ret = ProformaResponseDto.NullInstance;

                ret = await CreateProforma(proforma);

            if (_notificationHandler.HasNotification())
                ret = await Task.FromResult(ProformaResponseDto.NullInstance);

            return ret;
        }

        private async Task<InvoiceResponseDto> CreateAndServiceOrProductInvoice(List<InvoiceIntegrationDto> invoiceListDto)
        {
            InvoiceResponseDto invoiceService = null;

            foreach (var factura in invoiceListDto)
            {
                switch (EnumHelper.GetEnumValue<EnvironmentEnum>(_configuration.GetValue<string>("CloudEnvironment"), true))
                {
                    case EnvironmentEnum.Production:
                        break;
                    case EnvironmentEnum.Development:
                    case EnvironmentEnum.BugFix:
                    case EnvironmentEnum.Test:
                    case EnvironmentEnum.UAT:
                    case EnvironmentEnum.Staging:
                    default:
                        {
                            factura.IntegrationCode = _configuration.GetValue<string>("InvoiceXpressIntegrationCode");
                            factura.TokenClient = _configuration.GetValue<string>("InvoiceXpressTokenClient");
                            break;
                        }
                }

                switch (EnumHelper.GetEnumValue<BillingInvoiceTypeEnum>(factura.ExternalTypeId, true))
                {
                    case BillingInvoiceTypeEnum.Factura:
                        return await GenerateFacturaInvoice(factura);
                    case BillingInvoiceTypeEnum.CreditNoteTotal:
                        return await GenerateCreditNoteInvoice(factura);
                    case BillingInvoiceTypeEnum.CreditNotePartial:
                        return await GenerateCreditNotePartialInvoice(factura);
                    default:
                        return InvoiceResponseDto.NullInstance;
                }
            }

            return invoiceService;
        }

        private async Task<ProformaResponseDto> CreateProforma(ProformaDto proforma)
        {
            switch (EnumHelper.GetEnumValue<EnvironmentEnum>(_configuration.GetValue<string>("CloudEnvironment"), true))
            {
                case EnvironmentEnum.Production:
                    break;
                case EnvironmentEnum.Development:
                case EnvironmentEnum.BugFix:
                case EnvironmentEnum.Test:
                case EnvironmentEnum.UAT:
                case EnvironmentEnum.Staging:
                default:
                    {
                        proforma.IntegrationCode = _configuration.GetValue<string>("InvoiceXpressIntegrationCode");
                        proforma.TokenClient = _configuration.GetValue<string>("InvoiceXpressTokenClient");
                        break;
                    }

            }

            return await GenerateProformaInvoice(proforma);
        }

        private async Task<InvoiceResponseDto> GenerateCreditNotePartialInvoice(InvoiceIntegrationDto creditNote)
        {
            var invoice = await _facturaInvoiceReadRepository.GetServiceInvoiceByExternalInvoiceId(creditNote.FacturaReferenceId);

            return await GenerateCreditNoteInvoice(creditNote, GetPartialCreditNoteAmount(invoice.Amount, (creditNote.ProductInvoice.Sum(p => p.Amount) +
                                                                                                           creditNote.ServiceInvoice.Sum(s => s.Amount))));
        }

        private decimal GetPartialCreditNoteAmount(decimal oldValue, decimal newValue)
        {
            if (oldValue - newValue < 0)
            {
                _notificationHandler.Raise(_notificationHandler.DefaultBuilder
                           .WithMessage(AppConsts.LocalizationSourceName, TaxRuleEnum.Error.VerifyParams)
                           .WithDetailedMessage(_defaultParams)
                           .Build());
                return 0m;
            }

            return oldValue - newValue;
        }

        private async Task<InvoiceResponseDto> GenerateCreditNoteInvoice(InvoiceIntegrationDto creditNote, decimal partialPaymentValue = 0m)
        {
            var creditNoteInvoice = new CreditNoteInvoice(DateTime.Now.ToShortDateString(),
                                                          DateTime.Now.ToShortDateString(),
                                                          creditNote.IntegrationCode,
                                                          creditNote.TokenClient,
                                                          (creditNote.ProductInvoice.Sum(x => x.Amount) +
                                                          creditNote.ServiceInvoice.Sum(x => x.Amount)),
                                                          creditNote.ExternalId,
                                                          creditNote.FacturaReferenceId,
                                                          creditNote.FacturaSeriesId,
                                                          GetObservationsInCreditNote(creditNote));

            if (creditNote.Client != null)
                creditNoteInvoice.PersonDetails = GeneratePersonDetails(creditNote);

            if (creditNote.ProductInvoice != null && creditNote.ProductInvoice.Count > 0)
                creditNoteInvoice.ItensDetails.AddRange(GenerateProductDetails(creditNote.ProductInvoice, creditNote.PropertyUUId));

            if (creditNote.ServiceInvoice != null && creditNote.ServiceInvoice.Count > 0)
                creditNoteInvoice.ItensDetails.AddRange(GenerateServiceDetails(creditNote.ServiceInvoice, creditNote.PropertyUUId));

            if (!creditNoteInvoice.ItensDetails.FindAll(x => x.TaxDetails != null && x.TaxDetails.TaxName.Contains(_ivaDescription)).IsNullOrEmpty())
                creditNoteInvoice.TaxExemption = _taxExemptionCode;

            try
            {
                var invoiceService = await new InvoiceService(_genericLog,
                                                          _localizationManager,
                                                          _notificationHandler).SendCreditNoteAsync(creditNoteInvoice,
                                                                                                    GetCreditNoteUrl(creditNoteInvoice),
                                                                                                    GetChangeStateUrl(creditNoteInvoice),
                                                                                                    GetCancelPaymentUrl(creditNoteInvoice, creditNote.FacturaPaymentReferenceId),
                                                                                                    GetPaymentUrl(creditNoteInvoice: creditNoteInvoice),
                                                                                                    partialPaymentValue);

                if (invoiceService != null && !string.IsNullOrEmpty(invoiceService.Id))
                {
                    if (_creditNoteInvoiceReadRepository.GetCreditNoteInvoice(creditNoteInvoice.Id).Result == null)
                    {
                        creditNoteInvoice.FacturaId = invoiceService.ExternalId;
                        await _creditNoteInvoiceRepository.PostAsync(creditNoteInvoice);
                    }
                    else
                        await _creditNoteInvoiceRepository.ChangeAsync(creditNoteInvoice);

                    var factura = await _facturaInvoiceReadRepository.GetServiceInvoiceByExternalInvoiceId(creditNote.FacturaReferenceId);

                    if (factura != null)
                    {
                        factura.Amount = (creditNoteInvoice.Amount - factura.Amount) * -1;
                        await _facturaInvoiceRepository.ChangeAsync(factura);
                    }
                }

                return invoiceService;
            }
            catch (Exception ex)
            {
                _notificationHandler.Raise(_notificationHandler.DefaultBuilder
                           .WithMessage(AppConsts.LocalizationSourceName, TaxRuleEnum.Error.TributeNotFound)
                           .WithDetailedMessage(ex.Message)
                           .Build());
            }

            return InvoiceResponseDto.NullInstance;
        }

        private string GetObservationsInCreditNote(InvoiceIntegrationDto creditNote)
        {
            return string.Concat(string.IsNullOrEmpty(creditNote.Observations) ? $"Motivo: {creditNote.ReasonName}"
                : $"Motivo: {creditNote.ReasonName} | {creditNote.Observations}");
        }

        private async Task<InvoiceResponseDto> GenerateFacturaInvoice(InvoiceIntegrationDto factura)
        {
            var facturaInvoice = new FacturaInvoice(DateTime.Now.ToShortDateString(),
                                                                    DateTime.Now.ToShortDateString(),
                                                                    factura.IntegrationCode,
                                                                    factura.TokenClient,
                                                                    factura.ProductInvoice.Sum(x => x.Amount) +
                                                                    factura.ServiceInvoice.Sum(x => x.Amount),
                                                                    factura.ExternalId,
                                                                    factura.FacturaSeriesId,
                                                                    factura.Observations);

            if (factura.Client != null)
            {
                facturaInvoice.PersonDetails = GeneratePersonDetails(factura);

                int.TryParse(facturaInvoice.PersonDetails.Id, out int clientId);

                if (clientId != default(int))
                {
                    var clientInvoiceService = await new InvoiceService(_genericLog,
                                                             _localizationManager,
                                                             _notificationHandler).UpdateClientInformationsAsync(facturaInvoice.PersonDetails,
                                                                                                    GetClientUrl(facturaInvoice));
                }
            }

            if (factura.ProductInvoice != null && factura.ProductInvoice.Count > 0)
                facturaInvoice.ItensDetails.AddRange(GenerateProductDetails(factura.ProductInvoice, factura.PropertyUUId));

            if (factura.ServiceInvoice != null && factura.ServiceInvoice.Count > 0)
                facturaInvoice.ItensDetails.AddRange(GenerateServiceDetails(factura.ServiceInvoice, factura.PropertyUUId));

            if (!facturaInvoice.ItensDetails.FindAll(x => x.TaxDetails != null && x.TaxDetails.TaxName.Contains(_ivaDescription)).IsNullOrEmpty())
                facturaInvoice.TaxExemption = _taxExemptionCode;

            try
            {
                var invoiceService = await new InvoiceService(_genericLog,
                                                              _localizationManager,
                                                              _notificationHandler).SendFacturaAsync(facturaInvoice,
                                                                                                     GetInvoiceUrl(facturaInvoice),
                                                                                                     GetChangeStateUrl(facturaInvoice),
                                                                                                     GetPaymentUrl(facturaInvoice));

                if (invoiceService != null && !string.IsNullOrEmpty(invoiceService.Id))
                {
                    if (facturaInvoice.PersonDetails != null && invoiceService.Client != null)
                        facturaInvoice.PersonDetails.Id = invoiceService.Client.Id;

                    if (_facturaInvoiceReadRepository.GetServiceInvoice(facturaInvoice.Id).Result == null)
                    {
                        facturaInvoice.FacturaId = invoiceService.ExternalId;
                        await _facturaInvoiceRepository.PostAsync(facturaInvoice);
                    }
                    else
                        await _facturaInvoiceRepository.ChangeAsync(facturaInvoice);
                }

                return invoiceService;
            }
            catch (Exception ex)
            {
                _notificationHandler.Raise(_notificationHandler.DefaultBuilder
                           .WithMessage(AppConsts.LocalizationSourceName, TaxRuleEnum.Error.TributeNotFound)
                           .WithDetailedMessage(ex.Message)
                           .Build());
            }

            return InvoiceResponseDto.NullInstance;
        }

        private async Task<ProformaResponseDto> GenerateProformaInvoice(ProformaDto proforma)
        {
            foreach (var item in proforma.Items)
            {
                var tax = GetTribute(proforma.PropertyUUId, new GetAllEuropeTaxRulesWithParamsDto()
                {
                    ServiceId = item.ServiceId.ToString()
                }).Result;

                if (tax == null)
                {
                    _notificationHandler.Raise(_notificationHandler.DefaultBuilder
                                           .WithMessage(AppConsts.LocalizationSourceName, TaxRuleEnum.Error.TributeNotFound)
                                           .WithDetailedMessage(AppConsts.LocalizationSourceName, TaxRuleEnum.Error.TributeNotFound)
                                           .Build());
                    return null;
                }

                item.UnitPrice = GetServiceAmountWithTax(item.UnitPrice, tax.TaxPercentual);
                item.UnitMeasurement = _unitMeasurementServiceDefault;
                item.TaxDetails = new TaxInvoiceXpressDto()
                {
                    TaxName = tax.Name.Trim()
                };
            }

            if (!proforma.Items.FindAll(x => x.TaxDetails != null && x.TaxDetails.TaxName.Contains(_ivaDescription)).IsNullOrEmpty())
                proforma.TaxExemption = _taxExemptionCode;

            try
            {
                var invoiceService = await new InvoiceService(_genericLog,
                                                              _localizationManager,
                                                              _notificationHandler).SendProformaAsync(proforma,
                                                                                                     GetProformaUrl(proforma),
                                                                                                     GetChangeProformaStateUrl(proforma));

                return invoiceService;
            }
            catch (Exception ex)
            {
                _notificationHandler.Raise(_notificationHandler.DefaultBuilder
                           .WithMessage(AppConsts.LocalizationSourceName, TaxRuleEnum.Error.TributeNotFound)
                           .WithDetailedMessage(ex.Message)
                           .Build());
            }

            return ProformaResponseDto.NullInstance;
        }

        private void ValidateTaxDetails(TaxRulesDto databaseTax)
        {
            var requiredParamsList = new List<string>();

            if (string.IsNullOrEmpty(databaseTax.CestCode))
                requiredParamsList.Add(nameof(databaseTax.CestCode));

            if (requiredParamsList.Count > 0)
                _notificationHandler.Raise(_notificationHandler.DefaultBuilder
                   .WithMessage(AppConsts.LocalizationSourceName, TaxRuleEnum.Error.InvoiceRequiredParams)
                   .WithDetailedMessage(string.Concat(_defaultParams, String.Join(", ", requiredParamsList.ToArray())))
                   .Build());
        }

        private TaxInvoiceXpressDto GenerateProductTaxDetails(TaxRulesDto databaseTax)
        {
            var taxDetail = new TaxInvoiceXpressDto();

            return taxDetail;
        }

        private List<ItensInvoiceXpressDto> GenerateServiceDetails(List<IntegrationServiceInvoiceDTO> serviceList, string propertyUUId)
        {
            var ret = new List<ItensInvoiceXpressDto>();

            foreach (var serviceInvoice in serviceList)
            {
                var databaseTax = GetTribute(Guid.Parse(propertyUUId), new GetAllEuropeTaxRulesWithParamsDto()
                {
                    ServiceId = serviceInvoice.ServiceId
                }).Result;

                if (databaseTax == null)
                {
                    _notificationHandler.Raise(_notificationHandler.DefaultBuilder
                                           .WithMessage(AppConsts.LocalizationSourceName, TaxRuleEnum.Error.TributeNotFound)
                                           .WithDetailedMessage(AppConsts.LocalizationSourceName, TaxRuleEnum.Error.TributeNotFound)
                                           .Build());
                    return null;
                }

                ret.Add(new ItensInvoiceXpressDto()
                {
                    Name = serviceInvoice.Description,
                    Description = serviceInvoice.Description,
                    UnitPrice = this.GetServiceAmountWithTax(serviceInvoice.Amount, databaseTax.TaxPercentual),
                    Quantity = 1,
                    UnitMeasurement = _unitMeasurementServiceDefault,
                    TaxDetails = new TaxInvoiceXpressDto()
                    {
                        TaxName = databaseTax.Name.Trim()
                    }
                });
            }

            return ret;
        }

        private List<ItensInvoiceXpressDto> GenerateProductDetails(List<IntegrationProductInvoiceDTO> productInvoice, string propertyUUId)
        {
            var itensList = new List<ItensInvoiceXpressDto>();

            foreach (var product in productInvoice)
            {
                var databaseTax = GetTribute(Guid.Parse(propertyUUId), new GetAllEuropeTaxRulesWithParamsDto()
                {
                    ProductId = product.ProductId
                }).Result;

                // TODO: Ajustar motivo do erro
                if (databaseTax == null)
                {
                    _notificationHandler.Raise(_notificationHandler.DefaultBuilder
                                           .WithMessage(AppConsts.LocalizationSourceName, TaxRuleEnum.Error.TributeNotFound)
                                           .WithDetailedMessage(AppConsts.LocalizationSourceName, TaxRuleEnum.Error.TributeNotFound)
                                           .Build());
                    return null;
                }

                itensList.Add(new ItensInvoiceXpressDto()
                {
                    Name = product.Description,
                    Description = product.Description,
                    UnitPrice = this.GetServiceAmountWithTax(product.Amount, databaseTax.TaxPercentual),
                    Quantity = product.Quantity,
                    UnitMeasurement = _unitMeasurementProductDefault,
                    TaxDetails = new TaxInvoiceXpressDto()
                    {
                        TaxName = databaseTax.Name.Trim()
                    }
                });
            }

            return itensList;
        }

        private decimal GetServiceAmountWithTax(decimal amount, decimal taxPercentual)
        {
            if (amount < 0)
                amount = amount * -1;

            var amoutWithoutTax = amount - Math.Round((amount / ((taxPercentual / 100) + 1)), 4);

            return Math.Round(amount - amoutWithoutTax, 4);
        }

        private PersonInvoiceXpressDto GeneratePersonDetails(InvoiceIntegrationDto serviceInvoice)
        {
            var clientCode = serviceInvoice.Client.Id ?? string.Empty;

            var facturaInvoice = _facturaInvoiceReadRepository.GetServiceInvoiceByClientCode(clientCode).Result;

            var ret = new PersonInvoiceXpressDto()
            {
                Id = facturaInvoice?.PersonDetails?.Id,
                Code = serviceInvoice.Client?.Id,
                Name = serviceInvoice.Client?.Name,
                Email = serviceInvoice.Client?.Email,
                FiscalId = serviceInvoice.Client?.Document
            };

            if (serviceInvoice.Client.Address != null)
            {
                ret.StreetName = serviceInvoice.Client.Address.StreetName;
                ret.City = serviceInvoice.Client.Address.City.Name;
                ret.PostalCode = serviceInvoice.Client.Address.PostalCode;
                ret.Country = serviceInvoice.Client.Address.Country;
            }

            return ret;
        }

        public Task<TaxRulesDto> GetTribute(Guid propertyUUid, GetAllEuropeTaxRulesWithParamsDto requestDto)
        {
            return _taxRulesPTReadRepository.GetTributes(propertyUUid, requestDto);
        }

        public override Domain.Entities.TaxRule GeneratedEntity(TaxRulesDto dto)
        {
            return null;
        }

        /// <summary>
        /// Valida se a regra tribut�ria foi cadastrada corretamente
        /// </summary>
        /// <param name="serviceInvoiceList">Lista de NBS</param>
        /// <param name="productInvoiceList">Lista de NCM ou C�digo de Barras</param>
        /// <returns></returns>
        public Task<bool> CheckExistTax(List<IntegrationServiceInvoiceDTO> serviceInvoiceList = null, List<IntegrationProductInvoiceDTO> productInvoiceList = null)
        {
            if (serviceInvoiceList != null && serviceInvoiceList.Count > 0)
                return _taxRulesReadRepository.ValidateExistServiceTax(serviceInvoiceList.Select(x => x.Nbs).ToList());

            if (productInvoiceList != null && productInvoiceList.Count > 0)
            {
                return _taxRulesReadRepository.ValidateExistProductTax(productInvoiceList.DistinctBy(p => new { p.Code }).Where(x => x.Barcode != null).Select(x => x.Barcode).ToList(),
                                                                       productInvoiceList.DistinctBy(p => new { p.Code }).Where(x => x.Ncm != null).Select(x => x.Ncm).ToList(),
                                                                       productInvoiceList.DistinctBy(p => new { p.Code }).Count());
            }

            return Task.FromResult(true);
        }

        private string GetCreditNoteUrl(CreditNoteInvoice integrationInvoice)
        {
            var url = $"{_configuration.GetValue<string>("InvoiceXpressBaseEndpoint")}";

            if (integrationInvoice.InvoiceType == "CreditNote")
                url = string.Concat(string.Format(url, integrationInvoice.IntegrationCode), "/credit_notes.json?", $"api_key={integrationInvoice.TokenClient}");
            else
                throw new NotImplementedException();

            return url;
        }

        private string GetInvoiceUrl(FacturaInvoice integrationInvoice)
        {
            var url = $"{_configuration.GetValue<string>("InvoiceXpressBaseEndpoint")}";

            if (integrationInvoice.InvoiceType == "Factura")
                url = string.Concat(string.Format(url, integrationInvoice.IntegrationCode), "/invoices.json?", $"api_key={integrationInvoice.TokenClient}");
            else
                throw new NotImplementedException();

            return url;
        }

        private string GetChangeStateUrl(CreditNoteInvoice integrationInvoice)
        {
            var url = $"{_configuration.GetValue<string>("InvoiceXpressBaseEndpoint")}";

            if (integrationInvoice.InvoiceType == "CreditNote")
                url = string.Concat(string.Format(url, integrationInvoice.IntegrationCode), "/credit_notes/{0}/change-state.json?", $"api_key={integrationInvoice.TokenClient}");
            else
                throw new NotImplementedException();

            return url;
        }

        private string GetProformaUrl(ProformaDto proformaDto)
        {
            var url = $"{_configuration.GetValue<string>("InvoiceXpressBaseEndpoint")}";

                url = string.Concat(string.Format(url, proformaDto.TokenClient), "/proformas.json?", $"api_key={proformaDto.IntegrationCode}");

            return url;
        }

        private string GetChangeProformaStateUrl(ProformaDto proformaDto)
        {
            var url = $"{_configuration.GetValue<string>("InvoiceXpressBaseEndpoint")}";

                url = string.Concat(string.Format(url, proformaDto.TokenClient), "/proformas/{0}/change-state.json?", $"api_key={proformaDto.IntegrationCode}");

            return url;
        }

        private string GetCancelPaymentUrl(CreditNoteInvoice integrationInvoice, string facturaPaymentReferenceId)
        {
            var url = $"{_configuration.GetValue<string>("InvoiceXpressBaseEndpoint")}";

            if (integrationInvoice.InvoiceType == "CreditNote")
                url = string.Concat(string.Format(url, integrationInvoice.IntegrationCode),
                                    string.Format("/receipts/{0}/change-state.json?", facturaPaymentReferenceId), $"api_key={integrationInvoice.TokenClient}");
            else
                throw new NotImplementedException();

            return url;
        }

        private string GetChangeStateUrl(FacturaInvoice integrationInvoice)
        {
            var url = $"{_configuration.GetValue<string>("InvoiceXpressBaseEndpoint")}";

            if (integrationInvoice.InvoiceType == "Factura")
                url = string.Concat(string.Format(url, integrationInvoice.IntegrationCode), "/invoices/{0}/change-state.json?", $"api_key={integrationInvoice.TokenClient}");
            else
                throw new NotImplementedException();

            return url;
        }

        private string GetPaymentUrl(FacturaInvoice facturaInvoice = null, CreditNoteInvoice creditNoteInvoice = null)
        {
            var url = $"{_configuration.GetValue<string>("InvoiceXpressBaseEndpoint")}";

            if (facturaInvoice != null && facturaInvoice.InvoiceType == "Factura")
                url = string.Concat(string.Format(url, facturaInvoice.IntegrationCode), "/documents/{0}/partial_payments.json?", $"api_key={facturaInvoice.TokenClient}");
            else
            if (creditNoteInvoice != null && creditNoteInvoice.InvoiceType == "CreditNote")
                url = string.Concat(string.Format(url, creditNoteInvoice.IntegrationCode), "/documents/{0}/partial_payments.json?", $"api_key={creditNoteInvoice.TokenClient}");
            else
                throw new NotImplementedException();

            return url;
        }

        private string GetClientUrl(FacturaInvoice integrationInvoice)
        {
            var url = $"{_configuration.GetValue<string>("InvoiceXpressBaseEndpoint")}";

            if (integrationInvoice.InvoiceType == "Factura")
                url = string.Concat(string.Format(url, integrationInvoice.IntegrationCode), $"/clients/{integrationInvoice.PersonDetails.Id}.json?", $"api_key={integrationInvoice.TokenClient}");
            else
                throw new NotImplementedException();

            return url;
        }

        public async Task<string> GetIntegratorIdById(Guid id)
        {
            if (id != null && !string.IsNullOrEmpty(id.ToString()))
            {
                var factura = await _facturaInvoiceReadRepository.GetServiceInvoice(id.ToString());

                if (factura != null && !string.IsNullOrEmpty(factura.FacturaId))
                    return factura.ResponseDetails.ExternalId;
                else
                    return await Task.FromResult(string.Empty);
            }
            else
                return await Task.FromResult(string.Empty);
        }

        public async Task<ProformaResponseDto> SendProforma(ProformaDto proforma)
        {
            if (_notificationHandler.HasNotification())
                return ProformaResponseDto.NullInstance;

            if (_notificationHandler.HasNotification())
                return ProformaResponseDto.NullInstance;

            var result = await GenerateProforma(proforma);

            return (result ?? ProformaResponseDto.NullInstance);
        }
    }
}
