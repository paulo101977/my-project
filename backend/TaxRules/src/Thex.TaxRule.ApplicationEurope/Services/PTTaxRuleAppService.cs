﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Polly;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Thex.Common;
using Thex.TaxRule.ApplicationEurope.Adapters;
using Thex.TaxRule.ApplicationEurope.Interfaces;
using Thex.TaxRule.Domain.Entities;
using Thex.TaxRule.Domain.Enumerations;
using Thex.TaxRule.Dto;
using Thex.TaxRule.Infra.Interfaces;
using Thex.TaxRule.Infra.Interfaces.ReadRepositories;
using Tnf.Notifications;
using Tnf.Repositories.Uow;

namespace Thex.TaxRule.ApplicationEurope.Services
{
    public class PTTaxRuleAppService : ApplicationServiceBase<PTTaxRuleDto, PTTaxRule>, IPTTaxRuleAppService
    {
        private readonly IPTTaxRuleAdapter _pTTaxRulesAdapter;
        private readonly IPTTaxRuleReadRepository _pTTaxRuleReadRepository;
        private readonly IPTTaxRuleRepository _pTTaxRuleRepository;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IConfiguration _configuration;
        private readonly IIvaAppService _ivaAppService;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly INotificationHandler _notificationHandler;
        private readonly IIvaReadRepository _ivaReadRepository;

        public PTTaxRuleAppService(INotificationHandler notificationHandler,
            IPTTaxRuleAdapter pTTaxRulesAdapter,
            IPTTaxRuleReadRepository pTTaxRuleReadRepository,
            IPTTaxRuleRepository pTTaxRuleRepository,
            IHttpContextAccessor httpContextAccessor,
            IConfiguration configuration,
            IIvaAppService ivaAppService,
            IUnitOfWorkManager unitOfWorkManager,
            IIvaReadRepository ivaReadRepository
            ) : base(notificationHandler, null)
        {
            _notificationHandler = notificationHandler;
            _pTTaxRulesAdapter = pTTaxRulesAdapter;
            _pTTaxRuleReadRepository = pTTaxRuleReadRepository;
            _pTTaxRuleRepository = pTTaxRuleRepository;
            _httpContextAccessor = httpContextAccessor;
            _configuration = configuration;
            _ivaAppService = ivaAppService;
            _unitOfWorkManager = unitOfWorkManager;
            _ivaReadRepository = ivaReadRepository;
        }

        public async Task<List<PTTaxRuleServiceResponseDto>> GetAllTaxRulesServices(GetAllPTTaxRuleServiceDto getAllTaxRuleService)
        {
            var responseDtoList = new List<PTTaxRuleServiceResponseDto>();

            using (var httpClient = new HttpClient())
            {
                var response = await GetAllServices(httpClient);

                if (response.IsSuccessStatusCode)
                {
                    var message = await response.Content.ReadAsStringAsync();

                    if (!string.IsNullOrEmpty(message) || !string.IsNullOrWhiteSpace(message))
                    {
                        var billingItemExistingDtoList = JsonConvert.DeserializeObject<List<BillingItemDto>>(message);

                        var ivaIdSelected = getAllTaxRuleService.IvaId;

                        var ivaIdList = (await _ivaAppService.GetAll(new GetAllIvaDto())).Items.Select(i => i.Id).ToList();

                        if (IvaIdWasSelected(ivaIdSelected))
                            ivaIdList.Remove(ivaIdSelected.Value);

                        var ptTaxRuleOfIvaSelectedList = new List<PTTaxRuleDto>();

                        if (IvaIdWasSelected(ivaIdSelected))
                            ptTaxRuleOfIvaSelectedList = _pTTaxRuleReadRepository.GetAllByIvaIdList(new List<Guid> { ivaIdSelected.Value }).ToList();

                        var pTTaxRuleExistingList = _pTTaxRuleReadRepository.GetAllByIvaIdList(ivaIdList);

                        foreach (var billingItemDto in billingItemExistingDtoList)
                        {
                            var responseDto = new PTTaxRuleServiceResponseDto
                            {
                                ServiceId = billingItemDto.Id,
                                ServiceName = billingItemDto.BillingItemName
                            };

                            if ((IvaIdWasSelected(ivaIdSelected) && ptTaxRuleOfIvaSelectedList.IsNullOrEmpty())
                                || pTTaxRuleExistingList.Select(p => p.ServiceId).Contains(billingItemDto.Id))
                                continue;
                            else if (IvaIdWasSelected(ivaIdSelected) && !ptTaxRuleOfIvaSelectedList.Select(p => p.ServiceId).Contains(billingItemDto.Id))
                                continue;
                            else if (ptTaxRuleOfIvaSelectedList.Select(p => p.ServiceId).Contains(billingItemDto.Id))
                            {
                                responseDto.IvaId = ptTaxRuleOfIvaSelectedList.FirstOrDefault().IvaId;
                                var ptTaxRuleOfIvaByServiceId = ptTaxRuleOfIvaSelectedList.FirstOrDefault(r => responseDto.ServiceId == r.ServiceId);
                                responseDto.ExemptionCode = ptTaxRuleOfIvaByServiceId == null ? null : ptTaxRuleOfIvaByServiceId.ExemptionCode;
                            }
                                

                            responseDtoList.Add(responseDto);
                        }
                    }
                }
                else
                    return null;
            }

            return responseDtoList;
        }

        public async Task<List<PTTaxRuleProductResponseDto>> GetAllTaxRulesProducts(GetAllPTTaxRuleProductDto getAllPTTaxRuleProduct)
        {
            var responseDtoList = new List<PTTaxRuleProductResponseDto>();

            using (var httpClient = new HttpClient())
            {
                HttpResponseMessage response = await GetAllProducts(httpClient);

                if (response.IsSuccessStatusCode)
                {
                    var message = await response.Content.ReadAsStringAsync();

                    if (!string.IsNullOrEmpty(message) || !string.IsNullOrWhiteSpace(message))
                    {
                        var productExistingDtoList = JsonConvert.DeserializeObject<List<ProductDto>>(message);

                        var ivaIdSelected = getAllPTTaxRuleProduct.IvaId;

                        var ivaIdList = (await _ivaAppService.GetAll(new GetAllIvaDto())).Items.Select(i => i.Id).ToList();

                        if (IvaIdWasSelected(ivaIdSelected))
                            ivaIdList.Remove(ivaIdSelected.Value);

                        var ptTaxRuleOfIvaSelectedList = new List<PTTaxRuleDto>();

                        if (IvaIdWasSelected(ivaIdSelected))
                            ptTaxRuleOfIvaSelectedList = _pTTaxRuleReadRepository.GetAllByIvaIdList(new List<Guid> { ivaIdSelected.Value });

                        var pTTaxRuleExistingList = _pTTaxRuleReadRepository.GetAllByIvaIdList(ivaIdList);

                        foreach (var productDto in productExistingDtoList)
                        {
                            var responseDto = new PTTaxRuleProductResponseDto
                            {
                                ProductId = productDto.Id,
                                ProductName = productDto.ProductName
                            };

                            if ((IvaIdWasSelected(ivaIdSelected) && ptTaxRuleOfIvaSelectedList.IsNullOrEmpty())
                                 || pTTaxRuleExistingList.Select(p => p.ProductId).Contains(productDto.Id))
                                continue;
                            else if (IvaIdWasSelected(ivaIdSelected) && !ptTaxRuleOfIvaSelectedList.Select(p => p.ProductId).Contains(productDto.Id))
                                continue;
                            else if (ptTaxRuleOfIvaSelectedList.Select(p => p.ProductId).Contains(productDto.Id))
                                responseDto.IvaId = ptTaxRuleOfIvaSelectedList.FirstOrDefault().IvaId;

                            responseDtoList.Add(responseDto);
                        }
                    }
                }
                else
                    return null;
            }

            return responseDtoList;
        }

        private async Task<HttpResponseMessage> GetAllServices(HttpClient httpClient)
        {
            var pmsEndpoint = new Uri(string.Concat(_configuration.GetValue<string>("PmsEndpoint"), $"/api/billingitem/getallbillingitemservice"));
            var token = _httpContextAccessor.HttpContext.Request.Headers.FirstOrDefault(x => x.Key.Equals("Authorization")).Value.ToString();
            httpClient.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", token);

            var response = await Policy
                                         .HandleResult<HttpResponseMessage>(message => !message.IsSuccessStatusCode)
                                         .WaitAndRetryAsync(0, i => TimeSpan.FromSeconds(120), (result, timeSpan, retryCount, context) =>
                                         {
                                             Console.Write($"Request failed with {result.Result.StatusCode}. Waiting {timeSpan} before next retry. Retry attempt {retryCount}");
                                         })
                                         .ExecuteAsync(async () => await httpClient.GetAsync(pmsEndpoint));
            return response;
        }

        private async Task<HttpResponseMessage> GetAllProducts(HttpClient httpClient)
        {
            var pdvEndpoint = new Uri(string.Concat(_configuration.GetValue<string>("PdvEndpoint"), $"/api/product/getallidandname"));
            var token = _httpContextAccessor.HttpContext.Request.Headers.FirstOrDefault(x => x.Key.Equals("Authorization")).Value.ToString();
            httpClient.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", token);

            var response = await Policy
                                         .HandleResult<HttpResponseMessage>(message => !message.IsSuccessStatusCode)
                                         .WaitAndRetryAsync(0, i => TimeSpan.FromSeconds(120), (result, timeSpan, retryCount, context) =>
                                         {
                                             Console.Write($"Request failed with {result.Result.StatusCode}. Waiting {timeSpan} before next retry. Retry attempt {retryCount}");
                                         })
                                         .ExecuteAsync(async () => await httpClient.GetAsync(pdvEndpoint));
            return response;
        }


        private bool IvaIdWasSelected(Guid? ivaIdSelected)
            => ivaIdSelected.HasValue && ivaIdSelected.Value != Guid.Empty;

        public void AssociateRange(PTTaxRuleAssociateDto pTTaxRuleAssociateDto)
        {
            var entityList = new List<PTTaxRule>();

            var ivaId = pTTaxRuleAssociateDto.IvaId;
            var iva = _ivaReadRepository.GetById(ivaId);

            var serviceIdList = pTTaxRuleAssociateDto.ServiceList.Select(s => s.ServiceId).ToList();
            var productIdLIst = pTTaxRuleAssociateDto.ProductList.Select(p => p.ProductId).ToList();

            var pTTaxRuleIsAssociatedInOtherIva = _pTTaxRuleReadRepository.AnyExceptInIvaId(ivaId, serviceIdList, productIdLIst);

            if (pTTaxRuleIsAssociatedInOtherIva)
            {
                _notificationHandler.Raise(_notificationHandler.DefaultBuilder
                   .WithMessage(AppConsts.LocalizationSourceName, PTTaxRule.EntityError.InvalidAssociationInIvaAndSerivesAndProducts)
                   .WithDetailedMessage(AppConsts.LocalizationSourceName, PTTaxRule.EntityError.InvalidAssociationInIvaAndSerivesAndProducts)
                   .Build());
                return;
            }

            if (iva.Tax == null && pTTaxRuleAssociateDto.ServiceList.Any(s => string.IsNullOrEmpty(s.ExemptionCode) || string.IsNullOrWhiteSpace(s.ExemptionCode)))
            {
                _notificationHandler.Raise(_notificationHandler.DefaultBuilder
                   .WithMessage(AppConsts.LocalizationSourceName, PTTaxRule.EntityError.IvaZeroWithoutExemptionCode)
                   .WithDetailedMessage(AppConsts.LocalizationSourceName, PTTaxRule.EntityError.IvaZeroWithoutExemptionCode)
                   .Build());
                return;
            }

            foreach (var service in pTTaxRuleAssociateDto.ServiceList)
                entityList.Add(_pTTaxRulesAdapter.Map(iva, service.ServiceId, service.ExemptionCode).Build());

            foreach (var product in pTTaxRuleAssociateDto.ProductList)
                entityList.Add(_pTTaxRulesAdapter.Map(iva, product.ProductId).Build());

            using (var uow = _unitOfWorkManager.Begin())
            {
                _pTTaxRuleRepository.DeleteAllByIvaId(pTTaxRuleAssociateDto.IvaId);
                _pTTaxRuleRepository.CreateRange(entityList);

                uow.Complete();
            }
        }

        public override PTTaxRule GeneratedEntity(PTTaxRuleDto dto)
        {
            throw new System.NotImplementedException();
        }
    }
}
