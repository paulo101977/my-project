﻿using Newtonsoft.Json;
using Polly;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Thex.Common;
using Thex.GenericLog;
using Thex.TaxRule.Domain.Enumerations;
using Thex.TaxRule.Dto.Dto.Integration.Error;
using Thex.TaxRule.Dto.Dto.Integration.Exceptions;
using Thex.TaxRule.Dto.Dto.Integration.Exceptions.Base;
using Thex.TaxRule.Dto.Dto.Integration.Response;
using Thex.TaxRule.Dto.Dto.PT.InvoiceXpressIntegration.Proforma;
using Thex.TaxRule.Dto.Dto.PT.InvoiceXpressIntegration.Response;
using Thex.TaxRule.Dto.InvoiceXpressIntegration;
using Thex.TaxRule.Dto.InvoiceXpressIntegration.Response;
using Tnf.Localization;
using Tnf.Notifications;

namespace Thex.TaxRule.ApplicationEurope.Services.Integration
{
    public class InvoiceService : IDisposable
    {
        private string _invoiceXpressIndexName = "invoicexpress-log";
        private string _internalErrorMessage = "TaxRule internal error occurred";
        private string _canceledMessage = "Wrong payment values";

        private string _finalizedState = "finalized";
        private string _canceledState = "canceled";
        private string _acceptState = "accept";

        private HttpClient _client;
        private IGenericLogHandler _genericLog;
        private ILocalizationManager _localizationManager;
        private INotificationHandler _notificationHandler;

        public InvoiceService(IGenericLogHandler genericLogHandler,
                              ILocalizationManager localizationManager,
                              INotificationHandler notificationHandler)
        {
            _notificationHandler = notificationHandler;
            _localizationManager = localizationManager;
            _genericLog = genericLogHandler;
            _client = new HttpClient();
            _client.DefaultRequestHeaders.Clear();
            _client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

        private void GenerateinvoicePostLog(string jsonLog)
        {
            if (!jsonLog.IsNullOrEmpty() && !Debugger.IsAttached)
                _genericLog.AddLog(_genericLog.DefaultBuilder
                   .WithMessage(_localizationManager.GetString(AppConsts.LocalizationSourceName, TaxRuleEnum.Message.SendInvoice.ToString()))
                   .WithDetailedMessage(jsonLog.Replace(@"\", ""))
                   .AsInformation().Build());
        }

        public async Task<InvoiceResponseDto> SendCreditNoteAsync(CreditNoteInvoice creditNoteInvoice,
                                                                  string urlSendInvoice, string urlChangeStatus,
                                                                  string urlCancelPayment, string urlRegisterPayment,
                                                                  decimal partialPaymentValue = 0m)
        {
            try
            {
                var jsonInvoice = JsonConvert.SerializeObject(new
                {
                    credit_note = creditNoteInvoice
                }, Formatting.Indented, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });

                GenerateinvoicePostLog(jsonInvoice);

                await this.CancelInvoicePaymentAsync(urlCancelPayment);

                if (_notificationHandler.HasNotification()) return InvoiceResponseDto.NullInstance;

                var response = await Policy
                               .HandleResult<HttpResponseMessage>(message => !message.IsSuccessStatusCode)
                               .WaitAndRetryAsync(1, i => TimeSpan.FromSeconds(15), (result, timeSpan, retryCount, context) =>
                               {
                                   Console.Write($"Request failed with {result.Result.StatusCode}. Waiting {timeSpan} before next retry. Retry attempt {retryCount}");
                               })
                               .ExecuteAsync(() => _client.PostAsync(urlSendInvoice, new StringContent(jsonInvoice, Encoding.UTF8, "application/json")));

                if (response.IsSuccessStatusCode)
                {
                    var payment = FacturaPaymentResponseDto.NullInstance;
                    var invoiceResponse = JsonConvert.DeserializeObject<CreditNoteInvoiceResponseDto>(await response.Content.ReadAsStringAsync());

                    invoiceResponse = await this.ChangeCreditNoteStatusAsync(creditNoteInvoice, string.Format(urlChangeStatus, invoiceResponse.ResponseDetails.Id));

                    if (partialPaymentValue != 0m)
                        payment = await this.RegisterFacturaPaymentsAsync(new FacturaInvoice(partialPaymentValue,
                                                                                   creditNoteInvoice.FacturaId), string.Format(urlRegisterPayment, creditNoteInvoice.FacturaId));

                    return new InvoiceResponseDto()
                    {
                        ExternalId = invoiceResponse.ResponseDetails.Id,
                        Id = creditNoteInvoice.Id.ToString(),
                        InvoiceNumber = invoiceResponse.ResponseDetails.InvoiceNumber.Remove(invoiceResponse.ResponseDetails.InvoiceNumber.IndexOf("/")),
                        Series = invoiceResponse.ResponseDetails.InvoiceNumber.Remove(0, (invoiceResponse.ResponseDetails.InvoiceNumber.IndexOf("/") + 1)),
                        EmissionDate = Convert.ToDateTime(invoiceResponse.ResponseDetails.EmissionDate),
                        UrlPdf = invoiceResponse.ResponseDetails.UrlPdf,
                        PaymentId = payment?.ResponseDetails?.Id
                    };
                }
                else
                {
                    GenerateInvoiceIntegrationErrorResponse(await response.Content.ReadAsStringAsync(),
                                                                  response.StatusCode,
                                                                  response.ReasonPhrase,
                                                                  creditNoteInvoice.Id);
                    return InvoiceResponseDto.NullInstance;
                }
            }
            catch (BaseExceptionDto)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw (new GeneralExceptionDto(_internalErrorMessage, ex));
            }
        }

        public async Task<InvoiceResponseDto> SendFacturaAsync(FacturaInvoice facturaInvoice, string urlSendInvoice, string urlChangeStatus, string urlRegisterPayment)
        {
            try
            {
                var jsonInvoice = JsonConvert.SerializeObject(new
                {
                    invoice = facturaInvoice
                }, Formatting.Indented, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });

                GenerateinvoicePostLog(jsonInvoice);

                var response = await Policy
                               .HandleResult<HttpResponseMessage>(message => !message.IsSuccessStatusCode)
                               .WaitAndRetryAsync(1, i => TimeSpan.FromSeconds(15), (result, timeSpan, retryCount, context) =>
                               {
                                   Console.Write($"Request failed with {result.Result.StatusCode}. Waiting {timeSpan} before next retry. Retry attempt {retryCount}");
                               })
                               .ExecuteAsync(() => _client.PostAsync(urlSendInvoice, new StringContent(jsonInvoice, Encoding.UTF8, "application/json")));

                if (response.IsSuccessStatusCode)
                {
                    var payment = FacturaPaymentResponseDto.NullInstance;
                    var invoiceResponse = JsonConvert.DeserializeObject<FacturaInvoiceResponseDto>(await response.Content.ReadAsStringAsync());

                    invoiceResponse = await this.ChangeFacturaStatusAsync(facturaInvoice, string.Format(urlChangeStatus, invoiceResponse.ResponseDetails.Id));

                    if (invoiceResponse != null && invoiceResponse.ResponseDetails != null && !string.IsNullOrEmpty(invoiceResponse.ResponseDetails.InvoiceNumber))
                    {
                        facturaInvoice.Amount = invoiceResponse.ResponseDetails.Amount;
                        payment = await RegisterFacturaPaymentsAsync(facturaInvoice, string.Format(urlRegisterPayment, invoiceResponse.ResponseDetails.Id));
                    }

                    return new InvoiceResponseDto()
                    {
                        ExternalId = invoiceResponse.ResponseDetails.Id,
                        Id = facturaInvoice.Id.ToString(),
                        InvoiceNumber = invoiceResponse.ResponseDetails.InvoiceNumber.Remove(invoiceResponse.ResponseDetails.InvoiceNumber.IndexOf("/")),
                        Series = invoiceResponse.ResponseDetails.InvoiceNumber.Remove(0, (invoiceResponse.ResponseDetails.InvoiceNumber.IndexOf("/") + 1)),
                        EmissionDate = Convert.ToDateTime(invoiceResponse.ResponseDetails.EmissionDate),
                        UrlPdf = invoiceResponse.ResponseDetails.UrlPdf,
                        PaymentId = payment.ResponseDetails.Id,
                        Client = invoiceResponse.ResponseDetails.Client
                    };
                }
                else
                {
                    GenerateInvoiceIntegrationErrorResponse(await response.Content.ReadAsStringAsync(),
                                                                  response.StatusCode,
                                                                  response.ReasonPhrase,
                                                                  facturaInvoice.Id);
                    return InvoiceResponseDto.NullInstance;
                }
            }
            catch (BaseExceptionDto)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw (new GeneralExceptionDto(_internalErrorMessage, ex));
            }
        }

        public async Task<ProformaResponseDto> SendProformaAsync(ProformaDto proformaDto, string urlSendProforma, string urlChangeStatus)
        {
            try
            {
                var jsonInvoice = JsonConvert.SerializeObject(new
                {
                    proforma = proformaDto
                }, Formatting.Indented, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });

                var response = await Policy
                               .HandleResult<HttpResponseMessage>(message => !message.IsSuccessStatusCode)
                               .WaitAndRetryAsync(1, i => TimeSpan.FromSeconds(15), (result, timeSpan, retryCount, context) =>
                               {
                                   Console.Write($"Request failed with {result.Result.StatusCode}. Waiting {timeSpan} before next retry. Retry attempt {retryCount}");
                               })
                               .ExecuteAsync(() => _client.PostAsync(urlSendProforma, new StringContent(jsonInvoice, Encoding.UTF8, "application/json")));

                if (response.IsSuccessStatusCode)
                {
                    var proformaResponse = JsonConvert.DeserializeObject<ProformaResponseDto>(await response.Content.ReadAsStringAsync());

                    proformaResponse = await this.ChangeProformaStatusAsync(proformaDto, string.Format(urlChangeStatus, proformaResponse.ResponseDetails.Id));

                    proformaResponse = await this.ChangeProformaStatusToAcceptAsync(proformaDto, string.Format(urlChangeStatus, proformaResponse.ResponseDetails.Id));

                    return proformaResponse;
                }
                else
                {
                    GenerateProformaErrorResponse(await response.Content.ReadAsStringAsync());
                    return ProformaResponseDto.NullInstance;
                }
            }
            catch (BaseExceptionDto)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw (new GeneralExceptionDto(_internalErrorMessage, ex));
            }
        }

        private async Task<ProformaResponseDto> ChangeProformaStatusToAcceptAsync(ProformaDto proformaDto, string url)
        {
            try
            {
                var jsonStatus = JsonConvert.SerializeObject(new
                {
                    invoice = new
                    {
                        state = _acceptState
                    }
                }, Formatting.Indented, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });

                var response = await Policy
                               .HandleResult<HttpResponseMessage>(message => !message.IsSuccessStatusCode)
                               .WaitAndRetryAsync(1, i => TimeSpan.FromSeconds(15), (result, timeSpan, retryCount, context) =>
                               {
                                   Console.Write($"Request failed with {result.Result.StatusCode}. Waiting {timeSpan} before next retry. Retry attempt {retryCount}");
                               })
                               .ExecuteAsync(() => _client.PutAsync(url, new StringContent(jsonStatus, Encoding.UTF8, "application/json")));

                if (response.IsSuccessStatusCode)
                {
                    return JsonConvert.DeserializeObject<ProformaResponseDto>(await response.Content.ReadAsStringAsync());
                }
            }
            catch (BaseExceptionDto)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw (new GeneralExceptionDto(_internalErrorMessage, ex));
            }

            return ProformaResponseDto.NullInstance;
        }

        private async Task<FacturaInvoiceResponseDto> ChangeFacturaStatusAsync(FacturaInvoice facturaInvoice, string url)
        {
            try
            {
                var jsonStatus = JsonConvert.SerializeObject(new
                {
                    invoice = new
                    {
                        state = _finalizedState
                    }
                }, Formatting.Indented, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });

                var response = await Policy
                               .HandleResult<HttpResponseMessage>(message => !message.IsSuccessStatusCode)
                               .WaitAndRetryAsync(1, i => TimeSpan.FromSeconds(15), (result, timeSpan, retryCount, context) =>
                               {
                                   Console.Write($"Request failed with {result.Result.StatusCode}. Waiting {timeSpan} before next retry. Retry attempt {retryCount}");
                               })
                               .ExecuteAsync(() => _client.PutAsync(url, new StringContent(jsonStatus, Encoding.UTF8, "application/json")));

                if (response.IsSuccessStatusCode)
                {
                    return JsonConvert.DeserializeObject<FacturaInvoiceResponseDto>(await response.Content.ReadAsStringAsync());
                }
            }
            catch (BaseExceptionDto)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw (new GeneralExceptionDto(_internalErrorMessage, ex));
            }

            return FacturaInvoiceResponseDto.NullInstance;
        }

        private async Task<ProformaResponseDto> ChangeProformaStatusAsync(ProformaDto proformaDto, string url)
        {
            try
            {
                var jsonStatus = JsonConvert.SerializeObject(new
                {
                    invoice = new
                    {
                        state = _finalizedState
                    }
                }, Formatting.Indented, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });

                var response = await Policy
                               .HandleResult<HttpResponseMessage>(message => !message.IsSuccessStatusCode)
                               .WaitAndRetryAsync(1, i => TimeSpan.FromSeconds(15), (result, timeSpan, retryCount, context) =>
                               {
                                   Console.Write($"Request failed with {result.Result.StatusCode}. Waiting {timeSpan} before next retry. Retry attempt {retryCount}");
                               })
                               .ExecuteAsync(() => _client.PutAsync(url, new StringContent(jsonStatus, Encoding.UTF8, "application/json")));

                if (response.IsSuccessStatusCode)
                {
                    return JsonConvert.DeserializeObject<ProformaResponseDto>(await response.Content.ReadAsStringAsync());
                }
            }
            catch (BaseExceptionDto)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw (new GeneralExceptionDto(_internalErrorMessage, ex));
            }

            return ProformaResponseDto.NullInstance;
        }

        private async Task<CreditNoteInvoiceResponseDto> ChangeCreditNoteStatusAsync(CreditNoteInvoice facturaInvoice, string url)
        {
            try
            {
                var jsonStatus = JsonConvert.SerializeObject(new
                {
                    invoice = new
                    {
                        state = _finalizedState
                    }
                }, Formatting.Indented, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });

                GenerateinvoicePostLog(jsonStatus);

                var response = await Policy
                               .HandleResult<HttpResponseMessage>(message => !message.IsSuccessStatusCode)
                               .WaitAndRetryAsync(1, i => TimeSpan.FromSeconds(15), (result, timeSpan, retryCount, context) =>
                               {
                                   Console.Write($"Request failed with {result.Result.StatusCode}. Waiting {timeSpan} before next retry. Retry attempt {retryCount}");
                               })
                               .ExecuteAsync(() => _client.PutAsync(url, new StringContent(jsonStatus, Encoding.UTF8, "application/json")));

                if (response.IsSuccessStatusCode)
                {
                    return JsonConvert.DeserializeObject<CreditNoteInvoiceResponseDto>(await response.Content.ReadAsStringAsync());
                }
            }
            catch (BaseExceptionDto)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw (new GeneralExceptionDto(_internalErrorMessage, ex));
            }

            return CreditNoteInvoiceResponseDto.NullInstance;
        }

        private async Task<bool> CancelInvoicePaymentAsync(string url)
        {
            try
            {
                var jsonPayment = JsonConvert.SerializeObject(new
                {
                    receipt = new
                    {
                        state = _canceledState,
                        message = _canceledMessage
                    }
                }, Formatting.Indented, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });

                GenerateinvoicePostLog(jsonPayment);

                var response = await Policy
                               .HandleResult<HttpResponseMessage>(message => !message.IsSuccessStatusCode)
                               .WaitAndRetryAsync(1, i => TimeSpan.FromSeconds(15), (result, timeSpan, retryCount, context) =>
                               {
                                   Console.Write($"Request failed with {result.Result.StatusCode}. Waiting {timeSpan} before next retry. Retry attempt {retryCount}");
                               })
                               .ExecuteAsync(() => _client.PutAsync(url, new StringContent(jsonPayment, Encoding.UTF8, "application/json")));

                if (!response.IsSuccessStatusCode)
                {
                    return false;
                }
            }
            catch (BaseExceptionDto)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw (new GeneralExceptionDto(_internalErrorMessage, ex));
            }

            return true;
        }

        private async Task<FacturaPaymentResponseDto> RegisterFacturaPaymentsAsync(FacturaInvoice facturaInvoice, string url)
        {
            try
            {
                var jsonPayment = JsonConvert.SerializeObject(new
                {
                    partial_payment = new
                    {
                        amount = facturaInvoice.Amount
                    }
                }, Formatting.Indented, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });

                GenerateinvoicePostLog(jsonPayment);

                var response = await Policy
                               .HandleResult<HttpResponseMessage>(message => !message.IsSuccessStatusCode)
                               .WaitAndRetryAsync(1, i => TimeSpan.FromSeconds(15), (result, timeSpan, retryCount, context) =>
                               {
                                   Console.Write($"Request failed with {result.Result.StatusCode}. Waiting {timeSpan} before next retry. Retry attempt {retryCount}");
                               })
                               .ExecuteAsync(() => _client.PostAsync(url, new StringContent(jsonPayment, Encoding.UTF8, "application/json")));

                if (response.IsSuccessStatusCode)
                {
                    return JsonConvert.DeserializeObject<FacturaPaymentResponseDto>(await response.Content.ReadAsStringAsync());
                }
                else
                {
                    var aaa = await response.Content.ReadAsStringAsync();
                }
            }
            catch (BaseExceptionDto)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw (new GeneralExceptionDto(_internalErrorMessage, ex));
            }

            return FacturaPaymentResponseDto.NullInstance;
        }

        private void GenerateInvoiceIntegrationErrorResponse(string responseMessage, HttpStatusCode statusCode,
                                                             string reasonPhrase, string invoiceId)
        {
            GenerateInvoiceLogError(responseMessage, invoiceId);

            _notificationHandler.Raise(_notificationHandler.DefaultBuilder
                          .WithDetailedMessage(responseMessage.ToString())
                          .Build());
        }

        private void GenerateProformaErrorResponse(string responseMessage)
        {
            GenerateProformaLogError(responseMessage);

            _notificationHandler.Raise(_notificationHandler.DefaultBuilder
                          .WithDetailedMessage(responseMessage.ToString())
                          .Build());
        }

        private void GenerateInvoiceLogError(string responseMessage, string invoiceId)
        {
            if (!string.IsNullOrEmpty(responseMessage))
                _genericLog.AddLog(_genericLog.DefaultBuilder
                                 .WithMessage($"Invoice: {invoiceId}")
                                 .WithDetailedMessage(responseMessage.Replace(@"\", ""))
                                 .AsError().Build());

            if (_genericLog.HasGenericLog())
                _genericLog.Commit(_invoiceXpressIndexName);
        }

        private void GenerateProformaLogError(string responseMessage)
        {
            if (!string.IsNullOrEmpty(responseMessage))
                _genericLog.AddLog(_genericLog.DefaultBuilder
                                 .WithMessage($"Proforma: {Guid.NewGuid()}")
                                 .WithDetailedMessage(responseMessage.Replace(@"\", ""))
                                 .AsError().Build());

            if (_genericLog.HasGenericLog())
                _genericLog.Commit(_invoiceXpressIndexName);
        }

        private void GenerateInvoiceLogSucess(string responseMessage, FacturaInvoice invoice)
        {
            if (!string.IsNullOrEmpty(responseMessage) && invoice != null)
                _genericLog.AddLog(_genericLog.DefaultBuilder
                                 .WithMessage($"Invoice: {invoice.Id}")
                                 .WithDetailedMessage(responseMessage.Replace(@"\", ""))
                                 .AsError().Build());

            if (_genericLog.HasGenericLog())
                _genericLog.Commit(_invoiceXpressIndexName);
        }

        /// <summary>
        /// Cancela uma nota fiscal
        /// </summary>
        /// <param name="empresaId"></param>
        /// <param name="nfe"></param>
        /// <returns></returns>
        public async Task<bool> DeleteAsync(FacturaInvoice invoice, string url, INotificationHandler notificationHandler)
        {
            try
            {
                using (var request = new HttpRequestMessage(HttpMethod.Delete, url))
                {
                    using (var response = await _client.SendAsync(request))
                    {
                        var resultContent = await response.Content.ReadAsStringAsync();

                        if (response.IsSuccessStatusCode)
                            return true;
                        else
                        {
                            var messageException = new StringBuilder(((int)response.StatusCode) + " - " + response.ReasonPhrase);

                            var notification = notificationHandler.DefaultBuilder
                                      .WithMessage(AppConsts.LocalizationSourceName, TaxRuleEnum.Error.CancelInvoiceError);

                            var dataResponse = JsonConvert.DeserializeObject<ErrorDto[]>(resultContent);
                            if (dataResponse != null)
                                foreach (var message in dataResponse)
                                    messageException.AppendLine($", {message.Code} - {message.Message}");

                            notificationHandler.Raise(notification.WithDetailedMessage(messageException.ToString()).Build());

                            return false;
                        }
                    }
                }
            }
            catch (BaseExceptionDto)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw (new GeneralExceptionDto(_internalErrorMessage, ex));
            }
        }

        public async Task<bool> UpdateClientInformationsAsync(PersonInvoiceXpressDto clientInvoice, string url)
        {
            try
            {
                var jsonClient = JsonConvert.SerializeObject(new
                {
                    client = clientInvoice
                }, Formatting.Indented, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });

                var response = await Policy
                               .HandleResult<HttpResponseMessage>(message => !message.IsSuccessStatusCode)
                               .WaitAndRetryAsync(1, i => TimeSpan.FromSeconds(15), (result, timeSpan, retryCount, context) =>
                               {
                                   Console.Write($"Request failed with {result.Result.StatusCode}. Waiting {timeSpan} before next retry. Retry attempt {retryCount}");
                               })
                               .ExecuteAsync(() => _client.PutAsync(url, new StringContent(jsonClient, Encoding.UTF8, "application/json")));

                if (response.IsSuccessStatusCode)
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
            }

            return false;
        }

        public void Dispose()
        {
            _client.Dispose();
        }
    }
}
