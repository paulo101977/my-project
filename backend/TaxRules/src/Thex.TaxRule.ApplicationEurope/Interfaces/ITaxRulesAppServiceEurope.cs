﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.Common.Dto.InvoiceIntegration;
using Thex.Dto.Integration;
using Thex.TaxRule.Dto;
using Thex.TaxRule.Dto.Dto.Integration.Response;
using Thex.TaxRule.Dto.Dto.PT.InvoiceXpressIntegration.Proforma;
using Tnf.Application.Services;
using Tnf.Dto;

namespace Thex.TaxRule.ApplicationEurope.Interfaces
{
    public interface ITaxRulesAppServiceEurope : IApplicationService
    {
        Task<bool> CheckExistTax(List<IntegrationServiceInvoiceDTO> serviceInvoiceList = null, List<IntegrationProductInvoiceDTO> productInvoiceList = null);
        Task<TaxRulesDto> GetTribute(Guid propertyUUid, GetAllEuropeTaxRulesWithParamsDto requestDto);
        Task<InvoiceResponseDto> Send(List<InvoiceIntegrationDto> invoiceListDto);
        Task ToggleActivationObjAsync(Guid id);
        Task UpdateInvoiceResponse(InvoiceIntegrationResponseDto invoiceResponse, int invoiceType);
        Task<string> GetIntegratorIdById(Guid id);
        Task<ProformaResponseDto> SendProforma(ProformaDto proforma);
    }
}
