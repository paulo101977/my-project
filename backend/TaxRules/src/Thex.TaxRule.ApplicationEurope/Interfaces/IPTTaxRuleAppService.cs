﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.TaxRule.Dto;

namespace Thex.TaxRule.ApplicationEurope.Interfaces
{
    public interface IPTTaxRuleAppService
    {
        Task<List<PTTaxRuleServiceResponseDto>> GetAllTaxRulesServices(GetAllPTTaxRuleServiceDto getAllTaxRuleService);
        Task<List<PTTaxRuleProductResponseDto>> GetAllTaxRulesProducts(GetAllPTTaxRuleProductDto getAllPTTaxRuleProduct);
        void AssociateRange(PTTaxRuleAssociateDto pTTaxRuleAssociateDto);
    }
}
