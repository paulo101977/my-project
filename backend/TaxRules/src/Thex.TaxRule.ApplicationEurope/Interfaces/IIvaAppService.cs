using System.Threading.Tasks;
using Thex.TaxRule.Dto;
using Tnf.Dto;

namespace Thex.TaxRule.ApplicationEurope.Interfaces
{
    public interface IIvaAppService
    {
        Task<IListDto<IvaDto>> GetAll(GetAllIvaDto getAllIvaDto);
    }
}
