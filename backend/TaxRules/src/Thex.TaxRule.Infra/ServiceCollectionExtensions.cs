﻿using hex.TaxRule.Infra.Repositories;
using Microsoft.Extensions.DependencyInjection;
using Thex.TaxRule.Infra.Interfaces;
using Thex.TaxRule.Infra.Interfaces.ReadRepositories;
using Thex.TaxRule.Infra.Repositories;
using Thex.TaxRule.Infra.Repositories.ReadRepositories;
using Thex.TaxRules.Infra;
using Thex.ThexRule.Infra.Mappers;
using Thex.ThexRule.Infra.Mappers.DapperMappers;
using Tnf.Dapper;

namespace Thex.TaxRule.Infra.Infra
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddInfraDependency(this IServiceCollection services)
        {
            // Configura o uso do Dapper registrando os contextos que serão
            // usados pela aplicação
            services
                .AddTnfEntityFrameworkCore()
                .AddTnfDbContext<ThexTaxRuleContext>(config => DbContextConfigurer.Configure(config))
                .AddTnfDapper(options =>
                {
                    options.MapperAssemblies.Add(typeof(CFOPMapper).Assembly);
                    options.MapperAssemblies.Add(typeof(CSOSNMapper).Assembly);
                    options.MapperAssemblies.Add(typeof(CSTMapper).Assembly);
                    options.MapperAssemblies.Add(typeof(NaturezaReceitaMapper).Assembly);
                    options.MapperAssemblies.Add(typeof(NBSMapper).Assembly);
                    options.MapperAssemblies.Add(typeof(NCMMapper).Assembly);
                    options.MapperAssemblies.Add(typeof(RegimeApuracaoMapper).Assembly);
                    options.MapperAssemblies.Add(typeof(TaxRulesMapper).Assembly);
                    options.MapperAssemblies.Add(typeof(TipoServicoMapper).Assembly);
                    options.MapperAssemblies.Add(typeof(UFMapper).Assembly);
                    options.MapperAssemblies.Add(typeof(ImpostoRetidoMapper).Assembly);
                    options.MapperAssemblies.Add(typeof(TaxRulesPTMapper).Assembly);

                    options.DbType = DapperDbType.SqlServer;
                });

            services.AddTnfAutoMapper(config =>
            {
                config.AddProfile<CFOPProfile>();
                config.AddProfile<CSOSNProfile>();
                config.AddProfile<CSTProfile>();
                config.AddProfile<NaturezaReceitaProfile>();
                config.AddProfile<NBSProfile>();
                config.AddProfile<NCMProfile>();
                config.AddProfile<RegimeApuracaoProfile>();
                config.AddProfile<TaxRulesProfile>();
                config.AddProfile<TipoServicoProfile>();
                config.AddProfile<UFProfile>();
                config.AddProfile<ImpostoRetidoProfile>();
                config.AddProfile<TaxRulesPTProfile>();
            });

            services.AddTransient<ICFOPReadRepository, CFOPReadRepository>();
            services.AddTransient<ICSOSNReadRepository, CSOSNReadRepositorycs>();
            services.AddTransient<ICSTReadRepository, CSTReadRepository>();
            services.AddTransient<INaturezaReceitaReadRepository, NaturezaReceitaReadRepository>();
            services.AddTransient<INBSReadRepository, NBSReadRepository>();
            services.AddTransient<INCMReadRepository, NCMReadRepository>();
            services.AddTransient<IRegimeApuracaoReadRepository, RegimeApuracaoReadRepository>();
            services.AddTransient<ITipoServicoReadRepository, TipoServicoReadRepository>();
            services.AddTransient<IUFReadRepository, UFReadRepository>();
            services.AddTransient<ITaxRulesReadRepository, TaxRulesReadRepository>();
            services.AddTransient<ITaxRulesRepository, TaxRulesRepository>();
            services.AddTransient<IServiceInvoiceRepository, ServiceInvoiceRepository>();
            services.AddTransient<IServiceInvoiceReadRepository, ServiceInvoiceReadRepository>();
            services.AddTransient<IProductNFCeInvoiceRepository, ProductNFCeInvoiceRepository>();
            services.AddTransient<IProductNFCeInvoiceReadRepository, ProductNFCeInvoiceReadRepository>();
            services.AddTransient<IProductSATInvoiceRepository, ProductSATInvoiceRepository>();
            services.AddTransient<IProductSATInvoiceReadRepository, ProductSATInvoiceReadRepository>();
            services.AddTransient<IRetainedClientReadRepository, RetainedClientReadRepository>();
            services.AddTransient<IImpostoRetidoRepository, ImpostoRetidoRepository>();
            services.AddTransient<IImpostoRetidoReadRepository, ImpostoRetidoReadRepository>();
            services.AddTransient<ITaxRulesPTRepository, TaxRulesPTRepository>();
            services.AddTransient<ITaxRulesPTReadRepository, TaxRulesPTReadRepository>();
            services.AddTransient<IFacturaInvoiceRepository, FacturaInvoiceRepository>();
            services.AddTransient<IFacturaInvoiceReadRepository, FacturaInvoiceReadRepository>();
            services.AddTransient<ICreditNoteInvoiceRepository, CreditNoteInvoiceRepository>();
            services.AddTransient<ICreditNoteInvoiceReadRepository, CreditNoteInvoiceReadRepository>();
            services.AddTransient<IIvaReadRepository, IvaReadRepository>();
            services.AddTransient<IPTTaxRuleReadRepository, PTTaxRuleReadRepository>();
            services.AddTransient<IPTTaxRuleRepository, PTTaxRuleRepository>();

            return services;
        }
    }
}
