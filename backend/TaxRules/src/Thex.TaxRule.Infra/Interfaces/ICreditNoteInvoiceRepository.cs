﻿using System.Threading.Tasks;
using Thex.TaxRule.Dto.Dto.Integration;
using Thex.TaxRule.Dto.Dto.Integration.NFCe;
using Thex.TaxRule.Dto.Dto.Integration.NFSe;
using Thex.TaxRule.Dto.InvoiceXpressIntegration;

namespace Thex.TaxRule.Infra.Interfaces
{
    public interface ICreditNoteInvoiceRepository
    {
        Task PostAsync(CreditNoteInvoice invoice);
        Task ChangeAsync(CreditNoteInvoice invoice);
    }
}

