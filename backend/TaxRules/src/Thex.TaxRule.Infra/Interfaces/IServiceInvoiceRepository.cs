﻿using System.Threading.Tasks;
using Thex.TaxRule.Dto.Dto.Integration;
using Thex.TaxRule.Dto.Dto.Integration.NFCe;
using Thex.TaxRule.Dto.Dto.Integration.NFSe;

namespace Thex.TaxRule.Infra.Interfaces
{
    public interface IServiceInvoiceRepository
    {
        Task PostAsync(NFSeInvoice invoice);
        Task ChangeAsync(NFSeInvoice invoice);
    }
}

