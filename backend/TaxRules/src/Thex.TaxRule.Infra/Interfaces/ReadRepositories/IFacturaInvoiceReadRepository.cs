﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Thex.TaxRule.Dto.Dto.Integration;
using Thex.TaxRule.Dto.Dto.Integration.NFSe;
using Thex.TaxRule.Dto.InvoiceXpressIntegration;

namespace Thex.TaxRule.Infra.Interfaces.ReadRepositories
{
    public interface IFacturaInvoiceReadRepository
    {
        Task<FacturaInvoice> GetServiceInvoice(string invoiceId);
        Task<FacturaInvoice> GetServiceInvoiceByExternalInvoiceId(string facturaId);
        Task<FacturaInvoice> GetServiceInvoiceByClientCode(string clientCode);
    }
}
