﻿using System;
using System.Collections.Generic;
using Thex.TaxRule.Dto;

namespace Thex.TaxRule.Infra.Interfaces.ReadRepositories
{
    public interface IPTTaxRuleReadRepository
    {
        List<PTTaxRuleDto> GetAllByIvaIdList(List<Guid> ivaIdList);
        bool AnyExceptInIvaId(Guid ivaId, List<int> serviceIdList, List<Guid> productIdList);       
    }
}
