﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Thex.TaxRule.Dto;
using Thex.TaxRule.Dto.GetAll;
using Tnf.Dto;

namespace Thex.TaxRule.Infra.Interfaces.ReadRepositories
{
    public interface INBSReadRepository
    {
        Task<IListDto<NBSDto>> GetAll(GetAllNBSDto requestDto);
        Task<NBSDto> GetByCode(string code, GetAllNBSDto requestDto);
        Task<IListDto<NBSDto>> GetAllByFilters(SearchNbsDto searchDto);
    }
}
