﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Thex.TaxRule.Infra.Interfaces.ReadRepositories
{
    public interface IRetainedClientReadRepository
    {
        bool isRetainedClient(Guid guid);
    }
}
