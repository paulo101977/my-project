﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Thex.TaxRule.Dto;
using Tnf.Dto;

namespace Thex.TaxRule.Infra.Interfaces.ReadRepositories
{
    public interface IUFReadRepository
    {
        Task<IListDto<UFDto>> GetAll(GetAllUFDto requestDto);
        Task<UFDto> GetByCode(GetAllUFDto requestDto);
    }
}
