﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Thex.TaxRule.Dto;
using Tnf.Dto;

namespace Thex.TaxRule.Infra.Interfaces.ReadRepositories
{
    public interface ITipoServicoReadRepository
    {
        Task<IListDto<TipoServicoDto>> GetAll(GetAllTipoServicoDto requestDto);
        Task<TipoServicoDto> GetByCode(string code, GetAllTipoServicoDto requestDto);
    }
}
