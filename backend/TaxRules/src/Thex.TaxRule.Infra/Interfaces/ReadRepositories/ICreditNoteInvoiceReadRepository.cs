﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Thex.TaxRule.Dto.Dto.Integration;
using Thex.TaxRule.Dto.Dto.Integration.NFSe;
using Thex.TaxRule.Dto.InvoiceXpressIntegration;

namespace Thex.TaxRule.Infra.Interfaces.ReadRepositories
{
    public interface ICreditNoteInvoiceReadRepository
    {
        Task<CreditNoteInvoice> GetCreditNoteInvoice(string invoiceId);
    }
}
