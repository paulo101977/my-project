﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Thex.TaxRule.Dto.Dto.Integration;
using Thex.TaxRule.Dto.Dto.Integration.NFSe;

namespace Thex.TaxRule.Infra.Interfaces.ReadRepositories
{
    public interface IServiceInvoiceReadRepository
    {
        Task<NFSeInvoice> GetServiceInvoice(string invoiceId);
    }
}
