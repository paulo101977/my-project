﻿using System;
using System.Threading.Tasks;
using Thex.TaxRule.Dto;
using Tnf.Dto;

namespace Thex.TaxRule.Infra.Interfaces.ReadRepositories
{
    public interface IImpostoRetidoReadRepository
    {
        Task<IListDto<ImpostoRetidoDto>> GetAllByOwnerIdAndPropertyUid(GetAllImpostoRetidoDto requestDto, Guid propertyUid);
    }
}
