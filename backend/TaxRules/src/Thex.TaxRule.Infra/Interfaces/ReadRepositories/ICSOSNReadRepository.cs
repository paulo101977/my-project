﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Thex.TaxRule.Dto;
using Tnf.Dto;

namespace Thex.TaxRule.Infra.Interfaces.ReadRepositories
{
    public interface ICSOSNReadRepository
    {
        Task<IListDto<CSOSNDto>> GetAll(GetAllCSOSNDto requestDto);
        Task<CSOSNDto> GetByCode(string code, GetAllCSOSNDto requestDto);
    }
}
