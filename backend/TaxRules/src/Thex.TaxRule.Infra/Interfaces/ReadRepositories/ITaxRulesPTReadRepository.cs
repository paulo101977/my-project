﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.TaxRule.Dto;
using Tnf.Dto;

namespace Thex.TaxRule.Infra.Interfaces.ReadRepositories
{
    public interface ITaxRulesPTReadRepository
    {
        Task<bool> ValidateExistServiceTax(List<string> serviceParams);
        Task<bool> ValidateExistProductTax(List<string> productBarcodeParams, List<string> productNcmParams, int totalProducts);
        Task<TaxRulesDto> GetTributes(Guid propertyUUId, GetAllTaxRulesWithParamsDto requestDto);
        Task<TaxRulesDto> GetTributes(Guid propertyUUId, GetAllEuropeTaxRulesWithParamsDto requestDto);
        Task<Domain.Entities.TaxRulePT> GetEntityById(Guid id);
        Task<IListDto<TaxRulesDto>> GetAll(int type, GetAllTaxRulesDto requestDto);
        Task<TaxRulesDto> GetById(Guid id);
    }
}
