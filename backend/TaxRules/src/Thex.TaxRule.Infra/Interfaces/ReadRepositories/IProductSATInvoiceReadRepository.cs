﻿using System.Threading.Tasks;
using Thex.TaxRule.Dto.Dto.Integration.NFCe;

namespace Thex.TaxRule.Infra.Interfaces.ReadRepositories
{
    public interface IProductSATInvoiceReadRepository
    {
        Task<SATInvoice> GetProductInvoice(string invoiceId);
    }
}
