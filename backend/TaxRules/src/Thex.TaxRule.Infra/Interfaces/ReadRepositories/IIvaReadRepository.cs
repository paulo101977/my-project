﻿using System;
using System.Threading.Tasks;
using Thex.TaxRule.Dto;
using Tnf.Dto;

namespace Thex.TaxRule.Infra.Interfaces.ReadRepositories
{
    public interface IIvaReadRepository
    {
        Task<IListDto<IvaDto>> GetAll(GetAllIvaDto getAllIvaDto);
        IvaDto GetById(Guid id);
    }
}
