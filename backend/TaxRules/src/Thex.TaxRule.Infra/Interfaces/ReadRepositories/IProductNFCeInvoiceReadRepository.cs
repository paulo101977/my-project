﻿using System.Threading.Tasks;
using Thex.TaxRule.Dto.Dto.Integration.NFCe;

namespace Thex.TaxRule.Infra.Interfaces.ReadRepositories
{
    public interface IProductNFCeInvoiceReadRepository
    {
        Task<NFCeInvoice> GetProductInvoice(string invoiceId);
    }
}
