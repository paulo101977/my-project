﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.TaxRule.Domain.Entities;

namespace Thex.TaxRule.Infra.Interfaces
{
    public interface IPTTaxRuleRepository
    {
        void CreateRange(List<PTTaxRule> entityList);
        void DeleteAllByIvaId(Guid ivaId);
    }
}
