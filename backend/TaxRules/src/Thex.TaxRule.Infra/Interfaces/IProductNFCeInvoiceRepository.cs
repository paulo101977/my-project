﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Thex.TaxRule.Dto.Dto.Integration.NFCe;

namespace Thex.TaxRule.Infra.Interfaces
{
    public interface IProductNFCeInvoiceRepository
    {
        Task PostAsync(NFCeInvoice invoice);
        Task ChangeAsync(NFCeInvoice invoice);
    }
}
