﻿using System;
using System.Threading.Tasks;
using Thex.TaxRule.Domain.Entities;
using Tnf.Dto;

namespace Thex.ThexRule.Infra.Interfaces
{
    public interface IBaseRepository
    {
        Task<IDto> InsertAsync(BaseEntity baseEntity);
        Task<IDto> UpdateAsync(Guid id, BaseEntity baseEntity);
        Task RemoveAsync(Guid id);
        Task ToggleAsync(Guid id);
    }
}
