﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Thex.TaxRule.Dto.Dto.Integration.NFCe;

namespace Thex.TaxRule.Infra.Interfaces
{
    public interface IProductSATInvoiceRepository
    {
        Task PostAsync(SATInvoice invoice);
        Task ChangeAsync(SATInvoice invoice);
    }
}
