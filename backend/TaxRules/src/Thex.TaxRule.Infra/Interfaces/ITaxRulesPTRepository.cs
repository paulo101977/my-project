﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Thex.ThexRule.Infra.Interfaces;
using Tnf.Dto;

namespace Thex.TaxRule.Infra.Repositories
{
    public interface ITaxRulesPTRepository : IBaseRepository
    {
        Task<IDto> InsertTaxRule(Domain.Entities.TaxRulePT taxRule);
    }
}
