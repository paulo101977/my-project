﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.TaxRule.Domain.Entities;

namespace Thex.TaxRule.Infra.Interfaces
{
    public interface IImpostoRetidoRepository
    {
        Task InsertList(List<ImpostoRetido> impostoRetidoEntityList);
        Task DeleteImpostoRetido(Guid ownerId, Guid propertyUId);
    }
}
