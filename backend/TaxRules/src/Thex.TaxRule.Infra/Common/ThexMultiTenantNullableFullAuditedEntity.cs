﻿using System;
using Thex.TaxRule.Infra.Common;

namespace Thex.TaxRule.Common
{
    public abstract class ThexMultiTenantNullableFullAuditedEntity : ThexFullAuditedEntity
    {
        public Guid? TenantId { get; set; }
        public Tenant Tenant { get; set; }
    }
}
