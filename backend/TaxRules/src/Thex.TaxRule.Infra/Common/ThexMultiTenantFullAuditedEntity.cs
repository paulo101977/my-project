﻿using System;

namespace Thex.TaxRule.Infra.Common
{
    public abstract class ThexMultiTenantFullAuditedEntity : ThexFullAuditedEntity
    {
        public Guid TenantId { get; set; }

        public Tenant Tenant { get; set; }
    }
}
