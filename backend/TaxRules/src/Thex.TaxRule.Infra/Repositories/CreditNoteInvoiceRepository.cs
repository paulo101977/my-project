﻿using System.Threading.Tasks;
using Thex.TaxRule.Dto.InvoiceXpressIntegration;
using Thex.TaxRule.Infra.Interfaces;
using Tnf.MongoDb.Provider;
using Tnf.MongoDb.Repositories;

namespace Thex.TaxRule.Infra.Repositories
{
    public class CreditNoteInvoiceRepository : MongoDbRepository<CreditNoteInvoice, string>, ICreditNoteInvoiceRepository
    {
        public CreditNoteInvoiceRepository(IMongoDbProvider provider) : base(provider)
        {

        }

        public async Task ChangeAsync(CreditNoteInvoice invoice)
        {
            await UpdateAsync(invoice);
        }

        public async Task PostAsync(CreditNoteInvoice invoice)
        {
            await InsertAsync(invoice);
        }
    }
}
