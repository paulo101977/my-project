﻿using System;
using System.Threading.Tasks;
using Thex.TaxRule.Dto.Dto.Integration;
using Thex.TaxRule.Dto.Dto.Integration.NFSe;
using Thex.TaxRule.Infra.Interfaces;
using Tnf.MongoDb.Provider;
using Tnf.MongoDb.Repositories;

namespace Thex.TaxRule.Infra.Repositories
{
    public class ServiceInvoiceRepository : MongoDbRepository<NFSeInvoice, string>, IServiceInvoiceRepository
    {
        public ServiceInvoiceRepository(IMongoDbProvider provider) : base(provider)
        {

        }

        public async Task ChangeAsync(NFSeInvoice invoice)
        {
            await UpdateAsync(invoice);
        }

        public async Task PostAsync(NFSeInvoice invoice)
        {
            await InsertAsync(invoice);
        }
    }
}
