﻿using System.Threading.Tasks;
using Thex.TaxRule.Dto.InvoiceXpressIntegration;
using Thex.TaxRule.Infra.Interfaces;
using Tnf.MongoDb.Provider;
using Tnf.MongoDb.Repositories;

namespace Thex.TaxRule.Infra.Repositories
{
    public class FacturaInvoiceRepository : MongoDbRepository<FacturaInvoice, string>, IFacturaInvoiceRepository
    {
        public FacturaInvoiceRepository(IMongoDbProvider provider) : base(provider)
        {

        }

        public async Task ChangeAsync(FacturaInvoice invoice)
        {
            await UpdateAsync(invoice);
        }

        public async Task PostAsync(FacturaInvoice invoice)
        {
            await InsertAsync(invoice);
        }
    }
}
