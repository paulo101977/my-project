﻿using System;
using System.Threading.Tasks;
using Thex.TaxRule.Dto.Dto.Integration;
using Thex.TaxRule.Dto.Dto.Integration.NFCe;
using Thex.TaxRule.Dto.Dto.Integration.NFSe;
using Thex.TaxRule.Infra.Interfaces;
using Tnf.MongoDb.Provider;
using Tnf.MongoDb.Repositories;

namespace Thex.TaxRule.Infra.Repositories
{
    public class ProductNFCeInvoiceRepository : MongoDbRepository<NFCeInvoice, string>, IProductNFCeInvoiceRepository
    {
        public ProductNFCeInvoiceRepository(IMongoDbProvider provider) : base(provider)
        {

        }

        public async Task ChangeAsync(NFCeInvoice invoice)
        {
            await UpdateAsync(invoice);
        }

        public async Task PostAsync(NFCeInvoice invoice)
        {
            await InsertAsync(invoice);
        }
    }
}
