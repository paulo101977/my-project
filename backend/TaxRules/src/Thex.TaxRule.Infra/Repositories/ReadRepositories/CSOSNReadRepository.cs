﻿using System.Linq;
using System.Threading.Tasks;
using Thex.GenericLog;
using Thex.Kernel;
using Thex.TaxRule.Domain.Entities;
using Thex.TaxRule.Dto;
using Thex.TaxRule.Infra.Interfaces.ReadRepositories;
using Thex.TaxRules.Infra;
using Tnf.Dapper.Repositories;
using Tnf.Dto;
using Tnf.Repositories;

namespace Thex.TaxRule.Infra.Repositories
{
    public class CSOSNReadRepositorycs : DapperEfRepositoryBase<ThexTaxRuleContext, CSOSN>, ICSOSNReadRepository
    {
        private readonly IApplicationUser _applicationUser;

        public CSOSNReadRepositorycs(
               IActiveTransactionProvider activeTransactionProvider,
               IApplicationUser applicationUser,
               IGenericLogHandler genericLogHandler) : base(activeTransactionProvider, genericLogHandler)
        {
            _applicationUser = applicationUser;
        }

        public async Task<IListDto<CSOSNDto>> GetAll(GetAllCSOSNDto requestDto)
        {
            var query = base.Query<CSOSNDto>(@"
                SELECT BR_CSOSN.CsosnId as Id,
                       BR_CSOSN.Codigo as Code,
                       BR_CSOSN.Descricao as Description,
                       BR_CSOSN.*
                FROM BR_CSOSN");

            return await query.ToListDtoAsync(requestDto);
        }

        public async Task<CSOSNDto> GetByCode(string code, GetAllCSOSNDto requestDto)
        {
            var query = await base.QueryAsync<CSOSNDto>(@"
                SELECT BR_CSOSN.CsosnId as Id,
                       BR_CSOSN.Codigo as Code,
                       BR_CSOSN.Descricao as Description,
                       BR_CSOSN.*
                FROM BR_CSOSN
                WHERE BR_CSOSN.Codigo = @CFOP",
                new
                {
                    CFOP = code
                });

            return query.ToListDtoAsync(requestDto).Result.Items.FirstOrDefault();
        }
    }
}