﻿using System.Threading.Tasks;
using Thex.TaxRule.Dto.InvoiceXpressIntegration;
using Thex.TaxRule.Infra.Interfaces.ReadRepositories;
using Tnf.MongoDb.Provider;
using Tnf.MongoDb.Repositories;

namespace Thex.TaxRule.Infra.Repositories.ReadRepositories
{
    public class CreditNoteInvoiceReadRepository : MongoDbRepository<CreditNoteInvoice, string>, ICreditNoteInvoiceReadRepository
    {
        public CreditNoteInvoiceReadRepository(IMongoDbProvider provider) : base(provider)
        {
        }

        public async Task<CreditNoteInvoice> GetCreditNoteInvoice(string invoiceId)
        {
            return await GetAsync(invoiceId.ToLower());
        }
    }
}
