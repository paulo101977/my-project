﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Thex.GenericLog;
using Thex.Kernel;
using Thex.TaxRule.Dto;
using Thex.TaxRule.Infra.Interfaces.ReadRepositories;
using Thex.TaxRules.Infra;
using Tnf.Dapper.Repositories;
using Tnf.Dto;
using Tnf.Repositories;

namespace Thex.TaxRule.Infra.Repositories
{
    public class TaxRulesReadRepository : DapperEfRepositoryBase<ThexTaxRuleContext, Domain.Entities.TaxRule>, ITaxRulesReadRepository
    {
        private readonly IApplicationUser _applicationUser;

        public TaxRulesReadRepository(
               IActiveTransactionProvider activeTransactionProvider,
               IApplicationUser applicationUser,
               IGenericLogHandler genericLogHandler) : base(activeTransactionProvider, genericLogHandler)
        {
            _applicationUser = applicationUser;
        }

        public async Task<IListDto<TaxRulesDto>> GetAll(int type, GetAllTaxRulesDto requestDto)
        {
            var query = await QueryAsync<TaxRulesDto>(@"
                SELECT TaxRules.TaxRulesId           AS Id,
                       TaxRules.Nome                 AS Name,
                       TaxRules.CodigoBarras         AS Barcode,
                       TaxRules.NaturezaReceitaId    AS NatureId,
                       TaxRules.RegimeApuracaoId     AS TaxCalculationId,
                       TaxRules.TipoServicoid        AS ServiceTypeId,
                       TaxRules.Aliquota             AS TaxPercentual,
                       TaxRules.AliquotaSubstituicao AS TaxPercentualSub,
                       TaxRules.PercentualBase       AS TaxPercentualBase,
                       TaxRules.Tipo                 AS Type,
                       NCM.Codigo                    AS NcmCode,
                       NCM.CodigoCest                AS CestCode,
                       NBS.Codigo                    AS NbsCode,
                       CFOP.Codigo                   AS CfopCode,
                       UF.Sigla                      AS UfInitials,
                       TaxRules.PropertyId           AS PropertyUId,
                       TaxRules.*
                FROM   TaxRules
                       LEFT OUTER JOIN BR_NCM AS NCM
                                    ON TaxRules.NcmId = NCM.NcmId
                       LEFT OUTER JOIN BR_NBS AS NBS
                                    ON TaxRules.NbsId = NBS.NbsId
                       LEFT OUTER JOIN BR_UF AS UF
                                    ON TaxRules.UfId = UF.UfId
                       LEFT OUTER JOIN BR_CFOP AS CFOP
                                    ON TaxRules.CfopId = CFOP.CfopId
                WHERE TaxRules.isDeleted = 0
                AND TaxRules.Tipo = @Tipo",
                                    new
                                    {
                                        Tipo = type
                                    });

            // Lista as tributações para mais de uma property
            var ret = query.Where(x => x.PropertyUId != null)
                   .GroupBy(i => new { i.BarCode, i.NcmId, i.NbsId, i.UfId })
                   .Select(i => new TaxRulesDto()
                   {
                       PropertyList = i.Select(x => new PropertyDto
                       {
                           PropertyUId = x.PropertyUId,
                           TenantId = x.TenantId
                       }).ToList(),
                       ServiceTypeId = i.First().ServiceTypeId,
                       TaxCalculationId = i.First().TaxCalculationId,
                       UfInitials = i.First().UfInitials,
                       NatureId = i.First().NatureId,
                       BarCode = i.First().BarCode,
                       Name = i.First().Name,
                       Id = i.First().Id,
                       NcmCode = i.First().NcmCode,
                       NbsCode = i.First().NbsCode,
                       Type = i.First().Type,
                       TaxPercentualBase = i.First().TaxPercentualBase,
                       TaxPercentualSub = i.First().TaxPercentualSub,
                       TaxPercentual = i.First().TaxPercentual,
                       CfopId = i.First().CfopId,
                       CsosnId = i.First().CsosnId,
                       CstAid = i.First().CstAid,
                       CstBid = i.First().CstBid,
                       CstPisCofinsId = i.First().CstPisCofinsId,
                       NbsId = i.First().NbsId,
                       NcmId = i.First().NcmId,
                       UfId = i.First().UfId,
                       CfopCode = i.First().CfopCode,
                       isActive = i.First().isActive
                   }).ToList();

            // lista as tributações globais
            ret.AddRange(query.Where(x => x.PropertyUId == null)
                 .GroupBy(i => new { i.BarCode, i.NcmId, i.NbsId, i.UfId })
                 .Select(i => new TaxRulesDto()
                 {
                     PropertyList = i.Select(x => new PropertyDto
                     {
                         PropertyUId = x.PropertyUId,
                         TenantId = x.TenantId
                     }).ToList(),
                     ServiceTypeId = i.First().ServiceTypeId,
                     TaxCalculationId = i.First().TaxCalculationId,
                     UfInitials = i.First().UfInitials,
                     NatureId = i.First().NatureId,
                     BarCode = i.First().BarCode,
                     Name = i.First().Name,
                     Id = i.First().Id,
                     NcmCode = i.First().NcmCode,
                     NbsCode = i.First().NbsCode,
                     Type = i.First().Type,
                     TaxPercentualBase = i.First().TaxPercentualBase,
                     TaxPercentualSub = i.First().TaxPercentualSub,
                     TaxPercentual = i.First().TaxPercentual,
                     CfopId = i.First().CfopId,
                     CsosnId = i.First().CsosnId,
                     CstAid = i.First().CstAid,
                     CstBid = i.First().CstBid,
                     CstPisCofinsId = i.First().CstPisCofinsId,
                     NbsId = i.First().NbsId,
                     NcmId = i.First().NcmId,
                     UfId = i.First().UfId,
                     CfopCode = i.First().CfopCode,
                     isActive = i.First().isActive
                 }));

            return ret.ToListDto(requestDto);
        }

        public async Task<TaxRulesDto> GetById(Guid id)
        {
            var query = await base.QueryAsync<TaxRulesDto>(@"
                SELECT TaxRules.TaxRulesId           AS Id,
                       TaxRules.Nome                 AS Name,
                       TaxRules.CodigoBarras         AS Barcode,
                       TaxRules.NaturezaReceitaId    AS NatureId,
                       TaxRules.RegimeApuracaoId     AS TaxCalculationId,
                       TaxRules.TipoServicoid        AS ServiceTypeId,
                       TaxRules.Aliquota             AS TaxPercentual,
                       TaxRules.AliquotaSubstituicao AS TaxPercentualSub,
                       TaxRules.PercentualBase       AS TaxPercentualBase,
                       TaxRules.Tipo                 AS Type,
                       NCM.Codigo                    AS NcmCode,
                       NCM.CodigoCest                AS CestCode,
                       NBS.Codigo                    AS NbsCode,
                       CFOP.Codigo                   AS CfopCode,
                       UF.Sigla                      AS UfInitials,
                       CSOSN.Codigo                  AS Csosn,
                       CSOSN.CsosnId                 As CsosnId,
                       TaxRules.PropertyId           AS PropertyUId,
                       TaxRules.*
                FROM   TaxRules
                       LEFT OUTER JOIN BR_NCM AS NCM
                                    ON TaxRules.NcmId = NCM.NcmId
                       LEFT OUTER JOIN BR_NBS AS NBS
                                    ON TaxRules.NbsId = NBS.NbsId
                       LEFT OUTER JOIN BR_UF AS UF
                                    ON TaxRules.UfId = UF.UfId
                       LEFT OUTER JOIN BR_CFOP AS CFOP
                                    ON TaxRules.CfopId = CFOP.CfopId
                       LEFT OUTER JOIN BR_CSOSN AS CSOSN
                                    ON TaxRules.CsosnId = CSOSN.CsosnId
                WHERE TaxRules.TaxRulesId = @TaxRuleId",
                                    new
                                    {
                                        TaxRuleId = id
                                    });

            var ret = query.Where(x => x.PropertyUId != null)
                   .GroupBy(i => new { i.BarCode, i.NcmId, i.NbsId, i.UfId })
                   .Select(i => new TaxRulesDto()
                   {
                       PropertyList = i.Select(x => new PropertyDto
                       {
                           PropertyUId = x.PropertyUId,
                           TenantId = x.TenantId
                       }).ToList(),
                       ServiceTypeId = i.First().ServiceTypeId,
                       TaxCalculationId = i.First().TaxCalculationId,
                       UfInitials = i.First().UfInitials,
                       NatureId = i.First().NatureId,
                       BarCode = i.First().BarCode,
                       Name = i.First().Name,
                       Id = i.First().Id,
                       NcmCode = i.First().NcmCode,
                       NbsCode = i.First().NbsCode,
                       Type = i.First().Type,
                       TaxPercentualBase = i.First().TaxPercentualBase,
                       TaxPercentualSub = i.First().TaxPercentualSub,
                       TaxPercentual = i.First().TaxPercentual,
                       CfopId = i.First().CfopId,
                       CsosnId = i.First().CsosnId,
                       CstAid = i.First().CstAid,
                       CstBid = i.First().CstBid,
                       CstPisCofinsId = i.First().CstPisCofinsId,
                       NbsId = i.First().NbsId,
                       NcmId = i.First().NcmId,
                       UfId = i.First().UfId,
                       CfopCode = i.First().CfopCode,
                       isActive = i.First().isActive,
                       Csosn = i.First().Csosn
                   }).ToList();

            // lista as tributações globais
            ret.AddRange(query.Where(x => x.PropertyUId == null)
                 .GroupBy(i => new { i.BarCode, i.NcmId, i.NbsId, i.UfId })
                 .Select(i => new TaxRulesDto()
                 {
                     PropertyList = i.Select(x => new PropertyDto
                     {
                         PropertyUId = x.PropertyUId,
                         TenantId = x.TenantId
                     }).ToList(),
                     ServiceTypeId = i.First().ServiceTypeId,
                     TaxCalculationId = i.First().TaxCalculationId,
                     UfInitials = i.First().UfInitials,
                     NatureId = i.First().NatureId,
                     BarCode = i.First().BarCode,
                     Name = i.First().Name,
                     Id = i.First().Id,
                     NcmCode = i.First().NcmCode,
                     NbsCode = i.First().NbsCode,
                     Type = i.First().Type,
                     TaxPercentualBase = i.First().TaxPercentualBase,
                     TaxPercentualSub = i.First().TaxPercentualSub,
                     TaxPercentual = i.First().TaxPercentual,
                     CfopId = i.First().CfopId,
                     CsosnId = i.First().CsosnId,
                     CstAid = i.First().CstAid,
                     CstBid = i.First().CstBid,
                     CstPisCofinsId = i.First().CstPisCofinsId,
                     NbsId = i.First().NbsId,
                     NcmId = i.First().NcmId,
                     UfId = i.First().UfId,
                     CfopCode = i.First().CfopCode,
                     isActive = i.First().isActive,
                     Csosn = i.First().Csosn
                 }));

            return ret.FirstOrDefault();
        }

        public async Task<TaxRulesDto> GetTributes(Guid propertyUId, GetAllTaxRulesWithParamsDto requestDto)
        {
            var filter = string.Empty;
            var barcodeFilter = string.Empty;
            var ncmFilter = string.Empty;
            var nbsFilter = string.Empty;
            var propertyFilter = string.Format("AND TaxRules.PropertyId = '{0}'", propertyUId);
            var ufFilter = string.Format("AND UF.Sigla = '{0}'", _applicationUser.PropertyDistrictCode);

            if (!string.IsNullOrEmpty(requestDto.BarCode) && (!string.IsNullOrEmpty(requestDto.Ncm)))
            {
                filter = string.Format("AND (REPLACE(TaxRules.CodigoBarras, '.', '') = '{0}'" +
                                       " OR REPLACE(NCM.Codigo, '.', '') = '{1}')" +
                                       " AND TaxRules.Tipo = 0", requestDto.BarCode, requestDto.Ncm);
            }
            else if (!string.IsNullOrEmpty(requestDto.BarCode))
            {
                barcodeFilter = string.Format("AND REPLACE(TaxRules.CodigoBarras, '.', '') = '{0}'  AND TaxRules.Tipo = 0", requestDto.BarCode);
            }
            else if (!string.IsNullOrEmpty(requestDto.Ncm))
            {
                ncmFilter = string.Format("AND REPLACE(NCM.Codigo, '.', '') = '{0}' AND TaxRules.Tipo = 0", requestDto.Ncm);
            }
            else if (!string.IsNullOrEmpty(requestDto.Nbs))
            {
                nbsFilter = string.Format("AND REPLACE(NBS.Codigo, '.', '') = '{0}' AND TaxRules.Tipo = 1", requestDto.Nbs);
            }

            var query = @"
                SELECT TaxRules.TaxRulesId AS Id,
                       TaxRules.Nome AS Name,
                       TaxRules.CodigoBarras AS Barcode,
                       TaxRules.NaturezaReceitaId AS NatureId,
                       TaxRules.RegimeApuracaoId AS TaxCalculationId,
                       TaxRules.TipoServicoid AS ServiceTypeId,
                       TaxRules.Aliquota AS TaxPercentual,
                       TaxRules.AliquotaSubstituicao AS TaxPercentualSub,
                       TaxRules.PercentualBase AS TaxPercentualBase,
                       TaxRules.Tipo AS Type,
                       NCM.Codigo AS NcmCode,
                       NCM.CodigoCest AS CestCode,
                       NBS.Codigo AS NbsCode,
                       UF.Sigla AS UfInitials,
					   TipoServico.Codigo AS ServiceType,
					   RegimeApuracao.Descricao AS TaxCalculation,
					   NaturezaReceita.Codigo AS Nature,
					   CstPisCofins.Codigo AS CstPisCofins,
					   CstA.Codigo AS CstA,
					   CstB.Codigo AS CstB,
					   Csosn.Codigo AS Csosn,
					   Cfop.Codigo AS Cfop,
                       TaxRules.PropertyId           AS PropertyUId,
                       TaxRules.*
                FROM   TaxRules
                       LEFT OUTER JOIN BR_NCM AS NCM
                                    ON TaxRules.NcmId = NCM.NcmId
                       LEFT OUTER JOIN BR_NBS AS NBS
                                    ON TaxRules.NbsId = NBS.NbsId
                       LEFT OUTER JOIN BR_UF AS UF
                                    ON TaxRules.UfId = UF.UfId
                       LEFT OUTER JOIN BR_TipoServico AS TipoServico
                                    ON TaxRules.TipoServicoId = TipoServico.TipoServicoId
                       LEFT OUTER JOIN BR_RegimeApuracao AS RegimeApuracao
                                    ON TaxRules.RegimeApuracaoId = RegimeApuracao.RegimeApuracaoId
                       LEFT OUTER JOIN BR_NaturezaReceita AS NaturezaReceita
                                    ON TaxRules.NaturezaReceitaId = NaturezaReceita.NaturezaReceitaId
                       LEFT OUTER JOIN BR_CST AS CstPisCofins
                                    ON TaxRules.CstPisCofinsId = CstPisCofins.CstId
                       LEFT OUTER JOIN BR_CST AS CstA
                                    ON TaxRules.CstAId = CstA.CstId
                       LEFT OUTER JOIN BR_CST AS CstB
                                    ON TaxRules.CstBId = CstB.CstId
                       LEFT OUTER JOIN BR_CSOSN AS Csosn
                                    ON TaxRules.CsosnId = Csosn.CsosnId
                       LEFT OUTER JOIN BR_CFOP Cfop
                                    ON TaxRules.CfopId = Cfop.CfopId
                       WHERE TaxRules.isActive = 1 
                       {0}
                       {1}
                       {2}
                       {3}
                       {4}
                       {5}";

            // tenta recuperar a tributação para a property
            var result = await base.QueryAsync<TaxRulesDto>(string.Format(query, filter, propertyFilter, barcodeFilter, ncmFilter, nbsFilter, ufFilter));

            if (result.Count() > 0)
                return result.ToListDtoAsync(new GetAllTaxRulesDto()).Result.Items.FirstOrDefault();

            // caso não encontre recupera a tributação global
            propertyFilter = @"AND TaxRules.PropertyId IS NULL";
            result = await base.QueryAsync<TaxRulesDto>(string.Format(query, filter, propertyFilter, barcodeFilter, ncmFilter, nbsFilter, ufFilter));
            return result.ToListDtoAsync(new GetAllTaxRulesDto()).Result.Items.FirstOrDefault();
        }

        public async Task<Domain.Entities.TaxRule> GetEntityById(Guid id)
        {
            var query = await QueryAsync<Domain.Entities.TaxRule>(@"
                SELECT TaxRules.TaxRulesId           AS Id,
                       TaxRules.Nome                 AS Name,
                       TaxRules.CodigoBarras         AS Barcode,
                       TaxRules.NaturezaReceitaId    AS NatureId,
                       TaxRules.RegimeApuracaoId     AS TaxCalculationId,
                       TaxRules.TipoServicoid        AS ServiceTypeId,
                       TaxRules.Aliquota             AS TaxPercentual,
                       TaxRules.AliquotaSubstituicao AS TaxPercentualSub,
                       TaxRules.PercentualBase       AS TaxPercentualBase,
                       TaxRules.Tipo                 AS Type,
                       TaxRules.*
                FROM TaxRules
                WHERE TaxRules.isDeleted = 0
                AND TaxRules.TaxRulesId = @Id",
                new
                {
                    Id = id
                });

            return query.FirstOrDefault();
        }

        /// <summary>
        /// Valida as regras tributárias para os serviços
        /// </summary>
        /// <param name="serviceParams">Lista de NBS</param>
        /// <returns></returns>
        public async Task<bool> ValidateExistServiceTax(List<string> serviceParams)
        {
            var query = await base.QueryAsync<dynamic>(@"
                SELECT TaxRules.TaxRulesId           AS Id, 
                       TaxRules.Nome                 AS Name,
                       TaxRules.CodigoBarras         AS Barcode,
                       TaxRules.NaturezaReceitaId    AS NatureId,
                       TaxRules.RegimeApuracaoId     AS TaxCalculationId,
                       TaxRules.TipoServicoid        AS ServiceTypeId,
                       TaxRules.Aliquota             AS TaxPercentual,
                       TaxRules.AliquotaSubstituicao AS TaxPercentualSub,
                       TaxRules.PercentualBase       AS TaxPercentualBase,
                       TaxRules.Tipo                 AS Type,
                       NCM.Codigo                    AS NcmCode, 
                       NBS.Codigo                    AS NbsCode,
                       CFOP.Codigo                   AS CfopCode,
                       UF.Sigla                      AS UfInitials, 
                       TaxRules.* 
                FROM   TaxRules 
                       LEFT OUTER JOIN BR_NCM AS NCM 
                                    ON TaxRules.NcmId = NCM.NcmId
                       LEFT OUTER JOIN BR_NBS AS NBS
                                    ON TaxRules.NbsId = NBS.NbsId
                       LEFT OUTER JOIN BR_UF AS UF 
                                    ON TaxRules.UfId = UF.UfId
                       LEFT OUTER JOIN BR_CFOP AS CFOP 
                                    ON TaxRules.CfopId = CFOP.CfopId
                WHERE REPLACE(NBS.Codigo, '.', '')  in @Ids
                AND TaxRules.IsActive = 1
                AND TaxRules.IsDeleted = 0
                AND TaxRules.Tipo = 1
                AND UF.Sigla = @District",
                                   new
                                   {
                                       Ids = serviceParams,
                                       District = _applicationUser.PropertyDistrictCode
                                   });

            return query.Count() >= serviceParams.Count;
        }

        /// <summary>
        /// Valida as regras tributárias para os produtos
        /// </summary>
        /// <param name="productBarcodeParams">Lista de Código de barras</param>
        /// <param name="productNcmParams">Lista de NCM</param>
        /// <param name="totalProducts">Total de produtos</param>
        /// <returns></returns>
        public async Task<bool> ValidateExistProductTax(List<string> productBarcodeParams, List<string> productNcmParams, int totalProducts)
        {
            var validateParams = new List<string>();
            var filter = string.Empty;

            // Verifica se NCM e Codigo de barras foram informados
            if (productBarcodeParams.Count > 0 && productBarcodeParams.Count == totalProducts &&
                productNcmParams.Count > 0 && productNcmParams.Count == totalProducts)
            {
                filter = @"AND REPLACE(TaxRules.CodigoBarras, '.', '') in @validateParams
                          OR REPLACE(NCM.Codigo, '.', '')  in @validateParams";

                validateParams.AddRange(productBarcodeParams);
                validateParams.AddRange(productNcmParams);
            }
            // Verifica se o código de barras de todos os produtos foram informados
            else if (productBarcodeParams.Count > 0 && productBarcodeParams.Count == totalProducts)
            {
                filter = "AND REPLACE(TaxRules.CodigoBarras, '.', '') in (@validateParams)";
                validateParams.AddRange(productBarcodeParams);
            }
            // Verifica se o código ncm de todos os produtos foram informados
            else if (productNcmParams.Count > 0 && productNcmParams.Count == totalProducts)
            {
                filter = "AND REPLACE(NCM.Codigo, '.', '')  in (@validateParams)";
                validateParams.AddRange(productNcmParams);
            }
            // Verifica se o código ncm ou código de barras foram informados para todos os produtos
            else if (productBarcodeParams.Count > 0 && productNcmParams.Count > 0 && (productBarcodeParams.Count + productNcmParams.Count) == totalProducts)
            {
                validateParams.AddRange(productBarcodeParams);
                validateParams.AddRange(productNcmParams);

                filter = @"AND REPLACE(TaxRules.CodigoBarras, '.', '') in @validateParams
                          OR REPLACE(NCM.Codigo, '.', '')  in @validateParams";
            }

            var query = await base.QueryAsync<dynamic>(string.Format(@"
                SELECT TaxRules.TaxRulesId
                FROM   TaxRules 
                       LEFT OUTER JOIN BR_NCM AS NCM 
                                    ON TaxRules.NcmId = NCM.NcmId
                       LEFT OUTER JOIN BR_NBS AS NBS
                                    ON TaxRules.NbsId = NBS.NbsId
                       LEFT OUTER JOIN BR_UF AS UF 
                                    ON TaxRules.UfId = UF.UfId
                WHERE TaxRules.IsActive = 1
                AND TaxRules.IsDeleted = 0
                AND TaxRules.Tipo = 0
                AND UF.Sigla = @District
                {0}", filter),
                new
                {
                    validateParams = validateParams,
                    District = _applicationUser.PropertyDistrictCode
                });

            return query.Count() >= totalProducts;
        }

        public async Task<TaxRulesDto> GetTributes(Guid propertyUUId, GetAllEuropeTaxRulesWithParamsDto requestDto)
        {
            var productFilter = string.Empty;
            var serviceFilter = string.Empty;
            var propertyFilter = string.Format("AND PT_TaxRules.PropertyId = '{0}'", propertyUUId);

            if (!string.IsNullOrEmpty(requestDto.ProductId))
            {
                productFilter = string.Format("AND PT_TaxRules.ProductId = '{0}'", requestDto.ProductId);
            }
            else if (!string.IsNullOrEmpty(requestDto.ServiceId))
            {
                serviceFilter = string.Format("AND PT_TaxRules.ServiceId = '{0}'", requestDto.ServiceId);
            }

            var query = @"
                SELECT  PT_TaxRules.TaxRulesId as Id,
                        PT_IVA.Description as Name,
                        PT_IVA.Tax as TaxPercentual,
                        PT_TaxRules .ProductId as PropertyUId,
                        PT_TaxRules.*
                        FROM PT_TaxRules
                        LEFT OUTER JOIN PT_IVA on PT_TaxRules.IvaId = PT_IVA.IvaId
                        WHERE TaxRules.isActive = 1 
                        {0}
                        {1}
                        {2}";

            // tenta recuperar a tributação para a property
            var result = await base.QueryAsync<TaxRulesDto>(string.Format(query, propertyFilter, productFilter, serviceFilter));

            if (result.Count() > 0)
                return result.ToListDtoAsync(new GetAllTaxRulesDto()).Result.Items.FirstOrDefault();

            // caso não encontre recupera a tributação global
            propertyFilter = @"AND TaxRules.PropertyId IS NULL";
            result = await base.QueryAsync<TaxRulesDto>(string.Format(query, propertyFilter, productFilter, serviceFilter));
            return result.ToListDtoAsync(new GetAllTaxRulesDto()).Result.Items.FirstOrDefault();
        }
    }
}
