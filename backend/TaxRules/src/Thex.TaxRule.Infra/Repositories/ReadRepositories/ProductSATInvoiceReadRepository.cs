﻿using System.Threading.Tasks;
using Thex.TaxRule.Dto.Dto.Integration.NFCe;
using Thex.TaxRule.Dto.Dto.Integration.NFSe;
using Thex.TaxRule.Infra.Interfaces.ReadRepositories;
using Tnf.MongoDb.Provider;
using Tnf.MongoDb.Repositories;

namespace Thex.TaxRule.Infra.Repositories.ReadRepositories
{
    public class ProductSATInvoiceReadRepository : MongoDbRepository<SATInvoice, string>, IProductSATInvoiceReadRepository
    {
        public ProductSATInvoiceReadRepository(IMongoDbProvider provider) : base(provider)
        {
        }

        public async Task<SATInvoice> GetProductInvoice(string invoiceId)
        {
            return await GetAsync(invoiceId.ToLower());
        }
    }
}
