﻿using System;
using System.Collections.Generic;
using System.Linq;
using Thex.GenericLog;
using Thex.Kernel;
using Thex.TaxRule.Domain.Entities;
using Thex.TaxRule.Dto;
using Thex.TaxRule.Infra.Interfaces.ReadRepositories;
using Thex.TaxRules.Infra;
using Tnf.Dapper.Repositories;
using Tnf.Repositories;

namespace Thex.TaxRule.Infra.Repositories.ReadRepositories
{
    public class PTTaxRuleReadRepository : DapperEfRepositoryBase<ThexTaxRuleContext, PTTaxRule>, IPTTaxRuleReadRepository
    {
        private readonly IApplicationUser _applicationUser;

        public PTTaxRuleReadRepository(
               IActiveTransactionProvider activeTransactionProvider,
               IGenericLogHandler genericLogHandler,
               IApplicationUser applicationUser) : base(activeTransactionProvider, genericLogHandler)
        {
            _applicationUser = applicationUser;
        }

        public List<PTTaxRuleDto> GetAllByIvaIdList(List<Guid> ivaIdList)
        {
            var tenantId = _applicationUser.TenantId;

            var query =  base.Query<PTTaxRuleDto>(@"
                SELECT PT_TaxRules.TaxRulesId as ID,
                    PT_TaxRules.ServiceId,
                    PT_TaxRules.ProductId,
                    PT_TaxRules.IvaId,
                    PT_TaxRules.PropertyId,
                    PT_TaxRules.Description,
                    PT_TaxRules.ExemptionCode
                    FROM PT_TaxRules
                WHERE IvaId IN @ivaIdList AND TenantId = @tenantId",
                new
                {
                    ivaIdList,
                    tenantId
                });

            return query.ToList();
        }

        public bool AnyExceptInIvaId(Guid ivaId, List<int> serviceIdList, List<Guid> productIdList)
        {
            var tenantId = _applicationUser.TenantId;

            var query = base.Query<PTTaxRuleDto>(@"
                SELECT PT_TaxRules.TaxRulesId
                FROM PT_TaxRules
                WHERE IvaId != @ivaId 
                AND (ServiceId IN @serviceIdList
                OR ProductId IN @productIdList)
                AND TenantId = @tenantId ",
                new { ivaId, serviceIdList, productIdList, tenantId });

            return query.Any();
        }
    }
}
