﻿using System;
using System.Threading.Tasks;
using Thex.GenericLog;
using Thex.TaxRule.Domain.Entities;
using Thex.TaxRule.Dto;
using Thex.TaxRule.Dto.GetAll;
using Thex.TaxRule.Infra.Interfaces.ReadRepositories;
using Thex.TaxRules.Infra;
using Tnf.Dapper.Repositories;
using Tnf.Dto;
using Tnf.Repositories;

namespace hex.TaxRule.Infra.Repositories
{
    public class ImpostoRetidoReadRepository : DapperEfRepositoryBase<ThexTaxRuleContext, ImpostoRetido>, IImpostoRetidoReadRepository
    {
        public ImpostoRetidoReadRepository(
               IActiveTransactionProvider activeTransactionProvider,
               IGenericLogHandler genericLogHandler) : base(activeTransactionProvider, genericLogHandler)
        {
        }

        public async Task<IListDto<ImpostoRetidoDto>> GetAllByOwnerIdAndPropertyUid(GetAllImpostoRetidoDto requestDto, Guid propertyUid)
        {
            var query = base.Query<ImpostoRetidoDto>(@"
                SELECT BR_ImpostoRetido.ImpostoRetidoId as Id,
                       BR_ImpostoRetido.OwnerId as OwnerId,
                       BR_ImpostoRetido.PropertyUId as PropertyUId
                FROM BR_ImpostoRetido
                WHERE PropertyUId = @PropertyUId",
                new
                {
                    PropertyUId = propertyUid
                });

            return await query.ToListDtoAsync(requestDto);
        }
    }
}
