﻿using System.Linq;
using System.Threading.Tasks;
using Thex.GenericLog;
using Thex.Kernel;
using Thex.TaxRule.Domain.Entities;
using Thex.TaxRule.Dto;
using Thex.TaxRule.Infra.Interfaces.ReadRepositories;
using Thex.TaxRules.Infra;
using Tnf.Dapper.Repositories;
using Tnf.Dto;
using Tnf.Repositories;

namespace Thex.TaxRule.Infra.Repositories
{
    public class NaturezaReceitaReadRepository : DapperEfRepositoryBase<ThexTaxRuleContext, NaturezaReceita>, INaturezaReceitaReadRepository
    {
        private readonly IApplicationUser _applicationUser;

        public NaturezaReceitaReadRepository(
               IActiveTransactionProvider activeTransactionProvider,
               IApplicationUser applicationUser,
               IGenericLogHandler genericLogHandler) : base(activeTransactionProvider, genericLogHandler)
        {
            _applicationUser = applicationUser;
        }

        public async Task<IListDto<NaturezaReceitaDto>> GetAll(GetAllNaturezaReceitaDto requestDto)
        {
            var query = base.Query<NaturezaReceitaDto>(@"
                SELECT BR_NaturezaReceita.NaturezaReceitaId as Id,
                       BR_NaturezaReceita.Codigo as Code,
                       BR_NaturezaReceita.Descricao as Description,
                       BR_NaturezaReceita.*
                FROM BR_NaturezaReceita");

            return await query.ToListDtoAsync(requestDto);
        }

        public async Task<NaturezaReceitaDto> GetByCode(string code, GetAllNaturezaReceitaDto requestDto)
        {
            var query = await base.QueryAsync<NaturezaReceitaDto>(@"
                SELECT BR_NaturezaReceita.NaturezaReceitaId as Id,
                       BR_NaturezaReceita.Codigo as Code,
                       BR_NaturezaReceita.Descricao as Description,
                       BR_NaturezaReceita.*
                FROM BR_NaturezaReceita
                WHERE BR_NaturezaReceita.Codigo = @NaturezaReceita",
                new
                {
                    NaturezaReceita = code
                });

            return query.ToListDtoAsync(requestDto).Result.Items.FirstOrDefault();
        }

        public async Task<IListDto<NaturezaReceitaDto>> GetByNcm(string ncm, GetAllNaturezaReceitaDto requestDto)
        {
            var query = base.Query<NaturezaReceitaDto>(@"
                SELECT BR_NaturezaReceita.NaturezaReceitaId as Id,
                       BR_NaturezaReceita.Codigo as Code,
                       BR_NaturezaReceita.Descricao as Description,
                       BR_NaturezaReceita.*
                FROM BR_NaturezaReceita
                LEFT JOIN BR_NCM ON BR_NaturezaReceita.NcmId = BR_NCM.NcmId
                WHERE BR_NCM.Codigo = @Codigo",
                 new
                 {
                     Codigo = ncm
                 });

            return await query.ToListDtoAsync(requestDto);
        }
    }
}