﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Thex.GenericLog;
using Thex.Kernel;
using Thex.TaxRule.Domain.Entities;
using Thex.TaxRule.Dto;
using Thex.TaxRule.Infra.Interfaces.ReadRepositories;
using Thex.TaxRules.Infra;
using Tnf.Dapper.Repositories;
using Tnf.Dto;
using Tnf.Repositories;

namespace Thex.TaxRule.Infra.Repositories
{
    public class UFReadRepository : DapperEfRepositoryBase<ThexTaxRuleContext, UF>, IUFReadRepository
    {
        private readonly IApplicationUser _applicationUser;

        public UFReadRepository(
               IActiveTransactionProvider activeTransactionProvider,
               IApplicationUser applicationUser,
               IGenericLogHandler genericLogHandler) : base(activeTransactionProvider, genericLogHandler)
        {
            _applicationUser = applicationUser;
        }

        public async Task<IListDto<UFDto>> GetAll(GetAllUFDto requestDto)
        {
            var query = base.Query<UFDto>(@"
                SELECT BR_UF.UfId as Id,
                       BR_UF.CodigoIbge as IbgeCode,
                       BR_UF.Sigla as Initials,
                       BR_UF.*
                FROM BR_UF");

            return await query.ToListDtoAsync(requestDto);
        }

        public async Task<UFDto> GetByCode(GetAllUFDto requestDto)
        {
            var query = await base.QueryAsync<UFDto>(@"
                SELECT BR_UF.UfId as Id,
                       BR_UF.CodigoIbge as IbgeCode,
                       BR_UF.Sigla as Initials,
                       BR_UF.*
                FROM BR_UF
                WHERE BR_UF.Sigla = @Sigla",
                new
                {
                    Sigla = _applicationUser.PropertyDistrictCode
                });

            return query.ToListDtoAsync(requestDto).Result.Items.FirstOrDefault();
        }
    }
}