﻿using System.Linq;
using System.Threading.Tasks;
using Thex.GenericLog;
using Thex.Kernel;
using Thex.TaxRule.Domain.Entities;
using Thex.TaxRule.Dto;
using Thex.TaxRule.Dto.GetAll;
using Thex.TaxRule.Infra.Interfaces.ReadRepositories;
using Thex.TaxRules.Infra;
using Tnf.Dapper.Repositories;
using Tnf.Dto;
using Tnf.Repositories;

namespace Thex.TaxRule.Infra.Repositories
{
    public class NCMReadRepository : DapperEfRepositoryBase<ThexTaxRuleContext, NCM>, INCMReadRepository
    {
        private readonly IApplicationUser _applicationUser;

        public NCMReadRepository(
               IActiveTransactionProvider activeTransactionProvider,
               IApplicationUser applicationUser,
               IGenericLogHandler genericLogHandler) : base(activeTransactionProvider, genericLogHandler)
        {
            _applicationUser = applicationUser;
        }

        public async Task<IListDto<NCMDto>> GetAll(GetAllNCMDto requestDto)
        {
            var query = base.Query<NCMDto>(@"
                SELECT BR_NCM.NcmId as Id,
                       BR_NCM.Codigo as Code,
                       BR_NCM.Descricao as Description,
                       BR_NCM.*
                FROM BR_NCM");

            return await query.ToListDtoAsync(requestDto);
        }

        public async Task<IListDto<NCMDto>> GetAllByFilters(SearchNcmDto searchDto)
        {
            var query = base.Query<NCMDto>(@"
                SELECT BR_NCM.NcmId as Id,
                       BR_NCM.Codigo as Code,
                       BR_NCM.Descricao as Description,
                       BR_NCM.*
                FROM BR_NCM
                WHERE 
                (
				    (
				       BR_NCM.Codigo like @Param + '%'
				       OR BR_NCM.Codigo like '%' + @Param + '%'
				       OR BR_NCM.Codigo like '%' + @Param
				       OR BR_NCM.Codigo = @Param
			        )	 
                    OR 
				    (
				       BR_NCM.Descricao like @Param + '%'
				       OR BR_NCM.Descricao like '%' + @Param + '%'
				       OR BR_NCM.Descricao like '%' + @Param
				       OR BR_NCM.Descricao = @Param
			        ))",
                new
                {
                    searchDto.Param
                });

            return await query.ToListDtoAsync(searchDto);
        }

        public async Task<NCMDto> GetByCode(string code, GetAllNCMDto requestDto)
        {
            var query = await base.QueryAsync<NCMDto>(@"
                SELECT BR_NCM.NcmId as Id,
                       BR_NCM.Codigo as Code,
                       BR_NCM.Descricao as Description,
                       BR_NCM.*
                FROM BR_NCM
                WHERE BR_NCM.Codigo = @Codigo",
                new
                {
                    Codigo = code
                });

            return query.ToListDtoAsync(requestDto).Result.Items.FirstOrDefault();
        }
    }
}