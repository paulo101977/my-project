﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Thex.GenericLog;
using Thex.Kernel;
using Thex.TaxRule.Domain.Entities;
using Thex.TaxRule.Dto;
using Thex.TaxRule.Infra.Interfaces.ReadRepositories;
using Thex.TaxRules.Infra;
using Tnf.Dapper.Repositories;
using Tnf.Dto;
using Tnf.Repositories;

namespace Thex.TaxRule.Infra.Repositories
{
    public class TipoServicoReadRepository : DapperEfRepositoryBase<ThexTaxRuleContext, TipoServico>, ITipoServicoReadRepository
    {
        private readonly IApplicationUser _applicationUser;

        public TipoServicoReadRepository(
               IActiveTransactionProvider activeTransactionProvider,
               IApplicationUser applicationUser,
               IGenericLogHandler genericLogHandler) : base(activeTransactionProvider, genericLogHandler)
        {
            _applicationUser = applicationUser;
        }

        public async Task<IListDto<TipoServicoDto>> GetAll(GetAllTipoServicoDto requestDto)
        {
            var query = base.Query<TipoServicoDto>(@"
                SELECT BR_TipoServico.TipoServicoId as Id,
                       BR_TipoServico.Codigo as Code,
                       BR_TipoServico.Descricao as Description,
                       BR_TipoServico.*
                FROM BR_TipoServico");

            return await query.ToListDtoAsync(requestDto);
        }

        public async Task<TipoServicoDto> GetByCode(string code, GetAllTipoServicoDto requestDto)
        {
            var query = await base.QueryAsync<TipoServicoDto>(@"
                SELECT BR_TipoServico.TipoServicoId as Id,
                       BR_TipoServico.Codigo as Code,
                       BR_TipoServico.Descricao as Description,
                       BR_TipoServico.*
                FROM BR_TipoServico
                WHERE BR_TipoServico.Codigo = @Codigo",
                new
                {
                    Codigo = code
                });

            return query.ToListDtoAsync(requestDto).Result.Items.FirstOrDefault();
        }
    }
}