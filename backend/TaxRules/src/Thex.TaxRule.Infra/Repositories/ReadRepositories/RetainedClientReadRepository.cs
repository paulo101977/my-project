﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Thex.GenericLog;
using Thex.Kernel;
using Thex.TaxRule.Domain.Entities;
using Thex.TaxRule.Dto;
using Thex.TaxRule.Infra.Interfaces.ReadRepositories;
using Thex.TaxRules.Infra;
using Tnf.Dapper.Repositories;
using Tnf.Repositories;

namespace Thex.TaxRule.Infra.Repositories.ReadRepositories
{
    public class RetainedClientReadRepository : DapperEfRepositoryBase<ThexTaxRuleContext, ImpostoRetido>, IRetainedClientReadRepository
    {
        private readonly IApplicationUser _applicationUser;

        public RetainedClientReadRepository(
               IActiveTransactionProvider activeTransactionProvider,
               IApplicationUser applicationUser,
               IGenericLogHandler genericLogHandler) : base(activeTransactionProvider, genericLogHandler)
        {
            _applicationUser = applicationUser;
        }

        public bool isRetainedClient(Guid guid)
        {
            var ret = QueryAsync<ImpostoRetidoDto>(@"
                SELECT BR_ImpostoRetido.ImpostoRetidoId as Id,
                       BR_ImpostoRetido.*
                FROM BR_ImpostoRetido
                WHERE BR_ImpostoRetido.OwnerId = @Codigo",
                                new
                                {
                                    Codigo = guid
                                }).Result;

            if (ret.Count() > 0)
                return true;

            return false;
        }
    }
}