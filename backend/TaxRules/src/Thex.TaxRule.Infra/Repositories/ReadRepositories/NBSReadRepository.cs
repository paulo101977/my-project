﻿using System.Linq;
using System.Threading.Tasks;
using Thex.GenericLog;
using Thex.Kernel;
using Thex.TaxRule.Domain.Entities;
using Thex.TaxRule.Dto;
using Thex.TaxRule.Dto.GetAll;
using Thex.TaxRule.Infra.Interfaces.ReadRepositories;
using Thex.TaxRules.Infra;
using Tnf.Dapper.Repositories;
using Tnf.Dto;
using Tnf.Repositories;

namespace Thex.TaxRule.Infra.Repositories
{
    public class NBSReadRepository : DapperEfRepositoryBase<ThexTaxRuleContext, NBS>, INBSReadRepository
    {
        private readonly IApplicationUser _applicationUser;

        public NBSReadRepository(
               IActiveTransactionProvider activeTransactionProvider,
               IApplicationUser applicationUser,
               IGenericLogHandler genericLogHandler) : base(activeTransactionProvider, genericLogHandler)
        {
            _applicationUser = applicationUser;
        }

        public async Task<IListDto<NBSDto>> GetAll(GetAllNBSDto requestDto)
        {
            var query = base.Query<NBSDto>(@"
                SELECT BR_NBS.NbsId as Id,
                       BR_NBS.Codigo as Code,
                       BR_NBS.Descricao as Description,
                       BR_NBS.*
                FROM BR_NBS");

            return await query.ToListDtoAsync(requestDto);
        }

        public async Task<IListDto<NBSDto>> GetAllByFilters(SearchNbsDto searchDto)
        {
            var query = base.Query<NBSDto>(@"
                SELECT BR_NBS.NbsId as Id,
                       BR_NBS.Codigo as Code,
                       BR_NBS.Descricao as Description,
                       BR_NBS.*
                FROM BR_NBS
                WHERE 
                (
				    (
				       BR_NBS.Codigo like @Param + '%'
				       OR BR_NBS.Codigo like '%' + @Param + '%'
				       OR BR_NBS.Codigo like '%' + @Param
				       OR BR_NBS.Codigo = @Param
			        )	 
                    OR 
				    (
				       BR_NBS.Descricao like @Param + '%'
				       OR BR_NBS.Descricao like '%' + @Param + '%'
				       OR BR_NBS.Descricao like '%' + @Param
				       OR BR_NBS.Descricao = @Param
			        )	
                )",
                new
                {
                    searchDto.Param
                });

            return await query.ToListDtoAsync(searchDto);
        }

        public async Task<NBSDto> GetByCode(string code, GetAllNBSDto requestDto)
        {
            var query = await base.QueryAsync<NBSDto>(@"
                SELECT BR_NBS.NbsId as Id,
                       BR_NBS.Codigo as Code,
                       BR_NBS.Descricao as Description,
                       BR_NBS.*
                FROM BR_NBS
                WHERE BR_NBS.Codigo = @Codigo",
                new
                {
                    Codigo = code
                });

            return query.ToListDtoAsync(requestDto).Result.Items.FirstOrDefault();
        }
    }
}