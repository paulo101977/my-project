﻿using System.Linq;
using System.Threading.Tasks;
using Thex.TaxRule.Dto.InvoiceXpressIntegration;
using Thex.TaxRule.Infra.Interfaces.ReadRepositories;
using Tnf.MongoDb.Provider;
using Tnf.MongoDb.Repositories;

namespace Thex.TaxRule.Infra.Repositories.ReadRepositories
{
    public class FacturaInvoiceReadRepository : MongoDbRepository<FacturaInvoice, string>, IFacturaInvoiceReadRepository
    {
        public FacturaInvoiceReadRepository(IMongoDbProvider provider) : base(provider)
        {
        }

        public async Task<FacturaInvoice> GetServiceInvoice(string invoiceId)
        {
            return await GetAsync(invoiceId.ToLower());
        }

        public async Task<FacturaInvoice> GetServiceInvoiceByExternalInvoiceId(string invoiceId)
        {
            var invoiceList = await WhereAsync(x => x.FacturaId == invoiceId);

            return invoiceList.FirstOrDefault();
        }

        public async Task<FacturaInvoice> GetServiceInvoiceByClientCode(string clientCode)
        {
            var invoiceList = await WhereAsync(x => x.PersonDetails.Code.ToUpper() == clientCode.ToUpper());

            return invoiceList.FirstOrDefault();
        }
    }
}
