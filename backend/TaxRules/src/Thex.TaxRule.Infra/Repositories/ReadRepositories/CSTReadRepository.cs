﻿using System.Linq;
using System.Threading.Tasks;
using Thex.GenericLog;
using Thex.Kernel;
using Thex.TaxRule.Domain.Entities;
using Thex.TaxRule.Dto;
using Thex.TaxRule.Infra.Interfaces.ReadRepositories;
using Thex.TaxRules.Infra;
using Tnf.Dapper.Repositories;
using Tnf.Dto;
using Tnf.Repositories;

namespace Thex.TaxRule.Infra.Repositories
{
    public class CSTReadRepository : DapperEfRepositoryBase<ThexTaxRuleContext, CST>, ICSTReadRepository
    {
        private readonly IApplicationUser _applicationUser;

        public CSTReadRepository(
               IActiveTransactionProvider activeTransactionProvider,
               IApplicationUser applicationUser,
               IGenericLogHandler genericLogHandler) : base(activeTransactionProvider, genericLogHandler)
        {
            _applicationUser = applicationUser;
        }

        public async Task<IListDto<CSTDto>> GetAll(GetAllCSTDto requestDto)
        {
            var query = base.Query<CSTDto>(@"
                SELECT BR_CST.CstId as Id,
                       BR_CST.Codigo as Code,
                       BR_CST.Descricao as Description,
                       BR_CST.*
                FROM BR_CST");

            return await query.ToListDtoAsync(requestDto);
        }

        public async Task<IListDto<CSTDto>> GetAllByType(int tipo, GetAllCSTDto requestDto)
        {
            var query = base.Query<CSTDto>(@"
                SELECT BR_CST.CstId as Id,
                       BR_CST.Codigo as Code,
                       BR_CST.Descricao as Description,
                       BR_CST.*
                FROM BR_CST
                WHERE BR_CST.Tipo = @Type",
                new
                {
                    Type = tipo
                });

            return await query.ToListDtoAsync(requestDto);
        }

        public async Task<CSTDto> GetByCode(string code, GetAllCSTDto requestDto)
        {
            var query = await base.QueryAsync<CSTDto>(@"
                SELECT BR_CST.CstId as Id,
                       BR_CST.Codigo as Code,
                       BR_CST.Descricao as Description,
                       BR_CST.*
                FROM BR_CST
                WHERE BR_CST.Codigo = @Code",
                new
                {
                    Code = code
                });

            return query.ToListDtoAsync(requestDto).Result.Items.FirstOrDefault();
        }
    }
}