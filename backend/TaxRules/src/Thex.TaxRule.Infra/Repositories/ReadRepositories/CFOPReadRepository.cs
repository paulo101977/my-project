﻿using System.Linq;
using System.Threading.Tasks;
using Thex.GenericLog;
using Thex.Kernel;
using Thex.TaxRule.Domain.Entities;
using Thex.TaxRule.Dto;
using Thex.TaxRule.Dto.GetAll;
using Thex.TaxRule.Infra.Interfaces.ReadRepositories;
using Thex.TaxRules.Infra;
using Tnf.Dapper.Repositories;
using Tnf.Dto;
using Tnf.Repositories;

namespace Thex.TaxRule.Infra.Repositories
{
    public class CFOPReadRepository : DapperEfRepositoryBase<ThexTaxRuleContext, CFOP>, ICFOPReadRepository
    {
        private readonly IApplicationUser _applicationUser;

        public CFOPReadRepository(
               IActiveTransactionProvider activeTransactionProvider,
               IApplicationUser applicationUser,
               IGenericLogHandler genericLogHandler) : base(activeTransactionProvider, genericLogHandler)
        {
            _applicationUser = applicationUser;
        }

        public async Task<IListDto<CFOPDto>> GetAllByFilters(SearchCfopDto searchDto)
        {
            var query = base.Query<CFOPDto>(@"
                SELECT BR_CFOP.CfopId as Id,
                       BR_CFOP.Codigo as Code,
                       BR_CFOP.Descricao as Description,
                       BR_CFOP.Ativo
                FROM BR_CFOP
                WHERE 
                (
				    (
				       BR_CFOP.Codigo like @Param + '%'
				       OR BR_CFOP.Codigo like '%' + @Param + '%'
				       OR BR_CFOP.Codigo like '%' + @Param
				       OR BR_CFOP.Codigo = @Param
			        )	 
                    OR 
				    (
				       BR_CFOP.Descricao like @Param + '%'
				       OR BR_CFOP.Descricao like '%' + @Param + '%'
				       OR BR_CFOP.Descricao like '%' + @Param
				       OR BR_CFOP.Descricao = @Param
			        ))",
                    new
                    {
                        searchDto.Param
                    });



            return await query.ToListDtoAsync(searchDto);
        }

        public async Task<IListDto<CFOPDto>> GetAll(GetAllCFOPDto requestDto)
        {
            var query = base.Query<CFOPDto>(@"
                SELECT BR_CFOP.CfopId as Id,
                       BR_CFOP.Codigo as Code,
                       BR_CFOP.Descricao as Description,
                       BR_CFOP.Ativo
                FROM BR_CFOP");

            return await query.ToListDtoAsync(requestDto);
        }

        public async Task<CFOPDto> GetByCode(string code, GetAllCFOPDto requestDto)
        {
            var query = await base.QueryAsync<CFOPDto>(@"
                SELECT BR_CFOP.CfopId as Id,
                       BR_CFOP.Codigo as Code,
                       BR_CFOP.Descricao as Description,
                       BR_CFOP.Ativo
                FROM BR_CFOP
                WHERE BR_CFOP.Codigo = @CFOP",
                new
                {
                    CFOP = code
                });

            return query.ToListDtoAsync(requestDto).Result.Items.FirstOrDefault();
        }
    }
}