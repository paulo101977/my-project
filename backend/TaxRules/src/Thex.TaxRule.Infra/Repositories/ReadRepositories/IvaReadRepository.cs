using System;
using System.Linq;
using System.Threading.Tasks;
using Thex.GenericLog;
using Thex.TaxRule.Domain.Entities;
using Thex.TaxRule.Dto;
using Thex.TaxRule.Infra.Interfaces.ReadRepositories;
using Thex.TaxRules.Infra;
using Tnf.Dapper.Repositories;
using Tnf.Dto;
using Tnf.Repositories;

namespace Thex.TaxRule.Infra.Repositories.ReadRepositories
{
    public class IvaReadRepository : DapperEfRepositoryBase<ThexTaxRuleContext, Iva>, IIvaReadRepository
    {
        public IvaReadRepository(
               IActiveTransactionProvider activeTransactionProvider,
               IGenericLogHandler genericLogHandler) : base(activeTransactionProvider, genericLogHandler)
        {
        }

        public async Task<IListDto<IvaDto>> GetAll(GetAllIvaDto getAllIvaDto)
        {
            var query = await base.QueryAsync<IvaDto>(@"
                SELECT PT_IVA.IvaId as Id,
                       PT_IVA.Description,
                       PT_IVA.Location,
                       PT_IVA.Tax,
                       PT_IVA.IsActive
                FROM PT_IVA
                WHERE PT_IVA.IsActive = @IsActive",
                new
                {
                    getAllIvaDto.IsActive
                });

            return await query.ToListDtoAsync(getAllIvaDto);
        }

        public IvaDto GetById(Guid id)
        {
            var query = base.Query<IvaDto>(@"
                SELECT PT_IVA.IvaId as Id,
                PT_IVA.Description,
                PT_IVA.Tax
                FROM PT_IVA
                WHERE PT_IVA.IvaId = @id",
                new
                {
                    id
                });

            return  query.FirstOrDefault();
        }
    }
}
