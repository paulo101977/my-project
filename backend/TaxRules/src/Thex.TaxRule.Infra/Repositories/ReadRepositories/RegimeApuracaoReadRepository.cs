﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Thex.GenericLog;
using Thex.Kernel;
using Thex.TaxRule.Domain.Entities;
using Thex.TaxRule.Dto;
using Thex.TaxRule.Infra.Interfaces.ReadRepositories;
using Thex.TaxRules.Infra;
using Tnf.Dapper.Repositories;
using Tnf.Dto;
using Tnf.Repositories;

namespace Thex.TaxRule.Infra.Repositories
{
    public class RegimeApuracaoReadRepository : DapperEfRepositoryBase<ThexTaxRuleContext, RegimeApuracao>, IRegimeApuracaoReadRepository
    {
        private readonly IApplicationUser _applicationUser;

        public RegimeApuracaoReadRepository(
               IActiveTransactionProvider activeTransactionProvider,
               IApplicationUser applicationUser,
               IGenericLogHandler genericLogHandler) : base(activeTransactionProvider, genericLogHandler)
        {
            _applicationUser = applicationUser;
        }

        public async Task<IListDto<RegimeApuracaoDto>> GetAll(GetAllRegimeApuracaoDto requestDto)
        {
            var query = base.Query<RegimeApuracaoDto>(@"
                SELECT BR_RegimeApuracao.RegimeApuracaoId as Id,
                       BR_RegimeApuracao.Descricao as Description,
                       BR_RegimeApuracao.*
                FROM BR_RegimeApuracao");

            return await query.ToListDtoAsync(requestDto);
        }

        public async Task<RegimeApuracaoDto> GetByCode(Guid id, GetAllRegimeApuracaoDto requestDto)
        {
            var query = await base.QueryAsync<RegimeApuracaoDto>(@"
                SELECT BR_RegimeApuracao.RegimeApuracaoId as Id,
                       BR_RegimeApuracao.Descricao as Description,
                       BR_RegimeApuracao.*
                FROM BR_RegimeApuracao
                WHERE BR_RegimeApuracao.RegimeApuracaoId = @Id",
                new
                {
                    Id = id
                });

            return query.ToListDtoAsync(requestDto).Result.Items.FirstOrDefault();
        }
    }
}