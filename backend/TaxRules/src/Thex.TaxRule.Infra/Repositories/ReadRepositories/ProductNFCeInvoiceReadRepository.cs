﻿using System.Threading.Tasks;
using Thex.TaxRule.Dto.Dto.Integration.NFCe;
using Thex.TaxRule.Dto.Dto.Integration.NFSe;
using Thex.TaxRule.Infra.Interfaces.ReadRepositories;
using Tnf.MongoDb.Provider;
using Tnf.MongoDb.Repositories;

namespace Thex.TaxRule.Infra.Repositories.ReadRepositories
{
    public class ProductNFCeInvoiceReadRepository : MongoDbRepository<NFCeInvoice, string>, IProductNFCeInvoiceReadRepository
    {
        public ProductNFCeInvoiceReadRepository(IMongoDbProvider provider) : base(provider)
        {
        }

        public async Task<NFCeInvoice> GetProductInvoice(string invoiceId)
        {
            return await GetAsync(invoiceId.ToLower());
        }
    }
}
