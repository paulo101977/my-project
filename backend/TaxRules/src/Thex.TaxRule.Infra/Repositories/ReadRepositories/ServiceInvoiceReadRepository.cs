﻿using System.Threading.Tasks;
using Thex.TaxRule.Dto.Dto.Integration.NFSe;
using Thex.TaxRule.Infra.Interfaces.ReadRepositories;
using Tnf.MongoDb.Provider;
using Tnf.MongoDb.Repositories;

namespace Thex.TaxRule.Infra.Repositories.ReadRepositories
{
    public class ServiceInvoiceReadRepository : MongoDbRepository<NFSeInvoice, string>, IServiceInvoiceReadRepository
    {
        public ServiceInvoiceReadRepository(IMongoDbProvider provider) : base(provider)
        {
        }

        public async Task<NFSeInvoice> GetServiceInvoice(string invoiceId)
        {
            return await GetAsync(invoiceId.ToLower());
        }
    }
}
