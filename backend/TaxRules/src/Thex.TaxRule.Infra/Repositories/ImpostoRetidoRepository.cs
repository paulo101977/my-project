﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.GenericLog;
using Thex.TaxRule.Domain.Entities;
using Thex.TaxRule.Infra.Interfaces;
using Thex.TaxRules.Infra;
using Tnf.Dapper.Repositories;
using Tnf.Notifications;
using Tnf.Repositories;
using System.Linq;

namespace Thex.TaxRule.Infra.Repositories
{
    public class ImpostoRetidoRepository : DapperEfRepositoryBase<ThexTaxRuleContext, ImpostoRetido>, IImpostoRetidoRepository
    {
        private readonly INotificationHandler _notificationHandler;

        public ImpostoRetidoRepository(IActiveTransactionProvider activeTransactionProvider,
                                  INotificationHandler notificationHandler,
                                  IGenericLogHandler genericLogHandler)
        : base(activeTransactionProvider, genericLogHandler)
        {
            _notificationHandler = notificationHandler;
        }

        public async Task InsertList(List<ImpostoRetido> impostoRetidoEntityList)
        {
            foreach (var entity in impostoRetidoEntityList)
            {
                await InsertAsync(entity);
            }
        }

        public async Task DeleteImpostoRetido(Guid ownerId, Guid propertyUId)
        {
            var impostoRetido = GetAll(i => i.OwnerId == ownerId && i.PropertyUId == propertyUId).FirstOrDefault();
            await DeleteAsync(impostoRetido);
        }
    }
}
