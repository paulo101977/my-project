﻿using Thex.GenericLog;
using Thex.TaxRule.Domain.Entities;
using Thex.TaxRule.Infra.Interfaces;
using Tnf.Dapper.Repositories;
using Tnf.Repositories;
using System.Collections.Generic;
using Thex.Kernel;
using System;
using Thex.TaxRules.Infra;

namespace Thex.TaxRule.Infra.Repositories
{
    public class PTTaxRuleRepository : DapperEfRepositoryBase<ThexTaxRuleContext, PTTaxRule>, IPTTaxRuleRepository
    {
        private readonly IApplicationUser _applicationUser;

        public PTTaxRuleRepository(
               IActiveTransactionProvider activeTransactionProvider,
               IGenericLogHandler genericLogHandler,
               IApplicationUser applicationUser) : base(activeTransactionProvider, genericLogHandler)
        {
            _applicationUser = applicationUser;
        }

        public void CreateRange(List<PTTaxRule> entityList)
        {
            entityList.ForEach(e =>
            {
                e.IsActive = true;
                e.CreatorUserId = _applicationUser.UserUid;
                e.PropertyId = string.IsNullOrEmpty(_applicationUser.PropertyUid) || string.IsNullOrWhiteSpace(_applicationUser.PropertyUid) 
                    ? Guid.Parse("6146DA81-9CC3-46EF-B855-825551594B72") : Guid.Parse(_applicationUser.PropertyUid);
                e.TenantId = _applicationUser.TenantId;
            });

            base.Execute(@"
                INSERT INTO PT_TaxRules(TaxRulesId, ServiceId, ProductId, IvaId, IsActive, PropertyId, Description,
                ExemptionCode, TenantId, CreatorUserId) 
                VALUES(@Id, @ServiceId, @ProductId, @IvaId, @IsActive, @PropertyId, @Description, @ExemptionCode, @TenantId, @CreatorUserId)",
                entityList.ToArray());
        }

        public void DeleteAllByIvaId(Guid ivaId)
        {
            var tenantId = _applicationUser.TenantId;

            base.Execute(@"
                DELETE FROM PT_TaxRules
                WHERE PT_TaxRules.IvaId = @ivaId AND PT_TaxRules.TenantId = @tenantId",
                new { ivaId, tenantId });
        }
    }
}
