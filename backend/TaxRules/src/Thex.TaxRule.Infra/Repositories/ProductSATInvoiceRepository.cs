﻿using System;
using System.Threading.Tasks;
using Thex.TaxRule.Dto.Dto.Integration;
using Thex.TaxRule.Dto.Dto.Integration.NFCe;
using Thex.TaxRule.Dto.Dto.Integration.NFSe;
using Thex.TaxRule.Infra.Interfaces;
using Tnf.MongoDb.Provider;
using Tnf.MongoDb.Repositories;

namespace Thex.TaxRule.Infra.Repositories
{
    public class ProductSATInvoiceRepository : MongoDbRepository<SATInvoice, string>, IProductSATInvoiceRepository
    {
        public ProductSATInvoiceRepository(IMongoDbProvider provider) : base(provider)
        {

        }

        public async Task ChangeAsync(SATInvoice invoice)
        {
            await UpdateAsync(invoice);
        }

        public async Task PostAsync(SATInvoice invoice)
        {
            await InsertAsync(invoice);
        }
    }
}
