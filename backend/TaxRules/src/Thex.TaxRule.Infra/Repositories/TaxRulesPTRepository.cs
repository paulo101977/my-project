﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Thex.Common;
using Thex.Common.Enumerations;
using Thex.GenericLog;
using Thex.TaxRule.Domain.Entities;
using Thex.TaxRule.Dto;
using Thex.TaxRule.Infra.Interfaces.ReadRepositories;
using Thex.TaxRules.Infra;
using Tnf.Dapper.Repositories;
using Tnf.Dto;
using Tnf.Notifications;
using Tnf.Repositories;

namespace Thex.TaxRule.Infra.Repositories
{
    public class TaxRulesPTRepository : DapperEfRepositoryBase<ThexTaxRuleContext, TaxRulePT>, ITaxRulesPTRepository
    {
        private readonly INotificationHandler _notificationHandler;
        private readonly ITaxRulesPTReadRepository _taxRulesPTReadRepository;

        public TaxRulesPTRepository(IActiveTransactionProvider activeTransactionProvider,
                                  INotificationHandler notificationHandler,
                                  IGenericLogHandler genericLogHandler,
                                  ITaxRulesPTReadRepository taxRulesPTReadRepository)
        : base(activeTransactionProvider, genericLogHandler)
        {
            _notificationHandler = notificationHandler;
            _taxRulesPTReadRepository = taxRulesPTReadRepository;
        }

        public async Task<IDto> InsertTaxRule(Domain.Entities.TaxRulePT taxRule)
        {
            // Ajustes Dapper
            taxRule.IsActive = true;
            taxRule.IsDeleted = false;

            await base.InsertAsync(taxRule);

            return taxRule.MapTo<TaxRulesDto>();
        }

        public Task<IDto> InsertAsync(BaseEntity entity)
        {
            throw new NotImplementedException();
        }

        public Task RemoveAsync(Guid id)
        {
            throw new NotImplementedException();
        }

        public async Task ToggleAsync(Guid id)
        {
            var entity = await _taxRulesPTReadRepository.GetEntityById(id);

            if (entity == null)
            {
                _notificationHandler.Raise(_notificationHandler
                                    .DefaultBuilder
                                    .WithMessage(AppConsts.LocalizationSourceName, CommonsEnum.Error.ParameterInvalid)
                                    .Build());
                return;
            }

            entity.IsActive = !entity.IsActive;

            await base.UpdateAsync(entity);
        }

        public Task<IDto> UpdateAsync(Guid id, BaseEntity baseEntity)
        {
            throw new NotImplementedException();
        }
    }
}