﻿using Microsoft.EntityFrameworkCore;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using Thex.Common;
using Tnf.EntityFrameworkCore.Configuration;

namespace Thex.TaxRules.Infra
{
    [ExcludeFromCodeCoverageAttribute]
    public static class DbContextConfigurer
    {
        /// <summary>
        /// Configura o uso do dbcontext para sql server e faz o reuso da connexão existente em transações aninhadas
        /// </summary>
        public static void Configure<TDbContext>(TnfDbContextConfiguration<TDbContext> config)
            where TDbContext : DbContext
        {
            if (config.ExistingConnection != null)
                config.DbContextOptions.UseSqlServer(config.ExistingConnection);
            else
                config.DbContextOptions.UseSqlServer(config.ConnectionString);
        }
    }
}
