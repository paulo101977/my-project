﻿using Microsoft.EntityFrameworkCore;
using Thex.TaxRule.Domain.Entities;
using Tnf.EntityFrameworkCore;
using Tnf.Runtime.Session;

namespace Thex.TaxRules.Infra
{
    public partial class ThexTaxRuleContext : TnfDbContext
    {
        public ThexTaxRuleContext(DbContextOptions<ThexTaxRuleContext> options, ITnfSession session)
            : base(options, session)
        {
        }

        public virtual DbSet<CFOP> CFOP { get; set; }
        public virtual DbSet<CSOSN> CSOSN { get; set; }
        public virtual DbSet<CST> CST { get; set; }
        public virtual DbSet<NaturezaReceita> NaturezaReceita { get; set; }
        public virtual DbSet<NBS> NBS { get; set; }
        public virtual DbSet<NCM> NCM { get; set; }
        public virtual DbSet<RegimeApuracao> RegimeApuracao { get; set; }
        public virtual DbSet<TipoServico> TipoServico { get; set; }
        public virtual DbSet<UF> UF { get; set; }
        public virtual DbSet<Thex.TaxRule.Domain.Entities.TaxRule> TaxRules { get; set; }
        public virtual DbSet<Thex.TaxRule.Domain.Entities.TaxRulePT> TaxRulePT { get; set; }
        public virtual DbSet<ImpostoRetido> ImpostoRetido { get; set; }
    }
}
