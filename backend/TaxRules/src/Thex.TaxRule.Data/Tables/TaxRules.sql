﻿CREATE TABLE [dbo].[TaxRules]
(
	[TaxRulesId]			UNIQUEIDENTIFIER NOT NULL,
	[PropertyId]			UNIQUEIDENTIFIER NULL,				--ok
    [TenantId]				UNIQUEIDENTIFIER NULL,	--ok
	[Nome]					NVARCHAR(200) NOT NULL,		--ok
	[CodigoBarras]			NVARCHAR(200) NULL,			--ok
	[CfopId]				UNIQUEIDENTIFIER NOT NULL,	--ok
	[CsosnId]				UNIQUEIDENTIFIER NULL,		--ok
	[CstAId]				UNIQUEIDENTIFIER NULL,		--ok
	[CstBId]				UNIQUEIDENTIFIER NULL,		--ok
	[CstPisCofinsId]		UNIQUEIDENTIFIER NOT NULL,	--ok
	[NaturezaReceitaId]		UNIQUEIDENTIFIER NOT NULL,  --ok
	[RegimeApuracaoId]		UNIQUEIDENTIFIER NOT NULL,	--ok
	[NbsId]					UNIQUEIDENTIFIER NULL,
	[NcmId]					UNIQUEIDENTIFIER NULL,		--ok
	[TipoServicoId]			UNIQUEIDENTIFIER NULL,
	[UfId]					UNIQUEIDENTIFIER NULL,
	[isActive]				BIT NOT NULL DEFAULT 0,
	[isDeleted]				BIT	NOT NULL DEFAULT 0,
	[Aliquota]				NUMERIC(18,4) NOT NULL,
	[AliquotaSubstituicao]	NUMERIC(18,4) NOT NULL,
	[PercentualBase]		NUMERIC(18,4) NULL,
	[Tipo]					INT NOT NULL,
	[CreationTime]					DATETIME			NOT NULL	DEFAULT GETUTCDATE(),
	[CreatorUserId]					UNIQUEIDENTIFIER	NULL,
	[LastModificationTime]			DATETIME			NULL		DEFAULT GETUTCDATE(),
	[LastModifierUserId]			UNIQUEIDENTIFIER	NULL,
	[DeletionTime]					DATETIME			NULL,
	[DeleterUserId]					UNIQUEIDENTIFIER	NULL,

    CONSTRAINT [PK_TaxRules] PRIMARY KEY ([TaxRulesId]),
	CONSTRAINT [UK_TaxRules] UNIQUE (PropertyId, UfId, NbsId, NcmId, CodigoBarras, [isActive]),

	CONSTRAINT [FK_TaxRules_Cfop] FOREIGN KEY ([CfopId]) REFERENCES [dbo].[BR_CFOP] ([CfopId]),
	CONSTRAINT [FK_TaxRules_Csosn] FOREIGN KEY ([CsosnId]) REFERENCES [dbo].[BR_CSOSN] ([CsosnId]),
	CONSTRAINT [FK_TaxRules_CstA] FOREIGN KEY ([CstAId]) REFERENCES [dbo].[BR_CST] ([CstId]),
	CONSTRAINT [FK_TaxRules_CstB] FOREIGN KEY ([CstBId]) REFERENCES [dbo].[BR_CST] ([CstId]),
	CONSTRAINT [FK_TaxRules_CstPisCofins] FOREIGN KEY ([CstPisCofinsId]) REFERENCES [dbo].[BR_CST] ([CstId]),
	CONSTRAINT [FK_TaxRules_NaturezaReceita] FOREIGN KEY ([NaturezaReceitaId]) REFERENCES [dbo].[BR_NaturezaReceita] ([NaturezaReceitaId]),
	CONSTRAINT [FK_TaxRules_RegimeApuracao] FOREIGN KEY ([RegimeApuracaoId]) REFERENCES [dbo].[BR_RegimeApuracao] ([RegimeApuracaoId]),
	CONSTRAINT [FK_TaxRules_Nbs] FOREIGN KEY ([NbsId]) REFERENCES [dbo].[BR_NBS] ([NbsId]),
	CONSTRAINT [FK_TaxRules_Ncm] FOREIGN KEY ([NcmId]) REFERENCES [dbo].[BR_NCM] ([NcmId]),
	CONSTRAINT [FK_TaxRules_Uf] FOREIGN KEY ([UfId]) REFERENCES [dbo].[BR_UF] ([UfId]),
)
GO

CREATE INDEX
	x_TaxRules_UF ON dbo.[TaxRules] ( [UFId] )
GO

CREATE INDEX
	x_TaxRules_Ncm ON dbo.[TaxRules] ( [NcmId] )
GO

CREATE INDEX
	x_TaxRules_Nbs ON dbo.[TaxRules] ( [NbsId] )
GO

CREATE INDEX
	x_TaxRules_CodigoBarras ON dbo.[TaxRules] ( [CodigoBarras] )
GO
