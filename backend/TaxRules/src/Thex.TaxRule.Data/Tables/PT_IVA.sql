﻿CREATE TABLE [dbo].[PT_IVA]
(
	[IvaId] UNIQUEIDENTIFIER NOT NULL, 
    [Description] NVARCHAR(200) NOT NULL, 
    [Location] NVARCHAR(200) NOT NULL, 
    [Tax] NUMERIC(18, 4) NULL,
	[PropertyId] UNIQUEIDENTIFIER NULL,
    [IsActive] BIT NOT NULL DEFAULT 1, 

    CONSTRAINT [PK_Iva] PRIMARY KEY ([IvaId]),
	CONSTRAINT [UK_Iva] UNIQUE (PropertyId, Tax, Location, [isActive])
)
