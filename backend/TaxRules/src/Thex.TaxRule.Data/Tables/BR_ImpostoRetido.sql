﻿CREATE TABLE [dbo].[BR_ImpostoRetido]
(
	[ImpostoRetidoId] UNIQUEIDENTIFIER NOT NULL PRIMARY KEY, 
    [OwnerId] UNIQUEIDENTIFIER NULL,
	[PropertyUId]		UNIQUEIDENTIFIER	NULL,
	CONSTRAINT [UK_BR_ImpostoRetido] UNIQUE ( [OwnerId],[PropertyUId])
)
