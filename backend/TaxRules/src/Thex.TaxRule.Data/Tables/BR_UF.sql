﻿CREATE TABLE [dbo].[BR_UF]
(
	[UfId] UNIQUEIDENTIFIER NOT NULL,
	[CodigoIBGE] nvarchar(2) NOT NULL,
	[Sigla] nvarchar(2) NOT NULL,

	CONSTRAINT [PK_UF] PRIMARY KEY ([UfId])
)
