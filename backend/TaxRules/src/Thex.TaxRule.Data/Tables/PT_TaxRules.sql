﻿CREATE TABLE [dbo].[PT_TaxRules]
(
	[TaxRulesId] UNIQUEIDENTIFIER NOT NULL, 
    [ServiceId] INT NULL, 
    [ProductId] UNIQUEIDENTIFIER NULL, 
    [IvaId] UNIQUEIDENTIFIER NOT NULL,
    [IsActive] BIT NOT NULL DEFAULT 1, 
	[PropertyId] UNIQUEIDENTIFIER NULL,

    [Description] NVARCHAR(200) NULL, 
	[ExemptionCode] NVARCHAR(10) NULL, 
    [TenantId] UNIQUEIDENTIFIER NULL, 
    [CreationTime] DATETIME NOT NULL DEFAULT GETUTCDATE(), 
    [CreatorUserId] UNIQUEIDENTIFIER NULL, 
    [LastModificationTime] DATETIME NULL DEFAULT GETUTCDATE(), 
    [LastModifierUserId] UNIQUEIDENTIFIER NULL, 
    [DeletionTime] DATETIME NULL, 
    [DeleterUserId] UNIQUEIDENTIFIER NULL, 
    CONSTRAINT [PK_TaxRulesPT] PRIMARY KEY ([TaxRulesId]),
	CONSTRAINT [UK_TaxRulesPT] UNIQUE (PropertyId, IvaId, ServiceId, ProductId, [isActive]),
	CONSTRAINT [FK_TaxRulesPT_Iva] FOREIGN KEY ([IvaId]) REFERENCES [dbo].[PT_IVA] ([IvaId])
)
