﻿CREATE TABLE [dbo].[BR_TipoServico]
(
	[TipoServicoId] UNIQUEIDENTIFIER NOT NULL, 
    [Codigo] NVARCHAR(20) NOT NULL, 
    [Descricao] NVARCHAR(MAX) NOT NULL,

	[Ativo] BIT NULL DEFAULT 0, 
    CONSTRAINT [PK_TipoServico] PRIMARY KEY ([TipoServicoId])
)
GO

CREATE INDEX
	x_TipoServico_Codigo ON dbo.[BR_TipoServico] ( [Codigo] )
GO