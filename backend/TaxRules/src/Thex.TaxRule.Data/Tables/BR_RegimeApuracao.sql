﻿CREATE TABLE [dbo].[BR_RegimeApuracao]
(
	[RegimeApuracaoId] UNIQUEIDENTIFIER NOT NULL,
	[Descricao] nvarchar(20) NOT NULL,

	[Ativo] BIT NULL DEFAULT 0, 
    CONSTRAINT [PK_RegimeApuracao] PRIMARY KEY ([RegimeApuracaoId])
)
