﻿CREATE TABLE [dbo].[BR_NaturezaReceita]
(
	[NaturezaReceitaId] UNIQUEIDENTIFIER NOT NULL, 
    [Codigo]			NVARCHAR(20) NOT NULL, 
    [Descricao]			NVARCHAR(MAX) NOT NULL,
	[CstId]			    UNIQUEIDENTIFIER NOT NULL,
	[Ativo]				BIT NOT NULL DEFAULT 0,

	CONSTRAINT [PK_NaturezaReceita] PRIMARY KEY ([NaturezaReceitaId]),
	CONSTRAINT [FK_NaturezaReceita_Cst] FOREIGN KEY ([CstId]) REFERENCES [dbo].[BR_CST] ([CstId])
)
GO

CREATE INDEX
	x_NaturezaReceita_Codigo ON dbo.[BR_NaturezaReceita] ( [Codigo] )
GO

CREATE INDEX
	x_NaturezaReceita_Cst ON dbo.[BR_NaturezaReceita] ( [CstId] )
GO