﻿using Thex.TaxRule.Domain.Entities;
using Thex.TaxRule.Dto;

namespace hex.TaxRule.Application.Brazil.Adapters
{
    public interface IImpostoRetidoAdapter
    {
        ImpostoRetido.Builder Map(ImpostoRetido entity, ImpostoRetidoDto dto);
        ImpostoRetido.Builder Map(ImpostoRetidoDto dto);
    }
}
