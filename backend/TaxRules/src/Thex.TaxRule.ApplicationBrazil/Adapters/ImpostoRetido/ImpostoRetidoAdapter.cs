﻿using hex.TaxRule.Application.Brazil.Adapters;
using Thex.TaxRule.Domain.Entities;
using Thex.TaxRule.Dto;
using Tnf;
using Tnf.Notifications;

namespace Thex.TaxRule.Application.Brazil.Adapters
{
    public class ImpostoRetidoAdapter : IImpostoRetidoAdapter
    {
        public INotificationHandler NotificationHandler { get; }

        public ImpostoRetidoAdapter(INotificationHandler notificationHandler)
        {
            NotificationHandler = notificationHandler;
        }

        public virtual CST.Builder Map(CSTDto dto)
        {
            

            var builder = new CST.Builder(NotificationHandler)
                .WithId(dto.Id)
                .WithCode(dto.Code)
                .WithDescription(dto.Description)
                .WithType(dto.Type);

            return builder;
        }

        public ImpostoRetido.Builder Map(ImpostoRetido entity, ImpostoRetidoDto dto)
        {
            Check.NotNull(entity, nameof(entity));
            Check.NotNull(dto, nameof(dto));

            var builder = new ImpostoRetido.Builder(NotificationHandler, entity)
                .WithId(dto.Id)
                .WithOwnerId(dto.OwnerId)
                .WithPropertyUId(dto.PropertyUId.Value);

            return builder;
        }

        public ImpostoRetido.Builder Map(ImpostoRetidoDto dto)
        {
            Check.NotNull(dto, nameof(dto));

            var builder = new ImpostoRetido.Builder(NotificationHandler)
                .WithId(dto.Id)
                .WithOwnerId(dto.OwnerId)
                .WithPropertyUId(dto.PropertyUId.Value);

            return builder;
        }
    }
}
