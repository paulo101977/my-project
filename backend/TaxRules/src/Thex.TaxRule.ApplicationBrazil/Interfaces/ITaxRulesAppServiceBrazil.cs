﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.Common.Dto.InvoiceIntegration;
using Thex.Dto.Integration;
using Thex.TaxRule.Dto;
using Thex.TaxRule.Dto.Dto.Integration.Response;
using Tnf.Application.Services;
using Tnf.Dto;

namespace Thex.TaxRule.Application.Brasil.Interfaces
{
    public interface ITaxRulesAppServiceBrazil : IApplicationService
    {
        Task<bool> CheckExistTax(List<IntegrationServiceInvoiceDTO> serviceInvoiceList = null, List<IntegrationProductInvoiceDTO> productInvoiceList = null);
        Task<TaxRulesDto> GetTribute(Guid propertyUUid, GetAllTaxRulesWithParamsDto requestDto);
        Task<InvoiceResponseDto> Send(List<InvoiceIntegrationDto> invoiceListDto);
        Task<TaxRulesDto> Create(TaxRulesDto dto);
        Task ToggleActivationObjAsync(Guid id);
        Task<IListDto<TaxRulesDto>> GetAll(int type, GetAllTaxRulesDto requestDto);
        Task UpdateInvoiceResponse(InvoiceIntegrationResponseDto invoiceResponse, int invoiceType);
        Task<InvoiceCancelResponseDto> Cancel(InvoiceIntegrationDto invoiceDto, int invoiceType);
    }
}
