﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Thex.TaxRule.Dto;
using Tnf.Dto;

namespace Thex.TaxRule.Application.Brasil.Interfaces
{
    public interface IUFAppService
    {
        Task<IListDto<UFDto>> GetAll(GetAllUFDto requestDto);
        Task<UFDto> GetByCode(GetAllUFDto requestDto);
    }
}
