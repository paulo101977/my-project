﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Thex.TaxRule.Dto;
using Tnf.Dto;

namespace Thex.TaxRule.Application.Brasil.Interfaces
{
    public interface IRegimeApuracaoAppService
    {
        Task<IListDto<RegimeApuracaoDto>> GetAll(GetAllRegimeApuracaoDto requestDto);
        Task<RegimeApuracaoDto> GetByCode(Guid id, GetAllRegimeApuracaoDto requestDto);
    }
}
