﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Thex.TaxRule.Dto;
using Tnf.Dto;

namespace Thex.TaxRule.Application.Brasil.Interfaces
{
    public interface ITipoServicoAppService
    {
        Task<IListDto<TipoServicoDto>> GetAll(GetAllTipoServicoDto requestDto);
        Task<TipoServicoDto> GetByCode(string code, GetAllTipoServicoDto requestDto);
    }
}
