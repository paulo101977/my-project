﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.TaxRule.Dto;
using Tnf.Dto;

namespace Thex.TaxRule.Application.Brasil.Interfaces
{
    public interface IImpostoRetidoAppService
    {
        Task<IListDto<ImpostoRetidoDto>> GetAllByOwnerIdAndPropertyUid(GetAllImpostoRetidoDto requestDto, Guid propertyUid);
        Task CreateList(List<ImpostoRetidoDto> dtoList);
        Task Delete(Guid ownerId, Guid propertyUId);
    }
}
