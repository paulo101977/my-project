﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Thex.TaxRule.Dto;
using Tnf.Dto;

namespace Thex.TaxRule.Application.Brasil.Interfaces
{
    public interface ICSTAppService
    {
        Task<IListDto<CSTDto>> GetAll(GetAllCSTDto requestDto);
        Task<IListDto<CSTDto>> GetAllByType(int type, GetAllCSTDto requestDto);
        Task<CSTDto> GetByCode(string code, GetAllCSTDto requestDto);
    }
}
