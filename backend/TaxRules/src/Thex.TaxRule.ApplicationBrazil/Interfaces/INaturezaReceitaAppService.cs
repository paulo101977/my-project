﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Thex.TaxRule.Dto;
using Tnf.Dto;

namespace Thex.TaxRule.Application.Brasil.Interfaces
{
    public interface INaturezaReceitaAppService
    {
        Task<IListDto<NaturezaReceitaDto>> GetAll(GetAllNaturezaReceitaDto requestDto);
        Task<NaturezaReceitaDto> GetByCode(string code, GetAllNaturezaReceitaDto requestDto);
        Task<IListDto<NaturezaReceitaDto>> GetByNcm(string ncm, GetAllNaturezaReceitaDto requestDto);
    }
}
