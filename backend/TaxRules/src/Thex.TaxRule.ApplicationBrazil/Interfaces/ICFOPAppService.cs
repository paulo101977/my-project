﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Thex.TaxRule.Dto;
using Thex.TaxRule.Dto.GetAll;
using Tnf.Dto;

namespace Thex.TaxRule.Application.Brasil.Interfaces
{
    public interface ICFOPAppService
    {
        Task<IListDto<CFOPDto>> GetAll(GetAllCFOPDto requestDto);
        Task<CFOPDto> GetByCode(string code,GetAllCFOPDto requestDto);
        Task<IListDto<CFOPDto>> GetAllByFilters(SearchCfopDto searchDto);
    }
}
