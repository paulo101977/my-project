﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Thex.TaxRule.Dto;
using Thex.TaxRule.Dto.GetAll;
using Tnf.Dto;

namespace Thex.TaxRule.Application.Brasil.Interfaces
{
    public interface INBSAppService
    {
        Task<IListDto<NBSDto>> GetAll(GetAllNBSDto requestDto);
        Task<NBSDto> GetByCode(string code, GetAllNBSDto requestDto);
        Task<IListDto<NBSDto>> GetAllNbsByFilters(SearchNbsDto searchDto);
    }
}
