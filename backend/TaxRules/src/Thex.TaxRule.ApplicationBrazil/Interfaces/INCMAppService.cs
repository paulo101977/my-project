﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Thex.TaxRule.Dto;
using Thex.TaxRule.Dto.GetAll;
using Tnf.Dto;

namespace Thex.TaxRule.Application.Brasil.Interfaces
{
    public interface INCMAppService
    {
        Task<IListDto<NCMDto>> GetAll(GetAllNCMDto requestDto);
        Task<NCMDto> GetByCode(string code, GetAllNCMDto requestDto);
        Task<IListDto<NCMDto>> GetAllByFilters(SearchNcmDto requestDto);
    }
}
