using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Polly;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Thex.Common;
using Thex.Common.Dto.InvoiceIntegration;
using Thex.Common.Enumerations;
using Thex.Common.Extensions;
using Thex.Common.Helpers;
using Thex.Dto.Integration;
using Thex.GenericLog;
using Thex.Kernel;
using Thex.TaxRule.Application.Brasil.Interfaces;
using Thex.TaxRule.Application.Brazil.Adapters;
using Thex.TaxRule.Application.Brazil.Services.Integration;
using Thex.TaxRule.Domain.Enumerations;
using Thex.TaxRule.Dto;
using Thex.TaxRule.Dto.Dto.Integration;
using Thex.TaxRule.Dto.Dto.Integration.Itens;
using Thex.TaxRule.Dto.Dto.Integration.NFCe;
using Thex.TaxRule.Dto.Dto.Integration.NFSe;
using Thex.TaxRule.Dto.Dto.Integration.Order;
using Thex.TaxRule.Dto.Dto.Integration.Payment;
using Thex.TaxRule.Dto.Dto.Integration.Response;
using Thex.TaxRule.Dto.Dto.Integration.Tax;
using Thex.TaxRule.Infra.Interfaces;
using Thex.TaxRule.Infra.Interfaces.ReadRepositories;
using Thex.TaxRule.Infra.Repositories;
using Tnf.Dto;
using Tnf.Localization;
using Tnf.Notifications;
using Tnf.Repositories.Uow;

namespace Thex.TaxRule.Application.Brasil.Services
{
    public class TaxRulesAppServiceBrazil : ApplicationServiceBase<TaxRulesDto, Domain.Entities.TaxRule>, ITaxRulesAppServiceBrazil
    {
        private readonly string _ibptServiceTaxDetails = "Valor Aprox dos Tributos: R$ {0} Federal, R$ {1} Estadual e R$ {2} Municipal Fonte: {3}";
        private readonly string _defaultParams = "Parametros n�o informados: ";
        private readonly string _unitMeasurementDefault = "UN";
        private readonly decimal _taxBasePercentual = 100m;
        private readonly string _taxSubCode = "60";
        private readonly string _taxCalculation = "N�o cumulativo";
        private readonly decimal _taxCalculationPIS = 1.65m;
        private readonly decimal _taxCalculationCOFINS = 7.6m;
        private readonly string _enotasIntegrationIndexName = "taxrule-database-log";

        private readonly IApplicationUser _applicationUser;
        private readonly IConfiguration _configuration;
        private readonly INotificationHandler _notificationHandler;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly ITaxRulesAdapter _taxRulesAdapter;
        private readonly ITaxRulesRepository _taxRulesRepository;
        private readonly ITaxRulesReadRepository _taxRulesReadRepository;
        private readonly IServiceInvoiceRepository _serviceInvoiceRepository;
        private readonly IServiceInvoiceReadRepository _serviceInvoiceReadRepository;
        private readonly IRetainedClientReadRepository _retainedClientReadRepository;
        private readonly IProductNFCeInvoiceRepository _productNFCeInvoiceRepository;
        private readonly IProductNFCeInvoiceReadRepository _productNFCeInvoiceReadRepository;
        private readonly IProductSATInvoiceRepository _productSATInvoiceRepository;
        private readonly IProductSATInvoiceReadRepository _productSATInvoiceReadRepository;

        private readonly IGenericLogHandler _genericLog;
        private readonly ILocalizationManager _localizationManager;

        public TaxRulesAppServiceBrazil(INotificationHandler notificationHandler,
                          ITaxRulesRepository taxRulesRepository,
                          IUnitOfWorkManager unitOfWorkManager,
                          ITaxRulesAdapter taxRulesAdapter,
                          ITaxRulesReadRepository taxRulesReadRepository,
                          IConfiguration configuration,
                          IApplicationUser applicationUser,
                          IServiceInvoiceRepository serviceInvoiceRepository,
                          IServiceInvoiceReadRepository serviceInvoiceReadRepository,
                          IRetainedClientReadRepository retainedClientReadRepository,
                          IProductNFCeInvoiceRepository productInvoiceRepository,
                          IProductNFCeInvoiceReadRepository productInvoiceReadRepository,
                          IGenericLogHandler genericLogHandler,
                          ILocalizationManager localizationManager,
                          IProductSATInvoiceRepository productSATInvoiceRepository,
                          IProductSATInvoiceReadRepository productSATInvoiceReadRepository) : base(notificationHandler, taxRulesRepository)
        {
            _localizationManager = localizationManager;
            _genericLog = genericLogHandler;
            _unitOfWorkManager = unitOfWorkManager;
            _taxRulesAdapter = taxRulesAdapter;
            _notificationHandler = notificationHandler;
            _taxRulesRepository = taxRulesRepository;
            _taxRulesReadRepository = taxRulesReadRepository;
            _configuration = configuration;
            _applicationUser = applicationUser;
            _serviceInvoiceRepository = serviceInvoiceRepository;
            _serviceInvoiceReadRepository = serviceInvoiceReadRepository;
            _retainedClientReadRepository = retainedClientReadRepository;
            _productNFCeInvoiceRepository = productInvoiceRepository;
            _productNFCeInvoiceReadRepository = productInvoiceReadRepository;
            _productSATInvoiceRepository = productSATInvoiceRepository;
            _productSATInvoiceReadRepository = productSATInvoiceReadRepository;
        }

        /// <summary>
        /// Envia um registro de venda
        /// </summary>
        /// <param name="invoiceListDto">Lista de invoices</param>
        /// <returns></returns>
        public async Task<InvoiceResponseDto> Send(List<InvoiceIntegrationDto> invoiceListDto)
        {
            ValidateRequiredParams(invoiceListDto);

            if (_notificationHandler.HasNotification())
                return InvoiceResponseDto.NullInstance;

            ValidateInvoiceParams(invoiceListDto);

            if (_notificationHandler.HasNotification())
                return InvoiceResponseDto.NullInstance;

            var invoice = await GenetateInvoiceIntegration(invoiceListDto);

            return (invoice != null ? invoice : InvoiceResponseDto.NullInstance);
        }

        /// <summary>
        /// Valida se todos os servi�os ou produtos possuem tributa��o ativa
        /// </summary>
        /// <param name="invoiceListDto"></param>
        private void ValidateInvoiceParams(List<InvoiceIntegrationDto> invoiceListDto)
        {
            // TODO: Enotas
            // Incluir valida��o apenas ap�s o ajuste no cadastro de tributos de servi�o
            // verifica se a tributa��o do c�digo do servi�o est� cadastrado
            //if (invoiceListDto.SelectMany(x => x.ServiceInvoice).Count() > 0 &&
            //    !CheckExistTax(serviceInvoiceList: invoiceListDto.SelectMany(x => x.ServiceInvoice).ToList()).Result)
            //    _notificationHandler.Raise(_notificationHandler.DefaultBuilder
            //                               .WithMessage(AppConsts.LocalizationSourceName, TaxRuleEnum.Error.TributeNotFound)
            //                               .WithDetailedMessage(AppConsts.LocalizationSourceName, TaxRuleEnum.Error.TributeNotFound)
            //                               .Build());

            // verificar se a tributa��o do c�digo do produtos est� cadastrado
            if (invoiceListDto.SelectMany(x => x.ProductInvoice).Count() > 0 &&
                !CheckExistTax(productInvoiceList: invoiceListDto.SelectMany(x => x.ProductInvoice).ToList()).Result)
            {
                _notificationHandler.Raise(_notificationHandler.DefaultBuilder
                                           .WithMessage(AppConsts.LocalizationSourceName, TaxRuleEnum.Error.TributeNotFound)
                                           .WithDetailedMessage(AppConsts.LocalizationSourceName, TaxRuleEnum.Error.TributeNotFound)
                                           .Build());

                _genericLog.AddLog(_genericLog.DefaultBuilder
                                   .WithMessage(_localizationManager.GetString(AppConsts.LocalizationSourceName, TaxRuleEnum.Error.TributeNotFound.ToString()))
                                   .WithDetailedMessage(_localizationManager.GetString(AppConsts.LocalizationSourceName, TaxRuleEnum.Error.TributeNotFound.ToString()))
                                   .AsError().Build());
            }

            return;
        }

        /// <summary>
        /// Valida os campos requeridos da venda
        /// </summary>
        /// <param name="invoiceListDto"></param>
        /// <returns></returns>
        private void ValidateRequiredParams(List<InvoiceIntegrationDto> invoiceListDto)
        {
            var requiredParamsList = new List<string>();

            foreach (var invoice in invoiceListDto)
            {
                // Valida a UF no token
                if (string.IsNullOrEmpty(_applicationUser.PropertyDistrictCode))
                    requiredParamsList.Add(nameof(_applicationUser.PropertyDistrictCode));

                // Valida o campo PropertyIntegrationCode
                if (string.IsNullOrEmpty(invoice.IntegrationCode))
                    requiredParamsList.Add(nameof(invoice.IntegrationCode));

                // Valida o campo PartnerKey
                if (string.IsNullOrEmpty(invoice.TokenClient))
                    requiredParamsList.Add(nameof(invoice.TokenClient));

                // Valida o campo ExternalId
                if (string.IsNullOrEmpty(invoice.ExternalId))
                    requiredParamsList.Add(nameof(invoice.ExternalId));

                // Valida o campo idEmpresa
                if (string.IsNullOrEmpty(invoice.PropertyUUId))
                    requiredParamsList.Add(nameof(invoice.PropertyUUId));

                #region Service validations

                foreach (var serviceInvoice in invoice.ServiceInvoice)
                {
                    // Valida o campo descri��o
                    if (string.IsNullOrEmpty(serviceInvoice.Description))
                        requiredParamsList.Add(nameof(serviceInvoice.Description));

                    // TODO: Enotas
                    // Incluir valida��o apenas ap�s o ajuste no cadastro de tributos de servi�o
                    // Valida o campo nbs
                    //if (string.IsNullOrEmpty(serviceInvoice.Nbs))
                    //    requiredParamsList.Add(nameof(serviceInvoice.Nbs));

                    // Valida o campo valor total
                    if (serviceInvoice.Amount <= 0m)
                        requiredParamsList.Add(nameof(serviceInvoice.Amount));
                }

                #endregion

                #region Product validations

                foreach (var productInvoice in invoice.ProductInvoice)
                {
                    // Valida o campo ncm e barcode
                    if (string.IsNullOrEmpty(productInvoice.Ncm) && string.IsNullOrEmpty(productInvoice.Barcode))
                    {
                        requiredParamsList.Add(string.IsNullOrEmpty(productInvoice.Ncm) ? nameof(productInvoice.Ncm) : nameof(productInvoice.Barcode));
                    }

                    // Valida o campo descricao
                    if (string.IsNullOrEmpty(productInvoice.Description))
                        requiredParamsList.Add(nameof(productInvoice.Description));

                    //Valida o campo valor unit�rio
                    if (productInvoice.Amount <= 0m)
                        requiredParamsList.Add(nameof(productInvoice.Amount));

                    //Valida o campo quantidade
                    if (productInvoice.Quantity <= 0m)
                        requiredParamsList.Add(nameof(productInvoice.Quantity));

                }

                #endregion
            }

            if (requiredParamsList.Count > 0)
            {
                _notificationHandler.Raise(_notificationHandler.DefaultBuilder
                   .WithMessage(AppConsts.LocalizationSourceName, TaxRuleEnum.Error.InvoiceRequiredParams)
                   .WithDetailedMessage(String.Join(", ", requiredParamsList.ToArray()))
                   .Build());

                _genericLog.AddLog(_genericLog.DefaultBuilder
                   .WithMessage(_localizationManager.GetString(AppConsts.LocalizationSourceName, TaxRuleEnum.Error.InvoiceRequiredParams.ToString()))
                   .WithDetailedMessage(String.Join(", ", requiredParamsList.ToArray()))
                   .AsError().Build());
            }
        }

        private async Task UpdateNFSe(InvoiceIntegrationResponseDto invoiceResponse)
        {
            var serviceInvoice = await _serviceInvoiceReadRepository.GetServiceInvoice(invoiceResponse.ExternalId);

            if (serviceInvoice == null)
                return;

            serviceInvoice.ResponseDetails = new InvoiceIntegrationResponseDetailsDto(invoiceResponse);

            await _serviceInvoiceRepository.ChangeAsync(serviceInvoice);

            _genericLog.AddLog(_genericLog.DefaultBuilder
               .WithMessage(_localizationManager.GetString(AppConsts.LocalizationSourceName, TaxRuleEnum.Message.UpdateInvoice.ToString()))
               .WithDetailedMessage(JsonConvert.SerializeObject(invoiceResponse))
               .AsError().Build());
        }

        private async Task UpdateNFCe(InvoiceIntegrationResponseDto invoiceResponse)
        {
            var productInvoice = await _productNFCeInvoiceReadRepository.GetProductInvoice(invoiceResponse.ExternalId);

            if (productInvoice == null)
                return;

            productInvoice.ResponseDetails = new InvoiceIntegrationResponseDetailsDto(invoiceResponse);

            await _productNFCeInvoiceRepository.ChangeAsync(productInvoice);

            _genericLog.AddLog(_genericLog.DefaultBuilder
               .WithMessage(_localizationManager.GetString(AppConsts.LocalizationSourceName, TaxRuleEnum.Message.UpdateInvoice.ToString()))
               .WithDetailedMessage(JsonConvert.SerializeObject(invoiceResponse))
               .AsError().Build());
        }

        private async Task UpdateSAT(InvoiceIntegrationResponseDto invoiceResponse)
        {
            var productInvoice = await _productSATInvoiceReadRepository.GetProductInvoice(invoiceResponse.ExternalId);

            if (productInvoice == null)
                return;

            productInvoice.ResponseDetails = new InvoiceIntegrationResponseDetailsDto(invoiceResponse);

            await _productSATInvoiceRepository.ChangeAsync(productInvoice);

            _genericLog.AddLog(_genericLog.DefaultBuilder
               .WithMessage(_localizationManager.GetString(AppConsts.LocalizationSourceName, TaxRuleEnum.Message.UpdateInvoice.ToString()))
               .WithDetailedMessage(JsonConvert.SerializeObject(invoiceResponse))
               .AsError().Build());
        }

        public async Task UpdateInvoiceResponse(InvoiceIntegrationResponseDto invoiceResponse, int invoiceType)
        {
            switch (EnumHelper.GetEnumValue<BillingInvoiceTypeEnum>(invoiceType.ToString()))
            {
                case BillingInvoiceTypeEnum.NFSE:
                    {
                        await UpdateNFSe(invoiceResponse);
                        break;
                    }
                case BillingInvoiceTypeEnum.NFCE:
                    {
                        await UpdateNFCe(invoiceResponse);
                        break;
                    }
                case BillingInvoiceTypeEnum.SAT:
                    {
                        await UpdateSAT(invoiceResponse);
                        break;
                    }
                default:
                    break;
            }

            if (_genericLog.HasGenericLog())
                _genericLog.Commit(_enotasIntegrationIndexName);
        }

        private async Task<InvoiceResponseDto> GenetateInvoiceIntegration(List<InvoiceIntegrationDto> invoiceListDto)
        {
            var ret = InvoiceResponseDto.NullInstance;

            if (invoiceListDto.Any(x => x.ServiceInvoice != null && x.ServiceInvoice.Count > 0))
                ret = await CreateAndSendSendServiceInvoice(invoiceListDto);

            if (invoiceListDto.Any(x => x.ProductInvoice != null && x.ProductInvoice.Count > 0))
            {
                if (_applicationUser.PropertyDistrictCode == "SP")
                    return await CreateAndSendSendSATProductInvoice(invoiceListDto);
                else
                    return await CreateAndSendSendNFCeProductInvoice(invoiceListDto);
            }

            if (_notificationHandler.HasNotification())
                ret = await Task.FromResult(InvoiceResponseDto.NullInstance);

            return ret;
        }

        private async Task<InvoiceResponseDto> CreateAndSendSendSATProductInvoice(List<InvoiceIntegrationDto> invoiceListDto)
        {
            InvoiceResponseDto invoiceProduct = null;

            foreach (var productInvoice in invoiceListDto.Where(x => x.ProductInvoice != null && x.ProductInvoice.Count > 0))
            {
                var satInvoice = new SATInvoice(productInvoice.ExternalId, productInvoice.PropertyUUId,
                                                 productInvoice.Environment, productInvoice.Client != null ? productInvoice.Client.Email != null &&
                                                                                                             productInvoice.Client.Email != string.Empty ? true : false : false,
                                                 productInvoice.IntegrationCode, productInvoice.Number, productInvoice.SerialNumber);

                if (productInvoice.Client != null)
                    satInvoice.Person = GeneratePersonDetails(productInvoice);

                if (productInvoice.ProductInvoice != null)
                {
                    satInvoice.ItensDetails = GenerateProductItemsDetails(productInvoice.ProductInvoice, productInvoice.PropertyUUId);

                    if (_notificationHandler.HasNotification())
                        return InvoiceResponseDto.NullInstance;

                    satInvoice.OrderDetails = GenerateProductOrderDetails(productInvoice.ProductInvoice.FirstOrDefault().PaymentDetails);

                    if (_notificationHandler.HasNotification())
                        return InvoiceResponseDto.NullInstance;
                }

                try
                {
                    invoiceProduct = await new InvoiceService(productInvoice.TokenClient,
                                                              _genericLog,
                                                              _localizationManager,
                                                              _notificationHandler,
                                                              _configuration).SendAsync(satInvoice, GetInvoiceUrl(satInvoice));

                    if (invoiceProduct != null && !string.IsNullOrEmpty(invoiceProduct.Id))
                    {
                        if (_productSATInvoiceReadRepository.GetProductInvoice(productInvoice.ExternalId).Result == null)
                            await _productSATInvoiceRepository.PostAsync(satInvoice);
                        else
                            await _productSATInvoiceRepository.ChangeAsync(satInvoice);
                    }
                }
                catch (Exception ex)
                {
                    _notificationHandler.Raise(_notificationHandler.DefaultBuilder
                               .WithMessage(AppConsts.LocalizationSourceName, TaxRuleEnum.Error.TributeNotFound)
                               .WithDetailedMessage(ex.Message)
                               .Build());
                }
            }

            return invoiceProduct;
        }

        private async Task<InvoiceResponseDto> CreateAndSendSendNFCeProductInvoice(List<InvoiceIntegrationDto> invoiceListDto)
        {
            InvoiceResponseDto invoiceProduct = null;

            foreach (var productInvoice in invoiceListDto.Where(x => x.ProductInvoice != null && x.ProductInvoice.Count > 0))
            {
                var nfceInvoice = new NFCeInvoice(productInvoice.ExternalId, productInvoice.PropertyUUId,
                                                  productInvoice.Environment, productInvoice.Client != null ? productInvoice.Client.Email != null &&
                                                                                                          productInvoice.Client.Email != string.Empty ? true : false : false,
                                                  productInvoice.IntegrationCode, productInvoice.Number, productInvoice.SerialNumber);

                if (productInvoice.Client != null)
                    nfceInvoice.Person = GeneratePersonDetails(productInvoice);

                if (productInvoice.ProductInvoice != null)
                {
                    nfceInvoice.ItensDetails = GenerateProductItemsDetails(productInvoice.ProductInvoice, productInvoice.PropertyUUId);

                    if (_notificationHandler.HasNotification())
                        return InvoiceResponseDto.NullInstance;

                    nfceInvoice.OrderDetails = GenerateProductOrderDetails(productInvoice.ProductInvoice.FirstOrDefault().PaymentDetails);

                    if (_notificationHandler.HasNotification())
                        return InvoiceResponseDto.NullInstance;
                }

                try
                {
                    invoiceProduct = await new InvoiceService(productInvoice.TokenClient,
                                                              _genericLog,
                                                              _localizationManager,
                                                              _notificationHandler,
                                                              _configuration).SendAsync(nfceInvoice, GetInvoiceUrl(nfceInvoice),
                                                                                                     GetProductInvoiceRequestUrl(nfceInvoice));

                    if (invoiceProduct != null && !string.IsNullOrEmpty(invoiceProduct.Id))
                    {
                        if (_productNFCeInvoiceReadRepository.GetProductInvoice(productInvoice.ExternalId).Result == null)
                            await _productNFCeInvoiceRepository.PostAsync(nfceInvoice);
                        else
                            await _productNFCeInvoiceRepository.ChangeAsync(nfceInvoice);
                    }
                }
                catch (Exception ex)
                {
                    _notificationHandler.Raise(_notificationHandler.DefaultBuilder
                               .WithMessage(AppConsts.LocalizationSourceName, TaxRuleEnum.Error.InvoicePartnerError)
                               .WithDetailedMessage(ex.Message)
                               .Build());
                }
            }

            return invoiceProduct;
        }

        private OrderDto GenerateProductOrderDetails(List<IntegrationProductInvoicePaymentsDto> paymentDetails)
        {
            return new OrderDto()
            {
                PaymentDetails = GeneratePaymentDetails(paymentDetails)
            };
        }

        private PaymentoDto GeneratePaymentDetails(List<IntegrationProductInvoicePaymentsDto> paymentDetails)
        {
            var paymentDto = new PaymentoDto();

            foreach (var payment in paymentDetails)
            {
                var paymentDetail = new PaymentoDetailsDto()
                {
                    Amount = payment.Amount,
                    Type = payment.PaymentDescription
                };

                if (payment.CreditDetails != null)
                    paymentDetail.CreaditCardIssuerDetails = new CreaditCardIssuerDto()
                    {
                        AutorizationCode = payment.CreditDetails.NSU,
                        Document = payment.CreditDetails.Document,
                        PlasticBrand = payment.CreditDetails.PlasticBrand,
                        Type = payment.CreditDetails.Type
                    };

                paymentDto.Details.Add(paymentDetail);
            }

            return paymentDto;
        }

        private async Task<InvoiceResponseDto> CreateAndSendSendServiceInvoice(List<InvoiceIntegrationDto> invoiceListDto)
        {
            InvoiceResponseDto invoiceService = null;

            foreach (var serviceInvoice in invoiceListDto.Where(x => x.ServiceInvoice != null && x.ServiceInvoice.Count > 0))
            {
                foreach (var invoice in serviceInvoice.ServiceInvoice)
                {
                    var nfseInvoice = new NFSeInvoice(serviceInvoice.ExternalId, invoice.Amount, serviceInvoice.IntegrationCode,
                                                  serviceInvoice.Environment, serviceInvoice.Client != null ? serviceInvoice.Client.Email != null &&
                                                                                                              serviceInvoice.Client.Email != string.Empty ? true : false : false,
                                                  serviceInvoice.IntegrationCode);

                    if (serviceInvoice.Client != null)
                        nfseInvoice.Person = GeneratePersonDetails(serviceInvoice);

                    if (serviceInvoice.ServiceInvoice != null)
                    {
                        nfseInvoice.ServiceDetails = GenerateServiceDetails(invoice, serviceInvoice.PropertyUUId, serviceInvoice.Client != null ? serviceInvoice.Client.Id : string.Empty);
                        nfseInvoice.AdditionalInformations = await GenerateServiceIBPTTaxDetails(invoice.Nbs, invoice.Amount);
                    }

                    try
                    {
                        invoiceService = await new InvoiceService(serviceInvoice.TokenClient,
                                                                  _genericLog,
                                                                  _localizationManager,
                                                                  _notificationHandler,
                                                                  _configuration).SendAsync(nfseInvoice, GetInvoiceUrl(nfseInvoice),
                                                                                                         GetServiceInvoiceRequestUrl(nfseInvoice));

                        if (invoiceService != null && !string.IsNullOrEmpty(invoiceService.InvoiceId))
                        {
                            if (_serviceInvoiceReadRepository.GetServiceInvoice(nfseInvoice.ExternalId).Result == null)
                                await _serviceInvoiceRepository.PostAsync(nfseInvoice);
                            else
                                await _serviceInvoiceRepository.ChangeAsync(nfseInvoice);
                        }
                    }
                    catch (Exception ex)
                    {
                        _notificationHandler.Raise(_notificationHandler.DefaultBuilder
                                   .WithMessage(AppConsts.LocalizationSourceName, TaxRuleEnum.Error.TributeNotFound)
                                   .WithDetailedMessage(ex.Message)
                                   .Build());
                    }
                }
            }

            return invoiceService;
        }

        private async Task<string> GenerateServiceIBPTTaxDetails(string serviceCode, decimal amount)
        {
            var client = new HttpClient();

            var responseMessage = await Policy
                           .HandleResult<HttpResponseMessage>(message => !message.IsSuccessStatusCode)
                           .WaitAndRetryAsync(1, i => TimeSpan.FromSeconds(30), (result, timeSpan, retryCount, context) =>
                           {
                               Console.Write($"Request failed with {result.Result.StatusCode}. Waiting {timeSpan} before next retry. Retry attempt {retryCount}");
                           })
                           .ExecuteAsync(() => client.GetAsync(GetIBPTServiceUrl(serviceCode, amount)));

            var resultData = await responseMessage.Content.ReadAsStringAsync();

            if (responseMessage.IsSuccessStatusCode && !string.IsNullOrEmpty(resultData))
            {
                var ibptTax = JsonConvert.DeserializeObject<IBPTResponseDto>(resultData);


                return string.Format(_ibptServiceTaxDetails, ibptTax.FederalTax,
                                                             ibptTax.StateTax,
                                                             ibptTax.CityTax,
                                                             ibptTax.Source);
            }

            return string.Empty;
        }

        private List<ItensDto> GenerateProductItemsDetails(List<IntegrationProductInvoiceDTO> productInvoice, string propertyUUId)
        {
            var itensList = new List<ItensDto>();

            foreach (var product in productInvoice)
            {
                var databaseTax = GetTribute(Guid.Parse(propertyUUId), new GetAllTaxRulesWithParamsDto()
                {
                    Ncm = product.Ncm,
                    BarCode = product.Barcode,
                    TwoLetterUf = _applicationUser.PropertyDistrictCode,
                }).Result;

                // TODO: Ajustar motivo do erro
                if (databaseTax == null)
                {
                    _notificationHandler.Raise(_notificationHandler.DefaultBuilder
                                           .WithMessage(AppConsts.LocalizationSourceName, TaxRuleEnum.Error.TributeNotFound)
                                           .WithDetailedMessage(AppConsts.LocalizationSourceName, TaxRuleEnum.Error.TributeNotFound)
                                           .Build());
                    return null;
                }

                ValidateTaxDetails(databaseTax);

                var code = string.IsNullOrEmpty(product.Code) ? product.Barcode : product.Code;

                itensList.Add(new ItensDto()
                {
                    Cfop = databaseTax.Cfop,
                    Code = code,
                    Description = product.Description,
                    Ncm = databaseTax.NcmCode,
                    Cest = (_applicationUser.PropertyDistrictCode != "SP") ? databaseTax.CestCode : null,
                    Quantity = product.Quantity,
                    UnitMeasurement = _unitMeasurementDefault,
                    UnitPrice = product.Amount,
                    AditionalValue = product.Rate,
                    DiscontValue = 0m,
                    TaxDetails = GenerateProductIBPTTaxDetails(GenerateProductTaxDetails(databaseTax), databaseTax.NcmCode, product.Description, (product.Quantity * product.Quantity)).Result
                });
            }

            return itensList;
        }

        private void ValidateTaxDetails(TaxRulesDto databaseTax)
        {
            var requiredParamsList = new List<string>();

            if (string.IsNullOrEmpty(databaseTax.CestCode))
                requiredParamsList.Add(nameof(databaseTax.CestCode));

            if (_applicationUser.PropertyDistrictCode != "SP" && databaseTax.Cfop == "5105")
                _notificationHandler.Raise(_notificationHandler.DefaultBuilder
                  .WithMessage(AppConsts.LocalizationSourceName, TaxRuleEnum.Error.InvalidInvoiceCFOP)
                  .WithDetailedMessage(AppConsts.LocalizationSourceName, TaxRuleEnum.Error.InvalidInvoiceCFOP)
                  .Build());

            if (requiredParamsList.Count > 0)
                _notificationHandler.Raise(_notificationHandler.DefaultBuilder
                   .WithMessage(AppConsts.LocalizationSourceName, TaxRuleEnum.Error.InvoiceRequiredParams)
                   .WithDetailedMessage(string.Concat(_defaultParams, String.Join(", ", requiredParamsList.ToArray())))
                   .Build());
        }

        private TaxDto GenerateProductTaxDetails(TaxRulesDto databaseTax)
        {
            var taxDetail = new TaxDto();

            decimal? taxReduction = databaseTax.TaxPercentualBase != null && databaseTax.TaxPercentualBase != _taxBasePercentual ?
                                        databaseTax.TaxPercentualBase : null;

            decimal? tax = databaseTax.TaxPercentual;
            decimal? taxBase = databaseTax.TaxPercentualBase == null ? 100 : databaseTax.TaxPercentualBase.TryParseToDecimal();
            decimal? taxSt = null;
            decimal? taxBaseSt = null;
            decimal? taxReductionSt = null;

            // Tratamento substitui��o tribut�ria
            if (databaseTax.CstB == _taxSubCode)
            {
                taxSt = databaseTax.TaxPercentual;
                taxBaseSt = taxBase;
                tax = null;
                taxBase = null;

                if (taxReduction != null &&
                    taxReduction != _taxBasePercentual)
                {
                    taxReductionSt = taxReduction;
                    taxReduction = null;
                }
            }

            taxDetail = new TaxDto()
            {
                IcmsTaxDetails = new IcmsTaxDto()
                {
                    Code = databaseTax.CstB,
                    Source = databaseTax.CstA, //: TODO: Enotas
                    Tax = tax,
                    TaxBase = taxBase,
                    TaxST = taxSt,
                    TaxBaseST = taxBaseSt,
                    TaxBaseReduction = taxReduction,
                    TaxBaseSTReduction = taxReductionSt
                },
                PisTaxDetails = new PisTaxDto()
                {
                    Code = databaseTax.CstPisCofins,
                },
                CofinsTaxDetails = new CofinsTaxDto()
                {
                    Code = databaseTax.CstPisCofins
                }
            };

            // Envia o percentual de PIS e COFINS n�o cumulativo
            if (databaseTax.TaxCalculation == _taxCalculation)
            {
                taxDetail.PisTaxDetails.TaxPercentageDetails = new PisTaxPercentageDto()
                {
                    Tax = _taxCalculationPIS
                };


                taxDetail.CofinsTaxDetails.TaxPercentageDetails = new CofinsTaxPercentageDto()
                {
                    Tax = _taxCalculationCOFINS
                };
            }

            return taxDetail;
        }

        private async Task<TaxDto> GenerateProductIBPTTaxDetails(TaxDto productInvoiceTax, string ncmCode, string description, decimal amount)
        {
            var client = new HttpClient();

            var responseMessage = await Policy
                           .HandleResult<HttpResponseMessage>(message => !message.IsSuccessStatusCode)
                           .WaitAndRetryAsync(1, i => TimeSpan.FromSeconds(30), (result, timeSpan, retryCount, context) =>
                           {
                               Console.Write($"Request failed with {result.Result.StatusCode}. Waiting {timeSpan} before next retry. Retry attempt {retryCount}");
                           })
                           .ExecuteAsync(() => client.GetAsync(GetIBPTProductUrl(ncmCode, description, amount)));

            var resultData = await responseMessage.Content.ReadAsStringAsync();

            if (responseMessage.IsSuccessStatusCode && !string.IsNullOrEmpty(resultData))
            {
                var ibptTax = JsonConvert.DeserializeObject<IBPTResponseDto>(resultData);

                productInvoiceTax.TaxPredictionDetails = new TaxPrediction()
                {
                    DetailedTaxPredictionDetails = new DetailedTaxPrediction()
                    {
                        CityTax = ibptTax.CityTax,
                        FederalTax = ibptTax.FederalTax,
                        StateTax = ibptTax.StateTax
                    },
                    Source = ibptTax.Source
                };
            }

            return productInvoiceTax;
        }

        /// <summary>
        /// Gera os detalhes da nota de servi�o
        /// </summary>
        /// <param name="serviceInvoice"></param>
        /// <returns></returns>
        private NFSeDetailsDto GenerateServiceDetails(IntegrationServiceInvoiceDTO serviceInvoice, string propertyUUId, string clientId)
        {
            // TODO: Enotas
            // Incluir valida��o apenas ap�s o ajuste no cadastro de tributos de servi�o
            //var databaseTax = GetTribute(Guid.Parse(propertyUUId), new GetAllTaxRulesWithParamsDto()
            //{
            //    Nbs = serviceInvoice.Nbs,
            //    TwoLetterUf = _applicationUser.PropertyDistrictCode,
            //}).Result;

            //if (databaseTax != null)
            //    return GenerateServiceDetailsWithDatabaseTax(databaseTax, serviceInvoice.Description);

            return new NFSeDetailsDto()
            {
                Description = serviceInvoice.Description,
                TaxRetained = string.IsNullOrEmpty(clientId) ? false : _retainedClientReadRepository.isRetainedClient(Guid.Parse(clientId))
            };
        }

        /// <summary>
        /// Gera os detalhes da nota de servi�o com os dados cadastrados
        /// </summary>
        /// <param name="databaseTax">Entidade com as propiedades do banco de dados</param>
        /// <param name="description">Descri��o do servi�o</param>
        /// <returns></returns>
        private NFSeDetailsDto GenerateServiceDetailsWithDatabaseTax(TaxRulesDto databaseTax, string description)
        {
            return new NFSeDetailsDto()
            {
                Code = databaseTax.NbsCode.ToString(),
                Description = description,
                Tax = databaseTax.TaxPercentual,
            };
        }

        /// <summary>
        /// Gera os dados do cliente do invoice
        /// </summary>
        /// <param name="serviceInvoice"></param>
        /// <returns></returns>
        private PersonDto GeneratePersonDetails(InvoiceIntegrationDto serviceInvoice)
        {
            AddressDto address = null;

            if (serviceInvoice.Client.Address != null)
                address = new AddressDto()
                {
                    Country = serviceInvoice.Client.Address.Country,
                    District = serviceInvoice.Client.Address.District?.RemoveAccentuationAndSpecialChars(),
                    StreetName = serviceInvoice.Client.Address.StreetName?.RemoveAccentuationAndSpecialChars(),
                    StreetNumber = serviceInvoice.Client.Address.StreetNumber,
                    AdditionalInformation = serviceInvoice.Client.Address.AdditionalInformation != null ? serviceInvoice.Client.Address.AdditionalInformation.PadRight(29, ' ').Trim() : null,
                    PostalCode = serviceInvoice.Client.Address.PostalCode,
                    Neighborhood = serviceInvoice.Client.Address.Neighborhood?.RemoveAccentuationAndSpecialChars(),
                    City = serviceInvoice.Client.Address.City.Name?.RemoveAccentuationAndSpecialChars()
                };

            var person = new PersonDto()
            {
                Type = GetPersonType(serviceInvoice.Client.Document),
                Name = serviceInvoice.Client.Name?.RemoveAccentuationAndSpecialChars(),
                Email = serviceInvoice.Client.Email,
                Document = serviceInvoice.Client.Document,
                Phone = serviceInvoice.Client.Phone,
                Address = address
            };

            if (person.Name.IsNullOrEmpty() &&
                person.Document.IsNullOrEmpty() &&
                address.Country.IsNullOrEmpty() &&
                address.City.IsNullOrEmpty())
            {
                return null;
            }

            return person;
        }

        /// <summary>
        /// Retorna o tipo da pessoa atrav�s do documento
        /// </summary>
        /// <param name="document"></param>
        /// <returns></returns>
        private string GetPersonType(string document)
        {
            var docType = DocumentValidatorHelper.ValidateDocumentType(document);
            return (docType == DocumentTypeEnum.BrLegalPersonCNPJ ? "J" :
                    docType == DocumentTypeEnum.BrNaturalPersonCPF ? "F" : "F");
        }

        public async Task<TaxRulesDto> Create(TaxRulesDto dto)
        {
            Domain.Entities.TaxRule taxRule = null;

            using (var uow = _unitOfWorkManager.Begin())
            {
                taxRule = _taxRulesAdapter.Map(dto).Build();

                if (_notificationHandler.HasErrorNotification())
                    return TaxRulesDto.NullInstance;

                if (dto.PropertyList.Count > 0)
                {
                    foreach (var propertyDetails in dto.PropertyList)
                    {
                        taxRule.PropertyUId = propertyDetails.PropertyUId;
                        taxRule.TenantId = propertyDetails.TenantId;

                        await _taxRulesRepository.InsertTaxRule(taxRule);

                        taxRule = _taxRulesAdapter.Map(dto).Build();
                    }
                }
                else
                {
                    await _taxRulesRepository.InsertTaxRule(taxRule);
                }

                uow.Complete();
            }

            if (_notificationHandler.HasErrorNotification())
                return TaxRulesDto.NullInstance;

            return dto;
        }

        public Task<IListDto<TaxRulesDto>> GetAll(int type, GetAllTaxRulesDto requestDto)
        {
            return _taxRulesReadRepository.GetAll(type, requestDto);
        }

        public Task<TaxRulesDto> GetTribute(Guid propertyUUid, GetAllTaxRulesWithParamsDto requestDto)
        {
            return _taxRulesReadRepository.GetTributes(propertyUUid, requestDto);
        }

        public override Domain.Entities.TaxRule GeneratedEntity(TaxRulesDto dto)
        {
            return _taxRulesAdapter.Map(dto).Build();
        }

        /// <summary>
        /// Valida se a regra tribut�ria foi cadastrada corretamente
        /// </summary>
        /// <param name="serviceInvoiceList">Lista de NBS</param>
        /// <param name="productInvoiceList">Lista de NCM ou C�digo de Barras</param>
        /// <returns></returns>
        public Task<bool> CheckExistTax(List<IntegrationServiceInvoiceDTO> serviceInvoiceList = null, List<IntegrationProductInvoiceDTO> productInvoiceList = null)
        {
            if (serviceInvoiceList != null && serviceInvoiceList.Count > 0)
                return _taxRulesReadRepository.ValidateExistServiceTax(serviceInvoiceList.Select(x => x.Nbs).ToList());

            if (productInvoiceList != null && productInvoiceList.Count > 0)
            {
                return _taxRulesReadRepository.ValidateExistProductTax(productInvoiceList.DistinctBy(p => new { p.Code }).Where(x => x.Barcode != null).Select(x => x.Barcode).ToList(),
                                                                       productInvoiceList.DistinctBy(p => new { p.Code }).Where(x => x.Ncm != null).Select(x => x.Ncm).ToList(),
                                                                       productInvoiceList.DistinctBy(p => new { p.Code }).Count());
            }

            return Task.FromResult(true);
        }

        public async Task<InvoiceCancelResponseDto> Cancel(InvoiceIntegrationDto invoiceDto, int invoiceType)
        {
            if (_notificationHandler.HasNotification())
                return new InvoiceCancelResponseDto(false);

            var cancelInvoice = await CancelInvoiceIntegration(invoiceDto, invoiceType);

            return new InvoiceCancelResponseDto(cancelInvoice);
        }

        private async Task<bool> CancelInvoiceIntegration(InvoiceIntegrationDto invoiceDto, int invoiceType)
        {
            IntegrationInvoiceServiceBaseDto integrationInvoice = null;

            if (invoiceType == (int)BillingInvoiceTypeEnum.NFSE)
                integrationInvoice = new NFSeInvoice(invoiceDto.ExternalId, invoiceDto.Environment,
                                                     invoiceDto.IntegrationCode, invoiceDto.ExternalTypeId);
            else
            if (invoiceType == (int)BillingInvoiceTypeEnum.NFCE)
                integrationInvoice = new NFCeInvoice(invoiceDto.ExternalId, string.Empty, invoiceDto.Environment,
                                                     false, invoiceDto.IntegrationCode, null, string.Empty);
            else
                return false;

            return await new InvoiceService(invoiceDto.TokenClient,
                                            _genericLog,
                                            _localizationManager,
                                            _notificationHandler,
                                            _configuration).DeleteAsync(integrationInvoice, GetCancelInvoiceUrl(integrationInvoice), _notificationHandler);
        }

        private string GetCancelInvoiceUrl(IntegrationInvoice integrationInvoice)
        {
            var url = $"{_configuration.GetValue<string>("BaseEndPoint")}";

            if (integrationInvoice.InvoiceType == "NFSe")
                url = url + $"/v{_configuration.GetValue<string>("VersaoService")}/empresas/{integrationInvoice.PropertyIntegrationCode}/nfes/porIdExterno/{integrationInvoice.Id}";
            else if (integrationInvoice.InvoiceType == "NFCe")
                url = url + $"/v{_configuration.GetValue<string>("VersaoProduct")}/empresas/{integrationInvoice.PropertyIntegrationCode}/nfc-e/{integrationInvoice.Id}";
            else
                throw new NotImplementedException();

            return url;
        }

        private string GetIBPTProductUrl(string ncmCode, string description, decimal amount)
        {
            return $"{_configuration.GetValue<string>("BaseIBPTEndPoint")}" +
                                $"/v{_configuration.GetValue<string>("VersaoIBPT")}/produtos?" +
                                $"token={_configuration.GetValue<string>("TokenIBPT")}&" +
                                $"cnpj={_configuration.GetValue<string>("CNPJIBPT")}&" +
                                $"codigo={ncmCode}&uf={_applicationUser.PropertyDistrictCode}&ex=0&descricao={description}&unidadeMedida=UN&" +
                                $"valor={amount.ToString().Replace(',', '.')}&gtin={ncmCode}";
        }

        private string GetIBPTServiceUrl(string serviceCode, decimal amount)
        {
            return $"{_configuration.GetValue<string>("BaseIBPTEndPoint")}" +
                                $"/v{_configuration.GetValue<string>("VersaoIBPT")}/servicos?" +
                                $"token={_configuration.GetValue<string>("TokenIBPT")}&" +
                                $"cnpj={_configuration.GetValue<string>("CNPJIBPT")}&" +
                                $"codigo={serviceCode.Substring(0, 4)}&uf={_applicationUser.PropertyDistrictCode}&descricao=IBPT&unidadeMedida=UN&" +
                                $"valor={amount.ToString().Replace(',', '.')}";
        }

        private string GetInvoiceUrl(IntegrationInvoice integrationInvoice)
        {
            var url = $"{_configuration.GetValue<string>("BaseEndPoint")}";

            if (integrationInvoice.InvoiceType == "NFSe")
                url = url + $"/v{_configuration.GetValue<string>("VersaoService")}/empresas/{integrationInvoice.PropertyIntegrationCode}/nfes";
            else if (integrationInvoice.InvoiceType == "NFCe" || integrationInvoice.InvoiceType == "SAT")
                url = url + $"/v{_configuration.GetValue<string>("VersaoProduct")}/empresas/{integrationInvoice.PropertyIntegrationCode}/nfc-e";
            else
                throw new NotImplementedException();

            return url;
        }

        private string GetServiceInvoiceRequestUrl(IntegrationInvoice integrationInvoice)
        {
            return $"{_configuration.GetValue<string>("BaseEndPoint")}/v{_configuration.GetValue<string>("VersaoService")}/empresas/{integrationInvoice.PropertyIntegrationCode}/nfes/porIdExterno/{integrationInvoice.ExternalId}";
        }

        private string GetProductInvoiceRequestUrl(IntegrationInvoice integrationInvoice)
        {
            return $"{_configuration.GetValue<string>("BaseEndPoint")}/v{_configuration.GetValue<string>("VersaoProduct")}/empresas/{integrationInvoice.PropertyIntegrationCode}/nfc-e/{integrationInvoice.ExternalId}";
        }
    }
}
