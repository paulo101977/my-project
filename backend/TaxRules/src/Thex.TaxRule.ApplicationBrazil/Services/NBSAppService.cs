﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Thex.Kernel;
using Thex.TaxRule.Application.Brazil.Adapters;
using Thex.TaxRule.Application.Brasil.Interfaces;
using Thex.TaxRule.Domain.Entities;
using Thex.TaxRule.Dto;
using Thex.TaxRule.Infra.Interfaces.ReadRepositories;
using Tnf.Dto;
using Tnf.Notifications;
using Thex.TaxRule.Dto.GetAll;

namespace Thex.TaxRule.Application.Brasil.Services
{
    public class NBSAppService : ApplicationServiceBase<NBSDto, NBS>, INBSAppService
    {
        private IApplicationUser _applicationUser { get; set; }
        private INBSAdapter _NBSAdapter { get; set; }
        public INBSReadRepository _NBSReadRepository { get; set; }

        public NBSAppService(IApplicationUser applicationUser,
                             INotificationHandler notificationHandler,
                             INBSAdapter NBSAdapter,
                             INBSReadRepository NBSReadRepository)
                             : base(notificationHandler, null)
        {
            _applicationUser = applicationUser;
            _NBSAdapter = NBSAdapter;
            _NBSReadRepository = NBSReadRepository;
        }

        public Task<IListDto<NBSDto>> GetAll(GetAllNBSDto requestDto)
        {
            return _NBSReadRepository.GetAll(requestDto);
        }

        public Task<NBSDto> GetByCode(string code, GetAllNBSDto requestDto)
        {
            return _NBSReadRepository.GetByCode(code, requestDto);
        }

        public Task<IListDto<NBSDto>> GetAllNbsByFilters(SearchNbsDto searchDto)
        {
            return _NBSReadRepository.GetAllByFilters(searchDto);
        }

        public override NBS GeneratedEntity(NBSDto dto)
        {
            return _NBSAdapter.Map(dto).Build();
        }
    }
}