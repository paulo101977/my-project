﻿using hex.TaxRule.Application.Brazil.Adapters;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.TaxRule.Application.Brasil.Interfaces;
using Thex.TaxRule.Domain.Entities;
using Thex.TaxRule.Dto;
using Thex.TaxRule.Infra.Interfaces;
using Thex.TaxRule.Infra.Interfaces.ReadRepositories;
using Tnf.Dto;
using Tnf.Notifications;
using System.Linq;

namespace Thex.TaxRule.Application.Brasil.Services
{
    public class ImpostoRetidoAppService : ApplicationServiceBase<ImpostoRetidoDto, ImpostoRetido>, IImpostoRetidoAppService
    {
        private readonly IImpostoRetidoAdapter _impostoRetidoAdapter;
        private readonly IImpostoRetidoRepository _impostoRetidoRepository;
        private readonly IImpostoRetidoReadRepository _impostoRetidoReadRepository;

        public ImpostoRetidoAppService(
            INotificationHandler notificationHandler,
            IImpostoRetidoAdapter impostoRetidoAdapter,
            IImpostoRetidoRepository impostoRetidoRepository,
            IImpostoRetidoReadRepository impostoRetidoReadRepository)
            : base(notificationHandler, null)
        {
            _impostoRetidoAdapter = impostoRetidoAdapter;
            _impostoRetidoRepository = impostoRetidoRepository;
            _impostoRetidoReadRepository = impostoRetidoReadRepository;
        }

        public async Task<IListDto<ImpostoRetidoDto>> GetAllByOwnerIdAndPropertyUid(GetAllImpostoRetidoDto requestDto, Guid propertyUid)
        {
            return await _impostoRetidoReadRepository.GetAllByOwnerIdAndPropertyUid(requestDto, propertyUid);
        }

        public async Task CreateList(List<ImpostoRetidoDto> dtoList)
        {
            var impostoRetidoEntityList = new List<ImpostoRetido>();

            dtoList.ForEach(dto => {
                impostoRetidoEntityList.Add(GeneratedEntity(dto));
            });

            await _impostoRetidoRepository.InsertList(impostoRetidoEntityList);
        }

        public override ImpostoRetido GeneratedEntity(ImpostoRetidoDto dto)
        {
            return _impostoRetidoAdapter.Map(dto).Build();
        }

        public async Task Delete(Guid ownerId, Guid propertyUId)
        {
            await _impostoRetidoRepository.DeleteImpostoRetido(ownerId, propertyUId);
        }
    }
}
