﻿using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Polly;
using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Thex.AspNetCore.Security;
using Thex.Common;
using Thex.Common.Enumerations;
using Thex.GenericLog;
using Thex.TaxRule.Domain.Enumerations;
using Thex.TaxRule.Dto.Dto.Integration;
using Thex.TaxRule.Dto.Dto.Integration.Error;
using Thex.TaxRule.Dto.Dto.Integration.Exceptions;
using Thex.TaxRule.Dto.Dto.Integration.Exceptions.Base;
using Thex.TaxRule.Dto.Dto.Integration.Response;
using Tnf.Localization;
using Tnf.Notifications;

namespace Thex.TaxRule.Application.Brazil.Services.Integration
{
    public class InvoiceService : IDisposable
    {
        private readonly string _enotasIntegrationIndexName = "taxrule-database-log";
        private string _autorizedCode = "NFe0003";
        private string _authorizedDescription = "Autorizada";
        private string _propertyIntegrationCode = "848CC06F-8717-4D44-9706-1B13E3AA0400";
        private string _internalErrorMessage = "TaxRule internal error occurred";
        private HttpClient _client;
        private IGenericLogHandler _genericLog;
        private ILocalizationManager _localizationManager;
        private INotificationHandler _notificationHandler;
        private IConfiguration _configuration;

        public InvoiceService(string tokenClient,
                              IGenericLogHandler genericLogHandler,
                              ILocalizationManager localizationManager,
                              INotificationHandler notificationHandler,
                              IConfiguration configuration)
        {
            _configuration = configuration;
            _localizationManager = localizationManager;
            _genericLog = genericLogHandler;
            _notificationHandler = notificationHandler;
            _client = new HttpClient();
            _client.DefaultRequestHeaders.Clear();
            _client.DefaultRequestHeaders.Add("Authorization", "Basic " + HashManager.GenerateMD5Decrypto(tokenClient));
            _client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

        /// <summary>
        /// Emite uma nota fiscal
        /// </summary>
        /// <param name="empresaId"></param>
        /// <param name="nfe"></param>
        /// <returns></returns>
        public async Task<InvoiceResponseDto> SendAsync(IntegrationInvoice invoice, string url, string externalIdRequestUrl = "")
        {
            if (invoice.PropertyIntegrationCode == _propertyIntegrationCode)
            {
                invoice.Environment = "Homologação";
                _client.DefaultRequestHeaders.Clear();
                _client.DefaultRequestHeaders.Add("Authorization", ("Basic " + _configuration.GetValue<string>("EnotasToken")));
                _client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            }

            try
            {
                var jsonInvoice = JsonConvert.SerializeObject(invoice, Formatting.None,
                                  new JsonSerializerSettings
                                  {
                                      NullValueHandling = NullValueHandling.Ignore
                                  });

                if (!jsonInvoice.IsNullOrEmpty())
                    _genericLog.AddLog(_genericLog.DefaultBuilder
                       .WithMessage(_localizationManager.GetString(AppConsts.LocalizationSourceName, TaxRuleEnum.Message.SendInvoice.ToString()))
                       .WithDetailedMessage(jsonInvoice.Replace(@"\", ""))
                       .AsInformation().Build());

                var response = await Policy
                               .HandleResult<HttpResponseMessage>(message => !message.IsSuccessStatusCode)
                               .WaitAndRetryAsync(1, i => TimeSpan.FromSeconds(10), (result, timeSpan, retryCount, context) =>
                               {
                                   Console.Write($"Request failed with {result.Result.StatusCode}. Waiting {timeSpan} before next retry. Retry attempt {retryCount}");
                               })
                               .ExecuteAsync(() => _client.PostAsync(url, new StringContent(jsonInvoice, Encoding.UTF8, "application/json")));

                if (response.IsSuccessStatusCode)
                    return GenerateInvoiceIntegrationSucessResponse(await response.Content.ReadAsStringAsync(), invoice.InvoiceType, invoice);
                else
                    return GenerateInvoiceIntegrationErrorResponse(await response.Content.ReadAsStringAsync(), response.StatusCode, response.ReasonPhrase, externalIdRequestUrl, invoice);
            }
            catch (BaseExceptionDto)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw (new GeneralExceptionDto(_internalErrorMessage, ex));
            }
        }

        private InvoiceResponseDto GenerateInvoiceIntegrationErrorResponse(string responseMessage, HttpStatusCode statusCode, string reasonPhrase, string externalIdRequestUrl = "", IntegrationInvoice invoice = null)
        {
            InvoiceResponseDto responseDetails = null;

            responseMessage = responseMessage.Replace(@"\""", "'");

            GenerateInvoiceLogError(responseMessage, invoice);

            var messageException = new StringBuilder(((int)statusCode) + " - " + reasonPhrase);

            try
            {
                responseDetails = JsonConvert.DeserializeObject<InvoiceResponseDto>(responseMessage);
            }
            catch
            {

            }

            if (responseDetails != null && responseDetails.Status != _authorizedDescription)
            {
                throw new ValidationExceptionDto(responseDetails.Status + " - " + responseDetails.Reason);
            }

            var dataResponse = JsonConvert.DeserializeObject<ErrorDto[]>(responseMessage);
            if (dataResponse != null)
            {
                foreach (var message in dataResponse)
                {
                    messageException.AppendLine(message.Code + " - " + message.Message);

                    if (message.Code.Contains(_autorizedCode) && !string.IsNullOrEmpty(externalIdRequestUrl) && (invoice != null && !string.IsNullOrEmpty(invoice.InvoiceType)))
                    {
                        var response = Policy
                               .HandleResult<HttpResponseMessage>(result => !result.IsSuccessStatusCode)
                               .WaitAndRetryAsync(1, i => TimeSpan.FromSeconds(10), (result, timeSpan, retryCount, context) =>
                               {
                                   Console.Write($"Request failed with {result.Result.StatusCode}. Waiting {timeSpan} before next retry. Retry attempt {retryCount}");
                               })
                               .ExecuteAsync(() => _client.GetAsync(externalIdRequestUrl)).Result;

                        if (response.IsSuccessStatusCode)
                            return GenerateInvoiceIntegrationSucessResponse(response.Content.ReadAsStringAsync().Result, invoice.InvoiceType);
                    }
                    else
                        throw new ValidationExceptionDto(messageException.ToString(), dataResponse);
                }
            }

            _notificationHandler.Raise(_notificationHandler
             .DefaultBuilder
             .WithMessage(AppConsts.LocalizationSourceName, BillingAccountItemEnum.Error.BillingAccountInvoiceNFeioComunicationError)
             .WithDetailedMessage(messageException.ToString())
             .Build());

            if (statusCode == HttpStatusCode.BadRequest)
            {
                throw new ValidationExceptionDto(messageException.ToString(), dataResponse);
            }
            else if (statusCode == HttpStatusCode.Forbidden)
            {
                throw new AuthorizationExceptionDto(messageException.ToString(), dataResponse);
            }
            else if (statusCode == HttpStatusCode.Unauthorized)
            {
                throw new AuthenticationExceptionDto(messageException.ToString(), dataResponse);
            }
            else
            {
                throw new GeneralExceptionDto(messageException.ToString(), dataResponse);
            }
        }

        private void GenerateInvoiceLogError(string responseMessage, IntegrationInvoice invoice)
        {
            if (!string.IsNullOrEmpty(responseMessage) && invoice != null)
                _genericLog.AddLog(_genericLog.DefaultBuilder
                                 .WithMessage($"Invoice: {invoice.ExternalId}")
                                 .WithDetailedMessage(responseMessage.Replace(@"\", ""))
                                 .AsError().Build());

            if (_genericLog.HasGenericLog())
                _genericLog.Commit(_enotasIntegrationIndexName);
        }

        private void GenerateInvoiceLogSucess(string responseMessage, IntegrationInvoice invoice)
        {
            if (!string.IsNullOrEmpty(responseMessage) && invoice != null)
                _genericLog.AddLog(_genericLog.DefaultBuilder
                                 .WithMessage($"Invoice: {invoice.ExternalId}")
                                 .WithDetailedMessage(responseMessage.Replace(@"\", ""))
                                 .AsError().Build());

            if (_genericLog.HasGenericLog())
                _genericLog.Commit(_enotasIntegrationIndexName);
        }

        private InvoiceResponseDto GenerateInvoiceIntegrationSucessResponse(string responseMessage, string invoiceType, IntegrationInvoice invoice = null)
        {
            responseMessage = responseMessage.Replace(@"\""", "'");

            var responseDetails = JsonConvert.DeserializeObject<InvoiceResponseDto>(responseMessage);

            if (invoiceType == "NFSe" || invoiceType == "SAT" ||
                (invoiceType == "NFCe" && responseDetails.Status == _authorizedDescription))
            {
                GenerateInvoiceLogSucess(responseMessage, invoice);
                return responseDetails;
            }

            if ((invoiceType == "NFCe" || invoiceType == "SAT") && responseDetails.Status != _authorizedDescription)
            {
                GenerateInvoiceIntegrationErrorResponse(responseMessage, HttpStatusCode.BadRequest, responseDetails.Reason);
            }

            GenerateInvoiceLogSucess(responseMessage, invoice);

            return responseDetails;
        }

        /// <summary>
        /// Cancela uma nota fiscal
        /// </summary>
        /// <param name="empresaId"></param>
        /// <param name="nfe"></param>
        /// <returns></returns>
        public async Task<bool> DeleteAsync(IntegrationInvoice invoice, string url, INotificationHandler notificationHandler)
        {
            // TODO: Enotas
            // Criar maneira de alterar o ambiente
            if (invoice.PropertyIntegrationCode == _propertyIntegrationCode)
                invoice.Environment = "Homologação";

            try
            {
                using (var request = new HttpRequestMessage(HttpMethod.Delete, url))
                {
                    using (var response = await _client.SendAsync(request))
                    {
                        var resultContent = await response.Content.ReadAsStringAsync();

                        if (response.IsSuccessStatusCode)
                            return true;
                        else
                        {
                            var messageException = new StringBuilder(((int)response.StatusCode) + " - " + response.ReasonPhrase);

                            var notification = notificationHandler.DefaultBuilder
                                      .WithMessage(AppConsts.LocalizationSourceName, TaxRuleEnum.Error.CancelInvoiceError);

                            var dataResponse = JsonConvert.DeserializeObject<ErrorDto[]>(resultContent);
                            if (dataResponse != null)
                                foreach (var message in dataResponse)
                                    messageException.AppendLine($", {message.Code} - {message.Message}");

                            notificationHandler.Raise(notification.WithDetailedMessage(messageException.ToString()).Build());

                            return false;
                        }
                    }
                }
            }
            catch (BaseExceptionDto)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw (new GeneralExceptionDto(_internalErrorMessage, ex));
            }
        }

        public void Dispose()
        {
            _client.Dispose();
        }
    }
}
