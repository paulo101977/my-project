﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Thex.Kernel;
using Thex.TaxRule.Application.Brazil.Adapters;
using Thex.TaxRule.Application.Brasil.Interfaces;
using Thex.TaxRule.Domain.Entities;
using Thex.TaxRule.Dto;
using Thex.TaxRule.Infra.Interfaces.ReadRepositories;
using Tnf.Dto;
using Tnf.Notifications;

namespace Thex.TaxRule.Application.Brasil.Services
{
    public class RegimeApuracaoAppService : ApplicationServiceBase<RegimeApuracaoDto, RegimeApuracao>, IRegimeApuracaoAppService
    {
        private IApplicationUser _applicationUser { get; set; }
        private IRegimeApuracaoAdapter _regimeApuracaoAdapter { get; set; }
        private IRegimeApuracaoReadRepository _regimeApuracaoReadRepository { get; set; }

        public RegimeApuracaoAppService(IApplicationUser applicationUser,
                             INotificationHandler notificationHandler,
                             IRegimeApuracaoAdapter regimeApuracaoAdapter,
                             IRegimeApuracaoReadRepository regimeApuracaoReadRepository)
                             : base(notificationHandler, null)
        {
            _applicationUser = applicationUser;
            _regimeApuracaoAdapter = regimeApuracaoAdapter;
            _regimeApuracaoReadRepository = regimeApuracaoReadRepository;
        }

        public Task<IListDto<RegimeApuracaoDto>> GetAll(GetAllRegimeApuracaoDto requestDto)
        {
            return _regimeApuracaoReadRepository.GetAll(requestDto);
        }

        public Task<RegimeApuracaoDto> GetByCode(Guid id, GetAllRegimeApuracaoDto requestDto)
        {
            return _regimeApuracaoReadRepository.GetByCode(id, requestDto);
        }

        public override RegimeApuracao GeneratedEntity(RegimeApuracaoDto dto)
        {
            return _regimeApuracaoAdapter.Map(dto).Build();
        }
    }
}