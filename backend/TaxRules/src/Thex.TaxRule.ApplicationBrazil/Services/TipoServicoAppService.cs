﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Thex.Kernel;
using Thex.TaxRule.Application.Brazil.Adapters;
using Thex.TaxRule.Application.Brasil.Interfaces;
using Thex.TaxRule.Domain.Entities;
using Thex.TaxRule.Dto;
using Thex.TaxRule.Infra.Interfaces.ReadRepositories;
using Tnf.Dto;
using Tnf.Notifications;

namespace Thex.TaxRule.Application.Brasil.Services
{
    public class TipoServicoAppService : ApplicationServiceBase<TipoServicoDto, TipoServico>, ITipoServicoAppService
    {
        private IApplicationUser _applicationUser { get; set; }
        private ITipoServicoAdapter _tipoServicoAdapter { get; set; }
        private ITipoServicoReadRepository _tipoServicoReadRepository { get; set; }

        public TipoServicoAppService(IApplicationUser applicationUser,
                             INotificationHandler notificationHandler,
                             ITipoServicoAdapter tipoServicoAdapter,
                             ITipoServicoReadRepository tipoServicoReadRepository)
                             : base(notificationHandler, null)
        {
            _applicationUser = applicationUser;
            _tipoServicoAdapter = tipoServicoAdapter;
            _tipoServicoReadRepository = tipoServicoReadRepository;
        }

        public Task<IListDto<TipoServicoDto>> GetAll(GetAllTipoServicoDto requestDto)
        {
            return _tipoServicoReadRepository.GetAll(requestDto);
        }

        public Task<TipoServicoDto> GetByCode(string code, GetAllTipoServicoDto requestDto)
        {
            return _tipoServicoReadRepository.GetByCode(code, requestDto);
        }

        public override TipoServico GeneratedEntity(TipoServicoDto dto)
        {
            return _tipoServicoAdapter.Map(dto).Build();
        }
    }
}