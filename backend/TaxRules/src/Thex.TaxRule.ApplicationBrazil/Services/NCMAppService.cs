﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Thex.Kernel;
using Thex.TaxRule.Application.Brazil.Adapters;
using Thex.TaxRule.Application.Brasil.Interfaces;
using Thex.TaxRule.Domain.Entities;
using Thex.TaxRule.Dto;
using Thex.TaxRule.Infra.Interfaces.ReadRepositories;
using Tnf.Dto;
using Tnf.Notifications;
using Thex.TaxRule.Dto.GetAll;

namespace Thex.TaxRule.Application.Brasil.Services
{
    public class NCMAppService : ApplicationServiceBase<NCMDto, NCM>, INCMAppService
    {
        private IApplicationUser _applicationUser { get; set; }
        private INCMAdapter _NCMAdapter { get; set; }
        private INCMReadRepository _NCMReadRepository { get; set; }

        public NCMAppService(IApplicationUser applicationUser,
                             INotificationHandler notificationHandler,
                             INCMAdapter NCMAdapter,
                             INCMReadRepository NCMReadRepository)
                             : base(notificationHandler, null)
        {
            _applicationUser = applicationUser;
            _NCMAdapter = NCMAdapter;
            _NCMReadRepository = NCMReadRepository;
        }

        public Task<IListDto<NCMDto>> GetAll(GetAllNCMDto requestDto)
        {
            return _NCMReadRepository.GetAll(requestDto);
        }

        public Task<NCMDto> GetByCode(string code, GetAllNCMDto requestDto)
        {
            return _NCMReadRepository.GetByCode(code, requestDto);
        }
        public Task<IListDto<NCMDto>> GetAllByFilters(SearchNcmDto searchDto)
        {
            return _NCMReadRepository.GetAllByFilters(searchDto);
        }

        public override NCM GeneratedEntity(NCMDto dto)
        {
            return _NCMAdapter.Map(dto).Build();
        }
    }
}