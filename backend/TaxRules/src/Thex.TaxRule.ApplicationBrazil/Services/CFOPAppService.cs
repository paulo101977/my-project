﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Thex.Kernel;
using Thex.TaxRule.Application.Brazil.Adapters;
using Thex.TaxRule.Application.Brasil.Interfaces;
using Thex.TaxRule.Domain.Entities;
using Thex.TaxRule.Dto;
using Thex.TaxRule.Infra.Interfaces.ReadRepositories;
using Tnf.Dto;
using Tnf.Notifications;
using Thex.TaxRule.Dto.GetAll;

namespace Thex.TaxRule.Application.Brasil.Services
{
    public class CFOPAppService : ApplicationServiceBase<Dto.CFOPDto, Domain.Entities.CFOP>, ICFOPAppService
    {
        private IApplicationUser _applicationUser { get; set; }
        private ICFOPAdapter _CFOPAdapter { get; set; }
        public ICFOPReadRepository _CFOPReadRepository { get; set; }

        public CFOPAppService(IApplicationUser applicationUser,
                              INotificationHandler notificationHandler,
                              ICFOPAdapter CFOPAdapter,
                              ICFOPReadRepository CFOPReadRepository)
                              : base(notificationHandler, null)
        {
            _applicationUser = applicationUser;
            _CFOPAdapter = CFOPAdapter;
            _CFOPReadRepository = CFOPReadRepository;
        }

        public Task<IListDto<CFOPDto>> GetAll(GetAllCFOPDto requestDto)
        {
            return _CFOPReadRepository.GetAll(requestDto);
        }

        public Task<IListDto<CFOPDto>> GetAllByFilters(SearchCfopDto searchDto)
        {
            return _CFOPReadRepository.GetAllByFilters(searchDto);
        }

        public Task<CFOPDto> GetByCode(string code, GetAllCFOPDto requestDto)
        {
            return _CFOPReadRepository.GetByCode(code, requestDto);
        }

        public override CFOP GeneratedEntity(CFOPDto dto)
        {
            return _CFOPAdapter.Map(dto).Build();
        }
    }
}