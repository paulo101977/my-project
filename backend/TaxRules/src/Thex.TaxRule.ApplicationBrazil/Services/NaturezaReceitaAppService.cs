﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Thex.Kernel;
using Thex.TaxRule.Application.Brazil.Adapters;
using Thex.TaxRule.Application.Brasil.Interfaces;
using Thex.TaxRule.Domain.Entities;
using Thex.TaxRule.Dto;
using Thex.TaxRule.Infra.Interfaces.ReadRepositories;
using Tnf.Dto;
using Tnf.Notifications;

namespace Thex.TaxRule.Application.Brasil.Services
{
    public class NaturezaReceitaAppService : ApplicationServiceBase<NaturezaReceitaDto, NaturezaReceita>, INaturezaReceitaAppService
    {
        private IApplicationUser _applicationUser { get; set; }
        private INaturezaReceitaAdapter _naturezaReceitaAdapter { get; set; }
        public INaturezaReceitaReadRepository _naturezaReceitaReadRepository { get; set; }

        public NaturezaReceitaAppService(IApplicationUser applicationUser,
                                         INotificationHandler notificationHandler,
                                         INaturezaReceitaAdapter naturezaReceitaAdapter,
                                         INaturezaReceitaReadRepository naturezaReceitaReadRepository)
                                         : base(notificationHandler, null)
        {
            _applicationUser = applicationUser;
            _naturezaReceitaAdapter = naturezaReceitaAdapter;
            _naturezaReceitaReadRepository = naturezaReceitaReadRepository;
        }

        public Task<IListDto<NaturezaReceitaDto>> GetAll(GetAllNaturezaReceitaDto requestDto)
        {
            return _naturezaReceitaReadRepository.GetAll(requestDto);
        }

        public Task<IListDto<NaturezaReceitaDto>> GetByNcm(string ncm, GetAllNaturezaReceitaDto requestDto)
        {
            return _naturezaReceitaReadRepository.GetByNcm(ncm, requestDto);
        }

        public Task<NaturezaReceitaDto> GetByCode(string code, GetAllNaturezaReceitaDto requestDto)
        {
            return _naturezaReceitaReadRepository.GetByCode(code, requestDto);
        }

        public override NaturezaReceita GeneratedEntity(NaturezaReceitaDto dto)
        {
            return _naturezaReceitaAdapter.Map(dto).Build();
        }
    }
}