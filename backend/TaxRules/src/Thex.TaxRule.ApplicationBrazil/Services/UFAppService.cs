﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Thex.Kernel;
using Thex.TaxRule.Application.Brazil.Adapters;
using Thex.TaxRule.Application.Brasil.Interfaces;
using Thex.TaxRule.Domain.Entities;
using Thex.TaxRule.Dto;
using Thex.TaxRule.Infra.Interfaces.ReadRepositories;
using Tnf.Dto;
using Tnf.Notifications;

namespace Thex.TaxRule.Application.Brasil.Services
{
    public class UFAppService : ApplicationServiceBase<UFDto, UF>, IUFAppService
    {
        private IApplicationUser _applicationUser { get; set; }
        private IUFAdapter _ufAdapter { get; set; }
        private IUFReadRepository _ufReadRepository { get; set; }

        public UFAppService(IApplicationUser applicationUser,
                            INotificationHandler notificationHandler,
                            IUFAdapter ufAdapter,
                            IUFReadRepository ufReadRepository)
                            : base(notificationHandler, null)
        {
            _applicationUser = applicationUser;
            _ufAdapter = ufAdapter;
            _ufReadRepository = ufReadRepository;
        }

        public Task<IListDto<UFDto>> GetAll(GetAllUFDto requestDto)
        {
            return _ufReadRepository.GetAll(requestDto);
        }

        public Task<UFDto> GetByCode(GetAllUFDto requestDto)
        {
            return _ufReadRepository.GetByCode(requestDto);
        }

        public override UF GeneratedEntity(UFDto dto)
        {
            return _ufAdapter.Map(dto).Build();
        }
    }
}