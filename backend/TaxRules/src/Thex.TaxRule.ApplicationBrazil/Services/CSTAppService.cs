﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Thex.Kernel;
using Thex.TaxRule.Application.Brazil.Adapters;
using Thex.TaxRule.Application.Brasil.Interfaces;
using Thex.TaxRule.Domain.Entities;
using Thex.TaxRule.Dto;
using Thex.TaxRule.Infra.Interfaces.ReadRepositories;
using Tnf.Dto;
using Tnf.Notifications;

namespace Thex.TaxRule.Application.Brasil.Services
{
    public class CSTAppService : ApplicationServiceBase<CSTDto, CST>, ICSTAppService
    {
        private IApplicationUser _applicationUser { get; set; }
        private ICSTAdapter _CSTAdapter { get; set; }

        public ICSTReadRepository _CSTReadRepository { get; set; }

        public CSTAppService(IApplicationUser applicationUser,
                              INotificationHandler notificationHandler,
                              ICSTAdapter CSTAdapter,
                              ICSTReadRepository CSTReadRepository)
                              : base(notificationHandler, null)
        {
            _applicationUser = applicationUser;
            _CSTAdapter = CSTAdapter;
            _CSTReadRepository = CSTReadRepository;
        }

        public Task<IListDto<CSTDto>> GetAll(GetAllCSTDto requestDto)
        {
            return _CSTReadRepository.GetAll(requestDto);
        }

        public Task<IListDto<CSTDto>> GetAllByType(int type, GetAllCSTDto requestDto)
        {
            return _CSTReadRepository.GetAllByType(type, requestDto);
        }

        public Task<CSTDto> GetByCode(string code, GetAllCSTDto requestDto)
        {
            return _CSTReadRepository.GetByCode(code, requestDto);
        }

        public override CST GeneratedEntity(CSTDto dto)
        {
            return _CSTAdapter.Map(dto).Build();
        }
    }
}