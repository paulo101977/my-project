﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Thex.Kernel;
using Thex.TaxRule.Application.Brazil.Adapters;
using Thex.TaxRule.Application.Brasil.Interfaces;
using Thex.TaxRule.Domain.Entities;
using Thex.TaxRule.Dto;
using Thex.TaxRule.Infra.Interfaces.ReadRepositories;
using Tnf.Dto;
using Tnf.Notifications;

namespace Thex.TaxRule.Application.Brasil.Services
{
    public class CSOSNAppService : ApplicationServiceBase<CSOSNDto, CSOSN>, ICSOSNAppService
    {
        private IApplicationUser _applicationUser { get; set; }
        private ICSOSNAdapter _CSOSNAdapter { get; set; }
        public ICSOSNReadRepository _CSOSNReadRepository { get; set; }

        public CSOSNAppService(IApplicationUser applicationUser,
                              INotificationHandler notificationHandler,
                              ICSOSNAdapter CSOSNAdapter,
                              ICSOSNReadRepository CSOSNReadRepository)
                              : base(notificationHandler, null)
        {
            _applicationUser = applicationUser;
            _CSOSNAdapter = CSOSNAdapter;
            _CSOSNReadRepository = CSOSNReadRepository;
        }

        public Task<IListDto<CSOSNDto>> GetAll(GetAllCSOSNDto requestDto)
        {
            return _CSOSNReadRepository.GetAll(requestDto);
        }

        public Task<CSOSNDto> GetByCode(string code, GetAllCSOSNDto requestDto)
        {
            return _CSOSNReadRepository.GetByCode(code, requestDto);
        }

        public override CSOSN GeneratedEntity(CSOSNDto dto)
        {
            return _CSOSNAdapter.Map(dto).Build();
        }
    }
}