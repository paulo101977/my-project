﻿IF (SELECT COUNT(*) FROM BR_NBS) < 24
BEGIN
INSERT INTO BR_NBS (NBSID, CODIGO, DESCRICAO, ATIVO) VALUES (NEWID(),'10.301','Fornecimento de alimentação, incluindo refeições',1);
INSERT INTO BR_NBS (NBSID, CODIGO, DESCRICAO, ATIVO) VALUES (NEWID(),'1.0301.10.00','Fornecimento de refeições acompanhado de serviços de restaurante',1);
INSERT INTO BR_NBS (NBSID, CODIGO, DESCRICAO, ATIVO) VALUES (NEWID(),'1.0301.2','Fornecimento de refeições com serviços limitados de restaurante',1);
INSERT INTO BR_NBS (NBSID, CODIGO, DESCRICAO, ATIVO) VALUES (NEWID(),'1.0301.21.00','Fornecimento de refeições pelo sistema de autosserviço (self-service)',1);
INSERT INTO BR_NBS (NBSID, CODIGO, DESCRICAO, ATIVO) VALUES (NEWID(),'1.0301.22.00','Fornecimento de comidas rápidas (fast-food)',1);
INSERT INTO BR_NBS (NBSID, CODIGO, DESCRICAO, ATIVO) VALUES (NEWID(),'1.0301.29.00','Fornecimento de refeições com serviços limitados de restaurante não classificado em subposições anteriores',1);
INSERT INTO BR_NBS (NBSID, CODIGO, DESCRICAO, ATIVO) VALUES (NEWID(),'1.0301.3','Fornecimento de alimentação, incluindo refeições, sob contrato',1);
INSERT INTO BR_NBS (NBSID, CODIGO, DESCRICAO, ATIVO) VALUES (NEWID(),'1.0301.31.00','Fornecimento de alimentação para eventos',1);
INSERT INTO BR_NBS (NBSID, CODIGO, DESCRICAO, ATIVO) VALUES (NEWID(),'1.0301.32.00','Fornecimento de alimentação para operadores de transportes (comissaria ou catering)',1);
INSERT INTO BR_NBS (NBSID, CODIGO, DESCRICAO, ATIVO) VALUES (NEWID(),'1.0301.39.00','Fornecimento de alimentação, incluindo refeições, sob contrato não classificado em subposições anteriores',1);
INSERT INTO BR_NBS (NBSID, CODIGO, DESCRICAO, ATIVO) VALUES (NEWID(),'1.0301.90.00','Fornecimento de alimentação não classificado em subposições anteriores',1);
INSERT INTO BR_NBS (NBSID, CODIGO, DESCRICAO, ATIVO) VALUES (NEWID(),'1.0302.00.00','Fornecimento de bebidas em bares, cervejarias e outros',1);
INSERT INTO BR_NBS (NBSID, CODIGO, DESCRICAO, ATIVO) VALUES (NEWID(),'10.303','Serviços de hospedagem para visitantes',1);
INSERT INTO BR_NBS (NBSID, CODIGO, DESCRICAO, ATIVO) VALUES (NEWID(),'1.0303.1','Serviços de hospedagem em quartos ou unidades de hospedagem para visitantes',1);
INSERT INTO BR_NBS (NBSID, CODIGO, DESCRICAO, ATIVO) VALUES (NEWID(),'1.0303.11.00','Serviços de hospedagem em quartos ou unidades de hospedagem para visitantes, com serviços diários de faxina',1);
INSERT INTO BR_NBS (NBSID, CODIGO, DESCRICAO, ATIVO) VALUES (NEWID(),'1.0303.12.00','Serviços de hospedagem em quartos ou unidades de hospedagem para visitantes, sem serviços diários de faxina',1);
INSERT INTO BR_NBS (NBSID, CODIGO, DESCRICAO, ATIVO) VALUES (NEWID(),'1.0303.13.00','Serviços de hospedagem em quartos ou unidades de hospedagem para visitantes, em propriedades partilhadas',1);
INSERT INTO BR_NBS (NBSID, CODIGO, DESCRICAO, ATIVO) VALUES (NEWID(),'1.0303.14.00','Serviços de hospedagem em quartos ou unidades de hospedagem para visitantes, em quartos de múltipla ocupação',1);
INSERT INTO BR_NBS (NBSID, CODIGO, DESCRICAO, ATIVO) VALUES (NEWID(),'1.0303.20.00','Serviços de acampamentos turísticos (camping)',1);
INSERT INTO BR_NBS (NBSID, CODIGO, DESCRICAO, ATIVO) VALUES (NEWID(),'1.0303.90.00','Serviços de hospedagem para visitantes não classificados em subposições anteriores',1);
INSERT INTO BR_NBS (NBSID, CODIGO, DESCRICAO, ATIVO) VALUES (NEWID(),'10.304','Serviços de hospedagem, exceto para visitantes',1);
INSERT INTO BR_NBS (NBSID, CODIGO, DESCRICAO, ATIVO) VALUES (NEWID(),'1.0304.10.00','Serviços de hospedagem em quartos ou unidades de hospedagem para estudantes em residências estudantis',1);
INSERT INTO BR_NBS (NBSID, CODIGO, DESCRICAO, ATIVO) VALUES (NEWID(),'1.0304.20.00','Serviços de hospedagem em quartos ou unidades de hospedagem para trabalhadores em hotéis ou campos',1);
INSERT INTO BR_NBS (NBSID, CODIGO, DESCRICAO, ATIVO) VALUES (NEWID(),'1.0304.90.00','Serviços de hospedagem, exceto para visitantes não classificados em subposições anteriores',1);
END