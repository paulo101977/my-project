﻿IF (SELECT COUNT(*) FROM BR_TipoServico) < 4
BEGIN
INSERT INTO BR_TipoServico(TIPOSERVICOID, CODIGO, DESCRICAO, ATIVO) VALUES (NEWID(),'9','Serviços relativos a hospedagem, turismo, viagens e congêneres.',1); 
INSERT INTO BR_TipoServico(TIPOSERVICOID, CODIGO, DESCRICAO, ATIVO) VALUES (NEWID(),'9.01','Hospedagem de qualquer natureza em hotéis, apart-service condominiais, flat, apart-hotéis, hotéis residência, residence-service, suite service, hotelaria marítima, motéis, pensões e congêneres; ocupação por temporada com fornecimento de serviço',1); 
INSERT INTO BR_TipoServico(TIPOSERVICOID, CODIGO, DESCRICAO, ATIVO) VALUES (NEWID(),'9.02','Agenciamento, organização, promoção, intermediação e execução de programas de turismo, passeios, viagens, excursões, hospedagens e congêneres.',1); 
INSERT INTO BR_TipoServico(TIPOSERVICOID, CODIGO, DESCRICAO, ATIVO) VALUES (NEWID(),'9.03','Guias de turismo.',1); 
END