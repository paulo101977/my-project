﻿IF NOT EXISTS (SELECT * FROM BR_NaturezaReceita WHERE NATUREZARECEITAID = 'f1317c93-e04d-4515-8f16-778416c46ddf')
BEGIN
	INSERT INTO BR_NaturezaReceita(NATUREZARECEITAID, CODIGO, DESCRICAO, ATIVO, CSTID) VALUES ('f1317c93-e04d-4515-8f16-778416c46ddf','401','Águas Minerais Artificiais e Águas Gaseificadas Artificiais',1, 'd14906d4-6e50-479f-808a-b15726acabf3'); 
END

IF NOT EXISTS (SELECT * FROM BR_NaturezaReceita WHERE NATUREZARECEITAID = 'e6455a2b-ecf0-4dd4-afb7-7f1bfdf519de')
BEGIN
	INSERT INTO BR_NaturezaReceita(NATUREZARECEITAID, CODIGO, DESCRICAO, ATIVO, CSTID) VALUES ('e6455a2b-ecf0-4dd4-afb7-7f1bfdf519de','402','Águas Minerais Naturais, Incluídas as Naturalmente Gaseificadas',1, 'd14906d4-6e50-479f-808a-b15726acabf3'); 
END

IF NOT EXISTS (SELECT * FROM BR_NaturezaReceita WHERE NATUREZARECEITAID = '41927782-6716-4c49-8b29-67bfa8b05c39')
BEGIN
	INSERT INTO BR_NaturezaReceita(NATUREZARECEITAID, CODIGO, DESCRICAO, ATIVO, CSTID) VALUES ('41927782-6716-4c49-8b29-67bfa8b05c39','403','Refrigerantes',1, 'd14906d4-6e50-479f-808a-b15726acabf3'); 
END

IF NOT EXISTS (SELECT * FROM BR_NaturezaReceita WHERE NATUREZARECEITAID = 'ded57cff-2b37-4d94-b641-bf440de583d7')
BEGIN
	INSERT INTO BR_NaturezaReceita(NATUREZARECEITAID, CODIGO, DESCRICAO, ATIVO, CSTID) VALUES ('ded57cff-2b37-4d94-b641-bf440de583d71','404','Preparações Compostas, não Alcoólicas, para Elaboração de Bebida Refrigerante',1, 'd14906d4-6e50-479f-808a-b15726acabf3'); 
END

IF NOT EXISTS (SELECT * FROM BR_NaturezaReceita WHERE NATUREZARECEITAID = '674d3d3b-8f3b-4bb0-a080-3f96a9dd5afc')
BEGIN
	INSERT INTO BR_NaturezaReceita(NATUREZARECEITAID, CODIGO, DESCRICAO, ATIVO, CSTID) VALUES ('674d3d3b-8f3b-4bb0-a080-3f96a9dd5afc','405','Refrescos, Isotônicos e Energéticos',1, 'd14906d4-6e50-479f-808a-b15726acabf3'); 
END

IF NOT EXISTS (SELECT * FROM BR_NaturezaReceita WHERE NATUREZARECEITAID = '99887e63-dbbf-490a-9274-3ad4ecd7e131')
BEGIN
	INSERT INTO BR_NaturezaReceita(NATUREZARECEITAID, CODIGO, DESCRICAO, ATIVO, CSTID) VALUES ('99887e63-dbbf-490a-9274-3ad4ecd7e131','406','Cervejas de Malte e Cervejas Sem Álcool',1, 'd14906d4-6e50-479f-808a-b15726acabf3'); 
END

IF NOT EXISTS (SELECT * FROM BR_NaturezaReceita WHERE NATUREZARECEITAID = '40357f98-7736-4199-8b83-167bd344efe5')
BEGIN
	INSERT INTO BR_NaturezaReceita(NATUREZARECEITAID, CODIGO, DESCRICAO, ATIVO, CSTID) VALUES ('40357f98-7736-4199-8b83-167bd344efe5','918','Receita decorrente da venda de bebidas frias, classificadas nos códigos 2106.90.10 Ex02; 22.01 (exceto os Ex 01 e Ex 02 do código 2201.10.00); 22.02 (exceto os Ex 01, Ex 02 e Ex 03 do código 2202.90.00); e 22.02.90.00 Ex 03 e 22.03 da TIPI, quando auferida pela pessoa jurídica varejista, assim considerada, a pessoa jurídica cuja receita decorrente de venda de bens e serviços a consumidor final no ano-calendário imediatamente anterior ao da operação houver sido igual ou superior a 75% (setenta e cinco por cento) de sua receita total de venda de bens e serviços no mesmo período, depois de excluídos os impostos e contribuições incidentes sobre a venda.',1, 'ea94b0d0-ff9a-45b0-84ac-6ea81a0a5c0f'); 
END