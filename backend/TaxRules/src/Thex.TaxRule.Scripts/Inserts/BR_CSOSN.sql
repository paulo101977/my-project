﻿IF (SELECT COUNT(*) FROM BR_CSOSN) < 10
BEGIN
INSERT INTO BR_CSOSN(CSOSNID,CODIGO, DESCRICAO, ATIVO) VALUES (NEWID(),'101',' Tributada pelo Simples Nacional com permissão de crédito',1); 
INSERT INTO BR_CSOSN(CSOSNID,CODIGO, DESCRICAO, ATIVO) VALUES (NEWID(),'102',' Tributada pelo Simples Nacional sem permissão de crédito',1); 
INSERT INTO BR_CSOSN(CSOSNID,CODIGO, DESCRICAO, ATIVO) VALUES (NEWID(),'103',' Isenção do ICMS no Simples Nacional para faixa de receita bruta',1); 
INSERT INTO BR_CSOSN(CSOSNID,CODIGO, DESCRICAO, ATIVO) VALUES (NEWID(),'201',' Tributada pelo Simples Nacional com permissão de crédito e com cobrança do ICMS por substituição tributária',1); 
INSERT INTO BR_CSOSN(CSOSNID,CODIGO, DESCRICAO, ATIVO) VALUES (NEWID(),'202',' Tributada pelo Simples Nacional sem permissão de crédito e com cobrança do ICMS por substituição tributária',1); 
INSERT INTO BR_CSOSN(CSOSNID,CODIGO, DESCRICAO, ATIVO) VALUES (NEWID(),'203',' Isenção do ICMS no Simples Nacional para faixa de receita bruta e com cobrança do ICMS por substituição tributária',1); 
INSERT INTO BR_CSOSN(CSOSNID,CODIGO, DESCRICAO, ATIVO) VALUES (NEWID(),'300',' Imune',1);
INSERT INTO BR_CSOSN(CSOSNID,CODIGO, DESCRICAO, ATIVO) VALUES (NEWID(),'400',' Não tributada pelo Simples Nacional',1); 
INSERT INTO BR_CSOSN(CSOSNID,CODIGO, DESCRICAO, ATIVO) VALUES (NEWID(),'500',' ICMS cobrado anteriormente por substituição tributária (substituído) ou por antecipação',1); 
INSERT INTO BR_CSOSN(CSOSNID,CODIGO, DESCRICAO, ATIVO) VALUES (NEWID(),'900',' Outros',1);
END