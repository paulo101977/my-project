﻿IF (SELECT COUNT(*) FROM BR_CST) < 47
BEGIN
INSERT INTO BR_CST (CSTID, CODIGO, DESCRICAO, TIPO, ATIVO) VALUES (NEWID(),'0','Nacional',0,1);
INSERT INTO BR_CST (CSTID, CODIGO, DESCRICAO, TIPO, ATIVO) VALUES (NEWID(),'1','Estrangeira - Importação direta',0,1);
INSERT INTO BR_CST (CSTID, CODIGO, DESCRICAO, TIPO, ATIVO) VALUES (NEWID(),'2','Estrangeira - Adquirida no mercado interno',0,1);

INSERT INTO BR_CST (CSTID, CODIGO, DESCRICAO, TIPO, ATIVO) VALUES (NEWID(),'00','Tributada integralmente',1,1);
INSERT INTO BR_CST (CSTID, CODIGO, DESCRICAO, TIPO, ATIVO) VALUES (NEWID(),'10','Tributada e com cobrança do ICMS por substituição tributária',1,1);
INSERT INTO BR_CST (CSTID, CODIGO, DESCRICAO, TIPO, ATIVO) VALUES (NEWID(),'20','Com redução de base de cálculo',1,1);
INSERT INTO BR_CST (CSTID, CODIGO, DESCRICAO, TIPO, ATIVO) VALUES (NEWID(),'30','Isenta ou não tributada e com cobrança do ICMS por substituição tributária',1,1);
INSERT INTO BR_CST (CSTID, CODIGO, DESCRICAO, TIPO, ATIVO) VALUES (NEWID(),'40','Isenta',1,1);
INSERT INTO BR_CST (CSTID, CODIGO, DESCRICAO, TIPO, ATIVO) VALUES (NEWID(),'41','Não tributada',1,1);
INSERT INTO BR_CST (CSTID, CODIGO, DESCRICAO, TIPO, ATIVO) VALUES (NEWID(),'50','Suspensão',1,1);
INSERT INTO BR_CST (CSTID, CODIGO, DESCRICAO, TIPO, ATIVO) VALUES (NEWID(),'51','Diferimento',1,1);
INSERT INTO BR_CST (CSTID, CODIGO, DESCRICAO, TIPO, ATIVO) VALUES (NEWID(),'60','ICMS cobrado anteriormente por substituição tributária',1,1);
INSERT INTO BR_CST (CSTID, CODIGO, DESCRICAO, TIPO, ATIVO) VALUES (NEWID(),'70','Com redução de base de cálculo e cobrança do ICMS por substituição tributária',1,1);
INSERT INTO BR_CST (CSTID, CODIGO, DESCRICAO, TIPO, ATIVO) VALUES (NEWID(),'90','Outras',1,1);

INSERT INTO BR_CST (CSTID, CODIGO, DESCRICAO, TIPO, ATIVO) VALUES (NEWID(),'01','Operação Tributável com Alíquota Básica',2,1);
INSERT INTO BR_CST (CSTID, CODIGO, DESCRICAO, TIPO, ATIVO) VALUES (NEWID(),'02','Operação Tributável com Alíquota Diferenciada',2,1);
INSERT INTO BR_CST (CSTID, CODIGO, DESCRICAO, TIPO, ATIVO) VALUES (NEWID(),'03','Operação Tributável com Alíquota por Unidade de Medida de Produto',2,1);
INSERT INTO BR_CST (CSTID, CODIGO, DESCRICAO, TIPO, ATIVO) VALUES ('d14906d4-6e50-479f-808a-b15726acabf3','04','Operação Tributável Monofásica - Revenda a Alíquota Zero',2,1);
INSERT INTO BR_CST (CSTID, CODIGO, DESCRICAO, TIPO, ATIVO) VALUES (NEWID(),'05','Operação Tributável por Substituição Tributária',2,1);
INSERT INTO BR_CST (CSTID, CODIGO, DESCRICAO, TIPO, ATIVO) VALUES ('ea94b0d0-ff9a-45b0-84ac-6ea81a0a5c0f','06','Operação Tributável a Alíquota Zero',2,1);
INSERT INTO BR_CST (CSTID, CODIGO, DESCRICAO, TIPO, ATIVO) VALUES (NEWID(),'07','Operação Isenta da Contribuição',2,1);
INSERT INTO BR_CST (CSTID, CODIGO, DESCRICAO, TIPO, ATIVO) VALUES (NEWID(),'08','Operação sem Incidência da Contribuição',2,1);
INSERT INTO BR_CST (CSTID, CODIGO, DESCRICAO, TIPO, ATIVO) VALUES (NEWID(),'09','Operação com Suspensão da Contribuição',2,1);
INSERT INTO BR_CST (CSTID, CODIGO, DESCRICAO, TIPO, ATIVO) VALUES (NEWID(),'49','Outras Operações de Saída',2,1);
INSERT INTO BR_CST (CSTID, CODIGO, DESCRICAO, TIPO, ATIVO) VALUES (NEWID(),'50','Operação com Direito a Crédito - Vinculada Exclusivamente a Receita Tributada no Mercado Interno',2,1);
INSERT INTO BR_CST (CSTID, CODIGO, DESCRICAO, TIPO, ATIVO) VALUES (NEWID(),'51','Operação com Direito a Crédito – Vinculada Exclusivamente a Receita Não Tributada no Mercado Interno',2,1);
INSERT INTO BR_CST (CSTID, CODIGO, DESCRICAO, TIPO, ATIVO) VALUES (NEWID(),'52','Operação com Direito a Crédito - Vinculada Exclusivamente a Receita de Exportação',2,1);
INSERT INTO BR_CST (CSTID, CODIGO, DESCRICAO, TIPO, ATIVO) VALUES (NEWID(),'53','Operação com Direito a Crédito - Vinculada a Receitas Tributadas e Não-Tributadas no Mercado Interno',2,1);
INSERT INTO BR_CST (CSTID, CODIGO, DESCRICAO, TIPO, ATIVO) VALUES (NEWID(),'54','Operação com Direito a Crédito - Vinculada a Receitas Tributadas no Mercado Interno e de Exportação',2,1);
INSERT INTO BR_CST (CSTID, CODIGO, DESCRICAO, TIPO, ATIVO) VALUES (NEWID(),'55','Operação com Direito a Crédito - Vinculada a Receitas Não-Tributadas no Mercado Interno e de Exportação',2,1);
INSERT INTO BR_CST (CSTID, CODIGO, DESCRICAO, TIPO, ATIVO) VALUES (NEWID(),'56','Operação com Direito a Crédito - Vinculada a Receitas Tributadas e Não-Tributadas no Mercado Interno, e de Exportação',2,1);
INSERT INTO BR_CST (CSTID, CODIGO, DESCRICAO, TIPO, ATIVO) VALUES (NEWID(),'60','Crédito Presumido - Operação de Aquisição Vinculada Exclusivamente a Receita Tributada no Mercado Interno',2,1);
INSERT INTO BR_CST (CSTID, CODIGO, DESCRICAO, TIPO, ATIVO) VALUES (NEWID(),'61','Crédito Presumido - Operação de Aquisição Vinculada Exclusivamente a Receita Não-Tributada no Mercado Interno',2,1);
INSERT INTO BR_CST (CSTID, CODIGO, DESCRICAO, TIPO, ATIVO) VALUES (NEWID(),'62','Crédito Presumido - Operação de Aquisição Vinculada Exclusivamente a Receita de Exportação',2,1);
INSERT INTO BR_CST (CSTID, CODIGO, DESCRICAO, TIPO, ATIVO) VALUES (NEWID(),'63','Crédito Presumido - Operação de Aquisição Vinculada a Receitas Tributadas e Não-Tributadas no Mercado Interno',2,1);
INSERT INTO BR_CST (CSTID, CODIGO, DESCRICAO, TIPO, ATIVO) VALUES (NEWID(),'64','Crédito Presumido - Operação de Aquisição Vinculada a Receitas Tributadas no Mercado Interno e de Exportação',2,1);
INSERT INTO BR_CST (CSTID, CODIGO, DESCRICAO, TIPO, ATIVO) VALUES (NEWID(),'65','Crédito Presumido - Operação de Aquisição Vinculada a Receitas Não-Tributadas no Mercado Interno e de Exportação',2,1);
INSERT INTO BR_CST (CSTID, CODIGO, DESCRICAO, TIPO, ATIVO) VALUES (NEWID(),'66','Crédito Presumido - Operação de Aquisição Vinculada a Receitas Tributadas e Não-Tributadas no Mercado Interno, e de Exportação',2,1);
INSERT INTO BR_CST (CSTID, CODIGO, DESCRICAO, TIPO, ATIVO) VALUES (NEWID(),'67','Crédito Presumido - Outras Operações',2,1);
INSERT INTO BR_CST (CSTID, CODIGO, DESCRICAO, TIPO, ATIVO) VALUES (NEWID(),'70','Operação de Aquisição sem Direito a Crédito',2,1);
INSERT INTO BR_CST (CSTID, CODIGO, DESCRICAO, TIPO, ATIVO) VALUES (NEWID(),'71','Operação de Aquisição com Isenção',2,1);
INSERT INTO BR_CST (CSTID, CODIGO, DESCRICAO, TIPO, ATIVO) VALUES (NEWID(),'72','Operação de Aquisição com Suspensão',2,1);
INSERT INTO BR_CST (CSTID, CODIGO, DESCRICAO, TIPO, ATIVO) VALUES (NEWID(),'73','Operação de Aquisição a Alíquota Zero',2,1);
INSERT INTO BR_CST (CSTID, CODIGO, DESCRICAO, TIPO, ATIVO) VALUES (NEWID(),'74','Operação de Aquisição sem Incidência da Contribuição',2,1);
INSERT INTO BR_CST (CSTID, CODIGO, DESCRICAO, TIPO, ATIVO) VALUES (NEWID(),'75','Operação de Aquisição por Substituição Tributária',2,1);
INSERT INTO BR_CST (CSTID, CODIGO, DESCRICAO, TIPO, ATIVO) VALUES (NEWID(),'98','Outras Operações de Entrada',2,1);
INSERT INTO BR_CST (CSTID, CODIGO, DESCRICAO, TIPO, ATIVO) VALUES (NEWID(),'99','Outras Operações',2,1);
END