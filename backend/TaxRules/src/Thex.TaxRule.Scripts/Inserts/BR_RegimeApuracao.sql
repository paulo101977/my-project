﻿IF (SELECT COUNT(*) FROM BR_RegimeApuracao) < 3
BEGIN
INSERT INTO BR_RegimeApuracao(RegimeApuracaoId, DESCRICAO, ATIVO) VALUES (NEWID(),'Cumulativo', 1); 
INSERT INTO BR_RegimeApuracao(RegimeApuracaoId, DESCRICAO, ATIVO) VALUES (NEWID(),'Não cumulativo', 1); 
INSERT INTO BR_RegimeApuracao(RegimeApuracaoId, DESCRICAO, ATIVO) VALUES (NEWID(),'Não apurar', 1); 
END