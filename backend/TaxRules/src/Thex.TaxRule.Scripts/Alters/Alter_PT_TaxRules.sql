﻿IF NOT EXISTS(SELECT 1 FROM sys.columns 
          WHERE Name = N'ExemptionCode'
          AND Object_ID = Object_ID(N'dbo.PT_TaxRules'))
BEGIN
	ALTER TABLE PT_TaxRules
	ADD ExemptionCode NVARCHAR(10) NULL
END