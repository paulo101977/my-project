IF NOT EXISTS(SELECT 1 FROM sys.columns 
          WHERE Name = N'PropertyUId'
          AND Object_ID = Object_ID(N'dbo.BR_ImpostoRetido'))
BEGIN
	ALTER TABLE BR_ImpostoRetido
	ADD PropertyUId UNIQUEIDENTIFIER NULL
END

GO

IF (OBJECT_ID('UK_BR_ImpostoRetido') IS NULL)
BEGIN
	ALTER TABLE BR_ImpostoRetido
	ADD CONSTRAINT [UK_BR_ImpostoRetido] UNIQUE ( [OwnerId],[PropertyUId] )
END

GO