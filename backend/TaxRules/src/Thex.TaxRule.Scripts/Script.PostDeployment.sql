﻿/*
Post-Deployment Script Template							
--------------------------------------------------------------------------------------
 This file contains SQL statements that will be appended to the build script.		
 Use SQLCMD syntax to include a file in the post-deployment script.			
 Example:      :r .\myfile.sql								
 Use SQLCMD syntax to reference a variable in the post-deployment script.		
 Example:      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/
:r .\Alters\Alter_BR_ImpostoRetido.sql
:r .\Inserts\BR_CFOP.sql
:r .\Inserts\BR_CSOSN.sql
:r .\Inserts\BR_CST.sql
:r .\Inserts\BR_NBS.sql
:r .\Inserts\BR_NCM.sql
:r .\Inserts\BR_TipoServico.sql
:r .\Inserts\BR_NaturezaReceita.sql
:r .\Inserts\BR_RegimeApuracao.sql
:r .\Inserts\BR_UF.sql
:r .\Alters\AlterTaxRules.sql
:r .\Inserts\PT_IVA.sql
:r .\Alters\Alter_PT_TaxRules.sql
