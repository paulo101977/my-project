﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Thex.TaxRule.Domain.Enumerations
{
    public class TaxRuleEnum
    {
        public enum Error
        {
            InvalidyTwoLetterIsoCodeCountry = 1,
            InvalidyTwoLetterIsoCodeState = 2,
            InvalidyCountry = 3,
            VerifyParams = 4,
            TributeNotFound = 5,
            InvoiceRequiredParams = 6,
            CancelInvoiceError = 7,
            InvalidInvoiceCFOP = 8,
            InvoicePartnerError = 9
        }

        public enum Message
        {
            UpdateInvoice = 1,
            SendInvoice = 2
        }
    }
}
