﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a Crudzilla tool.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using Tnf.Notifications;

namespace Thex.TaxRule.Domain.Entities
{
    public partial class NaturezaReceita : IEntityGuid
    {
        public Guid Id { get; set; }
        public string Code { get; internal set; }
        public string Description { get; internal set; }
        public Guid NcmId { get; internal set; }
        public bool IsActive { get; set; }

        public enum EntityError
        {
            NaturezaReceitaMustHaveDescription,
            NaturezaReceitaOutOfBoundDescription,
            NaturezaReceitaMustHaveNcmId
        }

        public static Builder Create(INotificationHandler handler)
            => new Builder(handler);

        public static Builder Create(INotificationHandler handler, NaturezaReceita instance)
            => new Builder(handler, instance);
    }
}
