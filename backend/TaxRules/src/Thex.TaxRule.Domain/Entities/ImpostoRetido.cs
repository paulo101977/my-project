﻿using System;
using Tnf.Notifications;

namespace Thex.TaxRule.Domain.Entities
{
    public partial class ImpostoRetido : IEntityGuid
    {
        public Guid Id { get; set; }
        public Guid OwnerId { get; internal set; }
        public Guid? PropertyUId { get; internal set; }

        public enum EntityError
        {
            NaturezaReceitaMustHaveDescription,
            NaturezaReceitaOutOfBoundDescription,
            NaturezaReceitaMustHaveNcmId
        }

        public static Builder Create(INotificationHandler handler)
            => new Builder(handler);

        public static Builder Create(INotificationHandler handler, ImpostoRetido instance)
            => new Builder(handler, instance);
    }
}