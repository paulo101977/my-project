﻿using System;

namespace Thex.TaxRule.Domain.Entities
{
    public class BaseEntity : ThexMultiTenantFullAuditedEntity, IEntityGuid
    {
        public Guid Id { get; set; }
        public bool IsActive { get; set; }
    }
}
