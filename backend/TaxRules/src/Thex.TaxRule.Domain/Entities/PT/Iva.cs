using System;

namespace Thex.TaxRule.Domain.Entities
{
    public class Iva : BaseEntity
    {
        public string Description { get; set; }
        public string Location { get; set; }
        public decimal? Tax { get; set; }
    }
}
