﻿using System;

namespace Thex.TaxRule.Domain.Entities
{
    public partial class PTTaxRule : ThexFullAuditedEntity, IEntityGuid
    {
        public Guid Id { get; set; }
        public bool IsActive { get; set; }
        public int? ServiceId { get; set; }
        public Guid? ProductId { get; set; }
        public Guid IvaId { get; set; }
        public Guid PropertyId { get; set; }
        public string Description { get; set; }
        public Guid? TenantId { get; set; }
        public string ExemptionCode { get; set; }

        public enum EntityError
        {
            InvalidAssociationInIvaAndSerivesAndProducts,
            IvaZeroWithoutExemptionCode
        }
    }
}
