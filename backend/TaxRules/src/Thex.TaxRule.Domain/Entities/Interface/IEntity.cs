﻿using System;

namespace Thex.TaxRule.Domain.Entities
{
    public interface IEntityGuid : IEntity
    {
        Guid Id { get; set; }
    }

    public interface IEntityString : IEntity
    {
        string Id { get; set; }
    }

    public interface IEntityInt : IEntity
    {
        int Id { get; set; }
    }

    public interface IEntityLong : IEntity
    {
        long Id { get; set; }
    }

    public interface IEntityShort : IEntity
    {
        short Id { get; set; }
    }


    public interface IEntity
    {
    }
}
