﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a Crudzilla tool.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using Tnf.Notifications;

namespace Thex.TaxRule.Domain.Entities
{
    public partial class TaxRulePT : ThexFullAuditedEntity, IEntityGuid
    {
        public Guid Id { get; set; }
        public Guid? PropertyUId { get; set; }
        public Guid? TenantId { get; set; }
        public Guid? ProductId { get; set; }
        public int? ServiceId { get; set; }
        public Guid? IvaId { get; set; }
        public bool IsActive { get; set; }
        public enum EntityError
        {
            TaxRulesMustHaveNome,
            TaxRulesOutOfBoundNome,
            TaxRulesOutOfBoundBarCode,
            TaxRulesMustHaveCfopId,
            TaxRulesMustHaveCstPisCofinsId,
            TaxRulesMustHaveNaturezaReceitaId,
            TaxRulesMustHaveTaxCalculationId,
            TaxRulesMustHaveServiceTypeId
        }

        public static Builder Create(INotificationHandler handler)
            => new Builder(handler);

        public static Builder Create(INotificationHandler handler, TaxRulePT instance)
            => new Builder(handler, instance);
    }
}
