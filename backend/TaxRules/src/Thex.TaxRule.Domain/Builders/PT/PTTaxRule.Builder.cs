﻿using System;
using Tnf.Builder;
using Tnf.Notifications;

namespace Thex.TaxRule.Domain.Entities
{
    public partial class PTTaxRule
    {
        public class Builder : Builder<PTTaxRule>
        {
            public Builder(INotificationHandler handler) : base(handler)
            {
            }

            public Builder(INotificationHandler handler, PTTaxRule instance) : base(handler, instance)
            {
            }

            public virtual Builder WithId(Guid id)
            {
                Instance.Id = id;
                return this;
            }

            public virtual Builder WithServiceId(int serviceId)
            {
                Instance.ServiceId = serviceId;
                return this;
            }

            public virtual Builder WithProductId(Guid productId)
            {
                Instance.ProductId = productId;
                return this;
            }

            public virtual Builder WithIvaId(Guid ivaId)
            {
                Instance.IvaId = ivaId;
                return this;
            }

            public virtual Builder WithPropertyId(Guid propertyId)
            {
                Instance.PropertyId = propertyId;
                return this;
            }

            public virtual Builder WithDescription(string description)
            {
                Instance.Description = description;
                return this;
            }

            public virtual Builder WithExemptionCode(string exemptionCode)
            {
                Instance.ExemptionCode = exemptionCode;
                return this;
            }
        }
    }
}
