﻿using System;
using Tnf.Repositories.Entities.Auditing;

namespace Thex.TaxRule.Domain.Entities
{
    [Serializable]
    public abstract class ThexMultiTenantFullAuditedEntity : ThexFullAuditedEntity, IMustHaveThexTenant
    {
        public Guid? TenantId { get; set; }
        public int? PropertyId { get; set; }
        public int? ChainId { get; set; }
    }
}
