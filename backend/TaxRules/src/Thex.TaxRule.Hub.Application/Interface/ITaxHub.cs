﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.Common.Dto.InvoiceIntegration;
using Thex.Dto.Integration;
using Thex.TaxRule.Dto;
using Thex.TaxRule.Dto.Dto.Integration.Response;
using Thex.TaxRule.Dto.Dto.PT.InvoiceXpressIntegration.Proforma;
using Tnf.Dto;

namespace Thex.TaxRule.Hub.Application.Interface
{
    public interface ITaxHub
    {
        /// <summary>
        /// Envia um registro de venda 
        /// </summary>
        /// <param name="invoiceListDto">Lista de invoices</param>
        /// <returns></returns>
        Task<InvoiceResponseDto> Send(List<InvoiceIntegrationDto> invoiceListDto);
        Task<bool> UpdateInvoiceResponse(InvoiceIntegrationResponseDto invoiceResponse, int invoiceType);

        Task<TaxRulesDto> Create(TaxRulesDto dto);
        Task<IListDto<TaxRulesDto>> GetAll(int type, GetAllTaxRulesDto requestDto);
        Task<TaxRulesDto> Get(Guid id);
        Task Toggle(Guid id);
        Task<TaxRulesDto> GetTribute(Guid propertyUUId, GetAllTaxRulesWithParamsDto requestDto);
        Task<InvoiceCancelResponseDto> Cancel(InvoiceIntegrationDto invoiceDto, int invoiceType);
        Task<string> GetIntegratorId(Guid id);
        Task<ProformaResponseDto> SendProforma(ProformaDto proformaDto);
    }
}
