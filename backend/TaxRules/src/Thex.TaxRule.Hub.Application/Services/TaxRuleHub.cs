using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.Common;
using Thex.Common.Dto.InvoiceIntegration;
using Thex.Common.Enumerations;
using Thex.Common.Helpers;
using Thex.Dto.Integration;
using Thex.Kernel;
using Thex.TaxRule.Application.Brasil.Interfaces;
using Thex.TaxRule.ApplicationEurope.Interfaces;
using Thex.TaxRule.Domain.Enumerations;
using Thex.TaxRule.Dto;
using Thex.TaxRule.Dto.Dto.Integration.Response;
using Thex.TaxRule.Dto.Dto.PT.InvoiceXpressIntegration.Proforma;
using Thex.TaxRule.Hub.Application.Interface;
using Thex.TaxRule.Infra.Interfaces;
using Thex.TaxRule.Infra.Interfaces.ReadRepositories;
using Thex.TaxRule.Infra.Repositories;
using Tnf.Dto;
using Tnf.Notifications;

namespace Thex.TaxRule.Hub.Application
{
    public class TaxRuleHub : ITaxHub
    {
        private INotificationHandler _notificationHandler;
        private ITaxRulesAppServiceBrazil _taxRulesAppServiceBrazil;
        private ITaxRulesAppServiceEurope _taxRulesAppServiceEurope;
        private ITaxRulesReadRepository _taxRulesReadRepository;
        private ITaxRulesRepository _taxRulesRepository;
        private IConfiguration _configuration;
        private IApplicationUser _applicationUser;
        private IServiceInvoiceRepository _invoiceRepository;

        public TaxRuleHub(INotificationHandler notificationHandler,
                          ITaxRulesAppServiceBrazil taxRulesAppServiceBrazil,
                          ITaxRulesReadRepository taxRulesReadRepository,
                          ITaxRulesRepository taxRulesRepository,
                          IConfiguration configuration,
                          IApplicationUser applicationUser,
                          IServiceInvoiceRepository invoiceRepository,
                          ITaxRulesAppServiceEurope taxRulesAppServiceEurope)
        {
            _configuration = configuration;
            _notificationHandler = notificationHandler;
            _taxRulesAppServiceBrazil = taxRulesAppServiceBrazil;
            _taxRulesReadRepository = taxRulesReadRepository;
            _applicationUser = applicationUser;
            _invoiceRepository = invoiceRepository;
            _taxRulesAppServiceEurope = taxRulesAppServiceEurope;
        }

        /// <summary>
        /// Valida se o código do país foi informado e se é um país atendido pela aplicação
        /// </summary>
        private void ValidateIsoCodeCountry()
        {
            // Valida se o código está preenchido
            if (string.IsNullOrEmpty(_applicationUser.PropertyCountryCode))
            {
                _notificationHandler.Raise(_notificationHandler.DefaultBuilder
                                           .WithMessage(AppConsts.LocalizationSourceName, TaxRuleEnum.Error.InvalidyTwoLetterIsoCodeCountry)
                                           .WithDetailedMessage(AppConsts.LocalizationSourceName, TaxRuleEnum.Error.InvalidyTwoLetterIsoCodeCountry)
                                           .Build());
                return;
            }

            // Valida se faz parte de uma região atendida
            switch (EnumHelper.GetEnumValue<CountryIsoCodeEnum>(_applicationUser.PropertyCountryCode, true))
            {
                case CountryIsoCodeEnum.Europe:
                case CountryIsoCodeEnum.Brasil: break;
                case CountryIsoCodeEnum.Latam:
                default:
                    {
                        _notificationHandler.Raise(_notificationHandler.DefaultBuilder
                                                                 .WithMessage(AppConsts.LocalizationSourceName, TaxRuleEnum.Error.InvalidyCountry)
                                                                 .WithDetailedMessage(string.Concat(_applicationUser.PropertyCountryCode, AppConsts.LocalizationSourceName, TaxRuleEnum.Error.InvalidyCountry))
                                                                 .Build());

                        break;
                    }
            }
        }

        /// <summary>
        /// Cria uma nova regra tributária
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public async Task<TaxRulesDto> Create(TaxRulesDto dto)
        {
            ValidateIsoCodeCountry();

            if (_notificationHandler.HasNotification())
                return await Task.FromResult(TaxRulesDto.NullInstance);

            switch (EnumHelper.GetEnumValue<CountryIsoCodeEnum>(_applicationUser.PropertyCountryCode, true))
            {
                case CountryIsoCodeEnum.Brasil:
                    {
                        return await _taxRulesAppServiceBrazil.Create(dto);
                    }
                default:
                    return await Task.FromResult(TaxRulesDto.NullInstance);
            }
        }

        /// <summary>
        /// Retorna todos as regras tributárias cadastradas
        /// </summary>
        /// <param name="type">0 - Produto | 1 - Serviço</param>
        /// <param name="requestDto"></param>
        /// <returns></returns>
        public async Task<IListDto<TaxRulesDto>> GetAll(int type, GetAllTaxRulesDto requestDto)
        {
            IListDto<TaxRulesDto> tributesList = null;

            ValidateIsoCodeCountry();

            if (_notificationHandler.HasNotification())
                return await Task.FromResult(tributesList);

            switch (EnumHelper.GetEnumValue<CountryIsoCodeEnum>(_applicationUser.PropertyCountryCode, true))
            {
                case CountryIsoCodeEnum.Brasil:
                    {
                        return await _taxRulesAppServiceBrazil.GetAll(type, requestDto);
                    }
                default:
                    return await Task.FromResult(tributesList);
            }
        }

        /// <summary>
        /// Retorna uma regra tributária pelo identificador
        /// </summary>
        /// <param name="id">Identificador da regra tributária</param>
        /// <returns></returns>
        public async Task<TaxRulesDto> Get(Guid id)
        {
            return await _taxRulesReadRepository.GetById(id);
        }

        /// <summary>
        /// Ativa ou desativa uma regra tributária
        /// </summary>
        /// <param name="id">Identificador da regra tributária</param>
        /// <returns></returns>
        public async Task Toggle(Guid id)
        {
            await _taxRulesRepository.ToggleAsync(id);
        }

        /// <summary>
        /// Retorna um tribute para o produto ou serviço
        /// </summary>
        /// <param name="id">Identificador da regra tributária</param>
        /// <param name="propertyId">Identificador da property</param>
        /// <param name="requestDto"></param>
        /// <returns></returns>
        public async Task<TaxRulesDto> GetTribute(Guid propertyUUId, GetAllTaxRulesWithParamsDto requestDto)
        {
            ValidateIsoCodeCountry();

            if (_notificationHandler.HasNotification())
                return await Task.FromResult(TaxRulesDto.NullInstance);

            switch (EnumHelper.GetEnumValue<CountryIsoCodeEnum>(_applicationUser.PropertyCountryCode, true))
            {
                case CountryIsoCodeEnum.Brasil:
                    {
                        return await _taxRulesAppServiceBrazil.GetTribute(propertyUUId, requestDto);
                    }
                default:
                    return await Task.FromResult(TaxRulesDto.NullInstance);
            }
        }

        /// <summary>
        /// Envia um registro de venda
        /// </summary>
        /// <param name="invoiceListDto">Lista de invoices</param>
        /// <returns></returns>
        public async Task<InvoiceResponseDto> Send(List<InvoiceIntegrationDto> invoiceListDto)
        {
            ValidateIsoCodeCountry();

            if (_notificationHandler.HasNotification())
                return InvoiceResponseDto.NullInstance;

            switch (EnumHelper.GetEnumValue<CountryIsoCodeEnum>(_applicationUser.PropertyCountryCode, true))
            {
                case CountryIsoCodeEnum.Brasil:
                    {
                        return await _taxRulesAppServiceBrazil.Send(invoiceListDto);
                    }
                case CountryIsoCodeEnum.Europe:
                    {
                        return await _taxRulesAppServiceEurope.Send(invoiceListDto);
                    }
                default:
                    return InvoiceResponseDto.NullInstance;
            }
        }

        public async Task<bool> UpdateInvoiceResponse(InvoiceIntegrationResponseDto invoiceResponse, int invoiceType)
        {
            switch (EnumHelper.GetEnumValue<BillingInvoiceTypeEnum>(invoiceType.ToString()))
            {
                case BillingInvoiceTypeEnum.NFSE:
                case BillingInvoiceTypeEnum.NFCE:
                case BillingInvoiceTypeEnum.SAT:
                    {
                        await _taxRulesAppServiceBrazil.UpdateInvoiceResponse(invoiceResponse, invoiceType);
                        break;
                    }
                case BillingInvoiceTypeEnum.Factura:
                    {
                        await _taxRulesAppServiceEurope.UpdateInvoiceResponse(invoiceResponse, invoiceType);
                        break;
                    }
                default:
                    break;
            }

            if (_notificationHandler.HasNotification())
                return false;

            return true;
        }

        public async Task<InvoiceCancelResponseDto> Cancel(InvoiceIntegrationDto invoiceDto, int invoiceType)
        {
            ValidateIsoCodeCountry();

            if (_notificationHandler.HasNotification())
                return new InvoiceCancelResponseDto(false);

            switch (EnumHelper.GetEnumValue<CountryIsoCodeEnum>(_applicationUser.PropertyCountryCode, true))
            {
                case CountryIsoCodeEnum.Brasil:
                    return await _taxRulesAppServiceBrazil.Cancel(invoiceDto, invoiceType);
                default:
                    return new InvoiceCancelResponseDto(false);
            }
        }

        public async Task<string> GetIntegratorId(Guid id)
        {
            ValidateIsoCodeCountry();

            if (_notificationHandler.HasNotification())
                return await Task.FromResult(string.Empty);

             switch (EnumHelper.GetEnumValue<CountryIsoCodeEnum>(_applicationUser.PropertyCountryCode, true))
            {
                case CountryIsoCodeEnum.Europe:
                    return await _taxRulesAppServiceEurope.GetIntegratorIdById(id);
                default:
                    return await Task.FromResult(string.Empty);
            }
        }

        public async Task<ProformaResponseDto> SendProforma(ProformaDto proformaDto)
        {
            ValidateIsoCodeCountry();

            if (_notificationHandler.HasNotification())
                return ProformaResponseDto.NullInstance;

            switch (EnumHelper.GetEnumValue<CountryIsoCodeEnum>(_applicationUser.PropertyCountryCode, true))
            {
                case CountryIsoCodeEnum.Europe:
                    {
                        return await _taxRulesAppServiceEurope.SendProforma(proformaDto);
                    }
                default:
                    return ProformaResponseDto.NullInstance;
            }
        }
    }
}
