﻿using hex.TaxRule.Application.Brazil.Adapters;
using Microsoft.Extensions.DependencyInjection;
using Thex.TaxRule.Application.Brasil.Interfaces;
using Thex.TaxRule.Application.Brasil.Services;
using Thex.TaxRule.Application.Brazil.Adapters;
using Thex.TaxRule.ApplicationEurope.Adapters;
using Thex.TaxRule.ApplicationEurope.Interfaces;
using Thex.TaxRule.ApplicationEurope.Services;
using Thex.TaxRule.Domain;
using Thex.TaxRule.Hub.Application;
using Thex.TaxRule.Hub.Application.Interface;
using Thex.TaxRule.Infra.Infra;

namespace Thex.TaxRule.Application
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddApplicationServiceDependency(this IServiceCollection services)
        {
            // Dependencia do projeto Thex.Domain
            services
                .AddDomainDependency()
                .AddInfraDependency()
                .AddTnfDefaultConventionalRegistrations();

            // Registro dos serviços
            services.AddScoped<ITaxHub, TaxRuleHub>();
            services.AddScoped<ITaxRulesAppServiceBrazil, TaxRulesAppServiceBrazil>();

            services.AddScoped<ICFOPAppService, CFOPAppService>();
            services.AddScoped<ICFOPAdapter, CFOPAdapter>();

            services.AddScoped<ICSOSNAdapter, CSOSNAdapter>();
            services.AddScoped<ICSOSNAppService, CSOSNAppService>();

            services.AddScoped<ICSTAdapter, CSTAdapter>();
            services.AddScoped<ICSTAppService, CSTAppService>();

            services.AddScoped<INaturezaReceitaAdapter, NaturezaReceitaAdapter>();
            services.AddScoped<INaturezaReceitaAppService, NaturezaReceitaAppService>();

            services.AddScoped<INBSAdapter, NBSAdapter>();
            services.AddScoped<INBSAppService, NBSAppService>();

            services.AddScoped<INCMAdapter, NCMAdapter>();
            services.AddScoped<INCMAppService, NCMAppService>();

            services.AddScoped<IRegimeApuracaoAdapter, RegimeApuracaoAdapter>();
            services.AddScoped<IRegimeApuracaoAppService, RegimeApuracaoAppService>();

            services.AddScoped<ITipoServicoAdapter, TipoServicoAdapter>();
            services.AddScoped<ITipoServicoAppService, TipoServicoAppService>();

            services.AddScoped<IUFAdapter, UFAdapter>();
            services.AddScoped<IUFAppService, UFAppService>();

            services.AddScoped<ITaxRulesAdapter,TaxRulesAdapter>();
            services.AddScoped<IPTTaxRuleAdapter, PTTaxRulesAdapter>();

            services.AddScoped<IImpostoRetidoAdapter, ImpostoRetidoAdapter>();
            services.AddScoped<IImpostoRetidoAppService, ImpostoRetidoAppService>();

            services.AddScoped<IIvaAppService, IvaAppService>();

            services.AddScoped<IPTTaxRuleAppService, PTTaxRuleAppService>();

            return services;
        }
    }
}