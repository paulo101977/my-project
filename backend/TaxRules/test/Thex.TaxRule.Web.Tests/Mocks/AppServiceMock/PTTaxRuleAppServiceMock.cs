﻿using System;
using Thex.TaxRule.Dto;

namespace Thex.TaxRule.Web.Tests.Mocks
{
    public  class PTTaxRuleAppServiceMock
    {
        public static IvaDto GetIva()
        {
            var result = new IvaDto
            {
                Id = Guid.NewGuid(),
                Description = "IVA 6",
                Tax = 6
            };

            return result;
        }

        public static IvaDto GetIvaZero()
        {
            var result = new IvaDto
            {
                Id = Guid.NewGuid(),
                Description = "Insento",
                Tax = null
            };

            return result;
        }
    }
}
