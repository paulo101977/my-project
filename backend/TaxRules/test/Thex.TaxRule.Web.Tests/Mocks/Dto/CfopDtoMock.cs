﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Thex.TaxRule.Dto;
using Tnf.Dto;

namespace Thex.TaxRule.Web.Tests.Mocks.Dto
{
    public static class CfopDtoMock
    {
        public static CFOPDto GetDto(string description = "Cfop teste")
        {
            return new CFOPDto()
            {
                Code = "909090",
                Description = description,
                Id = Guid.NewGuid(),
                isActive = true
            };
        }

        public static Task<IListDto<CFOPDto>> GetListDtoAsync()
        {
            var ret = new ListDto<CFOPDto>();

            ret.Items.Add(CfopDtoMock.GetDto("Cfop 1"));
            ret.Items.Add(CfopDtoMock.GetDto("Cfop 2"));
            ret.Items.Add(CfopDtoMock.GetDto("Cfop 3"));

            return Task.FromResult<IListDto<CFOPDto>>(ret);
        }
    }
}
