﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Thex.TaxRule.Dto;
using Tnf.Dto;

namespace Thex.TaxRule.Web.Tests.Mocks.Dto
{
    public static class CsosnDtoMock
    {
        public static CSOSNDto GetDto(string description = "Csosn teste")
        {
            return new CSOSNDto()
            {
                Code = "909090",
                Description = description,
                Id = Guid.NewGuid(),
                isActive = true
            };
        }

        public static Task<IListDto<CSOSNDto>> GetListDtoAsync()
        {
            var ret = new ListDto<CSOSNDto>();

            ret.Items.Add(CsosnDtoMock.GetDto("Csosn 1"));
            ret.Items.Add(CsosnDtoMock.GetDto("Csosn 2"));
            ret.Items.Add(CsosnDtoMock.GetDto("Csosn 3"));

            return Task.FromResult<IListDto<CSOSNDto>>(ret);
        }
    }
}
