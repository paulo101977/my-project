﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.TaxRule.Dto;
using Tnf.Dto;

namespace Thex.TaxRules.Web.Tests.Mocks.Dto
{
    public static class TaxRulesDtoMock
    {
        public static TaxRulesDto GetDto(string name = "Teste")
        {
            return new TaxRulesDto()
            {
                Id = Guid.Parse("7d220627-853d-466f-835f-a06b6d6b1922"),
                BarCode = "789999123456",
                Cfop = "5101",
                CfopId = Guid.Parse("2823C956-0602-4A78-A41C-C6425A66C772"),
                Csosn = "500",
                CsosnId = Guid.Parse("981C4FD8-A18E-44ED-8E7E-C6F74174CE11"),
                CstA = "300",
                CstAid = Guid.Parse("A492529B-E638-476F-98AD-58D0297B738E"),
                CstB = "800",
                CstBid = Guid.Parse("CBA5DDCE-F6A5-416F-8A52-98C4516CB291"),
                CstPisCofins = "10",
                CstPisCofinsId = Guid.Parse("7BABC7CF-8264-4E49-8A07-5F494CE4718F"),
                isActive = true,
                Name = name,
                Nature = "412",
                NatureId = Guid.Parse("2CA6AD59-9105-4764-B726-56691BC393D3"),
                NbsCode = "1010241",
                NbsId = Guid.Parse("79D8CC59-031A-49BD-BBF4-6BA3C500ECCC"),
                NcmCode = "27101159",
                NcmId = Guid.Parse("40E098A2-4EA5-436E-ABF7-9593B587B3AA"),
                PropertyId = Guid.NewGuid(),
                PropertyList = new List<PropertyDto>(),
                ServiceType = "201",
                ServiceTypeId = Guid.Parse("04B39150-17D6-459B-A05B-17888E9A54C2"),
                TaxCalculation = "Não cumulativo",
                TaxCalculationId = Guid.Parse("2632ED17-487A-41E2-B0A4-8ADF6F9483BC"),
                TaxPercentual = 10.00m,
                TaxPercentualBase = 0m,
                TaxPercentualSub = 0m,
                TenantId = Guid.Parse("85E0E55C-549A-473C-B3CB-864E4BB61686"),
                Type = 0,
                UfId = Guid.Parse("6A3FFF92-EAE3-4F51-9456-939C9FD81881"),
                UfInitials = "RJ"
            };
        }

        public static Task<TaxRulesDto> GetDtoAsync(string name = "Teste")
        {
            var dto = new TaxRulesDto()
            {
                Id = Guid.Parse("7d220627-853d-466f-835f-a06b6d6b1922"),
                BarCode = "789999123456",
                Cfop = "5101",
                CfopId = Guid.Parse("2823C956-0602-4A78-A41C-C6425A66C772"),
                Csosn = "500",
                CsosnId = Guid.Parse("981C4FD8-A18E-44ED-8E7E-C6F74174CE11"),
                CstA = "300",
                CstAid = Guid.Parse("A492529B-E638-476F-98AD-58D0297B738E"),
                CstB = "800",
                CstBid = Guid.Parse("CBA5DDCE-F6A5-416F-8A52-98C4516CB291"),
                CstPisCofins = "10",
                CstPisCofinsId = Guid.Parse("7BABC7CF-8264-4E49-8A07-5F494CE4718F"),
                isActive = true,
                Name = name,
                Nature = "412",
                NatureId = Guid.Parse("2CA6AD59-9105-4764-B726-56691BC393D3"),
                NbsCode = "1010241",
                NbsId = Guid.Parse("79D8CC59-031A-49BD-BBF4-6BA3C500ECCC"),
                NcmCode = "27101159",
                NcmId = Guid.Parse("40E098A2-4EA5-436E-ABF7-9593B587B3AA"),
                PropertyId = Guid.NewGuid(),
                PropertyList = new List<PropertyDto>(),
                ServiceType = "201",
                ServiceTypeId = Guid.Parse("04B39150-17D6-459B-A05B-17888E9A54C2"),
                TaxCalculation = "Não cumulativo",
                TaxCalculationId = Guid.Parse("2632ED17-487A-41E2-B0A4-8ADF6F9483BC"),
                TaxPercentual = 10.00m,
                TaxPercentualBase = 0m,
                TaxPercentualSub = 0m,
                TenantId = Guid.Parse("85E0E55C-549A-473C-B3CB-864E4BB61686"),
                Type = 0,
                UfId = Guid.Parse("6A3FFF92-EAE3-4F51-9456-939C9FD81881"),
                UfInitials = "RJ"
            };

            return Task.FromResult(dto);
        }

        public static Task<IListDto<TaxRulesDto>> GetListDtoAsync()
        {
            var ret = new ListDto<TaxRulesDto>();

            ret.Items.Add(TaxRulesDtoMock.GetDto("Teste 1"));
            ret.Items.Add(TaxRulesDtoMock.GetDto("Teste 2"));
            ret.Items.Add(TaxRulesDtoMock.GetDto("Teste 3"));

            return Task.FromResult<IListDto<TaxRulesDto>>(ret);
        }
    }
}
