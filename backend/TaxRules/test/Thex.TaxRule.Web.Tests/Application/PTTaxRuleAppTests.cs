﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using NSubstitute;
using Shouldly;
using System;
using System.Collections.Generic;
using Thex.TaxRule.ApplicationEurope.Adapters;
using Thex.TaxRule.ApplicationEurope.Interfaces;
using Thex.TaxRule.ApplicationEurope.Services;
using Thex.TaxRule.Domain.Entities;
using Thex.TaxRule.Dto;
using Thex.TaxRule.Infra.Interfaces;
using Thex.TaxRule.Infra.Interfaces.ReadRepositories;
using Thex.TaxRule.Web.Tests.Mocks;
using Tnf.AspNetCore.TestBase;
using Tnf.Notifications;
using Tnf.Repositories.Uow;
using Xunit;

namespace Thex.TaxRule.Web.Tests.Application
{
    public class PTTaxRuleAppTests : TnfAspNetCoreIntegratedTestBase<StartupUnitTest>
    {
        private INotificationHandler _notificationHandler;
        private IPTTaxRuleAdapter _pTTaxRulesAdapter;
        private IPTTaxRuleReadRepository _pTTaxRuleReadRepository;
        private IPTTaxRuleRepository _pTTaxRuleRepository;
        private IHttpContextAccessor _httpContextAccessor;
        private IConfiguration _configuration;
        private IIvaAppService _ivaAppService;
        private IUnitOfWorkManager _unitOfWorkManager;
        private IIvaReadRepository _ivaReadRepository;

        public PTTaxRuleAppTests()
        {
            _notificationHandler = new NotificationHandler(ServiceProvider);
            LoadDependencies();
        }

        private void LoadDependencies()
        {
            _pTTaxRulesAdapter = Substitute.For<IPTTaxRuleAdapter>();
            _pTTaxRuleReadRepository = Substitute.For<IPTTaxRuleReadRepository>();
            _pTTaxRuleRepository = Substitute.For<IPTTaxRuleRepository>();
            _httpContextAccessor = Substitute.For<IHttpContextAccessor>();
            _configuration = Substitute.For<IConfiguration>();
            _ivaAppService = Substitute.For<IIvaAppService>();
            _unitOfWorkManager = Substitute.For<IUnitOfWorkManager>();
            _ivaReadRepository = Substitute.For<IIvaReadRepository>();
        }

        private PTTaxRule.Builder GetPtTaxRuleBuilder()
        {
            var resultBuilder = new PTTaxRule.Builder(_notificationHandler)
            .WithId(Guid.NewGuid())
            .WithServiceId(1)
            .WithProductId(Guid.NewGuid())
            .WithDescription("Iva 6");

            return resultBuilder;
        }

        [Fact]
        public void Should_Resolve_All()
        {
            ServiceProvider.GetService<IPTTaxRuleAdapter>().ShouldNotBeNull();
            ServiceProvider.GetService<IPTTaxRuleReadRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<IPTTaxRuleRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<IHttpContextAccessor>().ShouldNotBeNull();
            ServiceProvider.GetService<IConfiguration>().ShouldNotBeNull();
            ServiceProvider.GetService<IIvaAppService>().ShouldNotBeNull();
            ServiceProvider.GetService<IUnitOfWorkManager>().ShouldNotBeNull();
            ServiceProvider.GetService<INotificationHandler>().ShouldNotBeNull();
            ServiceProvider.GetService<IIvaReadRepository>().ShouldNotBeNull();
        }

        [Fact]
        public void Should_AssociateRange()
        {
            var ivaDto = PTTaxRuleAppServiceMock.GetIva();

            _ivaReadRepository.GetById(Guid.NewGuid()).ReturnsForAnyArgs(x =>
            {
                return ivaDto;
            });

            _pTTaxRuleReadRepository.AnyExceptInIvaId(Guid.NewGuid(), new List<int>(), new List<Guid>()).ReturnsForAnyArgs(x =>
            {
                return false;
            });

            _pTTaxRulesAdapter.Map(ivaDto, 1, null).ReturnsForAnyArgs(x =>
            {
                return GetPtTaxRuleBuilder();
            });

            _pTTaxRulesAdapter.Map(ivaDto, Guid.NewGuid()).ReturnsForAnyArgs(x =>
            {
                return GetPtTaxRuleBuilder();
            });

            var pTTaxRuleAssociateDto = new PTTaxRuleAssociateDto
            {
                IvaId = Guid.NewGuid(),
                ServiceList = new List<PTTaxRuleAssociateServiceDto>
                {
                    new PTTaxRuleAssociateServiceDto
                    {
                        ServiceId = 1
                    }
                },
                ProductList = new List<PTTaxRuleAssociateProductDto>
                {
                    new PTTaxRuleAssociateProductDto
                    {
                        ProductId = Guid.NewGuid()
                    }
                }
            };

            new PTTaxRuleAppService(_notificationHandler, _pTTaxRulesAdapter, _pTTaxRuleReadRepository,
                _pTTaxRuleRepository, _httpContextAccessor, _configuration, _ivaAppService, _unitOfWorkManager, _ivaReadRepository)
                .AssociateRange(pTTaxRuleAssociateDto);

            Assert.False(_notificationHandler.HasNotification());
            _pTTaxRuleRepository.Received().DeleteAllByIvaId(pTTaxRuleAssociateDto.IvaId);
        }

        [Fact]
        public void Should_AssociateRange_When_Iva_Zero()
        {
            var ivaDto = PTTaxRuleAppServiceMock.GetIvaZero();

            _ivaReadRepository.GetById(Guid.NewGuid()).ReturnsForAnyArgs(x =>
            {
                return ivaDto;
            });

            _pTTaxRuleReadRepository.AnyExceptInIvaId(Guid.NewGuid(), new List<int>(), new List<Guid>()).ReturnsForAnyArgs(x =>
            {
                return false;
            });

            _pTTaxRulesAdapter.Map(ivaDto, 1, null).ReturnsForAnyArgs(x =>
            {
                return GetPtTaxRuleBuilder();
            });

            _pTTaxRulesAdapter.Map(ivaDto, Guid.NewGuid()).ReturnsForAnyArgs(x =>
            {
                return GetPtTaxRuleBuilder();
            });

            var pTTaxRuleAssociateDto = new PTTaxRuleAssociateDto
            {
                IvaId = Guid.NewGuid(),
                ServiceList = new List<PTTaxRuleAssociateServiceDto>
                {
                    new PTTaxRuleAssociateServiceDto
                    {
                        ServiceId = 1,
                        ExemptionCode = "M12"
                    }
                },
                ProductList = new List<PTTaxRuleAssociateProductDto>
                {
                    new PTTaxRuleAssociateProductDto
                    {
                        ProductId = Guid.NewGuid()
                    }
                }
            };

            new PTTaxRuleAppService(_notificationHandler, _pTTaxRulesAdapter, _pTTaxRuleReadRepository,
                _pTTaxRuleRepository, _httpContextAccessor, _configuration, _ivaAppService, _unitOfWorkManager, _ivaReadRepository)
                .AssociateRange(pTTaxRuleAssociateDto);

            Assert.False(_notificationHandler.HasNotification());
            _pTTaxRuleRepository.Received().DeleteAllByIvaId(pTTaxRuleAssociateDto.IvaId);
        }

        [Fact]
        public void Should_Not_AssociateRange_When_Services_With_Zero_Does_Not_Have_ExemptionCode()
        {
            var ivaDto = PTTaxRuleAppServiceMock.GetIvaZero();

            _ivaReadRepository.GetById(Guid.NewGuid()).ReturnsForAnyArgs(x =>
            {
                return ivaDto;
            });

            _pTTaxRuleReadRepository.AnyExceptInIvaId(Guid.NewGuid(), new List<int>(), new List<Guid>()).ReturnsForAnyArgs(x =>
            {
                return false;
            });

            var pTTaxRuleAssociateDto = new PTTaxRuleAssociateDto
            {
                IvaId = Guid.NewGuid(),
                ServiceList = new List<PTTaxRuleAssociateServiceDto>
                {
                    new PTTaxRuleAssociateServiceDto
                    {
                        ServiceId = 1
                    }
                },
                ProductList = new List<PTTaxRuleAssociateProductDto>
                {
                    new PTTaxRuleAssociateProductDto
                    {
                        ProductId = Guid.NewGuid()
                    }
                }
            };

            new PTTaxRuleAppService(_notificationHandler, _pTTaxRulesAdapter, _pTTaxRuleReadRepository,
                _pTTaxRuleRepository, _httpContextAccessor, _configuration, _ivaAppService, _unitOfWorkManager, _ivaReadRepository)
                .AssociateRange(pTTaxRuleAssociateDto);

            Assert.True(_notificationHandler.HasNotification());
            _pTTaxRuleRepository.DidNotReceive().DeleteAllByIvaId(pTTaxRuleAssociateDto.IvaId);
        }

        [Fact]
        public void Should_Not_AssociateRange_When_Services_Or_Products_Are_Associated_In_Other_Iva()
        {
            var ivaDto = PTTaxRuleAppServiceMock.GetIva();

            _ivaReadRepository.GetById(Guid.NewGuid()).ReturnsForAnyArgs(x =>
            {
                return ivaDto;
            });

            _pTTaxRuleReadRepository.AnyExceptInIvaId(Guid.NewGuid(), new List<int>(), new List<Guid>()).ReturnsForAnyArgs(x =>
            {
                return true;
            });

            var pTTaxRuleAssociateDto = new PTTaxRuleAssociateDto
            {
                IvaId = Guid.NewGuid(),
                ServiceList = new List<PTTaxRuleAssociateServiceDto>
                {
                    new PTTaxRuleAssociateServiceDto
                    {
                        ServiceId = 1
                    }
                },
                ProductList = new List<PTTaxRuleAssociateProductDto>
                {
                    new PTTaxRuleAssociateProductDto
                    {
                        ProductId = Guid.NewGuid()
                    }
                }
            };

            new PTTaxRuleAppService(_notificationHandler, _pTTaxRulesAdapter, _pTTaxRuleReadRepository,
                _pTTaxRuleRepository, _httpContextAccessor, _configuration, _ivaAppService, _unitOfWorkManager, _ivaReadRepository)
                .AssociateRange(pTTaxRuleAssociateDto);

            Assert.True(_notificationHandler.HasNotification());
            _pTTaxRuleRepository.DidNotReceive().DeleteAllByIvaId(pTTaxRuleAssociateDto.IvaId);
        }
    }
}
