﻿using Microsoft.Extensions.DependencyInjection;
using NSubstitute;
using Shouldly;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Thex.Kernel;
using Thex.TaxRule.Application.Brasil.Services;
using Thex.TaxRule.Application.Brazil.Adapters;
using Thex.TaxRule.Domain.Entities;
using Thex.TaxRule.Dto;
using Thex.TaxRule.Dto.GetAll;
using Thex.TaxRule.Infra.Interfaces.ReadRepositories;
using Thex.TaxRule.Web.Tests.Mocks.Dto;
using Thex.ThexRule.Infra.Interfaces;
using Tnf.AspNetCore.TestBase;
using Tnf.Notifications;
using Xunit;

namespace Thex.TaxRule.Web.Tests.Application.ApplicationBase
{
    public class PTTaxRuleAppTests : TnfAspNetCoreIntegratedTestBase<StartupUnitTest>
    {
        public INotificationHandler _notificationHandler;
        public ICSOSNAdapter _csosnAdapter { get; set; }

        public PTTaxRuleAppTests()
        {
            _notificationHandler = new NotificationHandler(ServiceProvider);
            _csosnAdapter = new CSOSNAdapter(_notificationHandler);
        }

        [Fact]
        public void Should_Resolve_All()
        {
            TnfSession.ShouldNotBeNull();
            ServiceProvider.GetService<INotificationHandler>().ShouldNotBeNull();
            ServiceProvider.GetService<ICSOSNAdapter>().ShouldNotBeNull();
            ServiceProvider.GetService<IApplicationUser>().ShouldNotBeNull();
        }

        [Fact]
        public void Should_Create_Entity()
        {
            var dto = new CSOSNAppService(notificationHandler: _notificationHandler,
                                          applicationUser: ServiceProvider.GetService<IApplicationUser>(),
                                          CSOSNAdapter: _csosnAdapter,
                                          CSOSNReadRepository: ServiceProvider.GetService<ICSOSNReadRepository>())
                                         .GeneratedEntity(CsosnDtoMock.GetDto());

            Assert.False(_notificationHandler.HasNotification());
            Assert.True(dto != null && dto.Id != Guid.Empty);
        }

        [Fact]
        public void Should_GetAll()
        {
            var _csosnReadRepository = Substitute.For<ICSOSNReadRepository>();

            _csosnReadRepository.GetAll(new GetAllCSOSNDto()).ReturnsForAnyArgs(x =>
            {
                return CsosnDtoMock.GetListDtoAsync();
            });

            var cfopList = new CSOSNAppService(notificationHandler: _notificationHandler,
                                               applicationUser: ServiceProvider.GetService<IApplicationUser>(),
                                               CSOSNAdapter: _csosnAdapter,
                                               CSOSNReadRepository: _csosnReadRepository)
                    .GetAll(new GetAllCSOSNDto()).Result;

            Assert.False(_notificationHandler.HasNotification());
            Assert.Equal(3, cfopList.Items.Count);
        }

        [Fact]
        public void Should_Get_By_Code()
        {
            var _csosnReadRepository = Substitute.For<ICSOSNReadRepository>();

            _csosnReadRepository.GetByCode("010203", new GetAllCSOSNDto()).ReturnsForAnyArgs(x =>
            {
                return CsosnDtoMock.GetDto();
            });

            var csosn = new CSOSNAppService(notificationHandler: _notificationHandler,
                                               applicationUser: ServiceProvider.GetService<IApplicationUser>(),
                                               CSOSNAdapter: _csosnAdapter,
                                               CSOSNReadRepository: _csosnReadRepository)
                    .GetByCode("010203", new GetAllCSOSNDto()).Result;

            Assert.False(_notificationHandler.HasNotification());
            Assert.True(csosn != null);
        }
    }
}
