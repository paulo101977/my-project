﻿using Microsoft.Extensions.DependencyInjection;
using NSubstitute;
using Shouldly;
using System;
using System.Threading.Tasks;
using Thex.Common;
using Thex.Common.Enumerations;
using Thex.Kernel;
using Thex.TaxRules.Web.Tests.Mocks.Dto;
using Thex.TaxRule.Application.Brazil.Adapters;
using Thex.TaxRule.Domain.Entities;
using Thex.TaxRule.Web.Tests;
using Thex.TaxRule.Web.Tests.Application;
using Thex.ThexRule.Infra.Interfaces;
using Tnf.AspNetCore.TestBase;
using Tnf.Notifications;
using Xunit;

namespace Thex.TaxRules.Web.Tests.Application
{
    public class ApplicationBaseTests : TnfAspNetCoreIntegratedTestBase<StartupUnitTest>
    {
        public INotificationHandler _notificationHandler;
        public ITaxRulesAdapter _taxRuleAdapter { get; set; }

        public ApplicationBaseTests()
        {
            _notificationHandler = new NotificationHandler(ServiceProvider);
            _taxRuleAdapter = new TaxRulesAdapter(_notificationHandler);
        }

        [Fact]
        public void Should_Resolve_All()
        {
            TnfSession.ShouldNotBeNull();
            ServiceProvider.GetService<INotificationHandler>().ShouldNotBeNull();
            ServiceProvider.GetService<ITaxRulesAdapter>().ShouldNotBeNull();
        }

        [Fact]
        public async Task Should_Create_Entity()
        {
            var baseEntity = Substitute.For<IBaseRepository>();

            // Retorna um valor para a função interceptada
            baseEntity.InsertAsync(new BaseEntity()).ReturnsForAnyArgs(x =>
            {
                return TaxRulesDtoMock.GetDto();
            });

            var app = new ApplicationService(notificationHandler: _notificationHandler,
                                             taxRulesAdapter: _taxRuleAdapter,
                                             baseRepository: baseEntity);

            var dto = await app.CreateObjAsync(TaxRulesDtoMock.GetDto());

            Assert.False(_notificationHandler.HasNotification());
            Assert.True(dto != null && dto.Id != Guid.Empty);
        }

        [Fact]
        public async Task Should_Not_Create_Entity_With_Empty_Dto()
        {
            var baseEntity = Substitute.For<IBaseRepository>();

            // Retorna um valor para a função interceptada
            baseEntity.InsertAsync(new BaseEntity()).ReturnsForAnyArgs(x =>
            {
                return TaxRulesDtoMock.GetDto();
            });

            var app = new ApplicationService(notificationHandler: _notificationHandler,
                                             taxRulesAdapter: _taxRuleAdapter,
                                             baseRepository: baseEntity);

            var dto = await app.CreateObjAsync(null);

            Assert.True(_notificationHandler.HasNotification());
            Assert.True(dto == null);
        }

        [Fact]
        public async Task Should_Not_Create_Entity_With_Invalid_Entity()
        {
            var baseEntity = Substitute.For<IBaseRepository>();

            // Retorna um valor para a função interceptada
            baseEntity.InsertAsync(new BaseEntity()).ReturnsForAnyArgs(x =>
            {
                _notificationHandler.Raise(_notificationHandler.DefaultBuilder
                                    .WithMessage(AppConsts.LocalizationSourceName, BillingAccountItemEnum.Error.BillingAccountItemInvalidBillingItemId)
                                    .WithDetailedMessage(AppConsts.LocalizationSourceName, BillingAccountItemEnum.Error.BillingAccountItemInvalidBillingItemId)
                                    .Build());

                return TaxRulesDtoMock.GetDto();
            });

            var app = new ApplicationService(notificationHandler: _notificationHandler,
                                             taxRulesAdapter: _taxRuleAdapter,
                                             baseRepository: baseEntity);

            var dto = await app.CreateObjAsync(TaxRulesDtoMock.GetDto());

            Assert.True(_notificationHandler.HasNotification());
            Assert.True(dto == null);
        }

        [Fact]
        public async Task Should_Edit_Entity()
        {
            var baseEntity = Substitute.For<IBaseRepository>();

            // Retorna um valor para a função interceptada
            baseEntity.UpdateAsync(Guid.Parse("7d220627-853d-466f-835f-a06b6d6b1922"), new BaseEntity()).ReturnsForAnyArgs(x =>
            {
                return TaxRulesDtoMock.GetDto();
            });

            var app = new ApplicationService(notificationHandler: _notificationHandler,
                                             taxRulesAdapter: _taxRuleAdapter,
                                             baseRepository: baseEntity);

            var dto = await app.EditObjAsync(Guid.Parse("7d220627-853d-466f-835f-a06b6d6b1922"),
                                             TaxRulesDtoMock.GetDto());

            Assert.False(_notificationHandler.HasNotification());
            Assert.True(dto != null && dto.Id != Guid.Empty);
        }

        [Fact]
        public async Task Should_Not_Edit_Entity_With_Empty_Dto()
        {
            var app = new ApplicationService(notificationHandler: _notificationHandler,
                                             taxRulesAdapter: _taxRuleAdapter,
                                             baseRepository: ServiceProvider.GetService<IBaseRepository>());

            var dto = await app.EditObjAsync(Guid.Parse("7d220627-853d-466f-835f-a06b6d6b1922"),
                                             null);

            Assert.True(_notificationHandler.HasNotification());
            Assert.True(dto == null);
        }

        [Fact]
        public async Task Should_Not_Edit_Entity_With_Invalid_Entity()
        {
            var baseEntity = Substitute.For<IBaseRepository>();

            // Retorna um valor para a função interceptada
            baseEntity.UpdateAsync(Guid.Parse("7d220627-853d-466f-835f-a06b6d6b1922"), new BaseEntity()).ReturnsForAnyArgs(x =>
            {
                _notificationHandler.Raise(_notificationHandler.DefaultBuilder
                                    .WithMessage(AppConsts.LocalizationSourceName, BillingAccountItemEnum.Error.BillingAccountItemInvalidBillingItemId)
                                    .WithDetailedMessage(AppConsts.LocalizationSourceName, BillingAccountItemEnum.Error.BillingAccountItemInvalidBillingItemId)
                                    .Build());

                return TaxRulesDtoMock.GetDto();
            });

            var app = new ApplicationService(notificationHandler: _notificationHandler,
                                             taxRulesAdapter: _taxRuleAdapter,
                                             baseRepository: baseEntity);

            var dto = await app.EditObjAsync(Guid.Parse("7d220627-853d-466f-835f-a06b6d6b1922"),
                                             TaxRulesDtoMock.GetDto());

            Assert.True(_notificationHandler.HasNotification());
            Assert.True(dto == null);
        }

        [Fact]
        public async Task Should_Delete_Entity()
        {
            var baseEntity = Substitute.For<IBaseRepository>();

            // Retorna um valor para a função interceptada
            baseEntity.RemoveAsync(Guid.Parse("7d220627-853d-466f-835f-a06b6d6b1922")).ReturnsForAnyArgs(x =>
            {
                return Task.CompletedTask;
            });

            var app = new ApplicationService(notificationHandler: _notificationHandler,
                                             taxRulesAdapter: _taxRuleAdapter,
                                             baseRepository: baseEntity);

            await app.DeleteObjAsync(Guid.Parse("7d220627-853d-466f-835f-a06b6d6b1922"));

            Assert.False(_notificationHandler.HasNotification());
        }

        [Fact]
        public async Task Should_Not_Delete_Entity_With_Invalid_Id()
        {
            var app = new ApplicationService(notificationHandler: _notificationHandler,
                                             taxRulesAdapter: _taxRuleAdapter,
                                             baseRepository: ServiceProvider.GetService<IBaseRepository>());

            await app.DeleteObjAsync(Guid.Empty);

            Assert.True(_notificationHandler.HasNotification());
        }

        [Fact]
        public async Task Should_Not_Delete_Entity_With_Invalid_Entity()
        {
            var baseEntity = Substitute.For<IBaseRepository>();

            // Retorna um valor para a função interceptada
            baseEntity.RemoveAsync(Guid.Parse("7d220627-853d-466f-835f-a06b6d6b1922")).ReturnsForAnyArgs(x =>
            {
                _notificationHandler.Raise(_notificationHandler.DefaultBuilder
                                    .WithMessage(AppConsts.LocalizationSourceName, BillingAccountItemEnum.Error.BillingAccountItemInvalidBillingItemId)
                                    .WithDetailedMessage(AppConsts.LocalizationSourceName, BillingAccountItemEnum.Error.BillingAccountItemInvalidBillingItemId)
                                    .Build());

                return Task.CompletedTask;
            });

            var app = new ApplicationService(notificationHandler: _notificationHandler,
                                             taxRulesAdapter: _taxRuleAdapter,
                                             baseRepository: baseEntity);

            await app.DeleteObjAsync(Guid.Parse("7d220627-853d-466f-835f-a06b6d6b1922"));

            Assert.True(_notificationHandler.HasNotification());
        }

        [Fact]
        public async Task Should_Toggle_Entity()
        {
            var baseEntity = Substitute.For<IBaseRepository>();

            // Retorna um valor para a função interceptada
            baseEntity.ToggleAsync(Guid.Parse("7d220627-853d-466f-835f-a06b6d6b1922")).ReturnsForAnyArgs(x =>
            {
                return Task.CompletedTask;
            });

            var app = new ApplicationService(notificationHandler: _notificationHandler,
                                             taxRulesAdapter: _taxRuleAdapter,
                                             baseRepository: baseEntity);

            await app.ToggleActivationObjAsync(Guid.Parse("7d220627-853d-466f-835f-a06b6d6b1922"));

            Assert.False(_notificationHandler.HasNotification());
        }

        [Fact]
        public async Task Should_Not_Toggle_Entity_With_Invalid_Id()
        {
            var app = new ApplicationService(notificationHandler: _notificationHandler,
                                             taxRulesAdapter: _taxRuleAdapter,
                                             baseRepository: ServiceProvider.GetService<IBaseRepository>());

            await app.ToggleActivationObjAsync(Guid.Empty);

            Assert.True(_notificationHandler.HasNotification());
        }

        [Fact]
        public async Task Should_Not_Toggle_Entity_With_Invalid_Entity()
        {
            var baseEntity = Substitute.For<IBaseRepository>();

            // Retorna um valor para a função interceptada
            baseEntity.ToggleAsync(Guid.Parse("7d220627-853d-466f-835f-a06b6d6b1922")).ReturnsForAnyArgs(x =>
            {
                _notificationHandler.Raise(_notificationHandler.DefaultBuilder
                                    .WithMessage(AppConsts.LocalizationSourceName, BillingAccountItemEnum.Error.BillingAccountItemInvalidBillingItemId)
                                    .WithDetailedMessage(AppConsts.LocalizationSourceName, BillingAccountItemEnum.Error.BillingAccountItemInvalidBillingItemId)
                                    .Build());

                return Task.CompletedTask;
            });

            var app = new ApplicationService(notificationHandler: _notificationHandler,
                                             taxRulesAdapter: _taxRuleAdapter,
                                             baseRepository: baseEntity);

            await app.ToggleActivationObjAsync(Guid.Parse("7d220627-853d-466f-835f-a06b6d6b1922"));

            Assert.True(_notificationHandler.HasNotification());
        }
    }
}
