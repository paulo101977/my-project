﻿using Thex.TaxRule.Application.Brasil.Services;
using Thex.TaxRule.Application.Brazil.Adapters;
using Thex.TaxRule.Dto;
using Thex.ThexRule.Infra.Interfaces;
using Thex.TaxRule.Domain.Entities;
using Tnf.Notifications;

namespace Thex.TaxRule.Web.Tests.Application
{
    public class ApplicationService : ApplicationServiceBase<TaxRulesDto, Domain.Entities.TaxRule>
    {
        private readonly ITaxRulesAdapter _taxRulesAdapter;

        public ApplicationService(ITaxRulesAdapter taxRulesAdapter,
                                  INotificationHandler notificationHandler,
                                  IBaseRepository baseRepository) : base(notificationHandler, baseRepository)
        {
            _taxRulesAdapter = taxRulesAdapter;
        }

        public override Domain.Entities.TaxRule GeneratedEntity(TaxRulesDto dto)
        {
            return _taxRulesAdapter.Map(dto).Build();
        }
    }
}
