﻿using Microsoft.Extensions.DependencyInjection;
using NSubstitute;
using Shouldly;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Thex.Kernel;
using Thex.TaxRule.Application.Brasil.Services;
using Thex.TaxRule.Application.Brazil.Adapters;
using Thex.TaxRule.Domain.Entities;
using Thex.TaxRule.Dto;
using Thex.TaxRule.Dto.GetAll;
using Thex.TaxRule.Infra.Interfaces.ReadRepositories;
using Thex.TaxRule.Web.Tests.Mocks.Dto;
using Thex.ThexRule.Infra.Interfaces;
using Tnf.AspNetCore.TestBase;
using Tnf.Notifications;
using Xunit;

namespace Thex.TaxRule.Web.Tests.Application.ApplicationBase
{
    public class CfopAppTests : TnfAspNetCoreIntegratedTestBase<StartupUnitTest>
    {
        public INotificationHandler _notificationHandler;
        public ICFOPAdapter _cfopAdapter { get; set; }

        public CfopAppTests()
        {
            _notificationHandler = new NotificationHandler(ServiceProvider);
            _cfopAdapter = new CFOPAdapter(_notificationHandler);
        }

        [Fact]
        public void Should_Resolve_All()
        {
            TnfSession.ShouldNotBeNull();
            ServiceProvider.GetService<INotificationHandler>().ShouldNotBeNull();
            ServiceProvider.GetService<ICFOPAdapter>().ShouldNotBeNull();
            ServiceProvider.GetService<IApplicationUser>().ShouldNotBeNull();
        }

        [Fact]
        public void Should_Create_Entity()
        {
            var dto = new CFOPAppService(notificationHandler: _notificationHandler,
                                         applicationUser: ServiceProvider.GetService<IApplicationUser>(),
                                         CFOPAdapter: _cfopAdapter,
                                         CFOPReadRepository: ServiceProvider.GetService<ICFOPReadRepository>())
                                         .GeneratedEntity(CfopDtoMock.GetDto());

            Assert.False(_notificationHandler.HasNotification());
            Assert.True(dto != null && dto.Id != Guid.Empty);
        }

        [Fact]
        public void Should_GetAll()
        {
            var _cfopReadRepository = Substitute.For<ICFOPReadRepository>();

            _cfopReadRepository.GetAll(new GetAllCFOPDto()).ReturnsForAnyArgs(x =>
            {
                return CfopDtoMock.GetListDtoAsync();
            });

            var cfopList = new CFOPAppService(notificationHandler: _notificationHandler,
                                         applicationUser: ServiceProvider.GetService<IApplicationUser>(),
                                         CFOPAdapter: _cfopAdapter,
                                         CFOPReadRepository: _cfopReadRepository)
                    .GetAll(new GetAllCFOPDto()).Result;

            Assert.False(_notificationHandler.HasNotification());
            Assert.Equal(3, cfopList.Items.Count);
        }

        [Fact]
        public void Should_GetAll_By_Filters()
        {
            var _cfopReadRepository = Substitute.For<ICFOPReadRepository>();

            _cfopReadRepository.GetAllByFilters(new SearchCfopDto()).ReturnsForAnyArgs(x =>
            {
                return CfopDtoMock.GetListDtoAsync();
            });

            var cfopList = new CFOPAppService(notificationHandler: _notificationHandler,
                                         applicationUser: ServiceProvider.GetService<IApplicationUser>(),
                                         CFOPAdapter: _cfopAdapter,
                                         CFOPReadRepository: _cfopReadRepository)
                    .GetAllByFilters(new Dto.GetAll.SearchCfopDto()).Result;

            Assert.False(_notificationHandler.HasNotification());
            Assert.Equal(3, cfopList.Items.Count);
        }

        [Fact]
        public void Should_Get_By_Code()
        {
            var _cfopReadRepository = Substitute.For<ICFOPReadRepository>();

            _cfopReadRepository.GetByCode("010203", new GetAllCFOPDto()).ReturnsForAnyArgs(x =>
            {
                return CfopDtoMock.GetDto();
            });

            var cfop = new CFOPAppService(notificationHandler: _notificationHandler,
                                         applicationUser: ServiceProvider.GetService<IApplicationUser>(),
                                         CFOPAdapter: _cfopAdapter,
                                         CFOPReadRepository: _cfopReadRepository)
                    .GetByCode("010203", new GetAllCFOPDto()).Result;

            Assert.False(_notificationHandler.HasNotification());
            Assert.True(cfop != null);
        }
    }
}
