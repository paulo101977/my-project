﻿using Microsoft.Extensions.DependencyInjection;
using Shouldly;
using Thex.TaxRule.Base;
using Thex.TaxRule.Web.Tests;
using Tnf.AspNetCore.TestBase;
using Xunit;

namespace Thex.TaxRules.Web.Tests.Controllers
{
    public class ThexControllerBaseTests : TnfAspNetCoreIntegratedTestBase<StartupControllerTest>
    {
        [Fact]
        public void Should_Resolve_All()
        {
            TnfSession.ShouldNotBeNull();
            ServiceProvider.GetService<ThexAppController>().ShouldNotBeNull();
        }
    }
}
