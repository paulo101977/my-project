#load "./parameters.cake"

#tool "nuget:?package=OpenCover&version=4.7.922"
#tool "nuget:?package=ReportGenerator&version=4.1.4"

var parameters = BuildParameters.GetParameters(Context);
var target = Argument("target", "Default");

Task("BuildTest")
    .Does(() => 
    {
         DotNetCoreBuild(parameters.SolutionTarget, new DotNetCoreBuildSettings
		{
			Configuration = parameters.Configuration,
			ArgumentCustomization = arg => arg.AppendSwitch("/p:DebugType","=","Full")
		});
    });

	
Task("OpenCover")
    .IsDependentOn("BuildTest")
    .Does(() => 
    {
        var openCoverSettings = new OpenCoverSettings()
        {
            Register = "user",
			OldStyle = true,
            SkipAutoProps = true,
            ArgumentCustomization = args => args.Append("-coverbytest:*.Tests.dll").Append("-mergebyhash"),
        }
		.WithFilter("+[Thex*]*")
		.WithFilter("-[*.Tests*]*")
		.WithFilter("-[Thex.TaxRule.Dto*]*");
		

        if (!DirectoryExists("./generated-reports"))
            CreateDirectory("./generated-reports");
        
        var outputFile = new FilePath("./generated-reports/report.xml");

        var dotNetTestSettings = new DotNetCoreTestSettings
        {
            Configuration = parameters.Configuration,
            NoBuild = true
        };
		
        OpenCover(context => context.DotNetCoreTest(parameters.ProjectTarget, dotNetTestSettings), outputFile, openCoverSettings);
    });

Task("ReportGenerator")
    .IsDependentOn("OpenCover")
    .Does(() => 
    {
        var reportGeneratorSettings = new ReportGeneratorSettings()
        {
            HistoryDirectory = new DirectoryPath("./generated-reports/reports-history")
        };

        ReportGenerator("./generated-reports/report.xml", "./generated-reports/report-generator-output", reportGeneratorSettings);
    });

Task("Default")
    .IsDependentOn("ReportGenerator")
    .Does(() => 
    { 
        if (IsRunningOnWindows())
        {
			StartProcess("explorer", ".\\generated-reports\\report-generator-output\\index.htm");
        }
    });

RunTarget(target);
