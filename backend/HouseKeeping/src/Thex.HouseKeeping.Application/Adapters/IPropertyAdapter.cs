﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a Crudzilla tool.
// </auto-generated>
//------------------------------------------------------------------------------

using Thex.HouseKeeping.Infra.Entities;
using Thex.HouseKeeping.Dto;

namespace Thex.HouseKeeping.Application.Adapters
{
    public interface IPropertyAdapter
    {
        Property.Builder Map(Property entity, PropertyDto dto);
        Property.Builder Map(PropertyDto dto);
    }
}
