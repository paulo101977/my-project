﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.Common;
using Thex.Common.Enumerations;
using Thex.Common.Extensions;
using Thex.HouseKeeping.Application.Adapters;
using Thex.HouseKeeping.Application.Interfaces;
using Thex.HouseKeeping.Dto;
using Thex.HouseKeeping.Infra.Interfaces;
using Thex.HouseKeeping.Infra.Interfaces.ReadRepositories;
using Thex.Kernel;
using Tnf.Application.Services;
using Tnf.Notifications;
using Tnf.Repositories.Uow;

namespace Thex.HouseKeeping.Application.Services
{
    public class HousekeepingRoomStopAppService : ApplicationService, IHousekeepingRoomStopAppService
    {
        private readonly IHousekeepingRoomReadRepository _housekeepingRoomStopReadRepository;
        private readonly IHousekeepingRoomStopRepository _roomStopRepository;
        private readonly IHousekeepingRoomStopAdapter _stopAdapter;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IApplicationUser _applicationUser;
        private readonly INotificationHandler _notificationHandler;

        public HousekeepingRoomStopAppService(
            IHousekeepingRoomStopRepository roomStopRepository,
            IHousekeepingRoomStopAdapter stopAdapter,
            IApplicationUser applicationUser,
            INotificationHandler notificationHandler,
            IUnitOfWorkManager unitOfWorkManager)
            : base(notificationHandler)
        {
            _applicationUser = applicationUser;
            _notificationHandler = notificationHandler;
            _stopAdapter = stopAdapter;
            _roomStopRepository = roomStopRepository;
            _unitOfWorkManager = unitOfWorkManager;
        }

        public async Task<HousekeepingRoomStopDto> CreateAsync(HousekeepingRoomStopDto dto)
        {
            ValidateDto(dto);
            if (_notificationHandler.HasNotification())
                return null;

            using (var uow = _unitOfWorkManager.Begin())
            {
                dto.PropertyId = int.Parse(_applicationUser.PropertyId);
                dto.CreationTime = DateTime.Now;
                dto.CreatorUserId = _applicationUser.UserUid;

                var stopEntity = _stopAdapter.Map(dto).Build();
                if (_notificationHandler.HasNotification())
                    return null;

                await _roomStopRepository.Add(stopEntity);

                if (_notificationHandler.HasNotification())
                    return null;

                uow.Complete();
            }

            return null;
        }

        private void ValidateDto(HousekeepingRoomStopDto dto)
        {
            if (dto == null)
            {
                Notification.Raise(Notification
                    .DefaultBuilder
                    .WithMessage(AppConsts.LocalizationSourceName, CommonsEnum.Error.NullOrEmptyObject)
                    .Build());
            }
        }
    }
}
