﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Thex.Common;
using Thex.Common.Enumerations;
using Thex.HouseKeeping.Application.Adapters;
using Thex.HouseKeeping.Application.Interfaces;
using Thex.HouseKeeping.Dto;
using Thex.HouseKeeping.Infra.Entities;
using Thex.HouseKeeping.Infra.Interfaces;
using Thex.HouseKeeping.Infra.Interfaces.ReadRepositories;
using Thex.Kernel;
using Tnf.Application.Services;
using Tnf.Notifications;
using Tnf.Repositories.Uow;

namespace Thex.HouseKeeping.Application.Services
{
    public class HousekeepingGeneratorAppService : ApplicationService, IHousekeepingGeneratorAppService
    {
        private readonly IHousekeepingRoomReadRepository _housekeepingRoomReadRepository;
        private readonly IApplicationUser _applicationUser;
        private readonly IHousekeepingRoomAppService _housekeepingRoomAppService;
        private readonly IUserReadRepository _userReadRepository;
        private readonly IHousekeepingRoomAdapter _housekeepingRoomAdapter;
        private readonly IPropertyParameterReadRepository _propertyParameterReadRepository;
        private readonly IUnitOfWorkManager _unitOfWork;

        public HousekeepingGeneratorAppService(
            IHousekeepingRoomReadRepository housekeepingRoomReadRepository,
            IApplicationUser applicationUser,
            INotificationHandler notificationHandler,
            IUserReadRepository userReadRepository,
            IHousekeepingRoomAppService housekeepingRoomAppService,
            IHousekeepingRoomAdapter housekeepingRoomAdapter,
            IUnitOfWorkManager unitOfWork,
            IPropertyParameterReadRepository propertyParameterReadRepository)
            : base(notificationHandler)
        {
            _housekeepingRoomReadRepository = housekeepingRoomReadRepository;
            _applicationUser = applicationUser;
            _userReadRepository = userReadRepository;
            _housekeepingRoomAdapter = housekeepingRoomAdapter;
            _housekeepingRoomAppService = housekeepingRoomAppService;
            _propertyParameterReadRepository = propertyParameterReadRepository;
            _unitOfWork = unitOfWork;
        }

        public async Task<HousekeepingGeneratorResponseDto> GeneratorAsync(List<Guid> generatorList)
        {
            var listUser = new List<UserDto>();

            ValidateDto(generatorList, listUser);

            if (Notification.HasNotification()) return null;

            var systemDate = await _propertyParameterReadRepository.GetSystemDate(int.Parse(_applicationUser.PropertyId));
            var hkGenerator = new HousekeepingGeneratorResponseDto();

            var roomResult = await _housekeepingRoomReadRepository.GetAllAvailableToAssociateAsync(_applicationUser.PropertyId, _applicationUser.TenantId, systemDate);
            roomResult = roomResult.Where(x => x.HousekeepingStatusId == (int)HousekeepingStatusEnum.Dirty).ToList();
            if (roomResult.Any())
                GeneratorOrder(generatorList.OrderBy(x => new Random().Next()).ToList(), hkGenerator, roomResult, listUser);

            return hkGenerator;
        }

        public async Task CreateRangeAsync(HousekeepingGeneratorResponseDto dto)
        {
            ValidateDto(dto);
            if (Notification.HasNotification())
                return;

            var systemDate = await _propertyParameterReadRepository.GetSystemDate(int.Parse(_applicationUser.PropertyId));
            var RoomIdList = dto.RoomMaidList.SelectMany(rm => rm.ListRoom, (roomMaid, room) => room.RoomId).ToList();
            var houseKeepingRoomOriginalList = await _housekeepingRoomReadRepository.GetAllByFiltersAsync(int.Parse(_applicationUser.PropertyId), systemDate, systemDate, RoomIdList);

            ValidateHouseKeepingRoomList(houseKeepingRoomOriginalList.Select(x => x.StartDate).ToList());
            if (Notification.HasNotification())
                return;

            using (var uow = _unitOfWork.Begin())
            {
                if (houseKeepingRoomOriginalList.Any())
                    await _housekeepingRoomAppService.RemoveHouseKeepingAsync(houseKeepingRoomOriginalList.Select(x => x.Id).ToList());

                var housekeepingRoomList = await GeneratorHouseKeepingRoomDto(dto.RoomMaidList);

                await _housekeepingRoomAppService.CreateRangeAsync(housekeepingRoomList);

                if (Notification.HasNotification())
                    return;

                await uow.CompleteAsync();
            }
        }

        private void ValidateDto(List<Guid> dto, List<UserDto> listUser)
        {
            if (dto == null)
            {
                Notification.Raise(Notification
                    .DefaultBuilder
                    .WithMessage(AppConsts.LocalizationSourceName, CommonsEnum.Error.NullOrEmptyObject)
                    .Build());
            }

            var userReturn = _userReadRepository.GetByListId(dto);

            foreach (var userDto in userReturn)
            {
                if (dto.Contains(userDto.Id))
                {
                    listUser.Add(userDto);
                }
                else
                    Notification.Raise(Notification.DefaultBuilder
                              .WithMessage(AppConsts.LocalizationSourceName, HousekeepingGeneratorError.RoomMaidNoExistent)
                              .WithDetailedMessage(AppConsts.LocalizationSourceName, HousekeepingGeneratorError.RoomMaidNoExistent)
                              .Build());

            }
        }

        private void GeneratorOrder(List<Guid> GeneratorList, HousekeepingGeneratorResponseDto hkGenerator, IEnumerable<HousekeepingRoomNonAssocsDto> roomList, List<UserDto> listUser)
        {
            hkGenerator.RoomMaidList = new List<RoomMaid>();

            var roomListOrder = roomList.OrderBy(x => x.RoomNumber).OrderBy(x => x.Floor).OrderByDescending(a => a.CheckInRoomMaid).ToList();

            var average = (int)Math.Ceiling(decimal.Divide(roomListOrder.Count(), GeneratorList.Count()));

            var index = 0;

            foreach (var user in GeneratorList)
            {
                var roomMaid = new RoomMaid();

                var roomMaidList = new List<RoomMaid>();

                roomMaid.ListRoom = new List<DirtyRoom>();

                var listRoom = new List<HousekeepingRoomNonAssocsDto>();

                var roomListCount = Math.Abs(roomListOrder.Count() - index);

                if (index < roomListOrder.Count())
                {
                    if ((index + average) < roomListOrder.Count())
                        listRoom = roomListOrder.GetRange(index, average);
                    else
                        listRoom = roomListOrder.GetRange(index, roomListCount);
                }

                roomMaid.UserId = user;
                roomMaid.Name = listUser.FirstOrDefault(x => x.Id.Equals(user)).Name.ToTitleCase();


                roomMaid.PhotoUrl = (listUser.FirstOrDefault(x => x.Id.Equals(user)).PhotoUrl ?? "");

                foreach (var room in listRoom)
                {
                    roomMaid.ListRoom.Add(new DirtyRoom
                    {
                        RoomId = room.Id,
                        RoomNumber = room.RoomNumber,
                        RoomAbbreviation = room.RoomTypeAbbreviation,
                        Floor = room.Floor,
                        RoomStatus = room.RoomStatusName,
                        Checkin = room.CheckInRoomMaid
                    });
                }

                roomMaid.CountRoom = roomMaid.ListRoom.Count();
                hkGenerator.RoomMaidList.Add(roomMaid);

                index += average;
            }
        }

        private async Task<List<HousekeepingRoom>> GeneratorHouseKeepingRoomDto(List<RoomMaid> roomMaidList)
        {
            var listHouseKeepingRoom = new List<HousekeepingRoom>();

            foreach (var roomMaid in roomMaidList.Where(x => x.ListRoom.Any()))
            {
                foreach (var room in roomMaid.ListRoom)
                {
                    var systemDate = await _propertyParameterReadRepository.GetSystemDate(int.Parse(_applicationUser.PropertyId));

                    var hkDto = new HousekeepingRoomDto
                    {
                        Id = Guid.NewGuid(),
                        PropertyId = int.Parse(_applicationUser.PropertyId),
                        CreatorUserId = _applicationUser.UserUid,
                        CreationTime = DateTime.Now.ToZonedDateTimeLoggedUser(_applicationUser),
                        OwnerId = roomMaid.UserId,
                        RoomId = room.RoomId,
                        ServiceDate = systemDate.Date
                    };

                    listHouseKeepingRoom.Add(_housekeepingRoomAdapter.Map(hkDto).Build());
                }
            }

            return listHouseKeepingRoom;
        }

        private void ValidateHouseKeepingRoomList(ICollection<DateTime?> HouseKeepingRoomStartDateList)
        {
            if (HouseKeepingRoomStartDateList.Any(d => d.HasValue))
            {
                Notification.Raise(Notification
                    .DefaultBuilder
                    .WithMessage(AppConsts.LocalizationSourceName, HousekeepingRoom.EntityError.AnyHouseKeepingRoomsCanNotBeAssociated)
                    .Build());

                return;
            }
        }
    }
}
