﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.Common;
using Thex.Common.Enumerations;
using Thex.Common.Extensions;
using Thex.HouseKeeping.Application.Adapters;
using Thex.HouseKeeping.Application.Interfaces;
using Thex.HouseKeeping.Dto;
using Thex.HouseKeeping.Infra.Interfaces;
using Thex.HouseKeeping.Infra.Interfaces.ReadRepositories;
using Thex.Kernel;
using Tnf.Application.Services;
using Tnf.Notifications;
using Tnf.Repositories.Uow;

namespace Thex.HouseKeeping.Application.Services
{
    public class HousekeepingRoomReviewAppService : ApplicationService, IHousekeepingRoomReviewAppService
    {
        private readonly IHousekeepingRoomReviewRepository _roomReviewRepository;
        private readonly IHousekeepingRoomReviewAdapter _reviewAdapter;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IApplicationUser _applicationUser;
        private readonly INotificationHandler _notificationHandler;

        public HousekeepingRoomReviewAppService(
            IHousekeepingRoomReviewRepository roomReviewRepository,
            IHousekeepingRoomReviewAdapter reviewAdapter,
            IApplicationUser applicationUser,
            INotificationHandler notificationHandler,
            IUnitOfWorkManager unitOfWorkManager)
            : base(notificationHandler)
        {
            _applicationUser = applicationUser;
            _notificationHandler = notificationHandler;
            _reviewAdapter = reviewAdapter;
            _roomReviewRepository = roomReviewRepository;
            _unitOfWorkManager = unitOfWorkManager;
        }

        public async Task<HousekeepingRoomReviewDto> CreateAsync(HousekeepingRoomReviewDto dto)
        {
            ValidateDto(dto);
            if (_notificationHandler.HasNotification())
                return null;

            using (var uow = _unitOfWorkManager.Begin())
            {
                dto.PropertyId = int.Parse(_applicationUser.PropertyId);
                dto.CreationTime = DateTime.Now;
                dto.CreatorUserId = _applicationUser.UserUid;

                var reviewEntity = _reviewAdapter.Map(dto).Build();
                if (_notificationHandler.HasNotification())
                    return null;

                await _roomReviewRepository.Add(reviewEntity);

                if (_notificationHandler.HasNotification())
                    return null;

                uow.Complete();
            }

            return null;
        }

        private void ValidateDto(HousekeepingRoomReviewDto dto)
        {
            if (dto == null)
            {
                Notification.Raise(Notification
                    .DefaultBuilder
                    .WithMessage(AppConsts.LocalizationSourceName, CommonsEnum.Error.NullOrEmptyObject)
                    .Build());
            }
        }
    }
}
