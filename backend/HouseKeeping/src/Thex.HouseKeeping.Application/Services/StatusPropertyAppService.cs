﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Thex.Common.Extensions;
using Thex.HouseKeeping.Application.Adapters;
using Thex.HouseKeeping.Application.Interfaces;
using Thex.HouseKeeping.Dto;
using Thex.HouseKeeping.Infra.Entities;
using Thex.HouseKeeping.Infra.Interfaces;
using Thex.HouseKeeping.Infra.Interfaces.ReadRepositories;
using Thex.Kernel;
using Tnf.Application.Services;
using Tnf.Notifications;
using Tnf.Repositories.Uow;

namespace Thex.HouseKeeping.Application.Services
{
    public class StatusPropertyAppService : ApplicationService, IStatusPropertyAppService
    {
        private readonly IStatusPropertyReadRepository _statusPropertyReadRepository;
        private readonly IHousekeepingStatusPropertyAdapter _statusPropertyAdapter;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IApplicationUser _applicationUser;

        public StatusPropertyAppService(
            IStatusPropertyReadRepository statusPropertyReadRepository,
            IHousekeepingStatusPropertyAdapter statusPropertyAdapter,
            IUnitOfWorkManager unitOfWorkManager,
            IApplicationUser applicationUser,
            INotificationHandler notificationHandler)
            : base(notificationHandler)
        {
            _statusPropertyReadRepository = statusPropertyReadRepository;
            _statusPropertyAdapter = statusPropertyAdapter;
            _unitOfWorkManager = unitOfWorkManager;
            _applicationUser = applicationUser;
        }

        public virtual async Task<List<HousekeepingStatusPropertyDto>> GetAll()
        {
            return await _statusPropertyReadRepository.GetAll(int.Parse(_applicationUser.PropertyId));
        }
    }
}
