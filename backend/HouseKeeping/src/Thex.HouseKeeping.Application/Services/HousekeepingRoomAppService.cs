﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Thex.Common;
using Thex.Common.Extensions;
using Thex.HouseKeeping.Application.Adapters;
using Thex.HouseKeeping.Application.Interfaces;
using Thex.HouseKeeping.Dto;
using Thex.HouseKeeping.Infra.Entities;
using Thex.HouseKeeping.Infra.Interfaces;
using Thex.HouseKeeping.Infra.Interfaces.ReadRepositories;
using Thex.Kernel;
using Tnf.Application.Services;
using Tnf.Notifications;
using Tnf.Repositories.Uow;

namespace Thex.HouseKeeping.Application.Services
{
    public class HousekeepingRoomAppService : ApplicationService, IHousekeepingRoomAppService
    {
        private readonly IHousekeepingRoomReadRepository _housekeepingRoomReadRepository;
        private readonly IHousekeepingRoomRepository _housekeepingRoomRepository;
        private readonly IHousekeepingRoomAdapter _housekeepingRoomAdapter;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IApplicationUser _applicationUser;
        private readonly IPropertyParameterReadRepository _propertyParameterReadRepository;

        public HousekeepingRoomAppService(
            IHousekeepingRoomReadRepository housekeepingRoomReadRepository,
            IHousekeepingRoomRepository housekeepingRoomRepository,
            IHousekeepingRoomAdapter housekeepingRoomAdapter,
            IUnitOfWorkManager unitOfWorkManager,
            IPropertyParameterReadRepository propertyParameterReadRepository,
            IApplicationUser applicationUser,
            INotificationHandler notificationHandler)
            : base(notificationHandler)
        {
            _housekeepingRoomReadRepository = housekeepingRoomReadRepository;
            _housekeepingRoomRepository = housekeepingRoomRepository;
            _housekeepingRoomAdapter = housekeepingRoomAdapter;
            _unitOfWorkManager = unitOfWorkManager;
            _applicationUser = applicationUser;
            _propertyParameterReadRepository = propertyParameterReadRepository;
        }

        public virtual async Task<List<HousekeepingRoomDto>> GetAll()
        {
            var systemDate = await _propertyParameterReadRepository.GetSystemDate(int.Parse(_applicationUser.PropertyId));
            return await _housekeepingRoomReadRepository.GetAll(_applicationUser.PropertyId, _applicationUser.UserUid, _applicationUser.TenantId, systemDate);
        }

        public virtual async Task<List<HousekeepingRoomGridDto>> GetAllGrid()
        {
            var systemDate = await _propertyParameterReadRepository.GetSystemDate(int.Parse(_applicationUser.PropertyId));

            return await _housekeepingRoomReadRepository.GetAllGrid(_applicationUser.PropertyId, _applicationUser.TenantId, systemDate);
        }

        public virtual async Task<HousekeepingRoomGridDto> GetGridUser(Guid? housekeepingRoomId)
        {
            var systemDate = await _propertyParameterReadRepository.GetSystemDate(int.Parse(_applicationUser.PropertyId));
            return await _housekeepingRoomReadRepository.GetGridUser(_applicationUser.PropertyId, housekeepingRoomId, systemDate);
        }

        public virtual async Task<List<HousekeepingRoomNonAssocsDto>> GetAllNonAssocs()
        {
            var systemDate = await _propertyParameterReadRepository.GetSystemDate(int.Parse(_applicationUser.PropertyId));
            return await _housekeepingRoomReadRepository.GetAllNonAssocs(_applicationUser.PropertyId, _applicationUser.TenantId, systemDate);
        }

        public async Task<HousekeepingRoomDto> UpdateAsync(Guid id, HousekeepingRoomDto dto)
        {
            ValidateDto(dto);
            if (Notification.HasNotification())
                return null;

            using (var uow = _unitOfWorkManager.Begin())
            {
                var housekeepingRoomDto = await _housekeepingRoomReadRepository.GetDtoByIdAsync(id);
                dto.Id = id;
                dto.PropertyId = housekeepingRoomDto.PropertyId;
                dto.LastModifierUserId = _applicationUser.UserUid;
                dto.LastModificationTime = DateTime.Now.ToZonedDateTimeLoggedUser(_applicationUser);

                var local = _housekeepingRoomAdapter.Map(housekeepingRoomDto).Build();
                var housekeepingRoom = _housekeepingRoomAdapter.Map(local, dto).Build();
                if (Notification.HasNotification())
                    return null;

                await _housekeepingRoomRepository.UpdateItem(id, housekeepingRoom);

                if (Notification.HasNotification())
                    return null;

                uow.Complete();
            }

            return dto;
        }

        public async Task<HousekeepingRoomDto> CreateAsync(HousekeepingRoomDto dto)
        {
            ValidateDto(dto);
            if (Notification.HasNotification())
                return null;

            var houseKeepingRoomOriginalList = await _housekeepingRoomReadRepository.GetAllByFiltersAsync(int.Parse(_applicationUser.PropertyId), dto.ServiceDateStart.GetValueOrDefault(), dto.ServiceDateEnd.GetValueOrDefault(), dto.RoomList.Select(r => r.Id).ToList());

            ValidateHouseKeepingRoomList(houseKeepingRoomOriginalList.Select(r => r.StartDate).ToList());
            if (Notification.HasNotification())
                return null;

            using (var uow = _unitOfWorkManager.Begin())
            {
                if (houseKeepingRoomOriginalList.Any())
                    await RemoveHouseKeepingAsync(houseKeepingRoomOriginalList.Select(x => x.Id).ToList());

                dto.Id = Guid.NewGuid();
                dto.PropertyId = int.Parse(_applicationUser.PropertyId);
                dto.CreatorUserId = _applicationUser.UserUid;
                dto.CreationTime = DateTime.Now;
                if (Guid.Empty == dto.OwnerId)
                {
                    dto.OwnerId = _applicationUser.UserUid;
                }

                if (Notification.HasNotification())
                    return null;

                if (dto.ServiceDateStart != null && dto.ServiceDateEnd != null && dto.RoomList.Any())
                {
                    foreach (var room in dto.RoomList)
                    {
                        var now = dto.ServiceDateStart;
                        while (now <= dto.ServiceDateEnd)
                        {
                            dto.Id = Guid.NewGuid();
                            dto.RoomId = room.Id;
                            dto.ServiceDate = now;

                            var housekeepingRoom = _housekeepingRoomAdapter.Map(dto).Build();
                            await _housekeepingRoomRepository.CreateItem(housekeepingRoom);

                            if (Notification.HasNotification())
                                return null;

                            now = now.Value.AddDays(1);
                        }
                    }
                }
                else if (dto.ServiceDate != null)
                {
                    var systemDate = await _propertyParameterReadRepository.GetSystemDate(int.Parse(_applicationUser.PropertyId));
                    dto.ServiceDate = systemDate;

                    var housekeepingRoom = _housekeepingRoomAdapter.Map(dto).Build();
                    await _housekeepingRoomRepository.CreateItem(housekeepingRoom);
                }

                if (Notification.HasNotification())
                    return null;

                uow.Complete();
            }

            return dto;
        }

        public async Task CreateRangeAsync(List<HousekeepingRoom> dto)
            =>  await _housekeepingRoomRepository.CreateRangeItem(dto);

        private void ValidateHouseKeepingRoomList(ICollection<DateTime?> HouseKeepingRoomStartDateList)
        {
            if (HouseKeepingRoomStartDateList.Any(d => d.HasValue))
            {
                Notification.Raise(Notification
                    .DefaultBuilder
                    .WithMessage(AppConsts.LocalizationSourceName, HousekeepingRoom.EntityError.AnyHouseKeepingRoomsCanNotBeAssociated)
                    .Build());

                return;
            }
        }

        public async Task RemoveHouseKeepingAsync(List<Guid> houseKeepingIdList)
            => await _housekeepingRoomRepository.RemoveRangeAsync(houseKeepingIdList);
    }
}
