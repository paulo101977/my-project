﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Thex.Common.Extensions;
using Thex.HouseKeeping.Application.Adapters;
using Thex.HouseKeeping.Application.Interfaces;
using Thex.HouseKeeping.Dto;
using Thex.HouseKeeping.Infra.Entities;
using Thex.HouseKeeping.Infra.Interfaces;
using Thex.HouseKeeping.Infra.Interfaces.ReadRepositories;
using Thex.Kernel;
using Tnf.Application.Services;
using Tnf.Notifications;
using Tnf.Repositories.Uow;

namespace Thex.HouseKeeping.Application.Services
{
    public class ReasonAppService : ApplicationService, IReasonAppService
    {
        private readonly IReasonReadRepository _reasonReadRepository;
        private readonly IReasonAdapter _reasonAdapter;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IApplicationUser _applicationUser;

        public ReasonAppService(
            IReasonReadRepository reasonReadRepository,
            IReasonAdapter reasonAdapter,
            IUnitOfWorkManager unitOfWorkManager,
            IApplicationUser applicationUser,
            INotificationHandler notificationHandler)
            : base(notificationHandler)
        {
            _reasonReadRepository = reasonReadRepository;
            _reasonAdapter = reasonAdapter;
            _unitOfWorkManager = unitOfWorkManager;
            _applicationUser = applicationUser;
        }

        public virtual async Task<List<ReasonDto>> GetAll()
        {
            return await _reasonReadRepository.GetAll();
        }

        public virtual async Task<List<ReasonDto>> GetByCategory(int id)
        {
            return await _reasonReadRepository.GetByCategory(id);
        }
    }
}
