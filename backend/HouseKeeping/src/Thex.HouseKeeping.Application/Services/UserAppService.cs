﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Thex.HouseKeeping.Application.Adapters;
using Thex.HouseKeeping.Application.Interfaces;
using Thex.HouseKeeping.Dto;
using Thex.HouseKeeping.Infra.Interfaces;
using Thex.HouseKeeping.Infra.Interfaces.ReadRepositories;
using Thex.Kernel;
using Thex.Storage;
using Tnf.Application.Services;
using Tnf.Notifications;
using Tnf.Repositories.Uow;

namespace Thex.HouseKeeping.Application.Services
{
    public class UserAppService : ApplicationService, IUserAppService
    {
        private readonly IUserReadRepository _userReadRepository;
        private readonly IUserRepository _userRepository;
        private readonly IUserAdapter _userAdapter;
        private readonly IAzureStorage _azureStorage;

        private readonly IPersonReadRepository _personReadRepository;
        private readonly IPersonRepository _personRepository;
        private readonly IPersonAdapter _personAdapter;

        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IApplicationUser _applicationUser;
        private readonly IPropertyParameterReadRepository _propertyParameterReadRepository;

        public UserAppService(
            IUserReadRepository userReadRepository,
            IUserRepository userRepository,
            IUserAdapter userAdapter,
            IAzureStorage azureStorage,
            IApplicationUser applicationUser,
            IPersonReadRepository personReadRepository,
            IPersonRepository personRepository,
            IPropertyParameterReadRepository propertyParameterReadRepository,
            IPersonAdapter personAdapter,
            IUnitOfWorkManager unitOfWorkManager,
            INotificationHandler notificationHandler)
            : base(notificationHandler)
        {
            _userReadRepository = userReadRepository;
            _userRepository = userRepository;
            _userAdapter = userAdapter;
            _azureStorage = azureStorage;
            _applicationUser = applicationUser;
            _propertyParameterReadRepository = propertyParameterReadRepository;
            _personReadRepository = personReadRepository;
            _personRepository = personRepository;
            _personAdapter = personAdapter;

            _unitOfWorkManager = unitOfWorkManager;
        }

        public virtual async Task<List<UserDto>> GetAll()
        {
            return await _userReadRepository.GetAll(_applicationUser.TenantId);
        }

        public virtual async Task<List<HousekeepingRoomGridDto>> GetUserRoomByUserId(Guid? userId)
        {
            userId = userId ?? _applicationUser.UserUid;
            return await _userReadRepository.GetUserRoomByUserId(userId.Value, _applicationUser.PropertyId);
        }

        public virtual async Task<List<HousekeepingRoomGridDto>> GetRoomByUserId(Guid? userId)
        {
            var systemDate = await _propertyParameterReadRepository.GetSystemDate(int.Parse(_applicationUser.PropertyId));
            userId = userId ?? _applicationUser.UserUid;
            return await _userReadRepository.GetRoomByUserId(userId.Value, int.Parse(_applicationUser.PropertyId), systemDate);
        }


        public virtual async Task<List<HousekeepingRoomGridDto>> GetUserRoomHistory(bool isFilterSystemDate = false)
        {
            var systemDate = await _propertyParameterReadRepository.GetSystemDate(int.Parse(_applicationUser.PropertyId));
            return await _userReadRepository.GetUserRoomHistory(_applicationUser.TenantId, systemDate, isFilterSystemDate);
        }
            


        public virtual async Task<UserDto> GetById()
        {
            return await _userReadRepository.GetById(_applicationUser.UserUid);
        }

        public async Task<UserDto> SendPhoto(Guid id, UserDto dto)
        {
            var _bytes = Convert.FromBase64String(dto.PhotoBase64);
            var url = await _azureStorage.SendAsync(id, id.ToString(), id.ToString(), _bytes);

            if (Notification.HasNotification())
                return null;

            using (var uow = _unitOfWorkManager.Begin())
            {
                var userDto = await _userReadRepository.GetById(id);
                var personDto = await _personReadRepository.GetById(userDto.PersonId);
                personDto.PhotoUrl = url;

                var localPerson = _personAdapter.Map(personDto).Build();

                if (Notification.HasNotification())
                    return null;

                await _personRepository.UpdateItem(personDto.Id, localPerson);

                if (Notification.HasNotification())
                    return null;

                uow.Complete();
            }

            return new UserDto() { PhotoUrl = url };
        }

        public async Task<UserDto> UpdateAsync(Guid id, UserDto dto)
        {
            ValidateDto(dto);
            if (Notification.HasNotification())
                return null;

            using (var uow = _unitOfWorkManager.Begin())
            {
                var userDto = await _userReadRepository.GetById(id);
                userDto.Id = id;
                userDto.LastModifierUserId = _applicationUser.UserUid;
                userDto.LastModificationTime = DateTime.Now;

                userDto.Name = dto.Name;
                userDto.Email = dto.Email;

                var local = _userAdapter.Map(userDto).Build();
                //var user = _userAdapter.Map(local, dto).Build();
                if (Notification.HasNotification())
                    return null;

                await _userRepository.UpdateItem(id, local);

                if (Notification.HasNotification())
                    return null;

                var personDto = await _personReadRepository.GetById(local.PersonId);
                personDto.Id = local.PersonId.Value;
                personDto.PhotoUrl = dto.PhotoUrl;
                personDto.DateOfBirth = DateTime.Parse(dto.DateOfBirth);

                var localPerson = _personAdapter.Map(personDto).Build();

                if (Notification.HasNotification())
                    return null;

                await _personRepository.UpdateItem(personDto.Id, localPerson);

                if (Notification.HasNotification())
                    return null;

                uow.Complete();
            }

            return null;
        }
    }
}
