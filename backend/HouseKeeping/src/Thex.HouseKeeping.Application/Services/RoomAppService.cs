﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Thex.Common.Extensions;
using Thex.HouseKeeping.Application.Adapters;
using Thex.HouseKeeping.Application.Interfaces;
using Thex.HouseKeeping.Dto;
using Thex.HouseKeeping.Infra.Entities;
using Thex.HouseKeeping.Infra.Interfaces;
using Thex.HouseKeeping.Infra.Interfaces.ReadRepositories;
using Thex.Kernel;
using Tnf.Application.Services;
using Tnf.Notifications;
using Tnf.Repositories.Uow;

namespace Thex.HouseKeeping.Application.Services
{
    public class RoomAppService : ApplicationService, IRoomAppService
    {
        private readonly IRoomReadRepository _roomReadRepository;
        private readonly IRoomAdapter _roomAdapter;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IApplicationUser _applicationUser;

        public RoomAppService(
            IRoomReadRepository roomReadRepository,
            IRoomAdapter roomAdapter,
            IUnitOfWorkManager unitOfWorkManager,
            IApplicationUser applicationUser,
            INotificationHandler notificationHandler)
            : base(notificationHandler)
        {
            _roomReadRepository = roomReadRepository;
            _roomAdapter = roomAdapter;
            _unitOfWorkManager = unitOfWorkManager;
            _applicationUser = applicationUser;
        }

        public virtual async Task<List<RoomDto>> GetAll()
        {
            return await _roomReadRepository.GetAll(_applicationUser.PropertyId, _applicationUser.TenantId);
        }
    }
}
