﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.HouseKeeping.Dto;
using Tnf.Application.Services;

namespace Thex.HouseKeeping.Application.Interfaces
{
    public interface IHousekeepingRoomDisagreementAppService : IApplicationService
    {
        Task<HousekeepingRoomDisagreementDto> CreateAsync(HousekeepingRoomDisagreementDto dto);
    }
}
