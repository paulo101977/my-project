﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.HouseKeeping.Dto;
using Thex.HouseKeeping.Infra.Entities;
using Tnf.Application.Services;

namespace Thex.HouseKeeping.Application.Interfaces
{
    public interface IUserAppService : IApplicationService
    {
        Task<List<UserDto>> GetAll();
        Task<UserDto> GetById();
        Task<List<HousekeepingRoomGridDto>> GetUserRoomByUserId(Guid? userId);
        Task<List<HousekeepingRoomGridDto>> GetUserRoomHistory(bool isFilterSystemDate);

        Task<UserDto> SendPhoto(Guid id, UserDto dto);
        Task<UserDto> UpdateAsync(Guid id, UserDto dto);

        Task<List<HousekeepingRoomGridDto>> GetRoomByUserId(Guid? userId);
    }
}
