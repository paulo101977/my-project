﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.HouseKeeping.Dto;
using Thex.HouseKeeping.Infra.Entities;
using Tnf.Application.Services;

namespace Thex.HouseKeeping.Application.Interfaces
{
    public interface IReasonAppService : IApplicationService
    {
        Task<List<ReasonDto>> GetAll();
        Task<List<ReasonDto>> GetByCategory(int id);
    }
}
