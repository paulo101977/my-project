﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.HouseKeeping.Dto;
using Thex.HouseKeeping.Infra.Entities;
using Tnf.Application.Services;

namespace Thex.HouseKeeping.Application.Interfaces
{
    public interface IHousekeepingRoomAppService : IApplicationService
    {
        Task<HousekeepingRoomGridDto> GetGridUser(Guid? housekeepingRoomId);
        Task<List<HousekeepingRoomDto>> GetAll();
        Task<List<HousekeepingRoomGridDto>> GetAllGrid();
        Task<List<HousekeepingRoomNonAssocsDto>> GetAllNonAssocs();
        Task<HousekeepingRoomDto> CreateAsync(HousekeepingRoomDto dto);
        Task<HousekeepingRoomDto> UpdateAsync(Guid id, HousekeepingRoomDto dto);
        Task RemoveHouseKeepingAsync(List<Guid> houseKeepingIdList);
        Task CreateRangeAsync(List<HousekeepingRoom> dto);

    }
}
