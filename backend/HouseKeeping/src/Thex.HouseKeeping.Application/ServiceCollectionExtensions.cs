﻿using Thex.HouseKeeping.Application.Adapters;
using Thex.HouseKeeping.Application.Interfaces;
using Thex.HouseKeeping.Application.Services;
using Thex.Storage;

namespace Microsoft.Extensions.DependencyInjection
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddApplicationServiceDependency(this IServiceCollection services)
        {
            services.AddInfraDependency();

            // Registro dos serviços
            services.AddTransient<IHousekeepingRoomAppService, HousekeepingRoomAppService>();
            services.AddTransient<IHousekeepingRoomReviewAppService, HousekeepingRoomReviewAppService>();
            services.AddTransient<IHousekeepingRoomStopAppService, HousekeepingRoomStopAppService>();
            services.AddTransient<IHousekeepingRoomDisagreementAppService, HousekeepingRoomDisagreementAppService>();
            services.AddTransient<IStatusPropertyAppService, StatusPropertyAppService>();
            services.AddTransient<IReasonAppService, ReasonAppService>();
            services.AddTransient<IRoomAppService, RoomAppService>();
            services.AddTransient<IUserAppService, UserAppService>();
            services.AddTransient<IHousekeepingGeneratorAppService, HousekeepingGeneratorAppService>();



            services.AddTransient<IHousekeepingRoomAdapter, HousekeepingRoomAdapter>();
            services.AddTransient<IHousekeepingRoomReviewAdapter, HousekeepingRoomReviewAdapter>();
            services.AddTransient<IHousekeepingRoomStopAdapter, HousekeepingRoomStopAdapter>();
            services.AddTransient<IHousekeepingRoomDisagreementAdapter, HousekeepingRoomDisagreementAdapter>();
            services.AddTransient<IHousekeepingStatusPropertyAdapter, HousekeepingStatusPropertyAdapter>();
            services.AddTransient<IReasonAdapter, ReasonAdapter>();
            services.AddTransient<IRoomAdapter, RoomAdapter>();
            services.AddTransient<IUserAdapter, UserAdapter>();
            services.AddTransient<IPersonAdapter, PersonAdapter>();
            services.AddTransient<IAzureStorage, AzureStorage>();



            return services;
        }
    }
}