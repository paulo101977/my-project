﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using Thex.HouseKeeping.Application.Interfaces;
using Thex.HouseKeeping.Dto;
using Thex.HouseKeeping.Infra.Entities;
using Tnf.AspNetCore.Mvc.Response;

namespace Thex.HouseKeeping.Mobile.Controllers
{
    [Route(RouteConsts.ReasonRouteName)]
    [Authorize]
    public class ReasonController : TnfController
    {
        private readonly IReasonAppService _reasonAppService;

        public ReasonController(IReasonAppService reasonAppService)
        {
            _reasonAppService = reasonAppService;
        }

        [HttpGet("{id}")]
        [ProducesResponseType(typeof(ReasonDto), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> GetByCategory(int id)
        {
            if (id <= 0) return BadRequest();

            var response = await _reasonAppService.GetByCategory(id);

            return CreateResponseOnGet(response);
        }

        [HttpGet]
        [ProducesResponseType(typeof(ReasonDto), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> GetAll()
        {
            var response = await _reasonAppService.GetAll();

            return CreateResponseOnGet(response);
        }
    }
}
