﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using Thex.HouseKeeping.Application;
using Thex.HouseKeeping.Application.Interfaces;
using Thex.HouseKeeping.Dto;
using Thex.HouseKeeping.Infra.Entities;
using Tnf.AspNetCore.Mvc.Response;

namespace Thex.HouseKeeping.Mobile.Controllers
{
    [Route(RouteConsts.HousekeepingRoomRouteName)]
    [Authorize]
    public class HousekeepingRoomController : TnfController
    {
        private readonly IHousekeepingRoomAppService _housekeepingRoomAppService;

        public HousekeepingRoomController(IHousekeepingRoomAppService housekeepingRoomAppService)
        {
            _housekeepingRoomAppService = housekeepingRoomAppService;
        }

        [HttpGet("getAll")]
        [ProducesResponseType(typeof(HousekeepingRoomDto), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> GetAllAssocs()
        {
            var response = await _housekeepingRoomAppService.GetAll();

            return CreateResponseOnGet(response);
        }

        [HttpGet("getGridUser")]
        [ProducesResponseType(typeof(HousekeepingRoomGridDto), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> GetGridUser(Guid? housekeepingRoomId)
        {
            var response = await _housekeepingRoomAppService.GetGridUser(housekeepingRoomId);

            return CreateResponseOnGet(response);
        }

        [HttpGet("getAllGrid")]
        [ThexAuthorize("HK_HousekeepingRoom_Get_GetAllGrid")]
        [ProducesResponseType(typeof(HousekeepingRoomGridDto), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> GetAllGrid()
        {
            var response = await _housekeepingRoomAppService.GetAllGrid();

            return CreateResponseOnGet(response);
        }

        [HttpGet("getAllNonAssocs")]
        [ProducesResponseType(typeof(HousekeepingRoomNonAssocsDto), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> GetAllNonAssocs()
        {
            var response = await _housekeepingRoomAppService.GetAllNonAssocs();

            return CreateResponseOnGet(response);
        }

        [HttpPost]
        [ProducesResponseType(typeof(HousekeepingRoomDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Create([FromBody] HousekeepingRoomDto requestDto)
        {
            var res = await _housekeepingRoomAppService.CreateAsync(requestDto);

            return CreateResponseOnPost(res);
        }

        [HttpPut("{id}")]
        [ProducesResponseType(typeof(HousekeepingRoomDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Update(Guid id, [FromBody] HousekeepingRoomDto requestDto)
        {
            var res = await _housekeepingRoomAppService.UpdateAsync(id, requestDto);

            return CreateResponseOnPut(res);
        }
    }
}
