﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using Thex.HouseKeeping.Application.Interfaces;
using Thex.HouseKeeping.Dto;
using Thex.HouseKeeping.Infra.Entities;
using Tnf.AspNetCore.Mvc.Response;

namespace Thex.HouseKeeping.Mobile.Controllers
{
    [Route(RouteConsts.HousekeepingGeneratorRouteName)]
    [Authorize]
    public class HousekeepingGeneratorController : TnfController
    {
        private readonly IHousekeepingGeneratorAppService _housekeepingGeneratorAppService;

        public HousekeepingGeneratorController(IHousekeepingGeneratorAppService housekeepingGeneratorAppService)
        {
            _housekeepingGeneratorAppService = housekeepingGeneratorAppService;
        }

        [HttpPost]
        [ThexAuthorize("HK_HouseKeepingGenerator_Post")]
        [ProducesResponseType(typeof(HousekeepingGeneratorDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Create([FromBody] HousekeepingGeneratorDto requestDto)
        {
            var result = await _housekeepingGeneratorAppService.GeneratorAsync(requestDto.GeneratorList);

            return CreateResponseOnPost(result);
        }
               
        
        [HttpPost("CreateHouseKeeping")]
        [ThexAuthorize("HK_HouseKeepingGenerator_Post_CreateHouseKeeping")]
        [ProducesResponseType(typeof(HousekeepingGeneratorResponseDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Create([FromBody] HousekeepingGeneratorResponseDto requestDto)
        {
            await _housekeepingGeneratorAppService.CreateRangeAsync(requestDto);

            return CreateResponseOnPost();
        }

    }
}
