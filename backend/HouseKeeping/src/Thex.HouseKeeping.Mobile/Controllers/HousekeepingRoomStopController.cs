﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Thex.HouseKeeping.Application.Interfaces;
using Thex.HouseKeeping.Dto;
using Tnf.AspNetCore.Mvc.Response;

namespace Thex.HouseKeeping.Mobile.Controllers
{
    [Route(RouteConsts.HousekeepingRoomStopRouteName)]
    [Authorize]
    public class HousekeepingRoomStopController : TnfController
    {
        private readonly IHousekeepingRoomStopAppService _housekeepingRoomStopAppService;

        public HousekeepingRoomStopController(IHousekeepingRoomStopAppService housekeepingRoomStopAppService)
        {
            _housekeepingRoomStopAppService = housekeepingRoomStopAppService;
        }

        [HttpPost]
        [ProducesResponseType(typeof(HousekeepingRoomStopDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Create([FromBody] HousekeepingRoomStopDto requestDto)
        {
            await _housekeepingRoomStopAppService.CreateAsync(requestDto);

            return CreateResponseOnPost(HousekeepingRoomStopDto.NullInstance);
        }
    }
}
