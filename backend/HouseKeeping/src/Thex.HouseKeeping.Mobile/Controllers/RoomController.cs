﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using Thex.HouseKeeping.Application.Interfaces;
using Thex.HouseKeeping.Dto;
using Thex.HouseKeeping.Infra.Entities;
using Tnf.AspNetCore.Mvc.Response;

namespace Thex.HouseKeeping.Mobile.Controllers
{
    [Route(RouteConsts.RoomRouteName)]
    [Authorize]
    public class RoomController : TnfController
    {
        private readonly IRoomAppService _reasonAppService;

        public RoomController(IRoomAppService reasonAppService)
        {
            _reasonAppService = reasonAppService;
        }

        [HttpGet("{id}")]
        [ProducesResponseType(typeof(RoomDto), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> GetByCategory(int id)
        {
            if (id <= 0) return BadRequest();

            //var response = await _reasonAppService.GetAll(id);

            return CreateResponseOnGet(null);
        }

        [HttpGet]
        [ProducesResponseType(typeof(RoomDto), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> GetAll()
        {
            var response = await _reasonAppService.GetAll();

            return CreateResponseOnGet(response);
        }
    }
}
