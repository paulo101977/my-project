﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Thex.HouseKeeping.Application.Interfaces;
using Thex.HouseKeeping.Dto;
using Tnf.AspNetCore.Mvc.Response;

namespace Thex.HouseKeeping.Mobile.Controllers
{
    [Route(RouteConsts.HousekeepingRoomReviewRouteName)]
    [Authorize]
    public class HousekeepingRoomReviewController : TnfController
    {
        private readonly IHousekeepingRoomReviewAppService _housekeepingRoomReviewAppService;

        public HousekeepingRoomReviewController(IHousekeepingRoomReviewAppService housekeepingRoomReviewAppService)
        {
            _housekeepingRoomReviewAppService = housekeepingRoomReviewAppService;
        }

        [HttpPost]
        [ProducesResponseType(typeof(HousekeepingRoomReviewDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Create([FromBody] HousekeepingRoomReviewDto requestDto)
        {
            await _housekeepingRoomReviewAppService.CreateAsync(requestDto);

            return CreateResponseOnPost(HousekeepingRoomReviewDto.NullInstance);
        }
    }
}
