﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using Thex.HouseKeeping.Application.Interfaces;
using Thex.HouseKeeping.Dto;
using Thex.HouseKeeping.Infra.Entities;
using Tnf.AspNetCore.Mvc.Response;

namespace Thex.HouseKeeping.Mobile.Controllers
{
    [Route(RouteConsts.UserRouteName)]
    [Authorize]
    public class UserController : TnfController
    {
        private readonly IUserAppService _userAppService;

        public UserController(IUserAppService userAppService)
        {
            _userAppService = userAppService;
        }

        [HttpGet]
        [ProducesResponseType(typeof(UserDto), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> GetAll()
        {
            var response = await _userAppService.GetAll();

            return CreateResponseOnGet(response);
        }

        [HttpGet("getRoomByUserId")]
        [ThexAuthorize("HK_User_Get_GetUserRoomByUserId")]
        [ProducesResponseType(typeof(HousekeepingRoomGridDto), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> GetRoomByUserId([FromQuery] Guid? userId = null)
        {
            var response = await _userAppService.GetRoomByUserId(userId);

            return CreateResponseOnGet(response);
        }

        [HttpGet("getUserRoomByUserId")]
        [ThexAuthorize("HK_User_Get_GetUserRoomByUserId")]
        [ProducesResponseType(typeof(HousekeepingRoomGridDto), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> GetUserRoomByUserId([FromQuery] Guid? userId = null)
        {
            var response = await _userAppService.GetUserRoomByUserId(userId);

            return CreateResponseOnGet(response);
        }

        [HttpGet("getUserRoomHistory")]
        [ThexAuthorize("HK_User_Get_GetUserRoomHistory")]
        [ProducesResponseType(typeof(HousekeepingRoomGridDto), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> GetUserRoomHistory([FromQuery] Guid? userId = null, bool isFilterSystemDate = false)
        {
            var response = await _userAppService.GetUserRoomHistory(isFilterSystemDate);

            return CreateResponseOnGet(response);
        }

        [HttpGet("getById")] // frontend mobile
        [ProducesResponseType(typeof(UserDto), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> GetById()
        {
            var response = await _userAppService.GetById();

            return CreateResponseOnGet(response);
        }

        [HttpPut("sendPhoto")]
        [ProducesResponseType(typeof(UserDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> SendPhoto(Guid id, [FromBody] UserDto userDto)
        {
            var response = await _userAppService.SendPhoto(id, userDto);

            return CreateResponseOnPut(response);
        }

        [HttpPut("{id}")]
        [ProducesResponseType(typeof(UserDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Update(Guid id, [FromBody] UserDto requestDto)
        {
            await _userAppService.UpdateAsync(id, requestDto);

            return CreateResponseOnPut(UserDto.NullInstance);
        }
    }
}
