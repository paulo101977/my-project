﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Thex.HouseKeeping.Application.Interfaces;
using Thex.HouseKeeping.Dto;
using Tnf.AspNetCore.Mvc.Response;

namespace Thex.HouseKeeping.Mobile.Controllers
{
    [Route(RouteConsts.HousekeepingRoomDisagreementRouteName)]
    [Authorize]
    public class HousekeepingRoomDisagreementController : TnfController
    {
        private readonly IHousekeepingRoomDisagreementAppService _housekeepingRoomDisagreementAppService;

        public HousekeepingRoomDisagreementController(IHousekeepingRoomDisagreementAppService housekeepingRoomDisagreementAppService)
        {
            _housekeepingRoomDisagreementAppService = housekeepingRoomDisagreementAppService;
        }

        [HttpPost]
        [ProducesResponseType(typeof(HousekeepingRoomDisagreementDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Create([FromBody] HousekeepingRoomDisagreementDto requestDto)
        {
            await _housekeepingRoomDisagreementAppService.CreateAsync(requestDto);

            return CreateResponseOnPost(HousekeepingRoomDisagreementDto.NullInstance);
        }
    }
}
