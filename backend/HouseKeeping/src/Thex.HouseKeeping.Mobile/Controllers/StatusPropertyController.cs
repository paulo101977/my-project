﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using Thex.HouseKeeping.Application.Interfaces;
using Thex.HouseKeeping.Dto;
using Thex.HouseKeeping.Infra.Entities;
using Tnf.AspNetCore.Mvc.Response;

namespace Thex.HouseKeeping.Mobile.Controllers
{
    [Route(RouteConsts.StatusPropertyRouteName)]
    [Authorize]
    public class StatusPropertyController : TnfController
    {
        private readonly IStatusPropertyAppService _statusPropertyAppService;

        public StatusPropertyController(IStatusPropertyAppService statusPropertyAppService)
        {
            _statusPropertyAppService = statusPropertyAppService;
        }

        [HttpGet]
        [ProducesResponseType(typeof(HousekeepingStatusPropertyDto), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> GetAll()
        {
            var response = await _statusPropertyAppService.GetAll();

            return CreateResponseOnGet(response);
        }
    }
}
