﻿namespace Thex.HouseKeeping.Mobile
{
    public class RouteConsts
    {
        public const string HousekeepingRoomRouteName = "api/housekeepingRoom";
        public const string HousekeepingRoomReviewRouteName = "api/housekeepingRoomReview";
        public const string HousekeepingRoomStopRouteName = "api/housekeepingRoomStop";
        public const string HousekeepingRoomDisagreementRouteName = "api/housekeepingRoomDisagreement";
        public const string StatusPropertyRouteName = "api/statusProperty";
        public const string ReasonRouteName = "api/housekeepingReason";
        public const string RoomRouteName = "api/room";
        public const string UserRouteName = "api/user";
        public const string HousekeepingGeneratorRouteName = "api/housekeepingGenerator";
    }
}
