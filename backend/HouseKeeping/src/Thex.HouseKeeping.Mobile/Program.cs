﻿using Elasticsearch.Net;
using Elasticsearch.Net.Aws;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Serilog;
using Serilog.Sinks.Elasticsearch;
using System;
using System.Diagnostics;
using System.IO;
using Thex.AspNetCore.Security;
using Thex.Common;

namespace Thex.HouseKeeping.Mobile
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Console.Title = typeof(Program).Namespace;

            var hostConfig = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("hosting.json")
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .Build();

            var host = WebHost.CreateDefaultBuilder(args)
                .UseConfiguration(hostConfig)
                .ConfigureAppConfiguration((hostingContext, config) =>
                {
                    var env = hostingContext.HostingEnvironment;
                    config.AddJsonFile($"appsettings.json", optional: false, reloadOnChange: true);

                    config.AddEnvironmentVariables();
                    config.AddCommandLine(args);
                })
                .ConfigureLogging((hostingContext, logging) =>
                {
                    var config = new ServicesConfiguration();
                    hostConfig.GetSection("ServicesConfiguration").Bind(config);

                    var env = hostingContext.HostingEnvironment;

                    Log.Logger = new LoggerConfiguration()
                        .Enrich.WithProperty("Creation", DateTime.Now)
                        .Enrich.FromLogContext()
                        .MinimumLevel.Error()
                        .WriteTo.Elasticsearch(new ElasticsearchSinkOptions(new Uri(config.ElasticSearch.Uri))
                        {
                            ModifyConnectionSettings = conn =>
                            {
                                var awsCredentials = new Amazon.Runtime.BasicAWSCredentials(
                                    HashManager.GenerateMD5Decrypto(config.ElasticSearch.Accesskey),
                                    HashManager.GenerateMD5Decrypto(config.ElasticSearch.Secretkey));

                                var httpConnection = new AwsHttpConnection(awsCredentials, Amazon.RegionEndpoint.USEast1);

                                var pool = new SingleNodeConnectionPool(new Uri(config.ElasticSearch.Uri));
                                return new ConnectionConfiguration(pool, httpConnection);
                            },
                            ConnectionTimeout = new TimeSpan(0, 10, 0),
                            IndexFormat = "hk-log-" + env.EnvironmentName.ToLower() + "-{0:yyyy.MM}",
                            FailureCallback = e => Console.WriteLine("Unable to submit event " + e.MessageTemplate + e.Exception),
                            EmitEventFailure = EmitEventFailureHandling.WriteToSelfLog |
                                                EmitEventFailureHandling.WriteToFailureSink |
                                                EmitEventFailureHandling.RaiseCallback,
                            FailureSink = new LoggerConfiguration().WriteTo
                                            .File(Path.Combine(Directory.GetCurrentDirectory(), "hk-log-" + env.EnvironmentName.ToLower() + "-{Date}.txt")).CreateLogger()
                        })
                        .CreateLogger();

                    if (Debugger.IsAttached)
                        Log.Logger = new LoggerConfiguration()
                           .Enrich.WithMachineName()
                           .ReadFrom.Configuration(hostingContext.Configuration)
                           .CreateLogger();
                })
                .UseStartup<Startup>()
                .UseSerilog()
                .UseKestrel()
                .UseContentRoot(Directory.GetCurrentDirectory())
                .UseIISIntegration()
                .UseSetting("detailedErrors", "true")
                //.UseUrls("http://0.0.0.0:5004")
                .Build();

            host.Run();

            Log.CloseAndFlush();
        }
    }
}
