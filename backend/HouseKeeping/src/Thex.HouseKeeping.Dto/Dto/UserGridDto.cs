﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a Crudzilla tool.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using Tnf.Dto;

namespace Thex.HouseKeeping.Dto
{
    public partial class UserGridDto : UserDto
    {
        public static UserGridDto NullInstance = null;

        public Guid Id { get; set; }

        public string Name { get; set; }

        public string Email { get; set; }

        public string DateOfBirth { get; set; }
    }
}
