﻿using Tnf.Dto;

namespace Thex.HouseKeeping.Dto
{
    public class TransportationTypeDto : BaseDto
    {
        public int Id { get; set; }
        public static TransportationTypeDto NullInstance = null;

        public string TransportationTypeName { get; set; }
    }
}