﻿

using System;
using System.Collections.Generic;
using Tnf.Dto;

namespace Thex.HouseKeeping.Dto
{
    public partial class HousekeepingGeneratorDto : BaseDto
    {
        public List<Guid> GeneratorList { get; set; }
    }

    public partial class HousekeepingGeneratorResponseDto : BaseDto
    {
        public List<RoomMaid> RoomMaidList { get; set; }
    }


    public partial class RoomMaid
    {
        public Guid UserId { get; set; }
        public string Name { get; set; }
        public string PhotoUrl { get; set; }
        public List<DirtyRoom> ListRoom { get; set; }        
        public int CountRoom { get; set; }

    }

    public class DirtyRoom
    {
        public int RoomId { get; set; }
        public string RoomNumber { get; set; }
        public string RoomAbbreviation { get; set; }
        public string Floor { get; set; }
        public string RoomStatus { get; set; }
        public DateTime Checkin { get; set; }
        
        
    }

    public enum HousekeepingGeneratorError
    {
        RoomMaidNoExistent

    }


}
