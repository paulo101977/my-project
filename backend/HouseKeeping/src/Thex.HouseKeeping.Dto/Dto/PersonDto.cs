﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a Crudzilla tool.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using Tnf.Dto;

namespace Thex.HouseKeeping.Dto
{
    public partial class PersonDto : BaseDto
    {
        public static PersonDto NullInstance = null;

        public Guid Id { get; set; }
        public string PersonType { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FullName { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public int? CountrySubdivisionId { get; set; }
        public string PhotoUrl { get; set; }
    }
}
