﻿// //  <copyright file="TransportationTypeEnum.cs" company="TOTVS">
// //  Copyright (c) TOTVS. All rights reserved.
// //  </copyright>
namespace Thex.HouseKeeping.Dto.Enumerations
{
    public enum TransportationTypeEnum
    {
        ArrivingByPlane = 1,
        ArrivingByCar = 2,
        ArrivingByMotorcycle = 3,
        ArrivingByShipOrFerryBoat = 4,
        ArrivingByBus = 5,
        ArrivingByTrain = 6,
        ArrivingByOther = 7
    }
}