﻿using System.Threading.Tasks;
using Tnf.Dapper.Repositories;
using Tnf.Repositories;
using Tnf.Localization;
using Thex.HouseKeeping.Infra.Context;
using Thex.HouseKeeping.Infra.Entities;
using Thex.HouseKeeping.Infra.Interfaces;
using Thex.Kernel;
using Thex.GenericLog;

namespace Thex.HouseKeeping.Infra.Repositories
{
    public class HousekeepingRoomReviewRepository : DapperEfRepositoryBase<ThexHouseKeepingContext, HousekeepingRoomReview>, IHousekeepingRoomReviewRepository
    {
        private readonly ILocalizationManager _localizationManager;
        private readonly IApplicationUser _applicationUser;

        public HousekeepingRoomReviewRepository(
            IActiveTransactionProvider activeTransactionProvider,
            ILocalizationManager localizationManager,
            IApplicationUser applicationUser,
            IGenericLogHandler genericLogHandler)
            : base(activeTransactionProvider, genericLogHandler)
        {
            _localizationManager = localizationManager;
            _applicationUser = applicationUser;
        }

        public async Task Add(HousekeepingRoomReview obj)
        {
            await InsertAsync(obj);
        }
    }
}
