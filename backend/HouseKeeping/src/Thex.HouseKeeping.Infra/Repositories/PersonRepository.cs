﻿using System.Threading.Tasks;
using Tnf.Dapper.Repositories;
using Tnf.Repositories;
using Tnf.Localization;
using Thex.HouseKeeping.Infra.Context;
using Thex.HouseKeeping.Infra.Entities;
using Thex.HouseKeeping.Infra.Interfaces;
using System;
using Thex.HouseKeeping.Dto;
using Thex.Kernel;
using Thex.GenericLog;

namespace Thex.HouseKeeping.Infra.Repositories
{
    public class PersonRepository : DapperEfRepositoryBase<ThexHouseKeepingContext, Person>, IPersonRepository
    {
        private readonly ILocalizationManager _localizationManager;
        private readonly IApplicationUser _applicationUser;

        public PersonRepository(
            IActiveTransactionProvider activeTransactionProvider,
            ILocalizationManager localizationManager,
            IApplicationUser applicationUser,
            IGenericLogHandler genericLogHandler)
            : base(activeTransactionProvider, genericLogHandler)
        {
            _localizationManager = localizationManager;
            _applicationUser = applicationUser;
        }

        public async Task UpdateItem(Guid id, Person obj)
        {
            await UpdateAsync(obj);
        }
    }
}
