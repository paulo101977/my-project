﻿using System.Threading.Tasks;
using Tnf.Dapper.Repositories;
using Tnf.Repositories;
using Tnf.Localization;
using Thex.HouseKeeping.Infra.Context;
using Thex.HouseKeeping.Infra.Entities;
using Thex.HouseKeeping.Infra.Interfaces;
using System;
using Thex.HouseKeeping.Dto;
using Thex.Kernel;
using Thex.GenericLog;

namespace Thex.HouseKeeping.Infra.Repositories
{
    public class UserRepository : DapperEfRepositoryBase<ThexHouseKeepingContext, User>, IUserRepository
    {
        private readonly ILocalizationManager _localizationManager;
        private readonly IApplicationUser _applicationUser;

        public UserRepository(
            IActiveTransactionProvider activeTransactionProvider,
            ILocalizationManager localizationManager,
            IApplicationUser applicationUser,
            IGenericLogHandler genericLogHandler)
            : base(activeTransactionProvider, genericLogHandler)
        {
            _localizationManager = localizationManager;
            _applicationUser = applicationUser;
        }

        public async Task UpdateItem(Guid id, User obj)
        {
            await UpdateAsync(obj);
        }
    }
}
