﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.GenericLog;
using Thex.HouseKeeping.Infra.Context;
using Thex.HouseKeeping.Infra.Entities;
using Thex.HouseKeeping.Infra.Interfaces;
using Thex.Kernel;
using Tnf.Dapper.Repositories;
using Tnf.Localization;
using Tnf.Repositories;

namespace Thex.HouseKeeping.Infra.Repositories
{
    public class HousekeepingRoomRepository : DapperEfRepositoryBase<ThexHouseKeepingContext, HousekeepingRoom>, IHousekeepingRoomRepository
    {
        private readonly ILocalizationManager _localizationManager;
        private readonly IApplicationUser _applicationUser;

        public HousekeepingRoomRepository(
            IActiveTransactionProvider activeTransactionProvider,
            ILocalizationManager localizationManager,
            IApplicationUser applicationUser,
            IGenericLogHandler genericLogHandler)
            : base(activeTransactionProvider, genericLogHandler)
        {
            _localizationManager = localizationManager;
            _applicationUser = applicationUser;
        }

        public async Task UpdateItem(Guid id, HousekeepingRoom obj)
        => await UpdateAsync(obj);


        public async Task CreateItem(HousekeepingRoom obj)
        => await InsertAsync(obj);


        public async Task CreateRangeItem(List<HousekeepingRoom> obj)
        => await InsertAsync(obj);

        public async Task RemoveAsync(HousekeepingRoom obj)
        => await DeleteAsync(obj);

        public async Task RemoveRangeAsync(List<Guid> houseKeepingRoomIdList)
        {
            await QueryAsync(@" UPDATE HouseKeepingRoom
                                SET IsDeleted = 1,
                                    DeletionTime = @Date,
                                    DeleterUserId = @UserUid
                                WHERE HouseKeepingRoomId in @IdList",
                  new
                  {
                      Date = DateTime.Now.ToZonedDateTimeLoggedUser(_applicationUser),
                      _applicationUser.UserUid,
                      IdList = houseKeepingRoomIdList
                  });
        }
    }
}
