﻿using System.Threading.Tasks;
using Tnf.Dapper.Repositories;
using Tnf.Repositories;
using Tnf.Localization;
using Thex.HouseKeeping.Infra.Context;
using Thex.HouseKeeping.Infra.Entities;
using Thex.HouseKeeping.Infra.Interfaces;
using Thex.Kernel;
using Thex.GenericLog;

namespace Thex.HouseKeeping.Infra.Repositories
{
    public class HousekeepingRoomDisagreementRepository : DapperEfRepositoryBase<ThexHouseKeepingContext, HousekeepingRoomDisagreement>, IHousekeepingRoomDisagreementRepository
    {
        private readonly ILocalizationManager _localizationManager;
        private readonly IApplicationUser _applicationUser;

        public HousekeepingRoomDisagreementRepository(
            IActiveTransactionProvider activeTransactionProvider,
            ILocalizationManager localizationManager,
            IApplicationUser applicationUser,
            IGenericLogHandler genericLogHandler)
            : base(activeTransactionProvider, genericLogHandler)
        {
            _localizationManager = localizationManager;
            _applicationUser = applicationUser;
        }

        public async Task Add(HousekeepingRoomDisagreement obj)
        {
            await InsertAsync(obj);
        }
    }
}
