﻿using System;
using System.Threading.Tasks;
using Tnf.Dapper.Repositories;
using Tnf.Repositories;
using System.Linq;
using Tnf.Localization;
using Thex.Common;
using Thex.HouseKeeping.Infra.Context;
using Thex.HouseKeeping.Infra.Entities;
using Thex.HouseKeeping.Infra.Interfaces.ReadRepositories;
using Thex.HouseKeeping.Dto.Enumerations;
using Thex.HouseKeeping.Dto;
using System.Collections.Generic;
using Dapper;
using Slapper;
using Thex.Common.Enumerations;
using Thex.GenericLog;

namespace Thex.HouseKeeping.Infra.Repositories.ReadRepositories
{
    public class ReasonReadRepository : DapperEfRepositoryBase<ThexHouseKeepingContext, Reason>, IReasonReadRepository
    {
        private readonly ILocalizationManager _localizationManager;

        public ReasonReadRepository(
            IActiveTransactionProvider activeTransactionProvider,
            ILocalizationManager localizationManager,
            IGenericLogHandler genericLogHandler)
            : base(activeTransactionProvider, genericLogHandler)
        {
            _localizationManager = localizationManager;
        }

        private string GetStatusName(int? statusId)
        {
            if ((int)ReservationStatus.Checkin == statusId) return _localizationManager.GetString(AppConsts.LocalizationSourceName, RoomAvailabilityEnum.Busy.ToString());
            else return _localizationManager.GetString(AppConsts.LocalizationSourceName, RoomAvailabilityEnum.Vague.ToString());
        }

        public async Task<List<ReasonDto>> GetByCategory(int id)
        {

            var result = (await QueryAsync<ReasonDto>(@"
                select r.ReasonId as Id, r.*
                from   Reason r
                where  r.ReasonCategoryId = @ReasonCategoryId
            ", new { ReasonCategoryId = id })).ToList();

            return result;
        }

        public async Task<List<ReasonDto>> GetAll()
        {
            var result = (await QueryAsync<ReasonDto>(@"
                select r.ReasonId as Id, r.*
                from   Reason r
            ")).ToList();

            return result;
        }
    }
}
