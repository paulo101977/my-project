﻿using System;
using System.Threading.Tasks;
using Tnf.Dapper.Repositories;
using Tnf.Repositories;
using System.Linq;
using Tnf.Localization;
using Thex.Common;
using Thex.HouseKeeping.Infra.Context;
using Thex.HouseKeeping.Infra.Entities;
using Thex.HouseKeeping.Infra.Interfaces.ReadRepositories;
using Thex.HouseKeeping.Dto.Enumerations;
using Thex.HouseKeeping.Dto;
using System.Collections.Generic;
using Dapper;
using Slapper;
using Thex.Common.Enumerations;
using Thex.GenericLog;

namespace Thex.HouseKeeping.Infra.Repositories.ReadRepositories
{
    public class HousekeepingRoomReadRepository : DapperEfRepositoryBase<ThexHouseKeepingContext, HousekeepingRoom>, IHousekeepingRoomReadRepository
    {
        private readonly ILocalizationManager _localizationManager;

        public HousekeepingRoomReadRepository(
            IActiveTransactionProvider activeTransactionProvider,
            ILocalizationManager localizationManager,
            IGenericLogHandler genericLogHandler)
            : base(activeTransactionProvider, genericLogHandler)
        {
            _localizationManager = localizationManager;
        }

        private string GetStatusName(int? statusId)
        {
            if ((int)ReservationStatus.Checkin == statusId) return _localizationManager.GetString(AppConsts.LocalizationSourceName, RoomAvailabilityEnum.Busy.ToString());
            else return _localizationManager.GetString(AppConsts.LocalizationSourceName, RoomAvailabilityEnum.Vague.ToString());
        }

        public async Task<HousekeepingRoomDto> GetDtoByIdAsync(Guid? id)
        {

            var result = (await QueryAsync<HousekeepingRoomDto>(@"
                select hr.*
                from   HousekeepingRoom hr
                where  hr.HousekeepingRoomId = @HousekeepingRoomId
            ", new { HousekeepingRoomId = id })).First();

            return result;
        }

        public async Task<HousekeepingRoom> GetByIdASync(Guid id)
        {
            var result = (await QueryAsync<HousekeepingRoom>(@"
                select hr.HousekeepingRoomId as Id,
                       hr.*
                from   HousekeepingRoom hr
                where  hr.HousekeepingRoomId = @HousekeepingRoomId
            ", new { HousekeepingRoomId = id })).FirstOrDefault();

            return result;
        }

        public async Task<List<HousekeepingRoomDto>> GetAll(string propertyId, Guid? ownerId, Guid? tenantId, DateTime systemDate)
        {
            ownerId = (ownerId == Guid.Empty) ? null : ownerId;

            var query = await QueryAsync<dynamic>(@"SELECT 						
                        hr.HousekeepingRoomId as Id,
                        hr.HousekeepingRoomId,
	                    hr.PropertyId,
	                    hr.OwnerId,
	                    hr.RoomId,
	                    hr.IsStarted,
	                    hr.StartDate,
	                    hr.EndDate,
	                    hr.ElapsedTime,      
						rl.QuantitySingle,
						rl.QuantityDouble,

                        hrr.Review as HousekeepingRoomReviewList_Review,
                        hrs.Reason as HousekeepingRoomStopList_Reason,
                        hrd.Observation as HousekeepingRoomDisagreementList_Observation,
                            
                        ri.ReservationItemStatusId,
                        ri.CheckInDate, 
                        ri.CheckOutDate, 
                        r.RoomNumber, 
                        r.RoomTypeId,
                        rt.Abbreviation as RoomTypeAbbreviation,
                        hsp.StatusName as HousekeepingStatusName,
                        hsp.Color as HousekeepingStatusColor
                        FROM HousekeepingRoom hr    

                        inner join Room r on hr.RoomId = r.RoomId and r.TenantId = @TenantId and r.IsDeleted = 0
                        inner join RoomType rt on rt.RoomTypeId = r.RoomTypeId
                        left  join ReservationItem ri on ri.RoomId = r.RoomId and ri.ReservationItemStatusId = @ReservationStatusId and ri.IsDeleted = 0
                        left  join RoomLayout rl on rl.RoomLayoutId = ri.RoomLayoutId
                        inner join HousekeepingStatusProperty hsp on r.HousekeepingStatusPropertyId = hsp.HousekeepingStatusPropertyId

                        left  join HousekeepingRoomReview hrr on hr.HousekeepingRoomId = hrr.HousekeepingRoomId
                        left  join HousekeepingRoomStop hrs on hr.HousekeepingRoomId = hrs.HousekeepingRoomId
                        left  join HousekeepingRoomDisagreement hrd on hr.HousekeepingRoomId = hrd.HousekeepingRoomId
                        where  hr.PropertyId = @PropertyId 
                           AND hr.OwnerId = COALESCE(@OwnerId, hr.OwnerId)
                           AND hr.IsDeleted = 0
                           AND cast(hr.ServiceDate as date) = cast(@SystemDate as date)
                           AND hr.EndDate is null
            ", new
            {
                TenantId = tenantId,
                ReservationStatusId = (int)ReservationStatus.Checkin,
                PropertyId = propertyId,
                OwnerId = ownerId,
                SystemDate = systemDate
            });
            
            Slapper.AutoMapper.Cache.ClearAllCaches();
            Slapper.AutoMapper.Configuration.AddIdentifier(
                typeof(HousekeepingRoomDto), "Id");
            Slapper.AutoMapper.Configuration.AddIdentifier(
                typeof(HousekeepingRoomReviewDto), "Review");
            Slapper.AutoMapper.Configuration.AddIdentifier(
                typeof(HousekeepingRoomStopDto), "Reason");
            Slapper.AutoMapper.Configuration.AddIdentifier(
                typeof(HousekeepingRoomDisagreementDto), "Observation");

            var result = (Slapper.AutoMapper.MapDynamic<HousekeepingRoomDto>(query) as IEnumerable<HousekeepingRoomDto>).ToList();
            result.ForEach(item => {
                item.RoomStatusName = GetStatusName(item.ReservationItemStatusId);
            });

            return result;
        }

        public async Task<List<HousekeepingRoomNonAssocsDto>> GetAllNonAssocs(string propertyId, Guid? tenantId, DateTime systemDate)
        {
            var filters = " and  (select count(1) from HousekeepingRoom hr where hr.RoomId = r.RoomId AND cast(hr.ServiceDate as date) = cast(@SystemDate as date) AND hr.IsDeleted = 0) <= 0";

            var result = await GetAllByFiltersAsync(int.Parse(propertyId), tenantId, systemDate,  filters);

            var resultDto = result.MapTo<List<HousekeepingRoomNonAssocsDto>>();

            resultDto.ForEach(item => {
                item.RoomStatusName = GetStatusName(item.ReservationItemStatusId);
            });

            return resultDto;
        }

        public async Task<List<HousekeepingRoomNonAssocsDto>> GetAllAvailableToAssociateAsync(string propertyId, Guid? tenantId, DateTime systemDate)
        {
            var filters = " and hk.StartDate is null";

            var result = await GetAllByFiltersAsync(int.Parse(propertyId), tenantId, systemDate, filters);

            var resultDto = result.MapTo<List<HousekeepingRoomNonAssocsDto>>();

            resultDto.ForEach(item => {
                item.RoomStatusName = GetStatusName(item.ReservationItemStatusId);
            });

            return resultDto;
        }

        public async Task<List<HousekeepingRoomGridDto>> GetAllGrid(string propertyId, Guid? tenantId, DateTime systemDate)
        {
            var query = await QueryAsync<HousekeepingRoomGridDto>(@"
                select  distinct r.RoomId as Id,
                        hr.HousekeepingRoomId,
	                    ri.ReservationItemStatusId,
	                    ri.CheckInDate, 
                        ri.CheckOutDate, 
                        r.RoomId,
	                    r.RoomNumber, 
                        r.RoomTypeId,
	                    rt.Abbreviation as RoomTypeAbbreviation,
                        hsp.StatusName as HousekeepingStatusName,
                        hsp.Color as HousekeepingStatusColor,
                        hsp.HouseKeepingStatusId,					    
                        hr.StartDate,                        
                        u.Id as PersonId,
					    COALESCE(p.FullName, u.Name) as FullName,
					    p.PhotoUrl
                from   Room r
				    left  join HousekeepingRoom hr on hr.RoomId = r.RoomId and hr.IsDeleted = 0 and cast(hr.ServiceDate as date) = cast(@SystemDate as date)
				    left  join Users u on u.Id = hr.OwnerId and u.IsDeleted = 0
				    left  join Person p on p.PersonId = u.PersonId
                    inner join RoomType rt on rt.RoomTypeId = r.RoomTypeId
                    left  join ReservationItem ri on ri.RoomId = r.RoomId and ri.ReservationItemStatusId = @ReservationStatusId and ri.IsDeleted = 0
                    inner join HousekeepingStatusProperty hsp on r.HousekeepingStatusPropertyId = hsp.HousekeepingStatusPropertyId
                where   r.PropertyId = @PropertyId
				    and r.TenantId = @TenantId
                    and r.IsDeleted = 0
            ", new
            {
                ReservationStatusId = (int)ReservationStatus.Checkin,
                TenantId = tenantId,
                PropertyId = propertyId,
                SystemDate = systemDate
            });

            var result = query.MapTo<List<HousekeepingRoomGridDto>>();

            result.ForEach(item => {
                item.RoomStatusName = GetStatusName(item.ReservationItemStatusId);
                item.CanAssociate = item.HouseKeepingStatusId != (int)HousekeepingStatusEnum.Stowage && !item.StartDate.HasValue;
            });

            return result;
        }

        public async Task<HousekeepingRoomGridDto> GetGridUser(string propertyId, Guid? housekeepingRoomId, DateTime systemDate)
        {
            var query = await QueryAsync<HousekeepingRoomGridDto>(@"
                        SELECT 						
                            hr.HousekeepingRoomId,
	                        hr.PropertyId,
	                        hr.OwnerId,
	                        hr.RoomId,
	                        hr.IsStarted,
	                        hr.StartDate,
	                        hr.EndDate,
	                        hr.ElapsedTime,
	
	                        p.FullName,
					        p.PhotoUrl

                        FROM HousekeepingRoom hr    
	                    left  join Users u on u.Id = hr.OwnerId and u.IsDeleted = 0
	                    left  join Person p on p.PersonId = u.PersonId
	                    inner join Room r on r.RoomId = hr.RoomId
	                    left  join ReservationItem ri on ri.RoomId = r.RoomId and ri.ReservationItemStatusId = @ReservationStatusId and ri.IsDeleted = 0 
                        where  hr.HousekeepingRoomId = @HousekeepingRoomId 
                           AND hr.PropertyId = @PropertyId
                           AND hr.IsDeleted = 0
                           AND cast(hr.ServiceDate as date) = cast(@SystemDate as date)
            ", new
            {
                ReservationStatusId = (int)ReservationStatus.Checkin,
                HousekeepingRoomId = housekeepingRoomId,
                PropertyId = propertyId,
                SystemDate = systemDate
            });

            var result = query.MapTo<List<HousekeepingRoomGridDto>>();

            result.ForEach(item => {
                item.RoomStatusName = GetStatusName(item.ReservationItemStatusId);
            });

            return result.FirstOrDefault();
        }

        public async Task<ICollection<HousekeepingRoomDto>> GetAllByFiltersAsync(int propertyId, DateTime startDate, DateTime endDate, List<int> roomIdList)
        {
            var result = await QueryAsync<HousekeepingRoomDto>(@"
                        SELECT 						
                            hr.HousekeepingRoomId as Id,
                            hsp.HouseKeepingStatusId,
                            hr.StartDate,
	                        hr.RoomId

                        FROM HousekeepingRoom hr    
	                    INNER JOIN Room r ON r.RoomId = hr.RoomId
                        INNER JOIN HousekeepingStatusProperty hsp ON r.HousekeepingStatusPropertyId = hsp.HousekeepingStatusPropertyId
                        
                        WHERE hr.PropertyId = @PropertyId
                        AND hr.IsDeleted = 0
                        AND cast(hr.ServiceDate as date) between cast(@StartDate as date) and cast(@EndDate as date)
                        AND r.RoomId in @RoomIdList",
            new
            {
                StartDate = startDate,
                EndDate = endDate,
                PropertyId = propertyId,
                RoomIdList = roomIdList
            });

            return result.ToList();
        }

        private async Task<List<HousekeepingRoomNonAssocsDto>> GetAllByFiltersAsync(int propertyId, Guid? tenantId, DateTime systemDate, string filters)
        {
            var query = $@"
                select distinct r.RoomId as Id,
	                   ri.ReservationItemStatusId,
	                   ri.CheckInDate,
                       ri.CheckOutDate, 
                       r.RoomId,
	                   r.RoomNumber, 
                       r.RoomTypeId,
	                   rt.Abbreviation as RoomTypeAbbreviation,
                       hsp.StatusName as HousekeepingStatusName,
                       hsp.Color as HousekeepingStatusColor,
                       r.floor,                       
                       rt.Abbreviation,
                       hsp.HousekeepingStatusId
                from   Room r
                 inner join RoomType rt on rt.RoomTypeId = r.RoomTypeId and r.IsDeleted = 0
                 left  join ReservationItem ri on ri.RoomId = r.RoomId and ri.ReservationItemStatusId = @ReservationStatusId and ri.IsDeleted = 0
                 inner join HousekeepingStatusProperty hsp on r.HousekeepingStatusPropertyId = hsp.HousekeepingStatusPropertyId               
                 left  join HouseKeepingRoom hk on r.RoomId = hk.RoomId and hk.IsDeleted = 0
                where  r.TenantId = @TenantId
                  and  r.PropertyId = @PropertyId
                  and  r.IsDeleted = 0
                  {(!string.IsNullOrWhiteSpace(filters) ? filters : string.Empty)}";

            var result = await QueryAsync<HousekeepingRoomNonAssocsDto>(query, new
            {
                ReservationStatusId = (int)ReservationStatus.Checkin,
                TenantId = tenantId,
                PropertyId = propertyId,
                SystemDate = systemDate
            });

            return result.ToList();
        }
    }
}
