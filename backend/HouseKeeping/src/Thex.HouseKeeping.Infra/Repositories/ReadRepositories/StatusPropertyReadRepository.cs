﻿using System;
using System.Threading.Tasks;
using Tnf.Dapper.Repositories;
using Tnf.Repositories;
using System.Linq;
using Tnf.Localization;
using Thex.Common;
using Thex.HouseKeeping.Infra.Context;
using Thex.HouseKeeping.Infra.Entities;
using Thex.HouseKeeping.Infra.Interfaces.ReadRepositories;
using Thex.HouseKeeping.Dto.Enumerations;
using Thex.HouseKeeping.Dto;
using System.Collections.Generic;
using Dapper;
using Slapper;
using Thex.Common.Enumerations;
using Thex.GenericLog;

namespace Thex.HouseKeeping.Infra.Repositories.ReadRepositories
{
    public class StatusPropertyReadRepository : DapperEfRepositoryBase<ThexHouseKeepingContext, HousekeepingStatusProperty>, IStatusPropertyReadRepository
    {
        private readonly ILocalizationManager _localizationManager;

        public StatusPropertyReadRepository(
            IActiveTransactionProvider activeTransactionProvider,
            ILocalizationManager localizationManager,
            IGenericLogHandler genericLogHandler)
            : base(activeTransactionProvider, genericLogHandler)
        {
            _localizationManager = localizationManager;
        }

        public async Task<List<HousekeepingStatusPropertyDto>> GetAll(int propertyId)
        {
            var res = await base.GetAllAsync();
            return res.Where(x => x.PropertyId == propertyId).MapTo<List<HousekeepingStatusPropertyDto>>();
        }
    }
}
