﻿using System;
using System.Threading.Tasks;
using Tnf.Dapper.Repositories;
using Tnf.Repositories;
using System.Linq;
using Tnf.Localization;
using Thex.Common;
using Thex.HouseKeeping.Infra.Context;
using Thex.HouseKeeping.Infra.Entities;
using Thex.HouseKeeping.Infra.Interfaces.ReadRepositories;
using Thex.HouseKeeping.Dto.Enumerations;
using Thex.HouseKeeping.Dto;
using System.Collections.Generic;
using Dapper;
using Slapper;
using Thex.Common.Enumerations;
using Thex.GenericLog;

namespace Thex.HouseKeeping.Infra.Repositories.ReadRepositories
{
    public class RoomReadRepository : DapperEfRepositoryBase<ThexHouseKeepingContext, Room>, IRoomReadRepository
    {
        private readonly ILocalizationManager _localizationManager;

        public RoomReadRepository(
            IActiveTransactionProvider activeTransactionProvider,
            ILocalizationManager localizationManager,
            IGenericLogHandler genericLogHandler)
            : base(activeTransactionProvider, genericLogHandler)
        {
            _localizationManager = localizationManager;
        }

        private string GetStatusName(int? statusId)
        {
            if ((int)ReservationStatus.Checkin == statusId) return _localizationManager.GetString(AppConsts.LocalizationSourceName, RoomAvailabilityEnum.Busy.ToString());
            else return _localizationManager.GetString(AppConsts.LocalizationSourceName, RoomAvailabilityEnum.Vague.ToString());
        }

        public async Task<List<RoomDto>> GetByCategory(int id)
        {

            var result = (await QueryAsync<RoomDto>(@"
                select r.RoomId as Id, r.*
                from   Room r
                where  r.RoomCategoryId = @RoomCategoryId
            ", new { RoomCategoryId = id })).ToList();

            return result;
        }

        public async Task<List<RoomDto>> GetAll(string propertyId, Guid tenantId)
        {
            var result = (await QueryAsync<RoomDto>(@"
                select r.RoomId as Id, r.*
                from   Room r
                where  r.PropertyId = @PropertyId
                   and r.TenantId = @TenantId
            ", new {
                PropertyId = propertyId,
                TenantId = tenantId
            })).ToList();

            return result;
        }
    }
}
