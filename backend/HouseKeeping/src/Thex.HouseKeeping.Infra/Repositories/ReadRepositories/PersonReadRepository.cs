﻿using System;
using System.Threading.Tasks;
using Tnf.Dapper.Repositories;
using Tnf.Repositories;
using System.Linq;
using Tnf.Localization;
using Thex.Common;
using Thex.HouseKeeping.Infra.Context;
using Thex.HouseKeeping.Infra.Entities;
using Thex.HouseKeeping.Infra.Interfaces.ReadRepositories;
using Thex.HouseKeeping.Dto.Enumerations;
using Thex.HouseKeeping.Dto;
using System.Collections.Generic;
using Dapper;
using Slapper;
using Thex.Common.Enumerations;
using Thex.GenericLog;

namespace Thex.HouseKeeping.Infra.Repositories.ReadRepositories
{
    public class PersonReadRepository : DapperEfRepositoryBase<ThexHouseKeepingContext, Person>, IPersonReadRepository
    {
        private readonly ILocalizationManager _localizationManager;

        public PersonReadRepository(
            IActiveTransactionProvider activeTransactionProvider,
            ILocalizationManager localizationManager,
            IGenericLogHandler genericLogHandler)
            : base(activeTransactionProvider, genericLogHandler)
        {
            _localizationManager = localizationManager;
        }

        private string GetStatusName(int? statusId)
        {
            if ((int)ReservationStatus.Checkin == statusId) return _localizationManager.GetString(AppConsts.LocalizationSourceName, RoomAvailabilityEnum.Busy.ToString());
            else return _localizationManager.GetString(AppConsts.LocalizationSourceName, RoomAvailabilityEnum.Vague.ToString());
        }

        public async Task<PersonDto> GetById(Guid? id)
        {

            var result = (await QueryAsync<PersonDto>(@"
                select p.PersonId as Id, p.*
                from   Person p
                where  p.PersonId = @Id
            ", new { Id = id })).FirstOrDefault();

            return result;
        }
    }
}
