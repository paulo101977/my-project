using System;
using System.Threading.Tasks;
using Tnf.Dapper.Repositories;
using Tnf.Repositories;
using System.Linq;
using Tnf.Localization;
using Thex.Common;
using Thex.HouseKeeping.Infra.Context;
using Thex.HouseKeeping.Infra.Entities;
using Thex.HouseKeeping.Infra.Interfaces.ReadRepositories;
using Thex.HouseKeeping.Dto;
using System.Collections.Generic;
using Thex.Common.Enumerations;
using Thex.Common.Roles;
using Thex.GenericLog;
using Thex.HouseKeeping.Infra.Interfaces;
using Thex.Kernel;
using System.Dynamic;

namespace Thex.HouseKeeping.Infra.Repositories.ReadRepositories
{
    public class UserReadRepository : DapperEfRepositoryBase<ThexHouseKeepingContext, User>, IUserReadRepository
    {
        private readonly ILocalizationManager _localizationManager;
        private readonly IApplicationUser _applicationUser;

        public UserReadRepository(
            IActiveTransactionProvider activeTransactionProvider,
            ILocalizationManager localizationManager,
            IApplicationUser applicationUser,
            IGenericLogHandler genericLogHandler)
            : base(activeTransactionProvider, genericLogHandler)
        {
            _localizationManager = localizationManager;
            _applicationUser = applicationUser;
        }

        public async Task<List<HousekeepingRoomGridDto>> GetUserRoomByUserId(Guid id, string propertyId) // historico
        {
            var result = (await QueryAsync<HousekeepingRoomGridDto>(@"
                SELECT 						
                hr.HousekeepingRoomId,
                ri.ReservationItemStatusId,
	            hr.PropertyId,
	            hr.OwnerId,
	            hr.RoomId,
	            hr.IsStarted,
	            hr.StartDate,
	            hr.EndDate,
	            hr.ElapsedTime,
                r.RoomNumber, 
                rt.Abbreviation as RoomTypeAbbreviation,
                hsp.StatusName as HousekeepingStatusName,
                hsp.Color as HousekeepingStatusColor,	
	            p.FullName as Name,
	            p.PhotoUrl

                FROM HousekeepingRoom hr    
	            left  join Users u on u.Id = hr.OwnerId and u.IsDeleted = 0
	            left  join Person p on p.PersonId = u.PersonId
	            inner join Room r on r.RoomId = hr.RoomId
                inner join RoomType rt on rt.RoomTypeId = r.RoomTypeId
                inner join HousekeepingStatusProperty hsp on r.HousekeepingStatusPropertyId = hsp.HousekeepingStatusPropertyId
	            left  join ReservationItem ri on ri.RoomId = r.RoomId and ri.ReservationItemStatusId = @ReservationStatusId and ri.IsDeleted = 0 
                where  hr.PropertyId = @PropertyId 
		            AND hr.OwnerId = @OwnerId
                    AND hr.IsDeleted = 0
                    AND hr.EndDate is not null
            ", new
            {
                ReservationStatusId = (int)ReservationStatus.Checkin,
                PropertyId = propertyId,
                OwnerId = id
            })).ToList();

            result.ForEach(item =>
            {
                item.RoomStatusName = GetStatusName(item.ReservationItemStatusId);
            });


            return result;
        }

        public async Task<List<HousekeepingRoomGridDto>> GetUserRoomHistory(Guid tenantId, DateTime systemDate, bool isFilterSystemDate = false) // historico direita
        {
            dynamic _params = new ExpandoObject();

            _params.tenantId = tenantId;
            _params.roleId = Roles.PMSRoomMaid;

            string sql = (@"select u.Id as UserId, upper(substring(p.fullName,1,1))+ lower(substring (p.fullName,2,DATALENGTH(p.fullName))) as name, p.PhotoUrl, count(UH.RoomId) as Total
                from Users u
                left
                join (
                    select RoomId, OwnerId, CreationTime, ServiceDate, IsDeleted
                    from   HousekeepingRoom ");

            if (isFilterSystemDate)
            {
                sql += " where Cast(serviceDate as Date) = @systemDate and ";
                _params.systemDate = systemDate.Date;
            }

            sql += (@"IsDeleted = 0 group by RoomId, OwnerId, CreationTime, ServiceDate, IsDeleted) as UH ON UH.OwnerId = u.Id
                    left join Person p on p.PersonId = u.PersonId                   
                    join(
					select distinct userId
					from UserRoles
					where RoleId = @roleId
					) as ur on ur.UserId = u.Id
                    join UserTenant ut on ut.UserId = u.Id
                    where 
                    u.TenantId = @tenantId                     
                    and ut.IsActive = 1 and ut.IsDeleted = 0
                    group by u.Id, p.PhotoUrl, p.fullName");


            var result = await QueryAsync<HousekeepingRoomGridDto>(sql, _params);

            return result;
        }

        public async Task<UserDto> GetById(Guid id)
        {

            var query = (await QueryAsync<UserDto>(@"
                select
                    u.*,
                    p.DateOfBirth,
                    p.PhotoUrl
                From   Users u
                left   join Person p on u.PersonId = p.PersonId
                where  u.Id =  @Id
            ", new { Id = id }));

            var result = query.FirstOrDefault();

            return result;
        }

        public new async Task<List<UserDto>> GetAll(Guid tenantId)
        {
            var result = (await QueryAsync<UserDto>(@"
                select u.Id as Id, u.*
                from   Users u
                where  u.TenantId = @TenantId
            ", new { TenantId = tenantId })).ToList();

            return result;
        }

        private string GetStatusName(int? statusId)
        {
            if ((int)ReservationStatus.Checkin == statusId) return _localizationManager.GetString(AppConsts.LocalizationSourceName, RoomAvailabilityEnum.Busy.ToString());
            else return _localizationManager.GetString(AppConsts.LocalizationSourceName, RoomAvailabilityEnum.Vague.ToString());
        }

        public IEnumerable<UserDto> GetByListId(List<Guid> listId)
        {
            var query = (Query<UserDto>(@"
                select
                    u.*,
                    p.DateOfBirth,
                    p.PhotoUrl
                From   Users u
                left   join Person p on u.PersonId = p.PersonId
                where  u.Id IN @listId
            ", new { listId }));

            return query;

        }

        public async Task<List<HousekeepingRoomGridDto>> GetRoomByUserId(Guid id, int propertyId, DateTime systemDate)
        {
            var result = (await QueryAsync<HousekeepingRoomGridDto>(@"
                SELECT 						
                hr.HousekeepingRoomId,
                ri.ReservationItemStatusId,
	            hr.PropertyId,
	            hr.OwnerId,
	            hr.RoomId,
	            hr.IsStarted,
	            hr.StartDate,
	            hr.EndDate,
	            hr.ElapsedTime,
                r.RoomNumber, 
                rt.Abbreviation as RoomTypeAbbreviation,
                hsp.StatusName as HousekeepingStatusName,
                hsp.Color as HousekeepingStatusColor,	
	            p.FullName as Name,
	            p.PhotoUrl,
                ri.creationTime

                FROM HousekeepingRoom hr    
	            left  join Users u on u.Id = hr.OwnerId and u.IsDeleted = 0
	            left  join Person p on p.PersonId = u.PersonId
	            inner join Room r on r.RoomId = hr.RoomId
                inner join RoomType rt on rt.RoomTypeId = r.RoomTypeId
                inner join HousekeepingStatusProperty hsp on r.HousekeepingStatusPropertyId = hsp.HousekeepingStatusPropertyId
	           left  join ReservationItem ri on ri.RoomId = r.RoomId and ri.ReservationItemStatusId = @ReservationStatusId and ri.IsDeleted = 0 
                where  hr.PropertyId = @PropertyId 
		            AND hr.OwnerId = @OwnerId
                    AND CAST(hr.ServiceDate as DATE) = @SystemDate
                    AND hr.IsDeleted = 0                  
                    AND hr.EndDate is null
                    
            ", new
            {
                ReservationStatusId = (int)ReservationStatus.Checkin,
                PropertyId = propertyId,
                OwnerId = id,
                SystemDate = systemDate.Date
            })).ToList();

            result.ForEach(item =>
            {
                item.RoomStatusName = GetStatusName(item.ReservationItemStatusId);
            });


            return result;
        }
    }
}
