﻿using System.Threading.Tasks;
using Tnf.Dapper.Repositories;
using Tnf.Repositories;
using Tnf.Localization;
using Thex.HouseKeeping.Infra.Context;
using Thex.HouseKeeping.Infra.Entities;
using Thex.HouseKeeping.Infra.Interfaces;
using Thex.Kernel;
using Thex.GenericLog;

namespace Thex.HouseKeeping.Infra.Repositories
{
    public class HousekeepingRoomStopRepository : DapperEfRepositoryBase<ThexHouseKeepingContext, HousekeepingRoomStop>, IHousekeepingRoomStopRepository
    {
        private readonly ILocalizationManager _localizationManager;
        private readonly IApplicationUser _applicationUser;

        public HousekeepingRoomStopRepository(
            IActiveTransactionProvider activeTransactionProvider,
            ILocalizationManager localizationManager,
            IApplicationUser applicationUser,
            IGenericLogHandler genericLogHandler)
            : base(activeTransactionProvider, genericLogHandler)
        {
            _localizationManager = localizationManager;
            _applicationUser = applicationUser;
        }

        public async Task Add(HousekeepingRoomStop obj)
        {
            await InsertAsync(obj);
        }
    }
}
