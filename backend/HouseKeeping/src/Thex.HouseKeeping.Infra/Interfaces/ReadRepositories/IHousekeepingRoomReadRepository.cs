﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.HouseKeeping.Dto;
using Thex.HouseKeeping.Infra.Entities;

namespace Thex.HouseKeeping.Infra.Interfaces.ReadRepositories
{
    public interface IHousekeepingRoomReadRepository
    {
        Task<HousekeepingRoom> GetByIdASync(Guid id);
        Task<HousekeepingRoomDto> GetDtoByIdAsync(Guid? id);
        Task<HousekeepingRoomGridDto> GetGridUser(string propertyId, Guid? housekeepingRoomId, DateTime systemDate);
        Task<List<HousekeepingRoomDto>> GetAll(string propertyId, Guid? ownerId, Guid? tenantId, DateTime systemDate);
        Task<List<HousekeepingRoomGridDto>> GetAllGrid(string propertyId, Guid? tenantId, DateTime systemDate);
        Task<List<HousekeepingRoomNonAssocsDto>> GetAllNonAssocs(string propertyId, Guid? tenantId, DateTime systemDate);
        Task<ICollection<HousekeepingRoomDto>> GetAllByFiltersAsync(int propertyId, DateTime startDate, DateTime endDate, List<int> roomIdList);
        Task<List<HousekeepingRoomNonAssocsDto>> GetAllAvailableToAssociateAsync(string propertyId, Guid? tenantId, DateTime systemDate);
    }
}
