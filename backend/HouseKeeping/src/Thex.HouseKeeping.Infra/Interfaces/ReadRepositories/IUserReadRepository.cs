﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.HouseKeeping.Dto;
using Thex.HouseKeeping.Infra.Entities;

namespace Thex.HouseKeeping.Infra.Interfaces.ReadRepositories
{
    public interface IUserReadRepository
    {
        Task<List<UserDto>> GetAll(Guid tenantId);
        Task<List<HousekeepingRoomGridDto>> GetUserRoomByUserId(Guid id, string propertyId);
        Task<List<HousekeepingRoomGridDto>> GetUserRoomHistory(Guid propertyId, DateTime systemDate, bool isFilterSystemDate);
        Task<UserDto> GetById(Guid id);
        IEnumerable<UserDto> GetByListId(List<Guid> listId);
        Task<List<HousekeepingRoomGridDto>> GetRoomByUserId(Guid id, int propertyId, DateTime systemDate);
    }
}
