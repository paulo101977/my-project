﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.HouseKeeping.Dto;
using Thex.HouseKeeping.Infra.Entities;

namespace Thex.HouseKeeping.Infra.Interfaces.ReadRepositories
{
    public interface IRoomReadRepository
    {
        Task<List<RoomDto>> GetAll(string propertyId, Guid tenantId);
    }
}
