﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.HouseKeeping.Dto;
using Thex.HouseKeeping.Infra.Entities;
using Tnf.Dto;

namespace Thex.HouseKeeping.Infra.Interfaces
{
    public interface IHousekeepingRoomRepository
    {
        Task CreateItem(HousekeepingRoom obj);
        Task UpdateItem(Guid id, HousekeepingRoom obj);
        Task CreateRangeItem(List<HousekeepingRoom> obj);
        Task RemoveAsync(HousekeepingRoom obj);
        Task RemoveRangeAsync(List<Guid> houseKeepingRoomIdList);
    }
}
