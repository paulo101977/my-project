﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.HouseKeeping.Dto;
using Thex.HouseKeeping.Infra.Entities;
using Tnf.Dto;

namespace Thex.HouseKeeping.Infra.Interfaces
{
    public interface IHousekeepingRoomDisagreementRepository
    {
        Task Add(HousekeepingRoomDisagreement obj);
    }
}
