﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.HouseKeeping.Dto;
using Thex.HouseKeeping.Infra.Entities;
using Tnf.Dto;

namespace Thex.HouseKeeping.Infra.Interfaces
{
    public interface IPersonRepository
    {
        Task UpdateItem(Guid id, Person obj);
    }
}
