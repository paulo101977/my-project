﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Thex.HouseKeeping.Infra.Entities;

namespace Thex.HouseKeeping.Infra.Interfaces
{
    public interface IPropertyParameterReadRepository
    {
        Task<DateTime> GetSystemDate(int propertyId);
        Task<PropertyParameter> GetByPropertyIdAndApplicationParameterId(int propertyId, int applicationParameterId);
    }
}
