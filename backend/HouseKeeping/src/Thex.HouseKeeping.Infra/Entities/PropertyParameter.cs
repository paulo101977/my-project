﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Thex.HouseKeeping.Infra.Entities
{
    public partial class PropertyParameter : BaseEntity
    {
        public int ApplicationParameterId { get; internal set; }
        public string PropertyParameterValue { get; internal set; }
        public string PropertyParameterMinValue { get; internal set; }
        public string PropertyParameterMaxValue { get; internal set; }
        public string PropertyParameterPossibleValues { get; internal set; }

        public enum EntityError
        {
            PropertyParameterMustHavePropertyId,
            PropertyParameterMustHaveApplicationParameterId,
            PropertyParameterOutOfBoundPropertyParameterMinValue,
            PropertyParameterOutOfBoundPropertyParameterMaxValue,
            PropertyParameterHourFormatInvalid,
            PropertyParameterCheckoutLargerCheckin,
            PropertyParameterAgeRangeInvalidad,
            PropertyParameterIsNotDeactivateChildren_2,
            PropertyParameterMustHaveDailyBillingItem,
            PropertyParameterMustHaveTimeZone
        }
    }
}
