﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a Crudzilla tool.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using Tnf.Notifications;

namespace Thex.HouseKeeping.Infra.Entities
{
    public partial class HousekeepingRoom : ThexFullAuditedEntity, IEntityGuid
    {
        public HousekeepingRoom()
        {
            HousekeepingRoomDisagreementList = new HashSet<HousekeepingRoomDisagreement>();
            HousekeepingRoomReviewList = new HashSet<HousekeepingRoomReview>();
            HousekeepingRoomStopList = new HashSet<HousekeepingRoomStop>();
        }

        public Guid Id { get; set; } = Guid.NewGuid();
        public int PropertyId { get; set; }
        public Guid OwnerId { get; set; }
        public int RoomId { get; set; }
        public bool? IsStarted { get; internal set; }
        public DateTime? StartDate { get; internal set; }
        public DateTime? EndDate { get; internal set; }
        public long? ElapsedTime { get; internal set; } = 2400;
        public new DateTime? ServiceDate { get; internal set; }

        public virtual Property Property { get; internal set; }
        public virtual ICollection<HousekeepingRoomDisagreement> HousekeepingRoomDisagreementList { get; internal set; }
        public virtual ICollection<HousekeepingRoomReview> HousekeepingRoomReviewList { get; internal set; }
        public virtual ICollection<HousekeepingRoomStop> HousekeepingRoomStopList { get; internal set; }
        public virtual ICollection<BedType> BedTypeList { get; internal set; }

        public enum EntityError
        {
            HousekeepingRoomMustHavePropertyId,
            HousekeepingRoomInvalidStartDate,
            HousekeepingRoomInvalidEndDate,
            HousekeepingRoomOutOfBoundNotes,
            HouseKeepingRoomCanNotBeAssociated,
            AnyHouseKeepingRoomsCanNotBeAssociated
        }

        public static Builder Create(INotificationHandler handler)
            => new Builder(handler);

        public static Builder Create(INotificationHandler handler, HousekeepingRoom instance)
            => new Builder(handler, instance);
    }
}
