﻿using System;
using Thex.Dapper.Auditing;
using Tnf.Repositories.Entities;
using Tnf.Repositories.Entities.Auditing;

namespace Thex.HouseKeeping.Infra.Entities
{
    [Serializable]
    public abstract class ThexFullAuditedEntity : IModificationThexAudited, ICreationThexAudited,
                                  IHasCreationTime, ISoftDelete, IHasDeletionTime, IDeletionThexAudited
    {
        public bool IsDeleted { get; set; }

        public DateTime CreationTime { get; set; }

        public Guid CreatorUserId { get; set; }

        public DateTime? LastModificationTime { get; set; }

        public Guid? LastModifierUserId { get; set; }

        public DateTime? DeletionTime { get; set; }

        public DateTime? ServiceDate { get; set; }

        public Guid? DeleterUserId { get; set; }
    }
}
