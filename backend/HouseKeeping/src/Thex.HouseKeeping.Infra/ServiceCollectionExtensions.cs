﻿using Thex.Common;
using Thex.HouseKeeping.Infra.Context;
using Thex.HouseKeeping.Infra.Interfaces;
using Thex.HouseKeeping.Infra.Interfaces.ReadRepositories;
using Thex.HouseKeeping.Infra.Mappers.DapperMappers;
using Thex.HouseKeeping.Infra.Mappers.Profiles;
using Thex.HouseKeeping.Infra.Repositories;
using Thex.HouseKeeping.Infra.Repositories.ReadRepositories;
using Tnf.Dapper;

namespace Microsoft.Extensions.DependencyInjection
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddInfraDependency(this IServiceCollection services)
        {
            // Configura o uso do Dapper registrando os contextos que serão
            // usados pela aplicação
            services
                .AddCommonDependency()
                .AddTnfEntityFrameworkCore()
                .AddTnfDbContext<ThexHouseKeepingContext>(config => DbContextConfigurer.Configure(config))
                .AddTnfDapper(options =>
                {
                    //options.MapperAssemblies.Add(typeof(TransportationTypeMapper).Assembly);
                    options.DbType = DapperDbType.SqlServer;
                });

            services.AddTnfAutoMapper(config =>
            {
                config.AddProfile<HousekeepingRoomProfile>();
            });

            services.AddTransient<IHousekeepingRoomReadRepository, HousekeepingRoomReadRepository>();
            services.AddTransient<IReasonReadRepository, ReasonReadRepository>();
            services.AddTransient<IHousekeepingRoomStopRepository, HousekeepingRoomStopRepository>();
            services.AddTransient<IHousekeepingRoomRepository, HousekeepingRoomRepository>();
            services.AddTransient<IHousekeepingRoomReviewRepository, HousekeepingRoomReviewRepository>();
            services.AddTransient<IHousekeepingRoomStopRepository, HousekeepingRoomStopRepository>();
            services.AddTransient<IHousekeepingRoomDisagreementRepository, HousekeepingRoomDisagreementRepository>(); 
            services.AddTransient<IStatusPropertyReadRepository, StatusPropertyReadRepository>(); 
            services.AddTransient<IRoomReadRepository, RoomReadRepository>();
            services.AddTransient<IPersonReadRepository, PersonReadRepository>();
            services.AddTransient<IUserReadRepository, UserReadRepository>();
            services.AddTransient<IPersonRepository, PersonRepository>();
            services.AddTransient<IUserRepository, UserRepository>();
            services.AddTransient<IPropertyParameterReadRepository, PropertyParameterReadRepository>();
            

            return services;
        }
    }
}
