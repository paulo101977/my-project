﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a Crudzilla tool.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using Tnf.Builder;
using Tnf.Notifications;
using Tnf.Specifications;
using Thex.HouseKeeping.Infra.Entities;
using Thex.HouseKeeping.Infra;
using Thex.Common;

namespace Thex.HouseKeeping.Infra.Entities
{
    public partial class HousekeepingRoomStop
    {
        public class Builder : Builder<HousekeepingRoomStop>
        {
            public Builder(INotificationHandler handler) : base(handler)
            {
            }
            public Builder(INotificationHandler handler, HousekeepingRoomStop instance) : base(handler, instance)
            {
            }

            public virtual Builder WithId(Guid id)
            {
                Instance.Id = id;
                return this;
            }
            public virtual Builder WithPropertyId(int propertyId)
            {
                Instance.PropertyId = propertyId;
                return this;
            }
            public virtual Builder WithHousekeepingRoomId(Guid housekeepingRoomId)
            {
                Instance.HousekeepingRoomId = housekeepingRoomId;
                return this;
            }
            public virtual Builder WithReasonId(int reasonId)
            {
                Instance.ReasonId = reasonId;
                return this;
            }
            public virtual Builder WithReason(string reason)
            {
                Instance.Reason = reason;
                return this;
            }
            public virtual Builder WithIsDeleted(bool isDeleted)
            {
                Instance.IsDeleted = isDeleted;
                return this;
            }
            public virtual Builder WithCreationTime(DateTime creationTime)
            {
                if (creationTime == DateTime.MinValue) return this;
                Instance.CreationTime = creationTime;
                return this;
            }
            public virtual Builder WithCreatorUserId(Guid creatorUserId)
            {
                if (creatorUserId == Guid.Empty) return this;
                Instance.CreatorUserId = creatorUserId;
                return this;
            }
            public virtual Builder WithLastModificationTime(DateTime? lastModificationTime)
            {
                if (lastModificationTime == DateTime.MinValue) return this;
                Instance.LastModificationTime = lastModificationTime;
                return this;
            }
            public virtual Builder WithLastModifierUserId(Guid? lastModifierUserId)
            {
                if (lastModifierUserId == Guid.Empty) return this;
                Instance.LastModifierUserId = lastModifierUserId;
                return this;
            }
            public virtual Builder WithDeletionTime(DateTime? deletionTime)
            {
                if (deletionTime == DateTime.MinValue) return this;
                Instance.DeletionTime = deletionTime;
                return this;
            }
            public virtual Builder WithDeleterUserId(Guid? deleterUserId)
            {
                if (deleterUserId == Guid.Empty) return this;
                Instance.DeleterUserId = deleterUserId;
                return this;
            }

            protected override void Specifications()
            {
                AddSpecification(new ExpressionSpecification<HousekeepingRoomStop>(
				AppConsts.LocalizationSourceName, 
				HousekeepingRoomStop.EntityError.HousekeepingRoomStopMustHavePropertyId, 
				w => w.PropertyId != default(int)));

                AddSpecification(new ExpressionSpecification<HousekeepingRoomStop>(
				AppConsts.LocalizationSourceName, 
				HousekeepingRoomStop.EntityError.HousekeepingRoomStopMustHaveHousekeepingRoomId, 
				w => w.HousekeepingRoomId != default(Guid)));

                AddSpecification(new ExpressionSpecification<HousekeepingRoomStop>(
				AppConsts.LocalizationSourceName, 
				HousekeepingRoomStop.EntityError.HousekeepingRoomStopOutOfBoundReason, 
				w => string.IsNullOrWhiteSpace(w.Reason) || w.Reason.Length > 0 && w.Reason.Length <= 4000));

            }
        }
    }
}
