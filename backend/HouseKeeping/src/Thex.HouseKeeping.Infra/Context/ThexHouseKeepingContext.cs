﻿using Microsoft.EntityFrameworkCore;
using Thex.HouseKeeping.Infra.Entities;
using Tnf.EntityFrameworkCore;
using Tnf.Runtime.Session;

namespace Thex.HouseKeeping.Infra.Context
{
    public class ThexHouseKeepingContext : TnfDbContext
    {
        // Importante o construtor do contexto receber as opções com o tipo generico definido: DbContextOptions<TDbContext>
        public ThexHouseKeepingContext(DbContextOptions<ThexHouseKeepingContext> options, ITnfSession session)
            : base(options, session)
        {
        }
    }
}
