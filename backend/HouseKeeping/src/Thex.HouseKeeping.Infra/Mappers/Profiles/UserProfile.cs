﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a Crudzilla tool.
// </auto-generated>
//------------------------------------------------------------------------------

using AutoMapper;
using Thex.HouseKeeping.Infra.Entities;
using Thex.HouseKeeping.Dto;

namespace Thex.HouseKeeping.Infra
{
    public class UserProfile : Profile
    {
        public UserProfile()
        {
            CreateMap<User, UserDto>();
        }

    }
}
