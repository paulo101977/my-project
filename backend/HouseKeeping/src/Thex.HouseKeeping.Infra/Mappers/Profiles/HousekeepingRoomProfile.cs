﻿using AutoMapper;
using Thex.HouseKeeping.Dto;
using Thex.HouseKeeping.Infra.Entities;

namespace Thex.HouseKeeping.Infra.Mappers.Profiles
{
    public class HousekeepingRoomProfile : Profile
    {
        public HousekeepingRoomProfile()
        {
            CreateMap<HousekeepingRoom, HousekeepingRoomDto>();
        }
    }
}