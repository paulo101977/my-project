﻿using Shouldly;
using System;
using Tnf.AspNetCore.TestBase;
using Tnf.Notifications;
using Xunit;
using Microsoft.Extensions.DependencyInjection;
using Thex.HouseKeeping.Infra.Interfaces.ReadRepositories;
using Thex.HouseKeeping.Infra.Interfaces;
using Thex.HouseKeeping.Application.Adapters;
using Tnf.Repositories.Uow;
using Thex.HouseKeeping.Application.Services;
using Thex.HouseKeeping.Mobile.Tests.Mocks.Dto;
using Thex.Kernel;

namespace Thex.HouseKeeping.Mobile.Tests.Application
{
    public class HousekeepingRoomAppServiceTests : TnfAspNetCoreIntegratedTestBase<StartupUnitTest>
    {
        public INotificationHandler notificationHandler;

        public HousekeepingRoomAppServiceTests()
        {
            notificationHandler = new NotificationHandler(ServiceProvider);
        }

        [Fact]
        public void Should_Resolve_All()
        {
            TnfSession.ShouldNotBeNull();
            ServiceProvider.GetService<INotificationHandler>().ShouldNotBeNull();
            ServiceProvider.GetService<IHousekeepingRoomRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<IHousekeepingRoomReadRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<IHousekeepingRoomAdapter>().ShouldNotBeNull();
            ServiceProvider.GetService<IApplicationUser>().ShouldNotBeNull();
            ServiceProvider.GetService<IUnitOfWorkManager>().ShouldNotBeNull();
        }

        [Fact]
        public void Should_Create()
        {
            var housekeepingRoomReadRepository = ServiceProvider.GetService<IHousekeepingRoomReadRepository>();
            var housekeepingRoomRepository = ServiceProvider.GetService<IHousekeepingRoomRepository>();
            var housekeepingRoomAdapter = ServiceProvider.GetService<IHousekeepingRoomAdapter>();
            var applicationUser = ServiceProvider.GetService<IApplicationUser>();
            var unitOfWork = ServiceProvider.GetService<IUnitOfWorkManager>();
            var propertyParameterReadRepository = ServiceProvider.GetService<IPropertyParameterReadRepository>();


            var dto = new HousekeepingRoomAppService(housekeepingRoomReadRepository,
                                                    housekeepingRoomRepository,
                                                    housekeepingRoomAdapter,
                                                    unitOfWork,
                                                    propertyParameterReadRepository,
                                                    applicationUser,
                                                    notificationHandler)
                                                    .CreateAsync(HousekeepingRoomDtoMock.GetDto());

            Assert.False(notificationHandler.HasNotification());
        }

        [Fact]
        public void Should_Update()
        {
            var housekeepingRoomReadRepository = ServiceProvider.GetService<IHousekeepingRoomReadRepository>();
            var housekeepingRoomRepository = ServiceProvider.GetService<IHousekeepingRoomRepository>();
            var housekeepingRoomAdapter = ServiceProvider.GetService<IHousekeepingRoomAdapter>();
            var applicationUser = ServiceProvider.GetService<IApplicationUser>();
            var unitOfWork = ServiceProvider.GetService<IUnitOfWorkManager>();
            var propertyParameterReadRepository = ServiceProvider.GetService<IPropertyParameterReadRepository>();


            var dto = new HousekeepingRoomAppService(housekeepingRoomReadRepository,
                                                    housekeepingRoomRepository,
                                                    housekeepingRoomAdapter,
                                                    unitOfWork,
                                                    propertyParameterReadRepository,
                                                    applicationUser,
                                                    notificationHandler)
                                                    .UpdateAsync(Guid.NewGuid(), HousekeepingRoomDtoMock.GetDto());

            Assert.False(notificationHandler.HasNotification());
        }
    }
}
