﻿using Shouldly;
using System;
using Tnf.AspNetCore.TestBase;
using Tnf.Notifications;
using Xunit;
using Microsoft.Extensions.DependencyInjection;
using Thex.HouseKeeping.Infra.Interfaces.ReadRepositories;
using Thex.HouseKeeping.Infra.Interfaces;
using Thex.HouseKeeping.Application.Adapters;
using Tnf.Repositories.Uow;
using Thex.HouseKeeping.Application.Services;
using Thex.HouseKeeping.Mobile.Tests.Mocks.Dto;
using Thex.Kernel;

namespace Thex.HouseKeeping.Mobile.Tests.Application
{
    public class HousekeepingRoomDisagreementAppServiceTests : TnfAspNetCoreIntegratedTestBase<StartupUnitTest>
    {
        public INotificationHandler notificationHandler;

        public HousekeepingRoomDisagreementAppServiceTests()
        {
            notificationHandler = new NotificationHandler(ServiceProvider);
        }

        [Fact]
        public void Should_Resolve_All()
        {
            TnfSession.ShouldNotBeNull();
            ServiceProvider.GetService<INotificationHandler>().ShouldNotBeNull();
            ServiceProvider.GetService<IHousekeepingRoomDisagreementRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<IHousekeepingRoomDisagreementAdapter>().ShouldNotBeNull();
            ServiceProvider.GetService<IApplicationUser>().ShouldNotBeNull();
            ServiceProvider.GetService<IUnitOfWorkManager>().ShouldNotBeNull();
        }

        [Fact]
        public void Should_Create()
        {
            var housekeepingRoomDisagreementRepository = ServiceProvider.GetService<IHousekeepingRoomDisagreementRepository>();
            var housekeepingRoomDisagreementAdapter = ServiceProvider.GetService<IHousekeepingRoomDisagreementAdapter>();
            var applicationUser = ServiceProvider.GetService<IApplicationUser>();
            var unitOfWork = ServiceProvider.GetService<IUnitOfWorkManager>();

            var dto = new HousekeepingRoomDisagreementAppService(housekeepingRoomDisagreementRepository,
                                                    housekeepingRoomDisagreementAdapter,
                                                    applicationUser,
                                                    notificationHandler,
                                                    unitOfWork)
                                                    .CreateAsync(HousekeepingRoomDisagreementDtoMock.GetDto());

            Assert.False(notificationHandler.HasNotification());
        }
    }
}
