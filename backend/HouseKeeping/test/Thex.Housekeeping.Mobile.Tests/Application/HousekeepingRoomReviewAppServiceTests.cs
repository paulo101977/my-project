﻿using Shouldly;
using System;
using Tnf.AspNetCore.TestBase;
using Tnf.Notifications;
using Xunit;
using Microsoft.Extensions.DependencyInjection;
using Thex.HouseKeeping.Infra.Interfaces.ReadRepositories;
using Thex.HouseKeeping.Infra.Interfaces;
using Thex.HouseKeeping.Application.Adapters;
using Tnf.Repositories.Uow;
using Thex.HouseKeeping.Application.Services;
using Thex.HouseKeeping.Mobile.Tests.Mocks.Dto;
using Thex.Kernel;

namespace Thex.HouseKeeping.Mobile.Tests.Application
{
    public class HousekeepingRoomReviewAppServiceTests : TnfAspNetCoreIntegratedTestBase<StartupUnitTest>
    {
        public INotificationHandler notificationHandler;

        public HousekeepingRoomReviewAppServiceTests()
        {
            notificationHandler = new NotificationHandler(ServiceProvider);
        }

        [Fact]
        public void Should_Resolve_All()
        {
            TnfSession.ShouldNotBeNull();
            ServiceProvider.GetService<INotificationHandler>().ShouldNotBeNull();
            ServiceProvider.GetService<IHousekeepingRoomReviewRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<IHousekeepingRoomReviewAdapter>().ShouldNotBeNull();
            ServiceProvider.GetService<IApplicationUser>().ShouldNotBeNull();
            ServiceProvider.GetService<IUnitOfWorkManager>().ShouldNotBeNull();
        }

        [Fact]
        public void Should_Create()
        {
            var housekeepingRoomReviewRepository = ServiceProvider.GetService<IHousekeepingRoomReviewRepository>();
            var housekeepingRoomReviewAdapter = ServiceProvider.GetService<IHousekeepingRoomReviewAdapter>();
            var applicationUser = ServiceProvider.GetService<IApplicationUser>();
            var unitOfWork = ServiceProvider.GetService<IUnitOfWorkManager>();

            var dto = new HousekeepingRoomReviewAppService(housekeepingRoomReviewRepository,
                                                    housekeepingRoomReviewAdapter,
                                                    applicationUser,
                                                    notificationHandler,
                                                    unitOfWork)
                                                    .CreateAsync(HousekeepingRoomReviewDtoMock.GetDto());

            Assert.False(notificationHandler.HasNotification());
        }
    }
}
