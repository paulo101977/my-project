﻿using Shouldly;
using System;
using Tnf.AspNetCore.TestBase;
using Tnf.Notifications;
using Xunit;
using Microsoft.Extensions.DependencyInjection;
using Thex.HouseKeeping.Infra.Interfaces.ReadRepositories;
using Thex.HouseKeeping.Infra.Interfaces;
using Thex.HouseKeeping.Application.Adapters;
using Tnf.Repositories.Uow;
using Thex.HouseKeeping.Application.Services;
using Thex.HouseKeeping.Mobile.Tests.Mocks.Dto;
using Thex.Kernel;
using NSubstitute;
using Thex.HouseKeeping.Mobile.Tests.Mocks.AppServiceMock;
using Thex.HouseKeeping.Application.Interfaces;
using System.Linq;
using Thex.HouseKeeping.Dto;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.HouseKeeping.Infra.Entities;
using Tnf;

namespace Thex.HouseKeeping.Mobile.Tests.Application
{
    public class HousekeepingGeneratorAppServiceTests : TnfAspNetCoreIntegratedTestBase<StartupUnitTest>
    {
        public INotificationHandler notificationHandler;

        public HousekeepingGeneratorAppServiceTests()
        {
            notificationHandler = new NotificationHandler(ServiceProvider);
        }

        [Fact]
        public void Should_Resolve_All()
        {
            TnfSession.ShouldNotBeNull();
            ServiceProvider.GetService<INotificationHandler>().ShouldNotBeNull();
            ServiceProvider.GetService<IHousekeepingRoomRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<IHousekeepingRoomReadRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<IHousekeepingRoomAdapter>().ShouldNotBeNull();
            ServiceProvider.GetService<IApplicationUser>().ShouldNotBeNull();
            ServiceProvider.GetService<IUnitOfWorkManager>().ShouldNotBeNull();
            ServiceProvider.GetService<IHousekeepingRoomAppService>().ShouldNotBeNull();
            ServiceProvider.GetService<IPropertyParameterReadRepository>().ShouldNotBeNull();
        }

        [Fact]
        public void Should_Generator()
        {
            var housekeepingRoomReadRepository = Substitute.For<IHousekeepingRoomReadRepository>();
            var applicationUser = ServiceProvider.GetService<IApplicationUser>();
            var userReadRepository = Substitute.For<IUserReadRepository>();
            var housekeepingRoomAdapter = Substitute.For<IHousekeepingRoomAdapter>();
            var houseKeepingAppService = Substitute.For<IHousekeepingRoomAppService>();
            var notificationHandler = Substitute.For<INotificationHandler>();
            var propertyParameter = Substitute.For<IPropertyParameterReadRepository>();
            var unitOfWork = Substitute.For<IUnitOfWorkManager>();


            var listUser = HousekeepingGeneratorDtoMock.ListUser();

            userReadRepository.GetByListId(new List<Guid>()).ReturnsForAnyArgs(x =>
            {
                return HousekeepingGeneratorDtoMock.GetByListId();

            });
            housekeepingRoomReadRepository.GetAllAvailableToAssociateAsync(applicationUser.PropertyId, applicationUser.TenantId, DateTime.Now).ReturnsForAnyArgs(x =>
             {
                 return HousekeepingRoomDtoMock.GetListDtoNonAssocs();

             });


            propertyParameter.GetSystemDate(1).ReturnsForAnyArgs(x =>
            {
                return DateTime.Now.AsTask();
            });

            var dto = new HousekeepingGeneratorAppService(housekeepingRoomReadRepository,
                                                    applicationUser,
                                                    notificationHandler,
                                                    userReadRepository,
                                                    houseKeepingAppService,
                                                    housekeepingRoomAdapter,
                                                    unitOfWork,
                                                    propertyParameter)
                                                    .GeneratorAsync(HousekeepingGeneratorDtoMock.ListUser());



            var result = VerifyDto(dto.Result);


            if (notificationHandler.HasNotification())
                Assert.False(notificationHandler.HasNotification());
            else
                Assert.True(result);

        }


        [Fact]
        public void Should_Create()
        {
            var housekeepingRoomReadRepository = Substitute.For<IHousekeepingRoomReadRepository>();
            var applicationUser = ServiceProvider.GetService<IApplicationUser>();
            var userReadRepository = Substitute.For<IUserReadRepository>();
            var housekeepingRoomAdapter = Substitute.For<IHousekeepingRoomAdapter>();
            var notificationHandler = Substitute.For<INotificationHandler>();
            var housekeepingRoomAppService = Substitute.For<IHousekeepingRoomAppService>();
            var housekeepingRoomAdapter2 = ServiceProvider.GetService<IHousekeepingRoomAdapter>();
            var propertyParameter = Substitute.For<IPropertyParameterReadRepository>();
            var unitOfWork = Substitute.For<IUnitOfWorkManager>();


            housekeepingRoomAppService.CreateRangeAsync(new List<HousekeepingRoom>());
            {
                

            };

           
            housekeepingRoomAdapter.Map(new HousekeepingRoomDto()).ReturnsForAnyArgs(x =>
            {
                var dtoHkr = ((HousekeepingRoomDto)x[0]);

                return housekeepingRoomAdapter2.Map(dtoHkr); 
            });


            var dto = new HousekeepingGeneratorAppService(housekeepingRoomReadRepository,
                                                    applicationUser,
                                                    notificationHandler,
                                                    userReadRepository,
                                                    housekeepingRoomAppService,
                                                    housekeepingRoomAdapter,
                                                    unitOfWork,
                                                    propertyParameter)
                                                    .CreateRangeAsync(HousekeepingGeneratorDtoMock.GeneratorResponseDto());

 


            if (notificationHandler.HasNotification())
                Assert.False(notificationHandler.HasNotification());
             
        }





        private bool VerifyDto(HousekeepingGeneratorResponseDto dto)
        {


            if (dto.RoomMaidList[0].ListRoom.Count() != 2)
                return false;
            if (dto.RoomMaidList[1].ListRoom.Count() != 2)
                return false;
            if (dto.RoomMaidList[2].ListRoom.Count() != 2)
                return false;
            if (dto.RoomMaidList[3].ListRoom.Count() != 2)
                return false;
            if (dto.RoomMaidList[4].ListRoom.Count() != 2)
                return false;
            if (dto.RoomMaidList[5].ListRoom.Count() != 2)
                return false;
            if (dto.RoomMaidList[6].ListRoom.Count() != 1)
                return false;
           

            return true;

        }



    


    }
}
