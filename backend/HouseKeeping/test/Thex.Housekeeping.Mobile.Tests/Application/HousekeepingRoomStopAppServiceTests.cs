﻿using Shouldly;
using System;
using Tnf.AspNetCore.TestBase;
using Tnf.Notifications;
using Xunit;
using Microsoft.Extensions.DependencyInjection;
using Thex.HouseKeeping.Infra.Interfaces.ReadRepositories;
using Thex.HouseKeeping.Infra.Interfaces;
using Thex.HouseKeeping.Application.Adapters;
using Tnf.Repositories.Uow;
using Thex.HouseKeeping.Application.Services;
using Thex.HouseKeeping.Mobile.Tests.Mocks.Dto;
using Thex.Kernel;

namespace Thex.HouseKeeping.Mobile.Tests.Application
{
    public class HousekeepingRoomStopAppServiceTests : TnfAspNetCoreIntegratedTestBase<StartupUnitTest>
    {
        public INotificationHandler notificationHandler;

        public HousekeepingRoomStopAppServiceTests()
        {
            notificationHandler = new NotificationHandler(ServiceProvider);
        }

        [Fact]
        public void Should_Resolve_All()
        {
            TnfSession.ShouldNotBeNull();
            ServiceProvider.GetService<INotificationHandler>().ShouldNotBeNull();
            ServiceProvider.GetService<IHousekeepingRoomStopRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<IHousekeepingRoomStopAdapter>().ShouldNotBeNull();
            ServiceProvider.GetService<IApplicationUser>().ShouldNotBeNull();
            ServiceProvider.GetService<IUnitOfWorkManager>().ShouldNotBeNull();
        }

        [Fact]
        public void Should_Create()
        {
            var housekeepingRoomStopRepository = ServiceProvider.GetService<IHousekeepingRoomStopRepository>();
            var housekeepingRoomStopAdapter = ServiceProvider.GetService<IHousekeepingRoomStopAdapter>();
            var applicationUser = ServiceProvider.GetService<IApplicationUser>();
            var unitOfWork = ServiceProvider.GetService<IUnitOfWorkManager>();

            var dto = new HousekeepingRoomStopAppService(housekeepingRoomStopRepository,
                                                    housekeepingRoomStopAdapter,
                                                    applicationUser,
                                                    notificationHandler,
                                                    unitOfWork)
                                                    .CreateAsync(HousekeepingRoomStopDtoMock.GetDto());

            Assert.False(notificationHandler.HasNotification());
        }
    }
}
