﻿
using Shouldly;
using Thex.HouseKeeping.Application.Interfaces;
using Thex.HouseKeeping.Mobile.Controllers;
using Tnf.AspNetCore.TestBase;
using Xunit;
using Microsoft.Extensions.DependencyInjection;
using System.Threading.Tasks;
using Thex.HouseKeeping.Dto;
using Thex.HouseKeeping.Mobile.Tests.Mocks.Dto;
using Tnf.Dto;
using System.Collections.Generic;

namespace Thex.HouseKeeping.Mobile.Tests.Controllers
{
    public class HousekeepingRoomControllerTests : TnfAspNetCoreIntegratedTestBase<StartupControllerTest>
    {
        [Fact]
        public void Should_Resolve_All()
        {
            TnfSession.ShouldNotBeNull();
            ServiceProvider.GetService<HousekeepingRoomController>().ShouldNotBeNull();
            ServiceProvider.GetService<IHousekeepingRoomAppService>().ShouldNotBeNull();
        }

        [Fact]
        public async Task Should_GetAll()
        {
            var response = await GetResponseAsObjectAsync<List<HousekeepingRoomDto>>(
                $"{RouteConsts.HousekeepingRoomRouteName}/getAll");            

            Assert.NotNull(response);
        }

        [Fact]
        public async Task Should_Post()
        {
            var response = await PostResponseAsObjectAsync<HousekeepingRoomDto, HousekeepingRoomDto>(
            $"{RouteConsts.HousekeepingRoomRouteName}", HousekeepingRoomDtoMock.GetDto());

            Assert.NotNull(response);
        }

        [Fact]
        public async Task Should_Put()
        {
            var response = await PutResponseAsObjectAsync<HousekeepingRoomDto, HousekeepingRoomDto>(
            $"{RouteConsts.HousekeepingRoomRouteName}/{"50a66538-dd94-4162-a1ed-83fc8311e574"}", HousekeepingRoomDtoMock.GetDto());

            Assert.NotNull(response);
        }
    }
}
