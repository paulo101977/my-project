﻿
using Shouldly;
using Thex.HouseKeeping.Application.Interfaces;
using Thex.HouseKeeping.Mobile.Controllers;
using Tnf.AspNetCore.TestBase;
using Xunit;
using Microsoft.Extensions.DependencyInjection;
using System.Threading.Tasks;
using Thex.HouseKeeping.Dto;
using Thex.HouseKeeping.Mobile.Tests.Mocks.Dto;
using Tnf.Dto;
using System.Collections.Generic;

namespace Thex.HouseKeeping.Mobile.Tests.Controllers
{
    public class HousekeepingRoomReviewControllerTests : TnfAspNetCoreIntegratedTestBase<StartupControllerTest>
    {
        [Fact]
        public void Should_Resolve_All()
        {
            TnfSession.ShouldNotBeNull();
            ServiceProvider.GetService<HousekeepingRoomReviewController>().ShouldNotBeNull();
            ServiceProvider.GetService<IHousekeepingRoomReviewAppService>().ShouldNotBeNull();
        }

        [Fact]
        public async Task Should_Post()
        {
            var response = await PostResponseAsObjectAsync<HousekeepingRoomReviewDto, HousekeepingRoomReviewDto>(
            $"{RouteConsts.HousekeepingRoomReviewRouteName}", HousekeepingRoomReviewDtoMock.GetDto());

            Assert.Null(response);
        }
    }
}
