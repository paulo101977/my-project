﻿
using Shouldly;
using Thex.HouseKeeping.Application.Interfaces;
using Thex.HouseKeeping.Mobile.Controllers;
using Tnf.AspNetCore.TestBase;
using Xunit;
using Microsoft.Extensions.DependencyInjection;
using System.Threading.Tasks;
using Thex.HouseKeeping.Dto;
using Thex.HouseKeeping.Mobile.Tests.Mocks.Dto;
using Tnf.Dto;
using System.Collections.Generic;

namespace Thex.HouseKeeping.Mobile.Tests.Controllers
{
    public class HousekeepingRoomDisagreementControllerTests : TnfAspNetCoreIntegratedTestBase<StartupControllerTest>
    {
        [Fact]
        public void Should_Resolve_All()
        {
            TnfSession.ShouldNotBeNull();
            ServiceProvider.GetService<HousekeepingRoomDisagreementController>().ShouldNotBeNull();
            ServiceProvider.GetService<IHousekeepingRoomDisagreementAppService>().ShouldNotBeNull();
        }

        [Fact]
        public async Task Should_Post()
        {
            var response = await PostResponseAsObjectAsync<HousekeepingRoomDisagreementDto, HousekeepingRoomDisagreementDto>(
            $"{RouteConsts.HousekeepingRoomDisagreementRouteName}", HousekeepingRoomDisagreementDtoMock.GetDto());

            Assert.Null(response);
        }
    }
}
