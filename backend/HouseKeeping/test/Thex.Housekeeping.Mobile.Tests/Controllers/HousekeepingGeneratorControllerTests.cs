﻿
using Shouldly;
using Thex.HouseKeeping.Application.Interfaces;
using Thex.HouseKeeping.Mobile.Controllers;
using Tnf.AspNetCore.TestBase;
using Xunit;
using Microsoft.Extensions.DependencyInjection;
using System.Threading.Tasks;
using Thex.HouseKeeping.Dto;
using Thex.HouseKeeping.Mobile.Tests.Mocks.Dto;
using Tnf.Dto;
using System.Collections.Generic;

namespace Thex.HouseKeeping.Mobile.Tests.Controllers
{
    public class HousekeepingGeneratorControllerTests : TnfAspNetCoreIntegratedTestBase<StartupControllerTest>
    {
        [Fact]
        public void Should_Resolve_All()
        {
            TnfSession.ShouldNotBeNull();
            ServiceProvider.GetService<HousekeepingGeneratorController>().ShouldNotBeNull();
            ServiceProvider.GetService<IHousekeepingGeneratorAppService>().ShouldNotBeNull();
        }



        [Fact]
        public async Task Should_Generator()
        {
            var response = await PostResponseAsObjectAsync<HousekeepingGeneratorDto, HousekeepingGeneratorDto>(
            $"{RouteConsts.HousekeepingGeneratorRouteName}", HousekeepingGeneratorDtoMock.HousekeepingGeneratorDto());

            Assert.NotNull(response);
        }

        [Fact]
        public async Task Should_Create()
        {

            var response = await PostResponseAsObjectAsync<HousekeepingGeneratorResponseDto, HousekeepingGeneratorResponseDto>(
            $"{RouteConsts.HousekeepingGeneratorRouteName}/{"CreateHouseKeeping"}",
            HousekeepingGeneratorDtoMock.GeneratorResponseDto());

        }
    }
}
