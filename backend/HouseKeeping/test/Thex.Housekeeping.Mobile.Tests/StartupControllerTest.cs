﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using System;
using Thex.Common;
using Thex.HouseKeeping.Application.Interfaces;
using Thex.HouseKeeping.Mobile.Tests.Mocks.AppServiceMock;
using Thex.Kernel;

namespace Thex.HouseKeeping.Mobile.Tests
{
    public class StartupControllerTest
    {
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            services.AddTnfAspNetCoreSetupTest();
            services.AddTransient<IHousekeepingRoomAppService, HousekeepingRoomAppServiceMock>();
            services.AddTransient<IHousekeepingRoomStopAppService, HousekeepingRoomStopAppServiceMock>();
            services.AddTransient<IHousekeepingRoomDisagreementAppService, HousekeepingRoomDisagreementAppServiceMock>();
            services.AddTransient<IHousekeepingRoomReviewAppService, HousekeepingRoomReviewAppServiceMock>();
            services.AddTransient<IReasonAppService, ReasonAppServiceMock>();
            services.AddTransient<IHousekeepingGeneratorAppService, HousekeepingGeneratorAppServiceMock>();


            services.AddScoped<IApplicationUser>(a => new ApplicationUserMock());
            var serviceProvider = services.BuildServiceProvider();

            ServiceLocator.SetLocatorProvider(serviceProvider);

            return services.BuildServiceProvider();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app)
        {
            // Configura o uso do teste
            app.UseTnfAspNetCoreSetupTest();

            app.UseMvc(routes =>
            {
                routes.MapRoute("default", "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
