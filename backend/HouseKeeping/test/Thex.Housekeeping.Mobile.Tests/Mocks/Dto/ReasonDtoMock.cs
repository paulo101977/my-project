﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.HouseKeeping.Dto;

namespace Thex.HouseKeeping.Mobile.Tests.Mocks.Dto
{
    public class ReasonDtoMock
    {
        public static ReasonDto GetDto(int categoryId = 1)
        {
            return new ReasonDto()
            {
                Id = categoryId,
                ChainId  = 1, 
                ReasonCategoryId = 1,
                ReasonName = "Reason " + DateTime.Now
            };
        }

        public static async Task<ReasonDto> GetDtoAsync()
        {
            var dto = GetDto();
            return await Task.FromResult(dto);
        }

        public static Task<IList<ReasonDto>> GetDtoListAsync(int categoryId = 1)
        {
            var ret = new List<ReasonDto>();

            ret.Add(GetDto(categoryId));
            ret.Add(GetDto(categoryId));
            ret.Add(GetDto(categoryId));

            return Task.FromResult<IList<ReasonDto>>(ret);
        }
    }
}
