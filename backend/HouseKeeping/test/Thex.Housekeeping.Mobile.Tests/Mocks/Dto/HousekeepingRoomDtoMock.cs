﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.Common.Dto;
using Thex.HouseKeeping.Dto;
using Tnf.Dto;

namespace Thex.HouseKeeping.Mobile.Tests.Mocks.Dto
{
    public class HousekeepingRoomDtoMock
    {
        public static HousekeepingRoomDto GetDto()
        {
            return new HousekeepingRoomDto()
            {
                Id = Guid.Parse("23eb803c-726a-4c7c-b25b-2c22a56793d9"),
                RoomId = 1,
                CheckInDate = DateTime.Now,
                CheckOutDate = DateTime.Now,
                CreationTime = DateTime.Now,
                CreatorUserId = Guid.Parse("23eb803c-726a-4c7c-b25b-2c22a56793d9"),
                ElapsedTime = 2,
                OwnerId = Guid.Parse("23eb803c-726a-4c7c-b25b-2c22a56793d9"),
                PropertyId = 1
            };
        }

        public static HousekeepingRoomNonAssocsDto GetDtoNonAssocs()
        {
            return new HousekeepingRoomNonAssocsDto()
            {
                Id = 1,
                RoomId = 1,
                CheckInDate = DateTime.Now,
                CheckOutDate = DateTime.Now,
                CreationTime = DateTime.Now,
                CreatorUserId = Guid.Parse("23eb803c-726a-4c7c-b25b-2c22a56793d9"),
                ElapsedTime = 2,
                OwnerId = Guid.Parse("23eb803c-726a-4c7c-b25b-2c22a56793d9"),
                PropertyId = 1
            };
        }

        public static async Task<HousekeepingRoomDto> GetDtoAsync()
        {
            var dto = GetDto();
            return await Task.FromResult(dto);
        }

        public static async Task<HousekeepingRoomNonAssocsDto> GetDtoNonAssocsAsync()
        {
            var dto = GetDtoNonAssocs();
            return await Task.FromResult(dto);
        }

        public static Task<IList<HousekeepingRoomDto>> GetDtoListAsync()
        {
            var ret = new List<HousekeepingRoomDto>();

            ret.Add(GetDto());
            ret.Add(GetDto());
            ret.Add(GetDto());

            return Task.FromResult<IList<HousekeepingRoomDto>>(ret);
        }

        public static Task<IList<HousekeepingRoomNonAssocsDto>> GetDtoNonAssocsListAsync()
        {
            var ret = new List<HousekeepingRoomNonAssocsDto>();

            ret.Add(GetDtoNonAssocs());
            ret.Add(GetDtoNonAssocs());
            ret.Add(GetDtoNonAssocs());

            return Task.FromResult<IList<HousekeepingRoomNonAssocsDto>>(ret);
        }


        public static List<HousekeepingRoomNonAssocsDto> GetListDtoNonAssocs()
        {

            return new List<HousekeepingRoomNonAssocsDto>
            {
                new HousekeepingRoomNonAssocsDto
                {
                    Id = 1,
                    RoomId = 1,
                    CheckInDate = DateTime.Now,
                    CheckOutDate = DateTime.Now,
                    CreationTime = DateTime.Now,
                    CreatorUserId = Guid.Parse("23eb803c-726a-4c7c-b25b-2c22a56793d9"),
                    ElapsedTime = 2,
                    PropertyId = 1,
                    Floor = "1",
                    RoomTypeAbbreviation = "STD",
                    RoomNumber = "101",
                    HousekeepingStatusId = 2

                },
                 new HousekeepingRoomNonAssocsDto
                {
                    Id = 2,
                    RoomId = 2,                 
                    CreationTime = DateTime.Now,
                    CreatorUserId = Guid.Parse("23eb803c-726a-4c7c-b25b-2c22a56793d9"),
                    ElapsedTime = 2,
                    PropertyId = 1,
                    Floor = "1",
                    RoomTypeAbbreviation = "STD",
                    RoomNumber = "102",
                    HousekeepingStatusId = 2

                },
                  new HousekeepingRoomNonAssocsDto
                {
                    Id = 3,
                    RoomId = 3,                   
                    CreationTime = DateTime.Now,
                    CreatorUserId = Guid.Parse("23eb803c-726a-4c7c-b25b-2c22a56793d9"),
                    ElapsedTime = 2,
                    PropertyId = 1,
                    Floor = "1",
                    RoomTypeAbbreviation = "STD",
                    RoomNumber = "103",
                    HousekeepingStatusId = 2

                },
                   new HousekeepingRoomNonAssocsDto
                {
                    Id = 4,
                    RoomId = 4,                  
                    CreationTime = DateTime.Now,
                    CreatorUserId = Guid.Parse("23eb803c-726a-4c7c-b25b-2c22a56793d9"),
                    ElapsedTime = 2,
                    PropertyId = 1,
                    Floor = "2",
                    RoomTypeAbbreviation = "STD",
                    RoomNumber = "201",
                    HousekeepingStatusId = 2

                },
                    new HousekeepingRoomNonAssocsDto
                {
                    Id = 5,
                    RoomId = 5,                    
                    CreationTime = DateTime.Now,
                    CreatorUserId = Guid.Parse("23eb803c-726a-4c7c-b25b-2c22a56793d9"),
                    ElapsedTime = 2,
                    PropertyId = 1,
                    Floor = "2",
                    RoomTypeAbbreviation = "STD",
                    RoomNumber = "202",
                    HousekeepingStatusId = 2

                },
                     new HousekeepingRoomNonAssocsDto
                {
                    Id = 6,
                    RoomId = 6,                    
                    CreationTime = DateTime.Now,
                    CreatorUserId = Guid.Parse("23eb803c-726a-4c7c-b25b-2c22a56793d9"),
                    ElapsedTime = 2,
                    PropertyId = 1,
                    Floor = "3",
                    RoomTypeAbbreviation = "STD",
                    RoomNumber = "301",
                    HousekeepingStatusId = 2

                },
                      new HousekeepingRoomNonAssocsDto
                {
                    Id = 7,
                    RoomId = 7,
                    CreationTime = DateTime.Now,
                    CreatorUserId = Guid.Parse("23eb803c-726a-4c7c-b25b-2c22a56793d9"),
                    ElapsedTime = 2,
                    PropertyId = 1,
                    Floor = "3",
                    RoomTypeAbbreviation = "STD",
                    RoomNumber = "302",
                    HousekeepingStatusId = 2

                },
                       new HousekeepingRoomNonAssocsDto
                {
                    Id = 8,
                    RoomId = 8,                    
                    CreationTime = DateTime.Now,
                    CreatorUserId = Guid.Parse("23eb803c-726a-4c7c-b25b-2c22a56793d9"),
                    ElapsedTime = 2,
                    PropertyId = 1,
                    Floor = "3",
                    RoomTypeAbbreviation = "STD",
                    RoomNumber = "303",
                    HousekeepingStatusId = 2

                },
                        new HousekeepingRoomNonAssocsDto
                {
                    Id = 9,
                    RoomId = 9,                    
                    CreationTime = DateTime.Now,
                    CreatorUserId = Guid.Parse("23eb803c-726a-4c7c-b25b-2c22a56793d9"),
                    ElapsedTime = 2,
                    PropertyId = 1,
                    Floor = "4",
                    RoomTypeAbbreviation = "STD",
                    RoomNumber = "401",
                    HousekeepingStatusId = 2

                },
                         new HousekeepingRoomNonAssocsDto
                {
                    Id = 10,
                    RoomId = 10,                    
                    CreationTime = DateTime.Now,
                    CreatorUserId = Guid.Parse("23eb803c-726a-4c7c-b25b-2c22a56793d9"),
                    ElapsedTime = 2,
                    PropertyId = 1,
                    Floor = "4",
                    RoomTypeAbbreviation = "STD",
                    RoomNumber = "402",
                    HousekeepingStatusId = 2

                },
                          new HousekeepingRoomNonAssocsDto
                {
                    Id = 11,
                    RoomId = 11,                  
                    CreationTime = DateTime.Now,
                    CreatorUserId = Guid.Parse("23eb803c-726a-4c7c-b25b-2c22a56793d9"),
                    ElapsedTime = 2,
                    PropertyId = 1,
                    Floor = "4",
                    RoomTypeAbbreviation = "STD",
                    RoomNumber = "403",
                    HousekeepingStatusId = 2

                },
                           new HousekeepingRoomNonAssocsDto
                {
                    Id = 12,
                    RoomId = 12,                    
                    CreationTime = DateTime.Now,
                    CreatorUserId = Guid.Parse("23eb803c-726a-4c7c-b25b-2c22a56793d9"),
                    ElapsedTime = 2,
                    PropertyId = 1,
                    Floor = "5",
                    RoomTypeAbbreviation = "STD",
                    RoomNumber = "501",
                    HousekeepingStatusId = 2

                },
                            new HousekeepingRoomNonAssocsDto
                {
                    Id = 13,
                    RoomId = 13,                    
                    CreationTime = DateTime.Now,
                    CreatorUserId = Guid.Parse("23eb803c-726a-4c7c-b25b-2c22a56793d9"),
                    ElapsedTime = 2,
                    PropertyId = 1,
                    Floor = "5",
                    RoomTypeAbbreviation = "STD",
                    RoomNumber = "502",
                    HousekeepingStatusId = 2

                }
            };        
        }
    }
}
