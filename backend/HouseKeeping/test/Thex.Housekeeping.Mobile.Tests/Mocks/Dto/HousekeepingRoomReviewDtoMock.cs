﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.Common.Dto;
using Thex.HouseKeeping.Dto;
using Tnf.Dto;

namespace Thex.HouseKeeping.Mobile.Tests.Mocks.Dto
{
    public class HousekeepingRoomReviewDtoMock
    {
        public static HousekeepingRoomReviewDto GetDto()
        {
            return new HousekeepingRoomReviewDto()
            {
                Id = Guid.Parse("23eb803c-726a-4c7c-b25b-2c22a56793d9"),
                CreationTime = DateTime.Now,
                CreatorUserId = Guid.Parse("23eb803c-726a-4c7c-b25b-2c22a56793d9"),
                PropertyId = 1,
                HousekeepingRoomId = Guid.Parse("23eb803c-726a-4c7c-b25b-2c22a56793d9"),
                ReasonId = 1,
                Review = "Review criada em " + DateTime.Now
            };
        }

        public static async Task<HousekeepingRoomReviewDto> GetDtoAsync()
        {
            var dto = GetDto();
            return await Task.FromResult(dto);
        }

        public static Task<IList<HousekeepingRoomReviewDto>> GetDtoListAsync()
        {
            var ret = new List<HousekeepingRoomReviewDto>();

            ret.Add(GetDto());
            ret.Add(GetDto());
            ret.Add(GetDto());

            return Task.FromResult<IList<HousekeepingRoomReviewDto>>(ret);
        }
    }
}
