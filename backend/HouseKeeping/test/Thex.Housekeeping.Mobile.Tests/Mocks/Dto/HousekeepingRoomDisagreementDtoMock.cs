﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.Common.Dto;
using Thex.HouseKeeping.Dto;
using Tnf.Dto;

namespace Thex.HouseKeeping.Mobile.Tests.Mocks.Dto
{
    public class HousekeepingRoomDisagreementDtoMock
    {
        public static HousekeepingRoomDisagreementDto GetDto()
        {
            return new HousekeepingRoomDisagreementDto()
            {
                Id = Guid.Parse("23eb803c-726a-4c7c-b25b-2c22a56793d9"),
                CreationTime = DateTime.Now,
                CreatorUserId = Guid.Parse("23eb803c-726a-4c7c-b25b-2c22a56793d9"),
                PropertyId = 1,
                HousekeepingRoomId = Guid.Parse("23eb803c-726a-4c7c-b25b-2c22a56793d9"),
                AdultCount = 3,
                BagCount = 2,
                ChildrenCount = 0,
                Observation = "Observação criada em " + DateTime.Now
            };
        }

        public static async Task<HousekeepingRoomDisagreementDto> GetDtoAsync()
        {
            var dto = GetDto();
            return await Task.FromResult(dto);
        }

        public static Task<IList<HousekeepingRoomDisagreementDto>> GetDtoListAsync()
        {
            var ret = new List<HousekeepingRoomDisagreementDto>();

            ret.Add(GetDto());
            ret.Add(GetDto());
            ret.Add(GetDto());

            return Task.FromResult<IList<HousekeepingRoomDisagreementDto>>(ret);
        }
    }
}
