﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Thex.Common.Dto;
using Thex.HouseKeeping.Dto;
using Tnf.Dto;

namespace Thex.HouseKeeping.Mobile.Tests.Mocks.Dto
{
    public class HousekeepingGeneratorDtoMock
    {
        public static List<Guid> ListUser()
        {
            return new List<Guid>
            {
                Guid.Parse("DE6E82D6-C402-47AD-9044-235B58A22D71"),
                Guid.Parse("B52840D7-810F-436B-9039-E14C252ADD83"),
                Guid.Parse("2FC1C1E8-37C8-4691-A213-FF6FB005816A"),
                Guid.Parse("82EAAD97-8375-4C0A-99FC-1CC9A7B740A8"),
                Guid.Parse("B45B240E-4FC7-4693-AEE4-0D9422003DBD"),
                Guid.Parse("670921AC-2FA8-406F-A943-5B2B28024FC8"),
                Guid.Parse("EB0738BC-5ED8-4820-8871-76763FD1324B")
            };

        }


        public static HousekeepingGeneratorDto HousekeepingGeneratorDto()
        {
            return new HousekeepingGeneratorDto()
            {
                GeneratorList = new List<Guid>
                {
                    Guid.Parse("DE6E82D6-C402-47AD-9044-235B58A22D71"),
                    Guid.Parse("B52840D7-810F-436B-9039-E14C252ADD83"),
                    Guid.Parse("2FC1C1E8-37C8-4691-A213-FF6FB005816A"),
                    Guid.Parse("82EAAD97-8375-4C0A-99FC-1CC9A7B740A8"),
                    Guid.Parse("B45B240E-4FC7-4693-AEE4-0D9422003DBD"),
                    Guid.Parse("670921AC-2FA8-406F-A943-5B2B28024FC8"),
                    Guid.Parse("EB0738BC-5ED8-4820-8871-76763FD1324B")

                }
            };
        }

        public static List<HousekeepingRoomNonAssocsDto> GetAllNonAssocs()
        {
            var list = new List<HousekeepingRoomNonAssocsDto>();

            var i = 0;
            while (i < 5)
            {
                list.AddRange(HousekeepingRoomDtoMock.GetDtoNonAssocsListAsync().Result);
                i++;
            }

            return list;
        }


        public static IEnumerable<UserDto> GetByListId()
        {

            return new List<UserDto>
            {
                new UserDto
                {
                    Id = Guid.Parse("DE6E82D6-C402-47AD-9044-235B58A22D71"),
                    Name = "João",
                    PhotoUrl = null
                },
                  new UserDto
                {
                    Id = Guid.Parse("B52840D7-810F-436B-9039-E14C252ADD83"),
                    Name = "Maria",
                    PhotoUrl = null
                },
                    new UserDto
                {
                    Id = Guid.Parse("2FC1C1E8-37C8-4691-A213-FF6FB005816A"),
                    Name = "Carla",
                    PhotoUrl = null
                },
                      new UserDto
                {
                    Id = Guid.Parse("82EAAD97-8375-4C0A-99FC-1CC9A7B740A8"),
                    Name = "José",
                    PhotoUrl = null
                },
                        new UserDto
                {
                    Id = Guid.Parse("B45B240E-4FC7-4693-AEE4-0D9422003DBD"),
                    Name = "Joana",
                    PhotoUrl = null
                },
                          new UserDto
                {
                    Id = Guid.Parse("670921AC-2FA8-406F-A943-5B2B28024FC8"),
                    Name = "Marta",
                    PhotoUrl = null
                },
                            new UserDto
                {
                    Id = Guid.Parse("EB0738BC-5ED8-4820-8871-76763FD1324B"),
                    Name = "Isabel",
                    PhotoUrl = null
                },

            };
        }


        public static HousekeepingGeneratorResponseDto GeneratorResponseDto()
        {


            var hkGenerator = new HousekeepingGeneratorResponseDto();
            hkGenerator.RoomMaidList = new List<RoomMaid>();

            var roomMaid = new RoomMaid
            {
                Name = "Isabel",
                ListRoom = new List<DirtyRoom>{
            new DirtyRoom
            {
                RoomId = 1,
                RoomNumber = "101",
                RoomAbbreviation = "STD",
                Floor = "1",
                RoomStatus = "Vago"
            },
            new DirtyRoom
            {
                RoomId = 2,
                RoomNumber = "102",
                RoomAbbreviation = "STD",
                Floor = "1",
                RoomStatus = "Vago"
            }}
            };

            hkGenerator.RoomMaidList.Add(roomMaid);

            roomMaid = new RoomMaid
            {
                Name = "Carla",
                CountRoom = 2,
                ListRoom = new List<DirtyRoom>{
            new DirtyRoom
            {
                RoomId = 3,
                RoomNumber = "103",
                RoomAbbreviation = "STD",
                Floor = "1",
                RoomStatus = "Vago"
            },
            new DirtyRoom
            {
                RoomId = 4,
                RoomNumber = "201",
                RoomAbbreviation = "STD",
                Floor = "2",
                RoomStatus = "Vago"
            }}
            };

            hkGenerator.RoomMaidList.Add(roomMaid);

            roomMaid = new RoomMaid
            {
                Name = "Marta",
                CountRoom = 2,
                ListRoom = new List<DirtyRoom>{
            new DirtyRoom
            {
                RoomId = 5,
                RoomNumber = "202",
                RoomAbbreviation = "STD",
                Floor = "2",
                RoomStatus = "Vago"
            },
            new DirtyRoom
            {
                RoomId = 6,
                RoomNumber = "301",
                RoomAbbreviation = "STD",
                Floor = "3",
                RoomStatus = "Vago"
            }}
            };

            hkGenerator.RoomMaidList.Add(roomMaid);

            roomMaid = new RoomMaid
            {
                Name = "Joana",
                CountRoom = 2,
                ListRoom = new List<DirtyRoom>{
            new DirtyRoom
            {
                RoomId = 7,
                RoomNumber = "302",
                RoomAbbreviation = "STD",
                Floor = "3",
                RoomStatus = "Vago"
            },
            new DirtyRoom
            {
                RoomId = 8,
                RoomNumber = "303",
                RoomAbbreviation = "STD",
                Floor = "3",
                RoomStatus = "Vago"
            }}
            };

            hkGenerator.RoomMaidList.Add(roomMaid);

            roomMaid = new RoomMaid
            {
                Name = "João",
                CountRoom = 2,
                ListRoom = new List<DirtyRoom>{
            new DirtyRoom
            {
                RoomId = 9,
                RoomNumber = "401",
                RoomAbbreviation = "STD",
                Floor = "4",
                RoomStatus = "Vago"
            },
            new DirtyRoom
            {
                RoomId = 10,
                RoomNumber = "402",
                RoomAbbreviation = "STD",
                Floor = "4",
                RoomStatus = "Vago"
            }}
            };

            hkGenerator.RoomMaidList.Add(roomMaid);

            roomMaid = new RoomMaid
            {
                Name = "Jose",
                CountRoom = 2,
                ListRoom = new List<DirtyRoom>{
            new DirtyRoom
            {
                RoomId = 11,
                RoomNumber = "403",
                RoomAbbreviation = "STD",
                Floor = "4",
                RoomStatus = "Vago"
            },
            new DirtyRoom
            {
                RoomId = 12,
                RoomNumber = "501",
                RoomAbbreviation = "STD",
                Floor = "5",
                RoomStatus = "Vago"
            }}
            };

            hkGenerator.RoomMaidList.Add(roomMaid);

            roomMaid = new RoomMaid
            {
                Name = "Maria",
                CountRoom = 1,
                ListRoom = new List<DirtyRoom>{
            new DirtyRoom
            {
                RoomId = 1,
                RoomNumber = "502",
                RoomAbbreviation = "STD",
                Floor = "5",
                RoomStatus = "Vago"
            }}
            };

            hkGenerator.RoomMaidList.Add(roomMaid);

            return hkGenerator;
        }

    }
}
