﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.Common.Dto;
using Thex.HouseKeeping.Dto;
using Tnf.Dto;

namespace Thex.HouseKeeping.Mobile.Tests.Mocks.Dto
{
    public class HousekeepingRoomStopDtoMock
    {
        public static HousekeepingRoomStopDto GetDto()
        {
            return new HousekeepingRoomStopDto()
            {
                Id = Guid.Parse("23eb803c-726a-4c7c-b25b-2c22a56793d9"),
                CreationTime = DateTime.Now,
                CreatorUserId = Guid.Parse("23eb803c-726a-4c7c-b25b-2c22a56793d9"),
                PropertyId = 1,
                HousekeepingRoomId = Guid.Parse("23eb803c-726a-4c7c-b25b-2c22a56793d9"),
                ReasonId = 1,
                Reason = "Razão de cancelamento em " + DateTime.Now
            };
        }

        public static async Task<HousekeepingRoomStopDto> GetDtoAsync()
        {
            var dto = GetDto();
            return await Task.FromResult(dto);
        }

        public static Task<IList<HousekeepingRoomStopDto>> GetDtoListAsync()
        {
            var ret = new List<HousekeepingRoomStopDto>();

            ret.Add(GetDto());
            ret.Add(GetDto());
            ret.Add(GetDto());

            return Task.FromResult<IList<HousekeepingRoomStopDto>>(ret);
        }
    }
}
