﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.HouseKeeping.Application.Interfaces;
using Thex.HouseKeeping.Dto;
using Thex.HouseKeeping.Mobile.Tests.Mocks.Dto;


namespace Thex.HouseKeeping.Mobile.Tests.Mocks.AppServiceMock
{
    public class HousekeepingGeneratorAppServiceMock : IHousekeepingGeneratorAppService

    {
        public async Task CreateRangeAsync(HousekeepingGeneratorResponseDto dto)
        {
            return;
        }

        public async Task<HousekeepingGeneratorResponseDto> GeneratorAsync(List<Guid> dto)
        {

            var responseDto = HousekeepingGeneratorDtoMock.GeneratorResponseDto();

            return await Task.FromResult(responseDto);
        }       
    }
}
