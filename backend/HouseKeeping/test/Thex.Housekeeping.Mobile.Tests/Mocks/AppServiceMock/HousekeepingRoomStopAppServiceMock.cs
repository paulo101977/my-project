﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.HouseKeeping.Application.Interfaces;
using Thex.HouseKeeping.Dto;
using Thex.HouseKeeping.Mobile.Tests.Mocks.Dto;

namespace Thex.HouseKeeping.Mobile.Tests.Mocks.AppServiceMock
{
    public class HousekeepingRoomStopAppServiceMock : IHousekeepingRoomStopAppService
    {
        public async Task<List<HousekeepingRoomStopDto>> GetAll(GetAllHousekeepingRoomStopDto dto)
        {
            return (await HousekeepingRoomStopDtoMock.GetDtoListAsync()).MapTo<List<HousekeepingRoomStopDto>>();
        }

        public async Task<HousekeepingRoomStopDto> CreateAsync(HousekeepingRoomStopDto dto)
        {
            return await HousekeepingRoomStopDtoMock.GetDtoAsync();
        }
    }
}
