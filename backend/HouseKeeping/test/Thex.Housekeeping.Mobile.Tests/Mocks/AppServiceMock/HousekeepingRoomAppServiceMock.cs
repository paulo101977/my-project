﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Thex.HouseKeeping.Application.Interfaces;
using Thex.HouseKeeping.Dto;
using Thex.HouseKeeping.Infra.Entities;
using Thex.HouseKeeping.Mobile.Tests.Mocks.Dto;

namespace Thex.HouseKeeping.Mobile.Tests.Mocks.AppServiceMock
{
    public class HousekeepingRoomAppServiceMock : IHousekeepingRoomAppService
    {
        public async Task<HousekeepingRoomDto> CreateAsync(HousekeepingRoomDto dto)
        {
            return await HousekeepingRoomDtoMock.GetDtoAsync();
        }

        public Task CreateRangeAsync(List<HousekeepingRoom> dto)
        {
            throw new NotImplementedException();
        }

        public async Task<List<HousekeepingRoomDto>> GetAll()
        {
            return HousekeepingRoomDtoMock.GetDtoListAsync().Result.ToList();
        }

        public Task<List<HousekeepingRoomGridDto>> GetAllGrid()
        {
            throw new NotImplementedException();
        }

        public async Task<IList<HousekeepingRoomDto>> GetAllHousekeepingRoomDto(GetAllHousekeepingRoomDto requestDto)
        {
            return await HousekeepingRoomDtoMock.GetDtoListAsync();
        }

        public async Task<List<HousekeepingRoomNonAssocsDto>> GetAllNonAssocs()
        {
            return (await HousekeepingRoomDtoMock.GetDtoNonAssocsListAsync()).MapTo<List<HousekeepingRoomNonAssocsDto>>();
        }

        public Task<HousekeepingRoomGridDto> GetGridUser(Guid? housekeepingRoomId)
        {
            throw new NotImplementedException();
        }

        public Task<HousekeepingRoomDto> GetHousekeepingRoomDtoById(Guid id)
        {
            return HousekeepingRoomDtoMock.GetDtoAsync();
        }

        public Task RemoveHouseKeepingAsync(List<Guid> houseKeepingIdList)
        {
            throw new NotImplementedException();
        }

        public Task RemoveRangeAsync(List<Guid> houseKeepingIdList)
        {
            throw new NotImplementedException();
        }

        public Task<HousekeepingRoomDto> UpdateAsync(Guid id, HousekeepingRoomDto dto)
        {
            return HousekeepingRoomDtoMock.GetDtoAsync();
        }
    }
}
