﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.HouseKeeping.Application.Interfaces;
using Thex.HouseKeeping.Dto;
using Thex.HouseKeeping.Mobile.Tests.Mocks.Dto;

namespace Thex.HouseKeeping.Mobile.Tests.Mocks.AppServiceMock
{
    public class ReasonAppServiceMock : IReasonAppService
    {
        public async Task<List<ReasonDto>> GetAll()
        {
            return (await ReasonDtoMock.GetDtoListAsync()).MapTo<List<ReasonDto>>();
        }

        public async Task<List<ReasonDto>> GetByCategory(int categoryId)
        {
            return (await ReasonDtoMock.GetDtoListAsync(categoryId)).MapTo<List<ReasonDto>>();
        }
    }
}
