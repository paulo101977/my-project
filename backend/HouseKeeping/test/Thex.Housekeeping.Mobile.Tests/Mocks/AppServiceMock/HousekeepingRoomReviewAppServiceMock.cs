﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.HouseKeeping.Application.Interfaces;
using Thex.HouseKeeping.Dto;
using Thex.HouseKeeping.Mobile.Tests.Mocks.Dto;

namespace Thex.HouseKeeping.Mobile.Tests.Mocks.AppServiceMock
{
    public class HousekeepingRoomReviewAppServiceMock : IHousekeepingRoomReviewAppService
    {
        public async Task<List<HousekeepingRoomReviewDto>> GetAll(GetAllHousekeepingRoomReviewDto dto)
        {
            return (await HousekeepingRoomReviewDtoMock.GetDtoListAsync()).MapTo<List<HousekeepingRoomReviewDto>>();
        }

        public async Task<HousekeepingRoomReviewDto> CreateAsync(HousekeepingRoomReviewDto dto)
        {
            return await HousekeepingRoomReviewDtoMock.GetDtoAsync();
        }
    }
}
