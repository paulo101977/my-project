﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.HouseKeeping.Application.Interfaces;
using Thex.HouseKeeping.Dto;
using Thex.HouseKeeping.Mobile.Tests.Mocks.Dto;

namespace Thex.HouseKeeping.Mobile.Tests.Mocks.AppServiceMock
{
    public class HousekeepingRoomDisagreementAppServiceMock : IHousekeepingRoomDisagreementAppService
    {
        public async Task<List<HousekeepingRoomDisagreementDto>> GetAll(GetAllHousekeepingRoomDisagreementDto dto)
        {
            return (await HousekeepingRoomDisagreementDtoMock.GetDtoListAsync()).MapTo<List<HousekeepingRoomDisagreementDto>>();
        }

        public async Task<HousekeepingRoomDisagreementDto> CreateAsync(HousekeepingRoomDisagreementDto dto)
        {
            return await HousekeepingRoomDisagreementDtoMock.GetDtoAsync();
        }
    }
}
