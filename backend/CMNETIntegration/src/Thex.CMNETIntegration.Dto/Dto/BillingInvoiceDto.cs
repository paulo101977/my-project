﻿using System;
using System.Collections.Generic;
using Tnf.Dto;

namespace Thex.CMNETIntegration.Dto
{
    public class InvoiceItemDto
    {
        public Guid? Id { get; set; }        
        public decimal Amount { get; set; }        
        public string BillingItemName { get; set; }
        public string IntegrationCode { get; set; }  
        public DateTime? BillingAccountItemDate { get; set; }
    }

    public class BillingInvoiceDto : BaseDto
    {        
        public static BillingInvoiceDto NullInstance = null;

        public BillingInvoiceDto()
        {
            IssRetido = new IssRetido();
            ExternalInformation = new ExternalInformation();
            DocumentInformation = new DocumentInformation
            {
                DocumentNumber = "",
                DocumentTypeName = ""
            };
        }

        public Guid BillingInvoiceId { get; set; }
        public string BillingInvoiceNumber { get; set; }
        public string BillingInvoiceSeries { get; set; }
        public string CompanyClientExternalCode { get; set; }
        public string RatePlanCode { get; set; }
        public DateTime CheckinDate { get; set; }
        public DateTime CheckoutDate { get; set; }
        public DateTime PaymentDate { get; set; }
        public DateTime? CancelDate { get; set; }
        public int NumberOfNigths { get; set; }
        public string PersonName { get; set; }
        public string GuestName { get; set; }
        public decimal InvoiceAmount { get; set; }
        public string ReservationCode { get; set; }
        public DocumentInformation DocumentInformation { get; set; }
        public string IntegrationCode { get; set; }
        public string IntegratorXml { get; set; }
        public string IntegratorLink { get; set; }

        public ExternalInformation ExternalInformation { get; set; }
        public IssRetido IssRetido { get; set; }

        public List<InvoiceItemDto> InvoiceItems { get; set; }
    }

    public class BillingFiscalInvoiceDto : BillingInvoiceDto
    {
        public AddressInformation AddressInformation { get; set; }
    }

    public class AddressInformation
    {
        public string StreetName { get; set; }
        public string StreetNumber { get; set; }
        public string AdditionalAddressDetails { get; set; }
        public string Neighborhood { get; set; }
        public string PostalCode { get; set; }
        public string CityCode { get; set; }
        public string StateCode { get; set; }
        public string CountryThreeLetterIsoCode { get; set; }
        public int BR_CodigoBCB { get; set; }
    }

    public class DocumentInformation
    {
        public string DocumentNumber { get; set; }
        public string DocumentTypeName { get; set; }
    }

    public class IssRetido
    {
        public decimal BaseCalculo { get; set; }
        public decimal Aliquota { get; set; }
        public decimal IssRetidoAmount { get; set; }
    }

    public class ExternalInformation
    {
        public string ExternalNumber { get; set; }
        public string ExternalSeries { get; set; }
        public DateTime ExternalEmissionDate { get; set; }
    }        
}