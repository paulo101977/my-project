﻿using System;
using Tnf.Dto;

namespace Thex.CMNETIntegration.Dto
{
    public partial class BillingAccountDto : BaseDto
    {
        public static BillingAccountDto NullInstance = null;

        public Guid Id { get; set; }
        public Guid? CompanyClientId { get; set; }
        public long? ReservationId { get; set; }
        public long? ReservationItemId { get; set; }
        public long? GuestReservationItemId { get; set; }
        public int BillingAccountTypeId { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime? EndDate { get; set; }
        public int StatusId { get; set; }
        public bool IsMainAccount { get; set; }
        public int MarketSegmentId { get; set; }
        public int BusinessSourceId { get; set; }
        public int PropertyId { get; set; }
        public Guid GroupKey { get; set; }
        public bool Blocked { get; set; }
        public string BillingAccountName { get; set; }

        public int? ReopeningReasonId { get; set; }
        public DateTime? ReopeningDate { get; set; }
        public bool? CreateMainAccountOfCompanyInReservationItem { get; set; }
        public virtual CompanyClientDto CompanyClient { get; set; }

    }
}
