﻿using System;
using System.Collections.Generic;
using System.Text;
using Tnf.Dto;

namespace Thex.CMNETIntegration.Dto
{
    public class BillingAccountByRoomDto : BaseDto
    {
        public Guid BillingAccountId { get; set; }
        public int RoomId { get; set; }
    }
}
