﻿using System;
using System.Collections.Generic;
using System.Text;
using Tnf.Dto;

namespace Thex.CMNETIntegration.Dto
{
    public class BillingInvoiceNFCeDto : BaseDto
    {        
        public static BillingInvoiceNFCeDto NullInstance = null;

        public Guid BillingInvoiceId { get; set; }
        public string BillingInvoiceNumber { get; set; }
        public string BillingInvoiceSeries { get; set; }        
        public string IntegratorXml { get; set; }
        public string IntegratorLink { get; set; }
        public byte[] XmlFile { get; set; }

        public ExternalInformation ExternalInformation { get; set; }
    }
}
