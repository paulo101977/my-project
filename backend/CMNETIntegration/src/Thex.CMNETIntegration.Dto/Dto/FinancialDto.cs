﻿using System;
using Tnf.Dto;

namespace Thex.CMNETIntegration.Dto
{
    public class FinancialDto : BaseDto
    {         
	    public Guid BillingAccountItemId { get; set; }
        public DateTime BillingAccountItemDate { get; set; }
        public decimal Amount { get; set; }
        public string IntegrationCode { get; set; }
        public string BillingItemName { get; set; }

        public static FinancialDto NullInstance = null;
    }
}
