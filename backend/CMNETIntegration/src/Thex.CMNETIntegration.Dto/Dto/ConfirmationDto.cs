﻿using System;
using Tnf.Dto;

namespace Thex.CMNETIntegration.Dto
{
    public class ConfirmationDto : BaseDto
    {
        public static ConfirmationDto NullInstance = null;

        public Guid Id { get; set; }
        public string IntegrationCode { get; set; }
    }
}
