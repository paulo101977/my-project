﻿using System;
using System.Collections.Generic;
using System.Text;
using Tnf.Dto;

namespace Thex.CMNETIntegration.Dto
{
    public class CreditCardDto : BaseDto
    {
        public static CreditCardDto NullInstance = null;

        public Guid Id { get; set; }
        public string BillingAccountId { get; set; }
        public decimal BillingAccountItemAmount { get; set; }        
        public string NSU { get; set; }
        public string CheckNumber { get; set; }        
        public string ServiceDescription { get; set; }
        public string IntegrationDetail { get; set; }
        public string PlasticBrandName { get; set; }
        public string IntegrationCode { get; set; }
        public int InstallmentsQuantity { get; set; }
    }
}
