﻿using System;
using Tnf.Dto;

namespace Thex.CMNETIntegration.Dto
{
    public class BillingAccountItemDto : BaseDto
    {
        public Guid Id { get; set; }
        public Guid BillingAccountId { get; internal set; }
        public int? BillingItemId { get; internal set; }
        public int BillingAccountItemTypeId { get; internal set; }
        public decimal Amount { get; internal set; }
        public DateTime BillingAccountItemDate { get; internal set; }
        public string BillingItemName { get; set; }
        public string IntegrationCode { get; internal set; }
    }
}
