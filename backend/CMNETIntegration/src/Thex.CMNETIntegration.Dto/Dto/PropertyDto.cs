﻿using Tnf.Dto;

namespace Thex.CMNETIntegration.Dto
{
    public class PropertyDto : BaseDto
    {
        public int Id { get; set; }
        public string DocumentInformation { get; set; }
        public string PropertyName { get; set; }

        public static PropertyDto NullInstace = null;
    }
}
