﻿using System;
using System.Collections.Generic;
using Thex.Common.Enumerations;

namespace Thex.CMNETIntegration.Dto
{
    public class BillingAccountItemForEventDto
    {
        public Guid Id { get; set; }
        public string IntegrationCode { get; set; }
        public decimal Amount { get; set; }
        public string BillingItemName { get; set; }
        public DateTime BillingAccountItemDate { get; set; }
        public string CurrencyCode { get; set; }
    }

    public class BillingAccountForEventDto
    {
        public static BillingAccountForEventDto NullInstance = null;

        public Guid Id { get; set; }
        public decimal BillingAccountAmount { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public BillingAccountTypeEnum BillingAccountTypeId { get; set; }
        public IEnumerable<BillingAccountItemForEventDto> BillingAccountItemsForEvent { get; set; }
    }
}
