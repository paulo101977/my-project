﻿using System;
using Tnf.Dto;

namespace Thex.CMNETIntegration.Dto
{
    public class EventBillingAccountItemDto : BaseDto
    {
        public Guid? BillingAccountId { get; set; }
        public string BillingItemIntegrationCode { get; set; }
        public decimal BillingItemAmount { get; set; }
        public string BillingItemCurrencyCode { get; set; }
        public string RoomNumber { get; set; }
    }
}
