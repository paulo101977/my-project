﻿using Thex.Common.Enumerations;
using Tnf.Dto;

namespace Thex.CMNETIntegration.Dto
{
    public partial class EventBillingAccountDto: BaseDto
    {
        public static EventBillingAccountDto NullInstance = null;

        public string CompanyClientDocument { get; set; }
        public DocumentTypeEnum CompanyClientDocumentType { get; set; }
        public string EventAccountName { get; set; }     
    }
}
