﻿using System;
using Tnf.Dto;

namespace Thex.CMNETIntegration.Dto
{
    public class PrepaymentDto : BaseDto
    {
        public decimal Amount { get; set; }
        public Guid  Id { get; set; }
        public DateTime BillingAccountItemDate { get; set; }
        public string IntegrationCode { get; set; }
        public DateTime CheckinDate { get; set; }
        public DateTime CheckoutDate { get; set; }
        public string BillingItemName { get; set; }
        public string History { get; set; }
        public string ReservationCode { get; set; }
    }
}
