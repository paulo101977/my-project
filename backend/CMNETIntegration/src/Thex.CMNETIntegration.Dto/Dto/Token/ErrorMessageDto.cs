﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Thex.CMNETIntegration.Dto
{
    public class Detail
    {
        public string guid { get; set; }
        public string code { get; set; }
        public string message { get; set; }
        public string detailedMessage { get; set; }
    }

    public class ErrorMessageDto
    {
        public string code { get; set; }
        public string message { get; set; }
        public string detailedMessage { get; set; }
        public string helpUrl { get; set; }
        public List<Detail> details { get; set; }
    }
}
