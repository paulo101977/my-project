﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Thex.CMNETIntegration.Dto
{
    public class TokenInfoDto
    {
        public string TokenClient { get; set; }
        public string TokenApplication { get; set; }
        public int PropertyId { get; set; }
    }
}
