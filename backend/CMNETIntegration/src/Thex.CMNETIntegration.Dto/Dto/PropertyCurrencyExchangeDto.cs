﻿using System;
using Tnf.Dto;

namespace Thex.CMNETIntegration.Dto
{
    public class PropertyCurrencyExchangeDto : BaseDto
    {
        public static PropertyCurrencyExchangeDto NullInstance = null;

        public Guid PropertyCurrencyExchangeId { get; set; }  
        public Guid CurrencyId { get; set; }
    }
}
