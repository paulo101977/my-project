﻿using System;
using Tnf.Dto;

namespace Thex.CMNETIntegration.Dto
{
    public class BillingAccountItemBaseDto : BaseDto
    {
        public decimal Amount { get; set; }

        public Guid? CurrencyId { get; set; }
        public Guid? CurrencyExchangeReferenceId { get; set; }
        public Guid? CurrencyExchangeReferenceSecId { get; set; }

        public void CalculateAmountByExchangeRate(decimal exchangeRateFirst, decimal exchangeRateSec)
        {
            this.Amount = Math.Round(Amount / exchangeRateFirst * exchangeRateSec, 2);
        }
    }

    public class BillingAccountItemDebitDto : BillingAccountItemBaseDto
    {
        public static BillingAccountItemDebitDto NullInstance = null;

        public Guid Id { get; set; }
        public int PropertyId { get; set; }
        public Guid BillingAccountId { get; set; }
        public int BillingItemId { get; set; }
        public Guid ProductId { get; set; }
        public int BillingItemCategoryId { get; set; }
        public Guid TenantId { get; set; }
        public DateTime? DateParameter { get; set; }
    }


        
    
}
