﻿using System;
using Tnf.Dto;

namespace Thex.CMNETIntegration.Dto
{
    public class CompanyClientDto : BaseDto
    {
        public static CompanyClientDto NullInstance = null;

        public Guid Id { get; set; }
        public int CompanyId { get; set; }
        public Guid PersonId { get; set; }
        public int PropertyId { get; set; }

        private char _personType;
        public char PersonType
        {
            get
            {
                return this._personType.ToString().ToUpper()[0];
            }
            set
            {
                this._personType = value;
            }
        }
        public string ShortName { get; set; }
        public string TradeName { get; set; }        
    }
}
