﻿using System;
using Tnf.Dto;

namespace Thex.CMNETIntegration.Dto
{
    public class AccountingDto : BaseDto
    {        
        public DateTime BillingAccountItemDate { get; set; }
        public decimal Amount { get; set; }     
        public string IntegrationCode { get; set; }
        public string BillingItemName { get; set; }

        public static AccountingDto NullInstance = null;        
    }
}
