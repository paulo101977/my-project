﻿using Tnf.Dto;

namespace Thex.CMNETIntegration.Dto
{
    public class BillingItemDto : BaseDto
    {
        public static BillingItemDto NullInstance = null;

        public int BillingItemId { get; set; }
        public int BillingItemTypeId { get; set; }
        public string BillingItemName { get; set; }
        public string IntegrationCode { get; set; }
        public int PaymentTypeId { get; set; }
    }
}
