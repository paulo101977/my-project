﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Thex.CMNETIntegration.Dto.Enumerations
{
    public class CMNETIntegrationEnum
    {
        public enum Error
        {
            BillingAccountIdInvalid, 
            BillingAccountItemIdInvalid,
            DocumentIsMissing,
            DocumentTypeInvalid,
            EmptyFilter,    
            EmptyToken,
            GeneralError,                  
            SaveOrUpdateError,
            Unauthorized            
        }

        public enum NotFoundErrors
        {
            AccountingNotFound,            
            BillingAccountNotFound,
            BillingInvoicesForDateNotFound,
            BillingItemNotFound,
            BillingItemsForDateNotFound,
            CompanyClientNotFound,
            FinancialNotFound,
            PrepaymentNotFound,
            PropertyCurrencyExchangeNotFound,
            PropertyNotFound
        }

        public enum GuidTypeValidate
        {
            BillingAccountIdValidate,
            BillingAccountItemIdValidate,
            BillingInvoiceIdValidate,
            TenantIdValidate
        }
    }    
}

