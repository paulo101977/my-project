﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Thex.CMNETIntegration.Dto;
using Thex.CMNETIntegration.Infra.Context;
using Thex.CMNETIntegration.Infra.Entities;
using Thex.CMNETIntegration.Infra.Interfaces;
using Thex.Kernel;
using Tnf.Dapper.Repositories;
using Tnf.Repositories;

namespace Thex.CMNETIntegration.Infra.Repositories
{
    public class BillingInvoiceRepository : DapperEfRepositoryBase<ThexCMNETIntegrationContext, BillingInvoice>, IBillingInvoiceRepository
    {
        private readonly IApplicationUser _applicationUser;

        public BillingInvoiceRepository(
            IApplicationUser applicationUser, 
            IActiveTransactionProvider activeTransactionProvider) : base(activeTransactionProvider)
        {
            _applicationUser = applicationUser;
        }

        public void ConfirmReceipt(ConfirmationDto model)
        {
            var query = Query<BillingInvoiceDto>(@"
                          SELECT BillingInvoiceId, IntegrationCode
                            FROM BillingInvoice
                           WHERE BillingInvoiceId = @billingInvoiceId
                             AND TenantId = @tenantId",
                           new { billingInvoiceId = model.Id, tenantId = _applicationUser.TenantId });

            var existingBillingInvoice = query.FirstOrDefault();

            if (existingBillingInvoice != null)
            {
                string integrationCode = null;

                if (!model.IntegrationCode.IsNullOrWhiteSpace())
                    integrationCode = model.IntegrationCode;

                var result = Execute(@"
                              UPDATE BillingInvoice 
                                 SET IntegrationCode = @integrationCode 
                               WHERE BillingInvoiceId = @billingInvoiceId
                                 AND TenantId = @tenantId",
                               new { integrationCode, billingInvoiceId = model.Id, tenantId = _applicationUser.TenantId });
            }
        }
    }
}
