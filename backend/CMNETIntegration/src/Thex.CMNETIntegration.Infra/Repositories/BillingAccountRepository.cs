﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Thex.CMNETIntegration.Infra.Context;
using Thex.CMNETIntegration.Infra.Entities;
using Thex.CMNETIntegration.Infra.Interfaces;
using Thex.Kernel;
using Tnf.Localization;

namespace Thex.CMNETIntegration.Infra.Repositories
{
    public class BillingAccountRepository : IBillingAccountRepository
    {
        private IApplicationUser _applicationUser;
        private ILocalizationManager _localizationManager;
        private readonly ThexCMNETIntegrationContext _context;

        public BillingAccountRepository(
            ThexCMNETIntegrationContext context,
            IApplicationUser applicationUser,
            ILocalizationManager localizationManager)
        {
            _applicationUser = applicationUser;
            _localizationManager = localizationManager;
            _context = context;
        }

        public async Task<BillingAccount> InsertAndSaveChangesAsync(BillingAccount account)
        {
            var entityEntry = await _context.BillingAccounts.AddAsync(account);
            await _context.SaveChangesAsync();
            account = entityEntry.Entity;

            return account;
        }
    }
}
