﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Thex.CMNETIntegration.Dto;
using Thex.CMNETIntegration.Infra.Context;
using Thex.CMNETIntegration.Infra.Entities;
using Thex.CMNETIntegration.Infra.Interfaces;
using Thex.Kernel;
using Tnf.Dapper.Repositories;
using Tnf.Repositories;

namespace Thex.CMNETIntegration.Infra.Repositories
{
    public class BillingAccountItemRepository : DapperEfRepositoryBase<ThexCMNETIntegrationContext, BillingAccountItemDto>, IBillingAccountItemRepository
    {
        private readonly IApplicationUser _applicationUser;

        public BillingAccountItemRepository(
            IApplicationUser applicationUser,
            IActiveTransactionProvider activeTransactionProvider) : base(activeTransactionProvider)
        {
            _applicationUser = applicationUser;
        }

        public void ConfirmReceipt(ConfirmationDto model)
        {
            var query = Query<BillingAccountItemDto>(@"
                          SELECT BillingAccountItemId as Id, IntegrationCode
                            FROM BillingAccountItem
                           WHERE BillingAccountItemId = @billingAccountItemId
                             AND TenantId = @tenantId",
                           new { billingAccountItemId = model.Id, tenantId = _applicationUser.TenantId});

            var existingBillingAccountItem = query.FirstOrDefault();

            if (existingBillingAccountItem != null)
            {
                string integrationCode = null;

                if (!model.IntegrationCode.IsNullOrWhiteSpace())
                    integrationCode = model.IntegrationCode;

                var result = Execute(@"
                              UPDATE BillingAccountItem 
                                 SET IntegrationCode = @integrationCode 
                               WHERE BillingAccountItemId = @billingAccountItemId
                                 AND TenantId = @tenantId",
                               new { integrationCode, billingAccountItemId = model.Id, tenantId = _applicationUser.TenantId });
            }
        }
    }
}
