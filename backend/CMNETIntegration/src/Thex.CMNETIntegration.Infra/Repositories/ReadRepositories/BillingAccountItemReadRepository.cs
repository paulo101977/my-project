﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Thex.CMNETIntegration.Dto;
using Thex.CMNETIntegration.Infra.Context;
using Thex.CMNETIntegration.Infra.Entities;
using Thex.CMNETIntegration.Infra.Interfaces.ReadRepositories;
using Thex.Common.Enumerations;
using Thex.Kernel;
using Tnf.Dapper.Repositories;
using Tnf.Localization;
using Tnf.Repositories;

namespace Thex.CMNETIntegration.Infra.Repositories.ReadRepositories
{
    public class BillingAccountItemReadRepository : DapperEfRepositoryBase<ThexCMNETIntegrationContext, BillingAccountItem>, IBillingAccountItemReadRepository
    {
        private readonly ILocalizationManager _localizationManager;
        private readonly IApplicationUser _applicationUser;

        public BillingAccountItemReadRepository(
            IApplicationUser applicationUser,
            IActiveTransactionProvider activeTransactionProvider,
            ILocalizationManager localizationManager) : base(activeTransactionProvider)
        {
            _applicationUser = applicationUser;
            _localizationManager = localizationManager;
        }

        public async Task<IEnumerable<PrepaymentDto>> GetPrepayments(DateTime itemDate)
        {
            var sql = @"SELECT Amount, Id, BillingAccountItemDate, IntegrationCode, CheckinDate, CheckoutDate, BillingItemName, History, ReservationCode 
                          FROM (SELECT Mv.Amount, Mv.BillingAccountItemId as Id, Mv.BillingAccountItemDate, Bi.IntegrationCode,
                                       Ri.CheckinDate as CheckinDate, Ri.CheckoutDate as CheckoutDate, Bi.BillingItemName, Re.ReservationCode,
                                       'Reserva: ' + Re.ReservationCode + 
                                        CASE WHEN Gi.GuestName IS NOT NULL 
                                             THEN ' - Hóspede: ' + Gi.GuestName 
                                             ELSE ' - Empresa: ' + CC.TradeName 
                                             END + ' - Conta: ' + Coalesce(Ba.BillingAccountName, 'Geral') as History
                                  FROM BillingAccountItem Mv
	                                   INNER JOIN BillingItem Bi         ON (MV.BillingItemId = Bi.BillingItemId AND Bi.BillingItemTypeId = 3) 
	                                   INNER JOIN BillingAccount Ba      ON (MV.BillingAccountId = Ba.BillingAccountId AND Ba.BillingAccountTypeId BETWEEN 2 AND 3)
	                                   INNER JOIN ReservationItem Ri     ON (Ri.ReservationItemId = Ba.ReservationItemId AND MV.BillingAccountItemDate < CAST(COALESCE(Ri.CheckInDate, Ri.EstimatedArrivalDate) AS DATE))
                                       INNER JOIN Reservation Re		 ON (Re.ReservationId = Ri.ReservationId)
	                                   LEFT JOIN GuestReservationItem gi ON (Ri.ReservationItemId = Gi.ReservationItemId AND Ba.GuestReservationItemId = Gi.GuestReservationItemId)
	                                   LEFT JOIN CompanyClient Cc        ON (CC.CompanyClientId = Ba.CompanyClientId )
                                 WHERE Ba.PropertyId = @propertyId
                                UNION ALL
                                SELECT Mv.Amount, Mv.BillingAccountItemId, Mv.BillingAccountItemDate, Bi.IntegrationCode,
                                       Ri.CheckinDate as CheckinDate, Ri.CheckoutDate as CheckoutDate, Bi.BillingItemName, Rs.ReservationCode,
                                       'Reserva: ' + Rs.ReservationCode + ' - Grupo: ' + Rs.GroupName + ' - Conta Master: '
                                    	+ COALESCE(Ba.BillingAccountName, 'Geral') as History
                                  FROM BillingAccountItem Mv
	                                   INNER JOIN BillingItem Bi    ON (Mv.BillingItemId = Bi.BillingItemId AND Bi.BillingItemTypeId = 3)
	                                   INNER JOIN BillingAccount Ba ON (MV.BillingAccountId = Ba.BillingAccountId AND Ba.BillingAccountTypeId = 4)
	                                   INNER JOIN Reservation Rs    ON (Ba.ReservationId = Rs.ReservationId)
                                       LEFT JOIN (SELECT MIN(R.CHECKINDATE) AS CheckinDate, 
                                                         MIN(R.ESTIMATEDARRIVALDATE) AS ESTIMATEDARRIVALDATE,
	                                                     MAX(R.CHECKOUTDATE) AS CheckoutDate, 
                                                         MAX(R.EstimatedDepartureDate) AS EstimatedDepartureDate,
					                                     R.ReservationId
				                                    FROM ReservationItem R 
				                                   GROUP BY R.ReservationId) Ri ON (Ri.ReservationId = Rs.ReservationId AND Mv.BillingAccountItemDate < CAST(COALESCE(Ri.CheckInDate, Ri.EstimatedArrivalDate) AS DATE))
                                 WHERE Ba.PropertyId = @propertyId) T
                           WHERE CAST(BillingAccountItemDate AS DATE) = CAST(@itemDate AS DATE) OR 
                                 CAST(CheckinDate AS DATE) = CAST(@itemDate AS DATE) OR 
                                 CAST(CheckoutDate AS DATE) = CAST(@itemDate AS DATE)";

            var query = await QueryAsync<PrepaymentDto>(sql, new { propertyId = _applicationUser.PropertyId.TryParseToInt32(), itemDate });

            return query.ToList();
        }

        public async Task<IEnumerable<BillingAccountItem>> GetCreditCard(DateTime itemDate)
        {
            var stringDate = itemDate.ToString("yyyy-MM-dd");

            var sql = @"SELECT CASE
                        WHEN BAI.BillingAccountItemTypeId = 3
                        THEN
                        bai.billingaccountitemparentid
                        ELSE
                        BAI.BillingAccountItemId
                        END
                        AS Id,
                        BAI.BillingItemId,
                        BAI.BillingAccountItemTypeId,
                        BI.IntegrationCode,
                        CASE WHEN BAI.BillingAccountItemTypeId=5 THEN BAI.OriginalAmount ELSE
                        BAI.Amount END AS BillingAccountItemAmount,
                        BAI.BillingAccountItemDate,
                        BI.IntegrationCode,
                        BI.BillingItemName,
                        BAI.NSU,
                        BAI.CheckNumber,
                        BAI.BillingAccountId,
                        BAI.ServiceDescription,
                        BAI.IntegrationDetail,
                        PB.PlasticBrandName,
                        CASE
                        WHEN BAI.InstallmentsQuantity IS NULL THEN 1
                        ELSE BAI.InstallmentsQuantity
                        END
                        AS InstallmentsQuantity,
                        bai.WasReversed
                        FROM BillingItem BI
                        JOIN BillingAccountItem BAI 
                        ON (BAI.BillingItemId = BI.BillingItemId AND BAI.IsDeleted = 0
                        and (BAI.BillingAccountItemTypeId<>5 
                        OR (BAI.BillingAccountItemTypeId=5 
                        AND BAI.BillingAccountItemId = BAI.PartialParentBillingAccountItemId)))
                        JOIN BillingAccount BA
                        ON ( BA.BillingAccountId = BAI.BillingAccountId
                        AND BA.PropertyId = BI.PropertyId)
                        JOIN PlasticBrandProperty PBP
                        ON ( PBP.PlasticBrandPropertyId = BI.PlasticBrandPropertyId
                        AND PBP.PropertyId = BI.PropertyId)
                        JOIN PlasticBrand PB ON (PB.PlasticBrandId = PBP.PlasticBrandId)
                        WHERE CAST (BAI.BillingAccountItemDate AS DATE) =
                        CAST (@ItemDate AS DATE)
                        AND BI.PropertyId = @PropertyId
                        AND (BI.PaymentTypeId = 5 OR BI.PaymentTypeId = 9)
                        ORDER BY BAI.CREATIONTIME";

            var query = await QueryAsync<BillingAccountItem>(sql, new { ItemDate = stringDate, PropertyId = _applicationUser.PropertyId.TryParseToInt32(), PaymentTypeId = PaymentTypeEnum.CreditCard });

            var result = query.ToList();

            return result;
        }
    }
}
