﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Thex.CMNETIntegration.Dto;
using Thex.CMNETIntegration.Infra.Context;
using Thex.CMNETIntegration.Infra.Entities;
using Thex.CMNETIntegration.Infra.Interfaces.ReadRepositories;
using Thex.Common.Enumerations;
using Thex.Kernel;
using Tnf.Dapper.Repositories;
using Tnf.Repositories;

namespace Thex.CMNETIntegration.Infra.Repositories.ReadRepositories
{
    public class BillingAccountReadRepository : DapperEfRepositoryBase<ThexCMNETIntegrationContext, BillingAccount>, IBillingAccountReadRepository
    {
        private readonly IApplicationUser _applicationUser;
                
        public BillingAccountReadRepository(
            IApplicationUser applicationUser,
            IActiveTransactionProvider activeTransactionProvider) : base(activeTransactionProvider)
        {
            _applicationUser = applicationUser;
        }

        public async Task<BillingAccountForEventDto> GetBillingAccountAsync(string billingAccountId)
        {
            var sql = @"SELECT BA.BillingAccountId AS Id, BA.StartDate, BA.EndDate, BA.BillingAccountTypeId, SUM(BAI.Amount) AS BillingAccountAmount
                          FROM BillingAccount BA
                               LEFT JOIN BillingAccountItem BAI ON (BA.BillingAccountId = BAI.BillingAccountId AND BAI.IsDeleted = 0)
                         WHERE BA.BillingAccountId = @AccountId
                         GROUP BY BA.BillingAccountId, BA.StartDate, BA.EndDate, BA.BillingAccountTypeId";

            object param = new { AccountId = billingAccountId };           

            var query = await QueryAsync<BillingAccountForEventDto>(sql, param);

            return query.FirstOrDefault();
        }

        public async Task<BillingAccountByRoomDto> GetBillingAccountIdByRoomNumber(string roomNumber)
        {
            var checkinStatus = (int)ReservationStatus.Checkin;

            var sql = @"SELECT RO.RoomId, BA.BillingAccountId 
                          FROM BillingAccount BA
                               JOIN ReservationItem RI ON (RI.ReservationItemId = BA.ReservationItemId)
                               JOIN GuestReservationItem GRI ON (GRI.ReservationItemId = RI.ReservationItemId and GRI.IsMain = 1)
	                           JOIN Room RO ON (RO.RoomId = RI.RoomId AND RO.PropertyId = BA.PropertyId)
                         WHERE BA.PropertyId = @PropertyId
                           AND RO.RoomNumber = @RoomNumber
                           AND RI.ReservationItemStatusId = @CheckinStatus
                           AND BA.GuestReservationItemId = GRI.GuestReservationItemId
                           AND BA.EndDate IS NULL
                           AND BA.Blocked = 0
                         ORDER BY BA.IsMainAccount DESC, BA.CreationTime";

            object param = new { PropertyId = _applicationUser.PropertyId.TryParseToInt32(), RoomNumber = roomNumber, CheckinStatus = checkinStatus };

            var query = await QueryAsync<BillingAccountByRoomDto>(sql, param);

            return query.FirstOrDefault();
        }

        public async Task<IEnumerable<BillingAccountItemForEventDto>> GetBillingAccountItemsAsync(string billingAccountId, bool associatedAccounts, DateTime? itemStartDate, DateTime? itemEndDate)
        {                                      
               var sql = @"SELECT BAI.BillingAccountItemId AS Id, BAI.BillingItemId, BAI.BillingAccountItemTypeId,  
                               BAI.Amount, BAI.BillingAccountItemDate, BI.IntegrationCode, BI.BillingItemName, 
                               CUR.AlphabeticCode AS CurrencyCode
                          FROM BillingAccount BA
                               JOIN BillingAccountItem BAI ON (BA.BillingAccountId = BAI.BillingAccountId and BAI.BillingAccountId = @AccountId)
                               JOIN BillingItem BI ON (BAI.BillingItemId = BI.BillingItemId)
                               LEFT JOIN Currency CUR ON (CUR.CurrencyId = BAI.CurrencyId)";

            object param = new { AccountId = billingAccountId };

            if (itemStartDate.HasValue && itemEndDate.HasValue)
            {
                var startDateString = itemStartDate?.ToShortDateString();
                var endDateString = itemEndDate?.ToShortDateString();

                sql += " WHERE CAST(BAI.BillingAccountItemDate AS DATE) BETWEEN CAST(@StartDate AS DATE) AND CAST(EndDate AS DATE)";

                param = new
                {
                    AccountId = billingAccountId,
                    StartDate = startDateString,
                    EndDate = endDateString
                };
            }
            else if (itemStartDate.HasValue) 
            {
                var startDateString = itemStartDate?.ToShortDateString();
                var endDateString = itemEndDate?.ToShortDateString();

                sql += " WHERE CAST(BAI.BillingAccountItemDate AS DATE) = CAST(@StartDate AS DATE)";

                param = new
                {
                    AccountId = billingAccountId,
                    StartDate = startDateString
                };
            }

            var query = await QueryAsync<BillingAccountItemForEventDto>(sql, param);

            return query.ToList();
        }
    }
}
