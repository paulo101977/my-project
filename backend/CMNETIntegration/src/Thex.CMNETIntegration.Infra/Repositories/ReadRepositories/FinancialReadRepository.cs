﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Thex.CMNETIntegration.Dto;
using Thex.CMNETIntegration.Infra.Context;
using Thex.CMNETIntegration.Infra.Interfaces.ReadRepositories;
using Thex.Kernel;
using Tnf.Dapper.Repositories;
using Tnf.Localization;
using Tnf.Repositories;

namespace Thex.CMNETIntegration.Infra.Repositories.ReadRepositories
{
    public class FinancialReadRepository : DapperEfRepositoryBase<ThexCMNETIntegrationContext, FinancialDto>, IFinancialReadRepository
    {
        private readonly ILocalizationManager _localizationManager;
        private readonly IApplicationUser _applicationUser;

        public FinancialReadRepository(
            IApplicationUser applicationUser,
            IActiveTransactionProvider activeTransactionProvider,
            ILocalizationManager localizationManager)
            : base(activeTransactionProvider)
        {
            _localizationManager = localizationManager;
            _applicationUser = applicationUser;
        }

        public async Task<IEnumerable<FinancialDto>> GetFinancialByItemDate(DateTime billingAccountItemDate)
        {
            var itemDate = billingAccountItemDate.ToString("yyyy-MM-dd");

            var query = await QueryAsync<FinancialDto>(@"                
                SELECT BAI.Amount,
                       IT.IntegrationCode,
                       CAST(BAI.BillingAccountItemDate AS DATE) AS BillingAccountItemDate,
                       IT.BILLINGITEMNAME,
                       BAI.BillingAccountItemId
                  FROM BillingAccountItem BAI
                       JOIN BillingItem IT ON (BAI.BillingItemId = IT.BillingItemId AND BAI.IsDeleted = 0)
                 WHERE IT.PropertyId = @propertyId
                   AND CAST(BAI.BillingAccountItemDate AS DATE) = CAST(@itemDate AS DATE)
                   AND It.PaymentTypeId IS NOT NULL
                   AND It.PaymentTypeId not in (5, 9)
                 ORDER BY IT.IntegrationCode",
                new { propertyId = _applicationUser.PropertyId.TryParseToInt32(), itemDate });

            return query.ToList();            
        }
    }
}
