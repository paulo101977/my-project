﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Thex.CMNETIntegration.Dto;
using Thex.CMNETIntegration.Infra.Context;
using Thex.CMNETIntegration.Infra.Entities;
using Thex.CMNETIntegration.Infra.Interfaces.ReadRepositories;
using Thex.Common.Enumerations;
using Tnf.Dapper.Repositories;
using Tnf.Localization;
using Tnf.Repositories;

namespace Thex.CMNETIntegration.Infra.Repositories.ReadRepositories
{
    public class PropertyReadRepository : DapperEfRepositoryBase<ThexCMNETIntegrationContext, Property>, IPropertyReadRepository
    {
        private readonly ILocalizationManager _localizationManager;

        public PropertyReadRepository(
            IActiveTransactionProvider activeTransactionProvider,
            ILocalizationManager localizationManager)
            : base(activeTransactionProvider)
        {
            _localizationManager = localizationManager;
        }

        public async Task<PropertyDto> GetProperty(string documentInfo)
        {
            var query = await QueryAsync<Property>(@"
                SELECT PRO.NAME AS PROPERTYNAME, PRO.PROPERTYID AS ID, DO.DOCUMENTINFORMATION 
                  FROM COMPANY CO
                       JOIN PROPERTY PRO ON (PRO.COMPANYID = CO.COMPANYID)
	                   JOIN DOCUMENT DO ON (DO.OWNERID = CO.COMPANYUID)
                 WHERE REPLACE(REPLACE(REPLACE(REPLACE( DO.DocumentInformation, '.', ''), '-', ''), '\', ''), '/', '') = 
                         REPLACE(REPLACE(REPLACE(REPLACE( @DocumentInfo, '.', ''), '-', ''), '\', ''), '/', '')",
                new { DocumentInfo = documentInfo });

            var result = query.FirstOrDefault();

            return MapToDto(result);
        }

        public async Task<PropertyDto> GetProperty(int propertyId)
        {
            var CNPJTypeId = (int)DocumentTypeEnum.BrLegalPersonCNPJ;
            var NIFTypeId = (int)DocumentTypeEnum.LegalNIF;

            var query = await QueryAsync<Property>(@"
                SELECT PRO.NAME AS PROPERTYNAME, PRO.PROPERTYID AS ID, DO.DOCUMENTINFORMATION 
                  FROM COMPANY CO
                       JOIN PROPERTY PRO ON (PRO.COMPANYID = CO.COMPANYID)
	                   JOIN DOCUMENT DO ON (DO.OWNERID = CO.COMPANYUID AND (DO.DOCUMENTTYPEID = @CNPJTypeId OR DO.DOCUMENTTYPEID = @NIFTypeId))
                 WHERE PRO.PROPERTYID = @PropertyId",
                new { PropertyId = propertyId, CNPJTypeId, NIFTypeId });

            var result = query.FirstOrDefault();

            return MapToDto(result);
        }

        private PropertyDto MapToDto(Property property)
        {
            if (property is Property)
            {
                return new PropertyDto
                {
                    DocumentInformation = property.DocumentInformation,
                    Id = property.Id,
                    PropertyName = property.PropertyName
                };
            }
            return null;
        }
    }
}
