﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Thex.CMNETIntegration.Dto;
using Thex.CMNETIntegration.Infra.Context;
using Thex.CMNETIntegration.Infra.Entities;
using Thex.CMNETIntegration.Infra.Interfaces.ReadRepositories;
using Tnf.Dapper.Repositories;
using Tnf.Repositories;

namespace Thex.CMNETIntegration.Infra.Repositories.ReadRepositories
{
    public class HealthReadRepository : DapperEfRepositoryBase<ThexCMNETIntegrationContext, BillingItemDto>, IHealthReadRepository
    {
        public HealthReadRepository(IActiveTransactionProvider activeTransactionProvider) : base(activeTransactionProvider)
        {
        }

        public async Task<BillingItemDto> Check()
        {
            var sql = @" SELECT TOP 1 B.BillingItemId, B.BillingItemTypeId, B.BillingItemName, B.IntegrationCode 
                           FROM BillingItem B ";

            var query = await QueryAsync<BillingItemDto>(sql);

            var result = query.FirstOrDefault();

            return result;
        }
    }
}
