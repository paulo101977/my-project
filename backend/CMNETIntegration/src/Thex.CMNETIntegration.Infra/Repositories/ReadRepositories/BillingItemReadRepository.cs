﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Thex.CMNETIntegration.Dto;
using Thex.CMNETIntegration.Infra.Context;
using Thex.CMNETIntegration.Infra.Entities;
using Thex.CMNETIntegration.Infra.Interfaces.ReadRepositories;
using Thex.Kernel;
using Tnf.Dapper.Repositories;
using Tnf.Repositories;

namespace Thex.CMNETIntegration.Infra.Repositories.ReadRepositories
{
    public class BillingItemReadRepository : DapperEfRepositoryBase<ThexCMNETIntegrationContext, BillingItemDto>, IBillingItemReadRepository
    {
        private readonly IApplicationUser _applicationUser;

        public BillingItemReadRepository(
            IApplicationUser applicationUser,
            IActiveTransactionProvider activeTransactionProvider) : base(activeTransactionProvider)
        {
            _applicationUser = applicationUser;
        }

        public async Task<BillingItemDto> GetBillingItemByIntegrationCode(string integrationCode)
        {
            var sql = @" SELECT B.BillingItemId, B.BillingItemTypeId, B.BillingItemName, B.IntegrationCode 
                           FROM BillingItem B
                          WHERE B.IntegrationCode = @IntegrationCode
                            AND B.PropertyId = @PropertyId";

            var query = await QueryAsync<BillingItemDto>(sql, new { IntegrationCode = integrationCode, PropertyId = _applicationUser.PropertyId.TryParseToInt32() });

            var result = query.FirstOrDefault();

            return result;
        }

        public async Task<IEnumerable<BillingItemDto>> GetAll()
        {
            var sql = @" SELECT B.BillingItemId, B.BillingItemTypeId, B.BillingItemName, B.IntegrationCode, B.PaymentTypeId 
                           FROM BillingItem B
                          WHERE B.PropertyId = @PropertyId";

            var result = await QueryAsync<BillingItem>(sql, new { PropertyId = _applicationUser.PropertyId.TryParseToInt32() });

            return MapToDto(result);
        }

        private IEnumerable<BillingItemDto> MapToDto(IEnumerable<BillingItem> list)
        {
            var itemList = new List<BillingItemDto>();

            foreach (var item in list)
                itemList.Add(item.MapTo<BillingItemDto>());

            return itemList;
        }
    }
}
