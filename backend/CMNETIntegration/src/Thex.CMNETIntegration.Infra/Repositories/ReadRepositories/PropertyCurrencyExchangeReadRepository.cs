﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Thex.CMNETIntegration.Dto;
using Thex.CMNETIntegration.Infra.Context;
using Thex.CMNETIntegration.Infra.Interfaces.ReadRepositories;
using Thex.Kernel;
using Tnf.Dapper.Repositories;
using Tnf.Repositories;

namespace Thex.CMNETIntegration.Infra.Repositories.ReadRepositories
{
    public class PropertyCurrencyExchangeReadRepository : DapperEfRepositoryBase<ThexCMNETIntegrationContext, PropertyCurrencyExchangeDto>, IPropertyCurrencyExchangeReadRepository
    {
        private readonly IApplicationUser _applicationUser;

        public PropertyCurrencyExchangeReadRepository(
            IApplicationUser applicationUser,
            IActiveTransactionProvider activeTransactionProvider) : base(activeTransactionProvider)
        {
            _applicationUser = applicationUser;
        }

        public async Task<PropertyCurrencyExchangeDto> GetExchangeByCurrenyCode(string currencyCode)
        {
            var sql = @" SELECT TOP 1 P.PropertyCurrencyExchangeId, P.CurrencyId  
                           FROM PropertyCurrencyExchange P
                                JOIN CURRENCY C ON (C.CurrencyId = P.CurrencyId AND C.AlphabeticCode = @CurrencyCode)
                          WHERE P.propertyid = @PropertyId 
                          ORDER BY PropertyCurrencyExchangeDate DESC";            

            var query = await QueryAsync<PropertyCurrencyExchangeDto>(sql, new { CurrencyCode = currencyCode, PropertyId = _applicationUser.PropertyId.TryParseToInt32() });

            var result = query.FirstOrDefault();

            return result;
        }
    }
}
