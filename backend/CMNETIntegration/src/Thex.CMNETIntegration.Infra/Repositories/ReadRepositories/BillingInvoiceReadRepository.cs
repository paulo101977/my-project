﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Thex.CMNETIntegration.Dto;
using Thex.CMNETIntegration.Infra.Context;
using Thex.CMNETIntegration.Infra.Entities;
using Thex.CMNETIntegration.Infra.Interfaces.ReadRepositories;
using Thex.Common.Enumerations;
using Thex.Common.Helpers;
using Thex.Kernel;
using Tnf.Dapper.Repositories;
using Tnf.Repositories;

namespace Thex.CMNETIntegration.Infra.Repositories.ReadRepositories
{
    public class BillingInvoiceReadRepository : DapperEfRepositoryBase<ThexCMNETIntegrationContext, BillingInvoiceDto>, IBillingInvoiceReadRepository
    {
        private readonly IApplicationUser _applicationUser;
        private List<string> commaSeparatorIds = new List<string>();

        public BillingInvoiceReadRepository(
            IApplicationUser applicationUser,
            IActiveTransactionProvider activeTransactionProvider) : base(activeTransactionProvider)
        {
            commaSeparatorIds.Clear();
            _applicationUser = applicationUser;
            switch (EnumHelper.GetEnumValue<CountryIsoCodeEnum>(_applicationUser.PropertyCountryCode, true))
            {
                case CountryIsoCodeEnum.Brasil:
                    {
                        commaSeparatorIds.Add(BillingInvoiceTypeEnum.NFSE.TryParseToInt32().ToString());
                        break;
                    }
                case CountryIsoCodeEnum.Europe:
                    {
                        commaSeparatorIds.Add(BillingInvoiceTypeEnum.Factura.TryParseToInt32().ToString());
                        commaSeparatorIds.Add(BillingInvoiceTypeEnum.CreditNotePartial.TryParseToInt32().ToString());
                        commaSeparatorIds.Add(BillingInvoiceTypeEnum.CreditNoteTotal.TryParseToInt32().ToString());
                        commaSeparatorIds.Add(BillingInvoiceTypeEnum.DebitNote.TryParseToInt32().ToString());                        
                        break;
                    }
                case CountryIsoCodeEnum.Latam:
                    {
                        commaSeparatorIds.Add(BillingInvoiceTypeEnum.NFSE.TryParseToInt32().ToString());
                        break;
                    }
                default:
                    {
                        commaSeparatorIds.Add(BillingInvoiceTypeEnum.NFSE.TryParseToInt32().ToString());
                        break;
                    }

            }
        }                          

        public async Task<IEnumerable<BillingFiscalInvoice>> GetFiscalInvoices(DateTime emissionDate)
        {
            var stringDate = emissionDate.ToString("yyyy-MM-dd");            

            var sql = GetSQLInvoiceBase(true);                           

            var query = await QueryAsync<BillingFiscalInvoice>(sql, new { EmissionDate = stringDate,
                PropertyId = _applicationUser.PropertyId.TryParseToInt32(),
                billingInvoiceTypeIds = commaSeparatorIds });

            var result = query.ToList();

            return result;
        }

        public async Task<IEnumerable<BillingInvoice>> GetInvoices(DateTime emissionDate)
        {
            var stringDate = emissionDate.ToString("yyyy-MM-dd");            

            var sql = GetSQLInvoiceBase(false);                      
    
            var query = await QueryAsync<BillingInvoice>(sql, new { propertyId = _applicationUser.PropertyId.TryParseToInt32(),
                emissionDate, billingInvoiceTypeIds = commaSeparatorIds
            });

            var result = query.ToList();
            
            return result;
        }        

        public async Task<IEnumerable<InvoiceItem>> GetInvoicesItems(DateTime emissionDate, bool isFiscal)
        {          
            var paymentTypeId = (int)PaymentTypeEnum.TobeBilled;           

            var sql = string.Empty;
            if (isFiscal)
                sql = @"SELECT          BAI.BillingAccountItemId AS Id, BAI.Amount, IT.BillingItemName, IT.IntegrationCode, BA.BillingAccountId, 
                                        IT.PaymentTypeId, BAI.BillingAccountItemDate,
                                        BI.BillingInvoiceId
                        FROM            BillingInvoice BI
                                        JOIN BillingAccount BA ON (BI.BillingAccountId = BA.BillingAccountId and BI.PropertyId = BA.PropertyId)
                                        JOIN BillingAccountItem BAI 
                                            ON (BAI.BillingAccountId = BA.BillingAccountId AND bai.WasReversed=0
                                            And Bai.BillingAccountItemTypeId<>3 AND BAI.IsDeleted = 0 AND BAI.BillingInvoiceId=bi.BillingInvoiceId)
                                        JOIN BillingItem IT ON (BAI.BillingItemId = IT.BillingItemId AND IT.PropertyId = BA.PropertyId)
                        WHERE           BI.PropertyId = @propertyId   
                                        AND BI.BillingInvoiceTypeId IN @billingInvoiceTypeIds
                                        AND CAST(BI.EmissionDate AS DATE) = CAST(@emissionDate AS DATE)                                        
                                        AND EXISTS(SELECT 1 FROM BILLINGITEM B
                                                   WHERE B.PROPERTYID = BI.PropertyId
                                                   AND B.BillingItemId = BAI.BillingItemId
                                                   AND B.PAYMENTTYPEID IS NULL)";                
            else
                sql = @"SELECT          SUM(BAI.Amount) as Amount, IT.BillingItemName, IT.IntegrationCode, BA.BillingAccountId,BI.BillingInvoiceId                                
                        FROM            BillingInvoice BI
                                        JOIN BillingAccount BA ON (BI.BillingAccountId = BA.BillingAccountId and BI.PropertyId = BA.PropertyId)
                                        JOIN BillingAccountItem BAI ON (BAI.BillingAccountId = BA.BillingAccountId AND bai.WasReversed=0
                                            And Bai.BillingAccountItemTypeId<>3 AND BAI.IsDeleted = 0 AND BAI.BillingInvoiceId=bi.BillingInvoiceId)
                                        JOIN BillingItem IT ON (BAI.BillingItemId = IT.BillingItemId AND IT.PropertyId = BA.PropertyId)
                       WHERE            BI.PropertyId = @propertyId 
                                        AND BI.BillingInvoiceTypeId IN @billingInvoiceTypeIds
                                        AND CAST(BI.EmissionDate AS DATE) = CAST(@emissionDate AS DATE)
                                        AND EXISTS(SELECT 1 FROM BILLINGITEM B
                                        WHERE B.PROPERTYID = BI.PropertyId
                                        AND B.BillingItemId = BAI.BillingItemId
                                        AND COALESCE(B.PAYMENTTYPEID,99)<>6) 
                       GROUP BY         IT.BillingItemName, IT.IntegrationCode, BA.BillingAccountId,BI.BillingInvoiceId
                                        having SUM(BAI.Amount)<>0";

            var query = await QueryAsync<InvoiceItem>(sql, new { propertyId = _applicationUser.PropertyId.TryParseToInt32(),
                emissionDate, paymentTypeId, billingInvoiceTypeIds = commaSeparatorIds
            });

            var result = query.ToList();

            return result;
        }

        public async Task<IEnumerable<BillingInvoice>> GetNFCeInvoices(DateTime emissionDate)
        {
            var stringDate = emissionDate.ToString("yyyy-MM-dd");
            var billingInvoiceTypeId = (int)BillingInvoiceTypeEnum.NFCE;

            var sql = @"SELECT BillingInvoiceId AS Id, BillingInvoiceNumber, BillingInvoiceSeries, ExternalNumber, 
                               ExternalSeries, ExternalEmissionDate, IntegratorLink, IntegratorXml 
                          FROM BillingInvoice
                         WHERE BillingInvoiceTypeId = @billingInvoiceTypeId
                           AND PropertyId = @propertyId
                           AND CAST(EmissionDate AS DATE) = CAST(@emissionDate AS DATE) ";

            var query = await QueryAsync<BillingInvoice>(sql, new { propertyId = _applicationUser.PropertyId.TryParseToInt32(), emissionDate, billingInvoiceTypeId });

            var result = query.ToList();

            return result;
        }

        private string GetSQLInvoiceBase(bool isFiscal)
        {
            var selectClauseSQL = string.Empty;
            var fromClauseSQL = string.Empty;
            var whereClauseSQL = string.Empty;            
            var orderByClauseSQL = string.Empty;

            selectClauseSQL = @"SELECT BI.BillingInvoiceId as Id, COALESCE(BI.ExternalRPS, BI.BillingInvoiceNumber) as BillingInvoiceNumber, BI.BillingInvoiceSeries, 
                                       COALESCE(RI.CheckInDate, RI.EstimatedArrivalDate, BA.StartDate) AS CheckinDate,
                                       COALESCE(RI.CheckOutDate, RI.EstimatedDepartureDate, BA.EndDate)  AS CheckoutDate,
                                       CAST(COALESCE(RI.CheckOutDate, RI.EstimatedDepartureDate, BA.EndDate) - COALESCE(RI.CheckInDate, BA.StartDate) AS INT) AS NumberOfNigths,
                                       COALESCE(CC.TradeName, GR.FullName) as PersonName, COALESCE(GR.FullName, GRP.FullName) AS GuestName, RE.ReservationCode, 
	                                   COALESCE(DOC.DocumentInformation, GR.DocumentInformation) as DocumentInformation, CC.ExternalCode as CompanyClientExternalCode, 
	                                   RP.DistributionCode as RatePlanCode, BI.EmissionDate, BAT.TypeName, BAT.BillingAccountTypeId, BA.BillingAccountId,
                                       DT.Name as DocumentTypeName, BI.ExternalNumber, BI.ExternalSeries, BI.ExternalEmissionDate, BI.CancelDate,
                                       0 as BaseCalculo, 0 as Aliquota, 0 as IssRetidoAmount, BI.IntegratorLink, BI.IntegratorXml ";

            if (isFiscal)
                selectClauseSQL = string.Concat(selectClauseSQL, @", LOC.StreetName, LOC.StreetNumber, LOC.AdditionalAddressDetails, LOC.Neighborhood, LOC.PostalCode, LOC.CityCode,
	                                                                 LOC.StateCode, LOC.CountryThreeLetterIsoCode, LOC.BR_CodigoBCB");


            fromClauseSQL = @" FROM BillingInvoice BI
                                    JOIN BillingAccount BA ON (BI.BillingAccountId = BA.BillingAccountId AND BI.PropertyId = BA.PropertyId)
                                    JOIN BillingAccountType BAT ON (BAT.BillingAccountTypeId = BA.BillingAccountTypeId)
	                                LEFT JOIN CompanyClient CC ON (CC.CompanyClientId = BA.CompanyClientId)
                                    LEFT JOIN Reservation RE ON (BA.ReservationId = RE.ReservationId)
                                    LEFT JOIN ReservationItem RI ON (RI.ReservationItemId = BA.ReservationItemId AND RI.ReservationId = RE.ReservationId)
                                    LEFT JOIN RatePlan RP ON (RP.RatePlanId = RI.RatePlanId AND RP.PropertyId = RE.PropertyId)
                                    LEFT JOIN RatePlanCompanyClient RPC ON (RPC.RatePlanId = RP.RatePlanId AND CC.CompanyClientId = RPC.CompanyClientId)
                                    LEFT JOIN (SELECT GRI.ReservationItemId, GRE.FullName, GRE.DocumentInformation, GRE.DocumentTypeId, GRI.GuestRegistrationId, GRE.PersonId, GRI.GuestReservationItemId
                                                FROM GuestReservationItem GRI
                                                     JOIN GuestRegistration GRE ON (GRE.GuestRegistrationId = GRI.GuestRegistrationId)
                                              ) GR ON (GR.ReservationItemId = RI.ReservationItemId and GR.GuestReservationItemId = BA.GuestReservationItemId)
                                   LEFT JOIN (SELECT GRI.ReservationItemId, COALESCE(GRE.FullName, GRI.GuestName) As FullName, GRE.DocumentInformation, GRE.DocumentTypeId, 
                                                     GRI.GuestRegistrationId, GRE.PersonId, GRI.GuestReservationItemId
                                                FROM GuestReservationItem GRI
                                                     LEFT JOIN GuestRegistration GRE ON (GRE.GuestRegistrationId = GRI.GuestRegistrationId )
                                                WHERE GRI.IsMain = 1
                                              ) GRP ON (GRP.ReservationItemId = RI.ReservationItemId)
                                    LEFT JOIN Document DOC ON (DOC.OwnerId = CC.PersonId)
                                    LEFT JOIN DocumentType DT ON (DT.DocumentTypeId = COALESCE(DOC.DocumentTypeId, GR.DocumentTypeId)) ";

            if (isFiscal)
                fromClauseSQL = string.Concat(fromClauseSQL, @" LEFT JOIN (SELECT LO.OwnerId, LO.StreetName, LO.StreetNumber, LO.AdditionalAddressDetails, 
                                                                           SUBSTRING(LO.Neighborhood, 0, 40) AS Neighborhood, LO.PostalCode, city.ExternalCode AS CityCode,        
		                                                                   state.ExternalCode as StateCode, country.ThreeLetterIsoCode as CountryThreeLetterIsoCode, 
                                                                           country.BR_CodigoBCB 
                                                                      FROM Location LO
					                                                       LEFT JOIN countrysubdivision city on (city.CountrySubdivisionId = LO.CityId)
						                                                   LEFT JOIN countrysubdivision state on (state.CountrySubdivisionId = LO.StateId)
						                                                   JOIN countrysubdivision country on (country.CountrySubdivisionId = LO.CountryId) ) LOC 
                                                                ON (LOC.OwnerId = COALESCE(BA.PersonHeaderId, CC.PersonId, GR.GuestRegistrationId))");

             if (isFiscal)
                whereClauseSQL = @" WHERE BI.PropertyId = @PropertyId
                                      AND BI.BillingInvoiceTypeId IN @billingInvoiceTypeIds
                                      AND CAST(BI.EmissionDate AS DATE) = CAST(@EmissionDate AS DATE)
                                      AND BI.ExternalNumber IS NOT NULL";
            else
                whereClauseSQL = @" WHERE BI.PropertyId = @propertyId
                                      AND BI.BillingInvoiceTypeId IN @billingInvoiceTypeIds
                                      AND (CAST(BI.EmissionDate AS DATE) = CAST(@emissionDate AS DATE) OR 
                                           CAST(BI.CancelDate AS DATE) = CAST(@emissionDate AS DATE))                                
                                      AND RP.RatePlanId IS NOT NULL
                                      AND RP.AgreementTypeId = 2
                                      AND BA.BillingAccountTypeId <> 2
                                      AND RPC.RatePlanId = RP.RatePlanId
                                      AND RPC.CompanyClientId = CC.CompanyClientId
                                      AND EXISTS(SELECT 1 FROM BILLINGACCOUNTITEM BA 
	                                              WHERE BA.BILLINGACCOUNTID = BI.BILLINGACCOUNTID 
				                                    AND BA.BILLINGITEMID = (SELECT BILLINGITEMID 
                                                                              FROM BILLINGITEM B 
                                                                             WHERE B.PROPERTYID = @propertyId 
                                                                               AND B.PAYMENTTYPEID = 6 ))";

            orderByClauseSQL = " ORDER BY BI.EmissionDate";

            return string.Concat(selectClauseSQL, fromClauseSQL, whereClauseSQL, orderByClauseSQL);
        }
    }
}
