﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Thex.CMNETIntegration.Dto;
using Thex.CMNETIntegration.Infra.Context;
using Thex.CMNETIntegration.Infra.Entities;
using Thex.CMNETIntegration.Infra.Interfaces.ReadRepositories;
using Thex.Kernel;
using Tnf.Dapper.Repositories;
using Tnf.Localization;
using Tnf.Repositories;

namespace Thex.CMNETIntegration.Infra.Repositories.ReadRepositories
{
    public class AccountingReadRepository : DapperEfRepositoryBase<ThexCMNETIntegrationContext, Accounting>, IAccountingReadRepository
    {
        private readonly ILocalizationManager _localizationManager;
        private readonly IApplicationUser _applicationUser;

        public AccountingReadRepository(
            IApplicationUser applicationUser,
            IActiveTransactionProvider activeTransactionProvider,
            ILocalizationManager localizationManager)
            : base(activeTransactionProvider)
        {
            _applicationUser = applicationUser;
            _localizationManager = localizationManager;
        }
        
        public async Task<IEnumerable<AccountingDto>> GetByItemDate(DateTime billingAccountItemDate)
        {            
            var itemDate = billingAccountItemDate.ToString("yyyy-MM-dd");

            var query = await QueryAsync<Accounting>(@"
                SELECT SUM(CASE WHEN BAID.INTEGRATIONCODE IS NULL THEN BAI.Amount ELSE BAID.AMOUNT END) AS AMOUNT,
                       COALESCE(BAID.INTEGRATIONCODE,IT.IntegrationCode) AS INTEGRATIONCODE,
                       CAST(BAI.BillingAccountItemDate AS DATE) AS BillingAccountItemDate,
                       CASE WHEN BAID.INTEGRATIONCODE IS NULL THEN IT.BILLINGITEMNAME ELSE NULL END AS billingItemName
                FROM   BillingAccountItem BAI
                       JOIN BillingItem IT ON (BAI.BillingItemId = IT.BillingItemId AND BAI.IsDeleted = 0)
                       LEFT JOIN ( SELECT (SUM(BD.AMOUNT) * -1) AS AMOUNT, PC.INTEGRATIONCODE,BD.BillingAccountItemId
                       FROM BillingAccountItemDetail BD
                       INNER JOIN Product PRO on (PRO.ProductId = BD.ProductId)
                       INNER JOIN PRODUCTCATEGORY PC ON PRO.PRODUCTCATEGORYID = PC.PRODUCTCATEGORYID
                       GROUP BY PC.INTEGRATIONCODE,BD.BillingAccountItemId) BAID ON (BAID.BillingAccountItemId = BAI.BillingAccountItemId)
                WHERE  IT.PropertyId = @propertyId
                       AND CAST(BAI.BillingAccountItemDate AS DATE) = CAST(@itemDate AS DATE)
                GROUP BY COALESCE(BAID.INTEGRATIONCODE,IT.IntegrationCode), CAST(BAI.BillingAccountItemDate AS DATE),
                       BAID.INTEGRATIONCODE,IT.BILLINGITEMNAME",
                new { propertyId = _applicationUser.PropertyId.TryParseToInt32(), itemDate });

            var result = query.ToList();

            return MapToDto(result);
        }

        private IEnumerable<AccountingDto> MapToDto(IEnumerable<Accounting> list)
        {
            var listAccounting = new List<AccountingDto>();

            foreach (var item in list)
                listAccounting.Add(item.MapTo<AccountingDto>());

            return listAccounting;
        }
    }
}
