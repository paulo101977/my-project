﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Thex.CMNETIntegration.Dto;
using Thex.CMNETIntegration.Infra.Context;
using Thex.CMNETIntegration.Infra.Entities;
using Thex.CMNETIntegration.Infra.Interfaces.ReadRepositories;
using Thex.Kernel;
using Tnf.Dapper.Repositories;
using Tnf.Repositories;

namespace Thex.CMNETIntegration.Infra.Repositories.ReadRepositories
{
    public class CompanyClientReadRepository : DapperEfRepositoryBase<ThexCMNETIntegrationContext, CompanyClient>, ICompanyClientReadRepository
    {
        private readonly IApplicationUser _applicationUser;
        
        public CompanyClientReadRepository(
            IApplicationUser applicationUser,
            IActiveTransactionProvider activeTransactionProvider) : base(activeTransactionProvider)
        {
            _applicationUser = applicationUser;
        }

        public async Task<CompanyClientDto> Get(string document, int documentType)
        {
            var query = await QueryAsync<CompanyClient>(@"
                SELECT CC.CompanyClientId AS Id, CC.CompanyId, CC.PersonId, PR.PropertyId, CC.PersonType, 
                       CC.ShortName, CC.TradeName
                  FROM CompanyClient CC
                       JOIN Company CO  ON ( CO.CompanyId = CC.CompanyId )
	                   JOIN Property PR ON ( PR.CompanyId = CC.CompanyId )
	                   JOIN Document DO ON ( DO.OwnerId = CC.PersonId AND DO.DocumentTypeId = @DocType )
                 WHERE REPLACE(REPLACE(REPLACE(REPLACE( DO.DocumentInformation, '.', ''), '-', ''), '\', ''), '/', '') = 
                         REPLACE(REPLACE(REPLACE(REPLACE( @DocInformation, '.', ''), '-', ''), '\', ''), '/', '')
                   AND PR.PropertyId = @PropertyID",
                new { DocType = documentType, DocInformation = document, PropertyID = _applicationUser.PropertyId.TryParseToInt32() });

            var result = MapToDto(query);

            return result.FirstOrDefault();
        }

        private IEnumerable<CompanyClientDto> MapToDto(IEnumerable<CompanyClient> list)
        {
            var listCompanyClient = new List<CompanyClientDto>();

            foreach (var item in list)
                listCompanyClient.Add(item.MapTo<CompanyClientDto>());

            return listCompanyClient;
        }
    }
}
