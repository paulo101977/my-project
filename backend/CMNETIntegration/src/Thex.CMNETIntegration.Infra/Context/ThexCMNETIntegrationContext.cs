﻿
using Microsoft.EntityFrameworkCore;
using Thex.CMNETIntegration.Infra.Entities;
using Tnf.EntityFrameworkCore;
using Tnf.Runtime.Session;

namespace Thex.CMNETIntegration.Infra.Context
{
    public class ThexCMNETIntegrationContext : TnfDbContext
    {
        public DbSet<BillingAccount> BillingAccounts { get; set; }
        public DbSet<CompanyClient> CompanyClients { get; set; }
        public DbSet<BillingAccountItem> BillingAccountItems { get; set; }

        // Importante o construtor do contexto receber as opções com o tipo generico definido: DbContextOptions<TDbContext>
        public ThexCMNETIntegrationContext(DbContextOptions<ThexCMNETIntegrationContext> options, ITnfSession session)
            : base(options, session)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<BillingAccount>(a =>
            {
                a.ToTable("BillingAccount");

                a.HasAnnotation("Relational:TableName", "BillingAccount");

                a.HasIndex(e => e.BillingAccountTypeId)
                    .HasName("x_BillingAccount_BillingAccountTypeId");

                a.HasIndex(e => e.BusinessSourceId)
                    .HasName("x_BillingAccount_BusinessSourceId");

                a.HasIndex(e => e.CompanyClientId)
                    .HasName("x_BillingAccount_CompanyClientId");

                a.HasIndex(e => e.GroupKey)
                    .HasName("x_BillingAccount_GroupKey");

                a.HasIndex(e => e.GuestReservationItemId)
                    .HasName("x_BillingAccount_GuestReservationItemId");

                a.HasIndex(e => e.MarketSegmentId)
                    .HasName("x_BillingAccount_MarketSegmentId");

                a.HasIndex(e => e.PropertyId)
                    .HasName("x_BillingAccount_PropertyId");

                a.HasIndex(e => e.ReservationId)
                    .HasName("x_BillingAccount_ReservationId");

                a.HasIndex(e => e.ReservationItemId)
                    .HasName("x_BillingAccount_ReservationItemId");

                a.HasIndex(e => e.StatusId)
                    .HasName("x_BillingAccount_StatusId");

                a.HasIndex(e => e.TenantId)
                    .HasName("x_BillingAccount_TenantId");

                a.Property(e => e.Id)
                    .HasAnnotation("Relational:ColumnName", "BillingAccountId");

                a.Property(e => e.BillingAccountName)
                    .HasMaxLength(150)
                    .IsUnicode(false)
                    .HasAnnotation("Relational:ColumnName", "BillingAccountName");

                a.Property(e => e.BillingAccountTypeId).HasAnnotation("Relational:ColumnName", "BillingAccountTypeId");

                a.Property(e => e.Blocked).HasAnnotation("Relational:ColumnName", "Blocked");

                a.Property(e => e.BusinessSourceId).HasAnnotation("Relational:ColumnName", "BusinessSourceId");

                a.Property(e => e.CompanyClientId).HasAnnotation("Relational:ColumnName", "CompanyClientId");

                a.Property(e => e.CreationTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getutcdate())")
                    .HasAnnotation("Relational:ColumnName", "CreationTime");

                a.Property(e => e.CreatorUserId).HasAnnotation("Relational:ColumnName", "CreatorUserId");

                a.Property(e => e.DeleterUserId).HasAnnotation("Relational:ColumnName", "DeleterUserId");

                a.Property(e => e.DeletionTime)
                    .HasColumnType("datetime")
                    .HasAnnotation("Relational:ColumnName", "DeletionTime");

                a.Property(e => e.EndDate)
                    .HasColumnType("datetime")
                    .HasAnnotation("Relational:ColumnName", "EndDate");

                a.Property(e => e.GroupKey).HasAnnotation("Relational:ColumnName", "GroupKey");

                a.Property(e => e.GuestReservationItemId).HasAnnotation("Relational:ColumnName", "GuestReservationItemId");

                a.Property(e => e.IsDeleted).HasAnnotation("Relational:ColumnName", "IsDeleted");

                a.Property(e => e.IsMainAccount)
                    .HasAnnotation("Relational:ColumnName", "IsMainAccount");

                a.Property(e => e.LastModificationTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getutcdate())")
                    .HasAnnotation("Relational:ColumnName", "LastModificationTime");

                a.Property(e => e.LastModifierUserId).HasAnnotation("Relational:ColumnName", "LastModifierUserId");

                a.Property(e => e.MarketSegmentId).HasAnnotation("Relational:ColumnName", "MarketSegmentId");

                a.Property(e => e.PropertyId).HasAnnotation("Relational:ColumnName", "PropertyId");

                a.Property(e => e.ReopeningDate)
                    .HasColumnType("datetime")
                    .HasAnnotation("Relational:ColumnName", "ReopeningDate");

                a.Property(e => e.ReopeningReasonId).HasAnnotation("Relational:ColumnName", "ReopeningReasonId");

                a.Property(e => e.ReservationId).HasAnnotation("Relational:ColumnName", "ReservationId");

                a.Property(e => e.ReservationItemId).HasAnnotation("Relational:ColumnName", "ReservationItemId");

                a.Property(e => e.StartDate)
                    .HasColumnType("datetime")
                    .HasAnnotation("Relational:ColumnName", "StartDate");

                a.Property(e => e.StatusId).HasAnnotation("Relational:ColumnName", "StatusId");

                a.Property(e => e.TenantId).HasAnnotation("Relational:ColumnName", "TenantId");


            });

            modelBuilder.Entity<BillingAccountItem>(b =>
            {
                b.ToTable("BillingAccountItem");

                b.HasAnnotation("Relational:TableName", "BillingAccountItem");

                b.HasIndex(e => e.BillingAccountId)
                    .HasName("x_BillingAccountItem_BillingAccountId");

                b.HasIndex(e => e.BillingAccountItemTypeId)
                    .HasName("x_BillingAccountItem_BillingAccountItemTypeId");

                b.HasIndex(e => e.BillingItemId)
                    .HasName("x_BillingAccountItem_BillingItemId");

                b.Property(e => e.Id)
                    .HasAnnotation("Relational:ColumnName", "BillingAccountItemId");

                b.Property(e => e.BillingAccountItemAmount)
                    .HasColumnType("numeric(18, 4)")
                    .HasAnnotation("Relational:ColumnName", "Amount");

                b.Property(e => e.BillingAccountId).HasAnnotation("Relational:ColumnName", "BillingAccountId");

                b.Property(e => e.BillingAccountItemDate)
                    .HasColumnType("datetime")
                    .HasAnnotation("Relational:ColumnName", "BillingAccountItemDate");

                b.Property(e => e.BillingAccountItemTypeId).HasAnnotation("Relational:ColumnName", "BillingAccountItemTypeId");

                b.Property(e => e.BillingItemId).HasAnnotation("Relational:ColumnName", "BillingItemId");

            });
        }
    }
}
