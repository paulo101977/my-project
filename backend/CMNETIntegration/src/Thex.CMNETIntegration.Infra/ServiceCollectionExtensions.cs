﻿using Thex.Common;
using Thex.CMNETIntegration.Infra.Context;
using Thex.CMNETIntegration.Infra.Interfaces;
using Thex.CMNETIntegration.Infra.Interfaces.ReadRepositories;
using Thex.CMNETIntegration.Infra.Mappers.DapperMappers;
using Thex.CMNETIntegration.Infra.Mappers.Profiles;
using Thex.CMNETIntegration.Infra.Repositories;
using Thex.CMNETIntegration.Infra.Repositories.ReadRepositories;
using Tnf.Dapper;

namespace Microsoft.Extensions.DependencyInjection
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddInfraDependency(this IServiceCollection services)
        {
            // Configura o uso do Dapper registrando os contextos que serão
            // usados pela aplicação
            services
                .AddTnfEntityFrameworkCore()
                .AddTnfDbContext<ThexCMNETIntegrationContext>(config => DbContextConfigurer.Configure(config))
                .AddTnfDapper(options =>
                {
                    //options.MapperAssemblies.Add(typeof(TransportationTypeMapper).Assembly);
                    options.DbType = DapperDbType.SqlServer;
                });

            services.AddTnfAutoMapper(config =>
            {
                config.AddProfile<AccountingProfile>();
                config.AddProfile<CompanyClientProfile>();
                config.AddProfile<BillingAccountItemProfile>();
                config.AddProfile<BillingInvoiceProfile>();
                config.AddProfile<BillingItemProfile>();
            });

            //Read Repositories
            services.AddTransient<IAccountingReadRepository, AccountingReadRepository>();
            services.AddTransient<IPropertyReadRepository, PropertyReadRepository>();
            services.AddTransient<IBillingAccountReadRepository, BillingAccountReadRepository>();
            services.AddTransient<ICompanyClientReadRepository, CompanyClientReadRepository>();
            services.AddTransient<IBillingAccountItemReadRepository, BillingAccountItemReadRepository>();
            services.AddTransient<IPropertyCurrencyExchangeReadRepository, PropertyCurrencyExchangeReadRepository>();
            services.AddTransient<IBillingItemReadRepository, BillingItemReadRepository>();
            services.AddTransient<IBillingInvoiceReadRepository, BillingInvoiceReadRepository>();
            services.AddTransient<IFinancialReadRepository, FinancialReadRepository>();

            //Repositories
            services.AddTransient<IBillingAccountRepository, BillingAccountRepository>();
            services.AddTransient<IBillingAccountItemRepository, BillingAccountItemRepository>();
            services.AddTransient<IBillingInvoiceRepository, BillingInvoiceRepository>();

            services.AddTransient<IHealthReadRepository, HealthReadRepository>();

            return services;
        }
    }
}
