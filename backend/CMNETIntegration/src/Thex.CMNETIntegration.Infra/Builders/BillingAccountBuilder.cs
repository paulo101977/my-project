﻿using System;
using Thex.Common;
using Tnf.Builder;
using Tnf.Notifications;
using Tnf.Specifications;

namespace Thex.CMNETIntegration.Infra.Entities
{
    public partial class BillingAccount
    {
        public class Builder : Builder<BillingAccount>
        {
            public Builder(INotificationHandler handler) : base(handler)
            {
            }

            public Builder(INotificationHandler handler, BillingAccount instance) : base(handler, instance)
            {
            }

            public virtual Builder WithId(Guid id)
            {
                Instance.Id = id;
                return this;
            }

            public virtual Builder WithCompanyClientId(Guid? companyClientId)
            {
                Instance.CompanyClientId = companyClientId;
                return this;
            }

            public virtual Builder WithStartDate(DateTime startDate)
            {
                Instance.StartDate = startDate;
                return this;
            }

            public virtual Builder WithBillingAccountType(int billingAccountTypeId)
            {
                Instance.BillingAccountTypeId = billingAccountTypeId;
                return this;
            }

            public virtual Builder WithBillingAccountName(string billingAccountName)
            {
                Instance.BillingAccountName = billingAccountName;
                return this;
            } 

            protected override void Specifications()
            {
                AddSpecification(new ExpressionSpecification<BillingAccount>(
                    AppConsts.LocalizationSourceName,
                    BillingAccount.EntityError.BillingAccountMustHaveEventAccountName,
                    a => !string.IsNullOrEmpty(a.BillingAccountName)));

                AddSpecification(new ExpressionSpecification<BillingAccount>(
                    AppConsts.LocalizationSourceName,
                    BillingAccount.EntityError.BillingAccountMustHaveStartDate,
                    a => a.StartDate <= DateTime.MinValue));                
            }
        }
    }
}
