﻿using System;
using Thex.Common;
using Tnf.Builder;
using Tnf.Notifications;
using Tnf.Specifications;

namespace Thex.CMNETIntegration.Infra.Entities
{
    public partial class BillingAccountItem
    {
        public class Builder : Builder<BillingAccountItem>
        {
            public Builder(INotificationHandler handler) : base(handler)
            {
            }

            public Builder(INotificationHandler handler, BillingAccountItem instance) : base(handler, instance)
            {
            }

            public virtual Builder WithId(Guid id)
            {
                Instance.Id = id;
                return this;
            }

            public virtual Builder WithBillingAccountAmount(decimal amount)
            {
                Instance.BillingAccountItemAmount = amount;
                return this;
            }

            public virtual Builder WithBillingAccountId(Guid billingAccountId)
            {
                Instance.BillingAccountId = billingAccountId;
                return this;
            }

            public virtual Builder WithBillingAccountItemDated(DateTime billingAccountItemDate)
            {
                Instance.BillingAccountItemDate = billingAccountItemDate;
                return this;
            }

            public virtual Builder WithBillingAccountItemTypeId(int billingAccountItemTypeId)
            {
                Instance.BillingAccountItemTypeId = billingAccountItemTypeId;
                return this;
            }

            public virtual Builder WithBillingItemId(int billingItemId)
            {
                Instance.BillingItemId = billingItemId;
                return this;
            }

            public virtual Builder WithIntegrationCode(string integrationCode)
            {
                Instance.IntegrationCode = integrationCode;
                return this;
            }

            public virtual Builder WithNSU(string nsu)
            {
                Instance.NSU = nsu;
                return this;
            }

            public virtual Builder WithCheckNumber(string checkNumber)
            {
                Instance.CheckNumber = checkNumber;
                return this;
            }

            public virtual Builder WithServiceDescription(string serviceDescription)
            {
                Instance.ServiceDescription = serviceDescription;
                return this;
            }

            public virtual Builder WithBillingItemIdd(int billingItemId)
            {
                Instance.BillingItemId = billingItemId;
                return this;
            }

            public virtual Builder WithPlasticBrandName(string plasticBrandName)
            {
                Instance.PlasticBrandName = plasticBrandName;
                return this;
            }

            public virtual Builder WithInstallmentsQuantitye(int installmentsQuantity)
            {
                Instance.InstallmentsQuantity = installmentsQuantity;
                return this;
            }

            public string ServiceDescription { get; internal set; }
            public string IntegrationDetail { get; internal set; }
            protected override void Specifications()
            {
                AddSpecification(new ExpressionSpecification<BillingAccountItem>(
                    AppConsts.LocalizationSourceName,
                    BillingAccountItem.EntityError.BillingAccountItemMustHaveBillingAccountItemTypeId,
                    w => w.BillingAccountItemTypeId != default(int)));
                               
                AddSpecification(new ExpressionSpecification<BillingAccountItem>(
                    AppConsts.LocalizationSourceName,
                    BillingAccountItem.EntityError.BillingAccountItemMustHaveBillingAccountId,
                    w => w.BillingAccountId != default(Guid)));

                AddSpecification(new ExpressionSpecification<BillingAccountItem>(
                    AppConsts.LocalizationSourceName,
                    BillingAccountItem.EntityError.BillingAccountItemInvalidBillingAccountItemDate,
                    w => w.BillingAccountItemDate >= new DateTime(1753, 1, 1) && w.BillingAccountItemDate <= DateTime.MaxValue));                               
            }
        }
    }
}
