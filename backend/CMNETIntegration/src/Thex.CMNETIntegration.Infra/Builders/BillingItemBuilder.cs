﻿using System;
using Thex.Common;
using Tnf.Builder;
using Tnf.Notifications;
using Tnf.Specifications;

namespace Thex.CMNETIntegration.Infra.Entities
{
    public partial class BillingItem
    {
        public class Builder : Builder<BillingItem>
        {
            public Builder(INotificationHandler handler) : base(handler)
            {
            }

            public Builder(INotificationHandler handler, BillingItem instance) : base(handler, instance)
            {
            }

            public virtual Builder WithId(int id)
            {
                Instance.Id = id;
                return this;
            }

            public virtual Builder WithBillingItemTypeId(int billingItemTypeId)
            {
                Instance.BillingItemTypeId = billingItemTypeId;
                return this;
            }            

            public virtual Builder WithBillingItemName(string billingItemName)
            {
                Instance.BillingItemName = billingItemName;                
                return this;
            }

            public virtual Builder WithIntegrationCode(string integrationCode)
            {
                Instance.IntegrationCode = integrationCode;
                return this;
            }

            public virtual Builder WithPaymentTypeId(int paymentTypeId)
            {
                Instance.PaymentTypeId = paymentTypeId;
                return this;
            }

            protected override void Specifications()
            {
                AddSpecification(new ExpressionSpecification<BillingItem>(
                    AppConsts.LocalizationSourceName,
                    BillingAccountItem.EntityError.BillingAccountItemMustHaveBillingAccountItemTypeId,
                    w => w.IntegrationCode.IsNullOrEmpty()));                
            }
        }
    }
}
