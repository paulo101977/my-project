﻿using System;
using Thex.Common;
using Tnf.Builder;
using Tnf.Notifications;
using Tnf.Specifications;

namespace Thex.CMNETIntegration.Infra.Entities
{
    public partial class Accounting
    {
        public class Builder : Builder<Accounting>
        {
            public Builder(INotificationHandler handler) : base(handler)
            {
            }

            public Builder(INotificationHandler handler, Accounting instance) : base(handler, instance)
            {
            }

            public virtual Builder WithIntegrationCode(string integrationCode)
            {
                Instance.IntegrationCode = integrationCode;
                return this;
            }

            public virtual Builder WithBillingAccountItemDate(DateTime billingAccountItemDate)
            {
                Instance.BillingAccountItemDate = billingAccountItemDate;
                return this;
            }

            public virtual Builder WithAmount(decimal amount)
            {
                Instance.Amount = amount;
                return this;
            }
            
            protected override void Specifications()
            {
                AddSpecification(new ExpressionSpecification<Accounting>(
                    AppConsts.LocalizationSourceName,
                    Accounting.EntityError.AccountingMustHaveAccountingAmount,
                    w => w.Amount > 0));

                AddSpecification(new ExpressionSpecification<Accounting>(
                    AppConsts.LocalizationSourceName,
                    Accounting.EntityError.AccountingMustHaveAccountingCreationTime,
                    w => w.BillingAccountItemDate <= DateTime.MinValue));

                AddSpecification(new ExpressionSpecification<Accounting>(
                    AppConsts.LocalizationSourceName,
                    Accounting.EntityError.AccountingOutOfBound,
                    w => w.Amount > 0 && w.BillingAccountItemDate <= DateTime.MinValue));
            }
        }
    }
}
