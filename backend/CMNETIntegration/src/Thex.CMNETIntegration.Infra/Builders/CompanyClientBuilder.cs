﻿using System;
using Thex.Common;
using Tnf.Builder;
using Tnf.Notifications;
using Tnf.Specifications;

namespace Thex.CMNETIntegration.Infra.Entities
{
    public partial class CompanyClient
    {
        public class Builder : Builder<CompanyClient>
        {
            public Builder(INotificationHandler handler) : base(handler)
            {
            }

            public Builder(INotificationHandler handler, CompanyClient instance) : base(handler, instance)
            {
            }

            public virtual Builder WithId(Guid id)
            {
                Instance.Id = id;
                return this;
            }

            public virtual Builder WithCompanyId(int companyId)
            {
                Instance.CompanyId = companyId;
                return this;
            }

            public virtual Builder WithPersonId(Guid personId)
            {
                Instance.PersonId = personId;
                return this;
            }

            public virtual Builder WithPersonType(char personType)
            {
                Instance.PersonType = personType;
                return this;
            }
            
            public virtual Builder WithPropertyId(int propertyId)
            {
                Instance.PropertyId = propertyId;
                return this;
            }

            public virtual Builder WithShortName(string shortName)
            {
                Instance.ShortName = shortName;
                return this;
            }

            public virtual Builder WithTradeName(string tradeName)
            {
                Instance.TradeName = tradeName;
                return this;
            }

            protected override void Specifications()
            {
                AddSpecification(new ExpressionSpecification<CompanyClient>(
                    AppConsts.LocalizationSourceName,
                    EntityError.CompanyClientMustHaveCompanyId,
                    a => a.CompanyId <= 0));

                AddSpecification(new ExpressionSpecification<CompanyClient>(
                    AppConsts.LocalizationSourceName,
                    EntityError.CompanyClientOutOfBound,
                    a => Equals(a.PersonId, Guid.Empty) ));
            }
        }
    }
}
