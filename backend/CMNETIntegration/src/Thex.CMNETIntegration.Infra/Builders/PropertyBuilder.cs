﻿using Thex.Common;
using Tnf.Builder;
using Tnf.Notifications;
using Tnf.Specifications;

namespace Thex.CMNETIntegration.Infra.Entities
{
    public partial class Property
    {
        public class Builder : Builder<Property>
        {
            public Builder(INotificationHandler handler) : base(handler)
            {
            }

            public Builder(INotificationHandler handler, Property instance) : base(handler, instance)
            {
            }

            public virtual Builder WithId(int id)
            {
                Instance.Id = id;
                return this;
            }

            public virtual Builder WithAll(int id, string documentInfo, string name)
            {
                Instance.Id = id;
                Instance.DocumentInformation = documentInfo;
                Instance.PropertyName = name;
                return this;
            }

            protected override void Specifications()
            {
                AddSpecification(new ExpressionSpecification<Property>(
                    AppConsts.LocalizationSourceName,
                    Property.EntityError.PropertyMustHavePropertyDocumentInformation,
                    w => string.IsNullOrEmpty(w.DocumentInformation)));

                AddSpecification(new ExpressionSpecification<Property>(
                    AppConsts.LocalizationSourceName,
                    Property.EntityError.PropertyMustHavePropertyName,
                    w => string.IsNullOrEmpty(w.PropertyName)));
            }
        }
    }
}
