﻿using System;

namespace Thex.CMNETIntegration.Infra.Entities
{
    public class BaseEntity : ThexFullAuditedEntity, IEntityGuid
    {
        public Guid Id { get; set; }
    }
}
