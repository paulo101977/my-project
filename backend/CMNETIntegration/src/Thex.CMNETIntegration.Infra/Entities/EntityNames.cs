﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a Crudzilla tool.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Thex.CMNETIntegration.Infra.Entities
{
    public partial class EntityNames
    {
        public const string Health = "Health check";
        public const string CustomerStation = "CustomerStation";
        public const string BillingAccount = "BillingAccount";
        public const string BillingAccountForEvent = "BillingAccountForEvent";
        public const string BillingAccountItem = "Billing Account Item";
        public const string BillingInvoice = "Billing Invoice";
    }
}
