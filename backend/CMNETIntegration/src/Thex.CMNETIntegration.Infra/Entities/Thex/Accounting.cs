﻿using System;
using Tnf.Notifications;

namespace Thex.CMNETIntegration.Infra.Entities
{
    public partial class Accounting : IEntity
    {
        public string IntegrationCode { get; set; }
        public string BillingItemName { get; set; }
        public DateTime BillingAccountItemDate { get; set; }
        public decimal Amount { get; set; }        

        public enum EntityError
        {
            AccountingMustHaveAccountingCreationTime,
            AccountingMustHaveAccountingAmount,
            AccountingOutOfBound,
        }

        public static Builder Create(INotificationHandler handler)
            => new Builder(handler);

        public static Builder Create(INotificationHandler handler, Accounting instance)
            => new Builder(handler, instance);
    }
}
