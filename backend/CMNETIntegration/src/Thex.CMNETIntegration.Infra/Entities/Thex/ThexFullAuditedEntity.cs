﻿using System;

namespace Thex.CMNETIntegration.Infra.Entities
{
    [Serializable]
    public abstract class ThexFullAuditedEntity
    {
        public virtual bool IsDeleted { get; set; }

        public virtual DateTime CreationTime { get; set; }

        public virtual Guid? CreatorUserId { get; set; }

        public virtual DateTime? LastModificationTime { get; set; }

        public virtual Guid? LastModifierUserId { get; set; }

        public virtual DateTime? DeletionTime { get; set; }

        public virtual Guid? DeleterUserId { get; set; }
    }
}
