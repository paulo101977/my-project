﻿using System;
using Tnf.Notifications;

namespace Thex.CMNETIntegration.Infra.Entities
{
    public partial class BillingAccountItem : IEntityGuid
    {
        public BillingAccountItem()
        {
        }

        public Guid Id { get; set; }
        public Guid BillingAccountId { get; internal set; }        
        public int? BillingItemId { get; internal set; }
        public int BillingAccountItemTypeId { get; internal set; }        
        public decimal BillingAccountItemAmount { get; internal set; }                
        public DateTime BillingAccountItemDate { get; internal set; }
        public string BillingItemName { get; internal set; }
        public string IntegrationCode { get; internal set; }
        public string NSU { get; internal set; }
        public string CheckNumber { get; internal set; }
        public string ServiceDescription { get; internal set; }
        public string IntegrationDetail { get; internal set; }
        public string PlasticBrandName { get; internal set; }
        public int InstallmentsQuantity { get; internal set; }

        public enum EntityError
        {
            BillingAccountItemMustHaveBillingAccountId,
            BillingAccountItemMustHaveBillingAccountItemTypeId,            
            BillingAccountItemInvalidBillingAccountItemDate
        }

        public static Builder Create(INotificationHandler handler)
            => new Builder(handler);

        public static Builder Create(INotificationHandler handler, BillingAccountItem instance)
            => new Builder(handler, instance);
    }
}
