﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Thex.CMNETIntegration.Infra.Entities
{
    public class InvoiceItem : IEntityGuid
    {
        public Guid Id { get; set; }
        public decimal Amount { get; set; }
        public string BillingItemName { get; set; }
        public string IntegrationCode { get; set; }
        public Guid BillingAccountId { get; set; }
        public Guid BillingInvoiceId { get; set; }
        public int PaymentTypeId { get; set; }
        public DateTime BillingAccountItemDate { get; set; }
        public string ExternalNumber { get; set; }
    }

    public class BillingInvoice : IEntityGuid
    {
        public BillingInvoice()
        {
            InvoiceItems = new List<InvoiceItem>();
        }
        public Guid Id { get; set; }
        public Guid BillingAccountId { get; set; }
        public DateTime PaymentDate { get; set; }
        public DateTime EmissionDate { get; set; }
        public DateTime? CancelDate { get; set; }
        public decimal InvoiceAmount { get; set; }
        public string BillingInvoiceNumber { get; set; }
        public string BillingInvoiceSeries { get; set; }
        public string CompanyClientExternalCode { get; set; }
        public string RatePlanCode { get; set; }
        public DateTime CheckinDate { get; set; }
        public DateTime CheckoutDate { get; set; }        
        public int NumberOfNigths { get; set; }
        public string PersonName { get; set; }
        public string GuestName { get; set; }
        public string ReservationCode { get; set; }
        public string DocumentInformation { get; set; }
        public string DocumentTypeName { get; set; }
        public string ExternalNumber { get; set; }
        public string ExternalSeries { get; set; }
        public string ExternalEmissionDate { get; set; }
        public decimal BaseCalculo { get; set; }
        public decimal Aliquota { get; set; }
        public decimal IssRetidoAmount { get; set; }
        public int BR_CodigoBCB { get; set; }
        public string IntegratorXml { get; set; }
        public string IntegratorLink { get; set; }

        public List<InvoiceItem> InvoiceItems { get; set; }
    }

    public class BillingFiscalInvoice : BillingInvoice
    {
        public BillingFiscalInvoice()
        {
            InvoiceItems = new List<InvoiceItem>();
        }              
        
        public string StreetName { get; set; }
        public string StreetNumber { get; set; }
        public string AdditionalAddressDetails { get; set; }
        public string Neighborhood { get; set; }
        public string PostalCode { get; set; }
        public string CityCode { get; set; }
        public string StateCode { get; set; }
        public string CountryThreeLetterIsoCode { get; set; }
    }
}
