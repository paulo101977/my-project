﻿using System;
using System.Collections.Generic;
using System.Text;
using Thex.CMNETIntegration.Infra.Entities;
using Tnf.Notifications;

namespace Thex.CMNETIntegration.Infra.Entities
{
    public partial class BillingItem : IEntityInt
    {
        public int Id { get; set; }
        public int BillingItemTypeId { get; set; }
        public string BillingItemName { get; set; }
        public string IntegrationCode { get; set; }
        public int PaymentTypeId { get; set; }

        public enum EntityError
        {
            PropertyMustHavePropertyDocumentInformation,
            PropertyMustHavePropertyName,
            PropertyOutOfBound,
        }

        public static Builder Create(INotificationHandler handler)
            => new Builder(handler);

        public static Builder Create(INotificationHandler handler, BillingItem instance)
            => new Builder(handler, instance);
    }
}
