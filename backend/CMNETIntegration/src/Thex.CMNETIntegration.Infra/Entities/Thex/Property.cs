﻿using Tnf.Notifications;

namespace Thex.CMNETIntegration.Infra.Entities
{
    public partial class Property : IEntityInt
    {
        public Property()
        {
        }

        public Property(int id, string documentInfo, string name)
        {
            Id = id;
            DocumentInformation = documentInfo;
            PropertyName = name;
        }
        public int Id { get; set; }
        public string DocumentInformation { get; set; }
        public string PropertyName { get; set; }

        public enum EntityError
        {
            PropertyMustHavePropertyDocumentInformation,
            PropertyMustHavePropertyName,
            PropertyOutOfBound,
        }

        public static Builder Create(INotificationHandler handler)
            => new Builder(handler);

        public static Builder Create(INotificationHandler handler, Property instance)
            => new Builder(handler, instance);
    }
}
