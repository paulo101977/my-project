﻿using System;
using Tnf.Notifications;

namespace Thex.CMNETIntegration.Infra.Entities
{
    public partial class CompanyClient : IEntityGuid
    {
        public CompanyClient()
        {
        }

        public CompanyClient(Guid id, int companyId, Guid personId, int propertyId, char personType, string shortName, string tradeName)
        {
            Id = id;
            CompanyId = companyId;
            PersonId = personId;
            PropertyId = propertyId;
            PersonType = personType;
            ShortName = shortName;
            TradeName = tradeName;
        }

        public static CompanyClient NullInstance = null;

        public Guid Id { get; set; }
        public int CompanyId { get; set; }
        public Guid PersonId { get; set; }
        public int PropertyId { get; set; }

        private char _personType;           
        public char PersonType
        {
            get
            {
                return this._personType.ToString().ToUpper()[0];
            }
            set
            {
                this._personType = value;
            }
        }
        public string ShortName { get; set; }
        public string TradeName { get; set; }        

        public enum EntityError
        {
            CompanyClientMustHaveCompanyId,
            CompanyClientOutOfBound
        }

        public static Builder Create(INotificationHandler handler)
            => new Builder(handler);

        public static Builder Create(INotificationHandler handler, CompanyClient instance)
            => new Builder(handler, instance);
    }
}
