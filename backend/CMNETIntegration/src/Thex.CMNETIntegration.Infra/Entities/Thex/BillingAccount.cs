﻿using System;
using System.Collections.Generic;
using Tnf.Notifications;

namespace Thex.CMNETIntegration.Infra.Entities
{
    public partial class BillingAccount : IEntityGuid
    {
        public BillingAccount()
        {

        }

        public Guid Id { get; set; }
        public Guid TenantId { get; set; }
        public Guid? CompanyClientId { get; set; }
        public long? ReservationId { get; set; }
        public long? ReservationItemId { get; set; }
        public long? GuestReservationItemId { get; set; }
        public int BillingAccountTypeId { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime? EndDate { get; set; }
        public int StatusId { get; set; }
        public bool IsMainAccount { get; set; }
        public int MarketSegmentId { get; set; }
        public int BusinessSourceId { get; set; }
        public int PropertyId { get; set; }
        public Guid GroupKey { get; set; }
        public bool Blocked { get; set; }
        public string BillingAccountName { get; set; }

        public int? ReopeningReasonId { get; set; }
        public DateTime? ReopeningDate { get; set; }
        //public bool? CreateMainAccountOfCompanyInReservationItem { get; set; }

        public virtual DateTime CreationTime { get; set; }
        public virtual Guid? CreatorUserId { get; set; }
        public virtual Guid? DeleterUserId { get; set; }
        public virtual DateTime? DeletionTime { get; set; }
        public virtual bool IsDeleted { get; set; }
        public virtual DateTime? LastModificationTime { get; set; }
        public virtual Guid? LastModifierUserId { get; set; }

        public IEnumerable<BillingAccountItem> BillingAccountItems { get; set; }

        public enum EntityError
        {
            BillingAccountMustHaveStartDate,
            BillingAccountMustHaveEventAccountName
        }

        public static Builder Create(INotificationHandler handler)
            => new Builder(handler);

        public static Builder Create(INotificationHandler handler, BillingAccount instance)
            => new Builder(handler, instance);
    }
}
