﻿using DapperExtensions.Mapper;
using System;
using System.Collections.Generic;
using System.Text;
using Thex.CMNETIntegration.Infra.Entities;

namespace Thex.CMNETIntegration.Infra.Mappers.DapperMappers
{
    public class BillingAccountMapper : ClassMapper<BillingAccount>
    {
        public BillingAccountMapper()
        {
            Table("BillingAccount");
            Map(x => x.Id).Column("BillingAccountId").Key(KeyType.Identity);
            AutoMap();
        }
    }
}
