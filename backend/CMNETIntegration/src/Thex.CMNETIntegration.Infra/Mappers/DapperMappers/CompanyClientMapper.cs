﻿using DapperExtensions.Mapper;
using Thex.CMNETIntegration.Infra.Entities;

namespace Thex.CMNETIntegration.Infra.Mappers.DapperMappers
{
    public class CompanyClientMapper : ClassMapper<CompanyClient>
    {
        public CompanyClientMapper()
        {
            Table("CompanyClient");
            Map(x => x.Id).Column("CompanyClientId").Key(KeyType.Identity);
            AutoMap();
        }
    }
}
