﻿using AutoMapper;
using Thex.CMNETIntegration.Dto;
using Thex.CMNETIntegration.Infra.Entities;

namespace Thex.CMNETIntegration.Infra.Mappers.Profiles
{
    class CompanyClientProfile : Profile
    {
        public CompanyClientProfile()
        {
            CreateMap<CompanyClient, CompanyClientDto>();
        }
    }
}
