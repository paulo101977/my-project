﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using Thex.CMNETIntegration.Dto;
using Thex.CMNETIntegration.Infra.Entities;

namespace Thex.CMNETIntegration.Infra.Mappers.Profiles
{
    public class BillingAccountItemProfile : Profile
    {
        public BillingAccountItemProfile()
        {
            CreateMap<BillingAccountItem, CreditCardDto>()
                .ForMember(b => b.IntegrationCode, c => c.NullSubstitute(""));
        }
    }
}
