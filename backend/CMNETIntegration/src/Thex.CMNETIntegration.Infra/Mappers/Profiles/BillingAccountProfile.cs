﻿using AutoMapper;
using Thex.CMNETIntegration.Dto;
using Thex.CMNETIntegration.Infra.Entities;

namespace Thex.CMNETIntegration.Infra.Mappers.Profiles
{
    public class BillingAccountProfile : Profile
    {
        public BillingAccountProfile()
        {
            CreateMap<Accounting, AccountingDto>();
        }
    }
}
