﻿using AutoMapper;
using Thex.CMNETIntegration.Dto;
using Thex.CMNETIntegration.Infra.Entities;

namespace Thex.CMNETIntegration.Infra.Mappers.Profiles
{
    public class AccountingProfile : Profile
    {
        public AccountingProfile()
        {
            CreateMap<Accounting, AccountingDto>()
                .ForMember(a => a.IntegrationCode, c => c.NullSubstitute(""));
        }
    }
}