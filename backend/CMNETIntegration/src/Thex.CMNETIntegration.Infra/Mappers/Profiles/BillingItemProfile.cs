﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using Thex.CMNETIntegration.Dto;
using Thex.CMNETIntegration.Infra.Entities;

namespace Thex.CMNETIntegration.Infra.Mappers.Profiles
{
    public class BillingItemProfile : Profile
    {
        public BillingItemProfile()
        {
            CreateMap<BillingItem, BillingItemDto>()
                .ForMember(b => b.IntegrationCode, c => c.NullSubstitute(""));
        }
    }
}
