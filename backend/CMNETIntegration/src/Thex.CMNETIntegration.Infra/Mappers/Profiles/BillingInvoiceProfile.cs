﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using Thex.CMNETIntegration.Dto;
using Thex.CMNETIntegration.Infra.Entities;

namespace Thex.CMNETIntegration.Infra.Mappers.Profiles
{
    public class BillingInvoiceProfile : Profile
    {
        public BillingInvoiceProfile()
        {
            CreateMap<BillingInvoice, BillingInvoiceDto>()
                .ForMember(b => b.InvoiceItems, c => c.AllowNull())
                .ForMember(b => b.RatePlanCode, c => c.NullSubstitute(""))
                .ForMember(b => b.CompanyClientExternalCode, c => c.NullSubstitute(""))
                .ForMember(b => b.PersonName, c => c.NullSubstitute(""))
                .ForMember(b => b.GuestName, c => c.NullSubstitute(""))
                .ForMember(b => b.ReservationCode, c => c.NullSubstitute(""))

                .ForPath(dest => dest.BillingInvoiceId, opt => opt.MapFrom(src => src.Id))

                .ForPath(dest => dest.DocumentInformation.DocumentNumber, opt => opt.MapFrom(src => src.DocumentInformation))
                .AfterMap((src, dest) => dest.DocumentInformation.DocumentNumber = dest.DocumentInformation.DocumentNumber ?? "")

                .ForPath(dest => dest.DocumentInformation.DocumentTypeName, opt => opt.MapFrom(src => src.DocumentTypeName))
                .AfterMap((src, dest) => dest.DocumentInformation.DocumentTypeName = dest.DocumentInformation.DocumentTypeName ?? "")

                .ForPath(dest => dest.ExternalInformation.ExternalEmissionDate, opt => opt.MapFrom(src => src.ExternalEmissionDate))

                .ForPath(dest => dest.ExternalInformation.ExternalNumber, opt => opt.MapFrom(src => src.ExternalNumber))
                .AfterMap((src, dest) => dest.ExternalInformation.ExternalNumber = dest.ExternalInformation.ExternalNumber ?? "")

                .ForPath(dest => dest.ExternalInformation.ExternalSeries, opt => opt.MapFrom(src => src.ExternalSeries))
                .AfterMap((src, dest) => dest.ExternalInformation.ExternalSeries = dest.ExternalInformation.ExternalSeries ?? "")

                .ForPath(dest => dest.IssRetido.Aliquota, opt => opt.MapFrom(src => src.Aliquota))
                .ForPath(dest => dest.IssRetido.BaseCalculo, opt => opt.MapFrom(src => src.BaseCalculo))
                .ForPath(dest => dest.IssRetido.IssRetidoAmount, opt => opt.MapFrom(src => src.IssRetidoAmount));

            CreateMap<BillingFiscalInvoice, BillingFiscalInvoiceDto>()
                .ForMember(b => b.InvoiceItems, c => c.AllowNull())
                .ForMember(b => b.RatePlanCode, c => c.NullSubstitute(""))
                .ForMember(b => b.CompanyClientExternalCode, c => c.NullSubstitute(""))
                .ForMember(b => b.PersonName, c => c.NullSubstitute(""))
                .ForMember(b => b.GuestName, c => c.NullSubstitute(""))
                .ForMember(b => b.ReservationCode, c => c.NullSubstitute(""))

                .ForPath(dest => dest.BillingInvoiceId, opt => opt.MapFrom(src => src.Id))

                .ForPath(dest => dest.AddressInformation.AdditionalAddressDetails, opt => opt.MapFrom(src => src.AdditionalAddressDetails))
                .AfterMap((src, dest) => dest.AddressInformation.AdditionalAddressDetails = dest.AddressInformation.AdditionalAddressDetails ?? "")

                .ForPath(dest => dest.AddressInformation.CityCode, opt => opt.MapFrom(src => src.CityCode))
                .AfterMap((src, dest) => dest.AddressInformation.CityCode = dest.AddressInformation.CityCode ?? "")

                .ForPath(dest => dest.AddressInformation.CountryThreeLetterIsoCode, opt => opt.MapFrom(src => src.CountryThreeLetterIsoCode))
                .AfterMap((src, dest) => dest.AddressInformation.CountryThreeLetterIsoCode = dest.AddressInformation.CountryThreeLetterIsoCode ?? "")

                .ForPath(dest => dest.AddressInformation.Neighborhood, opt => opt.MapFrom(src => src.Neighborhood))
                .AfterMap((src, dest) => dest.AddressInformation.Neighborhood = dest.AddressInformation.Neighborhood ?? "")

                .ForPath(dest => dest.AddressInformation.PostalCode, opt => opt.MapFrom(src => src.PostalCode))
                .AfterMap((src, dest) => dest.AddressInformation.PostalCode = dest.AddressInformation.PostalCode ?? "")

                .ForPath(dest => dest.AddressInformation.StateCode, opt => opt.MapFrom(src => src.StateCode))
                .AfterMap((src, dest) => dest.AddressInformation.StateCode = dest.AddressInformation.StateCode ?? "")

                .ForPath(dest => dest.AddressInformation.StreetName, opt => opt.MapFrom(src => src.StreetName))
                .AfterMap((src, dest) => dest.AddressInformation.StreetName = dest.AddressInformation.StreetName ?? "")

                .ForPath(dest => dest.AddressInformation.StreetNumber, opt => opt.MapFrom(src => src.StreetNumber))
                .AfterMap((src, dest) => dest.AddressInformation.StreetNumber = dest.AddressInformation.StreetNumber ?? "")

                .ForPath(dest => dest.AddressInformation.BR_CodigoBCB, opt => opt.MapFrom(src => src.BR_CodigoBCB))                

                .ForPath(dest => dest.DocumentInformation.DocumentNumber, opt => opt.MapFrom(src => src.DocumentInformation))
                .AfterMap((src, dest) => dest.DocumentInformation.DocumentNumber = dest.DocumentInformation.DocumentNumber ?? "")
                
                .ForPath(dest => dest.DocumentInformation.DocumentTypeName, opt => opt.MapFrom(src => src.DocumentTypeName))
                .AfterMap((src, dest) => dest.DocumentInformation.DocumentTypeName = dest.DocumentInformation.DocumentTypeName ?? "")
                
                .ForPath(dest => dest.ExternalInformation.ExternalEmissionDate, opt => opt.MapFrom(src => src.ExternalEmissionDate))

                .ForPath(dest => dest.ExternalInformation.ExternalNumber, opt => opt.MapFrom(src => src.ExternalNumber))
                .AfterMap((src, dest) => dest.ExternalInformation.ExternalNumber = dest.ExternalInformation.ExternalNumber ?? "")

                .ForPath(dest => dest.ExternalInformation.ExternalSeries, opt => opt.MapFrom(src => src.ExternalSeries))
                .AfterMap((src, dest) => dest.ExternalInformation.ExternalSeries = dest.ExternalInformation.ExternalSeries ?? "")                               

                .ForPath(dest => dest.IssRetido.Aliquota, opt => opt.MapFrom(src => src.Aliquota))
                .ForPath(dest => dest.IssRetido.BaseCalculo, opt => opt.MapFrom(src => src.BaseCalculo))
                .ForPath(dest => dest.IssRetido.IssRetidoAmount, opt => opt.MapFrom(src => src.IssRetidoAmount));

            CreateMap<BillingInvoice, BillingInvoiceNFCeDto>()
                .ForPath(dest => dest.BillingInvoiceId, opt => opt.MapFrom(src => src.Id))

                .ForPath(dest => dest.ExternalInformation.ExternalEmissionDate, opt => opt.MapFrom(src => src.ExternalEmissionDate))

                .ForPath(dest => dest.ExternalInformation.ExternalNumber, opt => opt.MapFrom(src => src.ExternalNumber))
                .AfterMap((src, dest) => dest.ExternalInformation.ExternalNumber = dest.ExternalInformation.ExternalNumber ?? "")

                .ForPath(dest => dest.ExternalInformation.ExternalSeries, opt => opt.MapFrom(src => src.ExternalSeries))
                .AfterMap((src, dest) => dest.ExternalInformation.ExternalSeries = dest.ExternalInformation.ExternalSeries ?? "");                

            CreateMap<InvoiceItem, InvoiceItemDto>()
                .ForMember(i => i.IntegrationCode, c => c.NullSubstitute(""));
        }
    }
}
