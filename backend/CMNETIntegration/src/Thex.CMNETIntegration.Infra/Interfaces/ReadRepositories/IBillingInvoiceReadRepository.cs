﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.CMNETIntegration.Infra.Entities;

namespace Thex.CMNETIntegration.Infra.Interfaces.ReadRepositories
{
    public interface IBillingInvoiceReadRepository
    {
        Task<IEnumerable<BillingInvoice>> GetInvoices(DateTime emissionDate);
        Task<IEnumerable<BillingFiscalInvoice>> GetFiscalInvoices(DateTime emissionDate);
        Task<IEnumerable<InvoiceItem>> GetInvoicesItems(DateTime emissionDate, bool isFiscal);
        Task<IEnumerable<BillingInvoice>> GetNFCeInvoices(DateTime emissionDate);
    }
}
