﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.CMNETIntegration.Dto;

namespace Thex.CMNETIntegration.Infra.Interfaces.ReadRepositories
{
    public interface IFinancialReadRepository
    {
        Task<IEnumerable<FinancialDto>> GetFinancialByItemDate(DateTime billingAccountItemDate);
    }
}
