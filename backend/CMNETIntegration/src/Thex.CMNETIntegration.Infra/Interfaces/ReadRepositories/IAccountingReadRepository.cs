﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.CMNETIntegration.Dto;

namespace Thex.CMNETIntegration.Infra.Interfaces.ReadRepositories
{
    public interface IAccountingReadRepository
    {
        Task<IEnumerable<AccountingDto>> GetByItemDate(DateTime billingAccountItemDate);
    }
}
