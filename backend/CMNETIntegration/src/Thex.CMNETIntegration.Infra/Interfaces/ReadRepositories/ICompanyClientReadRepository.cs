﻿using System.Threading.Tasks;
using Thex.CMNETIntegration.Dto;

namespace Thex.CMNETIntegration.Infra.Interfaces.ReadRepositories
{
    public interface ICompanyClientReadRepository
    {
        Task<CompanyClientDto> Get(string document, int documentType);
    }
}
