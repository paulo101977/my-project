﻿using System.Threading.Tasks;
using Thex.CMNETIntegration.Dto;

namespace Thex.CMNETIntegration.Infra.Interfaces.ReadRepositories
{
    public interface IPropertyCurrencyExchangeReadRepository
    {
        Task<PropertyCurrencyExchangeDto> GetExchangeByCurrenyCode(string currencyCode);
    }
}
