﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.CMNETIntegration.Dto;

namespace Thex.CMNETIntegration.Infra.Interfaces.ReadRepositories
{
    public interface IBillingAccountReadRepository
    {
        Task<IEnumerable<BillingAccountItemForEventDto>> GetBillingAccountItemsAsync(string billingAccountId, bool associatedAccounts, DateTime? itemStartDate, DateTime? itemEndDate);
        Task<BillingAccountForEventDto> GetBillingAccountAsync(string billingAccountId);
        Task<BillingAccountByRoomDto> GetBillingAccountIdByRoomNumber(string roomNumber);
    }
}
