﻿using System.Threading.Tasks;
using Thex.CMNETIntegration.Dto;

namespace Thex.CMNETIntegration.Infra.Interfaces.ReadRepositories
{
    public interface IHealthReadRepository
    {
        Task<BillingItemDto> Check();
    }
}
