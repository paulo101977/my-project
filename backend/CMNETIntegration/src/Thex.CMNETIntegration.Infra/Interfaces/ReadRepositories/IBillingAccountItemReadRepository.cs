﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.CMNETIntegration.Dto;
using Thex.CMNETIntegration.Infra.Entities;

namespace Thex.CMNETIntegration.Infra.Interfaces.ReadRepositories
{
    public interface IBillingAccountItemReadRepository
    {
        Task<IEnumerable<BillingAccountItem>> GetCreditCard(DateTime itemDate);
        Task<IEnumerable<PrepaymentDto>> GetPrepayments(DateTime itemDate);
    }
}