﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.CMNETIntegration.Dto;

namespace Thex.CMNETIntegration.Infra.Interfaces.ReadRepositories
{
    public interface IBillingItemReadRepository
    {
        Task<BillingItemDto> GetBillingItemByIntegrationCode(string integrationCode);
        Task<IEnumerable<BillingItemDto>> GetAll();
    }
}
