﻿using System.Threading.Tasks;
using Thex.CMNETIntegration.Dto;

namespace Thex.CMNETIntegration.Infra.Interfaces.ReadRepositories
{
    public interface IPropertyReadRepository
    {
        Task<PropertyDto> GetProperty(string documentInfo);
        Task<PropertyDto> GetProperty(int propertyId);
    }
}
