﻿using System;
using System.Threading.Tasks;
using Thex.CMNETIntegration.Dto;

namespace Thex.CMNETIntegration.Infra.Interfaces
{
    public interface IBillingAccountItemRepository
    {
        void ConfirmReceipt(ConfirmationDto model);
    }
}
