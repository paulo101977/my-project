﻿using System;
using System.Threading.Tasks;
using Thex.CMNETIntegration.Infra.Entities;
using Tnf.Dto;

namespace Thex.CMNETIntegration.Infra.Interfaces
{
    public interface IBaseRepository
    {
        Task<IDto> InsertAsync(BaseEntity baseEntity);
        Task<IDto> UpdateAsync(Guid id, BaseEntity baseEntity);
        Task RemoveAsync(Guid id);
        Task ToggleAsync(Guid id);
    }
}
