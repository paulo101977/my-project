﻿using System;
using System.Collections.Generic;
using System.Text;
using Thex.CMNETIntegration.Dto;

namespace Thex.CMNETIntegration.Infra.Interfaces
{
    public interface IBillingInvoiceRepository
    {
        void ConfirmReceipt(ConfirmationDto model);
    }
}
