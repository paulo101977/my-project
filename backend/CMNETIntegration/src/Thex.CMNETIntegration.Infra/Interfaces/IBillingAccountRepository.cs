﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Thex.CMNETIntegration.Infra.Entities;

namespace Thex.CMNETIntegration.Infra.Interfaces
{
    public interface IBillingAccountRepository
    {
        Task<BillingAccount> InsertAndSaveChangesAsync(BillingAccount account);
        
    }
}
