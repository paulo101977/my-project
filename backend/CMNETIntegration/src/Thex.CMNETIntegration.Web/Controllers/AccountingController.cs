﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.CMNETIntegration.Application.Interfaces;
using Thex.CMNETIntegration.Dto;
using Thex.Kernel;
using Tnf.AspNetCore.Mvc.Response;

namespace Thex.CMNETIntegration.Web.Controllers
{
    [Authorize]
    [Route(RouteConsts.AccountingRouteName)]   
    public class AccountingController : TnfController
    {
        private readonly IAccountingAppService _AccountingAppService;

        public AccountingController(IAccountingAppService AccountingAppService)
        {
            _AccountingAppService = AccountingAppService;
        }

        [HttpGet("{date}")]
        [ProducesResponseType(typeof(IEnumerable<AccountingDto>), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> Get(DateTime date)
        {
            var response = await _AccountingAppService.Get(date);

            return CreateResponseOnGet(response);
        }
    }
}
