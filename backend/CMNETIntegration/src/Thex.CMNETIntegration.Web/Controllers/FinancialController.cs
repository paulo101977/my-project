﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Thex.CMNETIntegration.Application.Interfaces;
using Thex.CMNETIntegration.Dto;
using Thex.Kernel;
using Tnf.AspNetCore.Mvc.Response;

namespace Thex.CMNETIntegration.Web.Controllers
{
    [Authorize]
    [Route(RouteConsts.FinancialRouteName)]
    public class FinancialController : TnfController
    {
        private readonly IFinancialAppService _FinancialAppService;

        public FinancialController(IFinancialAppService FinancialAppService)
        {
            _FinancialAppService = FinancialAppService;
        }

        [HttpGet("{date}")]
        [ProducesResponseType(typeof(IEnumerable<FinancialDto>), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> Get(DateTime date)
        {
            var response = await _FinancialAppService.Get(date);

            return CreateResponseOnGet(response);
        }
    }
}