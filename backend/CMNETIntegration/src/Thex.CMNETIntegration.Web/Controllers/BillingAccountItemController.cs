﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Thex.CMNETIntegration.Application.Helpers;
using Thex.CMNETIntegration.Application.Interfaces;
using Thex.CMNETIntegration.Dto;
using Thex.CMNETIntegration.Infra.Entities;
using Thex.Kernel;
using Tnf.AspNetCore.Mvc.Response;

namespace Thex.CMNETIntegration.Web.Controllers
{
    [Authorize]
    [Route(RouteConsts.BillingAccountItemRouteName)]
    public class BillingAccountItemController : TnfController
    {
        private readonly IBillingAccountItemAppService _billingAccountItemAppService;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public BillingAccountItemController(
            IBillingAccountItemAppService billingAccountItemAppService,
            IApplicationUser applicationUser,
            IHttpContextAccessor httpContextAccessor)
        {
            _billingAccountItemAppService = billingAccountItemAppService;
            _httpContextAccessor = httpContextAccessor;
        }

        [HttpPost]
        [ProducesResponseType(typeof(BillingAccountItemDebitDto), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> Post([FromBody] EventBillingAccountItemDto model)
        {
            TokenHelper.TokenFromRequest = _httpContextAccessor.HttpContext.Request.Headers.FirstOrDefault(x => x.Key.Equals("Authorization")).Value.ToString();

            var response = await _billingAccountItemAppService.CreateDebit(model);

            return CreateResponseOnPost(response, EntityNames.BillingAccountForEvent);
        }

        [HttpGet("{date}/CreditCard")]
        [ProducesResponseType(typeof(IEnumerable<CreditCardDto>), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> GetCreditCard(DateTime date)
        {
            var response = await _billingAccountItemAppService.GetCreditCardPayment(date);

            return CreateResponseOnGet(response);
        }                

        [HttpPost("CreditCard/Confirmation")]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult PostConfirmCreditCard([FromBody]ConfirmationDto dto)
        {
            _billingAccountItemAppService.ConfirmReceipt(dto);

            return CreateResponseOnPost(null);
        }

        [HttpGet("{date}/Prepayment")]
        [ProducesResponseType(typeof(IEnumerable<PrepaymentDto>), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> GetAdvancePayments(DateTime date)
        {
            var response = await _billingAccountItemAppService.GetPrepayments(date);

            return CreateResponseOnGet(response);
        }

        [HttpPost("Prepayment/Confirmation")]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult PostConfirmPrepayment([FromBody]ConfirmationDto dto)
        {
            _billingAccountItemAppService.ConfirmReceipt(dto);

            return CreateResponseOnPost(null);
        }
    }
}