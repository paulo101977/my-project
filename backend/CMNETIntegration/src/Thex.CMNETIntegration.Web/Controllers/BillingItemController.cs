﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Thex.CMNETIntegration.Application.Interfaces;
using Thex.CMNETIntegration.Dto;
using Thex.Kernel;
using Tnf.AspNetCore.Mvc.Response;

namespace Thex.CMNETIntegration.Web.Controllers
{
    [Authorize]
    [Route(RouteConsts.BillingItemRouteName)]
    public class BillingItemController : TnfController
    {
        private readonly IBillingItemAppService _billingItemAppService;

        public BillingItemController(
            IHttpContextAccessor httpContextAccessor, 
            IBillingItemAppService billingItemAppService)
        {
            _billingItemAppService = billingItemAppService;
        }

        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<BillingItemDto>), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Get()
        {
            var response = await _billingItemAppService.GetAll();

            return CreateResponseOnGet(response);
        }
    }
}