﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Threading.Tasks;
using Thex.CMNETIntegration.Application.Helpers;
using Thex.CMNETIntegration.Application.Interfaces;
using Thex.CMNETIntegration.Dto;
using Thex.CMNETIntegration.Infra.Entities;
using Thex.Kernel;
using Tnf.AspNetCore.Mvc.Response;

namespace Thex.CMNETIntegration.Web.Controllers
{
    [Authorize]
    [Route(RouteConsts.BillingAccountRouteName)]
    public class BillingAccountController : TnfController
    {
        private readonly IBillingAccountAppService _billingAccountAppService;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public BillingAccountController(
            IBillingAccountAppService billingAccountAppService, 
            IHttpContextAccessor httpContextAccessor)
        {
            _billingAccountAppService = billingAccountAppService;
            _httpContextAccessor = httpContextAccessor;
        }

        [HttpPost]
        [ProducesResponseType(typeof(BillingAccountForEventDto), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> Post([FromBody] EventBillingAccountDto dto)
        {
            TokenHelper.TokenFromRequest = _httpContextAccessor.HttpContext.Request.Headers.FirstOrDefault(x => x.Key.Equals("Authorization")).Value.ToString();

            var response = await _billingAccountAppService.CreateSparseAccount(dto);

            return CreateResponseOnPost(response, EntityNames.BillingAccountForEvent);
        }

        [HttpGet]
        [ProducesResponseType(typeof(BillingAccountForEventDto), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> Get(string billingAccountId, bool associatedAccounts, DateTime? itemStartDate, DateTime? itemEndDate)
        {
            var response = await _billingAccountAppService.GetBillingAccountItems(billingAccountId, associatedAccounts, itemStartDate, itemEndDate);

            return CreateResponseOnPost(response, EntityNames.BillingAccountForEvent);
        }
    }
}