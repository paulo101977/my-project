﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics.CodeAnalysis;
using System.Threading.Tasks;
using Thex.CMNETIntegration.Application.Interfaces;
using Thex.CMNETIntegration.Infra.Entities;
using Tnf.AspNetCore.Mvc.Response;


namespace Thex.CMNETIntegration.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [AllowAnonymous]
    [ExcludeFromCodeCoverage]
    public class HealthController : TnfController
    {
        private IHealthAppService _healthAppService;

        public HealthController(IHealthAppService healthAppService)
        {
            _healthAppService = healthAppService;
        }

        /// <summary>
        /// Verifica se a API está em funcionamento e conectada no banco de dados
        /// </summary>
        /// <returns></returns>
        [HttpGet("check")]
        [ProducesResponseType(typeof(bool), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> GetAll()
        {
            var response = await _healthAppService.Check();

            return CreateResponseOnGetAll(response != null ? true : false, EntityNames.Health);
        }
    }
}