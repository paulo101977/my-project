﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Thex.CMNETIntegration.Application.Helpers;
using Thex.CMNETIntegration.Application.Interfaces;
using Thex.CMNETIntegration.Dto;
using Thex.CMNETIntegration.Infra.Entities;
using Thex.Kernel;
using Tnf.AspNetCore.Mvc.Response;

namespace Thex.CMNETIntegration.Web.Controllers
{
    [Authorize]
    [Route(RouteConsts.InvoiceRouteName)]
    public class BillingInvoiceController : TnfController
    {
        private readonly IBillingInvoiceAppService _invoiceAppService;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IApplicationUser _applicationUser;

        public BillingInvoiceController(IBillingInvoiceAppService invoiceAppService, 
            IHttpContextAccessor httpContextAccessor, IApplicationUser applicationUser)
        {
            _invoiceAppService = invoiceAppService;
            _httpContextAccessor = httpContextAccessor;
            _applicationUser = applicationUser;
        }

        [HttpGet("{date}")]
        [ProducesResponseType(typeof(IEnumerable<BillingInvoiceDto>), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Get(DateTime date)
        {
            var response = await _invoiceAppService.GetInvoices(date);

            return CreateResponseOnGet(response);
        }

        [HttpGet("{date}/Fiscal")]
        [ProducesResponseType(typeof(IEnumerable<BillingFiscalInvoiceDto>), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> GetFiscal(DateTime date)
        {
            var response = await _invoiceAppService.GetFiscalInvoices(date);

            return CreateResponseOnGet(response);
        }

        [HttpGet("{date}/NFCe")]
        [ProducesResponseType(typeof(IEnumerable<BillingInvoiceNFCeDto>), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> GetNFCe(DateTime date)
        {
            var response = await _invoiceAppService.GetNFCeInvoices(date);

            return CreateResponseOnGet(response);
        }

        [HttpPost("Confirmation")]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult PostConfirmInvoice([FromBody]ConfirmationDto dto)
        {
            _invoiceAppService.ConfirmInvoiceReceipt(dto);

            return CreateResponseOnPost(null);
        }
    }
}