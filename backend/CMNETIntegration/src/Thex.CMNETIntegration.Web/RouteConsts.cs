﻿namespace Thex.CMNETIntegration.Web
{
    public class RouteConsts
    {
        public const string TransportationTypeRouteName = "api/transportationType";
        public const string AccountingRouteName = "api/Accounting";
        public const string BillingRouteName = "api/Billing";
        public const string BillingAccountRouteName = "api/BillingAccount";
        public const string BillingAccountItemRouteName = "api/BillingAccountItem";
        public const string BillingItemRouteName = "api/BillingItem";
        public const string FinancialRouteName = "api/Financial";
        public const string InvoiceRouteName = "api/Invoice";
    }
}
