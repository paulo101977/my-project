﻿using System;
using System.Threading.Tasks;
using Thex.Common;
using Thex.Common.Enumerations;
using Thex.CMNETIntegration.Infra.Entities;
using Thex.CMNETIntegration.Infra.Interfaces;
using Tnf.Application.Services;
using Tnf.Dto;
using Tnf.Notifications;
using Thex.CMNETIntegration.Infra.Interfaces.ReadRepositories;
using Thex.CMNETIntegration.Dto.Enumerations;

namespace Thex.CMNETIntegration.Application.Services
{
    public abstract class ApplicationServiceBase : ApplicationService
    {
        private IPropertyReadRepository _propertyRepo;

        protected ApplicationServiceBase(INotificationHandler notification,         
                                         IPropertyReadRepository propertyReadRepository) : base(notification)
        {
            _propertyRepo = propertyReadRepository;
        }

        protected async Task ValidateProperty(int propertyId)
        {
            var property = await _propertyRepo.GetProperty(propertyId);

            if (property == null)
            {
                Notification.Raise(Notification
                   .DefaultBuilder
                   .WithMessage(AppConsts.LocalizationSourceName, CMNETIntegrationEnum.NotFoundErrors.PropertyNotFound)
                   .WithNotFoundStatus()
                   .Build());
            }
        }

        protected void ValidateGuid(string id, int guidType)
        {
            Guid gTest = new Guid();

            if (!Guid.TryParse(id, out gTest))
            {
                switch (guidType)
                {
                    case (int)CMNETIntegrationEnum.GuidTypeValidate.BillingAccountIdValidate:
                        {
                            Notification.Raise(Notification
                               .DefaultBuilder
                               .WithMessage(AppConsts.LocalizationSourceName, CMNETIntegrationEnum.Error.BillingAccountIdInvalid)
                               .WithBadRequestStatus()
                               .Build());
                            break;
                        }

                    case (int)CMNETIntegrationEnum.GuidTypeValidate.BillingAccountItemIdValidate:
                        {
                            Notification.Raise(Notification
                               .DefaultBuilder
                               .WithMessage(AppConsts.LocalizationSourceName, CMNETIntegrationEnum.Error.BillingAccountItemIdInvalid)
                               .WithBadRequestStatus()
                               .Build());
                            break;                            
                        }
                }
            }
        }

        protected virtual void NotifyNotFound(CMNETIntegrationEnum.NotFoundErrors enumType)
        {
            Notification.Raise(Notification
                .DefaultBuilder
                .WithMessage(AppConsts.LocalizationSourceName, enumType)
                .WithNotFoundStatus()
                .Build());
        }

        protected virtual void NotifyBadRequest(CMNETIntegrationEnum.Error enumType)
        {
            Notification.Raise(Notification
                .DefaultBuilder
                .WithMessage(AppConsts.LocalizationSourceName, enumType)
                .WithBadRequestStatus()
                .Build());
        }

        protected virtual void NotifyNoTenant()
        {
            Notification.Raise(Notification
                .DefaultBuilder
                .WithMessage(AppConsts.LocalizationSourceName, CMNETIntegrationEnum.GuidTypeValidate.TenantIdValidate)
                .WithBadRequestStatus()
                .Build());
        }
    }
}
