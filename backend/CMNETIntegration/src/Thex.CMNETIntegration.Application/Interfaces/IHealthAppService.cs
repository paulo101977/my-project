﻿using System.Threading.Tasks;
using Thex.CMNETIntegration.Dto;

namespace Thex.CMNETIntegration.Application.Interfaces
{
    public interface IHealthAppService
    {
        Task<BillingItemDto> Check();
    }
}
