﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Thex.CMNETIntegration.Dto;

namespace Thex.CMNETIntegration.Application.Interfaces
{
    public interface IFinancialAppService
    {
        Task<IEnumerable<FinancialDto>> Get(DateTime itemDate);
    }
}
