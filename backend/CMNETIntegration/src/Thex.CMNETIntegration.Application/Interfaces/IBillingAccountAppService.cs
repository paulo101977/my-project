﻿using System;
using System.Threading.Tasks;
using Thex.CMNETIntegration.Dto;
using Tnf.Application.Services;

namespace Thex.CMNETIntegration.Application.Interfaces
{
    public interface IBillingAccountAppService : IApplicationService
    {
        Task<BillingAccountForEventDto> CreateSparseAccount(EventBillingAccountDto dto);
        Task<BillingAccountForEventDto> GetBillingAccountItems(string billingAccountId, bool associatedAccounts, DateTime? itemStartDate, DateTime? itemEndDate);
    }
}
