﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Thex.CMNETIntegration.Dto;
using Tnf.Application.Services;

namespace Thex.CMNETIntegration.Application.Interfaces
{
    public interface IBillingInvoiceAppService : IApplicationService
    {        
        Task<IEnumerable<BillingInvoiceDto>> GetInvoices(DateTime date);
        Task<IEnumerable<BillingFiscalInvoiceDto>> GetFiscalInvoices(DateTime date);
        Task<IEnumerable<BillingInvoiceNFCeDto>> GetNFCeInvoices(DateTime date);
        void ConfirmInvoiceReceipt(ConfirmationDto dto);
    }
}
