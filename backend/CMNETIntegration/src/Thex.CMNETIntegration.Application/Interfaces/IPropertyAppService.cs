﻿using System.Threading.Tasks;
using Thex.CMNETIntegration.Dto;
using Tnf.Application.Services;

namespace Thex.CMNETIntegration.Application.Interfaces
{
    public interface IPropertyAppService : IApplicationService
    {
        Task<PropertyDto> Get(DefaultStringRequestDto document);
    }
}
