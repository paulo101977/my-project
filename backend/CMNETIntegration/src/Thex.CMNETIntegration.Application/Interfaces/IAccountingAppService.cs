﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.CMNETIntegration.Dto;
using Tnf.Application.Services;
using System;

namespace Thex.CMNETIntegration.Application.Interfaces
{
    public interface IAccountingAppService : IApplicationService
    {
        Task<IEnumerable<AccountingDto>> Get(DateTime itemDate);   
    }
}
