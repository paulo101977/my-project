﻿using System.Threading.Tasks;
using Thex.CMNETIntegration.Dto;
using Tnf.Application.Services;

namespace Thex.CMNETIntegration.Application.Interfaces
{
    interface ICompanyClientAppService : IApplicationService
    {
        Task<CompanyClientDto> GetAsync(string document, int documentType);
    }
}
