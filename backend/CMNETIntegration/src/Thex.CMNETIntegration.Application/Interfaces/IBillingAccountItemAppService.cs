﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.CMNETIntegration.Dto;
using Tnf.Application.Services;

namespace Thex.CMNETIntegration.Application.Interfaces
{
    public interface IBillingAccountItemAppService : IApplicationService
    {
        Task<BillingAccountItemDebitDto> CreateDebit(EventBillingAccountItemDto dto);
        Task<IEnumerable<CreditCardDto>> GetCreditCardPayment(DateTime date);
        Task<IEnumerable<PrepaymentDto>> GetPrepayments(DateTime date);
        void ConfirmReceipt(ConfirmationDto dto);
    }
}
