﻿using System.Threading.Tasks;
using Thex.CMNETIntegration.Application.Interfaces;
using Thex.CMNETIntegration.Dto;
using Thex.CMNETIntegration.Infra.Interfaces.ReadRepositories;
using Tnf.Application.Services;
using Tnf.Notifications;

namespace Thex.CMNETIntegration.Application.Services
{
    public class CompanyClientAppService : ApplicationService, ICompanyClientAppService
    {
        private readonly ICompanyClientReadRepository _companyClientReadRepo;

        public CompanyClientAppService(INotificationHandler notification, ICompanyClientReadRepository companyClientReadRepo) : base(notification)
        {
            _companyClientReadRepo = companyClientReadRepo;
        }

        public virtual async Task<CompanyClientDto> GetAsync(string document, int documentType)
        {
            return await _companyClientReadRepo.Get(document, documentType);
        }
    }
}
