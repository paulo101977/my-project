﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Thex.CMNETIntegration.Application.Helpers;
using Thex.CMNETIntegration.Application.Interfaces;
using Thex.CMNETIntegration.Dto;
using Thex.CMNETIntegration.Dto.Enumerations;
using Thex.CMNETIntegration.Infra.Entities;
using Thex.CMNETIntegration.Infra.Interfaces;
using Thex.CMNETIntegration.Infra.Interfaces.ReadRepositories;
using Thex.Common;
using Thex.Kernel;
using Tnf.Application.Services;
using Tnf.Notifications;
using Tnf.Runtime.Security;

namespace Thex.CMNETIntegration.Application.Services
{
    public class BillingAccountItemAppService : ApplicationServiceBase, IBillingAccountItemAppService
    {
        private readonly IConfiguration _configuration;
        private readonly IPropertyCurrencyExchangeReadRepository _exchangeReadRepository;
        private readonly IBillingItemReadRepository _billingItemReadRepository;
        private readonly IBillingAccountItemReadRepository _billingAccountItemReadRepository;
        private readonly IBillingAccountReadRepository _billingAccountReadRepository;
        private readonly IBillingAccountItemRepository _billingAccountItemRepository;
        private readonly IApplicationUser _applicationUser;

        public BillingAccountItemAppService(
            IApplicationUser applicationUser,
            INotificationHandler notification,
            IConfiguration configuration,
            IPropertyCurrencyExchangeReadRepository exchangeReadRepository,
            IBillingItemReadRepository billingItemReadRepository,
            IPropertyReadRepository propertyReadRepository,
            IBillingAccountItemReadRepository billingAccountItemReadRepository,
            IBillingAccountReadRepository billingAccountReadRepository,
            IBillingAccountItemRepository billingAccountItemRepository) : base(notification, propertyReadRepository)
        {
            _applicationUser = applicationUser;
            _configuration = configuration;
            _exchangeReadRepository = exchangeReadRepository;
            _billingItemReadRepository = billingItemReadRepository;
            _billingAccountItemReadRepository = billingAccountItemReadRepository;
            _billingAccountReadRepository = billingAccountReadRepository;
            _billingAccountItemRepository = billingAccountItemRepository;
        }

        private async Task<Guid?> GetBillingAccountId(string roomNumber)
        {
            var account = await _billingAccountReadRepository.GetBillingAccountIdByRoomNumber(roomNumber);

            if (account == null)
                NotifyNotFound(CMNETIntegrationEnum.NotFoundErrors.BillingAccountNotFound);

            return account?.BillingAccountId;            
        }

        private async Task<PropertyCurrencyExchangeDto> GetExchangeByCurrenyCode(string currencyCode)
        {
            var exchange = await _exchangeReadRepository.GetExchangeByCurrenyCode(currencyCode);

            if (exchange == null)
            {
                NotifyNotFound(CMNETIntegrationEnum.NotFoundErrors.PropertyCurrencyExchangeNotFound);
                return PropertyCurrencyExchangeDto.NullInstance;
            }

            return exchange;
        }

        private async Task<BillingItemDto> GetBillingItemByIntegrationCode(string integrationCode)
        {
            var item = await _billingItemReadRepository.GetBillingItemByIntegrationCode(integrationCode);

            if (item == null)
            {
                NotifyNotFound(CMNETIntegrationEnum.NotFoundErrors.BillingItemNotFound);               
                return BillingItemDto.NullInstance;
            }

            return item;
        }

        public async Task<BillingAccountItemDebitDto> CreateDebit(EventBillingAccountItemDto dto)
        {
            if (TokenHelper.TokenFromRequest.IsNullOrEmpty())
                NotifyBadRequest(CMNETIntegrationEnum.Error.EmptyToken);

            if (Notification.HasNotification())
                return BillingAccountItemDebitDto.NullInstance;

            if (!ValidateDto<EventBillingAccountItemDto>(dto))
                return BillingAccountItemDebitDto.NullInstance;

            await ValidateProperty(_applicationUser.PropertyId.TryParseToInt32());
            if (Notification.HasNotification())
                return BillingAccountItemDebitDto.NullInstance;

            Guid? AccountIdGuid = Guid.Empty;

            if (dto.BillingAccountId.HasValue)
            {
                AccountIdGuid = dto.BillingAccountId;
            }
            else
            {
                AccountIdGuid = await GetBillingAccountId(dto.RoomNumber);
            }

            if (Notification.HasNotification())
                return BillingAccountItemDebitDto.NullInstance;

            if (Notification.HasNotification())
                return BillingAccountItemDebitDto.NullInstance;

            var item = await GetBillingItemByIntegrationCode(dto.BillingItemIntegrationCode);

            if (Notification.HasNotification())
                return BillingAccountItemDebitDto.NullInstance;

            var itemDebit = new BillingAccountItemDebitDto
            {
                Amount = dto.BillingItemAmount,
                BillingAccountId = (Guid)AccountIdGuid,                
                PropertyId = _applicationUser.PropertyId.TryParseToInt32(),
                BillingItemId = item.BillingItemId
            };

            var uri = HttpClientHelper.GetThexUrl(_configuration, "/api/BillingAccountItem/debit");

            var result = await HttpClientHelper.SendRequest<BillingAccountItemDebitDto>(
                Notification, 
                CMNETIntegrationEnum.NotFoundErrors.BillingAccountNotFound, 
                uri.AbsoluteUri,
                TokenHelper.TokenFromRequest,
                HttpVerbs.Post, 
                itemDebit);
            
            return result;
        }

        public async Task<IEnumerable<CreditCardDto>> GetCreditCardPayment(DateTime itemDate)
        {
            await ValidateProperty(_applicationUser.PropertyId.TryParseToInt32());
            if (Notification.HasNotification())
                return null;

            var list = await _billingAccountItemReadRepository.GetCreditCard(itemDate);

            if (list.Count() == 0)
            {
                NotifyNotFound(CMNETIntegrationEnum.NotFoundErrors.BillingItemsForDateNotFound);
                return null;
            }

            return MapToDto(list);
        }

        public async Task<IEnumerable<PrepaymentDto>> GetPrepayments(DateTime itemDate)
        {
            await ValidateProperty(_applicationUser.PropertyId.TryParseToInt32());
            if (Notification.HasNotification())
                return null;

            var list = await _billingAccountItemReadRepository.GetPrepayments(itemDate);

            if (!list.Any())
            {
                NotifyNotFound(CMNETIntegrationEnum.NotFoundErrors.PrepaymentNotFound);
                return null;
            }

            return list;
        }

        private IEnumerable<CreditCardDto> MapToDto(IEnumerable<BillingAccountItem> listItems)
        {
            var result = new List<CreditCardDto>();

            foreach (var item in listItems)
                result.Add(item.MapTo<CreditCardDto>());

            return result;
        }

        public void ConfirmReceipt(ConfirmationDto dto)
        {
            if (_applicationUser.TenantId == Guid.Empty)
                NotifyNoTenant();

            if (Notification.HasNotification())
                return;

            ValidateDto<ConfirmationDto>(dto, nameof(dto));

            if (Notification.HasNotification())
                return;

            ValidateGuid(dto.Id.ToString(), (int)CMNETIntegrationEnum.GuidTypeValidate.BillingAccountItemIdValidate);

            if (Notification.HasNotification())
                return;

            _billingAccountItemRepository.ConfirmReceipt(dto);
        }
    }
}
