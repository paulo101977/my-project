﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Thex.CMNETIntegration.Application.Interfaces;
using Thex.CMNETIntegration.Dto;
using Thex.CMNETIntegration.Infra.Interfaces.ReadRepositories;
using Tnf.Notifications;

namespace Thex.CMNETIntegration.Application.Services
{
    public class HealthAppService : ApplicationServiceBase, IHealthAppService
    {
        private readonly IHealthReadRepository _healthReadRepository;

        public HealthAppService(INotificationHandler notification, 
            IPropertyReadRepository propertyReadRepository,
            IHealthReadRepository healthReadRepository) : base(notification, propertyReadRepository)
        {
            _healthReadRepository = healthReadRepository;
        }

        public async Task<BillingItemDto> Check()
            => await _healthReadRepository.Check();
    }
}
