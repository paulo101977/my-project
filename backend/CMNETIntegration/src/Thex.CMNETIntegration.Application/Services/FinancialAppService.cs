﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Thex.CMNETIntegration.Application.Interfaces;
using Thex.CMNETIntegration.Dto;
using Thex.CMNETIntegration.Dto.Enumerations;
using Thex.CMNETIntegration.Infra.Interfaces.ReadRepositories;
using Thex.Kernel;
using Tnf.Notifications;

namespace Thex.CMNETIntegration.Application.Services
{
    public class FinancialAppService : ApplicationServiceBase, IFinancialAppService
    {
        private readonly IFinancialReadRepository _FinancialReadRepository;
        private readonly IApplicationUser _applicationUser;

        public FinancialAppService(
            IApplicationUser applicationUser,
            IFinancialReadRepository FinancialReadRepository,
            IPropertyReadRepository propertyReadRepository,
            INotificationHandler notificationHandler) : base(notificationHandler, propertyReadRepository)
        {
            _FinancialReadRepository = FinancialReadRepository;
            _applicationUser = applicationUser;
        }

        public virtual async Task<IEnumerable<FinancialDto>> Get(DateTime itemDate)
        {
            await ValidateProperty(_applicationUser.PropertyId.TryParseToInt32());
            if (Notification.HasNotification())
                return null;

            IEnumerable<FinancialDto> dto = await _FinancialReadRepository.GetFinancialByItemDate(itemDate);

            if (dto == null || !dto.Any())
            {
                NotifyNotFound(CMNETIntegrationEnum.NotFoundErrors.FinancialNotFound);
                return null;
            }

            return dto;
        }
    }
}
