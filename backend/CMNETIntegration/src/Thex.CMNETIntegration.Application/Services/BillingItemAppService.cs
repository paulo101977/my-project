﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Thex.CMNETIntegration.Application.Interfaces;
using Thex.CMNETIntegration.Dto;
using Thex.CMNETIntegration.Infra.Interfaces.ReadRepositories;
using Thex.Kernel;
using Tnf.Notifications;

namespace Thex.CMNETIntegration.Application.Services
{
    public class BillingItemAppService : ApplicationServiceBase, IBillingItemAppService
    {
        private readonly IBillingItemReadRepository _billingItemReadRepository;
        private readonly IApplicationUser _applicationUser;

        public BillingItemAppService(
            IApplicationUser applicationUser,
            INotificationHandler notification, 
            IPropertyReadRepository propertyReadRepository,
            IBillingItemReadRepository billingItemReadRepository) : base(notification, propertyReadRepository)
        {
            _billingItemReadRepository = billingItemReadRepository;
            _applicationUser = applicationUser;
        }

        public async Task<IEnumerable<BillingItemDto>> GetAll()
        {
            await ValidateProperty(_applicationUser.PropertyId.TryParseToInt32());

            if (Notification.HasNotification())
                return null;

            return await _billingItemReadRepository.GetAll();
        }
    }
}
