﻿using Polly;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Thex.CMNETIntegration.Application.Interfaces;
using Thex.CMNETIntegration.Dto;
using Thex.CMNETIntegration.Dto.Enumerations;
using Thex.CMNETIntegration.Infra.Entities;
using Thex.CMNETIntegration.Infra.Interfaces;
using Thex.CMNETIntegration.Infra.Interfaces.ReadRepositories;
using Thex.Common.Enumerations;
using Thex.Kernel;
using Tnf.Notifications;

namespace Thex.CMNETIntegration.Application.Services
{
    public class BillingInvoiceAppService : ApplicationServiceBase, IBillingInvoiceAppService
    {
        private readonly IBillingInvoiceReadRepository _billingInvoiceReadRepository;
        private readonly IBillingInvoiceRepository _billingInvoiceRepository;
        private readonly IApplicationUser _applicationUser;

        public BillingInvoiceAppService(
            IApplicationUser applicationUser,
            INotificationHandler notification,
            IPropertyReadRepository propertyReadRepository,
            IBillingInvoiceReadRepository billingInvoiceReadRepository,
            IBillingInvoiceRepository billingInvoiceRepository) : base(notification, propertyReadRepository)
        {
            _applicationUser = applicationUser;
            _billingInvoiceReadRepository = billingInvoiceReadRepository;
            _billingInvoiceRepository = billingInvoiceRepository;
        }

        public async Task<IEnumerable<BillingInvoiceDto>> GetInvoices(DateTime date)
        {
            await ValidateProperty(_applicationUser.PropertyId.TryParseToInt32());

            if (Notification.HasNotification())
                return null;

            var invoices = await _billingInvoiceReadRepository.GetInvoices(date);

            if (invoices.Count() == 0)
            {
                NotifyNotFound(CMNETIntegrationEnum.NotFoundErrors.BillingInvoicesForDateNotFound);
                return null;
            }

            var invoicesItems = await _billingInvoiceReadRepository.GetInvoicesItems(date, false);

            decimal totalInvoice;

            if (invoicesItems.Count() > 0)
            {
                foreach (var invoice in invoices)
                {
                    totalInvoice = 0;
                    invoice.PaymentDate = invoice.EmissionDate;
                    foreach (var invItem in invoicesItems.Where(i => i.BillingInvoiceId == invoice.Id))
                    {
                        invoice.InvoiceItems.Add(invItem);
                        if (invItem.PaymentTypeId != (int)PaymentTypeEnum.TobeBilled)
                            totalInvoice += invItem.Amount;
                    }
                    invoice.InvoiceAmount = Math.Abs(totalInvoice);
                }
            }

            return MapToBillingInvoiceDto<BillingInvoiceDto>(invoices);
        }

        public async Task<IEnumerable<BillingFiscalInvoiceDto>> GetFiscalInvoices(DateTime date)
        {
            await ValidateProperty(_applicationUser.PropertyId.TryParseToInt32());

            if (Notification.HasNotification())
                return null;

            var invoices = await _billingInvoiceReadRepository.GetFiscalInvoices(date);

            if (invoices.Count() == 0)
            {
                NotifyNotFound(CMNETIntegrationEnum.NotFoundErrors.BillingInvoicesForDateNotFound);
                return null;
            }

            var invoicesItems = await _billingInvoiceReadRepository.GetInvoicesItems(date, true);

            decimal totalInvoice;

            if (invoicesItems.Count() > 0)
            {
                foreach (var invoice in invoices)
                {
                    totalInvoice = 0;
                    invoice.PaymentDate = invoice.EmissionDate;
                    foreach (var invItem in invoicesItems.Where(i => i.BillingInvoiceId == invoice.Id))
                    {
                        if (invItem.PaymentTypeId <= 0)
                            invoice.InvoiceItems.Add(invItem);

                        totalInvoice += invItem.Amount;
                    }

                    invoice.InvoiceAmount = Math.Abs(totalInvoice);
                }
            }

            return MapToBillingInvoiceDto<BillingFiscalInvoiceDto>(invoices);
        }

        private List<T> MapToBillingInvoiceDto<T>(IEnumerable<BillingInvoice> listOfEntities) where T : BillingInvoiceDto, new()
        {
            var result = new List<T>();
            var invoice = new T();
            foreach (var item in listOfEntities)
            {
                invoice = item.MapTo<T>();
                invoice.InvoiceItems = MapToInvoiceItemDto(item.InvoiceItems);
                result.Add(invoice);
            }

            return result;
        }

        private List<InvoiceItemDto> MapToInvoiceItemDto(IEnumerable<InvoiceItem> listOfEntities)
        {
            var result = new List<InvoiceItemDto>();
            foreach (var item in listOfEntities)
            {
                var dto = item.MapTo<InvoiceItemDto>();
                dto.Id = item.Id != Guid.Empty ? item.Id : (Guid?)null;
                dto.BillingAccountItemDate = dto.BillingAccountItemDate != DateTime.MinValue ? dto.BillingAccountItemDate : (DateTime?)null;
                result.Add(dto);
            }

            return result;
        }

        public void ConfirmInvoiceReceipt(ConfirmationDto dto)
        {
           if (_applicationUser.TenantId == Guid.Empty)
                NotifyNoTenant();

            if (Notification.HasNotification())
                return;

            ValidateDto<ConfirmationDto>(dto, nameof(dto));

            if (Notification.HasNotification())
                return;

            ValidateGuid(dto.Id.ToString(), (int)CMNETIntegrationEnum.GuidTypeValidate.BillingInvoiceIdValidate);

            if (Notification.HasNotification())
                return;

            _billingInvoiceRepository.ConfirmReceipt(dto);

        }

        public async Task<IEnumerable<BillingInvoiceNFCeDto>> GetNFCeInvoices(DateTime date)
        {
            await ValidateProperty(_applicationUser.PropertyId.TryParseToInt32());

            if (Notification.HasNotification())
                return null;

            var invoices = await _billingInvoiceReadRepository.GetNFCeInvoices(date);

            if (invoices.Count() == 0)
            {
                NotifyNotFound(CMNETIntegrationEnum.NotFoundErrors.BillingInvoicesForDateNotFound);
                return null;
            }

            var inovoicesDto = invoices.MapTo<IEnumerable<BillingInvoiceNFCeDto>>();
            var count = 0;    
            foreach (var invoice in inovoicesDto)
            {
                var response = await Policy
                                      .HandleResult<HttpResponseMessage>(message => !message.IsSuccessStatusCode)
                                      .WaitAndRetryAsync(1, i => TimeSpan.FromSeconds(60), (result, timeSpan, retryCount, context) => {})
                                      .ExecuteAsync(() => new HttpClient().GetAsync(invoice.IntegratorXml));

                var stream = response.Content.ReadAsByteArrayAsync();

                if (stream != null)
                {
                    count++;
                    invoice.XmlFile = await stream;
                }
            }

            return inovoicesDto;
        }
    }
}
