﻿using System.Threading.Tasks;
using Thex.CMNETIntegration.Application.Interfaces;
using Thex.CMNETIntegration.Dto;
using Thex.CMNETIntegration.Infra.Interfaces.ReadRepositories;
using Thex.Kernel;
using Tnf.Application.Services;
using Tnf.Notifications;

namespace Thex.CMNETIntegration.Application.Services
{
    public class PropertyAppService : ApplicationService, IPropertyAppService
    {
        private readonly IPropertyReadRepository _propertyReadRepository;

        public PropertyAppService(
            IPropertyReadRepository propertyReadRepository,
            INotificationHandler notificationHandler)
            : base(notificationHandler)
        {
            _propertyReadRepository = propertyReadRepository;
        }

        public virtual async Task<PropertyDto> Get(DefaultStringRequestDto document)
        {    
            if (!ValidateRequestDto(document))
                return null;

            if (Notification.HasNotification())
                return PropertyDto.NullInstace;

            var dto = await _propertyReadRepository.GetProperty(document.Id);

            return dto;
        }
    }
}
