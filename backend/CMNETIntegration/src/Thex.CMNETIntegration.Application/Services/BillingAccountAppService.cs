﻿using System;
using System.Threading.Tasks;
using Thex.CMNETIntegration.Application.Interfaces;
using Thex.CMNETIntegration.Dto;
using Thex.CMNETIntegration.Infra.Interfaces;
using Thex.Common;
using Thex.Common.Enumerations;
using Thex.Kernel;
using Tnf.Application.Services;
using Tnf.Notifications;
using Tnf.Repositories.Uow;
using Tnf.Runtime.Security;
using Thex.CMNETIntegration.Infra.Interfaces.ReadRepositories;
using Microsoft.Extensions.Configuration;
using Thex.CMNETIntegration.Application.Helpers;
using Thex.CMNETIntegration.Dto.Enumerations;

namespace Thex.CMNETIntegration.Application.Services
{
    public class BillingAccountAppService : ApplicationServiceBase, IBillingAccountAppService
    {        
        protected readonly IBillingAccountReadRepository _billingAccountReadRepository;
        protected readonly IBillingAccountRepository _billingAccountRepository;
        protected readonly ICompanyClientReadRepository _companyClientReadRepository;
        protected readonly IConfiguration _configuration;
        private readonly IApplicationUser _applicationUser;

        public BillingAccountAppService(
            IApplicationUser applicationUser,
            INotificationHandler notification,
            IBillingAccountReadRepository billingAccountReadRepository,
            IBillingAccountRepository billingAccountRepository,
            ICompanyClientReadRepository companyClientReadRepository,
            IPropertyReadRepository propertyReadRepository,
            IConfiguration configuration) : base(notification, propertyReadRepository)
        {
            _applicationUser = applicationUser;
            _billingAccountReadRepository = billingAccountReadRepository;
            _billingAccountRepository = billingAccountRepository;
            _companyClientReadRepository = companyClientReadRepository;
            _configuration = configuration;
        }

        private bool ValidateDocumenType(DocumentTypeEnum docType)
        {
            switch (docType)
            {
                case DocumentTypeEnum.BrNaturalPersonCPF:
                    return true;
                case DocumentTypeEnum.BrLegalPersonCNPJ:
                    return true;
                default:
                    return false;
            }            
        }

        public async Task<BillingAccountForEventDto> CreateSparseAccount(EventBillingAccountDto dto)
        {
            if (TokenHelper.TokenFromRequest.IsNullOrEmpty())
                NotifyBadRequest(CMNETIntegrationEnum.Error.EmptyToken);

            if (Notification.HasNotification())
                return BillingAccountForEventDto.NullInstance;

            if (!ValidateDocumenType(dto.CompanyClientDocumentType))
                NotifyBadRequest(CMNETIntegrationEnum.Error.DocumentTypeInvalid);

            if (Notification.HasNotification())
                return BillingAccountForEventDto.NullInstance;

            await ValidateProperty(_applicationUser.PropertyId.TryParseToInt32());            

            if (Notification.HasNotification())
                return BillingAccountForEventDto.NullInstance;

            var accountDto = new BillingAccountDto
            {
                BillingAccountName = dto.EventAccountName,
                PropertyId = _applicationUser.PropertyId.TryParseToInt32(),                
                BillingAccountTypeId = (int)BillingAccountTypeEnum.Sparse,
                StatusId = (int)AccountBillingStatusEnum.Opened,
                IsMainAccount = true,
                MarketSegmentId = 1,
                BusinessSourceId = 1                
            };

            var responseCC = await _companyClientReadRepository.Get(dto.CompanyClientDocument, (int)dto.CompanyClientDocumentType);

            if (responseCC == CompanyClientDto.NullInstance)
            {
                NotifyNotFound(CMNETIntegrationEnum.NotFoundErrors.CompanyClientNotFound);                         
            }

            if (Notification.HasNotification())
                return BillingAccountForEventDto.NullInstance;

            accountDto.CompanyClientId = responseCC.Id;

            var uri = HttpClientHelper.GetThexUrl(_configuration, "/api/BillingAccount/sparce");
            
            var responseDto = await HttpClientHelper.SendRequest<BillingAccountDto>(
                Notification, 
                CMNETIntegrationEnum.NotFoundErrors.BillingAccountNotFound, 
                uri.AbsoluteUri,
                TokenHelper.TokenFromRequest, 
                HttpVerbs.Post, 
                accountDto);

            BillingAccountForEventDto response = null;
            if (responseDto is BillingAccountDto)
            {
                response = MapBillingAccountForEventWithoutItems(responseDto);
            }

            return response;
        }

        public async Task<BillingAccountForEventDto> GetBillingAccountItems(string billingAccountId, bool associatedAccounts, DateTime? itemStartDate, DateTime? itemEndDate)
        {
            ValidateGuid(billingAccountId, (int)CMNETIntegrationEnum.GuidTypeValidate.BillingAccountIdValidate);

            if (Notification.HasNotification())
                return BillingAccountForEventDto.NullInstance;

            var accountDto = await _billingAccountReadRepository.GetBillingAccountAsync(billingAccountId);

            if (accountDto == BillingAccountForEventDto.NullInstance)
            {
                NotifyNotFound(CMNETIntegrationEnum.NotFoundErrors.BillingAccountNotFound);         
            }

            if (Notification.HasNotification())
                return BillingAccountForEventDto.NullInstance;

            var listItems = await _billingAccountReadRepository.GetBillingAccountItemsAsync(billingAccountId, associatedAccounts, itemStartDate, itemEndDate);

            accountDto.BillingAccountItemsForEvent = listItems;

            return accountDto;
        }
                

        private BillingAccountForEventDto MapBillingAccountForEventWithoutItems(BillingAccountDto entity)
        {
            try
            {
                return new BillingAccountForEventDto
                {
                    BillingAccountAmount = 0,
                    BillingAccountTypeId = (BillingAccountTypeEnum)Enum.Parse(typeof(BillingAccountTypeEnum), entity.BillingAccountTypeId.ToString()),
                    EndDate = entity.EndDate,
                    Id = entity.Id,
                    StartDate = entity.StartDate
                };
            }
            catch (Exception e)
            {
                Notification.Raise(Notification
                    .DefaultBuilder
                    .WithMessage(AppConsts.LocalizationSourceName, CMNETIntegrationEnum.Error.GeneralError)
                    .WithDetailedMessage(e.Message)
                    .Build());                
            }

            return BillingAccountForEventDto.NullInstance;
        }
    }
}
