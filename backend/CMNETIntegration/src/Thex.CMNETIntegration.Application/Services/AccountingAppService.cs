﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Thex.CMNETIntegration.Application.Interfaces;
using Thex.CMNETIntegration.Dto;
using Thex.CMNETIntegration.Dto.Enumerations;
using Thex.CMNETIntegration.Infra.Interfaces.ReadRepositories;
using Thex.Kernel;
using Tnf.Application.Services;
using Tnf.Notifications;

namespace Thex.CMNETIntegration.Application.Services
{
    public class AccountingAppService : ApplicationServiceBase, IAccountingAppService
    {
        private readonly IAccountingReadRepository _AccountingReadRepository;
        private readonly IApplicationUser _applicationUser;

        public AccountingAppService(
            IApplicationUser applicationUser,
            IAccountingReadRepository AccountingReadRepository,
            IPropertyReadRepository propertyReadRepository,
            INotificationHandler notificationHandler) : base(notificationHandler, propertyReadRepository)
        {
            _applicationUser = applicationUser;
            _AccountingReadRepository = AccountingReadRepository;
        }

        public virtual async Task<IEnumerable<AccountingDto>> Get(DateTime itemDate)
        {
            await ValidateProperty(_applicationUser.PropertyId.TryParseToInt32());
            if (Notification.HasNotification())
                return null;

            IEnumerable<AccountingDto> dto = await _AccountingReadRepository.GetByItemDate(itemDate);

            if (dto == null || !dto.Any())
            {
                NotifyNotFound(CMNETIntegrationEnum.NotFoundErrors.AccountingNotFound);
                return null;
            }

            return dto;
        }
    }
}
