﻿using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Thex.CMNETIntegration.Dto;

namespace Thex.CMNETIntegration.Application.Helpers
{    
    public static class TokenHelper
    {
        public static string TokenFromRequest = "";

        public static async Task<string> GetThexToken(IConfiguration configuration, int propertyId)
        {
            var Handler = new HttpClientHandler();
            var Client = new HttpClient(Handler);
            HttpResponseMessage response = null;

            var tokenInfo = new TokenInfoDto
            {
                TokenApplication = configuration.GetValue<string>(AppCmnetConsts.TokenApplicationThex),
                TokenClient = configuration.GetValue<string>(AppCmnetConsts.TokenClientThex),
                PropertyId = propertyId
            };

            var body = JsonConvert.SerializeObject(tokenInfo);

            var httpContent = new StringContent(body, Encoding.UTF8, "application/json");

            response = await Client.PostAsync(HttpClientHelper.GetSuperAdminUrl(configuration, "/api/IntegrationPartner/authenticate"), httpContent).ForAwait();            

            if (response.IsSuccessStatusCode)
            {
                var contentResponse = string.Empty;

                if (response?.Content != null)                
                    contentResponse = await response.Content.ReadAsStringAsync().ForAwait();

                if (!contentResponse.IsNullOrEmpty())
                {                   
                    UserTokenDto userTokenDto = JsonConvert.DeserializeObject<UserTokenDto>(contentResponse);
                    return "Bearer " + userTokenDto.NewPasswordToken;
                }
                return string.Empty;
            }
            else
                return string.Empty;
        }
    }
}
