﻿using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Thex.CMNETIntegration.Dto;
using Thex.CMNETIntegration.Dto.Enumerations;
using Thex.Common;
using Tnf.Notifications;
using Tnf.Runtime.Security;

namespace Thex.CMNETIntegration.Application.Helpers
{
    public class HttpClientHelper
    {                   
        public static async Task<T> SendRequest<T>(INotificationHandler notification, Enum notFoundEnum, string url, string token, HttpVerbs verb, object value = null)
        {
            var Handler = new HttpClientHandler();
            var Client = new HttpClient(Handler);

            Client.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", $"{token}");

            HttpResponseMessage response = null;

            switch (verb)
            {
                case HttpVerbs.Get:
                    response = await Client.GetAsync(url).ForAwait();
                    break;

                case HttpVerbs.Post:
                    var json = JsonConvert.SerializeObject(value);
                    var httpContent = new StringContent(json, Encoding.UTF8, "application/json");

                    try
                    {
                        response = await Client.PostAsync(url, httpContent).ForAwait();
                    }
                    catch (Exception e)
                    {
                        notification.Raise(notification
                        .DefaultBuilder
                        .WithMessage(AppConsts.LocalizationSourceName, CMNETIntegrationEnum.Error.GeneralError)
                        .WithDetailedMessage(e.Message)
                        .Build());
                    }

                    break;
            }

            var contentResponse = string.Empty;

            if (response?.Content != null)
                contentResponse = await response.Content.ReadAsStringAsync().ForAwait();

            switch (response?.StatusCode)
            {
                case HttpStatusCode.OK:
                case HttpStatusCode.Created:

                    if (!contentResponse.IsNullOrWhiteSpace())
                    {
                        return JsonConvert.DeserializeObject<T>(contentResponse);
                    }

                    return default(T);
                case HttpStatusCode.Unauthorized:
                    {
                        notification.Raise(notification
                        .DefaultBuilder
                        .WithMessage(AppConsts.LocalizationSourceName, CMNETIntegrationEnum.Error.Unauthorized)
                        .WithUnauthorizedStatus()
                        .Build());

                        break;
                    }
                case HttpStatusCode.NotFound:
                    {
                        notification.Raise(notification
                        .DefaultBuilder
                        .WithMessage(AppConsts.LocalizationSourceName, notFoundEnum)
                        .WithNotFoundStatus()
                        .Build());

                        break;
                    }
                case HttpStatusCode.BadRequest:
                    {
                        var errorResultMsg = JsonConvert.DeserializeObject<ErrorMessageDto>(contentResponse);                        

                        notification.Raise(notification
                        .DefaultBuilder
                        .WithCode(errorResultMsg.details.FirstOrDefault()?.code)
                        .WithMessage(AppConsts.LocalizationSourceName, CMNETIntegrationEnum.Error.SaveOrUpdateError)
                        .WithDetailedMessage(errorResultMsg.details.FirstOrDefault()?.message)
                        .WithBadRequestStatus()
                        .Build());

                        break;
                    }
                default:
                    {
                        notification.Raise(notification
                        .DefaultBuilder
                        .WithMessage(AppConsts.LocalizationSourceName, CMNETIntegrationEnum.Error.GeneralError)
                        .Build());

                        break;
                    }
            }

            return default(T);
        }

        public static Uri GetThexUrl(IConfiguration _configuration, string relativeUri)
        {
            var baseUri = new Uri(_configuration.GetValue<string>(AppCmnetConsts.ThexEndpoint));            

            return new Uri(baseUri, relativeUri);
        }

        public static Uri GetSuperAdminUrl(IConfiguration _configuration, string relativeUri)
        {
            var baseUri = new Uri(_configuration.GetValue<string>(AppCmnetConsts.SuperAdminEndpoint));

            return new Uri(baseUri, relativeUri);
        }
    }
}
