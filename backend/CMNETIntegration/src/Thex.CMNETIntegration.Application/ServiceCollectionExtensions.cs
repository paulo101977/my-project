﻿
using Thex.CMNETIntegration.Application.Adapters;
using Thex.CMNETIntegration.Application.Interfaces;
using Thex.CMNETIntegration.Application.Services;

namespace Microsoft.Extensions.DependencyInjection
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddApplicationServiceDependency(this IServiceCollection services)
        {
            services.AddInfraDependency();

            // Registro dos serviços
            services.AddTransient<IAccountingAdapter, AccountingAdapter>();
            services.AddTransient<IBillingAccountAdapter, BillingAccountAdapter>();

            services.AddTransient<IAccountingAppService, AccountingAppService>();
            services.AddTransient<IPropertyAppService, PropertyAppService>();

            services.AddTransient<IBillingAccountAppService, BillingAccountAppService>();
            services.AddTransient<IBillingAccountItemAppService, BillingAccountItemAppService>();

            services.AddTransient<ICompanyClientAppService, CompanyClientAppService>();

            services.AddTransient<IBillingInvoiceAppService, BillingInvoiceAppService>();
            services.AddTransient<IBillingItemAppService, BillingItemAppService>();

            services.AddTransient<IFinancialAppService, FinancialAppService>();

            services.AddTransient<IHealthAppService, HealthAppService>();
            return services;
        }
    }
}