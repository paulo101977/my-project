﻿namespace Thex.CMNETIntegration.Application
{
    public static class AppCmnetConsts
    {
        public const string ThexEndpoint = "ThexEndpoint";
        public const string TokenClientThex = "TokenClientThex";
        public const string TokenApplicationThex = "TokenApplicationThex";
        public const string SuperAdminEndpoint = "SuperAdminEndpoint";
    }
}
