﻿using Thex.CMNETIntegration.Dto;
using Thex.CMNETIntegration.Infra.Entities;

namespace Thex.CMNETIntegration.Application.Adapters
{
    public interface IBillingAccountAdapter
    {
        BillingAccount.Builder Map(BillingAccount entity, BillingAccountDto dto);
        BillingAccount.Builder Map(BillingAccountDto dto);
    }
}
