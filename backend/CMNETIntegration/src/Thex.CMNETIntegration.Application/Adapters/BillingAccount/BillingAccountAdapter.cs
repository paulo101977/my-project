﻿using Thex.CMNETIntegration.Dto;
using Thex.CMNETIntegration.Infra.Entities;
using Tnf;
using Tnf.Notifications;

namespace Thex.CMNETIntegration.Application.Adapters
{
    public class BillingAccountAdapter: IBillingAccountAdapter
    {
        private readonly INotificationHandler _notificationHandler;

        public BillingAccountAdapter(INotificationHandler notificationHandler)
        {
            _notificationHandler = notificationHandler;
        }

        public BillingAccount.Builder Map(BillingAccount entity, BillingAccountDto dto)
        {
            Check.NotNull(entity, nameof(entity));
            Check.NotNull(dto, nameof(dto));

            var builder = new BillingAccount.Builder(_notificationHandler, entity)
                .WithId(dto.Id)
                .WithBillingAccountName(dto.BillingAccountName)
                .WithBillingAccountType(dto.BillingAccountTypeId)
                .WithCompanyClientId(dto.CompanyClientId)
                .WithStartDate(dto.StartDate);

            return builder;
        }

        public BillingAccount.Builder Map(BillingAccountDto dto)
        {
            Check.NotNull(dto, nameof(dto));

            var builder = new BillingAccount.Builder(_notificationHandler)
                .WithId(dto.Id)
                .WithBillingAccountName(dto.BillingAccountName)
                .WithBillingAccountType(dto.BillingAccountTypeId)
                .WithCompanyClientId(dto.CompanyClientId)
                .WithStartDate(dto.StartDate);

            return builder;
        }
    }
}
