﻿using Thex.CMNETIntegration.Dto;
using Thex.CMNETIntegration.Infra.Entities;

namespace Thex.CMNETIntegration.Application.Adapters
{
    public interface ICompanyClientAdapter
    {
        CompanyClient.Builder Map(CompanyClient entity, CompanyClientDto dto);
        CompanyClient.Builder Map(CompanyClientDto dto);
    }
}
