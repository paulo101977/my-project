﻿using Thex.CMNETIntegration.Dto;
using Thex.CMNETIntegration.Infra.Entities;
using Tnf;
using Tnf.Notifications;

namespace Thex.CMNETIntegration.Application.Adapters
{
    public class CompanyClientAdapter : ICompanyClientAdapter
    {
        public INotificationHandler NotificationHandler { get; }

        public CompanyClientAdapter(INotificationHandler notificationHandler)
        {
            NotificationHandler = notificationHandler;
        }

        public virtual CompanyClient.Builder Map(CompanyClient entity, CompanyClientDto dto)
        {
            Check.NotNull(entity, nameof(entity));
            Check.NotNull(dto, nameof(dto));

            var builder = new CompanyClient.Builder(NotificationHandler, entity)
                .WithId(dto.Id)
                .WithCompanyId(dto.CompanyId)
                .WithPersonId(dto.PersonId)
                .WithPersonType(dto.PersonType)
                .WithPropertyId(dto.PropertyId)
                .WithShortName(dto.ShortName)
                .WithTradeName(dto.TradeName);

            return builder;
        }

        public virtual CompanyClient.Builder Map(CompanyClientDto dto)
        {
            Check.NotNull(dto, nameof(dto));

            var builder = new CompanyClient.Builder(NotificationHandler)
                .WithId(dto.Id)
                .WithCompanyId(dto.CompanyId)
                .WithPersonId(dto.PersonId)
                .WithPersonType(dto.PersonType)
                .WithPropertyId(dto.PropertyId)
                .WithShortName(dto.ShortName)
                .WithTradeName(dto.TradeName);

            return builder;
        }
    }

}
