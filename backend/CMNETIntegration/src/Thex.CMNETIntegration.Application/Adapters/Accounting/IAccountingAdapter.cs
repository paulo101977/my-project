﻿using Thex.CMNETIntegration.Dto;
using Thex.CMNETIntegration.Infra.Entities;

namespace Thex.CMNETIntegration.Application.Adapters
{
    public interface IAccountingAdapter
    {
        Accounting.Builder Map(Accounting entity, AccountingDto dto);
        Accounting.Builder Map(AccountingDto dto);
    }
}
