﻿using Thex.CMNETIntegration.Dto;
using Thex.CMNETIntegration.Infra.Entities;
using Tnf;
using Tnf.Notifications;

namespace Thex.CMNETIntegration.Application.Adapters
{
    public class AccountingAdapter : IAccountingAdapter
    {
        public INotificationHandler NotificationHandler { get; }

        public AccountingAdapter(INotificationHandler notificationHandler)
        {
            NotificationHandler = notificationHandler;
        }

        public virtual Accounting.Builder Map(Accounting entity, AccountingDto dto)
        {
            Check.NotNull(entity, nameof(entity));
            Check.NotNull(dto, nameof(dto));

            var builder = new Accounting.Builder(NotificationHandler, entity)
                .WithIntegrationCode(dto.IntegrationCode)
                .WithBillingAccountItemDate(dto.BillingAccountItemDate)
                .WithAmount(dto.Amount);

            return builder;
        }

        public virtual Accounting.Builder Map(AccountingDto dto)
        {
            Check.NotNull(dto, nameof(dto));

            var builder = new Accounting.Builder(NotificationHandler)
                .WithIntegrationCode(dto.IntegrationCode)
                .WithBillingAccountItemDate(dto.BillingAccountItemDate)
                .WithAmount(dto.Amount);

            return builder;
        }
    }

}
