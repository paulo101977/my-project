﻿namespace Thex.Reports.Web
{
    public class RouteConsts
    {
        public const string ReportsRoomRouteName = "api/ReportsRoom";
        public const string ReportsRoomReviewRouteName = "api/ReportsRoomReview";
        public const string ReportsRoomStopRouteName = "api/ReportsRoomStop";
        public const string ReportsRoomDisagreementRouteName = "api/ReportsRoomDisagreement";
        public const string StatusPropertyRouteName = "api/statusProperty";
        public const string ReasonRouteName = "api/ReportsReason";
        public const string RoomRouteName = "api/room";
        public const string UserRouteName = "api/user";
        public const string PensionPrevisionRouteName = "api/PensionPrevision";
        public const string BillingAccountTransferRouteName = "api/BillingAccountTransfer";
    }
}
