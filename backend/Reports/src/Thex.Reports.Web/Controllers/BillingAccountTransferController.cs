﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Thex.Reports.Application.Interfaces;
using Thex.Reports.Dto.Dto.BillingAccountTransfer;
using Tnf.AspNetCore.Mvc.Response;

namespace Thex.Reports.Web.Controllers
{
    [Route(RouteConsts.BillingAccountTransferRouteName)]
    public class BillingAccountTransferController : TnfController
    {
        private readonly IBillingAccountTransferAppService _billingAccountTransferAppService;

        public BillingAccountTransferController(IBillingAccountTransferAppService billingAccountTransferAppService)
        {
            _billingAccountTransferAppService = billingAccountTransferAppService;
        }

        [HttpGet]
        [ThexAuthorize("Reports_BillingAccountTransfer_Get")]
        [ProducesResponseType(typeof(BillingAccountTransferDto), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> GetAll([FromQuery] GetFilters filters)
        {
            var response = await _billingAccountTransferAppService.GetBillingAccountTransferAsync(filters);

            return CreateResponseOnGet(response);
        }
    }
}