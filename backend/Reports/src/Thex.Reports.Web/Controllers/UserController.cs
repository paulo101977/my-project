﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using Thex.Reports.Application.Interfaces;
using Thex.Reports.Dto;
using Thex.Reports.Infra.Entities;
using Tnf.AspNetCore.Mvc.Response;

namespace Thex.Reports.Web.Controllers
{
    [Route(RouteConsts.UserRouteName)]
    [Authorize]
    public class UserController : TnfController
    {
        private readonly IUserAppService _userAppService;

        public UserController(IUserAppService userAppService)
        {
            _userAppService = userAppService;
        }

        [HttpGet]
        [ProducesResponseType(typeof(UserDto), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> GetAll()
        {
            var response = await _userAppService.GetAll();

            return CreateResponseOnGet(response);
        }

        [HttpGet("getById")] // frontend mobile
        [ProducesResponseType(typeof(UserDto), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> GetById()
        {
            var response = await _userAppService.GetById();

            return CreateResponseOnGet(response);
        }
    }
}
