﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Thex.Reports.Application.Interfaces;
using Thex.Reports.Dto.Dto.PensionPrevision;
using Tnf.AspNetCore.Mvc.Response;

namespace Thex.Reports.Web.Controllers
{
    [Route(RouteConsts.PensionPrevisionRouteName)]
    public class PensionPrevisionController : TnfController
    {
        private readonly IPensionPrevisionAppService _pensionPrevision;

        public PensionPrevisionController(IPensionPrevisionAppService pensionPrevision)
        {
            _pensionPrevision = pensionPrevision;
        }

        [HttpGet]
        [ThexAuthorize("Reports_PensionPrevision_Get")]
        [ProducesResponseType(typeof(PensionPrevisionDto), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> GetAll([FromQuery] GetFilters filters)
        {
            var response = await _pensionPrevision.GetRangeDateAsync(filters);

            return CreateResponseOnGet(response);
        }
    }
}
