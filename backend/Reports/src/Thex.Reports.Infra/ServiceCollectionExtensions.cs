﻿using Thex.Common;
using Thex.Reports.Infra;
using Thex.Reports.Infra.Context;
using Thex.Reports.Infra.Interfaces;
using Thex.Reports.Infra.Interfaces.ReadRepositories;
using Thex.Reports.Infra.Mappers.DapperMappers;
using Thex.Reports.Infra.Mappers;
using Thex.Reports.Infra.Repositories;
using Thex.Reports.Infra.Repositories.ReadRepositories;
using Tnf.Dapper;

namespace Microsoft.Extensions.DependencyInjection
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddInfraDependency(this IServiceCollection services)
        {
            // Configura o uso do Dapper registrando os contextos que serão
            // usados pela aplicação
            services
                .AddCommonDependency()
                .AddTnfEntityFrameworkCore()
                .AddTnfDbContext<ThexReportsContext>(config => DbContextConfigurer.Configure(config))
                .AddTnfDapper(options =>
                {
                    //options.MapperAssemblies.Add(typeof(UserMapper).Assembly);
                    options.DbType = DapperDbType.SqlServer;
                });

            services.AddTnfAutoMapper(config =>
            {
                //config.AddProfile<UserProfile>();
            });

            services.AddTransient<IUserReadRepository, UserReadRepository>();
            services.AddTransient<IUserRepository, UserRepository>();
            services.AddTransient<IPensionPrevisionReadRepository, PensionPrevisionReadRepository>();
            services.AddTransient<IBillingAccountTransferReadRepository, BillingAccountTransferReadRepository>();

            return services;
        }
    }
}
