﻿using Thex.Common;
using Tnf.Configuration;
using Tnf.Localization;
using Tnf.Localization.Dictionaries;

namespace Thex.Reports.Infra
{
    public enum Operation
    {
        Insert,
        Update,
        Delete
    }
}
