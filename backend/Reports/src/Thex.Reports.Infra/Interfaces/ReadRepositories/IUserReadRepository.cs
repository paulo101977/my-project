﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.Reports.Dto;
using Thex.Reports.Infra.Entities;

namespace Thex.Reports.Infra.Interfaces.ReadRepositories
{
    public interface IUserReadRepository
    {
        Task<List<UserDto>> GetAll(Guid tenantId);
        Task<UserDto> GetById(Guid id); 
    }
}
