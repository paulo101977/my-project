﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.Reports.Dto.Dto.PensionPrevision;

namespace Thex.Reports.Infra.Interfaces.ReadRepositories
{
    public interface IPensionPrevisionReadRepository
    {
        Task<IEnumerable<PensionPrevisionDto>> GetRangeDate(GetFilters filters);
        Task<IEnumerable<ChildrenRange>> GetChildrenRange();
        Task<IEnumerable<GetMealPlan>> AllPension();
    }
}
