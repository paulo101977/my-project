﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.Reports.Dto.Dto.BillingAccountTransfer;

namespace Thex.Reports.Infra.Interfaces.ReadRepositories
{
    public interface IBillingAccountTransferReadRepository
    {
        Task<IEnumerable<BillingAccountTransferDto>> GetBillingAccountTransferAsync(GetFilters filters);
        Task<IEnumerable<OriginalAccountDto>> GetOriginalAccountByBillingAccountItemIdAsync(List<Guid> billingAccountItemIds);
    }
}
