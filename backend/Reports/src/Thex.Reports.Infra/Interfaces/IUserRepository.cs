﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.Reports.Dto;
using Thex.Reports.Infra.Entities;
using Tnf.Dto;

namespace Thex.Reports.Infra.Interfaces
{
    public interface IUserRepository
    {
        Task UpdateItem(Guid id, User obj);
    }
}
