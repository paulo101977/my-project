﻿using Microsoft.EntityFrameworkCore;
using Thex.Reports.Infra.Entities;
using Tnf.EntityFrameworkCore;
using Tnf.Runtime.Session;

namespace Thex.Reports.Infra.Context
{
    public class ThexReportsContext : TnfDbContext
    {
        // Importante o construtor do contexto receber as opções com o tipo generico definido: DbContextOptions<TDbContext>
        public ThexReportsContext(DbContextOptions<ThexReportsContext> options, ITnfSession session)
            : base(options, session)
        {
        }
    }
}
