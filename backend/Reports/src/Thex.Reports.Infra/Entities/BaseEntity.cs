﻿using System;

namespace Thex.Reports.Infra.Entities
{
    public class BaseEntity : ThexFullAuditedEntity, IEntityGuid
    {
        public Guid Id { get; set; }
    }
}
