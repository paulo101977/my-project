﻿using System;

namespace Thex.Reports.Infra.Entities
{
    [Serializable]
    public abstract class ThexMultiTenantFullAuditedEntity : ThexFullAuditedEntity
    {
        public Guid? TenantId { get; set; }

        public Tenant Tenant { get; set; }
    }
}
