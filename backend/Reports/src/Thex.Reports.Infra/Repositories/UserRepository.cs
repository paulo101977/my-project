﻿using System.Threading.Tasks;
using Tnf.Dapper.Repositories;
using Tnf.Repositories;
using Tnf.Localization;
using Thex.Reports.Infra.Context;
using Thex.Reports.Infra.Entities;
using Thex.Reports.Infra.Interfaces;
using System;
using Thex.Reports.Dto;
using Thex.Kernel;

namespace Thex.Reports.Infra.Repositories
{
    public class UserRepository : DapperEfRepositoryBase<ThexReportsContext, User>, IUserRepository
    {
        private readonly ILocalizationManager _localizationManager;
        private readonly IApplicationUser _applicationUser;

        public UserRepository(
            IActiveTransactionProvider activeTransactionProvider,
            ILocalizationManager localizationManager,
            IApplicationUser applicationUser)
            : base(activeTransactionProvider)
        {
            _localizationManager = localizationManager;
            _applicationUser = applicationUser;
        }

        public async Task UpdateItem(Guid id, User obj)
        {
            await UpdateAsync(obj);
        }
    }
}
