﻿using System;
using System.Threading.Tasks;
using Tnf.Dapper.Repositories;
using Tnf.Repositories;
using System.Linq;
using Tnf.Localization;
using Thex.Common;
using Thex.Reports.Infra.Context;
using Thex.Reports.Infra.Entities;
using Thex.Reports.Infra.Interfaces.ReadRepositories;
using Thex.Reports.Dto.Enumerations;
using Thex.Reports.Dto;
using System.Collections.Generic;
using Dapper;
using Slapper;
using Thex.Common.Enumerations;

namespace Thex.Reports.Infra.Repositories.ReadRepositories
{
    public class UserReadRepository : DapperEfRepositoryBase<ThexReportsContext, User>, IUserReadRepository
    {
        private readonly ILocalizationManager _localizationManager;

        public UserReadRepository(
            IActiveTransactionProvider activeTransactionProvider,
            ILocalizationManager localizationManager)
            : base(activeTransactionProvider)
        {
            _localizationManager = localizationManager;
        }

        public async Task<UserDto> GetById(Guid id)
        {

            var query = (await QueryAsync<UserDto>(@"
                select
                    u.*,
                    p.DateOfBirth,
                    p.PhotoUrl
                From   Users u
                left   join Person p on u.PersonId = p.PersonId
                where  u.Id =  @Id
            ", new { Id = id }));

            var result = query.FirstOrDefault(); 

            return result;
        }

        public new async Task<List<UserDto>> GetAll(Guid tenantId)
        {
            var result = (await QueryAsync<UserDto>(@"
                select u.Id as Id, u.*
                from   Users u
                where  u.TenantId = @TenantId
            ", new { TenantId = tenantId })).ToList();

            return result;
        }

        private string GetStatusName(int? statusId)
        {
            if ((int)ReservationStatus.Checkin == statusId) return _localizationManager.GetString(AppConsts.LocalizationSourceName, RoomAvailabilityEnum.Busy.ToString());
            else return _localizationManager.GetString(AppConsts.LocalizationSourceName, RoomAvailabilityEnum.Vague.ToString());
        }
    }
}
