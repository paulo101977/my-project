﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.Common.Enumerations;
using Thex.Common.Queries;
using Thex.Kernel;
using Thex.Reports.Dto.Dto.PensionPrevision;
using Thex.Reports.Infra.Context;
using Thex.Reports.Infra.Interfaces.ReadRepositories;
using Tnf.Dapper.Repositories;
using Tnf.Localization;
using Tnf.Repositories;

namespace Thex.Reports.Infra.Repositories.ReadRepositories
{
    public class PensionPrevisionReadRepository : DapperEfRepositoryBase<ThexReportsContext, PensionPrevisionDto>, IPensionPrevisionReadRepository
    {
        private readonly ILocalizationManager _localizationManager;
        private readonly IApplicationUser _applicationUser;

        public PensionPrevisionReadRepository(
            IActiveTransactionProvider activeTransactionProvider,
            ILocalizationManager localizationManager,
            IApplicationUser applicationUser)
            : base(activeTransactionProvider)
        {
            _localizationManager = localizationManager;
            _applicationUser = applicationUser;
        }

        public async Task<IEnumerable<PensionPrevisionDto>> GetRangeDate(GetFilters filters)
        {
            var checkIn = ReservationItemQueries.CheckInOrEstimatedArrivalDate("gri", "DateStart");
            var checkOut = ReservationItemQueries.CheckOutOrEstimatedDepartureDate("gri", "DateEnd");            
            var dateStart = filters.DateStart.ToString("yyyy-MM-dd");
            var dateEnd = filters.DateEnd.ToString("yyyy-MM-dd");
            var isDeletedReservation = ReservationQueries.IsDeleted("r", "0");
            var isDeletedReservationItem = ReservationQueries.IsDeleted("ri", "0");
            var isDeletedReservationBudget = ReservationQueries.IsDeleted("rb", "0");
            var isDeletedGuestReservationItem = ReservationQueries.IsDeleted("gri", "0");
            var childAge = GuestReservationItemQueries.CoalesceChildAge("gri", "childAge");
            var propertyId = PropertyQueries.FiltersByPropertyId("r", int.Parse(_applicationUser.PropertyId));
            var statusIdList = new List<int>() { (int)ReservationStatus.ToConfirm, (int)ReservationStatus.Confirmed, (int)ReservationStatus.Checkin,
                                                 (int)ReservationStatus.Checkout, (int)ReservationStatus.Pending };
            var propertyMealPlanId = PropertyQueries.FiltersByPropertyId("m", int.Parse(_applicationUser.PropertyId));
            var isActive = PropertyQueries.FiltersByIsActive("m", "1");

            return (await QueryAsync<PensionPrevisionDto>(
              $"select distinct " +
                $"CASE WHEN Cast(COALESCE(gri.checkindate, gri.estimatedarrivaldate) AS DATE) > rb.budgetday THEN " +
                $"Cast(COALESCE(gri.checkindate, gri.estimatedarrivaldate) AS DATE) ELSE rb.budgetday END AS Date, " +
                $"{checkIn}, " +
                $"{checkOut}, " +
                $"Cast(COALESCE(ri.checkindate, ri.estimatedarrivaldate) AS DATE) AS ReservationDateStart, " +
                $"Cast(COALESCE(ri.checkoutdate, ri.estimateddeparturedate) AS DATE) AS ReservationDateEnd, " +
                $"rb.MealPlanTypeId as MealPlanTypeId, gri.IsChild as IsChild, " +
                $"{childAge}, " +
                $"ri.ReservationItemId, ri.ReservationItemStatusId as ReservationStatus, gri.GuestReservationItemId as GuestReservationItemId, gri.GuestStatusId	" +
             $"from reservation r " +
               $"inner join reservationitem ri on r.reservationid = ri.reservationid and ri.ReservationItemStatusId IN @statusIdList " +
               $"inner join guestreservationitem gri on ri.reservationitemid = gri.reservationitemid and gri.guestStatusId in @statusIdList " +
               $"inner join reservationbudget rb on ri.reservationitemid = rb.reservationitemid and " +
               $"((rb.BudgetDay >= Cast(COALESCE(gri.checkindate, gri.estimatedarrivaldate) AS DATE) and " +
               $"rb.BudgetDay < Cast(COALESCE(gri.checkoutdate, gri.estimateddeparturedate) AS DATE)) or " +
               $"(rb.BudgetDay = Cast(COALESCE(gri.checkindate, gri.estimatedarrivaldate) AS DATE) and " +
               $"rb.BudgetDay = Cast(COALESCE(gri.checkoutdate, gri.estimateddeparturedate) AS DATE)) or " +
               $"rb.BudgetDay = DATEADD(day,-1,Cast(COALESCE(gri.checkoutdate, gri.estimateddeparturedate) AS DATE))) " +
               $"inner join PropertyMealPlanType m on m.MealPlanTypeId = rb.MealPlanTypeId and {propertyMealPlanId} and {isActive}" +
             $"where " +
               $"{isDeletedReservation} and " +
               $"{isDeletedReservationItem} and " +
               $"{isDeletedReservationBudget} and " +
               $"{isDeletedGuestReservationItem} and " +
               $"{propertyId} and " +
               $"(Cast(COALESCE(gri.checkindate, gri.estimatedarrivaldate) AS DATE) <= @dateEnd and " +
               $"Cast(COALESCE(gri.checkoutdate, gri.estimateddeparturedate) AS DATE) >= @dateStart)",
               new
               {
                   statusIdList,
                   dateStart,
                   dateEnd
               }));
        }

        public async Task<IEnumerable<ChildrenRange>> GetChildrenRange()
        {
            var propertyId = PropertyQueries.FiltersByPropertyId("p", int.Parse(_applicationUser.PropertyId));
            var isActive = PropertyQueries.FiltersByIsActive("p", "1");

            return (await QueryAsync<ChildrenRange>(
            "select PropertyParameterValue as FinalRange, applicationparameterid as RangeCod " +
            "from propertyparameter p " +
            $"where {propertyId} and " +
               $"p.applicationparameterid in (7,8,9) and {isActive}" +
            "order by p.applicationparameterid "));

        }

        public async Task<IEnumerable<GetMealPlan>> AllPension()
        {
            var propertyId = PropertyQueries.FiltersByPropertyId("m", int.Parse(_applicationUser.PropertyId));
            var isActive = PropertyQueries.FiltersByIsActive("m", "1");

            return (await QueryAsync<GetMealPlan>("select MealPlanTypeId as MealPlanId, PropertyMealPlanTypeName as MealPlanName from propertymealplantype  m " +
                                                  $"where {propertyId} and {isActive}"));
        }
    }
}
