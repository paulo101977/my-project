﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.Kernel;
using Thex.Reports.Dto.Dto.BillingAccountTransfer;
using Thex.Reports.Infra.Context;
using Thex.Reports.Infra.Interfaces.ReadRepositories;
using Tnf.Dapper.Repositories;
using Tnf.Localization;
using Tnf.Repositories;

namespace Thex.Reports.Infra.Repositories.ReadRepositories
{
    public class BillingAccountTransferReadRepository : DapperEfRepositoryBase<ThexReportsContext, BillingAccountTransferDto>, IBillingAccountTransferReadRepository
    {
        private readonly ILocalizationManager _localizationManager;
        private readonly IApplicationUser _applicationUser;

        public BillingAccountTransferReadRepository(
          IActiveTransactionProvider activeTransactionProvider,
          ILocalizationManager localizationManager,
          IApplicationUser applicationUser)
          : base(activeTransactionProvider)
        {
            _localizationManager = localizationManager;
            _applicationUser = applicationUser;
        }

        public async Task<IEnumerable<BillingAccountTransferDto>> GetBillingAccountTransferAsync(GetFilters filters)
        {
            return (await QueryAsync<BillingAccountTransferDto>(@"
                    select item.BillingAccountItemId, bi.BillingItemName, src.BillingAccountName as BillingAccountSource, 
                           dest.BillingAccountName as BillingAccountDestination, type.BillingAccountItemTypeId, 
                           type.BillingAccountItemTypeName as BillingAccountItemType, 
                           item.Amount, trans.CreationTime, Users.Name as UserName, risrc.ReservationItemCode as ReservationItemCodeSource, 
                           ridest.ReservationItemCode as ReservationItemCodeDestination, c.Symbol as CurrencySymbol
                    from BillingAccountItemTransfer trans
                    join BillingAccount src
                        on trans.BillingAccountIdSource = src.BillingAccountId
                        and src.IsDeleted = 0
                        and src.TenantId = @tenantId
                    join BillingAccount dest
                        on trans.BillingAccountIdDestination = dest.BillingAccountId
                        and dest.IsDeleted = 0
                        and dest.TenantId = @tenantId
                    join BillingAccountItem item
                        on item.BillingAccountItemId = trans.BillingAccountItemId
                        and item.IsDeleted = 0
                        and item.TenantId = @tenantId
                    join BillingAccountItemType type
                        on type.BillingAccountItemTypeId = item.BillingAccountItemTypeId
                    join BillingItem bi
                        on item.BillingItemId = bi.BillingItemId
                        and bi.IsDeleted = 0
                        and bi.TenantId = @tenantId
                    left join ReservationItem risrc
                        on risrc.ReservationItemId = src.ReservationItemId
                        and risrc.IsDeleted = 0
                        and risrc.TenantId = @tenantId
                    left join ReservationItem ridest
                        on ridest.ReservationItemId = dest.ReservationItemId
                        and ridest.IsDeleted = 0
                        and ridest.TenantId = @tenantId
                    join Users
                        on Users.Id = trans.CreatorUserId
                    join Currency c
                        on c.CurrencyId = item.CurrencyId
                    where trans.IsDeleted = 0
                        and trans.TenantId = @tenantId
                        and cast(trans.CreationTime as date) >= @dateStart
                        and cast(trans.CreationTime as date) <= @dateEnd
                    order by bi.BillingItemName, item.BillingAccountItemId, trans.CreationTime", 
                        new
                        {
                            tenantId = _applicationUser.TenantId,
                            dateStart = filters.DateStart,
                            dateEnd = filters.DateEnd
                        }));
        }

        public async Task<IEnumerable<OriginalAccountDto>> GetOriginalAccountByBillingAccountItemIdAsync(List<Guid> billingAccountItemIds)
        {
            return (await QueryAsync<OriginalAccountDto>(@"
                    select bait.BillingAccountItemId, BillingAccountName as OriginalAccount
                    from BillingAccountItemTransfer bait
                    join BillingAccount ba
                        on bait.BillingAccountIdSource = ba.BillingAccountId
                        and ba.IsDeleted = 0
                        and ba.TenantId = @tenantId
                    join (select BillingAccountItemId, min(bait.CreationTime) as creationTime
                            from BillingAccountItemTransfer bait		
                            where BillingAccountItemId in @billingAccountItem
                            and bait.IsDeleted = 0 and bait.TenantId = @tenantId
                            group by BillingAccountItemId) original
                        on original.BillingAccountItemId = bait.BillingAccountItemId
                        and bait.CreationTime = original.creationTime
                    where bait.BillingAccountItemId in @billingAccountItem
                    and bait.IsDeleted = 0 and bait.TenantId = @tenantId",
                        new
                        {
                            tenantId = _applicationUser.TenantId,
                            billingAccountItem = billingAccountItemIds
                        }));
        }
    }
}
