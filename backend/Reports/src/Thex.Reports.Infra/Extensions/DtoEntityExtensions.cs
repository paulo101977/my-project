﻿using System;
using System.Diagnostics;
using Thex.Common;
using Thex.Common.Extensions;
using Thex.Reports.Infra.Entities;
using Thex.Kernel;

namespace Thex.Reports.Infra.Extensions
{
    public static class DtoEntityExtensions
    {
        /// <summary>
        /// Preenche os campos de acordo com a operação (Inserção, Edição e deleção)
        /// </summary>
        public static BaseEntity SetOperationValues(this BaseEntity entity, Operation operation, IApplicationUser _applicationUser)
        {
            var timeZone = _applicationUser.TimeZoneName;
            var userId = _applicationUser.UserUid;
           
            switch (operation)
            {
                case Operation.Insert:
                    {
                        entity.Id = Guid.NewGuid();
                        entity.CreationTime = DateTime.UtcNow.ToZonedDateTime(timeZone);
                        entity.CreatorUserId = userId;
                        entity.IsDeleted = false;
                    }
                    break;
                case Operation.Update:
                    {
                        entity.LastModificationTime = DateTime.UtcNow.ToZonedDateTime(timeZone);
                        entity.LastModifierUserId = userId;
                    }
                    break;
                case Operation.Delete:
                    {
                        entity.IsDeleted = true;
                        entity.DeletionTime = DateTime.UtcNow.ToZonedDateTime(timeZone);
                        entity.DeleterUserId = userId;
                    }
                    break;
            }

            return entity;
        }
    }
}