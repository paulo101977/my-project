﻿using Thex.Common;
using Tnf.Configuration;
using Tnf.Localization;
using Tnf.Localization.Dictionaries;

namespace Thex.Reports.Infra
{
    public static class TnfConfigurationExtensions
    {
        public static void UseDomainLocalization(this ITnfConfiguration configuration)
        {
            // Incluindo o source de localização
            configuration.Localization.Sources.Add(
                new DictionaryBasedLocalizationSource(AppConsts.LocalizationSourceName,
                new JsonEmbeddedFileLocalizationDictionaryProvider(
                    typeof(TnfConfigurationExtensions).Assembly,
                    "Thex.Reports.Infra.Localization.SourceFiles")));

            // Incluindo suporte as seguintes linguagens
            configuration.Localization.Languages.Add(new LanguageInfo("pt-BR", "Português", isDefault: true));
            configuration.Localization.Languages.Add(new LanguageInfo("en-US", "English"));
            configuration.Localization.Languages.Add(new LanguageInfo("es-AR", "Espanhol"));
            configuration.Localization.Languages.Add(new LanguageInfo("es-CL", "Espanhol"));
            configuration.Localization.Languages.Add(new LanguageInfo("pt-PT", "Português"));
        }
    }
}
