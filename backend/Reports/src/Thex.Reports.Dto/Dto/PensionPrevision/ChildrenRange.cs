﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Thex.Reports.Dto.Dto.PensionPrevision
{
    public class ChildrenRange
    {
        public int InitialRange { get; set; }
        public int FinalRange { get; set; }
        public int RangeCod { get; set; }
    }
}
