﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using Thex.Common.Enumerations;
using Tnf.Dto;

namespace Thex.Reports.Dto.Dto.PensionPrevision
{
    public class PensionPrevisionDto : RequestAllDto
    {
        public DateTime Date { get; set; }
        public int TotalOccupied { get; set; }
        public int TotalCheckIn { get; set; }
        public int TotalCheckOut { get; set; }
        public int TotalAdult { get; set; }
        public int? TotalChild { get; set; }
        public int? TotalChild2 { get; set; }
        public int? TotalChild3 { get; set; }
        public int PropertyId { get; set; }
        public IEnumerable<MealPlan> MealPlan { get; set; }

        [JsonIgnore]
        public DateTime DateStart { get; set; }
        [JsonIgnore]
        public DateTime DateEnd { get; set; }
        [JsonIgnore]
        public DateTime ReservationDateStart { get; set; }
        [JsonIgnore]
        public DateTime ReservationDateEnd { get; set; }
        [JsonIgnore]
        public int MealPlanTypeId { get; set; }
        [JsonIgnore]
        public bool IsChild { get; set; }
        [JsonIgnore]
        public int ChildAge { get; set; }
        [JsonIgnore]
        public int ReservationItemId { get; set; }
        [JsonIgnore]
        public int ReservationStatus { get; set; }
        [JsonIgnore]
        public int GuestReservationItemId { get; set; }      
        [JsonIgnore]
        public int GuestStatusId { get; set; }

        public PensionPrevisionDto()
        {
            MealPlan = new HashSet<MealPlan>();
        }

        public enum EntityError
        {
            InvalidDate
        }

    }

    public class MealPlan
    {
        public int MealPlanType { get; set; }
        public string MealPlanTypeName { get; set; }
        public int TotalAdult { get; set; }
        public int TotalChild { get; set; }
        public int TotalChild2 { get; set; }
        public int TotalChild3 { get; set; }
        public int TotalMealPlan { get; set; }
    }

    public class GetFilters
    {
        public DateTime DateStart { get; set; }
        public DateTime DateEnd { get; set; }

    }

    public class GetMealPlan
    {
        public int MealPlanId { get; set; }
        public string MealPlanName { get; set; }
    }
}
