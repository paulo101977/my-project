﻿using System;

namespace Thex.Reports.Dto.Dto.BillingAccountTransfer
{
    public class OriginalAccountDto
    {
        public Guid BillingAccountItemId { get; set; }
        public string OriginalAccount { get; set; }
    }
}
