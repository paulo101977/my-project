﻿using Newtonsoft.Json;
using System;

namespace Thex.Reports.Dto.Dto.BillingAccountTransfer
{
    public class BillingAccountTransferDto
    {
        public Guid BillingAccountItemId { get; set; }
        public string BillingItemName { get; set; }
        public string BillingAccountOriginal { get; set; }
        public string BillingAccountSource { get; set; }
        public string BillingAccountDestination { get; set; }
        [JsonIgnore]
        public int BillingAccountItemTypeId { get; set; }
        public string BillingAccountItemType { get; set; }
        public double? Amount { get; set; }
        public DateTime CreationTime { get; set; }
        public string UserName { get; set; }
        public string ReservationItemCodeSource { get; set; }
        public string ReservationItemCodeDestination { get; set; }
        public string CurrencySymbol { get; set; }

        public string AmountFormatted
        {
            get
            {
                return Amount.HasValue ? $"{CurrencySymbol} {string.Format("{0:0.00}", Amount)}" : string.Empty;
            }
        }

        public enum EntityError
        {
            InvalidDate,
            DateMustHaveValue
        }
    }

    public class GetFilters
    {
        public DateTime DateStart { get; set; }
        public DateTime DateEnd { get; set; }
        public string BillingAccountName { get; set; }
        public string ReservationItemCode { get; set; }
    }    
}
