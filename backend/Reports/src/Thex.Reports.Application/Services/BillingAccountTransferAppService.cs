﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Thex.Common;
using Thex.Common.Enumerations;
using Thex.Reports.Application.Interfaces;
using Thex.Reports.Dto.Dto.BillingAccountTransfer;
using Thex.Reports.Infra.Interfaces.ReadRepositories;
using Tnf.Application.Services;
using Tnf.Dto;
using Tnf.Localization;
using Tnf.Notifications;

namespace Thex.Reports.Application.Services
{
    public class BillingAccountTransferAppService : ApplicationService, IBillingAccountTransferAppService
    {
        private readonly ILocalizationManager _localizationManager;
        private readonly IBillingAccountTransferReadRepository _billingAccountTransferReadRepository;

        public BillingAccountTransferAppService(
            INotificationHandler notificationHandler,
            ILocalizationManager localizationManager,
            IBillingAccountTransferReadRepository billingAccountTransferReadRepository)
          : base(notificationHandler)
        {
            _localizationManager = localizationManager;
            _billingAccountTransferReadRepository = billingAccountTransferReadRepository;
        }

        public async Task<IListDto<BillingAccountTransferDto>> GetBillingAccountTransferAsync(GetFilters filters)
        {
            ValidateFilters(filters);
            if (Notification.HasNotification())
                return null;

            return new ListDto<BillingAccountTransferDto>
            {
                Items = await GeneratorRel(await _billingAccountTransferReadRepository.GetBillingAccountTransferAsync(filters), filters),
                HasNext = false
            };
        }

        private async Task<IList<BillingAccountTransferDto>> GeneratorRel(IEnumerable<BillingAccountTransferDto> billingAccountTransfers, GetFilters filters)
        {
            var result = new List<BillingAccountTransferDto>();

            var billingAccountItemList = billingAccountTransfers.Select(x => x.BillingAccountItemId).Distinct().ToList();

            var originalAccounts = await _billingAccountTransferReadRepository.GetOriginalAccountByBillingAccountItemIdAsync(billingAccountItemList);

            billingAccountTransfers = BillingAccountTransfersFilters(billingAccountTransfers, filters);

            foreach (var billingAccountTransfer in billingAccountTransfers)
            {
                billingAccountTransfer.BillingAccountOriginal = originalAccounts.FirstOrDefault(x => x.BillingAccountItemId ==
                                                                                 billingAccountTransfer.BillingAccountItemId) == null ? string.Empty :
                                                                originalAccounts.FirstOrDefault(x => x.BillingAccountItemId ==
                                                                                 billingAccountTransfer.BillingAccountItemId).OriginalAccount;
                billingAccountTransfer.BillingAccountItemType = _localizationManager.GetString(AppConsts.LocalizationSourceName, 
                                                                ((BillingAccountItemTypeEnum)billingAccountTransfer.BillingAccountItemTypeId).ToString());
                result.Add(billingAccountTransfer);
            }

            return result;
        }

        private IEnumerable<BillingAccountTransferDto> BillingAccountTransfersFilters(IEnumerable<BillingAccountTransferDto> billingAccountTransfers, GetFilters filters)
        {          
            if (!string.IsNullOrWhiteSpace(filters.BillingAccountName))
                billingAccountTransfers = billingAccountTransfers.Where(x => (x.BillingAccountSource != null && x.BillingAccountSource.ToLower()
                                                                                                                 .Contains(filters.BillingAccountName.ToLower())) ||
                                                                             (x.BillingAccountDestination != null && x.BillingAccountDestination.ToLower()
                                                                                                                 .Contains(filters.BillingAccountName.ToLower())));

            if (!string.IsNullOrWhiteSpace(filters.ReservationItemCode))
                billingAccountTransfers = billingAccountTransfers.Where(x => (x.ReservationItemCodeSource != null && x.ReservationItemCodeSource.Trim().ToLower()
                                                                                                           .Contains(filters.ReservationItemCode.Trim().ToLower())) ||
                                                                             (x.ReservationItemCodeDestination != null && x.ReservationItemCodeDestination.Trim()
                                                                                                 .ToLower().Contains(filters.ReservationItemCode.Trim().ToLower())));

            return billingAccountTransfers;
        }

        private void ValidateFilters(GetFilters filters)
        {
            if (filters.DateStart == DateTime.MinValue || filters.DateEnd == DateTime.MinValue)
            {
                Notification.Raise(Notification.DefaultBuilder
                    .WithMessage(AppConsts.LocalizationSourceName, BillingAccountTransferDto.EntityError.DateMustHaveValue)
                    .WithDetailedMessage(AppConsts.LocalizationSourceName, BillingAccountTransferDto.EntityError.DateMustHaveValue)
                    .Build()); return;
            }

            if (filters.DateStart.Date > filters.DateEnd.Date)
            {
                Notification.Raise(Notification.DefaultBuilder
                    .WithMessage(AppConsts.LocalizationSourceName, BillingAccountTransferDto.EntityError.InvalidDate)
                    .WithDetailedMessage(AppConsts.LocalizationSourceName, BillingAccountTransferDto.EntityError.InvalidDate)
                    .Build()); return;
            }
        }
    }
}
