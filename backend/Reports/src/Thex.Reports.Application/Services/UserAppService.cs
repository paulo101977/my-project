﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.Reports.Application.Adapters;
using Thex.Reports.Application.Interfaces;
using Thex.Reports.Dto;
using Thex.Reports.Infra.Interfaces;
using Thex.Reports.Infra.Interfaces.ReadRepositories;
using Thex.Kernel;
using Thex.Storage;
using Tnf.Application.Services;
using Tnf.Notifications;
using Tnf.Repositories.Uow;

namespace Thex.Reports.Application.Services
{
    public class UserAppService : ApplicationService, IUserAppService
    {
        private readonly IUserReadRepository _userReadRepository;
        private readonly IUserRepository _userRepository;
        private readonly IUserAdapter _userAdapter;

        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IApplicationUser _applicationUser;

        public UserAppService(
            IUserReadRepository userReadRepository,
            IUserRepository userRepository,
            IUserAdapter userAdapter,
            IApplicationUser applicationUser,
            IUnitOfWorkManager unitOfWorkManager,
            INotificationHandler notificationHandler)
            : base(notificationHandler)
        {
            _userReadRepository = userReadRepository;
            _userRepository = userRepository;
            _userAdapter = userAdapter;
            _applicationUser = applicationUser;

            _unitOfWorkManager = unitOfWorkManager;
        }

        public virtual async Task<List<UserDto>> GetAll()
        {
            return await _userReadRepository.GetAll(_applicationUser.TenantId);
        }
        

        public virtual async Task<UserDto> GetById()
        {
            return await _userReadRepository.GetById(_applicationUser.UserUid);
        }
    }
}
