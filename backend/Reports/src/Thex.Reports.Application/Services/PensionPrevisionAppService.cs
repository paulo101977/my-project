﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Thex.Common;
using Thex.Common.Enumerations;
using Thex.Kernel;
using Thex.Reports.Application.Interfaces;
using Thex.Reports.Dto.Dto.PensionPrevision;
using Thex.Reports.Infra.Interfaces.ReadRepositories;
using Tnf.Application.Services;
using Tnf.Dto;
using Tnf.Notifications;

namespace Thex.Reports.Application.Services
{
    public class PensionPrevisionAppService : ApplicationService, IPensionPrevisionAppService
    {
        private readonly IPensionPrevisionReadRepository _pensionPrevisionReadRepository;
        private readonly IApplicationUser _applicationUser;


        public PensionPrevisionAppService(
            IPensionPrevisionReadRepository pensionPrevisionReadRepository,
            INotificationHandler notificationHandler,
            IApplicationUser applicationUser)
            : base(notificationHandler)
        {
            _pensionPrevisionReadRepository = pensionPrevisionReadRepository;
            _applicationUser = applicationUser;
        }

        public async Task<IListDto<PensionPrevisionDto>> GetRangeDateAsync(GetFilters filters)
        {
            ValidateFilters(filters);
            if (Notification.HasNotification())
                return null;

            return new ListDto<PensionPrevisionDto>
            {
                Items = await GeneratorRel(await _pensionPrevisionReadRepository.GetRangeDate(filters), filters),
                HasNext = false
            };
        }

        private async Task<IList<PensionPrevisionDto>> GeneratorRel(IEnumerable<PensionPrevisionDto> pension, GetFilters filters)
        {

            var traksList = await TraksList(pension);

            var result = new List<PensionPrevisionDto>();

            var allPension = _pensionPrevisionReadRepository.AllPension();

            var rangeDates = CreateRangeOfDates(filters.DateStart, filters.DateEnd);

            var checkInList = new List<int>() {(int)ReservationStatus.ToConfirm, (int)ReservationStatus.Confirmed, (int)ReservationStatus.Checkin };

            var checkOutList = new List<int>() { (int)ReservationStatus.ToConfirm, (int)ReservationStatus.Confirmed, (int)ReservationStatus.Checkin,
                                                 (int)ReservationStatus.Checkout, (int)ReservationStatus.Pending };

            foreach (var date in rangeDates)
            {
                var dto = new PensionPrevisionDto
                {
                    Date = date,
                    TotalCheckIn = pension.Where(e => e.ReservationDateStart.Date == date && checkInList.Contains(e.ReservationStatus)).GroupBy(d => d.ReservationItemId).Count(),
                    TotalCheckOut = pension.Where(e => e.ReservationDateEnd.Date == date && checkOutList.Contains(e.ReservationStatus)).GroupBy(d => d.ReservationItemId).Count(),
                    TotalOccupied = pension.Where(e => e.ReservationDateStart.Date <= date && e.ReservationDateEnd.Date > date).GroupBy(d => d.ReservationItemId).Count(),
                    PropertyId = int.Parse(_applicationUser.PropertyId),
                    MealPlan = GeneratorPensionMealPlane(allPension, date, pension, traksList)
                };

                dto.TotalAdult = dto.MealPlan.Where(x => x.MealPlanType != (int)MealPlanTypeEnum.None).Sum(x => x.TotalAdult);
                dto.TotalChild = dto.MealPlan.Where(x => x.MealPlanType != (int)MealPlanTypeEnum.None).Sum(x => x.TotalChild);
                dto.TotalChild2 = dto.MealPlan.Where(x => x.MealPlanType != (int)MealPlanTypeEnum.None).Sum(x => x.TotalChild2);
                dto.TotalChild3 = dto.MealPlan.Where(x => x.MealPlanType != (int)MealPlanTypeEnum.None).Sum(x => x.TotalChild3);                

                result.Add(dto);
            }
            return result;
        }

        private async Task<ChildrenRange[]> TraksList(IEnumerable<PensionPrevisionDto> pension)
        {
            ChildrenRange[] rangeList = null;

            if (pension.Any(x => x.IsChild))
            {
                rangeList = (await _pensionPrevisionReadRepository.GetChildrenRange()).ToArray();

                for (int i = 1; i < rangeList.Count(); i++)
                {
                    var initialTrack = rangeList[i - 1].FinalRange + 1;

                    rangeList[i].InitialRange = initialTrack;
                }
            }

            return rangeList;
        }

        private List<DateTime> CreateRangeOfDates(DateTime startDate, DateTime finalDate)
        {
            var rangeDates = new List<DateTime>();

            while (startDate <= finalDate)
            {
                rangeDates.Add(startDate);

                startDate = startDate.AddDays(1);
            }
            return rangeDates;
        }

        private List<MealPlan> GeneratorPensionMealPlane(Task<IEnumerable<GetMealPlan>> allPension, DateTime date, IEnumerable<PensionPrevisionDto> pension, ChildrenRange[] traks)
        {
            //This method will generate a meal quantity list by MealPlan type.

            ChildrenRange[] rangeList = null;
            if (pension.Any(x => x.IsChild))
                rangeList = traks;

            var listMealPlan = GeneratorListMealPlan(allPension);

            var listMeal = pension.Select(x => x.MealPlanTypeId).Distinct();

            foreach (var mealPlanId in listMeal)
            {
                if ((int)MealPlanTypeEnum.Breakfast == mealPlanId)
                {
                    // The guest is only entitled in the days following CheckIn.

                    var mealPlanPension = pension.Where(x => x.Date == date && x.MealPlanTypeId == mealPlanId);

                    var mealPlanPensionCheckOut = pension.Where(x => x.DateEnd == date && x.Date == date.AddDays(-1) && x.MealPlanTypeId == mealPlanId);

                    if (mealPlanPension != null && mealPlanPension.Any())
                        AddMealPlanBreakFast(rangeList, ref listMealPlan, date, mealPlanPension);

                    if (mealPlanPensionCheckOut != null && mealPlanPensionCheckOut.Any())
                        AddMealPlanBreakFast(rangeList, ref listMealPlan, date, mealPlanPensionCheckOut, true);

                }
                else if ((int)MealPlanTypeEnum.Halfboard == mealPlanId || (int)MealPlanTypeEnum.Halfboardlunchdinner == mealPlanId)
                {
                    // The guest is only entitled in the days following CheckIn.
                    // On the day of checkout you are not entitled to this plan, but you are entitled to breakfast.

                    var mealPlanPension = pension.Where(x => x.Date == date && x.MealPlanTypeId == mealPlanId);

                    var mealPlanPensionCheckOut = pension.Where(x => x.DateEnd == date && x.Date == date.AddDays(-1) && x.MealPlanTypeId == mealPlanId);

                    var meal = listMealPlan.FirstOrDefault(x => x.MealPlanType == mealPlanId);

                    MealPlanAndBreakFast(date, rangeList, ref listMealPlan, mealPlanId, mealPlanPension, mealPlanPensionCheckOut, ref meal);
                }
                else if ((int)MealPlanTypeEnum.Halfboardlunch == mealPlanId)
                {
                    // The guest is only entitled in the days following CheckIn

                    var mealPlanPension = pension.Where(x => x.Date == date && x.MealPlanTypeId == mealPlanId);

                    var mealPlanPensionCheckOut = pension.Where(x => x.DateEnd == date && x.Date == date.AddDays(-1) && x.MealPlanTypeId == mealPlanId);

                    var meal = listMealPlan.FirstOrDefault(x => x.MealPlanType == mealPlanId);

                    if (mealPlanPension != null && mealPlanPension.Any())
                    {
                        if (rangeList != null && rangeList.Any())
                            CountChild(mealPlanId, date, mealPlanPension, rangeList, ref meal);

                        CountAdult(mealPlanId, date, mealPlanPension, ref meal);

                        meal.TotalMealPlan = (meal.TotalAdult + meal.TotalChild + meal.TotalChild2 + meal.TotalChild3);

                        AddMealPlanBreakFast(rangeList, ref listMealPlan, date, mealPlanPension);

                    }
                    if (mealPlanPensionCheckOut != null && mealPlanPensionCheckOut.Any())
                    {
                        meal = listMealPlan.FirstOrDefault(x => x.MealPlanType == mealPlanId);

                        if (rangeList != null && rangeList.Any())
                            CountChildCheckOut(rangeList, ref meal, mealPlanPensionCheckOut, date, mealPlanId);

                        CountAdultCheckOut(date, mealPlanPensionCheckOut, mealPlanId, ref meal);

                        meal.TotalMealPlan = (meal.TotalAdult + meal.TotalChild + meal.TotalChild2 + meal.TotalChild3);

                        AddMealPlanBreakFast(rangeList, ref listMealPlan, date, mealPlanPensionCheckOut, true);
                    }
                }
                else if ((int)MealPlanTypeEnum.Fullboard == mealPlanId || (int)MealPlanTypeEnum.Allinclusive == mealPlanId)
                {
                    // The guest is only entitled to this plan on the day of CheckIn, in the later days he is entitled to it and the Breakfast.

                    var mealPlanPension = pension.Where(x => x.Date == date && x.MealPlanTypeId == mealPlanId);

                    var mealPlanPensionCheckOut = pension.Where(x => x.DateEnd == date && x.Date == date.AddDays(-1) && x.MealPlanTypeId == mealPlanId);

                    var meal = listMealPlan.FirstOrDefault(x => x.MealPlanType == mealPlanId);

                    AddMealPlanAllInclusive(date, rangeList, ref listMealPlan, mealPlanId, mealPlanPension, mealPlanPensionCheckOut, ref meal);

                }
                else if((int)MealPlanTypeEnum.None == mealPlanId)
                {
                    var mealPlanPension = pension.Where(x => x.Date == date && x.MealPlanTypeId == mealPlanId);

                    var meal = listMealPlan.FirstOrDefault(x => x.MealPlanType == mealPlanId);

                    AddMealPlanNone(date, rangeList, ref listMealPlan, mealPlanId, mealPlanPension, ref meal);
                }
            }

            return listMealPlan;
        }

        private static void MealPlanAndBreakFast(DateTime date, ChildrenRange[] rangeList, ref List<MealPlan> listMealPlan, int mealPlanId, IEnumerable<PensionPrevisionDto> mealPlanPension, IEnumerable<PensionPrevisionDto> mealPlanPensionCheckOut, ref MealPlan meal)
        {
            if (mealPlanPension != null && mealPlanPension.Any())
            {
                if (rangeList != null && rangeList.Any())
                    CountChild(mealPlanId, date, mealPlanPension, rangeList, ref meal);

                CountAdult(mealPlanId, date, mealPlanPension, ref meal);

                meal.TotalMealPlan = (meal.TotalAdult + meal.TotalChild + meal.TotalChild2 + meal.TotalChild3);

                AddMealPlanBreakFast(rangeList, ref listMealPlan, date, mealPlanPension);

            }
            if (mealPlanPensionCheckOut != null && mealPlanPensionCheckOut.Any())            
                AddMealPlanBreakFast(rangeList, ref listMealPlan, date, mealPlanPensionCheckOut, true);
            
        }

        private static void AddMealPlanAllInclusive(DateTime date, ChildrenRange[] rangeList, ref List<MealPlan> listMealPlan, int mealPlanId, IEnumerable<PensionPrevisionDto> mealPlanPension, IEnumerable<PensionPrevisionDto> mealPlanPensionCheckOut, ref MealPlan meal)
        {
            if (mealPlanPension != null && mealPlanPension.Any())
            {
                if (rangeList != null && rangeList.Any())
                    CountChild(mealPlanId, date, mealPlanPension, rangeList, ref meal);

                CountAdult(mealPlanId, date, mealPlanPension, ref meal);

                meal.TotalMealPlan = (meal.TotalAdult + meal.TotalChild + meal.TotalChild2 + meal.TotalChild3);

                AddMealPlanBreakFast(rangeList, ref listMealPlan, date, mealPlanPension);
            }
            if (mealPlanPensionCheckOut != null && mealPlanPensionCheckOut.Any())
            {
                meal = listMealPlan.FirstOrDefault(x => x.MealPlanType == mealPlanId);

                if (rangeList != null && rangeList.Any())
                    CountChildCheckOut(rangeList, ref meal, mealPlanPensionCheckOut, date, mealPlanId);

                CountAdultCheckOut(date, mealPlanPensionCheckOut, mealPlanId, ref meal);

                meal.TotalMealPlan = (meal.TotalAdult + meal.TotalChild + meal.TotalChild2 + meal.TotalChild3);

                AddMealPlanBreakFast(rangeList, ref listMealPlan, date, mealPlanPensionCheckOut, true);
            }
        }

        private static void AddMealPlanNone(DateTime date, ChildrenRange[] rangeList, ref List<MealPlan> listMealPlan, int mealPlanId, IEnumerable<PensionPrevisionDto> mealPlanPension, ref MealPlan meal)
        {
            if (mealPlanPension != null && mealPlanPension.Any())
            {
                if (rangeList != null && rangeList.Any())
                    CountChild(mealPlanId, date, mealPlanPension, rangeList, ref meal);

                CountAdult(mealPlanId, date, mealPlanPension, ref meal);

                meal.TotalMealPlan = (meal.TotalAdult + meal.TotalChild + meal.TotalChild2 + meal.TotalChild3);
            }          
        }

        private static void AddMealPlanBreakFast(ChildrenRange[] traksList, ref List<MealPlan> listMealPlan, DateTime date, IEnumerable<PensionPrevisionDto> pensionList, bool checkOut = false)
        {
            var meal = listMealPlan.FirstOrDefault(x => x.MealPlanType == (int)MealPlanTypeEnum.Breakfast);

            if (meal != null)
            {
                if (checkOut)
                {
                    if (traksList != null && traksList.Any())
                        CountChildCheckOut(traksList, ref meal, pensionList, date, (int)MealPlanTypeEnum.Breakfast);

                    CountAdultCheckOut(date, pensionList, (int)MealPlanTypeEnum.Breakfast, ref meal);
                }
                else
                {
                    if (traksList != null && traksList.Any())
                        CountChild((int)MealPlanTypeEnum.Breakfast, date, pensionList, traksList, ref meal);

                    CountAdult((int)MealPlanTypeEnum.Breakfast, date, pensionList, ref meal);
                }

                meal.TotalMealPlan = (meal.TotalAdult + meal.TotalChild + meal.TotalChild2 + meal.TotalChild3);
            }
            else
            {
                meal = new MealPlan
                {
                    MealPlanType = (int)MealPlanTypeEnum.Breakfast,
                    MealPlanTypeName = MealPlanTypeEnum.Breakfast.ToString()
                };

                if (checkOut)
                {
                    if (traksList != null && traksList.Any())
                        CountChildCheckOut(traksList, ref meal, pensionList, date, (int)MealPlanTypeEnum.Breakfast);

                    CountAdultCheckOut(date, pensionList, (int)MealPlanTypeEnum.Breakfast, ref meal);
                }
                else
                {
                    if (traksList != null && traksList.Any())
                        CountChild((int)MealPlanTypeEnum.Breakfast, date, pensionList, traksList, ref meal);

                    CountAdult((int)MealPlanTypeEnum.Breakfast, date, pensionList, ref meal);
                }

                meal.TotalMealPlan = (meal.TotalAdult + meal.TotalChild + meal.TotalChild2 + meal.TotalChild3);

                listMealPlan.Add(meal);
            }
        }

        private static void CountAdult(int mealPlanId, DateTime date, IEnumerable<PensionPrevisionDto> pension, ref MealPlan meal)
        {
            if ((int)MealPlanTypeEnum.Breakfast == mealPlanId || (int)MealPlanTypeEnum.Halfboardlunch == mealPlanId)
                meal.TotalAdult += pension.Count(e => (e.DateStart != date || (e.DateStart == date && e.DateEnd == date)) && e.Date.Date == date && !e.IsChild);
            else
                meal.TotalAdult += pension.Count(e => e.Date.Date == date && !e.IsChild);
        }

        private static void CountAdultCheckOut(DateTime date, IEnumerable<PensionPrevisionDto> pension, int MealPlanId, ref MealPlan meal)
           => meal.TotalAdult += pension.Where(e => !e.IsChild && e.DateEnd.Date == date).GroupBy(d => d.GuestReservationItemId).Count();

        private static void CountChild(int mealPlanId, DateTime date, IEnumerable<PensionPrevisionDto> pension, ChildrenRange[] rangeList, ref MealPlan meal)
        {
            foreach (var rangeItem in rangeList)
            {
                if (rangeItem.RangeCod == (int)ApplicationParameterEnum.ParameterChildren_1)
                {
                    if ((int)MealPlanTypeEnum.Breakfast == mealPlanId || (int)MealPlanTypeEnum.Halfboardlunch == mealPlanId)
                        meal.TotalChild += pension.Count(e => (e.DateStart != date || (e.DateStart == date && e.DateEnd == date)) && e.Date.Date == date && e.IsChild && rangeList.Length >= 1 && e.ChildAge >= rangeList[0].InitialRange && e.ChildAge <= rangeList[0].FinalRange);
                    else
                        meal.TotalChild += pension.Count(e => e.Date.Date == date && e.IsChild && rangeList.Length >= 1 && e.ChildAge >= rangeList[0].InitialRange && e.ChildAge <= rangeList[0].FinalRange);
                }

                else if (rangeItem.RangeCod == (int)ApplicationParameterEnum.ParameterChildren_2)
                {
                    if ((int)MealPlanTypeEnum.Breakfast == mealPlanId || (int)MealPlanTypeEnum.Halfboardlunch == mealPlanId)
                        meal.TotalChild2 += pension.Count(e => (e.DateStart != date || (e.DateStart == date && e.DateEnd == date)) && e.Date.Date == date && e.IsChild && rangeList.Length >= 2 && e.ChildAge >= rangeList[1].InitialRange && e.ChildAge <= rangeList[1].FinalRange);
                    else
                        meal.TotalChild2 += pension.Count(e => e.Date.Date == date && e.IsChild && rangeList.Length >= 2 && e.ChildAge >= rangeList[1].InitialRange && e.ChildAge <= rangeList[1].FinalRange);
                }

                else if (rangeItem.RangeCod == (int)ApplicationParameterEnum.ParameterChildren_3)
                {
                    if ((int)MealPlanTypeEnum.Breakfast == mealPlanId || (int)MealPlanTypeEnum.Halfboardlunch == mealPlanId)
                        meal.TotalChild3 += pension.Count(e => (e.DateStart != date || (e.DateStart == date && e.DateEnd == date)) && e.Date.Date == date && e.IsChild && rangeList.Length >= 3 && e.ChildAge >= rangeList[2].InitialRange && e.ChildAge <= rangeList[2].FinalRange);
                    else
                        meal.TotalChild3 += pension.Count(e => e.Date.Date == date && e.IsChild && rangeList.Length >= 3 && e.ChildAge >= rangeList[2].InitialRange && e.ChildAge <= rangeList[2].FinalRange);
                }
            }
        }
        private static void CountChildCheckOut(ChildrenRange[] rangeList, ref MealPlan meal, IEnumerable<PensionPrevisionDto> pension, DateTime date, int mealPlanId)
        {
            foreach (var rangeItem in rangeList)
            {
                if (rangeItem.RangeCod == (int)ApplicationParameterEnum.ParameterChildren_1)
                    meal.TotalChild += pension.Where(e => e.IsChild && e.DateEnd.Date == date && e.IsChild && rangeList.Length >= 1 && e.ChildAge >= rangeList[0].InitialRange && e.ChildAge <= rangeList[0].FinalRange).GroupBy(d => d.GuestReservationItemId).Count();
                else if (rangeItem.RangeCod == (int)ApplicationParameterEnum.ParameterChildren_2)
                    meal.TotalChild2 += pension.Where(e => e.IsChild && e.DateEnd.Date == date && e.IsChild && rangeList.Length >= 2 && e.ChildAge >= rangeList[1].InitialRange && e.ChildAge <= rangeList[1].FinalRange).GroupBy(d => d.GuestReservationItemId).Count();
                else if (rangeItem.RangeCod == (int)ApplicationParameterEnum.ParameterChildren_3)
                    meal.TotalChild3 += pension.Where(e => e.IsChild && e.DateEnd.Date == date && e.IsChild && rangeList.Length >= 3 && e.ChildAge >= rangeList[2].InitialRange && e.ChildAge <= rangeList[2].FinalRange).GroupBy(d => d.GuestReservationItemId).Count();
            }
        }

        private void ValidateFilters(GetFilters filters)
        {
            if (filters.DateStart.Date > filters.DateEnd.Date)
            {
                Notification.Raise(Notification.DefaultBuilder
                    .WithMessage(AppConsts.LocalizationSourceName, PensionPrevisionDto.EntityError.InvalidDate)
                    .WithDetailedMessage(AppConsts.LocalizationSourceName, PensionPrevisionDto.EntityError.InvalidDate)
                    .Build());
            }
        }

        private List<MealPlan> GeneratorListMealPlan(Task<IEnumerable<GetMealPlan>> allPension)
        {
            var listMealPlan = new List<MealPlan>();
            foreach (var mealPlan in allPension.Result)
            {
                listMealPlan.Add(new MealPlan
                {
                    MealPlanType = mealPlan.MealPlanId,
                    MealPlanTypeName = mealPlan.MealPlanName,
                    TotalAdult = 0,
                    TotalChild = 0,
                    TotalChild2 = 0,
                    TotalChild3 = 0,
                    TotalMealPlan = 0
                });
            }

            return listMealPlan;
        }       

    }
}
