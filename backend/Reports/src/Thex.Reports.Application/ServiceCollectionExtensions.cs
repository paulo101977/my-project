﻿using Thex.Reports.Application.Adapters;
using Thex.Reports.Application.Interfaces;
using Thex.Reports.Application.Services;
using Thex.Storage;

namespace Microsoft.Extensions.DependencyInjection
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddApplicationServiceDependency(this IServiceCollection services)
        {
            services.AddInfraDependency();

            // Registro dos serviços
            services.AddTransient<IUserAppService, UserAppService>();

            services.AddTransient<IUserAdapter, UserAdapter>();

            services.AddTransient<IPensionPrevisionAppService, PensionPrevisionAppService>();

            services.AddTransient<IBillingAccountTransferAppService, BillingAccountTransferAppService>();

            return services;
        }
    }
}