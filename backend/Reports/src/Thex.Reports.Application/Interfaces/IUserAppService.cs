﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.Reports.Dto;
using Thex.Reports.Infra.Entities;
using Tnf.Application.Services;

namespace Thex.Reports.Application.Interfaces
{
    public interface IUserAppService : IApplicationService
    {
        Task<List<UserDto>> GetAll();
        Task<UserDto> GetById();
    }
}
