﻿using System.Threading.Tasks;
using Thex.Reports.Dto.Dto.PensionPrevision;
using Tnf.Application.Services;
using Tnf.Dto;

namespace Thex.Reports.Application.Interfaces
{
    public interface IPensionPrevisionAppService : IApplicationService
    {
        Task<IListDto<PensionPrevisionDto>> GetRangeDateAsync(GetFilters filters);
    }
}
