﻿using System.Threading.Tasks;
using Thex.Reports.Dto.Dto.BillingAccountTransfer;
using Tnf.Dto;

namespace Thex.Reports.Application.Interfaces
{
    public interface IBillingAccountTransferAppService
    {
        Task<IListDto<BillingAccountTransferDto>> GetBillingAccountTransferAsync(GetFilters filters);
    }
}
