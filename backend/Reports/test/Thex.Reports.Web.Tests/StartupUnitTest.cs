﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using Thex.Common;
using Thex.Reports.Dto;
using Thex.Reports.Infra.Context;
using Thex.Reports.Infra.Entities;
using Thex.Kernel;
using Tnf.Configuration;
using Tnf.Dapper;
using Thex.Kernel.Dto;

namespace Thex.Reports.Web.Tests
{
    public class StartupUnitTest
    {
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            services
                .AddApplicationServiceDependency()
                .AddInfraDependency()
                .AddTnfDapper(options =>
                {
                    options.DbType = DapperDbType.Sqlite;
                })
                .AddTnfAspNetCoreSetupTest()        // Configura o setup de teste para AspNetCore
                .AddTnfEfCoreSqliteInMemory()       // Configura o setup de teste para EntityFrameworkCore em memória
                .RegisterDbContextToSqliteInMemory<ThexReportsContext>();    // Configura o cotexto a ser usado em memória pelo EntityFrameworkCore
            
            services.AddScoped<IApplicationUser>(a => new ApplicationUserMock());
            var serviceProvider = services.BuildServiceProvider();

            ServiceLocator.SetLocatorProvider(serviceProvider);

            return services.BuildServiceProvider();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app)
        {
            // Configura o uso do teste
            app.UseTnfAspNetCoreSetupTest(options =>
            {
                options.Repository(repositoryConfig =>
                {
                    repositoryConfig.Entity<IEntityGuid>(entity =>
                        entity.RequestDto<IDefaultGuidRequestDto>((e, d) => e.Id == d.Id));

                    repositoryConfig.Entity<IEntityInt>(entity =>
                        entity.RequestDto<IDefaultIntRequestDto>((e, d) => e.Id == d.Id));

                    repositoryConfig.Entity<IEntityLong>(entity =>
                        entity.RequestDto<IDefaultLongRequestDto>((e, d) => e.Id == d.Id));

                });
            });

            // Habilita o uso do UnitOfWork em todo o request
            app.UseTnfUnitOfWork();

            app.UseMvc(routes =>
            {
                routes.MapRoute("default", "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }

    public class ApplicationUserMock : IApplicationUser
    {
        public string Name => "Teste";

        public string UserEmail => "teste@email.com";

        public Guid UserUid => new Guid("215bc8a0-07db-4428-9955-fa53b58b4ded");

        public string UserName => "Teste";

        public Guid TenantId => new Guid("9bece27c-cf67-4ea3-8ae8-357b3080475d");

        public string PropertyUid => "e1a9185e-5e1b-49bd-a17e-0d18cd15eaf0";

        public string PropertyId => "1";

        public string ChainId => "1";

        public bool IsAdmin => false;

        public string PreferredLanguage => "pt-br";

        public string PreferredCulture => "pt-br";

        public string PropertyLanguage => "pt-br";

        public string PropertyCulture => "pt-br";

        public string TimeZoneName => "America/Sao_Paulo";

        public string PropertyCountryCode => "BR";

        public string PropertyDistrictCode => "RJ";

        public List<LoggedUserPropertyDto> PropertyList => throw new NotImplementedException();

        string IApplicationUser.UserName { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

        public Dictionary<string, string> GetClaimsIdentity()
        {
            var claims = new Dictionary<string, string>();
            claims.Add("admin", "true");
            return claims;
        }

        public bool IsAuthenticated()
        {
            return true;
        }

        public void SetProperties(string userUid, string tenantId, string propertyId, string timeZoneName)
        {
            throw new NotImplementedException();
        }
    }
}
