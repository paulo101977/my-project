﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using System;
using Thex.Common;
using Thex.Reports.Application.Interfaces;
using Thex.Reports.Web.Tests.Mocks.AppServiceMock;
using Thex.Kernel;

namespace Thex.Reports.Web.Tests
{
    public class StartupControllerTest
    {
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            services.AddTnfAspNetCoreSetupTest();
            services.AddTransient<IUserAppService, UserAppServiceMock>();
            services.AddTransient<IPensionPrevisionAppService, PensionPrevisionAppServiceMock>();
            services.AddTransient<IBillingAccountTransferAppService, BillingAccountTransferAppServiceMock>();

            services.AddScoped<IApplicationUser>(a => new ApplicationUserMock());
            var serviceProvider = services.BuildServiceProvider();

            ServiceLocator.SetLocatorProvider(serviceProvider);

            return services.BuildServiceProvider();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app)
        {
            // Configura o uso do teste
            app.UseTnfAspNetCoreSetupTest();

            app.UseMvc(routes =>
            {
                routes.MapRoute("default", "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
