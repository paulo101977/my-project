﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.Reports.Dto;

namespace Thex.Reports.Web.Tests.Mocks.Dto
{
    public class UserDtoMock
    {
        public static UserDto GetDto(Guid userId)
        {
            return new UserDto()
            {
                Id = userId,
                UserName = "User " + DateTime.Now                
            };
        }

        public static async Task<UserDto> GetDtoAsync()
        {
            var dto = GetDto(new Guid());
            return await Task.FromResult(dto);
        }

        public static Task<IList<UserDto>> GetDtoListAsync(Guid userId)
        {
            var ret = new List<UserDto>();

            ret.Add(GetDto(userId));
            ret.Add(GetDto(userId));
            ret.Add(GetDto(userId));

            return Task.FromResult<IList<UserDto>>(ret);
        }
    }
}
