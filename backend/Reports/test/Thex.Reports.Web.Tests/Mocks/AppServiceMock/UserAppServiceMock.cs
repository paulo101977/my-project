﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Thex.Reports.Application.Interfaces;
using Thex.Reports.Dto;
using Thex.Reports.Web.Tests.Mocks.Dto;

namespace Thex.Reports.Web.Tests.Mocks.AppServiceMock
{
    public class UserAppServiceMock : IUserAppService
    {
        public async Task<List<UserDto>> GetAll()
        {
            return (await UserDtoMock.GetDtoListAsync(Guid.NewGuid())).MapTo<List<UserDto>>();
        }

        public async Task<UserDto> GetById()
        {
            return (await UserDtoMock.GetDtoListAsync(Guid.NewGuid())).FirstOrDefault().MapTo<UserDto>();
        }
    }
}
