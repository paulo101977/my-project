﻿using System;
using System.Collections.Generic;
using Thex.Reports.Dto.Dto.BillingAccountTransfer;

namespace Thex.Reports.Web.Tests.Mocks.AppServiceMock
{
    public class BillingAccountTransferMock
    {
        public static IList<BillingAccountTransferDto> MockBillingAccountTransfer()
        {
            var billingAccountTransferListDto = new List<BillingAccountTransferDto>();            
            
            var billingAccountTransfer = new BillingAccountTransferDto
            {
                 BillingAccountItemId = Guid.NewGuid(),
                 BillingItemName = "Teste",
                 BillingAccountOriginal = "Teste Original",
                 BillingAccountSource = "Teste Origem",
                 BillingAccountDestination = "Teste Destino",
                 BillingAccountItemTypeId = 2,
                 BillingAccountItemType = "Débito",
                 Amount = 100,
                 CreationTime = DateTime.UtcNow,
                 UserName = "Teste usuário",
                 ReservationItemCodeSource = "AUSHASH-0001",
                 ReservationItemCodeDestination = "QUWIQUR-0001"
            };

            billingAccountTransferListDto.Add(billingAccountTransfer);            
            
            return billingAccountTransferListDto;
        }

        public static IList<OriginalAccountDto> MockOriginalAccount()
        {
            var originalAccountListDto = new List<OriginalAccountDto>();

            var originalAccountList = new OriginalAccountDto
            {
                BillingAccountItemId = Guid.NewGuid(),
                OriginalAccount = "Teste Original"
            };

            originalAccountListDto.Add(originalAccountList);

            return originalAccountListDto;
        }
    }
}
