﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.Reports.Application.Interfaces;
using Thex.Reports.Dto.Dto.PensionPrevision;
using Tnf.Dto;

namespace Thex.Reports.Web.Tests.Mocks.AppServiceMock
{
    public class PensionPrevisionAppServiceMock : IPensionPrevisionAppService
    {
        public async Task<IListDto<PensionPrevisionDto>> GetRangeDateAsync(GetFilters filters)
        {
            return new ListDto<PensionPrevisionDto>
            {
                HasNext = false,
                Items = new List<PensionPrevisionDto>()
            };
        }
    }
}
