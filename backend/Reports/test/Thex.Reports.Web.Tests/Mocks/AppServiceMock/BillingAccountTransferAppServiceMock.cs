﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.Reports.Application.Interfaces;
using Thex.Reports.Dto.Dto.BillingAccountTransfer;
using Tnf.Dto;

namespace Thex.Reports.Web.Tests.Mocks.AppServiceMock
{
    public class BillingAccountTransferAppServiceMock : IBillingAccountTransferAppService
    { 
        public async Task<IListDto<BillingAccountTransferDto>> GetBillingAccountTransferAsync(GetFilters filters)
        {
            return new ListDto<BillingAccountTransferDto>
            {
                HasNext = false,
                Items = new List<BillingAccountTransferDto>()
            };
        }    
    }
}
