﻿using System;
using System.Collections.Generic;
using Thex.Common.Enumerations;
using Thex.Reports.Dto.Dto.PensionPrevision;

namespace Thex.Reports.Web.Tests.Mocks.AppServiceMock
{
    public class PensionPrevisionMock
    {
        public static IList<PensionPrevisionDto> MochPensionPrevision()
        {
            var listPension = new List<PensionPrevisionDto>();

            var reservationItem = 1;

            foreach (var range in GetChildrenRange())
            {
                foreach (var item in AllPension())
                {
                    var pension = new PensionPrevisionDto
                    {
                        Date = DateTime.Now.Date,
                        DateStart = DateTime.Now.Date,
                        DateEnd = DateTime.Now.Date.AddDays(2),
                        ChildAge = range.FinalRange,
                        GuestReservationItemId = reservationItem,
                        IsChild = ((item.MealPlanId == (int)MealPlanTypeEnum.Breakfast) || (item.MealPlanId == (int)MealPlanTypeEnum.Halfboard)) ? true : false,
                        MealPlanTypeId = item.MealPlanId,
                        ReservationStatus = (int)ReservationStatus.Confirmed
                    };

                    listPension.Add(pension);
                }

                foreach (var item in AllPension())
                {
                    var pension = new PensionPrevisionDto
                    {
                        Date = DateTime.Now.Date.AddDays(1),
                        DateStart = DateTime.Now.Date,
                        DateEnd = DateTime.Now.Date.AddDays(2),
                        ChildAge = range.FinalRange,
                        GuestReservationItemId = reservationItem,
                        IsChild = ((item.MealPlanId == (int)MealPlanTypeEnum.Breakfast) || (item.MealPlanId == (int)MealPlanTypeEnum.Halfboard)) ? true : false,
                        MealPlanTypeId = item.MealPlanId,
                        ReservationStatus = (int)ReservationStatus.Checkin
                    };

                    listPension.Add(pension);
                }

                reservationItem++;

            }
            return listPension;
        }

        public static IEnumerable<ChildrenRange> GetChildrenRange()
        {
            return new List<ChildrenRange>
            {
                new ChildrenRange
                {
                    InitialRange = 0,
                    FinalRange = 2,
                    RangeCod = 7
                },

                new ChildrenRange
                {
                    InitialRange = 0,
                    FinalRange = 5,
                    RangeCod = 8
                },
                new ChildrenRange
                {
                    InitialRange = 0,
                    FinalRange = 6,
                    RangeCod = 9
                }
            };
        }

        public static IEnumerable<GetMealPlan> AllPension()
        {

            return new List<GetMealPlan>
            {
                new GetMealPlan
                {
                    MealPlanId = 1,
                    MealPlanName = "Sem Pensao"

                },

                new GetMealPlan
                {
                    MealPlanId = 2,
                    MealPlanName = "Café da Manha"

                },

                new GetMealPlan
                {
                    MealPlanId = 3,
                    MealPlanName = "Meia Pensão"

                },

                new GetMealPlan
                {
                    MealPlanId = 4,
                    MealPlanName = "Meia Pensão Almoço"

                },

                new GetMealPlan
                {
                    MealPlanId = 5,
                    MealPlanName = "Meia Pensão Jantar"

                },

                new GetMealPlan
                {
                    MealPlanId = 6,
                    MealPlanName = "Pensão Completa"

                },

                new GetMealPlan
                {
                    MealPlanId = 7,
                    MealPlanName = "Tudo Incluso"

                },
            };

        }

        public static bool ValidDto(IEnumerable<PensionPrevisionDto> dto)
        {
            var valid = true;
            foreach (var item in dto)
            {
                if (item.Date.Date == DateTime.Now.Date)
                {
                    if (item.TotalOccupied != 0)
                        valid = false;

                    if (item.TotalCheckIn != 0)
                        valid = false;

                    if (item.TotalCheckOut != 0)
                        valid = false;

                    if (item.TotalAdult != 9)
                        valid = false;

                    if (item.TotalChild != 1)
                        valid = false;

                    if (item.TotalChild2 != 1)
                        valid = false;

                    if (item.TotalChild3 != 1)
                        valid = false;

                    foreach (var meal in item.MealPlan)
                    {
                        if (meal.MealPlanType == (int)MealPlanTypeEnum.Breakfast || meal.MealPlanType == (int)MealPlanTypeEnum.Halfboardlunch)
                        {
                            if (meal.TotalAdult != 0)
                                valid = false;

                            if (meal.TotalChild != 0)
                                valid = false;

                            if (meal.TotalChild2 != 0)
                                valid = false;

                            if (meal.TotalChild3 != 0)
                                valid = false;

                            if (meal.TotalMealPlan != 0)
                                valid = false;
                        }
                        else if (meal.MealPlanType == (int)MealPlanTypeEnum.Halfboard)
                        {
                            if (meal.TotalAdult != 0)
                                valid = false;

                            if (meal.TotalChild != 1)
                                valid = false;

                            if (meal.TotalChild2 != 1)
                                valid = false;

                            if (meal.TotalChild3 != 1)
                                valid = false;

                            if (meal.TotalMealPlan != 3)
                                valid = false;
                        }
                        else if (meal.MealPlanType == (int)MealPlanTypeEnum.Halfboardlunchdinner || meal.MealPlanType == (int)MealPlanTypeEnum.Fullboard || meal.MealPlanType == (int)MealPlanTypeEnum.Allinclusive)
                        {
                            if (meal.TotalAdult != 3)
                                valid = false;

                            if (meal.TotalChild != 0)
                                valid = false;

                            if (meal.TotalChild2 != 0)
                                valid = false;

                            if (meal.TotalChild3 != 0)
                                valid = false;

                            if (meal.TotalMealPlan != 3)
                                valid = false;
                        }
                    }
                }
                else if (item.Date.Date == DateTime.Now.Date.AddDays(1))
                {
                    if (item.TotalOccupied != 0)
                        valid = false;

                    if (item.TotalCheckIn != 0)
                        valid = false;

                    if (item.TotalCheckOut != 0)
                        valid = false;

                    if (item.TotalAdult != 24)
                        valid = false;

                    if (item.TotalChild != 3)
                        valid = false;

                    if (item.TotalChild2 != 3)
                        valid = false;

                    if (item.TotalChild3 != 3)
                        valid = false;

                    foreach (var meal in item.MealPlan)
                    {
                        if (meal.MealPlanType == (int)MealPlanTypeEnum.None)
                        {
                            if (meal.TotalAdult != 3)
                                valid = false;

                            if (meal.TotalChild != 0)
                                valid = false;

                            if (meal.TotalChild2 != 0)
                                valid = false;

                            if (meal.TotalChild3 != 0)
                                valid = false;

                            if (meal.TotalMealPlan != 3)
                                valid = false;
                        }
                        else if (meal.MealPlanType == (int)MealPlanTypeEnum.Breakfast)
                        {
                            if (meal.TotalAdult != 12)
                                valid = false;

                            if (meal.TotalChild != 2)
                                valid = false;

                            if (meal.TotalChild2 != 2)
                                valid = false;

                            if (meal.TotalChild3 != 2)
                                valid = false;

                            if (meal.TotalMealPlan != 18)
                                valid = false;
                        }
                        else if (meal.MealPlanType == (int)MealPlanTypeEnum.Halfboard)
                        {
                            if (meal.TotalAdult != 0)
                                valid = false;

                            if (meal.TotalChild != 1)
                                valid = false;

                            if (meal.TotalChild2 != 1)
                                valid = false;

                            if (meal.TotalChild3 != 1)
                                valid = false;

                            if (meal.TotalMealPlan != 3)
                                valid = false;
                        }
                        else if (meal.MealPlanType == (int)MealPlanTypeEnum.Halfboardlunch || meal.MealPlanType == (int)MealPlanTypeEnum.Halfboardlunchdinner)
                        {
                            if (meal.TotalAdult != 3)
                                valid = false;

                            if (meal.TotalChild != 0)
                                valid = false;

                            if (meal.TotalChild2 != 0)
                                valid = false;

                            if (meal.TotalChild3 != 0)
                                valid = false;

                            if (meal.TotalMealPlan != 3)
                                valid = false;
                        }
                        else if (meal.MealPlanType == (int)MealPlanTypeEnum.Fullboard || meal.MealPlanType == (int)MealPlanTypeEnum.Allinclusive)
                        {
                            if (meal.TotalAdult != 3)
                                valid = false;

                            if (meal.TotalChild != 0)
                                valid = false;

                            if (meal.TotalChild2 != 0)
                                valid = false;

                            if (meal.TotalChild3 != 0)
                                valid = false;

                            if (meal.TotalMealPlan != 3)
                                valid = false;
                        }
                    }
                }
                else if (item.Date.Date == DateTime.Now.Date.AddDays(2))
                {
                    if (item.TotalOccupied != 0)
                        valid = false;

                    if (item.TotalCheckIn != 0)
                        valid = false;

                    if (item.TotalCheckOut != 0)
                        valid = false;

                    if (item.TotalAdult != 21)
                        valid = false;

                    if (item.TotalChild != 2)
                        valid = false;

                    if (item.TotalChild2 != 2)
                        valid = false;

                    if (item.TotalChild3 != 2)
                        valid = false;

                    foreach (var meal in item.MealPlan)
                    {
                        if (meal.MealPlanType == (int)MealPlanTypeEnum.None)
                        {
                            if (meal.TotalAdult != 0)
                                valid = false;

                            if (meal.TotalChild != 0)
                                valid = false;

                            if (meal.TotalChild2 != 0)
                                valid = false;

                            if (meal.TotalChild3 != 0)
                                valid = false;

                            if (meal.TotalMealPlan != 0)
                                valid = false;
                        }
                        else if (meal.MealPlanType == (int)MealPlanTypeEnum.Breakfast)
                        {
                            if (meal.TotalAdult != 12)
                                valid = false;

                            if (meal.TotalChild != 2)
                                valid = false;

                            if (meal.TotalChild2 != 2)
                                valid = false;

                            if (meal.TotalChild3 != 2)
                                valid = false;

                            if (meal.TotalMealPlan != 18)
                                valid = false;
                        }
                        else if (meal.MealPlanType == (int)MealPlanTypeEnum.Halfboard || meal.MealPlanType == (int)MealPlanTypeEnum.Halfboardlunchdinner)
                        {
                            if (meal.TotalAdult != 0)
                                valid = false;

                            if (meal.TotalChild != 0)
                                valid = false;

                            if (meal.TotalChild2 != 0)
                                valid = false;

                            if (meal.TotalChild3 != 0)
                                valid = false;

                            if (meal.TotalMealPlan != 0)
                                valid = false;
                        }
                        else if (meal.MealPlanType == (int)MealPlanTypeEnum.Halfboardlunch)
                        {
                            if (meal.TotalAdult != 3)
                                valid = false;

                            if (meal.TotalChild != 0)
                                valid = false;

                            if (meal.TotalChild2 != 0)
                                valid = false;

                            if (meal.TotalChild3 != 0)
                                valid = false;

                            if (meal.TotalMealPlan != 3)
                                valid = false;
                        }
                        else if (meal.MealPlanType == (int)MealPlanTypeEnum.Fullboard || meal.MealPlanType == (int)MealPlanTypeEnum.Allinclusive)
                        {
                            if (meal.TotalAdult != 3)
                                valid = false;

                            if (meal.TotalChild != 0)
                                valid = false;

                            if (meal.TotalChild2 != 0)
                                valid = false;

                            if (meal.TotalChild3 != 0)
                                valid = false;

                            if (meal.TotalMealPlan != 3)
                                valid = false;
                        }
                    }
                }
            }

            return valid;
        }
    }
}
