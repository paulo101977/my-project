﻿
using Shouldly;
using Thex.Reports.Application.Interfaces;
using Thex.Reports.Web.Controllers;
using Tnf.AspNetCore.TestBase;
using Xunit;
using Microsoft.Extensions.DependencyInjection;
using System.Threading.Tasks;
using Tnf.Dto;
using Thex.Reports.Dto.Dto.PensionPrevision;

namespace Thex.Reports.Web.Tests.Controllers
{
    public class PensionPrevisionControllerTests : TnfAspNetCoreIntegratedTestBase<StartupControllerTest>
    {
        [Fact]
        public void Should_Resolve_All()
        {
            TnfSession.ShouldNotBeNull();
            ServiceProvider.GetService<PensionPrevisionController>().ShouldNotBeNull();
            ServiceProvider.GetService<IPensionPrevisionAppService>().ShouldNotBeNull();
        }

        [Fact]
        public async Task Should_GetAll()
        {
            var response = await GetResponseAsObjectAsync<ListDto<PensionPrevisionDto>>(RouteConsts.PensionPrevisionRouteName);

            Assert.NotNull(response);
        }
    }
}
