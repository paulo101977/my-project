﻿
using Shouldly;
using Thex.Reports.Application.Interfaces;
using Thex.Reports.Web.Controllers;
using Tnf.AspNetCore.TestBase;
using Xunit;
using Microsoft.Extensions.DependencyInjection;
using System.Threading.Tasks;
using Tnf.Dto;
using Thex.Reports.Dto.Dto.BillingAccountTransfer;

namespace Thex.Reports.Web.Tests.Controllers
{
    public class BillingAccountTransferControllerTests : TnfAspNetCoreIntegratedTestBase<StartupControllerTest>
    {
        [Fact]
        public void Should_Resolve_All()
        {
            TnfSession.ShouldNotBeNull();
            ServiceProvider.GetService<BillingAccountTransferController>().ShouldNotBeNull();
            ServiceProvider.GetService<IBillingAccountTransferAppService>().ShouldNotBeNull();
        }

        [Fact]
        public async Task Should_GetAll()
        {
            var response = await GetResponseAsObjectAsync<ListDto<BillingAccountTransferDto>>(RouteConsts.BillingAccountTransferRouteName);

            Assert.NotNull(response);
        }
    }
}
