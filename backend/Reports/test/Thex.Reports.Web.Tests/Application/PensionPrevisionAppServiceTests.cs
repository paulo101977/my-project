﻿using Microsoft.Extensions.DependencyInjection;
using NSubstitute;
using Shouldly;
using System;
using System.Linq;
using Thex.Kernel;
using Thex.Reports.Application.Interfaces;
using Thex.Reports.Application.Services;
using Thex.Reports.Dto.Dto.PensionPrevision;
using Thex.Reports.Infra.Interfaces.ReadRepositories;
using Thex.Reports.Web.Tests.Mocks.AppServiceMock;
using Tnf.AspNetCore.TestBase;
using Tnf.Notifications;
using Xunit;

namespace Thex.Reports.Web.Tests.Application
{
    public class PensionPrevisionAppServiceTests : TnfAspNetCoreIntegratedTestBase<StartupUnitTest>
    {
        public INotificationHandler notificationHandler;

        public PensionPrevisionAppServiceTests()
        {
            notificationHandler = new NotificationHandler(ServiceProvider);
        }

        [Fact]
        public void Should_Resolve_All()
        {
            TnfSession.ShouldNotBeNull();
            ServiceProvider.GetService<INotificationHandler>().ShouldNotBeNull();
            ServiceProvider.GetService<IPensionPrevisionReadRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<IApplicationUser>().ShouldNotBeNull();
            ServiceProvider.GetService<IPensionPrevisionAppService>().ShouldNotBeNull();
        }

        [Fact]
        public void Should_Get_Pension_Prevision()
        {
            var pensionPrevisionRead = Substitute.For<IPensionPrevisionReadRepository>();
            var applicaitonUser = Substitute.For<ApplicationUserMock>();

            var filters = new GetFilters
            {
                DateStart = DateTime.Now.Date,
                DateEnd = DateTime.Now.Date.AddDays(2)
            };


            pensionPrevisionRead.GetRangeDate(filters).ReturnsForAnyArgs(x =>
            {
                return PensionPrevisionMock.MochPensionPrevision();
            });


            pensionPrevisionRead.AllPension().ReturnsForAnyArgs(x =>
            {
                return PensionPrevisionMock.AllPension();
            });

            pensionPrevisionRead.GetChildrenRange().ReturnsForAnyArgs(x =>
            {
                return PensionPrevisionMock.GetChildrenRange();

            });

            var dto = (new PensionPrevisionAppService(pensionPrevisionRead, notificationHandler, applicaitonUser)
                                                    .GetRangeDateAsync(filters)).Result.Items.OrderBy(x => x.Date);

            Assert.True(PensionPrevisionMock.ValidDto(dto));

        }


    }
}
