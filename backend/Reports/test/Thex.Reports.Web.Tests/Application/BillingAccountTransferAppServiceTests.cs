﻿using NSubstitute;
using Shouldly;
using System;
using System.Collections.Generic;
using Thex.Kernel;
using Thex.Reports.Application.Interfaces;
using Thex.Reports.Application.Services;
using Thex.Reports.Dto.Dto.BillingAccountTransfer;
using Thex.Reports.Infra.Interfaces.ReadRepositories;
using Thex.Reports.Web.Tests.Mocks.AppServiceMock;
using Tnf.AspNetCore.TestBase;
using Tnf.Notifications;
using Xunit;
using Microsoft.Extensions.DependencyInjection;
using Tnf.Localization;

namespace Thex.Reports.Web.Tests.Application
{
    public class BillingAccountTransferAppServiceTests : TnfAspNetCoreIntegratedTestBase<StartupUnitTest>
    {
        public INotificationHandler notificationHandler;

        public BillingAccountTransferAppServiceTests()
        {
            notificationHandler = new NotificationHandler(ServiceProvider);
        }

        [Fact]
        public void Should_Resolve_All()
        {
            TnfSession.ShouldNotBeNull();
            ServiceProvider.GetService<INotificationHandler>().ShouldNotBeNull();
            ServiceProvider.GetService<IBillingAccountTransferReadRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<IApplicationUser>().ShouldNotBeNull();
            ServiceProvider.GetService<IBillingAccountTransferAppService>().ShouldNotBeNull();
            ServiceProvider.GetService<ILocalizationManager>().ShouldNotBeNull();
        }

        [Fact]
        public void Should_Get_Pension_Prevision()
        {
            var billingAccountTransferRead = Substitute.For<IBillingAccountTransferReadRepository>();
            var localizationManager = Substitute.For<ILocalizationManager>();

            var filters = new GetFilters
            {
                DateStart = DateTime.Now.Date,
                DateEnd = DateTime.Now.Date.AddDays(2)
            };

            var billingAccountItemIdList = new List<Guid>() { Guid.NewGuid() };

            billingAccountTransferRead.GetBillingAccountTransferAsync(filters).ReturnsForAnyArgs(x =>
            {
                return BillingAccountTransferMock.MockBillingAccountTransfer();
            });

            billingAccountTransferRead.GetOriginalAccountByBillingAccountItemIdAsync(billingAccountItemIdList).ReturnsForAnyArgs(x =>
            {
                return BillingAccountTransferMock.MockOriginalAccount();
            });

            var dto = (new BillingAccountTransferAppService(notificationHandler, localizationManager, billingAccountTransferRead)
                                                    .GetBillingAccountTransferAsync(filters)).Result.Items;

            Assert.False(notificationHandler.HasNotification());

        }
    }
}
