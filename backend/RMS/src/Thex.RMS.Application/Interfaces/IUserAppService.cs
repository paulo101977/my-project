﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.RMS.Dto;
using Thex.RMS.Infra.Entities;
using Tnf.Application.Services;

namespace Thex.RMS.Application.Interfaces
{
    public interface IUserAppService : IApplicationService
    {
        Task<List<UserDto>> GetAll();

        Task<UserDto> SendPhoto(Guid id, UserDto dto);
        Task<UserDto> UpdateAsync(Guid id, UserDto dto);
    }
}
