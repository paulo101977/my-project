﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.RMS.Dto;
using Thex.RMS.Infra.Entities;
using Tnf.Application.Services;

namespace Thex.RMS.Application.Interfaces
{
    public interface IPropertyPremiseAppService : IApplicationService
    {
        Task<PropertyPremiseDto> GetById(Guid id);
        Task<List<PropertyPremiseDto>> GetAll();

        Task<PropertyPremiseDto> InsertAsync(PropertyPremiseDto dto);
        Task<PropertyPremiseDto> UpdateAsync(Guid id, PropertyPremiseDto dto);
        Task<PropertyPremiseDto> RemoveAsync(Guid id);
    }
}
