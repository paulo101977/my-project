﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.RMS.Dto;
using Thex.RMS.Infra.Entities;
using Tnf.Application.Services;

namespace Thex.RMS.Application.Interfaces
{
    public interface IPropertyPremiseHeaderAppService : IApplicationService
    {
        Task<PropertyPremiseHeaderDto> GetById(Guid id);
        Task<PropertyPremiseHeaderSelectDto> GetAll(GetAllPropertyPremiseHeaderDto request);

        Task<PropertyPremiseHeaderDto> InsertAsync(PropertyPremiseHeaderDto dto);
        Task<PropertyPremiseHeaderDto> UpdateAsync(Guid id, PropertyPremiseHeaderDto dto);
        Task<PropertyPremiseHeaderDto> ToggleAsync(Guid id);
        Task<PropertyPremiseHeaderDto> RemoveAsync(Guid id);
    }
}
