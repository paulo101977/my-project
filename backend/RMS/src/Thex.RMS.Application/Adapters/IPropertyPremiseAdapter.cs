﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a Crudzilla tool.
// </auto-generated>
//------------------------------------------------------------------------------

using Thex.RMS.Infra.Entities;
using Thex.RMS.Dto;

namespace Thex.RMS.Application.Adapters
{
    public interface IPropertyPremiseAdapter
    {
        PropertyPremise.Builder Map(PropertyPremise entity, PropertyPremiseDto dto);
        PropertyPremise.Builder Map(PropertyPremiseDto dto);
    }
}
