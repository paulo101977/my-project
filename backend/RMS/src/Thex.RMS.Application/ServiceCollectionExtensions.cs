﻿using Thex.RMS.Application.Adapters;
using Thex.RMS.Application.Interfaces;
using Thex.RMS.Application.Services;

namespace Microsoft.Extensions.DependencyInjection
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddApplicationServiceDependency(this IServiceCollection services)
        {
            services.AddInfraDependency();

            // Registro dos serviços
            services.AddTransient<IUserAppService, UserAppService>();
            services.AddTransient<IPropertyPremiseAppService, PropertyPremiseAppService>();
            services.AddTransient<IPropertyPremiseHeaderAppService, PropertyPremiseHeaderAppService>();

            services.AddTransient<IUserAdapter, UserAdapter>();
            services.AddTransient<IPropertyPremiseAdapter, PropertyPremiseAdapter>();
            services.AddTransient<IPropertyPremiseHeaderAdapter, PropertyPremiseHeaderAdapter>();

            return services;
        }
    }
}