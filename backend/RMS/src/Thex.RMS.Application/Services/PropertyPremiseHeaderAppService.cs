﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.RMS.Application.Adapters;
using Thex.RMS.Application.Interfaces;
using Thex.RMS.Dto;
using Thex.RMS.Infra.Interfaces;
using Thex.RMS.Infra.Interfaces.ReadRepositories;
using Thex.Kernel;
using Tnf.Application.Services;
using Tnf.Notifications;
using Tnf.Repositories.Uow;

namespace Thex.RMS.Application.Services
{
    public class PropertyPremiseHeaderAppService : ApplicationService, IPropertyPremiseHeaderAppService
    {
        private readonly IPropertyPremiseHeaderReadRepository _PropertyPremiseHeaderReadRepository;
        private readonly IPropertyPremiseHeaderRepository _PropertyPremiseHeaderRepository;
        private readonly IPropertyPremiseRepository _PropertyPremiseRepository;
        private readonly IPropertyPremiseHeaderAdapter _PropertyPremiseHeaderAdapter;
        private readonly IPropertyPremiseAdapter _PropertyPremiseAdapter;

        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IApplicationUser _applicationUser;

        public PropertyPremiseHeaderAppService(
            IPropertyPremiseHeaderReadRepository PropertyPremiseHeaderReadRepository,
            IPropertyPremiseHeaderRepository PropertyPremiseHeaderRepository,
            IPropertyPremiseRepository PropertyPremiseRepository,
            IPropertyPremiseHeaderAdapter PropertyPremiseHeaderAdapter,
            IPropertyPremiseAdapter PropertyPremiseAdapter,
            IApplicationUser applicationUser,
            IUnitOfWorkManager unitOfWorkManager,
            INotificationHandler notificationHandler)
            : base(notificationHandler)
        {
            _PropertyPremiseHeaderReadRepository = PropertyPremiseHeaderReadRepository;
            _PropertyPremiseHeaderRepository = PropertyPremiseHeaderRepository;
            _PropertyPremiseRepository = PropertyPremiseRepository;
            _PropertyPremiseAdapter = PropertyPremiseAdapter;
            _PropertyPremiseHeaderAdapter = PropertyPremiseHeaderAdapter;
            _applicationUser = applicationUser;

            _unitOfWorkManager = unitOfWorkManager;
        }

        public virtual async Task<PropertyPremiseHeaderSelectDto> GetAll(GetAllPropertyPremiseHeaderDto request)
        {
            request.StartDate = request.StartDate ?? DateTime.UtcNow.ToZonedDateTimeLoggedUser(_applicationUser).AddDays(-100);
            request.EndDate = request.EndDate ?? DateTime.UtcNow.ToZonedDateTimeLoggedUser(_applicationUser);
            request.PropertyId = int.Parse(_applicationUser.PropertyId);

            return await _PropertyPremiseHeaderReadRepository.GetAll(request);
        }

        public virtual async Task<PropertyPremiseHeaderDto> GetById(Guid id)
        {
            return await _PropertyPremiseHeaderReadRepository.GetById(id);
        }

        public async Task<PropertyPremiseHeaderDto> InsertAsync(PropertyPremiseHeaderDto dto)
        {
            using (var uow = _unitOfWorkManager.Begin())
            {
                dto.Id = Guid.NewGuid();
                dto.CreationTime = DateTime.Now;
                dto.CreatorUserId = _applicationUser.UserUid;
                dto.PropertyId = int.Parse(_applicationUser.PropertyId);
                dto.TenantId = _applicationUser.TenantId;

                var local = _PropertyPremiseHeaderAdapter.Map(dto).Build();

                if (Notification.HasNotification())
                    return null;

                await _PropertyPremiseHeaderRepository.InsertItem(local);

                if (Notification.HasNotification())
                    return null;

                foreach (var item in dto.PropertyPremiseList)
                {
                    item.Id = Guid.NewGuid();
                    item.CreationTime = DateTime.Now;
                    item.CreatorUserId = _applicationUser.UserUid;
                    item.PropertyPremiseHeaderId = dto.Id;
                    item.PropertyId = dto.PropertyId;
                    item.TenantId = dto.TenantId;

                    var localPremise = _PropertyPremiseAdapter.Map(item).Build();

                    if (Notification.HasNotification())
                        return null;

                    await _PropertyPremiseRepository.InsertItem(localPremise);

                    if (Notification.HasNotification())
                        return null;

                }

                uow.Complete();
            }

            return dto;
        }

        public async Task<PropertyPremiseHeaderDto> UpdateAsync(Guid id, PropertyPremiseHeaderDto dto)
        {
            PropertyPremiseHeaderDto propertyPremiseHeaderDto = null;

            using (var uow = _unitOfWorkManager.Begin())
            {
                propertyPremiseHeaderDto = await _PropertyPremiseHeaderReadRepository.GetById(id);
                propertyPremiseHeaderDto.Id = id;
                propertyPremiseHeaderDto.LastModifierUserId = _applicationUser.UserUid;
                propertyPremiseHeaderDto.LastModificationTime = DateTime.Now;
                propertyPremiseHeaderDto.PropertyId = int.Parse(_applicationUser.PropertyId);
                propertyPremiseHeaderDto.TenantId = _applicationUser.TenantId;

                var local = _PropertyPremiseHeaderAdapter.Map(propertyPremiseHeaderDto).Build();
                var propertyPremiseHeader = _PropertyPremiseHeaderAdapter.Map(local, dto).Build();

                if (Notification.HasNotification())
                    return null;

                await _PropertyPremiseHeaderRepository.UpdateItem(id, propertyPremiseHeader);

                if (Notification.HasNotification())
                    return null;

                foreach (var item in dto.PropertyPremiseList)
                {
                    item.Id = Guid.NewGuid();
                    item.CreationTime = DateTime.Now;
                    item.CreatorUserId = _applicationUser.UserUid;
                    item.PropertyPremiseHeaderId = propertyPremiseHeaderDto.Id;
                    item.PropertyId = propertyPremiseHeaderDto.PropertyId;
                    item.TenantId = propertyPremiseHeaderDto.TenantId;

                    var localPremise = _PropertyPremiseAdapter.Map(item).Build();

                    if (Notification.HasNotification())
                        return null;

                    await _PropertyPremiseRepository.InsertItem(localPremise);

                    if (Notification.HasNotification())
                        return null;

                }

                uow.Complete();
            }

            return propertyPremiseHeaderDto;
        }
        public async Task<PropertyPremiseHeaderDto> ToggleAsync(Guid id)
        {
            PropertyPremiseHeaderDto propertyPremiseHeaderDto = null;

            using (var uow = _unitOfWorkManager.Begin())
            {
                propertyPremiseHeaderDto = await _PropertyPremiseHeaderReadRepository.GetById(id);
                propertyPremiseHeaderDto.Id = id;
                propertyPremiseHeaderDto.LastModifierUserId = _applicationUser.UserUid;
                propertyPremiseHeaderDto.LastModificationTime = DateTime.Now;

                var local = _PropertyPremiseHeaderAdapter.Map(propertyPremiseHeaderDto).Build();

                if (Notification.HasNotification())
                    return null;

                local.IsActive = !local.IsActive;

                await _PropertyPremiseHeaderRepository.UpdateItem(id, local);

                if (Notification.HasNotification())
                    return null;

                uow.Complete();
            }

            return propertyPremiseHeaderDto;
        }

        public async Task<PropertyPremiseHeaderDto> RemoveAsync(Guid id)
        {
            PropertyPremiseHeaderDto dto = null;
            using (var uow = _unitOfWorkManager.Begin())
            {
                dto = await _PropertyPremiseHeaderReadRepository.GetById(id);
                var local = _PropertyPremiseHeaderAdapter.Map(dto).Build();

                if (Notification.HasNotification())
                    return null;

                await _PropertyPremiseHeaderRepository.RemoveItem(local);

                if (Notification.HasNotification())
                    return null;

                uow.Complete();
            }

            return dto;
        }
    }
}
