﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.RMS.Application.Adapters;
using Thex.RMS.Application.Interfaces;
using Thex.RMS.Dto;
using Thex.RMS.Infra.Interfaces;
using Thex.RMS.Infra.Interfaces.ReadRepositories;
using Thex.Kernel;
using Tnf.Application.Services;
using Tnf.Notifications;
using Tnf.Repositories.Uow;

namespace Thex.RMS.Application.Services
{
    public class PropertyPremiseAppService : ApplicationService, IPropertyPremiseAppService
    {
        private readonly IPropertyPremiseReadRepository _PropertyPremiseReadRepository;
        private readonly IPropertyPremiseRepository _PropertyPremiseRepository;
        private readonly IPropertyPremiseAdapter _PropertyPremiseAdapter;

        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IApplicationUser _applicationUser;

        public PropertyPremiseAppService(
            IPropertyPremiseReadRepository PropertyPremiseReadRepository,
            IPropertyPremiseRepository PropertyPremiseRepository,
            IPropertyPremiseAdapter PropertyPremiseAdapter,
            IApplicationUser applicationUser,
            IUnitOfWorkManager unitOfWorkManager,
            INotificationHandler notificationHandler)
            : base(notificationHandler)
        {
            _PropertyPremiseReadRepository = PropertyPremiseReadRepository;
            _PropertyPremiseRepository = PropertyPremiseRepository;
            _PropertyPremiseAdapter = PropertyPremiseAdapter;
            _applicationUser = applicationUser;

            _unitOfWorkManager = unitOfWorkManager;
        }

        public virtual async Task<List<PropertyPremiseDto>> GetAll()
        {
            return await _PropertyPremiseReadRepository.GetAll(int.Parse(_applicationUser.PropertyId));
        }

        public virtual async Task<PropertyPremiseDto> GetById(Guid id)
        {
            return await _PropertyPremiseReadRepository.GetById(id);
        }

        public async Task<PropertyPremiseDto> InsertAsync(PropertyPremiseDto dto)
        {
            using (var uow = _unitOfWorkManager.Begin())
            {
                dto.Id = Guid.NewGuid();
                dto.CreationTime = DateTime.Now;
                dto.CreatorUserId = _applicationUser.UserUid;

                var local = _PropertyPremiseAdapter.Map(dto).Build();

                if (Notification.HasNotification())
                    return null;

                await _PropertyPremiseRepository.InsertItem(local);

                if (Notification.HasNotification())
                    return null;

                uow.Complete();
            }

            return dto;
        }

        public async Task<PropertyPremiseDto> UpdateAsync(Guid id, PropertyPremiseDto dto)
        {
            PropertyPremiseDto propertyPremiseDto = null;

            using (var uow = _unitOfWorkManager.Begin())
            {
                propertyPremiseDto = await _PropertyPremiseReadRepository.GetById(id);
                propertyPremiseDto.Id = id;
                propertyPremiseDto.LastModifierUserId = _applicationUser.UserUid;
                propertyPremiseDto.LastModificationTime = DateTime.Now;

                var local = _PropertyPremiseAdapter.Map(propertyPremiseDto).Build();
                var propertyPremise = _PropertyPremiseAdapter.Map(local, dto).Build();

                if (Notification.HasNotification())
                    return null;

                await _PropertyPremiseRepository.UpdateItem(id, propertyPremise);

                if (Notification.HasNotification())
                    return null;

                uow.Complete();
            }

            return propertyPremiseDto;
        }

        public async Task<PropertyPremiseDto> RemoveAsync(Guid id)
        {
            if (Notification.HasNotification())
                return null;

            PropertyPremiseDto dto = null;
            using (var uow = _unitOfWorkManager.Begin())
            {
                dto = await _PropertyPremiseReadRepository.GetById(id);
                var local = _PropertyPremiseAdapter.Map(dto).Build();

                if (Notification.HasNotification())
                    return null;

                await _PropertyPremiseRepository.RemoveItem(local);

                if (Notification.HasNotification())
                    return null;

                uow.Complete();
            }

            return dto;
        }
    }
}
