﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.RMS.Application.Adapters;
using Thex.RMS.Application.Interfaces;
using Thex.RMS.Dto;
using Thex.RMS.Infra.Interfaces;
using Thex.RMS.Infra.Interfaces.ReadRepositories;
using Thex.Kernel;
using Tnf.Application.Services;
using Tnf.Notifications;
using Tnf.Repositories.Uow;

namespace Thex.RMS.Application.Services
{
    public class UserAppService : ApplicationService, IUserAppService
    {
        private readonly IUserReadRepository _userReadRepository;
        private readonly IUserRepository _userRepository;
        private readonly IUserAdapter _userAdapter;

        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IApplicationUser _applicationUser;

        public UserAppService(
            IUserReadRepository userReadRepository,
            IUserRepository userRepository,
            IUserAdapter userAdapter,
            IApplicationUser applicationUser,
            IUnitOfWorkManager unitOfWorkManager,
            INotificationHandler notificationHandler)
            : base(notificationHandler)
        {
            _userReadRepository = userReadRepository;
            _userRepository = userRepository;
            _userAdapter = userAdapter;
            _applicationUser = applicationUser;

            _unitOfWorkManager = unitOfWorkManager;
        }

        public virtual async Task<List<UserDto>> GetAll()
        {
            return await _userReadRepository.GetAll(_applicationUser.TenantId);
        }

        public virtual async Task<UserDto> GetById()
        {
            return await _userReadRepository.GetById(_applicationUser.UserUid);
        }

        public async Task<UserDto> SendPhoto(Guid id, UserDto dto)
        {
            var _bytes  = Convert.FromBase64String(dto.PhotoBase64);

            if (Notification.HasNotification())
                return null;

            using (var uow = _unitOfWorkManager.Begin())
            {
                var userDto = await _userReadRepository.GetById(id);
                // TODO: update

                if (Notification.HasNotification())
                    return null;

                uow.Complete();
            }

            return new UserDto() { PhotoUrl = "" };
        }

        public async Task<UserDto> UpdateAsync(Guid id, UserDto dto)
        {
            ValidateDto(dto);
            if (Notification.HasNotification())
                return null;

            using (var uow = _unitOfWorkManager.Begin())
            {
                var userDto = await _userReadRepository.GetById(id);
                userDto.Id = id;
                userDto.LastModifierUserId = _applicationUser.UserUid;
                userDto.LastModificationTime = DateTime.Now;

                userDto.Name  = dto.Name;
                userDto.Email = dto.Email;

                var local = _userAdapter.Map(userDto).Build();
                //var user = _userAdapter.Map(local, dto).Build();
                if (Notification.HasNotification())
                    return null;

                await _userRepository.UpdateItem(id, local);

                if (Notification.HasNotification())
                    return null;

                if (Notification.HasNotification())
                    return null;

                uow.Complete();
            }

            return null;
        }
    }
}
