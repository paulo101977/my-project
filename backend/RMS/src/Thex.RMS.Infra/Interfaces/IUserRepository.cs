﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.RMS.Dto;
using Thex.RMS.Infra.Entities;
using Tnf.Dto;

namespace Thex.RMS.Infra.Interfaces
{
    public interface IUserRepository
    {
        Task UpdateItem(Guid id, User obj);
    }
}
