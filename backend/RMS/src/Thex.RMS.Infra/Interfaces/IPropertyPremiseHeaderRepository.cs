﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.RMS.Dto;
using Thex.RMS.Infra.Entities;
using Tnf.Dto;

namespace Thex.RMS.Infra.Interfaces
{
    public interface IPropertyPremiseHeaderRepository
    {
        Task UpdateItem(Guid id, PropertyPremiseHeader obj);
        Task InsertItem(PropertyPremiseHeader obj);
        Task RemoveItem(PropertyPremiseHeader obj);
    }
}
