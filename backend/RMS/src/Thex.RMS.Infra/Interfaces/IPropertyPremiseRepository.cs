﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.RMS.Dto;
using Thex.RMS.Infra.Entities;
using Tnf.Dto;

namespace Thex.RMS.Infra.Interfaces
{
    public interface IPropertyPremiseRepository
    {
        Task UpdateItem(Guid id, PropertyPremise obj);
        Task InsertItem(PropertyPremise obj);
        Task RemoveItem(PropertyPremise obj);
    }
}
