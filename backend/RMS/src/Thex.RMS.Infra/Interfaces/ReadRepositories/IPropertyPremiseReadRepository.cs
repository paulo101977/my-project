﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.RMS.Dto;
using Thex.RMS.Infra.Entities;

namespace Thex.RMS.Infra.Interfaces.ReadRepositories
{
    public interface IPropertyPremiseReadRepository
    {
        Task<List<PropertyPremiseDto>> GetAll(int propertyId);
        Task<PropertyPremiseDto> GetById(Guid id); 
    }
}
