﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.RMS.Dto;
using Thex.RMS.Infra.Entities;

namespace Thex.RMS.Infra.Interfaces.ReadRepositories
{
    public interface IUserReadRepository
    {
        Task<List<UserDto>> GetAll(Guid tenantId);
        Task<UserDto> GetById(Guid id); 
    }
}
