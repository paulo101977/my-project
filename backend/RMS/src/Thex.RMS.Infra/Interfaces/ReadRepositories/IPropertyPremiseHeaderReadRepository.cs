﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.RMS.Dto;
using Thex.RMS.Infra.Entities;

namespace Thex.RMS.Infra.Interfaces.ReadRepositories
{
    public interface IPropertyPremiseHeaderReadRepository
    {
        Task<PropertyPremiseHeaderSelectDto> GetAll(GetAllPropertyPremiseHeaderDto request);
        Task<PropertyPremiseHeaderDto> GetById(Guid id); 
    }
}
