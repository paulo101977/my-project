﻿using Microsoft.EntityFrameworkCore;
using Thex.RMS.Infra.Entities;
using Tnf.EntityFrameworkCore;
using Tnf.Runtime.Session;

namespace Thex.RMS.Infra.Context
{
    public class ThexRMSContext : TnfDbContext
    {
        // Importante o construtor do contexto receber as opções com o tipo generico definido: DbContextOptions<TDbContext>
        public ThexRMSContext(DbContextOptions<ThexRMSContext> options, ITnfSession session)
            : base(options, session)
        {
        }
    }
}
