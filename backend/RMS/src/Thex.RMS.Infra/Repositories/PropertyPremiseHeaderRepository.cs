﻿using System.Threading.Tasks;
using Tnf.Dapper.Repositories;
using Tnf.Repositories;
using Tnf.Localization;
using Thex.RMS.Infra.Context;
using Thex.RMS.Infra.Entities;
using Thex.RMS.Infra.Interfaces;
using System;
using Thex.RMS.Dto;
using Thex.Kernel;
using Thex.GenericLog;

namespace Thex.RMS.Infra.Repositories
{
    public class PropertyPremiseHeaderRepository : DapperEfRepositoryBase<ThexRMSContext, PropertyPremiseHeader>, IPropertyPremiseHeaderRepository
    {
        private readonly ILocalizationManager _localizationManager;
        private readonly IApplicationUser _applicationUser;

        public PropertyPremiseHeaderRepository(
            IActiveTransactionProvider activeTransactionProvider,
            ILocalizationManager localizationManager,
            IApplicationUser applicationUser,
            IGenericLogHandler _genericLogHandler)
            : base(activeTransactionProvider, _genericLogHandler)
        {
            _localizationManager = localizationManager;
            _applicationUser = applicationUser;
        }

        public async Task UpdateItem(Guid id, PropertyPremiseHeader obj)
        {
            await UpdateAsync(obj);
        }

        public async Task InsertItem(PropertyPremiseHeader obj)
        {
            await InsertAsync(obj);
        }

        public async Task RemoveItem(PropertyPremiseHeader obj)
        {
            await DeleteAsync(obj);
        }
    }
}
