﻿using System.Threading.Tasks;
using Tnf.Dapper.Repositories;
using Tnf.Repositories;
using Tnf.Localization;
using Thex.RMS.Infra.Context;
using Thex.RMS.Infra.Entities;
using Thex.RMS.Infra.Interfaces;
using System;
using Thex.RMS.Dto;
using Thex.Kernel;
using Thex.GenericLog;

namespace Thex.RMS.Infra.Repositories
{
    public class UserRepository : DapperEfRepositoryBase<ThexRMSContext, User>, IUserRepository
    {
        private readonly ILocalizationManager _localizationManager;
        private readonly IApplicationUser _applicationUser;
        private readonly IGenericLogHandler _genericLogHandler;

        public UserRepository(
            IActiveTransactionProvider activeTransactionProvider,
            ILocalizationManager localizationManager,
            IApplicationUser applicationUser,
            IGenericLogHandler _genericLogHandler)
            : base(activeTransactionProvider, _genericLogHandler)
        {
            _localizationManager = localizationManager;
            _applicationUser = applicationUser;
        }

        public async Task UpdateItem(Guid id, User obj)
        {
            await UpdateAsync(obj);
        }
    }
}
