﻿using System;
using System.Threading.Tasks;
using Tnf.Dapper.Repositories;
using Tnf.Repositories;
using System.Linq;
using Tnf.Localization;
using Thex.Common;
using Thex.RMS.Infra.Context;
using Thex.RMS.Infra.Entities;
using Thex.RMS.Infra.Interfaces.ReadRepositories;
using Thex.RMS.Dto.Enumerations;
using Thex.RMS.Dto;
using System.Collections.Generic;
using Dapper;
using Slapper;
using Thex.Common.Enumerations;
using Thex.GenericLog;

namespace Thex.RMS.Infra.Repositories.ReadRepositories
{
    public class UserReadRepository : DapperEfRepositoryBase<ThexRMSContext, User>, IUserReadRepository
    {
        private readonly ILocalizationManager _localizationManager;

        public UserReadRepository(
            IActiveTransactionProvider activeTransactionProvider,
            ILocalizationManager localizationManager,
            IGenericLogHandler _genericLogHandler)
            : base(activeTransactionProvider, _genericLogHandler)
        {
            _localizationManager = localizationManager;
        }

        public async Task<UserDto> GetById(Guid id)
        {

            var query = (await QueryAsync<UserDto>(@"
                select
                    u.*,
                    p.DateOfBirth,
                    p.PhotoUrl
                From   Users u
                left   join Person p on u.PersonId = p.PersonId
                where  u.Id =  @Id
            ", new { Id = id }));

            var result = query.FirstOrDefault(); 

            return result;
        }

        public new async Task<List<UserDto>> GetAll(Guid tenantId)
        {
            var result = (await QueryAsync<UserDto>(@"
                select u.Id as Id, u.*
                from   Users u
                where  u.TenantId = @TenantId
            ", new { TenantId = tenantId })).ToList();

            return result;
        }
    }
}
