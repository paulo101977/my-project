﻿using System;
using System.Threading.Tasks;
using Tnf.Dapper.Repositories;
using Tnf.Repositories;
using System.Linq;
using Tnf.Localization;
using Thex.Common;
using Thex.RMS.Infra.Context;
using Thex.RMS.Infra.Entities;
using Thex.RMS.Infra.Interfaces.ReadRepositories;
using Thex.RMS.Dto.Enumerations;
using Thex.RMS.Dto;
using System.Collections.Generic;
using Dapper;
using Slapper;
using Thex.Common.Enumerations;
using Thex.GenericLog;

namespace Thex.RMS.Infra.Repositories.ReadRepositories
{
    public class PropertyPremiseHeaderReadRepository : DapperEfRepositoryBase<ThexRMSContext, PropertyPremiseHeader>, IPropertyPremiseHeaderReadRepository
    {
        private readonly ILocalizationManager _localizationManager;

        public PropertyPremiseHeaderReadRepository(
            IActiveTransactionProvider activeTransactionProvider,
            ILocalizationManager localizationManager,
            IGenericLogHandler _genericLogHandler)
            : base(activeTransactionProvider, _genericLogHandler)
        {
            _localizationManager = localizationManager;
        }

        public async Task<PropertyPremiseHeaderDto> GetById(Guid id)
        {
            var query = await base.GetAllAsync();

            return query.FirstOrDefault(x => x.Id == id).MapTo<PropertyPremiseHeaderDto>();
        }

        public async Task<PropertyPremiseHeaderSelectDto> GetAll(GetAllPropertyPremiseHeaderDto request)
        {
            var res = new PropertyPremiseHeaderSelectDto() {
                PropertyPremiseHeaderList = await GetPremises(request),
                RoomTypeList = GetRoomTypes(request.PropertyId),
                CurrencyList = GetCurrencies()
            };

            return res;
        }

        private List<RoomTypeDto> GetRoomTypes(int? PropertyId)
        {
            return (Query<dynamic>(@"Select RoomTypeId as Id, * from RoomType where PropertyId = @PropertyId", new { PropertyId }).MapTo<List<RoomTypeDto>>());
        }

        private List<PropertyParameterDto> GetPropertyParams(int? PropertyId)
        {
            return (Query<dynamic>(@"Select PropertyParameterId as Id, * from PropertyParameter where PropertyId = @PropertyId", new { PropertyId }).MapTo<List<PropertyParameterDto>>());
        }

        private List<CurrencyDto> GetCurrencies()
        {
            return (Query<dynamic>(@"Select CurrencyId as Id, * from Currency").MapTo<List<CurrencyDto>>());
        }

        public async Task<List<PropertyPremiseHeaderFullDto>> GetPremises(GetAllPropertyPremiseHeaderDto request)
        {
            var query = await QueryAsync<dynamic>(@"
                    select pp.PropertyPremiseHeaderId as Id, rt.Abbreviation as PropertyPremiseList_RoomTypeName,
                            pp.Pax_1 as PropertyPremiseList_Pax1,
                            pp.Pax_2 as PropertyPremiseList_Pax2,
                            pp.Pax_3 as PropertyPremiseList_Pax3,
                            pp.Pax_4 as PropertyPremiseList_Pax4,
                            pp.Pax_5 as PropertyPremiseList_Pax5,
                            pp.Pax_6 as PropertyPremiseList_Pax6,
                            pp.Pax_7 as PropertyPremiseList_Pax7,
                            pp.Pax_8 as PropertyPremiseList_Pax8,
                            pp.Pax_9 as PropertyPremiseList_Pax9,
                            pp.Pax_10 as PropertyPremiseList_Pax10,
                            pp.Pax_1_Operator as PropertyPremiseList_Pax1Operator,
                            pp.Pax_2_Operator as PropertyPremiseList_Pax2Operator,
                            pp.Pax_3_Operator as PropertyPremiseList_Pax3Operator,
                            pp.Pax_4_Operator as PropertyPremiseList_Pax4Operator,
                            pp.Pax_5_Operator as PropertyPremiseList_Pax5Operator,
                            pp.Pax_6_Operator as PropertyPremiseList_Pax6Operator,
                            pp.Pax_7_Operator as PropertyPremiseList_Pax7Operator,
                            pp.Pax_8_Operator as PropertyPremiseList_Pax8Operator,
                            pp.Pax_9_Operator as PropertyPremiseList_Pax9Operator,
                            pp.Pax_10_Operator as PropertyPremiseList_Pax10Operator,
                            pp.Pax_Additional as PropertyPremiseList_PaxAdditional,
                            pp.Pax_Additional_Operator as PropertyPremiseList_PaxAdditionalOperator,
                            pp.Child_1 as PropertyPremiseList_Child1,
                            pp.Child_2 as PropertyPremiseList_Child2,
                            pp.Child_3 as PropertyPremiseList_Child3,
                            pp.Child_1_Operator as PropertyPremiseList_Child1Operator,
                            pp.Child_2_Operator as PropertyPremiseList_Child2Operator,
                            pp.Child_3_Operator as PropertyPremiseList_Child3Operator,
                            rtr.Abbreviation as RoomTypeReferenceName,
                            c.CurrencyName,
                            c.AlphabeticCode as CurrencyAlphabeticCode,
                            c.Symbol as CurrencySymbol,
                            pph.*
                    from   PropertyPremiseHeader pph
                        inner join PropertyPremise pp on pp.PropertyPremiseHeaderId = pph.PropertyPremiseHeaderId
                        left  join RoomType rt on rt.RoomTypeId = pp.RoomTypeId
                        left  join RoomType rtr on rtr.RoomTypeId = pph.RoomTypeReferenceId
                        left  join Currency c on c.CurrencyId = pph.CurrencyId
                    where  pph.IsDeleted = 0 
                        AND  pph.IsDeleted = 0
                        AND  pph.PropertyId = @PropertyId
            " +
            (request.StartDate != null && request.EndDate != null ? " AND  pph.InitialDate >= @StartDate AND COALESCE(pph.FinalDate, getdate()) <= @EndDate " : ""),
            new
            {
                request.PropertyId,
                request.StartDate,
                request.EndDate
            });

            Slapper.AutoMapper.Cache.ClearAllCaches();
            Slapper.AutoMapper.Configuration.AddIdentifiers(
                typeof(PropertyPremiseDto), new List<string>() {
                "Pax1", "Pax2", "Pax3", "Pax4", "Pax5", "Pax6",  "Pax7",
                "Pax8", "Pax9", "Pax10", "Pax1Operator", "Pax2Operator", "Pax3Operator",
                "Pax4Operator", "Pax5Operator", "Pax6Operator", "Pax7Operator", "Pax8Operator",
                "Pax9Operator", "Pax10Operator", "PaxAdditional", "PaxAdditionalOperator",
                "Child1", "Child2", "Child3", "Child1Operator", "Child2Operator", "Child3Operator",
                "RoomTypeName"
            });

            var result = (Slapper.AutoMapper.MapDynamic<PropertyPremiseHeaderFullDto>(query)
                as IEnumerable<PropertyPremiseHeaderFullDto>).ToList();

            return result;
        }
    }
}
