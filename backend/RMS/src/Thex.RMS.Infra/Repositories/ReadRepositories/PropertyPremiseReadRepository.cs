﻿using System;
using System.Threading.Tasks;
using Tnf.Dapper.Repositories;
using Tnf.Repositories;
using System.Linq;
using Tnf.Localization;
using Thex.Common;
using Thex.RMS.Infra.Context;
using Thex.RMS.Infra.Entities;
using Thex.RMS.Infra.Interfaces.ReadRepositories;
using Thex.RMS.Dto;
using System.Collections.Generic;
using Thex.GenericLog;
using Dapper;

namespace Thex.RMS.Infra.Repositories.ReadRepositories
{
    public class PropertyPremiseReadRepository : DapperEfRepositoryBase<ThexRMSContext, PropertyPremise>, IPropertyPremiseReadRepository
    {
        private readonly ILocalizationManager _localizationManager;
        private readonly IPropertyPremiseHeaderReadRepository _propertyPremiseHeaderReadRepository;

        public PropertyPremiseReadRepository(
            IActiveTransactionProvider activeTransactionProvider,
            ILocalizationManager localizationManager,
            IPropertyPremiseHeaderReadRepository propertyPremiseHeaderReadRepository,
            IGenericLogHandler _genericLogHandler)
            : base(activeTransactionProvider, _genericLogHandler)
        {
            _localizationManager = localizationManager;
            _propertyPremiseHeaderReadRepository = propertyPremiseHeaderReadRepository;
        }

        public async Task<PropertyPremiseDto> GetById(Guid id)
        {
            var query = await base.GetAllAsync();

            return query.FirstOrDefault(x => x.Id == id).MapTo<PropertyPremiseDto>();
        }

        public async Task<List<PropertyPremiseDto>> GetAll(int propertyId)
        {
            var query = Connection.Query<PropertyPremiseDto, PropertyPremiseHeaderDto, PropertyPremiseDto>(@"
                    select pp.PropertyPremiseId as Id, rt.Name as RoomTypeName,
                            pp.Pax_1 as Pax1,
                            pp.Pax_2 as Pax2,
                            pp.Pax_3 as Pax3,
                            pp.Pax_4 as Pax4,
                            pp.Pax_5 as Pax5,
                            pp.Pax_6 as Pax6,
                            pp.Pax_7 as Pax7,
                            pp.Pax_8 as Pax8,
                            pp.Pax_9 as Pax9,
                            pp.Pax_10 as Pax10,
                            pp.Pax_1_Operator as Pax1Operator,
                            pp.Pax_2_Operator as Pax2Operator,
                            pp.Pax_3_Operator as Pax3Operator,
                            pp.Pax_4_Operator as Pax4Operator,
                            pp.Pax_5_Operator as Pax5Operator,
                            pp.Pax_6_Operator as Pax6Operator,
                            pp.Pax_7_Operator as Pax7Operator,
                            pp.Pax_8_Operator as Pax8Operator,
                            pp.Pax_9_Operator as Pax9Operator,
                            pp.Pax_10_Operator as Pax10Operator,
                            pp.Pax_Additional as PaxAdditional,
                            pp.Pax_Additional_Operator as PaxAdditionalOperator,
                            pp.Child_1 as Child1,
                            pp.Child_2 as Child2,
                            pp.Child_3 as Child3,
                            pp.Child_1_Operator as Child1Operator,
                            pp.Child_2_Operator as Child2Operator,
                            pp.Child_3_Operator as Child3Operator,
                            pph.*, pp.*
                    from   PropertyPremise pp
                        inner join PropertyPremiseHeader pph on pph.PropertyPremiseHeaderId = pp.PropertyPremiseHeaderId
                        inner join RoomType rt on rt.RoomTypeId = pp.RoomTypeId
                    where  pp.IsDeleted = 0 
                        AND  pph.IsActive = 1
                        AND  pph.PropertyId = @PropertyId
            ",
            map: (pp, pph) =>
            {
                pp.PropertyPremiseHeader = pph;
                return pp;
            },
            param: new
            {
                PropertyId = propertyId
            },
            transaction: ActiveTransaction,
            splitOn: "PropertyPremiseId,PropertyPremiseHeaderId");

            return query.MapTo<List<PropertyPremiseDto>>();
        }
    }
}
