﻿using System.Threading.Tasks;
using Tnf.Dapper.Repositories;
using Tnf.Repositories;
using Tnf.Localization;
using Thex.RMS.Infra.Context;
using Thex.RMS.Infra.Entities;
using Thex.RMS.Infra.Interfaces;
using System;
using Thex.RMS.Dto;
using Thex.Kernel;
using Thex.GenericLog;

namespace Thex.RMS.Infra.Repositories
{
    public class PropertyPremiseRepository : DapperEfRepositoryBase<ThexRMSContext, PropertyPremise>, IPropertyPremiseRepository
    {
        private readonly ILocalizationManager _localizationManager;
        private readonly IApplicationUser _applicationUser;

        public PropertyPremiseRepository(
            IActiveTransactionProvider activeTransactionProvider,
            ILocalizationManager localizationManager,
            IApplicationUser applicationUser,
            IGenericLogHandler _genericLogHandler)
            : base(activeTransactionProvider, _genericLogHandler)
        {
            _localizationManager = localizationManager;
            _applicationUser = applicationUser;
        }

        public async Task UpdateItem(Guid id, PropertyPremise obj)
        {
            await UpdateAsync(obj);
        }

        public async Task InsertItem(PropertyPremise obj)
        {
            await InsertAsync(obj);
        }

        public async Task RemoveItem(PropertyPremise obj)
        {
            await DeleteAsync(obj);
        }
    }
}
