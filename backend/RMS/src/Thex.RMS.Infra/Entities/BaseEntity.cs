﻿using System;

namespace Thex.RMS.Infra.Entities
{
    public class BaseEntity : ThexFullAuditedEntity, IEntityGuid
    {
        public Guid Id { get; set; }
    }
}
