﻿using Thex.Common;
using Thex.RMS.Infra;
using Thex.RMS.Infra.Context;
using Thex.RMS.Infra.Interfaces;
using Thex.RMS.Infra.Interfaces.ReadRepositories;
using Thex.RMS.Infra.Repositories;
using Thex.RMS.Infra.Repositories.ReadRepositories;
using Tnf.Dapper;

namespace Microsoft.Extensions.DependencyInjection
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddInfraDependency(this IServiceCollection services)
        {
            // Configura o uso do Dapper registrando os contextos que serão
            // usados pela aplicação
            services
                .AddCommonDependency()
                .AddTnfEntityFrameworkCore()
                .AddTnfDbContext<ThexRMSContext>(config => DbContextConfigurer.Configure(config))
                .AddTnfDapper(options =>
                {
                    //options.MapperAssemblies.Add(typeof(TransportationTypeMapper).Assembly);
                    options.DbType = DapperDbType.SqlServer;
                });

            services.AddTnfAutoMapper(config =>
            {
                config.AddProfile<UserProfile>();
            });

            services.AddTransient<IUserReadRepository, UserReadRepository>();
            services.AddTransient<IPropertyPremiseReadRepository, PropertyPremiseReadRepository>();
            services.AddTransient<IPropertyPremiseHeaderReadRepository, PropertyPremiseHeaderReadRepository>();

            services.AddTransient<IUserRepository, UserRepository>();
            services.AddTransient<IPropertyPremiseRepository, PropertyPremiseRepository>();
            services.AddTransient<IPropertyPremiseHeaderRepository, PropertyPremiseHeaderRepository>();

            return services;
        }
    }
}
