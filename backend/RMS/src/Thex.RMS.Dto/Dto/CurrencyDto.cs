﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a Crudzilla tool.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using Tnf.Dto;

namespace Thex.RMS.Dto
{
    public partial class CurrencyDto
    {
        public static CurrencyDto NullInstance = null;

        public Guid Id { get; set; }
        public int? CountrySubdivisionId { get; set; }
        public string TwoLetterIsoCode { get; set; }
        public string CurrencyName { get; set; }
        public string AlphabeticCode { get; set; }
        public string NumnericCode { get; set; }
        public int MinorUnit { get; set; }
        public string Symbol { get; set; }
        public decimal ExchangeRate { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime CreationTime { get; set; }
        public long? CreatorUserId { get; set; }
        public DateTime? LastModificationTime { get; set; }
        public long? LastModifierUserId { get; set; }
        public DateTime? DeletionTime { get; set; }
        public long? DeleterUserId { get; set; }
    }
}
