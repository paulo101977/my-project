﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Thex.RMS.Application;
using Thex.RMS.Application.Interfaces;
using Thex.RMS.Dto;
using Tnf.AspNetCore.Mvc.Response;

namespace Thex.RMS.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [AllowAnonymous]
    public class HealthController : TnfController
    {
        private readonly IUserAppService _userAppService;

        public HealthController(IUserAppService userAppService)
        {
            _userAppService = userAppService;
        }

        /// <summary>
        /// Verifica se a API está em funcionamento e conectada no banco de dados
        /// </summary>
        /// <returns></returns>
        [HttpGet("check")]
        [ProducesResponseType(typeof(bool), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> GetAll()
        {
            var response = await _userAppService.GetAll();

            return CreateResponseOnGetAll(response != null ? true : false, EntityNames.Health);
        }
    }
}