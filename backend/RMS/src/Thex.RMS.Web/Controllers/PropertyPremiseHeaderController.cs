﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.RMS.Application.Interfaces;
using Thex.RMS.Dto;
using Thex.RMS.Infra.Entities;
using Tnf.AspNetCore.Mvc.Response;

namespace Thex.RMS.Web.Controllers
{
    [Route(RouteConsts.PropertyPremiseHeaderRouteName)]
    [Authorize]
    public class PropertyPremiseHeaderController : TnfController
    {
        private readonly IPropertyPremiseHeaderAppService _PropertyPremiseHeaderAppService;

        public PropertyPremiseHeaderController(IPropertyPremiseHeaderAppService PropertyPremiseHeaderAppService)
        {
            _PropertyPremiseHeaderAppService = PropertyPremiseHeaderAppService;
        }


        /// <summary>
        /// Listar premissas
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(typeof(IList<PropertyPremiseHeaderDto>), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> GetAll([FromQuery] GetAllPropertyPremiseHeaderDto request)
        {
            var response = await _PropertyPremiseHeaderAppService.GetAll(request);

            return CreateResponseOnGet(response);
        }


        /// <summary>
        /// Recuperar premissa por Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id}")]
        [ProducesResponseType(typeof(PropertyPremiseHeaderDto), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> GetById(Guid id)
        {
            var response = await _PropertyPremiseHeaderAppService.GetById(id);

            return CreateResponseOnGet(response);
        }


        /// <summary>
        /// Cadastrar nova premissa
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType(typeof(PropertyPremiseHeaderDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Create([FromBody] PropertyPremiseHeaderDto requestDto)
        {
            var response = await _PropertyPremiseHeaderAppService.InsertAsync(requestDto);

            return CreateResponseOnPost(response);
        }


        /// <summary>
        /// Atualizar Premissa
        /// </summary>
        /// <returns></returns>
        [HttpPut("{id}")]
        [ProducesResponseType(typeof(PropertyPremiseHeaderDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Update(Guid id, [FromBody] PropertyPremiseHeaderDto requestDto)
        {
            var response = await _PropertyPremiseHeaderAppService.UpdateAsync(id, requestDto);

            return CreateResponseOnPut(response);
        }


        /// <summary>
        /// Remover Premissa
        /// </summary>
        /// <returns></returns>
        [HttpDelete("{id}")]
        [ProducesResponseType(typeof(PropertyPremiseHeaderDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Remove(Guid id)
        {
            var response = await _PropertyPremiseHeaderAppService.RemoveAsync(id);

            return CreateResponseOnDelete(response);
        }
        
        /// <summary>
        /// Ativa ou desativa uma premisa
        /// </summary>
        /// <returns></returns>
        [HttpPatch("{id}/toggleactivation")]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> ToggleActivation(string id)
        {
            var response = await _PropertyPremiseHeaderAppService.ToggleAsync(Guid.Parse(id));

            return CreateResponseOnPatch(response);
        }
    }
}
