﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using Thex.RMS.Application.Interfaces;
using Thex.RMS.Dto;
using Thex.RMS.Infra.Entities;
using Tnf.AspNetCore.Mvc.Response;

namespace Thex.RMS.Web.Controllers
{
    [Route(RouteConsts.UserRouteName)]
    [Authorize]
    public class UserController : TnfController
    {
        private readonly IUserAppService _userAppService;

        public UserController(IUserAppService userAppService)
        {
            _userAppService = userAppService;
        }

        [HttpGet]
        [ProducesResponseType(typeof(UserDto), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> GetAll()
        {
            var response = await _userAppService.GetAll();

            return CreateResponseOnGet(response);
        }

        [HttpPut("sendPhoto")]
        [ProducesResponseType(typeof(UserDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> SendPhoto(Guid id, [FromBody] UserDto userDto)
        {
            var response = await _userAppService.SendPhoto(id, userDto);

            return CreateResponseOnPut(response);
        }

        [HttpPut("{id}")]
        [ProducesResponseType(typeof(UserDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Update(Guid id, [FromBody] UserDto requestDto)
        {
            await _userAppService.UpdateAsync(id, requestDto);

            return CreateResponseOnPut(UserDto.NullInstance);
        }
    }
}
