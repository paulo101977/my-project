﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.RMS.Application.Interfaces;
using Thex.RMS.Dto;
using Thex.RMS.Infra.Entities;
using Tnf.AspNetCore.Mvc.Response;

namespace Thex.RMS.Web.Controllers
{
    [Route(RouteConsts.PropertyPremiseRouteName)]
    [Authorize]
    public class PropertyPremiseController : TnfController
    {
        private readonly IPropertyPremiseAppService _PropertyPremiseAppService;

        public PropertyPremiseController(IPropertyPremiseAppService PropertyPremiseAppService)
        {
            _PropertyPremiseAppService = PropertyPremiseAppService;
        }

        /// <summary>
        /// Listar variacao de premissa
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(typeof(IList<PropertyPremiseDto>), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> GetAll()
        {
            var response = await _PropertyPremiseAppService.GetAll();

            return CreateResponseOnGet(response);
        }

        /// <summary>
        /// Recupear uma variação de premissa
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id}")]
        [ProducesResponseType(typeof(PropertyPremiseDto), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> GetById(Guid id)
        {
            var response = await _PropertyPremiseAppService.GetById(id);

            return CreateResponseOnGet(response);
        }

        /// <summary>
        /// Criar uma variação de premissa
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType(typeof(PropertyPremiseDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Create([FromBody] PropertyPremiseDto requestDto)
        {
            var response = await _PropertyPremiseAppService.InsertAsync(requestDto);

            return CreateResponseOnPost(PropertyPremiseDto.NullInstance);
        }

        /// <summary>
        /// Atualizar uma variação de premissa
        /// </summary>
        /// <returns></returns>
        [HttpPut("{id}")]
        [ProducesResponseType(typeof(PropertyPremiseDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Update(Guid id, [FromBody] PropertyPremiseDto requestDto)
        {
            var response = await _PropertyPremiseAppService.UpdateAsync(id, requestDto);

            return CreateResponseOnPut(PropertyPremiseDto.NullInstance);
        }

        /// <summary>
        /// Remover uma variação de premissa
        /// </summary>
        /// <returns></returns>
        [HttpDelete("{id}")]
        [ProducesResponseType(typeof(PropertyPremiseDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Remove(Guid id)
        {
            var response = await _PropertyPremiseAppService.RemoveAsync(id);

            return CreateResponseOnDelete(response);
        }
    }
}
