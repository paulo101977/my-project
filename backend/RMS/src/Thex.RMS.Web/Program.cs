﻿using Elasticsearch.Net;
using Elasticsearch.Net.Aws;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Serilog;
using Serilog.Sinks.Elasticsearch;
using System;
using System.Diagnostics;
using System.IO;
using Thex.AspNetCore.Security;
using Thex.Common;

namespace Thex.RMS.Web
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Console.Title = typeof(Program).Namespace;

            var hostConfig = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("hosting.json")
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .Build();

            var host = WebHost.CreateDefaultBuilder(args)
                .UseConfiguration(hostConfig)
                .ConfigureAppConfiguration((hostingContext, config) =>
                {
                    var env = hostingContext.HostingEnvironment;
                    config.AddJsonFile($"appsettings.json", optional: false, reloadOnChange: true);

                    config.AddEnvironmentVariables();
                    config.AddCommandLine(args);
                })
                .ConfigureLogging((hostingContext, logging) =>
                {
                    var config = new ServicesConfiguration();
                    hostConfig.GetSection("ServicesConfiguration").Bind(config);

                    var env = hostingContext.HostingEnvironment;

                    if (!Debugger.IsAttached)
                        Log.Logger = new LoggerConfiguration()
                           .Enrich.WithProperty("Creation", DateTime.Now)
                           .Enrich.FromLogContext()
                           .MinimumLevel.Error()
                           .WriteTo.Elasticsearch(new ElasticsearchSinkOptions(new Uri(config.ElasticSearch.Uri))
                           {
                               ModifyConnectionSettings = conn =>
                               {
                                   var httpConnection = new AwsHttpConnection("us-east-1", new StaticCredentialsProvider(new AwsCredentials
                                   {
                                       AccessKey = HashManager.GenerateMD5Decrypto(config.ElasticSearch.Accesskey),
                                       SecretKey = HashManager.GenerateMD5Decrypto(config.ElasticSearch.Secretkey)
                                   }));

                                   var pool = new SingleNodeConnectionPool(new Uri(config.ElasticSearch.Uri));
                                   return new ConnectionConfiguration(pool, httpConnection);
                               },
                               ConnectionTimeout = new TimeSpan(0, 10, 0),
                               IndexFormat = env.EnvironmentName + "LogRMS-{0:yyyy.MM}",
                               FailureCallback = e => Console.WriteLine("Unable to submit event " + e.MessageTemplate + e.Exception),
                               EmitEventFailure = EmitEventFailureHandling.WriteToSelfLog |
                                                  EmitEventFailureHandling.WriteToFailureSink |
                                                  EmitEventFailureHandling.RaiseCallback,
                               FailureSink = new LoggerConfiguration().WriteTo
                                             .RollingFile(Path.Combine(Directory.GetCurrentDirectory(), env.EnvironmentName + "LogRMS-{Date}.txt")).CreateLogger()
                           })
                           .CreateLogger();
                    else
                        Log.Logger = new LoggerConfiguration()
                           .Enrich.WithMachineName()
                           .ReadFrom.Configuration(hostingContext.Configuration)
                           .CreateLogger();
                })
                .UseStartup<Startup>()
                .UseSerilog()
                .UseKestrel()
                .UseContentRoot(Directory.GetCurrentDirectory())
                .UseIISIntegration()
                .UseSetting("detailedErrors", "true")
                //.UseUrls("http://0.0.0.0:5005")
                .Build();

            host.Run();

            Log.CloseAndFlush();
        }
    }
}
