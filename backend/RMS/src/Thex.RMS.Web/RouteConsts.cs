﻿namespace Thex.RMS.Web
{
    public class RouteConsts
    {
        public const string UserRouteName = "api/user";
        public const string PropertyPremiseRouteName = "api/propertyPremise";
        public const string PropertyPremiseHeaderRouteName = "api/propertyPremiseHeader";
    }
}
