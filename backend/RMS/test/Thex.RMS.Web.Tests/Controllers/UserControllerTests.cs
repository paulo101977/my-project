﻿
using Shouldly;
using Thex.RMS.Application.Interfaces;
using Thex.RMS.Web.Controllers;
using Tnf.AspNetCore.TestBase;
using Xunit;
using Microsoft.Extensions.DependencyInjection;
using System.Threading.Tasks;
using Thex.RMS.Dto;
using Thex.RMS.Web.Tests.Mocks.Dto;
using Tnf.Dto;
using System.Collections.Generic;

namespace Thex.RMS.Web.Tests.Controllers
{
    public class UserControllerTests : TnfAspNetCoreIntegratedTestBase<StartupControllerTest>
    {
        [Fact]
        public void Should_Resolve_All()
        {
            TnfSession.ShouldNotBeNull();
            ServiceProvider.GetService<UserController>().ShouldNotBeNull();
            ServiceProvider.GetService<IUserAppService>().ShouldNotBeNull();
        }

        [Fact]
        public async Task Should_GetAll()
        {
            var response = await GetResponseAsObjectAsync<List<UserDto>>(
                $"{RouteConsts.UserRouteName}");

            Assert.NotNull(response);
        }
    }
}
