﻿using Shouldly;
using Tnf.AspNetCore.TestBase;
using Tnf.Notifications;
using Xunit;
using Microsoft.Extensions.DependencyInjection;
using Thex.RMS.Infra.Interfaces.ReadRepositories;
using Thex.RMS.Application.Adapters;
using Tnf.Repositories.Uow;
using Thex.Kernel;

namespace Thex.RMS.Web.Tests.Application
{
    public class UserAppServiceTests : TnfAspNetCoreIntegratedTestBase<StartupUnitTest>
    {
        public INotificationHandler notificationHandler;

        public UserAppServiceTests()
        {
            notificationHandler = new NotificationHandler(ServiceProvider);
        }

        [Fact]
        public void Should_Resolve_All()
        {
            TnfSession.ShouldNotBeNull();
            ServiceProvider.GetService<INotificationHandler>().ShouldNotBeNull();
            ServiceProvider.GetService<IUserReadRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<IUserAdapter>().ShouldNotBeNull();
            ServiceProvider.GetService<IApplicationUser>().ShouldNotBeNull();
            ServiceProvider.GetService<IUnitOfWorkManager>().ShouldNotBeNull();
        }
    }
}
