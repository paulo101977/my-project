﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.RMS.Dto;

namespace Thex.RMS.Web.Tests.Mocks.Dto
{
    public class UserDtoMock
    {
        public static UserDto GetDto(int userId = 1)
        {
            return new UserDto()
            {
                UserName = "User " + DateTime.Now
            };
        }

        public static async Task<UserDto> GetDtoAsync()
        {
            var dto = GetDto();
            return await Task.FromResult(dto);
        }

        public static Task<IList<UserDto>> GetDtoListAsync(int userId = 1)
        {
            var ret = new List<UserDto>();

            ret.Add(GetDto(userId));
            ret.Add(GetDto(userId));
            ret.Add(GetDto(userId));

            return Task.FromResult<IList<UserDto>>(ret);
        }
    }
}
