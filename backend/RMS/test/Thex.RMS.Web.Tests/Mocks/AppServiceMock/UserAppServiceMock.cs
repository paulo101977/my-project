﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.RMS.Application.Interfaces;
using Thex.RMS.Dto;
using Thex.RMS.Web.Tests.Mocks.Dto;

namespace Thex.RMS.Web.Tests.Mocks.AppServiceMock
{
    public class UserAppServiceMock : IUserAppService
    {
        public async Task<List<UserDto>> GetAll()
        {
            return (await UserDtoMock.GetDtoListAsync()).MapTo<List<UserDto>>();
        }

        public async Task<UserDto> SendPhoto(Guid id, UserDto dto)
        {
            return (await UserDtoMock.GetDtoAsync());
        }

        public async Task<UserDto> UpdateAsync(Guid id, UserDto dto)
        {
            return (await UserDtoMock.GetDtoAsync());
        }
    }
}
