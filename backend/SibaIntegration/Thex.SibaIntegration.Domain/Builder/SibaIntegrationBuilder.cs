﻿using System;
using Tnf.Builder;
using Tnf.Notifications;

namespace Thex.SibaIntegration.Domain.Entities
{
    public partial class SibaIntegration
    {
        protected internal class Builder : Builder<SibaIntegration>
        {
            public Builder(INotificationHandler notification) : base(notification)
            {
            }

            public Builder(INotificationHandler notification, SibaIntegration instance) : base(notification, instance)
            {
            }

            public virtual Builder WithId(Guid id)
            {
                Instance.Id = id;
                return this;
            }
        }
    }
}
