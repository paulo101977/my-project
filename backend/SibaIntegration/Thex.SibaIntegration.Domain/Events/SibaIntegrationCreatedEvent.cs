﻿using MediatR;
using System.Threading;
using System.Threading.Tasks;
using Thex.SibaIntegration.Domain.Command.Notification;
using Thex.SibaIntegration.Domain.Interfaces;

namespace Thex.SibaIntegration.Domain.Events
{
    public class SibaIntegrationCreatedEvent : INotificationHandler<SibaIntegrationCreatedNotification>
    {
        private ISibaIntegrationCreatedRepository _sibaIntegrationCreatedRepository;

        public SibaIntegrationCreatedEvent(ISibaIntegrationCreatedRepository sibaIntegrationCreatedRepository)
        {
            _sibaIntegrationCreatedRepository = sibaIntegrationCreatedRepository;
        }

        public async Task Handle(SibaIntegrationCreatedNotification notification, CancellationToken cancellationToken)
        {
            await Task.Run(() =>
            {
                // TODO: Gravar os xmls enviados em uma colection do mongo
                _sibaIntegrationCreatedRepository.Create(notification.SibaIntegrationResponseDetails);
            });
        }
    }
}
