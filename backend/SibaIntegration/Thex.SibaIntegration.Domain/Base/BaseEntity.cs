﻿using MediatR;
using System.Collections.Generic;

namespace Thex.SibaIntegration.Domain.Base
{
    public class BaseEntity
    {
        private readonly List<INotification> _events = new List<INotification>();

        public IReadOnlyList<INotification> Events => _events;

        protected void AddEvent(INotification @event)
        {
            _events.Add(@event);
        }
    }
}
