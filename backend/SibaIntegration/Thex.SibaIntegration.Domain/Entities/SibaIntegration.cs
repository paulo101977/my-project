﻿using System;
using Thex.SibaIntegration.Domain.Base;
using Thex.SibaIntegration.Domain.Command.Notification;
using Thex.SibaIntegration.Domain.MongoEntities;
using Tnf.Notifications;

namespace Thex.SibaIntegration.Domain.Entities
{
    public partial class SibaIntegration : BaseEntity
    {
        public SibaIntegration()
        {
        }

        public Guid Id { get; private set; }
        public int PropertyId { get; private set; }
        public int IntegrationFileNumber { get; private set; }

        internal void AddIntegrationCreated(SibaIntegrationResponseDetails sibaIntegrationResponseDetails)
        {
            AddEvent(new SibaIntegrationCreatedNotification(sibaIntegrationResponseDetails));
        }

        static internal Guid GenerateId()
         => Guid.NewGuid();

        protected internal static Builder Create(INotificationHandler handler)
            => new Builder(handler);

        protected internal static Builder Create(INotificationHandler handler, SibaIntegration instance)
            => new Builder(handler, instance);
    }
}
