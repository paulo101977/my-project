﻿using MediatR;
using Microsoft.Extensions.Configuration;
using System.Threading;
using System.Threading.Tasks;
using Thex.Common.Enumerations;
using Thex.Common.Helpers;
using Thex.SibaIntegration.Domain.Adapters;
using Thex.SibaIntegration.Domain.Interfaces;
using Thex.SibaIntegration.Domain.MongoEntities;
using Thex.SibaIntegration.Domain.Utils;
using Tnf.Notifications;
using Tnf.Repositories.Uow;

namespace Thex.SibaIntegration.Domain.Handlers
{
    internal class CreateSibaIntegrationHandler : IRequestHandler<CreateSibaIntegrationCommand, CreateSibaIntegrationCommand>
    {
        private readonly IMediator _mediator;
        private readonly INotificationHandler _notification;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly ISibaIntegrationAdapter _sibaIntegrationAdapter;
        private readonly ISibaIntegrationRepository _sibaIntegrationRepository;
        private readonly IConfiguration _configuration;
        private readonly IClientIntegrationDev _clientIntegrationDev;

        public CreateSibaIntegrationHandler(
            IMediator mediator,
            INotificationHandler notification,
            IUnitOfWorkManager unitOfWorkManager,
            ISibaIntegrationAdapter sibaIntegrationAdapter,
            ISibaIntegrationRepository sibaIntegrationRepository,
            IConfiguration configuration,
            IClientIntegrationDev clientIntegrationDev)
        {
            _mediator = mediator;
            _notification = notification;
            _unitOfWorkManager = unitOfWorkManager;
            _sibaIntegrationAdapter = sibaIntegrationAdapter;
            _sibaIntegrationRepository = sibaIntegrationRepository;
            _configuration = configuration;
            _clientIntegrationDev = clientIntegrationDev;
        }

        public async Task<CreateSibaIntegrationCommand> Handle(CreateSibaIntegrationCommand command, CancellationToken cancellationToken)
        {
            using (var uow = _unitOfWorkManager.Begin())
            {
                dynamic response = null;
                var guestDetails = GetGuesDetails();
                var propertyDetails = await GetPropertyDetails();

                if (EnumHelper.GetEnumValue<EnvironmentEnum>(_configuration.GetValue<string>("CloudEnvironment"), true) == EnvironmentEnum.Development)
                {
                    // TODO: Ajustar request
                    response = await _clientIntegrationDev.PostSibaIntegrationAsync(string.Empty, 0, string.Empty, string.Empty, null  /*MENSAGEM SOAP*/);
                }

                var sibaIntegrationMapper = _sibaIntegrationAdapter.CreateMap(command);

                var integration = sibaIntegrationMapper.Build();

                if (_notification.HasNotification())
                    return command;

                // Registar envio no SQL
                await _sibaIntegrationRepository.Create(integration);

                // Enviar xml para o MongoDB
                // Criar metodo para gerar a entidade
                integration.AddIntegrationCreated(new SibaIntegrationResponseDetails(integration.Id.ToString(),
                    null, /*MENSAGEM SOAP DESERIALIZADA EM JSON*/
                    response)); // response DESERIALIZADo EM JSON);

                await uow.CompleteAsync();

                await PublishEvents(integration);
            }

            return command;
        }

        private dynamic GetGuesDetails()
        {
            return Task.FromResult(new object());
        }

        private dynamic GetPropertyDetails()
        {
            return Task.FromResult(new object());
        }

        private async Task PublishEvents(Entities.SibaIntegration reservation)
        {
            foreach (var @event in reservation.Events)
                await _mediator.Publish((dynamic)@event);
        }
    }
}
