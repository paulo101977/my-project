﻿using SoapProd;
using System.Threading.Tasks;

namespace Thex.SibaIntegration.Domain.Interfaces
{
    public interface IClientIntegrationProd
    {
        Task<EntregaBoletinsAlojamentoResponse> PostSibaIntegrationAsync(string nipc, int property, string acessKey, string details, EntregaBoletinsAlojamentoRequest request);
    }
}
