﻿using SoapDev;
using System.Threading.Tasks;

namespace Thex.SibaIntegration.Domain.Interfaces
{
    public interface IClientIntegrationDev
    {
        Task<EntregaBoletinsAlojamentoResponse> PostSibaIntegrationAsync(string nipc, int property, string acessKey, string details, EntregaBoletinsAlojamentoRequest request);
    }
}
