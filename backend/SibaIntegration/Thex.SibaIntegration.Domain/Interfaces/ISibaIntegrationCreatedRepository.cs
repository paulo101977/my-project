﻿using System.Threading.Tasks;
using Thex.SibaIntegration.Domain.MongoEntities;

namespace Thex.SibaIntegration.Domain.Interfaces
{
    public interface ISibaIntegrationCreatedRepository
    {
        Task Create(SibaIntegrationResponseDetails sibaIntegrationResponseDetails);
    }
}
