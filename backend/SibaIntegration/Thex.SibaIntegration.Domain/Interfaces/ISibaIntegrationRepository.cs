﻿using System.Threading.Tasks;
using Tnf.Repositories;

namespace Thex.SibaIntegration.Domain.Interfaces
{
    public interface ISibaIntegrationRepository : IRepository
    {
        Task<Domain.Entities.SibaIntegration> Create(Domain.Entities.SibaIntegration sibaIntegration);
    }
}
