﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace Thex.SibaIntegration.Domain.Utils
{
    public static class XmlHelper
    {
        #region Privados

        #region Métodos

        /// <summary>
        /// Remove o caracter BOM do início do XML de entrada.
        /// </summary>
        /// <param name="xml">XML.</param>
        private static void RemoveBOMCharfromXML(ref string xml)
        {
            if (xml.Length > 0 && xml[0] != '<')
            {
                xml = xml.Remove(0, 1);
            }
        }

        #endregion

        #endregion

        #region Públicos

        #region Propriedades

        /// <summary>
        /// Mensagem de aviso/erro retornada pelos métodos.
        /// </summary>
        public static string Message
        {
            get;
            private set;
        }

        /// <summary>
        /// Exceção retornada pelos métodos.
        /// </summary>
        public static Exception Exception
        {
            get;
            private set;
        }

        #endregion

        #region Métodos

        /// <summary>
        /// Serializa o objeto do tipo T em XML.
        /// </summary>
        /// <remarks>
        /// O tipo T deve ter um construtor padrão.
        /// </remarks>
        /// <typeparam name="T">Tipo genérico da classe a ser serializada.</typeparam>
        /// <param name="obj">Objeto a ser serializado.</param>
        /// <returns>Representação da classe em formato XML.</returns>
        public static string Serialize<T>(T obj) where T : new()
        {
            var ret = string.Empty;

            Exception = null;
            Message = string.Empty;

            if (obj != null)
            {
                try
                {
                    var ms = new MemoryStream();
                    var tw = new XmlTextWriter(ms, Encoding.UTF8);

                    var xs = new XmlSerializer(typeof(T));
                    xs.Serialize(tw, obj);

                    ms = (MemoryStream)tw.BaseStream;
                    ret = UTF8ToString(ms.ToArray());

                    RemoveBOMCharfromXML(ref ret);
                }
                catch (Exception ex)
                {
                    Exception = ex;
                    Message = ex.Message + ((ex.InnerException != null) ? Environment.NewLine + ex.InnerException.Message : string.Empty);
                }
            }

            return ret;
        }

        /// <summary>
        /// Serializa o objeto do tipo T em XML sem colocar os namespaces no XML.
        /// </summary>
        /// <remarks>
        /// O tipo T deve ter um construtor padrão.
        /// </remarks>
        /// <typeparam name="T">Tipo genérico da classe a ser serializada.</typeparam>
        /// <param name="obj">Objeto a ser serializado.</param>
        /// <returns>Representação da classe em formato XML.</returns>
        public static string SerializeWithoutNS<T>(T obj) where T : new()
        {
            var ret = string.Empty;

            Exception = null;
            Message = string.Empty;

            if (obj != null)
            {
                try
                {
                    var ms = new MemoryStream();
                    var tw = new XmlTextWriter(ms, Encoding.UTF8);

                    var namespaces = new XmlSerializerNamespaces();
                    namespaces.Add("", "");

                    var xs = new XmlSerializer(typeof(T));
                    xs.Serialize(tw, obj, namespaces);

                    ms = (MemoryStream)tw.BaseStream;
                    ret = UTF8ToString(ms.ToArray());

                    RemoveBOMCharfromXML(ref ret);
                }
                catch (Exception ex)
                {
                    Exception = ex;
                    Message = ex.Message + ((ex.InnerException != null) ? Environment.NewLine + ex.InnerException.Message : string.Empty);
                }
            }

            return ret;
        }

        /// <summary>
        /// Retorna o XML deserializado em um objeto do tipo T.
        /// </summary>
        /// <remarks>
        /// O tipo T deve ter um construtor padrão.
        /// </remarks>
        /// <typeparam name="T">Tipo genérico da classe a ser deserializada.</typeparam>
        /// <param name="xml">XML representando o objeto.</param>
        /// <returns>Objeto deserializado.</returns>
        public static T Deserialize<T>(string xml) where T : new()
        {
            T ret = default(T);

            Exception = null;
            Message = string.Empty;

            if (!xml.IsNullOrEmpty() && !xml.IsNullOrWhiteSpace())
            {
                try
                {
                    RemoveBOMCharfromXML(ref xml);
                    var ms = new MemoryStream(StringToUTF8(xml));
                    var xs = new XmlSerializer(typeof(T));
                    ret = (T)xs.Deserialize(ms);
                }
                catch (Exception ex)
                {
                    Exception = ex;
                    Message = ex.Message + ((ex.InnerException != null) ? Environment.NewLine + ex.InnerException.Message : string.Empty);
                    //Log.Write(Message);
                }
            }

            return ret;
        }

        /// <summary>
        /// Serializa o objeto do tipo T em XML.
        /// </summary>
        /// <remarks>
        /// O tipo T deve ter um construtor padrão.
        /// </remarks>
        /// <typeparam name="T">Tipo genérico da classe a ser serializada.</typeparam>
        /// <param name="obj">Objeto a ser serializado.</param>
        /// <param name="encoding">Codificação a ser utilizada no arquivo.</param>
        /// <returns>Representação da classe em formato XML.</returns>
        public static string Serialize<T>(T obj, Encoding encoding) where T : new()
        {
            var ret = string.Empty;

            Exception = null;
            Message = string.Empty;

            if (obj != null)
            {
                try
                {
                    var ms = new MemoryStream();
                    var tw = new XmlTextWriter(ms, encoding);

                    var xs = new XmlSerializer(typeof(T));
                    xs.Serialize(tw, obj);

                    ms = (MemoryStream)tw.BaseStream;
                    ret = EncodingToString(ms.ToArray(), encoding);

                    RemoveBOMCharfromXML(ref ret);
                }
                catch (Exception ex)
                {
                    Exception = ex;
                    Message = ex.Message + ((ex.InnerException != null) ? Environment.NewLine + ex.InnerException.Message : string.Empty);
                }
            }

            return ret;
        }

        ///// <summary>
        ///// Serializa o objeto do tipo T em XML.
        ///// </summary>
        ///// <remarks>
        ///// O tipo T deve ter um construtor padrão.
        ///// </remarks>
        ///// <typeparam name="T">Tipo genérico da classe a ser serializada.</typeparam>
        ///// <param name="obj">Objeto a ser serializado.</param>
        ///// <param name="encoding">Codificação a ser utilizada no arquivo.</param>
        ///// <returns>Representação da classe em formato XML.</returns>
        //public static string Serialize<T>(T obj, Encoding encoding) where T : new()
        //{
        //    var ret = string.Empty;

        //    Exception = null;
        //    Message = string.Empty;

        //    if (obj != null)
        //    {
        //        try
        //        {
        //            var ms = new MemoryStream();
        //            var tw = new XmlTextWriter(ms, encoding);

        //            var xs = new XmlSerializer(typeof(T));
        //            xs.Serialize(tw, obj);

        //            ms = (MemoryStream)tw.BaseStream;
        //            ret = EncodingToString(ms.ToArray(), encoding);

        //            RemoveBOMCharfromXML(ref ret);
        //        }
        //        catch (Exception ex)
        //        {
        //            Exception = ex;
        //            Message = ex.Message + ((ex.InnerException != null) ? Environment.NewLine + ex.InnerException.Message : string.Empty);
        //        }
        //    }

        //    return ret;
        //}

        /// <summary>
        /// Retorna o XML deserializado em um objeto do tipo T.
        /// </summary>
        /// <remarks>
        /// O tipo T deve ter um construtor padrão.
        /// </remarks>
        /// <typeparam name="T">Tipo genérico da classe a ser deserializada.</typeparam>
        /// <param name="xml">XML representando o objeto.</param>
        /// <param name="encoding">Codificação a ser utilizada no arquivo.</param>
        /// <returns>Objeto deserializado.</returns>
        public static T Deserialize<T>(string xml, Encoding encoding) where T : new()
        {
            T ret = default(T);

            Exception = null;
            Message = string.Empty;

            if (!xml.IsNullOrEmpty() && !xml.IsNullOrWhiteSpace())
            {
                try
                {
                    RemoveBOMCharfromXML(ref xml);
                    var ms = new MemoryStream(StringToEncoding(xml, encoding));
                    var xs = new XmlSerializer(typeof(T));
                    ret = (T)xs.Deserialize(ms);
                }
                catch (Exception ex)
                {
                    Exception = ex;
                    Message = ex.Message + ((ex.InnerException != null) ? Environment.NewLine + ex.InnerException.Message : string.Empty);
                }
            }

            return ret;
        }

        /// <summary>
        /// Converte um vetor de bytes na codificação UTF8 em String.
        /// </summary>
        /// <param name="content">Conteúdo a ser convertido.</param>
        /// <returns>String representando o conteúdo.</returns>
        public static string UTF8ToString(byte[] content)
        {
            var ret = new UTF8Encoding().GetString(content, 0, content.Length);

            return ret;
        }

        /// <summary>
        /// Converte uma String em um vetor de bytes na codificação UTF8.
        /// </summary>
        /// <param name="content">Conteúdo a ser convertido.</param>
        /// <returns>Vetor representando o conteúdo.</returns>
        public static byte[] StringToUTF8(string content)
        {
            var ret = new UTF8Encoding().GetBytes(content);

            return ret;
        }

        /// <summary>
        /// Converte um vetor de bytes na codificação especificada em String.
        /// </summary>
        /// <param name="content">Conteúdo a ser convertido.</param>
        /// <param name="encoding">Codificação a ser utilizada para conversão.</param>
        /// <returns>String representando o conteúdo.</returns>
        public static string EncodingToString(byte[] content, Encoding encoding)
        {
            var ret = encoding.GetString(content, 0, content.Length);

            return ret;
        }

        /// <summary>
        /// Converte uma String em um vetor de bytes na codificação especificada.
        /// </summary>
        /// <param name="content">Conteúdo a ser convertido.</param>
        /// <param name="encoding">Codificação a ser utilizada para conversão.</param>
        /// <returns>Vetor representando o conteúdo.</returns>
        public static byte[] StringToEncoding(string content, Encoding encoding)
        {
            var ret = encoding.GetBytes(content);

            return ret;
        }

        #endregion

        #endregion
    }
}
