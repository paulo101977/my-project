﻿using Tnf.MongoDb.Entities;

namespace Thex.SibaIntegration.Domain.MongoEntities
{
    public class SibaIntegrationResponseDetails : IMongoDbEntity<string>
    {
        public SibaIntegrationResponseDetails(string id, dynamic requestDetails, dynamic requestResponse)
        {
            this.Id = id;
            this.RequestDetails = requestDetails;
            this.RequestResponse = requestResponse;
        }

        public string Id { get; set; }
        public dynamic RequestDetails { get; set; }
        public dynamic RequestResponse { get; set; }
    }
}
