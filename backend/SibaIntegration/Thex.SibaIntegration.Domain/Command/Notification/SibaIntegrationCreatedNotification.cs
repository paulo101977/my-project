﻿using MediatR;
using Thex.SibaIntegration.Domain.MongoEntities;

namespace Thex.SibaIntegration.Domain.Command.Notification
{
    public class SibaIntegrationCreatedNotification : INotification
    {
        public SibaIntegrationResponseDetails SibaIntegrationResponseDetails { get; private set; }

        public SibaIntegrationCreatedNotification(SibaIntegrationResponseDetails sibaIntegrationResponseDetails)
        {
            SibaIntegrationResponseDetails = sibaIntegrationResponseDetails;
        }
    }
}
