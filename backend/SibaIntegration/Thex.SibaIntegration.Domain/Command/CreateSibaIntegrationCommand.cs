﻿using MediatR;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Thex.SibaIntegration.Domain
{
    [XmlRoot("MovimentoBAL", Namespace = "http://sef.pt/BAws")]
    public class CreateSibaIntegrationCommand : IRequest<CreateSibaIntegrationCommand>
    {
        [XmlElement(ElementName = "Unidade_Hoteleira")]
        public Unidade_Hoteleira Property { get; set; }

        [XmlElement(ElementName = "Boletim_Alojamento")]
        public List<Boletim_Alojamento> GuestList { get; set; }

        [XmlElement(ElementName = "Envio")]
        public Envio SibaIntegrationInfo { get; set; }
    }

    public class Unidade_Hoteleira
    {
        [XmlElement(ElementName = "Codigo_Unidade_Hoteleira")]
        public long Code { get; set; }

        [XmlElement(ElementName = "Estabelecimento")]
        public int Number { get; set; }

        [XmlElement(ElementName = "Nome")]
        public string Name { get; set; }

        [XmlElement(ElementName = "Abreviatura")]
        public string NameAbbreviation { get; set; }

        [XmlElement(ElementName = "Morada")]
        public string Address { get; set; }

        [XmlElement(ElementName = "Localidade")]
        public string Location { get; set; }

        [XmlElement(ElementName = "Codigo_Postal")]
        public int PostalCode { get; set; }

        [XmlElement(ElementName = "Zona_Postal")]
        public int PostalZone { get; set; }

        [XmlElement(ElementName = "Telefone")]
        public long PhoneNumber { get; set; }

        private string _fax;

        [XmlElement(ElementName = "Fax", IsNullable = true)]
        public string Fax
        {
            get { return _fax == null ? string.Empty : _fax; }
            set { _fax = value; }
        }

        [XmlElement(ElementName = "Nome_Contacto")]
        public string ContactName { get; set; }

        [XmlElement(ElementName = "Email_Contacto")]
        public string ContactEmail { get; set; }
    }

    public class Boletim_Alojamento
    {
        [XmlElement(ElementName = "Apelido")]
        public string LastName { get; set; }

        [XmlElement(ElementName = "Nome")]
        public string FirstName { get; set; }

        [XmlElement(ElementName = "Nacionalidade")]
        public string Nationality { get; set; }

        [XmlElement(ElementName = "Data_Nascimento")]
        public DateTime DateOfBirth { get; set; }

        [XmlElement(ElementName = "Local_Nascimento")]
        public string BirthPlace { get; set; }

        [XmlElement(ElementName = "Documento_Identificacao")]
        public string DocumentInformation { get; set; }

        [XmlElement(ElementName = "Pais_Emissor_Documento")]
        public string DocumentIssuingCountry { get; set; }

        [XmlElement(ElementName = "Tipo_Documento")]
        public string TypeDocumentIdentification { get; set; }

        [XmlElement(ElementName = "Data_Entrada")]
        public DateTime ArrivalDate { get; set; }

        [XmlElement(ElementName = "Data_Saida")]
        public DateTime? DepartureDate { get; set; }

        [XmlElement(ElementName = "Pais_Residencia_Origem")]
        public string CountryResidenceOrigin { get; set; }

        [XmlElement(ElementName = "Local_Residencia_Origem")]
        public string Location { get; set; }
    }

    public class Envio
    {
        [XmlElement(ElementName = "Numero_Ficheiro")]
        public int IntegrationFileNumber { get; set; }

        [XmlElement(ElementName = "Data_Movimento")]
        public DateTime IntegrationDate { get; set; }

        [XmlIgnore]
        public string AccessKey { get; set; }
    }
}