﻿using Thex.SibaIntegration.Domain.Adapters;
using Tnf;
using Tnf.Notifications;

namespace Thex.SibaIntegration.Domain.Entities
{
    internal class SibaIntegrationAdapter : ISibaIntegrationAdapter
    {
        public INotificationHandler _notificationHandler { get; }

        public SibaIntegrationAdapter(INotificationHandler notificationHandler)
        {
            _notificationHandler = notificationHandler;
        }

        public SibaIntegration.Builder CreateMap(CreateSibaIntegrationCommand command)
        {
            Check.NotNull(command, nameof(command));

            var builder = new SibaIntegration.Builder(_notificationHandler)
               .WithId(SibaIntegration.GenerateId());

            return builder;
        }
    }
}
