﻿namespace Thex.SibaIntegration.Domain.Adapters
{
    internal interface ISibaIntegrationAdapter
    {
        Domain.Entities.SibaIntegration.Builder CreateMap(CreateSibaIntegrationCommand command);
    }
}
