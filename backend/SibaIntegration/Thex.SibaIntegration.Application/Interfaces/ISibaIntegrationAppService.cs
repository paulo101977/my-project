﻿using System.Threading.Tasks;
using Thex.SibaIntegration.Application.Dto;
using Thex.SibaIntegration.Domain;
using Tnf.Application.Services;

namespace Thex.SibaIntegration.Application.Interfaces
{
    public interface ISibaIntegrationAppService: IApplicationService
    {
        Task<SibaIntegrationResponseBodyDto> CreateIntegration(CreateSibaIntegrationCommand command);
    }
}
