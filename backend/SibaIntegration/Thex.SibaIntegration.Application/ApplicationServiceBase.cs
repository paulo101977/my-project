﻿using Tnf.Application.Services;
using Tnf.Notifications;

namespace Thex.SibaIntegration.Application
{
    public abstract class ApplicationServiceBase: ApplicationService
    {
        protected ApplicationServiceBase(INotificationHandler notification) : base(notification)
        {
        }
    }
}
