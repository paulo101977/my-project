﻿using System.Xml.Serialization;

namespace Thex.SibaIntegration.Application.Dto
{
    [XmlRoot("ErrosBA", Namespace = "http://www.sef.pt/BAws")]
    public class SibaIntegrationResponseDto
    {
        [XmlElement(ElementName = "RetornoBA")]
        public SibaIntegrationResponseBodyDto sibaIntegrationResponseBodyDto { get; set; }
    }

    public class SibaIntegrationResponseBodyDto
    {
        [XmlElement(ElementName = "Codigo_Retorno")]
        public int Code { get; set; }

        [XmlElement(ElementName = "Descricao")]
        public string Description { get; set; }
    }
}
