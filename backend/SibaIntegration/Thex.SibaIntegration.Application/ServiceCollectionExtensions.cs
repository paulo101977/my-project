﻿using Microsoft.Extensions.DependencyInjection;
using Thex.SibaIntegration.Application.Interfaces;
using Thex.SibaIntegration.Domain;
using Thex.SibaIntegration.Infra;

namespace Thex.SibaIntegration.Application
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddApplicationServiceDependency(this IServiceCollection services)
        {
            services
                .AddDomainDependency()
                .AddInfraDependency();

            services.AddScoped<ISibaIntegrationAppService, SibaIntegrationAppService>();

            return services;
        }
    }
}
