﻿using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;
using System.Xml;
using Thex.Common.Enumerations;
using Thex.Common.Helpers;
using Thex.SibaIntegration.Application.Dto;
using Thex.SibaIntegration.Application.Interfaces;
using Thex.SibaIntegration.Domain;
using Thex.SibaIntegration.Domain.Interfaces;
using Thex.SibaIntegration.Domain.Utils;
using Tnf.Notifications;

namespace Thex.SibaIntegration.Application
{
    public class SibaIntegrationAppService : ApplicationServiceBase, ISibaIntegrationAppService
    {
        private readonly IConfiguration _configuration;
        private readonly INotificationHandler _notification;
        private readonly IClientIntegrationDev _clientIntegrationDev;
        private readonly IClientIntegrationProd _clientIntegrationProd;

        public SibaIntegrationAppService(
           INotificationHandler notification,
           IConfiguration configuration,
           IClientIntegrationDev clientIntegrationDev,
           IClientIntegrationProd clientIntegrationProd) : base(notification)
        {
            _configuration = configuration;
            _notification = notification;
            _clientIntegrationDev = clientIntegrationDev;
            _clientIntegrationProd = clientIntegrationProd;
        }

        public async Task<SibaIntegrationResponseBodyDto> CreateIntegration(CreateSibaIntegrationCommand command)
        {
            switch (EnumHelper.GetEnumValue<EnvironmentEnum>(_configuration.GetValue<string>("CloudEnvironment"), true))
            {
                case EnvironmentEnum.Production:
                    return await CreateIntegrationInProd(command);
                default:
                    return await CreateIntegrationInDev(command);
            }
        }

        private async Task<SibaIntegrationResponseBodyDto> CreateIntegrationInDev(CreateSibaIntegrationCommand command)
        {
            if (command.Property != null)
            {
                command.Property.Code = _configuration.GetValue<long>("CodigoUnidadeHoteleira");
                command.Property.Number = _configuration.GetValue<int>("Estabelecimento");
            }

            var accessKey = _configuration.GetValue<string>("AccessKey");
            var xml_boletins = GetXml(command);

            var response = await _clientIntegrationDev.PostSibaIntegrationAsync(command.Property.Code.ToString(), 00, accessKey, xml_boletins, null);

            SibaIntegrationResponseDto responseDto = new SibaIntegrationResponseDto();

            if (response.Body != null && response.Body.EntregaBoletinsAlojamentoResult == "0")
            {
                responseDto = GetResponseWithSuccess();
            }
            else
                responseDto = XmlHelper.Deserialize<SibaIntegrationResponseDto>(response.Body.EntregaBoletinsAlojamentoResult);

            return responseDto?.sibaIntegrationResponseBodyDto;
        }

        private async Task<SibaIntegrationResponseBodyDto> CreateIntegrationInProd(CreateSibaIntegrationCommand command)
        {
            var accessKey = command.SibaIntegrationInfo.AccessKey;
            var xml_boletins = GetXml(command);

            var response = await _clientIntegrationProd.PostSibaIntegrationAsync(command.Property.Code.ToString(), 00, accessKey, xml_boletins, null);

            SibaIntegrationResponseDto responseDto = new SibaIntegrationResponseDto();

            if (response.Body != null && response.Body.EntregaBoletinsAlojamentoResult == "0")
                responseDto = GetResponseWithSuccess();
            else
                responseDto = XmlHelper.Deserialize<SibaIntegrationResponseDto>(response.Body.EntregaBoletinsAlojamentoResult);

            return responseDto?.sibaIntegrationResponseBodyDto;
        }

        private string GetXml(CreateSibaIntegrationCommand command)
        {
            var doc = new XmlDocument();
            doc.LoadXml(XmlHelper.Serialize(command));

            XmlNode root = doc.DocumentElement;

            if (root.HasChildNodes)
            {
                for (int i = 0; i < root.ChildNodes.Count; i++)
                {
                    if (root.ChildNodes[i].LocalName == "Boletim_Alojamento")
                    {
                        for (int j = 0; j < root.ChildNodes[i].ChildNodes.Count; j++)
                        {
                            var child = root.ChildNodes[i].ChildNodes[j];

                            if (child != null && child.LocalName == "Data_Saida"
                                && (string.IsNullOrEmpty(child.InnerText) || string.IsNullOrWhiteSpace(child.InnerText)))
                            {
                                var text = child.InnerText;
                                root.ChildNodes[i].RemoveChild(child);
                                break;
                            }
                        }
                    }
                }
            }

            return doc.InnerXml;
        }

        private SibaIntegrationResponseDto GetResponseWithSuccess()
        {
            return new SibaIntegrationResponseDto
            {
                sibaIntegrationResponseBodyDto = new SibaIntegrationResponseBodyDto
                {
                    Code = 0
                }
            };
        }
    }
}
