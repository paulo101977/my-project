﻿namespace Thex.SibaIntegration.Web
{
    public class RouteConsts
    {
        public const string Integration = "api/SibaIntegration";
    }

    public class RouteResponseConsts
    {
        public const string Integration = "SibaIntegration";
    }
}
