﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Thex.SibaIntegration.Application.Interfaces;
using Thex.SibaIntegration.Domain;
using Tnf.AspNetCore.Mvc.Response;

namespace Thex.SibaIntegration.Web.Controllers
{
    [Route(RouteConsts.Integration)]
    public class IntegrationController : TnfController
    {
        private ISibaIntegrationAppService _sibaIntegrationAppService;
        public IntegrationController(ISibaIntegrationAppService sibaIntegrationAppService)
        {
            _sibaIntegrationAppService = sibaIntegrationAppService;
        }

        [HttpPost]
        [ProducesResponseType(typeof(CreateSibaIntegrationCommand), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Post([FromBody] CreateSibaIntegrationCommand command)
        {
            var response = await _sibaIntegrationAppService.CreateIntegration(command);

            return CreateResponseOnPost(response, RouteResponseConsts.Integration);
        }
    }
}