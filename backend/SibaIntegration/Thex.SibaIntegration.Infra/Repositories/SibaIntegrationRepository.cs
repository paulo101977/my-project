﻿using System.Threading.Tasks;
using Thex.SibaIntegration.Domain.Interfaces;
using Thex.SibaIntegration.Infra.Context;
using Tnf.EntityFrameworkCore;
using Tnf.EntityFrameworkCore.Repositories;

namespace Thex.SibaIntegration.Infra.Repositories
{
    public class SibaIntegrationRepository : EfCoreRepositoryBase<ThexSibaIntegrationContext, Domain.Entities.SibaIntegration>, ISibaIntegrationRepository
    {
        public SibaIntegrationRepository(IDbContextProvider<ThexSibaIntegrationContext> dbContextProvider)
           : base(dbContextProvider)
        {
        }

        public Task<Domain.Entities.SibaIntegration> Create(Domain.Entities.SibaIntegration sibaIntegration)
        {
            // TODO: Implementar create
            return Task.FromResult(new Domain.Entities.SibaIntegration());
        }
    }
}
