﻿using System.Threading.Tasks;
using Thex.SibaIntegration.Domain.Interfaces;
using Thex.SibaIntegration.Domain.MongoEntities;
using Tnf.MongoDb.Provider;
using Tnf.MongoDb.Repositories;

namespace Thex.SibaIntegration.Infra.Repositories
{
    public class SibaCreatedRepository : MongoDbRepository<SibaIntegrationResponseDetails, string>, ISibaIntegrationCreatedRepository
    {
        public SibaCreatedRepository(IMongoDbProvider provider) : base(provider)
        {

        }

        public async Task Create(SibaIntegrationResponseDetails sibaIntegrationResponseDetails)
        {
            await InsertAsync(sibaIntegrationResponseDetails);
        }
    }
}
