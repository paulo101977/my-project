﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Thex.SibaIntegration.Infra.Mappers
{
    public class SibaIntegrationMapper : IEntityTypeConfiguration<Domain.Entities.SibaIntegration>
    {
        public void Configure(EntityTypeBuilder<Domain.Entities.SibaIntegration> mapper)
        {
            mapper.ToTable("SibaIntegration");
            mapper.HasAnnotation("Relational:TableName", "SibaIntegration");

            mapper.HasKey(e => e.Id)
                    .HasName("Id");

            mapper.Property(e => e.IntegrationFileNumber)
                .IsRequired()
                .HasColumnName("IntegrationFileNumber");
        }
    }
}
