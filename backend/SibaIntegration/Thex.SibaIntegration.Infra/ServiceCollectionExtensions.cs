﻿using Microsoft.Extensions.DependencyInjection;
using Thex.SibaIntegration.Domain.Interfaces;

namespace Thex.SibaIntegration.Infra
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddInfraDependency(this IServiceCollection services)
        {
            services.AddScoped<IClientIntegrationDev, IntegrationClientDev>();
            services.AddScoped<IClientIntegrationProd, IntegrationClientProd>();

            return services;
        }
    }
}
