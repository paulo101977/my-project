﻿using Microsoft.EntityFrameworkCore;
using Thex.SibaIntegration.Infra.Mappers;
using Tnf.EntityFrameworkCore;
using Tnf.Runtime.Session;

namespace Thex.SibaIntegration.Infra.Context
{
    public class ThexSibaIntegrationContext : TnfDbContext
    {
        public ThexSibaIntegrationContext(DbContextOptions<ThexSibaIntegrationContext> options, ITnfSession session)
            : base(options, session)
        {

        }

        public DbSet<Domain.Entities.SibaIntegration> Integration { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new SibaIntegrationMapper());

            base.OnModelCreating(modelBuilder);
        }
    }
}
