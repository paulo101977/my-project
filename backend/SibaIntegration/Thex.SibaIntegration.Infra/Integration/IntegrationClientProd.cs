﻿using SoapProd;
using System;
using System.Text;
using System.Threading.Tasks;
using Thex.SibaIntegration.Domain.Interfaces;

namespace Thex.SibaIntegration.Infra
{
    public class IntegrationClientProd : IClientIntegrationProd
    {
        public async Task<EntregaBoletinsAlojamentoResponse> PostSibaIntegrationAsync(string nipc, int property, string acessKey, string details,
                       EntregaBoletinsAlojamentoRequest request)
        {
            var ret = new EntregaBoletinsAlojamentoResponse();

            try
            {
                using (var client = new BoletinsAlojamentoSoapClient(new BoletinsAlojamentoSoapClient.EndpointConfiguration()))
                {
                    var encodind = new UTF8Encoding();
                    var bytes = encodind.GetBytes(details);
                    ret = await client.EntregaBoletinsAlojamentoAsync(nipc, property, acessKey, Convert.ToBase64String(bytes, 0, bytes.Length));
                }
            }
            catch(Exception ex)
            {
                // TODO: Enviar erro para o GenericLog
            }

            return ret;
        }
    }
}
