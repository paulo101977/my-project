﻿CREATE TABLE [dbo].[ReservationBudget]
(
	[Id]				UNIQUEIDENTIFIER NOT NULL, 
    [ReservationId]		UNIQUEIDENTIFIER NOT NULL, 
	[Day]				DATETIME		NOT NULL,
    [Budget]			DECIMAL			NOT NULL,

	CONSTRAINT [PK_ReservationBudget] PRIMARY KEY ([Id]),
	CONSTRAINT [FK_ReservationBudget_Reservation] FOREIGN KEY ([ReservationId]) REFERENCES [dbo].[Reservation] ([Id]),
	CONSTRAINT [UK_ReservationBudget] UNIQUE ([ReservationId], [Day])
)
GO
