﻿CREATE TABLE [dbo].[Reservation]
(
	[Id]				UNIQUEIDENTIFIER NOT NULL, 
    [GuestName]			NVARCHAR(150) NOT NULL, 
    [Code]				NVARCHAR(150) NOT NULL, 
    [RoomCode]			NVARCHAR(150) NOT NULL, 
    [NumberOfGuests]	INT	NOT NULL, 
    [Checkin]			DATETIME	NOT NULL,
    [Checkout]			DATETIME	NOT NULL,

	CONSTRAINT [PK_Reservation] PRIMARY KEY ([Id]),
	CONSTRAINT [UK_ReservationCode] UNIQUE (Code)
)
GO
