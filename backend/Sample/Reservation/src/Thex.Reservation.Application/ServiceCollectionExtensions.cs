﻿using Microsoft.Extensions.Configuration;
using Thex.Reservation.Application.Interfaces;
using Thex.Reservation.Application.Services;

namespace Microsoft.Extensions.DependencyInjection
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddApplicationServiceDependency(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddInfraDependency(configuration);
            services.AddDomainDependency();

            services.AddTransient<IReservationAppService, ReservationAppService>();
            
            return services;
        }
    }
}