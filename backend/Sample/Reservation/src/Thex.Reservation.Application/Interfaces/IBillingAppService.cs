﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Thex.CMNETIntegration.Dto;
using Tnf.Application.Services;

namespace Thex.CMNETIntegration.Application.Interfaces
{
    public interface IBillingAppService : IApplicationService
    {
        Task<IEnumerable<CreditCardDto>> GetCreditCardPayment(int propertyId, DateTime itemDate);   
    }
}
