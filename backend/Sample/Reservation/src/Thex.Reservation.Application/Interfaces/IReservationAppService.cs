﻿using Tnf.Application.Services;
using Thex.Reservation.Domain.Command;
using System.Threading.Tasks;
using Thex.Reservation.Dto;
using System;
using Tnf.Dto;

namespace Thex.Reservation.Application.Interfaces
{
    public interface IReservationAppService : IApplicationService
    {
        Task<IListDto<ReservationDto>> GetAll();
        Task<ReservationDto> GetById(Guid id);
        Task<CreateReservationCommand> CreateReservation(CreateReservationCommand command);
    }
}
