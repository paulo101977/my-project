﻿namespace Thex.Reservation.Domain
{
    public enum EmailNotificationEnum
    {
        EmailAdministrator = 1,
        EmailGuest = 2
    }
}
