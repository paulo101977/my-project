﻿using System;
using Thex.Common;
using Thex.Reservation.Domain.Entities;
using Tnf.Builder;
using Tnf.Notifications;
using Tnf.Specifications;

namespace Thex.Reservation.Domain
{
    public partial class Reservation
    {
        protected internal class Builder : Builder<Reservation>
        {
            public Builder(INotificationHandler notification) : base(notification)
            {
            }

            public Builder(INotificationHandler notification, Reservation instance) : base(notification, instance)
            {
            }

            public virtual Builder WithId(Guid id)
            {
                Instance.Id = id;
                return this;
            }

            public virtual Builder WithGuestName(string guestName)
            {
                Instance.GuestName = guestName;
                return this;
            }

            public virtual Builder WithCode(string code)
            {
                Instance.Code = code;
                return this;
            }

            public virtual Builder WithRoomCode(string roomCode)
            {
                Instance.RoomCode = roomCode;
                return this;
            }

            public virtual Builder WithNewCode()
            {
                Instance.GenerateReservationNumber();
                return this;
            }

            public virtual Builder WithNumberOfGuests(int numberOfGuests)
            {
                Instance.NumberOfGuests = numberOfGuests;
                return this;
            }

            public virtual Builder WithPeriod(DateTime checkin, DateTime checkout)
            {
                Instance.LengthOfPeriod = Period.Create(checkin, checkout, Notification);
                return this;
            }

            public virtual Builder WithBudget(ReservationBudget budget)
            {
                Instance.AddBudget(budget);
                return this;
            }

            public virtual Builder WithReservationCreatedEmailEvents()
            {
                Instance.AddReservationCreatedAdminEmailEvent();
                Instance.AddReservationCreatedGuestEmailEvent();
                return this;
            }

            public virtual Builder WithReservationCreatedCashOperationEvent()
            {
                Instance.AddReservationCreatedCashOperationEvent();
                return this;
            }

            protected override void Specifications()
            {
                AddSpecification(new ExpressionSpecification<Reservation>(
                    AppConsts.LocalizationSourceName,
                    EntityError.ReservationMustHaveId,
                    w => w.Id != default(Guid)));

                AddSpecification(new ExpressionSpecification<Reservation>(
                    AppConsts.LocalizationSourceName,
                    EntityError.ReservationMustHaveCode,
                    w => !string.IsNullOrWhiteSpace(w.Code)));

                AddSpecification(new ExpressionSpecification<Reservation>(
                    AppConsts.LocalizationSourceName,
                    EntityError.ReservationOutOfBoundCode,
                    w => string.IsNullOrWhiteSpace(w.Code) || w.Code.Length > 0 && w.Code.Length <= 150));

                AddSpecification(new ExpressionSpecification<Reservation>(
                    AppConsts.LocalizationSourceName,
                    EntityError.ReservationMustRoomCode,
                    w => !string.IsNullOrWhiteSpace(w.RoomCode)));
                
                AddSpecification(new ExpressionSpecification<Reservation>(
                    AppConsts.LocalizationSourceName,
                    EntityError.ReservationOutOfBoundRoomCode,
                    w => string.IsNullOrWhiteSpace(w.RoomCode) || w.RoomCode.Length > 0 && w.RoomCode.Length <= 150));

                AddSpecification(new ExpressionSpecification<Reservation>(
                    AppConsts.LocalizationSourceName,
                    EntityError.ReservationMustHaveGuestName,
                    w => !string.IsNullOrWhiteSpace(w.Code)));

                AddSpecification(new ExpressionSpecification<Reservation>(
                    AppConsts.LocalizationSourceName,
                    EntityError.ReservationOutOfBoundGuestName,
                    w => string.IsNullOrWhiteSpace(w.Code) || w.Code.Length > 0 && w.Code.Length <= 150));

                AddSpecification(new ExpressionSpecification<Reservation>(
                    AppConsts.LocalizationSourceName,
                    EntityError.ReservationMustHaveNumberOfGuests,
                    w => w.NumberOfGuests != default(int)));
            }
        }
    }
}
