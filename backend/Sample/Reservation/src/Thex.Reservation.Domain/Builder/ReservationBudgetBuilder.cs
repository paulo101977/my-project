﻿using System;
using System.Collections.Generic;
using System.Text;
using Thex.Reservation.Domain.Entities;
using Tnf.Builder;
using Tnf.Notifications;

namespace Thex.Reservation.Domain
{
    public partial class ReservationBudget
    {
        protected internal class Builder : Builder<ReservationBudget>
        {
            public Builder(INotificationHandler handler) : base(handler)
            {
            }

            public Builder(INotificationHandler handler, ReservationBudget instance) : base(handler, instance)
            {
            }

            public virtual Builder WithId(Guid id)
            {
                Instance.Id = id;
                return this;
            }

            public virtual Builder WithReservationId(Guid reservationId)
            {
                Instance.ReservationId = reservationId;
                return this;
            }

            public virtual Builder WithDay(DateTime day)
            {
                Instance.Day = day;
                return this;
            }

            public virtual Builder WithBudget(decimal budget)
            {
                Instance.Budget = budget;
                return this;
            }

            protected override void Specifications()
            {
                //AddSpecification(new ExpressionSpecification<Reservation>(
                //    AppConsts.LocalizationSourceName,
                //    Reservation.EntityError.ReservationMustHaveReservationCode,
                //    w => !string.IsNullOrWhiteSpace(w.ReservationCode)));
            }
        }
    }
}
