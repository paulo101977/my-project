﻿using System;

namespace Thex.Reservation.Domain
{
    public partial class ReservationBudget
    {
        static internal Guid GenerateId()
         => Guid.NewGuid();

        public Guid Id { get; private set; }
        public Guid ReservationId { get; private set; }
        public DateTime Day { get; private set; }
        public decimal Budget { get; private set; }

        public virtual Reservation Reservation { get; internal set; }
    }
}
