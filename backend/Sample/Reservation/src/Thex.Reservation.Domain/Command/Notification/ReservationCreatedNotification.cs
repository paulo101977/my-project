﻿using MediatR;

namespace Thex.Reservation.Domain.Notification
{
    public class ReservationCreatedNotification : INotification
    {
        public Reservation Reservation { get; private set; }

        public ReservationCreatedNotification(Reservation reservation)
        {
            Reservation = reservation;
        }
    }
}