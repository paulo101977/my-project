﻿using System;
using System.Threading.Tasks;
using Thex.Reservation.Dto;
using Tnf.Dto;

namespace Thex.Reservation.Domain.Interfaces
{
    public interface IReservationReadRepository
    {
        Task<bool> CheckAvailabilityByFilters(string roomCode, DateTime checkin, DateTime checkout);
        Task<IListDto<ReservationDto>> GetAll();
        Task<ReservationDto> GetById(Guid id);
    }
}
