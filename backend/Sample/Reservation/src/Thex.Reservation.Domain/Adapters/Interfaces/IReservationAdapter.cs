﻿using Thex.Reservation.Domain.Command;

namespace Thex.Reservation.Domain
{
    internal interface IReservationAdapter
    {
        Reservation.Builder CreateMap(CreateReservationCommand command);
    }
}
