﻿using System;

namespace Thex.Reservation.Domain
{
    internal interface IReservationBudgetAdapter
    {
        ReservationBudget.Builder CreateMap(Guid reservationId, DateTime day, decimal budget);
    }
}
