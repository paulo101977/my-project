﻿using Thex.Reservation.Domain.Command;
using Tnf;
using Tnf.Notifications;

namespace Thex.Reservation.Domain
{
    internal class ReservationAdapter : IReservationAdapter
    {
        public INotificationHandler _notificationHandler { get; }
        public IReservationBudgetAdapter _reservationBudgetAdapter { get; }

        public ReservationAdapter(INotificationHandler notificationHandler, IReservationBudgetAdapter reservationBudgetAdapter)
        {
            _notificationHandler = notificationHandler;
            _reservationBudgetAdapter = reservationBudgetAdapter;
        }

        public virtual Reservation.Builder CreateMap(CreateReservationCommand command)
        {
            Check.NotNull(command, nameof(command));

            var id = Reservation.GenerateId();

            var builder = new Reservation.Builder(_notificationHandler)
                .WithId(id)
                .WithPeriod(command.Checkin, command.Checkout)
                .WithNumberOfGuests(command.NumberOfGuests)
                .WithGuestName(command.GuestName)
                .WithRoomCode(command.RoomCode)
                .WithNewCode();

            foreach (var commandBudget in command.BudgetList)
            {
                var budgetBuilder = _reservationBudgetAdapter.CreateMap(id, commandBudget.Day, commandBudget.Budget);
                var budget = budgetBuilder.Build();

                if (_notificationHandler.HasNotification())
                    continue;

                builder.WithBudget(budget);
            }

            builder.WithReservationCreatedEmailEvents();
            builder.WithReservationCreatedCashOperationEvent();

            return builder;
        }
    }
}
