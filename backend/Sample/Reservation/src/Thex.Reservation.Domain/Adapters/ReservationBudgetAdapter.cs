﻿using System;
using Thex.Reservation.Domain.Command;
using Tnf;
using Tnf.Notifications;

namespace Thex.Reservation.Domain
{
    internal class ReservationBudgetAdapter : IReservationBudgetAdapter
    {
        public INotificationHandler NotificationHandler { get; }

        public ReservationBudgetAdapter(INotificationHandler notificationHandler)
        {
            NotificationHandler = notificationHandler;
        }

        public virtual ReservationBudget.Builder CreateMap(Guid reservationId, DateTime day, decimal budget)
        {
            var id = ReservationBudget.GenerateId();

            var builder = new ReservationBudget.Builder(NotificationHandler)
                .WithId(id)
                .WithReservationId(reservationId)
                .WithDay(day)
                .WithBudget(budget);

            return builder;
        }
    }
}
