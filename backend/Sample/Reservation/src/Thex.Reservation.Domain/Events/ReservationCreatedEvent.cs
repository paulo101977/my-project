﻿using MediatR;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Thex.Reservation.Domain.Interfaces;
using Thex.Reservation.Domain.Notification;

namespace Thex.Reservation.Domain.Events
{
    public class ReservationCreatedEvent : INotificationHandler<ReservationCreatedNotification>
    {
        private IReservationRepository _reservationRepository { get; set; }

        public ReservationCreatedEvent(IReservationRepository reservationRepository)
        {
            _reservationRepository = reservationRepository;
        }

        public async Task Handle(ReservationCreatedNotification notification, CancellationToken cancellationToken)
        {
            await Task.Run(() =>
            {
                _reservationRepository.FakeReservationDataSync(notification.Reservation);
            });
        }
    }
}
