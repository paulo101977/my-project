﻿using MediatR;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Thex.Reservation.Domain.Notification;
using Tnf.Localization;

namespace Thex.Reservation.Domain.Events
{
    public class SendEmailEvent : INotificationHandler<ReservationCreatedAdminEmailNotification>,
                                    INotificationHandler<ReservationCreatedGuestEmailNotification>
    {
        private readonly ILocalizationManager _localizationManager;

        public SendEmailEvent(ILocalizationManager localizationManager)
        {
            _localizationManager = localizationManager;
        }

        public async Task Handle(ReservationCreatedAdminEmailNotification notification, CancellationToken cancellationToken)
        {
            await Task.Run(() =>
            {
                using (StreamWriter writer = System.IO.File.AppendText("adminemail.txt"))
                {
                    writer.WriteLine(notification.ToString(_localizationManager));
                }
            });
        }

        public async Task Handle(ReservationCreatedGuestEmailNotification notification, CancellationToken cancellationToken)
        {
            await Task.Run(() =>
            {
                using (StreamWriter writer = System.IO.File.AppendText("guestemail.txt"))
                {
                    writer.WriteLine(notification.ToString(_localizationManager));
                }
            });
        }
    }
}
