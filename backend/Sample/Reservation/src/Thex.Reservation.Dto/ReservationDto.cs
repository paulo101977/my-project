﻿using System;
using Tnf.Dto;

namespace Thex.Reservation.Dto
{
    public class ReservationDto : BaseDto
    {
        public Guid Id { get; set; }
        public string GuestName { get; set; }
        public string Code { get; set; }
        public string RoomCode { get; set; }
        public int NumberOfGuests { get; set; }
        public int NumberOfNights { get; set; }
        public string Checkin { get; set; }
        public string Checkout { get; set; }
        public decimal Total { get; set; }
    }
}
