﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Thex.Reservation.Domain.Interfaces;
using Thex.Reservation.Infra.Context;
using Thex.Reservation.Infra.Mappers.Profiles;
using Thex.Reservation.Infra.Repositories.ReadRepositories;
using Tnf.Dapper;

namespace Microsoft.Extensions.DependencyInjection
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddInfraDependency(this IServiceCollection services, IConfiguration configuration)
        {
            services
                .AddTnfEntityFrameworkCore()
                .AddTnfDbContext<ThexReservationContext>((config) =>
                {
                    if (config.ExistingConnection != null)
                        config.DbContextOptions.UseSqlServer(config.ExistingConnection);
                    else
                        config.DbContextOptions.UseSqlServer(configuration[$"ConnectionStrings:SqlServer"]);

                });
            
            services.AddTnfAutoMapper(config =>
            {
                config.AddProfile<ReservationProfile>();
            });

            //Read Repositories
            services.AddTransient<IReservationReadRepository, ReservationReadRepository>();

            //Repositories
            services.AddTransient<IReservationRepository, ReservationRepository>();

            services.AddTnfMemoryCache();

            return services;
        }
    }
}
