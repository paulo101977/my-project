﻿using System;
using System.Threading.Tasks;
using Thex.Reservation.Domain.Interfaces;
using Thex.Reservation.Infra.Context;
using Tnf.Dapper.Repositories;
using Tnf.Repositories;
using Tnf.EntityFrameworkCore.Repositories;
using Tnf.EntityFrameworkCore;
using Tnf.Localization;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Tnf.Caching;
using System.Linq;
using Thex.Reservation.Dto;
using Tnf.Dto;

namespace Thex.Reservation.Infra.Repositories.ReadRepositories
{
    public class ReservationReadRepository : EfCoreRepositoryBase<ThexReservationContext, Domain.Reservation>, IReservationReadRepository
    {
        private readonly ILocalizationManager _localizationManager;
        private const string RESERVATION_CACHE_KEY = "ReservationStore";
        private readonly ICache _cache;

        public ReservationReadRepository(
            IDbContextProvider<ThexReservationContext> dbContextProvider,
            ILocalizationManager localizationManager,
            ICache cache) : base(dbContextProvider)
        {
            _localizationManager = localizationManager;
            _cache = cache;
        }

        public async Task<bool> CheckAvailabilityByFilters(string roomCode, DateTime checkin, DateTime checkout)
            => await Context.Reservations.AsNoTracking()
                      .AnyAsync(e => e.RoomCode == roomCode &&
                                e.LengthOfPeriod.Checkin.Date <= checkout.Date &&
                                e.LengthOfPeriod.Checkout.Date > checkin.Date);

        public new async Task<IListDto<ReservationDto>> GetAll()
        {
            var result = await _cache.GetAsync<List<Domain.Reservation>>(RESERVATION_CACHE_KEY);

            return new ListDto<ReservationDto>
            {
                HasNext = false,
                Items = result.Select(e => ToDto(e)).ToList()
            };
        }

        public async Task<ReservationDto> GetById(Guid id)
            => (await _cache.GetAsync<List<Domain.Reservation>>(RESERVATION_CACHE_KEY))
                .Select(e => ToDto(e)).FirstOrDefault(e => e.Id == id);


        private ReservationDto ToDto(Domain.Reservation e)
        {
            return new ReservationDto
            {
                Id = e.Id,
                GuestName = e.GuestName,
                Code = e.Code,
                RoomCode = e.RoomCode,
                NumberOfGuests = e.NumberOfGuests,
                NumberOfNights = e.NumberOfNights,
                Checkin = e.LengthOfPeriod.Checkin.ToShortDateString(),
                Checkout = e.LengthOfPeriod.Checkout.ToShortDateString(),
                Total = e.BudgetList.Sum(s => s.Budget)
            };
        }

    }
}
