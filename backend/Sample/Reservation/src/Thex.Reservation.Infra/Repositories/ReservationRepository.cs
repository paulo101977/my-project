﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Thex.Reservation.Domain;
using Thex.Reservation.Domain.Interfaces;
using Thex.Reservation.Infra.Context;
using Tnf.EntityFrameworkCore;
using Tnf.EntityFrameworkCore.Repositories;
using Tnf.Localization;
using Tnf.Caching;

namespace Thex.Reservation.Infra.Repositories.ReadRepositories
{
    public class ReservationRepository : EfCoreRepositoryBase<ThexReservationContext, Domain.Reservation>, IReservationRepository
    {
        private readonly ILocalizationManager _localizationManager;
        private const string RESERVATION_CACHE_KEY = "ReservationStore";
        private readonly ICache _cache;

        public ReservationRepository(
            IDbContextProvider<ThexReservationContext> dbContextProvider,
            ILocalizationManager localizationManager,
            ICache cache)
            : base(dbContextProvider)
        {
            _localizationManager = localizationManager;
            _cache = cache;
        }

        public async Task<Domain.Reservation> Create(Domain.Reservation reservation)
        {
            await Context.Reservations.AddAsync(reservation);
            await Context.SaveChangesAsync();
            return reservation;
        }

        public async Task FakeReservationDataSync(Domain.Reservation reservation)
        {
            var valuesInCache = await _cache.GetAsync<List<Domain.Reservation>>(RESERVATION_CACHE_KEY);
            if (valuesInCache == null)
                valuesInCache = new List<Domain.Reservation>();

            valuesInCache.Add(reservation);

            _cache.Add(RESERVATION_CACHE_KEY, valuesInCache, TimeSpan.FromMinutes(100));

        }
    }
}