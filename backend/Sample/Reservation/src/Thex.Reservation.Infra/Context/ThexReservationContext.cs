﻿using Microsoft.EntityFrameworkCore;
using Thex.Reservation.Infra.Mappers;
using Tnf.EntityFrameworkCore;
using Tnf.Runtime.Session;

namespace Thex.Reservation.Infra.Context
{
    public class ThexReservationContext : TnfDbContext
    {
        public DbSet<Domain.Reservation> Reservations { get; set; }
        public DbSet<Domain.ReservationBudget> ReservationBudgets { get; set; }

        public ThexReservationContext(DbContextOptions<ThexReservationContext> options, ITnfSession session)
            : base(options, session)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new ReservationMapper());
            modelBuilder.ApplyConfiguration(new ReservationBudgetMapper());

            base.OnModelCreating(modelBuilder);
        }
    }
}
