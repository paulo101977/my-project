﻿using Tnf.Application.Services;
using Tnf.Notifications;

namespace Thex.CashOperation.Application.Services
{
    public abstract class ApplicationServiceBase : ApplicationService
    {
        protected ApplicationServiceBase(INotificationHandler notification) : base(notification)
        {
        }
    }
}
