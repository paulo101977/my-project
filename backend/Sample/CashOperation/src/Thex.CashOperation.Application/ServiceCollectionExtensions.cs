﻿using Microsoft.Extensions.Configuration;
using Thex.CashOperation.Application.Interfaces;
using Thex.CashOperation.Application.Services;

namespace Microsoft.Extensions.DependencyInjection
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddApplicationServiceDependency(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddInfraDependency(configuration);
            services.AddDomainDependency();

            services.AddTransient<IBillingAccountItemAppService, BillingAccountItemAppService>();
            services.AddTransient<IBillingAccountAppService, BillingAccountAppService>();
            
                
            return services;
        }
    }
}