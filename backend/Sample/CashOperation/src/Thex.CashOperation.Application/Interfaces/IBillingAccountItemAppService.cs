﻿using System.Threading.Tasks;
using Thex.CashOperation.Dto;
using Tnf.Application.Services;

namespace Thex.CashOperation.Application.Interfaces
{
    public interface IBillingAccountItemAppService : IApplicationService
    {
        Task<CreditDto> CreateCreditAsync(CreditDto creditDto);
        Task<DebitDto> CreateDebitAsync(DebitDto debitDto);
    }
}
