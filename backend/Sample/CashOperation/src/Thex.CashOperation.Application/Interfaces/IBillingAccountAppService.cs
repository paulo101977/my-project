﻿using System;
using System.Threading.Tasks;
using Thex.CashOperation.Dto;
using Tnf.Application.Services;
using Tnf.Dto;

namespace Thex.CashOperation.Application.Interfaces
{
    public interface IBillingAccountAppService : IApplicationService
    {
        Task<IListDto<BillingAccountDto>> GetAll();
        Task<BillingAccountDto> GetById(Guid id);
        Task Close(Guid id);
    }
}
