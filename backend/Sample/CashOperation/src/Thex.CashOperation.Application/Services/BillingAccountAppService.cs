﻿using Tnf.Notifications;
using Thex.CashOperation.Application.Interfaces;
using Thex.CashOperation.Domain.Interfaces;
using System.Threading.Tasks;
using Tnf.Dto;
using System;
using Thex.CashOperation.Dto;
using Tnf.Repositories.Uow;
using Thex.CashOperation.Domain.Interfaces.Services;

namespace Thex.CashOperation.Application.Services
{
    public class BillingAccountAppService : ApplicationServiceBase, IBillingAccountAppService
    {
        private readonly IBillingAccountReadRepository _billingAccountReadRepository;
        private readonly IBillingAccountItemReadRepository _billingAccountItemReadRepository;
        private readonly IBillingAccountDomainService _billingAccountDomainService;
        private readonly IUnitOfWorkManager _unitOfWorkManager;

        public BillingAccountAppService(
            INotificationHandler notification, 
            IUnitOfWorkManager unitOfWorkManager,
            IBillingAccountReadRepository billingAccountReadRepository,
            IBillingAccountItemReadRepository billingAccountItemReadRepository,
            IBillingAccountDomainService billingAccountDomainService) 
            : base(notification)
        {
            _unitOfWorkManager = unitOfWorkManager;
            _billingAccountReadRepository = billingAccountReadRepository;
            _billingAccountDomainService = billingAccountDomainService;
            _billingAccountItemReadRepository = billingAccountItemReadRepository;
        }

        public async Task<IListDto<BillingAccountDto>> GetAll()
            => await _billingAccountReadRepository.GetAll();

        public async Task<BillingAccountDto> GetById(Guid id)
        {
            var result = await _billingAccountReadRepository.GetById(id);

            if (result != null)
                result.ItemList = await _billingAccountItemReadRepository.GetAllByBillingAccountId(id);

            return result;
        }

        public async Task Close(Guid id)
        {
            using (var uow = _unitOfWorkManager.Begin())
            {
                await _billingAccountDomainService.Close(id);

                if (Notification.HasNotification())
                    return;

                await uow.CompleteAsync();
            }
        }
    }
}
