﻿using Tnf.Notifications;
using MediatR;
using Thex.CashOperation.Application.Interfaces;
using Thex.CashOperation.Domain.Interfaces;
using Thex.CashOperation.Dto;
using Tnf.Repositories.Uow;
using Thex.CashOperation.Domain;
using System.Threading.Tasks;

namespace Thex.CashOperation.Application.Services
{
    public class BillingAccountItemAppService : ApplicationServiceBase, IBillingAccountItemAppService
    {
        private readonly IBillingAccountItemRepository _billingAccountItemRepository;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IBillingAccountItemAdapter _billingAccountItemAdapter;

        public BillingAccountItemAppService(
            INotificationHandler notification, 
            IBillingAccountItemRepository billingAccountItemRepository,
            IBillingAccountItemAdapter billingAccountItemAdapter,
            IUnitOfWorkManager unitOfWorkManager) 
            : base(notification)
        {
            _billingAccountItemRepository = billingAccountItemRepository;
            _billingAccountItemAdapter = billingAccountItemAdapter;
            _unitOfWorkManager = unitOfWorkManager;
        }

        public async Task<CreditDto> CreateCreditAsync(CreditDto creditDto)
        {
            using (var uow = _unitOfWorkManager.Begin())
            {
                ValidateDto<CreditDto>(creditDto, nameof(creditDto));

                if (Notification.HasNotification()) return null;

                var credit = _billingAccountItemAdapter.CreateCredit(creditDto).Build();
                
                if (Notification.HasNotification()) return null;

                await _billingAccountItemRepository.InsertAsync(credit);

                if (Notification.HasNotification()) return null;

                uow.Complete();
            }

            return creditDto;
        }

        public async Task<DebitDto> CreateDebitAsync(DebitDto debitDto)
        {
            using (var uow = _unitOfWorkManager.Begin())
            {
                ValidateDto<CreditDto>(debitDto, nameof(debitDto));

                if (Notification.HasNotification()) return null;

                var debit = _billingAccountItemAdapter.CreateDebit(debitDto).Build();

                if (Notification.HasNotification()) return null;

                await _billingAccountItemRepository.InsertAsync(debit);

                if (Notification.HasNotification()) return null;

                uow.Complete();
            }

            return debitDto;
        }
    }
}
