﻿using System;
using Tnf.Dto;

namespace Thex.CashOperation.Dto
{
    public class BillingAccountItemDto : BaseDto
    {
        public Guid Id { get; set; }
        public string RoomCode { get; set; }
        public string TypeName { get; set; }
        public decimal Amount { get; set; }
        public DateTime CreationTime { get; set; }
    }
}
