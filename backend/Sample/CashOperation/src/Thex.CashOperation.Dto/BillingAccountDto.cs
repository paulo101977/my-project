﻿using System;
using System.Collections.Generic;
using Tnf.Dto;

namespace Thex.CashOperation.Dto
{
    public class BillingAccountDto : BaseDto
    {
        public Guid Id { get; set; }
        public string GuestName { get; set; }
        public string RoomCode { get; set; }
        public bool IsClosed { get; set; }
        public decimal Total { get; set; }

        public List<BillingAccountItemDto> ItemList { get; set; }
        
    }
}
