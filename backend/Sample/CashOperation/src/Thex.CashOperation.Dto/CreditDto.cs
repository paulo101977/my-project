﻿using System;
using Tnf.Dto;

namespace Thex.CashOperation.Dto
{
    public class CreditDto : BaseDto
    {
        public Guid BillingAccountId { get; set; }
        public decimal Amount { get; set; }
    }
}
