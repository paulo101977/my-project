﻿CREATE TABLE [dbo].[BillingAccount]
(
	[Id]				UNIQUEIDENTIFIER NOT NULL, 
    [ReservationId]		NVARCHAR(150) NOT NULL, 
    [GuestName]			NVARCHAR(150) NOT NULL, 
    [RoomCode]			NVARCHAR(150) NOT NULL, 
    [IsClosed]			BIT			  NOT NULL,

	CONSTRAINT [PK_BillingAccount] PRIMARY KEY ([Id]),
	CONSTRAINT [UK_BillingAccount] UNIQUE (ReservationId)
)
GO
