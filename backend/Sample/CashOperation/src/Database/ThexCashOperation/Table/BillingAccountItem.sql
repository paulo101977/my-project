﻿CREATE TABLE [dbo].[BillingAccountItem]
(
	[Id]				UNIQUEIDENTIFIER NOT NULL, 
    [BillingAccountId]	UNIQUEIDENTIFIER NOT NULL, 
	[Type]				INT				 NOT NULL,
    [Amount]			DECIMAL			NOT NULL,
	[CreationTime]	    DATETIME		NOT NULL,

	CONSTRAINT [PK_BillingAccountItem] PRIMARY KEY ([Id]),
	CONSTRAINT [FK_BillingAccountItem_BillingAccount] FOREIGN KEY ([BillingAccountId]) REFERENCES [dbo].[BillingAccount] ([Id]),
)
GO
