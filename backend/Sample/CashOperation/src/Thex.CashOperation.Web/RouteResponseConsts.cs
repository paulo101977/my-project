﻿namespace Thex.CashOperation.Web
{
    public class RouteResponseConsts
    {
        public const string Account = "billingAccount";
        public const string Debit = "debit";
        public const string Credit = "credit";
    }
}
