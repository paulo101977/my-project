﻿using System;
using System.IO.Compression;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.ResponseCompression;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Thex.CashOperation.Domain;
using Thex.CashOperation.Domain.Events.QueueConfiguration;
using Thex.Common;
using Tnf.Bus.Queue.RabbitMQ;

namespace Thex.CashOperation.Web
{
    public class Startup
    {
        private IConfiguration _configuration { get; set; }

        public Startup(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            services.Configure<GzipCompressionProviderOptions>(
                options => options.Level = CompressionLevel.Optimal);

            services
                .AddCorsAll("AllowAll")
                .AddApplicationServiceDependency(_configuration)
                .AddTnfAspNetCore()
                .AddThexGenericLog()
                .AddResponseCompression(options =>
                {
                    options.Providers.Add<GzipCompressionProvider>();
                    options.EnableForHttps = true;
                });

            services.AddSingleton(_configuration.GetSection("ServicesConfiguration").Get<ServicesConfiguration>());

            services
                .AddThexAspNetCoreSecurity(options =>
                {
                    var settingsSection = _configuration.GetSection("TokenConfiguration");
                    var settings = settingsSection.Get<AspNetCore.Security.TokenConfiguration>();

                    options.TokenConfiguration = settings;
                    options.FrontEndpoint = _configuration.GetValue<string>("UrlFront");
                    options.SuperAdminEndpoint = _configuration.GetValue<string>("SuperAdminEndpoint");
                });

            services.AddSwaggerDocumentation();

            services.AddAntiforgery(options =>
            {
                options.Cookie.Name = "X-CSRF-TOKEN-GOTNEXT-COOKIE";
                options.HeaderName = "X-CSRF-TOKEN-GOTNEXT-HEADER";
                options.SuppressXFrameOptionsHeader = false;
            })
            .AddMvc()
            .AddJsonOptions(opts =>
            {
                opts.SerializerSettings.NullValueHandling =
                    Newtonsoft.Json.NullValueHandling.Ignore;
            });

            return services.BuildServiceProvider();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILogger<Startup> logger)
        {
            app.UseCors("AllowAll");

            app.UseTnfAspNetCore(options =>
            {
                options.UseDomainLocalization();

                // Recupera as configurações da fila
                var exchangeRouter = QueueConfiguration.GetExchangeRouterConfiguration();

                options.BusClient()
                   .AddSubscriber(
                        exBuilder: e => exchangeRouter,
                        listener: er => new SubscriberListener(
                            exchangeRouter: er,
                            serviceProvider: app.ApplicationServices),
                        poolSize: 2);
            });

            if (env.IsDevelopment())
                app.UseDeveloperExceptionPage();
            
            app.UseSwaggerDocumentation();

            app.UseThexAspNetCoreSecurity();

            app.UseMvcWithDefaultRoute();
            app.UseResponseCompression();

            app.Run(context =>
            {
                context.Response.Redirect("/swagger");
                return Task.CompletedTask;
            });

            logger.LogInformation("Start application ...");
        }
    }
}
