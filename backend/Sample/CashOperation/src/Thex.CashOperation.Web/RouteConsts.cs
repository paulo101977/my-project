﻿namespace Thex.CashOperation.Web
{
    public class RouteConsts
    {
        public const string Account = "api/account";
        public const string Debit = "api/debit";
        public const string Credit = "api/credit";
    }
}
