﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Thex.CashOperation.Application.Interfaces;
using Thex.CashOperation.Dto;
using Tnf.AspNetCore.Mvc.Response;

namespace Thex.CashOperation.Web.Controllers
{
    //[Authorize]
    [Route(RouteConsts.Debit)]
    public class DebitController : TnfController
    {
        private readonly IBillingAccountItemAppService _billingAccountItemAppService;

        public DebitController(IBillingAccountItemAppService billingAccountItemAppService)
        {
            _billingAccountItemAppService = billingAccountItemAppService;
        }

        [HttpPost]
        [ProducesResponseType(typeof(DebitDto), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Post([FromBody] DebitDto dto)
        {
            var response = await _billingAccountItemAppService.CreateDebitAsync(dto);

            return CreateResponseOnPost(response, RouteResponseConsts.Debit);
        }
    }
}