﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Thex.CashOperation.Application.Interfaces;
using Thex.CashOperation.Dto;
using Tnf.AspNetCore.Mvc.Response;
using Tnf.Dto;

namespace Thex.CashOperation.Web.Controllers
{
    //[Authorize]
    [Route(RouteConsts.Account)]
    public class AccountController : TnfController
    {
        private readonly IBillingAccountAppService _billingAccountAppService;

        public AccountController(IBillingAccountAppService billingAccountAppService)
        {
            _billingAccountAppService = billingAccountAppService;
        }

        [HttpPatch("{id}/close")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Close(Guid id)
        {
            await _billingAccountAppService.Close(id);

            return CreateResponseOnPatch(null, RouteResponseConsts.Account);
        }

        [HttpGet]
        [ProducesResponseType(typeof(IListDto<BillingAccountDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Get()
        => CreateResponseOnGetAll(await _billingAccountAppService.GetAll(), RouteResponseConsts.Account);

        [HttpGet("{id}")]
        [ProducesResponseType(typeof(BillingAccountDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Get(Guid id)
            => CreateResponseOnGet(await _billingAccountAppService.GetById(id), RouteResponseConsts.Account);

    }
}