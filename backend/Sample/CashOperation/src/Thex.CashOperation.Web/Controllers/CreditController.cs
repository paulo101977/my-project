﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Thex.CashOperation.Application.Interfaces;
using Thex.CashOperation.Dto;
using Tnf.AspNetCore.Mvc.Response;

namespace Thex.CashOperation.Web.Controllers
{
    //[Authorize]
    [Route(RouteConsts.Credit)]
    public class CreditController : TnfController
    {
        private readonly IBillingAccountItemAppService _billingAccountItemAppService;

        public CreditController(IBillingAccountItemAppService billingAccountItemAppService)
        {
            _billingAccountItemAppService = billingAccountItemAppService;
        }

        [HttpPost]
        [ProducesResponseType(typeof(CreditDto), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Post([FromBody] CreditDto dto)
        {
            var response = await _billingAccountItemAppService.CreateCreditAsync(dto);

            return CreateResponseOnPost(response, RouteResponseConsts.Credit);
        }
    }
}