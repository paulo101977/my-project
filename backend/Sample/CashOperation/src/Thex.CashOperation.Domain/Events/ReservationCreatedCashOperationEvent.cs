﻿using System;
using Tnf.Bus.Client;

namespace Thex.CashOperation.Domain.Events
{
    public class ReservationCreatedCashOperationEvent : Message
    {
        public Guid ReservationId { get; set; }
        public string GuestName { get; set; }
        public string RoomCode { get; set; }
        public decimal Total { get; set; }

        public ReservationCreatedCashOperationEvent()
        {
        }

        public ReservationCreatedCashOperationEvent(Guid reservationId, string guestName, string roomCode, decimal total)
        {
            ReservationId = reservationId;
            GuestName = guestName;
            RoomCode = roomCode;
            Total = total;
        }
    }
}
