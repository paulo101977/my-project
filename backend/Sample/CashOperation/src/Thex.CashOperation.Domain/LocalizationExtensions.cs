﻿using Thex.Common;
using Tnf.Configuration;
using Tnf.Localization;
using Tnf.Localization.Dictionaries;

namespace Thex.CashOperation.Domain
{
    public static class LocalizationExtensions
    {
        public static void UseDomainLocalization(this ITnfConfiguration configuration)
        {
            configuration.Localization.Sources.Add(
                new DictionaryBasedLocalizationSource(AppConsts.LocalizationSourceName,
                new JsonEmbeddedFileLocalizationDictionaryProvider(
                    typeof(LocalizationExtensions).Assembly,
                    "Thex.CashOperation.Domain.Localization.SourceFiles")));

            configuration.Localization.Languages.Add(new LanguageInfo("pt-BR", "Português", isDefault: true));
            configuration.Localization.Languages.Add(new LanguageInfo("en-US", "English"));
        }
    }
}
