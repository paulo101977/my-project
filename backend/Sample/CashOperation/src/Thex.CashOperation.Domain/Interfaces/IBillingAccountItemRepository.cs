﻿using System;
using Tnf.Repositories;
using System.Threading.Tasks;

namespace Thex.CashOperation.Domain.Interfaces
{
    public interface IBillingAccountItemRepository : IRepository
    {
        Task InsertAsync(Domain.BillingAccountItem entity);
    }
}
