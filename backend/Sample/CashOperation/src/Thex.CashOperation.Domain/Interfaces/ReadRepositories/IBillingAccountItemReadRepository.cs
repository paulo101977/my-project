﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.CashOperation.Dto;

namespace Thex.CashOperation.Domain.Interfaces
{
    public interface IBillingAccountItemReadRepository
    {
        Task<List<BillingAccountItemDto>> GetAllByBillingAccountId(Guid id);
    }
}
