﻿using System;
using System.Threading.Tasks;
using Thex.CashOperation.Dto;
using Tnf.Dto;

namespace Thex.CashOperation.Domain.Interfaces
{
    public interface IBillingAccountReadRepository
    {
        Task<IListDto<BillingAccountDto>> GetAll();
        Task<BillingAccountDto> GetById(Guid id);
    }
}
