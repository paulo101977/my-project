﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Tnf.Domain.Services;

namespace Thex.CashOperation.Domain.Interfaces.Services
{
    public interface IBillingAccountDomainService : IDomainService
    {
        Task Close(Guid id);
        Task Create(BillingAccount billingAccount);
    }
}
