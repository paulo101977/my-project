﻿using System;
using Tnf.Repositories;
using System.Threading.Tasks;

namespace Thex.CashOperation.Domain.Interfaces
{
    public interface IBillingAccountRepository : IRepository
    {
        Task<Domain.BillingAccount> GetByIdAsync(Guid id);
        Task UpdateAsync(Domain.BillingAccount entity);
        Task InsertAsync(Domain.BillingAccount entity);
    }
}
