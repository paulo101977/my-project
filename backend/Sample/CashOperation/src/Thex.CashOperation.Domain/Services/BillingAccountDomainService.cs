﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Thex.CashOperation.Domain.Interfaces;
using Thex.CashOperation.Domain.Interfaces.Services;
using Tnf.Domain.Services;
using Tnf.Notifications;

namespace Thex.CashOperation.Domain.Services
{
    public class BillingAccountDomainService : DomainService, IBillingAccountDomainService
    {
        private readonly IBillingAccountRepository _billingAccountRepository;

        public BillingAccountDomainService(IBillingAccountRepository billingAccountRepository,
            INotificationHandler notificationHandler)
            : base(notificationHandler)
        {
            _billingAccountRepository = billingAccountRepository;
        }

        public async Task Close(Guid id)
        {
            var billingAccount = await _billingAccountRepository.GetByIdAsync(id);

            billingAccount.Close();

            await _billingAccountRepository.UpdateAsync(billingAccount);
        }

        public async Task Create(BillingAccount billingAccount)
            => await _billingAccountRepository.InsertAsync(billingAccount);
            
    }
}