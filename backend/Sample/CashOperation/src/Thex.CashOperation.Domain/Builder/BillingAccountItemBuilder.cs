﻿using System;
using Tnf.Builder;
using Tnf.Notifications;

namespace Thex.CashOperation.Domain
{
    public partial class BillingAccountItem
    {
        public class Builder : Builder<BillingAccountItem>
        {
            public Builder(INotificationHandler handler) : base(handler)
            {
            }

            public Builder(INotificationHandler handler, BillingAccountItem instance) : base(handler, instance)
            {
            }

            public virtual Builder WithId(Guid id)
            {
                Instance.Id = id;
                return this;
            }

            public virtual Builder WithBillingAccountId(Guid billingAccountId)
            {
                Instance.BillingAccountId = billingAccountId;
                return this;
            }

            public virtual Builder WithType(BillingAccountItemTypeEnum type)
            {
                Instance.Type = (int)type;
                return this;
            }

            public virtual Builder WithAmount(decimal amount)
            {
                Instance.Amount = amount;
                return this;
            }

            public virtual Builder WithCreationTime()
            {
                Instance.CreationTime = DateTime.Now;
                return this;
            }

            protected override void Specifications()
            {
                //AddSpecification(new ExpressionSpecification<Reservation>(
                //    AppConsts.LocalizationSourceName,
                //    Reservation.EntityError.ReservationMustHaveReservationCode,
                //    w => !string.IsNullOrWhiteSpace(w.ReservationCode)));
            }
        }
    }
}
