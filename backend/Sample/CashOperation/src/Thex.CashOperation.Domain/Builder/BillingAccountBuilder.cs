﻿using System;
using Thex.Common;
using Tnf.Builder;
using Tnf.Notifications;
using Tnf.Specifications;

namespace Thex.CashOperation.Domain
{
    public partial class BillingAccount
    {
        public class Builder : Builder<BillingAccount>
        {
            public Builder(INotificationHandler notification) : base(notification)
            {
            }

            public Builder(INotificationHandler notification, BillingAccount instance) : base(notification, instance)
            {
            }

            public virtual Builder WithId(Guid id)
            {
                Instance.Id = id;
                return this;
            }

            public virtual Builder WithReservationId(Guid reservationId)
            {
                Instance.ReservationId = reservationId;
                return this;
            }

            public virtual Builder WithGuestName(string guestName)
            {
                Instance.GuestName = guestName;
                return this;
            }

            public virtual Builder WithRoomCode(string roomCode)
            {
                Instance.RoomCode = roomCode;
                return this;
            }

            public virtual Builder WithIsClosed(bool isClosed)
            {
                Instance.IsClosed = isClosed;
                return this;
            }

            public virtual Builder WithItem(BillingAccountItem item)
            {
                Instance.AddItem(item);
                return this;
            }
            
            protected override void Specifications()
            {
                AddSpecification(new ExpressionSpecification<BillingAccount>(
                    AppConsts.LocalizationSourceName,
                    EntityError.BillingAccountMustHaveId,
                    w => w.Id != default(Guid)));

                AddSpecification(new ExpressionSpecification<BillingAccount>(
                    AppConsts.LocalizationSourceName,
                    EntityError.BillingAccountMustHaveReservationId,
                    w => w.Id != default(Guid)));

                AddSpecification(new ExpressionSpecification<BillingAccount>(
                    AppConsts.LocalizationSourceName,
                    EntityError.BillingAccountMustRoomCode,
                    w => !string.IsNullOrWhiteSpace(w.RoomCode)));
                
                AddSpecification(new ExpressionSpecification<BillingAccount>(
                    AppConsts.LocalizationSourceName,
                    EntityError.BillingAccountOutOfBoundRoomCode,
                    w => string.IsNullOrWhiteSpace(w.RoomCode) || w.RoomCode.Length > 0 && w.RoomCode.Length <= 150));

                AddSpecification(new ExpressionSpecification<BillingAccount>(
                    AppConsts.LocalizationSourceName,
                    EntityError.BillingAccountMustHaveGuestName,
                    w => !string.IsNullOrWhiteSpace(w.GuestName)));

                AddSpecification(new ExpressionSpecification<BillingAccount>(
                    AppConsts.LocalizationSourceName,
                    EntityError.BillingAccountOutOfBoundGuestName,
                    w => string.IsNullOrWhiteSpace(w.GuestName) || w.GuestName.Length > 0 && w.GuestName.Length <= 150));
            }
        }
    }
}
