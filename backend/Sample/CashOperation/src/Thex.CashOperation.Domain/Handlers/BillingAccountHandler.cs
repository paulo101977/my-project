﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.CashOperation.Domain;
using Thex.CashOperation.Domain.Events;
using Thex.CashOperation.Domain.Interfaces;
using Thex.CashOperation.Domain.Interfaces.Services;
using Tnf.Bus.Client;
using Tnf.Bus.Queue.Interfaces;
using Tnf.Notifications;
using Tnf.Repositories.Uow;

namespace Thex.CashOperation.Handlers
{
    public class BillingAccountHandler : ISubscribe<ReservationCreatedCashOperationEvent>
    {
        private readonly IBillingAccountDomainService _billingAccountDomainService;
        private readonly IBillingAccountItemRepository _billingAccountItemRepository;
        private readonly IBillingAccountAdapter _billingAccountAdapter;
        private readonly IBillingAccountItemAdapter _billingAccountItemAdapter;
        private readonly INotificationHandler _notification;
        private readonly IUnitOfWorkManager _unitOfWorkManager;

        public BillingAccountHandler(
            INotificationHandler notification,
            IBillingAccountDomainService billingAccountDomainService,
            IBillingAccountAdapter billingAccountAdapter,
            IBillingAccountItemRepository billingAccountItemRepository,
            IBillingAccountItemAdapter billingAccountItemAdapter,
            IUnitOfWorkManager unitOfWorkManager)
        {
            _billingAccountDomainService = billingAccountDomainService;
            _billingAccountAdapter = billingAccountAdapter;
            _billingAccountItemAdapter = billingAccountItemAdapter;
            _billingAccountItemRepository = billingAccountItemRepository;
            _notification = notification;
            _unitOfWorkManager = unitOfWorkManager;
        }

        public async Task Handle(ReservationCreatedCashOperationEvent message)
        {
            using (var uow = _unitOfWorkManager.Begin())
            {
                var billingAccount = _billingAccountAdapter.CreateMap(message).Build();

                if (_notification.HasNotification())
                    return;

                await _billingAccountDomainService.Create(billingAccount);

                var billingAccountItem = _billingAccountItemAdapter.CreateMap(billingAccount.Id, message.Total).Build();

                if (_notification.HasNotification())
                    return;

                await _billingAccountItemRepository.InsertAsync(billingAccountItem);

                uow.Complete();
            }
            message.DoAck();
        }
    }
}
