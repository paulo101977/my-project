﻿namespace Thex.CashOperation.Domain
{
    public enum BillingAccountItemTypeEnum
    {
        Debit = 1,
        Credit = 2,
        Reversal = 3
    }
}
