﻿using System;

namespace Thex.CashOperation.Domain
{
    public partial class BillingAccountItem
    {
        static internal Guid GenerateId()
         => Guid.NewGuid();

        public Guid Id { get; private set; }
        public Guid BillingAccountId { get; private set; }
        public int Type { get; private set; }
        public decimal Amount { get; private set; }
        public DateTime CreationTime { get; private set; }

        public virtual BillingAccount BillingAccount { get; internal set; }
    }
}
