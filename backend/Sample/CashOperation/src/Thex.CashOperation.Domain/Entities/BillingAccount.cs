﻿using System;
using System.Collections.Generic;
using System.Linq;
using Tnf.Notifications;

namespace Thex.CashOperation.Domain
{
    public partial class BillingAccount
    {
        public BillingAccount()
        {
            ItemList = new HashSet<BillingAccountItem>();
        }

        static internal Guid GenerateId()
         => Guid.NewGuid();

        public Guid Id { get; private set; }
        public Guid ReservationId { get; private set; }
        public string GuestName { get; private set; }
        public string RoomCode { get; private set; }
        public bool IsClosed { get; private set; }
        public ICollection<BillingAccountItem> ItemList { get; private set; }

        internal void AddItem(BillingAccountItem item)
        {
            if (ItemList == null)
                ItemList = new List<BillingAccountItem>();

            if (ItemList.Contains(item))
                throw new Exception();

            ItemList.Add(item);
        }

        internal void Close()
        {
            IsClosed = true;
        }


        public enum EntityError
        {
            BillingAccountMustHaveId,
            BillingAccountMustHaveReservationId,
            BillingAccountMustHaveGuestName,
            BillingAccountOutOfBoundGuestName,
            BillingAccountMustRoomCode,
            BillingAccountOutOfBoundRoomCode,
            BillingAccountMustHaveNumberOfGuests,
            BillingAccountIsClosed
        }

        protected internal static Builder Create(INotificationHandler handler)
            => new Builder(handler);

        protected internal static Builder Create(INotificationHandler handler, BillingAccount instance)
            => new Builder(handler, instance);
    }
}
