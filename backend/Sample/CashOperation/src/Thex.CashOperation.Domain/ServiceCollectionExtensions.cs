﻿using Thex.CashOperation.Domain;
using Thex.CashOperation.Domain.Interfaces.Services;
using Thex.CashOperation.Domain.Services;

namespace Microsoft.Extensions.DependencyInjection
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddDomainDependency(this IServiceCollection services)
        {
            services.AddTransient<IBillingAccountAdapter, BillingAccountAdapter>();
            services.AddTransient<IBillingAccountItemAdapter, BillingAccountItemAdapter>();

            services.AddTransient<IBillingAccountDomainService, BillingAccountDomainService>();
            
            services.AddTnfBusClient();

            return services;
        }

    }
}
