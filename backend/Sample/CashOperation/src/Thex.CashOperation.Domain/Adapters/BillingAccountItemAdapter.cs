﻿using System;
using Thex.CashOperation.Dto;
using Tnf;
using Tnf.Notifications;

namespace Thex.CashOperation.Domain
{
    public class BillingAccountItemAdapter : IBillingAccountItemAdapter
    {
        public INotificationHandler NotificationHandler { get; }

        public BillingAccountItemAdapter(INotificationHandler notificationHandler)
        {
            NotificationHandler = notificationHandler;
        }

        public virtual BillingAccountItem.Builder CreateMap(Guid billingAccountId, decimal amount)
        {
            var id = BillingAccountItem.GenerateId();

            var builder = new BillingAccountItem.Builder(NotificationHandler)
                .WithId(id)
                .WithAmount(amount)
                .WithCreationTime()
                .WithBillingAccountId(billingAccountId)
                .WithType(BillingAccountItemTypeEnum.Debit);

            return builder;
        }

        public virtual BillingAccountItem.Builder CreateDebit(DebitDto debit)
        {
            var id = BillingAccountItem.GenerateId();

            var builder = new BillingAccountItem.Builder(NotificationHandler)
                .WithId(id)
                .WithBillingAccountId(debit.BillingAccountId)
                .WithAmount(debit.Amount * (-1))
                .WithCreationTime()
                .WithType(BillingAccountItemTypeEnum.Debit);

            return builder;
        }

        public virtual BillingAccountItem.Builder CreateCredit(CreditDto credit)
        {
            var id = BillingAccountItem.GenerateId();

            var builder = new BillingAccountItem.Builder(NotificationHandler)
                .WithId(id)
                .WithBillingAccountId(credit.BillingAccountId)
                .WithAmount(credit.Amount)
                .WithCreationTime()
                .WithType(BillingAccountItemTypeEnum.Credit);

            return builder;
        }
    }
}
