﻿using Thex.CashOperation.Domain.Events;
using Tnf;
using Tnf.Notifications;

namespace Thex.CashOperation.Domain
{
    public class BillingAccountAdapter : IBillingAccountAdapter
    {
        public INotificationHandler _notificationHandler { get; }

        public BillingAccountAdapter(INotificationHandler notificationHandler)
        {
            _notificationHandler = notificationHandler;
        }

        public virtual BillingAccount.Builder CreateMap(ReservationCreatedCashOperationEvent message)
        {
            Check.NotNull(message, nameof(message));

            var id = BillingAccount.GenerateId();

            var builder = new BillingAccount.Builder(_notificationHandler)
                .WithId(id)
                .WithGuestName(message.GuestName)
                .WithRoomCode(message.RoomCode)
                .WithIsClosed(false)
                .WithReservationId(message.ReservationId);
            
            return builder;
        }
    }
}
