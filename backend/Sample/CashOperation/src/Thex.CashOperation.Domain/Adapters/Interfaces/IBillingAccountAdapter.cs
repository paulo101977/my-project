﻿using Thex.CashOperation.Domain;
using Thex.CashOperation.Domain.Events;

namespace Thex.CashOperation.Domain
{
    public interface IBillingAccountAdapter
    { 
        BillingAccount.Builder CreateMap(ReservationCreatedCashOperationEvent message);
    }
}
