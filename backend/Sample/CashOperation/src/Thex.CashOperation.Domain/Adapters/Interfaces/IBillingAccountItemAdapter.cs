﻿using System;
using Thex.CashOperation.Dto;

namespace Thex.CashOperation.Domain
{
    public interface IBillingAccountItemAdapter
    {
        BillingAccountItem.Builder CreateMap(Guid billingAccountId, decimal amount);
        BillingAccountItem.Builder CreateDebit(DebitDto debit);
        BillingAccountItem.Builder CreateCredit(CreditDto credit);
    }
}
