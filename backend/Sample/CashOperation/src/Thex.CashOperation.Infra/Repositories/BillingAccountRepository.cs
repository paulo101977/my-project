﻿using Thex.CashOperation.Infra.Context;
using Tnf.Localization;
using Tnf.Dapper.Repositories;
using Tnf.Repositories;
using Thex.CashOperation.Domain.Interfaces;
using System.Threading.Tasks;
using System;
using Thex.CashOperation.Domain;
using Thex.GenericLog;

namespace Thex.CashOperation.Infra.Repositories.ReadRepositories
{
    public class BillingAccountRepository : DapperEfRepositoryBase<ThexCashOperationContext, Domain.BillingAccount>, IBillingAccountRepository
    {
        private readonly ILocalizationManager _localizationManager;

        public BillingAccountRepository(
             IActiveTransactionProvider activeTransactionProvider,
             ILocalizationManager localizationManager,
             IGenericLogHandler genericLogHandler)
            : base(activeTransactionProvider, genericLogHandler)
        {
            _localizationManager = localizationManager;
        }

        public async Task<Domain.BillingAccount> GetByIdAsync(Guid id)
            => await base.FirstOrDefaultAsync(e => e.Id == id);

        public new async Task UpdateAsync(Domain.BillingAccount entity)
            => await base.UpdateAsync(entity);

        public new async Task InsertAsync(Domain.BillingAccount entity)
            => await base.InsertAsync(entity);
    }
}