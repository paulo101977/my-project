﻿using Thex.CashOperation.Infra.Context;
using Tnf.EntityFrameworkCore.Repositories;
using Tnf.EntityFrameworkCore;
using Tnf.Localization;
using Tnf.Repositories;
using Tnf.Dapper.Repositories;
using Thex.CashOperation.Domain.Interfaces;
using System.Threading.Tasks;
using Tnf.Dto;
using Thex.CashOperation.Dto;
using System.Linq;
using System;
using System.Collections.Generic;
using Thex.GenericLog;

namespace Thex.CashOperation.Infra.Repositories.ReadRepositories
{
    public class BillingAccountItemReadRepository : DapperEfRepositoryBase<ThexCashOperationContext, Domain.BillingAccountItem>, IBillingAccountItemReadRepository
    {
        private readonly ILocalizationManager _localizationManager;

        public BillingAccountItemReadRepository(
             IActiveTransactionProvider activeTransactionProvider,
             ILocalizationManager localizationManager,
             IGenericLogHandler genericLogHandler)
            : base(activeTransactionProvider, genericLogHandler)
        {
            _localizationManager = localizationManager;
        }

        public async Task<List<BillingAccountItemDto>> GetAllByBillingAccountId(Guid id)
        {
            var query = await QueryAsync<BillingAccountItemDto>(@"
                SELECT [Id]
                  ,[BillingAccountId]
                  ,[Type]
                  ,[Amount]
                  ,[CreationTime]
              FROM [dbo].[BillingAccountItem]
              WHERE BillingAccountId = @BillingAccountId",
              new
            {
                BillingAccountId = id,
            });

            return query.ToList();
        }
    }
}
