﻿using Thex.CashOperation.Infra.Context;
using Tnf.Localization;
using Tnf.Dapper.Repositories;
using Tnf.Repositories;
using Thex.CashOperation.Domain.Interfaces;
using Tnf.Dto;
using System.Threading.Tasks;
using Thex.CashOperation.Dto;
using System.Linq;
using System;
using Thex.GenericLog;

namespace Thex.CashOperation.Infra.Repositories.ReadRepositories
{
    public class BillingAccountReadRepository : DapperEfRepositoryBase<ThexCashOperationContext, Domain.BillingAccount>, IBillingAccountReadRepository
    {
        private readonly ILocalizationManager _localizationManager;

        public BillingAccountReadRepository(
             IActiveTransactionProvider activeTransactionProvider,
             ILocalizationManager localizationManager,
             IGenericLogHandler genericLogHandler)
            : base(activeTransactionProvider, genericLogHandler)
        {
            _localizationManager = localizationManager;
        }

        public new async Task<IListDto<BillingAccountDto>> GetAll()
        {
            var ret = new ListDto<BillingAccountDto>();
            ret.HasNext = false;

            var query = await QueryAsync<BillingAccountDto>(@"
                SELECT b.[Id]
                  ,[ReservationId]
                  ,[GuestName]
                  ,[RoomCode]
                  ,[IsClosed]
	              ,sum(bi.Amount) Total
              FROM [BillingAccount] b
              INNER JOIN [BillingAccountItem] bi on b.Id = bi.BillingAccountId
              GROUP BY b.[Id],[ReservationId],[GuestName],[RoomCode],[IsClosed]");

            query.ToList().ForEach(billingItem =>
            {
                ret.Items.Add(billingItem);
            });

            return ret;
        }

        public async Task<BillingAccountDto> GetById(Guid id)
        {
            var query = await QueryAsync<BillingAccountDto>(@"
                SELECT b.[Id]
                  ,[ReservationId]
                  ,[GuestName]
                  ,[RoomCode]
                  ,[IsClosed]
	              ,sum(bi.Amount) Total
              FROM [BillingAccount] b
              INNER JOIN [BillingAccountItem] bi on b.Id = bi.BillingAccountId
              WHERE B.ID = @BillingAccountId
              GROUP BY b.[Id],[ReservationId],[GuestName],[RoomCode],[IsClosed]",
              new
              {
                  BillingAccountId = id,
              });

            return query.FirstOrDefault();
        }
    }
}
