﻿using Thex.CashOperation.Infra.Context;
using Tnf.Localization;
using Tnf.Dapper.Repositories;
using Tnf.Repositories;
using Thex.CashOperation.Domain.Interfaces;
using System.Threading.Tasks;
using Thex.GenericLog;

namespace Thex.CashOperation.Infra.Repositories.ReadRepositories
{
    public class BillingAccountItemRepository : DapperEfRepositoryBase<ThexCashOperationContext, Domain.BillingAccountItem>, IBillingAccountItemRepository
    {
        private readonly ILocalizationManager _localizationManager;

        public BillingAccountItemRepository(
             IActiveTransactionProvider activeTransactionProvider,
             ILocalizationManager localizationManager,
             IGenericLogHandler genericLogHandler)
            : base(activeTransactionProvider, genericLogHandler)
        {
            _localizationManager = localizationManager;
        }

        public new async Task InsertAsync(Domain.BillingAccountItem entity)
            => await base.InsertAsync(entity);
    }
}