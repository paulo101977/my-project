﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Thex.CashOperation.Infra.Context;
using Thex.CashOperation.Domain.Interfaces;
using Thex.CashOperation.Infra.Repositories.ReadRepositories;
using Tnf.Dapper;
using Thex.CashOperation.Infra.Mappers;

namespace Microsoft.Extensions.DependencyInjection
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddInfraDependency(this IServiceCollection services, IConfiguration configuration)
        {
            services
                .AddTnfEntityFrameworkCore()
                .AddTnfDbContext<ThexCashOperationContext>((config) =>
                {
                    if (config.ExistingConnection != null)
                        config.DbContextOptions.UseSqlServer(config.ExistingConnection);
                    else
                        config.DbContextOptions.UseSqlServer(configuration[$"ConnectionStrings:SqlServer"]);
                })
                .AddTnfDapper(options =>
                {
                    options.MapperAssemblies.Add(typeof(BillingAccountMapper).Assembly);
                    options.MapperAssemblies.Add(typeof(BillingAccountItemMapper).Assembly);

                    options.DbType = DapperDbType.SqlServer;
                });
            
            //Read Repositories
            services.AddTransient<IBillingAccountReadRepository, BillingAccountReadRepository>();
            services.AddTransient<IBillingAccountItemReadRepository, BillingAccountItemReadRepository>();

            //Repositories
            services.AddTransient<IBillingAccountRepository, BillingAccountRepository>();
            services.AddTransient<IBillingAccountItemRepository, BillingAccountItemRepository>();

            return services;
        }
    }
}
