﻿using Microsoft.EntityFrameworkCore;
using Thex.CashOperation.Domain;
using Tnf.EntityFrameworkCore;
using Tnf.Runtime.Session;

namespace Thex.CashOperation.Infra.Context
{
    public class ThexCashOperationContext : TnfDbContext
    {
        public DbSet<BillingAccount> BillingAccounts { get; set; }
        public DbSet<BillingAccountItem> BillingAccountItens { get; set; }

        public ThexCashOperationContext(DbContextOptions<ThexCashOperationContext> options, ITnfSession session)
            : base(options, session)
        {
        }
    }
}
