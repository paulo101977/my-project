﻿using DapperExtensions.Mapper;

namespace Thex.CashOperation.Infra.Mappers
{
    public sealed class BillingAccountItemMapper : ClassMapper<Domain.BillingAccountItem>
    {
        public BillingAccountItemMapper()
        {
            Table("BillingAccountItem");
            Map(e => e.Id).Column("Id").Key(KeyType.Guid);
            Map(e => e.BillingAccount).Ignore();
            AutoMap();
        }
    }
}
