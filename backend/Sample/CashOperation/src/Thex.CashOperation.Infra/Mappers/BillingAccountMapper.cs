﻿using DapperExtensions.Mapper;

namespace Thex.CashOperation.Infra.Mappers
{
    public sealed class BillingAccountMapper : ClassMapper<Domain.BillingAccount>
    {
        public BillingAccountMapper()
        {
            Table("BillingAccount");
            Map(e => e.Id).Column("Id").Key(KeyType.Guid);
            Map(e => e.ItemList).Ignore();
            AutoMap();
        }
    }
}
