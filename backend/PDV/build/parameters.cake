public class BuildParameters
{
	public string SolutionTarget { get; private set; }
    public string ProjectTarget { get; private set; }
    public string Configuration { get; private set; }
    public string TargetFramework { get; private set; }
    public string TargetFrameworkFull { get; private set; }
    
    public static BuildParameters GetParameters(ICakeContext context)
    {
        if (context == null)
        {
            throw new ArgumentNullException("context");
        }

        var buildSystem = context.BuildSystem();

        return new BuildParameters {
            SolutionTarget = "../../Thex.PDV.sln",
            ProjectTarget = "../test/Thex.PDV.Web.Tests/Thex.PDV.Web.Tests.csproj",
            Configuration = context.Argument("configuration", "Debug"),
            TargetFramework = "netcoreapp2.1",
            TargetFrameworkFull = "netcoreapp2.1"
        };
    }
}