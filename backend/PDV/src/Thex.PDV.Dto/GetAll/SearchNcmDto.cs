﻿using System;
using System.Collections.Generic;
using System.Text;
using Tnf.Dto;

namespace Thex.PDV.Dto.GetAll
{
    public class SearchNcmDto : RequestAllDto
    {
        public string Param { get; set; }
    }
}
