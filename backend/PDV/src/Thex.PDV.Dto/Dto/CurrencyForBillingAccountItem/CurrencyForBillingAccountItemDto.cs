﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Thex.PDV.Dto.Dto.CurrencyForBillingAccountItem
{
    public class CurrencyForBillingAccountItemDto
    {
        public static CurrencyForBillingAccountItemDto NullInstance = null;

        public Guid CurrencyId { get; set; }
        public Guid? CurrencyExchangeReferenceId { get; set; }
    }
}
