﻿using System;
using System.Collections.Generic;
using System.Text;
using Tnf.Dto;

namespace Thex.PDV.Dto.Product
{
    public class SearchProductDto : RequestAllDto
    {
        public string Param { get; set; }
    }
}
