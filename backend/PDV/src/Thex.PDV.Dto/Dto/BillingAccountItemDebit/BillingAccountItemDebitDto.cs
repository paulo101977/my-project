﻿using System;
using System.Collections.Generic;
using Tnf.Dto;

namespace Thex.PDV.Dto.Dto.BillingAccountItemDebit
{
    public class BillingAccountItemDebitDto : BillingAccountItemBaseDto
    {
        public static BillingAccountItemDebitDto NullInstance = null;

        public BillingAccountItemDebitDto()
        {
            this.DetailList = new List<DetailIntegration>();
        }

        public Guid Id { get; set; }
        public int PropertyId { get; set; }
        public Guid BillingAccountId { get; set; }
        public int BillingItemId { get; set; }
        public Guid TenantId { get; set; }
        public DateTime? DateParameter { get; set; }

        public List<DetailIntegration> DetailList { get; set; }
    }

    public class DetailIntegration : BillingAccountItemBaseDto
    {
        /// <summary>
        /// Id do produto para lançamentos de POS
        /// BillingItemId para lançamentos de serviço
        /// BillingItemTypeId para lançamentos de crédito
        /// </summary>
        public string ExternalId { get; set; }
        public string Code { get; set; }
        public string BarCode { get; set; }
        public string NcmCode { get; set; }
        public decimal Quantity { get; set; }
        public string Description { get; set; }
        public string GroupDescription { get; set; }
    }

    public class BillingAccountItemBaseDto : BaseDto
    {
        public decimal Amount { get; set; }

        public Guid? CurrencyId { get; set; }
        public Guid? CurrencyExchangeReferenceId { get; set; }
        public Guid? CurrencyExchangeReferenceSecId { get; set; }

        public void CalculateAmountByExchangeRate(decimal exchangeRateFirst, decimal exchangeRateSec)
        {
            this.Amount = Math.Round(Amount / exchangeRateFirst * exchangeRateSec, 2);
        }
    }
}
