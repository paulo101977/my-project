﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Thex.PDV.Dto.Dto.BillingItemServiceAndTaxAssociate
{
    public class BillingItemServiceAndTaxAssociateDto
    {
        public int? BillingItemTaxId { get; set; }
        public decimal TaxPercentage { get; set; }
    }
}
