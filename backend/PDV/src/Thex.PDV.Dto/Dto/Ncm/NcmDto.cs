﻿using System;
using Tnf.Dto;

namespace Thex.PDV.Dto
{
    public partial class NcmDto : BaseDto
    {
        public Guid Id { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
    }
}
