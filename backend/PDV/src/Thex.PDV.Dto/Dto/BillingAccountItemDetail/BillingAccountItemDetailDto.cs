﻿using System;
using Tnf.Dto;

namespace Thex.PDV.Dto.Dto.BillingAccountItem
{
    public partial class BillingAccountItemDetailDto : BaseDto
    {
        public static BillingAccountItemDetailDto NullInstance = null;

        public Guid Id { get; set; }
        public Guid BillingAccountItemId { get; set; }
        public decimal Amount { get; set; }
        public decimal Quantity { get; set; }
        public Guid ProductId { get; set; }
    }
}
