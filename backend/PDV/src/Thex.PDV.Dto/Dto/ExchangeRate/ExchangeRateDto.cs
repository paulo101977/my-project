﻿using System;
using Tnf.Dto;

namespace Thex.PDV.Dto
{
    public partial class ExchangeRateDto : BaseDto
    {
        public Guid Id { get; set; }
        public decimal ExchangeRate { get; set; }
    }
}
