﻿using System;
using System.Diagnostics.CodeAnalysis;
using Tnf.Dto;

namespace Thex.PDV.Dto
{
    [ExcludeFromCodeCoverage]
    public class DefaultGuidRequestDto : RequestDto, IDefaultGuidRequestDto
    {
        public DefaultGuidRequestDto()
        {
        }

        public DefaultGuidRequestDto(Guid id, RequestDto request)
        {
            Id = id;
            Fields = request.Fields;
            Expand = request.Expand;
        }

        public DefaultGuidRequestDto(Guid id)
        {
            Id = id;
        }

        public Guid Id { get; set; }
    }

    [ExcludeFromCodeCoverage]
    public class DefaultIntRequestDto : RequestDto, IDefaultIntRequestDto
    {
        public DefaultIntRequestDto()
        {
        }

        public DefaultIntRequestDto(int id, RequestDto request)
        {
            Id = id;
            Fields = request.Fields;
            Expand = request.Expand;
        }

        public DefaultIntRequestDto(int id)
        {
            Id = id;
        }

        public int Id { get; set; }
    }

    [ExcludeFromCodeCoverage]
    public class DefaultLongRequestDto : RequestDto, IDefaultLongRequestDto
    {
        public DefaultLongRequestDto()
        {
        }

        public DefaultLongRequestDto(long id, RequestDto request)
        {
            Id = id;
            Fields = request.Fields;
            Expand = request.Expand;
        }

        public DefaultLongRequestDto(long id)
        {
            Id = id;
        }

        public long Id { get; set; }
    }

    [ExcludeFromCodeCoverage]
    public class DefaultStringRequestDto : RequestDto, IDefaultStringRequestDto
    {
        public DefaultStringRequestDto()
        {
        }

        public DefaultStringRequestDto(string id, RequestDto request)
        {
            Id = id;
            Fields = request.Fields;
            Expand = request.Expand;
        }

        public DefaultStringRequestDto(string id)
        {
            Id = id;
        }

        public string Id { get; set; }
    }
}

