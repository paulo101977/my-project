﻿using Thex.Common;

namespace Microsoft.Extensions.DependencyInjection
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddDomainDependency(this IServiceCollection services)
        {
            // Adiciona as dependencias para utilização dos serviços de crud generico do Tnf
            services
                .AddTnfDefaultConventionalRegistrations()
                .AddCommonDependency();
            
            return services;
        }
    }
}
