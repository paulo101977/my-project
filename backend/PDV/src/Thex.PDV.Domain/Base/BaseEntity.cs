﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Text;
using Thex.PDV.Domain.Entities;

namespace Thex.PDV.Domain.Entities
{
    [ExcludeFromCodeCoverage]
    public class BaseEntity : ThexMultiTenantNullableFullAuditedEntity, IEntityGuid
    {
        public Guid Id { get; set; }
        public bool IsActive { get; set; }
    }
}
