﻿using System;
using System.Collections.Generic;
using System.Text;
using Thex.PDV.Domain.Entities;

namespace Thex.PDV.Domain.Entities
{
    public class BaseEntityId : ThexMultiTenantNullableFullAuditedEntity, IEntityInt
    {
        public int Id { get; set; }
        public bool IsActive { get; set; }
    }
}
