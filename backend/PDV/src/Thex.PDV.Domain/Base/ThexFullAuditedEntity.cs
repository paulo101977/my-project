﻿using System;
using Thex.Dapper.Auditing;
using Tnf.Repositories.Entities;
using Tnf.Repositories.Entities.Auditing;

namespace Thex.PDV.Domain.Entities
{
    [Serializable]
    public abstract class ThexFullAuditedEntity : IModificationThexAudited, ICreationThexAudited, IHasCreationTime,
                                                  ISoftDelete, IHasDeletionTime, IDeletionThexAudited
    {
        public virtual bool IsDeleted { get; set; }

        public virtual DateTime CreationTime { get; set; }

        public virtual Guid CreatorUserId { get; set; }

        public virtual DateTime? DeletionTime { get; set; }

        public virtual Guid? DeleterUserId { get; set; }

        public virtual DateTime? LastModificationTime { get; set; }

        public virtual Guid? LastModifierUserId { get; set; }
    }
}
