﻿using System;
using Tnf.Repositories.Entities.Auditing;

namespace Thex.PDV.Domain.Entities
{
    [Serializable]
    public abstract class ThexMultiTenantNullableFullAuditedEntity : ThexFullAuditedEntity, IMustHaveThexTenant
    {
        public Guid? TenantId { get; set; }
        public int? PropertyId { get; set; }
        public int? ChainId { get; set; }
    }
}
