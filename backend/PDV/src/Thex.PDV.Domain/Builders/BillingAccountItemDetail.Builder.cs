﻿using System;
using Tnf.Builder;
using Tnf.Notifications;

namespace Thex.PDV.Domain.Entities
{
    public partial class BillingAccountItemDetail
    {
        public class Builder : Builder<BillingAccountItemDetail>
        {
            public Builder(INotificationHandler handler) : base(handler)
            {
            }

            public Builder(INotificationHandler handler, BillingAccountItemDetail instance) : base(handler, instance)
            {
            }

            public virtual Builder WithId(Guid id)
            {
                Instance.Id = id;
                return this;
            }
            public virtual Builder WithBillingAccountItemId(Guid billingAccountItemId)
            {
                Instance.BillingAccountItemId = billingAccountItemId;
                return this;
            }
            public virtual Builder WithAmount(decimal amount)
            {
                Instance.Amount = amount;
                return this;
            }
            public virtual Builder WithQuantity(decimal quantity)
            {
                Instance.Quantity = quantity;
                return this;
            }
            public virtual Builder WithProductId(Guid productId)
            {
                Instance.ProductId = productId;
                return this;
            }

            internal void EntitySpecifications()
            {
            }
        }
    }
}