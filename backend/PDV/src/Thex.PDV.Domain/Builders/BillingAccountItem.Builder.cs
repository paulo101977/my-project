﻿

using System;
using Thex.Common;
using Tnf.Builder;
using Tnf.Notifications;
using Tnf.Specifications;

namespace Thex.PDV.Domain.Entities
{
    public partial class BillingAccountItem
    {
        public class Builder : Builder<BillingAccountItem>
        {
            public Builder(INotificationHandler handler) : base(handler)
            {
            }

            public Builder(INotificationHandler handler, BillingAccountItem instance) : base(handler, instance)
            {
            }

            public virtual Builder WithId(Guid id)
            {
                Instance.Id = id;
                return this;
            }
            public virtual Builder WithBillingAccountId(Guid billingAccountId)
            {
                Instance.BillingAccountId = billingAccountId;
                return this;
            }
            public virtual Builder WithBillingItemId(int? billingItemId)
            {
                Instance.BillingItemId = billingItemId;
                return this;
            }
            public virtual Builder WithBillingAccountItemTypeId(int billingAccountItemTypeId)
            {
                Instance.BillingAccountItemTypeId = billingAccountItemTypeId;
                return this;
            }
            public virtual Builder WithAmount(decimal amount)
            {
                Instance.Amount = amount;
                return this;
            }
            public virtual Builder WithCurrencyId(Guid currencyId)
            {
                Instance.CurrencyId = currencyId;
                return this;
            }
            public virtual Builder WithCurrencyExchangeReferenceId(Guid? currencyExchangeReferenceId)
            {
                Instance.CurrencyExchangeReferenceId = currencyExchangeReferenceId;
                return this;
            }
            public virtual Builder WithCurrencyExchangeReferenceSecId(Guid? currencyExchangeReferenceSecId)
            {
                Instance.CurrencyExchangeReferenceSecId = currencyExchangeReferenceSecId;
                return this;
            }
            public virtual Builder WithBillingAccountItemDate(DateTime billingAccountItemDate)
            {
                Instance.BillingAccountItemDate = billingAccountItemDate;
                return this;
            }
            public virtual Builder WithBillingAccountItemParentId(Guid? billingAccountItemParentId)
            {
                Instance.BillingAccountItemParentId = billingAccountItemParentId;
                return this;
            }
            public virtual Builder WithIntegrationDetail(string integrationDetail)
            {
                Instance.IntegrationDetail = integrationDetail;
                return this;
            }
            public virtual Builder WithDateParameter(DateTime? dateParameter)
            {
                Instance.DateParameter = dateParameter;
                return this;
            }
            protected override void Specifications()
            {
                AddSpecification(new ExpressionSpecification<BillingAccountItem>(
                    AppConsts.LocalizationSourceName,
                    BillingAccountItem.EntityError.BillingAccountItemMustHaveBillingAccountItemTypeId,
                    w => w.BillingAccountItemTypeId != default(int)));

                AddSpecification(new ExpressionSpecification<BillingAccountItem>(
                    AppConsts.LocalizationSourceName,
                    BillingAccountItem.EntityError.BillingAccountItemMustHaveCurrencyId,
                    w => w.CurrencyId != default(Guid)));
                AddSpecification(new ExpressionSpecification<BillingAccountItem>(
                    AppConsts.LocalizationSourceName,
                    BillingAccountItem.EntityError.BillingAccountItemInvalidBillingAccountItemDate,
                    w => w.BillingAccountItemDate >= new DateTime(1753, 1, 1) && w.BillingAccountItemDate <= DateTime.MaxValue));
            }
        }
    }

}
