﻿using System;
using Thex.Common;
using Tnf.Builder;
using Tnf.Notifications;
using Tnf.Specifications;

namespace Thex.PDV.Domain.Entities
{
    public partial class Product
    {
        public class Builder : Builder<Product>
        {
            public Builder(INotificationHandler handler) : base(handler)
            {
            }
            public Builder(INotificationHandler handler, Product instance) : base(handler, instance)
            {
            }

            public virtual Builder WithId(Guid id)
            {
                Instance.Id = id;
                return this;
            }
            public virtual Builder WithCode(string code)
            {
                Instance.Code = code;
                return this;
            }
            public virtual Builder WithProductName(string productName)
            {
                Instance.ProductName = productName;
                return this;
            }
            public virtual Builder WithProductCategoryId(Guid productCategoryId)
            {
                Instance.ProductCategoryId = productCategoryId;
                return this;
            }
            public virtual Builder WithUnitMeasurementId(Guid unitMeasurementId)
            {
                Instance.UnitMeasurementId = unitMeasurementId;
                return this;
            }
            public virtual Builder WithNcmId(Guid? ncmId)
            {
                Instance.NcmId = ncmId;
                return this;
            }
            public virtual Builder WithBarCode(string barCode)
            {
                Instance.BarCode = barCode;
                return this;
            }
            public virtual Builder WithIsActive(bool isActive)
            {
                Instance.IsActive = isActive;
                return this;
            }
            public virtual Builder WithPropertyId(int? propertyId)
            {
                Instance.PropertyId = propertyId;
                return this;
            }
            public virtual Builder WithChainId(int? chainId)
            {
                Instance.ChainId = chainId;
                return this;
            }
            public virtual Builder WithTenantId(Guid? tenantId)
            {
                Instance.TenantId = tenantId;
                return this;
            }
            public virtual Builder WithIsDeleted(bool isDeleted)
            {
                Instance.IsDeleted = isDeleted;
                return this;
            }
            public virtual Builder WithCreationTime(DateTime creationTime)
            {
                Instance.CreationTime = creationTime;
                return this;
            }
            public virtual Builder WithCreatorUserId(Guid creatorUserId)
            {
                Instance.CreatorUserId = creatorUserId;
                return this;
            }
            public virtual Builder WithLastModificationTime(DateTime? lastModificationTime)
            {
                Instance.LastModificationTime = lastModificationTime;
                return this;
            }
            public virtual Builder WithLastModifierUserId(Guid? lastModifierUserId)
            {
                Instance.LastModifierUserId = lastModifierUserId;
                return this;
            }
            public virtual Builder WithDeletionTime(DateTime? deletionTime)
            {
                Instance.DeletionTime = deletionTime;
                return this;
            }
            public virtual Builder WithDeleterUserId(Guid? deleterUserId)
            {
                Instance.DeleterUserId = deleterUserId;
                return this;
            }

            protected override void Specifications()
            {
                AddSpecification(new ExpressionSpecification<Product>(
                AppConsts.LocalizationSourceName,
                Product.EntityError.ProductMustHaveCode,
                w => !string.IsNullOrWhiteSpace(w.Code)));

                AddSpecification(new ExpressionSpecification<Product>(
                AppConsts.LocalizationSourceName,
                Product.EntityError.ProductOutOfBoundCode,
                w => string.IsNullOrWhiteSpace(w.Code) || w.Code.Length > 0 && w.Code.Length <= 20));

                AddSpecification(new ExpressionSpecification<Product>(
                AppConsts.LocalizationSourceName,
                Product.EntityError.ProductMustHaveProductName,
                w => !string.IsNullOrWhiteSpace(w.ProductName)));

                AddSpecification(new ExpressionSpecification<Product>(
                AppConsts.LocalizationSourceName,
                Product.EntityError.ProductOutOfBoundProductName,
                w => string.IsNullOrWhiteSpace(w.ProductName) || w.ProductName.Length > 0 && w.ProductName.Length <= 200));

                AddSpecification(new ExpressionSpecification<Product>(
                AppConsts.LocalizationSourceName,
                Product.EntityError.ProductMustHaveBarCode,
                w => !string.IsNullOrWhiteSpace(w.BarCode)));

                AddSpecification(new ExpressionSpecification<Product>(
                AppConsts.LocalizationSourceName,
                Product.EntityError.ProductOutOfBoundBarCode,
                w => string.IsNullOrWhiteSpace(w.BarCode) || w.BarCode.Length > 0 && w.BarCode.Length <= 200));

            }
        }
    }
}
