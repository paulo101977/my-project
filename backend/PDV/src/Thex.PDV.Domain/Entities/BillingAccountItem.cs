﻿using System;

namespace Thex.PDV.Domain.Entities
{
    public partial class BillingAccountItem : BaseEntity
    {
        public BillingAccountItem()
        {
            this.WasReversed = false;
        }

        public Guid BillingAccountId { get; internal set; }
        public int? BillingItemId { get; internal set; }
        public int BillingAccountItemTypeId { get; internal set; }
        public decimal Amount { get; internal set; }
        public Guid CurrencyId { get; internal set; }
        public Guid? CurrencyExchangeReferenceId { get; internal set; }
        public Guid? CurrencyExchangeReferenceSecId { get; internal set; }
        public DateTime BillingAccountItemDate { get; internal set; }
        public Guid? BillingAccountItemParentId { get; internal set; }
        public string IntegrationDetail { get; internal set; }
        public DateTime? DateParameter { get; internal set; }
        public bool WasReversed { get; internal set; }

        public enum EntityError
        {
            BillingAccountItemMustHaveBillingAccountId,
            BillingAccountItemMustHaveBillingAccountItemTypeId,
            BillingAccountItemOutOfBoundBillingAccountItemComments,
            BillingAccountItemMustHaveCurrencyId,
            BillingAccountItemOutOfBoundCurrencySymbol,
            BillingAccountItemOutOfBoundCheckNumber,
            BillingAccountItemOutOfBoundNsu,
            BillingAccountItemInvalidBillingAccountItemDate,
            BillingAccountItemInvalidCreationTime,
            BillingAccountItemInvalidLastModificationTime
        }
    }
}
