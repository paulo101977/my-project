﻿namespace Thex.PDV.Domain.Entities
{
    public partial class UnitMeasurement : BaseEntity
    {
        public string Name { get; internal set; }

        public enum EntityError
        {
            UnitMeasurementMustHaveName,
            UnitMeasurementOutOfBoundName
        }
    }
}
