﻿using System;

namespace Thex.PDV.Domain.Entities
{
    public partial class ProductBillingItem : BaseEntity
    {
        public Guid ProductId { get; internal set; }
        public int BillingItemId { get; internal set; }
        public decimal? UnitPrice { get; internal set; }

        public enum EntityError
        {
            ProductBillingItemInvalidCreationTime,
            ProductBillingItemInvalidLastModificationTime,
            ProductBillingItemInvalidDeletionTime
        }
    }
}
