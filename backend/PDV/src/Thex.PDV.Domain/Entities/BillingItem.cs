﻿using System;
using Tnf.Notifications;

namespace Thex.PDV.Domain.Entities
{
    public partial class BillingItem : BaseEntityId
    {
        public int BillingItemId { get; set; }
        public int BillingItemTypeId { get; internal set; }
        public string BillingItemName { get; internal set; }
        public int? BillingItemCategoryId { get; internal set; }
        public string Description { get; internal set; }
        public int? PaymentTypeId { get; internal set; }
        public Guid? PlasticBrandPropertyId { get; internal set; }
        public Guid? AcquirerId { get; internal set; }
        public short? MaximumInstallmentsQuantity { get; internal set; }
        public string IntegrationCode { get; internal set; }

        public enum EntityError
        {
            BillingItemOutOfBoundBillingItemName,
            BillingItemOutOfBoundDescription,
            BillingItemOutOfBoundIntegrationCode,
            BillingItemInvalidCreationTime,
            BillingItemInvalidLastModificationTime,
            BillingItemInvalidDeletionTime
        }

        public static Builder Create(INotificationHandler handler)
            => new Builder(handler);

        public static Builder Create(INotificationHandler handler, BillingItem instance)
            => new Builder(handler, instance);
    }
}
