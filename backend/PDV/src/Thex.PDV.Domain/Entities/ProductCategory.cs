﻿using System;
using Tnf.Notifications;

namespace Thex.PDV.Domain.Entities
{
    public partial class ProductCategory : BaseEntity 
    {
        public ProductCategory()
        {
            this.IsActive = true;
            this.IsDeleted = false;
        }

        public string CategoryName { get; internal set; }     
        public Guid? StandardCategoryId { get; internal set; }
        public string IconName { get; internal set; }
        public string IntegrationCode { get; set; }

        public enum EntityError
        {
            ProductCategoryMustHaveCategoryName,
            ProductCategoryOutOfBoundCategoryName,
            ProductCategoryOutOfBoundIconName,
            ProductCategoryInvalidCreationTime,
            ProductCategoryInvalidLastModificationTime,
            ProductCategoryInvalidDeletionTime
        }

        public static Builder Create(INotificationHandler handler)
            => new Builder(handler);

        public static Builder Create(INotificationHandler handler, ProductCategory instance)
            => new Builder(handler, instance);
    }
}
