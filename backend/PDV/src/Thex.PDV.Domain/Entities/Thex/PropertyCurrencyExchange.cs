﻿using System;

namespace Thex.PDV.Domain.Entities.Thex
{
    public partial class PropertyCurrencyExchange : BaseEntity
    {
        public DateTime PropertyCurrencyExchangeDate { get; internal set; }
        public Guid CurrencyId { get; internal set; }
        public decimal ExchangeRate { get; internal set; }
        public virtual Currency Currency { get; internal set; }

        public enum EntityError
        {
            PropertyCurrencyExchangeMustHavePropertyId,
            PropertyCurrencyExchangeInvalidPropertyCurrencyExchangeDate,
            PropertyCurrencyExchangeMustHaveCurrencyId,
            PropertyCurrencyExchangeIsExistQuotationForPeriod,
            PropertyCurrencyExchangeIsOnlyDateFuture
        }
    }
}
