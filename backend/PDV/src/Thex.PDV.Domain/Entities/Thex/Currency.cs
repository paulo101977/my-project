﻿using System.Collections.Generic;

namespace Thex.PDV.Domain.Entities.Thex
{
    public partial class Currency : BaseEntity
    {
        public Currency()
        {
            BillingAccountItemList = new HashSet<BillingAccountItem>();
        }

        public string TwoLetterIsoCode { get; internal set; }
        public string CurrencyName { get; internal set; }
        public string AlphabeticCode { get; internal set; }
        public string NumnericCode { get; internal set; }
        public int MinorUnit { get; internal set; }
        public string Symbol { get; internal set; }
        public decimal ExchangeRate { get; internal set; }

        public virtual ICollection<BillingAccountItem> BillingAccountItemList { get; internal set; }

        public enum EntityError
        {
            CurrencyMustHaveCountrySubdivisionId,
            CurrencyOutOfBoundTwoLetterIsoCode,
            CurrencyMustHaveCurrencyName,
            CurrencyOutOfBoundCurrencyName,
            CurrencyMustHaveAlphabeticCode,
            CurrencyOutOfBoundAlphabeticCode,
            CurrencyMustHaveNumnericCode,
            CurrencyOutOfBoundNumnericCode,
            CurrencyMustHaveSymbol,
            CurrencyOutOfBoundSymbol
        }
    }
}
