﻿using System;
using Thex.PDV.Domain.Entities;
using Tnf.Notifications;

namespace Thex.PDV.Domain.Entities
{
    public partial class BillingAccountItemDetail
    {
        public Guid Id { get; set; }
        public Guid BillingAccountItemId { get; internal set; }
        public decimal Amount { get; internal set; }
        public decimal Quantity { get; internal set; }
        public Guid ProductId { get; internal set; }

        public static Builder Create(INotificationHandler handler)
            => new Builder(handler);

        public static Builder Create(INotificationHandler handler, BillingAccountItemDetail instance)
            => new Builder(handler, instance);
    }
}
