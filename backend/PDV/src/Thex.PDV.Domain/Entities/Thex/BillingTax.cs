﻿using System;

namespace Thex.PDV.Domain.Entities.Thex
{
    public partial class BillingTax : BaseEntity
    {
        public int BillingItemId { get; internal set; }
        public int BillingItemTaxId { get; internal set; }
        public decimal TaxPercentage { get; internal set; }
        public DateTime BeginDate { get; internal set; }
        public DateTime? EndDate { get; internal set; }
        public virtual BillingItem BillingItem { get; internal set; }
        public virtual BillingItem BillingItemTax { get; internal set; }

        public enum EntityError
        {
            BillingTaxMustHaveBillingItemId,
            BillingTaxMustHaveBillingItemTaxId,
            BillingTaxInvalidBeginDate,
            BillingTaxInvalidEndDate
        }
    }
}
