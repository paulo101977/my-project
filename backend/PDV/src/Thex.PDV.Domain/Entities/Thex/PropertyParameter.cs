﻿namespace Thex.PDV.Domain.Entities.Thex
{
    public partial class PropertyParameter : BaseEntity
    {
        public int ApplicationParameterId { get; internal set; }
        public string PropertyParameterValue { get; internal set; }
        public string PropertyParameterMinValue { get; internal set; }
        public string PropertyParameterMaxValue { get; internal set; }
        public string PropertyParameterPossibleValues { get; internal set; }

        public enum EntityError
        {
            PropertyParameterMustHavePropertyId,
            PropertyParameterMustHaveApplicationParameterId,
            PropertyParameterOutOfBoundPropertyParameterMinValue,
            PropertyParameterOutOfBoundPropertyParameterMaxValue,
            PropertyParameterHourFormatInvalid,
            PropertyParameterCheckoutLargerCheckin,
            PropertyParameterAgeRangeInvalidad,
            PropertyParameterIsNotDeactivateChildren_2,
            PropertyParameterMustHaveDailyBillingItem,
            PropertyParameterMustHaveTimeZone
        }
    }
}
