﻿using System;

namespace Thex.PDV.Domain.Entities.Thex
{
    public partial class BillingAccount : BaseEntity
    {
        public BillingAccount()
        {

        }

        public Guid? CompanyClientId { get; internal set; }
        public long? ReservationId { get; internal set; }
        public long? ReservationItemId { get; internal set; }
        public long? GuestReservationItemId { get; internal set; }
        public int BillingAccountTypeId { get; internal set; }
        public DateTime StartDate { get; internal set; }
        public DateTime? EndDate { get; internal set; }
        public int StatusId { get; internal set; }
        public bool IsMainAccount { get; internal set; }
        public int MarketSegmentId { get; internal set; }
        public int BusinessSourceId { get; internal set; }
        public Guid GroupKey { get; internal set; }
        public bool Blocked { get; internal set; }
        public string BillingAccountName { get; internal set; }
        public int? ReopeningReasonId { get; internal set; }
        public DateTime? ReopeningDate { get; internal set; }

        public enum EntityError
        {
            BillingAccountMustHavePersonId,
            BillingAccountMustHaveBillingAccountTypeId,
            BillingAccountInvalidStartDate,
            BillingAccountInvalidEndDate,
            BillingAccountTypeInvalid,
            BillingAccountPersonIdRequired,
            BillingAccountReservationIdRequired,
            BillingAccountStatusIdInvalidate,
            BillingAccountMustHaveStatusId,

            BillingAccountReservationIdIsNull,
            BillingAccountMustHaveMarketSegmentId,
            BillingAccountMustHaveBusinessSourceId,
            BillingAccountMustHavePropertyId,
            BillingAccountHasAlreadyCreated
        }
    }
}
