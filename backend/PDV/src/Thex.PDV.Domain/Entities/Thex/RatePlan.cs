﻿using System;

namespace Thex.PDV.Domain.Entities.Thex
{
    public partial class RatePlan : BaseEntity
    {
        public int AgreementTypeId { get; internal set; }
        public string AgreementName { get; internal set; }
        public DateTime StartDate { get; internal set; }
        public DateTime? EndDate { get; internal set; }
        public int MealPlanTypeId { get; internal set; }
        public Guid CurrencyId { get; internal set; }
        public string CurrencySymbol { get; internal set; }
        public bool RateNet { get; internal set; }
        public int RateTypeId { get; internal set; }
        public int? MarketSegmentId { get; set; }
        public virtual Currency Currency { get; internal set; }

        public enum EntityError
        {
            RatePlanMustHaveAgreementTypeId,
            RatePlanMustHaveAgreementName,
            RatePlanOutOfBoundAgreementName,
            RatePlanInvalidStartDate,
            RatePlanInvalidEndDate,
            RatePlanMustHavePropertyId,
            RatePlanMustHaveRoomTypeId,
            RatePlanMustHavePropertyMealPlanTypeId,
            RatePlanMustHaveCurrencyId,
            RatePlanOutOfBoundCurrencySymbol,
            RatePlanNotNameAgreementSamePeriod,
            RatePlanNotDateStartGreaterThanDateEnd,
            RatePlanStartDateNotMustBeLessCurrentDate
        }
    }
}
