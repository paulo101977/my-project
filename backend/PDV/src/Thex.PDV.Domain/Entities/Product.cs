﻿using System;
using Tnf.Notifications;

namespace Thex.PDV.Domain.Entities
{
    public partial class Product : BaseEntity
    {
        public string Code { get; internal set; }
        public string ProductName { get; internal set; }
        public Guid ProductCategoryId { get; internal set; }
        public Guid UnitMeasurementId { get; internal set; }
        public Guid? NcmId { get; internal set; }
        public string BarCode { get; internal set; }

        public enum EntityError
        {
            ProductMustHaveCode,
            ProductOutOfBoundCode,
            ProductMustHaveProductName,
            ProductOutOfBoundProductName,
            ProductMustHaveBarCode,
            ProductOutOfBoundBarCode,
            ProductInvalidCreationTime,
            ProductInvalidLastModificationTime,
            ProductInvalidDeletionTime
        }

        public static Builder Create(INotificationHandler handler)
            => new Builder(handler);

        public static Builder Create(INotificationHandler handler, Product instance)
            => new Builder(handler, instance);
    }
}
