﻿namespace Thex.PDV.Domain.Entities
{
    public partial class Ncm : BaseEntity
    {
        public string Code { get; internal set; }
        public string Description { get; internal set; }

        public enum EntityError
        {
            NcmMustHaveCode,
            NcmOutOfBoundCode,
            NcmMustHaveDescription,
            NcmOutOfBoundDescription
        }
    }
}
