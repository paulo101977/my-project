﻿using System;
using System.Threading.Tasks;
using Thex.Common;
using Thex.Common.Enumerations;
using Thex.GenericLog;
using Thex.Infra.Extensions;
using Thex.Kernel;
using Thex.PDV.Domain.Entities;
using Thex.PDV.Dto;
using Thex.PDV.Infra.Context;
using Thex.PDV.Infra.Interfaces;
using Thex.PDV.Infra.Interfaces.ReadRepositories;
using Tnf.Dapper.Repositories;
using Tnf.Dto;
using Tnf.Notifications;
using Tnf.Repositories;

namespace Thex.PDV.Infra.Repositories
{
    public class ProductBillingItemRepository : DapperEfRepositoryBase<ThexPdvContext, ProductBillingItem>, IProductBillingItemRepository
    {
        private readonly IApplicationUser _applicationUser;
        private readonly INotificationHandler _notificationHandler;
        private readonly IProductBillingItemReadRepository _productBillingItemReadRepository;

        public ProductBillingItemRepository(
            IActiveTransactionProvider activeTransactionProvider,
            IApplicationUser applicationUser,
            INotificationHandler notificationHandler,
            IProductBillingItemReadRepository productBillingItemReadRepository,
            IGenericLogHandler genericLogHandler) : base(activeTransactionProvider, genericLogHandler)
        {
            _applicationUser = applicationUser;
            _notificationHandler = notificationHandler;
            _productBillingItemReadRepository = productBillingItemReadRepository;
        }

        public async Task<IDto> InsertAsync(BaseEntity entity)
        {
            // Ajustes Dapper
            entity.IsActive = true;
            entity.IsDeleted = false;

            await base.InsertAsync(entity: entity as ProductBillingItem);

            return entity.MapTo<ProductBillingItemDto>();
        }

        public async Task RemoveAsync(Guid id)
        {
            var entity = await _productBillingItemReadRepository.GetEntityProductBillingItem(id);

            if (entity == null)
            {
                _notificationHandler.Raise(_notificationHandler
                                    .DefaultBuilder
                                    .WithMessage(AppConsts.LocalizationSourceName, CommonsEnum.Error.ParameterInvalid)
                                    .Build());
                return;
            }

            await base.DeleteAsync(entity: entity as ProductBillingItem);
        }

        public async Task ToggleAsync(Guid id)
        {
            var dbProductBillingItem = await _productBillingItemReadRepository.GetEntityProductBillingItem(id);

            if (dbProductBillingItem == null)
            {
                _notificationHandler.Raise(_notificationHandler
                                    .DefaultBuilder
                                    .WithMessage(AppConsts.LocalizationSourceName, CommonsEnum.Error.ParameterInvalid)
                                    .Build());
                return;
            }

            dbProductBillingItem.IsActive = !dbProductBillingItem.IsActive;

            await base.UpdateAsync(entity: dbProductBillingItem as ProductBillingItem);
        }

        public async Task<IDto> UpdateAsync(Guid id, BaseEntity entity)
        {
            await base.UpdateAsync(entity: entity as ProductBillingItem);

            return entity.MapTo<ProductBillingItemDto>();
        }
    }
}
