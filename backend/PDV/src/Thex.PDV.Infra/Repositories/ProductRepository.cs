﻿using System;
using System.Threading.Tasks;
using Thex.Common;
using Thex.Common.Enumerations;
using Thex.GenericLog;
using Thex.Infra.Extensions;
using Thex.Kernel;
using Thex.PDV.Domain.Entities;
using Thex.PDV.Dto;
using Thex.PDV.Infra.Context;
using Thex.PDV.Infra.Interfaces;
using Thex.PDV.Infra.Interfaces.ReadRepositories;
using Tnf.Dapper.Repositories;
using Tnf.Dto;
using Tnf.Notifications;
using Tnf.Repositories;

namespace Thex.PDV.Infra.Repositories
{
    public class ProductRepository : DapperEfRepositoryBase<ThexPdvContext, Product>, IProductRepository
    {
        private readonly IApplicationUser _applicationUser;
        private readonly INotificationHandler _notificationHandler;
        private readonly IProductReadRepository _productReadRepository;

        public ProductRepository(
            IProductReadRepository productReadRepository,
            IActiveTransactionProvider activeTransactionProvider,
            IApplicationUser applicationUser,
            INotificationHandler notificationHandler,
            IGenericLogHandler genericLogHandler) : base(activeTransactionProvider, genericLogHandler)
        {
            _applicationUser = applicationUser;
            _notificationHandler = notificationHandler;
            _productReadRepository = productReadRepository;
        }

        public async Task<IDto> InsertAsync(BaseEntity entity)
        {
            await base.InsertAsync(entity: entity as Product);

            return entity.MapTo<ProductDto>();
        }

        public async Task<IDto> UpdateAsync(Guid id, BaseEntity entity)
        {
            var dbProduct = await _productReadRepository.GetProductById(id);

            if (dbProduct == null)
            {
                _notificationHandler.Raise(_notificationHandler
                                    .DefaultBuilder
                                    .WithMessage(AppConsts.LocalizationSourceName, CommonsEnum.Error.ParameterInvalid)
                                    .Build());
                return ProductDto.NullInstance;
            }

            // Ajustes dapper
            entity.Id = dbProduct.Id;
            await base.UpdateAsync(entity: entity as Product);

            if (_notificationHandler.HasNotification())
                return ProductDto.NullInstance;

            return entity.MapTo<ProductDto>();
        }

        public async Task<IDto> UpdateEntity(Product product)
        {
            await base.UpdateAsync(entity: product);

            if (_notificationHandler.HasNotification())
                return ProductDto.NullInstance;

            return product.MapTo<ProductDto>();
        }

        public async Task RemoveAsync(Guid id)
        {
            var entity = await _productReadRepository.GetProductEntityById(id);

            if (entity == null)
            {
                _notificationHandler.Raise(_notificationHandler
                                    .DefaultBuilder
                                    .WithMessage(AppConsts.LocalizationSourceName, CommonsEnum.Error.ParameterInvalid)
                                    .Build());
                return;
            }

            await base.DeleteAsync(entity: entity as Product);
        }

        public async Task ToggleAsync(Guid id)
        {
            var dbEntity = await _productReadRepository.GetProductEntityById(id);

            if (dbEntity == null)
            {
                _notificationHandler.Raise(_notificationHandler
                                    .DefaultBuilder
                                    .WithMessage(AppConsts.LocalizationSourceName, CommonsEnum.Error.ParameterInvalid)
                                    .Build());
                return;
            }

            dbEntity.IsActive = !dbEntity.IsActive;

            await base.UpdateAsync(entity: dbEntity as Product);
        }
    }
}
