﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Thex.GenericLog;
using Thex.Kernel;
using Thex.PDV.Domain.Entities;
using Thex.PDV.Domain.Entities.Thex;
using Thex.PDV.Dto;
using Thex.PDV.Dto.Dto.BillingItemServiceAndTaxAssociate;
using Thex.PDV.Infra.Context;
using Thex.PDV.Infra.Interfaces.ReadRepositories;
using Tnf.Dapper.Repositories;
using Tnf.Dto;
using Tnf.Repositories;

namespace Thex.PDV.Infra.Repositories.ReadRepositories
{
    public class BillingItemReadRepository : DapperEfRepositoryBase<ThexPdvContext, BillingItem>, IBillingItemReadRepository
    {
        private readonly IApplicationUser _applicationUser;

        public BillingItemReadRepository(IActiveTransactionProvider activeTransactionProvider,
                                         IApplicationUser applicationUser,
                                         IGenericLogHandler genericLogHandler)
                                         : base(activeTransactionProvider, genericLogHandler)
        {
            _applicationUser = applicationUser;
        }

        public async Task<bool> AnyBillingItemByBillingItemType(int billingItemId, int billingItemType, bool ignoreFilters = false)
        {
            var tId = _applicationUser.TenantId;
            var pId = _applicationUser.PropertyId;

            var query = await QueryAsync<BillingItem>(@"
                SELECT BillingItem.BillingItemId as Id,
                       BillingItem.*
                FROM BillingItem
                WHERE BillingItem.PropertyId = @PropertyId
                AND BillingItem.isActive = 1
                AND BillingItem.BillingItemTypeId = @BillingItemTypeId
                AND BillingItem.BillingItemId = @BillingItemId
                AND BillingItem.TenantId = @TenantId",
                new
                {
                    PropertyId = pId,
                    TenantId = tId,
                    BillingItemTypeId = billingItemType,
                    BillingItemId = billingItemId
                });

            return (query.ToList().Count > 0);
        }

        public async Task<IListDto<BillingItemDto>> GetAll(GetAllBillingItemDto requestDto)
        {
            var tId = _applicationUser.TenantId;
            var pId = _applicationUser.PropertyId;

            var ret = new ListDto<BillingItemDto>();
            ret.HasNext = false;

            var query = await QueryAsync<BillingItemDto>(@"
            SELECT BillingItem.BillingItemId as Id,
                   BillingItem.BillingItemId as BillingItemId,
                   BillingItem.*,
                   BillingItemCategory.CategoryName as BillingItemCategoryName,
                   BillingItemCategory.BillingItemCategoryId as BillingItemCategoryId
            FROM  BillingItem
            INNER JOIN BillingItemCategory ON BillingItem.BillingItemCategoryId = BillingItemCategory.BillingItemCategoryId AND BillingItemCategory.isDeleted = 0
            WHERE BillingItem.PropertyId = @PropertyId
            AND BillingItem.isDeleted = 0
            AND BillingItem.isActive = 1
            AND BillingItem.BillingItemTypeId = 4
            AND BillingItem.TenantId = @TenantId",
                        new
                        {
                            PropertyId = pId,
                            TenantId = tId
                        });

            query.ToList().ForEach(billingItem =>
            {
                ret.Items.Add(billingItem);
            });

            return ret;
        }

        public List<BillingItemDto> GetBillinItemFromProduct(Guid productId)
        {
            var tId = _applicationUser.TenantId;
            var pId = _applicationUser.PropertyId;

            var ret = new List<BillingItemDto>();

            var query = Query<BillingItem>(@"
                SELECT BillingItem.BillingItemId as Id,
                       BillingItem.*
                FROM ProductBillingItem
                LEFT JOIN BillingItem ON
                          BillingItem.BillingItemId = ProductBillingItem.BillingItemId
                 WHERE ProductBillingItem.TenantId = @TenantId
                   AND BillingItem.PropertyId = @PropertyId
                   AND ProductBillingItem.ProductId = @ProductId
                   AND ProductBillingItem.isDeleted = 0",
                new
                {
                    PropertyId = pId,
                    TenantId = tId,
                    ProductId = productId
                });

            query.ToList().ForEach(billingItem =>
            {
                ret.Add(billingItem.MapTo<BillingItemDto>());
            });

            return ret;
        }

        public BillingItem GetById(int id)
        {
            var tId = _applicationUser.TenantId;
            var pId = _applicationUser.PropertyId;

            var query = Query<BillingItem>(@"
                SELECT BillingItem.BillingItemId as Id,
                       BillingItem.*
                FROM BillingItem
                WHERE BillingItem.TenantId = @TenantId
                AND BillingItem.PropertyId = @PropertyId
                AND BillingItem.isActive = 1
                AND BillingItem.isDeleted = 0",
                new
                {
                    PropertyId = pId,
                    TenantId = tId,
                    BillingItemId = id
                });

            return query.FirstOrDefault();
        }

        public IList<BillingItemServiceAndTaxAssociateDto> GetTaxesForServices(IList<int> serviceIdsList)
        {
            var ret = new List<BillingItemServiceAndTaxAssociateDto>();

            var tId = _applicationUser.TenantId;
            var pId = _applicationUser.PropertyId;

            var query = Query<BillingTax>(@"
               SELECT BillingTax.BillingTaxId as Id,
                      BillingTax.*
                      FROM BillingTax
                      LEFT JOIN BillingItem ON BillingItem.BillingItemId = BillingTax.BillingItemTaxId
                      WHERE BillingTax.IsActive = 1
                      AND BillingTax.BillingItemId IN @ids
                      AND BillingTax.TenantId = @TenantId
                      AND BillingItem.PropertyId = @PropertyId",
                new
                {
                    PropertyId = pId,
                    TenantId = tId,
                    ids = serviceIdsList.ToArray()
                });

            query.ToList().ForEach(tax =>
            {
                var billingItem = GetById(tax.BillingItemId);

                ret.Add(new BillingItemServiceAndTaxAssociateDto()
                {
                    BillingItemTaxId = tax.BillingItemTaxId,
                    TaxPercentage = tax.TaxPercentage
                });
            });

            return ret;
        }

        public async Task<bool> IsServiceAndExistSupportedType(int billingItemId)
        {
            var tId = _applicationUser.TenantId;
            var pId = _applicationUser.PropertyId;

            var query = await QueryAsync<BillingItem>(@"
                SELECT BillingItem.BillingItemId as Id,
                       BillingItem.*
                FROM BillingItem
                RIGHT JOIN BillingInvoicePropertySupportedType ON
                           BillingInvoicePropertySupportedType.BillingItemId = BillingItem.BillingItemId
                WHERE BillingItem.PropertyId = @PropertyId
                AND BillingItem.isActive = 1
                AND BillingItem.BillingItemId = @BillingItemId
                AND BillingItem.TenantId = @TenantId",
                new
                {
                    PropertyId = pId,
                    TenantId = tId,
                    BillingItemId = billingItemId
                });

            return (query.ToList().Count > 0);
        }
    }
}
