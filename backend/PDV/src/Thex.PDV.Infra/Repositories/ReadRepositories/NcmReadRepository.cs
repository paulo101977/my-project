﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Thex.GenericLog;
using Thex.Kernel;
using Thex.PDV.Domain.Entities;
using Thex.PDV.Dto;
using Thex.PDV.Dto.GetAll;
using Thex.PDV.Infra.Context;
using Thex.PDV.Infra.Interfaces.ReadRepositories;
using Tnf.Dapper.Repositories;
using Tnf.Dto;
using Tnf.Repositories;

namespace Thex.PDV.Infra.Repositories.ReadRepositories
{
    public class NcmReadRepository : DapperEfRepositoryBase<ThexPdvContext, Ncm>, INcmReadRepository
    {
        private readonly IApplicationUser _applicationUser;

        public NcmReadRepository(
               IActiveTransactionProvider activeTransactionProvider,
               IApplicationUser applicationUser,
               IGenericLogHandler genericLogHandler) : base(activeTransactionProvider, genericLogHandler)
        {
            _applicationUser = applicationUser;
        }

        public async Task<IListDto<NcmDto>> GetAll(GetAllNcmDto requestDto)
        {
            var ret = new ListDto<NcmDto>();
            ret.HasNext = false;

            var query = await QueryAsync<Ncm>(@"
                SELECT Ncm.NcmId as Id,
                       Ncm.*
                FROM Ncm");

            query.ToList().ForEach(ncm =>
            {
                ret.Items.Add(ncm.MapTo<NcmDto>());
                ret.HasNext = false;
            });

            return ret;
        }

        public async Task<IListDto<NcmDto>> GetAllByFilters(SearchNcmDto searchDto)
        {
            var query = base.Query<NcmDto>(@"
                SELECT Ncm.NcmId as Id,
                       Ncm.*
                FROM Ncm
                WHERE 
                (
				    (
				       Ncm.Code like @Param + '%'
				       OR Ncm.Code like '%' + @Param + '%'
				       OR Ncm.Code like '%' + @Param
				       OR Ncm.Code = @Param
			        )	 
                    OR 
				    (
				       Ncm.Description like @Param + '%'
				       OR Ncm.Description like '%' + @Param + '%'
				       OR Ncm.Description like '%' + @Param
				       OR Ncm.Description = @Param
			        ))",
                new
                {
                    searchDto.Param
                });

            return await query.ToListDtoAsync(searchDto);
        }

        public async Task<NcmDto> GetNcmById(Guid id)
        {
            var ret = new ListDto<NcmDto>();
            ret.HasNext = false;

            var query = await QueryAsync<Ncm>(@"
                SELECT Ncm.NcmId as Id,
                       Ncm.*
                FROM Ncm
                WHERE NcmId = @Id",
                new
                {
                    Id = id
                });

            return query.Count() == 0 ? null : query.FirstOrDefault().MapTo<NcmDto>();
        }

        public async Task<Ncm> GetNcmEntityById(Guid id)
        {
            var query = await QueryAsync<Ncm>(@"
                SELECT Ncm.NcmId as Id,
                       Ncm.*
                FROM Ncm
                WHERE NcmId = @Id",
                new
                {
                    Id = id
                });

            return query.FirstOrDefault();
        }
    }
}
