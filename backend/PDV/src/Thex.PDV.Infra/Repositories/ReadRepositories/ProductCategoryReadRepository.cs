﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Thex.Common;
using Thex.Common.Enumerations;
using Thex.GenericLog;
using Thex.Kernel;
using Thex.PDV.Domain.Entities;
using Thex.PDV.Dto;
using Thex.PDV.Infra.Context;
using Thex.PDV.Infra.Interfaces.ReadRepositories;
using Tnf.Dapper.Repositories;
using Tnf.Dto;
using Tnf.Localization;
using Tnf.Repositories;

namespace Thex.PDV.Infra.Repositories.ReadRepositories
{
    public class ProductCategoryReadRepository : DapperEfRepositoryBase<ThexPdvContext, ProductCategory>, IProductCategoryReadRepository
    {
        private readonly IApplicationUser _applicationUser;
        private readonly ILocalizationManager _localizationManager;

        public ProductCategoryReadRepository(

        IActiveTransactionProvider activeTransactionProvider,
               IApplicationUser applicationUser,
               IGenericLogHandler genericLogHandler,
               ILocalizationManager localizationManager) : base(activeTransactionProvider, genericLogHandler)
        {
            _applicationUser = applicationUser;
            _localizationManager = localizationManager;
        }

        public async Task<IListDto<ProductCategoryDto>> GetAllGroupFixed()
        {
            var ret = new ListDto<ProductCategoryDto>();
            ret.HasNext = false;

            var query = await QueryAsync<ProductCategory>(@"
                SELECT productCategory.ProductCategoryId as Id,
                       productCategory.*
                FROM ProductCategory productCategory
                WHERE StandardCategoryId is null
                AND isActive = 1
                AND isDeleted = 0");

            query.ToList().ForEach(prodCategory =>
            {
                prodCategory.CategoryName = _localizationManager.GetString(AppConsts.LocalizationSourceName, prodCategory.CategoryName);
                ret.Items.Add(prodCategory.MapTo<ProductCategoryDto>());
                ret.HasNext = false;
            });

            return ret;
        }

        public async Task<IListDto<ProductCategoryDto>> GetAllCategories(GetAllProductCategoryDto request)
        {
            var tId = _applicationUser.TenantId;
            var pId = _applicationUser.PropertyId;
            var cId = _applicationUser.ChainId;

            var ret = new ListDto<ProductCategoryDto>();
            ret.HasNext = false;

            var query = await QueryAsync<ProductCategoryDto>(@"
            SELECT ProductCategory.ProductCategoryId as Id,
                   ProductCategory.*,
                   GroupFixed.CategoryName as GroupFixedName
            FROM ProductCategory productCategory,
            (SELECT ProductCategory.ProductCategoryId as Id,
                   ProductCategory.*
            FROM ProductCategory productCategory
            WHERE ProductCategory.StandardCategoryId is null
            AND ProductCategory.isDeleted = 0) as GroupFixed
            WHERE ProductCategory.TenantId = @TenantId
            AND ProductCategory.ChainId = @ChainId
            AND ProductCategory.StandardCategoryId is not null
            AND ProductCategory.PropertyId = @PropertyId
            AND ProductCategory.isDeleted = 0
            AND GroupFixed.Id = productCategory.StandardCategoryId",
                new
                {
                    PropertyId = pId,
                    TenantId = tId,
                    ChainId = cId
                });

            query.ToList().ForEach(category =>
            {
                category.GroupFixedName = _localizationManager.GetString(AppConsts.LocalizationSourceName, category.GroupFixedName);
                ret.Items.Add(category.MapTo<ProductCategoryDto>());
                ret.HasNext = false;
            });

            return ret;
        }

        public async Task<IListDto<ProductCategoryDto>> GetAllByBillingItemId(GetAllProductCategoryDto request)
        {
            var tId = _applicationUser.TenantId;
            var pId = _applicationUser.PropertyId;
            var cId = _applicationUser.ChainId;

            var ret = new ListDto<ProductCategoryDto>();
            ret.HasNext = false;

            var query = await QueryAsync<ProductCategoryDto>(@"
            select pc.CategoryName,
	            pc.ChainId,
	            GroupFixed.CategoryName as GroupFixedName,
	            pc.IconName,
	            pc.ProductCategoryId as Id,
	            pc.IsActive,
	            pc.IsDeleted,
	            pc.PropertyId,
	            pc.StandardCategoryId,
	            pc.TenantId
            from   productBillingItem pbi,
                   Product p,
	               ProductCategory pc,
	               (SELECT ProductCategory.ProductCategoryId as Id,
                    ProductCategory.*
		            FROM ProductCategory productCategory
                    WHERE ProductCategory.StandardCategoryId is null
                    AND ProductCategory.isActive = 1
                    AND ProductCategory.isDeleted = 0) as GroupFixed
            where  pbi.ProductId = p.ProductId
	            and pc.ProductCategoryId = p.ProductCategoryId
	            and GroupFixed.Id = pc.StandardCategoryId
	            and pbi.BillingItemId = COALESCE(@BillingItemId, pbi.BillingItemId)
	            and pc.TenantId = @TenantId
	            and pc.ChainId = @ChainId
	            and pc.StandardCategoryId is not null
	            and pc.PropertyId = @PropertyId
	            and pc.isActive = 1
	            and pc.isDeleted = 0
            Group by pc.CategoryName,
	            pc.ChainId,
	            GroupFixed.CategoryName,
	            pc.IconName,
	            pc.ProductCategoryId,
	            pc.IsActive,
	            pc.IsDeleted,
	            pc.PropertyId,
	            pc.StandardCategoryId,
	            pc.TenantId",
                new
                {
                    request.BillingItemId,
                    PropertyId = pId,
                    TenantId = tId,
                    ChainId = cId
                });

            query.ToList().ForEach(category =>
            {
                category.GroupFixedName = _localizationManager.GetString(AppConsts.LocalizationSourceName, category.GroupFixedName);

                ret.Items.Add(category.MapTo<ProductCategoryDto>());
                ret.HasNext = false;
            });

            return ret;
        }

        public async Task<ProductCategoryDto> GetCategoryById(Guid id)
        {
            var tId = _applicationUser.TenantId;
            var pId = _applicationUser.PropertyId;
            var cId = _applicationUser.ChainId;

            var ret = new ListDto<ProductCategoryDto>();
            ret.HasNext = false;

            var query = await QueryAsync<ProductCategoryDto>(@"
            SELECT ProductCategory.ProductCategoryId as Id,
                   ProductCategory.*,
                   GroupFixed.CategoryName as GroupFixedName
            FROM ProductCategory productCategory,
            (SELECT ProductCategory.ProductCategoryId as Id,
                   ProductCategory.*
            FROM ProductCategory productCategory
            WHERE ProductCategory.TenantId = @TenantId
            AND ProductCategory.ChainId = @ChainId
            AND ProductCategory.StandardCategoryId is null
            AND ProductCategory.PropertyId = @PropertyId
            AND ProductCategory.isActive = 1
            AND ProductCategory.isDeleted = 0) as GroupFixed
            WHERE ProductCategory.TenantId = @TenantId
            AND ProductCategory.ChainId = @ChainId
            AND ProductCategory.StandardCategoryId is not null
            AND ProductCategory.PropertyId = @PropertyId
            AND ProductCategory.isActive = 1
            AND ProductCategory.isDeleted = 0
            AND GroupFixed.Id = productCategory.StandardCategoryId
            AND ProductCategory.ProductCategoryId = @Id",
                new
                {
                    Id = id,
                    PropertyId = pId,
                    TenantId = tId,
                    ChainId = cId
                });

            var category = query.Count() == 0 ? null : query.FirstOrDefault().MapTo<ProductCategoryDto>();

            if (category != null)
                category.GroupFixedName = _localizationManager.GetString(AppConsts.LocalizationSourceName, category.GroupFixedName);

            return category;
        }

        public async Task<ProductCategory> GetCategoyEntityById(Guid id)
        {
            var tId = _applicationUser.TenantId;
            var pId = _applicationUser.PropertyId;
            var cId = _applicationUser.ChainId;

            var query = await QueryAsync<ProductCategory>(@"
                SELECT ProductCategory.ProductCategoryId as Id,
                       ProductCategory.*
                FROM ProductCategory productCategory
                WHERE ProductCategory.TenantId = @TenantId
                AND ProductCategory.ProductCategoryId = @Id
                AND ProductCategory.ChainId = @ChainId
                AND ProductCategory.StandardCategoryId is not null
                AND ProductCategory.PropertyId = @PropertyId
                AND ProductCategory.isDeleted = 0",
                new
                {
                    Id = id,
                    PropertyId = pId,
                    TenantId = tId,
                    ChainId = cId
                });

            return query.FirstOrDefault();

        }

        public async Task<ProductCategoryDto> GetOneById(Guid id)
        {
            var tId = _applicationUser.TenantId;
            var pId = _applicationUser.PropertyId;
            var cId = _applicationUser.ChainId;

            var query = await QueryAsync<ProductCategoryDto>(@"
                SELECT ProductCategory.ProductCategoryId as Id,
                       ProductCategory.*
                FROM ProductCategory productCategory
                WHERE ProductCategory.TenantId = @TenantId
                AND ProductCategory.ProductCategoryId = @Id
                AND ProductCategory.ChainId = @ChainId
                AND ProductCategory.PropertyId = @PropertyId
                AND ProductCategory.isDeleted = 0",
                new
                {
                    Id = id,
                    PropertyId = pId,
                    TenantId = tId,
                    ChainId = cId
                });

            var category = query.FirstOrDefault();

            if (category != null)
                category.CategoryName = category.StandardCategoryId != null ? category.CategoryName :
                                           _localizationManager.GetString(AppConsts.LocalizationSourceName, (category.CategoryName).ToString());

            return category;
        }
    }
}
