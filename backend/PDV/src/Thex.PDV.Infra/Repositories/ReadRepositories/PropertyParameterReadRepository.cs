﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Thex.GenericLog;
using Thex.Kernel;
using Thex.GenericLog;
using Thex.PDV.Domain.Entities.Thex;
using Thex.PDV.Infra.Context;
using Thex.PDV.Infra.Interfaces.ReadRepositories;
using Tnf.Dapper.Repositories;
using Tnf.Repositories;

namespace Thex.PDV.Infra.Repositories.ReadRepositories
{
    public class PropertyParameterReadRepository : DapperEfRepositoryBase<ThexPdvContext, PropertyParameter>, IPropertyParameterReadRepository
    {
        private readonly IApplicationUser _applicationUser;

        public PropertyParameterReadRepository(
               IActiveTransactionProvider activeTransactionProvider,
               IApplicationUser applicationUser,
               IGenericLogHandler genericLogHandler) : base(activeTransactionProvider, genericLogHandler)
        {
            _applicationUser = applicationUser;
        }

        public async Task<PropertyParameter> GetByPropertyIdAndApplicationParameterId(int propertyId, int applicationParameterId)
        {
            var query = await QueryAsync<PropertyParameter>(@"
                SELECT PropertyParameter.PropertyParameterId as Id,
                       PropertyParameter.* from PropertyParameter
                WHERE PropertyParameter.PropertyId = @PropertyId
                AND PropertyParameter.ApplicationParameterId = @ApplicationParameterId",
                new
                {
                    PropertyId = propertyId,
                    ApplicationParameterId = applicationParameterId
                });

            return query.FirstOrDefault(); ;
        }

        public async Task<DateTime> GetSystemDate(int propertyId)
        {
            const int applicationParameterId = 10;

            var propertyParameter = await GetByPropertyIdAndApplicationParameterId(propertyId, applicationParameterId);
            if (propertyParameter == null || (propertyParameter != null && propertyParameter.PropertyParameterValue == null))
            {
                return default(DateTime);
            }
            else
            {
                try
                {
                    var value = propertyParameter.PropertyParameterValue.Split("/");
                    return new DateTime(Convert.ToInt32(value[2]), Convert.ToInt32(value[1]), Convert.ToInt32(value[0]));
                }
                catch
                {
                    var value = propertyParameter.PropertyParameterValue.Split("-");
                    return new DateTime(Convert.ToInt32(value[0]), Convert.ToInt32(value[1]), Convert.ToInt32(value[2]));
                }

            }
        }
    }
}