﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Thex.GenericLog;
using Thex.Kernel;
using Thex.GenericLog;
using Thex.PDV.Domain.Entities;
using Thex.PDV.Dto;
using Thex.PDV.Infra.Context;
using Thex.PDV.Infra.Interfaces.ReadRepositories;
using Tnf.Dapper.Repositories;
using Tnf.Dto;
using Tnf.Repositories;

namespace Thex.PDV.Infra.Repositories.ReadRepositories
{
    public class UnitMeasurementReadRepository : DapperEfRepositoryBase<ThexPdvContext, UnitMeasurement>, IUnitMeasurementReadRepository
    {
        private readonly IApplicationUser _applicationUser;

        public UnitMeasurementReadRepository(
               IActiveTransactionProvider activeTransactionProvider,
               IApplicationUser applicationUser,
               IGenericLogHandler genericLogHandler) : base(activeTransactionProvider, genericLogHandler)
        {
            _applicationUser = applicationUser;
        }

        public async Task<IListDto<UnitMeasurementDto>> GetAll(GetAllUnitMeasurementDto requestDto)
        {

            var ret = new ListDto<UnitMeasurementDto>();
            ret.HasNext = false;

            var query = await QueryAsync<UnitMeasurement>(@"
                SELECT UnitMeasurement.UnitMeasurementId as Id,
                       UnitMeasurement.*
                FROM UnitMeasurement");

            query.ToList().ForEach(ncm =>
            {
                ret.Items.Add(ncm.MapTo<UnitMeasurementDto>());
                ret.HasNext = false;
            });

            return ret;
        }
    }
}
