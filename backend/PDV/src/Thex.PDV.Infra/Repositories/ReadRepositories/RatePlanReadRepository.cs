﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Thex.GenericLog;
using Thex.Kernel;
using Thex.PDV.Domain.Entities.Thex;
using Thex.PDV.Infra.Context;
using Thex.PDV.Infra.Interfaces.ReadRepositories;
using Tnf.Dapper.Repositories;
using Tnf.Repositories;

namespace Thex.PDV.Infra.Repositories.ReadRepositories
{
    public class RatePlanReadRepository : DapperEfRepositoryBase<ThexPdvContext, RatePlan>, IRatePlanReadRepository
    {
        private readonly IApplicationUser _applicationUser;

        public RatePlanReadRepository(
               IActiveTransactionProvider activeTransactionProvider,
               IApplicationUser applicationUser,
               IGenericLogHandler genericLogHandler) : base(activeTransactionProvider, genericLogHandler)
        {
            _applicationUser = applicationUser;
        }

        public async Task<RatePlan> GetByBillingAccountId(Guid billingAccountId)
        {
            var tId = _applicationUser.TenantId;
            var pId = _applicationUser.PropertyId;

            var query = await QueryAsync<RatePlan>(@"
                            SELECT
                            RatePlan.RatePlanId as Id,
                            RatePlan.*
                            FROM RatePlan
                            LEFT JOIN ReservationItem ON ReservationItem.RatePlanId = RatePlan.RatePlanId
                            LEFT JOIN BillingAccount ON BillingAccount.ReservationItemId = ReservationItem.ReservationId
                            WHERE BillingAccount.BillingAccountId = @BillingAccountId
                            AND RatePlan.PropertyId = @PropertyId
                            AND RatePlan.TenantId = @TenantId",
                new
                {
                    PropertyId = pId,
                    TenantId = tId,
                    BillingAccountId = billingAccountId
                });

            return query.FirstOrDefault();
        }
    }
}