﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Thex.GenericLog;
using Thex.Kernel;
using Thex.PDV.Domain.Entities;
using Thex.PDV.Dto;
using Thex.PDV.Infra.Context;
using Thex.PDV.Infra.Interfaces.ReadRepositories;
using Tnf.Dapper.Repositories;
using Tnf.Dto;
using Tnf.Repositories;

namespace Thex.PDV.Infra.Repositories.ReadRepositories
{
    public class ProductBillingItemReadRepository : DapperEfRepositoryBase<ThexPdvContext, ProductBillingItem>, IProductBillingItemReadRepository
    {
        private readonly IApplicationUser _applicationUser;

        public ProductBillingItemReadRepository(
               IActiveTransactionProvider activeTransactionProvider,
               IApplicationUser applicationUser,
               IGenericLogHandler genericLogHandler) : base(activeTransactionProvider, genericLogHandler)
        {
            _applicationUser = applicationUser;
        }

        public async Task<ProductBillingItemDto> GetProductBillingItem(Guid id)
        {
            var tId = _applicationUser.TenantId;
            var pId = _applicationUser.PropertyId;
            var cId = _applicationUser.ChainId;

            var ret = new ListDto<ProductBillingItemDto>();
            ret.HasNext = false;

            var query = await QueryAsync<ProductBillingItem>(@"
                          SELECT ProductBillingItem.ProductBillingItemId as Id,
                                 ProductBillingItem.*,
                                 BillingItem.Description
                          FROM ProductBillingItem productBillingItem
                          LEFT JOIN BillingItem ON ProductBillingItem.BillingItemId = BillingItem.BillingItemId
                          WHERE ProductBillingItem.TenantId = @TenantId
                          AND ProductBillingItem.ProductBillingItemId = @Id
                          AND ProductBillingItem.isDeleted = 0
                          @ProductId
                          @BillingItemId",
                          new
                          {
                              Id = id,
                              TenantId = tId
                          });

            return query.Count() == 0 ? null : query.FirstOrDefault().MapTo<ProductBillingItemDto>();
        }

        public async Task<ProductBillingItem> GetEntityProductBillingItem(Guid id)
        {
            var tId = _applicationUser.TenantId;
            var pId = _applicationUser.PropertyId;
            var cId = _applicationUser.ChainId;

            var query = await QueryAsync<ProductBillingItem>(@"
                          SELECT productBillingItem.ProductBillingItemId as Id,
                                 productBillingItem.*
                          FROM ProductBillingItem productBillingItem
                          WHERE productBillingItem.TenantId = @TenantId
                          AND productBillingItem.ProductBillingItemId = @Id
                          AND productBillingItem.isDeleted = 0",
                          new
                          {
                              Id = id,
                              TenantId = tId
                          });

            return query.FirstOrDefault();
        }

        public async Task<IListDto<ProductBillingItemDto>> GetBillingItensForProduct(Guid productId)
        {
            var ret = new ListDto<ProductBillingItemDto>();
            ret.HasNext = false;

            var tId = _applicationUser.TenantId;
            var pId = _applicationUser.PropertyId;
            var cId = _applicationUser.ChainId;

            var query = await QueryAsync<ProductBillingItemDto>(@"
                          SELECT ProductBillingItem.ProductBillingItemId as Id,
                                 BillingItem.Description,
                                 ProductBillingItem.*,
                                 BillingItem.BillingItemName
                          FROM ProductBillingItem productBillingItem
                          LEFT JOIN BillingItem ON ProductBillingItem.BillingItemId = BillingItem.BillingItemId
                          WHERE ProductBillingItem.TenantId = @TenantId
                          AND ProductBillingItem.ProductId = @Id 
                          AND productBillingItem.IsDeleted = 0
                          AND BillingItem.isActive = 1
                          AND BillingItem.isDeleted = 0",
                          new
                          {
                              Id = productId,
                              TenantId = tId
                          });

            query.ToList().ForEach(prodBillingItem =>
            {
                ret.Items.Add(prodBillingItem.MapTo<ProductBillingItemDto>());
            });

            return ret;
        }


        public async Task<IListDto<ProductBillingItemDto>> GetAll(GetAllProductBillingItemDto requestDto)
        {
            var tId = _applicationUser.TenantId;
            var pId = _applicationUser.PropertyId;
            var cId = _applicationUser.ChainId;

            var ret = new ListDto<ProductBillingItemDto>();
            ret.HasNext = false;

            var query = await QueryAsync<ProductBillingItemDto>(@"
                SELECT ProductBillingItem.ProductBillingItemId as Id,
                       ProductBillingItem.*,
                       BillingItem.BillingItemName as Description
                FROM ProductBillingItem
                LEFT JOIN BillingItem ON ProductBillingItem.BillingItemId = BillingItem.BillingItemId AND BillingItem.PropertyId = @PropertyId
                WHERE ProductBillingItem.isDeleted = 0
                  AND ProductBillingItem.TenantId = @TenantId",
            new
            {
                TenantId = _applicationUser.TenantId,
                PropertyId = _applicationUser.PropertyId
            });

            query.ToList().ForEach(prodBillingItem =>
            {
                ret.Items.Add(prodBillingItem.MapTo<ProductBillingItemDto>());
                ret.HasNext = false;
            });

            return ret;
        }
    }
}
