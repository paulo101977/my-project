﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Thex.GenericLog;
using Thex.Kernel;
using Thex.PDV.Domain.Entities.Thex;
using Thex.PDV.Infra.Context;
using Thex.PDV.Infra.Interfaces.ReadRepositories;
using Tnf.Dapper.Repositories;
using Tnf.Repositories;

namespace Thex.PDV.Infra.Repositories.ReadRepositories
{
    public class BillingAccountReadRepository : DapperEfRepositoryBase<ThexPdvContext, BillingAccount>, IBillingAccountReadRepository
    {
        private readonly IApplicationUser _applicationUser;

        public BillingAccountReadRepository(
               IActiveTransactionProvider activeTransactionProvider,
               IApplicationUser applicationUser,
               IGenericLogHandler genericLogHandler) : base(activeTransactionProvider, genericLogHandler)
        {
            _applicationUser = applicationUser;
        }

        public async Task<bool> IsClosedOrBlockedAccount(Guid id)
        {
            var tId = _applicationUser.TenantId;
            var pId = _applicationUser.PropertyId;

            var query = await QueryAsync<BillingAccount>(@"
                SELECT BillingAccount.BillingAccountId as Id,
                       BillingAccount.*
                FROM BillingAccount
                WHERE BillingAccount.BillingAccountId = @BillingAccountId
                AND BillingAccount.StatusId = 2
                AND BillingAccount.Blocked = 0
                AND BillingAccount.TenantId = @TenantId
                AND BillingAccount.PropertyId = @PropertyId",
                new
                {
                    BillingAccountId = id,
                    TenantId = tId,
                    PropertyId = pId
                });

            return (query.ToList().Count > 0);
        }
    }
}