﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Thex.GenericLog;
using Thex.Kernel;
using Thex.PDV.Domain.Entities;
using Thex.PDV.Dto;
using Thex.PDV.Dto.Product;
using Thex.PDV.Infra.Context;
using Thex.PDV.Infra.Interfaces.ReadRepositories;
using Tnf.Dapper.Repositories;
using Tnf.Dto;
using Tnf.Repositories;

namespace Thex.PDV.Infra.Repositories.ReadRepositories
{
    public class ProductReadRepository : DapperEfRepositoryBase<ThexPdvContext, Product>, IProductReadRepository
    {
        private readonly IApplicationUser _applicationUser;
        private readonly IProductBillingItemReadRepository _productBillingItemReadRepository;

        public ProductReadRepository(
               IActiveTransactionProvider activeTransactionProvider,
               IProductBillingItemReadRepository productBillingItemReadRepository,
               IApplicationUser applicationUser,
               IGenericLogHandler genericLogHandler) : base(activeTransactionProvider, genericLogHandler)
        {
            _applicationUser = applicationUser;
            _productBillingItemReadRepository = productBillingItemReadRepository;
        }

        public async Task<ProductDto> GetProductById(Guid id)
        {
            var tId = _applicationUser.TenantId;
            var pId = _applicationUser.PropertyId;
            var cId = _applicationUser.ChainId;

            var ret = new ListDto<ProductDto>();
            ret.HasNext = false;

            var query = await QueryAsync<ProductDto>(@"
                SELECT ProductId as Id,
                       Product.*,
                       ProductCategory.*,
                       Product.IsActive as IsActive,
				       Ncm.Code as NcmCode
                FROM Product
                LEFT JOIN ProductCategory ON Product.ProductCategoryId = ProductCategory.ProductCategoryId
                LEFT JOIN Ncm ON Product.NcmId = Ncm.NcmId
                WHERE Product.TenantId = @TenantId
                AND Product.PropertyId = @PropertyId
                AND Product.ChainId = @ChainId
                AND Product.ProductId = @ProductId
                AND Product.isDeleted = 0",
                           new
                           {
                               PropertyId = pId,
                               TenantId = tId,
                               ProductId = id,
                               ChainId = cId
                           });

            var prod = query.FirstOrDefault().MapTo<ProductDto>();

            if (prod == null)
                return ProductDto.NullInstance;

            var billingItens = await _productBillingItemReadRepository.GetBillingItensForProduct(prod.Id);

            foreach (var billingItem in billingItens.Items)
            {
                prod.BillingItens.Add(billingItem);
            }

            return prod;
        }

        public async Task<Product> GetProductEntityById(Guid id)
        {
            var tId = _applicationUser.TenantId;
            var pId = _applicationUser.PropertyId;
            var cId = _applicationUser.ChainId;

            var ret = new ListDto<ProductDto>();
            ret.HasNext = false;

            var query = await QueryAsync<Product>(@"
                SELECT ProductId as Id,
                       Product.*
                FROM Product
                WHERE Product.TenantId = @TenantId
                AND Product.PropertyId = @PropertyId
                AND Product.ChainId = @ChainId
                AND Product.ProductId = @ProductId
                AND Product.isDeleted = 0",
                           new
                           {
                               PropertyId = pId,
                               TenantId = tId,
                               ProductId = id,
                               ChainId = cId
                           });

            return query.FirstOrDefault();
        }

        public async Task<bool> ExistProductInCategory(Guid categoryId)
        {
            return (await base.GetAllAsync())
                    .Where(
                        x => x.TenantId == _applicationUser.TenantId
                            && x.PropertyId == int.Parse(_applicationUser.PropertyId)
                            && x.ChainId == int.Parse(_applicationUser.ChainId)
                            && x.ProductCategoryId == categoryId
                    ).Any();
    }

        public async Task<IListDto<ProductDto>> GetProductByCategory(Guid categoryId, int billingItemId)
        {
            var tId = _applicationUser.TenantId;
            var pId = _applicationUser.PropertyId;
            var cId = _applicationUser.ChainId;

            var ret = new ListDto<ProductDto>();
            ret.HasNext = false;

            var query = await QueryAsync<ProductDto>(@"
                SELECT Product.ProductId as Id,
                       Product.*,
				       Ncm.Code as NcmCode,
                       ProductCategory.*
                FROM Product
                LEFT JOIN ProductCategory ON Product.ProductCategoryId = ProductCategory.ProductCategoryId
				LEFT JOIN ProductBillingItem ON Product.ProductId = ProductBillingItem.ProductId
				LEFT JOIN BillingItem ON ProductBillingItem.BillingItemId = BillingItem.BillingItemId
                LEFT JOIN Ncm ON Product.NcmId = Ncm.NcmId
                WHERE Product.TenantId = @TenantId
                AND Product.ChainId = @ChainId
                AND Product.PropertyId = @PropertyId
                AND Product.isDeleted = 0
                AND Product.ProductCategoryId = @CategoryId
                AND ProductBillingItem.BillingItemId = @BillingItemId
                AND Product.isActive = 1
				AND BillingItem.isDeleted = 0
                AND BillingItem.isActive = 1",
                new
                {
                    PropertyId = pId,
                    TenantId = tId,
                    ChainId = cId,
                    CategoryId = categoryId,
                    BillingItemId = billingItemId
                });

            query.ToList().ForEach(prod =>
            {
                var product = prod.MapTo<ProductDto>();
                var billingItens = _productBillingItemReadRepository.GetBillingItensForProduct(product.Id).Result;

                foreach (var billingItem in billingItens.Items)
                {
                    product.BillingItens.Add(billingItem);
                }

                ret.Items.Add(product);
            });

            return ret;
        }

        public async Task<IListDto<ProductDto>> GetAll(GetAllProductDto requestDto)
        {
            var tId = _applicationUser.TenantId;
            var pId = _applicationUser.PropertyId;
            var cId = _applicationUser.ChainId;

            var ret = new ListDto<ProductDto>();
            ret.HasNext = false;

            var query = await QueryAsync<ProductDto>(@"
                SELECT Product.ProductId as Id,
                       Product.*,
				       Ncm.Code as NcmCode,
                       ProductCategory.*,
                       Product.IsActive as IsActive
                FROM Product
                LEFT JOIN ProductCategory ON Product.ProductCategoryId = ProductCategory.ProductCategoryId
                LEFT JOIN Ncm ON Product.NcmId = Ncm.NcmId
                WHERE Product.TenantId = @TenantId
                AND Product.ChainId = @ChainId
                AND Product.PropertyId = @PropertyId
                AND Product.isDeleted = 0",
                new
                {
                    PropertyId = pId,
                    TenantId = tId,
                    ChainId = cId
                });

            query.ToList().ForEach(prod =>
            {
                var product = prod.MapTo<ProductDto>();
                var billingItens = _productBillingItemReadRepository.GetBillingItensForProduct(product.Id).Result;

                foreach (var billingItem in billingItens.Items.Where(x => x.IsActive))
                {
                    product.BillingItens.Add(billingItem);
                }

                ret.Items.Add(product);
            });

            return ret;
        }

        public async Task<IListDto<ProductDto>> GetAllByFilters(SearchProductDto requestDto, int billingItemId)
        {
            var ret = new ListDto<ProductDto>();
            ret.HasNext = false;

            var query = await QueryAsync<ProductDto>(@"
                SELECT Product.ProductId as Id,
                       Product.*,
                       ProductCategory.*,
				       Ncm.Code as NcmCode
                FROM Product
                LEFT JOIN ProductCategory ON Product.ProductCategoryId = ProductCategory.ProductCategoryId
				LEFT JOIN ProductBillingItem ON Product.ProductId = ProductBillingItem.ProductId
				LEFT JOIN BillingItem ON ProductBillingItem.BillingItemId = BillingItem.BillingItemId
                LEFT JOIN Ncm ON Product.NcmId = Ncm.NcmId
                WHERE 
                (
				    (
				       Product.Code like @Param + '%'
				       OR Product.Code like '%' + @Param + '%'
				       OR Product.Code like '%' + @Param
				       OR Product.Code = @Param
			        )	 
                    OR 
				    (
				       Product.Barcode like @Param + '%'
				       OR Product.Barcode like '%' + @Param + '%'
				       OR Product.Barcode like '%' + @Param
				       OR Product.Barcode = @Param
			        )	
                    OR 
				    (
				       Product.ProductName like @Param + '%'
				       OR Product.ProductName like '%' + @Param + '%'
				       OR Product.ProductName like '%' + @Param
				       OR Product.ProductName = @Param
			        )	
                )
                AND Product.TenantId = @TenantId
                AND Product.ChainId = @ChainId
                AND Product.PropertyId = @PropertyId
                AND Product.isDeleted = 0
                AND ProductCategory.IsActive = 1
                AND ProductCategory.isDeleted = 0
                AND ProductBillingItem.BillingItemId = @BillingItemId
			    AND BillingItem.isActive = 1",
                            new
                            {
                                _applicationUser.PropertyId,
                                _applicationUser.TenantId,
                                _applicationUser.ChainId,
                                requestDto.Param,
                                BillingItemId = billingItemId
                            });

            query.ToList().ForEach(prod =>
            {
                var product = prod.MapTo<ProductDto>();
                var billingItens = _productBillingItemReadRepository.GetBillingItensForProduct(product.Id).Result;

                foreach (var billingItem in billingItens.Items.Where(x => x.BillingItemId == billingItemId && x.IsActive))
                {
                    product.BillingItens.Add(billingItem);
                }

                ret.Items.Add(product);
            });

            return ret;
        }

        public async Task<List<ProductDto>> GetAllIdAndName()
        {
            var tId = _applicationUser.TenantId;
            var pId = _applicationUser.PropertyId;
            var cId = _applicationUser.ChainId;

            var query = await QueryAsync<ProductDto>(@"
                SELECT Product.ProductId as Id,
                       Product.ProductName
                FROM Product
                WHERE Product.TenantId = @TenantId
                AND Product.IsActive = 1
                AND Product.ChainId = @ChainId
                AND Product.PropertyId = @PropertyId
                AND Product.isDeleted = 0",
                new
                {
                    PropertyId = pId,
                    TenantId = tId,
                    ChainId = cId
                });

            return query.ToList();
        }
    }
}
