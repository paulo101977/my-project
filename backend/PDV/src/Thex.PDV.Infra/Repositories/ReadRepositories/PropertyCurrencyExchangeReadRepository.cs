﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Thex.GenericLog;
using Thex.Kernel;
using Thex.GenericLog;
using Thex.PDV.Domain.Entities.Thex;
using Thex.PDV.Dto.Dto.CurrencyForBillingAccountItem;
using Thex.PDV.Infra.Context;
using Thex.PDV.Infra.Interfaces.ReadRepositories;
using Tnf.Dapper.Repositories;
using Tnf.Repositories;
using Thex.PDV.Dto;

namespace Thex.PDV.Infra.Repositories.ReadRepositories
{
    public class PropertyCurrencyExchangeReadRepository : DapperEfRepositoryBase<ThexPdvContext, PropertyCurrencyExchange>, IPropertyCurrencyExchangeReadRepository
    {
        private readonly IApplicationUser _applicationUser;

        public PropertyCurrencyExchangeReadRepository(
               IActiveTransactionProvider activeTransactionProvider,
               IApplicationUser applicationUser,
               IGenericLogHandler genericLogHandler) : base(activeTransactionProvider, genericLogHandler)
        {
            _applicationUser = applicationUser;
        }

        public async Task<CurrencyForBillingAccountItemDto> GetCurrencyIdExchangeByPropertyId(long propertyId)
        {
            var tId = _applicationUser.TenantId;
            var pId = _applicationUser.PropertyId;

            var query = await QueryAsync<CurrencyForBillingAccountItemDto>(@"
                        SELECT TOP 1
                        Currency.CurrencyId as Id,
                        CurrencyExchange.CurrencyExchangeId as CurrencyExchangeReferenceId,
                        Currency.*
                        FROM Currency
                        LEFT JOIN CurrencyExchange ON CurrencyExchange.CurrencyId = Currency.CurrencyId
                        LEFT JOIN PropertyCurrencyExchange ON CurrencyExchange.CurrencyExchangeId = PropertyCurrencyExchange.CurrencyId
                        AND CurrencyExchange.CurrencyId = PropertyCurrencyExchange.CurrencyId            
                        AND PropertyCurrencyExchange.PropertyId = @PropertyId
                        AND PropertyCurrencyExchange.TenantId = @TenantId
                        ORDER BY CurrencyExchange.CreationTime desc",
                new
                {
                    TenantId = tId,
                    PropertyId = pId
                });

            return query.FirstOrDefault();
        }


        public async Task<ExchangeRateDto> GetExchangeRateById(Guid id)
        {
            var tId = _applicationUser.TenantId;
            var pId = _applicationUser.PropertyId;

            var query = await QueryAsync<ExchangeRateDto>(@"
                        SELECT 
                            PropertyCurrencyExchange.PropertyCurrencyExchangeId as Id,
							PropertyCurrencyExchange.ExchangeRate as ExchangeRate
                        FROM PropertyCurrencyExchange
					    WHERE PropertyCurrencyExchange.PropertyCurrencyExchangeId = @id
						UNION ALL
					    SELECT 
                            CurrencyExchange.CurrencyExchangeId as Id,
							CurrencyExchange.ExchangeRate as ExchangeRate
                        FROM CurrencyExchange
					    WHERE CurrencyExchange.CurrencyExchangeId = @id",
                new
                {
                    id = id
                });

            return query.FirstOrDefault();
        }
    }
}