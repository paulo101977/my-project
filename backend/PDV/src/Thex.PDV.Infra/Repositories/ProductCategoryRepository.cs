﻿using System;
using System.Threading.Tasks;
using Thex.Common;
using Thex.Common.Enumerations;
using Thex.GenericLog;
using Thex.Kernel;
using Thex.PDV.Domain.Entities;
using Thex.PDV.Dto;
using Thex.PDV.Infra.Context;
using Thex.PDV.Infra.Interfaces;
using Thex.PDV.Infra.Interfaces.ReadRepositories;
using Tnf.Dapper.Repositories;
using Tnf.Dto;
using Tnf.Notifications;
using Tnf.Repositories;

namespace Thex.PDV.Infra.Repositories
{
    public class ProductCategoryRepository : DapperEfRepositoryBase<ThexPdvContext, ProductCategory>, IProductCategoryRepository
    {
        private readonly IApplicationUser _applicationUser;
        private readonly INotificationHandler _notificationHandler;
        private readonly IProductCategoryReadRepository _productCategoryReadRepository;

        public ProductCategoryRepository(
            IProductCategoryReadRepository productCategoryReadRepository,
            IActiveTransactionProvider activeTransactionProvider,
            INotificationHandler notificationHandler,
            IApplicationUser applicationUser,
            IGenericLogHandler genericLogHandler) : base(activeTransactionProvider, genericLogHandler)
        {
            _applicationUser = applicationUser;
            _notificationHandler = notificationHandler;
            _productCategoryReadRepository = productCategoryReadRepository;
        }

        public async Task<IDto> InsertAsync(BaseEntity entity)
        {
            await base.InsertAsync(entity: entity as ProductCategory);

            return entity.MapTo<ProductCategoryDto>();
        }

        public async Task<IDto> UpdateAsync(Guid id, BaseEntity entity)
        {
            var dbCategory = await _productCategoryReadRepository.GetOneById(id);

            if (dbCategory == null)
            {
                _notificationHandler.Raise(_notificationHandler
                                    .DefaultBuilder
                                    .WithMessage(AppConsts.LocalizationSourceName, CommonsEnum.Error.ParameterInvalid)
                                    .Build());
                return null;
            }

            // Ajustes dapper
            entity.Id = dbCategory.Id;
            entity.CreationTime = dbCategory.CreationTime;

            await base.UpdateAsync(entity: entity as ProductCategory);

            if (_notificationHandler.HasNotification())
                return null;

            return entity.MapTo<ProductCategoryDto>();
        }

        public async Task RemoveAsync(Guid id)
        {
            var entity = await _productCategoryReadRepository.GetCategoyEntityById(id);

            if (entity == null)
            {
                _notificationHandler.Raise(_notificationHandler
                                    .DefaultBuilder
                                    .WithMessage(AppConsts.LocalizationSourceName, CommonsEnum.Error.ParameterInvalid)
                                    .Build());
                return;
            }

            await base.DeleteAsync(entity: entity as ProductCategory);
        }

        public async Task ToggleAsync(Guid id)
        {
            var entity = await _productCategoryReadRepository.GetCategoyEntityById(id);

            if (entity == null)
            {
                _notificationHandler.Raise(_notificationHandler
                                    .DefaultBuilder
                                    .WithMessage(AppConsts.LocalizationSourceName, CommonsEnum.Error.ParameterInvalid)
                                    .Build());
                return;
            }

            entity.IsActive = !entity.IsActive;

            await base.UpdateAsync(entity: entity as ProductCategory);
        }
    }
}
