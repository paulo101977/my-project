﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Thex.GenericLog;
using Thex.Kernel;
using Thex.PDV.Domain.Entities;
using Thex.PDV.Dto.Dto.BillingAccountItem;
using Thex.PDV.Infra.Context;
using Thex.PDV.Infra.Interfaces;
using Tnf.Dapper.Repositories;
using Tnf.Dto;
using Tnf.Notifications;
using Tnf.Repositories;

namespace Thex.PDV.Infra.Repositories
{
    public class BillingAccountItemDetailRepository : DapperEfRepositoryBase<ThexPdvContext, BillingAccountItemDetail>, IBillingAccountItemDetailRepository
    {
        private readonly IApplicationUser _applicationUser;
        private readonly INotificationHandler _notificationHandler;

        public BillingAccountItemDetailRepository(
            IActiveTransactionProvider activeTransactionProvider,
            INotificationHandler notificationHandler,
            IApplicationUser applicationUser,
            IGenericLogHandler genericLogHandler) : base(activeTransactionProvider, genericLogHandler)
        {
            _applicationUser = applicationUser;
            _notificationHandler = notificationHandler;
        }

        public async Task AddRange(List<BillingAccountItemDetail> billingAccountItemDetails)
        {
            foreach (var billingAccountItemDetail in billingAccountItemDetails)
            {
                await base.InsertAsync(entity: billingAccountItemDetail as BillingAccountItemDetail);
            }
        }

        public async Task<IDto> InsertAsync(BaseEntity entity)
        {
            await base.InsertAsync(entity: null);

            return entity.MapTo<BillingAccountItemDto>();
        }

        public Task RemoveAsync(Guid id)
        {
            return Task.CompletedTask;
        }

        public Task ToggleAsync(Guid id)
        {
            return Task.CompletedTask;
        }

        public Task<IDto> UpdateAsync(Guid id, BaseEntity baseEntity)
        {
            return null;
        }
    }
}