﻿using System;
using System.Diagnostics;
using Thex.Common;
using Thex.Kernel;
using Thex.PDV.Domain.Entities;
using Thex.PDV.Infra;

namespace Thex.Infra.Extensions
{
    public static class DtoEntityExtensions
    {
        /// <summary>
        /// Preenche os campos de acordo com a operação (Inserção, Edição e deleção)
        /// </summary>
        public static BaseEntity SetOperationValues(this BaseEntity entity, Operation operation, IApplicationUser _applicationUser)
        {
            var timeZone = Debugger.IsAttached ? AppConsts.DEFAULT_TIME_ZONE_NAME : _applicationUser.TimeZoneName;
            var userId = Debugger.IsAttached ? Guid.Parse("215bc8a0-07db-4428-9955-fa53b58b4ded") : _applicationUser.UserUid;

            entity.TenantId = Debugger.IsAttached ? Guid.Parse("23eb803c-726a-4c7c-b25b-2c22a56793d9") : _applicationUser.UserUid;
            entity.PropertyId = Debugger.IsAttached ? 11 : _applicationUser.PropertyId.TryParseToInt32();
            entity.ChainId = Debugger.IsAttached ? 1 : _applicationUser.ChainId.TryParseToInt32();

            switch (operation)
            {
                case Operation.Insert:
                    {
                        if (entity.Id == Guid.Empty)
                            entity.Id = new Guid();
                        entity.CreationTime = DateTime.UtcNow.ToZonedDateTime(timeZone);
                        entity.CreatorUserId = userId;
                        entity.IsActive = true;
                        entity.IsDeleted = false;
                    }
                    break;
                case Operation.Update:
                    {
                        entity.LastModificationTime = DateTime.UtcNow.ToZonedDateTime(timeZone);
                        entity.LastModifierUserId = userId;
                    }
                    break;
                case Operation.Delete:
                    {
                        entity.IsDeleted = true;
                        entity.IsActive = false;
                        entity.DeletionTime = DateTime.UtcNow.ToZonedDateTime(timeZone);
                        entity.DeleterUserId = userId;
                    }
                    break;
            }

            return entity;
        }
    }
}