﻿using System;
using System.Diagnostics.CodeAnalysis;

namespace Thex.PDV.Infra.Common
{
    [ExcludeFromCodeCoverageAttribute]
    [Serializable]
    public abstract class ThexMultiTenantNullableFullAuditedEntity : ThexFullAuditedEntity
    {
        public Guid? TenantId { get; set; }
        public Tenant Tenant { get; set; }
    }
}
