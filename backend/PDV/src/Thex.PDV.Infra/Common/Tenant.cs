﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace Thex.PDV.Infra.Common
{
    [ExcludeFromCodeCoverageAttribute]
    public partial class Tenant : IEntityGuid
    {
        public Guid Id { get; set; }
        public string TenantName { get; set; }
        public Guid? ParentId { get; set; }
        public bool IsActive { get; set; }
        public virtual Tenant Parent { get; set; }
        public virtual ICollection<Tenant> ChildList { get; set; }
    }
}
