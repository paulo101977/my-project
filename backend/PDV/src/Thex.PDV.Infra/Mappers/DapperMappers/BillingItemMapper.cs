﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a Crudzilla tool.
// </auto-generated>
//------------------------------------------------------------------------------

using DapperExtensions.Mapper;
using Thex.PDV.Domain.Entities;

namespace Thex.PDV.Infra.Mappers.DapperMappers
{
    public sealed class BillingItemMapper : ClassMapper<BillingItem>
    {
        public BillingItemMapper()
        {
            Table("BillingItem");
            Map(e => e.Id).Column("BillingItemId").Key(KeyType.Assigned);
            AutoMap();
        }
    }
}
