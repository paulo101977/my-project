﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Thex.PDV.Domain.Entities;
using Thex.PDV.Dto;
using Tnf.Dto;

namespace Thex.PDV.Infra.Interfaces
{
    public interface IProductRepository : IBaseRepository
    {
        Task<IDto> UpdateEntity(Product product);
    }
}
