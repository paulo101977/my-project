﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Thex.PDV.Infra.Interfaces.ReadRepositories
{
    public interface IBillingAccountReadRepository
    {
        Task<bool> IsClosedOrBlockedAccount(Guid id);
    }
}
