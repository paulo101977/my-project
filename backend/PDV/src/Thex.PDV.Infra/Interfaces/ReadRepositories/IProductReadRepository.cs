﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.PDV.Domain.Entities;
using Thex.PDV.Dto;
using Thex.PDV.Dto.Product;
using Tnf.Dto;

namespace Thex.PDV.Infra.Interfaces.ReadRepositories
{
    public interface IProductReadRepository
    {
        Task<IListDto<ProductDto>> GetAll(GetAllProductDto requestDto);

        Task<bool> ExistProductInCategory(Guid categoryId);

        Task<IListDto<ProductDto>> GetProductByCategory(Guid categoryId, int billingItemId);

        Task<ProductDto> GetProductById(Guid id);

        Task<IListDto<ProductDto>> GetAllByFilters(SearchProductDto requestDto, int billingItemId);

        Task<Product> GetProductEntityById(Guid id);

        Task<List<ProductDto>> GetAllIdAndName();
    }
}
