﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Thex.PDV.Dto;
using Tnf.Dto;

namespace Thex.PDV.Infra.Interfaces.ReadRepositories
{
    public interface IUnitMeasurementReadRepository
    {
        Task<IListDto<UnitMeasurementDto>> GetAll(GetAllUnitMeasurementDto requestDto);
    }
}
