﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Thex.PDV.Domain.Entities.Thex;

namespace Thex.PDV.Infra.Interfaces.ReadRepositories
{
    public interface IRatePlanReadRepository
    {
        Task<RatePlan> GetByBillingAccountId(Guid billingAccountId);
    }
}
