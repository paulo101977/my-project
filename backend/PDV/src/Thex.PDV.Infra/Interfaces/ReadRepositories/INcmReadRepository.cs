﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Thex.PDV.Domain.Entities;
using Thex.PDV.Dto;
using Thex.PDV.Dto.GetAll;
using Tnf.Dto;

namespace Thex.PDV.Infra.Interfaces.ReadRepositories
{
    public interface INcmReadRepository
    {
        Task<IListDto<NcmDto>> GetAll(GetAllNcmDto requestDto);
        Task<IListDto<NcmDto>> GetAllByFilters(SearchNcmDto searchDto);
    }
}
