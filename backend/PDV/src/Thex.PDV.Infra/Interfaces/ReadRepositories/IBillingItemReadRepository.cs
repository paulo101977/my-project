﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Thex.PDV.Domain.Entities;
using Thex.PDV.Dto;
using Thex.PDV.Dto.Dto.BillingItemServiceAndTaxAssociate;
using Tnf.Dto;

namespace Thex.PDV.Infra.Interfaces.ReadRepositories
{
    public interface IBillingItemReadRepository
    {
        BillingItem GetById(int id);
        List<BillingItemDto> GetBillinItemFromProduct(Guid productId);
        Task<IListDto<BillingItemDto>> GetAll(GetAllBillingItemDto requestDto);
        Task<bool> AnyBillingItemByBillingItemType(int billingItemId, int billingItemType, bool ignoreFilters = false);
        Task<bool> IsServiceAndExistSupportedType(int billingItemId);
        IList<BillingItemServiceAndTaxAssociateDto> GetTaxesForServices(IList<int> serviceIdsList);
    }
}
