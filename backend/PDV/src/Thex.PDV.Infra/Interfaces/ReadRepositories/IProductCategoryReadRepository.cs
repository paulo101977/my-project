﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.PDV.Domain.Entities;
using Thex.PDV.Dto;
using Tnf.Dto;

namespace Thex.PDV.Infra.Interfaces.ReadRepositories
{
    public interface IProductCategoryReadRepository
    {
        Task<IListDto<ProductCategoryDto>> GetAllGroupFixed();
        
        Task<IListDto<ProductCategoryDto>> GetAllCategories(GetAllProductCategoryDto request);

        Task<IListDto<ProductCategoryDto>> GetAllByBillingItemId(GetAllProductCategoryDto request);

        Task<ProductCategoryDto> GetOneById(Guid id);

        Task<ProductCategoryDto> GetCategoryById(Guid id);

        Task<ProductCategory> GetCategoyEntityById(Guid id);
    }
}
