﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Thex.PDV.Domain.Entities;
using Thex.PDV.Dto;
using Tnf.Dto;

namespace Thex.PDV.Infra.Interfaces.ReadRepositories
{
    public interface IProductBillingItemReadRepository
    {
        Task<IListDto<ProductBillingItemDto>> GetAll(GetAllProductBillingItemDto requestDto);

        Task<ProductBillingItemDto> GetProductBillingItem(Guid id);

        Task<ProductBillingItem> GetEntityProductBillingItem(Guid id);

        Task<IListDto<ProductBillingItemDto>> GetBillingItensForProduct(Guid productId);
    }
}
