﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Thex.PDV.Domain.Entities.Thex;
using Thex.PDV.Dto;
using Thex.PDV.Dto.Dto.CurrencyForBillingAccountItem;

namespace Thex.PDV.Infra.Interfaces.ReadRepositories
{
    public interface IPropertyCurrencyExchangeReadRepository
    {
        Task<CurrencyForBillingAccountItemDto> GetCurrencyIdExchangeByPropertyId(long propertyId);
        Task<ExchangeRateDto> GetExchangeRateById(Guid id);
    }
}
