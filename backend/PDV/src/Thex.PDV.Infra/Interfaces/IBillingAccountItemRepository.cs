﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Thex.PDV.Domain.Entities;

namespace Thex.PDV.Infra.Interfaces
{
    public interface IBillingAccountItemRepository : IBaseRepository
    {
        Task AddRange(List<BillingAccountItem> billingAccountItems);
    }
}
