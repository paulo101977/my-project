﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Thex.PDV.Domain.Entities;
using Tnf.Builder;
using Tnf.Dto;

namespace Thex.PDV.Infra.Interfaces
{
    public interface IBaseRepository
    {
        Task<IDto> InsertAsync(BaseEntity baseEntity);
        Task<IDto> UpdateAsync(Guid id, BaseEntity baseEntity);
        Task RemoveAsync(Guid id);
        Task ToggleAsync(Guid id);
    }
}
