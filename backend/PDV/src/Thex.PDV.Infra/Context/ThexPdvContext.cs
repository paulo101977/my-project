﻿using Microsoft.EntityFrameworkCore;
using Thex.PDV.Domain.Entities;
using Tnf.EntityFrameworkCore;
using Tnf.Runtime.Session;

namespace Thex.PDV.Infra.Context
{
    public class ThexPdvContext : TnfDbContext
    {
        public DbSet<ProductCategory> ProductCategories { get; set; }
        public DbSet<Product> Product { get; set; }
        public DbSet<ProductBillingItem> ProductBillingItem { get; set; }
        public DbSet<Ncm> Ncms { get; set; }
        public DbSet<BillingAccountItem> BillingAccountItens { get; set; }

        // Importante o construtor do contexto receber as opções com o tipo generico definido: DbContextOptions<TDbContext>
        public ThexPdvContext(DbContextOptions<ThexPdvContext> options, ITnfSession session)
            : base(options, session)
        {
        }
    }
}
