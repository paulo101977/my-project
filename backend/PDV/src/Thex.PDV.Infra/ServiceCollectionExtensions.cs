﻿using Thex.PDV.Infra.Context;
using Thex.PDV.Infra.Interfaces;
using Thex.PDV.Infra.Interfaces.ReadRepositories;
using Thex.PDV.Infra.Mappers.DapperMappers;
using Thex.PDV.Infra.Mappers.Profiles;
using Thex.PDV.Infra.Repositories;
using Thex.PDV.Infra.Repositories.ReadRepositories;
using Tnf.Dapper;

namespace Microsoft.Extensions.DependencyInjection
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddInfraDependency(this IServiceCollection services)
        {
            // Configura o uso do Dapper registrando os contextos que serão
            // usados pela aplicação
            services
                .AddTnfEntityFrameworkCore()
                .AddTnfDbContext<ThexPdvContext>(config => DbContextConfigurer.Configure(config))
                .AddTnfDapper(options =>
                {
                    options.MapperAssemblies.Add(typeof(ProductCategoryMapper).Assembly);
                    options.MapperAssemblies.Add(typeof(ProductMapper).Assembly);
                    options.MapperAssemblies.Add(typeof(BillingItemMapper).Assembly);
                    options.MapperAssemblies.Add(typeof(ProductBillingItemMapper).Assembly);
                    options.MapperAssemblies.Add(typeof(NcmMapper).Assembly);
                    options.MapperAssemblies.Add(typeof(UnitMeasurementMapper).Assembly);
                    options.MapperAssemblies.Add(typeof(BillingItemMapper).Assembly);
                    options.MapperAssemblies.Add(typeof(BillingAccountItemMapper).Assembly);
                    options.MapperAssemblies.Add(typeof(BillingAccountItemDetailMapper).Assembly);

                    options.DbType = DapperDbType.SqlServer;
                });

            services.AddTnfAutoMapper(config =>
            {
                config.AddProfile<ProductCategoryProfile>();
                config.AddProfile<ProductProfile>();
                config.AddProfile<BillingItemProfile>();
                config.AddProfile<ProductBillingItemProfile>();
                config.AddProfile<NcmProfile>();
                config.AddProfile<UnitMeasurementProfile>();
                config.AddProfile<BillingAccountItemProfile>();
                config.AddProfile<BillingAccountItemDetailProfile>();
            });

            services.AddTransient<IBillingAccountReadRepository, BillingAccountReadRepository>();
            services.AddTransient<IBillingAccountItemRepository, BillingAccountItemRepository>();
            services.AddTransient<IPropertyCurrencyExchangeReadRepository, PropertyCurrencyExchangeReadRepository>();
            services.AddTransient<IPropertyParameterReadRepository, PropertyParameterReadRepository>();
            services.AddTransient<IRatePlanReadRepository, RatePlanReadRepository>();
            services.AddTransient<IProductCategoryReadRepository, ProductCategoryReadRepository>();
            services.AddTransient<IProductCategoryRepository, ProductCategoryRepository>();
            services.AddTransient<IProductReadRepository, ProductReadRepository>();
            services.AddTransient<IProductRepository, ProductRepository>();
            services.AddTransient<IProductBillingItemReadRepository, ProductBillingItemReadRepository>();
            services.AddTransient<IProductBillingItemRepository, ProductBillingItemRepository>();
            services.AddTransient<INcmReadRepository, NcmReadRepository>();
            services.AddTransient<IBillingItemReadRepository, BillingItemReadRepository>();
            services.AddTransient<IUnitMeasurementReadRepository, UnitMeasurementReadRepository>();
            services.AddTransient<IBillingAccountItemDetailRepository, BillingAccountItemDetailRepository>();

            return services;
        }
    }
}
