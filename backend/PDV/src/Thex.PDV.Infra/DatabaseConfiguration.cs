﻿using Microsoft.Extensions.Configuration;
using System.Diagnostics.CodeAnalysis;

namespace Thex.PDV.Infra
{
    [ExcludeFromCodeCoverageAttribute]
    public class DatabaseConfiguration
    {
        public DatabaseConfiguration(IConfiguration configuration)
        {
            DefaultSchema = configuration["DefaultSchema"];
            ConnectionStringName = configuration["DefaultConnectionString"];
            DatabaseType = DatabaseType.SqlServer;
            ConnectionString = configuration[$"ConnectionStrings:{ConnectionStringName}"];
        }

        public string DefaultSchema { get; }
        public string ConnectionStringName { get; }
        public string ConnectionString { get; }
        public DatabaseType DatabaseType { get; }
    }

    public enum DatabaseType
    {
        SqlServer,
        Sqlite,
        Oracle
    }
}
