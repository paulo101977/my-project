﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics.CodeAnalysis;
using System.Threading.Tasks;
using Thex.Kernel;
using Thex.PDV.Application.Interfaces;
using Thex.PDV.Base;
using Thex.PDV.Domain;
using Thex.PDV.Dto;
using Tnf.AspNetCore.Mvc.Response;

namespace Thex.PDV.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [AllowAnonymous]
    [ExcludeFromCodeCoverage]
    public class HealthController : ThexAppController
    {
        private IBillingItemAppService _billingItemAppService;

        public HealthController(IBillingItemAppService billingItemAppService,
                                IApplicationUser applicationUser) : base(applicationUser)
        {
            _billingItemAppService = billingItemAppService;
        }

        /// <summary>
        /// Verifica se a API está em funcionamento e conectada no banco de dados
        /// </summary>
        /// <returns></returns>
        [HttpGet("check")]
        [ProducesResponseType(typeof(bool), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> GetAll([FromQuery]GetAllBillingItemDto requestDto)
        {
            var response = await _billingItemAppService.GetAll(requestDto);

            return CreateResponseOnGetAll(response != null ? true : false, EntityNames.Health);
        }
    }
}