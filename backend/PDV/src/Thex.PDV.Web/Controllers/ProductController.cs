﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.Kernel;
using Thex.PDV.Application.Interfaces;
using Thex.PDV.Base;
using Thex.PDV.Domain;
using Thex.PDV.Dto;
using Thex.PDV.Dto.Product;
using Tnf.AspNetCore.Mvc.Response;
using Tnf.Dto;

namespace Thex.PDV.Web.Controllers
{
    [Route(RouteConsts.ProductRouteName)]
    public class ProductController : ThexAppController
    {
        private readonly IProductAppService _productAppService;

        public ProductController(IProductAppService ProductAppService,
                                 IApplicationUser applicationUser)
            : base(applicationUser)
        {
            _productAppService = ProductAppService;
        }

        /// <summary>
        /// Retorna a lista de todos os produtos disponíveis
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ThexAuthorize("PDV_Product_Get")]
        [ProducesResponseType(typeof(IListDto<ProductDto>), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> GetAll([FromQuery]GetAllProductDto requestDto)
        {
            var response = await _productAppService.GetAll(requestDto);

            return CreateResponseOnGetAll(response, EntityNames.Product);
        }

        [HttpGet("{billingItemId}/search")]
        [ThexAuthorize("PDV_Product_Get_Search")]
        [ProducesResponseType(typeof(IListDto<ProductDto>), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> GetAllByFilters([FromQuery] SearchProductDto request, int billingItemId)
        {
            var response = await _productAppService.GetAllByFilters(request, billingItemId);

            return CreateResponseOnGetAll(response, EntityNames.Product);
        }

        /// <summary>
        /// Retorna um produto através do identificador
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id}")]
        [ThexAuthorize("PDV_Product_Get_Param")]
        [ProducesResponseType(typeof(ProductDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> GetById(string id, RequestDto requestDto)
        {
            var response = await _productAppService.GetById(Guid.Parse(id));

            return CreateResponseOnGet(response, EntityNames.Product);
        }

        /// <summary>
        /// Retorna um produto através do código de barras
        /// </summary>
        /// <returns></returns>
        [HttpPost("getImageBarCode")]
        [ThexAuthorize("PDV_Product_GetImageBarCode")]
        [ProducesResponseType(typeof(ProductDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> GetImageBarCode([FromBody]GetAllProductDto barCode)
        {
            var response = await _productAppService.GetImageBarCode(barCode);

            return CreateResponseOnPost(response, EntityNames.Product);
        }

        /// <summary>
        /// Retorna uma lista de produtos através da categoria
        /// </summary>
        /// <returns></returns>
        [HttpGet("category/{categoryId}/{billintItemId}")]
        [ThexAuthorize("PDV_Product_Get_Category")]
        [ProducesResponseType(typeof(IListDto<ProductDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> GetByCategory(string categoryId, int billintItemId, GetAllProductDto requestDto)
        {
            var response = await _productAppService.GetByCategoryId(Guid.Parse(categoryId), billintItemId, requestDto);

            return CreateResponseOnGetAll(response, EntityNames.Product);
        }

        /// <summary>
        /// Registra um novo produto
        /// </summary>
        [HttpPost]
        [ThexAuthorize("PDV_Product_Post")]
        [ProducesResponseType(typeof(ProductDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Post([FromBody] ProductDto dto)
        {
            var response = await _productAppService.CreateObjAsync(dto); //CreateProduct(dto);

            return CreateResponseOnPost(response, EntityNames.Product);
        }

        /// <summary>
        /// Altera os detalhes de um produto
        /// </summary>
        [HttpPut("{id}")]
        [ThexAuthorize("PDV_Product_Put")]
        [ProducesResponseType(typeof(ProductDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Put(string id, [FromBody] ProductDto dto)
        {
            var response = await _productAppService.EditObjAsync(Guid.Parse(id), dto);

            return CreateResponseOnPut(response, EntityNames.Product);
        }

        /// <summary>
        /// Remove um produto
        /// </summary>
        /// <returns></returns>
        [HttpDelete("{id}")]
        [ThexAuthorize("PDV_Product_Delete")]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Delete(string id)
        {
            await _productAppService.DeleteObjAsync(Guid.Parse(id));

            return CreateResponseOnDelete(EntityNames.Product);
        }

        /// <summary>
        /// Ativa ou desativa um grupo de produtos
        /// </summary>
        /// <returns></returns>
        [HttpPatch("{id}/toggleactivation")]
        [ThexAuthorize("PDV_Product_Patch_ToggleActivation")]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> ToggleActivation(string id)
        {
            await _productAppService.ToggleActivationObjAsync(Guid.Parse(id));

            return CreateResponseOnPatch(null, EntityNames.Product);
        }

        [HttpGet("getallidandname")]
        [ThexAuthorize("PDV_Product_GetAllIdAndName")]
        [ProducesResponseType(typeof(List<ProductDto>), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> GetAllIdAndName()
        {
            var response = await _productAppService.GetAllIdAndName();

            return CreateResponseOnGet(response, EntityNames.Product);
        }
    }
}