﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Thex.Kernel;
using Thex.PDV.Application.Interfaces;
using Thex.PDV.Base;
using Thex.PDV.Domain;
using Thex.PDV.Dto;
using Tnf.AspNetCore.Mvc.Response;
using Tnf.Dto;

namespace Thex.PDV.Web.Controllers
{
    [Route(RouteConsts.ProductBillingItemRouteName)]
    public class ProductBillingItemController : ThexAppController
    {
        private readonly IProductBillingItemAppService _productBillingItemAppService;

        public ProductBillingItemController(IProductBillingItemAppService productBillingItemAppService,
                                            IApplicationUser appicationUser)
            :base(appicationUser)
        {
            _productBillingItemAppService = productBillingItemAppService;
        }

        /// <summary>
        /// Retorna a lista de todas as associações de produtos com pontos de venda
        /// </summary>
        [HttpGet]
        [ThexAuthorize("PDV_ProductBillingItem_Get")]
        [ProducesResponseType(typeof(IListDto<ProductBillingItemDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> GetAll([FromQuery]GetAllProductBillingItemDto requestDto)
        {
            var response = await _productBillingItemAppService.GetAll(requestDto);

            return CreateResponseOnGetAll(response, EntityNames.ProductCategory);
        }
    }
}