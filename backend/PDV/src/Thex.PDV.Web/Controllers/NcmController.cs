﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Thex.Kernel;
using Thex.PDV.Application.Interfaces;
using Thex.PDV.Base;
using Thex.PDV.Domain;
using Thex.PDV.Dto;
using Thex.PDV.Dto.GetAll;
using Tnf.AspNetCore.Mvc.Response;
using Tnf.Dto;

namespace Thex.PDV.Web.Controllers
{
    [Route(RouteConsts.NcmRouteName)]
    public class NcmController : ThexAppController
    {
        private readonly INcmAppService _ncmAppService;

        public NcmController(INcmAppService ncmAppService,
                             IApplicationUser applicationUser)
            : base(applicationUser)
        {
            _ncmAppService = ncmAppService;
        }

        /// <summary>
        /// Retorna a lista de todos os Ncm's disponíveis
        /// </summary>
        [HttpGet]
        [ThexAuthorize("PDV_Ncm_Get")]
        [ProducesResponseType(typeof(IListDto<NcmDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> GetAll([FromQuery]GetAllNcmDto requestDto)
        {
            var response = await _ncmAppService.GetAll(requestDto);

            return CreateResponseOnGetAll(response, EntityNames.Ncm);
        }

        /// <summary>
        /// Retorna a lista de todos os Ncm's disponíveis
        /// </summary>
        [HttpGet("search")]
        [ThexAuthorize("PDV_Ncm_Get")]
        [ProducesResponseType(typeof(IListDto<NcmDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> GetAllByFilters([FromQuery]SearchNcmDto requestDto)
        {
            var response = await _ncmAppService.GetAllByFilters(requestDto);

            return CreateResponseOnGetAll(response, EntityNames.Ncm);
        }
    }
}