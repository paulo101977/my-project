﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using Thex.Kernel;
using Thex.PDV.Application.Interfaces;
using Thex.PDV.Base;
using Thex.PDV.Domain;
using Thex.PDV.Dto;
using Tnf.AspNetCore.Mvc.Response;
using Tnf.Dto;

namespace Thex.PDV.Web.Controllers
{
    [Route(RouteConsts.ProductCategoryRouteName)]
    public class ProductCategoryController : ThexAppController
    {
        private readonly IProductCategoryAppService _productCategoryAppService;

        public ProductCategoryController(IApplicationUser applicationUser,
                                         IProductCategoryAppService ProductCategoryAppService)
                                         : base(applicationUser)
        {
            _productCategoryAppService = ProductCategoryAppService;
        }

        /// <summary>
        /// Retorna a lista com todos os grupos fixos
        /// </summary>
        [HttpGet("groupfixed")]
        [ThexAuthorize("PDV_ProductCategory_Get_GroupFixed")]
        [ProducesResponseType(typeof(IListDto<ProductCategoryDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> GetAllGroupFixed()
        {
            var response = await _productCategoryAppService.GetAllGroupFixed();

            return CreateResponseOnGetAll(response, EntityNames.ProductCategory);
        }

        /// <summary>
        /// Retorna todos os grupos de produtos cadastrados
        /// </summary>
        [HttpGet]
        [ThexAuthorize("PDV_ProductCategory_Get")]
        [ProducesResponseType(typeof(IListDto<ProductCategoryDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> GetAll([FromQuery]GetAllProductCategoryDto requestDto)
        {
            var response = await _productCategoryAppService.GetAll(requestDto);

            return CreateResponseOnGetAll(response, EntityNames.ProductCategory);
        }

        /// <summary>
        /// Retorna todos os grupos de produtos cadastrados por billingItemId
        /// </summary>
        [HttpGet("getAllByBillingItemId")]
        [ThexAuthorize("PDV_ProductCategory_Get_GetAllByBillingItemId")]
        [ProducesResponseType(typeof(IListDto<ProductCategoryDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> GetAllByBillingItemId([FromQuery]GetAllProductCategoryDto requestDto)
        {
            var response = await _productCategoryAppService.GetAllByBillingItemId(requestDto);

            return CreateResponseOnGetAll(response, EntityNames.ProductCategory);
        }

        /// <summary>
        /// Retorna um grupo de produto pelo identificador
        /// </summary>
        /// 
        [HttpGet("{id}")]
        [ThexAuthorize("PDV_ProductCategory_Get_Param")]
        [ProducesResponseType(typeof(ProductCategoryDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> GetById(string id, RequestDto requestDto)
        {
            var response = await _productCategoryAppService.GetById(Guid.Parse(id));

            return CreateResponseOnGet(response, EntityNames.ProductCategory);
        }

        /// <summary>
        /// Registra um novo grupo de produtos
        /// </summary>
        [HttpPost]
        [ThexAuthorize("PDV_ProductCategory_Post")]
        [ProducesResponseType(typeof(ProductCategoryDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Post([FromBody] ProductCategoryDto dto)
        {
            var response = await _productCategoryAppService.CreateObjAsync(dto);

            return CreateResponseOnPost(response, EntityNames.ProductCategory);
        }

        /// <summary>
        /// Altera os detalhes de um grupo de produtos
        /// </summary>
        [HttpPut("{id}")]
        [ThexAuthorize("PDV_ProductCategory_Put")]
        [ProducesResponseType(typeof(ProductCategoryDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Put(string id, [FromBody] ProductCategoryDto dto)
        {
            var response = await _productCategoryAppService.EditObjAsync(Guid.Parse(id), dto);

            return CreateResponseOnPut(response, EntityNames.ProductCategory);
        }

        /// <summary>
        /// Remove um grupo de produto
        /// </summary>
        /// <returns></returns>
        [HttpDelete("{id}")]
        [ThexAuthorize("PDV_ProductCategory_Delete")]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Delete(string id)
        {
            await _productCategoryAppService.DeleteObjAsync(Guid.Parse(id));

            return CreateResponseOnDelete(EntityNames.ProductCategory);
        }

        /// <summary>
        /// Ativa ou desativa um grupo de produtos
        /// </summary>
        /// <returns></returns>
        [HttpPatch("{id}/toggleactivation")]
        [ThexAuthorize("PDV_ProductCategory_Patch_ToggleActivation")]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> ToggleActivation(string id)
        {
            await _productCategoryAppService.ToggleActivationObjAsync(Guid.Parse(id));

            return CreateResponseOnPatch(null, EntityNames.ProductCategory);
        }
    }
}