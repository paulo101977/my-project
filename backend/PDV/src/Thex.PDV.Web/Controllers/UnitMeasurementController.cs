﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Thex.Kernel;
using Thex.PDV.Application.Interfaces;
using Thex.PDV.Base;
using Thex.PDV.Domain;
using Thex.PDV.Dto;
using Tnf.AspNetCore.Mvc.Response;
using Tnf.Dto;

namespace Thex.PDV.Web.Controllers
{
    [Route(RouteConsts.UnitMeasurementRouteName)]
    public class UnitMeasurementController : ThexAppController
    {
        private readonly IUnitMeasurementAppService _unitMeasurementAppService;

        public UnitMeasurementController(IUnitMeasurementAppService unitMeasurementAppService,
                                         IApplicationUser applicationUser)
            :base(applicationUser)
        {
            _unitMeasurementAppService = unitMeasurementAppService;
        }

        /// <summary>
        /// Retorna a lista de todos os Ncm's disponíveis
        /// </summary>
        [HttpGet]
        [ThexAuthorize("PDV_UnitMeasurement_Get")]
        [ProducesResponseType(typeof(IListDto<UnitMeasurementDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> GetAll([FromQuery]GetAllUnitMeasurementDto requestDto)
        {
            var response = await _unitMeasurementAppService.GetAll(requestDto);

            return CreateResponseOnGetAll(response, EntityNames.UnitMeasurement);
        }
    }
}