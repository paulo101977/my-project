﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Thex.Kernel;
using Thex.PDV.Application.Interfaces;
using Thex.PDV.Base;
using Thex.PDV.Domain;
using Thex.PDV.Dto.Dto.BillingAccountItemDebit;
using Tnf.AspNetCore.Mvc.Response;

namespace Thex.PDV.Web.Controllers
{
    [Route(RouteConsts.BillingAccountItem)]
    public class BillingAccountItemController : ThexAppController
    {
        private readonly IBillingAccountItemAppService _billingAccountItemAppService;

        public BillingAccountItemController(IBillingAccountItemAppService billingAccountItemAppService,
                                            IApplicationUser applicationUser)
            : base(applicationUser)
        {
            _billingAccountItemAppService = billingAccountItemAppService;
        }

        [HttpPost("debit")]
        [ThexAuthorize("PDV_BillingAccountItem_Post_Debit")]
        [ProducesResponseType(typeof(BillingAccountItemDebitDto), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> PostDebit([FromBody] BillingAccountItemDebitDto dto)
        {
            var response = await _billingAccountItemAppService.CreateDebit(dto);

            return CreateResponseOnPost(response, EntityNames.BillingAccountItem);
        }
    }
}