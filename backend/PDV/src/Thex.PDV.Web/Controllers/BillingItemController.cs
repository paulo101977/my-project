﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Thex.Kernel;
using Thex.PDV.Application.Interfaces;
using Thex.PDV.Base;
using Thex.PDV.Domain;
using Thex.PDV.Dto;
using Tnf.AspNetCore.Mvc.Response;
using Tnf.Dto;

namespace Thex.PDV.Web.Controllers
{
    [Route(RouteConsts.BillingItem)]
    public class BillingItemController : ThexAppController
    {
        private readonly IBillingItemAppService _billingItemAppService;

        public BillingItemController(IBillingItemAppService billingItemAppService,
                                     IApplicationUser applicationUser)
             : base(applicationUser)
        {
            _billingItemAppService = billingItemAppService;
        }

        /// <summary>
        /// Retorna a lista de todos os POS
        /// </summary>
        [HttpGet]
        [ThexAuthorize("PDV_BillingItem_Get")]
        [ProducesResponseType(typeof(IListDto<BillingItemDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> GetAll([FromQuery]GetAllBillingItemDto requestDto)
        {
            var response = await _billingItemAppService.GetAll(requestDto);

            return CreateResponseOnGetAll(response, EntityNames.BillingItem);
        }
    }
}