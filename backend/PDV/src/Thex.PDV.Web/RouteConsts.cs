﻿namespace Thex.PDV.Web
{
    public class RouteConsts
    {
        public const string TransportationTypeRouteName = "api/transportationType";
        public const string ProductCategoryRouteName = "api/productCategory";
        public const string ProductRouteName = "api/product";
        public const string ProductBillingItemRouteName = "api/productBillingItem";
        public const string NcmRouteName = "api/ncm";
        public const string UnitMeasurementRouteName = "api/unitMeasurement";
        public const string BillingItem = "api/billingItem";
        public const string BillingAccountItem = "api/BillingAccountItem";
    }
}
