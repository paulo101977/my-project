﻿CREATE TABLE [dbo].[TransportationType]
(
	[TransportationTypeId] INT NOT NULL PRIMARY KEY, 
    [TransportationTypeName] NVARCHAR(200) NULL
)
