﻿using Newtonsoft.Json;
using System;
using Thex.Common.Enumerations;
using Thex.PDV.Domain.Entities;
using Thex.PDV.Dto.Dto.BillingAccountItemDebit;
using Tnf.Notifications;

namespace Thex.PDV.Application.Adapters
{
    public class BillingAccountItemAdapter : IBillingAccountItemAdapter
    {
        private readonly INotificationHandler _notificationHandler;

        public BillingAccountItemAdapter(INotificationHandler notificationHandler)
        {
            _notificationHandler = notificationHandler;
        }

        public BillingAccountItem.Builder MapDebitTax(BillingAccountItemDebitDto dto, Guid parentId)
        {
            var builder = new BillingAccountItem.Builder(_notificationHandler)
                .WithId(Guid.NewGuid())
                .WithBillingAccountId(dto.BillingAccountId)
                .WithBillingItemId(dto.BillingItemId)
                .WithBillingAccountItemTypeId((int)BillingAccountItemTypeEnum.BillingAccountItemTypeDebit)
                .WithAmount(dto.Amount * (-1))
                .WithBillingAccountItemDate(dto.DateParameter.Value)
                .WithCurrencyId(dto.CurrencyId.Value)
                .WithCurrencyExchangeReferenceId(dto.CurrencyExchangeReferenceId)
                .WithCurrencyExchangeReferenceSecId(dto.CurrencyExchangeReferenceSecId)
                .WithDateParameter(dto.DateParameter)
                .WithBillingAccountItemParentId(parentId);

            return builder;
        }

        public BillingAccountItem.Builder MapDebit(BillingAccountItemDebitDto dto)
        {
            var builder = new BillingAccountItem.Builder(_notificationHandler)
                .WithId(Guid.NewGuid())
                .WithBillingAccountId(dto.BillingAccountId)
                .WithBillingItemId(dto.BillingItemId)
                .WithBillingAccountItemTypeId((int)BillingAccountItemTypeEnum.BillingAccountItemTypeDebit)
                .WithAmount(dto.Amount * (-1))
                .WithBillingAccountItemDate(dto.DateParameter.Value)
                .WithCurrencyId(dto.CurrencyId.Value)
                .WithCurrencyExchangeReferenceId(dto.CurrencyExchangeReferenceId)
                .WithCurrencyExchangeReferenceSecId(dto.CurrencyExchangeReferenceSecId)
                .WithDateParameter(dto.DateParameter);

            if (dto.DetailList != null && dto.DetailList.Count > 0)
                builder.WithIntegrationDetail(JsonConvert.SerializeObject(dto.DetailList));

            return builder;
        }
    }
}
