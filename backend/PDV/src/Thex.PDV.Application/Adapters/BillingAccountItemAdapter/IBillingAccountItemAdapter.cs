﻿using System;
using Thex.PDV.Domain.Entities;
using Thex.PDV.Dto.Dto.BillingAccountItemDebit;

namespace Thex.PDV.Application.Adapters
{
    public interface IBillingAccountItemAdapter
    {
        BillingAccountItem.Builder MapDebit(BillingAccountItemDebitDto dto);
        BillingAccountItem.Builder MapDebitTax(BillingAccountItemDebitDto dto, Guid parentId);
    }
}
