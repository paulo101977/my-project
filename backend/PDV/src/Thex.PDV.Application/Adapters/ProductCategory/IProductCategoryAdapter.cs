﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a Crudzilla tool.
// </auto-generated>
//------------------------------------------------------------------------------
using Thex.PDV.Domain.Entities;
using Thex.PDV.Dto;
using Tnf.Dependency;

namespace Thex.PDV.Application.Adapters
{
    public interface IProductCategoryAdapter: ITransientDependency
    {
        ProductCategory.Builder Map(ProductCategoryDto dto);
    }
}
