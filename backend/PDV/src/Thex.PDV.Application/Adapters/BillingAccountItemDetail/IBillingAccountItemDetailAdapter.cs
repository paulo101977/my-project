﻿using Thex.PDV.Domain.Entities;
using Thex.PDV.Dto.Dto.BillingAccountItem;
using Tnf.Dependency;

namespace Thex.PDV.Application.Adapters
{
    public interface IBillingAccountItemDetailAdapter
    {
        BillingAccountItemDetail.Builder Map(BillingAccountItemDetailDto dto);
    }
}
