﻿using Thex.PDV.Domain.Entities;
using Thex.PDV.Dto.Dto.BillingAccountItem;
using Tnf;
using Tnf.Notifications;

namespace Thex.PDV.Application.Adapters
{
    public class BillingAccountItemDetailAdapter : IBillingAccountItemDetailAdapter
    {
        private readonly INotificationHandler _notificationHandler;

        public BillingAccountItemDetailAdapter(INotificationHandler notificationHandler)
        {
            _notificationHandler = notificationHandler;
        }

        public BillingAccountItemDetail.Builder Map(BillingAccountItemDetailDto dto)
        {
            Check.NotNull(dto, nameof(dto));

            var builder = new BillingAccountItemDetail.Builder(_notificationHandler)
                .WithId(dto.Id)
                .WithAmount(dto.Amount)
                .WithBillingAccountItemId(dto.BillingAccountItemId)
                .WithProductId(dto.ProductId)
                .WithQuantity(dto.Quantity);

            return builder;
        }
    }
}
