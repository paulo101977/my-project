﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Text;
using System.Threading.Tasks;
using Thex.PDV.Dto;
using Tnf.Application.Services;
using Tnf.Dto;

namespace Thex.PDV.Application.Interfaces
{
    public interface IBillingItemAppService : IApplicationService
    {
        Task<IListDto<BillingItemDto>> GetAll(GetAllBillingItemDto requestDto);
    }
}
