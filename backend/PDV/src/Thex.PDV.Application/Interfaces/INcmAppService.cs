﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Thex.PDV.Dto;
using Thex.PDV.Dto.GetAll;
using Tnf.Application.Services;
using Tnf.Dto;

namespace Thex.PDV.Application.Interfaces
{
    public interface INcmAppService : IApplicationService
    {
        Task<IListDto<NcmDto>> GetAll(GetAllNcmDto requestDto);
        Task<IListDto<NcmDto>> GetAllByFilters(SearchNcmDto requestDto);
    }
}
