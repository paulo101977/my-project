﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.PDV.Dto;
using Thex.PDV.Dto.Product;
using Tnf.Application.Services;
using Tnf.Dto;

namespace Thex.PDV.Application.Interfaces
{
    public interface IProductAppService : IApplicationService
    {
        Task<IListDto<ProductDto>> GetAll(GetAllProductDto requestDto);

        Task<ProductDto> GetById(Guid id);

        Task<ProductDto> GetImageBarCode(GetAllProductDto product);

        Task<IListDto<ProductDto>> GetAllByFilters(SearchProductDto requestDto, int billingItemId);

        Task<IListDto<ProductDto>> GetByCategoryId(Guid categoryId, int billingItemId, GetAllProductDto requestDto);

        Task<ProductDto> CreateObjAsync(ProductDto dto);

        Task<ProductDto> EditObjAsync(Guid id, ProductDto productDto);

        Task DeleteObjAsync(Guid id);

        Task ToggleActivationObjAsync(Guid id);

        Task<List<ProductDto>> GetAllIdAndName();
    }
}
