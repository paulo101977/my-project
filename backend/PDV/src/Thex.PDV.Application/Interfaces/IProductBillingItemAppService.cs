﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Thex.PDV.Domain.Entities;
using Thex.PDV.Dto;
using Tnf.Application.Services;
using Tnf.Dto;

namespace Thex.PDV.Application.Interfaces
{
    public interface IProductBillingItemAppService : IApplicationService
    {
        Task<IListDto<ProductBillingItemDto>> GetAll(GetAllProductBillingItemDto requestDto);
    }
}
