﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Thex.PDV.Dto.Dto.BillingAccountItemDebit;
using Tnf.Application.Services;

namespace Thex.PDV.Application.Interfaces
{
    public interface IBillingAccountItemAppService : IApplicationService
    {
        Task<BillingAccountItemDebitDto> CreateDebit(BillingAccountItemDebitDto dto);
    }
}
