﻿using System;
using System.Threading.Tasks;
using Thex.PDV.Dto;
using Tnf.Application.Services;
using Tnf.Dto;

namespace Thex.PDV.Application.Interfaces
{
    public interface IProductCategoryAppService : IApplicationService
    {
        Task<IListDto<ProductCategoryDto>> GetAllGroupFixed();

        Task<IListDto<ProductCategoryDto>> GetAll(GetAllProductCategoryDto request);

        Task<IListDto<ProductCategoryDto>> GetAllByBillingItemId(GetAllProductCategoryDto request);

        Task<ProductCategoryDto> GetById(Guid id);

        Task<ProductCategoryDto> CreateObjAsync(ProductCategoryDto dto);

        Task<ProductCategoryDto> EditObjAsync(Guid id, ProductCategoryDto productDto);

        Task DeleteObjAsync(Guid id);

        Task ToggleActivationObjAsync(Guid id);
    }
}
