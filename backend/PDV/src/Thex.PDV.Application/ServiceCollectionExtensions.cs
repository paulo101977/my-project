﻿using Thex.PDV.Application.Adapters;
using Thex.PDV.Application.Interfaces;
using Thex.PDV.Application.Services;

namespace Microsoft.Extensions.DependencyInjection
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddApplicationServiceDependency(this IServiceCollection services)
        {
            // Dependencia do projeto Thex.Domain
            services
                .AddDomainDependency()
                .AddTnfDefaultConventionalRegistrations();

            // Registro dos serviços
            services.AddTransient<IUnitMeasurementAdapter, UnitMeasurementAdapter>();
            services.AddTransient<INcmAdapter, NcmAdapter>();
            services.AddTransient<IProductBillingItemAdapter, ProductBillingItemAdapter>();
            services.AddTransient<IProductCategoryAdapter, ProductCategoryAdapter>();
            services.AddTransient<IProductAdapter, ProductAdapter>();
            services.AddTransient<IBillingItemAdapter, BillingItemAdapter>();
            services.AddTransient<IBillingAccountItemAdapter, BillingAccountItemAdapter>();
            services.AddTransient<IBillingAccountItemDetailAdapter, BillingAccountItemDetailAdapter>();


            services.AddTransient<IBillingAccountItemAppService, BillingAccountItemAppService>();
            services.AddTransient<IBillingItemAppService, BillingItemAppService>();
            services.AddTransient<INcmAppService, NcmAppService>();
            services.AddTransient<IUnitMeasurementAppService, UnitMeasurementAppService>();
            services.AddTransient<IProductCategoryAppService, ProductCategoryAppService>();
            services.AddTransient<IProductAppService, ProductAppService>();
            services.AddTransient<IProductBillingItemAppService, ProductBillingItemAppService>();

            return services;
        }
    }
}