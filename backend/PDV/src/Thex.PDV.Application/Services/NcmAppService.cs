﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Thex.Kernel;
using Thex.PDV.Application.Adapters;
using Thex.PDV.Application.Interfaces;
using Thex.PDV.Domain.Entities;
using Thex.PDV.Dto;
using Thex.PDV.Dto.GetAll;
using Thex.PDV.Infra.Interfaces;
using Thex.PDV.Infra.Interfaces.ReadRepositories;
using Tnf.Dto;
using Tnf.Notifications;

namespace Thex.PDV.Application.Services
{
    public class NcmAppService : ApplicationServiceBase<NcmDto, Ncm>, INcmAppService
    {
        private readonly IApplicationUser _applicationUser;
        private readonly INcmAdapter _ncmAdapter;
        private readonly INcmReadRepository _ncmReadRepository;


        public NcmAppService(INcmReadRepository ncmReadRepository,
          IApplicationUser applicationUser,
          INcmAdapter ncmAdapter,
          INotificationHandler notificationHandler)
          : base(notificationHandler, null)
        {
            _applicationUser = applicationUser;
            _ncmAdapter = ncmAdapter;
            _ncmReadRepository = ncmReadRepository;
        }

        public override Ncm GeneratedEntity(NcmDto dto)
        {
            return _ncmAdapter.Map(dto).Build();
        }

        public async Task<IListDto<NcmDto>> GetAll(GetAllNcmDto requestDto)
        {
            ValidateRequestAllDto(requestDto, nameof(requestDto));

            if (Notification.HasNotification())
                return null;

            return await _ncmReadRepository.GetAll(requestDto);
        }

        public async Task<IListDto<NcmDto>> GetAllByFilters(SearchNcmDto searchDto)
        {
            return await _ncmReadRepository.GetAllByFilters(searchDto);
        }
    }
}

