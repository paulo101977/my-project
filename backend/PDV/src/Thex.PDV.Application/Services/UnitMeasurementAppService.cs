﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Thex.Common;
using Thex.Common.Enumerations;
using Thex.Kernel;
using Thex.PDV.Application.Adapters;
using Thex.PDV.Application.Interfaces;
using Thex.PDV.Domain.Entities;
using Thex.PDV.Dto;
using Thex.PDV.Infra.Interfaces.ReadRepositories;
using Tnf.Dto;
using Tnf.Localization;
using Tnf.Notifications;

namespace Thex.PDV.Application.Services
{
    public class UnitMeasurementAppService : ApplicationServiceBase<UnitMeasurementDto, UnitMeasurement>, IUnitMeasurementAppService
    {
        private string __unitMeasureKilo = "Kilo";
        private readonly IApplicationUser _applicationUser;
        private readonly IUnitMeasurementAdapter _unitMeasurementAdapter;
        private readonly IUnitMeasurementReadRepository _unitMeasurementReadRepository;
        private readonly ILocalizationManager _localizationManager;

        public UnitMeasurementAppService(IUnitMeasurementReadRepository unitMeasurementReadRepository,
          IApplicationUser applicationUser,
          IUnitMeasurementAdapter unitMeasurementAdapter,
          INotificationHandler notificationHandler,
          ILocalizationManager localizationManager)
          : base(notificationHandler, null)
        {
            _applicationUser = applicationUser;
            _unitMeasurementAdapter = unitMeasurementAdapter;
            _unitMeasurementReadRepository = unitMeasurementReadRepository;
            _localizationManager = localizationManager;
        }

        public override UnitMeasurement GeneratedEntity(UnitMeasurementDto dto)
        {
            return _unitMeasurementAdapter.Map(dto).Build();
        }

        public async Task<IListDto<UnitMeasurementDto>> GetAll(GetAllUnitMeasurementDto requestDto)
        {
            ValidateRequestAllDto(requestDto, nameof(requestDto));

            if (Notification.HasNotification())
                return null;

            var unitMeasureList = await _unitMeasurementReadRepository.GetAll(requestDto);

            return unitMeasureList;
        }
    }
}
