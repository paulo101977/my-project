﻿using System;
using System.Threading.Tasks;
using Thex.Common;
using Thex.Common.Enumerations;
using Thex.PDV.Application.Adapters;
using Thex.PDV.Application.Interfaces;
using Thex.PDV.Domain.Entities;
using Thex.PDV.Domain.Enumerations;
using Thex.PDV.Dto;
using Thex.PDV.Infra.Interfaces;
using Thex.PDV.Infra.Interfaces.ReadRepositories;
using Tnf;
using Tnf.Application.Services;
using Tnf.Builder;
using Tnf.Domain.Services;
using Tnf.Dto;
using Tnf.Notifications;

namespace Thex.PDV.Application.Services
{
    public class ProductCategoryAppService : ApplicationServiceBase<ProductCategoryDto, ProductCategory>, IProductCategoryAppService
    {
        private readonly IProductCategoryAdapter _productCategoryAdapter;
        private readonly IProductCategoryReadRepository _productCategoryReadRepository;
        private readonly IProductCategoryRepository _productCategorydRepository;
        private readonly IProductReadRepository _productReadRepository;

        public ProductCategoryAppService(IProductCategoryAdapter productCategoryAdapter,
                                         IProductCategoryReadRepository productCategoryReadRepository,
                                         IProductCategoryRepository productCategoryRepository,
                                         INotificationHandler notificationHandler,
                                         IProductReadRepository productReadRepository)
                                         : base(notificationHandler, productCategoryRepository)
        {
            _productReadRepository = productReadRepository;
            _productCategorydRepository = productCategoryRepository;
            _productCategoryReadRepository = productCategoryReadRepository;
            _productCategoryAdapter = Check.NotNull(productCategoryAdapter, nameof(productCategoryAdapter));
        }

        public new async Task DeleteObjAsync(Guid id)
        {
            if (!_productReadRepository.ExistProductInCategory(id).Result)
                await base.DeleteObjAsync(id);
            else
                Notification.Raise(Notification.DefaultBuilder
                    .WithMessage(AppConsts.LocalizationSourceName, ProductEnum.Error.ProductCategoryWithProducts)
                    .WithDetailedMessage(AppConsts.LocalizationSourceName, ProductEnum.Error.ProductCategoryWithProducts)
                    .Build());
        }

        public async Task<IListDto<ProductCategoryDto>> GetAllGroupFixed()
        {
            return await _productCategoryReadRepository.GetAllGroupFixed();
        }

        public async Task<ProductCategoryDto> GetById(Guid id)
        {
            return await _productCategoryReadRepository.GetCategoryById(id);
        }

        public async Task<IListDto<ProductCategoryDto>> GetAll(GetAllProductCategoryDto requestDto)
        {
            ValidateRequestAllDto(requestDto, nameof(requestDto));

            if (Notification.HasNotification())
                return null;

            return await _productCategoryReadRepository.GetAllCategories(requestDto);
        }

        public async Task<IListDto<ProductCategoryDto>> GetAllByBillingItemId(GetAllProductCategoryDto requestDto)
        {
            ValidateRequestAllDto(requestDto, nameof(requestDto));

            if (Notification.HasNotification())
                return null;

            return await _productCategoryReadRepository.GetAllByBillingItemId(requestDto);
        }

        public override ProductCategory GeneratedEntity(ProductCategoryDto dto)
        {
            return _productCategoryAdapter.Map(dto).Build();
        }
    }
}
