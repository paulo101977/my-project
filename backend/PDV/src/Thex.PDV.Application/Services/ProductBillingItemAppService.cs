﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Thex.PDV.Application.Adapters;
using Thex.PDV.Application.Interfaces;
using Thex.PDV.Domain.Entities;
using Thex.PDV.Dto;
using Thex.PDV.Infra.Interfaces;
using Thex.PDV.Infra.Interfaces.ReadRepositories;
using Tnf.Dto;
using Tnf.Notifications;

namespace Thex.PDV.Application.Services
{
    public class ProductBillingItemAppService : ApplicationServiceBase<ProductBillingItemDto, ProductBillingItem>, IProductBillingItemAppService
    {
        private readonly IProductBillingItemRepository _productBillingItemRepository;
        private readonly IProductBillingItemAdapter _productBillingItemAdapter;
        private readonly INotificationHandler _notificationHandler;
        private readonly IProductBillingItemReadRepository _productBillingItemReadRepository;

        public ProductBillingItemAppService(IProductBillingItemReadRepository productBillingItemReadRepository,
                                            IProductBillingItemRepository productBillingItemRepository,
                                            INotificationHandler notificationHandler,
                                            IProductBillingItemAdapter productBillingItemAdapter)
                               : base(notificationHandler, productBillingItemRepository)
        {
            _productBillingItemRepository = productBillingItemRepository;
            _productBillingItemAdapter = productBillingItemAdapter;
            _notificationHandler = notificationHandler;
            _productBillingItemReadRepository = productBillingItemReadRepository;
        }

        public override ProductBillingItem GeneratedEntity(ProductBillingItemDto dto)
        {
            return _productBillingItemAdapter.Map(dto).Build();
        }

        public async Task<IListDto<ProductBillingItemDto>> GetAll(GetAllProductBillingItemDto requestDto)
        {
            ValidateRequestAllDto(requestDto, nameof(requestDto));

            if (Notification.HasNotification())
                return null;

            return await _productBillingItemReadRepository.GetAll(requestDto);
        }
    }
}
