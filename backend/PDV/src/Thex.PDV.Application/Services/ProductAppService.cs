﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Thex.Common;
using Thex.Common.Enumerations;
using Thex.PDV.Application.Adapters;
using Thex.PDV.Application.Interfaces;
using Thex.PDV.Domain.Entities;
using Thex.PDV.Domain.Enumerations;
using Thex.PDV.Dto;
using Thex.PDV.Dto.Product;
using Thex.PDV.Infra.Interfaces;
using Thex.PDV.Infra.Interfaces.ReadRepositories;
using Tnf.Builder;
using Tnf.Dto;
using Tnf.Notifications;
using Tnf.Repositories.Uow;

namespace Thex.PDV.Application.Services
{
    public class ProductAppService : ApplicationServiceBase<ProductDto, Product>, IProductAppService
    {
        private readonly IProductReadRepository _productReadRepository;
        private readonly IProductRepository _productRepository;
        private readonly IProductAdapter _productAdapter;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly INotificationHandler _notificationHandler;
        private readonly IProductBillingItemRepository _productBillingItemRepository;
        private readonly IProductBillingItemReadRepository _productBillingItemReadRepository;
        private readonly IProductBillingItemAdapter _productBillingItemAdapter;
        private readonly IBillingItemReadRepository _billingItemReadRepository;
        private readonly ServicesConfiguration _servicesConfiguration;

        public ProductAppService(INotificationHandler notificationHandler,
                                 IUnitOfWorkManager unitOfWorkManager,
                                 IProductReadRepository productReadRepository,
                                 IProductRepository productRepository,
                                 IProductBillingItemRepository productBillingItemRepository,
                                 IProductBillingItemReadRepository productBillingItemReadRepository,
                                 IProductAdapter productAdapter,
                                 IProductBillingItemAdapter productBillingItemAdapter,
                                 IBillingItemReadRepository billingItemReadRepository,
                                 ServicesConfiguration serviceConfiguration)
                                 : base(notificationHandler, productRepository)
        {
            _productReadRepository = productReadRepository;
            _productRepository = productRepository;
            _productAdapter = productAdapter;
            _unitOfWorkManager = unitOfWorkManager;
            _notificationHandler = notificationHandler;
            _productBillingItemRepository = productBillingItemRepository;
            _productBillingItemReadRepository = productBillingItemReadRepository;
            _productBillingItemAdapter = productBillingItemAdapter;
            _billingItemReadRepository = billingItemReadRepository;
            _servicesConfiguration = serviceConfiguration;
        }

        #region Product

        public new async Task<ProductDto> CreateObjAsync(ProductDto dto)
        {
            Product product = null;

            using (var uow = _unitOfWorkManager.Begin())
            {
                product = _productAdapter.Map(dto).Build();

                await _productRepository.InsertAsync(product);

                foreach (var billingItem in dto.BillingItens)
                {
                    await CreateAssociation(product.Id, billingItem);
                }

                uow.Complete();
            }

            if (_notificationHandler.HasErrorNotification())
                return ProductDto.NullInstance;

            return dto;
        }

        public new async Task<ProductDto> EditObjAsync(Guid id, ProductDto dto)
        {
            Product product = null;

            using (var uow = _unitOfWorkManager.Begin())
            {
                var dbProduct = await _productReadRepository.GetProductById(id);

                if (dbProduct == null)
                {
                    _notificationHandler.Raise(_notificationHandler
                                        .DefaultBuilder
                                        .WithMessage(AppConsts.LocalizationSourceName, CommonsEnum.Error.ParameterInvalid)
                                        .Build());
                    return ProductDto.NullInstance;
                }

                product = _productAdapter.Map(dto).Build();
                product.Id = dbProduct.Id;
                product.CreationTime = dbProduct.CreationTime;
                product.CreatorUserId = dbProduct.CreatorUserId;

                await _productRepository.UpdateEntity(product);
                await ChangeAssociations(dbProduct, dto);

                if (_notificationHandler.HasErrorNotification())
                    return ProductDto.NullInstance;

                uow.Complete();
            }

            return dto;
        }

        public new async Task DeleteObjAsync(Guid id)
        {
            using (var uow = _unitOfWorkManager.Begin())
            {
                await _productRepository.RemoveAsync(id);

                var prodBillingItens = await _productBillingItemReadRepository.GetBillingItensForProduct(id);

                foreach (var billingItem in prodBillingItens.Items)
                {
                    await _productBillingItemRepository.RemoveAsync(billingItem.Id);
                }

                uow.Complete();
            }

            if (_notificationHandler.HasErrorNotification())
                return;
        }

        public new async Task ToggleActivationObjAsync(Guid id)
        {
            using (var uow = _unitOfWorkManager.Begin())
            {
                await _productRepository.ToggleAsync(id);

                uow.Complete();
            }

            if (_notificationHandler.HasErrorNotification())
                return;
        }


        #endregion

        #region Association

        private async Task UpdateAssociationValues(ProductBillingItemDto oldDto, ProductBillingItemDto newDto)
        {
            if (newDto.UnitPrice == 0m || newDto.UnitPrice is null && newDto.IsActive)
                _notificationHandler.Raise(_notificationHandler
                                            .DefaultBuilder
                                            .WithMessage(AppConsts.LocalizationSourceName, ProductBillingItemEnum.Error.ProducutWithoutAmount)
                                            .Build());

            if (oldDto.UnitPrice != newDto.UnitPrice ||
                oldDto.IsActive != newDto.IsActive)
            {
                oldDto.UnitPrice = newDto.UnitPrice;
                oldDto.IsActive = newDto.IsActive;

                await _productBillingItemRepository.UpdateAsync(oldDto.Id, _productBillingItemAdapter.Map(oldDto).Build());
            }
        }

        private async Task CreateAssociation(Guid id, ProductBillingItemDto productBillingItemDto)
        {
            if (productBillingItemDto.UnitPrice == 0m || productBillingItemDto.UnitPrice == null)
                _notificationHandler.Raise(_notificationHandler
                                            .DefaultBuilder
                                            .WithMessage(AppConsts.LocalizationSourceName, ProductBillingItemEnum.Error.ProducutWithoutAmount)
                                            .Build());

            var prodBillingItem = _productBillingItemAdapter.Map(productBillingItemDto)
                                                                            .WithProductId(id)
                                                                            .Build();

            await _productBillingItemRepository.InsertAsync(prodBillingItem as BaseEntity);
        }

        private async Task ChangeAssociations(ProductDto dbProduct, ProductDto dto)
        {
            await DeactivateProductBillingItem(dbProduct, dto);

            foreach (var productBillingItem in dto.BillingItens)
            {
                if (_billingItemReadRepository.GetById(productBillingItem.BillingItemId).IsActive)
                {
                    var dbEntity = dbProduct.BillingItens.Find(x => x.BillingItemId == productBillingItem.BillingItemId);

                    if (dbEntity != null)
                    {
                        await UpdateAssociationValues(dbEntity, productBillingItem);
                    }
                    else
                    {
                        await CreateAssociation(dbProduct.Id, productBillingItem);
                    }
                }
                else
                {
                    _notificationHandler.Raise(_notificationHandler
                                        .DefaultBuilder
                                        .WithMessage(AppConsts.LocalizationSourceName, ProductBillingItemEnum.Error.DeactivatedBillingItem)
                                        .Build());
                    break;
                }
            }
        }

        private async Task DeactivateProductBillingItem(ProductDto dbProduct, ProductDto dto)
        {
            var billingItens = await _productBillingItemReadRepository.GetBillingItensForProduct(dbProduct.Id);

            foreach (var productBillingItem in billingItens.Items)
            {
                var productDto = dto.BillingItens.Find(x => x.BillingItemId == productBillingItem.BillingItemId);

                if (productDto == null)
                {
                    await UpdateAssociationValues(productBillingItem, new ProductBillingItemDto()
                    {
                        IsActive = false,
                        UnitPrice = productBillingItem.UnitPrice
                    });
                }
            }
        }

        #endregion

        public virtual async Task<ProductDto> GetById(Guid id)
        {
            return await _productReadRepository.GetProductById(id);
        }

        public virtual async Task<ProductDto> GetImageBarCode(GetAllProductDto product)
        {
            var client = new HttpClient();
            var _body = new
            {
                image = product.ImageBase64
            };

            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            var _content = new StringContent(JToken.FromObject(_body).ToString(), Encoding.UTF8, "application/json");
            var _res = new ProductDto();
            var result = await client.PostAsync(_servicesConfiguration.BarCodeAPI, _content);

            if (result.IsSuccessStatusCode)
            {
                var _obj = JToken.Parse(result.Content.ReadAsStringAsync().Result);
                if (_obj != null && _obj["Barcodes"] != null)
                {
                    _res.BarCode = _obj["Barcodes"][0]["Text"].ToString();
                }
            }

            return _res;
        }

        public virtual async Task<IListDto<ProductDto>> GetAllByFilters(SearchProductDto requestDto, int billingItemId)
        {
            return await _productReadRepository.GetAllByFilters(requestDto, billingItemId);
        }

        public virtual async Task<IListDto<ProductDto>> GetByCategoryId(Guid categoryId, int billingItemId, GetAllProductDto requestDto)
        {
            return await _productReadRepository.GetProductByCategory(categoryId, billingItemId);
        }

        public virtual async Task<IListDto<ProductDto>> GetAll(GetAllProductDto requestDto)
        {
            ValidateRequestAllDto(requestDto, nameof(requestDto));

            if (Notification.HasNotification())
                return null;

            return await _productReadRepository.GetAll(requestDto);
        }

        public override Product GeneratedEntity(ProductDto dto)
        {
            return _productAdapter.Map(dto).Build();
        }

        public async Task<List<ProductDto>> GetAllIdAndName()
        {
            return await _productReadRepository.GetAllIdAndName();
        }
    }
}
