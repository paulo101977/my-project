﻿using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Thex.Common;
using Thex.Common.Enumerations;
using Thex.Common.Helpers;
using Thex.Kernel;
using Thex.PDV.Application.Adapters;
using Thex.PDV.Application.Interfaces;
using Thex.PDV.Domain.Entities;
using Thex.PDV.Domain.Entities.Thex;
using Thex.PDV.Dto;
using Thex.PDV.Dto.Dto.BillingAccountItem;
using Thex.PDV.Dto.Dto.BillingAccountItemDebit;
using Thex.PDV.Dto.Dto.BillingItemServiceAndTaxAssociate;
using Thex.PDV.Infra.Interfaces;
using Thex.PDV.Infra.Interfaces.ReadRepositories;
using Tnf.Notifications;
using Tnf.Repositories.Uow;

namespace Thex.PDV.Application.Services
{
    public class BillingAccountItemAppService : ApplicationServiceBase<BillingAccountItemDebitDto, BillingAccountItem>, IBillingAccountItemAppService
    {
        private readonly IRatePlanReadRepository _ratePlanReadRepository;
        private readonly IBillingAccountItemRepository _billingAccountItemRepository;
        private readonly IBillingItemReadRepository _billingItemReadRepository;
        private readonly IApplicationUser _applicationUser;
        private readonly IBillingAccountItemAdapter _billingAccountItemAdapter;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IBillingAccountReadRepository _billingAccountReadRepository;
        private readonly IPropertyCurrencyExchangeReadRepository _propertyCurrencyExchangeReadRepository;
        private readonly IPropertyParameterReadRepository _propertyParameterReadRepository;
        private readonly ServicesConfiguration _servicesConfiguration;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IBillingAccountItemDetailAdapter _billingAccountItemDetailAdapter;
        private readonly IBillingAccountItemDetailRepository _billingAccountItemDetailRepository;

        private readonly string _urlMostSelected = "/api/userpreference/mostselected";

        public BillingAccountItemAppService(IRatePlanReadRepository ratePlanReadRepository,
                                            IBillingAccountItemRepository billingAccountItemRepository,
                                            IApplicationUser applicationUser,
                                            INotificationHandler notificationHandler,
                                            IBillingAccountItemAdapter billingAccountItemAdapter,
                                            IUnitOfWorkManager unitOfWorkManager,
                                            IBillingItemReadRepository billingItemReadRepository,
                                            IBillingAccountReadRepository billingAccountReadRepository,
                                            IPropertyCurrencyExchangeReadRepository propertyCurrencyExchangeReadRepository,
                                            IPropertyParameterReadRepository propertyParameterReadRepository,
                                            ServicesConfiguration serviceConfiguration,
                                            IHttpContextAccessor httpContextAccessor,
                                            IBillingAccountItemDetailAdapter billingAccountItemDetailAdapter,
                                            IBillingAccountItemDetailRepository billingAccountItemDetailRepository)
          : base(notificationHandler, null)
        {
            _applicationUser = applicationUser;
            _billingAccountItemAdapter = billingAccountItemAdapter;
            _unitOfWorkManager = unitOfWorkManager;
            _billingItemReadRepository = billingItemReadRepository;
            _billingAccountItemRepository = billingAccountItemRepository;
            _ratePlanReadRepository = ratePlanReadRepository;
            _billingAccountReadRepository = billingAccountReadRepository;
            _propertyCurrencyExchangeReadRepository = propertyCurrencyExchangeReadRepository;
            _propertyParameterReadRepository = propertyParameterReadRepository;
            _servicesConfiguration = serviceConfiguration;
            _httpContextAccessor = httpContextAccessor;
            _billingAccountItemDetailAdapter = billingAccountItemDetailAdapter;
            _billingAccountItemDetailRepository = billingAccountItemDetailRepository;
        }

        public override BillingAccountItem GeneratedEntity(BillingAccountItemDebitDto dto)
        {
            return _billingAccountItemAdapter.MapDebit(dto).Build();
        }

        private void ValidateDebit(BillingAccountItemDebitDto dto)
        {
            if (!_billingItemReadRepository.AnyBillingItemByBillingItemType(dto.BillingItemId, (int)BillingItemTypeEnum.PointOfSale).Result)
            {
                Notification.Raise(Notification.DefaultBuilder
                .WithMessage(AppConsts.LocalizationSourceName, BillingAccountItemEnum.Error.BillingAccountItemInvalidBillingItemId)
                .WithDetailedMessage(AppConsts.LocalizationSourceName, BillingAccountItemEnum.Error.BillingAccountItemInvalidBillingItemId)
                .Build());
            }

            ValidateNegativeAmount(dto.Amount);
            IsClosedOrBlockedAccount(dto.BillingAccountId);
        }

        private void ValidateNegativeAmount(decimal amount)
        {
            if (amount <= 0)
            {
                Notification.Raise(Notification.DefaultBuilder
                .WithMessage(AppConsts.LocalizationSourceName, BillingAccountItemEnum.Error.BillingAccountItemInvalidAmount)
                .WithDetailedMessage(AppConsts.LocalizationSourceName, BillingAccountItemEnum.Error.BillingAccountItemInvalidAmount)
                .Build());
            }
        }

        private void IsClosedOrBlockedAccount(Guid billingAccountId)
        {
            if (_billingAccountReadRepository.IsClosedOrBlockedAccount(billingAccountId).Result)
            {
                Notification.Raise(Notification.DefaultBuilder
                .WithMessage(AppConsts.LocalizationSourceName, BillingAccountEnum.Error.BillingAccountWithBillingAccountItemsCanNotBeClosedOrBlocked)
                .WithDetailedMessage(AppConsts.LocalizationSourceName, BillingAccountEnum.Error.BillingAccountWithBillingAccountItemsCanNotBeClosedOrBlocked)
                .Build());
            }
        }

        private async Task MapAndCreateDebit(BillingAccountItemDebitDto dto)
        {
            dto.DateParameter = await GetSystemDate(dto.PropertyId);

            await CalculateDebitAmountBasedOnExchangeRate(dto);

            if (!dto.CurrencyId.HasValue)
                dto.CurrencyId = await GetDefaultCurrencyIdByProperty(dto.PropertyId);

            var billingAccountItemEntity = _billingAccountItemAdapter.MapDebit(dto).Build();

            dto.Id = billingAccountItemEntity.Id;

            await _billingAccountItemRepository.InsertAsync(billingAccountItemEntity);

            if (dto.Id != Guid.Empty)
                CreateBillingAccountItemDetail(dto.DetailList, dto.Id);
        }

        private void CreateBillingAccountItemDetail(List<DetailIntegration> detailList, Guid billingAccountItemId)
        {
            var billingAccountItemDetailList = new List<BillingAccountItemDetail>();

            foreach (var detail in detailList)
            {
                // TODO: Refatorar
                // O campo ExternalId é utilizado para Produtos e serviços por isso pode ser um Guid e um inteiro
                // Criar dois campos separados para identificar
                try
                {
                    var productId = Guid.Parse(detail.ExternalId);

                    var dto = new BillingAccountItemDetailDto()
                    {
                        Amount = detail.Amount,
                        Quantity = detail.Quantity,
                        ProductId = productId,
                        BillingAccountItemId = billingAccountItemId
                    };

                    ValidateDto(dto);

                    if (Notification.HasNotification())
                        return;

                    billingAccountItemDetailList.Add(_billingAccountItemDetailAdapter.Map(dto).Build());
                }
                catch
                {

                }
            }

            _billingAccountItemDetailRepository.AddRange(billingAccountItemDetailList);
        }

        private async Task CalculateDebitAmountBasedOnExchangeRate(BillingAccountItemDebitDto dto)
        {
            if (dto.CurrencyExchangeReferenceId.HasValue && dto.CurrencyExchangeReferenceSecId.HasValue)
            {
                var firstExchangeRate = (await GetExchangeRateById(dto.CurrencyExchangeReferenceId.Value)).ExchangeRate;
                var secondExchangeRate = (await GetExchangeRateById(dto.CurrencyExchangeReferenceSecId.Value)).ExchangeRate;

                dto.CalculateAmountByExchangeRate(firstExchangeRate, secondExchangeRate);

                if (dto.DetailList != null && dto.DetailList.Count > 0)
                    foreach (var detail in dto.DetailList)
                        detail.CalculateAmountByExchangeRate(firstExchangeRate, secondExchangeRate);
            }
        }

        private async Task<DateTime> GetSystemDate(int propertyId)
        {
            return await _propertyParameterReadRepository.GetSystemDate(propertyId);
        }

        private async Task<ExchangeRateDto> GetExchangeRateById(Guid id)
            => await _propertyCurrencyExchangeReadRepository.GetExchangeRateById(id);

        private async Task<Guid> GetDefaultCurrencyIdByProperty(int propertyId)
        {
            var propertyCurrency = await _propertyParameterReadRepository.GetByPropertyIdAndApplicationParameterId(propertyId, (int)ApplicationParameterEnum.ParameterDefaultCurrency);
            return Guid.Parse(propertyCurrency.PropertyParameterValue.Trim());
        }

        private bool AlllowsCreatRatePlanTax(RatePlan ratePlan)
        {
            return ratePlan != null && !ratePlan.RateNet;
        }

        private async Task CreateTaxBasedOnDebit(BillingAccountItemDebitDto dto)
        {
            var billingAccountItemList = new List<BillingAccountItem>();
            var ratePlanOfReservationItem = _ratePlanReadRepository.GetByBillingAccountId(dto.BillingAccountId).Result;

            if (ratePlanOfReservationItem == null || AlllowsCreatRatePlanTax(ratePlanOfReservationItem))
            {
                var taxes = _billingItemReadRepository.GetTaxesForServices(new List<int> { dto.BillingItemId });

                foreach (var tax in taxes)
                {
                    var taxDto = CreateTaxBasedOnDebitDto(dto, tax);
                    var builder = _billingAccountItemAdapter.MapDebitTax(taxDto, dto.Id);
                    billingAccountItemList.Add(builder.Build());
                }
            }

            if (Notification.HasNotification())
                return;

            if (billingAccountItemList.Count > 0)
                await _billingAccountItemRepository.AddRange(billingAccountItemList);
        }

        private decimal CalculateTax(decimal amount, decimal taxPercentage)
        {
            return Math.Round((amount * (taxPercentage / 100)), 2);
        }

        private BillingAccountItemDebitDto CreateTaxBasedOnDebitDto(BillingAccountItemDebitDto dto, BillingItemServiceAndTaxAssociateDto tax)
        {
            return new BillingAccountItemDebitDto
            {
                BillingAccountId = dto.BillingAccountId,
                BillingItemId = tax.BillingItemTaxId.Value,
                Amount = CalculateTax(dto.Amount, tax.TaxPercentage),
                PropertyId = dto.PropertyId,
                CurrencyId = dto.CurrencyId,
                CurrencyExchangeReferenceId = dto.CurrencyExchangeReferenceId,
                CurrencyExchangeReferenceSecId = dto.CurrencyExchangeReferenceSecId,
                DateParameter = dto.DateParameter
            };
        }

        public async Task<BillingAccountItemDebitDto> CreateDebit(BillingAccountItemDebitDto dto)
        {
            using (var uow = _unitOfWorkManager.Begin())
            {
                ValidateDto<BillingAccountItemDebitDto>(dto, nameof(dto));

                if (Notification.HasNotification()) return null;

                ValidateDebit(dto);

                if (Notification.HasNotification()) return null;

                await MapAndCreateDebit(dto);

                if (Notification.HasNotification()) return null;

                await CreateTaxBasedOnDebit(dto);

                if (Notification.HasNotification()) return null;

                CreateMostSelectedDebitAsync(dto);

                uow.Complete();
            }

            return dto;
        }

        private void CreateMostSelectedDebitAsync(BillingAccountItemDebitDto dto)
        {
            if (dto.BillingItemId > 0 && dto.DetailList != null && dto.DetailList.Count > 0)
            {
                System.Threading.Tasks.Task.Run(() =>
                {
                    try
                    {
                        var _token = _httpContextAccessor.HttpContext.Request.Headers.FirstOrDefault(x => x.Key.Equals("Authorization")).Value.ToString();
                        var _complete = false;
                        var _body = new Preference.Dto.MostSelectedDto()
                        {
                            UserId = _applicationUser.UserUid,
                            Type = (int)MostSelectedTypeEnum.MostSelectedProduct
                        };

                    dto.DetailList.ForEach(item =>
                    {
                        _body.Details = new List<Preference.Dto.MostSelectedDetailsDto>()
                        {
                            new Preference.Dto.MostSelectedDetailsDto()
                            {
                                BillingItemId = dto.BillingItemId.ToString(),
                                Description = item.Description,
                                ExternalId = item.ExternalId,
                                Quantity = item.Quantity,
                                GroupDescription = item.GroupDescription,
                                Code = item.Code,
                                NcmCode = item.NcmCode,
                                BarCode = item.BarCode
                            }
                        };
                    });

                        HttpClientHelper.SendRequest<bool>(string.Concat(_servicesConfiguration.PreferenceAPI, _urlMostSelected),
                                                           string.Empty, JsonConvert.SerializeObject(_body), ref _complete, _token);
                    }
                    catch
                    {
                    }
                });
            }
        }
    }
}
