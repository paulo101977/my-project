﻿using System.Diagnostics.CodeAnalysis;
using System.Threading.Tasks;
using Thex.Kernel;
using Thex.PDV.Application.Adapters;
using Thex.PDV.Application.Interfaces;
using Thex.PDV.Domain.Entities;
using Thex.PDV.Dto;
using Thex.PDV.Infra.Interfaces.ReadRepositories;
using Tnf.Dto;
using Tnf.Notifications;

namespace Thex.PDV.Application.Services
{
    public class BillingItemAppService : ApplicationServiceBase<BillingItemDto, BillingItem>, IBillingItemAppService
    {
        private readonly IApplicationUser _applicationUser;
        private readonly IBillingItemReadRepository _billingItemReadRepository;
        private readonly IBillingItemAdapter _billingItemAdapter;

        public BillingItemAppService(IBillingItemReadRepository billingItemReadRepository,
          IApplicationUser applicationUser,
          INotificationHandler notificationHandler,
          IBillingItemAdapter billingItemAdapter)
          : base(notificationHandler, null)
        {
            _applicationUser = applicationUser;
            _billingItemReadRepository = billingItemReadRepository;
            _billingItemAdapter = billingItemAdapter;
        }

        public override BillingItem GeneratedEntity(BillingItemDto dto)
        {
            return _billingItemAdapter.Map(dto).Build();
        }

        public async Task<IListDto<BillingItemDto>> GetAll(GetAllBillingItemDto requestDto)
        {
            ValidateRequestAllDto(requestDto, nameof(requestDto));

            if (Notification.HasNotification())
                return null;

            return await _billingItemReadRepository.GetAll(requestDto);
        }
    }
}
