﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Threading.Tasks;
using Thex.Common;
using Thex.Common.Enumerations;
using Thex.PDV.Domain.Entities;
using Thex.PDV.Infra.Interfaces;
using Tnf.Application.Services;
using Tnf.Dto;
using Tnf.Notifications;

namespace Thex.PDV.Application.Services
{
    public abstract class ApplicationServiceBase<T, U> : ApplicationService where T : IDto where U : class
    {
        private IBaseRepository _baseRepository;

        protected ApplicationServiceBase(INotificationHandler notification,
                                         IBaseRepository baseRepository)
            : base(notification)
        {
            _baseRepository = baseRepository;
        }

        public virtual async Task<T> CreateObjAsync(T dto)
        {
            if (!ValidateDto<T>(dto))
                return default(T);

            var entity = GeneratedEntity(dto);

            var createdDto = await _baseRepository.InsertAsync(entity as BaseEntity);

            if (Notification.HasNotification())
                return default(T);

            return entity.MapTo<T>();
        }

        public async Task<T> EditObjAsync(Guid id, T dto)
        {
            if (!ValidateDto<T>(dto))
                return default(T);

            var entity = GeneratedEntity(dto);

            var editedDto = await _baseRepository.UpdateAsync(id, entity as BaseEntity);

            if (Notification.HasNotification())
                return default(T);

            return entity.MapTo<T>();
        }

        public async Task DeleteObjAsync(Guid id)
        {
            if (!ValidateId(id))
                return;

            await _baseRepository.RemoveAsync(id);

            if (Notification.HasNotification())
                return;
        }

        public async Task ToggleActivationObjAsync(Guid id)
        {
            if (!ValidateId(id))
                return;

            await _baseRepository.ToggleAsync(id);

            if (Notification.HasNotification())
                return;
        }

        [ExcludeFromCodeCoverage]
        protected virtual void NotifyNullParameter()
        {
            Notification.Raise(Notification
                .DefaultBuilder
                .WithMessage(AppConsts.LocalizationSourceName, CommonsEnum.Error.NullOrEmptyObject)
                .Build());
        }

        [ExcludeFromCodeCoverage]
        protected virtual void NotifyIdIsMissing()
        {
            Notification.Raise(Notification
                .DefaultBuilder
                .WithMessage(AppConsts.LocalizationSourceName, CommonsEnum.Error.IdIsMissing)
                .Build());
        }

        [ExcludeFromCodeCoverage]
        protected virtual void NotifyParameterInvalid()
        {
            Notification.Raise(Notification
                .DefaultBuilder
                .WithMessage(AppConsts.LocalizationSourceName, CommonsEnum.Error.ParameterInvalid)
                .Build());
        }

        [ExcludeFromCodeCoverage]
        protected virtual void NotifyWhenEntityNotExist(string entityName)
        {
            Notification.Raise(Notification
                .DefaultBuilder
                .WithMessage(AppConsts.LocalizationSourceName, CommonsEnum.Error.EntityNotExist)
                .WithMessageFormat(entityName)
                .Build());
        }

        [ExcludeFromCodeCoverage]
        protected virtual void NotifyInvalidRangeDate()
        {
            Notification.Raise(Notification
                .DefaultBuilder
                .WithMessage(AppConsts.LocalizationSourceName, CommonsEnum.Error.DateGreaterThanAnotherDate)
                .Build());
        }

        [ExcludeFromCodeCoverage]
        protected virtual void NotifyMinimumCharacters(int quantity)
        {
            Notification.Raise(Notification
                .DefaultBuilder
                .WithMessage(AppConsts.LocalizationSourceName, CommonsEnum.Error.FieldMinimumOfCharacteres)
                .WithMessageFormat(quantity)
                .Build());
        }

        [ExcludeFromCodeCoverage]
        protected virtual void NotifyRequired(string field)
        {
            Notification.Raise(Notification
                .DefaultBuilder
                .WithMessage(AppConsts.LocalizationSourceName, CommonsEnum.Error.FieldRequired)
                .WithMessageFormat(field)
                .Build());
        }

        [ExcludeFromCodeCoverage]
        protected virtual void NotifyInvalidOrderBy()
        {
            Notification.Raise(Notification
                .DefaultBuilder
                .WithMessage(AppConsts.LocalizationSourceName, CommonsEnum.Error.InvalidOrderBy)
                .Build());
        }

        [ExcludeFromCodeCoverage]
        protected virtual void NotifyEmptyFilter()
        {
            Notification.Raise(Notification
                .DefaultBuilder
                .WithMessage(AppConsts.LocalizationSourceName, CommonsEnum.Error.EmptyFilter)
                .Build());
        }

        public abstract U GeneratedEntity(T dto);
    }
}
