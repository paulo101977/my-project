﻿using Microsoft.Extensions.DependencyInjection;
using NSubstitute;
using Shouldly;
using System;
using Thex.Kernel;
using Thex.PDV.Application.Adapters;
using Thex.PDV.Application.Interfaces;
using Thex.PDV.Application.Services;
using Thex.PDV.Dto;
using Thex.PDV.Infra.Interfaces.ReadRepositories;
using Thex.PDV.Web.Tests.Mocks.Dto;
using Tnf.AspNetCore.TestBase;
using Tnf.Notifications;
using Xunit;

namespace Thex.PDV.Web.Tests.Application
{
    public class NcmAppServiceTests : TnfAspNetCoreIntegratedTestBase<StartupUnitTest>
    {
        public INotificationHandler _notificationHandler;

        public NcmAppServiceTests()
        {
            _notificationHandler = new NotificationHandler(ServiceProvider);
        }

        [Fact]
        public void Should_Resolve_All()
        {
            TnfSession.ShouldNotBeNull();
            ServiceProvider.GetService<INotificationHandler>().ShouldNotBeNull();
            ServiceProvider.GetService<INcmAppService>().ShouldNotBeNull();
            ServiceProvider.GetService<INcmReadRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<INcmAdapter>().ShouldNotBeNull();          
        }

        [Fact]
        public void Should_Generate_Entity_With_Map()
        {
            var entity = new NcmAppService(ncmReadRepository: null,
                                        applicationUser: ServiceProvider.GetService<IApplicationUser>(),
                                        notificationHandler: _notificationHandler,
                                        ncmAdapter: ServiceProvider.GetService<INcmAdapter>())
                    .GeneratedEntity(NcmDtoMock.GetDto());

            Assert.False(_notificationHandler.HasNotification());
            Assert.Equal(Guid.Parse("50a66538-dd94-4162-a1ed-83fc8311e574"), entity.Id);
            Assert.Equal("000111", entity.Code);
            Assert.Equal("Bebidas aleatórias", entity.Description);
        }

        [Fact]
        public void Should_GetAll()
        {
            var ncmReadRepository = Substitute.For<INcmReadRepository>();

            ncmReadRepository.GetAll(new GetAllNcmDto()).ReturnsForAnyArgs(x =>
            {
                return NcmDtoMock.GetListDtoAsync();
            });

            var billingList = new NcmAppService(ncmReadRepository: ncmReadRepository,
                                                applicationUser: ServiceProvider.GetService<IApplicationUser>(),
                                                notificationHandler: _notificationHandler,
                                                ncmAdapter: ServiceProvider.GetService<INcmAdapter>())
                    .GetAll(new GetAllNcmDto()).Result;

            Assert.False(_notificationHandler.HasNotification());
            Assert.Equal(3, billingList.Items.Count);
        }
    }
}
