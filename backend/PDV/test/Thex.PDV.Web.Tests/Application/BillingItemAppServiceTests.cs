﻿using Microsoft.Extensions.DependencyInjection;
using NSubstitute;
using Shouldly;
using Thex.Kernel;
using Thex.PDV.Application.Adapters;
using Thex.PDV.Application.Interfaces;
using Thex.PDV.Application.Services;
using Thex.PDV.Dto;
using Thex.PDV.Infra.Interfaces.ReadRepositories;
using Thex.PDV.Web.Tests.Mocks.Dto;
using Tnf.AspNetCore.TestBase;
using Tnf.Notifications;
using Xunit;

namespace Thex.PDV.Web.Tests.Application
{
    public class BillingItemAppServiceTests : TnfAspNetCoreIntegratedTestBase<StartupUnitTest>
    {
        public INotificationHandler _notificationHandler;

        public BillingItemAppServiceTests()
        {
            _notificationHandler = new NotificationHandler(ServiceProvider);
        }

        [Fact]
        public void Should_Resolve_All()
        {
            TnfSession.ShouldNotBeNull();
            ServiceProvider.GetService<INotificationHandler>().ShouldNotBeNull();
            ServiceProvider.GetService<IBillingItemAppService>().ShouldNotBeNull();
            ServiceProvider.GetService<IBillingItemReadRepository >().ShouldNotBeNull();
            ServiceProvider.GetService<IBillingItemAdapter>().ShouldNotBeNull();
            ServiceProvider.GetService<IApplicationUser>().ShouldNotBeNull();
        }

        [Fact]
        public void Should_Generate_Entity_With_Map()
        {
            var entity = new BillingItemAppService(billingItemReadRepository: null,
                                                applicationUser: ServiceProvider.GetService<IApplicationUser>(),
                                                notificationHandler: _notificationHandler,
                                                billingItemAdapter: ServiceProvider.GetService<IBillingItemAdapter>())
                    .GeneratedEntity(BillingItemDtoMock.GetDto());

            Assert.False(_notificationHandler.HasNotification());
            Assert.Equal(8, entity.Id);
            Assert.Equal("PDV", entity.BillingItemName);
            Assert.Equal(4, entity.BillingItemTypeId);
            Assert.Equal(11, entity.PropertyId);
            Assert.True(entity.IsActive);
            Assert.False(entity.IsDeleted);
        }

        [Fact]
        public void Should_GetAll()
        {
            var billingItemReadRepository = Substitute.For<IBillingItemReadRepository>();

            billingItemReadRepository.GetAll(new GetAllBillingItemDto()).ReturnsForAnyArgs(x =>
            {
                return BillingItemDtoMock.GetListDtoAsync();
            });

            var billingList = new BillingItemAppService(billingItemReadRepository: billingItemReadRepository,
                                                     applicationUser: ServiceProvider.GetService<IApplicationUser>(),
                                                     notificationHandler: _notificationHandler,
                                                     billingItemAdapter: ServiceProvider.GetService<IBillingItemAdapter>())
                    .GetAll(new GetAllBillingItemDto()).Result;

            Assert.False(_notificationHandler.HasNotification());
            Assert.Equal(3, billingList.Items.Count);
        }
    }
}
