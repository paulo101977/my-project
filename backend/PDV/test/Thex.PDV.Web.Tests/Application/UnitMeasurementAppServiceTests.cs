﻿using Microsoft.Extensions.DependencyInjection;
using NSubstitute;
using Shouldly;
using System;
using Thex.Kernel;
using Thex.PDV.Application.Adapters;
using Thex.PDV.Application.Services;
using Thex.PDV.Dto;
using Thex.PDV.Infra.Interfaces.ReadRepositories;
using Thex.PDV.Web.Tests.Mocks.Dto;
using Tnf.AspNetCore.TestBase;
using Tnf.Localization;
using Tnf.Notifications;
using Xunit;

namespace Thex.PDV.Web.Tests.Application
{
    public class UnitMeasurementAppServiceTests : TnfAspNetCoreIntegratedTestBase<StartupUnitTest>
    {
        public INotificationHandler _notificationHandler;

        public UnitMeasurementAppServiceTests()
        {
            _notificationHandler = new NotificationHandler(ServiceProvider);
        }

        [Fact]
        public void Should_Resolve_All()
        {
            TnfSession.ShouldNotBeNull();
            ServiceProvider.GetService<INotificationHandler>().ShouldNotBeNull();
            ServiceProvider.GetService<IUnitMeasurementReadRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<IApplicationUser>().ShouldNotBeNull();
            ServiceProvider.GetService<IUnitMeasurementAdapter>().ShouldNotBeNull();
            ServiceProvider.GetService<ILocalizationManager>().ShouldNotBeNull();
        }

        [Fact]
        public void Should_Generate_Entity_With_Map()
        {
            var entity = new UnitMeasurementAppService(unitMeasurementReadRepository: null,
                                                    localizationManager: ServiceProvider.GetService<ILocalizationManager>(),
                                                    applicationUser: ServiceProvider.GetService<IApplicationUser>(),
                                                    notificationHandler: _notificationHandler,
                                                    unitMeasurementAdapter: ServiceProvider.GetService<IUnitMeasurementAdapter>())
                    .GeneratedEntity(UnitMeasurementDtoMock.GetDto());

            Assert.False(_notificationHandler.HasNotification());
            Assert.Equal(Guid.Parse("5fe72924-8bd8-4a7c-9df6-c9ebf8cd33b0"), entity.Id);
            Assert.Equal("Litro", entity.Name);
        }

        [Fact]
        public void Should_GetAll()
        {
            var unitMeasurementReadRepository = Substitute.For<IUnitMeasurementReadRepository>();

            unitMeasurementReadRepository.GetAll(new GetAllUnitMeasurementDto()).ReturnsForAnyArgs(x =>
            {
                return UnitMeasurementDtoMock.GetListDtoAsync();
            });

            var unitList = new UnitMeasurementAppService(unitMeasurementReadRepository: unitMeasurementReadRepository,
                                                    localizationManager: ServiceProvider.GetService<ILocalizationManager>(),
                                                    applicationUser: ServiceProvider.GetService<IApplicationUser>(),
                                                    notificationHandler: _notificationHandler,
                                                    unitMeasurementAdapter: ServiceProvider.GetService<IUnitMeasurementAdapter>())
                    .GetAll(new GetAllUnitMeasurementDto()).Result;

            Assert.False(_notificationHandler.HasNotification());
            Assert.Equal(3, unitList.Items.Count);
        }
    }
}
