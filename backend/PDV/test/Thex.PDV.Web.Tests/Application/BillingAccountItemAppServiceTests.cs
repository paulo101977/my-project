﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using NSubstitute;
using Shouldly;
using System;
using Thex.Common;
using Thex.Kernel;
using Thex.PDV.Application.Adapters;
using Thex.PDV.Application.Services;
using Thex.PDV.Dto.Dto.BillingAccountItemDebit;
using Thex.PDV.Infra.Interfaces;
using Thex.PDV.Infra.Interfaces.ReadRepositories;
using Thex.PDV.Web.Tests.Mocks;
using Thex.PDV.Web.Tests.Mocks.Dto;
using Thex.PDV.Web.Tests.Mocks.Entity;
using Tnf.AspNetCore.TestBase;
using Tnf.Notifications;
using Tnf.Repositories.Uow;
using Xunit;

namespace Thex.PDV.Web.Tests.Application
{
    public class BillingAccountItemAppServiceTests : TnfAspNetCoreIntegratedTestBase<StartupUnitTest>
    {
        public INotificationHandler _notificationHandler;
        public IBillingAccountItemAdapter _billingAccountItemAdapter;

        public BillingAccountItemAppServiceTests()
        {
            _notificationHandler = new NotificationHandler(ServiceProvider);
            _billingAccountItemAdapter = new BillingAccountItemAdapter(_notificationHandler);
        }

        [Fact]
        public void Should_Resolve_All()
        {
            TnfSession.ShouldNotBeNull();
            ServiceProvider.GetService<IRatePlanReadRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<IBillingAccountItemRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<INotificationHandler>().ShouldNotBeNull();
            ServiceProvider.GetService<IBillingItemReadRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<IBillingAccountReadRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<IPropertyCurrencyExchangeReadRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<IPropertyParameterReadRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<IApplicationUser>().ShouldNotBeNull();
            ServiceProvider.GetService<IBillingAccountItemAdapter>().ShouldNotBeNull();
            ServiceProvider.GetService<IUnitOfWorkManager>().ShouldNotBeNull();
            ServiceProvider.GetService<IHttpContextAccessor>().ShouldNotBeNull();
        }

        [Fact]
        public void Should_Generate_Entity_With_Map()
        {
            var entity = new BillingAccountItemAppService(ratePlanReadRepository: ServiceProvider.GetService<IRatePlanReadRepository>(),
                                                billingAccountItemRepository: ServiceProvider.GetService<IBillingAccountItemRepository>(),
                                                notificationHandler: _notificationHandler,
                                                billingItemReadRepository: ServiceProvider.GetService<IBillingItemReadRepository>(),
                                                billingAccountReadRepository: ServiceProvider.GetService<IBillingAccountReadRepository>(),
                                                propertyCurrencyExchangeReadRepository: ServiceProvider.GetService<IPropertyCurrencyExchangeReadRepository>(),
                                                propertyParameterReadRepository: ServiceProvider.GetService<IPropertyParameterReadRepository>(),
                                                applicationUser: ServiceProvider.GetService<IApplicationUser>(),
                                                billingAccountItemAdapter: ServiceProvider.GetService<IBillingAccountItemAdapter>(),
                                                unitOfWorkManager: ServiceProvider.GetService<IUnitOfWorkManager>(),
                                                serviceConfiguration: ServiceProvider.GetService<ServicesConfiguration>(),
                                                httpContextAccessor: ServiceProvider.GetService<IHttpContextAccessor>(),
                                                billingAccountItemDetailAdapter: ServiceProvider.GetService<IBillingAccountItemDetailAdapter>(),
                                                billingAccountItemDetailRepository: ServiceProvider.GetService<IBillingAccountItemDetailRepository>())
                  .GeneratedEntity(BillingAccountItemDebitDtoMock.GetDto());

            Assert.False(_notificationHandler.HasNotification());
            Assert.Equal(-100, entity.Amount);
            Assert.Equal(Guid.Parse("CBA11DEA-0582-4EA0-A01E-07EDAA6641BE"), entity.BillingAccountId);
            Assert.Equal(1, entity.BillingItemId);
            Assert.Equal(Guid.Parse("67838ACB-9525-4AEB-B0A6-127C1B986C48"), entity.CurrencyId);
            Assert.Equal(Guid.Parse("EF085323-3F4A-4B53-8B6E-01904BADDAEF"), entity.CurrencyExchangeReferenceId);
            Assert.Equal(new DateTime(2018, 09, 20), entity.DateParameter);
            Assert.NotEqual(string.Empty, entity.IntegrationDetail);
        }

        [Fact]
        public void Should_Create_Debit()
        {
            #region Intercepted repos

            // Intercepta a interface
            var billingItemReadRepository = Substitute.For<IBillingItemReadRepository>();
            var propertyCurrencyExchangeReadRepository = Substitute.For<IPropertyCurrencyExchangeReadRepository>();
            var propertyParameterReadRepository = Substitute.For<IPropertyParameterReadRepository>();
            var billingAccountItemRepository = Substitute.For<IBillingAccountItemRepository>();
            var ratePlanReadRepository = Substitute.For<IRatePlanReadRepository>();
            var billingAccountReadRepository = Substitute.For<IBillingAccountReadRepository>();

            // Retorna um valor para a função interceptada
            billingItemReadRepository.AnyBillingItemByBillingItemType(1, 4).ReturnsForAnyArgs(x =>
            {
                return true;
            });

            billingItemReadRepository.IsServiceAndExistSupportedType(1).ReturnsForAnyArgs(x =>
            {
                return true;
            });

            billingAccountReadRepository.IsClosedOrBlockedAccount(Guid.NewGuid()).ReturnsForAnyArgs(x =>
            {
                return false;
            });

            propertyCurrencyExchangeReadRepository.GetCurrencyIdExchangeByPropertyId(1).ReturnsForAnyArgs(x =>
            {
                return CurrencyForBillingAccountItemDtoMock.GetDto();
            });

            propertyParameterReadRepository.GetSystemDate(11).ReturnsForAnyArgs(x =>
            {
                return DateTime.Now;
            });

            billingAccountItemRepository.InsertAsync(BillingAccountItemEntityMock.GetEntity()).ReturnsForAnyArgs(x =>
            {
                return BillingAccountItemDebitDtoMock.GetDto();
            });

            ratePlanReadRepository.GetByBillingAccountId(Guid.NewGuid()).ReturnsForAnyArgs(x =>
            {
                return RatePlanEntityMock.GetEntity();
            });

            billingItemReadRepository.GetTaxesForServices(new[] { 1, 2, 3 }).ReturnsForAnyArgs(x =>
            {
                return BillingItemServiceAndTaxAssociateDtoMock.GetListDto();
            });

            billingItemReadRepository.GetById(8).ReturnsForAnyArgs(x =>
            {
                return BillingItemEntityMock.GetEntity();
            });

            billingAccountItemRepository.AddRange(BillingAccountItemEntityMock.GetListEntity());

            #endregion

            var dto = new BillingAccountItemAppService(ratePlanReadRepository: ratePlanReadRepository,
                                                       billingAccountItemRepository: billingAccountItemRepository,
                                                       notificationHandler: _notificationHandler,
                                                       billingItemReadRepository: billingItemReadRepository,
                                                       billingAccountReadRepository: billingAccountReadRepository,
                                                       propertyCurrencyExchangeReadRepository: propertyCurrencyExchangeReadRepository,
                                                       propertyParameterReadRepository: propertyParameterReadRepository,
                                                       applicationUser: ServiceProvider.GetService<IApplicationUser>(),
                                                       billingAccountItemAdapter: ServiceProvider.GetService<IBillingAccountItemAdapter>(),
                                                       unitOfWorkManager: ServiceProvider.GetService<IUnitOfWorkManager>(),
                                                       serviceConfiguration: ServiceProvider.GetService<ServicesConfiguration>(),
                                                       httpContextAccessor: ServiceProvider.GetService<IHttpContextAccessor>(),
                                                       billingAccountItemDetailAdapter: ServiceProvider.GetService<IBillingAccountItemDetailAdapter>(),
                                                       billingAccountItemDetailRepository: ServiceProvider.GetService<IBillingAccountItemDetailRepository>())
                         .CreateDebit(BillingAccountItemDebitDtoMock.GetDto());

            Assert.False(_notificationHandler.HasNotification());
            Assert.True(dto != null && dto.Id > 0);
        }

        [Fact]
        public void Should_Create_Debit_With_Invalid_Dto()
        {
            #region Intercepted repos

            // Intercepta a interface
            var billingItemReadRepository = Substitute.For<IBillingItemReadRepository>();
            var propertyCurrencyExchangeReadRepository = Substitute.For<IPropertyCurrencyExchangeReadRepository>();
            var propertyParameterReadRepository = Substitute.For<IPropertyParameterReadRepository>();
            var billingAccountItemRepository = Substitute.For<IBillingAccountItemRepository>();
            var ratePlanReadRepository = Substitute.For<IRatePlanReadRepository>();
            var billingAccountReadRepository = Substitute.For<IBillingAccountReadRepository>();

            // Retorna um valor para a função interceptada
            billingItemReadRepository.AnyBillingItemByBillingItemType(1, 4).ReturnsForAnyArgs(x =>
            {
                return true;
            });

            billingItemReadRepository.IsServiceAndExistSupportedType(1).ReturnsForAnyArgs(x =>
            {
                return true;
            });

            billingAccountReadRepository.IsClosedOrBlockedAccount(Guid.NewGuid()).ReturnsForAnyArgs(x =>
            {
                return false;
            });

            propertyCurrencyExchangeReadRepository.GetCurrencyIdExchangeByPropertyId(1).ReturnsForAnyArgs(x =>
            {
                return CurrencyForBillingAccountItemDtoMock.GetDto();
            });

            propertyParameterReadRepository.GetSystemDate(11).ReturnsForAnyArgs(x =>
            {
                return DateTime.Now;
            });

            billingAccountItemRepository.InsertAsync(BillingAccountItemEntityMock.GetEntity()).ReturnsForAnyArgs(x =>
            {
                return BillingAccountItemDebitDtoMock.GetDto();
            });

            ratePlanReadRepository.GetByBillingAccountId(Guid.NewGuid()).ReturnsForAnyArgs(x =>
            {
                return RatePlanEntityMock.GetEntity();
            });

            billingItemReadRepository.GetTaxesForServices(new[] { 1, 2, 3 }).ReturnsForAnyArgs(x =>
            {
                return BillingItemServiceAndTaxAssociateDtoMock.GetListDto();
            });

            billingItemReadRepository.GetById(8).ReturnsForAnyArgs(x =>
            {
                return BillingItemEntityMock.GetEntity();
            });

            billingAccountItemRepository.AddRange(BillingAccountItemEntityMock.GetListEntity());

            #endregion

            var dto = new BillingAccountItemAppService(ratePlanReadRepository: ratePlanReadRepository,
                                                       billingAccountItemRepository: billingAccountItemRepository,
                                                       notificationHandler: _notificationHandler,
                                                       billingItemReadRepository: billingItemReadRepository,
                                                       billingAccountReadRepository: billingAccountReadRepository,
                                                       propertyCurrencyExchangeReadRepository: propertyCurrencyExchangeReadRepository,
                                                       propertyParameterReadRepository: propertyParameterReadRepository,
                                                       applicationUser: ServiceProvider.GetService<IApplicationUser>(),
                                                       billingAccountItemAdapter: ServiceProvider.GetService<IBillingAccountItemAdapter>(),
                                                       unitOfWorkManager: ServiceProvider.GetService<IUnitOfWorkManager>(),
                                                       serviceConfiguration: ServiceProvider.GetService<ServicesConfiguration>(),
                                                       httpContextAccessor: ServiceProvider.GetService<IHttpContextAccessor>(),
                                                       billingAccountItemDetailAdapter: ServiceProvider.GetService<IBillingAccountItemDetailAdapter>(),
                                                       billingAccountItemDetailRepository: ServiceProvider.GetService<IBillingAccountItemDetailRepository>())
                         .CreateDebit(BillingAccountItemDebitDto.NullInstance);

            Assert.True(_notificationHandler.HasNotification());
        }

        [Fact]
        public void Should_Create_Debit_With_Invalid_Amount()
        {
            #region Intercepted repos

            // Intercepta a interface
            var billingItemReadRepository = Substitute.For<IBillingItemReadRepository>();
            var propertyCurrencyExchangeReadRepository = Substitute.For<IPropertyCurrencyExchangeReadRepository>();
            var propertyParameterReadRepository = Substitute.For<IPropertyParameterReadRepository>();
            var billingAccountItemRepository = Substitute.For<IBillingAccountItemRepository>();
            var ratePlanReadRepository = Substitute.For<IRatePlanReadRepository>();
            var billingAccountReadRepository = Substitute.For<IBillingAccountReadRepository>();

            // Retorna um valor para a função interceptada
            billingItemReadRepository.AnyBillingItemByBillingItemType(1, 4).ReturnsForAnyArgs(x =>
            {
                return true;
            });

            billingItemReadRepository.IsServiceAndExistSupportedType(1).ReturnsForAnyArgs(x =>
            {
                return true;
            });

            billingAccountReadRepository.IsClosedOrBlockedAccount(Guid.NewGuid()).ReturnsForAnyArgs(x =>
            {
                return false;
            });

            propertyCurrencyExchangeReadRepository.GetCurrencyIdExchangeByPropertyId(1).ReturnsForAnyArgs(x =>
            {
                return CurrencyForBillingAccountItemDtoMock.GetDto();
            });

            propertyParameterReadRepository.GetSystemDate(11).ReturnsForAnyArgs(x =>
            {
                return DateTime.Now;
            });

            billingAccountItemRepository.InsertAsync(BillingAccountItemEntityMock.GetEntity()).ReturnsForAnyArgs(x =>
            {
                return BillingAccountItemDebitDtoMock.GetDto();
            });

            ratePlanReadRepository.GetByBillingAccountId(Guid.NewGuid()).ReturnsForAnyArgs(x =>
            {
                return RatePlanEntityMock.GetEntity();
            });

            billingItemReadRepository.GetTaxesForServices(new[] { 1, 2, 3 }).ReturnsForAnyArgs(x =>
            {
                return BillingItemServiceAndTaxAssociateDtoMock.GetListDto();
            });

            billingItemReadRepository.GetById(8).ReturnsForAnyArgs(x =>
            {
                return BillingItemEntityMock.GetEntity();
            });

            billingAccountItemRepository.AddRange(BillingAccountItemEntityMock.GetListEntity());

            #endregion

            var dto = new BillingAccountItemAppService(ratePlanReadRepository: ratePlanReadRepository,
                                                       billingAccountItemRepository: billingAccountItemRepository,
                                                       notificationHandler: _notificationHandler,
                                                       billingItemReadRepository: billingItemReadRepository,
                                                       billingAccountReadRepository: billingAccountReadRepository,
                                                       propertyCurrencyExchangeReadRepository: propertyCurrencyExchangeReadRepository,
                                                       propertyParameterReadRepository: propertyParameterReadRepository,
                                                       applicationUser: ServiceProvider.GetService<IApplicationUser>(),
                                                       billingAccountItemAdapter: ServiceProvider.GetService<IBillingAccountItemAdapter>(),
                                                       unitOfWorkManager: ServiceProvider.GetService<IUnitOfWorkManager>(),
                                                       serviceConfiguration: ServiceProvider.GetService<ServicesConfiguration>(),
                                                       httpContextAccessor: ServiceProvider.GetService<IHttpContextAccessor>(),
                                                       billingAccountItemDetailAdapter: ServiceProvider.GetService<IBillingAccountItemDetailAdapter>(),
                                                       billingAccountItemDetailRepository: ServiceProvider.GetService<IBillingAccountItemDetailRepository>())
                         .CreateDebit(BillingAccountItemDebitDtoMock.GetDto(amount: -1));

            Assert.True(_notificationHandler.HasNotification());
        }

        [Fact]
        public void Should_Create_Debit_With_Closed_Account()
        {
            #region Intercepted repos

            // Intercepta a interface
            var billingItemReadRepository = Substitute.For<IBillingItemReadRepository>();
            var propertyCurrencyExchangeReadRepository = Substitute.For<IPropertyCurrencyExchangeReadRepository>();
            var propertyParameterReadRepository = Substitute.For<IPropertyParameterReadRepository>();
            var billingAccountItemRepository = Substitute.For<IBillingAccountItemRepository>();
            var ratePlanReadRepository = Substitute.For<IRatePlanReadRepository>();
            var billingAccountReadRepository = Substitute.For<IBillingAccountReadRepository>();

            // Retorna um valor para a função interceptada
            billingItemReadRepository.AnyBillingItemByBillingItemType(1, 4).ReturnsForAnyArgs(x =>
            {
                return false;
            });

            billingItemReadRepository.IsServiceAndExistSupportedType(1).ReturnsForAnyArgs(x =>
            {
                return true;
            });

            billingAccountReadRepository.IsClosedOrBlockedAccount(Guid.NewGuid()).ReturnsForAnyArgs(x =>
            {
                return true;
            });

            propertyCurrencyExchangeReadRepository.GetCurrencyIdExchangeByPropertyId(1).ReturnsForAnyArgs(x =>
            {
                return CurrencyForBillingAccountItemDtoMock.GetDto();
            });

            propertyParameterReadRepository.GetSystemDate(11).ReturnsForAnyArgs(x =>
            {
                return DateTime.Now;
            });

            billingAccountItemRepository.InsertAsync(BillingAccountItemEntityMock.GetEntity()).ReturnsForAnyArgs(x =>
            {
                return BillingAccountItemDebitDtoMock.GetDto();
            });

            ratePlanReadRepository.GetByBillingAccountId(Guid.NewGuid()).ReturnsForAnyArgs(x =>
            {
                return RatePlanEntityMock.GetEntity();
            });

            billingItemReadRepository.GetTaxesForServices(new[] { 1, 2, 3 }).ReturnsForAnyArgs(x =>
            {
                return BillingItemServiceAndTaxAssociateDtoMock.GetListDto();
            });

            billingItemReadRepository.GetById(8).ReturnsForAnyArgs(x =>
            {
                return BillingItemEntityMock.GetEntity();
            });

            billingAccountItemRepository.AddRange(BillingAccountItemEntityMock.GetListEntity());

            #endregion

            var dto = new BillingAccountItemAppService(ratePlanReadRepository: ratePlanReadRepository,
                                                       billingAccountItemRepository: billingAccountItemRepository,
                                                       notificationHandler: _notificationHandler,
                                                       billingItemReadRepository: billingItemReadRepository,
                                                       billingAccountReadRepository: billingAccountReadRepository,
                                                       propertyCurrencyExchangeReadRepository: propertyCurrencyExchangeReadRepository,
                                                       propertyParameterReadRepository: propertyParameterReadRepository,
                                                       applicationUser: ServiceProvider.GetService<IApplicationUser>(),
                                                       billingAccountItemAdapter: ServiceProvider.GetService<IBillingAccountItemAdapter>(),
                                                       unitOfWorkManager: ServiceProvider.GetService<IUnitOfWorkManager>(),
                                                       serviceConfiguration: ServiceProvider.GetService<ServicesConfiguration>(),
                                                       httpContextAccessor: ServiceProvider.GetService<IHttpContextAccessor>(),
                                                       billingAccountItemDetailAdapter: ServiceProvider.GetService<IBillingAccountItemDetailAdapter>(),
                                                       billingAccountItemDetailRepository: ServiceProvider.GetService<IBillingAccountItemDetailRepository>())
                         .CreateDebit(BillingAccountItemDebitDtoMock.GetDto(amount: -1));

            Assert.True(_notificationHandler.HasNotification());
        }

        [Fact]
        public void Should_Create_Debit_With_Invalid_Debit()
        {
            #region Intercepted repos

            // Intercepta a interface
            var billingItemReadRepository = Substitute.For<IBillingItemReadRepository>();
            var propertyCurrencyExchangeReadRepository = Substitute.For<IPropertyCurrencyExchangeReadRepository>();
            var propertyParameterReadRepository = Substitute.For<IPropertyParameterReadRepository>();
            var billingAccountItemRepository = Substitute.For<IBillingAccountItemRepository>();
            var ratePlanReadRepository = Substitute.For<IRatePlanReadRepository>();
            var billingAccountReadRepository = Substitute.For<IBillingAccountReadRepository>();

            // Retorna um valor para a função interceptada
            billingItemReadRepository.AnyBillingItemByBillingItemType(1, 4).ReturnsForAnyArgs(x =>
            {
                return true;
            });

            billingItemReadRepository.IsServiceAndExistSupportedType(1).ReturnsForAnyArgs(x =>
            {
                return true;
            });

            billingAccountReadRepository.IsClosedOrBlockedAccount(Guid.NewGuid()).ReturnsForAnyArgs(x =>
            {
                return true;
            });

            propertyCurrencyExchangeReadRepository.GetCurrencyIdExchangeByPropertyId(1).ReturnsForAnyArgs(x =>
            {
                return CurrencyForBillingAccountItemDtoMock.GetDto();
            });

            propertyParameterReadRepository.GetSystemDate(11).ReturnsForAnyArgs(x =>
            {
                return DateTime.Now;
            });

            billingAccountItemRepository.InsertAsync(BillingAccountItemEntityMock.GetEntity()).ReturnsForAnyArgs(x =>
            {
                return BillingAccountItemDebitDtoMock.GetDto();
            });

            ratePlanReadRepository.GetByBillingAccountId(Guid.NewGuid()).ReturnsForAnyArgs(x =>
            {
                return RatePlanEntityMock.GetEntity();
            });

            billingItemReadRepository.GetTaxesForServices(new[] { 1, 2, 3 }).ReturnsForAnyArgs(x =>
            {
                return BillingItemServiceAndTaxAssociateDtoMock.GetListDto();
            });

            billingItemReadRepository.GetById(8).ReturnsForAnyArgs(x =>
            {
                return BillingItemEntityMock.GetEntity();
            });

            billingAccountItemRepository.AddRange(BillingAccountItemEntityMock.GetListEntity());

            #endregion

            var dto = new BillingAccountItemAppService(ratePlanReadRepository: ratePlanReadRepository,
                                                       billingAccountItemRepository: billingAccountItemRepository,
                                                       notificationHandler: _notificationHandler,
                                                       billingItemReadRepository: billingItemReadRepository,
                                                       billingAccountReadRepository: billingAccountReadRepository,
                                                       propertyCurrencyExchangeReadRepository: propertyCurrencyExchangeReadRepository,
                                                       propertyParameterReadRepository: propertyParameterReadRepository,
                                                       applicationUser: ServiceProvider.GetService<IApplicationUser>(),
                                                       billingAccountItemAdapter: ServiceProvider.GetService<IBillingAccountItemAdapter>(),
                                                       unitOfWorkManager: ServiceProvider.GetService<IUnitOfWorkManager>(),
                                                       serviceConfiguration: ServiceProvider.GetService<ServicesConfiguration>(),
                                                       httpContextAccessor: ServiceProvider.GetService<IHttpContextAccessor>(),
                                                       billingAccountItemDetailAdapter: ServiceProvider.GetService<IBillingAccountItemDetailAdapter>(),
                                                       billingAccountItemDetailRepository: ServiceProvider.GetService<IBillingAccountItemDetailRepository>())
                         .CreateDebit(BillingAccountItemDebitDtoMock.GetDto(amount: -1));

            Assert.True(_notificationHandler.HasNotification());
        }

        [Fact]
        public void Should_Create_Debit_With_Invalid_SupportedType()
        {
            #region Intercepted repos

            // Intercepta a interface
            var billingItemReadRepository = Substitute.For<IBillingItemReadRepository>();
            var propertyCurrencyExchangeReadRepository = Substitute.For<IPropertyCurrencyExchangeReadRepository>();
            var propertyParameterReadRepository = Substitute.For<IPropertyParameterReadRepository>();
            var billingAccountItemRepository = Substitute.For<IBillingAccountItemRepository>();
            var ratePlanReadRepository = Substitute.For<IRatePlanReadRepository>();
            var billingAccountReadRepository = Substitute.For<IBillingAccountReadRepository>();

            // Retorna um valor para a função interceptada
            billingItemReadRepository.AnyBillingItemByBillingItemType(1, 4).ReturnsForAnyArgs(x =>
            {
                return true;
            });

            billingItemReadRepository.IsServiceAndExistSupportedType(1).ReturnsForAnyArgs(x =>
            {
                return false;
            });

            billingAccountReadRepository.IsClosedOrBlockedAccount(Guid.NewGuid()).ReturnsForAnyArgs(x =>
            {
                return true;
            });

            propertyCurrencyExchangeReadRepository.GetCurrencyIdExchangeByPropertyId(1).ReturnsForAnyArgs(x =>
            {
                return CurrencyForBillingAccountItemDtoMock.GetDto();
            });

            propertyParameterReadRepository.GetSystemDate(11).ReturnsForAnyArgs(x =>
            {
                return DateTime.Now;
            });

            billingAccountItemRepository.InsertAsync(BillingAccountItemEntityMock.GetEntity()).ReturnsForAnyArgs(x =>
            {
                return BillingAccountItemDebitDtoMock.GetDto();
            });

            ratePlanReadRepository.GetByBillingAccountId(Guid.NewGuid()).ReturnsForAnyArgs(x =>
            {
                return RatePlanEntityMock.GetEntity();
            });

            billingItemReadRepository.GetTaxesForServices(new[] { 1, 2, 3 }).ReturnsForAnyArgs(x =>
            {
                return BillingItemServiceAndTaxAssociateDtoMock.GetListDto();
            });

            billingItemReadRepository.GetById(8).ReturnsForAnyArgs(x =>
            {
                return BillingItemEntityMock.GetEntity();
            });

            billingAccountItemRepository.AddRange(BillingAccountItemEntityMock.GetListEntity());

            #endregion

            var dto = new BillingAccountItemAppService(ratePlanReadRepository: ratePlanReadRepository,
                                                       billingAccountItemRepository: billingAccountItemRepository,
                                                       notificationHandler: _notificationHandler,
                                                       billingItemReadRepository: billingItemReadRepository,
                                                       billingAccountReadRepository: billingAccountReadRepository,
                                                       propertyCurrencyExchangeReadRepository: propertyCurrencyExchangeReadRepository,
                                                       propertyParameterReadRepository: propertyParameterReadRepository,
                                                       applicationUser: ServiceProvider.GetService<IApplicationUser>(),
                                                       billingAccountItemAdapter: ServiceProvider.GetService<IBillingAccountItemAdapter>(),
                                                       unitOfWorkManager: ServiceProvider.GetService<IUnitOfWorkManager>(),
                                                       serviceConfiguration: ServiceProvider.GetService<ServicesConfiguration>(),
                                                       httpContextAccessor: ServiceProvider.GetService<IHttpContextAccessor>(),
                                                       billingAccountItemDetailAdapter: ServiceProvider.GetService<IBillingAccountItemDetailAdapter>(),
                                                       billingAccountItemDetailRepository: ServiceProvider.GetService<IBillingAccountItemDetailRepository>())
                         .CreateDebit(BillingAccountItemDebitDtoMock.GetDto(amount: -1));

            Assert.True(_notificationHandler.HasNotification());
        }
    }
}
