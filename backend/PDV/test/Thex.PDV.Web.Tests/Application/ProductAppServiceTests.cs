﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using NSubstitute;
using Shouldly;
using System;
using System.Threading.Tasks;
using Thex.Common;
using Thex.PDV.Application.Adapters;
using Thex.PDV.Application.Interfaces;
using Thex.PDV.Application.Services;
using Thex.PDV.Domain.Entities;
using Thex.PDV.Dto;
using Thex.PDV.Dto.Product;
using Thex.PDV.Infra.Interfaces;
using Thex.PDV.Infra.Interfaces.ReadRepositories;
using Thex.PDV.Web.Tests.Mocks.Dto;
using Tnf.AspNetCore.TestBase;
using Tnf.Notifications;
using Tnf.Repositories.Uow;
using Xunit;

namespace Thex.PDV.Web.Tests.Application
{
    public class ProductAppServiceTests : TnfAspNetCoreIntegratedTestBase<StartupUnitTest>
    {
        public INotificationHandler _notificationHandler;

        public ProductAppServiceTests()
        {
            _notificationHandler = new NotificationHandler(ServiceProvider);
        }

        [Fact]
        public void Should_Resolve_All()
        {
            TnfSession.ShouldNotBeNull();
            ServiceProvider.GetService<INotificationHandler>().ShouldNotBeNull();
            ServiceProvider.GetService<IProductAppService>().ShouldNotBeNull();
            ServiceProvider.GetService<IProductAdapter>().ShouldNotBeNull();
            ServiceProvider.GetService<IUnitOfWorkManager>().ShouldNotBeNull();
            ServiceProvider.GetService<IProductReadRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<IProductRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<IProductBillingItemRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<IProductBillingItemReadRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<IProductBillingItemAdapter>().ShouldNotBeNull();
        }

        [Fact]
        public void Should_Generate_Entity_With_Map()
        {
            var productReadRepository = Substitute.For<IProductReadRepository>();
            var productRepository = Substitute.For<IProductRepository>();
            var productBillingItemRepository = Substitute.For<IProductBillingItemRepository>();
            var productBillingItemReadRepository = Substitute.For<IProductBillingItemReadRepository>();

            var entity = new ProductAppService(unitOfWorkManager: ServiceProvider.GetService<IUnitOfWorkManager>(),
                                            productAdapter: ServiceProvider.GetService<IProductAdapter>(),
                                            productBillingItemAdapter: ServiceProvider.GetService<IProductBillingItemAdapter>(),
                                            notificationHandler: _notificationHandler,
                                            productReadRepository: productReadRepository,
                                            productRepository: productRepository,
                                            productBillingItemRepository: productBillingItemRepository,
                                            productBillingItemReadRepository: productBillingItemReadRepository,
                                            billingItemReadRepository: ServiceProvider.GetService<IBillingItemReadRepository>(),
                                            serviceConfiguration: ServiceProvider.GetService<ServicesConfiguration>())
                    .GeneratedEntity(ProductDtoMock.GetDto());

            Assert.False(_notificationHandler.HasNotification());
            Assert.Equal(Guid.Parse("23eb803c-726a-4c7c-b25b-2c22a56793d9"), entity.Id);
            Assert.Equal("1111111111111", entity.BarCode);
            Assert.Equal("999999", entity.Code);
            Assert.True(entity.IsActive);
            Assert.False(entity.IsDeleted);
            Assert.Equal(Guid.Parse("2fb0c657-1e31-4eaf-a04b-e1bef85c5901"), entity.NcmId);
            Assert.Equal(Guid.Parse("07ae782c-2b84-4bef-81da-a96001358fdc"), entity.ProductCategoryId);
            Assert.Equal("Produto teste", entity.ProductName);
            Assert.Equal(Guid.Parse("8e6671c3-35bf-4e26-9a37-517256143566"), entity.UnitMeasurementId);
        }

        [Fact]
        public void Should_Create_Entity_Without_Associations()
        {
            var productReadRepository = Substitute.For<IProductReadRepository>();
            var productRepository = Substitute.For<IProductRepository>();
            var productBillingItemRepository = Substitute.For<IProductBillingItemRepository>();
            var productBillingItemReadRepository = Substitute.For<IProductBillingItemReadRepository>();

            productRepository.InsertAsync(new BaseEntity()).ReturnsForAnyArgs(x =>
            {
                return ProductDtoMock.GetDto();
            });

            var dto = new ProductAppService(unitOfWorkManager: ServiceProvider.GetService<IUnitOfWorkManager>(),
                                            productAdapter: ServiceProvider.GetService<IProductAdapter>(),
                                            productBillingItemAdapter: ServiceProvider.GetService<IProductBillingItemAdapter>(),
                                            notificationHandler: _notificationHandler,
                                            productReadRepository: productReadRepository,
                                            productRepository: productRepository,
                                            productBillingItemRepository: productBillingItemRepository,
                                            productBillingItemReadRepository: productBillingItemReadRepository,
                                            billingItemReadRepository: ServiceProvider.GetService<IBillingItemReadRepository>(),
                                            serviceConfiguration: ServiceProvider.GetService<ServicesConfiguration>())
                    .CreateObjAsync(ProductDtoMock.GetDto());

            Assert.False(_notificationHandler.HasNotification());
        }

        [Fact]
        public void Should_Create_Entity_With_Associations()
        {
            var productReadRepository = Substitute.For<IProductReadRepository>();
            var productRepository = Substitute.For<IProductRepository>();
            var productBillingItemRepository = Substitute.For<IProductBillingItemRepository>();
            var productBillingItemReadRepository = Substitute.For<IProductBillingItemReadRepository>();

            productRepository.InsertAsync(new BaseEntity()).ReturnsForAnyArgs(x =>
            {
                return ProductDtoMock.GetDto();
            });

            productBillingItemRepository.InsertAsync(new BaseEntity()).ReturnsForAnyArgs(x =>
            {
                return ProductDtoMock.GetDtoWithAssociations();
            });

            var dto = new ProductAppService(unitOfWorkManager: ServiceProvider.GetService<IUnitOfWorkManager>(),
                                            productAdapter: ServiceProvider.GetService<IProductAdapter>(),
                                            productBillingItemAdapter: ServiceProvider.GetService<IProductBillingItemAdapter>(),
                                            notificationHandler: _notificationHandler,
                                            productReadRepository: productReadRepository,
                                            productRepository: productRepository,
                                            productBillingItemRepository: productBillingItemRepository,
                                            productBillingItemReadRepository: productBillingItemReadRepository,
                                            billingItemReadRepository: ServiceProvider.GetService<IBillingItemReadRepository>(),
                                            serviceConfiguration: ServiceProvider.GetService<ServicesConfiguration>())
                    .CreateObjAsync(ProductDtoMock.GetDtoWithAssociations());

            Assert.False(_notificationHandler.HasNotification());
        }

        [Fact]
        public void Should_Edit_Entity_Without_Associations()
        {
            var productReadRepository = Substitute.For<IProductReadRepository>();
            var productRepository = Substitute.For<IProductRepository>();
            var productBillingItemRepository = Substitute.For<IProductBillingItemRepository>();
            var productBillingItemReadRepository = Substitute.For<IProductBillingItemReadRepository>();

            productRepository.InsertAsync(new BaseEntity()).ReturnsForAnyArgs(x =>
            {
                return ProductDtoMock.GetDto();
            });

            productReadRepository.GetProductById(Guid.NewGuid()).ReturnsForAnyArgs(x =>
            {
                return ProductDtoMock.GetDto();
            });

            productRepository.UpdateEntity(new Product()).ReturnsForAnyArgs(x =>
            {
                return ProductDtoMock.GetDto();
            });

            productBillingItemReadRepository.GetBillingItensForProduct(Guid.NewGuid()).ReturnsForAnyArgs(x =>
            {
                return ProductBillingItemDtoMock.GetListDto();
            });

            var dto = new ProductAppService(unitOfWorkManager: ServiceProvider.GetService<IUnitOfWorkManager>(),
                                            productAdapter: ServiceProvider.GetService<IProductAdapter>(),
                                            productBillingItemAdapter: ServiceProvider.GetService<IProductBillingItemAdapter>(),
                                            notificationHandler: _notificationHandler,
                                            productReadRepository: productReadRepository,
                                            productRepository: productRepository,
                                            productBillingItemRepository: productBillingItemRepository,
                                            productBillingItemReadRepository: productBillingItemReadRepository,
                                            billingItemReadRepository: ServiceProvider.GetService<IBillingItemReadRepository>(),
                                            serviceConfiguration: ServiceProvider.GetService<ServicesConfiguration>())
                    .EditObjAsync(Guid.Parse("23eb803c-726a-4c7c-b25b-2c22a56793d9"), ProductDtoMock.GetDto());


            Assert.False(_notificationHandler.HasNotification());
        }

        [Fact]
        public void Should_Edit_Entity_Without_Associations_And_Invalid_Product()
        {
            var productReadRepository = Substitute.For<IProductReadRepository>();
            var productRepository = Substitute.For<IProductRepository>();
            var productBillingItemRepository = Substitute.For<IProductBillingItemRepository>();
            var productBillingItemReadRepository = Substitute.For<IProductBillingItemReadRepository>();

            productRepository.InsertAsync(new BaseEntity()).ReturnsForAnyArgs(x =>
            {
                return ProductDtoMock.GetDto();
            });

            productReadRepository.GetProductById(Guid.NewGuid()).ReturnsForAnyArgs(x =>
            {
                return ProductDto.NullInstance;
            });

            productRepository.UpdateEntity(new Product()).ReturnsForAnyArgs(x =>
            {
                return Task.CompletedTask;
            });

            productBillingItemReadRepository.GetBillingItensForProduct(Guid.NewGuid()).ReturnsForAnyArgs(x =>
            {
                return ProductBillingItemDtoMock.GetListDto();
            });

            var dto = new ProductAppService(unitOfWorkManager: ServiceProvider.GetService<IUnitOfWorkManager>(),
                                            productAdapter: ServiceProvider.GetService<IProductAdapter>(),
                                            productBillingItemAdapter: ServiceProvider.GetService<IProductBillingItemAdapter>(),
                                            notificationHandler: _notificationHandler,
                                            productReadRepository: productReadRepository,
                                            productRepository: productRepository,
                                            productBillingItemRepository: productBillingItemRepository,
                                            productBillingItemReadRepository: productBillingItemReadRepository,
                                            billingItemReadRepository: ServiceProvider.GetService<IBillingItemReadRepository>(),
                                            serviceConfiguration: ServiceProvider.GetService<ServicesConfiguration>())
                    .EditObjAsync(Guid.Parse("23eb803c-726a-4c7c-b25b-2c22a56793d9"), ProductDtoMock.GetDto());


            Assert.True(_notificationHandler.HasNotification());
        }

        [Fact]
        public void Should_Edit_Entity_With_Diff_UnitPrice()
        {
            var productReadRepository = Substitute.For<IProductReadRepository>();
            var productRepository = Substitute.For<IProductRepository>();
            var productBillingItemRepository = Substitute.For<IProductBillingItemRepository>();
            var productBillingItemReadRepository = Substitute.For<IProductBillingItemReadRepository>();

            productRepository.InsertAsync(new BaseEntity()).ReturnsForAnyArgs(x =>
            {
                return ProductDtoMock.GetDtoWithAssociations();
            });

            productReadRepository.GetProductById(Guid.NewGuid()).ReturnsForAnyArgs(x =>
            {
                return ProductDtoMock.GetDtoWithAssociations();
            });

            productRepository.UpdateEntity(new Product()).ReturnsForAnyArgs(x =>
            {
                return ProductDtoMock.GetDtoWithAssociations();
            });

            productBillingItemReadRepository.GetBillingItensForProduct(Guid.NewGuid()).ReturnsForAnyArgs(x =>
            {
                return ProductBillingItemDtoMock.GetListDto();
            });

            var dto = new ProductAppService(unitOfWorkManager: ServiceProvider.GetService<IUnitOfWorkManager>(),
                                            productAdapter: ServiceProvider.GetService<IProductAdapter>(),
                                            productBillingItemAdapter: ServiceProvider.GetService<IProductBillingItemAdapter>(),
                                            notificationHandler: _notificationHandler,
                                            productReadRepository: productReadRepository,
                                            productRepository: productRepository,
                                            productBillingItemRepository: productBillingItemRepository,
                                            productBillingItemReadRepository: productBillingItemReadRepository,
                                            billingItemReadRepository: ServiceProvider.GetService<IBillingItemReadRepository>(),
                                            serviceConfiguration: ServiceProvider.GetService<ServicesConfiguration>())
                    .EditObjAsync(Guid.Parse("23eb803c-726a-4c7c-b25b-2c22a56793d9"), ProductDtoMock.GetDtoWithAssociationsAndUnitPrice(55m));

            Assert.False(_notificationHandler.HasNotification());
        }

        [Fact]
        public void Should_Edit_Entity_With_Same_UnitPrice()
        {
            var productReadRepository = Substitute.For<IProductReadRepository>();
            var productRepository = Substitute.For<IProductRepository>();
            var productBillingItemRepository = Substitute.For<IProductBillingItemRepository>();
            var productBillingItemReadRepository = Substitute.For<IProductBillingItemReadRepository>();

            productRepository.InsertAsync(new BaseEntity()).ReturnsForAnyArgs(x =>
            {
                return ProductDtoMock.GetDto();
            });

            productReadRepository.GetProductById(Guid.NewGuid()).ReturnsForAnyArgs(x =>
            {
                return ProductDtoMock.GetDto();
            });

            productRepository.UpdateEntity(new Product()).ReturnsForAnyArgs(x =>
            {
                return ProductDtoMock.GetDto();
            });

            productBillingItemReadRepository.GetBillingItensForProduct(Guid.NewGuid()).ReturnsForAnyArgs(x =>
            {
                return ProductBillingItemDtoMock.GetListDto();
            });

            var dto = new ProductAppService(unitOfWorkManager: ServiceProvider.GetService<IUnitOfWorkManager>(),
                                            productAdapter: ServiceProvider.GetService<IProductAdapter>(),
                                            productBillingItemAdapter: ServiceProvider.GetService<IProductBillingItemAdapter>(),
                                            notificationHandler: _notificationHandler,
                                            productReadRepository: productReadRepository,
                                            productRepository: productRepository,
                                            productBillingItemRepository: productBillingItemRepository,
                                            productBillingItemReadRepository: productBillingItemReadRepository,
                                            billingItemReadRepository: ServiceProvider.GetService<IBillingItemReadRepository>(),
                                            serviceConfiguration: ServiceProvider.GetService<ServicesConfiguration>())
                    .EditObjAsync(Guid.Parse("23eb803c-726a-4c7c-b25b-2c22a56793d9"), ProductDtoMock.GetDtoWithAssociations());

            Assert.False(_notificationHandler.HasNotification());
        }

        [Fact]
        public void Should_Edit_Entity_With_Associations()
        {
            var productReadRepository = Substitute.For<IProductReadRepository>();
            var productRepository = Substitute.For<IProductRepository>();
            var productBillingItemRepository = Substitute.For<IProductBillingItemRepository>();
            var productBillingItemReadRepository = Substitute.For<IProductBillingItemReadRepository>();

            productRepository.InsertAsync(new BaseEntity()).ReturnsForAnyArgs(x =>
            {
                return ProductDtoMock.GetDtoWithAssociations();
            });

            productReadRepository.GetProductById(Guid.NewGuid()).ReturnsForAnyArgs(x =>
            {
                return ProductDtoMock.GetDtoWithAssociations();
            });

            productRepository.UpdateEntity(new Product()).ReturnsForAnyArgs(x =>
            {
                return ProductDtoMock.GetDtoWithAssociations();
            });

            productBillingItemReadRepository.GetBillingItensForProduct(Guid.NewGuid()).ReturnsForAnyArgs(x =>
            {
                return ProductBillingItemDtoMock.GetListDto();
            });

            var dto = new ProductAppService(unitOfWorkManager: ServiceProvider.GetService<IUnitOfWorkManager>(),
                                            productAdapter: ServiceProvider.GetService<IProductAdapter>(),
                                            productBillingItemAdapter: ServiceProvider.GetService<IProductBillingItemAdapter>(),
                                            notificationHandler: _notificationHandler,
                                            productReadRepository: productReadRepository,
                                            productRepository: productRepository,
                                            productBillingItemRepository: productBillingItemRepository,
                                            productBillingItemReadRepository: productBillingItemReadRepository,
                                            billingItemReadRepository: ServiceProvider.GetService<IBillingItemReadRepository>(),
                                            serviceConfiguration: ServiceProvider.GetService<ServicesConfiguration>())
                    .EditObjAsync(Guid.Parse("23eb803c-726a-4c7c-b25b-2c22a56793d9"), ProductDtoMock.GetDtoWithAssociations());

            Assert.False(_notificationHandler.HasNotification());
        }

        [Fact]
        public void Should_Toggle_Entity()
        {
            var productReadRepository = Substitute.For<IProductReadRepository>();
            var productRepository = Substitute.For<IProductRepository>();
            var productBillingItemRepository = Substitute.For<IProductBillingItemRepository>();
            var productBillingItemReadRepository = Substitute.For<IProductBillingItemReadRepository>();

            productRepository.InsertAsync(new BaseEntity()).ReturnsForAnyArgs(x =>
            {
                return ProductDtoMock.GetDto();
            });

            productBillingItemReadRepository.GetBillingItensForProduct(Guid.NewGuid()).ReturnsForAnyArgs(x =>
            {
                return ProductBillingItemDtoMock.GetListDto();
            });

            productBillingItemRepository.ToggleAsync(Guid.NewGuid()).ReturnsForAnyArgs(x =>
            {
                return Task.CompletedTask;
            });

            var dto = new ProductAppService(unitOfWorkManager: ServiceProvider.GetService<IUnitOfWorkManager>(),
                                             productAdapter: ServiceProvider.GetService<IProductAdapter>(),
                                             productBillingItemAdapter: ServiceProvider.GetService<IProductBillingItemAdapter>(),
                                             notificationHandler: _notificationHandler,
                                             productReadRepository: productReadRepository,
                                             productRepository: productRepository,
                                             productBillingItemRepository: productBillingItemRepository,
                                             productBillingItemReadRepository: productBillingItemReadRepository,
                                             billingItemReadRepository: ServiceProvider.GetService<IBillingItemReadRepository>(),
                                             serviceConfiguration: ServiceProvider.GetService<ServicesConfiguration>())
                     .ToggleActivationObjAsync(Guid.Parse("23eb803c-726a-4c7c-b25b-2c22a56793d9"));

            Assert.False(_notificationHandler.HasNotification());
        }

        [Fact]
        public void Should_Delete_Entity_With_Associations()
        {
            var productReadRepository = Substitute.For<IProductReadRepository>();
            var productRepository = Substitute.For<IProductRepository>();
            var productBillingItemRepository = Substitute.For<IProductBillingItemRepository>();
            var productBillingItemReadRepository = Substitute.For<IProductBillingItemReadRepository>();

            productRepository.RemoveAsync(Guid.Parse("23eb803c-726a-4c7c-b25b-2c22a56793d9")).ReturnsForAnyArgs(x =>
            {
                return Task.CompletedTask;
            });

            productBillingItemReadRepository.GetBillingItensForProduct(Guid.NewGuid()).ReturnsForAnyArgs(x =>
            {
                return ProductBillingItemDtoMock.GetListDto();
            });

            productBillingItemRepository.RemoveAsync(Guid.NewGuid()).ReturnsForAnyArgs(x =>
            {
                return Task.CompletedTask;
            });

            var dto = new ProductAppService(unitOfWorkManager: ServiceProvider.GetService<IUnitOfWorkManager>(),
                                            productAdapter: ServiceProvider.GetService<IProductAdapter>(),
                                            productBillingItemAdapter: ServiceProvider.GetService<IProductBillingItemAdapter>(),
                                            notificationHandler: _notificationHandler,
                                            productReadRepository: productReadRepository,
                                            productRepository: productRepository,
                                            productBillingItemRepository: productBillingItemRepository,
                                            productBillingItemReadRepository: productBillingItemReadRepository,
                                            billingItemReadRepository: ServiceProvider.GetService<IBillingItemReadRepository>(),
                                            serviceConfiguration: ServiceProvider.GetService<ServicesConfiguration>())
                    .DeleteObjAsync(Guid.Parse("23eb803c-726a-4c7c-b25b-2c22a56793d9"));

            Assert.False(_notificationHandler.HasNotification());
        }

        [Fact]
        public void Should_GetAll()
        {
            var productReadRepository = Substitute.For<IProductReadRepository>();

            productReadRepository.GetAll(new GetAllProductDto()).ReturnsForAnyArgs(x =>
            {
                return ProductDtoMock.GetListDtoAsync();
            });

            var billingList = new ProductAppService(unitOfWorkManager: ServiceProvider.GetService<IUnitOfWorkManager>(),
                                            productAdapter: ServiceProvider.GetService<IProductAdapter>(),
                                            productBillingItemAdapter: ServiceProvider.GetService<IProductBillingItemAdapter>(),
                                            notificationHandler: _notificationHandler,
                                            productReadRepository: productReadRepository,
                                            productRepository: null,
                                            productBillingItemRepository: null,
                                            productBillingItemReadRepository: null,
                                            billingItemReadRepository: ServiceProvider.GetService<IBillingItemReadRepository>(),
                                            serviceConfiguration: ServiceProvider.GetService<ServicesConfiguration>())
                    .GetAll(new GetAllProductDto()).Result;

            Assert.False(_notificationHandler.HasNotification());
            Assert.Equal(3, billingList.Items.Count);
        }

        [Fact]
        public void Should_GetAll_ByCategoryId()
        {
            var productReadRepository = Substitute.For<IProductReadRepository>();

            productReadRepository.GetProductByCategory(new Guid(), 0).ReturnsForAnyArgs(x =>
            {
                return ProductDtoMock.GetListDtoAsync();
            });

            var billingList = new ProductAppService(unitOfWorkManager: ServiceProvider.GetService<IUnitOfWorkManager>(),
                                            productAdapter: ServiceProvider.GetService<IProductAdapter>(),
                                            productBillingItemAdapter: ServiceProvider.GetService<IProductBillingItemAdapter>(),
                                            notificationHandler: _notificationHandler,
                                            productReadRepository: productReadRepository,
                                            productRepository: null,
                                            productBillingItemRepository: null,
                                            productBillingItemReadRepository: null,
                                            billingItemReadRepository: ServiceProvider.GetService<IBillingItemReadRepository>(),
                                            serviceConfiguration: ServiceProvider.GetService<ServicesConfiguration>())
                    .GetByCategoryId(new Guid(), 0, new GetAllProductDto()).Result;

            Assert.False(_notificationHandler.HasNotification());
            Assert.Equal(3, billingList.Items.Count);
        }

        [Fact]
        public void Should_GetAll_ByFilters()
        {
            var productReadRepository = Substitute.For<IProductReadRepository>();

            productReadRepository.GetAllByFilters(new SearchProductDto(), 8).ReturnsForAnyArgs(x =>
            {
                return ProductDtoMock.GetListDtoAsync();
            });

            var billingList = new ProductAppService(unitOfWorkManager: ServiceProvider.GetService<IUnitOfWorkManager>(),
                                            productAdapter: ServiceProvider.GetService<IProductAdapter>(),
                                            productBillingItemAdapter: ServiceProvider.GetService<IProductBillingItemAdapter>(),
                                            notificationHandler: _notificationHandler,
                                            productReadRepository: productReadRepository,
                                            productRepository: null,
                                            productBillingItemRepository: null,
                                            productBillingItemReadRepository: null,
                                            billingItemReadRepository: ServiceProvider.GetService<IBillingItemReadRepository>(),
                                            serviceConfiguration: ServiceProvider.GetService<ServicesConfiguration>())
                    .GetAllByFilters(new SearchProductDto(), 8).Result;

            Assert.False(_notificationHandler.HasNotification());
            Assert.Equal(3, billingList.Items.Count);
        }

        [Fact]
        public void Should_GetAll_ById()
        {
            var productReadRepository = Substitute.For<IProductReadRepository>();

            productReadRepository.GetProductById(new Guid()).ReturnsForAnyArgs(x =>
            {
                return ProductDtoMock.GetDto();
            });

            var product = new ProductAppService(unitOfWorkManager: ServiceProvider.GetService<IUnitOfWorkManager>(),
                                            productAdapter: ServiceProvider.GetService<IProductAdapter>(),
                                            productBillingItemAdapter: ServiceProvider.GetService<IProductBillingItemAdapter>(),
                                            notificationHandler: _notificationHandler,
                                            productReadRepository: productReadRepository,
                                            productRepository: null,
                                            productBillingItemRepository: null,
                                            productBillingItemReadRepository: null,
                                            billingItemReadRepository: ServiceProvider.GetService<IBillingItemReadRepository>(),
                                            serviceConfiguration: ServiceProvider.GetService<ServicesConfiguration>())
                    .GetById(new Guid()).Result;

            Assert.False(_notificationHandler.HasNotification());
            Assert.Equal(Guid.Parse("23eb803c-726a-4c7c-b25b-2c22a56793d9"), product.Id);
        }
    }
}
