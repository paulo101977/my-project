﻿using Microsoft.Extensions.DependencyInjection;
using NSubstitute;
using Shouldly;
using System;
using System.Threading.Tasks;
using Thex.PDV.Application.Adapters;
using Thex.PDV.Application.Services;
using Thex.PDV.Dto;
using Thex.PDV.Infra.Interfaces;
using Thex.PDV.Infra.Interfaces.ReadRepositories;
using Thex.PDV.Web.Tests.Mocks.Dto;
using Tnf.AspNetCore.TestBase;
using Tnf.Notifications;
using Xunit;

namespace Thex.PDV.Web.Tests.Application
{
    public class ProductCategoryAppServiceTest : TnfAspNetCoreIntegratedTestBase<StartupUnitTest>
    {
        public INotificationHandler _notificationHandler;

        public ProductCategoryAppServiceTest()
        {
            _notificationHandler = new NotificationHandler(ServiceProvider);
        }

        [Fact]
        public void Should_Resolve_All()
        {
            TnfSession.ShouldNotBeNull();
            ServiceProvider.GetService<INotificationHandler>().ShouldNotBeNull();
            ServiceProvider.GetService<IProductCategoryAdapter>().ShouldNotBeNull();
            ServiceProvider.GetService<IProductCategoryRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<IProductCategoryReadRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<IProductReadRepository>().ShouldNotBeNull();
        }

        [Fact]
        public void Should_Generate_Entity_With_Map()
        {
            var entity = new ProductCategoryAppService(productCategoryReadRepository: null,
                                                    productCategoryRepository: null,
                                                    productReadRepository: null,
                                                    notificationHandler: _notificationHandler,
                                                    productCategoryAdapter: ServiceProvider.GetService<IProductCategoryAdapter>())
                    .GeneratedEntity(ProductCategoryDtoMock.GetDto());

            Assert.False(_notificationHandler.HasNotification());
            Assert.Equal(Guid.Parse("7d220627-853d-466f-835f-a06b6d6b1922"), entity.Id);
            Assert.Equal("Teste", entity.CategoryName);
            Assert.Equal("Teste", entity.IconName);
            Assert.True(entity.IsActive);
            Assert.False(entity.IsDeleted);
            Assert.Equal(Guid.Parse("5fe72924-8bd8-4a7c-9df6-c9ebf8cd33b0"), entity.StandardCategoryId);
        }

        [Fact]
        public void Should_Delete_Entity_Without_Product()
        {
            var productReadRepository = Substitute.For<IProductReadRepository>();
            var productCategoryRepository = Substitute.For<IProductCategoryRepository>();

            productReadRepository.ExistProductInCategory(Guid.NewGuid()).ReturnsForAnyArgs(x =>
            {
                return false;
            });

            productCategoryRepository.RemoveAsync(Guid.NewGuid()).ReturnsForAnyArgs(x =>
            {
                return Task.CompletedTask;
            });

            var dto = new ProductCategoryAppService(productCategoryReadRepository: null,
                                                    productCategoryRepository: productCategoryRepository,
                                                    productReadRepository: productReadRepository,
                                                    notificationHandler: _notificationHandler,
                                                    productCategoryAdapter: ServiceProvider.GetService<IProductCategoryAdapter>())
                    .DeleteObjAsync(Guid.Parse("7d220627-853d-466f-835f-a06b6d6b1922"));

            Assert.False(_notificationHandler.HasNotification());
        }

        [Fact]
        public void Should_Delete_Entity_With_Product()
        {
            var productReadRepository = Substitute.For<IProductReadRepository>();

            productReadRepository.ExistProductInCategory(Guid.NewGuid()).ReturnsForAnyArgs(x =>
            {
                return true;
            });

            var dto = new ProductCategoryAppService(productCategoryReadRepository: null,
                                                    productCategoryRepository: null,
                                                    productReadRepository: productReadRepository,
                                                    notificationHandler: _notificationHandler,
                                                    productCategoryAdapter: ServiceProvider.GetService<IProductCategoryAdapter>())
                    .DeleteObjAsync(Guid.Parse("7d220627-853d-466f-835f-a06b6d6b1922"));

            Assert.True(_notificationHandler.HasNotification());
        }


        [Fact]
        public void Should_GetAll()
        {
            var productCategoryReadRepository = Substitute.For<IProductCategoryReadRepository>();

            productCategoryReadRepository.GetAllCategories(new GetAllProductCategoryDto()).ReturnsForAnyArgs(x =>
            {
                return ProductCategoryDtoMock.GetListDtoAsync();
            });

            var list = new ProductCategoryAppService(productCategoryReadRepository: productCategoryReadRepository,
                                                    productCategoryRepository: null,
                                                    productReadRepository: null,
                                                    notificationHandler: _notificationHandler,
                                                    productCategoryAdapter: ServiceProvider.GetService<IProductCategoryAdapter>())
                    .GetAll(new GetAllProductCategoryDto()).Result;

            Assert.False(_notificationHandler.HasNotification());
            Assert.Equal(3, list.Items.Count);
        }

        [Fact]
        public void Should_GetAll_GroupFixed()
        {
            var productCategoryReadRepository = Substitute.For<IProductCategoryReadRepository>();

            productCategoryReadRepository.GetAllGroupFixed().ReturnsForAnyArgs(x =>
            {
                return ProductCategoryDtoMock.GetListDtoAsync();
            });

            var list = new ProductCategoryAppService(productCategoryReadRepository: productCategoryReadRepository,
                                                    productCategoryRepository: null,
                                                    productReadRepository: null,
                                                    notificationHandler: _notificationHandler,
                                                    productCategoryAdapter: ServiceProvider.GetService<IProductCategoryAdapter>())
                    .GetAllGroupFixed().Result;

            Assert.False(_notificationHandler.HasNotification());
            Assert.Equal(3, list.Items.Count);
        }


        [Fact]
        public void Should_GetById()
        {
            var productCategoryReadRepository = Substitute.For<IProductCategoryReadRepository>();

            productCategoryReadRepository.GetCategoryById(new Guid()).ReturnsForAnyArgs(x =>
            {
                return ProductCategoryDtoMock.GetDtoAsync();
            });

            var category = new ProductCategoryAppService(productCategoryReadRepository: productCategoryReadRepository,
                                                    productCategoryRepository: null,
                                                    productReadRepository: null,
                                                    notificationHandler: _notificationHandler,
                                                    productCategoryAdapter: ServiceProvider.GetService<IProductCategoryAdapter>())
                    .GetById(new Guid()).Result;

            Assert.False(_notificationHandler.HasNotification());
            Assert.Equal("Teste", category.CategoryName);
        }
    }
}
