﻿using Thex.PDV.Application.Adapters;
using Thex.PDV.Application.Services;
using Thex.PDV.Domain.Entities;
using Thex.PDV.Dto;
using Thex.PDV.Dto.Dto.BillingAccountItemDebit;
using Thex.PDV.Infra.Interfaces;
using Thex.PDV.Web.Tests.Mocks;
using Tnf.Notifications;

namespace Thex.PDV.Web.Tests.Application
{
    public class ApplicationService : ApplicationServiceBase<ProductCategoryDto, ProductCategory>
    {
        private readonly IProductCategoryAdapter _productCategoryAdapter;

        public ApplicationService(IProductCategoryAdapter productCategoryAdapter,
                                  INotificationHandler notificationHandler,
                                  IBaseRepository baseRepository) : base(notificationHandler, baseRepository)
        {
            _productCategoryAdapter = productCategoryAdapter;
        }

        public override ProductCategory GeneratedEntity(ProductCategoryDto dto)
        {
            return _productCategoryAdapter.Map(dto).Build();
        }
    }
}
