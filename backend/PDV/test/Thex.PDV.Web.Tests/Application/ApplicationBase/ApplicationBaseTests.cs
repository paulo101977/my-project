﻿using Microsoft.Extensions.DependencyInjection;
using NSubstitute;
using Shouldly;
using System;
using System.Threading.Tasks;
using Thex.Common;
using Thex.Common.Enumerations;
using Thex.Infra.Extensions;
using Thex.Kernel;
using Thex.PDV.Application.Adapters;
using Thex.PDV.Domain.Entities;
using Thex.PDV.Infra.Interfaces;
using Thex.PDV.Web.Tests.Mocks.Dto;
using Tnf.AspNetCore.TestBase;
using Tnf.Notifications;
using Xunit;

namespace Thex.PDV.Web.Tests.Application
{
    public class ApplicationBaseTests : TnfAspNetCoreIntegratedTestBase<StartupUnitTest>
    {
        public INotificationHandler _notificationHandler;
        public IProductCategoryAdapter _productCategoryAdapter;

        public ApplicationBaseTests()
        {
            _notificationHandler = new NotificationHandler(ServiceProvider);
            _productCategoryAdapter = new ProductCategoryAdapter(_notificationHandler);
        }

        [Fact]
        public void Should_Resolve_All()
        {
            TnfSession.ShouldNotBeNull();
            ServiceProvider.GetService<INotificationHandler>().ShouldNotBeNull();
            ServiceProvider.GetService<IProductCategoryAdapter>().ShouldNotBeNull();
        }

        [Fact]
        public async Task Should_Create_Entity()
        {
            var baseEntity = Substitute.For<IBaseRepository>();

            // Retorna um valor para a função interceptada
            baseEntity.InsertAsync(new BaseEntity()).ReturnsForAnyArgs(x =>
            {
                return ProductCategoryDtoMock.GetDto();
            });

            var app = new Application.ApplicationService(notificationHandler: _notificationHandler,
                                                         productCategoryAdapter: _productCategoryAdapter,
                                                         baseRepository: baseEntity);

            var dto = await app.CreateObjAsync(ProductCategoryDtoMock.GetDto());

            Assert.False(_notificationHandler.HasNotification());
            Assert.True(dto != null && dto.Id != Guid.Empty);
        }

        [Fact]
        public async Task Should_Not_Create_Entity_With_Empty_Dto()
        {
            var baseEntity = Substitute.For<IBaseRepository>();

            // Retorna um valor para a função interceptada
            baseEntity.InsertAsync(new BaseEntity()).ReturnsForAnyArgs(x =>
            {
                return ProductCategoryDtoMock.GetDto();
            });

            var app = new Application.ApplicationService(notificationHandler: _notificationHandler,
                                                         productCategoryAdapter: _productCategoryAdapter,
                                                         baseRepository: baseEntity);

            var dto = await app.CreateObjAsync(null);

            Assert.True(_notificationHandler.HasNotification());
            Assert.True(dto == null);
        }

        [Fact]
        public async Task Should_Not_Create_Entity_With_Invalid_Entity()
        {
            var baseEntity = Substitute.For<IBaseRepository>();

            // Retorna um valor para a função interceptada
            baseEntity.InsertAsync(new BaseEntity()).ReturnsForAnyArgs(x =>
            {
                _notificationHandler.Raise(_notificationHandler.DefaultBuilder
                                    .WithMessage(AppConsts.LocalizationSourceName, BillingAccountItemEnum.Error.BillingAccountItemInvalidBillingItemId)
                                    .WithDetailedMessage(AppConsts.LocalizationSourceName, BillingAccountItemEnum.Error.BillingAccountItemInvalidBillingItemId)
                                    .Build());

                return ProductCategoryDtoMock.GetDto();
            });

            var app = new Application.ApplicationService(notificationHandler: _notificationHandler,
                                                         productCategoryAdapter: _productCategoryAdapter,
                                                         baseRepository: baseEntity);

            var dto = await app.CreateObjAsync(ProductCategoryDtoMock.GetDto());

            Assert.True(_notificationHandler.HasNotification());
            Assert.True(dto == null);
        }

        [Fact]
        public async Task Should_Edit_Entity()
        {
            var baseEntity = Substitute.For<IBaseRepository>();

            // Retorna um valor para a função interceptada
            baseEntity.UpdateAsync(Guid.Parse("7d220627-853d-466f-835f-a06b6d6b1922"), new BaseEntity()).ReturnsForAnyArgs(x =>
            {
                return ProductCategoryDtoMock.GetDto();
            });

            var app = new Application.ApplicationService(notificationHandler: _notificationHandler,
                                                         productCategoryAdapter: _productCategoryAdapter,
                                                         baseRepository: baseEntity);

            var dto = await app.EditObjAsync(Guid.Parse("7d220627-853d-466f-835f-a06b6d6b1922"),
                                             ProductCategoryDtoMock.GetDto());

            Assert.False(_notificationHandler.HasNotification());
            Assert.True(dto != null && dto.Id != Guid.Empty);
        }

        [Fact]
        public async Task Should_Not_Edit_Entity_With_Empty_Dto()
        {
            var app = new Application.ApplicationService(notificationHandler: _notificationHandler,
                                                         productCategoryAdapter: _productCategoryAdapter,
                                                         baseRepository: ServiceProvider.GetService<IBaseRepository>());

            var dto = await app.EditObjAsync(Guid.Parse("7d220627-853d-466f-835f-a06b6d6b1922"),
                                             null);

            Assert.True(_notificationHandler.HasNotification());
            Assert.True(dto == null);
        }

        [Fact]
        public async Task Should_Not_Edit_Entity_With_Invalid_Entity()
        {
            var baseEntity = Substitute.For<IBaseRepository>();

            // Retorna um valor para a função interceptada
            baseEntity.UpdateAsync(Guid.Parse("7d220627-853d-466f-835f-a06b6d6b1922"), new BaseEntity()).ReturnsForAnyArgs(x =>
            {
                _notificationHandler.Raise(_notificationHandler.DefaultBuilder
                                    .WithMessage(AppConsts.LocalizationSourceName, BillingAccountItemEnum.Error.BillingAccountItemInvalidBillingItemId)
                                    .WithDetailedMessage(AppConsts.LocalizationSourceName, BillingAccountItemEnum.Error.BillingAccountItemInvalidBillingItemId)
                                    .Build());

                return ProductCategoryDtoMock.GetDto();
            });

            var app = new Application.ApplicationService(notificationHandler: _notificationHandler,
                                                         productCategoryAdapter: _productCategoryAdapter,
                                                         baseRepository: baseEntity);

            var dto = await app.EditObjAsync(Guid.Parse("7d220627-853d-466f-835f-a06b6d6b1922"),
                                             ProductCategoryDtoMock.GetDto());

            Assert.True(_notificationHandler.HasNotification());
            Assert.True(dto == null);
        }

        [Fact]
        public async Task Should_Delete_Entity()
        {
            var baseEntity = Substitute.For<IBaseRepository>();

            // Retorna um valor para a função interceptada
            baseEntity.RemoveAsync(Guid.Parse("7d220627-853d-466f-835f-a06b6d6b1922")).ReturnsForAnyArgs(x =>
            {
                return Task.CompletedTask;
            });

            var app = new Application.ApplicationService(notificationHandler: _notificationHandler,
                                                         productCategoryAdapter: _productCategoryAdapter,
                                                         baseRepository: baseEntity);

            await app.DeleteObjAsync(Guid.Parse("7d220627-853d-466f-835f-a06b6d6b1922"));

            Assert.False(_notificationHandler.HasNotification());
        }

        [Fact]
        public async Task Should_Not_Delete_Entity_With_Invalid_Id()
        {
            var app = new Application.ApplicationService(notificationHandler: _notificationHandler,
                                                         productCategoryAdapter: _productCategoryAdapter,
                                                         baseRepository: ServiceProvider.GetService<IBaseRepository>());

            await app.DeleteObjAsync(Guid.Empty);

            Assert.True(_notificationHandler.HasNotification());
        }

        [Fact]
        public async Task Should_Not_Delete_Entity_With_Invalid_Entity()
        {
            var baseEntity = Substitute.For<IBaseRepository>();

            // Retorna um valor para a função interceptada
            baseEntity.RemoveAsync(Guid.Parse("7d220627-853d-466f-835f-a06b6d6b1922")).ReturnsForAnyArgs(x =>
            {
                _notificationHandler.Raise(_notificationHandler.DefaultBuilder
                                    .WithMessage(AppConsts.LocalizationSourceName, BillingAccountItemEnum.Error.BillingAccountItemInvalidBillingItemId)
                                    .WithDetailedMessage(AppConsts.LocalizationSourceName, BillingAccountItemEnum.Error.BillingAccountItemInvalidBillingItemId)
                                    .Build());

                return Task.CompletedTask;
            });

            var app = new Application.ApplicationService(notificationHandler: _notificationHandler,
                                                         productCategoryAdapter: _productCategoryAdapter,
                                                         baseRepository: baseEntity);

            await app.DeleteObjAsync(Guid.Parse("7d220627-853d-466f-835f-a06b6d6b1922"));

            Assert.True(_notificationHandler.HasNotification());
        }

        [Fact]
        public async Task Should_Toggle_Entity()
        {
            var baseEntity = Substitute.For<IBaseRepository>();

            // Retorna um valor para a função interceptada
            baseEntity.ToggleAsync(Guid.Parse("7d220627-853d-466f-835f-a06b6d6b1922")).ReturnsForAnyArgs(x =>
            {
                return Task.CompletedTask;
            });

            var app = new Application.ApplicationService(notificationHandler: _notificationHandler,
                                                         productCategoryAdapter: _productCategoryAdapter,
                                                         baseRepository: baseEntity);

            await app.ToggleActivationObjAsync(Guid.Parse("7d220627-853d-466f-835f-a06b6d6b1922"));

            Assert.False(_notificationHandler.HasNotification());
        }

        [Fact]
        public async Task Should_Not_Toggle_Entity_With_Invalid_Id()
        {
            var app = new Application.ApplicationService(notificationHandler: _notificationHandler,
                                                         productCategoryAdapter: _productCategoryAdapter,
                                                         baseRepository: ServiceProvider.GetService<IBaseRepository>());

            await app.ToggleActivationObjAsync(Guid.Empty);

            Assert.True(_notificationHandler.HasNotification());
        }

        [Fact]
        public async Task Should_Not_Toggle_Entity_With_Invalid_Entity()
        {
            var baseEntity = Substitute.For<IBaseRepository>();

            // Retorna um valor para a função interceptada
            baseEntity.ToggleAsync(Guid.Parse("7d220627-853d-466f-835f-a06b6d6b1922")).ReturnsForAnyArgs(x =>
            {
                _notificationHandler.Raise(_notificationHandler.DefaultBuilder
                                    .WithMessage(AppConsts.LocalizationSourceName, BillingAccountItemEnum.Error.BillingAccountItemInvalidBillingItemId)
                                    .WithDetailedMessage(AppConsts.LocalizationSourceName, BillingAccountItemEnum.Error.BillingAccountItemInvalidBillingItemId)
                                    .Build());

                return Task.CompletedTask;
            });

            var app = new Application.ApplicationService(notificationHandler: _notificationHandler,
                                                         productCategoryAdapter: _productCategoryAdapter,
                                                         baseRepository: baseEntity);

            await app.ToggleActivationObjAsync(Guid.Parse("7d220627-853d-466f-835f-a06b6d6b1922"));

            Assert.True(_notificationHandler.HasNotification());
        }

        [Fact]
        public void Should_Create_Entity_With_Audited_Properties_Creation()
        {
            var entity = new BaseEntity();
            var userId = Guid.Parse("215bc8a0-07db-4428-9955-fa53b58b4ded");
            var timeZoneName = AppConsts.DEFAULT_TIME_ZONE_NAME;

            entity.SetOperationValues(Infra.Operation.Insert, ServiceProvider.GetService<IApplicationUser>());
            Assert.True(entity.CreationTime != default(DateTime));
            Assert.True(entity.CreatorUserId == userId);

            entity.SetOperationValues(Infra.Operation.Update, ServiceProvider.GetService<IApplicationUser>());
            Assert.True(entity.LastModificationTime != default(DateTime));
            Assert.True(entity.LastModifierUserId == userId);

            entity.SetOperationValues(Infra.Operation.Delete, ServiceProvider.GetService<IApplicationUser>());
            Assert.True(entity.DeletionTime != default(DateTime));
            Assert.True(entity.DeleterUserId == userId);
            Assert.True(entity.IsDeleted);
        }
    }
}
