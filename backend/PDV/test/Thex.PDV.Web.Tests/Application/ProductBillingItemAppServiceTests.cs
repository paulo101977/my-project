﻿using Microsoft.Extensions.DependencyInjection;
using NSubstitute;
using Shouldly;
using System;
using Thex.PDV.Application.Adapters;
using Thex.PDV.Application.Services;
using Thex.PDV.Dto;
using Thex.PDV.Infra.Interfaces;
using Thex.PDV.Infra.Interfaces.ReadRepositories;
using Thex.PDV.Web.Tests.Mocks.Dto;
using Tnf.AspNetCore.TestBase;
using Tnf.Notifications;
using Xunit;

namespace Thex.PDV.Web.Tests.Application
{
    public class ProductBillingItemAppServiceTests : TnfAspNetCoreIntegratedTestBase<StartupUnitTest>
    {
        public INotificationHandler _notificationHandler;

        public ProductBillingItemAppServiceTests()
        {
            _notificationHandler = new NotificationHandler(ServiceProvider);
        }

        [Fact]
        public void Should_Resolve_All()
        {
            TnfSession.ShouldNotBeNull();
            ServiceProvider.GetService<INotificationHandler>().ShouldNotBeNull();
            ServiceProvider.GetService<IProductBillingItemReadRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<IProductBillingItemRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<IProductBillingItemAdapter>().ShouldNotBeNull();
        }

        [Fact]
        public void Should_Generate_Entity_With_Map()
        {
            var entity = new ProductBillingItemAppService(productBillingItemReadRepository: null,
                                                       productBillingItemRepository: null,
                                                       notificationHandler: _notificationHandler,
                                                       productBillingItemAdapter: ServiceProvider.GetService<IProductBillingItemAdapter>())
                    .GeneratedEntity(ProductBillingItemDtoMock.GetDto());

            Assert.False(_notificationHandler.HasNotification());
            Assert.Equal(Guid.Parse("50a66538-dd94-4162-a1ed-83fc8311e574"), entity.Id);
            Assert.Equal(8, entity.BillingItemId);
            Assert.True(entity.IsActive);
            Assert.False(entity.IsDeleted);
            Assert.Equal(Guid.Parse("f8b5303a-fda7-4de2-98f4-a96600ba0a99"), entity.ProductId);
            Assert.Equal(10, entity.UnitPrice);
        }

        [Fact]
        public void Should_GetAll()
        {
            var productBillingItemReadRepository = Substitute.For<IProductBillingItemReadRepository>();

            productBillingItemReadRepository.GetAll(new GetAllProductBillingItemDto()).ReturnsForAnyArgs(x =>
            {
                return ProductBillingItemDtoMock.GetListDtoAsync();
            });

            var list = new ProductBillingItemAppService(productBillingItemReadRepository: productBillingItemReadRepository,
                                                        productBillingItemRepository: null,
                                                        notificationHandler: _notificationHandler,
                                                        productBillingItemAdapter: ServiceProvider.GetService<IProductBillingItemAdapter>())
                    .GetAll(new GetAllProductBillingItemDto()).Result;

            Assert.False(_notificationHandler.HasNotification());
            Assert.Equal(3, list.Items.Count);
        }
    }
}
