﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;
using Thex.Common;
using Thex.Kernel;
using Thex.PDV.Application.Interfaces;
using Thex.PDV.Web.Tests.Mocks.AppServiceMock;

namespace Thex.PDV.Web.Tests
{
    public class StartupControllerTest
    {
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            services.AddTnfAspNetCoreSetupTest();

            services.AddTransient<IBillingAccountItemAppService, BillingAccountItemAppServiceMock>();
            services.AddTransient<IBillingItemAppService, BillingItemAppServiceMock>();
            services.AddTransient<INcmAppService, NcmAppServiceMock>();
            services.AddTransient<IProductBillingItemAppService, ProductBillingItemAppServiceMock>();
            services.AddTransient<IProductAppService, ProductAppServiceMock>();
            services.AddTransient<IProductCategoryAppService, ProductCategoryAppServiceMock>();

            services.AddScoped<IApplicationUser>(a => new ApplicationUserMock());
            var serviceProvider = services.BuildServiceProvider();

            ServiceLocator.SetLocatorProvider(serviceProvider);

            return services.BuildServiceProvider();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app)
        {
            // Configura o uso do teste
            app.UseTnfAspNetCoreSetupTest();

            app.UseMvc(routes =>
            {
                routes.MapRoute("default", "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
