﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using Thex.PDV.Domain.Entities;
using Thex.PDV.Dto.Dto.BillingAccountItemDebit;

namespace Thex.PDV.Web.Tests.Mocks.Entity
{
    public static class BillingAccountItemEntityMock
    {
        public static BillingAccountItem GetEntity()
        {
            var detailIntegration = new List<DetailIntegration>
            {
                new DetailIntegration()
                {
                    Quantity = 2,
                    Description = "Teste",
                    Amount = 70
                }
            };

            return new BillingAccountItem()
            {
                PropertyId = 1,
                BillingAccountId = new Guid("CBA11DEA-0582-4EA0-A01E-07EDAA6641BE"),
                BillingItemId = 1,
                Amount = 100,
                CurrencyId = new Guid("67838ACB-9525-4AEB-B0A6-127C1B986C48"),
                CurrencyExchangeReferenceId = new Guid("EF085323-3F4A-4B53-8B6E-01904BADDAEF"),
                TenantId = new Guid("23EB803C-726A-4C7C-B25B-2C22A56793D9"),
                DateParameter = new DateTime(2018, 09, 20),
                IntegrationDetail = JsonConvert.SerializeObject(detailIntegration)
            };
        }

        public static List<BillingAccountItem> GetListEntity()
        {
            var ret = new List<BillingAccountItem>();

            var detailIntegration = new List<DetailIntegration>
            {
                new DetailIntegration()
                {
                    Quantity = 2,
                    Description = "Teste",
                    Amount = 70
                }
            };

            ret.Add(new BillingAccountItem()
            {
                PropertyId = 1,
                BillingAccountId = new Guid("CBA11DEA-0582-4EA0-A01E-07EDAA6641BE"),
                BillingItemId = 1,
                Amount = 100,
                CurrencyId = new Guid("67838ACB-9525-4AEB-B0A6-127C1B986C48"),
                CurrencyExchangeReferenceId = new Guid("EF085323-3F4A-4B53-8B6E-01904BADDAEF"),
                TenantId = new Guid("23EB803C-726A-4C7C-B25B-2C22A56793D9"),
                DateParameter = new DateTime(2018, 09, 20),
                IntegrationDetail = JsonConvert.SerializeObject(detailIntegration)
            });

            return ret;
        }
    }
}
