﻿using System;
using System.Collections.Generic;
using System.Text;
using Thex.PDV.Domain.Entities;

namespace Thex.PDV.Web.Tests.Mocks.Entity
{
    public static class BillingItemEntityMock
    {
        public static BillingItem GetEntity()
        {
            return new BillingItem()
            {
                Id = 8,
                BillingItemTypeId = 4,
                BillingItemName = "PDV",
                PropertyId = 11,
                IsActive = true,
                TenantId = Guid.Parse("23eb803c-726a-4c7c-b25b-2c22a56793d9"),
                IsDeleted = false
            };
        }
    }
}
