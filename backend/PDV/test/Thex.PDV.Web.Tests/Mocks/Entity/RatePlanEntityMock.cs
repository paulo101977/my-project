﻿using System;
using System.Collections.Generic;
using System.Text;
using Thex.PDV.Domain.Entities.Thex;

namespace Thex.PDV.Web.Tests.Mocks.Entity
{
    public static class RatePlanEntityMock
    {
        public static RatePlan GetEntity()
        {
            return new RatePlan()
            {
                Id = Guid.Parse("af5c29b3-6a92-4bd6-94c1-16b8f4212cfd"),
                AgreementTypeId = 1,
                AgreementName = "Acordo",
                StartDate = DateTime.Now,
                EndDate = DateTime.Now.AddDays(7),
                PropertyId = 11,
                MealPlanTypeId = 1,
                IsActive = true,
                CurrencyId = Guid.Parse("67838acb-9525-4aeb-b0a6-127c1b986c48"),
                CurrencySymbol = "RS",
                RateNet = false,
                RateTypeId = 1,
                TenantId = Guid.Parse("23eb803c-726a-4c7c-b25b-2c22a56793d9"),
                IsDeleted = false,
                CreationTime = DateTime.Now
            };
        }
    }
}
