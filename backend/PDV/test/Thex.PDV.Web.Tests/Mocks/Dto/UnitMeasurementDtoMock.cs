﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Thex.PDV.Dto;
using Tnf.Dto;

namespace Thex.PDV.Web.Tests.Mocks.Dto
{
    public static class UnitMeasurementDtoMock
    {
        public static UnitMeasurementDto GetDto(string name = "Litro")
        {
            return new UnitMeasurementDto()
            {
                Id = Guid.Parse("5fe72924-8bd8-4a7c-9df6-c9ebf8cd33b0"),
                Name = name
            };
        }

        public static Task<IListDto<UnitMeasurementDto>> GetListDtoAsync()
        {
            var ret = new ListDto<UnitMeasurementDto>();
            ret.HasNext = false;

            ret.Items.Add(UnitMeasurementDtoMock.GetDto("Litro"));
            ret.Items.Add(UnitMeasurementDtoMock.GetDto("Kilo"));
            ret.Items.Add(UnitMeasurementDtoMock.GetDto("Metro"));

            return Task.FromResult<IListDto<UnitMeasurementDto>>(ret);
        }
    }
}
