﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Thex.PDV.Dto;
using Tnf.Dto;

namespace Thex.PDV.Web.Tests.Mocks.Dto
{
    public static class ProductBillingItemDtoMock
    {
        public static ProductBillingItemDto GetDto(decimal unitPrice = 10m)
        {
            return new ProductBillingItemDto()
            {
                Id = Guid.Parse("50a66538-dd94-4162-a1ed-83fc8311e574"),
                BillingItemId = 8,
                CreationTime = DateTime.Now,
                CreatorUserId = Guid.Parse("7d220627-853d-466f-835f-a06b6d6b1922"),
                IsActive = true,
                IsDeleted = false,
                ProductId = Guid.Parse("f8b5303a-fda7-4de2-98f4-a96600ba0a99"),
                TenantId = Guid.Parse("23eb803c-726a-4c7c-b25b-2c22a56793d9"),
                UnitPrice = unitPrice
            };
        }

        public static IListDto<ProductBillingItemDto> GetListDto()
        {
            var ret = new ListDto<ProductBillingItemDto>();

            ret.Items.Add(new ProductBillingItemDto()
            {
                Id = Guid.NewGuid(),
                BillingItemId = 8,
                CreationTime = DateTime.Now,
                CreatorUserId = Guid.Parse("7d220627-853d-466f-835f-a06b6d6b1922"),
                IsActive = true,
                IsDeleted = false,
                ProductId = Guid.Parse("f8b5303a-fda7-4de2-98f4-a96600ba0a99"),
                TenantId = Guid.Parse("23eb803c-726a-4c7c-b25b-2c22a56793d9"),
                UnitPrice = 10.00m
            });

            return ret;
        }

        public static Task<IListDto<ProductBillingItemDto>> GetListDtoAsync()
        {
            var ret = new ListDto<ProductBillingItemDto>();
            ret.HasNext = false;

            ret.Items.Add(ProductBillingItemDtoMock.GetDto(5));
            ret.Items.Add(ProductBillingItemDtoMock.GetDto(10));
            ret.Items.Add(ProductBillingItemDtoMock.GetDto(15));

            return Task.FromResult<IListDto<ProductBillingItemDto>>(ret);
        }
    }
}
