﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Thex.PDV.Dto;
using Tnf.Dto;

namespace Thex.PDV.Web.Tests.Mocks.Dto
{
    public static class ProductCategoryDtoMock
    {
        public static ProductCategoryDto GetDto(string name = "Teste")
        {
            return new ProductCategoryDto()
            {
                Id = Guid.Parse("7d220627-853d-466f-835f-a06b6d6b1922"),
                CategoryName = name,
                ChainId = 1,
                CreationTime = DateTime.Now,
                CreatorUserId = Guid.Parse("7d220627-853d-466f-835f-a06b6d6b1922"),
                IconName = "Teste",
                IsActive = true,
                IsDeleted = false,
                PropertyId = 11,
                StandardCategoryId = Guid.Parse("5fe72924-8bd8-4a7c-9df6-c9ebf8cd33b0"),
                TenantId = Guid.Parse("23eb803c-726a-4c7c-b25b-2c22a56793d9")
            };
        }

        public static Task<ProductCategoryDto> GetDtoAsync()
        {
            var dto = new ProductCategoryDto()
            {
                Id = Guid.Parse("7d220627-853d-466f-835f-a06b6d6b1922"),
                CategoryName = "Teste",
                ChainId = 1,
                CreationTime = DateTime.Now,
                CreatorUserId = Guid.Parse("7d220627-853d-466f-835f-a06b6d6b1922"),
                IconName = "Teste",
                IsActive = true,
                IsDeleted = false,
                PropertyId = 11,
                StandardCategoryId = Guid.Parse("5fe72924-8bd8-4a7c-9df6-c9ebf8cd33b0"),
                TenantId = Guid.Parse("23eb803c-726a-4c7c-b25b-2c22a56793d9")
            };

            return Task.FromResult(dto);
        }

        public static Task<IListDto<ProductCategoryDto>> GetListDtoAsync()
        {
            var ret = new ListDto<ProductCategoryDto>();

            ret.Items.Add(ProductCategoryDtoMock.GetDto("Teste 1"));
            ret.Items.Add(ProductCategoryDtoMock.GetDto("Teste 2"));
            ret.Items.Add(ProductCategoryDtoMock.GetDto("Teste 3"));

            return Task.FromResult<IListDto<ProductCategoryDto>>(ret);
        }
    }
}
