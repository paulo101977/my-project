﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.PDV.Dto;
using Tnf.Dto;

namespace Thex.PDV.Web.Tests.Mocks.Dto
{
    public static class ProductDtoMock
    {
        public static ProductDto GetDto(string name = "Produto teste")
        {
            return new ProductDto()
            {
                Id = Guid.Parse("23eb803c-726a-4c7c-b25b-2c22a56793d9"),
                BarCode = "1111111111111",
                ChainId = 1,
                Code = "999999",
                CreationTime = DateTime.Now,
                CreatorUserId = Guid.Parse("7d220627-853d-466f-835f-a06b6d6b1922"),
                IsActive = true,
                IsDeleted = false,
                NcmId = Guid.Parse("2fb0c657-1e31-4eaf-a04b-e1bef85c5901"),
                ProductCategoryId = Guid.Parse("07ae782c-2b84-4bef-81da-a96001358fdc"),
                ProductName = name,
                PropertyId = 11,
                TenantId = Guid.Parse("23eb803c-726a-4c7c-b25b-2c22a56793d9"),
                UnitMeasurementId = Guid.Parse("8e6671c3-35bf-4e26-9a37-517256143566")
            };
        }

        public static Task<ProductDto> GetDtoAsync()
        {
            var dto = new ProductDto()
            {
                Id = Guid.Parse("23eb803c-726a-4c7c-b25b-2c22a56793d9"),
                BarCode = "1111111111111",
                ChainId = 1,
                Code = "999999",
                CreationTime = DateTime.Now,
                CreatorUserId = Guid.Parse("7d220627-853d-466f-835f-a06b6d6b1922"),
                IsActive = true,
                IsDeleted = false,
                NcmId = Guid.Parse("2fb0c657-1e31-4eaf-a04b-e1bef85c5901"),
                ProductCategoryId = Guid.Parse("07ae782c-2b84-4bef-81da-a96001358fdc"),
                ProductName = "Produto teste",
                PropertyId = 11,
                TenantId = Guid.Parse("23eb803c-726a-4c7c-b25b-2c22a56793d9"),
                UnitMeasurementId = Guid.Parse("8e6671c3-35bf-4e26-9a37-517256143566")
            };

            return Task.FromResult(dto);
        }

        public static Task<IListDto<ProductDto>> GetListDtoAsync()
        {
            var ret = new ListDto<ProductDto>();

            ret.Items.Add(ProductDtoMock.GetDto("Produto 1"));
            ret.Items.Add(ProductDtoMock.GetDto("Produto 2"));
            ret.Items.Add(ProductDtoMock.GetDto("Produto 3"));

            return Task.FromResult<IListDto<ProductDto>>(ret);
        }

        public static ProductDto GetDtoWithAssociations()
        {
            var billingItens = new List<ProductBillingItemDto>
            {
                ProductBillingItemDtoMock.GetDto()
            };

            return new ProductDto()
            {
                Id = Guid.NewGuid(),
                BarCode = "1111111111111",
                ChainId = 1,
                Code = "999999",
                CreationTime = DateTime.Now,
                CreatorUserId = Guid.Parse("7d220627-853d-466f-835f-a06b6d6b1922"),
                IsActive = true,
                IsDeleted = false,
                NcmId = Guid.Parse("2fb0c657-1e31-4eaf-a04b-e1bef85c5901"),
                ProductCategoryId = Guid.Parse("07ae782c-2b84-4bef-81da-a96001358fdc"),
                ProductName = "Produto teste",
                PropertyId = 11,
                TenantId = Guid.Parse("23eb803c-726a-4c7c-b25b-2c22a56793d9"),
                UnitMeasurementId = Guid.Parse("8e6671c3-35bf-4e26-9a37-517256143566"),
                BillingItens = billingItens
            };
        }

        public static ProductDto GetDtoWithAssociationsAndUnitPrice(decimal unitPrice)
        {
            var billingItens = new List<ProductBillingItemDto>
            {
                ProductBillingItemDtoMock.GetDto(unitPrice)
            };

            return new ProductDto()
            {
                Id = Guid.NewGuid(),
                BarCode = "1111111111111",
                ChainId = 1,
                Code = "999999",
                CreationTime = DateTime.Now,
                CreatorUserId = Guid.Parse("7d220627-853d-466f-835f-a06b6d6b1922"),
                IsActive = true,
                IsDeleted = false,
                NcmId = Guid.Parse("2fb0c657-1e31-4eaf-a04b-e1bef85c5901"),
                ProductCategoryId = Guid.Parse("07ae782c-2b84-4bef-81da-a96001358fdc"),
                ProductName = "Produto teste",
                PropertyId = 11,
                TenantId = Guid.Parse("23eb803c-726a-4c7c-b25b-2c22a56793d9"),
                UnitMeasurementId = Guid.Parse("8e6671c3-35bf-4e26-9a37-517256143566"),
                BillingItens = billingItens
            };
        }
    }
}
