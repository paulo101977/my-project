﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Thex.PDV.Dto;
using Tnf.Dto;

namespace Thex.PDV.Web.Tests.Mocks.Dto
{
    public static class NcmDtoMock
    {
        public static NcmDto GetDto(string code = "000111")
        {
            return new NcmDto()
            {
                Id = Guid.Parse("50a66538-dd94-4162-a1ed-83fc8311e574"),
                Code = code,
                Description = "Bebidas aleatórias"
            };
        }

        public static Task<IListDto<NcmDto>> GetListDtoAsync()
        {
            var ret = new ListDto<NcmDto>();
            ret.HasNext = false;

            ret.Items.Add(NcmDtoMock.GetDto("001"));
            ret.Items.Add(NcmDtoMock.GetDto("002"));
            ret.Items.Add(NcmDtoMock.GetDto("003"));

            return Task.FromResult<IListDto<NcmDto>>(ret);
        }
    }
}
