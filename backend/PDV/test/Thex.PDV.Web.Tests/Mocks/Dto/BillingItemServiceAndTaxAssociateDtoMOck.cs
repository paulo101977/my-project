﻿using System;
using System.Collections.Generic;
using System.Text;
using Thex.PDV.Dto.Dto.BillingItemServiceAndTaxAssociate;

namespace Thex.PDV.Web.Tests.Mocks.Dto
{
    public static class BillingItemServiceAndTaxAssociateDtoMock
    {
        public static BillingItemServiceAndTaxAssociateDto GetDto()
        {
            return new BillingItemServiceAndTaxAssociateDto()
            {
                BillingItemTaxId = 7,
                TaxPercentage = 10
            };
        }

        public static List<BillingItemServiceAndTaxAssociateDto> GetListDto()
        {
            var ret = new List<BillingItemServiceAndTaxAssociateDto>();

            ret.Add(new BillingItemServiceAndTaxAssociateDto()
            {
                BillingItemTaxId = 7,
                TaxPercentage = 10
            });

            return ret;
        }
    }
}
