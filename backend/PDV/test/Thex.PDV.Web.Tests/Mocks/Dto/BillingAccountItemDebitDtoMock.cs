﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.PDV.Dto.Dto.BillingAccountItemDebit;

namespace Thex.PDV.Web.Tests.Mocks
{
    public static class BillingAccountItemDebitDtoMock
    {
        public static BillingAccountItemDebitDto GetDto(decimal amount = 100)
        {
            var detailIntegration = new List<DetailIntegration>
            {
                new DetailIntegration()
                {
                    Code = "001",
                    Quantity = 2,
                    Description = "Teste",
                    Amount = amount
                }
            };

            return new BillingAccountItemDebitDto()
            {
                PropertyId = 1,
                BillingAccountId = new Guid("CBA11DEA-0582-4EA0-A01E-07EDAA6641BE"),
                BillingItemId = 1,
                Amount = amount,
                CurrencyId = new Guid("67838ACB-9525-4AEB-B0A6-127C1B986C48"),
                CurrencyExchangeReferenceId = new Guid("EF085323-3F4A-4B53-8B6E-01904BADDAEF"),
                TenantId = new Guid("23EB803C-726A-4C7C-B25B-2C22A56793D9"),
                DateParameter = new DateTime(2018, 09, 20),
                DetailList = detailIntegration
            };
        }

        public static Task<BillingAccountItemDebitDto> GetDtoAsync(decimal amount = 100)
        {
            var detailIntegration = new List<DetailIntegration>
            {
                new DetailIntegration()
                {
                    Quantity = 2,
                    Description = "Teste",
                    Amount = amount
                }
            };

            var dto = new BillingAccountItemDebitDto()
            {
                PropertyId = 1,
                BillingAccountId = new Guid("CBA11DEA-0582-4EA0-A01E-07EDAA6641BE"),
                BillingItemId = 1,
                Amount = amount,
                CurrencyId = new Guid("67838ACB-9525-4AEB-B0A6-127C1B986C48"),
                CurrencyExchangeReferenceId = new Guid("EF085323-3F4A-4B53-8B6E-01904BADDAEF"),
                TenantId = new Guid("23EB803C-726A-4C7C-B25B-2C22A56793D9"),
                DateParameter = new DateTime(2018, 09, 20),
                DetailList = detailIntegration
            };

            return Task.FromResult(dto);
        }
    }
}
