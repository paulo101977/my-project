﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Thex.PDV.Dto;
using Tnf.Dto;

namespace Thex.PDV.Web.Tests.Mocks.Dto
{
    public static class BillingItemDtoMock
    {
        public static BillingItemDto GetDto(string name = "PDV")
        {
            return new BillingItemDto()
            {
                Id = 8,
                BillingItemTypeId = 4,
                BillingItemName = name,
                PropertyId = 11,
                IsActive = true,
                TenantId = Guid.Parse("23eb803c-726a-4c7c-b25b-2c22a56793d9"),
                IsDeleted = false
            };
        }

        public static Task<IListDto<BillingItemDto>> GetListDtoAsync()
        {
            var ret = new ListDto<BillingItemDto>();
            ret.HasNext = false;

            ret.Items.Add(BillingItemDtoMock.GetDto("PDV 1"));
            ret.Items.Add(BillingItemDtoMock.GetDto("PDV 2"));
            ret.Items.Add(BillingItemDtoMock.GetDto("PDV 3"));

            return Task.FromResult<IListDto<BillingItemDto>>(ret);
        }
    }
}
