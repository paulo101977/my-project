﻿using System;
using System.Collections.Generic;
using System.Text;
using Thex.PDV.Dto.Dto.CurrencyForBillingAccountItem;

namespace Thex.PDV.Web.Tests.Mocks.Dto
{
    public static class CurrencyForBillingAccountItemDtoMock
    {
        public static CurrencyForBillingAccountItemDto GetDto()
        {
            return new CurrencyForBillingAccountItemDto()
            {
                CurrencyExchangeReferenceId = Guid.Parse("3d01d5dc-28cb-4a6e-987a-298fc72dbd7b"),
                CurrencyId = Guid.Parse("67838acb-9525-4aeb-b0a6-127c1b986c48")
            };
        }
    }
}
