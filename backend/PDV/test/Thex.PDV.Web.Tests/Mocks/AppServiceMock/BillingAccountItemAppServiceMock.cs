﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Thex.PDV.Application.Interfaces;
using Thex.PDV.Dto.Dto.BillingAccountItemDebit;

namespace Thex.PDV.Web.Tests.Mocks.AppServiceMock
{
    public class BillingAccountItemAppServiceMock : IBillingAccountItemAppService
    {
        public Task<BillingAccountItemDebitDto> CreateDebit(BillingAccountItemDebitDto dto)
        {
            return BillingAccountItemDebitDtoMock.GetDtoAsync();
        }
    }
}
