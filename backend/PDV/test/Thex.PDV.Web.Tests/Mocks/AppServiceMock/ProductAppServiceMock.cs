﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.PDV.Application.Interfaces;
using Thex.PDV.Dto;
using Thex.PDV.Dto.Product;
using Thex.PDV.Web.Tests.Mocks.Dto;
using Tnf.Dto;

namespace Thex.PDV.Web.Tests.Mocks.AppServiceMock
{
    public class ProductAppServiceMock : IProductAppService
    {
        public Task<ProductDto> CreateObjAsync(ProductDto dto)
        {
            return ProductDtoMock.GetDtoAsync();
        }

        public Task DeleteObjAsync(Guid id)
        {
            return Task.CompletedTask;
        }

        public Task<ProductDto> EditObjAsync(Guid id, ProductDto productDto)
        {
            return ProductDtoMock.GetDtoAsync();
        }

        public Task<IListDto<ProductBillingItemDto>> GetAll(GetAllProductBillingItemDto requestDto)
        {
            return ProductBillingItemDtoMock.GetListDtoAsync();
        }

        public Task<IListDto<ProductDto>> GetAll(GetAllProductDto requestDto)
        {
            return ProductDtoMock.GetListDtoAsync();
        }

        public Task<IListDto<ProductDto>> GetAllByFilters(SearchProductDto requestDto, int billingItemId)
        {
            return ProductDtoMock.GetListDtoAsync();
        }

        public Task<ProductDto> GetByBarCode(string barCode)
        {
            return ProductDtoMock.GetDtoAsync();
        }

        public Task<ProductDto> GetImageBarCode(GetAllProductDto product)
        {
            return ProductDtoMock.GetDtoAsync();
        }

        public Task<IListDto<ProductDto>> GetByCategoryId(Guid categoryId, int billingItemId, GetAllProductDto requestDto)
        {
            return ProductDtoMock.GetListDtoAsync();
        }

        public Task<ProductDto> GetById(Guid id)
        {
            return ProductDtoMock.GetDtoAsync();
        }

        public Task ToggleActivationObjAsync(Guid id)
        {
            return Task.CompletedTask;
        }

        public Task<List<ProductDto>> GetAllIdAndName()
        {
            return Task.FromResult(new List<ProductDto>());
        }
    }
}
