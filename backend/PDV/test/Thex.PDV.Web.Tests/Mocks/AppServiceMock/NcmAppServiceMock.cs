﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Thex.PDV.Application.Interfaces;
using Thex.PDV.Dto;
using Thex.PDV.Dto.GetAll;
using Thex.PDV.Web.Tests.Mocks.Dto;
using Tnf.Dto;

namespace Thex.PDV.Web.Tests.Mocks.AppServiceMock
{
    public class NcmAppServiceMock : INcmAppService
    {
        public Task<IListDto<NcmDto>> GetAll(GetAllNcmDto requestDto)
        {
            return NcmDtoMock.GetListDtoAsync();
        }

        public Task<IListDto<NcmDto>> GetAllByFilters(SearchNcmDto requestDto)
        {
            return NcmDtoMock.GetListDtoAsync();
        }
    }
}
