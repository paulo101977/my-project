﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Thex.PDV.Application.Interfaces;
using Thex.PDV.Dto;
using Thex.PDV.Web.Tests.Mocks.Dto;
using Tnf.Dto;

namespace Thex.PDV.Web.Tests.Mocks.AppServiceMock
{
    public class UnitMeasurementAppServiceMock : IUnitMeasurementAppService
    {
        public Task<IListDto<UnitMeasurementDto>> GetAll(GetAllUnitMeasurementDto requestDto)
        {
            return UnitMeasurementDtoMock.GetListDtoAsync();
        }
    }
}
