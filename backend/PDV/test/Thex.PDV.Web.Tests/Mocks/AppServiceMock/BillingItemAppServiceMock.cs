﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Thex.PDV.Application.Interfaces;
using Thex.PDV.Dto;
using Thex.PDV.Web.Tests.Mocks.Dto;
using Tnf.Dto;

namespace Thex.PDV.Web.Tests.Mocks.AppServiceMock
{
    public class BillingItemAppServiceMock : IBillingItemAppService
    {
        public Task<IListDto<BillingItemDto>> GetAll(GetAllBillingItemDto requestDto)
        {
            return BillingItemDtoMock.GetListDtoAsync();
        }

        public Task<IListDto<BillingItemDto>> GetAllByBillingItem(string billingItemId)
        {
            return BillingItemDtoMock.GetListDtoAsync();
        }
    }
}
