﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Thex.PDV.Application.Interfaces;
using Thex.PDV.Dto;
using Thex.PDV.Web.Tests.Mocks.Dto;
using Tnf.Dto;

namespace Thex.PDV.Web.Tests.Mocks.AppServiceMock
{
    public class ProductBillingItemAppServiceMock : IProductBillingItemAppService
    {
        public Task<IListDto<ProductBillingItemDto>> GetAll(GetAllProductBillingItemDto requestDto)
        {
            return ProductBillingItemDtoMock.GetListDtoAsync();
        }

        public Task<IListDto<ProductBillingItemDto>> GetByBillingItem(string billingItemId)
        {
            return ProductBillingItemDtoMock.GetListDtoAsync();
        }
    }
}
