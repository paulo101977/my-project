﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Thex.PDV.Application.Interfaces;
using Thex.PDV.Dto;
using Thex.PDV.Web.Tests.Mocks.Dto;
using Tnf.Dto;

namespace Thex.PDV.Web.Tests.Mocks.AppServiceMock
{
    public class ProductCategoryAppServiceMock : IProductCategoryAppService
    {
        public Task<ProductCategoryDto> CreateObjAsync(ProductCategoryDto dto)
        {
            return ProductCategoryDtoMock.GetDtoAsync();
        }

        public Task DeleteObjAsync(Guid id)
        {
            return Task.CompletedTask;
        }

        public Task<ProductCategoryDto> EditObjAsync(Guid id, ProductCategoryDto productDto)
        {
            return ProductCategoryDtoMock.GetDtoAsync();
        }

        public Task<IListDto<ProductCategoryDto>> GetAll(GetAllProductCategoryDto request)
        {
            return ProductCategoryDtoMock.GetListDtoAsync();
        }

        public Task<IListDto<ProductCategoryDto>> GetAllByBillingItemId(GetAllProductCategoryDto request)
        {
            return ProductCategoryDtoMock.GetListDtoAsync();
        }

        public Task<IListDto<ProductCategoryDto>> GetAllGroupFixed()
        {
            return ProductCategoryDtoMock.GetListDtoAsync();
        }

        public Task<ProductCategoryDto> GetById(Guid id)
        {
            return ProductCategoryDtoMock.GetDtoAsync();
        }

        public Task ToggleActivationObjAsync(Guid id)
        {
            return Task.CompletedTask;
        }
    }
}
