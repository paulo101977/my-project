﻿using Shouldly;
using System;
using System.Collections.Generic;
using System.Text;
using Thex.PDV.Application.Interfaces;
using Thex.PDV.Web.Controllers;
using Tnf.AspNetCore.TestBase;
using Xunit;
using Microsoft.Extensions.DependencyInjection;
using System.Threading.Tasks;
using Thex.PDV.Dto;
using Tnf.Dto;

namespace Thex.PDV.Web.Tests.Controllers
{
    public class NcmControllerTest : TnfAspNetCoreIntegratedTestBase<StartupControllerTest>
    {
        [Fact]
        public void Should_Resolve_All()
        {
            TnfSession.ShouldNotBeNull();
            ServiceProvider.GetService<NcmController>().ShouldNotBeNull();
            ServiceProvider.GetService<INcmAppService>().ShouldNotBeNull();
        }

        [Fact]
        public async Task Should_GetAll()
        {
            var response = await GetResponseAsObjectAsync<ListDto<NcmDto>>(
                $"{RouteConsts.NcmRouteName}");

            Assert.False(response.HasNext);
            Assert.Equal(3, response.Items.Count);
        }
    }
}
