﻿using Shouldly;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Thex.PDV.Dto;
using Tnf.AspNetCore.TestBase;
using Tnf.Dto;
using Xunit;
using Microsoft.Extensions.DependencyInjection;
using Thex.PDV.Web.Controllers;
using Thex.PDV.Application.Interfaces;
using Thex.PDV.Web.Tests.Mocks.Dto;
using Thex.Kernel;

namespace Thex.PDV.Web.Tests.Controllers
{
    public class ProductCategoryControllerTests : TnfAspNetCoreIntegratedTestBase<StartupControllerTest>
    {
        [Fact]
        public void Should_Resolve_All()
        {
            TnfSession.ShouldNotBeNull();
            ServiceProvider.GetService<ProductCategoryController>().ShouldNotBeNull();
            ServiceProvider.GetService<IProductCategoryAppService>().ShouldNotBeNull();
            ServiceProvider.GetService<IApplicationUser>().ShouldNotBeNull();
        }

        [Fact]
        public async Task Should_GetAll()
        {
            var response = await GetResponseAsObjectAsync<ListDto<ProductCategoryDto>>(
                $"{RouteConsts.ProductCategoryRouteName}");

            Assert.False(response.HasNext);
            Assert.Equal(3, response.Items.Count);
        }

        [Fact]
        public async Task Should_GetAllGroupFixed()
        {
            var response = await GetResponseAsObjectAsync<ProductCategoryDto>(
                $"{RouteConsts.ProductCategoryRouteName}/{"groupfixed"}");

            Assert.NotNull(response);
        }

        [Fact]
        public async Task Should_GetById()
        {
            var response = await GetResponseAsObjectAsync<ProductCategoryDto>(
                $"{RouteConsts.ProductCategoryRouteName}/{"50a66538-dd94-4162-a1ed-83fc8311e574"}");

            Assert.NotNull(response);
        }

        //[Fact]
        //public async Task Should_GetByCategory()
        //{
        //    var response = await GetResponseAsObjectAsync<ProductCategoryDto>(
        //        $"{RouteConsts.ProductCategoryRouteName}/{"50a66538-dd94-4162-a1ed-83fc8311e574"}");

        //    Assert.NotNull(response);
        //}

        [Fact]
        public async Task Should_Post()
        {
            var billingAccountItem = await PostResponseAsObjectAsync<ProductCategoryDto, ProductCategoryDto>(
            $"{RouteConsts.ProductCategoryRouteName}", ProductCategoryDtoMock.GetDto());

            Assert.NotNull(billingAccountItem);
        }

        [Fact]
        public async Task Should_Put()
        {
            var billingAccountItem = await PutResponseAsObjectAsync<ProductCategoryDto, ProductCategoryDto>(
            $"{RouteConsts.ProductCategoryRouteName}/{"50a66538-dd94-4162-a1ed-83fc8311e574"}", ProductCategoryDtoMock.GetDto());

            Assert.NotNull(billingAccountItem);
        }

        [Fact]
        public Task Should_Delete()
        {
            return DeleteResponseAsync(
                $"{RouteConsts.ProductCategoryRouteName}/{"c3e5678d-815f-42af-bad0-c826d9bc7790"}"
            );
        }

        [Fact]
        public Task Should_Toggle()
        {
            return PatchResponseAsync(
                $"{RouteConsts.ProductCategoryRouteName}/{"c3e5678d-815f-42af-bad0-c826d9bc7790"}/{"toggleactivation"}",
                null
            );
        }
    }
}
