﻿using Shouldly;
using System.Threading.Tasks;
using Thex.PDV.Domain.Entities;
using Thex.PDV.Dto.Dto.BillingAccountItemDebit;
using Thex.PDV.Web.Tests.Mocks;
using Tnf.AspNetCore.TestBase;
using Xunit;
using Microsoft.Extensions.DependencyInjection;
using Thex.PDV.Web.Controllers;
using Thex.PDV.Application.Interfaces;

namespace Thex.PDV.Web.Tests.Controllers
{
    public class BillingAccountItemControllerTests : TnfAspNetCoreIntegratedTestBase<StartupControllerTest>
    {
        [Fact]
        public void Should_Resolve_All()
        {
            TnfSession.ShouldNotBeNull();
            ServiceProvider.GetService<BillingAccountItemController>().ShouldNotBeNull();
            ServiceProvider.GetService<IBillingAccountItemAppService>().ShouldNotBeNull();
        }

        [Fact]
        public async Task Should_Create_BillingAccountItem()
        {
            var billingAccountItem = await PostResponseAsObjectAsync<BillingAccountItemDebitDto, BillingAccountItemDebitDto>(
                $"{RouteConsts.BillingAccountItem}/debit", BillingAccountItemDebitDtoMock.GetDto()
            );

            Assert.NotNull(billingAccountItem);
        }
    }
}
