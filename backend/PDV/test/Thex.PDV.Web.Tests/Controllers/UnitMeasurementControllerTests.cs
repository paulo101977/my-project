﻿using Shouldly;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Thex.PDV.Dto;
using Tnf.AspNetCore.TestBase;
using Tnf.Dto;
using Xunit;
using Microsoft.Extensions.DependencyInjection;
using Thex.PDV.Web.Controllers;
using Thex.PDV.Application.Interfaces;

namespace Thex.PDV.Web.Tests.Controllers
{
    public class UnitMeasurementControllerTests : TnfAspNetCoreIntegratedTestBase<StartupControllerTest>
    {
        [Fact]
        public void Should_Resolve_All()
        {
            TnfSession.ShouldNotBeNull();
            ServiceProvider.GetService<UnitMeasurementController>().ShouldNotBeNull();
            ServiceProvider.GetService<IUnitMeasurementAppService>().ShouldNotBeNull();
        }

        [Fact]
        public async Task Should_GetAll()
        {
            var response = await GetResponseAsObjectAsync<ListDto<UnitMeasurementDto>>(
                $"{RouteConsts.UnitMeasurementRouteName}");

            Assert.False(response.HasNext);
            Assert.Equal(3, response.Items.Count);
        }
    }
}
