﻿using Microsoft.Extensions.DependencyInjection;
using Shouldly;
using System.Threading.Tasks;
using Thex.PDV.Application.Interfaces;
using Thex.PDV.Dto;
using Thex.PDV.Web.Controllers;
using Tnf.AspNetCore.TestBase;
using Tnf.Dto;
using Xunit;

namespace Thex.PDV.Web.Tests.Controllers
{
    public class ProductBillingItemControllerTests : TnfAspNetCoreIntegratedTestBase<StartupControllerTest>
    {
        [Fact]
        public void Should_Resolve_All()
        {
            TnfSession.ShouldNotBeNull();
            ServiceProvider.GetService<ProductBillingItemController>().ShouldNotBeNull();
            ServiceProvider.GetService<IProductAppService>().ShouldNotBeNull();
        }

        [Fact]
        public async Task Should_GetAll()
        {
            var response = await GetResponseAsObjectAsync<ListDto<ProductBillingItemDto>>(
                $"{RouteConsts.ProductBillingItemRouteName}");

            Assert.False(response.HasNext);
            Assert.Equal(3, response.Items.Count);
        }        
    }
}
