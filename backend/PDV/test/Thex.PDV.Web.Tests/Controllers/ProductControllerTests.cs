﻿using Microsoft.Extensions.DependencyInjection;
using Shouldly;
using System.Threading.Tasks;
using Thex.PDV.Application.Interfaces;
using Thex.PDV.Dto;
using Thex.PDV.Web.Controllers;
using Thex.PDV.Web.Tests.Mocks.Dto;
using Tnf.AspNetCore.TestBase;
using Tnf.Dto;
using Xunit;

namespace Thex.PDV.Web.Tests.Controllers
{
    public class ProductControllerTests : TnfAspNetCoreIntegratedTestBase<StartupControllerTest>
    {
        [Fact]
        public void Should_Resolve_All()
        {
            TnfSession.ShouldNotBeNull();
            ServiceProvider.GetService<ProductController>().ShouldNotBeNull();
            ServiceProvider.GetService<IProductAppService>().ShouldNotBeNull();
        }

        [Fact]
        public async Task Should_GetAllByFilters()
        {
            var response = await GetResponseAsObjectAsync<ListDto<ProductDto>>(
                $"{RouteConsts.ProductRouteName}/8/search?Code=123456");

            Assert.False(response.HasNext);
            Assert.Equal(3, response.Items.Count);
        }

        [Fact]
        public async Task Should_GetAll()
        {
            var response = await GetResponseAsObjectAsync<ListDto<ProductDto>>(
                $"{RouteConsts.ProductRouteName}");

            Assert.False(response.HasNext);
            Assert.Equal(3, response.Items.Count);
        }

        [Fact]
        public async Task Should_GetById()
        {
            var response = await GetResponseAsObjectAsync<ProductDto>(
                $"{RouteConsts.ProductRouteName}/{"23eb803c-726a-4c7c-b25b-2c22a56793d9"}");

            Assert.NotNull(response);
        }

        [Fact]
        public async Task Should_GetImageBarCode()
        {
            var response = await PostResponseAsObjectAsync<GetAllProductDto, ProductDto>(
                $"{RouteConsts.ProductRouteName}/getImageBarCode", new GetAllProductDto());

            Assert.NotNull(response);
        }

        [Fact]
        public async Task Should_GetByCategory()
        {
            var response = await GetResponseAsObjectAsync<ProductDto>(
                $"{RouteConsts.ProductRouteName}/category/{"50a66538-dd94-4162-a1ed-83fc8311e574"}/1");

            Assert.NotNull(response);
        }

        [Fact]
        public async Task Should_Post()
        {
            var billingAccountItem = await PostResponseAsObjectAsync<ProductDto, ProductDto>(
            $"{RouteConsts.ProductRouteName}", ProductDtoMock.GetDto());

            Assert.NotNull(billingAccountItem);
        }

        [Fact]
        public async Task Should_Put()
        {
            var billingAccountItem = await PutResponseAsObjectAsync<ProductDto, ProductDto>(
            $"{RouteConsts.ProductRouteName}/{"50a66538-dd94-4162-a1ed-83fc8311e574"}", ProductDtoMock.GetDto());

            Assert.NotNull(billingAccountItem);
        }

        [Fact]
        public Task Should_Delete()
        {
            return DeleteResponseAsync(
                $"{RouteConsts.ProductRouteName}/{"c3e5678d-815f-42af-bad0-c826d9bc7790"}"
            );
        }

        [Fact]
        public Task Should_Toggle()
        {
            return PatchResponseAsync(
                $"{RouteConsts.ProductRouteName}/{"c3e5678d-815f-42af-bad0-c826d9bc7790"}/{"toggleactivation"}",
                null
            );
        }
    }
}
