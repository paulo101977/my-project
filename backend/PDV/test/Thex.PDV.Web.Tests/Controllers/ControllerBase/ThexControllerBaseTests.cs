﻿using Microsoft.Extensions.DependencyInjection;
using Shouldly;
using Thex.PDV.Base;
using Tnf.AspNetCore.TestBase;
using Xunit;

namespace Thex.PDV.Web.Tests.Controllers
{
    public class ThexControllerBaseTests : TnfAspNetCoreIntegratedTestBase<StartupControllerTest>
    {
        [Fact]
        public void Should_Resolve_All()
        {
            TnfSession.ShouldNotBeNull();
            ServiceProvider.GetService<ThexAppController>().ShouldNotBeNull();
        }
    }
}
