﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Thex.Support.Dto
{
    public class DocumentTypeDto
    {
        public int Id { get; set; }
        public static DocumentTypeDto NullInstance = null;
        public string PersonType { get; set; }
        public int? CountrySubdivisionId { get; set; }
        public string Name { get; set; }
        public string StringFormatMask { get; set; }
        public string RegexValidationExpression { get; set; }
        public bool IsMandatory { get; set; }
    }
}
