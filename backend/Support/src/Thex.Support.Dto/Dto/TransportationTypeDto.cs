﻿using Tnf.Dto;

namespace Thex.Support.Dto
{
    public partial class TransportationTypeDto : BaseDto
    {
        public int Id { get; set; }
        public static TransportationTypeDto NullInstance = null;

        public string TransportationTypeName { get; set; }
    }
}
