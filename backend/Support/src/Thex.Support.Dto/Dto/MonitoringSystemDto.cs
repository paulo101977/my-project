﻿using System;
using Thex.Support.Dto.Enumerations;
using Tnf.Dto;

namespace Thex.Support.Dto
{
    public partial class MonitoringSystemDto : BaseDto
    {
        public int Id { get;   set; }
        public static MonitoringSystemDto NullInstance = null;
        public Guid PropertyUid { get;   set; }
        public RoutineEnum Routine { get;   set; }
        public bool IsWizard { get;   set; }
        public bool IsCompletedWizard { get;   set; }
        public Guid FinishedUserId { get;   set; }
    }
}
