﻿namespace Thex.Support.Dto.Enumerations
{
    public enum TransportationTypeEnum
    {
        ArrivingByPlane = 1,
        ArrivingByCar = 2,
        ArrivingByMotorcycle = 3,
        ArrivingByShipOrFerryBoat = 4,
        ArrivingByBus = 5,
        ArrivingByTrain = 6,
        ArrivingByOther = 7
    }
}