﻿using System;
using Tnf.Dto;

namespace Thex.Support.Dto
{
    public interface IDefaultGuidRequestDto : IRequestDto
    {
        Guid Id { get; set; }
    }

    public interface IDefaultIntRequestDto : IRequestDto
    {
        int Id { get; set; }
    }

    public interface IDefaultLongRequestDto : IRequestDto
    {
        long Id { get; set; }
    }

    public interface IDefaultStringRequestDto : IRequestDto
    {
        string Id { get; set; }
    }
}
