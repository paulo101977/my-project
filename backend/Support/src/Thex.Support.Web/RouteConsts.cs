﻿namespace Thex.Support.Web
{
    public class RouteConsts
    {
        public const string TransportationType = "api/TransportationType";
        public const string DocumentType = "api/Documenttypes";
        public const string MonitoringSystem = "api/MonitoringSystem";
    }
}
