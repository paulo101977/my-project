﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Thex.Support.Application;
using Thex.Support.Application.Interfaces;
using Thex.Support.Dto;
using Tnf.AspNetCore.Mvc.Response;
using Tnf.Dto;

namespace Thex.Support.Web.Controllers
{
    [Route(RouteConsts.TransportationType)]
    public class TransportationTypeController : TnfController
    {
        private readonly ITransportationTypeAppService _transportationTypeAppService;

        public TransportationTypeController(ITransportationTypeAppService transportationTypeAppService)
        {
            _transportationTypeAppService = transportationTypeAppService;
        }

        [HttpGet]
        [ThexAuthorize("PMS_TransportationType_Get")]
        [ProducesResponseType(typeof(IListDto<TransportationTypeDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> GetAsync()
        {
            var response = await _transportationTypeAppService.GetAllAsync();

            return CreateResponseOnGetAll(response, EntityNames.TransportationType);
        }
    }
}
