﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics.CodeAnalysis;
using System.Threading.Tasks;
using Thex.Support.Application;
using Thex.Support.Application.Interfaces;
using Thex.Support.Dto;
using Tnf.AspNetCore.Mvc.Response;

namespace Thex.Support.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [AllowAnonymous]
    [ExcludeFromCodeCoverage]
    public class HealthController : TnfController
    {
        public IDocumentTypeAppService _documentTypeAppService;

        public HealthController(IDocumentTypeAppService documentTypeAppService)
        {
            _documentTypeAppService = documentTypeAppService;
        }

        /// <summary>
        /// Verifica se a API está em funcionamento e conectada no banco de dados
        /// </summary>
        /// <returns></returns>
        [HttpGet("check")]
        [ProducesResponseType(typeof(bool), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> GetAll([FromQuery]GetAllDocumentTypeDto requestDto)
        {
            var response = await _documentTypeAppService.CheckHealthStatus(requestDto);

            return CreateResponseOnGetAll(response != null ? true : false, EntityNames.Health);
        }
    }
}