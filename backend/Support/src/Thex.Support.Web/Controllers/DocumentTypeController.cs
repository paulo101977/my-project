﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Thex.Support.Application;
using Thex.Support.Application.Interfaces;
using Thex.Support.Dto;
using Tnf.AspNetCore.Mvc.Response;
using Tnf.Dto;

namespace Thex.Support.Web.Controllers
{
    [Route(RouteConsts.DocumentType)]

    public class DocumentTypeController : TnfController
    {
        public IDocumentTypeAppService _documentTypeAppService;

        public DocumentTypeController(IDocumentTypeAppService documentTypeAppService)
        {
            _documentTypeAppService = documentTypeAppService;
        }

        [HttpGet]
        [ThexAuthorize("PMS_DocumentType_Get")]
        [ProducesResponseType(typeof(IListDto<DocumentTypeDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Get([FromQuery] GetAllDocumentTypeDto requestDto)
        {
            var response = await _documentTypeAppService.GetAllByPropertyAsync(requestDto);

            if(response == null || response.Items.Count == 0)
                response = await _documentTypeAppService.GetAllAsync();

            return CreateResponseOnGetAll(response, EntityNames.DocumentType);
        }

        [HttpGet("globaltypes")]
        [ThexAuthorize("PMS_DocumentType_Get")]
        [ProducesResponseType(typeof(IListDto<DocumentTypeDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> GetAllGlobal([FromQuery] GetAllDocumentTypeDto requestDto)
        {
            var response = await _documentTypeAppService.GetAllGlobalAsync(requestDto);

            return CreateResponseOnGetAll(response, EntityNames.DocumentType);
        }

        [HttpGet("{id}")]
        [ThexAuthorize("PMS_DocumentType_Get")]
        [ProducesResponseType(typeof(DocumentTypeDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Get(int id, RequestDto requestDto)
        {
            var request = new DefaultIntRequestDto(id, requestDto);

            var response = await _documentTypeAppService.GetByIdAsync(request);

            return CreateResponseOnGet(response, EntityNames.DocumentType);
        }

        [HttpGet("allbynaturalpersontype")]
        [ThexAuthorize("PMS_DocumentType_Get_AllByNaturalPersonType")]
        [ProducesResponseType(typeof(IListDto<DocumentTypeDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> GetAllByNaturalPersonType([FromQuery]RequestAllDto request)
        {
            var response = await _documentTypeAppService.GetAllByNaturalPersonTypeAsync(request);

            return CreateResponseOnGetAll(response, EntityNames.DocumentType);
        }

        [HttpGet("personType/{personType}/countryCode/{countryCode}")]
        [ThexAuthorize("PMS_DocumentType_Get_PersonType_CountryCode")]
        [ProducesResponseType(typeof(IListDto<DocumentTypeDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> GetAllByPersonTypeAndCountryCode([FromQuery]RequestAllDto request, string personType, string countryCode)
        {
            var response = await _documentTypeAppService.GetAllByFiltersAsync(request, personType, countryCode);

            return CreateResponseOnGetAll(response, EntityNames.DocumentType);
        }

        [HttpGet("getallbypropertycountrycode")]
        [ThexAuthorize("PMS_DocumentType_Get_PersonType_CountryCode")]
        [ProducesResponseType(typeof(IListDto<DocumentTypeDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> GetAllByPropertyCountryCode([FromQuery]RequestAllDto request)
        {
            var response = await _documentTypeAppService.GetAllByCountryCodeAsync(request);

            return CreateResponseOnGetAll(response, EntityNames.DocumentType);
        }
    }
}
