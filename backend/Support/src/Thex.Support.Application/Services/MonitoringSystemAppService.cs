﻿using System.Threading.Tasks;
using Thex.Support.Application.Interfaces;
using Thex.Support.Dto;
using Thex.Support.Infra.Interfaces.ReadRepositories;
using Tnf.Application.Services;
using Tnf.Dto;
using Tnf.Notifications;
using Tnf.Repositories.Uow;

namespace Thex.Support.Application.Services
{
    public class MonitoringSystemAppService : ApplicationService, IMonitoringSystemAppService
    {
        private readonly IMonitoringSystemReadRepository _MonitoringSystemReadRepository;
        
        public MonitoringSystemAppService(
            IMonitoringSystemReadRepository MonitoringSystemReadRepository,
            IUnitOfWorkManager unitOfWorkManager,
            INotificationHandler notificationHandler)
            : base(notificationHandler)
        {
            _MonitoringSystemReadRepository = MonitoringSystemReadRepository;
        }

       

        Task<IListDto<MonitoringSystemDto>> IMonitoringSystemAppService.GetAllAsync()
        {
            throw new System.NotImplementedException();
        }
    }
}
