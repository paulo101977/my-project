﻿using System.Threading.Tasks;
using Thex.Common;
using Thex.Common.Enumerations;
using Thex.Support.Application.Interfaces;
using Thex.Support.Dto;
using Thex.Support.Infra.Interfaces.ReadRepositories;
using Tnf.Application.Services;
using Tnf.Dto;
using Tnf.Notifications;
using Tnf.Repositories.Uow;

namespace Thex.Support.Application.Services
{
    public class DocumentTypeAppService : ApplicationService, IDocumentTypeAppService
    {
        private IDocumentTypeReadRepository _documentTypeReadRepository;
        private INotificationHandler _notificationHandler;

        public DocumentTypeAppService(
            IDocumentTypeReadRepository documentTypeReadRepository,
            IUnitOfWorkManager unitOfWorkManager,
            INotificationHandler notificationHandler)
            : base(notificationHandler)
        {
            _documentTypeReadRepository = documentTypeReadRepository;
            _notificationHandler = notificationHandler;
        }

        public virtual async Task<DocumentTypeDto> GetByIdAsync(DefaultIntRequestDto request)
        {
            if (!ValidateRequestDto(request) || !ValidateId<int>(request.Id)) return null;

            if (Notification.HasNotification())
                return DocumentTypeDto.NullInstance;

            return await _documentTypeReadRepository.GetByIdAsync(request).ConfigureAwait(false);
        }

        public async Task<bool> CheckHealthStatus(RequestAllDto request)
                   => await _documentTypeReadRepository.CheckHealthStatus(request);

        public async Task<IListDto<DocumentTypeDto>> GetAllByPropertyAsync(RequestAllDto request)
                   => await _documentTypeReadRepository.GetAllByPropertyAsync(request);

        public async Task<IListDto<DocumentTypeDto>> GetAllGlobalAsync(RequestAllDto request)
                   => await _documentTypeReadRepository.GetAllGlobalAsync(request);

        public async Task<IListDto<DocumentTypeDto>> GetAllByNaturalPersonTypeAsync(RequestAllDto request)
                  => await _documentTypeReadRepository.GetAllByNaturalPersonTypeAsync(request);

        public async Task<IListDto<DocumentTypeDto>> GetAllByFiltersAsync(RequestAllDto request, string personType, string countryCode)
        {
            if (personType.Length != 1)
            {
                _notificationHandler.DefaultBuilder
                                        .WithMessage(AppConsts.LocalizationSourceName, CommonsEnum.Error.ParameterInvalid)
                                        .Build();

                return null;
            }

            return await _documentTypeReadRepository.GetAllByFiltersAsync(request, (PersonTypeEnum)personType.ToUpper()[0], countryCode);
        }


        public async Task<IListDto<DocumentTypeDto>> GetAllByCountryCodeAsync(RequestAllDto request)
            => await _documentTypeReadRepository.GetAllByCountryCodeAsync(request);

        public async Task<IListDto<DocumentTypeDto>> GetAllAsync()
                   => await _documentTypeReadRepository.GetAllAsync();
    }
}
