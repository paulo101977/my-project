﻿using System.Threading.Tasks;
using Thex.Support.Application.Interfaces;
using Thex.Support.Dto;
using Thex.Support.Infra.Interfaces.ReadRepositories;
using Tnf.Application.Services;
using Tnf.Dto;
using Tnf.Notifications;
using Tnf.Repositories.Uow;

namespace Thex.Support.Application.Services
{
    public class TransportationTypeAppService : ApplicationService, ITransportationTypeAppService
    {
        private readonly ITransportationTypeReadRepository _transportationTypeReadRepository;
        
        public TransportationTypeAppService(
            ITransportationTypeReadRepository transportationTypeReadRepository,
            IUnitOfWorkManager unitOfWorkManager,
            INotificationHandler notificationHandler)
            : base(notificationHandler)
        {
            _transportationTypeReadRepository = transportationTypeReadRepository;
        }

        public async Task<IListDto<TransportationTypeDto>> GetAllAsync()
            => await _transportationTypeReadRepository.GetAllAsync();
    }
}
