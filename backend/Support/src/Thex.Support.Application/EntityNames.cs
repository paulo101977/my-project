﻿namespace Thex.Support.Application
{
    public partial class EntityNames
    {
        public const string TransportationType = "TransportationType";
        public const string DocumentType = "DocumentType";
        public const string MonitoringSystem = "MonitoringSystem";
        public const string Health = "Health";
    }
}
