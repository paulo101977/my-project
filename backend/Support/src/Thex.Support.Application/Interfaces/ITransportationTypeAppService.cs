﻿using System.Threading.Tasks;
using Thex.Support.Dto;
using Tnf.Application.Services;
using Tnf.Dto;

namespace Thex.Support.Application.Interfaces
{
    public interface ITransportationTypeAppService : IApplicationService
    {
        Task<IListDto<TransportationTypeDto>> GetAllAsync();
    }
}
