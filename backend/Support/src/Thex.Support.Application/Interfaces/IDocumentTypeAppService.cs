﻿using System.Threading.Tasks;
using Thex.Support.Dto;
using Tnf.Application.Services;
using Tnf.Dto;

namespace Thex.Support.Application.Interfaces
{
    public interface IDocumentTypeAppService : IApplicationService
    {
        Task<bool> CheckHealthStatus(RequestAllDto request);
        Task<IListDto<DocumentTypeDto>> GetAllByPropertyAsync(RequestAllDto request);
        Task<IListDto<DocumentTypeDto>> GetAllGlobalAsync(RequestAllDto request);
        Task<DocumentTypeDto> GetByIdAsync(DefaultIntRequestDto id);
        Task<IListDto<DocumentTypeDto>> GetAllByNaturalPersonTypeAsync(RequestAllDto request);
        Task<IListDto<DocumentTypeDto>> GetAllByFiltersAsync(RequestAllDto request, string personType, string countryCode);
        Task<IListDto<DocumentTypeDto>> GetAllByCountryCodeAsync(RequestAllDto request);
        Task<IListDto<DocumentTypeDto>> GetAllAsync();
    }
}
