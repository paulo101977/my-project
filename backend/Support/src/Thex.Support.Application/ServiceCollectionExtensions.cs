﻿using Thex.Support.Application.Interfaces;
using Thex.Support.Application.Services;

namespace Microsoft.Extensions.DependencyInjection
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddApplicationServiceDependency(this IServiceCollection services)
        {
            services.AddInfraDependency();

            services.AddTransient<ITransportationTypeAppService, TransportationTypeAppService>();
            services.AddTransient<IDocumentTypeAppService, DocumentTypeAppService>();
            services.AddTransient<IMonitoringSystemAppService, MonitoringSystemAppService>();

            return services;
        }
    }
}