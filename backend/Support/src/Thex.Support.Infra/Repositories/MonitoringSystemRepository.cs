﻿using System.Threading.Tasks;
using Tnf.Dapper.Repositories;
using Tnf.Repositories;
using Tnf.Localization;
using Thex.Support.Infra.Context;
using Thex.Support.Infra.Entities;
using Thex.Support.Infra.Interfaces;
using System;
using Thex.Support.Dto;
using Thex.Kernel;

namespace Thex.Support.Infra.Repositories
{
    public class MonitoringSystemRepository : DapperEfRepositoryBase<ThexSupportContext, MonitoringSystem>, IMonitoringSystemRepository
    {
        private readonly ILocalizationManager _localizationManager;
        private readonly IApplicationUser _applicationUser;

        public MonitoringSystemRepository(
            IActiveTransactionProvider activeTransactionProvider,
            ILocalizationManager localizationManager,
            IApplicationUser applicationUser)
            : base(activeTransactionProvider)
        {
            _localizationManager = localizationManager;
            _applicationUser = applicationUser;
        }

        public Task InsertItem(Guid id, TransportationType obj)
        {
            throw new NotImplementedException();
        }

        public Task UpdateItem(Guid id, TransportationType obj)
        {
            throw new NotImplementedException();
        }
    }
}
