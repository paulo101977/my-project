﻿using System.Threading.Tasks;
using Tnf.Dapper.Repositories;
using Tnf.Repositories;
using Tnf.Localization;
using Thex.Support.Infra.Context;
using Thex.Support.Infra.Entities;
using Thex.Support.Infra.Interfaces;
using System;
using Thex.Support.Dto;
using Thex.Kernel;

namespace Thex.Support.Infra.Repositories
{
    public class TransportationTypeRepository : DapperEfRepositoryBase<ThexSupportContext, TransportationType>, ITransportationTypeRepository
    {
        private readonly ILocalizationManager _localizationManager;
        private readonly IApplicationUser _applicationUser;

        public TransportationTypeRepository(
            IActiveTransactionProvider activeTransactionProvider,
            ILocalizationManager localizationManager,
            IApplicationUser applicationUser)
            : base(activeTransactionProvider)
        {
            _localizationManager = localizationManager;
            _applicationUser = applicationUser;
        }

        public async Task UpdateItem(Guid id, TransportationType obj)
        {
            await UpdateAsync(obj);
        }
    }
}
