﻿using Microsoft.Extensions.Caching.Distributed;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Thex.Common;
using Thex.Common.Enumerations;
using Thex.Kernel;
using Thex.Support.Dto;
using Thex.Support.Infra.Caching;
using Thex.Support.Infra.Context;
using Thex.Support.Infra.Entities;
using Thex.Support.Infra.Interfaces.ReadRepositories;
using Tnf.Dapper.Repositories;
using Tnf.Dto;
using Tnf.Localization;
using Tnf.Repositories;

namespace Thex.Support.Infra.Repositories.ReadRepositories
{
    public class DocumentTypeReadRepository : DapperEfRepositoryBase<ThexSupportContext, DocumentType>, IDocumentTypeReadRepository
    {
        private readonly ILocalizationManager _localizationManager;
        private readonly IDistributedCache _cache;
        private readonly IApplicationUser _applicationUser;

        private static string _cacheKey = "AllDocumentTypeKey";
        private static string _cacheKeyByProperty = "AllDocumentTypeByPropertyKey";
        private static string _cacheKeyByCountry = "AllDocumentTypeByPropertyKey";

        public DocumentTypeReadRepository(IActiveTransactionProvider activeTransactionProvider,
                                          ILocalizationManager localizationManager,
                                          IDistributedDocumentTypeCache cache,
                                          IApplicationUser applicationUser)
            : base(activeTransactionProvider)
        {
            _localizationManager = localizationManager;
            _cache = cache;
            _applicationUser = applicationUser;
        }

        private string GetKey()
            => $"{_cacheKey}_{CultureInfo.CurrentCulture.Name ?? AppConsts.DEFAULT_CULTURE_INFO_NAME}";

        private string GetKeyByProperty()
            => $"{_cacheKeyByProperty}-{_applicationUser.PropertyId}_{CultureInfo.CurrentCulture.Name ?? AppConsts.DEFAULT_CULTURE_INFO_NAME}";

        private string GetKeyByCountry(string countryCode)
            => $"{_cacheKeyByCountry}_{countryCode}_{CultureInfo.CurrentCulture.Name ?? AppConsts.DEFAULT_CULTURE_INFO_NAME}";

        private void SetCacheByCountry(List<DocumentTypeDto> items, string countryCode)
        {
            var options = new DistributedCacheEntryOptions();

            options.SetAbsoluteExpiration(TimeSpan.FromSeconds(ThexSupportCacheConstants.DOCUMENT_EXPIRATION));

            _cache.SetString(GetKeyByCountry(countryCode), JsonConvert.SerializeObject(items), options);
        }

        private void SetCacheByProperty(List<DocumentTypeDto> items)
        {
            if (items.Any())
            {
                var options = new DistributedCacheEntryOptions();

                options.SetAbsoluteExpiration(TimeSpan.FromSeconds(ThexSupportCacheConstants.DOCUMENT_EXPIRATION));

                _cache.SetString(GetKeyByProperty(), JsonConvert.SerializeObject(items), options);
            }            
        }

        private void SetCache(List<DocumentTypeDto> items)
        {
            var options = new DistributedCacheEntryOptions();

            options.SetAbsoluteExpiration(TimeSpan.FromSeconds(ThexSupportCacheConstants.DOCUMENT_EXPIRATION));

            _cache.SetString(GetKey(), JsonConvert.SerializeObject(items), options);
        }

        private string GetBaseQueryWithPropertyId()
        {
            return @"SELECT D.DOCUMENTTYPEID AS ID,
                                       D.PERSONTYPE,
                                       D.COUNTRYSUBDIVISIONID,
	                                   D.NAME,
	                                   D.STRINGFORMATMASK,
	                                   D.REGEXVALIDATIONEXPRESSION,
	                                   D.ISMANDATORY
                                FROM DOCUMENTTYPE D
                                JOIN PROPERTYDOCUMENTTYPE P
                                ON D.DOCUMENTTYPEID = P.DOCUMENTTYPEID
                                AND P.PROPERTYID = @PropertyId";
        }

        private string GetBaseQueryWithCountrySubdivision()
        {
            return @"SELECT D.DOCUMENTTYPEID AS ID,
                            D.PERSONTYPE,
                            D.COUNTRYSUBDIVISIONID,
	                        D.NAME,
	                        D.STRINGFORMATMASK,
	                        D.REGEXVALIDATIONEXPRESSION,
	                        D.ISMANDATORY
                      FROM DOCUMENTTYPE D
                      LEFT JOIN COUNTRYSUBDIVISION C ON
                      D.COUNTRYSUBDIVISIONID = C.COUNTRYSUBDIVISIONID
                      WHERE C.COUNTRYTWOLETTERISOCODE = @CountryCode OR D.COUNTRYSUBDIVISIONID IS NULL";
        }

        private string GetBaseQuery()
        {
            return @" SELECT D.DOCUMENTTYPEID AS ID,
                                       D.PERSONTYPE,
                                       D.COUNTRYSUBDIVISIONID,
	                                   D.NAME,
	                                   D.STRINGFORMATMASK,
	                                   D.REGEXVALIDATIONEXPRESSION,
	                                   D.ISMANDATORY
                                FROM DOCUMENTTYPE D";
        }

        private async Task<bool> CheckHealthStatusWithCache(RequestAllDto request)
        {
            var ret = !string.IsNullOrWhiteSpace(await _cache.GetStringAsync(GetKeyByProperty()));

            ret = (await QueryAsync<DocumentTypeDto>(GetBaseQueryWithPropertyId(),
            new
            {
                PropertyId = _applicationUser.PropertyId
            })).FirstOrDefault() != null;

            return true;
        }

        private async Task<IList<DocumentTypeDto>> GetAllUsingCacheAsync(RequestAllDto request)
        {
            Func<Task<List<DocumentTypeDto>>> func = async () =>
            {
                var baseQuery = string.IsNullOrEmpty(_applicationUser.PropertyId) ? GetBaseQuery() : GetBaseQueryWithPropertyId();

                var items = (await QueryAsync<DocumentTypeDto>(baseQuery, new {
                    _applicationUser.PropertyId
                })).ToList();

                SetCacheByProperty(items);

                return items;
            };

            var cacheKey = string.IsNullOrEmpty(_applicationUser.PropertyId) ? GetKey() : GetKeyByProperty();

            return await BaseMethod(cacheKey, func);
        }

        private async Task<IList<DocumentTypeDto>> GetAllGlobalUsingCacheAsync(RequestAllDto request)
        {
            Func<Task<List<DocumentTypeDto>>> func = async () =>
            {
                var items = (await QueryAsync<DocumentTypeDto>(GetBaseQuery())).ToList();

                SetCache(items);

                return items;
            };

            var result = await BaseMethod(GetKey(), func);

            return result.FindAll(x => x.CountrySubdivisionId == null);
        }

        private async Task<DocumentTypeDto> GetUsingCacheAsync(DefaultIntRequestDto request)
        {
            Func<Task<List<DocumentTypeDto>>> func = async () =>
            {
                var items = (await QueryAsync<DocumentTypeDto>(GetBaseQuery())).ToList();

                SetCache(items);

                return items;
            };

            var result = await BaseMethod(GetKey(), func);

            return result.FirstOrDefault(x => x.Id == request.Id);
        }

        private async Task<IList<DocumentTypeDto>> GetAllByFiltersUsingCacheAsync(RequestAllDto request)
        {
            Func<Task<List<DocumentTypeDto>>> func = async () =>
            {
                var items = (await QueryAsync<DocumentTypeDto>(GetBaseQueryWithPropertyId(), new {
                            PropertyId = _applicationUser.PropertyId
                        })).ToList();

                SetCacheByProperty(items);

                return items;
            };
            
            var result = await BaseMethod(GetKeyByProperty(), func);

            return result.FindAll(x => x.PersonType.Contains(((char)PersonTypeEnum.Natural).ToString()));
        }

        private async Task<IList<DocumentTypeDto>> GetAllByCountryCodeUsingCacheAsync(RequestAllDto request)
        {
            Func<Task<List<DocumentTypeDto>>> func = async () =>
            {
                var items = (await QueryAsync<DocumentTypeDto>(GetBaseQueryWithCountrySubdivision(), new {
                                    CountryCode = _applicationUser.PropertyCountryCode
                                })).ToList();

                SetCacheByCountry(items, _applicationUser.PropertyCountryCode);

                return items;
            };

            return await BaseMethod(GetKeyByCountry(_applicationUser.PropertyCountryCode), func);
        }

        private async Task<IList<DocumentTypeDto>> GetAllByFiltersUsingCacheAsync(RequestAllDto request, PersonTypeEnum personType, string countryCode)
        {
            Func<Task<List<DocumentTypeDto>>> func = async () =>
            {
                var items = (await QueryAsync<DocumentTypeDto>(GetBaseQueryWithCountrySubdivision(), new {
                                    CountryCode = countryCode
                                })).ToList();

                SetCacheByCountry(items, countryCode);

                return items;
            };

            var result = await BaseMethod(GetKeyByCountry(countryCode), func);

            return result.FindAll(x => x.PersonType.Contains(((char)PersonTypeEnum.Natural).ToString()));
        }

        public async Task<IListDto<DocumentTypeDto>> GetAllByPropertyAsync(RequestAllDto request)
            => new ListDto<DocumentTypeDto>
            {
                HasNext = false,
                Items = await GetAllUsingCacheAsync(request)
            };

        public async Task<IListDto<DocumentTypeDto>> GetAllGlobalAsync(RequestAllDto request)
           => new ListDto<DocumentTypeDto>
           {
               HasNext = false,
               Items = await GetAllGlobalUsingCacheAsync(request)
           };

        public async Task<DocumentTypeDto> GetByIdAsync(DefaultIntRequestDto id)
                                          => await GetUsingCacheAsync(id);

        public async Task<IListDto<DocumentTypeDto>> GetAllByFiltersAsync(RequestAllDto request, PersonTypeEnum personType, string countryCode)
           => new ListDto<DocumentTypeDto>
           {
               HasNext = false,
               Items = await GetAllByFiltersUsingCacheAsync(request, personType, countryCode)
           };

        public async Task<IListDto<DocumentTypeDto>> GetAllByNaturalPersonTypeAsync(RequestAllDto request)
            => new ListDto<DocumentTypeDto>
            {
                HasNext = false,
                Items = await GetAllByFiltersUsingCacheAsync(request)
            };

        public async Task<IListDto<DocumentTypeDto>> GetAllByCountryCodeAsync(RequestAllDto request)
            => new ListDto<DocumentTypeDto>
            {
                HasNext = false,
                Items = await GetAllByCountryCodeUsingCacheAsync(request)
            };

        public async Task<bool> CheckHealthStatus(RequestAllDto request)
              => await CheckHealthStatusWithCache(request);

        public new async Task<IListDto<DocumentTypeDto>> GetAllAsync()
            => new ListDto<DocumentTypeDto>
            {
                HasNext = false,
                Items = await GetAllUsingCacheAsync(null)
            };

        private async Task<List<DocumentTypeDto>> BaseMethod(string cacheKey, Func<Task<List<DocumentTypeDto>>> method)
        {
            var items = new List<DocumentTypeDto>();
            var documentTypeCache = await _cache.GetStringAsync(cacheKey);

            if (!string.IsNullOrWhiteSpace(documentTypeCache))
                items = JsonConvert.DeserializeObject<List<DocumentTypeDto>>(documentTypeCache);
            else
                items = await method();

            return items;
        }

    }
}
