﻿using System;
using System.Threading.Tasks;
using Tnf.Dapper.Repositories;
using Tnf.Repositories;
using System.Linq;
using Tnf.Localization;
using Thex.Common;
using Thex.Support.Infra.Context;
using Thex.Support.Infra.Entities;
using Thex.Support.Infra.Interfaces.ReadRepositories;
using Thex.Support.Dto;
using Thex.Common.Enumerations;
using Tnf.Dto;
using Newtonsoft.Json;
using Microsoft.Extensions.Caching.Distributed;
using System.Globalization;
using System.Collections.Generic;
using Thex.Support.Infra.Caching;

namespace Thex.Support.Infra.Repositories.ReadRepositories
{
    public class TransportationTypeReadRepository : DapperEfRepositoryBase<ThexSupportContext, TransportationType>, ITransportationTypeReadRepository
    {
        private readonly ILocalizationManager _localizationManager;
        private readonly IDistributedCache _cache;
        private static string _cacheKey = "AllTransportationTypesKey";

        public TransportationTypeReadRepository(
            IActiveTransactionProvider activeTransactionProvider,
            ILocalizationManager localizationManager,
            IDistributedTransportationTypeCache cache)
            : base(activeTransactionProvider)
        {
            _localizationManager = localizationManager;
            _cache = cache;
        }

        public new async Task<IListDto<TransportationTypeDto>> GetAllAsync()
            => new ListDto<TransportationTypeDto>
            {
                HasNext = false,
                Items = await GetAllUsingCacheAsync()
            };

        private async Task<List<TransportationTypeDto>> GetAllUsingCacheAsync()
        {
            var items = new List<TransportationTypeDto>();
            var transportationTypeCache = await _cache.GetStringAsync(GetKey());

            if (!string.IsNullOrWhiteSpace(transportationTypeCache))
                items = JsonConvert.DeserializeObject<List<TransportationTypeDto>>(transportationTypeCache);
            else
            {
                items = (await QueryAsync<TransportationTypeDto>(@"
                    select 
                        t.TransportationTypeId as Id, 
                        t.TransportationTypeName as TransportationTypeName
                    from TransportationType t
                ")).ToList();

                foreach (var item in items)
                    item.TransportationTypeName = TranslateName(item.Id);

                SetCache(items);
            }

            return items;
        }

        private string TranslateName(int id)
            => _localizationManager.GetString(AppConsts.LocalizationSourceName, ((TransportationTypeEnum)id).ToString());

        private string GetKey()
            => $"{_cacheKey}_{CultureInfo.CurrentCulture.Name ?? AppConsts.DEFAULT_CULTURE_INFO_NAME}";

        private void SetCache(List<TransportationTypeDto> items)
        {
            var options = new DistributedCacheEntryOptions();

            options.SetAbsoluteExpiration(TimeSpan.FromSeconds(ThexSupportCacheConstants.TRANSPORTATION_EXPIRATION));

            _cache.SetString(GetKey(), JsonConvert.SerializeObject(items), options);
        }
    }
}
