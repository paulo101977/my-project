﻿using Microsoft.Extensions.Configuration;
using Thex.Support.Infra.Caching;

namespace Microsoft.Extensions.DependencyInjection
{
    public static class CacheCollectionExtensions
    {
        public static IServiceCollection AddAllCacheConfig(this IServiceCollection services, IConfiguration configuration)
        {
            services
                .AddCacheConfig(configuration);

            return services;
        }

        public static IServiceCollection AddCacheConfig(this IServiceCollection services, IConfiguration configuration)
        {
            services.Configure<TransportationTypeCacheOptions>(config =>
            {
                config.Configuration = configuration[ThexSupportCacheConstants.TRANSPORTATION_TYPE_CACHE_CONNECTION_STRING];
                config.InstanceName = ThexSupportCacheConstants.TRANSPORTATION_TYPE_CACHE_INSTANCE_NAME;
            });

            services.Configure<DocumentTypeCacheOptions>(config =>
            {
                config.Configuration = configuration[ThexSupportCacheConstants.DOCUMENT_TYPE_CACHE_CONNECTION_STRING];
                config.InstanceName = ThexSupportCacheConstants.DOCUMENT_TYPE_CACHE_INSTANCE_NAME;
            });

            services.Add(ServiceDescriptor.Singleton<IDistributedTransportationTypeCache, TransportationTypeCache>());
            services.Add(ServiceDescriptor.Singleton<IDistributedDocumentTypeCache, DocumentTypeCache>());

            return services;
        }
    }
}
