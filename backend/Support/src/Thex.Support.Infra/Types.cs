﻿using Thex.Common;
using Tnf.Configuration;
using Tnf.Localization;
using Tnf.Localization.Dictionaries;

namespace Thex.Support.Infra
{
    public enum Operation
    {
        Insert,
        Update,
        Delete
    }
}
