﻿namespace Thex.Support.Infra.Caching
{
    public class ThexSupportCacheConstants
    {
        public const string TRANSPORTATION_TYPE_CACHE_CONNECTION_STRING = "ConnectionStrings:Redis:TransportationType";
        public const string TRANSPORTATION_TYPE_CACHE_INSTANCE_NAME = "TransportationType";
        public const int TRANSPORTATION_EXPIRATION = 10080;

        public const string DOCUMENT_TYPE_CACHE_CONNECTION_STRING = "ConnectionStrings:Redis:DocumentType";
        public const string DOCUMENT_TYPE_CACHE_INSTANCE_NAME = "DocumentType";
        public const int DOCUMENT_EXPIRATION = 10080;

        public const string MONITORING_SYSTEM_CACHE_CONNECTION_STRING = "ConnectionStrings:Redis:MonitoringSystem";
        public const string MONITORING_SYSTEM_CACHE_INSTANCE_NAME = "MonitoringSystem";
        public const int SYSTEM_EXPIRATION = 10080;

    }
}
