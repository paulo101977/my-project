﻿using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Caching.Redis;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Text;

namespace Thex.Support.Infra.Caching
{
    public interface IDistributedDocumentTypeCache : IDistributedCache
    {
    }

    public class DocumentTypeCache : RedisCache, IDistributedDocumentTypeCache
    {
        public DocumentTypeCache(IOptions<DocumentTypeCacheOptions> optionsAccessor) : base(optionsAccessor)
        {

        }
    }

    public class DocumentTypeCacheOptions : RedisCacheOptions
    {
    }
}