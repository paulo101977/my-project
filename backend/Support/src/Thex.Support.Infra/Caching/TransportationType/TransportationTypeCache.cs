﻿using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Caching.Redis;
using Microsoft.Extensions.Options;

namespace Thex.Support.Infra.Caching
{
    public interface IDistributedTransportationTypeCache : IDistributedCache
    {
    }

    public class TransportationTypeCache : RedisCache, IDistributedTransportationTypeCache
    {
        public TransportationTypeCache(IOptions<TransportationTypeCacheOptions> optionsAccessor) : base(optionsAccessor)
        {
        }
    }

    public class TransportationTypeCacheOptions : RedisCacheOptions
    {
    }
}
