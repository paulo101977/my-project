﻿using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Caching.Redis;
using Microsoft.Extensions.Options;

namespace Thex.Support.Infra.Caching
{
    public interface IDistributedMonitoringSystemCache : IDistributedCache
    {
    }

    public class MonitoringSystemCache : RedisCache, IDistributedMonitoringSystemCache
    {
        public MonitoringSystemCache(IOptions<MonitoringSystemCacheOptions> optionsAccessor) : base(optionsAccessor)
        {
        }
    }

    public class MonitoringSystemCacheOptions : RedisCacheOptions
    {
    }
}
