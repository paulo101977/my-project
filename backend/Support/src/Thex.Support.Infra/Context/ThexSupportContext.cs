﻿using Microsoft.EntityFrameworkCore;
using Tnf.EntityFrameworkCore;
using Tnf.Runtime.Session;

namespace Thex.Support.Infra.Context
{
    public class ThexSupportContext : TnfDbContext
    {
        public ThexSupportContext(DbContextOptions<ThexSupportContext> options, ITnfSession session)
            : base(options, session)
        {
        }
    }
}
