﻿using Thex.Common;
using Thex.Support.Infra;
using Thex.Support.Infra.Context;
using Thex.Support.Infra.Interfaces;
using Thex.Support.Infra.Interfaces.ReadRepositories;
using Thex.Support.Infra.Mappers.DapperMappers;
using Thex.Support.Infra.Mappers;
using Thex.Support.Infra.Repositories;
using Thex.Support.Infra.Repositories.ReadRepositories;
using Tnf.Dapper;

namespace Microsoft.Extensions.DependencyInjection
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddInfraDependency(this IServiceCollection services)
        {
            services
                .AddCommonDependency()
                .AddTnfEntityFrameworkCore()
                .AddTnfDbContext<ThexSupportContext>(config => DbContextConfigurer.Configure(config))
                .AddTnfDapper(options =>
                {
                    options.MapperAssemblies.Add(typeof(TransportationTypeMapper).Assembly);
                    options.DbType = DapperDbType.SqlServer;
                });

            services.AddTransient<ITransportationTypeReadRepository, TransportationTypeReadRepository>();
            services.AddTransient<ITransportationTypeRepository, TransportationTypeRepository>();

            services.AddTransient<IDocumentTypeReadRepository, DocumentTypeReadRepository>();

            return services;
        }
    }
}
