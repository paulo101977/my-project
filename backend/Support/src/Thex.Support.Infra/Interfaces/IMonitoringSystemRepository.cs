﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.Support.Dto;
using Thex.Support.Infra.Entities;
using Tnf.Dto;

namespace Thex.Support.Infra.Interfaces
{
    public interface IMonitoringSystemRepository
    {

        Task InsertItem(Guid id, TransportationType obj);
        Task UpdateItem(Guid id, TransportationType obj);
    }
}
