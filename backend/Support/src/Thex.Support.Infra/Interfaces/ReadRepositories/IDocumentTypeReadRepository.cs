﻿using System.Threading.Tasks;
using Thex.Common.Enumerations;
using Thex.Support.Dto;
using Tnf.Dto;

namespace Thex.Support.Infra.Interfaces.ReadRepositories
{
    public interface IDocumentTypeReadRepository
    {
        Task<bool> CheckHealthStatus(RequestAllDto request);     
        Task<IListDto<DocumentTypeDto>> GetAllByPropertyAsync(RequestAllDto request);
        Task<IListDto<DocumentTypeDto>> GetAllGlobalAsync(RequestAllDto request);
        Task<DocumentTypeDto> GetByIdAsync(DefaultIntRequestDto id);
        Task<IListDto<DocumentTypeDto>> GetAllByNaturalPersonTypeAsync(RequestAllDto request);
        Task<IListDto<DocumentTypeDto>> GetAllByFiltersAsync(RequestAllDto request, PersonTypeEnum personType, string countryCode);
        Task<IListDto<DocumentTypeDto>> GetAllByCountryCodeAsync(RequestAllDto request);
        Task<IListDto<DocumentTypeDto>> GetAllAsync();
    }
}
