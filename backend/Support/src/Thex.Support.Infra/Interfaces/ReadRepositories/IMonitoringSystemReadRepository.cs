﻿using System.Threading.Tasks;
using Thex.Support.Dto;
using Tnf.Dto;

namespace Thex.Support.Infra.Interfaces.ReadRepositories
{
    public interface IMonitoringSystemReadRepository
    {
        Task<IListDto<MonitoringSystemDto>> GetAllAsync();
    }
}
