﻿using System.Threading.Tasks;
using Thex.Support.Dto;
using Tnf.Dto;

namespace Thex.Support.Infra.Interfaces.ReadRepositories
{
    public interface ITransportationTypeReadRepository
    {
        Task<IListDto<TransportationTypeDto>> GetAllAsync();
    }
}
