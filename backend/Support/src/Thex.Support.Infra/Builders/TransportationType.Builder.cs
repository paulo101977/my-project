﻿using Tnf.Builder;
using Tnf.Notifications;
using Tnf.Specifications;
using Thex.Common;

namespace Thex.Support.Infra.Entities
{
    public partial class TransportationType
    {
        public class Builder : Builder<TransportationType>
        {
            public Builder(INotificationHandler handler) : base(handler)
            {
            }

            public Builder(INotificationHandler handler, TransportationType instance) : base(handler, instance)
            {
            }

            public virtual Builder WithId(int id)
            {
                Instance.Id = id;
                return this;
            }
            public virtual Builder WithTransportationTypeName(string transportationTypeName)
            {
                Instance.TransportationTypeName = transportationTypeName;
                return this;
            }

            protected override void Specifications()
            {
            }
        }
    }
}
