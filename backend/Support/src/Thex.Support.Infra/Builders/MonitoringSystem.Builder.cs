﻿using Tnf.Builder;
using Tnf.Notifications;
using Tnf.Specifications;
using Thex.Common;
using System;

namespace Thex.Support.Infra.Entities
{
    public partial class MonitoringSystem
    {
        public class Builder : Builder<MonitoringSystem>
        {
            public Builder(INotificationHandler handler) : base(handler)
            {
            }

            public Builder(INotificationHandler handler, MonitoringSystem instance) : base(handler, instance)
            {
            }

            public virtual Builder WithId(int id)
            {
                Instance.Id = id;
                return this;
            }

            public virtual Builder WithPropertyId(Guid propertyUId)
            {
                Instance.PropertyUid = propertyUId;
                return this;
            }

            public virtual Builder WithRoutine(string routine)
            {
                Instance.Routine = routine;
                return this;
            }

            public virtual Builder WithIsWizard(bool isWizard)
            {
                Instance.IsWizard = isWizard;
                return this;
            }

            public virtual Builder WithIsCompletedWizard(bool IsCompletedWizard)
            {
                Instance.IsWizard = IsCompletedWizard;
                return this;
            }

            public virtual Builder WithFinishedUserId(Guid finishedUserId)
            {
                Instance.FinishedUserId = finishedUserId;
                return this;
            }
                         

            protected override void Specifications()
            {
                AddSpecification(new ExpressionSpecification<MonitoringSystem>(
                AppConsts.LocalizationSourceName,
                MonitoringSystem.EntityError.MonitoringSystemMustHaveRoutine,
                w => !string.IsNullOrWhiteSpace(w.Routine)));

                AddSpecification(new ExpressionSpecification<MonitoringSystem>(
                    AppConsts.LocalizationSourceName,
                    MonitoringSystem.EntityError.MonitoringSystemOutOfBoundRoutine,
                    w => string.IsNullOrWhiteSpace(w.Routine) || w.Routine.Length > 0 && w.Routine.Length <= 60));
            }
        }
    }
}
