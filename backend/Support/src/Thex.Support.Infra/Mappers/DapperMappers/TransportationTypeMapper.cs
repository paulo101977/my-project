﻿using DapperExtensions.Mapper;
using Thex.Support.Infra.Entities;

namespace Thex.Support.Infra.Mappers.DapperMappers
{
    public sealed class TransportationTypeMapper : ClassMapper<TransportationType>
    {
        public TransportationTypeMapper()
        {
            Table("TransportationType");
            Map(e => e.Id).Column("Id").Key(KeyType.Assigned);
            AutoMap();
        }
    }
}
