﻿using DapperExtensions.Mapper;
using Thex.Support.Infra.Entities;

namespace Thex.Support.Infra.Mappers.DapperMappers
{
    public sealed class MonitoringSystemMapper : ClassMapper<MonitoringSystem>
    {
        public MonitoringSystemMapper()
        {
            Table("MonitoringSystem");
            Map(e => e.Id).Column("Id").Key(KeyType.Assigned);
            AutoMap();
        }
    }
}
