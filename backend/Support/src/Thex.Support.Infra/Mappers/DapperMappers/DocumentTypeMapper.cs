﻿using DapperExtensions.Mapper;
using Thex.Support.Infra.Entities;

namespace Thex.Support.Infra.Mappers.DapperMappers
{
    public sealed class DocumentTypeMapper : ClassMapper<DocumentType>
    {
        public DocumentTypeMapper()
        {
            Table("DocumentType");
            Map(e => e.Id).Column("DOCUMENTTYPEID").Key(KeyType.Assigned);
            AutoMap();
        }
    }
}
