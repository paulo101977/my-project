﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Thex.Support.Infra.Entities
{
    public partial class DocumentType : IEntityInt
    {
        public int Id { get; set; }

        public int? CountrySubdivisionId { get; internal set; }
        public string Name { get; internal set; }
        public string StringFormatMask { get; internal set; }
        public string RegexValidationExpression { get; internal set; }
        public bool IsMandatory { get; internal set; }

        public enum EntityError
        {
            DocumentTypeMustHavePersonType,
            DocumentTypeOutOfBoundPersonType,
            DocumentTypeMustHaveCountrySubdivisionId,
            DocumentTypeMustHaveName,
            DocumentTypeOutOfBoundName,
            DocumentTypeOutOfBoundStringFormatMask,
            DocumentTypeOutOfBoundRegexValidationExpression
        }
    }
}
