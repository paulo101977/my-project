﻿using System;

namespace Thex.Support.Infra.Entities
{
    public interface IEntityGuid : IEntity
    {
        Guid Id { get; set; }
    }

    public interface IEntityInt : IEntity
    {
        int Id { get; set; }
    }

    public interface IEntityLong : IEntity
    {
        long Id { get; set; }
    }

    public interface IEntityShort : IEntity
    {
        short Id { get; set; }
    }


    public interface IEntity
    {
    }
}
