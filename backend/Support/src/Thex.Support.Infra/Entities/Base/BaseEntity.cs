﻿using System;

namespace Thex.Support.Infra.Entities
{
    public class BaseEntity : ThexFullAuditedEntity, IEntityGuid
    {
        public Guid Id { get; set; }
    }
}
