﻿namespace Thex.Support.Infra.Entities
{
    public partial class TransportationType : IEntityInt
    {
        public int Id { get; set; }
        public string TransportationTypeName { get; internal set; }

        public enum EntityError
        {
            TransportationTypeMustHaveTransportationTypeName,
            TransportationTypeOutOfBoundTransportationTypeName
        }
    }
}
