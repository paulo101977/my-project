﻿using System;
using Tnf.Notifications;

namespace Thex.Support.Infra.Entities
{
    public partial class MonitoringSystem : ThexFullAuditedEntity
    {
        public int Id { get; internal set; }
        public Guid PropertyUid { get; internal set; }
        public string Routine { get; internal set; }
        public bool IsWizard { get; internal set; }
        public bool IsCompletedWizard { get; internal set; }
        public Guid FinishedUserId { get; internal set; }




        public enum EntityError
        {
            MonitoringSystemMustHaveRoutine,
            MonitoringSystemOutOfBoundRoutine
        }

        public static Builder Create(INotificationHandler handler)
            => new Builder(handler);

        public static Builder Create(INotificationHandler handler, MonitoringSystem instance)
            => new Builder(handler, instance);
    }
}
