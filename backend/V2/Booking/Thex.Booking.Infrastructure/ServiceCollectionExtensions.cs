﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Thex.Booking.Domain.Interfaces;
using Thex.Booking.Infra.Context;
using Thex.Booking.Infra.Mappers.Profiles;
using Thex.Booking.Infra.Repositories.ReadRepositories;
using Tnf.Dapper;

namespace Microsoft.Extensions.DependencyInjection
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddInfraDependency(this IServiceCollection services, IConfiguration configuration)
        {
            services
                .AddTnfEntityFrameworkCore()
                .AddTnfDbContext<ThexReservationContext>((config) =>
                {
                    if (config.ExistingConnection != null)
                        config.DbContextOptions.UseSqlServer(config.ExistingConnection);
                    else
                        config.DbContextOptions.UseSqlServer(configuration[$"ConnectionStrings:SqlServer"]);

                });
            
            services.AddTnfAutoMapper(config =>
            {
                config.AddProfile<ReservationProfile>();
            });

            //Read Repositories
            services.AddTransient<IReservationReadRepository, ReservationReadRepository>();

            //Repositories
            services.AddTransient<IReservationRepository, ReservationRepository>();

            services.AddTnfMemoryCache();

            return services;
        }
    }
}
