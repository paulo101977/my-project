﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;

namespace Thex.Booking.Infra.Mappers
{
    public class ReservationBudgetMapper : IEntityTypeConfiguration<Domain.ReservationBudget>
    {
        public void Configure(EntityTypeBuilder<Domain.ReservationBudget> mapper)
        {
            mapper.ToTable("ReservationBudget");

            mapper.HasKey(e => e.Id)
                .HasName("Id");

            mapper.Property(e => e.Day)
                .IsRequired()
                .HasColumnName("Day");

            mapper.Property(e => e.Budget)
                .IsRequired()
                .HasColumnName("Budget");

            mapper.HasOne(d => d.Reservation)
                .WithMany(p => p.BudgetList)
                .HasForeignKey(d => d.ReservationId)
                .HasConstraintName("FK_ReservationBudget_Reservation");
        }
    }
}

