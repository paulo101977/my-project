﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;

namespace Thex.Booking.Infra.Mappers
{
    public class ReservationMapper : IEntityTypeConfiguration<Domain.Reservation>
    {
        public void Configure(EntityTypeBuilder<Domain.Reservation> mapper)
        {
            mapper.ToTable("Reservation");

            mapper.HasAnnotation("Relational:TableName", "Reservation");

            mapper.HasKey(e => e.Id)
                    .HasName("Id");

            mapper.Property(e => e.GuestName)
                .IsRequired()
                .HasMaxLength(150)
                .HasColumnName("GuestName");

            mapper.Property(e => e.Code)
                .IsRequired()
                .HasMaxLength(150)
                .HasColumnName("Code");

            mapper.Property(e => e.RoomCode)
                .IsRequired()
                .HasMaxLength(150)
                .HasColumnName("RoomCode");

            mapper.Property(e => e.NumberOfGuests)
                .IsRequired()
                .HasColumnName("NumberOfGuests");

            mapper.OwnsOne(p => p.LengthOfPeriod, a =>
                {
                    a.Property(p => p.Checkin)
                    .IsRequired()
                    .HasColumnName("Checkin");

                    a.Property(p => p.Checkout)
                    .IsRequired()
                    .HasColumnName("Checkout");
                });

            mapper.Ignore(e => e.NumberOfNights);
        }
    }
}

