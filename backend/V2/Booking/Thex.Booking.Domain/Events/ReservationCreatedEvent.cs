﻿using MediatR;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Thex.Booking.Domain.Interfaces;
using Thex.Booking.Domain.Notification;

namespace Thex.Booking.Domain.Events
{
    public class ReservationCreatedEvent : INotificationHandler<ReservationCreatedNotification>
    {
        private IReservationRepository _reservationRepository { get; set; }

        public ReservationCreatedEvent(IReservationRepository reservationRepository)
        {
            _reservationRepository = reservationRepository;
        }

        public async Task Handle(ReservationCreatedNotification notification, CancellationToken cancellationToken)
        {
            await Task.Run(() =>
            {
                _reservationRepository.FakeReservationDataSync(notification.Reservation);
            });
        }
    }
}
