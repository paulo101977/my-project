﻿using System;
using System.Collections.Generic;
using Thex.Booking.Domain.Command;
using Tnf.Bus.Client;

namespace Thex.Booking.Domain.Events
{
    public class ReservationCreatedCashOperationEvent : Message
    {
        public Guid ReservationId { get; set; }
        public string GuestName { get; set; }
        public string RoomCode { get; set; }
        public decimal Total { get; set; }

        public ReservationCreatedCashOperationEvent()
        {
        }

        public ReservationCreatedCashOperationEvent(Guid reservationId, string guestName, string roomCode, decimal total)
        {
            ReservationId = reservationId;
            GuestName = guestName;
            RoomCode = roomCode;
            Total = total;
        }
    }
}

