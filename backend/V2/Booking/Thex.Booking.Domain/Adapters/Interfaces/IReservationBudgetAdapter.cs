﻿using System;

namespace Thex.Booking.Domain
{
    internal interface IReservationBudgetAdapter
    {
        ReservationBudget.Builder CreateMap(Guid reservationId, DateTime day, decimal budget);
    }
}
