﻿using Thex.Booking.Domain.Command;

namespace Thex.Booking.Domain
{
    internal interface IReservationAdapter
    {
        Reservation.Builder CreateMap(CreateReservationCommand command);
    }
}
