﻿using MediatR;
using System;
using System.Collections.Generic;
using Tnf.Dto;

namespace Thex.Booking.Domain.Command
{
    public class CreateReservationCommand : BaseDto, IRequest<CreateReservationCommand>
    {
        public string GuestName { get; set; }
        public string RoomCode { get; set; }
        public int NumberOfGuests { get; set; }
        public DateTime Checkin { get; set; }
        public DateTime Checkout { get; set; }
        public List<BudgetDto> BudgetList { get; set; }
    }

    public class BudgetDto
    {
        public DateTime Day { get; set; }
        public decimal Budget { get; set; }
    }
}
