﻿using MediatR;
using Thex.Common;
using Tnf.Localization;

namespace Thex.Booking.Domain.Notification
{
    public class ReservationCreatedGuestEmailNotification : INotification
    {
        public string GuestName { get; private set; }

        public ReservationCreatedGuestEmailNotification(string guestName)
        {
            GuestName = guestName;
        }

        public string ToString(ILocalizationManager localizationManager)
            => string.Format(
                    localizationManager.GetString(AppConsts.LocalizationSourceName, EmailNotificationEnum.EmailGuest.ToString()),
                    GuestName);
    }
}