﻿using MediatR;
using Thex.Common;
using Tnf.Localization;

namespace Thex.Booking.Domain.Notification
{
    public class ReservationCreatedAdminEmailNotification : INotification
    {
        public string RoomCode { get; set; }
        public string GuestName { get; set; }

        public ReservationCreatedAdminEmailNotification(string roomCode, string guestName)
        {
            RoomCode = roomCode;
            GuestName = guestName;
        }

        public string ToString(ILocalizationManager localizationManager)
            => string.Format(localizationManager.GetString(AppConsts.LocalizationSourceName, EmailNotificationEnum.EmailAdministrator.ToString()),
                RoomCode,
                GuestName);
    }
}