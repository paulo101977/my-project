﻿namespace Thex.Booking.Domain
{
    public enum EmailNotificationEnum
    {
        EmailAdministrator = 1,
        EmailGuest = 2
    }
}
