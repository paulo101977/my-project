﻿using MediatR;
using System.Threading;
using System.Threading.Tasks;
using Thex.Common;
using Thex.Booking.Domain.Command;
using Thex.Booking.Domain.Interfaces;
using Tnf.Notifications;
using Tnf.Repositories.Uow;
using Tnf.Bus.Client;
using Thex.Booking.Domain.Notification;

namespace Thex.Booking.Domain.Handlers
{
    internal class CreateReservationHandler : IRequestHandler<CreateReservationCommand, CreateReservationCommand>
    {
        private readonly IReservationRepository _reservationRepository;
        private readonly IReservationReadRepository _reservationReadRepository;
        private readonly IReservationAdapter _reservationAdapter;
        private readonly INotificationHandler _notification;
        private readonly IMediator _mediator;
        private readonly IUnitOfWorkManager _unitOfWorkManager;

        public CreateReservationHandler(
            IMediator mediator,
            IUnitOfWorkManager unitOfWorkManager,
            IReservationRepository reservationRepository,
            IReservationReadRepository reservationReadRepository,
            IReservationAdapter reservationAdapter, 
            INotificationHandler notification)
        {
            _mediator = mediator;
            _unitOfWorkManager = unitOfWorkManager;
            _reservationRepository = reservationRepository;
            _reservationReadRepository = reservationReadRepository;
            _reservationAdapter = reservationAdapter;
            _notification = notification;
        }

        public async Task<CreateReservationCommand> Handle(CreateReservationCommand command, CancellationToken cancellationToken)
        {
            await CheckAvailability(command);

            if (_notification.HasNotification())
                return command;

            using (var uow = _unitOfWorkManager.Begin())
            {
                var reservationBuilder = _reservationAdapter.CreateMap(command);

                var reservation = reservationBuilder.Build();

                if (_notification.HasNotification())
                    return command;

                await _reservationRepository.Create(reservation);

                reservation.AddReservationCreatedEvent(reservation);

                await uow.CompleteAsync();

                await PublishEvents(reservation);
            }

            return command;
        }

        private async Task PublishEvents(Reservation reservation)
        {
            foreach (var @event in reservation.Events)
                await _mediator.Publish((dynamic)@event);

            foreach (var @message in reservation.Messages)
                await @message.Publish();
        }

        private async Task CheckAvailability(CreateReservationCommand command)
        {
            if (await _reservationReadRepository.CheckAvailabilityByFilters(command.RoomCode, command.Checkin, command.Checkout))
            {
                _notification.Raise(_notification
                .DefaultBuilder
                .WithMessage(AppConsts.LocalizationSourceName, Reservation.EntityError.RoomNotAvailable)
                .Build());
            }
        }
    }
}
