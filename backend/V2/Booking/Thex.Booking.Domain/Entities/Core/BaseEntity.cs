﻿using MediatR;
using System.Collections.Generic;
using Tnf.Bus.Queue.Interfaces;

namespace Thex.Booking.Domain.Entities
{
    public class BaseEntity
    {
        private readonly List<INotification> _events = new List<INotification>();

        public IReadOnlyList<INotification> Events => _events;

        protected void AddEvent(INotification @event)
        {
            _events.Add(@event);
        }

        private readonly List<IMessage> _message = new List<IMessage>();

        public IReadOnlyList<IMessage> Messages => _message;

        protected void AddMessage(IMessage @message)
        {
            _message.Add(@message);
        }
    }
}
