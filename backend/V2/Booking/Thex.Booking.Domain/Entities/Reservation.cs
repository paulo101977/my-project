﻿using System;
using System.Collections.Generic;
using System.Linq;
using Thex.Booking.Domain.Command;
using Thex.Booking.Domain.Entities;
using Thex.Booking.Domain.Events;
using Thex.Booking.Domain.Notification;
using Tnf.Notifications;

namespace Thex.Booking.Domain
{
    public partial class Reservation : BaseEntity
    {
        public Reservation()
        {
            BudgetList = new HashSet<ReservationBudget>();
        }

        static internal Guid GenerateId()
         => Guid.NewGuid();

        public Guid Id { get; private set; }
        public string GuestName { get; private set; }
        public string Code { get; private set; }
        public string RoomCode { get; private set; }
        public int NumberOfGuests { get; private set; }
        public int NumberOfNights { get; private set; }
        public Period LengthOfPeriod { get; private set; }
        public ICollection<ReservationBudget> BudgetList { get; private set; }

        internal void GenerateReservationNumber()
        {
            long i = Guid.NewGuid()
                         .ToByteArray()
                         .Aggregate<byte, long>(1, (current, b) => current * (b + 1));

            Code = $"{i - LengthOfPeriod.Checkin.Ticks:x}";
        }

        internal void AddBudget(ReservationBudget reservationBudget)
        {
            if (BudgetList == null)
                BudgetList = new List<ReservationBudget>();

            if (BudgetList.Contains(reservationBudget))
                throw new Exception();

            BudgetList.Add(reservationBudget);
        }

        internal void AddReservationCreatedGuestEmailEvent()
        {
            AddEvent(new ReservationCreatedGuestEmailNotification(GuestName));
        }

        internal void AddReservationCreatedAdminEmailEvent()
        {
            AddEvent(new ReservationCreatedAdminEmailNotification(GuestName, RoomCode));
        }

        internal void AddReservationCreatedCashOperationEvent()
        {
            AddMessage(new ReservationCreatedCashOperationEvent(
                Id,
                GuestName, 
                RoomCode, 
                BudgetList.Sum(e => e.Budget)
            ));
        }

        internal void AddReservationCreatedEvent(Reservation reservation)
        {
            AddEvent(new ReservationCreatedNotification(reservation));
        }

        public enum EntityError
        {
            ReservationMustHaveCode,
            ReservationOutOfBoundCode,
            ReservationMustHaveGuestName,
            ReservationMustRoomCode,
            ReservationOutOfBoundRoomCode,
            ReservationOutOfBoundGuestName,
            ReservationMustHaveNumberOfGuests,
            ReservationMustHaveId,
            RoomNotAvailable
        }

        protected internal static Builder Create(INotificationHandler handler)
            => new Builder(handler);

        protected internal static Builder Create(INotificationHandler handler, Reservation instance)
            => new Builder(handler, instance);
    }
}
