﻿using System;
using System.Collections.Generic;
using System.Text;
using Thex.Common;
using Tnf.Notifications;

namespace Thex.Booking.Domain.Entities
{
    public class Period : ValueObject
    {
        private Period() { }

        private Period(DateTime checkin, DateTime checkout, INotificationHandler notification)
        {
            if (checkin == DateTime.MinValue || checkout == DateTime.MinValue || checkin > checkout)
                InvalidPeriod(notification);

            Checkin = checkin;
            Checkout = checkout;
        }

        public static Period Create(DateTime checkin, DateTime checkout, INotificationHandler notification)
        {
            return new Period(checkin, checkout, notification);
        }

        public DateTime Checkin { get; private set; }
        public DateTime Checkout { get; private set; }

        public static int EvaluateNumberOfNights(Period period)
        {
            return (period.Checkout - period.Checkin).Days;
        }

        protected override IEnumerable<object> GetEqualityComponents()
        {
            yield return Checkin;
            yield return Checkout;
        }

        public override string ToString()
        {
            return $"{Checkin} - {Checkout}";
        }

        private void InvalidPeriod(INotificationHandler notification)
        {
            notification.Raise(notification
                            .DefaultBuilder
                            .WithMessage(AppConsts.LocalizationSourceName, EntityError.InvalidPeriodOfCheckinAndCheckout)
                            .Build());
        }

        public enum EntityError
        {
            InvalidPeriodOfCheckinAndCheckout
        }
    }
}
