﻿using MediatR;
using System.Reflection;
using Thex.Booking.Domain;
using Thex.Booking.Domain.Command;

namespace Microsoft.Extensions.DependencyInjection
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddDomainDependency(this IServiceCollection services)
        {
            services.AddTransient<IReservationAdapter, ReservationAdapter>();
            services.AddTransient<IReservationBudgetAdapter, ReservationBudgetAdapter>();

            services.ConfigureMediatR();
            services.AddTnfBusClient();

            return services;
        }

        private static void ConfigureMediatR(this IServiceCollection services)
        {
            services.AddMediatR(typeof(CreateReservationCommand).GetTypeInfo().Assembly);
        }
    }
}
