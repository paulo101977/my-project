﻿using System;
using Tnf.Repositories;
using System.Threading.Tasks;

namespace Thex.Booking.Domain.Interfaces
{
    public interface IReservationRepository : IRepository
    {
        Task<Domain.Reservation> Create(Domain.Reservation reservation);
        Task FakeReservationDataSync(Domain.Reservation reservation);
        
    }
}
