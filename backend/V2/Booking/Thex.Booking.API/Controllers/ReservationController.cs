﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Thex.Booking.Application.Interfaces;
using Thex.Booking.Domain.Command;
using Thex.Booking.Dto;
using Tnf.AspNetCore.Mvc.Response;
using Tnf.Dto;

namespace Thex.Booking.Web.Controllers
{
    //[Authorize]
    [Route(RouteConsts.Reservation)]
    public class ReservationController : TnfController
    {
        private readonly IReservationAppService _reservationAppService;

        public ReservationController(IReservationAppService reservationAppService)
        {
            _reservationAppService = reservationAppService;
        }

        [HttpGet]
        [ProducesResponseType(typeof(IListDto<ReservationDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Get()
            => CreateResponseOnGetAll(await _reservationAppService.GetAll(), RouteResponseConsts.Reservation);

        [HttpGet("{id}")]
        [ProducesResponseType(typeof(ReservationDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Get(Guid id)
            => CreateResponseOnGet(await _reservationAppService.GetById(id), RouteResponseConsts.Reservation);

        [HttpPost]
        [ProducesResponseType(typeof(CreateReservationCommand), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> Post([FromBody] CreateReservationCommand command)
        {
            var response = await _reservationAppService.CreateReservation(command);

            return CreateResponseOnPost(response, RouteResponseConsts.Reservation);
        }
    }
}