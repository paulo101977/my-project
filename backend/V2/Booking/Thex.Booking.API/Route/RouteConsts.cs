﻿namespace Thex.Booking.Web
{
    public class RouteConsts
    {
        public const string Reservation = "api/reservation";
        public const string RoomList = "api/roomList";
    }
}
