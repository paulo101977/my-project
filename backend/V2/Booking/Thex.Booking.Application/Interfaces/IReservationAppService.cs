﻿using Tnf.Application.Services;
using Thex.Booking.Domain.Command;
using System.Threading.Tasks;
using Thex.Booking.Dto;
using System;
using Tnf.Dto;

namespace Thex.Booking.Application.Interfaces
{
    public interface IReservationAppService : IApplicationService
    {
        Task<IListDto<ReservationDto>> GetAll();
        Task<ReservationDto> GetById(Guid id);
        Task<CreateReservationCommand> CreateReservation(CreateReservationCommand command);
    }
}
