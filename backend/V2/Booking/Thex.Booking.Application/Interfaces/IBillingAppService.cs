﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Thex.Booking.Dto;
using Tnf.Application.Services;

namespace Thex.Booking.Application.Interfaces
{
    public interface IBillingAppService : IApplicationService
    {
        Task<IEnumerable<CreditCardDto>> GetCreditCardPayment(int propertyId, DateTime itemDate);   
    }
}
