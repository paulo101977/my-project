﻿using System.Threading.Tasks;
using Tnf.Notifications;
using Thex.Booking.Application.Interfaces;
using Thex.Booking.Domain.Command;
using MediatR;
using Thex.Booking.Domain.Interfaces;
using Thex.Booking.Dto;
using System;
using Tnf.Dto;

namespace Thex.Booking.Application.Services
{
    public class ReservationAppService : ApplicationServiceBase, IReservationAppService
    {
        private IMediator _mediator { get; set; }
        private IReservationReadRepository _reservationReadRepository { get; set; }

        public ReservationAppService(
            INotificationHandler notification, 
            IMediator mediator, 
            IReservationReadRepository reservationReadRepository) 
            : base(notification)
        {
            _mediator = mediator;
            _reservationReadRepository = reservationReadRepository;
        }

        public async Task<IListDto<ReservationDto>> GetAll()
            => await _reservationReadRepository.GetAll();

        public async Task<ReservationDto> GetById(Guid id)
            => await _reservationReadRepository.GetById(id);

        public Task<CreateReservationCommand> CreateReservation(CreateReservationCommand command)
        {
            var response = _mediator.Send(command);

            if (Notification.HasNotification())
                return null;

            return response;
        }
    }
}
