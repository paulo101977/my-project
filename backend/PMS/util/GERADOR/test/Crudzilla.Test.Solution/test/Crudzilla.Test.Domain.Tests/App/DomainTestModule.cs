﻿using Crudzilla.Test.EntityFrameworkCore;
using Crudzilla.Test.Mapper;
using Tnf.App.EntityFrameworkCore;
using Tnf.App.TestBase;
using Tnf.Modules;

namespace Crudzilla.Test.Domain.Tests.App
{
    [DependsOn(
        typeof(DomainModule),
        typeof(MapperModule),
        typeof(EntityFrameworkModule),
        typeof(TnfAppTestBaseModule))]
    public class DomainTestModule : TnfModule
    {
        public override void PreInitialize()
        {
            Configuration
                .TnfEfCoreInMemory()
                .RegisterDbContextInMemory<CrudzillaContext>();
        }

        public override void Initialize()
        {
            base.Initialize();

            IocManager.RegisterAssemblyByConvention<DomainTestModule>();
        }
    }
}
