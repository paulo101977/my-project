using Crudzilla.Test.EntityFrameworkCore;
using Tnf.App.EntityFrameworkCore;
using Tnf.App.TestBase;
using Tnf.Modules;

namespace Crudzilla.Test.Application.Tests.App
{
    [DependsOn(
        typeof(AppModule),
        typeof(TnfAppTestBaseModule))]
    public class ApplicationTestModule : TnfModule
    {
        public override void PreInitialize()
        {
            Configuration
                .TnfEfCoreInMemory()
                .RegisterDbContextInMemory<CrudzillaContext>();
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention<ApplicationTestModule>();
        }
    }
}