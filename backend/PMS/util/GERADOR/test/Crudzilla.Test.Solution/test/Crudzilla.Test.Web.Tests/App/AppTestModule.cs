﻿using Crudzilla.Test.Application;
using Crudzilla.Test.EntityFrameworkCore;
using System;
using Tnf.App.AspNetCore.TestBase;
using Tnf.App.EntityFrameworkCore;
using Tnf.Modules;

namespace Crudzilla.Test.Web.Tests.App
{
    [DependsOn(
        typeof(AppModule),
        typeof(TnfAppAspNetCoreTestBaseModule))]
    public class AppTestModule : TnfModule
    {
        public override void PreInitialize()
        {
            Configuration
                .TnfEfCoreInMemory(IocManager.Resolve<IServiceProvider>())
                .RegisterDbContextInMemory<CrudzillaContext>();
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention<AppTestModule>();
        }
    }
}
