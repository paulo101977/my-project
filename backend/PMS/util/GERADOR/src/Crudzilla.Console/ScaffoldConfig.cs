﻿using Crudzilla.Configuration;
using System.Collections.Generic;

namespace Crudzilla.Console
{
    public class TestScaffoldConfig
    {
        public int NumberOfEntitiesGenerated { get; set; } = 10;
        public string WebApi { get; set; }
        public string ApplicationService { get; set; }
        public string Domain { get; set; }
    }

    public class ScaffoldConfig
    {
        public ScaffoldConfig()
        {
            Localization = new LocalizationConfig();
            Test = new TestScaffoldConfig();
            Orm = new OrmConfig();
            TablesSelection = new List<TableMapConfig>();
        }

        public string ConnectionString { get; set; }
        public string SolutionPath { get; set; }

        public LocalizationConfig Localization { get; set; }
        public TestScaffoldConfig Test { get; set; }
        public string WebApi { get; set; }
        public string ApplicationService { get; set; }
        public string DataTransfer { get; set; }
        public string Entity { get; set; }
        public string Builder { get; set; }
        public OrmConfig Orm { get; set; }
        public string Adapter { get; set; }
        public string AutoMapper { get; set; }
        public List<TableMapConfig> TablesSelection { get; set; }

        public CrudzillaConfig GetFullConfiguration()
        {
            var scaffoldConfig = new CrudzillaConfig()
            {
                Adapter = new AdapterConfig(Adapter),
                ApplicationService = new ApplicationServiceConfig(ApplicationService),
                AutoMapper = new AutoMapperConfig(AutoMapper),
                Builder = new BuilderConfig(Builder),
                ConnectionString = ConnectionString,
                DataTransfer = new DataTransferConfig(DataTransfer),
                Entity = new EntityConfig(Entity),
                Localization = Localization,
                Orm = Orm,
                SolutionPath = SolutionPath,
                TablesSelection = TablesSelection,
                Test = new TestConfig()
                {
                    NumberOfEntitiesGenerated = Test.NumberOfEntitiesGenerated,
                    ApplicationService = new ApplicationServiceTestConfig(Test.ApplicationService),
                    Domain = new DomainTestConfig(Test.Domain),
                    WebApi = new WebApiTestConfig(Test.WebApi)
                },
                WebApi = new WebApiConfig(WebApi)
            };

            scaffoldConfig.Initialize();

            return scaffoldConfig;
        }
    }
}
