﻿using Crudzilla.Configuration;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Scaffolding.Internal;

namespace Crudzilla
{
    internal static class DataTransferExtensions
    {
        internal static string GetDataTransferInheritance(this CrudzillaConfig config, IEntityType entityType, ICSharpUtilities csharpUtilities)
        {
            Check.NotNull(config, nameof(config));
            Check.NotNull(entityType, nameof(entityType));
            Check.NotNull(csharpUtilities, nameof(csharpUtilities));

            var tnfDtoDefaultInheritance = string.Empty;

            var keyType = entityType.GetEntityKeyType(csharpUtilities);

            //tnfDtoDefaultInheritance = $"DtoBase<{keyType}>";
            tnfDtoDefaultInheritance = $"BaseDto";
            
            return tnfDtoDefaultInheritance;
        }

        internal static string GetDataTransferFilterInheritance(this CrudzillaConfig config)
            => "RequestAllDto";

        internal static string GetDtoName(this CrudzillaConfig config, IEntityType entityType)
        {
            Check.NotNull(config, nameof(config));
            Check.NotNull(entityType, nameof(entityType));

            var entityName = config.GetEntityNameOrDefault(entityType);
            return $"{entityName}Dto";
        }
    }
}
