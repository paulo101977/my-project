﻿using Crudzilla.Configuration;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Crudzilla
{
    internal static class DomainServiceExtensions
    {
        internal static string GetDomainServiceName(this CrudzillaConfig config, IEntityType entityType)
        {
            Check.NotNull(config, nameof(config));
            Check.NotNull(entityType, nameof(entityType));

            var entityName = config.GetEntityNameOrDefault(entityType);
            return $"{entityName}DomainService";
        }
    }
}
