﻿using Crudzilla.Configuration;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Scaffolding.Internal;
using Microsoft.EntityFrameworkCore.Storage;
using System;
using System.Collections.Generic;

namespace Crudzilla
{
    internal static class SpecificationsExtensions
    {
        internal static string NewLinePattern { get; } = $"{Environment.NewLine}\t\t\t\t";

        internal static List<string> GetSpecificationsExpression(this CrudzillaConfig config, IRelationalTypeMapper relationalTypeMapper, IEntityType entityType, IProperty property, ICSharpUtilities cSharpUtilities)
        {
            Check.NotNull(config, nameof(config));
            Check.NotNull(entityType, nameof(entityType));
            Check.NotNull(property, nameof(property));
            Check.NotNull(cSharpUtilities, nameof(cSharpUtilities));

            var entityName = config.GetEntityNameOrDefault(entityType);

            var propertyName = config.GetPropertyName(entityType, property);

            var mapping = relationalTypeMapper.FindMapping(property);
            var sb = new List<string>();

            if (property.ClrType == typeof(string))
            {
                var expressionResult = $"w => !string.IsNullOrWhiteSpace(w.{propertyName})";

                if (!property.IsColumnNullable())
                {
                    var specificationName = $"{entityName}{SpecificationTypes.MustHave}{propertyName}";
                    sb.Add($"AddSpecification(new ExpressionSpecification<{entityName}>({NewLinePattern}{config.Localization.ConstantClassName}, {NewLinePattern}{entityName}.EntityError.{specificationName}, {NewLinePattern}{expressionResult}));");
                }

                if (mapping.Size > 0)
                {
                    expressionResult = $"w => string.IsNullOrWhiteSpace(w.{propertyName})";

                    var specificationName = $"{entityName}{SpecificationTypes.OutOfBound}{propertyName}";
                    expressionResult = expressionResult + $" || w.{propertyName}.Length > 0 && w.{propertyName}.Length <= {mapping.Size}";
                    sb.Add($"AddSpecification(new ExpressionSpecification<{entityName}>({NewLinePattern}{config.Localization.ConstantClassName}, {NewLinePattern}{entityName}.EntityError.{specificationName}, {NewLinePattern}{expressionResult}));");
                }
            }

            if (property.ClrType == typeof(DateTime) ||
                property.ClrType == typeof(DateTime?))
            {
                var expressionResult = (property.ClrType == typeof(DateTime?)) ?
                    $"w => w.{propertyName} == null || " :
                    "w => ";

                // Min datetime from sql server compatibility: 1753-01-01
                var specificationName = $"{entityName}{SpecificationTypes.Invalid}{propertyName}";
                expressionResult = expressionResult + $"w.{propertyName} >= new DateTime(1753, 1, 1) && w.{propertyName} <= DateTime.MaxValue";

                sb.Add($"AddSpecification(new ExpressionSpecification<{entityName}>({NewLinePattern}{config.Localization.ConstantClassName}, {NewLinePattern}{entityName}.EntityError.{specificationName}, {NewLinePattern}{expressionResult}));");
            }

            if (property.IsForeignKey() && !property.IsColumnNullable())
            {
                var propertyType = cSharpUtilities.GetTypeName(property.ClrType);
                var expressionResult = $"w => w.{propertyName} != default({propertyType})";
                var specificationName = $"{entityName}{SpecificationTypes.MustHave}{propertyName}";

                sb.Add($"AddSpecification(new ExpressionSpecification<{entityName}>({NewLinePattern}{config.Localization.ConstantClassName}, {NewLinePattern}{entityName}.EntityError.{specificationName}, {NewLinePattern}{expressionResult}));");
            }

            return sb;
        }
    }
}
