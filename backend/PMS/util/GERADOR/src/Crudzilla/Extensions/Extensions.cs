﻿using Crudzilla.Configuration;
using Microsoft.EntityFrameworkCore.Metadata;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Xml.Linq;

namespace Crudzilla
{
    internal static class Extensions
    {
        internal static string GetEntityTypeConfigurationName(this CrudzillaConfig config, IEntityType entityType)
        {
            Check.NotNull(config, nameof(config));
            Check.NotNull(entityType, nameof(entityType));

            var entityName = config.GetEntityNameOrDefault(entityType);
            return $"{entityName}Mapper";
        }

        internal static string NormalizeString(this string value)
        {
            Check.NotNull(value, nameof(value));

            return value.Replace("_", string.Empty).ToLowerInvariant();
        }

        internal static string ToCamelCase(this string str, bool invariantCulture = true)
        {
            Check.NotNull(str, nameof(str));

            if (string.IsNullOrWhiteSpace(str))
                return str;

            if (str.Length == 1)
                return invariantCulture ? str.ToLowerInvariant() : str.ToLower();

            return (invariantCulture ? char.ToLowerInvariant(str[0]) : char.ToLower(str[0])) + str.Substring(1);
        }

        internal static string ToPascalCase(this string str, bool invariantCulture = true)
        {
            Check.NotNull(str, nameof(str));

            string result = string.Join("", str?.Select(c => Char.IsLetterOrDigit(c) ? c.ToString().ToLower() : "_").ToArray());

            var arr = result?
                .Split(new[] { '_' }, StringSplitOptions.RemoveEmptyEntries)
                .Select(s => $"{s.Substring(0, 1).ToUpper()}{s.Substring(1)}");

            result = string.Join("", arr);

            return result;
        }

        /// <remarks>
        /// Source: http://stackoverflow.com/a/14892813/1636276
        /// </remarks>
        internal static XElement GetElement(this XContainer container, string name)
        {
            Check.NotNull(name, nameof(name));

            if (container == null)
                return null;

            var element = container.Element(name);
            return element;
        }

        internal static string FixProjectName(this string projectName)
        {
            Check.NotNull(projectName, nameof(projectName));

            projectName = projectName.Contains(".csproj") ?
                projectName :
                $"{projectName}.csproj";

            return projectName;
        }

        internal static string GetNamespace(this FileInfo fileInfo)
        {
            Check.NotNull(fileInfo, nameof(fileInfo));

            var lines = File.ReadAllLines(fileInfo.FullName);

            var lineNamespace = lines.First(w => w.Contains("namespace"));
            lineNamespace = lineNamespace.Replace("namespace", string.Empty).Trim();

            return lineNamespace;
        }

        internal static string GetClassName(this FileInfo fileInfo)
        {
            Check.NotNull(fileInfo, nameof(fileInfo));

            var lines = File.ReadAllLines(fileInfo.FullName);

            var lineNamespace = lines.First(w => w.Contains("public class"));
            lineNamespace = lineNamespace.Replace("public class", string.Empty).Trim();
            lineNamespace = lineNamespace.Replace(": TnfModule", string.Empty).Trim();

            return lineNamespace;
        }

        internal static FileInfo FindFile(this FileInfo referenceFile, string fileName)
        {
            Check.NotNull(referenceFile, nameof(referenceFile));
            Check.NotNull(fileName, nameof(fileName));

            var projectFile = Directory.EnumerateFiles(referenceFile.Directory.FullName, fileName, SearchOption.AllDirectories).FirstOrDefault();
            if (!File.Exists(projectFile))
                throw new ArgumentException($"Coud not find {fileName}");

            return new FileInfo(projectFile);
        }

        public static bool IsNullableType(this Type type)
        {
            var typeInfo = type.GetTypeInfo();

            return !typeInfo.IsValueType
                   || typeInfo.IsGenericType
                   && typeInfo.GetGenericTypeDefinition() == typeof(Nullable<>);
        }

        internal static List<T> EliminateDuplicates<T>(this List<T> list)
        {
            HashSet<T> hashSet = new HashSet<T>();
            foreach (T item in list)
            {
                hashSet.Add(item);
            }
            return hashSet.ToList<T>();

        }

        internal static string UppercaseFirst(this string str)
        {
            if (string.IsNullOrEmpty(str))
                return string.Empty;
            return char.ToUpper(str[0]) + str.Substring(1).ToLower();
        }
    }
}
