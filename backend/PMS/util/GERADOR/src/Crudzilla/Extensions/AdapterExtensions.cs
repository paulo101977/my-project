﻿using Crudzilla.Configuration;
using JetBrains.Annotations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Crudzilla
{
    internal static class AdapterExtensions
    {
        internal static string GetAdapterName([NotNull] this CrudzillaConfig config, [NotNull] IEntityType entityType)
        {
            Check.NotNull(config, nameof(config));
            Check.NotNull(entityType, nameof(entityType));

            var entityName = config.GetEntityNameOrDefault(entityType);
            return $"{entityName}Adapter";
        }
    }
}
