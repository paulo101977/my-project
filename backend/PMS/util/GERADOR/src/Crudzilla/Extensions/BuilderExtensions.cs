﻿using Crudzilla.Configuration;
using JetBrains.Annotations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Crudzilla
{
    internal static class BuilderExtensions
    {
        internal static string GetBuilderName([NotNull]this CrudzillaConfig config, [NotNull] IEntityType entityType)
        {
            Check.NotNull(config, nameof(config));
            Check.NotNull(entityType, nameof(entityType));

            var entityName = config.GetEntityNameOrDefault(entityType);
            return $"{entityName}.Builder";
        }
    }
}
