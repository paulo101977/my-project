﻿using Crudzilla.Configuration;
using JetBrains.Annotations;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Scaffolding.Internal;
using System.Linq;

namespace Crudzilla.Writters
{
    internal class DataTransferWriter
    {
        private ICSharpUtilities CSharpUtilities { get; }
        private CrudzillaConfig Configuration { get; }

        private IndentedStringBuilder _sb;

        private string DefaultInheritance;
        private string DtoName;
        private string EntityKey;

        public DataTransferWriter(
            [NotNull] ICSharpUtilities cSharpUtilities,
            [NotNull] CrudzillaConfig configuration)
        {
            CSharpUtilities = Check.NotNull(cSharpUtilities, nameof(cSharpUtilities));
            Configuration = Check.NotNull(configuration, nameof(configuration));
        }

        public string WriteCodeScaffold([NotNull] IEntityType entityType, bool generateNavigationProperties)
        {
            Check.NotNull(entityType, nameof(entityType));

            //---
            DefaultInheritance = Configuration.GetDataTransferInheritance(entityType, CSharpUtilities);
            DtoName = Configuration.GetDtoName(entityType);
            EntityKey = entityType.GetEntityKeyType(CSharpUtilities);
            //---

            _sb = new IndentedStringBuilder();

            _sb.AppendLine(Configuration.HeaderGeneratedFile);

            _sb.AppendLine("using System;");
            _sb.AppendLine("using System.Collections.Generic;");
            _sb.AppendLine("using Tnf.Dto;");

            _sb.AppendLine();
            _sb.AppendLine($"namespace {Configuration.DataTransfer.Namespace}");
            _sb.AppendLine("{");
            using (_sb.Indent())
            {
                GenerateClass(entityType, generateNavigationProperties);
            }
            _sb.AppendLine("}");

            return _sb.ToString();
        }

        public void GenerateClass([NotNull] IEntityType entityType, bool generateNavigationProperties = false)
        {
            _sb.AppendLine($"public partial class {DtoName} : {DefaultInheritance}");
            _sb.AppendLine("{");
            using (_sb.Indent())
            {
                _sb.AppendLine($"public static {DtoName} NullInstance = null;");
                _sb.AppendLine();

                if (generateNavigationProperties) GenerateConstructor(entityType);

                _sb.AppendLine($"public {EntityKey} Id {{ get; set; }}");
                GenerateProperties(entityType);
                if (generateNavigationProperties) GenerateNavigationProperties(entityType);
            }
            _sb.AppendLine("}");
        }

        public void GenerateConstructor([NotNull] IEntityType entityType)
        {
            var collectionNavigations = entityType.GetNavigations().Where(n => n.IsCollection()).ToList();

            if (collectionNavigations.Count > 0)
            {
                _sb.AppendLine($"public {DtoName}()");
                _sb.AppendLine("{");
                using (_sb.Indent())
                {
                    foreach (var navigation in collectionNavigations)
                    {
                        var propertyName = Configuration.GetNavigationPropertyName(navigation);
                        var propertyType = Configuration.GetNavigationPropertyType(navigation);

                        _sb.AppendLine($"{propertyName}List = new HashSet<{propertyType}Dto>();");
                    }
                }
                _sb.AppendLine("}");
                _sb.AppendLine();
            }
        }


        public void GenerateProperties([NotNull] IEntityType entityType)
        {
            var properties = Configuration.GetOrderedProperties(entityType, CSharpUtilities);

            foreach (var property in properties)
            {
                var propertyName = Configuration.GetPropertyName(entityType, property);
                if (propertyName == "Id")
                    continue;

                _sb.AppendLine($"public {CSharpUtilities.GetTypeName(property.ClrType)} {propertyName} {{ get; set; }}");
            }

        }

        private void GenerateNavigationProperties(IEntityType entityType)
        {
            var sortedNavigations = entityType.GetNavigations()
                .OrderBy(n => n.IsDependentToPrincipal() ? 0 : 1)
                .ThenBy(n => n.IsCollection() ? 1 : 0);

            if (sortedNavigations.Any())
            {
                _sb.AppendLine();

                foreach (var navigation in sortedNavigations)
                {
                    var propertyName = Configuration.GetNavigationPropertyName(navigation);
                    var propertyType = Configuration.GetNavigationPropertyType(navigation);

                    propertyType = navigation.IsCollection() ? $"ICollection<{propertyType}Dto>" : $"{propertyType}Dto";
                    propertyName = navigation.IsCollection() ? $"{propertyName}List" : propertyName;

                    _sb.AppendLine($"public virtual {propertyType} {propertyName} {{ get; set; }}");
                }
            }
        }

        //public void GenerateNavigationProperties([NotNull] IEntityType entityType)
        //{
        //    var collectionNavigations = entityType.GetNavigations().Where(n => n.IsCollection()).ToList();

        //    if (collectionNavigations.Count > 0)
        //    {
        //        _sb.AppendLine();

        //        foreach (var navigation in collectionNavigations)
        //        {
        //            var propertyName = Configuration.GetNavigationPropertyName(navigation);
        //            var propertyType = Configuration.GetNavigationPropertyType(navigation);

        //            _sb.AppendLine($"public virtual ICollection<{propertyType}Dto> {propertyName}List {{ get; set; }}");
        //        }
        //    }
        //}

        public string WriteCode()
        {
            var sb = new IndentedStringBuilder();

            sb.AppendLine($"namespace {Configuration.DataTransfer.Namespace}");
            sb.AppendLine("{");
            using (sb.Indent())
            {
                sb.AppendLine($"public partial class {DtoName}");
                sb.AppendLine("{");
                sb.AppendLine("}");
            }
            sb.AppendLine("}");

            return sb.ToString();
        }

        public string WriteCodeRequestAllScaffold()
        {
            var sb = new IndentedStringBuilder();

            sb.AppendLine(Configuration.HeaderGeneratedFile);
            sb.AppendLine("using Tnf.Dto;");
            sb.AppendLine();
            sb.AppendLine($"namespace {Configuration.DataTransfer.Namespace}");
            sb.AppendLine("{");
            using (sb.Indent())
            {
                sb.AppendLine($"public partial class GetAll{DtoName} : RequestAllDto");
                sb.AppendLine("{");
                sb.AppendLine("}");
            }
            sb.AppendLine("}");

            return sb.ToString();
        }

        public string WriteCodeRequestAll()
        {
            var sb = new IndentedStringBuilder();

            sb.AppendLine($"namespace {Configuration.DataTransfer.Namespace}");
            sb.AppendLine("{");
            using (sb.Indent())
            {
                sb.AppendLine($"public partial class GetAll{DtoName}");
                sb.AppendLine("{");
                sb.AppendLine("}");
            }
            sb.AppendLine("}");

            return sb.ToString();
        }
    }
}
