﻿using Crudzilla.Configuration;
using JetBrains.Annotations;
using Microsoft.EntityFrameworkCore.Internal;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Scaffolding.Internal;
using System.Collections.Generic;
using System.Linq;

namespace Crudzilla.Writters
{
    internal class AutoMapperProfileWriter
    {
        private CrudzillaConfig Configuration { get; }
        private ICSharpUtilities CSharpUtilities { get; }
        private IndentedStringBuilder _sb;

        public AutoMapperProfileWriter(
            [NotNull] CrudzillaConfig configuration,
            [NotNull] ICSharpUtilities csharpUtilities)
        {
            Configuration = Check.NotNull(configuration, nameof(configuration));
            CSharpUtilities = Check.NotNull(csharpUtilities, nameof(csharpUtilities));
        }

        public string WriteCodeScaffold([NotNull] IEnumerable<IEntityType> entitiesType)
        {
            Check.NotNull(entitiesType, nameof(entitiesType));

            _sb = new IndentedStringBuilder();

            _sb.AppendLine(Configuration.HeaderGeneratedFile);
            _sb.AppendLine($"using {Configuration.Entity.Namespace};");
            _sb.AppendLine($"using {Configuration.DataTransfer.Namespace};");
            _sb.AppendLine();
            _sb.AppendLine($"namespace {Configuration.AutoMapper.Namespace}");
            _sb.AppendLine("{");
            using (_sb.Indent())
            {
                _sb.AppendLine($"public abstract class ScaffoldDomainToDtoProfile : AutoMapper.Profile");
                _sb.AppendLine("{");
                using (_sb.Indent())
                {
                    _sb.AppendLine("public ScaffoldDomainToDtoProfile()");
                    _sb.AppendLine("{");
                    using (_sb.Indent())
                    {
                        foreach (var entityType in entitiesType)
                        {
                            var entityName = Configuration.GetEntityNameOrDefault(entityType);

                            if (Configuration.IsValidEntity(entityType, CSharpUtilities))
                                _sb.AppendLine($"{entityName}Map();");
                        }
                    }
                    _sb.AppendLine("}");
                    _sb.AppendLine();

                    var last = entitiesType.Last();
                    foreach (var entityType in entitiesType)
                    {
                        var entityName = Configuration.GetEntityNameOrDefault(entityType);

                        if (Configuration.IsValidEntity(entityType, CSharpUtilities))
                        {
                            var methodName = $"{entityName}Map()";

                            _sb.AppendLine($"protected virtual AutoMapper.IMappingExpression<{entityName}, {entityName}Dto> {methodName}");
                            _sb.AppendLine("{");
                            using (_sb.Indent())
                            {
                                _sb.AppendLine($"return CreateMap<{entityName}, {entityName}Dto>();");
                            }
                            _sb.AppendLine("}");

                            if (last != entityType)
                                _sb.AppendLine();
                        }
                    }
                }
                _sb.AppendLine("}");
            }
            _sb.AppendLine("}");

            return _sb.ToString();
        }

        public string WriteCode([NotNull] IEnumerable<IEntityType> entitiesType)
        {
            Check.NotNull(entitiesType, nameof(entitiesType));

            var sb = new IndentedStringBuilder();

            sb.AppendLine($"using {Configuration.Entity.Namespace};");
            sb.AppendLine($"using {Configuration.DataTransfer.Namespace};");
            sb.AppendLine();
            sb.AppendLine($"namespace {Configuration.AutoMapper.Namespace}");
            sb.AppendLine("{");
            using (sb.Indent())
            {
                sb.AppendLine($"public class DomainToDtoProfile : ScaffoldDomainToDtoProfile");
                sb.AppendLine("{");
                using (sb.Indent())
                {
                    var last = entitiesType.Last();
                    foreach (var entityType in entitiesType)
                    {
                        var entityName = Configuration.GetEntityNameOrDefault(entityType);

                        if (Configuration.IsValidEntity(entityType, CSharpUtilities))
                        {
                            sb.AppendLine($"protected override AutoMapper.IMappingExpression<{entityName}, {entityName}Dto> {entityName}Map()");
                            sb.AppendLine("{");
                            using (sb.Indent())
                            {
                                sb.AppendLine($"var mapping = base.{entityName}Map();");
                                sb.AppendLine("return mapping;");
                            }
                            sb.AppendLine("}");

                            if (entityType != last)
                                sb.AppendLine();
                        }
                    }
                }
                sb.AppendLine("}");
            }
            sb.AppendLine("}");

            return sb.ToString();
        }
    }
}
