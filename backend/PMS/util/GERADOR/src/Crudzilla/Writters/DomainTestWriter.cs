﻿using Crudzilla.Configuration;
using JetBrains.Annotations;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Microsoft.EntityFrameworkCore.Scaffolding.Internal;
using Microsoft.EntityFrameworkCore.Storage;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Crudzilla.Writters
{
    internal class DomainTestWriter
    {
        private CrudzillaConfig Configuration { get; }
        private ICSharpUtilities CSharpUtilities { get; }
        private IRelationalTypeMapper RelationalTypeMapper { get; }

        private string PluralizedEntityName { get; set; }
        public string DomainServiceName { get; set; }
        public string BuilderName { get; set; }
        private string EntityName { get; set; }
        private string DtoName { get; set; }
        private string EntityKey { get; set; }

        public DomainTestWriter(
            [NotNull] CrudzillaConfig configuration,
            [NotNull] ICSharpUtilities cSharpUtilities,
            [NotNull] IRelationalTypeMapper relationalTypeMapper)
        {
            Configuration = Check.NotNull(configuration, nameof(configuration));
            CSharpUtilities = Check.NotNull(cSharpUtilities, nameof(cSharpUtilities));
            RelationalTypeMapper = Check.NotNull(relationalTypeMapper, nameof(relationalTypeMapper));
        }

        public string WriteCodeBuilderScaffold([NotNull] IEntityType entityType)
        {
            //--
            EntityName = Configuration.GetEntityNameOrDefault(entityType);
            DomainServiceName = Configuration.GetDomainServiceName(entityType);
            DtoName = Configuration.GetDtoName(entityType);
            BuilderName = Configuration.GetBuilderName(entityType);
            PluralizedEntityName = Configuration.GetPluralizedEntityNameOrDefault(entityType);
            EntityKey = entityType.GetEntityKeyType(CSharpUtilities);
            //--

            var sb = new IndentedStringBuilder();

            sb.AppendLine(Configuration.HeaderGeneratedFile);

            sb.AppendLine($"using {Configuration.Builder.Namespace};");
            sb.AppendLine($"using {Configuration.Entity.Namespace};");
            sb.AppendLine($"using {Configuration.Test.Domain.ModuleTestNamespace};");
            sb.AppendLine($"using System;");
            sb.AppendLine($"using System.Linq;");
            sb.AppendLine($"using Tnf.App.TestBase;");
            sb.AppendLine($"using Xunit;");
            sb.AppendLine($"using System.Collections.Generic;");
            sb.AppendLine();
            sb.AppendLine($"namespace {Configuration.Test.Domain.Namespace}");
            sb.AppendLine("{");
            using (sb.Indent())
            {
                sb.AppendLine($"[Collection(\"{BuilderName}\")]");
                sb.AppendLine($"public partial class {BuilderName}Tests : TestBaseWithLocalIocManager");
                sb.AppendLine("{");
                using (sb.Indent())
                {
                    WriteBuildValidEntityTest(sb, entityType);
                    WriteEmptyBuildEntityTest(sb, entityType);
                    WriteBuildInvalidEntityTest(sb, entityType);
                }
                sb.AppendLine("}");
            }
            sb.AppendLine("}");

            return sb.ToString();
        }

        private void WriteBuildValidEntityTest(IndentedStringBuilder sb, IEntityType entityType)
        {
            sb.AppendLine($"[Fact(DisplayName = \"Builder - Build valid {EntityName}\")]");
            sb.AppendLine($"public void BuildValid{EntityName}()");
            sb.AppendLine("{");
            using (sb.Indent())
            {
                sb.AppendLine("// Arrange");
                sb.AppendLine($"var builder = new {BuilderName}(LocalNotification)");

                using (sb.Indent())
                {
                    var keyType = entityType.GetEntityKeyType(CSharpUtilities);

                    if (!string.IsNullOrWhiteSpace(keyType))
                    {
                        var keys = entityType.GetKeys();
                        sb.AppendLine($".WithId({keys.First().Properties.First().ClrType.GetLiteralTypeRandomValue(RelationalTypeMapper)})");
                    }

                    var properties = Configuration.GetOrderedProperties(entityType, CSharpUtilities);

                    var lastProperty = properties.Last();
                    foreach (var property in properties)
                    {
                        var propertyName = Configuration.GetPropertyName(entityType, property);
                        if (propertyName == "Id" || propertyName == $"{EntityName}Id")
                            continue;

                        var end = (lastProperty == property) ? ";" : string.Empty;

                        var value = property.ClrType.GetLiteralTypeRandomValue(RelationalTypeMapper, property);
                        sb.AppendLine($".With{propertyName}({value}){end}");
                    }
                }

                sb.AppendLine();
                sb.AppendLine("// Act");
                sb.AppendLine("builder.Build();");

                sb.AppendLine();
                sb.AppendLine("// Assert");
                sb.AppendLine("Assert.False(LocalNotification.HasNotification(), string.Join(Environment.NewLine, LocalNotification.GetAll()));");
            }
            sb.AppendLine("}");
            sb.AppendLine();
        }

        private void WriteEmptyBuildEntityTest(IndentedStringBuilder sb, IEntityType entityType)
        {
            sb.AppendLine($"[Fact(DisplayName = \"Builder - Build empty {EntityName}\")]");
            sb.AppendLine($"public void BuildEmpty{EntityName}()");
            sb.AppendLine("{");
            using (sb.Indent())
            {
                sb.AppendLine("// Arrange");
                sb.AppendLine($"var emptyBuilder = new {BuilderName}(LocalNotification);");

                sb.AppendLine();
                sb.AppendLine("// Act");
                sb.AppendLine("emptyBuilder.Build();");

                var enumerationsErrors = Configuration.GetEntityErrorsEnumeration(RelationalTypeMapper, entityType, CSharpUtilities);
                if (enumerationsErrors.Count <= 0)
                {
                    sb.AppendLine();
                    sb.AppendLine("// Assert default specifications");
                    sb.AppendLine("Assert.False(LocalNotification.HasNotification(), string.Join(Environment.NewLine, LocalNotification.GetAll()));");
                }
                else
                {
                    enumerationsErrors = enumerationsErrors.Where(w => w.SpecificationType == SpecificationTypes.MustHave).ToList();
                    if (enumerationsErrors.Any())
                    {
                        sb.AppendLine();
                        sb.AppendLine("// Assert default specifications");
                        sb.AppendLine("Assert.True(LocalNotification.HasNotification());");
                        sb.AppendLine();
                        sb.AppendLine("var notifications = LocalNotification.GetAll();");

                        foreach (var error in enumerationsErrors)
                        {
                            sb.AppendLine($"Assert.Contains(notifications, a => a.Message == {EntityName}.EntityError.{error.EnumerationName}.ToString());");
                        }
                    }
                    else
                    {
                        sb.AppendLine();
                        sb.AppendLine("// Assert default specifications");
                        sb.AppendLine("Assert.False(LocalNotification.HasNotification());");
                    }
                }
            }
            sb.AppendLine("}");
            sb.AppendLine();
        }

        private void WriteBuildInvalidEntityTest(IndentedStringBuilder sb, IEntityType entityType)
        {
            var enumerationsErrors = Configuration.GetEntityErrorsEnumeration(RelationalTypeMapper, entityType, CSharpUtilities);
            if (enumerationsErrors.Count <= 0)
                return;

            var existOutOfBound = enumerationsErrors.Any(a => a.SpecificationType == SpecificationTypes.OutOfBound);
            if (!existOutOfBound)
                return;

            sb.AppendLine($"[Fact(DisplayName = \"Builder - Build invalid {EntityName}\")]");
            sb.AppendLine($"public void BuildInvalid{EntityName}()");
            sb.AppendLine("{");
            using (sb.Indent())
            {
                sb.AppendLine("// Arrange");
                sb.AppendLine($"var builder = new {BuilderName}(LocalNotification)");
                using (sb.Indent())
                {
                    var properties = Configuration.GetOrderedProperties(entityType, CSharpUtilities).ToList();
                    foreach (var property in properties.ToArray())
                    {
                        var propertyName = Configuration.GetPropertyName(entityType, property);

                        if (!enumerationsErrors.Any(a => a.Property == propertyName))
                            properties = properties.Where(w => w.Name != propertyName).ToList();
                    }

                    var lastProperty = properties.Last();
                    foreach (var property in properties)
                    {
                        var propertyName = Configuration.GetPropertyName(entityType, property);

                        var end = (lastProperty == property) ? ";" : string.Empty;

                        var value = property.ClrType.GetLiteralTypeRandomValue(RelationalTypeMapper, property, invalid: true);
                        sb.AppendLine($".With{propertyName}({value}){end}");
                    }
                }

                sb.AppendLine();
                sb.AppendLine("// Act");
                sb.AppendLine("builder.Build();");

                sb.AppendLine();
                sb.AppendLine("// Assert out of bound specifications");
                sb.AppendLine("var notifications = LocalNotification.GetAll();");
                foreach (var error in enumerationsErrors.Where(w => w.SpecificationType == SpecificationTypes.OutOfBound))
                {
                    sb.AppendLine($"Assert.Contains(notifications, a => a.Message == {EntityName}.EntityError.{error.EnumerationName}.ToString());");
                }
            }
            sb.AppendLine("}");
        }

        public string WriteCodeBuilder()
        {
            var sb = new IndentedStringBuilder();

            sb.AppendLine($"using {Configuration.Builder.Namespace};");
            sb.AppendLine($"using {Configuration.Entity.Namespace};");
            sb.AppendLine();
            sb.AppendLine($"namespace {Configuration.Test.Domain.Namespace}");
            sb.AppendLine("{");
            using (sb.Indent())
            {
                sb.AppendLine($"public partial class {BuilderName}Tests");
                sb.AppendLine("{");
                using (sb.Indent())
                {
                    sb.AppendLine();
                }
                sb.AppendLine("}");
            }
            sb.AppendLine("}");

            return sb.ToString();
        }

        public string WriteCodeServiceScaffold([NotNull] IEntityType entityType)
        {
            var sb = new IndentedStringBuilder();

            sb.AppendLine(Configuration.HeaderGeneratedFile);

            sb.AppendLine($"using {Configuration.Builder.Namespace};");
            sb.AppendLine($"using {Configuration.Entity.Namespace};");
            sb.AppendLine($"using {Configuration.DataTransfer.Namespace};");
            sb.AppendLine($"using {Configuration.Test.Domain.ModuleTestNamespace};");
            sb.AppendLine($"using {Configuration.Orm.Namespace};");
            sb.AppendLine("using Tnf.App.EntityFrameworkCore.TestBase;");
            sb.AppendLine("using Tnf.App.Domain.Services;");
            sb.AppendLine("using Tnf.App.Dto.Request;");
            sb.AppendLine("using Tnf.App.TestBase;");
            sb.AppendLine("using Xunit;");
            sb.AppendLine("using System.Linq;");
            sb.AppendLine("using System.Threading.Tasks;");
            sb.AppendLine("using System;");
            sb.AppendLine("using Tnf.App.Domain.Enums;");
            sb.AppendLine("using Tnf.App.Builder;");

            sb.AppendLine();
            sb.AppendLine($"namespace {Configuration.Test.Domain.Namespace}");
            sb.AppendLine("{");
            using (sb.Indent())
            {
                sb.AppendLine($"[Collection(\"{DomainServiceName}\")]");
                sb.AppendLine($"public partial class {DomainServiceName}Tests : TnfEfCoreIntegratedTestBase<{Configuration.Test.Domain.ModuleTestClassName}>");
                sb.AppendLine("{");
                using (sb.Indent())
                {
                    GenerateConstructor(sb);
                    GenerateGetAllEntityWithPagination(sb);
                    GenerateGetAllEntitySortedAsc(sb);
                    GenerateGetAllEntitySortedDesc(sb);
                    GenerateGetEntityWithSucess(sb, entityType);
                    GenerateGetEntityFieldsWithSucess(sb, entityType);
                    GenerateGetEntityWithInvalidParameterAndReturnNotification(sb);
                    GenerateGetEntityWhenNotExistsAndReturnNotification(sb);
                    GenerateDeleteEntityWithSucess(sb);
                    GenerateCreateEntityWithSucess(sb, entityType);
                    GenerateCreateNullEntityAndReturnNotification(sb);
                    GenerateCreateEmptyEntityAndReturnNotification(sb, entityType);
                    GenerateUpdateEntityWithSucess(sb, entityType);
                    GenerateUpdateEmptyEntityAndReturnNotification(sb, entityType);
                    GenerateUpdateEntityWhenNotExistsAndReturnNotification(sb, entityType);
                }
                sb.AppendLine("}");
            }
            sb.AppendLine("}");

            return sb.ToString();
        }

        private void GenerateGetAllEntityWithPagination(IndentedStringBuilder sb)
        {
            sb.AppendLine($"[Fact(DisplayName = \"DomainService - Get all {EntityName} with pagination\")]");
            sb.AppendLine($"public async Task GetAll{EntityName}WithPagination()");
            sb.AppendLine("{");
            using (sb.Indent())
            {
                sb.AppendLine("// Act");
                sb.AppendLine($"var response = await _service.GetAllAsync<{DtoName}>(new RequestAllDto() {{ PageSize = 5 }});");
                sb.AppendLine();
                sb.AppendLine("// Assert");
                sb.AppendLine("Assert.False(LocalNotification.HasNotification(), string.Join(Environment.NewLine, LocalNotification.GetAll()));");
                sb.AppendLine();
                sb.AppendLine("Assert.Equal(5, response.Items.Count);");
            }
            sb.AppendLine("}");
            sb.AppendLine();
        }

        private void GenerateGetAllEntitySortedAsc(IndentedStringBuilder sb)
        {
            sb.AppendLine($"[Fact(DisplayName = \"DomainService - Get all {EntityName} sorted asc\")]");
            sb.AppendLine($"public async Task GetAll{EntityName}SortedAsc()");
            sb.AppendLine("{");
            using (sb.Indent())
            {
                sb.AppendLine("// Act");
                sb.AppendLine($"var response = await _service.GetAllAsync<{DtoName}>(new RequestAllDto() {{ PageSize = 5, Order = \"id\" }});");
                sb.AppendLine();
                sb.AppendLine("// Assert");
                sb.AppendLine("Assert.False(LocalNotification.HasNotification(), string.Join(Environment.NewLine, LocalNotification.GetAll()));");
                sb.AppendLine();
                sb.AppendLine($"UsingDbContext<{Configuration.Orm.ContextClassName}>(context =>");
                sb.AppendLine("{");
                using (sb.Indent())
                {
                    sb.AppendLine($"var firstId = context.{PluralizedEntityName}.Take(5).OrderBy(o => o.Id).First().Id;");
                    sb.AppendLine();
                    sb.AppendLine("Assert.Equal(5, response.Items.Count);");
                    sb.AppendLine("Assert.Equal(response.Items[0].Id, firstId);");
                }
                sb.AppendLine("});");
            }
            sb.AppendLine("}");
            sb.AppendLine();
        }

        private void GenerateGetAllEntitySortedDesc(IndentedStringBuilder sb)
        {
            sb.AppendLine($"[Fact(DisplayName = \"DomainService - Get all {EntityName} sorted desc\")]");
            sb.AppendLine($"public async Task GetAll{EntityName}SortedDesc()");
            sb.AppendLine("{");
            using (sb.Indent())
            {
                sb.AppendLine("// Act");
                sb.AppendLine($"var response = await _service.GetAllAsync<{DtoName}>(new RequestAllDto() {{ PageSize = 5, Order = \"-id\" }});");
                sb.AppendLine();
                sb.AppendLine("// Assert");
                sb.AppendLine("Assert.False(LocalNotification.HasNotification(), string.Join(Environment.NewLine, LocalNotification.GetAll()));");
                sb.AppendLine();
                sb.AppendLine($"UsingDbContext<{Configuration.Orm.ContextClassName}>(context =>");
                sb.AppendLine("{");
                using (sb.Indent())
                {
                    sb.AppendLine($"var lastId = context.{PluralizedEntityName}.Take(5).OrderBy(o => o.Id).Last().Id;");
                    sb.AppendLine();
                    sb.AppendLine("Assert.Equal(5, response.Items.Count);");
                    sb.AppendLine("Assert.Equal(response.Items[0].Id, lastId);");
                }
                sb.AppendLine("});");
            }
            sb.AppendLine("}");
            sb.AppendLine();
        }

        private void GenerateGetEntityWithSucess(IndentedStringBuilder sb, IEntityType entityType)
        {
            var propertyNames = ReturnPropertiesNamesFromEntity(entityType);

            sb.AppendLine($"[Fact(DisplayName = \"DomainService - Get {EntityName} with sucess\")]");
            sb.AppendLine($"public async Task Get{EntityName}WithSucess()");
            sb.AppendLine("{");
            using (sb.Indent())
            {
                sb.AppendLine("// Arrange");
                sb.AppendLine($"var entity = default({EntityName});");
                sb.AppendLine();
                sb.AppendLine($"UsingDbContext<{Configuration.Orm.ContextClassName}>(context =>");
                sb.AppendLine("{");
                using (sb.Indent())
                {
                    sb.AppendLine($"entity = context.{PluralizedEntityName}.Take(5).OrderBy(o => o.Id).First();");
                }
                sb.AppendLine("});");

                sb.AppendLine();
                sb.AppendLine("// Act");
                sb.AppendLine($"var response = await _service.GetAsync(new RequestDto<{EntityKey}>(entity.Id));");
                sb.AppendLine();

                sb.AppendLine("// Assert");
                sb.AppendLine("Assert.False(LocalNotification.HasNotification(), string.Join(Environment.NewLine, LocalNotification.GetAll()));");
                sb.AppendLine();

                foreach (var propertyName in propertyNames)
                {
                    sb.AppendLine($"Assert.Equal(response.{propertyName}, entity.{propertyName});");
                }
            }
            sb.AppendLine("}");
            sb.AppendLine();
        }

        private void GenerateGetEntityFieldsWithSucess(IndentedStringBuilder sb, IEntityType entityType)
        {
            var propertyNames = ReturnPropertiesNamesFromEntity(entityType);

            sb.AppendLine($"[Fact(DisplayName = \"DomainService - Get {EntityName} fields with sucess\")]");
            sb.AppendLine($"public async Task Get{EntityName}FieldsWithSucess()");
            sb.AppendLine("{");
            using (sb.Indent())
            {
                sb.AppendLine("// Arrange");
                sb.AppendLine($"var entity = default({EntityName});");
                sb.AppendLine();
                sb.AppendLine($"UsingDbContext<{Configuration.Orm.ContextClassName}>(context =>");
                sb.AppendLine("{");
                using (sb.Indent())
                {
                    sb.AppendLine($"entity = context.{PluralizedEntityName}.Take(5).OrderBy(o => o.Id).First();");
                }
                sb.AppendLine("});");

                var propertyNamesFields = string.Join(",", propertyNames.Select(s => s.ToCamelCase()));

                sb.AppendLine();
                sb.AppendLine("// Act");
                sb.AppendLine($"var response = await _service.GetAsync(new RequestDto<{EntityKey}>(entity.Id, \"{propertyNamesFields}\"));");
                sb.AppendLine();
                sb.AppendLine("// Assert");
                sb.AppendLine("Assert.False(LocalNotification.HasNotification(), string.Join(Environment.NewLine, LocalNotification.GetAll()));");
                sb.AppendLine();

                foreach (var propertyName in propertyNames)
                {
                    sb.AppendLine($"Assert.Equal(response.{propertyName}, entity.{propertyName});");
                }
            }
            sb.AppendLine("}");
            sb.AppendLine();
        }

        private void GenerateGetEntityWithInvalidParameterAndReturnNotification(IndentedStringBuilder sb)
        {
            sb.AppendLine($"[Fact(DisplayName = \"DomainService - Get {EntityName} with invalid parameter and return notification\")]");
            sb.AppendLine($"public async Task Get{EntityName}WithInvalidParameterAndReturnNotification()");
            sb.AppendLine("{");
            using (sb.Indent())
            {
                sb.AppendLine("// Act");
                sb.AppendLine($"var response = await _service.GetAsync(new RequestDto<{EntityKey}>(default({EntityKey})));");
                sb.AppendLine();
                sb.AppendLine("// Assert");
                sb.AppendLine("var notifications = LocalNotification.GetAll();");
                sb.AppendLine();
                sb.AppendLine("Assert.Contains(notifications, a => a.DetailedMessage == TnfAppDomainErrors.AppDomainOnGetCouldNotFindError.ToString());");
            }
            sb.AppendLine("}");
            sb.AppendLine();
        }

        private void GenerateGetEntityWhenNotExistsAndReturnNotification(IndentedStringBuilder sb)
        {
            sb.AppendLine($"[Fact(DisplayName = \"DomainService - Get {EntityName} when not exists and return notification\")]");
            sb.AppendLine($"public async Task Get{EntityName}WhenNotExistsAndReturnNotification()");
            sb.AppendLine("{");
            using (sb.Indent())
            {
                sb.AppendLine("// Arrange");
                if (EntityKey == "Guid")
                {
                    sb.AppendLine("var notFoundId = Guid.NewGuid();");
                }
                else
                {
                    sb.AppendLine($"var notFoundId = default({EntityKey});");
                    sb.AppendLine();
                    sb.AppendLine($"UsingDbContext<{Configuration.Orm.ContextClassName}>(context =>");
                    sb.AppendLine("{");
                    using (sb.Indent())
                        sb.AppendLine($"notFoundId = context.{PluralizedEntityName}.Max(m => m.Id) + 1;");
                    sb.AppendLine("});");
                }

                sb.AppendLine();

                sb.AppendLine("// Act");
                sb.AppendLine($"var response = await _service.GetAsync(new RequestDto<{EntityKey}>(notFoundId));");
                sb.AppendLine();
                sb.AppendLine("// Assert");
                sb.AppendLine("var notifications = LocalNotification.GetAll();");
                sb.AppendLine();
                sb.AppendLine("Assert.Contains(notifications, a => a.DetailedMessage == TnfAppDomainErrors.AppDomainOnGetCouldNotFindError.ToString());");
            }
            sb.AppendLine("}");
            sb.AppendLine();
        }

        private void GenerateDeleteEntityWithSucess(IndentedStringBuilder sb)
        {
            sb.AppendLine($"[Fact(DisplayName = \"DomainService - Delete {EntityName} with sucess\")]");
            sb.AppendLine($"public async Task Delete{EntityName}WithSucess()");
            sb.AppendLine("{");
            using (sb.Indent())
            {
                sb.AppendLine($"await UsingDbContextAsync<{Configuration.Orm.ContextClassName}>(async context =>");
                sb.AppendLine("{");
                using (sb.Indent())
                {
                    sb.AppendLine($"var id = context.{PluralizedEntityName}.First().Id;");
                    sb.AppendLine();
                    sb.AppendLine("// Act");
                    sb.AppendLine("await _service.DeleteAsync(id);");
                    sb.AppendLine();
                    sb.AppendLine("// Assert");
                    sb.AppendLine("Assert.False(LocalNotification.HasNotification(), string.Join(Environment.NewLine, LocalNotification.GetAll()));");

                    sb.AppendLine();
                    sb.AppendLine("// Act");
                    sb.AppendLine($"await _service.GetAsync(new RequestDto<{EntityKey}>(id));");
                    sb.AppendLine();
                    sb.AppendLine("// Assert");
                    sb.AppendLine("var notifications = LocalNotification.GetAll();");
                    sb.AppendLine();
                    sb.AppendLine("Assert.Contains(notifications, a => a.DetailedMessage == TnfAppDomainErrors.AppDomainOnGetCouldNotFindError.ToString());");
                }
                sb.AppendLine("});");
            }
            sb.AppendLine("}");
            sb.AppendLine();
        }

        private void GenerateCreateEntityWithSucess(IndentedStringBuilder sb, IEntityType entityType)
        {
            sb.AppendLine($"[Fact(DisplayName = \"DomainService - Create {EntityName} with sucess\")]");
            sb.AppendLine($"public async Task Create{EntityName}WithSucess()");
            sb.AppendLine("{");
            using (sb.Indent())
            {
                sb.AppendLine("// Arrange");

                WriteBuilder(sb, entityType, forUpdate: false, invalidData: false);

                sb.AppendLine("// Act");
                sb.AppendLine($"var response = await _service.InsertAndGetIdAsync(builder);");
                sb.AppendLine();

                sb.AppendLine("// Assert");
                sb.AppendLine("Assert.False(LocalNotification.HasNotification(), string.Join(Environment.NewLine, LocalNotification.GetAll()));");
            }
            sb.AppendLine("}");
            sb.AppendLine();
        }

        private void GenerateCreateNullEntityAndReturnNotification(IndentedStringBuilder sb)
        {
            sb.AppendLine($"[Fact(DisplayName = \"DomainService - Create null {EntityName} and return notifications\")]");
            sb.AppendLine($"public async Task CreateNull{EntityName}AndReturnNotifications()");
            sb.AppendLine("{");
            using (sb.Indent())
            {
                sb.AppendLine("// Act");
                sb.AppendLine($"var response = await _service.InsertAndGetIdAsync(default({EntityName}Builder));");
                sb.AppendLine();

                sb.AppendLine("// Assert");
                sb.AppendLine("var notifications = LocalNotification.GetAll();");
                sb.AppendLine();
                sb.AppendLine("Assert.Contains(notifications, a => a.DetailedMessage == TnfAppDomainErrors.AppDomainOnInsertAndGetIdNullBuilderError.ToString());");
            }
            sb.AppendLine("}");
            sb.AppendLine();
        }

        private void GenerateCreateEmptyEntityAndReturnNotification(IndentedStringBuilder sb, IEntityType entityType)
        {
            var enumerationsErrors = Configuration.GetEntityErrorsEnumeration(RelationalTypeMapper, entityType, CSharpUtilities);
            if (enumerationsErrors.Count <= 0)
                return;

            sb.AppendLine($"[Fact(DisplayName = \"DomainService - Create empty {EntityName} and return notifications\")]");
            sb.AppendLine($"public async Task CreateEmpty{EntityName}AndReturnNotifications()");
            sb.AppendLine("{");
            using (sb.Indent())
            {
                sb.AppendLine("// Arrange");
                sb.AppendLine($"var builder = new {BuilderName}(LocalNotification);");
                sb.AppendLine();
                sb.AppendLine("// Act");
                sb.AppendLine($"var response = await _service.InsertAndGetIdAsync(builder);");
                sb.AppendLine();

                sb.AppendLine("// Assert");
                sb.AppendLine("var notifications = LocalNotification.GetAll();");
                sb.AppendLine();

                foreach (var error in enumerationsErrors.Where(w => w.SpecificationType == SpecificationTypes.MustHave))
                {
                    sb.AppendLine($"Assert.Contains(notifications, a => a.Message == {EntityName}.EntityError.{error.EnumerationName}.ToString());");
                }
            }
            sb.AppendLine("}");
            sb.AppendLine();
        }

        private void GenerateUpdateEntityWithSucess(IndentedStringBuilder sb, IEntityType entityType)
        {
            sb.AppendLine($"[Fact(DisplayName = \"DomainService - Update {EntityName} with sucess\")]");
            sb.AppendLine($"public async Task Update{EntityName}WithSucess()");
            sb.AppendLine("{");
            using (sb.Indent())
            {
                sb.AppendLine("// Arrange");

                WriteBuilder(sb, entityType, forUpdate: true, invalidData: false);

                sb.AppendLine();
                sb.AppendLine("// Act");
                sb.AppendLine($"var entity = await _service.UpdateAsync(builder);");

                sb.AppendLine();
                sb.AppendLine("// Assert");
                sb.AppendLine("Assert.False(LocalNotification.HasNotification(), string.Join(Environment.NewLine, LocalNotification.GetAll()));");
                sb.AppendLine();

                sb.AppendLine($"var response = await _service.GetAsync(new RequestDto<{EntityKey}>(entity.Id));");
                sb.AppendLine();
                foreach (var propertyName in ReturnPropertiesNamesFromEntity(entityType))
                {
                    sb.AppendLine($"Assert.Equal(response.{propertyName}, entity.{propertyName});");
                }
            }
            sb.AppendLine("}");
            sb.AppendLine();
        }

        private void GenerateUpdateEmptyEntityAndReturnNotification(IndentedStringBuilder sb, IEntityType entityType)
        {
            var enumerationsErrors = Configuration.GetEntityErrorsEnumeration(RelationalTypeMapper, entityType, CSharpUtilities);
            if (enumerationsErrors.Count <= 0)
                return;

            sb.AppendLine($"[Fact(DisplayName = \"DomainService - Update empty {EntityName} and return notification\")]");
            sb.AppendLine($"public async Task UpdateEmpty{EntityName}AndReturnNotification()");
            sb.AppendLine("{");
            using (sb.Indent())
            {
                sb.AppendLine("// Arrange");
                sb.AppendLine($"var builder = new {BuilderName}(LocalNotification);");
                sb.AppendLine();
                sb.AppendLine("// Act");
                sb.AppendLine($"var response = await _service.UpdateAsync(builder);");

                sb.AppendLine();
                sb.AppendLine("// Assert");
                sb.AppendLine("var notifications = LocalNotification.GetAll();");
                sb.AppendLine();

                foreach (var error in enumerationsErrors.Where(w => w.SpecificationType == SpecificationTypes.MustHave))
                {
                    sb.AppendLine($"Assert.Contains(notifications, a => a.Message == {EntityName}.EntityError.{error.EnumerationName}.ToString());");
                }
            }
            sb.AppendLine("}");
            sb.AppendLine();
        }

        private void GenerateUpdateEntityWhenNotExistsAndReturnNotification(IndentedStringBuilder sb, IEntityType entityType)
        {
            sb.AppendLine($"[Fact(DisplayName = \"DomainService - Update empty {EntityName} and return notification\")]");
            sb.AppendLine($"public async Task Update{EntityName}WhenNotExistsAndReturnNotification()");
            sb.AppendLine("{");
            using (sb.Indent())
            {
                sb.AppendLine("// Arrange");

                WriteBuilder(sb, entityType, forUpdate: false, invalidData: false);

                sb.AppendLine("// Act");
                sb.AppendLine($"var response = await _service.UpdateAsync(builder);");

                sb.AppendLine();
                sb.AppendLine("// Assert");
                sb.AppendLine("var notifications = LocalNotification.GetAll();");
                sb.AppendLine();
                sb.AppendLine("Assert.Contains(notifications, a => a.Message == TnfAppDomainErrors.AppDomainOnUpdateCouldNotFindError.ToString());");
            }
            sb.AppendLine("}");
        }

        private void GenerateConstructor(IndentedStringBuilder sb)
        {
            sb.AppendLine($"private readonly IAppDomainService<{EntityName}, {EntityKey}> _service;");
            sb.AppendLine();

            sb.AppendLine($"public {DomainServiceName}Tests()");
            sb.AppendLine("{");
            using (sb.Indent())
            {
                sb.AppendLine("var mockCollection = GetCollectionForInitializeInMemoryContext();");
                sb.AppendLine();
                sb.AppendLine($"UsingDbContext<{Configuration.Orm.ContextClassName}>(context => context.{PluralizedEntityName}.AddRange(mockCollection));");
                sb.AppendLine();
                sb.AppendLine($"_service = Resolve<IAppDomainService<{EntityName}, {EntityKey}>>();");
            }
            sb.AppendLine("}");
            sb.AppendLine();
        }

        public string WriteCodeService(IEntityType entityType)
        {
            var sb = new IndentedStringBuilder();

            sb.AppendLine($"using {Configuration.Entity.Namespace};");
            sb.AppendLine("using System;");
            sb.AppendLine("using System.Collections.Generic;");
            sb.AppendLine();
            sb.AppendLine($"namespace {Configuration.Test.Domain.Namespace}");
            sb.AppendLine("{");
            using (sb.Indent())
            {
                sb.AppendLine($"public partial class {DomainServiceName}Tests");
                sb.AppendLine("{");
                using (sb.Indent())
                {
                    sb.AppendLine($"public IEnumerable<{EntityName}> GetCollectionForInitializeInMemoryContext()");
                    sb.AppendLine("{");
                    using (sb.Indent())
                    {
                        sb.AppendLine($"var mock = new List<{EntityName}>();");
                        sb.AppendLine();

                        for (int i = 0; i < Configuration.Test.NumberOfEntitiesGenerated; i++)
                            WriteEntityForInMemorySetup(sb, entityType, i);

                        sb.AppendLine("return mock;");
                    }
                    sb.AppendLine("}");
                }
                sb.AppendLine("}");
            }
            sb.AppendLine("}");

            return sb.ToString();
        }

        private List<string> ReturnPropertiesNamesFromEntity(IEntityType entityType)
        {
            var properties = entityType.GetProperties().Where(w => !w.IsNullable).OrderBy(p => p.Scaffolding().ColumnOrdinal);

            var propertyNames = new List<string>();
            foreach (var property in properties)
            {
                var propertyName = Configuration.GetPropertyName(entityType, property);
                propertyNames.Add(propertyName);
            }

            return propertyNames;
        }

        private void WriteBuilder(IndentedStringBuilder sb, IEntityType entityType, bool forUpdate, bool invalidData)
        {
            sb.AppendLine($"var builder = new {BuilderName}(LocalNotification)");

            using (sb.Indent())
            {
                var properties = Configuration.GetOrderedProperties(entityType, CSharpUtilities).ToList();

                var last = properties.Last();
                foreach (var property in properties)
                {
                    var propertyName = Configuration.GetPropertyName(entityType, property);

                    if (propertyName == "Id" && forUpdate)
                        continue;

                    var end = property == last ? ";" : string.Empty;
                    var value = property.ClrType.GetLiteralTypeRandomValue(RelationalTypeMapper, property, invalid: invalidData);
                    sb.AppendLine($".With{propertyName}({value}){end}");
                }
            }

            sb.AppendLine();

            if (forUpdate)
            {
                sb.AppendLine($"UsingDbContext<{Configuration.Orm.ContextClassName}>(context =>");
                sb.AppendLine("{");
                using (sb.Indent())
                {
                    sb.AppendLine($"builder.WithId(context.{PluralizedEntityName}.First().Id);");
                }
                sb.AppendLine("});");
            }
        }

        private void WriteEntityForInMemorySetup(IndentedStringBuilder sb, IEntityType entityType, int index)
        {
            index++;

            sb.AppendLine($"var entity{index} = new {EntityName}()");
            sb.AppendLine("{");

            using (sb.Indent())
            {
                var properties = entityType.GetProperties().OrderBy(p => p.Scaffolding().ColumnOrdinal);

                foreach (var property in properties)
                {
                    var propertyName = Configuration.GetPropertyName(entityType, property);
                    if (propertyName == "Id")
                    {
                        if (property.ClrType == typeof(Guid))
                            sb.AppendLine($"{propertyName} = Guid.NewGuid(),");
                        else
                            sb.AppendLine($"{propertyName} = {index},");

                        continue;
                    }

                    var value = property.ClrType.GetLiteralTypeRandomValue(RelationalTypeMapper, property, invalid: false);
                    sb.AppendLine($"{propertyName} = {value},");
                }
            }

            sb.AppendLine("};");
            sb.AppendLine($"mock.Add(entity{index});");
            sb.AppendLine();
        }
    }
}
