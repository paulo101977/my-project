﻿using Crudzilla.Configuration;
using JetBrains.Annotations;
using Microsoft.EntityFrameworkCore.Internal;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Scaffolding.Internal;

namespace Crudzilla.Writters
{
    internal class ApplicationServiceWriter
    {
        private CrudzillaConfig Configuration { get; }
        private ICSharpUtilities CSharpUtilities { get; }

        private IndentedStringBuilder _sb;

        private string EntityName;
        private string DtoName;
        private string ApplicationServiceName;
        private string EntityKey;

        public ApplicationServiceWriter(
            [NotNull] ICSharpUtilities cSharpUtilities,
            [NotNull] CrudzillaConfig scaffoldConfiguration)
        {
            Check.NotNull(cSharpUtilities, nameof(cSharpUtilities));
            Check.NotNull(scaffoldConfiguration, nameof(scaffoldConfiguration));

            CSharpUtilities = cSharpUtilities;
            Configuration = scaffoldConfiguration;
        }

        public string WriteCodeScaffold([NotNull] IEntityType entityType)
        {
            Check.NotNull(entityType, nameof(entityType));

            //--
            EntityName = Configuration.GetEntityNameOrDefault(entityType);
            DtoName = Configuration.GetDtoName(entityType);
            ApplicationServiceName = Configuration.GetApplicationServiceName(entityType);
            EntityKey = entityType.GetEntityKeyType(CSharpUtilities);
            //--

            _sb = new IndentedStringBuilder();

            _sb.AppendLine(Configuration.HeaderGeneratedFile);

            _sb.AppendLine("using System;");
            _sb.AppendLine("using Tnf;");
            _sb.AppendLine("using Tnf.Dto;");
            _sb.AppendLine("using Tnf.Domain.Services;");
            _sb.AppendLine("using Tnf.Application.Services;");
            _sb.AppendLine("using Tnf.Notifications;");
            _sb.AppendLine("using System.Threading.Tasks;");
            _sb.AppendLine($"using {Configuration.DataTransfer.Namespace};");
            _sb.AppendLine($"using {Configuration.Entity.Namespace};");
            _sb.AppendLine($"using {Configuration.Adapter.Namespace};");
            _sb.AppendLine($"using {Configuration.ApplicationService.InterfaceNamespace};");
            _sb.AppendLine($"using {Configuration.Entity.ConstantNamespace};");
            _sb.AppendLine("using Tnf.App.Builder;");

            _sb.AppendLine();
            _sb.AppendLine($"namespace {Configuration.ApplicationService.Namespace}");
            _sb.AppendLine("{");
            using (_sb.Indent())
            {
                GenerateClass();
            }
            _sb.AppendLine("}");

            return _sb.ToString();
        }

        public string WriteCodeInterfaceScaffold()
        {
            var sb = new IndentedStringBuilder();

            sb.AppendLine(Configuration.HeaderGeneratedFile);
            sb.AppendLine("using System;");
            sb.AppendLine("using Tnf.Application.Services;");
            sb.AppendLine("using System.Threading.Tasks;");
            sb.AppendLine("using Tnf.Dto;");
            sb.AppendLine($"using {Configuration.DataTransfer.Namespace};");
            sb.AppendLine();

            sb.AppendLine($"namespace {Configuration.ApplicationService.InterfaceNamespace}");
            sb.AppendLine("{");
            using (sb.Indent())
            {
                sb.AppendLine($"public interface IScaffold{ApplicationServiceName} : IApplicationService");
                sb.AppendLine("{");
                sb.AppendLine($"Task<{DtoName}> Create({DtoName} dto);");
                sb.AppendLine($"Task Delete({EntityKey} id);");
                sb.AppendLine($"Task<{DtoName}> Get(Default{EntityKey.UppercaseFirst()}RequestDto id);");
                sb.AppendLine($"Task<IListDto<{DtoName}>> GetAll(GetAll{DtoName} request);");
                sb.AppendLine($"Task<{DtoName}> Update({EntityKey} id, {DtoName} dto);");
                sb.AppendLine("}");
            }
            sb.AppendLine("}");

            return sb.ToString();
        }

        public string WriteCodeInterface()
        {
            var sb = new IndentedStringBuilder();

            sb.AppendLine($"namespace {Configuration.ApplicationService.InterfaceNamespace}");
            sb.AppendLine("{");
            using (sb.Indent())
            {
                sb.AppendLine($"public interface I{ApplicationServiceName} : IScaffold{ApplicationServiceName}");
                sb.AppendLine("{");
                sb.AppendLine("}");
            }
            sb.AppendLine("}");

            return sb.ToString();
        }

        public string WriteCode()
        {
            var sb = new IndentedStringBuilder();

            sb.AppendLine($"using System;");
            sb.AppendLine($"using {Configuration.Adapter.Namespace};");
            sb.AppendLine($"using {Configuration.Entity.Namespace};");
            sb.AppendLine($"using {Configuration.ApplicationService.InterfaceNamespace};");
            sb.AppendLine($"using Tnf.App.Domain.Services;");
            sb.AppendLine($"using Tnf.Notifications;");
            sb.AppendLine();
            sb.AppendLine($"namespace {Configuration.ApplicationService.Namespace}");
            sb.AppendLine("{");
            using (sb.Indent())
            {
                sb.AppendLine($"public class {ApplicationServiceName} : Scaffold{ApplicationServiceName}, I{ApplicationServiceName}");
                sb.AppendLine("{");
                using (sb.Indent())
                {
                    sb.AppendLine($"public {ApplicationServiceName}(");
                    using (sb.Indent())
                    {
                        sb.AppendLine($"I{EntityName}Adapter {EntityName.ToCamelCase()}Adapter,");
                        sb.AppendLine($"IDomainService<{EntityName}, {EntityKey}> {EntityName.ToCamelCase()}DomainService,");
                        sb.AppendLine($"INotificationHandler notificationHandler");
                        using (sb.Indent())
                        {
                            sb.AppendLine($": base({EntityName.ToCamelCase()}Adapter, {EntityName.ToCamelCase()}DomainService, notificationHandler)");
                        }
                    }
                    sb.AppendLine("{");
                    sb.AppendLine("}");
                }
                sb.AppendLine("}");
            }
            sb.AppendLine("}");

            return sb.ToString();
        }

        public void GenerateClass()
        {
            _sb.AppendLine($"public abstract class Scaffold{ApplicationServiceName} : ApplicationServiceBase, IScaffold{ApplicationServiceName}");
            _sb.AppendLine("{");
            using (_sb.Indent())
            {
                GenerateMembers();
                GenerateConstructor();
                GenerateMethods();
            }
            _sb.AppendLine("}");
        }

        private void GenerateMembers()
        {
            _sb.AppendLine($"protected readonly I{EntityName}Adapter {EntityName}Adapter;");
            _sb.AppendLine($"protected readonly IDomainService<{EntityName}> {EntityName}DomainService;");
            _sb.AppendLine();
        }

        public void GenerateConstructor()
        {
            _sb.AppendLine($"public Scaffold{ApplicationServiceName}(");
            using (_sb.Indent())
            {
                _sb.AppendLine($"I{EntityName}Adapter {EntityName.ToCamelCase()}Adapter,");
                _sb.AppendLine($"IDomainService<{EntityName}> {EntityName.ToCamelCase()}DomainService,");
                _sb.AppendLine($"INotificationHandler notificationHandler)");
                _sb.AppendLine($"   : base(notificationHandler)");
            }
            _sb.AppendLine("{");
            using (_sb.Indent())
            {
                _sb.AppendLine($"{EntityName}Adapter = Check.NotNull({EntityName.ToCamelCase()}Adapter, nameof({EntityName.ToCamelCase()}Adapter));");
                _sb.AppendLine($"{EntityName}DomainService = Check.NotNull({EntityName.ToCamelCase()}DomainService, nameof({EntityName.ToCamelCase()}DomainService));");
                _sb.AppendLine();
                _sb.AppendLine($"{EntityName}DomainService.EntityName = EntityNames.{EntityName};");
            }
            _sb.AppendLine("}");
            _sb.AppendLine();
        }

        public void GenerateMethods()
        {
            _sb.AppendLine($"public virtual async Task<{DtoName}> Create({DtoName} dto)");
            _sb.AppendLine("{");
            using (_sb.Indent())
            {
                _sb.AppendLine($"ValidateDto<{DtoName}>(dto, nameof(dto));");
                _sb.AppendLine();
                _sb.AppendLine($"if (Notification.HasNotification())");
                using (_sb.Indent())
                    _sb.AppendLine($"return {DtoName}.NullInstance;");

                _sb.AppendLine();
                _sb.AppendLine($"var {EntityName.ToCamelCase()}Builder = {EntityName}Adapter.Map(dto);");
                _sb.AppendLine();
                _sb.AppendLine($"dto.Id = (await {EntityName}DomainService.InsertAndSaveChangesAsync({EntityName.ToCamelCase()}Builder)).Id;");
                _sb.AppendLine("return dto;");
            }
            _sb.AppendLine("}");
            _sb.AppendLine();

            _sb.AppendLine($"public virtual async Task Delete({EntityKey} id)");
            _sb.AppendLine("{");
            using (_sb.Indent())
            {
                _sb.AppendLine("if (!ValidateId(id)) return;;");
                _sb.AppendLine();
                _sb.AppendLine("if (!Notification.HasNotification())");
                using (_sb.Indent())
                    _sb.AppendLine($"await {EntityName}DomainService.DeleteAsync(w => w.Id == id);");
            }
            _sb.AppendLine("}");
            _sb.AppendLine();

            _sb.AppendLine($"public virtual async Task<{DtoName}> Get(Default{EntityKey.UppercaseFirst()}RequestDto id)");
            _sb.AppendLine("{");
            using (_sb.Indent())
            {
                _sb.AppendLine($"if (!ValidateRequestDto(id) || !ValidateId<{EntityKey}>(id.Id)) return null;");
                _sb.AppendLine();
                _sb.AppendLine($"if (Notification.HasNotification())");
                using (_sb.Indent())
                    _sb.AppendLine($"return {DtoName}.NullInstance;");

                _sb.AppendLine();
                _sb.AppendLine($"var entity = await {EntityName}DomainService.GetAsync(id).ConfigureAwait(false);");
                _sb.AppendLine($"return entity.MapTo<{DtoName}>();");
            }
            _sb.AppendLine("}");
            _sb.AppendLine();

            _sb.AppendLine($"public virtual async Task<IListDto<{DtoName}>> GetAll(GetAll{DtoName} request)");
            _sb.AppendLine("{");
            using (_sb.Indent())
            {
                _sb.AppendLine($"ValidateRequestAllDto(request, nameof(request));");
                _sb.AppendLine();
                _sb.AppendLine($"if (Notification.HasNotification())");
                using (_sb.Indent())
                    _sb.AppendLine($"return null;");

                _sb.AppendLine();
                _sb.AppendLine($"var response = await {EntityName}DomainService.GetAllAsync<{DtoName}>(request).ConfigureAwait(false);");
                _sb.AppendLine($"return response;");
            }
            _sb.AppendLine("}");
            _sb.AppendLine();

            _sb.AppendLine($"public virtual async Task<{DtoName}> Update({EntityKey} id, {DtoName} dto)");
            _sb.AppendLine("{");
            using (_sb.Indent())
            {
                _sb.AppendLine("ValidateDtoAndId(dto, id, nameof(dto), nameof(id));");
                _sb.AppendLine();
                _sb.AppendLine($"if (Notification.HasNotification())");
                using (_sb.Indent())
                    _sb.AppendLine($"return {DtoName}.NullInstance;");

                _sb.AppendLine();
                _sb.AppendLine("dto.Id = id;");
                _sb.AppendLine();
                _sb.AppendLine($"var {EntityName.ToCamelCase()}Builder = {EntityName}Adapter.Map(dto);");
                _sb.AppendLine();
                _sb.AppendLine($"await {EntityName}DomainService.UpdateAsync({EntityName.ToCamelCase()}Builder);");
                _sb.AppendLine($"return dto;");
            }
            _sb.AppendLine("}");
        }
    }
}
