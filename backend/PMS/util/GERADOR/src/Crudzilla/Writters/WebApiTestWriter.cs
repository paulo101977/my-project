﻿using Crudzilla.Configuration;
using JetBrains.Annotations;
using Microsoft.EntityFrameworkCore.Internal;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Microsoft.EntityFrameworkCore.Scaffolding.Internal;
using Microsoft.EntityFrameworkCore.Storage;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Crudzilla.Writters
{
    internal class WebApiTestWriter
    {
        private CrudzillaConfig Configuration { get; }
        private ICSharpUtilities CSharpUtilities { get; }
        private IRelationalTypeMapper RelationalTypeMapper { get; }

        private string EntityName { get; set; }
        private string DtoName { get; set; }
        private string ControllerName { get; set; }
        private string PluralizedEntityName { get; set; }
        private string EntityKey { get; set; }

        public WebApiTestWriter(
            [NotNull] CrudzillaConfig configuration,
            [NotNull] ICSharpUtilities cSharpUtilities,
            [NotNull] IRelationalTypeMapper relationalTypeMapper)
        {
            Configuration = configuration;
            CSharpUtilities = cSharpUtilities;
            RelationalTypeMapper = relationalTypeMapper;
        }

        public virtual string WriteCodeScaffold([NotNull] IEntityType entityType)
        {
            //--
            EntityName = Configuration.GetEntityNameOrDefault(entityType);
            DtoName = Configuration.GetDtoName(entityType);
            ControllerName = Configuration.GetControllerName(entityType);
            PluralizedEntityName = Configuration.GetPluralizedEntityNameOrDefault(entityType);
            EntityKey = entityType.GetEntityKeyType(CSharpUtilities);
            //--

            var sb = new IndentedStringBuilder();

            sb.AppendLine(Configuration.HeaderGeneratedFile);

            sb.AppendLine($"using {Configuration.Entity.Namespace};");
            sb.AppendLine($"using {Configuration.DataTransfer.Namespace};");
            sb.AppendLine($"using {Configuration.Orm.Namespace};");
            sb.AppendLine($"using {Configuration.WebApi.Namespace}.Controllers;");
            sb.AppendLine($"using {Configuration.Test.WebApi.StartupTestNamespace};");

            sb.AppendLine("using Tnf.App.AspNetCore.Mvc.Response;");
            sb.AppendLine("using Tnf.App.AspNetCore.Mvc.Controllers;");
            sb.AppendLine("using Microsoft.Extensions.DependencyInjection;");
            sb.AppendLine("using Tnf.App.Application.Enums;");
            sb.AppendLine("using Tnf.App.Domain.Enums;");
            sb.AppendLine("using System.Collections.Generic;");
            sb.AppendLine("using Tnf.App.EntityFrameworkCore;");
            sb.AppendLine("using Tnf.App.AspNetCore.TestBase;");
            sb.AppendLine("using System.Threading.Tasks;");
            sb.AppendLine("using Tnf.App.Dto.Response;");
            sb.AppendLine("using System.Linq;");
            sb.AppendLine("using System.Net;");
            sb.AppendLine("using System;");
            sb.AppendLine("using Xunit;");
            sb.AppendLine();
            sb.AppendLine($"namespace {Configuration.Test.WebApi.Namespace}");
            sb.AppendLine("{");
            using (sb.Indent())
            {
                sb.AppendLine($"[Collection(\"{ControllerName}\")]");
                sb.AppendLine($"public partial class {EntityName}ControllerTests : TnfAspNetCoreIntegratedTestBase<{Configuration.Test.WebApi.StartupTestClassName}>");
                sb.AppendLine("{");
                using (sb.Indent())
                {
                    GenerateConstructor(sb);
                    GenerateShouldResolveEntityControllerTest(sb);
                    GenerateGetAllEntityWithPagination(sb);
                    GenerateGetAllEntitySortedAsc(sb);
                    GenerateGetAllEntitySortedDesc(sb);
                    GenerateGetEntityWithSucess(sb, entityType);
                    GenerateGetEntityFieldsWithSucess(sb, entityType);
                    GenerateGetEntityWithInvalidParameterReturnBadRequest(sb);
                    GenerateGetEntityWhenNotExistsAndReturnNotFound(sb);
                    GenerateDeleteEntityWithSucess(sb);
                    GenerateDeleteEntityWithInvalidParameterReturnBadRequest(sb);
                    GenerateDeleteEntityWhenNotExistsReturnNotFound(sb);
                    GeneratePostEntityWithSucess(sb, entityType);
                    GeneratePostNullEntityAndReturnBadRequest(sb);
                    GeneratePostEmptyEntityAndReturnBadRequest(sb, entityType);
                    GeneratePostInvalidEntityAndReturnBadRequest(sb, entityType);
                    GeneratePutEntityWithSucess(sb, entityType);
                    GeneratePutEntityWithInvalidParameterReturnBadRequest(sb);
                    GeneratePutNullEntityAndReturnBadRequest(sb);
                    GeneratePutEmptyEntityAndReturnBadRequest(sb, entityType);
                    GeneratePutInvalidEntityAndReturnBadRequest(sb, entityType);
                    GeneratePutEntityWhenNotExistsAndReturnNotFound(sb, entityType);
                }
                sb.AppendLine("}");
            }
            sb.AppendLine("}");

            return sb.ToString();
        }

        public virtual string WriteCode(IEntityType entityType)
        {
            var sb = new IndentedStringBuilder();

            sb.AppendLine($"using {Configuration.Entity.Namespace};");
            sb.AppendLine("using System;");
            sb.AppendLine("using System.Collections.Generic;");
            sb.AppendLine();
            sb.AppendLine($"namespace {Configuration.Test.WebApi.Namespace}");
            sb.AppendLine("{");
            using (sb.Indent())
            {
                sb.AppendLine($"public partial class {EntityName}ControllerTests");
                sb.AppendLine("{");
                using (sb.Indent())
                {
                    sb.AppendLine($"public IEnumerable<{EntityName}> GetCollectionForInitializeInMemoryContext()");
                    sb.AppendLine("{");
                    using (sb.Indent())
                    {
                        sb.AppendLine($"var mock = new List<{EntityName}>();");
                        sb.AppendLine();

                        for (int i = 0; i < Configuration.Test.NumberOfEntitiesGenerated; i++)
                            WriteEntityForInMemorySetup(sb, entityType, i);

                        sb.AppendLine("return mock;");
                    }
                    sb.AppendLine("}");
                }
                sb.AppendLine("}");
            }
            sb.AppendLine("}");

            return sb.ToString();
        }

        private void GenerateConstructor(IndentedStringBuilder sb)
        {
            sb.AppendLine($"public {EntityName}ControllerTests()");
            sb.AppendLine("{");
            using (sb.Indent())
            {
                sb.AppendLine("var mockCollection = GetCollectionForInitializeInMemoryContext();");
                sb.AppendLine();
                sb.AppendLine($"IocManager.UsingDbContext<{Configuration.Orm.ContextClassName}>(context => context.{PluralizedEntityName}.AddRange(mockCollection));");
            }
            sb.AppendLine("}");
            sb.AppendLine();
        }

        private void GenerateShouldResolveEntityControllerTest(IndentedStringBuilder sb)
        {
            sb.AppendLine($"[Fact(DisplayName = \"Api - Should resolve {EntityName} Controller\")]");
            sb.AppendLine("public void ShouldResolveClaimController()");
            sb.AppendLine("{");
            using (sb.Indent())
            {
                sb.AppendLine($"Assert.NotNull(ServiceProvider.GetService<{ControllerName}>());");
            }
            sb.AppendLine("}");
            sb.AppendLine();
        }

        private void GenerateGetAllEntityWithPagination(IndentedStringBuilder sb)
        {
            sb.AppendLine($"[Fact(DisplayName = \"Api - Get all {EntityName} with pagination\")]");
            sb.AppendLine($"public async Task GetAll{EntityName}WithPagination()");
            sb.AppendLine("{");
            using (sb.Indent())
            {
                sb.AppendLine("// Act");
                sb.AppendLine($"var response = await GetResponseAsObjectAsync<ListDto<{DtoName}, {EntityKey}>>(");
                using (sb.Indent())
                {
                    sb.AppendLine($"$\"/{{RouteConsts.{EntityName}}}?pageSize=5\"");
                }
                sb.AppendLine(");");
                sb.AppendLine();
                sb.AppendLine("// Assert");
                sb.AppendLine("Assert.Equal(5, response.Items.Count);");
            }
            sb.AppendLine("}");
            sb.AppendLine();
        }

        private void GenerateGetAllEntitySortedAsc(IndentedStringBuilder sb)
        {
            sb.AppendLine($"[Fact(DisplayName = \"Api - Get all {EntityName} sorted asc\")]");
            sb.AppendLine($"public async Task GetAll{EntityName}SortedAsc()");
            sb.AppendLine("{");
            using (sb.Indent())
            {
                sb.AppendLine("// Act");
                sb.AppendLine($"var response = await GetResponseAsObjectAsync<ListDto<{DtoName}, {EntityKey}>>(");
                using (sb.Indent())
                {
                    sb.AppendLine($"$\"/{{RouteConsts.{EntityName}}}?pageSize=5&order=id\"");
                }
                sb.AppendLine(");");
                sb.AppendLine();
                sb.AppendLine("// Assert");
                sb.AppendLine($"IocManager.UsingDbContext<{Configuration.Orm.ContextClassName}>(context =>");
                sb.AppendLine("{");
                using (sb.Indent())
                {
                    sb.AppendLine($"var firstId = context.{PluralizedEntityName}.Take(5).OrderBy(o => o.Id).First().Id;");
                    sb.AppendLine();
                    sb.AppendLine("Assert.Equal(5, response.Items.Count);");
                    sb.AppendLine("Assert.Equal(response.Items[0].Id, firstId);");
                }
                sb.AppendLine("});");
            }
            sb.AppendLine("}");
            sb.AppendLine();
        }

        private void GenerateGetAllEntitySortedDesc(IndentedStringBuilder sb)
        {
            sb.AppendLine($"[Fact(DisplayName = \"Api - Get all {EntityName} sorted desc\")]");
            sb.AppendLine($"public async Task GetAll{EntityName}SortedDesc()");
            sb.AppendLine("{");
            using (sb.Indent())
            {
                sb.AppendLine("// Act");
                sb.AppendLine($"var response = await GetResponseAsObjectAsync<ListDto<{DtoName}, {EntityKey}>>(");
                using (sb.Indent())
                {
                    sb.AppendLine($"$\"/{{RouteConsts.{EntityName}}}?pageSize=5&order=-id\"");
                }
                sb.AppendLine(");");
                sb.AppendLine();
                sb.AppendLine("// Assert");
                sb.AppendLine($"IocManager.UsingDbContext<{Configuration.Orm.ContextClassName}>(context =>");
                sb.AppendLine("{");
                using (sb.Indent())
                {
                    sb.AppendLine($"var lastId = context.{PluralizedEntityName}.Take(5).OrderBy(o => o.Id).Last().Id;");
                    sb.AppendLine();
                    sb.AppendLine("Assert.Equal(5, response.Items.Count);");
                    sb.AppendLine("Assert.Equal(response.Items[0].Id, lastId);");
                }
                sb.AppendLine("});");
            }
            sb.AppendLine("}");
            sb.AppendLine();
        }

        private void GenerateGetEntityWithSucess(IndentedStringBuilder sb, IEntityType entityType)
        {
            var propertyNames = ReturnPropertiesNamesFromEntity(entityType);

            sb.AppendLine($"[Fact(DisplayName = \"Api - Get {EntityName} with sucess\")]");
            sb.AppendLine($"public async Task Get{EntityName}WithSucess()");
            sb.AppendLine("{");
            using (sb.Indent())
            {
                sb.AppendLine("// Arrange");
                sb.AppendLine($"var entity = default({EntityName});");
                sb.AppendLine();
                sb.AppendLine($"IocManager.UsingDbContext<{Configuration.Orm.ContextClassName}>(context =>");
                sb.AppendLine("{");
                using (sb.Indent())
                {
                    sb.AppendLine($"entity = context.{PluralizedEntityName}.Take(5).OrderBy(o => o.Id).First();");
                }
                sb.AppendLine("});");

                sb.AppendLine();
                sb.AppendLine("// Act");
                sb.AppendLine($"var response = await GetResponseAsObjectAsync<{DtoName}>(");
                using (sb.Indent())
                {
                    sb.AppendLine($"$\"/{{RouteConsts.{EntityName}}}/{{entity.Id}}\"");
                }
                sb.AppendLine(");");
                sb.AppendLine();
                sb.AppendLine("// Assert");

                foreach (var propertyName in propertyNames)
                {
                    sb.AppendLine($"Assert.Equal(response.{propertyName}, entity.{propertyName});");
                }
            }
            sb.AppendLine("}");
            sb.AppendLine();
        }

        private void GenerateGetEntityFieldsWithSucess(IndentedStringBuilder sb, IEntityType entityType)
        {
            var propertyNames = ReturnPropertiesNamesFromEntity(entityType);

            sb.AppendLine($"[Fact(DisplayName = \"Api - Get {EntityName} fields with sucess\")]");
            sb.AppendLine($"public async Task Get{EntityName}FieldsWithSucess()");
            sb.AppendLine("{");
            using (sb.Indent())
            {
                sb.AppendLine("// Arrange");
                sb.AppendLine($"var entity = default({EntityName});");
                sb.AppendLine();
                sb.AppendLine($"IocManager.UsingDbContext<{Configuration.Orm.ContextClassName}>(context =>");
                sb.AppendLine("{");
                using (sb.Indent())
                {
                    sb.AppendLine($"entity = context.{PluralizedEntityName}.Take(5).OrderBy(o => o.Id).First();");
                }
                sb.AppendLine("});");

                var propertyNamesFields = string.Join(",", propertyNames.Select(s => s.ToCamelCase()));

                sb.AppendLine();
                sb.AppendLine("// Act");
                sb.AppendLine($"var response = await GetResponseAsObjectAsync<{DtoName}>(");
                using (sb.Indent())
                {

                    sb.AppendLine($"$\"/{{RouteConsts.{EntityName}}}/{{entity.Id}}?fields={propertyNamesFields}\"");
                }
                sb.AppendLine(");");
                sb.AppendLine();
                sb.AppendLine("// Assert");

                foreach (var propertyName in propertyNames)
                {
                    sb.AppendLine($"Assert.Equal(response.{propertyName}, entity.{propertyName});");
                }
            }
            sb.AppendLine("}");
            sb.AppendLine();
        }

        private void GenerateGetEntityWithInvalidParameterReturnBadRequest(IndentedStringBuilder sb)
        {
            sb.AppendLine($"[Fact(DisplayName = \"Api - Get {EntityName} with invalid parameter and return bad request\")]");
            sb.AppendLine($"public async Task Get{EntityName}WithInvalidParameterReturnBadRequest()");
            sb.AppendLine("{");
            using (sb.Indent())
            {
                sb.AppendLine("// Act");
                sb.AppendLine($"var response = await GetResponseAsObjectAsync<ErrorResponse>(");
                using (sb.Indent())
                {
                    sb.AppendLine($"$\"/{{RouteConsts.{EntityName}}}/{{default({EntityKey})}}\",");
                    sb.AppendLine("HttpStatusCode.BadRequest");
                }
                sb.AppendLine(");");
                sb.AppendLine();
                sb.AppendLine("// Assert");
                sb.AppendLine("Assert.Equal(response.Message, TnfAppControllerErrors.AppControllerOnGetError.ToString());");
                sb.AppendLine("Assert.Equal(response.DetailedMessage, TnfAppControllerErrors.AppControllerOnGetError.ToString());");
                sb.AppendLine("Assert.Contains(response.Details, n => n.Message == TnfAppApplicationErrors.AppApplicationOnInvalidIdError.ToString());");
            }
            sb.AppendLine("}");
            sb.AppendLine();
        }

        private void GenerateGetEntityWhenNotExistsAndReturnNotFound(IndentedStringBuilder sb)
        {
            sb.AppendLine($"[Fact(DisplayName = \"Api - Get {EntityName} when not exists and return not found\")]");
            sb.AppendLine($"public async Task Get{EntityName}WhenNotExistsAndReturnNotFound()");
            sb.AppendLine("{");
            using (sb.Indent())
            {
                sb.AppendLine("// Arrange");
                if (EntityKey == "Guid")
                {
                    sb.AppendLine("var notFoundId = Guid.NewGuid();");
                }
                else
                {
                    sb.AppendLine($"var notFoundId = default({EntityKey});");
                    sb.AppendLine();
                    sb.AppendLine($"IocManager.UsingDbContext<{Configuration.Orm.ContextClassName}>(context =>");
                    sb.AppendLine("{");
                    using (sb.Indent())
                        sb.AppendLine($"notFoundId = context.{PluralizedEntityName}.Max(m => m.Id) + 1;");
                    sb.AppendLine("});");
                }

                sb.AppendLine();

                sb.AppendLine("// Act");
                sb.AppendLine($"var response = await GetResponseAsObjectAsync<ErrorResponse>(");
                using (sb.Indent())
                {
                    sb.AppendLine($"$\"/{{RouteConsts.{EntityName}}}/{{notFoundId}}\",");
                    sb.AppendLine("HttpStatusCode.NotFound");
                }
                sb.AppendLine(");");
                sb.AppendLine();
                sb.AppendLine("// Assert");
                sb.AppendLine("Assert.Equal(response.Message, TnfAppControllerErrors.AppControllerOnGetError.ToString());");
                sb.AppendLine("Assert.Equal(response.DetailedMessage, TnfAppControllerErrors.AppControllerOnGetError.ToString());");
                sb.AppendLine("Assert.Contains(response.Details, n => n.Message == TnfAppDomainErrors.AppDomainOnGetCouldNotFindError.ToString());");
            }
            sb.AppendLine("}");
            sb.AppendLine();
        }

        private void GenerateDeleteEntityWithSucess(IndentedStringBuilder sb)
        {
            sb.AppendLine($"[Fact(DisplayName = \"Api - Delete {EntityName} with sucess\")]");
            sb.AppendLine($"public async Task Delete{EntityName}WithSucess()");
            sb.AppendLine("{");
            using (sb.Indent())
            {
                sb.AppendLine();

                string defaultValue = "";
                switch (EntityKey)
                {
                    case "int":
                        defaultValue = "0";
                        break;
                    case "long":
                        defaultValue = "0";
                        break;
                    case "string":
                        defaultValue = "";
                        break;
                    case "Guid":
                        defaultValue = "Guid.Empty";
                        break;
                    case "char":
                        defaultValue = "'A'";
                        break;
                    default:
                        break;
                };
                sb.AppendLine($"var id = {defaultValue};");
                sb.AppendLine();
                sb.AppendLine($"IocManager.UsingDbContext<{Configuration.Orm.ContextClassName}>(context =>");
                sb.AppendLine("{");
                using (sb.Indent())
                {
                    sb.AppendLine($"id = context.{PluralizedEntityName}.First().Id;");
                }
                sb.AppendLine("});");
                sb.AppendLine();
                sb.AppendLine("// Act");
                sb.AppendLine("var response = await DeleteResponseAsObjectAsync<ErrorResponse>(");
                using (sb.Indent())
                {
                    sb.AppendLine($"$\"{{RouteConsts.{EntityName}}}/{{id}}\",");
                    sb.AppendLine("HttpStatusCode.OK");
                }
                sb.AppendLine(");");
                sb.AppendLine();
                sb.AppendLine("// Assert");
                sb.AppendLine("Assert.Null(response);");
                sb.AppendLine();
                sb.AppendLine("// Act");
                sb.AppendLine("var responseGet = await GetResponseAsObjectAsync<ErrorResponse>(");
                using (sb.Indent())
                {
                    sb.AppendLine($"$\"{{RouteConsts.{EntityName}}}/{{id}}\",");
                    sb.AppendLine("HttpStatusCode.NotFound");
                }
                sb.AppendLine(");");
                sb.AppendLine();
                sb.AppendLine("// Assert");
                sb.AppendLine("Assert.Contains(responseGet.Details, a => a.Message == TnfAppDomainErrors.AppDomainOnGetCouldNotFindError.ToString());");
                sb.AppendLine();
            }
            sb.AppendLine("}");
            sb.AppendLine();
        }

        private void GenerateDeleteEntityWithInvalidParameterReturnBadRequest(IndentedStringBuilder sb)
        {
            sb.AppendLine($"[Fact(DisplayName = \"Api - Delete {EntityName} with invalid parameter return bad request\")]");
            sb.AppendLine($"public async Task Delete{EntityName}WithInvalidParameterReturnBadRequest()");
            sb.AppendLine("{");
            using (sb.Indent())
            {
                sb.AppendLine("// Act");
                sb.AppendLine("var response = await DeleteResponseAsObjectAsync<ErrorResponse>(");
                using (sb.Indent())
                {
                    sb.AppendLine($"$\"{{RouteConsts.{EntityName}}}/{{default({EntityKey})}}\",");
                    sb.AppendLine("HttpStatusCode.BadRequest");
                }
                sb.AppendLine(");");
                sb.AppendLine();

                sb.AppendLine("// Assert");
                sb.AppendLine("Assert.Equal(response.Message, TnfAppControllerErrors.AppControllerOnDeleteError.ToString());");
                sb.AppendLine("Assert.Equal(response.DetailedMessage, TnfAppControllerErrors.AppControllerOnDeleteError.ToString());");
                sb.AppendLine("Assert.Contains(response.Details, n => n.Message == TnfAppApplicationErrors.AppApplicationOnInvalidIdError.ToString());");
            }
            sb.AppendLine("}");
            sb.AppendLine();
        }

        private void GenerateDeleteEntityWhenNotExistsReturnNotFound(IndentedStringBuilder sb)
        {
            sb.AppendLine($"[Fact(DisplayName = \"Api - Delete {EntityName} when not exists return not found\")]");
            sb.AppendLine($"public async Task Delete{EntityName}WhenNotExistsReturnNotFound()");
            sb.AppendLine("{");
            using (sb.Indent())
            {
                sb.AppendLine("// Arrange");
                if (EntityKey == "Guid")
                {
                    sb.AppendLine("var notFoundId = Guid.NewGuid();");
                }
                else
                {
                    sb.AppendLine($"var notFoundId = default({EntityKey});");
                    sb.AppendLine();
                    sb.AppendLine($"IocManager.UsingDbContext<{Configuration.Orm.ContextClassName}>(context =>");
                    sb.AppendLine("{");
                    using (sb.Indent())
                        sb.AppendLine($"notFoundId = context.{PluralizedEntityName}.Max(m => m.Id) + 1;");
                    sb.AppendLine("});");
                }

                sb.AppendLine();

                sb.AppendLine("// Act");
                sb.AppendLine("var response = await DeleteResponseAsObjectAsync<ErrorResponse>(");
                using (sb.Indent())
                {
                    sb.AppendLine($"$\"{{RouteConsts.{EntityName}}}/{{notFoundId}}\",");
                    sb.AppendLine("HttpStatusCode.NotFound");
                }
                sb.AppendLine(");");
                sb.AppendLine();

                sb.AppendLine("// Assert");
                sb.AppendLine("Assert.Equal(response.Message, TnfAppControllerErrors.AppControllerOnDeleteError.ToString());");
                sb.AppendLine("Assert.Equal(response.DetailedMessage, TnfAppControllerErrors.AppControllerOnDeleteError.ToString());");
                sb.AppendLine("Assert.Contains(response.Details, a => a.Message == TnfAppDomainErrors.AppDomainOnDeleteCouldNotFindError.ToString());");
            }
            sb.AppendLine("}");
            sb.AppendLine();
        }

        private void GeneratePostEntityWithSucess(IndentedStringBuilder sb, IEntityType entityType)
        {
            sb.AppendLine($"[Fact(DisplayName = \"Api - Post {EntityName} with sucess\")]");
            sb.AppendLine($"public async Task Post{EntityName}WithSucess()");
            sb.AppendLine("{");
            using (sb.Indent())
            {
                sb.AppendLine("// Arrange");

                WriteDto(sb, entityType, forUpdate: false, invalidData: false);

                sb.AppendLine("// Act");
                sb.AppendLine($"var response = await PostResponseAsObjectAsync<{DtoName}, {DtoName}>(");
                using (sb.Indent())
                {
                    sb.AppendLine($"$\"{{RouteConsts.{EntityName}}}\",");
                    sb.AppendLine("dto");
                }
                sb.AppendLine(");");
                sb.AppendLine();

                sb.AppendLine("// Assert");
                foreach (var propertyName in ReturnPropertiesNamesFromEntity(entityType))
                {
                    sb.AppendLine($"Assert.Equal(response.{propertyName}, dto.{propertyName});");
                }
            }
            sb.AppendLine("}");
            sb.AppendLine();
        }

        private void GeneratePostNullEntityAndReturnBadRequest(IndentedStringBuilder sb)
        {
            sb.AppendLine($"[Fact(DisplayName = \"Api - Post null {EntityName} and return bad request\")]");
            sb.AppendLine($"public async Task PostNull{EntityName}AndReturnBadRequest()");
            sb.AppendLine("{");
            using (sb.Indent())
            {
                sb.AppendLine("// Act");
                sb.AppendLine($"var response = await PostResponseAsObjectAsync<{DtoName}, ErrorResponse>(");
                using (sb.Indent())
                {
                    sb.AppendLine($"$\"{{RouteConsts.{EntityName}}}\",");
                    sb.AppendLine("null,");
                    sb.AppendLine("HttpStatusCode.BadRequest");
                }
                sb.AppendLine(");");
                sb.AppendLine();

                sb.AppendLine("// Assert");
                sb.AppendLine("Assert.Equal(response.Message, TnfAppControllerErrors.AppControllerOnPostError.ToString());");
                sb.AppendLine("Assert.Equal(response.DetailedMessage, TnfAppControllerErrors.AppControllerOnPostError.ToString());");
                sb.AppendLine();
                sb.AppendLine("Assert.Contains(response.Details, n => n.Message == TnfAppApplicationErrors.AppApplicationOnInvalidDtoError.ToString());");
            }
            sb.AppendLine("}");
            sb.AppendLine();
        }

        private void GeneratePostEmptyEntityAndReturnBadRequest(IndentedStringBuilder sb, IEntityType entityType)
        {
            var enumerationsErrors = Configuration.GetEntityErrorsEnumeration(RelationalTypeMapper, entityType, CSharpUtilities);
            if (enumerationsErrors.Count <= 1)
                return;

            sb.AppendLine($"[Fact(DisplayName = \"Api - Post empty {EntityName} and return bad request\")]");
            sb.AppendLine($"public async Task PostEmpty{EntityName}AndReturnBadRequest()");
            sb.AppendLine("{");
            using (sb.Indent())
            {
                sb.AppendLine("// Arrange");
                sb.AppendLine($"var dto = new {DtoName}();");
                sb.AppendLine();
                sb.AppendLine("// Act");
                sb.AppendLine($"var response = await PostResponseAsObjectAsync<{DtoName}, ErrorResponse>(");
                using (sb.Indent())
                {
                    sb.AppendLine($"$\"{{RouteConsts.{EntityName}}}\",");
                    sb.AppendLine("dto,");
                    sb.AppendLine("HttpStatusCode.BadRequest");
                }
                sb.AppendLine(");");
                sb.AppendLine();

                sb.AppendLine("// Assert");
                sb.AppendLine("Assert.Equal(response.Message, TnfAppControllerErrors.AppControllerOnPostError.ToString());");
                sb.AppendLine("Assert.Equal(response.DetailedMessage, TnfAppControllerErrors.AppControllerOnPostError.ToString());");
                sb.AppendLine();

                foreach (var error in enumerationsErrors.Where(w => w.SpecificationType == SpecificationTypes.MustHave))
                {
                    sb.AppendLine($"Assert.Contains(response.Details, a => a.Message == {EntityName}.EntityError.{error.EnumerationName}.ToString());");
                }
            }
            sb.AppendLine("}");
            sb.AppendLine();
        }



        private bool hasStringType(IEntityType entityType)
        {
            bool result = false;
            var properties = Configuration.GetOrderedProperties(entityType, CSharpUtilities).ToList();
            foreach (var property in properties)
            {
                if (property.ClrType == typeof(string))
                {
                    return true;
                }
            }
            return result;

        }

        private void GeneratePostInvalidEntityAndReturnBadRequest(IndentedStringBuilder sb, IEntityType entityType)
        {
            var enumerationsErrors = Configuration.GetEntityErrorsEnumeration(RelationalTypeMapper, entityType, CSharpUtilities);
            if ((enumerationsErrors.Count <= 1) || !(hasStringType(entityType)))
                return;

            sb.AppendLine($"[Fact(DisplayName = \"Api - Post invalid {EntityName} and return bad request\")]");
            sb.AppendLine($"public async Task PostInvalid{EntityName}AndReturnBadRequest()");
            sb.AppendLine("{");
            using (sb.Indent())
            {
                sb.AppendLine("// Arrange");

                WriteDto(sb, entityType, forUpdate: false, invalidData: true);

                sb.AppendLine("// Act");
                sb.AppendLine($"var response = await PostResponseAsObjectAsync<{DtoName}, ErrorResponse>(");
                using (sb.Indent())
                {
                    sb.AppendLine($"$\"{{RouteConsts.{EntityName}}}\",");
                    sb.AppendLine("dto,");
                    sb.AppendLine("HttpStatusCode.BadRequest");
                }
                sb.AppendLine(");");
                sb.AppendLine();

                sb.AppendLine("// Assert");
                sb.AppendLine("Assert.Equal(response.Message, TnfAppControllerErrors.AppControllerOnPostError.ToString());");
                sb.AppendLine("Assert.Equal(response.DetailedMessage, TnfAppControllerErrors.AppControllerOnPostError.ToString());");
                sb.AppendLine();

                foreach (var error in enumerationsErrors.Where(w => w.SpecificationType != SpecificationTypes.MustHave))
                {
                    sb.AppendLine($"Assert.Contains(response.Details, a => a.Message == {EntityName}.EntityError.{error.EnumerationName}.ToString());");
                }
            }
            sb.AppendLine("}");
            sb.AppendLine();
        }

        private void GeneratePutEntityWithSucess(IndentedStringBuilder sb, IEntityType entityType)
        {
            sb.AppendLine($"[Fact(DisplayName = \"Api - Put {EntityName} with sucess\")]");
            sb.AppendLine($"public async Task Put{EntityName}WithSucess()");
            sb.AppendLine("{");
            using (sb.Indent())
            {
                sb.AppendLine("// Arrange");

                WriteDto(sb, entityType, forUpdate: true, invalidData: false);

                sb.AppendLine();
                sb.AppendLine("// Act");
                sb.AppendLine($"await PutResponseAsObjectAsync<{DtoName}, {DtoName}>(");
                using (sb.Indent())
                {
                    sb.AppendLine($"$\"{{RouteConsts.{EntityName}}}/{{dto.Id}}\",");
                    sb.AppendLine("dto");
                }
                sb.AppendLine(");");
                sb.AppendLine();

                sb.AppendLine($"var response = await GetResponseAsObjectAsync<{DtoName}>(");
                using (sb.Indent())
                {
                    sb.AppendLine($"$\"{{RouteConsts.{EntityName}}}/{{dto.Id}}\"");
                }
                sb.AppendLine(");");

                sb.AppendLine();
                sb.AppendLine("// Assert");

                foreach (var propertyName in ReturnPropertiesNamesFromEntity(entityType))
                {
                    sb.AppendLine($"Assert.Equal(response.{propertyName}, dto.{propertyName});");
                }
            }
            sb.AppendLine("}");
            sb.AppendLine();
        }

        private void GeneratePutEntityWithInvalidParameterReturnBadRequest(IndentedStringBuilder sb)
        {
            sb.AppendLine($"[Fact(DisplayName = \"Api - Put {EntityName} with invalid parameter and return bad request\")]");
            sb.AppendLine($"public async Task Put{EntityName}WithInvalidParameterReturnBadRequest()");
            sb.AppendLine("{");
            using (sb.Indent())
            {
                sb.AppendLine("// Act");
                sb.AppendLine($"var response = await PutResponseAsObjectAsync<{DtoName}, ErrorResponse>(");
                using (sb.Indent())
                {
                    sb.AppendLine($"$\"{{RouteConsts.{EntityName}}}/{{default({EntityKey})}}\",");
                    sb.AppendLine($"new {DtoName}(),");
                    sb.AppendLine("HttpStatusCode.BadRequest");
                }
                sb.AppendLine(");");

                sb.AppendLine();
                sb.AppendLine("// Assert");
                sb.AppendLine("Assert.Equal(response.Message, TnfAppControllerErrors.AppControllerOnPutError.ToString());");
                sb.AppendLine("Assert.Equal(response.DetailedMessage, TnfAppControllerErrors.AppControllerOnPutError.ToString());");
                sb.AppendLine("Assert.Contains(response.Details, n => n.Message == TnfAppApplicationErrors.AppApplicationOnInvalidIdError.ToString());");
            }
            sb.AppendLine("}");
            sb.AppendLine();
        }

        private void GeneratePutNullEntityAndReturnBadRequest(IndentedStringBuilder sb)
        {
            sb.AppendLine($"[Fact(DisplayName = \"Api - Put null {EntityName} and return bad request\")]");
            sb.AppendLine($"public async Task PutNull{EntityName}AndReturnBadRequest()");
            sb.AppendLine("{");
            using (sb.Indent())
            {
                sb.AppendLine("// Act");
                sb.AppendLine($"var response = await PutResponseAsObjectAsync<{DtoName}, ErrorResponse>(");
                using (sb.Indent())
                {
                    sb.AppendLine($"$\"{{RouteConsts.{EntityName}}}/{{default({EntityKey})}}\",");
                    sb.AppendLine($"null,");
                    sb.AppendLine("HttpStatusCode.BadRequest");
                }
                sb.AppendLine(");");

                sb.AppendLine();
                sb.AppendLine("// Assert");
                sb.AppendLine("Assert.Equal(response.Message, TnfAppControllerErrors.AppControllerOnPutError.ToString());");
                sb.AppendLine("Assert.Equal(response.DetailedMessage, TnfAppControllerErrors.AppControllerOnPutError.ToString());");
                sb.AppendLine("Assert.Contains(response.Details, n => n.Message == TnfAppApplicationErrors.AppApplicationOnInvalidIdError.ToString());");
                sb.AppendLine("Assert.Contains(response.Details, n => n.Message == TnfAppApplicationErrors.AppApplicationOnInvalidDtoError.ToString());");
            }
            sb.AppendLine("}");
            sb.AppendLine();
        }

        private void GeneratePutEmptyEntityAndReturnBadRequest(IndentedStringBuilder sb, IEntityType entityType)
        {
            var enumerationsErrors = Configuration.GetEntityErrorsEnumeration(RelationalTypeMapper, entityType, CSharpUtilities);
            if (enumerationsErrors.Count <= 1)
                return;

            sb.AppendLine($"[Fact(DisplayName = \"Api - Put empty {EntityName} and return bad request\")]");
            sb.AppendLine($"public async Task PutEmpty{EntityName}AndReturnBadRequest()");
            sb.AppendLine("{");
            using (sb.Indent())
            {
                sb.AppendLine("// Arrange");

                WriteDto(sb, entityType, forUpdate: true, invalidData: false);

                sb.AppendLine();
                sb.AppendLine("// Act");
                sb.AppendLine($"var response = await PutResponseAsObjectAsync<{DtoName}, ErrorResponse>(");
                using (sb.Indent())
                {
                    sb.AppendLine($"$\"{{RouteConsts.{EntityName}}}/{{dto.Id}}\",");
                    sb.AppendLine($"new {DtoName}(),");
                    sb.AppendLine("HttpStatusCode.BadRequest");
                }
                sb.AppendLine(");");

                sb.AppendLine();
                sb.AppendLine("// Assert");
                sb.AppendLine("Assert.Equal(response.Message, TnfAppControllerErrors.AppControllerOnPutError.ToString());");
                sb.AppendLine("Assert.Equal(response.DetailedMessage, TnfAppControllerErrors.AppControllerOnPutError.ToString());");
                sb.AppendLine();

                foreach (var error in enumerationsErrors.Where(w => w.SpecificationType == SpecificationTypes.MustHave))
                {
                    sb.AppendLine($"Assert.Contains(response.Details, a => a.Message == {EntityName}.EntityError.{error.EnumerationName}.ToString());");
                }
            }
            sb.AppendLine("}");
            sb.AppendLine();
        }

        private void GeneratePutInvalidEntityAndReturnBadRequest(IndentedStringBuilder sb, IEntityType entityType)
        {
            var enumerationsErrors = Configuration.GetEntityErrorsEnumeration(RelationalTypeMapper, entityType, CSharpUtilities);
            if ((enumerationsErrors.Count <= 1) || !(hasStringType(entityType)))
                return;

            sb.AppendLine($"[Fact(DisplayName = \"Api - Put invalid {EntityName} and return bad request\")]");
            sb.AppendLine($"public async Task PutInvalid{EntityName}AndReturnBadRequest()");
            sb.AppendLine("{");
            using (sb.Indent())
            {
                sb.AppendLine("// Arrange");

                WriteDto(sb, entityType, forUpdate: true, invalidData: true);

                sb.AppendLine();
                sb.AppendLine("// Act");
                sb.AppendLine($"var response = await PutResponseAsObjectAsync<{DtoName}, ErrorResponse>(");
                using (sb.Indent())
                {
                    sb.AppendLine($"$\"{{RouteConsts.{EntityName}}}/{{dto.Id}}\",");
                    sb.AppendLine("dto,");
                    sb.AppendLine("HttpStatusCode.BadRequest");
                }
                sb.AppendLine(");");
                sb.AppendLine();

                sb.AppendLine("// Assert");
                sb.AppendLine("Assert.Equal(response.Message, TnfAppControllerErrors.AppControllerOnPutError.ToString());");
                sb.AppendLine("Assert.Equal(response.DetailedMessage, TnfAppControllerErrors.AppControllerOnPutError.ToString());");
                sb.AppendLine();

                foreach (var error in enumerationsErrors.Where(w => w.SpecificationType != SpecificationTypes.MustHave))
                {
                    sb.AppendLine($"Assert.Contains(response.Details, a => a.Message == {EntityName}.EntityError.{error.EnumerationName}.ToString());");
                }
            }
            sb.AppendLine("}");
            sb.AppendLine();
        }

        private void GeneratePutEntityWhenNotExistsAndReturnNotFound(IndentedStringBuilder sb, IEntityType entityType)
        {
            sb.AppendLine($"[Fact(DisplayName = \"Api - Put {EntityName} when not exists and return not found\")]");
            sb.AppendLine($"public async Task Put{EntityName}WhenNotExistsAndReturnNotFound()");
            sb.AppendLine("{");
            using (sb.Indent())
            {
                sb.AppendLine("// Arrange");

                WriteDto(sb, entityType, forUpdate: false, invalidData: false);

                sb.AppendLine();

                sb.AppendLine("// Act");
                sb.AppendLine($"var response = await PutResponseAsObjectAsync<{DtoName}, ErrorResponse>(");
                using (sb.Indent())
                {
                    sb.AppendLine($"$\"{{RouteConsts.{EntityName}}}/{{dto.Id}}\",");
                    sb.AppendLine($"dto,");
                    sb.AppendLine("HttpStatusCode.NotFound");
                }
                sb.AppendLine(");");

                sb.AppendLine();
                sb.AppendLine("// Assert");
                sb.AppendLine("Assert.Equal(response.Message, TnfAppControllerErrors.AppControllerOnPutError.ToString());");
                sb.AppendLine("Assert.Equal(response.DetailedMessage, TnfAppControllerErrors.AppControllerOnPutError.ToString());");
                sb.AppendLine("Assert.Contains(response.Details, n => n.Message == TnfAppDomainErrors.AppDomainOnUpdateCouldNotFindError.ToString());");
            }
            sb.AppendLine("}");
        }

        private List<string> ReturnPropertiesNamesFromEntity(IEntityType entityType)
        {
            var properties = Configuration.GetOrderedProperties(entityType, CSharpUtilities)
                .Where(w => !w.IsNullable).OrderBy(p => p.Scaffolding().ColumnOrdinal);

            var propertyNames = new List<string>();
            foreach (var property in properties)
            {
                var propertyName = Configuration.GetPropertyName(entityType, property);
                propertyNames.Add(propertyName);
            }

            return propertyNames;
        }

        private void WriteDto(IndentedStringBuilder sb, IEntityType entityType, bool forUpdate, bool invalidData)
        {
            sb.AppendLine($"var dto = new {DtoName}()");
            sb.AppendLine("{");

            using (sb.Indent())
            {
                var properties = Configuration.GetOrderedProperties(entityType, CSharpUtilities).ToList();

                foreach (var property in properties)
                {
                    var propertyName = Configuration.GetPropertyName(entityType, property);

                    if (propertyName == "Id" && forUpdate)
                        continue;

                    var value = property.ClrType.GetLiteralTypeRandomValue(RelationalTypeMapper, property, invalid: invalidData);
                    sb.AppendLine($"{propertyName} = {value},");
                }
            }

            sb.AppendLine("};");
            sb.AppendLine();

            if (forUpdate)
            {
                sb.AppendLine($"IocManager.UsingDbContext<{Configuration.Orm.ContextClassName}>(context =>");
                sb.AppendLine("{");
                using (sb.Indent())
                {
                    sb.AppendLine($"dto.Id = context.{PluralizedEntityName}.First().Id;");
                }
                sb.AppendLine("});");
            }
        }

        private void WriteEntityForInMemorySetup(IndentedStringBuilder sb, IEntityType entityType, int index)
        {
            index++;

            sb.AppendLine($"var entity{index} = new {EntityName}()");
            sb.AppendLine("{");

            using (sb.Indent())
            {
                var properties = entityType.GetProperties().OrderBy(p => p.Scaffolding().ColumnOrdinal);

                foreach (var property in properties)
                {
                    var propertyName = Configuration.GetPropertyName(entityType, property);
                    if (propertyName == "Id")
                    {
                        if (property.ClrType == typeof(Guid))
                            sb.AppendLine($"{propertyName} = Guid.NewGuid(),");
                        else
                            sb.AppendLine($"{propertyName} = {index},");

                        continue;
                    }

                    var value = property.ClrType.GetLiteralTypeRandomValue(RelationalTypeMapper, property, invalid: false);
                    sb.AppendLine($"{propertyName} = {value},");
                }
            }

            sb.AppendLine("};");
            sb.AppendLine($"mock.Add(entity{index});");
            sb.AppendLine();
        }
    }
}