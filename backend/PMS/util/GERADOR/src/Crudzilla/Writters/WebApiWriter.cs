﻿using Crudzilla.Configuration;
using JetBrains.Annotations;
using Microsoft.EntityFrameworkCore.Internal;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Scaffolding.Internal;

namespace Crudzilla.Writters
{
    internal class WebApiWriter
    {
        private CrudzillaConfig Configuration { get; }
        private ICSharpUtilities CSharpUtilities { get; }

        private string EntityName { get; set; }
        private string DtoName { get; set; }
        private string ApplicationServiceName { get; set; }
        private string EntityKey { get; set; }

        private IndentedStringBuilder _sb;

        public WebApiWriter(
            [NotNull] CrudzillaConfig configuration,
            [NotNull] ICSharpUtilities cSharpUtilities)
        {
            Configuration = Check.NotNull(configuration, nameof(configuration));
            CSharpUtilities = Check.NotNull(cSharpUtilities, nameof(cSharpUtilities));
        }

        public virtual string WriteCodeScaffold([NotNull] IEntityType entityType)
        {
            Check.NotNull(entityType, nameof(entityType));

            //--
            EntityName = Configuration.GetEntityNameOrDefault(entityType);
            DtoName = Configuration.GetDtoName(entityType);
            ApplicationServiceName = Configuration.GetApplicationServiceName(entityType);
            EntityKey = entityType.GetEntityKeyType(CSharpUtilities);
            //--

            _sb = new IndentedStringBuilder();

            _sb.AppendLine(Configuration.HeaderGeneratedFile);

            _sb.AppendLine("using System;");
            _sb.AppendLine("using Microsoft.AspNetCore.Mvc;");
            _sb.AppendLine("using Tnf.App.AspNetCore.Mvc.Controllers;");
            _sb.AppendLine("using Tnf.App.Dto.Request;");
            _sb.AppendLine("using Tnf.App;");
            _sb.AppendLine("using System.Threading.Tasks;");
            _sb.AppendLine($"using {Configuration.ApplicationService.InterfaceNamespace};");
            _sb.AppendLine($"using {Configuration.DataTransfer.Namespace};");

            // Os namespaces podem ser iguais
            if (Configuration.Localization.Namespace == Configuration.Entity.ConstantNamespace)
            {
                _sb.AppendLine($"using {Configuration.Localization.Namespace};");
            }
            else
            {
                _sb.AppendLine($"using {Configuration.Localization.Namespace};");
                _sb.AppendLine($"using {Configuration.Entity.ConstantNamespace};");
            }

            _sb.AppendLine();
            _sb.AppendLine($"namespace {Configuration.WebApi.Namespace}.Controllers");
            _sb.AppendLine("{");
            using (_sb.Indent())
            {
                AddClass();
            }
            _sb.AppendLine("}");

            return _sb.ToString();
        }

        public virtual void AddClass()
        {
            _sb.AppendLine($"public abstract class Scaffold{EntityName}Controller : TnfAppController");
            _sb.AppendLine("{");
            using (_sb.Indent())
            {
                AddMembers();
                AddConstructor();
                AddMethods();
            }
            _sb.AppendLine("}");
        }

        public virtual void AddMethods()
        {
            _sb.AppendLine($"public virtual async Task<IActionResult> Get([FromQuery]GetAll{DtoName} requestDto)");
            _sb.AppendLine("{");
            using (_sb.Indent())
            {
                _sb.AppendLine($"var response = await {ApplicationServiceName}.GetAll(requestDto);");
                _sb.AppendLine();
                _sb.AppendLine($"return CreateResponseOnGetAll<{DtoName}, {EntityKey}>(response, EntityNames.{EntityName});");
            }
            _sb.AppendLine("}");
            _sb.AppendLine();

            _sb.AppendLine($"public virtual async Task<IActionResult> Get({EntityKey} id, [FromQuery]RequestDto<{EntityKey}> requestDto)");
            _sb.AppendLine("{");
            using (_sb.Indent())
            {
                _sb.AppendLine($"var response = await {ApplicationServiceName}.Get(requestDto.WithId(id));");
                _sb.AppendLine();
                _sb.AppendLine($"return CreateResponseOnGet<{DtoName}, {EntityKey}>(response, EntityNames.{EntityName});");
            }
            _sb.AppendLine("}");
            _sb.AppendLine();

            _sb.AppendLine($"public virtual async Task<IActionResult> Post([FromBody]{DtoName} dto)");
            _sb.AppendLine("{");
            using (_sb.Indent())
            {
                _sb.AppendLine($"var response = await {ApplicationServiceName}.Create(dto);");
                _sb.AppendLine();
                _sb.AppendLine($"return CreateResponseOnPost<{DtoName}, {EntityKey}>(response, EntityNames.{EntityName});");
            }
            _sb.AppendLine("}");
            _sb.AppendLine();

            _sb.AppendLine($"public virtual async Task<IActionResult> Put({EntityKey} id, [FromBody]{DtoName} dto)");
            _sb.AppendLine("{");
            using (_sb.Indent())
            {
                _sb.AppendLine($"var response = await {ApplicationServiceName}.Update(id, dto);");
                _sb.AppendLine();
                _sb.AppendLine($"return CreateResponseOnPut<{DtoName}, {EntityKey}>(response, EntityNames.{EntityName});");
            }
            _sb.AppendLine("}");
            _sb.AppendLine();

            _sb.AppendLine($"public virtual async Task<IActionResult> Delete({EntityKey} id)");
            _sb.AppendLine("{");
            using (_sb.Indent())
            {
                _sb.AppendLine($"await {ApplicationServiceName}.Delete(id);");
                _sb.AppendLine();
                _sb.AppendLine($"return CreateResponseOnDelete<{DtoName}, {EntityKey}>(EntityNames.{EntityName});");
            }
            _sb.AppendLine("}");
        }

        public virtual void AddConstructor()
        {
            _sb.AppendLine($"protected Scaffold{EntityName}Controller(I{ApplicationServiceName} {ApplicationServiceName.ToCamelCase()})");
            _sb.AppendLine("{");
            using (_sb.Indent())
            {
                _sb.AppendLine($"LocalizationSourceName = {Configuration.Localization.ConstantClassName};");
                _sb.AppendLine();
                _sb.AppendLine($"{ApplicationServiceName} = Check.NotNull({ApplicationServiceName.ToCamelCase()}, nameof({ApplicationServiceName.ToCamelCase()}));");
            }
            _sb.AppendLine("}");
            _sb.AppendLine();
        }

        public virtual void AddMembers()
        {
            _sb.AppendLine($"protected readonly I{ApplicationServiceName} {ApplicationServiceName};");
            _sb.AppendLine();
        }

        public virtual string WriteCode()
        {
            var sb = new IndentedStringBuilder();

            sb.AppendLine("using System;");
            sb.AppendLine($"using {Configuration.ApplicationService.InterfaceNamespace};");
            sb.AppendLine($"using {Configuration.DataTransfer.Namespace};");
            sb.AppendLine($"using Tnf.App.Dto.Request;");
            sb.AppendLine("using Microsoft.AspNetCore.Mvc;");
            sb.AppendLine("using System.Threading.Tasks;");
            sb.AppendLine();
            sb.AppendLine($"namespace {Configuration.WebApi.Namespace}.Controllers");
            sb.AppendLine("{");
            using (sb.Indent())
            {
                sb.AppendLine($"[Route({Configuration.WebApi.RouteConstantsClassName}.{EntityName})]");
                sb.AppendLine($"public class {EntityName}Controller : Scaffold{EntityName}Controller");
                sb.AppendLine("{");
                using (sb.Indent())
                {
                    sb.AppendLine($"public {EntityName}Controller(");
                    using (sb.Indent())
                    {
                        sb.AppendLine($"I{ApplicationServiceName} {ApplicationServiceName.ToCamelCase()})");
                        sb.AppendLine($": base({ApplicationServiceName.ToCamelCase()})");
                    }
                    sb.AppendLine("{");
                    sb.AppendLine("}");
                    sb.AppendLine();

                    sb.AppendLine("[HttpGet]");
                    sb.AppendLine($"public override Task<IActionResult> Get([FromQuery] GetAll{DtoName} requestDto)");
                    sb.AppendLine("{");
                    using (sb.Indent())
                    {
                        sb.AppendLine("return base.Get(requestDto);");
                    }
                    sb.AppendLine("}");
                    sb.AppendLine();

                    sb.AppendLine("[HttpGet(\"{id}\")]");
                    sb.AppendLine($"public override Task<IActionResult> Get({EntityKey} id, [FromQuery] RequestDto<{EntityKey}> requestDto)");
                    sb.AppendLine("{");
                    using (sb.Indent())
                    {
                        sb.AppendLine("return base.Get(id, requestDto);");
                    }
                    sb.AppendLine("}");
                    sb.AppendLine();

                    sb.AppendLine("[HttpPut(\"{id}\")]");
                    sb.AppendLine($"public override Task<IActionResult> Put({EntityKey} id, [FromBody] {DtoName} dto)");
                    sb.AppendLine("{");
                    using (sb.Indent())
                    {
                        sb.AppendLine("return base.Put(id, dto);");
                    }
                    sb.AppendLine("}");
                    sb.AppendLine();

                    sb.AppendLine("[HttpPost]");
                    sb.AppendLine($"public override Task<IActionResult> Post([FromBody] {DtoName} dto)");
                    sb.AppendLine("{");
                    using (sb.Indent())
                    {
                        sb.AppendLine("return base.Post(dto);");
                    }
                    sb.AppendLine("}");
                    sb.AppendLine();

                    sb.AppendLine("[HttpDelete(\"{id}\")]");
                    sb.AppendLine($"public override Task<IActionResult> Delete({EntityKey} id)");
                    sb.AppendLine("{");
                    using (sb.Indent())
                    {
                        sb.AppendLine("return base.Delete(id);");
                    }
                    sb.AppendLine("}");
                }
                sb.AppendLine("}");
            }
            sb.AppendLine("}");

            return sb.ToString();
        }
    }
}
