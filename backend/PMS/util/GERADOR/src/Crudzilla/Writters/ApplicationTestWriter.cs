﻿using Crudzilla.Configuration;
using JetBrains.Annotations;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Microsoft.EntityFrameworkCore.Scaffolding.Internal;
using Microsoft.EntityFrameworkCore.Storage;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Crudzilla.Writters
{
    internal class ApplicationTestWriter
    {
        private CrudzillaConfig Configuration { get; }
        private ICSharpUtilities CSharpUtilities { get; }
        private IRelationalTypeMapper RelationalTypeMapper { get; }

        private string PluralizedEntityName { get; set; }
        public string ApplicationServiceName { get; set; }
        private string EntityName { get; set; }
        private string DtoName { get; set; }
        private string EntityKey { get; set; }

        public ApplicationTestWriter(
            [NotNull] CrudzillaConfig configuration,
            [NotNull] ICSharpUtilities cSharpUtilities,
            [NotNull] IRelationalTypeMapper relationalTypeMapper)
        {
            Configuration = Check.NotNull(configuration, nameof(configuration));
            CSharpUtilities = Check.NotNull(cSharpUtilities, nameof(cSharpUtilities));
            RelationalTypeMapper = Check.NotNull(relationalTypeMapper, nameof(relationalTypeMapper));
        }

        public string WriteCodeApplicationServiceScaffold([NotNull] IEntityType entityType)
        {
            Check.NotNull(entityType, nameof(entityType));

            //--
            EntityName = Configuration.GetEntityNameOrDefault(entityType);
            DtoName = Configuration.GetDtoName(entityType);
            ApplicationServiceName = Configuration.GetApplicationServiceName(entityType);
            PluralizedEntityName = Configuration.GetPluralizedEntityNameOrDefault(entityType);
            EntityKey = entityType.GetEntityKeyType(CSharpUtilities);
            //--

            var sb = new IndentedStringBuilder();

            sb.AppendLine(Configuration.HeaderGeneratedFile);

            sb.AppendLine($"using {Configuration.DataTransfer.Namespace};");
            sb.AppendLine($"using {Configuration.Test.ApplicationService.ModuleTestNamespace};");
            sb.AppendLine($"using {Configuration.ApplicationService.InterfaceNamespace};");
            sb.AppendLine($"using {Configuration.Orm.Namespace};");
            sb.AppendLine($"using {Configuration.Entity.Namespace};");
            sb.AppendLine($"using System;");
            sb.AppendLine($"using System.Linq;");
            sb.AppendLine($"using Tnf.App.TestBase;");
            sb.AppendLine($"using Xunit;");
            sb.AppendLine($"using System.Collections.Generic;");
            sb.AppendLine("using Tnf.App.EntityFrameworkCore.TestBase;");
            sb.AppendLine("using System.Threading.Tasks;");
            sb.AppendLine("using Tnf.App.Dto.Request;");
            sb.AppendLine("using Tnf.App.Domain.Enums;");
            sb.AppendLine("using Tnf.App.Application.Enums;");
            sb.AppendLine();
            sb.AppendLine($"namespace {Configuration.Test.ApplicationService.Namespace}");
            sb.AppendLine("{");
            using (sb.Indent())
            {
                sb.AppendLine($"[Collection(\"{ApplicationServiceName}\")]");
                sb.AppendLine($"public partial class {ApplicationServiceName}Tests : TnfEfCoreIntegratedTestBase<{Configuration.Test.ApplicationService.ModuleTestClassName}>");
                sb.AppendLine("{");
                using (sb.Indent())
                {
                    GeneratedConstructor(sb);
                    GeneratedGetAllEntityWithPagination(sb);
                    GeneratedGetAllEntitySortedAsc(sb);
                    GeneratedGetAllEntitySortedDesc(sb);
                    GeneratedGetEntityWithSucess(sb, entityType);
                    GeneratedGetEntityFieldsWithSucess(sb, entityType);
                    GeneratedGetEntityWithInvalidParameterAndReturnNotification(sb);
                    GeneratedGetEntityWhenNotExistsAndReturnNotification(sb);
                    GeneratedDeleteEntityWithSucess(sb);
                    GeneratedCreateEntityWithSucess(sb, entityType);
                    GeneratedCreateNullEntityAndReturnNotification(sb);
                    GeneratedCreateEmptyEntityAndReturnNotification(sb, entityType);
                    GeneratedUpdateEntityWithSucess(sb, entityType);
                    GeneratedUpdateEmptyEntityAndReturnNotification(sb, entityType);
                    GeneratedUpdateEntityWhenNotExistsAndReturnNotification(sb, entityType);
                }
                sb.AppendLine("}");
            }
            sb.AppendLine("}");

            return sb.ToString();
        }

        public string WriteCodeScaffoldInterface()
        {
            var sb = new IndentedStringBuilder();

            sb.AppendLine(Configuration.HeaderGeneratedFile);

            sb.AppendLine($"using {Configuration.Entity.Namespace};");
            sb.AppendLine($"using System;");
            sb.AppendLine($"using System.Collections.Generic;");
            sb.AppendLine($"using Tnf.App.Builder;");
            sb.AppendLine();
            sb.AppendLine($"namespace {Configuration.Test.ApplicationService.Namespace}");
            sb.AppendLine("{");
            using (sb.Indent())
            {
                sb.AppendLine($"public partial interface I{ApplicationServiceName}Tests");
                sb.AppendLine("{");
                using (sb.Indent())
                {
                    sb.AppendLine($"IEnumerable<{EntityName}> GetCollectionForInitializeInMemoryContext();");
                }
                sb.AppendLine("}");
            }
            sb.AppendLine("}");

            return sb.ToString();
        }

        private void GeneratedConstructor(IndentedStringBuilder sb)
        {
            sb.AppendLine($"private readonly I{ApplicationServiceName} _appService;");
            sb.AppendLine();

            sb.AppendLine($"public {ApplicationServiceName}Tests()");
            sb.AppendLine("{");
            using (sb.Indent())
            {
                sb.AppendLine("var mockCollection = GetCollectionForInitializeInMemoryContext();");
                sb.AppendLine();
                sb.AppendLine($"UsingDbContext<{Configuration.Orm.ContextClassName}>(context => context.{PluralizedEntityName}.AddRange(mockCollection));");
                sb.AppendLine();
                sb.AppendLine($"_appService = Resolve<I{ApplicationServiceName}>();");
            }
            sb.AppendLine("}");
            sb.AppendLine();
        }

        private void GeneratedGetAllEntityWithPagination(IndentedStringBuilder sb)
        {
            sb.AppendLine($"[Fact(DisplayName = \"ApplicationService - Get all {EntityName} with pagination\")]");
            sb.AppendLine($"public async Task GetAll{EntityName}WithPagination()");
            sb.AppendLine("{");
            using (sb.Indent())
            {
                sb.AppendLine("// Act");
                sb.AppendLine($"var response = await _appService.GetAll(new GetAll{EntityName}Dto() {{ PageSize = 5 }});");
                sb.AppendLine();
                sb.AppendLine("// Assert");
                sb.AppendLine("Assert.False(LocalNotification.HasNotification(), string.Join(Environment.NewLine, LocalNotification.GetAll()));");
                sb.AppendLine();
                sb.AppendLine("Assert.Equal(5, response.Items.Count);");
            }
            sb.AppendLine("}");
            sb.AppendLine();
        }

        private void GeneratedGetAllEntitySortedAsc(IndentedStringBuilder sb)
        {
            sb.AppendLine($"[Fact(DisplayName = \"ApplicationService - Get all {EntityName} sorted asc\")]");
            sb.AppendLine($"public async Task GetAll{EntityName}SortedAsc()");
            sb.AppendLine("{");
            using (sb.Indent())
            {
                sb.AppendLine("// Act");
                sb.AppendLine($"var response = await _appService.GetAll(new GetAll{EntityName}Dto() {{ PageSize = 5, Order = \"id\" }});");
                sb.AppendLine();
                sb.AppendLine("// Assert");
                sb.AppendLine("Assert.False(LocalNotification.HasNotification(), string.Join(Environment.NewLine, LocalNotification.GetAll()));");
                sb.AppendLine();
                sb.AppendLine($"UsingDbContext<{Configuration.Orm.ContextClassName}>(context =>");
                sb.AppendLine("{");
                using (sb.Indent())
                {
                    sb.AppendLine($"var firstId = context.{PluralizedEntityName}.Take(5).OrderBy(o => o.Id).First().Id;");
                    sb.AppendLine();
                    sb.AppendLine("Assert.Equal(5, response.Items.Count);");
                    sb.AppendLine("Assert.Equal(response.Items[0].Id, firstId);");
                }
                sb.AppendLine("});");
            }
            sb.AppendLine("}");
            sb.AppendLine();
        }

        private void GeneratedGetAllEntitySortedDesc(IndentedStringBuilder sb)
        {
            sb.AppendLine($"[Fact(DisplayName = \"ApplicationService - Get all {EntityName} sorted desc\")]");
            sb.AppendLine($"public async Task GetAll{EntityName}SortedDesc()");
            sb.AppendLine("{");
            using (sb.Indent())
            {
                sb.AppendLine("// Act");
                sb.AppendLine($"var response = await _appService.GetAll(new GetAll{EntityName}Dto() {{ PageSize = 5, Order = \"-id\" }});");
                sb.AppendLine();
                sb.AppendLine("// Assert");
                sb.AppendLine("Assert.False(LocalNotification.HasNotification(), string.Join(Environment.NewLine, LocalNotification.GetAll()));");
                sb.AppendLine();
                sb.AppendLine($"UsingDbContext<{Configuration.Orm.ContextClassName}>(context =>");
                sb.AppendLine("{");
                using (sb.Indent())
                {
                    sb.AppendLine($"var lastId = context.{PluralizedEntityName}.Take(5).OrderBy(o => o.Id).Last().Id;");
                    sb.AppendLine();
                    sb.AppendLine("Assert.Equal(5, response.Items.Count);");
                    sb.AppendLine("Assert.Equal(response.Items[0].Id, lastId);");
                }
                sb.AppendLine("});");
            }
            sb.AppendLine("}");
            sb.AppendLine();
        }

        private void GeneratedGetEntityWithSucess(IndentedStringBuilder sb, IEntityType entityType)
        {
            var propertyNames = ReturnPropertiesNamesFromEntity(entityType);

            sb.AppendLine($"[Fact(DisplayName = \"ApplicationService - Get {EntityName} with sucess\")]");
            sb.AppendLine($"public async Task Get{EntityName}WithSucess()");
            sb.AppendLine("{");
            using (sb.Indent())
            {
                sb.AppendLine("// Arrange");
                sb.AppendLine($"var entity = default({EntityName});");
                sb.AppendLine();
                sb.AppendLine($"UsingDbContext<{Configuration.Orm.ContextClassName}>(context =>");
                sb.AppendLine("{");
                using (sb.Indent())
                {
                    sb.AppendLine($"entity = context.{PluralizedEntityName}.Take(5).OrderBy(o => o.Id).First();");
                }
                sb.AppendLine("});");

                sb.AppendLine();
                sb.AppendLine("// Act");
                sb.AppendLine($"var response = await _appService.Get(new RequestDto<{EntityKey}>(entity.Id));");
                sb.AppendLine();

                sb.AppendLine("// Assert");
                sb.AppendLine("Assert.False(LocalNotification.HasNotification(), string.Join(Environment.NewLine, LocalNotification.GetAll()));");
                sb.AppendLine();

                foreach (var propertyName in propertyNames)
                {
                    sb.AppendLine($"Assert.Equal(response.{propertyName}, entity.{propertyName});");
                }
            }
            sb.AppendLine("}");
            sb.AppendLine();
        }

        private void GeneratedGetEntityFieldsWithSucess(IndentedStringBuilder sb, IEntityType entityType)
        {
            var propertyNames = ReturnPropertiesNamesFromEntity(entityType);

            sb.AppendLine($"[Fact(DisplayName = \"ApplicationService - Get {EntityName} fields with sucess\")]");
            sb.AppendLine($"public async Task Get{EntityName}FieldsWithSucess()");
            sb.AppendLine("{");
            using (sb.Indent())
            {
                sb.AppendLine("// Arrange");
                sb.AppendLine($"var entity = default({EntityName});");
                sb.AppendLine();
                sb.AppendLine($"UsingDbContext<{Configuration.Orm.ContextClassName}>(context =>");
                sb.AppendLine("{");
                using (sb.Indent())
                {
                    sb.AppendLine($"entity = context.{PluralizedEntityName}.Take(5).OrderBy(o => o.Id).First();");
                }
                sb.AppendLine("});");

                var propertyNamesFields = string.Join(",", propertyNames.Select(s => s.ToCamelCase()));

                sb.AppendLine();
                sb.AppendLine("// Act");
                sb.AppendLine($"var response = await _appService.Get(new RequestDto<{EntityKey}>(entity.Id, \"{propertyNamesFields}\"));");
                sb.AppendLine();
                sb.AppendLine("// Assert");
                sb.AppendLine("Assert.False(LocalNotification.HasNotification(), string.Join(Environment.NewLine, LocalNotification.GetAll()));");
                sb.AppendLine();

                foreach (var propertyName in propertyNames)
                {
                    sb.AppendLine($"Assert.Equal(response.{propertyName}, entity.{propertyName});");
                }
            }
            sb.AppendLine("}");
            sb.AppendLine();
        }

        private void GeneratedGetEntityWithInvalidParameterAndReturnNotification(IndentedStringBuilder sb)
        {
            sb.AppendLine($"[Fact(DisplayName = \"ApplicationService - Get {EntityName} with invalid parameter and return notification\")]");
            sb.AppendLine($"public async Task Get{EntityName}WithInvalidParameterAndReturnNotification()");
            sb.AppendLine("{");
            using (sb.Indent())
            {
                sb.AppendLine("// Act");
                sb.AppendLine($"var response = await _appService.Get(new RequestDto<{EntityKey}>(default({EntityKey})));");
                sb.AppendLine();
                sb.AppendLine("// Assert");
                sb.AppendLine("var notifications = LocalNotification.GetAll();");
                sb.AppendLine();
                sb.AppendLine("Assert.Contains(notifications, a => a.DetailedMessage == TnfAppApplicationErrors.AppApplicationOnInvalidIdError.ToString());");
            }
            sb.AppendLine("}");
            sb.AppendLine();
        }

        private void GeneratedGetEntityWhenNotExistsAndReturnNotification(IndentedStringBuilder sb)
        {
            sb.AppendLine($"[Fact(DisplayName = \"ApplicationService - Get {EntityName} when not exists and return notification\")]");
            sb.AppendLine($"public async Task Get{EntityName}WhenNotExistsAndReturnNotification()");
            sb.AppendLine("{");
            using (sb.Indent())
            {
                sb.AppendLine("// Arrange");
                if (EntityKey == "Guid")
                {
                    sb.AppendLine("var notFoundId = Guid.NewGuid();");
                }
                else
                {
                    sb.AppendLine($"var notFoundId = default({EntityKey});");
                    sb.AppendLine();
                    sb.AppendLine($"UsingDbContext<{Configuration.Orm.ContextClassName}>(context =>");
                    sb.AppendLine("{");
                    using (sb.Indent())
                        sb.AppendLine($"notFoundId = context.{PluralizedEntityName}.Max(m => m.Id) + 1;");
                    sb.AppendLine("});");
                }

                sb.AppendLine();

                sb.AppendLine("// Act");
                sb.AppendLine($"var response = await _appService.Get(new RequestDto<{EntityKey}>(notFoundId));");
                sb.AppendLine();
                sb.AppendLine("// Assert");
                sb.AppendLine("var notifications = LocalNotification.GetAll();");
                sb.AppendLine();
                sb.AppendLine("Assert.Contains(notifications, a => a.DetailedMessage == TnfAppDomainErrors.AppDomainOnGetCouldNotFindError.ToString());");
            }
            sb.AppendLine("}");
            sb.AppendLine();
        }

        private void GeneratedDeleteEntityWithSucess(IndentedStringBuilder sb)
        {
            sb.AppendLine($"[Fact(DisplayName = \"ApplicationService - Delete {EntityName} with sucess\")]");
            sb.AppendLine($"public async Task Delete{EntityName}WithSucess()");
            sb.AppendLine("{");
            using (sb.Indent())
            {
                sb.AppendLine($"await UsingDbContextAsync<{Configuration.Orm.ContextClassName}>(async context =>");
                sb.AppendLine("{");
                using (sb.Indent())
                {
                    sb.AppendLine($"var id = context.{PluralizedEntityName}.First().Id;");
                    sb.AppendLine();
                    sb.AppendLine("// Act");
                    sb.AppendLine("await _appService.Delete(id);");
                    sb.AppendLine();
                    sb.AppendLine("// Assert");
                    sb.AppendLine("Assert.False(LocalNotification.HasNotification(), string.Join(Environment.NewLine, LocalNotification.GetAll()));");
                    sb.AppendLine();
                    sb.AppendLine("// Act");
                    sb.AppendLine($"await _appService.Get(new RequestDto<{EntityKey}>(id));");
                    sb.AppendLine();
                    sb.AppendLine("// Assert");
                    sb.AppendLine("var notifications = LocalNotification.GetAll();");
                    sb.AppendLine();
                    sb.AppendLine("Assert.Contains(notifications, a => a.DetailedMessage == TnfAppDomainErrors.AppDomainOnGetCouldNotFindError.ToString());");
                }
                sb.AppendLine("});");
            }
            sb.AppendLine("}");
            sb.AppendLine();
        }

        private void GeneratedCreateEntityWithSucess(IndentedStringBuilder sb, IEntityType entityType)
        {
            sb.AppendLine($"[Fact(DisplayName = \"ApplicationService - Create {EntityName} with sucess\")]");
            sb.AppendLine($"public async Task Create{EntityName}WithSucess()");
            sb.AppendLine("{");
            using (sb.Indent())
            {
                sb.AppendLine("// Arrange");

                WriteDto(sb, forUpdate: false, invalidData: false, entityType: entityType);

                sb.AppendLine("// Act");
                sb.AppendLine($"var response = await _appService.Create(dto);");
                sb.AppendLine();

                sb.AppendLine("// Assert");
                sb.AppendLine("Assert.False(LocalNotification.HasNotification(), string.Join(Environment.NewLine, LocalNotification.GetAll()));");
            }
            sb.AppendLine("}");
            sb.AppendLine();
        }

        private void GeneratedCreateNullEntityAndReturnNotification(IndentedStringBuilder sb)
        {
            sb.AppendLine($"[Fact(DisplayName = \"ApplicationService - Create null {EntityName} and return bad request\")]");
            sb.AppendLine($"public async Task CreateNull{EntityName}AndReturnNotifications()");
            sb.AppendLine("{");
            using (sb.Indent())
            {
                sb.AppendLine("// Act");
                sb.AppendLine($"var response = await _appService.Create(default({EntityName}Dto));");
                sb.AppendLine();

                sb.AppendLine("// Assert");
                sb.AppendLine("var notifications = LocalNotification.GetAll();");
                sb.AppendLine();
                sb.AppendLine("Assert.Contains(notifications, a => a.DetailedMessage == TnfAppApplicationErrors.AppApplicationOnInvalidDtoError.ToString());");
            }
            sb.AppendLine("}");
            sb.AppendLine();
        }

        private void GeneratedCreateEmptyEntityAndReturnNotification(IndentedStringBuilder sb, IEntityType entityType)
        {
            var enumerationsErrors = Configuration.GetEntityErrorsEnumeration(RelationalTypeMapper, entityType, CSharpUtilities);
            if (enumerationsErrors.Count <= 0)
                return;

            sb.AppendLine($"[Fact(DisplayName = \"ApplicationService - Create empty {EntityName} and return bad request\")]");
            sb.AppendLine($"public async Task CreateEmpty{EntityName}AndReturnNotifications()");
            sb.AppendLine("{");
            using (sb.Indent())
            {
                sb.AppendLine("// Arrange");
                sb.AppendLine($"var dto = new {EntityName}Dto();");
                sb.AppendLine();
                sb.AppendLine("// Act");
                sb.AppendLine($"var response = await _appService.Create(dto);");
                sb.AppendLine();

                sb.AppendLine("// Assert");
                sb.AppendLine("var notifications = LocalNotification.GetAll();");
                sb.AppendLine();

                foreach (var error in enumerationsErrors.Where(w => w.SpecificationType == SpecificationTypes.MustHave))
                {
                    sb.AppendLine($"Assert.Contains(notifications, a => a.Message == {EntityName}.EntityError.{error.EnumerationName}.ToString());");
                }
            }
            sb.AppendLine("}");
            sb.AppendLine();
        }

        private void GeneratedUpdateEntityWithSucess(IndentedStringBuilder sb, IEntityType entityType)
        {
            sb.AppendLine($"[Fact(DisplayName = \"ApplicationService - Update {EntityName} with sucess\")]");
            sb.AppendLine($"public async Task Update{EntityName}WithSucess()");
            sb.AppendLine("{");
            using (sb.Indent())
            {
                sb.AppendLine("// Arrange");

                WriteDto(sb, forUpdate: true, invalidData: false, entityType: entityType);

                sb.AppendLine();
                sb.AppendLine("// Act");
                sb.AppendLine("dto = await _appService.Update(dto.Id, dto);");

                sb.AppendLine();
                sb.AppendLine("// Assert");
                sb.AppendLine("Assert.False(LocalNotification.HasNotification(), string.Join(Environment.NewLine, LocalNotification.GetAll()));");
                sb.AppendLine();

                sb.AppendLine($"var response = await _appService.Get(new RequestDto<{EntityKey}>(dto.Id));");
                sb.AppendLine();
                foreach (var propertyName in ReturnPropertiesNamesFromEntity(entityType))
                {
                    sb.AppendLine($"Assert.Equal(response.{propertyName}, dto.{propertyName});");
                }
            }
            sb.AppendLine("}");
            sb.AppendLine();
        }

        private void GeneratedUpdateEmptyEntityAndReturnNotification(IndentedStringBuilder sb, IEntityType entityType)
        {
            var enumerationsErrors = Configuration.GetEntityErrorsEnumeration(RelationalTypeMapper, entityType, CSharpUtilities).Where(w => w.SpecificationType != SpecificationTypes.MustHave);
            if (enumerationsErrors.Count() <= 0)
                return;

            sb.AppendLine($"[Fact(DisplayName = \"ApplicationService - Update empty {EntityName} and return notification\")]");
            sb.AppendLine($"public async Task UpdateEmpty{EntityName}AndReturnNotification()");
            sb.AppendLine("{");
            using (sb.Indent())
            {
                sb.AppendLine("// Arrange");

                WriteDto(sb, forUpdate: true, invalidData: true, entityType: entityType);

                sb.AppendLine();
                sb.AppendLine("// Act");
                sb.AppendLine($"await _appService.Update(dto.Id, dto);");

                sb.AppendLine();
                sb.AppendLine("// Assert");
                sb.AppendLine("var notifications = LocalNotification.GetAll();");
                sb.AppendLine();

                foreach (var error in enumerationsErrors)
                {
                    sb.AppendLine($"Assert.Contains(notifications, a => a.Message == {EntityName}.EntityError.{error.EnumerationName}.ToString());");
                }
            }
            sb.AppendLine("}");
            sb.AppendLine();
        }

        private void GeneratedUpdateEntityWhenNotExistsAndReturnNotification(IndentedStringBuilder sb, IEntityType entityType)
        {
            sb.AppendLine($"[Fact(DisplayName = \"ApplicationService - Update {EntityName} when not exists and return not found\")]");
            sb.AppendLine($"public async Task Update{EntityName}WhenNotExistsAndReturnNotification()");
            sb.AppendLine("{");
            using (sb.Indent())
            {
                sb.AppendLine("// Arrange");

                WriteDto(sb, forUpdate: false, invalidData: false, entityType: entityType);

                sb.AppendLine("// Act");
                sb.AppendLine($"await _appService.Update(dto.Id, dto);");

                sb.AppendLine();
                sb.AppendLine("// Assert");
                sb.AppendLine("var notifications = LocalNotification.GetAll();");
                sb.AppendLine();
                sb.AppendLine("Assert.Contains(notifications, a => a.Message == TnfAppDomainErrors.AppDomainOnUpdateCouldNotFindError.ToString());");
            }
            sb.AppendLine("}");
        }

        public string WriteCodeApplicationService(IEntityType entityType)
        {
            var sb = new IndentedStringBuilder();

            sb.AppendLine($"using {Configuration.Entity.Namespace};");
            sb.AppendLine("using System;");
            sb.AppendLine("using System.Collections.Generic;");
            sb.AppendLine();
            sb.AppendLine($"namespace {Configuration.Test.ApplicationService.Namespace}");
            sb.AppendLine("{");
            using (sb.Indent())
            {
                sb.AppendLine($"public partial class {ApplicationServiceName}Tests");
                sb.AppendLine("{");
                using (sb.Indent())
                {
                    sb.AppendLine($"public IEnumerable<{EntityName}> GetCollectionForInitializeInMemoryContext()");
                    sb.AppendLine("{");
                    using (sb.Indent())
                    {
                        sb.AppendLine($"var mock = new List<{EntityName}>();");
                        sb.AppendLine();

                        for (int i = 0; i < Configuration.Test.NumberOfEntitiesGenerated; i++)
                            WriteEntityForInMemorySetup(sb, i, entityType);

                        sb.AppendLine("return mock;");
                    }
                    sb.AppendLine("}");
                }
                sb.AppendLine("}");
            }
            sb.AppendLine("}");

            return sb.ToString();
        }

        private List<string> ReturnPropertiesNamesFromEntity(IEntityType entityType)
        {
            var properties = Configuration.GetOrderedProperties(entityType, CSharpUtilities)
                .Where(w => !w.IsNullable).OrderBy(p => p.Scaffolding().ColumnOrdinal);

            var propertyNames = new List<string>();
            foreach (var property in properties)
            {
                var propertyName = Configuration.GetPropertyName(entityType, property);
                propertyNames.Add(propertyName);
            }

            return propertyNames;
        }

        private void WriteDto(IndentedStringBuilder sb, bool forUpdate, bool invalidData, IEntityType entityType)
        {
            sb.AppendLine($"var dto = new {DtoName}()");
            sb.AppendLine("{");

            using (sb.Indent())
            {
                var properties = Configuration.GetOrderedProperties(entityType, CSharpUtilities).ToList();

                foreach (var property in properties)
                {
                    var propertyName = Configuration.GetPropertyName(entityType, property.Name, property.IsKey());

                    if (propertyName == "Id" && forUpdate)
                        continue;

                    var value = property.ClrType.GetLiteralTypeRandomValue(RelationalTypeMapper, property, invalid: invalidData);
                    sb.AppendLine($"{propertyName} = {value},");
                }
            }

            sb.AppendLine("};");
            sb.AppendLine();

            if (forUpdate)
            {
                sb.AppendLine($"UsingDbContext<{Configuration.Orm.ContextClassName}>(context =>");
                sb.AppendLine("{");
                using (sb.Indent())
                {
                    sb.AppendLine($"dto.Id = context.{PluralizedEntityName}.First().Id;");
                }
                sb.AppendLine("});");
            }
        }

        private void WriteEntityForInMemorySetup(IndentedStringBuilder sb, int index, IEntityType entityType)
        {
            index++;

            sb.AppendLine($"var entity{index} = new {EntityName}()");
            sb.AppendLine("{");

            using (sb.Indent())
            {
                var properties = entityType.GetProperties().OrderBy(p => p.Scaffolding().ColumnOrdinal);

                foreach (var property in properties)
                {
                    var propertyName = Configuration.GetPropertyName(entityType, property);
                    if (propertyName == "Id")
                    {
                        if (property.ClrType == typeof(Guid))
                            sb.AppendLine($"{propertyName} = Guid.NewGuid(),");
                        else
                            sb.AppendLine($"{propertyName} = {index},");

                        continue;
                    }

                    var value = property.ClrType.GetLiteralTypeRandomValue(RelationalTypeMapper, property, invalid: false);
                    sb.AppendLine($"{propertyName} = {value},");
                }
            }

            sb.AppendLine("};");
            sb.AppendLine($"mock.Add(entity{index});");
            sb.AppendLine();
        }
    }
}