using Crudzilla.Configuration;
using Crudzilla.Localization;
using JetBrains.Annotations;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Scaffolding.Internal;
using Microsoft.EntityFrameworkCore.Storage;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Crudzilla.Writters
{
    /// <summary>
    ///     Based on code: Microsoft.EntityFrameworkCore.Scaffolding.Internal.CSharpScaffoldingGenerator
    ///     Available in: https://github.com/aspnet/EntityFrameworkCore
    /// </summary>
    internal class CustomCSharpScaffoldingGenerator : CSharpScaffoldingGenerator
    {
        private readonly CrudzillaConfig _configuration;
        private readonly AutoMapperProfileWriter _autoMapperWriter;
        private readonly ConstantsWriter _constantsWriter;
        private readonly BuilderWriter _builderWriter;
        private readonly ICSharpUtilities _cSharpUtilities;
        private readonly DataTransferWriter _dataTransferWriter;
        private readonly AdapterWriter _adapterWriter;
        private readonly ApplicationServiceWriter _applicationServiceWriter;
        private readonly ApplicationTestWriter _applicationTestWriter;
        private readonly IRelationalTypeMapper _relationalTypeMapper;
        private readonly WebApiWriter _webApiWriter;
        private readonly WebApiTestWriter _webApiTestWriter;
        private readonly DomainTestWriter _domainTestWriter;
        private bool _generateNavigationProperties;

        public CustomCSharpScaffoldingGenerator(
            [NotNull] IFileService fileService,
            [NotNull] ICSharpDbContextGenerator cSharpDbContextGenerator,
            [NotNull] ICSharpEntityTypeGenerator cSharpEntityTypeGenerator,
            [NotNull] ICSharpUtilities cSharpUtilities,
            [NotNull] ConstantsWriter constantsWriter,
            [NotNull] AutoMapperProfileWriter autoMapperWriter,
            [NotNull] BuilderWriter builderWriter,
            [NotNull] DataTransferWriter dataTransferWriter,
            [NotNull] AdapterWriter adapterWriter,
            [NotNull] ApplicationServiceWriter applicationServiceWriter,
            [NotNull] ApplicationTestWriter applicationTestWriter,
            [NotNull] WebApiWriter webApiWriter,
            [NotNull] WebApiTestWriter webApiTestWriter,
            [NotNull] DomainTestWriter domainTestWriter,
            [NotNull] IRelationalTypeMapper relationalTypeMapper,
            [NotNull] CrudzillaConfig scaffoldConfiguration,
            bool generateNavigationProperties = true)
            : base(fileService, cSharpDbContextGenerator, cSharpEntityTypeGenerator)
        {
            _configuration = Check.NotNull(scaffoldConfiguration, nameof(scaffoldConfiguration));
            _autoMapperWriter = Check.NotNull(autoMapperWriter, nameof(autoMapperWriter));
            _constantsWriter = Check.NotNull(constantsWriter, nameof(constantsWriter));
            _builderWriter = Check.NotNull(builderWriter, nameof(builderWriter));
            _cSharpUtilities = Check.NotNull(cSharpUtilities, nameof(cSharpUtilities));
            _dataTransferWriter = Check.NotNull(dataTransferWriter, nameof(dataTransferWriter));
            _adapterWriter = Check.NotNull(adapterWriter, nameof(adapterWriter));
            _applicationServiceWriter = Check.NotNull(applicationServiceWriter, nameof(applicationServiceWriter));
            _applicationTestWriter = Check.NotNull(applicationTestWriter, nameof(applicationTestWriter));
            _relationalTypeMapper = Check.NotNull(relationalTypeMapper, nameof(relationalTypeMapper));
            _webApiWriter = Check.NotNull(webApiWriter, nameof(webApiWriter));
            _webApiTestWriter = Check.NotNull(webApiTestWriter, nameof(webApiTestWriter));
            _domainTestWriter = Check.NotNull(domainTestWriter, nameof(domainTestWriter));
            _generateNavigationProperties = generateNavigationProperties;
        }

        public override ReverseEngineerFiles WriteCode(
            IModel model,
            string outputPath,
            string @namespace,
            string contextName,
            string connectionString,
            bool useDataAnnotations)
        {
            Check.NotNull(model, nameof(model));
            Check.NotEmpty(outputPath, nameof(outputPath));
            Check.NotEmpty(@namespace, nameof(@namespace));
            Check.NotEmpty(contextName, nameof(contextName));
            Check.NotEmpty(connectionString, nameof(connectionString));

            ClearScaffoldFiles();

            var resultingFiles = new ReverseEngineerFiles();

            var entityTypes = model.GetEntityTypes();
            foreach (var entityType in entityTypes)
            {
                //// -------- DOMAIN -----------
                //// Entity
                GeneratedEntityTypeFile(@namespace, useDataAnnotations, resultingFiles, entityType);

                //// Builder
                GeneratedBuilderTypeFile(entityType);

                //// Localization Keys
                GenerateLocalizationKeysTypeFile(entityType);



                //// -------- APPLICATION ------
                //// Data Transfer
                GenerateDataTransferTypeFile(entityType, _generateNavigationProperties);

                // Adapter
                GenerateAdapterTypeFile(entityType);

                //// Application Service 
                GenerateApplicationServiceTypeFile(entityType);



                //// --------- API -------------
                //// Controller
                //GenerateControllerTypeFile(entityType);



                //// ------- TESTS ----------

                //// Controller Tests
                GenerateControllerTestsTypeFile(entityType);

                //// Builder and Domain Tests
                GenerateDomainTestsTypeFile(entityType);

                //// Application Tests
                GenerateApplicationTestsTypeFile(entityType);
            }

            //// Create EntityNames constant
            GenerateEntityNamesConstantTypeFile(entityTypes);

            //// Create RouteConstans
            GenerateRouteConstantTypeFile(entityTypes);

            //// Create AutoMapper profile
            GenerateAutoMapperProfilesTypeFile(entityTypes);

            // Create DbContext
            var dbContextFileName = contextName + FileExtension;

            if (_configuration.Orm.OverrideDbContext || !File.Exists(Path.Combine(_configuration.Orm.OutputPath, dbContextFileName)))
            {
                var generatedCode = CSharpDbContextGenerator.WriteCode(model, @namespace, contextName, connectionString, useDataAnnotations);

                resultingFiles.ContextFile = FileService.OutputFile(
                    _configuration.Orm.OutputPath,
                    dbContextFileName,
                    generatedCode);
            }

            return resultingFiles;
        }

        private void GeneratedEntityTypeFile(string @namespace, bool useDataAnnotations, ReverseEngineerFiles resultingFiles, IEntityType entityType)
        {
            var csharpEntityTypeGenerator = CSharpEntityTypeGenerator as CustomCSharpEntityTypeGenerator;

            var generatedCode = csharpEntityTypeGenerator.WriteCode(entityType, @namespace, useDataAnnotations);

            // Scaffold entity
            var entityName = _configuration.GetEntityNameOrDefault(entityType);

            var entityNameFileName = $"{entityName}{FileExtension}";

            var entityTypeFileFullPath = FileService.OutputFile(
                _configuration.Entity.GetScaffoldOutputPath(),
                entityNameFileName,
                generatedCode);

            generatedCode = csharpEntityTypeGenerator.WriteCode();

            if (!File.Exists(Path.Combine(_configuration.Entity.OutputPath, entityName, entityNameFileName)))
            {
                FileService.OutputFile(
                    Path.Combine(_configuration.Entity.OutputPath, entityName),
                    entityNameFileName,
                    generatedCode);
            }

            resultingFiles.EntityTypeFiles.Add(entityTypeFileFullPath);
        }

        private void GeneratedBuilderTypeFile(IEntityType entityType)
        {
            var entityName = _configuration.GetEntityNameOrDefault(entityType);
            var generatedCode = _builderWriter.WriteCodeScaffold(entityType);

            // Scaffold class
            FileService.OutputFile(
                _configuration.Builder.GetScaffoldOutputPath(),
                $"Scaffold{_configuration.GetBuilderName(entityType)}.cs",
                generatedCode);

            generatedCode = _builderWriter.WriteCodeScaffoldInterface(entityType);

            // Scaffold interface
            FileService.OutputFile(
                _configuration.Builder.GetScaffoldOutputPath(),
                $"I{_configuration.GetBuilderName(entityType)}.cs",
                generatedCode);

            // Class
            var externalBuilderName = $"{_configuration.GetBuilderName(entityType)}.cs";
            if (!File.Exists(Path.Combine(_configuration.Builder.OutputPath, entityName, externalBuilderName)))
            {
                FileService.OutputFile(
                   Path.Combine(_configuration.Builder.OutputPath, entityName),
                   externalBuilderName,
                   _builderWriter.WriteCode(entityType));
            }

            // Interface
            externalBuilderName = $"I{_configuration.GetBuilderName(entityType)}.cs";
            if (!File.Exists(Path.Combine(_configuration.Builder.OutputPath, entityName, externalBuilderName)))
            {
                FileService.OutputFile(
                   Path.Combine(_configuration.Builder.OutputPath, entityName),
                   externalBuilderName,
                   _builderWriter.WriteCodeInterface(entityType));
            }
        }

        private void GenerateDataTransferTypeFile(IEntityType entityType, bool generateNavigationProperties)
        {
            // Invalid entity for dto
            if (!_configuration.IsValidEntity(entityType, _cSharpUtilities))
                return;

            var entityName = _configuration.GetEntityNameOrDefault(entityType);
            var dtoName = _configuration.GetDtoName(entityType);
            var generatedCode = _dataTransferWriter.WriteCodeScaffold(entityType, generateNavigationProperties);

            FileService.OutputFile(
                _configuration.DataTransfer.GetScaffoldOutputPath(),
                $"{dtoName}.cs",
                generatedCode);

            var externalDtoName = $"{dtoName}.cs";
            if (!File.Exists(Path.Combine(_configuration.DataTransfer.OutputPath, entityName, externalDtoName)))
            {
                FileService.OutputFile(
                   Path.Combine(_configuration.DataTransfer.OutputPath, entityName),
                   externalDtoName,
                   _dataTransferWriter.WriteCode());
            }

            // Write request all dto
            generatedCode = _dataTransferWriter.WriteCodeRequestAllScaffold();

            FileService.OutputFile(
                _configuration.DataTransfer.GetScaffoldOutputPath(),
                $"GetAll{dtoName}{_configuration.DefaultFileExtension}",
                generatedCode);

            externalDtoName = $"GetAll{dtoName}{_configuration.DefaultFileExtension}";
            if (!File.Exists(Path.Combine(_configuration.DataTransfer.OutputPath, entityName, externalDtoName)))
            {
                FileService.OutputFile(
                   Path.Combine(_configuration.DataTransfer.OutputPath, entityName),
                   externalDtoName,
                   _dataTransferWriter.WriteCodeRequestAll());
            }
        }

        private void GenerateAdapterTypeFile(IEntityType entityType)
        {
            // Invalid entity for dto
            if (!_configuration.IsValidEntity(entityType, _cSharpUtilities))
                return;

            var entityName = _configuration.GetEntityNameOrDefault(entityType);
            var generatedCode = _adapterWriter.WriteCodeScaffold(entityType);
            var adapterName = _configuration.GetAdapterName(entityType);

            FileService.OutputFile(
                _configuration.Adapter.GetScaffoldOutputPath(),
                $"Scaffold{adapterName}{_configuration.DefaultFileExtension}",
                generatedCode);

            var externalAdapterName = adapterName + _configuration.DefaultFileExtension;
            if (!File.Exists(Path.Combine(_configuration.Adapter.GetOutputPath(entityName), externalAdapterName)))
            {
                FileService.OutputFile(
                   _configuration.Adapter.GetOutputPath(entityName),
                   externalAdapterName,
                   _adapterWriter.WriteCode());
            }

            generatedCode = _adapterWriter.WriteCodeInterfaceScaffold();

            FileService.OutputFile(
                _configuration.Adapter.GetScaffoldOutputPath(),
                $"IScaffold{adapterName}{_configuration.DefaultFileExtension}",
                generatedCode);

            externalAdapterName = $"I{adapterName}{_configuration.DefaultFileExtension}";
            if (!File.Exists(Path.Combine(_configuration.Adapter.GetOutputPath(entityName), externalAdapterName)))
            {
                FileService.OutputFile(
                   _configuration.Adapter.GetOutputPath(entityName),
                   externalAdapterName,
                   _adapterWriter.WriteCodeInterface());
            }
        }

        private void GenerateApplicationServiceTypeFile(IEntityType entityType)
        {
            // Invalid entity for dto
            if (!_configuration.IsValidEntity(entityType, _cSharpUtilities))
                return;

            var entityName = _configuration.GetEntityNameOrDefault(entityType);
            var generatedCode = _applicationServiceWriter.WriteCodeScaffold(entityType);
            var applicationServiceName = _configuration.GetApplicationServiceName(entityType);

            FileService.OutputFile(
                _configuration.ApplicationService.GetScaffoldServicesOutputPath(),
                $"Scaffold{applicationServiceName}{_configuration.DefaultFileExtension}",
                generatedCode);

            var externalApplicationServiceName = applicationServiceName + _configuration.DefaultFileExtension;
            if (!File.Exists(Path.Combine(_configuration.ApplicationService.GetServicesOutputPath(entityName), externalApplicationServiceName)))
            {
                FileService.OutputFile(
                   _configuration.ApplicationService.GetServicesOutputPath(entityName),
                   externalApplicationServiceName,
                   _applicationServiceWriter.WriteCode());
            }

            generatedCode = _applicationServiceWriter.WriteCodeInterfaceScaffold();

            FileService.OutputFile(
                _configuration.ApplicationService.GetScaffoldInterfacesOutputPath(),
                $"IScaffold{applicationServiceName}{_configuration.DefaultFileExtension}",
                generatedCode);

            externalApplicationServiceName = $"I{applicationServiceName}{_configuration.DefaultFileExtension}";
            if (!File.Exists(Path.Combine(_configuration.ApplicationService.GetInterfacesOutputPath(entityName), externalApplicationServiceName)))
            {
                FileService.OutputFile(
                   _configuration.ApplicationService.GetInterfacesOutputPath(entityName),
                   externalApplicationServiceName,
                   _applicationServiceWriter.WriteCodeInterface());
            }
        }

        private void GenerateLocalizationKeysTypeFile(IEntityType entityType)
        {
            var enumerations = _configuration.GetEntityErrorsEnumeration(_relationalTypeMapper, entityType, _cSharpUtilities);
            if (enumerations.Count > 0)
            {
                if (!File.Exists(_configuration.Localization.FilePath))
                    throw new FileNotFoundException($"Localization source file not exit: {_configuration.Localization.FilePath}");

                var schema = JsonConvert.DeserializeObject<LocalizationSchema>(File.ReadAllText(_configuration.Localization.FilePath));

                foreach (var entityError in enumerations.EliminateDuplicates().Where(w => !schema.Texts.Any(a => a.Key == w.EnumerationName)).ToArray())
                {
                    var localizationValue = string.Empty;
                    if (entityError.SpecificationType == SpecificationTypes.OutOfBound)
                        localizationValue = $"O campo {entityError.Property} excede o tamanho limite de {entityError.Size}";

                    if (entityError.SpecificationType == SpecificationTypes.MustHave)
                        localizationValue = $"Informe um valor para o campo {entityError.Property}";

                    if (entityError.SpecificationType == SpecificationTypes.Invalid)
                        localizationValue = $"Informe uma valor valido para o campo {entityError.Property}";

                    schema.Texts.Add(entityError.EnumerationName, localizationValue);
                }

                var contentLocalizationFile = JsonConvert.SerializeObject(schema, Formatting.Indented);
                File.WriteAllText(_configuration.Localization.FilePath, contentLocalizationFile);
            }
        }

        private void GenerateControllerTypeFile(IEntityType entityType)
        {
            // Invalid entity for dto
            if (!_configuration.IsValidEntity(entityType, _cSharpUtilities))
                return;

            var generatedCode = _webApiWriter.WriteCodeScaffold(entityType);
            var controllerName = _configuration.GetControllerName(entityType);

            FileService.OutputFile(
                _configuration.WebApi.GetControllersScaffoldOutputPath(),
                $"Scaffold{controllerName}{_configuration.DefaultFileExtension}",
                generatedCode);

            var externalControllerName = controllerName + _configuration.DefaultFileExtension;

            if (!File.Exists(Path.Combine(_configuration.WebApi.GetControllersOutputPath(), externalControllerName)))
            {
                FileService.OutputFile(
                   _configuration.WebApi.GetControllersOutputPath(),
                   externalControllerName,
                   _webApiWriter.WriteCode());
            }
        }

        private void GenerateEntityNamesConstantTypeFile(IEnumerable<IEntityType> entities)
        {
            // Create EntityNames constant
            var generatedCode = _constantsWriter.WriteCodeEntityName(entities);
            var entityNamesFileName = $"EntityNames{FileExtension}";
            FileService.OutputFile(
                _configuration.Entity.GetScaffoldConstantFilePath(),
                entityNamesFileName,
                generatedCode);
        }

        private void GenerateRouteConstantTypeFile(IEnumerable<IEntityType> entities)
        {
            // Create RouteConstans
            var generatedCode = _constantsWriter.WriteCodeRoutesScaffold(entities);
            var routeConstansFileName = $"RouteConsts{FileExtension}";

            FileService.OutputFile(
                _configuration.WebApi.GetScaffoldRouteConstansOutputPath(),
                routeConstansFileName,
                generatedCode);
        }

        private void GenerateAutoMapperProfilesTypeFile(IEnumerable<IEntityType> entities)
        {
            var generatedCode = _autoMapperWriter.WriteCodeScaffold(entities);
            var profileFileName = $"DomainToDtoProfile{FileExtension}";

            FileService.OutputFile(
                _configuration.AutoMapper.GetScaffoldOutputPath(),
                $"Scaffold{profileFileName}",
                generatedCode);

            if (!File.Exists(Path.Combine(_configuration.AutoMapper.OutputPath, profileFileName)))
            {
                generatedCode = _autoMapperWriter.WriteCode(entities);
                FileService.OutputFile(
                    _configuration.AutoMapper.OutputPath,
                    profileFileName,
                    generatedCode);
            }
        }

        private void GenerateControllerTestsTypeFile(IEntityType entityType)
        {
            if (!_configuration.IsValidEntity(entityType, _cSharpUtilities))
                return;

            var controllerName = $"{_configuration.GetControllerName(entityType)}Tests{_configuration.DefaultFileExtension}";

            var generatedCode = _webApiTestWriter.WriteCodeScaffold(entityType);

            FileService.OutputFile(
                _configuration.Test.WebApi.GetScaffoldOutputPath(),
                controllerName,
                generatedCode);

            generatedCode = _webApiTestWriter.WriteCode(entityType);

            var existFile = File.Exists(Path.Combine(_configuration.Test.WebApi.GetOutputPath(), controllerName));
            if (!existFile)
            {
                FileService.OutputFile(
                _configuration.Test.WebApi.GetOutputPath(),
                controllerName,
                generatedCode);
            }
        }

        private void GenerateDomainTestsTypeFile(IEntityType entityType)
        {
            if (_configuration.Test.Domain.Disabled)
                return;

            // Builder test
            var builderName = _configuration.GetBuilderName(entityType);
            var fileName = $"{builderName}Tests{_configuration.DefaultFileExtension}";

            var generatedCode = _domainTestWriter.WriteCodeBuilderScaffold(entityType);

            FileService.OutputFile(
                _configuration.Test.Domain.GetBuildersScaffoldOutputPath(),
                fileName,
                generatedCode);

            generatedCode = _domainTestWriter.WriteCodeBuilder();

            var existFile = File.Exists(Path.Combine(_configuration.Test.Domain.GetBuildersOutputPath(), fileName));

            if (!existFile)
            {
                FileService.OutputFile(
                    _configuration.Test.Domain.GetBuildersOutputPath(),
                    fileName,
                    generatedCode);
            }

            if (!_configuration.IsValidEntity(entityType, _cSharpUtilities))
                return;

            // Service test
            var serviceName = _configuration.GetDomainServiceName(entityType);
            fileName = $"{serviceName}Tests{_configuration.DefaultFileExtension}";

            generatedCode = _domainTestWriter.WriteCodeServiceScaffold(entityType);

            FileService.OutputFile(
                _configuration.Test.Domain.GetServicesScaffoldOutputPath(),
                fileName,
                generatedCode);

            generatedCode = _domainTestWriter.WriteCodeService(entityType);

            existFile = File.Exists(Path.Combine(_configuration.Test.Domain.GetServicesOutputPath(), fileName));
            if (!existFile)
            {
                FileService.OutputFile(
                    _configuration.Test.Domain.GetServicesOutputPath(),
                    fileName,
                    generatedCode);
            }
        }

        private void GenerateApplicationTestsTypeFile(IEntityType entityType)
        {
            if (_configuration.Test.ApplicationService.Disabled)
                return;

            // Invalid entity for application test
            if (!_configuration.IsValidEntity(entityType, _cSharpUtilities))
                return;

            // Service test
            var applicationServiceName = _configuration.GetApplicationServiceName(entityType);
            var fileName = $"{applicationServiceName}Tests{_configuration.DefaultFileExtension}";

            var generatedCode = _applicationTestWriter.WriteCodeApplicationServiceScaffold(entityType);

            FileService.OutputFile(
                _configuration.Test.ApplicationService.GetScaffoldOutputPath(),
                fileName,
                generatedCode);

            generatedCode = _applicationTestWriter.WriteCodeApplicationService(entityType);

            var existFile = File.Exists(Path.Combine(_configuration.Test.ApplicationService.GetOutputPath(), fileName));

            if (!existFile)
            {
                FileService.OutputFile(
                    _configuration.Test.ApplicationService.GetOutputPath(),
                    fileName,
                    generatedCode);
            }
        }

        private void ClearScaffoldFiles()
        {
            var filesToExclude = new List<string>();
            // Data Transfer
            var dtos = GetFilesFromPath(_configuration.DataTransfer.GetScaffoldOutputPath());
            filesToExclude.AddRange(dtos);

            // Application Service
            var appServices = GetFilesFromPath(_configuration.ApplicationService.GetScaffoldServicesOutputPath());
            filesToExclude.AddRange(appServices);

            // Adapter
            var adapters = GetFilesFromPath(_configuration.Adapter.GetScaffoldOutputPath());
            filesToExclude.AddRange(adapters);

            // Application Service Interfaces
            var appServicesInterfaces = GetFilesFromPath(_configuration.ApplicationService.GetScaffoldInterfacesOutputPath());
            filesToExclude.AddRange(appServicesInterfaces);

            // Entities
            var entitites = GetFilesFromPath(_configuration.Entity.GetScaffoldOutputPath());
            filesToExclude.AddRange(entitites);

            // Builders
            var builders = GetFilesFromPath(_configuration.Builder.GetScaffoldOutputPath());
            filesToExclude.AddRange(builders);

            // Application Service Tests
            if (!_configuration.Test.ApplicationService.Disabled)
            {
                var appServicesTests = GetFilesFromPath(_configuration.Test.ApplicationService.GetScaffoldOutputPath());
                filesToExclude.AddRange(appServicesTests);
            }

            // Controllers
            var controllers = GetFilesFromPath(_configuration.WebApi.GetControllersScaffoldOutputPath());
            filesToExclude.AddRange(controllers);

            // Controllers Tests
            if (!_configuration.Test.WebApi.Disabled)
            {
                var controllersTests = GetFilesFromPath(_configuration.Test.WebApi.GetScaffoldOutputPath());
                filesToExclude.AddRange(controllersTests);
            }

            // Builder Tests
            if (!_configuration.Test.Domain.Disabled)
            {
                var builderTests = GetFilesFromPath(_configuration.Test.Domain.GetBuildersScaffoldOutputPath());
                filesToExclude.AddRange(builderTests);

                // Services Tests
                var serviceTests = GetFilesFromPath(_configuration.Test.Domain.GetServicesScaffoldOutputPath());
                filesToExclude.AddRange(serviceTests);
            }

            // Delete files
            Parallel.ForEach(filesToExclude, file => File.Delete(file));
        }

        private IEnumerable<string> GetFilesFromPath(string directory)
        {
            if (!Directory.Exists(directory))
                return new List<string>();

            var files = Directory.EnumerateFiles(directory, "*.cs");
            return files;
        }
    }
}
