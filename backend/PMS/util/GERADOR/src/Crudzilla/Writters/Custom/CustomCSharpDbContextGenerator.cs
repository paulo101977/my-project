using Crudzilla.Configuration;
using JetBrains.Annotations;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Internal;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore.Metadata.Conventions.Internal;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Microsoft.EntityFrameworkCore.Scaffolding;
using Microsoft.EntityFrameworkCore.Scaffolding.Internal;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Crudzilla.Writters
{
    /// <summary>
    ///     Based on code: Microsoft.EntityFrameworkCore.Scaffolding.Internal.DbContextWriter
    ///     Available in: https://github.com/aspnet/EntityFrameworkCore
    /// </summary>
    internal class CustomCSharpDbContextGenerator : CSharpDbContextGenerator
    {
        private const string EntityLambdaIdentifier = "builder";
        private const string Language = "CSharp";

        private readonly ICSharpUtilities _cSharpUtilities;
        private readonly IScaffoldingProviderCodeGenerator _providerCodeGenerator;
        private readonly IAnnotationCodeGenerator _annotationCodeGenerator;
        private readonly CrudzillaConfig _configuration;
        private readonly OrmMapWriter _ormMapWriter;

        private IFileService _fileService { get; }
        private IndentedStringBuilder _sb;
        private bool _entityTypeBuilderInitialized;

        public CustomCSharpDbContextGenerator(
            [NotNull] IScaffoldingProviderCodeGenerator providerCodeGenerator,
            [NotNull] IAnnotationCodeGenerator annotationCodeGenerator,
            [NotNull] ICSharpUtilities cSharpUtilities,
            [NotNull] IFileService fileService,
            [NotNull] CrudzillaConfig configuration,
            [NotNull] OrmMapWriter ormMapWriter)
            : base(providerCodeGenerator, annotationCodeGenerator, cSharpUtilities)
        {
            Check.NotNull(providerCodeGenerator, nameof(providerCodeGenerator));
            Check.NotNull(annotationCodeGenerator, nameof(annotationCodeGenerator));
            Check.NotNull(cSharpUtilities, nameof(cSharpUtilities));
            Check.NotNull(fileService, nameof(fileService));
            Check.NotNull(configuration, nameof(configuration));
            Check.NotNull(ormMapWriter, nameof(ormMapWriter));

            _providerCodeGenerator = providerCodeGenerator;
            _annotationCodeGenerator = annotationCodeGenerator;
            _cSharpUtilities = cSharpUtilities;
            _fileService = fileService;
            _configuration = configuration;
            _ormMapWriter = ormMapWriter;
        }

        public override string WriteCode(
            IModel model,
            string @namespace,
            string contextName,
            string connectionString,
            bool useDataAnnotations)
        {
            Check.NotNull(model, nameof(model));

            _sb = new IndentedStringBuilder();

            _sb.AppendLine("using Microsoft.EntityFrameworkCore;");
            _sb.AppendLine("using Tnf.EntityFrameworkCore;");
            _sb.AppendLine($"using {_configuration.Entity.Namespace};");
            _sb.AppendLine($"using {_configuration.Orm.GetMappersNamespace()};");
            _sb.AppendLine();
            _sb.AppendLine($"namespace {_configuration.Orm.Namespace}");
            _sb.AppendLine("{");

            using (_sb.Indent())
            {
                GenerateClass(model, contextName, connectionString, useDataAnnotations);
            }

            _sb.AppendLine("}");

            return _sb.ToString();
        }

        private void GenerateClass(
            IModel model,
            string contextName,
            string connectionString,
            bool useDataAnnotations)
        {
            _sb.AppendLine($"public partial class {contextName} : TnfDbContext");
            _sb.AppendLine("{");
            using (_sb.Indent())
            {
                _sb.AppendLine($"public {contextName}(DbContextOptions<{contextName}> options)");
                using (_sb.Indent())
                {
                    _sb.AppendLine(": base(options)");
                }
                _sb.AppendLine("{");
                _sb.AppendLine("}");
            }

            _sb.AppendLine();

            using (_sb.Indent())
            {
                GenerateDbSets(model);
                GenerateEntityTypeErrors(model);
                //GenerateOnConfiguring(connectionString); -- disabled
                GenerateOnModelCreating(model, useDataAnnotations);
            }

            _sb.AppendLine("}");
        }

        private void GenerateDbSets(IModel model)
        {
            foreach (var entityType in model.GetEntityTypes())
            {
                var entityName = _configuration.GetEntityNameOrDefault(entityType);

                var pluralizedEntityName = _configuration.GetPluralizedEntityNameOrDefault(entityType);

                _sb.AppendLine($"public virtual DbSet<{entityName}> {pluralizedEntityName} {{ get; set; }}");
            }

            if (model.GetEntityTypes().Any())
                _sb.AppendLine();
        }

        private void GenerateEntityTypeErrors(IModel model)
        {
            var annotations = Microsoft.EntityFrameworkCore.Metadata.Internal.ScaffoldingMetadataExtensions.Scaffolding(model);

            foreach (var entityTypeError in annotations.EntityTypeErrors)
            {
                _sb.AppendLine($"// {entityTypeError.Value} Please see the warning messages.");
            }

            if (annotations.EntityTypeErrors.Any())
            {
                _sb.AppendLine();
            }
        }

        private void GenerateOnModelCreating(
            IModel model,
            bool useDataAnnotations)
        {
            _sb.AppendLine("protected override void OnModelCreating(ModelBuilder modelBuilder)");
            _sb.Append("{");

            var annotations = model.GetAnnotations().ToList();
            RemoveAnnotation(ref annotations, ScaffoldingAnnotationNames.DatabaseName);
            RemoveAnnotation(ref annotations, ScaffoldingAnnotationNames.EntityTypeErrors);

            var annotationsToRemove = new List<IAnnotation>();
            annotationsToRemove.AddRange(annotations.Where(a => a.Name.StartsWith(RelationalAnnotationNames.SequencePrefix, StringComparison.Ordinal)));

            var lines = new List<string>();

            foreach (var annotation in annotations)
            {
                if (_annotationCodeGenerator.IsHandledByConvention(model, annotation))
                {
                    annotationsToRemove.Add(annotation);
                }
                else
                {
                    var line = _annotationCodeGenerator.GenerateFluentApi(model, annotation, Language);

                    if (line != null)
                    {
                        lines.Add(line);
                        annotationsToRemove.Add(annotation);
                    }
                }
            }

            lines.AddRange(GenerateAnnotations(annotations.Except(annotationsToRemove)));

            if (lines.Count > 0)
            {
                using (_sb.Indent())
                {
                    _sb.AppendLine();
                    _sb.Append("modelBuilder" + lines[0]);

                    using (_sb.Indent())
                    {
                        foreach (var line in lines.Skip(1))
                        {
                            _sb.AppendLine();
                            _sb.Append(line);
                        }
                    }

                    _sb.AppendLine(";");
                }
            }

            _sb.AppendLine();

            using (_sb.Indent())
            {
                foreach (var entityType in model.GetEntityTypes())
                {
                    var sb = new IndentedStringBuilder();
                    sb.IncrementIndent();
                    sb.IncrementIndent();

                    _entityTypeBuilderInitialized = false;

                    GenerateEntityType(entityType, useDataAnnotations, sb);

                    if (_entityTypeBuilderInitialized)
                    {
                        sb.AppendLine("});");
                    }

                    var generatedCode = _ormMapWriter.WriteCode(entityType, sb.ToString());

                    var mapperName = $"{_configuration.GetEntityNameOrDefault(entityType)}Mapper";
                    var mapperFileName = $"{mapperName}.cs";
                    var mapperFullName = Path.Combine(_configuration.Orm.GetMappersOutputPath(), mapperFileName);

                    if (!File.Exists(mapperFullName) || _configuration.Orm.OverrideMappers)
                    {
                        _fileService.OutputFile(
                            _configuration.Orm.GetMappersOutputPath(),
                            mapperFileName,
                            generatedCode);
                    }

                    _sb.AppendLine($"modelBuilder.ApplyConfiguration(new {mapperName}());");
                }

                foreach (var sequence in model.Relational().Sequences)
                {
                    GenerateSequence(sequence, _sb);
                }
            }

            _sb.AppendLine("}");
        }

        private void GenerateEntityType(IEntityType entityType, bool useDataAnnotations, IndentedStringBuilder sb)
        {
            GenerateKey(entityType.FindPrimaryKey(), useDataAnnotations, sb);

            var annotations = entityType.GetAnnotations().ToList();
            // Force write table name
            //RemoveAnnotation(ref annotations, RelationalAnnotationNames.TableName); - Force write table name
            RemoveAnnotation(ref annotations, RelationalAnnotationNames.Schema);
            RemoveAnnotation(ref annotations, ScaffoldingAnnotationNames.DbSetName);

            if (!useDataAnnotations)
            {
                GenerateTableName(entityType, sb);
            }

            var annotationsToRemove = new List<IAnnotation>();
            var lines = new List<string>();

            foreach (var annotation in annotations)
            {
                if (_annotationCodeGenerator.IsHandledByConvention(entityType, annotation))
                {
                    annotationsToRemove.Add(annotation);
                }
                else
                {
                    var line = _annotationCodeGenerator.GenerateFluentApi(entityType, annotation, Language);

                    if (line != null)
                    {
                        lines.Add(line);
                        annotationsToRemove.Add(annotation);
                    }
                }
            }

            lines.AddRange(GenerateAnnotations(annotations.Except(annotationsToRemove)));

            AppendMultiLineFluentApi(lines, sb);

            foreach (var index in entityType.GetIndexes())
            {
                GenerateIndex(index, sb);
            }

            foreach (var property in entityType.GetProperties())
            {
                GenerateProperty(property, useDataAnnotations, sb);
            }

            foreach (var foreignKey in entityType.GetForeignKeys())
            {
                GenerateRelationship(foreignKey, useDataAnnotations, sb);
            }
        }

        private void AppendMultiLineFluentApi(IList<string> lines, IndentedStringBuilder sb)
        {
            if (lines.Count <= 0)
            {
                return;
            }

            using (sb.Indent())
            {
                sb.AppendLine();

                sb.Append(EntityLambdaIdentifier + lines[0]);

                using (sb.Indent())
                {
                    foreach (var line in lines.Skip(1))
                    {
                        sb.AppendLine();
                        sb.Append(line);
                    }
                }

                sb.AppendLine(";");
            }
        }

        private void GenerateKey(IKey key, bool useDataAnnotations, IndentedStringBuilder sb)
        {
            if (key == null)
                return;

            var annotations = key.GetAnnotations().ToList();

            var explicitName = key.Relational().Name != ConstraintNamer.GetDefaultName(key);
            RemoveAnnotation(ref annotations, RelationalAnnotationNames.Name);

            if (key.Properties.Count == 1)
            {

                if (key is Key concreteKey
                    && key.Properties.SequenceEqual(new KeyDiscoveryConvention().DiscoverKeyProperties(concreteKey.DeclaringEntityType, concreteKey.DeclaringEntityType.GetProperties().ToList())))
                {
                    return;
                }

                if (!explicitName
                    && useDataAnnotations)
                {
                    return;
                }

            }

            var lines = new List<string>
            {
                $".{nameof(EntityTypeBuilder.HasKey)}(e => {GenerateLambdaToKey(key.Properties, "e")})"
            };

            if (explicitName)
            {
                lines.Add($".{nameof(RelationalKeyBuilderExtensions.HasName)}({_cSharpUtilities.DelimitString(key.Relational().Name)})");
            }

            var annotationsToRemove = new List<IAnnotation>();

            foreach (var annotation in annotations)
            {
                if (_annotationCodeGenerator.IsHandledByConvention(key, annotation))
                {
                    annotationsToRemove.Add(annotation);
                }
                else
                {
                    var line = _annotationCodeGenerator.GenerateFluentApi(key, annotation, Language);

                    if (line != null)
                    {
                        lines.Add(line);
                        annotationsToRemove.Add(annotation);
                    }
                }
            }

            lines.AddRange(GenerateAnnotations(annotations.Except(annotationsToRemove)));

            AppendMultiLineFluentApi(lines, sb);
        }

        private void GenerateTableName(IEntityType entityType, IndentedStringBuilder sb)
        {
            var tableName = entityType.Relational().TableName;
            var schema = entityType.Relational().Schema;
            var defaultSchema = entityType.Model.Relational().DefaultSchema;

            var explicitSchema = schema != null && schema != defaultSchema;

            var parameterString = _cSharpUtilities.DelimitString(tableName);
            if (explicitSchema)
            {
                parameterString += ", " + _cSharpUtilities.DelimitString(schema);
            }

            var lines = new List<string>
                {
                    $".{nameof(RelationalEntityTypeBuilderExtensions.ToTable)}({parameterString})"
                };

            AppendMultiLineFluentApi(lines, sb);
        }

        private void GenerateIndex(IIndex index, IndentedStringBuilder sb)
        {
            var lines = new List<string>
            {
                $".{nameof(EntityTypeBuilder.HasIndex)}(e => {GenerateLambdaToKey(index.Properties, "e")})"
            };

            var annotations = index.GetAnnotations().ToList();

            if (!string.IsNullOrEmpty((string)index[RelationalAnnotationNames.Name]))
            {
                lines.Add($".{nameof(RelationalIndexBuilderExtensions.HasName)}({_cSharpUtilities.DelimitString(index.Relational().Name)})");
                RemoveAnnotation(ref annotations, RelationalAnnotationNames.Name);
            }

            if (index.IsUnique)
            {
                lines.Add($".{nameof(IndexBuilder.IsUnique)}()");
            }

            if (index.Relational().Filter != null)
            {
                lines.Add($".{nameof(RelationalIndexBuilderExtensions.HasFilter)}({_cSharpUtilities.DelimitString(index.Relational().Filter)})");
                RemoveAnnotation(ref annotations, RelationalAnnotationNames.Filter);
            }

            var annotationsToRemove = new List<IAnnotation>();

            foreach (var annotation in annotations)
            {
                if (_annotationCodeGenerator.IsHandledByConvention(index, annotation))
                {
                    annotationsToRemove.Add(annotation);
                }
                else
                {
                    var line = _annotationCodeGenerator.GenerateFluentApi(index, annotation, Language);

                    if (line != null)
                    {
                        lines.Add(line);
                        annotationsToRemove.Add(annotation);
                    }
                }
            }

            lines.AddRange(GenerateAnnotations(annotations.Except(annotationsToRemove)));

            AppendMultiLineFluentApi(lines, sb);
        }

        private void GenerateProperty(IProperty property, bool useDataAnnotations, IndentedStringBuilder sb)
        {
            var propertyName = _configuration.GetPropertyName(property.DeclaringEntityType, property);

            var lines = new List<string>
            {
                $".{nameof(EntityTypeBuilder.Property)}(e => e.{propertyName})"
            };

            var annotations = property.GetAnnotations().ToList();

            // Explicit name of column
            //RemoveAnnotation(ref annotations, RelationalAnnotationNames.ColumnName);-

            RemoveAnnotation(ref annotations, RelationalAnnotationNames.ColumnType);
            RemoveAnnotation(ref annotations, CoreAnnotationNames.MaxLengthAnnotation);
            RemoveAnnotation(ref annotations, CoreAnnotationNames.UnicodeAnnotation);
            RemoveAnnotation(ref annotations, RelationalAnnotationNames.DefaultValue);
            RemoveAnnotation(ref annotations, RelationalAnnotationNames.DefaultValueSql);
            RemoveAnnotation(ref annotations, RelationalAnnotationNames.ComputedColumnSql);
            RemoveAnnotation(ref annotations, ScaffoldingAnnotationNames.ColumnOrdinal);

            if (!useDataAnnotations)
            {
                if (!property.IsNullable
                    && property.ClrType.IsNullableType()
                    && !property.IsPrimaryKey())
                {
                    lines.Add($".{nameof(PropertyBuilder.IsRequired)}()");
                }

                var columnName = property.Relational().ColumnName;

                if (columnName != null
                    && columnName != property.Name)
                {
                    lines.Add($".{nameof(RelationalPropertyBuilderExtensions.HasColumnName)}({_cSharpUtilities.DelimitString(columnName)})");
                }

                var columnType = property.Relational().ColumnType;

                if (columnType != null)
                {
                    lines.Add($".{nameof(RelationalPropertyBuilderExtensions.HasColumnType)}({_cSharpUtilities.DelimitString(columnType)})");
                }

                var maxLength = property.GetMaxLength();

                if (maxLength.HasValue)
                {
                    lines.Add($".{nameof(PropertyBuilder.HasMaxLength)}({_cSharpUtilities.GenerateLiteral(maxLength.Value)})");
                }
            }

            if (property.IsUnicode() != null)
            {
                lines.Add($".{nameof(PropertyBuilder.IsUnicode)}({(property.IsUnicode() == false ? _cSharpUtilities.GenerateLiteral(false) : "")})");
            }

            if (property.Relational().DefaultValue != null)
            {
                lines.Add($".{nameof(RelationalPropertyBuilderExtensions.HasDefaultValue)}({_cSharpUtilities.GenerateLiteral((dynamic)property.Relational().DefaultValue)})");
            }

            if (property.Relational().DefaultValueSql != null)
            {
                lines.Add($".{nameof(RelationalPropertyBuilderExtensions.HasDefaultValueSql)}({_cSharpUtilities.DelimitString(property.Relational().DefaultValueSql)})");
            }

            if (property.Relational().ComputedColumnSql != null)
            {
                lines.Add($".{nameof(RelationalPropertyBuilderExtensions.HasComputedColumnSql)}({_cSharpUtilities.DelimitString(property.Relational().ComputedColumnSql)})");
            }

            var valueGenerated = property.ValueGenerated;
            var isRowVersion = false;
            if (((Property)property).GetValueGeneratedConfigurationSource().HasValue
                && new RelationalValueGeneratorConvention().GetValueGenerated((Property)property) != valueGenerated)
            {
                string methodName;
                switch (valueGenerated)
                {
                    case ValueGenerated.OnAdd:
                        methodName = nameof(PropertyBuilder.ValueGeneratedOnAdd);
                        break;

                    case ValueGenerated.OnAddOrUpdate:
                        isRowVersion = property.IsConcurrencyToken;
                        methodName = isRowVersion
                            ? nameof(PropertyBuilder.IsRowVersion)
                            : nameof(PropertyBuilder.ValueGeneratedOnAddOrUpdate);
                        break;

                    case ValueGenerated.Never:
                        methodName = nameof(PropertyBuilder.ValueGeneratedNever);
                        break;

                    default:
                        methodName = "";
                        break;
                }

                lines.Add($".{methodName}()");
            }

            if (property.IsConcurrencyToken
                && !isRowVersion)
            {
                lines.Add($".{nameof(PropertyBuilder.IsConcurrencyToken)}()");
            }

            var annotationsToRemove = new List<IAnnotation>();

            foreach (var annotation in annotations)
            {
                if (_annotationCodeGenerator.IsHandledByConvention(property, annotation))
                {
                    annotationsToRemove.Add(annotation);
                }
                else
                {
                    var line = _annotationCodeGenerator.GenerateFluentApi(property, annotation, Language);

                    if (line != null)
                    {
                        lines.Add(line);
                        annotationsToRemove.Add(annotation);
                    }
                }
            }

            lines.AddRange(GenerateAnnotations(annotations.Except(annotationsToRemove)));

            switch (lines.Count)
            {
                case 1:
                    return;
                case 2:
                    lines = new List<string>
                    {
                        lines[0] + lines[1]
                    };
                    break;
            }

            AppendMultiLineFluentApi(lines, sb);
        }

        private void GenerateRelationship(IForeignKey foreignKey, bool useDataAnnotations, IndentedStringBuilder sb)
        {
            var canUseDataAnnotations = true;
            var annotations = foreignKey.GetAnnotations().ToList();

            var dependentToPrincipalPropertyName = _configuration.GetNavigationPropertyName(foreignKey.DependentToPrincipal);
            var principalToDependentPropertyName = _configuration.GetNavigationPropertyName(foreignKey.PrincipalToDependent);

            var lines = new List<string>
            {
                /**
                $".{nameof(EntityTypeBuilder.HasOne)}(d => d.{foreignKey.DependentToPrincipal.Name})",
                $".{(foreignKey.IsUnique ? nameof(ReferenceNavigationBuilder.WithOne) : nameof(ReferenceNavigationBuilder.WithMany))}"
                + $"(p => p.{foreignKey.PrincipalToDependent.Name})"
                **/

                $".{nameof(EntityTypeBuilder.HasOne)}(d => d.{dependentToPrincipalPropertyName})",
                $".{(foreignKey.IsUnique ? nameof(ReferenceNavigationBuilder.WithOne) : nameof(ReferenceNavigationBuilder.WithMany))}"
                + $"(p => p.{principalToDependentPropertyName}{(foreignKey.IsUnique ? "" : "List")})"
            };

            if (!foreignKey.PrincipalKey.IsPrimaryKey())
            {
                canUseDataAnnotations = false;
                lines.Add($".{nameof(ReferenceReferenceBuilder.HasPrincipalKey)}"
                    + $"{(foreignKey.IsUnique ? $"<{foreignKey.PrincipalEntityType.DisplayName()}>" : "")}"
                    + $"(p => {GenerateLambdaToKey(foreignKey.PrincipalKey.Properties, "p")})");
            }

            lines.Add($".{nameof(ReferenceReferenceBuilder.HasForeignKey)}"
                      + $"{(foreignKey.IsUnique ? $"<{foreignKey.DeclaringEntityType.DisplayName()}>" : "")}"
                      + $"(d => {GenerateLambdaToKey(foreignKey.Properties, "d")})");

            var defaultOnDeleteAction = foreignKey.IsRequired
                ? DeleteBehavior.Cascade
                : DeleteBehavior.ClientSetNull;

            if (foreignKey.DeleteBehavior != defaultOnDeleteAction)
            {
                canUseDataAnnotations = false;
                lines.Add($".{nameof(ReferenceReferenceBuilder.OnDelete)}({_cSharpUtilities.GenerateLiteral(foreignKey.DeleteBehavior)})");
            }

            if (!string.IsNullOrEmpty((string)foreignKey[RelationalAnnotationNames.Name]))
            {
                canUseDataAnnotations = false;
                lines.Add($".{nameof(RelationalReferenceReferenceBuilderExtensions.HasConstraintName)}({_cSharpUtilities.DelimitString(foreignKey.Relational().Name)})");
                RemoveAnnotation(ref annotations, RelationalAnnotationNames.Name);
            }

            var annotationsToRemove = new List<IAnnotation>();

            foreach (var annotation in annotations)
            {
                if (_annotationCodeGenerator.IsHandledByConvention(foreignKey, annotation))
                {
                    annotationsToRemove.Add(annotation);
                }
                else
                {
                    var line = _annotationCodeGenerator.GenerateFluentApi(foreignKey, annotation, Language);

                    if (line != null)
                    {
                        canUseDataAnnotations = false;
                        lines.Add(line);
                        annotationsToRemove.Add(annotation);
                    }
                }
            }

            lines.AddRange(GenerateAnnotations(annotations.Except(annotationsToRemove)));

            if (!useDataAnnotations || !canUseDataAnnotations)
            {
                AppendMultiLineFluentApi(lines, sb);
            }
        }

        private void GenerateSequence(ISequence sequence, IndentedStringBuilder sb)
        {
            var methodName = nameof(RelationalModelBuilderExtensions.HasSequence);

            if (sequence.ClrType != Sequence.DefaultClrType)
            {
                methodName += $"<{_cSharpUtilities.GetTypeName(sequence.ClrType)}>";
            }

            var parameters = _cSharpUtilities.DelimitString(sequence.Name);

            if (string.IsNullOrEmpty(sequence.Schema)
                && sequence.Model.Relational().DefaultSchema != sequence.Schema)
            {
                parameters += $", {_cSharpUtilities.DelimitString(sequence.Schema)}";
            }

            var lines = new List<string>
            {
                $"modelBuilder.{methodName}({parameters})"
            };

            if (sequence.StartValue != Sequence.DefaultStartValue)
            {
                lines.Add($".{nameof(SequenceBuilder.StartsAt)}({sequence.StartValue})");
            }

            if (sequence.IncrementBy != Sequence.DefaultIncrementBy)
            {
                lines.Add($".{nameof(SequenceBuilder.IncrementsBy)}({sequence.IncrementBy})");
            }

            if (sequence.MinValue != Sequence.DefaultMinValue)
            {
                lines.Add($".{nameof(SequenceBuilder.HasMin)}({sequence.MinValue})");
            }

            if (sequence.MaxValue != Sequence.DefaultMaxValue)
            {
                lines.Add($".{nameof(SequenceBuilder.HasMax)}({sequence.MaxValue})");
            }

            if (sequence.IsCyclic != Sequence.DefaultIsCyclic)
            {
                lines.Add($".{nameof(SequenceBuilder.IsCyclic)}()");
            }

            if (lines.Count == 2)
            {
                lines = new List<string>
                {
                    lines[0] + lines[1]
                };
            }

            sb.AppendLine();
            sb.Append(lines[0]);

            using (sb.Indent())
            {
                foreach (var line in lines.Skip(1))
                {
                    sb.AppendLine();
                    sb.Append(line);
                }
            }

            sb.AppendLine(";");
        }

        private string GenerateLambdaToKey(
            IReadOnlyCollection<IProperty> properties,
            string lambdaIdentifier)
        {
            if (properties.Count <= 0)
                return string.Empty;

            var propertyNames = new List<string>();
            foreach (var property in properties)
            {
                var propertyName = _configuration.GetPropertyName(property.DeclaringEntityType, property);
                propertyNames.Add(propertyName);
            }

            return propertyNames.Count == 1
                ? $"{lambdaIdentifier}.{propertyNames[0]}"
                : $"new {{ {string.Join(", ", propertyNames.Select(p => lambdaIdentifier + "." + p))} }}";
        }

        private void RemoveAnnotation(ref List<IAnnotation> annotations, string annotationName)
        {
            annotations.Remove(annotations.SingleOrDefault(a => a.Name == annotationName));
        }

        private IList<string> GenerateAnnotations(IEnumerable<IAnnotation> annotations)
        {
            return annotations.Select(GenerateAnnotation).ToList();
        }

        private string GenerateAnnotation(IAnnotation annotation)
        {
            return $".HasAnnotation({_cSharpUtilities.DelimitString(annotation.Name)}, {_cSharpUtilities.GenerateLiteral((dynamic)annotation.Value)})";
        }
    }
}
