﻿namespace Crudzilla.Configuration
{
    public class TestConfig
    {
        public int NumberOfEntitiesGenerated { get; set; } = 10;
        public WebApiTestConfig WebApi { get; set; }
        public ApplicationServiceTestConfig ApplicationService { get; set; }
        public DomainTestConfig Domain { get; set; }
    }
}
