﻿using System;
using System.IO;
using System.Xml.Linq;

namespace Crudzilla.Configuration
{
    public class ApplicationServiceTestConfig
    {
        public ApplicationServiceTestConfig(string projectName)
        {
            ProjectName = projectName;
        }

        public bool Disabled
        {
            get => string.IsNullOrWhiteSpace(ProjectName);
        }

        public string ProjectName { get; set; }
        internal string ModuleTestClassName { get; set; }
        internal string ModuleTestNamespace { get; set; }
        internal string Namespace { get; set; }
        internal string OutputPath { get; set; }

        internal string GetScaffoldOutputPath(string folder = "")
            => Path.Combine(OutputPath, "Scaffold", folder);

        internal string GetInterfacesScaffoldOutputPath(string folder = "")
            => Path.Combine(OutputPath, "Scaffold", "Interfaces", folder);

        internal string GetOutputPath()
            => OutputPath;

        internal void ReadAttributes(FileInfo projectFileInfo)
        {
            if (projectFileInfo == null)
                throw new ArgumentNullException(nameof(projectFileInfo));

            var document = XDocument.Load(projectFileInfo.FullName);

            var rootNamespace = document
                .GetElement("Project")
                .GetElement("PropertyGroup")
                .GetElement("RootNamespace")?.Value;

            if (string.IsNullOrWhiteSpace(rootNamespace))
                rootNamespace = projectFileInfo.Name.Replace(".csproj", string.Empty);

            Namespace = rootNamespace;
            OutputPath = Path.Combine(projectFileInfo.Directory.FullName, "Tests");

            var moduleFile = projectFileInfo.FindFile("*Module*.cs");

            ModuleTestClassName = moduleFile.GetClassName();
            ModuleTestNamespace = moduleFile.GetNamespace();
        }
    }
}
