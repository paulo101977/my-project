﻿using System;
using System.IO;
using System.Xml.Linq;

namespace Crudzilla.Configuration
{
    public class WebApiConfig
    {
        public WebApiConfig(string projectName)
        {
            ProjectName = projectName;
        }

        public string ProjectName { get; set; }

        internal string Namespace { get; set; }
        internal string OutputPath { get; set; }
        internal string RouteConstantsClassName { get; set; } = "RouteConsts";

        internal string GetControllersScaffoldOutputPath()
            => Path.Combine(OutputPath, "Scaffold", "Controllers");

        internal string GetControllersOutputPath()
            => Path.Combine(OutputPath, "Controllers");

        internal string GetScaffoldRouteConstansOutputPath()
            => Path.Combine(OutputPath, "Scaffold");

        internal string GetRouteConstansOutputPath()
            => OutputPath;

        internal void ReadAttributes(FileInfo projectFileInfo)
        {
            if (projectFileInfo == null)
                throw new ArgumentNullException(nameof(projectFileInfo));

            var document = XDocument.Load(projectFileInfo.FullName);

            var rootNamespace = document
                .GetElement("Project")
                .GetElement("PropertyGroup")
                .GetElement("RootNamespace")?.Value;

            if (string.IsNullOrWhiteSpace(rootNamespace))
                rootNamespace = projectFileInfo.Name.Replace(".csproj", string.Empty);

            Namespace = rootNamespace;
            OutputPath = projectFileInfo.Directory.FullName;
        }
    }
}
