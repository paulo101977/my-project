﻿using System;
using System.IO;
using System.Xml.Linq;

namespace Crudzilla.Configuration
{
    public class ApplicationServiceConfig
    {
        public ApplicationServiceConfig(string projectName)
        {
            ProjectName = projectName;
        }

        public string ProjectName { get; set; }

        internal string InterfaceNamespace { get; set; }
        internal string InterfaceOutputPath { get; set; }
        internal string Namespace { get; set; }
        internal string OutputPath { get; set; }

        internal string GetScaffoldInterfacesOutputPath()
            => Path.Combine(InterfaceOutputPath, "Scaffold");

        internal string GetScaffoldServicesOutputPath()
            => Path.Combine(OutputPath, "Scaffold");

        internal string GetInterfacesOutputPath(string folder)
            => Path.Combine(InterfaceOutputPath, folder);

        internal string GetServicesOutputPath(string folder)
            => Path.Combine(OutputPath, folder);

        internal void ReadAttributes(FileInfo projectFileInfo)
        {
            if (projectFileInfo == null)
                throw new ArgumentNullException(nameof(projectFileInfo));

            var document = XDocument.Load(projectFileInfo.FullName);

            var rootNamespace = document
                .GetElement("Project")
                .GetElement("PropertyGroup")
                .GetElement("RootNamespace")?.Value;

            if (string.IsNullOrWhiteSpace(rootNamespace))
                rootNamespace = projectFileInfo.Name.Replace(".csproj", string.Empty);

            Namespace = $"{rootNamespace}.Services";
            InterfaceNamespace = $"{rootNamespace}.Interfaces";

            OutputPath = Path.Combine(projectFileInfo.Directory.FullName, "Services");
            InterfaceOutputPath = Path.Combine(projectFileInfo.Directory.FullName, "Interfaces");
        }
    }
}
