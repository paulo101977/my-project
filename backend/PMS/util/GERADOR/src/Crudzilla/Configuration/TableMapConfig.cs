﻿namespace Crudzilla.Configuration
{
    public class TableMapConfig
    {
        public string Name { get; set; }
        public string Alias { get; set; }
        public string PluralizedAlias { get; set; }
        public string WebApiRouteName { get; set; }
    }
}
