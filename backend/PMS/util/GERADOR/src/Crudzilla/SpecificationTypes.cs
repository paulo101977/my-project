﻿namespace Crudzilla
{
    internal enum SpecificationTypes
    {
        MustHave,
        OutOfBound,
        Invalid
    }
}
