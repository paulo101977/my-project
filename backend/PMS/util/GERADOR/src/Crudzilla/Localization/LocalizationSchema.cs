﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Crudzilla.Localization
{
    internal class LocalizationSchema
    {
        [JsonProperty("culture")]
        public string Culture { get; set; }
        [JsonProperty("texts")]
        public Dictionary<string, string> Texts { get; set; } = new Dictionary<string, string>();
    }
}
