﻿using Crudzilla.Configuration;
using Crudzilla.Writters;
using Microsoft.EntityFrameworkCore.Design.Internal;
using Microsoft.EntityFrameworkCore.Scaffolding.Internal;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;

namespace Crudzilla
{
    public static class Startup
    {
        internal static IServiceProvider ConfigureServices(CrudzillaConfig config)
        {
            var services = new ServiceCollection();

            if (config.DatabaseType == DatabaseType.SqlServer)
            {
                new SqlServerDesignTimeServices().ConfigureDesignTimeServices(services);
            }

            services
                .AddLogging()
                .AddSingleton<WebApiTestWriter>()
                .AddSingleton<ApplicationTestWriter>()
                .AddSingleton<DomainTestWriter>()
                .AddSingleton<WebApiWriter>()
                .AddSingleton<ConstantsWriter>()
                .AddSingleton<ApplicationServiceWriter>()
                .AddSingleton<DataTransferWriter>()
                .AddSingleton<BuilderWriter>()
                .AddSingleton<OrmMapWriter>()
                .AddSingleton<AdapterWriter>()
                .AddSingleton<AutoMapperProfileWriter>()

                .AddScaffolding(new CustomOperationReportHandler())

                .AddSingleton<ICSharpEntityTypeGenerator, CustomCSharpEntityTypeGenerator>()
                .AddSingleton<ICSharpDbContextGenerator, CustomCSharpDbContextGenerator>()
                .AddSingleton<IScaffoldingCodeGenerator, CustomCSharpScaffoldingGenerator>()
                .AddSingleton<IOperationReporter, CustomOperationReportHandler>()
                .AddSingleton(config);

            var serviceProvider = services.BuildServiceProvider();
            return serviceProvider;
        }

        public static void Dracarys(CrudzillaConfig config)
        {
            var services = ConfigureServices(config);

            var logger = services.GetService<ILoggerFactory>()
                .AddConsole(LogLevel.Error)
                .AddConsole(LogLevel.Warning)
#if DEBUG
                .AddConsole(LogLevel.Debug)
#endif
                .CreateLogger<CustomCSharpScaffoldingGenerator>();

            logger.LogDebug("Starting application");

            var generator = services.GetRequiredService<IModelScaffolder>();

            generator.Generate(
                config.ConnectionString,
                config.TablesSelection.Select(s => s.Name),
                Enumerable.Empty<string>(),
                config.Orm.OutputPath,
                outputPath: config.Orm.OutputPath,
                rootNamespace: config.Orm.Namespace,
                contextName: config.Orm.ContextClassName,
                useDataAnnotations: false,
                overwriteFiles: true,
                useDatabaseNames: true);

            logger.LogDebug("All done!");
        }
    }
}
