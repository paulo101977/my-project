﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace interfacesGenerate
{
    class Program
    {
        static void Main(string[] args)
        {
            var directoryList = new List<string>
            {
                //application
                //@"C:\Projetos\Thex\Thex_Novo_TNF20\Thex\src\Thex.Application\Adapters",
                //@"C:\Projetos\Thex\Thex_Novo_TNF20\Thex\src\Thex.Application\Interfaces",

                //domain
                //@"C:\Projetos\Thex\Thex_Novo_TNF20\Thex\src\Thex.Domain\Interfaces\Factories",
                //@"C:\Projetos\Thex\Thex_Novo_TNF20\Thex\src\Thex.Domain\Interfaces\Integration",
                //@"C:\Projetos\Thex\Thex_Novo_TNF20\Thex\src\Thex.Domain\Interfaces\Repositories",
                //@"C:\Projetos\Thex\Thex_Novo_TNF20\Thex\src\Thex.Domain\Interfaces\Services",

                //infra
                @"C:\Projetos\Thex\Thex_Novo_TNF20\Thex\src\Thex.Infra\ReadInterfaces"
            };

            directoryList.ForEach(delegate(string dir)
            {
                WriteComment();
                Console.WriteLine($"diretório: {dir}");
                WriteComment();

                Directory.GetFiles(dir, "*.cs", SearchOption.AllDirectories)
                    .Select(Path.GetFileNameWithoutExtension)
                    .ToList()
                    .ForEach(delegate (string name)
                    {
                        if(char.IsUpper(name[0]) && char.IsUpper(name[1]) && !name.Contains("IScaffold"))
                            Console.WriteLine($"services.AddTransient<{name}, {name.Substring(1)}>();");
                    });

                WriteComment();

            });

            Console.ReadKey();
        }

        private static void WriteComment()
        {
            for (int i = 0; i < 4; i++)
                Console.WriteLine("----------------------------------------------------------------------------------------");
        }
    }
}
