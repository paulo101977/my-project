SELECT 
''+
'{'+
'   "Name": "'+table_name+'",'+
'   "Alias": "'+table_name+'",'+
'   "PluralizedAlias": "'+ CASE RIGHT(table_name,1) WHEN 'y' THEN replace(table_name,'y','ies') ELSE table_name + 's' END +'",'+
'   "WebApiRouteName": "'+table_name+'"'+
'}'+
''
FROM information_schema.tables  
WHERE table_type = 'base table' AND TABLE_CATALOG='Thex.Database'
order by table_name