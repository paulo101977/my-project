﻿using Microsoft.Extensions.Configuration;
using System.IO;

namespace Crudzilla.Console
{
    public static class Program
    {
        public static void Main(string[] args)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory());

            builder.AddJsonFile("appsettings.ThexConfig.json", optional: false, reloadOnChange: true);

            var configuration = builder.Build();

            var config = new ScaffoldConfig();
            configuration.Bind(config);

            var scaffold = config.GetFullConfiguration();
            Startup.Dracarys(scaffold);
        }
    }
}