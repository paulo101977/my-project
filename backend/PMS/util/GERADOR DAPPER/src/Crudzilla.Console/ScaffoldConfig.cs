﻿using Crudzilla.Configuration;
using System.Collections.Generic;

namespace Crudzilla.Console
{
    public class ScaffoldConfig
    {
        public ScaffoldConfig()
        {
            Localization = new LocalizationConfig();
            TablesSelection = new List<TableMapConfig>();
            Orm = new OrmConfig();
        }

        public string ConnectionString { get; set; }
        public string SolutionPath { get; set; }

        public LocalizationConfig Localization { get; set; }
        public string AutoMapper { get; set; }
        public string DataTransfer { get; set; }
        public string Entity { get; set; }
        public OrmConfig Orm { get; set; }
        public string Adapter { get; set; }
        public string Builder { get; set; }
        public List<TableMapConfig> TablesSelection { get; set; }

        public CrudzillaConfig GetFullConfiguration()
        {
            var scaffoldConfig = new CrudzillaConfig()
            {
                Adapter = new AdapterConfig(Adapter),
                AutoMapper = new AutoMapperConfig(AutoMapper),
                Builder = new BuilderConfig(Builder),
                ConnectionString = ConnectionString,
                DataTransfer = new DataTransferConfig(DataTransfer),
                Entity = new EntityConfig(Entity),
                Localization = Localization,
                Orm = Orm,
                SolutionPath = SolutionPath,
                TablesSelection = TablesSelection
            };

            scaffoldConfig.Initialize();

            return scaffoldConfig;
        }
    }
}
