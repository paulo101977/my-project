﻿using Crudzilla.Configuration;
using JetBrains.Annotations;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Scaffolding.Internal;
using Microsoft.EntityFrameworkCore.Storage;

namespace Crudzilla.Writters
{
    internal class BuilderWriter
    {
        private CrudzillaConfig Configuration { get; }
        private IRelationalTypeMapper RelationalTypeMapper { get; }
        private ICSharpUtilities CSharpUtilities { get; }

        private IndentedStringBuilder _sb;

        private string EntityName;
        private string BuilderName;

        public BuilderWriter(
            [NotNull] ICSharpUtilities cSharpUtilities,
            [NotNull] CrudzillaConfig scaffoldConfiguration,
            [NotNull] IRelationalTypeMapper relationalTypeMapper)
        {
            Check.NotNull(cSharpUtilities, nameof(cSharpUtilities));
            Check.NotNull(scaffoldConfiguration, nameof(scaffoldConfiguration));
            Check.NotNull(relationalTypeMapper, nameof(relationalTypeMapper));

            CSharpUtilities = cSharpUtilities;
            Configuration = scaffoldConfiguration;
            RelationalTypeMapper = relationalTypeMapper;
        }

        public virtual string WriteCodeScaffold([NotNull] IEntityType entityType)
        {
            Check.NotNull(entityType, nameof(entityType));

            //--
            EntityName = Configuration.GetEntityNameOrDefault(entityType);
            BuilderName = $"{Configuration.GetBuilderName(entityType)}";
            //--

            _sb = new IndentedStringBuilder();

            _sb.AppendLine(Configuration.HeaderGeneratedFile);

            _sb.AppendLine("using System;");
            _sb.AppendLine("using System.Collections.Generic;");
            _sb.AppendLine("using Tnf.Builder;");
            _sb.AppendLine("using Tnf.Notifications;");
            _sb.AppendLine("using Tnf.Specifications;");
            _sb.AppendLine($"using {Configuration.Entity.Namespace};");
            _sb.AppendLine($"using {Configuration.Localization.Namespace};");
            _sb.AppendLine("using Thex.Common;");

            _sb.AppendLine();
            _sb.AppendLine($"namespace {Configuration.Entity.Namespace}");
            _sb.AppendLine("{");

            using (_sb.Indent())
            {
                _sb.AppendLine($"public partial class {EntityName}");
                _sb.AppendLine("{");

                using (_sb.Indent())
                {
                    GenerateClass(entityType);
                }

                _sb.AppendLine("}");
            }

            _sb.AppendLine("}");

            return _sb.ToString();
        }

        public virtual void GenerateClass([NotNull] IEntityType entityType)
        {
            var externalBuilderName = Configuration.GetBuilderName(entityType);

            _sb.AppendLine($"public class Builder : Builder<{EntityName}>");
            _sb.AppendLine("{");
            using (_sb.Indent())
            {
                GenerateConstructor();
                GenerateMethods(entityType);
                GenerateSpecifications(entityType);
            }
            _sb.AppendLine("}");
        }

        public virtual void GenerateConstructor()
        {
            _sb.AppendLine($"public Builder(INotificationHandler handler) : base(handler)");
            _sb.AppendLine("{");
            _sb.AppendLine("}");

            _sb.AppendLine($"public Builder(INotificationHandler handler, {EntityName} instance) : base(handler, instance)");
            _sb.AppendLine("{");
            _sb.AppendLine("}");

            _sb.AppendLine();
        }

        public virtual void GenerateMethods([NotNull] IEntityType entityType)
        {
            var externalBuilderName = Configuration.GetBuilderName(entityType);

            var entityKey = entityType.GetEntityKeyType(CSharpUtilities);

            if (!string.IsNullOrWhiteSpace(entityKey))
            {
                // Default method for PK
                _sb.AppendLine($"public virtual Builder WithId({entityKey} id)");
                _sb.AppendLine("{");
                using (_sb.Indent())
                {
                    _sb.AppendLine($"Instance.Id = id;");
                    _sb.AppendLine($"return this;");
                }
                _sb.AppendLine("}");
            }

            foreach (var property in Configuration.GetFullOrderedProperties(entityType, CSharpUtilities))
            {
                var methodName = Configuration.GetPropertyName(entityType, property);
                var parameterName = methodName.ToCamelCase();

                if (methodName == "Id" || methodName == $"{EntityName}Id")
                    continue;

                _sb.AppendLine($"public virtual Builder With{methodName}({CSharpUtilities.GetTypeName(property.ClrType)} {parameterName})");
                _sb.AppendLine("{");
                using (_sb.Indent())
                {
                    _sb.AppendLine($"Instance.{methodName} = {parameterName};");
                    _sb.AppendLine($"return this;");
                }
                _sb.AppendLine("}");
            }
        }

        private void GenerateSpecifications([NotNull] IEntityType entityType)
        {
            _sb.AppendLine();
            _sb.AppendLine($"protected override void Specifications()");
            _sb.AppendLine("{");

            using (_sb.Indent())
            {
                foreach (var property in Configuration.GetOrderedProperties(entityType, CSharpUtilities))
                {
                    var specificationExpression = Configuration.GetSpecificationsExpression(RelationalTypeMapper, entityType, property, CSharpUtilities);
                    if (specificationExpression.Count > 0)
                    {
                        foreach (var item in specificationExpression)
                        {
                            _sb.AppendLine(item);
                            _sb.AppendLine();
                        }
                    }
                }
            }

            _sb.AppendLine("}");
        }
    }
}