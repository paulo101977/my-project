﻿using Crudzilla.Configuration;
using JetBrains.Annotations;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Scaffolding.Internal;
using System.Linq;

namespace Crudzilla.Writters
{
    internal class AdapterWriter
    {
        private CrudzillaConfig Configuration { get; }
        private ICSharpUtilities CSharpUtilities { get; }

        private IndentedStringBuilder _sb;

        private string EntityName;
        private string BuilderName;

        public AdapterWriter(
            [NotNull] ICSharpUtilities cSharpUtilities,
            [NotNull] CrudzillaConfig scaffoldConfiguration)
        {
            Check.NotNull(cSharpUtilities, nameof(cSharpUtilities));
            Check.NotNull(scaffoldConfiguration, nameof(scaffoldConfiguration));

            CSharpUtilities = cSharpUtilities;
            Configuration = scaffoldConfiguration;
        }

        public string WriteCodeScaffold([NotNull] IEntityType entityType)
        {
            Check.NotNull(entityType, nameof(entityType));

            //---
            EntityName = Configuration.GetEntityNameOrDefault(entityType);
            BuilderName = Configuration.GetBuilderName(entityType);
            //---

            _sb = new IndentedStringBuilder();

            _sb.AppendLine(Configuration.HeaderGeneratedFile);

            _sb.AppendLine($"using {Configuration.Entity.Namespace};");
            _sb.AppendLine($"using {Configuration.DataTransfer.Namespace};");
            _sb.AppendLine($"using Tnf;");
            _sb.AppendLine($"using Tnf.Notifications;");

            _sb.AppendLine();
            _sb.AppendLine($"namespace {Configuration.Adapter.Namespace}");
            _sb.AppendLine("{");
            using (_sb.Indent())
            {
                GenerateClass(entityType);
            }
            _sb.AppendLine("}");

            return _sb.ToString();
        }

        private void GenerateClass([NotNull] IEntityType entityType)
        {
            Check.NotNull(entityType, nameof(entityType));

            var properties = Configuration.GetFullOrderedProperties(entityType, CSharpUtilities);
            if (!properties.Any())
                return;

            _sb.AppendLine($"public class {EntityName}Adapter : I{EntityName}Adapter");
            _sb.AppendLine("{");
            using (_sb.Indent())
            {
                _sb.AppendLine("public INotificationHandler NotificationHandler { get; }");
                _sb.AppendLine();
                _sb.AppendLine($"public {EntityName}Adapter(INotificationHandler notificationHandler)");
                _sb.AppendLine("{");
                using (_sb.Indent())
                {
                    _sb.AppendLine("NotificationHandler = notificationHandler;");
                }
                _sb.AppendLine("}");
                _sb.AppendLine();
                _sb.AppendLine($"public virtual {BuilderName} Map({EntityName} entity, {EntityName}Dto dto)");
                _sb.AppendLine("{");
                using (_sb.Indent())
                {
                    _sb.AppendLine($"Check.NotNull(entity, nameof(entity));");
                    _sb.AppendLine($"Check.NotNull(dto, nameof(dto));");
                    _sb.AppendLine();
                    _sb.AppendLine($"var builder = new {BuilderName}(NotificationHandler, entity)");
                    using (_sb.Indent())
                    {
                        _sb.AppendLine($".WithId(dto.Id)");

                        var lastProperty = properties.Last();
                        foreach (var property in properties)
                        {
                            var propertyName = Configuration.GetPropertyName(entityType, property);
                            if (propertyName == "Id" || propertyName == $"{EntityName}Id")
                                continue;

                            var end = (lastProperty == property) ? ";" : string.Empty;
                            _sb.AppendLine($".With{propertyName}(dto.{propertyName}){end}");
                        }
                    }
                    _sb.AppendLine();
                    _sb.AppendLine("return builder;");
                }
                _sb.AppendLine("}");
                _sb.AppendLine();
                _sb.AppendLine($"public virtual {BuilderName} Map({EntityName}Dto dto)");
                _sb.AppendLine("{");
                using (_sb.Indent())
                {
                    _sb.AppendLine($"Check.NotNull(dto, nameof(dto));");
                    _sb.AppendLine();
                    _sb.AppendLine($"var builder = new {BuilderName}(NotificationHandler)");
                    using (_sb.Indent())
                    {
                        _sb.AppendLine($".WithId(dto.Id)");

                        var lastProperty = properties.Last();
                        foreach (var property in properties)
                        {
                            var propertyName = Configuration.GetPropertyName(entityType, property);
                            if (propertyName == "Id" || propertyName == $"{EntityName}Id")
                                continue;

                            var end = (lastProperty == property) ? ";" : string.Empty;
                            _sb.AppendLine($".With{propertyName}(dto.{propertyName}){end}");
                        }
                    }
                    _sb.AppendLine();
                    _sb.AppendLine("return builder;");
                }
                _sb.AppendLine("}");
            }
            _sb.AppendLine("}");
        }

        public string WriteCodeInterfaceScaffold()
        {
            var sb = new IndentedStringBuilder();

            sb.AppendLine(Configuration.HeaderGeneratedFile);

            sb.AppendLine($"using {Configuration.Entity.Namespace};");
            sb.AppendLine($"using {Configuration.DataTransfer.Namespace};");
            sb.AppendLine();
            sb.AppendLine($"namespace {Configuration.Adapter.Namespace}");
            sb.AppendLine("{");
            using (sb.Indent())
            {
                sb.AppendLine($"public interface I{EntityName}Adapter");
                sb.AppendLine("{");
                using (sb.Indent())
                {
                    sb.AppendLine($"{BuilderName} Map({EntityName} entity, {EntityName}Dto dto);");
                    sb.AppendLine($"{BuilderName} Map({EntityName}Dto dto);");
                }
                sb.AppendLine("}");
            }
            sb.AppendLine("}");

            return sb.ToString();
        }
    }
}
