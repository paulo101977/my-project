﻿using Crudzilla.Configuration;
using JetBrains.Annotations;
using Microsoft.EntityFrameworkCore.Internal;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Scaffolding.Internal;

namespace Crudzilla.Writters
{
    internal class OrmMapWriter
    {
        private CrudzillaConfig Configuration { get; }
        private ICSharpUtilities CSharpUtilities { get; }

        private IndentedStringBuilder _sb;

        private string EntityName;
        private string EntityTypeConfigurationName;

        public OrmMapWriter(
            [NotNull] ICSharpUtilities cSharpUtilities,
            [NotNull] CrudzillaConfig scaffoldConfiguration)
        {
            Check.NotNull(cSharpUtilities, nameof(cSharpUtilities));
            Check.NotNull(scaffoldConfiguration, nameof(scaffoldConfiguration));

            CSharpUtilities = cSharpUtilities;
            Configuration = scaffoldConfiguration;
        }

        public string WriteCode([NotNull] IEntityType entityType, [NotNull] string fluentConfigurationEntity)
        {
            Check.NotNull(entityType, nameof(entityType));

            //---
            EntityName = Configuration.GetEntityNameOrDefault(entityType);
            EntityTypeConfigurationName = Configuration.GetEntityTypeConfigurationName(entityType);
            //---

            _sb = new IndentedStringBuilder();

            _sb.AppendLine(Configuration.HeaderGeneratedFile);
            _sb.AppendLine("using DapperExtensions.Mapper;");
            _sb.AppendLine("using System;");
            _sb.AppendLine("using System.Collections.Generic;");
            _sb.AppendLine("using System.Text;");
            _sb.AppendLine($"using {Configuration.Entity.Namespace};");

            _sb.AppendLine();
            _sb.AppendLine($"namespace {Configuration.Orm.GetMappersNamespace()}");
            _sb.AppendLine("{");
            using (_sb.Indent())
            {
                AddFluentConfiguration(fluentConfigurationEntity);
            }
            _sb.AppendLine("}");

            return _sb.ToString();
        }

        private void AddFluentConfiguration(string fluentConfigurationEntity)
        {
            _sb.AppendLine($"public sealed class {EntityTypeConfigurationName} : ClassMapper<{EntityName}>");
            _sb.AppendLine("{");
            using (_sb.Indent())
            {
                _sb.AppendLine($"public {EntityTypeConfigurationName}()");
                _sb.AppendLine("{");
                using (_sb.Indent())
                {
                    _sb.AppendLine(fluentConfigurationEntity);
                }
                _sb.AppendLine("}");
            }
            _sb.AppendLine("}");
        }
    }
}
