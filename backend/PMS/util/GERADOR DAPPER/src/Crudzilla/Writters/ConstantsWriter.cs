﻿using Crudzilla.Configuration;
using JetBrains.Annotations;
using Microsoft.EntityFrameworkCore.Internal;
using Microsoft.EntityFrameworkCore.Metadata;
using System.Collections.Generic;

namespace Crudzilla.Writters
{
    internal class ConstantsWriter
    {
        private CrudzillaConfig Configuration { get; }

        public ConstantsWriter(
           [NotNull] CrudzillaConfig configuration)
        {
            Configuration = Check.NotNull(configuration, nameof(configuration));
        }

        public virtual string WriteCodeEntityName([NotNull] IEnumerable<IEntityType> entityTypes)
        {
            Check.NotNull(entityTypes, nameof(entityTypes));

            var sb = new IndentedStringBuilder();

            sb.AppendLine(Configuration.HeaderGeneratedFile);
            sb.AppendLine($"namespace {Configuration.Entity.ConstantNamespace}");
            sb.AppendLine("{");
            using (sb.Indent())
            {
                sb.AppendLine($"public partial class EntityNames");
                sb.AppendLine("{");
                using (sb.Indent())
                {
                    foreach (var entityType in entityTypes)
                    {
                        var entityName = Configuration.GetEntityNameOrDefault(entityType);

                        sb.AppendLine($"public const string {entityName} = \"{entityName}\";");
                    }
                }
                sb.AppendLine("}");
            }
            sb.AppendLine("}");

            return sb.ToString();
        }
    }
}
