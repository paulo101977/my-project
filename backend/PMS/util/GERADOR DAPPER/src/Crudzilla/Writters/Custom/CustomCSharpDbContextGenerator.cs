using Crudzilla.Configuration;
using JetBrains.Annotations;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Internal;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore.Metadata.Conventions.Internal;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Microsoft.EntityFrameworkCore.Scaffolding;
using Microsoft.EntityFrameworkCore.Scaffolding.Internal;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Crudzilla.Writters
{
    /// <summary>
    ///     Based on code: Microsoft.EntityFrameworkCore.Scaffolding.Internal.DbContextWriter
    ///     Available in: https://github.com/aspnet/EntityFrameworkCore
    /// </summary>
    internal class CustomCSharpDbContextGenerator : CSharpDbContextGenerator
    {
        private const string EntityLambdaIdentifier = "builder";
        private const string Language = "CSharp";

        private readonly ICSharpUtilities _cSharpUtilities;
        private readonly IScaffoldingProviderCodeGenerator _providerCodeGenerator;
        private readonly IAnnotationCodeGenerator _annotationCodeGenerator;
        private readonly CrudzillaConfig _configuration;
        private readonly OrmMapWriter _ormMapWriter;

        private IFileService _fileService { get; }
        private IndentedStringBuilder _sb;
        private bool _entityTypeBuilderInitialized;

        public CustomCSharpDbContextGenerator(
            [NotNull] IScaffoldingProviderCodeGenerator providerCodeGenerator,
            [NotNull] IAnnotationCodeGenerator annotationCodeGenerator,
            [NotNull] ICSharpUtilities cSharpUtilities,
            [NotNull] IFileService fileService,
            [NotNull] CrudzillaConfig configuration,
            [NotNull] OrmMapWriter ormMapWriter)
            : base(providerCodeGenerator, annotationCodeGenerator, cSharpUtilities)
        {
            Check.NotNull(providerCodeGenerator, nameof(providerCodeGenerator));
            Check.NotNull(annotationCodeGenerator, nameof(annotationCodeGenerator));
            Check.NotNull(cSharpUtilities, nameof(cSharpUtilities));
            Check.NotNull(fileService, nameof(fileService));
            Check.NotNull(configuration, nameof(configuration));
            Check.NotNull(ormMapWriter, nameof(ormMapWriter));

            _providerCodeGenerator = providerCodeGenerator;
            _annotationCodeGenerator = annotationCodeGenerator;
            _cSharpUtilities = cSharpUtilities;
            _fileService = fileService;
            _configuration = configuration;
            _ormMapWriter = ormMapWriter;
        }

        public override string WriteCode(
            IModel model,
            string @namespace,
            string contextName,
            string connectionString,
            bool useDataAnnotations)
        {
            Check.NotNull(model, nameof(model));

            _sb = new IndentedStringBuilder();

            _sb.AppendLine("using Microsoft.EntityFrameworkCore;");
            _sb.AppendLine("using Tnf.EntityFrameworkCore;");
            _sb.AppendLine($"using {_configuration.Entity.Namespace};");
            _sb.AppendLine($"using Tnf.Runtime.Session;");
            _sb.AppendLine();
            _sb.AppendLine($"namespace {_configuration.Orm.Namespace}");
            _sb.AppendLine("{");

            using (_sb.Indent())
            {
                GenerateClass(model, contextName, connectionString, useDataAnnotations);
            }

            _sb.AppendLine("}");

            return _sb.ToString();
        }

        private void GenerateClass(
            IModel model,
            string contextName,
            string connectionString,
            bool useDataAnnotations)
        {
            _sb.AppendLine($"public partial class {contextName} : TnfDbContext");
            _sb.AppendLine("{");
            using (_sb.Indent())
            {
                _sb.AppendLine($"public {contextName}(DbContextOptions<{contextName}> options, ITnfSession session)");
                using (_sb.Indent())
                {
                    _sb.AppendLine(": base(options, session)");
                }
                _sb.AppendLine("{");
                _sb.AppendLine("}");
            }

            _sb.AppendLine();

            using (_sb.Indent())
            {
                GenerateDbSets(model);
                GenerateEntityTypeErrors(model);
                //GenerateOnConfiguring(connectionString); -- disabled
                GenerateOnModelCreating(model, useDataAnnotations);
            }

            _sb.AppendLine("}");
        }

        private void GenerateDbSets(IModel model)
        {
            foreach (var entityType in model.GetEntityTypes())
            {
                var entityName = _configuration.GetEntityNameOrDefault(entityType);

                var pluralizedEntityName = _configuration.GetPluralizedEntityNameOrDefault(entityType);

                _sb.AppendLine($"public virtual DbSet<{entityName}> {pluralizedEntityName} {{ get; set; }}");
            }

            if (model.GetEntityTypes().Any())
                _sb.AppendLine();
        }

        private void GenerateEntityTypeErrors(IModel model)
        {
            var annotations = Microsoft.EntityFrameworkCore.Metadata.Internal.ScaffoldingMetadataExtensions.Scaffolding(model);

            foreach (var entityTypeError in annotations.EntityTypeErrors)
            {
                _sb.AppendLine($"// {entityTypeError.Value} Please see the warning messages.");
            }

            if (annotations.EntityTypeErrors.Any())
            {
                _sb.AppendLine();
            }
        }

        private void GenerateOnModelCreating(
            IModel model,
            bool useDataAnnotations)
        {
            foreach (var entityType in model.GetEntityTypes())
            {
                var sb = new IndentedStringBuilder();
                sb.IncrementIndent();
                sb.IncrementIndent();

                GenerateEntityType(entityType, useDataAnnotations, sb);

                var generatedCode = _ormMapWriter.WriteCode(entityType, sb.ToString());

                var mapperName = $"{_configuration.GetEntityNameOrDefault(entityType)}Mapper";
                var mapperFileName = $"{mapperName}.cs";
                var mapperFullName = Path.Combine(_configuration.Orm.GetMappersOutputPath(), mapperFileName);

                if (!File.Exists(mapperFullName) || _configuration.Orm.OverrideMappers)
                {
                    _fileService.OutputFile(
                        _configuration.Orm.GetMappersOutputPath(),
                        mapperFileName,
                        generatedCode);
                }

            }
        }

        private void GenerateEntityType(IEntityType entityType, bool useDataAnnotations, IndentedStringBuilder sb)
        {

            var annotations = entityType.GetAnnotations().ToList();
            // Force write table name
            //RemoveAnnotation(ref annotations, RelationalAnnotationNames.TableName); - Force write table name
            RemoveAnnotation(ref annotations, RelationalAnnotationNames.Schema);
            RemoveAnnotation(ref annotations, ScaffoldingAnnotationNames.DbSetName);

            if (!useDataAnnotations)
            {
                GenerateTableName(entityType, sb);
            }

            //var annotationsToRemove = new List<IAnnotation>();
            //var lines = new List<string>();

            //foreach (var annotation in annotations)
            //{
            //    if (_annotationCodeGenerator.IsHandledByConvention(entityType, annotation))
            //    {
            //        annotationsToRemove.Add(annotation);
            //    }
            //    else
            //    {
            //        var line = _annotationCodeGenerator.GenerateFluentApi(entityType, annotation, Language);

            //        if (line != null)
            //        {
            //            lines.Add(line);
            //            annotationsToRemove.Add(annotation);
            //        }
            //    }
            //}

            //lines.AddRange(GenerateAnnotations(annotations.Except(annotationsToRemove)));

            //AppendMultiLineFluentApi(lines, sb);

            //foreach (var index in entityType.GetIndexes())
            //{
            //    GenerateIndex(index, sb);
            //}

            foreach (var property in entityType.GetProperties())
            {
                GeneratePropertyKey(property, useDataAnnotations, sb);
                //GenerateProperty(property, useDataAnnotations, sb);
            }

            //GenerateKey(entityType.FindPrimaryKey(), useDataAnnotations, sb);

            //foreach (var key in entityType.GetKeys())
            //{
            //    var aa = key.DeclaringEntityType.DisplayName();
            //    var ab = key.GetAnnotations();
            //    var ac = key.GetType();
            //    var ad = key.Properties;
            //    var ae = key.Relational();
            //    var af = key.Relational().Name;
            //    var ag = key.GetAnnotations().ToList();
            //    var explicitName = key.Relational().Name != ConstraintNamer.GetDefaultName(key);
            //    var propertyName = _configuration.GetPropertyName(key.DeclaringEntityType, key);
            //}

            foreach (var foreignKey in entityType.GetForeignKeys())
            {
                GenerateRelationship(foreignKey, useDataAnnotations, sb);
            }

            foreach (var foreignKey in entityType.GetReferencingForeignKeys())
            {
                GetReferencingRelationship(foreignKey, useDataAnnotations, sb);
            }

        }

        private void AppendMultiLineFluentApi(IList<string> lines, IndentedStringBuilder sb)
        {
            if (lines.Count <= 0)
            {
                return;
            }

            using (sb.Indent())
            {
                sb.AppendLine();

                sb.Append(EntityLambdaIdentifier + lines[0]);

                using (sb.Indent())
                {
                    foreach (var line in lines.Skip(1))
                    {
                        sb.AppendLine();
                        sb.Append(line);
                    }
                }

                sb.AppendLine(";");
            }
        }

        private void GenerateKey(IKey key, bool useDataAnnotations, IndentedStringBuilder sb)
        {
            if (key == null)
                return;
            
            var annotations = key.GetAnnotations().ToList();
            var explicitName = key.Relational().Name != ConstraintNamer.GetDefaultName(key);
            RemoveAnnotation(ref annotations, RelationalAnnotationNames.Name);

            if (key.Properties.Count == 1)
            {

                if (key is Key concreteKey
                    && key.Properties.SequenceEqual(new KeyDiscoveryConvention().DiscoverKeyProperties(concreteKey.DeclaringEntityType, concreteKey.DeclaringEntityType.GetProperties().ToList())))
                {
                    return;
                }

                if (!explicitName
                    && useDataAnnotations)
                {
                    return;
                }

            }

            var lines = new List<string>
            {
                $".{nameof(EntityTypeBuilder.HasKey)}(e => {GenerateLambdaToKey(key.Properties, "e")})"
            };

            if (explicitName)
            {
                lines.Add($".{nameof(RelationalKeyBuilderExtensions.HasName)}({_cSharpUtilities.DelimitString(key.Relational().Name)})");
            }

            var annotationsToRemove = new List<IAnnotation>();

            foreach (var annotation in annotations)
            {
                if (_annotationCodeGenerator.IsHandledByConvention(key, annotation))
                {
                    annotationsToRemove.Add(annotation);
                }
                else
                {
                    var line = _annotationCodeGenerator.GenerateFluentApi(key, annotation, Language);

                    if (line != null)
                    {
                        lines.Add(line);
                        annotationsToRemove.Add(annotation);
                    }
                }
            }

            lines.AddRange(GenerateAnnotations(annotations.Except(annotationsToRemove)));

            AppendMultiLineFluentApi(lines, sb);
        }

        private void GenerateTableName(IEntityType entityType, IndentedStringBuilder sb)
        {
            var tableName = entityType.Relational().TableName;
            var schema = entityType.Relational().Schema;
            var defaultSchema = entityType.Model.Relational().DefaultSchema;

            var explicitSchema = schema != null && schema != defaultSchema;

            var parameterString = _cSharpUtilities.DelimitString(tableName);
            if (explicitSchema)
            {
                parameterString += ", " + _cSharpUtilities.DelimitString(schema);
            }

            using (sb.Indent())
            {
                sb.AppendLine();
                sb.AppendLine($"Table({parameterString});");
            }
            
        }

        private void GeneratePropertyKey(IProperty property, bool useDataAnnotations, IndentedStringBuilder sb)
        {
            if (property.IsPrimaryKey())
            {
                var propertyName = _configuration.GetPropertyName(property.DeclaringEntityType, property);
                var columnName = property.Relational().ColumnName;
                var columnType = property.DeclaringEntityType.GetEntityKeyType(_cSharpUtilities);

                var valueGenerated = property.ValueGenerated;
                if (((Property)property).GetValueGeneratedConfigurationSource().HasValue
                    && new RelationalValueGeneratorConvention().GetValueGenerated((Property)property) != valueGenerated)
                {
                    switch (valueGenerated)
                    {
                        case ValueGenerated.OnAdd:
                        case ValueGenerated.OnAddOrUpdate:
                            columnType = $".Key(KeyType.Identity)";
                            break;
                        case ValueGenerated.Never:
                            columnType = columnType == "Guid" ? $".Key(KeyType.Guid)" : $".Key(KeyType.Assigned)";
                            break;
                        default:
                            columnType = $".Key(KeyType.Assigned)";
                            break;
                    }
                }
                else
                    columnType = $".Key(KeyType.Assigned)";

                using (sb.Indent())
                {
                    sb.AppendLine();
                    sb.AppendLine($"Map(e => e.{propertyName}).Column({_cSharpUtilities.DelimitString(columnName)}){columnType};");
                }

            }
        }

        private void GenerateProperty(IProperty property, bool useDataAnnotations, IndentedStringBuilder sb)
        {
            var propertyName = _configuration.GetPropertyName(property.DeclaringEntityType, property);

            using (sb.Indent())
            {
                sb.AppendLine();
                sb.AppendLine($"Map(e => e.{propertyName}).Column();");
            }

            var lines = new List<string>
            {
                $".{nameof(EntityTypeBuilder.Property)}(e => e.{propertyName})"
            };

            var annotations = property.GetAnnotations().ToList();

            // Explicit name of column
            //RemoveAnnotation(ref annotations, RelationalAnnotationNames.ColumnName);-

            RemoveAnnotation(ref annotations, RelationalAnnotationNames.ColumnType);
            RemoveAnnotation(ref annotations, CoreAnnotationNames.MaxLengthAnnotation);
            RemoveAnnotation(ref annotations, CoreAnnotationNames.UnicodeAnnotation);
            RemoveAnnotation(ref annotations, RelationalAnnotationNames.DefaultValue);
            RemoveAnnotation(ref annotations, RelationalAnnotationNames.DefaultValueSql);
            RemoveAnnotation(ref annotations, RelationalAnnotationNames.ComputedColumnSql);
            RemoveAnnotation(ref annotations, ScaffoldingAnnotationNames.ColumnOrdinal);

            if (!useDataAnnotations)
            {
                if (!property.IsNullable
                    && property.ClrType.IsNullableType()
                    && !property.IsPrimaryKey())
                {
                    lines.Add($".{nameof(PropertyBuilder.IsRequired)}()");
                }

                var columnName = property.Relational().ColumnName;

                if (columnName != null
                    && columnName != property.Name)
                {
                    lines.Add($".{nameof(RelationalPropertyBuilderExtensions.HasColumnName)}({_cSharpUtilities.DelimitString(columnName)})");
                }

                var columnType = property.Relational().ColumnType;

                if (columnType != null)
                {
                    lines.Add($".{nameof(RelationalPropertyBuilderExtensions.HasColumnType)}({_cSharpUtilities.DelimitString(columnType)})");
                }

                var maxLength = property.GetMaxLength();

                if (maxLength.HasValue)
                {
                    lines.Add($".{nameof(PropertyBuilder.HasMaxLength)}({_cSharpUtilities.GenerateLiteral(maxLength.Value)})");
                }
            }

            if (property.IsUnicode() != null)
            {
                lines.Add($".{nameof(PropertyBuilder.IsUnicode)}({(property.IsUnicode() == false ? _cSharpUtilities.GenerateLiteral(false) : "")})");
            }

            if (property.Relational().DefaultValue != null)
            {
                lines.Add($".{nameof(RelationalPropertyBuilderExtensions.HasDefaultValue)}({_cSharpUtilities.GenerateLiteral((dynamic)property.Relational().DefaultValue)})");
            }

            if (property.Relational().DefaultValueSql != null)
            {
                lines.Add($".{nameof(RelationalPropertyBuilderExtensions.HasDefaultValueSql)}({_cSharpUtilities.DelimitString(property.Relational().DefaultValueSql)})");
            }

            if (property.Relational().ComputedColumnSql != null)
            {
                lines.Add($".{nameof(RelationalPropertyBuilderExtensions.HasComputedColumnSql)}({_cSharpUtilities.DelimitString(property.Relational().ComputedColumnSql)})");
            }

            var valueGenerated = property.ValueGenerated;
            var isRowVersion = false;
            if (((Property)property).GetValueGeneratedConfigurationSource().HasValue
                && new RelationalValueGeneratorConvention().GetValueGenerated((Property)property) != valueGenerated)
            {
                string methodName;
                switch (valueGenerated)
                {
                    case ValueGenerated.OnAdd:
                        methodName = nameof(PropertyBuilder.ValueGeneratedOnAdd);
                        break;

                    case ValueGenerated.OnAddOrUpdate:
                        isRowVersion = property.IsConcurrencyToken;
                        methodName = isRowVersion
                            ? nameof(PropertyBuilder.IsRowVersion)
                            : nameof(PropertyBuilder.ValueGeneratedOnAddOrUpdate);
                        break;

                    case ValueGenerated.Never:
                        methodName = nameof(PropertyBuilder.ValueGeneratedNever);
                        break;

                    default:
                        methodName = "";
                        break;
                }

                lines.Add($".{methodName}()");
            }

            if (property.IsConcurrencyToken
                && !isRowVersion)
            {
                lines.Add($".{nameof(PropertyBuilder.IsConcurrencyToken)}()");
            }

            var annotationsToRemove = new List<IAnnotation>();

            foreach (var annotation in annotations)
            {
                if (_annotationCodeGenerator.IsHandledByConvention(property, annotation))
                {
                    annotationsToRemove.Add(annotation);
                }
                else
                {
                    var line = _annotationCodeGenerator.GenerateFluentApi(property, annotation, Language);

                    if (line != null)
                    {
                        lines.Add(line);
                        annotationsToRemove.Add(annotation);
                    }
                }
            }

            lines.AddRange(GenerateAnnotations(annotations.Except(annotationsToRemove)));

            switch (lines.Count)
            {
                case 1:
                    return;
                case 2:
                    lines = new List<string>
                    {
                        lines[0] + lines[1]
                    };
                    break;
            }

            AppendMultiLineFluentApi(lines, sb);
        }
        

        private void GetReferencingRelationship(IForeignKey foreignKey, bool useDataAnnotations, IndentedStringBuilder sb)
        {
            var principalToDependentPropertyName = _configuration.GetNavigationPropertyName(foreignKey.PrincipalToDependent);

            using (sb.Indent())
            {
                sb.AppendLine();
                sb.AppendLine($"Map(x => x.{principalToDependentPropertyName}List).Ignore();");
            }
        }

        private void GenerateRelationship(IForeignKey foreignKey, bool useDataAnnotations, IndentedStringBuilder sb)
        {
            //var canUseDataAnnotations = true;
            var annotations = foreignKey.GetAnnotations().ToList();

            var dependentToPrincipalPropertyName = _configuration.GetNavigationPropertyName(foreignKey.DependentToPrincipal);
            var principalToDependentPropertyName = _configuration.GetNavigationPropertyName(foreignKey.PrincipalToDependent);

            using (sb.Indent())
            {
                sb.AppendLine();
                sb.AppendLine($"Map(x => x.{dependentToPrincipalPropertyName}).Ignore();");
            }

            //var lines = new List<string>
            //{
            //    /**
            //    $".{nameof(EntityTypeBuilder.HasOne)}(d => d.{foreignKey.DependentToPrincipal.Name})",
            //    $".{(foreignKey.IsUnique ? nameof(ReferenceNavigationBuilder.WithOne) : nameof(ReferenceNavigationBuilder.WithMany))}"
            //    + $"(p => p.{foreignKey.PrincipalToDependent.Name})"
            //    **/

            //    $".{nameof(EntityTypeBuilder.HasOne)}(d => d.{dependentToPrincipalPropertyName})",
            //    $".{(foreignKey.IsUnique ? nameof(ReferenceNavigationBuilder.WithOne) : nameof(ReferenceNavigationBuilder.WithMany))}"
            //    + $"(p => p.{principalToDependentPropertyName}{(foreignKey.IsUnique ? "" : "List")})"
            //};

            //if (!foreignKey.PrincipalKey.IsPrimaryKey())
            //{
            //    canUseDataAnnotations = false;
            //    lines.Add($".{nameof(ReferenceReferenceBuilder.HasPrincipalKey)}"
            //        + $"{(foreignKey.IsUnique ? $"<{foreignKey.PrincipalEntityType.DisplayName()}>" : "")}"
            //        + $"(p => {GenerateLambdaToKey(foreignKey.PrincipalKey.Properties, "p")})");
            //}

            //lines.Add($".{nameof(ReferenceReferenceBuilder.HasForeignKey)}"
            //          + $"{(foreignKey.IsUnique ? $"<{foreignKey.DeclaringEntityType.DisplayName()}>" : "")}"
            //          + $"(d => {GenerateLambdaToKey(foreignKey.Properties, "d")})");

            //var defaultOnDeleteAction = foreignKey.IsRequired
            //    ? DeleteBehavior.Cascade
            //    : DeleteBehavior.ClientSetNull;

            //if (foreignKey.DeleteBehavior != defaultOnDeleteAction)
            //{
            //    canUseDataAnnotations = false;
            //    lines.Add($".{nameof(ReferenceReferenceBuilder.OnDelete)}({_cSharpUtilities.GenerateLiteral(foreignKey.DeleteBehavior)})");
            //}

            //if (!string.IsNullOrEmpty((string)foreignKey[RelationalAnnotationNames.Name]))
            //{
            //    canUseDataAnnotations = false;
            //    lines.Add($".{nameof(RelationalReferenceReferenceBuilderExtensions.HasConstraintName)}({_cSharpUtilities.DelimitString(foreignKey.Relational().Name)})");
            //    RemoveAnnotation(ref annotations, RelationalAnnotationNames.Name);
            //}

            //var annotationsToRemove = new List<IAnnotation>();

            //foreach (var annotation in annotations)
            //{
            //    if (_annotationCodeGenerator.IsHandledByConvention(foreignKey, annotation))
            //    {
            //        annotationsToRemove.Add(annotation);
            //    }
            //    else
            //    {
            //        var line = _annotationCodeGenerator.GenerateFluentApi(foreignKey, annotation, Language);

            //        if (line != null)
            //        {
            //            canUseDataAnnotations = false;
            //            lines.Add(line);
            //            annotationsToRemove.Add(annotation);
            //        }
            //    }
            //}

            //lines.AddRange(GenerateAnnotations(annotations.Except(annotationsToRemove)));

            //if (!useDataAnnotations || !canUseDataAnnotations)
            //{
            //    AppendMultiLineFluentApi(lines, sb);
            //}
        }

        private void GenerateSequence(ISequence sequence, IndentedStringBuilder sb)
        {
            var methodName = nameof(RelationalModelBuilderExtensions.HasSequence);

            if (sequence.ClrType != Sequence.DefaultClrType)
            {
                methodName += $"<{_cSharpUtilities.GetTypeName(sequence.ClrType)}>";
            }

            var parameters = _cSharpUtilities.DelimitString(sequence.Name);

            if (string.IsNullOrEmpty(sequence.Schema)
                && sequence.Model.Relational().DefaultSchema != sequence.Schema)
            {
                parameters += $", {_cSharpUtilities.DelimitString(sequence.Schema)}";
            }

            var lines = new List<string>
            {
                $"modelBuilder.{methodName}({parameters})"
            };

            if (sequence.StartValue != Sequence.DefaultStartValue)
            {
                lines.Add($".{nameof(SequenceBuilder.StartsAt)}({sequence.StartValue})");
            }

            if (sequence.IncrementBy != Sequence.DefaultIncrementBy)
            {
                lines.Add($".{nameof(SequenceBuilder.IncrementsBy)}({sequence.IncrementBy})");
            }

            if (sequence.MinValue != Sequence.DefaultMinValue)
            {
                lines.Add($".{nameof(SequenceBuilder.HasMin)}({sequence.MinValue})");
            }

            if (sequence.MaxValue != Sequence.DefaultMaxValue)
            {
                lines.Add($".{nameof(SequenceBuilder.HasMax)}({sequence.MaxValue})");
            }

            if (sequence.IsCyclic != Sequence.DefaultIsCyclic)
            {
                lines.Add($".{nameof(SequenceBuilder.IsCyclic)}()");
            }

            if (lines.Count == 2)
            {
                lines = new List<string>
                {
                    lines[0] + lines[1]
                };
            }

            sb.AppendLine();
            sb.Append(lines[0]);

            using (sb.Indent())
            {
                foreach (var line in lines.Skip(1))
                {
                    sb.AppendLine();
                    sb.Append(line);
                }
            }

            sb.AppendLine(";");
        }

        private string GenerateLambdaToKey(
            IReadOnlyCollection<IProperty> properties,
            string lambdaIdentifier)
        {
            if (properties.Count <= 0)
                return string.Empty;

            var propertyNames = new List<string>();
            foreach (var property in properties)
            {
                var propertyName = _configuration.GetPropertyName(property.DeclaringEntityType, property);
                propertyNames.Add(propertyName);
            }

            return propertyNames.Count == 1
                ? $"{lambdaIdentifier}.{propertyNames[0]}"
                : $"new {{ {string.Join(", ", propertyNames.Select(p => lambdaIdentifier + "." + p))} }}";
        }

        private void RemoveAnnotation(ref List<IAnnotation> annotations, string annotationName)
        {
            annotations.Remove(annotations.SingleOrDefault(a => a.Name == annotationName));
        }

        private IList<string> GenerateAnnotations(IEnumerable<IAnnotation> annotations)
        {
            return annotations.Select(GenerateAnnotation).ToList();
        }

        private string GenerateAnnotation(IAnnotation annotation)
        {
            return $".HasAnnotation({_cSharpUtilities.DelimitString(annotation.Name)}, {_cSharpUtilities.GenerateLiteral((dynamic)annotation.Value)})";
        }
    }
}
