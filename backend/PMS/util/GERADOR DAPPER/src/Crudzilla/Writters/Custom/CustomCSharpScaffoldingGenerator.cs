using Crudzilla.Configuration;
using Crudzilla.Localization;
using JetBrains.Annotations;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Scaffolding.Internal;
using Microsoft.EntityFrameworkCore.Storage;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Crudzilla.Writters
{
    /// <summary>
    ///     Based on code: Microsoft.EntityFrameworkCore.Scaffolding.Internal.CSharpScaffoldingGenerator
    ///     Available in: https://github.com/aspnet/EntityFrameworkCore
    /// </summary>
    internal class CustomCSharpScaffoldingGenerator : CSharpScaffoldingGenerator
    {
        private readonly CrudzillaConfig _configuration;
        private readonly ICSharpUtilities _cSharpUtilities;
        private readonly DataTransferWriter _dataTransferWriter;
        private readonly ConstantsWriter _constantsWriter;
        private readonly AutoMapperProfileWriter _autoMapperWriter;
        private readonly AdapterWriter _adapterWriter;
        private readonly BuilderWriter _builderWriter;

        private readonly IRelationalTypeMapper _relationalTypeMapper;
        private bool _generateNavigationProperties;

        public CustomCSharpScaffoldingGenerator(
            [NotNull] IFileService fileService,
            [NotNull] ICSharpDbContextGenerator cSharpDbContextGenerator,
            [NotNull] ICSharpEntityTypeGenerator cSharpEntityTypeGenerator,
            [NotNull] ICSharpUtilities cSharpUtilities,
            [NotNull] DataTransferWriter dataTransferWriter,
            [NotNull] ConstantsWriter constantsWriter,
            [NotNull] AutoMapperProfileWriter autoMapperWriter,
            [NotNull] IRelationalTypeMapper relationalTypeMapper,
            [NotNull] CrudzillaConfig scaffoldConfiguration,
            [NotNull] AdapterWriter adapterWriter,
            [NotNull] BuilderWriter builderWriter,
            bool generateNavigationProperties = true)
            : base(fileService, cSharpDbContextGenerator, cSharpEntityTypeGenerator)
        {
            _configuration = Check.NotNull(scaffoldConfiguration, nameof(scaffoldConfiguration));
            _cSharpUtilities = Check.NotNull(cSharpUtilities, nameof(cSharpUtilities));
            _dataTransferWriter = Check.NotNull(dataTransferWriter, nameof(dataTransferWriter));
            _autoMapperWriter = Check.NotNull(autoMapperWriter, nameof(autoMapperWriter));
            _adapterWriter = Check.NotNull(adapterWriter, nameof(adapterWriter));
            _builderWriter = Check.NotNull(builderWriter, nameof(builderWriter));
            _constantsWriter = Check.NotNull(constantsWriter, nameof(constantsWriter));
            _relationalTypeMapper = Check.NotNull(relationalTypeMapper, nameof(relationalTypeMapper));
            _generateNavigationProperties = generateNavigationProperties;
        }

        public override ReverseEngineerFiles WriteCode(
            IModel model,
            string outputPath,
            string @namespace,
            string contextName,
            string connectionString,
            bool useDataAnnotations)
        {
            Check.NotNull(model, nameof(model));
            Check.NotEmpty(outputPath, nameof(outputPath));
            Check.NotEmpty(@namespace, nameof(@namespace));
            Check.NotEmpty(contextName, nameof(contextName));
            Check.NotEmpty(connectionString, nameof(connectionString));

            ClearScaffoldFiles();

            var resultingFiles = new ReverseEngineerFiles();

            var entityTypes = model.GetEntityTypes();
            foreach (var entityType in entityTypes)
            {
                //// Entity
                GeneratedEntityTypeFile(@namespace, useDataAnnotations, resultingFiles, entityType);

                //// Builder
                GeneratedBuilderTypeFile(entityType);

                //// Localization Keys
                GenerateLocalizationKeysTypeFile(entityType);

                //// Data Transfer
                GenerateDataTransferTypeFile(entityType, _generateNavigationProperties);

                //// Create AutoMapper profile
                GenerateAutoMapperProfilesTypeFile(entityType);

                //// Adapter
                GenerateAdapterTypeFile(entityType);
            }

            //// Create EntityNames constant
            GenerateEntityNamesConstantTypeFile(entityTypes);

            //// Create DbContext
            var dbContextFileName = contextName + FileExtension;

            if (_configuration.Orm.OverrideDbContext || !File.Exists(Path.Combine(_configuration.Orm.OutputPath, dbContextFileName)))
            {
                var generatedCode = CSharpDbContextGenerator.WriteCode(model, @namespace, contextName, connectionString, useDataAnnotations);

                resultingFiles.ContextFile = FileService.OutputFile(
                    _configuration.Orm.OutputPath,
                    dbContextFileName,
                    generatedCode);
            }


            return resultingFiles;
        }


        private void GenerateAdapterTypeFile(IEntityType entityType)
        {
            // Invalid entity for dto
            if (!_configuration.IsValidEntity(entityType, _cSharpUtilities))
                return;

            var entityName = _configuration.GetEntityNameOrDefault(entityType);
            var generatedCode = _adapterWriter.WriteCodeScaffold(entityType);
            var adapterName = _configuration.GetAdapterName(entityType);

            FileService.OutputFile(
                _configuration.Adapter.GetScaffoldOutputPath(),
                $"{adapterName}{_configuration.DefaultFileExtension}",
                generatedCode);

            generatedCode = _adapterWriter.WriteCodeInterfaceScaffold();

            FileService.OutputFile(
                _configuration.Adapter.GetScaffoldOutputPath(),
                $"I{adapterName}{_configuration.DefaultFileExtension}",
                generatedCode);
        }

        private void GeneratedBuilderTypeFile(IEntityType entityType)
        {
            var entityName = _configuration.GetEntityNameOrDefault(entityType);
            var generatedCode = _builderWriter.WriteCodeScaffold(entityType);

            // Scaffold class
            FileService.OutputFile(
                _configuration.Builder.GetScaffoldOutputPath(),
                $"{_configuration.GetBuilderName(entityType)}.cs",
                generatedCode);
        }


        private void GenerateLocalizationKeysTypeFile(IEntityType entityType)
        {
            var enumerations = _configuration.GetEntityErrorsEnumeration(_relationalTypeMapper, entityType, _cSharpUtilities);
            if (enumerations.Count > 0)
            {
                if (!File.Exists(_configuration.Localization.FilePath))
                    throw new FileNotFoundException($"Localization source file not exit: {_configuration.Localization.FilePath}");

                var schema = JsonConvert.DeserializeObject<LocalizationSchema>(File.ReadAllText(_configuration.Localization.FilePath));

                foreach (var entityError in enumerations.EliminateDuplicates().Where(w => !schema.Texts.Any(a => a.Key == w.EnumerationName)).ToArray())
                {
                    var localizationValue = string.Empty;
                    if (entityError.SpecificationType == SpecificationTypes.OutOfBound)
                        localizationValue = $"O campo {entityError.Property} excede o tamanho limite de {entityError.Size}";

                    if (entityError.SpecificationType == SpecificationTypes.MustHave)
                        localizationValue = $"Informe um valor para o campo {entityError.Property}";

                    if (entityError.SpecificationType == SpecificationTypes.Invalid)
                        localizationValue = $"Informe uma valor valido para o campo {entityError.Property}";

                    schema.Texts.Add(entityError.EnumerationName, localizationValue);
                }

                var contentLocalizationFile = JsonConvert.SerializeObject(schema, Formatting.Indented);
                File.WriteAllText(_configuration.Localization.FilePath, contentLocalizationFile);
            }
        }


        private void GeneratedEntityTypeFile(string @namespace, bool useDataAnnotations, ReverseEngineerFiles resultingFiles, IEntityType entityType)
        {
            var csharpEntityTypeGenerator = CSharpEntityTypeGenerator as CustomCSharpEntityTypeGenerator;

            var generatedCode = csharpEntityTypeGenerator.WriteCode(entityType, @namespace, useDataAnnotations);

            // Scaffold entity
            var entityName = _configuration.GetEntityNameOrDefault(entityType);

            var entityNameFileName = $"{entityName}{FileExtension}";

            var entityTypeFileFullPath = FileService.OutputFile(
                _configuration.Entity.GetOutputPath(),
                entityNameFileName,
                generatedCode);

            resultingFiles.EntityTypeFiles.Add(entityTypeFileFullPath);
        }

        private void GenerateDataTransferTypeFile(IEntityType entityType, bool generateNavigationProperties)
        {
            // Invalid entity for dto
            if (!_configuration.IsValidEntity(entityType, _cSharpUtilities))
                return;

            var entityName = _configuration.GetEntityNameOrDefault(entityType);
            var dtoName = _configuration.GetDtoName(entityType);
            var generatedCode = _dataTransferWriter.WriteCodeScaffold(entityType, generateNavigationProperties);

            FileService.OutputFile(
                _configuration.DataTransfer.GetScaffoldOutputPath(),
                $"{dtoName}.cs",
                generatedCode);
            
            // Write request all dto
            generatedCode = _dataTransferWriter.WriteCodeRequestAllScaffold();

            FileService.OutputFile(
                _configuration.DataTransfer.GetScaffoldOutputPath(),
                $"GetAll{dtoName}{_configuration.DefaultFileExtension}",
                generatedCode);
        }

        private void GenerateEntityNamesConstantTypeFile(IEnumerable<IEntityType> entities)
        {
            // Create EntityNames constant
            var generatedCode = _constantsWriter.WriteCodeEntityName(entities);
            var entityNamesFileName = $"EntityNames{FileExtension}";
            FileService.OutputFile(
                _configuration.Entity.GetConstantFilePath(),
                entityNamesFileName,
                generatedCode);
        }

        private void GenerateAutoMapperProfilesTypeFile(IEntityType entityType)
        {
            if (!_configuration.IsValidEntity(entityType, _cSharpUtilities))
                return;

            var entityName = _configuration.GetEntityNameOrDefault(entityType);
            var dtoName = _configuration.GetDtoName(entityType);
            var generatedCode = _autoMapperWriter.WriteCodeScaffold(entityType);

            FileService.OutputFile(
                _configuration.AutoMapper.GetScaffoldOutputPath(),
                $"{entityName}Profile.cs",
                generatedCode);
        }

        private void ClearScaffoldFiles()
        {
            var filesToExclude = new List<string>();
            // Data Transfer
            var dtos = GetFilesFromPath(_configuration.DataTransfer.GetScaffoldOutputPath());
            filesToExclude.AddRange(dtos);

            // Entities
            var entitites = GetFilesFromPath(_configuration.Entity.GetOutputPath());
            filesToExclude.AddRange(entitites);

            // Delete files
            Parallel.ForEach(filesToExclude, file => File.Delete(file));
        }

        private IEnumerable<string> GetFilesFromPath(string directory)
        {
            if (!Directory.Exists(directory))
                return new List<string>();

            var files = Directory.EnumerateFiles(directory, "*.cs");
            return files;
        }
    }
}
