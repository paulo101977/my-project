using Crudzilla.Configuration;
using JetBrains.Annotations;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Metadata.Conventions.Internal;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Microsoft.EntityFrameworkCore.Scaffolding.Internal;
using Microsoft.EntityFrameworkCore.Storage;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace Crudzilla.Writters
{
    /// <summary>
    ///     Based on code: Microsoft.EntityFrameworkCore.Scaffolding.Internal.CSharpEntityTypeGenerator
    ///     Available in: https://github.com/aspnet/EntityFrameworkCore
    /// </summary>
    internal class CustomCSharpEntityTypeGenerator : CSharpEntityTypeGenerator
    {
        private IFileService FileService { get; }
        private ICSharpUtilities CSharpUtilities { get; }
        private IRelationalTypeMapper RelationalTypeMapper { get; }
        private CrudzillaConfig Configuration { get; }

        private bool _useDataAnnotations;

        private IndentedStringBuilder _sb;

        private string DefaultInheritance;
        private string EntityName;

        public CustomCSharpEntityTypeGenerator(
            [NotNull] ICSharpUtilities cSharpUtilities,
            [NotNull] IFileService fileService,
            [NotNull] CrudzillaConfig configuration,
            [NotNull] IRelationalTypeMapper relationalTypeMapper)
            : base(cSharpUtilities)
        {
            CSharpUtilities = Check.NotNull(cSharpUtilities, nameof(cSharpUtilities));
            FileService = Check.NotNull(fileService, nameof(fileService));
            Configuration = Check.NotNull(configuration, nameof(configuration));

            RelationalTypeMapper = Check.NotNull(relationalTypeMapper, nameof(relationalTypeMapper));
        }

        public override string WriteCode(
            IEntityType entityType,
            string @namespace,
            bool useDataAnnotations)
        {
            Check.NotNull(entityType, nameof(entityType));
            Check.NotNull(@namespace, nameof(@namespace));

            _sb = new IndentedStringBuilder();
            _useDataAnnotations = useDataAnnotations;

            //---
            DefaultInheritance = Configuration.GetEntityInheritance(entityType, CSharpUtilities);

            EntityName = Configuration.GetEntityNameOrDefault(entityType);
            //---

            _sb.AppendLine(Configuration.HeaderGeneratedFile);

            _sb.AppendLine("using System;");
            _sb.AppendLine("using System.Collections.Generic;");
            _sb.AppendLine("using System.Text;");
            _sb.AppendLine("using Tnf.Notifications;");
            
            if (_useDataAnnotations)
            {
                _sb.AppendLine("using System.ComponentModel.DataAnnotations;");
                _sb.AppendLine("using System.ComponentModel.DataAnnotations.Schema;");
            }

            _sb.AppendLine();
            _sb.AppendLine($"namespace {Configuration.Entity.Namespace}");
            _sb.AppendLine("{");
            using (_sb.Indent())
            {
                GenerateClass(entityType);
            }
            _sb.AppendLine("}");

            var code = _sb.ToString();
            return code;
        }

        private void GenerateClass(IEntityType entityType)
        {
            if (_useDataAnnotations)
            {
                GenerateEntityTypeDataAnnotations(entityType);
            }

            if (string.IsNullOrWhiteSpace(DefaultInheritance))
                _sb.AppendLine($"public partial class {EntityName}");
            else
                _sb.AppendLine($"public partial class {EntityName} : {DefaultInheritance}");

            _sb.AppendLine("{");

            using (_sb.Indent())
            {
                GenerateConstructor(entityType);
                GenerateProperties(entityType);
                GenerateNavigationProperties(entityType);
                GenerateErrorEnumerations(entityType);
                GenerateBuilderProperties();
            }

            _sb.AppendLine("}");
        }

        private void GenerateEntityTypeDataAnnotations(IEntityType entityType)
        {
            GenerateTableAttribute(entityType);
        }

        private void GenerateTableAttribute(IEntityType entityType)
        {
            var tableName = entityType.Relational().TableName;
            var schema = entityType.Relational().Schema;
            var defaultSchema = entityType.Model.Relational().DefaultSchema;

            var schemaParameterNeeded = schema != null && schema != defaultSchema;
            var tableAttributeNeeded = schemaParameterNeeded || tableName != null && tableName != entityType.Scaffolding().DbSetName;

            if (tableAttributeNeeded)
            {
                var tableAttribute = new AttributeWriter(nameof(TableAttribute));

                tableAttribute.AddParameter(CSharpUtilities.DelimitString(tableName));

                if (schemaParameterNeeded)
                {
                    tableAttribute.AddParameter($"{nameof(TableAttribute.Schema)} = {CSharpUtilities.DelimitString(schema)}");
                }

                _sb.AppendLine(tableAttribute.ToString());
            }
        }

        private void GenerateConstructor(IEntityType entityType)
        {
            var collectionNavigations = entityType.GetNavigations().Where(n => n.IsCollection()).ToList();

            if (collectionNavigations.Count > 0)
            {
                _sb.AppendLine($"public {EntityName}()");
                _sb.AppendLine("{");

                using (_sb.Indent())
                {
                    foreach (var navigation in collectionNavigations)
                    {
                        var propertyName = Configuration.GetNavigationPropertyName(navigation);
                        var propertyType = Configuration.GetNavigationPropertyType(navigation);

                        _sb.AppendLine($"{propertyName}List = new HashSet<{propertyType}>();");
                    }
                }

                _sb.AppendLine("}");
                _sb.AppendLine();
            }
        }

        private void GenerateProperties(IEntityType entityType)
        {
            foreach (var property in entityType
                .GetProperties()
                .OrderBy(p => p.Scaffolding().ColumnOrdinal))
            {
                if (_useDataAnnotations)
                {
                    GeneratePropertyDataAnnotations(property);
                }

                var propertyName = Configuration.GetPropertyName(entityType, property);

                if (Configuration.IsFullAuditedProperty(CSharpUtilities, property) ||
                    Configuration.IsFullTenantProperty(CSharpUtilities, property))
                    continue;

                var accessMethod = propertyName == "Id" || propertyName == $"{EntityName}Id" ? string.Empty : "internal";
                _sb.AppendLine($"public {CSharpUtilities.GetTypeName(property.ClrType)} {propertyName} {{ get; {accessMethod} set; }}");
            }
        }

        private void GeneratePropertyDataAnnotations(IProperty property)
        {
            GenerateKeyAttribute(property);
            GenerateRequiredAttribute(property);
            GenerateColumnAttribute(property);
            GenerateMaxLengthAttribute(property);
        }

        private void GenerateKeyAttribute(IProperty property)
        {
            var key = property.AsProperty().PrimaryKey;

            if (key?.Properties.Count == 1)
            {
                if (key is Key concreteKey
                    && key.Properties.SequenceEqual(new KeyDiscoveryConvention().DiscoverKeyProperties(concreteKey.DeclaringEntityType, concreteKey.DeclaringEntityType.GetProperties().ToList())))
                {
                    return;
                }

                if (key.Relational().Name != ConstraintNamer.GetDefaultName(key))
                {
                    return;
                }

                _sb.AppendLine(new AttributeWriter(nameof(KeyAttribute)));
            }
        }

        private void GenerateColumnAttribute(IProperty property)
        {
            var columnName = property.Relational().ColumnName;
            var columnType = property.Relational().ColumnType;

            var delimitedColumnName = columnName != null && columnName != property.Name ? CSharpUtilities.DelimitString(columnName) : null;
            var delimitedColumnType = columnType != null ? CSharpUtilities.DelimitString(columnType) : null;

            if ((delimitedColumnName ?? delimitedColumnType) != null)
            {
                var columnAttribute = new AttributeWriter(nameof(ColumnAttribute));

                if (delimitedColumnName != null)
                {
                    columnAttribute.AddParameter(delimitedColumnName);
                }

                if (delimitedColumnType != null)
                {
                    columnAttribute.AddParameter($"{nameof(ColumnAttribute.TypeName)} = {delimitedColumnType}");
                }

                _sb.AppendLine(columnAttribute);
            }
        }

        private void GenerateMaxLengthAttribute(IProperty property)
        {
            var maxLength = property.GetMaxLength();

            if (maxLength.HasValue)
            {
                var lengthAttribute = new AttributeWriter(
                    property.ClrType == typeof(string)
                        ? nameof(StringLengthAttribute)
                        : nameof(MaxLengthAttribute));

                lengthAttribute.AddParameter(CSharpUtilities.GenerateLiteral(maxLength.Value));

                _sb.AppendLine(lengthAttribute.ToString());
            }
        }

        private void GenerateRequiredAttribute(IProperty property)
        {
            if (!property.IsNullable
                && property.ClrType.IsNullableType()
                && !property.IsPrimaryKey())
            {
                _sb.AppendLine(new AttributeWriter(nameof(RequiredAttribute)).ToString());
            }
        }

        private void GenerateNavigationProperties(IEntityType entityType)
        {
            var sortedNavigations = entityType.GetNavigations()
                .OrderBy(n => n.IsDependentToPrincipal() ? 0 : 1)
                .ThenBy(n => n.IsCollection() ? 1 : 0);

            if (sortedNavigations.Any())
            {
                _sb.AppendLine();

                foreach (var navigation in sortedNavigations)
                {
                    if (_useDataAnnotations)
                    {
                        GenerateNavigationDataAnnotations(navigation);
                    }

                    var propertyName = Configuration.GetNavigationPropertyName(navigation);
                    var propertyType = Configuration.GetNavigationPropertyType(navigation);

                    propertyType = navigation.IsCollection() ? $"ICollection<{propertyType}>" : propertyType;
                    propertyName = navigation.IsCollection() ? $"{propertyName}List" : propertyName;

                    _sb.AppendLine($"public virtual {propertyType} {propertyName} {{ get; internal set; }}");
                }
            }
        }

        private void GenerateNavigationDataAnnotations(INavigation navigation)
        {
            GenerateForeignKeyAttribute(navigation);
            GenerateInversePropertyAttribute(navigation);
        }

        private void GenerateForeignKeyAttribute(INavigation navigation)
        {
            if (navigation.IsDependentToPrincipal() &&
                navigation.ForeignKey.PrincipalKey.IsPrimaryKey())
            {
                var foreignKeyAttribute = new AttributeWriter(nameof(ForeignKeyAttribute));

                foreignKeyAttribute.AddParameter(
                    CSharpUtilities.DelimitString(
                        string.Join(",", navigation.ForeignKey.Properties.Select(p => p.Name))));

                _sb.AppendLine(foreignKeyAttribute.ToString());
            }
        }

        private void GenerateInversePropertyAttribute(INavigation navigation)
        {
            if (navigation.ForeignKey.PrincipalKey.IsPrimaryKey())
            {
                var inverseNavigation = navigation.FindInverse();

                if (inverseNavigation != null)
                {
                    var inversePropertyAttribute = new AttributeWriter(nameof(InversePropertyAttribute));

                    inversePropertyAttribute.AddParameter(CSharpUtilities.DelimitString(inverseNavigation.Name));

                    _sb.AppendLine(inversePropertyAttribute.ToString());
                }
            }
        }

        private void GenerateBuilderProperties()
        {
            _sb.AppendLine("");
            _sb.AppendLine("public static Builder Create(INotificationHandler handler)");

            using (_sb.Indent())
            {
                _sb.AppendLine("=> new Builder(handler);");
            }
            _sb.AppendLine("");

             _sb.AppendLine($"public static Builder Create(INotificationHandler handler, {EntityName} instance)");

            using (_sb.Indent())
            {
                _sb.AppendLine("=> new Builder(handler, instance);");
            }
        }

        private class AttributeWriter
        {
            private readonly string _attibuteName;
            private readonly List<string> _parameters = new List<string>();

            public AttributeWriter([NotNull] string attributeName)
            {
                Check.NotEmpty(attributeName, nameof(attributeName));

                _attibuteName = attributeName;
            }

            public void AddParameter([NotNull] string parameter)
            {
                Check.NotEmpty(parameter, nameof(parameter));

                _parameters.Add(parameter);
            }

            public override string ToString()
                => "[" + (_parameters.Count == 0
                       ? StripAttribute(_attibuteName)
                       : StripAttribute(_attibuteName) + "(" + string.Join(", ", _parameters) + ")") + "]";

            private static string StripAttribute([NotNull] string attributeName)
                => attributeName.EndsWith("Attribute", StringComparison.Ordinal)
                    ? attributeName.Substring(0, attributeName.Length - 9)
                    : attributeName;
        }

        // Customized
        private void GenerateErrorEnumerations(IEntityType entityType)
        {
            var enumerations = Configuration.GetEntityErrorsEnumeration(RelationalTypeMapper, entityType, CSharpUtilities);

            if (enumerations.Count > 0)
            {
                _sb.AppendLine();
                _sb.AppendLine("public enum EntityError");
                _sb.AppendLine("{");

                using (_sb.Indent())
                {
                    var last = enumerations.Last();
                    foreach (var item in enumerations)
                    {


                        if (last == item)
                            _sb.AppendLine(item.EnumerationName);
                        else
                            _sb.AppendLine($"{item.EnumerationName},");
                    }
                }

                _sb.AppendLine("}");
            }
        }

    }
}

