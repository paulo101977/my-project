﻿using Microsoft.EntityFrameworkCore.Design.Internal;
using System;

namespace Crudzilla.Writters
{
    internal class CustomOperationReportHandler : IOperationReporter
    {
        public void WriteError(string message)
            => Console.WriteLine(message);

        public void WriteInformation(string message)
            => Console.WriteLine(message);

        public void WriteVerbose(string message)
            => Console.WriteLine(message);

        public void WriteWarning(string message)
            => Console.WriteLine(message);
    }
}
