﻿using Crudzilla.Configuration;
using JetBrains.Annotations;
using Microsoft.EntityFrameworkCore.Internal;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Scaffolding.Internal;
using System.Collections.Generic;
using System.Linq;

namespace Crudzilla.Writters
{
    internal class AutoMapperProfileWriter
    {
        private CrudzillaConfig Configuration { get; }
        private ICSharpUtilities CSharpUtilities { get; }
        private IndentedStringBuilder _sb;

        private string DefaultInheritance;
        private string DtoName;
        private string EntityKey;
        private string EntityName;

        public AutoMapperProfileWriter(
            [NotNull] CrudzillaConfig configuration,
            [NotNull] ICSharpUtilities csharpUtilities)
        {
            Configuration = Check.NotNull(configuration, nameof(configuration));
            CSharpUtilities = Check.NotNull(csharpUtilities, nameof(csharpUtilities));
        }

        public string WriteCodeScaffold([NotNull] IEntityType entityType)
        {
            Check.NotNull(entityType, nameof(entityType));
            //---
            DefaultInheritance = Configuration.GetDataTransferInheritance(entityType, CSharpUtilities);
            DtoName = Configuration.GetDtoName(entityType);
            EntityKey = entityType.GetEntityKeyType(CSharpUtilities);
            EntityName = Configuration.GetEntityNameOrDefault(entityType);
            //---

            _sb = new IndentedStringBuilder();

            _sb.AppendLine(Configuration.HeaderGeneratedFile);

            _sb.AppendLine($"using AutoMapper;");
            _sb.AppendLine($"using {Configuration.Entity.Namespace};");
            _sb.AppendLine($"using {Configuration.DataTransfer.Namespace};");

            _sb.AppendLine();
            _sb.AppendLine($"namespace {Configuration.AutoMapper.Namespace}");
            _sb.AppendLine("{");
            using (_sb.Indent())
            {
                GenerateClass(entityType);
            }
            _sb.AppendLine("}");

            return _sb.ToString();
        }


        public void GenerateClass([NotNull] IEntityType entityType)
        {
            _sb.AppendLine($"public class {EntityName}Profile : Profile");
            _sb.AppendLine("{");
            using (_sb.Indent())
            {
                GenerateConstructor(entityType);
            }
            _sb.AppendLine("}");
        }

        public void GenerateConstructor([NotNull] IEntityType entityType)
        {
            _sb.AppendLine($"public {EntityName}Profile()");
            _sb.AppendLine("{");
            using (_sb.Indent())
            {
                _sb.AppendLine($"CreateMap<{EntityName}, {DtoName}>();");
            }
            _sb.AppendLine("}");
            _sb.AppendLine();
        }

    }
}
