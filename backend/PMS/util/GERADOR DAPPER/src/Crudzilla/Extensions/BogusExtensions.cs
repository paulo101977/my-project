﻿using JetBrains.Annotations;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Storage;
using System;

namespace Crudzilla
{
    internal static class BogusExtensions
    {
        internal static string GetLiteralTypeRandomValue([NotNull]this Type type, [NotNull] IRelationalTypeMapper relationalTypeMapper, IProperty property = null, bool invalid = false)
        {
            Check.NotNull(type, nameof(type));
            Check.NotNull(relationalTypeMapper, nameof(relationalTypeMapper));

            var random = new Bogus.Randomizer();
            var date = new Bogus.DataSets.Date();
            var lorem = new Bogus.DataSets.Lorem();

            string result = "null";

            if (type == typeof(string))
            {
                var mapping = relationalTypeMapper.FindMapping(property);

                if (mapping.Size.HasValue)
                {
                    var size = mapping.Size.Value;
                    if (invalid)
                        size += 1;

                    result = $"\"{lorem.Sentence(size).Substring(0, size)}\"";
                }
                else
                {
                    // varchar(max)
                    result = $"\"{lorem.Sentence(100).Substring(0, 100)}\"";
                }
            }

            if (type == typeof(int))
                result = random.Number(100, int.MaxValue).ToString();

            if (type == typeof(decimal))
                result = $"new decimal({random.Decimal().ToString().Replace(",", ".")})";

            if (type == typeof(double))
                result = random.Double(100, double.MaxValue).ToString();

            if (type == typeof(float))
                result = random.Float(100, float.MaxValue).ToString();

            if (type == typeof(long))
                result = random.Long(100, long.MaxValue).ToString();

            if (type == typeof(byte))
                result = random.Byte(100, byte.MaxValue).ToString();

            if (type == typeof(short))
                result = random.Short(100, short.MaxValue).ToString();

            if (type == typeof(bool) || type == typeof(bool?))
            {
                if (property.Name == "IsDeleted")
                    result = "false";
                else
                    result = random.Bool().ToString().ToCamelCase();
            }

            if (type == typeof(Guid))
            {
                result = "Guid.NewGuid()";
                if (invalid)
                    result = "Guid.Empty";
            }

            if (type == typeof(DateTime) ||
                type == typeof(DateTime?))
            {
                var futureDate = date.Future();
                if (invalid)
                    futureDate = DateTime.MinValue;

                result = $"new DateTime({futureDate.Year}, {futureDate.Month}, {futureDate.Day}, {futureDate.Hour}, {futureDate.Minute}, {futureDate.Second})";
            }

            return result;
        }
    }
}
