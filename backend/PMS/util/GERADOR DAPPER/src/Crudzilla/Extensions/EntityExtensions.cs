﻿using Crudzilla.Configuration;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Microsoft.EntityFrameworkCore.Scaffolding.Internal;
using Microsoft.EntityFrameworkCore.Storage;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace Crudzilla
{
    [DebuggerStepThrough]
    internal static class EntityExtensions
    {
        internal static List<string> FullAuditedProperties { get; } = new List<string>()
        {
            "IsDeleted",
            "CreationTime",
            "CreatorUserId",
            "LastModificationTime",
            "LastModifierUserId",
            "DeletionTime",
            "DeleterUserId"
        };

        internal static List<string> FullTenantAuditedProperties { get; } = new List<string>()
        {
            "TenantId"
        };
        internal static string GetEntityKeyType(this IEntityType entityType, ICSharpUtilities csharpUtilities)
        {
            Check.NotNull(entityType, nameof(entityType));
            Check.NotNull(csharpUtilities, nameof(csharpUtilities));

            var key = entityType.GetKeys().FirstOrDefault();
            if (key == null)
                return string.Empty;

            // Multiples keys
            if (key.Properties.Count > 1)
                return "int";

            var keyType = csharpUtilities.GetTypeName(key.Properties.First().ClrType);

            return keyType;
        }

        internal static string GetEntityInheritance(this CrudzillaConfig config, IEntityType entityType, ICSharpUtilities csharpUtilities)
        {
            Check.NotNull(config, nameof(config));
            Check.NotNull(entityType, nameof(entityType));
            Check.NotNull(csharpUtilities, nameof(csharpUtilities));

            var key = entityType.GetKeys().FirstOrDefault();

            var keyCount = key.Properties.Count();

            // Wihtout key
            if (keyCount == 0)
                return string.Empty;

            string keyType = "";
            if (keyCount == 1)
            {
                keyType = csharpUtilities.GetTypeName(key.Properties.First().ClrType);
            }
            else
            {
                keyType = "int";
            }

            var tnfDefaultInheritance = $"IEntity{keyType.UppercaseFirst()}";

            var propertyNames = new List<string>();

            foreach (var property in entityType
                .GetProperties()
                .OrderBy(p => ScaffoldingMetadataExtensions.Scaffolding(p).ColumnOrdinal))
            {
                var propertyName = config.GetPropertyName(entityType, property);
                if (propertyName == "Id")
                    continue;

                propertyNames.Add(propertyName);
            }


            if (FullTenantAuditedProperties.Intersect(propertyNames).Count() == 1)
                tnfDefaultInheritance = $"ThexMultiTenantFullAuditedEntity, {tnfDefaultInheritance} ";
            else if (FullAuditedProperties.Intersect(propertyNames).Count() == 7)
                tnfDefaultInheritance = $"ThexFullAuditedEntity, {tnfDefaultInheritance} ";
            
            return tnfDefaultInheritance;
        }

        internal static string GetEntityNameOrDefault(this CrudzillaConfig config, IEntityType entityType)
        {
            Check.NotNull(config, nameof(config));
            Check.NotNull(entityType, nameof(entityType));

            var entityName = config.GetEntityName(entityType.Name);
            if (string.IsNullOrWhiteSpace(entityName))
                entityName = entityType.Name;

            return entityName;
        }

        private static string GetEntityName(this CrudzillaConfig config, string tableName)
        {
            Check.NotNull(config, nameof(config));

            var map = config
                .TablesSelection
                .FirstOrDefault(w => w.Name.NormalizeString() == tableName.NormalizeString());

            var entityName = string.Empty;
            if (map != null)
                entityName = map.Alias;

            return entityName;
        }

        internal static string GetPluralizedEntityNameOrDefault(this CrudzillaConfig config, IEntityType entityType)
        {
            Check.NotNull(config, nameof(config));
            Check.NotNull(entityType, nameof(entityType));

            var map = config
                .TablesSelection
                .FirstOrDefault(w => w.Name.NormalizeString() == entityType.Name.NormalizeString());

            var pluralizedEntityName = entityType.Scaffolding().DbSetName;
            if (map != null)
                pluralizedEntityName = string.IsNullOrWhiteSpace(map.PluralizedAlias) ? map.Alias : map.PluralizedAlias;

            return pluralizedEntityName;
        }

        internal static string GetWebApiRouteName(this CrudzillaConfig config, IEntityType entityType)
        {
            Check.NotNull(config, nameof(config));
            Check.NotNull(entityType, nameof(entityType));

            var map = config
                .TablesSelection
                .FirstOrDefault(w => w.Name.NormalizeString() == entityType.Name.NormalizeString());

            var webApiRouteName = entityType.Name;
            if (map != null)
                webApiRouteName = string.IsNullOrWhiteSpace(map.WebApiRouteName) ? map.PluralizedAlias : map.WebApiRouteName;

            return webApiRouteName.ToLowerInvariant();
        }

        internal static List<EntityError> GetEntityErrorsEnumeration(this CrudzillaConfig config, IRelationalTypeMapper relationalTypeMapper, IEntityType entityType, ICSharpUtilities cSharpUtilities)
        {
            Check.NotNull(config, nameof(config));
            Check.NotNull(relationalTypeMapper, nameof(relationalTypeMapper));
            Check.NotNull(entityType, nameof(entityType));
            Check.NotNull(cSharpUtilities, nameof(cSharpUtilities));

            var enumerations = new List<EntityError>();

            var entityName = config.GetEntityNameOrDefault(entityType);


            foreach (var property in entityType
                .GetProperties()
                .OrderBy(p => Microsoft.EntityFrameworkCore.Metadata.Internal.ScaffoldingMetadataExtensions.Scaffolding(p).ColumnOrdinal))
            {
                var propertyName = config.GetPropertyName(entityType, property);
                var mapping = relationalTypeMapper.FindMapping(property);

                if (property.ClrType == typeof(string))
                {
                    if (!property.IsColumnNullable())
                    {
                        var enumerationName = $"{entityName}{SpecificationTypes.MustHave}{propertyName}";
                        enumerations.Add(new EntityError(SpecificationTypes.MustHave, propertyName, enumerationName));
                    }

                    if (mapping.Size > 0)
                    {
                        var enumerationName = $"{entityName}{SpecificationTypes.OutOfBound}{propertyName}";
                        enumerations.Add(new EntityError(SpecificationTypes.OutOfBound, propertyName, enumerationName, mapping.Size.Value));
                    }
                }

                if (property.ClrType == typeof(DateTime) ||
                    property.ClrType == typeof(DateTime?))
                {
                    // Min datetime from sql server compatibility: 1753-01-01
                    var enumerationName = $"{entityName}{SpecificationTypes.Invalid}{propertyName}";
                    enumerations.Add(new EntityError(SpecificationTypes.Invalid, propertyName, enumerationName));
                }

                if (property.IsForeignKey() && !property.IsColumnNullable())
                {
                    var enumerationName = $"{entityName}{SpecificationTypes.MustHave}{propertyName}";
                    enumerations.Add(new EntityError(SpecificationTypes.MustHave, propertyName, enumerationName));
                }
            }

            // Remove properties when entity is FullAudited
            var isFullAuditedEntity = config.GetEntityInheritance(entityType, cSharpUtilities).Contains("ThexFullAuditedEntity");

            if (isFullAuditedEntity)
            {
                foreach (var item in enumerations.ToArray())
                {
                    if (FullAuditedProperties.Contains(item.Property))
                    {
                        enumerations = enumerations.Where(w => w.Property != item.Property).ToList();
                    }
                }
            }

            return enumerations;
        }

        internal static bool IsValidEntity(this CrudzillaConfig config, IEntityType entityType, ICSharpUtilities cSharpUtilities)
        {
            Check.NotNull(config, nameof(config));
            Check.NotNull(entityType, nameof(entityType));
            Check.NotNull(cSharpUtilities, nameof(cSharpUtilities));

            var defaultInheritance = config.GetEntityInheritance(entityType, cSharpUtilities);
            return !string.IsNullOrWhiteSpace(defaultInheritance);
        }
    }
}
