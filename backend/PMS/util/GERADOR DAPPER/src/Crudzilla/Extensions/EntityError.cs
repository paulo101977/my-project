﻿namespace Crudzilla
{
    internal class EntityError
    {
        protected bool Equals(EntityError other)
        {
            return SpecificationType == other.SpecificationType && string.Equals(Property, other.Property) && string.Equals(EnumerationName, other.EnumerationName) && Size == other.Size;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((EntityError)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = (int)SpecificationType;
                hashCode = (hashCode * 397) ^ (Property != null ? Property.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (EnumerationName != null ? EnumerationName.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ Size;
                return hashCode;
            }
        }

        internal EntityError(SpecificationTypes specificationType, string property, string enumName)
        {
            SpecificationType = specificationType;
            Property = property;
            EnumerationName = enumName;
        }

        internal EntityError(SpecificationTypes specificationType, string property, string enumName, int size)
        {
            SpecificationType = specificationType;
            Property = property;
            EnumerationName = enumName;
            Size = size;
        }

        public SpecificationTypes SpecificationType { get; set; }
        public string Property { get; set; }
        public string EnumerationName { get; set; }
        public int Size { get; set; }
    }
}
