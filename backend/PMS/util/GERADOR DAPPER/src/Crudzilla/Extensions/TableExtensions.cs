﻿using Crudzilla.Configuration;
using Microsoft.EntityFrameworkCore.Metadata;
using System.Collections.Generic;
using System.Linq;

namespace Crudzilla
{
    internal static class TableExtensions
    {
        internal static string GetTableName(this CrudzillaConfig config, IEntityType entityType)
        {
            Check.NotNull(config, nameof(config));
            Check.NotNull(entityType, nameof(entityType));

            return config.GetTableName(entityType.Name);
        }

        internal static string GetTableName(this CrudzillaConfig config, string entity)
        {
            var map = config
                .TablesSelection
                .FirstOrDefault(w => w.Name.NormalizeString() == entity.NormalizeString());

            return map?.Name;
        }

        internal static IEnumerable<string> GetTableNames(this CrudzillaConfig config)
        {
            var map = config
                .TablesSelection
                .Select(s => s.Name);

            return map;
        }
    }
}
