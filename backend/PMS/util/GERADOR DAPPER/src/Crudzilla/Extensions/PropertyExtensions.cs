﻿using Crudzilla.Configuration;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Microsoft.EntityFrameworkCore.Scaffolding.Internal;
using System.Linq;

namespace Crudzilla
{
    internal static class PropertyExtensions
    {
        internal static string GetPropertyName(this CrudzillaConfig config, IEntityType entityType, string propertyName, bool isId = true)
        {
            Check.NotNull(config, nameof(config));
            Check.NotNull(entityType, nameof(entityType));

            //var tablesCollectionNames = config.GetTableNames();
            var tableName = config.GetTableName(entityType);
            var aliasName = config.GetEntityNameOrDefault(entityType);

            if (isId)
            {
                if (string.IsNullOrWhiteSpace(tableName))
                {
                    if ($"{entityType.Name}Id" == propertyName)
                        return "Id";
                    if ($"Id{entityType.Name}" == propertyName)
                        return "Id";
                    return propertyName;
                }

                if ($"{tableName}Id" == propertyName ||
                    $"{aliasName}Id" == propertyName ||
                    $"Id{tableName}" == propertyName ||
                    $"Id{aliasName}" == propertyName ||
                    "Id" == propertyName)
                    return "Id";
            }

            // Recupera o prefixo da tabela como referencia:
            // Exemplo para nome das tabelas a seguir: 
            //  - Countries:
            //  - SYS009_PROFESSIONAL
            // retorna:
            //  - Countries
            //  - SYS009
            var tableKey = tableName.Split('_')[0].ToPascalCase();

            string[] tokens;

            // Se a propriedade tiver o mesmo prefixo na tabela
            // remove toda a referencia do nome na sua assinatura
            if (propertyName == tableKey)
            {
                tokens = tableName.Split('_');

                foreach (var token in tokens)
                {
                    propertyName = propertyName.Replace(token.ToPascalCase(), string.Empty);
                }
            }
            // Neste caso esta obtendo uma propriedade que não é da tabela atual
            // provavelmente é uma chave estrangeira
            // dessa forma procura entre todas as tabelas existentes no mapeamento para
            // remover sua referencia da assinatura
            else
            {
                if (isId)
                {

                    propertyName = propertyName.Replace(tableKey, string.Empty);

                    //foreach (var item in tablesCollectionNames.Where(w => w != tableName))
                    //{
                    //    tableKey = item.Split('_')[0].ToLowerInvariant().ToPascalCase();
                    //    propertyName = propertyName.Replace(tableKey, string.Empty);
                    //}
                }
            }

            if (string.IsNullOrWhiteSpace(propertyName))
                propertyName = tableKey;

            return propertyName;
        }

        internal static string GetPropertyName(this CrudzillaConfig config, IEntityType entityType, IProperty property)
        {
            Check.NotNull(config, nameof(config));
            Check.NotNull(entityType, nameof(entityType));
            Check.NotNull(property, nameof(property));

            var key = entityType.GetKeys().FirstOrDefault();
            if (key == null)
                return string.Empty;

            bool isId = false;
            // Single key
            if (key.Properties.Count == 1)
                isId = property.IsKey();

            return config.GetPropertyName(entityType, property.Name, isId);
        }

        internal static IOrderedEnumerable<IProperty> GetOrderedProperties(this CrudzillaConfig config, IEntityType entityType, ICSharpUtilities cSharpUtilities)
        {
            Check.NotNull(config, nameof(config));
            Check.NotNull(entityType, nameof(entityType));
            Check.NotNull(cSharpUtilities, nameof(cSharpUtilities));

            var properties = entityType.GetProperties().OrderBy(p => p.Scaffolding().ColumnOrdinal);

            var defaultInheritance = config.GetEntityInheritance(entityType, cSharpUtilities);

            var isFullAuditedEntity = defaultInheritance.Contains("ThexFullAuditedEntity");
            var isFullTenantProperty = defaultInheritance.Contains("ThexMultiTenantFullAuditedEntity");
            var isFullTenantNullableEntity = defaultInheritance.Contains("ThexMultiTenantNullableFullAuditedEntity");

            if (isFullAuditedEntity || isFullTenantProperty || isFullTenantNullableEntity)
            {
                foreach (var property in properties.ToArray())
                {
                    var propertyName = config.GetPropertyName(entityType, property);
                    if (EntityExtensions.FullAuditedProperties.Contains(propertyName) ||
                        EntityExtensions.FullTenantAuditedProperties.Contains(propertyName))
                    {
                        properties = properties.Where(w => w != property).OrderBy(p => p.Scaffolding().ColumnOrdinal);
                    }
                }
            }

            return properties;
        }

        internal static IOrderedEnumerable<IProperty> GetFullOrderedProperties(this CrudzillaConfig config, IEntityType entityType, ICSharpUtilities cSharpUtilities)
        {
            Check.NotNull(config, nameof(config));
            Check.NotNull(entityType, nameof(entityType));
            Check.NotNull(cSharpUtilities, nameof(cSharpUtilities));

            var properties = entityType.GetProperties().OrderBy(p => p.Scaffolding().ColumnOrdinal);

            var defaultInheritance = config.GetEntityInheritance(entityType, cSharpUtilities);

            return properties;
        }

        internal static bool IsFullAuditedProperty(this CrudzillaConfig config, ICSharpUtilities cSharpUtilities, IProperty property)
        {
            Check.NotNull(config, nameof(config));
            Check.NotNull(property, nameof(property));
            Check.NotNull(cSharpUtilities, nameof(cSharpUtilities));

            var defaultInheritance = config.GetEntityInheritance(property.DeclaringEntityType, cSharpUtilities);
            var isFullAuditedEntity = defaultInheritance.Contains("ThexFullAuditedEntity");
            var isFullTenantProperty = defaultInheritance.Contains("ThexMultiTenantFullAuditedEntity");
            var isFullTenantNullableEntity = defaultInheritance.Contains("ThexMultiTenantNullableFullAuditedEntity");
            if (isFullAuditedEntity || isFullTenantProperty || isFullTenantNullableEntity)
            {
                var propertyName = config.GetPropertyName(property.DeclaringEntityType, property);

                if (EntityExtensions.FullAuditedProperties.Any(a => a == propertyName))
                    return true;
            }

            return false;
        }

        internal static bool IsFullTenantProperty(this CrudzillaConfig config, ICSharpUtilities cSharpUtilities, IProperty property)
        {
            Check.NotNull(config, nameof(config));
            Check.NotNull(property, nameof(property));
            Check.NotNull(cSharpUtilities, nameof(cSharpUtilities));

            var defaultInheritance = config.GetEntityInheritance(property.DeclaringEntityType, cSharpUtilities);
            var isFullTenantProperty = defaultInheritance.Contains("ThexMultiTenantFullAuditedEntity");
            var isFullTenantNullableEntity = defaultInheritance.Contains("ThexMultiTenantNullableFullAuditedEntity");
            
            if (isFullTenantProperty || isFullTenantNullableEntity)
            {
                var propertyName = config.GetPropertyName(property.DeclaringEntityType, property);

                if (EntityExtensions.FullTenantAuditedProperties.Any(a => a == propertyName))
                    return true;
            }

            return false;
        }

        internal static string GetPropertyType(this CrudzillaConfig config, IEntityType entityType, INavigation navigation)
        {
            Check.NotNull(config, nameof(config));
            Check.NotNull(entityType, nameof(entityType));
            Check.NotNull(navigation, nameof(navigation));

            var tableName = navigation.Name;
            var entityAlias = config.GetEntityNameOrDefault(entityType);
            var tablesCollectionNames = config.GetTableNames();

            //if (tableName.Contains(entityAlias))
            //    return entityAlias;

            //foreach (var item in tablesCollectionNames)
            //{
            //    var tableKey = item.Split('_')[0].ToPascalCase();
            //    entityAlias = entityAlias.Replace(tableKey, string.Empty);
            //}

            //entityAlias = tableName.Replace(tableName, entityAlias);

            return entityAlias;
        }
    }
}
