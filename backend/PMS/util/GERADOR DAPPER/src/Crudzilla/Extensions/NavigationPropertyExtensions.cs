﻿using Crudzilla.Configuration;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using System.Text.RegularExpressions;

namespace Crudzilla
{
    internal static class NavigationPropertyExtensions
    {
        internal static string GetNavigationPropertyName(this CrudzillaConfig config, string entity, string name)
        {
            Check.NotNull(config, nameof(config));
            Check.NotNull(entity, nameof(entity));

            var tablesCollectionNames = config.GetTableNames();
            var tableName = config.GetTableName(entity);

            if (string.IsNullOrWhiteSpace(tableName))
                return entity;

            if (tableName == name)
                return tableName;

            var navigationPropertyName = name;
            foreach (var item in tablesCollectionNames)
            {
                var tableKey = item.Split('_')[0].ToLowerInvariant().ToPascalCase();
                navigationPropertyName = navigationPropertyName.Replace(tableKey, string.Empty);
            }

            return navigationPropertyName;
        }

        internal static string GetNavigationPropertyName(this CrudzillaConfig config, INavigation navigation)
        {
            Check.NotNull(config, nameof(config));
            Check.NotNull(navigation, nameof(navigation));

            var match = Regex.Match(navigation.Name, @"<([\w]+)>");
            var tableName = match.Success ? match.Groups[1].Value : navigation.Name;

            return config.GetNavigationPropertyName(navigation.Name, tableName);
        }

        internal static string GetNavigationPropertyType(this CrudzillaConfig config, INavigation navigation)
        {
            Check.NotNull(config, nameof(config));
            Check.NotNull(navigation, nameof(navigation));

            var entityType = navigation.IsDependentToPrincipal() ?
                navigation.ForeignKey.PrincipalToDependent.DeclaringEntityType :
                navigation.ForeignKey.DependentToPrincipal.DeclaringEntityType;

            var result = config.GetPropertyType(entityType, navigation);
            return result;
        }
    }
}
