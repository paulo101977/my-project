﻿using System;
using System.IO;
using System.Xml.Linq;

namespace Crudzilla.Configuration
{
    public class AdapterConfig
    {
        public AdapterConfig(string projectName)
        {
            ProjectName = projectName;
        }

        public string ProjectName { get; set; }

        internal string Namespace { get; set; }
        internal string OutputPath { get; set; }

        internal string GetOutputPath(string folder)
            => Path.Combine(OutputPath, folder);

        internal string GetScaffoldOutputPath()
            => Path.Combine(OutputPath);

        internal void ReadAttributes(FileInfo projectFileInfo)
        {
            if (projectFileInfo == null)
                throw new ArgumentNullException(nameof(projectFileInfo));

            var document = XDocument.Load(projectFileInfo.FullName);

            var rootNamespace = document
                .GetElement("Project")
                .GetElement("PropertyGroup")
                .GetElement("RootNamespace")?.Value;

            if (string.IsNullOrWhiteSpace(rootNamespace))
                rootNamespace = projectFileInfo.Name.Replace(".csproj", string.Empty);

            Namespace = $"{rootNamespace}.Adapters";
            OutputPath = Path.Combine(projectFileInfo.Directory.FullName, "Adapters");
        }
    }
}
