﻿using System;
using System.IO;
using System.Xml.Linq;

namespace Crudzilla.Configuration
{
    public class EntityConfig
    {
        public EntityConfig(string projectName)
        {
            ProjectName = projectName;
        }

        public string ProjectName { get; set; }

        internal string Namespace { get; set; }
        internal string OutputPath { get; set; }
        internal string ConstantNamespace { get; set; }
        internal string ConstantFilePath { get; set; }

        internal string GetConstantFilePath()
            => OutputPath;

        internal string GetOutputPath()
            => OutputPath;

        internal void ReadAttributes(FileInfo projectFileInfo)
        {
            if (projectFileInfo == null)
                throw new ArgumentNullException(nameof(projectFileInfo));

            var document = XDocument.Load(projectFileInfo.FullName);

            var rootNamespace = document
                .GetElement("Project")
                .GetElement("PropertyGroup")
                .GetElement("RootNamespace")?.Value;

            if (string.IsNullOrWhiteSpace(rootNamespace))
                rootNamespace = projectFileInfo.Name.Replace(".csproj", string.Empty);

            Namespace = $"{rootNamespace}.Entities";
            OutputPath = Path.Combine(projectFileInfo.Directory.FullName, "Entities");

            ConstantNamespace = rootNamespace;
            ConstantFilePath = projectFileInfo.Directory.FullName;
        }
    }
}
