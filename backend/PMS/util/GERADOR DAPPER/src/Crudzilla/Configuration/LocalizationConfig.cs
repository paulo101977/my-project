﻿using System;
using System.IO;
using System.Xml.Linq;

namespace Crudzilla.Configuration
{
    public class LocalizationConfig
    {
        public string ProjectName { get; set; }
        public string ConstantClassName { get; set; }
        public string FileName { get; set; }

        internal string Namespace { get; set; }
        internal string FilePath { get; set; }

        internal void ReadAttributes(FileInfo solutionFileInfo, FileInfo projectFileInfo)
        {
            if (solutionFileInfo == null)
                throw new ArgumentNullException(nameof(solutionFileInfo));

            if (projectFileInfo == null)
                throw new ArgumentNullException(nameof(projectFileInfo));

            var document = XDocument.Load(projectFileInfo.FullName);

            var rootNamespace = document
                .GetElement("Project")
                .GetElement("PropertyGroup")
                .GetElement("RootNamespace")?.Value;

            if (string.IsNullOrWhiteSpace(rootNamespace))
                rootNamespace = projectFileInfo.Name.Replace(".csproj", string.Empty);

            Namespace = rootNamespace;
            FilePath = solutionFileInfo.FindFile(FileName).FullName;
        }
    }
}
