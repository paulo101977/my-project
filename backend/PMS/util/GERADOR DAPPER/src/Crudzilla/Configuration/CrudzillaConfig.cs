﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Crudzilla.Configuration
{
    public class CrudzillaConfig
    {
        internal CrudzillaConfig()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("//------------------------------------------------------------------------------");
            sb.AppendLine("// <auto-generated>");
            sb.AppendLine("//     This code was generated by a Crudzilla tool.");
            sb.AppendLine("// </auto-generated>");
            sb.AppendLine("//------------------------------------------------------------------------------");
            HeaderGeneratedFile = sb.ToString();
        }

        public string ConnectionString { get; internal set; }
        public string SolutionPath { get; internal set; }
        public AutoMapperConfig AutoMapper { get; internal set; }
        public LocalizationConfig Localization { get; internal set; }
        public EntityConfig Entity { get; internal set; }
        public DataTransferConfig DataTransfer { get; internal set; }
        public AdapterConfig Adapter { get; internal set; }
        public BuilderConfig Builder { get; internal set; }
        public OrmConfig Orm { get; internal set; }
        public string HeaderGeneratedFile { get; internal set; }
        public string DefaultFileExtension { get; internal set; } = ".cs";
        public bool OverwriteFiles { get; internal set; } = true;
        public DatabaseType DatabaseType { get; internal set; } = DatabaseType.SqlServer;
        public List<TableMapConfig> TablesSelection { get; internal set; }

        internal void Initialize()
        {
            if (string.IsNullOrWhiteSpace(SolutionPath) || !File.Exists(SolutionPath))
                throw new ArgumentException("Invalid Solution Path");

            if (string.IsNullOrWhiteSpace(DataTransfer.ProjectName))
                throw new ArgumentException("Invalid Project Name from Data Transfer");

            if (string.IsNullOrWhiteSpace(Entity.ProjectName))
                throw new ArgumentException("Invalid Project Name from Entity");

            if (string.IsNullOrWhiteSpace(AutoMapper.ProjectName))
                throw new ArgumentException("Invalid Project Name from AutoMapper");

            if (string.IsNullOrWhiteSpace(Orm.ProjectName))
                throw new ArgumentException("Invalid Project Name from Orm");

            if (string.IsNullOrWhiteSpace(Adapter.ProjectName))
                throw new ArgumentException("Invalid Project Name from Adapter");

            if (string.IsNullOrWhiteSpace(Builder.ProjectName))
                throw new ArgumentException("Invalid Project Name from Builder");

            if (string.IsNullOrWhiteSpace(Localization.ProjectName))
                throw new ArgumentException("Invalid Project Name from Localization");

            var solutionFileInfo = new FileInfo(SolutionPath);

            DataTransfer.ProjectName = DataTransfer.ProjectName.FixProjectName();
            DataTransfer.ReadAttributes(solutionFileInfo.FindFile(DataTransfer.ProjectName));

            Entity.ProjectName = Entity.ProjectName.FixProjectName();
            Entity.ReadAttributes(solutionFileInfo.FindFile(Entity.ProjectName));

            AutoMapper.ProjectName = AutoMapper.ProjectName.FixProjectName();
            AutoMapper.ReadAttributes(solutionFileInfo.FindFile(AutoMapper.ProjectName));

            Orm.ProjectName = Orm.ProjectName.FixProjectName();
            Orm.ReadAttributes(solutionFileInfo.FindFile(Orm.ProjectName));

            Adapter.ProjectName = Adapter.ProjectName.FixProjectName();
            Adapter.ReadAttributes(solutionFileInfo.FindFile(Adapter.ProjectName));

            Builder.ProjectName = Builder.ProjectName.FixProjectName();
            Builder.ReadAttributes(solutionFileInfo.FindFile(Builder.ProjectName));

            Localization.ProjectName = Localization.ProjectName.FixProjectName();
            Localization.ReadAttributes(solutionFileInfo, solutionFileInfo.FindFile(Localization.ProjectName));
        }
    }

    public enum DatabaseType
    {
        SqlServer
    }
}
