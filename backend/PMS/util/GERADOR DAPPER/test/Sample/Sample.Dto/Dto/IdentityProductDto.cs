﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a Crudzilla tool.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using Tnf.Dto;

namespace Sample.Dto
{
    public partial class IdentityProductDto : BaseDto
    {
        public static IdentityProductDto NullInstance = null;

        public IdentityProductDto()
        {
            IdentityFeatureList = new HashSet<IdentityFeatureDto>();
            IdentityMenuFeatureList = new HashSet<IdentityMenuFeatureDto>();
            IdentityModuleList = new HashSet<IdentityModuleDto>();
            IdentityPermissionList = new HashSet<IdentityPermissionDto>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime CreationTime { get; set; }
        public Guid? CreatorUserId { get; set; }
        public DateTime? LastModificationTime { get; set; }
        public Guid? LastModifierUserId { get; set; }
        public DateTime? DeletionTime { get; set; }
        public Guid? DeleterUserId { get; set; }

        public virtual ICollection<IdentityFeatureDto> IdentityFeatureList { get; set; }
        public virtual ICollection<IdentityMenuFeatureDto> IdentityMenuFeatureList { get; set; }
        public virtual ICollection<IdentityModuleDto> IdentityModuleList { get; set; }
        public virtual ICollection<IdentityPermissionDto> IdentityPermissionList { get; set; }
    }
}
