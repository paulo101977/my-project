﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a Crudzilla tool.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using Tnf.Dto;

namespace Sample.Dto
{
    public partial class IdentityPermissionDto : BaseDto
    {
        public static IdentityPermissionDto NullInstance = null;

        public IdentityPermissionDto()
        {
            IdentityMenuFeatureIdentityPermissionList = new HashSet<IdentityMenuFeatureDto>();
            IdentityMenuFeatureIdentityPermissionParentList = new HashSet<IdentityMenuFeatureDto>();
            IdentityPermissionFeatureList = new HashSet<IdentityPermissionFeatureDto>();
            IdentityRolePermissionList = new HashSet<IdentityRolePermissionDto>();
        }

        public Guid Id { get; set; }
        public int IdentityProductId { get; set; }
        public int IdentityModuleId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime CreationTime { get; set; }
        public Guid? CreatorUserId { get; set; }
        public DateTime? LastModificationTime { get; set; }
        public Guid? LastModifierUserId { get; set; }
        public DateTime? DeletionTime { get; set; }
        public Guid? DeleterUserId { get; set; }

        public virtual IdentityModuleDto IdentityModule { get; set; }
        public virtual IdentityProductDto IdentityProduct { get; set; }
        public virtual ICollection<IdentityMenuFeatureDto> IdentityMenuFeatureIdentityPermissionList { get; set; }
        public virtual ICollection<IdentityMenuFeatureDto> IdentityMenuFeatureIdentityPermissionParentList { get; set; }
        public virtual ICollection<IdentityPermissionFeatureDto> IdentityPermissionFeatureList { get; set; }
        public virtual ICollection<IdentityRolePermissionDto> IdentityRolePermissionList { get; set; }
    }
}
