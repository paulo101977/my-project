﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a Crudzilla tool.
// </auto-generated>
//------------------------------------------------------------------------------

using DapperExtensions.Mapper;
using System;
using System.Collections.Generic;
using System.Text;
using Sample.Infra.Entities;

namespace Sample.Infra.Mappers.DapperMappers
{
    public sealed class UserClaimsMapper : ClassMapper<UserClaims>
    {
        public UserClaimsMapper()
        {
            
            Table("UserClaims");

            Map(e => e.Id).Column("Id").Key(KeyType.Guid);

            Map(x => x.User).Ignore();

        }
    }
}
