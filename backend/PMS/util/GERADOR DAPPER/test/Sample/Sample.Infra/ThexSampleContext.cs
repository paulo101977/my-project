﻿using Microsoft.EntityFrameworkCore;
using Tnf.EntityFrameworkCore;
using Sample.Infra.Entities;
using Tnf.Runtime.Session;

namespace Sample.Infra
{
    public partial class ThexSampleContext : TnfDbContext
    {
        public ThexSampleContext(DbContextOptions<ThexSampleContext> options, ITnfSession session)
            : base(options, session)
        {
        }

        public virtual DbSet<IdentityFeature> IdentityFeatures { get; set; }
        public virtual DbSet<IdentityMenuFeature> IdentityMenuFeatures { get; set; }
        public virtual DbSet<IdentityModule> IdentityModules { get; set; }
        public virtual DbSet<IdentityPermission> IdentityPermissions { get; set; }
        public virtual DbSet<IdentityPermissionFeature> IdentityPermissionFeatures { get; set; }
        public virtual DbSet<IdentityProduct> IdentityProducts { get; set; }
        public virtual DbSet<IdentityRolePermission> IdentityRolePermissions { get; set; }

    }
}
