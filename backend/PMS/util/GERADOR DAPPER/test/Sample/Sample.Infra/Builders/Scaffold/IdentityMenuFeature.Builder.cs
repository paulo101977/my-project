﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a Crudzilla tool.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using Tnf.Builder;
using Tnf.Notifications;
using Tnf.Specifications;
using Sample.Infra.Entities;
using Sample.Infra;
using Thex.Common;

namespace Sample.Infra.Entities
{
    public partial class IdentityMenuFeature
    {
        public class Builder : Builder<IdentityMenuFeature>
        {
            public Builder(INotificationHandler handler) : base(handler)
            {
            }
            public Builder(INotificationHandler handler, IdentityMenuFeature instance) : base(handler, instance)
            {
            }

            public virtual Builder WithId(Guid id)
            {
                Instance.Id = id;
                return this;
            }
            public virtual Builder WithIdentityProductId(int identityProductId)
            {
                Instance.IdentityProductId = identityProductId;
                return this;
            }
            public virtual Builder WithIdentityModuleId(int identityModuleId)
            {
                Instance.IdentityModuleId = identityModuleId;
                return this;
            }
            public virtual Builder WithIdentityPermissionId(Guid identityPermissionId)
            {
                Instance.IdentityPermissionId = identityPermissionId;
                return this;
            }
            public virtual Builder WithIdentityPermissionParentId(Guid? identityPermissionParentId)
            {
                Instance.IdentityPermissionParentId = identityPermissionParentId;
                return this;
            }
            public virtual Builder WithIdentityFeatureId(Guid identityFeatureId)
            {
                Instance.IdentityFeatureId = identityFeatureId;
                return this;
            }
            public virtual Builder WithIsActive(bool isActive)
            {
                Instance.IsActive = isActive;
                return this;
            }
            public virtual Builder WithOrderNumber(int orderNumber)
            {
                Instance.OrderNumber = orderNumber;
                return this;
            }
            public virtual Builder WithDescription(string description)
            {
                Instance.Description = description;
                return this;
            }
            public virtual Builder WithIsDeleted(bool isDeleted)
            {
                Instance.IsDeleted = isDeleted;
                return this;
            }
            public virtual Builder WithCreationTime(DateTime creationTime)
            {
                Instance.CreationTime = creationTime;
                return this;
            }
            public virtual Builder WithCreatorUserId(Guid? creatorUserId)
            {
                Instance.CreatorUserId = creatorUserId;
                return this;
            }
            public virtual Builder WithLastModificationTime(DateTime? lastModificationTime)
            {
                Instance.LastModificationTime = lastModificationTime;
                return this;
            }
            public virtual Builder WithLastModifierUserId(Guid? lastModifierUserId)
            {
                Instance.LastModifierUserId = lastModifierUserId;
                return this;
            }
            public virtual Builder WithDeletionTime(DateTime? deletionTime)
            {
                Instance.DeletionTime = deletionTime;
                return this;
            }
            public virtual Builder WithDeleterUserId(Guid? deleterUserId)
            {
                Instance.DeleterUserId = deleterUserId;
                return this;
            }

            protected override void Specifications()
            {
                AddSpecification(new ExpressionSpecification<IdentityMenuFeature>(
				AppConsts.LocalizationSourceName, 
				IdentityMenuFeature.EntityError.IdentityMenuFeatureMustHaveIdentityProductId, 
				w => w.IdentityProductId != default(int)));

                AddSpecification(new ExpressionSpecification<IdentityMenuFeature>(
				AppConsts.LocalizationSourceName, 
				IdentityMenuFeature.EntityError.IdentityMenuFeatureMustHaveIdentityModuleId, 
				w => w.IdentityModuleId != default(int)));

                AddSpecification(new ExpressionSpecification<IdentityMenuFeature>(
				AppConsts.LocalizationSourceName, 
				IdentityMenuFeature.EntityError.IdentityMenuFeatureMustHaveIdentityPermissionId, 
				w => w.IdentityPermissionId != default(Guid)));

                AddSpecification(new ExpressionSpecification<IdentityMenuFeature>(
				AppConsts.LocalizationSourceName, 
				IdentityMenuFeature.EntityError.IdentityMenuFeatureMustHaveIdentityFeatureId, 
				w => w.IdentityFeatureId != default(Guid)));

                AddSpecification(new ExpressionSpecification<IdentityMenuFeature>(
				AppConsts.LocalizationSourceName, 
				IdentityMenuFeature.EntityError.IdentityMenuFeatureOutOfBoundDescription, 
				w => string.IsNullOrWhiteSpace(w.Description) || w.Description.Length > 0 && w.Description.Length <= 400));

            }
        }
    }
}
