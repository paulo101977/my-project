﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a Crudzilla tool.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using Tnf.Builder;
using Tnf.Notifications;
using Tnf.Specifications;
using Sample.Infra.Entities;
using Sample.Infra;
using Thex.Common;

namespace Sample.Infra.Entities
{
    public partial class RoleClaims
    {
        public class Builder : Builder<RoleClaims>
        {
            public Builder(INotificationHandler handler) : base(handler)
            {
            }
            public Builder(INotificationHandler handler, RoleClaims instance) : base(handler, instance)
            {
            }

            public virtual Builder WithId(int id)
            {
                Instance.Id = id;
                return this;
            }
            public virtual Builder WithRoleId(Guid roleId)
            {
                Instance.RoleId = roleId;
                return this;
            }
            public virtual Builder WithClaimType(string claimType)
            {
                Instance.ClaimType = claimType;
                return this;
            }
            public virtual Builder WithClaimValue(string claimValue)
            {
                Instance.ClaimValue = claimValue;
                return this;
            }
            public virtual Builder WithIsDeleted(bool? isDeleted)
            {
                Instance.IsDeleted = isDeleted;
                return this;
            }
            public virtual Builder WithCreationTime(DateTime? creationTime)
            {
                Instance.CreationTime = creationTime;
                return this;
            }
            public virtual Builder WithCreatorUserId(Guid? creatorUserId)
            {
                Instance.CreatorUserId = creatorUserId;
                return this;
            }
            public virtual Builder WithLastModificationTime(DateTime? lastModificationTime)
            {
                Instance.LastModificationTime = lastModificationTime;
                return this;
            }
            public virtual Builder WithLastModifierUserId(Guid? lastModifierUserId)
            {
                Instance.LastModifierUserId = lastModifierUserId;
                return this;
            }
            public virtual Builder WithDeletionTime(DateTime? deletionTime)
            {
                Instance.DeletionTime = deletionTime;
                return this;
            }
            public virtual Builder WithDeleterUserId(Guid? deleterUserId)
            {
                Instance.DeleterUserId = deleterUserId;
                return this;
            }

            protected override void Specifications()
            {
                AddSpecification(new ExpressionSpecification<RoleClaims>(
				AppConsts.LocalizationSourceName, 
				RoleClaims.EntityError.RoleClaimsMustHaveRoleId, 
				w => w.RoleId != default(Guid)));

            }
        }
    }
}
