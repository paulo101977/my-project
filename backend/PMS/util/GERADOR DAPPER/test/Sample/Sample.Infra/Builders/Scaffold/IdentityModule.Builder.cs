﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a Crudzilla tool.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using Tnf.Builder;
using Tnf.Notifications;
using Tnf.Specifications;
using Sample.Infra.Entities;
using Sample.Infra;
using Thex.Common;

namespace Sample.Infra.Entities
{
    public partial class IdentityModule
    {
        public class Builder : Builder<IdentityModule>
        {
            public Builder(INotificationHandler handler) : base(handler)
            {
            }
            public Builder(INotificationHandler handler, IdentityModule instance) : base(handler, instance)
            {
            }

            public virtual Builder WithId(int id)
            {
                Instance.Id = id;
                return this;
            }
            public virtual Builder WithIdentityProductId(int identityProductId)
            {
                Instance.IdentityProductId = identityProductId;
                return this;
            }
            public virtual Builder WithName(string name)
            {
                Instance.Name = name;
                return this;
            }
            public virtual Builder WithDescription(string description)
            {
                Instance.Description = description;
                return this;
            }
            public virtual Builder WithIsDeleted(bool isDeleted)
            {
                Instance.IsDeleted = isDeleted;
                return this;
            }
            public virtual Builder WithCreationTime(DateTime creationTime)
            {
                Instance.CreationTime = creationTime;
                return this;
            }
            public virtual Builder WithCreatorUserId(Guid? creatorUserId)
            {
                Instance.CreatorUserId = creatorUserId;
                return this;
            }
            public virtual Builder WithLastModificationTime(DateTime? lastModificationTime)
            {
                Instance.LastModificationTime = lastModificationTime;
                return this;
            }
            public virtual Builder WithLastModifierUserId(Guid? lastModifierUserId)
            {
                Instance.LastModifierUserId = lastModifierUserId;
                return this;
            }
            public virtual Builder WithDeletionTime(DateTime? deletionTime)
            {
                Instance.DeletionTime = deletionTime;
                return this;
            }
            public virtual Builder WithDeleterUserId(Guid? deleterUserId)
            {
                Instance.DeleterUserId = deleterUserId;
                return this;
            }

            protected override void Specifications()
            {
                AddSpecification(new ExpressionSpecification<IdentityModule>(
				AppConsts.LocalizationSourceName, 
				IdentityModule.EntityError.IdentityModuleMustHaveIdentityProductId, 
				w => w.IdentityProductId != default(int)));

                AddSpecification(new ExpressionSpecification<IdentityModule>(
				AppConsts.LocalizationSourceName, 
				IdentityModule.EntityError.IdentityModuleMustHaveName, 
				w => !string.IsNullOrWhiteSpace(w.Name)));

                AddSpecification(new ExpressionSpecification<IdentityModule>(
				AppConsts.LocalizationSourceName, 
				IdentityModule.EntityError.IdentityModuleOutOfBoundName, 
				w => string.IsNullOrWhiteSpace(w.Name) || w.Name.Length > 0 && w.Name.Length <= 50));

                AddSpecification(new ExpressionSpecification<IdentityModule>(
				AppConsts.LocalizationSourceName, 
				IdentityModule.EntityError.IdentityModuleOutOfBoundDescription, 
				w => string.IsNullOrWhiteSpace(w.Description) || w.Description.Length > 0 && w.Description.Length <= 256));

            }
        }
    }
}
