﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a Crudzilla tool.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using Tnf.Builder;
using Tnf.Notifications;
using Tnf.Specifications;
using Sample.Infra.Entities;
using Sample.Infra;
using Thex.Common;

namespace Sample.Infra.Entities
{
    public partial class IdentityFeature
    {
        public class Builder : Builder<IdentityFeature>
        {
            public Builder(INotificationHandler handler) : base(handler)
            {
            }
            public Builder(INotificationHandler handler, IdentityFeature instance) : base(handler, instance)
            {
            }

            public virtual Builder WithId(Guid id)
            {
                Instance.Id = id;
                return this;
            }
            public virtual Builder WithIdentityProductId(int identityProductId)
            {
                Instance.IdentityProductId = identityProductId;
                return this;
            }
            public virtual Builder WithIdentityModuleId(int identityModuleId)
            {
                Instance.IdentityModuleId = identityModuleId;
                return this;
            }
            public virtual Builder WithKey(string key)
            {
                Instance.Key = key;
                return this;
            }
            public virtual Builder WithName(string name)
            {
                Instance.Name = name;
                return this;
            }
            public virtual Builder WithDescription(string description)
            {
                Instance.Description = description;
                return this;
            }
            public virtual Builder WithIsInternal(bool isInternal)
            {
                Instance.IsInternal = isInternal;
                return this;
            }
            public virtual Builder WithIsDeleted(bool isDeleted)
            {
                Instance.IsDeleted = isDeleted;
                return this;
            }
            public virtual Builder WithCreationTime(DateTime creationTime)
            {
                Instance.CreationTime = creationTime;
                return this;
            }
            public virtual Builder WithCreatorUserId(Guid? creatorUserId)
            {
                Instance.CreatorUserId = creatorUserId;
                return this;
            }
            public virtual Builder WithLastModificationTime(DateTime? lastModificationTime)
            {
                Instance.LastModificationTime = lastModificationTime;
                return this;
            }
            public virtual Builder WithLastModifierUserId(Guid? lastModifierUserId)
            {
                Instance.LastModifierUserId = lastModifierUserId;
                return this;
            }
            public virtual Builder WithDeletionTime(DateTime? deletionTime)
            {
                Instance.DeletionTime = deletionTime;
                return this;
            }
            public virtual Builder WithDeleterUserId(Guid? deleterUserId)
            {
                Instance.DeleterUserId = deleterUserId;
                return this;
            }

            protected override void Specifications()
            {
                AddSpecification(new ExpressionSpecification<IdentityFeature>(
				AppConsts.LocalizationSourceName, 
				IdentityFeature.EntityError.IdentityFeatureMustHaveIdentityProductId, 
				w => w.IdentityProductId != default(int)));

                AddSpecification(new ExpressionSpecification<IdentityFeature>(
				AppConsts.LocalizationSourceName, 
				IdentityFeature.EntityError.IdentityFeatureMustHaveIdentityModuleId, 
				w => w.IdentityModuleId != default(int)));

                AddSpecification(new ExpressionSpecification<IdentityFeature>(
				AppConsts.LocalizationSourceName, 
				IdentityFeature.EntityError.IdentityFeatureMustHaveKey, 
				w => !string.IsNullOrWhiteSpace(w.Key)));

                AddSpecification(new ExpressionSpecification<IdentityFeature>(
				AppConsts.LocalizationSourceName, 
				IdentityFeature.EntityError.IdentityFeatureOutOfBoundKey, 
				w => string.IsNullOrWhiteSpace(w.Key) || w.Key.Length > 0 && w.Key.Length <= 200));

                AddSpecification(new ExpressionSpecification<IdentityFeature>(
				AppConsts.LocalizationSourceName, 
				IdentityFeature.EntityError.IdentityFeatureMustHaveName, 
				w => !string.IsNullOrWhiteSpace(w.Name)));

                AddSpecification(new ExpressionSpecification<IdentityFeature>(
				AppConsts.LocalizationSourceName, 
				IdentityFeature.EntityError.IdentityFeatureOutOfBoundName, 
				w => string.IsNullOrWhiteSpace(w.Name) || w.Name.Length > 0 && w.Name.Length <= 300));

                AddSpecification(new ExpressionSpecification<IdentityFeature>(
				AppConsts.LocalizationSourceName, 
				IdentityFeature.EntityError.IdentityFeatureMustHaveDescription, 
				w => !string.IsNullOrWhiteSpace(w.Description)));

                AddSpecification(new ExpressionSpecification<IdentityFeature>(
				AppConsts.LocalizationSourceName, 
				IdentityFeature.EntityError.IdentityFeatureOutOfBoundDescription, 
				w => string.IsNullOrWhiteSpace(w.Description) || w.Description.Length > 0 && w.Description.Length <= 400));

            }
        }
    }
}
