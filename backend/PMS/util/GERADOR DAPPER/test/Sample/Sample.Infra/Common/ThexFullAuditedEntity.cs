﻿using System;

namespace Sample.Infra.Entities
{
    [Serializable]
    public abstract class ThexFullAuditedEntity// : Tnf.Repositories.Entities.ISoftDelete
    {
        public virtual bool IsDeleted { get; set; }

        public virtual DateTime CreationTime { get; set; }

        public virtual Guid? CreatorUserId { get; set; }

        public virtual DateTime? LastModificationTime { get; set; }

        public virtual Guid? LastModifierUserId { get; set; }

        public virtual DateTime? DeletionTime { get; set; }

        public virtual Guid? DeleterUserId { get; set; }
    }
}
