﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a Crudzilla tool.
// </auto-generated>
//------------------------------------------------------------------------------

using Sample.Infra.Entities;
using Sample.Dto;

namespace Sample.Application.Adapters
{
    public interface IRoomAdapter
    {
        Room.Builder Map(Room entity, RoomDto dto);
        Room.Builder Map(RoomDto dto);
    }
}
