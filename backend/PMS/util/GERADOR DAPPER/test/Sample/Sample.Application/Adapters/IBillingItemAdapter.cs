﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a Crudzilla tool.
// </auto-generated>
//------------------------------------------------------------------------------

using Sample.Infra.Entities;
using Sample.Dto;

namespace Sample.Application.Adapters
{
    public interface IBillingItemAdapter
    {
        BillingItem.Builder Map(BillingItem entity, BillingItemDto dto);
        BillingItem.Builder Map(BillingItemDto dto);
    }
}
