﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a Crudzilla tool.
// </auto-generated>
//------------------------------------------------------------------------------

using Sample.Infra.Entities;
using Sample.Dto;
using Tnf;
using Tnf.Notifications;

namespace Sample.Application.Adapters
{
    public class IdentityFeatureAdapter : IIdentityFeatureAdapter
    {
        public INotificationHandler NotificationHandler { get; }

        public IdentityFeatureAdapter(INotificationHandler notificationHandler)
        {
            NotificationHandler = notificationHandler;
        }

        public virtual IdentityFeature.Builder Map(IdentityFeature entity, IdentityFeatureDto dto)
        {
            Check.NotNull(entity, nameof(entity));
            Check.NotNull(dto, nameof(dto));

            var builder = new IdentityFeature.Builder(NotificationHandler, entity)
                .WithId(dto.Id)
                .WithIdentityProductId(dto.IdentityProductId)
                .WithIdentityModuleId(dto.IdentityModuleId)
                .WithKey(dto.Key)
                .WithName(dto.Name)
                .WithDescription(dto.Description)
                .WithIsInternal(dto.IsInternal)
                .WithIsDeleted(dto.IsDeleted)
                .WithCreationTime(dto.CreationTime)
                .WithCreatorUserId(dto.CreatorUserId)
                .WithLastModificationTime(dto.LastModificationTime)
                .WithLastModifierUserId(dto.LastModifierUserId)
                .WithDeletionTime(dto.DeletionTime)
                .WithDeleterUserId(dto.DeleterUserId);

            return builder;
        }

        public virtual IdentityFeature.Builder Map(IdentityFeatureDto dto)
        {
            Check.NotNull(dto, nameof(dto));

            var builder = new IdentityFeature.Builder(NotificationHandler)
                .WithId(dto.Id)
                .WithIdentityProductId(dto.IdentityProductId)
                .WithIdentityModuleId(dto.IdentityModuleId)
                .WithKey(dto.Key)
                .WithName(dto.Name)
                .WithDescription(dto.Description)
                .WithIsInternal(dto.IsInternal)
                .WithIsDeleted(dto.IsDeleted)
                .WithCreationTime(dto.CreationTime)
                .WithCreatorUserId(dto.CreatorUserId)
                .WithLastModificationTime(dto.LastModificationTime)
                .WithLastModifierUserId(dto.LastModifierUserId)
                .WithDeletionTime(dto.DeletionTime)
                .WithDeleterUserId(dto.DeleterUserId);

            return builder;
        }
    }
}
