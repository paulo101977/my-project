﻿
namespace Thex.Common.Enumerations
{
    public enum ApplicationParameterEnum
    {
        ParameterCheckInTime = 1,
        ParameterCheckOutTime = 2,
        ParameterDailyLaunch = 3,
        ParameterDefaultCurrency = 4,
        ParameterDaily = 5,
        ParameterUpselling = 6,
        ParameterChildren_1 = 7,
        ParameterChildren_2 = 8,
        ParameterChildren_3 = 9,
        ParameterTimeZone = 12
    }
}
