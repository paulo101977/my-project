﻿namespace Thex.Common.Enumerations
{
    /// <summary>
    /// Nested <see cref="Person"/> type information 
    /// </summary>
    public enum PersonTypeEnum
    {
        /// <summary>
        /// Natural person represents an individual 
        /// </summary>
        Natural = 'N',

        /// <summary>
        /// A legal person represents a company 
        /// </summary>
        Legal = 'L',

        /// <summary>
        /// A contact person represents a contact
        /// </summary>
        Contact = 'C'
    }
}
