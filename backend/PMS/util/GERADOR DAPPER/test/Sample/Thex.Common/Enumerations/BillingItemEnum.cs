﻿namespace Thex.Common.Enumerations
{
    public class BillingItemEnum
    {

        /// <summary>
        /// Error enumerations of <see cref="BillingItem"/>/>
        /// </summary>
        public enum Error
        {
            InvalidPropertyPaymentTypeParameters = 1,
            InvalidPropertyPaymentTypeMaximumInstallmentsQuantity = 2,
            DuplicateBillingItemEnumPaymentType = 3,
            BillingItemForeignKeyError = 4,
            BillingAccountItemWithoutBillingInvoicePropertySupportedType = 5
        }
    }
}
