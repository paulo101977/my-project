﻿namespace Thex.Common.Enumerations
{
    public class BrandEnum
    {
        /// <summary>
        /// Error enumerations of <see cref="Brand"/>/>
        /// </summary>
        public enum Error
        {
            BrandMustHaveName = 1
        }
    }
}
