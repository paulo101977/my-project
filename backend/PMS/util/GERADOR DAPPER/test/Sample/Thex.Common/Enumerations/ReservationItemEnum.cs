﻿namespace Thex.Common.Enumerations
{
    public class ReservationItemEnum
    {

        /// <summary>
        /// Error enumerations of <see cref="ReservationItem"/>/>
        /// </summary>
        public enum Error
        {
            RoomIsNotAvaiable = 1,
            CouldNotChangeStatus = 2,
            ReservationItemCanNotBeChanged = 3,
            ReservationItemWithStatusCheckinCanNotBeChanged = 4,
            ReservationItemRoomCanNotBeChanged = 5,
            DuplicateRoomsInReservationItem = 6,
            InvalidCheckoutReservationItemHasOpenBillingAccount = 7
        }
    }
}
