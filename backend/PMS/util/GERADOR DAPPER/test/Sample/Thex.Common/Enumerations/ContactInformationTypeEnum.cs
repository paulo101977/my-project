﻿namespace Thex.Common.Enumerations
{
    /// <summary>
    /// Types of <see cref="ContactDto"/> information 
    /// manageable by the application
    /// </summary>
    public enum ContactInformationTypeEnum
    {
        /// <summary>
        /// Email address
        /// </summary>
        Email = 1,

        /// <summary>
        /// Phone number
        /// </summary>
        PhoneNumber = 2,

        /// <summary>
        /// Website address
        /// </summary>
        Website = 3,

        /// <summary>
        /// Cell Phone number
        /// </summary>
        CellPhoneNumber = 4
    }
}
