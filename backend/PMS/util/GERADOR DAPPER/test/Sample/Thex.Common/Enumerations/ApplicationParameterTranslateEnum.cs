﻿
namespace Thex.Common.Enumerations
{
    public enum ApplicationParameterTranslateEnum
    {
        ParameterCheckInTimeTranslate = 1,
        ParameterCheckOutTimeTranslate = 2,
        ParameterDailyLaunchTranslate = 3,
        ParameterDefaultCurrencyTranslate = 4,
        ParameterDailyTranslate = 5,
        ParameterChildren_1Translate = 7,
        ParameterChildren_2Translate = 8,
        ParameterChildren_3Translate = 9,
        ParameterSystemDateTranslate = 10,
        ParameterAuditStepTranslate = 11,
        ParameterTimeZoneTranslate = 12,
        ParameterUHStatusTranslate = 13
    }
}
