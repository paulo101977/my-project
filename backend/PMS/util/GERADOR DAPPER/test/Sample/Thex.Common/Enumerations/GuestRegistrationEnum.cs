﻿namespace Thex.Common.Enumerations
{
    public class GuestRegistrationEnum
    {
        /// <summary>
        /// Error enumerations of <see cref="GuestRegistrationEnum"/>/>
        /// </summary>
        public enum Error
        {
            GuestRegistrationLocationRequiredCpfOrRg = 1,
            GuestRegistrationResponsibleDocument = 2,
            GuestRegistrationMustBeAdult = 3,
            GuestRegistrationMustBeChild = 4
        }
    }
}