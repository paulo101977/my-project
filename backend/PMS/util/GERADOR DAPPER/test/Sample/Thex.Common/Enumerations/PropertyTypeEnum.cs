﻿//  <copyright file="PropertyTypeEnum.cs" company="TOTVS">
//  Copyright (c) TOTVS. All rights reserved.
//  </copyright>

namespace Thex.Common.Enumerations
{
    /// <summary>
    /// Types of <see cref="PropertyDto"/> manageable by the application
    /// </summary>
    public enum PropertyTypeEnum
    {
        /// <summary>
        /// Property is a Hostel
        /// </summary>
        Hotel = 1,

        /// <summary>
        /// Property is a Resort
        /// </summary>
        Resort = 2,

        /// <summary>
        /// Property is a Bed and Breakfast
        /// </summary>
        BedAndBreakfast = 3,

        /// <summary>
        /// Property is a Flat Hotel
        /// </summary>
        FlatHotel = 4,

        /// <summary>
        /// Property is a Hostel
        /// </summary>
        Hostel = 5,

        /// <summary>
        /// Property is a Motel
        /// </summary>
        Motel = 6
    }
}
