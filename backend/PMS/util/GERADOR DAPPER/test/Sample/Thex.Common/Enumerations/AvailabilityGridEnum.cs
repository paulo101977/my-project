// //  <copyright file="AvailabilityGridEnum.cs" company="TOTVS">
// //  Copyright (c) TOTVS. All rights reserved.
// //  </copyright>
namespace Thex.Common.Enumerations
{
    public enum AvailabilityGridEnum
    {
        AvailabilityGridTotal = 1,
        AvailabilityGridPercentage = 2,
        AvailabilityGridBlocked = 3
    }
}