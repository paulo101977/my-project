﻿namespace Thex.Common.Enumerations
{
    public class RateCalendarEnum
    {
        /// <summary>
        /// Error enumerations of <see cref="RateCalendarEnum"/>/>
        /// </summary>
        public enum Error
        {
            RateCalendarInvalidChildAge = 1,
            RateCalendarInvalidPeriod = 2,
            RateCalendarInvalidCurrency = 3,
            RateCalendarInvalidRoomType = 4,
            RateCalendarInvalidPropertyMealPlanType = 5,
            RateCalendarInvalidAdultCapacityOrChildCapacityPropertyMealPlanType = 6
        }

    }
}
