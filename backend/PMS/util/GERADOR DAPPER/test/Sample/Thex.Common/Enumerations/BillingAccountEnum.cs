﻿namespace Thex.Common.Enumerations
{
    public class BillingAccountEnum
    {
        /// <summary>
        /// Error enumerations of <see cref="BillingAccountEnum"/>/>
        /// </summary>
        public enum Error
        {
            MainBillingAccountCanNotBeDeleted = 1,
            BillingAccountWithBillingAccountItemsCanNotBeDeleted = 2,
            BillingAccountWithBillingAccountItemsCanNotBeClosedOrBlocked = 3,
            CanNotReopenAnBillingAccountAlreadyOpen = 4
        }

        public enum Default
        {
            BillingAccountDefaultName = 1,
            BillingAccountDefaultNameConsolidated = 2,
            BillingAccountDefaultNameMain = 3
        }
        
    }
}
