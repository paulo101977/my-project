﻿//  <copyright file="PaymentTypeEnum.cs" company="TOTVS">
//  Copyright (c) TOTVS. All rights reserved.
//  </copyright>
namespace Thex.Common.Enumerations
{
    public enum PaymentTypeEnum
    {
        InternalUsage = 1,
        Deposit = 2,
        Courtesy = 3,
        Hotel = 4,
        CreditCard = 5,
        TobeBilled = 6,
        Check = 7,
        Money = 8,
        Debit = 9
    }
}
