﻿namespace Thex.Common.Enumerations
{
    public class ReservationBudgetEnum
    {
        /// <summary>
        /// Error enumerations of <see cref="ReservationBudgetEnum"/>/>
        /// </summary>
        public enum Error
        {
            ReservationBudgetDayWasNotFound = 1
        }
    }
}
