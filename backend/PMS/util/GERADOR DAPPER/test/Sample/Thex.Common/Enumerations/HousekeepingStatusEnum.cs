﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Thex.Common.Enumerations
{
     public enum HousekeepingStatusEnum
    {
        Clean = 1,
        Dirty = 2,
        Maintenance = 3,
        Inspection = 4,
        Stowage = 5,
        CreatedByHotel = 6

    }
    public enum RoomAvailabilityEnum
    {
        Vague = 1,
        Busy = 2,
        Blocked = 3
    }

    public enum HousekeepingStatusEnumError
    {
        HousekeepingStatusDirtyNotExisting = 1
    }

}
