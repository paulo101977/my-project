﻿namespace Thex.Common.Enumerations
{
    public class PropertyEnum
    {
        /// <summary>
        /// Error enumerations of <see cref="Property"/>/>
        /// </summary>
        public enum Error
        {
            PropertyMustHaveName = 1,
            PropertyMustHaveLocation = 2
        }
    }
}
