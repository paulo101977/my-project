﻿namespace Thex.Common.Enumerations
{
    public class BillingAccountItemEnum
    {

        /// <summary>
        /// Error enumerations of <see cref="BillingAccountItem"/>/>
        /// </summary>
        public enum Error
        {
            BillingAccountItemInvalidAmount = 1,
            BillingAccountItemInvalidBillingItemId = 2,
            BillingAccountItemCanNotBeATax = 3,
            BillingAccountItemInvalidReasonId = 4,
            DuplicateBillingAccountEnumReversal = 5,
            BillingAccountItemsAlreadyReversed = 6,
            BillingAccountItemInvalidPaymentType = 7,
            BillingAccountItemExceedingMaximumInstallmentsQuantity = 8,

            BillingAccountItemsCanNotBeChildren = 9,
            BillingAccountItemsCanNotBeDiscountItem = 10,
            BillingAccountItemsCanNotBeReversalItem = 11,
            BillingAccountItemsMustBeFromTheSameDivision = 12,
            BillingAccountItemsMustBeFromTheSameBillingAccount = 13,
            BillingAccountItemsCantHasAnInvoiceLinked = 14,
            BillingAccountItemCantHasAnInvoiceLinked = 15,
            BillingAccountInvoiceNFeioComunicationError = 16
        }
    }
}
