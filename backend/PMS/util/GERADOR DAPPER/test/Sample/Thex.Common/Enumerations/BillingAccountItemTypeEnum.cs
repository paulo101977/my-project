﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Thex.Common.Enumerations
{
    public enum BillingAccountItemTypeEnum
    {
        BillingAccountItemTypeCredit = 1,
        BillingAccountItemTypeDebit = 2,
        BillingAccountItemTypeReversal = 3,
        BillingAccountItemTypeDiscount = 4,
        BillingAccountItemTypePartial = 5
    }
}
