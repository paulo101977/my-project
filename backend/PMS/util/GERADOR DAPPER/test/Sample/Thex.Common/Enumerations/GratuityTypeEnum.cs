﻿//  <copyright file="GratuityTypeEnum.cs" company="TOTVS">
//  Copyright (c) TOTVS. All rights reserved.
//  </copyright>
namespace Thex.Common.Enumerations
{
    public enum GratuityTypeEnum
    {
        GratuityTypeInternalUsage = 1,
        GratuityTypeCourtesy = 2
    }
}
