﻿
namespace Thex.Common.Enumerations
{
    public enum EmailTemplateEnum
    {
        EmailForgotPassword = 1,
        EmailUserInvitation,
        EmailIntegration
    }
}
