﻿namespace Thex.Common.Enumerations
{
    public class ChainEnum
    {
        /// <summary>
        /// Error enumerations of <see cref="Chain"/>/>
        /// </summary>
        public enum Error
        {
            ChainMustHaveName = 1
        }
    }
}