﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Thex.Common.Enumerations
{
    public enum PropertyAuditProcessStepEnum
    {
        StepOne = 1,
        StepTwo = 2,
        StepThree = 3,
        StepFour = 4,
        StepFive = 5
    }
}
