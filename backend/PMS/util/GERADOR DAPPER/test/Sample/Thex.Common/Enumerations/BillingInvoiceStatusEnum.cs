﻿//  <copyright file="BillingInvoiceStatusEnum.cs" company="TOTVS">
//  Copyright (c) TOTVS. All rights reserved.
//  </copyright>
namespace Thex.Common.Enumerations
{
    public enum BillingInvoiceStatusEnum
    {
        NotSent = 9,
        Issued = 10,
        Pending = 11,
        Error = 12,
        Canceling = 13,
        Canceled = 14
    }
}




