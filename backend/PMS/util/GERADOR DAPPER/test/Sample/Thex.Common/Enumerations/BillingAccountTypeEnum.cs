﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Thex.Common.Enumerations
{
    public enum BillingAccountTypeEnum
    {
        Sparse = 1,
        Guest = 2,
        Company = 3,
        GroupAccount = 4
    }
}
