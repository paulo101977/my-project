namespace Thex.Common.Enumerations
{
    public enum PersonInformationTypeEnum
    {
        SocialName = 1,
        HealthInsurance = 2,
        Gender = 3
    }
}
