﻿// //  <copyright file="BedTypeEnum.cs" company="TOTVS">
// //  Copyright (c) TOTVS. All rights reserved.
// //  </copyright>
namespace Thex.Common.Enumerations
{
    public enum BedTypeEnum
    {
        /// <summary>
        /// Single bed
        /// </summary>
        Single = 1,

        /// <summary>
        /// Double bed
        /// </summary>
        Double = 2,

        /// <summary>
        /// A single bed that can be joined together with another
        /// combinable single to form a double bed
        /// </summary>
        Reversible = 3,

        /// <summary>
        /// Extra beds of any kind
        /// </summary>
        Extra = 4,

        /// <summary>
        /// A baby crib
        /// </summary>
        Crib = 5
    }
}