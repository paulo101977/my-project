﻿namespace Thex.Common.Enumerations
{
    public class BillingInvoiceEnum
    {
        public enum Error
        {
            BillingInvoiceInvalidReason,
            BillingInvoiceInvalidEmissionDateOfCancel,
            BillingInvoiceInvalidAccountClosed
        }
    }
}
