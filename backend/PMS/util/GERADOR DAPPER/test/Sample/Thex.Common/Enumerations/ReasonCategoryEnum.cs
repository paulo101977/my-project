﻿// //  <copyright file="ReasonCategoryEnum.cs" company="TOTVS">
// //  Copyright (c) TOTVS. All rights reserved.
// //  </copyright>
namespace Thex.Common.Enumerations
{
    public enum ReasonCategoryEnum
    {
        CancelReservation = 1,
        ReservationScheduling = 2,
        EventDeadline = 3,
        RoomTransference = 4,
        RoomBlocking = 5,
        Discount = 6,
        PurposeOfTrip = 7,
        ArrivingBy = 8,
        Reversal = 9,
        ReopenBillingAccount = 10,
        BillingInvoiceCancel = 11
    }
}