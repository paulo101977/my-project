﻿
namespace Thex.Common.Enumerations
{
    public enum PolicyType
    {
        CheckInPolicy = 1,
        CheckOutPolicy,
        ModificationPolicy,
        PrepaymentAndWarrantyPolicy,
        ChildrensPolicy,
        OtherPolicies,
        CancellationPolicy
    }
}
