﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Thex.Common.Extensions
{
    public static class ObjectExtensions
    {
        public static Int64 TryParseToInt64(this object obj)
        {
            Int64 ret = 0;

            try
            {
                ret = Convert.ToInt64(obj);
            }
            catch (Exception)
            {
            }

            return ret;
        }

        public static int TryParseToInt32(this object obj)
        {
            int ret = 0;

            try
            {
                ret = Convert.ToInt32(obj);
            }
            catch (Exception)
            {
            }

            return ret;
        }
    }
}
