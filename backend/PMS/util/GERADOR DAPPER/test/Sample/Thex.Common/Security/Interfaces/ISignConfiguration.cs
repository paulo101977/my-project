﻿using Microsoft.IdentityModel.Tokens;

namespace Thex.Common.Security
{
    public interface ISignConfiguration
    {
        SecurityKey Key { get; }
        SigningCredentials SigningCredentials { get; }
    }
}