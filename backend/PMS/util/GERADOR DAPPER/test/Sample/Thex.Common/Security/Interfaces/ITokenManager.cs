﻿using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;

namespace Thex.Common.Security
{
    public interface ITokenManager
    {
        ClaimsIdentity CreateClaimIdentity(Guid userId, TokenInfo tokenInfo);
        ClaimsIdentity CreateClaimIdentity(string email);
        ClaimsIdentity CreateClaimIdentity(int IntegrationPartnerId, TokenInfo tokenInfo);
        ClaimsIdentity CreateClaimIdentity(string email, string name, Guid id, TokenInfo tokenInfo);
        UserTokenDto CreateUserToken(ClaimsIdentity identity);
        IntegrationPartnerTokenDto CreateIntegrationPartnerToken(ClaimsIdentity identity);
        JwtSecurityToken VerifyToken(string token);
        string CreateUserLink(string email, string name, Guid id);
        string CreateForgotPasswordLink(string email);
    }
}