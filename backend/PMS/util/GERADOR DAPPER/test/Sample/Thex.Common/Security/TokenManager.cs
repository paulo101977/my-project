﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Security.Claims;
using System.Security.Principal;
using System.IdentityModel.Tokens.Jwt;
using Tnf.Dto;

namespace Thex.Common.Security
{
    public class TokenConfiguration : ITokenConfiguration
    {
        public string Audience { get; set; }
        public string Issuer { get; set; }
        public int Seconds { get; set; }
    }

    public class UserTokenDto : BaseDto
    {
        public Guid Id { get; set; }
        public bool Authenticated { get; set; }
        public string Created { get; set; }
        public string Expiration { get; set; }
        public string NewPasswordToken { get; set; }
    }

    public class IntegrationPartnerTokenDto : BaseDto
    {
        public Guid Id { get; set; }
        public bool Authenticated { get; set; }
        public string Created { get; set; }
        public string Expiration { get; set; }
        public string NewPasswordToken { get; set; }
    }

    public class TokenManager : ITokenManager
    {
        IApplicationUser _applicationUser;
        IConfiguration _configuration;
        ISignConfiguration _signConfiguration;
        TokenConfiguration _tokenConfigurations;

        public TokenManager(IApplicationUser applicationUser, IConfiguration configuration, ISignConfiguration signConfiguration)
        {
            _applicationUser = applicationUser;
            _configuration = configuration;
            _signConfiguration = signConfiguration;

            _tokenConfigurations = new TokenConfiguration();
            new ConfigureFromConfigurationOptions<TokenConfiguration>(
                _configuration.GetSection("TokenConfiguration"))
                    .Configure(_tokenConfigurations);
        }

        // New  user claims and new user token
        public ClaimsIdentity CreateClaimIdentity(string email, string name, Guid id, TokenInfo tokenInfo)
        {
            ClaimsIdentity identity = new ClaimsIdentity(
                new GenericIdentity(id.ToString(), "NewUser"),
                new[] {
                        new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString("N")),
                        new Claim(TokenInfoClaims.Name, name),
                        new Claim(JwtRegisteredClaimNames.Email, email)
                }
            ).AddTokenInfo(tokenInfo);
            return identity;
        }

        // Forgot password Claims
        public ClaimsIdentity CreateClaimIdentity(string email)
        {
            string TempId = Guid.NewGuid().ToString();
            ClaimsIdentity identity = new ClaimsIdentity(
                new GenericIdentity(TempId, "TempId"),
                new[] {
                        new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString("N")),
                        new Claim(JwtRegisteredClaimNames.Email, email)
                }
            );
            return identity;
        }

        // Login Claims
        public ClaimsIdentity CreateClaimIdentity(Guid userId, TokenInfo tokenInfo)
        {
            string strUserId = userId.ToString();

            ClaimsIdentity identity = new ClaimsIdentity(
                new GenericIdentity(strUserId, "user"),
                new[] {
                        new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString("N")),
                        new Claim(JwtRegisteredClaimNames.Sub, strUserId)
                }
            ).AddTokenInfo(tokenInfo);
            return identity;
        }

        public ClaimsIdentity CreateClaimIdentity(int IntegrationPartnerId, TokenInfo tokenInfo)
        {
            string strIntegrationPartnerId = IntegrationPartnerId.ToString();

            ClaimsIdentity identity = new ClaimsIdentity(
                new GenericIdentity(strIntegrationPartnerId, "integrationPartner"),
                new[] {
                        new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString("N")),
                        new Claim(JwtRegisteredClaimNames.Sub, strIntegrationPartnerId)
                }
            ).AddTokenInfo(tokenInfo);

            return identity;
        }

        public static TokenInfo GetTokenInfo(IApplicationUser applicationUser)
        {
            TokenInfo tokenInfo = new TokenInfo();

            tokenInfo.LoggedUserName = applicationUser.UserName;
            tokenInfo.LoggedUserUid = applicationUser.UserUid;
            tokenInfo.LoggedUserEmail = applicationUser.UserEmail;
            tokenInfo.TenantId = applicationUser.TenantId;

            int propertyId = 0;
            Int32.TryParse(applicationUser.PropertyId, out propertyId);
            tokenInfo.PropertyId = propertyId;

            int chainId = 0;
            Int32.TryParse(applicationUser.ChainId, out chainId);
            tokenInfo.ChainId = chainId;

            tokenInfo.IsAdmin = applicationUser.IsAdmin;
            tokenInfo.PreferredCulture = applicationUser.PreferredCulture;
            tokenInfo.PreferredLanguage = applicationUser.PreferredLanguage;
            tokenInfo.PropertyCulture = applicationUser.PropertyCulture;
            tokenInfo.PropertyLanguage = applicationUser.PropertyLanguage;
            tokenInfo.TimeZoneName = applicationUser.TimeZoneName;

            return tokenInfo;
        }

        public string CreateUserLink(string email, string name, Guid id)
        {
            string frontUrl = _configuration.GetValue<string>("UrlFront");

            ClaimsIdentity identity = CreateClaimIdentity(email, name, id, GetTokenInfo(_applicationUser));
            UserTokenDto usertoken = CreateUserToken(identity);
            Uri link;
            Uri.TryCreate(frontUrl + "/auth/signup;token=" + usertoken.NewPasswordToken, UriKind.Absolute, out link);

            return link.AbsoluteUri;
        }

        public string CreateForgotPasswordLink(string email)
        {
            string frontUrl = _configuration.GetValue<string>("UrlFront");

            ClaimsIdentity identity = CreateClaimIdentity(email);
            UserTokenDto usertoken = CreateUserToken(identity);
            Uri link;
            Uri.TryCreate(frontUrl + "/auth/reset-password;token=" + usertoken.NewPasswordToken, UriKind.Absolute, out link);

            return link.AbsoluteUri;
        }

        public UserTokenDto CreateUserToken(ClaimsIdentity identity)
        {
            if (identity == null)
            {
                return null;
            }

            DateTime dataCriacao = DateTime.UtcNow;
            DateTime dataExpiracao = dataCriacao +
                TimeSpan.FromSeconds(_tokenConfigurations.Seconds);

            var handler = new JwtSecurityTokenHandler();
            var securityToken = handler.CreateToken(new SecurityTokenDescriptor
            {
                Issuer = _tokenConfigurations.Issuer,
                Audience = _tokenConfigurations.Audience,
                SigningCredentials = _signConfiguration.SigningCredentials,
                Subject = identity,
                NotBefore = dataCriacao,
                Expires = dataExpiracao
            });
            var token = handler.WriteToken(securityToken);

            return new UserTokenDto
            {
                Authenticated = true,
                Created = dataCriacao.ToString("yyyy-MM-dd HH:mm:ss"),
                Expiration = dataExpiracao.ToString("yyyy-MM-dd HH:mm:ss"),
                NewPasswordToken = token,
                Id = Guid.NewGuid()
            };
        }

        public IntegrationPartnerTokenDto CreateIntegrationPartnerToken(ClaimsIdentity identity)
        {
            if (identity == null)
            {
                return null;
            }

            DateTime dataCriacao = DateTime.UtcNow;
            DateTime dataExpiracao = dataCriacao +
                TimeSpan.FromSeconds(_tokenConfigurations.Seconds);

            var handler = new JwtSecurityTokenHandler();
            var securityToken = handler.CreateToken(new SecurityTokenDescriptor
            {
                Issuer = _tokenConfigurations.Issuer,
                Audience = _tokenConfigurations.Audience,
                SigningCredentials = _signConfiguration.SigningCredentials,
                Subject = identity,
                NotBefore = dataCriacao,
                Expires = dataExpiracao
            });
            var token = handler.WriteToken(securityToken);

            return new IntegrationPartnerTokenDto
            {
                Authenticated = true,
                Created = dataCriacao.ToString("yyyy-MM-dd HH:mm:ss"),
                Expiration = dataExpiracao.ToString("yyyy-MM-dd HH:mm:ss"),
                NewPasswordToken = token,
                Id = Guid.NewGuid()
            };
        }

        public JwtSecurityToken VerifyToken(string token)
        {
            var validationParameters = new TokenValidationParameters()
            {
                IssuerSigningKey = _signConfiguration.Key,
                ValidAudience = _tokenConfigurations.Audience,
                ValidIssuer = _tokenConfigurations.Issuer,
                ValidateLifetime = true,
                ValidateAudience = true,
                ValidateIssuer = true,
                ValidateIssuerSigningKey = true
            };

            var handler = new JwtSecurityTokenHandler();
            SecurityToken validatedToken = null;
            try
            {
                handler.ValidateToken(token, validationParameters, out validatedToken);
                return (validatedToken as JwtSecurityToken);
            }
            catch (SecurityTokenException)
            {
                return null;
            }
            catch (Exception e)
            {
                return null;
            }
        }
    }
}