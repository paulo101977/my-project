﻿using System;

namespace Thex.Common
{
    public interface ITokenInfo
    {
        string LoggedUserEmail { get; set; }
        string LoggedUserName { get; set; }
        Guid LoggedUserUid { get; set; }
        int? PropertyId { get; set; }
        int? ChainId { get; set; }
        Guid TenantId { get; set; }
        int? BrandId { get; set; }
        bool? IsAdmin { get; set; }
        string PreferredLanguage { get; set; }
        string PreferredCulture { get; set; }
        string PropertyLanguage { get; set; }
        string PropertyCulture { get; set; }
        string TimeZoneName { get; set; }
    }
}