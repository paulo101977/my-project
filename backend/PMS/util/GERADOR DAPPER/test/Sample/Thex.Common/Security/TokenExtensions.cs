﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;

namespace Thex.Common.Security
{
    public static class TokenExtensions
    {

        public static ClaimsIdentity AddTokenInfo(this ClaimsIdentity identity, TokenInfo tokenInfo)
        {
            identity.AddClaims(
                new[] {
                        new Claim(TokenInfoClaims.LoggedUserEmail, tokenInfo.LoggedUserEmail),
                        new Claim(TokenInfoClaims.LoggedUserName, tokenInfo.LoggedUserName),
                        new Claim(TokenInfoClaims.LoggedUserUid, tokenInfo.LoggedUserUid.ToString()),
                        new Claim(TokenInfoClaims.PropertyId, tokenInfo.PropertyId.HasValue ?  tokenInfo.PropertyId.Value.ToString() : ""),
                        new Claim(TokenInfoClaims.BrandId, tokenInfo.BrandId.HasValue ? tokenInfo.BrandId.Value.ToString() : ""),
                        new Claim(TokenInfoClaims.ChainId, tokenInfo.ChainId.HasValue ? tokenInfo.ChainId.Value.ToString() : ""),
                        new Claim(TokenInfoClaims.TenantId, tokenInfo.TenantId.ToString()),
                        new Claim(TokenInfoClaims.IsAdmin, tokenInfo.IsAdmin.HasValue ? tokenInfo.IsAdmin.Value.ToString().ToLower() : "false"),
                        new Claim(TokenInfoClaims.PreferredCulture, tokenInfo.PreferredCulture != null ? tokenInfo.PreferredCulture : ""),
                        new Claim(TokenInfoClaims.PreferredLanguage, tokenInfo.PreferredLanguage != null ? tokenInfo.PreferredLanguage : ""),
                        new Claim(TokenInfoClaims.PropertyCulture, tokenInfo.PropertyCulture != null ? tokenInfo.PropertyCulture : ""),
                        new Claim(TokenInfoClaims.PropertyLanguage,tokenInfo.PropertyLanguage != null ? tokenInfo.PropertyLanguage : ""),
                        new Claim(TokenInfoClaims.TimeZoneName, tokenInfo.TimeZoneName != null ? tokenInfo.TimeZoneName : AppConsts.DEFAULT_TIME_ZONE_NAME)
                }
            );
            return identity;
        }
    }
}
