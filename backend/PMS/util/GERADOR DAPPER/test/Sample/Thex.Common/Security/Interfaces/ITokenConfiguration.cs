﻿namespace Thex.Common.Security
{
    public interface ITokenConfiguration
    {
        string Audience { get; set; }
        string Issuer { get; set; }
        int Seconds { get; set; }
    }
}