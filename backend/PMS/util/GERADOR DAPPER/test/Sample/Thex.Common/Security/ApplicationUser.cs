﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Linq;

namespace Thex.Common.Security
{
    public class ApplicationUser : IApplicationUser
    {
        private readonly IHttpContextAccessor _accessor;
        private Dictionary<string, string> _claims;
        private readonly string TenantIdField = "TenantId";
        private readonly string LoggedUserEmailField = "LoggedUserEmail";
        private readonly string LoggedUserUidField = "LoggedUserUid";
        private readonly string LoggedUserNameField = "LoggedUserName";
        private readonly string PropertyIdField = "PropertyId";
        private readonly string BrandIdField = "BrandId";
        private readonly string ChainIdField = "ChainId";
        private readonly string IsAdminField = "IsAdmin";
        private readonly string PreferredCultureField = "PreferredCulture";
        private readonly string PreferredLanguageField = "PreferredLanguage";
        private readonly string PropertyCultureField = "PropertyCulture";
        private readonly string PropertyLanguageField = "PropertyLanguage";
        private readonly string TimeZoneNameField = "TimeZoneName";
        
        public ApplicationUser(IHttpContextAccessor accessor)
        {
            _accessor = accessor;
            _claims = null;
        }

        public string Name => _accessor.HttpContext.User.Identity.Name;

        public string UserEmail
        {
            get
            {
                if (_claims == null)
                    _claims = GetClaimsIdentity();
                if (_claims.ContainsKey(LoggedUserEmailField))
                    return _claims[LoggedUserEmailField];
                else return "";
            }
        }

        public Guid UserUid { get {
                if (_claims == null)
                    _claims = GetClaimsIdentity();
                if (_claims.ContainsKey(LoggedUserUidField))
                    return new Guid(_claims[LoggedUserUidField]);
                else return Guid.Empty;
            }
        }

        public string UserName
        {
            get
            {
                if (_claims == null)
                    _claims = GetClaimsIdentity();
                if (_claims.ContainsKey(LoggedUserNameField))
                    return _claims[LoggedUserNameField];
                else return "";
            }
        }

        public Guid TenantId
        {
            get
            {
                if (_claims == null)
                    _claims = GetClaimsIdentity();
                if (_claims.ContainsKey(TenantIdField))
                    return new Guid(_claims[TenantIdField]);
                else return Guid.Empty;
            }
        }

        public string PropertyId
        {
            get
            {
                if (_claims == null)
                    _claims = GetClaimsIdentity();
                if (_claims.ContainsKey(PropertyIdField))
                    return _claims[PropertyIdField];
                else return "";
            }
        }
        
        public string BrandId
        {
            get
            {
                if (_claims == null)
                    _claims = GetClaimsIdentity();
                if (_claims.ContainsKey(BrandIdField))
                    return _claims[BrandIdField];
                else return "";
            }
        }

        public string ChainId
        {
            get
            {
                if (_claims == null)
                    _claims = GetClaimsIdentity();
                if (_claims.ContainsKey(ChainIdField))
                    return _claims[ChainIdField];
                else return "";
            }
        }


        public bool IsAdmin
        {
            get
            {
                bool isAdminValue = false;
                if (_claims == null)
                    _claims = GetClaimsIdentity();
                if (_claims.ContainsKey(IsAdminField))
                {
                    Boolean.TryParse(_claims[IsAdminField], out isAdminValue);

                    return isAdminValue;
                }
                else return isAdminValue;
            }
        }

        public string PreferredCulture
        {
            get
            {
                if (_claims == null)
                    _claims = GetClaimsIdentity();
                if (_claims.ContainsKey(PreferredCultureField))
                    return _claims[PreferredCultureField];
                else return "";
            }
        }

        public string PreferredLanguage
        {
            get
            {
                if (_claims == null)
                    _claims = GetClaimsIdentity();
                if (_claims.ContainsKey(PreferredLanguageField))
                    return _claims[PreferredLanguageField];
                else return "";
            }
        }

        public string PropertyCulture
        {
            get
            {
                if (_claims == null)
                    _claims = GetClaimsIdentity();
                if (_claims.ContainsKey(PropertyCultureField))
                    return _claims[PropertyCultureField];
                else return "";
            }
        }

        public string PropertyLanguage
        {
            get
            {
                if (_claims == null)
                    _claims = GetClaimsIdentity();
                if (_claims.ContainsKey(PropertyLanguageField))
                    return _claims[PropertyLanguageField];
                else return "";
            }
        }


        public string TimeZoneName
        {
            get
            {
                if (_claims == null)
                    _claims = GetClaimsIdentity();
                if (_claims.ContainsKey(TimeZoneNameField))
                    return _claims[TimeZoneNameField];
                else return "";
            }
        }

        public bool IsAuthenticated()
        {
            return _accessor.HttpContext.User.Identity.IsAuthenticated;
        }

        public Dictionary<string, string> GetClaimsIdentity()
        {
            Dictionary<string, string> result = new Dictionary<string, string>();

            foreach (var claim in _accessor.HttpContext.User.Claims)
            {
                result.AddIfNotContains<KeyValuePair<string, string>>(new KeyValuePair<string, string>(claim.Type,claim.Value));
            }
            return result;
        }
    }
}
