﻿using System;
using System.Collections.Generic;
using System.Security.Claims;

namespace Thex.Common.Security
{
    public interface IApplicationUser
    {
        string Name { get; }
        Dictionary<string, string> GetClaimsIdentity();
        bool IsAuthenticated();
        string UserEmail { get; }
        Guid UserUid { get; }
        string UserName { get; }
        Guid TenantId { get; }
        string PropertyId { get; }
        string ChainId { get; }
        bool IsAdmin { get; }
        string PreferredLanguage { get; }
        string PreferredCulture { get; }
        string PropertyLanguage { get; }
        string PropertyCulture { get; }
        string TimeZoneName { get; }
    }
}