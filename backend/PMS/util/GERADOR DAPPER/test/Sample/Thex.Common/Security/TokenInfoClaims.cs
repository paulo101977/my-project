﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Thex.Common.Security
{
    public struct TokenInfoClaims
    {
        public const string LoggedUserUid = "LoggedUserUid";
        public const string LoggedUserName  = "LoggedUserName";
        public const string LoggedUserEmail  = "LoggerUserEmail";
        public const string TenantId = "TenantId";
        public const string PropertyId = "PropertyId";
        public const string BrandId = "BrandId";
        public const string ChainId  = "ChainId";
        public const string Name = "Name";
        public const string IsAdmin = "IsAdmin";
        public const string PreferredCulture = "PreferredCulture";
        public const string PreferredLanguage = "PreferredLanguage";
        public const string PropertyCulture = "PropertyCulture";
        public const string PropertyLanguage = "PropertyLanguage";
        public const string TimeZoneName = "TimeZoneName"; 
    }
}