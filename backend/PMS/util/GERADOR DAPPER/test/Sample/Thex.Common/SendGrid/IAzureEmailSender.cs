﻿using System.Threading.Tasks;
using Thex.Common.SendGrid.Message;

namespace Thex.Common.SendGrid
{
    public interface IAzureEmailSender
    {
        Task<ResponseMessage> SendAsync(EmailMessage message, bool IsHtml = false);
    }
}