﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Thex.Common
{
    //TODO: resolver problema com chamadas anônimas na API de integrações
    //fazer com que estas APIs obtenham um token
    public class IntegrationUserDataHack
    {
        public static string TimeZone { get; set; }
        public static string UserId { get; set; }
        public static string TenantId { get; set; }
    }
}
