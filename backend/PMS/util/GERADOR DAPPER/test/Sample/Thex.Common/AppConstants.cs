﻿using Microsoft.Extensions.DependencyInjection;
using System;
using Thex.Common.Security;

namespace Thex.Common
{
    public class AppConsts
    {
        public const string LocalizationSourceName = "Thex";

        private const string ENVIRONMENT_VARIABLE = "ASPNETCORE_ENVIRONMENT";
        private const string DEV_ENVIRONMENT_VARIABLE = "DEVELOPMENT";
        public const string DEFAULT_TIME_ZONE_NAME = "America/Sao_Paulo";

        public static bool IsDevelopment()
        {
            var environmentName = Environment.GetEnvironmentVariable(ENVIRONMENT_VARIABLE);
            if (environmentName.ToUpperInvariant() == DEV_ENVIRONMENT_VARIABLE)
                return true;

            return false;
        }
    }
}
