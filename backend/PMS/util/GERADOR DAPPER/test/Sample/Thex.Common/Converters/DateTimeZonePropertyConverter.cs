﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using Thex.Common.Extensions;
using Thex.Common.Security;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Http;

namespace Thex.Common.Converters
{
    //HOW TO USE THIS NOTATION:
    //[JsonConverter(typeof(DateTimeZoneConverter))]

    public class DateTimeZoneConverter : DateTimeConverterBase
    {
        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            return DateTime.Parse(reader.Value.ToString());
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            var contextAccessor = ServiceLocator.Current.GetInstance<IHttpContextAccessor>();
            var applicationUser = new ApplicationUser(contextAccessor);
            var dateTime = ((DateTime)value);

            if (applicationUser != null)
                dateTime = dateTime.ToZonedDateTimeLoggedUser(applicationUser);

            writer.WriteValue(dateTime);
        }
    }
}
