﻿using Crudzilla.Test.Domain;
using Crudzilla.Test.EntityFrameworkCore;
using Crudzilla.Test.Mapper;
using Tnf.Modules;

namespace Crudzilla.Test.Application
{
    [DependsOn(
        typeof(DomainModule),
        typeof(MapperModule),
        typeof(EntityFrameworkModule))]
    public class AppModule : TnfModule
    {
        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention<AppModule>();
        }
    }
}
