﻿using Microsoft.EntityFrameworkCore;
using Tnf.App.EntityFrameworkCore;
using Tnf.EntityFrameworkCore.Configuration;
using Tnf.Modules;

namespace Crudzilla.Test.EntityFrameworkCore
{
    [DependsOn(
        typeof(TnfAppEntityFrameworkCoreModule))]
    public class EntityFrameworkModule : TnfModule
    {
        public override void PreInitialize()
        {
            Configuration
                .Modules
                .TnfEfCore()
                .AddDbContext<CrudzillaContext>(configuration =>
            {
                configuration.DbContextOptions.UseSqlServer(configuration.ConnectionString);
            });
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention<EntityFrameworkModule>();
        }
    }
}