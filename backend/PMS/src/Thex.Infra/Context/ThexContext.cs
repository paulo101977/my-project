﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Thex.Common;
using Thex.Domain.Entities;
using Thex.EntityFrameworkCore.Utils;
using Thex.GenericLog;
using Thex.Infra.Context.Builders;
using Thex.Kernel;
using Thex.SuperAdmin.Infra.Mappers;
using Tnf.EntityFrameworkCore;
using Tnf.Runtime.Session;

namespace Thex.Infra.Context
{
    public abstract class ThexContext : TnfDbContext
    {
        private IGenericLogHandler _genericLog;
        protected IApplicationUser _applicationUser;
        private IConfiguration _configuration;
        protected Guid _tenantId = Guid.Empty;
        protected Guid _userUid = Guid.Empty;
        protected IServiceProvider _serviceProvider;

        public ThexContext(IApplicationUser applicationUser,
                           IConfiguration configuration,
                           DbContextOptions<ThexContext> options,
                           ITnfSession session,
                           IGenericLogHandler genericLog,
                           IServiceProvider serviceProvider)
                : base(options, session)
        {
            _applicationUser = applicationUser;
            _configuration = configuration;
            _genericLog = genericLog;
            _tenantId = applicationUser.TenantId != Guid.Empty ? applicationUser.TenantId : Guid.Empty;
            _userUid = applicationUser.UserUid != Guid.Empty ? applicationUser.UserUid : Guid.Empty;
            _serviceProvider = serviceProvider;
        }

        public virtual DbSet<AgreementType> AgreementTypes { get; set; }
        public virtual DbSet<ApplicationModule> ApplicationModules { get; set; }
        public virtual DbSet<ApplicationParameter> ApplicationParameters { get; set; }
        public virtual DbSet<AuditStepType> AuditStepTypes { get; set; }
        public virtual DbSet<BedType> BedTypes { get; set; }
        public virtual DbSet<BillingAccount> BillingAccounts { get; set; }
        public virtual DbSet<BillingAccountItem> BillingAccountItems { get; set; }
        public virtual DbSet<BillingAccountItemInvoiceHistory> BillingAccountItemInvoiceHistories { get; set; }
        public virtual DbSet<BillingAccountItemTransfer> BillingAccountItemTransfers { get; set; }
        public virtual DbSet<BillingAccountItemType> BillingAccountItemTypes { get; set; }
        public virtual DbSet<BillingAccountType> BillingAccountTypes { get; set; }
        public virtual DbSet<BillingInvoice> BillingInvoices { get; set; }
        public virtual DbSet<BillingInvoiceModel> BillingInvoiceModels { get; set; }
        public virtual DbSet<BillingInvoiceProperty> BillingInvoiceProperties { get; set; }
        public virtual DbSet<BillingInvoicePropertySupportedType> BillingInvoicePropertySupportedTypes { get; set; }
        public virtual DbSet<BillingInvoiceType> BillingInvoiceTypes { get; set; }
        public virtual DbSet<BillingItem> BillingItems { get; set; }
        public virtual DbSet<BillingItemCategory> BillingItemCategories { get; set; }
        public virtual DbSet<BillingItemPaymentCondition> BillingItemPaymentConditions { get; set; }
        public virtual DbSet<BillingItemType> BillingItemTypes { get; set; }
        public virtual DbSet<BillingTax> BillingTaxs { get; set; }
        public virtual DbSet<Brand> Brands { get; set; }
        public virtual DbSet<BusinessPartner> BusinessPartners { get; set; }
        public virtual DbSet<BusinessPartnerType> BusinessPartnerTypes { get; set; }
        public virtual DbSet<BusinessSource> BusinessSources { get; set; }
        public virtual DbSet<HousekeepingStatus> HousekeepingStatuss { get; set; }
        public virtual DbSet<Chain> Chains { get; set; }
        public virtual DbSet<ChainBusinessSource> ChainBusinessSources { get; set; }
        public virtual DbSet<ChainMarketSegment> ChainMarketSegments { get; set; }
        public virtual DbSet<Channel> Channels { get; set; }
        public virtual DbSet<Level> Levels { get; set; }
        public virtual DbSet<ChannelCode> ChannelCodes { get; set; }
        public virtual DbSet<Company> Companies { get; set; }
        public virtual DbSet<CompanyClient> CompanyClients { get; set; }
        public virtual DbSet<CompanyClientChannel> CompanyClientChannels { get; set; }
        public virtual DbSet<CompanyClientContactPerson> CompanyClientContactPersons { get; set; }
        public virtual DbSet<CompanyContactPerson> CompanyContactPersons { get; set; }
        public virtual DbSet<ContactInformation> ContactInformations { get; set; }
        public virtual DbSet<ContactInformationType> ContactInformationTypes { get; set; }
        public virtual DbSet<CountryCurrency> CountryCurrencies { get; set; }
        public virtual DbSet<CountryLanguage> CountryLanguages { get; set; }
        public virtual DbSet<CountryPersonDocumentType> CountryPersonDocumentTypes { get; set; }
        public virtual DbSet<CountrySubdivision> CountrySubdivisions { get; set; }
        public virtual DbSet<CountrySubdivisionTranslation> CountrySubdivisionTranslations { get; set; }
        public virtual DbSet<CountrySubdvisionService> CountrySubdvisionServices { get; set; }
        public virtual DbSet<Currency> Currencies { get; set; }
        public virtual DbSet<CurrencyExchange> CurrencyExchanges { get; set; }
        public virtual DbSet<Document> Documents { get; set; }
        public virtual DbSet<DocumentType> DocumentTypes { get; set; }
        public virtual DbSet<Employee> Employees { get; set; }
        public virtual DbSet<FeatureGroup> FeatureGroups { get; set; }
        public virtual DbSet<GuestRegistration> GuestRegistrations { get; set; }
        public virtual DbSet<GuestRegistrationDocument> GuestRegistrationDocuments { get; set; }
        public virtual DbSet<GeneralOccupation> GeneralOccupations { get; set; }
        public virtual DbSet<GratuityType> GratuityTypes { get; set; }
        public virtual DbSet<HousekeepingStatusProperty> HousekeepingStatusProperties { get; set; }
        public virtual DbSet<Guest> Guests { get; set; }
        public virtual DbSet<GuestReservationItem> GuestReservationItems { get; set; }
        public virtual DbSet<IntegrationPartner> IntegrationPartners { get; set; }
        public virtual DbSet<IntegrationPartnerProperty> IntegrationPartnerProperties { get; set; }
        public virtual DbSet<LevelRate> LevelRates { get; set; }
        public virtual DbSet<LevelRateHeader> LevelRateHeaders { get; set; }
        public virtual DbSet<Location> Locations { get; set; }
        public virtual DbSet<LocationCategory> LocationCategories { get; set; }
        public virtual DbSet<MarketSegment> MarketSegments { get; set; }
        public virtual DbSet<MealPlanType> MealPlanTypes { get; set; }
        public virtual DbSet<Occupation> Occupations { get; set; }
        public virtual DbSet<ParameterType> ParameterTypes { get; set; }
        public virtual DbSet<PaymentType> PaymentTypes { get; set; }
        public virtual DbSet<Person> Persons { get; set; }
        public virtual DbSet<PersonInformation> PersonInformations { get; set; }
        public virtual DbSet<PersonInformationType> PersonInformationTypes { get; set; }
        public virtual DbSet<Plastic> Plastics { get; set; }
        public virtual DbSet<PlasticBrand> PlasticBrands { get; set; }
        public virtual DbSet<PlasticBrandProperty> PlasticBrandProperties { get; set; }
        public virtual DbSet<PolicyType> PolicyTypes { get; set; }
        public virtual DbSet<Property> Properties { get; set; }
        public virtual DbSet<PropertyBaseRateHeaderHistory> PropertyBaseRateHeaderHistories { get; set; }
        public virtual DbSet<PropertyBaseRateHistory> PropertyBaseRateHistories { get; set; }
        public virtual DbSet<PropertyAuditProcess> PropertyAuditProcesss { get; set; }
        public virtual DbSet<PropertyAuditProcessStep> PropertyAuditProcessSteps { get; set; }
        public virtual DbSet<PropertyAuditProcessStepError> PropertyAuditProcessStepErrors { get; set; }
        public virtual DbSet<PropertyBaseRate> PropertyBaseRates { get; set; }
        public virtual DbSet<PropertyRateStrategy> PropertyRateStrategies { get; set; }
        public virtual DbSet<PropertyRateStrategyRatePlan> PropertyRateStrategyRatePlans { get; set; }
        public virtual DbSet<PropertyRateStrategyRoomType> PropertyRateStrategyRoomTypes { get; set; }
        public virtual DbSet<PropertyBusinessPartner> PropertyBusinessPartners { get; set; }
        public virtual DbSet<PropertyCompanyClientCategory> PropertyCompanyClientCategories { get; set; }
        public virtual DbSet<PropertyContactPerson> PropertyContactPersons { get; set; }
        public virtual DbSet<PropertyContract> PropertyContracts { get; set; }
        public virtual DbSet<PropertyCurrencyExchange> PropertyCurrencyExchanges { get; set; }
        public virtual DbSet<PropertyGuestPrefs> PropertyGuestPrefss { get; set; }
        public virtual DbSet<PropertyGuestType> PropertyGuestTypes { get; set; }
        public virtual DbSet<PropertyMealPlanType> PropertyMealPlanTypes { get; set; }
        public virtual DbSet<PropertyMealPlanTypeRate> PropertyMealPlanTypeRates { get; set; }
        public virtual DbSet<PropertyMealPlanTypeRateHistory> PropertyMealPlanTypeRateHistories { get; set; }
        public virtual DbSet<PropertyMealPlanTypeRateHeader> PropertyMealPlanTypeRateHeaders { get; set; }
        public virtual DbSet<PropertyParameter> PropertyParameters { get; set; }
        public virtual DbSet<PropertyPolicy> PropertyPolicies { get; set; }
        public virtual DbSet<PropertyType> PropertyTypes { get; set; }
        public virtual DbSet<RateType> RateTypes { get; set; }
        public virtual DbSet<RatePlan> RatePlans { get; set; }
        public virtual DbSet<RatePlanCommission> RatePlanCommissions { get; set; }
        public virtual DbSet<RatePlanCompanyClient> RatePlanCompanyClients { get; set; }
        public virtual DbSet<RatePlanRoomType> RatePlanRoomTypes { get; set; }
        public virtual DbSet<Reason> Reasons { get; set; }
        public virtual DbSet<ReasonCategory> ReasonCategories { get; set; }
        public virtual DbSet<Reservation> Reservations { get; set; }
        public virtual DbSet<ReservationBudget> ReservationBudgets { get; set; }
        public virtual DbSet<ReservationConfirmation> ReservationConfirmations { get; set; }
        public virtual DbSet<ReservationItem> ReservationItems { get; set; }
        public virtual DbSet<ReservationItemLaunch> ReservationItemLaunchers { get; set; }
        public virtual DbSet<Room> Rooms { get; set; }
        public virtual DbSet<RoomBlocking> RoomBlockings { get; set; }
        public virtual DbSet<RoomLayout> RoomLayouts { get; set; }
        public virtual DbSet<RoomType> RoomTypes { get; set; }
        public virtual DbSet<RoomTypeBedType> RoomTypeBedTypes { get; set; }
        public virtual DbSet<RoomTypeInventory> RoomTypeInventories { get; set; }
        public virtual DbSet<Status> Statuss { get; set; }
        public virtual DbSet<StatusCategory> StatusCategories { get; set; }
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<Subscription> Subscriptions { get; set; }
        public virtual DbSet<Tenant> Tenants { get; set; }
        public virtual DbSet<TransportationType> TransportationTypes { get; set; }
        public object PowerBiGroupReportDashboard { get; internal set; }
        public virtual DbSet<PropertyPremise> PropertyPremises { get; set; }
        public virtual DbSet<PropertyPremiseHeader> PropertyPremiseHeaders { get; set; }
        public virtual DbSet<TourismTax> TourismTaxs { get; set; }
        public virtual DbSet<RoomOccupied> RoomOccupieds { get; set; }
        public virtual DbSet<PropertyDocumentType> PropertyDocumentTypes { get; set; }
        public virtual DbSet<SibaIntegration> SibaIntegrations { get; set; }
        public virtual DbSet<ContactOwner> ContactOwners { get; set; }

        public virtual DbSet<ExternalGuestRegistration> ExternalGuestRegistrations { get; set; }
        public virtual DbSet<ExternalRegistration> ExternalRegistrations { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new AgreementTypeMapper());
            modelBuilder.ApplyConfiguration(new ApplicationModuleMapper());
            modelBuilder.ApplyConfiguration(new ApplicationParameterMapper());
            modelBuilder.ApplyConfiguration(new AuditStepTypeMapper());
            modelBuilder.ApplyConfiguration(new BedTypeMapper());
            modelBuilder.ApplyConfiguration(new BillingAccountMapper());
            modelBuilder.ApplyConfiguration(new BillingAccountItemMapper());
            modelBuilder.ApplyConfiguration(new BillingAccountItemInvoiceHistoryMapper());
            modelBuilder.ApplyConfiguration(new BillingAccountItemTransferMapper());
            modelBuilder.ApplyConfiguration(new BillingAccountItemTypeMapper());
            modelBuilder.ApplyConfiguration(new BillingAccountTypeMapper());
            modelBuilder.ApplyConfiguration(new BillingInvoiceMapper());
            modelBuilder.ApplyConfiguration(new BillingInvoiceModelMapper());
            modelBuilder.ApplyConfiguration(new BillingInvoicePropertyMapper());
            modelBuilder.ApplyConfiguration(new BillingInvoicePropertySupportedTypeMapper());
            modelBuilder.ApplyConfiguration(new BillingInvoiceTypeMapper());
            modelBuilder.ApplyConfiguration(new BillingItemMapper());
            modelBuilder.ApplyConfiguration(new BillingItemCategoryMapper());
            modelBuilder.ApplyConfiguration(new BillingItemPaymentConditionMapper());
            modelBuilder.ApplyConfiguration(new BillingItemTypeMapper());
            modelBuilder.ApplyConfiguration(new BillingTaxMapper());
            modelBuilder.ApplyConfiguration(new BrandMapper());
            modelBuilder.ApplyConfiguration(new BusinessPartnerMapper());
            modelBuilder.ApplyConfiguration(new BusinessPartnerTypeMapper());
            modelBuilder.ApplyConfiguration(new BusinessSourceMapper());
            modelBuilder.ApplyConfiguration(new ChainMapper());
            modelBuilder.ApplyConfiguration(new ChainBusinessSourceMapper());
            modelBuilder.ApplyConfiguration(new ChainMarketSegmentMapper());
            modelBuilder.ApplyConfiguration(new ChannelMapper());
            modelBuilder.ApplyConfiguration(new LevelMapper());
            modelBuilder.ApplyConfiguration(new ChannelCodeMapper());
            modelBuilder.ApplyConfiguration(new CompanyMapper());
            modelBuilder.ApplyConfiguration(new CompanyClientMapper());
            modelBuilder.ApplyConfiguration(new CompanyClientChannelMapper());
            modelBuilder.ApplyConfiguration(new CompanyClientContactPersonMapper());
            modelBuilder.ApplyConfiguration(new CompanyContactPersonMapper());
            modelBuilder.ApplyConfiguration(new ContactInformationMapper());
            modelBuilder.ApplyConfiguration(new ContactInformationTypeMapper());
            modelBuilder.ApplyConfiguration(new CountryCurrencyMapper());
            modelBuilder.ApplyConfiguration(new CountryLanguageMapper());
            modelBuilder.ApplyConfiguration(new CountryPersonDocumentTypeMapper());
            modelBuilder.ApplyConfiguration(new CountrySubdivisionMapper());
            modelBuilder.ApplyConfiguration(new CountrySubdivisionTranslationMapper());
            modelBuilder.ApplyConfiguration(new CountrySubdvisionServiceMapper());
            modelBuilder.ApplyConfiguration(new CurrencyMapper());
            modelBuilder.ApplyConfiguration(new CurrencyExchangeMapper());
            modelBuilder.ApplyConfiguration(new DocumentMapper());
            modelBuilder.ApplyConfiguration(new DocumentTypeMapper());
            modelBuilder.ApplyConfiguration(new EmployeeMapper());
            modelBuilder.ApplyConfiguration(new FeatureGroupMapper());
            modelBuilder.ApplyConfiguration(new HousekeepingStatusMapper());
            modelBuilder.ApplyConfiguration(new HousekeepingStatusPropertyMapper());
            modelBuilder.ApplyConfiguration(new GuestRegistrationMapper());
            modelBuilder.ApplyConfiguration(new GuestRegistrationDocumentMapper());
            modelBuilder.ApplyConfiguration(new GeneralOccupationMapper());
            modelBuilder.ApplyConfiguration(new GratuityTypeMapper());
            modelBuilder.ApplyConfiguration(new GuestMapper());
            modelBuilder.ApplyConfiguration(new GuestReservationItemMapper());
            modelBuilder.ApplyConfiguration(new IntegrationPartnerMapper());
            modelBuilder.ApplyConfiguration(new IntegrationPartnerPropertyMapper());
            modelBuilder.ApplyConfiguration(new LevelRateMapper());
            modelBuilder.ApplyConfiguration(new LevelRateHeaderMapper());
            modelBuilder.ApplyConfiguration(new LocationMapper());
            modelBuilder.ApplyConfiguration(new LocationCategoryMapper());
            modelBuilder.ApplyConfiguration(new MarketSegmentMapper());
            modelBuilder.ApplyConfiguration(new MealPlanTypeMapper());
            modelBuilder.ApplyConfiguration(new OccupationMapper());
            modelBuilder.ApplyConfiguration(new ParameterTypeMapper());
            modelBuilder.ApplyConfiguration(new PaymentTypeMapper());
            modelBuilder.ApplyConfiguration(new PersonMapper());
            modelBuilder.ApplyConfiguration(new PersonInformationMapper());
            modelBuilder.ApplyConfiguration(new PersonInformationTypeMapper());
            modelBuilder.ApplyConfiguration(new PlasticMapper());
            modelBuilder.ApplyConfiguration(new PlasticBrandMapper());
            modelBuilder.ApplyConfiguration(new PlasticBrandPropertyMapper());
            modelBuilder.ApplyConfiguration(new PolicyTypeMapper());
            modelBuilder.ApplyConfiguration(new PropertyMapper());
            modelBuilder.ApplyConfiguration(new PropertyBaseRateHeaderHistoryMapper());
            modelBuilder.ApplyConfiguration(new PropertyBaseRateHistoryMapper());
            modelBuilder.ApplyConfiguration(new PropertyAuditProcessMapper());
            modelBuilder.ApplyConfiguration(new PropertyAuditProcessStepMapper());
            modelBuilder.ApplyConfiguration(new PropertyAuditProcessStepErrorMapper());
            modelBuilder.ApplyConfiguration(new PropertyBaseRateMapper());
            modelBuilder.ApplyConfiguration(new PropertyBusinessPartnerMapper());
            modelBuilder.ApplyConfiguration(new PropertyCompanyClientCategoryMapper());
            modelBuilder.ApplyConfiguration(new PropertyContactPersonMapper());
            modelBuilder.ApplyConfiguration(new PropertyContractMapper());
            modelBuilder.ApplyConfiguration(new PropertyCurrencyExchangeMapper());
            modelBuilder.ApplyConfiguration(new PropertyGuestPrefsMapper());
            modelBuilder.ApplyConfiguration(new PropertyGuestTypeMapper());
            modelBuilder.ApplyConfiguration(new PropertyMealPlanTypeMapper());
            modelBuilder.ApplyConfiguration(new PropertyMealPlanTypeRateMapper());
            modelBuilder.ApplyConfiguration(new PropertyMealPlanTypeRateHeaderMapper());
            modelBuilder.ApplyConfiguration(new PropertyMealPlanTypeRateHistoryMapper());
            modelBuilder.ApplyConfiguration(new PropertyParameterMapper());
            modelBuilder.ApplyConfiguration(new PropertyPolicyMapper());
            modelBuilder.ApplyConfiguration(new PropertyRateStrategyMapper());
            modelBuilder.ApplyConfiguration(new PropertyRateStrategyRoomTypeMapper());
            modelBuilder.ApplyConfiguration(new PropertyTypeMapper());
            modelBuilder.ApplyConfiguration(new RateTypeMapper());
            modelBuilder.ApplyConfiguration(new RatePlanMapper());
            modelBuilder.ApplyConfiguration(new RatePlanCommissionMapper());
            modelBuilder.ApplyConfiguration(new RatePlanCompanyClientMapper());
            modelBuilder.ApplyConfiguration(new RatePlanRoomTypeMapper());
            modelBuilder.ApplyConfiguration(new ReasonMapper());
            modelBuilder.ApplyConfiguration(new ReasonCategoryMapper());
            modelBuilder.ApplyConfiguration(new ReservationMapper());
            modelBuilder.ApplyConfiguration(new ReservationBudgetMapper());
            modelBuilder.ApplyConfiguration(new ReservationConfirmationMapper());
            modelBuilder.ApplyConfiguration(new ReservationItemMapper());
            modelBuilder.ApplyConfiguration(new RoomMapper());
            modelBuilder.ApplyConfiguration(new RoomBlockingMapper());
            modelBuilder.ApplyConfiguration(new RoomLayoutMapper());
            modelBuilder.ApplyConfiguration(new RoomTypeMapper());
            modelBuilder.ApplyConfiguration(new RoomTypeBedTypeMapper());
            modelBuilder.ApplyConfiguration(new RoomTypeInventoryMapper());
            modelBuilder.ApplyConfiguration(new StatusMapper());
            modelBuilder.ApplyConfiguration(new StatusCategoryMapper());
            modelBuilder.ApplyConfiguration(new SubscriptionMapper());
            modelBuilder.ApplyConfiguration(new TenantMapper());
            modelBuilder.ApplyConfiguration(new TourismTaxMapper());
            modelBuilder.ApplyConfiguration(new UserMapper());
            modelBuilder.ApplyConfiguration(new PropertyPremiseMapper());
            modelBuilder.ApplyConfiguration(new PropertyPremiseHeaderMapper());
            modelBuilder.ApplyConfiguration(new ReservationItemLaunchMapper());
            modelBuilder.ApplyConfiguration(new RoomOccupiedMapper());
            modelBuilder.ApplyConfiguration(new PropertyDocumentTypeMapper());
            modelBuilder.ApplyConfiguration(new SibaIntegrationMapper());
            modelBuilder.ApplyConfiguration(new ContactOwnerMapper());

            modelBuilder.ApplyConfiguration(new ExternalRegistrationMapper());
            modelBuilder.ApplyConfiguration(new ExternalGuestRegistrationMapper());

            modelBuilder.Entity<BillingAccount>().HasQueryFilter(b => EF.Property<Guid>(b, "TenantId") == _tenantId && !b.IsDeleted);
            modelBuilder.Entity<BillingAccountItem>().HasQueryFilter(b => EF.Property<Guid>(b, "TenantId") == _tenantId && !b.IsDeleted);
            modelBuilder.Entity<BillingAccountItemInvoiceHistory>().HasQueryFilter(b => EF.Property<Guid>(b, "TenantId") == _tenantId && !b.IsDeleted);
            modelBuilder.Entity<BillingAccountItemTransfer>().HasQueryFilter(b => EF.Property<Guid>(b, "TenantId") == _tenantId && !b.IsDeleted);
            modelBuilder.Entity<BillingInvoiceProperty>().HasQueryFilter(b => EF.Property<Guid>(b, "TenantId") == _tenantId && !b.IsDeleted);
            modelBuilder.Entity<BillingInvoicePropertySupportedType>().HasQueryFilter(b => EF.Property<Guid>(b, "TenantId") == _tenantId && !b.IsDeleted);
            modelBuilder.Entity<BillingItem>().HasQueryFilter(b => EF.Property<Guid>(b, "TenantId") == _tenantId && !b.IsDeleted);
            modelBuilder.Entity<BillingItemCategory>().HasQueryFilter(b => (EF.Property<Guid>(b, "TenantId") == _tenantId || EF.Property<Guid>(b, "TenantId") == null) && !b.IsDeleted);
            modelBuilder.Entity<BillingItemPaymentCondition>().HasQueryFilter(b => EF.Property<Guid>(b, "TenantId") == _tenantId && !b.IsDeleted);
            modelBuilder.Entity<BillingTax>().HasQueryFilter(b => EF.Property<Guid>(b, "TenantId") == _tenantId && !b.IsDeleted);
            modelBuilder.Entity<GuestRegistration>().HasQueryFilter(b => EF.Property<Guid>(b, "TenantId") == _tenantId && !b.IsDeleted);
            modelBuilder.Entity<GuestReservationItem>().HasQueryFilter(b => EF.Property<Guid>(b, "TenantId") == _tenantId && !b.IsDeleted);
            modelBuilder.Entity<IntegrationPartner>().HasQueryFilter(b => b.IsActive);
            modelBuilder.Entity<IntegrationPartnerProperty>().HasQueryFilter(b => b.IsActive);
            modelBuilder.Entity<HousekeepingStatusProperty>().HasQueryFilter(b => EF.Property<Guid>(b, "TenantId") == _tenantId && !b.IsDeleted);
            modelBuilder.Entity<PlasticBrandProperty>().HasQueryFilter(b => EF.Property<Guid>(b, "TenantId") == _tenantId && !b.IsDeleted);
            modelBuilder.Entity<Property>().HasQueryFilter(b => EF.Property<Guid>(b, "TenantId") == _tenantId && !b.IsDeleted);
            modelBuilder.Entity<PropertyBaseRate>().HasQueryFilter(b => EF.Property<Guid>(b, "TenantId") == _tenantId && !b.IsDeleted);
            modelBuilder.Entity<PropertyBusinessPartner>().HasQueryFilter(b => EF.Property<Guid>(b, "TenantId") == _tenantId && !b.IsDeleted);
            modelBuilder.Entity<PropertyBusinessSource>().HasQueryFilter(b => EF.Property<Guid>(b, "TenantId") == _tenantId && !b.IsDeleted);
            modelBuilder.Entity<PropertyCompanyClientCategory>().HasQueryFilter(b => !b.IsDeleted);
            modelBuilder.Entity<PropertyContactPerson>().HasQueryFilter(b => EF.Property<Guid>(b, "TenantId") == _tenantId && !b.IsDeleted);
            modelBuilder.Entity<PropertyCurrencyExchange>().HasQueryFilter(b => EF.Property<Guid>(b, "TenantId") == _tenantId && !b.IsDeleted);
            modelBuilder.Entity<PropertyGuestPrefs>().HasQueryFilter(b => EF.Property<Guid>(b, "TenantId") == _tenantId && !b.IsDeleted);
            modelBuilder.Entity<PropertyMealPlanType>().HasQueryFilter(b => EF.Property<Guid>(b, "TenantId") == _tenantId && !b.IsDeleted);
            modelBuilder.Entity<PropertyMealPlanTypeRate>().HasQueryFilter(b => EF.Property<Guid>(b, "TenantId") == _tenantId && !b.IsDeleted);
            modelBuilder.Entity<PropertyMealPlanTypeRateHeader>().HasQueryFilter(b => EF.Property<Guid>(b, "TenantId") == _tenantId && !b.IsDeleted);
            modelBuilder.Entity<PropertyMealPlanTypeRateHistory>().HasQueryFilter(b => EF.Property<Guid>(b, "TenantId") == _tenantId && !b.IsDeleted);
            modelBuilder.Entity<PropertyParameter>().HasQueryFilter(b => EF.Property<Guid>(b, "TenantId") == _tenantId && !b.IsDeleted);
            modelBuilder.Entity<PropertyPolicy>().HasQueryFilter(b => EF.Property<Guid>(b, "TenantId") == _tenantId && !b.IsDeleted);
            modelBuilder.Entity<RatePlan>().HasQueryFilter(b => EF.Property<Guid>(b, "TenantId") == _tenantId && !b.IsDeleted);
            modelBuilder.Entity<RatePlanCommission>().HasQueryFilter(b => EF.Property<Guid>(b, "TenantId") == _tenantId && !b.IsDeleted);
            modelBuilder.Entity<RatePlanCompanyClient>().HasQueryFilter(b => EF.Property<Guid>(b, "TenantId") == _tenantId && !b.IsDeleted);
            modelBuilder.Entity<RatePlanRoomType>().HasQueryFilter(b => EF.Property<Guid>(b, "TenantId") == _tenantId && !b.IsDeleted);
            modelBuilder.Entity<Reservation>().HasQueryFilter(b => EF.Property<Guid>(b, "TenantId") == _tenantId && !b.IsDeleted);
            modelBuilder.Entity<ReservationBudget>().HasQueryFilter(b => EF.Property<Guid>(b, "TenantId") == _tenantId && !b.IsDeleted);
            modelBuilder.Entity<ReservationConfirmation>().HasQueryFilter(b => EF.Property<Guid>(b, "TenantId") == _tenantId && !b.IsDeleted);
            modelBuilder.Entity<ReservationItem>().HasQueryFilter(b => EF.Property<Guid>(b, "TenantId") == _tenantId && !b.IsDeleted);
            modelBuilder.Entity<Room>().HasQueryFilter(b => EF.Property<Guid>(b, "TenantId") == _tenantId && !b.IsDeleted);
            modelBuilder.Entity<RoomBlocking>().HasQueryFilter(b => EF.Property<Guid>(b, "TenantId") == _tenantId && !b.IsDeleted);
            modelBuilder.Entity<RoomTypeInventory>().HasQueryFilter(b => EF.Property<Guid>(b, "TenantId") == _tenantId && !b.IsDeleted);
            modelBuilder.Entity<Channel>().HasQueryFilter(b => EF.Property<Guid>(b, "TenantId") == _tenantId && !b.IsDeleted);
            modelBuilder.Entity<Level>().HasQueryFilter(b => EF.Property<Guid>(b, "TenantId") == _tenantId && !b.IsDeleted);
            modelBuilder.Entity<LevelRate>().HasQueryFilter(b => EF.Property<Guid>(b, "TenantId") == _tenantId && !b.IsDeleted);
            modelBuilder.Entity<LevelRateHeader>().HasQueryFilter(b => EF.Property<Guid>(b, "TenantId") == _tenantId && !b.IsDeleted);
            modelBuilder.Entity<PropertyBaseRateHeaderHistory>().HasQueryFilter(b => EF.Property<Guid>(b, "TenantId") == _tenantId && !b.IsDeleted);
            modelBuilder.Entity<PropertyBaseRateHistory>().HasQueryFilter(b => EF.Property<Guid>(b, "TenantId") == _tenantId && !b.IsDeleted);
            modelBuilder.Entity<PropertyRateStrategy>().HasQueryFilter(b => EF.Property<Guid>(b, "TenantId") == _tenantId && !b.IsDeleted);
            modelBuilder.Entity<PropertyRateStrategyRatePlan>().HasQueryFilter(b => EF.Property<Guid>(b, "TenantId") == _tenantId && !b.IsDeleted);
            modelBuilder.Entity<PropertyRateStrategyRoomType>().HasQueryFilter(b => EF.Property<Guid>(b, "TenantId") == _tenantId && !b.IsDeleted);
            modelBuilder.Entity<PropertyPremise>().HasQueryFilter(b => EF.Property<Guid>(b, "TenantId") == _tenantId && !b.IsDeleted);
            modelBuilder.Entity<PropertyPremiseHeader>().HasQueryFilter(b => EF.Property<Guid>(b, "TenantId") == _tenantId && !b.IsDeleted);
            modelBuilder.Entity<TourismTax>().HasQueryFilter(b => EF.Property<Guid>(b, "TenantId") == _tenantId && !b.IsDeleted);
            modelBuilder.Entity<RoomOccupied>().HasQueryFilter(b => EF.Property<Guid>(b, "TenantId") == _tenantId && !b.IsDeleted);


            modelBuilder.Entity<ExternalRegistration>().HasQueryFilter(b => EF.Property<Guid>(b, "TenantId") == _tenantId && !b.IsDeleted);
            modelBuilder.Entity<ExternalGuestRegistration>().HasQueryFilter(b => EF.Property<Guid>(b, "TenantId") == _tenantId && !b.IsDeleted);

            modelBuilder.Entity<Subscription>().HasQueryFilter(b => !b.IsDeleted);
            modelBuilder.Entity<PropertyMarketSegment>().HasQueryFilter(b => !b.IsDeleted);
            modelBuilder.Entity<CurrencyExchange>().HasQueryFilter(b => !b.IsDeleted);
            modelBuilder.Entity<Currency>().HasQueryFilter(b => !b.IsDeleted);
            modelBuilder.Entity<CountryCurrency>().HasQueryFilter(b => !b.IsDeleted);
            modelBuilder.Entity<ChainMarketSegment>().HasQueryFilter(b => !b.IsDeleted);
            modelBuilder.Entity<ChainBusinessSource>().HasQueryFilter(b => !b.IsDeleted);
            modelBuilder.Entity<GuestRegistrationDocument>().HasQueryFilter(b => !b.IsDeleted);
            modelBuilder.Entity<BusinessPartner>().HasQueryFilter(b => !b.IsDeleted);
            modelBuilder.Entity<ApplicationParameter>().HasQueryFilter(b => !b.IsDeleted);
            modelBuilder.Entity<PropertyContract>().HasQueryFilter(b => !b.IsDeleted);
            modelBuilder.Entity<PropertyPremise>().HasQueryFilter(b => !b.IsDeleted);
            modelBuilder.Entity<PropertyPremiseHeader>().HasQueryFilter(b => !b.IsDeleted);

            modelBuilder.Entity<ExternalRegistration>().HasQueryFilter(b => !b.IsDeleted);
            modelBuilder.Entity<ExternalGuestRegistration>().HasQueryFilter(b => !b.IsDeleted);

            base.OnModelCreating(modelBuilder.EnableAutoHistory(null));
        }


        public virtual void FillAuditFiedls()
        {
            var utcNowAuditDate = DateTime.UtcNow.ToZonedDateTimeLoggedUser();

            var ignoreAuditedEntity = _serviceProvider.GetRequiredService<IIgnoreAuditedEntity>();

            var entriesWithBase = ChangeTracker.Entries();

            var FullAuditedEntries = entriesWithBase
                .Where(entry => (entry.Metadata.ClrType.BaseType != null && entry.Metadata.ClrType.BaseType.Name.Contains("ThexFullAuditedEntity")) ||
                                (entry.Metadata.ClrType.BaseType.BaseType != null && entry.Metadata.ClrType.BaseType.BaseType.Name.Contains("ThexFullAuditedEntity")));

            if (FullAuditedEntries != null)
                foreach (EntityEntry entityEntry in FullAuditedEntries)
                {
                    var entity = entityEntry.Entity;

                    if (ignoreAuditedEntity.Any(entity))
                        continue;

                    switch (entityEntry.State)
                    {
                        case EntityState.Added:
                            entity.GetType().GetProperty("CreationTime").SetValue(entity, utcNowAuditDate, null);
                            entity.GetType().GetProperty("LastModificationTime").SetValue(entity, utcNowAuditDate, null);
                            entity.GetType().GetProperty("CreatorUserId").SetValue(entity, _userUid, null);
                            break;
                        case EntityState.Modified:
                            entityEntry.Property("CreationTime").IsModified = false;
                            entityEntry.Property("CreatorUserId").IsModified = false;

                            entity.GetType().GetProperty("LastModificationTime").SetValue(entity, utcNowAuditDate, null);
                            entity.GetType().GetProperty("LastModifierUserId").SetValue(entity, _userUid, null);
                            break;
                        case EntityState.Deleted:
                            entityEntry.Property("CreationTime").IsModified = false;
                            entityEntry.Property("CreatorUserId").IsModified = false;
                            entityEntry.Property("LastModificationTime").IsModified = false;
                            entityEntry.Property("LastModifierUserId").IsModified = false;

                            entity.GetType().GetProperty("DeletionTime").SetValue(entity, utcNowAuditDate, null);
                            entity.GetType().GetProperty("DeleterUserId").SetValue(entity, _userUid, null);
                            entity.GetType().GetProperty("IsDeleted").SetValue(entity, true);
                            entityEntry.State = EntityState.Modified;
                            break;
                    }
                }


            var MultiTentantEntries = entriesWithBase
                .Where(entry => (entry.Metadata.ClrType.BaseType != null && entry.Metadata.ClrType.BaseType.Name.Contains("ThexMultiTenantFullAuditedEntity")) ||
                                (entry.Metadata.ClrType.BaseType.BaseType != null && entry.Metadata.ClrType.BaseType.BaseType.Name.Contains("ThexMultiTenantFullAuditedEntity")));

            if (MultiTentantEntries != null)
                foreach (EntityEntry entityEntry in MultiTentantEntries)
                {
                    var entity = entityEntry.Entity;

                    if (ignoreAuditedEntity.Any(entity))
                        continue;

                    switch (entityEntry.State)
                    {
                        case EntityState.Added:
                            entity.GetType().GetProperty("TenantId").SetValue(entity, _tenantId, null);
                            break;
                        case EntityState.Modified:
                        case EntityState.Deleted:
                            entityEntry.Property("TenantId").IsModified = false;
                            break;
                    }
                }
        }

        public override int SaveChanges()
        {
            FillAuditFiedls();

            _genericLog.AddLog(_genericLog.DefaultBuilder
                       .WithMessage(this.EnsureAutoHistory())
                       .WithDetailedMessage(AppConsts.DatabaseModification)
                       .AsInformation()
                       .Build());

            return base.SaveChanges();
        }

        public override async Task<int> SaveChangesAsync(CancellationToken cancellationToken = default(CancellationToken))
        {
            FillAuditFiedls();

            _genericLog.AddLog(_genericLog.DefaultBuilder
                      .WithMessage(this.EnsureAutoHistory())
                      .WithDetailedMessage(AppConsts.DatabaseModification)
                      .AsInformation()
                      .Build());

            return (await base.SaveChangesAsync(true, cancellationToken));
        }
    }
}
