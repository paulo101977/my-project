﻿
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;
using Thex.Domain.Entities;

namespace Thex.Infra.Context.Builders
{
    public class RoomOccupiedMapper : IEntityTypeConfiguration<RoomOccupied>
    {
        public void Configure(EntityTypeBuilder<RoomOccupied> builder)
        {

            builder.ToTable("RoomOccupied");

            builder.HasAnnotation("Relational:TableName", "RoomOccupied");

            builder.HasIndex(e => e.RoomId)
                .HasName("x_RoomOccupied_RoomId");

            builder.HasIndex(e => e.ReservationId)
                .HasName("x_RoomOccupied_ReservationId");

            builder.HasIndex(e => e.TenantId)
                .HasName("x_RoomOccupied_TenantId");

            builder.Property(e => e.Id)
                .ValueGeneratedNever()
                .HasAnnotation("Relational:ColumnName", "Id");


            builder.Property(e => e.PropertyId).HasAnnotation("Relational:ColumnName", "PropertyId");

            builder.Property(e => e.TenantId).HasAnnotation("Relational:ColumnName", "TenantId");

            builder.Property(e => e.IsDeleted).HasAnnotation("Relational:ColumnName", "IsDeleted");

            builder.Property(e => e.CreationTime)
                .HasColumnType("datetime")
                .HasDefaultValueSql("(getutcdate())")
                .HasAnnotation("Relational:ColumnName", "CreationTime");

            builder.Property(e => e.CreatorUserId).HasAnnotation("Relational:ColumnName", "CreatorUserId");

            builder.Property(e => e.LastModificationTime)
               .HasColumnType("datetime")
               .HasDefaultValueSql("(getutcdate())")
               .HasAnnotation("Relational:ColumnName", "LastModificationTime");

            builder.Property(e => e.LastModifierUserId).HasAnnotation("Relational:ColumnName", "LastModifierUserId");

            builder.Property(e => e.DeletionTime)
             .HasColumnType("datetime")
             .HasAnnotation("Relational:ColumnName", "DeletionTime");

            builder.Property(e => e.DeleterUserId).HasAnnotation("Relational:ColumnName", "DeleterUserId");

            builder.HasOne(d => d.Property)
                .WithMany(p => p.RoomOccupiedList)
                .HasForeignKey(d => d.PropertyId)
                .HasConstraintName("FK_RoomOccupied_PropertyId");

            builder.HasOne(d => d.Room)
              .WithMany(p => p.RoomOccupiedList)
              .HasForeignKey(d => d.RoomId)
              .HasConstraintName("FK_RoomOccupied_RoomId");

            builder.HasOne(d => d.Reservation)
              .WithMany(p => p.RoomOccupiedList)
              .HasForeignKey(d => d.ReservationId)
              .HasConstraintName("FK_RoomRoomOccupied_ReservationId");


        }
    }
}