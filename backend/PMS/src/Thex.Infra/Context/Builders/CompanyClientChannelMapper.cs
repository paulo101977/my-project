﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Thex.Domain.Entities;

namespace Thex.Infra.Context.Builders
{
    public class CompanyClientChannelMapper : IEntityTypeConfiguration<CompanyClientChannel>
    {
        public void Configure(EntityTypeBuilder<CompanyClientChannel> builder)
        {
            builder.ToTable("CompanyClientChannel");

            builder.Property(e => e.Id)
                .ValueGeneratedNever()
                .HasAnnotation("Relational:ColumnName", "CompanyClientChannelId");

            builder.HasAnnotation("Relational:TableName", "CompanyClientChannel");

            builder.HasIndex(e => e.ChannelId)
               .HasName("x_CompanyClientChannel_ChannelId");

            builder.HasIndex(e => e.CompanyClientId)
                .HasName("x_CompanyClientChannel_CompanyClientId");

            builder.Property(e => e.IsActive).HasAnnotation("Relational:ColumnName", "IsActive");

            builder.HasOne(d => d.Channel)
                .WithMany(p => p.CompanyClientChannelList)
                .HasForeignKey(d => d.ChannelId)
                .HasConstraintName("CompanyClientChannel_Channel");

            builder.HasOne(d => d.CompanyClient)
                .WithMany(p => p.CompanyClientChannelList)
                .HasForeignKey(d => d.CompanyClientId)
                .HasConstraintName("FK_CompanyClientChannel_Company");
        }    
    }
}
