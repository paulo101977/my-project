﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Thex.Domain.Entities;

namespace Thex.Infra.Context.Builders
{
    public class IntegrationPartnerMapper : IEntityTypeConfiguration<IntegrationPartner>
    {
        public void Configure(EntityTypeBuilder<IntegrationPartner> builder)
        {

            builder.ToTable("IntegrationPartner");

            builder.HasAnnotation("Relational:TableName", "IntegrationPartner");

            builder.Property(e => e.Id).HasAnnotation("Relational:ColumnName", "IntegrationPartnerId");

            builder.Property(e => e.TokenClient)
                .HasColumnName("TokenClient")
                .HasMaxLength(150)
                .IsUnicode(false)
                .HasAnnotation("Relational:ColumnName", "TokenClient");

            builder.Property(e => e.TokenApplication)
                .HasColumnName("TokenApplication")
                .HasMaxLength(150)
                .IsUnicode(false)
                .HasAnnotation("Relational:ColumnName", "TokenApplication");

            builder.Property(e => e.IntegrationPartnerName)
                .HasColumnName("IntegrationPartnerName")
                .IsRequired()
                .HasMaxLength(100)
                .IsUnicode(false)
                .HasAnnotation("Relational:ColumnName", "IntegrationPartnerName");

            builder.Property(e => e.IntegrationPartnerType)
               .HasColumnName("IntegrationPartnerType")
               .IsRequired()
               .HasAnnotation("Relational:ColumnName", "IntegrationPartnerType");

            builder.Property(e => e.IsActive).HasAnnotation("Relational:ColumnName", "IsActive");
        }
    }
}
