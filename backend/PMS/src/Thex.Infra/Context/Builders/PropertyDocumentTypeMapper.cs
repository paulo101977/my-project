﻿
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;
using Thex.Domain.Entities;

namespace Thex.Infra.Context.Builders
{
    public class PropertyDocumentTypeMapper : IEntityTypeConfiguration<PropertyDocumentType>
    {
        public void Configure(EntityTypeBuilder<PropertyDocumentType> builder)
        {

            builder.ToTable("PropertyDocumentType");

            builder.HasAnnotation("Relational:TableName", "PropertyDocumentType");

            builder.HasIndex(e => e.Id)
                .HasName("x_PropertyDocumentType_Id");

            builder.HasIndex(e => e.PropertyId)
                .HasName("x_PropertyDocumentType_PropertyId");

            builder.HasIndex(e => e.DocumentTypeId)
                .HasName("x_PropertyDocumentType_DocumentTypeId");

            builder.Property(e => e.Id).HasAnnotation("Relational:ColumnName", "Id");

            builder.Property(e => e.PropertyId).HasAnnotation("Relational:ColumnName", "PropertyId");

            builder.Property(e => e.DocumentTypeId).HasAnnotation("Relational:ColumnName", "DocumentTypeId");

            builder.HasOne(d => d.Property)
                .WithMany(p => p.PropertyDocumentTypeList)
                .HasForeignKey(d => d.PropertyId)
                .HasConstraintName("FK_PropertyDocumentType_PropertyId");

            builder.HasOne(d => d.DocumentType)
              .WithMany(p => p.PropertyDocumentTypeList)
              .HasForeignKey(d => d.DocumentTypeId)
              .HasConstraintName("FK_PropertyDocumentType_DocumentTypeId");


        }
    }
}