﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a Crudzilla tool.
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;
using Thex.Domain.Entities;

namespace Thex.Infra.Context.Builders
{
    public class PropertyBusinessSourceMapper : IEntityTypeConfiguration<PropertyBusinessSource>
    {
        public void Configure(EntityTypeBuilder<PropertyBusinessSource> builder)
        {
            
            builder.ToTable("PropertyBusinessSource");

            builder.HasAnnotation("Relational:TableName", "PropertyBusinessSource");

            builder.HasIndex(e => e.ChainIdBusinessSourceId)
                .HasName("x_PropertyBusinessSource_ChainIdBusinessSourceId");

            builder.HasIndex(e => e.PropertyId)
                .HasName("x_PropertyBusinessSource_PropertyId");

            builder.HasIndex(e => e.TenantId)
                .HasName("x_PropertyBusinessSource_TenantId");

            builder.HasIndex(e => new { e.ChainId, e.BusinessSourceId })
                .HasName("x_PropertyBusinessSource_ChainId_BusinessSourceId");

            builder.HasIndex(e => new { e.ChainId, e.PropertyId, e.BusinessSourceId })
                .HasName("UK_PropertyBusinessSource")
                .IsUnique();

            builder.Property(e => e.Id)
                .HasAnnotation("Relational:ColumnName", "PropertyBusinessSourceId");

            builder.Property(e => e.BusinessSourceId).HasAnnotation("Relational:ColumnName", "BusinessSourceId");

            builder.Property(e => e.ChainId).HasAnnotation("Relational:ColumnName", "ChainId");

            builder.Property(e => e.ChainIdBusinessSourceId).HasAnnotation("Relational:ColumnName", "ChainIdBusinessSourceId");

            builder.Property(e => e.CreationTime)
                .HasColumnType("datetime")
                .HasDefaultValueSql("(getutcdate())")
                .HasAnnotation("Relational:ColumnName", "CreationTime");

            builder.Property(e => e.CreatorUserId).HasAnnotation("Relational:ColumnName", "CreatorUserId");

            builder.Property(e => e.DeleterUserId).HasAnnotation("Relational:ColumnName", "DeleterUserId");

            builder.Property(e => e.DeletionTime)
                .HasColumnType("datetime")
                .HasAnnotation("Relational:ColumnName", "DeletionTime");

            builder.Property(e => e.IsActive)
                .HasDefaultValueSql("((1))")
                .HasAnnotation("Relational:ColumnName", "IsActive");

            builder.Property(e => e.IsDeleted).HasAnnotation("Relational:ColumnName", "IsDeleted");

            builder.Property(e => e.LastModificationTime)
                .HasColumnType("datetime")
                .HasDefaultValueSql("(getutcdate())")
                .HasAnnotation("Relational:ColumnName", "LastModificationTime");

            builder.Property(e => e.LastModifierUserId).HasAnnotation("Relational:ColumnName", "LastModifierUserId");

            builder.Property(e => e.PropertyId).HasAnnotation("Relational:ColumnName", "PropertyId");

            builder.Property(e => e.TenantId).HasAnnotation("Relational:ColumnName", "TenantId");

            builder.HasOne(d => d.ChainBusinessSource)
                .WithMany(p => p.PropertyBusinessSourceList)
                .HasForeignKey(d => d.ChainIdBusinessSourceId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_PropertyBusinessSourcee_ChainBusinessSource_1");

            builder.HasOne(d => d.Property)
                .WithMany(p => p.PropertyBusinessSourceList)
                .HasForeignKey(d => d.PropertyId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_PropertyBusinessSource_Property");

        }
    }
}
