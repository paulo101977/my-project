﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Thex.Domain.Entities;

namespace Thex.Infra.Context.Builders
{
    public class ContactOwnerMapper : IEntityTypeConfiguration<ContactOwner>
    {
        public void Configure(EntityTypeBuilder<ContactOwner> builder)
        {
            builder.Property(e => e.Id)
              .HasAnnotation("Relational:ColumnName", "ContactOwnerId");

            builder.HasKey(e => new { e.PersonId, e.OwnerId });

            builder.ToTable("ContactOwner");

            builder.HasAnnotation("Relational:TableName", "ContactOwner"); 

            builder.HasOne(d => d.Occupation)
                .WithMany(p => p.ContactOwnerList)
                .HasForeignKey(d => d.OccupationId)
                .HasConstraintName("FK_ContactOwner_Occupation");

            builder.HasOne(d => d.Person)
                .WithMany(p => p.ContactOwnerList)
                .HasForeignKey(d => d.PersonId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_ContactOwner_Person");
        }
    }
}
