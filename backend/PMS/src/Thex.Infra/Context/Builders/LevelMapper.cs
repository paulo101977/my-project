﻿
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;
using Thex.Domain.Entities;

namespace Thex.Infra.Context.Builders
{
    public class LevelMapper : IEntityTypeConfiguration<Level>
    {
        public void Configure(EntityTypeBuilder<Level> builder)
        {

            builder.ToTable("Level");

            builder.HasAnnotation("Relational:TableName", "Level");

            builder.HasIndex(e => e.LevelCode)
                .HasName("x_Level_LevelCode");

            builder.HasIndex(e => e.TenantId)
                .HasName("x_Level_TenantId");

            builder.Property(e => e.Id)
                .ValueGeneratedNever()
                .HasAnnotation("Relational:ColumnName", "LevelId");

            builder.Property(e => e.LevelCode).HasAnnotation("Relational:ColumnName", "LevelCode");

            builder.Property(e => e.PropertyId).HasAnnotation("Relational:ColumnName", "PropertyId");

            builder.Property(e => e.Description)
              .IsRequired()
              .HasMaxLength(60)
              .IsUnicode(false)
              .HasAnnotation("Relational:ColumnName", "Description");

            builder.Property(e => e.Order).HasAnnotation("Relational:ColumnName", "Order");

            builder.Property(e => e.IsActive).HasAnnotation("Relational:ColumnName", "IsActive");

            builder.Property(e => e.TenantId).HasAnnotation("Relational:ColumnName", "TenantId");

            builder.Property(e => e.IsDeleted).HasAnnotation("Relational:ColumnName", "IsDeleted");

            builder.Property(e => e.CreationTime)
                .HasColumnType("datetime")
                .HasDefaultValueSql("(getutcdate())")
                .HasAnnotation("Relational:ColumnName", "CreationTime");

            builder.Property(e => e.CreatorUserId).HasAnnotation("Relational:ColumnName", "CreatorUserId");

            builder.Property(e => e.LastModificationTime)
               .HasColumnType("datetime")
               .HasDefaultValueSql("(getutcdate())")
               .HasAnnotation("Relational:ColumnName", "LastModificationTime");

            builder.Property(e => e.LastModifierUserId).HasAnnotation("Relational:ColumnName", "LastModifierUserId");

            builder.Property(e => e.DeletionTime)
             .HasColumnType("datetime")
             .HasAnnotation("Relational:ColumnName", "DeletionTime");

            builder.Property(e => e.DeleterUserId).HasAnnotation("Relational:ColumnName", "DeleterUserId");

            builder.HasOne(d => d.Property)
                .WithMany(p => p.LevelList)
                .HasForeignKey(d => d.PropertyId)
                .HasConstraintName("FK_Level_PropertyId");
        }
    }
}