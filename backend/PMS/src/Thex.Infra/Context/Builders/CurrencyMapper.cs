﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a Crudzilla tool.
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;
using Thex.Domain.Entities;

namespace Thex.Infra.Context.Builders
{
    public class CurrencyMapper : IEntityTypeConfiguration<Currency>
    {
        public void Configure(EntityTypeBuilder<Currency> builder)
        {
            builder.HasKey(e => e.Id);

            builder.ToTable("Currency");

            builder.HasAnnotation("Relational:TableName", "Currency");

            /*
            builder.HasIndex(e => e.CountrySubdivisionId)
                .HasName("x_Currency_CountrySubdivisionId");
            */

            builder.Property(e => e.Id)
                .HasAnnotation("Relational:ColumnName", "CurrencyId");

            builder.Property(e => e.AlphabeticCode)
                .IsRequired()
                .HasMaxLength(10)
                .IsUnicode(false)
                .HasAnnotation("Relational:ColumnName", "AlphabeticCode");

            //builder.Property(e => e.CountrySubdivisionId).HasAnnotation("Relational:ColumnName", "CountrySubdivisionId");

            builder.Property(e => e.CreationTime)
                .HasColumnType("datetime")
                .HasDefaultValueSql("(getutcdate())")
                .HasAnnotation("Relational:ColumnName", "CreationTime");

            builder.Property(e => e.CreatorUserId).HasAnnotation("Relational:ColumnName", "CreatorUserId");

            builder.Property(e => e.CurrencyName)
                .IsRequired()
                .HasMaxLength(200)
                .IsUnicode(false)
                .HasAnnotation("Relational:ColumnName", "CurrencyName");

            builder.Property(e => e.DeleterUserId).HasAnnotation("Relational:ColumnName", "DeleterUserId");

            builder.Property(e => e.DeletionTime)
                .HasColumnType("datetime")
                .HasAnnotation("Relational:ColumnName", "DeletionTime");

            builder.Property(e => e.ExchangeRate)
                .HasColumnType("numeric(18, 4)")
                .HasAnnotation("Relational:ColumnName", "ExchangeRate");

            builder.Property(e => e.IsDeleted).HasAnnotation("Relational:ColumnName", "IsDeleted");

            builder.Property(e => e.LastModificationTime)
                .HasColumnType("datetime")
                .HasDefaultValueSql("(getutcdate())")
                .HasAnnotation("Relational:ColumnName", "LastModificationTime");

            builder.Property(e => e.LastModifierUserId).HasAnnotation("Relational:ColumnName", "LastModifierUserId");

            builder.Property(e => e.MinorUnit).HasAnnotation("Relational:ColumnName", "MinorUnit");

            builder.Property(e => e.NumnericCode)
                .IsRequired()
                .HasMaxLength(10)
                .IsUnicode(false)
                .HasAnnotation("Relational:ColumnName", "NumnericCode");

            builder.Property(e => e.Symbol)
                .IsRequired()
                .HasMaxLength(10)
                .IsUnicode(false)
                .HasAnnotation("Relational:ColumnName", "Symbol");

            builder.Property(e => e.TwoLetterIsoCode)
                .HasColumnType("char(2)")
                .HasAnnotation("Relational:ColumnName", "TwoLetterIsoCode");
        }
    }
}
