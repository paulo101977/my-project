﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a Crudzilla tool.
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;
using Thex.Domain.Entities;

namespace Thex.Infra.Context.Builders
{
    public class CompanyClientMapper : IEntityTypeConfiguration<CompanyClient>
    {
        public void Configure(EntityTypeBuilder<CompanyClient> builder)
        {
            
            builder.ToTable("CompanyClient");

            builder.HasAnnotation("Relational:TableName", "CompanyClient");

            builder.HasIndex(e => e.CompanyId)
                .HasName("x_CompanyClient_CompanyId");

            builder.HasIndex(e => e.PersonId)
                .HasName("x_CompanyClient_PersonId");

            builder.Property(e => e.Id)
                .HasAnnotation("Relational:ColumnName", "CompanyClientId");

            builder.Property(e => e.CompanyId).HasAnnotation("Relational:ColumnName", "CompanyId");

            builder.Property(e => e.IsAcquirer).HasAnnotation("Relational:ColumnName", "IsAcquirer");

            builder.Property(e => e.IsActive)
                .HasDefaultValueSql("((1))")
                .HasAnnotation("Relational:ColumnName", "IsActive");

            builder.Property(e => e.PersonId).HasAnnotation("Relational:ColumnName", "PersonId");

            builder.Property(e => e.ShortName)
                .IsRequired()
                .HasMaxLength(50)
                .IsUnicode(false)
                .HasAnnotation("Relational:ColumnName", "ShortName");

            builder.Property(e => e.TradeName)
                .IsRequired()
                .HasMaxLength(200)
                .IsUnicode(false)
                .HasAnnotation("Relational:ColumnName", "TradeName");

            builder.HasOne(d => d.Company)
                .WithMany(p => p.CompanyClientList)
                .HasForeignKey(d => d.CompanyId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_CompanyClient_Company");

            builder.HasOne(d => d.Person)
                .WithMany(p => p.CompanyClientList)
                .HasForeignKey(d => d.PersonId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_CompanyClient_Person");

        }
    }
}
