﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using Thex.Domain.Entities;

namespace Thex.Infra.Context.Builders
{
    public class GuestRegistrationDocumentMapper : IEntityTypeConfiguration<GuestRegistrationDocument>
    {
        public void Configure(EntityTypeBuilder<GuestRegistrationDocument> builder)
        {

            builder.ToTable("GuestRegistrationDocument");

            builder.HasAnnotation("Relational:TableName", "GuestRegistrationDocument");

            builder.HasIndex(e => e.DocumentTypeId)
                .HasName("x_GuestRegistrationDocument_DocumentTypeId");

            builder.HasIndex(e => e.GuestRegistrationId)
                .HasName("x_GuestRegistrationDocument_GuestRegistrationId");

            builder.Property(e => e.Id)
                .ValueGeneratedNever()
                .HasAnnotation("Relational:ColumnName", "GuestRegistrationDocumentId");

            builder.Property(e => e.CreationTime)
                .HasColumnType("datetime")
                .HasDefaultValueSql("(getutcdate())")
                .HasAnnotation("Relational:ColumnName", "CreationTime");

            builder.Property(e => e.CreatorUserId).HasAnnotation("Relational:ColumnName", "CreatorUserId");

            builder.Property(e => e.DeleterUserId).HasAnnotation("Relational:ColumnName", "DeleterUserId");

            builder.Property(e => e.DeletionTime)
                .HasColumnType("datetime")
                .HasAnnotation("Relational:ColumnName", "DeletionTime");

            builder.Property(e => e.IsDeleted).HasAnnotation("Relational:ColumnName", "IsDeleted");

            builder.Property(e => e.LastModificationTime)
                .HasColumnType("datetime")
                .HasDefaultValueSql("(getutcdate())")
                .HasAnnotation("Relational:ColumnName", "LastModificationTime");

            builder.Property(e => e.LastModifierUserId).HasAnnotation("Relational:ColumnName", "LastModifierUserId");

            builder.HasOne(d => d.DocumentType)
                .WithMany(p => p.GuestRegistrationDocumentList)
                .HasForeignKey(d => d.DocumentTypeId)
                .HasConstraintName("FK_GuestRegistrationDocument_DocumentType");

            builder.HasOne(d => d.GuestRegistration)
                .WithMany(p => p.GuestRegistrationDocumentList)
                .HasForeignKey(d => d.GuestRegistrationId)
                .HasConstraintName("FK_GuestRegistrationDocument_GuestRegistration");
        }
    }
}
