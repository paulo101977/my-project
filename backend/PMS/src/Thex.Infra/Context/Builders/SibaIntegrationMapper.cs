﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Thex.Domain.Entities;

namespace Thex.Infra.Context.Builders
{
    public class SibaIntegrationMapper : IEntityTypeConfiguration<SibaIntegration>
    {
        public void Configure(EntityTypeBuilder<SibaIntegration> builder)
        {
            builder.ToTable("SibaIntegration");

            builder.HasAnnotation("Relational:TableName", "SibaIntegration");

            builder.Property(e => e.Id)
                .HasAnnotation("Relational:ColumnName", "SibaIntegrationId");

            builder.Property(e => e.PropertyUId).HasAnnotation("Relational:ColumnName", "PropertyUId");

            builder.Property(e => e.IntegrationFileNumber).HasAnnotation("Relational:ColumnName", "IntegrationFileNumber");
        }
    }
}
