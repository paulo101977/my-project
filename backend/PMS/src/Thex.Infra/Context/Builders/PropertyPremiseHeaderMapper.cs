﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a Crudzilla tool.
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;
using Thex.Domain.Entities;

namespace Thex.Infra.Context.Builders
{
    public class PropertyPremiseHeaderMapper : IEntityTypeConfiguration<PropertyPremiseHeader>
    {
        public void Configure(EntityTypeBuilder<PropertyPremiseHeader> builder)
        {
            
            builder.ToTable("PropertyPremiseHeader");

            builder.HasAnnotation("Relational:TableName", "PropertyPremiseHeader");

            builder.HasIndex(e => e.CurrencyId)
                .HasName("x_PropertyPremiseHeader_CurrencyId");

            builder.HasIndex(e => e.PropertyId)
                .HasName("x_PropertyPremiseHeader_PropertyId");

            builder.HasIndex(e => e.RoomTypeReferenceId)
                .HasName("x_PropertyPremiseHeader_RoomTypeReferenceId");

            builder.HasIndex(e => e.TenantId)
                .HasName("x_PropertyPremiseHeader_TenantId");

            builder.HasIndex(e => new { e.Name, e.PropertyId, e.IsDeleted, e.DeletionTime })
                .HasName("UK_PropertyPremiseHeader")
                .IsUnique();

            builder.Property(e => e.Id)
                .ValueGeneratedNever()
                .HasAnnotation("Relational:ColumnName", "PropertyPremiseHeaderId");

            builder.Property(e => e.CreationTime)
                .HasColumnType("datetime")
                .HasDefaultValueSql("(getutcdate())")
                .HasAnnotation("Relational:ColumnName", "CreationTime");

            builder.Property(e => e.CreatorUserId).HasAnnotation("Relational:ColumnName", "CreatorUserId");

            builder.Property(e => e.CurrencyId).HasAnnotation("Relational:ColumnName", "CurrencyId");

            builder.Property(e => e.DeleterUserId).HasAnnotation("Relational:ColumnName", "DeleterUserId");

            builder.Property(e => e.DeletionTime)
                .HasColumnType("datetime")
                .HasAnnotation("Relational:ColumnName", "DeletionTime");

            builder.Property(e => e.FinalDate)
                .HasColumnType("datetime")
                .HasAnnotation("Relational:ColumnName", "FinalDate");

            builder.Property(e => e.InitialDate)
                .HasColumnType("datetime")
                .HasAnnotation("Relational:ColumnName", "InitialDate");

            builder.Property(e => e.IsActive).HasAnnotation("Relational:ColumnName", "IsActive");

            builder.Property(e => e.IsDeleted).HasAnnotation("Relational:ColumnName", "IsDeleted");

            builder.Property(e => e.LastModificationTime)
                .HasColumnType("datetime")
                .HasDefaultValueSql("(getutcdate())")
                .HasAnnotation("Relational:ColumnName", "LastModificationTime");

            builder.Property(e => e.LastModifierUserId).HasAnnotation("Relational:ColumnName", "LastModifierUserId");

            builder.Property(e => e.Name)
                .IsRequired()
                .HasMaxLength(200)
                .IsUnicode(false)
                .HasAnnotation("Relational:ColumnName", "Name");

            builder.Property(e => e.PaxReference).HasAnnotation("Relational:ColumnName", "PaxReference");

            builder.Property(e => e.PropertyId).HasAnnotation("Relational:ColumnName", "PropertyId");

            builder.Property(e => e.RoomTypeReferenceId).HasAnnotation("Relational:ColumnName", "RoomTypeReferenceId");

            builder.Property(e => e.TenantId).HasAnnotation("Relational:ColumnName", "TenantId");

        }
    }
}
