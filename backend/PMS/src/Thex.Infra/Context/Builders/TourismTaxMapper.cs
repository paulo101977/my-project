﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Thex.Domain.Entities;

namespace Thex.Infra.Context.Builders
{
    public class TourismTaxMapper : IEntityTypeConfiguration<TourismTax>
    {
        public void Configure(EntityTypeBuilder<TourismTax> builder)
        {
            builder.ToTable("TourismTax");

            builder.HasAnnotation("Relational:TableName", "TourismTax");

            //-----------

            builder.HasIndex(e => e.PropertyId)
                .HasName("x_TourismTax_PropertyId");

            builder.HasIndex(e => e.TenantId)
                .HasName("x_TourismTax_TenantId");

            //-----------

            builder.Property(e => e.Id)
                .HasAnnotation("Relational:ColumnName", "TourismTaxId");

            builder.Property(e => e.PropertyId)
                .HasAnnotation("Relational:ColumnName", "PropertyId");

            builder.Property(e => e.IsActive)
                .HasAnnotation("Relational:ColumnName", "IsActive");

            builder.Property(e => e.BillingItemId)
                .HasAnnotation("Relational:ColumnName", "BillingItemId");

            builder.Property(e => e.Amount)
                .HasAnnotation("Relational:ColumnName", "Amount");

            builder.Property(e => e.NumberOfNights)
                .HasAnnotation("Relational:ColumnName", "NumberOfNights");

            builder.Property(e => e.IsTotalOfNights)
                .HasAnnotation("Relational:ColumnName", "IsTotalOfNights");

            builder.Property(e => e.IsIntegratedFiscalDocument)
                .HasAnnotation("Relational:ColumnName", "IsIntegratedFiscalDocument");

            builder.Property(e => e.LaunchType)
                .HasAnnotation("Relational:ColumnName", "LaunchType");

            builder.Property(e => e.CreationTime)
                .HasColumnType("datetime")
                .HasDefaultValueSql("(getutcdate())")
                .HasAnnotation("Relational:ColumnName", "CreationTime");

            builder.Property(e => e.CreatorUserId).HasAnnotation("Relational:ColumnName", "CreatorUserId");

            builder.Property(e => e.DeleterUserId).HasAnnotation("Relational:ColumnName", "DeleterUserId");

            builder.Property(e => e.DeletionTime)
                .HasColumnType("datetime")
                .HasAnnotation("Relational:ColumnName", "DeletionTime");

            builder.Property(e => e.IsDeleted).HasAnnotation("Relational:ColumnName", "IsDeleted");

            builder.Property(e => e.LastModificationTime)
                .HasColumnType("datetime")
                .HasDefaultValueSql("(getutcdate())")
                .HasAnnotation("Relational:ColumnName", "LastModificationTime");

            builder.Property(e => e.LastModifierUserId).HasAnnotation("Relational:ColumnName", "LastModifierUserId");

            builder.Property(e => e.TenantId).HasAnnotation("Relational:ColumnName", "TenantId");

            //------------

            builder.HasOne(d => d.Property)
                .WithMany(p => p.TourismTaxList)
                .HasForeignKey(d => d.PropertyId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_TourismTax_Property");

            builder.HasOne(d => d.BillingItem)
                .WithMany(p => p.TourismTaxList)
                .HasForeignKey(d => d.BillingItemId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_TourismTax_BillingItem");
        }
    }
}
