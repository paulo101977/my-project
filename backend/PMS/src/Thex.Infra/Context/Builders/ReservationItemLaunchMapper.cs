﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Thex.Domain.Entities;

namespace Thex.Infra.Context.Builders
{
    public class ReservationItemLaunchMapper : IEntityTypeConfiguration<ReservationItemLaunch>
    {
        public void Configure(EntityTypeBuilder<ReservationItemLaunch> builder)
        {
            builder.ToTable("ReservationItemLaunch");

            builder.HasAnnotation("Relational:TableName", "ReservationItemLaunch");

            builder.HasIndex(e => e.ReservationItemId)
                .HasName("x_ReservationItemLaunch_ReservationItemId");

            builder.HasIndex(e => e.GuestReservationItemId)
                .HasName("x_ReservationItemLaunch_GuestReservationItemId");

            builder.HasIndex(e => e.TenantId)
                .HasName("x_ReservationItemLaunch_TenantId");

            builder.HasKey(e => e.Id);

            builder.Property(e => e.Id)
                .HasAnnotation("Relational:ColumnName", "ReservationItemLaunchId");

            builder.Property(e => e.ReservationItemId)
                .HasAnnotation("Relational:ColumnName", "ReservationItemId");

            builder.Property(e => e.GuestReservationItemId)
                .HasAnnotation("Relational:ColumnName", "GuestReservationItemId");

            builder.Property(e => e.QuantityOfTourismTaxLaunched)
                .HasAnnotation("Relational:ColumnName", "QuantityOfTourismTaxLaunched");

            builder.HasOne(p => p.ReservationItem)
                .WithMany(r => r.ReservationItemLaunchList)
                .HasForeignKey(d => d.ReservationItemId)
                .HasConstraintName("FK_ReservationItemLaunch_ReservationItem");

            builder.HasOne(p => p.GuestReservationItem)
                .WithMany(r => r.ReservationItemLaunchList)
                .HasForeignKey(d => d.GuestReservationItemId)
                .HasConstraintName("FK_ReservationItemLaunch_GuestReservationItem");
        }
    }
}
