﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a Crudzilla tool.
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;
using Thex.Domain.Entities;

namespace Thex.Infra.Context.Builders
{
    public class RoomTypeBedTypeMapper : IEntityTypeConfiguration<RoomTypeBedType>
    {
        public void Configure(EntityTypeBuilder<RoomTypeBedType> builder)
        {
            builder.HasKey(e => new { e.RoomTypeId, e.BedTypeId });

            builder.ToTable("RoomTypeBedType");

            builder.HasAnnotation("Relational:TableName", "RoomTypeBedType");

            builder.Property(e => e.RoomTypeId).HasAnnotation("Relational:ColumnName", "RoomTypeId");

            builder.Property(e => e.BedTypeId).HasAnnotation("Relational:ColumnName", "BedTypeId");

            builder.Property(e => e.Count)
                .HasDefaultValueSql("((1))")
                .HasAnnotation("Relational:ColumnName", "Count");

            builder.Property(e => e.OptionalLimit)
                .HasDefaultValueSql("((0))")
                .HasAnnotation("Relational:ColumnName", "OptionalLimit");

            builder.HasOne(d => d.BedType)
                .WithMany(p => p.RoomTypeBedTypeList)
                .HasForeignKey(d => d.BedTypeId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_RoomTypeBedType_BedType");

            builder.HasOne(d => d.RoomType)
                .WithMany(p => p.RoomTypeBedTypeList)
                .HasForeignKey(d => d.RoomTypeId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_RoomTypeBedType_RoomType");

            builder.Ignore(e => e.Id);
        }
    }
}
