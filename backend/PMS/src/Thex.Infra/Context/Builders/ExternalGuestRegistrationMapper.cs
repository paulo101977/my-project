﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;
using Thex.Domain.Entities;

namespace Thex.Infra.Context.Builders
{
    public class ExternalGuestRegistrationMapper : IEntityTypeConfiguration<ExternalGuestRegistration>
    {
        public void Configure(EntityTypeBuilder<ExternalGuestRegistration> builder)
        {
            builder.ToTable("ExternalGuestRegistration");

            builder.HasAnnotation("Relational:TableName", "ExternalGuestRegistration");

            builder.Property(e => e.Id)
                .HasAnnotation("Relational:ColumnName", "ExternalGuestRegistrationId");

            builder.Property(e => e.GuestReservationItemId)
                .HasAnnotation("Relational:ColumnName", "GuestReservationItemId");

            builder.Property(e => e.ExternalRegistrationEmail)
                .HasMaxLength(200)
                .HasAnnotation("Relational:ColumnName", "ExternalRegistrationEmail");


            builder.Property(e => e.FirstName)
                .HasMaxLength(200)
                .IsUnicode(false)
                .HasAnnotation("Relational:ColumnName", "FirstName");


            builder.Property(e => e.LastName)
                .IsRequired()
                .HasMaxLength(200)
                .IsUnicode(false)
                .HasAnnotation("Relational:ColumnName", "LastName");

            builder.Property(e => e.FullName)
                .HasMaxLength(500)
                .IsUnicode(false)
                .HasAnnotation("Relational:ColumnName", "FullName");

            builder.Property(e => e.SocialName)
                .HasMaxLength(500)
                .IsUnicode(false)
                .HasAnnotation("Relational:ColumnName", "SocialName");

            builder.Property(e => e.Email)
                .HasMaxLength(200)
                .IsUnicode(false)
                .HasAnnotation("Relational:ColumnName", "Email");

            builder.Property(e => e.BirthDate)
                .HasColumnType("date")
                .HasAnnotation("Relational:ColumnName", "BirthDate");

            builder.Property(e => e.Gender)
                .HasMaxLength(1)
                .IsUnicode(false)
                .HasAnnotation("Relational:ColumnName", "Gender");

            builder.Property(e => e.OccupationId)
                .HasAnnotation("Relational:ColumnName", "OccupationId");

            builder.Property(e => e.PhoneNumber)
                .HasMaxLength(50)
                .IsUnicode(false)
                .HasAnnotation("Relational:ColumnName", "PhoneNumber");

            builder.Property(e => e.MobilePhoneNumber)
                .HasMaxLength(50)
                .IsUnicode(false)
                .HasAnnotation("Relational:ColumnName", "MobilePhoneNumber");

            builder.Property(e => e.NationalityId)
                .HasAnnotation("Relational:ColumnName", "NationalityId");

            builder.Property(e => e.PurposeOfTrip)
                .HasAnnotation("Relational:ColumnName", "PurposeOfTrip");

            builder.Property(e => e.ArrivingBy).HasAnnotation("Relational:ColumnName", "ArrivingBy");

            builder.Property(e => e.ArrivingFrom).HasAnnotation("Relational:ColumnName", "ArrivingFrom");

            builder.Property(e => e.ArrivingFromText)
                .HasMaxLength(100)
                .IsUnicode(false)
                .HasAnnotation("Relational:ColumnName", "ArrivingFromText");

            builder.Property(e => e.NextDestination)
                .HasAnnotation("Relational:ColumnName", "NextDestination");

            builder.Property(e => e.NextDestinationText)
                .HasMaxLength(100)
                .IsUnicode(false)
                .HasAnnotation("Relational:ColumnName", "NextDestinationText");


            builder.Property(e => e.AdditionalInformation)
                .HasColumnType("text")
                .HasAnnotation("Relational:ColumnName", "AdditionalInformation");

            builder.Property(e => e.HealthInsurance)
                .HasMaxLength(200)
                .IsUnicode(false)
                .HasAnnotation("Relational:ColumnName", "HealthInsurance");


            builder.Property(e => e.DocumentTypeId)
                .HasAnnotation("Relational:ColumnName", "DocumentTypeId");

            builder.Property(e => e.DocumentInformation)
               .HasMaxLength(20)
               .IsUnicode(false)
               .HasAnnotation("Relational:ColumnName", "DocumentInformation");

            builder.Property(e => e.LocationCategoryId)
                .HasDefaultValueSql("((1))")
                .HasAnnotation("Relational:ColumnName", "LocationCategoryId");

            builder.Property(e => e.Latitude)
                .HasColumnType("decimal(9, 6)")
                .HasAnnotation("Relational:ColumnName", "Latitude");

            builder.Property(e => e.Longitude)
                .HasColumnType("decimal(9, 6)")
                .HasAnnotation("Relational:ColumnName", "Longitude");

            builder.Property(e => e.StreetName)
                .IsRequired()
                .HasMaxLength(200)
                .IsUnicode(false)
                .HasAnnotation("Relational:ColumnName", "StreetName");

            builder.Property(e => e.StreetNumber)
                .IsRequired()
                .HasMaxLength(50)
                .IsUnicode(false)
                .HasAnnotation("Relational:ColumnName", "StreetNumber");

            builder.Property(e => e.AdditionalAddressDetails)
                .HasMaxLength(50)
                .IsUnicode(false)
                .HasAnnotation("Relational:ColumnName", "AdditionalAddressDetails");

            builder.Property(e => e.Neighborhood)
                .HasMaxLength(100)
                .IsUnicode(false)
                .HasAnnotation("Relational:ColumnName", "Neighborhood");

            builder.Property(e => e.CityId).HasAnnotation("Relational:ColumnName", "CityId");

            builder.Property(e => e.StateId).HasAnnotation("Relational:ColumnName", "StateId");

            builder.Property(e => e.CountryId).HasAnnotation("Relational:ColumnName", "CountryId");

            builder.Property(e => e.PostalCode).HasAnnotation("Relational:ColumnName", "PostalCode");

            builder.Property(e => e.CountryCode)
                .IsRequired()
                .HasColumnType("char(2)")
                .HasAnnotation("Relational:ColumnName", "CountryCode");

            builder.Property(e => e.TenantId)
                .HasAnnotation("Relational:ColumnName", "TenantId");

            builder.Property(e => e.IsDeleted)
                .HasAnnotation("Relational:ColumnName", "IsDeleted");

            builder.Property(e => e.CreationTime)
                .HasColumnType("datetime")
                .HasDefaultValueSql("(getutcdate())")
                .HasAnnotation("Relational:ColumnName", "CreationTime");

            builder.Property(e => e.CreatorUserId)
                .HasAnnotation("Relational:ColumnName", "CreatorUserId");

            builder.Property(e => e.DeleterUserId)
                .HasAnnotation("Relational:ColumnName", "DeleterUserId");

            builder.Property(e => e.DeletionTime)
                .HasColumnType("datetime")
                .HasAnnotation("Relational:ColumnName", "DeletionTime");

            builder.Property(e => e.LastModificationTime)
                .HasColumnType("datetime")
                .HasDefaultValueSql("(getutcdate())")
                .HasAnnotation("Relational:ColumnName", "LastModificationTime");

            builder.Property(e => e.LastModifierUserId)
                .HasAnnotation("Relational:ColumnName", "LastModifierUserId");
        }
    }
}
