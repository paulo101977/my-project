﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;
using Thex.Domain.Entities;

namespace Thex.Infra.Context.Builders
{
    public class ExternalRegistrationMapper : IEntityTypeConfiguration<ExternalRegistration>
    {
        public void Configure(EntityTypeBuilder<ExternalRegistration> builder)
        {
            builder.ToTable("ExternalRegistration");

            builder.HasAnnotation("Relational:TableName", "ExternalRegistration");

            builder.Property(e => e.Id)
                .HasAnnotation("Relational:ColumnName", "ExternalRegistrationId");

            builder.Property(e => e.ReservationUid)
                .HasAnnotation("Relational:ColumnName", "ReservationUid");

            builder.Property(e => e.Email)
                .HasMaxLength(200)
                .IsUnicode(false)
                .HasAnnotation("Relational:ColumnName", "Email");

            builder.Property(e => e.Link)
                .IsUnicode(false)
                .HasAnnotation("Relational:ColumnName", "Link");

            builder.Property(e => e.CreationTime)
                .HasColumnType("datetime")
                .HasDefaultValueSql("(getutcdate())")
                .HasAnnotation("Relational:ColumnName", "CreationTime");

            builder.Property(e => e.CreatorUserId).HasAnnotation("Relational:ColumnName", "CreatorUserId");

            builder.Property(e => e.DeleterUserId).HasAnnotation("Relational:ColumnName", "DeleterUserId");

            builder.Property(e => e.DeletionTime)
                .HasColumnType("datetime")
                .HasAnnotation("Relational:ColumnName", "DeletionTime");

            builder.Property(e => e.IsDeleted).HasAnnotation("Relational:ColumnName", "IsDeleted");

            builder.Property(e => e.LastModificationTime)
                .HasColumnType("datetime")
                .HasDefaultValueSql("(getutcdate())")
                .HasAnnotation("Relational:ColumnName", "LastModificationTime");

            builder.Property(e => e.LastModifierUserId).HasAnnotation("Relational:ColumnName", "LastModifierUserId");

            builder.Ignore(e => e.ExternalGuestRegistrationList);
        }
    }
}
