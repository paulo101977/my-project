﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a Crudzilla tool.
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;
using Thex.Domain.Entities;

namespace Thex.Infra.Context.Builders
{
    public class MealPlanTypeMapper : IEntityTypeConfiguration<MealPlanType>
    {
        public void Configure(EntityTypeBuilder<MealPlanType> builder)
        {
            
            builder.ToTable("MealPlanType");

            builder.HasAnnotation("Relational:TableName", "MealPlanType");

            builder.HasIndex(e => e.MealPlanTypeCode)
                .HasName("UK_MealPlanType_1")
                .IsUnique();

            builder.HasIndex(e => e.MealPlanTypeName)
                .HasName("UK_MealPlanType_2")
                .IsUnique();

            builder.Property(e => e.Id)
                .ValueGeneratedNever()
                .HasAnnotation("Relational:ColumnName", "MealPlanTypeId");

            builder.Property(e => e.MealPlanTypeCode)
                .HasMaxLength(10)
                .IsUnicode(false)
                .HasAnnotation("Relational:ColumnName", "MealPlanTypeCode");

            builder.Property(e => e.MealPlanTypeName)
                .IsRequired()
                .HasMaxLength(100)
                .IsUnicode(false)
                .HasAnnotation("Relational:ColumnName", "MealPlanTypeName");

        }
    }
}
