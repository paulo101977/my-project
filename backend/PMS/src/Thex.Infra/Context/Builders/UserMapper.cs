﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a Crudzilla tool.
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;
using Thex.Domain.Entities;

namespace Thex.Infra.Context.Builders
{
    public class UserMapper : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.ToTable("Users");

            builder.HasAnnotation("Relational:TableName", "Users");

            builder.Property(e => e.Id).HasAnnotation("Relational:ColumnName", "Id");

            builder.Property(e => e.Name)
                .IsRequired()
                .HasMaxLength(300)
                .IsUnicode(false)
                .HasAnnotation("Relational:ColumnName", "Name");
        }
    }
}
