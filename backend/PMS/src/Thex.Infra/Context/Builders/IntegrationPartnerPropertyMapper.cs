﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Thex.Domain.Entities;

namespace Thex.SuperAdmin.Infra.Mappers
{
    public class IntegrationPartnerPropertyMapper : IEntityTypeConfiguration<IntegrationPartnerProperty>
    {
        public void Configure(EntityTypeBuilder<IntegrationPartnerProperty> builder)
        {

            builder.ToTable("IntegrationPartnerProperty");

            builder.HasAnnotation("Relational:TableName", "IntegrationPartnerProperty");

            builder.HasKey(e => e.Id);

            builder.Property(e => e.Id)
              .HasAnnotation("Relational:ColumnName", "IntegrationPartnerPropertyId");
        }
    }
}
