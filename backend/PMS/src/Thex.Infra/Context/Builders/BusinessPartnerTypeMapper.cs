﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a Crudzilla tool.
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;
using Thex.Domain.Entities;

namespace Thex.Infra.Context.Builders
{
    public class BusinessPartnerTypeMapper : IEntityTypeConfiguration<BusinessPartnerType>
    {
        public void Configure(EntityTypeBuilder<BusinessPartnerType> builder)
        {
            
            builder.ToTable("BusinessPartnerType");

            builder.HasAnnotation("Relational:TableName", "BusinessPartnerType");

            builder.Property(e => e.Id)
                .ValueGeneratedNever()
                .HasAnnotation("Relational:ColumnName", "BusinessPartnerTypeId");

            builder.Property(e => e.BusinessPartnerTypeName)
                .IsRequired()
                .HasMaxLength(100)
                .IsUnicode(false)
                .HasAnnotation("Relational:ColumnName", "BusinessPartnerTypeName");

            builder.Property(e => e.Description)
                .IsRequired()
                .HasMaxLength(300)
                .IsUnicode(false)
                .HasAnnotation("Relational:ColumnName", "Description");

        }
    }
}
