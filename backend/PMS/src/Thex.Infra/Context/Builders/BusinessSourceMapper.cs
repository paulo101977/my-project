﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a Crudzilla tool.
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;
using Thex.Domain.Entities;

namespace Thex.Infra.Context.Builders
{
    public class BusinessSourceMapper : IEntityTypeConfiguration<BusinessSource>
    {
        public void Configure(EntityTypeBuilder<BusinessSource> builder)
        {
            
            builder.ToTable("BusinessSource");

            builder.HasAnnotation("Relational:TableName", "BusinessSource");

            builder.Property(e => e.Id).HasAnnotation("Relational:ColumnName", "BusinessSourceId").ValueGeneratedOnAdd();

            builder.Property(e => e.Description)
                .IsRequired()
                .HasMaxLength(50)
                .IsUnicode(false)
                .HasAnnotation("Relational:ColumnName", "Description");


            builder.Property(e => e.Code)
               .IsRequired()
               .HasMaxLength(20)
               .IsUnicode(false)
               .HasAnnotation("Relational:ColumnName", "Code");

            builder.Property(e => e.CreationTime)
                .HasColumnType("datetime")
                .HasDefaultValueSql("(getutcdate())")
                .HasAnnotation("Relational:ColumnName", "CreationTime");

            builder.Property(e => e.CreatorUserId).HasAnnotation("Relational:ColumnName", "CreatorUserId");

            builder.Property(e => e.DeleterUserId).HasAnnotation("Relational:ColumnName", "DeleterUserId");

            builder.Property(e => e.DeletionTime)
                .HasColumnType("datetime")
                .HasAnnotation("Relational:ColumnName", "DeletionTime");

            builder.Property(e => e.IsActive)
                // .HasDefaultValueSql("((1))")
                .HasAnnotation("Relational:ColumnName", "IsActive");

            builder.Property(e => e.IsDeleted).HasAnnotation("Relational:ColumnName", "IsDeleted");

            builder.Property(e => e.LastModificationTime)
                .HasColumnType("datetime")
                .HasDefaultValueSql("(getutcdate())")
                .HasAnnotation("Relational:ColumnName", "LastModificationTime");

            builder.Property(e => e.LastModifierUserId).HasAnnotation("Relational:ColumnName", "LastModifierUserId");


        }
    }
}
