﻿using AutoMapper;
using System;
using System.Collections.Generic;
using Thex.Common;
using Thex.Common.Enumerations;
using Thex.Domain.Entities;
using Thex.Domain.Extensions;
using Thex.Dto;
using Thex.Dto.Person;
using Tnf.Localization;
using Microsoft.Extensions.DependencyInjection;
using System.Linq;

namespace Thex.Infra.MapperProfiles
{
    public class DomainToDtoProfile : ScaffoldDomainToDtoProfile
    {
        public DomainToDtoProfile() : base()
        {
            NaturalPersonDtoMap();
            LegalPersonDtoMap();
            ContactPersonDtoMap();
        }

        private class PersonTypeConverter : ITypeConverter<Person, PersonDto>
        {
            public PersonDto Convert(Person source, PersonDto destination, ResolutionContext context)
            {
                if (source == null)
                {
                    return null;
                }

                destination = PersonConverter(source);
                return destination;
            }
        }

        private static PersonDto PersonConverter(Person person)
        {
            PersonDto result = null;
            if (person != null)
            {
                switch (person.PersonTypeValue)
                {
                    case (PersonTypeEnum.Natural):
                        result = person.MapTo<NaturalPersonDto>(); // AutoMapper.Mapper.Map<NaturalPersonDto>(person);
                        break;
                    case (PersonTypeEnum.Legal):
                        result = person.MapTo<LegalPersonDto>(); //AutoMapper.Mapper.Map<LegalPersonDto>(person);
                        break;
                    case (PersonTypeEnum.Contact):
                        result = person.MapTo<ContactPersonDto>(); //AutoMapper.Mapper.Map<ContactPersonDto>(person);
                        break;
                    default:
                        result = person.MapTo<ContactPersonDto>(); //AutoMapper.Mapper.Map<ContactPersonDto>(person);
                        break;
                }
                
            }
            return result;
        }

        protected AutoMapper.IMappingExpression<Person, NaturalPersonDto> NaturalPersonDtoMap()
        {
            return CreateMap<Person, NaturalPersonDto>()
                .ForMember(p => p.PropertyContactPersonList, s => s.Ignore())
                .ForMember(p => p.CompanyContactPersonList, s => s.Ignore())
                .ForMember(p => p.Guest, s => s.Ignore())
                .ForMember(p => p.GuestList, s => s.Ignore());
        }

        protected AutoMapper.IMappingExpression<Person, LegalPersonDto> LegalPersonDtoMap()
        {
            return CreateMap<Person, LegalPersonDto>()
                .ForMember(p => p.PropertyContactPersonList, s => s.Ignore())
                .ForMember(p => p.CompanyContactPersonList, s => s.Ignore())
                .ForMember(p => p.Guest, s => s.Ignore())
                .ForMember(p => p.GuestList, s => s.Ignore());
        }

        protected AutoMapper.IMappingExpression<Person, ContactPersonDto> ContactPersonDtoMap()
        {
            return CreateMap<Person, ContactPersonDto>()
                .ForMember(p => p.PropertyContactPersonList, s => s.Ignore())
                .ForMember(p => p.CompanyContactPersonList, s => s.Ignore())
                .ForMember(p => p.Guest, s => s.Ignore())
                .ForMember(p => p.GuestList, s => s.Ignore());
        }

        protected override AutoMapper.IMappingExpression<BedType, BedTypeDto> BedTypeMap()
        {
            var mapping = base.BedTypeMap();
            return mapping;
        }

        protected override AutoMapper.IMappingExpression<BillingAccount, BillingAccountDto> BillingAccountMap()
        {
            var mapping = base.BillingAccountMap();
            return mapping;
        }

        protected override AutoMapper.IMappingExpression<BillingAccountItem, BillingAccountItemDto> BillingAccountItemMap()
        {
            var mapping = base.BillingAccountItemMap();
            return mapping;
        }

        protected override AutoMapper.IMappingExpression<BillingAccountItemTransfer, BillingAccountItemTransferDto> BillingAccountItemTransferMap()
        {
            var mapping = base.BillingAccountItemTransferMap();
            return mapping;
        }

        protected override AutoMapper.IMappingExpression<BillingAccountType, BillingAccountTypeDto> BillingAccountTypeMap()
        {
            var mapping = base.BillingAccountTypeMap();
            return mapping;
        }

        protected override AutoMapper.IMappingExpression<Brand, BrandDto> BrandMap()
        {
            return CreateMap<Brand, BrandDto>()
            .ForMember(d => d.Chain, s => s.Ignore())
            .ForMember(d => d.PropertyList, s => s.Ignore())
            .AfterMap((brand, dto) =>
            {
                if (brand.Chain != null)
                {
                    dto.Chain = new ChainDto(brand.ChainId, brand.Chain.Name);
                }
            });
        }

        protected override AutoMapper.IMappingExpression<BusinessSource, BusinessSourceDto> BusinessSourceMap()
        {
            var mapping = base.BusinessSourceMap();
            return mapping;
        }

        protected override AutoMapper.IMappingExpression<Chain, ChainDto> ChainMap()
        {
            return CreateMap<Chain, ChainDto>()
            .ForMember(d => d.ChainBusinessSourceList, s => s.Ignore())
            .ForMember(d => d.ChainMarketSegmentList, s => s.Ignore())
            .ForMember(d => d.ReasonList, s => s.Ignore())
            .ForMember(d => d.BrandList, s => s.Ignore());

            //var mapping = base.ChainMap();
            //return mapping;
        }

        protected override AutoMapper.IMappingExpression<ChainBusinessSource, ChainBusinessSourceDto> ChainBusinessSourceMap()
        {
            var mapping = base.ChainBusinessSourceMap();
            return mapping;
        }

        protected override AutoMapper.IMappingExpression<ChainMarketSegment, ChainMarketSegmentDto> ChainMarketSegmentMap()
        {
            var mapping = base.ChainMarketSegmentMap();
            return mapping;
        }
		
		protected override AutoMapper.IMappingExpression<Channel, ChannelDto> ChannelMap()
        {
            var mapping = base.ChannelMap();
            return mapping;
        }
		
		protected override AutoMapper.IMappingExpression<ChannelCode, ChannelCodeDto> ChannelCodeMap()
        {
            var mapping = base.ChannelCodeMap();
            return mapping;
        }

        protected override AutoMapper.IMappingExpression<Company, CompanyDto> CompanyMap()
        {
            //var mapping = base.CompanyMap();
            //return mapping;
            return CreateMap<Company, CompanyDto>()
            .ForMember(d => d.PropertyList, s => s.Ignore())
            .ForMember(d => d.OccupationList, s => s.Ignore());
        }

        protected override AutoMapper.IMappingExpression<CompanyClient, CompanyClientDto> CompanyClientMap()
        {
            var mapping = base.CompanyClientMap();
            return mapping;
        }

        protected override AutoMapper.IMappingExpression<CompanyClientContactPerson, CompanyClientContactPersonDto> CompanyClientContactPersonMap()
        {
            var mapping = base.CompanyClientContactPersonMap();
            return mapping;
        }

        protected override AutoMapper.IMappingExpression<CompanyContactPerson, CompanyContactPersonDto> CompanyContactPersonMap()
        {
            var mapping = base.CompanyContactPersonMap();
            return mapping;
        }

        protected override AutoMapper.IMappingExpression<ContactInformation, ContactInformationDto> ContactInformationMap()
        {
            return CreateMap<ContactInformation, ContactInformationDto>()
                .ForMember(contactInformationDto => contactInformationDto.Owner, s => s.Ignore())
                .ForMember(contactInformationDto => contactInformationDto.ContactInformationType, s => s.Ignore())
                .AfterMap((contactInformationEntity, contactInformationDto) =>
                {
                    if (contactInformationEntity.ContactInformationType != null)
                        contactInformationDto.ContactInformationType =
                            new ContactInformationTypeDto
                            {
                                Id = contactInformationEntity.ContactInformationType.Id,
                                Name = contactInformationEntity.ContactInformationType.Name,

                            };
                });
        }

        protected override AutoMapper.IMappingExpression<ContactInformationType, ContactInformationTypeDto> ContactInformationTypeMap()
        {
            var mapping = base.ContactInformationTypeMap();
            return mapping;
        }

        protected override AutoMapper.IMappingExpression<CountryLanguage, CountryLanguageDto> CountryLanguageMap()
        {
            var mapping = base.CountryLanguageMap();
            return mapping;
        }

        protected override AutoMapper.IMappingExpression<CountryPersonDocumentType, CountryPersonDocumentTypeDto> CountryPersonDocumentTypeMap()
        {
            var mapping = base.CountryPersonDocumentTypeMap();
            return mapping;
        }

        protected override AutoMapper.IMappingExpression<CountrySubdivision, CountrySubdivisionDto> CountrySubdivisionMap()
        {
            var mapping = base.CountrySubdivisionMap();
            return mapping;
        }

        protected override AutoMapper.IMappingExpression<CountrySubdivisionTranslation, CountrySubdivisionTranslationDto> CountrySubdivisionTranslationMap()
        {
            var mapping = base.CountrySubdivisionTranslationMap();
            return mapping;
        }

        protected override AutoMapper.IMappingExpression<Document, DocumentDto> DocumentMap()
        {
            var mapping = base.DocumentMap();
            return mapping;
        }
        protected override AutoMapper.IMappingExpression<Employee, EmployeeDto> EmployeeMap()
        {
            var mapping = base.EmployeeMap();
            return mapping;
        }

        protected override AutoMapper.IMappingExpression<GuestRegistration, GuestRegistrationDto> GuestRegistrationMap()
        {
            var mapping = base.GuestRegistrationMap();
            return mapping;
        }

        protected override AutoMapper.IMappingExpression<GeneralOccupation, GeneralOccupationDto> GeneralOccupationMap()
        {
            var mapping = base.GeneralOccupationMap();
            return mapping;
        }

        protected override AutoMapper.IMappingExpression<Guest, GuestDto> GuestMap()
        {
            var mapping = base.GuestMap();
            return mapping;
        }

        protected override AutoMapper.IMappingExpression<GuestReservationItem, GuestReservationItemDto> GuestReservationItemMap()
        {
            var mapping = base.GuestReservationItemMap();
            return mapping;
        }

        protected override AutoMapper.IMappingExpression<Location, LocationDto> LocationMap()
        {
            return CreateMap<Location, LocationDto>()
                .ForMember(locationDto => locationDto.LocationCategory, s => s.Ignore())
                .AfterMap((locationEntity, locationDto) =>
                {
                    if (locationEntity.LocationCategory != null)
                        locationDto.LocationCategory =
                        new LocationCategoryDto
                        {
                            Id = locationEntity.LocationCategory.Id,
                            Name = locationEntity.LocationCategory.Name,
                            RecordScope = locationEntity.LocationCategory.RecordScope

                        };
                });

        }

        protected override AutoMapper.IMappingExpression<LocationCategory, LocationCategoryDto> LocationCategoryMap()
        {
            var mapping = base.LocationCategoryMap();
            return mapping;
        }

        protected override AutoMapper.IMappingExpression<MarketSegment, MarketSegmentDto> MarketSegmentMap()
        {
            var mapping = base.MarketSegmentMap();
            return mapping;
        }

        protected override AutoMapper.IMappingExpression<Occupation, OccupationDto> OccupationMap()
        {
            var mapping = base.OccupationMap();
            return mapping;
        }

        protected override AutoMapper.IMappingExpression<PaymentType, PaymentTypeDto> PaymentTypeMap()
        {
            var mapping = base.PaymentTypeMap();
            return mapping;
        }

        protected override void PersonMap()
        {
            CreateMap<Person, PersonDto>()
                .Include<Person, NaturalPersonDto>()
                .Include<Person, LegalPersonDto>()
                .Include<Person, ContactPersonDto>()
                .ConvertUsing<PersonTypeConverter>();

            CreateMap<Person, NaturalPersonDto>()
                .ForMember(p => p.PropertyContactPersonList, s => s.Ignore())
                .ForMember(p => p.CompanyContactPersonList, s => s.Ignore())
                .ForMember(p => p.Guest, s => s.Ignore())
                .ForMember(p => p.GuestList, s => s.Ignore());

            CreateMap<Person, LegalPersonDto>()
                .ForMember(p => p.PropertyContactPersonList, s => s.Ignore())
                .ForMember(p => p.CompanyContactPersonList, s => s.Ignore())
                .ForMember(p => p.Guest, s => s.Ignore())
                .ForMember(p => p.GuestList, s => s.Ignore());

            CreateMap<Person, ContactPersonDto>()
                .ForMember(p => p.PropertyContactPersonList, s => s.Ignore())
                .ForMember(p => p.CompanyContactPersonList, s => s.Ignore())
                .ForMember(p => p.Guest, s => s.Ignore())
                .ForMember(p => p.GuestList, s => s.Ignore());


        }

        protected override AutoMapper.IMappingExpression<PersonInformation, PersonInformationDto> PersonInformationMap()
        {
            return CreateMap<PersonInformation, PersonInformationDto>()
                .ForMember(personInformationDto => personInformationDto.Owner, s => s.Ignore())
                .ForMember(personInformationDto => personInformationDto.PersonInformationType, s => s.Ignore())
                .AfterMap((personInformationEntity, personInformationDto) =>
                {
                    if (personInformationEntity.PersonInformationType != null)
                        personInformationDto.PersonInformationType =
                        new PersonInformationTypeDto
                        {
                            Id = personInformationEntity.PersonInformationType.Id,
                            Name = personInformationEntity.PersonInformationType.Name,

                        };
                });

        }

        protected override AutoMapper.IMappingExpression<PersonInformationType, PersonInformationTypeDto> PersonInformationTypeMap()
        {
            var mapping = base.PersonInformationTypeMap();
            return mapping;
        }

        protected override AutoMapper.IMappingExpression<Plastic, PlasticDto> PlasticMap()
        {
            var mapping = base.PlasticMap();
            return mapping;
        }

        protected override AutoMapper.IMappingExpression<Thex.Domain.Entities.Property, PropertyDto> PropertyMap()
        {
            return CreateMap<Thex.Domain.Entities.Property, PropertyDto>();
            //.ForMember(p => p.PropertyGuestPrefsList, s => s.Ignore())
            //.ForMember(p => p.ReservationList, s => s.Ignore());
        }


        protected override AutoMapper.IMappingExpression<PropertyContactPerson, PropertyContactPersonDto> PropertyContactPersonMap()
        {
            var mapping = base.PropertyContactPersonMap();
            return mapping;
        }


        protected override AutoMapper.IMappingExpression<PropertyGuestPrefs, PropertyGuestPrefsDto> PropertyGuestPrefsMap()
        {
            var mapping = base.PropertyGuestPrefsMap();
            return mapping;
        }

        protected override AutoMapper.IMappingExpression<PropertyGuestType, PropertyGuestTypeDto> PropertyGuestTypeMap()
        {
            var mapping = base.PropertyGuestTypeMap();
            return mapping;
        }

        protected override AutoMapper.IMappingExpression<PropertyType, PropertyTypeDto> PropertyTypeMap()
        {
            var mapping = base.PropertyTypeMap();
            return mapping;
        }

        protected override AutoMapper.IMappingExpression<Reason, ReasonDto> ReasonMap()
        {
            var mapping = base.ReasonMap();
            return mapping;
        }

        protected override AutoMapper.IMappingExpression<ReasonCategory, ReasonCategoryDto> ReasonCategoryMap()
        {
            var mapping = base.ReasonCategoryMap();
            return mapping;
        }

        protected override AutoMapper.IMappingExpression<Reservation, ReservationDto> ReservationMap()
        {
            return CreateMap<Reservation, ReservationDto>()
                .ForMember(reservationDto => reservationDto.ReservationItemList, s => s.Ignore())
                .ForMember(reservationDto => reservationDto.ReservationConfirmation, s => s.Ignore())
                .AfterMap((reservationEntity, reservationDto) =>
                {
                    if (reservationEntity.ReservationItemList.ContainsElement())
                    {
                        foreach (var reservationItemEntity in reservationEntity.ReservationItemList)
                        {
                            var reservationItemDto = reservationItemEntity.MapTo<ReservationItemDto>();
                            reservationDto.ReservationItemList.Add(reservationItemDto);
                        }
                    }
                    if (reservationEntity.ReservationConfirmationList.ContainsElement())
                    {
                        foreach (var reservationConfirmationEntity in reservationEntity.ReservationConfirmationList)
                        {

                            var reservationConfirmationDto = reservationConfirmationEntity.MapTo<ReservationConfirmationDto>();
                            reservationDto.ReservationConfirmation = reservationConfirmationDto;
                        }
                    }
                });
        }

        protected override AutoMapper.IMappingExpression<ReservationBudget, ReservationBudgetDto> ReservationBudgetMap()
        {
            return CreateMap<ReservationBudget, ReservationBudgetDto>()
                .ForMember(reservationBudget => reservationBudget.ReservationItemId, s => s.Ignore());
        }

        protected override AutoMapper.IMappingExpression<ReservationConfirmation, ReservationConfirmationDto> ReservationConfirmationMap()
        {
            return CreateMap<ReservationConfirmation, ReservationConfirmationDto>()
                .ForMember(reservationConfirmationDto => reservationConfirmationDto.Plastic, s => s.Ignore())
                .AfterMap((reservationConfirmationEntity, reservationConfirmationDto) =>
                {
                    if (reservationConfirmationEntity.Plastic != null)
                        reservationConfirmationDto.Plastic = reservationConfirmationEntity.Plastic.MapTo<PlasticDto>();
                });
        }

        protected override AutoMapper.IMappingExpression<ReservationItem, ReservationItemDto> ReservationItemMap()
        {
            return CreateMap<ReservationItem, ReservationItemDto>()
                .ForMember(reservationItemDto => reservationItemDto.ReservationBudgetList, s => s.Ignore())
                .ForMember(reservationItemDto => reservationItemDto.GuestReservationItemList, s => s.Ignore())
                .ForMember(reservationItemDto => reservationItemDto.ReservationItemStatus, s => s.Ignore())
                .ForMember(reservationItemDto => reservationItemDto.ReceivedRoomType, s => s.Ignore())
                .ForMember(reservationItemDto => reservationItemDto.RequestedRoomType, s => s.Ignore())
                .ForMember(reservationItemDto => reservationItemDto.Room, s => s.Ignore())
                .ForMember(reservationItemDto => reservationItemDto.RoomLayout, s => s.Ignore())
                .ForMember(reservationItemDto => reservationItemDto.RatePlan, s => s.Ignore())
                .ForMember(reservationItemDto => reservationItemDto.MealPlanType, s => s.Ignore())
                .ForMember(reservationItemDto => reservationItemDto.Currency, s => s.Ignore())
                .ForMember(reservationItemDto => reservationItemDto.GratuityType, s => s.Ignore())
                .AfterMap((reservationItemEntity, reservationItemDto) =>
                {
                    reservationItemDto.ReservationItemStatus = (ReservationStatus)reservationItemEntity.ReservationItemStatusId;
                    reservationItemDto.ReservationBudgetList = new List<ReservationBudgetDto>();
                    reservationItemDto.GuestReservationItemList = new List<GuestReservationItemDto>();

                    if (reservationItemEntity.Room != null)
                        reservationItemDto.Room = reservationItemEntity.Room.MapTo<RoomDto>();

                    if (reservationItemEntity.ReceivedRoomType != null)
                        reservationItemDto.ReceivedRoomType = reservationItemEntity.ReceivedRoomType.MapTo<RoomTypeDto>();

                    if (reservationItemEntity.RequestedRoomType != null)
                        reservationItemDto.RequestedRoomType = reservationItemEntity.RequestedRoomType.MapTo<RoomTypeDto>();

                    if (reservationItemEntity.RoomLayout != null)
                        reservationItemDto.RoomLayout = reservationItemEntity.RoomLayout.MapTo<RoomLayoutDto>();

                    if (reservationItemEntity.RatePlan != null)
                        reservationItemDto.RatePlan = reservationItemEntity.RatePlan.MapTo<RatePlanDto>();

                    if (reservationItemEntity.MealPlanType != null)
                        reservationItemDto.MealPlanType = reservationItemEntity.MealPlanType.MapTo<MealPlanTypeDto>();

                    if (reservationItemEntity.Currency != null)
                        reservationItemDto.Currency = reservationItemEntity.Currency.MapTo<CurrencyDto>();

                    if (reservationItemEntity.GratuityType != null)
                        reservationItemDto.GratuityType = reservationItemEntity.GratuityType.MapTo<GratuityTypeDto>();

                    if (reservationItemEntity.ReservationBudgetList.ContainsElement())
                    {
                        foreach (var reservationBudgetEntity in reservationItemEntity.ReservationBudgetList)
                        {
                            ReservationBudgetDto reservationBudgetDto = reservationBudgetEntity.MapTo<ReservationBudgetDto>();
                            reservationItemDto.ReservationBudgetList.Add(reservationBudgetDto);
                        }
                    }

                    if (reservationItemEntity.GuestReservationItemList.ContainsElement())
                    {
                        foreach (var guestReservationItemEntity in reservationItemEntity.GuestReservationItemList)
                        {
                            GuestReservationItemDto guestReservationItemDto = guestReservationItemEntity.MapTo<GuestReservationItemDto>();
                            reservationItemDto.GuestReservationItemList.Add(guestReservationItemDto);
                        }
                    }

                });
        }

        protected override AutoMapper.IMappingExpression<Room, RoomDto> RoomMap()
        {
            return CreateMap<Room, RoomDto>()
                .ForMember(roomDto => roomDto.ChildRoomIdList, s => s.Ignore())
                .AfterMap((roomEntity, roomDto) =>
                {
                    roomDto.ChildRoomIdList = new List<int>();
                    if (roomEntity.ChildRoomList.ContainsElement())
                    {
                        foreach (var childRoom in roomEntity.ChildRoomList)
                        {
                            roomDto.ChildRoomIdList.Add(childRoom.Id);
                        }
                    }
                });
        }

        protected override AutoMapper.IMappingExpression<RoomLayout, RoomLayoutDto> RoomLayoutMap()
        {
            var mapping = base.RoomLayoutMap();
            return mapping;
        }

        protected override AutoMapper.IMappingExpression<RoomType, RoomTypeDto> RoomTypeMap()
        {
            return CreateMap<RoomType, RoomTypeDto>()
            .ForMember(roomTypeDto => roomTypeDto.RoomTypeBedTypeList, s => s.Ignore())
            .AfterMap((roomTypeEntity, roomTypeDto) =>
            {
                if (roomTypeEntity.RoomTypeBedTypeList.ContainsElement())
                {
                    foreach (var bedTypeEntity in roomTypeEntity.RoomTypeBedTypeList)
                    {
                        RoomTypeBedTypeDto roomTypeBedTypeDto = new RoomTypeBedTypeDto
                        {
                            RoomTypeId = bedTypeEntity.RoomTypeId,
                            BedType = (BedTypeEnum)bedTypeEntity.BedTypeId,
                            Count = bedTypeEntity.Count,
                            OptionalLimit = bedTypeEntity.OptionalLimit,
                        };
                        roomTypeDto.RoomTypeBedTypeList.Add(roomTypeBedTypeDto);
                    }
                }
            });
        }

        protected override AutoMapper.IMappingExpression<RoomTypeBedType, RoomTypeBedTypeDto> RoomTypeBedTypeMap()
        {
            return CreateMap<RoomTypeBedType, RoomTypeBedTypeDto>()
                .ForMember(roomTypeBedTypeDto => roomTypeBedTypeDto.BedType, s => s.Ignore())
                .AfterMap((roomTypeBedTypeEntity, roomTypeBedTypeDto) =>
                {
                    roomTypeBedTypeDto.BedType = (BedTypeEnum)roomTypeBedTypeEntity.BedTypeId;
                    roomTypeBedTypeDto.TypeName = ((BedTypeEnum)roomTypeBedTypeEntity.BedTypeId).ToString();
                }
                );
        }

        protected override AutoMapper.IMappingExpression<Status, StatusDto> StatusMap()
        {
            var mapping = base.StatusMap();
            return mapping;
        }

        protected override AutoMapper.IMappingExpression<StatusCategory, StatusCategoryDto> StatusCategoryMap()
        {
            var mapping = base.StatusCategoryMap();
            return mapping;
        }

        protected override AutoMapper.IMappingExpression<Subscription, SubscriptionDto> SubscriptionMap()
        {
            var mapping = base.SubscriptionMap();
            return mapping;
        }

        protected override AutoMapper.IMappingExpression<PropertyContract, PropertyContractDto> PropertyContracteMap()
        {
            var mapping = base.PropertyContracteMap();
            return mapping;
        }
    }
}
