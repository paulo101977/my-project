﻿using AutoMapper;
using Thex.Domain.Entities;
using Thex.Dto;

namespace Thex.Infra.MapperProfiles
{
    public abstract class ScaffoldDomainToDtoProfile : AutoMapper.Profile
    {
        public ScaffoldDomainToDtoProfile()
        {
            AgreementTypeMap();
            ApplicationModuleMap();
            ApplicationParameterMap();
            AuditStepTypeMap();
            BedTypeMap();
            BillingAccountMap();
            BillingAccountItemMap();
            BillingAccountItemTransferMap();
            BillingAccountItemTypeMap();
            BillingAccountTypeMap();
            BillingInvoiceMap();
            BillingInvoiceModelMap();
            BillingInvoicePropertyMap();
            BillingInvoicePropertySupportedTypeMap();
            BillingItemMap();
            BillingItemCategoryMap();
            BillingItemPaymentConditionMap();
            BillingItemTypeMap();
            BillingTaxMap();
            BrandMap();
            BusinessPartnerMap();
            BusinessPartnerTypeMap();
            BusinessSourceMap();
            ChainMap();
            ChainBusinessSourceMap();
            ChainMarketSegmentMap();
            ChannelMap();
            ChannelCodeMap();
            CompanyMap();
            CompanyClientMap();
            CompanyClientContactPersonMap();
            CompanyContactPersonMap();
            ContactInformationMap();
            ContactInformationTypeMap();
            CountryCurrencyMap();
            CountryLanguageMap();
            CountryPersonDocumentTypeMap();
            CountrySubdivisionMap();
            CountrySubdivisionTranslationMap();
            CountrySubdvisionServiceMap();
            CurrencyMap();
            CurrencyExchangeMap();
            DocumentMap();
            EmployeeMap();
            FeatureGroupMap();
            GuestRegistrationMap();
            GeneralOccupationMap();
            GratuityTypeMap();
            GuestMap();
            GuestReservationItemMap();
            LocationMap();
            LocationCategoryMap();
            MarketSegmentMap();
            MealPlanTypeMap();
            OccupationMap();
            ParameterTypeMap();
            PaymentTypeMap();
            PersonMap();
            PersonInformationMap();
            PersonInformationTypeMap();
            PlasticMap();
            PlasticBrandMap();
            PlasticBrandPropertyMap();
            PolicyTypeMap();
            PropertyMap();
            RateTypeMap();
            PropertyBaseRateHeaderHistoryMap();
            PropertyBaseRateHistoryMap();
            PropertyAuditProcessMap();
            PropertyAuditProcessStepMap();
            PropertyAuditProcessStepErrorMap();
            PropertyBaseRateMap();
            PropertyBusinessPartnerMap();
            PropertyCompanyClientCategoryMap();
            PropertyContactPersonMap();
            PropertyCurrencyExchangeMap();
            PropertyGuestPrefsMap();
            PropertyGuestTypeMap();
            PropertyMealPlanTypeMap();
            PropertyMealPlanTypeRateDtoMap();
            PropertyMealPlanTypeRateMap();
            PropertyMealPlanTypeRateHistoryMap();
            PropertyMealPlanTypeRateHeaderMap();
            PropertyMealPlanTypeRateHeaderDtoMap();
            PropertyParameterMap();
            PropertyPolicyMap();
            PropertyTypeMap();
            PropertyPremiseHeaderMap();
            RatePlanMap();
            RatePlanCommissionMap();
            RatePlanCompanyClientMap();
            RatePlanRoomTypeMap();
            ReasonMap();
            ReasonCategoryMap();
            ReservationMap();
            ReservationBudgetMap();
            ReservationConfirmationMap();
            ReservationItemMap();
            RoomMap();
            RoomLayoutMap();
            RoomTypeMap();
            RoomTypeBedTypeMap();
            StatusMap();
            StatusCategoryMap();
            SubscriptionMap();
            TenantMap();
            HousekeepingStatusMap();
            HousekeepingStatusPropertyMap();
            RoomBlockingMap();
            PropertyContracteMap();
        }

        protected virtual AutoMapper.IMappingExpression<AgreementType, AgreementTypeDto> AgreementTypeMap()
        {
            return CreateMap<AgreementType, AgreementTypeDto>();
        }

        protected virtual AutoMapper.IMappingExpression<ApplicationModule, ApplicationModuleDto> ApplicationModuleMap()
        {
            return CreateMap<ApplicationModule, ApplicationModuleDto>();
        }

        protected virtual AutoMapper.IMappingExpression<ApplicationParameter, ApplicationParameterDto> ApplicationParameterMap()
        {
            return CreateMap<ApplicationParameter, ApplicationParameterDto>();
        }

        protected virtual AutoMapper.IMappingExpression<AuditStepType, AuditStepTypeDto> AuditStepTypeMap()
        {
            return CreateMap<AuditStepType, AuditStepTypeDto>();
        }

        protected virtual AutoMapper.IMappingExpression<BedType, BedTypeDto> BedTypeMap()
        {
            return CreateMap<BedType, BedTypeDto>();
        }

        protected virtual AutoMapper.IMappingExpression<BillingAccount, BillingAccountDto> BillingAccountMap()
        {
            return CreateMap<BillingAccount, BillingAccountDto>();
        }

        protected virtual AutoMapper.IMappingExpression<BillingAccountItem, BillingAccountItemDto> BillingAccountItemMap()
        {
            return CreateMap<BillingAccountItem, BillingAccountItemDto>();
        }

        protected virtual AutoMapper.IMappingExpression<BillingAccountItemTransfer, BillingAccountItemTransferDto> BillingAccountItemTransferMap()
        {
            return CreateMap<BillingAccountItemTransfer, BillingAccountItemTransferDto>();
        }

        protected virtual AutoMapper.IMappingExpression<BillingAccountItemType, BillingAccountItemTypeDto> BillingAccountItemTypeMap()
        {
            return CreateMap<BillingAccountItemType, BillingAccountItemTypeDto>();
        }

        protected virtual AutoMapper.IMappingExpression<BillingAccountType, BillingAccountTypeDto> BillingAccountTypeMap()
        {
            return CreateMap<BillingAccountType, BillingAccountTypeDto>();
        }

        protected virtual AutoMapper.IMappingExpression<BillingInvoice, BillingInvoiceDto> BillingInvoiceMap()
        {
            return CreateMap<BillingInvoice, BillingInvoiceDto>();
        }

        protected virtual AutoMapper.IMappingExpression<BillingInvoiceModel, BillingInvoiceModelDto> BillingInvoiceModelMap()
        {
            return CreateMap<BillingInvoiceModel, BillingInvoiceModelDto>();
        }

        protected virtual AutoMapper.IMappingExpression<BillingInvoiceProperty, BillingInvoicePropertyDto> BillingInvoicePropertyMap()
        {
            return CreateMap<BillingInvoiceProperty, BillingInvoicePropertyDto>();
        }

        protected virtual AutoMapper.IMappingExpression<BillingInvoicePropertySupportedType, BillingInvoicePropertySupportedTypeDto> BillingInvoicePropertySupportedTypeMap()
        {
            return CreateMap<BillingInvoicePropertySupportedType, BillingInvoicePropertySupportedTypeDto>();
        }

        protected virtual AutoMapper.IMappingExpression<BillingItem, BillingItemDto> BillingItemMap()
        {
            return CreateMap<BillingItem, BillingItemDto>();
        }

        protected virtual AutoMapper.IMappingExpression<BillingItemCategory, BillingItemCategoryDto> BillingItemCategoryMap()
        {
            return CreateMap<BillingItemCategory, BillingItemCategoryDto>();
        }

        protected virtual AutoMapper.IMappingExpression<BillingItemPaymentCondition, BillingItemPaymentConditionDto> BillingItemPaymentConditionMap()
        {
            return CreateMap<BillingItemPaymentCondition, BillingItemPaymentConditionDto>();
        }

        protected virtual AutoMapper.IMappingExpression<BillingItemType, BillingItemTypeDto> BillingItemTypeMap()
        {
            return CreateMap<BillingItemType, BillingItemTypeDto>();
        }

        protected virtual AutoMapper.IMappingExpression<BillingTax, BillingTaxDto> BillingTaxMap()
        {
            return CreateMap<BillingTax, BillingTaxDto>();
        }

        protected virtual AutoMapper.IMappingExpression<Brand, BrandDto> BrandMap()
        {
            return CreateMap<Brand, BrandDto>();
        }

        protected virtual AutoMapper.IMappingExpression<BusinessPartner, BusinessPartnerDto> BusinessPartnerMap()
        {
            return CreateMap<BusinessPartner, BusinessPartnerDto>();
        }

        protected virtual AutoMapper.IMappingExpression<BusinessPartnerType, BusinessPartnerTypeDto> BusinessPartnerTypeMap()
        {
            return CreateMap<BusinessPartnerType, BusinessPartnerTypeDto>();
        }

        protected virtual AutoMapper.IMappingExpression<BusinessSource, BusinessSourceDto> BusinessSourceMap()
        {
            return CreateMap<BusinessSource, BusinessSourceDto>();
        }
        protected virtual AutoMapper.IMappingExpression<HousekeepingStatus, HousekeepingStatusDto> HousekeepingStatusMap()
        {
            return CreateMap<HousekeepingStatus, HousekeepingStatusDto>();
        }

        protected virtual AutoMapper.IMappingExpression<Chain, ChainDto> ChainMap()
        {
            return CreateMap<Chain, ChainDto>();
        }

        protected virtual AutoMapper.IMappingExpression<ChainBusinessSource, ChainBusinessSourceDto> ChainBusinessSourceMap()
        {
            return CreateMap<ChainBusinessSource, ChainBusinessSourceDto>();
        }

        protected virtual AutoMapper.IMappingExpression<ChainMarketSegment, ChainMarketSegmentDto> ChainMarketSegmentMap()
        {
            return CreateMap<ChainMarketSegment, ChainMarketSegmentDto>();
        }

        protected virtual AutoMapper.IMappingExpression<Channel, ChannelDto> ChannelMap()
        {
            return CreateMap<Channel, ChannelDto>();
        }

        protected virtual AutoMapper.IMappingExpression<ChannelCode, ChannelCodeDto> ChannelCodeMap()
        {
            return CreateMap<ChannelCode, ChannelCodeDto>();
        }

        protected virtual AutoMapper.IMappingExpression<Company, CompanyDto> CompanyMap()
        {
            return CreateMap<Company, CompanyDto>();
        }

        protected virtual AutoMapper.IMappingExpression<CompanyClient, CompanyClientDto> CompanyClientMap()
        {
            return CreateMap<CompanyClient, CompanyClientDto>();
        }

        protected virtual AutoMapper.IMappingExpression<CompanyClientContactPerson, CompanyClientContactPersonDto> CompanyClientContactPersonMap()
        {
            return CreateMap<CompanyClientContactPerson, CompanyClientContactPersonDto>();
        }

        protected virtual AutoMapper.IMappingExpression<CompanyContactPerson, CompanyContactPersonDto> CompanyContactPersonMap()
        {
            return CreateMap<CompanyContactPerson, CompanyContactPersonDto>();
        }

        protected virtual AutoMapper.IMappingExpression<ContactInformation, ContactInformationDto> ContactInformationMap()
        {
            return CreateMap<ContactInformation, ContactInformationDto>();
        }

        protected virtual AutoMapper.IMappingExpression<ContactInformationType, ContactInformationTypeDto> ContactInformationTypeMap()
        {
            return CreateMap<ContactInformationType, ContactInformationTypeDto>();
        }

        protected virtual AutoMapper.IMappingExpression<CountryCurrency, CountryCurrencyDto> CountryCurrencyMap()
        {
            return CreateMap<CountryCurrency, CountryCurrencyDto>();
        }

        protected virtual AutoMapper.IMappingExpression<CountryLanguage, CountryLanguageDto> CountryLanguageMap()
        {
            return CreateMap<CountryLanguage, CountryLanguageDto>();
        }

        protected virtual AutoMapper.IMappingExpression<CountryPersonDocumentType, CountryPersonDocumentTypeDto> CountryPersonDocumentTypeMap()
        {
            return CreateMap<CountryPersonDocumentType, CountryPersonDocumentTypeDto>();
        }

        protected virtual AutoMapper.IMappingExpression<CountrySubdivision, CountrySubdivisionDto> CountrySubdivisionMap()
        {
            return CreateMap<CountrySubdivision, CountrySubdivisionDto>();
        }

        protected virtual AutoMapper.IMappingExpression<CountrySubdivisionTranslation, CountrySubdivisionTranslationDto> CountrySubdivisionTranslationMap()
        {
            return CreateMap<CountrySubdivisionTranslation, CountrySubdivisionTranslationDto>();
        }

        protected virtual AutoMapper.IMappingExpression<CountrySubdvisionService, CountrySubdvisionServiceDto> CountrySubdvisionServiceMap()
        {
            return CreateMap<CountrySubdvisionService, CountrySubdvisionServiceDto>();
        }
        protected virtual AutoMapper.IMappingExpression<Currency, CurrencyDto> CurrencyMap()
        {
            return CreateMap<Currency, CurrencyDto>();
        }

        protected virtual AutoMapper.IMappingExpression<CurrencyExchange, CurrencyExchangeDto> CurrencyExchangeMap()
        {
            return CreateMap<CurrencyExchange, CurrencyExchangeDto>();
        }

        protected virtual AutoMapper.IMappingExpression<Document, DocumentDto> DocumentMap()
        {
            return CreateMap<Document, DocumentDto>();
        }

        protected virtual AutoMapper.IMappingExpression<Employee, EmployeeDto> EmployeeMap()
        {
            return CreateMap<Employee, EmployeeDto>();
        }

        protected virtual AutoMapper.IMappingExpression<FeatureGroup, FeatureGroupDto> FeatureGroupMap()
        {
            return CreateMap<FeatureGroup, FeatureGroupDto>();
        }

        protected virtual AutoMapper.IMappingExpression<GuestRegistration, GuestRegistrationDto> GuestRegistrationMap()
        {
            return CreateMap<GuestRegistration, GuestRegistrationDto>();
        }

        protected virtual AutoMapper.IMappingExpression<GeneralOccupation, GeneralOccupationDto> GeneralOccupationMap()
        {
            return CreateMap<GeneralOccupation, GeneralOccupationDto>();
        }

        protected virtual AutoMapper.IMappingExpression<GratuityType, GratuityTypeDto> GratuityTypeMap()
        {
            return CreateMap<GratuityType, GratuityTypeDto>();
        }

        protected virtual AutoMapper.IMappingExpression<Guest, GuestDto> GuestMap()
        {
            return CreateMap<Guest, GuestDto>();
        }

        protected virtual AutoMapper.IMappingExpression<GuestReservationItem, GuestReservationItemDto> GuestReservationItemMap()
        {
            return CreateMap<GuestReservationItem, GuestReservationItemDto>();
        }

        protected virtual AutoMapper.IMappingExpression<HousekeepingStatusProperty, HousekeepingStatusPropertyDto> HousekeepingStatusPropertyMap()
        {
            return CreateMap<HousekeepingStatusProperty, HousekeepingStatusPropertyDto>();
        }

        protected virtual AutoMapper.IMappingExpression<Location, LocationDto> LocationMap()
        {
            return CreateMap<Location, LocationDto>();
        }

        protected virtual AutoMapper.IMappingExpression<LocationCategory, LocationCategoryDto> LocationCategoryMap()
        {
            return CreateMap<LocationCategory, LocationCategoryDto>();
        }

        protected virtual AutoMapper.IMappingExpression<MarketSegment, MarketSegmentDto> MarketSegmentMap()
        {
            return CreateMap<MarketSegment, MarketSegmentDto>();
        }

        protected virtual AutoMapper.IMappingExpression<MealPlanType, MealPlanTypeDto> MealPlanTypeMap()
        {
            return CreateMap<MealPlanType, MealPlanTypeDto>();
        }

        protected virtual AutoMapper.IMappingExpression<Occupation, OccupationDto> OccupationMap()
        {
            return CreateMap<Occupation, OccupationDto>();
        }

        protected virtual AutoMapper.IMappingExpression<ParameterType, ParameterTypeDto> ParameterTypeMap()
        {
            return CreateMap<ParameterType, ParameterTypeDto>();
        }

        protected virtual AutoMapper.IMappingExpression<PaymentType, PaymentTypeDto> PaymentTypeMap()
        {
            return CreateMap<PaymentType, PaymentTypeDto>();
        }

        protected virtual void PersonMap()
        {
            CreateMap<Person, PersonDto>();
        }

        protected virtual AutoMapper.IMappingExpression<PersonInformation, PersonInformationDto> PersonInformationMap()
        {
            return CreateMap<PersonInformation, PersonInformationDto>();
        }

        protected virtual AutoMapper.IMappingExpression<PersonInformationType, PersonInformationTypeDto> PersonInformationTypeMap()
        {
            return CreateMap<PersonInformationType, PersonInformationTypeDto>();
        }

        protected virtual AutoMapper.IMappingExpression<Plastic, PlasticDto> PlasticMap()
        {
            return CreateMap<Plastic, PlasticDto>();
        }

        protected virtual AutoMapper.IMappingExpression<PlasticBrand, PlasticBrandDto> PlasticBrandMap()
        {
            return CreateMap<PlasticBrand, PlasticBrandDto>();
        }

        protected virtual AutoMapper.IMappingExpression<PlasticBrandProperty, PlasticBrandPropertyDto> PlasticBrandPropertyMap()
        {
            return CreateMap<PlasticBrandProperty, PlasticBrandPropertyDto>();
        }

        protected virtual AutoMapper.IMappingExpression<PolicyType, PolicyTypeDto> PolicyTypeMap()
        {
            return CreateMap<PolicyType, PolicyTypeDto>();
        }

        protected virtual AutoMapper.IMappingExpression<Property, PropertyDto> PropertyMap()
        {
            return CreateMap<Property, PropertyDto>();
        }

        protected virtual AutoMapper.IMappingExpression<PropertyBaseRateHeaderHistory, PropertyBaseRateHeaderHistoryDto> PropertyBaseRateHeaderHistoryMap()
        {
            return CreateMap<PropertyBaseRateHeaderHistory, PropertyBaseRateHeaderHistoryDto>();
        }

        protected virtual AutoMapper.IMappingExpression<PropertyBaseRateHistory, PropertyBaseRateHistoryDto> PropertyBaseRateHistoryMap()
        {
            return CreateMap<PropertyBaseRateHistory, PropertyBaseRateHistoryDto>();
        }

        protected virtual AutoMapper.IMappingExpression<PropertyAuditProcess, PropertyAuditProcessDto> PropertyAuditProcessMap()
        {
            return CreateMap<PropertyAuditProcess, PropertyAuditProcessDto>();
        }

        protected virtual AutoMapper.IMappingExpression<PropertyAuditProcessStep, PropertyAuditProcessStepDto> PropertyAuditProcessStepMap()
        {
            return CreateMap<PropertyAuditProcessStep, PropertyAuditProcessStepDto>();
        }

        protected virtual AutoMapper.IMappingExpression<PropertyAuditProcessStepError, PropertyAuditProcessStepErrorDto> PropertyAuditProcessStepErrorMap()
        {
            return CreateMap<PropertyAuditProcessStepError, PropertyAuditProcessStepErrorDto>();
        }
        protected virtual AutoMapper.IMappingExpression<PropertyBaseRate, PropertyBaseRateDto> PropertyBaseRateMap()
        {
            return CreateMap<PropertyBaseRate, PropertyBaseRateDto>();
        }

        protected virtual AutoMapper.IMappingExpression<PropertyBusinessPartner, PropertyBusinessPartnerDto> PropertyBusinessPartnerMap()
        {
            return CreateMap<PropertyBusinessPartner, PropertyBusinessPartnerDto>();
        }

        protected virtual AutoMapper.IMappingExpression<PropertyCompanyClientCategory, PropertyCompanyClientCategoryDto> PropertyCompanyClientCategoryMap()
        {
            return CreateMap<PropertyCompanyClientCategory, PropertyCompanyClientCategoryDto>();
        }

        protected virtual AutoMapper.IMappingExpression<PropertyContactPerson, PropertyContactPersonDto> PropertyContactPersonMap()
        {
            return CreateMap<PropertyContactPerson, PropertyContactPersonDto>();
        }

        protected virtual AutoMapper.IMappingExpression<PropertyCurrencyExchange, PropertyCurrencyExchangeDto> PropertyCurrencyExchangeMap()
        {
            return CreateMap<PropertyCurrencyExchange, PropertyCurrencyExchangeDto>();
        }


        protected virtual AutoMapper.IMappingExpression<PropertyGuestPrefs, PropertyGuestPrefsDto> PropertyGuestPrefsMap()
        {
            return CreateMap<PropertyGuestPrefs, PropertyGuestPrefsDto>();
        }

        protected virtual AutoMapper.IMappingExpression<PropertyGuestType, PropertyGuestTypeDto> PropertyGuestTypeMap()
        {
            return CreateMap<PropertyGuestType, PropertyGuestTypeDto>();
        }

        protected virtual AutoMapper.IMappingExpression<PropertyMealPlanType, PropertyMealPlanTypeDto> PropertyMealPlanTypeMap()
        {
            return CreateMap<PropertyMealPlanType, PropertyMealPlanTypeDto>();
        }

        protected virtual AutoMapper.IMappingExpression<PropertyMealPlanTypeRateDto, PropertyMealPlanTypeRate> PropertyMealPlanTypeRateDtoMap()
        {
            return CreateMap<PropertyMealPlanTypeRateDto, PropertyMealPlanTypeRate>();
        }

        protected virtual AutoMapper.IMappingExpression<PropertyMealPlanTypeRate, PropertyMealPlanTypeRateDto> PropertyMealPlanTypeRateMap()
        {
            return CreateMap<PropertyMealPlanTypeRate, PropertyMealPlanTypeRateDto>()
                    .ForMember(x => x.InitialDate, opt => opt.Ignore())
                    .ForMember(x => x.FinalDate, opt => opt.Ignore())
                    .ForMember(x => x.Expandables, opt => opt.Ignore());
        }

        protected virtual AutoMapper.IMappingExpression<PropertyMealPlanTypeRateHistory, PropertyMealPlanTypeRateHistoryDto> PropertyMealPlanTypeRateHistoryMap()
        {
            return CreateMap<PropertyMealPlanTypeRateHistory, PropertyMealPlanTypeRateHistoryDto>()
                    .ForMember(x => x.Expandables, opt => opt.Ignore());
        }

        protected virtual AutoMapper.IMappingExpression<PropertyMealPlanTypeRateHeader, PropertyMealPlanTypeRateHeaderDto> PropertyMealPlanTypeRateHeaderMap()
        {
            return CreateMap<PropertyMealPlanTypeRateHeader, PropertyMealPlanTypeRateHeaderDto>()
                    .ForMember(x => x.Expandables, opt => opt.Ignore());
        }

        protected virtual AutoMapper.IMappingExpression<PropertyMealPlanTypeRateHeaderDto, PropertyMealPlanTypeRateHeader> PropertyMealPlanTypeRateHeaderDtoMap()
        {
            return CreateMap<PropertyMealPlanTypeRateHeaderDto, PropertyMealPlanTypeRateHeader>();
        }

        protected virtual AutoMapper.IMappingExpression<PropertyParameter, PropertyParameterDto> PropertyParameterMap()
        {
            return CreateMap<PropertyParameter, PropertyParameterDto>();
        }

        protected virtual AutoMapper.IMappingExpression<PropertyPolicy, PropertyPolicyDto> PropertyPolicyMap()
        {
            return CreateMap<PropertyPolicy, PropertyPolicyDto>();
        }

        protected virtual AutoMapper.IMappingExpression<PropertyType, PropertyTypeDto> PropertyTypeMap()
        {
            return CreateMap<PropertyType, PropertyTypeDto>();
        }

        protected virtual AutoMapper.IMappingExpression<PropertyPremiseHeader, PropertyPremiseHeaderDto> PropertyPremiseHeaderMap()
        {
            return CreateMap<PropertyPremiseHeader, PropertyPremiseHeaderDto>();
        }

        protected virtual AutoMapper.IMappingExpression<RateType, RateTypeDto> RateTypeMap()
        {
            return CreateMap<RateType, RateTypeDto>();
        }

        protected virtual AutoMapper.IMappingExpression<RatePlan, RatePlanDto> RatePlanMap()
        {
            return CreateMap<RatePlan, RatePlanDto>();
        }

        protected virtual AutoMapper.IMappingExpression<RatePlanCommission, RatePlanCommissionDto> RatePlanCommissionMap()
        {
            return CreateMap<RatePlanCommission, RatePlanCommissionDto>();
        }

        protected virtual AutoMapper.IMappingExpression<RatePlanCompanyClient, RatePlanCompanyClientDto> RatePlanCompanyClientMap()
        {
            return CreateMap<RatePlanCompanyClient, RatePlanCompanyClientDto>();
        }

        protected virtual AutoMapper.IMappingExpression<RatePlanRoomType, RatePlanRoomTypeDto> RatePlanRoomTypeMap()
        {
            return CreateMap<RatePlanRoomType, RatePlanRoomTypeDto>();
        }

        protected virtual AutoMapper.IMappingExpression<Reason, ReasonDto> ReasonMap()
        {
            return CreateMap<Reason, ReasonDto>();
        }

        protected virtual AutoMapper.IMappingExpression<ReasonCategory, ReasonCategoryDto> ReasonCategoryMap()
        {
            return CreateMap<ReasonCategory, ReasonCategoryDto>();
        }

        protected virtual AutoMapper.IMappingExpression<Reservation, ReservationDto> ReservationMap()
        {
            return CreateMap<Reservation, ReservationDto>();
        }

        protected virtual AutoMapper.IMappingExpression<ReservationBudget, ReservationBudgetDto> ReservationBudgetMap()
        {
            return CreateMap<ReservationBudget, ReservationBudgetDto>();
        }

        protected virtual AutoMapper.IMappingExpression<ReservationConfirmation, ReservationConfirmationDto> ReservationConfirmationMap()
        {
            return CreateMap<ReservationConfirmation, ReservationConfirmationDto>();
        }

        protected virtual AutoMapper.IMappingExpression<ReservationItem, ReservationItemDto> ReservationItemMap()
        {
            return CreateMap<ReservationItem, ReservationItemDto>();
        }

        protected virtual AutoMapper.IMappingExpression<Room, RoomDto> RoomMap()
        {
            return CreateMap<Room, RoomDto>();
        }

        protected virtual AutoMapper.IMappingExpression<RoomLayout, RoomLayoutDto> RoomLayoutMap()
        {
            return CreateMap<RoomLayout, RoomLayoutDto>();
        }

        protected virtual AutoMapper.IMappingExpression<RoomBlocking, RoomBlockingDto> RoomBlockingMap()
        {
            return CreateMap<RoomBlocking, RoomBlockingDto>();
        }

        protected virtual AutoMapper.IMappingExpression<RoomType, RoomTypeDto> RoomTypeMap()
        {
            return CreateMap<RoomType, RoomTypeDto>();
        }

        protected virtual AutoMapper.IMappingExpression<RoomTypeBedType, RoomTypeBedTypeDto> RoomTypeBedTypeMap()
        {
            return CreateMap<RoomTypeBedType, RoomTypeBedTypeDto>();
        }

        protected virtual AutoMapper.IMappingExpression<Status, StatusDto> StatusMap()
        {
            return CreateMap<Status, StatusDto>();
        }

        protected virtual AutoMapper.IMappingExpression<StatusCategory, StatusCategoryDto> StatusCategoryMap()
        {
            return CreateMap<StatusCategory, StatusCategoryDto>();
        }

        protected virtual AutoMapper.IMappingExpression<Subscription, SubscriptionDto> SubscriptionMap()
        {
            return CreateMap<Subscription, SubscriptionDto>();
        }

        protected virtual AutoMapper.IMappingExpression<Tenant, TenantDto> TenantMap()
        {
            return CreateMap<Tenant, TenantDto>();
        }

        protected virtual AutoMapper.IMappingExpression<PropertyContract, PropertyContractDto> PropertyContracteMap()
        {
            return CreateMap<PropertyContract, PropertyContractDto>();
        }
    }
}
