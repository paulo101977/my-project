﻿// //  <copyright file="IReservationConfirmationReadRepository.cs" company="TOTVS">
// //  Copyright (c) TOTVS. All rights reserved.
// //  </copyright>
namespace Thex.Infra.ReadInterfaces
{
    using System;
    using Tnf.Repositories;

    public interface IReservationConfirmationReadRepository : IRepository
    {
        bool AnyToBeBilledByReservation(long reservationId);
        bool AnyToBeBilledByBillingAccountId(Guid billingAccountId);
    }
}