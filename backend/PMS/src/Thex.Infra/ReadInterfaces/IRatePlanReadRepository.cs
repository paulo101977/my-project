﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.Domain.Entities;
using Thex.Domain.JoinMap;
using Thex.Dto;
using Tnf.Dto;

namespace Thex.Infra.ReadInterfaces
{
    public interface IRatePlanReadRepository
    {
        RatePlanDto RatePlanDtoRatePlanId(Guid ratePlanId);
        ListDto<RatePlanDto> GetAllRatePlanSearch(GetAllRatePlanDto request,int propertyId);
        RatePlan GetRatePlanById(Guid ratePlanId);
        bool VerifyNameAgreementSamePeriod(DateTime startDate, DateTime endDate, string agreementName, Guid ratePlanId, int propertyId);
        Task<List<RatePlanBaseJoinMap>> GetAllCompleteRatePlan(int propertyId, DateTime initialDate, DateTime finalDate, Guid? companyClientId, List<int?> roomTypeId);
        RatePlan GetByBillingAccountId(Guid billingAccountId);        
        Task<List<PropertyBaseRateHeaderHistory>> GetRatePlanHistoryByDate(Guid ratePlanId, DateTime dateTimeHistory);
        //Task<List<PropertyBaseRateHeaderHistory>> GetRatePlanHistoryById(Guid ratePlanId);
         Task<IListDto<PropertyBaseRateHeaderHistoryDto>> GetRatePlanHistoryById(Guid ratePlanId);
        bool IsBaseRateType(Guid ratePlanId);
        Task<IListDto<PropertyBaseRateHeaderHistoryDto>> GetRatePlanHistoryByHistoryId(Guid Id);
        Task<List<RatePlanDto>> GetAllBaseRateByPeriodAndPropertyId(DateTime startDate, DateTime endDate, int propertyId);
        bool DistributionCodeAvailable(string distributionCode, int propertyId, Guid ratePlanId);
        bool IsBillingAccountDoesNotHaveRatePlanCompanyClient(List<Guid> billingAccountIds);
        List<RatePlanBillingAccountDto> GetAllByBillingAccountIdList(List<Guid> billingAccountIdList);
    }
    
}
