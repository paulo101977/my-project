﻿// //  <copyright file="ICompanyClientContactPersonReadRepository.cs" company="TOTVS">
// //  Copyright (c) TOTVS. All rights reserved.
// //  </copyright>
namespace Thex.Infra.ReadInterfaces
{
    using System;
    using System.Collections.Generic;

    using Thex.Dto;

    public interface ICompanyClientContactPersonReadRepository
    {
        ICollection<CompanyClientContactPersonDto> GetContactDtoListByClientPersonId(Guid personId);
        CompanyClientContactPersonDto GetContactByReservationId(long reservationId);
    }
}