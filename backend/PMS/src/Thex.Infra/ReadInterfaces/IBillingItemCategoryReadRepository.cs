﻿using System;
using System.Collections.Generic;
using System.Text;
using Thex.Domain.Entities;
using Thex.Dto;
using Thex.Dto.BillingItemCategory;
using Tnf.Dto;

namespace Thex.Infra.ReadInterfaces
{
    public interface IBillingItemCategoryReadRepository
    {
        IListDto<BillingItemCategoryDto> GetAllGroupFixed();
        IListDto<BillingItemCategoryDto> GetItemsByGroupId(int groupId);

        IListDto<BillingItemCategoryDto> GetAllGroupByPropertyId(GetAllBillingItemCategoryDto request, int propertyId);
        IListDto<GetAllCategoriesForItemDto> GetAllCategoriesForItem(int propertyId); 
    }
}
