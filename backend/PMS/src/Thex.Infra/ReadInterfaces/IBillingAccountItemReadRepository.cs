﻿using System;
using System.Collections.Generic;
using Thex.Domain.Entities;
using Thex.Dto;
using System.Threading.Tasks;

namespace Thex.Infra.ReadInterfaces
{
    public interface IBillingAccountItemReadRepository
    {
        List<BillingAccountItem> GetAllByBillingAccountId(DefaultGuidRequestDto id);
        decimal GetAmountBalanceByBillingAccountId(Guid billingAccountId);
        List<BillingAccountItem> GetAllWithChildrenByBillingAccountId(DefaultGuidRequestDto id);
        bool BillingAccountItemsAlreadyReversed(List<Guid> billingAccountItemIds);
        bool BillingAccountHasBillingAccountItems(Guid billingAccountId);
        List<BillingAccountItem> GetAllWithChildrenExceptReversedItems(List<Guid> billingAccountItemIds);
        bool AnyReversalOrTaxByBillingAccountItemIdList(List<Guid> billingAccountItemIds);
        int CountBillingAccountItems(Guid billingAccountId, List<Guid> billingAccountItemIds);
        List<BillingAccountItem> GetAllWithChildrenExceptReversedItemsByBillingAccountIdList(List<Guid> billingAccountIdList);
        List<BillingAccountItem> GetAllByBillingAccountId(Guid billingAccountId);
        List<Guid> GetAllBillingAccountIdListByBillingAccountItemIdList(List<Guid> billingAccountItemIdList);
        List<BillingAccountItem> GetAllWithChildrenExceptReversedItemsByBillingAccountIdListAndItemIdList(List<Guid> billingAccountIdList, List<Guid> billingAccountItemIdList);
        bool AnyTaxByBillingAccountItemIdList(List<Guid> billingAccountItemIds);
        List<BillingAccountItem> GetAllChildrenByBillingAccountParentId(Guid billingAccountItemId);
        List<BillingAccountItem> GetAllWithChildren(List<Guid> billingAccountItemIds);
        bool AnyChildrenItem(List<Guid> billingAccountItemIds);
        List<BillingAccountItem> GetBillingAccountItemsById(List<Guid> billingAccountItemIds);
        bool HasLinkedInvoice(Guid id);
        bool HasLinkedInvoice(List<Guid> ids);
        List<BillingAccountItem> GetAllForSetInvoiceIdByBillingAccountId(Guid billingAccountId);
        Task<List<BillingAccountItem>> GetByBillingInvoiceIdAsync(Guid billingInvoiceId);
        List<BillingAccountItemDto> GetAllByBillingInvoiceId(Guid billingInvoiceId);
        List<BillingAccountItem> GetAllCreditWithInvoiceByBillingAccountId(Guid billingAccountId);
        bool AnyDailyDebit(Guid tenantId, DateTime systemDate);
        List<BillingAccountItemCreditAmountDto> GetBillingAccountItemCreditNoteLiByInvoiceId(Guid billingInvoiceId);
        List<BillingAccountItemTourismTaxDto> GetAllAccountItemTourismTaxByAccountIdList(List<Guid> billingAccountIdList);
        BillingAccountItem GetByIdWithBillingItem(Guid id);
        List<BillingAccountItem> GetAllByIdList(List<Guid> idList);
        List<BillingAccountItem> GetAllByBillingInvoiceIdList(List<Guid?> billingInvoiceIdList);
        List<BillingAccountItem> GetAllByPartialParentBillingAccountItemId(Guid partialParentBillingAccountItemId);
    }
}