﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using Thex.Domain.Entities;
using Thex.Dto;

namespace Thex.Infra.ReadInterfaces
{
    public interface ILevelRateHeaderReadRepository
    {
        ICollection<LevelRateHeaderDto> GetAllDto(Guid tenantId);
        LevelRateResultDto GetDtoById(Guid levelRateHeaderId);
        ICollection<LevelDto> GetAllLevelsAvailable(LevelRateRequestDto request, int propertyId);
        bool LevelIsAvailable(DateTime initialDate, DateTime endDate, Guid currencyId, Guid levelId, Guid levelHeaderId, int propertyId);
        LevelRateHeader GetById(Guid id);
        bool LevelRateAlreadyUsed(Guid levelRateHeaderId);
        ICollection<PropertyBaseRateByLevelRateResultDto> GetAllDtoForPropertyBaseRate(DateTime initialDate, DateTime endDate, Guid? currencyId);
        ICollection<LevelRateHeader> GetAllByLevelRateHeaderIdList(ICollection<Guid> levelRateHeaderIdList);
    }
}
