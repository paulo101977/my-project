﻿using System;
using System.Collections.Generic;
using System.Text;
using Thex.Domain.Entities;
using Thex.Dto;
using Thex.Dto.HousekeepingStatusProperty;

namespace Thex.Infra.ReadInterfaces
{
    public interface IRoomBlockingReadRepository
    {
        List<RoomBlocking> GetAllBlockedInPeriod(DateTime startBlock, DateTime endBlock, List<int> roomIds);
        List<RoomBlocking> GetAllBlockedInPeriod(DateTime startBlock, DateTime endBlock, int roomId);
        List<RoomBlocking> GetAllRoomBlockingForUnlock(DateTime startBlock, DateTime endBlock, int propertyId);
        List<RoomWithReservationsDto> GetReservationByRoomIds(List<int> roomIds, DateTime startBlock, DateTime endBlock);
        List<RoomBlockingDto> GetAllRoomsBlockedByPropertyId(int propertyId, DateTime initialDate, DateTime finalDate);
        List<RoomBlocking> GetAllRoomBlockingForUnlockByBlockingEndDate(DateTime blockingEndDate, int propertyId);
        List<RoomBlocking> GetAllRoomBlockingForBlockingByBlockingEndDate(DateTime blockingStartDate, int propertyId);
        List<int> GetAllBlockedRoomIdsByPeriod(DateTime startDate, DateTime endDate);
    }
}
