﻿// //  <copyright file="IChainReadRepository.cs" company="TOTVS">
// //  Copyright (c) TOTVS. All rights reserved.
// //  </copyright>
namespace Thex.Infra.ReadInterfaces
{
    using Thex.Domain.Entities;

    public interface IChainReadRepository
    {
        Chain GetChainByPropertyId(int propertyId);
        Chain GetChainById(int id);
    }
}