﻿using System;
using System.Collections.Generic;
using System.Text;
using Tnf.Dto;

namespace Thex.Infra.ReadInterfaces
{
    public interface IPowerBiReadRepository
    {
        string GetGroupIdByReportId(string ReportId, Guid? tenantId);
        string GetGroupIdByDashboardId(string DashboardId, Guid? tenantId);
    }
}
