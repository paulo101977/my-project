﻿// //  <copyright file="IPersonReadRepository.cs" company="TOTVS">
// //  Copyright (c) TOTVS. All rights reserved.
// //  </copyright>
namespace Thex.Infra.ReadInterfaces
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Thex.Domain.Entities;
    using Thex.Dto;
    using Thex.Dto.Person;
    using Tnf.Dto;
    using Tnf.Repositories;

    public interface IPersonReadRepository : IRepository
    {
        IListDto<SearchPersonsResultDto> GetPersonsBasedOnFilter(SearchPersonsDto request);
        IListDto<SearchPersonsResultDto> GetPersonsBasedOnFilterWithCompanyClient(SearchPersonsDto request, int companyId);
        IQueryable<SearchPersonsResultDto> GetDocumentsByPersonId(Guid personId);
        KeyValuePair<Guid, long>? GetPersonIdAndGuestIdByDocument(int documentTypeId, string document);
        List<PersonInformation> GetPersonInformationByPersonsIds(List<Guid> personsIds);
        List<ContactInformation> GetContactInformationByPersonsIds(List<Guid> personsIds);
        Person GetById(Guid personId);
        Person GetCompletePersonById(Guid personId);
        HeaderPersonDto GetHeaderByBillingAccountId(Guid billingAccountId);
        HeaderPersonDto GetHeaderByBillingInvoiceId(Guid billingInvoiceId);
        CompanyClientDto GetHeaderRpsById(Guid personId);
    }
}