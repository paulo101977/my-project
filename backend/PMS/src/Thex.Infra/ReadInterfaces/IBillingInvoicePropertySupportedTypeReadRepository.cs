using System;
using System.Collections.Generic;
using Thex.Domain.Entities;
using Thex.Dto;
using Tnf.Dto;

namespace Thex.Infra.ReadInterfaces
{
    public interface IBillingInvoicePropertySupportedTypeReadRepository
    {
        List<BillingInvoicePropertySupportedType> GetAllByPropertyId(int propertyId);
        IListDto<BillingInvoicePropertySupportedTypeDto> VerifyExistingBillingPropertyWithCodeService(int propertyId, Guid billingInvoicePropertyId, List<Guid> countrySubdvisionServiceIdList, List<int> billingItemIdList);
        IListDto<BillingInvoicePropertySupportedTypeDto> GetBillingPropertyListWithoutCodeService(int propertyId, Guid billingInvoicePropertyId, List<int> billingItemIdList);
        IListDto<BillingInvoicePropertySupportedTypeDto> GetBillingPropertyListWithoutCodeService(Guid billingInvoicePropertyId, Guid childBillingInvoicePropertyId, List<int> billingItemIdList);
        List<BillingInvoicePropertySupportedType> GetByType(int invoiceType, int propertyId);
    }
}
