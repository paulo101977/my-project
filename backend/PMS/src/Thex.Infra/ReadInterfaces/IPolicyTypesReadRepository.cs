﻿using System;
using System.Collections.Generic;
using System.Text;
using Thex.Domain.Entities;
using Thex.Dto;
using Tnf.Dto;

namespace Thex.Infra.ReadInterfaces
{
  public  interface IPolicyTypesReadRepository
    {
        IListDto<PolicyTypeDto> GetAllPolicyTypes();
    }
}
