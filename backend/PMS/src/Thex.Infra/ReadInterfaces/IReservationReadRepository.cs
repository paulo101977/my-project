﻿// //  <copyright file="IReservationReadRepository.cs" company="TOTVS">
// //  Copyright (c) TOTVS. All rights reserved.
// //  </copyright>
namespace Thex.Infra.ReadInterfaces
{
    using Thex.Domain.Entities;
    using Thex.Dto.Reservation;
    using Thex.Dto.ReservationCompanyClient;
    using Thex.Dto.ReservationGuest;
    using Thex.Dto;
    using Tnf.Dto;
    using Tnf.Repositories;
    using System;

    public interface IReservationReadRepository : IRepository
    {
        Reservation GetWithReservationItemsAndRoomsAndRoomTypes(DefaultLongRequestDto request);
        IListDto<SearchReservationResultDto> GetAllByFilters(SearchReservationDto request, int propertyId);
        IListDto<SearchReservationResultDto> GetAllByFiltersV2(SearchReservationDto request, int propertyId);
        SearchReservationResultDto GetReservationDetail(SearchReservationDto request, int propertyId);
        IListDto<ReservationDto> GetReservationsByPropertyId(RequestAllDto request, int propertyId);
        Reservation GetByReservationItemId(DefaultLongRequestDto request);
        ReservationHeaderResultDto GetReservationHeaderByReservationItemId(long reservationItemId);
        IListDto<ReservationCompanyClientResultDto> GetReservationCompanyClient(GetAllReservationCompanyClientDto request, int propertyId);
        IListDto<ReservationGuestResultDto> GetReservationGuest(GetAllReservationGuestDto request, int propertyId);
        Reservation GetReservationForCheckin(long id);
        ReservationHeaderResultDto GetReservationHeader(long reservationId);
        Guid? GetCompanyClientIdByReservationId(long reservationId);
        ReservationDto GetBusinessSourceAndMarketSegmentByReservationId(long id);
        Reservation GetById(long id);
    }
}