// //  <copyright file="ICountryPersonDocumentTypeReadRepository.cs" company="TOTVS">
// //  Copyright (c) TOTVS. All rights reserved.
// //  </copyright>
namespace Thex.Infra.ReadInterfaces
{
    using Thex.Dto;
    using Tnf.Repositories;

    public interface ICountryPersonDocumentTypeReadRepository : IRepository
    {
    }
}