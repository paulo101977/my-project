﻿using System;
using System.Collections.Generic;
using Thex.Dto;

namespace Thex.Infra.ReadInterfaces
{
    public interface IGuestReadRepository
    {
        List<GetAllForeingGuest> GetAllForeingGuestByFilters(DateTime startDate, DateTime endDate, List<int> listStatus);
    }
}
