﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.Domain.Entities;
using Thex.Dto;
using Tnf.Dto;

namespace Thex.Infra.ReadInterfaces
{
    public interface IPropertyMealPlanTypeRateReadRepository
    {
        List<PropertyMealPlanTypeRate> GetAllEntity(GetAllPropertyMealPlanTypeRateDto dto);
        IListDto<PropertyMealPlanTypeRateDto> GetAll(GetAllPropertyMealPlanTypeRateDto dto);
        PropertyMealPlanTypeRateHeaderDto GetById(Guid id);
        bool CheckConstraints(GetAllPropertyMealPlanTypeRateDto dto);
        bool ExistsForAllPeriod(Guid currencyId, int propertyId, List<DateTime> dates);
    }
}
