﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Thex.Dto;
using Tnf.Dto;


namespace Thex.Infra.ReadInterfaces
{
    public interface IAgreementTypeReadRepository
    {
        ListDto<AgreementTypeDto> GetAllAgreementType();
    }
}