﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.Dto;

namespace Thex.Infra.ReadInterfaces
{
    public interface ICustomerStationReadRepository
    {
        Task<ICollection<CustomerStationDto>> GetAllByCompanyClient(Guid companyClientId);
    }
}
