﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Thex.Dto;
using Tnf.Dto;

namespace Thex.Infra.ReadInterfaces
{
    public interface IChannelCodeReadRepository
    {
        Task<IListDto<ChannelCodeDto>> GetAllChannelsCodeDtoAsync();
        ChannelCodeDto GetDtoById(int id);
    }
}
