﻿using Thex.Dto;
using Tnf.Dto;

namespace Thex.Infra.ReadInterfaces
{
    public interface IBillingInvoiceModelReadRepository
    {
        IListDto<BillingInvoiceModelDto> GetAllVisible();
        int GetIdByBillingInvoiceTypeId(int billingInvoiceTypeId);
        int GetInvoiceTypeByModel(int billingInvoiceModelId);
    }
}
