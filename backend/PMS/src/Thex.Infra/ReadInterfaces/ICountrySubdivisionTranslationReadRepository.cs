﻿// //  <copyright file="ICountrySubdivisionTranslationReadRepository.cs" company="TOTVS">
// //  Copyright (c) TOTVS. All rights reserved.
// //  </copyright>
namespace Thex.Infra.ReadInterfaces
{
    using Tnf.Repositories;
    using Thex.Dto.CountrySubdivisionTranslation;
    using Tnf.Dto;
    using Thex.Dto;
    using System.Collections.Generic;

    public interface ICountrySubdivisionTranslationReadRepository : IRepository
    {
        AddressTranslationDto GetAddressTranslationByCityId(int? cityId, string languageIsoCode);

        IListDto<CountryDto> GetAllCountriesByLanguageIsoCode(GetAllCountryDto request);

        List<BaseCountrySubdvisionTranslationDto> GetCountryWithTranslations(string countryTwoLetterIsoCode);

        List<BaseCountrySubdvisionTranslationDto> GetStateWithTranslations(string translationName, int parentSubdivionId);
        List<BaseCountrySubdvisionTranslationDto> GetStateWithTranslationsByCode(string twoLetterIsoCode, int parentSubdivionId);

        List<BaseCountrySubdvisionTranslationDto> GetCityWithTranslations(string translationName, int parentSubdivionId);

        List<BaseCountrySubdvisionTranslationDto> GetCountrySubdivisionWithTranslations(int subdivisionId);

        IListDto<CountrySubdivisionTranslationDto> GetNationalities(GetAllCountrySubdivisionTranslationDto request);

        CountrySubdivisionTranslationDto GetNationalityByIdAndProperty(int nationalityId, int propertyId);
        int GetIdByCountryName(string countryName);
    }
}