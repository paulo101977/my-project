﻿using Thex.Dto;
using Thex.Dto.RoomStatus;
using Tnf.Dto;

namespace Thex.Infra.ReadInterfaces
{
    public interface IRoomOccupiedReadRepository
    {
       bool RoomIdUsed(int roomId, long reservationId);
    }
}
