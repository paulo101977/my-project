﻿// //  <copyright file="IDocumentReadRepository.cs" company="TOTVS">
// //  Copyright (c) TOTVS. All rights reserved.
// //  </copyright>
namespace Thex.Infra.ReadInterfaces
{
    using System;
    using System.Collections.Generic;
    using Thex.Common.Enumerations;
    using Thex.Domain.Entities;
    using Thex.Dto;

    public interface IDocumentReadRepository
    {
        ICollection<DocumentDto> GetDocumentDtoListByPersonId(Guid owerId);
        string GetDocument(Guid owerId, DocumentTypeEnum type);
        string GetDocumentForPlasticBrand(int billingItemId, DocumentTypeEnum type);
        Document GetByOwnerId(Guid ownerId);
    }
}