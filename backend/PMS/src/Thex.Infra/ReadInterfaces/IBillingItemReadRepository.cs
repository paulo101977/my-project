﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using Thex.Domain.Entities;
using Thex.Dto;
using Thex.Dto.BillingItem;
using Tnf.Dto;

namespace Thex.Infra.ReadInterfaces
{
    public interface IBillingItemReadRepository
    {
        BillingItemServiceDto GetBillingItem(long id, long propertyId);
        IListDto<GetAllBillingItemServiceDto> GetAllBillingItemService(long propertyId, bool? isActive);
        IListDto<GetAllBillingItemServiceDto> GetAllBillingItemServiceByFilters(SearchBillingItemDto requestDto, long propertyId);
        IListDto<GetAllBillingItemTaxDto> GetAllBillingItemTax(long propertyId);
        IListDto<GetAllBillingItemForAssociateTaxDto> GetAllBillingItemForAssociateTax(long propertyId);
        IListDto<GetAllBillingItemForAssociateServiceDto> GetAllBillingItemForAssociateService(long propertyId);
        List<BillingItemServiceAndTaxAssociateDto> GetBillingTaxForBiilingItem(int id);
        List<Guid> GetBillingTaxByBillingItemId(int id);
        bool ValidateActiveForTax(int id);
        BillingItemPaymentTypeDto GetByPaymentTypeIdAndAcquirerId(int propertyId, int paymentTypeId, Guid acquirerId);
        BillingItemPaymentTypeDto GetByPaymentTypeId(int propertyId, int paymentTypeId);
        IListDto<BillingItemPaymentTypeDto> GetAllPaymentTypeIdByPropertyId(int propertyId);
        List<BillingItem> GetAllByExpression(Expression<Func<BillingItem, bool>> exp);
        List<BillingItemPaymentCondition> GetAllBillingItemPaymentConditionByBillingItemId(int billingItemId);
        bool AnyBillingItemByExpression(Expression<Func<BillingItem, bool>> exp);
        IListDto<BillingItemServiceLancamentoDto> GetAllServiceWithAssociateTaxForDebitRealease(int propertyId, GetAllBillingItemServiceForRealase request, DateTime propertyDate);
        IList<BillingItemServiceAndTaxAssociateDto> GetTaxesForServices(IList<int> serviceIdsList, DateTime? propertyDate = null);
        bool AnyBillingItemByBillingItemType(int billingItemId, int billingItemType, bool ignoreFilters = false);
        IListDto<BillingItemPlasticBrandCompanyClientDto> GetPlasticBrandWithCompanyClientByPaymentType(int paymentTypeId);
        decimal GetSumBillingAccountItemsByBillingAccountId(Guid billingAccountId);
        decimal GetSumBillingAccountItemsByBillingAccountIdList(List<Guid> billingAccountId);
        bool CheckPaymentTypeByBillingItemId(int billingItemId, int paymentTypeId);
        bool CheckIfExceedingMaximumInstallmentsQuantity(int billingItemId, int installmentsQuantity);
        List<BillingItemDto> GetAllServiceFilterTypeDailyByProperty(int propertyId);
        ListDto<BillingItemWithParametersDocumentsDto> GetAllServiceWithParameters(int propertyId);
        List<BillingItem> GetBillingItemsByIds(List<int> billingItemsIds);
        bool IsServiceAndExistSupportedType(int billingItemId);
        bool IntegrationCodeIsAvailable(string integrationCode, int billingItemId, int propertyId);
        string GetPlasticBrandFormattedByBillingItemId(int billingItemId);
        BillingItem GetById(int id);
        List<BillingItemDto> GetAllByTypeId(int typeId, bool isActive = true);
    }
}
