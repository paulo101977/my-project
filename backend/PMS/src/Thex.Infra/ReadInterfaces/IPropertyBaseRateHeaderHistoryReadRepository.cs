﻿namespace Thex.Infra.ReadInterfaces
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Thex.Domain.Entities;
    using Thex.Dto;
    using Tnf.Repositories;

    public interface IPropertyBaseRateHeaderHistoryReadRepository
    {
        Task<List<PropertyBaseRateHeaderHistory>> GetAll(Guid? pRatePlanId);
        IQueryable<PropertyBaseRateHeaderHistoryDto> GetAllPropertyBaseRateHeaderHistoryByRatePlanId(Guid pRatePlanId);
        Task<List<PropertyBaseRateHeaderHistoryDto>> GetAllPropertyBaseRateHeaderHistoryByPropertyId(int propertyId);
    }
}
