﻿// //  <copyright file="IReservationBudgetReadRepository.cs" company="TOTVS">
// //  Copyright (c) TOTVS. All rights reserved.
// //  </copyright>
namespace Thex.Infra.ReadInterfaces
{
    using Thex.Domain.Entities;
    using Tnf.Repositories;
    using System.Collections.Generic;
    using System;

    public interface IReservationBudgetReadRepository : IRepository
    {
        List<ReservationBudget> GetAllByReservationItemId(long reservationItemId);
        List<ReservationBudget> GetAllByReservationId(long reservationId);
    }
}