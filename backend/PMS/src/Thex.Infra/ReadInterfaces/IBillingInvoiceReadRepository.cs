﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.Domain.Entities;
using Thex.Dto;

namespace Thex.Infra.ReadInterfaces
{
    public interface IBillingInvoiceReadRepository
    {
        InvoiceDto GetDtoById(Guid invoiceId);
        List<InvoiceDto> GetAllByBillingAccountIdList(List<Guid> billingAccountIdList);
        List<InvoiceDto> GetAllByBillingAccountId(Guid billingAccountId);
        Task<Byte[]> GeneratePDFBillingInvoice(Guid id);
        Task<List<InvoiceDto>> GetAllByFilters(int propertyId, DateTime? initialDate, DateTime? finalDate, int? initialNumber, int? finalNumber);
        string GetIntegratorIdById(Guid invoiceId);
        BillingInvoice GetById(Guid id);
        List<BillingInvoice> GetAllByIntegratorId(string integratorId);
    }
}
