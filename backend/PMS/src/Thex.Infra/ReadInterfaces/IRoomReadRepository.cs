﻿//  <copyright file="IRoomReadRepository.cs" company="TOTVS">
//  Copyright (c) TOTVS. All rights reserved.
//  </copyright>
namespace Thex.Domain.Interfaces.Repositories
{
    using System.Collections.Generic;
    
    using Thex.Dto.Rooms;
    using Tnf.Dto;
    using System;
    using Thex.Dto.Base;
    using Thex.Dto;
    using Tnf.Repositories;

    public interface IRoomReadRepository : IRepository
    {
        ListDto<RoomDto> GetRoomsByPropertyId(GetAllRoomsDto request, int propertyId);
        ListDto<RoomDto> GetChildRoomsByParentRoomId(GetAllRoomsDto request, int parentRoomId);
        ListDto<RoomDto> GetParentRoomsByPropertyId(GetAllRoomsDto request, int propertyId);
        ListDto<RoomDto> GetRoomsWithoutChildrenByPropertyId(GetAllRoomsDto request, int propertyId);
        ListDto<RoomDto> GetRoomsWithoutParentAndChildrenByPropertyId(GetAllRoomsDto request, int propertyId);
        ListDto<RoomDto> GetAllRoomsByPropertyAndPeriod(GetAllRoomsByPropertyAndPeriodDto request, int propertyId);
        ListDto<RoomDto> GetAllAvailableByPeriodOfRoomType(PeriodDto request, int propertyId, int roomTypeId);
    }
}