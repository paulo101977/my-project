﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Thex.Domain.Entities;
using Thex.Dto;
using Tnf.Dto;

namespace Thex.Infra.ReadInterfaces
{
    public interface IChannelReadRepository
    {
        Task<IListDto<ChannelDto>> GetAllChannelsDtoByTenantIdAsync(GetAllChannelDto requestDto, Guid tenantId);
        Task<List<Channel>> GetAllChannelsByTenantIdAsync(Guid tenantId);
        Channel GetById(Guid id);
        ChannelDto GetDtoById(Guid id);
        IListDto<ChannelDto> SearchChannels(string search);
    }
}
