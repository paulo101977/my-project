﻿using System;
using System.Collections.Generic;
using Thex.Domain.Entities;
using Thex.Dto.PropertyCurrencyExchange;
using Tnf.Dto;

namespace Thex.Infra.ReadInterfaces
{
    public interface IPropertyCurrencyExchangeReadRepository
    {
        ListDto<QuotationCurrentDto> GetAllQuotationByPeriod(int propertyId, DateTime? startDate, Guid currencyId, DateTime? endDate);
        QuotationCurrentDto GetQuotationByCurrencyId(int propertyId, Guid currencyId);
        List<QuotationCurrentDto> GetQuotationCurrentByPropertyId(int propertyId, DateTime initialDate, DateTime endDate);
        bool IsQuotationByPeriodList(DateTime quotationStartDate, DateTime quotationEndDate, Guid CurrencyId, int propertyId);
        List<PropertyCurrencyExchange> GetAllByIds(List<Guid> ids);

        void DeleteByPeriodList(InsertQuotationByPeriodDto quotationByPeriod);
        decimal? GetExchangeRateById(Guid id);
        List<QuotationCurrentDto> GetCurrentQuotationList(int propertyId, Guid? currencyId);
        List<QuotationCurrentDto> GetPropertyQuotationByPropertyId(int propertyId, DateTime initialDate, DateTime endDate, bool isToDolar = false);
    }
}
