﻿// //  <copyright file="IPropertyReadRepository.cs" company="TOTVS">
// //  Copyright (c) TOTVS. All rights reserved.
// //  </copyright>
namespace Thex.Infra.ReadInterfaces
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Thex.Domain.Entities;
    using Thex.Dto;
    using Tnf.Dto;
    using Tnf.Repositories;

    public interface IPropertyReadRepository : IRepository
    {
        IListDto<PropertyDto> GetAll(IRequestAllDto request, bool central = false);
        PropertyDto GetByPropertyId(int propertyId);

        int GetTotalOfRooms(int propertyId);

        Guid GetTenantByPropertyId(int propertyId);

        bool IsValidTenantIdByPropertyId(int propertyId, Guid tenant);

        Property GetByPropertyIdWithTenant(int propertyId, bool ignoreQueryFilters = false);

        CountryLanguageDto GetPropertyCulture(int propertyId);

        string GetPropertyDefaultCurrency(int propertyId);

        Guid GetPropertyUId(int propertyId);

        string GetPropertyCountryTwoLetterIsoCode(int propertyId);

        GetPropertySiba GetByPropertyId(List<int> listIds);

        Guid GetUIdById(int id);

        Task<PropertyInformationForDashboardDto> GetInformationsForDashboardAsync(int propertyId);
    }
}
