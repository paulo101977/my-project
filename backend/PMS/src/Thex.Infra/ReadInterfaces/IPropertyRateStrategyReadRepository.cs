﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.Dto;
using Tnf.Dto;

namespace Thex.Infra.ReadInterfaces
{
    public interface IPropertyRateStrategyReadRepository
    {
        IListDto<GetAllPropertyRateStrategyDto> GetAllByPropertyId(int propertyId);
        Task<IList<GetAllPropertyRateStrategyWithRatePlanDto>> GetAllWithRatePlanByPropertyIdAsync(int propertyId);
        Task<PropertyRateStrategyDto> GetPropertyRateStrategyById(Guid propertyRateStrategyId);
    }
}
