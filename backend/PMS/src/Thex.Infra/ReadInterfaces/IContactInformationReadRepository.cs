﻿using System;
using System.Collections.Generic;
using System.Text;
using Thex.Domain.Entities;

namespace Thex.Infra.ReadInterfaces
{
    public interface IContactInformationReadRepository
    {
        List<ContactInformation> GetAllByOwnerId(Guid ownerId);
    }
}
