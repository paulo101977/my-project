﻿// //  <copyright file="ILocationReadRepository.cs" company="TOTVS">
// //  Copyright (c) TOTVS. All rights reserved.
// //  </copyright>
namespace Thex.Infra.ReadInterfaces
{
    using System;
    using System.Collections.Generic;
    using Thex.Dto;

    public interface ILocationReadRepository
    {
        ICollection<LocationDto> GetLocationDtoListByPersonId(Guid owerId, string languageIsoCode);
        LocationDto GetLocationDtoListByPropertyUId(int pPropertyId);
        ICollection<LocationDto> GetLocationDtoListByFilters(List<int> propertyIds, string languageIsoCode);
    }
}