﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Thex.Domain.Entities;
using Thex.Domain.JoinMap;
using Thex.Dto;
using Tnf.Dto;

namespace Thex.Infra.ReadInterfaces
{
    public interface IRateTypeReadRepository
    {
        ListDto<RateTypeDto> GetAllRateType(); 
    }
}
