﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Thex.Infra.ReadInterfaces
{
    public interface IPropertyCompanyClientCategoryRepository
    {
        void ToggleAndSaveIsActive(Guid id);
        void Delete(Guid id);
    }
}
