﻿using System;
using System.Collections.Generic;
using System.Text;
using Thex.Domain.Entities;

namespace Thex.Infra.ReadInterfaces
{
    public interface IPropertyAuditProcessReadRepository
    {
        PropertyAuditProcess GetPropertyAuditProcessById(Guid id);
        PropertyAuditProcess GetPropertyAuditProcessCurrentByPropertyId(int propertyId);
    }
}
