﻿using System;
using System.Collections.Generic;
using System.Text;
using Thex.Domain.Entities;
using Thex.Dto;

namespace Thex.Infra.ReadInterfaces
{
    public interface IGuestRegistrationDocumentReadRepository
    {
        ICollection<GuestRegistrationDocument> GetAllGuestRegistrationDocumentByGuestRegistrationId(Guid guestRegistrationId);
        GuestRegistrationDocument GetById(Guid id);
        ICollection<GuestRegistrationDocumentDto> GetAllDocumentsByGuestRegistrationId(Guid guestRegistrationId);
    }
}
