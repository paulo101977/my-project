﻿using Thex.Dto;
using Thex.Dto.RoomStatus;
using Tnf.Dto;

namespace Thex.Infra.ReadInterfaces
{
    public interface IRoomStatusReadRepository
    {
        ListDto<RoomStatusDto> GetAll(GetAllRoomStatusDto getAllRoomStatusDto, int propertyId);
    }
}
