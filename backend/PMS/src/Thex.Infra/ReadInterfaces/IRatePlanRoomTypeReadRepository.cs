﻿// //  <copyright file="IRatePlanRoomTypeReadRepository.cs" company="TOTVS">
// //  Copyright (c) TOTVS. All rights reserved.
// //  </copyright>
namespace Thex.Infra.ReadInterfaces
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Thex.Dto;
    using Thex.Dto.RatePlan;

    public interface IRatePlanRoomTypeReadRepository
    {
        Task<RatePlanWithRatePlanRoomTypeDto> GetByRatePlanIdAndRoomTypeId(int roomTypeId, Guid ratePlanId);
    }
}