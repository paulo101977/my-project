﻿// //  <copyright file="RoomTypeReadRepository.cs" company="TOTVS">
// //  Copyright (c) TOTVS. All rights reserved.
// //  </copyright>
namespace Thex.Infra.ReadInterfaces
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Thex.Domain.Entities;
    using Thex.Dto;
    using Thex.Dto.Availability;
    using Thex.Dto.Base;
    using Thex.Dto.RoomType;
    using Thex.Dto.RoomTypes;
    using Tnf.Dto;
    using Tnf.Repositories;

    public interface IRoomTypeReadRepository : IRepository
    {
        IListDto<RoomTypeDto> GetRoomTypesByPropertyId(GetAllRoomTypesDto request, int propertyId);
        IListDto<RoomTypeOverbookingDto> GetAllRoomTypesByPropertyAndPeriodDto(GetAllRoomTypesByPropertyAndPeriodDto request, int propertyId);
        RoomTypeOverbookingDto GetRoomTypeAndRoomWithOverbooking(PeriodDto request, int propertyId, int roomTypeId);
        List<AvailabilityRoomTypeRowDto> GetAvailabilityRoomTypeRow(int propertyId, int? roomTypeId = null);
        Task<List<AvailabilityRoomTypeRowDto>> GetAvailabilityRoomTypeRowAsync(int propertyId, int? roomTypeId = null);
        RoomTypeDto GetRoomTypeById(int roomTypeId);
        RoomTypeDto GetRoomTypeByRoomId(int roomId);
        Task<IListDto<RoomTypeDto>> GetAllRoomTypesByPropertyIdAsync(int propertyId);
        RoomTypeDto GetRoomTypeByPropertyIdRoomId(int propertyId, int roomid);
        Task<List<int>> GetAllRoomTypeIdsByPropertyIdAsync(int propertyId, int? adultCapacity, List<int> childCapacity);
        Task<List<RoomTypeOverbookingperDateSqlDto>> GetRoomTypeOverbookinperDateData(DateTime initialDate, DateTime finalDate, int propertyId, int? roomTypeId, byte? childCapacity = null, byte? adultCapacity = null);
        Task<List<RoomTypeOverbookingSqlDto>> GetRoomTypeOverbookingData(DateTime initialDate, DateTime finalDate, int propertyId, int? roomTypeId, byte? childCapacity = null, byte? adultCapacity = null);
        bool DistributionCodeAvailable(string distributionCode, int propertyId, int roomTypeId);
        RoomType GetRoomTypeByReservationItemId(int reservationItemId);
        bool ValidateIfRoomTypeIsInCheckIn(int roomTypeId);
    }
}