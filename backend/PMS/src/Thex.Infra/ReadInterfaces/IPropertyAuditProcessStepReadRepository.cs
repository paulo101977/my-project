﻿using System;
using System.Collections.Generic;
using System.Text;
using Thex.Domain.Entities;

namespace Thex.Infra.ReadInterfaces
{
    public interface IPropertyAuditProcessStepReadRepository
    {
        PropertyAuditProcessStep GetPropertyAuditProcessStepById(Guid id);
        PropertyAuditProcessStep GetPropertyAuditProcessStepByPropertyAuditProcessId(Guid propertyAuditProcessId);
        void SaveChanges();
    }
}
