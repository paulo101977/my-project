﻿using System;
using Thex.Domain.Entities;

namespace Thex.Infra.ReadInterfaces
{
    public interface ISibaIntegrationReadRepository
    {
        SibaIntegration GetByPropertyId(Guid propertyUId);
    }
}
