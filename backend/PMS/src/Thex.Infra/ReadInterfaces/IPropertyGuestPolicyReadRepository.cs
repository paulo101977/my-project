﻿using System;
using System.Collections.Generic;
using Thex.Dto;

namespace Thex.Infra.ReadInterfaces
{
    public interface IPropertyGuestPolicyReadRepository
    {
        List<PropertyGuestPolicyDto> GetAll();
        PropertyGuestPolicyDto GetById(Guid id);
    }
}
