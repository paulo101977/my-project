﻿//  <copyright file="PersonReadRepository.cs" company="TOTVS">
//  Copyright (c) TOTVS. All rights reserved.
//  </copyright>
using System;
using Thex.Domain.Entities;
using Thex.Dto;
using Thex.Dto.GuestRegistration;
using Tnf.Dto;
using Tnf.Repositories;

namespace Thex.Infra.ReadInterfaces
{
    public interface IGuestRegistrationReadRepository : IRepository
    {
        GuestRegistrationResultDto GetGuestRegistrationByGuestReservationItemId(long propertyId, long guestReservationItemId, string languageIsoCode);
        GuestRegistrationResultDto GetLastGuestRegistrationByGuestReservationItemId(long propertyId, long guestReservationItemId, string languageIsoCode);
        GuestRegistrationResultDto GetGuestRegistrationFilledByGuestReservationAndPerson(long propertyId, long guestReservationItemId, string languageIsoCode);
        GuestRegistrationResultDto GetGuestRegistrationBasedOnFilter(SearchGuestRegistrationsDto request);
        IListDto<GuestRegistrationResultDto> GetAllGuestRegistrationBasedOnFilter(SearchGuestRegistrationsDto request);
        IListDto<GuestRegistrationResultDto> GetAllGuestRegistrationByReservationItemId(long propertyId, long reservationItemId, string languageIsoCode);
        bool GuestRegistrationHasDocument(Guid guestRegistrationId);
        IListDto<GuestRegistrationResultDto> GetAllResponsibles(long propertyId, long reservationId);
        bool IsChild(long id);
        IListDto<GuestRegistrationResultDto> GetAllResponsibleFromReservation(long reservationId);
        string GetNationalityByGuestReservationItemId(long guestReservationItemId);
        GuestRegistrationResultDto GetById(Guid guestRegistrationId);
        Guid? GetIdByGuestReservationItemId(long guestReservationItemId);
        GuestRegistration GetByGuestId(long guestId);



    }
}