﻿
using Thex.Domain.Entities;

namespace Thex.Infra.ReadInterfaces
{
    public interface IBrandReadRepository
    {
        Brand GetById(int id);
    }
}
