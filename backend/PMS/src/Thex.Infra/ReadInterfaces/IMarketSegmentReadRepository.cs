﻿// //  <copyright file="IMarketSegmentReadRepository.cs" company="TOTVS">
// //  Copyright (c) TOTVS. All rights reserved.
// //  </copyright>
namespace Thex.Infra.ReadInterfaces
{
    using Thex.Dto;

    using Tnf.Dto;

    public interface IMarketSegmentReadRepository
    {
        IListDto<MarketSegmentDto> GetAllByChainId(GetAllMarketSegmentDto request, int chainId);
        MarketSegmentDto GetById(int id);
        bool ValidateMarketSegmentWithReservation(int marketSegmentId, int propertyId);
        bool ValidateIfDescriptionAlreadyExists(MarketSegmentDto dto, int chainId);
    }
}