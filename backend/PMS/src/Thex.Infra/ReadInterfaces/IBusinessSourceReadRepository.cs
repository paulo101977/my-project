﻿// //  <copyright file="IBusinessSourceReadRepository.cs" company="TOTVS">
// //  Copyright (c) TOTVS. All rights reserved.
// //  </copyright>
namespace Thex.Infra.ReadInterfaces
{
    using Thex.Dto;

    using Tnf.Dto;

    public interface IBusinessSourceReadRepository
    {
        IListDto<BusinessSourceDto> GetAllByChainId(GetAllBusinessSourceDto request, int chainId);
        bool ValidateBusinessSourceWithReservation(int businessSourceId, int propertyId);
        bool ValidateIfDescriptionAlreadyExists(BusinessSourceDto dto, int chainId);
    }
}