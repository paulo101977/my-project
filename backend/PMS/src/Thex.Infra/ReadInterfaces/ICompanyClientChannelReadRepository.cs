﻿using System;
using System.Collections.Generic;
using System.Text;
using Thex.Domain.Entities;
using Thex.Dto;
using Tnf.Dto;

namespace Thex.Infra.ReadInterfaces
{
    public interface ICompanyClientChannelReadRepository
    {
        CompanyClientChannel GetbyId(Guid id);
        ICollection<CompanyClientChannel> GetAllByCompanyClientId(Guid companyClientId);
        CompanyClientChannelDto GetDtoByid(Guid id);
        IListDto<CompanyClientChannelDto> GetAllDtoByCompanyClientId(Guid companyClientId);
        bool AssociationExists(string companyId, Guid channelId);
        bool AllCompanyClientChannelAreAvailable(ICollection<CompanyClientChannelDto> companyClientList);
        ICollection<Guid> GetAllCompanyClientChannelIdByCompanyClientId(Guid companyClientId);
    }
}
