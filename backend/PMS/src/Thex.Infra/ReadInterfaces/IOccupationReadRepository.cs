﻿// //  <copyright file="IOccupationReadRepository.cs" company="TOTVS">
// //  Copyright (c) TOTVS. All rights reserved.
// //  </copyright>
namespace Thex.Infra.ReadInterfaces
{
    using Thex.Domain.Entities;

    public interface IOccupationReadRepository
    {
        Occupation GetOccupationByNameAndCompanyId(string name, int companyId);
    }
}