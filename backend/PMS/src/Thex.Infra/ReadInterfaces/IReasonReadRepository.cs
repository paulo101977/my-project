﻿// //  <copyright file="IReasonReadRepository.cs" company="TOTVS">
// //  Copyright (c) TOTVS. All rights reserved.
// //  </copyright>
namespace Thex.Infra.ReadInterfaces
{
    using System.Collections.Generic;
    using Thex.Common.Enumerations;

    using Thex.Dto;

    using Tnf.Dto;

    public interface IReasonReadRepository
    {
        IListDto<ReasonDto> GetAllByChainIdAndReasonCategory(GetAllReasonDto request, int chainId, ReasonCategoryEnum reasonCategoryEnum);
        IListDto<ReasonDto> GetAllByPropertyIdAndReasonCategoryId(GetAllReasonDto request, int propertyId, int reasonCategoryId);
        IListDto<ReasonDto> GetAllByPropertyId(GetAllReasonDto request, int PropertyId);
        ReasonDto GetReasonByReasonId(int reasonId);
        bool CheckIfReasonIsReversal(int reasonId);
        bool CheckIfReasonIsToReopenBillingAccount(int reasonId);
        bool CheckIfReasonIsDiscount(int reasonId);
        IList<ReasonDto> GetAllByPropertyIdAndReasonCategoryId(int propertyId, int reasonCategoryId);
        bool CheckIfReasonIsBillingInvoiceCancel(int reasonId);
        string GetReasonNameById(int id);
    }
}