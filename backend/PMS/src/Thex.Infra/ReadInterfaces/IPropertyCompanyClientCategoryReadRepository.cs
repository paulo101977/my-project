﻿using System;
using System.Collections.Generic;
using System.Text;
using Thex.Dto;
using Tnf.Dto;

namespace Thex.Infra.ReadInterfaces
{
    public interface IPropertyCompanyClientCategoryReadRepository
    {
        IListDto<PropertyCompanyClientCategoryDto> GetAllByPropertyId(GetAllPropertyCompanyClientCategoryDto request, int propertyId);
        bool Exists(string categoryName, int propertyId, Guid id);
        bool HasClient(Guid id);
    }
}
