﻿// //  <copyright file="IReservationItemReadRepository.cs" company="TOTVS">
// //  Copyright (c) TOTVS. All rights reserved.
// //  </copyright>
namespace Thex.Infra.ReadInterfaces
{
    using Thex.Dto.ReservationItem;
    using Thex.Dto;
    using Tnf.Dto;
    using Tnf.Repositories;
    using System.Collections.Generic;
    using Thex.Domain.Entities;
    using System;
    using System.Threading.Tasks;

    public interface IReservationItemReadRepository : IRepository
    {
        IListDto<ReservationItemDto> GetAllByPropertyAndReservation(RequestAllDto request, long reservationId);
        IListDto<AccommodationsByReservationIdResultDto> GetAllAccommodations(RequestAllDto request, long propertyId, long reservationId);
        IListDto<GuestGuestRegistrationResultDto> GetAllGuestGuestRegistration(long propertyId, long reservationItemId, string language);
        void Detach();
        List<ReservationItem> GetAvailabilityReservationsItemsByPeriodAndPropertyId(DateTime initialDate, DateTime finalDate, int propertyId, bool? completeReservationItemInformations = true, int? roomTypeId = null);
        Task<List<ReservationItem>> GetAvailabilityReservationsItemsByPeriodAndPropertyIdAsync(DateTime initialDate, DateTime finalDate, int propertyId, bool? completeReservationItemInformations = true, int? roomTypeId = null);
        List<ReservationItem> GetAvailabilityReservationsItemsWithMainGuest(DateTime initialDate, DateTime finalDate, int propertyId, int roomTypeId);
        ReservationItemDto GetDtoById(long reservationItemId);
        ReservationItemDto GetByIdNoTracking(long reservationItemId);
        IList<ReservationItemDto> GetAllDtoByReservationId(long reservationId);
        List<GuestReservationItem> GetGuestReservationItemByReservationItemId(long reservationItemId);
        List<ReservationItem> GetReservationItemListWithoutDailyByCheckinDate(int propertyId, DateTime systemDate);
        ReservationItemSlipReservationDto GetSlipReservation(long reservationId, int propertyId);
        ReservationItem GetByBillingAccountdId(Guid id);
        List<GuestReservationItem> GetGuestReservationItemByReservationItemIdList(List<long> reservationItemIdList);
        bool IsReservationItensDoesNotHaveRatePlan(List<long?> ids);
        ReservationItem GetReservationItemParent(long reservationId, long reservationItemId);
        bool IsReservationItemStatusConfirmed(long reservationItemId);
        bool IsEarlyDayUse(DateTime systemDate, long reservationItemId);
        ReservationItem GetById(long reservationItemId);
        Dictionary<DateTime, int> GetNumberOfAdultGuestAndBudgetDayByReservationId(ProformaRequestDto requestDto);
        string GetIntegrationIdById(long reservationId);
        string GetIntegrationIdByReservationId(long reservationId);
        ReservationItemDetailsDto GetDetailsDtoById(long id);
        Task<List<ReservationItemDto>> GetReservationsForDashboardAsync(DateTime systemDate, int propertyId);
    }
}
