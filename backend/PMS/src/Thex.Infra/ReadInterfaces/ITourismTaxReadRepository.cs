﻿using System;
using Thex.Dto;

namespace Thex.Infra.ReadInterfaces
{
    public interface ITourismTaxReadRepository
    {
        TourismTaxDto GetByPropertyId();
        Guid GetIdByBillintItemId(int billingItemId);
    }
}
