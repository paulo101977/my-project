﻿// //  <copyright file="IPropertyPremiseHeaderReadRepository.cs" company="TOTVS">
// //  Copyright (c) TOTVS. All rights reserved.
// //  </copyright>
namespace Thex.Infra.ReadInterfaces
{
    using Tnf.Dto;
    using Tnf.Repositories;
    using Thex.Dto;
    using System;
    using Thex.Domain.Entities;

    public interface IPropertyPremiseHeaderReadRepository : IRepository
    {
        IListDto<PropertyPremiseHeaderListDto> GetAll(GetAllPropertyPremiseHeaderDto request);
        PropertyPremiseHeaderListDto GetDtoById(Guid propertyPremiseHeaderId);
        bool CheckPremiseHeaderConstraints(PropertyPremiseHeaderDto dto);
        PropertyPremiseHeader GetByIdWithPremiseList(Guid id);
    }
}