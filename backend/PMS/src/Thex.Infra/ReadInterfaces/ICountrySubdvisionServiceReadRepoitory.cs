﻿using System;
using System.Collections.Generic;
using System.Text;
using Thex.Dto;
using Tnf.Dto;

namespace Thex.Infra.ReadInterfaces
{
    public interface ICountrySubdvisionServiceReadRepository
    {
        ListDto<CountrySubdvisionServiceDto> GetAllByPropertyId(int propertyId);
    }
}
