﻿// //  <copyright file="ICompanyReadRepository.cs" company="TOTVS">
// //  Copyright (c) TOTVS. All rights reserved.
// //  </copyright>
namespace Thex.Infra.ReadInterfaces
{
    using System;
    using Thex.Domain.Entities;
    using Thex.Dto;

    public interface ICompanyReadRepository
    {
        int GetCompanyIdByPropertyId(int propertyId);
        int GetCompanyIdByClientId(Guid clientId);
        Company GetCompanyById(int companyId);
    }
}