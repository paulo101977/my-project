﻿// //  <copyright file="ICompanyClientReadRepository.cs" company="TOTVS">
// //  Copyright (c) TOTVS. All rights reserved.
// //  </copyright>
namespace Thex.Infra.ReadInterfaces
{
    using System;
    using System.Collections.Generic;
    using Thex.Dto;

    using Tnf.Dto;

    public interface ICompanyClientReadRepository
    {
        IListDto<GetAllCompanyClientResultDto> GetAllByCompanyId(GetAllCompanyClientDto request, int companyId);
        CompanyClientDto GetCompanyClientDtoById(Guid id);
        CompanyClientDto GetCompanyClientDtoByReservationId(long id);
        IListDto<GetAllCompanyClientSearchResultDto> GetAllBasedOnFilter(GetAllCompanyClientSearchDto request, int companyId);

        IListDto<GetAllCompanyClientContactInformationSearchResultDto> GetAllContactPersonByCompanyClientId(GetAllCompanyClientContactInformationSearchDto request, int companyId, Guid companyClientId);
        string GetCompanyClientTradeNameById(Guid id);
        IListDto<CompanyClientDto> GetAcquirerCompanyClient(GetAllCompanyClientDto request, int companyId);
        string GetTradeNameById(Guid id);
        bool ExternalCodeIsAvailable(string externalCode, Guid companyClientId, int chainId);
        GetAllCompanyClientResultDto GetByIdWithDocument(Guid id);
        bool HasCompanyClient(Guid id);
    }
}