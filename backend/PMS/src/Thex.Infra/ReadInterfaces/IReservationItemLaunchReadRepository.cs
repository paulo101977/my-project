﻿using System.Collections.Generic;
using Thex.Domain.Entities;

namespace Thex.Infra.ReadInterfaces
{
    public interface IReservationItemLaunchReadRepository
    {
        List<ReservationItemLaunch> GetAllByGuestReservationItemIdList(List<long?> guestReservationItemIdList);
    }
}
