﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Thex.Common.Enumerations;
using Thex.Dto;
using Thex.Dto.Report;
using Thex.Dto.Reservation;
using Thex.Dto.ReservationReport;
using Thex.Dto.Room;
using Thex.Dto.Rooms;
using Thex.Dto.SearchReservationReportDto;
using Tnf.Dto;

namespace Thex.Infra.ReadInterfaces
{
    public interface IReservationReportReadRepository
    {
        IListDto<SearchReservationReportResultDto> GetAllByFilters(SearchReservationReportDto request, int propertyId, List<ReservationStatus> status, DateTime systemDate);

        IListDto<SearchReservationReportResultDto> GetAllCheckByFilters(SearchReservationReportDto request, int propertyId, List<ReservationStatus> listStatus, DateTime systemDate);

        ListDto<SearchMeanPlanControlReportResultDto> GetAllMeanPlanByFilters(SearchReservationReportDto request, int propertyId, DateTime systemDate, int checkInHour);

        ListDto<SearchGuestSaleReportResultDto> GetAllGuestSaleByFilters(int propertyId, bool guestSale,
            bool sparseSale, bool masterGroupSale, bool companySale, bool eventSale, bool pendingSale, bool positiveSale, bool canceledSale,
            bool noShowSale, double? sale, bool aheadSale);

        ListDto<SearchIssuedNotesReportResultDto> GetAllIssuedNotes(int propertyId, SearchIssuedNotesReportDto requestDto);

        ListDto<SearchReservationUhReportResultReturnDto> GetRoomsByPropertyId(GetAllSituationRoomsDto request, int propertyId);

        Task<ReportUhSituationReturnDto> GetUhSituation(int propertyId);

        Task<ReportHousekeepingStatusReturnDto> GetHousekeepingStatus(int propertyId);

        Task<ReportReservationStatusReturnDto> GetReservationStatus(int propertyId, DateTime? systemDate);

        Task<ReportCheckStatusReturnDto> GetCheckStatus(int propertyId, DateTime? systemDate);

        ListDto<SearchBordereauReportResultDto> GetBordereau(SearchBordereauReportDto requestDto, int propertyId);

        ListDto<ReportReservationsMadeReturnDto> GetReservationsMade(SearchReservationsMadeReportDto requestDto, int propertyId);

        IListDto<ReportHousekeepingStatusDisagreementReturnDto> GetAllHousekeepingStatusDisagreement(SearchReportHousekeepingStatusDisagreementDto request, int propertyId, DateTime? systemDate);

        ListDto<ReservationsInAdvanceReturnDto> GetAllReservationsInAdvance(SearchReservationsInAdvanceDto request, int propertyId);
    }
}
