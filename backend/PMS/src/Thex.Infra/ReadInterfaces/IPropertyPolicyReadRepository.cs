﻿using System;
using System.Collections.Generic;
using System.Text;
using Thex.Dto;
using Tnf.Dto;

namespace Thex.Infra.ReadInterfaces
{
  public  interface IPropertyPolicyReadRepository
    {
        IListDto<PropertyPolicyDto> GetAllPropertyPolicyByPropertyId(int propertyId);
        PropertyPolicyDto GetPropertyPolicy(Guid propertyPolicyId);
    }
}
