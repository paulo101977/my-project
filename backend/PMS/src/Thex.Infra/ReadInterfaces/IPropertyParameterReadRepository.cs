﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Thex.Dto;
using Thex.Dto.PropertyAuditProcessStep;
using Thex.Dto.PropertyParameter;
using Tnf.Dto;
using Tnf.Repositories;

namespace Thex.Infra.ReadInterfaces
{
    public interface IPropertyParameterReadRepository
    {
        PropertyParameterDto GetPropertyParameterForPropertyIdAndApplicationParameterId(int propertyId, int applicationParameterId);
        PropertyParameterDividedByGroupDto GetPropertyParameterForPropertyId(int propertyId);
        Task<List<PropertyParameterDto>> GetChildrenAgeGroupList(int propertyId);
        PropertyParameterDto GetPropertyParameterMaxChildrenAge(int propertyId);
        ListDto<PropertyParameterDto> GetChildrenAgeGroupForRoomTypeList(int propertyId);
        PropertyParameterDto GetPropertyParameterById(Guid propertyParameterId);
        List<PropertyParameterDto> GetPropertyParametersByFeatureGroupId(int propertyId, int FeatureGroupId);
        DateTime? GetSystemDate(int propertyId);
        Task<DateTime?> GetSystemDateAsync(int propertyId);
        int? GetDifferenceDailyBillingItem(int propertyId);
        int? GetDailyBillingItem(int propertyId);
        PropertyAuditProcessStepNumber GetAuditStepByPropertyId(int propertyId);
        string GetTimeZoneByPropertyId(int propertyId);
        int? GetNumberOfDaysToChangeRoomStatusByPropertyId(int propertyId);
        PropertyParameterDto GetPropertyParameterByApplicationParameterId(int applicationParameterId);
        Task<PropertyDto> GetDtoByIdAsync(int propertyId);
    }
}
