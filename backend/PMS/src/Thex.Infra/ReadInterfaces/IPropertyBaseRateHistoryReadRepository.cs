﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Thex.Infra.ReadInterfaces
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Thex.Domain.Entities;
    using Thex.Dto;
    using Tnf.Repositories;
    public interface IPropertyBaseRateHistoryReadRepository
    {
        Task<List<PropertyBaseRateHistory>> GetAll(Guid? pRatePlanId);

        IQueryable<PropertyBaseRateHistoryDto> GetAllByRatePlanId(Guid? pRatePlanId);
        Task<List<PropertyBaseRateHistory>> GetAllByHistoryId(Guid? pHistoryId);

        IQueryable<PropertyBaseRateHistoryDto> GetAllDtoByHistoryId(List<Guid> pLstHistoryId);




    }
}
