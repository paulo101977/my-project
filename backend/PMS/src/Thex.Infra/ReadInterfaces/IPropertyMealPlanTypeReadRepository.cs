﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Thex.Dto;
using Tnf.Dto;

namespace Thex.Infra.ReadInterfaces
{
    public interface IPropertyMealPlanTypeReadRepository
    {
        ListDto<PropertyMealPlanTypeDto> GetAllPropertyMealPlanTypeByProperty(int propertyId);
        Task<IListDto<PropertyMealPlanTypeDto>> GetAllPropertyMealPlanTypeByPropertyAsync(int propertyId);
        ListDto<PropertyMealPlanTypeDto> GetAllPropertyMealPlanTypeByPropertyWithApplicationMealPlanType(int propertyId, bool all = false);
        Task<ListDto<PropertyMealPlanTypeDto>> GetAllPropertyMealPlanTypeAsync(int propertyId, bool all = false);
        MealPlanTypeDto GetPropertyMealPlanTypeByMealTypeId(int mealTypeId, int propertyId);
        bool ExistsPropertyMealPlanTypeActive(Guid id);
        ICollection<int> GetAllMealPlanTypeIdListByPropertyId(int propertyId);
    }
}
