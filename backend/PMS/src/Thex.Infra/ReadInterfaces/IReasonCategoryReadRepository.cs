﻿using System;
using System.Collections.Generic;
using System.Text;
using Thex.Dto;
using Tnf.Dto;

namespace Thex.Infra.ReadInterfaces
{
    public interface IReasonCategoryReadRepository
    {
        IListDto<ReasonCategoryGetAllDto> GetAllReasonCategory();
    }
}
