﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Thex.Domain.Entities;
using Thex.Dto.BillingItem;
using Thex.Dto;
using Thex.Domain.JoinMap;

namespace Thex.Infra.ReadInterfaces
{
    public interface ITenantReadRepository
    {
        Expression<Func<T, bool>> FilterByTenant<T>(List<Guid> tenantList);
        Tenant GetCurrentTenant();
        TenantPropertyParameterJoinMap GetTenantById(Guid id);
        List<Guid> GetIdListByLoggedTenantId();
        List<Guid> GetIdListByLoggedUserId();
    }
}
