﻿
using System.Collections.Generic;
using Thex.Dto;

namespace Thex.Infra.ReadInterfaces
{
    public interface IPropertyGuestTypeReadRepository
    {
        bool Any(int id, int propertyId);
        List<PropertyGuestTypeDto> GetAllByPropertyId(int propertyId, bool activeOnly = false);
    }
}
