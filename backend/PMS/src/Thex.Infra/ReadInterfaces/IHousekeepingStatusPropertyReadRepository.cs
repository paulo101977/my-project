﻿using System;
using System.Collections.Generic;
using System.Text;
using Thex.Domain.Entities;
using Thex.Dto;
using Thex.Dto.Housekeeping;
using Tnf.Dto;

namespace Thex.Infra.ReadInterfaces
{
    public interface IHousekeepingStatusPropertyReadRepository
    {
        IListDto<HousekeepingStatusPropertyDto> GetAll(GetAllHousekeepingStatusPropertyDto request, int propertyId);
        HousekeepingStatusPropertyDto Get(Guid guid);

        IListDto<GetAllHousekeepingAlterStatusDto> GetAllHousekeepingAlterStatusRoom(GetAllHousekeepingAlterStatusRoomDto request, int propertyId, DateTime systemDate);
        Guid GetHousekeepingStatusIdDirty();
        Guid GetHousekeepingStatusIdMaintenance();
        ListDto<HousekeepingStatusPropertyDto> GetAllHousekeepingStatusCreateBySystem(int propertyId);
       
    }
}
