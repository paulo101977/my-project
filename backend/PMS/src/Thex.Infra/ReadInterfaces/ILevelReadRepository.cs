﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Thex.Domain.Entities;
using Thex.Dto;
using Tnf.Dto;

namespace Thex.Infra.ReadInterfaces
{
    public interface ILevelReadRepository
    {
        LevelDto GetDtoById(int propertyId, Guid id);
        LevelDto GetDtoByLevelCode(int propertyId, int levelCode);
        IListDto<LevelDto> GetAllDto(int propertyId, GetAllLevelDto requestDto);
        List<Level> GetAll(int propertyId);
        Level GetById(int propertyId, Guid id);
        bool LevelAlreadyUsed(Guid levelId);
    }
}
