﻿using System.Threading.Tasks;
using Thex.Dto;

namespace Thex.Infra.ReadInterfaces
{
    public interface IPropertyContractReadRepository
    {
        PropertyContractDto GetPropertyContractByPropertyId(int propertyId);
        Task<int?> GetTotalRoomsByPropertyIdAsync(int propertyId);
    }
}
