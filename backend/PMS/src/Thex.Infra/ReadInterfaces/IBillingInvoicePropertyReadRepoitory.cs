﻿using System;
using Thex.Dto;
using Tnf.Dto;

namespace Thex.Infra.ReadInterfaces
{
    public interface IBillingInvoicePropertyReadRepository
    {
        IListDto<BillingInvoicePropertyDto> GetAllByPropertyId(int propertyId, GetAllBillingInvoicePropertyDto request);
        BillingInvoicePropertyDto GetById(Guid id);
        IListDto<BillingInvoicePropertyDto> GetAllWithoutReferenceByPropertyId(int propertyId);
        BillingInvoicePropertyDto GetChildByParentId(Guid parentId);
    }
}
