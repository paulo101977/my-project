﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.Domain.Entities;
using Thex.Dto;

namespace Thex.Infra.ReadInterfaces
{
    public interface IPropertyBaseRateReadRepository
    {
        Task<List<PropertyBaseRate>> GetAll(int mealPlanTypeId, Guid currencyId, List<DateTime> dateList, List<int> roomTypeIdList, Guid? ratePlanId = null);
        Task<List<PropertyBaseRate>> GetAllByPeriod(int propertyId, List<int> roomTypeId, Guid? currencyId, int? mealPlanTypeId, DateTime initialDate, DateTime finalDate, bool? include = null, bool? fixedRates = null, Guid? ratePlanId = null, bool? level = null);
        ICollection<PropertyBaseRate> GetAllWithPropertyMealPlanTypeRate(PropertyMealPlanTypeRateHeaderDto request, int propertyId);

    }
}
