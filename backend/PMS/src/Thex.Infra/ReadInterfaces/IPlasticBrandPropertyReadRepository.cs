﻿// //  <copyright file="IPlasticBrandPropertyReadRepository.cs" company="TOTVS">
// //  Copyright (c) TOTVS. All rights reserved.
// //  </copyright>
namespace Thex.Infra.ReadInterfaces
{
    using Tnf.Repositories;
    using Tnf.Dto;
    using Thex.Dto;
    using System;
    using Thex.Dto.PlasticBrandProperty;

    public interface IPlasticBrandPropertyReadRepository : IRepository
    {
        IListDto<PlasticBrandPropertyResultDto> GetAllByPropertyId(GetAllPlasticBrandPropertyDto request);

        PlasticBrandPropertyResultDto GetPlasticBrandDetails(Guid? plasticBrandPropertyId);
    }
}