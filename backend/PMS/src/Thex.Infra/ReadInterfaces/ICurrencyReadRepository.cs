﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Thex.Domain.Entities;
using Thex.Dto;
using Tnf.Dto;

namespace Thex.Infra.ReadInterfaces
{
    public interface ICurrencyReadRepository
    {
        ListDto<CurrencyDto> GetAllCurrency();
        Task<IListDto<CurrencyDto>> GetAllCurrencyAsync(GetAllCurrencyDto request = null);
        bool IsExistCurrencyId(Guid currencyId);
        CurrencyDto GetNameCurrencyById(Guid currencyId);
        CurrencyDto GetPropertyCurrencyById(int propertyId);
        bool IsToDolarQuotation(Guid currencyId);
    }
}
