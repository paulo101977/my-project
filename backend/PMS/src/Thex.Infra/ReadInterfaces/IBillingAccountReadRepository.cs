using Thex.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using Tnf.Dto;
using Thex.Dto.BillingAccount;
using System.Linq;
using Thex.Dto;
using Thex.Common.Enumerations;

namespace Thex.Infra.ReadInterfaces
{
    public interface IBillingAccountReadRepository
    {
        List<BillingAccount> GetAllForGuestReservationItemId(List<long> guestReservationItemId,long reservationId);
        decimal CountBalanceGuest(long? reservationItemId);
        int CountComments(string externalComments, string internalComments, string partnerComments);
        BillingAccountWithAccountsDto GetById(DefaultGuidRequestDto id);
        List<Guid> GetBillingAccountIdsByGroupId(Guid groupId);
        List<BillingAccount> GetAllByBillingAccountGroupKey(List<Guid> groupKeys);
        bool IsMainAccount(Guid id);
        bool IsClosedOrBlockedAccount(Guid id);
        bool AnyClosedOrBlockedAccount(List<Guid> ids);
        BillingAccountTotalForClosureDto GetBillingAccountsForClosure(Guid id, bool allSimiliarAccounts);
        List<BillingAccountWithAccountsDto> GetAllByIds(List<Guid> ids);
        List<BillingAccount> GetAllByBillingAccountByIds(List<Guid> ids);
        List<BillingAccountWithAccountsDto> GetBillingAccountsIdsForEntries(Guid id);
        BillingAccount GetBillingAccountByOwnerIdAndReservationId(Guid ownerDestination, bool isCompany, long reservationId, long? reservationItemId);
        ListDto<SearchBillingAccountResultDto> GetAllBillingAccountsByUhOrSparseAccount(Guid id, int propertyId);
        bool AnyGroupAccount(long reservationId);
        bool AnyCompanyAccountAccomodation(long reservationItemId);
        decimal SumOfAccount(Guid billingAccountGroupKey);
        List<Guid> GetAllBillingAccountsByUhOrSparseAccountIds(Guid id, int propertyId);
        bool IsOpen(Guid id);
        BillingAccount GetBillingAccountById(Guid billingAccountId);
        bool CheckIfExistIsMainBillingAccount(long reservationId, long reservationItemId, long? guestReservationItem, Guid? companyClient, int billingAccountTypeId);
        bool AnyGroupAccount(List<Guid> ids);
        bool AllSimiliarBillingAccountsAreClosed(Guid billingAccountId);
        BillingAccountDto GetByInvoiceId(Guid invoiceId);
        bool AnyOpenAccountByReservationItemId(long reservationItemId);
        List<BillingAccount> GetAllByReservationItemId(long id);
        bool CheckAllBillingAccountsAreClosedByReservationItemId(Guid billingAccountId);
        Guid GetIdByPersonHeaderId(Guid personId);
        (bool, bool) AnyOpenAccountAndHaveBillingAccountItemByReservationItemId(long reservationItemId);
        List<BillingAccount> GetAllOpenedByReservationItemId(long reservationItemId);
        bool AnyBillingAccountItemByPropertyId(int propertyId);

        IListDto<SearchBillingAccountResultDto> GetAllByFiltersV2(SearchBillingAccountDto request, int propertyId);
        BillingAccountHeaderDto BillingAccountHeaderV2(Guid? billingAccountId);
        IListDto<BillingAccountSidebarDto> BillingAccountSidebarV2(Guid billingAccountId);

        List<BillingAccountDto> GetByIds(List<Guid> ids);
        List<BillingAccountGuestReservationItemDto> GetAllAccountGuestReservationItemDtoByFilters(TourismTaxLaunchSearchDto dto);
        BillingAccount GetById(Guid id);
        List<BillingAccount> GetAllExistingBillingAccountsByGuestReservationItemIds(List<long?> guestReservationItemId);
        bool CheckIfExistIsMainBillingAccountByGuestReservationItemIds(List<long?> guestReservationItemId);
        List<BillingAccount> GetAllByFilters(List<long> reservationIdList, BillingAccountTypeEnum billingAccountTypeId, AccountBillingStatusEnum statusId);
    }
}
