﻿using Microsoft.Extensions.DependencyInjection;
using System.Collections.Generic;
using System.Linq;
using Thex.Domain.Interfaces.Integration;
using Thex.Domain.Interfaces.Repositories;
using Thex.Domain.Interfaces.Repositories.ReadDapper;
using Thex.EntityFrameworkCore.Repositories;
using Thex.Infra.ReadInterfaces;
using Thex.Infra.Repositories;
using Thex.Infra.Repositories.Integration;
using Thex.Infra.Repositories.Read;
using Thex.Infra.Repositories.ReadDapper;
//using Thex.Infra.Repositories;

namespace Thex.Infra
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddInfraDependency(this IServiceCollection services)
        {
            services
                .AddTnfEntityFrameworkCore()    // Configura o uso do EntityFrameworkCore registrando os contextos que serão usados pela aplicação
                .AddThexEntityFrameworkCore()
                .AddMapperDependency();         // Configura o uso do AutoMappper

            services.AddTnfDefaultConventionalRegistrations();

            // Registro dos repositórios
            services.AddTransient<IInvoiceIntegrationBrazil, InvoiceIntegrationBrazil>();
            services.AddTransient<IInvoiceIntegrationEurope, InvoiceIntegrationEurope>();
            services.AddTransient<IInvoiceIntegrationClient, InvoiceIntegrationClient>();
            services.AddTransient<IInvoiceIntegration, InvoiceIntegration>();

            services.AddTransient<IBillingAccountItemRepository, BillingAccountItemRepository>();
            services.AddTransient<IBillingAccountItemTransferRepository, BillingAccountItemTransferRepository>();
            services.AddTransient<IBillingAccountRepository, BillingAccountRepository>();
            services.AddTransient<IBillingAccountItemInvoiceHistoryRepository, BillingAccountItemInvoiceHistoryRepository>();
            services.AddTransient<IBillingInvoicePropertyRepository, BillingInvoicePropertyRepository>();
            services.AddTransient<IBillingInvoicePropertySupportedTypeRepository, BillingInvoicePropertySupportedTypeRepository>();
            services.AddTransient<IBillingInvoiceRepository, BillingInvoiceRepository>();
            services.AddTransient<IBillingItemCategoryRepository, BillingItemCategoryRepository>();
            services.AddTransient<IBillingItemRepository, BillingItemRepository>();
            services.AddTransient<IBillingTaxRepository, BillingTaxRepository>();
            services.AddTransient<IBusinessSourceRepository, BusinessSourceRepository>();
            services.AddTransient<IChainBusinessSourceRepository, ChainBusinessSourceRepository>();
            services.AddTransient<IChannelRepository, ChannelRepository>();
            services.AddTransient<ILevelRepository, LevelRepository>();
            services.AddTransient<ICompanyClientContactPersonRepository, CompanyClientContactPersonRepository>();
            services.AddTransient<ICompanyClientRepository, CompanyClientRepository>();
            services.AddTransient<ICompanyClientChannelRepository, CompanyClientChannelRepository>();
            services.AddTransient<ICompanyRepository, CompanyRepository>();
            services.AddTransient<IContactInformationRepository, ContactInformationRepository>();
            services.AddTransient<IDocumentRepository, DocumentRepository>();
            services.AddTransient<IGuestRegistrationDocumentRepository, GuestRegistrationDocumentRepository>();
            services.AddTransient<IGuestReservationItemRepository, GuestReservationItemRepository>();
            services.AddTransient<IHousekeepingStatusPropertyRepository, HousekeepingStatusPropertyRepository>();
            services.AddTransient<ILevelRateRepository, LevelRateRepository>();
            services.AddTransient<ILevelRateHeaderRepository, LevelRateHeaderRepository>();
            services.AddTransient<ILocationRepository, LocationRepository>();
            services.AddTransient<IMarketSegmentRepository, MarketSegmentRepository>();
            services.AddTransient<IPersonInformationRepository, PersonInformationRepository>();
            services.AddTransient<IPersonRepository, PersonRepository>();
            services.AddTransient<IPropertyBaseRateRepository, PropertyBaseRateRepository>();
            services.AddTransient<IPropertyMealPlanTypeRepository, PropertyMealPlanTypeRepository>();
            services.AddTransient<IPropertyMealPlanTypeRateRepository, PropertyMealPlanTypeRateRepository>();
            services.AddTransient<IPropertyParameterRepository, PropertyParameterRepository>();
            services.AddTransient<IPropertyPremiseHeaderRepository, PropertyPremiseHeaderRepository>();
            services.AddTransient<IPropertyPolicyRepository, PropertyPolicyRepository>();
            services.AddTransient<IPropertyRateStrategyRepository, PropertyRateStrategyRepository>();
            services.AddTransient<IRatePlanCommissionRepository, RatePlanCommissionRepository>();
            services.AddTransient<IRatePlanCompanyClientRepository, RatePlanCompanyClientRepository>();
            services.AddTransient<IRatePlanRepository, RatePlanRepository>();
            services.AddTransient<IRatePlanRoomTypeRepository, RatePlanRoomTypeRepository>();
            services.AddTransient<IReasonRepository, ReasonRepository>();
            services.AddTransient<IReservationBudgetRepository, ReservationBudgetRepository>();
            services.AddTransient<IReservationRepository, ReservationRepository>();
            services.AddTransient<IReservationItemLaunchRepository, ReservationItemLaunchRepository>();
            services.AddTransient<IRoomBlockingRepository, RoomBlockingRepository>();
            services.AddTransient<IRoomRepository, RoomRepository>();
            services.AddTransient<IRoomTypeRepository, RoomTypeRepository>();
            services.AddTransient<IRoomTypeInventoryRepository, RoomTypeInventoryRepository>();
            services.AddScoped<ITourismTaxRepository, TourismTaxRepository>();
            services.AddTransient<IPropertyGuestPolicyRepository, PropertyGuestPolicyRepository>();
            services.AddTransient<IPropertyCurrencyExchangeRepository, PropertyCurrencyExchangeRepository>();
            services.AddTransient<ISibaIntegrationRepository, SibaIntegrationRepository>();

            // Read
            services.AddTransient<IAgreementTypeReadRepository, AgreementTypeReadRepository>();
            services.AddTransient<IBillingAccountItemReadRepository, BillingAccountItemReadRepository>();
            services.AddTransient<IBillingAccountReadRepository, BillingAccountReadRepository>();
            services.AddTransient<IBillingInvoicePropertyReadRepository, BillingInvoicePropertyReadRepository>();
            services.AddTransient<IBillingInvoicePropertySupportedTypeReadRepository, BillingInvoicePropertySupportedTypeReadRepository>();
            services.AddTransient<IBillingInvoiceReadRepository, BillingInvoiceReadRepository>();
            services.AddTransient<IBillingItemCategoryReadRepository, BillingItemCategoryReadRepository>();
            services.AddTransient<IBillingItemPDVReadRepository, BillingItemPDVReadRepository>();
            services.AddTransient<IBillingItemReadRepository, BillingItemReadRepository>();
            services.AddTransient<IBrandReadRepository, BrandReadRepository>();
            services.AddTransient<IBusinessSourceReadRepository, BusinessSourceReadRepository>();
            services.AddTransient<IChainReadRepository, ChainReadRepository>();
            services.AddTransient<IChannelReadRepository, ChannelReadRepository>();
            services.AddTransient<ILevelReadRepository, LevelReadRepository>();
            services.AddTransient<IChannelCodeReadRepository, ChannelCodeReadRepository>();
            services.AddTransient<ICompanyClientContactPersonReadRepository, CompanyClientContactPersonReadRepository>();
            services.AddTransient<ICompanyClientReadRepository, CompanyClientReadRepository>();
            services.AddTransient<ICompanyClientChannelReadRepository, CompanyClientChannelReadRepository>();
            services.AddTransient<ICompanyReadRepository, CompanyReadRepository>();
            services.AddTransient<IContactInformationReadRepository, ContactInformationReadRepository>();
            services.AddTransient<ICountryPersonDocumentTypeReadRepository, CountryPersonDocumentTypeReadRepository>();
            services.AddTransient<ICountrySubdivisionTranslationReadRepository, CountrySubdivisionTranslationReadRepository>();
            services.AddTransient<ICountrySubdvisionServiceReadRepository, CountrySubdvisionServiceReadRepository>();
            services.AddTransient<ICurrencyReadRepository, CurrencyReadRepository>();
            services.AddTransient<ICustomerStationReadRepository, CustomerStationReadRepository>();
            services.AddTransient<IDocumentReadRepository, DocumentReadRepository>();
            services.AddTransient<IGuestRegistrationDocumentReadRepository, GuestRegistrationDocumentReadRepository>();
            services.AddTransient<IGuestRegistrationReadRepository, GuestRegistrationReadRepository>();
            services.AddTransient<IGuestReservationItemReadRepository, GuestReservationItemReadRepository>();
            services.AddTransient<IHousekeepingStatusPropertyReadRepository, HousekeepingStatusPropertyReadRepository>();
            services.AddTransient<ILevelRateReadRepository, LevelRateReadRepository>();
            services.AddTransient<ILevelRateHeaderReadRepository, LevelRateHeaderReadRepository>();
            services.AddTransient<ILocationReadRepository, LocationReadRepository>();
            services.AddTransient<IMarketSegmentReadRepository, MarketSegmentReadRepository>();
            services.AddTransient<IOccupationReadRepository, OccupationReadRepository>();
            services.AddTransient<IPersonReadRepository, PersonReadRepository>();
            services.AddTransient<IPlasticBrandPropertyReadRepository, PlasticBrandPropertyReadRepository>();
            services.AddTransient<IPolicyTypesReadRepository, PolicyTypesReadRepository>();
            services.AddTransient<IPropertyAuditProcessReadRepository, PropertyAuditProcessReadRepository>();
            services.AddTransient<IPropertyAuditProcessStepReadRepository, PropertyAuditProcessStepReadRepository>();
            services.AddTransient<IPropertyBaseRateReadRepository, PropertyBaseRateReadRepository>();
            services.AddTransient<IPropertyCompanyClientCategoryReadRepository, PropertyCompanyClientCategoryReadRepository>();
            services.AddTransient<IPropertyCompanyClientCategoryRepository, PropertyCompanyClientCategoryRepository>();
            services.AddTransient<IPropertyCurrencyExchangeReadRepository, PropertyCurrencyExchangeReadRepository>();
            services.AddTransient<IPropertyMealPlanTypeReadRepository, PropertyMealPlanTypeReadRepository>();
            services.AddTransient<IPropertyMealPlanTypeRateReadRepository, PropertyMealPlanTypeRateReadRepository>();
            services.AddTransient<IPropertyParameterReadRepository, PropertyParameterReadRepository>();
            services.AddTransient<IPropertyPolicyReadRepository, PropertyPolicyReadRepository>();
            services.AddTransient<IPropertyReadRepository, PropertyReadRepository>();
            services.AddTransient<IPropertyPremiseHeaderReadRepository, PropertyPremiseHeaderReadRepository>();
            services.AddTransient<IRatePlanReadRepository, RatePlanReadRepository>();
            services.AddTransient<IRatePlanRoomTypeReadRepository, RatePlanRoomTypeReadRepository>();
            services.AddTransient<IReasonCategoryReadRepository, ReasonCategoryReadRepository>();
            services.AddTransient<IReasonReadRepository, ReasonReadRepository>();
            services.AddTransient<IReservationBudgetReadRepository, ReservationBudgetReadRepository>();
            services.AddTransient<IReservationConfirmationReadRepository, ReservationConfirmationReadRepository>();
            services.AddTransient<IReservationItemReadRepository, ReservationItemReadRepository>();
            services.AddTransient<IReservationReadRepository, ReservationReadRepository>();
            services.AddTransient<IReservationItemLaunchReadRepository, ReservationItemLaunchReadRepository>();
            services.AddTransient<IRoomStatusReadRepository, RoomStatusReadRepository>();
            services.AddTransient<IRoomBlockingReadRepository, RoomBlockingReadRepository>();
            services.AddTransient<EntityFrameworkCore.ReadInterfaces.Repositories.IRoomReadRepository, RoomReadRepository>();
            services.AddTransient<IRoomTypeReadRepository, RoomTypeReadRepository>();
            services.AddTransient<ITenantReadRepository, TenantReadRepository>();
            services.AddTransient<IPropertyGuestTypeReadRepository, PropertyGuestTypeReadRepository>();
            services.AddTransient<IRateTypeReadRepository, RateTypeReadRepository>();
            services.AddTransient<IPropertyBaseRateHeaderHistoryReadRepository, PropertyBaseRateHeaderHistoryReadRepository>();
            services.AddTransient<IPropertyBaseRateHistoryReadRepository, PropertyBaseRateHistoryReadRepository>();
            services.AddTransient<IPropertyRateStrategyReadRepository, PropertyRateStrategyReadRepository>();
            services.AddTransient<IPropertyContractReadRepository, PropertyContractReadRepository>();
            services.AddTransient<IBillingInvoiceModelReadRepository, BillingInvoiceModelReadRepository>();
            services.AddScoped<ITourismTaxReadRepository, TourismTaxReadRepository>();
            services.AddScoped<IRoomOccupiedReadRepository, RoomOccupiedReadRepository>();
            services.AddScoped<ISibaIntegrationReadRepository, SibaIntegrationReadRepository>();
            services.AddScoped<Domain.Interfaces.Repositories.Read.IBillingAccountReadRepository, BillingAccountReadRepository>();
            services.AddScoped<Domain.Interfaces.Repositories.Read.IReservationBudgetReadRepository, ReservationBudgetReadRepository>();
            services.AddScoped<Domain.Interfaces.Repositories.Read.IReservationItemReadRepository, ReservationItemReadRepository>();
            services.AddScoped<Domain.Interfaces.Repositories.Read.IBillingAccountItemReadRepository, BillingAccountItemReadRepository>();
            services.AddTransient<IPropertyCurrencyExchangeRepository, PropertyCurrencyExchangeRepository>();
            services.AddTransient<IPowerBiReadRepository, PowerBiReadRepository>();
            services.AddTransient<IPropertyGuestPolicyReadRepository, PropertyGuestPolicyReadRepository>();
            services.AddScoped<IGuestReadRepository, GuestReadRepository>();


            services.AddTransient<IGuestReservationItemDapperReadRepository, GuestReservationItemDapperReadRepository>();
            services.AddTransient<IReservationDapperReadRepository, ReservationDapperReadRepository>();
            services.AddTransient<IReservationItemDapperReadRepository, ReservationItemDapperReadRepository>();
            services.AddTransient<IExternalRegistrationRepository, ExternalRegistrationRepository>();

            return services;
        }
    }
}
