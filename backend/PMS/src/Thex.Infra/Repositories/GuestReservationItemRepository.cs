﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Thex.Common;
using Thex.Common.Enumerations;
using Thex.Domain.Entities;
using Thex.Domain.Interfaces.Repositories;
using Thex.Infra.ReadInterfaces;
using Tnf.EntityFrameworkCore.Repositories;
using Tnf.EntityFrameworkCore;
using Thex.Infra.Context;
using Tnf.Notifications;
using EFCore.BulkExtensions;
using Thex.Kernel;

namespace Thex.EntityFrameworkCore.Repositories
{
    public class GuestReservationItemRepository : EfCoreRepositoryBase<ThexContext, GuestReservationItem>, IGuestReservationItemRepository
    {
        private readonly INotificationHandler Notification;
        private readonly IApplicationUser _applicationUser;

        public GuestReservationItemRepository(
            IDbContextProvider<ThexContext> dbContextProvider, 
            INotificationHandler notificationHandler,
            IApplicationUser applicationUser) : base(dbContextProvider)
        {
            Notification = notificationHandler;
            _applicationUser = applicationUser;
        }

        public void UpdateAllGuestsToCheckout(long reservationItemId, DateTime pCheckOutDate)
        {
            var guestReservationItemList = Context
                .GuestReservationItems
                
                .Where(e => e.ReservationItemId == reservationItemId && e.GuestStatusId != (int)ReservationStatus.Checkout)
                .ToList();

            foreach (var guestReservationItem in guestReservationItemList)
                guestReservationItem.SetToCheckout(pCheckOutDate);

            Context.SaveChanges();
        }


        public void UpdateAllGuestsToPending(long reservationItemId)
        {
            var guestReservationItemList = Context
                .GuestReservationItems

                .Where(e => e.ReservationItemId == reservationItemId && e.GuestStatusId != (int)ReservationStatus.Checkout)
                .ToList();

            foreach (var guestReservationItem in guestReservationItemList)
                guestReservationItem.SetToPending();

            Context.SaveChanges();
        }

        public void UpdateAllGuestsToNoShow(long reservationItemId)
        {
            var guestReservationItemList = Context
                .GuestReservationItems

                .Where(e => e.ReservationItemId == reservationItemId)
                .ToList();

            foreach (var guestReservationItem in guestReservationItemList)
                guestReservationItem.SetToNoShow();

            Context.SaveChanges();
        }

        /*
        ToConfirm = 0,
        Confirmed = 1,
        Checkin = 2,
        Checkout = 3,
        Pending = 4,
        NoShow = 5,
        Canceled = 6,
        WaitList = 7,
        ReservationProposal = 8
        */

        public void UpdateAllGuestsToCanceled(long reservationItemId)
        {
            var guestReservationItemList = Context
                .GuestReservationItems

                .Where(e => e.ReservationItemId == reservationItemId && e.GuestStatusId != (int)ReservationStatus.Canceled)
                .ToList();

            foreach (var guestReservationItem in guestReservationItemList)
                guestReservationItem.SetToCanceled();

            Context.SaveChanges();
        }

        public void UpdateGuestToCanceled(long guestReservationItemId)
        {
            var guestReservationItem = Context
                .GuestReservationItems

                .FirstOrDefault(e => e.ReservationItemId == guestReservationItemId && e.GuestStatusId != (int)ReservationStatus.Canceled);

            if (guestReservationItem == null)
            {
                Notification.Raise(Notification
                 .DefaultBuilder
                 .WithMessage(AppConsts.LocalizationSourceName, CommonsEnum.Error.EntityNotExist)
                 .WithMessageFormat("GuestReservationItem")
                 .Build());
                return;

            }

            guestReservationItem.SetToCanceled();

            Context.SaveChanges();
        }
        public void UpdateGuestToCheckout(long guestReservationItemId, DateTime pCheckOutDate)
        {
            var guestReservationItem = Context
                .GuestReservationItems
                
                .FirstOrDefault(e => e.Id == guestReservationItemId && e.GuestStatusId != (int)ReservationStatus.Checkout);

            if (guestReservationItem == null)
            {
                Notification.Raise(Notification
                 .DefaultBuilder
                 .WithMessage(AppConsts.LocalizationSourceName, CommonsEnum.Error.EntityNotExist)
                 .WithMessageFormat("GuestReservationItem")
                 .Build());
                return;

            }

            guestReservationItem.SetToCheckout(pCheckOutDate, true);

            Context.SaveChanges();
        }

        public void UpdateAllGuestToConfirm(long reservationItemId)
        {
            var guestReservationItemList = Context
                .GuestReservationItems
                .Where(e => e.ReservationItemId == reservationItemId && e.GuestStatusId != (int)ReservationStatus.ToConfirm)
                .ToList();

            foreach (var guestReservationItem in guestReservationItemList)
                guestReservationItem.SetToConfirm();

            Context.SaveChanges();
        }

        public void UpdateAllGuestToConfirmed(long reservationItemId)
        {
            var guestReservationItemList = Context
                .GuestReservationItems
                .Where(e => e.ReservationItemId == reservationItemId && e.GuestStatusId != (int)ReservationStatus.Confirmed)
                .ToList();

            foreach (var guestReservationItem in guestReservationItemList)
                guestReservationItem.SetToConfirmed();

            Context.SaveChanges();
        }

        public GuestReservationItem GetById(long id)
        {
            return Context.GuestReservationItems
                .Include(g => g.GuestRegistration)
                .FirstOrDefault(g => g.Id == id);
        }

        public void UpdateGuestEstimatedDepartureDate(long guestReservationItemId, DateTime estimatedDepartureDate)
        {
            var guestReservationItem = Context
                .GuestReservationItems

                .FirstOrDefault(e => e.Id == guestReservationItemId);

            if (guestReservationItem == null)
            {
                Notification.Raise(Notification
                 .DefaultBuilder
                 .WithMessage(AppConsts.LocalizationSourceName, CommonsEnum.Error.EntityNotExist)
                 .WithMessageFormat("GuestReservationItem")
                 .Build());
                return;

            }

            guestReservationItem.EstimatedDepartureDate = estimatedDepartureDate;
            Context.SaveChanges();
        }

        public void ChangeGuestReservationItemStatus(long id, ReservationStatus reservationStatus, DateTime? systemDate = null)
        {
            var guestReservationItem = Context.GuestReservationItems.FirstOrDefault(x => x.Id == id);
            if (guestReservationItem != null)
            {
                guestReservationItem.GuestStatusId = (int)reservationStatus;

                if (ReservationStatus.Checkout == reservationStatus && !guestReservationItem.CheckOutDate.HasValue && systemDate.HasValue)
                    guestReservationItem.SetToCheckout(systemDate.Value, true);

                Context.SaveChanges();
            }
        }

        public void UpdateGuestEstimatedArrivalAndDepartureDate(long guestReservationItemId, DateTime estimatedArrivalDate, DateTime estimatedDepartureDate)
        {
            var guestReservationItem = Context
               .GuestReservationItems
               .FirstOrDefault(e => e.Id == guestReservationItemId);

            if (guestReservationItem == null) return;            

            guestReservationItem.EstimatedArrivalDate = estimatedArrivalDate;
            guestReservationItem.EstimatedDepartureDate = estimatedDepartureDate;
            Context.SaveChanges();
        }

        public void UpdateGuestEstimatedDepartureDate(List<GuestReservationItem> guestReservationItemList, DateTime systemDate)
        {
            foreach (var guestReservationItem in guestReservationItemList)
            {
                guestReservationItem.EstimatedDepartureDate = systemDate
                                                                .AddDays(1)
                                                                .AddHours(guestReservationItem.EstimatedDepartureDate.Hour)
                                                                .AddMinutes(guestReservationItem.EstimatedDepartureDate.Minute);

                guestReservationItem.LastModificationTime = DateTime.UtcNow.ToZonedDateTimeLoggedUser(_applicationUser);
                guestReservationItem.LastModifierUserId = _applicationUser.UserUid;
            }

            Context.BulkUpdate(guestReservationItemList);
        }


        public void UpdateAllGuestsToNoShow(List<long> reservationItemIdList)
        {
            var guestReservationItemList = Context
                .GuestReservationItems
                .Where(e => reservationItemIdList.Contains(e.ReservationItemId))
                .ToList();

            foreach (var guestReservationItem in guestReservationItemList)
            {
                guestReservationItem.SetToNoShow();
                guestReservationItem.LastModificationTime = DateTime.UtcNow.ToZonedDateTimeLoggedUser(_applicationUser);
                guestReservationItem.LastModifierUserId = _applicationUser.UserUid;
            }

            Context.BulkUpdate(guestReservationItemList);
        }

        public GuestReservationItem GetByGuestRegistrationId(Guid guestRegistrationId)
        {
            return Context.GuestReservationItems.FirstOrDefault(g => g.GuestRegistrationId == guestRegistrationId);
        }

        public void UpdateGuestsToCheckIn(List<long> ids)
        {
            Context.GuestReservationItems
               .Where(p => ids.Contains(p.Id))
               .IgnoreQueryFilters()
               .BatchUpdate(new GuestReservationItem { GuestStatusId = (int)ReservationStatus.Checkin });

            Context.SaveChanges();
        }

        public void UpdateRange(List<GuestReservationItem> guestReservationItemList)
        {
            if (guestReservationItemList != null && guestReservationItemList.Count > 0)
            {
                Context.BulkUpdate(guestReservationItemList);

                Context.SaveChanges();
            }
        }

        public void UpdateGuestsToConfirmAndEstimatedArrivalDate(long reservationItemId, DateTime estimatedArrivalDate)
        {
            Context.GuestReservationItems
            .Where(g => g.TenantId == _applicationUser.TenantId && !g.IsDeleted &&
                        g.ReservationItemId == reservationItemId)
            .IgnoreQueryFilters()
            .BatchUpdate(new GuestReservationItem
            {
                GuestStatusId = (int)ReservationStatus.ToConfirm,
                EstimatedArrivalDate = estimatedArrivalDate
            }, new List<string> { nameof(GuestReservationItem.GuestStatusId) });
        }

        public void UpdateAndSaveChanges(GuestReservationItem guestReservationItem)
        {
            Context.Update(guestReservationItem);
            Context.SaveChanges();
        }
    }
}
