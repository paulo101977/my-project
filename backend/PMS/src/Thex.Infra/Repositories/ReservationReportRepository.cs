﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using System;
using Thex.Domain.Entities;
using Thex.Domain.Interfaces.Repositories;
using Thex.Dto.Report;
using Thex.Dto.Reservation;
using Thex.Infra.Context;
using Tnf.EntityFrameworkCore;
using Tnf.EntityFrameworkCore.Repositories;
using System.Linq;
using Thex.Kernel;

namespace Thex.Infra.Repositories
{
    public class ReservationReportRepository : EfCoreRepositoryBase<ThexContext, Reservation>, IReservationReportRepository
    {
        private readonly IApplicationUser _applicationUser;

        public ReservationReportRepository(
            IDbContextProvider<ThexContext> dbContextProvider,
            IApplicationUser applicationUser)
            : base(dbContextProvider)
        {
            _applicationUser = applicationUser;
        }

        public PostHousekeepingRoomDisagreementDto PostHousekeepingRoomDisagreement(PostHousekeepingRoomDisagreementDto request, DateTime? systemDate)
        {
            var contextDatabase = Context.Database;
            var conn = contextDatabase.GetDbConnection();

            if (conn.State == System.Data.ConnectionState.Closed)
                conn.Open();

            var housekeepingRoomDisagreementId = request.HousekeepingRoomDisagreementId.HasValue ? request.HousekeepingRoomDisagreementId : Guid.NewGuid();

            if (request.HousekeepingRoomDisagreementId.HasValue)
            {
                using (var commandUpdate = conn.CreateCommand())
                {
                    string housekeepingRoomDisagreementStatusId = "null";
                    string adultCount = "null";
                    string bagCount = "null";
                    string housekeepingRoomInspectionId = "null";

                    if (request.HousekeepingRoomDisagreementStatusId.HasValue)
                        housekeepingRoomDisagreementStatusId = request.HousekeepingRoomDisagreementStatusId.ToString();

                    if (request.DisagreementAdultCount.HasValue)
                        adultCount = request.DisagreementAdultCount.ToString();

                    if (request.BagCount.HasValue)
                        bagCount = request.BagCount.ToString();

                    if (request.HousekeepingRoomInspectionId.HasValue)
                        housekeepingRoomInspectionId = request.HousekeepingRoomInspectionId.ToString();

                    var queryUpdate = $"update HousekeepingRoomDisagreement set AdultCount = {adultCount}, BagCount = {bagCount}, " +
                                      $"Observation = '{request.Observation}', HousekeepingRoomDisagreementStatusId = " +
                                      $"{housekeepingRoomDisagreementStatusId}, LastModificationTime = (SELECT CAST(CAST('{systemDate.Value.Year.ToString()}' AS varchar) + '-' + CAST('{systemDate.Value.Month.ToString()}' AS varchar) + '-' + " +
                                      $"CAST('{systemDate.Value.Day.ToString()}' AS varchar) AS DATETIME))," +
                                      $"HousekeepingRoomInspectionId = {housekeepingRoomInspectionId} " +
                                      $"where HousekeepingRoomDisagreementId = '{housekeepingRoomDisagreementId}'";

                    if (contextDatabase.CurrentTransaction != null)
                        commandUpdate.Transaction = contextDatabase.CurrentTransaction.GetDbTransaction();

                    commandUpdate.CommandText = queryUpdate;

                    commandUpdate.ExecuteNonQuery();

                    commandUpdate.Dispose();
                }
            }

            else if (!request.HousekeepingStatusId.HasValue)
            {
                using (var commandInsert = conn.CreateCommand())
                {
                    string housekeepingRoomId = "null";
                    string housekeepingRoomDisagreementStatusId = "null";
                    string adultCount = "null";
                    string bagCount = "null";
                    string housekeepingRoomInspectionId = "null";

                    if (request.HousekeepingRoomId.HasValue)
                        housekeepingRoomId = $"'{request.HousekeepingRoomId}'";

                    if (request.HousekeepingRoomDisagreementStatusId.HasValue)
                        housekeepingRoomDisagreementStatusId = request.HousekeepingRoomDisagreementStatusId.ToString();

                    if (request.DisagreementAdultCount.HasValue)
                        adultCount = request.DisagreementAdultCount.ToString();

                    if (request.BagCount.HasValue)
                        bagCount = request.BagCount.ToString();

                    if (request.HousekeepingRoomInspectionId.HasValue)
                        housekeepingRoomInspectionId = request.HousekeepingRoomInspectionId.ToString();

                    var queryInsert = $"insert into HousekeepingRoomDisagreement ([HousekeepingRoomDisagreementId],[PropertyId]," +
                                      $"[HousekeepingRoomId],[AdultCount],[BagCount],[Observation],[IsDeleted],[CreationTime]," +
                                      $"[HousekeepingRoomDisagreementStatusId],[OwnerId],[RoomId],[HousekeepingRoomInspectionId]) " +
                                      $"VALUES('{housekeepingRoomDisagreementId}',{request.PropertyId},{housekeepingRoomId}," +
                                      $"{adultCount},{bagCount},'{request.Observation}',0," +
                                      $"(SELECT CAST(CAST('{systemDate.Value.Year.ToString()}' AS varchar) + '-' + CAST('{systemDate.Value.Month.ToString()}' AS varchar) + '-' + " +
                                      $"CAST('{systemDate.Value.Day.ToString()}' AS varchar) AS DATETIME))," +
                                      $"{housekeepingRoomDisagreementStatusId},'{_applicationUser.UserUid}',{request.RoomId}," +
                                      $"{housekeepingRoomInspectionId})";

                    if (contextDatabase.CurrentTransaction != null)
                        commandInsert.Transaction = contextDatabase.CurrentTransaction.GetDbTransaction();

                    commandInsert.CommandText = queryInsert;

                    commandInsert.ExecuteNonQuery();

                    request.HousekeepingRoomDisagreementId = housekeepingRoomDisagreementId;

                    commandInsert.Dispose();
                }
            }
            else 
            {
                var housekeepingStatusPropertyId = Context.HousekeepingStatusProperties
                    .FirstOrDefault(x => x.HousekeepingStatusId.Equals(request.HousekeepingStatusId.Value)
                    && x.PropertyId.Equals(request.PropertyId)).Id;

                using (var commandUpdate = conn.CreateCommand())
                {

                    var queryUpdate = $"update Room Set HousekeepingStatusPropertyId = '{housekeepingStatusPropertyId}'" +
                                      $"where RoomId = {request.RoomId}";

                    if (contextDatabase.CurrentTransaction != null)
                        commandUpdate.Transaction = contextDatabase.CurrentTransaction.GetDbTransaction();

                    commandUpdate.CommandText = queryUpdate;

                    commandUpdate.ExecuteNonQuery();

                    commandUpdate.Dispose();
                }

            }


            return request;
        }

        public PostMeanPlanControlReportDto PostMeanPlan(PostMeanPlanControlReportDto request, Guid tenantId, DateTime? systemDate)
        {
            var contextDatabase = Context.Database;
            var conn = contextDatabase.GetDbConnection();

            if (conn.State == System.Data.ConnectionState.Closed)
                conn.Open();

            var guestMealPlanControlId = string.IsNullOrEmpty(request.GuestMealPlanControlId) ? Guid.NewGuid().ToString() : request.GuestMealPlanControlId;

            using (var command = conn.CreateCommand())
            {
                var querySelect = $"select distinct GuestMealPlanControlId, RoomNumber as UH, MealPlanTypeName, FullName, " +
                            $"case IsMain when 1 then 'Principal' when 0 then 'Acompanhante' end as [Type], " +
                            $"IsChild, isnull(Coffee,0) as Coffee, isnull(Lunch,0) as Lunch, isnull(Dinner,0) as Dinner " +
                            $"from Reservation a join ReservationItem b on a.ReservationId = b.ReservationId " +
                            $"join GuestReservationItem c on c.ReservationItemId = b.ReservationItemId " +
                            $"left join GuestRegistration d on d.GuestId = c.GuestId " +
                            $"join ReservationBudget e on e.ReservationItemId = c.ReservationItemId " +
                            $"join MealPlanType f on f.MealPlanTypeId = e.MealPlanTypeId " +
                            $"left join Room g on g.RoomId = b.RoomId " +
                            $"left join GuestMealPlanControl h on h.GuestReservationItemId = c.GuestReservationItemId " +
                            $"where GuestMealPlanControlId = '{guestMealPlanControlId}' " +
                            $"order by RoomNumber, [Type] desc";

                if (contextDatabase.CurrentTransaction != null)
                    command.Transaction = contextDatabase.CurrentTransaction.GetDbTransaction();

                command.CommandText = querySelect;

                var reader = command.ExecuteReader();

                if (reader.HasRows)
                {

                    reader.Dispose();

                    using (var commandUpdate = conn.CreateCommand())
                    {
                        var queryUpdate = $"update GuestMealPlanControl set Coffee = '{request.Coffee}', Lunch = '{request.Lunch}', Dinner = '{request.Dinner}' " +
                                          $"where GuestMealPlanControlId = '{guestMealPlanControlId}'";

                        if (contextDatabase.CurrentTransaction != null)
                            commandUpdate.Transaction = contextDatabase.CurrentTransaction.GetDbTransaction();

                        commandUpdate.CommandText = queryUpdate;

                        commandUpdate.ExecuteNonQuery();

                        commandUpdate.Dispose();
                    }


                    if (!request.Coffee && !request.Lunch && !request.Dinner)
                    {
                        using (var commandDelete = conn.CreateCommand())
                        {
                            var queryDelete = $"delete from GuestMealPlanControl where GuestMealPlanControlId = '{guestMealPlanControlId}'";

                            if (contextDatabase.CurrentTransaction != null)
                                commandDelete.Transaction = contextDatabase.CurrentTransaction.GetDbTransaction();

                            commandDelete.CommandText = queryDelete;

                            commandDelete.ExecuteNonQuery();

                            request.GuestMealPlanControlId = "";

                            commandDelete.Dispose();
                        }

                    }

                }
                else
                {

                    reader.Dispose();

                    if (!(!request.Coffee && !request.Lunch && !request.Dinner))
                    {
                        using (var commandInsert = conn.CreateCommand())
                        {      
                            var queryInsert = $"insert into GuestMealPlanControl ([GuestMealPlanControlId],[GuestReservationItemId],[Coffee],[Lunch],[Dinner]," +
                            $"[GuestMealPlanControlDate],[PropertyId],[TenantId]) " +
                            $"VALUES('{guestMealPlanControlId}',{request.GuestReservationItemId},'{request.Coffee}','{request.Lunch}','{request.Dinner}',(SELECT CAST(CAST('{systemDate.Value.Year.ToString()}' AS varchar) + '-' + CAST('{systemDate.Value.Month.ToString()}' AS varchar) + '-' + " +
                            $"CAST('{systemDate.Value.Day.ToString()}' AS varchar) AS DATETIME))," +
                            $"{request.PropertyId}, '{tenantId}')";

                            if (contextDatabase.CurrentTransaction != null)
                                commandInsert.Transaction = contextDatabase.CurrentTransaction.GetDbTransaction();

                            commandInsert.CommandText = queryInsert;

                            commandInsert.ExecuteNonQuery();

                            request.GuestMealPlanControlId = guestMealPlanControlId;

                            commandInsert.Dispose();
                        }
                    }    
                }               

            }

            return request;
        }

    }
}
