﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using Thex.Domain.Entities;
using Thex.Domain.Interfaces.Repositories;
using Thex.Dto;
using Thex.Infra.Context;
using Tnf.EntityFrameworkCore;
using Tnf.EntityFrameworkCore.Repositories;

namespace Thex.Infra.Repositories
{
    public class ChannelRepository : EfCoreRepositoryBase<ThexContext, Channel>, IChannelRepository
    {
        public ChannelRepository(IDbContextProvider<ThexContext> dbContextProvider) : base(dbContextProvider)
        {

        }

        public void Create(Channel channel)
        {
            base.Insert(channel);
            Context.SaveChanges();
        }

        public new void Update(Channel channel)
        {
            var channelOld = Context
                .Channels
                .Where(x => x.Id == channel.Id).SingleOrDefault();

            if (channelOld != null)
            {
                Context.Entry(channelOld).State = EntityState.Modified;
                Context.Entry(channelOld).CurrentValues.SetValues(channel);
                Context.SaveChanges();
            }
        }

        public void DeleteChannel(Channel channel)
        {
            var channelOld = Context
                .Channels
                .Where(x => x.Id == channel.Id).SingleOrDefault();

            if (channelOld != null)
            {
                Context.Entry(channelOld).State = EntityState.Modified;
                Context.Entry(channelOld).CurrentValues.SetValues(channel);
                Context.SaveChanges();
            }
        }

        public void ToggleAndSaveIsActive(Guid id)
        {
            Channel channel = base.Get(new DefaultGuidRequestDto(id));
            if (channel != null)
            {
                channel.IsActive = !channel.IsActive;
                Context.SaveChanges();
            }
        }
    }
}
