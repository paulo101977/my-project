﻿// //  <copyright file="BillingAccountItemRepository.cs" company="TOTVS">
// //  Copyright (c) TOTVS. All rights reserved.
// //  </copyright>
namespace Thex.EntityFrameworkCore.Repositories
{
    using System;
    using System.Linq;
    using System.Collections.Generic;
    using Thex.Common.Enumerations;
    using Thex.Domain.Entities;
    using Thex.Domain.Interfaces.Repositories;
    using Thex.Dto;
    using Tnf.EntityFrameworkCore.Repositories;
    using Tnf.EntityFrameworkCore;
    using Thex.Infra.Context;
    using Microsoft.EntityFrameworkCore;
    using EFCore.BulkExtensions;

    public class BillingAccountItemRepository : EfCoreRepositoryBase<ThexContext, BillingAccountItem>, IBillingAccountItemRepository
    {
        public BillingAccountItemRepository(IDbContextProvider<ThexContext> dbContextProvider)
            : base(dbContextProvider)
        {
        }

        public void AddRange(List<BillingAccountItem> billingAccountItems)
        {
            Context.BillingAccountItems.AddRange(billingAccountItems);
            Context.SaveChanges();
        }

        public void TransferRange(Guid billingAccountDestination, List<BillingAccountItem> billingAccountItems)
        {
            foreach (var billingAccountItem in billingAccountItems)
            {
                int revertAccountItemTypeId;

                //itens divididos
                //caso seja um item que tenha sido divido
                //ou
                //caso seja um item que tenha sido dividido mas que foi transferido e que está voltando a sua conta origem
                //executa: inverte os valores do billingaccountitemtype e billingaccountitemtypelastsource
                if (billingAccountItem.BillingAccountItemTypeIdLastSource.HasValue &&
                    (billingAccountItem.BillingAccountItemTypeId == (int)BillingAccountItemTypeEnum.BillingAccountItemTypePartial) ||
                    (
                    billingAccountItem.BillingAccountItemTypeIdLastSource.HasValue &&
                    billingAccountItem.BillingAccountItemTypeIdLastSource.Value == (int)BillingAccountItemTypeEnum.BillingAccountItemTypePartial &&
                    billingAccountItem.PartialParentBillingAccountItemId == billingAccountDestination)
                    )
                {
                    revertAccountItemTypeId = billingAccountItem.BillingAccountItemTypeId;

                    billingAccountItem.SetTypeIdAndTypeIdLastSource(billingAccountItem.BillingAccountItemTypeIdLastSource.Value, revertAccountItemTypeId);
                }
                billingAccountItem.SetAccountIdLastSourceAndAccountId(billingAccountItem.BillingAccountId, billingAccountDestination);


                Context.Entry(billingAccountItem).State = EntityState.Modified;

                Context.SaveChanges();
            }
        }

        public IList<BillingAccountItem> GetListByInvoiceId(Guid invoiceId)
        {
            return (from bai in Context.BillingAccountItems
                    join biv in Context.BillingInvoices on bai.BillingInvoiceId equals biv.Id
                    join bi in Context.BillingItems on bai.BillingItemId equals bi.Id
                    join bipst in Context.BillingInvoicePropertySupportedTypes on bi.Id equals bipst.BillingItemId into
                    leftJoin
                    where 
                    biv.Id == invoiceId && 
                    (bai.BillingAccountItemTypeId == (int)BillingAccountItemTypeEnum.BillingAccountItemTypeDebit ||
                    bai.BillingAccountItemTypeId == (int)BillingAccountItemTypeEnum.BillingAccountItemTypePartial ||
                    bai.BillingAccountItemTypeId == (int)BillingAccountItemTypeEnum.BillingAccountItemCreditNote)
                    && !bai.WasReversed
                    select bai).Include(b => b.BillingItem).ToList();
        }

        public BillingAccountItem GetById(Guid id)
        {
            return Context.BillingAccountItems.Find(id);
        }

        public void UpdateRange(List<BillingAccountItem> billingAccountItem)
        {
            Context.UpdateRange(billingAccountItem);

            Context.SaveChanges();
        }

        public void CreateBulk(List<BillingAccountItem> billingAccountItemList)
        {
            if (billingAccountItemList.Any())
                Context.BulkInsert(billingAccountItemList);
        }

        public void UpdateBulk(List<BillingAccountItem> billingAccountItemList)
        {
            if (billingAccountItemList.Any())
                Context.BulkUpdate(billingAccountItemList);
        }
    }
}