﻿using EFCore.BulkExtensions;
using System.Collections.Generic;
using System.Linq;
using Thex.Domain.Entities;
using Thex.Domain.Interfaces.Repositories;
using Thex.Infra.Context;
using Tnf.EntityFrameworkCore;
using Tnf.EntityFrameworkCore.Repositories;

namespace Thex.EntityFrameworkCore.Repositories
{
    public class ReservationItemLaunchRepository : EfCoreRepositoryBase<ThexContext, ReservationItemLaunch>, IReservationItemLaunchRepository
    {
        public ReservationItemLaunchRepository(
            IDbContextProvider<ThexContext> dbContextProvider)
            : base(dbContextProvider)
        {
        }

        public void InsertList(List<ReservationItemLaunch> entityList)
        {
            Context.BulkInsert(entityList);

            Context.SaveChanges();
        }

        public void UpdateList(List<ReservationItemLaunch> entityList)
        {
            Context.BulkUpdate(entityList);

            Context.SaveChanges();
        }
    }
}
