﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Thex.Common;
using Thex.Common.Enumerations;
using Thex.Domain.Entities;
using Thex.Domain.Interfaces.Repositories;
using Tnf.EntityFrameworkCore.Repositories;
using Tnf.EntityFrameworkCore;
using Thex.Infra.Context;
using Tnf.Notifications;
using Thex.Dto;

namespace Thex.EntityFrameworkCore.Repositories
{
   public class BillingItemCategoryRepository : EfCoreRepositoryBase<ThexContext, BillingItemCategory>, IBillingItemCategoryRepository
    {
        private readonly INotificationHandler Notification;

        public BillingItemCategoryRepository(IDbContextProvider<ThexContext> dbContextProvider,
            INotificationHandler notificationHandler)
            : base(dbContextProvider)
        {
            Notification = notificationHandler;
        }

        public void ToggleAndSaveIsActive(int id)
        {
            BillingItemCategory  billingItemCategory= base.Get(new DefaultIntRequestDto(id));
            billingItemCategory.IsActive = !billingItemCategory.IsActive;
            Context.SaveChanges();
        }

        public void BillingItemCategoryDelete(int id)
        {
            try
            {
                var obj = base.Get(new DefaultIntRequestDto(id));
                base.Delete(obj);
                Context.SaveChanges();
            }
            catch (Exception)
            {
                Context.ChangeTracker.Entries()
                   .Where(e => e.Entity != null).ToList()
                   .ForEach(e => e.State = EntityState.Detached);

                Notification.Raise(Notification.DefaultBuilder
                        .WithMessage(AppConsts.LocalizationSourceName, BillingItemCategory.EntityError.BillingItemCategoryForeignKeyError)
                        .WithDetailedMessage(AppConsts.LocalizationSourceName, BillingItemCategory.EntityError.BillingItemCategoryForeignKeyError)
                        .Build());

            }
        }
    }
}
