﻿//  <copyright file="RoomRepository.cs" company="TOTVS">
//  Copyright (c) TOTVS. All rights reserved.
//  </copyright>
namespace Thex.EntityFrameworkCore.Repositories
{
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;

    using Thex.Domain.Entities;

    using Microsoft.EntityFrameworkCore;

    using Tnf.EntityFrameworkCore.Repositories;
    using Tnf.EntityFrameworkCore;
    using Thex.Domain.Interfaces.Repositories;
    using Thex.Common;
    using Thex.Common.Enumerations;
    using System;
    using Thex.Common.Extensions;
    using Thex.Infra.Context;
    using Thex.Dto;
    using Tnf.Notifications;
    using Thex.Infra.ReadInterfaces;
    using Thex.Domain.JoinMap;
    using EFCore.BulkExtensions;
    using Thex.Kernel;

    public class RoomRepository : EfCoreRepositoryBase<ThexContext, Room>, IRoomRepository
    {
        private readonly IPropertyParameterReadRepository _propertyParameterReadRepository;
        private readonly INotificationHandler Notification;
        private readonly IApplicationUser _applicationUser;

        public RoomRepository(
            IDbContextProvider<ThexContext> dbContextProvider,
            IPropertyParameterReadRepository propertyParameterReadRepository,
            IApplicationUser applicationUser,
            INotificationHandler notificationHandler)
            : base(dbContextProvider)
        {
            _propertyParameterReadRepository = propertyParameterReadRepository;
            Notification = notificationHandler;
            _applicationUser = applicationUser;
        }

        public bool ToggleAndSaveIsActive(int id)
        {

            Room roomPoco = base.Get(new DefaultIntRequestDto(id));
            roomPoco.IsActive = !roomPoco.IsActive;
            Context.SaveChanges();
            return roomPoco.IsActive;
        }

        public void UpdateChildRoomList(IList<int> ids, int parentId)
        {
            var rooms = Context
                .Rooms
                .Where(x => ids.Contains(x.Id));

            foreach (var room in rooms)
            {
                room.ParentRoomId = parentId;
                Update(room);
            }
        }

        public void RemoveParentIdOfChildren(IList<int> ids)
        {
            if (!ids.IsNullOrEmpty<int>())
            {
                var rooms = Context
                    .Rooms
                    .Where(x => ids.Contains(x.Id));

                foreach (var room in rooms)
                {
                    room.ParentRoomId = null;
                    Update(room);
                }
            }
        }

        public IList<int> GetChildrenIdsOfParentExcept(int id, IList<int> exceptIds)
        {
            return exceptIds != null && exceptIds.Any()
                       ? Context
                            .Rooms
                            .Where(x => !exceptIds.Contains(x.Id) && x.ParentRoomId.Value == id)
                            .Select(x => x.Id)
                            .ToList()
                       : null;
        }



        public IList<int> GetChildrenIds(int id)
        {
            return Context
                .Rooms
                .Where(x => (x.ParentRoomId.HasValue && x.ParentRoomId.Value == id) && x.IsActive)
                .Select(x => x.Id)
                .ToList();
        }

        public void Delete(int id)
        {
            try
            {
                var room = base.Get(new DefaultIntRequestDto(id));
                base.Delete(room);

                Context.SaveChanges();
            }
            catch
            {
                
                Context.ChangeTracker.Entries()
                  .Where(e => e.Entity != null).ToList()
                  .ForEach(e => e.State = EntityState.Detached);

                Notification.Raise(Notification.DefaultBuilder
                        .WithMessage(AppConsts.LocalizationSourceName, RoomEnum.Error.RoomForeignKeyError)
                        .WithDetailedMessage(AppConsts.LocalizationSourceName, RoomEnum.Error.RoomForeignKeyError)
                        .Build());
            }
        }

        public void UpdateRoomForHousekeeppingStatus(int roomId, Guid housekeepingStatusPropertyId)
        {
            var roomOld = base.Get(new DefaultIntRequestDto(roomId));
            roomOld.HousekeepingStatusPropertyId = housekeepingStatusPropertyId;
            roomOld.HousekeepingStatusLastModificationTime = DateTime.UtcNow.ToZonedDateTimeLoggedUser();
            
            Context.SaveChanges();
        }

        public void UpdateManyRoomForHousekeeppingStatus(List<int> roomIds, Guid housekeepingStatusPropertyId)
        {
            var roomOldList = Context.Rooms.Where(e => roomIds.Contains(e.Id)).ToList();
            foreach (var room in roomOldList)
            {
                room.HousekeepingStatusPropertyId = housekeepingStatusPropertyId;
                room.HousekeepingStatusLastModificationTime = DateTime.UtcNow.ToZonedDateTimeLoggedUser();
                room.LastModificationTime = DateTime.UtcNow.ToZonedDateTimeLoggedUser(_applicationUser);
                room.LastModifierUserId = _applicationUser.UserUid;
            }

            Context.BulkUpdate(roomOldList);
        }

        public Room GetByReservationItemId(long reservationItemId)
        {
            return (from room in Context.Rooms
                    join reservationItem in Context.ReservationItems on room.Id equals reservationItem.RoomId
                    where reservationItem.Id == reservationItemId
                    select room
                    ).FirstOrDefault();
        }

        public Room GetByGuestReservationItemId(long guestReservationItemId)
        {
            return (from guestReservationItem in Context.GuestReservationItems
                    join reservationItem in Context.ReservationItems on guestReservationItem.ReservationItemId equals reservationItem.Id
                    join room in Context.Rooms on reservationItem.RoomId equals room.Id
                    where guestReservationItem.Id == guestReservationItemId
                    select room
                    ).FirstOrDefault();
        }

        public IQueryable<RoomReservationItemJoinMap> GetAllOccupiedRoomList()
        {
            var statusList = new List<int>()
            {
                (int)ReservationStatus.Checkin,
            };

            return (from room in Context.Rooms
                    join reservationItem in Context.ReservationItems.Where(ri =>
                                    statusList.Contains(ri.ReservationItemStatusId))
                               on room.Id equals reservationItem.RoomId into a
                    from reservitem in a.DefaultIfEmpty()
                    select new RoomReservationItemJoinMap
                    {
                        ReservationItem = reservitem,
                        Room = room
                    });
        }

        public IQueryable<RoomBlockingJoinMap> GetAllBlockingRoomList(int propertyId)
        {
            var statusList = new List<int>()
            {
                (int)ReservationStatus.Checkin,
            };

            DateTime? systemDate = _propertyParameterReadRepository.GetSystemDate(propertyId);            

            return (from room in Context.Rooms

                              join RoomBlocking in Context.RoomBlockings.Where(x =>
                                                   x.BlockingStartDate.Date <=
                                                   systemDate && x.BlockingEndDate
                                                   >= systemDate && !x.IsDeleted)
                              on room.Id equals RoomBlocking.RoomId into b
                              from leftRoomBlocking in b.DefaultIfEmpty()                             

                    select new RoomBlockingJoinMap
                    {                        
                        RoomBlocking = leftRoomBlocking,
                        Room = room
                    });
            
        }

        public IList<Room> GetAllByIdList(IEnumerable<int> roomIdList)
        {
            return Context.Rooms
                .Where(r => roomIdList.Contains(r.Id))
                .ToList();
        }

        public void UpdateAndSaveChanges(Room roomNew)
        {
            var roomOld = Context.Rooms.FirstOrDefault(r => r.Id.Equals(roomNew.Id));

            Context.Entry(roomOld).State = EntityState.Modified;
            Context.Entry(roomOld).CurrentValues.SetValues(roomNew);

            Context.SaveChanges();
        }

        public void CleanAllRoomIdInReservationItem(int roomId)
        {
            Context.ReservationItems.Where(r => r.RoomId.Value.Equals(roomId) && r.TenantId.Equals(_applicationUser.TenantId) && (
                                                r.ReservationItemStatusId.Equals((int)ReservationStatus.ToConfirm) ||
                                                r.ReservationItemStatusId.Equals((int)ReservationStatus.Confirmed)))
                                    .IgnoreQueryFilters()
                                    .BatchUpdate(new ReservationItem { RoomId = null }, new List<string> { nameof(ReservationItem.RoomId) });
        }
    }
}