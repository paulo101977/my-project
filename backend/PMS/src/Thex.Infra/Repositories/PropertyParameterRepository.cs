﻿using System;
using System.Linq;
using Thex.Common;
using Thex.Common.Enumerations;
using Thex.Domain.Entities;
using Thex.Domain.Interfaces.Repositories;
using Tnf.EntityFrameworkCore.Repositories;
using Tnf.EntityFrameworkCore;
using Thex.Infra.Context;
using Tnf.Notifications;
using Thex.Dto;
using Microsoft.EntityFrameworkCore;
using Thex.Kernel;

namespace Thex.EntityFrameworkCore.Repositories
{
    public class PropertyParameterRepository : EfCoreRepositoryBase<ThexContext, PropertyParameter>, IPropertyParameterRepository
    {
        private readonly INotificationHandler Notification;
        private readonly IApplicationUser _applicationUser;

        public PropertyParameterRepository(IDbContextProvider<ThexContext> dbContextProvider,
            INotificationHandler notificationHandler,
            IApplicationUser applicationUser) : base(dbContextProvider)
        {
            Notification = notificationHandler;
            _applicationUser = applicationUser;
        }

        public new void Update(PropertyParameter propertyParameter)
        {
            var propertyParameterOld = Context
                .PropertyParameters
                
                .Where(x => x.Id == propertyParameter.Id &&  x.ApplicationParameterId == propertyParameter.ApplicationParameterId && x.PropertyId == propertyParameter.PropertyId).SingleOrDefault();

            if (propertyParameterOld != null)
            {
                propertyParameter.Id = propertyParameterOld.Id;
                Context.Entry(propertyParameterOld).State = EntityState.Modified;
                Context.Entry(propertyParameterOld).CurrentValues.SetValues(propertyParameter);
            }
            else
                Context.PropertyParameters.Add(propertyParameter);

            Context.SaveChanges();
        }

        public void ToggleAndSaveIsActive(Guid id)
        {
            PropertyParameter propertyParameter = base.Get(new DefaultGuidRequestDto(id));
            propertyParameter.IsActive = !propertyParameter.IsActive;

            if (propertyParameter.ApplicationParameterId == (int)ApplicationParameterEnum.ParameterChildren_2 && !propertyParameter.IsActive && CheckChildren_3_IsActive())
            {
                Notification.Raise(Notification.DefaultBuilder
                .WithMessage(AppConsts.LocalizationSourceName, PropertyParameter.EntityError.PropertyParameterIsNotDeactivateChildren_2)
                .WithDetailedMessage(AppConsts.LocalizationSourceName, PropertyParameter.EntityError.PropertyParameterIsNotDeactivateChildren_2)
                .Build());
                return;
            }
            Context.SaveChanges();
        }

        private bool CheckChildren_3_IsActive()
        {
            return Context
                .PropertyParameters
                
                .Any(x => x.ApplicationParameterId == (int)ApplicationParameterEnum.ParameterChildren_3 && x.IsActive);
        }
    }
}
