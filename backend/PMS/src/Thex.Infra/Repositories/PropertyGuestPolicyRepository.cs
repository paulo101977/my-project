﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using Thex.Domain.Entities;
using Thex.Domain.Interfaces.Repositories;
using Thex.Dto;
using Thex.Infra.Context;
using Tnf.EntityFrameworkCore;
using Tnf.EntityFrameworkCore.Repositories;
using Tnf.Notifications;

namespace Thex.Infra.Repositories
{
    public class PropertyGuestPolicyRepository : EfCoreRepositoryBase<ThexContext, PropertyPolicy>, IPropertyGuestPolicyRepository
    {
        private readonly INotificationHandler Notification;

        public PropertyGuestPolicyRepository(IDbContextProvider<ThexContext> dbContextProvider, 
                                             INotificationHandler notificationHandler) : base(dbContextProvider)
        {
            Notification = notificationHandler;            
        }

        public void Create(PropertyPolicy propertyPolicy)
        {            
            Context.PropertyPolicies.Add(propertyPolicy);
            Context.SaveChanges();
        }

        void IPropertyGuestPolicyRepository.Update(PropertyPolicy propertyPolicy)
        {
            var propertyPolicyOld = Context.PropertyPolicies.FirstOrDefault(x => x.Id == propertyPolicy.Id);

            if (propertyPolicyOld != null)
            {
                Context.Entry(propertyPolicyOld).State = EntityState.Modified;
                Context.Entry(propertyPolicyOld).CurrentValues.SetValues(propertyPolicy);
                Context.SaveChanges();
            }
        }

        public void Delete(Guid id)
        {
            var obj = base.Get(new DefaultGuidRequestDto(id));
            base.Delete(obj);
            Context.SaveChanges();
        } 
    }
}
