﻿// //  <copyright file="LocationRepository.cs" company="TOTVS">
// //  Copyright (c) TOTVS. All rights reserved.
// //  </copyright>
namespace Thex.EntityFrameworkCore.Repositories
{
    using System.Collections.Generic;
    using Thex.Domain.Entities;
    using Thex.Domain.Interfaces.Repositories;

    using Tnf.EntityFrameworkCore.Repositories;
    using Tnf.EntityFrameworkCore;
    using System.Linq;
    using System;
    using Thex.Infra.Context;
    using Microsoft.EntityFrameworkCore;

    public class LocationRepository : EfCoreRepositoryBase<ThexContext, Location>, ILocationRepository
    {
        public LocationRepository(IDbContextProvider<ThexContext> dbContextProvider)
            : base(dbContextProvider)
        {
        }

        public void AddRange(IList<Location> locationList)
        {
            locationList.ToList().ForEach(l => { l.CountryId = l.CountryId == 0 ? null : l.CountryId; l.CityId = l.CityId == 0 ? null : l.CityId; l.StateId = l.StateId == 0 ? null : l.StateId; });
            Context.Locations.AddRange(locationList);
            Context.SaveChanges();
        }

        public void UpdateLocationList(IList<Location> locationList, Guid ownerId)
        {
            var existingLocations = Context.Locations
                           .Where(exp => exp.OwnerId == ownerId)
                           .ToList();

            if (existingLocations != null)
            {
                RemoveLocations(locationList, existingLocations);

                foreach (var location in locationList)
                    InsertOrUpdateLocation(ownerId, existingLocations, location);
            }
            Context.SaveChanges();
        }

        private void InsertOrUpdateLocation(Guid ownerId, List<Location> existingLocations, Location location)
        {
            var existingLocation = existingLocations
                                   .Where(d => d.Id == location.Id && d.Id != 0)
                                   .FirstOrDefault();

            location.OwnerId = ownerId;

            if (existingLocation != null)
            {
                Context.Entry(existingLocation).State = EntityState.Modified;
                Context.Entry(existingLocation).CurrentValues.SetValues(location);
            }
            else
                existingLocations.Add(location);

        }

        private void RemoveLocations(IList<Location> locationList, List<Location> existingLocations)
        {
            var locationIds = locationList.Select(e => e.Id).ToList();
            var locationListToExclude = existingLocations.Where(d => locationIds.Contains(d.Id)).ToList();

            if (locationListToExclude.Count > 0)
                Context.RemoveRange(locationListToExclude);
            
        }

        public void CreateOrUpdate(Location location, Guid ownerId)
        {
            location.CountryId = location.CountryId == 0 ? null : location.CountryId;
            location.StateId = location.StateId == 0 ? null : location.StateId;
            location.CityId = location.CityId == 0 ? null : location.CityId;

            var existingLocation = Context.Locations
                           .Where(exp => exp.OwnerId == ownerId)
                           .FirstOrDefault();

            location.OwnerId = ownerId;

            if (existingLocation != null)
            {
                location.Id = existingLocation.Id;
                Context.Entry(existingLocation).State = EntityState.Modified;
                Context.Entry(existingLocation).CurrentValues.SetValues(location);
            }
            else
                Context.Locations.Add(location);

            Context.SaveChanges();
        }
    }
}