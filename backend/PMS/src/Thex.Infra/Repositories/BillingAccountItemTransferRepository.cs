﻿// //  <copyright file="BillingAccountItemTransferRepository.cs" company="TOTVS">
// //  Copyright (c) TOTVS. All rights reserved.
// //  </copyright>
namespace Thex.EntityFrameworkCore.Repositories
{
    using System;
    using System.Collections.Generic;

    using Thex.Domain.Entities;
    using Thex.Domain.Interfaces.Repositories;

    using Tnf.EntityFrameworkCore.Repositories;
    using Tnf.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore;
    using System.Linq;
    using Thex.Infra.Context;

    public class BillingAccountItemTransferRepository : EfCoreRepositoryBase<ThexContext, BillingAccountItemTransfer>, IBillingAccountItemTransferRepository
    {
        public BillingAccountItemTransferRepository(IDbContextProvider<ThexContext> dbContextProvider)
            : base(dbContextProvider)
        {
        }

        public void AddRange(IList<BillingAccountItemTransfer> billingAccountItemTransferList)
        {
            Context.BillingAccountItemTransfers.AddRange(billingAccountItemTransferList);
            Context.SaveChanges();
        }
    }
}