﻿// //  <copyright file="CompanyClientContactPersonRepository.cs" company="TOTVS">
// //  Copyright (c) TOTVS. All rights reserved.
// //  </copyright>
namespace Thex.EntityFrameworkCore.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Thex.Domain.Entities;
    using Thex.Domain.Interfaces.Repositories;
    using Thex.Domain.Specifications.RoomType;

    using Tnf.EntityFrameworkCore.Repositories;
    using Tnf.EntityFrameworkCore;
    using Thex.Infra.Context;

    public class CompanyClientContactPersonRepository : EfCoreRepositoryBase<ThexContext, CompanyClientContactPerson>, ICompanyClientContactPersonRepository
    {
        public CompanyClientContactPersonRepository(IDbContextProvider<ThexContext> dbContextProvider)
            : base(dbContextProvider)
        {
        }

        public void AddRange(IList<CompanyClientContactPerson> companyClientContactPersonList)
        {
            Context.CompanyClientContactPersons.AddRange(companyClientContactPersonList);
            Context.SaveChanges();
        }

        public void RemoveContactInformationListFromPersonContactByClientId(Guid clientId)
        {
            var companyContactListQueryable =
                Context.CompanyClientContactPersons.Where(c => c.CompanyClientId == clientId);

            if (companyContactListQueryable == null)
            {
                return;
            }           

            ICollection<CompanyClientContactPerson> companyContactListToExcludedContactInformartion =
                companyContactListQueryable.ToList();

            ICollection<ContactInformation> contacInformationListToExcluded = new List<ContactInformation>();

            foreach (var contact in companyContactListToExcludedContactInformartion)
            {
                Person person = Context.Persons.Find(contact.PersonId);

                var contactInformationListFromPerson = Context.ContactInformations.Where(c => c.OwnerId == person.Id)
                    .ToList();

                foreach (var contactInformation in contactInformationListFromPerson)
                {
                    contacInformationListToExcluded.Add(contactInformation);
                }
            }

            Context.RemoveRange(contacInformationListToExcluded);
            Context.SaveChanges();
        }

        public void RemoveRangeFromCompanyClientContactPerson(IList<CompanyClientContactPerson> companyClientContactPersonList)
        {
            ICollection<Person> personListToExcluded = new List<Person>();

            foreach (var companyClientContactPerson in companyClientContactPersonList)
            {
                Person personToExcluded = Context.Persons.Find(companyClientContactPerson.PersonId);
                personListToExcluded.Add(personToExcluded);
            }

            Context.RemoveRange(companyClientContactPersonList);
            Context.RemoveRange(personListToExcluded);
        }
    }
}