﻿// //  <copyright file="CompanyClientRepository.cs" company="TOTVS">
// //  Copyright (c) TOTVS. All rights reserved.
// //  </copyright>
using Thex.Domain.Entities;
using Thex.Domain.Interfaces.Repositories;

namespace Thex.EntityFrameworkCore.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Thex.Common.Enumerations;
    using Thex.Infra.Context;
    using Tnf.EntityFrameworkCore;
    using Tnf.EntityFrameworkCore.Repositories;

    public class ContactInformationRepository : EfCoreRepositoryBase<ThexContext, ContactInformation>, IContactInformationRepository
    {
        public ContactInformationRepository(IDbContextProvider<ThexContext> dbContextProvider)
            : base(dbContextProvider)
        {
        }

        public void AddRangeAndSaveChanges(IList<ContactInformation> contactInformationList)
        {
            Context.ContactInformations.AddRange(contactInformationList);

            Context.SaveChanges();
        }

        public string GetEmailContactInformationByPersonId(Guid guid)
        {
            var email = Context.ContactInformations.Where(x => x.OwnerId == guid && x.ContactInformationTypeId == (int)ContactInformationTypeEnum.Email).FirstOrDefault();

            return email != null ? email.Information : null;
        }

        public void ResetContactInformationsProperty(Guid guid)
        {
            var contactInformations = Context.ContactInformations.Where(x => x.OwnerId == guid);
            if(contactInformations != null && contactInformations.Any())
            {
                foreach (var item in contactInformations)
                {
                    Context.ContactInformations.Remove(item);
                }
                Context.SaveChanges();
            }
        }
    }
}