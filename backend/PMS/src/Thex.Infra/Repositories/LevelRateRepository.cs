﻿using System;
using System.Collections.Generic;
using System.Linq;
using Thex.Domain.Entities;
using Thex.Domain.Interfaces.Repositories;
using Thex.Infra.Context;
using Tnf.EntityFrameworkCore;
using Tnf.EntityFrameworkCore.Repositories;

namespace Thex.Infra.Repositories
{
    public class LevelRateRepository : EfCoreRepositoryBase<ThexContext, LevelRate>, ILevelRateRepository
    {
        public LevelRateRepository(IDbContextProvider<ThexContext> dbContextProvider)
            : base(dbContextProvider)
        {
        }

        public void AddRangeAndSaveChanges(ICollection<LevelRate> levelRateList)
        {
            Context.LevelRates.AddRange(levelRateList);

            Context.SaveChanges();
        }

        public void RemoveRangeByLevelRateHeaderIdAndSaveChanges(Guid levelRateHeaderId)
        {
            var levelRateListToRemove = Context.LevelRates.Where(lr => lr.LevelRateHeaderId == levelRateHeaderId);

            Context.LevelRates.RemoveRange(levelRateListToRemove);

            Context.SaveChanges();
        }
    }
}
