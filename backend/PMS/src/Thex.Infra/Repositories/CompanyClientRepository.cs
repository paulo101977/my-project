﻿//  <copyright file="CompanyClientRepository.cs" company="TOTVS">
//  Copyright (c) TOTVS. All rights reserved.
//  </copyright>
namespace Thex.EntityFrameworkCore.Repositories
{
    using System.Linq;


    using Thex.Domain.Entities;

    using Microsoft.EntityFrameworkCore;
    using Tnf.EntityFrameworkCore.Repositories;
    using Tnf.EntityFrameworkCore;
    using Thex.Domain.Interfaces.Repositories;
    using System;
    using Thex.Infra.Context;
    using Thex.Dto;

    public class CompanyClientRepository : EfCoreRepositoryBase<ThexContext, CompanyClient>, ICompanyClientRepository
    {
        public CompanyClientRepository(IDbContextProvider<ThexContext> dbContextProvider)
            : base(dbContextProvider)
        {
        }

        public void ToggleAndSaveIsActive(Guid id)
        {
            var request = new DefaultGuidRequestDto(id);
            var companyClient = base.Get(request);

            companyClient.IsActive = !companyClient.IsActive;
            
            Context.SaveChanges();
        }

        public new void Update(CompanyClient companyClient)
        {
            var existingCompanyClient = Context.CompanyClients
                .Where(exp => exp.Id == companyClient.Id)
                .Include(exp => exp.Person)
                .ThenInclude(exp => exp.ContactInformationList)
                .Include(exp => exp.Person)
                .ThenInclude(exp => exp.DocumentList)
                .Include(exp => exp.Person)
                .ThenInclude(exp => exp.LocationList)
                .Include(exp => exp.CompanyClientContactPersonList)
                .ThenInclude(exp => exp.Person)
                .ThenInclude(exp => exp.DocumentList)
                .Include(exp => exp.CompanyClientContactPersonList)
                .ThenInclude(exp => exp.Person)
                .ThenInclude(exp => exp.ContactInformationList)
                .SingleOrDefault();

            if (existingCompanyClient != null)
            {
                Context.Entry(existingCompanyClient).State = EntityState.Modified;
                Context.Entry(existingCompanyClient).CurrentValues.SetValues(companyClient);
                existingCompanyClient.TradeName = companyClient.TradeName;
                existingCompanyClient.ShortName = companyClient.ShortName;
                existingCompanyClient.Person.PersonType = companyClient.Person.PersonType;
                existingCompanyClient.Person.FirstName = companyClient.Person.FirstName;
                existingCompanyClient.Person.LastName = companyClient.Person.LastName;
                existingCompanyClient.Person.FullName = companyClient.Person.FullName;

                RemoveLocationListFromCompanyClient(existingCompanyClient, companyClient);
                RemoveContactInformationListFromCompanyClient(existingCompanyClient, companyClient);
                RemoveDocumentListFromCompanyClient(existingCompanyClient, companyClient);

                Context.SaveChanges();

                foreach (var contactInformation in companyClient.Person.ContactInformationList)
                {
                    AddContactInformationListFromCompanyClient(existingCompanyClient, contactInformation);
                }

                foreach (var document in companyClient.Person.DocumentList)
                {
                    AddDocumentListFromCompanyClient(existingCompanyClient, document);
                }

                foreach (var location in companyClient.Person.LocationList)
                {
                    location.CityId = location.CityId == 0 ? null : location.CityId;
                    location.StateId = location.StateId == 0 ? null : location.StateId;
                    location.CountryId = location.CountryId == 0 ? null : location.CountryId;
                    AddLocationListFromCompanyClient(existingCompanyClient, location);
                }


                Context.SaveChanges();

            }
        }

        private void RemoveLocationListFromCompanyClient(CompanyClient existingCompanyClient, CompanyClient companyClient)
        {
            var locationListToExclude = existingCompanyClient.Person.LocationList.Where(l => l.OwnerId == companyClient.PersonId).ToList();

            if (locationListToExclude.Any())
            {
                if (locationListToExclude.Count() > 1)
                {
                    foreach (var location in locationListToExclude)
                    {
                        Context.RemoveRange(location);
                        Context.SaveChanges();
                    }
                }
                else
                    Context.RemoveRange(locationListToExclude);
            }
          
        }

        private void RemoveDocumentListFromCompanyClient(CompanyClient existingCompanyClient, CompanyClient companyClient)
        {
            var documentListToExclude = existingCompanyClient.Person.DocumentList.Where(d => d.OwnerId == companyClient.PersonId).ToList();

            if (documentListToExclude.Any())
            {
                if(documentListToExclude.Count() > 1)
                {
                    foreach (var document in documentListToExclude)
                    {
                        Context.RemoveRange(document);
                        Context.SaveChanges();
                    }
                }
                else
                    Context.RemoveRange(documentListToExclude);
            } 
        }

        private void RemoveContactInformationListFromCompanyClient(CompanyClient existingCompanyClient, CompanyClient companyClient)
        {
            var contactInformationListToExclude = existingCompanyClient.Person.ContactInformationList.Where(c => c.OwnerId == companyClient.PersonId).ToList();
            if (contactInformationListToExclude.Count > 0)
                Context.RemoveRange(contactInformationListToExclude);
        }

        private void AddContactInformationListFromCompanyClient(CompanyClient existingCompanyClient,
                                                                ContactInformation contactInformation)
        {
            existingCompanyClient.Person.ContactInformationList.Add(contactInformation);
        }

        private void AddDocumentListFromCompanyClient(CompanyClient existingCompanyClient, Document document)
        {
            existingCompanyClient.Person.DocumentList.Add(document);
        }

        private void AddLocationListFromCompanyClient(CompanyClient existingCompanyClient, Location location)
        {
            existingCompanyClient.Person.LocationList.Add(location);
        }
    }
}