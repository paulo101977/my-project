﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Thex.Domain.Entities;
using Thex.Domain.Interfaces.Repositories;
using Tnf.EntityFrameworkCore.Repositories;
using Tnf.EntityFrameworkCore;
using Thex.Infra.Context;
using Microsoft.EntityFrameworkCore;

namespace Thex.EntityFrameworkCore.Repositories
{
    public class CompanyRepository : EfCoreRepositoryBase<ThexContext, Company>, ICompanyRepository
    {
        public CompanyRepository(IDbContextProvider<ThexContext> dbContextProvider)
           : base(dbContextProvider)
        {
        }


        public void CompanyUpdate(Company company)
        {
            var existingCompany = Context.Companies
                    .Where(exp => exp.Id == company.Id)
                    .SingleOrDefault();

            company.PersonId = existingCompany.PersonId;
            company.CompanyUid = existingCompany.CompanyUid;
            if (existingCompany != null)
            {
                Context.Entry(existingCompany).State = EntityState.Modified;
                Context.Entry(existingCompany).CurrentValues.SetValues(company);
                Context.SaveChanges();
            }
        }
    }
}
