﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Thex.Common;
using Thex.Common.Enumerations;
using Thex.Domain.Entities;
using Thex.Domain.Interfaces.Repositories;
using Tnf.EntityFrameworkCore.Repositories;
using Tnf.EntityFrameworkCore;
using Thex.Infra.Context;
using Thex.Dto;

namespace Thex.EntityFrameworkCore.Repositories
{
    public class RatePlanRepository : EfCoreRepositoryBase<ThexContext, RatePlan>, IRatePlanRepository
    {
        public RatePlanRepository(IDbContextProvider<ThexContext> dbContextProvider) : base(dbContextProvider)
        {
        }
        public void RatePlanCreate(RatePlan RatePlan)
        {
            Context.RatePlans.Add(RatePlan);
            Context.SaveChanges();
        }

        public void RatePlanUpdate(RatePlan ratePlanOld, RatePlan ratePlanNew, List<RatePlanRoomType> ratePlanRoomTypeList, List<RatePlanCompanyClient> ratePlanCompanyClientList, List<RatePlanCommission> ratePlanCommissionList)
        {

            if (ratePlanOld != null && ratePlanNew != null)
            {
                var roomTypesOld = ratePlanOld.RatePlanRoomTypeList;
                var companyClientOld = ratePlanOld.RatePlanCompanyClientList;
                var comissionOld = ratePlanOld.RatePlanCommissionList;

                Context.Entry(ratePlanOld).State = EntityState.Modified;
                Context.Entry(ratePlanOld).CurrentValues.SetValues(ratePlanNew);

                var (roomTypesForRemove, roomTypesForUpdate, roomTypesForInsert) = SeparateRoomType(ratePlanOld.RatePlanRoomTypeList.ToList(), ratePlanRoomTypeList);
                ValidationRatePlanRoomType(roomTypesOld, roomTypesForRemove, roomTypesForUpdate, roomTypesForInsert);

                if (ratePlanNew.AgreementTypeId == (int)AgreementTypeEnum.BusinessSale)
                {
                    var (companyClientsForRemove, companyClientsForUpdate, companyClientsForInsert) = SeparateCompanyClient(ratePlanOld.RatePlanCompanyClientList.ToList(), ratePlanCompanyClientList);
                    var (comissionForRemove, comissionForUpdate, comissionForInsert) = SeparateComission(ratePlanOld.RatePlanCommissionList.ToList(), ratePlanCommissionList);
                    ValidationRatePlanCompanyClient(companyClientOld, companyClientsForRemove, companyClientsForUpdate, companyClientsForInsert);
                    ValidationRatePlanComission(comissionOld, comissionForRemove, comissionForUpdate, comissionForInsert);
                }
                Context.SaveChanges();
            }
        }
        #region Insert Update Delete       
        private void ValidationRatePlanRoomType(ICollection<RatePlanRoomType> roomTypesOld, List<RatePlanRoomType> roomTypesForRemove, List<RatePlanRoomType> roomTypesForUpdate, List<RatePlanRoomType> roomTypesForInsert)
        {
            if (roomTypesForRemove != null && roomTypesForRemove.Count() > 0)
                foreach (var roomtype in roomTypesForRemove)
                    Context.Remove(roomtype);


            if (roomTypesForUpdate != null && roomTypesForUpdate.Count() > 0)
            {
                foreach (var roomtype in roomTypesForUpdate)
                {
                    var roomtypeOld = roomTypesOld.Where(x => x.Id == roomtype.Id).FirstOrDefault();
                    Context.Entry(roomtypeOld).State = EntityState.Modified;
                    Context.Entry(roomtypeOld).CurrentValues.SetValues(roomtype);
                }
            }

            if (roomTypesForInsert != null && roomTypesForInsert.Count() > 0)
            {
                Context.RatePlanRoomTypes.AddRange(roomTypesForInsert);

            }
            Context.SaveChanges();
        }
        private void ValidationRatePlanCompanyClient(ICollection<RatePlanCompanyClient> companyClientOld, List<RatePlanCompanyClient> companyClientsForRemove, List<RatePlanCompanyClient> companyClientsForUpdate, List<RatePlanCompanyClient> companyClientForInsert)
        {
            if (companyClientsForRemove != null && companyClientsForRemove.Count() > 0)
                foreach (var roomtype in companyClientsForRemove)
                    Context.Remove(roomtype);


            if (companyClientsForUpdate != null && companyClientsForUpdate.Count() > 0)
            {
                foreach (var roomtype in companyClientsForUpdate)
                {
                    var roomtypeOld = companyClientOld.Where(x => x.Id == roomtype.Id).FirstOrDefault();
                    Context.Entry(roomtypeOld).State = EntityState.Modified;
                    Context.Entry(roomtypeOld).CurrentValues.SetValues(roomtype);
                }
            }

            if (companyClientForInsert != null && companyClientForInsert.Count() > 0)
            {
                Context.AddRange(companyClientForInsert);

            }
            Context.SaveChanges();
        }
        private void ValidationRatePlanComission(ICollection<RatePlanCommission> comissionOld, List<RatePlanCommission> comissionForRemove, List<RatePlanCommission> comissionForUpdate, List<RatePlanCommission> comissionForInsert)
        {
            if (comissionForRemove != null && comissionForRemove.Count() > 0)
                foreach (var roomtype in comissionForRemove)
                    Context.Remove(roomtype);


            if (comissionForUpdate != null && comissionForUpdate.Count() > 0)
            {
                foreach (var roomtype in comissionForUpdate)
                {
                    var roomtypeOld = comissionOld.Where(x => x.Id == roomtype.Id).FirstOrDefault();
                    Context.Entry(roomtypeOld).State = EntityState.Modified;
                    Context.Entry(roomtypeOld).CurrentValues.SetValues(roomtype);
                }
            }

            if (comissionForInsert != null && comissionForInsert.Count() > 0)
            {
                Context.AddRange(comissionForInsert);

            }
            Context.SaveChanges();
        }

        #endregion

        private (List<RatePlanRoomType> roomTypesForRemove, List<RatePlanRoomType> roomTypesForUpdate, List<RatePlanRoomType> roomTypesForInsert)
           SeparateRoomType(List<RatePlanRoomType> roomTypesExistings, List<RatePlanRoomType> roomTypesNews)
        {
            var roomTypesExistingsIds = roomTypesExistings.Select(x => x.Id);
            var roomTypesNewsIds = roomTypesNews.Select(x => x.Id);

            var roomTypesForRemove = roomTypesExistings.Where(x => !roomTypesNewsIds.Contains(x.Id)).ToList();
            var roomTypeForUpdate = roomTypesNews.Where(x => roomTypesExistingsIds.Contains(x.Id)).ToList();
            var roomTypesForInsert = roomTypesNews.Where(x => !roomTypesExistingsIds.Contains(x.Id)).ToList();


            return (roomTypesForRemove, roomTypeForUpdate, roomTypesForInsert);
        }

        private (List<RatePlanCompanyClient> companyClientsForRemove, List<RatePlanCompanyClient> companyClientsForUpdate, List<RatePlanCompanyClient> companyClientsForInsert)
   SeparateCompanyClient(List<RatePlanCompanyClient> companyClientsExistings, List<RatePlanCompanyClient> companyClientsNews)
        {
            var companyClientsExistingsIds = companyClientsExistings.Select(x => x.Id);
            var companyClientsNewsIds = companyClientsNews.Select(x => x.Id);

            var companyClientsForRemove = companyClientsExistings.Where(x => !companyClientsNewsIds.Contains(x.Id)).ToList();
            var companyClientsForUpdate = companyClientsNews.Where(x => companyClientsExistingsIds.Contains(x.Id)).ToList();
            var companyClientsForInsert = companyClientsNews.Where(x => !companyClientsExistingsIds.Contains(x.Id)).ToList();


            return (companyClientsForRemove, companyClientsForUpdate, companyClientsForInsert);
        }


        public void ToggleAndSaveIsActive(Guid id)
        {
            RatePlan RatePlan = base.Get(new DefaultGuidRequestDto(id));
            RatePlan.IsActive = !RatePlan.IsActive;
            Context.SaveChanges();
        }



        private (List<RatePlanCommission> comissionForRemove, List<RatePlanCommission> comissionForUpdate, List<RatePlanCommission> comissionForInsert)
           SeparateComission(List<RatePlanCommission> comissionExistings, List<RatePlanCommission> comissionNews)
        {
            var comissionExistingsIds = comissionExistings.Select(x => x.Id);
            var comissionNewsIds = comissionNews.Select(x => x.Id);

            var comissionForRemove = comissionExistings.Where(x => !comissionNewsIds.Contains(x.Id)).ToList();
            var comissionForUpdate = comissionNews.Where(x => comissionExistingsIds.Contains(x.Id)).ToList();
            var comissionForInsert = comissionNews.Where(x => !comissionExistingsIds.Contains(x.Id)).ToList();


            return (comissionForRemove, comissionForUpdate, comissionForInsert);
        }

        public void Delete(Guid id)
        {
            var ratePlan = base.Get(new DefaultGuidRequestDto(id));
            base.Delete(ratePlan);

            Context.SaveChanges();
        }
    }
}
