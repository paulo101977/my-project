﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Thex.Common;
using Thex.Common.Enumerations;
using Thex.Domain.Entities;
using Thex.Domain.Interfaces.Repositories;
using Thex.Infra.ReadInterfaces;
using Tnf.EntityFrameworkCore.Repositories;
using Tnf.EntityFrameworkCore;
using Thex.Infra.Context;
using Tnf.Notifications;
using Thex.Dto;

namespace Thex.EntityFrameworkCore.Repositories
{
    public class PropertyPremiseHeaderRepository : EfCoreRepositoryBase<ThexContext, PropertyPremiseHeader>, IPropertyPremiseHeaderRepository
    {
        private readonly INotificationHandler Notification;

        public PropertyPremiseHeaderRepository(IDbContextProvider<ThexContext> dbContextProvider, INotificationHandler notificationHandler) : base(dbContextProvider)
        {
            Notification = notificationHandler;
        }

        public void ToggleActive(Guid id)
        {
            PropertyPremiseHeader propertyPremiseHeader = base.Get(new DefaultGuidRequestDto(id));
            propertyPremiseHeader.IsActive = !propertyPremiseHeader.IsActive;
            Context.SaveChanges();
        }

        public void UpdatePropertyPremiseRange(ICollection<PropertyPremise> propertyPremiseList)
        {
            Context.PropertyPremises.UpdateRange(propertyPremiseList);

            Context.SaveChanges();
        }

        public void AddPropertyPremiseRange(ICollection<PropertyPremise> propertyPremiseList)
        {
            Context.PropertyPremises.AddRange(propertyPremiseList);

            Context.SaveChanges();
        }
    }
}
