﻿using System;
using System.Collections.Generic;
using System.Text;
using Thex.Domain.Entities;
using Thex.Domain.Interfaces.Repositories;
using Tnf.EntityFrameworkCore.Repositories;
using Tnf.EntityFrameworkCore;
using Thex.Infra.Context;
using Thex.Dto;

namespace Thex.EntityFrameworkCore.Repositories
{
    public class RoomBlockingRepository : EfCoreRepositoryBase<ThexContext, RoomBlocking>, IRoomBlockingRepository
    {
        public RoomBlockingRepository(IDbContextProvider<ThexContext> dbContextProvider) : base(dbContextProvider)
        {
        }

        public void BlockingRoom(RoomBlocking roomBlocking)
        {
            base.Insert(roomBlocking);
            Context.SaveChanges();

        }

        public void UnlockingRoom(Guid id)
        {
            var obj = base.Get(new DefaultGuidRequestDto(id));
            base.Delete(obj);
            Context.SaveChanges();
        }

        public void UnlockingRoom(List<RoomBlocking> roomBlockingList)
        {
            Context.RemoveRange(roomBlockingList);
            Context.SaveChanges();
        }
    }
}
