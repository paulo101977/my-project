﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Thex.Domain.Entities;
using Thex.Domain.Interfaces.Repositories;
using Tnf.EntityFrameworkCore.Repositories;
using Tnf.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Thex.Infra.ReadInterfaces;
using Thex.Infra.Context;
using Thex.Dto;

namespace Thex.EntityFrameworkCore.Repositories
{
    public class PropertyMealPlanTypeRepository : EfCoreRepositoryBase<ThexContext, PropertyMealPlanType>, IPropertyMealPlanTypeRepository
    {

        public PropertyMealPlanTypeRepository(IDbContextProvider<ThexContext> dbContextProvider) : base(dbContextProvider)
        {
            
        }

        public PropertyMealPlanType GetById(Guid id)
        {
            return Context.PropertyMealPlanTypes.Find(id);
        }

        public void PropertyMealPlanTypeUpdate(PropertyMealPlanType propertyMealPlanType)
        {
            var propertyMealPlanTypeOld = Context
                .PropertyMealPlanTypes
                
                .Where(x => x.Id == propertyMealPlanType.Id).SingleOrDefault();


            if (propertyMealPlanTypeOld != null)
            {
                Context.Entry(propertyMealPlanTypeOld).State = EntityState.Modified;
                Context.Entry(propertyMealPlanTypeOld).CurrentValues.SetValues(propertyMealPlanType);
                Context.SaveChanges();
            }
        }

        public void ToggleAndSaveIsActive(PropertyMealPlanType propertyMealPlanType)
        {
            propertyMealPlanType.IsActive = !propertyMealPlanType.IsActive;
            Context.SaveChanges();
        }
    }
}
