﻿// //  <copyright file="BillingAccountRepository.cs" company="TOTVS">
// //  Copyright (c) TOTVS. All rights reserved.
// //  </copyright>
namespace Thex.EntityFrameworkCore.Repositories
{
    using EFCore.BulkExtensions;
    using Microsoft.EntityFrameworkCore;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Thex.Domain.Entities;
    using Thex.Domain.Interfaces.Repositories;
    using Thex.Infra.Context;
    using Thex.Kernel;
    using Tnf.EntityFrameworkCore;
    using Tnf.EntityFrameworkCore.Repositories;

    public class PropertyBaseRateRepository : EfCoreRepositoryBase<ThexContext, PropertyBaseRate>, IPropertyBaseRateRepository
    {
        private readonly IApplicationUser _applicationUser;

        public PropertyBaseRateRepository(IDbContextProvider<ThexContext> dbContextProvider, IApplicationUser applicationUser)
            : base(dbContextProvider)
        {
            _applicationUser = applicationUser;
        }

        public void CreateAndUpdate(List<PropertyBaseRate> propertyBaseRateToAddList, List<PropertyBaseRate> propertyBaseRateToUpdateList)
        {
            if (propertyBaseRateToUpdateList.Count > 0)
                Context.BulkUpdate(propertyBaseRateToUpdateList);

            if (propertyBaseRateToAddList.Count > 0)
                Context.BulkInsert(propertyBaseRateToAddList);
        }

        public async Task<IQueryable<PropertyBaseRate>> GetByFiltersAsync(int propertyId, List<int> roomTypeIdList, Guid currencyId, DateTime startDate, DateTime finalDate, int mealPlanTypeDefaultId)
        {
            var propertyBaseRateList = Context.PropertyBaseRates
               .Where(p => p.PropertyId == propertyId &&
                           roomTypeIdList.Contains(p.RoomTypeId) &&
                           p.CurrencyId == currencyId &&
                           p.Date >= startDate &&
                           p.Date <= finalDate &&
                           p.TenantId == _applicationUser.TenantId)
               .IgnoreQueryFilters();

            await propertyBaseRateList.ForEachAsync(x => x.MealPlanTypeDefault = mealPlanTypeDefaultId);

            return propertyBaseRateList;
        }

        public void UpdatePropertyBaseRateMealPlanRange(IQueryable<PropertyBaseRate> propertyBaseRateList)
        {
            if(propertyBaseRateList != null && propertyBaseRateList.Any())
                Context.BulkUpdate(propertyBaseRateList.ToList());

            Context.SaveChanges();
        }

        public void InsertHeaderHistoryRangeAndSaveChanges(ICollection<PropertyBaseRateHeaderHistory> baseRateHeaderHistoryList)
        {
            if (baseRateHeaderHistoryList.Count > 0)
                Context.PropertyBaseRateHeaderHistories.AddRange(baseRateHeaderHistoryList.ToList());

            Context.SaveChanges();
        }
    }
}