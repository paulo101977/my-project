﻿using EFCore.BulkExtensions;
using System;
using System.Collections.Generic;
using Thex.Domain.Entities;
using Thex.Domain.Interfaces.Repositories;
using Thex.Dto;
using Thex.Infra.Context;
using Thex.Kernel;
using Tnf.EntityFrameworkCore;
using Tnf.EntityFrameworkCore.Repositories;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Thex.Dto.PropertyCurrencyExchange;

namespace Thex.Infra.Repositories
{
    public class PropertyCurrencyExchangeRepository : EfCoreRepositoryBase<ThexContext, PropertyCurrencyExchange>, IPropertyCurrencyExchangeRepository
    {
        private readonly IApplicationUser _applicationUser;

        public PropertyCurrencyExchangeRepository(IDbContextProvider<ThexContext> dbContextProvider, IApplicationUser applicationUser) : base(dbContextProvider)
        {
            _applicationUser = applicationUser;
        }

        public void DeleteAllAndSaveChanges()
        {
            Context.PropertyCurrencyExchanges
             .Where(p => p.PropertyId == int.Parse(_applicationUser.PropertyId) && !p.IsDeleted)
             .IgnoreQueryFilters()
             .BatchUpdate(new PropertyCurrencyExchange
             {
                 IsDeleted = true,
                 DeletionTime = DateTime.UtcNow.ToZonedDateTimeLoggedUser(),
                 DeleterUserId = _applicationUser.UserUid
             });
        }

        public void InsertAndSaveChanges(List<PropertyCurrencyExchangeDto> propertyCurrencyExchanges)
        {
            var propertyCurrencyExchangeList = new List<PropertyCurrencyExchange>();

            foreach(var propertyCurrencyExchange in propertyCurrencyExchanges)
            {
                propertyCurrencyExchangeList.Add(new PropertyCurrencyExchange()
                {                 
                    Id = Guid.NewGuid(),
                    CurrencyId = propertyCurrencyExchange.CurrencyId,
                    ExchangeRate = propertyCurrencyExchange.ExchangeRate,
                    PropertyCurrencyExchangeDate = propertyCurrencyExchange.PropertyCurrencyExchangeDate,
                    PropertyId = int.Parse(_applicationUser.PropertyId),
                    CreationTime = DateTime.UtcNow.ToZonedDateTimeLoggedUser(),
                    LastModificationTime = DateTime.UtcNow.ToZonedDateTimeLoggedUser(),
                    TenantId = _applicationUser.TenantId,
                    CreatorUserId = _applicationUser.UserUid
                });                    
            }
            
            Context.BulkInsert(propertyCurrencyExchangeList);

            Context.SaveChanges();
        }

        public void UpdateAndSaveChanges(PropertyCurrencyExchangeDto propertyCurrencyExchange)
        {
            Context.PropertyCurrencyExchanges
              .Where(p =>p.Id == propertyCurrencyExchange.Id)
              .IgnoreQueryFilters()
              .BatchUpdate(new PropertyCurrencyExchange {
                  ExchangeRate = propertyCurrencyExchange.ExchangeRate,
                  LastModificationTime = DateTime.UtcNow.ToZonedDateTimeLoggedUser(),
                  LastModifierUserId = _applicationUser.UserUid
              });
        }    

        public void UpdateCurrenciesAndSaveChanges(List<QuotationCurrentDto> quotationCurrencyDtoList)
        {
            var propertyCurrencyExchangeList = new List<PropertyCurrencyExchange>();

            foreach (var propertyCurrencyExchange in quotationCurrencyDtoList)
            {
                propertyCurrencyExchangeList.Add(new PropertyCurrencyExchange()
                {
                    Id = propertyCurrencyExchange.Id,
                    CurrencyId = propertyCurrencyExchange.CurrencyId,
                    ExchangeRate = propertyCurrencyExchange.ExchangeRate.Value,
                    PropertyCurrencyExchangeDate = propertyCurrencyExchange.CurrentDate,
                    PropertyId = int.Parse(_applicationUser.PropertyId),
                    CreationTime = propertyCurrencyExchange.CreationTime,
                    LastModificationTime = DateTime.UtcNow.ToZonedDateTimeLoggedUser(),
                    TenantId = _applicationUser.TenantId,
                    CreatorUserId = propertyCurrencyExchange.CreatorUserId,
                    LastModifierUserId = _applicationUser.UserUid
                });
            }

            Context.BulkUpdate(propertyCurrencyExchangeList);

            Context.SaveChanges();
        }
    }
}
