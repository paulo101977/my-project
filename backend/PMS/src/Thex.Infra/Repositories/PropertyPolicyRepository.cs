﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Thex.Common;
using Thex.Common.Enumerations;
using Thex.Domain.Entities;
using Thex.Domain.Interfaces.Repositories;
using Thex.Infra.ReadInterfaces;
using Tnf.EntityFrameworkCore.Repositories;
using Tnf.EntityFrameworkCore;
using Thex.Infra.Context;
using Tnf.Notifications;
using Thex.Dto;

namespace Thex.EntityFrameworkCore.Repositories
{
    public class PropertyPolicyRepository : EfCoreRepositoryBase<ThexContext, PropertyPolicy>, IPropertyPolicyRepository
    {
        private readonly INotificationHandler Notification;

        public PropertyPolicyRepository(IDbContextProvider<ThexContext> dbContextProvider, INotificationHandler notificationHandler) : base(dbContextProvider)
        {
            Notification = notificationHandler;
        }

        public void PropertyPolicyCreate(PropertyPolicy propertyPolicy)
        {
            Context.PropertyPolicies.Add(propertyPolicy);
            Context.SaveChanges();
        }

        public void Delete(Guid id)
        {
            try
            {
                var obj = base.Get(new DefaultGuidRequestDto(id));
                base.Delete(obj);
                Context.SaveChanges();
            }
            catch (Exception)
            {
                Context.ChangeTracker.Entries()
                   .Where(e => e.Entity != null).ToList()
                   .ForEach(e => e.State = EntityState.Detached);

                Notification.Raise(Notification.DefaultBuilder
                        .WithMessage(AppConsts.LocalizationSourceName, BillingCategoryEnum.Error.BillingCategoryForeignKeyError)
                        .WithDetailedMessage(AppConsts.LocalizationSourceName, BillingCategoryEnum.Error.BillingCategoryForeignKeyError)
                        .Build());

            }
        }

        public void PropertyPolicyDelete(Guid id)
        {
            try
            {
                var obj = base.Get(new DefaultGuidRequestDto(id));
                base.Delete(obj);
                Context.SaveChanges();
            }
            catch (Exception)
            {
                Context.ChangeTracker.Entries()
                   .Where(e => e.Entity != null).ToList()
                   .ForEach(e => e.State = EntityState.Detached);

                Notification.Raise(Notification.DefaultBuilder
                        .WithMessage(AppConsts.LocalizationSourceName, BillingCategoryEnum.Error.BillingCategoryForeignKeyError)
                        .WithDetailedMessage(AppConsts.LocalizationSourceName, BillingCategoryEnum.Error.BillingCategoryForeignKeyError)
                        .Build());

            }
        }

        public void PropertyPolicyUpdate(PropertyPolicy propertyPolicy)
        {
            var propertyPolicyOld = Context
                .PropertyPolicies
                
                .Where(x => x.Id == propertyPolicy.Id).SingleOrDefault();

            if (propertyPolicyOld != null)
            {
                Context.Entry(propertyPolicyOld).State = EntityState.Modified;
                Context.Entry(propertyPolicyOld).CurrentValues.SetValues(propertyPolicy);
                Context.SaveChanges();
            }
        }

        public void ToggleAndSaveIsActive(Guid id)
        {
            PropertyPolicy propertyPolicy = base.Get(new DefaultGuidRequestDto(id));
            propertyPolicy.IsActive = !propertyPolicy.IsActive;
            Context.SaveChanges();
        }
    }
}
