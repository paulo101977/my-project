﻿// //  <copyright file="BillingAccountRepository.cs" company="TOTVS">
// //  Copyright (c) TOTVS. All rights reserved.
// //  </copyright>
namespace Thex.EntityFrameworkCore.Repositories
{
    using System.Linq;
    using Thex.Domain.Entities;
    using Thex.Domain.Interfaces.Repositories;

    using Tnf.EntityFrameworkCore.Repositories;
    using Tnf.EntityFrameworkCore;
    using Thex.Infra.Context;
    using Microsoft.EntityFrameworkCore;
    using System;

    public class BillingInvoicePropertyRepository : EfCoreRepositoryBase<ThexContext, BillingInvoiceProperty>, IBillingInvoicePropertyRepository
    {
        public BillingInvoicePropertyRepository(IDbContextProvider<ThexContext> dbContextProvider)
            : base(dbContextProvider)
        {
        }

        public void IncrementLastNumberAndUpdate(BillingInvoiceProperty billingInvoiceProperty)
        {
            billingInvoiceProperty.IncrementLastNumber();

            Update(billingInvoiceProperty);
        }

        public void UpdateBillingInvoiceProperty(BillingInvoiceProperty billingInvoiceProperty)
        {
            Update(billingInvoiceProperty);
        }

        public void SaveChanges()
        {
            Context.SaveChanges();
        }

        public bool CompanyHasSerieByPropertyId(int propertyId, string billingInvoicePropertySeries)
        {
            var companyId = Context.Properties.Find(propertyId).CompanyId;
            return (from billingInvoiceProperty in Context.BillingInvoiceProperties.IgnoreQueryFilters()
                    join property in Context.Properties.IgnoreQueryFilters() on billingInvoiceProperty.PropertyId equals property.Id
                    join company in Context.Companies.IgnoreQueryFilters() on property.CompanyId equals company.Id
                    where property.CompanyId == companyId && billingInvoiceProperty.BillingInvoicePropertySeries == billingInvoicePropertySeries
                    select billingInvoiceProperty).Any();
        }

        public BillingInvoiceProperty GetChildByParentId(Guid parentId)
        {
            return (from childBillingInvoiceProperty in Context.BillingInvoiceProperties
                    where childBillingInvoiceProperty.BillingInvoicePropertyReferenceId == parentId
                    select childBillingInvoiceProperty).FirstOrDefault();
        }
    }
}