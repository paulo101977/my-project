﻿using System;
using System.Collections.Generic;
using System.Text;
using Thex.Domain.Entities;
using Thex.Domain.Interfaces.Repositories;
using Tnf.EntityFrameworkCore.Repositories;
using Tnf.EntityFrameworkCore;
using Thex.Infra.Context;

namespace Thex.EntityFrameworkCore.Repositories
{
    public class RatePlanCommissionRepository : EfCoreRepositoryBase<ThexContext, RatePlanCommission>, IRatePlanCommissionRepository
    {
        public RatePlanCommissionRepository(IDbContextProvider<ThexContext> dbContextProvider) : base(dbContextProvider)
        {
        }

        public void RatePlanCommissionCreate(List<RatePlanCommission> ratePlanCommissions)
        {
            Context.RatePlanCommissions.AddRange(ratePlanCommissions);
            Context.SaveChanges();
        }
    }
}
