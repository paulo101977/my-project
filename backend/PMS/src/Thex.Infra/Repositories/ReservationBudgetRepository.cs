﻿//  <copyright file="ReservationBudgetRepository.cs" company="TOTVS">
//  Copyright (c) TOTVS. All rights reserved.
//  </copyright>
namespace Thex.EntityFrameworkCore.Repositories
{
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;

    using Thex.Domain.Entities;

    using Microsoft.EntityFrameworkCore;

    using Tnf.EntityFrameworkCore.Repositories;
    using Tnf.EntityFrameworkCore;
    using Thex.Domain.Interfaces.Repositories;
    using Thex.Common.Enumerations;
    using System;
    using Thex.Infra.ReadInterfaces;
    using Thex.Infra.Context;
    using EFCore.BulkExtensions;

    public class ReservationBudgetRepository : EfCoreRepositoryBase<ThexContext, ReservationBudget>, IReservationBudgetRepository
    {


        public ReservationBudgetRepository(IDbContextProvider<ThexContext> dbContextProvider)
            : base(dbContextProvider)
        {

        }

        public void AddRange(List<ReservationBudget> reservationBudget)
        {
            Context.AddRange(reservationBudget);

            Context.SaveChanges();
        }

        public void RemoveAndAddRange(ReservationItem reservationItem, List<ReservationBudget> reservationBudget, DateTime? systemDate = null)
        {
            RemoveAndAddRangeByReservationItemId(reservationItem.Id, reservationBudget, systemDate);
        }

        public void RemoveAndAddRange(long reservationItemId, List<ReservationBudget> reservationBudget, DateTime? systemDate = null)
        {
            RemoveAndAddRangeByReservationItemId(reservationItemId, reservationBudget, systemDate);
        }

        public void RemoveAndAddRangeByReservationItemId(long reservationItemId, List<ReservationBudget> reservationBudget, DateTime? systemDate)
        {
            var reservationBudgetListToRemoveQuery = Context
                .ReservationBudgets
                .Where(exp => exp.ReservationItemId == reservationItemId);
            
            if (systemDate != null)
            {
                reservationBudgetListToRemoveQuery = reservationBudgetListToRemoveQuery.Where(x => x.BudgetDay >= systemDate);
            }
            
            var reservationBudgetListToRemove = reservationBudgetListToRemoveQuery.ToList();

            if (reservationBudgetListToRemove != null && reservationBudgetListToRemove.Count() > 0)
            {
                Context.RemoveRange(reservationBudgetListToRemove);

                Context.SaveChanges();
            }

            Context.AddRange(reservationBudget);

            Context.SaveChanges();
        }

        public void UpdateRange(List<ReservationBudget> reservationBudget)
        {
            Context.UpdateRange(reservationBudget);

            Context.SaveChanges();
        }

        public void AddReservationBudgetRange(ICollection<ReservationBudget> reservationBudgetList)
        {
            Context.BulkInsert(reservationBudgetList.ToList());
            Context.SaveChanges();
        }

        public void RemoveAll(ReservationItem reservationItem, List<ReservationBudget> reservationBudget, bool completeChanges, DateTime? systemDate = null)
        {
            var reservationBudgetQueryToRemove = Context
                .ReservationBudgets

                .Where(exp => exp.ReservationItemId == reservationItem.Id);

            if (systemDate != null)
            {
                reservationBudgetQueryToRemove = reservationBudgetQueryToRemove.Where(x => x.BudgetDay.Date > systemDate.Value.Date);
            }

            var reservationBudgetListToRemove = reservationBudgetQueryToRemove
                .ToList();

            if (reservationBudgetListToRemove != null && reservationBudgetListToRemove.Count() > 0)
            {
                Context.RemoveRange(reservationBudgetListToRemove);

                if (completeChanges)
                {
                    Context.SaveChanges();
                }
            }
        }

        public void RemoveRange(DateTime systemDate, long reservationItemId, bool isEarlyDayUse)
        {
            if (Context.ReservationBudgets
                    .Count(exp => exp.ReservationItemId == reservationItemId && exp.BudgetDay.Date >= systemDate) < 2)
                return;

            var reservationBudgetToRemove = new List<ReservationBudget>();

            if (isEarlyDayUse)
            {
                reservationBudgetToRemove = Context
                                   .ReservationBudgets
                                   .Where(exp => exp.ReservationItemId == reservationItemId && exp.BudgetDay.Date > systemDate)
                                   .ToList();
            }
            else
            {
                reservationBudgetToRemove = Context
                   .ReservationBudgets
                   .Where(exp => exp.ReservationItemId == reservationItemId && exp.BudgetDay.Date >= systemDate)
                   .ToList();
            }


            Context.RemoveRange(reservationBudgetToRemove);
            Context.SaveChanges();
        }

        public void BulkInsert(List<ReservationBudget> reservationBudgetList)
        {
            if (reservationBudgetList.Count > 0)
                Context.BulkInsert(reservationBudgetList);
        }

        public ReservationBudget GetLastReservationBudget(ReservationItem reservationItem)
        {
            var lastReservationBudget = Context
                 .ReservationBudgets
                 .Where(exp => exp.ReservationItemId == reservationItem.Id)
                 .OrderByDescending(e => e.Id).FirstOrDefault();

            return lastReservationBudget;
        }

        public void DeleteRange(List<long> idList)
        {
            if (idList != null && idList.Count > 0)
            {
                Context.ReservationBudgets
                    .Where(rb => idList.Contains(rb.Id))
                    .IgnoreQueryFilters()
                    .BatchDelete();

                Context.SaveChanges();
            }
        }

        public void RemoveRangeNoShow(DateTime? systemDate, long reservationItemId)
        {
            if (!systemDate.HasValue) return;

            var reservationBudgetToRemove = Context
                .ReservationBudgets
                .Where(exp => exp.ReservationItemId == reservationItemId && exp.BudgetDay.Date < systemDate.Value.Date)
                .ToList();

            Context.RemoveRange(reservationBudgetToRemove);
            Context.SaveChanges();
        }       
    }
}
