﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Thex.Common;
using Thex.Common.Enumerations;
using Thex.Domain.Entities;
using Thex.Domain.Interfaces.Repositories;
using Thex.Infra.ReadInterfaces;
using Tnf.EntityFrameworkCore.Repositories;
using Tnf.EntityFrameworkCore;
using Thex.Infra.Context;
using Tnf.Notifications;
using Thex.Dto;

namespace Thex.EntityFrameworkCore.Repositories
{
    public class BillingItemRepository : EfCoreRepositoryBase<ThexContext, BillingItem>, IBillingItemRepository
    {
        private readonly INotificationHandler Notification;

        public BillingItemRepository(IDbContextProvider<ThexContext> dbContextProvider,
            INotificationHandler notificationHandler) 
            : base(dbContextProvider)
        {
            Notification = notificationHandler;
        }

        public new void Update(BillingItem billingItem)
        {
            var billingItemOld = Context
                .BillingItems
                
                .Where(x => x.Id == billingItem.Id).SingleOrDefault();

            if(billingItemOld != null)
            {
                Context.Entry(billingItemOld).State = EntityState.Modified;
                Context.Entry(billingItemOld).CurrentValues.SetValues(billingItem);
                Context.SaveChanges();
            }
        }

        public void ToggleAndSaveIsActive(int id)
        {
            BillingItem billingItem = base.Get(new DefaultIntRequestDto(id));
            billingItem.IsActive = !billingItem.IsActive;
            Context.SaveChanges();
        }

        public void ToggleAndSaveIsActive(BillingItem billingItem)
        {
            billingItem.IsActive = !billingItem.IsActive;
            Context.SaveChanges();
        }

        public void DeleteBillingItemService(int id)
        {
            DeleteCommom(id);
        }

        public BillingItem UpdatePropertyPaymentType(BillingItem billingItem)
        {
            var existingBillingItem = Context.BillingItems
                            
                            .Include(e => e.BillingItemPaymentConditionList)
                            .Where(e => e.Id == billingItem.Id)
                            .FirstOrDefault();

            if (existingBillingItem != null)
            {
                Context.Entry(existingBillingItem).State = EntityState.Modified;
                Context.Entry(existingBillingItem).CurrentValues.SetValues(billingItem);

                RemoveConditions(billingItem, existingBillingItem);

                Context.SaveChanges();

                foreach (var detail in billingItem.BillingItemPaymentConditionList)
                    InsertOrUpdateConditionsFromPropertyPaymentType(billingItem, existingBillingItem, detail);
            }
            else
            {
                Context.BillingItems.Add(billingItem);
            }

            Context.SaveChanges();

            return existingBillingItem;
        }

        private void InsertOrUpdateConditionsFromPropertyPaymentType(BillingItem existingBillingItem, BillingItem billingItem, BillingItemPaymentCondition detail)
        {
            var existingDetail = billingItem.BillingItemPaymentConditionList.Where(e => e.Id == detail.Id && e.Id != Guid.Empty).FirstOrDefault();

            if (existingDetail != null)
            {
                detail.BillingItemId = existingBillingItem.Id;

                Context.Entry(existingDetail).State = EntityState.Modified;
                Context.Entry(existingDetail).CurrentValues.SetValues(detail);
            }
            else
            {
                billingItem.BillingItemPaymentConditionList.Add(detail);
            }
        }

        private void RemoveConditions(BillingItem billingItem, BillingItem existingBillingItem)
        {
            List<Guid> conditionsIdsExceptList = GetDetailsIds(billingItem);

            RemoveConditionsExceptListOfIds(existingBillingItem, conditionsIdsExceptList);
        }

        private void RemoveConditionsExceptListOfIds(BillingItem existingBillingItem, List<Guid> conditionsIdsExceptList)
        {
            var conditionsToExclude = existingBillingItem.BillingItemPaymentConditionList.Where(exp => !conditionsIdsExceptList.Contains(exp.Id)).ToList();
            if (conditionsToExclude.Count > 0)
                Context.RemoveRange(conditionsToExclude);
        }

        private List<Guid> GetDetailsIds(BillingItem billingItem)
        {
            return billingItem.BillingItemPaymentConditionList.Select(r => r.Id).ToList();
        }

        public void AddRange(List<BillingItem> billingItems)
        {
            Context.BillingItems.AddRange(billingItems);
            Context.SaveChanges();
        }

        public void ToggleActivationPaymentType(BillingItem billingItem, bool activate)
        {
            billingItem.IsActive = activate;
            base.Update(billingItem);
            Context.SaveChanges();
        }

        public void DeletePaymentType(int id)
        {
            DeleteCommom(id);
        }
        private void DeleteCommom(int id)
        {
            try
            {
                if (Context.RatePlanCommissions.Where(x => x.BillingItemId == id && !x.IsDeleted).Any())
                    BillingItemForeignKeyError();

                if (Context.BillingAccountItems.Where(x => x.BillingItemId == id && !x.IsDeleted).Any())
                    BillingItemForeignKeyError();

                if (Context.BillingItemPaymentConditions.Where(x => x.BillingItemId == id && !x.IsDeleted).Any())
                    BillingItemForeignKeyError();

                if (Notification.HasNotification())
                    return;

                var obj = base.Get(new DefaultIntRequestDto(id));
                base.Delete(obj);
                Context.SaveChanges();

            }
            catch
            {

                Context.ChangeTracker.Entries()
                  .Where(e => e.Entity != null).ToList()
                  .ForEach(e => e.State = EntityState.Detached);

                Notification.Raise(Notification.DefaultBuilder
                        .WithMessage(AppConsts.LocalizationSourceName, BillingItemEnum.Error.BillingItemForeignKeyError)
                        .WithDetailedMessage(AppConsts.LocalizationSourceName, BillingItemEnum.Error.BillingItemForeignKeyError)
                        .Build());
            }
        }
        private void BillingItemForeignKeyError()
        {
            Notification.Raise(Notification.DefaultBuilder
                                  .WithMessage(AppConsts.LocalizationSourceName, BillingItemEnum.Error.BillingItemForeignKeyError)
                                  .WithDetailedMessage(AppConsts.LocalizationSourceName, BillingItemEnum.Error.BillingItemForeignKeyError)
                                  .Build());
        }

        public void Delete(int id)
        {
            var obj = base.Get(new DefaultIntRequestDto(id));
            base.Delete(obj);
            Context.SaveChanges();
        }
    }
}
