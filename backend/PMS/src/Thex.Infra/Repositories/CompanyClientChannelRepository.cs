﻿using System;
using System.Collections.Generic;
using System.Linq;
using Thex.Domain.Entities;
using Thex.Domain.Interfaces.Repositories;
using Thex.Infra.Context;
using Tnf.EntityFrameworkCore;
using Tnf.EntityFrameworkCore.Repositories;

namespace Thex.Infra.Repositories
{
    public class CompanyClientChannelRepository : EfCoreRepositoryBase<ThexContext, CompanyClientChannel>, ICompanyClientChannelRepository
    {
        public CompanyClientChannelRepository(IDbContextProvider<ThexContext> dbContextProvider) : base(dbContextProvider)
        {

        }

        public void ToogleIsActive(Guid id)
        {
            var companyClientChannel = Context.CompanyClientChannels.FirstOrDefault(x=>x.Id == id);
            if(companyClientChannel != null)
            {
                companyClientChannel.IsActive = !companyClientChannel.IsActive;
                Context.SaveChanges();
            }
        }

        public void UpdateRangeAndSaveChanges(List<CompanyClientChannel> companyClientChannelList)
        {
            Context.CompanyClientChannels.UpdateRange(companyClientChannelList);

            Context.SaveChanges();
        }

        public void InsertRangeAndSavechanges(List<CompanyClientChannel> companyClientChannelList)
        {
            Context.CompanyClientChannels.AddRange(companyClientChannelList);

            Context.SaveChanges();
        }

        public void RemoveRangeAndSavechanges(List<CompanyClientChannel> companyClientChannelList)
        {
            Context.CompanyClientChannels.RemoveRange(companyClientChannelList);

            Context.SaveChanges();
        }
    }
}
