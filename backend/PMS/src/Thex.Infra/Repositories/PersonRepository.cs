﻿// //  <copyright file="PersonRepository.cs" company="TOTVS">
// //  Copyright (c) TOTVS. All rights reserved.
// //  </copyright>
namespace Thex.EntityFrameworkCore.Repositories
{
    using System;
    using System.Collections.Generic;

    using Thex.Domain.Entities;
    using Thex.Domain.Interfaces.Repositories;

    using Tnf.EntityFrameworkCore.Repositories;
    using Tnf.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore;
    using System.Linq;
    using Thex.Infra.Context;

    public class PersonRepository : EfCoreRepositoryBase<ThexContext, Person>, IPersonRepository
    {
        public PersonRepository(IDbContextProvider<ThexContext> dbContextProvider)
            : base(dbContextProvider)
        {
        }

        public void AddRange(IList<Person> personList)
        {
            Context.Persons.AddRange(personList);
            Context.SaveChanges();
        }

        public void UpdateWithDocumentLocationContactInformation(Person person)
        {
            var existingPerson = Context.Persons
                            .Where(exp => exp.Id == person.Id)
                            .Include(exp => exp.ContactInformationList)
                            .Include(exp => exp.PersonInformationList)
                            .Include(exp => exp.DocumentList)
                            .Include(exp => exp.LocationList)
                            .SingleOrDefault();

            if (existingPerson != null)
            {
                Context.Entry(existingPerson).State = EntityState.Modified;
                Context.Entry(existingPerson).CurrentValues.SetValues(person);

                existingPerson.PersonType = person.PersonType;
                existingPerson.FirstName = person.FirstName;
                existingPerson.LastName = person.LastName;
                existingPerson.FullName = person.FullName;
                existingPerson.CountrySubdivisionId = person.CountrySubdivisionId;
                existingPerson.PersonType = person.PersonType;
                existingPerson.ReceiveOffers = person.ReceiveOffers;
                existingPerson.SharePersonData = person.SharePersonData;
                existingPerson.AllowContact = person.AllowContact;

                RemoveLocationListFromPerson(existingPerson, person);
                RemoveContactInformationListFromPerson(existingPerson, person);
                RemovePersonInformationListFromPerson(existingPerson, person);
                RemoveDocumentListFromPerson(existingPerson, person);

                Context.SaveChanges();

                foreach (var contactInformation in person.ContactInformationList)
                {
                    AddContactInformationListFromPerson(existingPerson, contactInformation);
                }

                foreach (var personInformation in person.PersonInformationList)
                {
                    AddPersonInformationListFromPerson(existingPerson, personInformation);
                }

                foreach (var document in person.DocumentList)
                {
                    AddDocumentListFromPerson(existingPerson, document);
                }

                foreach (var location in person.LocationList)
                {
                    AddLocationListFromPerson(existingPerson, location);
                }

                Context.SaveChanges();
            }
        }

        public void UpdatePersonHeaderWithContactInformation(Person person)
        {
            var existingPerson = Context.Persons
                            .Where(exp => exp.Id == person.Id)
                            .Include(exp => exp.ContactInformationList)
                            .SingleOrDefault();

            if (existingPerson != null)
            {
                Context.Entry(existingPerson).State = EntityState.Modified;
                Context.Entry(existingPerson).CurrentValues.SetValues(person);

                existingPerson.PersonType = person.PersonType;
                existingPerson.FirstName = person.FirstName;
                existingPerson.LastName = person.LastName;
                existingPerson.FullName = person.FullName;
                existingPerson.CountrySubdivisionId = person.CountrySubdivisionId;
                existingPerson.PersonType = person.PersonType;
                existingPerson.ReceiveOffers = person.ReceiveOffers;
                existingPerson.SharePersonData = person.SharePersonData;
                existingPerson.AllowContact = person.AllowContact;

                RemoveContactInformationListFromPerson(existingPerson, person);

                Context.SaveChanges();

                if (person.ContactInformationList != null && person.ContactInformationList.Any())
                    AddContactInformationListFromPerson(existingPerson, person.ContactInformationList.FirstOrDefault());

                Context.SaveChanges();
            }
        }

        private void RemoveLocationListFromPerson(Person existingPerson, Person person)
        {
            var locationCategoryIds = person.LocationList.Select(e => e.LocationCategoryId).ToList();
            var locationListToExclude = existingPerson.LocationList.Where(l => l.OwnerId == person.Id && locationCategoryIds.Contains(l.LocationCategoryId)).ToList();

            if (locationListToExclude.Count > 0)
                Context.RemoveRange(locationListToExclude);
        }

        private void RemoveDocumentListFromPerson(Person existingPerson, Person person)
        {
            var documentTypeIds = person.DocumentList.Select(e => e.DocumentTypeId).ToList();
            var documentListToExclude = existingPerson.DocumentList.Where(d => d.OwnerId == person.Id && documentTypeIds.Contains(d.DocumentTypeId)).ToList();

            if (documentListToExclude.Count > 0)
                Context.RemoveRange(documentListToExclude);
        }

        private void RemoveContactInformationListFromPerson(Person existingPerson, Person person)
        {
            var contactInformationListToExclude = existingPerson.ContactInformationList.Where(c => c.OwnerId == person.Id).ToList();
            if (contactInformationListToExclude.Count > 0)
                Context.RemoveRange(contactInformationListToExclude);
        }

        private void RemovePersonInformationListFromPerson(Person existingPerson, Person person)
        {
            var personInformationListToExclude = existingPerson.PersonInformationList.Where(c => c.OwnerId == person.Id).ToList();
            if (personInformationListToExclude.Count > 0)
                Context.RemoveRange(personInformationListToExclude);
        }

        private void AddContactInformationListFromPerson(Person existingPerson,
                                                                ContactInformation contactInformation)
        {
            existingPerson.ContactInformationList.Add(contactInformation);
        }

        private void AddPersonInformationListFromPerson(Person existingPerson,
                                                                PersonInformation personInformation)
        {
            existingPerson.PersonInformationList.Add(personInformation);
        }
        

        private void AddDocumentListFromPerson(Person existingPerson, Document document)
        {
            existingPerson.DocumentList.Add(document);
        }

        private void AddLocationListFromPerson(Person existingPerson, Location location)
        {
            existingPerson.LocationList.Add(location);
        }


        public Guest CreateGuestIfNotExist(Guest guest)
        {
            Guest guestDb = Context.Guests.Where(exp => exp.PersonId == guest.PersonId).FirstOrDefault();

            if (guestDb == null)
            { 
                Context.Guests.Add(guest);
                Context.SaveChanges();
            }
            else
                guest = guestDb;

            return guest;
        }

        public void SaveChanges()
        {
            Context.SaveChanges();
        }

    }
}