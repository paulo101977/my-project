﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using Thex.Domain.Entities;
using Thex.Domain.Interfaces.Repositories;
using Thex.Dto;
using Thex.Infra.Context;
using Thex.Kernel;
using Tnf.EntityFrameworkCore;
using Tnf.EntityFrameworkCore.Repositories;

namespace Thex.Infra.Repositories
{
    public class RoomOccupiedRepository : EfCoreRepositoryBase<ThexContext, RoomOccupied>, IRoomOccupiedRepository
    {
        private readonly IApplicationUser _applicationUser;

        public RoomOccupiedRepository(IDbContextProvider<ThexContext> dbContextProvider, IApplicationUser applicationUser)
            : base(dbContextProvider)
        {
            _applicationUser = applicationUser;
        }

        public void InsertRoomOccupied(RoomOccupied room)
        {
            Context.RoomOccupieds.Add(room);
            Context.SaveChanges();
        }

        public void DeleteRoomOccupiedWithRoomId(int roomId)
        {
            var roomOld = Context
                .RoomOccupieds
                .FirstOrDefault(x => x.RoomId == roomId);

            if (roomOld != null)
            {
                roomOld.IsDeleted = true;
                roomOld.DeletionTime = DateTime.UtcNow.ToZonedDateTimeLoggedUser();
                roomOld.DeleterUserId = _applicationUser.UserUid;
                
                Context.Entry(roomOld).State = EntityState.Modified;
                Context.Entry(roomOld).CurrentValues.SetValues(roomOld);
                Context.SaveChanges();
            }
        }
    }
}
