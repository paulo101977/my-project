﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Thex.Domain.Entities;
using Thex.Domain.Interfaces.Repositories;
using Tnf.EntityFrameworkCore.Repositories;
using Tnf.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Thex.Infra.ReadInterfaces;
using Thex.Infra.Context;
using Thex.Dto;

namespace Thex.EntityFrameworkCore.Repositories
{
    public class BillingTaxRepository : EfCoreRepositoryBase<ThexContext, BillingTax>, IBillingTaxRepository
    {
        

        public BillingTaxRepository(IDbContextProvider<ThexContext> dbContextProvider) 
            : base(dbContextProvider)
        {
            
        }

        public void ToggleAndSaveIsActive(Guid id)
        {
            BillingTax billingTax = base.Get(new DefaultGuidRequestDto(id));
            billingTax.IsActive = !billingTax.IsActive;
            Context.SaveChanges();
        }

        public new void Update(BillingTax billingTax)
        {
            var billingTaxOld = Context
                .BillingTaxs
                
                .Where(x => x.Id == billingTax.Id).SingleOrDefault();

            if (billingTaxOld != null)
            {
                Context.Entry(billingTaxOld).State = EntityState.Modified;
                Context.Entry(billingTaxOld).CurrentValues.SetValues(billingTax);
                Context.SaveChanges();
            }
        }

        public void Delete(Guid id)
        {
            var obj = base.Get(new DefaultGuidRequestDto(id));
            base.Delete(obj);
            Context.SaveChanges();
        }

        public void Create(BillingTax billingTax)
        {
            base.Insert(billingTax);
            Context.SaveChanges();
        }
    }
}
