﻿//  <copyright file="ReservationRepository.cs" company="TOTVS">
//  Copyright (c) TOTVS. All rights reserved.
//  </copyright>
namespace Thex.EntityFrameworkCore.Repositories
{
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;

    using Thex.Domain.Entities;

    using Microsoft.EntityFrameworkCore;

    using Tnf.EntityFrameworkCore.Repositories;
    using Tnf.EntityFrameworkCore;
    using Thex.Domain.Interfaces.Repositories;
    using Thex.Common.Enumerations;
    using System;
    using Thex.Infra.ReadInterfaces;
    using Thex.Infra.Context;
    using EFCore.BulkExtensions;
    using Thex.Dto;

    public class ReservationItemRepository : EfCoreRepositoryBase<ThexContext, ReservationItem>, IReservationItemRepository
    {
        public ReservationItemRepository(IDbContextProvider<ThexContext> dbContextProvider)
            : base(dbContextProvider)
        {
        }

        public void BulkUpdate(List<ReservationItem> reservationItem)
        {
            Context.BulkUpdate(reservationItem);
        }

        void IReservationItemRepository.Update(ReservationItem reservationItem)
        {
            var entity = reservationItem;

            Context.Entry(entity).State = EntityState.Modified;

            Context.SaveChanges();
        }

        public void UpdateIntegrationId(string integrationId, ProformaRequestDto requestDto)
        {
            if (requestDto.ReservationId.HasValue)
            {
                Context.ReservationItems
                       .Where(p => p.ReservationId == requestDto.ReservationId.Value)
                       .IgnoreQueryFilters()
                       .BatchUpdate(new ReservationItem
                       {
                           IntegrationId = integrationId
                       });
            }
            else
            {
                Context.ReservationItems
                       .Where(p => p.Id == requestDto.ReservationItemId.Value)
                       .IgnoreQueryFilters()
                       .BatchUpdate(new ReservationItem
                       {
                           IntegrationId = integrationId
                       });
            }
        }
    }
}