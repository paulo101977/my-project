﻿using System;
using System.Collections.Generic;
using System.Text;
using Thex.Domain.Entities;
using Thex.Domain.Interfaces.Repositories;
using Tnf.EntityFrameworkCore.Repositories;
using Tnf.EntityFrameworkCore;
using Thex.Infra.Context;

namespace Thex.EntityFrameworkCore.Repositories
{
    public class RatePlanCompanyClientRepository : EfCoreRepositoryBase<ThexContext, RatePlanCompanyClient>, IRatePlanCompanyClientRepository
    {
        public RatePlanCompanyClientRepository(IDbContextProvider<ThexContext> dbContextProvider) : base(dbContextProvider)
        {
        }

        public void RatePlanCompanyClientCreate(List<RatePlanCompanyClient> ratePlanCompanyClients)
        {
            Context.RatePlanCompanyClients.AddRange(ratePlanCompanyClients);
            Context.SaveChanges();
        }
    }
}
