﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Thex.Domain.Entities;
using Thex.Domain.Interfaces.Repositories;
using Thex.Infra.Context;
using Tnf.EntityFrameworkCore;
using Tnf.EntityFrameworkCore.Repositories;

namespace Thex.Infra.Repositories
{
    public class LevelRateHeaderRepository : EfCoreRepositoryBase<ThexContext, LevelRateHeader>, ILevelRateHeaderRepository
    {
        public LevelRateHeaderRepository(IDbContextProvider<ThexContext> dbContextProvider)
            : base(dbContextProvider)
        {
        }

        public void ToggleIsActive(Guid id)
        {
            var levelRateHeader = Context.LevelRateHeaders.FirstOrDefault(lrh => lrh.Id == id);

            levelRateHeader.IsActive = !levelRateHeader.IsActive;

            Context.LevelRateHeaders.Update(levelRateHeader);

            Context.SaveChanges();
        }

        public void RemoveAndSaveChanges(LevelRateHeader levelRateHeader)
        {
            var levelRates = Context.LevelRates.Where(lr => lr.LevelRateHeaderId == levelRateHeader.Id);

            Context.LevelRates.RemoveRange(levelRates);

            Context.LevelRateHeaders.Remove(levelRateHeader);

            Context.SaveChanges();
        }
    }
}
