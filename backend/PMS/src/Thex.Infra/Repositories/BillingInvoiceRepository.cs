﻿// //  <copyright file="BillingAccountRepository.cs" company="TOTVS">
// //  Copyright (c) TOTVS. All rights reserved.
// //  </copyright>
namespace Thex.EntityFrameworkCore.Repositories
{
    using Microsoft.EntityFrameworkCore;
    using System;
    using System.Linq;
    using Thex.Domain.Entities;
    using Thex.Domain.Interfaces.Repositories;

    using Tnf.EntityFrameworkCore.Repositories;
    using Tnf.EntityFrameworkCore;
    using Thex.Infra.Context;
    using System.Threading.Tasks;

    public class BillingInvoiceRepository : EfCoreRepositoryBase<ThexContext, BillingInvoice>, IBillingInvoiceRepository
    {
        private readonly IBillingAccountItemRepository _billingAccountItemRepository;

        public BillingInvoiceRepository(IDbContextProvider<ThexContext> dbContextProvider,
            IBillingAccountItemRepository billingAccountItemRepository)
            : base(dbContextProvider)
        {
            _billingAccountItemRepository = billingAccountItemRepository;
        }

        public void Create(BillingInvoice billingInvoice)
        {
            try
            {
                Context.BillingInvoices.Add(billingInvoice);
                Context.SaveChanges();
            }
            catch (DbUpdateException ex)
            {
                DetachBillingInvoice(billingInvoice);

                throw ex;
            }
        }

        public void DetachBillingInvoice(BillingInvoice billingInvoice)
        {
            Context.Entry(billingInvoice).State = EntityState.Detached;
        }

        public async Task SaveChanges()
        {
            await Context.SaveChangesAsync();
        }

        public async Task<BillingInvoice> GetById(Guid id)
        {
            return await Context.BillingInvoices
                .Include(b => b.BillingAccount)
                .Include(b => b.BillingAccountItemList)
                .FirstOrDefaultAsync(b => b.Id == id);
        }

        public async Task RemoveBillingInvoiceIdOfBillingAccountList(Guid billingInvoiceId)
        {
            var billingAccountItemList = await _billingAccountItemRepository.GetAllListAsync(b => b.BillingInvoiceId == billingInvoiceId);

            if (billingAccountItemList.Any())
            {
                foreach (var billingAccountItem in billingAccountItemList)
                {
                    billingAccountItem.BillingInvoiceId = null;
                }
            }
        }

        public BillingInvoice GetByIdReference(Guid billingInvoiceReferenceId)
        {
            return (from billingInvoice in Context.BillingInvoices
                    where billingInvoice.BillingInvoiceReferenceId == billingInvoiceReferenceId
                    select billingInvoice
                    ).FirstOrDefault();
        }
    }
}