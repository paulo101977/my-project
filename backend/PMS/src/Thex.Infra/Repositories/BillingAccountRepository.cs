﻿// //  <copyright file="BillingAccountRepository.cs" company="TOTVS">
// //  Copyright (c) TOTVS. All rights reserved.
// //  </copyright>
namespace Thex.EntityFrameworkCore.Repositories
{
    using Microsoft.EntityFrameworkCore;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Thex.Common.Enumerations;
    using Thex.Domain.Entities;
    using Thex.Domain.Interfaces.Repositories;
    using Thex.Dto;
    using Thex.Infra.ReadInterfaces;
    using Tnf.EntityFrameworkCore.Repositories;
    using Tnf.EntityFrameworkCore;
    using Thex.Infra.Context;
    using Thex.Common.Extensions;
    using EFCore.BulkExtensions;

    public class BillingAccountRepository : EfCoreRepositoryBase<ThexContext, BillingAccount>, IBillingAccountRepository
    {
        
        public BillingAccountRepository(IDbContextProvider<ThexContext> dbContextProvider)
            : base(dbContextProvider)
        {
            
        }

        public BillingAccount GetById(Guid id)
        {
            return Context.BillingAccounts.Find(id);
        }


        public IList<BillingAccount> GetListByGroupKey(Guid groupKey)
        {
            return Context.BillingAccounts.Where(b => b.GroupKey == groupKey).ToList();
        }

        public decimal GetTotalAmountByBillingAccountId(Guid billingAccountId)
        {
            var result = Context
                .BillingAccountItems
                
                .Where(e => e.BillingAccountId == billingAccountId).Sum(e => e.Amount);

            return result;
        }

        public void CloseBillingAccount(Guid billingAccountId)
        {
            var accountBalance = GetTotalAmountByBillingAccountId(billingAccountId);

            if (accountBalance != 0)
                return;

            VerifyAttachedEntityAndCloseBillingAccount(billingAccountId);

            //Context.Set<BillingAccount>().Find(Func<BillingAccount, Guid>)
        }

        private static void Close(BillingAccount billingAccountEntity)
        {
            billingAccountEntity.EndDate = DateTime.UtcNow.ToZonedDateTimeLoggedUser();
            billingAccountEntity.StatusId = (int)AccountBillingStatusEnum.Closed;
        }

        public void CloseAllBillingAccounts(List<Guid> billingAccountIdList)
        {
            var billingAccountGroup = Context
                                    .BillingAccountItems
                                    
                                    .Where(e => billingAccountIdList.Contains(e.BillingAccountId))
                                    .GroupBy(e => e.BillingAccountId)
                                    .Select(e => new
                                    {
                                        Id = e.Key,
                                        Amount = e.Sum(a => a.Amount)
                                    });

            foreach (var billingAccountId in billingAccountIdList)
            {
                var billingAccountWithAmount = billingAccountGroup
                    .FirstOrDefault(e => e.Id == billingAccountId);

                if (billingAccountWithAmount != null && billingAccountWithAmount.Amount != 0)
                    continue;

                VerifyAttachedEntityAndCloseBillingAccount(billingAccountId);
            }
        }

        private void VerifyAttachedEntityAndCloseBillingAccount(Guid billingAccountId)
        {
            var entityAttached = Context.Set<BillingAccount>().Local.FirstOrDefault(e => e.Id == billingAccountId);

            if (entityAttached != null)
            {
                Close(entityAttached);
            }
            else
            {
                var billingAccountEntity = new BillingAccount() { Id = billingAccountId };

                Context.BillingAccounts.Attach(billingAccountEntity);

                Close(billingAccountEntity);
            }
        }

        public void SaveChanges()
        {
            Context.SaveChanges();
        }

        public void DetachBillingInvoice(BillingInvoice billingInvoice)
        {
            Context.Entry(billingInvoice).State = EntityState.Detached;
        }


        public void DetachBillingAccount(BillingAccount billingAccount)
        {
            Context.Entry(billingAccount).State = EntityState.Detached;
        }

        public BillingAccount GetBillingAccountByInvoiceId(Guid invoiceId)
        {
            return (from ba in Context.BillingAccounts
                       join bi in Context.BillingInvoices on ba.Id equals bi.BillingAccountId
                    where bi.Id == invoiceId
                    select ba).FirstOrDefault();
        }

        public BillingAccount GetWithPersonHeaderById(Guid id)
        {
            return Context.BillingAccounts
                .Include(b => b.PersonHeader)
                .FirstOrDefault(b => b.Id == id);
        }

        public void CreateBillingAccounts(List<BillingAccount> billingAccountList)
        {
            if (billingAccountList.Any())
                Context.BillingAccounts.AddRange(billingAccountList);
        }

        public void UpdateBillingAccounts(List<BillingAccount> billingAccountList)
        {
            if (billingAccountList.Any())
                Context.BulkUpdate(billingAccountList);
        }

        public void DeleteRange(List<Guid> idList)
        {
            if (idList != null && idList.Count > 0)
            {
                Context.BillingAccounts
                    .Where(b => idList.Contains(b.Id))
                    .IgnoreQueryFilters()
                    .BatchDelete();

                Context.SaveChanges();
            }
        }
    }
}
