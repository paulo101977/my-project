﻿//  <copyright file="ReservationRepository.cs" company="TOTVS">
//  Copyright (c) TOTVS. All rights reserved.
//  </copyright>
namespace Thex.EntityFrameworkCore.Repositories
{
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;

    using Thex.Domain.Entities;

    using Microsoft.EntityFrameworkCore;

    using Tnf.EntityFrameworkCore.Repositories;
    using Tnf.EntityFrameworkCore;
    using Thex.Domain.Interfaces.Repositories;
    using Thex.Common.Enumerations;
    using System;
    using Thex.Infra.ReadInterfaces;
    using Thex.Infra.Context;

    public class ReservationRepository : EfCoreRepositoryBase<ThexContext, Reservation>, IReservationRepository
    {
       
        public ReservationRepository(IDbContextProvider<ThexContext> dbContextProvider)
            : base(dbContextProvider)
        {
            
        }

        public Reservation GetExistingReservation(long reservationId)
        {
            return Context
                .Reservations
                
                .Where(exp => exp.Id == reservationId)
                    .Include(exp => exp.ReservationItemList)
                    .ThenInclude(exp => exp.GuestReservationItemList)
                    .Include(exp => exp.ReservationItemList)
                    .ThenInclude(exp => exp.ReservationBudgetList)
                .SingleOrDefault();
        }
   
        public Reservation Update(Reservation reservation, Reservation existingReservation, ReservationConfirmation reservationConfirmation, Plastic plastic, DateTime systemDate)
        {
            var existingReservationConfirmation = Context
                .ReservationConfirmations
                
               .Where(exp => exp.ReservationId == reservation.Id)
                   .Include(exp => exp.Plastic)
               .SingleOrDefault();
            
            if(existingReservationConfirmation != null)
            {
                RemovePlasticFromReservationConfirmation(existingReservationConfirmation);

                reservationConfirmation.Id = existingReservationConfirmation.Id;

                if (plastic != null)
                    InsertPlasticAndSetReservationConfirmation(reservation, reservationConfirmation, plastic);
                else
                    SetReservationConfirmationFromReservation(reservation, reservationConfirmation, null);

                Context.Entry(existingReservationConfirmation).CurrentValues.SetValues(reservationConfirmation);
            }
            else
            {
                if (plastic != null)
                    InsertPlasticAndSetReservationConfirmation(reservation, reservationConfirmation, plastic);
                else
                    SetReservationConfirmationFromReservation(reservation, reservationConfirmation, null);

                Context.Add(reservationConfirmation);
            }


            if(existingReservation != null)
            {
                Context.Entry(existingReservation).CurrentValues.SetValues(reservation);

                RemoveGuestReservationsItemsFromReservation(reservation, existingReservation);
                RemoveReservationBudgetItemsFromReservation(reservation, existingReservation, systemDate);

                Context.SaveChanges();

                foreach (var reservationItem in reservation.ReservationItemList)
                    InsertOrUpdateReservationItemFromReservation(reservation, existingReservation, reservationItem);
            }

            Context.SaveChanges();

            return existingReservation;
        }

        private void InsertPlasticAndSetReservationConfirmation(Reservation reservation, ReservationConfirmation reservationConfirmation, Plastic plastic)
        {
            var plasticId = InsertPlasticFromReservationConfirmation(plastic);

            SetReservationConfirmationFromReservation(reservation, reservationConfirmation, plasticId);
        }

        private static void SetReservationConfirmationFromReservation(Reservation reservation, ReservationConfirmation reservationConfirmation, long? plasticId)
        {
            reservationConfirmation.ReservationId = reservation.Id;

            if (plasticId.HasValue)
                reservationConfirmation.PlasticId = plasticId;
        }

        private long InsertPlasticFromReservationConfirmation(Plastic plastic)
        {
            Context.Plastics.Add(plastic);
            Context.SaveChanges();

            return plastic.Id;
        }

        private void InsertOrUpdateReservationItemFromReservation(Reservation reservation, Reservation existingReservation, ReservationItem reservationItem)
        {
            var existingReservationItem = existingReservation.ReservationItemList.Where(e => e.Id == reservationItem.Id && e.Id != 0).FirstOrDefault();

            if (existingReservationItem != null)
            {
                foreach (var reservationBudgetItem in reservationItem.ReservationBudgetList)
                    InsertOrUpdateReservationBudgetItemFromReservation(reservationItem, existingReservationItem, reservationBudgetItem);

                foreach (var guestReservationItem in reservationItem.GuestReservationItemList)
                    InsertOrUpdateGuestReservationItemFromReservation(reservationItem, existingReservationItem, guestReservationItem);

                reservationItem.ReservationId = reservation.Id;

                if (existingReservationItem.ReservationItemStatusId >= (int)ReservationStatus.Checkin)
                    reservationItem.ReservationItemStatusId = existingReservationItem.ReservationItemStatusId;

                Context.Entry(existingReservationItem).CurrentValues.SetValues(reservationItem);
            }
            else
            {
                existingReservation.ReservationItemList.Add(reservationItem);
            }
        }

        private void InsertOrUpdateReservationBudgetItemFromReservation(ReservationItem reservationItem, ReservationItem existingReservationItem, ReservationBudget reservationBudgetItem)
        {
            var existingReservationBudge = existingReservationItem.ReservationBudgetList
                                           .Where(d => d.Id == reservationBudgetItem.Id && d.Id != 0)
                                           .FirstOrDefault();

            if (existingReservationBudge != null)
            {
                reservationBudgetItem.ReservationItemId = reservationItem.Id;
                
                Context.Entry(existingReservationBudge).CurrentValues.SetValues(reservationBudgetItem);
            }
            else
                existingReservationItem.ReservationBudgetList.Add(reservationBudgetItem);
        }

        private void InsertOrUpdateGuestReservationItemFromReservation(ReservationItem reservationItem, ReservationItem existingReservationItem, GuestReservationItem guestReservationItem)
        {
            var existingGuestReservationItem = existingReservationItem.GuestReservationItemList
                                            .Where(d => d.Id == guestReservationItem.Id && d.Id != 0)
                                            .FirstOrDefault();

            if (existingGuestReservationItem != null)
            {
                if(existingGuestReservationItem.GuestStatusId < (int)ReservationStatus.Checkin)
                {
                    guestReservationItem.ReservationItemId = reservationItem.Id;

                    Context.Entry(existingGuestReservationItem).CurrentValues.SetValues(guestReservationItem);
                }
            }
            else
                existingReservationItem.GuestReservationItemList.Add(guestReservationItem);
        }

        private void RemovePlasticFromReservationConfirmation(ReservationConfirmation existingReservationConfirmation)
        {
            if (existingReservationConfirmation.Plastic != null)
                Context.Remove(existingReservationConfirmation.Plastic);
        }

        private void RemoveReservationBudgetItemsFromReservation(Reservation reservation, Reservation existingReservation, DateTime systemDate)
        {
            List<long> reservationItemsIds = GetReservationItemsIdsFromReservation(reservation);
            List<long> reservationBudgetIdsExceptList = reservation.ReservationItemList.SelectMany(d => d.ReservationBudgetList).Select(g => g.Id).ToList();

            var reservationBudgetListToExclude = existingReservation
                                                    .ReservationItemList
                                                    .SelectMany(exp => exp.ReservationBudgetList)
                                                    .Where(exp =>  exp.BudgetDay.Date >= systemDate.Date && reservationItemsIds.Contains(exp.ReservationItemId) && !reservationBudgetIdsExceptList.Contains(exp.Id)).ToList();

            if (reservationBudgetListToExclude.Count > 0)
                Context.RemoveRange(reservationBudgetListToExclude);
        }

        private void RemoveGuestReservationsItemsFromReservation(Reservation reservation, Reservation existingReservation)
        {
            List<long> reservationItemsIds = GetReservationItemsIdsFromReservation(reservation);
            List<long> guestReservationsIdsExceptList = GetGuestReservationItemsIdsFromReservation(reservation);

            RemoveReservatinItemsExceptListOfIds(existingReservation, reservationItemsIds, guestReservationsIdsExceptList);
        }
        
        private void RemoveReservatinItemsExceptListOfIds(Reservation existingReservation, List<long> reservationItemsIds, List<long> guestReservationsIdsExceptList)
        {
            var guestReservationItemsToExclude = existingReservation.ReservationItemList.SelectMany(exp => exp.GuestReservationItemList).Where(exp => reservationItemsIds.Contains(exp.ReservationItemId) && !guestReservationsIdsExceptList.Contains(exp.Id) && exp.GuestStatusId <= (int)ReservationStatus.Checkin).ToList();
            if (guestReservationItemsToExclude.Count > 0)
                Context.RemoveRange(guestReservationItemsToExclude);
        }

        private List<long> GetReservationBudgetItemsIdsFromReservation(Reservation reservation)
        {
            return reservation.ReservationItemList.SelectMany(d => d.ReservationBudgetList).Select(g => g.Id).ToList();
        }

        private List<long> GetGuestReservationItemsIdsFromReservation(Reservation reservation)
        {
            return reservation.ReservationItemList.SelectMany(d => d.GuestReservationItemList).Select(g => g.Id).ToList();
        }

        private List<long> GetReservationItemsIdsFromReservation(Reservation reservation)
        {
            return reservation.ReservationItemList.Select(r => r.Id).ToList();
        }

        public bool CheckStatusAllItensForModification(long reservationId)
        {
            //Verificar quantos itens possui a reservar e depois validar se todas estam em condições de serem alteradas
            var numberItens = Context
                .ReservationItems
                
                .Where(exp => exp.ReservationId == reservationId).Count();

            var numberItensStatusForModification = Context.ReservationItems
                .Where(exp => exp.ReservationId == reservationId && (exp.ReservationItemStatusId == (int)ReservationStatus.Canceled || exp.ReservationItemStatusId == (int)ReservationStatus.NoShow || exp.ReservationItemStatusId == (int)ReservationStatus.Checkout)).Count();

            if (numberItens == numberItensStatusForModification)
                return false;

            return true;
        }

        public IList<Reservation> GetReservationListWithConfirmedOrToConfirmStatus(int propertyId)
        {
            return (from reservation in Context.Reservations
                    join reservationConfirmation in Context.ReservationConfirmations on reservation.Id equals reservationConfirmation.ReservationId
                    join reservationItem in Context.ReservationItems on reservation.Id equals reservationItem.ReservationId
                    where (reservationItem.ReservationItemStatusId == (int)ReservationStatus.ToConfirm || reservationItem.ReservationItemStatusId == (int)ReservationStatus.Confirmed)
                        && reservation.PropertyId == propertyId
                    select reservation)
                    .Include(r => r.ReservationConfirmationList)
                    .Include(r => r.ReservationItemList)
                    .DistinctBy(r => r.Id)
                    .ToList();
        }

        public List<ReservationItem> GetReservationItemListByEstimatedDepartureDateAndPropertyId(DateTime estimatedDepartureDate, int propertyId)
        {
            return (from reservationItem in Context.ReservationItems
                    join reservation in Context.Reservations on reservationItem.ReservationId equals reservation.Id
                    where reservationItem.ReservationItemStatusId == (int)ReservationStatus.Checkin
                    && reservationItem.EstimatedDepartureDate <= estimatedDepartureDate
                    && reservation.PropertyId == propertyId
                    select reservationItem).ToList();
        }

        public void ChangeStatusReservationItem(long reservationItemId, int status, DateTime systemDate)
        {
            var reservationItem = Context.ReservationItems.FirstOrDefault(x => x.Id == reservationItemId);
            if (reservationItem != null)
            {
                reservationItem.ReservationItemStatusId = status;

                if (status == (int)ReservationStatus.Checkout && !reservationItem.CheckOutDate.HasValue)
                    reservationItem.SetToCheckout(systemDate, true);

                Context.SaveChanges();
            }
        }

        //public void GetSlipReservation(long reservationId)
        //{
        //     Context.Reservations
        //       .Where(exp => exp.Id == reservationId)
        //           .Include(exp => exp.ReservationItemList)
        //           .ThenInclude(exp => exp.GuestReservationItemList)
        //           .Include(exp => exp.ReservationItemList)
        //           .ThenInclude(exp => exp.ReservationBudgetList)
        //       .SingleOrDefault();
        //}

        public void RemoveRoom(long reservationItemId)
        {
            var entity = Context.ReservationItems.FirstOrDefault(e => e.Id == reservationItemId);

            entity.RoomId = null;

            Context.Entry(entity).State = EntityState.Modified;

            Context.SaveChanges();
        }

    }
}