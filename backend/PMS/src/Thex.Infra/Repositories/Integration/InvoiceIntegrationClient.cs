﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Thex.Common.Enumerations;
using Thex.Common.Helpers;
using Thex.Domain.Entities;
using Thex.Domain.Interfaces.Integration;
using Thex.Dto.Integration;
using Thex.Infra.Context;
using Thex.Kernel;
using Tnf.EntityFrameworkCore;
using Tnf.EntityFrameworkCore.Repositories;

namespace Thex.Infra.Repositories.Integration
{
    public class InvoiceIntegrationClient : EfCoreRepositoryBase<ThexContext, BillingInvoice>, IInvoiceIntegrationClient
    {
        public IApplicationUser _applicationUser { get; set; }

        public InvoiceIntegrationClient(
            IDbContextProvider<ThexContext> dbContextProvider,
            IApplicationUser applicationUser)
            : base(dbContextProvider)
        {
            _applicationUser = applicationUser;
        }

        public async Task<List<IntegrationClientDto>> GetBorrowerInformation(BillingAccount billingAccount, BillingInvoiceTypeEnum invoiceType)
        {
            var clientList = new List<IntegrationClientDto>();

            if (billingAccount.PersonHeaderId.HasValue)
                clientList = await GetBorrowerInformationByPersonHeader(billingAccount.PersonHeaderId.Value);
            else if (billingAccount.GuestReservationItemId.HasValue)
                clientList = await GetBorrowerInformationsByReservation(billingAccount.GuestReservationItemId.Value);
            else
                clientList = await GetBorrowerInformationByCompany(billingAccount.CompanyClientId.Value);

            if (invoiceType == BillingInvoiceTypeEnum.NFCE && clientList != null && clientList.Count > 0 &&
                EnumHelper.GetEnumValue<CountryIsoCodeEnum>(_applicationUser.PropertyCountryCode, false) == CountryIsoCodeEnum.Brasil)
                foreach (var client in clientList)
                {
                    if (client != null && client.Address != null && client.Address.Country != null &&
                        client.Address.Country != null && client.Address.Country.ToUpper() != "BRA")
                    {
                        client.Address = null;
                        client.Document = null;
                        client.Email = null;
                        client.Name = null;
                        client.Phone = null;
                    }
                }

            return clientList;
        }

        /// <summary>
        /// Retornar as informações do cliente através do cabeçalho
        /// </summary>
        /// <param name="companyClientId"></param>
        /// <returns></returns>
        private async Task<List<IntegrationClientDto>> GetBorrowerInformationByPersonHeader(Guid personId)
        {
            return await (from person in Context.Persons
                          join document in Context.Documents on person.Id equals document.OwnerId
                          join documentType in Context.DocumentTypes on document.DocumentTypeId equals documentType.Id

                          join phoneInformation in this.Context.ContactInformations.AsNoTracking() on
                            new { p3 = person.Id, p4 = Convert.ToInt32(ContactInformationTypeEnum.PhoneNumber) } equals
                            new { p3 = phoneInformation.OwnerId, p4 = phoneInformation.ContactInformationTypeId } into ri2
                          from phoneInformationLeft in ri2.DefaultIfEmpty()

                          join emailInformation in Context.ContactInformations on
                              new { p1 = person.Id, p2 = Convert.ToInt32(ContactInformationTypeEnum.Email) } equals
                              new { p1 = emailInformation.OwnerId, p2 = emailInformation.ContactInformationTypeId } into ri
                          from emailInformationLeftRoom in ri.DefaultIfEmpty()

                          join l in Context.Locations on person.Id equals l.OwnerId into locations
                          from leftLocation in locations.DefaultIfEmpty()

                          join lc in Context.LocationCategories on leftLocation.LocationCategoryId equals lc.Id into locationCategories
                          from leftLocationCategory in locationCategories.DefaultIfEmpty()

                          join c in Context.CountrySubdivisions on leftLocation.CityId equals c.Id into city
                          from leftCity in city.DefaultIfEmpty()

                          join s in Context.CountrySubdivisions on leftCity.ParentSubdivision.Id equals s.Id into state
                          from leftState in state.DefaultIfEmpty()

                          join co in Context.CountrySubdivisions on leftLocation.CountryId equals co.Id into country
                          from leftCountry in country.DefaultIfEmpty()

                          from leftCityTranslation in Context.CountrySubdivisionTranslations.Where(o => leftCity.Id == o.CountrySubdivisionId)
                                                      .Take(1)
                                                      .DefaultIfEmpty()

                          from leftStateTranslation in Context.CountrySubdivisionTranslations.Where(o => leftState.Id == o.CountrySubdivisionId)
                                                      .Take(1)
                                                      .DefaultIfEmpty()

                          from leftCountryTranslation in Context.CountrySubdivisionTranslations.Where(o => leftCountry.Id == o.CountrySubdivisionId &&
                                                                                                                             o.LanguageIsoCode == "en-us")
                                                      .Take(1)
                                                      .DefaultIfEmpty()

                          where person.Id == personId

                          select new IntegrationClientDto
                          {
                              Id = person.Id.ToString(),
                              Document = document != null && document.DocumentInformation != null ? GetDocumentInformationValidatedInCountry(document.DocumentInformation) : null,
                              DocumentTypeId = document != null ? document.DocumentTypeId : default(int),
                              Name = person.FullName,
                              Email = emailInformationLeftRoom != null ? emailInformationLeftRoom.Information : string.Empty,
                              Phone = phoneInformationLeft != null ? phoneInformationLeft.Information : string.Empty,
                              Address = new IntegrationAddressDto
                              {
                                  Country = EnumHelper.GetEnumValue<CountryIsoCodeEnum>(_applicationUser.PropertyCountryCode, false) == CountryIsoCodeEnum.Europe ?
                                                    leftCountryTranslation != null ? leftCountryTranslation.Name : null :
                                                    leftCountry != null ? leftCountry.ThreeLetterIsoCode : null,
                                  District = leftState != null ? leftState.TwoLetterIsoCode : null,
                                  City = new IntegrationCityDTO
                                  {
                                      Code = leftCity != null ? leftCity.Id.ToString() : "0",
                                      Name = leftCityTranslation != null ? leftCityTranslation.Name : null
                                  },
                                  StreetName = leftLocation != null ? leftLocation.StreetName : null,
                                  StreetNumber = leftLocation != null ? leftLocation.StreetNumber : null,
                                  AdditionalInformation = leftLocation != null && leftLocation.AdditionalAddressDetails != null ? leftLocation.AdditionalAddressDetails.PadLeft(30) : null,
                                  Neighborhood = leftLocation != null ? leftLocation.Neighborhood : null,
                                  PostalCode = leftLocation != null && leftLocation.PostalCode != null ? leftLocation.PostalCode.ToString() : null
                              }
                          }).ToListAsync();
        }

        /// <summary>
        /// Retornar as informações do cliente através da empresa
        /// </summary>
        /// <param name="companyClientId"></param>
        /// <returns></returns>
        private async Task<List<IntegrationClientDto>> GetBorrowerInformationByCompany(Guid companyClientId)
        {
            return await (from client in Context.CompanyClients
                          join document in Context.Documents on client.PersonId equals document.OwnerId
                          join documentType in Context.DocumentTypes on document.DocumentTypeId equals documentType.Id
                          join emailInformation in Context.ContactInformations on
                          new { p1 = client.PersonId, p2 = Convert.ToInt32(ContactInformationTypeEnum.Email) } equals
                          new { p1 = emailInformation.OwnerId, p2 = emailInformation.ContactInformationTypeId } into ri
                          from emailInformationLeftRoom in ri.DefaultIfEmpty()

                          join l in Context.Locations on client.PersonId equals l.OwnerId into locations
                          from leftLocation in locations.DefaultIfEmpty()

                          join lc in Context.LocationCategories on leftLocation.LocationCategoryId equals lc.Id into locationCategories
                          from leftLocationCategory in locationCategories.DefaultIfEmpty()

                          join c in Context.CountrySubdivisions on leftLocation.CityId equals c.Id into city
                          from leftCity in city.DefaultIfEmpty()

                          join s in Context.CountrySubdivisions on leftCity.ParentSubdivision.Id equals s.Id into state
                          from leftState in state.DefaultIfEmpty()

                          join co in Context.CountrySubdivisions on client.CountrySubdivisionId equals co.Id into country
                          from leftCountry in country.DefaultIfEmpty()

                          from leftCityTranslation in Context.CountrySubdivisionTranslations.Where(o => leftCity.Id == o.CountrySubdivisionId)
                                                      .Take(1)
                                                      .DefaultIfEmpty()

                          from leftStateTranslation in Context.CountrySubdivisionTranslations.Where(o => leftState.Id == o.CountrySubdivisionId)
                                                      .Take(1)
                                                      .DefaultIfEmpty()

                          from leftCountryTranslation in Context.CountrySubdivisionTranslations.Where(o => leftCountry.Id == o.CountrySubdivisionId &&
                                                                                                                             o.LanguageIsoCode == "en-us")
                                                      .Take(1)
                                                      .DefaultIfEmpty()

                          where client.Id == companyClientId

                          select new IntegrationClientDto
                          {
                              Id = document != null && document.OwnerId != null ? document.OwnerId.ToString() : string.Empty,
                              Document = document != null && document.DocumentInformation != null ? GetDocumentInformationValidatedInCountry(document.DocumentInformation) : null,
                              DocumentTypeId = document != null ? document.DocumentTypeId : default(int),
                              Name = client.TradeName,
                              Email = emailInformationLeftRoom != null ? emailInformationLeftRoom.Information : string.Empty,
                              Address = new IntegrationAddressDto
                              {
                                  Country = EnumHelper.GetEnumValue<CountryIsoCodeEnum>(_applicationUser.PropertyCountryCode, false) == CountryIsoCodeEnum.Europe ?
                                                    leftCountryTranslation != null ? leftCountryTranslation.Name : null :
                                                    leftCountry != null ? leftCountry.ThreeLetterIsoCode : null,
                                  District = leftState != null ? leftState.TwoLetterIsoCode : null,
                                  City = new IntegrationCityDTO
                                  {
                                      Code = leftCity != null ? leftCity.Id.ToString() : "0",
                                      Name = leftCityTranslation != null ? leftCityTranslation.Name : null
                                  },
                                  StreetName = leftLocation != null ? leftLocation.StreetName : null,
                                  StreetNumber = leftLocation != null ? leftLocation.StreetNumber : null,
                                  AdditionalInformation = leftLocation != null && leftLocation.AdditionalAddressDetails != null ? leftLocation.AdditionalAddressDetails.PadLeft(30) : null,
                                  Neighborhood = leftLocation != null ? leftLocation.Neighborhood : null,
                                  PostalCode = leftLocation != null && leftLocation.PostalCode != null ? leftLocation.PostalCode.ToString() : null
                              }
                          }).ToListAsync();
        }

        /// <summary>
        /// Retorna as informações do cliente através da reserva
        /// </summary>
        /// <param name="guestReservationItemId"></param>
        /// <returns></returns>
        private async Task<List<IntegrationClientDto>> GetBorrowerInformationsByReservation(long guestReservationItemId)
        {
            return await
                        (from gri in Context.GuestReservationItems

                         join gr in Context.GuestRegistrations on gri.GuestRegistrationId equals gr.Id into gr
                         from leftGuestRegistration in gr.DefaultIfEmpty()

                         join responsibleguestRegistration in Context.GuestRegistrations on leftGuestRegistration.ResponsibleGuestRegistrationId equals responsibleguestRegistration.Id into lrgr
                         from leftResponsibleGuestRegistration in lrgr.DefaultIfEmpty()

                         join ri in Context.ReservationItems on gri.ReservationItemId equals ri.Id into ri
                         from leftReservationItem in ri.DefaultIfEmpty()

                         join r in Context.Reservations on leftReservationItem.ReservationId equals r.Id into r
                         from leftReservation in r.DefaultIfEmpty()

                         join l in Context.Locations on (leftResponsibleGuestRegistration != null ? leftResponsibleGuestRegistration.Id : leftGuestRegistration.Id) equals l.OwnerId into locations

                         from leftLocation in locations.DefaultIfEmpty()
                         join lc in Context.LocationCategories on leftLocation.LocationCategoryId equals lc.Id into locationCategories

                         from leftLocationCategory in locationCategories.DefaultIfEmpty()
                         join c in Context.CountrySubdivisions on leftLocation.CityId equals c.Id into city

                         from leftCity in city.DefaultIfEmpty()
                         join s in Context.CountrySubdivisions on leftCity.ParentSubdivision.Id equals s.Id into state

                         from leftState in state.DefaultIfEmpty()
                         join co in Context.CountrySubdivisions on leftLocation.CountryId equals co.Id into country

                         from leftCountry in country.DefaultIfEmpty()
                         from leftCityTranslation in Context.CountrySubdivisionTranslations.Where(o => leftCity.Id == o.CountrySubdivisionId)
                                                     .Take(1)
                                                     .DefaultIfEmpty()

                         from leftStateTranslation in Context.CountrySubdivisionTranslations.Where(o => leftState.Id == o.CountrySubdivisionId)
                                                     .Take(1)
                                                     .DefaultIfEmpty()

                         from leftCountryTranslation in Context.CountrySubdivisionTranslations.Where(o => leftCountry.Id == o.CountrySubdivisionId &&
                                                                                                                            o.LanguageIsoCode == "en-us")
                                                     .Take(1)
                                                     .DefaultIfEmpty()

                         where gri.Id == guestReservationItemId

                         select new IntegrationClientDto
                         {
                             Id = leftResponsibleGuestRegistration != null && leftResponsibleGuestRegistration.Id != null ?
                                            leftResponsibleGuestRegistration.Id.ToString() : leftGuestRegistration != null && leftGuestRegistration.PersonId != null ? leftGuestRegistration.PersonId.ToString() : string.Empty,
                             Document = leftResponsibleGuestRegistration != null && leftResponsibleGuestRegistration.DocumentInformation != null ?
                                            GetDocumentInformationValidatedInCountry(leftResponsibleGuestRegistration.DocumentInformation) : leftGuestRegistration.DocumentInformation != null ? GetDocumentInformationValidatedInCountry(leftGuestRegistration.DocumentInformation) : null,
                             DocumentTypeId = leftResponsibleGuestRegistration != null && leftResponsibleGuestRegistration.DocumentTypeId.HasValue ?
                                             leftResponsibleGuestRegistration.DocumentTypeId.Value : leftGuestRegistration.DocumentTypeId.GetValueOrDefault(),
                             Phone = leftResponsibleGuestRegistration != null ?
                                             leftResponsibleGuestRegistration.PhoneNumber != null ? leftResponsibleGuestRegistration.PhoneNumber :
                                             leftResponsibleGuestRegistration.MobilePhoneNumber != null ? leftResponsibleGuestRegistration.MobilePhoneNumber : string.Empty : string.Empty,
                             Name = leftResponsibleGuestRegistration != null ? leftResponsibleGuestRegistration.FullName : leftGuestRegistration != null && leftGuestRegistration.FullName != null ? leftGuestRegistration.FullName : leftReservation.ContactName,
                             Email = leftResponsibleGuestRegistration != null ? leftResponsibleGuestRegistration.Email : leftGuestRegistration != null && leftGuestRegistration.Email != null ? leftGuestRegistration.Email : null,
                             Address = new IntegrationAddressDto
                             {
                                 Country = EnumHelper.GetEnumValue<CountryIsoCodeEnum>(_applicationUser.PropertyCountryCode, false) == CountryIsoCodeEnum.Europe ?
                                                    leftCountryTranslation != null ? leftCountryTranslation.Name : null :
                                                    leftCountry != null ? leftCountry.ThreeLetterIsoCode : null,
                                 Neighborhood = leftLocation != null ? leftLocation.Neighborhood : null,
                                 City = new IntegrationCityDTO
                                 {
                                     Code = leftCity != null ? leftCity.Id.ToString() : "0",
                                     Name = leftCityTranslation != null ? leftCityTranslation.Name : null
                                 },
                                 StreetName = leftLocation != null ? leftLocation.StreetName : null,
                                 StreetNumber = leftLocation != null ? leftLocation.StreetNumber : null,
                                 AdditionalInformation = leftLocation != null && leftLocation.AdditionalAddressDetails != null ? leftLocation.AdditionalAddressDetails.PadLeft(30) : null,
                                 District = leftState != null ? leftState.TwoLetterIsoCode : null,
                                 PostalCode = leftLocation != null ? leftLocation.PostalCode.ToString() : null
                             }
                         }).ToListAsync();
        }

        private string GetDocumentInformationValidatedInCountry(string documentInformation)
        {
            return EnumHelper.GetEnumValue<CountryIsoCodeEnum>(_applicationUser.PropertyCountryCode, true) == CountryIsoCodeEnum.Brasil ?
                Regex.Replace(documentInformation, "[^0-9]", "") : documentInformation;
        }

        public async Task<List<IntegrationClientDto>> GetProformaBorrowerInformation(Guid companyClientId)
        {
            return await GetBorrowerInformationByCompany(companyClientId);
        }
    }
}
