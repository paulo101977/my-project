﻿using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Thex.AspNetCore.Security;
using Thex.Common;
using Thex.Common.Dto.InvoiceIntegration;
using Thex.Common.Emails.AwsSes;
using Thex.Common.Enumerations;
using Thex.Common.Helpers;
using Thex.Common.Message;
using Thex.Domain.Entities;
using Thex.Domain.Interfaces.Factories;
using Thex.Domain.Interfaces.Integration;
using Thex.Domain.Interfaces.Repositories;
using Thex.Domain.JoinMap;
using Thex.Dto;
using Thex.Dto.BillingAccountClosure;
using Thex.Dto.BillingInvoice;
using Thex.Dto.Email;
using Thex.Dto.Integration;
using Thex.Dto.IntegrationPartner.IntegrationPartnerProperty;
using Thex.EntityFrameworkCore.Utils;
using Thex.Infra.Context;
using Thex.Infra.ReadInterfaces;
using Thex.Kernel;
using Thex.Preference.Dto;
using Tnf.EntityFrameworkCore;
using Tnf.EntityFrameworkCore.Repositories;
using Tnf.Localization;
using Tnf.Notifications;

namespace Thex.EntityFrameworkCore.Repositories
{
    public class InvoiceIntegrationBrazil : EfCoreRepositoryBase<ThexContext, BillingInvoice>, IInvoiceIntegrationBrazil
    {
        private string _country = "BRA";
        private string _apiKey = "gWqqYgPUIEimDHjlBOuKhA==";
        private readonly string _productionParam = "Producao";
        private readonly string _testParam = "Homologacao";
        private readonly IApplicationUser _applicationUser;
        private readonly IConfiguration _configuration;
        private readonly INotificationHandler _notificationHandler;
        private readonly IBillingAccountItemRepository _billingAccountItemRepository;
        private readonly IGuestRegistrationReadRepository _guestRegistrationReadRepository;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly ILocationReadRepository _locationReadRepository;
        private readonly IDocumentReadRepository _documentReadRepository;
        private readonly IPlasticBrandPropertyReadRepository _plasticBrandReadRepository;
        private readonly ILocalizationManager _localizationManager;
        private readonly IPropertyParameterReadRepository _propertyParameterReadRepository;
        private readonly IBillingAccountItemInvoiceHistoryRepository _billingAccountItemInvoiceHistoryRepository;
        private readonly IBillingAccountItemReadRepository _billingAccountItemReadRepository;
        private readonly IIgnoreAuditedEntity _ignoreAuditedEntity;
        private readonly IEmailFactory _emailFactory;
        private readonly IReservationReadRepository _reservationReadRepository;
        private readonly IBillingAccountReadRepository _billingAccountReadRepository;
        private readonly IBillingInvoiceReadRepository _billingInvoiceReadRepository;
        private readonly IInvoiceIntegrationClient _invoiceIntegrationClient;

        public InvoiceIntegrationBrazil(IDbContextProvider<ThexContext> dbContextProvider,
            IApplicationUser applicationUser,
            IConfiguration configuration,
            INotificationHandler notificationHandler,
            IGuestRegistrationReadRepository guestRegistrationReadRepository,
            IBillingAccountItemRepository billingAccountItemRepository,
            IHttpContextAccessor httpContextAccessor,
            ILocationReadRepository locationReadRepository,
            IDocumentReadRepository documentReadRepository,
            IPlasticBrandPropertyReadRepository plasticBrandReadRepository,
            ILocalizationManager localizationManager,
            IPropertyParameterReadRepository propertyParameterReadRepository,
            IBillingAccountItemInvoiceHistoryRepository billingAccountItemInvoiceHistoryRepository,
            IBillingAccountItemReadRepository billingAccountItemReadRepository,
            IIgnoreAuditedEntity ignoreAuditedEntity,
            IEmailFactory emailFactory,
            IBillingAccountReadRepository billingAccountReadRepository,
            IReservationReadRepository reservationReadRepository,
            IBillingInvoiceReadRepository billingInvoiceReadRepository,
            IInvoiceIntegrationClient invoiceIntegrationClient)
            : base(dbContextProvider)
        {
            _applicationUser = applicationUser;
            _configuration = configuration;
            _notificationHandler = notificationHandler;
            _billingAccountItemRepository = billingAccountItemRepository;
            _guestRegistrationReadRepository = guestRegistrationReadRepository;
            _httpContextAccessor = httpContextAccessor;
            _locationReadRepository = locationReadRepository;
            _documentReadRepository = documentReadRepository;
            _plasticBrandReadRepository = plasticBrandReadRepository;
            _localizationManager = localizationManager;
            _propertyParameterReadRepository = propertyParameterReadRepository;
            _billingAccountItemInvoiceHistoryRepository = billingAccountItemInvoiceHistoryRepository;
            _billingAccountItemReadRepository = billingAccountItemReadRepository;
            _ignoreAuditedEntity = ignoreAuditedEntity;
            _emailFactory = emailFactory;
            _billingAccountReadRepository = billingAccountReadRepository;
            _reservationReadRepository = reservationReadRepository;
            _billingInvoiceReadRepository = billingInvoiceReadRepository;
            _invoiceIntegrationClient = invoiceIntegrationClient;
        }

        /// <summary>
        /// Retorna o invoice de serviço através da lista de identificadores
        /// </summary>
        /// <param name="billingInvoiceIdList"></param>
        /// <returns></returns>
        public async Task<List<IntegrationBaseJoinMap>> GetBillingServiceInvoice(List<Guid> billingInvoiceIdList)
        {
            // Serviços
            var ret = await (from b in Context.BillingInvoices.AsNoTracking()
                             join bip in Context.BillingInvoiceProperties on b.BillingInvoicePropertyId equals bip.Id
                             join ba in Context.BillingAccounts on b.BillingAccountId equals ba.Id
                             join bai in Context.BillingAccountItems on b.Id equals bai.BillingInvoiceId
                             join bi in Context.BillingItems on bai.BillingItemId equals bi.Id
                             join bst in Context.BillingInvoicePropertySupportedTypes on
                             new { p1 = bi.Id, p2 = b.BillingInvoicePropertyId } equals
                             new { p1 = bst.BillingItemId, p2 = bst.BillingInvoicePropertyId } into
                             lj
                             from bstl in lj.DefaultIfEmpty()
                             join css in Context.CountrySubdvisionServices on bstl.CountrySubdvisionServiceId equals css.Id
                             where billingInvoiceIdList.Contains(b.Id) &&
                             bai.BillingInvoiceId.HasValue &&
                             (bai.BillingAccountItemTypeId == (int)BillingAccountItemTypeEnum.BillingAccountItemTypeDebit ||
                              bai.BillingAccountItemTypeId == (int)BillingAccountItemTypeEnum.BillingAccountItemTypePartial) &&
                             !bai.WasReversed &&
                             bi.BillingItemTypeId == (int)BillingItemTypeEnum.Service
                             select new IntegrationBaseJoinMap
                             {
                                 BillingInvoice = b,
                                 BillingInvoiceProperty = bip,
                                 BillingAccount = ba,
                                 BillingAccountItem = bai,
                                 BillingItem = bi,
                                 BillingInvoicePropertySupportedType = bstl,
                                 CountrySubdvisionService = css
                             }).ToListAsync();

            // Descontos
            if (ret != null)
            {
                var billingAcountId = ret.FirstOrDefault().BillingAccount.Id;

                ret.AddRange(await (from ba in Context.BillingAccounts
                                    join bai in Context.BillingAccountItems on ba.Id equals bai.BillingAccountId
                                    where ba.Id == billingAcountId &&
                                    (bai.BillingAccountItemTypeId == (int)BillingAccountItemTypeEnum.BillingAccountItemTypeDiscount) &&
                                    !bai.WasReversed
                                    select new IntegrationBaseJoinMap
                                    {
                                        BillingAccount = ba,
                                        BillingAccountItem = bai
                                    }).ToListAsync());
            }

            // Taxas
            if (ret != null)
                ret.AddRange(await (from b in Context.BillingInvoices.AsNoTracking()
                                    join bip in Context.BillingInvoiceProperties on b.BillingInvoicePropertyId equals bip.Id
                                    join ba in Context.BillingAccounts on b.BillingAccountId equals ba.Id
                                    join bai in Context.BillingAccountItems on b.Id equals bai.BillingInvoiceId
                                    join bi in Context.BillingItems on bai.BillingItemId equals bi.Id
                                    join bst in Context.BillingInvoicePropertySupportedTypes on
                                    new { p1 = bi.Id, p2 = b.BillingInvoicePropertyId } equals
                                    new { p1 = bst.BillingItemId, p2 = bst.BillingInvoicePropertyId } into
                                    lj
                                    from bstl in lj.DefaultIfEmpty()
                                    where billingInvoiceIdList.Contains(b.Id) &&
                                    bai.BillingInvoiceId.HasValue &&
                                    (bai.BillingAccountItemTypeId == (int)BillingAccountItemTypeEnum.BillingAccountItemTypeDebit ||
                                     bai.BillingAccountItemTypeId == (int)BillingAccountItemTypeEnum.BillingAccountItemTypePartial)
                                     && !bai.WasReversed &&
                                    bi.BillingItemTypeId == (int)BillingItemTypeEnum.Tax
                                    select new IntegrationBaseJoinMap
                                    {
                                        BillingInvoice = b,
                                        BillingInvoiceProperty = bip,
                                        BillingAccount = ba,
                                        BillingAccountItem = bai,
                                        BillingItem = bi,
                                        BillingInvoicePropertySupportedType = bstl
                                    }).ToListAsync());

            return ret;
        }

        private async Task<List<BillingAccountItem>> GetAccountPayments(List<Guid> billingAccountIdList)
        {
            return await (from b in Context.BillingAccountItems.AsNoTracking()
                          join bip in Context.BillingAccountItems on b.Id equals bip.BillingAccountItemParentId
                          where b.BillingAccountItemTypeId == (int)BillingAccountItemTypeEnum.BillingAccountItemTypeCredit &&
                          !b.WasReversed &&
                          billingAccountIdList.Contains(b.BillingAccountId)
                          select b).ToListAsync();
        }

        /// <summary>
        /// Retorna o invoice de produtos através da lista de identificadores
        /// </summary>
        /// <param name="billingInvoiceIdList"></param>
        /// <returns></returns>
        public async Task<List<IntegrationBaseJoinMap>> GetBillingProductInvoice(List<Guid> billingInvoiceIdList)
        {
            return await (from b in Context.BillingInvoices.AsNoTracking()
                          join bip in Context.BillingInvoiceProperties on b.BillingInvoicePropertyId equals bip.Id
                          join ba in Context.BillingAccounts on b.BillingAccountId equals ba.Id
                          join pro in Context.Properties on ba.PropertyId equals pro.Id
                          join bai in Context.BillingAccountItems on b.Id equals bai.BillingInvoiceId
                          join bi in Context.BillingItems on bai.BillingItemId equals bi.Id
                          join bst in Context.BillingInvoicePropertySupportedTypes on
                          new { p1 = bi.Id, p2 = b.BillingInvoicePropertyId } equals
                          new { p1 = bst.BillingItemId, p2 = bst.BillingInvoicePropertyId } into
                          lj
                          from bstl in lj.DefaultIfEmpty()
                          where billingInvoiceIdList.Contains(b.Id) &&
                          bai.BillingInvoiceId.HasValue &&
                          bai.BillingAccountItemTypeId == (int)BillingAccountItemTypeEnum.BillingAccountItemTypeDebit &&
                          !bai.WasReversed &&
                          (bi.BillingItemTypeId == (int)BillingItemTypeEnum.PointOfSale ||
                          bi.BillingItemTypeId == (int)BillingItemTypeEnum.Tax)
                          select new IntegrationBaseJoinMap
                          {
                              BillingInvoice = b,
                              BillingInvoiceProperty = bip,
                              BillingAccount = ba,
                              BillingAccountItem = bai,
                              BillingItem = bi,
                              BillingInvoicePropertySupportedType = bstl
                          }).ToListAsync();
        }

        private async Task SendBillingInvoiceAsync(InvoiceIntegrationResponseDto invoiceResponsetDto, int invoiceType)
        {
            var taxRuleEndpoint = new Uri(string.Concat(_configuration.GetValue<string>("TaxRuleEndpoint"), $"/api/invoice/status/", invoiceType.ToString()));

            using (var httpClient = new HttpClient())
            {
                httpClient.DefaultRequestHeaders.Add("x-token", _apiKey);
                var response = await httpClient.PostAsync(taxRuleEndpoint, new StringContent(JsonConvert.SerializeObject(invoiceResponsetDto), Encoding.UTF8, "application/json"));

                if (response.IsSuccessStatusCode)
                    return;
            }
        }

        private void SendMailBillingInvoiceIntegrationError(string email, string errorMessage)
        {
            var emailDto = new EmailIntegrationDto()
            {
                Title = _localizationManager.GetString(AppConsts.LocalizationSourceName, EmailEnum.EmailSubjectFiscalDocument.ToString()),
                Body = _localizationManager.GetString(AppConsts.LocalizationSourceName, EmailEnum.EmailBodyFiscalDocument.ToString()),
                ErrorMessage = errorMessage
            };

            var host = _configuration.GetValue<string>("AwsEmailHost");
            var port = Convert.ToInt32(_configuration.GetValue<string>("AwsEmailPort"));
            var userName = _configuration.GetValue<string>("AwsEmailUserName");
            var passsword = _configuration.GetValue<string>("AwsEmailPassword");

            var subject = emailDto.Title;
            var from = _configuration.GetValue<string>("EmailBilling");
            var body = _emailFactory.GenerateEmail(EmailTemplateEnum.EmailIntegration, emailDto);
            var to = email;

            // Prepare Email Message
            var message = EmailMessageBuilder
                            .Init()
                            .AddSubject(subject)
                            .AddFrom(from)
                            .AddBody(body)
                            .AddTo(to)
                            .Build();

            // Send Email Message
            IAwsEmailSender sender = new AwsEmailSender(new AwsEmailSettings(host, port, userName, passsword));
            sender.Send(message, true);
        }

        public async Task SendInvoiceIntegratioResponse(BillingInvoiceIntegrationResponseDto response, InvoiceIntegrationResponseDto invoiceResponse)
        {
            if (response.Success)
                await SendBillingInvoiceAsync(invoiceResponse, response.Type);

            if (!string.IsNullOrEmpty(response.EmailAlert))
                SendMailBillingInvoiceIntegrationError(response.EmailAlert, invoiceResponse.Reason);
        }


        public async Task<NotificationResponseDto> GenerateInvoice(List<BillingAccountClosureInvoiceDto> billingInvoiceList, BillingInvoiceTypeEnum invoiceType)
        {
            var taxRuleEndpoint = new Uri(string.Concat(_configuration.GetValue<string>("TaxRuleEndpoint"), $"/api/invoice"));

            List<IntegrationBaseJoinMap> integrationBaseList = null;

            switch (invoiceType)
            {
                case BillingInvoiceTypeEnum.NFSE:
                    {
                        integrationBaseList = await GetBillingServiceInvoice(billingInvoiceList.Where(x => x.Type == BillingInvoiceTypeEnum.NFSE)
                                                                                               .Select(x => Guid.Parse(x.InvoiceId)).ToList());
                    }
                    break;
                case BillingInvoiceTypeEnum.NFCE:
                case BillingInvoiceTypeEnum.SAT:
                    {
                        integrationBaseList = await GetBillingProductInvoice(billingInvoiceList.Where(x => x.Type == BillingInvoiceTypeEnum.NFCE ||
                                                                                                           x.Type == BillingInvoiceTypeEnum.SAT)
                                                                                               .Select(x => Guid.Parse(x.InvoiceId)).ToList());

                        break;
                    }
                case BillingInvoiceTypeEnum.NFE:
                case BillingInvoiceTypeEnum.Factura:
                case BillingInvoiceTypeEnum.CreditNoteTotal:
                case BillingInvoiceTypeEnum.CreditNotePartial:
                case BillingInvoiceTypeEnum.DebitNote:
                default:
                    _notificationHandler.Raise(_notificationHandler
                                    .DefaultBuilder
                                    .WithMessage(AppConsts.LocalizationSourceName, BillingAccountItemEnum.Error.BillingAccountInvoiceNFeioComunicationError)
                                    .Build());
                    break;
            }

            using (var httpClient = new HttpClient())
            {
                var token = _httpContextAccessor.HttpContext.Request.Headers.FirstOrDefault(x => x.Key.Equals("Authorization")).Value.ToString();
                httpClient.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", token);

                var responseMessage = await httpClient.PostAsync(taxRuleEndpoint,
                                                new StringContent(await ProccessInvoiceList(integrationBaseList, invoiceType), Encoding.UTF8, "application/json"));

                if (responseMessage.IsSuccessStatusCode)
                {
                    UpdateEmissionDateBillingInvoiceList(integrationBaseList, invoiceType);

                    var message = await responseMessage.Content.ReadAsStringAsync();

                    if (!string.IsNullOrEmpty(message))
                    {
                        if (invoiceType == BillingInvoiceTypeEnum.NFCE ||
                            invoiceType == BillingInvoiceTypeEnum.SAT)
                        {
                            var invoiceResponse = JsonConvert.DeserializeObject<InvoiceResponseDto>(message);

                            var responseDto = GenerateInvoiceResponse(invoiceResponse);

                            var ret = await UpdateWithSuccess(responseDto);

                            await SendInvoiceIntegratioResponse(ret, responseDto);
                        }
                    }
                }
                else
                {
                    var message = await responseMessage.Content.ReadAsStringAsync();

                    if (!string.IsNullOrEmpty(message))
                    {
                        var taxRuleNotification = JsonConvert.DeserializeObject<InvoiceIntegrationNotification>(message);

                        if (taxRuleNotification != null)
                        {
                            // TODO: Verificar correção nas atualizações do TNF
                            // Enviar erro genérico/dinâmico para o Notification
                            foreach (var notify in taxRuleNotification.Details)
                            {
                                return new NotificationResponseDto(notify.Message, notify.DetailedMessage,
                                                                   integrationBaseList.Select(x => x.BillingInvoiceProperty.Id).FirstOrDefault());
                            }
                        }
                        else
                        {
                            _notificationHandler.Raise(_notificationHandler
                                                .DefaultBuilder
                                                .WithMessage(AppConsts.LocalizationSourceName, BillingAccountItemEnum.Error.BillingAccountInvoiceNFeioComunicationError)
                                                .Build());
                        }
                    }
                }
            }

            return NotificationResponseDto.NullInstance;
        }

        private InvoiceIntegrationResponseDto GenerateInvoiceResponse(InvoiceResponseDto responseDto)
        {
            return new InvoiceIntegrationResponseDto()
            {
                EmissionDate = responseDto.EmissionDate.ToLongDateString(),
                ExternalId = responseDto.ExternalId,
                InternalId = responseDto.ExternalId,
                InvoiceNumber = responseDto.InvoiceNumber,
                Number = responseDto.InvoiceNumber,
                RpsNumber = responseDto.InvoiceNumber,
                RpsSerial = responseDto.Series,
                Status = responseDto.Status,
                UrlPdf = responseDto.UrlPdf,
                UrlXml = responseDto.UrlXml,
                VerificationCode = ""
            };
        }

        public async Task<bool> CancelInvoice(Guid billingInvoiceId, int billingInvoiceTypeId)
        {
            var cancelInvoiceEndpoint = new Uri(string.Concat(_configuration.GetValue<string>("TaxRuleEndpoint"), $"/api/invoice/cancel/{billingInvoiceTypeId}"));

            using (var httpClient = new HttpClient())
            {
                #region TODO compartilhar código

                var propertyLocation = _locationReadRepository.GetLocationDtoListByPropertyUId(_applicationUser.PropertyId.TryParseToInt32());
                var invoiceIntegration = new InvoiceIntegrationDto
                {
                    PropertyDistrict = _applicationUser.PropertyDistrictCode,
                    PropertyUUId = propertyLocation.OwnerId.ToString(),
                    TenantId = _applicationUser.TenantId.ToString(),
                    ExternalId = billingInvoiceId.ToString(),
                    ExternalTypeId = billingInvoiceTypeId.ToString(),
                    Environment = GetEnviromentInvoice(_configuration)
                };

                GeneratePartnertIntegrationDetails(ref invoiceIntegration, _applicationUser.PropertyId.TryParseToInt32());

                #endregion

                httpClient.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", GetCurrentToken());

                var responseMessage = await httpClient.PostAsync(cancelInvoiceEndpoint,
                                                new StringContent(JsonConvert.SerializeObject(invoiceIntegration), Encoding.UTF8, "application/json"));

                if (!responseMessage.IsSuccessStatusCode)
                    await GenerateTaxRulesCancelErrorResponse(responseMessage);

                return responseMessage.IsSuccessStatusCode;
            }
        }

        private async Task GenerateTaxRulesCancelErrorResponse(HttpResponseMessage responseMessage)
        {
            var message = await responseMessage.Content.ReadAsStringAsync();

            if (!string.IsNullOrEmpty(message))
            {
                var taxRuleNotification = JsonConvert.DeserializeObject<InvoiceIntegrationNotification>(message);

                if (taxRuleNotification != null)
                {
                    // TODO: Verificar correção nas atualizações do TNF
                    // Enviar erro genérico/dinâmico para o Notification
                    foreach (var notify in taxRuleNotification.Details)
                    {
                        taxRuleNotification.Message = notify.DetailedMessage;
                        taxRuleNotification.DetailedMessage = notify.DetailedMessage;
                    }

                    var notification = JsonConvert.DeserializeObject<NotificationEvent>(JsonConvert.SerializeObject(taxRuleNotification));
                    _notificationHandler.Raise(notification);
                }
            }
            else
            {
                _notificationHandler.Raise(_notificationHandler
                                .DefaultBuilder
                                .WithMessage(AppConsts.LocalizationSourceName, BillingAccountItemEnum.Error.BillingAccountInvoiceNFeioComunicationError)
                                .Build());
            }
        }

        private string GetCurrentToken()
        {
            return _httpContextAccessor.HttpContext.Request.Headers.FirstOrDefault(x => x.Key.Equals("Authorization")).Value.ToString();
        }

        private void UpdateEmissionDateBillingInvoiceList(List<IntegrationBaseJoinMap> billingInvoiceList, BillingInvoiceTypeEnum invoiceType)
        {
            var invoiceIdList = billingInvoiceList.Select(e => e.BillingInvoice.Id).ToList();
            var entityAttachedList = Context.Set<BillingInvoice>().Local.Where(e => invoiceIdList.Contains(e.Id));

            foreach (var billingInvoiceId in billingInvoiceList.Select(e => e.BillingInvoice.Id).Distinct().ToList())
            {
                var entityAttached = entityAttachedList.FirstOrDefault(e => e.Id == billingInvoiceId);

                if (entityAttached == null)
                {
                    entityAttached = new BillingInvoice();
                    entityAttached.BillingInvoiceTypeId = (int)invoiceType;
                    entityAttached.SetIntegrationPending(billingInvoiceId, _applicationUser.UserUid);
                    Context.BillingInvoices.AttachRange(entityAttached);
                }
                else
                {
                    entityAttached.BillingInvoiceTypeId = (int)invoiceType;
                    entityAttached.SetIntegrationPending(billingInvoiceId, _applicationUser.UserUid);
                }
            }

            Context.SaveChanges();
        }

        private async Task<string> ProccessInvoiceList(List<IntegrationBaseJoinMap> integrationBaseJoinMapList,
                                                       BillingInvoiceTypeEnum invoiceType)
        {
            var propertyLocation = _locationReadRepository.GetLocationDtoListByPropertyUId(_applicationUser.PropertyId.TryParseToInt32());

            var invoiceList = new List<InvoiceIntegrationDto>();

            IEnumerable<IntegrationBaseJoinMap> qry = null;

            if (invoiceType == BillingInvoiceTypeEnum.NFCE || invoiceType == BillingInvoiceTypeEnum.SAT)
                qry = integrationBaseJoinMapList.Where(x => x.BillingItem.BillingItemTypeId == (int)BillingItemTypeEnum.PointOfSale);
            else if (invoiceType == BillingInvoiceTypeEnum.NFSE)
                qry = integrationBaseJoinMapList.Where(x => x.BillingAccountItem.BillingAccountItemTypeId != (int)BillingAccountItemTypeEnum.BillingAccountItemTypeDiscount &&
                                                            x.BillingItem.BillingItemTypeId == (int)BillingItemTypeEnum.Service);
            else
                qry = integrationBaseJoinMapList;

            var billingInvoiceList = qry
                .Where(i => i.BillingInvoice != null)
                .Select(e => new
                {
                    e.BillingInvoice.Id,
                    e.BillingInvoice.BillingInvoiceTypeId,
                    e.BillingInvoice.BillingInvoiceSeries,
                    e.BillingInvoice.BillingInvoiceNumber,
                    e.BillingInvoicePropertySupportedType.CountrySubdvisionServiceId,
                    e.BillingInvoice.Property
                }).DistinctBy(b => b.CountrySubdvisionServiceId).ToList();

            foreach (var invoice in billingInvoiceList)
            {
                var billingAccountItemList = _billingAccountItemRepository.GetListByInvoiceId(invoice.Id);
                if (billingAccountItemList.Any())
                {
                    var billingAccount = integrationBaseJoinMapList.FirstOrDefault().BillingAccount;

                    string description = CreateInvoiceDescription(billingAccountItemList);
                    description += GetInvoiceDescriptionWithTaxList(billingAccountItemList);
                    description += GetDiscountDescription(integrationBaseJoinMapList);

                    var invoiceIntegration = new InvoiceIntegrationDto
                    {
                        PropertyDistrict = _applicationUser.PropertyDistrictCode,
                        PropertyUUId = propertyLocation.OwnerId.ToString(),
                        TenantId = _applicationUser.TenantId.ToString(),
                        ExternalId = invoice.Id.ToString(),
                        Environment = GetEnviromentInvoice(_configuration),
                        Client = _invoiceIntegrationClient.GetBorrowerInformation(billingAccount, invoiceType).Result.FirstOrDefault()
                    };

                    if (invoiceIntegration.Client != null && invoiceIntegration.Client.Address != null &&
                        invoiceIntegration.Client.Address.Country != null && invoiceIntegration.Client.Address.Country == _country &&
                        !ValidatePostalCode(invoiceIntegration.Client.Address.PostalCode))
                        return await Task.FromResult(string.Empty);

                    if (invoice.BillingInvoiceTypeId == (int)BillingInvoiceTypeEnum.NFCE || invoice.BillingInvoiceTypeId == (int)BillingInvoiceTypeEnum.SAT)
                        invoiceIntegration.Number = Convert.ToInt32(invoice.BillingInvoiceNumber);
                    invoiceIntegration.SerialNumber = invoice.BillingInvoiceSeries;

                    GeneratePartnertIntegrationDetails(ref invoiceIntegration, _applicationUser.PropertyId.TryParseToInt32());

                    if (_notificationHandler.HasNotification())
                        return await Task.FromResult(string.Empty);

                    GenerateServiceInvoiceDetails(invoiceIntegration.ServiceInvoice, invoice.Id, integrationBaseJoinMapList, description);
                    GenerateProductInvoiceDetails(invoiceIntegration.ProductInvoice, invoice.Id, integrationBaseJoinMapList);

                    invoiceList.Add(invoiceIntegration);
                }
            }

            return await Task.FromResult(JsonConvert.SerializeObject(invoiceList,
                            Formatting.None,
                            new JsonSerializerSettings
                            {
                                NullValueHandling = NullValueHandling.Ignore
                            }));
        }

        private bool ValidatePostalCode(string postalCode)
        {
            var ret = false;

            try
            {
                if (string.IsNullOrEmpty(postalCode))
                    ret = false;

                postalCode = postalCode.Replace(".", "").Replace("-", "").Replace(" ", "");

                if (postalCode.Length == 8)
                {
                    postalCode = postalCode.Substring(0, 5) + "-" + postalCode.Substring(5, 3);
                }

                ret = Regex.IsMatch(postalCode, ("[0-9]{5}-[0-9]{3}"));

                if (!ret)
                    _notificationHandler.Raise(_notificationHandler
                        .DefaultBuilder
                        .WithMessage(AppConsts.LocalizationSourceName, Location.EntityError.LocationMustHavePostalCode)
                        .WithDetailedMessage(AppConsts.LocalizationSourceName, Location.EntityError.LocationMustHavePostalCode)
                        .Build());
            }
            catch
            {
                ret = false;
            }

            return ret;
        }

        private void GeneratePartnertIntegrationDetails(ref InvoiceIntegrationDto invoiceIntegration, int propertyId)
        {
            var propertyWithDetails = _propertyParameterReadRepository.GetDtoByIdAsync(propertyId).Result;

            if (propertyWithDetails == null || propertyWithDetails.IntegrationPartnerPropertyList == null || propertyWithDetails.IntegrationPartnerPropertyList.Count == 0)
                _notificationHandler.Raise(_notificationHandler
                                       .DefaultBuilder
                                       .WithMessage(AppConsts.LocalizationSourceName, BillingAccountItemEnum.Error.BillingAccountInvoiceNFeioComunicationError)
                                       .Build());

            var integrationProperty = propertyWithDetails.IntegrationPartnerPropertyList.FirstOrDefault(x => x.PropertyId == propertyId &&
                                                                                                             x.IntegrationPartnerType == (int)IntegrationPartnerTypeEnum.ENotas);
            if (integrationProperty == null)
                _notificationHandler.Raise(_notificationHandler
                                   .DefaultBuilder
                                   .WithMessage(AppConsts.LocalizationSourceName, BillingAccountItemEnum.Error.BillingAccountInvoiceNFeioComunicationError)
                                   .Build());

            GeneratePartnertIntegrationByEnviroment(ref invoiceIntegration, integrationProperty);
        }

        private void GeneratePartnertIntegrationByEnviroment(ref InvoiceIntegrationDto invoiceIntegration, IntegrationPartnerPropertyDto integrationProperty)
        {
            switch (EnumHelper.GetEnumValue<EnvironmentEnum>(_configuration.GetValue<string>("CloudEnvironment"), true))
            {
                case EnvironmentEnum.Production:
                    {
                        invoiceIntegration.TokenClient = HashManager.GenerateMD5Crypto(integrationProperty.TokenClient.Trim());
                        invoiceIntegration.IntegrationCode = integrationProperty.IntegrationCode.Trim();
                        break;
                    }
                case EnvironmentEnum.Development:
                case EnvironmentEnum.BugFix:
                case EnvironmentEnum.Test:
                case EnvironmentEnum.UAT:
                case EnvironmentEnum.Staging:
                default:
                    {
                        invoiceIntegration.TokenClient = _configuration.GetValue<string>("EnotasToken");
                        invoiceIntegration.IntegrationCode = _configuration.GetValue<string>("EnotasIntegrationCode");
                        break;
                    }
            }
        }

        private decimal GetRateFromInvoice(List<IntegrationBaseJoinMap> integrationBaseJoinMapList, Guid invoiceId)
        {
            return integrationBaseJoinMapList.Where(e => e.BillingAccountItem.BillingInvoiceId == invoiceId &&
                         e.BillingItem.BillingItemTypeId == (int)BillingItemTypeEnum.Tax)
               .Select(e => e.BillingAccountItem)
               .Sum(x => x.Amount);
        }

        private void GenerateProductInvoiceDetails(List<IntegrationProductInvoiceDTO> productInvoice, Guid invoiceId,
                                                   List<IntegrationBaseJoinMap> integrationBaseJoinMapList)
        {
            var billingAccountItem = integrationBaseJoinMapList.Where(e => e.BillingAccountItem.BillingInvoiceId == invoiceId &&
                                                                           e.BillingItem.BillingItemTypeId == (int)BillingItemTypeEnum.PointOfSale)
                                                               .Select(e => e.BillingAccountItem)
                                                               .ToList();

            foreach (var invoiceDetails in billingAccountItem.Where(x => x.IntegrationDetail != null))
            {
                var productsList = JsonConvert.DeserializeObject<List<MostSelectedDetailsDto>>(invoiceDetails.IntegrationDetail);

                if (productsList != null && productsList.Count > 0)
                {
                    // Rateia o total de taxa por todos os itens vendidos
                    var rate = GetRateFromInvoice(integrationBaseJoinMapList, (invoiceDetails.BillingInvoiceId != null ? invoiceDetails.BillingInvoiceId.Value : Guid.Empty)) / productsList.Count;

                    foreach (var product in productsList)
                    {
                        var integrationInvoice = new IntegrationProductInvoiceDTO()
                        {
                            Barcode = product.BarCode,
                            Ncm = product.NcmCode,
                            Code = product.Code,
                            Quantity = product.Quantity,
                            Amount = (product.Amount / product.Quantity).TruncateDecimal(2),
                            Description = product.Description,
                            Rate = rate * -1,
                            PaymentDetails = GenerateProductInvoicePaymentDetails(integrationBaseJoinMapList.Select(e => e.BillingAccountItem).Sum(x => x.Amount), 0m)
                        };

                        productInvoice.Add(integrationInvoice);
                    }
                }
            }
        }

        private List<IntegrationProductInvoicePaymentsDto> GenerateProductInvoicePaymentDetails(decimal amount, decimal rate)
        {
            var ret = new List<IntegrationProductInvoicePaymentsDto>();

            ret.Add(new IntegrationProductInvoicePaymentsDto()
            {
                Amount = amount * -1,
                PaymentDescription = "Outros",
                CreditDetails = null
            });

            return ret;
        }

        private IntegrationProductInvoicePaymentCreditDetailsDto GenerateCreditPaymentDetails(int billingItemId, string nsuCode)
        {
            IntegrationProductInvoicePaymentCreditDetailsDto ret = null;

            var billingItem = (from billing in Context.BillingItems.AsNoTracking()
                               where billing.Id == billingItemId
                               select billing).SingleOrDefault();

            if (billingItem != null && billingItem.PaymentTypeId != null && billingItem.PaymentTypeId == (int)PaymentTypeEnum.CreditCard)
                ret = new IntegrationProductInvoicePaymentCreditDetailsDto()
                {
                    Document = _documentReadRepository.GetDocumentForPlasticBrand(billingItemId, DocumentTypeEnum.BrLegalPersonCNPJ),
                    NSU = nsuCode,
                    PlasticBrand = _plasticBrandReadRepository.GetPlasticBrandDetails(billingItem.PlasticBrandPropertyId).PlasticBrandName
                };

            return ret;
        }

        private string GeneratePaymentDescription(int billingItemId)
        {
            var paymentTypeId = (from billing in Context.BillingItems.AsNoTracking()
                                 join paymentType in Context.PaymentTypes on billing.PaymentTypeId equals paymentType.Id
                                 where billing.Id == billingItemId
                                 select paymentType.Id).SingleOrDefault();

            return _localizationManager.GetString(AppConsts.LocalizationSourceName, ((PaymentTypeEnum)paymentTypeId).ToString());
        }

        private string GetEnviromentInvoice(IConfiguration configuration)
        {
            switch (EnumHelper.GetEnumValue<EnvironmentEnum>(configuration.GetValue<string>("CloudEnvironment"), true))
            {
                case EnvironmentEnum.Production: return _productionParam;
                case EnvironmentEnum.Development:
                case EnvironmentEnum.BugFix:
                case EnvironmentEnum.Test:
                case EnvironmentEnum.UAT:
                case EnvironmentEnum.Staging:
                default: return _testParam;
            }
        }

        private void GenerateServiceInvoiceDetails(List<IntegrationServiceInvoiceDTO> serviceInvoice, Guid invoiceId,
                                                   List<IntegrationBaseJoinMap> integrationBaseJoinMapList, string description)
        {
            var nbs = integrationBaseJoinMapList.Where(e => e.BillingInvoice.Id == invoiceId && e.CountrySubdvisionService != null)
                                                             .Select(e => e.CountrySubdvisionService.Code)
                                                             .FirstOrDefault();

            if (nbs != null)
            {
                var amount = (integrationBaseJoinMapList.Select(e => e.BillingAccountItem).Where(x => x.BillingAccountItemTypeId != (int)BillingAccountItemTypeEnum.BillingAccountItemTypeCredit &&
                                                                                                      x.BillingAccountItemTypeId != (int)BillingAccountItemTypeEnum.BillingAccountItemTypeDiscount)
                                                                          .Sum(x => x.Amount) * -1);

                var discount = (integrationBaseJoinMapList.Select(e => e.BillingAccountItem).Where(x => x.BillingAccountItemTypeId == (int)BillingAccountItemTypeEnum.BillingAccountItemTypeDiscount)
                                                                                          .Sum(x => x.Amount));

                serviceInvoice.Add(new IntegrationServiceInvoiceDTO()
                {
                    Nbs = nbs, // TODO: Esse codigo não é o NBS - criar o campo correto após definição
                    Amount = (amount - discount),
                    Description = description + GenerateCompleteServiceDescription(integrationBaseJoinMapList, invoiceId)
                });
            }
        }

        private string GenerateCompleteServiceDescription(List<IntegrationBaseJoinMap> integrationBaseJoinMapList, Guid invoiceId)
        {
            var detailDescription = string.Empty;
            var reservationId = integrationBaseJoinMapList.Where(e => e.BillingInvoice.Id == invoiceId && e.CountrySubdvisionService != null)
                                                        .Select(e => e.BillingAccount.ReservationId)
                                                        .FirstOrDefault();

            if (reservationId != null && reservationId > 0)
            {
                var details = _reservationReadRepository.GetReservationDetail(new Dto.Reservation.SearchReservationDto()
                {
                    ReservationId = reservationId
                }, _applicationUser.PropertyId.TryParseToInt32());

                if (details != null)
                {
                    detailDescription = $"Localizador: {details.ReservationCode} Tipo UH: {details.ReceivedRoomTypeName} UH: {details.RoomNumber}{Environment.NewLine}";
                    detailDescription += $"Checkin: {details.ArrivalDate} Checkout: {details.DepartureDate}{Environment.NewLine}";
                    detailDescription += $"Adultos: {details.AdultsCount} Crianças: {details.ChildCount}{Environment.NewLine}";

                    if (!string.IsNullOrEmpty(details.GuestName))
                        detailDescription += $"Hóspede: {details.GuestName} E-Mail:{details.GuestEmail}{Environment.NewLine}";

                    if (!string.IsNullOrEmpty(details.GroupName))
                        detailDescription += $"Grupo: {details.GroupName}{Environment.NewLine}";

                    if (!string.IsNullOrEmpty(details.CompanyClientName))
                        detailDescription += $"Cliente: {details.CompanyClientName}{Environment.NewLine}";

                    var paymentsList = _billingAccountItemReadRepository.GetAllCreditWithInvoiceByBillingAccountId(integrationBaseJoinMapList.Where(e => e.BillingInvoice.Id == invoiceId && e.CountrySubdvisionService != null)
                                                                                                                  .Select(e => e.BillingAccount.Id)
                                                                                                                  .FirstOrDefault())
                                                                                                                  .Where(p => !p.WasReversed).ToList();
                    if (paymentsList != null && paymentsList.Count > 0)
                    {
                        detailDescription += $"{Environment.NewLine}Formas de pagamento: {Environment.NewLine}";

                        var amount = 0m;

                        if (paymentsList.Count == 1)
                            detailDescription += $"Nome: {paymentsList.FirstOrDefault().BillingItem.BillingItemName} Valor: {(integrationBaseJoinMapList.Select(e => e.BillingAccountItem).Sum(x => x.Amount) * -1).ToString("F2")}{Environment.NewLine}";
                        else
                            foreach (var payments in paymentsList)
                            {
                                // encontra o percentual do pagamento
                                var percent = payments.Amount / paymentsList.Sum(x => x.Amount);

                                // aplica sobre o total da nota
                                amount = (integrationBaseJoinMapList.Select(e => e.BillingAccountItem).Sum(x => x.Amount) * -1) * percent;
                                detailDescription += $"Nome: {payments.BillingItem.BillingItemName} Valor: {amount.ToString("F2")}{Environment.NewLine}";
                            }
                    }
                }
            }

            return detailDescription;
        }

        private string CreateInvoiceDescription(IList<BillingAccountItem> billingAccountItemList)
        {
            string description = "";

            var billingAccountItemListWithouTax = billingAccountItemList.Where(e => e.BillingItem.BillingItemTypeId == (int)BillingItemTypeEnum.Service
                || e.BillingItem.BillingItemTypeId == (int)BillingItemTypeEnum.PointOfSale);

            if (billingAccountItemListWithouTax.Any())
            {
                foreach (var billingAccountItem in billingAccountItemListWithouTax.DistinctBy(b => b.BillingItemId))
                {
                    description += billingAccountItem.BillingItem.BillingItemName + ": " + billingAccountItem.CurrencySymbol;

                    description += Math.Round((billingAccountItemList
                        .Where(e => !e.IsDeleted && e.BillingItemId == billingAccountItem.BillingItemId).Sum(e => e.Amount * -1)), 2).ToString()
                        + "\n";
                }
            }
            return description;
        }

        private string GetDiscountDescription(List<IntegrationBaseJoinMap> integrationBaseJoinMapList)
        {
            string description = "";

            var discountList = (integrationBaseJoinMapList.Select(e => e.BillingAccountItem).Where(x => x.BillingAccountItemTypeId == (int)BillingAccountItemTypeEnum.BillingAccountItemTypeDiscount));

            if (discountList.Any())
            {
                description += "Descontos: " + Math.Round((integrationBaseJoinMapList.Select(e => e.BillingAccountItem)
                                                                                     .Where(x => x.BillingAccountItemTypeId == (int)BillingAccountItemTypeEnum.BillingAccountItemTypeDiscount))
                                                                                     .Sum(x => x.Amount), 2).ToString()
                    + "\n";
            }

            return description;
        }

        private string GetInvoiceDescriptionWithTaxList(IList<BillingAccountItem> billingAccountItemList)
        {
            string description = "";
            var taxList = billingAccountItemList.Where(b => b.BillingItem.BillingItemTypeId == (int)BillingItemTypeEnum.Tax);

            if (taxList.Any())
            {
                foreach (var tax in taxList.DistinctBy(b => b.BillingItemId))
                {
                    description += tax.BillingItem.BillingItemName + ": " + tax.CurrencySymbol;

                    description += Math.Round(billingAccountItemList.Where(e => e.BillingItemId == tax.BillingItemId
                        && e.BillingItem.BillingItemTypeId == (int)BillingItemTypeEnum.Tax)
                        .Sum(t => t.Amount * -1), 2).ToString()
                        + "\n";
                }
            }
            return description;
        }

        private decimal GetInvoiceAmount(IList<BillingAccountItem> billingAccountItemList, Guid countrySubdvisionServiceId)
        {
            decimal invoiceAmount = 0;
            var billingAccountItemParentList = billingAccountItemList.Where(b => b.BillingAccountItemParentId == null);
            foreach (var billingAccountItem in billingAccountItemParentList)
            {
                invoiceAmount += (billingAccountItem.Amount * -1);

                var taxList = billingAccountItemList.Where(b => b.BillingAccountItemParentId == billingAccountItem.Id && b.BillingItem.BillingItemTypeId == (int)BillingItemTypeEnum.Tax);
                foreach (var tax in taxList)
                {
                    invoiceAmount += (tax.Amount * -1);
                }
            }
            return invoiceAmount;
        }

        private decimal GetDeductionsAmount(IList<BillingAccountItem> billingAccountItemList, Guid countrySubdvisionServiceId)
        {
            decimal deductionsAmount = 0;
            var billingAccountItemParentList = billingAccountItemList.Where(b => b.BillingAccountItemParentId == null);
            foreach (var billingAccountItem in billingAccountItemParentList)
            {
                // O valor da dedução é a soma dos itens que não têm tem o countrySubdvisionServiceId
                if (!billingAccountItem.BillingItem.BillingInvoicePropertySupportedTypeList.Any(b => b.CountrySubdvisionServiceId == countrySubdvisionServiceId))
                {
                    deductionsAmount += (billingAccountItem.Amount * -1);

                    var taxList = billingAccountItemList.Where(b => b.BillingAccountItemParentId == billingAccountItem.Id && b.BillingItem.BillingItemTypeId == (int)BillingItemTypeEnum.Tax);
                    foreach (var tax in taxList)
                    {
                        deductionsAmount += (tax.Amount * -1);
                    }
                }
            }
            return deductionsAmount;
        }

        public async Task<BillingInvoiceIntegrationResponseDto> UpdateWithError(InvoiceIntegrationResponseDto dto)
        {
            if (string.IsNullOrEmpty(dto.ExternalId))
                NotifyInvalidIntegrationResponse();

            var invoiceId = Guid.Parse(dto.ExternalId);

            var billingInvoiceEntity = Context
                                       .BillingInvoices
                                       .FirstOrDefault(e => e.Id == invoiceId);

            if (billingInvoiceEntity == null)
            {
                NotifyNullBillingInvoice();
                return new BillingInvoiceIntegrationResponseDto { Success = false };
            }

            if (billingInvoiceEntity != null)
                billingInvoiceEntity.SetIntegrationError(dto.Reason, dto.InternalId);

            await Context.SaveChangesAsync();

            return new BillingInvoiceIntegrationResponseDto() { Success = true, EmailAlert = string.Empty, Type = billingInvoiceEntity.BillingInvoiceTypeId.Value };
        }

        private void NotifyNullBillingInvoice()
        {
            _notificationHandler.Raise(_notificationHandler
                .DefaultBuilder
                .WithMessage(AppConsts.LocalizationSourceName, CommonsEnum.Error.NullOrEmptyObject)
                .Build());
        }

        private void NotifyInvalidIntegrationResponse()
        {
            _notificationHandler.Raise(_notificationHandler
                .DefaultBuilder
                .WithMessage(AppConsts.LocalizationSourceName, BillingInvoice.EntityMessage.BillingInvoiceIntegrationInvalidIntegrationResponse)
                .Build());
        }

        public async Task<BillingInvoiceIntegrationResponseDto> UpdateWithSuccess(InvoiceIntegrationResponseDto dto)
        {
            if (string.IsNullOrEmpty(dto.ExternalId))
                NotifyInvalidIntegrationResponse();

            var invoiceId = Guid.Parse(dto.ExternalId);
            var billingInvoiceEntity = Context.BillingInvoices.FirstOrDefault(e => e.Id == invoiceId);

            if (billingInvoiceEntity == null)
            {
                NotifyNullBillingInvoice();
                return new BillingInvoiceIntegrationResponseDto { Success = false };
            }

            billingInvoiceEntity.SetIntegrationSuccess(dto, string.Empty);

            await Context.SaveChangesAsync();

            return new BillingInvoiceIntegrationResponseDto { Success = true, Type = billingInvoiceEntity.BillingInvoiceTypeId.Value };
        }

        public async Task<BillingInvoiceIntegrationResponseDto> UpdateWithCanceled(InvoiceIntegrationResponseDto dto)
        {
            if (string.IsNullOrEmpty(dto.ExternalId))
                NotifyInvalidIntegrationResponse();

            var invoiceId = Guid.Parse(dto.ExternalId);
            var billingInvoiceEntity = Context.BillingInvoices.FirstOrDefault(e => e.Id == invoiceId);

            if (billingInvoiceEntity == null)
            {
                NotifyNullBillingInvoice();
                return new BillingInvoiceIntegrationResponseDto { Success = false };
            }

            _ignoreAuditedEntity.SetEntityNameList(new List<string> { "BillingAccountItemInvoiceHistory", "BillingInvoice" });

            var timeZone = _propertyParameterReadRepository.GetTimeZoneByPropertyId(billingInvoiceEntity.PropertyId);
            var billingAccountItemList = await _billingAccountItemReadRepository.GetByBillingInvoiceIdAsync(invoiceId);

            await CreateBillingAccountItemInvoiceHistory(billingAccountItemList, timeZone);

            RemoveBillingInvoice(billingAccountItemList, timeZone);

            await SetIntegrationCancelSuccess(billingInvoiceEntity, timeZone);

            return new BillingInvoiceIntegrationResponseDto { Success = true, Type = billingInvoiceEntity.BillingInvoiceTypeId.Value };
        }

        private async Task CreateBillingAccountItemInvoiceHistory(List<BillingAccountItem> billingAccountItemList, string timeZoneName)
        {
            var billingAccountItemInvoiceHistoryList = CreateBillingAccountItemInvoiceHistoryList(billingAccountItemList, timeZoneName);

            await _billingAccountItemInvoiceHistoryRepository.AddRangeAsync(billingAccountItemInvoiceHistoryList);
        }

        private List<BillingAccountItemInvoiceHistory> CreateBillingAccountItemInvoiceHistoryList(List<BillingAccountItem> billingAccountItemList, string timeZoneName)
        {
            var billingAccountItemInvoiceHistoryList = new List<BillingAccountItemInvoiceHistory>();
            var currentDate = DateTime.UtcNow.ToZonedDateTime(timeZoneName);

            if (billingAccountItemList.Any())
            {
                foreach (var billingAccountItem in billingAccountItemList)
                {
                    var billingAccountItemInvoiceHistory = new BillingAccountItemInvoiceHistory.Builder(_notificationHandler)
                                    .WithId(Guid.NewGuid())
                                    .WithBillingAccountItemId(billingAccountItem.Id)
                                    .WithBillingInvoiceId(billingAccountItem.BillingInvoiceId.Value)
                                    .WithTenantId(billingAccountItem.TenantId)
                                    .WithCreationTime(currentDate)
                                    .WithLastModificationTime(currentDate)
                                    .Build();

                    billingAccountItemInvoiceHistoryList.Add(billingAccountItemInvoiceHistory);
                }
            }

            return billingAccountItemInvoiceHistoryList;
        }

        private void RemoveBillingInvoice(List<BillingAccountItem> billingAccountItemList, string timeZone)
        {
            foreach (var billingAccountItem in billingAccountItemList)
                billingAccountItem.RemoveBillingInvoice(timeZone);

            _billingAccountItemRepository.UpdateRange(billingAccountItemList);
        }

        private async Task SetIntegrationCancelSuccess(BillingInvoice billingInvoiceEntity, string timeZone)
        {
            billingInvoiceEntity.SetIntegrationCancelSuccess(timeZone);

            await Context.SaveChangesAsync();
        }

        public Task<List<InvoiceDto>> GetAllInvoicesByFilters(SearchBillingInvoiceDto requestDto, int propertyId)
        {
            return _billingInvoiceReadRepository.GetAllByFilters(propertyId, requestDto.InitialDate, requestDto.FinalDate,
                                                                 requestDto.InitialNumber, requestDto.FinalNumber);
        }

        public async Task<Dictionary<string, string>> GetIntegrationDetails(string propertyId)
        {
            var ret = new Dictionary<string, string>();
            var propertyWithDetails = await _propertyParameterReadRepository.GetDtoByIdAsync(propertyId.TryParseToInt32());

            if (propertyWithDetails == null || propertyWithDetails.IntegrationPartnerPropertyList == null || propertyWithDetails.IntegrationPartnerPropertyList.Count == 0)
                _notificationHandler.Raise(_notificationHandler
                                       .DefaultBuilder
                                       .WithMessage(AppConsts.LocalizationSourceName, BillingAccountItemEnum.Error.BillingAccountInvoiceNFeioComunicationError)
                                       .Build());

            var integrationProperty = propertyWithDetails.IntegrationPartnerPropertyList.Where(x => x.PropertyId == propertyId.TryParseToInt32() &&
                                                                                                    x.IntegrationPartnerType == (int)IntegrationPartnerTypeEnum.ENotas).FirstOrDefault();
            if (integrationProperty == null)
                _notificationHandler.Raise(_notificationHandler
                                   .DefaultBuilder
                                   .WithMessage(AppConsts.LocalizationSourceName, BillingAccountItemEnum.Error.BillingAccountInvoiceNFeioComunicationError)
                                   .Build());

            if (!string.IsNullOrEmpty(integrationProperty.IntegrationCode))
                ret.Add(HashManager.GenerateMD5Crypto(integrationProperty.TokenClient.Trim()), integrationProperty.IntegrationCode);

            return ret;
        }
    }
}
