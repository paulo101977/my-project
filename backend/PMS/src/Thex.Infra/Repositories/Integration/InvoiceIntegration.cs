﻿using Microsoft.AspNetCore.Http;
using Polly;
using System;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Thex.Domain.Interfaces.Integration;

namespace Thex.Infra.Repositories.Integration
{
    public class InvoiceIntegration : IInvoiceIntegration
    {
        private readonly IHttpContextAccessor _httpContextAccessor;

        public InvoiceIntegration(
            IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        public async Task<HttpResponseMessage> PostRequest(string json, Uri taxRuleEndpoint)
        {
            using (var httpClient = new HttpClient())
            {
                var token = _httpContextAccessor.HttpContext.Request.Headers.FirstOrDefault(x => x.Key.Equals("Authorization")).Value.ToString();
                httpClient.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", token);

                var responseMessage = await Policy
                                         .HandleResult<HttpResponseMessage>(message => !message.IsSuccessStatusCode)
                                         .WaitAndRetryAsync(0, i => TimeSpan.FromSeconds(15), (result, timeSpan, retryCount, context) =>
                                         {
                                             Console.Write($"Request failed with {result.Result.StatusCode}. Waiting {timeSpan} before next retry. Retry attempt {retryCount}");
                                         })
                                         .ExecuteAsync(async () => await httpClient.PostAsync(taxRuleEndpoint, new StringContent(json, Encoding.UTF8, "application/json")));
                return responseMessage;
            }
        }
    }
}
