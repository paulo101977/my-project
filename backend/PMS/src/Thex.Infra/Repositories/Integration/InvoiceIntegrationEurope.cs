﻿using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Polly;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Thex.Common;
using Thex.Common.Dto.InvoiceIntegration;
using Thex.Common.Emails.AwsSes;
using Thex.Common.Enumerations;
using Thex.Common.Message;
using Thex.Domain.Entities;
using Thex.Domain.Interfaces.Factories;
using Thex.Domain.Interfaces.Integration;
using Thex.Domain.Interfaces.Repositories;
using Thex.Domain.JoinMap;
using Thex.Dto;
using Thex.Dto.BillingAccountClosure;
using Thex.Dto.BillingInvoice;
using Thex.Dto.Email;
using Thex.Dto.Integration;
using Thex.Infra.Context;
using Thex.Infra.ReadInterfaces;
using Thex.Kernel;
using Thex.Preference.Dto;
using Tnf.EntityFrameworkCore;
using Tnf.EntityFrameworkCore.Repositories;
using Tnf.Localization;
using Tnf.Notifications;

namespace Thex.EntityFrameworkCore.Repositories
{
    public class InvoiceIntegrationEurope : EfCoreRepositoryBase<ThexContext, BillingInvoice>, IInvoiceIntegrationEurope
    {
        private string _apiKey = "gWqqYgPUIEimDHjlBOuKhA==";

        private readonly IApplicationUser _applicationUser;
        private readonly IConfiguration _configuration;
        private readonly INotificationHandler _notificationHandler;
        private readonly IBillingAccountItemRepository _billingAccountItemRepository;
        private readonly IGuestRegistrationReadRepository _guestRegistrationReadRepository;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly ILocationReadRepository _locationReadRepository;
        private readonly IDocumentReadRepository _documentReadRepository;
        private readonly IPlasticBrandPropertyReadRepository _plasticBrandReadRepository;
        private readonly ILocalizationManager _localizationManager;
        private readonly IPropertyParameterReadRepository _propertyParameterReadRepository;
        private readonly IInvoiceIntegrationClient _invoiceIntegrationClient;
        private readonly IEmailFactory _emailFactory;
        private readonly IGuestRegistrationDocumentReadRepository _guestRegistrationDocumentReadRepository;
        private readonly IReasonReadRepository _reasonReadRepository;
        private readonly IBillingAccountItemReadRepository _billingAccountItemReadRepository;
        private readonly IReservationItemReadRepository _reservationItemReadRepository;
        private readonly IReservationReadRepository _reservationReadRepository;

        public InvoiceIntegrationEurope(IDbContextProvider<ThexContext> dbContextProvider,
            IApplicationUser applicationUser,
            IConfiguration configuration,
            INotificationHandler notificationHandler,
            IGuestRegistrationReadRepository guestRegistrationReadRepository,
            IBillingAccountItemRepository billingAccountItemRepository,
            IHttpContextAccessor httpContextAccessor,
            ILocationReadRepository locationReadRepository,
            IDocumentReadRepository documentReadRepository,
            IPlasticBrandPropertyReadRepository plasticBrandReadRepository,
            ILocalizationManager localizationManager,
            IPropertyParameterReadRepository propertyParameterReadRepository,
            IInvoiceIntegrationClient invoiceIntegrationClient,
            IEmailFactory emailFactory,
            IGuestRegistrationDocumentReadRepository guestRegistrationDocumentReadRepository,
            IReasonReadRepository reasonReadRepository,
            IBillingAccountItemReadRepository billingAccountItemReadRepository,
            IReservationItemReadRepository reservationItemReadRepository,
            IReservationReadRepository reservationReadRepository)
            : base(dbContextProvider)
        {
            _applicationUser = applicationUser;
            _configuration = configuration;
            _notificationHandler = notificationHandler;
            _billingAccountItemRepository = billingAccountItemRepository;
            _guestRegistrationReadRepository = guestRegistrationReadRepository;
            _httpContextAccessor = httpContextAccessor;
            _locationReadRepository = locationReadRepository;
            _documentReadRepository = documentReadRepository;
            _plasticBrandReadRepository = plasticBrandReadRepository;
            _localizationManager = localizationManager;
            _propertyParameterReadRepository = propertyParameterReadRepository;
            _invoiceIntegrationClient = invoiceIntegrationClient;
            _emailFactory = emailFactory;
            _guestRegistrationDocumentReadRepository = guestRegistrationDocumentReadRepository;
            _reasonReadRepository = reasonReadRepository;
            _billingAccountItemReadRepository = billingAccountItemReadRepository;
            _reservationItemReadRepository = reservationItemReadRepository;
            _reservationReadRepository = reservationReadRepository;
        }

        public async Task<List<IntegrationBaseJoinMap>> GetFacturaBillingInvoice(List<Guid> billingInvoiceIdList)
        {
            var ret = await (from b in Context.BillingInvoices.AsNoTracking()
                             join bip in Context.BillingInvoiceProperties on b.BillingInvoicePropertyId equals bip.Id
                             join ba in Context.BillingAccounts on b.BillingAccountId equals ba.Id
                             join pro in Context.Properties on ba.PropertyId equals pro.Id
                             join bai in Context.BillingAccountItems on b.Id equals bai.BillingInvoiceId
                             join bi in Context.BillingItems on bai.BillingItemId equals bi.Id
                             join bst in Context.BillingInvoicePropertySupportedTypes on
                             new { p1 = bi.Id, p2 = b.BillingInvoicePropertyId } equals
                             new { p1 = bst.BillingItemId, p2 = bst.BillingInvoicePropertyId } into
                             lj
                             from bstl in lj.DefaultIfEmpty()
                             where billingInvoiceIdList.Contains(b.Id) &&
                             b.BillingInvoiceTypeId == (int)BillingInvoiceTypeEnum.Factura &&
                             bai.BillingInvoiceId.HasValue &&
                             (bai.BillingAccountItemTypeId == (int)BillingAccountItemTypeEnum.BillingAccountItemTypeDebit ||
                              bai.BillingAccountItemTypeId == (int)BillingAccountItemTypeEnum.BillingAccountItemTypePartial) &&
                             !bai.WasReversed &&
                             (bi.BillingItemTypeId == (int)BillingItemTypeEnum.Service ||
                              bi.BillingItemTypeId == (int)BillingItemTypeEnum.PointOfSale ||
                              bi.BillingItemTypeId == (int)BillingItemTypeEnum.Tax)
                             select new IntegrationBaseJoinMap
                             {
                                 BillingInvoice = b,
                                 BillingInvoiceProperty = bip,
                                 BillingAccount = ba,
                                 BillingAccountItem = bai,
                                 BillingItem = bi,
                                 BillingInvoicePropertySupportedType = bstl
                             }).ToListAsync();

            if (ret != null)
            {
                var billingAcountId = ret.FirstOrDefault().BillingAccount.Id;

                ret.AddRange(await (from ba in Context.BillingAccounts
                                    join bai in Context.BillingAccountItems on ba.Id equals bai.BillingAccountId
                                    where ba.Id == billingAcountId &&
                                    (bai.BillingAccountItemTypeId == (int)BillingAccountItemTypeEnum.BillingAccountItemTypeDiscount) &&
                                    !bai.WasReversed
                                    select new IntegrationBaseJoinMap
                                    {
                                        BillingAccount = ba,
                                        BillingAccountItem = bai
                                    }).ToListAsync());
            }

            return ret;
        }

        public async Task<List<IntegrationBaseJoinMap>> GetCreditNoteBillingInvoice(List<Guid> billingInvoiceIdList)
        {
            var ret = await (from b in Context.BillingInvoices.AsNoTracking()
                             join bip in Context.BillingInvoiceProperties on b.BillingInvoicePropertyId equals bip.Id
                             join ba in Context.BillingAccounts on b.BillingAccountId equals ba.Id
                             join pro in Context.Properties on ba.PropertyId equals pro.Id
                             join bai in Context.BillingAccountItems on b.Id equals bai.BillingInvoiceId
                             join bi in Context.BillingItems on bai.BillingItemId equals bi.Id
                             join bst in Context.BillingInvoicePropertySupportedTypes on
                             new { p1 = bi.Id, p2 = b.BillingInvoicePropertyId } equals
                             new { p1 = bst.BillingItemId, p2 = bst.BillingInvoicePropertyId } into
                             lj
                             from bstl in lj.DefaultIfEmpty()
                             where billingInvoiceIdList.Contains(b.Id) &&
                             (b.BillingInvoiceTypeId == (int)BillingInvoiceTypeEnum.CreditNotePartial ||
                             b.BillingInvoiceTypeId == (int)BillingInvoiceTypeEnum.CreditNoteTotal) &&
                             bai.BillingInvoiceId.HasValue &&
                             (bai.BillingAccountItemTypeId == (int)BillingAccountItemTypeEnum.BillingAccountItemCreditNote) &&
                             !bai.WasReversed &&
                             (bi.BillingItemTypeId == (int)BillingItemTypeEnum.Service ||
                              bi.BillingItemTypeId == (int)BillingItemTypeEnum.PointOfSale ||
                              bi.BillingItemTypeId == (int)BillingItemTypeEnum.Tax)
                             select new IntegrationBaseJoinMap
                             {
                                 BillingInvoice = b,
                                 BillingInvoiceProperty = bip,
                                 BillingAccount = ba,
                                 BillingAccountItem = bai,
                                 BillingItem = bi,
                                 BillingInvoicePropertySupportedType = bstl
                             }).ToListAsync();

            if (ret != null)
            {
                var billingAcountId = ret.FirstOrDefault().BillingAccount.Id;

                ret.AddRange(await (from ba in Context.BillingAccounts
                                    join bai in Context.BillingAccountItems on ba.Id equals bai.BillingAccountId
                                    where ba.Id == billingAcountId &&
                                    (bai.BillingAccountItemTypeId == (int)BillingAccountItemTypeEnum.BillingAccountItemTypeDiscount) &&
                                    !bai.WasReversed
                                    select new IntegrationBaseJoinMap
                                    {
                                        BillingAccount = ba,
                                        BillingAccountItem = bai
                                    }).ToListAsync());
            }

            return ret;
        }

        private InvoiceIntegrationResponseDto GenerateInvoiceResponse(InvoiceResponseDtoPortugal responseDto)
        {
            return new InvoiceIntegrationResponseDto()
            {
                EmissionDate = responseDto.EmissionDate.ToLongDateString(),
                ExternalId = responseDto.ExternalId,
                InternalId = responseDto.Id,
                InvoiceNumber = responseDto.InvoiceNumber,
                Number = responseDto.InvoiceNumber,
                RpsNumber = responseDto.InvoiceNumber,
                RpsSerial = responseDto.Series,
                Status = responseDto.Status,
                UrlPdf = responseDto.UrlPdf,
                UrlXml = responseDto.UrlXml,
                VerificationCode = ""
            };
        }

        public async Task<BillingInvoiceIntegrationResponseDto> UpdateWithSuccess(InvoiceIntegrationResponseDto dto)
        {
            if (string.IsNullOrEmpty(dto.InternalId))
                NotifyInvalidIntegrationResponse();

            var invoiceId = Guid.Parse(dto.InternalId);
            var billingInvoiceEntity = Context.BillingInvoices.FirstOrDefault(e => e.Id == invoiceId);

            if (billingInvoiceEntity == null)
            {
                NotifyNullBillingInvoice();
                return new BillingInvoiceIntegrationResponseDto { Success = false };
            }

            billingInvoiceEntity.SetIntegrationSuccess(dto, dto.ExternalId);

            await Context.SaveChangesAsync();

            return new BillingInvoiceIntegrationResponseDto { Success = true, Type = billingInvoiceEntity.BillingInvoiceTypeId.Value };
        }


        public async Task<BillingInvoiceIntegrationResponseDto> UpdateWithError(InvoiceIntegrationResponseDto dto)
        {
            var invoiceId = Guid.Parse(dto.InternalId);
            //var billingInvoiceIdList = Context.BillingInvoices.Select(e => e.Id).ToList();
            //var entityAttachedList = Context.Set<BillingInvoice>().Where(e => billingInvoiceIdList.Contains(e.Id));

            foreach (var billingInvoice in Context.BillingInvoices.Where(x => x.Id == invoiceId))
            {
                if (billingInvoice != null)
                    billingInvoice.SetIntegrationError(dto.Reason, dto.InternalId);

                Context.Set<BillingInvoice>()
                       .Attach(billingInvoice);
            }

            await Context.SaveChangesAsync();

            return new BillingInvoiceIntegrationResponseDto { Success = false, Type = (int)BillingInvoiceTypeEnum.Factura };
        }

        private void NotifyNullBillingInvoice()
        {
            _notificationHandler.Raise(_notificationHandler
                .DefaultBuilder
                .WithMessage(AppConsts.LocalizationSourceName, CommonsEnum.Error.NullOrEmptyObject)
                .Build());
        }

        private async Task SendBillingInvoiceAsync(InvoiceIntegrationResponseDto invoiceResponsetDto, int invoiceType)
        {
            var taxRuleEndpoint = new Uri(string.Concat(_configuration.GetValue<string>("TaxRuleEndpoint"), $"/api/invoice/status/", invoiceType.ToString()));

            using (var httpClient = new HttpClient())
            {
                httpClient.DefaultRequestHeaders.Add("x-token", _apiKey);
                var response = await httpClient.PostAsync(taxRuleEndpoint, new StringContent(JsonConvert.SerializeObject(invoiceResponsetDto), Encoding.UTF8, "application/json"));

                if (response.IsSuccessStatusCode)
                    return;
            }
        }

        private void NotifyInvalidIntegrationResponse()
        {
            _notificationHandler.Raise(_notificationHandler
                .DefaultBuilder
                .WithMessage(AppConsts.LocalizationSourceName, BillingInvoice.EntityMessage.BillingInvoiceIntegrationInvalidIntegrationResponse)
                .Build());
        }

        public async Task SendInvoiceIntegratioResponse(BillingInvoiceIntegrationResponseDto response, InvoiceIntegrationResponseDto invoiceResponse)
        {
            if (response.Success)
                await SendBillingInvoiceAsync(invoiceResponse, response.Type);

            if (!string.IsNullOrEmpty(response.EmailAlert))
                SendMailBillingInvoiceIntegrationError(response.EmailAlert, invoiceResponse.Reason);
        }

        private void SendMailBillingInvoiceIntegrationError(string email, string errorMessage)
        {
            var emailDto = new EmailIntegrationDto()
            {
                Title = _localizationManager.GetString(AppConsts.LocalizationSourceName, EmailEnum.EmailSubjectFiscalDocument.ToString()),
                Body = _localizationManager.GetString(AppConsts.LocalizationSourceName, EmailEnum.EmailBodyFiscalDocument.ToString()),
                ErrorMessage = errorMessage
            };

            var host = _configuration.GetValue<string>("AwsEmailHost");
            var port = Convert.ToInt32(_configuration.GetValue<string>("AwsEmailPort"));
            var userName = _configuration.GetValue<string>("AwsEmailUserName");
            var passsword = _configuration.GetValue<string>("AwsEmailPassword");

            var subject = emailDto.Title;
            var from = _configuration.GetValue<string>("EmailBilling");
            var body = _emailFactory.GenerateEmail(EmailTemplateEnum.EmailIntegration, emailDto);
            var to = email;

            // Prepare Email Message
            var message = EmailMessageBuilder
                            .Init()
                            .AddSubject(subject)
                            .AddFrom(from)
                            .AddBody(body)
                            .AddTo(to)
                            .Build();

            // Send Email Message
            IAwsEmailSender sender = new AwsEmailSender(new AwsEmailSettings(host, port, userName, passsword));
            sender.Send(message, true);
        }

        public async Task<string> GetIntegratorIdById(Guid id)
        {
            var taxRuleEndpoint = new Uri(string.Concat(_configuration.GetValue<string>("TaxRuleEndpoint"), $"/api/invoice/invoiceId/{id}"));

            using (var httpClient = new HttpClient())
            {
                var token = _httpContextAccessor.HttpContext.Request.Headers.FirstOrDefault(x => x.Key.Equals("Authorization")).Value.ToString();
                httpClient.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", token);

                var responseMessage = await Policy
                                         .HandleResult<HttpResponseMessage>(message => !message.IsSuccessStatusCode)
                                         .WaitAndRetryAsync(0, i => TimeSpan.FromSeconds(15), (result, timeSpan, retryCount, context) =>
                                         {
                                             Console.Write($"Request failed with {result.Result.StatusCode}. Waiting {timeSpan} before next retry. Retry attempt {retryCount}");
                                         })
                                         .ExecuteAsync(async () => await httpClient.GetAsync(taxRuleEndpoint));

                if (responseMessage.IsSuccessStatusCode)
                    return await responseMessage.Content.ReadAsStringAsync();
                else
                {
                    _notificationHandler.Raise(_notificationHandler
                                        .DefaultBuilder
                                        .WithMessage(AppConsts.LocalizationSourceName, BillingAccountItemEnum.Error.BillingAccountInvoiceNFeioComunicationError)
                                        .Build());
                    return await Task.FromResult(string.Empty);
                }
            }
        }

        public async Task<NotificationResponseDto> GenerateInvoice(List<BillingAccountClosureInvoiceDto> billingInvoiceList, BillingInvoiceTypeEnum invoiceType, BillingAccountItemCreditDto payment = null)
        {
            var taxRuleEndpoint = new Uri(string.Concat(_configuration.GetValue<string>("TaxRuleEndpoint"), $"/api/invoice"));

            List<IntegrationBaseJoinMap> integrationBaseList = null;

            switch (invoiceType)
            {
                case BillingInvoiceTypeEnum.Factura:
                    integrationBaseList = await GetFacturaBillingInvoice(billingInvoiceList.Where(x => x.Type == BillingInvoiceTypeEnum.Factura ||
                                                                                                       x.Type == BillingInvoiceTypeEnum.CreditNoteTotal ||
                                                                                                       x.Type == BillingInvoiceTypeEnum.CreditNotePartial ||
                                                                                                       x.Type == BillingInvoiceTypeEnum.DebitNote)
                                                                                           .Select(x => Guid.Parse(x.InvoiceId)).ToList());
                    break;
                case BillingInvoiceTypeEnum.CreditNoteTotal:
                case BillingInvoiceTypeEnum.CreditNotePartial:
                case BillingInvoiceTypeEnum.DebitNote:
                    integrationBaseList = await GetCreditNoteBillingInvoice(billingInvoiceList.Where(x => x.Type == BillingInvoiceTypeEnum.Factura ||
                                                                                                       x.Type == BillingInvoiceTypeEnum.CreditNoteTotal ||
                                                                                                       x.Type == BillingInvoiceTypeEnum.CreditNotePartial ||
                                                                                                       x.Type == BillingInvoiceTypeEnum.DebitNote)
                                                                                           .Select(x => Guid.Parse(x.InvoiceId)).ToList());
                    break;
                case BillingInvoiceTypeEnum.NFSE:
                case BillingInvoiceTypeEnum.NFCE:
                case BillingInvoiceTypeEnum.NFE:
                default:
                    _notificationHandler.Raise(_notificationHandler
                                    .DefaultBuilder
                                    .WithMessage(AppConsts.LocalizationSourceName, BillingAccountItemEnum.Error.BillingAccountInvoiceNFeioComunicationError)
                                    .Build());
                    break;
            }

            try
            {
                using (var httpClient = new HttpClient())
                {
                    var token = _httpContextAccessor.HttpContext.Request.Headers.FirstOrDefault(x => x.Key.Equals("Authorization")).Value.ToString();
                    httpClient.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", token);

                    var responseMessage = await Policy
                                             .HandleResult<HttpResponseMessage>(message => !message.IsSuccessStatusCode)
                                             .WaitAndRetryAsync(0, i => TimeSpan.FromSeconds(15), (result, timeSpan, retryCount, context) =>
                                             {
                                                 Console.Write($"Request failed with {result.Result.StatusCode}. Waiting {timeSpan} before next retry. Retry attempt {retryCount}");
                                             })
                                             .ExecuteAsync(async () => await httpClient.PostAsync(taxRuleEndpoint, new StringContent(await ProccessInvoiceList(integrationBaseList, invoiceType), Encoding.UTF8, "application/json")));

                    UpdateEmissionDateBillingInvoiceList(integrationBaseList);

                    if (responseMessage.IsSuccessStatusCode)
                    {
                        var message = await responseMessage.Content.ReadAsStringAsync();

                        if (!string.IsNullOrEmpty(message))
                        {
                            var invoiceResponse = JsonConvert.DeserializeObject<InvoiceResponseDtoPortugal>(message);

                            var responseDto = GenerateInvoiceResponse(invoiceResponse);

                            var ret = await UpdateWithSuccess(responseDto);
                            await SendInvoiceIntegratioResponse(ret, responseDto);

                            if (_notificationHandler.HasNotification()) return NotificationResponseDto.NullInstance;

                            UpdateBillingAccountItem(integrationBaseList, invoiceResponse.PaymentId);

                            if (invoiceType == BillingInvoiceTypeEnum.CreditNotePartial)
                                UpdatePaymentIdOfFacturaItem(invoiceType, integrationBaseList, invoiceResponse);
                        }
                    }
                    else
                    {
                        if (_notificationHandler.HasNotification()) return NotificationResponseDto.NullInstance;

                        var message = await responseMessage.Content.ReadAsStringAsync();

                        if (!string.IsNullOrEmpty(message))
                        {
                            var taxRuleNotification = JsonConvert.DeserializeObject<InvoiceIntegrationNotification>(message);

                            if (taxRuleNotification != null)
                            {
                                foreach (var notify in taxRuleNotification.Details)
                                {
                                    return new NotificationResponseDto(notify.DetailedMessage, notify.DetailedMessage,
                                                                       integrationBaseList.Where(x => x.BillingInvoiceProperty != null).Select(x => x.BillingInvoiceProperty.Id).FirstOrDefault());
                                }
                            }
                            else
                                _notificationHandler.Raise(_notificationHandler
                                                    .DefaultBuilder
                                                    .WithMessage(AppConsts.LocalizationSourceName, BillingAccountItemEnum.Error.BillingAccountInvoiceNFeioComunicationError)
                                                    .Build());
                        }
                        else
                            await GenerateIntegrationError(string.Empty, integrationBaseList);
                    }
                }
            }
            catch (Exception ex)
            {
                await GenerateIntegrationError(ex.Message, integrationBaseList);
            }

            return NotificationResponseDto.NullInstance;
        }

        private void UpdatePaymentIdOfFacturaItem(BillingInvoiceTypeEnum invoiceType, List<IntegrationBaseJoinMap> integrationBaseList, InvoiceResponseDtoPortugal invoiceResponse)

        {
            if (invoiceType == BillingInvoiceTypeEnum.CreditNotePartial)
            {
                var billingAccountItemCreditNoteList = integrationBaseList.Select(b => b.BillingAccountItem).Where(b => b.BillingInvoiceId.HasValue);

                var billingAccountItemParentList = _billingAccountItemReadRepository
                    .GetAllByIdList(billingAccountItemCreditNoteList.Select(b => b.BillingAccountItemParentId.GetValueOrDefault()).ToList());

                var billingAccountItemOfFacturaList = _billingAccountItemReadRepository
                    .GetAllByBillingInvoiceIdList(billingAccountItemParentList.Select(b => b.BillingInvoiceId).ToList());

                if (billingAccountItemOfFacturaList.Any() && invoiceResponse != null && invoiceResponse.PaymentId != null)
                {
                    billingAccountItemOfFacturaList.ForEach(b => b.SetPaymentId(invoiceResponse.PaymentId));

                    _billingAccountItemRepository.UpdateBulk(billingAccountItemOfFacturaList);

                    Context.SaveChanges();
                }
            }
        }

        private async Task GenerateIntegrationError(string message, List<IntegrationBaseJoinMap> integrationBaseList)
        {
            _notificationHandler.Raise(_notificationHandler
                                .DefaultBuilder
                                .WithMessage(AppConsts.LocalizationSourceName, BillingAccountItemEnum.Error.BillingAccountInvoiceNFeioComunicationError)
                                .WithDetailedMessage(message)
                                .Build());

            await UpdateWithError(new InvoiceIntegrationResponseDto()
            {
                InternalId = integrationBaseList.Where(e => e.BillingInvoice != null).Select(x => x.BillingInvoice.Id).FirstOrDefault().ToString(),
                Reason = message
            });
        }

        private void UpdateBillingAccountItem(List<IntegrationBaseJoinMap> billingAccountItemList, string paymentId)
        {
            var billingAccountItemIdList = billingAccountItemList.Select(e => e.BillingAccountItem.Id).ToList();
            var entityAttachedList = Context.Set<BillingAccountItem>().Local.Where(e => billingAccountItemIdList.Contains(e.Id));

            foreach (var billingaccountId in billingAccountItemList.Select(e => e.BillingAccountItem.Id).Distinct().ToList())
            {
                var entityAttached = entityAttachedList.FirstOrDefault(e => e.Id == billingaccountId);

                if (entityAttached != null)
                {
                    entityAttached.SetPaymentId(paymentId);
                }
            }

            Context.SaveChanges();
        }

        private void UpdateEmissionDateBillingInvoiceList(List<IntegrationBaseJoinMap> billingInvoiceList)
        {
            var invoiceIdList = billingInvoiceList.Where(e => e.BillingInvoice != null)
                                                  .Select(e => e.BillingInvoice.Id).ToList();

            var entityAttachedList = Context.Set<BillingInvoice>().Local.Where(e => invoiceIdList.Contains(e.Id));

            foreach (var billingInvoiceId in billingInvoiceList.Where(e => e.BillingInvoice != null)
                                                               .Select(e => e.BillingInvoice.Id).Distinct().ToList())
            {
                var entityAttached = entityAttachedList.FirstOrDefault(e => e.Id == billingInvoiceId);

                if (entityAttached != null)
                {
                    entityAttached.SetIntegrationPending(billingInvoiceId, _applicationUser.UserUid);
                }
            }

            Context.SaveChanges();
        }

        private async Task<string> ProccessInvoiceList(List<IntegrationBaseJoinMap> integrationBaseJoinMapList, BillingInvoiceTypeEnum invoiceType)
        {
            var propertyLocation = _locationReadRepository.GetLocationDtoListByPropertyUId(_applicationUser.PropertyId.TryParseToInt32());

            var invoiceList = new List<InvoiceIntegrationDto>();

            var billingInvoiceList = integrationBaseJoinMapList
                .Where(i => i.BillingInvoice != null)
                .Select(e => new
                {
                    e.BillingInvoice.Id,
                    e.BillingInvoice.BillingInvoiceSeries,
                    e.BillingInvoice.BillingInvoiceNumber,
                    e.BillingInvoice.Property,
                    e.BillingInvoice.BillingInvoiceTypeId,
                    e.BillingInvoice.IntegratorId,
                    e.BillingInvoiceProperty,
                    e.BillingInvoicePropertySupportedType,
                    e.BillingInvoice.BillingInvoiceCancelReasonId
                }).DistinctBy(b => b.BillingInvoiceNumber).ToList();

            var nifDocumentTypeIdList = new List<int>() { (int)DocumentTypeEnum.NaturalNIF, (int)DocumentTypeEnum.LegalNIF };
            var billingAccount = integrationBaseJoinMapList.FirstOrDefault().BillingAccount;
            Guid? guestRegistrationId = null;

            if (billingAccount.BillingAccountTypeId == (int)BillingAccountTypeEnum.Guest)
                guestRegistrationId = _guestRegistrationReadRepository.GetIdByGuestReservationItemId(billingAccount.GuestReservationItemId.Value);

            var hasNifDocument = false;

            var reasonId = billingInvoiceList.Any() ? billingInvoiceList.FirstOrDefault().BillingInvoiceCancelReasonId : null;
            string reasonName = reasonId.HasValue ? _reasonReadRepository.GetReasonNameById(reasonId.Value) : null;

            foreach (var invoice in billingInvoiceList)
            {
                var billingAccountItemList = _billingAccountItemRepository.GetListByInvoiceId(invoice.Id);
                if (billingAccountItemList.Any())
                {
                    var invoiceIntegration = new InvoiceIntegrationDto
                    {
                        PropertyDistrict = _applicationUser.PropertyDistrictCode,
                        PropertyUUId = propertyLocation.OwnerId.ToString(),
                        TenantId = _applicationUser.TenantId.ToString(),
                        ExternalId = invoice.Id.ToString(),
                        Client = _invoiceIntegrationClient.GetBorrowerInformation(billingAccount, invoiceType).Result
                                                          .OrderByDescending(x => nifDocumentTypeIdList.Contains(x.DocumentTypeId))
                                                          .FirstOrDefault(),
                        ExternalTypeId = invoice.BillingInvoiceTypeId.ToString(),
                        Number = Convert.ToInt32(invoice.BillingInvoiceNumber),
                        SerialNumber = invoice.BillingInvoiceSeries,
                        FacturaReferenceId = invoice.IntegratorId,
                        FacturaPaymentReferenceId = billingAccountItemList.Where(x => x.IntegrationPaymentId != null)
                                                                          .Select(x => x.IntegrationPaymentId).FirstOrDefault(),
                        FacturaSeriesId = invoice.BillingInvoiceProperty.IntegrationId,
                        ReasonName = reasonName,
                        Observations = GenerateDetailsDescription(integrationBaseJoinMapList, invoice.Id)
                    };

                    if (string.IsNullOrWhiteSpace(invoiceIntegration.Client.Name))
                    {
                        _notificationHandler.Raise(_notificationHandler
                                   .DefaultBuilder
                                   .WithMessage(AppConsts.LocalizationSourceName, BillingInvoice.EntityError.BillingInvoiceMustHaveClientName)
                                   .Build()); return string.Empty;
                    }

                    GeneratePartnertIntegrationDetails(ref invoiceIntegration, _applicationUser.PropertyId.TryParseToInt32());

                    if (nifDocumentTypeIdList.Any() && invoiceIntegration.Client != null && nifDocumentTypeIdList.Contains(invoiceIntegration.Client.DocumentTypeId))
                        hasNifDocument = true;

                    if (_notificationHandler.HasNotification())
                        return await Task.FromResult(string.Empty);

                    GenerateServiceInvoiceDetails(invoiceIntegration.ServiceInvoice, invoice.Id, integrationBaseJoinMapList, billingAccountItemList);
                    GenerateProductInvoiceDetails(invoiceIntegration.ProductInvoice, invoice.Id, integrationBaseJoinMapList, invoiceType);

                    invoiceList.Add(invoiceIntegration);
                }
            }

            if (!hasNifDocument && guestRegistrationId.HasValue && billingAccount.BillingAccountTypeId == (int)BillingAccountTypeEnum.Guest)
                CheckAndSetNif(invoiceList, nifDocumentTypeIdList, guestRegistrationId);
            else if (!hasNifDocument)
            {
                foreach (var invoiceIntegration in invoiceList)
                {
                    if (invoiceIntegration.Client != null)
                        invoiceIntegration.Client.Document = null;
                }
            }

            return await Task.FromResult(JsonConvert.SerializeObject(invoiceList));
        }

        private string GenerateDetailsDescription(long? reservationItemId)
        {
            var detailDescription = string.Empty;

            if (reservationItemId.HasValue && reservationItemId > 0)
            {
                var details = _reservationItemReadRepository.GetDetailsDtoById(reservationItemId.Value);

                if (details != null)
                {
                    detailDescription = $"Código da Reserva: {details.ReservationCode}";

                    if (!details.PartnerReservationNumber.IsNullOrEmpty() && !details.PartnerReservationNumber.IsNullOrWhiteSpace())
                        detailDescription += $" | Código do parceiro: {details.PartnerReservationNumber}";
                }
            }

            return detailDescription;
        }

        private string GenerateDetailsDescription(List<IntegrationBaseJoinMap> integrationBaseJoinMapList, Guid invoiceId)
        {
            var detailDescription = string.Empty;
            var reservationId = integrationBaseJoinMapList.Where(e => e.BillingInvoice.Id == invoiceId)
                                                                 .Select(e => e.BillingAccount.ReservationId)
                                                                 .FirstOrDefault();

            if (reservationId != null && reservationId > 0)
            {
                var details = _reservationReadRepository.GetReservationDetail(new Dto.Reservation.SearchReservationDto()
                {
                    ReservationId = reservationId,
                }, _applicationUser.PropertyId.TryParseToInt32());

                if (details != null)
                {
                    detailDescription = $"Código da Reserva: {details.ReservationCode}";

                    if (!details.PartnerReservationNumber.IsNullOrEmpty() && !details.PartnerReservationNumber.IsNullOrWhiteSpace())
                        detailDescription += $"Código do parceiro: {details.PartnerReservationNumber}";

                    if (!string.IsNullOrEmpty(details.RoomNumber))
                        detailDescription += $"UH: {details.RoomNumber}{Environment.NewLine}";

                    if (!string.IsNullOrEmpty(details.GuestName))
                        detailDescription += $"Hóspede: {details.GuestName} E-Mail:{details.GuestEmail}{Environment.NewLine}";

                    if (!string.IsNullOrEmpty(details.GroupName))
                        detailDescription += $"Grupo: {details.GroupName}{Environment.NewLine}";

                    if (!string.IsNullOrEmpty(details.CompanyClientName))
                        detailDescription += $"Cliente: {details.CompanyClientName}{Environment.NewLine}";

                    var paymentsList = _billingAccountItemReadRepository.GetAllCreditWithInvoiceByBillingAccountId(integrationBaseJoinMapList.Where(e => e.BillingInvoice.Id == invoiceId)
                                                                                                                                      .Select(e => e.BillingAccount.Id)
                                                                                                                                      .FirstOrDefault())
                                                                                                                                      .Where(p => !p.WasReversed).ToList();
                    if (paymentsList != null && paymentsList.Count > 0)
                    {
                        detailDescription += $"{Environment.NewLine}Formas de pagamento: {Environment.NewLine}";

                        var amount = 0m;

                        if (paymentsList.Count == 1)
                            detailDescription += $"Nome: {paymentsList.FirstOrDefault().BillingItem.BillingItemName} Valor: {(integrationBaseJoinMapList.Select(e => e.BillingAccountItem).Sum(x => x.Amount) * -1).ToString("F2")}{Environment.NewLine}";
                        else
                            foreach (var payments in paymentsList)
                            {
                                // find payment percent
                                var percent = payments.Amount / paymentsList.Sum(x => x.Amount);

                                // generate invoice amount with percent
                                amount = (integrationBaseJoinMapList.Select(e => e.BillingAccountItem).Sum(x => x.Amount) * -1) * percent;
                                detailDescription += $"Nome: {payments.BillingItem.BillingItemName} Valor: {amount.ToString("F2")}{Environment.NewLine}";
                            }
                    }
                }
            }

            return detailDescription;
        }

        private void GeneratePartnertIntegrationDetails(ref InvoiceIntegrationDto invoiceIntegration, int propertyId)
        {
            var propertyWithDetails = _propertyParameterReadRepository.GetDtoByIdAsync(propertyId).Result;
            if (propertyWithDetails == null || propertyWithDetails.IntegrationPartnerPropertyList == null || propertyWithDetails.IntegrationPartnerPropertyList.Count == 0)
                _notificationHandler.Raise(_notificationHandler
                                       .DefaultBuilder
                                       .WithMessage(AppConsts.LocalizationSourceName, BillingAccountItemEnum.Error.BillingAccountInvoiceNFeioComunicationError)
                                       .Build());

            var integrationProperty = propertyWithDetails.IntegrationPartnerPropertyList.Where(x => x.PropertyId == propertyId &&
                                                                                                    x.IntegrationPartnerType == (int)IntegrationPartnerTypeEnum.InvoiceXpress).FirstOrDefault();
            if (integrationProperty == null)
                _notificationHandler.Raise(_notificationHandler
                                   .DefaultBuilder
                                   .WithMessage(AppConsts.LocalizationSourceName, BillingAccountItemEnum.Error.BillingAccountInvoiceNFeioComunicationError)
                                   .Build());

            invoiceIntegration.TokenClient = integrationProperty.IntegrationNumber.Trim();
            invoiceIntegration.IntegrationCode = integrationProperty.IntegrationCode.Trim();
        }

        private void GenerateProductInvoiceDetails(List<IntegrationProductInvoiceDTO> productInvoice, Guid invoiceId,
                                                   List<IntegrationBaseJoinMap> integrationBaseJoinMapList, BillingInvoiceTypeEnum invoiceType)
        {
            var billingAccountItem = integrationBaseJoinMapList.Where(e => e.BillingAccountItem.BillingInvoiceId == invoiceId &&
                                                                        e.BillingInvoicePropertySupportedType.BillingItemTypeId == (int)BillingItemTypeEnum.PointOfSale)
                                                               .Select(e => e.BillingAccountItem)
                                                               .ToList();

            foreach (var invoiceDetails in billingAccountItem.Where(x => x.IntegrationDetail != null))
            {
                var productsList = JsonConvert.DeserializeObject<List<MostSelectedDetailsDto>>(invoiceDetails.IntegrationDetail);

                if (productsList != null && productsList.Count > 0)
                {
                    foreach (var product in productsList)
                    {
                        var integrationInvoice = new IntegrationProductInvoiceDTO()
                        {
                            ProductId = product.ExternalId,
                            Barcode = product.BarCode,
                            Ncm = product.NcmCode,
                            Code = product.Code,
                            Quantity = product.Quantity,
                            Amount = (invoiceType == BillingInvoiceTypeEnum.Factura ?
                                        GetFacturaProductAmount(product, integrationBaseJoinMapList, invoiceDetails.Id) :
                                        GetCreditNoteProductAmount(invoiceDetails.Amount, product.Quantity)),
                            Description = product.Description
                        };

                        productInvoice.Add(integrationInvoice);
                    }
                }
            }
        }

        private void CheckAndSetNif(List<InvoiceIntegrationDto> invoiceList, List<int> nifDocumentTypeIdList, Guid? guestRegistrationId)
        {
            var nifDocument = _guestRegistrationDocumentReadRepository.GetAllDocumentsByGuestRegistrationId(guestRegistrationId.GetValueOrDefault())
                                .FirstOrDefault(d => nifDocumentTypeIdList.Any() && nifDocumentTypeIdList.Contains(d.DocumentTypeId));

            if (nifDocument != null)
                invoiceList.ForEach(invoiceIntegration => invoiceIntegration.Client.Document = nifDocument.DocumentInformation);
            else
                invoiceList.ForEach(invoiceIntegration => invoiceIntegration.Client.Document = null);
        }

        private void GenerateServiceInvoiceDetails(List<IntegrationServiceInvoiceDTO> serviceInvoice, Guid invoiceId,
                                                   List<IntegrationBaseJoinMap> integrationBaseJoinMapList, IList<BillingAccountItem> billingAccountItemList)
        {
            var billingItemList = integrationBaseJoinMapList.Where(e => e.BillingInvoice != null &&
                                                                        e.BillingInvoice.Id == invoiceId &&
                                                                        e.BillingInvoicePropertySupportedType.BillingItemTypeId == (int)BillingItemTypeEnum.Service)
                                                                 .Select(e => e.BillingAccountItem)
                                                                 .ToList();

            if (billingItemList != null)
                foreach (var billingItem in billingItemList)
                {
                    var discount = (integrationBaseJoinMapList.Select(e => e.BillingAccountItem).Where(x => x.BillingAccountItemTypeId == (int)BillingAccountItemTypeEnum.BillingAccountItemTypeDiscount &&
                                                                                        x.BillingAccountItemParentId == billingItem.Id)
                                                                          .Sum(x => x.Amount));

                    serviceInvoice.Add(new IntegrationServiceInvoiceDTO()
                    {
                        Description = GetInvoiceDescription(billingAccountItemList.FirstOrDefault(b => b.Id == billingItem.Id)),
                        ServiceId = billingItem.BillingItemId.ToString(),
                        Amount = GetAmount(billingItem, discount).TruncateDecimal(2)
                    });
                }
        }

        private decimal GetAmount(BillingAccountItem billingAccountItem, decimal discount)
        {
            if (billingAccountItem.CreditAmount.HasValue)
                return billingAccountItem.CreditAmount.Value;
            else if (billingAccountItem.Amount < 0)
                return ((billingAccountItem.Amount * -1) - discount);
            else
                return billingAccountItem.Amount;
        }

        private decimal GetFacturaProductAmount(MostSelectedDetailsDto product, List<IntegrationBaseJoinMap> integrationBaseJoinMapList, Guid billingAccountItemId)
        {
            var discount = (integrationBaseJoinMapList.Select(e => e.BillingAccountItem)
                                                      .Where(x => x.BillingInvoiceId == null &&
                                                                  x.BillingAccountItemTypeId == (int)BillingAccountItemTypeEnum.BillingAccountItemTypeDiscount &&
                                                                  x.BillingAccountItemParentId == billingAccountItemId)
                                                      .Sum(x => x.Amount));

            return ((product.Amount - discount) / product.Quantity).TruncateDecimal(2);
        }

        public decimal GetCreditNoteProductAmount(decimal amount, decimal quantity)
        {
            return (amount / quantity).TruncateDecimal(2);
        }

        private string GetInvoiceDescription(BillingAccountItem billingAccountItem)
        {
            return billingAccountItem?.BillingItem?.BillingItemName;
        }
    }
}
