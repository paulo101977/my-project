﻿//  <copyright file="RoomRepository.cs" company="TOTVS">
//  Copyright (c) TOTVS. All rights reserved.
//  </copyright>
namespace Thex.EntityFrameworkCore.Repositories
{
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;

    using Thex.Domain.Entities;

    using Microsoft.EntityFrameworkCore;

    using Tnf.EntityFrameworkCore.Repositories;
    using Tnf.EntityFrameworkCore;
    using Thex.Domain.Interfaces.Repositories;
    using Thex.Common;
    using Thex.Infra.Context;
    using Tnf.Notifications;

    public class MarketSegmentRepository : EfCoreRepositoryBase<ThexContext, MarketSegment>, IMarketSegmentRepository
    {
        private readonly INotificationHandler Notification;

        public MarketSegmentRepository(IDbContextProvider<ThexContext> dbContextProvider,
            INotificationHandler notificationHandler)
            : base(dbContextProvider)
        {
            Notification = notificationHandler;
        }

        public override void Delete(MarketSegment entity)
        {
            try
            {

                RemoveChainMarketSegment(entity.Id);

                base.Delete(entity);

                Context.SaveChanges();


            }
            catch
            {
                Context.ChangeTracker.Entries()
                    .Where(e => e.Entity != null).ToList()
                    .ForEach(e => e.State = EntityState.Detached);

                Notification.Raise(Notification.DefaultBuilder
                    .WithMessage(AppConsts.LocalizationSourceName, MarketSegment.EntityError.MarketSegmentCannotDeleteWithChild)
                    .WithDetailedMessage(AppConsts.LocalizationSourceName, MarketSegment.EntityError.MarketSegmentCannotDeleteWithChild)
                    .Build());
            }

        }

        private void RemoveChainMarketSegment(int MarketSegmentId)
        {
            var chainMarketSegment = Context.ChainMarketSegments.FirstOrDefault(exp => exp.MarketSegmentId == MarketSegmentId);

            if (chainMarketSegment != null)
                Context.Remove(chainMarketSegment);
        }


        public void ToggleAndSaveIsActive(int id)
        {
            var MarketSegment = Context
                .MarketSegments
                .Include(exp => exp.ChainMarketSegmentList)
                .FirstOrDefault(exp => exp.Id == id);

            var toogle = !MarketSegment.IsActive;

            MarketSegment.IsActive = toogle;

            foreach (var chainMarketSegment in MarketSegment.ChainMarketSegmentList)
            {
                chainMarketSegment.IsActive = toogle;
                Context.ChainMarketSegments.Update(chainMarketSegment);
            }

            Context.SaveChanges();
        }
    }
}