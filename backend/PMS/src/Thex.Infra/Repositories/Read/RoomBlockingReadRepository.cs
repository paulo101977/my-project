﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using Thex.Common.Enumerations;
using Thex.Domain.Entities;
using Thex.Dto;
using Thex.Dto.HousekeepingStatusProperty;
using Thex.Infra.ReadInterfaces;
using Tnf.EntityFrameworkCore.Repositories;
using Tnf.EntityFrameworkCore;
using Thex.Infra.Context;

namespace Thex.Infra.Repositories.Read
{
    public class RoomBlockingReadRepository : EfCoreRepositoryBase<ThexContext, RoomBlocking>, IRoomBlockingReadRepository
    {
        

        public RoomBlockingReadRepository(
            IDbContextProvider<ThexContext> dbContextProvider) : base(dbContextProvider)
        {
            
        }

        public List<RoomBlocking> GetAllBlockedInPeriod(DateTime startBlock, DateTime endBlock, int roomId)
        {
            return GetAllBlockedInPeriod(startBlock, endBlock, new List<int> { roomId });
        }

        public List<RoomBlocking> GetAllBlockedInPeriod(DateTime startBlock, DateTime endBlock, List<int> roomIds)
        {
            return Context
                .RoomBlockings
                .Where(x => roomIds.Contains(x.RoomId) && x.BlockingStartDate.Date <= endBlock.Date && x.BlockingEndDate >= startBlock.Date && !x.IsDeleted).ToList();
        }

        public List<RoomBlocking> GetAllRoomBlockingForUnlock(DateTime startBlock, DateTime endBlock, int propertyId)
        {
            return Context
                .RoomBlockings
                
                .Where(x => x.BlockingStartDate.Date <= endBlock.Date && x.BlockingEndDate >= startBlock.Date && x.PropertyId == propertyId && !x.IsDeleted).ToList();

        }

        public List<RoomBlocking> GetAllRoomBlockingForUnlockByBlockingEndDate(DateTime blockingEndDate, int propertyId)
        {
            return Context
                .RoomBlockings
                
                .Where(x => x.BlockingEndDate == blockingEndDate.Date && x.PropertyId == propertyId && !x.IsDeleted).ToList();

        }

        public List<RoomWithReservationsDto> GetReservationByRoomIds(List<int> roomIds, DateTime startBlock, DateTime endBlock)
        {

            var status = new int[]
            {
                (int)ReservationStatus.ToConfirm,
                (int)ReservationStatus.Confirmed,
                (int)ReservationStatus.Checkin,
                (int)ReservationStatus.ReservationProposal
            };
            var roomsBlockWithReservations = (from reservation in Context.Reservations

                                              join reservationItem in Context.ReservationItems
                                  on reservation.Id equals reservationItem.ReservationId

                              join room in Context.Rooms
                                   on reservationItem.RoomId equals room.Id

                              where status.Contains(reservationItem.ReservationItemStatus.Id) && roomIds.Contains(room.Id) &&
                                       (reservationItem.CheckInDate ?? reservationItem.EstimatedArrivalDate).Date <= endBlock.Date &&
                                       (reservationItem.CheckOutDate ?? reservationItem.EstimatedDepartureDate).Date > startBlock.Date
                              select new RoomWithReservationsDto

                              {
                                  RoomId = room.Id,
                                  ReservationCode = reservation.ReservationCode,
                                  EstimatedArrivalDate = reservationItem.EstimatedArrivalDate,
                                  EstimatedDepartureDate = reservationItem.EstimatedDepartureDate,
                                  Id = reservation.Id
                              }).ToList();


            return roomsBlockWithReservations;
        }

        public List<RoomBlockingDto> GetAllRoomsBlockedByPropertyId(int propertyId, DateTime initialDate, DateTime finalDate)
        {
            return (from roomBlocking in Context.RoomBlockings.AsNoTracking()
                    join room in Context.Rooms.AsNoTracking() on roomBlocking.RoomId equals room.Id

                    where roomBlocking.PropertyId == propertyId && !roomBlocking.IsDeleted &&
                    roomBlocking.BlockingStartDate.Date <= finalDate.Date &&
                    roomBlocking.BlockingEndDate.Date >= initialDate.Date
                    select new RoomBlockingDto
                    {
                        RoomId = roomBlocking.RoomId,
                        BlockingStartDate = roomBlocking.BlockingStartDate,
                        BlockingEndDate = roomBlocking.BlockingEndDate,
                        Id = roomBlocking.Id,
                        RoomTypeId = room.RoomTypeId
                    }).ToList();
        }

        public List<RoomBlocking> GetAllRoomBlockingForBlockingByBlockingEndDate(DateTime blockingStartDate, int propertyId)
        {
            return Context
                .RoomBlockings
                
                .Where(x => x.BlockingStartDate == blockingStartDate.Date && x.PropertyId == propertyId && !x.IsDeleted).ToList();
        }

        public List<int> GetAllBlockedRoomIdsByPeriod(DateTime startDate, DateTime endDate)
        {
            return Context.RoomBlockings.Where(x => x.BlockingStartDate <= endDate.Date && x.BlockingEndDate >= startDate.Date).Select(x => x.RoomId).ToList();
        }
    }
}
