﻿using Thex.Infra.Context;
using Tnf.EntityFrameworkCore;

namespace Thex.Infra.Repositories.Read
{
    public class EfCoreRepositoryBase<T>
    {
        private IDbContextProvider<ThexContext> dbContextProvider;

        public EfCoreRepositoryBase(IDbContextProvider<ThexContext> dbContextProvider)
        {
            this.dbContextProvider = dbContextProvider;
        }
    }
}