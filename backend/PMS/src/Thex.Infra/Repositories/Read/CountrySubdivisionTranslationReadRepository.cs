﻿//  <copyright file="CountrySubdivisionTranslationReadRepository.cs" company="TOTVS">
//  Copyright (c) TOTVS. All rights reserved.
//  </copyright>
namespace Thex.EntityFrameworkCore.Repositories
{
    using Thex.Domain.Entities;
    using Thex.Infra.ReadInterfaces;
    using System.Linq;
    using Tnf.Dto;
    using Tnf.EntityFrameworkCore.Repositories;
    using Tnf.EntityFrameworkCore;
    using System.Collections.Generic;
    using Thex.Dto.CountrySubdivisionTranslation;
    using Thex.Dto;
    
    using Thex.Infra.Context;
    using System;
    using Thex.Kernel;

    public class CountrySubdivisionTranslationReadRepository : EfCoreRepositoryBase<ThexContext, CountrySubdivisionTranslation>, ICountrySubdivisionTranslationReadRepository
    {
        private readonly IApplicationUser _applicationUser;

        public CountrySubdivisionTranslationReadRepository(
            IDbContextProvider<ThexContext> dbContextProvider,
            IApplicationUser applicationUser)
            : base(dbContextProvider)
        {
            _applicationUser = applicationUser;
        }
        
        public AddressTranslationDto GetAddressTranslationByCityId(int? cityId, string languageIsoCode)
        {
            return (from city in Context.CountrySubdivisions
                               join state in Context.CountrySubdivisions on city.ParentSubdivision.Id equals state.Id
                               join country in Context.CountrySubdivisions on state.ParentSubdivision.Id equals country.Id
                               join cityTranslation in Context.CountrySubdivisionTranslations on city.Id equals cityTranslation.CountrySubdivision.Id
                               join stateTranslation in Context.CountrySubdivisionTranslations on state.Id equals stateTranslation.CountrySubdivision.Id
                               join countryTranslation in Context.CountrySubdivisionTranslations on country.Id equals countryTranslation.CountrySubdivision.Id
                               where city.Id == cityId &&
                               cityTranslation.LanguageIsoCode.ToLower() == languageIsoCode &&
                               stateTranslation.LanguageIsoCode.ToLower() == languageIsoCode &&
                               countryTranslation.LanguageIsoCode.ToLower() == languageIsoCode
                               select new AddressTranslationDto
                               {
                                   CityId = city.Id,
                                   CityName = cityTranslation.Name,
                                   StateId = state.Id,
                                   StateName = stateTranslation.Name,
                                   CountryId = country.Id,
                                   CountryName = countryTranslation.Name,
                                   LanguageIsoCode = cityTranslation.LanguageIsoCode,
                                   TwoLetterIsoCode = state.TwoLetterIsoCode,
                                   CountryTwoLetterIsoCode = state.CountryTwoLetterIsoCode
                               })
                               .FirstOrDefault();
        }

        public IListDto<CountryDto> GetAllCountriesByLanguageIsoCode(GetAllCountryDto request)
        {
            var result = (from country in Context.CountrySubdivisions
                             join translation in Context.CountrySubdivisionTranslations on country.Id equals translation.CountrySubdivision.Id
                             where translation.LanguageIsoCode.ToLower() == request.LanguageIsoCode.ToLower() &&
                             !country.ParentSubdivisionId.HasValue
                             select new CountryDto
                             {
                                 Id = country.Id,
                                 Name = translation.Name,
                                 TranslationId = translation.Id,
                                 TwoLetterIsoCode = country.TwoLetterIsoCode,
                                 CountryTwoLetterIsoCode = country.CountryTwoLetterIsoCode
                             })
                             .OrderBy(exp => exp.Name).ToList();

            return new ListDto<CountryDto>
            {
                HasNext = false,
                Items = result,
            };
        }

        public List<BaseCountrySubdvisionTranslationDto> GetCountryWithTranslations(string countryTwoLetterIsoCode)
        {
            return (from country in Context.CountrySubdivisions
                    join translation in Context.CountrySubdivisionTranslations on country.Id equals translation.CountrySubdivision.Id
                    where country.CountryTwoLetterIsoCode.ToLower() == countryTwoLetterIsoCode.ToLower() &&
                    !country.ParentSubdivisionId.HasValue
                    select new BaseCountrySubdvisionTranslationDto
                    {
                        Id = country.Id,
                        Name = translation.Name,
                        TranslationId = translation.Id,
                        TwoLetterIsoCode = country.TwoLetterIsoCode,
                        CountryTwoLetterIsoCode = country.CountryTwoLetterIsoCode,
                        LanguageIsoCode = translation.LanguageIsoCode
                    }).ToList();

        }

        public List<BaseCountrySubdvisionTranslationDto> GetStateWithTranslations(string translationName, int parentSubdivionId)
        {
            return (from country in Context.CountrySubdivisions
                    join translation in Context.CountrySubdivisionTranslations on country.Id equals translation.CountrySubdivision.Id
                    where translation.Name.ToLower() == translationName.ToLower() && country.ParentSubdivision.Id == parentSubdivionId 
                    select new BaseCountrySubdvisionTranslationDto
                    {
                        Id = country.Id,
                        Name = translation.Name,
                        TranslationId = translation.Id,
                        TwoLetterIsoCode = country.TwoLetterIsoCode,
                        CountryTwoLetterIsoCode = country.CountryTwoLetterIsoCode,
                        LanguageIsoCode = translation.LanguageIsoCode
                    }).ToList();
        }


        public List<BaseCountrySubdvisionTranslationDto> GetStateWithTranslationsByCode(string twoLetterIsoCode, int parentSubdivionId)
        {
            return (from state in Context.CountrySubdivisions
                    join translation in Context.CountrySubdivisionTranslations on state.Id equals translation.CountrySubdivision.Id
                    where state.TwoLetterIsoCode.ToLower() == twoLetterIsoCode.Substring(0,2).ToLower() && state.ParentSubdivision.Id == parentSubdivionId
                    select new BaseCountrySubdvisionTranslationDto
                    {
                        Id = state.Id,
                        Name = translation.Name,
                        TranslationId = translation.Id,
                        TwoLetterIsoCode = state.TwoLetterIsoCode,
                        CountryTwoLetterIsoCode = state.CountryTwoLetterIsoCode,
                        LanguageIsoCode = translation.LanguageIsoCode
                    }).ToList();
        }

        public List<BaseCountrySubdvisionTranslationDto> GetCityWithTranslations(string translationName, int parentSubdivionId)
        {
            return (from country in Context.CountrySubdivisions
                    join translation in Context.CountrySubdivisionTranslations on country.Id equals translation.CountrySubdivision.Id
                    where translation.Name.ToLower() == translationName.ToLower() && country.ParentSubdivision.Id == parentSubdivionId
                    select new BaseCountrySubdvisionTranslationDto
                    {
                        Id = country.Id,
                        Name = translation.Name,
                        TranslationId = translation.Id,
                        TwoLetterIsoCode = country.TwoLetterIsoCode,
                        CountryTwoLetterIsoCode = country.CountryTwoLetterIsoCode,
                        LanguageIsoCode = translation.LanguageIsoCode
                    }).ToList();
        }


        public List<BaseCountrySubdvisionTranslationDto> GetCountrySubdivisionWithTranslations(int subdivisionId)
        {
            return (from countrySubdivision in Context.CountrySubdivisions
                    join translation in Context.CountrySubdivisionTranslations on countrySubdivision.Id equals translation.CountrySubdivision.Id
                    where countrySubdivision.Id == subdivisionId
                    select new BaseCountrySubdvisionTranslationDto
                    {
                        Id = countrySubdivision.Id,
                        Name = translation.Name,
                        TranslationId = translation.Id,
                        TwoLetterIsoCode = countrySubdivision.TwoLetterIsoCode,
                        CountryTwoLetterIsoCode = countrySubdivision.CountryTwoLetterIsoCode,
                        LanguageIsoCode = translation.LanguageIsoCode
                    }).ToList();
        }

        public IListDto<CountrySubdivisionTranslationDto> GetNationalities(GetAllCountrySubdivisionTranslationDto request)
        {
            if (string.IsNullOrEmpty(request.Language) || request.Language.ToLower() == "en-us")
                request.Language = "en";

            var propertyCountryCode = _applicationUser.PropertyCountryCode;

            var result = (from countrySubdivision in Context.CountrySubdivisions
                             join translation in Context.CountrySubdivisionTranslations on countrySubdivision.Id equals translation.CountrySubdivision.Id
                             where !countrySubdivision.ParentSubdivisionId.HasValue &&
                             translation.LanguageIsoCode.ToLower() == request.Language.ToLower() &&
                             translation.Nationality != null
                             orderby countrySubdivision.TwoLetterIsoCode == propertyCountryCode descending,
                             translation.Nationality
                          select new CountrySubdivisionTranslation
                             {
                                 Id = translation.CountrySubdivisionId,
                                 CountrySubdivisionId = translation.CountrySubdivisionId,
                                 Name = translation.Name,
                                 LanguageIsoCode = translation.LanguageIsoCode,
                                 Nationality = translation.Nationality
                             })
                             .ToList();

            var resultList = new List<CountrySubdivisionTranslationDto>();
            foreach (var item in result)
                resultList.Add(item.MapTo<CountrySubdivisionTranslationDto>());

            var nationalities = new ListDto<CountrySubdivisionTranslationDto>()
            {
                HasNext = false,
                Items = resultList,
            };

            return nationalities;
        }


        public CountrySubdivisionTranslationDto GetNationalityByIdAndProperty(int nationalityId, int propertyId)
        {
            var translationProperty = (
                            from property in Context.Properties
                            join location in Context.Locations on property.PropertyUId equals location.OwnerId
                            join countrySubdivision in Context.CountrySubdivisions on location.CountryCode equals countrySubdivision.CountryTwoLetterIsoCode
                            join countryLanguage in Context.CountryLanguages on countrySubdivision.Id equals countryLanguage.CountrySubdivisionId
                            join translation in Context.CountrySubdivisionTranslations on 
                            new { a = countrySubdivision.Id, b = countryLanguage.CultureInfoCode.ToLower() } equals 
                            new { a = translation.CountrySubdivisionId, b = translation.LanguageIsoCode.ToLower() }
                            where !countrySubdivision.ParentSubdivisionId.HasValue && property.Id == propertyId
                            select new CountrySubdivisionTranslation
                            {
                                Id = translation.Id,
                                CountrySubdivisionId = translation.CountrySubdivisionId,
                                Name = translation.Name,
                                LanguageIsoCode = translation.LanguageIsoCode,
                                Nationality = translation.Nationality
                            }).FirstOrDefault();

            var language = translationProperty == null ? "en-us" : translationProperty.LanguageIsoCode.ToLower();

            var result = (from countrySubdivision in Context.CountrySubdivisions
                          join translation in Context.CountrySubdivisionTranslations on countrySubdivision.Id equals translation.CountrySubdivision.Id
                          where !countrySubdivision.ParentSubdivisionId.HasValue &&
                          translation.LanguageIsoCode.ToLower() == language.ToLower() &&
                          translation.Nationality != null
                          select new CountrySubdivisionTranslation
                          {
                              Id = translation.Id,
                              CountrySubdivisionId = translation.CountrySubdivisionId,
                              Name = translation.Name,
                              LanguageIsoCode = translation.LanguageIsoCode,
                              Nationality = translation.Nationality
                          })
                            .FirstOrDefault();

            return result.MapTo<CountrySubdivisionTranslationDto>();
        }
        
        public int GetIdByCountryName(string countryName)        
          =>  Context.CountrySubdivisionTranslations.FirstOrDefault(x => x.Name.Equals(countryName)).CountrySubdivisionId;
        
    }
}