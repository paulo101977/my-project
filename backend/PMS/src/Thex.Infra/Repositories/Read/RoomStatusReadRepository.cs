﻿using System;
using System.Collections.Generic;
using System.Linq;
using Thex.Common;
using Thex.Common.Enumerations;
using Thex.Domain.Entities;
using Thex.Domain.Interfaces.Repositories;
using Thex.Domain.JoinMap;
using Thex.Dto;
using Thex.Dto.RoomStatus;
using Thex.Infra.Context;
using Thex.Infra.ReadInterfaces;
using Tnf.Dto;
using Tnf.EntityFrameworkCore;
using Tnf.EntityFrameworkCore.Repositories;
using Tnf.Localization;

namespace Thex.Infra.Repositories.Read
{
    public class RoomStatusReadRepository : EfCoreRepositoryBase<ThexContext, Room>, IRoomStatusReadRepository
    {
        private readonly IRoomRepository _roomRepository;
        private readonly IPropertyParameterReadRepository _propertyParameterReadRepository;
        private readonly ILocalizationManager _localizationManager;

        public RoomStatusReadRepository(
            IDbContextProvider<ThexContext> dbContextProvider,
            IRoomRepository roomRepository,
            IPropertyParameterReadRepository propertyParameterReadRepository,
            ILocalizationManager localizationManager)
            : base(dbContextProvider)
        {
            _roomRepository = roomRepository;
            _propertyParameterReadRepository = propertyParameterReadRepository;
            _localizationManager = localizationManager;
        }

        public ListDto<RoomStatusDto> GetAll(GetAllRoomStatusDto getAllRoomStatusDto, int propertyId)
        {
            DateTime? systemDate = _propertyParameterReadRepository.GetSystemDate(propertyId);

            var statusList = new List<int>()
            {
                (int)ReservationStatus.Checkin,
            };

            var result = from roomReservationItemJoinMap in _roomRepository.GetAllOccupiedRoomList()
                    join roomType in Context.RoomTypes on roomReservationItemJoinMap.Room.RoomTypeId equals roomType.Id
                    join housekeeping in Context.HousekeepingStatusProperties
                               on roomReservationItemJoinMap.Room.HousekeepingStatusPropertyId equals housekeeping.Id into h
                    from housekeeping in h.DefaultIfEmpty()
                    join roomBlocking in Context.RoomBlockings.Where(x =>
                                                   x.BlockingStartDate.Date <=
                                                   systemDate && x.BlockingEndDate
                                                   >= systemDate && !x.IsDeleted)
                              on roomReservationItemJoinMap.Room.Id equals roomBlocking.RoomId into b
                    from leftRoomBlocking in b.DefaultIfEmpty()
                    select new RoomStatusDto
                    {
                        RoomId = roomReservationItemJoinMap.Room.Id,
                        RoomNumber = roomReservationItemJoinMap.Room.RoomNumber,
                        RoomStatusId = GetRoomStatusId(roomReservationItemJoinMap, leftRoomBlocking),
                        RoomStatusName = _localizationManager.GetString(AppConsts.LocalizationSourceName, (GetRoomStatusId(roomReservationItemJoinMap, leftRoomBlocking)).ToString()),
                        HousekeepingStatusPropertyId = housekeeping != null ? housekeeping.Id : Guid.Empty,
                        HousekeepingStatusPropertyName = housekeeping != null ? housekeeping.StatusName : null,
                        HousekeepingStatusPropertyColor = housekeeping != null ? housekeeping.Color : null,
                        HousekeepingStatusId = housekeeping != null ? housekeeping.HousekeepingStatusId : 0,
                        RoomTypeName = roomType.Name,
                        IsActive = roomReservationItemJoinMap.Room.IsActive,
                        RoomTypeAbbreviation = roomType.Abbreviation,
                        ReservationItemId = roomReservationItemJoinMap.ReservationItem != null ? roomReservationItemJoinMap.ReservationItem.Id : 0,
                        AdultCount = roomReservationItemJoinMap.ReservationItem != null ? roomReservationItemJoinMap.ReservationItem.AdultCount : 0,
                        ChildCount = roomReservationItemJoinMap.ReservationItem != null ? roomReservationItemJoinMap.ReservationItem.ChildCount : 0,
                        CheckInDate = roomReservationItemJoinMap.ReservationItem != null ? roomReservationItemJoinMap.ReservationItem.CheckInDate : null,
                        CheckOutDate = roomReservationItemJoinMap.ReservationItem != null ? roomReservationItemJoinMap.ReservationItem.CheckOutDate : null,
                        ReservationItemStatusId = roomReservationItemJoinMap.ReservationItem != null ? roomReservationItemJoinMap.ReservationItem.ReservationItemStatusId : 0,
                        ReservationItemStatusName = roomReservationItemJoinMap.ReservationItem != null ? _localizationManager.GetString(AppConsts.LocalizationSourceName, ((ReservationStatus)roomReservationItemJoinMap.ReservationItem.ReservationItemStatusId).ToString()) : null
                    };

            if (getAllRoomStatusDto.HouseKeepingStatusId > 0)
                result = result.Where(r => r.HousekeepingStatusId == getAllRoomStatusDto.HouseKeepingStatusId);

            return new ListDto<RoomStatusDto>
            {
                Items = result.ToList(),
                HasNext = false
            };

        }

        private RoomAvailabilityEnum GetRoomStatusId(RoomReservationItemJoinMap roomReservationItemJoinMap, RoomBlocking roomBlocking)
        {
            if (roomReservationItemJoinMap.ReservationItem == null && roomBlocking != null)
            {
                return RoomAvailabilityEnum.Blocked;
            }
            else if (roomReservationItemJoinMap.ReservationItem != null)
            {
                return RoomAvailabilityEnum.Busy;
            }
            else
            {
                return RoomAvailabilityEnum.Vague;
            }
        }
    }
}
