﻿// //  <copyright file="LocationReadRepository.cs" company="TOTVS">
// //  Copyright (c) TOTVS. All rights reserved.
// //  </copyright>
namespace Thex.EntityFrameworkCore.Repositories
{
    using Microsoft.EntityFrameworkCore;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Thex.Common;
    using Thex.Common.Enumerations;
    using Thex.Domain.Entities;
    using Thex.Dto;
    using Thex.Infra.Context;
    using Thex.Infra.ReadInterfaces;
    using Tnf.EntityFrameworkCore;
    using Tnf.EntityFrameworkCore.Repositories;
    using Tnf.Localization;

    public class LocationReadRepository : EfCoreRepositoryBase<ThexContext, Location>, ILocationReadRepository
    {
        private readonly ILocalizationManager _localizationManager;

        public LocationReadRepository(IDbContextProvider<ThexContext> dbContextProvider, ILocalizationManager localizationManager)
            : base(dbContextProvider)
        {
            _localizationManager = localizationManager;
        }

        public ICollection<LocationDto> GetLocationDtoListByPersonId(Guid owerId, string languageIsoCode)
        {
            var defaultLanguage = AppConsts.DEFAULT_CULTURE_INFO_NAME.ToLower();

            var locationDtoListBaseQuery = (from location in this.Context.Locations
                                           join city in Context.CountrySubdivisions on location.CityId equals city.Id into city
                                           from leftCity in city.DefaultIfEmpty()
                                           join state in Context.CountrySubdivisions on leftCity.ParentSubdivision.Id equals state.Id into state
                                           from leftState in state.DefaultIfEmpty()
                                           join country in Context.CountrySubdivisions on leftState.ParentSubdivision.Id equals country.Id into country
                                           from leftCountry in country.DefaultIfEmpty()
                                           join cityTranslation in Context.CountrySubdivisionTranslations.Where(c => c.LanguageIsoCode.ToLower() == languageIsoCode || c.LanguageIsoCode.ToLower() == defaultLanguage).OrderBy(t => t.LanguageIsoCode == languageIsoCode).ThenBy(tb => tb.LanguageIsoCode)
                                           on leftCity.Id equals cityTranslation.CountrySubdivisionId into cityTranslation from leftCityTranslation in cityTranslation.DefaultIfEmpty().Take(1)
                                            join stateTranslation in Context.CountrySubdivisionTranslations.Where(s => s.LanguageIsoCode.ToLower() == languageIsoCode || s.LanguageIsoCode.ToLower() == defaultLanguage).OrderBy(t => t.LanguageIsoCode == languageIsoCode).ThenBy(tb => tb.LanguageIsoCode)
                                           on leftState.Id equals stateTranslation.CountrySubdivisionId into stateTranslation from leftStateTranslation in stateTranslation.DefaultIfEmpty().Take(1)
                                            join countryTranslation in Context.CountrySubdivisionTranslations.Where(c => c.LanguageIsoCode.ToLower() == languageIsoCode || c.LanguageIsoCode.ToLower() == defaultLanguage).OrderBy(t => t.LanguageIsoCode == languageIsoCode).ThenBy(tb => tb.LanguageIsoCode)
                                           on leftCountry.Id equals countryTranslation.CountrySubdivisionId into countryTranslation from leftCountryTranslation in countryTranslation.DefaultIfEmpty().Take(1)
                                            join locationCategory in Context.LocationCategories on location.LocationCategoryId equals locationCategory.Id
                                           where location.OwnerId == owerId
                                           select new LocationDto
                                           {
                                               OwnerId = location.OwnerId,
                                               LocationCategoryId = location.LocationCategoryId,
                                               Latitude = location.Latitude,
                                               Longitude = location.Longitude,
                                               StreetName = location.StreetName,
                                               StreetNumber = location.StreetNumber,
                                               AdditionalAddressDetails = location.AdditionalAddressDetails,
                                               PostalCode = location.PostalCode,
                                               CountryCode = location.CountryCode,
                                               Neighborhood = location.Neighborhood,
                                               Division = leftStateTranslation.Name,
                                               Subdivision = leftCityTranslation.Name,
                                               CountryId = location.CountryId,
                                               Country = leftCountryTranslation.Name,
                                               Id = location.Id,
                                               LocationCategory = new LocationCategoryDto
                                               {
                                                   Id = locationCategory.Id
                                               }
                                           }).ToList();

            foreach (var location in locationDtoListBaseQuery)
                location.LocationCategory.Name = _localizationManager.GetString(AppConsts.LocalizationSourceName, ((LocationCategoryEnum)location.LocationCategory.Id).ToString());

            return locationDtoListBaseQuery.ToList();
        }

        public LocationDto GetLocationDtoListByPropertyUId(int pPropertyId)
        {
            var locationDtoListBaseQuery = (from location in this.Context.Locations
                                            join city in Context.CountrySubdivisions on location.CityId equals city.Id
                                            join properties in Context.Properties on location.OwnerId equals properties.PropertyUId
                                            join state in Context.CountrySubdivisions on city.ParentSubdivision.Id equals state.Id
                                            where
                                            properties.Id == pPropertyId
                                            select new LocationDto
                                            {
                                                OwnerId = location.OwnerId,
                                                LocationCategoryId = location.LocationCategoryId,
                                                Latitude = location.Latitude,
                                                Longitude = location.Longitude,
                                                StreetName = location.StreetName,
                                                StreetNumber = location.StreetNumber,
                                                AdditionalAddressDetails = location.AdditionalAddressDetails,
                                                PostalCode = location.PostalCode,
                                                CountryCode = location.CountryCode,
                                                Neighborhood = location.Neighborhood,
                                                Country = location.CountryCode,
                                                Id = location.Id,
                                                State = state.MapTo<CountrySubdivisionDto>()
                                            }).FirstOrDefault();

            return locationDtoListBaseQuery;
        }

        public ICollection<LocationDto> GetLocationDtoListByFilters(List<int> propertyIds, string languageIsoCode)
        {
            var defaultLanguage = AppConsts.DEFAULT_CULTURE_INFO_NAME.ToLower();

            var locationDtoListBaseQuery = (from location in this.Context.Locations
                                            join property in Context.Properties.Where(p => propertyIds.Contains(p.Id)).IgnoreQueryFilters() 
                                            on   location.OwnerId equals property.PropertyUId
                                            join city in Context.CountrySubdivisions on location.CityId equals city.Id into city
                                            from leftCity in city.DefaultIfEmpty()
                                            join state in Context.CountrySubdivisions on leftCity.ParentSubdivision.Id equals state.Id into state
                                            from leftState in state.DefaultIfEmpty()
                                            join country in Context.CountrySubdivisions on leftState.ParentSubdivision.Id equals country.Id into country
                                            from leftCountry in country.DefaultIfEmpty()
                                            join cityTranslation in Context.CountrySubdivisionTranslations.Where(c => c.LanguageIsoCode.ToLower() == languageIsoCode || c.LanguageIsoCode.ToLower() == defaultLanguage).OrderBy(t => t.LanguageIsoCode == languageIsoCode).ThenBy(tb => tb.LanguageIsoCode)
                                            on leftCity.Id equals cityTranslation.CountrySubdivisionId into cityTranslation
                                            from leftCityTranslation in cityTranslation.DefaultIfEmpty().Take(1)
                                            join stateTranslation in Context.CountrySubdivisionTranslations.Where(s => s.LanguageIsoCode.ToLower() == languageIsoCode || s.LanguageIsoCode.ToLower() == defaultLanguage).OrderBy(t => t.LanguageIsoCode == languageIsoCode).ThenBy(tb => tb.LanguageIsoCode)
                                            on leftState.Id equals stateTranslation.CountrySubdivisionId into stateTranslation
                                            from leftStateTranslation in stateTranslation.DefaultIfEmpty().Take(1)
                                            join countryTranslation in Context.CountrySubdivisionTranslations.Where(c => c.LanguageIsoCode.ToLower() == languageIsoCode || c.LanguageIsoCode.ToLower() == defaultLanguage).OrderBy(t => t.LanguageIsoCode == languageIsoCode).ThenBy(tb => tb.LanguageIsoCode)
                                            on leftCountry.Id equals countryTranslation.CountrySubdivisionId into countryTranslation
                                            from leftCountryTranslation in countryTranslation.DefaultIfEmpty().Take(1)
                                            join locationCategory in Context.LocationCategories on location.LocationCategoryId equals locationCategory.Id
                                            select new LocationDto
                                            {
                                                OwnerId = location.OwnerId,
                                                LocationCategoryId = location.LocationCategoryId,
                                                Latitude = location.Latitude,
                                                Longitude = location.Longitude,
                                                StreetName = location.StreetName,
                                                StreetNumber = location.StreetNumber,
                                                AdditionalAddressDetails = location.AdditionalAddressDetails,
                                                PostalCode = location.PostalCode,
                                                CountryCode = location.CountryCode,
                                                Neighborhood = location.Neighborhood,
                                                Division = leftStateTranslation.Name,
                                                Subdivision = leftCityTranslation.Name,
                                                Country = leftCountryTranslation.Name,
                                                Id = location.Id,
                                                LocationCategory = new LocationCategoryDto
                                                {
                                                    Id = locationCategory.Id
                                                }
                                            }).ToList();

            foreach (var location in locationDtoListBaseQuery)
                location.LocationCategory.Name = _localizationManager.GetString(AppConsts.LocalizationSourceName, ((LocationCategoryEnum)location.LocationCategory.Id).ToString());

            return locationDtoListBaseQuery;
        }
    }
}