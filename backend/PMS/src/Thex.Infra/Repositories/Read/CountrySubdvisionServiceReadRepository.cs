﻿using Thex.Domain.Entities;
using Thex.Dto;
using Thex.Infra.ReadInterfaces;
using Tnf.Dto;
using Tnf.EntityFrameworkCore.Repositories;
using Tnf.EntityFrameworkCore;
using System.Linq;
using Thex.Infra.Context;

namespace Thex.Infra.Repositories.Read
{
    public class CountrySubdvisionServiceReadRepository : EfCoreRepositoryBase<ThexContext, CountrySubdvisionService>, ICountrySubdvisionServiceReadRepository
    {
        
        public CountrySubdvisionServiceReadRepository(
            IDbContextProvider<ThexContext> dbContextProvider) : base(dbContextProvider)
        {
            
        }
        public ListDto<CountrySubdvisionServiceDto> GetAllByPropertyId(int propertyId)
        {

            var dbBaseQuery = (from countrySubdvisionServices in Context.CountrySubdvisionServices
                               join countrySubdivision in Context.CountrySubdivisions on countrySubdvisionServices.CityId equals countrySubdivision.Id
                               join location in Context.Locations on countrySubdivision.Id equals location.CityId
                               join property in Context.Properties on location.OwnerId equals property.PropertyUId
                               where property.Id == propertyId
                               select new CountrySubdvisionServiceDto
                               {
                                   Id = countrySubdvisionServices.Id,
                                   Code = countrySubdvisionServices.Code,
                                   Description = countrySubdvisionServices.Description
                               });

            return new ListDto<CountrySubdvisionServiceDto>
            {
                HasNext = false,
                Items = dbBaseQuery.ToList(),
            };

        }
    }
}
