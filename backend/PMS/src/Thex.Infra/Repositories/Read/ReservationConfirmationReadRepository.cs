﻿//  <copyright file="ReservationConfirmationReadRepository.cs" company="TOTVS">
//  Copyright (c) TOTVS. All rights reserved.
//  </copyright>
namespace Thex.EntityFrameworkCore.Repositories
{
    using System;
    using System.Collections.Generic;

    using Thex.Domain.Entities;
    using Thex.Infra.ReadInterfaces;
    using System.Linq;
    using Tnf.EntityFrameworkCore.Repositories;
    using Tnf.EntityFrameworkCore;

    using Microsoft.EntityFrameworkCore;
    using Thex.Common.Enumerations;
    using Thex.Infra.Context;

    public class ReservationConfirmationReadRepository : EfCoreRepositoryBase<ThexContext, ReservationConfirmation>, IReservationConfirmationReadRepository
    {
        

        public ReservationConfirmationReadRepository(
            IDbContextProvider<ThexContext> dbContextProvider)
            : base(dbContextProvider)
        {
            
        }

        public bool AnyToBeBilledByReservation(long reservationId)
        {
            return Context
                    .ReservationConfirmations
                    
                    .Any(e => e.ReservationId == reservationId && e.PaymentTypeId == (int)PaymentTypeEnum.TobeBilled);
        }

        public bool AnyToBeBilledByBillingAccountId(Guid billingAccountId)
        {
            return (from r in Context.ReservationConfirmations
                    
                    join b in Context.BillingAccounts.Where(bw => bw.ReservationId.HasValue)
                    on r.ReservationId equals b.ReservationId
                    where b.Id == billingAccountId &&
                    r.PaymentTypeId == (int)PaymentTypeEnum.TobeBilled
                    select r).Any();
        }
    }
}