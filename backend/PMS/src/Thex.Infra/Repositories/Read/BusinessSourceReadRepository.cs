﻿// //  <copyright file="BusinessSourceReadRepository.cs" company="TOTVS">
// //  Copyright (c) TOTVS. All rights reserved.
// //  </copyright>
namespace Thex.EntityFrameworkCore.Repositories
{
    using System.Collections.Generic;
    using System.Linq;

    using Thex.Domain.Entities;
    using Thex.Infra.ReadInterfaces;

    using Thex.Dto;

    using Tnf.Dto;
    using Tnf.EntityFrameworkCore.Repositories;
    using Tnf.EntityFrameworkCore;
    using Thex.Infra.Context;
    using Microsoft.EntityFrameworkCore;

    public class BusinessSourceReadRepository : EfCoreRepositoryBase<ThexContext, BusinessSource>, IBusinessSourceReadRepository
    {

        public BusinessSourceReadRepository(IDbContextProvider<ThexContext> dbContextProvider)
            : base(dbContextProvider)
        {


        }

        public IListDto<BusinessSourceDto> GetAllByChainId(GetAllBusinessSourceDto request, int chainId)
        {
            var dbBaseQuery = SearchBusinessSourceByChainIdBaseQuery(request, chainId);

            var totalItemsDto = dbBaseQuery.Count();

            //var resultDto = dbBaseQuery.SkipAndTakeByRequestDto(request).OrderByRequestDto(request).ToList();
            var resultDto = dbBaseQuery.ToList();

            var result =
                new ListDto<BusinessSourceDto>
                {
                    Items = resultDto,
                    HasNext = SearchBusinessSourceHasNext(
                            request,
                            totalItemsDto,
                            resultDto),
                };

            return result;
        }

        public bool ValidateBusinessSourceWithReservation(int businessSourceId, int propertyId)
        {
            return Context.Reservations.AsNoTracking().Any(b => b.BusinessSourceId.Equals(businessSourceId) && b.PropertyId.Equals(propertyId));
        }

        public bool ValidateIfDescriptionAlreadyExists(BusinessSourceDto dto, int chainId)
        {
            var businessSourceIds = Context.ChainBusinessSources.Where(c => c.ChainId.Equals(chainId)).Select(c => c.BusinessSourceId);

            return Context.BusinessSources.Any(b => businessSourceIds.Contains(b.Id) && b.Description.ToLower().Equals(dto.Description.ToLower()) && !b.Id.Equals(dto.Id));
        }

        private IQueryable<BusinessSourceDto> SearchBusinessSourceByChainIdBaseQuery(GetAllBusinessSourceDto request, int chainId)
        {
            var dbBaseQuery = from businessSource in this.Context.BusinessSources
                              join chainBusinessSource in this.Context.ChainBusinessSources on
                              new { c1 = businessSource.Id, c2 = chainId } equals
                              new { c1 = chainBusinessSource.BusinessSourceId, c2 = chainBusinessSource.ChainId }
                              select new BusinessSourceDto
                              {
                                  Id = businessSource.Id,
                                  Description = businessSource.Description,
                                  IsActive = businessSource.IsActive,
                                  Code = businessSource.Code
                              };
            if (!request.BringsInactive)
            {
                dbBaseQuery = dbBaseQuery.Where(b => b.IsActive);
            }

            return dbBaseQuery;
        }

        private bool SearchBusinessSourceHasNext(
            GetAllBusinessSourceDto request,
            int totalItemsDto,
            List<BusinessSourceDto> resultDto)
        {
            return totalItemsDto > ((request.Page - 1) * request.PageSize) + resultDto.Count();
        }
    }
}