﻿//  <copyright file="PersonReadRepository.cs" company="TOTVS">
//  Copyright (c) TOTVS. All rights reserved.
//  </copyright>
namespace Thex.EntityFrameworkCore.Repositories
{
    using System;
    using Thex.Domain.Entities;
    using Thex.Infra.ReadInterfaces;
    using System.Linq;
    using Tnf.Dto;
    using Tnf.EntityFrameworkCore.Repositories;
    using Tnf.EntityFrameworkCore;
    using Thex.Dto.Person;
    using Thex.Common.Enumerations;
    using System.Collections.Generic;
    using Microsoft.EntityFrameworkCore;
    using Thex.Infra.Context;
    using System.Threading.Tasks;
    using Thex.Dto;
    using Tnf.Localization;
    using Thex.Common;
    using Thex.Kernel;

    public class PropertyMealPlanTypeRateReadRepository : EfCoreRepositoryBase<ThexContext, PropertyMealPlanTypeRate>, IPropertyMealPlanTypeRateReadRepository
    {
        private readonly ILocalizationManager _localizationManager;
        private readonly IApplicationUser _applicationUser;

        public PropertyMealPlanTypeRateReadRepository(
            IDbContextProvider<ThexContext> dbContextProvider,
            ILocalizationManager localizationManager,
            IApplicationUser applicationUser
        )
            : base(dbContextProvider)
        {
            _localizationManager = localizationManager;
            _applicationUser = applicationUser;
        }

        public List<PropertyMealPlanTypeRate> GetAllEntity(GetAllPropertyMealPlanTypeRateDto dto)
        {
            var baseQuery = Context.PropertyMealPlanTypeRates
                .Include(x => x.Currency)
                .Include(x => x.MealPlanType)
                .Include(x => x.PropertyMealPlanTypeRateHeader)
                .Where(x => !x.IsDeleted && x.PropertyId == int.Parse(_applicationUser.PropertyId));

            if (dto.StartDate != null)
            {
                baseQuery = baseQuery.Where(x => x.Date.Date >= dto.StartDate.Value.Date);
            }

            if (dto.EndDate != null)
            {
                baseQuery = baseQuery.Where(x => x.Date.Date <= dto.EndDate.Value.Date);
            }

            if (dto.CurrencyId != null)
            {
                baseQuery = baseQuery.Where(x => x.CurrencyId == dto.CurrencyId.Value);
            }

            var result = baseQuery.ToList();
            foreach (var item in result)
            {
                item.MealPlanType.MealPlanTypeName = _localizationManager.GetString(AppConsts.LocalizationSourceName, ((MealPlanTypeEnum)item.MealPlanType.Id).ToString());
            }

            return result;
        }

        public IListDto<PropertyMealPlanTypeRateDto> GetAll(GetAllPropertyMealPlanTypeRateDto dto)
        {
            return GetAllEntity(dto).ToListDto<PropertyMealPlanTypeRate, PropertyMealPlanTypeRateDto>(false);
        }

        public PropertyMealPlanTypeRateHeaderDto GetById(Guid id)
        {
            var baseQuery = Context.PropertyMealPlanTypeRateHeaders
                  .Include(x => x.Currency)
                  .Include(x => x.PropertyMealPlanTypeRateHistoryList)
                  .ThenInclude(k => k.MealPlanType)
                  .Where(x => !x.IsDeleted);

            return baseQuery.FirstOrDefault(x => x.Id == id).MapTo<PropertyMealPlanTypeRateHeaderDto>();
        }

        public bool CheckConstraints(GetAllPropertyMealPlanTypeRateDto dto)
        {
            return Context.PropertyMealPlanTypeRates
                    .Any(x => x.IsActive
                        && x.PropertyId == dto.PropertyId
                        && x.CurrencyId == dto.CurrencyId
                        && (x.Date.Date >= dto.StartDate.Value.Date && x.Date.Date <= dto.EndDate.Value.Date)
                        && dto.MealPlanTypeList.Any(d => d == x.MealPlanTypeId));
        }

        public bool ExistsForAllPeriod(Guid currencyId, int propertyId, List<DateTime> dates)
        {
            var baseQueryDates = Context.PropertyMealPlanTypeRates.Where(pmptr => pmptr.CurrencyId == currencyId && pmptr.PropertyId == propertyId).Select(pmptr => pmptr.Date).Distinct();

            return dates.All(d => baseQueryDates.Contains(d));
;        }
    }
}