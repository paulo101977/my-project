﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Thex.Domain.Entities;
using Thex.Dto;
using Thex.Infra.Context;
using Thex.Infra.ReadInterfaces;
using Tnf.EntityFrameworkCore;
using Tnf.EntityFrameworkCore.Repositories;

namespace Thex.Infra.Repositories.Read
{
    public class GuestRegistrationDocumentReadRepository : EfCoreRepositoryBase<ThexContext, GuestRegistrationDocument>, IGuestRegistrationDocumentReadRepository
    {
        public GuestRegistrationDocumentReadRepository(IDbContextProvider<ThexContext> dbContextProvider)
            : base(dbContextProvider)
        {
        }

        public ICollection<GuestRegistrationDocument> GetAllGuestRegistrationDocumentByGuestRegistrationId(Guid guestRegistrationId)
            => Context.GuestRegistrationDocuments.Where(grd => grd.GuestRegistrationId == guestRegistrationId).ToList();

        public GuestRegistrationDocument GetById(Guid id)
            => Context.GuestRegistrationDocuments.FirstOrDefault(grd => grd.Id == id);

        public ICollection<GuestRegistrationDocumentDto> GetAllDocumentsByGuestRegistrationId(Guid guestRegistrationId)
            => Context.GuestRegistrationDocuments.Include(grd => grd.DocumentType).Where(grd => grd.GuestRegistrationId == guestRegistrationId)
                                                     .Select(grd => new GuestRegistrationDocumentDto
                                                     {
                                                         Id = grd.Id,
                                                         DocumentInformation = grd.DocumentInformation,
                                                         DocumentTypeId = grd.DocumentTypeId,
                                                         DocumentTypeName = grd.DocumentType.Name
                                                     }).ToList();
    }
}
