﻿// //  <copyright file="ChainReadRepository.cs" company="TOTVS">
// //  Copyright (c) TOTVS. All rights reserved.
// //  </copyright>
using Thex.Domain.Entities;
using Thex.Infra.ReadInterfaces;

namespace Thex.EntityFrameworkCore.Repositories
{
    using Microsoft.EntityFrameworkCore;
    using System.Linq;
    using Tnf.EntityFrameworkCore.Repositories;
    using Tnf.EntityFrameworkCore;
    using Thex.Infra.ReadInterfaces;
    using Thex.Infra.Context;

    public class ChainReadRepository : EfCoreRepositoryBase<ThexContext, Chain>, IChainReadRepository
    {
        

        public ChainReadRepository(IDbContextProvider<ThexContext> dbContextProvider)
            : base(dbContextProvider)
        {
            
        }

        public Chain GetChainByPropertyId(int propertyId)
        {        
            Property property = Context.Properties.Where(p => p.Id == propertyId)             
                .Include(p => p.Brand)
                .ThenInclude(b => b.Chain).FirstOrDefault();

            return property?.Brand?.Chain;
        }

        public Chain GetChainById(int id)
        {
            var chain = Context.Chains.FirstOrDefault(x => x.Id == id);
            return chain;
        }
    }
}