﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using System;
using Thex.Infra.Context;
using Thex.Infra.ReadInterfaces;
using Tnf.EntityFrameworkCore;
using Tnf.EntityFrameworkCore.Repositories;

namespace Thex.Infra.Repositories.Read
{
    public class PowerBiReadRepository : EfCoreRepositoryBase<ThexContext, ThexContext>, IPowerBiReadRepository
    {

        public PowerBiReadRepository(
            IDbContextProvider<ThexContext> dbContextProvider) : base(dbContextProvider)
        {
        }


        public string GetGroupIdByDashboardId(string DashboardId, Guid? tenantId)
        {
            var GroupdId = "";

            var contextDatabase = Context.Database;
            var conn = contextDatabase.GetDbConnection();

            var tenant = tenantId != null ? $"and d.TenantId = {tenantId}" : "";

            if (conn.State == System.Data.ConnectionState.Closed)
                conn.Open();

            using (var command = conn.CreateCommand())
            {
                var query = $"SELECT b.PowerBiGroupKey FROM PowerBiGroupReportDashboard a " +
                            $"JOIN PowerBiGroup b " +
                            $"ON a.PowerBiGroupId = b.PowerBiGroupId " +
                            $"JOIN PowerBiDashboard c " +
                            $"ON a.PowerBiDashboardId = c.PowerBiDashboardId " +
                            $"WHERE c.PowerBiDashboardKey = '{DashboardId}' {tenant}";

                if (contextDatabase.CurrentTransaction != null)
                    command.Transaction = contextDatabase.CurrentTransaction.GetDbTransaction();

                command.CommandText = query;

                var reader = command.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        GroupdId = reader.GetValue(0).ToString();
                    }
                }

                reader.Dispose();

            }

            return GroupdId;

        }

        public string GetGroupIdByReportId(string ReportId, Guid? tenantId)
        {
            var GroupdId = "";

            var contextDatabase = Context.Database;
            var conn = contextDatabase.GetDbConnection();

            var tenant = tenantId != null ? $"and d.TenantId = {tenantId}" : "";

            if (conn.State == System.Data.ConnectionState.Closed)
                conn.Open();

            using (var command = conn.CreateCommand())
            {
                var query = $"SELECT b.PowerBiGroupKey FROM PowerBiGroupReportDashboard a " +
                             $"JOIN PowerBiGroup b " +
                             $"ON a.PowerBiGroupId = b.PowerBiGroupId " +
                             $"JOIN PowerBiReport c " +
                             $"ON a.PowerBiReportId = c.PowerBiReportId " +
                             $"LEFT JOIN PowerBiTenantConfig d " +
                             $"ON d.PowerBiGroupId = b.PowerBiGroupId " +
                             $"WHERE c.PowerBiReportKey = '{ReportId}' {tenant}";

                if (contextDatabase.CurrentTransaction != null)
                    command.Transaction = contextDatabase.CurrentTransaction.GetDbTransaction();

                command.CommandText = query;

                var reader = command.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        GroupdId = reader.GetValue(0).ToString();
                    }
                }

                reader.Dispose();

            }

            return GroupdId;
        }
    }
}
