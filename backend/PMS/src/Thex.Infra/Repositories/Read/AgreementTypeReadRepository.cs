﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Thex.Common;
using Thex.Common.Enumerations;
using Thex.Domain.Entities;
using Thex.Dto;
using Thex.Infra.ReadInterfaces;

using Tnf.Dto;
using Tnf.EntityFrameworkCore.Repositories;
using Tnf.EntityFrameworkCore;
using Tnf.Localization;
using Thex.Infra.Context;

namespace Thex.Infra.Repositories.Read
{
    public class AgreementTypeReadRepository : EfCoreRepositoryBase<ThexContext, AgreementType>, IAgreementTypeReadRepository
    {
        private readonly ILocalizationManager _localizationManager;

        public AgreementTypeReadRepository(IDbContextProvider<ThexContext> dbContextProvider, ILocalizationManager localizationManager) : base(dbContextProvider)
        {
            _localizationManager = localizationManager;
        }

        public ListDto<AgreementTypeDto> GetAllAgreementType()
        {
            var dbBaseQuery = Context.AgreementTypes.ToList();


            var agreementTypes = new List<AgreementTypeDto>();

            
            foreach (var agreementType in dbBaseQuery.ToList())
            {
                agreementType.AgreementTypeName = _localizationManager.GetString(AppConsts.LocalizationSourceName, ((AgreementTypeEnum)agreementType.Id).ToString());
                agreementTypes.Add(agreementType.MapTo<AgreementTypeDto>());
            }
                

            return new ListDto<AgreementTypeDto>
            {
                HasNext = false,
                Items = agreementTypes,
            };
        }
    }
}
