﻿// //  <copyright file="DocumentReadRepository.cs" company="TOTVS">
// //  Copyright (c) TOTVS. All rights reserved.
// //  </copyright>
namespace Thex.EntityFrameworkCore.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Thex.Common.Enumerations;
    using Thex.Domain.Entities;
    using Thex.Infra.ReadInterfaces;

    using Thex.Dto;
    using Tnf.EntityFrameworkCore.Repositories;
    using Tnf.EntityFrameworkCore;
    using Thex.Infra.Context;

    public class DocumentReadRepository : EfCoreRepositoryBase<ThexContext, Document>, IDocumentReadRepository
    {
        public DocumentReadRepository(IDbContextProvider<ThexContext> dbContextProvider)
            : base(dbContextProvider)
        {

        }

        public string GetDocumentForPlasticBrand(int billingItemId, DocumentTypeEnum type)
        {
            return (from billingItem in Context.BillingItems
                    join plasticBrandProperty in Context.PlasticBrandProperties on billingItem.PlasticBrandPropertyId equals plasticBrandProperty.Id
                    join plasticBrand in Context.PlasticBrands on plasticBrandProperty.PlasticBrandId equals plasticBrand.Id
                    join document in Context.Documents on plasticBrand.PlasticBrandUid equals document.OwnerId
                    where document.DocumentTypeId == (int)type &&
                          billingItem.Id == billingItemId
                    select document.DocumentInformation).SingleOrDefault();
        }

        public string GetDocument(Guid owerId, DocumentTypeEnum type)
        {
            return (from document in Context.Documents
                    where document.DocumentTypeId == (int)type &&
                          document.OwnerId == owerId
                    select document.DocumentInformation).SingleOrDefault();
        }

        public ICollection<DocumentDto> GetDocumentDtoListByPersonId(Guid owerId)
        {
            var documentDtoListBaseQuery = from document in this.Context.Documents
                                           join documentType in Context.DocumentTypes on document.DocumentTypeId equals documentType.Id
                                           where document.OwnerId == owerId
                                           select new DocumentDto
                                           {
                                               OwnerId = document.OwnerId,
                                               DocumentInformation = document.DocumentInformation,
                                               DocumentTypeId = document.DocumentTypeId,
                                               Id = document.Id,
                                               DocumentType = new DocumentTypeDto
                                               {
                                                   Name = documentType.Name,
                                                   Id = documentType.Id
                                               }
                                           };
            return documentDtoListBaseQuery.ToList();
        }

        public Document GetByOwnerId(Guid ownerId)
        {
            return Context.Documents
                           .FirstOrDefault(exp => exp.OwnerId == ownerId);
        }
    }
}