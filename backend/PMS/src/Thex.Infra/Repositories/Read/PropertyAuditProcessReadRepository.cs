﻿using System;
using System.Linq;
using Thex.Domain.Entities;
using Thex.Infra.ReadInterfaces;
using Tnf.EntityFrameworkCore.Repositories;
using Tnf.EntityFrameworkCore;
using Thex.Infra.Context;

namespace Thex.Infra.Repositories.Read
{
    public class PropertyAuditProcessReadRepository : EfCoreRepositoryBase<ThexContext, PropertyAuditProcess>, IPropertyAuditProcessReadRepository
    {
        

        public PropertyAuditProcessReadRepository(IDbContextProvider<ThexContext> dbContextProvider)
            : base(dbContextProvider)
        {
            
        }

        public PropertyAuditProcess GetPropertyAuditProcessById(Guid id)
        {
            return Context.PropertyAuditProcesss.Where(p => p.Id == id).FirstOrDefault();
        }

        public PropertyAuditProcess GetPropertyAuditProcessCurrentByPropertyId(int propertyId)
        {
            return Context.PropertyAuditProcesss.Where(p => p.EndDate == null && p.PropertyId == propertyId).FirstOrDefault();
        }
    }
}
