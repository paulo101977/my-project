using Thex.Domain.Entities;
using Thex.Infra.ReadInterfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using Tnf.EntityFrameworkCore.Repositories;
using Tnf.EntityFrameworkCore;
using System.Linq;
using Thex.Dto.BillingAccount;
using Tnf.Dto;
using Thex.Common.Enumerations;
using Thex.Dto;
using Thex.Domain.JoinMap;

using Thex.Infra.Context;
using Thex.Common;
using Tnf.Localization;
using System.Data.SqlTypes;
using Microsoft.EntityFrameworkCore.Storage;
using Thex.Kernel;

namespace Thex.Infra.Repositories.Read
{
    public class BillingAccountReadRepository : EfCoreRepositoryBase<ThexContext, BillingAccount>, IBillingAccountReadRepository, Domain.Interfaces.Repositories.Read.IBillingAccountReadRepository
    {
        private readonly ILocalizationManager _localizationManager;
        private readonly string _billingAccountDefaultName;
        private readonly IApplicationUser _applicationUser;

        public BillingAccountReadRepository(IDbContextProvider<ThexContext> dbContextProvider, ILocalizationManager localizationManager,
            IApplicationUser applicationUser)
            : base(dbContextProvider)
        {
            _localizationManager = localizationManager;
            _billingAccountDefaultName = _localizationManager.GetString(AppConsts.LocalizationSourceName, BillingAccountEnum.Default.BillingAccountDefaultName.ToString());
            _applicationUser = applicationUser;
        }


        public IListDto<SearchBillingAccountResultDto> GetAllByFiltersV2(SearchBillingAccountDto request, int propertyId)
        {
            var resultList = new ListDto<SearchBillingAccountResultDto>();

            var contextDatabase = Context.Database;
            var conn = contextDatabase.GetDbConnection();

            if (conn.State == System.Data.ConnectionState.Closed)
                conn.Open();

            using (var command = conn.CreateCommand())
            {
                var query = $"select                                                                                                                      " +
                            $"distinct                                                                                                                    " +
                            $"	ba.GroupKey as BillingAccountId,                                                                                          " +
                            $"	ba.GroupKey as Id,                                                                                                        " +
                            $"	ba.BillingAccountTypeId as BillingAccountTypeId,                                                                          " +
                            $"	coalesce(cc.TradeName, gri.GuestName) as HolderName,                                                                      " +
                            $"	rt.Abbreviation as RoomTypeName,                                                                                          " +
                            $"	ro.RoomNumber as RoomNumber,                                                                                              " +
                            $"	r.ReservationCode as ReservationCode,                                                                                     " +
                            $"	coalesce(ri.ReservationItemStatusId, -1) as ReservationStatusId,                                                          " +
                            $"	ba.GroupKey as GroupKey,                                                                                                  " +
                            $"	coalesce(balance_ba.Balance, 0) as Balance,                                                                               " +
                            $"	c.Symbol as CurrencySymbol,                                                                                               " +
                            $"	coalesce(c.CurrencyId, '00000000-0000-0000-0000-000000000000') as CurrencyId,                                             " +
                            $"	coalesce(gri.CheckInDate, gri.EstimatedArrivalDate, ri.CheckInDate, ri.EstimatedArrivalDate) as ArrivalDate,              " +
                            $"	coalesce(gri.CheckOutDate, gri.EstimatedDepartureDate, ri.CheckOutDate, ri.EstimatedDepartureDate) as DepartureDate,      " +
                            $"	coalesce(open_ba.AnyOpenAccount, 0) as AnyOpenAccount,                                                                    " +
                            $"	coalesce(closed_ba.AnyClosedAccount, 0) as AnyClosedAccount,                                                              " +
                            $"  coalesce(pending_ba.anypendingaccount, 0) as AnyPendingAccount                                                            " +
                            $"from                                                                                                                        " +
                            $"BillingAccount ba                                                                                                           " +
                            $"left join BillingAccountItem bai on bai.BillingAccountId = ba.BillingAccountId and bai.IsDeleted = 0                        " +
                            $"left join PropertyParameter pp on ba.PropertyId = pp.PropertyId and 4 = pp.ApplicationParameterId and pp.IsDeleted = 0      " +
                            $"left join Currency  c on pp.PropertyParameterValue = c.CurrencyId and c.IsDeleted = 0                                       " +
                            $"left join Reservation r on ba.ReservationId = r.ReservationId and r.IsDeleted = 0                                           " +
                            $"left join ReservationItem ri on ba.ReservationItemId = ri.ReservationItemId and ri.IsDeleted = 0                            " +
                            $"left join CompanyClient cc on ba.CompanyClientId = cc.CompanyClientId                                                       " +
                            $"left join GuestReservationItem gri on ba.GuestReservationItemId = gri.GuestReservationItemId and gri.IsDeleted = 0          " +
                            $"left join Room ro on ri.RoomId = ro.RoomId and ro.IsDeleted = 0                                                             " +
                            $"left join RoomType rt on ri.ReceivedRoomTypeId = rt.RoomTypeId                                                              " +
                            $"left join Guest g on gri.GuestId = g.GuestId                                                                                " +
                            $"left join ReservationConfirmation rc on r.ReservationId = rc.ReservationId and rc.IsDeleted = 0                             " +
                            $"left join (                                                                                                                 " +
                            $"	select count(*) as AnyOpenAccount, sub_ba1.GroupKey as GroupKey                                                           " +
                            $"	from BillingAccount as sub_ba1                                                                                            " +
                            $"	where sub_ba1.StatusId = 1 and sub_ba1.IsDeleted = 0                                                                      " +
                            $"	group by sub_ba1.GroupKey) open_ba on ba.GroupKey = open_ba.GroupKey                                                      " +
                            $"left join (                                                                                                                 " +
                            $"	select count(*) as AnyClosedAccount, sub_ba1.GroupKey as GroupKey                                                         " +
                            $"	from BillingAccount as sub_ba1                                                                                            " +
                            $"	where sub_ba1.StatusId = 2 and sub_ba1.IsDeleted = 0                                                                      " +
                            $"	group by sub_ba1.GroupKey) closed_ba on ba.GroupKey = closed_ba.GroupKey                                                  " +
                            $"left join (                                                                                                                 " +
                            $"	select count(*) as AnyPendingAccount, sub_ba1.GroupKey as GroupKey                                                        " +
                            $"	from BillingAccount as sub_ba1                                                                                            " +
                            $"  join ReservationItem AS res_ba1 on sub_ba1.ReservationItemId = res_ba1.ReservationItemId and res_ba1.IsDeleted = 0        " +
                            $"	where res_ba1.ReservationItemStatusId = 4 and sub_ba1.IsDeleted = 0                                                       " +
                            $"	group by sub_ba1.GroupKey) pending_ba on ba.GroupKey = pending_ba.GroupKey                                                " +
                            $"left join (                                                                                                                 " +
                            $"	select SUM(sub_bai1.Amount) as Balance, sub_ba1.GroupKey as GroupKey                                                      " +
                            $"		from BillingAccountItem as sub_bai1                                                                                   " +
                            $"		inner join BillingAccount as sub_ba1 on sub_bai1.BillingAccountId =  sub_ba1.BillingAccountId and sub_bai1.IsDeleted = 0 " +
                            $"		group by sub_ba1.GroupKey) balance_ba on ba.GroupKey = balance_ba.GroupKey                                            " +
                            $" where ba.propertyid = {propertyId} and ba.IsDeleted = 0                                                    				  ";


                if (request.OpenedAccounts.HasValue && request.OpenedAccounts.Value)
                    query = query + $" and open_ba.AnyOpenAccount > 0";

                if (request.ClosedAccounts.HasValue && request.ClosedAccounts.Value)
                    query = query + $" and closed_ba.AnyClosedAccount > 0 ";

                if (request.PendingAccounts.HasValue && request.PendingAccounts.Value)
                    query = query + $" and pending_ba.AnyPendingAccount > 0 ";

                if (!string.IsNullOrEmpty(request.ReservationCode))
                    query = query + $" and LOWER(r.ReservationCode) like '%{request.ReservationCode.ToLower()}%'";

                if (!string.IsNullOrEmpty(request.AccountTypeName))
                    query = query + $" and LOWER(ba.BillingAccountTypeId) like '%{request.AccountTypeName.ToLower()}%'";

                if (!string.IsNullOrEmpty(request.BillingAccountName))
                    query = query + $" and LOWER(ba.BillingAccountName) like '%{request.BillingAccountName.ToLower()}%'";

                if (!string.IsNullOrEmpty(request.RoomTypeName))
                    query = query + $" and LOWER(rt.Abbreviation) like '%{request.RoomTypeName.ToLower()}%'";

                if (!string.IsNullOrEmpty(request.FreeTextSearch))
                    query = query + $" and (LOWER(ba.BillingAccountName) like '%{request.FreeTextSearch.ToLower()}%' " +
                        $" or LOWER(r.ReservationCode) like '%{request.FreeTextSearch.ToLower()}%' " +
                        $" or LOWER(coalesce(cc.TradeName, gri.GuestName)) like '%{request.FreeTextSearch.ToLower()}%' " +
                        $" or LOWER(ro.RoomNumber) like '%{request.FreeTextSearch.ToLower()}%' " +
                        $" )";

                if (request.BillingAccountIgnoredList != null && request.BillingAccountIgnoredList.Count > 0)
                    query = query + $" and ba.GroupKey not in ({string.Join(",", request.BillingAccountIgnoredList.Select(e => "'" + e.ToString() + "'"))})";

                if (request.BaseAccountIgnored.HasValue)
                    query = query + $" and ba.GroupKey <> '{request.BaseAccountIgnored.Value.ToString()}'";

                if (request.AccountStatus.HasValue)
                    query = query + $" and ba.StatusId = {request.AccountStatus.Value}";

                query = query + $" order by ro.RoomNumber, ba.BillingAccountTypeId ";

                if (contextDatabase.CurrentTransaction != null)
                    command.Transaction = contextDatabase.CurrentTransaction.GetDbTransaction();

                command.CommandText = query;

                var reader = command.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        var resultDto = new SearchBillingAccountResultDto()
                        {
                            BillingAccountId = reader.GetValue(0).ToString(),
                            Id = Guid.Parse(reader.GetValue(1).ToString()),
                            AccountTypeId = Convert.ToInt32(reader.GetValue(2).ToString()),
                            HolderName = reader.GetValue(3).ToString(),
                            BillingAccountDefaultName = _billingAccountDefaultName,
                            RoomTypeName = reader.GetValue(4).ToString(),
                            RoomNumber = reader.GetValue(5).ToString(),
                            ReservationCode = reader.GetValue(6).ToString(),
                            ReservationStatusId = (int?)(reader.IsDBNull(7) ? null : reader[7]),
                            GroupKey = Guid.Parse(reader.GetValue(8).ToString()),
                            Balance = Convert.ToDecimal(reader.GetValue(9)),
                            CurrencySymbol = reader.GetValue(10).ToString(),
                            CurrencyId = Guid.Parse(reader.GetValue(11).ToString()),
                            ArrivalDate = (DateTime?)(reader.IsDBNull(12) ? null : reader[12]),
                            DepartureDate = (DateTime?)(reader.IsDBNull(13) ? null : reader[13]),
                            AnyOpenAccount = Convert.ToInt32(reader.GetValue(14)) > 0 ? true : false,
                            AnyClosedAccount = Convert.ToInt32(reader.GetValue(15)) > 0 ? true : false,
                            AnyPendingAccount = Convert.ToInt32(reader.GetValue(16)) > 0 ? true : false
                        };

                        resultDto.AccountTypeName = _localizationManager.GetString(AppConsts.LocalizationSourceName, ((BillingAccountTypeEnum)resultDto.AccountTypeId).ToString());

                        if (resultDto.ReservationStatusId.HasValue)
                            resultDto.ReservationStatusName = resultDto.ReservationStatusId == -1 ? null : _localizationManager.GetString(AppConsts.LocalizationSourceName, ((ReservationStatus)resultDto.ReservationStatusId).ToString());

                        resultList.Items.Add(resultDto);
                    }
                }

                reader.Dispose();

            }

            return resultList;
        }

        public List<BillingAccount> GetAllForGuestReservationItemId(List<long> listGuestReservation, long reservationId)
        {
            if (listGuestReservation == null || listGuestReservation.Count() == 0)
                return null;

            return (from b in Context.BillingAccounts.AsNoTracking()
                    where b.ReservationId == reservationId && b.GuestReservationItemId.HasValue &&
                    listGuestReservation.Contains(b.GuestReservationItemId.Value)
                    select b).ToList();

        }

        public BillingAccountHeaderDto BillingAccountHeaderV2(Guid? billingAccountId)
        {

            if (billingAccountId.HasValue)
            {
                var billingAccounts = (from billingAccount in Context.BillingAccounts.AsNoTracking()
                                       where billingAccount.Id == billingAccountId.Value
                                       select new
                                       {
                                           Id = billingAccount.Id,
                                           GroupKey = billingAccount.GroupKey,
                                           ReservationItemId = billingAccount.ReservationItemId,
                                           BillingAccountTypeId = billingAccount.BillingAccountTypeId,
                                       })
                                       .FirstOrDefault();

                if (billingAccounts != null)
                {
                    var headerList = new List<BillingAccountHeaderDto>();

                    var contextDatabase = Context.Database;
                    var conn = contextDatabase.GetDbConnection();

                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();

                    using (var command = conn.CreateCommand())
                    {
                        var query =
                            $"select																										" +
                            $"	COALESCE(SUM(bai.Amount),0) as PriceTotal,                                                                            " +
                            $"	ri.reservationItemCode as ReservationItemCode,                                                              " +
                            $"	r.ExternalComments as ExternalComments,                                                                     " +
                            $"	r.PartnerComments as PartnerComments,                                                                       " +
                            $"	r.InternalComments as InternalComments,                                                                     " +
                            $"	coalesce(ri.ReservationItemStatusId, 0) as ReservationStatusId,                                            " +
                            $"	r.ReservationCode as ReservationCode,                                                                       " +
                            $"	ro.RoomNumber as RoomNumber,                                                                                " +
                            $"	coalesce(ri.ReservationItemId, 0) as ReservationItemId,                                                     " +
                            $"	ba.StatusId as BillingAccountStatusId,                                                                      " +
                            $"	c.Symbol as CurrencySymbol                                                                                  " +
                            $"from                                                                                                          " +
                            $"BillingAccount ba                                                                                             " +
                            $"left join BillingAccountItem bai on bai.BillingAccountId = ba.BillingAccountId and bai.IsDeleted = 0          " +
                            $"left join PropertyParameter pp on ba.PropertyId = pp.PropertyId and 4 = pp.ApplicationParameterId and pp.IsDeleted = 0            " +
                            $"left join Currency  c on pp.PropertyParameterValue = c.CurrencyId and c.IsDeleted = 0                                             " +
                            $"left join Reservation r on ba.ReservationId = r.ReservationId and r.IsDeleted = 0                                                 " +
                            $"left join ReservationItem ri on ba.ReservationItemId = ri.ReservationItemId and ri.IsDeleted = 0                                  " +
                            $"left join CompanyClient cc on ba.CompanyClientId = cc.CompanyClientId                                                             " +
                            $"left join GuestReservationItem gri on ba.GuestReservationItemId = gri.GuestReservationItemId and gri.IsDeleted = 0                " +
                            $"left join Room ro on ri.RoomId = ro.RoomId and ro.IsDeleted = 0                                                                   " +
                            $"left join RoomType rt on ri.ReceivedRoomTypeId = rt.RoomTypeId                                                " +
                            $"left join Guest g on gri.GuestId = g.GuestId                                                                  " +
                            $"left join ReservationConfirmation rc on r.ReservationId = rc.ReservationId and rc.IsDeleted = 0               " +
                            $"                                                                                                              " +
                            $"where ba.tenantid = '{_applicationUser.TenantId}' and ba.IsDeleted = 0                                       ";

                        if (billingAccounts.ReservationItemId.HasValue)
                            query = query + $" and (ba.ReservationItemId = {billingAccounts.ReservationItemId.Value} or";
                        else
                            query = query + $" and (";

                        query = query + $" ba.BillingAccountId = '{billingAccounts.Id}'";
                        query = query + $" or ba.GroupKey = '{billingAccounts.GroupKey}')";

                        query = query + $" group by                                                                                                      " +
                        $"ri.reservationItemCode,                                                                                       " +
                        $"r.ExternalComments,                                                                                           " +
                        $"r.PartnerComments,                                                                                            " +
                        $"r.InternalComments,                                                                                           " +
                        $"ri.ReservationItemStatusId,                                                                                   " +
                        $"r.ReservationCode,                                                                                            " +
                        $"ro.RoomNumber,                                                                                                " +
                        $"ri.ReservationItemId,                                                                                         " +
                        $"c.Symbol,                                                                                                     " +
                        $"ba.StatusId                                                                                                   ";

                        if (contextDatabase.CurrentTransaction != null)
                            command.Transaction = contextDatabase.CurrentTransaction.GetDbTransaction();

                        command.CommandText = query;

                        var reader = command.ExecuteReader();

                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                var resultDto = new BillingAccountHeaderDto()
                                {
                                    priceTotal = Convert.ToDecimal(reader.GetValue(0)),
                                    reservationItemCode = reader.GetValue(1).ToString(),
                                    externalComments = reader.GetValue(2).ToString(),
                                    PartnerComments = reader.GetValue(3).ToString(),
                                    internalComments = reader.GetValue(4).ToString(),
                                    reservationItemStatusId = Convert.ToInt32(reader.GetValue(5).ToString()),
                                    reservationCode = reader.GetValue(6).ToString(),
                                    roomNumber = reader.GetValue(7).ToString(),
                                    ReservationItemId = (long?)(reader.IsDBNull(8) ? null : reader[8]),
                                    BillingAccountStatusId = Convert.ToInt32(reader.GetValue(9).ToString()),
                                    CurrencySymbol = reader.GetValue(10).ToString(),
                                };

                                resultDto.quantityComments = !string.IsNullOrEmpty(resultDto.reservationCode) ? CountComments(resultDto.externalComments, resultDto.internalComments, resultDto.PartnerComments) : 0;
                                resultDto.ReservationItemId = billingAccounts.ReservationItemId;
                                resultDto.GroupKey = billingAccounts.GroupKey;
                                resultDto.BillingAccountTypeId = billingAccounts.BillingAccountTypeId;

                                headerList.Add(resultDto);
                            }
                        }
                        reader.Dispose();
                    }

                    if (headerList != null && headerList.Count() > 0)
                    {
                        var header = headerList.FirstOrDefault();

                        header.statusName = header.reservationItemStatusId > 0 ?
                                                _localizationManager.GetString(AppConsts.LocalizationSourceName, ((ReservationStatus)header.reservationItemStatusId).ToString())
                                                : null;
                        header.priceTotal = headerList.Sum(x => x.priceTotal);
                        header.AllClosed = headerList.Count(e => e.BillingAccountStatusId == (int)AccountBillingStatusEnum.Closed) == headerList.Count();

                        return header;
                    }
                }
            }
            return null;
        }

        private string GetBaseQueryV2(Guid billingAccountId, int type)
        {
            var query =
                    $" SELECT DISTINCT                            ";

            if (type == 0)
            {
                query = query + $" coalesce(gri.isMain, 0) AS isHolder,                                                                             " +
                $" (CASE                                                                                                            " +
                $" WHEN  ba.BillingAccountTypeId < 4 THEN ba.BillingAccountName                                                     " +
                $" ELSE                                                                                                             " +
                $"  (select top 1 sub_ba.BillingAccountName from BillingAccount sub_ba where sub_ba.GroupKey = ba.GroupKey)         " +
                $" END    ) AS HolderName,                                                                                          ";
            }

            if (type == 1)
            {
                query = query + $" 1 as isHolder,                      " +
                                $" NULL as HolderName,                 ";
            }

            if (type == 2)
            {
                query = query + $" (CASE ba.BillingAccountTypeId WHEN 4 THEN 1                                 " +
                                $"     ELSE coalesce(gri.isMain, 1)   END    ) AS isHolder,                    " +
                                $" coalesce(cc.TradeName, gri.PreferredName) AS HolderName,     ";
            }


            if (type == 3)
            {
                query = query + $" (CASE ba.BillingAccountTypeId WHEN 4 THEN 1     " +
                                $" ELSE coalesce(gri.isMain, 1) END    ) AS isHolder,           " +

                                 $" (CASE ba.BillingAccountTypeId WHEN 4 THEN (select top 1 sub_ba.BillingAccountName from BillingAccount sub_ba where sub_ba.GroupKey = ba.GroupKey)     " +
                                $" ELSE coalesce(cc.TradeName, gri.PreferredName)  END    ) AS HolderName,           ";

            }

            if (type == 4)
            {
                query = query + $" 0 AS isHolder,    " +
                                $" coalesce(cc.TradeName, gri.PreferredName) AS HolderName,           ";
            }


            query = query +
                    $" ba.GroupKey AS BillingAccountId,                                                                                         " +
                    $" ba.GroupKey AS Id,                                                                                                       " +
                    $" ba.BillingAccountTypeId AS accountTypeId,                                                                                " +
                    $" coalesce(gri.CheckInDate, gri.EstimatedArrivalDate, ri.CheckInDate, ri.EstimatedArrivalDate) AS ArrivalDate,             " +
                    $" coalesce(gri.CheckOutDate, gri.EstimatedDepartureDate, ri.CheckOutDate, ri.EstimatedDepartureDate) AS DepartureDate,     " +
                    $" coalesce(ri.ReservationItemId, 0) AS ReservatiomItemId,                                                                  " +
                    $" coalesce(ri.ReservationId, 0),                                                                                                        " +
                    $" ba.GroupKey AS GroupKey,                                                                                                 " +
                    $" coalesce(gri.GuestStatusId, 999) AS GuestReservationItemStatusId,                                                        " +
                    $" c.Symbol AS CurrencySymbol,                                                                                              " +
                    $" coalesce(c.CurrencyId, '00000000-0000-0000-0000-000000000000') AS CurrencyId,                                            " +
                    $" rt.Abbreviation AS RoomTypeName,                                                                                         " +
                    $" ro.RoomNumber AS RoomNumber,                                                                                             " +
                    $" r.ReservationCode AS ReservationCode,                                                                                    " +
                    $" coalesce(ri.ReservationItemStatusId, -1) AS ReservationStatusId,                                                         " +
                    $" coalesce(balance_ba.Balance, 0) AS Price,                                                                                " +
                    $" coalesce(open_ba.AnyOpenAccount, 0) AS AnyOpenAccount,                                                                   " +
                    $" coalesce(closed_ba.AnyClosedAccount, 0) AS AnyClosedAccount,                                                             " +
                    $" coalesce(pending_invoice.AnyInvoicePending, 0) AS BillingInvoicePendingStatusId,                                         " +
                    $" coalesce(error_invoice.AnyErrorInvoice, 0) AS BillingInvoiceErrorStatusId,                                               " +
                    $" coalesce(issued_invoice.AnyIssuedInvoice, 0) AS BillingInvoiceIssuedStatusId                                             " +
                    $" FROM BillingAccount ba                                                                                                   " +
                    $" LEFT JOIN BillingAccountItem bai ON bai.BillingAccountId = ba.BillingAccountId                                           " +
                    $" LEFT JOIN PropertyParameter pp ON ba.PropertyId = pp.PropertyId                                                          " +
                    $" AND 4 = pp.ApplicationParameterId                                                                                        " +
                    $" LEFT JOIN Currency c ON pp.PropertyParameterValue = c.CurrencyId                                                         " +
                    $" LEFT JOIN Reservation r ON ba.ReservationId = r.ReservationId                                                            " +
                    $" LEFT JOIN ReservationItem ri ON ba.ReservationItemId = ri.ReservationItemId                                              " +
                    $" LEFT JOIN CompanyClient cc ON ba.CompanyClientId = cc.CompanyClientId                                                    " +
                    $" LEFT JOIN GuestReservationItem gri ON ba.GuestReservationItemId = gri.GuestReservationItemId                             " +
                    $" LEFT JOIN Room ro ON ri.RoomId = ro.RoomId                                                                               " +
                    $" LEFT JOIN RoomType rt ON ri.ReceivedRoomTypeId = rt.RoomTypeId                                                           " +
                    $" LEFT JOIN Guest g ON gri.GuestId = g.GuestId                                                                             " +
                    $" LEFT JOIN ReservationConfirmation rc ON r.ReservationId = rc.ReservationId                                               " +
                    $" LEFT JOIN                                                                                                                " +
                    $"   (SELECT count(*) AS AnyIssuedInvoice,                                                                                  " +
                    $"           sub_ba1.GroupKey AS GroupKey                                                                                   " +
                    $"    FROM BillingInvoice AS sub_bi1                                                                                        " +
                    $"    INNER JOIN BillingAccount sub_ba1 on sub_ba1.BillingAccountId = sub_bi1.BillingAccountId                              " +
                    $"    WHERE sub_bi1.BillingInvoiceStatusId = 10                                                                             " +
                    $"    GROUP BY sub_ba1.GroupKey) issued_invoice ON ba.GroupKey = issued_invoice.GroupKey                                    " +
                    $" LEFT JOIN                                                                                                                " +
                    $"   (SELECT count(*) AS AnyErrorInvoice,                                                                                   " +
                    $"           sub_ba1.GroupKey AS GroupKey                                                                                   " +
                    $"    FROM BillingInvoice AS sub_bi1                                                                                        " +
                    $"    INNER JOIN BillingAccount sub_ba1 on sub_ba1.BillingAccountId = sub_bi1.BillingAccountId                              " +
                    $"    WHERE sub_bi1.BillingInvoiceStatusId = 12                                                                             " +
                    $"    GROUP BY sub_ba1.GroupKey) error_invoice ON ba.GroupKey = error_invoice.GroupKey                                      " +
                    $" LEFT JOIN                                                                                                                " +
                    $"   (SELECT count(*) AS AnyInvoicePending,                                                                                 " +
                    $"           sub_ba1.GroupKey AS GroupKey                                                                                   " +
                    $"    FROM BillingInvoice AS sub_bi1                                                                                        " +
                    $"    INNER JOIN BillingAccount sub_ba1 on sub_ba1.BillingAccountId = sub_bi1.BillingAccountId                              " +
                    $"    WHERE sub_bi1.BillingInvoiceStatusId = 11                                                                             " +
                    $"    GROUP BY sub_ba1.GroupKey) pending_invoice ON ba.GroupKey = pending_invoice.GroupKey                                  " +
                    $" LEFT JOIN                                                                                                                " +
                    $"   (SELECT count(sub_ba1.GroupKey) AS AnyOpenAccount,                                                                     " +
                    $"           sub_ba1.GroupKey AS GroupKey                                                                                   " +
                    $"    FROM BillingAccount AS sub_ba1                                                                                        " +
                    $"    WHERE sub_ba1.StatusId = 1                                                                                            " +
                    $"    GROUP BY sub_ba1.GroupKey) open_ba ON ba.GroupKey = open_ba.GroupKey                                                  " +
                    $" LEFT JOIN                                                                                                                " +
                    $"   (SELECT count(sub_ba1.GroupKey) AS AnyClosedAccount,                                                                   " +
                    $"           sub_ba1.GroupKey AS GroupKey                                                                                   " +
                    $"    FROM BillingAccount AS sub_ba1                                                                                        " +
                    $"    WHERE sub_ba1.StatusId = 2                                                                                            " +
                    $"    GROUP BY sub_ba1.GroupKey) closed_ba ON ba.GroupKey = closed_ba.GroupKey                                              " +
                    $" LEFT JOIN                                                                                                                " +
                    $"   (SELECT SUM(sub_bai1.Amount) AS Balance,                                                                               " +
                    $"           sub_ba1.GroupKey AS GroupKey                                                                                   " +
                    $"    FROM BillingAccountItem AS sub_bai1                                                                                   " +
                    $"    INNER JOIN BillingAccount AS sub_ba1 ON sub_bai1.BillingAccountId = sub_ba1.BillingAccountId                          " +
                    $"      AND sub_bai1.IsDeleted = 0                                                                                          " +
                    $"    GROUP BY sub_ba1.GroupKey) balance_ba ON ba.GroupKey = balance_ba.GroupKey                                            " +
                    $" WHERE ba.tenantid = '{_applicationUser.TenantId}'                                                                         ";

            return query;
        }

        public IListDto<BillingAccountSidebarDto> BillingAccountSidebarV2(Guid billingAccountId)
        {
            var resultList = new List<BillingAccountSidebarDto>();
            List<BillingAccountSidebarDto> dbBaseQueryAll = new List<BillingAccountSidebarDto>();
            BillingAccountSidebarDto dbBaseQuery = null;

            var minDateSql = (DateTime)SqlDateTime.MinValue;

            var contextDatabase = Context.Database;
            var conn = contextDatabase.GetDbConnection();

            if (conn.State == System.Data.ConnectionState.Closed)
                conn.Open();

            using (var command = conn.CreateCommand())
            {
                if (contextDatabase.CurrentTransaction != null)
                    command.Transaction = contextDatabase.CurrentTransaction.GetDbTransaction();

                var baseQuery = GetBaseQueryV2(billingAccountId, 0);

                baseQuery = baseQuery + $"   AND ba.BillingAccountId = '{billingAccountId}'                                                                         " +
                    $"   AND ba.propertyid = {_applicationUser.PropertyId}                                                                       " +
                    $" ORDER BY reservationcode                                                                                                 ";

                command.CommandText = baseQuery;

                var reader = command.ExecuteReader();

                FillReader(resultList, reader);

                reader.Dispose();
            }

            dbBaseQuery = resultList.FirstOrDefault();

            if (resultList.FirstOrDefault().accountTypeId == (int)BillingAccountTypeEnum.GroupAccount)
            {
                using (var command = conn.CreateCommand())
                {
                    if (contextDatabase.CurrentTransaction != null)
                        command.Transaction = contextDatabase.CurrentTransaction.GetDbTransaction();

                    var baseQuery = GetBaseQueryV2(billingAccountId, 1);

                    baseQuery = baseQuery + $"   AND ba.ReservationItemId = {dbBaseQuery.ReservatiomItemId}                                                                         " +
                        $"   AND ba.BillingAccountTypeId = {(int)BillingAccountTypeEnum.GroupAccount}                                                                       " +
                        $" ORDER BY reservationcode                                                                                                 ";

                    command.CommandText = baseQuery;

                    var reader = command.ExecuteReader();

                    FillReader(dbBaseQueryAll, reader);

                    reader.Dispose();
                }
            }
            else if (resultList.FirstOrDefault().accountTypeId == (int)BillingAccountTypeEnum.Company)
            {
                using (var command = conn.CreateCommand())
                {
                    if (contextDatabase.CurrentTransaction != null)
                        command.Transaction = contextDatabase.CurrentTransaction.GetDbTransaction();

                    var baseQuery = GetBaseQueryV2(billingAccountId, 2);

                    baseQuery = baseQuery + $"   AND ba.ReservationItemId = {dbBaseQuery.ReservatiomItemId}   or       " +
                        $" (  ba.ReservationId = {dbBaseQuery.ReservationId}  AND ba.BillingAccountTypeId = {(int)BillingAccountTypeEnum.GroupAccount} ) " +
                        $" ORDER BY reservationcode                                                                                                 ";

                    command.CommandText = baseQuery;

                    var reader = command.ExecuteReader();

                    FillReader(dbBaseQueryAll, reader);

                    reader.Dispose();
                }
            }
            else if (dbBaseQuery.accountTypeId != (int)BillingAccountTypeEnum.Sparse)
            {
                using (var command = conn.CreateCommand())
                {
                    if (contextDatabase.CurrentTransaction != null)
                        command.Transaction = contextDatabase.CurrentTransaction.GetDbTransaction();

                    var baseQuery = GetBaseQueryV2(billingAccountId, 3);

                    baseQuery = baseQuery + $"   AND ( ba.ReservationItemId = {dbBaseQuery.ReservatiomItemId}   or       " +
                        $" (  ba.ReservationId = {dbBaseQuery.ReservationId}  AND ba.BillingAccountTypeId = {(int)BillingAccountTypeEnum.GroupAccount} )) " +
                         $" or (  ba.ReservationItemId = 0 AND ba.ReservationId = {dbBaseQuery.ReservationId} )  " +
                       $" ORDER BY reservationcode                                                                                                 ";

                    command.CommandText = baseQuery;

                    var reader = command.ExecuteReader();

                    FillReader(dbBaseQueryAll, reader);

                    reader.Dispose();
                }

            }
            else if (dbBaseQuery.accountTypeId == (int)BillingAccountTypeEnum.Sparse)
            {
                using (var command = conn.CreateCommand())
                {
                    if (contextDatabase.CurrentTransaction != null)
                        command.Transaction = contextDatabase.CurrentTransaction.GetDbTransaction();

                    var baseQuery = GetBaseQueryV2(billingAccountId, 4);

                    baseQuery = baseQuery + $"   AND ba.GroupKey = '{billingAccountId}'  " +
                        $" ORDER BY reservationcode                                                                                                 ";

                    command.CommandText = baseQuery;

                    var reader = command.ExecuteReader();

                    FillReader(dbBaseQueryAll, reader);

                    reader.Dispose();
                }
            }

            foreach (var queryResult in dbBaseQueryAll)
            {
                queryResult.GuestReservationItemStatusName = !queryResult.GuestReservationItemStatusId.HasValue ? null : _localizationManager.GetString(AppConsts.LocalizationSourceName, ((ReservationStatus)queryResult.GuestReservationItemStatusId.Value).ToString());
            }

            List<BillingAccountSidebarDto> singleAccount = new List<BillingAccountSidebarDto>();

            if (dbBaseQueryAll == null || dbBaseQueryAll.Count() == 0)
            {
                singleAccount.Add(dbBaseQuery);
            }

            var result = new ListDto<BillingAccountSidebarDto>()
            {
                HasNext = false,
                Items = dbBaseQueryAll != null && dbBaseQueryAll.Count() > 0 ? dbBaseQueryAll
                    .OrderByDescending(e => e.isHolder)
                    .ThenByDescending(e => e.accountTypeId == (int)BillingAccountTypeEnum.Guest)
                    .ThenByDescending(e => e.accountTypeId == (int)BillingAccountTypeEnum.Sparse)
                    .ThenByDescending(e => e.accountTypeId == (int)BillingAccountTypeEnum.Company)
                    .ThenByDescending(e => e.accountTypeId == (int)BillingAccountTypeEnum.GroupAccount)
                    .ToList() : singleAccount
            };

            return result;
        }

        private void FillReader(List<BillingAccountSidebarDto> resultList, System.Data.Common.DbDataReader reader)
        {
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    var resultDto = new BillingAccountSidebarDto()
                    {
                        holderName = reader.GetValue(1).ToString(),
                        isHolder = Convert.ToInt32(reader.GetValue(0)) == 0 ? false : true,
                        billingAccountId = Guid.Parse(reader.GetValue(2).ToString()),
                        Id = Guid.Parse(reader.GetValue(3).ToString()),
                        accountTypeId = Convert.ToInt32(reader.GetValue(4).ToString()),
                        arrivalDate = (DateTime?)(reader.IsDBNull(5) ? null : reader[5]),
                        departureDate = (DateTime?)(reader.IsDBNull(6) ? null : reader[6]),
                        ReservatiomItemId = Convert.ToInt32(reader.GetValue(7)),
                        ReservationId = Convert.ToInt32(reader.GetValue(8)),
                        GroupKey = Guid.Parse(reader.GetValue(9).ToString()),
                        GuestReservationItemStatusId = Convert.ToInt32(reader.GetValue(10)),
                        CurrencySymbol = reader.GetValue(11).ToString(),
                        CurrencyId = Guid.Parse(reader.GetValue(12).ToString()),
                        price = Convert.ToDecimal(reader.GetValue(17)),
                        isClosed = Convert.ToInt32(reader.GetValue(18)) == 0 ? true : false,
                        BillingInvoicePendingStatusId = Convert.ToInt32(reader.GetValue(20)) > 0 ? 1 : 0,
                        BillingInvoiceErrorStatusId = Convert.ToInt32(reader.GetValue(21)) > 0 ? 1 : 0,
                        BillingInvoiceIssuedStatusId = Convert.ToInt32(reader.GetValue(22)) > 0 ? 1 : 0,
                    };

                    resultDto.GuestReservationItemStatusName = !resultDto.GuestReservationItemStatusId.HasValue ? null : _localizationManager.GetString(AppConsts.LocalizationSourceName, ((ReservationStatus)resultDto.GuestReservationItemStatusId.Value).ToString());
                    resultList.Add(resultDto);
                }
            }
        }

        public decimal CountBalanceGuest(long? reservationItemId)
        {
            if (reservationItemId.HasValue)
            {
                var dbBaseQuery = Context.BillingAccounts

                                    .Where(b => b.ReservationItemId == reservationItemId).Select(x => x.GroupKey);

                decimal sumAllAccounts = 0;
                foreach (var account in dbBaseQuery)
                {
                    sumAllAccounts = sumAllAccounts + SumOfAccount(account);
                }

                return sumAllAccounts;
            }
            return 0;

        }

        public BillingAccountWithAccountsDto GetById(DefaultGuidRequestDto request)
        {
            var result = GetBillingAccountWithAccountsDtoBaseQuery(new List<Guid> { request.Id }).FirstOrDefault();

            if (result != null)
            {
                if (result.AccountTypeId == (int)BillingAccountTypeEnum.GroupAccount)
                    result.HolderName = Context.BillingAccounts.FirstOrDefault(x => x.Id == result.GroupKey).BillingAccountName;

                result.AccountTypeName = _localizationManager.GetString(AppConsts.LocalizationSourceName, ((BillingAccountTypeEnum)result.AccountTypeId).ToString());
                result.StatusName = _localizationManager.GetString(AppConsts.LocalizationSourceName, ((AccountBillingStatusEnum)result.StatusId).ToString());
            }

            return result;
        }

        public List<BillingAccountWithAccountsDto> GetAllByIds(List<Guid> ids)
        {
            var results = GetBillingAccountWithAccountsDtoBaseQuery(ids).ToList();

            foreach (var result in results)
            {
                if (result != null)
                {
                    result.AccountTypeName = _localizationManager.GetString(AppConsts.LocalizationSourceName, ((BillingAccountTypeEnum)result.AccountTypeId).ToString());
                    result.StatusName = _localizationManager.GetString(AppConsts.LocalizationSourceName, ((AccountBillingStatusEnum)result.StatusId).ToString());
                }
            }

            return results;
        }

        private IQueryable<BillingAccountWithAccountsDto> GetBillingAccountWithAccountsDtoBaseQuery(List<Guid> ids)
        {
            return (from p in GetBillingAccountBaseJoinWithReservationConfirmationMap()
                    where
                    ids.Contains(p.BillingAccount.Id)

                    select new BillingAccountWithAccountsDto
                    {
                        Id = p.BillingAccount.Id,
                        MainBillingAccountId = p.BillingAccount.Id,

                        AccountTypeId = p.BillingAccount.BillingAccountTypeId,
                        AccountTypeName = Enum.GetName(typeof(BillingAccountTypeEnum), p.BillingAccount.BillingAccountTypeId),

                        ArrivalDate = p.GuestReservationItem != null ? p.GuestReservationItem.CheckInDate ?? p.GuestReservationItem.EstimatedArrivalDate : (DateTime?)null,
                        DepartureDate = p.GuestReservationItem != null ? p.GuestReservationItem.CheckOutDate ?? p.GuestReservationItem.EstimatedDepartureDate : (DateTime?)null,

                        HolderName = p.CompanyClient != null ? p.CompanyClient.TradeName : (p.GuestReservationItem != null ? p.GuestReservationItem.GuestName : null),
                        IsHolder = p.GuestReservationItem != null ? p.GuestReservationItem.IsMain : false,
                        StatusId = p.BillingAccount.StatusId,
                        GroupKey = p.BillingAccount.GroupKey,

                        ReservationItemCode = p.ReservationItem != null ? p.ReservationItem.ReservationItemCode : null,
                        ReservationId = p.Reservation != null ? p.Reservation.Id : (long?)null,
                        ReservationCode = p.Reservation != null ? p.Reservation.ReservationCode : null,
                        RoomNumber = p.Room != null ? p.Room.RoomNumber : null,
                        RoomTypeName = p.RoomType != null ? p.RoomType.Abbreviation : null,

                        BillingClientId = p.CompanyClientConfirmationReservation != null ? p.CompanyClientConfirmationReservation.Id : (Guid?)null,
                        BillingClientName = p.CompanyClientConfirmationReservation != null ? p.CompanyClientConfirmationReservation.TradeName : null

                    });
        }

        public List<Guid> GetBillingAccountIdsByGroupId(Guid groupId)
        {
            var result = (from billingaccount in Context.BillingAccounts

                          where billingaccount.GroupKey == groupId

                          select new BillingAccount
                          {
                              GroupKey = billingaccount.GroupKey
                          })
                          .ToList();

            return result.Select(e => e.GroupKey).ToList();
        }

        public int CountComments(string externalComments, string internalComments, string partnerComments)
        {
            var commentList = new List<string> { externalComments, internalComments, partnerComments };
            return commentList.Count(c => !string.IsNullOrWhiteSpace(c));
        }

        public List<BillingAccount> GetAllByBillingAccountGroupKey(List<Guid> groupKeys)
        {
            if (groupKeys == null || groupKeys.Count == 0)
                return null;

            return Context
                        .BillingAccounts

                        .Include(e => e.BillingAccountItemList)
                        .ThenInclude(e => e.BillingItem)
                        .ThenInclude(e => e.BillingItemCategory)

                        .Include(e => e.BillingAccountItemList)
                        .ThenInclude(e => e.BillingInvoice)

                        .Include(e => e.BillingAccountItemList)
                        .ThenInclude(e => e.BillingItem)
                        .ThenInclude(e => e.PaymentType)
                        .Where(e => groupKeys.Contains(e.GroupKey))

                        .Include(e => e.BillingAccountItemList)
                        .ThenInclude(e => e.BillingAccountItemReason)
                        .ToList();
        }

        public bool IsMainAccount(Guid id)
        {
            return Context
                    .BillingAccounts

                    .Any(e => e.Id == id && e.IsMainAccount);
        }

        public bool IsClosedOrBlockedAccount(Guid id)
        {
            return Context
                    .BillingAccounts

                    .Any(e => e.Id == id && (e.Blocked || e.StatusId == (int)AccountBillingStatusEnum.Closed));
        }

        public bool IsOpen(Guid id)
        {
            return Context
                    .BillingAccounts

                    .Any(e => e.Id == id && e.StatusId == (int)AccountBillingStatusEnum.Opened);
        }

        public bool AnyClosedOrBlockedAccount(List<Guid> ids)
        {
            return Context
                    .BillingAccounts

                    .Any(e => ids.Contains(e.Id) && (e.Blocked || e.StatusId == (int)AccountBillingStatusEnum.Closed));
        }

        public bool AnyOpenAccountByReservationItemId(long reservationItemId)
        {
            return Context
                    .BillingAccounts
                    .Any(e => e.ReservationItemId == reservationItemId && e.StatusId == (int)AccountBillingStatusEnum.Opened);
        }

        public BillingAccountTotalForClosureDto GetBillingAccountsForClosure(Guid id, bool allSimiliarAccounts)
        {
            List<BillingAccountForClosureDto> resultQuery = null;

            var baseBillingAccount = Context
                                        .BillingAccounts.AsNoTracking()
                                        .FirstOrDefault(e => e.Id == id);

            if (baseBillingAccount == null)
                return null;

            if (allSimiliarAccounts && baseBillingAccount.BillingAccountTypeId != (int)BillingAccountTypeEnum.Sparse)
            {
                resultQuery = (from p in GetBillingAccountBaseJoinMap()
                               where
                               p.BillingAccount.ReservationItemId == baseBillingAccount.ReservationItemId
                               select new BillingAccountForClosureDto
                               {
                                   Id = p.BillingAccount.Id,
                                   IsHolder = p.BillingAccount.IsMainAccount,
                                   BillingAccountTypeId = p.BillingAccount.BillingAccountTypeId,
                                   Total = Context.BillingAccountItems.Where(x => x.BillingAccountId == p.BillingAccount.Id && (x.BillingInvoiceId == null ||
                                                                                                                               x.BillingInvoiceId != Guid.Empty))
                                                                      .Sum(x => x.Amount),
                                   BillingAccountItemName = p.BillingAccount.BillingAccountName,
                                   GuestReservatiomItemId = p.GuestReservationItem != null ? p.GuestReservationItem.Id : (long?)null,
                                   ReservatiomItemId = p.GuestReservationItem != null ? p.GuestReservationItem.ReservationItemId : (long?)null,
                                   ReservationId = p.ReservationItem != null ? p.ReservationItem.ReservationId : (long?)null,
                                   StatusId = p.BillingAccount.StatusId,
                                   RoomId = p.Room != null ? p.Room.Id : (int?)null,
                                   RoomNumber = p.Room != null ? p.Room.RoomNumber : null,
                                   OwnerName = p.CompanyClient != null ? p.CompanyClient.TradeName : (p.GuestReservationItem != null ? p.GuestReservationItem.GuestName : null),
                                   OwnerId = p.CompanyClient != null ? p.CompanyClient.Id : (p.Guest != null ? (p.Guest != null && p.Guest.PersonId.HasValue ? p.Guest.PersonId.Value : Guid.Empty) : Guid.Empty),
                                   IsCompany = p.CompanyClient != null ? true : false
                               }).ToList();
            }
            else
            {
                resultQuery = (from p in GetBillingAccountBaseJoinMap()
                               where
                                   p.BillingAccount.GroupKey == baseBillingAccount.GroupKey
                               select new BillingAccountForClosureDto
                               {
                                   Id = p.BillingAccount.Id,
                                   IsHolder = p.BillingAccount.IsMainAccount,
                                   BillingAccountTypeId = p.BillingAccount.BillingAccountTypeId,
                                   Total = Context.BillingAccountItems.Where(x => x.BillingAccountId == p.BillingAccount.Id && (x.BillingInvoiceId == null ||
                                                                                                                               x.BillingInvoiceId != Guid.Empty))
                                                                      .Sum(x => x.Amount),
                                   BillingAccountItemName = p.BillingAccount.BillingAccountName,
                                   GuestReservatiomItemId = p.GuestReservationItem != null ? p.GuestReservationItem.Id : (long?)null,
                                   ReservatiomItemId = p.GuestReservationItem != null ? p.GuestReservationItem.ReservationItemId : (long?)null,
                                   ReservationId = p.ReservationItem != null ? p.ReservationItem.ReservationId : (long?)null,
                                   StatusId = p.BillingAccount.StatusId,
                                   RoomId = p.Room != null ? p.Room.Id : (int?)null,
                                   RoomNumber = p.Room != null ? p.Room.RoomNumber : null,
                                   OwnerName = p.CompanyClient != null ? p.CompanyClient.TradeName : (p.GuestReservationItem != null ? p.GuestReservationItem.GuestName : null),
                                   OwnerId = p.CompanyClient != null ? p.CompanyClient.Id : (p.Guest != null ? (p.Guest != null && p.Guest.PersonId.HasValue ? p.Guest.PersonId.Value : Guid.Empty) : Guid.Empty),
                                   IsCompany = p.CompanyClient != null ? true : false
                               }).ToList();
            }

            return new BillingAccountTotalForClosureDto
            {
                Id = Guid.NewGuid(),
                Total = resultQuery == null ? 0 : resultQuery.Sum(e => e.Total),
                BillingAccountForClosureList = resultQuery,
                OwnerList = resultQuery == null ? null :
                            resultQuery
                            .Where(e => e.OwnerId != Guid.Empty)
                            .GroupBy(g => new { g.OwnerId, g.IsCompany, g.OwnerName })
                            .Select(e => new OwnerForClosureDto
                            {
                                OwnerId = e.Key.OwnerId,
                                IsCompany = e.Key.IsCompany,
                                OwnerName = e.Key.OwnerName
                            }).ToList()
            };
        }


        public List<BillingAccount> GetAllByBillingAccountByIds(List<Guid> ids)
        {
            if (ids == null || ids.Count == 0)
                return null;

            return Context
                        .BillingAccounts

                        .Include(e => e.BillingAccountItemList)
                        .ThenInclude(e => e.BillingItem)
                        .ThenInclude(e => e.BillingItemCategory)

                        .Include(e => e.BillingAccountItemList)
                        .ThenInclude(e => e.BillingItem)
                        .ThenInclude(e => e.PaymentType)

                        .Include(e => e.BillingAccountItemList)
                        .ThenInclude(e => e.BillingAccountItemReason)
                        .Where(e => ids.Contains(e.Id))
                        .ToList();
        }

        public List<BillingAccountWithAccountsDto> GetBillingAccountsIdsForEntries(Guid id)
        {
            List<BillingAccountWithAccountsDto> resultQuery = null;

            var baseBillingAccount = Context
                                        .BillingAccounts
                                        .FirstOrDefault(e => e.Id == id);

            if (baseBillingAccount == null)
                return null;

            resultQuery = (from p in GetBillingAccountBaseJoinMap()
                           where
                           //conta avulsa e subcontas
                           (baseBillingAccount.BillingAccountTypeId == (int)BillingAccountTypeEnum.Sparse &&
                           p.BillingAccount.GroupKey == baseBillingAccount.GroupKey)
                           ||
                           //contas de hospedes
                           (baseBillingAccount.BillingAccountTypeId == (int)BillingAccountTypeEnum.Guest &&
                               ((baseBillingAccount.BillingAccountTypeId == (int)BillingAccountTypeEnum.Guest &&
                               p.BillingAccount.ReservationItemId == baseBillingAccount.ReservationItemId) ||
                               (p.BillingAccount.BillingAccountTypeId == (int)BillingAccountTypeEnum.Company &&
                               p.BillingAccount.ReservationItemId == baseBillingAccount.ReservationItemId))
                           //n�o deve considerar conta em grupo --03/09/2018
                           //||(p.BillingAccount.BillingAccountTypeId == (int)BillingAccountTypeEnum.GroupAccount &&
                           //p.BillingAccount.ReservationId == baseBillingAccount.ReservationId))
                           )
                           ||
                           //conta empresa
                           (baseBillingAccount.BillingAccountTypeId == (int)BillingAccountTypeEnum.Company &&
                               ((baseBillingAccount.BillingAccountTypeId == (int)BillingAccountTypeEnum.Company &&
                               p.BillingAccount.ReservationItemId == baseBillingAccount.ReservationItemId) ||
                               (p.BillingAccount.BillingAccountTypeId == (int)BillingAccountTypeEnum.Guest &&
                               p.BillingAccount.ReservationItemId == baseBillingAccount.ReservationItemId))
                           //n�o deve considerar conta em grupo --03/09/2018
                           //||(p.BillingAccount.BillingAccountTypeId == (int)BillingAccountTypeEnum.GroupAccount &&
                           //p.BillingAccount.ReservationId == baseBillingAccount.ReservationId))
                           )
                           ||
                           //conta grupo
                            (baseBillingAccount.BillingAccountTypeId == (int)BillingAccountTypeEnum.GroupAccount &&
                           p.BillingAccount.GroupKey == baseBillingAccount.GroupKey)
                           //(baseBillingAccount.BillingAccountTypeId == (int)BillingAccountTypeEnum.GroupAccount &&
                           //    ((baseBillingAccount.BillingAccountTypeId == (int)BillingAccountTypeEnum.GroupAccount &&
                           //    p.BillingAccount.ReservationId == baseBillingAccount.ReservationId))
                           //    //||(p.BillingAccount.BillingAccountTypeId != (int)BillingAccountTypeEnum.GroupAccount &&
                           //    //p.BillingAccount.ReservationId == baseBillingAccount.ReservationId)))

                           select new BillingAccountWithAccountsDto
                           {
                               Id = p.BillingAccount.Id,
                               MainBillingAccountId = p.BillingAccount.Id,

                               AccountTypeId = p.BillingAccount.BillingAccountTypeId,
                               AccountTypeName = Enum.GetName(typeof(BillingAccountTypeEnum), p.BillingAccount.BillingAccountTypeId),

                               ArrivalDate = p.GuestReservationItem != null ? p.GuestReservationItem.CheckInDate ?? p.GuestReservationItem.EstimatedArrivalDate : (DateTime?)null,
                               DepartureDate = p.GuestReservationItem != null ? p.GuestReservationItem.CheckOutDate ?? p.GuestReservationItem.EstimatedDepartureDate : (DateTime?)null,

                               ReservationItemArrivalDate = p.ReservationItem != null ? p.GuestReservationItem.CheckInDate ?? p.GuestReservationItem.EstimatedArrivalDate : (DateTime?)null,
                               ReservationItemDepartureDate = p.ReservationItem != null ? p.GuestReservationItem.CheckOutDate ?? p.GuestReservationItem.EstimatedDepartureDate : (DateTime?)null,

                               HolderName = p.CompanyClient != null ? p.CompanyClient.TradeName : (p.GuestReservationItem != null ? p.GuestReservationItem.GuestName : null),
                               IsHolder = p.GuestReservationItem != null ? p.GuestReservationItem.IsMain : false,
                               StatusId = p.BillingAccount.StatusId,
                               GroupKey = p.BillingAccount.GroupKey,


                               ReservationItemCode = p.ReservationItem != null ? p.ReservationItem.ReservationItemCode : null,
                               ReservationCode = p.Reservation != null ? p.Reservation.ReservationCode : null,
                               RoomNumber = p.Room != null ? p.Room.RoomNumber : null,
                               RoomTypeName = p.RoomType != null ? p.RoomType.Abbreviation : null,

                               BillingClientId = p.CompanyClientConfirmationReservation != null ? p.CompanyClientConfirmationReservation.Id : (Guid?)null,
                               BillingClientName = p.CompanyClientConfirmationReservation != null ? p.CompanyClientConfirmationReservation.TradeName : null


                           }).ToList();

            return resultQuery;
        }


        public BillingAccount GetBillingAccountByOwnerIdAndReservationId(Guid ownerDestination, bool isCompany, long reservationId, long? reservationItemId)
        {
            if (isCompany)
                return (from billingaccount in Context.BillingAccounts.AsNoTracking()
                        where billingaccount.CompanyClientId == ownerDestination &&
                        billingaccount.BillingAccountTypeId != (int)BillingAccountTypeEnum.GroupAccount &&
                        billingaccount.ReservationId.HasValue && billingaccount.ReservationId == reservationId
                        select billingaccount)

                        .FirstOrDefault();
            else
                return (from billingaccount in Context.BillingAccounts.AsNoTracking()
                        join guestReserItem in Context.GuestReservationItems.AsNoTracking() on billingaccount.GuestReservationItemId equals guestReserItem.Id into f
                        from guestReserItem in f.DefaultIfEmpty()
                        join guest in Context.Guests.AsNoTracking().Where(exp => exp.PersonId == ownerDestination) on guestReserItem.GuestId equals guest.Id into g
                        from guest in g.DefaultIfEmpty()
                        where
                        (billingaccount.ReservationId.HasValue && billingaccount.ReservationId == reservationId) &&
                        (billingaccount.ReservationItemId.HasValue && billingaccount.ReservationItemId == reservationItemId.Value) &&
                        guest.PersonId == ownerDestination
                        select billingaccount)

                    .FirstOrDefault();
        }

        public ListDto<SearchBillingAccountResultDto> GetAllBillingAccountsByUhOrSparseAccount(Guid id, int propertyId)
        {
            List<SearchBillingAccountResultDto> resultQuery = null;

            var baseBillingAccount = Context
                                        .BillingAccounts.AsNoTracking()
                                        .FirstOrDefault(e => e.Id == id);

            if (baseBillingAccount == null)
                return null;

            if (baseBillingAccount.BillingAccountTypeId != (int)BillingAccountTypeEnum.Sparse)
            {
                resultQuery = (from p in GetBillingAccountWithItemsJoinMap()
                               where
                               !p.BillingAccount.Blocked && p.BillingAccount.StatusId != (int)AccountBillingStatusEnum.Closed &&
                                p.BillingAccount.ReservationItemId == baseBillingAccount.ReservationItemId ||
                               (p.BillingAccount.ReservationId == baseBillingAccount.ReservationId &&
                               p.BillingAccount.BillingAccountTypeId == (int)BillingAccountTypeEnum.Company) &&
                               !p.BillingAccount.EndDate.HasValue
                               group p by new
                               {
                                   p.BillingAccount.GroupKey,
                                   p.BillingAccount.Id,
                                   AccountTypeId = p.BillingAccount.BillingAccountTypeId,
                                   HolderName = p.CompanyClient != null ? p.CompanyClient.TradeName : p.GuestReservationItem.GuestName,
                                   RoomTypeName = p.RoomType != null ? p.RoomType.Abbreviation : null,
                                   RoomNumber = p.Room != null ? p.Room.RoomNumber : null,
                                   ReservationCode = p.Reservation != null ? p.Reservation.ReservationCode : null,
                                   p.BillingAccount.StatusId,
                                   p.BillingAccount.BillingAccountName,
                                   CurrencyId = p.Currency == null ? Guid.Empty : p.Currency.Id,
                                   CurrencySymbol = p.Currency == null ? "" : p.Currency.Symbol
                               } into e
                               select new SearchBillingAccountResultDto
                               {
                                   BillingAccountId = e.Key.Id.ToString(),
                                   Id = e.Key.Id,
                                   AccountTypeId = e.Key.AccountTypeId,
                                   HolderName = e.Key.HolderName,
                                   RoomTypeName = e.Key.RoomTypeName,
                                   RoomNumber = e.Key.RoomNumber,
                                   ReservationCode = e.Key.ReservationCode,
                                   StatusId = e.Key.StatusId,
                                   BillingAccountName = e.Key.BillingAccountName,
                                   BillingAccountDefaultName = _billingAccountDefaultName,
                                   GroupKey = e.Key.GroupKey,
                                   Balance = e.Where(x => x.BillingAccountItem != null).Sum(p => p.BillingAccountItem.Amount),
                                   CurrencySymbol = e.Key.CurrencySymbol,
                                   CurrencyId = e.Key.CurrencyId

                               }).ToList();
            }
            else
            {
                resultQuery = (from p in GetBillingAccountWithItemsJoinMap()
                               where
                               !p.BillingAccount.Blocked && p.BillingAccount.StatusId != (int)AccountBillingStatusEnum.Closed &&
                               p.BillingAccount.GroupKey == baseBillingAccount.GroupKey &&
                               !p.BillingAccount.EndDate.HasValue && !p.BillingAccount.Blocked                                
                               group p by new
                               {
                                   GroupKey = p.BillingAccount.GroupKey,
                                   Id = p.BillingAccount.Id,
                                   AccountTypeId = p.BillingAccount.BillingAccountTypeId,
                                   HolderName = p.CompanyClient != null ? p.CompanyClient.TradeName : p.GuestReservationItem.GuestName,
                                   RoomTypeName = p.RoomType != null ? p.RoomType.Abbreviation : null,
                                   RoomNumber = p.Room != null ? p.Room.RoomNumber : null,
                                   ReservationCode = p.Reservation != null ? p.Reservation.ReservationCode : null,
                                   StatusId = p.BillingAccount.StatusId,
                                   BillingAccountName = p.BillingAccount.BillingAccountName,
                                   CurrencyId = p.Currency == null ? Guid.Empty : p.Currency.Id,
                                   CurrencySymbol = p.Currency == null ? "" : p.Currency.Symbol
                               } into e
                               select new SearchBillingAccountResultDto
                               {
                                   BillingAccountId = e.Key.Id.ToString(),
                                   Id = e.Key.Id,
                                   AccountTypeId = e.Key.AccountTypeId,
                                   HolderName = e.Key.HolderName,
                                   RoomTypeName = e.Key.RoomTypeName,
                                   RoomNumber = e.Key.RoomNumber,
                                   ReservationCode = e.Key.ReservationCode,
                                   StatusId = e.Key.StatusId,
                                   BillingAccountName = e.Key.BillingAccountName,
                                   BillingAccountDefaultName = _billingAccountDefaultName,
                                   GroupKey = e.Key.GroupKey,
                                   Balance = e.Where(x => x.BillingAccountItem != null).Sum(p => p.BillingAccountItem.Amount),
                                   CurrencySymbol = e.Key.CurrencySymbol,
                                   CurrencyId = e.Key.CurrencyId

                               }).ToList();
            }

            return new ListDto<SearchBillingAccountResultDto>
            {
                HasNext = false,
                Items = resultQuery,
                //Total = resultQuery.Count()
            };
        }

        public bool AnyGroupAccount(long reservationId)
        {
            return Context
                    .BillingAccounts
                    .AsNoTracking()

                    .Any(e => e.ReservationId == reservationId &&
                            e.BillingAccountTypeId == (int)BillingAccountTypeEnum.GroupAccount);
        }

        public bool AnyCompanyAccountAccomodation(long reservationItemId)
        {
            return Context
                    .BillingAccounts

                    .AsNoTracking()
                    .Any(e => e.ReservationItemId == reservationItemId &&
                            e.BillingAccountTypeId == (int)BillingAccountTypeEnum.Company);
        }
        public decimal SumOfAccount(Guid billingAccountGroupKey)
        {
            var result = (from billingaccount in Context.BillingAccounts
                          join billingaccountItem in Context.BillingAccountItems on billingaccount.Id equals billingaccountItem.BillingAccountId
                          where billingaccount.GroupKey == billingAccountGroupKey
                          select new
                          {
                              Price = billingaccountItem.Amount
                          })

                          .ToList();

            return result.Sum(x => x.Price);

        }

        public List<Guid> GetAllBillingAccountsByUhOrSparseAccountIds(Guid id, int propertyId)
        {
            List<Guid> resultQuery = null;

            var baseBillingAccount = Context
                                        .BillingAccounts.AsNoTracking()

                                        .FirstOrDefault(e => e.Id == id);

            if (baseBillingAccount == null)
                return null;

            if (baseBillingAccount.BillingAccountTypeId != (int)BillingAccountTypeEnum.Sparse)
            {
                resultQuery = (from billingaccount in Context
                                                        .BillingAccounts

                               where
                               billingaccount.ReservationItemId == baseBillingAccount.ReservationItemId ||
                               (billingaccount.ReservationId == baseBillingAccount.ReservationId &&
                               billingaccount.BillingAccountTypeId == (int)BillingAccountTypeEnum.Company)
                               select billingaccount.Id).ToList();
            }
            else
            {
                resultQuery = (from billingaccount in Context
                                                        .BillingAccounts

                               where
                               billingaccount.GroupKey == baseBillingAccount.GroupKey
                               select billingaccount.Id).ToList();
            }

            return resultQuery;
        }

        public IQueryable<BillingAccountBaseJoinMap> GetBillingAccountBaseJoinWithReservationConfirmationMap()
        {
            return
                 (from billingaccount in Context.BillingAccounts.AsNoTracking()
                  join reserv in Context.Reservations on billingaccount.ReservationId equals reserv.Id into a
                  from reserv in a.DefaultIfEmpty()
                  join reservItem in Context.ReservationItems.AsNoTracking() on billingaccount.ReservationItemId equals reservItem.Id into b
                  from reservItem in b.DefaultIfEmpty()
                  join companyClient in Context.CompanyClients.AsNoTracking() on billingaccount.CompanyClientId equals companyClient.Id into c
                  from companyClient in c.DefaultIfEmpty()
                  join guestReserItem in Context.GuestReservationItems.AsNoTracking() on billingaccount.GuestReservationItemId equals guestReserItem.Id into f
                  from guestReserItem in f.DefaultIfEmpty()
                  join room in Context.Rooms.AsNoTracking() on reservItem.RoomId equals room.Id into r
                  from room in r.DefaultIfEmpty()
                  join roomType in Context.RoomTypes.AsNoTracking() on reservItem.ReceivedRoomTypeId equals roomType.Id into e
                  from roomType in e.DefaultIfEmpty()
                  join guest in Context.Guests.AsNoTracking() on guestReserItem.GuestId equals guest.Id into g
                  from guest in g.DefaultIfEmpty()
                  join reservbudget in Context.ReservationBudgets.AsNoTracking() on reservItem.Id equals reservbudget.ReservationItemId into h
                  from reservbudget in h.DefaultIfEmpty()
                  join reservConfirm in Context.ReservationConfirmations.AsNoTracking() on reserv.Id equals reservConfirm.ReservationId into rc
                  from reservConfirm in rc.DefaultIfEmpty()
                  join companyClientRc in Context.CompanyClients.AsNoTracking() on
                  new { Key = reservConfirm.BillingClientId } equals new { Key = (Guid?)companyClientRc.Id } into cRc
                  from companyClientRc in cRc.DefaultIfEmpty()
                  select new BillingAccountBaseJoinMap
                  {
                      BillingAccount = billingaccount,
                      Reservation = reserv,
                      ReservationItem = reservItem,
                      CompanyClient = companyClient,
                      GuestReservationItem = guestReserItem,
                      Room = room,
                      RoomType = roomType,
                      Guest = guest,
                      ReservationConfirmation = reservConfirm,
                      CompanyClientConfirmationReservation = companyClientRc,
                      TenantId = billingaccount.TenantId
                  })
                ;
        }


        public IQueryable<BillingAccountBaseJoinMap> GetBillingAccountBaseJoinMap()
        {
            return
                 (from billingaccount in Context.BillingAccounts.AsNoTracking()
                  join reserv in Context.Reservations.AsNoTracking() on billingaccount.ReservationId equals reserv.Id into a
                  from reserv in a.DefaultIfEmpty()
                  join reservItem in Context.ReservationItems.AsNoTracking() on billingaccount.ReservationItemId equals reservItem.Id into b
                  from reservItem in b.DefaultIfEmpty()
                  join companyClient in Context.CompanyClients.AsNoTracking() on billingaccount.CompanyClientId equals companyClient.Id into c
                  from companyClient in c.DefaultIfEmpty()
                  join guestReserItem in Context.GuestReservationItems.AsNoTracking() on billingaccount.GuestReservationItemId equals guestReserItem.Id into f
                  from guestReserItem in f.DefaultIfEmpty()
                  join room in Context.Rooms.AsNoTracking() on reservItem.RoomId equals room.Id into r
                  from room in r.DefaultIfEmpty()
                  join roomType in Context.RoomTypes.AsNoTracking() on reservItem.ReceivedRoomTypeId equals roomType.Id into e
                  from roomType in e.DefaultIfEmpty()
                  join guest in Context.Guests.AsNoTracking() on guestReserItem.GuestId equals guest.Id into g
                  from guest in g.DefaultIfEmpty()
                  select new BillingAccountBaseJoinMap
                  {
                      BillingAccount = billingaccount,
                      Reservation = reserv,
                      ReservationItem = reservItem,
                      CompanyClient = companyClient,
                      GuestReservationItem = guestReserItem,
                      Room = room,
                      RoomType = roomType,
                      Guest = guest,
                      TenantId = billingaccount.TenantId
                  })
                ;
        }

        public IQueryable<BillingAccountWithItemsMap> GetBillingAccountWithItemsJoinMap()
        {
            return
                 (from billingaccount in Context.BillingAccounts.AsNoTracking()
                  join billingAccountItem in Context.BillingAccountItems.AsNoTracking()
                    on billingaccount.Id equals billingAccountItem.BillingAccountId into ba
                  from leftBillingAccountItem in ba.DefaultIfEmpty()


                  join propertyParameterCurrency in Context.PropertyParameters.AsNoTracking()
                  on new { pId = billingaccount.PropertyId, appId = (int)ApplicationParameterEnum.ParameterDefaultCurrency }
                  equals new { pId = propertyParameterCurrency.PropertyId, appId = propertyParameterCurrency.ApplicationParameterId } into propParameter
                  from leftPropertyParameterCurrency in propParameter.DefaultIfEmpty()

                  join currency in Context.Currencies.AsNoTracking()
                    on Guid.Parse(leftPropertyParameterCurrency.PropertyParameterValue) equals currency.Id into cur
                  from currency in cur.DefaultIfEmpty()

                  join reserv in Context.Reservations.AsNoTracking() on billingaccount.ReservationId equals reserv.Id into a
                  from reserv in a.DefaultIfEmpty()
                  join reservItem in Context.ReservationItems.AsNoTracking() on billingaccount.ReservationItemId equals reservItem.Id into b
                  from reservItem in b.DefaultIfEmpty()
                  join companyClient in Context.CompanyClients.AsNoTracking() on billingaccount.CompanyClientId equals companyClient.Id into c
                  from companyClient in c.DefaultIfEmpty()
                  join guestReserItem in Context.GuestReservationItems.AsNoTracking() on billingaccount.GuestReservationItemId equals guestReserItem.Id into f
                  from guestReserItem in f.DefaultIfEmpty()
                  join room in Context.Rooms.AsNoTracking() on reservItem.RoomId equals room.Id into r
                  from room in r.DefaultIfEmpty()
                  join roomType in Context.RoomTypes.AsNoTracking() on reservItem.ReceivedRoomTypeId equals roomType.Id into e
                  from roomType in e.DefaultIfEmpty()
                  join guest in Context.Guests.AsNoTracking() on guestReserItem.GuestId equals guest.Id into g
                  from guest in g.DefaultIfEmpty()

                  join reservConfirm in Context.ReservationConfirmations.AsNoTracking() on reserv.Id equals reservConfirm.ReservationId into rc
                  from reservConfirm in rc.DefaultIfEmpty()
                  select new BillingAccountWithItemsMap
                  {
                      BillingAccount = billingaccount,
                      BillingAccountItem = leftBillingAccountItem,
                      Reservation = reserv,
                      ReservationItem = reservItem,
                      CompanyClient = companyClient,
                      GuestReservationItem = guestReserItem,
                      Room = room,
                      RoomType = roomType,
                      Guest = guest,
                      ReservationConfirmation = reservConfirm,
                      TenantId = billingaccount.TenantId,
                      Currency = currency
                  })
                ;
        }

        public BillingAccount GetBillingAccountById(Guid billingAccountId)
        {
            return Context
                .BillingAccounts
                .AsNoTracking()
                .FirstOrDefault(e => e.Id == billingAccountId);
        }

        public bool CheckAllBillingAccountsAreClosedByReservationItemId(Guid billingAccountId)
        {
            var reservationItemId = Context.BillingAccounts.Where(x => x.Id == billingAccountId).Select(x => x.ReservationItemId).FirstOrDefault();
            return Context.BillingAccounts.AsNoTracking()
               .Where(e => e.ReservationItemId.HasValue && e.ReservationItemId == reservationItemId).All(e => e.StatusId == (int)AccountBillingStatusEnum.Closed);
        }

        public bool CheckIfExistIsMainBillingAccount(long reservationId, long reservationItemId, long? guestReservationItem, Guid? companyClient, int billingAccountTypeId)
        {
            if (companyClient.HasValue)
            {
                return Context
                    .BillingAccounts

                    .Any(x => x.ReservationId == reservationId && x.CompanyClientId == companyClient.Value && x.BillingAccountTypeId == billingAccountTypeId && x.IsMainAccount);

            }
            else
            {
                return Context
                    .BillingAccounts

                    .Any(x => x.ReservationId == reservationId && x.ReservationItemId == reservationItemId && x.GuestReservationItemId == guestReservationItem && x.BillingAccountTypeId == billingAccountTypeId && x.IsMainAccount);

            }
        }

        public bool AnyGroupAccount(List<Guid> ids)
        {
            return Context
                    .BillingAccounts

                    .Any(e => ids.Contains(e.Id) && e.BillingAccountTypeId == (int)BillingAccountTypeEnum.GroupAccount);
        }


        public bool AllSimiliarBillingAccountsAreClosed(Guid billingAccountId)
        {
            var billingAccount = (from ba in Context.BillingAccounts.AsNoTracking()
                                  where ba.Id == billingAccountId
                                  select ba)
                                    .FirstOrDefault();

            if (billingAccount == null)
                return true;

            var billingAccountStatusIdList = (from ba in Context.BillingAccounts.AsNoTracking()
                                              where (billingAccount.ReservationItem != null && ba.ReservationItemId == billingAccount.ReservationItemId)
                                              || ba.Id == billingAccount.Id
                                              || ba.GroupKey == billingAccount.GroupKey /*&& billingAccount.BillingAccount.BillingAccountTypeId == (int)BillingAccountTypeEnum.Sparse*/
                                              select new
                                              {
                                                  BillingAccountStatusId = billingAccount.StatusId,
                                              }).ToList();


            return billingAccountStatusIdList.Count(e => e.BillingAccountStatusId == (int)AccountBillingStatusEnum.Closed) == billingAccountStatusIdList.Count();
        }

        public BillingAccountDto GetByInvoiceId(Guid invoiceId)
        {
            var billingAccount = (from ba in Context.BillingAccounts
                                  join bai in Context.BillingAccountItems on ba.Id equals bai.BillingAccountId
                                  join bi in Context.BillingInvoices on bai.BillingInvoiceId equals bi.Id
                                  where bi.Id == invoiceId
                                  select ba).FirstOrDefault();

            return billingAccount.MapTo<BillingAccountDto>();
        }

        public List<BillingAccount> GetAllByReservationItemId(long id)
        {
            var billingAccounts = Context.BillingAccounts.Where(x => x.ReservationItemId == id).ToList();
            return billingAccounts;
        }

        public Guid GetIdByPersonHeaderId(Guid personId)
        {
            return (from billingAccount in Context.BillingAccounts
                    where billingAccount.PersonHeaderId == personId
                    select billingAccount.Id).FirstOrDefault();
        }

        public (bool, bool) AnyOpenAccountAndHaveBillingAccountItemByReservationItemId(long reservationItemId)
        {
            return (Context
                    .BillingAccounts
                    .Any(e => e.ReservationItemId == reservationItemId && e.StatusId == (int)AccountBillingStatusEnum.Opened),
                Context
                    .BillingAccounts
                    .Include(e => e.BillingAccountItemList)
                    .Any(e => e.ReservationItemId == reservationItemId && e.StatusId == (int)AccountBillingStatusEnum.Opened && e.BillingAccountItemList.Any()));
        }

        public List<BillingAccount> GetAllOpenedByReservationItemId(long reservationItemId)
        {
            return Context.BillingAccounts.Where(x => x.ReservationItemId == reservationItemId && x.StatusId == (int)AccountBillingStatusEnum.Opened).ToList();
        }

        public bool AnyBillingAccountItemByPropertyId(int propertyId)
        {
            return Context.BillingAccounts.Any(b => b.PropertyId == propertyId && b.BillingAccountItemList.Any());
        }

        public List<BillingAccountDto> GetByIds(List<Guid> ids)
        {
            return Context.BillingAccounts.Where(b => ids.Contains(b.Id)).Select(b => new BillingAccountDto
            {
                Id = b.Id,
                BillingAccountTypeId = b.BillingAccountTypeId,
                ReservationItemId = b.ReservationItemId
            }).ToList();
        }

        public List<BillingAccountGuestReservationItemDto> GetAllAccountGuestReservationItemDtoByFilters(TourismTaxLaunchSearchDto dto)
        {
            return (from ba in Context.BillingAccounts.AsNoTracking()
                    join ri in Context.ReservationItems.AsNoTracking() on ba.ReservationItemId equals ri.Id
                    join r in Context.Reservations.AsNoTracking() on ri.ReservationId equals r.Id
                    join gri in Context.GuestReservationItems.AsNoTracking() on ba.GuestReservationItemId equals gri.Id
                    join gr in Context.GuestRegistrations.AsNoTracking() on gri.GuestId equals gr.GuestId
                    join ril in Context.ReservationItemLaunchers.AsNoTracking() on gri.Id equals ril.GuestReservationItemId into lri
                    from leftRi in lri.DefaultIfEmpty()
                    where dto.GuestReservationItemIdList.Contains(ba.GuestReservationItemId)
                        && gri.IsChild == dto.IsChild
                        && ba.IsMainAccount == dto.IsMainAccount
                        && ba.StatusId == dto.StatusAccount
                        && gri.GuestStatusId == dto.ReservationItemStatus
                        && dto.PropertyGuestTypeIdList.Contains(gr.GuestTypeId)
                    select new BillingAccountGuestReservationItemDto
                    {
                        BillingAccountId = ba.Id,
                        CheckinDate = gri.CheckInDate.Value,
                        EstimatedDepartureDate = gri.EstimatedDepartureDate,
                        ReservationItemId = ri.Id,
                        ReservationId = r.Id,
                        GuestReservationItemId = gri.Id,
                        IsMainAccount = ba.IsMainAccount,
                        QuantityOfTourismTaxLaunched = leftRi.QuantityOfTourismTaxLaunched,
                        ReservationUnifyAccounts = r.UnifyAccounts
                    }).DistinctBy(g => g.GuestReservationItemId).ToList();
        }

        public BillingAccount GetById(Guid id)
        {
            return Context.BillingAccounts.Find(id);
        }

        public List<BillingAccount> GetAllExistingBillingAccountsByGuestReservationItemIds(List<long?> guestReservationItemId)
        {
            return Context
                   .BillingAccounts
                   .Where(x => guestReservationItemId.Contains(x.GuestReservationItemId) && 
                          x.BillingAccountTypeId == (int)BillingAccountTypeEnum.Guest)
                   .ToList();
        }

        public bool CheckIfExistIsMainBillingAccountByGuestReservationItemIds(List<long?> guestReservationItemIds)
        {
            return Context
                  .BillingAccounts
                  .Any(x => guestReservationItemIds.Contains(x.GuestReservationItemId) &&
                       x.BillingAccountTypeId == (int)BillingAccountTypeEnum.Guest &&
                       x.IsMainAccount);
        }

        public BillingAccount GetByFilters(long reservationId, BillingAccountTypeEnum billingAccountTypeId)
            => Context.BillingAccounts.FirstOrDefault(b => b.ReservationId == reservationId && b.BillingAccountTypeId == (int)billingAccountTypeId);

        public List<Guid> GetAllIdByFilters(List<long> guestReservationItemId, BillingAccountTypeEnum billingAccountTypeId)
            => (from ba in Context.BillingAccounts
                where guestReservationItemId.Contains(ba.GuestReservationItemId.Value) && ba.BillingAccountTypeId == (int)billingAccountTypeId
                select ba.Id).ToList();

        public Guid GetIdByFilters(long reservationItemId, BillingAccountTypeEnum billingAccountTypeId)
            => (from ba in Context.BillingAccounts
                where ba.ReservationItemId == reservationItemId && ba.BillingAccountTypeId == (int)billingAccountTypeId
                select ba.Id).FirstOrDefault();

        public List<BillingAccount> GetAllByFilters(List<long> reservationIdList, BillingAccountTypeEnum billingAccountTypeId, AccountBillingStatusEnum statusId)
            => (from ba in Context.BillingAccounts
                where reservationIdList.Contains(ba.ReservationId.Value) 
                    && ba.BillingAccountTypeId == (int)billingAccountTypeId
                    && ba.StatusId == (int)statusId
                select ba).ToList();
    }
}
