﻿using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Thex.Common;
using Thex.Kernel;
using Thex.Dto;
using Thex.Infra.ReadInterfaces;

namespace Thex.Infra.Repositories.Read
{
    public class CustomerStationReadRepository : ICustomerStationReadRepository
    {
        public const string GetAllByCompanyClientUrl = "api/customerstation/getallbycompanyclient";
        private readonly ServicesConfiguration _serviceConfiguration;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public CustomerStationReadRepository(ServicesConfiguration serviceConfiguration,
                                             IHttpContextAccessor httpContextAccessor)
        {
            _serviceConfiguration = serviceConfiguration;
            _httpContextAccessor = httpContextAccessor;
        }

        public async Task<ICollection<CustomerStationDto>> GetAllByCompanyClient(Guid companyClientId)
        {
            var url = $"{_serviceConfiguration.PreferenceAPI}/{GetAllByCompanyClientUrl}/{companyClientId}";
            var customerStationList = await GetCustomerStationByHttp(url);

            return customerStationList;
        }

        private async Task<ICollection<CustomerStationDto>> GetCustomerStationByHttp(string url)
        {
            using (var httpClient = new HttpClient())
            {
                var token = _httpContextAccessor.HttpContext.Request.Headers.FirstOrDefault(x => x.Key.Equals("Authorization")).Value.ToString();

                httpClient.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", token);
                try
                {

                    var response = await httpClient.GetAsync(url);

                    if (response.IsSuccessStatusCode)
                    {

                        var responseString = response.Content.ReadAsStringAsync().Result;
                        var customerStationDtoList = JsonConvert.DeserializeObject<ICollection<CustomerStationDto>>(responseString);
                        return customerStationDtoList;
                    }
                    return null;
                }
                catch (Exception ex)
                {
                    return null;
                }
            }
        }
    }
}
