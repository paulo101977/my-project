﻿
using System.Linq;
using Thex.Domain.Entities;
using Thex.Infra.Context;
using Thex.Infra.ReadInterfaces;
using Tnf.EntityFrameworkCore;
using Tnf.EntityFrameworkCore.Repositories;

namespace Thex.Infra.Repositories.Read
{
    public class BrandReadRepository : EfCoreRepositoryBase<ThexContext, Brand>, IBrandReadRepository
    {
        public BrandReadRepository(IDbContextProvider<ThexContext> dbContextProvider) : base(dbContextProvider)
        {

        }

        public Brand GetById(int id)
        {
            var brand = Context.Brands.FirstOrDefault(x => x.Id == id);
            return brand;
        }
    }
}
