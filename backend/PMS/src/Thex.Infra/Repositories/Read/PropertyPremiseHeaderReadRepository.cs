﻿//  <copyright file="PropertyPremiseHeaderReadRepository.cs" company="TOTVS">
//  Copyright (c) TOTVS. All rights reserved.
//  </copyright>
namespace Thex.EntityFrameworkCore.Repositories
{
    using System;
    using System.Linq;

    using Thex.Domain.Entities;
    using Thex.Infra.ReadInterfaces;
    using Tnf.Dto;
    using Tnf.EntityFrameworkCore.Repositories;
    using Tnf.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore;
    using Thex.Dto;
    using Thex.Infra.Context;
    using Thex.Kernel;

    public class PropertyPremiseHeaderReadRepository : EfCoreRepositoryBase<ThexContext, PropertyPremiseHeader>, IPropertyPremiseHeaderReadRepository
    {
        private readonly IApplicationUser _applicationUser;

        public PropertyPremiseHeaderReadRepository(
            IDbContextProvider<ThexContext> dbContextProvider,
            IApplicationUser applicationUser
         ) : base(dbContextProvider)
        {
            _applicationUser = applicationUser;
        }

        public IListDto<PropertyPremiseHeaderListDto> GetAll(GetAllPropertyPremiseHeaderDto request)
        {
            var _propertyId = int.Parse(_applicationUser.PropertyId);

            var baseQuery = Context.PropertyPremiseHeaders
                .Include(x => x.Currency)
                .Include(x => x.RoomTypeReference)
                .Include(x => x.PropertyPremiseList)
                .ThenInclude(k => k.RoomType)
                .Where(x => x.PropertyId == _propertyId);

            if (request.StartDate.HasValue)
                baseQuery = baseQuery.Where(x => x.InitialDate.Date <= request.StartDate.Value.Date);

            if (request.FinalDate.HasValue)
                baseQuery = baseQuery.Where(x => x.FinalDate.Date >= request.FinalDate.Value.Date);

            if(request.CurrencyId.HasValue)
                baseQuery = baseQuery.Where(x => x.CurrencyId == request.CurrencyId.Value);

            return baseQuery.ToListDto<PropertyPremiseHeader, PropertyPremiseHeaderListDto>(request);
        }

        public PropertyPremiseHeaderListDto GetDtoById(Guid propertyPremiseHeaderId)
        {
            var baseQuery = Context.PropertyPremiseHeaders
                .Include(x => x.Currency)
                .Include(x => x.RoomTypeReference)
                .Include(x => x.PropertyPremiseList)
                .ThenInclude(k => k.RoomType);

            return baseQuery.FirstOrDefault(x => x.Id == propertyPremiseHeaderId).MapTo<PropertyPremiseHeaderListDto>();
        }

        public PropertyPremiseHeader GetByIdWithPremiseList(Guid id)
            => Context.PropertyPremiseHeaders.Include(pph => pph.PropertyPremiseList).FirstOrDefault(pph => pph.Id == id);
        
        public bool CheckPremiseHeaderConstraints(PropertyPremiseHeaderDto dto)
        {
            return Context.PropertyPremiseHeaders
                    .Any(x => x.IsActive
                        && x.Name.Trim() == dto.Name.Trim()
                        && x.PropertyId == dto.PropertyId);
        }
    }
}