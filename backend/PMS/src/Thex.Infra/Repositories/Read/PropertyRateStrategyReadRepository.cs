﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Thex.Common.Enumerations;
using Thex.Domain.Entities;
using Thex.Dto;
using Thex.Infra.Context;
using Thex.Infra.ReadInterfaces;
using Tnf.Dto;
using Tnf.EntityFrameworkCore;
using Tnf.EntityFrameworkCore.Repositories;


namespace Thex.Infra.Repositories.Read
{
    public class PropertyRateStrategyReadRepository : EfCoreRepositoryBase<ThexContext, PropertyRateStrategy>, IPropertyRateStrategyReadRepository
    {
        public PropertyRateStrategyReadRepository(IDbContextProvider<ThexContext> dbContextProvider)
            : base(dbContextProvider)
        {
        }

        public IListDto<GetAllPropertyRateStrategyDto> GetAllByPropertyId(int propertyId)
        {
            var dtoList = new ListDto<GetAllPropertyRateStrategyDto>
            {
                Items = new List<GetAllPropertyRateStrategyDto>()
            };

            var propertyRateStrategyList = from strategy in Context.PropertyRateStrategies
                                           .Include(s => s.PropertyRateStrategyRoomTypeList)
                                           .ThenInclude(r => r.RoomType)
                                           .Where(p => p.PropertyId == propertyId)
                                           select strategy;

            foreach (var propertyRateStrategy in propertyRateStrategyList)
            {
                var dto = new GetAllPropertyRateStrategyDto
                {
                    Id = propertyRateStrategy.Id,
                    IsActive = propertyRateStrategy.IsActive,
                    Name = propertyRateStrategy.Name,
                    StartDate = propertyRateStrategy.StartDate,
                    EndDate = propertyRateStrategy.EndDate
                };

                foreach (var propertyRateStrategyRoomType in propertyRateStrategy.PropertyRateStrategyRoomTypeList)
                {
                    dto.RoomTypeAbbreviationList.Add(propertyRateStrategyRoomType.RoomType.Abbreviation);
                }

                dtoList.Items.Add(dto);
            }

            return dtoList;
        }

        public async Task<IList<GetAllPropertyRateStrategyWithRatePlanDto>> GetAllWithRatePlanByPropertyIdAsync(int propertyId)
        {
            var dtoList = new List<GetAllPropertyRateStrategyWithRatePlanDto>();

            var propertyRateStrategyList = await Context.PropertyRateStrategies
                                           .Include(s => s.PropertyRateStrategyRoomTypeList)
                                           .ThenInclude(r => r.RoomType)                                         
                                           .Where(x => x.IsActive && x.PropertyId == propertyId)
                                           .ToListAsync();

            foreach (var propertyRateStrategy in propertyRateStrategyList)
            {

                var dto = new GetAllPropertyRateStrategyWithRatePlanDto
                {
                    Id = propertyRateStrategy.Id,
                    IsActive = propertyRateStrategy.IsActive,
                    Name = propertyRateStrategy.Name,
                    StartDate = propertyRateStrategy.StartDate,
                    EndDate = propertyRateStrategy.EndDate
                };

                foreach (var propertyRateStrategyRoomType in propertyRateStrategy.PropertyRateStrategyRoomTypeList)
                {
                    var roomTypeDto = new RoomTypeDto
                    {
                        PropertyId = propertyRateStrategyRoomType.RoomType.PropertyId,
                        Order = propertyRateStrategyRoomType.RoomType.Order,
                        Name = propertyRateStrategyRoomType.RoomType.Name,
                        Abbreviation = propertyRateStrategyRoomType.RoomType.Abbreviation,
                        AdultCapacity = propertyRateStrategyRoomType.RoomType.AdultCapacity,
                        ChildCapacity = propertyRateStrategyRoomType.RoomType.ChildCapacity,
                        FreeChildQuantity1 = propertyRateStrategyRoomType.RoomType.FreeChildQuantity1,
                        FreeChildQuantity2 = propertyRateStrategyRoomType.RoomType.FreeChildQuantity2,
                        FreeChildQuantity3 = propertyRateStrategyRoomType.RoomType.FreeChildQuantity3,
                        IsActive = propertyRateStrategyRoomType.RoomType.IsActive,
                        MaximumRate = propertyRateStrategyRoomType.RoomType.MaximumRate,
                        MinimumRate = propertyRateStrategyRoomType.RoomType.MinimumRate,
                        DistributionCode = propertyRateStrategyRoomType.RoomType.DistributionCode
                    };

                    var propertyRateStrategyRoomTypeDto = new PropertyRateStrategyRoomTypeDto
                    {
                        Id = propertyRateStrategyRoomType.Id,
                        PropertyRateStrategyId = propertyRateStrategyRoomType.PropertyRateStrategyId,
                        RoomTypeId = propertyRateStrategyRoomType.RoomTypeId,
                        MinOccupationPercentual = propertyRateStrategyRoomType.MinOccupationPercentual,
                        MaxOccupationPercentual = propertyRateStrategyRoomType.MaxOccupationPercentual,
                        IsDecreased = propertyRateStrategyRoomType.IsDecreased,
                        PercentualAmount = propertyRateStrategyRoomType.PercentualAmount,
                        IncrementAmount = propertyRateStrategyRoomType.IncrementAmount
                    };

                    dto.RoomTypeList.Add(roomTypeDto);
                    dto.PropertyRateStrategyRoomTypeList.Add(propertyRateStrategyRoomTypeDto);
                }              

                dtoList.Add(dto);
            }

            return dtoList;
        }

        public async Task<PropertyRateStrategyDto> GetPropertyRateStrategyById(Guid propertyRateStrategyId)
        {
            var propertyRateStrategy = await Context.PropertyRateStrategies
                                           .Include(s => s.PropertyRateStrategyRoomTypeList)
                                           .ThenInclude(r => r.RoomType)                                         
                                           .Where(x => x.Id == propertyRateStrategyId)
                                           .FirstOrDefaultAsync();

            var dtoReturn = new PropertyRateStrategyDto();

            if (propertyRateStrategy != null)
            {
                var dto = new PropertyRateStrategyDto
                {
                    Id = propertyRateStrategy.Id,
                    IsActive = propertyRateStrategy.IsActive,
                    Name = propertyRateStrategy.Name,
                    StartDate = propertyRateStrategy.StartDate,
                    EndDate = propertyRateStrategy.EndDate
                };


                foreach (var propertyRateStrategyRoomType in propertyRateStrategy.PropertyRateStrategyRoomTypeList)
                {
                    var propertyRateStrategyRoomTypeDto = new PropertyRateStrategyRoomTypeDto
                    {
                        Id = propertyRateStrategyRoomType.Id,
                        PropertyRateStrategyId = propertyRateStrategyRoomType.PropertyRateStrategyId,
                        RoomTypeId = propertyRateStrategyRoomType.RoomTypeId,
                        RoomTypeName = propertyRateStrategyRoomType.RoomType?.Abbreviation,
                        MinOccupationPercentual = propertyRateStrategyRoomType.MinOccupationPercentual,
                        MaxOccupationPercentual = propertyRateStrategyRoomType.MaxOccupationPercentual,
                        IsDecreased = propertyRateStrategyRoomType.IsDecreased,
                        PercentualAmount = propertyRateStrategyRoomType.PercentualAmount,
                        IncrementAmount = propertyRateStrategyRoomType.IncrementAmount
                    };

                    dto.PropertyRateStrategyRoomTypeList.Add(propertyRateStrategyRoomTypeDto);
                }

                dtoReturn = dto;
            }

            return dtoReturn;
        }
    }
}
