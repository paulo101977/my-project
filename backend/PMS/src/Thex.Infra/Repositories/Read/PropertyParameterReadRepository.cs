﻿using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using Thex.Common.Enumerations;
using Thex.Domain.Entities;
using Thex.Dto;
using Thex.Dto.PropertyAuditProcessStep;
using Thex.Dto.PropertyParameter;
using Thex.Infra.ReadInterfaces;
using Tnf.Dto;
using Tnf.EntityFrameworkCore.Repositories;
using Tnf.EntityFrameworkCore;
using Thex.Infra.Context;
using Tnf.Localization;
using Thex.Common;
using TimeZoneNames;
using System.Globalization;
using Tnf.Notifications;
using Thex.Dto.IntegrationPartner.IntegrationPartnerProperty;

namespace Thex.Infra.Repositories.Read
{
    public class PropertyParameterReadRepository : EfCoreRepositoryBase<ThexContext, PropertyParameter>, IPropertyParameterReadRepository
    {
        private readonly ICurrencyReadRepository _currencyReadRepository;
        private readonly IBillingItemReadRepository _billingItemReadRepository;
        private readonly ILocalizationManager _localizationManager;
        private readonly ILocationReadRepository _locationReadRepository;
        private readonly INotificationHandler Notification;

        public PropertyParameterReadRepository(
            IDbContextProvider<ThexContext> dbContextProvider,
            ICurrencyReadRepository currencyReadRepository,
            IBillingItemReadRepository billingItemReadRepository,
            ILocalizationManager localizationManager, ILocationReadRepository locationReadRepository,
            INotificationHandler notificationHandler) : base(dbContextProvider)
        {
            _currencyReadRepository = currencyReadRepository;
            _billingItemReadRepository = billingItemReadRepository;
            _localizationManager = localizationManager;
            _locationReadRepository = locationReadRepository;
            Notification = notificationHandler;
        }

        public PropertyParameterDto GetPropertyParameterById(Guid propertyParameterId)
        {
            return Context
                .PropertyParameters
                .Where(x => x.Id == propertyParameterId).FirstOrDefault().MapTo<PropertyParameterDto>();
        }

        public PropertyParameterDto GetPropertyParameterByApplicationParameterId(int applicationParameterId)
        {
            return Context
                .PropertyParameters
                .Where(x => x.ApplicationParameterId == applicationParameterId).FirstOrDefault().MapTo<PropertyParameterDto>();
        }

        public PropertyParameterDto GetPropertyParameterForPropertyIdAndApplicationParameterId(int propertyId, int applicationParameterId)
        {
            var parameter = (from a in Context.ApplicationParameters

                             join p in Context.PropertyParameters.Where(e => e.PropertyId == propertyId && e.ApplicationParameterId == applicationParameterId)
                             on a.Id equals p.ApplicationParameterId into propertyParameters
                             from leftPropertyParameters in propertyParameters.DefaultIfEmpty()

                             join t in Context.ParameterTypes on a.ParameterTypeId equals t.Id

                             where a.Id == applicationParameterId

                             select new PropertyParameterDto
                             {
                                 Id = leftPropertyParameters != null ? leftPropertyParameters.Id : Guid.Empty,
                                 ParameterTypeName = t.ParameterTypeName,
                                 PropertyParameterValue = leftPropertyParameters != null ? leftPropertyParameters.PropertyParameterValue : a.ParameterDefaultValue,
                                 PropertyParameterMinValue = leftPropertyParameters != null ? leftPropertyParameters.PropertyParameterMinValue : a.ParameterMinValue,
                                 PropertyParameterMaxValue = leftPropertyParameters != null ? leftPropertyParameters.PropertyParameterMaxValue : a.ParameterMaxValue,
                                 PropertyParameterPossibleValues = leftPropertyParameters != null ? leftPropertyParameters.PropertyParameterPossibleValues : a.ParameterPossibleValues,
                                 ParameterDescription = a.ParameterName,
                                 FeatureGroupId = a.FeatureGroupId,
                                 ApplicationParameterId = a.Id,
                                 PropertyId = leftPropertyParameters != null ? leftPropertyParameters.PropertyId : 0
                             }).FirstOrDefault();

            return parameter;
        }

        private AllParameters GeneralParameters(List<PropertyParameterDto> generalParameters)
        {
            var filterByGeneral = generalParameters.Where(x => x.FeatureGroupId == (int)FeatureGroupEnum.GeneralParameters).ToList();
            var currencies = _currencyReadRepository.GetAllCurrency();

            var generalNewList = new List<PropertyParameterDto>();

            var allParameters = new AllParameters();
            allParameters.FeatureGroupId = filterByGeneral.FirstOrDefault().FeatureGroupId;
            allParameters.FeatureGroupName = _localizationManager.GetString(AppConsts.LocalizationSourceName, ((FeatureGroupEnum)filterByGeneral.FirstOrDefault().FeatureGroupId).ToString());

            foreach (var general in filterByGeneral)
            {
                var generalNew = new PropertyParameterDto
                {
                    Id = general.Id,
                    ApplicationParameterId = general.ApplicationParameterId,
                    FeatureGroupId = general.FeatureGroupId,
                    ParameterDescription = general.ParameterDescription,
                    PropertyId = general.PropertyId,
                    ParameterTypeName = general.ParameterTypeName,
                    IsActive = general.IsActive,
                    PropertyParameterValue = general.PropertyParameterValue,
                    PropertyParameterMaxValue = general.PropertyParameterMaxValue,
                    PropertyParameterMinValue = general.PropertyParameterMinValue,
                    CanInactive = false
                };

                int vSelection = general.ApplicationParameterId;

                switch (vSelection)
                {
                    case (int)ApplicationParameterEnum.ParameterTimeZone:


                        var timezones = GetAllTimeZones(general.PropertyId);

                        if (timezones != null)
                        {

                            var timezonesForJson = from key in timezones.Keys
                                                   select new ListToJsonDto
                                                   {
                                                       value = key,
                                                       title = timezones[key]
                                                   };

                            string timezonesJson = JsonConvert.SerializeObject(timezonesForJson);
                            generalNew.PropertyParameterPossibleValues = timezonesJson;

                        }
                        break;

                    case (int)ApplicationParameterEnum.ParameterDefaultCurrency:

                        List<CurrencyDto> currencyDtos = currencies.Items.ToList();
                        var currecyForJson = (from currency in currencyDtos
                                              select new ListToJsonDto
                                              {
                                                  value = currency.Id.ToString(),
                                                  title = $"{currency.Symbol}-{currency.CurrencyName}"
                                              });

                        string currenciesJson = JsonConvert.SerializeObject(currecyForJson);
                        generalNew.PropertyParameterPossibleValues = currenciesJson;

                        break;

                    default:

                        generalNew.PropertyParameterPossibleValues = general.PropertyParameterPossibleValues;

                        break;
                }

                generalNewList.Add(generalNew);
            }

            allParameters.Parameters = generalNewList;

            return allParameters;

        }

        private AllParameters TypesOfService(List<PropertyParameterDto> typesOfService, int propertyId)
        {
            var filterByTypesServices = typesOfService.Where(x => x.FeatureGroupId == (int)FeatureGroupEnum.TypesOfService).ToList();
            var billingItemServiceDaily = _billingItemReadRepository.GetAllServiceFilterTypeDailyByProperty(propertyId);
            var typeNewList = new List<PropertyParameterDto>();

            var allParameters = new AllParameters();
            allParameters.FeatureGroupId = filterByTypesServices.FirstOrDefault().FeatureGroupId;
            allParameters.FeatureGroupName = _localizationManager.GetString(AppConsts.LocalizationSourceName, ((FeatureGroupEnum)filterByTypesServices.FirstOrDefault().FeatureGroupId).ToString());

            foreach (var typeService in filterByTypesServices)
            {
                var typeServiceNew = new PropertyParameterDto
                {
                    Id = typeService.Id,
                    ApplicationParameterId = typeService.ApplicationParameterId,
                    FeatureGroupId = typeService.FeatureGroupId,
                    ParameterDescription = typeService.ParameterDescription,
                    PropertyId = typeService.PropertyId,
                    ParameterTypeName = typeService.ParameterTypeName,
                    IsActive = typeService.IsActive,
                    PropertyParameterValue = typeService.PropertyParameterValue,
                    PropertyParameterMaxValue = typeService.PropertyParameterMaxValue,
                    PropertyParameterMinValue = typeService.PropertyParameterMinValue,
                    CanInactive = false
                };

                if (typeService.ApplicationParameterId == (int)ApplicationParameterEnum.ParameterDaily
                 || typeService.ApplicationParameterId == (int)ApplicationParameterEnum.ParameterDifferenceDaily)
                {
                    var billingItems = (from billingItem in billingItemServiceDaily
                                        select new ListToJsonDto
                                        {
                                            value = billingItem.Id.ToString(),
                                            title = billingItem.BillingItemName
                                        });
                    string billingItemJson = JsonConvert.SerializeObject(billingItems);
                    typeServiceNew.PropertyParameterPossibleValues = billingItemJson;

                }
                else
                {
                    typeServiceNew.PropertyParameterPossibleValues = typeService.PropertyParameterPossibleValues;
                }
                typeNewList.Add(typeServiceNew);

            }

            allParameters.Parameters = typeNewList;
            return allParameters;
        }

        private AllParameters AgeGroupChildren(List<PropertyParameterDto> ageGroupChieldren)
        {
            var ageGroupChieldrenList = ageGroupChieldren.Where(x => x.FeatureGroupId == (int)FeatureGroupEnum.AgeGroupChieldren).ToList();
            var allParameters = new AllParameters();

            var ageNewList = new List<PropertyParameterDto>();

            foreach (var ageGroupChildren in ageGroupChieldrenList)
            {
                var ageGroupChildrenNew = new PropertyParameterDto
                {
                    Id = ageGroupChildren.Id,
                    ApplicationParameterId = ageGroupChildren.ApplicationParameterId,
                    FeatureGroupId = ageGroupChildren.FeatureGroupId,
                    ParameterDescription = ageGroupChildren.ParameterDescription,
                    PropertyId = ageGroupChildren.PropertyId,
                    ParameterTypeName = ageGroupChildren.ParameterTypeName,
                    IsActive = ageGroupChildren.IsActive,
                    PropertyParameterValue = ageGroupChildren.PropertyParameterValue,
                    PropertyParameterMaxValue = ageGroupChildren.PropertyParameterMaxValue,
                    PropertyParameterMinValue = ageGroupChildren.PropertyParameterMinValue,

                };

                if (ageGroupChildrenNew.ApplicationParameterId == (int)ApplicationParameterEnum.ParameterChildren_2 || ageGroupChildrenNew.ApplicationParameterId == (int)ApplicationParameterEnum.ParameterChildren_3)
                    ageGroupChildrenNew.CanInactive = true;
                else
                    ageGroupChildrenNew.CanInactive = false;

                ageNewList.Add(ageGroupChildrenNew);
            }



            allParameters.FeatureGroupId = ageGroupChieldrenList.FirstOrDefault().FeatureGroupId;
            allParameters.FeatureGroupName = _localizationManager.GetString(AppConsts.LocalizationSourceName, ((FeatureGroupEnum)ageGroupChieldrenList.FirstOrDefault().FeatureGroupId).ToString());
            allParameters.Parameters = ageNewList;

            return allParameters;

        }

        public PropertyParameterDividedByGroupDto GetPropertyParameterForPropertyId(int propertyId)
        {
            var dbBaseQuery = (from a in Context.ApplicationParameters
                               join p in Context.PropertyParameters.Where(e => e.PropertyId == propertyId)
                               on a.Id equals p.ApplicationParameterId into propertyParameters
                               from leftPropertyParameters in propertyParameters.DefaultIfEmpty()
                               join t in Context.ParameterTypes on a.ParameterTypeId equals t.Id
                               select new PropertyParameterDto
                               {
                                   Id = leftPropertyParameters != null ? leftPropertyParameters.Id : Guid.Empty,
                                   ParameterTypeName = t.ParameterTypeName,
                                   PropertyParameterValue = leftPropertyParameters != null ? leftPropertyParameters.PropertyParameterValue : a.ParameterDefaultValue,
                                   PropertyParameterMinValue = leftPropertyParameters != null ? leftPropertyParameters.PropertyParameterMinValue : a.ParameterMinValue,
                                   PropertyParameterMaxValue = leftPropertyParameters != null ? leftPropertyParameters.PropertyParameterMaxValue : a.ParameterMaxValue,
                                   PropertyParameterPossibleValues = leftPropertyParameters != null ? leftPropertyParameters.PropertyParameterPossibleValues : a.ParameterPossibleValues,
                                   ParameterDescription = a.ParameterName,
                                   ApplicationParameterId = a.Id,
                                   FeatureGroupId = a.FeatureGroupId,
                                   PropertyId = leftPropertyParameters != null ? leftPropertyParameters.PropertyId : propertyId,
                                   IsActive = leftPropertyParameters != null ? leftPropertyParameters.IsActive : true

                               }).ToList();

            foreach (var result in dbBaseQuery)
                result.ParameterDescription = _localizationManager.GetString(AppConsts.LocalizationSourceName, ((ApplicationParameterTranslateEnum)result.ApplicationParameterId).ToString());

            var parameterNew = new PropertyParameterDividedByGroupDto();

            parameterNew.propertyParameters.Add(GeneralParameters(dbBaseQuery));
            parameterNew.propertyParameters.Add(TypesOfService(dbBaseQuery, propertyId));
            parameterNew.propertyParameters.Add(AgeGroupChildren(dbBaseQuery));


            return parameterNew;

        }

        public async Task<List<PropertyParameterDto>> GetChildrenAgeGroupList(int propertyId)
        {
            var result = await (from a in Context.ApplicationParameters.AsNoTracking()

                                join p in Context.PropertyParameters.AsNoTracking().Where(e => e.PropertyId == propertyId)
                                 on a.Id equals p.ApplicationParameterId into propertyParameters
                                from leftPropertyParameters in propertyParameters.DefaultIfEmpty()

                                join t in Context.ParameterTypes on a.ParameterTypeId equals t.Id

                                where a.FeatureGroupId == (int)FeatureGroupEnum.AgeGroupChieldren

                                select new PropertyParameterDto
                                {
                                    Id = leftPropertyParameters != null ? leftPropertyParameters.Id : Guid.Empty,
                                    ParameterTypeName = t.ParameterTypeName,
                                    PropertyParameterValue = leftPropertyParameters != null ? leftPropertyParameters.PropertyParameterValue : a.ParameterDefaultValue,
                                    PropertyParameterMinValue = leftPropertyParameters != null ? leftPropertyParameters.PropertyParameterMinValue : a.ParameterMinValue,
                                    PropertyParameterMaxValue = leftPropertyParameters != null ? leftPropertyParameters.PropertyParameterMaxValue : a.ParameterMaxValue,
                                    PropertyParameterPossibleValues = leftPropertyParameters != null ? leftPropertyParameters.PropertyParameterPossibleValues : a.ParameterPossibleValues,
                                    ParameterDescription = a.ParameterName,
                                    FeatureGroupId = a.FeatureGroupId,
                                    ApplicationParameterId = a.Id,
                                    PropertyId = leftPropertyParameters != null ? leftPropertyParameters.PropertyId : 0,
                                    IsActive = leftPropertyParameters != null ? leftPropertyParameters.IsActive : (bool?)null
                                })
                          .OrderBy(e => e.ApplicationParameterId)
                          .ToListAsync();

            foreach (var parameter in result)
                parameter.ParameterDescription = _localizationManager.GetString(AppConsts.LocalizationSourceName, ((ApplicationParameterTranslateEnum)parameter.ApplicationParameterId).ToString());


            return result;

        }
        public ListDto<PropertyParameterDto> GetChildrenAgeGroupForRoomTypeList(int propertyId)
        {
            var dbBaseQuery = (from a in Context.ApplicationParameters.AsNoTracking()

                               join p in Context.PropertyParameters.AsNoTracking().Where(e => e.PropertyId == propertyId)
                                on a.Id equals p.ApplicationParameterId into propertyParameters
                               from leftPropertyParameters in propertyParameters.DefaultIfEmpty()

                               join t in Context.ParameterTypes on a.ParameterTypeId equals t.Id

                               where a.FeatureGroupId == (int)FeatureGroupEnum.AgeGroupChieldren

                               select new PropertyParameterDto
                               {
                                   Id = leftPropertyParameters != null ? leftPropertyParameters.Id : Guid.Empty,
                                   ParameterTypeName = t.ParameterTypeName,
                                   PropertyParameterValue = leftPropertyParameters != null ? leftPropertyParameters.PropertyParameterValue : a.ParameterDefaultValue,
                                   PropertyParameterMinValue = leftPropertyParameters != null ? leftPropertyParameters.PropertyParameterMinValue : a.ParameterMinValue,
                                   PropertyParameterMaxValue = leftPropertyParameters != null ? leftPropertyParameters.PropertyParameterMaxValue : a.ParameterMaxValue,
                                   PropertyParameterPossibleValues = leftPropertyParameters != null ? leftPropertyParameters.PropertyParameterPossibleValues : a.ParameterPossibleValues,
                                   ParameterDescription = a.ParameterName,
                                   FeatureGroupId = a.FeatureGroupId,
                                   ApplicationParameterId = a.Id,
                                   PropertyId = leftPropertyParameters != null ? leftPropertyParameters.PropertyId : 0,
                                   IsActive = leftPropertyParameters != null ? leftPropertyParameters.IsActive : (bool?)null
                               })
                          .OrderBy(e => e.ApplicationParameterId);

            var propertyParametersList = new List<PropertyParameterDto>();

            foreach (var item in dbBaseQuery.ToList())
            {
                item.ParameterDescription = _localizationManager.GetString(AppConsts.LocalizationSourceName, ((ApplicationParameterTranslateEnum)item.ApplicationParameterId).ToString());
                propertyParametersList.Add(item.MapTo<PropertyParameterDto>());
            }

            return new ListDto<PropertyParameterDto>
            {
                HasNext = false,
                Items = propertyParametersList,

            };
        }

        public List<PropertyParameterDto> GetPropertyParametersByFeatureGroupId(int propertyId, int FeatureGroupId)
        {
            var result = (from a in Context.ApplicationParameters.AsNoTracking()

                          join p in Context.PropertyParameters.AsNoTracking().Where(e => e.PropertyId == propertyId)
                           on a.Id equals p.ApplicationParameterId into propertyParameters
                          from leftPropertyParameters in propertyParameters.DefaultIfEmpty()

                          join t in Context.ParameterTypes on a.ParameterTypeId equals t.Id

                          where a.FeatureGroupId == FeatureGroupId

                          select new PropertyParameterDto
                          {
                              Id = leftPropertyParameters != null ? leftPropertyParameters.Id : Guid.Empty,
                              ParameterTypeName = t.ParameterTypeName,
                              PropertyParameterValue = leftPropertyParameters != null ? leftPropertyParameters.PropertyParameterValue : a.ParameterDefaultValue,
                              PropertyParameterMinValue = leftPropertyParameters != null ? leftPropertyParameters.PropertyParameterMinValue : a.ParameterMinValue,
                              PropertyParameterMaxValue = leftPropertyParameters != null ? leftPropertyParameters.PropertyParameterMaxValue : a.ParameterMaxValue,
                              PropertyParameterPossibleValues = leftPropertyParameters != null ? leftPropertyParameters.PropertyParameterPossibleValues : a.ParameterPossibleValues,
                              ParameterDescription = a.ParameterName,
                              FeatureGroupId = a.FeatureGroupId,
                              ApplicationParameterId = a.Id,
                              PropertyId = leftPropertyParameters != null ? leftPropertyParameters.PropertyId : 0,
                              IsActive = leftPropertyParameters != null ? leftPropertyParameters.IsActive : true
                          })
                          .OrderBy(e => e.ApplicationParameterId).ToList();


            foreach (var item in result)
                item.ParameterDescription = _localizationManager.GetString(AppConsts.LocalizationSourceName, ((ApplicationParameterTranslateEnum)item.ApplicationParameterId).ToString());

            return result;
        }


        public PropertyParameterDto GetPropertyParameterMaxChildrenAge(int propertyId)
        {
            var propertyParameterList = (from a in Context.ApplicationParameters.AsNoTracking()

                                         join p in Context.PropertyParameters.AsNoTracking().Where(e => e.PropertyId == propertyId)
                                          on a.Id equals p.ApplicationParameterId into propertyParameters
                                         from leftPropertyParameters in propertyParameters.DefaultIfEmpty()

                                         join t in Context.ParameterTypes on a.ParameterTypeId equals t.Id

                                         where a.FeatureGroupId == (int)FeatureGroupEnum.AgeGroupChieldren
                                         select new PropertyParameterDto
                                         {
                                             Id = leftPropertyParameters != null ? leftPropertyParameters.Id : Guid.Empty,
                                             ParameterTypeName = t.ParameterTypeName,
                                             PropertyParameterValue = leftPropertyParameters != null ? leftPropertyParameters.PropertyParameterValue : a.ParameterDefaultValue,
                                             PropertyParameterMinValue = leftPropertyParameters != null ? leftPropertyParameters.PropertyParameterMinValue : a.ParameterMinValue,
                                             PropertyParameterMaxValue = leftPropertyParameters != null ? leftPropertyParameters.PropertyParameterMaxValue : a.ParameterMaxValue,
                                             PropertyParameterPossibleValues = leftPropertyParameters != null ? leftPropertyParameters.PropertyParameterPossibleValues : a.ParameterPossibleValues,
                                             ParameterDescription = a.ParameterDescription,
                                             FeatureGroupId = a.FeatureGroupId,
                                             ApplicationParameterId = a.Id,
                                             PropertyId = leftPropertyParameters != null ? leftPropertyParameters.PropertyId : 0,
                                             IsActive = leftPropertyParameters != null ? leftPropertyParameters.IsActive : (bool?)null
                                         })
                          .OrderByDescending(e => e.ApplicationParameterId)
                          .ToList();

            foreach (var propertyParameterDto in propertyParameterList)
                if (propertyParameterDto.IsActive.HasValue && propertyParameterDto.IsActive.Value && !string.IsNullOrEmpty(propertyParameterDto.PropertyParameterValue))
                {
                    propertyParameterDto.ParameterDescription = _localizationManager.GetString(AppConsts.LocalizationSourceName, ((ApplicationParameterTranslateEnum)propertyParameterDto.ApplicationParameterId).ToString());
                    return propertyParameterDto;
                }
            return null;
        }

        private PropertyParameter GetByPropertyIdAndApplicationParameterId(int propertyId, int applicationParameterId)
        {
            var result = Context.PropertyParameters.IgnoreQueryFilters().Where(p => p.PropertyId == propertyId && p.ApplicationParameterId == applicationParameterId).FirstOrDefault();

            return result;
        }

        public DateTime? GetSystemDate(int propertyId)
        {
            const int applicationParameterId = 10;

            var propertyParameter = GetByPropertyIdAndApplicationParameterId(propertyId, applicationParameterId);
            if (propertyParameter == null || (propertyParameter != null && propertyParameter.PropertyParameterValue == null))
            {
                return null;
            }
            else
            {
                try
                {
                    var value = propertyParameter.PropertyParameterValue.Split("/");
                    return new DateTime(Convert.ToInt32(value[2]), Convert.ToInt32(value[1]), Convert.ToInt32(value[0]));
                }
                catch
                {
                    var value = propertyParameter.PropertyParameterValue.Split("-");
                    return new DateTime(Convert.ToInt32(value[0]), Convert.ToInt32(value[1]), Convert.ToInt32(value[2]));
                }

            }
        }

        public Task<DateTime?> GetSystemDateAsync(int propertyId)
        {
            const int applicationParameterId = 10;

            var propertyParameter = GetByPropertyIdAndApplicationParameterId(propertyId, applicationParameterId);
            if (propertyParameter == null || (propertyParameter != null && propertyParameter.PropertyParameterValue == null))
            {
                return null;
            }
            else
            {
                try
                {
                    var value = propertyParameter.PropertyParameterValue.Split("/");
                    var systemDate = new DateTime(Convert.ToInt32(value[2]), Convert.ToInt32(value[1]), Convert.ToInt32(value[0]));
                    return Task<DateTime?>.Factory.StartNew(() => systemDate);
                }
                catch
                {
                    var value = propertyParameter.PropertyParameterValue.Split("-");
                    var systemDate = new DateTime(Convert.ToInt32(value[0]), Convert.ToInt32(value[1]), Convert.ToInt32(value[2]));
                    return Task<DateTime?>.Factory.StartNew(() => systemDate);
                }
            }
        }

        public int? GetDailyBillingItem(int propertyId)
        {
            const int applicationParameterId = (int)ApplicationParameterTranslateEnum.ParameterDailyTranslate;

            var propertyParameter = GetByPropertyIdAndApplicationParameterId(propertyId, applicationParameterId);
            if (propertyParameter == null || (propertyParameter != null && propertyParameter.PropertyParameterValue == null))
            {
                return null;
            }
            else
            {
                return Convert.ToInt32(propertyParameter.PropertyParameterValue);
            }
        }

        public int? GetDifferenceDailyBillingItem(int propertyId)
        {
            const int applicationParameterId = (int)ApplicationParameterTranslateEnum.ParameterDifferenceDailyTranslate;

            var propertyParameter = GetByPropertyIdAndApplicationParameterId(propertyId, applicationParameterId);
            if (propertyParameter == null || (propertyParameter != null && propertyParameter.PropertyParameterValue == null))
            {
                return null;
            }
            else
            {
                return Convert.ToInt32(propertyParameter.PropertyParameterValue);
            }
        }

        public PropertyAuditProcessStepNumber GetAuditStepByPropertyId(int propertyId)
        {
            const int applicationParameterId = 11;

            var propertyParameter = GetByPropertyIdAndApplicationParameterId(propertyId, applicationParameterId);
            if (propertyParameter == null || (propertyParameter != null && (String.IsNullOrEmpty(propertyParameter.PropertyParameterValue))))
            {
                return new PropertyAuditProcessStepNumber { StepNumber = 0 };
            }
            else
            {
                return new PropertyAuditProcessStepNumber { StepNumber = Convert.ToInt32(propertyParameter.PropertyParameterValue) };
            }
        }

        public string GetTimeZoneByPropertyId(int propertyId)
        {
            const int applicationParameterId = 12;

            var propertyParameter = GetByPropertyIdAndApplicationParameterId(propertyId, applicationParameterId);
            if (propertyParameter == null || (propertyParameter != null && propertyParameter.PropertyParameterValue == null))
            {
                return null;
            }
            else
            {
                return propertyParameter.PropertyParameterValue;
            }
        }

        public int? GetNumberOfDaysToChangeRoomStatusByPropertyId(int propertyId)
        {
            const int applicationParameterId = 13;

            var propertyParameter = GetByPropertyIdAndApplicationParameterId(propertyId, applicationParameterId);

            if (propertyParameter == null || (propertyParameter != null && propertyParameter.PropertyParameterValue == null))
            {
                return null;
            }
            else
            {
                return Convert.ToInt32(propertyParameter.PropertyParameterValue);
            }
        }

        public IDictionary<string, string> GetAllTimeZones(int pPropertyId)
        {
            LocationDto vLocationDto = null;

            vLocationDto = _locationReadRepository.GetLocationDtoListByPropertyUId(pPropertyId);

            if (vLocationDto == null)
                return null;

            var zones = TZNames.GetTimeZonesForCountry(vLocationDto.CountryCode, CultureInfo.CurrentCulture.Name, DateTimeOffset.UtcNow)
            .Select(y => new { CountryCode = vLocationDto.CountryCode, Country = vLocationDto.Country, TimeZoneId = y.Key, TimeZoneName = y.Value })
                .GroupBy(x => x.TimeZoneId)
            .ToDictionary(x => x.Key, x => $"{x.First().Country} - {x.First().TimeZoneName}");

            return zones;
        }

        public async Task<PropertyDto> GetDtoByIdAsync(int propertyId)
        {
            var baseQuery = await (from c in Context.Chains.AsNoTracking().IgnoreQueryFilters()
                                   join t in Context.Tenants.AsNoTracking().IgnoreQueryFilters() on c.TenantId equals t.Id
                                   join b in Context.Brands.AsNoTracking().IgnoreQueryFilters() on c.Id equals b.ChainId
                                   join p in Context.Properties.AsNoTracking().IgnoreQueryFilters() on b.Id equals p.BrandId
                                   join co in Context.Companies.AsNoTracking().IgnoreQueryFilters() on p.CompanyId equals co.Id
                                   join pc in Context.PropertyContracts.AsNoTracking().IgnoreQueryFilters() on p.Id equals pc.PropertyId
                                   into pcp
                                   from pc in pcp.DefaultIfEmpty()
                                   where p.Id == propertyId
                                   select new PropertyDto
                                   {
                                       Id = p.Id,
                                       PropertyUId = p.PropertyUId,
                                       Name = p.Name,
                                       TenantId = t.Id,
                                       ChainId = c.Id,
                                       BrandId = b.Id,
                                       CompanyId = co.Id,
                                       ShortName = co.ShortName,
                                       TradeName = co.TradeName
                                   }).FirstOrDefaultAsync();

            baseQuery.IntegrationPartnerPropertyList = await
                  (from ipp in Context.IntegrationPartnerProperties
                   join partner in Context.IntegrationPartners on ipp.IntegrationPartnerId equals partner.Id into ip
                   from partner in ip.DefaultIfEmpty()
                   where ipp.PropertyId == propertyId
                   select new IntegrationPartnerPropertyDto()
                   {
                       Id = ipp.Id,
                       PropertyId = ipp.PropertyId,
                       IntegrationCode = ipp.IntegrationCode,
                       IsActive = ipp.IsActive,
                       PartnerId = ipp.PartnerId,
                       PartnerName = partner != null ? partner.IntegrationPartnerName : string.Empty,
                       TokenClient = partner != null ? partner.TokenClient : string.Empty,
                       IntegrationNumber = ipp.IntegrationNumber,
                       IntegrationPartnerType = partner != null ? (int?)partner.IntegrationPartnerType : null,
                   }).ToListAsync();

            return baseQuery;
        }
    }
}
