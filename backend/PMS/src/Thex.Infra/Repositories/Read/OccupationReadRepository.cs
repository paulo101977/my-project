﻿// //  <copyright file="OccupationReadRepository.cs" company="TOTVS">
// //  Copyright (c) TOTVS. All rights reserved.
// //  </copyright>
using Thex.Domain.Entities;
using Thex.Infra.ReadInterfaces;

namespace Thex.EntityFrameworkCore.Repositories
{
    using System.Linq;

    using Tnf.EntityFrameworkCore.Repositories;
    using Tnf.EntityFrameworkCore;
    using Thex.Infra.Context;

    public class OccupationReadRepository : EfCoreRepositoryBase<ThexContext, Occupation>, IOccupationReadRepository
    {

        public OccupationReadRepository(IDbContextProvider<ThexContext> dbContextProvider)
            : base(dbContextProvider)
        {

        }

        public Occupation GetOccupationByNameAndCompanyId(string name, int companyId)
        {
            Occupation occupation =
                this.Context.Occupations.FirstOrDefault(o => o.Name.ToLower().Equals(name.ToLower()) 
                && o.CompanyId == companyId);
            return occupation;
        }
    }
}