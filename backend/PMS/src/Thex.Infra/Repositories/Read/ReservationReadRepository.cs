﻿//  <copyright file="ReservationReadRepository.cs" company="TOTVS">
//  Copyright (c) TOTVS. All rights reserved.
//  </copyright>
namespace Thex.EntityFrameworkCore.Repositories
{
    using Microsoft.EntityFrameworkCore;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Thex.Common;
    using Thex.Common.Enumerations;
    using Thex.Domain.Entities;
    using Thex.Dto;
    using Thex.Dto.Reservation;
    using Thex.Dto.ReservationCompanyClient;
    using Thex.Dto.ReservationGuest;
    using Thex.Infra.Context;
    using Thex.Infra.ReadInterfaces;
    using Tnf.Dto;
    using Tnf.EntityFrameworkCore;
    using Tnf.EntityFrameworkCore.Repositories;
    using Tnf.Localization;

    public class ReservationReadRepository : EfCoreRepositoryBase<ThexContext, Reservation>, IReservationReadRepository
    {
        private readonly ILocalizationManager _localizationManager;
        private readonly IPropertyParameterReadRepository _propertyParameterReadRepository;

        public ReservationReadRepository(
            IDbContextProvider<ThexContext> dbContextProvider,
            ILocalizationManager localizationManager,
            IPropertyParameterReadRepository propertyParameterReadRepository)
            : base(dbContextProvider)
        {
            _localizationManager = localizationManager;
            _propertyParameterReadRepository = propertyParameterReadRepository;
        }

        public Reservation GetWithReservationItemsAndRoomsAndRoomTypes(DefaultLongRequestDto request)
        {
            return Context
                        .Reservations

                        .Include(exp => exp.ReservationItemList).ThenInclude(exp => exp.Room)
                        .Include(exp => exp.ReservationItemList).ThenInclude(exp => exp.ReceivedRoomType)
                        .Include(exp => exp.ReservationItemList).ThenInclude(exp => exp.RequestedRoomType)
                        .Include(exp => exp.ReservationItemList).ThenInclude(exp => exp.RoomLayout)
                        .FirstOrDefault(exp => exp.Id == request.Id);
        }

        public IListDto<SearchReservationResultDto> GetAllByFilters(SearchReservationDto request, int propertyId)
        {
            var dbBaseQuery = SearchReservationBaseQuery(request, propertyId);

            var totalGuests = dbBaseQuery.Count();
            var totalReservations = dbBaseQuery.Count(x => x.IsMain);

            dbBaseQuery = GroupReservation(request, dbBaseQuery);
            dbBaseQuery = SearchReservationSorting(request, dbBaseQuery);
            dbBaseQuery = SearchReservationPaging(request, dbBaseQuery);

            var totalItemsDto = dbBaseQuery.Count();
            var resultDto = dbBaseQuery.ToList();

            foreach (var reservation in resultDto)
            {
                reservation.BillingAccountTypeName = _localizationManager.GetString(AppConsts.LocalizationSourceName, ((BillingAccountTypeEnum)reservation.BillingAccountTypeId).ToString());
                reservation.GuestReservationItemStatusName = _localizationManager.GetString(AppConsts.LocalizationSourceName, ((ReservationStatus)reservation.GuestReservationItemStatusId).ToString());
                reservation.ReservationItemStatusName = _localizationManager.GetString(AppConsts.LocalizationSourceName, ((ReservationStatus)reservation.ReservationItemStatusId).ToString());
                reservation.PaymentTypeName = _localizationManager.GetString(AppConsts.LocalizationSourceName, ((PaymentTypeEnum)reservation.PaymentTypeId).ToString());
            }

            var result =
                new ListReservationDto<SearchReservationResultDto>
                {
                    TotalGuests = totalGuests,
                    TotalReservations = totalReservations,

                    Items = resultDto,
                    HasNext = SearchReservationHasNext(request, totalItemsDto, resultDto),
                };

            return result;
        }

        public IListDto<SearchReservationResultDto> GetAllByFiltersV2(SearchReservationDto request, int propertyId)
        {
            var dbBaseQuery = SearchReservationBaseQueryV2(request, propertyId);

            var totalItemsDto = dbBaseQuery.Count();

            dbBaseQuery = SearchReservationSorting(request, dbBaseQuery);
            dbBaseQuery = SearchReservationPaging(request, dbBaseQuery);

            var resultDto = dbBaseQuery.ToList();

            foreach (var reservation in resultDto)
            {
                reservation.GuestReservationItemStatusName = _localizationManager.GetString(AppConsts.LocalizationSourceName, ((ReservationStatus)reservation.GuestReservationItemStatusId).ToString());
                reservation.ReservationItemStatusName = _localizationManager.GetString(AppConsts.LocalizationSourceName, ((ReservationStatus)reservation.ReservationItemStatusId).ToString());
            }

            var result =
                new ListDto<SearchReservationResultDto>
                {
                    Items = resultDto,
                    HasNext = SearchReservationHasNext(request, totalItemsDto, resultDto),
                };

            return result;
        }

        public SearchReservationResultDto GetReservationDetail(SearchReservationDto request, int propertyId)
        {
            var dbBaseQuery = GetReservationDetailBaseQuery(propertyId, request.ReservationId);

            var totalItemsDto = dbBaseQuery.Count();

            dbBaseQuery = SearchReservationSorting(request, dbBaseQuery);
            dbBaseQuery = SearchReservationPaging(request, dbBaseQuery);

            var reservation = dbBaseQuery.FirstOrDefault();
            reservation.GuestReservationItemStatusName = _localizationManager.GetString(AppConsts.LocalizationSourceName, ((ReservationStatus)reservation.GuestReservationItemStatusId).ToString());
            reservation.ReservationItemStatusName = _localizationManager.GetString(AppConsts.LocalizationSourceName, ((ReservationStatus)reservation.ReservationItemStatusId).ToString());

            return reservation;
        }

        private IQueryable<SearchReservationResultDto> PassingByFilter(SearchReservationDto request, IQueryable<SearchReservationResultDto> dbBaseQuery)
        {
            //passing by date
            if (request.PassingByInitial.HasValue && request.PassingByFinal.HasValue)
                dbBaseQuery = dbBaseQuery.Where(exp =>
                                            exp.ArrivalDate.Date <= request.PassingByFinal.Value.Date &&
                                            exp.DepartureDate.Date > request.PassingByInitial.Value.Date);
            return dbBaseQuery;
        }


        private IQueryable<SearchReservationResultDto> DeadlineDateFilter(SearchReservationDto request, IQueryable<SearchReservationResultDto> dbBaseQuery)
        {
            //deadline date
            if (request.DeadlineInitial.HasValue && request.DeadlineFinal.HasValue)
            {
                var validStatus = new int[] { (int)ReservationStatus.ToConfirm, (int)ReservationStatus.Confirmed }.ToList();
                dbBaseQuery = dbBaseQuery.Where(exp => exp.Deadline.HasValue &&
                                            exp.Deadline.Value.Date >= request.DeadlineInitial.Value.Date &&
                                            exp.Deadline.Value.Date <= request.DeadlineFinal.Value.Date &&
                                            validStatus.Contains(exp.ReservationItemStatusId));
            }

            return dbBaseQuery;
        }

        private IQueryable<SearchReservationResultDto> DepartureDateFilter(SearchReservationDto request, IQueryable<SearchReservationResultDto> dbBaseQuery)
        {
            //departure date
            if (request.DepartureInitial.HasValue && request.DepartureFinal.HasValue)
                dbBaseQuery = dbBaseQuery.Where(exp =>
                                            exp.DepartureDate.Date >= request.DepartureInitial.Value.Date &&
                                            exp.DepartureDate.Date <= request.DepartureFinal.Value.Date);
            return dbBaseQuery;
        }

        private IQueryable<SearchReservationResultDto> ArrivalDateFilter(SearchReservationDto request, IQueryable<SearchReservationResultDto> dbBaseQuery)
        {
            //arrival date
            if (request.ArrivalInitial.HasValue && request.ArrivalFinal.HasValue)
                dbBaseQuery = dbBaseQuery.Where(exp =>
                                            exp.ArrivalDate.Date >= request.ArrivalInitial.Value.Date &&
                                            exp.ArrivalDate.Date <= request.ArrivalFinal.Value.Date);
            return dbBaseQuery;
        }

        private IQueryable<SearchReservationResultDto> CreationDateFilter(SearchReservationDto request, IQueryable<SearchReservationResultDto> dbBaseQuery)
        {
            //creation date
            if (request.CreationInitial.HasValue && request.CreationFinal.HasValue)
                dbBaseQuery = dbBaseQuery.Where(exp =>
                                            exp.CreationTime.Date >= request.CreationInitial.Value.Date &&
                                            exp.CreationTime.Date <= request.CreationFinal.Value.Date);
            return dbBaseQuery;
        }

        private IQueryable<SearchReservationResultDto> CancellationDateFilter(SearchReservationDto request, IQueryable<SearchReservationResultDto> dbBaseQuery)
        {
            //CancellationDateFilter date
            if (request.CancellationInitial.HasValue && request.CancellationFinal.HasValue)
                dbBaseQuery = dbBaseQuery.Where(exp => exp.CancellationDate.HasValue &&
                                            exp.CancellationDate.Value.Date >= request.CancellationInitial.Value.Date &&
                                            exp.CancellationDate.Value.Date <= request.CancellationFinal.Value.Date);
            return dbBaseQuery;
        }

        private IQueryable<SearchReservationResultDto> ReservationItemStatusFilter(SearchReservationDto request, IQueryable<SearchReservationResultDto> dbBaseQuery)
        {
            //reservation item status

            if (request.ReservationItemStatus.HasValue)
                dbBaseQuery = dbBaseQuery.Where(exp => exp.ReservationItemStatusId == request.ReservationItemStatus);

            return dbBaseQuery;
        }

        private IQueryable<SearchReservationResultDto> ClientNameFilter(SearchReservationDto request, IQueryable<SearchReservationResultDto> dbBaseQuery)
        {
            if (!string.IsNullOrEmpty(request.ClientName))
                dbBaseQuery = dbBaseQuery.Where(exp => exp.CompanyClientName.Contains(request.ClientName));

            return dbBaseQuery;
        }

        private IQueryable<SearchReservationResultDto> FreeTextFilter(SearchReservationDto request, IQueryable<SearchReservationResultDto> dbBaseQuery)
        {
            //free text search | name, document, reservationCode, uhNumber

            if (!string.IsNullOrEmpty(request.FreeTextSearch))
                dbBaseQuery = dbBaseQuery.Where(exp => exp.GuestName.ToLower().Contains(request.FreeTextSearch.ToLower()) ||
                                                    exp.GuestDocument.Replace(".", "").Replace("-", "").Contains(request.FreeTextSearch.Replace(".", "").Replace("-", "")) ||
                                                    exp.ReservationItemCode.Contains(request.FreeTextSearch) ||
                                                    exp.PartnerReservationNumber.ToLower().Contains(request.FreeTextSearch.ToLower()) ||
                                                    exp.RoomNumber.Contains(request.FreeTextSearch));
            return dbBaseQuery;
        }

        private IQueryable<SearchReservationResultDto> GroupNameFilter(SearchReservationDto request, IQueryable<SearchReservationResultDto> dbBaseQuery)
        {
            //group name
            if (!string.IsNullOrEmpty(request.GroupName))
                dbBaseQuery = dbBaseQuery.Where(exp => exp.GroupName.ToLower().Contains(request.GroupName.ToLower()));

            return dbBaseQuery;
        }

        private IQueryable<SearchReservationResultDto> RoomTypeFilter(SearchReservationDto request, IQueryable<SearchReservationResultDto> dbBaseQuery)
        {
            if (request.RoomType.HasValue && request.RoomType != 0)
                dbBaseQuery = dbBaseQuery.Where(x => x.ReceivedRoomTypeId == request.RoomType.Value);

            return dbBaseQuery;
        }

        private IQueryable<SearchReservationResultDto> MealPlanTypeFilter(SearchReservationDto request, IQueryable<SearchReservationResultDto> dbBaseQuery)
        {
            if (request.MealPlanType.HasValue && request.MealPlanType != 0)
                dbBaseQuery = dbBaseQuery.Where(x => x.MealPlanTypeId == request.MealPlanType.Value);

            return dbBaseQuery;
        }

        private IQueryable<SearchReservationResultDto> MarketSegmentFilter(SearchReservationDto request, IQueryable<SearchReservationResultDto> dbBaseQuery)
        {
            if (request.MarketSegment.HasValue && request.MarketSegment != 0)
                dbBaseQuery = dbBaseQuery.Where(x => x.MarketSegmentId == request.MarketSegment.Value);

            return dbBaseQuery;
        }

        private IQueryable<SearchReservationResultDto> BusinessSourceFilter(SearchReservationDto request, IQueryable<SearchReservationResultDto> dbBaseQuery)
        {
            if (request.BusinessSource.HasValue && request.BusinessSource != 0)
                dbBaseQuery = dbBaseQuery.Where(x => x.BusinessSourceId == request.BusinessSource.Value);

            return dbBaseQuery;
        }

        private IQueryable<SearchReservationResultDto> RatePlanFilter(SearchReservationDto request, IQueryable<SearchReservationResultDto> dbBaseQuery)
        {
            if (request.RatePlan.HasValue && request.RatePlan != Guid.Empty)
                dbBaseQuery = dbBaseQuery.Where(x => x.RatePlanId == request.RatePlan.Value);

            return dbBaseQuery;
        }

        private IQueryable<SearchReservationResultDto> AdultsAndChildrenFilter(SearchReservationDto request, IQueryable<SearchReservationResultDto> dbBaseQuery)
        {
            if (request.Adults.HasValue && request.Adults >= 0)
                dbBaseQuery = dbBaseQuery.Where(x => x.AdultsCount == request.Adults.Value);

            if (request.Children.HasValue && request.Children >= 0)
                dbBaseQuery = dbBaseQuery.Where(x => x.ChildCount == request.Children.Value);

            return dbBaseQuery;
        }

        private IQueryable<SearchReservationResultDto> CompanyClientFilter(SearchReservationDto request, IQueryable<SearchReservationResultDto> dbBaseQuery)
        {
            if (!string.IsNullOrWhiteSpace(request.CompanyClientName))
            {
                if (request.CompanyClientId.HasValue && request.CompanyClientId != Guid.Empty)
                    dbBaseQuery = dbBaseQuery.Where(x => x.CompanyClientId == request.CompanyClientId.Value);
                else
                    dbBaseQuery = dbBaseQuery.Where(x => x.CompanyClientName.ToLower().Contains(request.CompanyClientName.ToLower()));
            }

            return dbBaseQuery;
        }

        private IQueryable<SearchReservationResultDto> GuestTypeFilter(SearchReservationDto request, IQueryable<SearchReservationResultDto> dbBaseQuery)
        {
            if (request.GuestType.HasValue && request.GuestType != 0)
                dbBaseQuery = dbBaseQuery.Where(x => x.GuestTypeId == request.GuestType.Value);

            return dbBaseQuery;
        }

        private IQueryable<SearchReservationResultDto> PartnerReservationNumberFilter(SearchReservationDto request, IQueryable<SearchReservationResultDto> dbBaseQuery)
        {
            if (!string.IsNullOrWhiteSpace(request.PartnerReservationNumber))
                dbBaseQuery = dbBaseQuery.Where(x => x.PartnerReservationNumber.ToLower().Contains(request.PartnerReservationNumber.ToLower()));

            return dbBaseQuery;
        }

        private IQueryable<SearchReservationResultDto> IsMigratedFilter(SearchReservationDto request, IQueryable<SearchReservationResultDto> dbBaseQuery)
        {
            if (request.IsMigrated)
                dbBaseQuery = dbBaseQuery.Where(x => x.IsMigrated);

            return dbBaseQuery;
        }

        private IQueryable<SearchReservationResultDto> RoomFilter(SearchReservationDto request, IQueryable<SearchReservationResultDto> dbBaseQuery)
        {
            if (!string.IsNullOrWhiteSpace(request.Room))
                dbBaseQuery = dbBaseQuery.Where(x => !string.IsNullOrWhiteSpace(x.Room) && x.Room.ToLower().Contains(request.Room.ToLower()));

            return dbBaseQuery;
        }

        private IQueryable<SearchReservationResultDto> GroupReservation(SearchReservationDto request, IQueryable<SearchReservationResultDto> dbBaseQuery)
        {
            if (request.IsMain)
                dbBaseQuery = dbBaseQuery.Where(x => x.IsMain);

            return dbBaseQuery;
        }

        private bool SearchReservationHasNext(SearchReservationDto request, int totalItemsDto, List<SearchReservationResultDto> resultDto)
        {
            return totalItemsDto
                   > ((request.Page - 1) * request.PageSize)
                   + resultDto.Count();
        }

        private IQueryable<SearchReservationResultDto> GetReservationDetailBaseQuery(int propertyId, long? ReservationId)
        {
            var dbBaseQuery = (from reserv in this.Context.Reservations
                               join reservItem in this.Context.ReservationItems on reserv.Id equals reservItem.Reservation.Id
                               join receivRoomType in this.Context.RoomTypes on reservItem.ReceivedRoomType.Id equals receivRoomType.Id
                               join requesRoomType in this.Context.RoomTypes on reservItem.RequestedRoomType.Id equals requesRoomType.Id

                               join room in this.Context.Rooms on reservItem.Room.Id equals room.Id into ri
                               from leftRoom in ri.DefaultIfEmpty()

                               join guestReserv in this.Context.GuestReservationItems
                               on reservItem.Id equals guestReserv.ReservationItem.Id

                               join ba in this.Context.BillingAccounts.Where(e => e.Id == e.GroupKey && e.BillingAccountTypeId != (int)BillingAccountTypeEnum.GroupAccount) on guestReserv.Id equals ba.GuestReservationItem.Id into billingAccount
                               from leftBillingAccount in billingAccount.DefaultIfEmpty()

                               join companyclient in this.Context.CompanyClients on reserv.CompanyClientId equals companyclient.Id into cc
                               from leftcompanyclient in cc.DefaultIfEmpty()

                               let totalBudgetQuery = Context.ReservationBudgets.Where(e => e.ReservationItemId == reservItem.Id)
                               let totalBudget = totalBudgetQuery.Sum(e => Convert.ToDouble(e.ManualRate))

                               where reserv.Property.Id == propertyId && reserv.Id == ReservationId
                               select new SearchReservationResultDto
                               {
                                   Id = reserv.Id,
                                   ReservationCode = reserv.ReservationCode,
                                   ReservationItemCode = reservItem.ReservationItemCode,
                                   ReservationItemId = reservItem.Id,
                                   ReservationItemStatusId = reservItem.ReservationItemStatusId,
                                   GuestReservationItemStatusId = guestReserv.GuestStatusId,
                                   ArrivalDate = reservItem.CheckInDate ?? reservItem.EstimatedArrivalDate,
                                   DepartureDate = reservItem.CheckOutDate ?? reservItem.EstimatedDepartureDate,
                                   //EstimatedArrivalDate = reservItem.EstimatedArrivalDate,
                                   //EstimatedDepartureDate = reservItem.EstimatedDepartureDate,
                                   //CheckInDateGuest = guestReserv.CheckInDate,
                                   //CheckOutDate = guestReserv.CheckOutDate,
                                   EstimatedArrivalDateGuest = guestReserv.CheckInDate ?? guestReserv.EstimatedArrivalDate,
                                   EstimatedDepartureDateGuest = guestReserv.CheckOutDate ?? guestReserv.EstimatedDepartureDate,
                                   CancellationDate = reservItem.CancellationDate,
                                   RoomNumber = leftRoom != null ? leftRoom.RoomNumber : string.Empty,
                                   RequestedRoomTypeId = requesRoomType.Id,
                                   RequestedRoomTypeName = requesRoomType.Name,
                                   RequestedRoomTypeAbbreviation = requesRoomType.Abbreviation,
                                   ReceivedRoomTypeId = receivRoomType.Id,
                                   ReceivedRoomTypeName = receivRoomType.Name,
                                   ReceivedRoomTypeAbbreviation = receivRoomType.Abbreviation,
                                   GuestId = guestReserv.GuestId,
                                   GuestIsIncognito = guestReserv.IsIncognito,
                                   GuestName = string.IsNullOrEmpty(guestReserv.GuestName) ? reserv.ContactName : guestReserv.GuestName,
                                   GuestEmail = string.IsNullOrEmpty(guestReserv.GuestEmail) ? reserv.ContactEmail : guestReserv.GuestEmail,
                                   GuestPhoneNumber = reserv.ContactPhone,
                                   GuestLanguage = guestReserv.GuestLanguage,
                                   Client = string.Empty,
                                   GroupName = reserv.GroupName,
                                   CreationTime = reservItem.CreationTime,
                                   ReservationBy = string.Empty,
                                   Deadline = reserv.Deadline,
                                   GuestDocument = guestReserv.GuestDocument,
                                   Total = totalBudget,
                                   TotalARR = CalculateARR(totalBudget, reservItem.CheckInDate ?? reservItem.EstimatedArrivalDate, reservItem.CheckOutDate ?? reservItem.EstimatedDepartureDate),
                                   CurrencySymbol = Context.ReservationBudgets.Where(e => e.ReservationItemId == reservItem.Id).Select(e => e.CurrencySymbol).FirstOrDefault(),
                                   BillingAccountId = leftBillingAccount != null ? leftBillingAccount.Id : Guid.Empty,
                                   GuestReservationItemId = guestReserv.Id,
                                   TenantId = reserv.TenantId,
                                   CompanyClientName = leftcompanyclient != null ? leftcompanyclient.TradeName : string.Empty,
                                   MealPlanTypeId = reservItem.MealPlanTypeId.HasValue ? reservItem.MealPlanTypeId.Value : 0,
                                   MarketSegmentId = reserv.MarketSegmentId.HasValue ? reserv.MarketSegmentId.Value : 0,
                                   BusinessSourceId = reserv.BusinessSourceId.HasValue ? reserv.BusinessSourceId.Value : 0,
                                   RatePlanId = reservItem.RatePlanId.HasValue ? reservItem.RatePlanId.Value : Guid.Empty,

                                   MarketSegmentName = reserv.MarketSegment != null ? reserv.MarketSegment.Name : "",
                                   BusinessSourceName = reserv.BusinessSource != null ? reserv.BusinessSource.Description : "",
                                   RatePlanName = reservItem.RatePlan != null ? reservItem.RatePlan.AgreementName : _localizationManager.GetString(AppConsts.LocalizationSourceName, PropertyBaseRateEnum.Info.PropertyBaseRateName.ToString()),
                                   PartnerReservationNumber = reservItem.PartnerReservationNumber,
                                   AdultsCount = reservItem.AdultCount,
                                   ChildCount = reservItem.ChildCount,
                                   GuestTypeId = guestReserv.GuestTypeId,
                                   GuestTypeName = guestReserv.GuestType.GuestTypeName,
                                   CompanyClientId = leftcompanyclient != null ? leftcompanyclient.Id : Guid.Empty
                               });


            return dbBaseQuery;
        }

        private IQueryable<SearchReservationResultDto> SearchReservationBaseQueryV2(SearchReservationDto request, int propertyId)
        {
            var dbBaseQuery = (from reserv in this.Context.Reservations
                               join reservItem in this.Context.ReservationItems on reserv.Id equals reservItem.Reservation.Id

                               join guestReserv in this.Context.GuestReservationItems
                               on reservItem.Id equals guestReserv.ReservationItem.Id

                               join companyclient in this.Context.CompanyClients on reserv.CompanyClientId equals companyclient.Id into cc
                               from leftcompanyclient in cc.DefaultIfEmpty()

                               where reserv.Property.Id == propertyId
                               select new SearchReservationResultDto
                               {
                                   Id = reserv.Id,
                                   ReservationCode = reserv.ReservationCode,
                                   ReservationItemCode = reservItem.ReservationItemCode,
                                   ReservationItemId = reservItem.Id,
                                   ReservationItemStatusId = reservItem.ReservationItemStatusId,
                                   GuestReservationItemStatusId = guestReserv.GuestStatusId,
                                   ArrivalDate = reservItem.CheckInDate ?? reservItem.EstimatedArrivalDate,
                                   DepartureDate = reservItem.CheckOutDate ?? reservItem.EstimatedDepartureDate,
                                   EstimatedArrivalDateGuest = guestReserv.CheckInDate ?? guestReserv.EstimatedArrivalDate,
                                   EstimatedDepartureDateGuest = guestReserv.CheckOutDate ?? guestReserv.EstimatedDepartureDate,
                                   GuestId = guestReserv.GuestId,
                                   GuestName = string.IsNullOrEmpty(guestReserv.GuestName) ? reserv.ContactName : guestReserv.GuestName,
                                   GroupName = reserv.GroupName,
                                   CreationTime = reservItem.CreationTime,
                                   GuestDocument = guestReserv.GuestDocument,
                                   TenantId = reserv.TenantId,
                                   CompanyClientName = leftcompanyclient != null ? leftcompanyclient.TradeName : string.Empty,
                                   GuestTypeId = guestReserv.GuestTypeId,
                                   GuestTypeName = guestReserv.GuestType.GuestTypeName,
                               });

            dbBaseQuery = IsCheckInToday(request, dbBaseQuery, propertyId);
            dbBaseQuery = IsCheckOutToday(request, dbBaseQuery, propertyId);
            dbBaseQuery = IsDeadline(request, dbBaseQuery);

            #region filters 

            dbBaseQuery = GroupNameFilter(request, dbBaseQuery);
            dbBaseQuery = FreeTextFilter(request, dbBaseQuery);
            dbBaseQuery = ReservationItemStatusFilter(request, dbBaseQuery);
            dbBaseQuery = ArrivalDateFilter(request, dbBaseQuery);
            dbBaseQuery = DepartureDateFilter(request, dbBaseQuery);
            dbBaseQuery = CreationDateFilter(request, dbBaseQuery);
            dbBaseQuery = CancellationDateFilter(request, dbBaseQuery);
            dbBaseQuery = DeadlineDateFilter(request, dbBaseQuery);
            dbBaseQuery = PassingByFilter(request, dbBaseQuery);
            dbBaseQuery = ClientNameFilter(request, dbBaseQuery);
            dbBaseQuery = RoomTypeFilter(request, dbBaseQuery);
            dbBaseQuery = MealPlanTypeFilter(request, dbBaseQuery);
            dbBaseQuery = MarketSegmentFilter(request, dbBaseQuery);
            dbBaseQuery = BusinessSourceFilter(request, dbBaseQuery);
            dbBaseQuery = RatePlanFilter(request, dbBaseQuery);
            dbBaseQuery = AdultsAndChildrenFilter(request, dbBaseQuery);
            dbBaseQuery = CompanyClientFilter(request, dbBaseQuery);
            dbBaseQuery = GuestTypeFilter(request, dbBaseQuery);
            #endregion


            return dbBaseQuery;
        }

        private IQueryable<SearchReservationResultDto> SearchReservationBaseQuery(SearchReservationDto request, int propertyId)
        {
            var dbBaseQuery = (from reserv in this.Context.Reservations
                               join reservItem in this.Context.ReservationItems on reserv.Id equals reservItem.Reservation.Id
                               join receivRoomType in this.Context.RoomTypes on reservItem.ReceivedRoomType.Id equals receivRoomType.Id
                               join requesRoomType in this.Context.RoomTypes on reservItem.RequestedRoomType.Id equals requesRoomType.Id

                               join reservConfirmation in this.Context.ReservationConfirmations on reserv.Id equals reservConfirmation.ReservationId into rc
                               from leftReservConf in rc.DefaultIfEmpty()

                               join paymentType in this.Context.PaymentTypes on leftReservConf.PaymentTypeId equals paymentType.Id into pt
                               from leftpaymentType in pt.DefaultIfEmpty()

                               join room in this.Context.Rooms on reservItem.Room.Id equals room.Id into ri
                               from leftRoom in ri.DefaultIfEmpty()

                               join guestReserv in this.Context.GuestReservationItems
                               on reservItem.Id equals guestReserv.ReservationItem.Id

                               join ba in this.Context.BillingAccounts.Where(e => e.Id == e.GroupKey && e.BillingAccountTypeId != (int)BillingAccountTypeEnum.GroupAccount) on guestReserv.Id equals ba.GuestReservationItem.Id into billingAccount
                               from leftBillingAccount in billingAccount.DefaultIfEmpty()

                               join companyclient in this.Context.CompanyClients on reserv.CompanyClientId equals companyclient.Id into cc
                               from leftcompanyclient in cc.DefaultIfEmpty()

                               let totalBudget = Context.ReservationBudgets.Where(e => e.ReservationItemId == reservItem.Id).Sum(e => Convert.ToDouble(e.ManualRate))

                               where reserv.Property.Id == propertyId
                               select new SearchReservationResultDto
                               {
                                   Id = reserv.Id,
                                   ReservationCode = reserv.ReservationCode,
                                   ReservationItemCode = reservItem.ReservationItemCode,
                                   ReservationItemId = reservItem.Id,
                                   ReservationItemStatusId = reservItem.ReservationItemStatusId,
                                   GuestReservationItemStatusId = guestReserv.GuestStatusId,
                                   ArrivalDate = guestReserv.CheckInDate ?? guestReserv.EstimatedArrivalDate,
                                   DepartureDate = guestReserv.CheckOutDate ?? guestReserv.EstimatedDepartureDate,
                                   //EstimatedArrivalDate = reservItem.EstimatedArrivalDate,
                                   //EstimatedDepartureDate = reservItem.EstimatedDepartureDate,
                                   //CheckInDateGuest = guestReserv.CheckInDate,
                                   //CheckOutDate = guestReserv.CheckOutDate,
                                   EstimatedArrivalDateGuest = guestReserv.CheckInDate ?? guestReserv.EstimatedArrivalDate,
                                   EstimatedDepartureDateGuest = guestReserv.CheckOutDate ?? guestReserv.EstimatedDepartureDate,
                                   CancellationDate = reservItem.CancellationDate,
                                   RoomNumber = leftRoom != null ? leftRoom.RoomNumber : string.Empty,
                                   RequestedRoomTypeId = requesRoomType.Id,
                                   RequestedRoomTypeName = requesRoomType.Name,
                                   RequestedRoomTypeAbbreviation = requesRoomType.Abbreviation,
                                   ReceivedRoomTypeId = receivRoomType.Id,
                                   ReceivedRoomTypeName = receivRoomType.Name,
                                   ReceivedRoomTypeAbbreviation = receivRoomType.Abbreviation,
                                   GuestId = guestReserv.GuestId,
                                   GuestIsIncognito = guestReserv.IsIncognito,
                                   GuestName = string.IsNullOrEmpty(guestReserv.GuestName) ? reserv.ContactName : guestReserv.GuestName,
                                   GuestEmail = string.IsNullOrEmpty(guestReserv.GuestEmail) ? reserv.ContactEmail : guestReserv.GuestEmail,
                                   GuestPhoneNumber = reserv.ContactPhone,

                                   GuestLanguage = guestReserv.GuestLanguage,
                                   Client = string.Empty,
                                   GroupName = reserv.GroupName,
                                   CreationTime = reservItem.CreationTime,
                                   ReservationBy = string.Empty,
                                   Deadline = leftReservConf != null ? leftReservConf.Deadline : null,
                                   GuestDocument = guestReserv.GuestDocument,
                                   Total = totalBudget,
                                   TotalARR = CalculateARR(totalBudget, reservItem.CheckInDate ?? reservItem.EstimatedArrivalDate, reservItem.CheckOutDate ?? reservItem.EstimatedDepartureDate),
                                   CurrencySymbol = Context.ReservationBudgets.Where(e => e.ReservationItemId == reservItem.Id).Select(e => e.CurrencySymbol).FirstOrDefault(),

                                   BillingAccountId = leftBillingAccount != null ? leftBillingAccount.Id : Guid.Empty,
                                   BillingAccountTypeId = leftBillingAccount != null ? leftBillingAccount.BillingAccountTypeId : (int)BillingAccountTypeEnum.Guest,

                                   GuestReservationItemId = guestReserv.Id,
                                   TenantId = reserv.TenantId,
                                   CompanyClientName = leftcompanyclient != null ? leftcompanyclient.TradeName : string.Empty,
                                   MealPlanTypeId = reservItem.MealPlanTypeId.HasValue ? reservItem.MealPlanTypeId.Value : 0,
                                   MarketSegmentId = reserv.MarketSegmentId.HasValue ? reserv.MarketSegmentId.Value : 0,
                                   BusinessSourceId = reserv.BusinessSourceId.HasValue ? reserv.BusinessSourceId.Value : 0,
                                   RatePlanId = reservItem.RatePlanId.HasValue ? reservItem.RatePlanId.Value : Guid.Empty,
                                   PartnerReservationNumber = reservItem.PartnerReservationNumber,
                                   IsMigrated = reservItem.IsMigrated,
                                   Room = leftRoom != null ? leftRoom.RoomNumber : string.Empty,

                                   MarketSegmentName = reserv.MarketSegment != null ? reserv.MarketSegment.Name : "--",
                                   BusinessSourceName = reserv.BusinessSource != null ? reserv.BusinessSource.Description : "--",
                                   RatePlanName = reservItem.RatePlan != null ? reservItem.RatePlan.AgreementName : "--",

                                   AdultsCount = reservItem.AdultCount,
                                   ChildCount = reservItem.ChildCount,
                                   GuestTypeId = guestReserv.GuestTypeId,
                                   GuestTypeName = guestReserv.GuestType.GuestTypeName,
                                   IsMain = guestReserv.IsMain,
                                   CompanyClientId = leftcompanyclient != null ? leftcompanyclient.Id : Guid.Empty,

                                   PaymentTypeId = leftpaymentType != null ? leftpaymentType.Id : 0,
                                   PaymentTypeName = leftpaymentType != null ? leftpaymentType.PaymentTypeName : null,

                                   InternalComments = reserv.InternalComments,
                                   ExternalComments = reserv.ExternalComments,
                                   PartnerComments = reserv.PartnerComments,

                                   RoomId = reservItem.RoomId.GetValueOrDefault()
                               });

            dbBaseQuery = IsCheckInToday(request, dbBaseQuery, propertyId);
            dbBaseQuery = IsCheckOutToday(request, dbBaseQuery, propertyId);
            dbBaseQuery = IsDeadline(request, dbBaseQuery);
            dbBaseQuery = IsPreCheckIn(request, dbBaseQuery);

            #region filters 

            dbBaseQuery = GroupNameFilter(request, dbBaseQuery);
            dbBaseQuery = FreeTextFilter(request, dbBaseQuery);
            dbBaseQuery = ReservationItemStatusFilter(request, dbBaseQuery);
            dbBaseQuery = ArrivalDateFilter(request, dbBaseQuery);
            dbBaseQuery = DepartureDateFilter(request, dbBaseQuery);
            dbBaseQuery = CreationDateFilter(request, dbBaseQuery);
            dbBaseQuery = CancellationDateFilter(request, dbBaseQuery);
            dbBaseQuery = DeadlineDateFilter(request, dbBaseQuery);
            dbBaseQuery = PassingByFilter(request, dbBaseQuery);
            dbBaseQuery = ClientNameFilter(request, dbBaseQuery);
            dbBaseQuery = RoomTypeFilter(request, dbBaseQuery);
            dbBaseQuery = MealPlanTypeFilter(request, dbBaseQuery);
            dbBaseQuery = MarketSegmentFilter(request, dbBaseQuery);
            dbBaseQuery = BusinessSourceFilter(request, dbBaseQuery);
            dbBaseQuery = RatePlanFilter(request, dbBaseQuery);
            dbBaseQuery = AdultsAndChildrenFilter(request, dbBaseQuery);
            dbBaseQuery = CompanyClientFilter(request, dbBaseQuery);
            dbBaseQuery = GuestTypeFilter(request, dbBaseQuery);
            dbBaseQuery = PartnerReservationNumberFilter(request, dbBaseQuery);
            dbBaseQuery = IsMigratedFilter(request, dbBaseQuery);
            dbBaseQuery = RoomFilter(request, dbBaseQuery);
            #endregion


            return dbBaseQuery;
        }

        private double CalculateARR(double totalAmount, DateTime dtStart, DateTime dtEnd)
        {
            var totalDays = (dtEnd.Date - dtStart.Date).TotalDays;
            if (totalDays == 0) totalDays = 1;
            return totalAmount / totalDays;
        }

        private IQueryable<SearchReservationResultDto> SearchReservationPaging(SearchReservationDto request, IQueryable<SearchReservationResultDto> dbBaseQuery)
        {
            var pageSize = request.PageSize.HasValue ? request.PageSize.Value : 1000;
            var page = request.Page.HasValue ? request.Page.Value : 1;
            dbBaseQuery = dbBaseQuery.Skip(pageSize * (page - 1)).Take(pageSize);
            return dbBaseQuery;
        }

        private IQueryable<SearchReservationResultDto> SearchReservationSorting(SearchReservationDto request, IQueryable<SearchReservationResultDto> dbBaseQuery)
        {
            dbBaseQuery = dbBaseQuery.OrderByRequestDto(request);

            return dbBaseQuery;
        }

        private IQueryable<SearchReservationResultDto> IsCheckOutToday(SearchReservationDto request, IQueryable<SearchReservationResultDto> dbBaseQuery, int propertyId)
        {
            var today = _propertyParameterReadRepository.GetSystemDate(propertyId);
            var status = new int[]
                             {
                                 (int)ReservationStatus.Checkin
                             };

            if (request.CheckOutToday.HasValue && request.CheckOutToday.Value)
                dbBaseQuery = dbBaseQuery.Where(exp => exp.EstimatedDepartureDateGuest.Value.Date == today && status.Contains(exp.GuestReservationItemStatusId));

            return dbBaseQuery;
        }

        private IQueryable<SearchReservationResultDto> IsCheckInToday(SearchReservationDto request, IQueryable<SearchReservationResultDto> dbBaseQuery, int propertyId)
        {
            var today = _propertyParameterReadRepository.GetSystemDate(propertyId);
            var status = new int[]
                             {
                                 (int)ReservationStatus.ToConfirm,
                                 (int)ReservationStatus.Confirmed,
                                 (int)ReservationStatus.WaitList
                             };

            if (request.CheckInToday.HasValue && request.CheckInToday.Value)
                dbBaseQuery = dbBaseQuery.Where(exp => exp.EstimatedArrivalDateGuest.Value.Date == today && status.Contains(exp.GuestReservationItemStatusId));

            return dbBaseQuery;
        }

        private IQueryable<SearchReservationResultDto> IsDeadline(SearchReservationDto request, IQueryable<SearchReservationResultDto> dbBaseQuery)
        {
            var today = DateTime.Today.Date;

            if (request.Deadline.HasValue && request.Deadline.Value)
            {
                var validStatus = new int[] { (int)ReservationStatus.ToConfirm, (int)ReservationStatus.Confirmed }.ToList();
                dbBaseQuery = dbBaseQuery.Where(exp => exp.Deadline.HasValue && exp.Deadline.Value < today && validStatus.Contains(exp.ReservationItemStatusId));
            }

            return dbBaseQuery;
        }

        private IQueryable<SearchReservationResultDto> IsPreCheckIn(SearchReservationDto request, IQueryable<SearchReservationResultDto> dbBaseQuery)
        {
            if (request.IsPreCheckIn)
            {
                var reservationStatusIdList = new List<int>() { (int)ReservationStatus.ToConfirm, (int)ReservationStatus.Confirmed };

                if (request.IsGuestVision)                
                    dbBaseQuery = dbBaseQuery.Where(x => reservationStatusIdList.Contains(x.GuestReservationItemStatusId));
                else
                {
                    dbBaseQuery = dbBaseQuery.Where(x => x.IsMain);
                    if (!request.ReservationItemStatus.HasValue)
                        dbBaseQuery = dbBaseQuery.Where(x => reservationStatusIdList.Contains(x.ReservationItemStatusId));
                }
            }

            return dbBaseQuery;
        }

        public IListDto<ReservationDto> GetReservationsByPropertyId(RequestAllDto request, int propertyId)
        {
            var baseQuery = Context.Reservations.Where(p => p.Id == propertyId);

            var reservations = baseQuery
                //.SkipAndTakeByRequestDto(request)
                //.OrderByRequestDto(request)
                .ToListDto<Reservation, ReservationDto>(request);

            return reservations;
        }

        public Reservation GetByReservationItemId(DefaultLongRequestDto request)
        {
            return Context.Reservations

                .Include("ReservationItemList.Room")
                .Include("ReservationItemList.ReceivedRoomType")
                .Include("ReservationItemList.RequestedRoomType")
                .Include("ReservationItemList.GuestReservationItemList.Guest.Person.PersonInformationList.PersonInformationType")
                .Include("ReservationItemList.GuestReservationItemList.Guest.Person.ContactInformationList.ContactInformationType")
                .Include("ReservationItemList.GuestReservationItemList.Guest.Person.LocationList.LocationCategory")
                //.Include("ReservationItemList.GuestReservationItemList.Guest.Person.LocationList.City.ParentSubdivision.ParentSubdivision")
                .FirstOrDefault(exp => exp.ReservationItemList.Any(d => d.Id == request.Id));
        }

        public Reservation GetReservationForCheckin(long id)
        {
            return Context.Reservations
                .Include(c => c.CompanyClient)
                .FirstOrDefault(r => r.Id == id);
        }

        public ReservationHeaderResultDto GetReservationHeaderByReservationItemId(long reservationItemId)
        {
            var dbBaseQuery = (from reserv in Context.Reservations
                               join reservItem in Context.ReservationItems on reserv.Id equals reservItem.Reservation.Id
                               join roomLayout in Context.RoomLayouts on reservItem.RoomLayoutId equals roomLayout.Id
                               join reservationConfirmation in Context.ReservationConfirmations on reserv.Id equals reservationConfirmation.ReservationId
                               join receivRoomType in Context.RoomTypes on reservItem.ReceivedRoomType.Id equals receivRoomType.Id
                               join requesRoomType in Context.RoomTypes on reservItem.RequestedRoomType.Id equals requesRoomType.Id
                               join room in Context.Rooms on reservItem.Room.Id equals room.Id into ri
                               from leftRoom in ri.DefaultIfEmpty()
                               join housekeeping in Context.HousekeepingStatusProperties on leftRoom.HousekeepingStatusPropertyId equals housekeeping.Id into h
                               from housekeeping in h.DefaultIfEmpty()

                               let totalBudgetQuery = Context.ReservationBudgets.Where(e => e.ReservationItemId == reservItem.Id)

                               where reservItem.Id == reservationItemId
                               select new ReservationHeaderResultDto
                               {
                                   Id = reserv.Id,
                                   ReservationCode = reserv.ReservationCode,
                                   ReservationItemId = reservItem.Id,
                                   ReservationItemStatusId = reservItem.ReservationItemStatusId,
                                   ArrivalDate = reservItem.CheckInDate ?? reservItem.EstimatedArrivalDate,
                                   DepartureDate = reservItem.CheckOutDate ?? reservItem.EstimatedDepartureDate,
                                   RoomNumber = leftRoom != null ? leftRoom.RoomNumber : string.Empty,
                                   ReceivedRoomTypeId = receivRoomType.Id,
                                   ReceivedRoomTypeName = receivRoomType.Name,
                                   ReceivedRoomTypeAbbreviation = receivRoomType.Abbreviation,
                                   RequestedRoomTypeId = requesRoomType.Id,
                                   RequestedRoomTypeName = requesRoomType.Name,
                                   RequestedRoomTypeAbbreviation = requesRoomType.Abbreviation,
                                   AdultCount = reservItem.AdultCount,
                                   ChildCount = reservItem.ChildCount,
                                   PaymentTypeId = reservationConfirmation.PaymentTypeId,
                                   RoomLayoutId = roomLayout.Id.ToString(),
                                   QuantityDouble = roomLayout.QuantityDouble,
                                   QuantitySingle = roomLayout.QuantitySingle,
                                   ExternalComments = reserv.ExternalComments,
                                   InternalComments = reserv.InternalComments,
                                   PartnerComments = reserv.PartnerComments,
                                   NumberObservations = NumberObservations(reserv.InternalComments, reserv.ExternalComments, reserv.PartnerComments),
                                   Color = housekeeping != null ? housekeeping.Color : string.Empty,
                                   HousekeepingStatusPropertyId = housekeeping != null ? housekeeping.Id : (Guid?)null,
                                   HousekeepingStatusName = housekeeping != null ? housekeeping.StatusName : null,
                                   ReservationBudget = totalBudgetQuery.Sum(e => e.ManualRate),
                                   ChildrenAge = Context
                                                            .GuestReservationItems
                                                            .Where(e => e.ReservationItemId == reservItem.Id && e.IsChild && e.ChildAge.HasValue)
                                                            .Select(e => Convert.ToInt32(e.ChildAge)).ToList(),
                                   PropertyId = reserv.PropertyId,
                                   CurrencyId = reservItem.CurrencyId,
                                   CurrencySymbol = Context.Currencies.Where(c => c.Id == reservItem.CurrencyId) == null ? null :
                                                    Context.Currencies.Where(c => c.Id == reservItem.CurrencyId).Select(c => c.Symbol).FirstOrDefault(),
                                   CompanyClientId = reserv.CompanyClientId,
                                   RatePlanId = reservItem.RatePlanId,
                                   MealPlanTypeId = reservItem.MealPlanTypeId,
                                   TenantId = reserv.TenantId,
                                   RoomId = reservItem.RoomId
                               });

            var dbBaseQueryFirstResult = dbBaseQuery.FirstOrDefault();

            if (dbBaseQueryFirstResult != null)
            {
                dbBaseQueryFirstResult.ReservationItemStatusName = _localizationManager.GetString(AppConsts.LocalizationSourceName, ((ReservationStatus)dbBaseQueryFirstResult.ReservationItemStatusId).ToString());
                dbBaseQueryFirstResult.PaymentType = _localizationManager.GetString(AppConsts.LocalizationSourceName, ((PaymentTypeEnum)dbBaseQueryFirstResult.PaymentTypeId).ToString());
            }


            return dbBaseQueryFirstResult;
        }

        public int NumberObservations(string internalComments, string externalComments, string partnerComments)
        {
            var commentList = new List<string> { internalComments, externalComments, partnerComments };
            return commentList.Count(c => !string.IsNullOrWhiteSpace(c));
        }

        public IListDto<ReservationCompanyClientResultDto> GetReservationCompanyClient(GetAllReservationCompanyClientDto request, int propertyId)
        {
            var status = new int[] { (int)ReservationStatus.Canceled, (int)ReservationStatus.Checkout, (int)ReservationStatus.WaitList }.ToList();

            var dbBaseQuery = (from r in Context.Reservations
                               join cc in Context.CompanyClients on r.CompanyClientId.Value equals cc.Id
                               join r2 in (from ri in Context.ReservationItems
                                           where !status.Contains(ri.ReservationItemStatusId)
                                           select new
                                           {
                                               ReservationIdAux = ri.ReservationId,
                                               ReservationItemId = ri.Id,
                                               ReservationItemcode = ri.ReservationItemCode
                                           }).Distinct()
                                     on r.Id equals r2.ReservationIdAux

                               join b in Context.BillingAccounts
                                .Where(x => x.Id == x.GroupKey && x.CompanyClientId.HasValue &&
                                        x.BillingAccountTypeId == (int)BillingAccountTypeEnum.Company &&
                                        x.CompanyClientId.HasValue &&
                                        x.ReservationId.HasValue)
                               on new { p1 = cc.Id, p2 = r.Id } equals new { p1 = b.CompanyClientId.Value, p2 = b.ReservationId.Value } into a
                               from leftbillingAccount in a.DefaultIfEmpty()

                               where
                               r.CompanyClientId.HasValue && (string.IsNullOrEmpty(request.SearchData) ||
                                cc.ShortName.ToLower().Contains(request.SearchData.ToLower()) ||
                                cc.TradeName.ToLower().Contains(request.SearchData.ToLower()))
                               select new ReservationCompanyClientResultDto
                               {
                                   ReservationId = r.Id,
                                   ReservationItemId = r2.ReservationItemId,
                                   ReservationCode = r.ReservationCode,
                                   ReservationItemCode = r2.ReservationItemcode,
                                   PersonId = cc.PersonId.ToString(),
                                   CompannyClientId = cc.Id.ToString(),
                                   CompanyClientName = cc.TradeName,
                                   BillingAccountId = leftbillingAccount != null ? leftbillingAccount.Id : Guid.Empty,
                                   TenantId = r.TenantId
                               })
                              ;


            var totalItemsDto = dbBaseQuery.Count();

            //var resultDto = dbBaseQuery.SkipAndTakeByRequestDto(request).OrderByRequestDto(request).ToList();
            var resultDto = dbBaseQuery.ToList();
            var result =
                new ListDto<ReservationCompanyClientResultDto>
                {
                    Items = resultDto,
                    HasNext = totalItemsDto
                                  > ((request.Page - 1) * request
                                         .PageSize)
                                  + resultDto.Count()
                };

            return result;
        }

        public IListDto<ReservationGuestResultDto> GetReservationGuest(GetAllReservationGuestDto request, int propertyId)
        {
            var dbBaseQuery = (from r in Context.Reservations
                               join ri in Context.ReservationItems on r.Id equals ri.ReservationId
                               join gri in Context.GuestReservationItems on ri.Id equals gri.ReservationItemId
                               join g in Context.Guests on gri.GuestId equals g.Id into g
                               from leftGuest in g.DefaultIfEmpty()

                               join b in Context.BillingAccounts.Where(x => x.Id == x.GroupKey && x.GuestReservationItemId.HasValue) on gri.Id equals b.GuestReservationItemId into a
                               from leftbillingAccount in a.DefaultIfEmpty()

                               where (string.IsNullOrEmpty(request.SearchData) ||
                              (ri.ReservationItemCode.ToLower().Contains(request.SearchData.ToLower()) ||
                               gri.GuestName.ToLower().Contains(request.SearchData.ToLower())))
                               select new ReservationGuestResultDto
                               {
                                   ReservationId = r.Id,
                                   ReservationCode = r.ReservationCode,
                                   ReservationItemId = ri.Id,
                                   ReservationItemCode = ri.ReservationItemCode,
                                   ReservationItemStatus = ri.ReservationItemStatusId,
                                   PersonId = leftGuest.PersonId.Value.ToString(),
                                   ArrivalDate = ri.CheckInDate ?? ri.EstimatedArrivalDate,
                                   DepartureDate = ri.CheckOutDate ?? ri.EstimatedDepartureDate,
                                   GuestId = gri.GuestId,
                                   GuestName = gri.GuestName,
                                   GuestReservationItemId = gri.Id,
                                   BillingAccountId = leftbillingAccount != null ? leftbillingAccount.Id : Guid.Empty,
                                   TenantId = r.TenantId,
                               });


            var totalItemsDto = dbBaseQuery.Count();

            //var resultDto = dbBaseQuery.SkipAndTakeByRequestDto(request).OrderByRequestDto(request).ToList();
            var resultDto = dbBaseQuery.ToList();

            foreach (var item in resultDto)
            {
                item.ReservationItemStatusName = _localizationManager.GetString(AppConsts.LocalizationSourceName, ((ReservationStatus)item.ReservationItemStatus).ToString());
            }

            var result =
                new ListDto<ReservationGuestResultDto>
                {
                    Items = resultDto,
                    HasNext = totalItemsDto
                                  > ((request.Page - 1) * request
                                         .PageSize)
                                  + resultDto.Count()
                };

            return result;
        }

        public ReservationHeaderResultDto GetReservationHeader(long reservationId)
        {
            //select 
            //    r.ReservationId,
            //    r.ReservationCode,
            //    MIN(coalesce(ri.CheckInDate, ri.EstimatedArrivalDate)) as arrivalDate,
            //    MAX(coalesce(ri.CheckOutDate, ri.EstimatedDepartureDate)) as departureDate,
            //    SUM(case gri.isChild when 1 then 1 else 0 end) as child,
            //    SUM(case gri.isChild when 0 then 1 else 0 end) as adult,
            //    COUNT(DISTINCT(ri.ReservationItemId)) as acomodacoes,
            //    (select SUM(ManualRate) as ManualRate 
            //        from ReservationBudget rbs 
            //        inner join reservationitem ris on rbs.ReservationItemId =  ris.ReservationItemId
            //        where ris.ReservationId = r.ReservationId
            //        ) as soma

            //from reservation r 
            //inner join reservationItem ri on r.ReservationId = ri.ReservationId
            //inner join guestreservationitem gri on ri.ReservationItemId = gri.ReservationItemId
            //where r.reservationid = 5
            //group by
            //r.ReservationId, ReservationCode


            var dbBaseQuery = (from reserv in Context.Reservations
                               join reservItem in Context.ReservationItems on reserv.Id equals reservItem.Reservation.Id
                               join currency in Context.Currencies on reservItem.CurrencyId equals currency.Id
                               join guestReservItem in Context.GuestReservationItems on reservItem.Id equals guestReservItem.ReservationItemId
                               where reserv.Id == reservationId
                               group new { reserv, reservItem, guestReservItem, currency } by new { reserv.Id, reserv.ReservationCode, reserv.TenantId, currency.Symbol, reservItem.CurrencyId } into gr

                               let totalBudgetQuery = (from reservationBudget in Context.ReservationBudgets
                                                       join reservItemR in Context.ReservationItems on reservationBudget.ReservationItemId equals reservItemR.Id
                                                       where reservItemR.ReservationId == reservationId
                                                       select reservationBudget
                                                                )

                               select new ReservationHeaderResultDto
                               {
                                   Id = gr.Key.Id,
                                   ReservationCode = gr.Key.ReservationCode,
                                   TenantId = gr.Key.TenantId,
                                   ArrivalDate = gr.Min(e => e.reservItem.CheckInDate ?? e.reservItem.EstimatedArrivalDate),
                                   DepartureDate = gr.Max(e => e.reservItem.CheckOutDate ?? e.reservItem.EstimatedDepartureDate),
                                   ChildCount = gr.Sum(e => (e.guestReservItem.IsChild == true ? 1 : 0)),
                                   AdultCount = gr.Sum(e => (e.guestReservItem.IsChild == true ? 0 : 1)),
                                   Accomodations = gr.Select(e => e.reservItem.Id).Distinct().Count(),
                                   ReservationBudget = totalBudgetQuery.Sum(rb => rb.ManualRate),
                                   CurrencyId = gr.Key.CurrencyId,
                                   CurrencySymbol = gr.Key.Symbol
                               })
                              ;

            return dbBaseQuery.FirstOrDefault();
        }

        public Guid? GetCompanyClientIdByReservationId(long reservationId)
        {
            var result = Context.Reservations.FirstOrDefault(e => e.Id == reservationId);

            if (result != null)
                return result.CompanyClientId;

            return null;
        }

        public ReservationDto GetBusinessSourceAndMarketSegmentByReservationId(long id)
            => Context.Reservations.AsNoTracking().Select(e => new ReservationDto
            {
                Id = e.Id,
                BusinessSourceId = e.BusinessSourceId,
                MarketSegmentId = e.MarketSegmentId
            })
                .FirstOrDefault(r => r.Id == id);

        public Reservation GetById(long id)
            => (Context.Reservations
            .Include(r => r.ReservationItemList)
            .FirstOrDefault(r => r.Id == id));
    }
}