﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Thex.Domain.Entities;
using Thex.Dto;
using Thex.Infra.Context;
using Thex.Infra.ReadInterfaces;
using Tnf.Dto;
using Tnf.EntityFrameworkCore;
using Tnf.EntityFrameworkCore.Repositories;

namespace Thex.Infra.Repositories.Read
{
    public class ChannelCodeReadRepository : EfCoreRepositoryBase<ThexContext, ChannelCode>, IChannelCodeReadRepository
    {
        public ChannelCodeReadRepository(IDbContextProvider<ThexContext> dbContextProvider) : base(dbContextProvider)
        {

        }

        public async Task<IListDto<ChannelCodeDto>> GetAllChannelsCodeDtoAsync()
        {
            var channelsCode = await (from channelCode in Context.ChannelCodes.AsNoTracking()
                                      select new ChannelCodeDto
                                      {
                                          ChannelCode1 = channelCode.ChannelCode1,
                                          ChannelCodeDescription = channelCode.ChannelCodeDescription,
                                          Id = channelCode.Id
                                      }).ToListAsync();

            return new ListDto<ChannelCodeDto>
            {
                HasNext = false,
                Items = channelsCode
            };
        }

        public ChannelCodeDto GetDtoById(int id)
        {
            var channelCode = Context.ChannelCodes.SingleOrDefault(x => x.Id == id);
            if (channelCode == null) return null;

            return new ChannelCodeDto
            {
                ChannelCode1 = channelCode.ChannelCode1,
                ChannelCodeDescription = channelCode.ChannelCodeDescription,
                Id = channelCode.Id
            };
        }
    }
}
