﻿// //  <copyright file="MarketSegmentReadRepository.cs" company="TOTVS">
// //  Copyright (c) TOTVS. All rights reserved.
// //  </copyright>

namespace Thex.EntityFrameworkCore.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Thex.Domain.Entities;
    using Thex.Dto;
    using Thex.Infra.ReadInterfaces;
    using Tnf.Dto;
    using Tnf.EntityFrameworkCore.Repositories;
    using Tnf.EntityFrameworkCore;

    using Thex.Infra.Context;
    using Microsoft.EntityFrameworkCore;

    public class MarketSegmentReadRepository : EfCoreRepositoryBase<ThexContext, MarketSegment>, IMarketSegmentReadRepository
    {
        

        public MarketSegmentReadRepository(
            IDbContextProvider<ThexContext> dbContextProvider)
            : base(dbContextProvider)
        {
            
        }

        public MarketSegmentDto GetById(int id)
        {
            return Context.MarketSegments.Where(x => x.Id == id).FirstOrDefault().MapTo<MarketSegmentDto>();
        }

        public IListDto<MarketSegmentDto> GetAllByChainId(GetAllMarketSegmentDto request, int chainId)
        {
            var dbBaseQuery = (from marketSegment in this.Context.MarketSegments
                               join chainMarketSegment in this.Context.ChainMarketSegments on
                               new { c1 = marketSegment.Id, c2 = chainId } equals
                               new { c1 = chainMarketSegment.MarketSegmentId, c2 = chainMarketSegment.ChainId }                             
                               select new MarketSegmentDto
                               {
                                   Id = marketSegment.Id,
                                   Name = marketSegment.Name,
                                   Code = marketSegment.Code,
                                   SegmentAbbreviation = marketSegment.SegmentAbbreviation,
                                   ParentMarketSegmentId = marketSegment.ParentMarketSegmentId,
                                   IsActive = marketSegment.IsActive,
                               });

            if (!request.All)
                dbBaseQuery = dbBaseQuery.Where(x => x.IsActive);

            var parentQuery = dbBaseQuery.Where(e => !e.ParentMarketSegmentId.HasValue);
            var totalItemsDto = parentQuery.Count();
            var resultDto = parentQuery.ToList();

            foreach (var parent in resultDto)
                parent.ChildMarketSegmentList = dbBaseQuery.Where(e => e.ParentMarketSegmentId == parent.Id).ToList();

            var result =
                new ListDto<MarketSegmentDto>
                    {
                        Items = resultDto,
                        HasNext = SearchMarketSegmentClientHasNext(
                            request,
                            totalItemsDto,
                            resultDto),
                    };

            return result;
        }

        private bool SearchMarketSegmentClientHasNext(
            GetAllMarketSegmentDto request,
            int totalItemsDto,
            List<MarketSegmentDto> resultDto)
        {
            return totalItemsDto > ((request.Page - 1) * request.PageSize) + resultDto.Count();
        }

        public bool ValidateMarketSegmentWithReservation(int marketSegmentId, int propertyId)
        {
            var marketSegment = Context.MarketSegments.AsNoTracking().FirstOrDefault(m => m.Id.Equals(marketSegmentId));

            if (!marketSegment.ParentMarketSegmentId.HasValue)
            {
                var marketSegmentList = Context.MarketSegments.AsNoTracking().Where(m => m.ParentMarketSegmentId.Equals(marketSegmentId)).Select(m => m.Id).ToList();

                return Context.Reservations.AsNoTracking().Any(m => marketSegmentList.Contains(m.MarketSegmentId.Value) && m.PropertyId.Equals(propertyId));
            }            
            else
                return Context.Reservations.AsNoTracking().Any(m => m.MarketSegmentId.Equals(marketSegmentId) && m.PropertyId.Equals(propertyId));
        }

        public bool ValidateIfDescriptionAlreadyExists(MarketSegmentDto dto, int chainId)
        {
            var marketSegmentsIds = Context.ChainMarketSegments.Where(c => c.ChainId.Equals(chainId)).Select(c => c.MarketSegmentId);

            return Context.MarketSegments.Any(m => marketSegmentsIds.Contains(m.Id) && m.Name.ToLower().Equals(dto.Name.ToLower()) && !m.Id.Equals(dto.Id));
        }
    }
}