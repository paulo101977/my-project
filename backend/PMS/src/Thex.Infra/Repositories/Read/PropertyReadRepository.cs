﻿//  <copyright file="PropertyReadRepository.cs" company="TOTVS">
//  Copyright (c) TOTVS. All rights reserved.
//  </copyright>
namespace Thex.EntityFrameworkCore.Repositories
{
    using Microsoft.EntityFrameworkCore;
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Text.RegularExpressions;
    using System.Threading.Tasks;
    using Thex.Common;
    using Thex.Common.Enumerations;
    using Thex.Domain.Entities;
    using Thex.Dto;
    using Thex.Infra.Context;
    using Thex.Infra.ReadInterfaces;
    using Thex.Kernel;
    using Tnf.Dto;
    using Tnf.EntityFrameworkCore;
    using Tnf.EntityFrameworkCore.Repositories;

    public class PropertyReadRepository : EfCoreRepositoryBase<ThexContext, Property>, IPropertyReadRepository
    {

        private readonly IPersonReadRepository _personReadRepository;
        private readonly ILocationReadRepository _locationReadRepository;
        protected readonly IDocumentReadRepository _documentReadRepository;
        protected readonly ITenantReadRepository _tenantReadRepository;
        private readonly IApplicationUser _applicationUser;

        public PropertyReadRepository(
            IDbContextProvider<ThexContext> dbContextProvider,
            IPersonReadRepository personReadRepository,
            ILocationReadRepository locationReadRepository,
            IDocumentReadRepository documentReadRepository,
            ITenantReadRepository tenantReadRepository,
            IApplicationUser applicationUser)
            : base(dbContextProvider)
        {

            _personReadRepository = personReadRepository;
            _locationReadRepository = locationReadRepository;
            _documentReadRepository = documentReadRepository;
            _tenantReadRepository = tenantReadRepository;
            _applicationUser = applicationUser;
        }

        public IListDto<PropertyDto> GetAll(IRequestAllDto request, bool central = false)
        {
            var tenantIdList = _tenantReadRepository.GetIdListByLoggedUserId();

            if (!tenantIdList.Any())
                throw new Exception("Tenant Must Have Value");

            var baseQuery = Context.Properties
                .Include(d => d.Brand)
                .ThenInclude(d => d.Chain)
                .FilterByTenant(_tenantReadRepository.GetIdListByLoggedUserId())
                .IgnoreQueryFilters();

            var propertyList = baseQuery.ToListDto<Property, PropertyDto>(request);

            if (central)
            {
                var propertyIds = propertyList.Items.Select(p => p.Id).ToList();
                var locationList = _locationReadRepository.GetLocationDtoListByFilters(propertyIds, CultureInfo.CurrentCulture.Name);
                propertyList.Items = propertyList.Items.Where(p => p.PropertyStatusId == (int)PropertyStatusEnum.Production &&
                                                             (!p.IsBlocked.HasValue || !p.IsBlocked.Value)).ToList();
                propertyList.Items.ToList().ForEach(p => p.LocationList = locationList.Where(l => l.OwnerId == p.PropertyUId).ToList());
            }

            return propertyList;
        }

        public PropertyDto GetByPropertyId(int propertyId)
        {
            var dbBaseQuery = (from property in Context.Properties
                               join brand in Context.Brands on property.BrandId equals brand.Id
                               where property.Id == propertyId
                               select new PropertyDto
                               {
                                   Id = property.Id,
                                   Name = property.Name,
                                   PropertyUId = property.PropertyUId,
                                   CompanyId = property.CompanyId,
                                   BrandId = property.BrandId,
                                   ChainId = brand.ChainId,
                                   PropertyStatusId = property.PropertyStatusId,
                                   PropertyTypeId = PropertyTypeEnum.Hotel,
                                   Photo = property.Photo
                               }).FirstOrDefault();

            dbBaseQuery.LocationList = _locationReadRepository.GetLocationDtoListByPersonId(dbBaseQuery.PropertyUId, CultureInfo.CurrentCulture.Name);

            if (dbBaseQuery.LocationList == null || dbBaseQuery.LocationList.Count == 0)
                dbBaseQuery.LocationList = _locationReadRepository.GetLocationDtoListByPersonId(dbBaseQuery.PropertyUId, AppConsts.DEFAULT_CULTURE_INFO_NAME);

            var personIds = new List<Guid>();
            personIds.Add(dbBaseQuery.PropertyUId);

            var contactInformations = _personReadRepository.GetContactInformationByPersonsIds(personIds);

            if (contactInformations != null && contactInformations.Count > 0)
            {
                var contactPhoneNumber = contactInformations.Where(x => x.ContactInformationTypeId == (int)ContactInformationTypeEnum.PhoneNumber).FirstOrDefault();

                if (contactPhoneNumber != null)
                    dbBaseQuery.ContactPhone = contactPhoneNumber.Information;

                var contactEmail = contactInformations.Where(x => x.ContactInformationTypeId == (int)ContactInformationTypeEnum.Email).FirstOrDefault();

                if (contactEmail != null)
                    dbBaseQuery.ContactEmail = contactEmail.Information;

                var contactWebSite = contactInformations.Where(x => x.ContactInformationTypeId == (int)ContactInformationTypeEnum.Website).FirstOrDefault();

                if (contactWebSite != null)
                    dbBaseQuery.ContactWebSite = contactWebSite.Information;

            }

            dbBaseQuery.TotalOfRooms = GetTotalOfRooms(dbBaseQuery.Id);

            var documentInformations = _documentReadRepository.GetDocumentDtoListByPersonId(Context.Companies.Where(x => x.Id == dbBaseQuery.CompanyId).FirstOrDefault().CompanyUid);

            if (documentInformations != null && documentInformations.Count() > 0)
            {
                var documentCnpj = documentInformations.Where(x => x.DocumentTypeId == (int)DocumentTypeEnum.BrLegalPersonCNPJ).FirstOrDefault();

                if (documentCnpj != null)
                {
                    dbBaseQuery.CNPJ = documentCnpj.DocumentInformation;
                }
                var documentInscEst = documentInformations.Where(x => x.DocumentTypeId == (int)DocumentTypeEnum.BrLegalPersonInscEst).FirstOrDefault();

                if (documentInscEst != null)
                {
                    dbBaseQuery.InscEst = documentInscEst.DocumentInformation;
                }
                var documentInscMun = documentInformations.Where(x => x.DocumentTypeId == (int)DocumentTypeEnum.BrLegalPersonInscMun).FirstOrDefault();

                if (documentInscMun != null)
                {
                    dbBaseQuery.InscMun = documentInscMun.DocumentInformation;
                }


            }

            if (dbBaseQuery.CompanyId > 0)
            {
                var companyInformation = Context.Companies.Where(x => x.Id == dbBaseQuery.CompanyId).FirstOrDefault();
                if (companyInformation != null)
                {
                    dbBaseQuery.ShortName = companyInformation.ShortName;
                    dbBaseQuery.TradeName = companyInformation.TradeName;
                }
            }

            return dbBaseQuery;
        }

        public int GetTotalOfRooms(int propertyId)
        {
            return Context.Rooms.Count(d => d.Property.Id == propertyId);
        }

        public Guid GetTenantByPropertyId(int propertyId)
        {
            var result = Context
                .Properties
                .Where(e => e.Id == propertyId)
                .Select(e => e.TenantId)
                .FirstOrDefault();

            if (result == null)
                return Guid.Empty;

            return result;
        }

        public bool IsValidTenantIdByPropertyId(int propertyId, Guid tenant)
        {
            return Context
                .Properties
                .IgnoreQueryFilters()
                .Any(e => e.Id == propertyId && e.TenantId == tenant);
        }

        public Property GetByPropertyIdWithTenant(int propertyId, bool ignoreQueryFilters = false)
        {
            IQueryable<Property> dbBaseQuery;

            if (!ignoreQueryFilters)
                dbBaseQuery = Context.Properties.IgnoreQueryFilters();
            else
                dbBaseQuery = Context.Properties;

            dbBaseQuery = dbBaseQuery.Include(e => e.Tenant).Where(e => e.Id == propertyId);

            return dbBaseQuery.FirstOrDefault();
        }

        public string GetPropertyDefaultCurrency(int propertyId)
        {
            var propertyParameter = Context.PropertyParameters.FirstOrDefault(x => x.PropertyId == propertyId && x.ApplicationParameterId == (int)ApplicationParameterEnum.ParameterDefaultCurrency);
            if (propertyParameter != null)
                return propertyParameter.PropertyParameterValue;

            return "";
        }

        public CountryLanguageDto GetPropertyCulture(int propertyId)
        {
            var result = (from p in Context.Properties.AsNoTracking().IgnoreQueryFilters()

                          join l in Context.Locations.AsNoTracking().IgnoreQueryFilters()
                          on p.PropertyUId equals l.OwnerId

                          join c in Context.CountrySubdivisions.AsNoTracking().IgnoreQueryFilters()
                          on l.CountryCode equals c.TwoLetterIsoCode

                          join cl in Context.CountryLanguages.AsNoTracking().IgnoreQueryFilters()
                          on c.Id equals cl.CountrySubdivisionId

                          where p.Id == propertyId && c.ParentSubdivisionId == null

                          select cl).FirstOrDefault();

            return result.MapTo<CountryLanguageDto>();
        }

        public string GetPropertyCountryTwoLetterIsoCode(int propertyId)
        {
            var result = (from p in Context.Properties.AsNoTracking().IgnoreQueryFilters()

                          join l in Context.Locations.AsNoTracking().IgnoreQueryFilters()
                          on p.PropertyUId equals l.OwnerId

                          join c in Context.CountrySubdivisions.AsNoTracking().IgnoreQueryFilters()
                          on l.CountryCode equals c.TwoLetterIsoCode

                          join cl in Context.CountryLanguages.AsNoTracking().IgnoreQueryFilters()
                          on c.Id equals cl.CountrySubdivisionId

                          where p.Id == propertyId && c.ParentSubdivisionId == null

                          select c.TwoLetterIsoCode).FirstOrDefault();

            return result;
        }

        public Guid GetPropertyUId(int propertyId)
        {
            return (from p in Context.Properties.AsNoTracking()
                    where p.Id == propertyId
                    select p.PropertyUId).FirstOrDefault();
        }

        public GetPropertySiba GetByPropertyId(List<int> listIds)
        {
            var defaultLanguage = AppConsts.DEFAULT_CULTURE_INFO_NAME.ToLower();

            var baseQuery = (from p in Context.Properties.AsNoTracking()
                             join c in Context.Companies.AsNoTracking() on p.CompanyId equals c.Id
                             join d in Context.Documents.Where(d => listIds.Contains(d.DocumentTypeId)).AsNoTracking() on c.CompanyUid equals d.OwnerId
                             join ipp in Context.IntegrationPartnerProperties.Where(ipp => ipp.PartnerId == 6).AsNoTracking() on p.Id equals ipp.PropertyId
                             join l in Context.Locations.AsNoTracking() on p.PropertyUId equals l.OwnerId
                             join cst in Context.CountrySubdivisionTranslations.Where(cst => cst.LanguageIsoCode.ToLower() == _applicationUser.PropertyLanguage.ToLower() ||
                                cst.LanguageIsoCode.ToLower() == defaultLanguage).OrderBy(t => t.LanguageIsoCode == defaultLanguage).AsNoTracking()
                                      on l.CityId equals cst.CountrySubdivisionId
                             join co in Context.ContactOwners.AsNoTracking() on p.PropertyUId equals co.OwnerId
                             join pe in Context.Persons.AsNoTracking() on co.PersonId equals pe.Id
                             join cit in Context.ContactInformations.Where(cit => cit.ContactInformationTypeId == (int)ContactInformationTypeEnum.PhoneNumber)
                                      on pe.Id equals cit.OwnerId
                             join cie in Context.ContactInformations.Where(cie => cie.ContactInformationTypeId == (int)ContactInformationTypeEnum.Email)
                                      on pe.Id equals cie.OwnerId
                             select new GetPropertySiba()
                             {
                                 Code = long.Parse(Regex.Replace(d.DocumentInformation, "[^0-9]", "")),
                                 IntegrationCode = ipp.IntegrationCode,
                                 Number = ipp.IntegrationNumber != null ? int.Parse(Regex.Replace(ipp.IntegrationNumber, "[^0-9]", "")) : 0,
                                 Name = p.Name,
                                 Address = $"{l.StreetName}, {l.StreetNumber}".Length > 40 ? $"{l.StreetName}, {l.StreetNumber}".Substring(0, 40) : $"{l.StreetName}, {l.StreetNumber}",
                                 Location = cst.Name,
                                 PostalCode = l.PostalCode.Length == 7 ? int.Parse(Regex.Replace(l.PostalCode.Substring(0, 4), "[^0-9]", "")) : 0,
                                 PostalZone = l.PostalCode.Length == 7 ? int.Parse(Regex.Replace(l.PostalCode.Substring(4, 3), "[^0-9]", "")) : 0,
                                 PhoneNumber = long.Parse(Regex.Replace(cit.Information, "[^0-9]", "")),
                                 ContactName = pe.FullName.Length > 40 ? pe.FullName.Substring(0, 40) : pe.FullName,
                                 ContactEmail = cie.Information.Length > 140 ? cie.Information.Substring(0, 140) : cie.Information
                             }).FirstOrDefault();

            baseQuery.NameAbbreviation = baseQuery.Name.Split(' ').Select(x => x.First()).ToArray().JoinAsString("").ToUpper();

            return baseQuery;
        }

        public Guid GetUIdById(int id)
           => (from p in Context.Properties
               where p.Id == id
              select p.PropertyUId).FirstOrDefault();

        public async Task<PropertyInformationForDashboardDto> GetInformationsForDashboardAsync(int propertyId)
        {
            return await Context.Properties.Include(p => p.Brand).ThenInclude(b => b.Chain)
                          .Select(p => new PropertyInformationForDashboardDto
                          {
                              PropertyId = p.Id,
                              PropertyName = p.Name,
                              ChainName = p.Brand.Chain.Name
                          }).FirstOrDefaultAsync(p => p.PropertyId == propertyId);
        }
    }
}
