﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using Thex.Domain.Entities;
using Thex.Dto;
using Thex.Infra.Context;
using Thex.Infra.ReadInterfaces;
using Tnf.Dto;
using Tnf.EntityFrameworkCore;
using Tnf.EntityFrameworkCore.Repositories;

namespace Thex.EntityFrameworkCore.Repositories
{
    public class BillingInvoicePropertySupportedTypeReadRepository : EfCoreRepositoryBase<ThexContext, BillingAccountItem>, IBillingInvoicePropertySupportedTypeReadRepository
    {


        public BillingInvoicePropertySupportedTypeReadRepository(IDbContextProvider<ThexContext> dbContextProvider)
            : base(dbContextProvider)
        {

        }

        public List<BillingInvoicePropertySupportedType> GetByType(int invoiceType, int propertyId)
        {
            return Context
                    .BillingInvoicePropertySupportedTypes
                    .Include(e => e.BillingInvoiceProperty)
                    .AsNoTracking()
                    .Where(e => e.PropertyId == propertyId &&
                                e.BillingInvoiceProperty.BillingInvoiceModelId == invoiceType &&
                                e.IsDeleted == false)
                    .ToList();
        }

        public List<BillingInvoicePropertySupportedType> GetAllByPropertyId(int propertyId)
        {
            return Context
                    .BillingInvoicePropertySupportedTypes
                    .Include(e => e.BillingInvoiceProperty)
                    .AsNoTracking()
                    .Where(e => e.PropertyId == propertyId)
                    .ToList();
        }

        public IListDto<BillingInvoicePropertySupportedTypeDto> VerifyExistingBillingPropertyWithCodeService(int propertyId, Guid billingInvoicePropertyId, List<Guid> countrySubdvisionServiceIdList, List<int> billingItemIdList)
        {
            var dbBaseQuery = Context.BillingInvoicePropertySupportedTypes.
                                                Where(x => x.PropertyId == propertyId &&
                                                            x.BillingInvoicePropertyId != billingInvoicePropertyId &&
                                                            countrySubdvisionServiceIdList.Contains(x.CountrySubdvisionServiceId.Value) &&
                                                            billingItemIdList.Contains(x.BillingItemId));

            var request = new RequestAllDto();

            var result = dbBaseQuery
           //.SkipAndTakeByRequestDto(request)
           //.OrderByRequestDto(request)
           .ToListDto<BillingInvoicePropertySupportedType, BillingInvoicePropertySupportedTypeDto>(request);

            return result;
        }

        public IListDto<BillingInvoicePropertySupportedTypeDto> GetBillingPropertyListWithoutCodeService(int propertyId, Guid billingInvoicePropertyId, List<int> billingItemIdList)
        {
            var dbBaseQuery = Context.BillingInvoicePropertySupportedTypes.
                                                Where(x => x.PropertyId == propertyId &&
                                                            x.BillingInvoicePropertyId != billingInvoicePropertyId &&
                                                            billingItemIdList.Contains(x.BillingItemId));

            var request = new RequestAllDto();

            var result = dbBaseQuery
           .ToListDto<BillingInvoicePropertySupportedType, BillingInvoicePropertySupportedTypeDto>(request);

            return result;
        }

        public IListDto<BillingInvoicePropertySupportedTypeDto> GetBillingPropertyListWithoutCodeService(Guid billingInvoicePropertyId, Guid childBillingInvoicePropertyId, List<int> billingItemIdList)
        {
            var dbBaseQuery = Context.BillingInvoicePropertySupportedTypes.
                                                Where(x => x.BillingInvoicePropertyId != billingInvoicePropertyId &&
                                                            x.BillingInvoicePropertyId != childBillingInvoicePropertyId &&
                                                            billingItemIdList.Contains(x.BillingItemId));

            var request = new RequestAllDto();

            var result = dbBaseQuery
           .ToListDto<BillingInvoicePropertySupportedType, BillingInvoicePropertySupportedTypeDto>(request);

            return result;
        }

    }
}
