﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Thex.Domain.Entities;
using Thex.Dto;
using Thex.Infra.ReadInterfaces;

using Tnf.Dto;
using Tnf.EntityFrameworkCore.Repositories;
using Tnf.EntityFrameworkCore;
using Thex.Infra.Context;

namespace Thex.Infra.Repositories.Read
{
    public class PolicyTypesReadRepository : EfCoreRepositoryBase<ThexContext, PolicyType>, IPolicyTypesReadRepository
    {
        public PolicyTypesReadRepository(IDbContextProvider<ThexContext> dbContextProvider) : base(dbContextProvider)
        {
        }

        public IListDto<PolicyTypeDto> GetAllPolicyTypes()
        {
            var dbBaseQuery = Context.PolicyTypes.ToList();


            var policyTypeList = new List<PolicyTypeDto>();

            foreach (var item in dbBaseQuery.ToList())
                policyTypeList.Add(item.MapTo<PolicyTypeDto>());


            return new ListDto<PolicyTypeDto>
            {
                HasNext = false,
                Items = policyTypeList,
            };
        }
    }
}
