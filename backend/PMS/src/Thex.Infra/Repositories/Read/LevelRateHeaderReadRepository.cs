﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Thex.Common;
using Thex.Common.Enumerations;
using Thex.Domain.Entities;
using Thex.Dto;
using Thex.Infra.Context;
using Thex.Infra.ReadInterfaces;
using Tnf.EntityFrameworkCore;
using Tnf.EntityFrameworkCore.Repositories;
using Tnf.Localization;

namespace Thex.Infra.Repositories.Read
{
    public class LevelRateHeaderReadRepository : EfCoreRepositoryBase<ThexContext, LevelRateHeader>, ILevelRateHeaderReadRepository
    {
        private readonly ILocalizationManager _localizationManager;

        public LevelRateHeaderReadRepository(
            IDbContextProvider<ThexContext> dbContextProvider,
            ILocalizationManager localizationManager)
            : base(dbContextProvider)
        {
            _localizationManager = localizationManager;
        }

        public ICollection<LevelRateHeaderDto> GetAllDto(Guid tenantId)
        {
            var headers = Context.LevelRateHeaders.Include(lrh => lrh.Level)
                                            .Include(lrh => lrh.Currency)
                                            .Where(lrh => lrh.TenantId == tenantId)
                                            .Select(lrh => new LevelRateHeaderDto
                                            {
                                                Id = lrh.Id,
                                                RateName = lrh.RateName,
                                                InitialDate = lrh.InitialDate,
                                                EndDate = lrh.EndDate,
                                                LevelId = lrh.LevelId,
                                                LevelName = lrh.Level.Description,
                                                IsActive = lrh.IsActive,
                                                CurrencyId = lrh.CurrencyId,
                                                CurrencyName = lrh.Currency.CurrencyName,
                                                CurrencySymbol = lrh.Currency.Symbol,
                                                LevelOrder = lrh.Level.Order
                                            }).Distinct().OrderBy(lrh => lrh.LevelOrder).ThenBy(lrh => lrh.EndDate).ToList();

            return headers;
        }

        public LevelRateResultDto GetDtoById(Guid levelRateHeaderId)
        {
            var levelRateHeader = Context.LevelRateHeaders
                .Include(lrh => lrh.LevelRateList)
                .Include(lrh => lrh.Level)
                .Include(lrh => lrh.Currency)
                .FirstOrDefault(lrh => lrh.Id == levelRateHeaderId);
            var levelRateHeaderDto = new LevelRateHeaderDto
            {
                Id = levelRateHeader.Id,
                LevelId = levelRateHeader.LevelId,
                LevelName = levelRateHeader.Level.Description,
                EndDate = levelRateHeader.EndDate,
                InitialDate = levelRateHeader.InitialDate,
                IsActive = levelRateHeader.IsActive,
                RateName = levelRateHeader.RateName,
                PropertyMealPlanTypeRate = levelRateHeader.PropertyMealPlanTypeRate,
                CurrencyId = levelRateHeader.CurrencyId,
                CurrencySymbol = levelRateHeader.Currency.Symbol
            };

            var levelRateListDto = levelRateHeader.LevelRateList.GroupBy(lr => new
            {
                lr.CurrencyId,
                lr.CurrencySymbol,
                LevelRateHeaderId = lr.LevelRateHeader.Id,
                lr.Pax1Amount,
                lr.Pax2Amount,
                lr.Pax3Amount,
                lr.Pax4Amount,
                lr.Pax5Amount,
                lr.Pax6Amount,
                lr.Pax7Amount,
                lr.Pax8Amount,
                lr.Pax9Amount,
                lr.Pax10Amount,
                lr.PaxAdditionalAmount,
                lr.Child1Amount,
                lr.Child2Amount,
                lr.Child3Amount,
                lr.RoomTypeId
            }).Select(glr => new LevelRateDto
            {
                CurrencyId = glr.Key.CurrencyId,
                CurrencySymbol = glr.Key.CurrencySymbol,
                LevelRateHeaderId = glr.Key.LevelRateHeaderId,
                Pax1Amount = glr.Key.Pax1Amount,
                Pax2Amount = glr.Key.Pax2Amount,
                Pax3Amount = glr.Key.Pax3Amount,
                Pax4Amount = glr.Key.Pax4Amount,
                Pax5Amount = glr.Key.Pax5Amount,
                Pax6Amount = glr.Key.Pax6Amount,
                Pax7Amount = glr.Key.Pax7Amount,
                Pax8Amount = glr.Key.Pax8Amount,
                Pax9Amount = glr.Key.Pax9Amount,
                Pax10Amount = glr.Key.Pax10Amount,
                PaxAdditionalAmount = glr.Key.PaxAdditionalAmount,
                Child1Amount = glr.Key.Child1Amount,
                Child2Amount = glr.Key.Child2Amount,
                Child3Amount = glr.Key.Child3Amount,
                RoomTypeId = glr.Key.RoomTypeId
            }).ToList();

            var levelRateMealPlanTypeDtoList = levelRateHeader.LevelRateList.GroupBy(lr => new
            {
                IsMealPlanTypeDefault = lr.MealPlanTypeId == lr.MealPlanTypeDefault,
                lr.MealPlanTypeId,
                lr.AdultMealPlanAmount,
                lr.Child1MealPlanAmount,
                lr.Child2MealPlanAmount,
                lr.Child3MealPlanAmount,
                MealPlanTypeName = _localizationManager.GetString(AppConsts.LocalizationSourceName, ((MealPlanTypeEnum)lr.MealPlanTypeId).ToString())

            }).Select(glr => new LevelRateMealPlanTypeDto
            {
                IsMealPlanTypeDefault = glr.Key.IsMealPlanTypeDefault,
                MealPlanTypeId = glr.Key.MealPlanTypeId,
                AdultMealPlanAmount = glr.Key.AdultMealPlanAmount,
                Child1MealPlanAmount = glr.Key.Child1MealPlanAmount,
                Child2MealPlanAmount = glr.Key.Child2MealPlanAmount,
                Child3MealPlanAmount = glr.Key.Child3MealPlanAmount,
                MealPlanTypeName = _localizationManager.GetString(AppConsts.LocalizationSourceName, ((MealPlanTypeEnum)glr.Key.MealPlanTypeId).ToString())
            }).ToList();

            return new LevelRateResultDto
            {
                LevelRateHeader = levelRateHeaderDto,
                LevelRateList = levelRateListDto,
                LevelRateMealPlanTypeList = levelRateMealPlanTypeDtoList
            };
        }

        public ICollection<LevelDto> GetAllLevelsAvailable(LevelRateRequestDto request, int propertyId)
        {
            var levelIdUnavailableList = Context.LevelRateHeaders.Where(lrh => lrh.InitialDate.Date <= request.EndDate.Date
                                                              && lrh.EndDate.Date >= request.InitialDate.Date && lrh.IsActive
                                                              && lrh.CurrencyId == request.CurrencyId)
                                                              .Select(lrh => lrh.LevelId);

            return Context.Levels.Where(l => !levelIdUnavailableList.Contains(l.Id) && l.IsActive && l.PropertyId == propertyId)
                                 .OrderBy(l => l.Order)
                                 .Select(l => new LevelDto
                                 {
                                     Id = l.Id,
                                     Description = l.Description,
                                     LevelCode = l.LevelCode,
                                     IsActive = l.IsActive
                                 }).ToList();

        }

        public bool LevelIsAvailable(DateTime initialDate, DateTime endDate, Guid currencyId, Guid levelId, Guid levelHeaderId, int propertyId)
         => !Context.LevelRateHeaders.Include(lrh => lrh.Level).Any(lrh => lrh.InitialDate.Date <= endDate.Date
                                                              && lrh.EndDate.Date >= endDate.Date && lrh.IsActive
                                                              && lrh.CurrencyId == currencyId
                                                              && lrh.Level.PropertyId == propertyId
                                                              && lrh.Level.IsActive
                                                              && lrh.LevelId == levelId
                                                              && lrh.Id != levelHeaderId);

        public LevelRateHeader GetById(Guid id)
            => Context.LevelRateHeaders.FirstOrDefault(lrh => lrh.Id == id);

        public bool LevelRateAlreadyUsed(Guid levelRateHeaderId)
            => Context.PropertyBaseRateHistories.Any(pbr => pbr.LevelRateId == levelRateHeaderId);

        public ICollection<PropertyBaseRateByLevelRateResultDto> GetAllDtoForPropertyBaseRate(DateTime initialDate, DateTime endDate, Guid? currencyId)
        {
            var levelRateHeaderList = Context.LevelRateHeaders.Include(lrh => lrh.LevelRateList)
                                                              .Include(lrh => lrh.Currency)
                                                              .Include(lrh => lrh.Level)
                                                              .Where(lrh => lrh.InitialDate.Date <= endDate.Date
                                                                           && lrh.EndDate.Date >= initialDate.Date
                                                                           && lrh.IsActive)
                                                              .OrderBy(lrh => lrh.Level.Order)
                                                              .ToList();

            if (currencyId.HasValue)
                levelRateHeaderList = levelRateHeaderList.Where(lrh => lrh.CurrencyId == currencyId.Value).ToList();

            var propertyBaseRateByLevelRateList = new List<PropertyBaseRateByLevelRateResultDto>();

            foreach (var level in levelRateHeaderList.Select(x => x.Level).Distinct())
            {
                propertyBaseRateByLevelRateList.Add(new PropertyBaseRateByLevelRateResultDto
                {
                    LevelId = level.Id,
                    LevelName = level.Description,
                    LevelRateCurrencyList = GetLevelRateCurrencyDtoList(levelRateHeaderList, level.Id, initialDate, endDate)
                });
            }

            return propertyBaseRateByLevelRateList.Where(pbr => pbr.LevelRateCurrencyList.Any()).ToList();
        }

        public ICollection<LevelRateHeader> GetAllByLevelRateHeaderIdList(ICollection<Guid> levelRateHeaderIdList)
            => Context.LevelRateHeaders.Include(lrh => lrh.LevelRateList).Where(lrh => levelRateHeaderIdList.Contains(lrh.Id)).ToList();


        private ICollection<LevelRateCurrencyDto> GetLevelRateCurrencyDtoList(ICollection<LevelRateHeader> levelRateHeaderList, Guid levelId, DateTime initialDate, DateTime endDate)
        {
            var levelRateCurrencyList = new List<LevelRateCurrencyDto>();
            foreach (var currency in levelRateHeaderList.Where(lrh => lrh.LevelId == levelId).GroupBy(lrh => new { lrh.CurrencyId, lrh.Currency.CurrencyName }).Select(glrh => glrh.Key))
            {
                var levelRateList = GetLevelRateResultDtoList(levelRateHeaderList, currency.CurrencyId, levelId);
                levelRateCurrencyList.Add(new LevelRateCurrencyDto
                {
                    CurrencyId = currency.CurrencyId,
                    CurrencyName = currency.CurrencyName,
                    LevelRateList = levelRateList,
                    DateListOutsideRateList = GenerateDateListOutsideRatePeriod(levelRateList, initialDate, endDate)
                });
            }

            return levelRateCurrencyList.Where(lrc => !lrc.DateListOutsideRateList.Any()).ToList();
        }

        private ICollection<LevelRateResultDto> GetLevelRateResultDtoList(ICollection<LevelRateHeader> levelRateHeaderList, Guid currencyId, Guid levelId)
        {
            var levelRateHeaderDistinctList = levelRateHeaderList.Where(lrhl => lrhl.CurrencyId == currencyId && lrhl.LevelId == levelId).Distinct();
            var levelRateResultList = new List<LevelRateResultDto>();

            foreach (var levelRateHeader in levelRateHeaderDistinctList)
            {
                levelRateResultList.Add(new LevelRateResultDto
                {
                    LevelRateHeader = GetLevelRateHeaderDto(levelRateHeader),
                    LevelRateList = GetLevelRateDtoList(levelRateHeader.LevelRateList),
                    LevelRateMealPlanTypeList = GetLevelRateMealPlanTypeDtoList(levelRateHeader.LevelRateList)
                });
            }

            return levelRateResultList;
        }

        private LevelRateHeaderDto GetLevelRateHeaderDto(LevelRateHeader levelRateHeader)
        {
            return new LevelRateHeaderDto
            {
                Id = levelRateHeader.Id,
                LevelId = levelRateHeader.LevelId,
                LevelName = levelRateHeader.Level.Description,
                EndDate = levelRateHeader.EndDate,
                InitialDate = levelRateHeader.InitialDate,
                IsActive = levelRateHeader.IsActive,
                RateName = levelRateHeader.RateName,
                PropertyMealPlanTypeRate = levelRateHeader.PropertyMealPlanTypeRate
            };
        }

        private ICollection<LevelRateDto> GetLevelRateDtoList(ICollection<LevelRate> levelRateList)
        {
            return levelRateList.GroupBy(lr => new
            {
                lr.CurrencyId,
                lr.CurrencySymbol,
                LevelRateHeaderId = lr.LevelRateHeader.Id,
                lr.Pax1Amount,
                lr.Pax2Amount,
                lr.Pax3Amount,
                lr.Pax4Amount,
                lr.Pax5Amount,
                lr.Pax6Amount,
                lr.Pax7Amount,
                lr.Pax8Amount,
                lr.Pax9Amount,
                lr.Pax10Amount,
                lr.Child1Amount,
                lr.Child2Amount,
                lr.Child3Amount,
                lr.RoomTypeId
            }).Select(glr => new LevelRateDto
            {
                CurrencyId = glr.Key.CurrencyId,
                CurrencySymbol = glr.Key.CurrencySymbol,
                LevelRateHeaderId = glr.Key.LevelRateHeaderId,
                Pax1Amount = glr.Key.Pax1Amount,
                Pax2Amount = glr.Key.Pax2Amount,
                Pax3Amount = glr.Key.Pax3Amount,
                Pax4Amount = glr.Key.Pax4Amount,
                Pax5Amount = glr.Key.Pax5Amount,
                Pax6Amount = glr.Key.Pax6Amount,
                Pax7Amount = glr.Key.Pax7Amount,
                Pax8Amount = glr.Key.Pax8Amount,
                Pax9Amount = glr.Key.Pax9Amount,
                Pax10Amount = glr.Key.Pax10Amount,
                Child1Amount = glr.Key.Child1Amount,
                Child2Amount = glr.Key.Child2Amount,
                Child3Amount = glr.Key.Child3Amount,
                RoomTypeId = glr.Key.RoomTypeId
            }).ToList();
        }

        private ICollection<LevelRateMealPlanTypeDto> GetLevelRateMealPlanTypeDtoList(ICollection<LevelRate> levelRateList)
        {
            return levelRateList.GroupBy(lr => new
            {
                IsMealPlanTypeDefault = lr.MealPlanTypeId == lr.MealPlanTypeDefault,
                lr.MealPlanTypeId,
                lr.AdultMealPlanAmount,
                lr.Child1MealPlanAmount,
                lr.Child2MealPlanAmount,
                lr.Child3MealPlanAmount,
                MealPlanTypeName = _localizationManager.GetString(AppConsts.LocalizationSourceName, ((MealPlanTypeEnum)lr.MealPlanTypeId).ToString())

            }).Select(glr => new LevelRateMealPlanTypeDto
            {
                IsMealPlanTypeDefault = glr.Key.IsMealPlanTypeDefault,
                MealPlanTypeId = glr.Key.MealPlanTypeId,
                AdultMealPlanAmount = glr.Key.AdultMealPlanAmount,
                Child1MealPlanAmount = glr.Key.Child1MealPlanAmount,
                Child2MealPlanAmount = glr.Key.Child2MealPlanAmount,
                Child3MealPlanAmount = glr.Key.Child3MealPlanAmount,
                MealPlanTypeName = _localizationManager.GetString(AppConsts.LocalizationSourceName, ((MealPlanTypeEnum)glr.Key.MealPlanTypeId).ToString())
            }).ToList();
        }

        private IEnumerable<DateTime> GenerateRangeDate(DateTime initialDate, DateTime endDate)
        {
            var dateList = new List<DateTime>();

            while (initialDate <= endDate)
            {
                dateList.Add(initialDate.Date);
                initialDate = initialDate.AddDays(1);
            }

            return dateList.AsEnumerable();
        }

        private List<DateTime> GenerateDateListOutsideRatePeriod(ICollection<LevelRateResultDto> levelRateList, DateTime initialDate, DateTime endDate)
        {
            var levelRatePeriod = Enumerable.Empty<DateTime>();
            var periodDateList = GenerateRangeDate(initialDate, endDate);
            foreach (var levelRate in levelRateList)
                levelRatePeriod = levelRatePeriod.Concat(GenerateRangeDate(levelRate.LevelRateHeader.InitialDate, levelRate.LevelRateHeader.EndDate));

            return periodDateList.Where(p => !levelRatePeriod.Contains(p.Date)).ToList();
        }

    }
}
