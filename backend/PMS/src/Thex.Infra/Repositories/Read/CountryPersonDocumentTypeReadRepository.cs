//  <copyright file="CountryPersonDocumentTypeReadRepository.cs" company="TOTVS">
//  Copyright (c) TOTVS. All rights reserved.
//  </copyright>
namespace Thex.EntityFrameworkCore.Repositories
{

    using Thex.Domain.Entities;
    using Thex.Infra.Context;
    using Thex.Infra.ReadInterfaces;
    using Tnf.EntityFrameworkCore;
    using Tnf.EntityFrameworkCore.Repositories;

    public class CountryPersonDocumentTypeReadRepository : EfCoreRepositoryBase<ThexContext, CountryPersonDocumentType>, ICountryPersonDocumentTypeReadRepository
    {
        public CountryPersonDocumentTypeReadRepository(IDbContextProvider<ThexContext> dbContextProvider)
            : base(dbContextProvider)
        {
        }
    }
}