﻿using System;
using System.Collections.Generic;
using Thex.Domain.Entities;
using Thex.Dto;
using Thex.Infra.ReadInterfaces;
using Tnf.Dto;
using Tnf.EntityFrameworkCore.Repositories;
using Tnf.EntityFrameworkCore;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Thex.Domain.JoinMap;
using System.Threading.Tasks;
using Thex.Infra.Context;
using Thex.Common.Enumerations;
using System.Data.SqlTypes;

namespace Thex.Infra.Repositories.Read
{
    public class RatePlanReadRepository : EfCoreRepositoryBase<ThexContext, RatePlan>, IRatePlanReadRepository
    {
        private readonly ICompanyClientReadRepository _companyClientReadRepository;
        private readonly ICompanyReadRepository _companyReadRepository;
        private readonly IPropertyBaseRateHeaderHistoryReadRepository _propertyBaseRateHeaderHistoryReadRepository;
        private readonly IPropertyBaseRateHistoryReadRepository _propertyBaseRateHistoryReadRepository;

        public RatePlanReadRepository(
            IDbContextProvider<ThexContext> dbContextProvider,
            ICompanyClientReadRepository companyClientReadRepository,
            ICompanyReadRepository companyReadRepository,
            IPropertyBaseRateHeaderHistoryReadRepository propertyBaseRateHeaderHistoryReadRepository,
            IPropertyBaseRateHistoryReadRepository propertyBaseRateHistoryReadRepository
            ) : base(dbContextProvider)
        {
            _companyClientReadRepository = companyClientReadRepository;
            _companyReadRepository = companyReadRepository;
            _propertyBaseRateHeaderHistoryReadRepository = propertyBaseRateHeaderHistoryReadRepository;
            _propertyBaseRateHistoryReadRepository = propertyBaseRateHistoryReadRepository;
        }

        public RatePlanDto RatePlanDtoRatePlanId(Guid ratePlanId)
        {
            var dbBaseQuery = (from ratePlan in Context.RatePlans

                               join agreementType in Context.AgreementTypes on ratePlan.AgreementTypeId equals agreementType.Id

                               join mealPlanType in Context.MealPlanTypes on ratePlan.MealPlanTypeId equals mealPlanType.Id

                               join propertyMealPlanType in Context.PropertyMealPlanTypes on mealPlanType.Id equals propertyMealPlanType.MealPlanTypeId into a
                               from leftPropertyMealPlanType in a.DefaultIfEmpty()

                               where ratePlan.Id == ratePlanId
                               select new RatePlanDto
                               {
                                   Id = ratePlan.Id,
                                   AgreementName = ratePlan.AgreementName,
                                   AgreementTypeName = agreementType.AgreementTypeName,
                                   AgreementTypeId = ratePlan.AgreementTypeId,
                                   StartDate = ratePlan.StartDate,
                                   EndDate = ratePlan.EndDate == (DateTime)SqlDateTime.MaxValue ? null : ratePlan.EndDate,
                                   CurrencyId = ratePlan.CurrencyId,
                                   CurrencySymbol = ratePlan.CurrencySymbol,
                                   MealPlanTypeId = leftPropertyMealPlanType != null ? leftPropertyMealPlanType.MealPlanTypeId : mealPlanType.Id,
                                   PropertyMealPlanTypeName = leftPropertyMealPlanType != null ? leftPropertyMealPlanType.PropertyMealPlanTypeName : mealPlanType.MealPlanTypeName,
                                   IsActive = ratePlan.IsActive,                                   
                                   PropertyId = ratePlan.PropertyId,
                                   RateNet = ratePlan.RateNet,                                  
                                   RateTypeId = ratePlan.RateTypeId,
                                   DistributionCode = ratePlan.DistributionCode,
                                   MarketSegmentId = ratePlan.MarketSegmentId ?? default(int),
                                   PublishOnline = ratePlan.PublishOnline,
                                   Description = ratePlan.Description,
                                   RoundingTypeId = ratePlan.RoundingTypeId
                               }).FirstOrDefault();

            dbBaseQuery.RatePlanRoomTypeList = GetAllRatePlanRoomTypeByRatePlanId(ratePlanId).ToList();
            dbBaseQuery.RatePlanCompanyClientList = GetAllRatePlanCompanyClientByRatePlanId(dbBaseQuery.Id, dbBaseQuery.PropertyId).ToList();
            dbBaseQuery.RatePlanCommissionList = GetAllRatePlanComissionByRatePlanId(ratePlanId).ToList();
            dbBaseQuery.PropertyBaseRateHeaderHistoryList = _propertyBaseRateHeaderHistoryReadRepository.GetAllPropertyBaseRateHeaderHistoryByRatePlanId(dbBaseQuery.Id).ToList();

            return dbBaseQuery;
        }

        public RatePlan GetRatePlanById(Guid ratePlanId)
        {
            return Context.RatePlans

                .Where(x => x.Id == ratePlanId)
                .Include(x => x.RatePlanRoomTypeList)
                .Include(x => x.RatePlanCompanyClientList)
                .Include(x => x.RatePlanCommissionList)
                .FirstOrDefault();
        }


        public ListDto<RatePlanDto> GetAllRatePlanSearch(GetAllRatePlanDto request, int propertyId)
        {
            IQueryable<RatePlanDto> dbBaseQuery = null;

            if (request.CompanyClientId.HasValue &&
                request.CurrencyId.HasValue &&
                request.RoomTypeId.HasValue)
                dbBaseQuery = GetDbBaseQueryCompleteSearch(request, propertyId);
            else
                dbBaseQuery = GetDbBaseQuerySimpleSearch(propertyId);

            if (!String.IsNullOrEmpty(request.SearchData))
            {
                var isDate = DateTime.TryParse(request.SearchData, out var date);
                if (isDate)
                    dbBaseQuery = dbBaseQuery.Where(x => DateTime.Compare(x.StartDate.Date, date.Date) == 0 || DateTime.Compare(x.EndDate.Value.Date, date.Date) == 0);
                else
                    dbBaseQuery = dbBaseQuery.Where(x => x.AgreementName.ToLower().Contains(request.SearchData.ToLower()));

            }

            var result = dbBaseQuery.ToList();
            int totalItemsDto = result.Count;

            return new ListDto<RatePlanDto>
            {
                HasNext = SearchRatePlanHasNext(request, totalItemsDto, result),
                Items = result,

            };
        }

        private IQueryable<RatePlanDto> GetDbBaseQuerySimpleSearch(int propertyId)
        {
            return (from ratePlan in Context
                    .RatePlans

                    where ratePlan.PropertyId == propertyId && !ratePlan.IsDeleted
                    select new RatePlanDto()
                    {
                        Id = ratePlan.Id,
                        AgreementName = ratePlan.AgreementName,
                        StartDate = ratePlan.StartDate,
                        EndDate = ratePlan.EndDate == (DateTime)SqlDateTime.MaxValue ? null : ratePlan.EndDate,
                        IsActive = ratePlan.IsActive
                    });
        }

        private IQueryable<RatePlanDto> GetDbBaseQueryCompleteSearch(GetAllRatePlanDto request, int propertyId)
        {
            return (from ratePlan in Context.RatePlans
                    join ratePlanCompanyClient in Context.RatePlanCompanyClients on ratePlan.Id equals ratePlanCompanyClient.RatePlanId
                    join ratePlanRoomType in Context.RatePlanRoomTypes on ratePlan.Id equals ratePlanRoomType.RatePlanId
                    join currency in Context.Currencies on ratePlan.CurrencyId equals currency.Id

                    where ratePlan.PropertyId == propertyId &&
                         !ratePlan.IsDeleted &&
                         ratePlan.IsActive.HasValue &&
                         ratePlan.IsActive.Value &&
                         ratePlan.CurrencyId == request.CurrencyId &&
                         ratePlanRoomType.RoomTypeId == request.RoomTypeId &&
                         !ratePlanRoomType.IsDeleted &&
                         !ratePlanCompanyClient.IsDeleted &&
                         ratePlanCompanyClient.CompanyClientId == request.CompanyClientId
                    select new RatePlanDto()
                    {
                        Id = ratePlan.Id,
                        AgreementName = ratePlan.AgreementName,
                        StartDate = ratePlan.StartDate,
                        EndDate = ratePlan.EndDate,
                        IsActive = ratePlan.IsActive
                    });
        }

        private bool SearchRatePlanHasNext(GetAllRatePlanDto request, int totalItemsDto, List<RatePlanDto> resultDto)
        {
            return totalItemsDto > ((request.Page - 1) * request.PageSize) + resultDto.Count();
        }

        private IQueryable<RatePlanCommissionDto> GetAllRatePlanComissionByRatePlanId(Guid ratePlanId)
        {
            var dbBaseQuery = (from commission in Context.RatePlanCommissions
                               join billingItem in Context.BillingItems on commission.BillingItemId equals billingItem.Id
                               where commission.RatePlanId == ratePlanId && !commission.IsDeleted
                               select new RatePlanCommissionDto()
                               {
                                   Id = commission.Id,
                                   BillingItemId = commission.BillingItemId,
                                   BillingItemName = billingItem.BillingItemName,
                                   CommissionPercentualAmount = commission.CommissionPercentualAmount,
                                   RatePlanId = commission.RatePlanId
                               });

            return dbBaseQuery;

        }

        private IQueryable<RatePlanRoomTypeDto> GetAllRatePlanRoomTypeByRatePlanId(Guid ratePlanId)
        {
            var dbBaseQuery = (from ratePlanRoomType in Context.RatePlanRoomTypes

                               join roomType in Context.RoomTypes on ratePlanRoomType.RoomTypeId equals roomType.Id

                               where ratePlanRoomType.RatePlanId == ratePlanId && !ratePlanRoomType.IsDeleted
                               select new RatePlanRoomTypeDto()
                               {
                                   Id = ratePlanRoomType.Id,
                                   RatePlanId = ratePlanRoomType.RatePlanId,
                                   IsDecreased = ratePlanRoomType.IsDecreased,
                                   PercentualAmmount = ratePlanRoomType.PercentualAmmount,
                                   //PublishOnline = ratePlanRoomType.PublishOnline,
                                   RoomTypeId = ratePlanRoomType.RoomTypeId,
                                   RoomTypeName = roomType.Name,
                                   Abbreviation = roomType.Abbreviation
                               });


            return dbBaseQuery;
        }


        private bool SearchCompanyClientHasNext(GetAllCompanyClientDto request, int totalItemsDto, List<RatePlanCompanyClientDto> resultDto)
        {
            return totalItemsDto > ((request.Page - 1) * request.PageSize) + resultDto.Count();
        }

        private IQueryable<RatePlanCompanyClientDto> GetAllRatePlanCompanyClientByRatePlanId(Guid ratePlanId, int propertyId)
        {
            var ratePlanCompanyClients = Context.RatePlanCompanyClients.AsNoTracking().Where(x => x.RatePlanId == ratePlanId && !x.IsDeleted);

            int companyId = _companyReadRepository.GetCompanyIdByPropertyId(propertyId);

            var request = new GetAllCompanyClientDto()
            {
                PropertyId = propertyId
            };

            var companyClientsByPropertyId = _companyClientReadRepository.GetAllByCompanyId(request, companyId);

            var companyClients = companyClientsByPropertyId.Items.Distinct();

            var dbBaseQuery = (from companyClient in companyClients
                               join ratePlanCompanyClient in ratePlanCompanyClients on companyClient.Id equals ratePlanCompanyClient.CompanyClientId
                               select new RatePlanCompanyClientDto()
                               {
                                   Id = ratePlanCompanyClient.Id,
                                   CompanyClientId = ratePlanCompanyClient.CompanyClientId,
                                   RatePlanId = ratePlanCompanyClient.RatePlanId,
                                   ClientCategoryId = companyClient.ClientCategoryId,
                                   ClientCategoryName = companyClient.ClientCategoryName,
                                   Document = companyClient.Document,
                                   Name = companyClient.Name,
                                   Phone = companyClient.Phone
                               }).ToList();


            var companyClientWithLocation = Context.CompanyClients.
                  Include(x => x.Person)
                  .ThenInclude(x => x.LocationList)
                  .Include(x => x.PropertyCompanyClientCategory)
                  .Where(x => ratePlanCompanyClients.Select(y => y.CompanyClientId).Contains(x.Id));

            foreach (var companyClient in dbBaseQuery)
            {
                var location = companyClientWithLocation.Where(x => x.Id == companyClient.CompanyClientId).FirstOrDefault().Person.LocationList.FirstOrDefault();
                companyClient.Address = location.StreetName + " " + location.StreetNumber + " " + location.Neighborhood;
            }

            return dbBaseQuery.AsQueryable();

        }


        public bool VerifyNameAgreementSamePeriod(DateTime startDate, DateTime endDate, string agreementName, Guid ratePlanId, int propertyId)
        {
            return Context.RatePlans
                .Any(x => x.AgreementName.ToLower() == agreementName.ToLower() && startDate.Date <= x.EndDate.Value.Date && endDate.Date >= x.StartDate.Date && x.Id != ratePlanId && x.PropertyId == propertyId);
        }


        public async Task<List<RatePlanBaseJoinMap>> GetAllCompleteRatePlan(int propertyId, DateTime initialDate, DateTime finalDate, Guid? companyClientId, List<int?> roomTypeId)
        {
            var dbBaseQuery = (
                                from ratePlan in Context.RatePlans.AsNoTracking()

                                join ratePlanRoomType in Context.RatePlanRoomTypes.AsNoTracking() on ratePlan.Id equals ratePlanRoomType.RatePlanId into rp
                                from leftRatePlanRoomType in rp.DefaultIfEmpty()

                                join roomType in Context.RoomTypes.AsNoTracking() on leftRatePlanRoomType.RoomTypeId equals roomType.Id into rt
                                from leftRoomType in rt.DefaultIfEmpty()

                                join currency in Context.Currencies.AsNoTracking() on ratePlan.CurrencyId equals currency.Id

                                join c in Context.RatePlanCompanyClients.AsNoTracking() on ratePlan.Id equals c.RatePlanId into ratePlanCompanyClient
                                from leftRatePlanCompanyClient in ratePlanCompanyClient.DefaultIfEmpty()

                                join cc in Context.CompanyClients.AsNoTracking() on leftRatePlanCompanyClient.CompanyClientId equals cc.Id into companyClient
                                from leftCompanyClient in companyClient.DefaultIfEmpty()

                                where ratePlan.PropertyId == propertyId &&
                                ratePlan.IsActive.HasValue && ratePlan.IsActive.Value &&
                                ratePlan.StartDate.Date <= initialDate.Date &&
                               (ratePlan.EndDate ?? (DateTime)SqlDateTime.MaxValue).Date >= finalDate.Date
                                select new RatePlanBaseJoinMap
                                {
                                    RatePlan = ratePlan,
                                    RatePlanRoomType = leftRatePlanRoomType,
                                    RoomType = leftRoomType,
                                    Currency = currency,
                                    RatePlanCompanyClient = leftRatePlanCompanyClient,
                                    CompanyClient = leftCompanyClient
                                });

            if (companyClientId.HasValue)
                dbBaseQuery = dbBaseQuery.Where(e => (e.CompanyClient != null && e.CompanyClient.Id == companyClientId.Value) || (e.RatePlan.AgreementTypeId == (int)AgreementTypeEnum.DirectSelling));
            else
                dbBaseQuery = dbBaseQuery.Where(e => e.RatePlan.AgreementTypeId == (int)AgreementTypeEnum.DirectSelling);

            if (roomTypeId.Any(x => x.HasValue))
                dbBaseQuery = dbBaseQuery.Where(e =>
                    (e.RoomType != null && roomTypeId.Contains(e.RoomType.Id) && e.RatePlan.RateTypeId == (int)RatePlanTypeEnum.BaseRate) ||
                    (e.RatePlan.PropertyBaseRateList.Any(f => roomTypeId.Contains(f.RoomType.Id)) && e.RatePlan.RateTypeId == (int)RatePlanTypeEnum.FlatRate));

            return await dbBaseQuery.OrderBy(x=>x.RoomType.Id).ToListAsync();

        }

        public RatePlan GetByBillingAccountId(Guid billingAccountId)
        {
            return (from b in Context.BillingAccounts
                    join ri in Context.ReservationItems on b.ReservationItemId equals ri.Id
                    join rp in Context.RatePlans on ri.RatePlanId equals rp.Id
                    where b.Id == billingAccountId
                    select rp)
                                    .FirstOrDefault();
        }

        public async Task<IListDto<PropertyBaseRateHeaderHistoryDto>> GetRatePlanHistoryById(Guid ratePlanId)
        {
            var dbBaseQuery = await Context.PropertyBaseRateHeaderHistories
                                .Where(x => x.RatePlanId == ratePlanId)
                                .OrderByDescending(x => x.CreationTime).AsTask();

            var PropertyBaseRateHeaderHistoryDtoList = new List<PropertyBaseRateHeaderHistoryDto>();

            var t = dbBaseQuery.ToList();

            var idList = t.Select(e => e.Id).ToList();

            var resultItems = _propertyBaseRateHistoryReadRepository.GetAllDtoByHistoryId(idList).ToList();

            foreach (var item in dbBaseQuery.ToList())
            {
                var v = item.MapTo<PropertyBaseRateHeaderHistoryDto>();

                v.PropertyBaseRateHistoryList = resultItems.Where(e => e.PropertyBaseRateHeaderHistoryId == item.Id).ToList();

                PropertyBaseRateHeaderHistoryDtoList.Add(v);
            }

            return new ListDto<PropertyBaseRateHeaderHistoryDto>
            {
                HasNext = true,
                Items = PropertyBaseRateHeaderHistoryDtoList
            };
        }

        public async Task<List<PropertyBaseRateHeaderHistory>> GetRatePlanHistoryByDate(Guid ratePlanId, DateTime dateTimeHistory)
        {
            var dbBaseQuery = Context.PropertyBaseRateHeaderHistories
                                .AsNoTracking()
                                .Where(x => x.RatePlanId == ratePlanId
                                  && (dateTimeHistory >= x.InitialDate && dateTimeHistory <= x.FinalDate))
                                .OrderByDescending(x => x.CreationTime);

            var PropertyBaseRateHeaderHistoryDtoList = new List<PropertyBaseRateHeaderHistoryDto>();

            foreach (var item in dbBaseQuery.ToList())
            {
                item.PropertyBaseRateHistoryList = _propertyBaseRateHistoryReadRepository.GetAllByHistoryId(item.Id).Result;
            }

            return await dbBaseQuery.ToListAsync();
        }

        public bool IsBaseRateType(Guid ratePlanId)
        {
            return Context.RatePlans.AsNoTracking().Any(e => e.Id == ratePlanId && e.RateTypeId == (int)RatePlanTypeEnum.BaseRate);
        }

        public async Task<IListDto<PropertyBaseRateHeaderHistoryDto>> GetRatePlanHistoryByHistoryId(Guid Id)
        {
            var dbBaseQuery = await Context.PropertyBaseRateHeaderHistories
                                .Where(x => x.Id == Id)
                                .OrderByDescending(x => x.CreationTime).AsTask();
            var PropertyBaseRateHeaderHistoryDtoList = new List<PropertyBaseRateHeaderHistoryDto>();
            var t = dbBaseQuery.ToList();
            var idList = t.Select(e => e.Id).ToList();
            var resultItems = _propertyBaseRateHistoryReadRepository.GetAllDtoByHistoryId(idList).ToList();
            foreach (var item in dbBaseQuery.ToList())
            {
                var v = item.MapTo<PropertyBaseRateHeaderHistoryDto>();
                v.PropertyBaseRateHistoryList = resultItems.Where(e => e.PropertyBaseRateHeaderHistoryId == item.Id).ToList();
                PropertyBaseRateHeaderHistoryDtoList.Add(v);
            }
            return new ListDto<PropertyBaseRateHeaderHistoryDto>
            {
                HasNext = true,
                Items = PropertyBaseRateHeaderHistoryDtoList
            };
        }

        public async Task<List<RatePlanDto>> GetAllBaseRateByPeriodAndPropertyId(DateTime startDate, DateTime endDate, int propertyId)
        {
            return await (from ratePlan in Context.RatePlans
                                     where (
                                            (ratePlan.StartDate.Date >= startDate.Date && ratePlan.StartDate.Date <= endDate.Date) ||
                                            (ratePlan.EndDate.Value.Date >= startDate.Date && ratePlan.EndDate.Value.Date <= endDate.Date)
                                           ) &&
                                           ratePlan.PropertyId.Equals(propertyId) &&
                                           ratePlan.RateTypeId.Equals((int)RatePlanTypeEnum.BaseRate) &&
                                           ratePlan.IsActive.Value
                                                select new RatePlanDto()
                                                {
                                                    Id = ratePlan.Id,
                                                    AgreementTypeId  = ratePlan.AgreementTypeId,
                                                    AgreementName = ratePlan.AgreementName,
                                                    StartDate = ratePlan.StartDate,
                                                    EndDate = ratePlan.EndDate,
                                                    PropertyId = ratePlan.PropertyId,
                                                    MealPlanTypeId = ratePlan.MealPlanTypeId,                                                  
                                                    IsActive = ratePlan.IsActive,
                                                    CurrencyId = ratePlan.CurrencyId,
                                                    RateNet = ratePlan.RateNet,
                                                    RateTypeId = ratePlan.RateTypeId,
                                                    MarketSegmentId = ratePlan.MarketSegmentId ?? default(int)
                                                }).ToListAsync();
        }

        public bool DistributionCodeAvailable(string distributionCode, int propertyId, Guid ratePlanId)
        {
            return !Context.RatePlans.Any(rp => rp.DistributionCode.ToLower().Equals(distributionCode.ToLower())
                                                           && rp.PropertyId == propertyId && rp.Id != ratePlanId);
        }

        public bool IsBillingAccountDoesNotHaveRatePlanCompanyClient(List<Guid> billingAccountIds)
            => Context.BillingAccounts.Include(ba => ba.ReservationItem).ThenInclude(ri => ri.RatePlan).ThenInclude(rp => rp.RatePlanCompanyClientList)
                                   .Any(ba => billingAccountIds.Contains(ba.Id) && !ba.ReservationItem.RatePlan.RatePlanCompanyClientList
                                              .Any(x => x.CompanyClientId == ba.CompanyClientId));

        public List<RatePlanBillingAccountDto> GetAllByBillingAccountIdList(List<Guid> billingAccountIdList)
        {
            return (from b in Context.BillingAccounts
                    join ri in Context.ReservationItems on b.ReservationItemId equals ri.Id
                    join rp in Context.RatePlans on ri.RatePlanId equals rp.Id
                    where billingAccountIdList.Contains(b.Id)
                    select new RatePlanBillingAccountDto
                    {
                        BillingAccountId = b.Id,
                        RatePlanId = rp.Id
                    }).ToList();
        }
    }
}
