﻿using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;
using Thex.Domain.Entities;
using Thex.Dto;
using Thex.Infra.Context;
using Thex.Infra.ReadInterfaces;
using Tnf.EntityFrameworkCore;
using Tnf.EntityFrameworkCore.Repositories;

namespace Thex.Infra.Repositories.Read
{
    public class PropertyContractReadRepository : EfCoreRepositoryBase<ThexContext, PropertyContract>, IPropertyContractReadRepository
    {
        public PropertyContractReadRepository(
            IDbContextProvider<ThexContext> dbContextProvider)
            : base(dbContextProvider)
        {
        }

        public PropertyContractDto GetPropertyContractByPropertyId(int propertyId)
        {
            return (from propertyContract in Context.PropertyContracts.AsNoTracking()                  
                    where propertyContract.PropertyId == propertyId
                    select new PropertyContractDto
                    {
                        Id = propertyContract.Id,
                        PropertyId = propertyContract.PropertyId,
                        MaxUh = propertyContract.MaxUh
                    }).FirstOrDefault();
        }

        public async Task<int?> GetTotalRoomsByPropertyIdAsync(int propertyId)
            => (await Context.PropertyContracts.FirstOrDefaultAsync(pc => pc.PropertyId == propertyId))?.MaxUh;
    }
}
