﻿using System;
using System.Collections.Generic;
using System.Linq;
using Thex.Domain.Entities;
using Thex.Infra.ReadInterfaces;
using Tnf.EntityFrameworkCore.Repositories;
using Tnf.EntityFrameworkCore;
using Thex.Infra.Context;

namespace Thex.Infra.Repositories.Read
{
    public class ContactInformationReadRepository : EfCoreRepositoryBase<ThexContext, ContactInformation>, IContactInformationReadRepository
    {

        public ContactInformationReadRepository(IDbContextProvider<ThexContext> dbContextProvider)
           : base(dbContextProvider)
        {
        }

        public List<ContactInformation> GetAllByOwnerId(Guid ownerId)
        {
            return Context.ContactInformations.Where(x => x.OwnerId == ownerId).ToList();
        }
    }
}
