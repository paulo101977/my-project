﻿using System;
using System.Linq;
using Thex.Domain.Entities;
using Thex.Infra.ReadInterfaces;
using Tnf.EntityFrameworkCore.Repositories;
using Tnf.EntityFrameworkCore;
using Thex.Infra.Context;

namespace Thex.Infra.Repositories.Read
{
    public class PropertyAuditProcessStepReadRepository: EfCoreRepositoryBase<ThexContext, PropertyAuditProcessStep>, IPropertyAuditProcessStepReadRepository
    {
        

        public PropertyAuditProcessStepReadRepository(IDbContextProvider<ThexContext> dbContextProvider)
            : base(dbContextProvider)
        {
            
        }

        public PropertyAuditProcessStep GetPropertyAuditProcessStepById(Guid id)
        {
            return Context.PropertyAuditProcessSteps.Where(p => p.Id == id).FirstOrDefault();
        }

        public PropertyAuditProcessStep GetPropertyAuditProcessStepByPropertyAuditProcessId(Guid propertyAuditProcessId)
        {
            return Context.PropertyAuditProcessSteps.Where(p => p.PropertyAuditProcessId == propertyAuditProcessId).FirstOrDefault();
        }

        public void SaveChanges()
        {
            Context.SaveChanges();
        }
    }
}
