﻿//  <copyright file="ReservationBudgetReadRepository.cs" company="TOTVS">
//  Copyright (c) TOTVS. All rights reserved.
//  </copyright>
namespace Thex.EntityFrameworkCore.Repositories
{
    using Thex.Domain.Entities;
    using Thex.Infra.ReadInterfaces;
    using System.Linq;

    using Microsoft.EntityFrameworkCore;

    using Tnf.EntityFrameworkCore.Repositories;
    using Tnf.EntityFrameworkCore;
    using System.Collections.Generic;
    using Thex.Infra.Context;
    using System;

    public class ReservationBudgetReadRepository : EfCoreRepositoryBase<ThexContext, ReservationBudget>, IReservationBudgetReadRepository, Domain.Interfaces.Repositories.Read.IReservationBudgetReadRepository
    {
        

        public ReservationBudgetReadRepository(
            IDbContextProvider<ThexContext> dbContextProvider)
            : base(dbContextProvider)
        {
            
        }

        public List<ReservationBudget> GetAllByReservationItemId(long reservationItemId)
        {
            return Context.ReservationBudgets.AsNoTracking().Where(e => e.ReservationItemId == reservationItemId).ToList();
        }

        public List<ReservationBudget> GetAllByReservationId(long reservationId)
        {
            var reservationItemIdList = Context.ReservationItems.Where(e => e.ReservationId == reservationId).Select(e => e.Id).ToList();

            if(reservationItemIdList.Count() > 0)
            {
                return Context.ReservationBudgets.AsNoTracking().Where(e => reservationItemIdList.Contains(e.ReservationItemId)).ToList();
            }

            return null;
        }

        public List<long> GetAllIdByFilters(long reservationItemId, DateTime endDate)
         => (from rb in Context.ReservationBudgets
            where rb.ReservationItemId == reservationItemId && rb.BudgetDay.Date < endDate
            select rb.Id).ToList();
    }
}