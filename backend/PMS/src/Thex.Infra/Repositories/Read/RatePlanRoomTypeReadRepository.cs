﻿using System;
using Thex.Domain.Entities;
using Thex.Dto;
using Thex.Infra.ReadInterfaces;
using Tnf.EntityFrameworkCore.Repositories;
using Tnf.EntityFrameworkCore;
using System.Linq;
using Microsoft.EntityFrameworkCore;

using System.Threading.Tasks;
using Thex.Infra.Context;
using Thex.Dto.RatePlan;

namespace Thex.Infra.Repositories.Read
{
    public class RatePlanRoomTypeReadRepository : EfCoreRepositoryBase<ThexContext, RatePlanRoomType>, IRatePlanRoomTypeReadRepository
    {
        

        public RatePlanRoomTypeReadRepository(
            IDbContextProvider<ThexContext> dbContextProvider) : base(dbContextProvider)
        {
            
        }

        public async Task<RatePlanWithRatePlanRoomTypeDto> GetByRatePlanIdAndRoomTypeId(int roomTypeId, Guid ratePlanId)
        {
            return await (from r in Context
                                          .RatePlanRoomTypes
                                          join rp in Context.RatePlans on r.RatePlanId equals rp.Id
                                        where r.RoomTypeId == roomTypeId && 
                                            r.RatePlanId == ratePlanId && 
                                            !r.IsDeleted &&
                                            rp.IsActive.HasValue && 
                                            rp.IsActive.Value
                                        select new RatePlanWithRatePlanRoomTypeDto
                                        {
                                            Id = r.Id,
                                            IsDecreased = r.IsDecreased,
                                            PercentualAmmount = r.PercentualAmmount,
                                            RatePlanId = r.RatePlanId,
                                            RateTypeId = rp.RateTypeId,
                                            RoomTypeId = r.RoomTypeId,
                                            RoundingTypeId = rp.RoundingTypeId
                                        }).FirstOrDefaultAsync();
        }
    }
}
