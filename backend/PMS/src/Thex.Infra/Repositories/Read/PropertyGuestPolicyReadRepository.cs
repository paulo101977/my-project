﻿using System;
using System.Collections.Generic;
using System.Linq;
using Thex.Domain.Entities;
using Thex.Dto;
using Thex.Infra.Context;
using Thex.Infra.ReadInterfaces;
using Tnf.EntityFrameworkCore;
using Tnf.EntityFrameworkCore.Repositories;

namespace Thex.Infra.Repositories.Read
{
    public class PropertyGuestPolicyReadRepository : EfCoreRepositoryBase<ThexContext, PropertyPolicy>, IPropertyGuestPolicyReadRepository
    {
        public PropertyGuestPolicyReadRepository(
            IDbContextProvider<ThexContext> dbContextProvider) : base(dbContextProvider)
        {

        }

        public new PropertyGuestPolicyDto GetById(Guid id)
        => (from propertyPolicy in Context.PropertyPolicies.Where(x => x.Id == id && x.PolicyTypeId == (int)Common.Enumerations.PolicyType.GuestPolicies)
            join country in Context.CountrySubdivisions on propertyPolicy.CountryId equals country.Id
            select new PropertyGuestPolicyDto()
            {
                Id = propertyPolicy.Id,
                CountryId = propertyPolicy.CountryId,
                CountryCode = country.TwoLetterIsoCode,
                Description = propertyPolicy.Description,
                IsActive = propertyPolicy.IsActive
            }).FirstOrDefault();

        public List<PropertyGuestPolicyDto> GetAll()
            => (from propertyPolicy in Context.PropertyPolicies.Where(x => x.PolicyTypeId == (int)Common.Enumerations.PolicyType.GuestPolicies)
                join country in Context.CountrySubdivisions on propertyPolicy.CountryId equals country.Id
                select new PropertyGuestPolicyDto()
                {
                    Id = propertyPolicy.Id,
                    CountryId = propertyPolicy.CountryId,
                    CountryCode = country.TwoLetterIsoCode,
                    Description = propertyPolicy.Description,
                    IsActive = propertyPolicy.IsActive
                }).ToList();
    }
}
