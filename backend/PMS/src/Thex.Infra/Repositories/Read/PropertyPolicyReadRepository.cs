﻿using System;
using System.Linq;
using Thex.Domain.Entities;
using Thex.Dto;
using Thex.Infra.ReadInterfaces;

using Tnf.Dto;
using Tnf.EntityFrameworkCore.Repositories;
using Tnf.EntityFrameworkCore;
using Thex.Infra.Context;

namespace Thex.Infra.Repositories.Read
{
    public class PropertyPolicyReadRepository : EfCoreRepositoryBase<ThexContext, PropertyPolicy>, IPropertyPolicyReadRepository
    {
        

        public PropertyPolicyReadRepository(
            IDbContextProvider<ThexContext> dbContextProvider) : base(dbContextProvider)
        {
            
        }

        public IListDto<PropertyPolicyDto> GetAllPropertyPolicyByPropertyId(int propertyId)
        {
            var dbBaseQuery = (from p in Context.PropertyPolicies
                               join t in Context.PolicyTypes on p.PolicyTypeId equals t.Id
                               where p.PropertyId == propertyId && !p.IsDeleted
                               select new PropertyPolicyDto()
                               {
                                   Id = p.Id,
                                   PolicyTypeId = t.Id,
                                   PolicyTypeName = t.PolicyTypeName,
                                   Description = p.Description,
                                   IsActive = p.IsActive,
                                   PropertyId = p.PropertyId
                               });

            var propertyPolicyList = dbBaseQuery.ToList();

            return new ListDto<PropertyPolicyDto>
            {
                HasNext = false,
                Items = propertyPolicyList,
                
            };
        }

        public PropertyPolicyDto GetPropertyPolicy(Guid propertyPolicyId)
        {

            var dbBaseQuery = (from p in Context.PropertyPolicies
                               join t in Context.PolicyTypes on p.PolicyTypeId equals t.Id
                               where p.Id == propertyPolicyId && !p.IsDeleted
                               select new PropertyPolicyDto()
                               {
                                   Id = p.Id,
                                   PolicyTypeId = t.Id,
                                   PolicyTypeName = t.PolicyTypeName,
                                   Description = p.Description,
                                   IsActive = p.IsActive,
                                   PropertyId = p.PropertyId

                               }).FirstOrDefault();

            return dbBaseQuery;

        }
    }
}
