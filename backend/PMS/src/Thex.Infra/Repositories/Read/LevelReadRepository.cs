﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Thex.Domain.Entities;
using Thex.Dto;
using Thex.Infra.Context;
using Thex.Infra.ReadInterfaces;
using Tnf.Dto;
using Tnf.EntityFrameworkCore;
using Tnf.EntityFrameworkCore.Repositories;

namespace Thex.Infra.Repositories.Read
{
    public class LevelReadRepository : EfCoreRepositoryBase<ThexContext, Level>, ILevelReadRepository
    {
        public LevelReadRepository(IDbContextProvider<ThexContext> dbContextProvider) : base(dbContextProvider)
        {

        }

        public Level GetById(int propertyId, Guid id)
        {
            var level = base.Get(new DefaultGuidRequestDto(id));
            return level;
        }



        public LevelDto GetDtoById(int propertyId, Guid id)
        {
            var level = Context.Levels.FirstOrDefault(x => x.PropertyId == propertyId && x.Id == id);
            if (level == null) return null;

            return new LevelDto
            {
                Id = level.Id,
                LevelCode = level.LevelCode,
                Order = level.Order,
                PropertyId = level.PropertyId,
                Description = level.Description,
                IsActive = level.IsActive
            };
        }

        public LevelDto GetDtoByLevelCode(int propertyId, int levelCode)
        {
            var level = Context.Levels.FirstOrDefault(x => x.PropertyId == propertyId && x.LevelCode == levelCode);
            if (level == null) return null;

            return new LevelDto
            {
                Id = level.Id,
                LevelCode = level.LevelCode,
                Order = level.Order,
                PropertyId = level.PropertyId,
                Description = level.Description,
                IsActive = level.IsActive
            };
        }


        public List<Level> GetAll(int propertyId)
                    => Context.Levels.Where(x => x.PropertyId == propertyId).ToList();



        public IListDto<LevelDto> GetAllDto(int propertyId, GetAllLevelDto requestDto)
        {
            var list = (from level in Context.Levels.AsNoTracking()
                        where level.PropertyId == propertyId
                        select new LevelDto
                        {
                            Id = level.Id,
                            LevelCode = level.LevelCode,
                            Order = level.Order,
                            PropertyId = level.PropertyId,
                            Description = level.Description,
                            IsActive = level.IsActive

                        });

            if (requestDto != null)
            {
                if (!requestDto.All)
                    list = list.Where(x => x.IsActive);
            }

            return new ListDto<LevelDto>
            {
                HasNext = false,
                Items = list.OrderBy(x => x.Order).ToList()
            };
        }


        public async Task<List<Level>> GetAllChannelsByTenantIdAsync(Guid tenantId)
        {
            var dbBaseQuery = await Context.Levels
                    .Where(r => r.TenantId == tenantId)
                    .OrderBy(r => r.LevelCode)
                    .ToListAsync();

            return dbBaseQuery;

        }

        public bool LevelAlreadyUsed(Guid levelId)
            => Context.LevelRateHeaders.Any(lr => lr.LevelId == levelId);

    }
}
