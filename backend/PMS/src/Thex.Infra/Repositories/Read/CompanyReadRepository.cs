﻿// //  <copyright file="CompanyReadRepository.cs" company="TOTVS">
// //  Copyright (c) TOTVS. All rights reserved.
// //  </copyright>
namespace Thex.EntityFrameworkCore.Repositories
{
    using System;
    using System.Linq;

    using Thex.Domain.Entities;
    using Thex.Infra.ReadInterfaces;
    using Tnf.EntityFrameworkCore.Repositories;
    using Tnf.EntityFrameworkCore;
    using Thex.Infra.Context;
    using Microsoft.EntityFrameworkCore;

    public class CompanyReadRepository : EfCoreRepositoryBase<ThexContext, Company>, ICompanyReadRepository
    {
        

        public CompanyReadRepository(IDbContextProvider<ThexContext> dbContextProvider)
            : base(dbContextProvider)
        {
            
        }

        public int GetCompanyIdByClientId(Guid clientId)
        {
            var result = Context.CompanyClients.Find(clientId);

            if (result != null)
                return result.CompanyId;

            return 0;
        }

        public Company GetCompanyById(int companyId)
        {
            return Context.Companies.Where(x => x.Id == companyId).FirstOrDefault();
        }

        public int GetCompanyIdByPropertyId(int propertyId)
        {
            var result = Context
                .Properties
                .FirstOrDefault(e => e.Id == propertyId);

            if (result != null)
                return result.CompanyId;

            return 0;
        }
    }
}