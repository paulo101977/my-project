﻿using System.Linq;
using Thex.Domain.Entities;
using Thex.Dto;
using Thex.Infra.ReadInterfaces;
using Tnf.Dto;
using Tnf.EntityFrameworkCore.Repositories;
using Tnf.EntityFrameworkCore;
using Thex.Infra.Context;
using System;

namespace Thex.Infra.Repositories.Read
{
    public class PropertyCompanyClientCategoryReadRepository : EfCoreRepositoryBase<ThexContext, PropertyCompanyClientCategory>, IPropertyCompanyClientCategoryReadRepository
    {
        

        public PropertyCompanyClientCategoryReadRepository(
            IDbContextProvider<ThexContext> dbContextProvider) : base(dbContextProvider)
        {
            
        }

        public IListDto<PropertyCompanyClientCategoryDto> GetAllByPropertyId(GetAllPropertyCompanyClientCategoryDto request, int propertyId)
        {
            var baseQuery = Context
                .PropertyCompanyClientCategories

                .Where(p => p.PropertyId == propertyId);

            if (request == null || !request.All.HasValue || !request.All.Value)
                baseQuery = baseQuery.Where(b => b.IsActive);

            var companyClientCategory = baseQuery
                //.SkipAndTakeByRequestDto(request)
                //.OrderByRequestDto(request)
                .ToListDto<PropertyCompanyClientCategory, PropertyCompanyClientCategoryDto>(request);

            return companyClientCategory;

        }

        public bool Exists(string categoryName, int propertyId, Guid id)
        {
            return Context.PropertyCompanyClientCategories.Any(p => p.PropertyCompanyClientCategoryName.ToLower()
            .Equals(categoryName.ToLower()) && propertyId.Equals(propertyId) && p.Id != id);
        }

        public bool HasClient(Guid id)
        {
            return Context.CompanyClients.Any(c => c.PropertyCompanyClientCategoryId.Equals(id));
        }
    }
}
