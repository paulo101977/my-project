﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Thex.Common.Enumerations;
using Thex.Domain.Entities;
using Thex.Dto;
using Thex.Infra.Context;
using Thex.Infra.ReadInterfaces;
using Tnf.EntityFrameworkCore;
using Tnf.EntityFrameworkCore.Repositories;

namespace Thex.Infra.Repositories.Read
{
    public class BillingInvoiceReadRepository : EfCoreRepositoryBase<ThexContext, BillingInvoice>, IBillingInvoiceReadRepository
    {
        public BillingInvoiceReadRepository(IDbContextProvider<ThexContext> dbContextProvider) : base(dbContextProvider)
        {
        }

        public List<InvoiceDto> GetAllByBillingAccountId(Guid billingAccountId)
        {
            return (from bi in Context.BillingInvoices
                    join bit in Context.BillingInvoiceTypes on bi.BillingInvoiceTypeId equals bit.Id
                    join bai in Context.BillingAccountItems on bi.Id equals bai.BillingInvoiceId
                    join ba in Context.BillingAccounts on bai.BillingAccountId equals ba.Id
                    where bi.BillingAccountId == billingAccountId
                    select new InvoiceDto
                    {
                        Id = bi.Id,
                        BillingInvoiceNumber = bi.BillingInvoiceNumber,
                        BillingInvoiceSeries = bi.BillingInvoiceSeries,
                        EmissionDate = bi.EmissionDate,
                        CancelDate = bi.CancelDate,
                        EmissionUserId = bi.EmissionUserId,
                        CancelUserId = bi.CancelUserId,
                        ExternalNumber = bi.ExternalNumber,
                        ExternalSeries = bi.ExternalSeries,
                        ExternalRps = bi.ExternalRps,
                        ExternalEmissionDate = bi.ExternalEmissionDate,
                        BillingAccountId = bi.BillingAccountId,
                        AditionalDetails = bi.AditionalDetails,
                        BillingInvoicePropertyId = bi.BillingInvoicePropertyId,
                        BillingInvoiceCancelReasonId = bi.BillingInvoiceCancelReasonId,
                        IntegratorLink = bi.IntegratorLink,
                        IntegratorXml = bi.IntegratorXml,
                        IntegratorId = bi.IntegratorId,
                        BillingInvoiceStatusId = bi.BillingInvoiceStatusId,
                        ExternalCodeNumber = bi.ExternalCodeNumber,
                        ErrorDate = bi.ErrorDate,
                        BillingInvoiceTypeId = bi.BillingInvoiceTypeId.Value,
                        BillingInvoiceTypeName = bit.BillingInvoiceTypeName
                    }).Distinct().ToList();
        }

        public List<InvoiceDto> GetAllByBillingAccountIdList(List<Guid> billingAccountIdList)
        {
            return (from bi in Context.BillingInvoices
                    join bit in Context.BillingInvoiceTypes on bi.BillingInvoiceTypeId equals bit.Id
                    join bai in Context.BillingAccountItems on bi.Id equals bai.BillingInvoiceId
                    join ba in Context.BillingAccounts on bai.BillingAccountId equals ba.Id
                    where billingAccountIdList.Contains(ba.Id)
                    select new InvoiceDto
                    {
                        Id = bi.Id,
                        BillingInvoiceNumber = bi.BillingInvoiceNumber,
                        BillingInvoiceSeries = bi.BillingInvoiceSeries,
                        EmissionDate = bi.EmissionDate,
                        CancelDate = bi.CancelDate,
                        EmissionUserId = bi.EmissionUserId,
                        CancelUserId = bi.CancelUserId,
                        ExternalNumber = bi.ExternalNumber,
                        ExternalSeries = bi.ExternalSeries,
                        ExternalRps = bi.ExternalRps,
                        ExternalEmissionDate = bi.ExternalEmissionDate,
                        BillingAccountId = bi.BillingAccountId,
                        AditionalDetails = bi.AditionalDetails,
                        BillingInvoicePropertyId = bi.BillingInvoicePropertyId,
                        BillingInvoiceCancelReasonId = bi.BillingInvoiceCancelReasonId,
                        IntegratorLink = bi.IntegratorLink,
                        IntegratorXml = bi.IntegratorXml,
                        IntegratorId = bi.IntegratorId,
                        BillingInvoiceStatusId = bi.BillingInvoiceStatusId,
                        ExternalCodeNumber = bi.ExternalCodeNumber,
                        ErrorDate = bi.ErrorDate,
                        BillingInvoiceTypeId = bi.BillingInvoiceTypeId.Value,
                        BillingInvoiceTypeName = bit.BillingInvoiceTypeName
                    }).Distinct().ToList();
        }

        public async Task<byte[]> GeneratePDFBillingInvoice(Guid id)
        {
            var billingInvoice = base.Get(new DefaultGuidRequestDto(id));
            if (billingInvoice != null && !string.IsNullOrWhiteSpace(billingInvoice.IntegratorLink))
            {
                var pdfByteArray = await GetPdf(billingInvoice.IntegratorLink);
                return pdfByteArray;
            }

            return null;
        }

        private async Task<byte[]> GetPdf(string link)
        {
            var client = new HttpClient();
            client.DefaultRequestHeaders.TryAddWithoutValidation("X-NFEIO-APIKEY", "Jx4TJQ0gbdvXChtfS20lT1JeT7WUPNdu4VNPImUuJw5LsUaGqBLXBsIDSmIVWhRPyz1");

            var result = await client.GetAsync(link);
            if (result.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var byteArray = await result.Content.ReadAsByteArrayAsync();
                return byteArray;
            }

            return null;
        }

        public InvoiceDto GetDtoById(Guid invoiceId)
        {
            return (from bi in Context.BillingInvoices
                    join bit in Context.BillingInvoiceTypes on bi.BillingInvoiceTypeId equals bit.Id
                    join bai in Context.BillingAccountItems on bi.Id equals bai.BillingInvoiceId
                    join ba in Context.BillingAccounts on bai.BillingAccountId equals ba.Id
                    where bi.Id == invoiceId
                    select new InvoiceDto
                    {
                        Id = bi.Id,
                        BillingInvoiceNumber = bi.BillingInvoiceNumber,
                        BillingInvoiceSeries = bi.BillingInvoiceSeries,
                        EmissionDate = bi.EmissionDate,
                        CancelDate = bi.CancelDate,
                        EmissionUserId = bi.EmissionUserId,
                        CancelUserId = bi.CancelUserId,
                        ExternalNumber = bi.ExternalNumber,
                        ExternalSeries = bi.ExternalSeries,
                        ExternalEmissionDate = bi.ExternalEmissionDate,
                        BillingAccountId = bi.BillingAccountId,
                        AditionalDetails = bi.AditionalDetails,
                        BillingInvoicePropertyId = bi.BillingInvoicePropertyId,
                        BillingInvoiceCancelReasonId = bi.BillingInvoiceCancelReasonId,
                        IntegratorLink = bi.IntegratorLink,
                        IntegratorXml = bi.IntegratorXml,
                        IntegratorId = bi.IntegratorId,
                        BillingInvoiceStatusId = bi.BillingInvoiceStatusId,
                        ExternalCodeNumber = bi.ExternalCodeNumber,
                        ErrorDate = bi.ErrorDate,
                        BillingInvoiceTypeId = bi.BillingInvoiceTypeId.Value,
                        BillingInvoiceTypeName = bit.BillingInvoiceTypeName
                    }).Distinct().FirstOrDefault();
        }

        public async Task<List<InvoiceDto>> GetAllByFilters(int propertyId, DateTime? initialDate, DateTime? finalDate, int? initialNumber, int? finalNumber)
        {
            var qry = (from bi in Context.BillingInvoices
                       where bi.PropertyId == propertyId
                       && bi.BillingInvoiceTypeId == (int)BillingInvoiceTypeEnum.NFCE
                       && bi.BillingInvoiceStatusId == (int)BillingInvoiceStatusEnum.Issued
                       select new InvoiceDto
                       {
                           Id = bi.Id,
                           EmissionDate = bi.EmissionDate,
                           CancelDate = bi.CancelDate,
                           ExternalNumber = bi.ExternalNumber,
                           ExternalSeries = bi.ExternalSeries,
                           ExternalRps = bi.ExternalRps,
                           ExternalEmissionDate = bi.ExternalEmissionDate,
                           BillingAccountId = bi.BillingAccountId,
                           BillingInvoiceCancelReasonId = bi.BillingInvoiceCancelReasonId,
                           IntegratorLink = bi.IntegratorLink,
                           IntegratorXml = bi.IntegratorXml,
                       });

            if (initialDate != null && initialDate != default(DateTime))
                qry = qry.Where(x => x.ExternalEmissionDate >= initialDate &&
                                     x.ExternalEmissionDate <= finalDate);

            if (initialNumber != null && initialNumber > 0 &&
                finalNumber != null && finalNumber > 0)
                qry = qry.Where(x => x.ExternalNumber.TryParseToInt32() >= initialNumber &&
                                     x.ExternalNumber.TryParseToInt32() <= finalNumber);

            return await Task.FromResult(qry.ToList());
        }

        public string GetIntegratorIdById(Guid id)
        {
            return (from bi in Context.BillingInvoices
                    where bi.Id == id
                    select bi.IntegratorId).FirstOrDefault();
        }

        public BillingInvoice GetById(Guid id)
        {
            return Context.BillingInvoices.Find(id);
        }

        public List<BillingInvoice> GetAllByIntegratorId(string integratorId)
        {
            return (from bi in Context.BillingInvoices
                    where bi.IntegratorId == integratorId
                    select bi).ToList();
        }
    }
}
