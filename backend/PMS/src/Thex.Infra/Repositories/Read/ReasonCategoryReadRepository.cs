﻿using System;
using System.Collections.Generic;
using System.Linq;
using Thex.Domain.Entities;
using Thex.Dto;
using Thex.Infra.ReadInterfaces;

using Tnf.Dto;
using Tnf.EntityFrameworkCore.Repositories;
using Tnf.EntityFrameworkCore;
using Thex.Infra.Context;
using Tnf.Localization;
using Thex.Common;
using Thex.Common.Enumerations;

namespace Thex.Infra.Repositories.Read
{
    public class ReasonCategoryReadRepository : EfCoreRepositoryBase<ThexContext, ReasonCategory>, IReasonCategoryReadRepository
    {
        private readonly ILocalizationManager _localizationManager;

        public ReasonCategoryReadRepository(IDbContextProvider<ThexContext> dbContextProvider, ILocalizationManager localizationManager) 
            : base(dbContextProvider)
        {
            _localizationManager = localizationManager;
        }

        public IListDto<ReasonCategoryGetAllDto> GetAllReasonCategory()
        {
            var dbQuery = (from c in Context.ReasonCategories
                           select new ReasonCategoryGetAllDto()
                           {
                               Id = c.Id                          
                           });

            var reasonCategoryDtoList = new List<ReasonCategoryGetAllDto>();

            foreach (var item in dbQuery.ToList())
            {
                item.ReasonCategoryName = _localizationManager.GetString(AppConsts.LocalizationSourceName, ((ReasonCategoryEnum)item.Id).ToString());
                reasonCategoryDtoList.Add(item.MapTo<ReasonCategoryGetAllDto>());
            }

            return new ListDto<ReasonCategoryGetAllDto>
            {
                HasNext = false,
                Items = reasonCategoryDtoList,
              
            };
        }
    }
}
