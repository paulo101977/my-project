﻿using System;
using System.Collections.Generic;
using Thex.Domain.Entities;
using System.Linq;
using Thex.Infra.ReadInterfaces;
using Tnf.EntityFrameworkCore.Repositories;
using Tnf.EntityFrameworkCore;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Thex.Common.Extensions;
using Thex.Infra.Context;
using Thex.Dto;
using Thex.Common;
using Tnf.Localization;
using Thex.Common.Enumerations;

namespace Thex.Infra.Repositories.Read
{
    public class PropertyBaseRateHistoryReadRepository : EfCoreRepositoryBase<ThexContext, PropertyBaseRateHistory>, IPropertyBaseRateHistoryReadRepository
    {
        private readonly ICurrencyReadRepository _currencyReadRepository;
        private readonly IPropertyMealPlanTypeReadRepository _mealPlanTypeReadRepository;
        private readonly IRoomTypeReadRepository _roomTypeReadRepository;
        private readonly ILocalizationManager _localizationManager;

        public PropertyBaseRateHistoryReadRepository(
            IDbContextProvider<ThexContext> dbContextProvider,
            ICurrencyReadRepository currencyReadRepository,
            IPropertyMealPlanTypeReadRepository mealPlanTypeReadRepository,
            IRoomTypeReadRepository roomTypeReadRepository,
            ILocalizationManager localizationManager)
            : base(dbContextProvider)
        {
            _currencyReadRepository = currencyReadRepository;
            _mealPlanTypeReadRepository = mealPlanTypeReadRepository;
            _roomTypeReadRepository = roomTypeReadRepository;
            _localizationManager = localizationManager;
        }

        public async Task<List<PropertyBaseRateHistory>> GetAll(Guid? pRatePlanId)
        {
            var dbBaseQuery = Context
                                .PropertyBaseRateHistories
                                .AsNoTracking()
                                .Where(e => e.RatePlanId == pRatePlanId)
                                .OrderByDescending(e => e.CreationTime);

            return await dbBaseQuery.ToListAsync();
        }

        public IQueryable<PropertyBaseRateHistoryDto> GetAllByRatePlanId(Guid? pRatePlanId)
        {
            var dbBaseQuery = (from propertyBaseRateHistory in Context.PropertyBaseRateHeaderHistories
                               where propertyBaseRateHistory.RatePlanId == pRatePlanId
                               orderby propertyBaseRateHistory.CreationTime

                               select new PropertyBaseRateHistoryDto
                               {
                                   CurrencyId = propertyBaseRateHistory.CurrencyId,
                                   Id = propertyBaseRateHistory.Id,
                                   PropertyId = propertyBaseRateHistory.PropertyId,
                                   RatePlanId = propertyBaseRateHistory.RatePlanId,
                                   TenantId = propertyBaseRateHistory.TenantId,
                               });
            return dbBaseQuery;
        }

        public async Task<List<PropertyBaseRateHistory>> GetAllByHistoryId(Guid? pHistoryId)
        {
            var dbBaseQuery = Context.PropertyBaseRateHistories
                                .AsNoTracking()
                                .Where(x => x.PropertyBaseRateHeaderHistoryId == pHistoryId)
                               .OrderByDescending(x => x.CreationTime);

            return await dbBaseQuery.ToListAsync();
        }

        public PropertyBaseRateHistory GetByHeaderHistoryId(Guid? pPropertyBaseRateHistoryid)
        {
            if (pPropertyBaseRateHistoryid == null) return null;

            var item = Context.PropertyBaseRateHistories
                             .AsNoTracking()
                              .Where(x => x.Id == pPropertyBaseRateHistoryid).FirstOrDefault();
            return item;
        }

        public IQueryable<PropertyBaseRateHistoryDto> GetAllDtoByHistoryId(List<Guid> pLstHistoryId)
        {
            var dbBaseQuery = (from propertyBaseRateHistory in Context.PropertyBaseRateHistories

                               join MealPlanProper in Context.PropertyMealPlanTypes
                                             on propertyBaseRateHistory.MealPlanTypeId equals MealPlanProper.MealPlanTypeId into a
                               from MealPlanProper in a.DefaultIfEmpty()

                               join MealPlan in Context.MealPlanTypes
                                             on propertyBaseRateHistory.MealPlanTypeId equals MealPlan.Id into z
                               from MealPlan in z.DefaultIfEmpty()

                               join Currenc in Context.Currencies
                                            on propertyBaseRateHistory.CurrencyId equals Currenc.Id into b
                               from Currenc in b.DefaultIfEmpty()

                               join U in Context.Users
                                      on propertyBaseRateHistory.CreatorUserId equals U.Id into c
                               from U in c.DefaultIfEmpty()

                               join Roomtype in Context.RoomTypes
                                             on propertyBaseRateHistory.RoomTypeId equals Roomtype.Id into d
                               from Roomtype in d.DefaultIfEmpty()
                               where pLstHistoryId.Contains((Guid)propertyBaseRateHistory.PropertyBaseRateHeaderHistoryId)
                               orderby propertyBaseRateHistory.CreationTime
                               
                               select new PropertyBaseRateHistoryDto
                               {
                                   CurrencyId = propertyBaseRateHistory.CurrencyId,
                                   Id = propertyBaseRateHistory.Id,
                                   PropertyId = propertyBaseRateHistory.PropertyId,
                                   RatePlanId = propertyBaseRateHistory.RatePlanId,
                                   TenantId = propertyBaseRateHistory.TenantId,
                                   AdultMealPlanAmount = propertyBaseRateHistory.AdultMealPlanAmount,
                                   Child1Amount = propertyBaseRateHistory.Child1Amount,
                                   Child2Amount = propertyBaseRateHistory.Child2Amount,
                                   Child3Amount = propertyBaseRateHistory.Child3Amount,
                                   Child1MealPlanAmount = propertyBaseRateHistory.Child1MealPlanAmount,
                                   Child2MealPlanAmount = propertyBaseRateHistory.Child2MealPlanAmount,
                                   Child3MealPlanAmount = propertyBaseRateHistory.Child3MealPlanAmount,
                                   Pax10Amount = propertyBaseRateHistory.Pax10Amount,
                                   Pax11Amount = propertyBaseRateHistory.Pax11Amount,
                                   Pax12Amount = propertyBaseRateHistory.Pax12Amount,
                                   Pax13Amount = propertyBaseRateHistory.Pax13Amount,
                                   Pax14Amount = propertyBaseRateHistory.Pax14Amount,
                                   Pax15Amount = propertyBaseRateHistory.Pax15Amount,
                                   Pax16Amount = propertyBaseRateHistory.Pax16Amount,
                                   Pax17Amount = propertyBaseRateHistory.Pax17Amount,
                                   Pax18Amount = propertyBaseRateHistory.Pax18Amount,
                                   Pax19Amount = propertyBaseRateHistory.Pax19Amount,
                                   Pax20Amount = propertyBaseRateHistory.Pax20Amount,
                                   Pax1Amount = propertyBaseRateHistory.Pax1Amount,
                                   Pax2Amount = propertyBaseRateHistory.Pax2Amount,
                                   Pax3Amount = propertyBaseRateHistory.Pax3Amount,
                                   Pax4Amount = propertyBaseRateHistory.Pax4Amount,
                                   Pax5Amount = propertyBaseRateHistory.Pax5Amount,
                                   Pax6Amount = propertyBaseRateHistory.Pax6Amount,
                                   Pax7Amount = propertyBaseRateHistory.Pax7Amount,
                                   Pax8Amount = propertyBaseRateHistory.Pax8Amount,
                                   Pax9Amount = propertyBaseRateHistory.Pax9Amount,
                                   Level = propertyBaseRateHistory.Level,
                                   CurrencySymbol = Currenc.Symbol,
                                   MealPlanTypeDefault = propertyBaseRateHistory.MealPlanTypeDefault,
                                   PaxAdditionalAmount = propertyBaseRateHistory.PaxAdditionalAmount,
                                   RoomTypeId = propertyBaseRateHistory.RoomTypeId,
                                   MealPlanTypeId = propertyBaseRateHistory.MealPlanTypeId,
                                   PropertyBaseRateHeaderHistoryId = propertyBaseRateHistory.PropertyBaseRateHeaderHistoryId,
                                   CurrencyName = Currenc.CurrencyName != null ? Currenc.CurrencyName : null,
                                   RoomTypeName = Roomtype != null ? Roomtype.Name : null,                                   
                                   MealPlanTypeName = MealPlanProper != null ? MealPlanProper.PropertyMealPlanTypeName : MealPlan.MealPlanTypeName,
                                   CreatorUserName = U != null ? U.Name : null                                   
                               });
            return dbBaseQuery;
        }
    }
}
