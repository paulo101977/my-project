﻿using System;
using System.Linq;
using Thex.Domain.Entities;
using Thex.Infra.Context;
using Thex.Infra.ReadInterfaces;
using Tnf.EntityFrameworkCore;
using Tnf.EntityFrameworkCore.Repositories;

namespace Thex.Infra.Repositories.Read
{
    public class SibaIntegrationReadRepository : EfCoreRepositoryBase<ThexContext, SibaIntegration>, ISibaIntegrationReadRepository
    {
        public SibaIntegrationReadRepository(
            IDbContextProvider<ThexContext> dbContextProvider)
            : base(dbContextProvider)
        {

        }


        public SibaIntegration GetByPropertyId(Guid propertyUId)
        {
            return Context.SibaIntegrations.FirstOrDefault(s => s.PropertyUId == propertyUId);
        }
    }
}
