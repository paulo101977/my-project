﻿using Tnf.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using Thex.Domain.Entities;
using Thex.Infra.ReadInterfaces;
using Tnf.EntityFrameworkCore.Repositories;
using System.Linq;
using Thex.Dto;
using Tnf.Dto;
using Thex.Dto.BillingItemCategory;
using Thex.Infra.Context;

namespace Thex.Infra.Repositories.Read
{
    public class BillingItemCategoryReadRepository : EfCoreRepositoryBase<ThexContext, BillingItemCategory>, IBillingItemCategoryReadRepository
    {


        public BillingItemCategoryReadRepository(IDbContextProvider<ThexContext> dbContextProvider)
            : base(dbContextProvider)
        {

        }


        public IListDto<GetAllCategoriesForItemDto> GetAllCategoriesForItem(int propertyId)
        {
            var dbBaseQuery = from c in Context
                              .BillingItemCategories

                              where c.StandardCategoryId != null && c.PropertyId == propertyId
                              && c.IsActive
                              select new GetAllCategoriesForItemDto()
                              {
                                  Id = c.Id,
                                  BillingItemCategoryName = c.CategoryName
                              };


            var billingItemCategoryDtoList = new List<GetAllCategoriesForItemDto>();

            foreach (var item in dbBaseQuery.ToList())
                billingItemCategoryDtoList.Add(item.MapTo<GetAllCategoriesForItemDto>());

            return new ListDto<GetAllCategoriesForItemDto>
            {
                HasNext = false,
                Items = billingItemCategoryDtoList,

            };
        }
        public IListDto<BillingItemCategoryDto> GetAllGroupByPropertyId(GetAllBillingItemCategoryDto request, int propertyId)
        {
            //var dbBaseQuery =  Context.BillingItemCategories.Where(b => b.StandardCategoryId != null && b.PropertyId == propertyId);

            var dbBaseQuery = from b in Context.BillingItemCategories

                              join g in Context.BillingItemCategories on b.StandardCategoryId equals g.Id
                              where b.StandardCategoryId != null && b.PropertyId == propertyId
                              select new BillingItemCategoryDto()
                              {
                                  Id = b.Id,
                                  CategoryName = b.CategoryName,
                                  IsActive = b.IsActive,
                                  PropertyId = b.PropertyId,
                                  StandardCategoryId = b.StandardCategoryId,
                                  CategoryFixedName = g.CategoryName
                              };

            if (dbBaseQuery != null && !string.IsNullOrEmpty(request.SearchData))
                dbBaseQuery = dbBaseQuery.Where(d => d.CategoryName.Contains(request.SearchData));

            var billingItemCategoryDtoList = new List<BillingItemCategoryDto>();

            foreach (var item in dbBaseQuery.ToList())
                billingItemCategoryDtoList.Add(item.MapTo<BillingItemCategoryDto>());

            return new ListDto<BillingItemCategoryDto>
            {
                HasNext = false,
                Items = billingItemCategoryDtoList,
            };
        }

        public IListDto<BillingItemCategoryDto> GetAllGroupFixed()
        {

            var dbBaseQuery = Context.BillingItemCategories
                              .Where(b => b.StandardCategoryId == null);

            var billingItemCategoryDtoList = new List<BillingItemCategoryDto>();

            foreach (var item in dbBaseQuery.ToList())
                billingItemCategoryDtoList.Add(item.MapTo<BillingItemCategoryDto>());

            return new ListDto<BillingItemCategoryDto>
            {
                HasNext = false,
                Items = billingItemCategoryDtoList,
            };
        }

        public IListDto<BillingItemCategoryDto> GetItemsByGroupId(int groupId)
        {

            var dbBaseQuery = Context.BillingItemCategories
                              .Where(b => b.StandardCategoryId == groupId);

            var billingItemCategoryDtoList = new List<BillingItemCategoryDto>();

            foreach (var item in dbBaseQuery.ToList())
                billingItemCategoryDtoList.Add(item.MapTo<BillingItemCategoryDto>());

            return new ListDto<BillingItemCategoryDto>
            {
                HasNext = false,
                Items = billingItemCategoryDtoList,
            };
        }
    }
}

