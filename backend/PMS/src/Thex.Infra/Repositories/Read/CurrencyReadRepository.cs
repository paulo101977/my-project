﻿using System;
using System.Collections.Generic;
using Thex.Domain.Entities;
using Thex.Dto;
using Thex.Infra.ReadInterfaces;
using Tnf.Dto;
using Tnf.EntityFrameworkCore.Repositories;
using Tnf.EntityFrameworkCore;
using System.Linq;

using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using Thex.Infra.Context;
using Thex.Common.Enumerations;
using Thex.Kernel;

namespace Thex.Infra.Repositories.Read
{
    public class CurrencyReadRepository : EfCoreRepositoryBase<ThexContext, Currency>, ICurrencyReadRepository
    {
        private readonly IApplicationUser _applicationUser;

        public CurrencyReadRepository(IDbContextProvider<ThexContext> dbContextProvider, IApplicationUser applicationUser)
                   : base(dbContextProvider)
        {
            _applicationUser = applicationUser;
        }

        public ListDto<CurrencyDto> GetAllCurrency()
        {
            var dbBaseQuery = Context.Currencies.ToList();


            var currencies = new List<CurrencyDto>();

            foreach (var item in dbBaseQuery.ToList())
                currencies.Add(item.MapTo<CurrencyDto>());


            return new ListDto<CurrencyDto>
            {
                HasNext = false,
                Items = currencies,
            };

        }

        public async Task<IListDto<CurrencyDto>> GetAllCurrencyAsync(GetAllCurrencyDto request = null)
        {
            var dbBaseQuery = await Context.Currencies.AsNoTracking().ToListAsync();

            if (request != null && request.IsQuotation)
            {
                var propertyId = int.Parse(_applicationUser.PropertyId);
                var propertyParameter = Context.PropertyParameters.FirstOrDefault(x => x.PropertyId == propertyId && x.ApplicationParameterId == (int)ApplicationParameterEnum.ParameterDefaultCurrency);
                if(!string.IsNullOrWhiteSpace(propertyParameter?.PropertyParameterValue))
                    dbBaseQuery = dbBaseQuery.Where(c => !c.Id.Equals(Guid.Parse(propertyParameter.PropertyParameterValue))).ToList();
            }                

            var currencies = new List<CurrencyDto>();

            foreach (var item in dbBaseQuery.ToList())
                currencies.Add(item.MapTo<CurrencyDto>());

            return new ListDto<CurrencyDto>
            {
                HasNext = false,
                Items = currencies,
            };
        }

        public bool IsExistCurrencyId(Guid currencyId)
        {
            return Context.Currencies.Any(x => x.Id == currencyId);
        }

        public CurrencyDto GetNameCurrencyById(Guid currencyId)
        {
            var vCurr = Context.Currencies.Where(x=> x.Id == currencyId).FirstOrDefault();

            if (vCurr == null)
                return null;
            
            return vCurr.MapTo<CurrencyDto>();            
        }

        public CurrencyDto GetPropertyCurrencyById(int propertyId)
        {
            var propertyParameter = Context.PropertyParameters.FirstOrDefault(x => x.PropertyId == propertyId && x.ApplicationParameterId == (int)ApplicationParameterEnum.ParameterDefaultCurrency);
            var propertyCurrencyParameter = new CurrencyDto();
            if (!string.IsNullOrWhiteSpace(propertyParameter?.PropertyParameterValue))
            {
                var currencyId = Guid.Parse(propertyParameter.PropertyParameterValue);
                propertyCurrencyParameter = Context.Currencies.FirstOrDefault(c => c.Id.Equals(currencyId)).MapTo<CurrencyDto>();
            }
            return propertyCurrencyParameter;
        }

        public bool IsToDolarQuotation(Guid currencyId)
        {
            return Context.Currencies.Any(c => c.Id == currencyId && c.AlphabeticCode.ToUpper() == "USD");
        }
    }
}
