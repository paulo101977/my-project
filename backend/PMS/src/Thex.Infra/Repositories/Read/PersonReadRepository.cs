﻿//  <copyright file="PersonReadRepository.cs" company="TOTVS">
//  Copyright (c) TOTVS. All rights reserved.
//  </copyright>
namespace Thex.EntityFrameworkCore.Repositories
{
    using System;
    using Thex.Domain.Entities;
    using Thex.Infra.ReadInterfaces;
    using System.Linq;
    using Tnf.Dto;
    using Tnf.EntityFrameworkCore.Repositories;
    using Tnf.EntityFrameworkCore;
    using Thex.Dto.Person;
    using Thex.Common.Enumerations;
    using System.Collections.Generic;
    using Microsoft.EntityFrameworkCore;
    using Thex.Infra.Context;
    using Thex.Dto;
    using Thex.Dto.CountrySubdivisionTranslation;

    public class PersonReadRepository : EfCoreRepositoryBase<ThexContext, Person>, IPersonReadRepository
    {
        public PersonReadRepository(IDbContextProvider<ThexContext> dbContextProvider)
            : base(dbContextProvider)
        {
        }

        public List<ContactInformation> GetContactInformationByPersonsIds(List<Guid> personsIds)
        {
            return Context.ContactInformations
                .Include(exp => exp.ContactInformationType)
                .Where(exp => personsIds.Contains(exp.OwnerId)).ToList();
        }

        public Person GetById(Guid personId)
        {
            return Context.Persons.Where(x => x.Id == personId).FirstOrDefault();
        }

        public List<PersonInformation> GetPersonInformationByPersonsIds(List<Guid> personsIds)
        {
            return Context.PersonInformations
                .Include(exp => exp.PersonInformationType)
                .Where(exp => personsIds.Contains(exp.OwnerId)).ToList();
        }

        public IListDto<SearchPersonsResultDto> GetPersonsBasedOnFilter(SearchPersonsDto request)
        {
            IQueryable<SearchPersonsResultDto> dbBaseQuery = null;
            request.Page = request.Page == 0 ? 1 : request.Page;

            if (request.ByEmail) dbBaseQuery = this.SearchPersonsByEmail(request);
            else if (request.DocumentTypeId != 0) dbBaseQuery = this.SearchPersonsByFullNameAndDocument(request);
            else dbBaseQuery = this.SearchPersonsByFullname(request);

            var totalItemsDto = dbBaseQuery.Count();
            var resultDto = dbBaseQuery
                        .Skip(request.PageSize * (request.Page - 1))
                        .Take(request.PageSize)
                        .OrderBy(p => p.Name)
                        .ToList();

            if (resultDto.Count > 0)
            {
                GetContactInformations(resultDto);
                GetPersonsInformations(resultDto);
            }
            var result = new ListDto<SearchPersonsResultDto>();
            result.Items = resultDto;
            result.HasNext = totalItemsDto > ((request.Page - 1) * request.PageSize) + resultDto.Count;


            return result;
        }

        private void GetContactInformations(List<SearchPersonsResultDto> resultDto)
        {
            var allContactInformations = GetContactInformationByPersonsIds(resultDto.Select(exp => exp.Id).ToList());

            foreach (var item in resultDto)
            {
                var contactInformations = allContactInformations.Where(exp => exp.OwnerId == item.Id).OrderBy(exp => exp.Information).ToList();

                if (contactInformations.Any())
                    item.ContactInformation = contactInformations.Select(exp => new SearchPersonsContactInformationResultDto
                    {
                        Type = exp.ContactInformationType.Id,
                        TypeValue = exp.ContactInformationType.Name,
                        Value = exp.Information
                    }).ToList();
            }
        }

        private void GetPersonsInformations(List<SearchPersonsResultDto> resultDto)
        {
            var allPersonInformations = GetPersonInformationByPersonsIds(resultDto.Select(exp => exp.Id).ToList());

            foreach (var item in resultDto)
            {
                var personInformations = allPersonInformations.Where(exp => exp.OwnerId == item.Id).OrderBy(exp => exp.Information).ToList();

                if (personInformations.Any())
                    item.PersonInformation = personInformations.Select(exp => new SearchPersonsInformationResultDto
                    {
                        Type = exp.PersonInformationType.Id,
                        TypeValue = exp.PersonInformationType.Name,
                        Value = exp.Information
                    }).ToList();
            }
        }

        private IQueryable<SearchPersonsResultDto> SearchPersonsByFullname(SearchPersonsDto request)
        {
            var dbBaseQuery = (from person in Context.Persons
                               join guest in Context.Guests on person.Id equals guest.PersonId.Value into a
                               from guest in a.DefaultIfEmpty()
                               select new
                               {
                                   FullName = person.FirstName + " " + person.LastName,
                                   PeopleId = person.Id,
                                   PersonType = person.PersonType,
                                   GuestId = guest != null ? guest.Id : (long?)null
                               }
                               ).Where(x => x.FullName.ToLower().Contains(request.SearchData.ToLower()) && x.PersonType[0] == (char)PersonTypeEnum.Natural)
                               .Select(x => new SearchPersonsResultDto
                               {

                                   Id = x.PeopleId,
                                   Name = x.FullName,
                                   GuestId = x.GuestId != null ? x.GuestId : (long?)null

                               });


            return dbBaseQuery;
        }

        private IQueryable<SearchPersonsResultDto> SearchPersonsByFullNameAndDocument(SearchPersonsDto request)
        {
            var dbBaseQuery = this.Context.Persons
                .Join(
                    this.Context.Documents,
                    p => p.Id,
                    d => d.OwnerId,
                    (p, d) => new { people = p, document = d })
                .Join(
                    this.Context.DocumentTypes,
                    dd => dd.document.DocumentTypeId,
                    dt => dt.Id,
                    (dd, dt) => new { dd.people, dd.document, documentType = dt })
                .GroupJoin(
                    this.Context.Guests,
                    doc => doc.document.OwnerId,
                    gt => gt.PersonId,
                    (doc, gt) => new { doc.people, doc.document, doc.documentType, guest = gt })
                    .SelectMany(
                                g => g.guest.DefaultIfEmpty(),
                               (doc, gt) => new
                               {
                                   doc.people,
                                   doc.document,
                                   doc.documentType,
                                   guest = gt
                               })
                .Select(
                    x => new
                    {
                        FullName = x.people.FirstName + " " + x.people.LastName,
                        PersonType = x.people.PersonType,
                        PeopleId = x.people.Id,
                        DocumentInfo = x.document.DocumentInformation,
                        DocumentTypeId = x.documentType.Id,
                        DocumentType = x.documentType.Name,
                        GuestId = x.guest != null ? x.guest.Id : (long?)null
                    }).Where(
                    x => (x.FullName.ToLower().Contains(request.SearchData.ToLower()) || x.DocumentInfo.ToLower().Contains(request.SearchData.ToLower())) &&
                         Convert.ToChar(x.PersonType) == (char)PersonTypeEnum.Natural &&
                         x.DocumentTypeId == request.DocumentTypeId).Select(
                    x => new SearchPersonsResultDto
                    {
                        Id = x.PeopleId,
                        Document = x.DocumentInfo,
                        Name = x.FullName,
                        DocumentType = x.DocumentType,
                        DocumentTypeId = x.DocumentTypeId,
                        GuestId = x.GuestId != null ? x.GuestId : (long?)null
                    });
            return dbBaseQuery;
        }

        private IQueryable<SearchPersonsResultDto> SearchPersonsByEmail(SearchPersonsDto request)
        {
            var dbBaseQuery = (from p in Context.Persons
                               join g in Context.Guests on p.Id equals g.PersonId.Value into a
                               from g in a.DefaultIfEmpty()
                               join c in Context.ContactInformations on g.PersonId.Value equals c.OwnerId
                               join t in Context.ContactInformationTypes on c.ContactInformationTypeId equals t.Id
                               where g.PersonId.HasValue &&
                               t.Id == (int)ContactInformationTypeEnum.Email &&
                               c.Information.ToLower().Contains(request.SearchData.ToLower()) &&
                               Convert.ToChar(p.PersonType) == (char)PersonTypeEnum.Natural
                               select new SearchPersonsResultDto
                               {
                                   Id = p.Id,
                                   Name = p.FirstName + " " + p.LastName,
                                   GuestId = g != null ? g.Id : (long?)null
                               });

            return dbBaseQuery;
        }


        public IListDto<SearchPersonsResultDto> GetPersonsBasedOnFilterWithCompanyClient(SearchPersonsDto request, int companyId)
        {
            IQueryable<SearchPersonsResultDto> dbBaseQuery = null;
            request.Page = request.Page == 0 ? 1 : request.Page;

            dbBaseQuery = this.SearchPersonsByFullnameAndDocumentWithCompanyClient(request, companyId);

            var totalItemsDto = dbBaseQuery.Count();
            var resultDto = dbBaseQuery
                        .Skip(request.PageSize * (request.Page - 1))
                        .Take(request.PageSize)
                        //.OrderBy(p => p.)
                        .ToList();


            var result = new ListDto<SearchPersonsResultDto>();
            result.Items = resultDto.OrderBy(d => d.Name).ToList();
            result.HasNext = totalItemsDto > ((request.Page - 1) * request.PageSize) + resultDto.Count;


            return result;
        }

        public IQueryable<SearchPersonsResultDto> GetDocumentsByPersonId(Guid personId)
        {
            var dbBaseQuery = (from p in Context.Persons

                               join d2 in Context.Documents on p.Id equals d2.OwnerId 
                               
                               join cpd in Context.CountryPersonDocumentTypes on
                               new { cpd1 = p.CountrySubdivisionId.HasValue ? p.CountrySubdivisionId.Value : 0, cpd2 = p.PersonType, cpd3 = d2.DocumentTypeId} equals
                               new { cpd1 = cpd.CountrySubdivisionId, cpd2 = cpd.PersonType, cpd3 = cpd.DocumentTypeId } into countryPersonDocumentTypes
                               from leftCountryPersonDocumentTypes in countryPersonDocumentTypes.DefaultIfEmpty()


                               where p.Id == personId
                               select new SearchPersonsResultDto
                               {
                                   Document = d2 != null ? d2.DocumentInformation: null,
                                   DocumentTypeId = d2 != null ?  d2.DocumentTypeId : 0,
                                   IsDocumentMain = leftCountryPersonDocumentTypes != null ? IsDocumentMain(d2.DocumentTypeId, leftCountryPersonDocumentTypes) : false
                               }
                         );

            return dbBaseQuery;
        }

        private bool IsDocumentMain(int documentTypeId, CountryPersonDocumentType countryPersonDocumentType)
        {
            if (documentTypeId == countryPersonDocumentType.DocumentTypeId)
               return true;

            return false;
            
        }
        private IQueryable<SearchPersonsResultDto> SearchPersonsByFullnameAndDocumentWithCompanyClient(SearchPersonsDto request, int companyId)
        {
            var dbBaseQuery = (from p in Context.Persons

                               join g in Context.Guests on p.Id equals g.PersonId.Value into guests
                               from leftJoinGuest in guests.DefaultIfEmpty()

                               join c in Context.CompanyClients on
                                new { cPersonId1 = p.Id, cCompanyId = companyId } equals
                                new { cPersonId1 = c.PersonId, cCompanyId = c.CompanyId }
                                into companyClients
                               from leftCompanyClient in companyClients.DefaultIfEmpty()

                               join ciEmail in Context.ContactInformations on
                                new { ciEmail1 = p.Id, ciEmail2 = 1 } equals
                                new { ciEmail1 = ciEmail.OwnerId, ciEmail2 = ciEmail.ContactInformationTypeId } into email
                               from leftEmail in email.DefaultIfEmpty()

                               join ciPhone in Context.ContactInformations on
                               new { ciPhone1 = p.Id, ciPhone2 = 2 } equals
                               new { ciPhone1 = ciPhone.OwnerId, ciPhone2 = ciPhone.ContactInformationTypeId } into phone
                               from leftPhone in phone.DefaultIfEmpty()

                               join d2 in Context.Documents on
                               new { da = request.DocumentTypeId, db = p.Id } equals
                               new { da = d2.DocumentTypeId, db = d2.OwnerId } into document2
                               from leftDocument2 in document2.DefaultIfEmpty()
                               where p.PersonType == request.PersonType &&
                               (
                                p.FirstName.ToLower().Contains(request.SearchData.ToLower()) ||
                                p.LastName.ToLower().Contains(request.SearchData.ToLower()) ||
                                (leftDocument2.DocumentInformation.ToLower().Contains(request.SearchData.ToLower()))


                                )
                               select new SearchPersonsResultDto
                               {
                                   Id = p.Id,
                                   Document = leftDocument2 != null ? leftDocument2.DocumentInformation : null,
                                   Name = p.FirstName + " " + p.LastName,
                                   GuestId = leftJoinGuest != null ? leftJoinGuest.Id : 0,
                                   CompanyClientId = leftCompanyClient != null ? leftCompanyClient.Id : Guid.Empty,
                                   ContactInformation = new List<SearchPersonsContactInformationResultDto>
                                    {
                                        new SearchPersonsContactInformationResultDto
                                        {
                                            Type = leftEmail != null ? leftEmail.ContactInformationTypeId : 0,
                                            TypeValue = ContactInformationTypeEnum.Email.ToString(),
                                            Value = leftEmail != null ? leftEmail.Information : null,
                                        },
                                        new SearchPersonsContactInformationResultDto
                                        {
                                            Type = leftPhone != null ? leftPhone.ContactInformationTypeId : 0,
                                            TypeValue = ContactInformationTypeEnum.PhoneNumber.ToString(),
                                            Value = leftPhone != null ? leftPhone.Information : null,
                                        },
                                    }
                               }
                );

            return dbBaseQuery;
        }


        public KeyValuePair<Guid, long>? GetPersonIdAndGuestIdByDocument(int documentTypeId, string document)
        {
            return (from p in Context.Persons
                    join d in Context.Documents on p.Id equals d.OwnerId

                    join g in Context.Guests on p.Id equals g.PersonId into guests
                    from leftGuest in guests.DefaultIfEmpty()
                    where d.DocumentTypeId == documentTypeId && d.DocumentInformation == document
                    select new KeyValuePair<Guid, long>(p.Id, leftGuest != null ? leftGuest.Id : 0)
                    ).FirstOrDefault();
        }

        public Person GetCompletePersonById(Guid personId)
        {
            return Context.Persons
                            .Include(p => p.LocationList)
                            .Include(p => p.DocumentList)
                            .Include(p => p.ContactInformationList)
                            .FirstOrDefault(p => p.Id == personId);
        }

        public HeaderPersonDto GetHeaderByBillingAccountId(Guid billingAccountId)
        {
            return (from billingAccount in this.Context.BillingAccounts
                   join person in this.Context.Persons on billingAccount.PersonHeaderId equals person.Id

                   join emailInformation in this.Context.ContactInformations on
                   new { p1 = person.Id, p2 = Convert.ToInt32(ContactInformationTypeEnum.Email) } equals
                   new { p1 = emailInformation.OwnerId, p2 = emailInformation.ContactInformationTypeId } into ri
                   from emailInformationLeft in ri.DefaultIfEmpty()

                   join phoneInformation in this.Context.ContactInformations on
                   new { p3 = person.Id, p4 = Convert.ToInt32(ContactInformationTypeEnum.PhoneNumber) } equals
                   new { p3 = phoneInformation.OwnerId, p4 = phoneInformation.ContactInformationTypeId } into ri2
                   from phoneInformationLeft in ri2.DefaultIfEmpty()

                   where billingAccount.Id == billingAccountId
                   select new HeaderPersonDto
                   {
                       Id = person.Id,
                       FirstName = person.FirstName,
                       LastName = person.LastName,
                       FullName = person.FullName,
                       BillingAccountId = billingAccount.Id,
                       Email = emailInformationLeft != null ? emailInformationLeft.Information : null,
                       PhoneNumber = phoneInformationLeft != null ? phoneInformationLeft.Information : null
                   }).FirstOrDefault();
        }


        public HeaderPersonDto GetHeaderByBillingInvoiceId(Guid billingInvoiceId)
        {
            return (from billingInvoice in this.Context.BillingInvoices.AsNoTracking()
                    join person in this.Context.Persons on billingInvoice.PersonHeaderId equals person.Id

                    join emailInformation in this.Context.ContactInformations.AsNoTracking() on
                    new { p1 = person.Id, p2 = Convert.ToInt32(ContactInformationTypeEnum.Email) } equals
                    new { p1 = emailInformation.OwnerId, p2 = emailInformation.ContactInformationTypeId } into ri
                    from emailInformationLeft in ri.DefaultIfEmpty()

                    join phoneInformation in this.Context.ContactInformations.AsNoTracking() on
                    new { p3 = person.Id, p4 = Convert.ToInt32(ContactInformationTypeEnum.PhoneNumber) } equals
                    new { p3 = phoneInformation.OwnerId, p4 = phoneInformation.ContactInformationTypeId } into ri2
                    from phoneInformationLeft in ri2.DefaultIfEmpty()

                    where billingInvoice.Id == billingInvoiceId
                    select new HeaderPersonDto
                    {
                        Id = person.Id,
                        FirstName = person.FirstName,
                        LastName = person.LastName,
                        FullName = person.FullName,
                        BillingInvoiceId = billingInvoice.Id,
                        Email = emailInformationLeft != null ? emailInformationLeft.Information : null,
                        PhoneNumber = phoneInformationLeft != null ? phoneInformationLeft.Information : null
                    }).FirstOrDefault();
        }

        public CompanyClientDto GetHeaderRpsById(Guid personId)
        {
            var dto = (from person in this.Context.Persons.AsNoTracking()
                    join document in Context.Documents on person.Id equals document.OwnerId

                    join phoneInformation in this.Context.ContactInformations.AsNoTracking() on
                    new { p3 = person.Id, p4 = Convert.ToInt32(ContactInformationTypeEnum.PhoneNumber) } equals
                    new { p3 = phoneInformation.OwnerId, p4 = phoneInformation.ContactInformationTypeId } into ri2
                    from phoneInformationLeft in ri2.DefaultIfEmpty()

                       join l in Context.Locations on person.Id equals l.OwnerId into locations
                       from leftLocation in locations.DefaultIfEmpty()

                       join lc in Context.LocationCategories on leftLocation.LocationCategoryId equals lc.Id into locationCategories
                       from leftLocationCategory in locationCategories.DefaultIfEmpty()

                       join c in Context.CountrySubdivisions on leftLocation.CityId equals c.Id into city
                       from leftCity in city.DefaultIfEmpty()

                       join s in Context.CountrySubdivisions on leftCity.ParentSubdivision.Id equals s.Id into state
                       from leftState in state.DefaultIfEmpty()

                       join co in Context.CountrySubdivisions on person.CountrySubdivisionId equals co.Id into country
                       from leftCountry in country.DefaultIfEmpty()

                       from leftCityTranslation in Context.CountrySubdivisionTranslations.Where(o => leftCity.Id == o.CountrySubdivisionId)
                                                   .Take(1)
                                                   .DefaultIfEmpty()

                       from leftStateTranslation in Context.CountrySubdivisionTranslations.Where(o => leftState.Id == o.CountrySubdivisionId)
                                                   .Take(1)
                                                   .DefaultIfEmpty()

                       from leftCountryTranslation in Context.CountrySubdivisionTranslations.Where(o => leftCountry.Id == o.CountrySubdivisionId)
                                                   .Take(1)
                                                   .DefaultIfEmpty()

                       where person.Id == personId && person.PersonTypeValue == PersonTypeEnum.Header
                    select new CompanyClientDto
                    {
                        TradeName = person.FullName,
                        DocumentList = new List<DocumentDto> { new DocumentDto { DocumentInformation = document.DocumentInformation } },
                        LocationList = new List<LocationDto>
                        {
                            new LocationDto
                            {
                                StreetName = leftLocation != null ? leftLocation.StreetName : null,
                                StreetNumber = leftLocation != null ? leftLocation.StreetNumber : null,
                                AdditionalAddressDetails = leftLocation != null && leftLocation.AdditionalAddressDetails != null ? leftLocation.AdditionalAddressDetails.PadLeft(30) : null,
                                Neighborhood = leftLocation != null ? leftLocation.Neighborhood : null,
                                //AddressTranslation
                                AddressTranslation = new AddressTranslationDto
                                {
                                    CityName = leftCityTranslation != null ? leftCityTranslation.Name : "",
                                    CountryName = leftCountryTranslation != null ? leftCountryTranslation.Name : "",
                                    TwoLetterIsoCode = leftState != null ? leftState.TwoLetterIsoCode : ""
                                }
                            }
                        },
                        PhoneNumber = phoneInformationLeft.Information
                    }).FirstOrDefault();

            return dto;
        }
    }
}