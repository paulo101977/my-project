﻿using System;
using System.Collections.Generic;
using System.Linq;
using Thex.Common;
using Thex.Common.Enumerations;
using Thex.Domain.Entities;
using Thex.Domain.Interfaces.Repositories;
using Thex.Domain.JoinMap;
using Thex.Dto;
using Thex.Dto.RoomStatus;
using Thex.Infra.Context;
using Thex.Infra.ReadInterfaces;
using Thex.Kernel;
using Tnf.Dto;
using Tnf.EntityFrameworkCore;
using Tnf.EntityFrameworkCore.Repositories;
using Tnf.Localization;

namespace Thex.Infra.Repositories.Read
{
    public class RoomOccupiedReadRepository : EfCoreRepositoryBase<ThexContext, RoomOccupied>, IRoomOccupiedReadRepository
    {
        private readonly IRoomOccupiedRepository _roomOccupiedRepository;
        private readonly IPropertyParameterReadRepository _propertyParameterReadRepository;
        private readonly ILocalizationManager _localizationManager;
        private readonly IApplicationUser _applicationUser;

        public RoomOccupiedReadRepository(
            IDbContextProvider<ThexContext> dbContextProvider,
            IRoomOccupiedRepository roomOccupiedRepository,
            IPropertyParameterReadRepository propertyParameterReadRepository,
            ILocalizationManager localizationManager,
            IApplicationUser applicationUser)
            : base(dbContextProvider)
        {
            _roomOccupiedRepository = roomOccupiedRepository;
            _propertyParameterReadRepository = propertyParameterReadRepository;
            _localizationManager = localizationManager;
            _applicationUser = applicationUser;
        }



        public bool RoomIdUsed(int roomId, long reservationId)
          => Context.RoomOccupieds.Any(x => x.RoomId == roomId && x.ReservationId == reservationId && x.PropertyId == int.Parse(_applicationUser.PropertyId));




    }
}
