﻿using System;
using System.Collections.Generic;
using Thex.Domain.Entities;
using System.Linq;
using Thex.Infra.ReadInterfaces;
using Tnf.EntityFrameworkCore.Repositories;
using Tnf.EntityFrameworkCore;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Thex.Common.Extensions;
using Thex.Infra.Context;
using Thex.Dto;
using Thex.Common.Enumerations;

namespace Thex.Infra.Repositories.Read
{
    public class PropertyBaseRateReadRepository : EfCoreRepositoryBase<ThexContext, PropertyBaseRate>, IPropertyBaseRateReadRepository
    {
        public PropertyBaseRateReadRepository(
            IDbContextProvider<ThexContext> dbContextProvider)
            : base(dbContextProvider)
        {
            
        }

        public async Task<List<PropertyBaseRate>> GetAll(int mealPlanTypeId, Guid currencyId, List<DateTime> dateList, List<int> roomTypeIdList, Guid? ratePlanId = null)
        {
            var dbBaseQuery = Context
                                .PropertyBaseRates
                                
                                .AsNoTracking()
                                .Where(e => e.MealPlanTypeId == mealPlanTypeId && 
                                            roomTypeIdList.Contains(e.RoomTypeId) && 
                                            e.CurrencyId == currencyId);

            //var queryDateList = dateList.ChunkBy(800);
            //foreach (var items in queryDateList)
            //    dbBaseQuery = dbBaseQuery.Where(e =>  items.Contains(e.Date.Date));

            var minDate = dateList.Min(p => p);
            var maxDate = dateList.Max(p => p);
            
            dbBaseQuery = dbBaseQuery.Where(e => e.Date.Date >=  minDate.Date);
            dbBaseQuery = dbBaseQuery.Where(e => e.Date.Date <= maxDate.Date);

            if (ratePlanId.HasValue)
                dbBaseQuery = dbBaseQuery.Where(e => e.RatePlanId == ratePlanId.Value);
            else
                dbBaseQuery = dbBaseQuery.Where(e => e.RatePlanId == null);

            return await dbBaseQuery.ToListAsync();
        }

        public async Task<List<PropertyBaseRate>> GetAllByPeriod(int propertyId, List<int> roomTypeId, Guid? currencyId, int? mealPlanTypeId, DateTime initialDate, DateTime finalDate, bool? include = null, bool? fixedRates = null, Guid? ratePlanId = null, bool? level = null)
        {
            var dbBaseQuery = Context
                                .PropertyBaseRates
                                .AsNoTracking();

            if (level.HasValue && level.Value)
                dbBaseQuery = dbBaseQuery.Include(l => l.LevelEntity);

            if (include.HasValue && include.Value)
                dbBaseQuery = dbBaseQuery
                    .Include(e => e.Currency)
                    .Include(e => e.RoomType);

            dbBaseQuery = dbBaseQuery
                .Where(e => e.PropertyId == propertyId &&
                            roomTypeId.Contains(e.RoomTypeId) &&
                            e.Date.Date >= initialDate.Date &&
                            e.Date.Date <= finalDate.Date);

            if (mealPlanTypeId.HasValue)
                dbBaseQuery = dbBaseQuery.Where(e => e.MealPlanTypeId == mealPlanTypeId);

            if(currencyId.HasValue)
                dbBaseQuery = dbBaseQuery.Where(e => e.CurrencyId == currencyId);

            if (fixedRates.HasValue && fixedRates.Value)
                dbBaseQuery = dbBaseQuery.Where(e => e.RatePlanId != null);
            else
                dbBaseQuery = dbBaseQuery.Where(e => e.RatePlanId == null);

            if (ratePlanId.HasValue)
                dbBaseQuery = dbBaseQuery.Where(e => e.RatePlanId == ratePlanId.Value);            

            return await dbBaseQuery.ToListAsync();
        }

        public ICollection<PropertyBaseRate> GetAllWithPropertyMealPlanTypeRate(PropertyMealPlanTypeRateHeaderDto request, int propertyId)
           => Context.PropertyBaseRates.Where(pbr => pbr.PropertyId == propertyId && pbr.PropertyMealPlanTypeRate.HasValue && pbr.PropertyMealPlanTypeRate.Value
                                                     && pbr.CurrencyId == request.CurrencyId && pbr.Date.Date <= request.FinalDate.Date
                                                     && pbr.Date.Date >= request.InitialDate.Date)
                                     .ToList();
    }
}
