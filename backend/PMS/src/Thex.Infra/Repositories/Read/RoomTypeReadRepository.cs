﻿//  <copyright file="RoomTypeReadRepository.cs" company="TOTVS">
//  Copyright (c) TOTVS. All rights reserved.
//  </copyright>
namespace Thex.EntityFrameworkCore.Repositories
{
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Storage;
    using System;
    using System.Collections.Generic;
    using System.Data.Common;
    using Thex.Domain.Entities;
    using Thex.Infra.ReadInterfaces;
    using Thex.Dto.RoomTypes;
    using System.Linq;
    using Tnf.Dto;
    using Tnf.EntityFrameworkCore.Repositories;
    using Tnf.EntityFrameworkCore;
    using Thex.Dto.Base;
    using Thex.Dto;
    using System.Data.SqlClient;
    using Thex.Dto.Availability;
    using Thex.Common.Enumerations;
    using System.Threading.Tasks;
    using Thex.Infra.Context;
    using Thex.Dto.RoomType;

    // Em conversa com Marcelo e Hudson decidimos não utilizar o TNF para consultas com GetAll que precisa de Expand
    // Iremos contribuir com o Framework
    public class RoomTypeReadRepository : EfCoreRepositoryBase<ThexContext, Room>, IRoomTypeReadRepository
    {
        private readonly IPropertyParameterReadRepository _propertyParameterReadRepository;


        public RoomTypeReadRepository(IDbContextProvider<ThexContext> dbContextProvider,
            IPropertyParameterReadRepository propertyParameterReadRepository)
            : base(dbContextProvider)
        {
            _propertyParameterReadRepository = propertyParameterReadRepository;

        }


        public RoomTypeDto GetRoomTypeByRoomId(int roomId)
        {
            var room = Context.Rooms.FirstOrDefault(x => x.Id == roomId);
            return GetRoomTypeById(room.RoomTypeId);
        }

        public RoomTypeDto GetRoomTypeById(int roomTypeId)
        {
            var dbBaseQuery = Context.RoomTypes.Include(x => x.RoomTypeBedTypeList).
                Where(x => x.Id == roomTypeId).AsNoTracking().FirstOrDefault().MapTo<RoomTypeDto>();
            var dbPropertyParameters = _propertyParameterReadRepository.GetPropertyParameterForPropertyId(dbBaseQuery.PropertyId);
            if (dbPropertyParameters != null)
            {
                var ageChildrenList = dbPropertyParameters.propertyParameters.Where(x => x.FeatureGroupId == (int)FeatureGroupEnum.AgeGroupChieldren).FirstOrDefault().Parameters;
                var ageChildren1 = ageChildrenList.Where(x => x.ApplicationParameterId == (int)ApplicationParameterEnum.ParameterChildren_1).FirstOrDefault().PropertyParameterValue;
                var ageChildren2 = ageChildrenList.Where(x => x.ApplicationParameterId == (int)ApplicationParameterEnum.ParameterChildren_2).FirstOrDefault().PropertyParameterValue;
                var ageChildren3 = ageChildrenList.Where(x => x.ApplicationParameterId == (int)ApplicationParameterEnum.ParameterChildren_3).FirstOrDefault().PropertyParameterValue;


                var roomTypeDto = new RoomTypeDto
                {
                    Id = dbBaseQuery.Id,
                    Abbreviation = dbBaseQuery.Abbreviation,
                    AdultCapacity = dbBaseQuery.AdultCapacity,
                    ChildCapacity = dbBaseQuery.ChildCapacity,
                    DistributionCode = dbBaseQuery.DistributionCode,
                    FreeChildQuantity1 = dbBaseQuery.FreeChildQuantity1,
                    FreeChildQuantity2 = dbBaseQuery.FreeChildQuantity2,
                    FreeChildQuantity3 = dbBaseQuery.FreeChildQuantity3,
                    MaximumRate = dbBaseQuery.MaximumRate,
                    MinimumRate = dbBaseQuery.MinimumRate,
                    Name = dbBaseQuery.Name,
                    Order = dbBaseQuery.Order,
                    PropertyId = dbBaseQuery.PropertyId,
                    IsActive = dbBaseQuery.IsActive,
                    RoomTypeBedTypeList = dbBaseQuery.RoomTypeBedTypeList,
                    AgeChildren1 = Convert.ToInt16(ageChildren1),
                    AgeChildren2 = Convert.ToInt16(ageChildren2),
                    AgeChildren3 = Convert.ToInt16(ageChildren3),
                };

                return roomTypeDto;
            }

            return null;
        }

        public IListDto<RoomTypeDto> GetRoomTypesByPropertyId(GetAllRoomTypesDto request, int propertyId)
        {
            var dbBaseQuery = Context.RoomTypes
                    .Include(r => r.RoomTypeBedTypeList)
                    .Where(r => r.PropertyId == propertyId);

            if (!DoesNotHaveFilter(request))
            {
                dbBaseQuery =
                    dbBaseQuery
                    .Where(r => RoomTypeContainsName(r, request.SearchData) ||
                                RoomTypeContainsAbbreviation(r, request.SearchData) ||
                                RoomTypeContainsOrder(r, request.SearchData));
            }

            if (!request.All)
                dbBaseQuery = dbBaseQuery.Where(x => x.IsActive);

            if (string.IsNullOrEmpty(request.Order))
                request.Order = "order";

            var roomTypes = dbBaseQuery.OrderBy(x=>x.Order)
                ////.SkipAndTakeByRequestDto(request)
                ////.OrderByRequestDto(request)
                .ToListDto<RoomType, RoomTypeDto>(request);
            return roomTypes;
        }

        private bool DoesNotHaveFilter(GetAllRoomTypesDto request)
        {
            return request != null && string.IsNullOrEmpty(request.SearchData);
        }

        private bool RoomTypeContainsName(RoomType roomTypeDto, string searchData)
        {
            return roomTypeDto.Name.ToLower().Contains(searchData.ToLower());
        }

        private bool RoomTypeContainsAbbreviation(RoomType roomTypeDto, string searchData)
        {
            return roomTypeDto.Abbreviation.ToLower().Contains(searchData.ToLower());
        }

        private bool RoomTypeContainsOrder(RoomType roomTypeDto, string searchData)
        {
            return roomTypeDto.Order.ToString().Contains(searchData);
        }

        public IListDto<RoomTypeOverbookingDto> GetAllRoomTypesByPropertyAndPeriodDto(GetAllRoomTypesByPropertyAndPeriodDto request, int propertyId)
        {
            var roomTypeOverbookingDtoList = new List<RoomTypeOverbookingDto>();

            var query = (from roomType in Context.RoomTypes
                         where
                         roomType.PropertyId == propertyId && roomType.IsActive
                         select new RoomTypeOverbookingDto
                         {
                            Id = roomType.Id,
                            Name = roomType.Name,
                            Abbreviation = roomType.Abbreviation,
                            AdultCapacity = roomType.AdultCapacity,
                            ChildCapacity = roomType.ChildCapacity,
                         });

            query = FilterByChildCapacityQuery(request.ChildCapacity, query);
            query = FilterByAdultCapacityQuery(request.AdultCapacity, query);

            roomTypeOverbookingDtoList = query.ToList();

            foreach (var roomTypeOverbookingDto in roomTypeOverbookingDtoList)
            {
                roomTypeOverbookingDto.RoomTypeBedTypeList = GetBedTypesByRoomType(roomTypeOverbookingDto.Id);
            }

            return new ListDto<RoomTypeOverbookingDto>
            {
                Items = roomTypeOverbookingDtoList,
                HasNext = false
            };
        }

        private IQueryable<RoomTypeOverbookingDto> FilterByChildCapacityQuery(int? childCapacity, IQueryable<RoomTypeOverbookingDto> baseQuery)
        {
            if (childCapacity.HasValue)
                baseQuery = baseQuery.Where(r => r.ChildCapacity >= childCapacity.Value);
            return baseQuery;
        }

        private IQueryable<RoomTypeOverbookingDto> FilterByAdultCapacityQuery(int? adultCapacity, IQueryable<RoomTypeOverbookingDto> baseQuery)
        {
            if (adultCapacity.HasValue)
                baseQuery = baseQuery.Where(r => r.AdultCapacity >= adultCapacity.Value);
            return baseQuery;
        }

        private IQueryable<RoomTypeOverbookingDto> FilterByRoomTypeIdQuery(int? roomTypeId, IQueryable<RoomTypeOverbookingDto> baseQuery)
        {
            if (roomTypeId.HasValue)
                baseQuery = baseQuery.Where(r => r.Id == roomTypeId.Value);
            return baseQuery;
        }

        public RoomTypeOverbookingDto GetRoomTypeAndRoomWithOverbooking(PeriodDto request, int propertyId, int roomTypeId)
        {
            var resultSqlCommand = GetRoomTypeOverbookingData(request.InitialDate, request.FinalDate, propertyId, roomTypeId);

            var result = new RoomTypeOverbookingDto();
            var uniqueResult = resultSqlCommand.Result.FirstOrDefault();

            if (uniqueResult != null)
            {
                result.Id = uniqueResult.Id;
                result.Name = uniqueResult.Name;
                result.Abbreviation = uniqueResult.Abbreviation;
                result.AdultCapacity = uniqueResult.AdultCapacity;
                result.ChildCapacity = uniqueResult.ChildCapacity;
                result.Overbooking = uniqueResult.Overbooking;
                result.RoomTypeBedTypeList = GetBedTypesByRoomType(uniqueResult.Id);
                result.TotalConjugatedRooms = uniqueResult.TotalConjugatedRoom;
            }

            return result;
        }
        public async Task<List<RoomTypeOverbookingSqlDto>> GetRoomTypeOverbookingData(DateTime initialDate, DateTime finalDate, int propertyId, int? roomTypeId, byte? childCapacity = null, byte? adultCapacity = null)
        {
            var contextDatabase = Context.Database;
            var resultSqlCommand = new List<RoomTypeOverbookingSqlDto>();
            var conn = contextDatabase.GetDbConnection();

            if(conn.State == System.Data.ConnectionState.Closed)
                conn.Open();

            using (var command = conn.CreateCommand())
            {
                var query = await GetRoomTypeSqlOverbookingQuery(roomTypeId, childCapacity, adultCapacity);

                if (contextDatabase.CurrentTransaction != null)
                    command.Transaction = contextDatabase.CurrentTransaction.GetDbTransaction();

                command.CommandText = query;

                SetParametersRowTypeSqlOverbookingQuery(initialDate, finalDate, propertyId, command);

                ExecuteAndReadRowTypeSqlOverbookingQuery(command, resultSqlCommand);

            }

            return resultSqlCommand;
        }

        public async Task<List<RoomTypeOverbookingperDateSqlDto>> GetRoomTypeOverbookinperDateData(DateTime initialDate, DateTime finalDate, int propertyId, int? roomTypeId, byte? childCapacity = null, byte? adultCapacity = null)
        {
            var contextDatabase = Context.Database;
            var resultSqlCommand = new List<RoomTypeOverbookingperDateSqlDto>();
            var conn = contextDatabase.GetDbConnection();

            if (conn.State == System.Data.ConnectionState.Closed)
                conn.Open();

            using (var command = conn.CreateCommand())
            {
                var query = await GetRoomTypeSqlOverbookingperDateQuery(roomTypeId, childCapacity, adultCapacity);

                if (contextDatabase.CurrentTransaction != null)
                    command.Transaction = contextDatabase.CurrentTransaction.GetDbTransaction();

                command.CommandText = query;

                SetParametersRowTypeSqlOverbookingQuery(initialDate, finalDate, propertyId, command);

                ExecuteAndReadRowTypeSqlOverbookingperDateQuery(command, resultSqlCommand);

            }

            return resultSqlCommand;
        }

        private void ExecuteAndReadRowTypeSqlOverbookingQuery(DbCommand command, List<RoomTypeOverbookingSqlDto> resultSqlCommand)
        {
            var reader = command.ExecuteReader();

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    resultSqlCommand.Add(
                        new RoomTypeOverbookingSqlDto
                        {
                            Id = reader.GetInt32(0),
                            Abbreviation = reader.GetString(1),
                            AdultCapacity = reader.GetInt32(2),
                            ChildCapacity = reader.GetInt32(3),
                            Name = reader.GetString(4),
                            Qtd = reader.IsDBNull(5) ? (int?)null : (int?)reader.GetInt32(5),
                            Total = reader.IsDBNull(6) ? (int?)null : (int?)reader.GetInt32(6),
                            TotalConjugatedRoom = reader.GetInt32(7)
                        });
                }
            }

            reader.Dispose();
        }

        private void ExecuteAndReadRowTypeSqlOverbookingperDateQuery(DbCommand command, List<RoomTypeOverbookingperDateSqlDto> resultSqlCommand)
        {
            var reader = command.ExecuteReader();

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    resultSqlCommand.Add(
                        new RoomTypeOverbookingperDateSqlDto
                        {
                            Id = reader.GetInt32(0),
                            Abbreviation = reader.GetString(1),
                            AdultCapacity = reader.GetInt32(2),
                            ChildCapacity = reader.GetInt32(3),
                            Name = reader.GetString(4),
                            Qtd = reader.IsDBNull(5) ? null : (int?)reader.GetInt32(5),
                            Total = reader.IsDBNull(6) ? null : (int?)reader.GetInt32(6),
                            TotalConjugatedRoom = reader.GetInt32(7),
                            InitialDate = reader.IsDBNull(8) ? null : (DateTime?)reader.GetValue(8),
                            FinalDate = reader.IsDBNull(9) ? null : (DateTime?)reader.GetValue(9)
                        });
                }
            }

            reader.Dispose();
        }

        private void SetParametersRowTypeSqlOverbookingQuery(DateTime initialDate, DateTime finalDate, int propertyId, DbCommand command)
        {
            command.Parameters.Add(GetParameterRowTypeSqlOverbooking("@propertyId", propertyId));
            command.Parameters.Add(GetParameterRowTypeSqlOverbooking("@initialDate", initialDate));
            command.Parameters.Add(GetParameterRowTypeSqlOverbooking("@finalDate", finalDate));
            command.Parameters.Add(GetParameterRowTypeSqlOverbooking("@active", 1));
        }

        private SqlParameter GetParameterRowTypeSqlOverbooking(string parameterName, object value)
        {
            return new SqlParameter { ParameterName = parameterName, Value = value };
        }

        private Task<string> GetRoomTypeSqlOverbookingQuery(int? roomTypeId, byte? childCapacity, byte? adultCapacity)
        {
            var dbBaseQuery =
                " select                                                                                                                 "
                + "    roomtype.RoomTypeId,                                                                                                "
                + "    Abbreviation,                                                                                                       "
                + "    AdultCapacity,                                                                                                      "
                + "    ChildCapacity,                                                                                                      "
                + "    Name,                                                                                                               "
                + "    Qtd,                                                                                                                "
                + "    Total,                                                                                                              "
                + "     (select count(*) from room uh where uh.RoomTypeid = roomtype.RoomTypeid                                            "
                + " and uh.IsActive = 1 and uh.roomid in(select coalesce(ParentRoomId, -1) from room where PropertyId = @propertyId)) as TotalConjugatedRoom "
                + " from                                                                                                                   "
                + "    RoomType roomtype                                                                                                   "
                + "    left join                                                                                                           "
                + "       (                                                                                                                "
                + "          SELECT                                                                                                        "
                + "             COUNT(*) AS QTD,                                                                                           "
                + "             ROOMTYPEID,                                                                                                "
                + "             TOTAL                                                                                                      "
                + "          FROM                                                                                                          "
                + "             (                                                                                                          "
                + "                SELECT                                                                                                  "
                + "                   RT.RoomTypeId,                                                                                       "
                + "                   (                                                                                                    "
                + "                      select                                                                                            "
                + "                         count(*)                                                                                       "
                + "                      from                                                                                              "
                + "                         room uh                                                                                        "
                + "                      where                                                                                             "
                + "                         uh.RoomTypeid = rt.RoomTypeid                                                                  "
                + "                         and uh.IsActive = @active                                                                      "
                + "                         and uh.roomid not in                                                                           "
                + "                         (                                                                                              "
                + "                            select                                                                                      "
                + "                               coalesce(ParentRoomId, - 1)                                                              "
                + "                            from                                                                                        "
                + "                               room                                                                                     "
                + "                            where                                                                                       "
                + "                               PropertyId = @propertyId                                                                 "
                + "                         )                                                                                              "
                + "                   )                                                                                                    "
                + "                   as total                                                                                             "
                + "                FROM                                                                                                    "
                + "                   RoomType RT                                                                                          "
                + "                   INNER JOIN                                                                                           "
                + "                      ReservationItem RI                                                                                "
                + "                      ON RT.RoomTypeId = RI.ReceivedRoomTypeId                                                          "
                + "                      AND NOT EXISTS                                                                                    "
                + "                      (                                                                                                 "
                + "                         SELECT                                                                                         "
                + "                            1                                                                                           "
                + "                         FROM                                                                                           "
                + "                            Room                                                                                        "
                + "                         WHERE                                                                                          "
                + "                            ROOM.ParentRoomId = RI.RoomId                                                               "
                + "                      )                                                                                                 "
                + "                      and CONVERT(date, coalesce(ri.CheckInDate, ri.EstimatedArrivalDate)) <= CONVERT(date, @finalDate)      "
                + "                      and CONVERT(date, coalesce(ri.CheckOutDate, ri.EstimatedDepartureDate)) > CONVERT(date, @initialDate)  "
                + "                WHERE                                                                                                   "
                + "                   RT.PropertyId = @propertyId                                                                          ";

            dbBaseQuery = FilterByRoomTypeIdSqlQuery(roomTypeId, dbBaseQuery);
            dbBaseQuery = FilterByChildCapacitySqlQuery(childCapacity, dbBaseQuery);
            dbBaseQuery = FilterByAdultCapacitySqlQuery(adultCapacity, dbBaseQuery);

            dbBaseQuery = dbBaseQuery
                + "                UNION ALL                                                                                               "
                + "                SELECT                                                                                                  "
                + "                   RT.RoomTypeId,                                                                                       "
                + "                   (                                                                                                    "
                + "                      select                                                                                            "
                + "                         count(*)                                                                                       "
                + "                      from                                                                                              "
                + "                         room uh                                                                                        "
                + "                      where                                                                                             "
                + "                         uh.RoomTypeid = rt.RoomTypeid                                                                  "
                + "                         and uh.IsActive = @active                                                                      "
                + "                         and uh.roomid not in                                                                           "
                + "                         (                                                                                              "
                + "                            select                                                                                      "
                + "                               coalesce(ParentRoomId, - 1)                                                              "
                + "                            from                                                                                        "
                + "                               room                                                                                     "
                + "                            where                                                                                       "
                + "                               PropertyId = @propertyId                                                                 "
                + "                         )                                                                                              "
                + "                   )                                                                                                    "
                + "                   as total                                                                                             "
                + "                FROM                                                                                                    "
                + "                   RoomType RT                                                                                          "
                + "                   INNER JOIN                                                                                           "
                + "                      ROOM UH                                                                                           "
                + "                      ON RT.RoomTypeId = UH.RoomTypeId                                                                  "
                + "                   INNER JOIN                                                                                           "
                + "                      ReservationItem RI                                                                                "
                + "                      ON UH.ParentRoomId = RI.RoomId                                                                    "
                + "                      and CONVERT(date, coalesce(ri.CheckInDate, ri.EstimatedArrivalDate)) <= CONVERT(date, @finalDate)      "
                + "                      and CONVERT(date, coalesce(ri.CheckOutDate, ri.EstimatedDepartureDate)) > CONVERT(date, @initialDate)  "
                + "                WHERE                                                                                                   "
                + "                   RT.PropertyId = @propertyId                                                                          ";

            dbBaseQuery = FilterByRoomTypeIdSqlQuery(roomTypeId, dbBaseQuery);
            dbBaseQuery = FilterByChildCapacitySqlQuery(childCapacity, dbBaseQuery);
            dbBaseQuery = FilterByAdultCapacitySqlQuery(adultCapacity, dbBaseQuery);

            dbBaseQuery = dbBaseQuery
                + "             )                                                                                                          "
                + "             RTYPE                                                                                                      "
                + "          GROUP BY                                                                                                      "
                + "             ROOMTYPEID,                                                                                                "
                + "             total                                                                                                      "
                + "       )                                                                                                                "
                + "       rt                                                                                                               "
                + "       on roomtype.RoomTypeId = rt.RoomTypeId                                                                           "
                + " where                                                                                                                  "
                + "    IsActive = @active   and propertyid = @propertyId                                                                                                  "
                + "    AND EXISTS                                                                                                          "
                + "    (                                                                                                                   "
                + "       select                                                                                                           "
                + "          1                                                                                                             "
                + "       from                                                                                                             "
                + "          RoomTypeBedType                                                                                               "
                + "       where                                                                                                            "
                + "          roomtype.RoomTypeId = RoomTypeId                                                                              "
                + "    )                                                                                                                   ";

            if (roomTypeId.HasValue)
                dbBaseQuery = dbBaseQuery + string.Format(" and roomtype.roomtypeid = {0} ", roomTypeId.Value);

            if (childCapacity.HasValue)
                dbBaseQuery = dbBaseQuery + string.Format(" and roomtype.ChildCapacity >= {0} ", childCapacity.Value);

            if (adultCapacity.HasValue)
                dbBaseQuery = dbBaseQuery + string.Format(" and roomtype.AdultCapacity >= {0} ", adultCapacity.Value);

            Task<string> dbBaseQueryTask = Task<string>.Factory.StartNew(() => dbBaseQuery);

            return dbBaseQueryTask;
        }

        private Task<string> GetRoomTypeSqlOverbookingperDateQuery(int? roomTypeId, byte? childCapacity, byte? adultCapacity)
        {
            var dbBaseQuery =
                " select                                                                                                                 "
                + "    roomtype.RoomTypeId,                                                                                                "
                + "    Abbreviation,                                                                                                       "
                + "    AdultCapacity,                                                                                                      "
                + "    ChildCapacity,                                                                                                      "
                + "    Name,                                                                                                               "
                + "    Qtd,                                                                                                                "
                + "    Total,                                                                                                              "
                + "     (select count(*) from room uh where uh.RoomTypeid = roomtype.RoomTypeid                                            "
                + " and uh.IsActive = 1 and uh.roomid in(select coalesce(ParentRoomId, -1) from room where PropertyId = @propertyId)) as TotalConjugatedRoom, "
                + "	   InitialDate,                                                                                                        "
                + "    FinalDate                                                                                                           "
                + " from                                                                                                                   "
                + "    RoomType roomtype                                                                                                   "
                + "    left join                                                                                                           "
                + "       (                                                                                                                "
                + "          SELECT                                                                                                        "
                + "             COUNT(*) AS QTD,                                                                                           "
                + "             ROOMTYPEID,                                                                                                "
                + "             TOTAL,                                                                                                     "
                + "             InitialDate,                                                                                               "
                + "             FinalDate                                                                                                  "
                + "          FROM                                                                                                          "
                + "             (                                                                                                          "
                + "                SELECT                                                                                                  "
                + "                   RT.RoomTypeId,                                                                                       "
                + "                   (                                                                                                    "
                + "                      select                                                                                            "
                + "                         count(*)                                                                                       "
                + "                      from                                                                                              "
                + "                         room uh                                                                                        "
                + "                      where                                                                                             "
                + "                         uh.RoomTypeid = rt.RoomTypeid                                                                  "
                + "                         and uh.IsActive = @active                                                                      "
                + "                         and uh.roomid not in                                                                           "
                + "                         (                                                                                              "
                + "                            select                                                                                      "
                + "                               coalesce(ParentRoomId, - 1)                                                              "
                + "                            from                                                                                        "
                + "                               room                                                                                     "
                + "                            where                                                                                       "
                + "                               PropertyId = @propertyId                                                                 "
                + "                         )                                                                                              "
                + "                   )                                                                                                    "
                + "                   as total,                                                                                            "
                + "                   CONVERT(DATE, COALESCE(ri.checkindate,ri.estimatedarrivaldate)) AS InitialDate,                      "
                + "                   CONVERT(DATE, COALESCE(ri.checkoutdate, ri.estimateddeparturedate)) AS FinalDate                     "
                + "                FROM                                                                                                    "
                + "                   RoomType RT                                                                                          "
                + "                   INNER JOIN                                                                                           "
                + "                      ReservationItem RI                                                                                "
                + "                      ON RT.RoomTypeId = RI.ReceivedRoomTypeId                                                          "
                + "                      AND NOT EXISTS                                                                                    "
                + "                      (                                                                                                 "
                + "                         SELECT                                                                                         "
                + "                            1                                                                                           "
                + "                         FROM                                                                                           "
                + "                            Room                                                                                        "
                + "                         WHERE                                                                                          "
                + "                            ROOM.ParentRoomId = RI.RoomId                                                               "
                + "                      )                                                                                                 "
                + "                      and CONVERT(date, coalesce(ri.CheckInDate, ri.EstimatedArrivalDate)) <= CONVERT(date, @finalDate)      "
                + "                      and CONVERT(date, coalesce(ri.CheckOutDate, ri.EstimatedDepartureDate)) > CONVERT(date, @initialDate)  "
                + "                WHERE                                                                                                   "
                + "                   RT.PropertyId = @propertyId                                                                          ";

            dbBaseQuery = FilterByRoomTypeIdSqlQuery(roomTypeId, dbBaseQuery);
            dbBaseQuery = FilterByChildCapacitySqlQuery(childCapacity, dbBaseQuery);
            dbBaseQuery = FilterByAdultCapacitySqlQuery(adultCapacity, dbBaseQuery);

            dbBaseQuery = dbBaseQuery
                + "                UNION ALL                                                                                               "
                + "                SELECT                                                                                                  "
                + "                   RT.RoomTypeId,                                                                                       "
                + "                   (                                                                                                    "
                + "                      select                                                                                            "
                + "                         count(*)                                                                                       "
                + "                      from                                                                                              "
                + "                         room uh                                                                                        "
                + "                      where                                                                                             "
                + "                         uh.RoomTypeid = rt.RoomTypeid                                                                  "
                + "                         and uh.IsActive = @active                                                                      "
                + "                         and uh.roomid not in                                                                           "
                + "                         (                                                                                              "
                + "                            select                                                                                      "
                + "                               coalesce(ParentRoomId, - 1)                                                              "
                + "                            from                                                                                        "
                + "                               room                                                                                     "
                + "                            where                                                                                       "
                + "                               PropertyId = @propertyId                                                                 "
                + "                         )                                                                                              "
                + "                   )                                                                                                    "
                + "                   as total,                                                                                            "
                + "                   CONVERT(DATE,COALESCE(ri.checkindate,ri.estimatedarrivaldate)) AS InitialDate,                       "
                + "                   CONVERT(DATE,COALESCE(ri.checkoutdate,ri.estimateddeparturedate)) AS FinalDate 	                   "
                + "                FROM                                                                                                    "
                + "                   RoomType RT                                                                                          "
                + "                   INNER JOIN                                                                                           "
                + "                      ROOM UH                                                                                           "
                + "                      ON RT.RoomTypeId = UH.RoomTypeId                                                                  "
                + "                   INNER JOIN                                                                                           "
                + "                      ReservationItem RI                                                                                "
                + "                      ON UH.ParentRoomId = RI.RoomId                                                                    "
                + "                      and CONVERT(date, coalesce(ri.CheckInDate, ri.EstimatedArrivalDate)) <= CONVERT(date, @finalDate)      "
                + "                      and CONVERT(date, coalesce(ri.CheckOutDate, ri.EstimatedDepartureDate)) > CONVERT(date, @initialDate)  "
                + "                WHERE                                                                                                   "
                + "                   RT.PropertyId = @propertyId                                                                          ";

            dbBaseQuery = FilterByRoomTypeIdSqlQuery(roomTypeId, dbBaseQuery);
            dbBaseQuery = FilterByChildCapacitySqlQuery(childCapacity, dbBaseQuery);
            dbBaseQuery = FilterByAdultCapacitySqlQuery(adultCapacity, dbBaseQuery);

            dbBaseQuery = dbBaseQuery
                + "             )                                                                                                          "
                + "             RTYPE                                                                                                      "
                + "          GROUP BY                                                                                                      "
                + "             ROOMTYPEID,                                                                                                "
                + "             total,InitialDate,FinalDate                                                                               "
                + "       )                                                                                                                "
                + "       rt                                                                                                               "
                + "       on roomtype.RoomTypeId = rt.RoomTypeId                                                                           "
                + " where                                                                                                                  "
                + "    IsActive = @active   and propertyid = @propertyId                                                                                                  "
                + "    AND EXISTS                                                                                                          "
                + "    (                                                                                                                   "
                + "       select                                                                                                           "
                + "          1                                                                                                             "
                + "       from                                                                                                             "
                + "          RoomTypeBedType                                                                                               "
                + "       where                                                                                                            "
                + "          roomtype.RoomTypeId = RoomTypeId                                                                              "
                + "    )                                                                                                                   ";

            if (roomTypeId.HasValue)
                dbBaseQuery = dbBaseQuery + string.Format(" and roomtype.roomtypeid = {0} ", roomTypeId.Value);

            if (childCapacity.HasValue)
                dbBaseQuery = dbBaseQuery + string.Format(" and roomtype.ChildCapacity >= {0} ", childCapacity.Value);

            if (adultCapacity.HasValue)
                dbBaseQuery = dbBaseQuery + string.Format(" and roomtype.AdultCapacity >= {0} ", adultCapacity.Value);

            Task<string> dbBaseQueryTask = Task<string>.Factory.StartNew(() => dbBaseQuery);

            return dbBaseQueryTask;
        }

        private string FilterByChildCapacitySqlQuery(int? childCapacity, string dbBaseQuery)
        {
            if (childCapacity.HasValue)
                dbBaseQuery = dbBaseQuery + string.Format(" and RT.ChildCapacity = {0} ", childCapacity.Value);
            return dbBaseQuery;
        }

        private string FilterByAdultCapacitySqlQuery(int? adultCapacity, string dbBaseQuery)
        {
            if (adultCapacity.HasValue)
                dbBaseQuery = dbBaseQuery + string.Format(" and RT.AdultCapacity = {0} ", adultCapacity.Value);
            return dbBaseQuery;
        }

        private string FilterByRoomTypeIdSqlQuery(int? roomTypeId, string dbBaseQuery)
        {
            if (roomTypeId.HasValue)
                dbBaseQuery = dbBaseQuery + string.Format(" and RT.roomtypeid = {0} ", roomTypeId.Value);
            return dbBaseQuery;
        }

        public List<RoomTypeBedTypeDto> GetBedTypesByRoomType(int roomType)
        {
            var result = Context.RoomTypeBedTypes.Include(exp => exp.BedType).Where(exp => exp.RoomTypeId == roomType);

            return result.MapTo<List<RoomTypeBedTypeDto>>();
        }


        public List<AvailabilityRoomTypeRowDto> GetAvailabilityRoomTypeRowByRoomTypeId(int propertyId, int roomTypeId)
        {
            var parentRooms = (from parentRoom in Context.Rooms.AsNoTracking()
                               where parentRoom.PropertyId == propertyId
                      && parentRoom.ParentRoomId.HasValue
                               select parentRoom.ParentRoomId);

            return (from rt in Context.RoomTypes.AsNoTracking()
                    join r in Context.Rooms.AsNoTracking() on rt.Id equals r.RoomTypeId
                    where rt.PropertyId == propertyId &&
                    !parentRooms.Contains(r.Id)
                    group rt by new { rt.Id, rt.Name } into rtGroup
                    select new AvailabilityRoomTypeRowDto
                    {
                        Id = rtGroup.Key.Id,
                        Name = rtGroup.Key.Name,
                        Total = rtGroup.Count()
                    })
                     .ToList();
        }

        public List<AvailabilityRoomTypeRowDto> GetAvailabilityRoomTypeRow(int propertyId, int? roomTypeId = null)
        {
            #region query
            //select rt.RoomTypeId, rt.Name, count(*)
            //from roomtype rt inner
            //join room r
            //on rt.RoomTypeId = r.RoomTypeId
            //where roomid
            //not in(select ParentRoomId from room where ParentRoomId is not null and room.PropertyId = 1)
            //and rt.PropertyId = 1
            //group by rt.Name, rt.RoomTypeId
            #endregion

            var parentRooms = (from parentRoom in Context.Rooms.AsNoTracking()
                               where parentRoom.PropertyId == propertyId
                      && parentRoom.ParentRoomId.HasValue
                               select parentRoom.ParentRoomId);

            var dbBaseQuery = (from rt in Context.RoomTypes.AsNoTracking()
                               join r in Context.Rooms.AsNoTracking() on rt.Id equals r.RoomTypeId
                               where rt.PropertyId == propertyId &&
                               !parentRooms.Contains(r.Id) &&
                               r.IsActive
                               group rt by new { rt.Id, rt.Name, rt.Order } into rtGroup
                               orderby rtGroup.Key.Order
                               select new AvailabilityRoomTypeRowDto
                               {
                                   Id = rtGroup.Key.Id,
                                   Name = rtGroup.Key.Name,
                                   Total = rtGroup.Count(),
                                   Order = rtGroup.Key.Order
                               });

            if (roomTypeId.HasValue)
                dbBaseQuery.Where(e => e.Id == roomTypeId);

            return dbBaseQuery.ToList();
        }


        public async Task<IListDto<RoomTypeDto>> GetAllRoomTypesByPropertyIdAsync(int propertyId)
        {
            var dbBaseQuery = await Context.RoomTypes.AsNoTracking()
                    .Where(r => r.PropertyId == propertyId)
                    .OrderBy(r => r.Name)
                    .ToListAsync();

            var roomTypeList = new List<RoomTypeDto>();

            foreach (var item in dbBaseQuery.ToList())
                roomTypeList.Add(item.MapTo<RoomTypeDto>());

            return new ListDto<RoomTypeDto>
            {
                HasNext = false,
                Items = roomTypeList
            };
        }

        public RoomTypeDto GetRoomTypeByPropertyIdRoomId(int propertyId, int roomid)
        {
            var dbBaseQuery = Context.RoomTypes.AsNoTracking()
                    .Where(r => r.PropertyId == propertyId && r.Id == roomid)
                    .OrderBy(r => r.Name).FirstOrDefault();

            return dbBaseQuery.MapTo<RoomTypeDto>();
        }

        public async Task<List<int>> GetAllRoomTypeIdsByPropertyIdAsync(int propertyId, int? adultCapacity, List<int> childCapacity)
        {
            var dbBaseQuery = await Context.RoomTypes.AsNoTracking()
                    .Where(r => r.PropertyId == propertyId)
                    .OrderBy(r => r.Id)
                    .ToListAsync();

            if (adultCapacity.HasValue)
                dbBaseQuery = dbBaseQuery.Where(r => r.AdultCapacity >= adultCapacity.Value).ToList();

            if(childCapacity != null && childCapacity.Any())
                dbBaseQuery = dbBaseQuery.Where(r => r.ChildCapacity >= childCapacity.Count).ToList();

            return dbBaseQuery.Select(x => x.Id).ToList();
        }

        public async Task<List<AvailabilityRoomTypeRowDto>> GetAvailabilityRoomTypeRowAsync(int propertyId, int? roomTypeId = null)
        {
            var parentRooms = await (from parentRoom in Context.Rooms.AsNoTracking()
                               where parentRoom.PropertyId == propertyId
                      && parentRoom.ParentRoomId.HasValue
                               select parentRoom.ParentRoomId).ToListAsync();

            var dbBaseQuery = await (from rt in Context.RoomTypes.AsNoTracking()
                               join r in Context.Rooms.AsNoTracking() on rt.Id equals r.RoomTypeId
                               where rt.PropertyId == propertyId &&
                               !parentRooms.Contains(r.Id)
                               group rt by new { rt.Id, rt.Name, rt.Order } into rtGroup
                               orderby rtGroup.Key.Order
                               select new AvailabilityRoomTypeRowDto
                               {
                                   Id = rtGroup.Key.Id,
                                   Name = rtGroup.Key.Name,
                                   Total = rtGroup.Count(),
                                   Order = rtGroup.Key.Order
                               }).ToListAsync();

            if (roomTypeId.HasValue)
                dbBaseQuery.Where(e => e.Id == roomTypeId);

            return dbBaseQuery.ToList();
        }

        public bool DistributionCodeAvailable(string distributionCode, int propertyId, int roomTypeId)
        {
            return !Context.RoomTypes.Any(rt => rt.DistributionCode.ToLower().Equals(distributionCode.ToLower())
                                                           && rt.PropertyId == propertyId && rt.Id != roomTypeId);
        }

        public RoomType GetRoomTypeByReservationItemId(int reservationItemId)
           => Context.ReservationItems.Include(ri => ri.RequestedRoomType).FirstOrDefault(ri => ri.Id == reservationItemId)?.RequestedRoomType;

        public bool ValidateIfRoomTypeIsInCheckIn(int roomTypeId)
        {
            return Context.ReservationItems.Any(r => Context.Rooms.Where(rt => rt.RoomTypeId.Equals(roomTypeId)).Select(x => x.Id).Contains(r.RoomId.Value) 
                                                && r.ReservationItemStatusId.Equals((int)ReservationStatus.Checkin));
        }
    }
}