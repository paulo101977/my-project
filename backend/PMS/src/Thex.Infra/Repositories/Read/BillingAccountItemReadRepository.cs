﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Thex.Common.Enumerations;
using Thex.Domain.Entities;
using Thex.Dto;
using Thex.Infra.Context;
using Thex.Infra.ReadInterfaces;
using Tnf.EntityFrameworkCore;
using Tnf.EntityFrameworkCore.Repositories;

namespace Thex.EntityFrameworkCore.Repositories
{
    public class BillingAccountItemReadRepository : EfCoreRepositoryBase<ThexContext, BillingAccountItem>, IBillingAccountItemReadRepository, Domain.Interfaces.Repositories.Read.IBillingAccountItemReadRepository
    {


        public BillingAccountItemReadRepository(IDbContextProvider<ThexContext> dbContextProvider)
            : base(dbContextProvider)
        {

        }

        public decimal GetAmountBalanceByBillingAccountId(Guid billingAccountId)
        {
            return Context
                    .BillingAccountItems

                    .Where(exp => exp.BillingAccountId == billingAccountId)
                    .Select(exp => exp.Amount)
                    .Sum();
        }

        public List<BillingAccountItem> GetAllByBillingAccountId(DefaultGuidRequestDto request)
        {
            return Context
                        .BillingAccountItems

                        .Include(e => e.BillingItem)
                        .ThenInclude(e => e.BillingItemCategory)
                        .Where(e => e.BillingAccountId == request.Id)
                        .ToList();
        }

        public List<BillingAccountItem> GetAllWithChildrenByBillingAccountId(DefaultGuidRequestDto request)
        {
            return Context
                        .BillingAccountItems

                        .Include(e => e.BillingItem)
                        .Where(e => e.Id == request.Id ||
                        (e.BillingAccountItemParentId == request.Id))
                        .ToList();

        }

        public bool BillingAccountItemsAlreadyReversed(List<Guid> billingAccountItemIds)
        {
            return Context
                .BillingAccountItems

                .Any(e => billingAccountItemIds.Contains(e.Id) && e.WasReversed);
        }

        public bool BillingAccountHasBillingAccountItems(Guid billingAccountId)
        {
            return Context
                .BillingAccountItems

                .Any(e => e.BillingAccountId == billingAccountId);
        }

        public List<BillingAccountItem> GetAllWithChildrenExceptReversedItems(List<Guid> billingAccountItemIds)
        {
            return Context
                    .BillingAccountItems

                    .Where(e =>
                                (billingAccountItemIds.Contains(e.Id) ||
                                (e.BillingAccountItemParentId.HasValue && billingAccountItemIds.Contains(e.BillingAccountItemParentId.Value))) &&
                                !e.WasReversed &&
                                e.BillingAccountItemTypeId != (int)BillingAccountItemTypeEnum.BillingAccountItemTypeReversal).ToList();
        }


        public bool AnyReversalOrTaxByBillingAccountItemIdList(List<Guid> billingAccountItemIds)
        {
            return Context
                        .BillingAccountItems

                        .Include(e => e.BillingItem)
                        .Any(e => billingAccountItemIds.Contains(e.Id) &&
                                (e.WasReversed || e.BillingItem.BillingItemTypeId == (int)BillingItemTypeEnum.Tax));
        }
        public List<BillingAccountItem> GetBillingAccountItemsById(List<Guid> billingAccountItemIds)
        {
            return Context
                .BillingAccountItems

                .Where(x => billingAccountItemIds.Contains(x.Id)).ToList();
        }

        public bool AnyTaxByBillingAccountItemIdList(List<Guid> billingAccountItemIds)
        {
            return Context
                .BillingAccountItems

                .Include(e => e.BillingItem)
                .Any(e => billingAccountItemIds.Contains(e.Id) && e.BillingItem.BillingItemTypeId == (int)BillingItemTypeEnum.Tax);
        }

        public int CountBillingAccountItems(Guid billingAccountId, List<Guid> billingAccountItemIds)
        {
            return Context
                .BillingAccountItems

                .Include(e => e.BillingItem)
                .Count(e => billingAccountItemIds.Contains(e.Id) && e.BillingAccountId == billingAccountId);
        }

        public List<BillingAccountItem> GetAllWithChildrenExceptReversedItemsByBillingAccountIdList(List<Guid> billingAccountIdList)
        {
            return Context.BillingAccountItems

                            .Where(e =>
                                billingAccountIdList.Contains(e.BillingAccountId) &&
                                !e.WasReversed &&
                                e.BillingAccountItemTypeId != (int)BillingAccountItemTypeEnum.BillingAccountItemTypeReversal).ToList();
        }

        public List<BillingAccountItem> GetAllWithChildrenExceptReversedItemsByBillingAccountIdListAndItemIdList(List<Guid> billingAccountIdList, List<Guid> billingAccountItemIdList)
        {
            return Context
                .BillingAccountItems

                .Where(e =>
                                billingAccountIdList.Contains(e.BillingAccountId) &&
                                (
                                billingAccountItemIdList.Contains(e.Id) ||
                                (e.BillingAccountItemParentId.HasValue && billingAccountItemIdList.Contains(e.BillingAccountItemParentId.Value))
                                )
                                 &&
                                !e.WasReversed &&
                                e.BillingAccountItemTypeId != (int)BillingAccountItemTypeEnum.BillingAccountItemTypeReversal).ToList();
        }

        public List<BillingAccountItem> GetAllByBillingAccountId(Guid billingAccountId)
        {
            if (billingAccountId == Guid.Empty)
                return null;

            return Context
                        .BillingAccountItems

                        .Include(e => e.BillingItem)
                        .Where(e => e.BillingAccountId == billingAccountId)
                        .ToList();
        }

        public List<Guid> GetAllBillingAccountIdListByBillingAccountItemIdList(List<Guid> billingAccountItemIdList)
        {
            if (billingAccountItemIdList.Count == 0)
                return null;

            return Context
                        .BillingAccountItems

                        .Where(e => billingAccountItemIdList.Contains(e.Id))
                        .Select(e => e.BillingAccountId)
                        .ToList();
        }

        public List<BillingAccountItem> GetAllChildrenByBillingAccountParentId(Guid billingAccountItemId)
        {
            return Context
                        .BillingAccountItems

                        .Where(e => e.BillingAccountItemParentId == billingAccountItemId)
                        .ToList();
        }

        public List<BillingAccountItem> GetAllWithChildren(List<Guid> billingAccountItemIds)
        {
            return Context
                .BillingAccountItems

                .Where(e =>
                                billingAccountItemIds.Contains(e.Id) ||
                                (e.BillingAccountItemParentId.HasValue && billingAccountItemIds.Contains(e.BillingAccountItemParentId.Value))).ToList();
        }

        public bool AnyChildrenItem(List<Guid> billingAccountItemIds)
        {
            return Context.BillingAccountItems
                              .Any(e =>
                                billingAccountItemIds.Contains(e.Id) && e.BillingAccountItemParentId.HasValue);
        }

        public bool HasLinkedInvoice(Guid id)
        {
            return Context
                    .BillingAccountItems

                    .Any(e => e.Id == id && e.BillingInvoiceId.HasValue);
        }

        public bool HasLinkedInvoice(List<Guid> ids)
        {
            return Context
                    .BillingAccountItems

                    .Any(e => ids.Contains(e.Id) && e.BillingInvoiceId.HasValue);
        }

        public List<BillingAccountItem> GetAllForSetInvoiceIdByBillingAccountId(Guid billingAccountId)
        {
            if (billingAccountId == Guid.Empty)
                return null;

            return Context
                        .BillingAccountItems

                        .Include(e => e.BillingItem)
                        .Where(e => e.BillingAccountId == billingAccountId && !e.BillingInvoiceId.HasValue
                        && (e.BillingAccountItemTypeId == (int)BillingAccountItemTypeEnum.BillingAccountItemTypeDebit ||
                            e.BillingAccountItemTypeId == (int)BillingAccountItemTypeEnum.BillingAccountItemTypePartial) && !e.WasReversed
                        && e.Amount < 0)
                        .ToList();
        }

        public List<BillingAccountItemDto> GetAllByBillingInvoiceId(Guid billingInvoiceId)
        {
            return (from billingAccountItem in Context.BillingAccountItems
                    join billingItem in Context.BillingItems on billingAccountItem.BillingItemId equals billingItem.Id
                    where billingAccountItem.BillingInvoiceId == billingInvoiceId
                    select new BillingAccountItemDto
                    {
                        Id = billingAccountItem.Id,
                        Amount = billingAccountItem.Amount,
                        OriginalAmount = billingAccountItem.OriginalAmount,
                        BillingAccountId = billingAccountItem.BillingAccountId,
                        BillingAccountIdLastSource = billingAccountItem.BillingAccountIdLastSource,
                        BillingAccountItemDate = billingAccountItem.BillingAccountItemDate,
                        BillingAccountItemComments = billingAccountItem.BillingAccountItemComments,
                        BillingAccountItemParentId = billingAccountItem.BillingAccountItemParentId,
                        BillingAccountItemReasonId = billingAccountItem.BillingAccountItemReasonId,
                        BillingAccountItemObservation = billingAccountItem.BillingAccountItemObservation,
                        BillingAccountItemTypeId = billingAccountItem.BillingAccountItemTypeId,
                        BillingAccountItemTypeIdLastSource = billingAccountItem.BillingAccountItemTypeIdLastSource,
                        BillingInvoiceId = billingAccountItem.BillingInvoiceId,
                        BillingItemId = billingAccountItem.BillingItemId,
                        CheckNumber = billingAccountItem.CheckNumber,
                        CurrencyExchangeReferenceId = billingAccountItem.CurrencyExchangeReferenceId,
                        CurrencyExchangeReferenceSecId = billingAccountItem.CurrencyExchangeReferenceSecId,
                        CurrencySymbol = billingAccountItem.CurrencySymbol,
                        CurrencyId = billingAccountItem.CurrencyId,
                        DateParameter = billingAccountItem.DateParameter,
                        InstallmentsQuantity = billingAccountItem.InstallmentsQuantity,
                        IsOriginal = billingAccountItem.IsOriginal,
                        Nsu = billingAccountItem.Nsu,
                        Order = billingAccountItem.Order,
                        PartialParentBillingAccountItemId = billingAccountItem.PartialParentBillingAccountItemId,
                        PercentualAmount = billingAccountItem.PercentualAmount,
                        ServiceDescription = billingAccountItem.ServiceDescription,
                        WasReversed = billingAccountItem.WasReversed,
                        BillingItemName = billingItem.BillingItemName
                    }).ToList();
        }

        public List<BillingAccountItem> GetAllCreditWithInvoiceByBillingAccountId(Guid billingAccountId)
        {
            if (billingAccountId == Guid.Empty)
                return null;

            return Context
                        .BillingAccountItems

                        .Include(e => e.BillingItem)
                        .Where(e => e.BillingAccountId == billingAccountId &&
                               e.BillingAccountItemTypeId == (int)BillingAccountItemTypeEnum.BillingAccountItemTypeCredit)
                        .ToList();
        }

        public async Task<List<BillingAccountItem>> GetByBillingInvoiceIdAsync(Guid billingInvoiceId)
            => await Context
                    .BillingAccountItems.IgnoreQueryFilters().AsNoTracking()
                    .Where(e => e.BillingInvoiceId == billingInvoiceId)
                    .ToListAsync();

        public bool AnyDailyDebit(Guid tenantId, DateTime systemDate)
            => Context.BillingAccountItems.AsNoTracking()
                .Any(e => e.TenantId == tenantId && e.Daily.HasValue && e.Daily.Value && e.DateParameter.Value == systemDate);

        public List<BillingAccountItemCreditAmountDto> GetBillingAccountItemCreditNoteLiByInvoiceId(Guid billingInvoiceId)
        {
            var creditList = (from billingAccountItem in Context.BillingAccountItems
                              join billingItem in Context.BillingItems on billingAccountItem.BillingItemId equals billingItem.Id
                              join propertyParameter in Context.PropertyParameters on billingItem.PropertyId equals propertyParameter.PropertyId
                              join currency in Context.Currencies on Guid.Parse(propertyParameter.PropertyParameterValue) equals currency.Id
                              where
                                     propertyParameter.ApplicationParameterId == 4
                                     && billingAccountItem.BillingInvoiceId == billingInvoiceId
                                     && (billingAccountItem.Amount * -1) > 0
                                     && (!billingAccountItem.CreditAmount.HasValue || ((billingAccountItem.Amount * -1) > billingAccountItem.CreditAmount.Value))
                              select new BillingAccountItemCreditAmountDto
                              {
                                  Id = billingAccountItem.Id,
                                  BillingAccountId = billingAccountItem.BillingAccountId,
                                  CreditAmount = billingAccountItem.CreditAmount,
                                  BillingItemName = billingItem.BillingItemName,
                                  BillingInvoiceId = billingAccountItem.BillingInvoiceId,
                                  BillingAccountItemDate = billingAccountItem.BillingAccountItemDate,
                                  CurrencySymbol = currency.Symbol,
                                  DifferenceAmountAndCreditNote = billingAccountItem.CreditAmount.HasValue ? (billingAccountItem.Amount * -1) - billingAccountItem.CreditAmount.Value : (billingAccountItem.Amount * -1)
                              }).ToList();

            var discountList = (from ba in Context.BillingAccounts
                                join bai in Context.BillingAccountItems on ba.Id equals bai.BillingAccountId
                                where ba.Id == creditList.FirstOrDefault().BillingAccountId &&
                                (bai.BillingAccountItemTypeId == (int)BillingAccountItemTypeEnum.BillingAccountItemTypeDiscount) &&
                                !bai.WasReversed
                                select new BillingAccountItemCreditAmountDto
                                {
                                    Id = bai.Id,
                                    BillingAccountItemParentId = bai.BillingAccountItemParentId,
                                    DiscountAmount = bai.Amount
                                }).ToList();

            foreach (var credit in creditList)
            {
                credit.DifferenceAmountAndCreditNote =
                    credit.DifferenceAmountAndCreditNote - (discountList.Where(x => x.BillingAccountItemParentId == credit.Id)
                    .Select(d => d.DiscountAmount).FirstOrDefault());
            }

            return creditList;
        }

        public List<BillingAccountItemTourismTaxDto> GetAllAccountItemTourismTaxByAccountIdList(List<Guid> billingAccountIdList)
            =>
            (from billingAccountItem in Context.BillingAccountItems
             join billingAccount in Context.BillingAccounts on billingAccountItem.BillingAccountId equals billingAccount.Id
             where
                    billingAccountIdList.Contains(billingAccount.Id)
             select new BillingAccountItemTourismTaxDto
             {
                 Id = billingAccountItem.Id,
                 BillingAccountItemDate = billingAccountItem.BillingAccountItemDate,
                 BillingAccountId = billingAccount.Id,
                 IsTourismTax = billingAccountItem.IsTourismTax
             }).ToList();

        public BillingAccountItem GetByIdWithBillingItem(Guid id)
            => Context.BillingAccountItems.Include(bai => bai.BillingItem).FirstOrDefault(bai => bai.Id == id);

        public List<BillingAccountItem> GetAllByIdList(List<Guid> idList)
         => Context.BillingAccountItems.Where(b => idList.Contains(b.Id)).ToList();

        public List<BillingAccountItem> GetAllByBillingInvoiceIdList(List<Guid?> billingInvoiceIdList)
         => Context.BillingAccountItems.Where(b => b.BillingInvoiceId.HasValue && billingInvoiceIdList.Contains(b.BillingInvoiceId)).ToList();

        public List<Guid> GetAllIdByBillingAccountIdList(List<Guid> billingAccountIdList)
            => (from b in Context.BillingAccountItems
                where billingAccountIdList.Contains(b.BillingAccountId)
                select b.Id).ToList();

        public bool AnyByBillingAccountId(Guid billingAccountId)
            => (from b in Context.BillingAccountItems
                where b.BillingAccountId == billingAccountId
                select b.Id).Any();

        public List<BillingAccountItem> GetAllByPartialParentBillingAccountItemId(Guid partialParentBillingAccountItemId)
            => Context.BillingAccountItems.Where(b => b.PartialParentBillingAccountItemId == partialParentBillingAccountItemId).ToList();

        public List<Guid> GetAllIdByFilters(List<Guid> billingAccountIdList)
            => (from b in Context.BillingAccountItems
                where billingAccountIdList.Contains(b.BillingAccountId)
                select b.Id).ToList();
    }
}
