﻿// //  <copyright file="RoomReadRepository.cs" company="TOTVS">
// //  Copyright (c) TOTVS. All rights reserved.
// //  </copyright>
namespace Thex.EntityFrameworkCore.Repositories
{
    using Microsoft.EntityFrameworkCore;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Thex.Common;
    using Thex.Common.Enumerations;
    using Thex.Domain.Entities;
    using Thex.Domain.Interfaces.Repositories;
    using Thex.Dto;
    using Thex.Dto.Base;
    using Thex.Dto.Housekeeping;
    using Thex.Dto.Room;
    using Thex.Dto.Rooms;
    using Thex.Infra.Context;
    using Thex.Infra.ReadInterfaces;
    using Tnf.Dto;
    using Tnf.EntityFrameworkCore;
    using Tnf.EntityFrameworkCore.Repositories;
    using Tnf.Localization;

    // Em conversa com Marcelo e Hudson decidimos não utilizar o TNF para consultas com GetAll que precisa de Expand
    // Iremos contribuir com o Framework
    public class RoomReadRepository : EfCoreRepositoryBase<ThexContext, Room>, ReadInterfaces.Repositories.IRoomReadRepository
    {
        public class SimpleRoom
        {
            public int Id { get; set; }
            public int? RoomParentId { get; set; }
            public int RoomTypeId { get; set; }
        }

        private readonly IRoomTypeReadRepository _roomTypeReadRepository;
        private readonly IRoomBlockingReadRepository _roomBlockingReadRepository;
        private readonly ILocalizationManager _localizationManager;
        private readonly IRoomRepository _roomRepository;
        private readonly IPropertyParameterReadRepository _propertyParameterReadRepository;

        public RoomReadRepository(IDbContextProvider<ThexContext> dbContextProvider,
            IRoomTypeReadRepository roomTypeReadRepository,
            IRoomBlockingReadRepository roomBlockingReadRepository,
            ILocalizationManager localizationManager,
            IRoomRepository roomRepository,
            IPropertyParameterReadRepository propertyParameterReadRepository)
            : base(dbContextProvider)
        {
            _roomTypeReadRepository = roomTypeReadRepository;
            _roomBlockingReadRepository = roomBlockingReadRepository;
            _localizationManager = localizationManager;
            _roomRepository = roomRepository;
            _propertyParameterReadRepository = propertyParameterReadRepository;
        }

        public IListDto<RoomDto> GetRoomsByPropertyId(GetAllRoomsDto request, int propertyId)
        {
            var baseQuery = QueryOfGetRoomsByPropertyId(propertyId);

            if (!DoesNotHaveFilter(request))
            {
                baseQuery = baseQuery
                    .Where(r => RoomContainsRoomNumber(r, request.SearchData));
            }

            var rooms = baseQuery.OrderBy(x => x.RoomNumber.Length.ToString()).ThenBy(x => x.RoomNumber)
                .ToListDto<Room, RoomDto>(request);
            return rooms;
        }

        public IListDto<RoomDto> GetChildRoomsByParentRoomId(GetAllRoomsDto request, int parentRoomId)
        {
            var baseQuery = Context
                    .Rooms
                    .Include(r => r.ChildRoomList)

                    .Where(r => r.ParentRoomId == parentRoomId);

            if (!DoesNotHaveFilter(request))
            {
                baseQuery = baseQuery.Where(r => RoomContainsRoomNumber(r, request.SearchData));
            }

            var rooms = baseQuery
                //.SkipAndTakeByRequestDto(request)
                //.OrderByRequestDto(request)
                .ToListDto<Room, RoomDto>(request);
            return rooms;
        }

        public IListDto<RoomDto> GetParentRoomsByPropertyId(GetAllRoomsDto request, int propertyId)
        {
            var baseQuery = QueryOfGetRoomsByPropertyId(propertyId)
                .Where(r => r.ChildRoomList.Any());

            if (!DoesNotHaveFilter(request))
            {
                baseQuery = baseQuery
                    .Where(r => RoomContainsRoomNumber(r, request.SearchData));
            }

            var rooms = baseQuery
                //.SkipAndTakeByRequestDto(request)
                //.OrderByRequestDto(request)
                .ToListDto<Room, RoomDto>(request);
            return rooms;
        }

        public IListDto<RoomDto> GetRoomsWithoutChildrenByPropertyId(GetAllRoomsDto request, int propertyId)
        {
            var baseQuery = QueryOfGetRoomsByPropertyId(propertyId)
                .Where(r => !r.ChildRoomList.Any());

            if (!DoesNotHaveFilter(request))
            {
                baseQuery = baseQuery
                    .Where(r => RoomContainsRoomNumber(r, request.SearchData));
            }

            var rooms = baseQuery
                //.SkipAndTakeByRequestDto(request)
                //.OrderByRequestDto(request)
                .ToListDto<Room, RoomDto>(request);
            return rooms;
        }

        public IListDto<RoomDto> GetRoomsWithoutParentAndChildrenByPropertyId(GetAllRoomsDto request, int propertyId)
        {
            var baseQuery = QueryOfGetRoomsByPropertyId(propertyId).Where(r => r.ParentRoomId == null && !r.ChildRoomList.Any() && r.IsActive);

            if (!DoesNotHaveFilter(request))
            {
                baseQuery = baseQuery.Where(r => RoomContainsRoomNumber(r, request.SearchData));
            }

            var rooms = baseQuery
                //.SkipAndTakeByRequestDto(request)
                //.OrderByRequestDto(request)
                .ToListDto<Room, RoomDto>(request);
            return rooms;
        }

        private IQueryable<Room> QueryOfGetRoomsByPropertyId(int propertyId)
        {
            var rooms = Context.Rooms
                .Include(r => r.RoomType.RoomTypeBedTypeList)
                .Include(r => r.ChildRoomList)

                .Where(r => r.PropertyId == propertyId);

            return rooms;
        }

        private bool DoesNotHaveFilter(GetAllRoomsDto request)
        {
            return request != null && string.IsNullOrEmpty(request.SearchData);
        }

        private bool RoomContainsRoomNumber(Room roomDto, string searchData)
        {
            return roomDto.RoomNumber.Contains(searchData);
        }

        private HashSet<SimpleRoom> GetAllReservedRoomsByProperty(GetAllRoomsByPropertyAndPeriodDto request, int propertyId, int? roomTypeId = null, bool isCheckIn = false)
        {
            int[] status;

            if (!isCheckIn)
               status = new int[]
               {
                    (int)ReservationStatus.ToConfirm,
                    (int)ReservationStatus.Checkin,
                    (int)ReservationStatus.Confirmed,
                    (int)ReservationStatus.ReservationProposal
               };
            else
                status = new int[]
               {
                    (int)ReservationStatus.Checkin
               };

            // Pegam-se os quartos do hotel que possuam items de reserva no intervalo entre Data Inicial e Data Final
            // ou seja, os quartos já reservados.
            // Para isso pega-se os quartos do hotel que possuam itens de reserva
            // cuja data de checkin ou a data Estimada de chegada seja <= Data Final e
            // cuja data de checkout ou a data Estimada de saída seja > Data Inicial
            IQueryable<Room> reservedRoomsQuery = (from room in Context.Rooms
                                      join reservationItem in Context.ReservationItems
                                      on room.Id equals reservationItem.RoomId
                                      where status.Contains(reservationItem.ReservationItemStatusId) &&
                                        room.PropertyId == propertyId &&
                                        room.IsActive 
                                      select room);

            if (!isCheckIn)
            {
                reservedRoomsQuery = (from room in Context.Rooms
                                      join reservationItem in Context.ReservationItems
                                      on room.Id equals reservationItem.RoomId
                                      where status.Contains(reservationItem.ReservationItemStatusId) &&
                                        room.PropertyId == propertyId &&
                                        room.IsActive &&
                                        (reservationItem.CheckInDate ?? reservationItem.EstimatedArrivalDate).Date <= request.FinalDate.AddDays(-1).Date &&
                                        (reservationItem.CheckOutDate ?? reservationItem.EstimatedDepartureDate).Date > request.InitialDate.Date
                                      select room);
            }


            if (roomTypeId != null)
                reservedRoomsQuery = reservedRoomsQuery.Where(exp => exp.RoomTypeId == roomTypeId);

            var reservedRooms = reservedRoomsQuery.DistinctBy(r => r.Id).Select(room => new SimpleRoom
            {
                Id = room.Id,
                RoomParentId = room.ParentRoomId
            });

            return new HashSet<SimpleRoom>(reservedRooms);
        }

        // Pegam-se todos os Ids da lista
        Func<HashSet<SimpleRoom>, List<int>> GetReservedRoomIds = r => r.Select(d => d.Id).ToList();
        // Pegam-se os pais dos quartos da lista (se estes tiverem pais), pois se um filho estiver reservado o pai também o estará.
        Func<HashSet<SimpleRoom>, List<int>> GetOccupiedParentIds = r => r.Where(d => d.RoomParentId.HasValue).Select(d => d.RoomParentId.Value).ToList();
        // Pegam-se os filhos que estão reservados pelo fato de que se o pai estiver reservado o filho também estará.
        Func<HashSet<SimpleRoom>, List<int>, List<int>> GetOccupiedChildIds = (r, ids) => r.Where(d => d.RoomParentId.HasValue && ids.Contains(d.RoomParentId.Value)).Select(d => d.Id).ToList();

        public List<int> GetAllOccupiedRoomIdsByPropertyAndPeriod(GetAllRoomsByPropertyAndPeriodDto request, int propertyId, int? roomTypeId = null, bool isCheckIn = false)
        {

            var reservedRooms = GetAllReservedRoomsByProperty(request, propertyId, roomTypeId, isCheckIn);

            var reservedRoomIds = GetReservedRoomIds(reservedRooms);
            var occupiedParentIds = GetOccupiedParentIds(reservedRooms);
            var occupiedChildIds = GetOccupiedChildIds(reservedRooms, reservedRoomIds);

            List<int> ocuppiedRoomIds = new List<int>();
            ocuppiedRoomIds.AddRange(reservedRoomIds);
            ocuppiedRoomIds.AddRange(occupiedParentIds);
            ocuppiedRoomIds.AddRange(occupiedChildIds);

            return ocuppiedRoomIds;
        }

        private IQueryable<RoomDto> GetAllRoomsByPropertyAndPeriodBaseQuery(GetAllRoomsByPropertyAndPeriodDto request, int propertyId, int? roomTypeId, bool isCheckIn = false)
        {
            List<int> ocuppiedRoomIds = GetAllOccupiedRoomIdsByPropertyAndPeriod(request, propertyId, null, isCheckIn);
            List<int> blockedRoomIds = _roomBlockingReadRepository.GetAllBlockedRoomIdsByPeriod(request.InitialDate, request.FinalDate);

            // Pega-se os quartos ativos que sobraram e que não façam parte das duas listas de exclusão acima.
            var dbBaseQuery = (from room in Context.Rooms
                               join housekeepingstatus in Context.HousekeepingStatusProperties on room.HousekeepingStatusPropertyId equals housekeepingstatus.Id
                               where !ocuppiedRoomIds.Contains(room.Id) && // exceto os quartos que estão ocupados
                                     room.Property.Id == propertyId &&
                                     room.IsActive == request.IsActive &&
                                     !blockedRoomIds.Contains(room.Id)
                               select new RoomDto
                               {
                                   Building = room.Building,
                                   Floor = room.Floor,
                                   Id = room.Id,
                                   ParentRoomId = room.ParentRoomId,
                                   HousekeepingStatusPropertyId = room.HousekeepingStatusPropertyId,
                                   HousekeepingStatusName = housekeepingstatus.StatusName,
                                   Remarks = room.Remarks,
                                   RoomNumber = room.RoomNumber,
                                   Wing = room.Wing,
                                   RoomTypeId = room.RoomTypeId,
                                   IsActive = room.IsActive,
                                   PropertyId = room.PropertyId,
                                   ChildRoomTotal = room.ChildRoomTotal,
                                   Color = housekeepingstatus.Color
                               });

            if (roomTypeId != null)
                dbBaseQuery = dbBaseQuery.Where(exp => exp.RoomTypeId == roomTypeId);

            return dbBaseQuery;
        }

        public IListDto<RoomDto> GetAllRoomsByPropertyAndPeriod(GetAllRoomsByPropertyAndPeriodDto request, int propertyId, int? roomTypeId = null)
        {
            var dbBaseQuery = GetAllRoomsByPropertyAndPeriodBaseQuery(request, propertyId, roomTypeId);

            var result = dbBaseQuery.ToList();

            var resultListDto = new ListDto<RoomDto>();

            //foreach (var res in result)
            //{
            //    resultListDto.Items.Add(res.MapTo<RoomDto>());
            //}

            resultListDto.HasNext = false;
            resultListDto.Items = result;

            return resultListDto;
        }

        public List<RoomDto> GetAllRoomsByPropertyAndPeriodWithoutPagination(GetAllRoomsByPropertyAndPeriodDto request, int propertyId, int? roomTypeId = null, bool isCheckIn = false)
        {
            var result = new List<RoomDto>();

            var dbBaseQuery = GetAllRoomsByPropertyAndPeriodBaseQuery(request, propertyId, roomTypeId, isCheckIn);


            foreach (var res in dbBaseQuery.ToList())
                result.Add(res.MapTo<RoomDto>());

            return result;
        }

        public IListDto<RoomDto> GetAllAvailableByPeriodOfRoomType(PeriodDto request, int propertyId, int roomTypeId, int? reservationItemId, bool isCheckin, bool available)
        {
            var roomTypeWithRooms = _roomTypeReadRepository.GetRoomTypeAndRoomWithOverbooking(request, propertyId, roomTypeId);

            if (roomTypeWithRooms != null)
                roomTypeWithRooms.RoomList = GetAllRoomsByPropertyAndPeriodWithoutPagination(
        new GetAllRoomsByPropertyAndPeriodDto { InitialDate = request.InitialDate, FinalDate = request.FinalDate, IsActive = true },
        propertyId, roomTypeId, isCheckin);            

            if(!isCheckin || available)
                roomTypeWithRooms.RoomList = GetRoomFromReservationItem(roomTypeWithRooms.RoomList, roomTypeId, reservationItemId);

            var result = new ListDto<RoomDto>
            {
                HasNext = false,
                Items = roomTypeWithRooms?.RoomList
            };

            return result;
        }

        public async Task<int> CountByPropertyIdAsync(int propertyId)
            => await Context.Rooms.CountAsync(r => r.PropertyId == propertyId);

        private List<RoomDto> GetRoomFromReservationItem(List<RoomDto> roomList, int roomTypeId, int? reservationItemId)
        {
            if (reservationItemId.HasValue && reservationItemId != 0)
            {
                var reservationItem = Context.ReservationItems.AsNoTracking()
                .Include(x => x.Room.HousekeepingStatusProperty)
                .FirstOrDefault(x => x.Id == reservationItemId);

                if (reservationItem != null && reservationItem.Room != null && reservationItem.ReceivedRoomTypeId == roomTypeId && !roomList.Any(r => r.Id == reservationItem.RoomId))
                {
                    roomList.Add(new RoomDto
                    {
                        Building = reservationItem.Room.Building,
                        Floor = reservationItem.Room.Floor,
                        Id = reservationItem.Room.Id,
                        ParentRoomId = reservationItem.Room.ParentRoomId,
                        HousekeepingStatusPropertyId = reservationItem.Room.HousekeepingStatusPropertyId,
                        HousekeepingStatusName = reservationItem.Room.HousekeepingStatusProperty.StatusName,
                        Remarks = reservationItem.Room.Remarks,
                        RoomNumber = reservationItem.Room.RoomNumber,
                        Wing = reservationItem.Room.Wing,
                        RoomTypeId = reservationItem.Room.RoomTypeId,
                        IsActive = reservationItem.Room.IsActive,
                        PropertyId = reservationItem.Room.PropertyId,
                        ChildRoomTotal = reservationItem.Room.ChildRoomTotal,
                        Color = reservationItem.Room.HousekeepingStatusProperty.Color
                    });
                    roomList = roomList.OrderBy(x => x.RoomNumber).ToList();
                }
            }

            return roomList;
        }

        public bool RoomIsAvailableByPeriodOfRoomType(PeriodDto request, int propertyId, int roomTypeId, int roomId)
        {
            var roomTypeWithRooms = _roomTypeReadRepository.GetRoomTypeAndRoomWithOverbooking(request, propertyId, roomTypeId);

            if (roomTypeWithRooms != null)
            {
                var periodoDto = new GetAllRoomsByPropertyAndPeriodDto { InitialDate = request.InitialDate, FinalDate = request.FinalDate, IsActive = true };

                var ocuppiedRoomIds = GetAllOccupiedRoomIdsByPropertyAndPeriod(periodoDto, propertyId, roomTypeId);

                // Pega-se os quartos ativos que sobraram e que não façam parte das duas listas de exclusão acima.
                var roomIsavailable = (from room in Context.Rooms
                                       where !ocuppiedRoomIds.Contains(room.Id) && // exceto os quartos que estão ocupados
                                             room.Property.Id == propertyId &&
                                             room.IsActive == periodoDto.IsActive &&
                                             room.RoomTypeId == roomTypeId &&
                                             room.Id == roomId
                                       select room).Any();

                return roomIsavailable;
            }

            return false;
        }

        public List<ParentRoomsWithTotalChildrens> GetParentRoomsWithTotalChildrens(int propertyId)
        {
            var parentRooms = (from parentRoom in Context.Rooms.AsNoTracking()
                               where parentRoom.PropertyId == propertyId
                               && parentRoom.ParentRoomId.HasValue
                               select parentRoom.ParentRoomId);

            return (from r in Context.Rooms.AsNoTracking()
                    where r.PropertyId == propertyId &&
                    parentRooms.Contains(r.Id)
                    select new ParentRoomsWithTotalChildrens
                    {
                        Id = r.Id,
                        Name = r.RoomNumber,
                        Total = (from rt in Context.Rooms
                                 where rt.ParentRoomId == r.Id
                                 select rt).Count()
                    })
                     .ToList();

        }

        public bool AnyRoomTypeIdForRoomId(int roomTypeId, int roomId)
        {
            return (from r in Context.Rooms.AsNoTracking()
                    where r.RoomTypeId == roomTypeId &&
                    r.Id == roomId
                    select r)
                    .Any();
        }

        private string GetStatusName(int statusId)
        {
            if ((int)ReservationStatus.Checkin == statusId) return _localizationManager.GetString(AppConsts.LocalizationSourceName, RoomAvailabilityEnum.Busy.ToString());
            else return _localizationManager.GetString(AppConsts.LocalizationSourceName, RoomAvailabilityEnum.Vague.ToString());
        }

        public IListDto<GetAllHousekeepingAlterStatusDto> GetAllHousekeepingAlterStatusRoom(GetAllHousekeepingAlterStatusRoomDto request, int propertyId, DateTime systemDate)
        {
            var housekeepingStatusIdDirty = Context.HousekeepingStatuss.Where(x => x.Id == (int)HousekeepingStatusEnum.Dirty).FirstOrDefault();

            //Filtrar o reservationItem apenas pela lista abaixo
            var statusList = new List<int>()
            {
                (int)ReservationStatus.Checkin,
            };

            var dbBaseQuery = (from roomReservationItemJoinMap in _roomRepository.GetAllOccupiedRoomList()

                               join roomtype in Context.RoomTypes on roomReservationItemJoinMap.Room.RoomTypeId equals roomtype.Id

                               join housekeeping in Context.HousekeepingStatusProperties
                               on roomReservationItemJoinMap.Room.HousekeepingStatusPropertyId equals housekeeping.Id into h
                               from housekeeping in h.DefaultIfEmpty()

                                   //obter acomodações em check-in
                                   //join reservitem in Context.ReservationItems.Where(ri =>
                                   //     statusList.Contains(ri.ReservationItemStatusId))
                                   //on room.Id equals reservitem.RoomId into a
                                   //from reservitem in a.DefaultIfEmpty()

                               join roomlayout in Context.RoomLayouts on (roomReservationItemJoinMap.ReservationItem != null ? roomReservationItemJoinMap.ReservationItem.RoomLayoutId : Guid.Empty) equals roomlayout.Id into b
                               from roomlayout in b.DefaultIfEmpty()

                               where roomReservationItemJoinMap.Room.PropertyId == propertyId && roomReservationItemJoinMap.Room.IsActive
                                     && roomtype.IsActive

                               select new GetAllHousekeepingAlterStatusDto()
                               {
                                   Id = roomReservationItemJoinMap.Room.Id,
                                   HousekeepingStatusId = housekeeping != null ? housekeeping.HousekeepingStatusId : housekeepingStatusIdDirty.Id,
                                   HousekeepingStatusName = housekeeping != null ? housekeeping.StatusName : housekeepingStatusIdDirty.StatusName,
                                   Color = housekeeping != null ? housekeeping.Color : housekeepingStatusIdDirty.DefaultColor,
                                   RoomNumber = roomReservationItemJoinMap.Room.RoomNumber,
                                   RoomTypeName = roomtype.Abbreviation,
                                   CheckInDate = roomReservationItemJoinMap.ReservationItem == null ? (DateTime?)null : (roomReservationItemJoinMap.ReservationItem.CheckInDate ?? roomReservationItemJoinMap.ReservationItem.EstimatedArrivalDate),
                                   CheckOutDate = roomReservationItemJoinMap.ReservationItem == null ? (DateTime?)null : roomReservationItemJoinMap.ReservationItem.CheckOutDate ?? roomReservationItemJoinMap.ReservationItem.EstimatedDepartureDate,
                                   AdultCount = roomReservationItemJoinMap.ReservationItem != null ? roomReservationItemJoinMap.ReservationItem.AdultCount : 0,
                                   ChildCount = roomReservationItemJoinMap.ReservationItem != null ? roomReservationItemJoinMap.ReservationItem.ChildCount : 0,
                                   QuantityDouble = roomlayout != null ? roomlayout.QuantityDouble : 0,
                                   QuantitySingle = roomlayout != null ? roomlayout.QuantitySingle : 0,
                                   ExtraBadCount = roomReservationItemJoinMap.ReservationItem != null ? roomReservationItemJoinMap.ReservationItem.ExtraBedCount : 0,
                                   ExtraCribCount = roomReservationItemJoinMap.ReservationItem != null ? roomReservationItemJoinMap.ReservationItem.ExtraCribCount : 0,
                                   ReservationItemStatusId = roomReservationItemJoinMap.ReservationItem != null ? roomReservationItemJoinMap.ReservationItem.ReservationItemStatusId : 0,
                                   HousekeepingStatusPropertyId = housekeeping != null ? housekeeping.Id : Guid.Empty,
                                   HousekeepingStatusLastModificationTime = roomReservationItemJoinMap.Room.HousekeepingStatusLastModificationTime.HasValue ? roomReservationItemJoinMap.Room.HousekeepingStatusLastModificationTime.Value : DateTime.MinValue.Date
                               });

            //  dbBaseQuery = dbBaseQuery.Where(x => x.CheckInDate == null || x.CheckOutDate == null
            //|| (x.CheckInDate.Value.Date <= nowDateProperty && x.CheckOutDate.Value.Date > nowDateProperty));

            if (!String.IsNullOrEmpty(request.SearchData))
            {
                dbBaseQuery = dbBaseQuery.Where(x => x.RoomNumber.Contains(request.SearchData) || x.HousekeepingStatusName.Contains(request.SearchData) || x.StatusRoom.Contains(request.SearchData));
            }

            if (request.HousekeepingPropertyStatusId.HasValue)
            {
                dbBaseQuery = dbBaseQuery.Where(x => x.HousekeepingStatusId == (int)request.HousekeepingPropertyStatusId.Value);
            }

            var totalItemsDto = dbBaseQuery.Count();

            var resultDto = dbBaseQuery.OrderBy(x => x.RoomNumber.Length).ThenBy(x => x.RoomNumber).ToList();
            var roomsId = resultDto.Select(x => x.Id);

            var roomBlockingForPeriod = _roomBlockingReadRepository.GetAllBlockedInPeriod(systemDate, systemDate, roomsId.ToList()).Select(x => x.RoomId).ToList();
            foreach (var room in resultDto)
            {
                room.StatusRoom = room.ReservationItemStatusId > 0 ? GetStatusName(room.ReservationItemStatusId) : _localizationManager.GetString(AppConsts.LocalizationSourceName, RoomAvailabilityEnum.Vague.ToString());

                if (room.ReservationItemStatusId != (int)ReservationStatus.Checkin && roomBlockingForPeriod.Contains(room.Id))
                {
                    room.StatusRoom = _localizationManager.GetString(AppConsts.LocalizationSourceName, RoomAvailabilityEnum.Blocked.ToString());
                    room.Blocked = true;
                }
                else room.Blocked = false;
            }

            var result =
                new ListDto<GetAllHousekeepingAlterStatusDto>
                {
                    Items = resultDto,
                    HasNext = SearchReasonHasNext(
                            request,
                            totalItemsDto,
                            resultDto),
                };

            return result;


        }

        private bool SearchReasonHasNext(GetAllHousekeepingAlterStatusRoomDto request, int totalItemsDto, List<GetAllHousekeepingAlterStatusDto> resultDto)
        {
            return totalItemsDto > ((request.Page - 1) * request.PageSize) + resultDto.Count();
        }

        public IListDto<GetAllHousekeepingAlterStatusDto> GetAllRoomWithHousekeepingStatus(int propertyId)
        {
            var dbBaseQuery = (from room in Context.Rooms
                               join house in Context.HousekeepingStatusProperties on room.HousekeepingStatusPropertyId equals house.Id
                               join roomtype in Context.RoomTypes on room.RoomTypeId equals roomtype.Id
                               where room.PropertyId == propertyId
                               select new GetAllHousekeepingAlterStatusDto
                               {
                                   Id = room.Id,
                                   HousekeepingStatusPropertyId = room.HousekeepingStatusPropertyId,
                                   RoomTypeName = roomtype.Name,
                                   HousekeepingStatusName = house.StatusName,
                                   RoomNumber = room.RoomNumber
                               }).ToList();

            var result = new ListDto<GetAllHousekeepingAlterStatusDto>()
            {
                Items = dbBaseQuery,
                HasNext = false,
            };

            return result;

        }

        public List<int> GetAllOccupedRoomsByPropertyAndPeriod(DateTime initialDate, DateTime endDate, int propertyId)
        {
            var request = new GetAllRoomsByPropertyAndPeriodDto();
            request.InitialDate = initialDate;
            request.FinalDate = endDate;

            return GetAllOccupiedRoomIdsByPropertyAndPeriod(request, propertyId);
        }

        public IListDto<RoomDto> GetAllStatusCheckinRooms(GetAllRoomsByPropertyAndPeriodDto request, int propertyId)
        {
            var status = new int[]
            {
                (int)ReservationStatus.Checkin,
            };

            var roomsStatusCheckingList = (from room in Context.Rooms
                                           join reservationItem in Context.ReservationItems
                                           on room.Id equals reservationItem.RoomId                                        
                                           where room.Property.Id == propertyId && room.IsActive && status.Contains(reservationItem.ReservationItemStatusId)
                                           select room).ToList();         

            if (!string.IsNullOrEmpty(request.SearchData))            
                roomsStatusCheckingList = roomsStatusCheckingList.Where(x => x.RoomNumber.ToLower().Contains(request.SearchData.ToLower().Trim())).ToList();            

            var rooms = roomsStatusCheckingList
                 .ToListDto<Room, RoomDto>(request);

            rooms.Items.ToList().ForEach(room => room.RoomTypeAbbreviation = Context.RoomTypes.FirstOrDefault(r => r.Id == room.RoomTypeId)?.Abbreviation);

            return rooms;
        }

        public IList<Room> GetOccupiedRoomListByPropertyIdAndSystemDate(int propertyId, DateTime systemDate)
        {
            var occupiedRoomIdList = GetAllOccupedRoomsByPropertyAndPeriod(systemDate, systemDate, propertyId);
            var roomBaseQuery = from room in Context.Rooms
                                join housekeepingStatusProperty in Context.HousekeepingStatusProperties on room.HousekeepingStatusPropertyId equals housekeepingStatusProperty.Id
                                where housekeepingStatusProperty.HousekeepingStatusId != (int)HousekeepingStatusEnum.CreatedByHotel
                                && occupiedRoomIdList.Contains(room.Id)
                                && room.PropertyId == propertyId
                                select room;
            return roomBaseQuery.ToList();
        }

        public IList<Room> GetSpareRoomListByPropertyIdAndSystemDateForChangeHousekeepingStatus(int propertyId, DateTime systemDate, int numberOfDaysToChangeRoomStatus)
        {
            var occupiedRoomIdList = GetAllOccupedRoomsByPropertyAndPeriod(systemDate, systemDate, propertyId);
            var roomBaseQuery = from room in Context.Rooms
                                join housekeepingStatusProperty in Context.HousekeepingStatusProperties on room.HousekeepingStatusPropertyId equals housekeepingStatusProperty.Id
                                where housekeepingStatusProperty.HousekeepingStatusId != (int)HousekeepingStatusEnum.CreatedByHotel
                                && !occupiedRoomIdList.Contains(room.Id)
                                && room.PropertyId == propertyId
                                && (room.HousekeepingStatusLastModificationTime == null ||
                                    (room.HousekeepingStatusLastModificationTime != null
                                    && (int)systemDate.Date.Subtract(room.HousekeepingStatusLastModificationTime.GetValueOrDefault().Date).TotalDays >= numberOfDaysToChangeRoomStatus))
                                select room;
            return roomBaseQuery.ToList();
        }

        //existem quartos reservados com o mesmo id do meu exceto meu reservationitem no mesmo dia que estou 
        public bool IsAvailableExceptReservationItemId(int propertyId, int roomId, long reservationItemId)
        {
            var status = new int[] { (int)ReservationStatus.Checkin }.ToList();
            var dateNowProperty = DateTime.UtcNow.ToZonedDateTimeLoggedUser().Date;

            var anyOccupiedRoom = (from room in Context.Rooms
                                   join reservationItem in Context.ReservationItems
                                   on room.Id equals reservationItem.RoomId
                                   where room.PropertyId == propertyId && room.IsActive && room.Id == roomId
                                   && status.Contains(reservationItem.ReservationItemStatusId) && reservationItem.Id != reservationItemId
                                   select room.Id).Any();

            return !anyOccupiedRoom;
        }

        public bool ValidateIfRoomIsInCheckIn(int roomId)
        {
            return Context.ReservationItems.Any(r => r.RoomId.Equals(roomId) && r.ReservationItemStatusId.Equals((int)ReservationStatus.Checkin));                                                                                
        }

        public bool RoomTypeCouldNotBeChanged(int roomId, int roomTypeId)
        {
            var roomTypeIsChange = Context.Rooms.Any(r => r.Id.Equals(roomId) && !r.RoomTypeId.Equals(roomTypeId));
            return Context.ReservationItems.Any(r => r.RoomId.Equals(roomId) && r.ReservationItemStatusId < 3) && roomTypeIsChange;
        }

        public int? GetRoomTypeIdByRoomId(int id)
            => Context.Rooms.Select(r => new { r.Id, r.RoomTypeId}).FirstOrDefault(r => r.Id == id)?.RoomTypeId;
    }
}