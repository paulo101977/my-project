﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Thex.Domain.Entities;
using Thex.Dto;
using Thex.Infra.Context;
using Thex.Infra.ReadInterfaces;
using Tnf.Dto;
using Tnf.EntityFrameworkCore;
using Tnf.EntityFrameworkCore.Repositories;

namespace Thex.Infra.Repositories.Read
{
    public class CompanyClientChannelReadRepository : EfCoreRepositoryBase<ThexContext, CompanyClientChannel>, ICompanyClientChannelReadRepository
    {
        public CompanyClientChannelReadRepository(IDbContextProvider<ThexContext> dbContextProvider) : base(dbContextProvider)
        {

        }

        public CompanyClientChannel GetbyId(Guid id)
        {
            var companyClientChannel = Context.CompanyClientChannels.FirstOrDefault(x => x.Id == id);
            return companyClientChannel;
        }

        public ICollection<CompanyClientChannel> GetAllByCompanyClientId(Guid companyClientId)
        {
            var companyClientChannels = Context.CompanyClientChannels.Where(x => x.CompanyClientId == companyClientId).ToList();
            return companyClientChannels;
        }

        public ICollection<Guid> GetAllCompanyClientChannelIdByCompanyClientId(Guid companyClientId)
            => Context.CompanyClientChannels.Where(ccc => ccc.CompanyClientId == companyClientId).Select(ccc => ccc.Id).ToList();

        public CompanyClientChannelDto GetDtoByid(Guid id)
        {
            var dto = (from companyClientChannel in Context.CompanyClientChannels.AsNoTracking()
                       join channel in Context.Channels.AsNoTracking() on companyClientChannel.ChannelId equals channel.Id
                       join channelCode in Context.ChannelCodes.AsNoTracking() on channel.ChannelCodeId equals channelCode.Id
                       join businessSource in Context.BusinessSources.AsNoTracking() on channel.BusinessSourceId equals businessSource.Id into a
                       from businessSource in a.DefaultIfEmpty()
                       select new CompanyClientChannelDto
                       {
                           Id = companyClientChannel.Id,
                           CompanyId = companyClientChannel.CompanyId,
                           IsActive = companyClientChannel.IsActive,
                           CompanyClientId = companyClientChannel.CompanyClientId,
                           ChannelId = channel.Id,
                           ChannelCodeId = channelCode.Id,
                           ChannelCodeDescription = channelCode.ChannelCodeDescription,
                           ChannelDescription = channel.Description,
                           BusinessSourceDescription = businessSource != null ? businessSource.Description : "",
                           BusinessSourceId = businessSource != null ? businessSource.Id : 0
                        }).FirstOrDefault(x=>x.Id == id);

            return dto;
        }

        public IListDto<CompanyClientChannelDto> GetAllDtoByCompanyClientId(Guid companyClientId)
        {
            var dtoList = (from companyClientChannel in Context.CompanyClientChannels.AsNoTracking()
                       join channel in Context.Channels.AsNoTracking() on companyClientChannel.ChannelId equals channel.Id
                       join channelCode in Context.ChannelCodes.AsNoTracking() on channel.ChannelCodeId equals channelCode.Id
                       join businessSource in Context.BusinessSources.AsNoTracking() on channel.BusinessSourceId equals businessSource.Id into a
                       from businessSource in a.DefaultIfEmpty()
                       where companyClientChannel.CompanyClientId == companyClientId
                       select new CompanyClientChannelDto
                       {
                           Id = companyClientChannel.Id,
                           CompanyId = companyClientChannel.CompanyId,
                           IsActive = companyClientChannel.IsActive,
                           CompanyClientId = companyClientChannel.CompanyClientId,
                           ChannelId = channel.Id,
                           ChannelCodeId = channelCode.Id,
                           ChannelCodeName = channelCode.ChannelCode1,
                           ChannelCodeDescription = channelCode.ChannelCodeDescription,
                           ChannelDescription = channel.Description,
                           BusinessSourceDescription = businessSource != null ? businessSource.Description : "",
                           BusinessSourceId = businessSource != null ? businessSource.Id : 0
                       }).ToList();

            return new ListDto<CompanyClientChannelDto>
            {
                HasNext = false,
                Items = dtoList
            };
        }

        public bool AssociationExists(string companyId, Guid channelId)
        {
            return Context.CompanyClientChannels.Any(x => x.ChannelId == channelId && x.CompanyId.ToLower().Equals(companyId.ToLower()));
        }

        public bool AllCompanyClientChannelAreAvailable(ICollection<CompanyClientChannelDto> companyClientList)
        {
            return !Context.CompanyClientChannels.Any(ccc => companyClientList
                            .Select(cc => new { ChannelId = cc.ChannelId, CompanyId = cc.CompanyId.ToLower() })
                            .Contains(new {ChannelId = ccc.ChannelId, CompanyId = ccc.CompanyId.ToLower() }) && !companyClientList.Select(cc => cc.Id).Contains(ccc.Id));
        }
    }
}
