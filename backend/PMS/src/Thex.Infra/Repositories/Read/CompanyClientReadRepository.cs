﻿// //  <copyright file="CompanyClientReadRepository.cs" company="TOTVS">
// //  Copyright (c) TOTVS. All rights reserved.
// //  </copyright>
namespace Thex.EntityFrameworkCore.Repositories
{
    using System;
    using System.Linq;

    using Thex.Domain.Entities;
    using Thex.Infra.ReadInterfaces;
    using Thex.Dto;
    using Tnf.Dto;
    using Tnf.EntityFrameworkCore.Repositories;
    using Tnf.EntityFrameworkCore;
    using Thex.Common.Enumerations;
    using System.Collections.Generic;
    using Thex.Infra.Context;
    using Thex.Common;
    using Tnf.Localization;
    using System.Globalization;
    using Microsoft.EntityFrameworkCore;

    public class CompanyClientReadRepository : EfCoreRepositoryBase<ThexContext, CompanyClient>, ICompanyClientReadRepository
    {
        private readonly IDocumentReadRepository _documentReadRepository;
        private readonly ILocationReadRepository _locationReadRepository;
        private readonly ICompanyClientContactPersonReadRepository _companyClientContactPersonReadRepository;
        private readonly ICompanyClientChannelReadRepository _companyClientChannelReadRepository;
        private readonly ICustomerStationReadRepository _customerStationReadRepository;
        private readonly ILocalizationManager _localizationManager;

        public CompanyClientReadRepository(
            IDbContextProvider<ThexContext> dbContextProvider,
            IDocumentReadRepository documentReadRepository,
            ICompanyClientContactPersonReadRepository companyClientContactPersonReadRepository,
            ICompanyClientChannelReadRepository companyClientChannelReadRepository,
            ICustomerStationReadRepository customerStationReadRepository,
            ILocationReadRepository locationReadRepository, 
            ILocalizationManager localizationManager)
            : base(dbContextProvider)
        {
            _documentReadRepository = documentReadRepository;
            _locationReadRepository = locationReadRepository;
            _companyClientContactPersonReadRepository = companyClientContactPersonReadRepository;
            _localizationManager = localizationManager;
            _companyClientChannelReadRepository = companyClientChannelReadRepository;
            _customerStationReadRepository = customerStationReadRepository;
        }

        public IListDto<GetAllCompanyClientResultDto> GetAllByCompanyId(
            GetAllCompanyClientDto request,
            int companyId)
        {
            var dbBaseQuery = SearchCompanyClientByCompanyIdBaseQuery(request, companyId);

            var totalItemsDto = dbBaseQuery.Count();

            //var resultDto = dbBaseQuery.SkipAndTakeByRequestDto(request).OrderByRequestDto(request).ToList();
            var resultDto = dbBaseQuery.ToList();

            var result =
                new ListDto<GetAllCompanyClientResultDto>
                {
                    Items = resultDto,
                    HasNext = SearchCompanyClientHasNext(
                            request,
                            totalItemsDto,
                            resultDto)
                };

            return result;
        }

        private IQueryable<GetAllCompanyClientResultDto> SearchCompanyClientByCompanyIdBaseQuery(
            GetAllCompanyClientDto request,
            int companyId)
        {
            string naturalPerson = _localizationManager.GetString(AppConsts.LocalizationSourceName, PersonTypeEnum.Natural.ToString());
            string legalPerson = _localizationManager.GetString(AppConsts.LocalizationSourceName, PersonTypeEnum.Legal.ToString());

            //var documentTypesCountry =
            //    new int[]
            //        {
            //            Convert.ToInt32(DocumentTypeEnum.BrNaturalPersonCPF),
            //            Convert.ToInt32(DocumentTypeEnum.BrLegalPersonCNPJ)
            //        }.ToList();

            var dbBaseQuery = from client in this.Context.CompanyClients
                              join person in this.Context.Persons on client.PersonId equals person.Id
                              from document in this.Context.Documents.Where(d => d.OwnerId == person.Id).Take(1).DefaultIfEmpty()                                             
                              join documentType in this.Context.DocumentTypes on document.DocumentTypeId equals
                              documentType.Id 
                              join emailInformation in this.Context.ContactInformations on
                              new { p1 = person.Id, p2 = Convert.ToInt32(ContactInformationTypeEnum.Email) } equals
                              new { p1 = emailInformation.OwnerId, p2 = emailInformation.ContactInformationTypeId } into
                              ri
                              from emailInformationLeftRoom in ri.DefaultIfEmpty()
                              join phoneInformation in this.Context.ContactInformations on
                              new { p3 = person.Id, p4 = Convert.ToInt32(ContactInformationTypeEnum.PhoneNumber) }
                              equals
                              new { p3 = phoneInformation.OwnerId, p4 = phoneInformation.ContactInformationTypeId } into
                              ri2
                              from phoneInformationLeftRoom in ri2.DefaultIfEmpty()
                              join homePageInformation in this.Context.ContactInformations on
                              new { p5 = person.Id, p6 = Convert.ToInt32(ContactInformationTypeEnum.Website) } equals
                              new
                              {
                                  p5 = homePageInformation.OwnerId,
                                  p6 = homePageInformation.ContactInformationTypeId
                              } into ri3
                              from homePageInformationLeftRoom in ri3.DefaultIfEmpty()

                              join companyClientCategory in Context.PropertyCompanyClientCategories on client.PropertyCompanyClientCategoryId equals companyClientCategory.Id

                             

                              where client.CompanyId == companyId /* && documentTypesCountry.Contains(documentType.Id)*/
                              select new GetAllCompanyClientResultDto
                              {
                                  Id = client.Id,
                                  IsActive = client.IsActive,
                                  Type =  person.PersonType == ((char)PersonTypeEnum.Natural).ToString() ? naturalPerson : legalPerson,
                                  Name = client.TradeName,
                                  Document = documentType.Name + ' ' + document.DocumentInformation,
                                  Email = emailInformationLeftRoom != null ? emailInformationLeftRoom.Information : string.Empty,
                                  Phone = phoneInformationLeftRoom != null ? phoneInformationLeftRoom.Information : string.Empty,
                                  HomePage = homePageInformationLeftRoom != null ? homePageInformationLeftRoom.Information : "",
                                  ClientCategoryId = companyClientCategory.Id,
                                  ClientCategoryName = companyClientCategory.PropertyCompanyClientCategoryName,
                                  Address = person.LocationList.FirstOrDefault() != null ? 
                                      $"{person.LocationList.FirstOrDefault().StreetName} {person.LocationList.FirstOrDefault().StreetNumber} {person.LocationList.FirstOrDefault().Neighborhood}" :
                                      string.Empty,
                                  ExternalCode = client.ExternalCode
                              };


            if (!String.IsNullOrEmpty(request.SearchData) && request.SearchData.Count() >= 3)
            {
                dbBaseQuery = dbBaseQuery.Where(x => x.Name.Contains(request.SearchData) ||  x.Document.Contains(request.SearchData));
            }

            if (request.PropertyCompanyClientCategoryId != null && request.PropertyCompanyClientCategoryId != Guid.Empty)
            {
                dbBaseQuery = dbBaseQuery.Where(x => x.ClientCategoryId == request.PropertyCompanyClientCategoryId);
            }

            return dbBaseQuery.Distinct();
        }

        private bool SearchCompanyClientHasNext(
            GetAllCompanyClientDto request,
            int totalItemsDto,
            List<GetAllCompanyClientResultDto> resultDto)
        {
            return totalItemsDto > ((request.Page - 1) * request.PageSize) + resultDto.Count();
        }

        #region Search 

        public IListDto<GetAllCompanyClientSearchResultDto> GetAllBasedOnFilter(
            GetAllCompanyClientSearchDto request,
            int companyId)
        {
            var dbBaseQuery = (from client in Context.CompanyClients
                               join person in Context.Persons on client.PersonId equals person.Id
                               join document in Context.Documents on person.Id equals document.OwnerId
                               join documentType in Context.DocumentTypes on document.DocumentTypeId equals documentType.Id
                               where client.CompanyId == companyId && client.IsActive && client.PropertyCompanyClientCategoryId.HasValue
                                     && (document.DocumentInformation.ToLower().Replace(".", "").Replace("-","").Contains(request.SearchData.ToLower().Replace(".", "").Replace("-", ""))
                                         || client.ShortName.ToLower().Contains(request.SearchData.ToLower())
                                         || client.TradeName.ToLower().Contains(request.SearchData.ToLower()))
                               select new GetAllCompanyClientSearchResultDto
                               {
                                   Id = client.Id,
                                   Document = document.DocumentInformation,
                                   DocumentType = documentType.Name,
                                   DocumentTypeId = documentType.Id,
                                   ShortName = client.ShortName,
                                   TradeName = client.TradeName
                               });

            var totalItemsDto = dbBaseQuery.Count();

            //var resultDto = dbBaseQuery.SkipAndTakeByRequestDto(request).OrderByRequestDto(request).ToList();
            var resultDto = dbBaseQuery.ToList();

            var result =
                new ListDto<GetAllCompanyClientSearchResultDto>
                {
                    Items = resultDto,
                    HasNext = totalItemsDto
                                  > ((request.Page - 1) * request
                                         .PageSize)
                                  + resultDto.Count()
                };

            return result;
        }

        #endregion

        public CompanyClientDto GetCompanyClientDtoById(Guid id)
        {
            var companyClientDto = SearchCompanyClientByIdBaseQuery(id).ToList().FirstOrDefault();

            if (companyClientDto != null)
            {
                companyClientDto.DocumentList =
                    _documentReadRepository.GetDocumentDtoListByPersonId(companyClientDto.PersonId);

                companyClientDto.LocationList = _locationReadRepository.GetLocationDtoListByPersonId(companyClientDto.PersonId, CultureInfo.CurrentCulture.Name);

                if (companyClientDto.LocationList == null || companyClientDto.LocationList.Count == 0)
                    companyClientDto.LocationList = _locationReadRepository.GetLocationDtoListByPersonId(companyClientDto.PersonId, AppConsts.DEFAULT_CULTURE_INFO_NAME);

                companyClientDto.CompanyClientContactPersonList =
                    this._companyClientContactPersonReadRepository.GetContactDtoListByClientPersonId(companyClientDto.PersonId);
                companyClientDto.CompanyClientChannelList = _companyClientChannelReadRepository.GetAllDtoByCompanyClientId(id).Items;
                companyClientDto.CustomerStationList = _customerStationReadRepository.GetAllByCompanyClient(id).Result;
            }
            return companyClientDto;
        }

        private IQueryable<CompanyClientDto> SearchCompanyClientByIdBaseQuery(Guid id)
        {
            return from client in this.Context.CompanyClients
                   join person in this.Context.Persons on client.PersonId equals person.Id

                   join emailInformation in this.Context.ContactInformations on
                   new { p1 = person.Id, p2 = Convert.ToInt32(ContactInformationTypeEnum.Email) } equals
                   new { p1 = emailInformation.OwnerId, p2 = emailInformation.ContactInformationTypeId } into ri
                   from emailInformationLeft in ri.DefaultIfEmpty()

                   join phoneInformation in this.Context.ContactInformations on
                   new { p3 = person.Id, p4 = Convert.ToInt32(ContactInformationTypeEnum.PhoneNumber) } equals
                   new { p3 = phoneInformation.OwnerId, p4 = phoneInformation.ContactInformationTypeId } into ri2
                   from phoneInformationLeft in ri2.DefaultIfEmpty()

                   join homePageInformation in this.Context.ContactInformations on
                   new { p5 = person.Id, p6 = Convert.ToInt32(ContactInformationTypeEnum.Website) } equals
                   new { p5 = homePageInformation.OwnerId, p6 = homePageInformation.ContactInformationTypeId } into ri3
                   from homePageInformationLeft in ri3.DefaultIfEmpty()

                   join cellPhoneInformation in this.Context.ContactInformations on
                   new { p7 = person.Id, p8 = Convert.ToInt32(ContactInformationTypeEnum.CellPhoneNumber) } equals
                   new { p7 = cellPhoneInformation.OwnerId, p8 = cellPhoneInformation.ContactInformationTypeId } into
                   ri4

                   from cellPhoneInformationLeft in ri4.DefaultIfEmpty()

                 where client.Id == id
                   select new CompanyClientDto
                   {
                       Id = client.Id.ToString(),
                       IsActive = client.IsActive,
                       ShortName = client.ShortName,
                       TradeName = client.TradeName,
                       PersonType = person.PersonType[0],
                       Email = emailInformationLeft != null ? emailInformationLeft.Information : null,
                       HomePage = homePageInformationLeft != null ? homePageInformationLeft.Information : null,
                       PhoneNumber = phoneInformationLeft != null ? phoneInformationLeft.Information : null,
                       CellPhoneNumber = cellPhoneInformationLeft != null ? cellPhoneInformationLeft.Information : null,
                       PersonId = person.Id,
                       PropertyCompanyClientCategoryId = client.PropertyCompanyClientCategoryId,
                       ExternalCode = client.ExternalCode
                   };
        }

        public IListDto<GetAllCompanyClientContactInformationSearchResultDto> GetAllContactPersonByCompanyClientId(GetAllCompanyClientContactInformationSearchDto request, int companyId, Guid companyClientId)
        {
            var dbBaseQuery = (from client in Context.CompanyClients
                               join clientContactPersons in Context.CompanyClientContactPersons on client.Id equals clientContactPersons.CompanyClientId
                               join person in Context.Persons on clientContactPersons.PersonId equals person.Id
                               join emailInformation in this.Context.ContactInformations on
                              new { p1 = person.Id, p2 = Convert.ToInt32(ContactInformationTypeEnum.Email) } equals new { p1 = emailInformation.OwnerId, p2 = emailInformation.ContactInformationTypeId } into ri
                               from emailInformationLeftRoom in ri.DefaultIfEmpty()

                               join phoneInformation in this.Context.ContactInformations on
                               new { p3 = person.Id, p4 = Convert.ToInt32(ContactInformationTypeEnum.PhoneNumber) } equals new { p3 = phoneInformation.OwnerId, p4 = phoneInformation.ContactInformationTypeId } into ri2
                               from phoneInformationLeftRoom in ri2.DefaultIfEmpty()

                               where
                                clientContactPersons.CompanyClientId == companyClientId &&
                                client.CompanyId == companyId &&
                                (person.FirstName.ToLower().Contains(request.SearchData.ToLower()) ||
                                person.LastName.ToLower().Contains(request.SearchData.ToLower()) ||
                                person.FullName.ToLower().Contains(request.SearchData.ToLower()))
                               select new GetAllCompanyClientContactInformationSearchResultDto
                               {
                                   Id = person.Id,
                                   Name = $"{person.FirstName} {person.LastName}",
                                   PhoneNumber = phoneInformationLeftRoom.Information,
                                   Email = emailInformationLeftRoom.Information
                               });

            var totalItemsDto = dbBaseQuery.Count();

            //var resultDto = dbBaseQuery.SkipAndTakeByRequestDto(request)
            //    .OrderByRequestDto(request).ToList();
            var resultDto = dbBaseQuery.ToList();

            var result = new ListDto<GetAllCompanyClientContactInformationSearchResultDto>
            {
                Items = resultDto,
                HasNext = totalItemsDto
                   > ((request.Page - 1) * request.PageSize)
                   + resultDto.Count()
            };

            return result;
        }

        public CompanyClientDto GetCompanyClientDtoByReservationId(long id)
        {
            var dbBaseQuery = from client in this.Context.CompanyClients
                              join reservation in this.Context.Reservations on client.Id equals reservation.CompanyClientId
                              where reservation.Id == id
                              select new CompanyClientDto
                              {
                                  Id = client.Id.ToString(),
                                  IsActive = client.IsActive,
                                  ShortName = client.ShortName,
                                  TradeName = client.TradeName
                              };
            return dbBaseQuery.FirstOrDefault();
        }


        public string GetCompanyClientTradeNameById(Guid id)
        {
            var companyClient = this.Context.CompanyClients.FirstOrDefault(exp => exp.Id == id);

            return companyClient?.TradeName;
        }


        public IListDto<CompanyClientDto> GetAcquirerCompanyClient(GetAllCompanyClientDto request, int companyId)
        {
            var retorno = new ListDto<CompanyClientDto>();

            var dbBaseQuery = (from cc in Context.CompanyClients
                               where cc.CompanyId == companyId && cc.IsActive /*&& cc.IsAcquirer será removido quando entrarem categorias*/
                               select new CompanyClientDto
                               {
                                   Id = cc.Id.ToString(),
                                   CompanyId = cc.CompanyId,
                                   PersonId = cc.PersonId,
                                   ShortName = cc.ShortName,
                                   TradeName = cc.TradeName,
                                   IsActive = cc.IsActive,
                                   IsAcquirer = cc.IsAcquirer
                               });

            var totalItemsDto = dbBaseQuery.Count();

            //var resultDto = dbBaseQuery.SkipAndTakeByRequestDto(request)
            //    .OrderByRequestDto(request).ToList();
            var resultDto = dbBaseQuery.ToList();

            var result = new ListDto<CompanyClientDto>
            {
                Items = resultDto,
                HasNext = totalItemsDto
                   > ((request.Page - 1) * request.PageSize)
                   + resultDto.Count()
            };

            return result;
        }

        public string GetTradeNameById(Guid id)
        {
            return (from companyClient in Context.CompanyClients
                    where companyClient.Id == id
                    select companyClient.TradeName).FirstOrDefault();
        }

        public bool ExternalCodeIsAvailable(string externalCode, Guid companyClientId, int chainId)
        {
            return !Context.CompanyClients
                .Include(cc => cc.PropertyCompanyClientCategory).ThenInclude(pcc => pcc.Property).ThenInclude(p => p.Brand)
                .Any(cc => cc.ExternalCode.ToLower().Equals(externalCode.ToLower())
                            && cc.Id != companyClientId && cc.PropertyCompanyClientCategory.Property.Brand.ChainId == chainId);
        }

        public GetAllCompanyClientResultDto GetByIdWithDocument(Guid id)
        {
            return (from client in this.Context.CompanyClients
                              join person in this.Context.Persons on client.PersonId equals person.Id
                              from document in this.Context.Documents.Where(d => d.OwnerId == person.Id).Take(1).DefaultIfEmpty()
                              join documentType in this.Context.DocumentTypes on document.DocumentTypeId equals
                              documentType.Id
                              where client.Id == id
                              select new GetAllCompanyClientResultDto
                              {
                                  Id = client.Id,
                                  IsActive = client.IsActive,
                                  Name = client.TradeName,
                                  Document = documentType.Name + ' ' + document.DocumentInformation,
                              }).FirstOrDefault();
        }

        public bool HasCompanyClient(Guid id)
        {
            return Context.CompanyClients.Any(c => c.Id == id);
        }
    }
}
