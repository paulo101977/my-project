﻿using System;
using Thex.Domain.Entities;
using Thex.Dto;
using System.Linq;
using Thex.Infra.ReadInterfaces;
using Tnf.Dto;
using Tnf.EntityFrameworkCore.Repositories;
using Tnf.EntityFrameworkCore;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Thex.Infra.Context;
using Tnf.Localization;
using Thex.Common;
using Thex.Common.Enumerations;
using System.Collections.Generic;

namespace Thex.Infra.Repositories.Read
{
    public class PropertyMealPlanTypeReadRepository : EfCoreRepositoryBase<ThexContext, PropertyMealPlanType>, IPropertyMealPlanTypeReadRepository
    {
        private readonly ILocalizationManager _localizationManager;

        public PropertyMealPlanTypeReadRepository(
            IDbContextProvider<ThexContext> dbContextProvider, ILocalizationManager localizationManager)
            : base(dbContextProvider)
        {
            _localizationManager = localizationManager;
        }

        public ListDto<PropertyMealPlanTypeDto> GetAllPropertyMealPlanTypeByPropertyWithApplicationMealPlanType(int propertyId, bool all = false)
        {
            var dbBaseQuery = (from mealPlanType in Context.MealPlanTypes
                               join propertyMealPlanType in Context.PropertyMealPlanTypes.Where(e => e.PropertyId == propertyId) on mealPlanType.Id equals propertyMealPlanType.MealPlanTypeId into a
                               from propertyMealPlanType in a.DefaultIfEmpty()
                               select new PropertyMealPlanTypeDto
                               {
                                   Id = propertyMealPlanType != null ? propertyMealPlanType.Id : Guid.Empty,
                                   PropertyMealPlanTypeName = propertyMealPlanType != null ? propertyMealPlanType.PropertyMealPlanTypeName : mealPlanType.MealPlanTypeName,
                                   PropertyMealPlanTypeCode = propertyMealPlanType != null ? propertyMealPlanType.PropertyMealPlanTypeCode : mealPlanType.MealPlanTypeCode,
                                   MealPlanTypeId = propertyMealPlanType != null ? propertyMealPlanType.MealPlanTypeId : mealPlanType.Id,
                                   PropertyId = propertyMealPlanType != null ? propertyMealPlanType.PropertyId : propertyId,
                                   IsFirstView = propertyMealPlanType != null ? false : true,
                                   IsActive = propertyMealPlanType.IsActive
                               });
            var resultDto = !all ? dbBaseQuery.Where(x => x.IsActive == true).ToList() : dbBaseQuery.ToList();

            foreach (var account in resultDto)
                account.MealPlanTypeName = _localizationManager.GetString(AppConsts.LocalizationSourceName, ((MealPlanTypeEnum)account.MealPlanTypeId).ToString());

            var result =
                new ListDto<PropertyMealPlanTypeDto>
                {
                    Items = resultDto,
                    HasNext = false,

                };

            return result;
        }

        public async Task<ListDto<PropertyMealPlanTypeDto>> GetAllPropertyMealPlanTypeAsync(int propertyId, bool all = false)
        {
            var dbBaseQuery = await (from mealPlanType in Context.MealPlanTypes
                                     join propertyMealPlanType in Context.PropertyMealPlanTypes.Where(e => e.PropertyId == propertyId) on mealPlanType.Id equals propertyMealPlanType.MealPlanTypeId into a
                                     from propertyMealPlanType in a.DefaultIfEmpty()
                                     select new PropertyMealPlanTypeDto
                                     {
                                         Id = propertyMealPlanType != null ? propertyMealPlanType.Id : Guid.Empty,
                                         PropertyMealPlanTypeName = propertyMealPlanType != null ? propertyMealPlanType.PropertyMealPlanTypeName : mealPlanType.MealPlanTypeName,
                                         PropertyMealPlanTypeCode = propertyMealPlanType != null ? propertyMealPlanType.PropertyMealPlanTypeCode : mealPlanType.MealPlanTypeCode,
                                         MealPlanTypeId = propertyMealPlanType != null ? propertyMealPlanType.MealPlanTypeId : mealPlanType.Id,
                                         PropertyId = propertyMealPlanType != null ? propertyMealPlanType.PropertyId : propertyId,
                                         IsFirstView = propertyMealPlanType != null ? false : true,
                                         IsActive = propertyMealPlanType.IsActive
                                     }).ToListAsync();
            var resultDto = !all ? dbBaseQuery.Where(x => x.IsActive == true).ToList() : dbBaseQuery.ToList();
            var result =
               new ListDto<PropertyMealPlanTypeDto>
               {
                   Items = resultDto,
                   HasNext = false,
               };

            return result;
        }

        public ListDto<PropertyMealPlanTypeDto> GetAllPropertyMealPlanTypeByProperty(int propertyId)
        {
            var dbBaseQuery = GetAllPropertyMealPlanTypeByPropertyBaseQuery(propertyId);

            var resultDto = dbBaseQuery.ToList();

            foreach (var account in resultDto)
                account.MealPlanTypeName = _localizationManager.GetString(AppConsts.LocalizationSourceName, ((MealPlanTypeEnum)account.MealPlanTypeId).ToString());

            var result =
                new ListDto<PropertyMealPlanTypeDto>
                {
                    Items = resultDto,
                    HasNext = false,

                };

            return result;
        }

        public async Task<IListDto<PropertyMealPlanTypeDto>> GetAllPropertyMealPlanTypeByPropertyAsync(int propertyId)
        {
            var dbBaseQuery = GetAllPropertyMealPlanTypeByPropertyBaseQuery(propertyId);

            var resultDto = await dbBaseQuery.ToListAsync();

            foreach (var account in resultDto)
                account.MealPlanTypeName = _localizationManager.GetString(AppConsts.LocalizationSourceName, ((MealPlanTypeEnum)account.MealPlanTypeId).ToString());

            var result =
                new ListDto<PropertyMealPlanTypeDto>
                {
                    Items = resultDto,
                    HasNext = false,

                };

            return result;
        }

        public ICollection<int> GetAllMealPlanTypeIdListByPropertyId(int propertyId)
            => Context.PropertyMealPlanTypes.Where(pmpt => pmpt.PropertyId == propertyId).Select(pmpt => pmpt.MealPlanTypeId).ToList();

        private IQueryable<PropertyMealPlanTypeDto> GetAllPropertyMealPlanTypeByPropertyBaseQuery(int propertyId)
        {
            return (from mealPlanType in Context.MealPlanTypes
                    join propertyMealPlanType in Context.PropertyMealPlanTypes on mealPlanType.Id equals propertyMealPlanType.MealPlanTypeId
                    where propertyMealPlanType.PropertyId == propertyId && propertyMealPlanType.IsActive.Value
                    select new PropertyMealPlanTypeDto
                    {
                        Id = propertyMealPlanType.Id,
                        PropertyMealPlanTypeName = propertyMealPlanType != null ? propertyMealPlanType.PropertyMealPlanTypeName : mealPlanType.MealPlanTypeName,
                        PropertyMealPlanTypeCode = propertyMealPlanType != null ? propertyMealPlanType.PropertyMealPlanTypeCode : mealPlanType.MealPlanTypeCode,
                        MealPlanTypeId = propertyMealPlanType != null ? propertyMealPlanType.MealPlanTypeId : mealPlanType.Id,
                        PropertyId = propertyMealPlanType != null ? propertyMealPlanType.PropertyId : propertyId
                    });
        }

        public MealPlanTypeDto GetPropertyMealPlanTypeByMealTypeId(int mealTypeId, int propertyId)
        {
            return (from mealPlanType in Context.MealPlanTypes
                     join propertyMealPlanType in Context.PropertyMealPlanTypes on mealPlanType.Id equals propertyMealPlanType.MealPlanTypeId
                     where propertyMealPlanType.PropertyId == propertyId && propertyMealPlanType.MealPlanTypeId == mealTypeId

                     select new MealPlanTypeDto
                     {
                         Id = mealPlanType.Id,
                         MealPlanTypeName = propertyMealPlanType.PropertyMealPlanTypeName != null ? propertyMealPlanType.PropertyMealPlanTypeName :mealPlanType.MealPlanTypeName ,
                         MealPlanTypeCode = propertyMealPlanType != null ? propertyMealPlanType.PropertyMealPlanTypeCode : mealPlanType.MealPlanTypeCode,

                     }).FirstOrDefault();
        }

        public bool ExistsPropertyMealPlanTypeActive(Guid id)
          => Context.PropertyMealPlanTypes.Any(pmpt => (pmpt.IsActive.HasValue && pmpt.IsActive.Value) && pmpt.Id != id);
    }
}
