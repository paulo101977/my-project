﻿using Thex.Domain.Entities;
using Thex.Dto;
using Thex.Infra.Context;
using Thex.Infra.ReadInterfaces;
using Tnf.EntityFrameworkCore;
using Tnf.EntityFrameworkCore.Repositories;
using System.Linq;
using Thex.Kernel;
using System;

namespace Thex.Infra.Repositories.Read
{
    public class TourismTaxReadRepository : EfCoreRepositoryBase<ThexContext, TourismTax>, ITourismTaxReadRepository
    {
        private readonly IApplicationUser _applicationUser;

        public TourismTaxReadRepository(
            IDbContextProvider<ThexContext> dbContextProvider,
            IApplicationUser applicationUser)
            : base(dbContextProvider)
        {
            _applicationUser = applicationUser;
        }

        public TourismTaxDto GetByPropertyId()
        {
            return (from tourismTax in Context.TourismTaxs
                            where tourismTax.PropertyId == int.Parse(_applicationUser.PropertyId)
                            select new TourismTaxDto
                            {
                                Id = tourismTax.Id,
                                IsActive = tourismTax.IsActive,
                                BillingItemId = tourismTax.BillingItemId,
                                Amount = tourismTax.Amount,
                                NumberOfNights = tourismTax.NumberOfNights,
                                IsTotalOfNights = tourismTax.IsTotalOfNights,
                                IsIntegratedFiscalDocument = tourismTax.IsIntegratedFiscalDocument,
                                LaunchType = (int?)tourismTax.LaunchType,
                            }).FirstOrDefault();
        }

        public Guid GetIdByBillintItemId(int billingItemId)
        {
            return (from tourismTax in Context.TourismTaxs
                    where tourismTax.BillingItemId == billingItemId
                    select tourismTax.Id).FirstOrDefault();
        }
    }
}
