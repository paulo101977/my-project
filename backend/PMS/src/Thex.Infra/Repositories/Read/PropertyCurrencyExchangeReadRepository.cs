﻿using System;
using System.Collections.Generic;
using System.Linq;
using Thex.Common.Enumerations;
using Thex.Domain.Entities;
using Thex.Dto.PropertyCurrencyExchange;
using Thex.Infra.ReadInterfaces;
using Tnf.Dto;
using Tnf.EntityFrameworkCore.Repositories;
using Tnf.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Thex.Infra.Context;
using System.Data.SqlTypes;
using Thex.Kernel;
using NodaTime;

namespace Thex.Infra.Repositories.Read
{
    public class PropertyCurrencyExchangeReadRepository : EfCoreRepositoryBase<ThexContext, PropertyCurrencyExchange>, IPropertyCurrencyExchangeReadRepository
    {
        private readonly IApplicationUser _applicationUser;

        public PropertyCurrencyExchangeReadRepository(
            IApplicationUser applicationUser,
            IDbContextProvider<ThexContext> dbContextProvider) : base(dbContextProvider)
        {
            _applicationUser = applicationUser;
        }

        public List<QuotationCurrentDto> GetQuotationCurrentByPropertyId(int propertyId, DateTime initialDate, DateTime endDate)
        {
            var dbBaseQuery = (from currency in Context.Currencies

                               join currencyExchange in Context.CurrencyExchanges.Where(leftCurrencyExchange => 
                                                                leftCurrencyExchange.CurrencyExchangeDate.Date <= initialDate.Date &&
                                                                leftCurrencyExchange.CurrencyExchangeDate.Date >= endDate.Date) 
                                                     on currency.Id equals currencyExchange.CurrencyId
                               into ce from leftCurrencyExchange in ce.DefaultIfEmpty()

                               join propertyCurrencyExchange in Context.PropertyCurrencyExchanges.Where(leftpropertyCurrencyExchange =>
                                                                leftpropertyCurrencyExchange.PropertyCurrencyExchangeDate.Date <= initialDate.Date &&
                                                                leftpropertyCurrencyExchange.PropertyCurrencyExchangeDate.Date >= endDate.Date)
                                                             on currency.Id equals propertyCurrencyExchange.CurrencyId
                               into a from leftpropertyCurrencyExchange in a.DefaultIfEmpty()

                               from propertyParameter in Context.PropertyParameters                                                                 

                               where propertyParameter.ApplicationParameterId == (int)ApplicationParameterEnum.ParameterDefaultCurrency && 
                                    (propertyParameter.PropertyParameterValue == leftCurrencyExchange.CurrencyId.ToString() ||
                                     propertyParameter.PropertyParameterValue == leftpropertyCurrencyExchange.CurrencyId.ToString())

                               select new QuotationCurrentDto
                               {
                                   Id = leftpropertyCurrencyExchange != null ? leftpropertyCurrencyExchange.Id : Guid.Empty,
                                   CurrencyId = currency.Id,
                                   CurrencyName = currency.CurrencyName,
                                   CurrentDate = leftpropertyCurrencyExchange != null ? leftpropertyCurrencyExchange.PropertyCurrencyExchangeDate : 
                                                 leftCurrencyExchange != null ? leftCurrencyExchange.CurrencyExchangeDate : (DateTime)SqlDateTime.MinValue,
                                   ExchangeRate = leftCurrencyExchange != null ? leftCurrencyExchange.ExchangeRate : (decimal?)null,
                                   PropertyExchangeRate = leftpropertyCurrencyExchange == null ? (decimal?)null : leftpropertyCurrencyExchange.ExchangeRate,
                                   CurrencySymbol = currency.Symbol,
                                   CreationTime = leftpropertyCurrencyExchange.CreationTime != null ? leftpropertyCurrencyExchange.CreationTime :
                                                  leftCurrencyExchange != null ? leftCurrencyExchange.CreationTime : (DateTime)SqlDateTime.MinValue
                               }).ToList();

            return dbBaseQuery;
        }

        private List<QuotationCurrentDto> CalculateQuotationByPeriod(int propertyId, DateTime? startDate, Guid currencyId, DateTime? endDate)
        {
            return CalculateQuotation(propertyId, currencyId, startDate, endDate);
        }

        private List<QuotationCurrentDto> CalculateQuotation(int propertyId, Guid currencyId, DateTime? startDate, DateTime? endDate)
        {
            var lstCurrencyExchanges = Context.CurrencyExchanges
                              .Where(e => e.CurrencyExchangeDate.Date <= startDate.Value.Date
                                    && e.CurrencyExchangeDate.Date >= endDate.Value.Date
                                    && e.CurrencyId == currencyId)
                              .Select(x => new QuotationCurrentDto
                              {
                                  Id = x.Id,
                                  CurrencyId = x.CurrencyId,
                                  CurrentDate = x.CurrencyExchangeDate.Date,
                                  ExchangeRate = x.ExchangeRate,
                                  PropertyExchangeRate = null,
                                  Origin = (int)CurrencyExchangeEnum.CurrencyExchange,
                                  PropertyId = propertyId,
                                  CreationTime = x.CreationTime
                              });

            var lstPropertyCurrencyExchange = Context.PropertyCurrencyExchanges
                        .Where(e => e.PropertyCurrencyExchangeDate.Date <= startDate.Value.Date
                            && e.PropertyCurrencyExchangeDate.Date >= endDate.Value.Date
                            && e.PropertyId == propertyId
                            && e.CurrencyId == currencyId)
                .Select(x => new QuotationCurrentDto
                {
                    Id = x.Id,
                    CurrencyId = x.CurrencyId,
                    CurrentDate = x.PropertyCurrencyExchangeDate.Date,
                    ExchangeRate = null,
                    PropertyExchangeRate = x.ExchangeRate,
                    Origin = (int)CurrencyExchangeEnum.PropertyCurrencyExchange,
                    PropertyId = propertyId,
                    CreationTime = x.CreationTime
                });


            var lstTotal = lstCurrencyExchanges.Union(lstPropertyCurrencyExchange);

            var lstCurrentDate = lstTotal
                                .Select(e => new QuotationCurrentDto
                                {
                                    CurrentDate = e.CurrentDate
                                }).DistinctBy(e => e.CurrentDate);

            var lstFinal = new List<QuotationCurrentDto>();

            foreach (var item in lstCurrentDate)
            {
                var auxAmbos = lstTotal.Where(x => x.CurrentDate == item.CurrentDate).ToList();
                QuotationCurrentDto v = new QuotationCurrentDto();

                if (auxAmbos == null) break;

                var auxPropertyCurrencyExchanges = auxAmbos.Where(x => x.Origin == 1).OrderByDescending(x => x.CreationTime).FirstOrDefault();
                var auxCurrencyExchanges = auxAmbos.Where(x => x.Origin == 2).OrderByDescending(x => x.CreationTime).FirstOrDefault();

                if (auxCurrencyExchanges != null)
                {
                    v.CurrentDate = auxCurrencyExchanges.CurrentDate;
                    v.Id = Guid.Empty;
                    v.ExchangeRate = auxCurrencyExchanges.ExchangeRate;
                    v.CurrencyId = auxCurrencyExchanges.CurrencyId;
                    v.PropertyId = propertyId;
                    v.CreationTime = auxCurrencyExchanges.CreationTime;
                }

                if (auxPropertyCurrencyExchanges != null)
                {
                    v.CurrentDate = auxPropertyCurrencyExchanges.CurrentDate;
                    v.Id = auxPropertyCurrencyExchanges.Id != null ? auxPropertyCurrencyExchanges.Id : Guid.Empty;
                    v.PropertyExchangeRate = auxPropertyCurrencyExchanges.PropertyExchangeRate;
                    v.CurrencyId = auxPropertyCurrencyExchanges.CurrencyId;
                    v.PropertyId = propertyId;
                    v.CreationTime = auxPropertyCurrencyExchanges.CreationTime;
                }

                lstFinal.Add(v);

            }

            return lstFinal;
        }

        public ListDto<QuotationCurrentDto> GetAllQuotationByPeriod(int propertyId, DateTime? startDate, Guid currencyId, DateTime? endDate)
        {
            var initial = startDate ?? DateTime.UtcNow.ToZonedDateTimeLoggedUser();
            var end = endDate ?? DateTime.UtcNow.Date.AddDays(-30).ToZonedDateTimeLoggedUser();

            var lstFinal = new List<QuotationCurrentDto>();
            var lstQuotationByPeriod = new List<QuotationCurrentDto>();

            if (!endDate.HasValue)
            {
                lstFinal = CalculateQuotationByPeriod(propertyId, initial, currencyId, end);
                lstQuotationByPeriod = GetQuotationCurrentByPropertyId(propertyId, initial, end);
            }            
            else
            {
                lstFinal = CalculateQuotationByPeriod(propertyId, end, currencyId, initial);
                lstQuotationByPeriod = GetQuotationCurrentByPropertyId(propertyId, end, initial);
            }

            CalculatePropertyCurrencyQuotationByPeriod(lstFinal, lstQuotationByPeriod);

            lstFinal = lstFinal.OrderByDescending(x => x.CurrentDate).ToList();

            return new ListDto<QuotationCurrentDto>()
            {
                HasNext = false,
                Items = lstFinal,
            };
        }

        private void CalculatePropertyCurrencyQuotationByPeriod(List<QuotationCurrentDto> lstFinal, List<QuotationCurrentDto> lstQuotationByPeriod)
        {
            lstFinal.ForEach(currencyQuotation => {
                currencyQuotation.ExchangeRate =
                     lstQuotationByPeriod.Any(l => l.CurrentDate.Date == currencyQuotation?.CurrentDate.Date &&
                                                   l.ExchangeRate != null && currencyQuotation.ExchangeRate != null) ?
                     lstQuotationByPeriod.Where(l => l.CurrentDate.Date == currencyQuotation.CurrentDate.Date)
                                         .OrderByDescending(c => c.CreationTime).FirstOrDefault()
                                         .ExchangeRate / currencyQuotation.ExchangeRate : null;
                currencyQuotation.PropertyExchangeRate =
                     lstQuotationByPeriod.Any(l => l.CurrentDate.Date == currencyQuotation?.CurrentDate.Date &&
                                                   l.PropertyExchangeRate != null && currencyQuotation.PropertyExchangeRate != null) ?
                     lstQuotationByPeriod.Where(l => l.CurrentDate.Date == currencyQuotation.CurrentDate.Date)
                                         .OrderByDescending(c => c.CreationTime).FirstOrDefault()
                                         .PropertyExchangeRate / currencyQuotation.PropertyExchangeRate : null;
                currencyQuotation.CurrencySymbol = lstQuotationByPeriod.FirstOrDefault()?.CurrencySymbol;
            });
        }

        public bool IsQuotationByPeriodList(DateTime quotationStartDate, DateTime quotationEndDate, Guid CurrencyId, int propertyId)
        {
            return Context
                .PropertyCurrencyExchanges

                .Where(x => x.PropertyCurrencyExchangeDate.Date <= quotationEndDate.Date
            && x.PropertyCurrencyExchangeDate.Date >= quotationStartDate.Date && x.CurrencyId == CurrencyId && x.PropertyId == propertyId).Any();
        }

        public List<PropertyCurrencyExchange> GetAllByIds(List<Guid> ids)
        {
            return Context.PropertyCurrencyExchanges.AsNoTracking().Where(x => ids.Contains(x.Id)).ToList();
        }

        public void DeleteByPeriodList(InsertQuotationByPeriodDto quotationByPeriod)
        {
            var vLst = Context.PropertyCurrencyExchanges.AsNoTracking()
                                      .Where(x => (x.PropertyCurrencyExchangeDate >= quotationByPeriod.StartDate
                                             && x.PropertyCurrencyExchangeDate <= quotationByPeriod.EndDate)
                                             && x.PropertyId == quotationByPeriod.PropertyId
                                             && x.CurrencyId == quotationByPeriod.CurrencyId).ToList();

            Context.PropertyCurrencyExchanges.RemoveRange(vLst);
            Context.SaveChanges();
        }

        public decimal? GetExchangeRateById(Guid id)
        {
            //problems with union and transactions
            //=> (from pce in Context.PropertyCurrencyExchanges.AsNoTracking()
            //    where pce.Id == id
            //    select new { pce.ExchangeRate })
            //.Union(
            //    from ce in Context.CurrencyExchanges.AsNoTracking()
            //    where ce.Id == id
            //    select new { ce.ExchangeRate }
            //).FirstOrDefault()?.ExchangeRate;

            var exchangeRate = (from pce in Context.PropertyCurrencyExchanges.AsNoTracking()
                                where pce.Id == id
                                select new { pce.ExchangeRate }).FirstOrDefault()?.ExchangeRate;

            if (!exchangeRate.HasValue)
                exchangeRate = (from ce in Context.CurrencyExchanges.AsNoTracking()
                                where ce.Id == id
                                select new { ce.ExchangeRate }).FirstOrDefault()?.ExchangeRate;

            return exchangeRate;
        }


        private IQueryable<QuotationCurrentDto> GetCurrencyExchangeRateBaseQuery()
            => from ce in Context.CurrencyExchanges.AsNoTracking()
               join c in Context.Currencies.AsNoTracking() on ce.CurrencyId equals c.Id
               select new QuotationCurrentDto
               {
                   Id = ce.Id,
                   CurrencyId = c.Id,
                   CurrencyName = c.CurrencyName,
                   CurrentDate = ce.CurrencyExchangeDate,
                   ExchangeRate = ce.ExchangeRate,
               };

        private IQueryable<QuotationCurrentDto> GetPropertyCurrencyExchangeRateBaseQuery()
            => from pce in Context.PropertyCurrencyExchanges.AsNoTracking()
               join c in Context.Currencies.AsNoTracking() on pce.CurrencyId equals c.Id
               select new QuotationCurrentDto
               {
                   Id = pce.Id,
                   CurrencyId = c.Id,
                   CurrencyName = c.CurrencyName,
                   CurrentDate = pce.PropertyCurrencyExchangeDate,
                   ExchangeRate = pce.ExchangeRate,
                   PropertyId = pce.PropertyId,
                   CreationTime = pce.CreationTime,
                   CreatorUserId = pce.CreatorUserId == null ? Guid.Empty : pce.CreatorUserId.Value
               };

        public List<QuotationCurrentDto> GetCurrentQuotationList(int propertyId, Guid? currencyId)
        {
            var dateTimeZone = DateTime.UtcNow.ToZonedDateTimeLoggedUser();

            var currencyExchangeBaseQuery =
                 ((
                 from p in GetPropertyCurrencyExchangeRateBaseQuery()
                 where p.PropertyId == propertyId && p.CurrentDate.Date <= dateTimeZone.Date
                 select new QuotationCurrentDto
                 {
                     Id = p.Id,
                     CurrencyId = p.CurrencyId,
                     CurrencyName = p.CurrencyName,
                     CurrentDate = p.CurrentDate,
                     ExchangeRate = p.ExchangeRate,
                     Origin = 1
                 })
                 .Union(
                    from p in GetCurrencyExchangeRateBaseQuery()
                    where p.CurrentDate.Date >= DateTime.UtcNow.AddMonths(-1).Date && p.CurrentDate.Date <= DateTime.UtcNow.Date
                    select new QuotationCurrentDto
                    {
                        Id = p.Id,
                        CurrencyId = p.CurrencyId,
                        CurrencyName = p.CurrencyName,
                        CurrentDate = p.CurrentDate,
                        ExchangeRate = p.ExchangeRate,
                        Origin = 2
                    }
                 ));

            if (currencyId.HasValue)
                currencyExchangeBaseQuery = currencyExchangeBaseQuery
                    .Where(c => c.CurrencyId == currencyId.Value);

            var currencyExchangeList = currencyExchangeBaseQuery.ToList();

            var result = new List<QuotationCurrentDto>();
            foreach (var currency in currencyExchangeList.Select(e => e.CurrencyId).Distinct())
            {
                if (result.Any(c => c.CurrencyId == currency))
                    continue;

                var propertyCurrencyExchange = currencyExchangeList.Where(e => e.CurrencyId == currency && e.Origin == 1).OrderByDescending(e => e.CurrentDate).FirstOrDefault();
                var currencyExchange = currencyExchangeList.Where(e => e.CurrencyId == currency && e.Origin == 2).OrderByDescending(e => e.CurrentDate).FirstOrDefault();

                result.Add(new QuotationCurrentDto
                {
                    Id = propertyCurrencyExchange != null ? propertyCurrencyExchange.Id : currencyExchange.Id,
                    CurrencyId = propertyCurrencyExchange != null ? propertyCurrencyExchange.CurrencyId : currencyExchange.CurrencyId,
                    CurrencyName = propertyCurrencyExchange != null ? propertyCurrencyExchange.CurrencyName : currencyExchange.CurrencyName,
                    CurrentDate = propertyCurrencyExchange != null ? propertyCurrencyExchange.CurrentDate : currencyExchange.CurrentDate,
                    ExchangeRate = propertyCurrencyExchange != null ? propertyCurrencyExchange.ExchangeRate : currencyExchange.ExchangeRate
                });
            }

            return result;
        }

        public QuotationCurrentDto GetQuotationByCurrencyId(int propertyId, Guid currencyId)
            => GetCurrentQuotationList(propertyId, currencyId).FirstOrDefault();

        public List<QuotationCurrentDto> GetPropertyQuotationByPropertyId(int propertyId, DateTime initialDate, DateTime endDate, bool isToDolar = false)
        {             
            var currencyExchangeBaseQuery =
                 
                 from p in GetPropertyCurrencyExchangeRateBaseQuery()
                 where p.PropertyId == propertyId && p.CurrentDate.Date >= initialDate.Date && p.CurrentDate.Date <= endDate.Date
                 select new QuotationCurrentDto
                 {
                     Id = p.Id,
                     CurrencyId = p.CurrencyId,
                     CurrencyName = p.CurrencyName,
                     CurrentDate = p.CurrentDate,
                     ExchangeRate = p.ExchangeRate,
                     CreationTime = p.CreationTime,
                     CreatorUserId = p.CreatorUserId
                 };

            var result = new List<QuotationCurrentDto>();

            if (!isToDolar)
            {
                var propertyParameter = Context.PropertyParameters.FirstOrDefault(x => x.PropertyId == propertyId && x.ApplicationParameterId == (int)ApplicationParameterEnum.ParameterDefaultCurrency);

                var propertyCurrencyId = propertyParameter?.PropertyParameterValue != null ? Guid.Parse(propertyParameter.PropertyParameterValue) : Guid.Empty;

                currencyExchangeBaseQuery = currencyExchangeBaseQuery.Where(c => c.CurrencyId == propertyCurrencyId);

                foreach (var date in currencyExchangeBaseQuery.Select(e => e.CurrentDate).Distinct())
                {
                    if (result.Any(c => c.CurrentDate.Date == date.Date)) continue;

                    var propertyCurrencyExchange = currencyExchangeBaseQuery.Where(e => e.CurrentDate.Date == date.Date).OrderByDescending(e => e.CreationTime).FirstOrDefault();

                    result.Add(new QuotationCurrentDto
                    {
                        Id = propertyCurrencyExchange != null ? propertyCurrencyExchange.Id : Guid.Empty,
                        CurrencyId = propertyCurrencyExchange != null ? propertyCurrencyExchange.CurrencyId : Guid.Empty,
                        CurrencyName = propertyCurrencyExchange?.CurrencyName,
                        CurrentDate = propertyCurrencyExchange != null ? propertyCurrencyExchange.CurrentDate : DateTime.MinValue,
                        ExchangeRate = propertyCurrencyExchange?.ExchangeRate
                    });
                }
            }
            else
            {
                foreach (var currency in currencyExchangeBaseQuery.Select(e => new { e.CurrentDate, e.CurrencyId}).Distinct())
                {
                    if (result.Any(c => c.CurrentDate.Date == currency.CurrentDate.Date && c.CurrencyId == currency.CurrencyId)) continue;

                    var propertyCurrencyExchange = currencyExchangeBaseQuery.Where(e => e.CurrentDate.Date == currency.CurrentDate.Date && 
                                                                                        e.CurrencyId == currency.CurrencyId)
                                                                            .OrderByDescending(e => e.CreationTime).FirstOrDefault();

                    result.Add(new QuotationCurrentDto
                    {
                        Id = propertyCurrencyExchange != null ? propertyCurrencyExchange.Id : Guid.Empty,
                        CurrencyId = propertyCurrencyExchange != null ? propertyCurrencyExchange.CurrencyId : Guid.Empty,
                        CurrencyName = propertyCurrencyExchange?.CurrencyName,
                        CurrentDate = propertyCurrencyExchange != null ? propertyCurrencyExchange.CurrentDate : DateTime.MinValue,
                        ExchangeRate = propertyCurrencyExchange?.ExchangeRate,
                        CreationTime = propertyCurrencyExchange.CreationTime,
                        CreatorUserId = propertyCurrencyExchange.CreatorUserId
                    });
                }
            }

            return result;
        }
    }
}
