﻿// //  <copyright file="ReasonReadRepository.cs" company="TOTVS">
// //  Copyright (c) TOTVS. All rights reserved.
// //  </copyright>
namespace Thex.EntityFrameworkCore.Repositories
{
    using System.Collections.Generic;
    using System.Linq;

    using Thex.Common.Enumerations;
    using Thex.Domain.Entities;
    using Thex.Infra.ReadInterfaces;
    using Thex.Dto;
    using Tnf.Dto;
    using Tnf.EntityFrameworkCore.Repositories;
    using Tnf.EntityFrameworkCore;
    using Thex.Infra.Context;
    using Tnf.Localization;
    using Thex.Common;

    public class ReasonReadRepository : EfCoreRepositoryBase<ThexContext, Reason>, IReasonReadRepository
    {
        private readonly IChainReadRepository _chainReadRepository;
        private readonly ILocalizationManager _localizationManager;

        public ReasonReadRepository(
            IDbContextProvider<ThexContext> dbContextProvider,
            IChainReadRepository chainReadRepository, 
            ILocalizationManager localizationManager)
            : base(dbContextProvider)
        {
            _chainReadRepository = chainReadRepository;
            _localizationManager = localizationManager;
        }

        public IListDto<ReasonDto> GetAllByPropertyId(GetAllReasonDto request, int propertyId)
        {
            var chain = _chainReadRepository.GetChainByPropertyId(propertyId);
            var dbBaseQuery = GetAllReasonBaseQuery(request, chain.Id);

            if (dbBaseQuery != null && !string.IsNullOrEmpty(request.SearchData))
                dbBaseQuery = dbBaseQuery.Where(d => d.ReasonName.Contains(request.SearchData));

            var totalItemsDto = dbBaseQuery.Count();

            var resultDto = new List<ReasonDto>();

            foreach (var reason in dbBaseQuery)
            {
                reason.ReasonCategoryName = _localizationManager.GetString(AppConsts.LocalizationSourceName, ((ReasonCategoryEnum)reason.ReasonCategoryId).ToString());
                resultDto.Add(reason);
            }

            var result =
                new ListDto<ReasonDto>
                {
                    Items = resultDto,
                    HasNext = SearchReasonHasNext(
                            request,
                            totalItemsDto,
                            resultDto)
                };

            return result;
        }

        public IListDto<ReasonDto> GetAllByPropertyIdAndReasonCategoryId(GetAllReasonDto request, int propertyId, int reasonCategoryId)
        {
            var dbBaseQuery = SearchGetAllReasonByPropertyIdAndReasonCategoryIdBaseQuery(propertyId, reasonCategoryId);

            if (dbBaseQuery != null && !string.IsNullOrEmpty(request.SearchData))
                dbBaseQuery = dbBaseQuery.Where(d => d.ReasonName.Contains(request.SearchData));

            var totalItemsDto = dbBaseQuery.Count();

            var resultDto = dbBaseQuery.ToList();

            foreach (var reason in resultDto)
            {
                reason.ReasonCategoryName = _localizationManager.GetString(AppConsts.LocalizationSourceName, ((ReasonCategoryEnum)reason.ReasonCategoryId).ToString());
            }

            var result =
                new ListDto<ReasonDto>
                {
                    Items = resultDto,
                    HasNext = SearchReasonHasNext(
                            request,
                            totalItemsDto,
                            resultDto),
                };

            return result;
        }

        private IQueryable<ReasonDto> GetAllReasonBaseQuery(GetAllReasonDto request, int chainId)
        {
            var dbBaseQuery = from reason in this.Context.Reasons
                              join c in this.Context.ReasonCategories on reason.ReasonCategoryId equals c.Id
                              where reason.ChainId == chainId
                              select new ReasonDto
                              {
                                  Id = reason.Id,
                                  ReasonName = reason.ReasonName,                                  
                                  IsActive = reason.IsActive,
                                  ChainId = reason.ChainId,
                                  ReasonCategoryId = reason.ReasonCategoryId,
                                  ReasonCategoryName = c.ReasonType
                          
                              };
            return dbBaseQuery;
        }

        public IList<ReasonDto> GetAllByPropertyIdAndReasonCategoryId(int propertyId, int reasonCategoryId)
        {
            return SearchGetAllReasonByPropertyIdAndReasonCategoryIdBaseQuery(propertyId, reasonCategoryId).ToList();
        }

        private IQueryable<ReasonDto> SearchGetAllReasonByPropertyIdAndReasonCategoryIdBaseQuery(int propertyId, int reasonCategoryId)
        {
            var chain = _chainReadRepository.GetChainByPropertyId(propertyId);
            var dbBaseQuery = from reason in this.Context.Reasons
                              where reason.ChainId == chain.Id && reason.ReasonCategoryId == reasonCategoryId
                              select new ReasonDto
                              {
                                  Id = reason.Id,
                                  ReasonName = reason.ReasonName,
                                  IsActive = reason.IsActive,
                                  ChainId = reason.ChainId,
                                  ReasonCategoryId = reason.ReasonCategoryId,

                              };

            return dbBaseQuery;

        }

        public IListDto<ReasonDto> GetAllByChainIdAndReasonCategory(GetAllReasonDto request, int chainId, ReasonCategoryEnum reasonCategoryEnum)
        {
            var reasonsCategories = new int[] { (int)ReasonCategoryEnum.PurposeOfTrip }.ToList();
            var dbBaseQuery = SearchReasonByChainIdAndReasonCategoryBaseQuery(request, chainId, reasonCategoryEnum);

            var totalItemsDto = dbBaseQuery.Count();

            //var resultDto = dbBaseQuery.SkipAndTakeByRequestDto(request).OrderByRequestDto(request).ToList();
            var resultDto = dbBaseQuery.OrderBy(x=>x.ReasonCategoryName).ThenBy(x=>x.ReasonName).ToList();

            var result =
                new ListDto<ReasonDto>
                {
                    Items = resultDto,
                    HasNext = SearchReasonHasNext(
                            request,
                            totalItemsDto,
                            resultDto),
                };

            return result;
        }

        private IQueryable<ReasonDto> SearchReasonByChainIdAndReasonCategoryBaseQuery(GetAllReasonDto request, int chainId, ReasonCategoryEnum reasonCategoryEnum)
        {
            var dbBaseQuery = from reason in this.Context.Reasons
                              where reason.IsActive
                              && reason.ChainId == chainId
                              && reason.ReasonCategoryId == (int)reasonCategoryEnum
                              select new ReasonDto
                              {
                                  Id = reason.Id,
                                  ReasonName = reason.ReasonName,
                                  IsActive = reason.IsActive,
                                  ChainId = reason.ChainId,
                                  ReasonCategoryId = reason.ReasonCategoryId
                              };

            return dbBaseQuery;
        }

        private bool SearchReasonHasNext(
            GetAllReasonDto request,
            int totalItemsDto,
            List<ReasonDto> resultDto)
        {
            return totalItemsDto > ((request.Page - 1) * request.PageSize) + resultDto.Count();
        }

        public ReasonDto GetReasonByReasonId(int reasonId)
        {

             return (from reason in this.Context.Reasons
                               join c in this.Context.ReasonCategories on reason.ReasonCategoryId equals c.Id
                              where reason.Id == reasonId
                              select new ReasonDto
                              {
                                  Id = reason.Id,
                                  ReasonName = reason.ReasonName,
                                  IsActive = reason.IsActive,
                                  ChainId = reason.ChainId,
                                  ReasonCategoryId = reason.ReasonCategoryId,
                                  ReasonCategoryName = c.ReasonType

                              }).FirstOrDefault();
        }

        public bool CheckIfReasonIsReversal(int reasonId)
        {
            return Context.Reasons.Any(e => e.Id == reasonId && e.IsActive == true && e.ReasonCategoryId == (int)ReasonCategoryEnum.Reversal);
        }

        public bool CheckIfReasonIsToReopenBillingAccount(int reasonId)
        {
            return Context.Reasons.Any(e => e.Id == reasonId && e.IsActive == true && e.ReasonCategoryId == (int)ReasonCategoryEnum.ReopenBillingAccount);
        }

        public bool CheckIfReasonIsDiscount(int reasonId)
        {
            return Context.Reasons.Any(e => e.Id == reasonId && e.IsActive == true && e.ReasonCategoryId == (int)ReasonCategoryEnum.Discount);
        }

        public bool CheckIfReasonIsBillingInvoiceCancel(int reasonId)
        {
            return Context.Reasons.Any(e => e.Id == reasonId && e.IsActive == true && e.ReasonCategoryId == (int)ReasonCategoryEnum.BillingInvoiceCancel);
        }

        public string GetReasonNameById(int id)
            => (from reason in this.Context.Reasons
                    join c in this.Context.ReasonCategories on reason.ReasonCategoryId equals c.Id
                    where reason.Id == id
                    select reason.ReasonName).FirstOrDefault();
    }
}