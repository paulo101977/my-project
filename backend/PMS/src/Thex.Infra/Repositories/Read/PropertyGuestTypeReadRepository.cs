﻿// //  <copyright file="PropertyGuestTypeReadRepository.cs" company="TOTVS">
// //  Copyright (c) TOTVS. All rights reserved.
// //  </copyright>
namespace Thex.EntityFrameworkCore.Repositories
{
    using System.Collections.Generic;
    using System.Linq;

    using Thex.Domain.Entities;
    using Thex.Infra.ReadInterfaces;

    using Thex.Dto;
    using Tnf.EntityFrameworkCore.Repositories;
    using Tnf.EntityFrameworkCore;
    using Thex.Infra.Context;

    public class PropertyGuestTypeReadRepository : EfCoreRepositoryBase<ThexContext, PropertyGuestType>, IPropertyGuestTypeReadRepository
    {
        public PropertyGuestTypeReadRepository(IDbContextProvider<ThexContext> dbContextProvider)
            : base(dbContextProvider)
        {
        }

        public bool Any(int id, int propertyId)
        {
            return Context.PropertyGuestTypes.Any(e => e.Id == id && e.PropertyId == propertyId);
        }

        public List<PropertyGuestTypeDto> GetAllByPropertyId(int propertyId, bool activeOnly = false)
        {
            if (activeOnly)
            {
                return (from propertyGuestType in Context.PropertyGuestTypes
                        where propertyGuestType.PropertyId == propertyId 
                        && propertyGuestType.IsActive.HasValue && propertyGuestType.IsActive.Value
                        select new PropertyGuestTypeDto
                        {
                            Id = propertyGuestType.Id,
                            GuestTypeName = propertyGuestType.GuestTypeName,
                            IsExemptOfTourismTax = propertyGuestType.IsExemptOfTourismTax
                        }).ToList();
            }
            else
            {
                return (from propertyGuestType in Context.PropertyGuestTypes
                        where propertyGuestType.PropertyId == propertyId
                        select new PropertyGuestTypeDto
                        {
                            Id = propertyGuestType.Id,
                            GuestTypeName = propertyGuestType.GuestTypeName,
                            IsExemptOfTourismTax = propertyGuestType.IsExemptOfTourismTax
                        }).ToList();
            }
        }
    }
}