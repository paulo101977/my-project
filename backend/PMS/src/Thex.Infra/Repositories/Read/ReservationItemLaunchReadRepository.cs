﻿using System.Collections.Generic;
using Thex.Domain.Entities;
using Thex.Infra.Context;
using Thex.Infra.ReadInterfaces;
using Tnf.EntityFrameworkCore;
using Tnf.EntityFrameworkCore.Repositories;
using System.Linq;

namespace Thex.EntityFrameworkCore.Repositories
{
    public class ReservationItemLaunchReadRepository : EfCoreRepositoryBase<ThexContext, ReservationItemLaunch>, IReservationItemLaunchReadRepository
    {
        public ReservationItemLaunchReadRepository(
               IDbContextProvider<ThexContext> dbContextProvider)
               : base(dbContextProvider)
        {
        }
         
        public List<ReservationItemLaunch> GetAllByGuestReservationItemIdList(List<long?> guestReservationItemIdList)
        {
            return (from reservationItemLaunch in Context.ReservationItemLaunchers
                               where guestReservationItemIdList.Contains(reservationItemLaunch.GuestReservationItemId)
                               select reservationItemLaunch).ToList();
        }
        
      
    }
}
