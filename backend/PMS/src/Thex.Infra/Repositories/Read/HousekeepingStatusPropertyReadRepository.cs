﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Thex.Common.Enumerations;
using Thex.Domain.Entities;
using Thex.Dto;
using Thex.Dto.Housekeeping;
using Thex.Infra.ReadInterfaces;
using Tnf.Dto;
using Tnf.EntityFrameworkCore.Repositories;
using Tnf.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Thex.Infra.Context;
using Thex.EntityFrameworkCore.ReadInterfaces.Repositories;
using Tnf.Localization;
using Thex.Common;
using Tnf.Notifications;

namespace Thex.Infra.Repositories.Read
{
    public class HousekeepingStatusPropertyReadRepository : EfCoreRepositoryBase<ThexContext, HousekeepingStatusProperty>, IHousekeepingStatusPropertyReadRepository
    {
        private readonly IRoomReadRepository _roomReadRepository;
        private readonly ILocalizationManager _localizationManager;
        private readonly INotificationHandler Notification;

        public HousekeepingStatusPropertyReadRepository(
            IDbContextProvider<ThexContext> dbContextProvider,
            IRoomReadRepository roomReadRepository,
            ILocalizationManager localizationManager,
            INotificationHandler notificationHandler)
            : base(dbContextProvider)
        {
            _roomReadRepository = roomReadRepository;
            _localizationManager = localizationManager;
            Notification = notificationHandler;
        }

        public IListDto<HousekeepingStatusPropertyDto> GetAll(GetAllHousekeepingStatusPropertyDto request, int propertyId)
        {
            var dbBaseQuery = (from houseproperty in Context.HousekeepingStatusProperties
                               join housestatus in Context.HousekeepingStatuss on houseproperty.HousekeepingStatusId equals housestatus.Id
                               where houseproperty.PropertyId == propertyId && !houseproperty.IsDeleted
                               select new HousekeepingStatusPropertyDto()
                               {
                                   Id = houseproperty.Id,
                                   PropertyId = houseproperty.PropertyId,
                                   HousekeepingStatusId = houseproperty.HousekeepingStatusId,
                                   HousekeepingStatusName = housestatus.StatusName,
                                   StatusName = houseproperty.StatusName,
                                   Color = houseproperty.Color,
                                   IsActive = houseproperty.IsActive,
                                   VisibleBtnIsActive = VisibleBtnIsActive(houseproperty.HousekeepingStatusId)

                               });

            if (!String.IsNullOrEmpty(request.SearchData))
            {
                dbBaseQuery = dbBaseQuery.Where(x => x.StatusName.Contains(request.SearchData));
            }

            foreach (var housekeeping in dbBaseQuery)
            {
                housekeeping.HousekeepingStatusName = _localizationManager.GetString(AppConsts.LocalizationSourceName, ((HousekeepingStatusEnum)housekeeping.HousekeepingStatusId).ToString());
            }

            var totalItemsDto = dbBaseQuery.Count();

            var resultDto = dbBaseQuery.ToList();

            var result =
                new ListDto<HousekeepingStatusPropertyDto>
                {
                    Items = resultDto,
                    HasNext = SearchReasonHasNext(
                            request,
                            totalItemsDto,
                            resultDto),
                };

            return result;
        }

        private bool VisibleBtnIsActive(int HousekeepingStatusId)
        {
            if (HousekeepingStatusId == (int)HousekeepingStatusEnum.CreatedByHotel)
                return true;

            return false;
        }

        public HousekeepingStatusPropertyDto Get(Guid guid)
        {
            var dbBaseQuery = (from houseproperty in Context.HousekeepingStatusProperties
                               join housestatus in Context.HousekeepingStatuss on houseproperty.HousekeepingStatusId equals housestatus.Id
                               where houseproperty.Id == guid
                               select new HousekeepingStatusPropertyDto()
                               {
                                   Id = houseproperty.Id,
                                   PropertyId = houseproperty.PropertyId,
                                   HousekeepingStatusId = houseproperty.HousekeepingStatusId,
                                   HousekeepingStatusName = housestatus.StatusName,
                                   StatusName = houseproperty.StatusName,
                                   Color = houseproperty.Color,
                                   IsActive = houseproperty.IsActive

                               }).FirstOrDefault();

            return dbBaseQuery;
        }

        private bool SearchReasonHasNext(GetAllHousekeepingStatusPropertyDto request, int totalItemsDto, List<HousekeepingStatusPropertyDto> resultDto)
        {
            return totalItemsDto > ((request.Page - 1) * request.PageSize) + resultDto.Count();
        }

        public IListDto<GetAllHousekeepingAlterStatusDto> GetAllHousekeepingAlterStatusRoom(GetAllHousekeepingAlterStatusRoomDto request, int propertyId, DateTime systemDate)
        {
            return _roomReadRepository.GetAllHousekeepingAlterStatusRoom(request, propertyId, systemDate);
        }

        public Guid GetHousekeepingStatusIdDirty()
        {
            var houseKeepingDirty = Context.HousekeepingStatusProperties.Where(x => x.HousekeepingStatusId == (int)HousekeepingStatusEnum.Dirty).FirstOrDefault();

            if (houseKeepingDirty == null)
            {
                Notification.Raise(Notification
                 .DefaultBuilder
                 .WithMessage(AppConsts.LocalizationSourceName, HousekeepingStatusEnumError.HousekeepingStatusDirtyNotExisting)
                 .Build());

                return Guid.Empty;
            }

            return houseKeepingDirty.Id;
        }
        
        public Guid GetHousekeepingStatusIdMaintenance()
        {
            return Context.HousekeepingStatusProperties.Where(x => x.HousekeepingStatusId == (int)HousekeepingStatusEnum.Maintenance).FirstOrDefault().Id;
        }

        public ListDto<HousekeepingStatusPropertyDto> GetAllHousekeepingStatusCreateBySystem(int propertyId)
        {
            var dbBaseQuery = (from houseproperty in Context.HousekeepingStatusProperties
                               join housestatus in Context.HousekeepingStatuss on houseproperty.HousekeepingStatusId equals housestatus.Id
                               where houseproperty.PropertyId == propertyId && !houseproperty.IsDeleted //&& houseproperty.HousekeepingStatusId  != (int)HousekeepingStatusEnum.CreatedByHotel
                               select new HousekeepingStatusPropertyDto()
                               {
                                   Id = houseproperty.Id,
                                   HousekeepingStatusId = houseproperty.HousekeepingStatusId,
                                   StatusName = houseproperty.StatusName   ,
                                   Color = houseproperty.Color
                               });

            var resultDto = dbBaseQuery.ToList();
            var totalItemsDto = dbBaseQuery.Count();

            var result =
                new ListDto<HousekeepingStatusPropertyDto>
                {
                    Items = resultDto,
                    HasNext = false,
                };

            return result;

        }
    }
}
