﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Thex.Common.Enumerations;
using Thex.Domain.Entities;
using Thex.Dto;
using Thex.Infra.Context;
using Thex.Infra.ReadInterfaces;
using Thex.Kernel;
using Tnf.EntityFrameworkCore;
using Tnf.EntityFrameworkCore.Repositories;

namespace Thex.Infra.Repositories.Read
{
    public class GuestReadRepository : EfCoreRepositoryBase<ThexContext, GuestRegistration>, IGuestReadRepository
    {
        private readonly IApplicationUser _applicationUser;

        public GuestReadRepository(IDbContextProvider<ThexContext> dbContextProvider, IApplicationUser applicationUser)
            : base(dbContextProvider)
        {
            _applicationUser = applicationUser;
        }

        public List<GetAllForeingGuest> GetAllForeingGuestByFilters(DateTime startDate, DateTime endDate, List<int> listStatus)
            =>  (from gr in Context.GuestRegistrations.AsNoTracking()
                 join gri in Context.GuestReservationItems.AsNoTracking() on gr.Id equals gri.GuestRegistrationId
                 join l in Context.Locations.AsNoTracking() on gr.Id equals l.OwnerId
                 join cs in Context.CountrySubdivisions.AsNoTracking() on gr.Nationality equals cs.Id
                 join dt in Context.DocumentTypes.AsNoTracking() on gr.DocumentTypeId equals dt.Id
                 where cs.TwoLetterIsoCode != _applicationUser.PropertyCountryCode &&
                     listStatus.Contains(gri.GuestStatusId) &&
                     gri.CheckInDate.HasValue &&
                     gri.CheckInDate.Value.Date <= endDate.Date &&
                     (gri.CheckOutDate.HasValue ? gri.CheckOutDate.Value.Date > startDate.Date :
                     gri.EstimatedDepartureDate.Date > startDate.Date)
                 select new GetAllForeingGuest()
                 {
                     LastName = string.IsNullOrWhiteSpace(gr.LastName) ? gr.FirstName : gr.LastName,
                     FirstName = string.IsNullOrWhiteSpace(gr.LastName) ? string.Empty : gr.FirstName,
                     Nationality = cs.ThreeLetterIsoCode,
                     DateOfBirth = gr.BirthDate.Value,
                     BirthPlace = string.Empty,
                     DocumentInformation = Regex.Replace(gr.DocumentInformation, @"[^0-9a-zA-Z]+", ""),
                     TypeDocumentIdentification = gr.DocumentTypeId == (int)DocumentTypeEnum.Passport ? "P" : "O",
                     DocumentIssuingCountry = cs.ThreeLetterIsoCode,
                     ArrivalDate = gri.CheckInDate.Value,
                     DepartureDate = gri.CheckOutDate,
                     CountryResidenceOrigin = cs.ThreeLetterIsoCode,
                     Location = $"{l.StreetName}, {l.StreetNumber}".Length > 30 ? $"{l.StreetName}, {l.StreetNumber}".Substring(0,30) : $"{l.StreetName}, {l.StreetNumber}"
                 }).Distinct().ToList();
    }
}
