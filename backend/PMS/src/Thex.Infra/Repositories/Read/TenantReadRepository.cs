﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;
using Thex.Common;
using Thex.Domain.Entities;
using Thex.Domain.JoinMap;
using Thex.Infra.Context;
using Thex.Infra.ReadInterfaces;
using Thex.Kernel;
using Tnf.EntityFrameworkCore;
using Tnf.EntityFrameworkCore.Repositories;

namespace Thex.Infra.Repositories.Read
{
    public class TenantReadRepository : EfCoreRepositoryBase<ThexContext, Tenant>, ITenantReadRepository
    {
        private IApplicationUser _applicationUser;
        private IConfiguration _configuration;

        private Guid _tenantId = Guid.Empty;
        private Guid _userUid = Guid.Empty;
        private List<Guid> _tenantIds = null;

        public TenantReadRepository(
            IDbContextProvider<ThexContext> dbContextProvider,
            IApplicationUser applicationUser,
            IConfiguration configuration)
            : base(dbContextProvider)
        {
            _configuration = configuration;
            _applicationUser = applicationUser;

            _tenantId = applicationUser.TenantId;
            _userUid = applicationUser.UserUid;
            _tenantIds = null;
        }

        public List<Guid> GetIdListByLoggedTenantId()
        {
            if (_tenantIds != null)
                return _tenantIds;

            var connectionString = _configuration.GetSection("ConnectionStrings").GetValue<string>("SqlServer");
            var connection = new SqlConnection(connectionString);
            connection.Open();
            var cmd = connection.CreateCommand();

            List<Guid> result = new List<Guid>();
            using (var transaction = connection.BeginTransaction(IsolationLevel.ReadCommitted))
            {
                cmd.Transaction = transaction;

                cmd.CommandText = "GetTenantIds";
                cmd.CommandType = CommandType.StoredProcedure;

                var param = cmd.CreateParameter();
                param.ParameterName = "@MainTenantId";
                param.Direction = ParameterDirection.Input;
                param.DbType = DbType.Guid;
                cmd.Parameters.Add(param);
                param.Value = _tenantId;

                using (var reader = cmd.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            Guid tenantId = reader.GetGuid(0);
                            result.Add(tenantId);
                        }
                    }
                    reader.Close();
                }

                if (transaction != null) transaction.Commit();


            }

            connection.Close();

            _tenantIds = result.ToList();
            return _tenantIds;
        }

        public List<Guid> GetIdListByLoggedUserId()
        {
            if (_tenantIds != null)
                return _tenantIds;

            var connectionString = _configuration.GetSection("ConnectionStrings").GetValue<string>("SqlServer");
            var connection = new SqlConnection(connectionString);
            connection.Open();
            var cmd = connection.CreateCommand();

            List<Guid> result = new List<Guid>();
            using (var transaction = connection.BeginTransaction(IsolationLevel.ReadCommitted))
            {
                cmd.Transaction = transaction;

                cmd.CommandText = "GetTenantIdListByUserId";
                cmd.CommandType = CommandType.StoredProcedure;

                var param = cmd.CreateParameter();
                param.ParameterName = "@UserId";
                param.Direction = ParameterDirection.Input;
                param.DbType = DbType.Guid;
                cmd.Parameters.Add(param);
                param.Value = _userUid;

                using (var reader = cmd.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            Guid tenantId = reader.GetGuid(0);
                            result.Add(tenantId);
                        }
                    }
                    reader.Close();
                }

                if (transaction != null) transaction.Commit();


            }
            connection.Close();

            _tenantIds = result.ToList();
            return _tenantIds;
        }

        public Expression<Func<T, bool>> FilterByTenant<T>(List<Guid> tenantList)
        {
            var methodInfo = typeof(List<string>).GetMethod("Contains",
                new Type[] { typeof(string) });

            var list = Expression.Constant(tenantList);

            var param = Expression.Parameter(typeof(T), "j");
            var value = Expression.Property(param, "TenantId");
            var body = Expression.Call(list, methodInfo, value);

            // j => codes.Contains(j.Code)
            return Expression.Lambda<Func<T, bool>>(body, param);
        }

        public Tenant GetCurrentTenant()
        {
            return Context.Tenants.Where(e => e.Id == _tenantId).FirstOrDefault();
        }

        public TenantPropertyParameterJoinMap GetTenantById(Guid id)
        {
            return (from tenant in Context.Tenants.IgnoreQueryFilters()
                    join propertyParameter in this.Context.PropertyParameters.IgnoreQueryFilters() on
                    tenant.Id equals propertyParameter.TenantId into tpl
                    from tenantPropertyParameterLeft in tpl.DefaultIfEmpty()
                    where (tenant.Id == id && tenantPropertyParameterLeft == null)
                    || (tenant.Id == id && tenantPropertyParameterLeft != null && tenantPropertyParameterLeft.ApplicationParameterId == 12)
                    select new TenantPropertyParameterJoinMap
                    {
                        PropertyParameter = tenantPropertyParameterLeft,
                        Tenant = tenant
                    }).FirstOrDefault();
        }

        //public static IQueryable<TEntity> WhereContains<TEntity, TProperty>(this IQueryable<TEntity> source, IList<TProperty> acceptedValues, Expression<Func<TEntity, TProperty>> property)
        //{
        //    if (acceptedValues == null || !acceptedValues.Any())
        //        return source;

        //    var member = property.Body as MemberExpression;
        //    if (member == null)
        //        return source; // better throw

        //    string propertyName = member.Member.Name;
        //    var containsMethod = typeof(ICollection<TProperty>).GetMethod("Contains"); // get Contains method of IList
        //    var parameter = Expression.Parameter(typeof(TEntity), "p");
        //    var memberAccess = Expression.Property(parameter, propertyName); // p.ValidValue (for example)
        //    var list = Expression.Constant(acceptedValues); // your list
        //    var body = Expression.Call(list, containsMethod, memberAccess); // list.Contains(p.ValidValue)
        //    return source.Where(Expression.Lambda<Func<TEntity, bool>>(body, parameter));
        //}

        //public static IQueryable<Tsource> WhereTenant<Tsource, Tproperty>(this IQueryable<Tsource> source, Expression<Func<Tsource, Tproperty>> property, IList<int> accpetedValues)
        //{
        //    var propertyName = ((MemberExpression)property.Body).Member.Name;

        //    if (accpetedValues == null || !accpetedValues.Any() || string.IsNullOrEmpty(propertyName))
        //        return source;

        //    var item = Expression.Parameter(typeof(Tsource), "item");
        //    var selector = Expression.PropertyOrField(item, propertyName);
        //    var predicate = Expression.Lambda<Func<Tsource, bool>>(
        //        Expression.Call(typeof(Enumerable), "Contains", new[] { typeof(Tproperty) },
        //            Expression.Constant(accpetedValues), selector),
        //        item);
        //    return source.Where(predicate);
        //}

        //        public static Expression<Func<T, bool>> FilterByCode<T>(List<string> codes)
        //where T : ICoded //some interface with a `Code` field
        //        {
        //            return p => codes.Contains(p.Code);
        //        }
        // j => codes.Contains(j.Code) isn't accurate. What that lambda actually looks like is:  (j, codes) => codes.Contains(j.Code);

    }
}
