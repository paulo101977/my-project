﻿using System;
using System.Collections.Generic;
using Thex.Domain.Entities;
using System.Linq;
using Thex.Infra.ReadInterfaces;
using Tnf.EntityFrameworkCore.Repositories;
using Tnf.EntityFrameworkCore;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Thex.Common.Extensions;
using Thex.Infra.Context;
using Thex.Dto;
using Tnf.Localization;
using Thex.Common;
using Thex.Common.Enumerations;

namespace Thex.Infra.Repositories.Read
{
    public class PropertyBaseRateHeaderHistoryReadRepository : EfCoreRepositoryBase<ThexContext, PropertyBaseRateHeaderHistory>, IPropertyBaseRateHeaderHistoryReadRepository
    {
        private readonly IPropertyBaseRateHistoryReadRepository _propertyBaseRateHistoryReadRepository;
        private readonly ICurrencyReadRepository _currencyReadRepository;
        private readonly ILocalizationManager _localizationManager;
        private readonly IPropertyMealPlanTypeReadRepository _mealPlanTypeReadRepository;

        public PropertyBaseRateHeaderHistoryReadRepository(
            IDbContextProvider<ThexContext> dbContextProvider, 
            IPropertyBaseRateHistoryReadRepository propertyBaseRateHistoryReadRepository,
            ILocalizationManager localizationManager,
            IPropertyMealPlanTypeReadRepository mealPlanTypeReadRepository,
              ICurrencyReadRepository currencyReadRepository)
            : base(dbContextProvider)
        {
            _propertyBaseRateHistoryReadRepository = propertyBaseRateHistoryReadRepository;
            _currencyReadRepository = currencyReadRepository;
            _localizationManager = localizationManager;
            _mealPlanTypeReadRepository = mealPlanTypeReadRepository;
        }

        public async Task<List<PropertyBaseRateHeaderHistory>> GetAll(Guid? pRatePlanId)
        {
            var dbBaseQuery = Context
                                .PropertyBaseRateHeaderHistories
                                .AsNoTracking()
                                .Where(e => e.RatePlanId == pRatePlanId)
                                .OrderBy(e => e.CreationTime);

            return await dbBaseQuery.ToListAsync();
        }

        public async Task<List<PropertyBaseRateHeaderHistoryDto>> GetAllPropertyBaseRateHeaderHistoryByPropertyId(int propertyId)
        {
            var dbBaseQuery = (from propertyBaseRateHeaderHistory in Context.PropertyBaseRateHeaderHistories
                               join currency in Context.Currencies on propertyBaseRateHeaderHistory.CurrencyId equals currency.Id
                               join propertyMealPlanType in Context.PropertyMealPlanTypes on propertyBaseRateHeaderHistory.MealPlanTypeDefaultId equals propertyMealPlanType.MealPlanTypeId
                               where propertyBaseRateHeaderHistory.PropertyId == propertyId
                               orderby propertyBaseRateHeaderHistory.CreationTime descending
                               select new PropertyBaseRateHeaderHistoryDto
                               {
                                   CurrencyId = propertyBaseRateHeaderHistory.CurrencyId,
                                   FinalDate = propertyBaseRateHeaderHistory.FinalDate,
                                   InitialDate = propertyBaseRateHeaderHistory.InitialDate,
                                   Id = propertyBaseRateHeaderHistory.Id,
                                   MealPlanTypeDefaultId = propertyBaseRateHeaderHistory.MealPlanTypeDefaultId,
                                   MealPlanTypeDefaultName = propertyMealPlanType.PropertyMealPlanTypeName,
                                   PropertyId = propertyBaseRateHeaderHistory.PropertyId,
                                   Sunday = propertyBaseRateHeaderHistory.Sunday,
                                   Monday = propertyBaseRateHeaderHistory.Monday,
                                   Tuesday = propertyBaseRateHeaderHistory.Tuesday,
                                   Wednesday = propertyBaseRateHeaderHistory.Wednesday,
                                   Thursday = propertyBaseRateHeaderHistory.Thursday,
                                   Friday = propertyBaseRateHeaderHistory.Friday,
                                   Saturday = propertyBaseRateHeaderHistory.Saturday,
                                   RatePlanId = propertyBaseRateHeaderHistory.RatePlanId,
                                   TenantId = propertyBaseRateHeaderHistory.TenantId,
                                   CurrencyName = currency.CurrencyName
                               });
            return await dbBaseQuery.ToListAsync();
        }

        public IQueryable<PropertyBaseRateHeaderHistoryDto> GetAllPropertyBaseRateHeaderHistoryByRatePlanId(Guid pRatePlanId)
        {
            var dbBaseQuery = (from propertyBaseRateHeaderHistory in Context.PropertyBaseRateHeaderHistories
                               join currency in Context.Currencies on propertyBaseRateHeaderHistory.CurrencyId equals currency.Id
                               where propertyBaseRateHeaderHistory.RatePlanId == pRatePlanId
                               orderby propertyBaseRateHeaderHistory.CreationTime

                               select new PropertyBaseRateHeaderHistoryDto
                               {
                                   CurrencyId = propertyBaseRateHeaderHistory.CurrencyId,
                                   FinalDate = propertyBaseRateHeaderHistory.FinalDate,
                                   InitialDate = propertyBaseRateHeaderHistory.InitialDate,
                                   Id = propertyBaseRateHeaderHistory.Id,
                                   MealPlanTypeDefaultId = propertyBaseRateHeaderHistory.MealPlanTypeDefaultId,
                                   PropertyId = propertyBaseRateHeaderHistory.PropertyId,
                                   Sunday = propertyBaseRateHeaderHistory.Sunday,
                                   Monday = propertyBaseRateHeaderHistory.Monday,
                                   Tuesday = propertyBaseRateHeaderHistory.Tuesday,
                                   Wednesday = propertyBaseRateHeaderHistory.Wednesday,
                                   Thursday = propertyBaseRateHeaderHistory.Thursday,
                                   Friday = propertyBaseRateHeaderHistory.Friday,
                                   Saturday = propertyBaseRateHeaderHistory.Saturday,
                                   RatePlanId = propertyBaseRateHeaderHistory.RatePlanId,
                                   TenantId = propertyBaseRateHeaderHistory.TenantId,
                                   CurrencyName = currency.CurrencyName
                               });
            return dbBaseQuery;
        }

        public PropertyBaseRateHeaderHistory GetPropertyBaseRateHeaderHistoryByHeaderHistoryId(Guid? pPropertyBaseRateHeaderHistoryid)
        {
            if (pPropertyBaseRateHeaderHistoryid == null) return null;

            var item = Context.PropertyBaseRateHeaderHistories
                              .Where(x => x.Id == pPropertyBaseRateHeaderHistoryid).FirstOrDefault();

            if (item == null) return null;
            
            return item;
        }


    }
}
