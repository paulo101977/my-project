﻿using System;
using System.Collections.Generic;
using Thex.Domain.Entities;
using Thex.Dto;
using Thex.Infra.ReadInterfaces;
using Tnf.Dto;
using Tnf.EntityFrameworkCore.Repositories;
using Tnf.EntityFrameworkCore;
using System.Linq;
using Thex.Infra.Context;

namespace Thex.Infra.Repositories.Read
{
    public class BillingInvoicePropertyReadRepository : EfCoreRepositoryBase<ThexContext, BillingInvoiceProperty>, IBillingInvoicePropertyReadRepository
    {
        
        public BillingInvoicePropertyReadRepository(IDbContextProvider<ThexContext> dbContextProvider) : base(dbContextProvider)
        {
            
        }

        public IListDto<BillingInvoicePropertyDto> GetAllByPropertyId(int propertyId, GetAllBillingInvoicePropertyDto request)
        {
            var dbBaseQuery = from billingInvoiceProperty in Context.BillingInvoiceProperties.Where(exp => exp.PropertyId == propertyId)
                               join billingInvoiceModel in Context.BillingInvoiceModels on billingInvoiceProperty.BillingInvoiceModelId equals billingInvoiceModel.Id
                               select new BillingInvoicePropertyDto
                               {
                                   Id = billingInvoiceProperty.Id,
                                   BillingInvoiceModeName = billingInvoiceModel.Description,
                                   BillingInvoicePropertySeries = billingInvoiceProperty.BillingInvoicePropertySeries
                               };

            var result = new ListDto<BillingInvoicePropertyDto>()
            {
                HasNext = false,
                Items = dbBaseQuery.ToList(),
            };

            return result;

        }

        public IListDto<BillingInvoicePropertyDto> GetAllWithoutReferenceByPropertyId(int propertyId)
        {
            var dbBaseQuery = from billingInvoiceProperty in Context.BillingInvoiceProperties
                        join billingInvoiceModel in Context.BillingInvoiceModels on billingInvoiceProperty.BillingInvoiceModelId equals billingInvoiceModel.Id
                        where billingInvoiceProperty.PropertyId == propertyId
                            && billingInvoiceProperty.BillingInvoicePropertyReferenceId == null
                    select new BillingInvoicePropertyDto
                        {
                            Id = billingInvoiceProperty.Id,
                            BillingInvoiceModeName = billingInvoiceModel.Description,
                            BillingInvoicePropertySeries = billingInvoiceProperty.BillingInvoicePropertySeries
                        };

            var result = new ListDto<BillingInvoicePropertyDto>()
            {
                HasNext = false,
                Items = dbBaseQuery.ToList(),
            };

            return result;
        }

        public BillingInvoicePropertyDto GetById(Guid id)
        {

            var dbBaseQuery = (from billingInvoiceProperty in Context.BillingInvoiceProperties
                               join billingInvoiceModel in Context.BillingInvoiceModels on billingInvoiceProperty.BillingInvoiceModelId equals billingInvoiceModel.Id
                               join childBillingInvoiceProperty in Context.BillingInvoiceProperties on
                                  billingInvoiceProperty.Id equals childBillingInvoiceProperty.BillingInvoicePropertyReferenceId
                                  into cbip
                                from childBillingInvoicePropertyLeftRoom in cbip.DefaultIfEmpty()
                               where billingInvoiceProperty.Id == id
                               select new BillingInvoicePropertyDto
                               {
                                   Id = billingInvoiceProperty.Id,
                                   BillingInvoiceModelId = billingInvoiceProperty.BillingInvoiceModelId,
                                   BillingInvoicePropertySeries = billingInvoiceProperty.BillingInvoicePropertySeries,
                                   Description = billingInvoiceProperty.Description,
                                   EmailAlert = billingInvoiceProperty.EmailAlert,
                                   IsActive = billingInvoiceProperty.IsActive,
                                   IsIntegrated = billingInvoiceProperty.IsIntegrated,
                                   LastNumber = billingInvoiceProperty.LastNumber,
                                   PropertyId = billingInvoiceProperty.PropertyId,
                                   BillingInvoiceModeName = billingInvoiceModel.Description,
                                   AllowsCancel = billingInvoiceProperty.AllowsCancel.Value,
                                   DaysForCancel = billingInvoiceProperty.DaysForCancel,
                                   IntegrationId = billingInvoiceProperty.IntegrationId,
                                   ChildBillingInvoicePropertySeries = childBillingInvoicePropertyLeftRoom == null ? null : childBillingInvoicePropertyLeftRoom.BillingInvoicePropertySeries,
                                   ChildLastNumber = childBillingInvoicePropertyLeftRoom == null ? null : childBillingInvoicePropertyLeftRoom.LastNumber,
                                   ChildIntegrationId = childBillingInvoicePropertyLeftRoom == null ? null : childBillingInvoicePropertyLeftRoom.IntegrationId
                               }).FirstOrDefault();

            dbBaseQuery.BillingInvoicePropertySupportedTypeList = GetPropertySupportedTypeByBillingInvoicePropertyId(dbBaseQuery.Id);
            return dbBaseQuery;

        }

        private List<BillingInvoicePropertySupportedTypeDto> GetPropertySupportedTypeByBillingInvoicePropertyId(Guid BillingInvoicePropertyId)
        {
            var dbBaseQuery = (from propertySupported in Context.BillingInvoicePropertySupportedTypes
                               join billingItem in Context.BillingItems on propertySupported.BillingItemId equals billingItem.Id
                               join billingItemType in Context.BillingItemTypes on propertySupported.BillingItemTypeId equals billingItemType.Id
                               join countrySubdivionService in Context.CountrySubdvisionServices on propertySupported.CountrySubdvisionServiceId equals countrySubdivionService.Id
                               into cs from countrySubdivionService in cs.DefaultIfEmpty()

                               where propertySupported.BillingInvoicePropertyId == BillingInvoicePropertyId
                               select new BillingInvoicePropertySupportedTypeDto
                               {
                                   Id = propertySupported.Id,
                                   BillingItemTypeName = billingItemType.BillingItemTypeName,
                                   BillingItemName = billingItem.BillingItemName,
                                   CodeService = countrySubdivionService != null ? countrySubdivionService.Code : null,
                                   CountrySubdvisionServiceId = countrySubdivionService != null ? countrySubdivionService.Id : (Guid?)null,
                                   BillingItemId = propertySupported.BillingItemId,
                                   BillingItemTypeId = propertySupported.BillingItemTypeId,
                                   BillingInvoicePropertyId = propertySupported.BillingInvoicePropertyId,
                                   PropertyId = propertySupported.PropertyId
                               }).ToList();

            return dbBaseQuery;
        }

        public BillingInvoicePropertyDto GetChildByParentId(Guid parentId)
        {
            return (from childBillingInvoiceProperty in Context.BillingInvoiceProperties
                    where childBillingInvoiceProperty.BillingInvoicePropertyReferenceId == parentId
                    select new BillingInvoicePropertyDto
                    {
                        Id = childBillingInvoiceProperty.Id,
                        BillingInvoiceModelId = childBillingInvoiceProperty.BillingInvoiceModelId,
                        BillingInvoicePropertySeries = childBillingInvoiceProperty.BillingInvoicePropertySeries,
                        Description = childBillingInvoiceProperty.Description,
                        EmailAlert = childBillingInvoiceProperty.EmailAlert,
                        IsActive = childBillingInvoiceProperty.IsActive,
                        IsIntegrated = childBillingInvoiceProperty.IsIntegrated,
                        LastNumber = childBillingInvoiceProperty.LastNumber,
                        PropertyId = childBillingInvoiceProperty.PropertyId,
                        BillingInvoicePropertyReferenceId = childBillingInvoiceProperty.BillingInvoicePropertyReferenceId,
                        IntegrationId = childBillingInvoiceProperty.IntegrationId
                    }).FirstOrDefault();
        }
    }
}
