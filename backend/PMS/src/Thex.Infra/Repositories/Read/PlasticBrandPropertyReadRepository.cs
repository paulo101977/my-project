﻿//  <copyright file="PlasticBrandPropertyReadRepository.cs" company="TOTVS">
//  Copyright (c) TOTVS. All rights reserved.
//  </copyright>
namespace Thex.EntityFrameworkCore.Repositories
{
    using Thex.Domain.Entities;
    using Thex.Infra.ReadInterfaces;
    using Tnf.Dto;
    using Tnf.EntityFrameworkCore.Repositories;
    using Tnf.EntityFrameworkCore;
    using Thex.Dto;
    using Tnf.Localization;
    using Thex.Common;
    using Thex.Common.Enumerations;
    using Microsoft.EntityFrameworkCore;
    using System;
    using Thex.Dto.PlasticBrandProperty;
    using System.Linq;
    using Thex.Infra.Context;

    public class PlasticBrandPropertyReadRepository : EfCoreRepositoryBase<ThexContext, PlasticBrandProperty>, IPlasticBrandPropertyReadRepository
    {


        public PlasticBrandPropertyReadRepository(
            IDbContextProvider<ThexContext> dbContextProvider)
            : base(dbContextProvider)
        {

        }

        public PlasticBrandPropertyResultDto GetPlasticBrandDetails(Guid? plasticBrandPropertyId)
        {
            return (from plasticBrandProperty in Context.PlasticBrandProperties
                    join plasticBrand in Context.PlasticBrands on plasticBrandProperty.PlasticBrandId equals plasticBrand.Id
                    where plasticBrandProperty.Id == plasticBrandPropertyId
                    select new PlasticBrandPropertyResultDto
                    {
                        Id = plasticBrandProperty.Id,
                        PlasticBrandId = plasticBrand.Id,
                        PlasticBrandName = plasticBrand.PlasticBrandName
                    }).SingleOrDefault();
        }

        public IListDto<PlasticBrandPropertyResultDto> GetAllByPropertyId(GetAllPlasticBrandPropertyDto request)
        {
            var result = (from pbp in Context.PlasticBrandProperties.AsNoTracking()
                          join pb in Context.PlasticBrands.AsNoTracking()
                          on pbp.PlasticBrandId equals pb.Id
                          where pbp.PropertyId == request.PropertyId && pbp.IsActive
                          select new PlasticBrandPropertyResultDto
                          {
                              Id = pbp.Id,
                              PlasticBrandId = pb.Id,
                              PlasticBrandName = pb.PlasticBrandName
                          }).ToList();

            return new ListDto<PlasticBrandPropertyResultDto>
            {
                HasNext = false,
                Items = result,
            };
        }
    }
}