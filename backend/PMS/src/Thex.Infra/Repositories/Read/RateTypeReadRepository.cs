﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Thex.Common;
using Thex.Common.Enumerations;
using Thex.Domain.Entities;
using Thex.Dto;
using Thex.Infra.ReadInterfaces;

using Tnf.Dto;
using Tnf.EntityFrameworkCore.Repositories;
using Tnf.EntityFrameworkCore;
using Tnf.Localization;
using Thex.Infra.Context;
using Thex.Dto;

namespace Thex.Infra.Repositories.Read
{
    public class RateTypeReadRepository : EfCoreRepositoryBase<ThexContext, RateType>, IRateTypeReadRepository
    {
        private readonly ILocalizationManager _localizationManager;

        public RateTypeReadRepository(IDbContextProvider<ThexContext> dbContextProvider, ILocalizationManager localizationManager) : base(dbContextProvider)
        {
            _localizationManager = localizationManager;
        }

        public ListDto<RateTypeDto> GetAllRateType()
        {
            var dbBaseQuery = Context.RateTypes.ToList();


            var RateTypes = new List<RateTypeDto>();


            foreach (var RateType in dbBaseQuery.ToList())
            {
                RateType.RateTypeName = _localizationManager.GetString(AppConsts.LocalizationSourceName, ((RatePlanTypeEnum)RateType.Id).ToString());
                RateTypes.Add(RateType.MapTo<RateTypeDto>());
            }


            return new ListDto<RateTypeDto>
            {
                HasNext = false,
                Items = RateTypes,
            };
        }
    }
}
