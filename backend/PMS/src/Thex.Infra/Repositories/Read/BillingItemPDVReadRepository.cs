﻿using System;
using System.Collections.Generic;
using System.Text;
using Thex.Infra.ReadInterfaces;
using Tnf.EntityFrameworkCore.Repositories;
using System.Linq;
using Thex.Dto;
using Tnf.Dto;

using Thex.Domain.Entities;
using Tnf.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Thex.Dto.BillingItem;
using Thex.Common.Enumerations;
using Thex.Common;
using Tnf.Localization;
using System.Linq.Expressions;
using Thex.Infra.Context;

namespace Thex.EntityFrameworkCore.Repositories
{
    public class BillingItemPDVReadRepository : EfCoreRepositoryBase<ThexContext, BillingItem>, IBillingItemPDVReadRepository
    {
        private readonly ILocalizationManager _localizationManager;

        public BillingItemPDVReadRepository(IDbContextProvider<ThexContext> dbContextProvider, ILocalizationManager localizationManager)
            : base(dbContextProvider)
        {
            _localizationManager = localizationManager;
        }

        public List<BillingItem> GetBillingItemsByIds(List<int> billingItemsIds)
        {
            return Context.BillingItems.Where(x => billingItemsIds.Contains(x.Id)).ToList();
        }

        public IListDto<GetAllBillingItemServiceDto> GetAllBillingItemService(long propertyId)
        {
            var dbBaseQuery = (from b in Context
                                .BillingItems
                                
                               join  g in Context.BillingItemCategories on b.BillingItemCategoryId equals g.Id 
                               where b.PropertyId == propertyId && b.BillingItemTypeId == (int)BillingItemTypeEnum.PointOfSale
                                  && !g.IsDeleted && !b.IsDeleted
                               select new GetAllBillingItemServiceDto()
                               {
                                   Id = b.Id,
                                   BillingItemName = b.BillingItemName,
                                   BillingItemCategoryName = g.CategoryName,
                                   IsActive = b.IsActive
                               });

            var result = dbBaseQuery.ToList();

            var resultListDto = new ListDto<GetAllBillingItemServiceDto>();

            foreach (var res in result)
            {
                resultListDto.Items.Add(res.MapTo<GetAllBillingItemServiceDto>());
            }

            resultListDto.HasNext = false;

            return resultListDto;
        }

        public IListDto<GetAllBillingItemTaxDto> GetAllBillingItemTax(long propertyId)
        {
            var dbBaseQuery = (from b in Context.BillingItems
                                            
                               join g in Context.BillingItemCategories on b.BillingItemCategoryId equals g.Id
                               where g.PropertyId == propertyId && b.BillingItemTypeId == (int)BillingItemTypeEnum.Tax
                               select new GetAllBillingItemTaxDto()
                               {
                                   Id = b.Id,
                                   BillingItemName = b.BillingItemName,
                                   BillingItemCategoryName = g.CategoryName,
                                   IsActive = b.IsActive
                               });

            var result = dbBaseQuery.ToList();

            var resultListDto = new ListDto<GetAllBillingItemTaxDto>();

            foreach (var res in result)
            {
                resultListDto.Items.Add(res.MapTo<GetAllBillingItemTaxDto>());
            }

            resultListDto.HasNext = false;

            return resultListDto;

        }

        public BillingItemServiceDto GetBillingItem(long id, long propertyId)
        {
            var resultBillingItem = (from b in Context
                                     .BillingItems
                                     
                                     join g in Context.BillingItemCategories on b.BillingItemCategoryId equals g.Id
                                     where b.Id == id && g.PropertyId == propertyId
                                     select b).FirstOrDefault();
            var queryResultBillingTax = (from b in Context.BillingTaxs
                                         join c in Context.BillingItems on b.BillingItemId equals c.Id
                                         join d in Context.BillingItems on b.BillingItemTaxId equals d.Id
                                         join g in Context.BillingItemCategories on c.BillingItemCategoryId equals g.Id
                                         join h in Context.BillingItemCategories on d.BillingItemCategoryId equals h.Id
                                         where b.BillingItemId == id || b.BillingItemTaxId == id
                                         select new BillingItemServiceAndTaxAssociateDto
                                         {
                                             Id = b.Id,
                                             BillingItemTaxId = b.BillingItemTaxId,
                                             BillingItemServiceId = b.BillingItemId,
                                             Name = id == c.Id ? d.BillingItemName : c.BillingItemName,
                                             TaxPercentage = b.TaxPercentage,
                                             IsActive = b.IsActive,
                                             BeginDate = b.BeginDate,
                                             EndDate = b.EndDate,
                                             CategoryName = id == c.Id ? h.CategoryName : g.CategoryName
                                         }).ToList();

            var result = new BillingItemServiceDto
            {
                Id = resultBillingItem.Id,
                BillingItemCategoryId = resultBillingItem.BillingItemCategoryId.Value,
                IsActive = resultBillingItem.IsActive,
                Name = resultBillingItem.BillingItemName,
                ServiceAndTaxList = queryResultBillingTax.ToList(),
                IntegrationCode = resultBillingItem.IntegrationCode
            };
            return result;
        }

        public IListDto<GetAllBillingItemForAssociateServiceDto> GetAllBillingItemForAssociateService(long propertyId)
        {
            var dbBaseQuery = (from b in Context
                               .BillingItems
                               
                               join g in Context.BillingItemCategories on b.BillingItemCategoryId equals g.Id
                               where g.PropertyId == propertyId && b.BillingItemTypeId == (int)BillingItemTypeEnum.PointOfSale
                               && b.IsActive
                               select new GetAllBillingItemForAssociateServiceDto()
                               {
                                   Id = b.Id,
                                   BillingItemName = b.BillingItemName,
                                   BillingItemCategoryName = g.CategoryName

                               });

            var result = dbBaseQuery.ToList();

            var resultListDto = new ListDto<GetAllBillingItemForAssociateServiceDto>();

            foreach (var res in result)
            {
                resultListDto.Items.Add(res.MapTo<GetAllBillingItemForAssociateServiceDto>());
            }

            resultListDto.HasNext = false;

            return resultListDto;

        }

        public IListDto<GetAllBillingItemForAssociateTaxDto> GetAllBillingItemForAssociateTax(long propertyId)
        {
            var dbBaseQuery = (from b in Context
                               .BillingItems
                               
                               join g in Context.BillingItemCategories on b.BillingItemCategoryId equals g.Id
                               where g.PropertyId == propertyId && b.BillingItemTypeId == (int)BillingItemTypeEnum.Tax
                               && b.IsActive
                               select new GetAllBillingItemForAssociateTaxDto()
                               {
                                   Id = b.Id,
                                   BillingItemName = b.BillingItemName,
                                   BillingItemCategoryName = g.CategoryName,
                               });

            var result = dbBaseQuery.ToList();

            var resultListDto = new ListDto<GetAllBillingItemForAssociateTaxDto>();

            foreach (var res in result)
            {
                resultListDto.Items.Add(res.MapTo<GetAllBillingItemForAssociateTaxDto>());
            }

            resultListDto.HasNext = false;

            return resultListDto;

        }

        public List<BillingItemServiceAndTaxAssociateDto> GetBillingTaxForBiilingItem(int id)
        {
            var queryResultBillingTax = (from b in Context.BillingTaxs
                                         join c in Context.BillingItems on b.BillingItemId equals c.Id
                                         join d in Context.BillingItems on b.BillingItemTaxId equals d.Id
                                         join g in Context.BillingItemCategories on c.BillingItemCategoryId equals g.Id
                                         join h in Context.BillingItemCategories on d.BillingItemCategoryId equals h.Id
                                         where b.BillingItemId == id || b.BillingItemTaxId == id
                                         select new BillingItemServiceAndTaxAssociateDto
                                         {
                                             Id = b.Id,
                                             BillingItemTaxId = b.BillingItemTaxId,
                                             BillingItemServiceId = b.BillingItemId,
                                             Name = id == c.Id ? d.BillingItemName : c.BillingItemName,
                                             TaxPercentage = b.TaxPercentage,
                                             IsActive = b.IsActive,
                                             BeginDate = b.BeginDate,
                                             EndDate = b.EndDate,
                                             CategoryName = id == c.Id ? h.CategoryName : g.CategoryName
                                         }).ToList();

            return queryResultBillingTax;
        }

        public List<Guid> GetBillingTaxByBillingItemId(int id)
        {
            return Context.BillingTaxs.Where(b => b.BillingItemId == id || b.BillingItemTaxId == id).Select(s => s.Id).ToList();
        }

        public bool ValidateActiveForTax(int id)
        {
            return Context.BillingItems.Any(x => !x.IsActive && x.Id == id);

        }

        public IListDto<BillingItemServiceLancamentoDto> GetAllServiceWithAssociateTaxForDebitRealease(int propertyId, GetAllBillingItemServiceForRealase request)
        {
            var dbBaseQuery = (from service in Context.BillingItems
                               join category in Context.BillingItemCategories on service.BillingItemCategoryId equals category.Id
                               join supportedType in Context.BillingInvoicePropertySupportedTypes on service.Id equals supportedType.BillingItemId
                               where category.PropertyId == propertyId && service.BillingItemTypeId == (int)BillingItemTypeEnum.PointOfSale
                                 && service.IsActive
                               select new BillingItemServiceLancamentoDto()
                               {
                                   Id = service.Id,
                                   Name = service.BillingItemName,
                                   billingItemCategoryName = category.CategoryName,
                                   BillingItemCategoryId = category.Id

                               });

            if (!String.IsNullOrEmpty(request.SearchData))
                dbBaseQuery = dbBaseQuery.Where(b => b.Name.Contains(request.SearchData));


            var totalItemsDto = dbBaseQuery.Count();

            var resultDto = dbBaseQuery.ToList();

            var result =
                new ListDto<BillingItemServiceLancamentoDto>
                {
                    Items = resultDto,
                    HasNext = SearchBillingItemServiceHasNext(
                            request,
                            totalItemsDto,
                            resultDto),
                };


            return result;

        }
        private bool SearchBillingItemServiceHasNext(GetAllBillingItemServiceForRealase request, int totalItemsDto, List<BillingItemServiceLancamentoDto> resultDto)
        {
            return totalItemsDto > ((request.Page - 1) * request.PageSize) + resultDto.Count();
        }


        public IList<BillingItemServiceAndTaxAssociateDto> GetTaxesForServices(IList<int> serviceIdsList)
        {
            var dbBaseQuery = (from tax in Context.BillingTaxs
                               join service in Context.BillingItems on tax.BillingItemTaxId equals service.Id
                               where tax.IsActive && serviceIdsList.Contains(tax.BillingItemId)
                               select new BillingItemServiceAndTaxAssociateDto()
                               {
                                   BillingItemTaxId = tax.BillingItemTaxId,
                                   BillingItemServiceId = tax.BillingItemId,
                                   Name = service.BillingItemName,
                                   TaxPercentage = tax.TaxPercentage
                               }
                   );

            if (dbBaseQuery != null) return dbBaseQuery.ToList();

            return null;
        }

        public BillingItemPaymentTypeDto GetByPaymentTypeIdAndAcquirerId(int propertyId, int paymentTypeId, Guid acquirerId)
        {
            var billingItems = Context.BillingItems
                .Include(e => e.PaymentType)
                .Include(e => e.Acquirer)
                .Include(e => e.PlasticBrandProperty).ThenInclude(e => e.PlasticBrand)
                .Include(e => e.BillingItemPaymentConditionList)
                .Where(e => e.PropertyId == propertyId &&
                        e.PaymentTypeId == paymentTypeId &&
                        e.AcquirerId == acquirerId)
                .ToList();

            if (billingItems == null || billingItems.Count() == 0)
                return null;

            var result = new BillingItemPaymentTypeDto();

            foreach (var billingItem in billingItems)
            {
                result.BillingItemPaymentTypeDetailList.Add(new BillingItemPaymentTypeDetailDto
                {
                    Id = billingItem.Id,
                    PlasticBrandPropertyId = billingItem.PlasticBrandPropertyId.Value,
                    PlasticBrandPropertyName = (billingItem.PlasticBrandProperty != null && billingItem.PlasticBrandProperty.PlasticBrand != null ?
                                                    billingItem.PlasticBrandProperty.PlasticBrand.PlasticBrandName
                                                    : null),
                    PlasticBrandId = (billingItem.PlasticBrandProperty != null && billingItem.PlasticBrandProperty.PlasticBrand != null ?
                                                            billingItem.PlasticBrandProperty.PlasticBrand.Id
                                                            : (int?)null),
                    IsActive = billingItem.IsActive,
                    BillingItemPaymentTypeId = billingItem.PaymentTypeId.Value,
                    MaximumInstallmentsQuantity = billingItem.MaximumInstallmentsQuantity.Value,
                    BillingItemPaymentTypeDetailConditionList = billingItem.BillingItemPaymentConditionList.Any() ?
                        billingItem.BillingItemPaymentConditionList.Select(a => new BillingItemAcquirerPaymentTypeDetailConditionDto
                        {
                            Id = a.Id,
                            CommissionPct = a.CommissionPct,
                            InstallmentNumber = a.InstallmentNumber,
                            PaymentDeadline = a.PaymentDeadline,
                            BillingItemPaymentTypeDetailId = billingItem.Id
                        }).ToList() : null
                });
            }

            var firstResult = billingItems.First();
            result.Id = 0;
            result.IsActive = billingItems.Any(e => e.IsActive);
            result.PaymentTypeId = firstResult.PaymentTypeId.Value;
            result.PaymentTypeName = _localizationManager.GetString(AppConsts.LocalizationSourceName, ((PaymentTypeEnum)firstResult.PaymentTypeId.Value).ToString());
            result.PropertyId = firstResult.PropertyId.Value;
            result.Description = firstResult.Description;
            result.AcquirerName = firstResult.Acquirer.TradeName;
            result.AcquirerId = firstResult.AcquirerId;

            return result;
        }

        public BillingItemPaymentTypeDto GetByPaymentTypeId(int propertyId, int paymentTypeId)
        {
            var billingItem = Context.BillingItems
                .Include(e => e.PaymentType)
                .Include(e => e.Acquirer)
                .Include(e => e.PlasticBrandProperty).ThenInclude(e => e.PlasticBrand)
                .Include(e => e.BillingItemPaymentConditionList)
                .Where(e => e.PropertyId == propertyId &&
                        e.PaymentTypeId == paymentTypeId)
                .FirstOrDefault();

            if (billingItem == null)
                return null;

            return new BillingItemPaymentTypeDto()
            {
                Id = billingItem.Id,
                IsActive = billingItem.IsActive,
                PaymentTypeId = billingItem.PaymentTypeId.Value,
                PaymentTypeName = _localizationManager.GetString(AppConsts.LocalizationSourceName, ((PaymentTypeEnum)billingItem.PaymentTypeId.Value).ToString()),
                PropertyId = billingItem.PropertyId.Value,
                Description = billingItem.Description
            };
        }

        public IListDto<BillingItemPaymentTypeDto> GetAllPaymentTypeIdByPropertyId(int propertyId)
        {
            var resultList = new List<BillingItemPaymentTypeDto>();

            var billingItems = Context.BillingItems
                .Include(e => e.PaymentType)
                .Include(e => e.Acquirer)
                .Include(e => e.PlasticBrandProperty).ThenInclude(e => e.PlasticBrand)
                .Include(e => e.BillingItemPaymentConditionList)
                .Where(e => e.PropertyId == propertyId && e.PaymentTypeId.HasValue)
                .ToList();

            if (billingItems != null && billingItems.Count > 0)
            {
                var paymentTypes = billingItems.Select(d => new
                {
                    PaymentTypeId = d.PaymentTypeId,
                    AcquirerId = d.AcquirerId.HasValue ? d.AcquirerId.Value : Guid.Empty
                }).Distinct().ToList();

                foreach (var payment in paymentTypes)
                {
                    var result = new BillingItemPaymentTypeDto();
                    var groupItems = billingItems.Where(d => d.PaymentTypeId == payment.PaymentTypeId && (!d.AcquirerId.HasValue || (d.AcquirerId.HasValue && d.AcquirerId.Value == payment.AcquirerId)));

                    foreach (var billingItem in groupItems)
                    {
                        result.BillingItemPaymentTypeDetailList.Add(new BillingItemPaymentTypeDetailDto
                        {
                            Id = billingItem.Id,
                            PlasticBrandPropertyId = billingItem.PlasticBrandPropertyId.HasValue ? billingItem.PlasticBrandPropertyId.Value : Guid.Empty,
                            PlasticBrandPropertyName = (billingItem.PlasticBrandProperty != null && billingItem.PlasticBrandProperty.PlasticBrand != null ?
                                                            billingItem.PlasticBrandProperty.PlasticBrand.PlasticBrandName
                                                            : null),
                            PlasticBrandId = (billingItem.PlasticBrandProperty != null && billingItem.PlasticBrandProperty.PlasticBrand != null ?
                                                            billingItem.PlasticBrandProperty.PlasticBrand.Id
                                                            : (int?)null),
                            IsActive = billingItem.IsActive,
                            BillingItemPaymentTypeId = billingItem.PaymentTypeId.Value,
                            MaximumInstallmentsQuantity = billingItem.MaximumInstallmentsQuantity.HasValue ? billingItem.MaximumInstallmentsQuantity.Value : (short)0
                        });
                    }

                    var firstResult = groupItems.First();
                    result.Id = 0;
                    result.IsActive = groupItems.Any(e => e.IsActive);
                    result.PaymentTypeId = firstResult.PaymentTypeId.Value;
                    result.PaymentTypeName = _localizationManager.GetString(AppConsts.LocalizationSourceName, ((PaymentTypeEnum)firstResult.PaymentTypeId.Value).ToString());
                    result.PropertyId = firstResult.PropertyId.Value;
                    result.Description = firstResult.Description;
                    result.AcquirerName = firstResult.Acquirer != null ? firstResult.Acquirer.TradeName : null;
                    result.AcquirerId = firstResult.AcquirerId;

                    resultList.Add(result);

                }

            }

            return new ListDto<BillingItemPaymentTypeDto>()
            {
                HasNext = false,
                Items = resultList,
            };
        }

        public List<BillingItem> GetAllByExpression(Expression<Func<BillingItem, bool>> exp)
        {
            return Context.BillingItems.AsNoTracking().Where(exp).ToList();
        }


        public List<BillingItemPaymentCondition> GetAllBillingItemPaymentConditionByBillingItemId(int billingItemId)
        {
            return Context.BillingItemPaymentConditions.AsNoTracking().Where(e => e.BillingItemId == billingItemId).ToList();
        }

        public bool AnyBillingItemByExpression(Expression<Func<BillingItem, bool>> exp)
        {
            return Context.BillingItems.AsNoTracking().Any(exp);
        }

        public bool AnyBillingItemByBillingItemType(int billingItemId, int billingItemType)
        {
            return Context.BillingItems.Any(exp => exp.IsActive && exp.Id == billingItemId && exp.BillingItemTypeId == billingItemType);
        }

        public IListDto<BillingItemPlasticBrandCompanyClientDto> GetPlasticBrandWithCompanyClientByPaymentType(int paymentTypeId)
        {
            var dbBaseQuery = (from p in Context.BillingItems
                               join pbp in Context.PlasticBrandProperties on p.PlasticBrandPropertyId equals pbp.Id
                               join pb in Context.PlasticBrands on pbp.PlasticBrandId equals pb.Id
                               join cc in Context.CompanyClients on p.AcquirerId equals cc.Id
                               where p.IsActive &&
                               pbp.IsActive &&
                               cc.IsActive &&
                               //cc.IsAcquirer &&
                               p.PaymentTypeId == paymentTypeId
                               select new BillingItemPlasticBrandCompanyClientDto
                               {
                                   Id = p.Id,
                                   AcquirerId = cc.CompanyId,
                                   PlasticBrandPropertyId = pbp.Id,
                                   PlasticBrandId = pb.Id,
                                   PlasticBrandName = pb.PlasticBrandName,
                                   AcquirerName = cc.TradeName,
                                   MaximumInstallmentsQuantity = p.MaximumInstallmentsQuantity
                               });

            var resultDto = dbBaseQuery.ToList();

            var result =
                new ListDto<BillingItemPlasticBrandCompanyClientDto>
                {
                    Items = resultDto,
                    HasNext = false,
                };

            return result;

        }

        public decimal GetSumBillingAccountItemsByBillingAccountId(Guid billingAccountId)
        {
            return Context.BillingAccountItems
                .Where(e => e.BillingAccountId == billingAccountId)
                .Sum(e => e.Amount);
        }

        public decimal GetSumBillingAccountItemsByBillingAccountIdList(List<Guid> billingAccountId)
        {
            return Context.BillingAccountItems
                .Where(e => billingAccountId.Contains(e.BillingAccountId))
                .Sum(e => e.Amount);
        }

        public bool CheckPaymentTypeByBillingItemId(int billingItemId, int paymentTypeId)
        {
            return Context.BillingItems.Any(exp => exp.IsActive && exp.Id == billingItemId && exp.PaymentTypeId == paymentTypeId);
        }

        public bool CheckIfExceedingMaximumInstallmentsQuantity(int billingItemId, int installmentsQuantity)
        {
            if (installmentsQuantity <= 0)
                return true;

            return !Context.BillingItems.Any(exp => exp.IsActive && exp.Id == billingItemId && installmentsQuantity <= exp.MaximumInstallmentsQuantity);
        }

        public List<BillingItemDto> GetAllServiceFilterTypeDailyByProperty(int propertyId)
        {
            var dbBaseQuery = (from billingItem in Context.BillingItems
                               where billingItem.BillingItemTypeId == (int)BillingItemTypeEnum.PointOfSale
                               select new BillingItemDto
                               {
                                   Id = billingItem.Id,
                                   BillingItemName = billingItem.BillingItemName
                               }).ToList();

            return dbBaseQuery;
        }

        public ListDto<BillingItemWithParametersDocumentsDto> GetAllServiceWithParameters(int propertyId)
        {

            var billingItemTypeEnum = new int[]
            {
                (int)BillingItemTypeEnum.Service,
                (int)BillingItemTypeEnum.PointOfSale              
            };

            var dbBaseQuery = (from billing in Context.BillingItems

                               join billingInvoicePropertySupportedType in Context.BillingInvoicePropertySupportedTypes on billing.Id equals billingInvoicePropertySupportedType.BillingItemId into a
                               from leftbillingInvoicePropertySupportedType in a.DefaultIfEmpty()

                               join billingInvoiceProperty in Context.BillingInvoiceProperties on leftbillingInvoicePropertySupportedType.BillingInvoicePropertyId equals billingInvoiceProperty.Id into b
                               from leftbillingInvoiceProperty in b.DefaultIfEmpty()

                               join billingInvoiceModel in Context.BillingInvoiceModels on leftbillingInvoiceProperty.BillingInvoiceModelId equals billingInvoiceModel.Id into c
                               from leftbillingInvoiceModel in c.DefaultIfEmpty()

                               join countrySubdvisionServices in Context.CountrySubdvisionServices on leftbillingInvoicePropertySupportedType.CountrySubdvisionServiceId equals countrySubdvisionServices.Id into d
                               from leftcountrySubdvisionServices in d.DefaultIfEmpty()

                               join countrySubdivision in Context.CountrySubdivisions on leftcountrySubdvisionServices.CityId equals countrySubdivision.Id into e
                               from leftcountrySubdivision in e.DefaultIfEmpty()

                               join location in Context.Locations on leftcountrySubdivision.Id equals location.CityId into f
                               from leftlocation in f.DefaultIfEmpty()

                               join property in Context.Properties on leftlocation.OwnerId equals property.PropertyUId into g
                               from leftproperty in g.DefaultIfEmpty()

                               where billing.PropertyId == propertyId && billingItemTypeEnum.Contains(billing.BillingItemTypeId)

                               select new BillingItemWithParametersDocumentsDto
                               {
                                   BillingItemId = billing.Id,
                                   BillingItemName = billing.BillingItemName,
                                   BillingItemTypeId = billing.BillingItemTypeId,
                                   BillingInvoiceModelName = leftbillingInvoiceModel != null ? leftbillingInvoiceModel.Description : null,
                                   BillingInvoicePropertySeries = leftbillingInvoiceProperty != null ? leftbillingInvoiceProperty.BillingInvoicePropertySeries : null,
                                   Code = leftcountrySubdvisionServices != null ? leftcountrySubdvisionServices.Code : null
                               });

            var result = new ListDto<BillingItemWithParametersDocumentsDto>
            {
                HasNext = false,
                Items = dbBaseQuery.ToList(),
            };

            return result;
        }

        public bool IntegrationCodeIsAvailable(string integrationCode, int billingItemId, int propertyId)
        {
            return !Context.BillingItems.Any(
                bi => bi.IntegrationCode.ToLower().Equals(integrationCode.ToLower())
                       && bi.Id != billingItemId && bi.PropertyId == propertyId);
        }

        public bool IsServiceAndNotExistSupportedType(int billingItemId)
        {
            //SearchBillingItemServiceHasNext o item for de serviço E não tem supportedtype 
            return Context
                    .BillingItems
                    .Include(e => e.BillingInvoicePropertySupportedTypeList)
                    .Any(e => e.Id == billingItemId &&
                        e.BillingItemTypeId == (int)BillingItemTypeEnum.PointOfSale &&
                        e.BillingInvoicePropertySupportedTypeList == null || e.BillingInvoicePropertySupportedTypeList.Count == 0);
        }

    }
}
