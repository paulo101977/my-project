﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Thex.Domain.Entities;
using Thex.Dto;
using Thex.Infra.Context;
using Thex.Infra.ReadInterfaces;
using Tnf.Dto;
using Tnf.EntityFrameworkCore;
using Tnf.EntityFrameworkCore.Repositories;

namespace Thex.Infra.Repositories.Read
{
    public class ChannelReadRepository : EfCoreRepositoryBase<ThexContext, Channel>, IChannelReadRepository
    {
        public ChannelReadRepository(IDbContextProvider<ThexContext> dbContextProvider) : base(dbContextProvider)
        {

        }

        public Channel GetById(Guid id)
        {
            var channel = base.Get(new DefaultGuidRequestDto(id));
            return channel;
        }

        public ChannelDto GetDtoById(Guid id)
        {
            var channel = Context.Channels.Include(c => c.ChannelCode).Include(c => c.BusinessSource).Include(c => c.MarketSegment).FirstOrDefault(x => x.Id == id);
            if (channel == null) return null;        

           return new ChannelDto
            {
                Id = channel.Id,
                BusinessSourceId = channel.BusinessSourceId,
                BusinessSourceName = channel.BusinessSource?.Description,
                ChannelCodeName = channel.ChannelCode.ChannelCode1,
                ChannelCodeId = channel.ChannelCode.Id,
                Description = channel.Description,
                DistributionAmount = channel.DistributionAmount,
                IsActive = channel.IsActive,
                MarketSegmentName = channel.MarketSegment?.Name,
                MarketSegmentId = channel.MarketSegment != null ? channel.MarketSegment.Id : default(int)
            };             
        }

        public async Task<IListDto<ChannelDto>> GetAllChannelsDtoByTenantIdAsync(GetAllChannelDto requestDto, Guid tenantId)
        {
            var baseQuery = (from channel in Context.Channels.AsNoTracking()
                           join channelCode in Context.ChannelCodes.AsNoTracking() on channel.ChannelCodeId equals channelCode.Id
                           join businessSource in Context.BusinessSources.AsNoTracking() on channel.BusinessSourceId equals businessSource.Id into a
                           from businessSource in a.DefaultIfEmpty()
                           join marketSegment in Context.MarketSegments.AsNoTracking() on channel.MarketSegmentId equals marketSegment.Id into cm
                           from marketSegment in cm.DefaultIfEmpty()
                           where channel.TenantId == tenantId
                           select new ChannelDto
                           {
                               Id = channel.Id,
                               BusinessSourceId = businessSource.Id,
                               BusinessSourceName = businessSource.Description,
                               ChannelCodeName = channelCode.ChannelCode1,
                               ChannelCodeId = channelCode.Id,
                               Description = channel.Description,
                               DistributionAmount = channel.DistributionAmount,
                               IsActive = channel.IsActive,
                               MarketSegmentName = marketSegment != null ? marketSegment.Name : string.Empty,
                               MarketSegmentId = marketSegment != null ? marketSegment.Id : default(int)
                           });

            if(requestDto != null)
            {
                if (!requestDto.All)
                    baseQuery = baseQuery.Where(c => c.IsActive);

                if (!string.IsNullOrWhiteSpace(requestDto.SearchData))
                    baseQuery = baseQuery.Where(c => c.ChannelCodeName.Contains(requestDto.SearchData.ToLower())
                        || c.Description.ToLower().Contains(requestDto.SearchData.ToLower()));
            }

            return new ListDto<ChannelDto>
            {
                HasNext = false,
                Items = await baseQuery.ToListAsync()
            };
        }

        public async Task<List<Channel>> GetAllChannelsByTenantIdAsync(Guid tenantId)
        {
            var dbBaseQuery = await Context.Channels
                    .Where(r => r.TenantId == tenantId)
                    .OrderBy(r => r.ChannelCode.ChannelCode1)
                    .ToListAsync();

            return dbBaseQuery;

        }

        public IListDto<ChannelDto> SearchChannels(string search)
        {
            var channels = new List<ChannelDto>();

            if (!string.IsNullOrWhiteSpace(search))
            {
                channels = Context.Channels.Include(x => x.ChannelCode)
                .Include(x => x.BusinessSource).Where(x => x.IsActive &&
                  (x.Description.ToLower().Contains(search.ToLower()) ||
                   x.ChannelCode.ChannelCode1.ToLower().Contains(search.ToLower())))
                .Select(x => new ChannelDto
                {
                    Id = x.Id,
                    BusinessSourceId = x.BusinessSource.Id,
                    BusinessSourceName = x.BusinessSource.Description,
                    ChannelCodeName = x.ChannelCode.ChannelCode1,
                    ChannelCodeId = x.ChannelCode.Id,
                    Description = x.Description,
                    DistributionAmount = x.DistributionAmount,
                    IsActive = x.IsActive
                }).ToList();
            }

            

            return new ListDto<ChannelDto>
            {
                HasNext = false,
                Items = channels
            };
        }
    }
}
