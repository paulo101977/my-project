﻿//  <copyright file="GuestRegistrationReadRepository.cs" company="TOTVS">
//  Copyright (c) TOTVS. All rights reserved.
//  </copyright>
namespace Thex.EntityFrameworkCore.Repositories
{
    using System;
    using Thex.Domain.Entities;
    using Thex.Infra.ReadInterfaces;
    using System.Linq;
    using Tnf.EntityFrameworkCore.Repositories;
    using Tnf.EntityFrameworkCore;
    using Thex.Dto.GuestRegistration;
    using Thex.Domain.Extensions;
    using Thex.Dto;

    using Thex.Dto.CountrySubdivisionTranslation;
    using Tnf.Dto;
    using Thex.Common.Enumerations;
    using Thex.Infra.Context;
    using Thex.Kernel;
    using Microsoft.EntityFrameworkCore;
    using System.Collections.Generic;

    public class GuestRegistrationReadRepository : EfCoreRepositoryBase<ThexContext, GuestRegistration>, IGuestRegistrationReadRepository
    {
        private IApplicationUser _applicationUser;

        public GuestRegistrationReadRepository(IDbContextProvider<ThexContext> dbContextProvider, IApplicationUser applicationUser)
            : base(dbContextProvider)
        {
            _applicationUser = applicationUser;
        }

        public GuestRegistrationResultDto GetGuestRegistrationByGuestReservationItemId(long propertyId, long guestReservationItemId, string languageIsoCode)
        {
            var queryResult =
                (from gr in Context.GuestReservationItems

                 join guestRegistration in Context.GuestRegistrations on gr.GuestRegistrationId equals guestRegistration.Id into guestRegistrations
                 from leftJoinGuestRegistration in guestRegistrations.DefaultIfEmpty()

                 join g in Context.Guests on gr.GuestId equals g.Id into guests
                 from leftJoinGuest in guests.DefaultIfEmpty()

                 join l in Context.Locations on leftJoinGuestRegistration.Id equals l.OwnerId into locations
                 from leftLocation in locations.DefaultIfEmpty()

                 join lc in Context.LocationCategories on leftLocation.LocationCategoryId equals lc.Id into locationCategories
                 from leftLocationCategory in locationCategories.DefaultIfEmpty()

                 join c in Context.CountrySubdivisions on leftLocation.CityId equals c.Id into city
                 from leftCity in city.DefaultIfEmpty()

                 join s in Context.CountrySubdivisions on leftCity.ParentSubdivision.Id equals s.Id into state
                 from leftState in state.DefaultIfEmpty()

                 join co in Context.CountrySubdivisions on leftJoinGuestRegistration.Nationality equals co.Id into country
                 from leftCountry in country.DefaultIfEmpty()

                     // person
                 join p in Context.Persons on leftJoinGuest.PersonId equals p.Id into person
                 from leftPerson in person.DefaultIfEmpty()

                 join ct in Context.CountrySubdivisionTranslations on
                 new { ct1 = leftCity.Id, ct2 = languageIsoCode } equals
                 new { ct1 = ct.CountrySubdivisionId, ct2 = ct.LanguageIsoCode.ToLower() } into cityTranslation
                 from leftCityTranslation in cityTranslation.DefaultIfEmpty()

                 join st in Context.CountrySubdivisionTranslations on
                 new { st1 = leftState.Id, st2 = languageIsoCode } equals
                 new { st1 = st.CountrySubdivisionId, st2 = st.LanguageIsoCode.ToLower() } into stateTranslation
                 from leftStateTranslation in stateTranslation.DefaultIfEmpty()

                 join cot in Context.CountrySubdivisionTranslations on
                 new { cot1 = leftCountry.Id, cot2 = languageIsoCode } equals
                 new { cot1 = cot.CountrySubdivisionId, cot2 = cot.LanguageIsoCode.ToLower() } into countryTranslation
                 from leftCountryTranslation in countryTranslation.DefaultIfEmpty()


                 where gr.Id == guestReservationItemId

                 select new
                 {
                     //guestreservationItem
                     GuestReservationItemId = gr.Id,
                     //guestRegistration
                     Id = leftJoinGuestRegistration != null ? leftJoinGuestRegistration.Id : Guid.Empty,
                     FirstName = leftJoinGuestRegistration != null ? leftJoinGuestRegistration.FirstName : null,
                     LastName = leftJoinGuestRegistration != null ? leftJoinGuestRegistration.LastName : null,
                     FullName = leftJoinGuestRegistration != null ? leftJoinGuestRegistration.FullName : null,
                     SocialName = leftJoinGuestRegistration != null ? leftJoinGuestRegistration.SocialName : null,
                     BirthDate = leftJoinGuestRegistration != null ? leftJoinGuestRegistration.BirthDate : (DateTime?)null,
                     GuestId = gr != null ? (gr.GuestId != null ? (long)gr.GuestId : 0) : 0,
                     Email = leftJoinGuestRegistration != null ? leftJoinGuestRegistration.Email : null,
                     Gender = leftJoinGuestRegistration != null ? leftJoinGuestRegistration.Gender : null,
                     OccupationId = leftJoinGuestRegistration != null ? leftJoinGuestRegistration.OccupationId : 0,
                     GuestTypeId = leftJoinGuestRegistration != null ? leftJoinGuestRegistration.GuestTypeId : 0,
                     PhoneNumber = leftJoinGuestRegistration != null ? leftJoinGuestRegistration.PhoneNumber : null,
                     MobilePhoneNumber = leftJoinGuestRegistration != null ? leftJoinGuestRegistration.MobilePhoneNumber : null,
                     Nationality = leftJoinGuestRegistration != null ? leftJoinGuestRegistration.Nationality : 0,
                     PurposeOfTrip = leftJoinGuestRegistration != null ? leftJoinGuestRegistration.PurposeOfTrip : 0,
                     ArrivingBy = leftJoinGuestRegistration != null ? leftJoinGuestRegistration.ArrivingBy : 0,
                     ArrivingFrom = leftJoinGuestRegistration != null ? leftJoinGuestRegistration.ArrivingFrom : 0,
                     NextDestination = leftJoinGuestRegistration != null ? leftJoinGuestRegistration.NextDestination : 0,
                     ArrivingFromText = leftJoinGuestRegistration != null ? leftJoinGuestRegistration.ArrivingFromText : null,
                     NextDestinationText = leftJoinGuestRegistration != null ? leftJoinGuestRegistration.NextDestinationText : null,
                     AdditionalInformation = leftJoinGuestRegistration != null ? leftJoinGuestRegistration.AdditionalInformation : null,
                     HealthInsurance = leftJoinGuestRegistration != null ? leftJoinGuestRegistration.HealthInsurance : null,
                     ResponsibleGuestRegistrationId = leftJoinGuestRegistration != null ? leftJoinGuestRegistration.ResponsibleGuestRegistrationId : null,
                     IsLegallyIncompetent = leftJoinGuestRegistration != null ? leftJoinGuestRegistration.IsLegallyIncompetent : false,

                     Document = leftJoinGuestRegistration != null ? leftJoinGuestRegistration.DocumentInformation : null,
                     DocumentTypeId = leftJoinGuestRegistration != null && leftJoinGuestRegistration.DocumentTypeId.HasValue ? leftJoinGuestRegistration.DocumentTypeId.Value : 0,
                     PersonId = leftJoinGuest != null ? leftJoinGuest.PersonId.ToString() : null,
                     //Location
                     Location = new LocationDto
                     {
                         Id = leftLocation != null ? leftLocation.Id : 0,
                         OwnerId = leftLocation != null ? leftLocation.OwnerId : Guid.Empty,
                         Latitude = leftLocation != null ? leftLocation.Latitude : null,
                         Longitude = leftLocation != null ? leftLocation.Longitude : null,
                         StreetName = leftLocation != null ? leftLocation.StreetName : null,
                         StreetNumber = leftLocation != null ? leftLocation.StreetNumber : null,
                         AdditionalAddressDetails = leftLocation != null ? leftLocation.AdditionalAddressDetails : "",
                         PostalCode = leftLocation != null ? leftLocation.PostalCode : null,
                         CountryCode = leftLocation != null ? leftLocation.CountryCode : null,

                         //LocationCategory
                         LocationCategory = new LocationCategoryDto
                         {
                             Id = leftLocationCategory != null ? leftLocationCategory.Id : 0,
                             Name = leftLocationCategory != null ? leftLocationCategory.Name : "",
                             RecordScope = leftLocationCategory != null ? leftLocationCategory.RecordScope : ""
                         },
                         LocationCategoryId = leftLocation != null ? leftLocationCategory.Id : 0,
                         Neighborhood = leftLocation != null ? leftLocation.Neighborhood : null,
                         CityId = leftLocation != null ? leftLocation.CityId : 0,
                         City = leftLocation != null ? leftLocation.City.MapTo<CountrySubdivisionDto>() : null,
                         Division = leftStateTranslation != null ? leftState.TwoLetterIsoCode ?? leftStateTranslation.Name : null,
                         Subdivision = leftCityTranslation != null ? leftCityTranslation.Name : null,
                         Country = leftCountryTranslation != null ? leftCountryTranslation.Name : null,

                         //AddressTranslation
                         AddressTranslation = new AddressTranslationDto
                         {
                             CityId = leftCity != null ? leftCity.Id : 0,
                             CityName = leftCityTranslation != null ? leftCityTranslation.Name : "",
                             StateId = leftState != null ? leftState.Id : 0,
                             StateName = leftStateTranslation != null ? leftStateTranslation.Name : "",
                             CountryId = leftCountry != null ? leftCountry.Id : 0,
                             CountryName = leftCountryTranslation != null ? leftCountryTranslation.Name : "",
                             LanguageIsoCode = leftCityTranslation != null ? leftCityTranslation.LanguageIsoCode : "",
                             TwoLetterIsoCode = leftState != null ? leftState.TwoLetterIsoCode : "",
                             CountryTwoLetterIsoCode = leftCountry != null ? leftCountry.CountryTwoLetterIsoCode : ""
                         }
                     },
                     Person = new Person()
                     {
                         ReceiveOffers = (leftPerson != null ? leftPerson.ReceiveOffers : false),
                         SharePersonData = (leftPerson != null ? leftPerson.SharePersonData : false),
                         AllowContact = (leftPerson != null ? leftPerson.AllowContact : false),
                     },
                     IsChild = gr.IsChild,
                     ChildAge = gr.ChildAge,
                     IsMain = gr.IsMain
                 }).FirstOrDefault();

            GuestRegistrationResultDto result = null;

            if (queryResult != null)
            {
                result = new GuestRegistrationResultDto
                {
                    GuestReservationItemId = queryResult.GuestReservationItemId,
                    Id = queryResult.Id,
                    FirstName = queryResult.FirstName,
                    LastName = queryResult.LastName,
                    FullName = queryResult.FullName,
                    SocialName = queryResult.SocialName,
                    BirthDate = queryResult.BirthDate,
                    GuestId = queryResult.GuestId,
                    Email = queryResult.Email,
                    Gender = queryResult.Gender,
                    OccupationId = queryResult.OccupationId,
                    GuestTypeId = queryResult.GuestTypeId,
                    PhoneNumber = queryResult.PhoneNumber,
                    MobilePhoneNumber = queryResult.MobilePhoneNumber,
                    Nationality = queryResult.Nationality,
                    PurposeOfTrip = queryResult.PurposeOfTrip,
                    ArrivingBy = queryResult.ArrivingBy,
                    ArrivingFrom = queryResult.ArrivingFrom,
                    NextDestination = queryResult.NextDestination,
                    ArrivingFromText = queryResult.ArrivingFromText,
                    NextDestinationText = queryResult.NextDestinationText,
                    AdditionalInformation = queryResult.AdditionalInformation,
                    HealthInsurance = queryResult.HealthInsurance,
                    ResponsibleGuestRegistrationId = queryResult.ResponsibleGuestRegistrationId,
                    IsLegallyIncompetent = queryResult.IsLegallyIncompetent,

                    Document = queryResult.Document,
                    DocumentTypeId = queryResult.DocumentTypeId,
                    Location = queryResult.Location,
                    PersonId = queryResult.PersonId,
                    IsChild = queryResult.IsChild,
                    ChildAge = queryResult.ChildAge,
                    IsMain = queryResult.IsMain,

                    Person = new PersonDto()
                    {
                        ReceiveOffers = queryResult.Person.ReceiveOffers,
                        SharePersonData = queryResult.Person.SharePersonData,
                        AllowContact = queryResult.Person.AllowContact
                    },
                };

                if (result.Id != Guid.Empty)
                    result.GuestRegistrationDocumentList = GetAllDocumentsByGuestRegistrationId(result.Id);
            }

            result.GuestRegistrationDocumentList = GetAllDocumentsByGuestRegistrationId(result.Id);
            return result;
        }

        public IListDto<GuestRegistrationResultDto> GetAllGuestRegistrationByReservationItemId(long propertyId, long reservationItemId, string languageIsoCode)
        {
            var queryResult =
                (from gr in Context.GuestReservationItems

                 join guestRegistration in Context.GuestRegistrations on gr.GuestRegistrationId equals guestRegistration.Id
                 //into guestRegistrations
                 //from leftJoinGuestRegistration in guestRegistrations.DefaultIfEmpty()

                 join g in Context.Guests on gr.GuestId equals g.Id into guests
                 from leftJoinGuest in guests.DefaultIfEmpty()

                 join l in Context.Locations on guestRegistration.Id equals l.OwnerId into locations
                 from leftLocation in locations.DefaultIfEmpty()

                 join lc in Context.LocationCategories on leftLocation.LocationCategoryId equals lc.Id into locationCategories
                 from leftLocationCategory in locationCategories.DefaultIfEmpty()

                 join c in Context.CountrySubdivisions on leftLocation.CityId equals c.Id into city
                 from leftCity in city.DefaultIfEmpty()

                 join s in Context.CountrySubdivisions on leftCity.ParentSubdivision.Id equals s.Id into state
                 from leftState in state.DefaultIfEmpty()

                 join co in Context.CountrySubdivisions on guestRegistration.Nationality equals co.Id into country
                 from leftCountry in country.DefaultIfEmpty()

                     // person
                 join p in Context.Persons on leftJoinGuest.PersonId equals p.Id into person
                 from leftPerson in person.DefaultIfEmpty()

                 join ct in Context.CountrySubdivisionTranslations on
                 new { ct1 = leftCity.Id, ct2 = languageIsoCode } equals
                 new { ct1 = ct.CountrySubdivisionId, ct2 = ct.LanguageIsoCode.ToLower() } into cityTranslation
                 from leftCityTranslation in cityTranslation.DefaultIfEmpty()

                 join st in Context.CountrySubdivisionTranslations on
                 new { st1 = leftState.Id, st2 = languageIsoCode } equals
                 new { st1 = st.CountrySubdivisionId, st2 = st.LanguageIsoCode.ToLower() } into stateTranslation
                 from leftStateTranslation in stateTranslation.DefaultIfEmpty()

                 join cot in Context.CountrySubdivisionTranslations on
                 new { cot1 = leftCountry.Id, cot2 = languageIsoCode } equals
                 new { cot1 = cot.CountrySubdivisionId, cot2 = cot.LanguageIsoCode.ToLower() } into countryTranslation
                 from leftCountryTranslation in countryTranslation.DefaultIfEmpty()


                 where gr.ReservationItemId == reservationItemId

                 select new
                 {
                     //guestreservationItem
                     GuestReservationItemId = gr.Id,
                     //guestRegistration
                     Id = guestRegistration != null ? guestRegistration.Id : Guid.Empty,
                     FirstName = guestRegistration != null ? guestRegistration.FirstName : null,
                     LastName = guestRegistration != null ? guestRegistration.LastName : null,
                     FullName = guestRegistration != null ? guestRegistration.FullName : null,
                     SocialName = guestRegistration != null ? guestRegistration.SocialName : null,
                     BirthDate = guestRegistration != null ? guestRegistration.BirthDate : (DateTime?)null,
                     GuestId = gr != null ? (gr.GuestId != null ? (long)gr.GuestId : 0) : 0,
                     Email = guestRegistration != null ? guestRegistration.Email : null,
                     Gender = guestRegistration != null ? guestRegistration.Gender : null,
                     OccupationId = guestRegistration != null ? guestRegistration.OccupationId : 0,
                     GuestTypeId = guestRegistration != null ? guestRegistration.GuestTypeId : 0,
                     PhoneNumber = guestRegistration != null ? guestRegistration.PhoneNumber : null,
                     MobilePhoneNumber = guestRegistration != null ? guestRegistration.MobilePhoneNumber : null,
                     Nationality = guestRegistration != null ? guestRegistration.Nationality : 0,
                     PurposeOfTrip = guestRegistration != null ? guestRegistration.PurposeOfTrip : 0,
                     ArrivingBy = guestRegistration != null ? guestRegistration.ArrivingBy : 0,
                     ArrivingFrom = guestRegistration != null ? guestRegistration.ArrivingFrom : 0,
                     NextDestination = guestRegistration != null ? guestRegistration.NextDestination : 0,
                     ArrivingFromText = guestRegistration != null ? guestRegistration.ArrivingFromText : null,
                     NextDestinationText = guestRegistration != null ? guestRegistration.NextDestinationText : null,
                     AdditionalInformation = guestRegistration != null ? guestRegistration.AdditionalInformation : null,
                     HealthInsurance = guestRegistration != null ? guestRegistration.HealthInsurance : null,
                     ResponsibleGuestRegistrationId = guestRegistration != null ? guestRegistration.ResponsibleGuestRegistrationId : null,
                     IsLegallyIncompetent = guestRegistration != null ? guestRegistration.IsLegallyIncompetent : false,

                     Document = guestRegistration != null ? guestRegistration.DocumentInformation : null,
                     DocumentTypeId = guestRegistration != null && guestRegistration.DocumentTypeId.HasValue ? guestRegistration.DocumentTypeId.Value : 0,
                     PersonId = leftJoinGuest != null ? leftJoinGuest.PersonId.ToString() : null,
                     //Location
                     Location = new LocationDto
                     {
                         Id = leftLocation != null ? leftLocation.Id : 0,
                         OwnerId = leftLocation != null ? leftLocation.OwnerId : Guid.Empty,
                         Latitude = leftLocation != null ? leftLocation.Latitude : 0,
                         Longitude = leftLocation != null ? leftLocation.Longitude : 0,
                         StreetName = leftLocation != null ? leftLocation.StreetName : null,
                         StreetNumber = leftLocation != null ? leftLocation.StreetNumber : null,
                         AdditionalAddressDetails = leftLocation != null ? leftLocation.AdditionalAddressDetails : "",
                         PostalCode = leftLocation != null ? leftLocation.PostalCode : null,
                         CountryCode = leftLocation != null ? leftLocation.CountryCode : null,

                         //LocationCategory
                         LocationCategory = new LocationCategoryDto
                         {
                             Id = leftLocationCategory != null ? leftLocationCategory.Id : 0,
                             Name = leftLocationCategory != null ? leftLocationCategory.Name : "",
                             RecordScope = leftLocationCategory != null ? leftLocationCategory.RecordScope : ""
                         },
                         LocationCategoryId = leftLocation != null ? leftLocationCategory.Id : 0,
                         Neighborhood = leftLocation != null ? leftLocation.Neighborhood : null,
                         CityId = leftLocation != null ? leftLocation.CityId : 0,
                         City = leftLocation != null ? leftLocation.City.MapTo<CountrySubdivisionDto>() : null,
                         Division = leftStateTranslation != null ? leftState.TwoLetterIsoCode ?? leftStateTranslation.Name : null,
                         Subdivision = leftCityTranslation != null ? leftCityTranslation.Name : null,
                         Country = leftCountryTranslation != null ? leftCountryTranslation.Name : null,

                         //AddressTranslation
                         AddressTranslation = new AddressTranslationDto
                         {
                             CityId = leftCity != null ? leftCity.Id : 0,
                             CityName = leftCityTranslation != null ? leftCityTranslation.Name : "",
                             StateId = leftState != null ? leftState.Id : 0,
                             StateName = leftStateTranslation != null ? leftStateTranslation.Name : "",
                             CountryId = leftCountry != null ? leftCountry.Id : 0,
                             CountryName = leftCountryTranslation != null ? leftCountryTranslation.Name : "",
                             LanguageIsoCode = leftCityTranslation != null ? leftCityTranslation.LanguageIsoCode : "",
                             TwoLetterIsoCode = leftState != null ? leftState.TwoLetterIsoCode : "",
                             CountryTwoLetterIsoCode = leftCountry != null ? leftCountry.CountryTwoLetterIsoCode : ""
                         }
                     },
                     Person = new Person()
                     {
                         ReceiveOffers = (leftPerson != null ? leftPerson.ReceiveOffers : false),
                         SharePersonData = (leftPerson != null ? leftPerson.SharePersonData : false),
                         AllowContact = (leftPerson != null ? leftPerson.AllowContact : false),
                     },
                     IsChild = gr.IsChild,
                     ChildAge = gr.ChildAge,
                     IsMain = gr.IsMain
                 }).ToList();

            IListDto<GuestRegistrationResultDto> result = new ListDto<GuestRegistrationResultDto>();

            foreach (var item in queryResult)
            {
                if (item != null)
                {
                    GuestRegistrationResultDto newItem = new GuestRegistrationResultDto
                    {
                        GuestReservationItemId = item.GuestReservationItemId,
                        Id = item.Id,
                        FirstName = item.FirstName,
                        LastName = item.LastName,
                        FullName = item.FullName,
                        SocialName = item.SocialName,
                        BirthDate = item.BirthDate,
                        GuestId = item.GuestId,
                        Email = item.Email,
                        Gender = item.Gender,
                        OccupationId = item.OccupationId,
                        GuestTypeId = item.GuestTypeId,
                        PhoneNumber = item.PhoneNumber,
                        MobilePhoneNumber = item.MobilePhoneNumber,
                        Nationality = item.Nationality,
                        PurposeOfTrip = item.PurposeOfTrip,
                        ArrivingBy = item.ArrivingBy,
                        ArrivingFrom = item.ArrivingFrom,
                        NextDestination = item.NextDestination,
                        ArrivingFromText = item.ArrivingFromText,
                        NextDestinationText = item.NextDestinationText,
                        AdditionalInformation = item.AdditionalInformation,
                        HealthInsurance = item.HealthInsurance,
                        ResponsibleGuestRegistrationId = item.ResponsibleGuestRegistrationId,
                        IsLegallyIncompetent = item.IsLegallyIncompetent,

                        Document = item.Document,
                        DocumentTypeId = item.DocumentTypeId,
                        Location = item.Location,
                        PersonId = item.PersonId,
                        IsChild = item.IsChild,
                        ChildAge = item.ChildAge,
                        IsMain = item.IsMain,

                        Person = new PersonDto()
                        {
                            ReceiveOffers = item.Person.ReceiveOffers,
                            SharePersonData = item.Person.SharePersonData,
                            AllowContact = item.Person.AllowContact
                        },
                    };
                    result.Items.Add(newItem);
                }
            }


            return result;
        }

        public GuestRegistrationResultDto GetLastGuestRegistrationByGuestReservationItemId(long propertyId, long guestReservationItemId, string languageIsoCode)
        {
            var queryResult =
            (from gr in Context.GuestReservationItems

             join guestRegistration in Context.GuestRegistrations on gr.GuestRegistrationId equals guestRegistration.Id into guestRegistrations
             from leftJoinGuestRegistration in guestRegistrations.DefaultIfEmpty()

             join l in Context.Locations on leftJoinGuestRegistration.Id equals l.OwnerId into locations
             from leftLocation in locations.DefaultIfEmpty()

             join lc in Context.LocationCategories on leftLocation.LocationCategoryId equals lc.Id into locationCategories
             from leftLocationCategory in locationCategories.DefaultIfEmpty()

             join c in Context.CountrySubdivisions on leftLocation.CityId equals c.Id into city
             from leftCity in city.DefaultIfEmpty()

             join s in Context.CountrySubdivisions on leftCity.ParentSubdivision.Id equals s.Id into state
             from leftState in state.DefaultIfEmpty()

             join co in Context.CountrySubdivisions on leftJoinGuestRegistration.Nationality equals co.Id into country
             from leftCountry in country.DefaultIfEmpty()

             join ct in Context.CountrySubdivisionTranslations on
             new { ct1 = leftCity.Id, ct2 = languageIsoCode } equals
             new { ct1 = ct.CountrySubdivisionId, ct2 = ct.LanguageIsoCode.ToLower() } into cityTranslation
             from leftCityTranslation in cityTranslation.DefaultIfEmpty()

             join st in Context.CountrySubdivisionTranslations on
             new { st1 = leftState.Id, st2 = languageIsoCode } equals
             new { st1 = st.CountrySubdivisionId, st2 = st.LanguageIsoCode.ToLower() } into stateTranslation
             from leftStateTranslation in stateTranslation.DefaultIfEmpty()

             join cot in Context.CountrySubdivisionTranslations on
             new { cot1 = leftCountry.Id, cot2 = languageIsoCode } equals
             new { cot1 = cot.CountrySubdivisionId, cot2 = cot.LanguageIsoCode.ToLower() } into countryTranslation
             from leftCountryTranslation in countryTranslation.DefaultIfEmpty()

             join g in Context.Guests on gr.GuestId equals g.Id into guests
             from leftJoinGuest in guests.DefaultIfEmpty()

             join p in Context.Persons on leftJoinGuest.PersonId equals p.Id into person
             from leftPerson in person.DefaultIfEmpty()

             where gr.Id == guestReservationItemId

             select new
             {
                 //guestreservationItem
                 GuestReservationItemId = gr.Id,
                 //guestRegistration
                 Id = leftJoinGuestRegistration != null ? leftJoinGuestRegistration.Id : Guid.Empty,
                 FirstName = leftJoinGuestRegistration != null ? leftJoinGuestRegistration.FirstName : null,
                 LastName = leftJoinGuestRegistration != null ? leftJoinGuestRegistration.LastName : null,
                 FullName = leftJoinGuestRegistration != null ? leftJoinGuestRegistration.FullName : null,
                 SocialName = leftJoinGuestRegistration != null ? leftJoinGuestRegistration.SocialName : null,
                 BirthDate = leftJoinGuestRegistration != null ? leftJoinGuestRegistration.BirthDate : (DateTime?)null,
                 GuestId = gr != null ? (gr.GuestId != null ? (long)gr.GuestId : 0) : 0,
                 Email = leftJoinGuestRegistration != null ? leftJoinGuestRegistration.Email : null,
                 Gender = leftJoinGuestRegistration != null ? leftJoinGuestRegistration.Gender : null,
                 OccupationId = leftJoinGuestRegistration != null ? leftJoinGuestRegistration.OccupationId : 0,
                 GuestTypeId = leftJoinGuestRegistration != null ? leftJoinGuestRegistration.GuestTypeId : 0,
                 PhoneNumber = leftJoinGuestRegistration != null ? leftJoinGuestRegistration.PhoneNumber : null,
                 MobilePhoneNumber = leftJoinGuestRegistration != null ? leftJoinGuestRegistration.MobilePhoneNumber : null,
                 Nationality = leftJoinGuestRegistration != null ? leftJoinGuestRegistration.Nationality : 0,
                 PurposeOfTrip = leftJoinGuestRegistration != null ? leftJoinGuestRegistration.PurposeOfTrip : 0,
                 ArrivingBy = leftJoinGuestRegistration != null ? leftJoinGuestRegistration.ArrivingBy : 0,
                 ArrivingFrom = leftJoinGuestRegistration != null ? leftJoinGuestRegistration.ArrivingFrom : 0,
                 NextDestination = leftJoinGuestRegistration != null ? leftJoinGuestRegistration.NextDestination : 0,
                 ArrivingFromText = leftJoinGuestRegistration != null ? leftJoinGuestRegistration.ArrivingFromText : null,
                 NextDestinationText = leftJoinGuestRegistration != null ? leftJoinGuestRegistration.NextDestinationText : null,
                 AdditionalInformation = leftJoinGuestRegistration != null ? leftJoinGuestRegistration.AdditionalInformation : null,
                 HealthInsurance = leftJoinGuestRegistration != null ? leftJoinGuestRegistration.HealthInsurance : null,
                 ResponsibleGuestRegistrationId = leftJoinGuestRegistration != null ? leftJoinGuestRegistration.ResponsibleGuestRegistrationId : null,
                 IsLegallyIncompetent = leftJoinGuestRegistration != null ? leftJoinGuestRegistration.IsLegallyIncompetent : false,
                 Document = leftJoinGuestRegistration != null ? leftJoinGuestRegistration.DocumentInformation : null,
                 DocumentTypeId = leftJoinGuestRegistration != null && leftJoinGuestRegistration.DocumentTypeId.HasValue ? leftJoinGuestRegistration.DocumentTypeId.Value : 0,
                 PersonId = leftJoinGuest != null ? leftJoinGuest.PersonId.ToString() : null,

                 //Location
                 Location = new LocationDto
                 {
                     Id = leftLocation != null ? leftLocation.Id : 0,
                     OwnerId = leftLocation != null ? leftLocation.OwnerId : Guid.Empty,
                     Latitude = leftLocation != null ? leftLocation.Latitude : 0,
                     Longitude = leftLocation != null ? leftLocation.Longitude : 0,
                     StreetName = leftLocation != null ? leftLocation.StreetName : null,
                     StreetNumber = leftLocation != null ? leftLocation.StreetNumber : null,
                     AdditionalAddressDetails = leftLocation != null ? leftLocation.AdditionalAddressDetails : null,
                     PostalCode = leftLocation != null ? leftLocation.PostalCode : null,
                     CountryCode = leftLocation != null ? leftLocation.CountryCode : null,

                     //LocationCategory
                     LocationCategory = new LocationCategoryDto
                     {
                         Id = leftLocationCategory != null ? leftLocationCategory.Id : 0,
                         Name = leftLocationCategory != null ? leftLocationCategory.Name : "",
                         RecordScope = leftLocationCategory != null ? leftLocationCategory.RecordScope : ""
                     },
                     LocationCategoryId = leftLocation != null ? leftLocationCategory.Id : 0,
                     Neighborhood = leftLocation != null ? leftLocation.Neighborhood : null,
                     CityId = leftLocation != null ? leftLocation.CityId : 0,
                     City = leftLocation != null ? leftLocation.City.MapTo<CountrySubdivisionDto>() : null,
                     Division = leftStateTranslation != null ? leftState.TwoLetterIsoCode ?? leftStateTranslation.Name : null,
                     Subdivision = leftCityTranslation != null ? leftCityTranslation.Name : null,
                     Country = leftCountryTranslation != null ? leftCountryTranslation.Name : null,

                     //AddressTranslation
                     AddressTranslation = new AddressTranslationDto
                     {
                         CityId = leftCity != null ? leftCity.Id : 0,
                         CityName = leftCityTranslation != null ? leftCityTranslation.Name : null,
                         StateId = leftState != null ? leftState.Id : 0,
                         StateName = leftStateTranslation != null ? leftStateTranslation.Name : null,
                         CountryId = leftCountry != null ? leftCountry.Id : 0,
                         CountryName = leftCountryTranslation != null ? leftCountryTranslation.Name : null,
                         LanguageIsoCode = leftCityTranslation != null ? leftCityTranslation.LanguageIsoCode : null,
                         TwoLetterIsoCode = leftState != null ? leftState.TwoLetterIsoCode : null,
                         CountryTwoLetterIsoCode = leftCountry != null ? leftCountry.CountryTwoLetterIsoCode : null
                     }
                 },
                 CheckinDate = gr != null ? gr.CheckInDate : null,
                 IsChild = gr.IsChild,
                 ChildAge = gr.ChildAge,
                 IsMain = gr.IsMain,
                 Person = new Person()
                 {
                     ReceiveOffers = (leftPerson != null ? leftPerson.ReceiveOffers : false),
                     SharePersonData = (leftPerson != null ? leftPerson.SharePersonData : false),
                     AllowContact = (leftPerson != null ? leftPerson.AllowContact : false),
                 },

             }).OrderByDescending(t => t.CheckinDate).FirstOrDefault();

            GuestRegistrationResultDto result = null;
            if (queryResult != null)
            {
                result = new GuestRegistrationResultDto
                {
                    GuestReservationItemId = queryResult.GuestReservationItemId,
                    Id = queryResult.Id,
                    FirstName = queryResult.FirstName,
                    LastName = queryResult.LastName,
                    FullName = queryResult.FullName,
                    SocialName = queryResult.SocialName,
                    BirthDate = queryResult.BirthDate,
                    GuestId = queryResult.GuestId,
                    Email = queryResult.Email,
                    Gender = queryResult.Gender,
                    OccupationId = queryResult.OccupationId,
                    GuestTypeId = queryResult.GuestTypeId,
                    PhoneNumber = queryResult.PhoneNumber,
                    MobilePhoneNumber = queryResult.MobilePhoneNumber,
                    Nationality = queryResult.Nationality,
                    PurposeOfTrip = queryResult.PurposeOfTrip,
                    ArrivingBy = queryResult.ArrivingBy,
                    ArrivingFrom = queryResult.ArrivingFrom,
                    NextDestination = queryResult.NextDestination,
                    ArrivingFromText = queryResult.ArrivingFromText,
                    NextDestinationText = queryResult.NextDestinationText,
                    AdditionalInformation = queryResult.AdditionalInformation,
                    HealthInsurance = queryResult.HealthInsurance,
                    ResponsibleGuestRegistrationId = queryResult.ResponsibleGuestRegistrationId,
                    IsLegallyIncompetent = queryResult.IsLegallyIncompetent,
                    Document = queryResult.Document,
                    DocumentTypeId = queryResult.DocumentTypeId,
                    Location = queryResult.Location,
                    PersonId = queryResult.PersonId,

                    Person = new PersonDto()
                    {
                        ReceiveOffers = queryResult.Person.ReceiveOffers,
                        SharePersonData = queryResult.Person.SharePersonData,
                        AllowContact = queryResult.Person.AllowContact
                    },
                };
            }

            return result;
        }


        public GuestRegistrationResultDto GetGuestRegistrationFilledByGuestReservationAndPerson(long propertyId, long guestReservationItemId, string languageIsoCode)
        {
            var queryResult = (
                // GuestReservationItem
                from gr in Context.GuestReservationItems

                    // GuestRegistration
                join guestRegistration in Context.GuestRegistrations on gr.GuestRegistrationId equals guestRegistration.Id into guestRegistrations
                from leftJoinGuestRegistration in guestRegistrations.DefaultIfEmpty()

                    // Nationality
                join guestRegistrationNat in Context.CountrySubdivisions on leftJoinGuestRegistration.Nationality equals guestRegistrationNat.Id into guestRegistrationNationality
                from leftGuestRegistrationNationality in guestRegistrationNationality.DefaultIfEmpty()

                    // Location linked to GuestRegistration
                join lGuestRegistration in Context.Locations on leftJoinGuestRegistration.Id equals lGuestRegistration.OwnerId into locationsGuestRegistration
                from leftLocationGuestRegistration in locationsGuestRegistration.DefaultIfEmpty()


                join lCategoryGuestRegistration in Context.LocationCategories on leftLocationGuestRegistration.LocationCategoryId equals lCategoryGuestRegistration.Id into locationCategoriesGuestRegistration
                from leftLocationCategoryGuestRegistration in locationCategoriesGuestRegistration.DefaultIfEmpty()


                    // City , State and Country based on GuestRegistration
                join cGuestRegistration in Context.CountrySubdivisions on leftLocationGuestRegistration.CityId equals cGuestRegistration.Id into cityGuestRegistration
                from leftCityGuestRegistration in cityGuestRegistration.DefaultIfEmpty()

                join sGuestRegistration in Context.CountrySubdivisions on leftCityGuestRegistration.ParentSubdivisionId equals sGuestRegistration.Id into stateGuestRegistration
                from leftStateGuestRegistration in stateGuestRegistration.DefaultIfEmpty()

                join coGuestRegistration in Context.CountrySubdivisions on leftStateGuestRegistration.ParentSubdivisionId equals coGuestRegistration.Id into countryGuestRegistration
                from leftCountryGuestRegistration in countryGuestRegistration.DefaultIfEmpty()


                    // City , State and Country based on GuestRegistration translations
                join ctGuestRegistration in Context.CountrySubdivisionTranslations on
                new { ctGuestRegistration1 = leftCityGuestRegistration.Id, ctGuestRegistration2 = languageIsoCode } equals
                new { ctGuestRegistration1 = ctGuestRegistration.CountrySubdivisionId, ctGuestRegistration2 = ctGuestRegistration.LanguageIsoCode.ToLower() } into cityGuestRegistrationTranslation
                from leftCityGuestRegistrationTranslation in cityGuestRegistrationTranslation.DefaultIfEmpty()

                join stGuestRegistration in Context.CountrySubdivisionTranslations on
                new { stGuestRegistration1 = leftStateGuestRegistration.Id, stGuestRegistration2 = languageIsoCode } equals
                new { stGuestRegistration1 = stGuestRegistration.CountrySubdivisionId, stGuestRegistration2 = stGuestRegistration.LanguageIsoCode.ToLower() } into stateGuestRegistrationTranslation
                from leftStateGuestRegistrationTranslation in stateGuestRegistrationTranslation.DefaultIfEmpty()

                join cotGuestRegistration in Context.CountrySubdivisionTranslations on
                new { cotGuestRegistration1 = leftCountryGuestRegistration.Id, cotGuestRegistration2 = languageIsoCode } equals
                new { cotGuestRegistration1 = cotGuestRegistration.CountrySubdivisionId, cotGuestRegistration2 = cotGuestRegistration.LanguageIsoCode.ToLower() } into countryGuestRegistrationTranslation
                from leftCountryGuestRegistrationTranslation in countryGuestRegistrationTranslation.DefaultIfEmpty()


                    // Guest and Person
                join g in Context.Guests on gr.GuestId equals g.Id into guests
                from leftGuests in guests.DefaultIfEmpty()

                join p in Context.Persons on leftGuests.PersonId equals p.Id into person
                from leftPerson in person.DefaultIfEmpty()


                    // Location linked to Person
                join lPerson in Context.Locations on leftPerson.Id equals lPerson.OwnerId into locationsPerson
                from leftLocationPerson in locationsPerson.DefaultIfEmpty()

                join lCategoryPerson in Context.LocationCategories on leftLocationPerson.LocationCategoryId equals lCategoryPerson.Id into locationCategoriesPerson
                from leftLocationCategoryPerson in locationCategoriesPerson.DefaultIfEmpty()


                    // City , State and Country based on Person
                join cPerson in Context.CountrySubdivisions on leftLocationPerson.CityId equals cPerson.Id into cityPerson
                from leftCityPerson in cityPerson.DefaultIfEmpty()

                join sPerson in Context.CountrySubdivisions on leftCityPerson.ParentSubdivisionId equals sPerson.Id into statePerson
                from leftStatePerson in statePerson.DefaultIfEmpty()

                join coPerson in Context.CountrySubdivisions on leftStatePerson.ParentSubdivisionId equals coPerson.Id into countryPerson
                from leftCountryPerson in countryPerson.DefaultIfEmpty()


                    // Person main document
                join cpd in Context.CountryPersonDocumentTypes on
                new { cpd1 = leftCountryPerson.Id, cpd2 = leftPerson.PersonType } equals
                new { cpd1 = cpd.CountrySubdivisionId, cpd2 = cpd.PersonType } into countryPersonDocumentTypes
                from leftCountryPersonDocumentTypes in countryPersonDocumentTypes.DefaultIfEmpty()

                join d in Context.Documents on
                new { d1 = leftCountryPersonDocumentTypes.DocumentTypeId, d2 = leftPerson.Id } equals
                new { d1 = d.DocumentTypeId, d2 = d.OwnerId } into documentPerson
                from leftDocumentPerson in documentPerson.DefaultIfEmpty()


                    // City , State and Country based on Person translations
                join ctPerson in Context.CountrySubdivisionTranslations on
                new { ctPerson1 = leftCityPerson.Id, ctPerson2 = languageIsoCode } equals
                new { ctPerson1 = ctPerson.CountrySubdivisionId, ctPerson2 = ctPerson.LanguageIsoCode.ToLower() } into cityPersonTranslation
                from leftCityPersonTranslation in cityPersonTranslation.DefaultIfEmpty()

                join stPerson in Context.CountrySubdivisionTranslations on
                new { stPerson1 = leftStatePerson.Id, stPerson2 = languageIsoCode } equals
                new { stPerson1 = stPerson.CountrySubdivisionId, stPerson2 = stPerson.LanguageIsoCode.ToLower() } into statePersonTranslation
                from leftStatePersonTranslation in statePersonTranslation.DefaultIfEmpty()

                join cotPerson in Context.CountrySubdivisionTranslations on
                new { cotPerson1 = leftCountryPerson.Id, cotPerson2 = languageIsoCode } equals
                new { cotPerson1 = cotPerson.CountrySubdivisionId, cotPerson2 = cotPerson.LanguageIsoCode.ToLower() } into countryPersonTranslation
                from leftCountryPersonTranslation in countryPersonTranslation.DefaultIfEmpty()


                    // Contact Information Linked to Person
                join ciEmail in Context.ContactInformations on
                new { ciEmail1 = leftPerson.Id, ciEmail2 = (int)ContactInformationTypeEnum.Email } equals
                new { ciEmail1 = ciEmail.OwnerId, ciEmail2 = ciEmail.ContactInformationTypeId } into email
                from leftEmail in email.DefaultIfEmpty()

                join ciPhone in Context.ContactInformations on
                new { ciPhone1 = leftPerson.Id, ciPhone2 = (int)ContactInformationTypeEnum.PhoneNumber } equals
                new { ciPhone1 = ciPhone.OwnerId, ciPhone2 = ciPhone.ContactInformationTypeId } into phone
                from leftPhone in phone.DefaultIfEmpty()

                join ciMobilePhone in Context.ContactInformations on
                new { ciMobilePhone1 = leftPerson.Id, ciMobilePhone2 = (int)ContactInformationTypeEnum.CellPhoneNumber } equals
                new { ciMobilePhone1 = ciMobilePhone.OwnerId, ciMobilePhone2 = ciMobilePhone.ContactInformationTypeId } into mobilephone
                from leftMobilePhone in mobilephone.DefaultIfEmpty()

                    // Person Information
                join piSocialName in Context.PersonInformations on
                new { piSocialName1 = leftPerson.Id, piSocialName2 = (int)PersonInformationTypeEnum.SocialName } equals
                new { piSocialName1 = piSocialName.OwnerId, piSocialName2 = piSocialName.PersonInformationTypeId } into socialName
                from leftSocialName in socialName.DefaultIfEmpty()

                join piHealthInsurance in Context.PersonInformations on
                new { piHealthInsurance1 = leftPerson.Id, piHealthInsurance2 = (int)PersonInformationTypeEnum.SocialName } equals
                new { piHealthInsurance1 = piHealthInsurance.OwnerId, piHealthInsurance2 = piHealthInsurance.PersonInformationTypeId } into healthInsurance
                from leftHealthInsurance in healthInsurance.DefaultIfEmpty()



                where gr.Id == guestReservationItemId



                select new
                {
                    //guestreservationItem
                    GuestReservationItemId = gr.Id,
                    //guestRegistration
                    Id = leftJoinGuestRegistration != null ? leftJoinGuestRegistration.Id : Guid.Empty,
                    FirstName = leftJoinGuestRegistration != null ? leftJoinGuestRegistration.FirstName : gr != null ? gr.GuestName : leftPerson != null ? leftPerson.FirstName : null,
                    LastName = leftJoinGuestRegistration != null ? leftJoinGuestRegistration.LastName : leftPerson != null ? leftPerson.LastName : null,
                    FullName = leftJoinGuestRegistration != null ? leftJoinGuestRegistration.FullName : gr != null ? gr.GuestName : leftPerson != null ? leftPerson.FirstName : null,
                    SocialName = leftJoinGuestRegistration != null ? leftJoinGuestRegistration.SocialName : leftSocialName != null ? leftSocialName.Information : null,
                    BirthDate = leftJoinGuestRegistration != null ? leftJoinGuestRegistration.BirthDate : leftPerson != null ? leftPerson.DateOfBirth : (DateTime?)null,
                    GuestId = gr != null ?
                            (gr.GuestId != null ? (long)gr.GuestId : 0)
                            : 0,
                    Email = leftJoinGuestRegistration != null ? leftJoinGuestRegistration.Email : gr != null ? gr.GuestEmail : leftEmail != null ? leftEmail.Information : null,
                    Gender = leftJoinGuestRegistration != null ? leftJoinGuestRegistration.Gender : null,
                    OccupationId = leftJoinGuestRegistration != null ? leftJoinGuestRegistration.OccupationId : 0,
                    GuestTypeId = leftJoinGuestRegistration != null ? leftJoinGuestRegistration.GuestTypeId : gr != null ? gr.GuestTypeId : 0,
                    PhoneNumber = leftJoinGuestRegistration != null ? leftJoinGuestRegistration.PhoneNumber : leftPhone != null ? leftPhone.Information : null,
                    MobilePhoneNumber = leftJoinGuestRegistration != null ? leftJoinGuestRegistration.MobilePhoneNumber : leftMobilePhone != null ? leftMobilePhone.Information : null,
                    Nationality = leftJoinGuestRegistration != null ? leftJoinGuestRegistration.Nationality : 0,
                    PurposeOfTrip = leftJoinGuestRegistration != null ? leftJoinGuestRegistration.PurposeOfTrip : 0,
                    ArrivingBy = leftJoinGuestRegistration != null ? leftJoinGuestRegistration.ArrivingBy : 0,
                    ArrivingFrom = leftJoinGuestRegistration != null ? leftJoinGuestRegistration.ArrivingFrom : 0,
                    NextDestination = leftJoinGuestRegistration != null ? leftJoinGuestRegistration.NextDestination : 0,
                    ArrivingFromText = leftJoinGuestRegistration != null ? leftJoinGuestRegistration.ArrivingFromText : null,
                    NextDestinationText = leftJoinGuestRegistration != null ? leftJoinGuestRegistration.NextDestinationText : null,
                    AdditionalInformation = leftJoinGuestRegistration != null ? leftJoinGuestRegistration.AdditionalInformation : null,
                    HealthInsurance = leftJoinGuestRegistration != null ? leftJoinGuestRegistration.HealthInsurance : leftHealthInsurance != null ? leftHealthInsurance.Information : null,
                    ResponsibleGuestRegistrationId = leftJoinGuestRegistration != null ? leftJoinGuestRegistration.ResponsibleGuestRegistrationId : null,
                    IsLegallyIncompetent = leftJoinGuestRegistration != null ? leftJoinGuestRegistration.IsLegallyIncompetent : false,
                    Document = leftJoinGuestRegistration != null ? leftJoinGuestRegistration.DocumentInformation : leftDocumentPerson != null ? leftDocumentPerson.DocumentInformation : null,
                    DocumentTypeId = leftJoinGuestRegistration != null && leftJoinGuestRegistration.DocumentTypeId.HasValue ? leftJoinGuestRegistration.DocumentTypeId.Value : leftDocumentPerson != null ? leftDocumentPerson.DocumentTypeId : 0,
                    PersonId = leftPerson != null ? leftPerson.Id.ToString() : null,

                    //Location
                    Location = new LocationDto
                    {
                        Id = leftLocationGuestRegistration != null ? leftLocationGuestRegistration.Id : leftLocationPerson != null ? leftLocationPerson.Id : 0,
                        OwnerId = leftLocationGuestRegistration != null ? leftLocationGuestRegistration.OwnerId : leftLocationPerson != null ? leftLocationPerson.OwnerId : Guid.Empty,
                        Latitude = leftLocationGuestRegistration != null ? leftLocationGuestRegistration.Latitude : leftLocationPerson != null ? leftLocationPerson.Latitude : 0,
                        Longitude = leftLocationGuestRegistration != null ? leftLocationGuestRegistration.Longitude : leftLocationPerson != null ? leftLocationPerson.Longitude : 0,
                        StreetName = leftLocationGuestRegistration != null ? leftLocationGuestRegistration.StreetName : leftLocationPerson != null ? leftLocationPerson.StreetName : null,
                        StreetNumber = leftLocationGuestRegistration != null ? leftLocationGuestRegistration.StreetNumber : leftLocationPerson != null ? leftLocationPerson.StreetNumber : null,
                        AdditionalAddressDetails = leftLocationGuestRegistration != null ? leftLocationGuestRegistration.AdditionalAddressDetails : leftLocationPerson != null ? leftLocationPerson.AdditionalAddressDetails : null,
                        PostalCode = leftLocationGuestRegistration != null ? leftLocationGuestRegistration.PostalCode : leftLocationPerson != null ? leftLocationPerson.PostalCode : null,
                        CountryCode = leftLocationGuestRegistration != null ? leftLocationGuestRegistration.CountryCode : leftLocationPerson != null ? leftLocationPerson.CountryCode : null,

                        //LocationCategory
                        LocationCategory = new LocationCategoryDto
                        {
                            Id = leftLocationCategoryGuestRegistration != null ? leftLocationCategoryGuestRegistration.Id : leftLocationCategoryPerson != null ? leftLocationCategoryPerson.Id : 0,
                            Name = leftLocationCategoryGuestRegistration != null ? leftLocationCategoryGuestRegistration.Name : leftLocationCategoryPerson != null ? leftLocationCategoryPerson.Name : "",
                            RecordScope = leftLocationCategoryGuestRegistration != null ? leftLocationCategoryGuestRegistration.RecordScope : leftLocationCategoryPerson != null ? leftLocationCategoryPerson.RecordScope : ""
                        },
                        LocationCategoryId = leftLocationGuestRegistration != null ? leftLocationGuestRegistration.LocationCategoryId : leftLocationPerson != null ? leftLocationPerson.LocationCategoryId : 0,
                        Neighborhood = leftLocationGuestRegistration != null ? leftLocationGuestRegistration.Neighborhood : leftLocationPerson != null ? leftLocationPerson.Neighborhood : null,
                        CityId = leftLocationGuestRegistration != null ? leftLocationGuestRegistration.CityId : leftLocationPerson != null ? leftLocationPerson.CityId : 0,
                        City = leftLocationGuestRegistration != null ? leftLocationGuestRegistration.City.MapTo<CountrySubdivisionDto>() : leftLocationPerson != null ? leftLocationPerson.City.MapTo<CountrySubdivisionDto>() : null,
                        Division = leftStateGuestRegistrationTranslation != null ? leftStateGuestRegistration.TwoLetterIsoCode ?? leftStateGuestRegistrationTranslation.Name : leftStatePersonTranslation != null ? leftStatePerson.TwoLetterIsoCode ?? leftStatePersonTranslation.Name : null,
                        Subdivision = leftCityGuestRegistrationTranslation != null ? leftCityGuestRegistration.TwoLetterIsoCode ?? leftCityGuestRegistrationTranslation.Name : leftCityPersonTranslation != null ? leftCityPerson.TwoLetterIsoCode ?? leftCityPersonTranslation.Name : null,
                        Country = leftCountryGuestRegistrationTranslation != null ? leftCountryGuestRegistration.TwoLetterIsoCode ?? leftCountryGuestRegistrationTranslation.Name : leftCountryPersonTranslation != null ? leftCountryPerson.TwoLetterIsoCode ?? leftCountryPersonTranslation.Name : null,

                        ////AddressTranslation
                        AddressTranslation = new AddressTranslationDto
                        {
                            CityId = leftCityGuestRegistration != null ? leftCityGuestRegistration.Id : leftCityPerson != null ? leftCityPerson.Id : 0,
                            CityName = leftCityGuestRegistrationTranslation != null ? leftCityGuestRegistrationTranslation.Name : leftCityPersonTranslation != null ? leftCityPersonTranslation.Name : null,
                            StateId = leftStateGuestRegistration != null ? leftStateGuestRegistration.Id : leftStatePerson != null ? leftStatePerson.Id : 0,
                            StateName = leftStateGuestRegistrationTranslation != null ? leftStateGuestRegistrationTranslation.Name : leftStatePersonTranslation != null ? leftStatePersonTranslation.Name : null,
                            CountryId = leftCountryGuestRegistration != null ? leftCountryGuestRegistration.Id : leftCountryPerson != null ? leftCountryPerson.Id : 0,
                            CountryName = leftCountryGuestRegistrationTranslation != null ? leftCountryGuestRegistrationTranslation.Name : leftCountryPersonTranslation != null ? leftCountryPersonTranslation.Name : null,
                            LanguageIsoCode = leftCityGuestRegistrationTranslation != null ? leftCityGuestRegistrationTranslation.LanguageIsoCode : leftCityPersonTranslation != null ? leftCityPersonTranslation.LanguageIsoCode : null,
                            TwoLetterIsoCode = leftStateGuestRegistration != null ? leftStateGuestRegistration.TwoLetterIsoCode : leftStatePerson != null ? leftStatePerson.TwoLetterIsoCode : "",
                            CountryTwoLetterIsoCode = leftCountryGuestRegistration != null ? leftCountryGuestRegistration.CountryTwoLetterIsoCode : leftCountryPerson != null ? leftCountryPerson.CountryTwoLetterIsoCode : null
                        }
                    },
                    IsChild = gr.IsChild,
                    ChildAge = gr.ChildAge,
                    IsMain = gr.IsMain,

                    Person = new Person()
                    {
                        ReceiveOffers = (leftPerson != null ? leftPerson.ReceiveOffers : false),
                        SharePersonData = (leftPerson != null ? leftPerson.SharePersonData : false),
                        AllowContact = (leftPerson != null ? leftPerson.AllowContact : false),
                    },
                }).FirstOrDefault();


            GuestRegistrationResultDto result = null;
            if (queryResult != null)
            {
                result = new GuestRegistrationResultDto
                {
                    GuestReservationItemId = queryResult.GuestReservationItemId,
                    Id = queryResult.Id,
                    FirstName = queryResult.FirstName,
                    LastName = queryResult.LastName,
                    FullName = queryResult.FullName,
                    SocialName = queryResult.SocialName,
                    BirthDate = queryResult.BirthDate,
                    GuestId = queryResult.GuestId,
                    Email = queryResult.Email,
                    Gender = queryResult.Gender,
                    OccupationId = queryResult.OccupationId,
                    GuestTypeId = queryResult.GuestTypeId,
                    PhoneNumber = queryResult.PhoneNumber,
                    MobilePhoneNumber = queryResult.MobilePhoneNumber,
                    Nationality = queryResult.Nationality,
                    PurposeOfTrip = queryResult.PurposeOfTrip,
                    ArrivingBy = queryResult.ArrivingBy,
                    ArrivingFrom = queryResult.ArrivingFrom,
                    NextDestination = queryResult.NextDestination,
                    ArrivingFromText = queryResult.ArrivingFromText,
                    NextDestinationText = queryResult.NextDestinationText,
                    AdditionalInformation = queryResult.AdditionalInformation,
                    HealthInsurance = queryResult.HealthInsurance,
                    ResponsibleGuestRegistrationId = queryResult.ResponsibleGuestRegistrationId,
                    IsLegallyIncompetent = queryResult.IsLegallyIncompetent,
                    Document = queryResult.Document,
                    DocumentTypeId = queryResult.DocumentTypeId,
                    Location = queryResult.Location,
                    PersonId = queryResult.PersonId,
                    IsChild = queryResult.IsChild,
                    ChildAge = queryResult.ChildAge,
                    IsMain = queryResult.IsMain,

                    Person = new PersonDto()
                    {
                        ReceiveOffers = queryResult.Person.ReceiveOffers,
                        SharePersonData = queryResult.Person.SharePersonData,
                        AllowContact = queryResult.Person.AllowContact
                    },
                };
            }

            return result;
        }

        public IQueryable<GuestRegistrationResultDto> GetGuestRegistration(string languageIsoCode)
        {

            var queryResult =
            (
                // GuestRegistration
                from guestRegistration in Context.GuestRegistrations

                    // GuestReservationItem
                join gr in Context.GuestReservationItems on
                    guestRegistration.Id equals gr.GuestRegistrationId

                // Locations
                join l in Context.Locations on guestRegistration.Id equals l.OwnerId into locations
                from leftLocation in locations.DefaultIfEmpty()

                join lc in Context.LocationCategories on leftLocation.LocationCategoryId equals lc.Id into locationCategories
                from leftLocationCategory in locationCategories.DefaultIfEmpty()

                    // City, State and Country
                join c in Context.CountrySubdivisions on leftLocation.CityId equals c.Id into city
                from leftCity in city.DefaultIfEmpty()

                join s in Context.CountrySubdivisions on leftCity.ParentSubdivision.Id equals s.Id into state
                from leftState in state.DefaultIfEmpty()

                join co in Context.CountrySubdivisions on guestRegistration.Nationality equals co.Id into country
                from leftCountry in country.DefaultIfEmpty()

                    // City, State and Country Translations
                join ct in Context.CountrySubdivisionTranslations on
                    new { ct1 = leftCity.Id, ct2 = languageIsoCode } equals
                    new { ct1 = ct.CountrySubdivisionId, ct2 = ct.LanguageIsoCode.ToLower() } into cityTranslation
                from leftCityTranslation in cityTranslation.DefaultIfEmpty()

                join st in Context.CountrySubdivisionTranslations on
                    new { st1 = leftState.Id, st2 = languageIsoCode } equals
                    new { st1 = st.CountrySubdivisionId, st2 = st.LanguageIsoCode.ToLower() } into stateTranslation
                from leftStateTranslation in stateTranslation.DefaultIfEmpty()

                join cot in Context.CountrySubdivisionTranslations on
                    new { cot1 = leftCountry.Id, cot2 = languageIsoCode } equals
                    new { cot1 = cot.CountrySubdivisionId, cot2 = cot.LanguageIsoCode.ToLower() } into countryTranslation
                from leftCountryTranslation in countryTranslation.DefaultIfEmpty()

                join g in Context.Guests on guestRegistration.GuestId equals g.Id into guests
                from leftJoinGuest in guests.DefaultIfEmpty()

                    // Person

                join p in Context.Persons on leftJoinGuest.PersonId equals p.Id into person
                from leftPerson in person.DefaultIfEmpty()

                select new GuestRegistrationResultDto
                {
                    //guestreservationItem
                    GuestReservationItemId = gr.Id,
                    //guestRegistration
                    Id = guestRegistration != null ? guestRegistration.Id : Guid.Empty,
                    FirstName = guestRegistration != null ? guestRegistration.FirstName : null,
                    LastName = guestRegistration != null ? guestRegistration.LastName : null,
                    FullName = guestRegistration != null ? guestRegistration.FullName : null,
                    SocialName = guestRegistration != null ? guestRegistration.SocialName : null,
                    BirthDate = guestRegistration != null ? guestRegistration.BirthDate : (DateTime?)null,
                    GuestId = guestRegistration != null ? guestRegistration.GuestId : 0,
                    Email = guestRegistration != null ? guestRegistration.Email : null,
                    Gender = guestRegistration != null ? guestRegistration.Gender : null,
                    OccupationId = guestRegistration != null ? guestRegistration.OccupationId : 0,
                    GuestTypeId = guestRegistration != null ? guestRegistration.GuestTypeId : 0,
                    PhoneNumber = guestRegistration != null ? guestRegistration.PhoneNumber : null,
                    MobilePhoneNumber = guestRegistration != null ? guestRegistration.MobilePhoneNumber : null,
                    Nationality = guestRegistration != null ? guestRegistration.Nationality : 0,
                    PurposeOfTrip = guestRegistration != null ? guestRegistration.PurposeOfTrip : 0,
                    ArrivingBy = guestRegistration != null ? guestRegistration.ArrivingBy : 0,
                    ArrivingFrom = guestRegistration != null ? guestRegistration.ArrivingFrom : 0,
                    NextDestination = guestRegistration != null ? guestRegistration.NextDestination : 0,
                    ArrivingFromText = guestRegistration != null ? guestRegistration.ArrivingFromText : null,
                    NextDestinationText = guestRegistration != null ? guestRegistration.NextDestinationText : null,
                    AdditionalInformation = guestRegistration != null ? guestRegistration.AdditionalInformation : null,
                    HealthInsurance = guestRegistration != null ? guestRegistration.HealthInsurance : null,
                    ResponsibleGuestRegistrationId = guestRegistration != null ? guestRegistration.ResponsibleGuestRegistrationId : null,
                    IsLegallyIncompetent = guestRegistration != null ? guestRegistration.IsLegallyIncompetent : false,
                    Document = guestRegistration != null ? guestRegistration.DocumentInformation : null,
                    DocumentTypeId = guestRegistration != null && guestRegistration.DocumentTypeId.HasValue ? guestRegistration.DocumentTypeId.Value : 0,
                    PersonId = leftJoinGuest != null ? leftJoinGuest.PersonId.ToString() : null,

                    //Location
                    Location = new LocationDto
                    {
                        Id = leftLocation != null ? leftLocation.Id : 0,
                        OwnerId = leftLocation != null ? leftLocation.OwnerId : Guid.Empty,
                        Latitude = leftLocation != null ? leftLocation.Latitude : 0,
                        Longitude = leftLocation != null ? leftLocation.Longitude : 0,
                        StreetName = leftLocation != null ? leftLocation.StreetName : null,
                        StreetNumber = leftLocation != null ? leftLocation.StreetNumber : null,
                        AdditionalAddressDetails = leftLocation != null ? leftLocation.AdditionalAddressDetails : null,
                        PostalCode = leftLocation != null ? leftLocation.PostalCode : null,
                        CountryCode = leftLocation != null ? leftLocation.CountryCode : null,

                        //LocationCategory
                        LocationCategory = new LocationCategoryDto
                        {
                            Id = leftLocationCategory != null ? leftLocationCategory.Id : 0,
                            Name = leftLocationCategory != null ? leftLocationCategory.Name : "",
                            RecordScope = leftLocationCategory != null ? leftLocationCategory.RecordScope : ""
                        },

                        LocationCategoryId = leftLocation != null ? leftLocationCategory.Id : 0,
                        Neighborhood = leftLocation != null ? leftLocation.Neighborhood : null,
                        CityId = leftLocation != null ? leftLocation.CityId : 0,
                        City = leftLocation != null ? leftLocation.City.MapTo<CountrySubdivisionDto>() : null,
                        Division = leftStateTranslation != null ? leftState.TwoLetterIsoCode ?? leftStateTranslation.Name : null,
                        Subdivision = leftCityTranslation != null ? leftCityTranslation.Name : null,
                        Country = leftCountryTranslation != null ? leftCountryTranslation.Name : null,

                        //AddressTranslation
                        AddressTranslation = new AddressTranslationDto
                        {
                            CityId = leftCity != null ? leftCity.Id : 0,
                            CityName = leftCityTranslation != null ? leftCityTranslation.Name : null,
                            StateId = leftState != null ? leftState.Id : 0,
                            StateName = leftStateTranslation != null ? leftStateTranslation.Name : null,
                            CountryId = leftCountry != null ? leftCountry.Id : 0,
                            CountryName = leftCountryTranslation != null ? leftCountryTranslation.Name : null,
                            LanguageIsoCode = leftCityTranslation != null ? leftCityTranslation.LanguageIsoCode : null,
                            TwoLetterIsoCode = leftState != null ? leftState.TwoLetterIsoCode : null,
                            CountryTwoLetterIsoCode = leftCountry != null ? leftCountry.CountryTwoLetterIsoCode : null
                        }
                    },
                    PropertyId = guestRegistration.PropertyId,
                    TenantId = guestRegistration.TenantId,
                    CheckinDate = gr != null ? gr.CheckInDate : null,
                    IsChild = gr.IsChild,
                    ChildAge = gr.ChildAge,
                    IsMain = gr.IsMain,
                    Person = new PersonDto()
                    {
                        ReceiveOffers = (leftPerson != null ? leftPerson.ReceiveOffers : false),
                        SharePersonData = (leftPerson != null ? leftPerson.SharePersonData : false),
                        AllowContact = (leftPerson != null ? leftPerson.AllowContact : false),
                    }
                }
            );

            return queryResult;
        }

        public IQueryable<GuestRegistrationResultDto> GetGuestRegistrationSearch(string languageIsoCode)
        {

            var queryResult =
            (
                // GuestRegistration
                from guestRegistration in Context.GuestRegistrations

                    // GuestReservationItem
                join gri in Context.GuestReservationItems on
                    guestRegistration.Id equals gri.GuestRegistrationId

                // Locations
                join l in Context.Locations on guestRegistration.Id equals l.OwnerId into locations
                from leftLocation in locations.DefaultIfEmpty()

                join lc in Context.LocationCategories on leftLocation.LocationCategoryId equals lc.Id into locationCategories
                from leftLocationCategory in locationCategories.DefaultIfEmpty()

                    // City, State and Country
                join c in Context.CountrySubdivisions on leftLocation.CityId equals c.Id into city
                from leftCity in city.DefaultIfEmpty()

                join s in Context.CountrySubdivisions on leftCity.ParentSubdivision.Id equals s.Id into state
                from leftState in state.DefaultIfEmpty()

                join co in Context.CountrySubdivisions on guestRegistration.Nationality equals co.Id into country
                from leftCountry in country.DefaultIfEmpty()

                from leftCityTranslation in Context.CountrySubdivisionTranslations.Where(o => leftCity.Id == o.CountrySubdivisionId)
                                            .Take(1)
                                            .DefaultIfEmpty()

                from leftStateTranslation in Context.CountrySubdivisionTranslations.Where(o => leftState.Id == o.CountrySubdivisionId)
                                            .Take(1)
                                            .DefaultIfEmpty()

                from leftCountryTranslation in Context.CountrySubdivisionTranslations.Where(o => leftCountry.Id == o.CountrySubdivisionId)
                                            .Take(1)
                                            .DefaultIfEmpty()

                    //City, State and Country Translations
                    //join ct in Context.CountrySubdivisionTranslations on
                    //    new { ct1 = leftCity.Id, ct2 = languageIsoCode } equals
                    //    new { ct1 = ct.CountrySubdivisionId, ct2 = ct.LanguageIsoCode.ToLower() } into cityTranslation
                    //from leftCityTranslation in cityTranslation.DefaultIfEmpty()

                    //join st in Context.CountrySubdivisionTranslations on
                    //	new { st1 = leftState.Id, st2 = languageIsoCode } equals
                    //	new { st1 = st.CountrySubdivisionId, st2 = st.LanguageIsoCode.ToLower() } into stateTranslation
                    //from leftStateTranslation in stateTranslation.Take(1).DefaultIfEmpty()

                    //join cot in Context.CountrySubdivisionTranslations on
                    //	new { cot1 = leftCountry.Id, cot2 = languageIsoCode } equals
                    //	new { cot1 = cot.CountrySubdivisionId, cot2 = cot.LanguageIsoCode.ToLower() } into countryTranslation
                    //from leftCountryTranslation in countryTranslation.Take(1).DefaultIfEmpty()

                join g in Context.Guests on guestRegistration.GuestId equals g.Id into guests
                from leftJoinGuest in guests.DefaultIfEmpty()

                    // Person
                join p in Context.Persons on leftJoinGuest.PersonId equals p.Id into person
                from leftPerson in person.DefaultIfEmpty()

                where gri.CreationTime == (Context.GuestReservationItems.Where(g => g.GuestId == gri.GuestId).Max(r => r.CreationTime))

                select new GuestRegistrationResultDto
                {
                    //guestreservationItem
                    GuestReservationItemId = gri.Id,
                    //guestRegistration
                    Id = guestRegistration != null ? guestRegistration.Id : Guid.Empty,
                    FirstName = guestRegistration != null ? guestRegistration.FirstName : null,
                    LastName = guestRegistration != null ? guestRegistration.LastName : null,
                    FullName = guestRegistration != null ? guestRegistration.FullName : null,
                    SocialName = guestRegistration != null ? guestRegistration.SocialName : null,
                    BirthDate = guestRegistration != null ? guestRegistration.BirthDate : (DateTime?)null,
                    GuestId = guestRegistration != null ? guestRegistration.GuestId : 0,
                    Email = guestRegistration != null ? guestRegistration.Email : null,
                    Gender = guestRegistration != null ? guestRegistration.Gender : null,
                    OccupationId = guestRegistration != null ? guestRegistration.OccupationId : 0,
                    GuestTypeId = guestRegistration != null ? guestRegistration.GuestTypeId : 0,
                    PhoneNumber = guestRegistration != null ? guestRegistration.PhoneNumber : null,
                    MobilePhoneNumber = guestRegistration != null ? guestRegistration.MobilePhoneNumber : null,
                    Nationality = guestRegistration != null ? guestRegistration.Nationality : 0,
                    PurposeOfTrip = guestRegistration != null ? guestRegistration.PurposeOfTrip : 0,
                    ArrivingBy = guestRegistration != null ? guestRegistration.ArrivingBy : 0,
                    ArrivingFrom = guestRegistration != null ? guestRegistration.ArrivingFrom : 0,
                    NextDestination = guestRegistration != null ? guestRegistration.NextDestination : 0,
                    ArrivingFromText = guestRegistration != null ? guestRegistration.ArrivingFromText : null,
                    NextDestinationText = guestRegistration != null ? guestRegistration.NextDestinationText : null,
                    AdditionalInformation = guestRegistration != null ? guestRegistration.AdditionalInformation : null,
                    HealthInsurance = guestRegistration != null ? guestRegistration.HealthInsurance : null,
                    ResponsibleGuestRegistrationId = guestRegistration != null ? guestRegistration.ResponsibleGuestRegistrationId : null,
                    IsLegallyIncompetent = guestRegistration != null ? guestRegistration.IsLegallyIncompetent : false,
                    Document = guestRegistration != null ? guestRegistration.DocumentInformation : null,
                    DocumentTypeId = guestRegistration != null && guestRegistration.DocumentTypeId.HasValue ? guestRegistration.DocumentTypeId.Value : 0,
                    PersonId = leftJoinGuest != null ? leftJoinGuest.PersonId.ToString() : null,

                    //Location
                    Location = new LocationDto
                    {
                        Id = leftLocation != null ? leftLocation.Id : 0,
                        OwnerId = leftLocation != null ? leftLocation.OwnerId : Guid.Empty,
                        Latitude = leftLocation != null ? leftLocation.Latitude : 0,
                        Longitude = leftLocation != null ? leftLocation.Longitude : 0,
                        StreetName = leftLocation != null ? leftLocation.StreetName : null,
                        StreetNumber = leftLocation != null ? leftLocation.StreetNumber : null,
                        AdditionalAddressDetails = leftLocation != null ? leftLocation.AdditionalAddressDetails : null,
                        PostalCode = leftLocation != null ? leftLocation.PostalCode : null,
                        CountryCode = leftLocation != null ? leftLocation.CountryCode : null,

                        //LocationCategory
                        LocationCategory = new LocationCategoryDto
                        {
                            Id = leftLocationCategory != null ? leftLocationCategory.Id : 0,
                            Name = leftLocationCategory != null ? leftLocationCategory.Name : "",
                            RecordScope = leftLocationCategory != null ? leftLocationCategory.RecordScope : ""
                        },

                        LocationCategoryId = leftLocation != null ? leftLocationCategory.Id : 0,
                        Neighborhood = leftLocation != null ? leftLocation.Neighborhood : null,
                        CityId = leftLocation != null ? leftLocation.CityId : 0,
                        City = leftLocation != null ? leftLocation.City.MapTo<CountrySubdivisionDto>() : null,
                        Division = leftStateTranslation != null ? leftState.TwoLetterIsoCode ?? leftStateTranslation.Name : null,
                        Subdivision = leftCityTranslation != null ? leftCityTranslation.Name : null,
                        Country = leftCountryTranslation != null ? leftCountryTranslation.Name : null,

                        AddressTranslation = new AddressTranslationDto
                        {
                            CityId = leftCity != null ? leftCity.Id : 0,
                            CityName = leftCityTranslation != null ? leftCityTranslation.Name : null,
                            StateId = leftState != null ? leftState.Id : 0,
                            StateName = leftStateTranslation != null ? leftStateTranslation.Name : null,
                            CountryId = leftCountry != null ? leftCountry.Id : 0,
                            CountryName = leftCountryTranslation != null ? leftCountryTranslation.Name : null,
                            LanguageIsoCode = leftCityTranslation != null ? leftCityTranslation.LanguageIsoCode : null,
                            TwoLetterIsoCode = leftState != null ? leftState.TwoLetterIsoCode : null,
                            CountryTwoLetterIsoCode = leftCountry != null ? leftCountry.CountryTwoLetterIsoCode : null
                        }
                    },
                    PropertyId = guestRegistration.PropertyId,
                    TenantId = guestRegistration.TenantId,
                    CheckinDate = gri != null ? gri.CheckInDate : null,
                    IsChild = gri.IsChild,
                    ChildAge = gri.ChildAge,
                    IsMain = gri.IsMain,
                    Person = new PersonDto()
                    {
                        ReceiveOffers = (leftPerson != null ? leftPerson.ReceiveOffers : false),
                        SharePersonData = (leftPerson != null ? leftPerson.SharePersonData : false),
                        AllowContact = (leftPerson != null ? leftPerson.AllowContact : false),
                    },
                }
            );

            return queryResult;
        }

        public IQueryable<GuestRegistrationResultDto> GetPersonAsGuestRegistration(string languageIsoCode)
        {

            var queryResult =
            (
                // GuestRegistration
                from person in Context.Persons

                    // Locations
                join l in Context.Locations on person.Id equals l.OwnerId into locations
                from leftLocation in locations.DefaultIfEmpty()

                join lc in Context.LocationCategories on leftLocation.LocationCategoryId equals lc.Id into locationCategories
                from leftLocationCategory in locationCategories.DefaultIfEmpty()

                    // City, State and Country
                join c in Context.CountrySubdivisions on leftLocation.CityId equals c.Id into city
                from leftCity in city.DefaultIfEmpty()

                join s in Context.CountrySubdivisions on leftCity.ParentSubdivision.Id equals s.Id into state
                from leftState in state.DefaultIfEmpty()

                join co in Context.CountrySubdivisions on person.CountrySubdivisionId equals co.Id into country
                from leftCountry in country.DefaultIfEmpty()


                    // City, State and Country Translations
                join ct in Context.CountrySubdivisionTranslations on
                    new { ct1 = leftCity.Id, ct2 = languageIsoCode } equals
                    new { ct1 = ct.CountrySubdivisionId, ct2 = ct.LanguageIsoCode.ToLower() } into cityTranslation
                from leftCityTranslation in cityTranslation.DefaultIfEmpty()

                join st in Context.CountrySubdivisionTranslations on
                    new { st1 = leftState.Id, st2 = languageIsoCode } equals
                    new { st1 = st.CountrySubdivisionId, st2 = st.LanguageIsoCode.ToLower() } into stateTranslation
                from leftStateTranslation in stateTranslation.DefaultIfEmpty()

                join cot in Context.CountrySubdivisionTranslations on
                    new { cot1 = leftCountry.Id, cot2 = languageIsoCode } equals
                    new { cot1 = cot.CountrySubdivisionId, cot2 = cot.LanguageIsoCode.ToLower() } into countryTranslation
                from leftCountryTranslation in countryTranslation.DefaultIfEmpty()

                join g in Context.Guests on person.Id equals g.PersonId into guests
                from leftJoinGuest in guests.DefaultIfEmpty()

                    // Person main document
                join cpd in Context.CountryPersonDocumentTypes on
                new { cpd1 = leftCountry.Id, cpd2 = person.PersonType } equals
                new { cpd1 = cpd.CountrySubdivisionId, cpd2 = cpd.PersonType } into countryPersonDocumentTypes
                from leftCountryPersonDocumentTypes in countryPersonDocumentTypes.DefaultIfEmpty()

                join d in Context.Documents on
                new { d1 = leftCountryPersonDocumentTypes.DocumentTypeId, d2 = person.Id } equals
                new { d1 = d.DocumentTypeId, d2 = d.OwnerId } into documentPerson
                from leftDocumentPerson in documentPerson.DefaultIfEmpty()

                    // Contact Information Linked to Person
                join ciEmail in Context.ContactInformations on
                new { ciEmail1 = person.Id, ciEmail2 = (int)ContactInformationTypeEnum.Email } equals
                new { ciEmail1 = ciEmail.OwnerId, ciEmail2 = ciEmail.ContactInformationTypeId } into email
                from leftEmail in email.DefaultIfEmpty()

                join ciPhone in Context.ContactInformations on
                new { ciPhone1 = person.Id, ciPhone2 = (int)ContactInformationTypeEnum.PhoneNumber } equals
                new { ciPhone1 = ciPhone.OwnerId, ciPhone2 = ciPhone.ContactInformationTypeId } into phone
                from leftPhone in email.DefaultIfEmpty()

                join ciMobilePhone in Context.ContactInformations on
                new { ciMobilePhone1 = person.Id, ciMobilePhone2 = (int)ContactInformationTypeEnum.CellPhoneNumber } equals
                new { ciMobilePhone1 = ciMobilePhone.OwnerId, ciMobilePhone2 = ciMobilePhone.ContactInformationTypeId } into mobilephone
                from leftMobilePhone in email.DefaultIfEmpty()

                    // Person Information
                join piSocialName in Context.PersonInformations on
                new { piSocialName1 = person.Id, piSocialName2 = (int)PersonInformationTypeEnum.SocialName } equals
                new { piSocialName1 = piSocialName.OwnerId, piSocialName2 = piSocialName.PersonInformationTypeId } into socialName
                from leftSocialName in socialName.DefaultIfEmpty()

                join piHealthInsurance in Context.PersonInformations on
                new { piHealthInsurance1 = person.Id, piHealthInsurance2 = (int)PersonInformationTypeEnum.HealthInsurance } equals
                new { piHealthInsurance1 = piHealthInsurance.OwnerId, piHealthInsurance2 = piHealthInsurance.PersonInformationTypeId } into healthInsurance
                from leftHealthInsurance in healthInsurance.DefaultIfEmpty()

                join piGender in Context.PersonInformations on
                new { piGender1 = person.Id, piGender2 = (int)PersonInformationTypeEnum.Gender } equals
                new { piGender1 = piGender.OwnerId, piGender2 = piGender.PersonInformationTypeId } into gender
                from lefGender in healthInsurance.DefaultIfEmpty()

                select new GuestRegistrationResultDto
                {
                    //guestreservationItem
                    GuestReservationItemId = 0,
                    //person
                    Id = person != null ? person.Id : Guid.Empty,
                    FirstName = person != null ? person.FirstName : null,
                    LastName = person != null ? person.LastName : null,
                    FullName = person != null ? person.FirstName : null,
                    SocialName = leftSocialName != null ? leftSocialName.Information : (person != null ? person.FirstName : null),
                    BirthDate = person != null ? person.DateOfBirth : (DateTime?)null,
                    GuestId = leftJoinGuest != null ? leftJoinGuest.Id : 0,
                    Email = leftEmail != null ? leftEmail.Information : null,
                    Gender = lefGender != null ? lefGender.Information : null,
                    OccupationId = null,
                    GuestTypeId = null,
                    PhoneNumber = leftPhone != null ? leftPhone.Information : null,
                    MobilePhoneNumber = leftMobilePhone != null ? leftMobilePhone.Information : null,
                    Nationality = person != null ? person.CountrySubdivisionId : 0,
                    PurposeOfTrip = null,
                    ArrivingBy = null,
                    ArrivingFrom = null,
                    NextDestination = null,
                    ArrivingFromText = "",
                    NextDestinationText = "",
                    AdditionalInformation = "",
                    HealthInsurance = leftHealthInsurance != null ? leftHealthInsurance.Information : "",
                    ResponsibleGuestRegistrationId = null,
                    IsLegallyIncompetent = false,

                    Document = leftDocumentPerson != null ? leftDocumentPerson.DocumentInformation : "",
                    DocumentTypeId = leftDocumentPerson != null ? leftDocumentPerson.DocumentTypeId : 0,
                    PersonId = person != null ? person.Id.ToString() : null,

                    //Location
                    Location = new LocationDto
                    {
                        Id = leftLocation != null ? leftLocation.Id : 0,
                        OwnerId = leftLocation != null ? leftLocation.OwnerId : Guid.Empty,
                        Latitude = leftLocation != null ? leftLocation.Latitude : 0,
                        Longitude = leftLocation != null ? leftLocation.Longitude : 0,
                        StreetName = leftLocation != null ? leftLocation.StreetName : null,
                        StreetNumber = leftLocation != null ? leftLocation.StreetNumber : null,
                        AdditionalAddressDetails = leftLocation != null ? leftLocation.AdditionalAddressDetails : null,
                        PostalCode = leftLocation != null ? leftLocation.PostalCode : null,
                        CountryCode = leftLocation != null ? leftLocation.CountryCode : null,

                        //LocationCategory
                        LocationCategory = new LocationCategoryDto
                        {
                            Id = leftLocationCategory != null ? leftLocationCategory.Id : 0,
                            Name = leftLocationCategory != null ? leftLocationCategory.Name : "",
                            RecordScope = leftLocationCategory != null ? leftLocationCategory.RecordScope : ""
                        },

                        LocationCategoryId = leftLocation != null ? leftLocationCategory.Id : 0,
                        Neighborhood = leftLocation != null ? leftLocation.Neighborhood : null,
                        CityId = leftLocation != null ? leftLocation.CityId : 0,
                        City = leftLocation != null ? leftLocation.City.MapTo<CountrySubdivisionDto>() : null,
                        Division = leftStateTranslation != null ? leftState.TwoLetterIsoCode ?? leftStateTranslation.Name : null,
                        Subdivision = leftCityTranslation != null ? leftCityTranslation.Name : null,
                        Country = leftCountryTranslation != null ? leftCountryTranslation.Name : null,

                        //AddressTranslation
                        AddressTranslation = new AddressTranslationDto
                        {
                            CityId = leftCity != null ? leftCity.Id : 0,
                            CityName = leftCityTranslation != null ? leftCityTranslation.Name : null,
                            StateId = leftState != null ? leftState.Id : 0,
                            StateName = leftStateTranslation != null ? leftStateTranslation.Name : null,
                            CountryId = leftCountry != null ? leftCountry.Id : 0,
                            CountryName = leftCountryTranslation != null ? leftCountryTranslation.Name : null,
                            LanguageIsoCode = leftCityTranslation != null ? leftCityTranslation.LanguageIsoCode : null,
                            TwoLetterIsoCode = leftState != null ? leftState.TwoLetterIsoCode : null,
                            CountryTwoLetterIsoCode = leftCountry != null ? leftCountry.CountryTwoLetterIsoCode : null
                        }
                    },
                    PropertyId = Convert.ToInt32(_applicationUser.PropertyId),
                    TenantId = _applicationUser.TenantId,
                    CheckinDate = null,
                    Person = new PersonDto()
                    {
                        ReceiveOffers = person.ReceiveOffers,
                        SharePersonData = person.SharePersonData,
                        AllowContact = person.AllowContact
                    },
                }
            );

            return queryResult;
        }

        //public GuestRegistrationResultDto queryToDto(GuestRegistrationResultDto queryResult)
        //{
        //    GuestRegistrationResultDto result = null;
        //    if (queryResult != null)
        //    {
        //        result = new GuestRegistrationResultDto
        //        {
        //            GuestReservationItemId = queryResult.GuestReservationItemId,
        //            Id = queryResult.Id,
        //            FirstName = queryResult.FirstName,
        //            LastName = queryResult.LastName,
        //            FullName = queryResult.FullName,
        //            SocialName = queryResult.SocialName,
        //            BirthDate = queryResult.BirthDate,
        //            GuestId = queryResult.GuestId,
        //            Email = queryResult.Email,
        //            Gender = queryResult.Gender,
        //            OccupationId = queryResult.OccupationId,
        //            GuestTypeId = queryResult.GuestTypeId,
        //            PhoneNumber = queryResult.PhoneNumber,
        //            MobilePhoneNumber = queryResult.MobilePhoneNumber,
        //            Nationality = queryResult.Nationality,
        //            PurposeOfTrip = queryResult.PurposeOfTrip,
        //            ArrivingBy = queryResult.ArrivingBy,
        //            ArrivingFrom = queryResult.ArrivingFrom,
        //            NextDestination = queryResult.NextDestination,
        //            ArrivingFromText = queryResult.ArrivingFromText,
        //            NextDestinationText = queryResult.NextDestinationText,
        //            AdditionalInformation = queryResult.AdditionalInformation,
        //            HealthInsurance = queryResult.HealthInsurance,
        //            Document = queryResult.Document,
        //            DocumentTypeId = queryResult.DocumentTypeId,
        //            Location = queryResult.Location,
        //            PersonId = queryResult.PersonId
        //        };
        //    }
        //    return result;
        //}


        public GuestRegistrationResultDto GetLastGuestRegistrationByPropertyAndDocument(long propertyId, DocumentTypeEnum documentType, string documentInformation, string languageIsoCode)
        {
            return GetGuestRegistration(languageIsoCode)
                    .OrderByDescending(t => t.CheckinDate)
                    .Where(q => q.PropertyId == propertyId && q.DocumentTypeId == (int)documentType && q.Document.Contains(documentInformation)).FirstOrDefault();
        }

        public GuestRegistrationResultDto GetLastGuestRegistrationByTenantAndDocument(Guid tenantId, DocumentTypeEnum documentType, string documentInformation, string languageIsoCode)
        {
            return GetGuestRegistration(languageIsoCode)
                    .OrderByDescending(t => t.CheckinDate)
                    .Where(q => q.TenantId == tenantId && q.DocumentTypeId == (int)documentType && q.Document.Contains(documentInformation)).FirstOrDefault();
        }

        public GuestRegistrationResultDto GetLastGuestRegistrationByPropertyAndEmail(long propertyId, string email, string languageIsoCode)
        {
            return GetGuestRegistration(languageIsoCode)
                    .OrderByDescending(t => t.CheckinDate)
                    .Where(q => q.PropertyId == propertyId && q.Email.Contains(email)).FirstOrDefault();
        }

        public GuestRegistrationResultDto GetLastGuestRegistrationByTenantAndEmail(Guid tenantId, string email, string languageIsoCode)
        {
            return GetGuestRegistration(languageIsoCode)
                    .OrderByDescending(t => t.CheckinDate)
                    .Where(q => q.TenantId == tenantId && q.Email.Contains(email)).FirstOrDefault();
        }


        public GuestRegistrationResultDto GetPersonAsGuestRegistrationByDocument(DocumentTypeEnum documentType, string documentInformation, string languageIsoCode)
        {
            return GetPersonAsGuestRegistration(languageIsoCode)
                    .Where(q => q.DocumentTypeId == (int)documentType && q.Document.Contains(documentInformation)).FirstOrDefault();
        }

        public GuestRegistrationResultDto GetPersonAsGuestRegistrationByEmail(string email, string languageIsoCode)
        {
            return GetPersonAsGuestRegistration(languageIsoCode)
                    .Where(q => q.Email.Contains(email)).FirstOrDefault();
        }

        public GuestRegistrationResultDto GetGuestRegistrationBasedOnFilter(SearchGuestRegistrationsDto request)
        {
            GuestRegistrationResultDto result = null;
            if (request != null)
            {
                if (request.ByEmail)
                {

                    result = GetLastGuestRegistrationByPropertyAndEmail(request.PropertyId, request.SearchData, request.LanguageIsoCode);
                    if (result != null) return result;
                    result = GetLastGuestRegistrationByTenantAndEmail(request.TenantId, request.SearchData, request.LanguageIsoCode);
                    if (result != null) return result;
                    result = GetPersonAsGuestRegistrationByEmail(request.SearchData, request.LanguageIsoCode);
                }
                else
                {
                    result = GetLastGuestRegistrationByPropertyAndDocument(request.PropertyId, (DocumentTypeEnum)request.DocumentTypeId, request.SearchData, request.LanguageIsoCode);
                    if (result != null) return result;
                    result = GetLastGuestRegistrationByTenantAndDocument(request.TenantId, (DocumentTypeEnum)request.DocumentTypeId, request.SearchData, request.LanguageIsoCode);
                    if (result != null) return result;
                    result = GetPersonAsGuestRegistrationByDocument((DocumentTypeEnum)request.DocumentTypeId, request.SearchData, request.LanguageIsoCode);
                }
            }
            return result;
        }

        //public IQueryable<GuestRegistrationResultDto> GetPersonFromGuestRegistrationByDocument(long propertyId, DocumentTypeEnum documentType, string documentInformation, string languageIsoCode)
        //{

        //	var queryResult =
        //		(from guestRegistration in Context.GuestRegistrations

        //		 join doc in Context.Documents on
        //		 new { d1 = guestRegistration.Document.DocumentTypeId, d2 = guestRegistration.Id } equals
        //		 new { d1 = doc.DocumentTypeId, d2 = doc.OwnerId }

        //		 join docType in Context.DocumentTypes on
        //		 doc.DocumentTypeId equals docType.Id

        //		 join l in Context.Locations on guestRegistration.Id equals l.OwnerId into locations
        //		 from leftLocation in locations.DefaultIfEmpty()

        //		 join co in Context.CountrySubdivisions on guestRegistration.Nationality equals co.Id into country
        //		 from leftCountry in country.DefaultIfEmpty()

        //		 join nat in Context.CountrySubdivisionTranslations on
        //		 new { nat1 = leftCountry.Id, nat2 = languageIsoCode } equals
        //		 new { nat1 = nat.CountrySubdivisionId, nat2 = nat.LanguageIsoCode.ToLower() } into nationality
        //		 from leftNationality in nationality.DefaultIfEmpty()

        //		 where doc.DocumentTypeId == (int)documentType && doc.DocumentInformation == documentInformation && guestRegistration.PropertyId == propertyId

        //		 select new GuestRegistrationResultDto
        //		 {
        //			 Name = guestRegistration != null ? guestRegistration.FullName : null,
        //			 Document = doc != null ? doc.DocumentInformation : null,
        //			 DocumentTypeId = doc != null ? doc.DocumentTypeId : 0,
        //			 IsDocumentMain = true,
        //			 DocumentType = docType != null ? docType.Name : "",
        //			 PersonInformation = new List<SearchPersonsInformationResultDto>()
        //			 {
        //				 new SearchPersonsInformationResultDto()
        //				 {
        //					 Type = (int)PersonInformationTypeEnum.SocialName,
        //					 TypeValue = PersonInformationTypeEnum.SocialName.ToString(),
        //					 Value = guestRegistration.SocialName
        //				 },
        //				 new SearchPersonsInformationResultDto()
        //				 {
        //					 Type = (int)PersonInformationTypeEnum.HealthInsurance,
        //					 TypeValue = PersonInformationTypeEnum.HealthInsurance.ToString(),
        //					 Value = guestRegistration.HealthInsurance
        //				 },
        //				 new SearchPersonsInformationResultDto()
        //				 {
        //					 Type = (int)PersonInformationTypeEnum.Gender,
        //					 TypeValue = PersonInformationTypeEnum.Gender.ToString(),
        //					 Value = guestRegistration.Gender
        //				 }
        //			 },
        //			 ContactInformation = new List<SearchPersonsContactInformationResultDto>()
        //			 {
        //				 new SearchPersonsContactInformationResultDto()
        //				 {
        //					 Type = (int)ContactInformationTypeEnum.Email,
        //					 TypeValue = ContactInformationTypeEnum.Email.ToString(),
        //					 Value = guestRegistration.Email

        //				 },
        //				 new SearchPersonsContactInformationResultDto()
        //				 {
        //					 Type = (int)ContactInformationTypeEnum.PhoneNumber,
        //					 TypeValue = ContactInformationTypeEnum.PhoneNumber.ToString(),
        //					 Value = guestRegistration.PhoneNumber
        //				 },
        //				 new SearchPersonsContactInformationResultDto()
        //				 {
        //					 Type = (int)ContactInformationTypeEnum.CellPhoneNumber,
        //					 TypeValue = ContactInformationTypeEnum.CellPhoneNumber.ToString(),
        //					 Value = guestRegistration.MobilePhoneNumber
        //				 }
        //			 },
        //			 LocationList = new List<LocationDto>()
        //			 {
        //				 new LocationDto
        //				 {
        //					 Id = leftLocation != null ? leftLocation.Id : 0,
        //					 OwnerId = leftLocation != null ? leftLocation.OwnerId : Guid.Empty,
        //					 Latitude = leftLocation != null ? leftLocation.Latitude : 0,
        //					 Longitude = leftLocation != null ? leftLocation.Longitude : 0,
        //					 StreetName = leftLocation != null ? leftLocation.StreetName : null,
        //					 StreetNumber = leftLocation != null ? leftLocation.StreetNumber : null,
        //					 AdditionalAddressDetails = leftLocation != null ? leftLocation.AdditionalAddressDetails : null,
        //					 PostalCode = leftLocation != null ? leftLocation.PostalCode : 0,
        //					 CountryCode = leftLocation != null ? leftLocation.CountryCode : null,
        //				 }
        //			 },
        //			 CompanyClientId = null
        //		 });

        //	return queryResult;
        //}

        //public IQueryable<SearchPersonsResultDto> GetPersonFromGuestRegistrationByEmail(long propertyId, string email, string languageIsoCode)
        //{

        //	var queryResult =
        //		(from guestRegistration in Context.GuestRegistrations

        //		 join doc in Context.Documents on
        //		 new { d1 = guestRegistration.Document.DocumentTypeId, d2 = guestRegistration.Id } equals
        //		 new { d1 = doc.DocumentTypeId, d2 = doc.OwnerId }

        //		 join docType in Context.DocumentTypes on
        //		 doc.DocumentTypeId equals docType.Id

        //		 join l in Context.Locations on guestRegistration.Id equals l.OwnerId into locations
        //		 from leftLocation in locations.DefaultIfEmpty()

        //		 join co in Context.CountrySubdivisions on guestRegistration.Nationality equals co.Id into country
        //		 from leftCountry in country.DefaultIfEmpty()

        //		 join nat in Context.CountrySubdivisionTranslations on
        //		 new { nat1 = leftCountry.Id, nat2 = languageIsoCode } equals
        //		 new { nat1 = nat.CountrySubdivisionId, nat2 = nat.LanguageIsoCode.ToLower() } into nationality
        //		 from leftNationality in nationality.DefaultIfEmpty()

        //		 where guestRegistration.Email == email && guestRegistration.PropertyId == propertyId

        //		 select new SearchPersonsResultDto
        //		 {
        //			 Name = guestRegistration != null ? guestRegistration.FullName : null,
        //			 Document = doc != null ? doc.DocumentInformation : null,
        //			 DocumentTypeId = doc != null ? doc.DocumentTypeId : 0,
        //			 IsDocumentMain = true,
        //			 DocumentType = docType != null ? docType.Name : "",
        //			 PersonInformation = new List<SearchPersonsInformationResultDto>()
        //			 {
        //				 new SearchPersonsInformationResultDto()
        //				 {
        //					 Type = (int)PersonInformationTypeEnum.SocialName,
        //					 TypeValue = PersonInformationTypeEnum.SocialName.ToString(),
        //					 Value = guestRegistration.SocialName
        //				 },
        //				 new SearchPersonsInformationResultDto()
        //				 {
        //					 Type = (int)PersonInformationTypeEnum.HealthInsurance,
        //					 TypeValue = PersonInformationTypeEnum.HealthInsurance.ToString(),
        //					 Value = guestRegistration.HealthInsurance
        //				 },
        //				 new SearchPersonsInformationResultDto()
        //				 {
        //					 Type = (int)PersonInformationTypeEnum.Gender,
        //					 TypeValue = PersonInformationTypeEnum.Gender.ToString(),
        //					 Value = guestRegistration.Gender
        //				 }
        //			 },
        //			 ContactInformation = new List<SearchPersonsContactInformationResultDto>()
        //			 {
        //				 new SearchPersonsContactInformationResultDto()
        //				 {
        //					 Type = (int)ContactInformationTypeEnum.Email,
        //					 TypeValue = ContactInformationTypeEnum.Email.ToString(),
        //					 Value = guestRegistration.Email

        //				 },
        //				 new SearchPersonsContactInformationResultDto()
        //				 {
        //					 Type = (int)ContactInformationTypeEnum.PhoneNumber,
        //					 TypeValue = ContactInformationTypeEnum.PhoneNumber.ToString(),
        //					 Value = guestRegistration.PhoneNumber
        //				 },
        //				 new SearchPersonsContactInformationResultDto()
        //				 {
        //					 Type = (int)ContactInformationTypeEnum.CellPhoneNumber,
        //					 TypeValue = ContactInformationTypeEnum.CellPhoneNumber.ToString(),
        //					 Value = guestRegistration.MobilePhoneNumber
        //				 }
        //			 },
        //			 LocationList = new List<LocationDto>()
        //			 {
        //				 new LocationDto
        //				 {
        //					 Id = leftLocation != null ? leftLocation.Id : 0,
        //					 OwnerId = leftLocation != null ? leftLocation.OwnerId : Guid.Empty,
        //					 Latitude = leftLocation != null ? leftLocation.Latitude : 0,
        //					 Longitude = leftLocation != null ? leftLocation.Longitude : 0,
        //					 StreetName = leftLocation != null ? leftLocation.StreetName : null,
        //					 StreetNumber = leftLocation != null ? leftLocation.StreetNumber : null,
        //					 AdditionalAddressDetails = leftLocation != null ? leftLocation.AdditionalAddressDetails : null,
        //					 PostalCode = leftLocation != null ? leftLocation.PostalCode : 0,
        //					 CountryCode = leftLocation != null ? leftLocation.CountryCode : null,
        //				 }
        //			 },
        //			 CompanyClientId = null
        //		 });

        //	return queryResult;
        //}

        //public IQueryable<SearchPersonsResultDto> GetPersonFromGuestRegistrationByFullName(long propertyId, string fullName, string languageIsoCode)
        //{

        //	var queryResult =
        //		(from guestRegistration in Context.GuestRegistrations

        //		 join doc in Context.Documents on
        //		 new { d1 = guestRegistration.Document.DocumentTypeId, d2 = guestRegistration.Id } equals
        //		 new { d1 = doc.DocumentTypeId, d2 = doc.OwnerId }

        //		 join docType in Context.DocumentTypes on
        //		 doc.DocumentTypeId equals docType.Id

        //		 join l in Context.Locations on guestRegistration.Id equals l.OwnerId into locations
        //		 from leftLocation in locations.DefaultIfEmpty()

        //		 join co in Context.CountrySubdivisions on guestRegistration.Nationality equals co.Id into country
        //		 from leftCountry in country.DefaultIfEmpty()

        //		 join nat in Context.CountrySubdivisionTranslations on
        //		 new { nat1 = leftCountry.Id, nat2 = languageIsoCode } equals
        //		 new { nat1 = nat.CountrySubdivisionId, nat2 = nat.LanguageIsoCode.ToLower() } into nationality
        //		 from leftNationality in nationality.DefaultIfEmpty()

        //		 where guestRegistration.FullName.ToLower().Contains(fullName.ToLower()) && guestRegistration.PropertyId == propertyId

        //		 select new SearchPersonsResultDto
        //		 {
        //			 Name = guestRegistration != null ? guestRegistration.FullName : null,
        //			 Document = doc != null ? doc.DocumentInformation : null,
        //			 DocumentTypeId = doc != null ? doc.DocumentTypeId : 0,
        //			 IsDocumentMain = true,
        //			 DocumentType = docType != null ? docType.Name : "",
        //			 PersonInformation = new List<SearchPersonsInformationResultDto>()
        //			 {
        //				 new SearchPersonsInformationResultDto()
        //				 {
        //					 Type = (int)PersonInformationTypeEnum.SocialName,
        //					 TypeValue = PersonInformationTypeEnum.SocialName.ToString(),
        //					 Value = guestRegistration.SocialName
        //				 },
        //				 new SearchPersonsInformationResultDto()
        //				 {
        //					 Type = (int)PersonInformationTypeEnum.HealthInsurance,
        //					 TypeValue = PersonInformationTypeEnum.HealthInsurance.ToString(),
        //					 Value = guestRegistration.HealthInsurance
        //				 },
        //				 new SearchPersonsInformationResultDto()
        //				 {
        //					 Type = (int)PersonInformationTypeEnum.Gender,
        //					 TypeValue = PersonInformationTypeEnum.Gender.ToString(),
        //					 Value = guestRegistration.Gender
        //				 }
        //			 },
        //			 ContactInformation = new List<SearchPersonsContactInformationResultDto>()
        //			 {
        //				 new SearchPersonsContactInformationResultDto()
        //				 {
        //					 Type = (int)ContactInformationTypeEnum.Email,
        //					 TypeValue = ContactInformationTypeEnum.Email.ToString(),
        //					 Value = guestRegistration.Email

        //				 },
        //				 new SearchPersonsContactInformationResultDto()
        //				 {
        //					 Type = (int)ContactInformationTypeEnum.PhoneNumber,
        //					 TypeValue = ContactInformationTypeEnum.PhoneNumber.ToString(),
        //					 Value = guestRegistration.PhoneNumber
        //				 },
        //				 new SearchPersonsContactInformationResultDto()
        //				 {
        //					 Type = (int)ContactInformationTypeEnum.CellPhoneNumber,
        //					 TypeValue = ContactInformationTypeEnum.CellPhoneNumber.ToString(),
        //					 Value = guestRegistration.MobilePhoneNumber
        //				 }
        //			 },
        //			 LocationList = new List<LocationDto>()
        //			 {
        //				 new LocationDto
        //				 {
        //					 Id = leftLocation != null ? leftLocation.Id : 0,
        //					 OwnerId = leftLocation != null ? leftLocation.OwnerId : Guid.Empty,
        //					 Latitude = leftLocation != null ? leftLocation.Latitude : 0,
        //					 Longitude = leftLocation != null ? leftLocation.Longitude : 0,
        //					 StreetName = leftLocation != null ? leftLocation.StreetName : null,
        //					 StreetNumber = leftLocation != null ? leftLocation.StreetNumber : null,
        //					 AdditionalAddressDetails = leftLocation != null ? leftLocation.AdditionalAddressDetails : null,
        //					 PostalCode = leftLocation != null ? leftLocation.PostalCode : 0,
        //					 CountryCode = leftLocation != null ? leftLocation.CountryCode : null,
        //				 }
        //			 },
        //			 CompanyClientId = null
        //		 });

        //	return queryResult;
        //}

        public IListDto<GuestRegistrationResultDto> GetAllGuestRegistrationBasedOnFilter(SearchGuestRegistrationsDto request)
        {
            int propertyId = Convert.ToInt32(_applicationUser.PropertyId);

            IQueryable<GuestRegistrationResultDto> dbBaseQuery = null;

            request.Page = request.Page == 0 ? 1 : request.Page;

            if (request.ByEmail) dbBaseQuery = this.GetGuestRegistrationSearch(request.LanguageIsoCode)
                    .Where(q => q.PropertyId == propertyId && q.Email.Contains(request.SearchData));
            else if (request.DocumentTypeId != 0) dbBaseQuery = this.GetGuestRegistrationSearch(request.LanguageIsoCode)
                    .Where(q => q.PropertyId == propertyId && q.DocumentTypeId == request.DocumentTypeId && q.Document.Contains(request.SearchData));
            else dbBaseQuery = this.GetGuestRegistrationSearch(request.LanguageIsoCode)
                    .Where(q => q.PropertyId == propertyId && q.FullName.Contains(request.SearchData));

            var totalItemsDto = dbBaseQuery.Count();
            var resultDto = dbBaseQuery
                        .Skip(request.PageSize * (request.Page - 1))
                        .Take(request.PageSize)
                        .OrderBy(p => p.FullName)
                        .ToList();

            var result = new ListDto<GuestRegistrationResultDto>();
            result.Items = resultDto;
            result.HasNext = totalItemsDto > ((request.Page - 1) * request.PageSize) + resultDto.Count;


            return result;
        }

        public bool GuestRegistrationHasDocument(Guid guestRegistrationId)
        {
            return (from g in Context.GuestRegistrations
                    where g.Id == guestRegistrationId && !String.IsNullOrEmpty(g.DocumentInformation)
                    select g).Any();
        }

        public IListDto<GuestRegistrationResultDto> GetAllResponsibles(long propertyId, long reservationId)
        {
            var result = (from gri in Context.GuestReservationItems
                          join guestRegistration in Context.GuestRegistrations on gri.GuestRegistrationId equals guestRegistration.Id
                          join reservationItem in Context.ReservationItems on gri.ReservationItemId equals reservationItem.Id
                          // Guest and Person
                          join g in Context.Guests on guestRegistration.GuestId equals g.Id into guests
                          from leftGuests in guests.DefaultIfEmpty()

                          join p in Context.Persons on leftGuests.PersonId equals p.Id into person
                          from leftPerson in person.DefaultIfEmpty()

                          where guestRegistration.PropertyId == propertyId &&
                          !gri.IsChild &&
                          guestRegistration.ResponsibleGuestRegistrationId == null &&
                          (gri.GuestStatusId == (int)ReservationStatus.Checkin ||
                          reservationItem.ReservationId == reservationId)
                          select new GuestRegistrationResultDto
                          {
                              //guestreservationItem
                              GuestReservationItemId = gri.Id,
                              //guestRegistration
                              Id = guestRegistration != null ? guestRegistration.Id : Guid.Empty,
                              FirstName = guestRegistration != null ? guestRegistration.FirstName : null,
                              LastName = guestRegistration != null ? guestRegistration.LastName : null,
                              FullName = guestRegistration != null ? guestRegistration.FullName : null,
                              SocialName = guestRegistration != null ? guestRegistration.SocialName : null,
                              BirthDate = guestRegistration != null ? guestRegistration.BirthDate : (DateTime?)null,
                              GuestId = guestRegistration != null ? guestRegistration.GuestId : 0,
                              Email = guestRegistration != null ? guestRegistration.Email : null,
                              Gender = guestRegistration != null ? guestRegistration.Gender : null,
                              OccupationId = guestRegistration != null ? guestRegistration.OccupationId : 0,
                              GuestTypeId = guestRegistration != null ? guestRegistration.GuestTypeId : 0,
                              PhoneNumber = guestRegistration != null ? guestRegistration.PhoneNumber : null,
                              MobilePhoneNumber = guestRegistration != null ? guestRegistration.MobilePhoneNumber : null,
                              Nationality = guestRegistration != null ? guestRegistration.Nationality : 0,
                              PurposeOfTrip = guestRegistration != null ? guestRegistration.PurposeOfTrip : 0,
                              ArrivingBy = guestRegistration != null ? guestRegistration.ArrivingBy : 0,
                              ArrivingFrom = guestRegistration != null ? guestRegistration.ArrivingFrom : 0,
                              NextDestination = guestRegistration != null ? guestRegistration.NextDestination : 0,
                              ArrivingFromText = guestRegistration != null ? guestRegistration.ArrivingFromText : null,
                              NextDestinationText = guestRegistration != null ? guestRegistration.NextDestinationText : null,
                              AdditionalInformation = guestRegistration != null ? guestRegistration.AdditionalInformation : null,
                              HealthInsurance = guestRegistration != null ? guestRegistration.HealthInsurance : null,
                              ResponsibleGuestRegistrationId = guestRegistration != null ? guestRegistration.ResponsibleGuestRegistrationId : null,
                              IsLegallyIncompetent = guestRegistration != null ? guestRegistration.IsLegallyIncompetent : false,
                              Document = null,
                              DocumentTypeId = 0,
                              PersonId = null,
                              //Location
                              Location = null,
                              PropertyId = guestRegistration.PropertyId,
                              TenantId = guestRegistration.TenantId,
                              CheckinDate = gri != null ? gri.CheckInDate : null,
                              IsChild = gri.IsChild,
                              ChildAge = gri.ChildAge,
                              IsMain = gri.IsMain,
                              Person = new PersonDto()
                              {
                                  ReceiveOffers = (leftPerson != null ? leftPerson.ReceiveOffers : false),
                                  SharePersonData = (leftPerson != null ? leftPerson.SharePersonData : false),
                                  AllowContact = (leftPerson != null ? leftPerson.AllowContact : false),
                              },
                          }).ToList();

            return new ListDto<GuestRegistrationResultDto>()
            {
                HasNext = false,
                Items = result,
            };

        }

        public IListDto<GuestRegistrationResultDto> GetAllResponsibleFromReservation(long reservationId)
        {
            var result = (from gri in Context.GuestReservationItems
                          join guestRegistration in Context.GuestRegistrations on gri.GuestRegistrationId equals guestRegistration.Id
                          join reservationItem in Context.ReservationItems on gri.ReservationItemId equals reservationItem.Id
                          join reservation in Context.Reservations on reservationItem.ReservationId equals reservation.Id
                          // Guest and Person
                          join g in Context.Guests on guestRegistration.GuestId equals g.Id into guests
                          from leftGuests in guests.DefaultIfEmpty()

                          join p in Context.Persons on leftGuests.PersonId equals p.Id into person

                          from leftPerson in person.DefaultIfEmpty()
                          where reservation.Id == reservationId &&
                                !gri.IsChild &&
                                guestRegistration.ResponsibleGuestRegistrationId == null &&
                                (gri.GuestStatusId == (int)ReservationStatus.Checkin ||
                                reservationItem.ReservationId == reservationId)
                          select new GuestRegistrationResultDto
                          {
                              //guestreservationItem
                              GuestReservationItemId = gri.Id,
                              //guestRegistration
                              Id = guestRegistration != null ? guestRegistration.Id : Guid.Empty,
                              FirstName = guestRegistration != null ? guestRegistration.FirstName : null,
                              LastName = guestRegistration != null ? guestRegistration.LastName : null,
                              FullName = guestRegistration != null ? guestRegistration.FullName : null,
                              SocialName = guestRegistration != null ? guestRegistration.SocialName : null,
                              BirthDate = guestRegistration != null ? guestRegistration.BirthDate : (DateTime?)null,
                              GuestId = guestRegistration != null ? guestRegistration.GuestId : 0,
                              Email = guestRegistration != null ? guestRegistration.Email : null,
                              Gender = guestRegistration != null ? guestRegistration.Gender : null,
                              OccupationId = guestRegistration != null ? guestRegistration.OccupationId : 0,
                              GuestTypeId = guestRegistration != null ? guestRegistration.GuestTypeId : 0,
                              PhoneNumber = guestRegistration != null ? guestRegistration.PhoneNumber : null,
                              MobilePhoneNumber = guestRegistration != null ? guestRegistration.MobilePhoneNumber : null,
                              Nationality = guestRegistration != null ? guestRegistration.Nationality : 0,
                              PurposeOfTrip = guestRegistration != null ? guestRegistration.PurposeOfTrip : 0,
                              ArrivingBy = guestRegistration != null ? guestRegistration.ArrivingBy : 0,
                              ArrivingFrom = guestRegistration != null ? guestRegistration.ArrivingFrom : 0,
                              NextDestination = guestRegistration != null ? guestRegistration.NextDestination : 0,
                              ArrivingFromText = guestRegistration != null ? guestRegistration.ArrivingFromText : null,
                              NextDestinationText = guestRegistration != null ? guestRegistration.NextDestinationText : null,
                              AdditionalInformation = guestRegistration != null ? guestRegistration.AdditionalInformation : null,
                              HealthInsurance = guestRegistration != null ? guestRegistration.HealthInsurance : null,
                              ResponsibleGuestRegistrationId = guestRegistration != null ? guestRegistration.ResponsibleGuestRegistrationId : null,
                              IsLegallyIncompetent = guestRegistration != null ? guestRegistration.IsLegallyIncompetent : false,
                              Document = null,
                              DocumentTypeId = 0,
                              PersonId = null,
                              //Location
                              Location = null,
                              PropertyId = guestRegistration.PropertyId,
                              TenantId = guestRegistration.TenantId,
                              CheckinDate = gri != null ? gri.CheckInDate : null,
                              IsChild = gri.IsChild,
                              ChildAge = gri.ChildAge,
                              IsMain = gri.IsMain,
                              Person = new PersonDto()
                              {
                                  ReceiveOffers = (leftPerson != null ? leftPerson.ReceiveOffers : false),
                                  SharePersonData = (leftPerson != null ? leftPerson.SharePersonData : false),
                                  AllowContact = (leftPerson != null ? leftPerson.AllowContact : false),
                              },

                          }).ToList();

            return new ListDto<GuestRegistrationResultDto>()
            {
                HasNext = false,
                Items = result,
            };
        }

        public bool IsChild(long id)
        {
            var reservationItem = Context.GuestReservationItems.FirstOrDefault(x => x.Id == id);
            if (reservationItem != null)
                return reservationItem.IsChild;

            return false;
        }

        public string GetNationalityByGuestReservationItemId(long guestReservationItemId)
            => (from g in Context.GuestReservationItems.AsNoTracking()
                join gr in Context.GuestRegistrations.AsNoTracking() on g.GuestRegistrationId equals gr.Id
                join n in Context.CountrySubdivisions.AsNoTracking() on gr.Nationality equals n.Id
                where g.Id == guestReservationItemId
                select new
                {
                    n.TwoLetterIsoCode
                }).FirstOrDefault()?.TwoLetterIsoCode;

        public GuestRegistrationResultDto GetById(Guid guestRegistrationId)
        => (from guestRegistration in Context.GuestRegistrations
            join l in Context.Locations on guestRegistration.Id equals l.OwnerId into locations
            from leftLocation in locations.DefaultIfEmpty()
            where guestRegistration.Id == guestRegistrationId
            select new GuestRegistrationResultDto
            {
                Id = guestRegistration.Id,
                Email = guestRegistration.Email,
                Nationality = guestRegistration.Nationality,
                Document = guestRegistration.DocumentInformation,
                Location = new LocationDto
                {
                    Id = leftLocation != null ? leftLocation.Id : 0,
                    OwnerId = leftLocation != null ? leftLocation.OwnerId : Guid.Empty,
                    Latitude = leftLocation != null ? leftLocation.Latitude : null,
                    Longitude = leftLocation != null ? leftLocation.Longitude : null,
                    StreetName = leftLocation != null ? leftLocation.StreetName : null,
                    StreetNumber = leftLocation != null ? leftLocation.StreetNumber : null,
                    AdditionalAddressDetails = leftLocation != null ? leftLocation.AdditionalAddressDetails : "",
                    PostalCode = leftLocation != null ? leftLocation.PostalCode : null,
                    CountryCode = leftLocation != null ? leftLocation.CountryCode : null,
                }
            }).FirstOrDefault();

        private ICollection<GuestRegistrationDocumentDto> GetAllDocumentsByGuestRegistrationId(Guid guestRegistrationId)
           => Context.GuestRegistrationDocuments.Include(grd => grd.DocumentType).Where(grd => grd.GuestRegistrationId == guestRegistrationId)
                                                    .Select(grd => new GuestRegistrationDocumentDto
                                                    {
                                                        Id = grd.Id,
                                                        DocumentInformation = grd.DocumentInformation,
                                                        DocumentTypeId = grd.DocumentTypeId,
                                                        DocumentTypeName = grd.DocumentType.Name
                                                    }).ToList();

        public Guid? GetIdByGuestReservationItemId(long guestReservationItemId)
        {
            return (from g in Context.GuestReservationItems
                    where g.Id == guestReservationItemId
                    select g.GuestRegistrationId).FirstOrDefault();
        }

        public GuestRegistration GetByGuestId(long guestId)
             => Context.GuestRegistrations.FirstOrDefault(x => x.GuestId == guestId);
    }
}