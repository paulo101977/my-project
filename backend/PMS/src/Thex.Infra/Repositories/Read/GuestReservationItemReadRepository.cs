﻿//  <copyright file="GuestReservationItemReadRepository.cs" company="TOTVS">
//  Copyright (c) TOTVS. All rights reserved.
//  </copyright>

using System.Linq;
using Thex.Domain.Interfaces.Repositories;

namespace Thex.EntityFrameworkCore.Repositories
{
    using System;
    using System.Collections.Generic;
    using Domain.Entities;
    using Tnf.EntityFrameworkCore.Repositories;
    using Tnf.EntityFrameworkCore;
    using Thex.Common.Enumerations;
    using Tnf.Dto;
    using Thex.Dto;
    using Thex.Infra.Context;
    using Microsoft.EntityFrameworkCore;
    using Thex.Dto.GuestReservationItem;
    using System.Threading.Tasks;

    public class GuestReservationItemReadRepository : EfCoreRepositoryBase<ThexContext, GuestReservationItem>, IGuestReservationItemReadRepository
    {
        public GuestReservationItemReadRepository(IDbContextProvider<ThexContext> dbContextProvider)
            : base(dbContextProvider)
        {

        }

        public List<GuestReservationItem> GetListGuestReservationForCheckin(List<long> ids)
        {
            return Context.GuestReservationItems.Where(g => ids.Contains(g.Id))
                .Include(g => g.ReservationItem)
                .ToList();
        }

        public int GetSumOfPctDailyRateItemsByReservationIdExceptId(long reservationItemId, long guestReservationItemId)
        {
            return Context
                .GuestReservationItems

                .Where(exp => exp.ReservationItemId == reservationItemId && exp.Id != guestReservationItemId)
                .Sum(exp => exp.PctDailyRate);
        }

        public bool AnyGuestReservationItemWithStatusEqualCheckin(List<long> ids)
        {
            return Context
                    .GuestReservationItems

                    .Any(g => ids.Contains(g.Id) && g.GuestStatusId == (int)ReservationStatus.Checkin);
        }

        public ListDto<GuestReservationItemDto> GetGuestReservationItensByRoomId(long roomId)
        {
            var dbBaseQuery = (from guestReservationItem in Context.GuestReservationItems
                               join reservationItem in Context.ReservationItems on guestReservationItem.ReservationItemId equals reservationItem.Id
                               join room in Context.Rooms on reservationItem.RoomId equals room.Id
                               join currency in Context.Currencies on reservationItem.CurrencyId equals currency.Id

                               let totalBudgetQuery = Context.ReservationBudgets.Where(x => x.ReservationItemId == guestReservationItem.ReservationItemId)

                               where room.Id == roomId && reservationItem.ReservationItemStatusId == (int)ReservationStatus.Checkin
                               select new GuestReservationItemDto
                               {
                                   Id = guestReservationItem.Id,
                                   GuestName = guestReservationItem.GuestName,
                                   ReservationItemId = guestReservationItem.ReservationItemId,
                                   CheckInDate = guestReservationItem.CheckInDate,
                                   EstimatedDepartureDate = guestReservationItem.EstimatedDepartureDate,
                                   CurrentBudget = totalBudgetQuery.Sum(x => x.ManualRate),
                                   CurrencyId = currency.Id,
                                   CurrencySymbol = currency.Symbol

                               }).ToList();

            var result = new ListDto<GuestReservationItemDto>
            {
                HasNext = false,
                Items = dbBaseQuery,
            };

            return result;
        }

        public GuestReservationItem GetGuestReservationItemByBilligAccountId(Guid billingAccountId)
        {
            var billingAccount = Context.BillingAccounts.AsNoTracking()
                .Include(x => x.GuestReservationItem)
                .FirstOrDefault(x => x.Id == billingAccountId);
            if (billingAccount != null)
                return billingAccount.GuestReservationItem;

            return null;
        }

        public List<GuestReservationItem> GetListGuestReservationPerReservationItemIdForCheckin(long id)
        {
            return Context.GuestReservationItems.Where(g => Context.GuestReservationItems
                                                .Where(gr => gr.Id == id && g.GuestRegistrationId.HasValue)
                                                .Select(gr => gr.ReservationItemId)
                                                .Contains(g.ReservationItemId))
                                                .Include(ri => ri.ReservationItem)
                                                .ToList();
        }

        public List<long> GetAllIdByStatus(int guestStatusId)
        {
            return (from g in Context.GuestReservationItems
                    where g.GuestStatusId == guestStatusId
                    select g.Id).ToList();
        }

        public GuestReservationItemWithCountrySubdivisionDto GetReservationItemWithCountrySubdivision(long guestReservationItemId, string languageIsoCode)
        {
            return (
                 from r in Context.GuestReservationItems
                 join gr in Context.GuestRegistrations on r.GuestRegistrationId equals gr.Id into rgr
                 from leftJoinGuestRegistration in rgr.DefaultIfEmpty()
                 join gt in Context.PropertyGuestTypes on leftJoinGuestRegistration.GuestTypeId equals gt.Id into grgt
                 from leftJoinGuestType in grgt.DefaultIfEmpty()
                 from cNat in Context.CountrySubdivisionTranslations.Where(x => x.Nationality == r.GuestCitizenship && x.LanguageIsoCode == languageIsoCode).DefaultIfEmpty()
                 from cName in Context.CountrySubdivisionTranslations.Where(x => x.Name == r.GuestCitizenship && x.LanguageIsoCode == languageIsoCode).DefaultIfEmpty()
                 where r.Id == guestReservationItemId

                 select new GuestReservationItemWithCountrySubdivisionDto
                 {
                     GuestName = r.GuestName,
                     FullName = r.GuestName,
                     GuestEmail = r.GuestEmail,
                     GuestDocument = r.GuestDocument,
                     GuestDocumentTypeId = r.GuestDocumentTypeId,
                     GuestTypeId = leftJoinGuestType != null ? leftJoinGuestType.Id : default(int),
                     GuestTypeName = leftJoinGuestType != null ? leftJoinGuestType.GuestTypeName : null,
                     PreferredName = r.PreferredName,
                     CountrySubdivisionId = cNat.CountrySubdivisionId > 0? cNat.CountrySubdivisionId : cName.CountrySubdivisionId,
                     GuestId = r.GuestId,
                 }).FirstOrDefault();
        }

        public List<GuestReservationItem> GetAllByReservationItemId(long reservationItemId)
         => Context.GuestReservationItems
            .Where(g => g.ReservationItemId == reservationItemId)
            .ToList();

        public async Task<List<PropertyDashboardGuestDto>> GetAllForPropertyDashboardAsync(DateTime systemDate, int propertyId)
        {
            return await Context.GuestReservationItems
                          .Include(gri => gri.GuestDocumentType)
                          .Include(gri => gri.ReservationItem).ThenInclude(ri => ri.ReceivedRoomType)
                          .Include(gri => gri.ReservationItem).ThenInclude(ri => ri.Reservation)
                          .Include(gri => gri.BillingAccountList)
                          .Where(gri => (gri.EstimatedArrivalDate.Date == systemDate && !gri.CheckInDate.HasValue) ||
                                        (gri.EstimatedDepartureDate.Date == systemDate && !gri.CheckOutDate.HasValue)
                                        && gri.PropertyId == propertyId)
                          .Select(gri => new PropertyDashboardGuestDto
                          {
                              GuestName = !string.IsNullOrWhiteSpace(gri.GuestName) ? gri.GuestName : gri.ReservationItem.Reservation.ContactName,
                              GuestReservationItemId = gri.Id,
                              ReservationItemCode = gri.ReservationItem.IsMigrated ? gri.ReservationItem.PartnerReservationNumber : gri.ReservationItem.ReservationItemCode,
                              ReservationItemId = gri.ReservationItemId,
                              RoomType = gri.ReservationItem.ReceivedRoomType.Name,
                              GuestDocument = gri.GuestDocument,
                              GuestDocumentTypeName = gri.GuestDocumentTypeId.HasValue ? gri.GuestDocumentType.Name : null,
                              GuestStatusId = gri.GuestStatusId,
                              IsMigrated = gri.ReservationItem.IsMigrated,
                              IsGroupReservation = !string.IsNullOrWhiteSpace(gri.ReservationItem.Reservation.GroupName),
                              BillingAccountId = gri.BillingAccountList != null && gri.BillingAccountList.Any() ? gri.BillingAccountList.FirstOrDefault().Id : Guid.Empty
                          }).ToListAsync();
        }
    }
}
