﻿using Thex.Domain.Entities;
using Thex.Dto;
using Thex.Infra.Context;
using Thex.Infra.ReadInterfaces;
using Tnf.EntityFrameworkCore;
using Tnf.EntityFrameworkCore.Repositories;
using System.Linq;
using Tnf.Dto;

namespace Thex.Infra.Repositories.Read
{
    public class BillingInvoiceModelReadRepository : EfCoreRepositoryBase<ThexContext, BillingInvoiceModel>, IBillingInvoiceModelReadRepository
    {
        public BillingInvoiceModelReadRepository(IDbContextProvider<ThexContext> dbContextProvider)
            : base(dbContextProvider)
        {
        }

        public IListDto<BillingInvoiceModelDto> GetAllVisible()
        {
            var dbBaseQuery = from billingInvoiceModel in Context.BillingInvoiceModels
                              where billingInvoiceModel.IsVisible
                              select new BillingInvoiceModelDto
                              {
                                  Id = billingInvoiceModel.Id,
                                  Description = billingInvoiceModel.Description,
                                  TaxModel = billingInvoiceModel.TaxModel,
                                  IsVisible = billingInvoiceModel.IsVisible,
                                  BillingInvoiceTypeId = billingInvoiceModel.BillingInvoiceTypeId
                              };

            var result = new ListDto<BillingInvoiceModelDto>()
            {
                HasNext = false,
                Items = dbBaseQuery.ToList(),
            };

            return result;
        }

        public int GetIdByBillingInvoiceTypeId(int billingInvoiceTypeId)
        {
            return (from billingInvoiceModel in Context.BillingInvoiceModels
                    where billingInvoiceModel.BillingInvoiceTypeId == billingInvoiceTypeId
                    select billingInvoiceModel.Id).FirstOrDefault();
        }

        public int GetInvoiceTypeByModel(int billingInvoiceModelId)
        {
            return (from billingInvoiceModel in Context.BillingInvoiceModels
                    where billingInvoiceModel.Id == billingInvoiceModelId
                    select billingInvoiceModel.BillingInvoiceTypeId).FirstOrDefault();
        }
    }
}
