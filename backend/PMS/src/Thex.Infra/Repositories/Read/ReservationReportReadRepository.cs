﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Thex.Common;
using Thex.Common.Enumerations;
using Thex.Domain.Entities;
using Thex.Domain.Interfaces.Repositories;
using Thex.Dto;
using Thex.Dto.Report;
using Thex.Dto.Reservation;
using Thex.Dto.ReservationReport;
using Thex.Dto.Room;
using Thex.Dto.SearchReservationReportDto;
using Thex.Infra.Context;
using Thex.Infra.ReadInterfaces;
using Tnf.Dto;
using Tnf.EntityFrameworkCore;
using Tnf.EntityFrameworkCore.Repositories;
using Tnf.Localization;

namespace Thex.Infra.Repositories.Read
{
    public class ReservationReportReadRepository : EfCoreRepositoryBase<ThexContext, Reservation>, IReservationReportReadRepository
    {
        private readonly ILocalizationManager _localizationManager;
        private readonly IRoomRepository _roomRepository;
        private readonly IRoomBlockingReadRepository _roomBlockingReadRepository;
        private readonly IRoomStatusReadRepository _roomStatusReadRepository;
        private readonly IPropertyParameterReadRepository _propertyParameterReadRepository;

        public ReservationReportReadRepository(
            IDbContextProvider<ThexContext> dbContextProvider, ILocalizationManager localizationManager,
            IRoomRepository roomRepository, IRoomBlockingReadRepository roomBlockingReadRepository,
            IRoomStatusReadRepository roomStatusReadRepository,
            IPropertyParameterReadRepository propertyParameterReadRepository)
            : base(dbContextProvider)
        {
            _localizationManager = localizationManager;
            _roomRepository = roomRepository;
            _roomBlockingReadRepository = roomBlockingReadRepository;
            _roomStatusReadRepository = roomStatusReadRepository;
            _propertyParameterReadRepository = propertyParameterReadRepository;
        }

        public IListDto<SearchReservationReportResultDto> GetAllByFilters(SearchReservationReportDto request, int propertyId, List<ReservationStatus> status, DateTime systemDate)
        {
            var dbBaseQuery = SearchReservationBaseQuery(request, propertyId, status, systemDate);

            var totalItemsDto = dbBaseQuery.Count();

            dbBaseQuery = SearchReservationSorting(request, dbBaseQuery);
            dbBaseQuery = SearchReservationPaging(request, dbBaseQuery);

            var resultDto = dbBaseQuery.OrderBy(x => x.RoomNumber.ToString().Length).ThenBy(x => x.RoomNumber).ToList();

            foreach (var reservation in resultDto)
            {
                reservation.GuestReservationItemStatusName = string.IsNullOrEmpty(_localizationManager.GetString(AppConsts.LocalizationSourceName, ((ReservationStatus)reservation.GuestReservationItemStatusId).ToString())) ?
                                                             "" : _localizationManager.GetString(AppConsts.LocalizationSourceName, ((ReservationStatus)reservation.GuestReservationItemStatusId).ToString());
                reservation.ReservationItemStatusName = string.IsNullOrEmpty(_localizationManager.GetString(AppConsts.LocalizationSourceName, ((ReservationStatus)reservation.ReservationItemStatusId).ToString())) ?
                                                        "" : _localizationManager.GetString(AppConsts.LocalizationSourceName, ((ReservationStatus)reservation.ReservationItemStatusId).ToString());
            }

            var result =
                new ListDto<SearchReservationReportResultDto>
                {
                    Items = resultDto,
                    HasNext = SearchReservationHasNext(request, totalItemsDto, resultDto),
                };

            return result;
        }

        private IQueryable<SearchReservationReportResultDto> SearchReservationBaseQuery(SearchReservationReportDto request, int propertyId, List<ReservationStatus> status, DateTime sytemDate)
        {

            var dbBaseQuery = (from reserv in this.Context.Reservations
                               join reservItem in this.Context.ReservationItems on reserv.Id equals reservItem.Reservation.Id
                               join receivRoomType in this.Context.RoomTypes on reservItem.ReceivedRoomType.Id equals receivRoomType.Id
                               join requesRoomType in this.Context.RoomTypes on reservItem.RequestedRoomType.Id equals requesRoomType.Id

                               join reservConfir in this.Context.ReservationConfirmations
                               on reserv.Id equals reservConfir.ReservationId

                               join ratePlan in this.Context.RatePlans
                               on reservItem.RatePlanId equals ratePlan.Id into rp
                               from leftRatePlan in rp.DefaultIfEmpty()

                               join room in this.Context.Rooms on reservItem.Room.Id equals room.Id into ri
                               from leftRoom in ri.DefaultIfEmpty()

                               join guestReserv in this.Context.GuestReservationItems
                               on reservItem.Id equals guestReserv.ReservationItem.Id

                               join companyclient in this.Context.CompanyClients on reserv.CompanyClientId equals companyclient.Id into cc
                               from leftcompanyclient in cc.DefaultIfEmpty()

                               join propertyGuestType in this.Context.PropertyGuestTypes
                               on guestReserv.GuestTypeId equals propertyGuestType.Id


                               join user in this.Context.Users
                               on reserv.CreatorUserId equals user.Id into user
                               from leftuser in user.DefaultIfEmpty()

                               let totalBudgetQuery = Context.ReservationBudgets.Where(e => e.ReservationItemId == reservItem.Id)
                               let totalBudgetObj = totalBudgetQuery.FirstOrDefault()

                               where reserv.Property.Id == propertyId

                               select new SearchReservationReportResultDto
                               {
                                   Id = reserv.Id,
                                   GuestName = guestReserv.GuestName,
                                   GuestTypeId = propertyGuestType.Id,
                                   GuestTypeName = propertyGuestType.GuestTypeName,
                                   ReservationId = reserv.Id,
                                   ReservationCode = reservItem.ReservationItemCode,
                                   RoomNumber = leftRoom != null ? leftRoom.RoomNumber : null,
                                   RequestedRoomTypeAbbreviation = requesRoomType.Abbreviation,
                                   AdultsAndChildren = guestReserv.IsMain ? $"{(reservItem.AdultCount < 10 ? "0" : "")}{reservItem.AdultCount}|" +
                                                       $"0{(reservItem.ChildCount >= 1 ? 1 : 0)}|0{(reservItem.ChildCount >= 2 ? 1 : 0)}|" +
                                                       $"0{(reservItem.ChildCount >= 3 ? 1 : 0)}" : "Acomp.",
                                   Adult = guestReserv.IsMain ? reservItem.AdultCount : 0,
                                   Child = guestReserv.IsMain ? reservItem.ChildCount : 0,
                                   Client = leftcompanyclient.ShortName,
                                   MealPlanTypeAmount = Convert.ToDecimal(Context.ReservationBudgets.Where(e => e.ReservationItemId == reservItem.Id)
                                                        .FirstOrDefault().MealPlanTypeAgreementRate).Equals(0) ?
                                                        Convert.ToDecimal(Context.ReservationBudgets.Where(e => e.ReservationItemId == reservItem.Id)
                                                        .FirstOrDefault().MealPlanTypeBaseRate +
                                                        Context.ReservationBudgets.Where(e => e.ReservationItemId == reservItem.Id).FirstOrDefault()
                                                        .ChildMealPlanTypeRate) :
                                                        Convert.ToDecimal(Context.ReservationBudgets.Where(e => e.ReservationItemId == reservItem.Id)
                                                        .FirstOrDefault().MealPlanTypeAgreementRate +
                                                        Context.ReservationBudgets.Where(e => e.ReservationItemId == reservItem.Id).FirstOrDefault()
                                                        .ChildMealPlanTypeAgreementRate),
                                   MealPlan = Context.ReservationBudgets.FirstOrDefault(e => e.ReservationItemId == reservItem.Id && e.BudgetDay.Date == sytemDate.Date) == null ?
                                             _localizationManager.GetString(AppConsts.LocalizationSourceName,
                                                 ((MealPlanTypeEnum)Context.ReservationBudgets.Where(e => e.ReservationItemId == reservItem.Id).FirstOrDefault().MealPlanTypeId)
                                                 .ToString()) :
                                             _localizationManager.GetString(AppConsts.LocalizationSourceName,
                                                 ((MealPlanTypeEnum)Context.ReservationBudgets.FirstOrDefault(e => e.ReservationItemId == reservItem.Id && e.BudgetDay.Date == sytemDate.Date).MealPlanTypeId)
                                                 .ToString()),
                                   MealPlanLastDay = Context.ReservationBudgets.FirstOrDefault(e => e.ReservationItemId == reservItem.Id && e.BudgetDay.Date == sytemDate.Date.AddDays(-1)) == null ? "" :
                                             _localizationManager.GetString(AppConsts.LocalizationSourceName,
                                                 ((MealPlanTypeEnum)Context.ReservationBudgets.FirstOrDefault(e => e.ReservationItemId == reservItem.Id && e.BudgetDay.Date == sytemDate.Date.AddDays(-1)).MealPlanTypeId)
                                                 .ToString()),
                                   IsBudget = Context.ReservationBudgets.FirstOrDefault(e => e.ReservationItemId == reservItem.Id && e.BudgetDay.Date == sytemDate.Date) != null,
                                   IsBudgetLastDay = Context.ReservationBudgets.FirstOrDefault(e => e.ReservationItemId == reservItem.Id && e.BudgetDay.Date == sytemDate.Date.AddDays(-1)) != null,
                                   MealPlanType = Context.MealPlanTypes.FirstOrDefault(x => x.Id == Context.ReservationBudgets.FirstOrDefault(e => e.ReservationItemId == reservItem.Id && e.BudgetDay.Date == sytemDate.Date).MealPlanTypeId).MealPlanTypeCode,
                                   MealPlanTypeId = Context.ReservationBudgets.FirstOrDefault(e => e.ReservationItemId == reservItem.Id && e.BudgetDay.Date == sytemDate.Date).MealPlanTypeId,
                                   DailyValue = Convert.ToDecimal(totalBudgetObj.ManualRate),
                                   ArrivalDate = reservItem.CheckInDate ?? reservItem.EstimatedArrivalDate,
                                   DepartureDate = reservItem.CheckOutDate ?? reservItem.EstimatedDepartureDate,
                                   IsInternalUsage = reservItem.GratuityTypeId == null || !guestReserv.IsMain ? false :
                                   Context.GratuityTypes.Where(e => e.Id == reservItem.GratuityTypeId).FirstOrDefault().Id.Equals(GratuityTypeEnum.GratuityTypeInternalUsage),
                                   IsCourtesy = reservItem.GratuityTypeId == null || !guestReserv.IsMain ? false :
                                   Context.GratuityTypes.Where(e => e.Id == reservItem.GratuityTypeId).FirstOrDefault().Id.Equals(GratuityTypeEnum.GratuityTypeCourtesy),
                                   IsAdult = !guestReserv.IsChild,
                                   IsChild = guestReserv.IsChild,
                                   IsCompanion = guestReserv.IsMain,
                                   EnsuresNoShow = reservConfir.EnsuresNoShow,
                                   UserName = leftuser.Name,
                                   AgreementTypeId = leftRatePlan.AgreementTypeId,
                                   AgreementName = leftRatePlan.AgreementName,
                                   Deposit = reservConfir.PaymentTypeId == (int)PaymentTypeEnum.Deposit ? reservConfir.Value : 0,

                                   ReservationItemCode = reservItem.ReservationItemCode,
                                   ReservationItemId = reservItem.Id,
                                   ReservationItemStatusId = reservItem.ReservationItemStatusId,
                                   GuestReservationItemStatusId = guestReserv.GuestStatusId,
                                   EstimatedArrivalDateGuest = guestReserv.CheckInDate ?? guestReserv.EstimatedArrivalDate,
                                   EstimatedDepartureDateGuest = guestReserv.CheckOutDate ?? guestReserv.EstimatedDepartureDate,
                                   CancellationDate = reservItem.CancellationDate,
                                   RequestedRoomTypeId = requesRoomType.Id,
                                   RequestedRoomTypeName = requesRoomType.Name,
                                   ReceivedRoomTypeId = receivRoomType.Id,
                                   ReceivedRoomTypeName = receivRoomType.Name,
                                   ReceivedRoomTypeAbbreviation = receivRoomType.Abbreviation,
                                   GuestId = guestReserv.GuestId,
                                   GuestIsIncognito = guestReserv.IsIncognito,
                                   GuestLanguage = string.IsNullOrEmpty(guestReserv.GuestLanguage) ? "" : guestReserv.GuestLanguage,
                                   GroupName = reserv.GroupName,
                                   CreationTime = reservItem.CreationTime,
                                   ReservationBy = string.Empty,
                                   Deadline = reserv.Deadline,
                                   GuestDocument = guestReserv.GuestDocument,
                                   Total = totalBudgetQuery.Sum(e => Convert.ToDouble(e.ManualRate)),
                                   CurrencySymbol = string.IsNullOrEmpty(Context.ReservationBudgets.Where(e => e.ReservationItemId == reservItem.Id).Select(e => e.CurrencySymbol).FirstOrDefault()) ? "" :
                                                    Context.ReservationBudgets.Where(e => e.ReservationItemId == reservItem.Id).Select(e => e.CurrencySymbol).FirstOrDefault(),
                                   GuestReservationItemId = guestReserv.Id,
                                   TenantId = reserv.TenantId,
                                   CompanyClientName = leftcompanyclient != null ? leftcompanyclient.TradeName : string.Empty,
                                   IsMain = guestReserv.IsMain,
                                   InternalComments = reserv.InternalComments ?? null,
                                   ExternalComments = reserv.ExternalComments ?? null,
                                   PartnerComments = reserv.PartnerComments ?? null
                               });

            dbBaseQuery = dbBaseQuery.Where(s => status.Contains((ReservationStatus)Enum.Parse(typeof(ReservationStatus), s.ReservationItemStatusId.ToString()))
                                            && status.Contains((ReservationStatus)Enum.Parse(typeof(ReservationStatus), s.GuestReservationItemStatusId.ToString())))
                                     .OrderBy(x => x.RoomNumber).ThenBy(x => x.AdultsAndChildren);

            return dbBaseQuery;
        }

        private IQueryable<SearchReservationReportResultDto> GroupReservation(SearchReservationReportDto request, IQueryable<SearchReservationReportResultDto> dbBaseQuery)
        {
            if (request.IsMain)
                dbBaseQuery = dbBaseQuery.Where(x => x.IsMain);

            return dbBaseQuery;
        }

        private IQueryable<SearchReservationReportResultDto> SearchReservationSorting(SearchReservationReportDto request, IQueryable<SearchReservationReportResultDto> dbBaseQuery)
        {
            dbBaseQuery = dbBaseQuery.OrderByRequestDto(request);

            return dbBaseQuery;
        }

        private IQueryable<SearchReservationReportResultDto> SearchReservationPaging(SearchReservationReportDto request, IQueryable<SearchReservationReportResultDto> dbBaseQuery)
        {
            var pageSize = request.PageSize ?? 1000;
            var page = request.Page ?? 1;
            dbBaseQuery = page == 1 ? dbBaseQuery : dbBaseQuery.Skip(pageSize * (page - 1)).Take(pageSize);
            return dbBaseQuery;
        }

        private bool SearchReservationHasNext(SearchReservationReportDto request, int totalItemsDto, List<SearchReservationReportResultDto> resultDto)
        {
            return totalItemsDto
                   > ((request.Page - 1) * request.PageSize)
                   + resultDto.Count();
        }

        public ListDto<SearchMeanPlanControlReportResultDto> GetAllMeanPlanByFilters(SearchReservationReportDto request, int propertyId, DateTime systemDate, int checkInHour)
        {
            ListDto<SearchMeanPlanControlReportResultDto> searchMeanPlanControlReportResultDtos = new ListDto<SearchMeanPlanControlReportResultDto>();

            var contextDatabase = Context.Database;
            var conn = contextDatabase.GetDbConnection();

            if (conn.State == System.Data.ConnectionState.Closed)
                conn.Open();

            using (var command = conn.CreateCommand())
            {
                var query = $"select distinct GuestMealPlanControlId, c.GuestReservationItemId, isnull(RoomNumber,'') as UH, " +
                            $"f.MealPlanTypeName, f.MealPlanTypeId, fa.mealplantypename as mealplantypenamelastday, fa.mealplantypeid as mealplantypeidlastday, " +
                            $"case when f.mealplantypename is null then 0 else 1 end as hasbudget, " +
                            $"case when fa.mealplantypename is null then 0 else 1 end as hasbudgetlastday, " +
                            $"case when f.mealplantypename is not null or fa.mealplantypename is not null then 1 else 0 end as hasanybudget, " +
                            $"i.PropertyGuestTypeId, i.GuestTypeName, GuestName, " +
                            $"case IsMain when 1 then 'Principal' when 0 then 'Acompanhante' end as [Type], " +
                            $"IsChild, isnull(Coffee,0) as Coffee, isnull(Lunch,0) as Lunch, isnull(Dinner,0) as Dinner, " +
                            $"len(isnull(RoomNumber,'')) as LenUH " +
                            $"from Reservation a join ReservationItem b on a.ReservationId = b.ReservationId " +
                            $"join GuestReservationItem c on c.ReservationItemId = b.ReservationItemId " +
                            $"left join ReservationBudget rb on rb.ReservationItemId = b.ReservationItemId and rb.budgetDay = (SELECT CAST(CAST('{systemDate.Year.ToString()}' " +
                            $"AS varchar) + '-' + CAST('{systemDate.Month.ToString()}' AS varchar) + '-' + " +
                            $"CAST('{systemDate.Day.ToString()}' AS varchar) AS DATETIME)) and rb.IsDeleted = 0 " +
                            $"left join MealPlanType f on f.MealPlanTypeId = rb.MealPlanTypeId " +
                            $"left join ReservationBudget rba on rba.ReservationItemId = b.ReservationItemId and rba.budgetDay = dateadd(day,-1,(SELECT CAST(CAST('{systemDate.Year.ToString()}' " +
                            $"AS varchar) + '-' + CAST('{systemDate.Month.ToString()}' AS varchar) + '-' + " +
                            $"CAST('{systemDate.Day.ToString()}' AS varchar) AS DATETIME))) and rba.IsDeleted = 0 " +
                            $"left join MealPlanType fa on fa.MealPlanTypeId = rba.MealPlanTypeId " +
                            $"left join Room g on g.RoomId = b.RoomId " +
                            $"left join GuestMealPlanControl h on h.GuestReservationItemId = c.GuestReservationItemId " +
                            $"left join PropertyGuestType i on i.PropertyGuestTypeId = c.GuestTypeId " +
                            $"where a.PropertyId = {propertyId} and b.ReservationItemStatusId = {(int)ReservationStatus.Checkin} and c.GuestStatusId = {(int)ReservationStatus.Checkin} " +
                            $"order by LenUH, UH, [Type] desc";

                if (contextDatabase.CurrentTransaction != null)
                    command.Transaction = contextDatabase.CurrentTransaction.GetDbTransaction();

                command.CommandText = query;

                var reader = command.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        var searchMeanPlanControlReportResultDto = new SearchMeanPlanControlReportResultDto()
                        {
                            GuestMealPlanControlId = reader.GetValue(0).ToString(),
                            GuestReservationItemId = Convert.ToInt32(reader.GetValue(1)),
                            UH = reader.GetValue(2).ToString(),
                            MealPlanTypeName = Convert.ToBoolean(reader.GetValue(9)) ?
                            Convert.ToBoolean(reader.GetValue(7)) ?
                            DateTime.UtcNow.ToZonedDateTimeLoggedUser().Hour < checkInHour ?
                            Convert.ToBoolean(reader.GetValue(8)) ? 
                            _localizationManager.GetString(AppConsts.LocalizationSourceName,
                                                 ((MealPlanTypeEnum)reader.GetValue(6))
                                                 .ToString()) :
                            _localizationManager.GetString(AppConsts.LocalizationSourceName,
                                                 (MealPlanTypeEnum.None).ToString()) : 
                            _localizationManager.GetString(AppConsts.LocalizationSourceName,
                                                 ((MealPlanTypeEnum)reader.GetValue(4))
                                                 .ToString()) : 
                            _localizationManager.GetString(AppConsts.LocalizationSourceName,
                                                 ((MealPlanTypeEnum)reader.GetValue(6))
                                                 .ToString()) :
                            _localizationManager.GetString(AppConsts.LocalizationSourceName,
                                                 (MealPlanTypeEnum.None).ToString()),
                            MealPlanTypeId = Convert.ToBoolean(reader.GetValue(9)) ?
                            Convert.ToBoolean(reader.GetValue(7)) ?
                            DateTime.UtcNow.ToZonedDateTimeLoggedUser().Hour < checkInHour ?
                            Convert.ToBoolean(reader.GetValue(8))  ? Convert.ToInt32(reader.GetValue(6)) :
                            Convert.ToInt32(MealPlanTypeEnum.None) : Convert.ToInt32(reader.GetValue(4)) :
                            Convert.ToInt32(reader.GetValue(6))    : Convert.ToInt32(MealPlanTypeEnum.None),
                            PropertyGuestTypeId = Convert.ToInt32(reader.GetValue(10)),
                            GuestTypeName = reader.GetValue(11).ToString(),
                            FullName = reader.GetValue(12).ToString(),
                            Type = reader.GetValue(13).ToString(),
                            IsChild = Convert.ToBoolean(reader.GetValue(14)),
                            Coffee = Convert.ToBoolean(reader.GetValue(15)),
                            Lunch = Convert.ToBoolean(reader.GetValue(16)),
                            Dinner = Convert.ToBoolean(reader.GetValue(17))
                        };
                        searchMeanPlanControlReportResultDtos.Items.Add(searchMeanPlanControlReportResultDto);
                    }
                }

                reader.Dispose();

            }

            return searchMeanPlanControlReportResultDtos;
        }

        public ListDto<SearchGuestSaleReportResultDto> GetAllGuestSaleByFilters(int propertyId, bool guestSale,
            bool sparseSale, bool masterGroupSale, bool companySale, bool eventSale, bool pendingSale, bool positiveSale, bool canceledSale,
            bool noShowSale, double? sale, bool aheadSale)
        {
            ListDto<SearchGuestSaleReportResultDto> searchGuestSaleReportResultDtos = new ListDto<SearchGuestSaleReportResultDto>();

            var contextDatabase = Context.Database;
            var conn = contextDatabase.GetDbConnection();

            if (conn.State == System.Data.ConnectionState.Closed)
                conn.Open();

            using (var command = conn.CreateCommand())
            {

                var where = "";
                var whereSaldo = "";

                // contas de hóspede com saldo
                if (guestSale)
                    where += "and CF.BillingAccountTypeId = 2 ";

                // contas de avulsas com saldo
                if (sparseSale)
                    where += "and CF.BillingAccountTypeId = 1 ";

                // contas master de grupo com saldo
                if (masterGroupSale)
                    where += "and CF.BillingAccountTypeId = 4 ";

                // contas de empresa com saldo
                if (companySale)
                    where += "and CF.BillingAccountTypeId = 3 ";

                // contas de eventos com saldo
                if (eventSale)
                    where += "and GF.BillingItemCategoryId = 5 ";

                // contas de pendentes com saldo
                if (pendingSale)
                    where += "and ReservationItemStatusId =  4 ";

                // contas com saldo maior que zero, inibir as contas com saldo negativo
                if (positiveSale)
                    whereSaldo += "and total > 0 ";

                // não emitir somente as contas de reservas canceladas com saldo
                if (canceledSale)
                    where += "and ReservationItemStatusId !=  6 ";

                // não emitir somente as contas de reservas no-show com saldo
                if (noShowSale)
                    where += "and ReservationItemStatusId !=  5 ";

                // contas com saldo acima do valor informado
                if (sale != null)
                    whereSaldo += $"and total > {sale.ToString().Replace(",",".")} ";

                // não emitir as contas de reservas com saldo com datas a frente da data de hoje
                if (aheadSale)
                    where += "and BillingAccountItemDate <= Getdate() ";


                var query = $"select a.*, CASE WHEN A.CurrencyId IS NULL THEN " +
                            $"(select symbol as propertysymbol from currency " +
                            $"join propertyparameter on upper(propertyparametervalue) = currencyid " +
                            $"and propertyid = {propertyId} and applicationparameterid = {(int)ApplicationParameterEnum.ParameterDefaultCurrency}) " +
                            $"WHEN A.CurrencyId = C.CurrencyId THEN symbol END AS CurrencySymbol " +
                            $"from (select HO.PropertyId,UH.RoomNumber,COALESCE(RF.RESERVATIONITEMCODE,'') AS NUMRESERVA, " +
                            $"COALESCE(CF.BILLINGACCOUNTNAME,'GERAL') AS NOMECONTA, HO.Name AS NOMEHOTEL, HO.TenantId, " +
                            $"CASE CF.BillingAccountTypeId WHEN 1 THEN 'AVULSA' WHEN 2 THEN 'HOSPEDE' WHEN 3 THEN 'CLIENTE' " +
                            $"WHEN 4 THEN 'GRUPO' END AS TIPOCONTA, SUM(CASE WHEN GF.BillingItemCategoryId=1 THEN LF.Amount ELSE 0 END) " +
                            $"AS Restaurante, SUM(CASE WHEN GF.BillingItemCategoryId=2 THEN LF.Amount ELSE 0 END) AS Outros, " +
                            $"SUM(CASE WHEN GF.BillingItemCategoryId=3 THEN LF.Amount ELSE 0 END) AS Bares, " +
                            $"SUM(CASE WHEN GF.BillingItemCategoryId=4 THEN LF.Amount ELSE 0 END) AS Diária, " +
                            $"SUM(CASE WHEN GF.BillingItemCategoryId=5 THEN LF.Amount ELSE 0 END) AS Eventos, " +
                            $"SUM(CASE WHEN GF.BillingItemCategoryId=6 THEN LF.Amount ELSE 0 END) AS Banquetes, " +
                            $"SUM(CASE WHEN GF.BillingItemCategoryId=7 THEN LF.Amount ELSE 0 END) AS Lavandeira, " +
                            $"SUM(CASE WHEN GF.BillingItemCategoryId=8 THEN LF.Amount ELSE 0 END) AS Telecomunicacoes, " +
                            $"SUM(CASE WHEN GF.BillingItemCategoryId=9 THEN LF.Amount ELSE 0 END) AS ISS, " +
                            $"SUM(CASE WHEN GF.BillingItemCategoryId=10 THEN LF.Amount ELSE 0 END) AS TaxadeServico, " +
                            $"SUM(CASE WHEN GF.BillingItemCategoryId IS NULL THEN ISNULL(LF.amount,0) ELSE 0 END) AS Creditos, " +
                            $"SUM(ISNULL(LF.amount,0)) AS TOTAL, Max(LF.CurrencyId) as CurrencyId " +
                            $"from BillingAccount CF INNER JOIN Property HO ON CF.PROPERTYID=HO.PropertyId " +
                            $"LEFT JOIN BillingAccountItem LF ON CF.BillingAccountId=LF.BillingAccountId " +
                            $"LEFT JOIN BillingItem TD ON LF.BillingItemId=TD.BillingItemId " +
                            $"LEFT JOIN BillingItemType TL ON TD.BillingItemTypeID=TL.BillingItemTypeId " +
                            $"LEFT JOIN BillingItemCategory GR ON TD.BillingItemCategoryId=GR.BillingItemCategoryId " +
                            $"LEFT JOIN BillingItemCategory GF ON GR.StandardCategoryId =GF.BillingItemCategoryId " +
                            $"LEFT JOIN Reservationitem RF ON CF.RESERVATIONITEMID=RF.ReservationItemId " +
                            $"LEFT JOIN ROOM UH ON RF.RoomId=UH.RoomId where HO.PropertyId = {propertyId} {where} " +
                            $"GROUP BY HO.PropertyId,UH.RoomNumber,COALESCE(RF.RESERVATIONITEMCODE,'') , " +
                            $"COALESCE(CF.BILLINGACCOUNTNAME,'GERAL') , HO.Name, HO.TenantId, CF.BillingAccountTypeId) a " +
                            $"left join (select currencyid,symbol from currency) C on A.CurrencyId = C.CurrencyId " +
                            $"where PropertyId = {propertyId} {whereSaldo} order BY PropertyId";

                if (contextDatabase.CurrentTransaction != null)
                    command.Transaction = contextDatabase.CurrentTransaction.GetDbTransaction();

                command.CommandText = query;

                var reader = command.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        var searchGuestSaleReportResultDto = new SearchGuestSaleReportResultDto()
                        {
                            PropertyId = Convert.ToInt32(reader.GetValue(0)),
                            RoomNumber = reader.GetValue(1).ToString(),
                            NumReserva = reader.GetValue(2).ToString(),
                            NomeConta = reader.GetValue(3).ToString(),
                            NomeHotel = reader.GetValue(4).ToString(),
                            TenantId = new Guid(reader.GetValue(5).ToString()),
                            TipoConta = reader.GetValue(6).ToString(),
                            Restaurante = Convert.ToDouble(reader.GetValue(7)),
                            Outros = Convert.ToDouble(reader.GetValue(8)),
                            Bares = Convert.ToDouble(reader.GetValue(9)),
                            Diaria = Convert.ToDouble(reader.GetValue(10)),
                            Eventos = Convert.ToDouble(reader.GetValue(11)),
                            Banquetes = Convert.ToDouble(reader.GetValue(12)),
                            Lavandeira = Convert.ToDouble(reader.GetValue(13)),
                            Telecomunicacoes = Convert.ToDouble(reader.GetValue(14)),
                            Iss = Convert.ToDouble(reader.GetValue(15)),
                            TaxadeServico = Convert.ToDouble(reader.GetValue(16)),
                            Creditos = Convert.ToDouble(reader.GetValue(17)),
                            Total = Convert.ToDouble(reader.GetValue(18)),
                            CurrencySymbol = reader.GetValue(20).ToString()
                        };
                        searchGuestSaleReportResultDtos.Items.Add(searchGuestSaleReportResultDto);
                    }
                }

                reader.Dispose();

            }

            return searchGuestSaleReportResultDtos;
        }

        public IListDto<SearchReservationReportResultDto> GetAllCheckByFilters(SearchReservationReportDto request, int propertyId,
            List<ReservationStatus> listStatus, DateTime systemDate)
        {
            var dbBaseQuery = SearchReservationBaseQuery(request, propertyId, listStatus, systemDate);

            if (!string.IsNullOrEmpty(request.GroupName))
                dbBaseQuery = dbBaseQuery.Where(x => x.GroupName.ToLower().Contains(request.GroupName.ToLower()));

            if (!string.IsNullOrEmpty(request.CompanyClientName))
                dbBaseQuery = dbBaseQuery.Where(x => x.CompanyClientName.ToLower().Contains(request.CompanyClientName.ToLower()));

            if (request.ReservationItemStatus.HasValue)
                dbBaseQuery = dbBaseQuery.Where(x => x.ReservationItemStatusId == request.ReservationItemStatus);

            if (!string.IsNullOrEmpty(request.ClientName))
                dbBaseQuery = dbBaseQuery.Where(x => x.Client.ToLower().Contains(request.ClientName.ToLower()));

            if (request.IsCheckIn)
            {
                if (request.DateInitial.HasValue && request.DateFinal.HasValue)
                    dbBaseQuery = dbBaseQuery.Where(x =>
                                                x.ArrivalDate.Date >= request.DateInitial.Value.Date &&
                                                x.ArrivalDate.Date <= request.DateFinal.Value.Date).OrderBy(x => x.ArrivalDate.Date)
                                                .ThenBy(x => x.RoomNumber).ThenBy(x => x.ReservationCode);
            }
            else
            {
                if (request.DateInitial.HasValue && request.DateFinal.HasValue)
                    dbBaseQuery = dbBaseQuery.Where(x =>
                                                x.DepartureDate.Date >= request.DateInitial.Value.Date &&
                                                x.DepartureDate.Date <= request.DateFinal.Value.Date).OrderBy(x => x.DepartureDate.Date)
                                                .ThenBy(x => x.RoomNumber).ThenBy(x => x.ReservationCode);
            }

            if (!string.IsNullOrEmpty(request.UserName))
                dbBaseQuery = dbBaseQuery.Where(x => x.UserName.ToLower().Contains(request.UserName.ToLower()));

            var totalGuests = dbBaseQuery.Count();
            var totalReservations = dbBaseQuery.Count(x => x.IsMain);

            dbBaseQuery = GroupReservation(request, dbBaseQuery);
            dbBaseQuery = SearchReservationSorting(request, dbBaseQuery);
            dbBaseQuery = SearchReservationPaging(request, dbBaseQuery);

            var totalItemsDto = dbBaseQuery.Count();

            var resultDto = dbBaseQuery.ToList();

            foreach (var reservation in resultDto)
            {
                reservation.MealPlanType = reservation.MealPlan;
                reservation.GuestReservationItemStatusName = string.IsNullOrEmpty(_localizationManager.GetString(AppConsts.LocalizationSourceName, ((ReservationStatus)reservation.GuestReservationItemStatusId).ToString())) ?
                                                             "" : _localizationManager.GetString(AppConsts.LocalizationSourceName, ((ReservationStatus)reservation.GuestReservationItemStatusId).ToString());
                reservation.ReservationItemStatusName = string.IsNullOrEmpty(_localizationManager.GetString(AppConsts.LocalizationSourceName, ((ReservationStatus)reservation.ReservationItemStatusId).ToString())) ?
                                                        "" : _localizationManager.GetString(AppConsts.LocalizationSourceName, ((ReservationStatus)reservation.ReservationItemStatusId).ToString());
            }

            var result =
                new ListReservationDto<SearchReservationReportResultDto>
                {
                    TotalGuests = totalGuests,
                    TotalReservations = totalReservations,

                    Items = resultDto,
                    HasNext = SearchReservationHasNext(request, totalItemsDto, resultDto),
                };

            return result;
        }

        public ListDto<SearchIssuedNotesReportResultDto> GetAllIssuedNotes(int propertyId, SearchIssuedNotesReportDto requestDto)
        {
            ListDto<SearchIssuedNotesReportResultDto> searchIssuedNotesReportResultDto = new ListDto<SearchIssuedNotesReportResultDto>();

            var contextDatabase = Context.Database;
            var conn = contextDatabase.GetDbConnection();

            if (conn.State == System.Data.ConnectionState.Closed)
                conn.Open();

            #region IssuedNotesFilters
            var filters = "";

            if (requestDto.InitialDate.HasValue)
                filters += $"and BI.ExternalEmissionDate >= '{requestDto.InitialDate.Value.ToString("yyyy-MM-dd")} 00:00:00.000'";

            if (requestDto.EndDate.HasValue)
                filters += $"and BI.ExternalEmissionDate <= '{requestDto.EndDate.Value.ToString("yyyy-MM-dd")} 23:59:59.000'";

            if (requestDto.FilterOnlyCancelledNotes.HasValue && requestDto.FilterOnlyCancelledNotes.Value)
                filters += $"and BI.Billinginvoicestatusid = {(int)BillingInvoiceStatusEnum.Canceled}";

            if (requestDto.NumberInitialNote.HasValue)
                filters += $"and BI.ExternalNumber >= {requestDto.NumberInitialNote.Value}";

            if (requestDto.NumberEndNote.HasValue)
                filters += $"and BI.ExternalNumber <= {requestDto.NumberEndNote.Value}";

            if (!string.IsNullOrWhiteSpace(requestDto.ReservationNumber))
                filters += $"and RF.reservationitemcode like '%{requestDto.ReservationNumber}%'";

            if (!string.IsNullOrWhiteSpace(requestDto.CompanyClientName))
                filters += $"and CC.TradeName like '%{requestDto.CompanyClientName}%'";
            #endregion


            using (var command = conn.CreateCommand())
            {
                var query = $"SELECT BA.billingaccountid, " +
                            $"       CASE BA.billingaccounttypeid " +
                            $"         WHEN 1 THEN COALESCE(HEADER.fullname, Cia.tradename) " +
                            $"         WHEN 2 THEN COALESCE(HEADER.fullname, Gri.guestname) " +
                            $"         WHEN 3 THEN COALESCE(HEADER.fullname, Cia.tradename) " +
                            $"         WHEN 4 THEN COALESCE(HEADER.fullname, Cia.tradename, Gri.guestname) " +
                            $"       END AS RecipientsName,  " +
                            $"       UH.roomnumber,  " +
                            $"       COALESCE(RF.reservationitemcode, '')     AS NUMRESERVA, " +
                            $"       COALESCE(BA.billingaccountname, 'GERAL') AS NOMECONTA, " +
                            $"       BM.description AS Type,  " +
                            $"       BI.externalnumber AS Number,  " +
                            $"       BI.externalseries AS Serie,  " +
                            $"       BI.externalrps AS RPS,  " +
                            $"       BI.externalemissiondate as emissiondate,  " +
                            $"       CONVERT(date, BI.canceldate),  " +
                            $"       CASE BI.billinginvoicestatusid " +
                            $"         WHEN 9 THEN  'Nao Enviada' " +
                            $"         WHEN 10 THEN 'Emitida' " +
                            $"         WHEN 11 THEN 'Pendente' " +
                            $"         WHEN 12 THEN 'Erro' " +
                            $"         WHEN 13 THEN 'Cancelando' " +
                            $"         WHEN 14 THEN 'Cancelada' " +
                            $"       END AS Status,  " +
                            $"       ROUND(sum(pay.Deposito) * (sum(lf.amount) /case when sum(pay.totalamount) = 0 then 1 else sum(pay.totalamount) end),2) as deposito,  " +
                            $"       ROUND(sum(pay.Cartao) * (sum(lf.amount) /case when sum(pay.totalamount) = 0 then 1 else sum(pay.totalamount) end),2)  as cartao,  " +
                            $"       ROUND(sum(pay.Faturar) * (sum(lf.amount) /case when sum(pay.totalamount) = 0 then 1 else sum(pay.totalamount) end),2) as Faturar,  " +
                            $"       ROUND(sum(pay.Cheque) * (sum(lf.amount) /case when sum(pay.totalamount) = 0 then 1 else sum(pay.totalamount) end),2) as Cheque,  " +
                            $"       ROUND(sum(pay.Dinheiro) * (sum(lf.amount) /case when sum(pay.totalamount) = 0 then 1 else sum(pay.totalamount) end),2) as Dinheiro,  " +
                            $"       ROUND(sum(pay.OutrosReceb) * (sum(lf.amount) /case when sum(pay.totalamount) = 0 then 1 else sum(pay.totalamount) end),2) as OutrosReceb, " +
                            $"       Sum(CASE " +
                            $"             WHEN GF.billingitemcategoryid = 1 THEN LF.amount " +
                            $"             ELSE 0 " +
                            $"           END)                                 AS Restaurante, " +
                            $"       Sum(CASE " +
                            $"             WHEN GF.billingitemcategoryid = 2 THEN LF.amount " +
                            $"             ELSE 0 " +
                            $"           END)                                 AS Outros, " +
                            $"       Sum(CASE " +
                            $"             WHEN GF.billingitemcategoryid = 3 THEN LF.amount " +
                            $"             ELSE 0 " +
                            $"           END)                                 AS Bares, " +
                            $"       Sum(CASE " +
                            $"             WHEN GF.billingitemcategoryid = 4 THEN LF.amount " +
                            $"             ELSE 0 " +
                            $"           END)                                 AS Diária, " +
                            $"       Sum(CASE " +
                            $"             WHEN GF.billingitemcategoryid = 5 THEN LF.amount " +
                            $"             ELSE 0 " +
                            $"           END)                                 AS Eventos, " +
                            $"       Sum(CASE " +
                            $"             WHEN GF.billingitemcategoryid = 6 THEN LF.amount " +
                            $"             ELSE 0 " +
                            $"           END)                                 AS Banquetes, " +
                            $"       Sum(CASE " +
                            $"             WHEN GF.billingitemcategoryid = 7 THEN LF.amount " +
                            $"             ELSE 0 " +
                            $"           END)                                 AS Lavandeira, " +
                            $"       Sum(CASE " +
                            $"             WHEN GF.billingitemcategoryid = 8 THEN LF.amount " +
                            $"             ELSE 0 " +
                            $"           END)                                 AS Telecomunicacoes, " +
                            $"       Sum(CASE " +
                            $"             WHEN GF.billingitemcategoryid = 9 THEN LF.amount " +
                            $"             ELSE 0 " +
                            $"           END)                                 AS ISS, " +
                            $"       Sum(CASE " +
                            $"             WHEN GF.billingitemcategoryid = 10 THEN LF.amount " +
                            $"             ELSE 0 " +
                            $"           END)                                 AS TaxadeServico, " +
                            $"       Sum(LF.amount)                           AS TOTAL, " +
                            $"       Sum(CASE " +
                            $"             WHEN TL.billingitemtypeid = 1 THEN LF.amount " +
                            $"             ELSE 0 " +
                            $"           END)                                 AS Serviço, " +
                            $"       Sum(CASE " +
                            $"             WHEN TL.billingitemtypeid = 2 " +
                            $"                  AND GF.billingitemcategoryid != 9 THEN LF.amount " +
                            $"             ELSE 0 " +
                            $"           END)                                 AS Taxa, " +
                            $"       Sum(LF.amount " +
                            $"           ) *-1                                AS TipodePagamento, " +
                            $"       Sum(CASE " +
                            $"             WHEN TL.billingitemtypeid = 4 THEN LF.amount " +
                            $"             ELSE 0 " +
                            $"           END)                                 AS PontodeVenda, " +
                            $"       ST.statusId, " +
                            $"       BI.billingInvoiceTypeId, " +
                            $"       MAX(Symbol) as Symbol " +
                            $"FROM billingaccount BA " +
                            $"       INNER JOIN billingaccountitem LF " +
                            $"               ON BA.billingaccountid = LF.billingaccountid " +
                            $"                  AND LF.isdeleted = 0 " +
                            $"       INNER JOIN billinginvoice BI " +
                            $"               ON BI.BillingAccountId = BA.BillingAccountId  " +
                            $"       INNER JOIN " +
                            $"               (SELECT BillingInvoiceId " +
                            $"                FROM BillingAccountItem BAI " +
                            $"                UNION" +
                            $"                SELECT BillingInvoiceId " +
                            $"                FROM BillingAccountItemInvoiceHistory BAIIH) as U " +
                            $"                ON U.BillingInvoiceId = BI.billinginvoiceid " +
                            $"       INNER JOIN billinginvoiceproperty BP " +
                            $"               ON BI.billinginvoicepropertyid = BP.billinginvoicepropertyid " +
                            $"       INNER JOIN billinginvoicemodel BM " +
                            $"               ON BP.billinginvoicemodelid = BM.billinginvoicemodelid " +
                            $"       INNER JOIN property PR " +
                            $"               ON PR.propertyid = BI.propertyid " +
                            $"       INNER JOIN status ST " +
                            $"               ON ST.statusid = bi.billinginvoicestatusid " +
                            $"       INNER JOIN billingitem TD " +
                            $"               ON LF.billingitemid = TD.billingitemid " +
                            $"       RIGHT JOIN paymenttype PT " +
                            $"               ON PT.paymenttypeid = TD.billingitemtypeid " +
                            $"       LEFT JOIN billingitemcategory GR " +
                            $"              ON TD.billingitemcategoryid = GR.billingitemcategoryid " +
                            $"       LEFT JOIN billingitemcategory GF " +
                            $"              ON GR.standardcategoryid = GF.billingitemcategoryid " +
                            $"       INNER JOIN billingitemtype TL " +
                            $"               ON TD.billingitemtypeid = TL.billingitemtypeid " +
                            $"       LEFT JOIN reservationitem RF " +
                            $"              ON BA.reservationitemid = RF.reservationitemid " +
                            $"       LEFT JOIN person header " +
                            $"              ON header.personid = bi.personheaderid " +
                            $"       LEFT JOIN guestreservationitem Gri " +
                            $"              ON BA.guestreservationitemid = Gri.guestreservationitemid " +
                            $"       LEFT JOIN companyclient Cia " +
                            $"              ON Cia.companyclientid = BA.companyclientid " +
                            $"       LEFT JOIN reservation RR " +
                            $"              ON RF.reservationid = RR.reservationid " +
                            $"       LEFT JOIN companyclient CC " +
                            $"              ON RR.companyclientid = CC.companyclientid " +
                            $"       LEFT JOIN room UH " +
                            $"              ON RF.roomid = UH.roomid " +
                            $"       LEFT JOIN currency C " +
                            $"              ON LF.CurrencyId = C.CurrencyId " +
                            $"       LEFT JOIN(SELECT Sum(CASE " +
                            $"             WHEN PayIt.paymenttypeid = 2 THEN Pay.amount " +
                            $"             ELSE 0 " +
                            $"           END)                                 AS Deposito, " +
                            $"       Sum(CASE " +
                            $"             WHEN PayIt.paymenttypeid = 5 THEN Pay.amount " +
                            $"             ELSE 0 " +
                            $"           END)                                 AS Cartao, " +
                            $"       Sum(CASE " +
                            $"             WHEN PayIt.paymenttypeid = 6 THEN Pay.amount " +
                            $"             ELSE 0 " +
                            $"           END)                                 AS Faturar, " +
                            $"       Sum(CASE " +
                            $"             WHEN PayIt.paymenttypeid = 7 THEN Pay.amount " +
                            $"             ELSE 0 " +
                            $"           END)                                 AS Cheque, " +
                            $"       Sum(CASE " +
                            $"             WHEN PayIt.paymenttypeid = 8 THEN Pay.amount " +
                            $"             ELSE 0 " +
                            $"           END)                                 AS Dinheiro, " +
                            $"       Sum(CASE " +
                            $"             WHEN PayIt.billingitemtypeid = 3 " +
                            $"                  AND PayIt.paymenttypeid NOT IN(2, 5, 6, 7, 8) THEN Pay.amount " +
                            $"             ELSE 0 " +
                            $"           END)                                 AS OutrosReceb, Pay.billingaccountid, " +
                            $"           sum(pay.amount) as TotalAmount " +
                            $"                        FROM BillingAccountItem Pay " +
                            $"       INNER JOIN BillingItem PayIt " +
                            $"              ON Pay.BillingItemId = PayIt.BillingItemId And Payit.BillingItemTypeId = 3 And Pay.IsDeleted = 0 " +
                            $"              Group By  Pay.billingaccountId) " +
                            $"              Pay on Ba.billingaccountId = pay.billingaccountId " +
                            $"where PR.PropertyId = {propertyId} {filters}" +
                            $"GROUP  BY BA.billingaccountid,  " +
                            $"          BI.externalrps,  " +
                            $"          PR.propertyid,  " +
                            $"          UH.roomnumber,  " +
                            $"          COALESCE(RF.reservationitemcode, ''),  " +
                            $"          COALESCE(BA.billingaccountname, 'GERAL'),  " +
                            $"          BA.billingaccounttypeid,  " +
                            $"          BI.billinginvoicenumber,  " +
                            $"          BI.externalemissiondate,  " +
                            $"          BI.canceldate,  " +
                            $"          BI.billinginvoicestatusid,  " +
                            $"          BM.description,  " +
                            $"          BI.externalnumber,  " +
                            $"          BI.externalseries,  " +
                            $"          ST.statusId, " +
                            $"          BI.billingInvoiceTypeId, " +
                            $"          BI.externalemissiondate, " +
                            $"          CASE BA.billingaccounttypeid" +
                            $"            WHEN 1 THEN COALESCE(HEADER.fullname, Cia.tradename) " +
                            $"            WHEN 2 THEN COALESCE(HEADER.fullname, Gri.guestname) " +
                            $"            WHEN 3 THEN COALESCE(HEADER.fullname, Cia.tradename) " +
                            $"            WHEN 4 THEN COALESCE(HEADER.fullname, Cia.tradename, Gri.guestname) " +
                            $"          END " +
                            $"ORDER  BY PR.propertyid,  " +
                            $"          BI.billinginvoicenumber";

                if (contextDatabase.CurrentTransaction != null)
                    command.Transaction = contextDatabase.CurrentTransaction.GetDbTransaction();

                command.CommandText = query;

                var reader = command.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        var searchGuestSaleReportResultDto = new SearchIssuedNotesReportResultDto()
                        {
                            BillingAccountId = Guid.Parse(reader.GetValue(0).ToString()),
                            RecipientsName = reader.GetValue(1).ToString(),
                            UH = reader.GetValue(2).ToString(),
                            ReservationNumber = reader.GetValue(3).ToString(),
                            AccountName = reader.GetValue(4).ToString(),
                            Type = reader.GetValue(5).ToString(),
                            ExternalNumber = reader.GetValue(6).ToString(),
                            ExternalSeries = reader.GetValue(7).ToString(),
                            Rps = reader.GetValue(8).ToString(),
                            EmissionDate = (DateTime?)(reader.IsDBNull(9) ? null : reader[9]),
                            CancelDate = (DateTime?)(reader.IsDBNull(10) ? null : reader[10]),
                            Status = reader.GetValue(11).ToString(),
                            Deposit = (decimal?)(reader.IsDBNull(12) ? null : reader[12]),
                            Card = (decimal?)(reader.IsDBNull(13) ? null : reader[13]),
                            Invoice = (decimal?)(reader.IsDBNull(14) ? null : reader[14]),
                            Check = (decimal?)(reader.IsDBNull(15) ? null : reader[15]),
                            Cash = (decimal?)(reader.IsDBNull(16) ? null : reader[16]),
                            OthersReceipts = (decimal?)(reader.IsDBNull(17) ? null : reader[17]),
                            Restaurant = Convert.ToDouble(reader.GetValue(18)),
                            Others = Convert.ToDouble(reader.GetValue(19)),
                            Bars = Convert.ToDouble(reader.GetValue(20)),
                            Daily = Convert.ToDouble(reader.GetValue(21)),
                            Events = Convert.ToDouble(reader.GetValue(22)),
                            Banquet = Convert.ToDouble(reader.GetValue(23)),
                            Laundry = Convert.ToDouble(reader.GetValue(24)),
                            Telecommunications = Convert.ToDouble(reader.GetValue(25)),
                            Iss = Convert.ToDouble(reader.GetValue(26)),
                            ServiceRate = Convert.ToDouble(reader.GetValue(27)),
                            Total = Convert.ToDouble(reader.GetValue(28)),
                            Service = Convert.ToDouble(reader.GetValue(29)),
                            Rate = Convert.ToDouble(reader.GetValue(30)),
                            TypeOfPayment = Convert.ToDouble(reader.GetValue(31)),
                            PointOfSale = Convert.ToDouble(reader.GetValue(32)),
                            StatusId = int.Parse(reader.GetValue(33).ToString()),
                            BillingInvoiceTypeId = int.Parse(reader.GetValue(34).ToString()),
                            CurrencySymbol = reader.GetValue(35).ToString()
                        };
                        searchIssuedNotesReportResultDto.Items.Add(searchGuestSaleReportResultDto);
                    }
                }
                reader.Dispose();
            }

            searchIssuedNotesReportResultDto.Items = searchIssuedNotesReportResultDto.Items.OrderBy(x => x.BillingAccountId).ToList();
            return searchIssuedNotesReportResultDto;
        }

        public ListDto<SearchReservationUhReportResultReturnDto> GetRoomsByPropertyId(GetAllSituationRoomsDto request, int propertyId)
        {
            var baseQuery = QueryOfGetRoomsByPropertyId(propertyId);

            if (HaveFilter(request))
            {
                if (request.OnlyVipGuests)
                    baseQuery = baseQuery.Where(x => x.IsVIP);

                if (request.UhSituationId != null)
                    baseQuery = baseQuery.Where(x => x.UhSituationId == request.UhSituationId);

                if (request.HousekeepingStatusId != null)
                    baseQuery = baseQuery.Where(x => x.HousekeepingStatusId == request.HousekeepingStatusId);

                if (request.TypeUHId != null)
                    baseQuery = baseQuery.Where(x => x.TypeUHId == request.TypeUHId);

                if (!string.IsNullOrEmpty(request.ClientName))
                    baseQuery = baseQuery.Where(x => x.Client.ToLower().Contains(request.ClientName.ToLower()));

                if (!string.IsNullOrEmpty(request.GroupName))
                    baseQuery = baseQuery.Where(x => x.Group.ToLower().Contains(request.GroupName.ToLower()));

            }

            var searchReservationResultReturnDto = new ListDto<SearchReservationUhReportResultReturnDto>();

            var searchReservationResultReturnList = new List<SearchReservationUhReportResultReturnDto>();

            foreach (var searchReservationResult in baseQuery)
            {
                var searchReservationResultReturn = new SearchReservationUhReportResultReturnDto()
                {
                    Id = searchReservationResult.Id,
                    UH = searchReservationResult.UH,
                    UhSituation = searchReservationResult.UhSituation,
                    TypeUH = searchReservationResult.TypeUH,
                    HousekeepingStatus = searchReservationResult.HousekeepingStatus,
                    Wing = searchReservationResult.Wing,
                    Floor = searchReservationResult.Floor,
                    AdultsAndChildren = searchReservationResult.AdultsAndChildren,
                    GuestName = searchReservationResult.GuestName,
                    ArrivalDate = searchReservationResult.ArrivalDate,
                    DepartureDate = searchReservationResult.DepartureDate,
                    Company = searchReservationResult.Company,
                    MealPlanTypeId = searchReservationResult.MealPlanTypeId,
                    MealPlan = searchReservationResult.MealPlan,
                    Observation = searchReservationResult.Observation
                };

                searchReservationResultReturnList.AddIfNotContains(searchReservationResultReturn);
            }

            searchReservationResultReturnDto.Items = searchReservationResultReturnList.OrderBy(x => x.UH.Length).ThenBy(x => x.UH).ToList();

            return searchReservationResultReturnDto;

        }

        private IQueryable<ReservationItem> GetReservationItemsConfirmedAndCheckin(DateTime systemDate)
        {
            return (from ri in Context.ReservationItems
                    where
                        ri.ReservationItemStatusId == (int)ReservationStatus.Checkin
                        ||
                        (
                          ri.ReservationItemStatusId < (int)ReservationStatus.Checkin
                          && ri.EstimatedArrivalDate.Date == systemDate.Date
                        )
                    select ri);
        }

        private IQueryable<GuestReservationItem> GetGuestReservationItemsCheckin()
        {
            return (from gri in Context.GuestReservationItems
                    where gri.GuestStatusId == (int)ReservationStatus.Checkin
                    select gri);
        }

        private IQueryable<SearchReservationUhReportResultReturnDto> QueryOfGetRoomsByPropertyId(int propertyId)
        {
            var systemDate = _propertyParameterReadRepository.GetSystemDate(propertyId) ?? DateTime.Now;

            var dbBaseQuery = (from room in this.Context.Rooms

                               join roomReservation in _roomRepository.GetAllBlockingRoomList(propertyId)
                               on room.Id equals roomReservation.Room.Id

                               join reservItem in GetReservationItemsConfirmedAndCheckin(systemDate)
                               on room.Id equals reservItem.RoomId into ri
                               from leftReservItem in ri.DefaultIfEmpty()

                               join receivRoomType in this.Context.RoomTypes
                               on leftReservItem.ReceivedRoomType.Id
                               equals receivRoomType.Id into rt
                               from leftReceivRoomType in rt.DefaultIfEmpty()

                               join housekeeping in this.Context.HousekeepingStatusProperties
                               on room.HousekeepingStatusPropertyId equals housekeeping.Id

                               join reserv in this.Context.Reservations
                               on leftReservItem.ReservationId equals reserv.Id into r
                               from leftReserv in r.DefaultIfEmpty()

                               join guestReserv in GetGuestReservationItemsCheckin()
                               on leftReservItem.Id equals guestReserv.ReservationItemId into gr
                               from leftguestReserv in gr.DefaultIfEmpty()

                               join companyclient in this.Context.CompanyClients
                               on leftReserv.CompanyClientId equals companyclient.Id into cc
                               from leftcompanyclient in cc.DefaultIfEmpty()

                               join propertyGuestType in this.Context.PropertyGuestTypes
                               on leftguestReserv.GuestTypeId equals propertyGuestType.Id into gt
                               from leftPropertyGuestType in gt.DefaultIfEmpty()

                               where room.PropertyId == propertyId

                               select new SearchReservationUhReportResultReturnDto()
                               {
                                   Id = room.Id,
                                   UH = room.RoomNumber,
                                   UhSituationId = leftReservItem.ReservationItemStatusId != (int)ReservationStatus.Checkin &&
                                                 roomReservation.RoomBlocking == null ? (int)RoomAvailabilityEnum.Vague :
                                                 roomReservation.RoomBlocking != null ? (int)RoomAvailabilityEnum.Blocked :
                                                 (int)RoomAvailabilityEnum.Busy,
                                   UhSituation = leftReservItem.ReservationItemStatusId != (int)ReservationStatus.Checkin &&
                                                 roomReservation.RoomBlocking == null ?
                                                 _localizationManager.GetString(AppConsts.LocalizationSourceName,
                                                 (RoomAvailabilityEnum.Vague).ToString()) :
                                                 roomReservation.RoomBlocking != null ?
                                                 _localizationManager.GetString(AppConsts.LocalizationSourceName,
                                                 (RoomAvailabilityEnum.Blocked).ToString()) :
                                                 _localizationManager.GetString(AppConsts.LocalizationSourceName,
                                                 (RoomAvailabilityEnum.Busy).ToString()),
                                   TypeUHId = leftReceivRoomType.Id,
                                   TypeUH = leftReceivRoomType.Abbreviation ?? string.Empty,
                                   HousekeepingStatusId = housekeeping.HousekeepingStatusId,
                                   HousekeepingStatus = _localizationManager.GetString(AppConsts.LocalizationSourceName,
                                                        ((HousekeepingStatusEnum)housekeeping.HousekeepingStatusId).ToString()),
                                   Wing = room.Wing,
                                   Floor = room.Floor,
                                   AdultsAndChildren = leftReservItem == null ||
                                                       leftReservItem.ReservationItemStatusId != (int)ReservationStatus.Checkin ? string.Empty :
                                                       leftguestReserv.IsMain ? $"{(leftReservItem.AdultCount < 10 ? "0" : "")}{leftReservItem.AdultCount}/" +
                                                       $"0{(leftReservItem.ChildCount >= 1 ? 1 : 0)}/0{(leftReservItem.ChildCount >= 2 ? 1 : 0)}/" +
                                                       $"0{(leftReservItem.ChildCount >= 3 ? 1 : 0)}" : "Acomp.",
                                   GuestName = leftReservItem.ReservationItemStatusId != (int)ReservationStatus.Checkin ? string.Empty :
                                               leftguestReserv.GuestName,
                                   GuestNameValidation = leftguestReserv.GuestName,
                                   ArrivalDate = leftReservItem.ReservationItemStatusId != (int)ReservationStatus.Checkin ? null :
                                                 leftReservItem.CheckInDate != null ? leftReservItem.CheckInDate :
                                                 leftReservItem.EstimatedArrivalDate,
                                   DepartureDate = leftReservItem.ReservationItemStatusId != (int)ReservationStatus.Checkin ? null :
                                                   leftReservItem.CheckOutDate != null ? leftReservItem.CheckOutDate :
                                                   leftReservItem.EstimatedDepartureDate,
                                   Company = leftReservItem.ReservationItemStatusId != (int)ReservationStatus.Checkin ? string.Empty :
                                             leftcompanyclient != null ? leftcompanyclient.TradeName : string.Empty,
                                   MealPlanTypeId = leftReservItem.MealPlanTypeId == null ||
                                                    leftReservItem.ReservationItemStatusId != (int)ReservationStatus.Checkin ? null :
                                                    leftReservItem.MealPlanTypeId,
                                   MealPlan = leftReservItem.MealPlanTypeId == null ||
                                              leftReservItem.ReservationItemStatusId != (int)ReservationStatus.Checkin ? string.Empty :
                                              _localizationManager.GetString(AppConsts.LocalizationSourceName,
                                                ((MealPlanTypeEnum)leftReservItem.MealPlanTypeId).ToString()),
                                   Observation = leftReservItem.ReservationItemStatusId != (int)ReservationStatus.Checkin ? string.Empty :
                                                 leftReserv.InternalComments,
                                   ReservationItemStatusId = leftReservItem.ReservationItemStatusId,
                                   Client = leftcompanyclient.ShortName,
                                   Group = leftReserv.GroupName,
                                   IsVIP = leftPropertyGuestType.IsVip
                               });

            dbBaseQuery = dbBaseQuery.Where(x => !(x.ReservationItemStatusId != (int)ReservationStatus.Checkin && !string.IsNullOrEmpty(x.GuestNameValidation)));

            dbBaseQuery = dbBaseQuery.OrderBy(x => x.UH).ThenBy(x => x.AdultsAndChildren);

            return dbBaseQuery;
        }

        private bool HaveFilter(GetAllSituationRoomsDto request)
        {
            return request != null && (request.OnlyVipGuests || request.UhSituationId != null || request.HousekeepingStatusId != null ||
                                       request.TypeUHId != null || !string.IsNullOrEmpty(request.ClientName) ||
                                                                   !string.IsNullOrEmpty(request.GroupName));
        }

        public async Task<ReportUhSituationReturnDto> GetUhSituation(int propertyId)
        {
            var dbBaseQuery = await (from roomStatus in _roomStatusReadRepository.GetAll(new Dto.RoomStatus.GetAllRoomStatusDto(), propertyId).Items.AsQueryable()
                                     where roomStatus.IsActive
                                     select new ReportUhSituationResultDto()
                                     {
                                         RoomAvailability = roomStatus.RoomStatusId
                                     }).ToListDtoAsync(false);
            return new ReportUhSituationReturnDto
            {
                Vague = dbBaseQuery.Items.Count(x => x.RoomAvailability.Equals(RoomAvailabilityEnum.Vague)),
                Busy = dbBaseQuery.Items.Count(x => x.RoomAvailability.Equals(RoomAvailabilityEnum.Busy)),
                Blocked = dbBaseQuery.Items.Count(x => x.RoomAvailability.Equals(RoomAvailabilityEnum.Blocked))
            };
        }

        public async Task<ReportHousekeepingStatusReturnDto> GetHousekeepingStatus(int propertyId)
        {
            var dbBaseQuery = await (from housekeeping in Context.HousekeepingStatusProperties

                                     join room in this.Context.Rooms
                                     on housekeeping.Id equals room.HousekeepingStatusPropertyId into a
                                     from leftroom in a.DefaultIfEmpty()

                                     where housekeeping.PropertyId == propertyId

                                     select new ReportHousekeepingStatusResultDto()
                                     {
                                         HousekeepingStatusId = housekeeping.HousekeepingStatusId,
                                         HousekeepingStatusName = housekeeping.StatusName,
                                         Color = housekeeping.Color,
                                         RoomId = leftroom.Id

                                     }).ToListAsync();

            var reportHouseKeepingStatusReturnDto = new ReportHousekeepingStatusReturnDto
            {
                reportHousekeepingStatusResolveDtos = new List<ReportHousekeepingStatusResolveDto>()
                {
                    new ReportHousekeepingStatusResolveDto()
                    {
                        Id = (int)HousekeepingStatusEnum.Clean,
                        Name =  dbBaseQuery.FirstOrDefault(x => x.HousekeepingStatusId.Equals((int)HousekeepingStatusEnum.Clean))?.HousekeepingStatusName,
                        Total = dbBaseQuery.Count(x => x.HousekeepingStatusId.Equals((int)HousekeepingStatusEnum.Clean) && x.RoomId != null),
                        Color = dbBaseQuery.FirstOrDefault(x => x.HousekeepingStatusId.Equals((int)HousekeepingStatusEnum.Clean))?.Color
                    },
                    new ReportHousekeepingStatusResolveDto()
                    {
                        Id = (int)HousekeepingStatusEnum.Dirty,
                        Name =  dbBaseQuery.FirstOrDefault(x => x.HousekeepingStatusId.Equals((int)HousekeepingStatusEnum.Dirty))?.HousekeepingStatusName,
                        Total = dbBaseQuery.Count(x => x.HousekeepingStatusId.Equals((int)HousekeepingStatusEnum.Dirty) && x.RoomId != null),
                        Color = dbBaseQuery.FirstOrDefault(x => x.HousekeepingStatusId.Equals((int)HousekeepingStatusEnum.Dirty))?.Color
                    },
                    new ReportHousekeepingStatusResolveDto()
                    {
                        Id = (int)HousekeepingStatusEnum.Maintenance,
                        Name =  dbBaseQuery.FirstOrDefault(x => x.HousekeepingStatusId.Equals((int)HousekeepingStatusEnum.Maintenance))?.HousekeepingStatusName,
                        Total = dbBaseQuery.Count(x => x.HousekeepingStatusId.Equals((int)HousekeepingStatusEnum.Maintenance) && x.RoomId != null),
                        Color = dbBaseQuery.FirstOrDefault(x => x.HousekeepingStatusId.Equals((int)HousekeepingStatusEnum.Maintenance))?.Color
                    },
                    new ReportHousekeepingStatusResolveDto()
                    {
                        Id = (int)HousekeepingStatusEnum.Inspection,
                        Name =  dbBaseQuery.FirstOrDefault(x => x.HousekeepingStatusId.Equals((int)HousekeepingStatusEnum.Inspection))?.HousekeepingStatusName,
                        Total = dbBaseQuery.Count(x => x.HousekeepingStatusId.Equals((int)HousekeepingStatusEnum.Inspection) && x.RoomId != null),
                        Color = dbBaseQuery.FirstOrDefault(x => x.HousekeepingStatusId.Equals((int)HousekeepingStatusEnum.Inspection))?.Color
                    },
                    new ReportHousekeepingStatusResolveDto()
                    {
                        Id = (int)HousekeepingStatusEnum.Stowage,
                        Name =  dbBaseQuery.FirstOrDefault(x => x.HousekeepingStatusId.Equals((int)HousekeepingStatusEnum.Stowage))?.HousekeepingStatusName,
                        Total = dbBaseQuery.Count(x => x.HousekeepingStatusId.Equals((int)HousekeepingStatusEnum.Stowage) && x.RoomId != null),
                        Color = dbBaseQuery.FirstOrDefault(x => x.HousekeepingStatusId.Equals((int)HousekeepingStatusEnum.Stowage))?.Color
                    }
                }

            };

            var reportHouseKeepingStatusCreatedByHotelList = new List<ReportHousekeepingStatusResolveDto>();

            foreach (var createdByHotel in dbBaseQuery.Where(x => x.HousekeepingStatusId.Equals((int)HousekeepingStatusEnum.CreatedByHotel)).Distinct())
            {
                reportHouseKeepingStatusCreatedByHotelList.Add(
                     new ReportHousekeepingStatusResolveDto()
                     {
                         Id = createdByHotel.HousekeepingStatusId,
                         Name = createdByHotel.HousekeepingStatusName,
                         Total = dbBaseQuery.Count(x => x.HousekeepingStatusName.Equals(createdByHotel.HousekeepingStatusName) && x.RoomId != null),
                         Color = createdByHotel.Color
                     }
                 );
            }

            reportHouseKeepingStatusReturnDto.reportHousekeepingStatusResolveDtos.AddRange(reportHouseKeepingStatusCreatedByHotelList);

            return reportHouseKeepingStatusReturnDto;
        }

        public async Task<ReportReservationStatusReturnDto> GetReservationStatus(int propertyId, DateTime? systemDate)
        {
            var dbBaseQuery = await (from reserv in this.Context.Reservations
                                     join reservItem in this.Context.ReservationItems
                                     on reserv.Id equals reservItem.Reservation.Id

                                     where reserv.PropertyId == propertyId
                                     && reservItem.EstimatedArrivalDate >= systemDate.GetValueOrDefault().Date
                                     && reservItem.EstimatedArrivalDate < systemDate.GetValueOrDefault().Date.AddDays(1)

                                     select new ReportReservationStatusResultDto()
                                     {
                                         //ReservationStatus = (ReservationStatus)reservItem.ReservationItemStatusId
                                         ReservationStatus = (ReservationStatus)Enum.ToObject(typeof(ReservationStatus), reservItem.ReservationItemStatusId)
                                     }).ToListAsync();

            return new ReportReservationStatusReturnDto()
            {
                ToConfirm = new ReportReservationListDto
                {
                    Id = (int)ReservationStatus.ToConfirm,
                    Name = _localizationManager.GetString(AppConsts.LocalizationSourceName, (ReservationStatus.ToConfirm).ToString()),
                    Total = dbBaseQuery.Count(x => x.ReservationStatus.Equals(ReservationStatus.ToConfirm))
                },
                Confirmed = new ReportReservationListDto
                {
                    Id = (int)ReservationStatus.Confirmed,
                    Name = _localizationManager.GetString(AppConsts.LocalizationSourceName, (ReservationStatus.Confirmed).ToString()),
                    Total = dbBaseQuery.Count(x => x.ReservationStatus.Equals(ReservationStatus.Confirmed))
                },
                Checkin = new ReportReservationListDto
                {
                    Id = (int)ReservationStatus.Checkin,
                    Name = _localizationManager.GetString(AppConsts.LocalizationSourceName, (ReservationStatus.Checkin).ToString()),
                    Total = dbBaseQuery.Count(x => x.ReservationStatus.Equals(ReservationStatus.Checkin))
                },
                Checkout = new ReportReservationListDto
                {
                    Id = (int)ReservationStatus.Checkout,
                    Name = _localizationManager.GetString(AppConsts.LocalizationSourceName, (ReservationStatus.Checkout).ToString()),
                    Total = dbBaseQuery.Count(x => x.ReservationStatus.Equals(ReservationStatus.Checkout))
                },
                Pending = new ReportReservationListDto
                {
                    Id = (int)ReservationStatus.Pending,
                    Name = _localizationManager.GetString(AppConsts.LocalizationSourceName, (ReservationStatus.Pending).ToString()),
                    Total = dbBaseQuery.Count(x => x.ReservationStatus.Equals(ReservationStatus.Pending))
                },
                NoShow = new ReportReservationListDto
                {
                    Id = (int)ReservationStatus.NoShow,
                    Name = _localizationManager.GetString(AppConsts.LocalizationSourceName, (ReservationStatus.NoShow).ToString()),
                    Total = dbBaseQuery.Count(x => x.ReservationStatus.Equals(ReservationStatus.NoShow))
                },
                Canceled = new ReportReservationListDto
                {
                    Id = (int)ReservationStatus.Canceled,
                    Name = _localizationManager.GetString(AppConsts.LocalizationSourceName, (ReservationStatus.Canceled).ToString()),
                    Total = dbBaseQuery.Count(x => x.ReservationStatus.Equals(ReservationStatus.Canceled))
                },
                WaitList = new ReportReservationListDto
                {
                    Id = (int)ReservationStatus.WaitList,
                    Name = _localizationManager.GetString(AppConsts.LocalizationSourceName, (ReservationStatus.WaitList).ToString()),
                    Total = dbBaseQuery.Count(x => x.ReservationStatus.Equals(ReservationStatus.WaitList))
                },
                ReservationProposal = new ReportReservationListDto
                {
                    Id = (int)ReservationStatus.ReservationProposal,
                    Name = _localizationManager.GetString(AppConsts.LocalizationSourceName, (ReservationStatus.ReservationProposal).ToString()),
                    Total = dbBaseQuery.Count(x => x.ReservationStatus.Equals(ReservationStatus.ReservationProposal))
                }
            };
        }

        public async Task<ReportCheckStatusReturnDto> GetCheckStatus(int propertyId, DateTime? systemDate)
        {
            var dbBaseQuery = await (from reserv in this.Context.Reservations
                                     join reservItem in this.Context.ReservationItems
                                     on reserv.Id equals reservItem.Reservation.Id

                                     where reserv.PropertyId == propertyId

                                     select new ReportCheckStatusResultDto()
                                     {
                                         EstimatedArrivalDate = reservItem.EstimatedArrivalDate,
                                         CheckInDate = reservItem.CheckInDate,
                                         EstimatedDepartureDate = reservItem.EstimatedDepartureDate,
                                         CheckOutDate = reservItem.CheckOutDate,
                                         WalkIn = reservItem.WalkIn,
                                         ReservationStatus = (ReservationStatus)Enum.ToObject(typeof(ReservationStatus), reservItem.ReservationItemStatusId)
                                     }).ToListAsync();

            return new ReportCheckStatusReturnDto()
            {
                CheckInReturn = new CheckInReturnDto()
                {
                    Estimated = dbBaseQuery.Count(x => x.EstimatedArrivalDate >= systemDate.GetValueOrDefault().Date &&
                                                       x.EstimatedArrivalDate < systemDate.GetValueOrDefault().Date.AddDays(1) &&
                                                       x.ReservationStatus != ReservationStatus.Canceled),
                    Accomplished = dbBaseQuery.Count(x => x.CheckInDate >= systemDate.GetValueOrDefault().Date &&
                                                     x.CheckInDate < systemDate.GetValueOrDefault().Date.AddDays(1)),
                    DayUse = dbBaseQuery.Count(x => x.CheckInDate >= systemDate.GetValueOrDefault().Date && x.CheckInDate
                                                                   < systemDate.GetValueOrDefault().Date.AddDays(1) &&
                                                    x.CheckOutDate >= systemDate.GetValueOrDefault().Date && x.CheckOutDate
                                                                   < systemDate.GetValueOrDefault().Date.AddDays(1))
                },
                CheckOutReturn = new CheckOutReturnDto()
                {
                    Estimated = dbBaseQuery.Count(x => x.EstimatedDepartureDate >= systemDate.GetValueOrDefault().Date &&
                                                               x.EstimatedDepartureDate < systemDate.GetValueOrDefault().Date.AddDays(1)),
                    Accomplished = dbBaseQuery.Count(x => x.CheckOutDate >= systemDate.GetValueOrDefault().Date && x.CheckOutDate
                                                                     < systemDate.GetValueOrDefault().Date.AddDays(1)),
                    DayUse = dbBaseQuery.Count(x => x.CheckInDate >= systemDate.GetValueOrDefault().Date && x.CheckInDate
                                                                   < systemDate.GetValueOrDefault().Date.AddDays(1) &&
                                                    x.CheckOutDate >= systemDate.GetValueOrDefault().Date && x.CheckOutDate
                                                                   < systemDate.GetValueOrDefault().Date.AddDays(1))
                },
                WalkInReturn = new WalkInReturnDto()
                {
                    Estimated = dbBaseQuery.Count(x => x.CheckInDate >= systemDate.GetValueOrDefault().Date && x.CheckInDate
                                                                    < systemDate.GetValueOrDefault().Date.AddDays(1) && x.WalkIn),
                    Accomplished = dbBaseQuery.Count(x => x.CheckInDate >= systemDate.GetValueOrDefault().Date && x.CheckInDate
                                                                   < systemDate.GetValueOrDefault().Date.AddDays(1) && x.WalkIn),
                    DayUse = dbBaseQuery.Count(x => x.CheckInDate >= systemDate.GetValueOrDefault().Date && x.CheckInDate
                                                                   < systemDate.GetValueOrDefault().Date.AddDays(1) &&
                                                    x.CheckOutDate >= systemDate.GetValueOrDefault().Date && x.CheckOutDate
                                                                    < systemDate.GetValueOrDefault().Date.AddDays(1) && x.WalkIn)
                }
            };
        }

        public ListDto<SearchBordereauReportResultDto> GetBordereau(SearchBordereauReportDto requestDto, int propertyId)
        {
            ListDto<SearchBordereauReportResultDto> searchBordereauReportResultList = new ListDto<SearchBordereauReportResultDto>();

            var contextDatabase = Context.Database;
            var conn = contextDatabase.GetDbConnection();

            if (conn.State == System.Data.ConnectionState.Closed)
                conn.Open();

            var filters = "";
            var filtersSynthesise = "";

            if (requestDto.InitialDate.HasValue)
            {
                filters += $"and Cast(BillingAccountItemDate as date) >= '{requestDto.InitialDate.Value.ToString("yyyy-MM-dd")} 00:00:00.000'";
                filtersSynthesise += $"and Cast(BillingAccountItemDate as date) >= '{requestDto.InitialDate.Value.ToString("yyyy-MM-dd")} 00:00:00.000'";
            }

            if (requestDto.EndDate.HasValue)
            {
                filters += $"and Cast(BillingAccountItemDate as date) <= '{requestDto.EndDate.Value.ToString("yyyy-MM-dd")} 23:59:59.000'";
                filtersSynthesise += $"and Cast(BillingAccountItemDate as date) <= '{requestDto.EndDate.Value.ToString("yyyy-MM-dd")} 23:59:59.000'";
            }

            if (!string.IsNullOrWhiteSpace(requestDto.AccountName))
                filters += $"and BillingItemName like '%{requestDto.AccountName}%'";

            if (requestDto.IssuePayment)
            {
                filters += $"and PaymentTypeId is not null ";
                filtersSynthesise += $"and BI.PaymentTypeId is not null ";
            }

            if (!string.IsNullOrWhiteSpace(requestDto.Name))
            {
                filters += $"and Name like '%{requestDto.Name}%'";
                filtersSynthesise += $"and US.Name like '%{requestDto.Name}%'";
            }

            if (requestDto.WasReversed)
                filters += $"and BillingAccountItemTypeId = 3 ";

            if (requestDto.Synthesise)
            {
                using (var command = conn.CreateCommand())
                {
                    var query = $"SELECT  BI.BillingItemName ," +
                          $"         SUM(BA.Amount) AS Amount, " +
                          $"         C.Symbol AS Currency " +
                          $"      FROM Property PR " +
                          $"INNER JOIN BillingItem BI" +
                          $"    ON PR.PropertyId = BI.PropertyId " +
                          $"INNER JOIN BillingAccountItem BA" +
                          $"    ON (BA.BillingItemId = BI.BillingItemId AND BA.IsDeleted = 0) " +
                          $"INNER JOIN BillingAccount BC" +
                          $"    ON BA.BillingAccountId = BC.BillingAccountId " +
                          $"INNER JOIN Currency C " +
                          $"    ON BA.CurrencyId = C.CurrencyId " +
                          $"LEFT JOIN PaymentType PT" +
                          $"    ON BI.PaymentTypeId = PT.PaymentTypeId " +
                          $"LEFT JOIN Reservation RF" +
                          $"    ON BC.ReservationId = RF.ReservationId " +
                          $"LEFT JOIN ReservationItem RI" +
                          $"    ON BC.ReservationItemId = RI.ReservationItemId " +
                          $"LEFT JOIN GuestReservationItem GI" +
                          $"    ON RI.ReservationItemId = GI.ReservationItemId" +
                          $"    AND GI.GuestReservationItemId = BC.GuestReservationItemId " +
                          $"LEFT JOIN GuestRegistration GR" +
                          $"    ON Gi.GuestRegistrationId = GR.GuestRegistrationId " +
                          $"LEFT JOIN Room UH" +
                          $"    ON RI.RoomId = UH.RoomId " +
                          $"LEFT JOIN [Users] US" +
                          $"   ON BA.CreatorUserId = US.Id " +
                          $"WHERE PR.PropertyId = {propertyId} {filtersSynthesise} " +
                          $"GROUP BY BillingItemName, C.symbol " +
                          $"ORDER BY BillingItemName";


                    if (contextDatabase.CurrentTransaction != null)
                        command.Transaction = contextDatabase.CurrentTransaction.GetDbTransaction();

                    command.CommandText = query;

                    var reader = command.ExecuteReader();

                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            var searchBordereauReportResultDto = new SearchBordereauReportResultDto()
                            {
                                BillingItemName = reader.GetValue(0).ToString(),
                                Amount = (decimal?)(reader.IsDBNull(1) ? null : reader.GetValue(1)),
                                CurrencySymbol = reader.GetValue(2).ToString(),
                            };
                            searchBordereauReportResultList.Items.Add(searchBordereauReportResultDto);
                        }
                    }
                    reader.Dispose();
                }
            }
            else
            {
                using (var command = conn.CreateCommand())
                {
                    var query = $"SELECT  A.* from (SELECT PR.TenantId," +
                                $"        PR.PropertyId," +
                                $"        COALESCE(BC.BILLINGACCOUNTNAME, 'GERAL') AS NOMECONTA," +
                                $"        BC.BillingAccountName," +
                                $"        UH.RoomNUMBER," +
                                $"        RI.ReservationItemCode," +
                                $"        GR.FullName," +
                                $"        GI.GuestName," +
                                $"        BI.BillingItemName ," +
                                $"        BA.Amount," +
                                $"        BA.OriginalAmount," +
                                $"        BA.PercentualAmount," +
                                $"        BA.CheckNumber," +
                                $"        BA.NSU," +
                                $"        BA.BillingAccountItemDate," +
                                $"        US.NAME,PT.*,BA.BillingAccountItemTypeId," +
                                $"        CC.tradename, " +
                                $"        ParentReason.ReasonName, " +
                                $"        COALESCE(Parent.BillingAccountItemObservation, BA.BillingAccountItemObservation) AS BillingAccountItemObservation," +
                                $"        BIC.CategoryName, " +
                                $"        BIN.externalrps, " +
                                $"        C.Symbol " +
                                $"      FROM Property PR " +
                                $"INNER JOIN BillingItem BI" +
                                $"    ON PR.PropertyId = BI.PropertyId " +
                                $"INNER JOIN BillingAccountItem BA" +
                                $"    ON (BA.BillingItemId = BI.BillingItemId AND BA.IsDeleted = 0) " +
                                $"INNER JOIN BillingAccount BC" +
                                $"    ON BA.BillingAccountId = BC.BillingAccountId " +
                                $"INNER JOIN Currency C " +
                                $"    ON BA.CurrencyId = C.CurrencyId " +
                                $"LEFT JOIN Reason R" +
                                $"    ON BA.BillingAccountItemReasonId = R.ReasonId " +
                                $"LEFT JOIN PaymentType PT" +
                                $"    ON BI.PaymentTypeId = PT.PaymentTypeId " +
                                $"LEFT JOIN Reservation RF" +
                                $"    ON BC.ReservationId = RF.ReservationId " +
                                $"LEFT JOIN ReservationItem RI" +
                                $"    ON BC.ReservationItemId = RI.ReservationItemId " +
                                $"LEFT JOIN GuestReservationItem GI" +
                                $"    ON RI.ReservationItemId = GI.ReservationItemId" +
                                $"    AND GI.GuestReservationItemId = BC.GuestReservationItemId " +
                                $"LEFT JOIN GuestRegistration GR" +
                                $"    ON Gi.GuestRegistrationId = GR.GuestRegistrationId " +
                                $"LEFT JOIN Room UH" +
                                $"    ON RI.RoomId = UH.RoomId " +
                                $"LEFT JOIN [Users] US" +
                                $"   ON BA.CreatorUserId = US.Id " +
                                $"LEFT JOIN CompanyClient CC " +
                                $"   ON BI.AcquirerId = CC.CompanyClientId " +
                                $"LEFT JOIN BillingItemCategory BIC " +
                                $"   ON BI.BillingItemCategoryId = BIC.BillingItemCategoryId " +
                                $"LEFT JOIN BillingItemCategory BIC2 " +
                                $"   ON BIC.StandardCategoryId = BIC2.BillingItemCategoryId " +
                                $"LEFT JOIN BillingAccountItem Parent " +
                                $"   ON PARENT.BillingAccountItemId = BA.BillingAccountItemParentId " +
                                $"LEFT JOIN Reason ParentReason " +
                                $"   ON Parent.BillingAccountItemReasonId = ParentReason.ReasonId " +
                                $"LEFT JOIN billinginvoice BIN " +
                                $"   ON BIN.billinginvoiceid = BA.billinginvoiceid" +
                                $") A " +
                                $"WHERE PropertyId = {propertyId} {filters} " +
                                $"ORDER BY CASE WHEN categoryname is null THEN 1  ELSE 0 END, CategoryName, BillingItemName, " +
                                $"LEN(RoomNumber), RoomNumber, BillingAccountItemDate";


                    if (contextDatabase.CurrentTransaction != null)
                        command.Transaction = contextDatabase.CurrentTransaction.GetDbTransaction();

                    command.CommandText = query;

                    var reader = command.ExecuteReader();

                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            var searchBordereauReportResultDto = new SearchBordereauReportResultDto()
                            {
                                TenantId = new Guid(reader.GetValue(0).ToString()),
                                ProperyId = Convert.ToInt32(reader.GetValue(1)),
                                AccountName = reader.GetValue(2).ToString(),
                                BillingAccountName = reader.GetValue(3).ToString(),
                                UH = reader.GetValue(4).ToString(),
                                ReservationItemCode = reader.GetValue(5).ToString(),
                                FullName = reader.GetValue(6).ToString(),
                                GuestName = reader.GetValue(7).ToString(),
                                BillingItemName = reader.GetValue(8).ToString(),
                                Amount = (decimal?)(reader.IsDBNull(9) ? null : reader.GetValue(9)),                               
                                PercentualAmount = (decimal?)(reader.IsDBNull(11) ? null : reader.GetValue(11)),
                                CheckNumber = reader.GetValue(12).ToString(),
                                NSU = reader.GetValue(13).ToString(),
                                BillingAccountItemDate = Convert.ToDateTime(reader.GetValue(14)),
                                Name = reader.GetValue(15).ToString(),
                                PaymentTypeId = (int?)(reader.IsDBNull(16) ? null : reader.GetValue(16)),
                                PaymentTypeName = string.IsNullOrEmpty(reader.GetValue(16).ToString()) ? "" :
                                                                       _localizationManager.GetString(AppConsts.LocalizationSourceName,
                                                                      ((PaymentTypeEnum)reader.GetValue(16)).ToString()),
                                BillingAccountItemTypeId = int.Parse(reader.GetValue(18).ToString()),
                                AcquirerName = reader.GetValue(19).ToString(),
                                ReasonName = reader.GetValue(20).ToString(),
                                Observation = reader.GetValue(21).ToString(),
                                CategoryName = reader.GetValue(22).ToString(),
                                Rps = reader.GetValue(23).ToString(),
                                CurrencySymbol = reader.GetValue(24).ToString()
                            };
                            searchBordereauReportResultList.Items.Add(searchBordereauReportResultDto);
                        }
                    }
                    reader.Dispose();
                }
            }

            return searchBordereauReportResultList;
        }

        public IListDto<ReportHousekeepingStatusDisagreementReturnDto> GetAllHousekeepingStatusDisagreement(SearchReportHousekeepingStatusDisagreementDto request, int propertyId, DateTime? systemDate)
        {
            var nowDateProperty = DateTime.UtcNow.ToZonedDateTimeLoggedUser();

            var housekeepingStatusIdDirty = Context.HousekeepingStatuss.Where(x => x.Id == (int)HousekeepingStatusEnum.Dirty).FirstOrDefault();

            //Filtrar o reservationItem apenas pela lista abaixo
            var statusList = new List<int>()
            {
                (int)ReservationStatus.Checkin,
            };

            //Filtro de data de discrepância
            var date = request.Date.HasValue ? request.Date : systemDate;

            var dbBaseQuery = (from roomReservationItemJoinMap in _roomRepository.GetAllOccupiedRoomList()

                               join roomtype in Context.RoomTypes on roomReservationItemJoinMap.Room.RoomTypeId equals roomtype.Id

                               join housekeeping in Context.HousekeepingStatusProperties
                               on roomReservationItemJoinMap.Room.HousekeepingStatusPropertyId equals housekeeping.Id into h
                               from housekeeping in h.DefaultIfEmpty()

                               join roomlayout in Context.RoomLayouts on (roomReservationItemJoinMap.ReservationItem != null ?
                                                  roomReservationItemJoinMap.ReservationItem.RoomLayoutId : Guid.Empty)
                                                  equals roomlayout.Id into b
                               from roomlayout in b.DefaultIfEmpty()

                               join HousekeepingRoomDisagreementJoinMap in HousekeepingRoomDisagreement(date, propertyId)
                               on roomReservationItemJoinMap.Room.Id equals HousekeepingRoomDisagreementJoinMap.RoomId into c
                               from leftHousekeepingRoomDisagreement in c.DefaultIfEmpty()

                               join roomStatus in _roomStatusReadRepository.GetAll(new Dto.RoomStatus.GetAllRoomStatusDto(), propertyId).Items.AsQueryable()
                               on roomReservationItemJoinMap.Room.Id equals roomStatus.RoomId into roomStatus
                               from lefRoomStatus in roomStatus.DefaultIfEmpty()

                               where roomReservationItemJoinMap.Room.PropertyId == propertyId && roomReservationItemJoinMap.Room.IsActive

                               select new ReportHousekeepingStatusDisagreementReturnDto()
                               {
                                   Id = roomReservationItemJoinMap.Room.Id,
                                   HousekeepingStatusId = housekeeping != null ? housekeeping.HousekeepingStatusId : housekeepingStatusIdDirty.Id,
                                   HousekeepingStatusName = housekeeping != null ? housekeeping.StatusName : housekeepingStatusIdDirty.StatusName,
                                   Color = housekeeping != null ? housekeeping.Color : housekeepingStatusIdDirty.DefaultColor,
                                   RoomId = roomReservationItemJoinMap.Room.Id,
                                   RoomNumber = roomReservationItemJoinMap.Room.RoomNumber,
                                   RoomTypeId = roomtype.Id,
                                   StatusRoomId = (int)lefRoomStatus.RoomStatusId,
                                   StatusRoom = lefRoomStatus.RoomStatusName,
                                   RoomTypeName = roomtype.Abbreviation,
                                   CheckInDate = roomReservationItemJoinMap.ReservationItem == null ? (DateTime?)null :
                                                (roomReservationItemJoinMap.ReservationItem.CheckInDate ??
                                                 roomReservationItemJoinMap.ReservationItem.EstimatedArrivalDate),
                                   CheckOutDate = roomReservationItemJoinMap.ReservationItem == null ? (DateTime?)null :
                                                 roomReservationItemJoinMap.ReservationItem.CheckOutDate ??
                                                 roomReservationItemJoinMap.ReservationItem.EstimatedDepartureDate,
                                   TotalDays = Math.Round((Convert.ToDateTime(roomReservationItemJoinMap.ReservationItem == null ? (DateTime?)null :
                                                 roomReservationItemJoinMap.ReservationItem.CheckOutDate ??
                                                 roomReservationItemJoinMap.ReservationItem.EstimatedDepartureDate) -
                                                Convert.ToDateTime(roomReservationItemJoinMap.ReservationItem == null ? (DateTime?)null :
                                                (roomReservationItemJoinMap.ReservationItem.CheckInDate ??
                                                 roomReservationItemJoinMap.ReservationItem.EstimatedArrivalDate))).TotalDays, 0),
                                   AdultCount = roomReservationItemJoinMap.ReservationItem != null ?
                                                roomReservationItemJoinMap.ReservationItem.AdultCount : 0,
                                   ChildCount = roomReservationItemJoinMap.ReservationItem != null ?
                                                roomReservationItemJoinMap.ReservationItem.ChildCount : 0,
                                   QuantityDouble = roomlayout != null ? roomlayout.QuantityDouble : 0,
                                   QuantitySingle = roomlayout != null ? roomlayout.QuantitySingle : 0,
                                   ExtraBadCount = roomReservationItemJoinMap.ReservationItem != null ?
                                                   roomReservationItemJoinMap.ReservationItem.ExtraBedCount : 0,
                                   ExtraCribCount = roomReservationItemJoinMap.ReservationItem != null ?
                                                    roomReservationItemJoinMap.ReservationItem.ExtraCribCount : 0,
                                   ReservationItemStatusId = roomReservationItemJoinMap.ReservationItem != null ?
                                                             roomReservationItemJoinMap.ReservationItem.ReservationItemStatusId : 0,
                                   HousekeepingStatusPropertyId = housekeeping != null ? housekeeping.Id : Guid.Empty,
                                   HousekeepingStatusLastModificationTime = roomReservationItemJoinMap.Room.HousekeepingStatusLastModificationTime ??
                                                                            DateTime.MinValue.Date,
                                   Wing = roomReservationItemJoinMap.Room.Wing,
                                   Floor = roomReservationItemJoinMap.Room.Floor,
                                   Building = roomReservationItemJoinMap.Room.Building,
                                   HousekeepingRoomDisagreementId = leftHousekeepingRoomDisagreement.HousekeepingRoomDisagreementId,
                                   HousekeepingRoomId = leftHousekeepingRoomDisagreement.HousekeepingRoomId,
                                   HousekeepingRoomDisagreementStatusId = leftHousekeepingRoomDisagreement.HousekeepingRoomDisagreementStatusId,
                                   HousekeepingRoomDisagreementStatus = leftHousekeepingRoomDisagreement.HousekeepingRoomDisagreementStatus,
                                   DisagreementAdultCount = leftHousekeepingRoomDisagreement.AdultCount,
                                   BagCount = leftHousekeepingRoomDisagreement.BagCount,
                                   Bag = leftHousekeepingRoomDisagreement.Bag,
                                   Observation = leftHousekeepingRoomDisagreement.Observation,
                                   LoggedUserUid = leftHousekeepingRoomDisagreement.LoggedUserUid,
                                   DisagreementRoomId = leftHousekeepingRoomDisagreement.RoomId,
                                   HousekeepingRoomInspectionId = leftHousekeepingRoomDisagreement.HousekeepingRoomInspectionId,
                                   HousekeepingRoomInspection = leftHousekeepingRoomDisagreement.HousekeepingRoomInspection
                               });

            if (!string.IsNullOrEmpty(request.SearchData))
            {
                dbBaseQuery = dbBaseQuery.Where(x => x.RoomNumber.Contains(request.SearchData) ||
                                                     x.HousekeepingStatusName.Contains(request.SearchData) ||
                                                     x.StatusRoom.Contains(request.SearchData) ||
                                                     x.Wing.Contains(request.SearchData) ||
                                                     x.Floor.Contains(request.SearchData) ||
                                                     x.Building.Contains(request.SearchData) ||
                                                     x.RoomTypeName.Contains(request.SearchData));
            }

            if (request.HousekeepingPropertyStatusId.HasValue)
            {
                dbBaseQuery = dbBaseQuery.Where(x => x.HousekeepingStatusId == request.HousekeepingPropertyStatusId.Value);
            }

            if (request.RoomTypeId.HasValue)
            {
                dbBaseQuery = dbBaseQuery.Where(x => x.RoomTypeId == request.RoomTypeId.Value);
            }

            if (request.StatusRoomId.HasValue)
            {
                dbBaseQuery = dbBaseQuery.Where(x => x.StatusRoomId == request.StatusRoomId.Value);
            }

            var totalItemsDto = dbBaseQuery.Count();

            var resultDto = dbBaseQuery.OrderBy(x => x.RoomNumber.Length).ThenBy(x => x.RoomNumber).ToList();

            var result =
                new ListDto<ReportHousekeepingStatusDisagreementReturnDto>
                {
                    Items = resultDto,
                    HasNext = SearchReasonHasNext(
                            request,
                            totalItemsDto,
                            resultDto),
                };

            return result;
        }

        private string GetStatusName(int statusId)
        {
            if ((int)ReservationStatus.Checkin == statusId) return _localizationManager.GetString(AppConsts.LocalizationSourceName, RoomAvailabilityEnum.Busy.ToString());
            else return _localizationManager.GetString(AppConsts.LocalizationSourceName, RoomAvailabilityEnum.Vague.ToString());
        }

        private string GetStatus(int statusId)
        {
            if ((int)ReservationStatus.Checkin == statusId) return RoomAvailabilityEnum.Busy.ToString();
            else return RoomAvailabilityEnum.Vague.ToString();
        }

        private bool SearchReasonHasNext(SearchReportHousekeepingStatusDisagreementDto request, int totalItemsDto, List<ReportHousekeepingStatusDisagreementReturnDto> resultDto)
        {
            return totalItemsDto > ((request.Page - 1) * request.PageSize) + resultDto.Count();
        }

        private IQueryable<HousekeepingRoomDisagreementReturnDto> HousekeepingRoomDisagreement(DateTime? date, int propertyId)
        {
            var housekeepingRoomDisagreementReturnDtos = new List<HousekeepingRoomDisagreementReturnDto>();

            var contextDatabase = Context.Database;
            var conn = contextDatabase.GetDbConnection();

            if (conn.State == System.Data.ConnectionState.Closed)
                conn.Open();

            using (var command = conn.CreateCommand())
            {
                var query = $"select * from HousekeepingRoomDisagreement b " +
                      $"where PropertyId = {propertyId} " +
                      $"and CreationTime >= (SELECT CAST(CAST('{date.Value.Year}' AS varchar) + '-' + CAST('{date.Value.Month}' AS varchar) + '-' + CAST('{date.Value.Day}' AS varchar) AS DATETIME)) " +
                      $"and CreationTime < DATEADD(DAY,1,(SELECT CAST(CAST('{date.Value.Year}' AS varchar) + '-' + CAST('{date.Value.Month}' AS varchar) + '-' + CAST('{date.Value.Day}' AS varchar) AS DATETIME))) ";

                if (contextDatabase.CurrentTransaction != null)
                    command.Transaction = contextDatabase.CurrentTransaction.GetDbTransaction();

                command.CommandText = query;

                var reader = command.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        var housekeepingRoomDisagreementReturnDto = new HousekeepingRoomDisagreementReturnDto()
                        {
                            HousekeepingRoomDisagreementId = (Guid?)(reader.IsDBNull(reader.GetOrdinal("HousekeepingRoomDisagreementId")) ? null :
                                                                     reader.GetValue(reader.GetOrdinal("HousekeepingRoomDisagreementId"))),
                            HousekeepingRoomId = (Guid?)(reader.IsDBNull(reader.GetOrdinal("HousekeepingRoomId")) ? null :
                                            reader.GetValue(reader.GetOrdinal("HousekeepingRoomId"))),
                            RoomId = (int?)(reader.IsDBNull(reader.GetOrdinal("RoomId")) ? null :
                                            reader.GetValue(reader.GetOrdinal("RoomId"))),
                            CreationTime = (DateTime?)(reader.IsDBNull(reader.GetOrdinal("CreationTime")) ? null :
                                                       reader.GetValue(reader.GetOrdinal("CreationTime"))),
                            HousekeepingRoomDisagreementStatusId = (int?)(reader.IsDBNull(reader.GetOrdinal("HousekeepingRoomDisagreementStatusId"))
                                                                ? null : reader.GetValue(reader.GetOrdinal("HousekeepingRoomDisagreementStatusId"))),
                            HousekeepingRoomDisagreementStatus = reader.IsDBNull(reader.GetOrdinal("HousekeepingRoomDisagreementStatusId")) ? null :
                                                                 _localizationManager.GetString(AppConsts.LocalizationSourceName,
                                                                 ((HousekeepingRoomDisagreementEnum)reader.GetValue(
                                                                 reader.GetOrdinal("HousekeepingRoomDisagreementStatusId"))).ToString()),
                            AdultCount = (int?)(reader.IsDBNull(reader.GetOrdinal("AdultCount")) ? null :
                                            reader.GetValue(reader.GetOrdinal("AdultCount"))),
                            BagCount = (int?)(reader.IsDBNull(reader.GetOrdinal("BagCount")) ? null :
                                            reader.GetValue(reader.GetOrdinal("BagCount"))),
                            Bag = reader.IsDBNull(reader.GetOrdinal("BagCount")) ? null :
                                  _localizationManager.GetString(AppConsts.LocalizationSourceName,
                                  ((HousekeepingRoomDisagreementBagEnum)reader.GetValue(reader.GetOrdinal("BagCount"))).ToString()),
                            Observation = reader.GetValue(reader.GetOrdinal("Observation")).ToString(),
                            LoggedUserUid = (Guid?)(reader.IsDBNull(reader.GetOrdinal("OwnerId")) ? null :
                                              reader.GetValue(reader.GetOrdinal("OwnerId"))),
                            HousekeepingRoomInspectionId = (int?)(reader.IsDBNull(reader.GetOrdinal("HousekeepingRoomInspectionId")) ? null :
                                            reader.GetValue(reader.GetOrdinal("HousekeepingRoomInspectionId"))),
                            HousekeepingRoomInspection = reader.IsDBNull(reader.GetOrdinal("HousekeepingRoomInspectionId")) ? null :
                                  _localizationManager.GetString(AppConsts.LocalizationSourceName,
                                  ((HousekeepingRoomInspectionEnum)reader.GetValue(reader.GetOrdinal("HousekeepingRoomInspectionId"))).ToString())
                        };
                        housekeepingRoomDisagreementReturnDtos.Add(housekeepingRoomDisagreementReturnDto);
                    }
                }

                reader.Dispose();

            }

            return housekeepingRoomDisagreementReturnDtos.AsQueryable();
        }

        public ListDto<ReportReservationsMadeReturnDto> GetReservationsMade(SearchReservationsMadeReportDto requestDto, int propertyId)
        {
            ListDto<ReportReservationsMadeReturnDto> reportReservationsMadeReturnDtoList = new ListDto<ReportReservationsMadeReturnDto>();

            var contextDatabase = Context.Database;
            var conn = contextDatabase.GetDbConnection();

            if (conn.State == System.Data.ConnectionState.Closed)
                conn.Open();

            #region ReservationsMadeFilters
            var filters = new List<int>();

            var statusid = "";

            if (requestDto.ToConfirm)
                filters.Add((int)ReservationStatus.ToConfirm);

            if (requestDto.Confirmed)
                filters.Add((int)ReservationStatus.Confirmed);

            if (requestDto.Checkin)
                filters.Add((int)ReservationStatus.Checkin);

            if (requestDto.Checkout)
                filters.Add((int)ReservationStatus.Checkout);

            if (requestDto.Pending)
                filters.Add((int)ReservationStatus.Pending);

            if (requestDto.NoShow)
                filters.Add((int)ReservationStatus.NoShow);

            if (requestDto.Canceled)
                filters.Add((int)ReservationStatus.Canceled);

            if (requestDto.WaitList)
                filters.Add((int)ReservationStatus.WaitList);

            if (requestDto.ReservationProposal)
                filters.Add((int)ReservationStatus.ReservationProposal);

            if (filters.Count > 0)
                statusid = $"and statusid in ({ filters.JoinAsString(",")})";
            #endregion

            #region ReservationsMadeDateFilters

            var filterDate = "";
          
            if (requestDto.PassingBy)
            {
                filterDate += $"and (Cast(COALESCE(ri.checkindate, ri.estimatedarrivaldate) AS DATE) <= '{requestDto.EndDate.Value.ToString("yyyy-MM-dd")}' " +
                              $"and Cast(COALESCE(ri.checkoutdate, ri.estimateddeparturedate) AS DATE) > '{requestDto.InitialDate.Value.ToString("yyyy-MM-dd")}') ";
            }
            else
            {
                filterDate += $"and Cast(RS.CreationTime AS DATE) >= '{requestDto.InitialDate.Value.ToString("yyyy-MM-dd")}' " +
                              $"and Cast(RS.CreationTime AS DATE) <= '{requestDto.EndDate.Value.ToString("yyyy-MM-dd")}' ";
            }                    

            #endregion

            using (var command = conn.CreateCommand())
            {
                var query = $"SELECT PR.PropertyId,PR.Name,RI.ReservationItemCode,RS.ContactName,RS.ContactEmail,RS.ContactPhone," +
                            $"RS.InternalComments,RS.ExternalComments,RS.PartnerComments,RS.GroupName,RS.CreationTime,RS.LastModificationTime," +
                            $"RS.CreatorUserId,RS.LastModifierUserId,RI.EstimatedArrivalDate,RI.EstimatedDepartureDate," +
                            $"RI.Checkindate,RI.CheckInHour,RI.CheckoutDate,RI.CheckoutHour,RI.RequestedRoomTypeId," +
                            $"RI.ReceivedRoomTypeId,RI.RoomId," +
                            $"CAST(RI.AdultCount AS VARCHAR(2)) + '/' + CASE RI.ChildCount WHEN 0 THEN '0/0/0' WHEN 1 THEN '1/0/0' WHEN 2 THEN '1/1/0' WHEN 3 THEN '1/1/1' END AS AdultsAndChildren," +
                            $"RI.RoomLayoutId,RI.ExtraBedCount," +
                            $"RI.ExtraCribCount,CC.CompanyClientId,CC.CompanyId,CC.PersonId,CC.ShortName,CC.TradeName,RT.Name," +
                            $"RT.Abbreviation,ST.StatusCategoryId,ST.Name AS StatusName,ST.StatusId,GI.GuestName,GI.GuestEmail,GI.GuestDocument," +
                            $"GI.GuestTypeId,US.Name AS UserName,R.RoomNumber AS UH,MPT.MealPlanTypeCode AS MealPlan, PR.PROPERTYID " +
                            $"FROM Property PR INNER JOIN Reservation RS ON PR.PropertyId=RS.PropertyId " +
                            $"INNER JOIN ReservationItem RI ON RS.ReservationId=RI.ReservationId " +
                            $"LEFT JOIN Room R ON R.RoomId = RI.RoomId LEFT JOIN CompanyClient CC " +
                            $"ON RS.CompanyClientId=CC.CompanyClientId INNER JOIN RoomType RT " +
                            $"ON RI.ReceivedRoomTypeId=RT.RoomTypeId INNER JOIN [Status] ST " +
                            $"ON RI.ReservationItemStatusId=ST.StatusId LEFT JOIN GuestReservationItem GI " +
                            $"ON RI.ReservationItemId=GI.ReservationItemId AND GI.isDeleted=0 LEFT JOIN Guest GU " +
                            $"ON GI.GuestId=GU.GuestId LEFT JOIN [Users] US ON RS.CreatorUserId=US.Id " +
                            $"LEFT JOIN MealPlanType MPT ON MPT.MealPlanTypeId = RI.MealPlanTypeId " +
                            $"WHERE PR.PROPERTYID = {propertyId} {filterDate} {statusid} ";



                if (contextDatabase.CurrentTransaction != null)
                    command.Transaction = contextDatabase.CurrentTransaction.GetDbTransaction();

                command.CommandText = query;

                var reader = command.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        var reportReservationsMadeReturnDto = new ReportReservationsMadeReturnDto()
                        {
                            GuestName = reader.GetValue(reader.GetOrdinal("GuestName")).ToString(),
                            GuestTypeId = Convert.ToInt32(reader.GetValue(reader.GetOrdinal("GuestTypeId"))),
                            Uh = reader.GetValue(reader.GetOrdinal("Uh")).ToString(),
                            Abbreviation = reader.GetValue(reader.GetOrdinal("Abbreviation")).ToString(),
                            CheckinDate = reader.IsDBNull(reader.GetOrdinal("CheckinDate")) ?
                                          (DateTime?)reader.GetValue(reader.GetOrdinal("EstimatedArrivalDate")) :
                                          (DateTime?)reader.GetValue(reader.GetOrdinal("CheckinDate")),
                            CheckoutDate = reader.IsDBNull(reader.GetOrdinal("CheckoutDate")) ?
                                           (DateTime?)reader.GetValue(reader.GetOrdinal("EstimatedDepartureDate")) :
                                           (DateTime?)reader.GetValue(reader.GetOrdinal("CheckoutDate")),
                            AdultsAndChildren = reader.GetValue(reader.GetOrdinal("AdultsAndChildren")).ToString(),
                            ReservationCode = reader.GetValue(reader.GetOrdinal("ReservationItemCode")).ToString(),
                            StatusName = reader.GetValue(reader.GetOrdinal("StatusName")).ToString(),
                            MealPlan = reader.GetValue(reader.GetOrdinal("MealPlan")).ToString(),
                            TradeName = reader.GetValue(reader.GetOrdinal("TradeName")).ToString(),
                            UserName = reader.GetValue(reader.GetOrdinal("UserName")).ToString(),
                            ReservationStatusId = int.Parse(reader.GetValue(reader.GetOrdinal("StatusId")).ToString())
                        };
                        reportReservationsMadeReturnDtoList.Items.Add(reportReservationsMadeReturnDto);
                    }

                }
                reader.Dispose();
            }

            return reportReservationsMadeReturnDtoList;
        }

        public ListDto<ReservationsInAdvanceReturnDto> GetAllReservationsInAdvance(SearchReservationsInAdvanceDto requestDto, int propertyId)
        {
            var contextDatabase = Context.Database;
            var conn = contextDatabase.GetDbConnection();

            if (conn.State == System.Data.ConnectionState.Closed)
                conn.Open();

            ListDto<ReservationsInAdvanceReturnDto> reservationWithDepositsReturnDto = new ListDto<ReservationsInAdvanceReturnDto>();

            #region Filters

            var filterDate = "";

            if (requestDto.InitialDate.HasValue)
                filterDate += $"and Cast(COALESCE(RI.checkindate,RI.estimatedarrivaldate) as date) >= '{requestDto.InitialDate.Value.ToString("yyyy-MM-dd")} 00:00:00.000'";

            if (requestDto.EndDate.HasValue)
                filterDate += $"and Cast(COALESCE(RI.checkindate,RI.estimatedarrivaldate) as date) <= '{requestDto.EndDate.Value.ToString("yyyy-MM-dd")} 23:59:59.000'";

            #endregion

            using (var command = conn.CreateCommand())
            {
                var query = $"SELECT RS.reservationcode, " +
                            $"		  R.roomnumber AS UH,  " +
                            $"	     RT.abbreviation," +
                            $"       PT.paymentTypeId," +
                            $"	     SUM(BAI.Amount) AS Amount," +
                            $"	     BA.billingAccountName," +
                            $"	     GI.guestname,        " +
                            $"	     ST.NAME AS StatusName," +
                            $"       COALESCE(RI.checkindate,RI.estimatedarrivaldate) AS arrivalDate," +
                            $"	     COALESCE(RI.checkoutdate,RI.estimateddeparturedate) AS departureDate, " +
                            $"       BillingAccountItemDate, " +
                            $"       C.Symbol As CurrencySymbol " +
                            $"FROM   property PR " +
                            $"       JOIN reservation RS " +
                            $"              ON PR.propertyid = RS.propertyid " +
                            $"       JOIN reservationitem RI " +
                            $"              ON RS.reservationid = RI.reservationid " +
                            $"       LEFT JOIN room R " +
                            $"              ON R.roomid = RI.roomid        " +
                            $"       JOIN roomtype RT " +
                            $"              ON RI.receivedroomtypeid = RT.roomtypeid " +
                            $"       JOIN [status] ST " +
                            $"              ON RI.reservationitemstatusid = ST.statusid " +
                            $"       JOIN guestreservationitem GI " +
                            $"              ON RI.reservationitemid = GI.reservationitemid       " +
                            $"	     JOIN billingAccount BA" +
                            $"	  		    ON BA.guestreservationitemId = GI.guestreservationitemId" +
                            $"	     JOIN billingAccountItem BAI" +
                            $"			    ON BA.billingaccountId = BAI.billingaccountId" +
                            $"              AND BillingAccountItemTypeId != 3" +
                            $"       JOIN Currency C " +
                            $"              ON BAI.CurrencyId = C.CurrencyId " +
                            $"	     JOIN billingItem BI" +
                            $"			    ON BI.billingItemId = BAI.billingItemId " +
                            $"       JOIN paymentType PT" +
                            $"              ON PT.paymentTypeId = BI.paymentTypeId " +                         
                            $"WHERE    PR.propertyid = {propertyId} " +
                            $"         AND cast(BAI.billingAccountItemDate as date) < cast(COALESCE(RI.checkindate,RI.estimatedarrivaldate) as date) " +
                            $"{filterDate} " +
                            $"GROUP BY RS.reservationcode, " +
                            $"		    R.roomnumber,     " +
                            $"		   RT.abbreviation,	" +
                            $"         PT.paymentTypeId," +
                            $"		   BA.billingAccountName," +
                            $"		   GI.guestname,        " +
                            $"		   ST.NAME,         " +
                            $"		   RI.estimatedarrivaldate, " +
                            $"		   RI.estimateddeparturedate," +
                            $"		   RI.checkindate,       " +
                            $"		   RI.checkoutdate," +
                            $"         BillingAccountItemDate," +
                            $"         C.symbol";

                if (contextDatabase.CurrentTransaction != null)
                    command.Transaction = contextDatabase.CurrentTransaction.GetDbTransaction();

                command.CommandText = query;

                var reader = command.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        var reservationWithDepositsDto = new ReservationsInAdvanceReturnDto()
                        {
                            GuestName = reader.GetValue(reader.GetOrdinal("GuestName")).ToString(),                           
                            Uh = reader.GetValue(reader.GetOrdinal("Uh")).ToString(),
                            Abbreviation = reader.GetValue(reader.GetOrdinal("Abbreviation")).ToString(),
                            ArrivalDate = DateTime.Parse(reader.GetValue(reader.GetOrdinal("ArrivalDate")).ToString()),
                            DepartureDate = DateTime.Parse(reader.GetValue(reader.GetOrdinal("DepartureDate")).ToString()),
                            ReservationCode = reader.GetValue(reader.GetOrdinal("ReservationCode")).ToString(),
                            StatusName = reader.GetValue(reader.GetOrdinal("StatusName")).ToString(),
                            BillingAccountName = reader.GetValue(reader.GetOrdinal("BillingAccountName")).ToString(),
                            Amount = decimal.Parse(reader.GetValue(reader.GetOrdinal("Amount")).ToString()),
                            PaymentTypeName = _localizationManager.GetString(AppConsts.LocalizationSourceName, ((PaymentTypeEnum)int.Parse(reader.GetValue(reader.GetOrdinal("paymentTypeId")).ToString())).ToString()),
                            BillingAccountItemDate = DateTime.Parse(reader.GetValue(reader.GetOrdinal("BillingAccountItemDate")).ToString()),
                            CurrencySymbol = reader.GetValue(reader.GetOrdinal("CurrencySymbol")).ToString(),
                        };
                        reservationWithDepositsReturnDto.Items.Add(reservationWithDepositsDto);
                    }

                }
                reader.Dispose();
            }

            reservationWithDepositsReturnDto.Items = reservationWithDepositsReturnDto.Items.OrderBy(x => x.ArrivalDate).ThenBy(x => x.PaymentTypeName).ToList();

            return reservationWithDepositsReturnDto;
        }
    }
}
