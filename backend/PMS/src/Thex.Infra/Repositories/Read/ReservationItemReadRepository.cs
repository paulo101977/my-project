﻿//  <copyright file="ReservationItemReadRepository.cs" company="TOTVS">
//  Copyright (c) TOTVS. All rights reserved.
//  </copyright>
namespace Thex.EntityFrameworkCore.Repositories
{
    using Microsoft.EntityFrameworkCore;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Thex.Common;
    using Thex.Common.Enumerations;
    using Thex.Domain.Entities;
    using Thex.Dto;
    using Thex.Dto.ReservationItem;
    using Thex.Infra.Context;
    using Thex.Infra.ReadInterfaces;
    using Thex.Kernel;
    using Tnf.Dto;
    using Tnf.EntityFrameworkCore;
    using Tnf.EntityFrameworkCore.Repositories;
    using Tnf.Localization;

    public class ReservationItemReadRepository : EfCoreRepositoryBase<ThexContext, ReservationItem>, IReservationItemReadRepository, Domain.Interfaces.Repositories.Read.IReservationItemReadRepository
    {

        private readonly IApplicationUser _applicationUser;
        private readonly ILocalizationManager _localizationManager;

        public ReservationItemReadRepository(
            IApplicationUser applicationUser,
            IDbContextProvider<ThexContext> dbContextProvider, ILocalizationManager localizationManager)
            : base(dbContextProvider)
        {
            _applicationUser = applicationUser;
            _localizationManager = localizationManager;
        }

        public IListDto<AccommodationsByReservationIdResultDto> GetAllAccommodations(RequestAllDto request, long propertyId, long reservationId)
        {
            var queryResult = (from ri in Context.ReservationItems
                               join r in Context.Reservations on ri.Reservation.Id equals r.Id

                               join ro in Context.Rooms on ri.Room.Id equals ro.Id into rooms
                               from leftJoinRoom in rooms.DefaultIfEmpty()

                               join rt in Context.RoomTypes on ri.ReceivedRoomType.Id equals rt.Id

                               join rrt in Context.RoomTypes on ri.RequestedRoomType.Id equals rrt.Id

                               join gri in Context.GuestReservationItems on ri.Id equals gri.ReservationItem.Id into guests
                               from leftJoinGuest in guests.DefaultIfEmpty()

                               where r.Id == reservationId &&
                               r.Property.Id == propertyId
                               select new
                               {
                                   ReservationId = r.Id,
                                   ReservationItemCode = ri.ReservationItemCode,
                                   ReservationItemId = ri.Id,
                                   ReservationItemStatusId = ri.ReservationItemStatusId,
                                   ReservationContactName = r.ContactName,
                                   CheckInDate = ri.CheckInDate,
                                   CheckOutDate = ri.CheckOutDate,
                                   EstimatedArrivalDate = ri.EstimatedArrivalDate,
                                   EstimatedDepartureDate = ri.EstimatedDepartureDate,
                                   RoomNumber = leftJoinRoom != null ? leftJoinRoom.RoomNumber : string.Empty,
                                   ReceivedRoomTypeId = rt.Id,
                                   ReceivedRoomTypeName = rt.Name,
                                   ReceivedRoomTypeAbbreviation = rt.Abbreviation,
                                   GuestName = leftJoinGuest != null ? leftJoinGuest.GuestName : string.Empty,
                                   AdultCount = ri.AdultCount,
                                   ChildCount = ri.ChildCount,
                                   RatePlanId = ri.RatePlanId,
                                   CompanyClientId = r.CompanyClientId,
                                   IsMigrated = ri.IsMigrated,
                                   GratuityTypeId = ri.GratuityTypeId,
                                   MealPlanTypeId = ri.MealPlanTypeId,
                                   CurrencyId = ri.CurrencyId,
                                   RequestedRoomTypeId = rrt.Id,
                                   RequestedRoomTypeName = rrt.Name,
                                   RequestedRoomTypeAbbreviation = rrt.Abbreviation
                               }).ToList();

            var listResult = new List<AccommodationsByReservationIdResultDto>();
            foreach (var item in queryResult)
            {
                if (!listResult.Any(exp => exp.Id == item.ReservationItemId))
                {
                    var searchableNames = new List<string>();

                    foreach (var itemSearchableNames in queryResult.Where(exp => exp.ReservationItemId == item.ReservationItemId && !string.IsNullOrEmpty(exp.GuestName)))
                        searchableNames.Add(itemSearchableNames.GuestName);

                    var accomodation = new AccommodationsByReservationIdResultDto
                    {
                        Id = item.ReservationItemId,
                        ReservationId = item.ReservationId,
                        RoomTypeId = item.ReceivedRoomTypeId,
                        StatusId = item.ReservationItemStatusId,
                        RoomNumber = item.RoomNumber,
                        RoomTypeName = item.ReceivedRoomTypeName,
                        RoomTypeAbbreviation = item.ReceivedRoomTypeAbbreviation,
                        CheckIn = item.CheckInDate,
                        CheckOut = item.CheckOutDate,
                        EstimatedArrivalDate = item.EstimatedArrivalDate,
                        EstimatedDepartureDate = item.EstimatedDepartureDate,
                        AdultCount = item.AdultCount,
                        ChildCount = item.ChildCount,
                        SearchableNames = searchableNames,
                        IsValid = searchableNames.Count == item.AdultCount + item.ChildCount,
                        ReservationContactName = item.ReservationContactName,
                        ReservationItemCode = item.ReservationItemCode,
                        CompanyClientId = item.CompanyClientId,
                        GratuityTypeId = item.GratuityTypeId,
                        MealPlanTypeId = item.MealPlanTypeId,
                        CurrencyId = item.CurrencyId,
                        ReservationItemStatus = _localizationManager.GetString(AppConsts.LocalizationSourceName, ((ReservationStatus)item.ReservationItemStatusId).ToString()),
                        RequestedRoomTypeId = item.RequestedRoomTypeId,
                        RequestedRoomTypeName = item.RequestedRoomTypeName,
                        RequestedRoomTypeAbbreviation = item.RequestedRoomTypeAbbreviation,
                        IsMigrated = item.IsMigrated
                    };

                    listResult.Add(accomodation);
                }
            }


            var result = new ListDto<AccommodationsByReservationIdResultDto>();
            result.Items = listResult;
            result.HasNext = false;

            return result;
        }

        public IListDto<ReservationItemDto> GetAllByPropertyAndReservation(RequestAllDto request, long reservationId)
        {
            var baseQuery = Context
                .ReservationItems

                .Include(d => d.Room)
                .Include(d => d.ReceivedRoomType)
                .Include(d => d.RequestedRoomType)
                .Where(d => d.Reservation.Id == reservationId);
            //.SkipAndTakeByRequestDto(request)
            //.OrderByRequestDto(request);

            var result = baseQuery.ToListDto<ReservationItem, ReservationItemDto>(request);

            foreach (var reservationItem in result.Items)
            {
                reservationItem.ReservationItemStatusFormatted = _localizationManager.GetString(AppConsts.LocalizationSourceName, ((ReservationStatus)reservationItem.ReservationItemStatusId).ToString());
            }

            return result;
        }

        public IListDto<GuestGuestRegistrationResultDto> GetAllGuestGuestRegistration(long propertyId, long reservationItemId, string language)
        {
            var queryResult = (from ri in Context.ReservationItems
                               join r in Context.Reservations on ri.Reservation.Id equals r.Id
                               join gr in Context.GuestReservationItems on ri.Id equals gr.ReservationItemId

                               join b in Context.BusinessSources on r.BusinessSourceId equals b.Id into businessSources
                               from leftBusinessSources in businessSources.DefaultIfEmpty()

                               join guestRegistration in Context.GuestRegistrations on gr.GuestRegistrationId equals guestRegistration.Id into guestRegistrations
                               from leftJoinGuestRegistration in guestRegistrations.DefaultIfEmpty()

                               join g in Context.Guests on gr.GuestId equals g.Id into guests
                               from leftJoinGuest in guests.DefaultIfEmpty()

                                   //join p in Context.Persons on leftJoinGuest.PersonId equals p.Id into person
                                   //from leftPerson in person.DefaultIfEmpty()

                               join l in Context.Locations on leftJoinGuestRegistration.Id equals l.OwnerId into locations
                               from leftLocation in locations.DefaultIfEmpty()

                               join c in Context.CountrySubdivisions on leftLocation.CityId equals c.Id into city
                               from leftCity in city.DefaultIfEmpty()

                               join s in Context.CountrySubdivisions on leftCity.ParentSubdivision.Id equals s.Id into state
                               from leftState in state.DefaultIfEmpty()

                               join co in Context.CountrySubdivisions on leftJoinGuestRegistration.Nationality equals co.Id into country
                               from leftCountry in country.DefaultIfEmpty()

                               join ct in Context.CountrySubdivisionTranslations on //leftCity.Id equals ct.CountrySubdivisionId into cityTranslation
                               new { ct1 = leftCity.Id, ct2 = language } equals
                               new { ct1 = ct.CountrySubdivisionId, ct2 = ct.LanguageIsoCode.ToLower() } into cityTranslation
                               from leftCityTranslation in cityTranslation.DefaultIfEmpty()

                               join st in Context.CountrySubdivisionTranslations on //leftState.Id equals st.CountrySubdivisionId into stateTranslation
                               new { st1 = leftState.Id, st2 = language } equals
                               new { st1 = st.CountrySubdivisionId, st2 = st.LanguageIsoCode.ToLower() } into stateTranslation
                               from leftStateTranslation in stateTranslation.DefaultIfEmpty()

                               join cot in Context.CountrySubdivisionTranslations on //leftCountry.Id equals cot.CountrySubdivisionId into countryTranslation
                               new { cot1 = leftCountry.Id, cot2 = language } equals
                               new { cot1 = cot.CountrySubdivisionId, cot2 = cot.LanguageIsoCode.ToLower() } into countryTranslation
                               from leftCountryTranslation in countryTranslation.DefaultIfEmpty()

                                   //join cpd in Context.CountryPersonDocumentTypes on
                                   //new { cpd1 = leftCountry.Id, cpd2 = leftPerson.PersonType } equals
                                   //new { cpd1 = cpd.CountrySubdivisionId, cpd2 = cpd.PersonType } into countryPersonDocumentTypes
                                   //from leftCountryPersonDocumentTypes in countryPersonDocumentTypes.DefaultIfEmpty()

                                   //join d in Context.Documents on leftJoinGuestRegistration.Id equals d.OwnerId into document
                                   //from leftDocument in document.DefaultIfEmpty()

                               join dt in Context.DocumentTypes on leftJoinGuestRegistration.DocumentTypeId equals dt.Id into documentType
                               from leftDocumentType in documentType.DefaultIfEmpty()

                               where ri.Id == reservationItemId &&
                               r.Property.Id == propertyId
                               select new
                               {
                                   //reserva
                                   ReservationId = r.Id,
                                   ReservationContactName = r.ContactName,
                                   //origem
                                   BusinessSourceName = leftBusinessSources != null ? leftBusinessSources.Description : null,
                                   //reservation item
                                   ReservationItemId = ri.Id,
                                   ReservationItemCode = ri.ReservationItemCode,
                                   ReservationItemStatusId = ri.ReservationItemStatusId,
                                   //guest reservation item
                                   GuestReservationItemId = gr.Id,
                                   GuestReservationItemName = gr.GuestName,
                                   GuestStatusId = gr.GuestStatusId,
                                   IsLegallyIncompetent = leftJoinGuestRegistration != null ? leftJoinGuestRegistration.IsLegallyIncompetent : false,
                                   //guest - person
                                   PersonId = leftJoinGuestRegistration != null ? leftJoinGuestRegistration.PersonId : (Guid?)null,
                                   PersonName = leftJoinGuestRegistration != null ? leftJoinGuestRegistration.FullName : null,
                                   //documents
                                   DocumentInformation = leftJoinGuestRegistration != null ? leftJoinGuestRegistration.DocumentInformation : null,
                                   DocumentTypeName = leftDocumentType != null ? leftDocumentType.Name : null,
                                   //guestRegistration
                                   GuestRegistrationId = leftJoinGuestRegistration != null ? leftJoinGuestRegistration.Id : (Guid?)null,
                                   GuestRegistrationGuestName = leftJoinGuestRegistration != null ? leftJoinGuestRegistration.FullName : string.Empty,
                                   GuestRegistrationDocumentBirthDate = leftJoinGuestRegistration != null ? leftJoinGuestRegistration.BirthDate : (DateTime?)null,
                                   //location
                                   Division = leftStateTranslation != null ? leftState.TwoLetterIsoCode ?? leftStateTranslation.Name : null,
                                   Subdivision = leftCityTranslation != null ? leftCityTranslation.Name : null,
                                   Country = leftCountryTranslation != null ? leftCountryTranslation.Name : null,
                                   gr.IsChild,
                                   gr.ChildAge,
                                   gr.IsMain,
                                   gr.IsSeparated,
                                   gr.IsGratuity,
                                   gr.PaxPosition,
                               }).ToList();

            var listResult = new List<GuestGuestRegistrationResultDto>();
            foreach (var item in queryResult)
            {
                if (!listResult.Any(exp => exp.GuestReservationItemId == item.GuestReservationItemId))
                {
                    var guestReservationItem = new GuestGuestRegistrationResultDto
                    {
                        Id = item.ReservationId,
                        BusinessSourceName = item.BusinessSourceName,
                        ReservationItemId = item.ReservationItemId,
                        ReservationItemCode = item.ReservationItemCode,
                        ReservationItemStatusId = item.ReservationItemStatusId,
                        GuestReservationItemId = item.GuestReservationItemId,
                        GuestReservationItemName = !string.IsNullOrEmpty(item.GuestReservationItemName) ?
                                                        item.GuestReservationItemName :
                                                        (!string.IsNullOrEmpty(item.PersonName) ? item.PersonName : item.GuestRegistrationGuestName),
                        GuestStatusId = item.GuestStatusId,
                        IsLegallyIncompetent = item.IsLegallyIncompetent,
                        PersonId = item.PersonId,
                        DocumentInformation = item.DocumentInformation,
                        DocumentTypeName = item.DocumentTypeName,
                        GuestRegistrationId = item.GuestRegistrationId,
                        GuestRegistrationDocumentBirthDate = item.GuestRegistrationDocumentBirthDate,
                        Division = item.Division,
                        Subdivision = item.Subdivision,
                        Country = item.Country,
                        IsValid = item.GuestRegistrationId.HasValue,
                        IsChild = item.IsChild,
                        ChildAge = item.ChildAge,
                        IsMain = item.IsMain,
                    };

                    listResult.Add(guestReservationItem);
                }
            }


            var result = new ListDto<GuestGuestRegistrationResultDto>();
            result.Items = listResult;
            result.HasNext = false;

            return result;
        }

        public void Detach()
        {
            Context.ChangeTracker.Entries()
                .Where(e => e.Entity != null).ToList()
                .ForEach(e => e.State = EntityState.Detached);
        }

        //used only in the unavailability grid display
        public List<ReservationItem> GetAvailabilityReservationsItemsByPeriodAndPropertyId(DateTime initialDate, DateTime finalDate, int propertyId, bool? completeReservationItemInformations = true, int? roomTypeId = null)
        {
            var status = new int[] { (int)ReservationStatus.ToConfirm, (int)ReservationStatus.Confirmed, (int)ReservationStatus.Checkin }.ToList();

            var dbBaseQuery = (from ri in Context.ReservationItems.AsNoTracking()
                               join r in Context.Reservations.AsNoTracking()
                               on ri.ReservationId equals r.Id
                               where r.PropertyId == propertyId &&
                               (ri.CheckInDate ?? ri.EstimatedArrivalDate).Date <= finalDate.Date &&
                               status.Contains(ri.ReservationItemStatusId) &&
                               (ri.CheckOutDate ?? ri.EstimatedDepartureDate).Date >= initialDate.Date
                               select ri);

            if (roomTypeId.HasValue)
                dbBaseQuery = dbBaseQuery.Where(e => e.ReceivedRoomTypeId == roomTypeId.Value);

            if (completeReservationItemInformations.HasValue && !completeReservationItemInformations.Value)
                dbBaseQuery.Select(ri => new ReservationItem
                {
                    Id = ri.Id,
                    CheckInDate = ri.CheckInDate,
                    CheckOutDate = ri.CheckOutDate,
                    EstimatedArrivalDate = ri.EstimatedArrivalDate,
                    EstimatedDepartureDate = ri.EstimatedDepartureDate,
                    ReceivedRoomTypeId = ri.ReceivedRoomTypeId,
                    RoomId = ri.RoomId
                });

            return dbBaseQuery.ToList();
        }

        //used only in the unavailability grid display
        public List<ReservationItem> GetAvailabilityReservationsItemsWithMainGuest(DateTime initialDate, DateTime finalDate, int propertyId, int roomTypeId)
        {
            var status = new int[] { (int)ReservationStatus.ToConfirm, (int)ReservationStatus.Confirmed, (int)ReservationStatus.Checkin }.ToList();

            var dbBaseQuery = (from ri in Context.ReservationItems.AsNoTracking()
                               join gr in Context.GuestReservationItems.AsNoTracking()
                               on ri.Id equals gr.ReservationItemId

                               join reservation in Context.Reservations.AsNoTracking() on ri.ReservationId equals reservation.Id

                               join room in Context.Rooms.AsNoTracking() on ri.RoomId equals room.Id

                               join roomtype in Context.RoomTypes.AsNoTracking() on ri.ReceivedRoomTypeId equals roomtype.Id
                               where
                               gr.IsMain &&
                               ri.ReceivedRoomTypeId == roomTypeId &&
                               (ri.CheckInDate ?? ri.EstimatedArrivalDate).Date <= finalDate.Date &&
                               status.Contains(ri.ReservationItemStatusId) &&
                               (ri.CheckOutDate ?? ri.EstimatedDepartureDate).Date >= initialDate.Date
                               select new ReservationItem
                               {
                                   Id = ri.Id,
                                   RoomId = ri.RoomId,
                                   ReservationId = ri.ReservationId,
                                   ReceivedRoomTypeId = ri.ReceivedRoomTypeId,
                                   AdultCount = ri.AdultCount,
                                   ChildCount = ri.ChildCount,
                                   CheckInDate = ri.CheckInDate,
                                   CheckOutDate = ri.CheckOutDate,
                                   EstimatedArrivalDate = ri.EstimatedArrivalDate,
                                   EstimatedDepartureDate = ri.EstimatedDepartureDate,
                                   ReservationItemStatusId = ri.ReservationItemStatusId,
                                   GuestReservationItemList = new List<GuestReservationItem>
                                   {
                                       new GuestReservationItem
                                       {
                                           GuestName = gr.GuestName
                                       }
                                   },
                                   Reservation = new Reservation
                                   {
                                       ReservationCode = reservation.ReservationCode
                                   },
                                   ReceivedRoomType = new RoomType
                                   {
                                       Abbreviation = roomtype.Abbreviation
                                   }

                               });

            return dbBaseQuery.ToList();
        }

        public ReservationItemDto GetDtoById(long reservationItemId)
        {
            var dbBaseQuery = Context.ReservationItems.AsNoTracking()
                        .FirstOrDefault(e => e.Id == reservationItemId);

            var reservationItem = dbBaseQuery.MapTo<ReservationItemDto>();

            if (reservationItem != null)
                reservationItem.ReservationItemStatusFormatted = _localizationManager.GetString(AppConsts.LocalizationSourceName, ((ReservationStatus)reservationItem.ReservationItemStatusId).ToString());

            return reservationItem;
        }

        public ReservationItemDto GetByIdNoTracking(long reservationItemId)
        {
            var dbBaseQuery = Context.ReservationItems.AsNoTracking()

                        .Include(e => e.ReservationBudgetList)
                        .Include(e => e.GuestReservationItemList)
                        .Include(e => e.Currency)
                        .FirstOrDefault(e => e.Id == reservationItemId);

            var reservationItem = dbBaseQuery.MapTo<ReservationItemDto>();

            if (reservationItem != null)
                reservationItem.ReservationItemStatusFormatted = _localizationManager.GetString(AppConsts.LocalizationSourceName, ((ReservationStatus)reservationItem.ReservationItemStatusId).ToString());

            return reservationItem;
        }

        public IList<ReservationItemDto> GetAllDtoByReservationId(long reservationId)
        {
            var reservationItemList = Context.ReservationItems.AsNoTracking()
                        .Where(e => e.ReservationId == reservationId);

            var reservationItemDtoList = new List<ReservationItemDto>();

            foreach (var reservationItem in reservationItemList)
            {
                reservationItemDtoList.Add(reservationItem.MapTo<ReservationItemDto>());
            }

            return reservationItemDtoList;
        }

        public List<GuestReservationItem> GetGuestReservationItemByReservationItemId(long reservationItemId)
        {
            return Context
                .GuestReservationItems

                .Where(x => x.ReservationItemId == reservationItemId).ToList();
        }

        public List<ReservationItem> GetReservationItemListWithoutDailyByCheckinDate(int propertyId, DateTime systemDate)
        {
            var dbBaseQuery = Context.ReservationItems
                .FromSql(@"select * from reservationitem rii where not rii.reservationitemid in (
                select distinct(ri.ReservationItemId) from billingaccount ba
                inner join Reservation r on ba.ReservationId = r.ReservationId
                inner join ReservationItem ri on ba.ReservationItemId = ri.ReservationItemId
                inner join billingaccountitem bai on ba.BillingAccountId = bai.BillingAccountId
                inner join billingitem bi on bai.BillingItemId = bi.BillingItemId
                where bai.daily = 1 and bai.WasReversed = 0 and CONVERT(date, bai.BillingAccountItemDate) = CONVERT(date,{0}) and r.PropertyId = {1})
                and 
                CONVERT(date, coalesce(rii.CheckInDate, rii.EstimatedArrivalDate)) <= CONVERT(date,{0})
                and CONVERT(date, coalesce(rii.CheckOutDate, rii.EstimatedDepartureDate)) > CONVERT(date,{0})
                and rii.ReservationItemStatusId = {2}
                and rii.TenantId = {3}",
                systemDate.ToString("yyyy-MM-dd"), propertyId, (int)ReservationStatus.Checkin, _applicationUser.TenantId.ToString())
                .Include(r => r.ReservationBudgetList)
                .Include(r => r.BillingAccountList)
                .Include(r => r.GuestReservationItemList)
                .Include(r => r.Reservation)
                .ThenInclude(re => re.ReservationConfirmationList)
                .Include(r => r.Reservation)
                .ThenInclude(re => re.BillingAccountList)
                .Include(r => r.RatePlan)
                .Include(r => r.Room)
                .ToList();
            return dbBaseQuery.ToList();
        }

        public ReservationItem GetByBillingAccountdId(Guid id)
        {
            var billingAccount = Context.BillingAccounts.AsNoTracking().Include(x => x.ReservationItem).FirstOrDefault(x => x.Id == id);
            if (billingAccount != null)
                return billingAccount.ReservationItem;

            return null;
        }

        public ReservationItemSlipReservationDto GetSlipReservation(long reservationId, int propertyId)
        {
            var dbBaseQuery = (from reservation in Context.Reservations
                               join companyClient in Context.CompanyClients on reservation.CompanyClientId equals companyClient.Id into a
                               from leftCompanyClient in a.DefaultIfEmpty()
                               where reservation.Id == reservationId
                               select new ReservationItemSlipReservationDto
                               {
                                   ReservationCode = reservation.ReservationCode,
                                   CompanyClientName = leftCompanyClient != null ? leftCompanyClient.TradeName : null,
                                   ContactName = reservation.ContactName,
                                   ContactPhone = reservation.ContactPhone,
                                   ContactEmail = reservation.ContactEmail,
                                   PropertyPoliciesList = GetAllPropertyPolicies(propertyId),
                                   ReservationItemList = GetAllAccommodationsForSlipReservation(reservationId),
                                   ReservationConfirmation = GetReservationConfirmation(reservationId),
                                   ExternalComments = reservation.ExternalComments
                               }).FirstOrDefault();

            foreach (var reservationItem in dbBaseQuery.ReservationItemList)
            {
                reservationItem.GuestReservationItemForSlipList = GetGuesReservationItemByAccommodation(reservationItem.Id);
            }
            return dbBaseQuery;
        }

        private ReservationConfirmationDto GetReservationConfirmation(long reservationId)
        {
            return (from reservationConfirmation in Context.ReservationConfirmations       
                    join paymentType in Context.PaymentTypes on reservationConfirmation.PaymentTypeId equals paymentType.Id
                    where reservationConfirmation.ReservationId == reservationId
                    select new ReservationConfirmationDto
                    {
                        PaymentType = _localizationManager.GetString(AppConsts.LocalizationSourceName, ((PaymentTypeEnum)paymentType.Id).ToString())
                    }).FirstOrDefault();
        }

        private IList<PropertyPolicyDto> GetAllPropertyPolicies(int propertyId)
        {
            return (from policies in Context.PropertyPolicies
                    join policyTypes in Context.PolicyTypes on policies.PolicyTypeId equals policyTypes.Id
                    where policies.PropertyId == propertyId
                    select new PropertyPolicyDto
                    {
                        PolicyTypeName = policyTypes.PolicyTypeName,
                        PolicyTypeId = policyTypes.Id,
                        Description = policies.Description
                    }).ToList();
        }

        private IList<ReservationItemDto> GetAllAccommodationsForSlipReservation(long reservationId)
        {
            return (from reservationItem in Context.ReservationItems
                    join roomType in Context.RoomTypes on reservationItem.ReceivedRoomTypeId equals roomType.Id

                    let totalBudgetQuery = Context.ReservationBudgets.Where(x => x.ReservationItemId == reservationItem.Id)

                    where reservationItem.ReservationId == reservationId &&
                          reservationItem.ReservationItemStatusId != (int)ReservationStatus.Canceled
                    select new ReservationItemDto
                    {
                        ReservationItemCode = reservationItem.ReservationItemCode,
                        AdultCount = reservationItem.AdultCount,
                        ChildCount = reservationItem.ChildCount,
                        Id = reservationItem.Id,
                        CheckInDate = (reservationItem.CheckInDate ?? reservationItem.EstimatedArrivalDate),
                        CheckOutDate = (reservationItem.CheckOutDate ?? reservationItem.EstimatedDepartureDate),
                        RateDaily = totalBudgetQuery.Any() ? totalBudgetQuery.Average(x => x.ManualRate) : 0,
                        RequestedRoomType = new RoomTypeDto
                        {
                            Name = roomType.Name
                        }
                    }).ToList();
        }

        private IList<GuestReservationItemDto> GetGuesReservationItemByAccommodation(long reservationItemId)
        {
            return (from guestreservationItem in Context.GuestReservationItems
                    where guestreservationItem.ReservationItemId == reservationItemId
                    select new GuestReservationItemDto
                    {
                        GuestName = guestreservationItem.GuestName,
                        IsMain = guestreservationItem.IsMain,
                        ReservationItemId = guestreservationItem.ReservationItemId
                    }).ToList();

        }

        public async Task<List<ReservationItem>> GetAvailabilityReservationsItemsByPeriodAndPropertyIdAsync(DateTime initialDate, DateTime finalDate, int propertyId, bool? completeReservationItemInformations = true, int? roomTypeId = null)
        {
            var status = new int[] { (int)ReservationStatus.ToConfirm, (int)ReservationStatus.Confirmed, (int)ReservationStatus.Checkin }.ToList();

            var dbBaseQuery = await (from ri in Context.ReservationItems.AsNoTracking()
                                     join r in Context.Reservations.AsNoTracking()
                                     on ri.ReservationId equals r.Id
                                     where r.PropertyId == propertyId &&
                                     (ri.CheckInDate ?? ri.EstimatedArrivalDate).Date <= finalDate.Date &&
                                     status.Contains(ri.ReservationItemStatusId) &&
                                     (ri.CheckOutDate ?? ri.EstimatedDepartureDate).Date >= initialDate.Date
                                     select ri).ToListAsync();

            if (roomTypeId.HasValue)
                dbBaseQuery = dbBaseQuery.Where(e => e.ReceivedRoomTypeId == roomTypeId.Value).ToList();

            if (completeReservationItemInformations.HasValue && !completeReservationItemInformations.Value)
                dbBaseQuery.Select(ri => new ReservationItem
                {
                    Id = ri.Id,
                    CheckInDate = ri.CheckInDate,
                    CheckOutDate = ri.CheckOutDate,
                    EstimatedArrivalDate = ri.EstimatedArrivalDate,
                    EstimatedDepartureDate = ri.EstimatedDepartureDate,
                    ReceivedRoomTypeId = ri.ReceivedRoomTypeId,
                    RoomId = ri.RoomId
                });

            return dbBaseQuery.ToList();
        }

        public List<GuestReservationItem> GetGuestReservationItemByReservationItemIdList(List<long> reservationItemIdList)
        {
            return Context
                .GuestReservationItems
                .Where(x => reservationItemIdList.Contains(x.ReservationItemId)).ToList();
        }

        public bool IsReservationItensDoesNotHaveRatePlan(List<long?> ids)
        {
            return Context.ReservationItems.Any(r => ids.Contains(r.Id) && !r.RatePlanId.HasValue);
        }

        public ReservationItem GetReservationItemParent(long reservationId, long reservationItemId)
            => Context.ReservationItems.FirstOrDefault(ri => ri.ReservationId == reservationId && ri.Id != reservationItemId);

        public bool IsReservationItemStatusConfirmed(long reservationItemId)
            => Context.ReservationItems.Any(r => r.Id == reservationItemId && r.ReservationItemStatusId == (int)ReservationStatus.Confirmed);

        public ReservationItem GetByIdWithGuestReservationItemList(long id)
            => Context.ReservationItems.Where(r => r.Id == id)
            .Include(r => r.GuestReservationItemList)
            .FirstOrDefault();

        public IList<ReservationItem> GetAllByReservationId(long reservationId)
        {
            return Context.ReservationItems.AsNoTracking()
                        .Where(e => e.ReservationId == reservationId).ToList();
        }

        public bool IsEarlyDayUse(DateTime systemDate, long reservationItemId)
            => Context.ReservationItems.Any(r => r.Id == reservationItemId && r.CheckInDate.HasValue &&
                                                 r.CheckInDate.Value.Date == systemDate.Date &&
                                                 r.EstimatedDepartureDate.Date > systemDate.Date);

        public async Task<List<ReservationItemDto>> GetReservationsForDashboardAsync(DateTime systemDate, int propertyId)
        {
            return await Context.ReservationItems.Include(ri => ri.Reservation)
                               .Where(ri => ri.Reservation.PropertyId == propertyId &&
                                      ri.EstimatedArrivalDate.Date == systemDate || ri.EstimatedDepartureDate.Date == systemDate ||
                                      ri.CheckInDate.GetValueOrDefault().Date == systemDate || ri.CheckOutDate.GetValueOrDefault().Date == systemDate)
                               .Select(ri => new ReservationItemDto
                               {
                                   Id = ri.Id,
                                    ReservationItemStatusId = ri.ReservationItemStatusId,
                                    EstimatedArrivalDate = ri.EstimatedArrivalDate,
                                    EstimatedDepartureDate = ri.EstimatedDepartureDate,
                                    CheckInDate = ri.CheckInDate,
                                    CheckOutDate = ri.CheckOutDate,
                                    WalkIn = ri.WalkIn
                               }).ToListAsync();
        }

        public ReservationItem GetById(long reservationItemId)
            => Context.ReservationItems.AsNoTracking().FirstOrDefault(e => e.Id == reservationItemId);


        public Dictionary<DateTime,int> GetNumberOfAdultGuestAndBudgetDayByReservationId(ProformaRequestDto requestDto)
        {
            if (requestDto.ReservationId.HasValue)
            {
                return (from ri in Context.ReservationItems.AsNoTracking()
                        join rb in Context.ReservationBudgets.AsNoTracking() on ri.Id equals rb.ReservationItemId
                        join gri in Context.GuestReservationItems.AsNoTracking() on ri.Id equals gri.ReservationItemId
                        join gr in Context.GuestRegistrations.AsNoTracking() on gri.GuestRegistrationId equals gr.Id
                        into gr
                        from leftgr in gr.DefaultIfEmpty()
                        join pgt in Context.PropertyGuestTypes.AsNoTracking()
                        on leftgr.GuestTypeId equals pgt.Id into pgt
                        from leftpgt in pgt.DefaultIfEmpty()
                        where !gri.IsChild && requestDto.ReservationId.Value == ri.ReservationId &&
                        (leftpgt == null || !leftpgt.IsExemptOfTourismTax.HasValue || !leftpgt.IsExemptOfTourismTax.Value)
                        group ri by rb.BudgetDay into g
                        select new
                        {
                            Date = g.Key,
                            Adults = g.Count()
                        }).ToDictionary(d => d.Date, d => d.Adults);
            }
            else
            {
                return (from ri in Context.ReservationItems.AsNoTracking()
                        join rb in Context.ReservationBudgets.AsNoTracking() on ri.Id equals rb.ReservationItemId
                        join gri in Context.GuestReservationItems.AsNoTracking() on ri.Id equals gri.ReservationItemId
                        join gr in Context.GuestRegistrations.AsNoTracking() on gri.GuestRegistrationId equals gr.Id
                        into gr
                        from leftgr in gr.DefaultIfEmpty()
                        join pgt in Context.PropertyGuestTypes.AsNoTracking()
                        on leftgr.GuestTypeId equals pgt.Id into pgt
                        from leftpgt in pgt.DefaultIfEmpty()
                        where !gri.IsChild && requestDto.ReservationItemId.Value == ri.Id &&
                        (leftpgt == null || !leftpgt.IsExemptOfTourismTax.HasValue || !leftpgt.IsExemptOfTourismTax.Value)
                        group ri by rb.BudgetDay into g
                        select new
                        {
                            Date = g.Key,
                            Adults = g.Count()
                        }).ToDictionary(d => d.Date, d => d.Adults);
            }
        }

        public string GetIntegrationIdById(long id)
        {
            return (from ri in Context.ReservationItems
                    where ri.Id == id
                    select ri.IntegrationId).FirstOrDefault();
        }

        public string GetIntegrationIdByReservationId(long reservationId)
        {
            return (from ri in Context.ReservationItems
                    join r in Context.Reservations on ri.ReservationId equals r.Id
                    where ri.ReservationId == reservationId
                    select ri.IntegrationId).FirstOrDefault();
        }

        public ReservationItemDetailsDto GetDetailsDtoById(long id)
        {
            return (from ri in Context.ReservationItems
                    join r in Context.Reservations on ri.ReservationId equals r.Id
                    where ri.Id == id
                    select new ReservationItemDetailsDto
                    {
                        ReservationItemUid = ri.ReservationItemUid,
                        ReservationCode = r.ReservationCode,
                        PartnerReservationNumber = ri.PartnerReservationNumber
                    }).FirstOrDefault();
        }
    }
}
