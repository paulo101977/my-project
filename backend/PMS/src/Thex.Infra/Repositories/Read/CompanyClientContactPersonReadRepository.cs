﻿// //  <copyright file="CompanyClientContactPersonReadRepository.cs" company="TOTVS">
// //  Copyright (c) TOTVS. All rights reserved.
// //  </copyright>
namespace Thex.EntityFrameworkCore.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Thex.Domain.Entities;
    using Thex.Infra.ReadInterfaces;
    using Thex.Dto;
    using Tnf.EntityFrameworkCore.Repositories;
    using Tnf.EntityFrameworkCore;
    using Thex.Common.Enumerations;
    using Thex.Infra.Context;

    public class CompanyClientContactPersonReadRepository : EfCoreRepositoryBase<ThexContext, CompanyClientContactPerson>, ICompanyClientContactPersonReadRepository
    {
        

        public CompanyClientContactPersonReadRepository(IDbContextProvider<ThexContext> dbContextProvider)
            : base(dbContextProvider)
        {

            
        }

        public ICollection<CompanyClientContactPersonDto> GetContactDtoListByClientPersonId(Guid personId)
        {
            var companyClientContactPersonDtoListBaseQuery =
                from contact in this.Context.CompanyClientContactPersons
                join client in this.Context.CompanyClients on contact.CompanyClientId equals client.Id 

                join occupation in this.Context.Occupations on contact.OccupationId equals occupation.Id into
                riOccupation
                from occupationLeft in riOccupation.DefaultIfEmpty()

                join person in this.Context.Persons on contact.PersonId equals person.Id
                
                join emailInformation in this.Context.ContactInformations on
                new { p1 = person.Id, p2 = Convert.ToInt32(ContactInformationTypeEnum.Email) } equals
                new { p1 = emailInformation.OwnerId, p2 = emailInformation.ContactInformationTypeId } into
                ri
                from emailInformationLeft in ri.DefaultIfEmpty()

                join phoneInformation in this.Context.ContactInformations on
                new { p3 = person.Id, p4 = Convert.ToInt32(ContactInformationTypeEnum.PhoneNumber) }
                equals
                new { p3 = phoneInformation.OwnerId, p4 = phoneInformation.ContactInformationTypeId } into
                ri2
                from phoneInformationLeft in ri2.DefaultIfEmpty()

                where client.PersonId == personId
                select new CompanyClientContactPersonDto
                    {
                        PersonId = contact.PersonId,
                        CompanyClientId = contact.CompanyClientId,
                        OccupationId = occupationLeft != null ? occupationLeft.Id : (int?) null,
                        Name = person.FullName,
                        Email = emailInformationLeft.Information,
                        PhoneNumber = phoneInformationLeft.Information,
                        OccupationName = occupationLeft != null ? occupationLeft.Name : string.Empty,
                        Id = contact.Id
                };
            return companyClientContactPersonDtoListBaseQuery.ToList();
        }

        public CompanyClientContactPersonDto GetContactByReservationId(long reservationId)
        {
            var companyClientContactPersonDtoListBaseQuery =
                from contact in this.Context.CompanyClientContactPersons
                join person in this.Context.Persons on contact.PersonId equals person.Id
                join reservation in this.Context.Reservations on person.Id equals reservation.CompanyClientContactPersonId

                join occupation in this.Context.Occupations on contact.OccupationId equals occupation.Id into
                riOccupation
                from occupationLeft in riOccupation.DefaultIfEmpty()

                join emailInformation in this.Context.ContactInformations on
                new { p1 = person.Id, p2 = Convert.ToInt32(ContactInformationTypeEnum.Email) } equals
                new { p1 = emailInformation.OwnerId, p2 = emailInformation.ContactInformationTypeId } into
                ri
                from emailInformationLeft in ri.DefaultIfEmpty()

                join phoneInformation in this.Context.ContactInformations on
                new { p3 = person.Id, p4 = Convert.ToInt32(ContactInformationTypeEnum.PhoneNumber) }
                equals
                new { p3 = phoneInformation.OwnerId, p4 = phoneInformation.ContactInformationTypeId } into
                ri2
                from phoneInformationLeft in ri2.DefaultIfEmpty()

                where reservation.Id == reservationId
                select new CompanyClientContactPersonDto
                           {
                               PersonId = contact.PersonId,
                               CompanyClientId = contact.CompanyClientId,
                               OccupationId = occupationLeft != null ? occupationLeft.Id : (int?)null,
                               Name = person.FirstName,
                               Email = emailInformationLeft == null ? string.Empty : emailInformationLeft.Information,
                               PhoneNumber = phoneInformationLeft == null ? string.Empty : phoneInformationLeft.Information,
                           };
            return companyClientContactPersonDtoListBaseQuery.FirstOrDefault();
        }
    }
}