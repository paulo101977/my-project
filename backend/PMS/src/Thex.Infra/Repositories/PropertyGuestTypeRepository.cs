﻿using Thex.Domain.Entities;
using Thex.Domain.Interfaces.Repositories;
using Tnf.EntityFrameworkCore.Repositories;
using Tnf.EntityFrameworkCore;
using Thex.Infra.Context;
using Thex.Dto;
using System.Collections.Generic;
using System.Linq;
using EFCore.BulkExtensions;

namespace Thex.EntityFrameworkCore.Repositories
{
    public class PropertyGuestTypeRepository : EfCoreRepositoryBase<ThexContext, PropertyGuestType>, IPropertyGuestTypeRepository
    {
        public PropertyGuestTypeRepository(
            IDbContextProvider<ThexContext> dbContextProvider)
            : base(dbContextProvider)
        {
        }

        public void ToggleAndSaveIsActive(int id)
        {
            var entity = base.Get(new DefaultIntRequestDto(id));
            entity.IsActive = !entity.IsActive;

            Context.SaveChanges();
        }

        public void UpdateIsExemptOfTourismTax(List<int> idList, bool isExemptOfTourismTax)
        {
            Context.PropertyGuestTypes
                .Where(p => idList.Contains(p.Id))
                .BatchUpdate(new PropertyGuestType { IsExemptOfTourismTax = isExemptOfTourismTax });

            Context.SaveChanges();
        }  
    }
}
