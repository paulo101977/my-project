﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Thex.Common;
using Thex.Common.Enumerations;
using Thex.Domain.Entities;
using Thex.Infra.ReadInterfaces;
using Tnf.EntityFrameworkCore.Repositories;
using Tnf.EntityFrameworkCore;
using Thex.Infra.Context;
using Tnf.Notifications;
using Thex.Dto;

namespace Thex.EntityFrameworkCore.Repositories
{
    public class PropertyCompanyClientCategoryRepository : EfCoreRepositoryBase<ThexContext, PropertyCompanyClientCategory>, IPropertyCompanyClientCategoryRepository
    {
        private readonly INotificationHandler Notification;

        public PropertyCompanyClientCategoryRepository(IDbContextProvider<ThexContext> dbContextProvider, INotificationHandler notificationHandler) : base(dbContextProvider)
        {
            Notification = notificationHandler;
        }

        public void ToggleAndSaveIsActive(Guid id)
        {
            PropertyCompanyClientCategory category = base.Get(new DefaultGuidRequestDto(id));
            category.IsActive = !category.IsActive;
            Context.SaveChanges();
        }

        public void Delete(Guid id)
        {
            try
            {
                var obj = base.Get(new DefaultGuidRequestDto(id));
                base.Delete(obj);

                Context.SaveChanges();

            }
            catch
            {
                Context.ChangeTracker.Entries()
                    .Where(e => e.Entity != null).ToList()
                    .ForEach(e => e.State = EntityState.Detached);

                Notification.Raise(Notification.DefaultBuilder
                        .WithMessage(AppConsts.LocalizationSourceName, PropertyCompanyClientCategory.EntityError.PropertyCompanyClientCategoryForeignKeyError)
                                    .WithDetailedMessage(AppConsts.LocalizationSourceName, PropertyCompanyClientCategory.EntityError.PropertyCompanyClientCategoryForeignKeyError)
                                    .Build());
            }
        }
    }
}
