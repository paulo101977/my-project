﻿// //  <copyright file="BillingAccountRepository.cs" company="TOTVS">
// //  Copyright (c) TOTVS. All rights reserved.
// //  </copyright>
namespace Thex.EntityFrameworkCore.Repositories
{
    using Microsoft.EntityFrameworkCore;
    using System;
    using System.Linq;
    using Thex.Domain.Entities;
    using Thex.Domain.Interfaces.Repositories;

    using Tnf.EntityFrameworkCore.Repositories;
    using Tnf.EntityFrameworkCore;
    using Thex.Infra.Context;
    using System.Threading.Tasks;
    using System.Collections.Generic;

    public class BillingAccountItemInvoiceHistoryRepository : EfCoreRepositoryBase<ThexContext, BillingAccountItemInvoiceHistory>, IBillingAccountItemInvoiceHistoryRepository
    {
        public BillingAccountItemInvoiceHistoryRepository(IDbContextProvider<ThexContext> dbContextProvider)
            : base(dbContextProvider)
        {
        }

        public async Task AddRangeAsync(List<BillingAccountItemInvoiceHistory> billingAccountItemInvoiceHistoryList)
        {
            if (billingAccountItemInvoiceHistoryList == null || billingAccountItemInvoiceHistoryList.Count == 0)
                return;

            await Context.BillingAccountItemInvoiceHistories.AddRangeAsync(billingAccountItemInvoiceHistoryList);
            await Context.SaveChangesAsync();
        }
    }
}