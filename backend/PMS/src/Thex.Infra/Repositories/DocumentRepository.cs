﻿// //  <copyright file="DocumentRepository.cs" company="TOTVS">
// //  Copyright (c) TOTVS. All rights reserved.
// //  </copyright>
namespace Thex.EntityFrameworkCore.Repositories
{
    using System.Collections.Generic;

    using Thex.Domain.Entities;
    using Thex.Domain.Interfaces.Repositories;

    using Tnf.EntityFrameworkCore.Repositories;
    using Tnf.EntityFrameworkCore;
    using System;
    using System.Linq;
    using Microsoft.EntityFrameworkCore;
    using Thex.Infra.Context;
    using Thex.Dto;

    public class DocumentRepository : EfCoreRepositoryBase<ThexContext, Document>, IDocumentRepository
    {
        public DocumentRepository(IDbContextProvider<ThexContext> dbContextProvider)
            : base(dbContextProvider)
        {
        }

        public void AddRange(IList<Document> documentList)
        {
            Context.Documents.AddRange(documentList);
            Context.SaveChanges();
        }

        public void UpdateDocumentList(IList<Document> documentList, Guid ownerId)
        {
            var existingDocuments = Context.Documents
                           .Where(exp => exp.OwnerId == ownerId)
                           .ToList();

            if (existingDocuments != null)
            {
                RemoveDocuments(documentList, existingDocuments);

                foreach (var document in documentList)
                    InsertOrUpdateDocument(ownerId, existingDocuments, document);

                Context.SaveChanges();
            }
        }

        private void InsertOrUpdateDocument(Guid ownerId, List<Document> existingDocuments, Document document)
        {
            var existingDocument = existingDocuments
                                   .Where(d => d.Id == document.Id && d.Id != 0)
                                   .FirstOrDefault();

            document.OwnerId = ownerId;

            if (existingDocument != null)
            {
                Context.Entry(existingDocument).State = EntityState.Modified;
                Context.Entry(existingDocument).CurrentValues.SetValues(document);
            }
            else
                Context.Documents.Add(document);
        }

        private void RemoveDocuments(IList<Document> documentList, List<Document> existingDocuments)
        {
            var documentIds = documentList.Select(e => e.Id).ToList();
            var documentListToExclude = existingDocuments.Where(d => documentIds.Contains(d.Id)).ToList();

            if (documentListToExclude.Count > 0)
                Context.RemoveRange(documentListToExclude);
        }


        public void CreateOrUpdateDocumentGuestRegistration(Document document, Guid ownerId)
        {
            var existingDocument = Context.Documents
                           .Where(exp => exp.OwnerId == ownerId && exp.DocumentTypeId == document.DocumentTypeId)
                           .FirstOrDefault();


            document.OwnerId = ownerId;

            if (existingDocument != null)
            {
                Context.Entry(existingDocument).State = EntityState.Modified;
                Context.Entry(existingDocument).CurrentValues.SetValues(document);
            }
            else
                Context.Documents.Add(document);

            Context.SaveChanges();
        }

        public void RemoveDocument(Document existingDocument)
        {
            if (existingDocument != null)
            {
                Context.Remove(existingDocument);
                Context.SaveChanges();
            }  
        }
    }
}