// //  <copyright file="PersonInformationRepository.cs" company="TOTVS">
// //  Copyright (c) TOTVS. All rights reserved.
// //  </copyright>
using Thex.Domain.Entities;
using Thex.Domain.Interfaces.Repositories;

namespace Thex.EntityFrameworkCore.Repositories
{
    using System.Collections.Generic;
    using Tnf.EntityFrameworkCore.Repositories;
    using Tnf.EntityFrameworkCore;
    using Thex.Infra.Context;

    public class PersonInformationRepository : EfCoreRepositoryBase<ThexContext, PersonInformation>, IPersonInformationRepository
    {
        public PersonInformationRepository(IDbContextProvider<ThexContext> dbContextProvider)
            : base(dbContextProvider)
        {
        }

        public void AddRange(IList<PersonInformation> personInformationList)
        {
            Context.PersonInformations.AddRange(personInformationList);
            Context.SaveChanges();
        }
    }
}