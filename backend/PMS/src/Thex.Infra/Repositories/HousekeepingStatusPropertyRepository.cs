﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Thex.Domain.Entities;
using Thex.Domain.Interfaces.Repositories;
using Tnf.EntityFrameworkCore.Repositories;
using Tnf.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Thex.Infra.ReadInterfaces;
using Thex.Infra.Context;
using Thex.Dto;

namespace Thex.EntityFrameworkCore.Repositories
{
    public class HousekeepingStatusPropertyRepository : EfCoreRepositoryBase<ThexContext, HousekeepingStatusProperty>, IHousekeepingStatusPropertyRepository
    {
        private readonly IRoomRepository _roomRepository;
        

        public HousekeepingStatusPropertyRepository(
            IDbContextProvider<ThexContext> dbContextProvider, 
            IRoomRepository roomRepository) : base(dbContextProvider)
        {
            _roomRepository = roomRepository;
            
        }

        public void HousekeepingStatusPropertyCreate(HousekeepingStatusProperty housekeepingStatusProperty)
        {
            Context.HousekeepingStatusProperties.Add(housekeepingStatusProperty);
            Context.SaveChanges();
        }

        public void ToggleAndSaveIsActive(Guid id)
        {
            var housekeepingStatusProperty = base.Get(new DefaultGuidRequestDto(id));
            housekeepingStatusProperty.IsActive = !housekeepingStatusProperty.IsActive;
            Context.SaveChanges();
        }

        public new void Update(HousekeepingStatusProperty housekeepingStatusProperty)
        {
            var housekeepingStatusPropertyOld = Context
                .HousekeepingStatusProperties
                
                .Where(x => x.Id == housekeepingStatusProperty.Id).SingleOrDefault();


            if (housekeepingStatusPropertyOld != null)
            {
                Context.Entry(housekeepingStatusPropertyOld).State = EntityState.Modified;
                Context.Entry(housekeepingStatusPropertyOld).CurrentValues.SetValues(housekeepingStatusProperty);
                Context.SaveChanges();
            }
        }

        public void HousekeepingStatusPropertyDelete(Guid id)
        {
            var obj = base.Get(new DefaultGuidRequestDto(id));
            base.Delete(obj);
            Context.SaveChanges();
        }

        public void UpdateRoomForHousekeeppingStatus(int roomId, Guid housekeepingStatusPropertyId)
        {
            _roomRepository.UpdateRoomForHousekeeppingStatus(roomId, housekeepingStatusPropertyId);
        }

        public void UpdateManyRoomForHousekeeppingStatus(List<int> roomIds, Guid housekeepingStatusPropertyId)
        {
            _roomRepository.UpdateManyRoomForHousekeeppingStatus(roomIds, housekeepingStatusPropertyId);
        }
    }
}
