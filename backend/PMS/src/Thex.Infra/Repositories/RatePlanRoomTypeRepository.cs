﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Thex.Domain.Entities;
using Thex.Domain.Interfaces.Repositories;
using Thex.Infra.ReadInterfaces;
using Tnf.EntityFrameworkCore.Repositories;
using Tnf.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Thex.Infra.Context;

namespace Thex.EntityFrameworkCore.Repositories
{
    public class RatePlanRoomTypeRepository : EfCoreRepositoryBase<ThexContext, RatePlanRoomType>, IRatePlanRoomTypeRepository
    {
        

        public RatePlanRoomTypeRepository(IDbContextProvider<ThexContext> dbContextProvider) : base(dbContextProvider)
        {
            
        }

        public void AddRange(List<RatePlanRoomType> ratePlanRoomTypes)
        {

            foreach (var roomType in ratePlanRoomTypes)
            {
                Context.Add(roomType);
            }
           
            Context.SaveChanges();
        }

        public void RatePlanRoomTypeUpdate(RatePlanRoomType ratePlanRoomType)
        {
            var RatePlanOld = Context
                .RatePlanRoomTypes
                
                .Where(x => x.Id == ratePlanRoomType.Id)
                .SingleOrDefault();

            if (RatePlanOld != null)
            {
                Context.Entry(RatePlanOld).State = EntityState.Modified;
                Context.Entry(RatePlanOld).CurrentValues.SetValues(ratePlanRoomType);
                Context.SaveChanges();
            }
        }
    }
}
