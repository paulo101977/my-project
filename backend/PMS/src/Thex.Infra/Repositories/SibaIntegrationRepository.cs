﻿using Thex.Domain.Entities;
using Thex.Domain.Interfaces.Repositories;
using Thex.Infra.Context;
using Thex.Infra.ReadInterfaces;
using Tnf.EntityFrameworkCore;
using Tnf.EntityFrameworkCore.Repositories;

namespace Thex.Infra.Repositories
{
    public class SibaIntegrationRepository : EfCoreRepositoryBase<ThexContext, SibaIntegration>, ISibaIntegrationRepository
    {
        public SibaIntegrationRepository(
            IDbContextProvider<ThexContext> dbContextProvider,
            ISibaIntegrationReadRepository sibaIntegrationReadRepository) 
            : base(dbContextProvider)
        {

        }

        public new void Update(SibaIntegration sibaIntegration)
        {
            Context.SibaIntegrations.Update(sibaIntegration);
            Context.SaveChanges();
        }
    }
}
