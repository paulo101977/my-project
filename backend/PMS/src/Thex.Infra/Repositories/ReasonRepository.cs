﻿using Microsoft.EntityFrameworkCore;
using System.Linq;
using Thex.Common;
using Thex.Common.Enumerations;
using Thex.Domain.Entities;
using Thex.Domain.Interfaces.Repositories;
using Tnf.EntityFrameworkCore.Repositories;
using Tnf.EntityFrameworkCore;
using Thex.Infra.Context;
using Thex.Dto;
using Tnf.Notifications;

namespace Thex.EntityFrameworkCore.Repositories
{
    public class ReasonRepository : EfCoreRepositoryBase<ThexContext, Reason>, IReasonRepository
    {
        private readonly INotificationHandler Notification;

        public ReasonRepository(IDbContextProvider<ThexContext> dbContextProvider,
            INotificationHandler notificationHandler) : base(dbContextProvider)
        {
            Notification = notificationHandler;
        }

        public void Delete(int id)
        {
            try
            {
                var obj = base.Get(new DefaultIntRequestDto(id));
                base.Delete(obj);
                Context.SaveChanges();
            }
            catch
            {

                Context.ChangeTracker.Entries()
                     .Where(e => e.Entity != null).ToList()
                     .ForEach(e => e.State = EntityState.Detached);

                Notification.Raise(Notification.DefaultBuilder
                        .WithMessage(AppConsts.LocalizationSourceName, ReasonEnum.Error.DeleteReason)
                        .WithDetailedMessage(AppConsts.LocalizationSourceName, ReasonEnum.Error.DeleteReason)
                        .Build());
            }

        }

        public void ToggleAndSaveIsActive(int id)
        {
            Reason reason = base.Get(new DefaultIntRequestDto(id));
            reason.IsActive = !reason.IsActive;
            Context.SaveChanges();
        }


        public new void Update(Reason reason)
        {
            var reasonOld = Context.Reasons.Where(x => x.Id == reason.Id).SingleOrDefault();

            if (reasonOld != null)
            {
                Context.Entry(reasonOld).State = EntityState.Modified;
                Context.Entry(reasonOld).CurrentValues.SetValues(reason);
                Context.SaveChanges();
            }

        }
    }
}
