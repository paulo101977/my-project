﻿using Thex.Domain.Entities;
using Thex.Domain.Interfaces.Repositories;
using Tnf.EntityFrameworkCore.Repositories;
using Tnf.EntityFrameworkCore;
using Thex.Infra.Context;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using System;

namespace Thex.EntityFrameworkCore.Repositories
{
    public class ExternalRegistrationRepository : EfCoreRepositoryBase<ThexContext, ExternalRegistration>, IExternalRegistrationRepository
    {
        public ExternalRegistrationRepository(IDbContextProvider<ThexContext> dbContextProvider)
            : base(dbContextProvider)
        {
        }

        public override async Task<ExternalRegistration> InsertAndSaveChangesAsync(ExternalRegistration entity)
        {
            await Context.ExternalRegistrations.AddAsync(entity);
            await Context.SaveChangesAsync();

            return entity;
        }

        public async Task InsertItemsAndSaveChangesAsync(List<ExternalGuestRegistration> externalRegistrationList)
        {
            await Context.ExternalGuestRegistrations.AddRangeAsync(externalRegistrationList);
            await Context.SaveChangesAsync();
        }

        public async Task UpdateExternalGuestAndSaveChanges(ExternalGuestRegistration externalGuestRegistration)
        {
            Context.ExternalGuestRegistrations.Update(externalGuestRegistration);
            await Context.SaveChangesAsync();
        }

        public async Task<ExternalGuestRegistration> GetByGuestReservationItemIdAsync(long guestReservationItemId)
            => await Context.ExternalGuestRegistrations.FirstOrDefaultAsync(e => e.GuestReservationItemId == guestReservationItemId);

        public async Task<ExternalRegistration> GetByReservationUidAsync(Guid reservationUid)
            => await Context.ExternalRegistrations.FirstOrDefaultAsync(e => e.ReservationUid == reservationUid);

    }
}