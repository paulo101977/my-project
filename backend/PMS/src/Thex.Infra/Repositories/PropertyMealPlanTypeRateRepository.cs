﻿// //  <copyright file="BillingAccountRepository.cs" company="TOTVS">
// //  Copyright (c) TOTVS. All rights reserved.
// //  </copyright>
namespace Thex.EntityFrameworkCore.Repositories
{
    using EFCore.BulkExtensions;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Thex.Domain.Entities;
    using Thex.Domain.Interfaces.Repositories;
    using Thex.Infra.Context;
    using Thex.Kernel;
    using Tnf.EntityFrameworkCore;
    using Tnf.EntityFrameworkCore.Repositories;
    using Microsoft.EntityFrameworkCore;
    using Thex.Dto;

    public class PropertyMealPlanTypeRateRepository : EfCoreRepositoryBase<ThexContext, PropertyMealPlanTypeRate>, IPropertyMealPlanTypeRateRepository
    {
        private readonly IApplicationUser _applicationUser;

        public PropertyMealPlanTypeRateRepository(IDbContextProvider<ThexContext> dbContextProvider, IApplicationUser applicationUser)
            : base(dbContextProvider)
        {
            _applicationUser = applicationUser;
        }

        public void ToggleActive(Guid id)
        {
            PropertyMealPlanTypeRate obj = base.Get(new DefaultGuidRequestDto(id));
            obj.IsActive = !obj.IsActive;
            Context.SaveChanges();
        }

        public void CreateOrUpdate(List<PropertyMealPlanTypeRate> addList, List<PropertyMealPlanTypeRate> updateList)
        {
            if (addList.Count > 0)
                Context.BulkInsert(addList);

            if (updateList.Count > 0)
                Context.BulkUpdate(updateList);

            if (addList.Count > 0 || updateList.Count > 0)
                Context.SaveChanges();
        }
    }
}