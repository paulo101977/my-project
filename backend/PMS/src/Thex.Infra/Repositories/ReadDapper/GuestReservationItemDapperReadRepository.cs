﻿using Dapper;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Thex.Domain.Interfaces.Repositories.ReadDapper;
using Thex.Dto.ExternalRegistration;
using Thex.Kernel;

namespace Thex.Infra.Repositories.ReadDapper
{
    public class GuestReservationItemDapperReadRepository : BaseDapperReadRepository, IGuestReservationItemDapperReadRepository
    {
        protected readonly IApplicationUser _applicationUser;

        public GuestReservationItemDapperReadRepository(
            IApplicationUser applicationUser,
            IConfiguration configuration) 
            : base(configuration)
        {
            _applicationUser = applicationUser;
        }

        protected static string sqlExternalGuestRegistration => @"
                SELECT G.ExternalGuestRegistrationId as Id
                      ,G.GuestReservationItemId
                      ,G.ExternalRegistrationEmail
                      ,G.FirstName
                      ,G.LastName
                      ,G.FullName
                      ,G.SocialName
                      ,G.Email
                      ,G.BirthDate
                      ,G.Gender
                      ,G.OccupationId
                      ,G.PhoneNumber
                      ,G.MobilePhoneNumber
                      ,G.NationalityId
                      ,G.PurposeOfTrip
                      ,G.ArrivingBy
                      ,G.ArrivingFrom
                      ,G.ArrivingFromText
                      ,G.NextDestination
                      ,G.NextDestinationText
                      ,G.AdditionalInformation
                      ,G.HealthInsurance
                      ,G.DocumentTypeId
                      ,G.DocumentInformation
                      ,G.LocationCategoryId
                      ,G.Latitude
                      ,G.Longitude
                      ,G.StreetName
                      ,G.StreetNumber
                      ,G.AdditionalAddressDetails
                      ,G.Neighborhood
                      ,G.CityId
                      ,G.PostalCode
                      ,G.CountryCode
                      ,G.StateId
                      ,G.CountryId
                      ,Ri.ReservationItemUid
					  ,cityt.Name  as CityName
					  ,cityt.LanguageIsoCode  as LanguageIsoCode
					  ,stat.Name as StateName
					  ,ctryt.Name as CountryName
					  ,sta.TwoLetterIsoCode as StateTwoLetterIsoCode
					  ,sta.CountryTwoLetterIsoCode as CountryTwoLetterIsoCode
				      ,Gri.IsChild as IsChild
                  FROM ExternalGuestRegistration G
                  INNER JOIN GuestReservationItem Gri
                  ON G.GuestReservationItemId = Gri.GuestReservationItemId
                  INNER JOIN ReservationItem Ri
                  ON Gri.ReservationItemId = Ri.ReservationItemId
				LEFT JOIN
					CountrySubdivision ctry
				INNER JOIN CountrySubdivisionTranslation ctryt 
					ON ctry.CountrySubdivisionId=ctryt.CountrySubdivisionId
					AND ctry.SubdivisionTypeId=1
				INNER JOIN CountrySubdivision sta
					INNER JOIN CountrySubdivisionTranslation stat 
						ON sta.CountrySubdivisionId=stat.CountrySubdivisionId
						AND sta.SubdivisionTypeId=2 
					ON ctry.CountrySubdivisionId=sta.ParentSubdivisionId 
					AND ctryt.LanguageIsoCode=stat.LanguageIsoCode
				  LEFT JOIN CountrySubdivision city
                  INNER JOIN CountrySubdivisionTranslation cityt 
                  	ON city.CountrySubdivisionId=cityt.CountrySubdivisionId
                  	AND city.SubdivisionTypeId=3 
				  ON stat.CountrySubdivisionId=city.ParentSubdivisionId
				  AND cityt.LanguageIsoCode = stat.LanguageIsoCode
				  ON G.CityId=coalesce(city.CountrySubdivisionId,stat.CountrySubdivisionId)
				  AND G.CountryCode=ctry.CountryTwoLetterIsoCode
                  WHERE G.TenantId = @tenantId ";

        protected static string sqlGuestRegistration => @"
                SELECT 
                    GR.GuestRegistrationId as Id,
                    Ri.ReservationItemUid as ReservationItemUid,
	                COALESCE(GI.GuestName, GR.FirstName) as FirstName,
	                GR.LastName as LastName,
	                GR.FullName as FullName,
	                GR.SocialName as SocialName,
	                GR.OccupationId as OccupationId,
	                GR.PhoneNumber as PhoneNumber,
                    GR.MobilePhoneNumber as MobilePhoneNumber,
	                GR.Nationality as NationalityId,
	                GR.PurposeOfTrip as PurposeOfTrip,
                    GR.ArrivingBy as ArrivingBy,
                    GR.ArrivingFrom as ArrivingFrom,
                    GR.ArrivingFromText as ArrivingFromText,
                    GR.NextDestination as NextDestination,
                    GR.NextDestinationText as NextDestinationText,
                    GR.AdditionalInformation as AdditionalInformation,
                    GR.HealthInsurance as HealthInsurance,
                    ADDRH.LocationCategoryId as LocationCategoryId,
                    ADDRH.latitude as Latitude,
                    ADDRH.longitude as Longitude,
                    ADDRH.StreetNumber as StreetNumber,
                    ADDRH.CityId as CityId,
                    ADDRH.CountryTwoLetterIsoCode as CountryCode,
                    ADDRH.StateID as StateId,
	                ADDRH.STREETNAME as StreetName,
	                ADDRH.CITYNAME as CityName,
					ADDRH.statename as StateName,
	                ADDRH.POSTALCODE as PostalCode,
	                ADDRH.COUNTRYNAME as CountryName,
	                ADDRH.CountryTwoLetterIsoCode as CountryTwoLetterIsoCode,
	                ADDRH.isocodelevel2 AS State,
	                ADDRH.Neighborhood as Neighborhood,
	                ADDRH.AdditionalAddressDetails as AdditionalAddressDetails,
	                COALESCE(GI.GuestEmail,GR.Email) AS Email,
	                ADDRH.CountryId as CountryId,
	                RI.ReservationItemStatusId as ReservationItemStatusId,
	                COALESCE(GI.GuestDocument, GR.DocumentInformation ) AS DocumentInformation,
	                DGT.DocumentTypeId AS DocumentTypeId,
	                DGT.Name AS DocumentTypeName,
	                GR.Gender as Gender,
	                GI.GuestTypeId as GuestTypeId,
	                GR.BirthDate as BirthDate,
	                GI.IsChild as IsChild,
	                GI.IsMain as IsMain,
	                GI.IsIncognito as IsIncognito,
	                COALESCE(ADDRH.Nationality, GI.GuestCitizenship) AS Nationality,
	                RI.ReservationItemCode AS ReservationCode, 
	                ADDRH.Latitude,
	                ADDRH.Longitude,
                  GR.GuestRegistrationId as GuestRegistrationId,
                  GR.ResponsibleGuestRegistrationId as ResponsibleGuestRegistrationId,
                  GR.GuestId as GuestId,     
                  GI.GuestReservationItemId as GuestReservationItemId,
                  RI.ReservationItemId as ReservationItemId,
                  RS.ReservationId as ReservationId,
                  ADDRH.LanguageIsoCode as LanguageIsoCode
	        FROM Reservation RS
	        INNER JOIN ReservationItem RI
	        ON RS.ReservationId = RI.ReservationId
	        INNER JOIN GuestReservationItem GI 
	        ON RI.ReservationItemId=GI.ReservationItemId
	        INNER JOIN GuestRegistration GR
	        ON GI.GuestRegistrationId = GR.GuestRegistrationId
	        LEFT JOIN VWADDRESS ADDRH
	        ON ADDRH.OwnerId=GR.GuestRegistrationId AND ADDRH.LocationCategoryId=4
	        LEFT JOIN DocumentType DGT
	        ON COALESCE(GR.DocumentTypeId, GI.GuestDocumentTypeId) =DGT.DocumentTypeId
            WHERE GI.TenantId = @tenantId ";

        public async Task<List<long>> GetIdListByReservationUidAsync(Guid reservationUid)
        {
            var sql = @"
                        SELECT GRI.GuestReservationItemId
                        FROM GuestReservationItem GRI
                        INNER JOIN ReservationItem RI ON RI.ReservationItemId = GRI.ReservationItemId
                        INNER JOIN Reservation R ON RI.ReservationId = R.ReservationId
                        WHERE R.ReservationUid = @reservationUid
                        AND R.TenantId = @tenantId
						AND not exists (
							SELECT EGR.GuestReservationItemId FROM ExternalGuestRegistration as EGR
							WHERE GRI.GuestReservationItemId = EGR.GuestReservationItemId
						) 
                        ";

            using (SqlConnection conn = new SqlConnection(ConnectionStringName))
                return (await conn.QueryAsync<long>(sql, new { reservationUid, tenantId = _applicationUser.TenantId })).ToList();
        }

        public async Task<ExternalGuestRegistrationDto> GetExternalGuestByGuestReservationItemId(long guestReservationItemId)
        {
            var sql = $"{sqlExternalGuestRegistration} AND G.GuestReservationItemId = @guestReservationItemId ";

            using (SqlConnection conn = new SqlConnection(ConnectionStringName))
            {
                var queryResult = (await conn.QueryAsync<ExternalGuestRegistrationDto>(sql, new { guestReservationItemId, tenantId = _applicationUser.TenantId })).ToList();
                var result = queryResult.FirstOrDefault(e => e.LanguageIsoCode != null && e.LanguageIsoCode.ToUpper() == CultureInfo.CurrentCulture.Name.ToUpper());
                return result ?? queryResult.FirstOrDefault();
            }
        }

        public async Task<GuestRegistrationDapperDto> GetGuestRegistrationByGuestReservationItemId(long guestReservationItemId)
        {
            using (SqlConnection conn = new SqlConnection(ConnectionStringName))
            {
                var sql = $"{sqlGuestRegistration} AND GI.GuestReservationItemId = @guestReservationItemId ";

                var queryResult =  (await conn.QueryAsync<GuestRegistrationDapperDto>(sql, new { guestReservationItemId, tenantId = _applicationUser.TenantId })).ToList();
                var result = queryResult.FirstOrDefault(e => e.LanguageIsoCode != null && e.LanguageIsoCode.ToUpper() == CultureInfo.CurrentCulture.Name.ToUpper());

                return result ?? queryResult.FirstOrDefault();
            }
        }

        public async Task<List<ExternalGuestRegistrationDto>> GetExternalGuestListByReservationItemUid(Guid reservationItemUid)
        {
            var sql = $"{sqlExternalGuestRegistration} AND Ri.ReservationItemUid = @reservationItemUid ";

            var resultList = new List<ExternalGuestRegistrationDto>();

            using (SqlConnection conn = new SqlConnection(ConnectionStringName))
            {
                var queryResult = await conn.QueryAsync<ExternalGuestRegistrationDto>(sql, new { reservationItemUid, tenantId = _applicationUser.TenantId });

                foreach (var result in queryResult.Select(e => e.Id).Distinct())
                {
                    if (!resultList.Any(e => e.Id == result))
                    {
                        var resultTranslated = queryResult.FirstOrDefault(e => e.Id == result && e.LanguageIsoCode != null && e.LanguageIsoCode.ToUpper() == CultureInfo.CurrentCulture.Name.ToUpper());

                        resultList.Add(resultTranslated ?? queryResult.FirstOrDefault(e => e.Id == result));
                    }
                }
            }
            return resultList;
        }

        public async Task<List<GuestRegistrationDapperDto>> GetGuestRegistrationListByReservationItemUid(Guid reservationItemUid)
        {
            var resultList = new List<GuestRegistrationDapperDto>();

            using (SqlConnection conn = new SqlConnection(ConnectionStringName))
            {
                var sql = $"{sqlGuestRegistration} AND RI.ReservationItemUid = @reservationItemUid ";
                var queryResult = (await conn.QueryAsync<GuestRegistrationDapperDto>(sql, new { reservationItemUid, tenantId = _applicationUser.TenantId })).ToList();

                foreach (var result in queryResult.Select(e => e.Id).Distinct())
                {
                    if (!resultList.Any(e => e.Id == result))
                    {
                        var resultTranslated = queryResult.FirstOrDefault(e => e.Id == result && e.LanguageIsoCode != null && e.LanguageIsoCode.ToUpper() == CultureInfo.CurrentCulture.Name);

                        resultList.Add(resultTranslated ?? queryResult.FirstOrDefault(e => e.Id == result));
                    }
                }
            }
            return resultList;
        }
    }
}
