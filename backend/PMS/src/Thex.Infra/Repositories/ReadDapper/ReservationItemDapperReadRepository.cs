﻿using Dapper;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Thex.Common;
using Thex.Common.Enumerations;
using Thex.Domain.Interfaces.Repositories.ReadDapper;
using Thex.Dto.ExternalRegistration;
using Thex.Kernel;
using Tnf.Dto;
using Tnf.Localization;

namespace Thex.Infra.Repositories.ReadDapper
{
    public class ReservationItemDapperReadRepository : BaseDapperReadRepository, IReservationItemDapperReadRepository
    {
        protected readonly IApplicationUser _applicationUser;
        protected readonly ILocalizationManager _localizationManager;

        public ReservationItemDapperReadRepository(
            IApplicationUser applicationUser,
            IConfiguration configuration,
            ILocalizationManager localizationManager) 
            : base(configuration)
        {
            _applicationUser = applicationUser;
            _localizationManager = localizationManager;
        }

        public async Task<DateTime> GetMaxDepartureDateByReservationUidAsync(Guid reservationUid)
        {
            var sql = @"
                        SELECT 
                            MAX(COALESCE(RI.EstimatedDepartureDate, RI.CheckoutDate)) 
                        FROM ReservationItem RI
                        INNER JOIN Reservation R ON RI.ReservationId = r.ReservationId
                        WHERE R.ReservationUid = @reservationUid
                        AND R.TenantId = @tenantId  
                        ";

            using (SqlConnection conn = new SqlConnection(ConnectionStringName))
                return (await conn.QueryAsync<DateTime>(sql, new { reservationUid, tenantId = _applicationUser.TenantId })).First();
        }

        public async Task<ExternalRoomListHeaderDto> GetHeaderByReservationUid(Guid reservationUid)
        {
            var sql = @"
                    select distinct
	                    Adultcount,
	                    Childcount,
	                    MinDate,
	                    MaxDate,
	                    ReservationCode,
                        Name,
	                    PropertyUId,
	                    addrs.CountryTwoLetterIsoCode as CountryCode,
                        addrs.StreetNumber as StreetNumber,
	                    addrs.STREETNAME as StreetName,
	                    addrs.CITYNAME as CityName,
	                    addrs.POSTALCODE as PostalCode,
	                    addrs.COUNTRYNAME as CountryName,
	                    addrs.CountryTwoLetterIsoCode as CountryTwoLetterIsoCode,
	                    addrs.isocodelevel2 AS State,
	                    addrs.Neighborhood as Neighborhood,
	                    addrs.AdditionalAddressDetails as AdditionalAddressDetails,
                        addrs.CityId as CityId,
                        addrs.StateID as StateId,
	                    addrs.CountryId as CountryId,
	                    addrs.Latitude,
	                    addrs.Longitude,
	                    addrs.LanguageIsoCode
                    from (
                     select
	                    sum(ri.adultcount) as Adultcount,
	                    sum(ri.childcount) as Childcount,
	                    min(coalesce(ri.checkindate, ri.estimatedarrivaldate)) as MinDate,
	                    max(coalesce(ri.checkoutdate, ri.estimateddeparturedate)) as MaxDate,
	                    r.reservationcode as ReservationCode,
                        p.Name as Name,
	                    p.PropertyUId as PropertyUId
                        from reservation r 
                        inner join reservationitem ri on r.ReservationId = ri.ReservationId 
                        inner join property p on r.PropertyId = p.PropertyId
                    where 
                          r.ReservationUid = @reservationUid and
	                      r.TenantId = @tenantId
                    group by 	
	                    r.reservationcode,
	                    p.Name,
	                    p.PropertyUId) r
                        left join VWADDRESS addrs
                        ON addrs.OwnerId = r.PropertyUId AND addrs.LocationCategoryId = 1";

            using (SqlConnection conn = new SqlConnection(ConnectionStringName))
            {
                var resultList = (await conn.QueryAsync<ExternalRoomListHeaderDto>(sql, new { reservationUid, tenantId = _applicationUser.TenantId })).ToList();

                return
                    resultList.FirstOrDefault(e => e.LanguageIsoCode != null && e.LanguageIsoCode.ToLower() == CultureInfo.CurrentCulture.Name.ToLower()) ??
                    resultList.FirstOrDefault();
            }
        }

        public async Task<IListDto<ExternalRoomListDto>> GetExternalListByReservationUid(Guid reservationUid)
        {
            IListDto<ExternalRoomListDto> result = new ListDto<ExternalRoomListDto>()
            {
                HasNext = false
            };
            
            var sql = @"
                    select 
		                    r.ReservationUid,
                            ri.ReservationItemUid,
                            ri.ReservationItemStatusId,
                            ro.RoomNumber,
                            coalesce(rt.Name, rtt.Name) as RoomTypeName,
                            rt.Abbreviation as RoomTypeAbbreviation,
                            ri.CheckinDate,
                            ri.CheckoutDate,
                            ri.EstimatedArrivalDate,
                            ri.EstimatedDepartureDate,
                            ri.AdultCount,
                            ri.ChildCount,
                            ri.ReservationItemCode,
                            ri.ReservationItemStatusId,
                            coalesce(egr.FullName, GuestName) as GuestName,
                            egr.FullName as ExternalName
                     from reservationItem ri 
                    inner join reservation r on ri.ReservationId = r.ReservationId
                    inner join GuestReservationItem gri on ri.reservationItemId = gri.reservationItemId
                    inner join ExternalGuestRegistration egr on gri.GuestReservationItemId = egr.GuestReservationItemId 
                    left join room ro on ri.RoomId = ro.RoomId
                    left join RoomType rt on ri.ReceivedRoomTypeId = rt.RoomTypeId
                    left join RoomType rtt on ri.RequestedRoomTypeId = rtt.RoomTypeId
                    where r.ReservationUid = @reservationUid and
	                      r.TenantId = @tenantId";

            using (SqlConnection conn = new SqlConnection(ConnectionStringName))
            {
                 var externalResultList = (await conn.QueryAsync<ExternalRoomListDto>(sql, new { reservationUid, tenantId = _applicationUser.TenantId })).ToList();

                var distinctExternalList = new List<ExternalRoomListDto>();

                foreach (var externalResult in externalResultList)
                    ProcessResult(externalResultList, distinctExternalList, externalResult);

                result.Items = distinctExternalList;
            }

            return result;
        }

        private void ProcessResult(List<ExternalRoomListDto> externalResultList, List<ExternalRoomListDto> distinctExternalList, ExternalRoomListDto externalResult)
        {
            if (!distinctExternalList.Any(e => e.ReservationItemUid == externalResult.ReservationItemUid))
            {
                var searchableNames = new List<string>();
                var isValid = true;

                foreach (var itemSearchableNames in externalResultList
                                .Where(exp => exp.ReservationItemUid == externalResult.ReservationItemUid &&
                                              !string.IsNullOrEmpty(exp.GuestName)))
                {
                    searchableNames.Add(itemSearchableNames.GuestName);

                    if (string.IsNullOrEmpty(itemSearchableNames.ExternalName))
                        isValid = false;
                }

                distinctExternalList.Add(new ExternalRoomListDto
                {
                    ReservationUid = externalResult.ReservationUid,
                    ReservationItemUid = externalResult.ReservationItemUid,
                    ReservationItemCode = externalResult.ReservationItemCode,
                    RoomNumber = externalResult.RoomNumber,
                    RoomTypeName = externalResult.RoomTypeName,
                    RoomTypeAbbreviation = externalResult.RoomTypeAbbreviation,
                    CheckinDate = externalResult.CheckinDate,
                    CheckoutDate = externalResult.CheckoutDate,
                    EstimatedArrivalDate = externalResult.EstimatedArrivalDate,
                    EstimatedDepartureDate = externalResult.EstimatedDepartureDate,
                    IsValid = isValid,
                    StatusId = externalResult.StatusId,
                    SearchableNames = externalResult.SearchableNames = searchableNames,
                    AdultCount = externalResult.AdultCount,
                    ChildCount = externalResult.ChildCount,
                    ReservationItemStatus = _localizationManager.GetString(AppConsts.LocalizationSourceName, ((ReservationStatus)externalResult.StatusId).ToString()),
                });
            }
        }
    }
}
