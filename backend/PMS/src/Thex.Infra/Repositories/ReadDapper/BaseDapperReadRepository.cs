﻿using Microsoft.Extensions.Configuration;

namespace Thex.Infra.Repositories.ReadDapper
{
    public class BaseDapperReadRepository
    {
        protected readonly string ConnectionStringName;
        protected readonly IConfiguration _configuration;

        public BaseDapperReadRepository(IConfiguration configuration)
        {
            _configuration = configuration;

            ConnectionStringName = _configuration["ConnectionStrings:SqlServer"];
        }
    }
}
