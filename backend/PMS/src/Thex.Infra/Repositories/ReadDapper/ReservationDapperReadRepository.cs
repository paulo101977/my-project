﻿using Dapper;
using Microsoft.Extensions.Configuration;
using System;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Thex.Domain.Interfaces.Repositories.ReadDapper;
using Thex.Kernel;

namespace Thex.Infra.Repositories.ReadDapper
{
    public class ReservationDapperReadRepository : BaseDapperReadRepository, IReservationDapperReadRepository
    {
        protected readonly IApplicationUser _applicationUser;

        public ReservationDapperReadRepository(
            IApplicationUser applicationUser,
            IConfiguration configuration) 
            : base(configuration)
        {
            _applicationUser = applicationUser;
        }

        public async Task<bool> AnyByReservationUidAsync(Guid reservationUid)
        {
            var sql = @"
                        SELECT 1 
                        FROM Reservation 
                        WHERE ReservationUid = @reservationUid
                        AND TenantId = @tenantId
                        ";

            using (SqlConnection conexao = new SqlConnection(ConnectionStringName))
                return (await conexao.QueryAsync<object>(sql, new { reservationUid, tenantId = _applicationUser.TenantId })).Any();
        }

        public async Task<bool> AnyByGuestReservationItemIdAsync(Guid reservationUid, long guestReservationItemId)
        {
            var sql = @"
                        SELECT 1 
                        FROM Reservation r
                        INNER JOIN ReservationItem ri 
                        ON r.ReservationId = ri.ReservationId
                        INNER JOIN GuestReservationItem gri 
                        ON ri.ReservationItemId = gri.ReservationItemId
                        WHERE r.ReservationUid = @reservationUid and gri.GuestReservationItemId = @guestReservationItemId
                        AND r.TenantId = @tenantId
                        ";

            using (SqlConnection conn = new SqlConnection(ConnectionStringName))
                return (await conn.QueryAsync<object>(sql, new { reservationUid, guestReservationItemId, tenantId = _applicationUser.TenantId })).Any();
        }
    }
}
