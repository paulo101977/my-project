﻿using System;
using System.Collections.Generic;
using System.Text;
using Thex.Domain.Entities;
using Thex.Domain.Interfaces.Repositories;
using Tnf.EntityFrameworkCore.Repositories;
using Tnf.EntityFrameworkCore;
using Thex.Infra.Context;

namespace Thex.EntityFrameworkCore.Repositories
{
    public class BillingInvoicePropertySupportedTypeRepository : EfCoreRepositoryBase<ThexContext, BillingInvoicePropertySupportedType>, IBillingInvoicePropertySupportedTypeRepository
    {
        public BillingInvoicePropertySupportedTypeRepository(IDbContextProvider<ThexContext> dbContextProvider) : base(dbContextProvider)
        {
        }

        public void CreateRange(List<BillingInvoicePropertySupportedType> entityList)
        {
            Context.BillingInvoicePropertySupportedTypes.AddRange(entityList);
            Context.SaveChanges();
        }

    }
}
