﻿//  <copyright file="RoomTypeRepository.cs" company="TOTVS">
//  Copyright (c) TOTVS. All rights reserved.
//  </copyright>
namespace Thex.EntityFrameworkCore.Repositories
{
    using System.Linq;


    using Thex.Domain.Entities;

    using Microsoft.EntityFrameworkCore;
    using Tnf.EntityFrameworkCore.Repositories;
    using Tnf.EntityFrameworkCore;
    using Thex.Domain.Interfaces.Repositories;
    using Thex.Infra.Context;
    using Thex.Dto;
    using Tnf.Notifications;
    using System.Collections.Generic;
    using Thex.Common;
    using Thex.Common.Enumerations;
    using System;

    public class RoomTypeRepository : EfCoreRepositoryBase<ThexContext, RoomType>, IRoomTypeRepository
    {
        private readonly INotificationHandler Notification;

        public RoomTypeRepository(IDbContextProvider<ThexContext> dbContextProvider, INotificationHandler notificationHandler)
            : base(dbContextProvider)
        {
            Notification = notificationHandler;
        }

        public override RoomType Update(RoomType entity)
        {
            RemoveRoomTypeBedTypePocoFromDatabaseWithRoomTypeId(entity.Id);

            Context.SaveChanges();

            foreach (var roomTypeBedType in entity.RoomTypeBedTypeList)
                Context.RoomTypeBedTypes.Add(roomTypeBedType);

            base.Update(entity);

            return entity;
        }

        public bool ToggleAndSaveIsActive(int id)
        {
            RoomType roomTypePoco = base.Get(new DefaultIntRequestDto(id));
            roomTypePoco.IsActive = !roomTypePoco.IsActive;
            Context.SaveChanges();
            return roomTypePoco.IsActive;
        }

        public void RemoveRoomTypeBedTypePocoFromDatabaseWithRoomTypeId(int roomTypeId)
        {
            try
            {
                var roomTypeBedTypes = Context
              .RoomTypeBedTypes
              .AsNoTracking()
              .Where(exp => exp.RoomTypeId == roomTypeId);

                Context.RoomTypeBedTypes.RemoveRange(roomTypeBedTypes);

            }
            catch
            {

                Context.ChangeTracker.Entries()
                 .Where(e => e.Entity != null).ToList()
                 .ForEach(e => e.State = EntityState.Detached);
                
                Notification.Raise(Notification.DefaultBuilder
                        .WithMessage(AppConsts.LocalizationSourceName, RoomTypeEnum.Error.RoomTypeErrorRemove)
                        .WithDetailedMessage(AppConsts.LocalizationSourceName, RoomTypeEnum.Error.RoomTypeErrorRemove)
                        .Build());
            }
        }

        public List<RoomType> GetAllByPropertyId(int propertyId)
        {
            return Context.RoomTypes
                .Include(r => r.RoomList)
                .Where(r => r.PropertyId == propertyId).ToList();
        }

        public void Delete(int id)
        {
            try
            {
                var roomType = Context
                    .RoomTypes                    
                    .FirstOrDefault(x => x.Id == id);

                Context.Remove(roomType);

                Context.SaveChanges();
              
            }
            catch
            { 
                Notification.Raise(Notification.DefaultBuilder
                        .WithMessage(AppConsts.LocalizationSourceName, RoomTypeEnum.Error.RoomTypeErrorRemove)
                        .WithDetailedMessage(AppConsts.LocalizationSourceName, RoomTypeEnum.Error.RoomTypeErrorRemove)
                        .Build());
            }
        }
    }
}