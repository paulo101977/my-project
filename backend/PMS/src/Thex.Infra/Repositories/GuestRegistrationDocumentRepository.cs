﻿using System;
using System.Collections.Generic;
using System.Text;
using Thex.Domain.Entities;
using Thex.Domain.Interfaces.Repositories;
using Thex.Infra.Context;
using Tnf.EntityFrameworkCore;
using Tnf.EntityFrameworkCore.Repositories;

namespace Thex.Infra.Repositories
{
    public class GuestRegistrationDocumentRepository : EfCoreRepositoryBase<ThexContext, GuestRegistrationDocument>, IGuestRegistrationDocumentRepository
    {
        public GuestRegistrationDocumentRepository(IDbContextProvider<ThexContext> dbContextProvider)
            : base(dbContextProvider)
        {
        }

        public void AddRange(ICollection<GuestRegistrationDocument> guestRegistrationDocumentList)
        {
            Context.AddRange(guestRegistrationDocumentList);

            Context.SaveChanges();
        }

        public void UpdateRange(ICollection<GuestRegistrationDocument> guestRegistrationDocumentList)
        {
            Context.UpdateRange(guestRegistrationDocumentList);

            Context.SaveChanges();
        }

        public void RemoveRange(ICollection<GuestRegistrationDocument> guestRegistrationDocumentList)
        {
            Context.RemoveRange(guestRegistrationDocumentList);

            Context.SaveChanges();
        }
    }
}
