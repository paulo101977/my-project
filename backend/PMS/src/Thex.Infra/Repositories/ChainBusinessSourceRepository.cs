﻿// //  <copyright file="ChainBusinessSourceRepository.cs" company="TOTVS">
// //  Copyright (c) TOTVS. All rights reserved.
// //  </copyright>
namespace Thex.EntityFrameworkCore.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Thex.Domain.Entities;
    using Thex.Domain.Interfaces.Repositories;
    using Thex.Domain.Specifications.RoomType;

    using Tnf.EntityFrameworkCore.Repositories;
    using Tnf.EntityFrameworkCore;
    using Thex.Infra.Context;

    public class ChainBusinessSourceRepository : EfCoreRepositoryBase<ThexContext, ChainBusinessSource>, IChainBusinessSourceRepository
    {
        public ChainBusinessSourceRepository(IDbContextProvider<ThexContext> dbContextProvider)
            : base(dbContextProvider)
        {
        }

        public void Create(ChainBusinessSource chainBusinessSource)
        {
            Context.ChainBusinessSources.Add(chainBusinessSource);
            Context.SaveChanges();
        }
    }
}