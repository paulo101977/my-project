﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using Thex.Domain.Entities;
using Thex.Domain.Interfaces.Repositories;
using Thex.Dto;
using Thex.Infra.Context;
using Tnf.EntityFrameworkCore;
using Tnf.EntityFrameworkCore.Repositories;

namespace Thex.Infra.Repositories
{
    public class PropertyRateStrategyRepository : EfCoreRepositoryBase<ThexContext, PropertyRateStrategy>, IPropertyRateStrategyRepository
    {
        public PropertyRateStrategyRepository(IDbContextProvider<ThexContext> dbContextProvider) : base(dbContextProvider)
        {
        }

        public PropertyRateStrategy GetById(Guid id)
        {
            return Context.PropertyRateStrategies
                .Include(p => p.PropertyRateStrategyRoomTypeList)
                .FirstOrDefault(p => p.Id == id);
        }

        public PropertyRateStrategy Create(PropertyRateStrategy propertyRateStrategy)
        {
            Context.PropertyRateStrategies.Add(propertyRateStrategy);
            Context.SaveChanges();
            return propertyRateStrategy;
        }

        public PropertyRateStrategy Update(PropertyRateStrategy propertyRateStrategy, PropertyRateStrategy existingPropertyRateStrategy)
        {
            if (existingPropertyRateStrategy != null)
            {
                RemovePropertyRateStrategyRoomTypeListFromStrategy(propertyRateStrategy, existingPropertyRateStrategy);

                Context.Entry(existingPropertyRateStrategy).CurrentValues.SetValues(propertyRateStrategy);

                foreach (var propertyRateStrategyRoomType in propertyRateStrategy.PropertyRateStrategyRoomTypeList)
                    InsertOrUpdatePropertyRateStrategyRoomType(propertyRateStrategy, existingPropertyRateStrategy, propertyRateStrategyRoomType);
            }

            Context.SaveChanges();
            return propertyRateStrategy;
        }

        //Vai ter a modal de associação e desassociação de acordos comerciais
        #region Remove PropertyRateStrategyRatePlan
        //
        //private void RemovePropertyRateStrategyRatePlanListFromStrategy(PropertyRateStrategy propertyRateStrategy, PropertyRateStrategy existingPropertyRateStrategy)
        //{
        //    List<Guid> propertyRateStrategyRatePlanIdExceptList = GetPropertyRateStrategyRatePlanIdListFromStrategy(propertyRateStrategy);
        //    RemovePropertyRateStrategyRatePlanListExceptIdList(existingPropertyRateStrategy, propertyRateStrategyRatePlanIdExceptList);
        //}

        //private List<Guid> GetPropertyRateStrategyRatePlanIdListFromStrategy(PropertyRateStrategy propertyRateStrategy)
        //{
        //    return propertyRateStrategy.PropertyRateStrategyRatePlanList.Select(r => r.Id).ToList();
        //}

        //private void RemovePropertyRateStrategyRatePlanListExceptIdList(PropertyRateStrategy existingPropertyRateStrategy, List<Guid> propertyRateStrategyRatePlanIdExceptList)
        //{
        //    var propertyRateStrategyRatePlanListToExclude = existingPropertyRateStrategy.PropertyRateStrategyRatePlanList.Where(exp => !propertyRateStrategyRatePlanIdExceptList.Contains(exp.Id)).ToList();
        //    if (propertyRateStrategyRatePlanListToExclude.Count > 0)
        //        Context.RemoveRange(propertyRateStrategyRatePlanListToExclude);
        //}
        #endregion

        #region Remove PropertyRateStrategyRoomType
        private void RemovePropertyRateStrategyRoomTypeListFromStrategy(PropertyRateStrategy propertyRateStrategy, PropertyRateStrategy existingPropertyRateStrategy)
        {
            List<Guid> propertyRateStrategyRoomTypeIdExceptList = GetPropertyRateStrategyRoomTypeIdListFromStrategy(propertyRateStrategy);
            RemovePropertyRateStrategyRoomTypeListExceptIdList(existingPropertyRateStrategy, propertyRateStrategyRoomTypeIdExceptList);
        }

        private List<Guid> GetPropertyRateStrategyRoomTypeIdListFromStrategy(PropertyRateStrategy propertyRateStrategy)
        {
            return propertyRateStrategy.PropertyRateStrategyRoomTypeList.Select(r => r.Id).ToList();
        }

        private void RemovePropertyRateStrategyRoomTypeListExceptIdList(PropertyRateStrategy existingPropertyRateStrategy, List<Guid> propertyRateStrategyRoomTypeIdExceptList)
        {
            var propertyRateStrategyRoomTypeListToExclude = existingPropertyRateStrategy.PropertyRateStrategyRoomTypeList.Where(exp => !propertyRateStrategyRoomTypeIdExceptList.Contains(exp.Id)).ToList();
            if (propertyRateStrategyRoomTypeListToExclude.Count > 0)
                Context.RemoveRange(propertyRateStrategyRoomTypeListToExclude);
        }
        #endregion

        private void InsertOrUpdatePropertyRateStrategyRoomType(PropertyRateStrategy propertyRateStrategy, PropertyRateStrategy existingPropertyRateStrategy, PropertyRateStrategyRoomType propertyRateStrategyRoomType)
        {
            var existingPropertyRateStrategyRoomType = existingPropertyRateStrategy.PropertyRateStrategyRoomTypeList
                                            .Where(d => d.Id == propertyRateStrategyRoomType.Id)
                                            .FirstOrDefault();

            if (existingPropertyRateStrategyRoomType != null)
            {
                propertyRateStrategyRoomType.PropertyRateStrategyId = propertyRateStrategy.Id;

                Context.Entry(existingPropertyRateStrategyRoomType).CurrentValues.SetValues(propertyRateStrategyRoomType);
            }
            else
                existingPropertyRateStrategy.PropertyRateStrategyRoomTypeList.Add(propertyRateStrategyRoomType);
        }

        public void ToggleAndSaveIsActive(Guid id)
        {
            PropertyRateStrategy propertyRateStrategy = base.Get(new DefaultGuidRequestDto(id));
            if (propertyRateStrategy != null)
            {
                propertyRateStrategy.IsActive = !propertyRateStrategy.IsActive;
                Context.SaveChanges();
            }
        }
    }
}
