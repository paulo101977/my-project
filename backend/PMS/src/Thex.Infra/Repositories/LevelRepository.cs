﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using Thex.Domain.Entities;
using Thex.Domain.Interfaces.Repositories;
using Thex.Dto;
using Thex.Infra.Context;
using Tnf.EntityFrameworkCore;
using Tnf.EntityFrameworkCore.Repositories;

namespace Thex.Infra.Repositories
{
    public class LevelRepository : EfCoreRepositoryBase<ThexContext, Level>, ILevelRepository
    {
        public LevelRepository(IDbContextProvider<ThexContext> dbContextProvider) : base(dbContextProvider)
        {

        }

        public void Create(Level level)
        {
            base.Insert(level);
            Context.SaveChanges();
        }

        public new void Update(Level Level)
        {
            var LevelOld = Context
                .Levels
                .SingleOrDefault(x => x.Id == Level.Id);

            if (LevelOld != null)
            {
                Context.Entry(LevelOld).State = EntityState.Modified;
                Context.Entry(LevelOld).CurrentValues.SetValues(Level);
                Context.SaveChanges();
            }
        }

        public void UpdateRange(ICollection<Level> listLevel)
        {            
            Context.UpdateRange(listLevel);
            Context.SaveChanges();
        }

        public void DeleteLevel(Level Level)
        {
            var LevelOld = Context
                .Levels
                .SingleOrDefault(x => x.Id == Level.Id);

            if (LevelOld != null)
            {
                Context.Entry(LevelOld).State = EntityState.Modified;
                Context.Entry(LevelOld).CurrentValues.SetValues(Level);
                Context.SaveChanges();
            }
        }

        public void ToggleAndSaveIsActive(Guid id)
        {
            Level Level = base.Get(new DefaultGuidRequestDto(id));
            if (Level != null)
            {
                Level.IsActive = !Level.IsActive;
                Context.SaveChanges();
            }
        }
    }
}
