﻿using System;
using Thex.Domain.Entities;
using Thex.Domain.Interfaces.Repositories;
using Thex.Dto;
using Thex.Infra.Context;
using Tnf.EntityFrameworkCore;
using Tnf.EntityFrameworkCore.Repositories;

namespace Thex.Infra.Repositories
{
    public class TourismTaxRepository : EfCoreRepositoryBase<ThexContext, TourismTax>, ITourismTaxRepository
    {
        public TourismTaxRepository(IDbContextProvider<ThexContext> dbContextProvider)
            : base(dbContextProvider)
        {
        }

        public void ToggleActivation(Guid id)
        {
            var entity = base.Get(new DefaultGuidRequestDto(id));
            entity.IsActive = !entity.IsActive;

            Context.SaveChanges();
        }

        public TourismTax Get(Guid id)
        {
            return Context.TourismTaxs.Find(id);
        }

        public override TourismTax Insert(TourismTax entity)
        {
            Context.TourismTaxs.Add(entity);
            return entity;
        }

        public override TourismTax Update(TourismTax entity)
        {
            Context.TourismTaxs.Update(entity);
            return entity;
        }
    }
}
