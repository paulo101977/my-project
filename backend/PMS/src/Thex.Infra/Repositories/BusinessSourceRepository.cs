﻿//  <copyright file="RoomRepository.cs" company="TOTVS">
//  Copyright (c) TOTVS. All rights reserved.
//  </copyright>
namespace Thex.EntityFrameworkCore.Repositories
{
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;

    using Thex.Domain.Entities;

    using Microsoft.EntityFrameworkCore;

    using Tnf.EntityFrameworkCore.Repositories;
    using Tnf.EntityFrameworkCore;
    using Thex.Domain.Interfaces.Repositories;
    using Thex.Infra.ReadInterfaces;
    using Thex.Infra.Context;

    public class BusinessSourceRepository : EfCoreRepositoryBase<ThexContext, BusinessSource>, IBusinessSourceRepository
    {
        

        public BusinessSourceRepository(IDbContextProvider<ThexContext> dbContextProvider)
            : base(dbContextProvider)
        {
            
        }

        public override void Delete(BusinessSource entity)
        {
            RemoveChainBusinessSource(entity.Id);

            base.Delete(entity);

            Context.SaveChanges();
        }

        private void RemoveChainBusinessSource(int businessSourceId)
        {
            var chainBusinessSource = Context.ChainBusinessSources.FirstOrDefault(exp => exp.BusinessSourceId == businessSourceId);

            if (chainBusinessSource != null)
                Context.Remove(chainBusinessSource);
        }


        public void ToggleAndSaveIsActive(int id)
        {
            var businessSource = Context
                .BusinessSources
                .Include(exp => exp.ChainBusinessSourceList)
                .FirstOrDefault(exp => exp.Id == id);

            var toogle = !businessSource.IsActive;

            businessSource.IsActive = toogle;

            foreach (var chainBusinessSource in businessSource.ChainBusinessSourceList)
            {
                chainBusinessSource.IsActive = toogle;
                Context.ChainBusinessSources.Update(chainBusinessSource);
            }

            Context.SaveChanges();
        }
    }
}