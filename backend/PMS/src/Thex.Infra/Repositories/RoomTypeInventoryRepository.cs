﻿using EFCore.BulkExtensions;
using System;
using System.Collections.Generic;
using System.Linq;
using Thex.Domain.Entities;
using Thex.Domain.Interfaces.Repositories;
using Thex.Infra.Context;
using Tnf.EntityFrameworkCore;
using Tnf.EntityFrameworkCore.Repositories;

namespace Thex.Infra.Repositories
{
    public class RoomTypeInventoryRepository : EfCoreRepositoryBase<ThexContext, RoomTypeInventory>, IRoomTypeInventoryRepository
    {
        //private readonly ThexContext _context;

        public RoomTypeInventoryRepository(IDbContextProvider<ThexContext> dbContextProvider)
            : base(dbContextProvider)
        {
        }

        public void CreateList(IList<RoomTypeInventory> roomTypeInventoryList)
        {
            if (roomTypeInventoryList.Any())
                Context.BulkInsert(roomTypeInventoryList);
        }

        public void UpdateList(IList<RoomTypeInventory> roomTypeInventoryList)
        {
            if (roomTypeInventoryList.Any())
                Context.BulkUpdate(roomTypeInventoryList);
        }

        public IEnumerable<RoomTypeInventory> GetAllByPropertyIdAndDate(int propertyId, DateTime date)
        {
            return Context.RoomTypeInventories.Where(r => r.PropertyId == propertyId
                && r.Date.Date == date.Date).ToList();
        }

        public IEnumerable<RoomTypeInventory> GetAllByRoomTypeIdAndDate(int roomTypeId, DateTime date)
        {
            return Context.RoomTypeInventories.Where(r => r.RoomTypeId == roomTypeId
                && r.Date.Date >= date.Date).ToList();
        }

        public IEnumerable<RoomTypeInventory> GetAllByRoomTypeIdAndPeriod(int roomTypeId, DateTime initialDate, DateTime endDate)
        {
            return Context.RoomTypeInventories.Where(r => r.RoomTypeId == roomTypeId
                && r.Date.Date >= initialDate.Date && r.Date.Date <= endDate.Date).ToList();
        }

        public IEnumerable<RoomTypeInventory> GetAllByRoomTypeIdListAndPeriod(IEnumerable<int> roomTypeIdList, DateTime initialDate, DateTime endDate)
        {
            return Context.RoomTypeInventories.Where(r => roomTypeIdList.Contains(r.RoomTypeId)
                && r.Date.Date >= initialDate.Date && r.Date.Date <= endDate.Date).ToList();
        }

        public RoomTypeInventory GetLastByRoomTypeId(int roomTypeId)
        {
            return Context.RoomTypeInventories.Where(r => r.RoomTypeId == roomTypeId)
                .OrderByDescending(r => r.Date).FirstOrDefault();
        }

        public IEnumerable<RoomTypeInventory> GetAllByPeriod(DateTime initialDate, DateTime endDate)
        {
            return Context.RoomTypeInventories.Where(r => r.Date.Date >= initialDate.Date && r.Date.Date <= endDate.Date).ToList();
        }
    }
}
