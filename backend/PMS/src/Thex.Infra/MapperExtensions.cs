﻿using Thex.Infra.MapperProfiles;
using Microsoft.Extensions.DependencyInjection;
using Tnf.Localization;

namespace Thex.Infra
{
    public static class MapperExtensions
    {


        public static IServiceCollection AddMapperDependency(this IServiceCollection services)
        {
            // Configura o uso do AutoMappper
            return services.AddTnfAutoMapper(config =>
            {
                config.AddProfile(new DomainToDtoProfile());
            });
        }
    }
}