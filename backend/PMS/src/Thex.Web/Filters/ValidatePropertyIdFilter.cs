﻿using Microsoft.AspNetCore.Mvc.Controllers;
using System.Linq;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Authentication;
using Thex.Kernel;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Mvc;
using Thex.AspNetCore.Security.Interfaces;

namespace Thex.Web
{
    public class ValidatePropertyIdFilter : ActionFilterAttribute
    {
        private const string _tokenPropertyIdName = "PropertyId";
        private string _propertyIdName = _tokenPropertyIdName;

        public ValidatePropertyIdFilter() { }

        public ValidatePropertyIdFilter(string propertyIdName) { _propertyIdName = propertyIdName; }

        public override void OnActionExecuting(ActionExecutingContext context)
        {
            var _tokenManager = ResolveTokenManager(context);
            var authorized = true;

            object propertyId = null;

            propertyId = GetLoggedPropertyId(context, _tokenManager, propertyId);

            if (IsValidRequestMethod(context))
            {
                foreach (ControllerParameterDescriptor param in context.ActionDescriptor.Parameters)
                {
                    context.ActionArguments.TryGetValue(param.Name, out var modelValue);

                    if (modelValue == null)
                        continue;

                    if (param.Name.ToLower() == _propertyIdName.ToLower())
                    {
                        if (!IsValid(modelValue.ToString(), propertyId.ToString()))
                            authorized = false;
                    }
                    else
                    {
                        var type = modelValue.GetType();

                        var propertyIdList = type.GetProperties().Where(x => x.Name.ToLower().Equals("propertyid"));

                        foreach (var prop in propertyIdList)
                        {
                            var value = prop.GetValue(modelValue);

                            if (value == null)
                                continue;

                            if (!IsValid(value.ToString(), propertyId.ToString()))
                                authorized = false;
                        }
                    }
                }

                if (!authorized)
                    context.Result = new UnauthorizedResult();
            }

        }

        private bool IsValid(string v, string v2)
        {
            return v == v2 || v == "0";
        }

        private bool IsValidRequestMethod(ActionExecutingContext context)
        {
            var methods = new string[] { "POST", "DELETE", "PATCH", "PUT" }.ToList();
            var request = context.HttpContext.Request;
            var currentMethod = request.Method.ToUpper();

            var isValidMethod = methods.Contains(currentMethod);
            return isValidMethod;
        }

        private object GetLoggedPropertyId(ActionExecutingContext context, ITokenManager _tokenManager, object propertyId)
        {
            var accessToken = context.HttpContext.GetBearerToken();

            var validToken = _tokenManager.VerifyToken(accessToken);

            if (validToken != null)
                validToken.Payload.TryGetValue(_tokenPropertyIdName, out propertyId);
            return propertyId;
        }

        private ITokenManager ResolveTokenManager(ActionExecutingContext context)
        {
            return context.HttpContext.RequestServices.GetService<ITokenManager>();
        }
    }
}
