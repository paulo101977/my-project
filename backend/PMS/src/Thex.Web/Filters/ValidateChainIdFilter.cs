﻿using Microsoft.AspNetCore.Mvc.Controllers;
using System.Linq;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Authentication;
using Thex.Kernel;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Mvc;
using Thex.AspNetCore.Security.Interfaces;

namespace Thex.Web
{
    public class ValidateChainIdFilter : ActionFilterAttribute
    {
        private const string _tokenChainIdName = "ChainId";
        private string _chainIdName = _tokenChainIdName;

        public ValidateChainIdFilter() { }

        public ValidateChainIdFilter(string chainIdName) { _chainIdName = chainIdName; }

        public override void OnActionExecuting(ActionExecutingContext context)
        {
            var _tokenManager = ResolveTokenManager(context);
            var authorized = true;

            object chainId = null;

            chainId = GetLoggedChainId(context, _tokenManager, chainId);

            if (IsValidRequestMethod(context))
            {
                foreach (ControllerParameterDescriptor param in context.ActionDescriptor.Parameters)
                {
                    context.ActionArguments.TryGetValue(param.Name, out var modelValue);

                    if (modelValue == null)
                        continue;

                    if (param.Name.ToLower() == _chainIdName.ToLower())
                    {
                        if (!IsValid(modelValue.ToString(), chainId.ToString()))
                            authorized = false;
                    }
                    else
                    {
                        var type = modelValue.GetType();

                        var chainIdList = type.GetProperties().Where(x => x.Name.ToLower().Equals("chainid"));

                        foreach (var prop in chainIdList)
                        {
                            var value = prop.GetValue(modelValue);

                            if (value == null)
                                continue;

                            if (!IsValid(value.ToString(), chainId.ToString()))
                                authorized = false;
                        }
                    }
                }

                if (!authorized)
                    context.Result = new UnauthorizedResult();
            }

        }

        private bool IsValid(string v, string v2)
        {
            return v == v2 || v == "0";
        }

        private bool IsValidRequestMethod(ActionExecutingContext context)
        {
            var methods = new string[] { "POST", "DELETE", "PATCH", "UPDATE" }.ToList();
            var request = context.HttpContext.Request;
            var currentMethod = request.Method;

            var isValidMethod = methods.Contains(currentMethod);
            return isValidMethod;
        }

        private object GetLoggedChainId(ActionExecutingContext context, ITokenManager _tokenManager, object chainId)
        {
            var accessToken = context.HttpContext.GetTokenAsync("access_token").ConfigureAwait(false).GetAwaiter().GetResult();
            var validToken = _tokenManager.VerifyToken(accessToken);

            if (validToken != null)
                validToken.Payload.TryGetValue(_tokenChainIdName, out chainId);
            return chainId;
        }

        private ITokenManager ResolveTokenManager(ActionExecutingContext context)
        {
            return context.HttpContext.RequestServices.GetService<ITokenManager>();
        }
    }
}
