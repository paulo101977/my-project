﻿using Microsoft.AspNetCore.Mvc.Internal;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using NodaTime;
using System;
using System.Globalization;
using System.Threading.Tasks;

namespace Thex.Web.ModelBinding
{
    public class TimeZoneModelBinder : IModelBinder
    {
        public TimeZoneModelBinder()
        {
        }

        public Task BindModelAsync(ModelBindingContext bindingContext)
        {
            if (bindingContext == null)
            {
                throw new ArgumentNullException(nameof(bindingContext));
            }

            var valueProviderResult = bindingContext.ValueProvider
              .GetValue(bindingContext.ModelName);

            if (string.IsNullOrEmpty(valueProviderResult.FirstValue))
                return Task.CompletedTask;

            DateTime datetime;

            //if (DateTime.TryParse(valueProviderResult.FirstValue, null, DateTimeStyles.AdjustToUniversal, out datetime))
            if (DateTime.TryParse(valueProviderResult.FirstValue, CultureInfo.CurrentCulture, DateTimeStyles.AdjustToUniversal, out datetime))
            {
                bindingContext.Result = ModelBindingResult.Success(DateTime.SpecifyKind(datetime, DateTimeKind.Unspecified).ToUniversalTime());
            }
            else
            {
                // TODO: [Enhancement] Could be implemented in better way.  
                bindingContext.ModelState.TryAddModelError(
                    bindingContext.ModelName,
                    bindingContext.ModelMetadata
                    .ModelBindingMessageProvider.AttemptedValueIsInvalidAccessor(
                      valueProviderResult.ToString(), nameof(DateTime)));
            }

            return Task.CompletedTask;
        }
    }
}