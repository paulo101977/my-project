﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using System;

namespace Thex.Web.ModelBinding
{
    public class TimeZoneModelBinderProvider : IModelBinderProvider
    {
        public TimeZoneModelBinderProvider()
        {
        }

        public IModelBinder GetBinder(ModelBinderProviderContext context)
        {
            if (context == null)
            {
                throw new ArgumentNullException(nameof(context));
            }

            if (context.Metadata.UnderlyingOrModelType == typeof(DateTime))
            //if (!context.Metadata.IsComplexType && context.Metadata.ModelType == typeof(DateTime))
            {
                return new TimeZoneModelBinder();
            }
            return null;
        }
    }
}
