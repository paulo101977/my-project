﻿using Thex.Application;
using Thex.Domain;
using Thex.Infra;
using Thex.Infra.SqlServer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.IO;
using System.Threading.Tasks;
using Tnf.Configuration;
using Thex.Kernel;
using Microsoft.Extensions.Options;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Thex.Domain.Entities;
using Thex.Dto;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.AspNetCore.ResponseCompression;
using System.IO.Compression;
using Thex.Common;
using Thex.AspNetCore.Security;
using Thex.AspNetCore.Filters;
using Thex.Inventory;
using Thex.Maps.Geocode;

namespace Thex.Web
{
    public class Startup
    {
        DatabaseConfiguration DatabaseConfiguration { get; }
        private IConfiguration Configuration { get; set; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
            DatabaseConfiguration = new DatabaseConfiguration(configuration);
        }

        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            services.Configure<GzipCompressionProviderOptions>(
                options => options.Level = CompressionLevel.Optimal);                  

            services
                .AddThexGenericLog()
                .AddCorsAll("AllowAll")
                .AddPMSApplicationServiceDependency(options =>
                {
                    var settingsSection = Configuration.GetSection("MapsGeocodeConfig");
                    var settings = settingsSection.Get<MapsGeocodeConfig>();                   
                })
                .AddTnfAspNetCore()
                .AddInventoryDependency(Configuration)
                .AddSqlServerDependency()
                .AddResponseCompression(options =>
                {
                    options.Providers.Add<GzipCompressionProvider>();
                    options.EnableForHttps = true;
                });

            services
                .AddThexAspNetCoreSecurity(options =>
                {
                    var settingsSection = Configuration.GetSection("TokenConfiguration");
                    var settings = settingsSection.Get<AspNetCore.Security.TokenConfiguration>();

                    options.TokenConfiguration = settings;
                    options.FrontEndpoint = Configuration.GetValue<string>("UrlFront");
                    options.SuperAdminEndpoint = Configuration.GetValue<string>("SuperAdminEndpoint");
                });

            services.AddSingleton(Configuration.GetSection("ServicesConfiguration").Get<ServicesConfiguration>());           

            services.AddScoped<ActionLogFilter>();

            services.AddSwaggerDocumentation();

            services.AddAntiforgery(options =>
            {
                options.Cookie.Name = "X-CSRF-TOKEN-GOTNEXT-COOKIE";
                options.HeaderName = "X-CSRF-TOKEN-GOTNEXT-HEADER";
                options.SuppressXFrameOptionsHeader = false;
            });

            services.AddMvc()
                .AddJsonOptions(opts =>
                {
                    opts.SerializerSettings.NullValueHandling =
                        Newtonsoft.Json.NullValueHandling.Ignore;
                        
                    opts.SerializerSettings.ReferenceLoopHandling =
                        Newtonsoft.Json.ReferenceLoopHandling.Ignore;
                });

            services.AddSingleton(Configuration.GetSection("ServicesConfiguration").Get<ServicesConfiguration>());

            var serviceProvider = services.BuildServiceProvider();

            ServiceLocator.SetLocatorProvider(serviceProvider);

            return serviceProvider;
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseCors("AllowAll");

            // Configura o use do AspNetCore do Tnf
            app.UseTnfAspNetCore(options =>
            {
                // Adiciona as configurações de localização da aplicação
                options.UseDomainLocalization();

                options.Repository(repositoryConfig =>
                {
                    repositoryConfig.Entity<IEntityGuid>(entity =>
                        entity.RequestDto<IDefaultGuidRequestDto>((e, d) => e.Id == d.Id));

                    repositoryConfig.Entity<IEntityInt>(entity =>
                        entity.RequestDto<IDefaultIntRequestDto>((e, d) => e.Id == d.Id));

                    repositoryConfig.Entity<IEntityLong>(entity =>
                        entity.RequestDto<IDefaultLongRequestDto>((e, d) => e.Id == d.Id));
                });

                // Configura a connection string da aplicação
                options.DefaultNameOrConnectionString = DatabaseConfiguration.ConnectionString;
                options.DefaultPageSize(1000, 1000);

            });

            if (env.IsDevelopment())
                app.UseDeveloperExceptionPage();

            
            //app.UseTnfUnitOfWork();

            app.UseSwaggerDocumentation();

            app.UseThexAspNetCoreSecurity();

            app.UseMvcWithDefaultRoute();

            app.UseResponseCompression();

            app.Run(context =>
            {
                context.Response.Redirect("/swagger");
                return Task.CompletedTask;
            });
        }
    }
}
