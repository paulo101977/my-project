﻿using System;
using Thex.Application.Interfaces;
using Thex.Dto;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Thex.Domain;
using Thex.Dto.BillingItem;
using Thex.Web.Base;
using Thex.Kernel;
using Tnf.AspNetCore.Mvc.Response;
using Tnf.Dto;

namespace Thex.Web.Controllers
{
    [Route(RouteConsts.BillingItemPDV)]
    public class BillingItemPDVController : ThexAppController
    {
        private readonly IBillingItemPDVAppService BillingItemPDVAppService;

        public BillingItemPDVController(
            IApplicationUser applicationUser,
            IBillingItemPDVAppService billingItemAppService) : base(applicationUser)
        {
            BillingItemPDVAppService = billingItemAppService;
        }

      
        [HttpGet("{billingItemId}/property/{propertyId}/billingitem")]
        [ThexAuthorize("PMS_BillingItemPDV_Get_Params_BillingItem")]
        [ProducesResponseType(typeof(BillingItemServiceDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult GetBillingItem(int billingItemId, long propertyId)
        {
            var response = BillingItemPDVAppService.GetBillingItem(billingItemId, propertyId);
            return CreateResponseOnGet(response, EntityNames.BillingItem);
        }


        [HttpDelete("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Delete(int id)
        {
            await BillingItemPDVAppService.Delete(id);

            return CreateResponseOnDelete(EntityNames.BillingItem);
        }

        [HttpDelete("{id}/property/{propertyId}")]
        [ThexAuthorize("PMS_BillingItemPDV_Delete_Params")]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public  IActionResult Delete(int id,long propertyId)
        {
            BillingItemPDVAppService.Delete(id, propertyId);

            return CreateResponseOnDelete(EntityNames.BillingItem);
        }

        [HttpPost("createservicewithtax")]
        [ThexAuthorize("PMS_BillingItemPDV_Post_CreateServiceWithTax")]
        [ValidatePropertyIdFilter]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult CreateServiceAssociateTax([FromBody] BillingItemServiceDto dto)
        {
            BillingItemPDVAppService.CreateServiceAssociateTax(dto);

            return CreateResponseOnPost(null, EntityNames.BillingItem);

        }

        [HttpPost("createtaxwithservice")]
        [ValidatePropertyIdFilter]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult CreateTaxAssociateService([FromBody] BillingItemTaxDto dto)
        {
            BillingItemPDVAppService.CreateTaxAssociateService(dto);

            return CreateResponseOnPost(null, EntityNames.BillingItem);

        }

        [HttpGet("{propertyId}/getallbillingitemservice")]
        [ThexAuthorize("PMS_BillingItemPDV_Get_GetAllBillingItemService")]
        [ProducesResponseType(typeof(IListDto<GetAllBillingItemServiceDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult GetAllBillingItemService(long propertyId)
        {
            var response = BillingItemPDVAppService.GetAllBillingItemService(propertyId);
            return CreateResponseOnGetAll(response, EntityNames.BillingItem);

        }

        [HttpGet("{propertyId}/getallbillingitemtax")]
        [ProducesResponseType(typeof(IListDto<GetAllBillingItemTaxDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult GetAllBillingItemTax(long propertyId)
        {
            var response = BillingItemPDVAppService.GetAllBillingItemTax(propertyId);
            return CreateResponseOnGetAll(response, EntityNames.BillingItem);
        }

        [HttpGet("{propertyId}/getAllbillingitemforassociatetax")]
        [ProducesResponseType(typeof(IListDto<GetAllBillingItemForAssociateTaxDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult GetAllBillingItemForAssociateTax(long propertyId)
        {
            var response = BillingItemPDVAppService.GetAllBillingItemForAssociateTax(propertyId);
            return CreateResponseOnGetAll(response, EntityNames.BillingItem);
        }

        [HttpGet("{propertyId}/getAllbillingitemforassociateservice")]
        [ProducesResponseType(typeof(IListDto<GetAllBillingItemForAssociateServiceDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult GetAllBillingItemForAssociateService(long propertyId)
        {
            var response = BillingItemPDVAppService.GetAllBillingItemForAssociateService(propertyId);
            return CreateResponseOnGetAll(response, EntityNames.BillingItem);

        }

        [HttpPut("updatebillingitemservicewithtax")]
        [ThexAuthorize("PMS_BillingItemPDV_Update_UpdateBillingItemServiceWithTax")]
        [ValidatePropertyIdFilter]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult UpdateBillingItemServicesWithTax([FromBody] BillingItemServiceDto serviceDto)
        {
            BillingItemPDVAppService.UpdateBillingItemServiceWithTax(serviceDto);

            return CreateResponseOnPut(null, EntityNames.BillingItem);
        }

        [HttpPut("updatebillingitemtaxwithservice")]
        [ValidatePropertyIdFilter]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult UpdateBillingItemTaxWithService([FromBody] BillingItemServiceDto serviceDto)
        {
            BillingItemPDVAppService.UpdateBillingItemTaxWithService(serviceDto);

            return CreateResponseOnPut(null, EntityNames.BillingItem);
        }
        
        [HttpPatch("{id}/toggleactivation")]
        [ThexAuthorize("PMS_BillingItemPDV_Patch_ToggleActivation")]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult ToggleActivation(int id)
        {
            BillingItemPDVAppService.ToggleActivation(id);

            return CreateResponseOnPatch(null, EntityNames.BillingItem);
        }

        #region PropertyPaymentTypes

        [HttpGet("{propertyId}/{paymentTypeId}/{acquirerId}/getpaymenttype")]
        [ProducesResponseType(typeof(BillingItemPaymentTypeDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public virtual IActionResult GetPaymentType(int propertyId, int paymentTypeId, Guid acquirerId)
        {
            var response = BillingItemPDVAppService.GetByPaymentTypeIdAndAcquirerId(propertyId, paymentTypeId, acquirerId);

            return CreateResponseOnGet(response, EntityNames.BillingItem);
        }

        [HttpGet("{propertyId}/{paymentTypeId}/getpaymenttype")]
        [ProducesResponseType(typeof(BillingItemPaymentTypeDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public virtual IActionResult GetPaymentType(int propertyId, int paymentTypeId)
        {
            var response = BillingItemPDVAppService.GetByPaymentTypeId(propertyId, paymentTypeId);

            return CreateResponseOnGet(response, EntityNames.BillingItem);
        }

        [HttpPut("paymenttype")]
        [ValidatePropertyIdFilter]
        [ProducesResponseType(typeof(BillingItemPaymentTypeDto), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult Put([FromBody] BillingItemPaymentTypeDto dto)
        {
            var response = BillingItemPDVAppService.UpdatePropertyPaymentType(dto);

            return CreateResponseOnPut(response, EntityNames.BillingItem);
        }
        
        [HttpPost("paymenttype")]
        [ValidatePropertyIdFilter]
        [ProducesResponseType(typeof(BillingItemPaymentTypeDto), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult Post([FromBody] BillingItemPaymentTypeDto dto)
        {
            var response = BillingItemPDVAppService.CreatePropertyPaymentType(dto);

            return CreateResponseOnPost(response, EntityNames.BillingItem);
        }


        [HttpGet("{propertyId}/allpaymenttypes")]
        [ProducesResponseType(typeof(IListDto<BillingItemPaymentTypeDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public virtual IActionResult GetAllPaymentTypes(int propertyId)
        {
            var response = BillingItemPDVAppService.GetAllPaymentTypeIdByPropertyId(propertyId);

            return CreateResponseOnGetAll(response, EntityNames.BillingItem);
        }

        [HttpDelete("{propertyId}/{paymentTypeId}/{acquirerId}/paymenttype")]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult DeletePaymentType(int propertyId, int paymentTypeId, Guid acquirerId)
        {
            BillingItemPDVAppService.DeletePaymentType(propertyId, paymentTypeId, acquirerId);

            return CreateResponseOnDelete(EntityNames.BillingItem);
        }

        [HttpPatch("{propertyId}/{paymentTypeId}/{acquirerId}/{activate}/paymenttype")]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult ToggleActivationPaymentType(int propertyId, int paymentTypeId, Guid acquirerId, bool activate)
        {
            BillingItemPDVAppService.ToggleActivationPaymentType(propertyId, paymentTypeId, acquirerId, activate);

            return CreateResponseOnPatch(null, EntityNames.BillingItem);
        }


        #endregion
        [HttpGet("{propertyId}/getallserviceforrealese")]
        [ProducesResponseType(typeof(IListDto<BillingItemServiceLancamentoDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult GetllAllBillingItemServiceForRealese([FromQuery] GetAllBillingItemServiceForRealase dto, int propertyId)
        {
            var response = BillingItemPDVAppService.GetAllServiceWithAssociateTaxForDebitRealease(propertyId, dto);

            return CreateResponseOnGetAll(response, EntityNames.BillingItem);
        }

        [HttpGet("{paymentTypeId}/plasticbrand")]
        [ProducesResponseType(typeof(IListDto<BillingItemPlasticBrandCompanyClientDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult GetPlasticBrandWithCompanyClientByPaymentType(int paymentTypeId)
        {
            var response = BillingItemPDVAppService.GetPlasticBrandWithCompanyClientByPaymentType(paymentTypeId);

            return CreateResponseOnGetAll(response, EntityNames.BillingItem);
        }

        [HttpGet("paymenttypes")]
        [ProducesResponseType(typeof(IListDto<PaymentTypeWithBillingItemDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult GetPaymentTypes([FromQuery] GetAllPaymentTypeDto requestDto)
        {
            var response = BillingItemPDVAppService.GetPaymentTypes(requestDto);

            return CreateResponseOnGetAll(response, EntityNames.BillingItem);
        }
        [HttpGet("{propertyId}/billingitemforparametersdocuments")]
        [ProducesResponseType(typeof(IListDto<BillingItemWithParametersDocumentsDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public virtual IActionResult GetAllServiceWithParametersDocuments(int propertyId)
        {
            var response = BillingItemPDVAppService.GetAllServiceWithParameters(propertyId);

            return CreateResponseOnGetAll(response, EntityNames.BillingItem);
        }
    }
}

