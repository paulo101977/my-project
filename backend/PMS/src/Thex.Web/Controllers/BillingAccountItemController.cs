﻿using System;
using Thex.Application.Interfaces;
using Thex.Dto;
using Microsoft.AspNetCore.Mvc;
using Thex.Domain;
using System.Collections.Generic;
using Thex.Dto.BillingAccountItem;
using Thex.Web.Base;
using Thex.Kernel;
using Tnf.AspNetCore.Mvc.Response;
using Thex.Domain.Entities;

namespace Thex.Web.Controllers
{
    [Route(RouteConsts.BillingAccountItem)]
    public class BillingAccountItemController : ThexAppController
    {
        protected readonly IBillingAccountItemAppService BillingAccountItemAppService;

        public BillingAccountItemController(
            IApplicationUser applicationUser,
            IBillingAccountItemAppService billingAccountItemAppService) : base(applicationUser)
        {
            BillingAccountItemAppService = billingAccountItemAppService;
        }

        [HttpPost("debit")]
        [ThexAuthorize("PMS_BillingAccountItem_Post_Debit")]
        [ValidatePropertyIdFilter]
        [ProducesResponseType(typeof(BillingAccountItemDebitDto), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult PostDebit([FromBody] BillingAccountItemDebitDto dto)
        {
            var response = BillingAccountItemAppService.CreateDebit(dto);

            return CreateResponseOnPost(response, EntityNames.BillingAccountItem);
        }

        [HttpPost("credit")]
        [ThexAuthorize("PMS_BillingAccountItem_Post_Credit")]
        [ValidatePropertyIdFilter]
        [ProducesResponseType(typeof(BillingAccountItemCreditDto), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult PostCredit([FromBody] BillingAccountItemCreditDto dto)
        {
            var response = BillingAccountItemAppService.CreateCredit(dto);

            return CreateResponseOnPost(response, EntityNames.BillingAccountItem);
        }

        [HttpPost("reversal")]
        [ThexAuthorize("PMS_BillingAccountItem_Post_Reversal")]
        [ProducesResponseType(typeof(BillingAccountItemReversalParentDto), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult PostReversal([FromBody] BillingAccountItemReversalParentDto dto)
        {
            var response = BillingAccountItemAppService.CreateReversal(dto);

            return CreateResponseOnPost(response, EntityNames.BillingAccountItem);
        }

        [HttpPost("transfer")]
        [ThexAuthorize("PMS_BillingAccountItem_Post_Transfer")]
        [ProducesResponseType(typeof(BillingAccountItemTransferSourceDestinationDto), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult PostTransfer([FromBody] BillingAccountItemTransferSourceDestinationDto dto)
        {
            var response = BillingAccountItemAppService.CreateTransfer(dto);

            return CreateResponseOnPost(response, EntityNames.BillingAccountItem);
        }

        [HttpPost("undotransfer")]
        [ThexAuthorize("PMS_BillingAccountItem_Post_UndoTransfer")]
        [ProducesResponseType(typeof(BillingAccountItemUndoTransferSourceDestinationDto), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult UndoTransfer([FromBody] BillingAccountItemUndoTransferSourceDestinationDto dto)
        {
            var response = BillingAccountItemAppService.UndoTransfer(dto);

            return CreateResponseOnPost(response, EntityNames.BillingAccountItem);
        }

        [HttpPost("discount")]
        [ThexAuthorize("PMS_BillingAccountItem_Post_Discount")]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult Discount([FromBody] IList<BillingAccountItemDiscountDto> billingAccountItemsDiscountDto )
        {
            BillingAccountItemAppService.CreateBillingAccountItemDiscountList(billingAccountItemsDiscountDto);

            return CreateResponseOnPost(null, EntityNames.BillingAccountItem);
        }

        [HttpPost("partial")]
        [ThexAuthorize("PMS_BillingAccountItem_Post_Partial")]
        [ProducesResponseType(typeof(BillingAccountItemPartialDto),  200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult Partial([FromBody] BillingAccountItemPartialDto dto)
        {
            var response = BillingAccountItemAppService.CreatePartial(dto);

            return CreateResponseOnPost(response, EntityNames.BillingAccountItem);
        }

        [HttpPost("undopartial")]
        [ThexAuthorize("PMS_BillingAccountItem_Post_UndoPartial")]
        [ValidatePropertyIdFilter]
        [ProducesResponseType(typeof(BillingAccountItemUndoPartialDto), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult UndoPartial([FromBody] BillingAccountItemUndoPartialDto dto)
        {
            var response = BillingAccountItemAppService.UndoPartial(dto);

            return CreateResponseOnPost(response, EntityNames.BillingAccountItem);
        }

        [HttpGet("getallbillingaccountitemcreditamountbyinvoiceid/{billingInvoiceId}")]
        [ThexAuthorize("PMS_BillingInvoice_Get_All_Credit_Note")]
        [ProducesResponseType(typeof(List<BillingAccountItemDto>), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult GetAllBillingAccountItemCreditAmountByInvoiceId(Guid billingInvoiceId)
        {
            var response = BillingAccountItemAppService.GetAllBillingAccountItemCreditAmountByInvoiceId(billingInvoiceId);

            return CreateResponseOnGet(response, EntityNames.BillingAccountItem);
        }
    }
}
