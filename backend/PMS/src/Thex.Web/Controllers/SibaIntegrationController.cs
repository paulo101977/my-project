﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Thex.Application.Interfaces;
using Thex.Domain;
using Thex.Dto;
using Thex.Kernel;
using Thex.Web.Base;
using Tnf.AspNetCore.Mvc.Response;

namespace Thex.Web.Controllers
{
    [Route(RouteConsts.SibaIntegration)]
    public class SibaIntegrationController : ThexAppController
    {
        private readonly ISibaIntegrationAppService _sibaIntegrationAppService;

        public SibaIntegrationController(
            IApplicationUser applicationUser,
            ISibaIntegrationAppService sibaIntegrationAppService)
            : base(applicationUser)
        {
            _sibaIntegrationAppService = sibaIntegrationAppService;
        }

        [HttpPost("send")]
        [ThexAuthorize("PMS_SibaIntegration_Post")]
        [ValidatePropertyIdFilter]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Send([FromBody] SibaIntegrationRequestDto dto)
        {
            await _sibaIntegrationAppService.Send(dto);

            return CreateResponseOnPost(dto, EntityNames.SibaIntegration);
        }
    }
}
