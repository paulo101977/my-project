﻿using Thex.Application.Interfaces;
using Thex.Dto;
using Microsoft.AspNetCore.Mvc;
using Thex.Domain;
using Thex.Common;

namespace Thex.Web.Controllers
{
    using System;
    using Thex.Kernel;
    using Thex.Dto.PlasticBrandProperty;
    using Thex.Web.Base;
    using Tnf.AspNetCore.Mvc.Response;
    using Tnf.Dto;

    [Route(RouteConsts.PlasticBrandProperty)]
    public class PlasticBrandPropertyController : ThexAppController
    {
        protected readonly IPlasticBrandPropertyAppService PlasticBrandPropertyAppService;

        public PlasticBrandPropertyController(
            IApplicationUser applicationUser,
            IPlasticBrandPropertyAppService plasticBrandPropertyAppService) : base(applicationUser)
        {
            LocalizationSourceName = AppConsts.LocalizationSourceName;
            PlasticBrandPropertyAppService = plasticBrandPropertyAppService;
        }

        [HttpGet]
        [ThexAuthorize("PMS_PlasticBrandProperty_Get")]
        [ProducesResponseType(typeof(IListDto<PlasticBrandPropertyResultDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public virtual IActionResult Get([FromQuery] GetAllPlasticBrandPropertyDto requestDto)
        {
            var response = PlasticBrandPropertyAppService.GetAllByPropertyId(requestDto);

            return CreateResponseOnGetAll(response, EntityNames.PaymentType);
        }
    }
}


