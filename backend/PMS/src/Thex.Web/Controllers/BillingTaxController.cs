﻿using System;
using Thex.Application.Interfaces;
using Thex.Dto;
using Tnf.Dto;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Thex.Kernel;
using Thex.Web.Base;
using Thex.Domain;
using Tnf.AspNetCore.Mvc.Response;

namespace Thex.Web.Controllers
{
    [Route(RouteConsts.BillingTax)]
    public class BillingTaxController : ThexAppController
    {
        private readonly IBillingTaxAppService BillingTaxAppService;

        public BillingTaxController(
            IApplicationUser applicationUser,
            IBillingTaxAppService billingTaxAppService)
            : base(applicationUser)
        {
            BillingTaxAppService = billingTaxAppService;
        }

        [HttpGet]
        [ProducesResponseType(typeof(IListDto<BillingTaxDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Get([FromQuery] GetAllBillingTaxDto requestDto)
        {
            var response = await BillingTaxAppService.GetAll(requestDto);

            return CreateResponseOnGetAll(response, EntityNames.BillingTax);
        }

        [HttpGet("{id}")]
        [ProducesResponseType(typeof(BillingTaxDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Get(Guid id, RequestDto requestDto)
        {
            var request = new DefaultGuidRequestDto(id, requestDto);

            var response = await BillingTaxAppService.Get(request);

            return CreateResponseOnGet(response, EntityNames.BillingTax);
        }

        [HttpPut("{id}")]
        [ProducesResponseType(typeof(BillingTaxDto), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Put(Guid id, [FromBody] BillingTaxDto dto)
        {
            var response = await BillingTaxAppService.Update(id, dto);

            return CreateResponseOnPut(response, EntityNames.BillingTax);
        }

        [HttpPost]
        [ProducesResponseType(typeof(BillingTaxDto), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Post([FromBody] BillingTaxDto dto)
        {
            var response = await BillingTaxAppService.Create(dto);

            return CreateResponseOnPost(response, EntityNames.BillingTax);
        }

        [HttpDelete("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Delete(Guid id)
        {
            await BillingTaxAppService.Delete(id);

            return CreateResponseOnDelete(EntityNames.BillingTax);
        }
    }
}
