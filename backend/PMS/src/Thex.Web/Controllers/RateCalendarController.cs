﻿using Thex.Application.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Thex.Domain;
using Thex.Dto.Reservation;
using Thex.Web.Base;
using Thex.Kernel;
using Tnf.Dto;
using Tnf.AspNetCore.Mvc.Response;
using Thex.Dto;
using Thex.Dto.Availability;

namespace Thex.Web.Controllers
{
    [Route(RouteConsts.RateCalendar)]
    public class RateCalendarController : ThexAppController
    {
        private readonly IRateCalendarAppService RateCalendarAppService;

        public RateCalendarController(
            IApplicationUser applicationUser,
            IRateCalendarAppService rateCalendarAppService) : base(applicationUser)
        {
            RateCalendarAppService = rateCalendarAppService;
        }

        [HttpGet("{propertyId}/calendarParameters")]
        [ThexAuthorize("PMS_RateCalendar_Get_CalendarParameters")]
        [ProducesResponseType(typeof(PropertyBaseRateConfigurationParametersDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> GetCalendarDataParameters(int propertyId)
        {
            var response = await RateCalendarAppService.GetCalendarDataParameters(propertyId);

            return CreateResponseOnGet(response, EntityNames.PropertyBaseRate);
        }

        [HttpGet("{propertyId}")]
        [ThexAuthorize("PMS_RateCalendar_Get_Param")]
        [ProducesResponseType(typeof(ListDto<RateCalendarDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> GetRateCalendar([FromQuery] SearchRateCalendarDto requestDto, int propertyId)
        {
            var response = await RateCalendarAppService.GetRateCalendar(requestDto, propertyId);

            return CreateResponseOnGetAll(response, EntityNames.RoomType);
        }
    }
}
