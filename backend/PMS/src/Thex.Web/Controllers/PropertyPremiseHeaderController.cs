﻿using Thex.Application.Interfaces;
using Thex.Dto;
using Tnf.Dto;
using Microsoft.AspNetCore.Mvc;
using Thex.Dto.RoomTypes;
using Thex.Dto.Rooms;
using Thex.Domain;
using Thex.Kernel;
using Thex.Web.Base;
using Tnf.AspNetCore.Mvc.Response;
using System;

namespace Thex.Web.Controllers
{
    [Route(RouteConsts.PropertyPremiseHeader)]
    public class PropertyPremiseHeaderController : ThexAppController
    {
        private readonly IRoomTypeAppService RoomTypeAppService;
        private readonly IRoomAppService RoomAppService;
        private readonly IReservationAppService ReservationAppService;
        private readonly IPropertyPremiseHeaderAppService PropertyPremiseHeaderAppService;

        public PropertyPremiseHeaderController(
            IApplicationUser applicationUser,
            IPropertyPremiseHeaderAppService propertyPremiseHeaderAppService, IRoomTypeAppService roomTypeAppService, IRoomAppService roomAppService, IReservationAppService reservationAppService) : base(applicationUser)
        {
            PropertyPremiseHeaderAppService = propertyPremiseHeaderAppService;
            RoomTypeAppService = roomTypeAppService;
            RoomAppService = roomAppService;
            ReservationAppService = reservationAppService;
        }

        [HttpGet]
        [ThexAuthorize("PMS_PropertyPremise_Get")]
        [ProducesResponseType(typeof(IListDto<PropertyPremiseHeaderListDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult GetAll([FromQuery] GetAllPropertyPremiseHeaderDto requestDto)
        {
            var response = PropertyPremiseHeaderAppService.GetAllHeader(requestDto);

            return CreateResponseOnGetAll(response, EntityNames.PropertyPremiseHeader);
        }

        [HttpGet("{id}")]
        [ThexAuthorize("PMS_PropertyPremise_Get_Param")]
        [ProducesResponseType(typeof(PropertyPremiseHeaderListDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult GetById(Guid id)
        {
            var response = PropertyPremiseHeaderAppService.GetById(id);

            return CreateResponseOnGet(response, EntityNames.PropertyPremiseHeader);
        }

        [HttpPut("{id}")]
        [ThexAuthorize("PMS_PropertyPremise_Put")]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult Put(Guid Id, [FromBody] PropertyPremiseHeaderDto dto)
        {
            PropertyPremiseHeaderAppService.Update(Id, dto);

            return CreateResponseOnPut(null, EntityNames.PropertyPremiseHeader);
        }

        [HttpPost]
        [ThexAuthorize("PMS_PropertyPremise_Post")]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult Post([FromBody] PropertyPremiseHeaderDto dto)
        {
            PropertyPremiseHeaderAppService.Create(dto);

            return CreateResponseOnPost(null, EntityNames.PropertyPremiseHeader);
        }

        [HttpPatch("{id}")]
        [ThexAuthorize("PMS_PropertyPremise_Patch_ToggleActivation")]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult ToggleActivation(Guid id)
        {
            PropertyPremiseHeaderAppService.ToggleActivation(id);

            return CreateResponseOnPatch(null, EntityNames.PropertyPremiseHeader);
        }

        [HttpDelete("{id}")]
        [ThexAuthorize("PMS_PropertyPremise_Delete")]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult Delete(Guid id)
        {
            PropertyPremiseHeaderAppService.Delete(id);

            return CreateResponseOnDelete(EntityNames.PropertyPremiseHeader);
        }
    }
}