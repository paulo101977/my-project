﻿using Thex.Application.Interfaces;
using Thex.Dto;
using Tnf.Dto;
using Microsoft.AspNetCore.Mvc;
using Thex.Dto.RoomTypes;
using Thex.Dto.Rooms;
using Thex.Domain;
using Thex.Kernel;
using Thex.Web.Base;
using Tnf.AspNetCore.Mvc.Response;
using System;

namespace Thex.Web.Controllers
{
    [Route(RouteConsts.PropertyMealPlanTypeRate)]
    public class PropertyMealPlanTypeRateController : ThexAppController
    {
        private readonly IRoomTypeAppService RoomTypeAppService;
        private readonly IRoomAppService RoomAppService;
        private readonly IReservationAppService ReservationAppService;
        private readonly IPropertyMealPlanTypeRateAppService PropertyMealPlanTypeRateAppService;

        public PropertyMealPlanTypeRateController(
            IApplicationUser applicationUser,
            IPropertyMealPlanTypeRateAppService propertyMealPlanTypeRateAppService
            ) : base(applicationUser)
        {
            PropertyMealPlanTypeRateAppService = propertyMealPlanTypeRateAppService;
        }

        [HttpGet("getAll")]
        [ThexAuthorize("PMS_PropertyMealPlanTypeRate_GetAll")]
        [ProducesResponseType(typeof(IListDto<PropertyMealPlanTypeRateSelectDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult Get([FromQuery] GetAllPropertyMealPlanTypeRateDto requestDto)
        {
            var response = PropertyMealPlanTypeRateAppService.GetAll(requestDto);

            return CreateResponseOnGetAll(response, EntityNames.PropertyMealPlanTypeRate);
        }

        [HttpGet("{id}")]
        [ThexAuthorize("PMS_PropertyMealPlanTypeRate_Get")]
        [ProducesResponseType(typeof(PropertyMealPlanTypeRateHeaderSelectDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult Get(Guid id)
        {
            var response = PropertyMealPlanTypeRateAppService.GetById(id);

            return CreateResponseOnGet(response, EntityNames.PropertyMealPlanTypeRate);
        }

        [HttpPut]
        [ThexAuthorize("PMS_PropertyMealPlanTypeRate_Put")]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult Put([FromBody] PropertyMealPlanTypeRateDto dto)
        {
            PropertyMealPlanTypeRateAppService.Update(dto.Id, dto);

            return CreateResponseOnPut(null, EntityNames.PropertyMealPlanTypeRate);
        }

        [HttpPost]
        [ThexAuthorize("PMS_PropertyMealPlanTypeRate_Post")]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult Post([FromBody] PropertyMealPlanTypeRateHeaderDto dto)
        {
            var result = PropertyMealPlanTypeRateAppService.Create(dto);

            return CreateResponseOnPost(result, EntityNames.PropertyMealPlanTypeRate);
        }

        [HttpDelete("{id}")]
        [ThexAuthorize("PMS_PropertyMealPlanTypeRate_Delete")]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult Delete(Guid id)
        {
            PropertyMealPlanTypeRateAppService.Delete(id);

            return CreateResponseOnDelete(EntityNames.PropertyMealPlanTypeRate);
        }

        [HttpPatch("{id}")]
        [ThexAuthorize("PMS_PropertyMealPlanTypeRate_Patch_Toggle")]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult Toggle(Guid id)
        {
            PropertyMealPlanTypeRateAppService.ToggleActivation(id);

            return CreateResponseOnPatch(EntityNames.PropertyMealPlanTypeRate);
        }
    }
}