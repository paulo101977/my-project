﻿using Thex.Application.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Thex.Domain;
using Thex.Kernel;
using Thex.Web.Base;
using Thex.Dto.PropertyAuditProcessStep;
using Tnf.AspNetCore.Mvc.Response;
using Thex.Dto;

namespace Thex.Web.Controllers
{
    [Route(RouteConsts.PropertyAuditProcess)]
    public class PropertyAuditProcessController : ThexAppController
    {
        private readonly IPropertyAuditProcessAppService PropertyAuditProcessAppService;

        public PropertyAuditProcessController(
            IApplicationUser applicationUser,
            IPropertyAuditProcessAppService propertyAuditProcessAppService) : base(applicationUser)
        {
            PropertyAuditProcessAppService = propertyAuditProcessAppService;
        }

        [HttpPatch("{propertyId}")]
        [ThexAuthorize("PMS_PropertyAuditProcess_Patch_Params")]
        [ProducesResponseType(typeof(PropertyAuditProcessStepDto), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult Patch(int propertyId)
        {
            var response = PropertyAuditProcessAppService.Execute(propertyId);
            return CreateResponseOnPost(response, EntityNames.PropertyAuditProcessStep);
        }

        [HttpGet("{propertyId}/GetAuditStep")]
        [ThexAuthorize("PMS_PropertyAuditProcess_Get_GetAuditStep")]
        [ProducesResponseType(typeof(PropertyAuditProcessStepNumber), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public virtual IActionResult GetAuditStep(int propertyId)
        {
            var response = PropertyAuditProcessAppService.GetAuditStepByPropertyId(propertyId);
            return CreateResponseOnGet(response, EntityNames.PropertyAuditProcessStep);
        }

        [HttpGet("{propertyId}/GetTimeForAudit")]
        [ThexAuthorize("PMS_PropertyAuditProcess_Get_GetTimeForAudit")]
        [ProducesResponseType(typeof(PropertyAuditProcessTimeDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public virtual IActionResult  GetTimeForAudit(int propertyId)
        {
            var response = PropertyAuditProcessAppService.GetTimeForAuditByPropertyId(propertyId);
            return CreateResponseOnGet(response, EntityNames.PropertyAuditProcessStep);
        }

        [HttpGet("{propertyId}/GetSystemDate")]
        [ProducesResponseType(typeof(PropertyAuditProcessTimeDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public virtual IActionResult GetSytemDate(int propertyId)
        {
            var response = PropertyAuditProcessAppService.GetTimeForAuditByPropertyId(propertyId);
            return CreateResponseOnGet(response, EntityNames.PropertyAuditProcessStep);
        }

    }
}
