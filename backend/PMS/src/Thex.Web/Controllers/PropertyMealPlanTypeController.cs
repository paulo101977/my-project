﻿using System;
using Thex.Application.Interfaces;
using Thex.Dto;
using Tnf.Dto;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Thex.Domain;
using System.Collections.Generic;
using Thex.Kernel;
using Thex.Web.Base;
using Tnf.AspNetCore.Mvc.Response;

namespace Thex.Web.Controllers
{
    [Route(RouteConsts.PropertyMealPlanType)]
    public class PropertyMealPlanTypeController : ThexAppController
    {
        private readonly IPropertyMealPlanTypeAppService PropertyMealPlanTypeAppService;

        public PropertyMealPlanTypeController(
            IApplicationUser applicationUser,
            IPropertyMealPlanTypeAppService propertyMealPlanTypeAppService)
            : base(applicationUser)
        {
            PropertyMealPlanTypeAppService = propertyMealPlanTypeAppService;
        }

        [HttpGet]
        [ProducesResponseType(typeof(IListDto<PropertyMealPlanTypeDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Get([FromQuery] GetAllPropertyMealPlanTypeDto requestDto, bool all = false)
        {
            var response = await PropertyMealPlanTypeAppService.GetAllPropertyMealPlanTypeByProperty(all);

            return CreateResponseOnGetAll(response, EntityNames.PropertyMealPlanType);
        }

        [HttpGet("{id}")]
        [ProducesResponseType(typeof(PropertyMealPlanTypeDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Get(Guid id, RequestDto requestDto)
        {
            var request = new DefaultGuidRequestDto(id, requestDto);

            var response = await PropertyMealPlanTypeAppService.Get(request);

            return CreateResponseOnGet(response, EntityNames.PropertyMealPlanType);
        }

        [HttpPut("{id}")]
        [ValidatePropertyIdFilter]
        [ProducesResponseType(typeof(PropertyMealPlanTypeDto), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Put(Guid id, [FromBody] PropertyMealPlanTypeDto dto)
        {
            var response = await PropertyMealPlanTypeAppService.Update(id, dto);

            return CreateResponseOnPut(response, EntityNames.PropertyMealPlanType);
        }

        [HttpPost]
        [ValidatePropertyIdFilter]
        [ProducesResponseType(typeof(PropertyMealPlanTypeDto), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Post([FromBody] PropertyMealPlanTypeDto dto)
        {
            var response = await PropertyMealPlanTypeAppService.Create(dto);

            return CreateResponseOnPost(response, EntityNames.PropertyMealPlanType);
        }

        [HttpDelete("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Delete(Guid id)
        {
            await PropertyMealPlanTypeAppService.Delete(id);

            return CreateResponseOnDelete(EntityNames.PropertyMealPlanType);
        }

        [HttpGet("{propertyId}/getallpropertymealplantype")]
        [ThexAuthorize("PMS_PropertyMealPlanType_Get_GetAllPropertyMealPlanType")]
        [ProducesResponseType(typeof(IListDto<PropertyMealPlanTypeDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult GetAll(int propertyId, bool all = false)
        {
            var response = PropertyMealPlanTypeAppService.GetAllPropertyMealPlanTypeByProperty(propertyId,all);

            return CreateResponseOnGetAll(response, EntityNames.PropertyMealPlanType);
        }

        [HttpPut]
        [ThexAuthorize("PMS_PropertyMealPlanType_Update")]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult PropertyMealPlanTypeUpdate([FromBody] IList<PropertyMealPlanTypeDto> dto)
        {
            PropertyMealPlanTypeAppService.PropertyMealPlanTypeUpdate(dto);

            return CreateResponseOnPut(null, EntityNames.PropertyMealPlanType);
        }

        [HttpPatch("{id}/toggleactivation")]
        [ThexAuthorize("PMS_PropertyMealPlanType_Patch_ToggleActivation")]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult ToggleActivation(Guid id)
        {
            PropertyMealPlanTypeAppService.ToggleActivation(id);

            return CreateResponseOnPatch(null, EntityNames.PropertyMealPlanType);
        }
    }
}
