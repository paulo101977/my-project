﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Thex.Application.Interfaces;
using Thex.Dto.PowerBI;
using Thex.Kernel;
using Thex.Web.Base;
using Tnf.AspNetCore.Mvc.Response;

namespace Thex.Web.Controllers
{
    [Authorize]
    [Route(RouteConsts.PowerBI)]
    public class PowerBiController : ThexAppController
    {
        private readonly IPowerBIAppService _powerBIAppService;

        public PowerBiController(
            IPowerBIAppService powerBIAppService,
            IApplicationUser applicationUser) : base(applicationUser)
        {
            _powerBIAppService = powerBIAppService;
        }

        [HttpGet]
        [ThexAuthorize("PMS_DashboardReport_Get")]
        [ProducesResponseType(typeof(PowerBiMenuDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> GetAllPowerBI()
        {
            var powerBIMenuDto = await _powerBIAppService.GetAllAsync();

            return CreateResponseOnGet(powerBIMenuDto);
        }
    }
}