﻿using System;
using Thex.Application.Interfaces;
using Thex.Dto;
using Tnf.Dto;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Thex.Domain;
using Thex.Application.Services;

namespace Thex.Web.Controllers
{
    using System.Collections.Generic;
    using System.Net;
    using Thex.Kernel;
    using Thex.Storage;
    using Thex.Web.Base;
    using Tnf.AspNetCore.Mvc.Response;


    [Route("api/[controller]")]
    public class StorageController :  ThexAppController
    {
        private readonly IAzureStorage _azureStorage;

        public StorageController(
            IApplicationUser applicationUser,
            IAzureStorage azureStorage)
            : base(applicationUser)
        {
            _azureStorage = azureStorage;
        }        

        // GET: api/<controller>
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/<controller>/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }


        [HttpPost]        
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Post([FromBody] DocumentDto dto)
        {
            await _azureStorage.SendAsync(Guid.NewGuid(),"aaaaa","bbbbb",dto.Value);

            //return CreateResponseOnPost(response, EntityNames.Document);
            return null;

        }
        // PUT api/<controller>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
