﻿using Thex.Application.Interfaces;
using Thex.Dto;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Thex.Web.Controllers
{
    using Thex.Kernel;
    using Thex.Domain;
    using Thex.Web.Base;
    using Tnf.AspNetCore.Mvc.Response;
    using Tnf.Dto;

    [Route(RouteConsts.MarketSegment)]
    public class MarketSegmentController : ThexAppController
    {
        private readonly IMarketSegmentAppService MarketSegmentAppService;
        public MarketSegmentController(
            IApplicationUser applicationUser,
            IMarketSegmentAppService marketSegmentAppService)
            : base(applicationUser)
        {
            MarketSegmentAppService = marketSegmentAppService;
        }

        [HttpGet]
        [ThexAuthorize("PMS_MarketSegment_Get")]
        [ProducesResponseType(typeof(IListDto<MarketSegmentDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult Get([FromQuery] GetAllMarketSegmentDto requestDto)
        {
            int propertyId = requestDto.PropertyId;
            var response = MarketSegmentAppService.GetAllByPropertyId(requestDto, propertyId);

            return CreateResponseOnGetAll(response, EntityNames.MarketSegment);
        }

        [HttpGet("{id}")]
        [ProducesResponseType(typeof(MarketSegmentDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult Get(int id)
        {
            var response = MarketSegmentAppService.GetById(id);
            return CreateResponseOnGet(response, EntityNames.MarketSegment);
        }

        [HttpPut("{id}")]
        [ThexAuthorize("PMS_MarketSegment_Put")]
        [ProducesResponseType(typeof(MarketSegmentDto), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Put(int id, [FromBody]MarketSegmentDto dto)
        {
            var response = await MarketSegmentAppService.Update(dto.Id, dto);

            return CreateResponseOnPut(response, EntityNames.MarketSegment);
        }

        [HttpPost]
        [ThexAuthorize("PMS_MarketSegment_Post")]
        [ProducesResponseType(typeof(MarketSegmentDto), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Post([FromBody] MarketSegmentDto dto)
        {
            var response = await MarketSegmentAppService.Create(dto);

            return CreateResponseOnPost(response, EntityNames.MarketSegment);
        }

        [HttpDelete("{id}")]
        [ThexAuthorize("PMS_MarketSegment_Delete")]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult Delete(int id)
        {
            MarketSegmentAppService.Delete(id);

            return CreateResponseOnDelete(EntityNames.MarketSegment);
        }

        [HttpPatch("{id}/toggleactivation")]
        [ThexAuthorize("PMS_MarketSegment_Patch_ToggleActivation")]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult ToggleActivation(int id)
        {
            MarketSegmentAppService.ToggleActivation(id);

            return CreateResponseOnPatch(null, EntityNames.MarketSegment);
        }
    }



}
