﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.Application.Interfaces;
using Thex.Common.Dto.InvoiceIntegration;
using Thex.Domain;
using Thex.Dto;
using Thex.Dto.BillingAccountClosure;
using Thex.Kernel;
using Thex.Web.Base;
using Tnf.AspNetCore.Mvc.Response;

namespace Thex.Web.Controllers
{
    [Route(RouteConsts.BillingInvoiceIntegration)]
    public class BillingInvoiceIntegrationController : ThexAppController
    {
        private readonly IIntegrationAppService _integrationAppService;
        private readonly IBillingInvoiceAppService _billingInvoiceAppService;

        public BillingInvoiceIntegrationController(
            IApplicationUser applicationUser,
            IIntegrationAppService integrationAppService,
            IBillingInvoiceAppService billingInvoiceAppService) : base(applicationUser)
        {
            _integrationAppService = integrationAppService;
            _billingInvoiceAppService = billingInvoiceAppService;
        }

        /// <summary>
        /// Recebe o status 
        /// </summary>
        /// <returns></returns>
        [HttpPost("status")]
        [AllowAnonymous]
        [ValidateApiKeyFilter]
        [ProducesResponseType(typeof(InvoiceIntegrationResponseDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> UpdateStatus([FromBody]InvoiceIntegrationResponseDto invoiceResponsetDto)
        {
            var response = await _integrationAppService.UpdateStatus(invoiceResponsetDto);

            return CreateResponseOnPost(response, EntityNames.BillingInvoice);
        }

        /// <summary>
        /// Reenvia a nota para emissão
        /// </summary>
        /// <returns></returns>
        [HttpPatch("property/{propertyId}/invoice/{invoiceId}/resend")]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Resend(int propertyId, Guid invoiceId)
        {
            await _integrationAppService.ResendInvoice(
                new List<BillingAccountClosureInvoiceDto> {new BillingAccountClosureInvoiceDto {
                                                                InvoiceId = invoiceId.ToString()
                                                           }}).ConfigureAwait(false);

            return CreateResponseOnPatch(null, EntityNames.BillingInvoice);
        }

        [HttpGet("property/{propertyId}/invoice/{invoiceId}/languague/{languageIsoCode}/rps")]
        [ThexAuthorize("PMS_BillingInvoiceIntegration_Get_Invoice_Rps")]
        [ProducesResponseType(typeof(RpsDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> GetRps(int propertyId, Guid invoiceId, string languageIsoCode)
        {
            var response = await _billingInvoiceAppService.GetRpsByInvoiceId(propertyId, invoiceId, languageIsoCode);

            return CreateResponseOnGet(response, EntityNames.BillingInvoice);
        }

        /// <summary>
        /// Faz o download os arquivos de acordo com o período solicitado
        /// </summary>
        /// <returns></returns>
        [ThexAuthorize("PMS_BillingInvoiceIntegration_Get_Invoice_Download")]
        [HttpGet("property/{propertyId}/invoice/download")]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Download(int propertyId, [FromQuery]SearchBillingInvoiceDto requestDto)
        {
            var response = await _integrationAppService.Download(requestDto, propertyId);
            return File(response, "application/octet-stream", $"propertyInvoices{propertyId}-{DateTime.Now}.zip");
        }

        [HttpGet("downloadSetup")]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> GetDownloadSetup()
        {
            var setupDownloadUrl = await _integrationAppService.DownloadSetupUrl(_applicationUser.PropertyId);
            return CreateResponseOnGet(setupDownloadUrl, EntityNames.BillingInvoice);
        }
    }
}
