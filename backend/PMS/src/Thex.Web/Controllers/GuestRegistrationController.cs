﻿using System;
using Thex.Application.Interfaces;
using Thex.Dto;
using Tnf.Dto;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Thex.Domain;
using Thex.Dto.GuestRegistration;
using Thex.Kernel;
using Thex.Web.Base;
using Tnf.AspNetCore.Mvc.Response;

namespace Thex.Web.Controllers
{
    [Route(RouteConsts.GuestRegistration)]
    public class GuestRegistrationController : ThexAppController
    {
        private readonly IGuestRegistrationAppService GuestRegistrationAppService;
        public GuestRegistrationController(
            IApplicationUser applicationUser,
            IGuestRegistrationAppService guestRegistrationAppService)
            : base(applicationUser)
        {
            GuestRegistrationAppService = guestRegistrationAppService;
        }

        [HttpGet]
        [ProducesResponseType(typeof(IListDto<GuestRegistrationDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Get([FromQuery] GetAllGuestRegistrationDto requestDto)
        {
            var response = await GuestRegistrationAppService.GetAll(requestDto);

            return CreateResponseOnGetAll(response, EntityNames.GuestRegistration);
        }

        [HttpGet("{id}")]
        [ProducesResponseType(typeof(GuestRegistrationDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Get(Guid id, RequestDto requestDto)
        {
            var request = new DefaultGuidRequestDto(id, requestDto);

            var response = await GuestRegistrationAppService.Get(request);

            return CreateResponseOnGet(response, EntityNames.GuestRegistration);
        }

        [HttpPut("{id}")]
        [ThexAuthorize("PMS_GuestRegistration_Put")]
        [ValidatePropertyIdFilter]
        [ProducesResponseType(typeof(GuestRegistrationDto), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Put(Guid id, [FromBody] GuestRegistrationDto dto)
        {
            var response = await GuestRegistrationAppService.Update(id, dto);

            return CreateResponseOnPut(response, EntityNames.GuestRegistration);
        }

        [HttpPost]
        [ThexAuthorize("PMS_GuestRegistration_Post")]
        [ValidatePropertyIdFilter]
        [ProducesResponseType(typeof(GuestRegistrationDto), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Post([FromBody] GuestRegistrationDto dto)
        {
            var response = await GuestRegistrationAppService.Create(dto);

            return CreateResponseOnPost(response, EntityNames.GuestRegistration);
        }

        [HttpDelete("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Delete(Guid id)
        {
            await GuestRegistrationAppService.Delete(id);

            return CreateResponseOnDelete(EntityNames.GuestRegistration);
        }

        [HttpGet("ByGuestReservationItem/{guestReservationItemId}/property/{propertyId}/language/{languageIsoCode}")]
        [ThexAuthorize("PMS_GuestRegistration_Get_ByGuestReservationItem")]
        [ProducesResponseType(typeof(GuestRegistrationResultDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult GetGuestRegistrationByGuestReservationItem(long propertyId, long guestReservationItemId, string languageIsoCode)
        {
            var response = GuestRegistrationAppService.GetGuestRegistrationByGuestReservationItem(propertyId, guestReservationItemId, languageIsoCode);
            return CreateResponseOnGet(response, EntityNames.GuestRegistration);
        }

        [HttpGet("ByReservationItem/{reservationItemId}/property/{propertyId}/language/{languageIsoCode}")]
        [ProducesResponseType(typeof(IListDto<GuestRegistrationResultDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult GetAllGuestRegistrationByReservationItem(long propertyId, long reservationItemId, string languageIsoCode)
        {
            var response = GuestRegistrationAppService.GetAllGuestRegistrationByReservationItemId(propertyId, reservationItemId, languageIsoCode);
            return CreateResponseOnGetAll(response, EntityNames.GuestRegistration);

        }

        [HttpGet("search")]
        [ThexAuthorize("PMS_GuestRegistration_Get_Search")]
        [ProducesResponseType(typeof(IListDto<GuestRegistrationResultDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult Search([FromQuery]SearchGuestRegistrationsDto request)
        {
            var response = GuestRegistrationAppService.GetAllGuestRegistrationBasedOnFilter(request);

            return CreateResponseOnGetAll(response, EntityNames.Person);
        }


        [HttpGet("{propertyId}/{reservationId}/responsibles")]
        [ThexAuthorize("PMS_GuestRegistration_Get_Responsibles")]
        [ProducesResponseType(typeof(IListDto<GuestRegistrationResultDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult GetAllResponsibles(long propertyId, long reservationId)
        {
            var response = GuestRegistrationAppService.GetAllResponsibles(propertyId, reservationId);
            return CreateResponseOnGetAll(response, EntityNames.GuestRegistration);
        }

    }
}
