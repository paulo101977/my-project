﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Thex.Application.Interfaces;
using Thex.Kernel;
using Thex.Domain;
using Thex.Dto;
using Thex.Web.Base;
using Tnf.AspNetCore.Mvc.Response;
using Tnf.Dto;

namespace Thex.Web.Controllers
{

    [Route(RouteConsts.PropertyCompanyClientCategory)]
    public class PropertyCompanyClientCategoryController : ThexAppController
    {
        private readonly IPropertyCompanyClientCategoryAppService _propertyCompanyClientCategoryAppService;

        public PropertyCompanyClientCategoryController(
            IApplicationUser applicationUser, 
            IPropertyCompanyClientCategoryAppService propertyCompanyClientCategoryAppService)
            : base(applicationUser)
        {
            _propertyCompanyClientCategoryAppService = propertyCompanyClientCategoryAppService;
        }

        [HttpGet("{propertyId}/getallcompanyclientcategory")]
        [ThexAuthorize("PMS_PropertyCompanyClientCategory_Get_GetAllCompanyClientCategory")]
        [ProducesResponseType(typeof(IListDto<PropertyCompanyClientCategoryDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult GetAllCompanyClientCategory([FromQuery] GetAllPropertyCompanyClientCategoryDto request, int propertyId)
        {
            var response = _propertyCompanyClientCategoryAppService.GetAllByPropertyId(request, propertyId);
            return CreateResponseOnGetAll(response, EntityNames.PropertyCompanyClientCategory);
        }

        [HttpGet("{propertycompanyclientcategoryid}")]
        [ProducesResponseType(typeof(PropertyCompanyClientCategoryDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult GetCompanyClientCategory(RequestDto request,Guid propertycompanyclientcategoryid)
        {
            var requestDto = new DefaultGuidRequestDto(propertycompanyclientcategoryid);

            var response = _propertyCompanyClientCategoryAppService.GetPropertyCompanyClientCategoryById(requestDto);

            return CreateResponseOnGet(EntityNames.PropertyCompanyClientCategory);
        }

        [HttpDelete("{id}/delete")]
        [ThexAuthorize("PMS_PropertyCompanyClientCategory_Delete")]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult Delete(Guid id)
        {
            _propertyCompanyClientCategoryAppService.Delete(id);

            return CreateResponseOnDelete(EntityNames.PropertyCompanyClientCategory);
        }

        [HttpPatch("{id}/toggleactivation")]
        [ThexAuthorize("PMS_PropertyCompanyClientCategory_Patch_ToggleActivation")]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult ToggleActivation(Guid id)
        {
            _propertyCompanyClientCategoryAppService.ToggleAndSaveIsActive(id);

            return CreateResponseOnPatch(null, EntityNames.PropertyCompanyClientCategory);
        }

        [HttpPost]
        [ThexAuthorize("PMS_PropertyCompanyClientCategory_Post")]
        [ValidatePropertyIdFilter]
        [ProducesResponseType(typeof(PropertyCompanyClientCategoryDto), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Post([FromBody]PropertyCompanyClientCategoryDto propertyCompanyClientCategoryDto)
        {
            var response = await _propertyCompanyClientCategoryAppService.Create(propertyCompanyClientCategoryDto);

            return CreateResponseOnPost(response, EntityNames.PropertyCompanyClientCategory);
        }

        [HttpPut]
        [ThexAuthorize("PMS_PropertyCompanyClientCategory_Put")]
        [ValidatePropertyIdFilter]
        [ProducesResponseType(typeof(PropertyCompanyClientCategoryDto), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Update([FromBody] PropertyCompanyClientCategoryDto propertyCompanyClientCategoryDto)
        {
           var response = await _propertyCompanyClientCategoryAppService.Update(propertyCompanyClientCategoryDto.Id, propertyCompanyClientCategoryDto);

            return CreateResponseOnPut(response, EntityNames.PropertyCompanyClientCategory);
        }
    }
}