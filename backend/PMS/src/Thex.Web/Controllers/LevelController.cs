﻿using Microsoft.AspNetCore.Mvc;
using System;
using Thex.Application.Interfaces;
using Thex.Domain;
using Thex.Dto;
using Thex.Kernel;
using Thex.Web.Base;
using Tnf.AspNetCore.Mvc.Response;

namespace Thex.Web.Controllers
{
    [Route(RouteConsts.Level)]
    public class LevelController : ThexAppController
    {
        private readonly ILevelAppService _levelAppService;

        public LevelController(
            IApplicationUser applicationUser,
            ILevelAppService levelAppService) : base(applicationUser)
        {
            _levelAppService = levelAppService;
            _applicationUser = applicationUser;
        }

        [HttpGet]
        [ProducesResponseType(typeof(LevelDto), 200)]
        [ProducesResponseType(404)]
        [ThexAuthorize("PMS_Level_Get_All")]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult GetAllDto(GetAllLevelDto requestDto)
        {
            var response = _levelAppService.GetAllDto(requestDto);
            return CreateResponseOnGet(response, EntityNames.Level);
        }


        [HttpGet("{id}")]
        [ProducesResponseType(typeof(LevelDto), 200)]
        [ProducesResponseType(404)]
        [ThexAuthorize("PMS_Level_Get")]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult Get(Guid id)
        {
            var response = _levelAppService.GetDtoById(id);
            return CreateResponseOnGet(response, EntityNames.Level);
        }

        [HttpPut]
        [ValidatePropertyIdFilter]
        [ProducesResponseType(typeof(LevelDto), 200)]
        [ThexAuthorize("PMS_Level_Put")]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult Put([FromBody] LevelDto dto)
        {
            var response = _levelAppService.UpdateLevel(dto);
            return CreateResponseOnPut(response, EntityNames.Level);
        }

        [HttpPost]
        [ValidatePropertyIdFilter]
        [ProducesResponseType(typeof(LevelDto), 200)]
        [ThexAuthorize("PMS_Level_Post")]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult Post([FromBody] LevelDto dto)
        {
            var response = _levelAppService.CreateLevel(dto);
            return CreateResponseOnPost(response, EntityNames.Level);
        }

        [HttpDelete("{id}")]
        [ProducesResponseType(200)]
        [ThexAuthorize("PMS_Level_Delete")]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult Delete(Guid id)
        {
            _levelAppService.DeleteLevel(id);
            return CreateResponseOnDelete(EntityNames.Level);
        }

        [HttpPatch("{id}/toggleactivation")]
        [ProducesResponseType(200)]
        [ThexAuthorize("PMS_Level_Patch_ToggleActivation")]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult ToggleActivation(Guid id)
        {
            _levelAppService.ToggleAndSaveIsActive(id);
            return CreateResponseOnPatch(null, EntityNames.Level);
        }

    }
}