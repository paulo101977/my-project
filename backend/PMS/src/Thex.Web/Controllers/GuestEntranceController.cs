﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.Application.Interfaces;
using Thex.Kernel;
using Thex.Domain;
using Thex.Dto;
using Thex.Web.Base;
using Tnf.AspNetCore.Mvc.Response;
using System;

namespace Thex.Web.Controllers
{
    [Route(RouteConsts.GuestEntrance)]
    public class GuestEntranceController : ThexAppController
    {
        private readonly IGuestEntranceAppService _guestEntranceAppService;
        private readonly IReservationBudgetAppService _reservationBudgetAppService;
        public GuestEntranceController(
            IApplicationUser applicationUser,
            IGuestEntranceAppService guestEntranceAppService,
            IReservationBudgetAppService reservationBudgetAppService
        ) : base(applicationUser)
        {
            _guestEntranceAppService = guestEntranceAppService;
            _reservationBudgetAppService = reservationBudgetAppService;
        }

        [HttpGet("{propertyId}")]
        [ThexAuthorize("PMS_GuestEntranceBudget_Get_Param")]
        [ProducesResponseType(typeof(ReservationItemBudgetHeaderDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> GetWithBudgetOffer([FromQuery] ReservationItemBudgetRequestDto requestDto, int propertyId)
        {
            if (requestDto.RoomTypeId.HasValue)
                requestDto.RoomTypeIdList = new List<int>() { requestDto.RoomTypeId.Value };

            var response = await _guestEntranceAppService.GetWithBudgetOffer(requestDto, propertyId);

            return CreateResponseOnGet(response, EntityNames.ReservationBudget);
        }

        [HttpPost("confirm")]
        [ThexAuthorize("PMS_GuestEntranceConfirm_Post")]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> ConfirmEntrance([FromBody] ReservationItemGuestDto requestDto)
        {
            await _guestEntranceAppService.ConfirmEntrance(requestDto);

            return CreateResponseOnPut(null, EntityNames.ReservationBudget);
        }

        [HttpPost("createGuest")]
        [ThexAuthorize("PMS_GuestEntrance_Post")]
        [ValidatePropertyIdFilter]
        [ProducesResponseType(typeof(GuestRegistrationDto), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> CreateGuest([FromBody] GuestRegistrationDto dto)
        {
            var response = await _guestEntranceAppService.CreateGuest(dto);

            return CreateResponseOnPost(response, EntityNames.GuestRegistration);
        }

        [HttpPut("updateGuest/{id}")]
        [ThexAuthorize("PMS_GuestEntrance_Put")]
        [ValidatePropertyIdFilter]
        [ProducesResponseType(typeof(GuestRegistrationDto), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> UpdateGuest(Guid id, [FromBody] GuestRegistrationDto dto)
        {
            var response = await _guestEntranceAppService.UpdateGuest(id, dto);

            return CreateResponseOnPut(response, EntityNames.GuestRegistration);
        }
    }
}