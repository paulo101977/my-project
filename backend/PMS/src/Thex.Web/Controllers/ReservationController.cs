﻿using Thex.Application.Interfaces;
using Thex.Dto;
using Tnf.Dto;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

using Thex.Domain;

namespace Thex.Web.Controllers
{    
    using Thex.Kernel;
    using Thex.Dto.Reservation;
    using Thex.Web.Base;
    using Tnf.AspNetCore.Mvc.Response;

    [Route(RouteConsts.Reservation)]
    public class ReservationController : ThexAppController
    {
        protected readonly IReservationAppService ReservationAppService;

        public ReservationController(
            IApplicationUser applicationUser,
            IReservationAppService reservationAppService) : base(applicationUser)
        {
            ReservationAppService = reservationAppService;
        }

        [HttpGet]
        [ProducesResponseType(typeof(IListDto<ReservationDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Get([FromQuery] GetAllReservationDto requestDto)
        {
            var response = await ReservationAppService.GetAll(requestDto);

            return CreateResponseOnGetAll(response, EntityNames.Reservation);
        }

        [HttpGet("{id}")]
        [ThexAuthorize("PMS_Reservation_Get")]
        [ProducesResponseType(typeof(ReservationDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult Get(long id, [FromQuery] RequestDto requestDto)
        {
            var request = new DefaultLongRequestDto(id, requestDto);

            var response = ReservationAppService.GetReservation(request);

            return CreateResponseOnGet(response, EntityNames.Reservation);
        }

        [HttpPut("{id}")]
        [ThexAuthorize("PMS_Reservation_Put")]
        [ValidatePropertyIdFilter]
        [ProducesResponseType(typeof(ReservationDto), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Put(long id, [FromBody] ReservationDto dto)
        {
            var response = await ReservationAppService.UpdateReservationAsync(id, dto);

            return CreateResponseOnPut(response, EntityNames.Reservation);
        }

        [HttpPost]
        [ThexAuthorize("PMS_Reservation_Post")]
        [ValidatePropertyIdFilter]
        [ProducesResponseType(typeof(ReservationDto), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult Post([FromBody] ReservationDto dto)
        {
            var response = ReservationAppService.CreateReservation(dto);

            return CreateResponseOnPost(response, EntityNames.Reservation);
        }

        [HttpDelete("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult Delete(long id)
        {
            ReservationAppService.DeleteReservation(id);

            return CreateResponseOnDelete(EntityNames.Reservation);
        }

        [HttpGet("{id}/withreservationitemsandroomsandroomstypes")]
        [ProducesResponseType(typeof(ReservationDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult GetWithReservationItemsAndRoomsAndRoomTypes([FromQuery] RequestDto request, long id)
        {
            var requestDto = new DefaultLongRequestDto(id, request);

            var response = ReservationAppService.GetWithReservationItemsAndRoomsAndRoomTypes(requestDto);
            
            return CreateResponseOnGet(response, EntityNames.Reservation);
        }

        [HttpGet("{propertyId}/search")]
        [ThexAuthorize("PMS_Reservation_Get_Search")]
        [ProducesResponseType(typeof(IListDto<SearchReservationResultDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult GetAllByFilters([FromQuery] SearchReservationDto request, int propertyId)
        {
            var response = ReservationAppService.GetAllByFilters(request, propertyId);

            return CreateResponseOnGetAll(response, EntityNames.Reservation);
        }

        [HttpGet("{propertyId}/searchV2")]
        [ProducesResponseType(typeof(IListDto<SearchReservationResultDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult GetAllByFiltersV2([FromQuery] SearchReservationDto request, int propertyId)
        {
            var response = ReservationAppService.GetAllByFiltersV2(request, propertyId);

            return CreateResponseOnGetAll(response, EntityNames.Reservation);
        }

        [HttpGet("details")]
        [ProducesResponseType(typeof(SearchReservationResultDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult GetReservationDetail([FromQuery] SearchReservationDto request)
        {
            var response = ReservationAppService.GetReservationDetail(request);

            return CreateResponseOnGet(response, EntityNames.Reservation);
        }

        [HttpGet("{reservationItemId}/byreservationitem")]
        [ThexAuthorize("PMS_Reservation_Get_ByReservationItem")]
        [ProducesResponseType(typeof(ReservationDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult GetByReservationItemId(long reservationItemId, [FromQuery] RequestDto requestDto)
        {
            var request = new DefaultLongRequestDto(reservationItemId, requestDto);

            var response = ReservationAppService.GetByReservationItemId(request);

            return CreateResponseOnGet(response, EntityNames.ReservationItem);
        }

        [HttpPatch("{reservationId}/cancel")]
        [ThexAuthorize("PMS_Reservation_Patch_Cancel")]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult CancelReservation(long reservationId, [FromBody] ReservationCancelDto dto)
        {
            this.ReservationAppService.CancelReservation(reservationId, dto);

            return CreateResponseOnPatch(null, EntityNames.ReservationItem);
        }

        [HttpGet("{reservationItemId}/header")]
        [ThexAuthorize("PMS_Reservation_Get_Header")]
        [ProducesResponseType(typeof(ReservationHeaderResultDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult GetReservationHeaderByReservationItemId(long reservationItemId)
        {
            var response = ReservationAppService.GetReservationHeaderByReservationItemId(reservationItemId);

            return CreateResponseOnGet(response, EntityNames.ReservationItem);
        }

        [HttpPatch("{reservationItemId}/savenote")]
        [ThexAuthorize("PMS_Reservation_Patch_SaveNote")]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult ReservationSaveNote(long reservationItemId, [FromBody] ReservationSaveNotesDto dto)
        {
            ReservationAppService.ReservationSaveNote(reservationItemId, dto);

            return CreateResponseOnPatch(null, EntityNames.ReservationItem);
        }

        [HttpPatch("{reservationId}/reactivate")]
        [ThexAuthorize("PMS_Reservation_Patch_Reactivate")]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult ReactivateReservation(long reservationId)
        {
            ReservationAppService.ReactivateReservation(reservationId);

            return CreateResponseOnPatch(null, EntityNames.ReservationItem);
        }

        [HttpPatch("{reservationId}/confirm")]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult ConfirmReservation(long reservationId)
        {
            ReservationAppService.ConfirmReservation(reservationId);

            return CreateResponseOnPatch(null, EntityNames.ReservationItem);
        }

        [HttpGet("{reservationId}/reservationHeader")]
        [ThexAuthorize("PMS_Reservation_Get_ReservationHeader")]
        [ProducesResponseType(typeof(ReservationHeaderResultDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult GetReservationId(long reservationId)
        {
            var response = ReservationAppService.GetReservationHeader(reservationId);

            return CreateResponseOnGet(response, EntityNames.ReservationItem);
        }

        [HttpGet("newReservationCode")]
        [ThexAuthorize("PMS_Reservation_Get_NewReservationCode")]
        [ProducesResponseType(typeof(NewReservationCodeDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult GetReservationCode()
            => CreateResponseOnGet(ReservationAppService.GetNewReservationCode(), EntityNames.Reservation);
    }
}
