﻿using System;
using Thex.Application.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Thex.Domain;
using Thex.Web.Base;
using Tnf.AspNetCore.Mvc.Response;
using Thex.Kernel;
using Thex.Dto;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace Thex.Web.Controllers
{
    [Route(RouteConsts.BillingInvoice)]
    public class BillingInvoiceController : ThexAppController
    {
        private readonly IBillingInvoiceAppService _billingInvoiceAppService;

        public BillingInvoiceController(
              IApplicationUser applicationUser,
            IBillingInvoiceAppService billingInvoiceAppService)
            : base(applicationUser)
        {
            _billingInvoiceAppService = billingInvoiceAppService;
        }


        [HttpPost("createcreditnote")]
        [ThexAuthorize("PMS_BillingInvoice_Post_Create_Credit_Note")]
        [ValidatePropertyIdFilter]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> CreateCreditNote([FromBody] List<BillingAccountItemCreditAmountDto> billingAccountItemDtoList)
        {
            var response = await _billingInvoiceAppService.CreateCreditNoteAndUpdateLastNumber(billingAccountItemDtoList);

            return CreateResponseOnPost(response, EntityNames.BillingInvoice);
        }
    }
}
