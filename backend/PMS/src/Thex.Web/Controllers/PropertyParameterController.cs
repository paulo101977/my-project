﻿using System;
using Microsoft.AspNetCore.Mvc;
using Thex.Application.Interfaces;
using Thex.Kernel;
using Thex.Domain;
using Thex.Dto;
using Thex.Dto.PropertyParameter;
using Thex.Web.Base;
using Tnf.AspNetCore.Mvc.Response;
using Tnf.Dto;

namespace Thex.Web.Controllers
{
    [Route(RouteConsts.PropertyParameter)]
    public class PropertyParameterController : ThexAppController
    {
        protected readonly IPropertyParameterAppService PropertyParameterAppService;

        public PropertyParameterController(
            IApplicationUser applicationUser,
            IPropertyParameterAppService propertyParameterAppService) : base(applicationUser)
        {
            PropertyParameterAppService = propertyParameterAppService;
        }

        [HttpGet("{propertyId}/propertyparameters")]
        [ThexAuthorize("PMS_PropertyParameter_Get_PropertyParameters",
                       "PMS_Property_Get_GetByPropertyId")]
        [ProducesResponseType(typeof(PropertyParameterDividedByGroupDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult GetPropertyParameterByPropertyId(int propertyId)
        {
            var response = PropertyParameterAppService.GetPropertyParameterByPropertyId(propertyId);

            return CreateResponseOnGet(response, EntityNames.PropertyParameter);
        }
        [HttpGet("{propertyId}/propertyparametersagelist")]
        [ThexAuthorize("PMS_PropertyParameter_Get_PropertyParametersAgeList")]
        [ProducesResponseType(typeof(IListDto<PropertyParameterDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult GetPropertyParameterAgeForRoomTypeByPropertyId(int propertyId)
        {
            var response = PropertyParameterAppService.GetChildrenAgeGroupForRoomTypeList(propertyId);

            return CreateResponseOnGetAll(response, EntityNames.PropertyParameter);
        }

        [HttpPut("updatesingle")]
        [ThexAuthorize("PMS_PropertyParameter_Update_UpdateSingle")]
        [ValidatePropertyIdFilter]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult Update([FromBody]PropertyParameterDto propertyParameterDto)
        {
            PropertyParameterAppService.UpdateParameter(propertyParameterDto);

            return CreateResponseOnPut(null, EntityNames.PropertyParameter);
        }

        [HttpPatch("{id}/toggleactivation")]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult ToggleActivation(Guid id)
        {
            PropertyParameterAppService.ToggleAndSaveIsActive(id);

            return CreateResponseOnPatch(null, EntityNames.PropertyParameter);
        }

        [HttpGet("{propertyId}/systemdate")]
        [ProducesResponseType(typeof(PropertyParameterDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult GetSystemdateByPropertyId(int propertyId)
        {
            var response = PropertyParameterAppService.GetSystemDate(propertyId);

            return CreateResponseOnGet(response, EntityNames.PropertyParameter);

        }
    }
}