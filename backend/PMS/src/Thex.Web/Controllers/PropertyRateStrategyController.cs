﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using Thex.Application.Interfaces;
using Thex.Domain;
using Thex.Dto;
using Thex.Kernel;
using Thex.Web.Base;
using Tnf.AspNetCore.Mvc.Response;
using Tnf.Dto;

namespace Thex.Web.Controllers
{
    [Route(RouteConsts.PropertyRateStrategy)]
    public class PropertyRateStrategyController : ThexAppController
    {
        private readonly IPropertyRateStrategyAppService _propertyRateStrategyAppService;

        public PropertyRateStrategyController(
            IApplicationUser applicationUser,
            IPropertyRateStrategyAppService propertyRateStrategyAppService) : base(applicationUser)
        {
            _propertyRateStrategyAppService = propertyRateStrategyAppService;
        }

        [HttpGet("getallbypropertyid")]
        [ThexAuthorize("PMS_PropertyRateStrategy_Get_GetAllByPropertyId")]
        [ProducesResponseType(typeof(IListDto<GetAllPropertyRateStrategyDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult GetAllByPropertyId()
        {
            var response = _propertyRateStrategyAppService.GetAllByPropertyId();
            return CreateResponseOnGetAll(response, EntityNames.PropertyRateStrategy);
        }

        [HttpGet("{id}")]
        [ThexAuthorize("PMS_PropertyRateStrategy_Get_Param")]
        [ProducesResponseType(typeof(PropertyRateStrategyDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Get(Guid id)
        {
            var response = await _propertyRateStrategyAppService.GetPropertyRateStrategyById(id);
            return CreateResponseOnGet(response, EntityNames.PropertyRateStrategy);
        }

        [HttpPost]
        [ThexAuthorize("PMS_PropertyRateStrategy_Post")]
        [ValidatePropertyIdFilter]
        [ProducesResponseType(typeof(PropertyRateStrategyDto), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Post([FromBody]PropertyRateStrategyDto propertyRateStrategyDto)
        {
            var response = await _propertyRateStrategyAppService.CreateAsync(propertyRateStrategyDto);
            return CreateResponseOnPost(response, EntityNames.PropertyRateStrategy);
        }

        [HttpPut("{id}")]
        [ThexAuthorize("PMS_PropertyRateStrategy_Put")]
        [ProducesResponseType(typeof(PropertyRateStrategyDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Put(Guid id, [FromBody]PropertyRateStrategyDto propertyRateStrategyDto)
        {
            var response = await _propertyRateStrategyAppService.UpdateAsync(id, propertyRateStrategyDto);
            return CreateResponseOnPut(response, EntityNames.PropertyRateStrategy);
        }

        [HttpPatch("{id}/toggleactivation")]
        [ThexAuthorize("PMS_PropertyRateStrategy_Patch_ToggleActivation")]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult ToggleActivation(Guid id)
        {
            _propertyRateStrategyAppService.ToggleAndSaveIsActive(id);
            return CreateResponseOnPatch(null, EntityNames.PropertyRateStrategy);
        }
    }
}
