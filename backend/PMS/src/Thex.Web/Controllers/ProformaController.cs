﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Thex.Application.Interfaces;
using Thex.Domain;
using Thex.Dto;
using Thex.Kernel;
using Thex.Web.Base;
using Tnf.AspNetCore.Mvc.Response;

namespace Thex.Web.Controllers
{
    [Route(RouteConsts.Proforma)]
    public class ProformaController : ThexAppController
    {
        private readonly IProformaAppService _proformaAppService;

        public ProformaController(
            IApplicationUser applicationUser,
            IProformaAppService proformaAppService)
            : base(applicationUser)
        {
            _proformaAppService = proformaAppService;
        }

        [HttpPost]
        [ThexAuthorize("PMS_Proforma_Post")]
        [ValidatePropertyIdFilter]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> CreateCreditNote([FromBody]ProformaRequestDto requestDto)
        {
            var response = await _proformaAppService.CreateProforma(requestDto);

            return CreateResponseOnPost(response, EntityNames.Proforma);
        }
    }
}
