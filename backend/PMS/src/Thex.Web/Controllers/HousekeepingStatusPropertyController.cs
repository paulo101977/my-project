﻿using System;
using Thex.Application.Interfaces;
using Thex.Dto;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Thex.Domain;
using Thex.Dto.Housekeeping;
using Thex.Dto.Room;
using System.Collections.Generic;
using Thex.Web.Base;
using Thex.Kernel;
using Tnf.AspNetCore.Mvc.Response;
using Tnf.Dto;
using Thex.Dto.HousekeepingStatusProperty;

namespace Thex.Web.Controllers
{
    [Route(RouteConsts.HousekeepingStatusProperty)]
    public class HousekeepingStatusPropertyController : ThexAppController
    {
        private readonly IHousekeepingStatusPropertyAppService HousekeepingStatusPropertyAppService;
        private readonly IRoomBlockingAppService RoomblockingAppService;

        public HousekeepingStatusPropertyController(
            IApplicationUser applicationUser,
            IHousekeepingStatusPropertyAppService housekeepingStatusPropertyAppService,
            IRoomBlockingAppService roomblockingAppService) : base(applicationUser)
        {
            HousekeepingStatusPropertyAppService = housekeepingStatusPropertyAppService;
            RoomblockingAppService = roomblockingAppService;
        }

        [HttpGet("{propertyId}/getallhousekeepingstatusproperty")]
        [ThexAuthorize("PMS_HouseKeepingStatusProperty_Get_GetAllHousekeepingStatusProperty")]
        [ProducesResponseType(typeof(IListDto<HousekeepingStatusPropertyDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult GetAll([FromQuery] GetAllHousekeepingStatusPropertyDto request, int propertyId)
        {
            var response = HousekeepingStatusPropertyAppService.GetAll(request, propertyId);
            return CreateResponseOnGetAll(response, EntityNames.HousekeepingStatusProperty);
        }

        [HttpGet("{id}")]
        [ProducesResponseType(typeof(HousekeepingStatusPropertyDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult GetHousekeepingStatusProperty(Guid id)
        {
            var response = HousekeepingStatusPropertyAppService.Get(id);

            return CreateResponseOnGet(response, EntityNames.HousekeepingStatusProperty);
        }

        [HttpPatch("{id}/toggleactivation")]
        [ThexAuthorize("PMS_HouseKeepingStatusProperty_Patch_ToggleActivation")]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult ToggleActivation(Guid id)
        {
            HousekeepingStatusPropertyAppService.ToggleAndSaveIsActive(id);

            return CreateResponseOnPatch(null, EntityNames.HousekeepingStatusProperty);
        }

        [HttpDelete("{id}/delete")]
        [ThexAuthorize("PMS_HouseKeepingStatusProperty_Delete")]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Delete(Guid id)
        {
            HousekeepingStatusPropertyAppService.HousekeepingStatusPropertyDelete(id);

            return CreateResponseOnDelete(EntityNames.HousekeepingStatusProperty);
        }

        [HttpDelete("{id}/unlockroomforavailability")]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> UnlockRoomForAvailability(Guid id)
        {
            RoomblockingAppService.UnlockRoomForAvailability(id);

            return CreateResponseOnDelete(EntityNames.HousekeepingStatusProperty);
        }

        [HttpPut]
        [ThexAuthorize("PMS_HouseKeepingStatusProperty_Put")]
        [ValidatePropertyIdFilter]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Update([FromBody]HousekeepingStatusPropertyDto housekeepingStatusProperty)
        {
            HousekeepingStatusPropertyAppService.Update(housekeepingStatusProperty);

            return CreateResponseOnPut(null, EntityNames.HousekeepingStatusProperty);
        }

        [HttpPost]
        [ThexAuthorize("PMS_HouseKeepingStatusProperty_Post")]
        [ValidatePropertyIdFilter]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Post([FromBody]HousekeepingStatusPropertyDto housekeepingStatusProperty)
        {
            HousekeepingStatusPropertyAppService.HousekeepingStatusPropertyCreate(housekeepingStatusProperty);

            return CreateResponseOnPost(null, EntityNames.HousekeepingStatusProperty);
        }

        [HttpGet("{propertyId}/getAllhousekeepingalterstatusroom")]
        [ThexAuthorize("PMS_HouseKeepingStatusProperty_Get_GetAllhouseKeepingAlterStatusRoom")]
        [ProducesResponseType(typeof(IListDto<GetAllHousekeepingAlterStatusDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult GetAllHousekeepingAlterStatusRoomDto([FromQuery]GetAllHousekeepingAlterStatusRoomDto request, int propertyId)
        {
            var response = HousekeepingStatusPropertyAppService.GetAllHousekeepingAlterStatusRoom(request, propertyId);
            return CreateResponseOnGetAll(response, EntityNames.HousekeepingStatusProperty);
        }

        [HttpGet("{propertyId}/getallhousekeepingstatuscreatebysystem")]
        [ProducesResponseType(typeof(IListDto<HousekeepingStatusPropertyDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> GetAllHousekeepingStatusCreateBySystem(int propertyId)
        {
            var response = HousekeepingStatusPropertyAppService.GetAllHousekeepingStatusCreateBySystem(propertyId);

            return CreateResponseOnGetAll(response, EntityNames.HousekeepingStatusProperty);
        }

        [HttpPut("room/{roomId}/housestatuskeeping/{housekeepingStatusPropertyId}/alterstatusroom")]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> UpdateRoomForHousekeeppingStatus(int roomId, Guid housekeepingStatusPropertyId)
        {
            HousekeepingStatusPropertyAppService.UpdateRoomForHousekeeppingStatus(roomId, housekeepingStatusPropertyId);

            return CreateResponseOnPut(null, EntityNames.HousekeepingStatusProperty);
        }

        [HttpPut("housestatuskeeping/{housekeepingStatusPropertyId}/altermanyroom")]
        [ThexAuthorize("PMS_HouseKeepingStatusProperty_Update_AlterManyRoom")]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> UpdateManyRoomForHousekeeppingStatus([FromBody]RoomIdsDto roomIds, Guid housekeepingStatusPropertyId)
        {
            HousekeepingStatusPropertyAppService.UpdateManyRoomForHousekeeppingStatus(roomIds.RoomIdsList, housekeepingStatusPropertyId);

            return CreateResponseOnPut(null, EntityNames.HousekeepingStatusProperty);
        }

        [HttpPost("blockingroom")]
        [ThexAuthorize("PMS_HouseKeepingStatusProperty_Post_BlockingRoom")]
        [ProducesResponseType(typeof(List<RoomWithReservationsDto>), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> BlockingRoom([FromBody]RoomBlockingDto roomBlocking)
        {
            var roomWithReservations = RoomblockingAppService.BlockingRoom(roomBlocking);

            return CreateResponseOnPost(roomWithReservations, EntityNames.HousekeepingStatusProperty);
        }
        [HttpPost("roomblockinglist")]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> BlockingRoomList([FromBody]List<RoomBlockingDto> roomBlockingList)
        {
            RoomblockingAppService.BlockingRoomList(roomBlockingList);

            return CreateResponseOnPost(null, EntityNames.HousekeepingStatusProperty);
        }
        
        [HttpDelete("{propertyId}/unlockroom")]
        [ThexAuthorize("PMS_HouseKeepingStatusProperty_Delete_Unlockroom")]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> UnlockRoom([FromBody]RoomIdsDto roomIds, int propertyId)
        {
            RoomblockingAppService.UnlockRoomList(roomIds.RoomIdsList, propertyId);

            return CreateResponseOnDelete(EntityNames.HousekeepingStatusProperty);
        }

    }
}
