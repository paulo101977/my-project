﻿using Thex.Application.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Thex.Domain;
using Thex.Dto.Reservation;
using Thex.Web.Base;
using Thex.Kernel;
using Tnf.AspNetCore.Mvc.Response;
using Thex.Dto.Availability;

namespace Thex.Web.Controllers
{
    [Route(RouteConsts.Availability)]
    public class AvailabilityController : ThexAppController
    {
        private readonly IAvailabilityAppService _availabilityAppService;

        public AvailabilityController(
            IApplicationUser applicationUser,
            IAvailabilityAppService availabilityAppService) : base(applicationUser)
        {
            _availabilityAppService = availabilityAppService;
        }

        [HttpGet("{propertyId}/roomtypes")]
        [ThexAuthorize("PMS_Availability_Get_RoomTypes",
                       "PMS_Property_Get_GetByPropertyId")]
        [ProducesResponseType(typeof(AvailabilityDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public virtual IActionResult GetAvailabilityRoomTypes([FromQuery] SearchAvailabilityDto requestDto, int propertyId)
        {
            var response = _availabilityAppService.GetAvailabilityRoomTypes(requestDto, propertyId);

            return CreateResponseOnGet(response, EntityNames.RoomType);
        }

        [HttpGet("{propertyId}/{roomTypeId}/rooms")]
        [ThexAuthorize("PMS_Availability_Get_Rooms")]
        [ProducesResponseType(typeof(AvailabilityRoomsDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public virtual IActionResult GetAvailabilityRooms([FromQuery] SearchAvailabilityDto requestDto, int propertyId, int roomTypeId)
        {
            var response = _availabilityAppService.GetAvailabilityRooms(requestDto, propertyId, roomTypeId);

            return CreateResponseOnGet(response, EntityNames.RoomType);
        }
    }
}
