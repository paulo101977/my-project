﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Thex.Application.Interfaces;
using Thex.Application.Services;
using Thex.Kernel;
using Thex.Domain;
using Thex.Dto;
using Thex.Web.Base;
using Tnf.AspNetCore.Mvc.Response;
using Tnf.Dto;

namespace Thex.Web.Controllers
{
    [Route(RouteConsts.ChannelCode)]
    public class ChannelCodeController : ThexAppController
    {
        private readonly IChannelCodeAppService _channelCodeAppService;

        public ChannelCodeController(
             IApplicationUser applicationUser,
             IChannelCodeAppService channelCodeAppService) : base(applicationUser)
        {
            _channelCodeAppService = channelCodeAppService;
        }

        [HttpGet]
        [ProducesResponseType(typeof(IListDto<ChannelCodeDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Get([FromQuery] GetAllChannelDto requestDto)
        {
            var response = await _channelCodeAppService.GetAllChannelsCodeDtoAsync();
            return CreateResponseOnGetAll(response, EntityNames.ChannelCode);
        }

        [HttpGet("{id}")]
        [ProducesResponseType(typeof(ChannelCodeDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Get(int id, [FromQuery] RequestDto requestDto)
        {
            var response = _channelCodeAppService.GetDtoById(id);
            return CreateResponseOnGet(response, EntityNames.ChannelCode);
        }

    }
}