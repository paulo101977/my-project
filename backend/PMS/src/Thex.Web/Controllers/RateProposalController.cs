﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Thex.Application.Interfaces.RateProposal;
using Thex.Common.Enumerations;
using Thex.Kernel;
using Thex.Domain;
using Thex.Dto;
using Thex.Web.Base;
using Tnf.AspNetCore.Mvc.Response;

namespace Thex.Web.Controllers
{
    [Route(RouteConsts.RateProposal)]
    public class RateProposalController : ThexAppController
    {
        private readonly IRateProposalAppService RateProposalAppService;
        public RateProposalController(IRateProposalAppService rateProposalAppService, 
            IApplicationUser applicationUser) : base(applicationUser)
        {
            RateProposalAppService = rateProposalAppService;
        }

        [HttpGet("{propertyId}")]
        [ThexAuthorize("PMS_RateProposal_Get")]
        [ProducesResponseType(typeof(ReservationItemBudgetHeaderDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> GetWithBudgetOffer([FromQuery] ReservationItemBudgetRequestDto requestDto, int propertyId)
        {
            var response = await RateProposalAppService.GetAllhBudgetOfferWithRateProposal(requestDto, propertyId);

            return CreateResponseOnGet(response, EntityNames.ReservationBudget);
        }
    }
}