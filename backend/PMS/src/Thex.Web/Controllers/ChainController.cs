﻿using Thex.Application.Interfaces;
using Thex.Dto;
using Tnf.Dto;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Thex.Kernel;
using Thex.Web.Base;
using Thex.Domain;
using Tnf.AspNetCore.Mvc.Response;

namespace Thex.Web.Controllers
{
    [Route(RouteConsts.Chain)]
    public class ChainController : ThexAppController
    {
        private readonly IChainAppService ChainAppService;

        public ChainController(
            IApplicationUser applicationUser,
            IChainAppService chainAppService)
            : base(applicationUser)
        {
            ChainAppService = chainAppService;
        }

        [HttpGet]
        [ThexAuthorize("PMS_Chain_Get")]
        [ProducesResponseType(typeof(IListDto<ChainDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Get([FromQuery] GetAllChainDto requestDto)
        {
            var response = await ChainAppService.GetAll(requestDto);

            return CreateResponseOnGetAll(response, EntityNames.Chain);
        }

        [HttpGet("{id}")]
        [ProducesResponseType(typeof(ChainDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Get(int id, RequestDto requestDto)
        {
            var request = new DefaultIntRequestDto(id, requestDto);

            var response = await ChainAppService.Get(request);

            return CreateResponseOnGet(response, EntityNames.Chain);
        }

        [HttpPut("{id}")]
        [ProducesResponseType(typeof(ChainDto), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Put(int id, [FromBody] ChainDto dto)
        {
            var response = await ChainAppService.Update(id, dto);

            return CreateResponseOnPut(response, EntityNames.Chain);
        }

        [HttpPost]
        [ProducesResponseType(typeof(ChainDto), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Post([FromBody] ChainDto dto)
        {
            var response = await ChainAppService.Create(dto);

            return CreateResponseOnPost(response, EntityNames.Chain);
        }

        [HttpDelete("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Delete(int id)
        {
            await ChainAppService.Delete(id);

            return CreateResponseOnDelete(EntityNames.Chain);
        }
    }
}
