﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using Thex.Application.Interfaces;
using Thex.Domain;
using Thex.Dto;
using Thex.Kernel;
using Thex.Web.Base;
using Tnf.AspNetCore.Mvc.Response;

namespace Thex.Web.Controllers
{
    [Route(RouteConsts.PropertyGuestPolicy)]
    public class PropertyGuestPolicyController : ThexAppController
    {
        private readonly IPropertyGuestPolicyAppService _propertyGuestPolicyAppService;

        public PropertyGuestPolicyController(
            IApplicationUser applicationUser,
            IPropertyGuestPolicyAppService propertyGuestPolicyAppService) : base(applicationUser)
        {
            _propertyGuestPolicyAppService = propertyGuestPolicyAppService;
        }

        [HttpPost]
        [ThexAuthorize("PMS_PropertyGuestPolicy_Post")]
        [ValidatePropertyIdFilter]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult Create([FromBody]PropertyGuestPolicyDto propertyPolicyDto)
        {
            _propertyGuestPolicyAppService.Create(propertyPolicyDto);

            return CreateResponseOnPost(null, EntityNames.PropertyGuestPolicy);
        }

        [HttpGet("{id}")]
        [ThexAuthorize("PMS_PropertyGuestPolicy_GetAll")]
        [ValidatePropertyIdFilter]
        [ProducesResponseType(typeof(PropertyGuestPolicyDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult GetPropertyPolicy(Guid id)
        {
            var response = _propertyGuestPolicyAppService.GetById(id);

            return CreateResponseOnGet(response, EntityNames.PropertyGuestPolicy);

        }

        [HttpGet]
        [ThexAuthorize("PMS_PropertyGuestPolicy_Get")]
        [ValidatePropertyIdFilter]
        [ProducesResponseType(typeof(List<PropertyGuestPolicyDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult GetPropertyPolicy()
        {
            var response = _propertyGuestPolicyAppService.GetAll();

            return CreateResponseOnGet(response, EntityNames.PropertyGuestPolicy);

        }

        [HttpPut]
        [ThexAuthorize("PMS_PropertyGuestPolicy_Update")]
        [ValidatePropertyIdFilter]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult Update([FromBody]PropertyGuestPolicyDto propertyGuestPolicyDto)
        {
            _propertyGuestPolicyAppService.Update(propertyGuestPolicyDto);

            return CreateResponseOnPut(null, EntityNames.PropertyGuestPolicy);
        }

        [HttpDelete("{id}")]
        [ThexAuthorize("PMS_PropertyGuestPolicy_Delete")]
        [ValidatePropertyIdFilter]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult Delete(Guid id)
        {
            _propertyGuestPolicyAppService.Delete(id);

            return CreateResponseOnDelete(EntityNames.PropertyGuestPolicy);
        }
    }
}
