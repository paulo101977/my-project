﻿using Thex.Application.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Thex.Domain;
using Thex.Dto.ReservationGuest;
using Thex.Web.Base;
using Thex.Kernel;
using Tnf.AspNetCore.Mvc.Response;
using Tnf.Dto;

namespace Thex.Web.Controllers
{
    [Route(RouteConsts.ReservationGuest)]
    public class ReservationGuestController : ThexAppController
    {
        private readonly IReservationAppService ReservationAppService;

        public ReservationGuestController(
            IApplicationUser applicationUser,
            IReservationAppService reservationAppService) : base(applicationUser)
        {
            ReservationAppService = reservationAppService;
        }

        [HttpGet("{propertyId}")]
        [ThexAuthorize("PMS_ReservationGuest_Get_Param")]
        [ProducesResponseType(typeof(IListDto<ReservationGuestResultDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public virtual IActionResult Get([FromQuery] GetAllReservationGuestDto requestDto, int propertyId)
        {
            var response = ReservationAppService.GetReservationGuest(requestDto, propertyId);

            return CreateResponseOnGetAll(response, EntityNames.TransportationType);
        }
    }
}
