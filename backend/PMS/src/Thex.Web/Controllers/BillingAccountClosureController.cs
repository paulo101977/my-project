﻿using Thex.Application.Interfaces;
using Thex.Dto;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Thex.Domain;
using System.Linq;
using Thex.Kernel;
using Thex.Web.Base;
using Tnf.AspNetCore.Mvc.Response;

namespace Thex.Web.Controllers
{
    [Route(RouteConsts.BillingAccountClosure)]
    public class BillingAccountClosureController : ThexAppController
    {
        private readonly IBillingAccountClosureAppService _billingAccountClosureAppService;
        private readonly IIntegrationAppService _integrationAppService;

        public BillingAccountClosureController(
            IApplicationUser applicationUser,
            IBillingAccountClosureAppService billingAccountClosureAppService,
            IIntegrationAppService integrationAppService) : base(applicationUser)
        {
            _billingAccountClosureAppService = billingAccountClosureAppService;
            _integrationAppService = integrationAppService;
        }

        [HttpPost]
        [ThexAuthorize("PMS_BillingAccountClosure_Post")]
        [ValidatePropertyIdFilter]
        [ProducesResponseType(typeof(BillingAccountClosureResultDto), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Post([FromBody] BillingAccountClosureDto dto)
        {
            var response = await _billingAccountClosureAppService.Create(dto);

            return CreateResponseOnPost(response, EntityNames.BillingInvoice);
        }


        [HttpPost("partial")]
        [ThexAuthorize("PMS_BillingAccountClosure_Post_Partial")]
        [ValidatePropertyIdFilter]
        [ProducesResponseType(typeof(BillingAccountClosureResultDto), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> PostPartial([FromBody] BillingAccountClosurePartial dto)
        {
            var response = await _billingAccountClosureAppService.CreatePartial(dto);
            return CreateResponseOnPost(response, EntityNames.BillingInvoice);
        }
    }
}
