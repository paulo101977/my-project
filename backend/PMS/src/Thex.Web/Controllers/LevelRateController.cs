﻿using Microsoft.AspNetCore.Mvc;
using System;
using Thex.Application.Interfaces;
using Thex.Domain;
using Thex.Dto;
using Thex.Kernel;
using Thex.Web.Base;
using Tnf.AspNetCore.Mvc.Response;
using Tnf.Dto;


namespace Thex.Web.Controllers
{
    [Route(RouteConsts.LevelRate)]
    public class LevelRateController : ThexAppController
    {
        private readonly ILevelRateAppService _levelRateAppService;

        public LevelRateController
            (IApplicationUser applicationUser,
            ILevelRateAppService levelRateAppService) : base(applicationUser)
        {
            _levelRateAppService = levelRateAppService;
        }

        [HttpGet]
        [ProducesResponseType(typeof(IListDto<LevelRateHeaderDto>), 200)]
        [ProducesResponseType(404)]
        [ThexAuthorize("PMS_Level_Rate_Get_All")]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult GetAllLevelRates()
        {
            var response = _levelRateAppService.GetAllDto();

            return CreateResponseOnGetAll(response, EntityNames.LevelRate);
        }

        [HttpGet("{id}")]
        [ProducesResponseType(typeof(LevelRateResultDto), 200)]
        [ProducesResponseType(404)]
        [ThexAuthorize("PMS_Level_Rate_Get")]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult GetById(Guid id)
        {
            var response = _levelRateAppService.GetById(id);

            return CreateResponseOnGet(response, EntityNames.LevelRate);
        }

        [HttpPost]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ThexAuthorize("PMS_Level_Rate_Post")]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult Create([FromBody] LevelRateResultDto dto)
        {
            _levelRateAppService.Create(dto);

            return CreateResponseOnPost(null, EntityNames.LevelRate);
        }

        [HttpPut("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ThexAuthorize("PMS_Level_Rate_Put")]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult Update([FromBody] LevelRateResultDto dto, Guid id)
        {
            _levelRateAppService.Update(dto, id);

            return CreateResponseOnPut(null, EntityNames.LevelRate);
        }

        [HttpGet("getalllevelsavailable")]
        [ProducesResponseType(typeof(LevelRateResultDto), 200)]
        [ProducesResponseType(404)]
        [ThexAuthorize("PMS_Level_Rate_Get_All_Levels_Available")]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult GetAllLevelAvailable([FromQuery] LevelRateRequestDto request)
        {
            var response = _levelRateAppService.GetAllLevelsAvailable(request);

            return CreateResponseOnGet(response, EntityNames.LevelRate);
        }

        [HttpPatch("toggleisactive/{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ThexAuthorize("PMS_Level_Rate_Toggle")]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult ToggleIsActive(Guid id)
        {
            _levelRateAppService.ToggleActive(id);

            return CreateResponseOnPatch(null, EntityNames.LevelRate);
        }

        [HttpDelete("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ThexAuthorize("PMS_Level_Rate_Delete")]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult Remove(Guid id)
        {
            _levelRateAppService.Remove(id);

            return CreateResponseOnDelete(null, EntityNames.LevelRate);
        }

        [HttpGet("getalllevelratesavailable")]
        [ProducesResponseType(typeof(IListDto<PropertyBaseRateByLevelRateResultDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult GetAllLevelRateForPropertyBaseRate([FromQuery] DateTime initialDate, [FromQuery] DateTime endDate, [FromQuery]Guid? currencyId)
        {
            var response = _levelRateAppService.GetAllLevelRateForPropertyBaseRate(initialDate, endDate, currencyId);

            return CreateResponseOnGetAll(response, EntityNames.LevelRate);
        }
    }
}