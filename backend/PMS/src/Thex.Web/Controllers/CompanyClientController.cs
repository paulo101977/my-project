﻿using System;
using Thex.Application.Interfaces;
using Thex.Dto;
using Tnf.Dto;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Thex.Web.Controllers
{
    using Thex.Kernel;
    using Thex.Domain;
    using Thex.Dto.Person;
    using Thex.Web.Base;
    using Tnf.AspNetCore.Mvc.Response;
    using System.Collections.Generic;

    [Route(RouteConsts.CompanyClient)]
    public class CompanyClientController : ThexAppController
    {
        private readonly ICompanyClientAppService CompanyClientAppService;

        public CompanyClientController(
            IApplicationUser applicationUser,
            ICompanyClientAppService companyClientAppService)
            : base(applicationUser)
        {
            CompanyClientAppService = companyClientAppService;
        }

        [HttpGet]
        [ThexAuthorize("PMS_CompanyClient_Get")]
        [ProducesResponseType(typeof(IListDto<GetAllCompanyClientResultDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Get([FromQuery] GetAllCompanyClientDto requestDto)
        {
            int propertyId = requestDto.PropertyId;
            var response = CompanyClientAppService.GetAllByPropertyId(requestDto, propertyId);

             return CreateResponseOnGetAll(response, EntityNames.CompanyClient);
        }

        [HttpGet("{id}")]
        [ThexAuthorize("PMS_CompanyClient_Get_Params")]
        [ProducesResponseType(typeof(CompanyClientDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Get(string id, RequestDto requestDto)
        {
            var request = new DefaultStringRequestDto(id, requestDto);

            var response = await CompanyClientAppService.Get(request);

            return CreateResponseOnGet(response, EntityNames.CompanyClient);
        }

        [HttpPut("{id}")]
        [ThexAuthorize("PMS_CompanyClient_Put")]
        [ProducesResponseType(typeof(CompanyClientDto), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Put(string id, [FromBody] CompanyClientDto dto)
        {
            var response = await CompanyClientAppService.Update(id, dto);

            return CreateResponseOnPut(response, EntityNames.CompanyClient);
        }

        [HttpPost]
        [ThexAuthorize("PMS_CompanyClient_Post")]
        [ProducesResponseType(typeof(CompanyClientDto), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Post([FromBody] CompanyClientDto dto)
        {
            var response = await CompanyClientAppService.Create(dto);

            return CreateResponseOnPost(response, EntityNames.CompanyClient);
        }

        [HttpDelete("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Delete(string id)
        {
            await CompanyClientAppService.Delete(id);

            return CreateResponseOnDelete(EntityNames.CompanyClient);
        }

        [HttpGet("search")]
        [ThexAuthorize("PMS_CompanyClient_Get_Search")]
        [ProducesResponseType(typeof(IListDto<GetAllCompanyClientSearchResultDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult Search([FromQuery] GetAllCompanyClientSearchDto requestDto)
        {
            int propertyId = requestDto.PropertyId;
            var response = CompanyClientAppService.GetAllBasedOnFilter(requestDto, propertyId);

            return CreateResponseOnGetAll(response, EntityNames.CompanyClient);
        }


        [HttpGet("searchwithlocation")]
        [ThexAuthorize("PMS_CompanyClient_Get_SearchWithLocation")]
        [ProducesResponseType(typeof(IListDto<SearchPersonsResultDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult SearchWithLocation([FromQuery] GetAllCompanyClientSearchDto requestDto)
        {
            int propertyId = requestDto.PropertyId;
            var response = CompanyClientAppService.GetAllBasedOnFilterWithLocation(requestDto, propertyId);

            return CreateResponseOnGetAll(response, EntityNames.CompanyClient);
        }


        [HttpGet("contactpersons")]
        [ThexAuthorize("PMS_CompanyClient_Get_ContactPersons")]
        [ProducesResponseType(typeof(IListDto<GetAllCompanyClientContactInformationSearchResultDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult ContactPerson([FromQuery] GetAllCompanyClientContactInformationSearchDto requestDto)
        {
            var response = CompanyClientAppService.GetAllContactPersonByCompanyClientId(requestDto);

            return CreateResponseOnGetAll(response, EntityNames.CompanyClient);
        }


        [HttpPatch("{id}/toggleactivation")]
        [ThexAuthorize("PMS_CompanyClient_Patch_ToggleActivation")]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult ToggleActivation(string id)
        {
            CompanyClientAppService.ToggleActivation(id);

            return CreateResponseOnPatch(null, EntityNames.CompanyClient);
        }


        [HttpGet("isacquirer")]
        [ThexAuthorize("PMS_CompanyClient_Get_IsAcquirer")]
        [ProducesResponseType(typeof(IListDto<CompanyClientDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult GetAcquirer([FromQuery] GetAllCompanyClientDto requestDto)
        {
            var response = CompanyClientAppService.GetAcquirerCompanyClient(requestDto);

            return CreateResponseOnGetAll(response, EntityNames.CompanyClient);
        }


        [HttpGet("checkavailablechannelassociation")]
        [ProducesResponseType(typeof(bool), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult CheckAvailableChannelAssociation([FromQuery] CompanyClientChannelDto dto)
        {
            var response = CompanyClientAppService.CheckAvailableChannelAssociation(dto);

            return CreateResponseOnGet(response, EntityNames.CompanyClient);
        }

        [HttpPatch("associatewithtributswithheld")]
        [ThexAuthorize("PMS_CompanyClient_Patch_Associate_With_Tributs_Withheld")]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> AssociateWithTributsWithheld([FromBody] List<CompanyClientWithTributeWihheldDto> dtoList)
        {
            await CompanyClientAppService.AssociateClientListWithTributsWithheld(dtoList);

            return CreateResponseOnPatch(null, EntityNames.CompanyClient);
        }

        [HttpGet("getallwithtributwithheld")]
        [ThexAuthorize("PMS_CompanyClient_Get_Associate_With_Tributs_Withheld")]
        [ProducesResponseType(typeof(List<GetAllCompanyClientResultDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> GetAllWithTributWithheld()
        {
            var response = await CompanyClientAppService.GetAllAssociatedWithTributWithheld();

            return CreateResponseOnGet(response, EntityNames.CompanyClient);
        }

        [HttpDelete("deleteassociationwithtributwithheld/{id}")]
        [ThexAuthorize("PMS_CompanyClient_Delete_Association_With_Tribut_Withheld")]
        [ProducesResponseType(typeof(CompanyClientDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> GetAllClientWithImpostoRetido(Guid id)
        {
            await CompanyClientAppService.DeleteAssociationClientListWithTributsWithheld(id);

            return CreateResponseOnDelete();
        }
    }
}
