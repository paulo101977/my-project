﻿using Microsoft.AspNetCore.Mvc;
using Thex.Application.Interfaces;
using Thex.Domain;
using Thex.Dto;
using Thex.Kernel;
using Thex.Web.Base;
using Tnf.AspNetCore.Mvc.Response;

namespace Thex.Web.Controllers
{
    [Route(RouteConsts.BillingAccountItemIntegration)]
    public class BillingAccountItemIntegrationController : ThexAppController
    {
        private readonly IIntegrationAppService _integrationAppService;
        protected readonly IBillingAccountItemAppService _billingAccountItemAppService;

        public BillingAccountItemIntegrationController(
            IIntegrationAppService integrationAppService,
            IApplicationUser applicationUser,
            IBillingAccountItemAppService billingAccountItemAppService) : base(applicationUser)
        {
            _integrationAppService = integrationAppService;
            _billingAccountItemAppService = billingAccountItemAppService;
        }

        [HttpPost("v2/expenditure")]
        [ProducesResponseType(typeof(BillingAccountItemIntegrationDto), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult CreateDebitAndCreditWithInvocie([FromBody] BillingAccountItemIntegrationDto dto)
        {
            var response = _billingAccountItemAppService.CreateDebitAndCreditWithInvoice(dto);

            return CreateResponseOnPost(response, EntityNames.BillingAccountItem);
        }

        [HttpPost("debit")]
        [ProducesResponseType(typeof(BillingAccountItemDebitDto), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult PostDebit([FromBody] BillingAccountItemDebitDto dto)
        {
            var response = _billingAccountItemAppService.CreateDebitIntegration(dto);

            return CreateResponseOnPost(response, EntityNames.BillingAccountItem);
        }

        [HttpPost("reversal")]
        [ProducesResponseType(typeof(BillingAccountItemReversalParentDto), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult PostReversal([FromBody] BillingAccountItemReversalParentDto dto)
        {
            var response = _billingAccountItemAppService.CreateReversalIntegration(dto);

            return CreateResponseOnPost(response, EntityNames.BillingAccountItem);
        }
    }
}
