﻿using System;
using Thex.Application.Interfaces;
using Thex.Dto;
using Tnf.Dto;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Thex.Domain;
using Thex.Dto.Person;
using Thex.Kernel;
using Thex.Web.Base;
using Tnf.AspNetCore.Mvc.Response;

namespace Thex.Web.Controllers
{
    [Route(RouteConsts.Guest)]
    public class GuestController : ThexAppController
    {
        private readonly IGuestAppService GuestAppService;

        public GuestController(
            IApplicationUser applicationUser,
            IGuestAppService guestAppService)
            : base(applicationUser)
        {
            GuestAppService = guestAppService;
        }

        [HttpGet("search")]
        [ThexAuthorize("PMS_Guest_Get_Search")]
        [ProducesResponseType(typeof(SearchPersonsResultDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult Search([FromQuery]SearchPersonsDto request)
        {
            var response = GuestAppService.SearchBasedOnFilter(request);

            return CreateResponseOnGet(response, EntityNames.Person);
        }
    }
}
