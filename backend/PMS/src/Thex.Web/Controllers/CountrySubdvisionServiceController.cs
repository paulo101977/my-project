﻿using Thex.Application.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Thex.Domain;
using Thex.Web.Base;
using Thex.Kernel;
using Tnf.AspNetCore.Mvc.Response;
using Tnf.Dto;
using Thex.Dto;

namespace Thex.Web.Controllers
{
    [Route(RouteConsts.CountrySubdvisionService)]
    public class CountrySubdvisionServiceController : ThexAppController
    {
        private readonly ICountrySubdvisionServiceAppService CountrySubdvisionServiceAppService;
        public CountrySubdvisionServiceController(
              IApplicationUser applicationUser,
            ICountrySubdvisionServiceAppService countrySubdvisionServiceAppService)
            : base(applicationUser)
        {
            CountrySubdvisionServiceAppService = countrySubdvisionServiceAppService;
        }

        [HttpGet("{propertyId}/getall")]
        [ThexAuthorize("PMS_CountrySubdvisionService_Get_GetAll")]
        [ProducesResponseType(typeof(IListDto<CountrySubdvisionServiceDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult GetAllByPropertyId(int propertyId)
        {
            var response = CountrySubdvisionServiceAppService.GetAllByPropertyId(propertyId);
            return CreateResponseOnGetAll(response, EntityNames.CountrySubdvisionService);
        }
    }
}
