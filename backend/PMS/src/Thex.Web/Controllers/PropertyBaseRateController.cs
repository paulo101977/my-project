﻿using Thex.Application.Interfaces;
using Thex.Dto;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Thex.Domain;
using Thex.Kernel;
using Thex.Web.Base;
using Tnf.AspNetCore.Mvc.Response;

namespace Thex.Web.Controllers
{
    [Route(RouteConsts.PropertyBaseRate)]
    public class PropertyBaseRateController : ThexAppController
    {
        private readonly IPropertyBaseRateAppService PropertyBaseRateAppService;

        public PropertyBaseRateController(
            IApplicationUser applicationUser,
            IPropertyBaseRateAppService propertyBaseRateAppService) : base(applicationUser)
        {
            PropertyBaseRateAppService = propertyBaseRateAppService;
        }

        [HttpGet("{propertyId}/configurationDataParameters")]
        [ThexAuthorize("PMS_PropertyBaseRate_Get_ConfigurationDataParameters")]
        [ProducesResponseType(typeof(PropertyBaseRateConfigurationParametersDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> GetConfigurationDataParameters(int propertyId)
        {
            var response = await PropertyBaseRateAppService.GetConfigurationDataParameters(propertyId);

            return CreateResponseOnGet(response, EntityNames.PropertyBaseRate);
        }

        [HttpPost]
        [ValidatePropertyIdFilter]
        [ThexAuthorize("PMS_PropertyBaseRate_Post")]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Post([FromBody] BaseRateConfigurationDto dto)
        {
            await PropertyBaseRateAppService.Create(dto);

            return CreateResponseOnPost(null, EntityNames.PropertyBaseRate);
        }

        [HttpPost("createbylevel")]
        [ValidatePropertyIdFilter]
        [ThexAuthorize("PMS_PropertyBaseRate_Post_By_Level")]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> PostByLevel([FromBody] PropertyBaseLevelRateDto dto)
        {
            await PropertyBaseRateAppService.CreateByLevelRate(dto);

            return CreateResponseOnPost(null, EntityNames.PropertyBaseRate);
        }

        [HttpGet("{propertyId}/PropertyBaseRateHeaderHistory")]
        [ThexAuthorize("PMS_PropertyBaseRate_Get_PropertyBaseRateHeaderHistory")]
        [ProducesResponseType(typeof(PropertyBaseRateHeaderHistoryDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> GetPropertyBaseRateHeaderHistory(int propertyId)
        {
            var response = await PropertyBaseRateAppService.GetPropertyBaseRateHeaderHistory(propertyId);
            return CreateResponseOnGet(response, EntityNames.PropertyBaseRate);
        }


        [HttpPost("createbypremise")]
        [ValidatePropertyIdFilter]
        [ThexAuthorize("PMS_PropertyBaseRate_Post_By_Premise")]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> PostByPremise([FromBody] BaseRateConfigurationDto dto)
        {
            await PropertyBaseRateAppService.CreateByPremise(dto);
            return CreateResponseOnPost(null, EntityNames.PropertyBaseRate);
        }

    }
}
