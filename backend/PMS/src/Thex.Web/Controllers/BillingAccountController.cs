﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.Application.Interfaces;
using Thex.Domain;
using Thex.Dto;
using Thex.Dto.BillingAccount;
using Thex.Dto.Person;
using Thex.Kernel;
using Thex.Web.Base;
using Tnf.AspNetCore.Mvc.Response;
using Tnf.Dto;

namespace Thex.Web.Controllers
{
    [Route(RouteConsts.BillingAccount)]
    public class BillingAccountController : ThexAppController
    {
        private readonly IBillingAccountAppService BillingAccountAppService;

        public BillingAccountController(
            IApplicationUser applicationUser,
            IBillingAccountAppService billingAccountAppService)
            : base(applicationUser)
        {
            BillingAccountAppService = billingAccountAppService;
        }

        [HttpGet]
        [ProducesResponseType(typeof(IListDto<BillingAccountDto>), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Get([FromQuery] GetAllBillingAccountDto requestDto)
        {
            var response = await BillingAccountAppService.GetAll(requestDto);

            return CreateResponseOnGetAll(response, EntityNames.BillingAccount);
        }

        [HttpGet("{id}")]
        [ProducesResponseType(typeof(BillingAccountDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Get(Guid id, [FromQuery] RequestDto requestDto)
        {
            var request = new DefaultGuidRequestDto(id, requestDto);

            var response = await BillingAccountAppService.Get(request);

            return CreateResponseOnGet(response, EntityNames.BillingAccount);
        }

        [HttpPut("{id}")]
        [ValidatePropertyIdFilter]
        [ProducesResponseType(typeof(BillingAccountDto), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Put(Guid id, [FromBody] BillingAccountDto dto)
        {
            var response = await BillingAccountAppService.Update(id, dto);

            return CreateResponseOnPut(response, EntityNames.BillingAccount);
        }

        [HttpPost]
        [ThexAuthorize("PMS_BillingAccount_Post")]
        [ValidatePropertyIdFilter]
        [ProducesResponseType(typeof(BillingAccountDto), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Post([FromBody] BillingAccountDto dto)
        {
            var response = await BillingAccountAppService.Create(dto);

            return CreateResponseOnPost(response, EntityNames.BillingAccount);
        }

        [HttpDelete("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Delete(Guid id)
        {
            await BillingAccountAppService.Delete(id);

            return CreateResponseOnDelete(EntityNames.BillingAccount);
        }

        [HttpGet("{propertyId}/search")]
        [ThexAuthorize("PMS_BillingAccount_Get_Search")]
        [ProducesResponseType(typeof(IListDto<SearchBillingAccountResultDto>), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult GetAllByFilters([FromQuery] SearchBillingAccountDto request, int propertyId)
        {
            var response = BillingAccountAppService.GetAllByFilters(request, propertyId);

            return CreateResponseOnGetAll(response, EntityNames.BillingAccount);
        }

        [HttpPost("sparce")]
        [ThexAuthorize("PMS_BillingAccount_Post_Sparce")]
        [ValidatePropertyIdFilter]
        [ProducesResponseType(typeof(BillingAccountDto), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult PostSparceAccount([FromBody] BillingAccountDto dto)
        {
            var response = BillingAccountAppService.CreateSparceAccount(dto);

            return CreateResponseOnPost(response, EntityNames.BillingAccount);
        }

        [HttpGet("{id}/header")]
        [ThexAuthorize("PMS_BillingAccount_Get_Header")]
        [ProducesResponseType(typeof(BillingAccountHeaderDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult GetHeaderBillingAccount(Guid id)
        {
            var response = BillingAccountAppService.BillingAccountHeader(id);

            return CreateResponseOnGet(response, EntityNames.BillingAccount);
                                         
        }

        [HttpGet("{id}/sidebar")]
        [ThexAuthorize("PMS_BillingAccount_Get_Sidebar")]
        [ProducesResponseType(typeof(IListDto<BillingAccountSidebarDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult GetSidebarBillingAccount(Guid id)
        {
            var response = BillingAccountAppService.BillingAccountSidebar(id);
            return CreateResponseOnGetAll(response, EntityNames.BillingAccount);
        }

        [HttpGet("{id}/accountentries")]
        [ThexAuthorize("PMS_BillingAccount_Get_AccountEntries")]
        [ProducesResponseType(typeof(BillingAccountWithAccountsDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult GetWithAccountEntries(Guid id, RequestDto requestDto)
        {
            var request = new DefaultGuidRequestDto(id, requestDto);

            var response = BillingAccountAppService.GetWithAccountEntries(request);

            return CreateResponseOnGet(response, EntityNames.BillingAccount);
        }

        [HttpGet("{id}/invoices")]
        [ThexAuthorize("PMS_BillingAccount_Get_AccountEntries")]
        [ProducesResponseType(typeof(InvoiceDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult GetInvoicesAccount(Guid id, RequestDto requestDto)
        {
            var request = new DefaultGuidRequestDto(id, requestDto);

            var response = BillingAccountAppService.GetInvoicesAccount(request);

            return CreateResponseOnGet(response, EntityNames.BillingInvoice);
        }

        [HttpPost("accountentrieslist")]
        [ThexAuthorize("PMS_BillingAccount_Post_AccountEntriesList")]
        [ProducesResponseType(typeof(IListDto<BillingAccountWithAccountsDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult GetWithAccountEntriesList([FromBody] List<Guid> ids)
        {
            var response = BillingAccountAppService.GetAllWithAccountEntriesList(ids);

            return CreateResponseOnGetAll(response, EntityNames.BillingAccount);
        }

        [HttpGet("{id}/accountentriescomplete")]
        [ThexAuthorize("PMS_BillingAccount_Get_AccountEntriesComplete")]
        [ProducesResponseType(typeof(BillingAccountWithCompleteAccountDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult GetWithAccountEntriesComplete(Guid id)
        {
            var response = BillingAccountAppService.GetAllWithAccountEntriesComplete(id);

            return CreateResponseOnGet(response, EntityNames.BillingAccount);
        }

        [HttpGet("{id}/statement")]
        [ThexAuthorize("PMS_BillingAccount_Get_Statement")]
        [ProducesResponseType(typeof(BillingAccountWithAccountsDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult GetBillingAccountStatement(Guid id, RequestDto requestDto)
        {
            var request = new DefaultGuidRequestDto(id, requestDto);

            var response = BillingAccountAppService.GetBillingAccountStatement(request);

            return CreateResponseOnGet(response, EntityNames.BillingAccount);
        }

        [HttpPost("add")]
        [ThexAuthorize("PMS_BillingAccount_Post_Add")]
        [ProducesResponseType(typeof(BillingAccountDto), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult AddBillingAccount([FromBody] AddBillingAccountDto dto)
        {
            var response = BillingAccountAppService.AddBillingAccount(dto);

            return CreateResponseOnPost(response, EntityNames.BillingAccount);
        }

        [HttpGet("{id}/{allSimiliarAccounts}/forclosure")]
        [ThexAuthorize("PMS_BillingAccount_Get_ForClosure")]
        [ProducesResponseType(typeof(BillingAccountTotalForClosureDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult GetBillingAccountsForClosure(Guid id, bool allSimiliarAccounts)
        {
            var response = BillingAccountAppService.GetBillingAccountsForClosure(id, allSimiliarAccounts);

            return CreateResponseOnGet(response, EntityNames.BillingAccount);
        }

        [HttpGet("{propertyId}/{id}/uhorsparceaccounts")]
        [ThexAuthorize("PMS_BillingAccount_Get_UhOrSparceAccounts")]
        [ProducesResponseType(typeof(IListDto<SearchBillingAccountResultDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult GetAllBillingAccountsByUhOrSparseAccount(int propertyId, Guid id)
        {
            var response = BillingAccountAppService.GetAllBillingAccountsByUhOrSparseAccount(id, propertyId);

            return CreateResponseOnGetAll(response, EntityNames.BillingAccount);
        }


        [HttpGet("{propertyId}/transfersearch")]
        [ThexAuthorize("PMS_BillingAccount_Get_TransferSearch")]
        [ProducesResponseType(typeof(IListDto<SearchBillingAccountResultDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult GetAllBasedOnTransferSearch([FromQuery] SearchBillingAccountDto request, int propertyId)
        {
            var response = BillingAccountAppService.GetAllByFiltersForTransferSearch(request, propertyId);

            return CreateResponseOnGetAll(response, EntityNames.BillingAccount);
        }


        [HttpPost("{propertyId}/{billingAccountId}/{reasonId}/reopen")]
        [ThexAuthorize("PMS_BillingAccount_Post_Reopen")]
        [ValidatePropertyIdFilter]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult ReopenBillingAccount(int propertyId, Guid billingAccountId, int reasonId)
        {
            BillingAccountAppService.ReopenBillingAccount(propertyId, billingAccountId, reasonId);

            return CreateResponseOnPost(null, EntityNames.BillingAccount);
        }

        [HttpPatch("associatepersonheader")]
        [ThexAuthorize("PMS_BillingAccount_Patch_Person_Header")]
        [ValidatePropertyIdFilter]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult AssociatePersonHeader([FromBody] HeaderPersonDto personDto)
        {
            var person = BillingAccountAppService.AssociatePersonHeader(personDto);

            return CreateResponseOnPost(person, EntityNames.Person);
        }

        [HttpGet("getpersonheaderbybillingaccountid/{billingAccountId}")]
        [ThexAuthorize("PMS_BillingAccount_Get_Person_Header")]
        [ValidatePropertyIdFilter]
        [ProducesResponseType(typeof(HeaderPersonDto), 200)]
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult GetPersonHeaderByBillingAccountId(Guid billingAccountId)
        {
            var response = BillingAccountAppService.GetHeaderByBillingAccountId(billingAccountId);

            if (response == null)
            {
                return CreateResponse()
                .WithCode("204")
                .Build();
            }
            else
            {
                return CreateResponseOnGet(response, EntityNames.Person);
            }
        }

    }
}
