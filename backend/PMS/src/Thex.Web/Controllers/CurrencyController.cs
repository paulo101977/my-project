﻿using Thex.Application.Interfaces;
using Thex.Dto;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Thex.Domain;
using Thex.Web.Base;
using Thex.Kernel;
using Tnf.AspNetCore.Mvc.Response;
using Tnf.Dto;

namespace Thex.Web.Controllers
{
    [Route(RouteConsts.Currency)]
    public class CurrencyController : ThexAppController
    {
        private readonly ICurrencyAppService CurrencyAppService;

        public CurrencyController(
            IApplicationUser applicationUser,
            ICurrencyAppService currencyAppService) : base(applicationUser)
        {
            CurrencyAppService = currencyAppService;
        }

        [HttpGet]
        [ThexAuthorize("PMS_Currency_Get")]
        [ProducesResponseType(typeof(IListDto<CurrencyDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Get([FromQuery] GetAllCurrencyDto requestDto)
        {
            var response = await CurrencyAppService.GetAll(requestDto);

            return CreateResponseOnGetAll(response, EntityNames.Currency);
        }
    }
}
