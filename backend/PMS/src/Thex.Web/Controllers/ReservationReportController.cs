﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.Application.Interfaces.ReservationReport;
using Thex.Common.Enumerations;
using Thex.Kernel;
using Thex.Domain;
using Thex.Dto.Report;
using Thex.Dto.Reservation;
using Thex.Dto.ReservationReport;
using Thex.Dto.Room;
using Thex.Dto.SearchReservationReportDto;
using Thex.Web.Base;
using Tnf.AspNetCore.Mvc.Response;
using Tnf.Dto;
using Thex.Dto;

namespace Thex.Web.Controllers
{
    [Route(RouteConsts.ReservationReport)]
    public class ReservationReportController : ThexAppController
    {
        protected readonly IReservationReportAppService ReservationReportAppService;

        public ReservationReportController(
            IApplicationUser applicationUser,
            IReservationReportAppService reservationReportAppService) : base(applicationUser)
        {
            ReservationReportAppService = reservationReportAppService;
        }

        [HttpGet("{propertyId}/search")]
        [ThexAuthorize("PMS_ReservationReport_Get_Search")]
        [ProducesResponseType(typeof(IListDto<SearchReservationReportResultDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult GetAllByFilters([FromQuery] SearchReservationReportDto request, int propertyId)
        {         
            var response = ReservationReportAppService.GetAllByFilters(request, propertyId);

            return CreateResponseOnGetAll(response, EntityNames.Reservation);
        }

        [HttpGet("{propertyId}/searchCheck")]
        [ThexAuthorize("PMS_ReservationReport_Get_SearchCheck")]
        [ProducesResponseType(typeof(SearchReservationCheckReportResultList), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult GetAllCheckByFilters([FromQuery] SearchReservationReportDto request, int propertyId)
        {
            var response = ReservationReportAppService.GetAllCheckByFilters(request, propertyId);

            return CreateResponseOnGet(response, EntityNames.Reservation);
        }


        [HttpGet("{propertyId}/searchMeanPlan")]
        [ThexAuthorize("PMS_ReservationReport_Get_SearchMeanPlan")]
        [ProducesResponseType(typeof(IListDto<SearchMeanPlanControlReportResultDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult GetAllMeanPlanByFilters([FromQuery] SearchReservationReportDto request, int propertyId)
        {
            var response = ReservationReportAppService.GetAllMeanPlanByFilters(request, propertyId);

            return CreateResponseOnGetAll(response, EntityNames.Reservation);
        }

        [HttpGet("{propertyId}/{guestSale}/{sparseSale}/{masterGroupSale}/{companySale}/{eventSale}/{pendingSale}/{positiveSale}/" +
            "{canceledSale}/{noShowSale}/{sale}/{aheadSale}/searchGuestSale")]
        [ThexAuthorize("PMS_ReservationReport_Get_SearchGuestSale")]
        [ProducesResponseType(typeof(IListDto<SearchGuestSaleReportResultDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult GetAllGuestSaleByFilters(int propertyId, bool guestSale,
            bool sparseSale, bool masterGroupSale, bool companySale, bool eventSale, bool pendingSale, bool positiveSale, bool canceledSale,
            bool noShowSale, double? sale, bool aheadSale)
        {
            var response = ReservationReportAppService.GetAllGuestSaleByFilters(propertyId, guestSale, sparseSale, masterGroupSale,
                companySale, eventSale, pendingSale, positiveSale, canceledSale, noShowSale, sale, aheadSale);

            return CreateResponseOnGetAll(response, EntityNames.Reservation);
        }

        [HttpPost("MeanPlan")]
        [ThexAuthorize("PMS_ReservationReport_Post_MeanPlan")]
        [ProducesResponseType(typeof(PostMeanPlanControlReportDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult PostMeanPlan([FromBody] PostMeanPlanControlReportDto request)
        {
            var response = ReservationReportAppService.PostMeanPlan(request);

            return CreateResponseOnPost(response, EntityNames.Reservation);
        }

        [HttpGet("{propertyId}/IssuedNotes")]
        [ThexAuthorize("PMS_ReservationReport_Get_IssuedNotes")]
        [ProducesResponseType(typeof(ListSearchIssueNotesReportResultDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult GetIssuedNotes(int propertyId, [FromQuery] SearchIssuedNotesReportDto requestDto)
        {
            var response = ReservationReportAppService.GetAllIssuedNotes(propertyId, requestDto);
            return CreateResponseOnGet(response, EntityNames.Reservation);
        }

        [HttpGet("{propertyId}/UhSituation")]
        [ThexAuthorize("PMS_ReservationReport_Get_UhSituation")]
        [ProducesResponseType(typeof(IListDto<SearchReservationUhReportResultReturnDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult GetAllUhSituationByFilters([FromQuery] GetAllSituationRoomsDto requestDto, int propertyId)
        {
            var response = ReservationReportAppService.GetAllUhSituationByFilters(requestDto, propertyId);
            return CreateResponseOnGetAll(response, EntityNames.Reservation);
        }

        [HttpGet("{propertyId}/Grafics")]
        [ThexAuthorize("PMS_ReservationReport_Get_Grafics",
                       "PMS_Property_Get_GetByPropertyId")]
        [ProducesResponseType(typeof(ReportReturnDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> GetGrafics(int propertyId)
        {
            var response = await ReservationReportAppService.GetGraficReport(propertyId);
            return CreateResponseOnGet(response);
        }

        [HttpGet("{propertyId}/Bordereau")]
        [ThexAuthorize("PMS_ReservationReport_Get_Bordereau")]
        [ProducesResponseType(typeof(ListSearchBordereauReportResultDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult GetBordereau(int propertyId, [FromQuery] SearchBordereauReportDto requestDto)
        {
            var response = ReservationReportAppService.GetBordereau(requestDto, propertyId);
            return CreateResponseOnGet(response, EntityNames.Reservation);
        }

        [HttpGet("{propertyId}/HousekeepingStatusDisagreement")]
        [ThexAuthorize("PMS_ReservationReport_Get_HousekeepingStatusDisagreement")]
        [ProducesResponseType(typeof(IListDto<ReportHousekeepingStatusDisagreementReturnDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult GetAllHousekeepingStatusDisagreement(int propertyId, [FromQuery] SearchReportHousekeepingStatusDisagreementDto requestDto)
        {
            var response = ReservationReportAppService.GetAllHousekeepingStatusDisagreement(requestDto, propertyId);
            return CreateResponseOnGetAll(response, EntityNames.Reservation);
        }

        [HttpPost("HousekeepingRoomDisagreement")]
        [ThexAuthorize("PMS_ReservationReport_Post_HousekeepingRoomDisagreement")]
        [ProducesResponseType(typeof(List<PostHousekeepingRoomDisagreementDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult HousekeepingRoomDisagreement([FromBody] List<PostHousekeepingRoomDisagreementDto> request)
        {
            var response = ReservationReportAppService.PostHousekeepingRoomDisagreement(request);

            return CreateResponseOnPost(response, EntityNames.Reservation);
        }

        [HttpGet("{propertyId}/ReservationsMade")]
        [ThexAuthorize("PMS_ReservationReport_Get_ReservationsMade")]
        [ProducesResponseType(typeof(ReportReservationsMadeReturnListDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult GetAllHousekeepingStatusDisagreement(int propertyId, [FromQuery] SearchReservationsMadeReportDto requestDto)
        {
            var response = ReservationReportAppService.GetReservationsMade(requestDto, propertyId);
            return CreateResponseOnGet(response, EntityNames.Reservation);
        }

        [HttpGet("{propertyId}/ReservationsInAdvance")]
        [ThexAuthorize("PMS_ReservationReport_Get_ReservationsInAdvance")]
        [ProducesResponseType(typeof(IListDto<ReservationsInAdvanceReturnDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult GetAllReservationsInAdvance(int propertyId, [FromQuery] SearchReservationsInAdvanceDto requestDto)
        {
            var response = ReservationReportAppService.GetAllReservationsInAdvance(requestDto, propertyId);
            return CreateResponseOnGetAll(response, EntityNames.Reservation);
        }

        [HttpGet("propertydashboard")]
        [ThexAuthorize("PMS_ReservationReport_Get_PropertyDashboard")]
        [ProducesResponseType(typeof(PropertyDashboardDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> PropertyDashboardAsync()
        {
            var response = await ReservationReportAppService.GetDashboardInformationsAsync();

            return CreateResponseOnGet(response, EntityNames.Reservation);
        }
    }
}