﻿using Thex.Application.Interfaces;
using Thex.Dto;
using Microsoft.AspNetCore.Mvc;
using Thex.Domain;
using Thex.Common;
using System.Threading.Tasks;
using Thex.Kernel;
using Thex.Web.Base;
using Tnf.AspNetCore.Mvc.Response;
using Tnf.Dto;

namespace Thex.Web.Controllers
{
    [Route(RouteConsts.Reason)]
    public class ReasonController : ThexAppController
    {
        protected readonly IReasonAppService ReasonAppService;
        protected readonly IReasonCategoryAppService ReasonCategoryAppService;
        protected readonly Domain.Interfaces.Integration.IInvoiceIntegrationBrazil _INFEIntegration;

        public ReasonController(
            IApplicationUser applicationUser,
            IReasonAppService reasonAppService,
            IReasonCategoryAppService reasonCategoryAppService,
            Domain.Interfaces.Integration.IInvoiceIntegrationBrazil INFEIntegration) : base(applicationUser)
        {
            LocalizationSourceName = AppConsts.LocalizationSourceName;
            ReasonAppService = reasonAppService;
            ReasonCategoryAppService = reasonCategoryAppService;
            _INFEIntegration = INFEIntegration;
        }

        [HttpGet("getallreasoncategory")]
        [ThexAuthorize("PMS_Reason_Get_GetAllReasonCategory")]
        [ProducesResponseType(typeof(IListDto<ReasonCategoryGetAllDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult GetAllReasonCategory()
        {
            var response = ReasonCategoryAppService.GetAllReasonCategory();
            return CreateResponseOnGetAll(response, EntityNames.ReasonCategory);
        }

        [HttpGet]
        [ThexAuthorize("PMS_Reason_Get",
                       "PMS_Property_Get_GetByPropertyId")]
        [ProducesResponseType(typeof(IListDto<ReasonDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult Get([FromQuery] GetAllReasonDto requestDto)
        {
            var response = ReasonAppService.GetAllByChainIdAndReasonCategory(requestDto);

            return CreateResponseOnGetAll(response, EntityNames.Reason);
        }

        [HttpGet("{propertyId}/{reasonCategoryId}/getallreason")]
        [ThexAuthorize("PMS_Reason_Get_GetAllReason_Params")]
        [ProducesResponseType(typeof(IListDto<ReasonDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult Get([FromQuery] GetAllReasonDto requestDto, int propertyId, int reasonCategoryId)
        {
            var response = ReasonAppService.GetAllByPropertyIdAndReasonCategoryId(requestDto, propertyId, reasonCategoryId);

            return CreateResponseOnGetAll(response, EntityNames.Reason);
        }

        [HttpGet("{propertyId}/getallreason")]
        [ThexAuthorize("PMS_Reason_Get_GetAllReason")]
        [ProducesResponseType(typeof(IListDto<ReasonDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult GetAllByPropertyId([FromQuery] GetAllReasonDto requestDto, int propertyId)
        {
            var response = ReasonAppService.GetAllByPropertyId(requestDto, propertyId);

            return CreateResponseOnGetAll(response, EntityNames.Reason);
        }

        [HttpGet("{reasonId}/getreason")]
        [ProducesResponseType(typeof(ReasonDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult GetReasonByReasonId(int reasonId)
        {
            var response = ReasonAppService.GetReasonByReasonId(reasonId);
            return CreateResponseOnGet(response, EntityNames.Reason);
        }

        [HttpPatch("{reasonId}/toggleactivation")]
        [ThexAuthorize("PMS_Reason_Patch_ToggleActivation")]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult ToggleIsActive(int reasonId)
        {
            ReasonAppService.ToggleAndSaveIsActive(reasonId);

            return CreateResponseOnPatch(null, EntityNames.Reason);
        }

        [HttpDelete("{reasonId}/delete")]
        [ThexAuthorize("PMS_Reason_Delete")]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult Delete(int reasonId)
        {
            ReasonAppService.Delete(reasonId);

            return CreateResponseOnDelete(EntityNames.Reason);
        }

        [HttpPut]
        [ThexAuthorize("PMS_Reason_Put")]
        [ValidatePropertyIdFilter]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult Update([FromBody] ReasonDto reasonDto)
        {
            ReasonAppService.Update(reasonDto);

            return CreateResponseOnPut(null, EntityNames.Reason);
        }

        [HttpPost]
        [ThexAuthorize("PMS_Reason_Post")]
        [ValidatePropertyIdFilter]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Create([FromBody] ReasonDto reasonDto)
        {
             ReasonAppService.CreateReason(reasonDto);

            return CreateResponseOnPost(null, EntityNames.Reason);
        }
    }
}


