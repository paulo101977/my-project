﻿using Thex.Application.Interfaces;
using Thex.Dto;
using Tnf.Dto;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Thex.Dto.RoomTypes;
using Thex.Domain;
using Thex.Web.Base;
using Thex.Kernel;
using Tnf.AspNetCore.Mvc.Response;

namespace Thex.Web.Controllers
{
    [Route(RouteConsts.RoomType)]
    public class RoomTypeController : ThexAppController
    {
        protected readonly IRoomTypeAppService RoomTypeAppService;

        public RoomTypeController(
            IApplicationUser applicationUser,
            IRoomTypeAppService roomTypeAppService) : base(applicationUser)
        {
            RoomTypeAppService = roomTypeAppService;
        }

        [HttpGet]
        [ProducesResponseType(typeof(IListDto<RoomTypeDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Get([FromQuery] GetAllRoomTypeDto requestDto)
        {
            var response = await RoomTypeAppService.GetAll(requestDto);

            return CreateResponseOnGetAll(response, EntityNames.RoomType);
        }

        [HttpGet("{id}")]
        [ThexAuthorize("PMS_RoomType_Get")]
        [ProducesResponseType(typeof(RoomTypeDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Get(int id, [FromQuery] RequestDto requestDto)
        {
            var response = RoomTypeAppService.GetRoomTypeById(id);

            return CreateResponseOnGet(response, EntityNames.RoomType);
        }

        [HttpPut("{id}")]
        [ThexAuthorize("PMS_RoomType_Put")]
        [ValidatePropertyIdFilter]
        [ProducesResponseType(typeof(RoomTypeDto), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult Put(int id, [FromBody] RoomTypeDto dto)
        {
            var response = RoomTypeAppService.UpdateRoomType(id, dto);

            return CreateResponseOnPut(response, EntityNames.RoomType);
        }

        [HttpPost]
        [ThexAuthorize("PMS_RoomType_Post")]
        [ValidatePropertyIdFilter]
        [ProducesResponseType(typeof(RoomTypeDto), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult Post([FromBody] RoomTypeDto dto)
        {
            var response = RoomTypeAppService.CreateRoomType(dto);

            return CreateResponseOnPost(response, EntityNames.Room);
        }
        
        [HttpPatch("{id}/toggleactivation")]
        [ThexAuthorize("PMS_RoomType_Patch_ToggleActivation")]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult ToggleActivation(int id)
        {
            RoomTypeAppService.ToggleActivation(id);

            return CreateResponseOnPatch(null, EntityNames.Room);
        }
        
        [HttpGet("{propertyId}/withoverbooking")]
        [ThexAuthorize("PMS_RoomType_Get_WithOverbooking")]
        [ProducesResponseType(typeof(IListDto<RoomTypeOverbookingDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult GetAll([FromQuery]GetAllRoomTypesByPropertyAndPeriodDto request, int propertyId)
        {
            var response = RoomTypeAppService.GetAllRoomTypesByPropertyAndPeriodDto(request, propertyId);

            return CreateResponseOnGetAll(response, EntityNames.RoomType);
        }


        [HttpDelete("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ThexAuthorize("PMS_RoomType_Delete")]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult Delete(int id)
        {
            RoomTypeAppService.DeleteRoomTypeAsync(id);
             
            return CreateResponseOnDelete(null, EntityNames.RoomType);
        }
    }              
}
