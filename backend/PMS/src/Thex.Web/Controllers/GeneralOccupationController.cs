﻿using Thex.Application.Interfaces;
using Thex.Dto;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Thex.Domain;
using Thex.Kernel;
using Thex.Web.Base;
using Tnf.AspNetCore.Mvc.Response;
using Tnf.Dto;

namespace Thex.Web.Controllers
{
    [Route(RouteConsts.GeneralOccupation)]
    public class GeneralOccupationController : ThexAppController
    {
        private readonly IGeneralOccupationAppService GeneralOccupationAppService;
        public GeneralOccupationController(
            IApplicationUser applicationUser,
            IGeneralOccupationAppService generalOccupationAppService) : base(applicationUser)        {
            GeneralOccupationAppService = generalOccupationAppService;
        }

        [HttpGet]
        [ThexAuthorize("PMS_GeneralOccupation_Get")]
        [ProducesResponseType(typeof(IListDto<GeneralOccupationDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public virtual async Task<IActionResult> Get([FromQuery] GetAllGeneralOccupationDto requestDto)
        {
            var response = await GeneralOccupationAppService.GetAll(requestDto);

            return CreateResponseOnGetAll(response, EntityNames.GeneralOccupation);
        }

        

        
    }
}
