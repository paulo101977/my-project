﻿using Thex.Application.Interfaces;
using Thex.Dto;
using Tnf.Dto;
using Microsoft.AspNetCore.Mvc;
using Thex.Dto.RoomTypes;
using Thex.Dto.Rooms;
using Thex.Domain;
using Thex.Kernel;
using Thex.Web.Base;
using Tnf.AspNetCore.Mvc.Response;
using System.Threading.Tasks;

namespace Thex.Web.Controllers
{
    [Route(RouteConsts.Property)]
    public class PropertyController : ThexAppController
    {
        private readonly IRoomTypeAppService RoomTypeAppService;
        private readonly IRoomAppService RoomAppService;
        private readonly IReservationAppService ReservationAppService;
        private readonly IPropertyAppService PropertyAppService;

        public PropertyController(
            IApplicationUser applicationUser,
            IPropertyAppService propertyAppService, IRoomTypeAppService roomTypeAppService, IRoomAppService roomAppService, IReservationAppService reservationAppService) : base(applicationUser)
        {
            PropertyAppService = propertyAppService;
            RoomTypeAppService = roomTypeAppService;
            RoomAppService = roomAppService;
            ReservationAppService = reservationAppService;
        }

        [HttpGet("getall")]
        [ProducesResponseType(typeof(IListDto<PropertyDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult Get([FromQuery] GetAllPropertyDto requestDto)
        {
            var response = PropertyAppService.GetAllProperty(requestDto);

            return CreateResponseOnGetAll(response, EntityNames.Property);
        }

        [HttpGet("{id}")]
        [ProducesResponseType(typeof(PropertyDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult Get(int id, [FromQuery] RequestDto requestDto)
        {
            var request = new DefaultIntRequestDto(id, requestDto);
            var response = PropertyAppService.GetProperty(request);

            return CreateResponseOnGet(response, EntityNames.Property);
        }

        [HttpGet("{id}/getbypropertyid")]
        [ThexAuthorize("PMS_Property_Get_GetByPropertyId")]
        [ProducesResponseType(typeof(PropertyDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult GetByPropertyId(int id)
        {
            var response = PropertyAppService.GetByPropertyId(id);

            return CreateResponseOnGet(response, EntityNames.Property);
        }

        [HttpPut]
        [ThexAuthorize("PMS_Property_Update")]
        [ValidatePropertyIdFilter("id")]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult Put([FromBody] PropertyDto dto)
        {
            PropertyAppService.UpdateProperty(dto.Id, dto);

            return CreateResponseOnPut(null, EntityNames.Property);
        }

        [HttpPost]
        [ValidatePropertyIdFilter("id")]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult Post([FromBody] PropertyDto dto)
        {
            PropertyAppService.CreateProperty(dto);

            return CreateResponseOnPost(null, EntityNames.Property);
        }

        [HttpDelete("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult Delete(int id)
        {
            PropertyAppService.Delete(id);

            return CreateResponseOnDelete(EntityNames.Property);
        }

        [HttpGet("{id}/roomtypes")]
        [ThexAuthorize("PMS_Property_Get_RoomTypes")]
        [ProducesResponseType(typeof(IListDto<RoomTypeDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult Get([FromQuery] GetAllRoomTypesDto request, int id)
        {
            var response = RoomTypeAppService.GetRoomTypesByPropertyId(request, id);

            return CreateResponseOnGetAll(response, EntityNames.Property);
        }

        [HttpGet("{id}/rooms")]
        [ThexAuthorize("PMS_Property_Get_Rooms")]
        [ProducesResponseType(typeof(IListDto<RoomDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult Get([FromQuery] GetAllRoomsDto request, int id)
        {
            var response = RoomAppService.GetRoomsByPropertyId(request, id);

            return CreateResponseOnGetAll(response, EntityNames.Property);
        }

        [HttpGet("{id}/parentrooms")]
        [ThexAuthorize("PMS_Property_Get_ParentRooms")]
        [ProducesResponseType(typeof(IListDto<RoomDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult GetParentRoomsByPropertyId([FromQuery] GetAllRoomsDto request, int id)
        {
            var response = RoomAppService.GetParentRoomsByPropertyId(request, id);

            return CreateResponseOnGetAll(response, EntityNames.Property);
        }

        [HttpGet("{id}/roomswithoutchildren")]
        [ProducesResponseType(typeof(IListDto<RoomDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult GetRoomsWithoutChildrenByPropertyId([FromQuery] GetAllRoomsDto request, int id)
        {
            var response = RoomAppService.GetRoomsWithoutChildrenByPropertyId(request, id);

            return CreateResponseOnGetAll(response, EntityNames.Property);
        }

        [HttpGet("{id}/roomswithoutparentandchildren")]
        [ProducesResponseType(typeof(IListDto<RoomDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult GetRoomsWithoutParentAndChildrenByPropertyId([FromQuery] GetAllRoomsDto request, int id)
        {
            var response = RoomAppService.GetRoomsWithoutParentAndChildrenByPropertyId(request, id);

            return CreateResponseOnGetAll(response, EntityNames.Property);
        }

        [HttpGet("{id}/reservations")]
        [ProducesResponseType(typeof(IListDto<ReservationDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult Get([FromQuery] RequestAllDto request, int id)
        {
            var response = ReservationAppService.GetReservationsByPropertyId(request, id);

            return CreateResponseOnGetAll(response, EntityNames.Property);
        }


        [HttpPatch("changephoto")]
        [ThexAuthorize("PMS_Property_ChangePhoto")]
        [ProducesResponseType(typeof(PropertyDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> ChangePhoto([FromBody] PropertyDto request)
        {
            var response =  await PropertyAppService.SendPhoto(request);

            return CreateResponseOnPatch(response, EntityNames.Property);
        }
    }
}