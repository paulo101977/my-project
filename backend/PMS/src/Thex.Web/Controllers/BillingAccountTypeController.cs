﻿using Thex.Application.Interfaces;
using Thex.Dto;
using Tnf.Dto;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Thex.Kernel;
using Thex.Web.Base;
using Thex.Domain;
using Tnf.AspNetCore.Mvc.Response;

namespace Thex.Web.Controllers
{
    [Route(RouteConsts.BillingAccountType)]
    public class BillingAccountTypeController : ThexAppController
    {
        private readonly IBillingAccountTypeAppService BillingAccountTypeAppService;

        public BillingAccountTypeController(
            IApplicationUser applicationUser,
            IBillingAccountTypeAppService billingAccountTypeAppService)
            : base(applicationUser)
        {
            BillingAccountTypeAppService = billingAccountTypeAppService;
        }

        [HttpGet]
        [ProducesResponseType(typeof(IListDto<BillingAccountTypeDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Get([FromQuery]GetAllBillingAccountTypeDto requestDto)
        {
            var response = await BillingAccountTypeAppService.GetAll(requestDto);

            return CreateResponseOnGetAll(response, EntityNames.BillingAccountType);
        }

        [HttpGet("{id}")]
        [ProducesResponseType(typeof(BillingAccountTypeDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Get(int id, [FromQuery]RequestDto requestDto)
        {
            var request = new DefaultIntRequestDto(id, requestDto);

            var response = await BillingAccountTypeAppService.Get(request);

            return CreateResponseOnGet(response, EntityNames.BillingAccountType);
        }

        [HttpPut("{id}")]
        [ProducesResponseType(typeof(BillingAccountTypeDto), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Put(int id, [FromBody]BillingAccountTypeDto dto)
        {
            var response = await BillingAccountTypeAppService.Update(id, dto);

            return CreateResponseOnPut(response, EntityNames.BillingAccountType);
        }

        [HttpPost]
        [ProducesResponseType(typeof(BillingAccountTypeDto), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Post([FromBody]BillingAccountTypeDto dto)
        {
            var response = await BillingAccountTypeAppService.Create(dto);

            return CreateResponseOnPost(response, EntityNames.BillingAccountType);
        }

        [HttpDelete("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Delete(int id)
        {
            await BillingAccountTypeAppService.Delete(id);

            return CreateResponseOnDelete(EntityNames.BillingAccountType);
        }
    }
}
