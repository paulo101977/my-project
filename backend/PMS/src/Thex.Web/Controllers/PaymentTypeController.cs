﻿using Thex.Application.Interfaces;
using Thex.Dto;
using Microsoft.AspNetCore.Mvc;
using Thex.Domain;
using Thex.Common;

namespace Thex.Web.Controllers
{
    using System.Threading.Tasks;
    using Thex.Kernel;
    using Thex.Web.Base;
    using Tnf.AspNetCore.Mvc.Response;
    using Tnf.Dto;

    [Route(RouteConsts.PaymentType)]
    public class PaymentTypeController : ThexAppController
    {
        protected readonly IPaymentTypeAppService PaymentTypeAppService;

        public PaymentTypeController(
            IApplicationUser applicationUser,
            IPaymentTypeAppService paymentTypeAppService) : base(applicationUser)
        {
            LocalizationSourceName = AppConsts.LocalizationSourceName;
            PaymentTypeAppService = paymentTypeAppService;
        }

        [HttpGet]
        [ProducesResponseType(typeof(IListDto<PaymentTypeDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public virtual async Task<IActionResult> Get([FromQuery] GetAllPaymentTypeDto requestDto)
        {
            var response = await PaymentTypeAppService.GetAll(requestDto);

            return CreateResponseOnGetAll(response, EntityNames.PaymentType);
        }

        [HttpGet("forcreation")]
        [ThexAuthorize("PMS_PaymentType_Get_ForCreation")]
        [ProducesResponseType(typeof(IListDto<PaymentTypeDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public virtual async Task<IActionResult> GetAllForCreation([FromQuery] GetAllPaymentTypeDto requestDto)
        {
            var response = await PaymentTypeAppService.GetAllForCreation(requestDto);

            return CreateResponseOnGetAll(response, EntityNames.PaymentType);
        }
    }
}


