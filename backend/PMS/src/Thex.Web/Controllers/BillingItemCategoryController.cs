﻿using Thex.Application.Interfaces;
using Thex.Dto;
using Tnf.Dto;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Thex.Domain;
using Thex.Dto.BillingItemCategory;
using Thex.Kernel;
using Thex.Web.Base;
using Tnf.AspNetCore.Mvc.Response;

namespace Thex.Web.Controllers
{
    [Route(RouteConsts.BillingItemCategory)]
    public class BillingItemCategoryController : ThexAppController
    {
        private readonly IBillingItemCategoryAppService BillingItemCategoryAppService;
        public BillingItemCategoryController(
            IApplicationUser applicationUser,
            IBillingItemCategoryAppService billingItemCategoryAppService)
            : base(applicationUser)
        {
            BillingItemCategoryAppService = billingItemCategoryAppService;
        }

        [HttpGet]
        [ProducesResponseType(typeof(IListDto<BillingItemCategoryDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Get([FromQuery] GetAllBillingItemCategoryDto requestDto)
        {
            var response = await BillingItemCategoryAppService.GetAll(requestDto);

            return CreateResponseOnGetAll(response, EntityNames.BillingItemCategory);
        }

        [HttpGet("{id}")]
        [ThexAuthorize("PMS_BillingItemCategory_Get_Params")]
        [ProducesResponseType(typeof(BillingItemCategoryDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Get(int id, RequestDto requestDto)
        {
            var request = new DefaultIntRequestDto(id, requestDto);

            var response = await BillingItemCategoryAppService.Get(request);

            return CreateResponseOnGet(response, EntityNames.BillingItemCategory);
        }

        [HttpPut("{id}")]
        [ThexAuthorize("PMS_BillingItemCategory_Put")]
        [ValidatePropertyIdFilter]
        [ProducesResponseType(typeof(BillingItemCategoryDto), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Put(int id, [FromBody] BillingItemCategoryDto dto)
        {
            var response = await BillingItemCategoryAppService.Update(id, dto);

            return CreateResponseOnPut(response, EntityNames.BillingItemCategory);
        }

        [HttpDelete("{id}")]
        [ThexAuthorize("PMS_BillingItemCategory_Delete")]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Delete(int id)
        {
            await BillingItemCategoryAppService.Delete(id);

            return CreateResponseOnDelete(EntityNames.BillingItemCategory);
        }

        [HttpPost]
        [ThexAuthorize("PMS_BillingItemCategory_Post")]
        [ValidatePropertyIdFilter]
        [ProducesResponseType(typeof(BillingItemCategoryDto), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Post([FromBody] BillingItemCategoryDto dto)
        {
            var response = await BillingItemCategoryAppService.Create(dto);

            return CreateResponseOnPost(response, EntityNames.BillingItemCategory);
        }

        [HttpDelete("{id}/delete")]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult  DeleteBillingItemCategory(int id)
        {
            BillingItemCategoryAppService.DeleteBillingCategory(id);

            return CreateResponseOnDelete(EntityNames.BillingItemCategory);
        }

        [HttpGet("groupfixed")]
        [ThexAuthorize("PMS_BillingItemCategory_Get_GroupFixed")]
        [ProducesResponseType(typeof(IListDto<BillingItemCategoryDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult GetAllGroupFixed()
        {
            var response = BillingItemCategoryAppService.GetAllGroupFixed();

            return CreateResponseOnGetAll(response, EntityNames.BillingItemCategory);
        }

        [HttpGet("{id}/billingItemCategory")]
        [ThexAuthorize("PMS_BillingItemCategory_Get_BillingItemCategory")]
        [ProducesResponseType(typeof(IListDto<BillingItemCategoryDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult GetAllGroupByPropertyId([FromQuery] GetAllBillingItemCategoryDto request, int id)
        {
            var response = BillingItemCategoryAppService.GetAllGroupByPropertyId(request, id);

            return CreateResponseOnGetAll(response, EntityNames.BillingItemCategory);
        }

        [HttpGet("billingItemsByCategoryId/{id}")]
        [ProducesResponseType(typeof(IListDto<BillingItemCategoryDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult GetItemsByGroupId(int id)
        {
            var response = BillingItemCategoryAppService.GetItemsByGroupId(id);

            return CreateResponseOnGetAll(response, EntityNames.BillingItemCategory);
        }

        [HttpGet("{propertyId}/getallcategoriesforitem")]
        [ThexAuthorize("PMS_BillingItemCategory_Get_GetAllCategoriesForItem")]
        [ProducesResponseType(typeof(IListDto<GetAllCategoriesForItemDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult GetAllGroupByPropertyId(int propertyId)
        {
            var response = BillingItemCategoryAppService.GetAllCategoriesForItem(propertyId);

            return CreateResponseOnGetAll(response, EntityNames.BillingItemCategory);
        }

        [HttpPatch("{id}/toggleactivation")]
        [ThexAuthorize("PMS_BillingItemCategory_Patch_ToggleActivation")]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult ToggleActivation(int id)
        {
            BillingItemCategoryAppService.ToggleActivation(id);

            return CreateResponseOnPatch(null, EntityNames.BillingItemCategory);
        }


    }
}
