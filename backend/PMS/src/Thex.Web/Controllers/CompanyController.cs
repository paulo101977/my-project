﻿using Thex.Application.Interfaces;
using Thex.Dto;
using Tnf.Dto;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Thex.Kernel;
using Thex.Web.Base;
using Thex.Domain;
using Tnf.AspNetCore.Mvc.Response;

namespace Thex.Web.Controllers
{
    [Route(RouteConsts.Company)]
    public class CompanyController : ThexAppController
    {
        private readonly ICompanyAppService CompanyAppService;

        public CompanyController(
            IApplicationUser applicationUser,
            ICompanyAppService companyAppService)
            : base(applicationUser)
        {
            CompanyAppService = companyAppService;
        }

        [HttpGet]
        [ProducesResponseType(typeof(IListDto<CompanyDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Get([FromQuery] GetAllCompanyDto requestDto)
        {
            var response = await CompanyAppService.GetAll(requestDto);

            return CreateResponseOnGetAll(response, EntityNames.Company);
        }

        [HttpGet("{id}")]
        [ProducesResponseType(typeof(CompanyDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Get(int id, RequestDto requestDto)
        {
            var request = new DefaultIntRequestDto(id, requestDto);

            var response = await CompanyAppService.Get(request);

            return CreateResponseOnGet(response, EntityNames.Company);
        }

        [HttpPut("{id}")]
        [ProducesResponseType(typeof(CompanyDto), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Put(int id, [FromBody] CompanyDto dto)
        {
            var response = await CompanyAppService.Update(id, dto);

            return CreateResponseOnPut(response, EntityNames.Company);
        }

        [HttpPost]
        [ProducesResponseType(typeof(CompanyDto), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Post([FromBody] CompanyDto dto)
        {
            var response = await CompanyAppService.Create(dto);

            return CreateResponseOnPost(response, EntityNames.Company);
        }

        [HttpDelete("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Delete(int id)
        {
            await CompanyAppService.Delete(id);

            return CreateResponseOnDelete(EntityNames.Company);
        }
    }
}
