﻿using System;
using Thex.Application.Interfaces;
using Thex.Dto;
using Tnf.Dto;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Thex.Domain;
using Thex.Dto.Person;
using Thex.Kernel;
using Thex.Web.Base;
using Tnf.AspNetCore.Mvc.Response;

namespace Thex.Web.Controllers
{
    [Route(RouteConsts.Person)]
    public class PersonController : ThexAppController
    {
        private readonly IPersonAppService PersonAppService;

        public PersonController(
            IApplicationUser applicationUser,
            IPersonAppService personAppService)
            : base(applicationUser)
        {
            PersonAppService = personAppService;
        }

        [HttpGet]
        [ProducesResponseType(typeof(IListDto<PersonDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Get([FromQuery] GetAllPersonDto requestDto)
        {
            var response = await PersonAppService.GetAll(requestDto);

            return CreateResponseOnGetAll(response, EntityNames.Person);
        }

        [HttpGet("{id}")]
        [ProducesResponseType(typeof(PersonDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Get(Guid id, [FromQuery] RequestDto requestDto)
        {
            var request = new DefaultGuidRequestDto(id, requestDto);

            var response = await PersonAppService.Get(request);

            return CreateResponseOnGet(response, EntityNames.Person);
        }

        [HttpPut("{id}")]
        [ProducesResponseType(typeof(PersonDto), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Put(Guid id, [FromBody] PersonDto dto)
        {
            var response = await PersonAppService.Update(id, dto);

            return CreateResponseOnPut(response, EntityNames.Person);
        }

        [HttpPost]
        [ProducesResponseType(typeof(PersonDto), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Post([FromBody] PersonDto dto)
        {
            var response = await PersonAppService.Create(dto);

            return CreateResponseOnPost(response, EntityNames.Person);
        }

        [HttpDelete("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Delete(Guid id)
        {
            await PersonAppService.Delete(id);

            return CreateResponseOnDelete(EntityNames.Person);
        }

        [HttpGet("search")]
        [ProducesResponseType(typeof(IListDto<SearchPersonsResultDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult Search([FromQuery]SearchPersonsDto request)
        {
            var response = PersonAppService.GetPersonsBasedOnFilter(request);

            return CreateResponseOnGetAll(response, EntityNames.Person);
        }
    }
}
