﻿using Thex.Application.Interfaces;
using Thex.Dto;
using Microsoft.AspNetCore.Mvc;
using Thex.Domain;
using Thex.Common;
using Thex.Kernel;
using Thex.Web.Base;
using Tnf.Dto;
using Tnf.AspNetCore.Mvc.Response;

namespace Thex.Web.Controllers
{
    [Route(RouteConsts.Country)]
    public class CountryController : ThexAppController
    {
        protected readonly ICountrySubdivisionAppService CountrySubdivisionAppService;

        public CountryController(
            IApplicationUser applicationUser,
            ICountrySubdivisionAppService countrySubdivisionAppService) : base(applicationUser)
        {
            LocalizationSourceName = AppConsts.LocalizationSourceName;
            CountrySubdivisionAppService = countrySubdivisionAppService;
        }

        [HttpGet]
        [ThexAuthorize("PMS_Country_Get")]
        [ProducesResponseType(typeof(IListDto<CountryDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult Get([FromQuery] GetAllCountryDto requestDto)
        {
            var response = CountrySubdivisionAppService.GetAllCountriesByLanguageIsoCode(requestDto);
            
            return CreateResponseOnGetAll(response, EntityNames.CountrySubdivision);
        }
    }
}


