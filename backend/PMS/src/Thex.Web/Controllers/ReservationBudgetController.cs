﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.Application.Interfaces;
using Thex.Kernel;
using Thex.Domain;
using Thex.Dto;
using Thex.Web.Base;
using Tnf.AspNetCore.Mvc.Response;

namespace Thex.Web.Controllers
{
    [Route(RouteConsts.ReservationBudget)]
    public class ReservationBudgetController : ThexAppController
    {
        private readonly IReservationBudgetAppService ReservationBudgetAppService;
        public ReservationBudgetController(IApplicationUser applicationUser, IReservationBudgetAppService reservationBudgetAppService) : base(applicationUser)
        {
            ReservationBudgetAppService = reservationBudgetAppService;
        }

        [HttpGet("{propertyId}")]
        [ThexAuthorize("PMS_ReservationBudget_Get_Param")]
        [ProducesResponseType(typeof(ReservationItemBudgetHeaderDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> GetWithBudgetOffer([FromQuery] ReservationItemBudgetRequestDto requestDto, int propertyId)
        {
            if (requestDto.RoomTypeId.HasValue)
                requestDto.RoomTypeIdList = new List<int>() { requestDto.RoomTypeId.Value };

            var response = await ReservationBudgetAppService.GetWithBudgetOffer(requestDto, propertyId);

            return CreateResponseOnGet(response, EntityNames.ReservationBudget);
        }

        [HttpPut("{propertyId}/{reservationItemId}/accomodation")]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> UpdateByReservationItemId(int propertyId, long reservationItemId)
        {
            await ReservationBudgetAppService.UpdateByReservationItemId(propertyId, reservationItemId);

            return CreateResponseOnPut(null, EntityNames.ReservationBudget);
        }

        [HttpPut("{propertyId}/{reservationId}/accomodations")]
        [ThexAuthorize("PMS_ReservationBudget_Update_Accomodations")]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> UpdateByReservationId(int propertyId, long reservationId)
        {
            await ReservationBudgetAppService.UpdateByReservationId(propertyId, reservationId);

            return CreateResponseOnPut(null, EntityNames.ReservationBudget);
        }
    }
}