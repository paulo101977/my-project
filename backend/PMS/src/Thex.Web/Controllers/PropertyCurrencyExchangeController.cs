﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Thex.Application.Interfaces;
using Thex.Domain;
using Thex.Dto;
using Thex.Dto.PropertyCurrencyExchange;
using Thex.Web.Base;
using Thex.Kernel;
using Tnf.AspNetCore.Mvc.Response;
using Tnf.Dto;

namespace Thex.Web.Controllers
{
    [Route(RouteConsts.PropertyCurrencyExchange)]
    public class PropertyCurrencyExchangeController : ThexAppController
    {
        private readonly IPropertyCurrencyExchangeAppService PropertyCurrencyExchangeAppService;

        public PropertyCurrencyExchangeController(
            IApplicationUser applicationUser,
            IPropertyCurrencyExchangeAppService propertyCurrencyExchangeAppService)
            : base(applicationUser)
        {
            PropertyCurrencyExchangeAppService = propertyCurrencyExchangeAppService;
        }                

        [HttpGet("{propertyId}/quotationbyperiod")]
        [ThexAuthorize("PMS_PropertyCurrencyExchange_Get_QuotationByPeriod")]
        [ProducesResponseType(typeof(IListDto<QuotationCurrentDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult QuotationCurrentByPropertyId([FromQuery]GetAllPropertyCurrencyExchangeDto dto, int propertyId)
        {
            var response = PropertyCurrencyExchangeAppService.GetAllQuotationByPeriod(propertyId, dto.StartDate, dto.CurrencyId, dto.EndDate);

            return CreateResponseOnGetAll(response, EntityNames.PropertyCurrencyExchange);
        }

        [HttpGet("quotationByCurrencyId")]
        [ThexAuthorize("PMS_PropertyCurrencyExchange_Get_QuotationByCurrencyId")]
        [ProducesResponseType(typeof(ExchangeRateDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult GetQuotationByCurrencyId([FromQuery]GetAllPropertyCurrencyExchangeDto dto)
        {
            var response = PropertyCurrencyExchangeAppService.GetQuotationByCurrencyId(int.Parse(_applicationUser.PropertyId), dto.CurrencyId);

            return CreateResponseOnGet(response, EntityNames.PropertyCurrencyExchange);
        }

        [HttpPut("updatequotationfuture")]
        [ThexAuthorize("PMS_PropertyCurrencyExchange_Get_UpdateQuotationFuture")]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult QuotationUpdate([FromBody]List<PropertyCurrencyExchangeDto> dto)
        {
            PropertyCurrencyExchangeAppService.UpdateQuotationFuture(dto);

            return CreateResponseOnPut(null, EntityNames.PropertyCurrencyExchange);
        }

        [HttpPost("insertquotation")]
        [ThexAuthorize("PMS_PropertyCurrencyExchange_Get_InsertQuotation")]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult QuotationInsert([FromBody]InsertQuotationByPeriodDto dto)
        {
            PropertyCurrencyExchangeAppService.InsertQuotationByPeriod(dto);

            return CreateResponseOnPost(null, EntityNames.PropertyCurrencyExchange);
        }


        [HttpGet("currentquotationlist")]
        [ThexAuthorize("PMS_PropertyCurrencyExchange_Get_CurrentQuotationList")]
        [ProducesResponseType(typeof(QuotationCurrentDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult GetCurrentQuotationList()
        {
            var response = PropertyCurrencyExchangeAppService.GetCurrentQuotationList();

            return CreateResponseOnGet(response, EntityNames.PropertyCurrencyExchange);
        }

        [HttpPatch]
        [ThexAuthorize("PMS_PropertyCurrencyExchange_Patch_CurrentQuotation")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult PatchCurrentQuotation([FromBody]QuotationCurrentDto quotationCurrentDto)
        {
            PropertyCurrencyExchangeAppService.PatchCurrentQuotation(quotationCurrentDto);

            return CreateResponseOnPatch(null, EntityNames.PropertyCurrencyExchange);
        }

    }
}