﻿using Microsoft.AspNetCore.Mvc;
using Thex.Domain;
using Thex.Web.Base;
using Thex.Kernel;
using Tnf.AspNetCore.Mvc.Response;
using Thex.Application.Interfaces;
using Thex.Dto;
using System;
using System.Collections.Generic;

namespace Thex.Web.Controllers
{
    [Route(RouteConsts.TourismTax)]
    public class TourismTaxController : ThexAppController
    {
        private readonly ITourismTaxAppService _tourismTaxAppService;

        public TourismTaxController(
            IApplicationUser applicationUser,
            ITourismTaxAppService tourismTaxAppService) : base(applicationUser)
        {
            _tourismTaxAppService = tourismTaxAppService;
        }

        [HttpGet("getbypropertyid")]
        [ThexAuthorize("PMS_TourismTax_Get")]
        [ProducesResponseType(typeof(TourismTaxDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public virtual IActionResult GetByPropertyId()
        {
            var response = _tourismTaxAppService.GetByPropertyId();
            return CreateResponseOnGet(response, EntityNames.TourismTax);
        }

        [HttpPost]
        [ThexAuthorize("PMS_TourismTax_Post")]
        [ValidatePropertyIdFilter]
        [ProducesResponseType(typeof(TourismTaxDto), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public virtual IActionResult Create([FromBody] TourismTaxDto dto)
        {
            var response = _tourismTaxAppService.Create(dto);
            return CreateResponseOnGet(response, EntityNames.TourismTax);
        }

        [HttpPut]
        [ThexAuthorize("PMS_TourismTax_Put")]
        [ValidatePropertyIdFilter]
        [ProducesResponseType(typeof(TourismTaxDto), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public virtual IActionResult Update([FromBody] TourismTaxDto dto)
        {
            var response = _tourismTaxAppService.Update(dto);
            return CreateResponseOnGet(response, EntityNames.TourismTax);
        }

        [HttpPatch("toggleactivation/{id}")]
        [ThexAuthorize("PMS_TourismTax_ToggleActivation")]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult ToggleActivation(Guid id)
        {
            _tourismTaxAppService.ToggleActivation(id);

            return CreateResponseOnPatch(null, EntityNames.PropertyGuestType);
        }

        [HttpPatch("launch")]
        [ThexAuthorize("PMS_TourismTax_Launch_Patch")]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult Launch([FromBody]List<TourismTaxLaunchRequestDto> requestListDto)
        {
            _tourismTaxAppService.Launch(requestListDto);

            return CreateResponseOnPatch(null, EntityNames.PropertyGuestType);
        }
    }
}
