﻿using Thex.Application.Interfaces;
using Thex.Dto;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Thex.Domain;
using Thex.Dto.GuestReservationItem;
using System.Collections.Generic;
using Thex.Kernel;
using Thex.Web.Base;
using Tnf.Dto;
using Tnf.AspNetCore.Mvc.Response;

namespace Thex.Web.Controllers
{
    [Route(RouteConsts.GuestReservationItem)]
    public class GuestReservationItemController : ThexAppController
    {
        private readonly IGuestReservationItemAppService GuestReservationItemAppService;

        public GuestReservationItemController(
            IApplicationUser applicationUser,
            IGuestReservationItemAppService guestReservationItemAppService)
            : base(applicationUser)
        {
            GuestReservationItemAppService = guestReservationItemAppService;
        }

        [HttpGet]
        [ProducesResponseType(typeof(IListDto<GuestReservationItemDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public virtual async Task<IActionResult> Get([FromQuery]GetAllGuestReservationItemDto requestDto)
        {
            var response = await GuestReservationItemAppService.GetAll(requestDto);

            return CreateResponseOnGetAll(response, EntityNames.GuestReservationItem);
        }

        [HttpGet("{id}")]
        [ProducesResponseType(typeof(GuestReservationItemDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public virtual async Task<IActionResult> Get(long id, [FromQuery]RequestDto requestDto)
        {
            var request = new DefaultLongRequestDto(id, requestDto);

            var response = await GuestReservationItemAppService.Get(request);

            return CreateResponseOnGet(response, EntityNames.GuestReservationItem);
        }

        [HttpPut("{id}")]
        [ValidatePropertyIdFilter]
        [ProducesResponseType(typeof(GuestReservationItemDto), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public virtual async Task<IActionResult> Put(long id, [FromBody]GuestReservationItemDto dto)
        {
            var response = await GuestReservationItemAppService.Update(id, dto);

            return CreateResponseOnPut(response, EntityNames.GuestReservationItem);
        }

        [HttpPost]
        [ValidatePropertyIdFilter]
        [ProducesResponseType(typeof(GuestReservationItemDto), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public virtual async Task<IActionResult> Post([FromBody]GuestReservationItemDto dto)
        {
            var response = await GuestReservationItemAppService.Create(dto);

            return CreateResponseOnPost(response, EntityNames.GuestReservationItem);
        }

        [HttpDelete("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public virtual async Task<IActionResult> Delete(long id)
        {
            await GuestReservationItemAppService.Delete(id);

            return CreateResponseOnDelete(EntityNames.GuestReservationItem);
        }

        [HttpGet("roomid/{id}")]
        [ThexAuthorize("PMS_GuestReservationItem_Get_RoomId")]
        [ProducesResponseType(typeof(IListDto<GuestReservationItemDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult GetGuestReservationByRoomId( [FromQuery] GetAllGuestReservationItemDto requestDto, long id)
        {
            var response = GuestReservationItemAppService.guestReservationItensByRoomId(id);

            return CreateResponseOnGetAll(response, EntityNames.GuestReservationItem);

        }

        [HttpPatch("{id}/associate")]
        [ThexAuthorize("PMS_GuestReservationItem_Patch_Associate")]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult Associate(long id, [FromBody] GuestReservationItemDto dto)
        {
            GuestReservationItemAppService.Associate(id, dto);

            return CreateResponseOnPatch(null, EntityNames.GuestReservationItem);
        }

        [HttpPatch("{id}/disassociate")]
        [ThexAuthorize("PMS_GuestReservationItem_Patch_Disassociate")]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult Disassociate(long id)
        {
            GuestReservationItemAppService.Disassociate(id);

            return CreateResponseOnPatch(null, EntityNames.GuestReservationItem);
        }

        [HttpPatch("{reservationId}/{propertyId}/confirmcheckin")]
        [ThexAuthorize("PMS_GuestReservationItem_Patch_ConfirmCheckin")]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult ConfirmCheckin(long reservationId,int propertyId, [FromBody]List<GuestReservationIdsDto> ListDto)
        {

            GuestReservationItemAppService.ConfirmCheckin(reservationId, ListDto, propertyId);

            return CreateResponseOnPatch(null, EntityNames.GuestReservationItem);
        }

        [HttpPatch("property/{propertyId}/guestreservationitem/{guestReservationItemId}/checkout")]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult Checkout(int propertyId, long guestReservationItemId)
        {
            GuestReservationItemAppService.Checkout(propertyId, guestReservationItemId);

            return CreateResponseOnPatch(null, EntityNames.GuestReservationItem);
        }
    }
}
