﻿using System;
using Thex.Application.Interfaces;
using Thex.Dto;
using Microsoft.AspNetCore.Mvc;
using Thex.Domain;
using Thex.Web.Base;
using Tnf.AspNetCore.Mvc.Response;
using Tnf.Dto;
using System.Threading.Tasks;
using Thex.Kernel;
using System.Collections.Generic;

namespace Thex.Web.Controllers
{
    [Route(RouteConsts.BillingInvoiceProperty)]
    public class BillingInvoicePropertyController : ThexAppController
    {
        private readonly IBillingInvoicePropertyAppService _billingInvoicePropertyAppService;
        public BillingInvoicePropertyController(
              IApplicationUser applicationUser,
            IBillingInvoicePropertyAppService billingInvoicePropertyAppService)
            : base(applicationUser)
        {
            _billingInvoicePropertyAppService = billingInvoicePropertyAppService;
        }


        [HttpPost("parametersdocument")]
        [ThexAuthorize("PMS_BillingInvoiceProperty_Post_ParametersDocument")]
        [ValidatePropertyIdFilter]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult CreateBillingInvoiceProperty([FromBody] BillingInvoicePropertyDto request)
        {
            _billingInvoicePropertyAppService.Create(request);

            return CreateResponseOnPost(null, EntityNames.BillingInvoiceProperty);
        }

        [HttpGet("{id}")]
        [ThexAuthorize("PMS_BillingInvoiceProperty_Get_Param")]
        [ProducesResponseType(typeof(BillingInvoicePropertyDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult GetBillingInvoiceProperty(Guid id)
        {
            var response = _billingInvoicePropertyAppService.GetById(id);
            return CreateResponseOnGet(response, EntityNames.BillingInvoiceProperty);
        }

        [HttpPut]
        [ValidatePropertyIdFilter]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult UpdateBillingInvoiceProperty([FromBody] BillingInvoicePropertyDto dto)
        {
            _billingInvoicePropertyAppService.UpdateBillingInvoiceProperty(dto);

            return CreateResponseOnPut(null, EntityNames.BillingInvoiceProperty);
        }

        [HttpGet("{propertyId}/getall")]
        [ThexAuthorize("PMS_BillingInvoice_Get_GetAll")]
        [ProducesResponseType(typeof(IListDto<BillingInvoicePropertyDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult GetAllBillingInvoice([FromQuery] GetAllBillingInvoicePropertyDto request, int propertyId)
        {
            var response = _billingInvoicePropertyAppService.GetAllByPropertyId(propertyId, request);
            return CreateResponseOnGetAll(response, EntityNames.BillingInvoiceProperty);
        }

        [HttpGet("getallwithoutreference")]
        [ThexAuthorize("PMS_BillingInvoice_Get_GetAll")]
        [ProducesResponseType(typeof(IListDto<BillingInvoicePropertyDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult GetAllWithoutReference()
        {
            var response = _billingInvoicePropertyAppService.GetAllWithoutReference();
            return CreateResponseOnGetAll(response, EntityNames.BillingInvoiceProperty);
        }

        [HttpPatch("{propertyId}/CancelBillingInvoice/{billingInvoiceId}/{reasonId}")]
        [ProducesResponseType(typeof(IListDto<BillingInvoicePropertyDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> CancelBillingInvoice(int propertyId, Guid billingInvoiceId, int reasonId)
        {
            await _billingInvoicePropertyAppService.CancelBillingInvoiceAsync(propertyId, billingInvoiceId, reasonId);
            return CreateResponseOnGetAll(null, EntityNames.BillingInvoiceProperty);
        }

        [HttpGet("pdf/{id}")]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> GetPdfBillingInvoice(Guid id)
        {
            var pdfByteArray = await _billingInvoicePropertyAppService.GetBillingInvoicePdf(id);
            if (pdfByteArray != null)
                return File(pdfByteArray, "application/pdf");

            return NotFound();
        }

        [HttpGet("{billingInvoiceId}/billingAccountId/{billingAccountId}/details")]
        //[ThexAuthorize("PMS_BillingInvoiceProperty_Get_Param")]
        [ProducesResponseType(typeof(BillingAccountWithAccountsDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult GetInvoice(Guid billingInvoiceId, Guid billingAccountId)
        {
            var response = _billingInvoicePropertyAppService.GetInvoiceDetails(billingInvoiceId, billingAccountId);
            return CreateResponseOnGet(response, EntityNames.BillingInvoiceProperty);
        }

        [HttpGet("proforma/pdf/{reservationId}/{reservationItemId}")]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> GetProformaPdf(long reservationId, long reservationItemId)
        {
            var pdfByteArray = await _billingInvoicePropertyAppService.GetProformaPdf(reservationId, reservationItemId);
            if (pdfByteArray != null)
                return File(pdfByteArray, "application/pdf");

            return NotFound();
        }
    }
}
