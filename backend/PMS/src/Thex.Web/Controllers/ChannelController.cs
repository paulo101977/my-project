﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Thex.Application.Interfaces;
using Thex.Kernel;
using Thex.Domain;
using Thex.Dto;
using Thex.Web.Base;
using Tnf.AspNetCore.Mvc.Response;
using Tnf.Dto;

namespace Thex.Web.Controllers
{
    [Route(RouteConsts.Channel)]
    public class ChannelController : ThexAppController
    {
        private readonly IChannelAppService _channelAppService;

        public ChannelController(
            IApplicationUser applicationUser,
            IChannelAppService channelAppService) : base(applicationUser)
        {
            _channelAppService = channelAppService;
            _applicationUser = applicationUser;
        }

        [HttpGet]
        [ProducesResponseType(typeof(IListDto<ChannelDto>), 200)]
        [ProducesResponseType(404)]
        [ThexAuthorize("PMS_Channel_GetAll")]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Get([FromQuery] GetAllChannelDto requestDto)
        {
            var response = await _channelAppService.GetAllChannelsDtoByTenantIdAsync(requestDto);
            return CreateResponseOnGetAll(response, EntityNames.Channel);
        }

        [HttpGet("{id}")]
        [ProducesResponseType(typeof(ChannelDto), 200)]
        [ProducesResponseType(404)]
        [ThexAuthorize("PMS_Channel_Get")]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Get(Guid id, [FromQuery] RequestDto requestDto)
        {
            var response = _channelAppService.GetDtoById(id);
            return CreateResponseOnGet(response, EntityNames.Channel);
        }

        [HttpPut("{id}")]
        [ValidatePropertyIdFilter]
        [ProducesResponseType(typeof(ChannelDto), 200)]
        [ThexAuthorize("PMS_Channel_Put")]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult Put(Guid id, [FromBody] ChannelDto dto)
        {
            var response = _channelAppService.UpdateChannel(id, dto);
            return CreateResponseOnPut(response, EntityNames.Channel);
        }

        [HttpPost]
        [ValidatePropertyIdFilter]
        [ProducesResponseType(typeof(ChannelDto), 200)]
        [ThexAuthorize("PMS_Channel_Post")]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult Post([FromBody] ChannelDto dto)
        {
            var response = _channelAppService.CreateChannel(dto);
            return CreateResponseOnPost(response, EntityNames.Channel);
        }

        [HttpDelete("{id}")]
        [ProducesResponseType(200)]
        [ThexAuthorize("PMS_Channel_Delete")]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult Delete(Guid id)
        {
            _channelAppService.DeleteChannel(id);
            return CreateResponseOnDelete(EntityNames.Channel);
        }

        [HttpPatch("{id}/toggleactivation")]
        [ProducesResponseType(200)]
        [ThexAuthorize("PMS_Channel_Patch_ToggleActivation")]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult ToggleActivation(Guid id)
        {
            _channelAppService.ToggleAndSaveIsActive(id);
            return CreateResponseOnPatch(null, EntityNames.Channel);
        }

        [HttpGet("search")]
        [ProducesResponseType(typeof(IListDto<ChannelDto>), 200)]
        [ProducesResponseType(404)]
        [ThexAuthorize("PMS_Channel_Get_Search")]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Search([FromQuery] string q)
        {
            var response = _channelAppService.SearchChannel(q);
            return CreateResponseOnGetAll(response, EntityNames.Channel);
        }
    }
}