﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Thex.Application.Interfaces;
using Thex.Kernel;
using Thex.Domain;
using Thex.Dto;
using Tnf.AspNetCore.Mvc.Response;
using Tnf.Dto;

namespace Thex.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [AllowAnonymous]
    public class HealthController : TnfController
    {
        private readonly IPropertyAppService _propertyAppService;

        public HealthController(IPropertyAppService propertyAppService)
        {
            _propertyAppService = propertyAppService;
        }

        [HttpGet("check")]
        [ProducesResponseType(typeof(bool), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult Get()
        {
            var response = _propertyAppService.GetAll(new GetAllPropertyDto());

            return CreateResponseOnGetAll(response != null ? true : false, EntityNames.Health);
        }
    }
}