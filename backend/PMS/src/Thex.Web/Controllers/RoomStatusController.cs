﻿using Thex.Dto;
using Tnf.Dto;
using Microsoft.AspNetCore.Mvc;
using Thex.Kernel;
using Thex.Web.Base;
using Tnf.AspNetCore.Mvc.Response;
using Thex.Application.Interfaces.RoomStatus;
using Thex.Domain;
using Thex.Dto.RoomStatus;

namespace Thex.Web.Controllers
{
    [Route(RouteConsts.RoomStatus)]
    public class RoomStatusController : ThexAppController
    {
        protected readonly IRoomStatusAppService _roomStatusAppService;

        public RoomStatusController(
            IApplicationUser applicationUser,
            IRoomStatusAppService roomStatusAppService) : base(applicationUser)
        {
            _roomStatusAppService = roomStatusAppService;
        }

        [HttpGet("{propertyId}/getall")]
        [ProducesResponseType(typeof(IListDto<RoomStatusDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult GetAll([FromQuery] GetAllRoomStatusDto request, int propertyId)
        {
            var response = _roomStatusAppService.GetAll(request, propertyId);

            return CreateResponseOnGetAll(response, EntityNames.Room);
        } 
    }
}
