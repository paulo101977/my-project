﻿using Thex.Application.Interfaces;
using Thex.Dto;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Thex.Web.Controllers
{
    using Thex.Kernel;
    using Thex.Domain;
    using Thex.Web.Base;
    using Tnf.AspNetCore.Mvc.Response;
    using Tnf.Dto;

    [Route(RouteConsts.BusinessSource)]
    public class BusinessSourceController : ThexAppController
    {
        private readonly IBusinessSourceAppService BusinessSourceAppService;

        public BusinessSourceController(
            IApplicationUser applicationUser,
            IBusinessSourceAppService businessSourceAppService)
            : base(applicationUser)
        {
            BusinessSourceAppService = businessSourceAppService;
        }

        [HttpGet]
        [ThexAuthorize("PMS_BusinessSource_Get")]
        [ProducesResponseType(typeof(IListDto<BusinessSourceDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public virtual async Task<IActionResult> Get([FromQuery] GetAllBusinessSourceDto requestDto)
        {
            int propertyId = requestDto.PropertyId;
            var response = BusinessSourceAppService.GetAllByPropertyId(requestDto, propertyId);

            return CreateResponseOnGetAll(response, EntityNames.BusinessSource);
        }

        [HttpGet("{id}")]
        [ProducesResponseType(typeof(BusinessSourceDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public virtual async Task<IActionResult> Get(int id)
        {
            var request = new DefaultIntRequestDto(id);

            var response = await BusinessSourceAppService.Get(request);

            return CreateResponseOnGet(response, EntityNames.BusinessSource);
        }

        [HttpPut("{id}")]
        [ThexAuthorize("PMS_BusinessSource_Update")]
        [ValidatePropertyIdFilter]
        [ProducesResponseType(typeof(BusinessSourceDto), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public virtual async Task<IActionResult> Put(int id, [FromBody] BusinessSourceDto dto)
        {
            var response = await BusinessSourceAppService.Update(dto.Id, dto);

            return CreateResponseOnPut(response, EntityNames.BusinessSource);
        }

        [HttpPatch("{id}/toggleactivation")]
        [ThexAuthorize("PMS_BusinessSource_Get_ToggleActivation")]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult ToggleActivation(int id)
        {
            BusinessSourceAppService.ToggleActivation(id);

            return CreateResponseOnPatch(null, EntityNames.BusinessSource);
        }

        [HttpPost]
        [ThexAuthorize("PMS_BusinessSource_Post")]
        [ValidatePropertyIdFilter]
        [ProducesResponseType(typeof(BusinessSourceDto), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public virtual async Task<IActionResult> Post([FromBody] BusinessSourceDto dto)
        {
            var response = await BusinessSourceAppService.Create(dto);

            return CreateResponseOnPost(response, EntityNames.BusinessSource);
        }

        [HttpDelete("{id}")]
        [ThexAuthorize("PMS_BusinessSource_Delete")]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public virtual async Task<IActionResult> Delete(int id)
        {
            await BusinessSourceAppService.Delete(id);

            return CreateResponseOnDelete(EntityNames.BusinessSource);
        }
    }
}
