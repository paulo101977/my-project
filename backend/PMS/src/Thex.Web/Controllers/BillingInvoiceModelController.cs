﻿using Thex.Application.Interfaces;
using Thex.Dto;
using Tnf.Dto;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Thex.Web.Base;
using Thex.Kernel;
using Thex.Domain;
using Tnf.AspNetCore.Mvc.Response;
using System.Collections.Generic;

namespace Thex.Web.Controllers
{
    [Route(RouteConsts.BillingInvoiceModel)]
    public class BillingInvoiceModelController : ThexAppController
    {
        private readonly IBillingInvoiceModelAppService BillingInvoiceModelAppService;

        public BillingInvoiceModelController(
            IApplicationUser applicationUser,
            IBillingInvoiceModelAppService billingInvoiceModelAppService)
            : base(applicationUser)
        {
            BillingInvoiceModelAppService = billingInvoiceModelAppService;
        }

        [HttpGet]
        [ThexAuthorize("PMS_BillingInvoiceModel_Get")]
        [ProducesResponseType(typeof(IListDto<BillingInvoiceModelDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Get([FromQuery] GetAllBillingInvoiceModelDto requestDto)
        {
            var response = await BillingInvoiceModelAppService.GetAll(requestDto);

            return CreateResponseOnGetAll(response, EntityNames.BillingInvoiceModel);
        }

        [HttpGet("getallvisible")]
        [ThexAuthorize("PMS_BillingInvoiceModel_Get")]
        [ProducesResponseType(typeof(IListDto<BillingInvoiceModelDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult GetAllVisible([FromQuery] GetAllBillingInvoiceModelDto requestDto)
        {
            var response = BillingInvoiceModelAppService.GetAllVisible();

            return CreateResponseOnGetAll(response, EntityNames.BillingInvoiceModel);
        }

        [HttpGet("{id}")]
        [ProducesResponseType(typeof(BillingInvoiceModelDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Get(int id, RequestDto requestDto)
        {
            var request = new DefaultIntRequestDto(id, requestDto);

            var response = await BillingInvoiceModelAppService.Get(request);

            return CreateResponseOnGet(response, EntityNames.BillingInvoiceModel);
        }

        [HttpPut("{id}")]
        [ProducesResponseType(typeof(BillingInvoiceModelDto), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Put(int id, [FromBody] BillingInvoiceModelDto dto)
        {
            var response = await BillingInvoiceModelAppService.Update(id, dto);

            return CreateResponseOnPut(response, EntityNames.BillingInvoiceModel);
        }

        [HttpPost]
        [ProducesResponseType(typeof(BillingInvoiceModelDto), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Post([FromBody] BillingInvoiceModelDto dto)
        {
            var response = await BillingInvoiceModelAppService.Create(dto);

            return CreateResponseOnPost(response, EntityNames.BillingInvoiceModel);
        }

        [HttpDelete("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Delete(int id)
        {
            await BillingInvoiceModelAppService.Delete(id);

            return CreateResponseOnDelete(EntityNames.BillingInvoiceModel);
        }
    }
}
