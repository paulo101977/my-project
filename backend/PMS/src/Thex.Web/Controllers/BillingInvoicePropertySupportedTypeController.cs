﻿using System;
using Thex.Application.Interfaces;
using Thex.Dto;
using Tnf.Dto;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Thex.Web.Base;
using Thex.Kernel;
using Thex.Domain;
using Tnf.AspNetCore.Mvc.Response;

namespace Thex.Web.Controllers
{
    [Route(RouteConsts.BillingInvoicePropertySupportedType)]
    public class BillingInvoicePropertySupportedTypeController : ThexAppController
    {
        private readonly IBillingInvoicePropertySupportedTypeAppService BillingInvoicePropertySupportedTypeAppService;

        public BillingInvoicePropertySupportedTypeController(
            IApplicationUser applicationUser,
            IBillingInvoicePropertySupportedTypeAppService billingInvoicePropertySupportedTypeAppService)
            : base(applicationUser)
        {
            BillingInvoicePropertySupportedTypeAppService = billingInvoicePropertySupportedTypeAppService;
        }

        [HttpGet]
        [ProducesResponseType(typeof(IListDto<BillingInvoicePropertySupportedTypeDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Get([FromQuery] GetAllBillingInvoicePropertySupportedTypeDto requestDto)
        {
            var response = await BillingInvoicePropertySupportedTypeAppService.GetAll(requestDto);

            return CreateResponseOnGetAll(response, EntityNames.BillingInvoicePropertySupportedType);
        }

        [HttpGet("{id}")]
        [ProducesResponseType(typeof(BillingInvoicePropertySupportedTypeDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Get(Guid id, RequestDto requestDto)
        {
            var request = new DefaultGuidRequestDto(id, requestDto);

            var response = await BillingInvoicePropertySupportedTypeAppService.Get(request);

            return CreateResponseOnGet(response, EntityNames.BillingInvoicePropertySupportedType);
        }

        [HttpPut("{id}")]
        [ValidatePropertyIdFilter]
        [ProducesResponseType(typeof(BillingInvoicePropertySupportedTypeDto), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Put(Guid id, [FromBody] BillingInvoicePropertySupportedTypeDto dto)
        {
            var response = await BillingInvoicePropertySupportedTypeAppService.Update(id, dto);

            return CreateResponseOnPut(response, EntityNames.BillingInvoicePropertySupportedType);
        }

        [HttpPost]
        [ValidatePropertyIdFilter]
        [ProducesResponseType(typeof(BillingInvoicePropertySupportedTypeDto), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Post([FromBody] BillingInvoicePropertySupportedTypeDto dto)
        {
            var response = await BillingInvoicePropertySupportedTypeAppService.Create(dto);

            return CreateResponseOnPost(response, EntityNames.BillingInvoicePropertySupportedType);
        }

        [HttpDelete("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Delete(Guid id)
        {
            await BillingInvoicePropertySupportedTypeAppService.Delete(id);

            return CreateResponseOnDelete(EntityNames.BillingInvoicePropertySupportedType);
        }
    }
}
