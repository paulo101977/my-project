﻿using System;
using Thex.Application.Interfaces;
using Thex.Dto;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Thex.Domain;
using Thex.Dto.BillingItem;
using Thex.Web.Base;
using Thex.Kernel;
using Tnf.AspNetCore.Mvc.Response;
using Tnf.Dto;
using System.Collections.Generic;

namespace Thex.Web.Controllers
{
    [Route(RouteConsts.BillingItem)]
    public class BillingItemController : ThexAppController
    {
        private readonly IBillingItemAppService BillingItemAppService;

        public BillingItemController(
            IApplicationUser applicationUser,
            IBillingItemAppService billingItemAppService) : base(applicationUser)
        {
            BillingItemAppService = billingItemAppService;
        }

      
        [HttpGet("{billingItemId}/property/{propertyId}/billingitem")]
        [ThexAuthorize("PMS_BillingItem_Get_BillingItem_Params")]
        [ProducesResponseType(typeof(BillingItemServiceDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult GetBillingItem(int billingItemId, long propertyId)
        {
            var response = BillingItemAppService.GetBillingItem(billingItemId, propertyId);

            return CreateResponseOnGet(response, EntityNames.BillingItem);
        }

        [HttpDelete("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Delete(int id)
        {
            await BillingItemAppService.Delete(id);

            return CreateResponseOnDelete(EntityNames.BillingItem);
        }

        [HttpDelete("{id}/property/{propertyId}")]
        [ThexAuthorize("PMS_BillingItem_Delete_Params")]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult Delete(int id,long propertyId)
        {
            BillingItemAppService.Delete(id, propertyId);

            return CreateResponseOnDelete(EntityNames.BillingItem);
        }

        [HttpPost("createservicewithtax")]
        [ThexAuthorize("PMS_BillingItem_Post_CreateServiceWithTax")]
        [ValidatePropertyIdFilter]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult CreateServiceAssociateTax([FromBody] BillingItemServiceDto dto)
        {
            BillingItemAppService.CreateServiceAssociateTax(dto);

            return CreateResponseOnPost(null, EntityNames.BillingItem);

        }

        [HttpPost("createtaxwithservice")]
        [ThexAuthorize("PMS_BillingItem_Post_CreateTaxWithService")]
        [ValidatePropertyIdFilter]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult CreateTaxAssociateService([FromBody] BillingItemTaxDto dto)
        {
            BillingItemAppService.CreateTaxAssociateService(dto);

            return CreateResponseOnPost(null, EntityNames.BillingItem);

        }

        [HttpGet("{propertyId}/getallbillingitemservice/{isActive?}")]
        [ThexAuthorize("PMS_BillingItem_Get_GetAllBillingItemService")]
        [ProducesResponseType(typeof(IListDto<GetAllBillingItemServiceDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult GetAllBillingItemService(long propertyId, bool? isActive)
        {
            var response = BillingItemAppService.GetAllBillingItemService(propertyId, isActive);

            return CreateResponseOnGetAll(response, EntityNames.BillingItem);

        }

        [HttpGet("{propertyId}/getallbillingitemservice/search")]
        [ThexAuthorize("PMS_BillingItem_Get_GetAllBillingItemService_Search")]
        [ProducesResponseType(typeof(IListDto<GetAllBillingItemServiceDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult GetAllBillingItemServiceByFilters(SearchBillingItemDto requestDto, long propertyId)
        {
            var response = BillingItemAppService.GetAllBillingItemServiceByFilters(requestDto, propertyId);

            return CreateResponseOnGetAll(response, EntityNames.BillingItem);

        }

        [HttpGet("{propertyId}/getallbillingitemtax")]
        [ThexAuthorize("PMS_BillingItem_Get_GetAllBillingItemTax")]
        [ProducesResponseType(typeof(IListDto<GetAllBillingItemTaxDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult GetAllBillingItemTax(long propertyId)
        {
            var response = BillingItemAppService.GetAllBillingItemTax(propertyId);

            return CreateResponseOnGetAll(response, EntityNames.BillingItem);

        }

        [HttpGet("{propertyId}/getAllbillingitemforassociatetax")]
        [ThexAuthorize("PMS_BillingItem_Get_GetAllBillingItemForAssociateTax")]
        [ProducesResponseType(typeof(IListDto<GetAllBillingItemForAssociateTaxDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult GetAllBillingItemForAssociateTax(long propertyId)
        {
            var response = BillingItemAppService.GetAllBillingItemForAssociateTax(propertyId);

            return CreateResponseOnGetAll(response, EntityNames.BillingItem);

        }

        [HttpGet("{propertyId}/getAllbillingitemforassociateservice")]
        [ThexAuthorize("PMS_BillingItem_Get_GetAllBillingItemForAssociateService")]
        [ProducesResponseType(typeof(IListDto<GetAllBillingItemForAssociateServiceDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult GetAllBillingItemForAssociateService(long propertyId)
        {
            var response = BillingItemAppService.GetAllBillingItemForAssociateService(propertyId);

            return CreateResponseOnGetAll(response, EntityNames.BillingItem);

        }

        [HttpPut("updatebillingitemservicewithtax")]
        [ThexAuthorize("PMS_BillingItem_Update_UpdateBillingItemServiceWithTax")]
        [ValidatePropertyIdFilter]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult UpdateBillingItemServicesWithTax([FromBody] BillingItemServiceDto serviceDto)
        {
            BillingItemAppService.UpdateBillingItemServiceWithTax(serviceDto);

            return CreateResponseOnPut(null, EntityNames.BillingItem);
        }

        [HttpPut("updatebillingitemtaxwithservice")]
        [ThexAuthorize("PMS_BillingItem_Update_UpdateBillingItemTaxWithService")]
        [ValidatePropertyIdFilter]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult UpdateBillingItemTaxWithService([FromBody] BillingItemServiceDto serviceDto)
        {
            BillingItemAppService.UpdateBillingItemTaxWithService(serviceDto);

            return CreateResponseOnPut(null, EntityNames.BillingItem);
        }

        [HttpPatch("{id}/toggleactivation")]
        [ThexAuthorize("PMS_BillingItem_Patch_ToggleActivation")]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult ToggleActivation(int id)
        {
            BillingItemAppService.ToggleActivation(id);

            return CreateResponseOnPatch(null, EntityNames.BillingItem);
        }

        #region PropertyPaymentTypes

        [HttpGet("{propertyId}/{paymentTypeId}/{acquirerId}/getpaymenttype")]
        [ProducesResponseType(typeof(BillingItemPaymentTypeDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public virtual IActionResult GetPaymentType(int propertyId, int paymentTypeId, Guid acquirerId)
        {
            var response = BillingItemAppService.GetByPaymentTypeIdAndAcquirerId(propertyId, paymentTypeId, acquirerId);

            return CreateResponseOnGet(response, EntityNames.BillingItem);
        }

        [HttpGet("{propertyId}/{paymentTypeId}/getpaymenttype")]
        [ThexAuthorize("PMS_BillingItem_Get_GetPaymentType")]
        [ProducesResponseType(typeof(BillingItemPaymentTypeDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public virtual IActionResult GetPaymentType(int propertyId, int paymentTypeId)
        {
            var response = BillingItemAppService.GetByPaymentTypeId(propertyId, paymentTypeId);

            return CreateResponseOnGet(response, EntityNames.BillingItem);
        }

        [HttpPut("paymenttype")]
        [ThexAuthorize("PMS_BillingItem_Update_PaymentType")]
        [ValidatePropertyIdFilter]
        [ProducesResponseType(typeof(BillingItemPaymentTypeDto), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult Put([FromBody] BillingItemPaymentTypeDto dto)
        {
            var response = BillingItemAppService.UpdatePropertyPaymentType(dto);

            return CreateResponseOnPut(response, EntityNames.BillingItem);
        }

        [HttpPost("paymenttype")]
        [ThexAuthorize("PMS_BillingItem_Post_PaymentType")]
        [ValidatePropertyIdFilter]
        [ProducesResponseType(typeof(BillingItemPaymentTypeDto), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult Post([FromBody] BillingItemPaymentTypeDto dto)
        {
            var response = BillingItemAppService.CreatePropertyPaymentType(dto);

            return CreateResponseOnPost(response, EntityNames.BillingItem);
        }


        [HttpGet("{propertyId}/allpaymenttypes")]
        [ThexAuthorize("PMS_BillingItem_Get_AllPaymentTypes")]
        [ProducesResponseType(typeof(IListDto<BillingItemPaymentTypeDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public virtual IActionResult GetAllPaymentTypes(int propertyId)
        {
            var response = BillingItemAppService.GetAllPaymentTypeIdByPropertyId(propertyId);

            return CreateResponseOnGetAll(response, EntityNames.BillingItem);
        }

        [HttpDelete("{propertyId}/{paymentTypeId}/{acquirerId}/paymenttype")]
        [ThexAuthorize("PMS_BillingItem_Delete_PaymentType_Params")]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult DeletePaymentType(int propertyId, int paymentTypeId, Guid acquirerId)
        {
            BillingItemAppService.DeletePaymentType(propertyId, paymentTypeId, acquirerId);

            return CreateResponseOnDelete(EntityNames.BillingItem);
        }

        [HttpPatch("{propertyId}/{paymentTypeId}/{acquirerId}/{activate}/paymenttype")]
        [ThexAuthorize("PMS_BillingItem_Patch_PaymentType_Params")]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult ToggleActivationPaymentType(int propertyId, int paymentTypeId, Guid acquirerId, bool activate)
        {
            BillingItemAppService.ToggleActivationPaymentType(propertyId, paymentTypeId, acquirerId, activate);

            return CreateResponseOnPatch(null, EntityNames.BillingItem);
        }


        #endregion
        [HttpGet("{propertyId}/getallserviceforrealese")]
        [ThexAuthorize("PMS_BillingItem_Get_GetAllServiceForRealese")]
        [ProducesResponseType(typeof(IListDto<BillingItemServiceLancamentoDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult GetllAllBillingItemServiceForRealese([FromQuery] GetAllBillingItemServiceForRealase dto, int propertyId)
        {
            var response = BillingItemAppService.GetAllServiceWithAssociateTaxForDebitRealease(propertyId, dto);

            return CreateResponseOnGetAll(response, EntityNames.BillingItem);
        }

        [HttpGet("{paymentTypeId}/plasticbrand")]
        [ThexAuthorize("PMS_BillingItem_Get_PlasticBrand")]
        [ProducesResponseType(typeof(IListDto<BillingItemPlasticBrandCompanyClientDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult GetPlasticBrandWithCompanyClientByPaymentType(int paymentTypeId)
        {
            var response = BillingItemAppService.GetPlasticBrandWithCompanyClientByPaymentType(paymentTypeId);

            return CreateResponseOnGetAll(response, EntityNames.BillingItem);
        }

        [HttpGet("paymenttypes")]
        [ThexAuthorize("PMS_BillingItem_Get_PaymentTypes")]
        [ProducesResponseType(typeof(IListDto<PaymentTypeWithBillingItemDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult GetPaymentTypes([FromQuery] GetAllPaymentTypeDto requestDto)
        {
            var response = BillingItemAppService.GetPaymentTypes(requestDto);

            return CreateResponseOnGetAll(response, EntityNames.BillingItem);
        }

        [HttpGet("{propertyId}/billingitemforparametersdocuments")]
        [ThexAuthorize("PMS_BillingItem_Get_BillingItemForParametersDocuments")]
        [ProducesResponseType(typeof(IListDto<BillingItemWithParametersDocumentsDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public virtual IActionResult GetAllServiceWithParametersDocuments(int propertyId)
        {
            var response = BillingItemAppService.GetAllServiceWithParameters(propertyId);

            return CreateResponseOnGetAll(response, EntityNames.BillingItem);
        }

        [HttpGet("getallbillingitemservice")]
        [ThexAuthorize("PMS_BillingItem_Get_BillingItemService")]
        [ProducesResponseType(typeof(List<BillingItemDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult GetAllBillingItemService()
        {
            var response = BillingItemAppService.GetAllBillingItemService();

            return CreateResponseOnGet(response, EntityNames.BillingItem);
        }
    }
}

