﻿using Thex.Application.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Thex.Domain;
using Thex.Dto.ReservationCompanyClient;
using Thex.Kernel;
using Thex.Web.Base;
using Tnf.Dto;
using Tnf.AspNetCore.Mvc.Response;

namespace Thex.Web.Controllers
{
    [Route(RouteConsts.ReservationCompanyClient)]
    public class ReservationCompanyClientController : ThexAppController
    {
        private readonly IReservationAppService ReservationAppService;

        public ReservationCompanyClientController(
            IApplicationUser applicationUser,
            IReservationAppService reservationAppService) : base(applicationUser)
        {
            ReservationAppService = reservationAppService;
        }

        [HttpGet("{propertyId}")]
        [ThexAuthorize("PMS_ReservationCompanyClient_Get_Param")]
        [ProducesResponseType(typeof(IListDto<ReservationCompanyClientResultDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public virtual IActionResult Get([FromQuery] GetAllReservationCompanyClientDto requestDto, int propertyId)
        {
            var response = ReservationAppService.GetReservationCompanyClient(requestDto, propertyId);

            return CreateResponseOnGetAll(response, EntityNames.TransportationType);
        }
    }
}
