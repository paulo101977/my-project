﻿using Microsoft.AspNetCore.Mvc;
using Thex.Domain;
using Tnf.AspNetCore.Mvc.Response;
using System;
using System.Threading.Tasks;
using Thex.Application.Interfaces;
using Tnf.Dto;
using Thex.Dto.ExternalRegistration;

namespace Thex.Web.Controllers
{
    [Route(RouteConsts.ExternalRegistration)]
    public class ExternalRegistrationController : TnfController
    {
        private readonly IExternalRegistrationAppService _externalRegistrationAppService;

        public ExternalRegistrationController(IExternalRegistrationAppService externalRegistrationAppService)
        {
            _externalRegistrationAppService = externalRegistrationAppService;
        }

        [HttpPost("reservationUid/{reservationUid}/email/{email}")]
        [ThexAuthorize("Booking_ExternalRegistration_Post")]
        [ProducesResponseType(typeof(string), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> CreateExternalRegistration(Guid reservationUid, string email)
        {
            var response = await _externalRegistrationAppService.CreateAsync(reservationUid, email);

            return CreateResponseOnPost(response, EntityNames.ExternalRegistration);
        }

        [HttpGet("roomList")]
        [ThexAuthorize("Booking_ExternalRegistration_RoomList_Get")]
        [ProducesResponseType(typeof(ExternalRoomListHeaderDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> GetRoomList()
        {
            var response = await _externalRegistrationAppService.GetRoomListAsync();

            return CreateResponseOnGet(response, EntityNames.ExternalRegistration);
        }

        [HttpGet("reservationItemUid/{reservationItemUid}/registrationList")]
        [ThexAuthorize("Booking_ExternalRegistration_RegistrationList_Get")]
        [ProducesResponseType(typeof(IListDto<ExternalGuestRegistrationDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> GetExternalGuestRegistration(Guid reservationItemUid)
        {
            var response = await _externalRegistrationAppService.GetRegistrationListAsync(reservationItemUid);

            return CreateResponseOnGetAll(response, EntityNames.ExternalRegistration);
        }

        [HttpGet("guestReservationItemId/{guestReservationItemId}/registration")]
        [ThexAuthorize("Booking_ExternalRegistration_Registration_Get")]
        [ProducesResponseType(typeof(ExternalGuestRegistrationDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> GetExternalGuestRegistration(long guestReservationItemId)
        {
            var response = await _externalRegistrationAppService.GetRegistrationAsync(guestReservationItemId);

            return CreateResponseOnGet(response, EntityNames.ExternalRegistration);
        }

        [HttpPut("{guestReservationItemId}")]
        [ThexAuthorize("Booking_ExternalRegistration_Put")]
        [ValidatePropertyIdFilter]
        [ProducesResponseType(typeof(ExternalGuestRegistrationDto), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Put(long guestReservationItemId, [FromBody] ExternalGuestRegistrationDto dto)
        {
            var response = await _externalRegistrationAppService.UpdateAsync(guestReservationItemId, dto);

            return CreateResponseOnPut(response, EntityNames.ExternalRegistration);
        }
    }
}
