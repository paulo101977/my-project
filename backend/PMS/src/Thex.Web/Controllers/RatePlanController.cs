﻿using System;
using Microsoft.AspNetCore.Mvc;
using Thex.Application.Interfaces;
using Thex.Kernel;
using Thex.Domain;
using Thex.Dto;
using Thex.Dto.RatePlan;
using Thex.Web.Base;
using Tnf.AspNetCore.Mvc.Response;
using Tnf.Dto;

namespace Thex.Web.Controllers
{
    [Route(RouteConsts.RatePlan)]
    public class RatePlanController : ThexAppController
    {

        private readonly IRatePlanAppService RatePlanAppService;
        public RatePlanController(
            IApplicationUser applicationUser,
            IRatePlanAppService ratePlanAppService) : base(applicationUser)
        {
            RatePlanAppService = ratePlanAppService;
        }

        [HttpGet("{propertyId}/headerrateplan")]
        [ThexAuthorize("PMS_RatePlan_Get_HeaderRatePlan")]
        [ProducesResponseType(typeof(HeaderRatePlanDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public virtual IActionResult HeaderRatePlan(int propertyId)
        {
            var response = RatePlanAppService.GetHeaderRatePlan(propertyId);

            return CreateResponseOnGet(response, EntityNames.RatePlan);
        }

        [HttpGet("{propertyId}/billingitens")]
        [ThexAuthorize("PMS_RatePlan_Get_BillingItens")]
        [ProducesResponseType(typeof(IListDto<BillingItemDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult GetBillingItens(int propertyId)
        {
            var response = RatePlanAppService.GetAllBillingItemByPropertyId(propertyId);

            return CreateResponseOnGetAll(response, EntityNames.RatePlan);
        }

        [HttpGet("{propertyId}/rateplans")]
        [ThexAuthorize("PMS_RatePlan_Get_RatePlans")]
        [ProducesResponseType(typeof(IListDto<RatePlanDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult GetAllSearch([FromQuery] GetAllRatePlanDto request, int propertyId)
        {
            var response = RatePlanAppService.GetAllRatePlanSearch(request, propertyId);
            return CreateResponseOnGetAll(response, EntityNames.RatePlan);
        }

        [HttpPost]
        [ThexAuthorize("PMS_RatePlan_Post")]
        [ValidatePropertyIdFilter]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult Create([FromBody] RatePlanDto dto)
        {
            RatePlanAppService.RatePlanCreate(dto);

            return CreateResponseOnPost(dto, EntityNames.RatePlan);
        }

        [HttpPatch("{ratePlanId}/toggleactivation")]
        [ThexAuthorize("PMS_RatePlan_Patch_ToggleActivation")]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult ToggleIsActive(Guid ratePlanId)
        {
            RatePlanAppService.ToggleAndSaveIsActive(ratePlanId);

            return CreateResponseOnPatch(null, EntityNames.RatePlan);
        }

        [HttpPut]
        [ThexAuthorize("PMS_RatePlan_Put")]
        [ValidatePropertyIdFilter]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult Update([FromBody] RatePlanDto dto)
        {
            RatePlanAppService.RatePlanUpdate(dto);

            return CreateResponseOnPut(null, EntityNames.RatePlan);
        }

        [HttpGet("{rateplanId}")]
        [ThexAuthorize("PMS_RatePlan_Get_Param")]
        [ProducesResponseType(typeof(RatePlanDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult Get(Guid rateplanId)
        {
            var response = RatePlanAppService.GetRatePlanById(rateplanId);
            return CreateResponseOnGet(response, EntityNames.RatePlan);
        }

        [HttpGet("{rateplanId}/rateplanhistory")]
        [ProducesResponseType(typeof(IListDto<PropertyBaseRateHeaderHistoryDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]        
        public IActionResult GetRatePlanHistory(Guid rateplanId)
        {
            var response = RatePlanAppService.GetRatePlanHistoryById(rateplanId);
            return CreateResponseOnGet(response, EntityNames.RatePlan);
        }

        [HttpGet("{rateplanId}/rateplanhistorybydate/{dateTimeHistory}")]
        [ProducesResponseType(typeof(IListDto<PropertyBaseRateHeaderHistoryDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult GetRatePlanHistorybyDate(Guid rateplanId, DateTime dateTimeHistory)
        {
            var response = RatePlanAppService.GetRatePlanHistoryByDate(rateplanId, dateTimeHistory);
            return CreateResponseOnGet(response, EntityNames.RatePlan);
        }

        [HttpGet("{Id}/ratePlanHistoryId")]
        [ThexAuthorize("PMS_RatePlan_Get_RatePlanHistoryId")]
        [ProducesResponseType(typeof(IListDto<PropertyBaseRateHeaderHistoryDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult GetRatePlanHistoryByHistoryId(Guid Id)
        {
            var response = RatePlanAppService.GetRatePlanHistoryByHistoryId(Id);
            return CreateResponseOnGet(response, EntityNames.RatePlan);
        }

        [HttpGet("{propertyId}/baserate")]
        [ProducesResponseType(typeof(HeaderRatePlanDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult GetAllBaseRateByPeriodAndPropertyId([FromQuery]DateTime startDate, [FromQuery]DateTime endDate, int propertyId)
        {
            var response = RatePlanAppService.GetAllBaseRateByPeriodAndPropertyId(startDate, endDate, propertyId).Result;

            return CreateResponseOnGetAll(response, EntityNames.RatePlan);
        }
    }
}