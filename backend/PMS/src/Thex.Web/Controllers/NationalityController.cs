﻿using Thex.Application.Interfaces;
using Thex.Dto;
using Microsoft.AspNetCore.Mvc;
using Thex.Domain;
using Thex.Common;

namespace Thex.Web.Controllers
{
    using System.Threading.Tasks;
    using Thex.Kernel;
    using Thex.Web.Base;
    using Tnf.AspNetCore.Mvc.Response;
    using Tnf.Dto;

    [Route(RouteConsts.Nationality)]
    public class NationalityController : ThexAppController
    {
        protected readonly ICountrySubdivisionTranslationAppService CountrySubdivisionTranslationAppService;

        public NationalityController(
            IApplicationUser applicationUser,
            ICountrySubdivisionTranslationAppService countrySubdivisionTranslationAppService) : base(applicationUser)
        {
            LocalizationSourceName = AppConsts.LocalizationSourceName;
            CountrySubdivisionTranslationAppService = countrySubdivisionTranslationAppService;
        }

        [HttpGet]
        [ThexAuthorize("PMS_Nationality_Get")]
        [ProducesResponseType(typeof(IListDto<CountrySubdivisionTranslationDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public virtual IActionResult Get([FromQuery] GetAllCountrySubdivisionTranslationDto requestDto)
        {
            var response = CountrySubdivisionTranslationAppService.GetAllNationalities(requestDto);

            return CreateResponseOnGetAll(response, EntityNames.CountrySubdivisionTranslation);
        }
    }
}


