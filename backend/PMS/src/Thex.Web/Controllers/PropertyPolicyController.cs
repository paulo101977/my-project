﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Thex.Application.Interfaces;
using Thex.Kernel;
using Thex.Domain;
using Thex.Dto;
using Thex.Web.Base;
using Tnf.AspNetCore.Mvc.Response;
using Tnf.Dto;

namespace Thex.Web.Controllers
{
    [Route(RouteConsts.PropertyPolicy)]
    public class PropertyPolicyController : ThexAppController
    {
        private readonly IPropertyPolicyAppService PropertyPolicyAppService;

        public PropertyPolicyController(
            IApplicationUser applicationUser,
            IPropertyPolicyAppService propertyPolicyAppService) : base(applicationUser)
        {
            PropertyPolicyAppService = propertyPolicyAppService;
        }

        [HttpPost]
        [ThexAuthorize("PMS_PropertyPolicy_Post")]
        [ValidatePropertyIdFilter]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult Create([FromBody]PropertyPolicyDto propertyPolicyDto)
        {
            PropertyPolicyAppService.PropertyPolicyCreate(propertyPolicyDto);

            return CreateResponseOnPost(null, EntityNames.PropertyPolicy);
        }

        [HttpPut]
        [ThexAuthorize("PMS_PropertyPolicy_Update")]
        [ValidatePropertyIdFilter]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult Update([FromBody]PropertyPolicyDto propertyPolicyDto)
        {
            PropertyPolicyAppService.PropertyPolicyUpdate(propertyPolicyDto);

            return CreateResponseOnPut(null, EntityNames.PropertyPolicy);
        }

        [HttpDelete("{id}/delete")]
        [ThexAuthorize("PMS_PropertyPolicy_Delete")]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Delete(Guid id)
        {
            PropertyPolicyAppService.Delete(id);

            return CreateResponseOnDelete(EntityNames.PropertyPolicy);
        }

        [HttpPatch("{id}/toggleactivation")]
        [ThexAuthorize("PMS_PropertyPolicy_Patch_ToggleActivation")]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult ToggleActivation(Guid id)
        {
            PropertyPolicyAppService.ToggleAndSaveIsActive(id);

            return CreateResponseOnPatch(null, EntityNames.PropertyPolicy);
        }

        [HttpGet("{id}")]
        [ProducesResponseType(typeof(PropertyPolicyDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult GetPropertyPolicy(Guid id)
        {
            var response = PropertyPolicyAppService.GetPropertyPolicy(id);

            return CreateResponseOnGet(response, EntityNames.PropertyPolicy);

        }

        [HttpGet("{propertyId}/getallpropertypolicy")]
        [ThexAuthorize("PMS_PropertyPolicy_Get_GetAllPropertyPolicy")]
        [ProducesResponseType(typeof(IListDto<PropertyPolicyDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult GetPropertyPolicy(int propertyId)
        {
            var response = PropertyPolicyAppService.GetAllPropertyPolicy(propertyId);

            return CreateResponseOnGetAll(response, EntityNames.PropertyPolicy);

        }
        [HttpGet("getallpolicytypes")]
        [ProducesResponseType(typeof(IListDto<PolicyTypeDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult GetAllPolicyTypes()
        {
            var response = PropertyPolicyAppService.GetAllPolicyTypes();

            return CreateResponseOnGetAll(response, EntityNames.PropertyPolicy);
        }

    }
}