﻿using System;
using Thex.Application.Interfaces;
using Thex.Dto;
using Tnf.Dto;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Thex.Domain;

namespace Thex.Web.Controllers
{
    using System.Collections.Generic;
    using System.Net;
    using Thex.Kernel;
    using Thex.Dto.ReservationItem;
    using Thex.Web.Base;
    using Tnf.AspNetCore.Mvc.Response;

    [Route(RouteConsts.ReservationItem)]
    public class ReservationItemController : ThexAppController
    {
        private readonly IReservationItemAppService ReservationItemAppService;
        private readonly ICheckinAppService _checkinAppService;

        public ReservationItemController(
            IApplicationUser applicationUser,
            IReservationItemAppService reservationItemAppService,
            ICheckinAppService checkinAppService)
            : base(applicationUser)
        {
            ReservationItemAppService = reservationItemAppService;
            _checkinAppService = checkinAppService;
        }

        [HttpGet]
        [ProducesResponseType(typeof(IListDto<ReservationItemDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Get([FromQuery] GetAllReservationItemDto requestDto)
        {
            var response = await ReservationItemAppService.GetAll(requestDto);

            return CreateResponseOnGetAll(response, EntityNames.ReservationItem);
        }

        [HttpGet("{id}")]
        [ThexAuthorize("PMS_ReservationItem_Get_Param")]
        [ProducesResponseType(typeof(ReservationItemDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Get(long id, [FromQuery] RequestDto requestDto)
        {
            var request = new DefaultLongRequestDto(id, requestDto);

            var response = await ReservationItemAppService.Get(request);

            return CreateResponseOnGet(response, EntityNames.ReservationItem);
        }

        [HttpPut("{id}")]
        [ProducesResponseType(typeof(ReservationItemDto), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Put(long id, [FromBody] ReservationItemDto dto)
        {
            var response = await ReservationItemAppService.Update(id, dto);

            return CreateResponseOnPut(response, EntityNames.ReservationItem);
        }

        [HttpPost]
        [ProducesResponseType(typeof(ReservationItemDto), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Post([FromBody] ReservationItemDto dto)
        {
            var response = await ReservationItemAppService.Create(dto);

            return CreateResponseOnPost(response, EntityNames.ReservationItem);
        }

        [HttpDelete("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Delete(long id)
        {
            await ReservationItemAppService.Delete(id);

            return CreateResponseOnDelete(EntityNames.ReservationItem);
        }

        [HttpGet("{reservationId}/getall")]
        [ProducesResponseType(typeof(IListDto<ReservationItemDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult GetAll([FromQuery] RequestAllDto request, long reservationId)
        {
            var response = ReservationItemAppService.GetAllByPropertyAndReservation(request, reservationId);

            return CreateResponseOnGetAll(response, EntityNames.ReservationItem);
        }

        [HttpGet("{propertyId}/{reservationId}/accommodations")]
        [ThexAuthorize("PMS_ReservationItem_Get_Accommodations")]
        [ProducesResponseType(typeof(IListDto<AccommodationsByReservationIdResultDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult GetAll([FromQuery] RequestAllDto requestDto, long propertyId, long reservationId)
        {
            var response = ReservationItemAppService.GetAllAccommodations(requestDto, propertyId, reservationId);

            return CreateResponseOnGetAll(response, EntityNames.ReservationItem);
        }

        [HttpPatch("{reservationItemId}/cancel")]
        [ThexAuthorize("PMS_ReservationItem_Patch_Cancel")]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult CancelReservationItem(long reservationItemId, [FromBody] ReservationItemCancelDto dto)
        {
            this.ReservationItemAppService.CancelReservationItem(reservationItemId, dto);

            return CreateResponseOnPatch(null, EntityNames.ReservationItem);
        }


        [HttpPatch("{reservationItemId}/reactivate")]
        [ThexAuthorize("PMS_ReservationItem_Patch_Reactivate")]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult ReactivateReservationItem(long reservationItemId)
        {
            ReservationItemAppService.ReactivateReservationItem(reservationItemId);

            return CreateResponseOnPatch(null, EntityNames.ReservationItem);
        }

        [HttpPatch("{reservationItemId}/realease")]
        [ThexAuthorize("PMS_ReservationItem_Patch_Realease")]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult RealeaseReservationItem(long reservationItemId)
        {
            ReservationItemAppService.RealeseReservationItem(reservationItemId);

            return CreateResponseOnPatch(null, EntityNames.ReservationItem);
        }

        [HttpGet("{propertyId}/{reservationItemId}/{language}/guestguestRegistration")]
        [ThexAuthorize("PMS_ReservationItem_Get_GuestGuestRegistration")]
        [ProducesResponseType(typeof(IListDto<GuestGuestRegistrationResultDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult GetAll(long propertyId, long reservationItemId, string language)
        {
            var response = ReservationItemAppService.GetAllGuestGuestRegistration(propertyId, reservationItemId, language);

            return CreateResponseOnGetAll(response, EntityNames.ReservationItem);
        }

        [HttpPatch("{propertyId}/{reservationItemId}/associateroom/{roomTypeId}/{roomId?}")]
        [ThexAuthorize("PMS_ReservationItem_Patch_AssociateRoom")]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult AssociateRoom(int propertyId, long reservationItemId, int roomTypeId, int? roomId = null)
        {
            ReservationItemAppService.AssociateRoom(propertyId, reservationItemId, roomTypeId, roomId);

            return CreateResponseOnPatch(null, EntityNames.ReservationItem);
        }

        [HttpPatch("{propertyId}/{reservationItemId}/changeperiod")]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult ChangePeriod(int propertyId, long reservationItemId, [FromBody] ChangePeriodReservationItemDto dto)
        {
            ReservationItemAppService.ChangePeriodAndRoomTypeAndRoom(propertyId, reservationItemId, dto);

            return CreateResponseOnPatch(null, EntityNames.ReservationItem);
        }

        [HttpPatch("{propertyId}/{reservationItemId}/budget")]
        [ThexAuthorize("PMS_ReservationItem_Patch_Budget")]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult PatchReservationItemBudget(int propertyId, long reservationItemId, [FromBody] ReservationItemWithBudgetDto dto)
        {
            ReservationItemAppService.PatchReservationItemBudget(propertyId, reservationItemId, dto);

            return CreateResponseOnPatch(null, EntityNames.ReservationItem);
        }

        [HttpPost("accommodationchange")]
        [ThexAuthorize("PMS_ReservationItem_Post_AccommodationChange")]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult AccommodationChange([FromBody] AccommodationChangeDto dto)
        {
            ReservationItemAppService.AccommodationChange(dto);

            return CreateResponseOnPost(null, EntityNames.ReservationItem);
        }


        [HttpPatch("property/{propertyId}/reservationitem/{reservationItemId}/checkout")]
        [ThexAuthorize("PMS_ReservationItem_Patch_Checkout")]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult Checkout(int propertyId, long reservationItemId)
        {
            ReservationItemAppService.Checkout(propertyId, reservationItemId);

            return CreateResponseOnPatch(null, EntityNames.ReservationItem);
        }

        [HttpGet("property/{propertyId}/reservationitem/{reservationItemId}/earlyCheckout")]
        [ThexAuthorize("PMS_ReservationItem_Get_EarlyCheckout")]
        [ProducesResponseType(typeof(EarlyCheckoutResponseDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult EarlyCheckout(int propertyId, long reservationItemId)
        {
            var response = ReservationItemAppService.IsEarlyCheckout(propertyId, reservationItemId);

            return CreateResponseOnGet(response, EntityNames.ReservationItem);
        }

        [HttpGet("property/{propertyId}/reservationId/{reservationId}/confirmationreservation")]
        [ThexAuthorize("PMS_ReservationItem_Get_ConfirmationReservation")]
        [ProducesResponseType(typeof(TemplateReservationSlipDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult GetInformationForSlip(int propertyId, long reservationId)
        {
            var response = ReservationItemAppService.GenerateHtmlForSlipReservationAccommodation(propertyId, reservationId);

            return new ContentResult
            {
                ContentType = "text/html",
                StatusCode = (int)HttpStatusCode.OK,
                Content = response.SlipReservation
            };
        }

        [HttpPost("property/{propertyId}/reservation/{reservationId}/sendemail")]
        [ThexAuthorize("PMS_ReservationItem_Post_SendEmail")]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult SendEmailConfirmationReservation([FromBody] List<String> emailsList,int propertyId, long reservationId)
        {
            ReservationItemAppService.SendMailConfirmationReservation(emailsList,propertyId, reservationId);

            return CreateResponseOnPost(null, EntityNames.ReservationItem);
        }

        [HttpPatch("{reservationItemId}/confirm")]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult ConfirmReservationItem(long reservationItemId)
        {
            ReservationItemAppService.ConfirmReservationItem(reservationItemId);

            return CreateResponseOnPatch(null, EntityNames.ReservationItem);
        }

        [HttpPatch("{id}/cancelcheckin")]
        [ThexAuthorize("PMS_ReservationItem_Patch_CancelCheckin")]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult CancelCheckin(long id)
        {
            _checkinAppService.Cancel(id);

            return CreateResponseOnPatch(null, EntityNames.GuestReservationItem);
        }

        [HttpPatch("{reservationItemId}/reactivateNoShow")]
        [ThexAuthorize("PMS_ReservationItem_Patch_ReactivateNoShow")]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult ReactivateNoShowReservationItem(long reservationItemId)
        {
            ReservationItemAppService.ReactivateNoShow(reservationItemId);

            return CreateResponseOnPatch(null, EntityNames.ReservationItem);
        }
    }
}
