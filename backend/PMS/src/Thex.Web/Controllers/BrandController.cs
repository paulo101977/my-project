﻿using Thex.Application.Interfaces;
using Thex.Dto;
using Tnf.Dto;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Thex.Kernel;
using Thex.Web.Base;
using Thex.Domain;
using Tnf.AspNetCore.Mvc.Response;

namespace Thex.Web.Controllers
{
    [Route(RouteConsts.Brand)]
    public class BrandController : ThexAppController
    {
        private readonly IBrandAppService BrandAppService;

        public BrandController(
            IApplicationUser applicationUser,
            IBrandAppService brandAppService)
            : base(applicationUser)
        {
            BrandAppService = brandAppService;
        }

        [HttpGet]
        [ThexAuthorize("PMS_Brand_Get")]
        [ProducesResponseType(typeof(IListDto<BrandDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Get([FromQuery] GetAllBrandDto requestDto)
        {
            var response = await BrandAppService.GetAll(requestDto);

            return CreateResponseOnGetAll(response, EntityNames.Brand);
        }

        [HttpGet("{id}")]
        [ProducesResponseType(typeof(BrandDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Get(int id, RequestDto requestDto)
        {
            var request = new DefaultIntRequestDto(id, requestDto);

            var response = await BrandAppService.Get(request);

            return CreateResponseOnGet(response, EntityNames.Brand);
        }

        [HttpPut("{id}")]
        [ValidateChainIdFilter]
        [ProducesResponseType(typeof(BrandDto), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Put(int id, [FromBody] BrandDto dto)
        {
            var response = await BrandAppService.Update(id, dto);

            return CreateResponseOnPut(response, EntityNames.Brand);
        }

        [HttpPost]
        [ValidateChainIdFilter]
        [ProducesResponseType(typeof(BrandDto), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Post([FromBody] BrandDto dto)
        {
            var response = await BrandAppService.Create(dto);

            return CreateResponseOnPost(response, EntityNames.Brand);
        }

        [HttpDelete("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Delete(int id)
        {
            await BrandAppService.Delete(id);

            return CreateResponseOnDelete(EntityNames.Brand);
        }
    }
}
