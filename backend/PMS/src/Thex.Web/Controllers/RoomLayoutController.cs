﻿using System;
using Thex.Application.Interfaces;
using Thex.Dto;
using Tnf.Dto;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Thex.Domain;
using Thex.Kernel;
using Thex.Web.Base;
using Tnf.AspNetCore.Mvc.Response;

namespace Thex.Web.Controllers
{
    [Route(RouteConsts.RoomLayout)]
    public class RoomLayoutController : ThexAppController
    {
        private readonly IRoomLayoutAppService RoomLayoutAppService;

        public RoomLayoutController(
            IApplicationUser applicationUser,
            IRoomLayoutAppService roomLayoutAppService) : base(applicationUser)
        {
            RoomLayoutAppService = roomLayoutAppService;
        }

        [HttpGet]
        [ProducesResponseType(typeof(IListDto<RoomLayoutDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Get([FromQuery] GetAllRoomLayoutDto requestDto)
        {
            var response = await RoomLayoutAppService.GetAll(requestDto);

            return CreateResponseOnGetAll(response, EntityNames.RoomLayout);
        }

        [HttpGet("{id}")]
        [ProducesResponseType(typeof(RoomLayoutDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Get(Guid id, [FromQuery] RequestDto requestDto)
        {
            var request = new DefaultGuidRequestDto(id, requestDto);

            var response = await RoomLayoutAppService.Get(request);

            return CreateResponseOnGet(response, EntityNames.RoomLayout);
        }

        [HttpPut("{id}")]
        [ProducesResponseType(typeof(RoomLayoutDto), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Put(Guid id, [FromBody] RoomLayoutDto dto)
        {
            var response = await RoomLayoutAppService.Update(id, dto);

            return CreateResponseOnPut(response, EntityNames.RoomLayout);
        }

        [HttpPost]
        [ProducesResponseType(typeof(RoomLayoutDto), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Post([FromBody] RoomLayoutDto dto)
        {
            var response = await RoomLayoutAppService.Create(dto);

            return CreateResponseOnPost(response, EntityNames.RoomLayout);
        }

        [HttpDelete("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Delete(Guid id)
        {
            await RoomLayoutAppService.Delete(id);

            return CreateResponseOnDelete(EntityNames.RoomLayout);
        }

        [HttpGet("byroomtype/{id}")]
        [ThexAuthorize("PMS_RoomLayout_Get_ByRoomType")]
        [ProducesResponseType(typeof(IListDto<RoomLayoutDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult GetAllLayoutsByRoomTypeId([FromQuery] RequestDto request, int id)
        {
            var requestDto = new DefaultIntRequestDto(id, request);

            var response = RoomLayoutAppService.GetAllLayoutsByRoomTypeId(requestDto);

            return CreateResponseOnGetAll(response, EntityNames.RoomLayout);
        }
    }
}
