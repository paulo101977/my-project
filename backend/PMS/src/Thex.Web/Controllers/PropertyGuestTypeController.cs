﻿using Thex.Application.Interfaces;
using Thex.Dto;
using Tnf.Dto;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Thex.Domain;
using Thex.Kernel;
using Thex.Web.Base;
using Tnf.AspNetCore.Mvc.Response;

namespace Thex.Web.Controllers
{
    [Route(RouteConsts.PropertyGuestType)]
    public class PropertyGuestTypeController : ThexAppController
    {
        private readonly IPropertyGuestTypeAppService PropertyGuestTypeAppService;

        public PropertyGuestTypeController(
            IApplicationUser applicationUser,
            IPropertyGuestTypeAppService propertyGuestTypeAppService)
            : base(applicationUser)
        {
            PropertyGuestTypeAppService = propertyGuestTypeAppService;
        }

        [HttpGet]
        [ProducesResponseType(typeof(IListDto<PropertyGuestTypeDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Get([FromQuery] GetAllPropertyGuestTypeDto requestDto)
        {
            var response = await PropertyGuestTypeAppService.GetAll(requestDto);

            return CreateResponseOnGetAll(response, EntityNames.PropertyGuestType);
        }

        [HttpGet("{id}")]
        [ProducesResponseType(typeof(PropertyGuestTypeDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Get(int id, [FromQuery] RequestDto requestDto)
        {
            var request = new DefaultIntRequestDto(id, requestDto);

            var response = await PropertyGuestTypeAppService.Get(request);

            return CreateResponseOnGet(response, EntityNames.PropertyGuestType);
        }

        [HttpPut("{id}")]
        [ThexAuthorize("PMS_PropertyGuestType_Put")]
        [ValidatePropertyIdFilter]
        [ProducesResponseType(typeof(PropertyGuestTypeDto), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Put(int id, [FromBody] PropertyGuestTypeDto dto)
        {
            var response = await PropertyGuestTypeAppService.Update(id, dto);

            return CreateResponseOnPut(response, EntityNames.PropertyGuestType);
        }

        [HttpPost]
        [ThexAuthorize("PMS_PropertyGuestType_Post")]
        [ValidatePropertyIdFilter]
        [ProducesResponseType(typeof(PropertyGuestTypeDto), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Post([FromBody] PropertyGuestTypeDto dto)
        {
            var response = await PropertyGuestTypeAppService.Create(dto);

            return CreateResponseOnPost(response, EntityNames.PropertyGuestType);
        }

        [HttpDelete("{id}")]
        [ThexAuthorize("PMS_PropertyGuestType_Delete")]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Delete(int id)
        {
            await PropertyGuestTypeAppService.Delete(id);

            return CreateResponseOnDelete(EntityNames.PropertyGuestType);
        }

        [HttpGet("{propertyId}/byproperty")]
        [ThexAuthorize("PMS_PropertyGuestType_Get_ByProperty")]
        [ProducesResponseType(typeof(IListDto<PropertyGuestTypeDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> GetAllByPropertyId([FromQuery] GetAllPropertyGuestTypeDto requestDto, int propertyId)
        {
            var response = await PropertyGuestTypeAppService.GetAllByPropertyId(requestDto, propertyId);

            return CreateResponseOnGetAll(response, EntityNames.PropertyGuestType);
        }

        [HttpPatch("{id}/toggleactivation")]
        [ThexAuthorize("PMS_PropertyGuestType_Patch_ToggleActivation")]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult ToggleActivation(int id)
        {
            PropertyGuestTypeAppService.ToggleActivation(id);

            return CreateResponseOnPatch(null, EntityNames.PropertyGuestType);
        }

    }
}
