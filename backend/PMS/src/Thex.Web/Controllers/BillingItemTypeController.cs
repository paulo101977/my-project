﻿using Thex.Application.Interfaces;
using Thex.Dto;
using Tnf.Dto;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Thex.Kernel;
using Thex.Web.Base;
using Thex.Domain;
using Tnf.AspNetCore.Mvc.Response;

namespace Thex.Web.Controllers
{
    [Route(RouteConsts.BillingItemType)]
    public class BillingItemTypeController : ThexAppController
    {
        private readonly IBillingItemTypeAppService BillingItemTypeAppService;

        public BillingItemTypeController(
            IApplicationUser applicationUser,
            IBillingItemTypeAppService billingItemTypeAppService)
            : base(applicationUser)
        {
            BillingItemTypeAppService = billingItemTypeAppService;
        }

        [HttpGet]
        [ProducesResponseType(typeof(IListDto<BillingItemTypeDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Get([FromQuery] GetAllBillingItemTypeDto requestDto)
        {
            var response = await BillingItemTypeAppService.GetAll(requestDto);

            return CreateResponseOnGetAll(response, EntityNames.BillingItemType);
        }

        [HttpGet("{id}")]
        [ProducesResponseType(typeof(BillingItemTypeDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Get(int id, RequestDto requestDto)
        {
            var request = new DefaultIntRequestDto(id, requestDto);

            var response = await BillingItemTypeAppService.Get(request);

            return CreateResponseOnGet(response, EntityNames.BillingItemType);
        }

        [HttpPut("{id}")]
        [ProducesResponseType(typeof(BillingItemTypeDto), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Put(int id, [FromBody] BillingItemTypeDto dto)
        {
            var response = await BillingItemTypeAppService.Update(id, dto);

            return CreateResponseOnPut(response, EntityNames.BillingItemType);
        }

        [HttpPost]
        [ProducesResponseType(typeof(BillingItemTypeDto), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Post([FromBody] BillingItemTypeDto dto)
        {
            var response = await BillingItemTypeAppService.Create(dto);

            return CreateResponseOnPost(response, EntityNames.BillingItemType);
        }

        [HttpDelete("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Delete(int id)
        {
            await BillingItemTypeAppService.Delete(id);

            return CreateResponseOnDelete(EntityNames.BillingItemType);
        }
    }
}
