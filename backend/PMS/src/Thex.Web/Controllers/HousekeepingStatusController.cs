﻿using Thex.Application.Interfaces;
using Thex.Dto;
using Tnf.Dto;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Thex.Kernel;
using Thex.Web.Base;
using Thex.Domain;
using Tnf.AspNetCore.Mvc.Response;

namespace Thex.Web.Controllers
{
    [Route(RouteConsts.HousekeepingStatus)]
    public class HousekeepingStatusController : ThexAppController
    {
        private readonly IHousekeepingStatusAppService HousekeepingStatusAppService;
        public HousekeepingStatusController(
            IApplicationUser applicationUser,
            IHousekeepingStatusAppService housekeepingStatusAppService)
            : base(applicationUser)
        {
            HousekeepingStatusAppService = housekeepingStatusAppService;
        }

        [HttpGet]
        [ProducesResponseType(typeof(IListDto<HousekeepingStatusDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Get([FromQuery] GetAllHousekeepingStatusDto requestDto)
        {
            var response = await HousekeepingStatusAppService.GetAll(requestDto);

            return CreateResponseOnGetAll(response, EntityNames.HousekeepingStatus);
        }

        [HttpGet("{id}")]
        [ProducesResponseType(typeof(HousekeepingStatusDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Get(int id, RequestDto requestDto)
        {
            var request = new DefaultIntRequestDto(id, requestDto);

            var response = await HousekeepingStatusAppService.Get(request);

            return CreateResponseOnGet(response, EntityNames.HousekeepingStatus);
        }

        [HttpPut("{id}")]
        [ProducesResponseType(typeof(HousekeepingStatusDto), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Put(int id, [FromBody] HousekeepingStatusDto dto)
        {
            var response = await HousekeepingStatusAppService.Update(id, dto);

            return CreateResponseOnPut(response, EntityNames.HousekeepingStatus);
        }

        [HttpPost]
        [ProducesResponseType(typeof(HousekeepingStatusDto), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Post([FromBody] HousekeepingStatusDto dto)
        {
            var response = await HousekeepingStatusAppService.Create(dto);

            return CreateResponseOnPost(response, EntityNames.HousekeepingStatus);
        }

        [HttpDelete("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Delete(int id)
        {
            await HousekeepingStatusAppService.Delete(id);

            return CreateResponseOnDelete(EntityNames.HousekeepingStatus);
        }
    }
}
