﻿using Thex.Application.Interfaces;
using Thex.Dto;
using Tnf.Dto;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Thex.Domain;
using Thex.Dto.Base;
using Thex.Dto.Rooms;
using Thex.Kernel;
using Thex.Web.Base;
using Thex.Dto.Room;
using Tnf.AspNetCore.Mvc.Response;
using Thex.Dto.Housekeeping;

namespace Thex.Web.Controllers
{
    [Route(RouteConsts.Room)]
    public class RoomController : ThexAppController
    {
        protected readonly IRoomAppService RoomAppService;

        public RoomController(
            IApplicationUser applicationUser,
            IRoomAppService roomAppService) : base(applicationUser)
        {
            RoomAppService = roomAppService;
        }

        [HttpGet]
        [ProducesResponseType(typeof(IListDto<RoomDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Get([FromQuery] GetAllRoomDto requestDto)
        {
            var response = await RoomAppService.GetAll(requestDto);

            return CreateResponseOnGetAll(response, EntityNames.Room);
        }

        [HttpGet("{id}")]
        [ThexAuthorize("PMS_Room_Get_Param")]
        [ProducesResponseType(typeof(RoomDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Get(int id, [FromQuery] RequestDto requestDto)
        {
            var request = new DefaultIntRequestDto(id, requestDto);

            var response = RoomAppService.GetRoom(request);

            return CreateResponseOnGet(response, EntityNames.Room);
        }

        [HttpPut("{id}")]
        [ThexAuthorize("PMS_Room_Put")]
        [ValidatePropertyIdFilter]
        [ProducesResponseType(typeof(RoomDto), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult Put(int id, [FromBody] RoomDto dto)
        {
            var response = RoomAppService.UpdateRoom(id, dto);

            return CreateResponseOnPut(response, EntityNames.Room);
        }

        [HttpPost]
        [ThexAuthorize("PMS_Room_Post")]
        [ValidatePropertyIdFilter]
        [ProducesResponseType(typeof(RoomDto), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult Post([FromBody] RoomDto dto)
        {
            var response = RoomAppService.CreateRoom(dto);

            return CreateResponseOnPost(response, EntityNames.Room);
        }       

        [HttpGet("{id}/childrooms")]
        [ProducesResponseType(typeof(IListDto<RoomDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult Get([FromQuery] GetAllRoomsDto request, int id)
        {
            var response = RoomAppService.GetChildRoomsByParentRoomId(request, id);

            return CreateResponseOnGetAll(response, EntityNames.Room);
        }

        [HttpPatch("{id}/toggleactivation")]
        [ThexAuthorize("PMS_Room_Patch_ToggleActivation")]
        [ProducesResponseType(200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult ToggleActivation(int id)
        {
            RoomAppService.ToggleActivation(id);

            return CreateResponseOnPatch(null, EntityNames.Room);
        }

        [HttpGet("{propertyId}/byperiodofreservationitems")]
        [ProducesResponseType(typeof(IListDto<RoomDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult GetAll([FromQuery]GetAllRoomsByPropertyAndPeriodDto request, int propertyId)
        {
            var response = RoomAppService.GetAllRoomsByPropertyAndPeriod(request, propertyId);

            return CreateResponseOnGetAll(response, EntityNames.Room);
        }

        [HttpGet("{propertyId}/{roomTypeId}/byperiodofroomtype")]
        [ThexAuthorize("PMS_Room_Get_ByPeriodOfRoomType")]
        [ProducesResponseType(typeof(IListDto<RoomDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult GetAllAvailable([FromQuery] PeriodDto request, int propertyId, int roomTypeId, int? reservationItemId)
        {
            var response = RoomAppService.GetAllAvailableByPeriodOfRoomType(request, propertyId, roomTypeId, reservationItemId, true);

            return CreateResponseOnGetAll(response, EntityNames.Room);
        }

        [HttpGet("{propertyId}/{roomTypeId}/byperiodofroomtype/changeofroom")]
        [ThexAuthorize("PMS_Room_Get_ByPeriodOfRoomType_ChangeOfRoom")]
        [ProducesResponseType(typeof(IListDto<RoomDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult GetAllAvailableChangeRoom([FromQuery] PeriodDto request, int propertyId, int roomTypeId, int? reservationItemId)
        {
            var response = RoomAppService.GetAllAvailableByPeriodOfRoomType(request, propertyId, roomTypeId, reservationItemId, false);

            return CreateResponseOnGetAll(response, EntityNames.Room);
        }

        [HttpGet("{propertyId}/getallroomwithhousekeepingstatus")]
        [ProducesResponseType(typeof(IListDto<GetAllHousekeepingAlterStatusDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult GetAllRoomWithHousekeepingStatus(int propertyId)
        {
            var response = RoomAppService.GetAllRoomWithHousekeepingStatus(propertyId);
            return CreateResponseOnGetAll(response, EntityNames.Room);
        }

        [HttpGet("{propertyId}/statuscheckinrooms")]
        [ThexAuthorize("PMS_Room_Get_StatusCheckinRooms")]
        [ProducesResponseType(typeof(IListDto<RoomDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public IActionResult GetAllRoomWihtGuest([FromQuery] GetAllRoomsByPropertyAndPeriodDto request, int propertyId)
        {
            var response = RoomAppService.GetAllStatusCheckinRooms(request, propertyId);

            return CreateResponseOnGetAll(response, EntityNames.Room);
        }

        [HttpGet("{propertyId}/{roomtypeId}/{reservatiomItemId}/budget")]
        [ProducesResponseType(typeof(Task<BudgetCurrentOrFutureDto>), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> GetCurrentOrFutureBudgetByReservationItem([FromQuery] GetAllRoomsByPropertyAndPeriodDto request, int propertyId,int roomTypeId,long reservatiomItemId)
        {
            var response =  await RoomAppService.GetCurrentOrFutureBudgetByReservationItem(propertyId, roomTypeId,request.InitialDate,request.FinalDate, reservatiomItemId);

            return CreateResponseOnGet(response, EntityNames.Room);
        }

        [HttpGet("gettotalandavailablerooms")]
        [ThexAuthorize("PMS_Room_Get_Total_And_Available_Rooms")]
        [ProducesResponseType(typeof(RoomsAvailableDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> GetTotalAndAvailableRoomsAsync()
        {
            var response = await RoomAppService.GetTotalAndAvailableRoomsByPropertyAsync();

            return CreateResponseOnGet(response, EntityNames.Room);
        }

        [HttpDelete("{roomId}")]
        [ThexAuthorize("PMS_Room_Delete")]
        [ProducesResponseType(typeof(RoomsAvailableDto), 200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        public async Task<IActionResult> Delete(int roomId)
        {
            RoomAppService.DeleteRoom(roomId);

            return CreateResponseOnDelete(EntityNames.Room);
        }
    }
}
