﻿using System;
using System.Collections.Generic;
using Thex.Common;
using Thex.Common.Enumerations;
using Thex.Domain.Entities;
using Thex.Kernel;
using Tnf.Application.Services;
using Tnf.Notifications;

namespace Thex.Application
{
    public abstract class ApplicationServiceBase : ApplicationService
    {
        protected ApplicationServiceBase(
            INotificationHandler notification)
            : base(notification)
        {
        }

        protected virtual void NotifyNullParameter()
        {
            Notification.Raise(Notification
                .DefaultBuilder
                .WithMessage(AppConsts.LocalizationSourceName, CommonsEnum.Error.NullOrEmptyObject)
                .Build());
        }

        protected virtual void NotifyIdIsMissing()
        {
            Notification.Raise(Notification
                .DefaultBuilder
                .WithMessage(AppConsts.LocalizationSourceName, CommonsEnum.Error.IdIsMissing)
                .Build());
        }

        protected virtual void NotifyParameterInvalid()
        {
            Notification.Raise(Notification
                .DefaultBuilder
                .WithMessage(AppConsts.LocalizationSourceName, CommonsEnum.Error.ParameterInvalid)
                .Build());
        }

        protected virtual void NotifyWhenEntityNotExist(string entityName)
        {
            Notification.Raise(Notification
                .DefaultBuilder
                .WithMessage(AppConsts.LocalizationSourceName, CommonsEnum.Error.EntityNotExist)
                .WithMessageFormat(entityName)
                .Build());
        }

        protected virtual void NotifyInvalidRangeDate()
        {
            Notification.Raise(Notification
                .DefaultBuilder
                .WithMessage(AppConsts.LocalizationSourceName, CommonsEnum.Error.DateGreaterThanAnotherDate)
                .Build());
        }

        protected virtual void NotifyMinimumCharacters(int quantity)
        {
            Notification.Raise(Notification
                .DefaultBuilder
                .WithMessage(AppConsts.LocalizationSourceName, CommonsEnum.Error.FieldMinimumOfCharacteres)
                .WithMessageFormat(quantity)
                .Build());
        }

        protected virtual void NotifyRequired(string field)
        {
            Notification.Raise(Notification
                .DefaultBuilder
                .WithMessage(AppConsts.LocalizationSourceName, CommonsEnum.Error.FieldRequired)
                .WithMessageFormat(field)
                .Build());
        }
        protected virtual void NotifyInvalidOrderBy()
        {
            Notification.Raise(Notification
                .DefaultBuilder
                .WithMessage(AppConsts.LocalizationSourceName, CommonsEnum.Error.InvalidOrderBy)
                .Build());
        }

        protected virtual void NotifyEmptyFilter()
        {
            Notification.Raise(Notification
                .DefaultBuilder
                .WithMessage(AppConsts.LocalizationSourceName, CommonsEnum.Error.EmptyFilter)
                .Build());
        }

        protected virtual void NotifyInvalidLocation()
        {
            Notification.Raise(Notification
                .DefaultBuilder
                .WithMessage(AppConsts.LocalizationSourceName, CommonsEnum.Error.InvalidyCountry)
                .Build());
        }

        protected virtual void ValidatePermission<T>(T entityAuditId, T userAuditId)
        {
            if(!EqualityComparer<T>.Default.Equals(entityAuditId, userAuditId))
                Notification.Raise(Notification
                .DefaultBuilder
                .WithMessage(AppConsts.LocalizationSourceName, CommonsEnum.Error.PermissionDenied)
                .Build());
        }

        protected List<DateTime> GenerateRangeDate(DateTime initialDate, DateTime endDate)
        {
            var dateList = new List<DateTime>();

            while (initialDate <= endDate)
            {
                dateList.Add(initialDate);
                initialDate = initialDate.AddDays(1);
            }

            return dateList;
        }
    }
}
