﻿using System.Threading.Tasks;
using Thex.Dto;
using Tnf.Application.Services;

namespace Thex.Application.Interfaces
{
    public interface ISibaIntegrationAppService : IApplicationService
    {
        Task Send(SibaIntegrationRequestDto dto);
    }
}
