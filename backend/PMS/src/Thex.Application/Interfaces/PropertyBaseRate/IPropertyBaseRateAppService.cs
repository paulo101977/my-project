﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.Dto;

namespace Thex.Application.Interfaces
{
    public interface IPropertyBaseRateAppService : IScaffoldPropertyBaseRateAppService
    {
        Task<PropertyBaseRateConfigurationParametersDto> GetConfigurationDataParameters(int propertyId);
        Task Create(BaseRateConfigurationDto dto);
        Task<List<PropertyBaseRateHeaderHistoryDto>> GetPropertyBaseRateHeaderHistory(int propertyId);
        Task CreateByLevelRate(PropertyBaseLevelRateDto dto);
        Task CreateByPremise(BaseRateConfigurationDto dto);
    }
}
