﻿namespace Thex.Application.Interfaces
{
    using Thex.Common.Enumerations;
    using Thex.Domain.Entities;
    using Thex.Dto;
    using Tnf.Dto;

    public interface IReasonAppService : IScaffoldReasonAppService
    {
        IListDto<ReasonDto> GetAllByPropertyIdAndReasonCategoryId(GetAllReasonDto request,int propertyId, int reasonCategoryId);
        IListDto<ReasonDto> GetAllByChainIdAndReasonCategory(GetAllReasonDto request);
        IListDto<ReasonDto> GetAllByPropertyId(GetAllReasonDto request, int propertyId);
        ReasonDto GetReasonByReasonId(int reasonId);
        void Delete(int id);
        void ToggleAndSaveIsActive(int id);
        void Update(ReasonDto reason);
        void CreateReason(ReasonDto reasonDto);
    }
}
