﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.Domain.Entities;
using Thex.Dto;
using Thex.Dto.GuestReservationItem;
using Tnf.Dto;

namespace Thex.Application.Interfaces
{
    public interface IGuestReservationItemAppService : IScaffoldGuestReservationItemAppService
    {
        GuestReservationItem.Builder GetGuestReservationItemBuilder(GuestReservationItemDto guestReservationItemDto, ReservationItemDto reservationItemDto, PropertyParameterDto childAgeParameter, bool update = false);
        GuestReservationItem.Builder GetGuestReservationItemFromReservationBuilder(GuestReservationItemDto guestReservationItemDto, ReservationItemDto reservationItemDto, PropertyParameterDto childAgeParameter, bool update = false);
        void Associate(long id, GuestReservationItemDto dto);
        void Disassociate(long id);
        void ConfirmCheckin(long id, List<GuestReservationIdsDto> dto, int propertyId);
        void ConfirmEntranceCheckin(GuestReservationItem dto, long reservationId, int propertyId, DateTime? systemDate);
        IListDto<GuestReservationItemDto> guestReservationItensByRoomId(long roomId);
        void Checkout(int propertyId, long id);
        IList<GuestReservationItem> GetGuestReservationItemByReservationItemId(long reservationItemId);
        void UpdateGuestEstimatedDepartureDate(long guestReservationItemId, DateTime estimatedDepartureDate);
        void UpdateGuestEstimatedArrivalAndDepartureDate(long guestReservationItemId, DateTime estimatedArrivalDat, DateTime estimatedDepartureDate);
        Task<GuestReservationItem> CreateByEntrance(GuestRegistrationDto guestRegistrationDto, ReservationItemDto reservationItem, GuestReservationItemEntranceDto dto);

        void UpdateGuestEstimatedDepartureDate(List<GuestReservationItem> guestReservationItemList, DateTime systemDate);
        IList<GuestReservationItem> GetGuestReservationItemByReservationItemId(List<long> reservationItemIdList);
    }
}
