﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.Dto;
using Tnf.Dto;

namespace Thex.Application.Interfaces
{
    public interface IPropertyMealPlanTypeAppService : IScaffoldPropertyMealPlanTypeAppService
    {
        IListDto<PropertyMealPlanTypeDto> GetAllPropertyMealPlanTypeByProperty(int propertyId, bool all = false);
        Task<IListDto<PropertyMealPlanTypeDto>> GetAllPropertyMealPlanTypeByProperty(bool all = false);
        void PropertyMealPlanTypeUpdate(IList<PropertyMealPlanTypeDto> propertyMealPlanTypeList);
        void ToggleActivation(Guid id);
    }
}
