﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.Domain.Entities;
using Thex.Dto;
using Thex.Dto.GuestRegistration;
using Thex.Dto.Person;
using Tnf.Dto;

namespace Thex.Application.Interfaces
{
    public interface IGuestRegistrationAppService : IScaffoldGuestRegistrationAppService
    {
        Task<GuestRegistrationDto> GetById(Guid Id);
        Task<GuestRegistrationDto> Create(GuestRegistrationDto dto, bool hasReservation);
        Task<GuestRegistrationDto> Update(Guid id, GuestRegistrationDto dto, bool hasReservation);

        GuestRegistrationResultDto GetGuestRegistrationByGuestReservationItem(long propertyId, long guestReservationItemId, string languageIsoCode);
        GuestRegistrationResultDto GetGuestRegistrationBasedOnFilter(SearchGuestRegistrationsDto request);
        IListDto<GuestRegistrationResultDto> GetAllGuestRegistrationBasedOnFilter(SearchGuestRegistrationsDto request);
        IListDto<GuestRegistrationResultDto> GetAllGuestRegistrationByReservationItemId(long propertyId, long reservationItemId, string languageIsoCode);
        IListDto<GuestRegistrationResultDto> GetAllResponsibles(long propertyId, long reservationId);
        IListDto<GuestRegistrationResultDto> GetAllResponsiblesFromReservation(long reservationId);
        void UpdateNationalityOfGuestRegistration(GuestRegistration guestRegistration, int nationalityId);        
    }
}
