﻿using System;
using System.Collections.Generic;
using Thex.Domain.Entities;
using Thex.Dto;
using Thex.Dto.HousekeepingStatusProperty;
using Tnf.Dto;

namespace Thex.Application.Interfaces
{
    public interface IRoomBlockingAppService : IScaffoldRoomBlockingAppService
    {
        List<RoomWithReservationsDto> BlockingRoom(RoomBlockingDto roomBlocking);
        void BlockingRoomList(List<RoomBlockingDto> roomBlockingList);
        void UnlockRoomList(List<int> roomBlockingList, int propertyId);
        void UnlockRoomForAvailability(Guid id);
        IEnumerable<int> UnlockAndGetRoomListByBlockingEndDate(int propertyId, DateTime blockingEndDate);
        IEnumerable<int> GetRoomBlockingListByBlockingStartDate(int propertyId, DateTime blockingStartDate);
    }
}
