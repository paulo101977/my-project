﻿using System.Threading.Tasks;
using Thex.Dto;
using Tnf.Dto;

namespace Thex.Application.Interfaces
{
    public interface IPropertyGuestTypeAppService : IScaffoldPropertyGuestTypeAppService
    {
        Task<IListDto<PropertyGuestTypeDto>> GetAllByPropertyId(GetAllPropertyGuestTypeDto request, int propertyId);
        void ToggleActivation(int id);
    }
}
