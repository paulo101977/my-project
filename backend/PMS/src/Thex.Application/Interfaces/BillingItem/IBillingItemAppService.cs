﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.Dto;
using Thex.Dto.BillingItem;
using Tnf.Dto;

namespace Thex.Application.Interfaces
{
    public interface IBillingItemAppService : IScaffoldBillingItemAppService
    {
        void CreateServiceAssociateTax(BillingItemServiceDto dto);
        void CreateTaxAssociateService(BillingItemTaxDto dto);
        BillingItemServiceDto GetBillingItem(long id, long propertyId);
        IListDto<GetAllBillingItemServiceDto> GetAllBillingItemService(long propertyId, bool? isActive);
        IListDto<GetAllBillingItemServiceDto> GetAllBillingItemServiceByFilters(SearchBillingItemDto requestDto, long propertyId);
        IListDto<GetAllBillingItemTaxDto> GetAllBillingItemTax(long propertyId);
        IListDto<GetAllBillingItemForAssociateTaxDto> GetAllBillingItemForAssociateTax(long propertyId);
        IListDto<GetAllBillingItemForAssociateServiceDto> GetAllBillingItemForAssociateService(long propertyId);
        void UpdateBillingItemTaxWithService(BillingItemServiceDto dto);
        void UpdateBillingItemServiceWithTax(BillingItemServiceDto dto);
        void Delete(int id, long propertyId);
        void ToggleActivation(int id);
        void ToggleActivationForTax(Guid id);
        BillingItemPaymentTypeDto CreatePropertyPaymentType(BillingItemPaymentTypeDto billingItemPaymentTypeDto);
        BillingItemPaymentTypeDto UpdatePropertyPaymentType(BillingItemPaymentTypeDto billingItemPaymentTypeDto);
        BillingItemPaymentTypeDto GetByPaymentTypeIdAndAcquirerId(int propertyId, int paymentTypeId, Guid acquirerId);
        BillingItemPaymentTypeDto GetByPaymentTypeId(int propertyId, int paymentTypeId);
        IListDto<BillingItemPaymentTypeDto> GetAllPaymentTypeIdByPropertyId(int propertyId);
        void DeletePaymentType(int propertyId, int paymentTypeId, Guid acquirerId);
        void ToggleActivationPaymentType(int propertyId, int paymentTypeId, Guid acquirerId, bool activate);
        IListDto<BillingItemServiceLancamentoDto> GetAllServiceWithAssociateTaxForDebitRealease(int propertyId, GetAllBillingItemServiceForRealase request);
        IListDto<BillingItemPlasticBrandCompanyClientDto> GetPlasticBrandWithCompanyClientByPaymentType(int paymentTypeId);
        IListDto<PaymentTypeWithBillingItemDto> GetPaymentTypes(GetAllPaymentTypeDto request);
        IListDto<BillingItemWithParametersDocumentsDto> GetAllServiceWithParameters(int propertyId);
        List<BillingItemDto> GetAllBillingItemService();
    }
}
