﻿using System.Collections.Generic;
using Thex.Domain.Entities;
using Thex.Dto;

namespace Thex.Application.Interfaces
{
    public interface IRatePlanCompanyClientAppService : IScaffoldRatePlanCompanyClientAppService
    {
        void RatePlanCompanyClientCreate(List<RatePlanCompanyClientDto> ratePlanCompanyClients);
    }
}
