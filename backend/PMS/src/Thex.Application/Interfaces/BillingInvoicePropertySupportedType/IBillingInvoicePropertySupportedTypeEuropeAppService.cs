﻿using System;
using System.Collections.Generic;
using Thex.Dto;

namespace Thex.Application.Interfaces
{
    public interface IBillingInvoicePropertySupportedTypeEuropeAppService
    {
        void CreateRange(List<BillingInvoicePropertySupportedTypeDto> BillingInvoicePropertySupportedTypes, Guid billingInvoicePropertyId, Guid childBillingInvoicePropertyId);
    }
}
