﻿using System;
using System.Collections.Generic;
using Thex.Dto;

namespace Thex.Application.Interfaces
{
    public interface IBillingInvoicePropertySupportedTypeAppService : IScaffoldBillingInvoicePropertySupportedTypeAppService
    {
        void BillingInvoicePropertySupportedTypeCreate(List<BillingInvoicePropertySupportedTypeDto> BillingInvoicePropertySupportedTypes, Guid billingInvoicePropertyId, Guid childBillingInvoicePropertyId = new Guid());
    }
}
