﻿using System;
using System.Collections.Generic;
using Thex.Dto;

namespace Thex.Application.Interfaces
{
    public interface IBillingInvoicePropertySupportedTypeBrazilAppService
    {
        void BillingInvoicePropertySupportedTypeCreate(List<BillingInvoicePropertySupportedTypeDto> BillingInvoicePropertySupportedTypes, Guid billingInvoicePropertyId,int propertyId);
    }
}
