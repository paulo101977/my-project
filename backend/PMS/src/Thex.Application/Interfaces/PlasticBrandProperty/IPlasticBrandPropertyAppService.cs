﻿using System;
using Thex.Dto;
using Thex.Dto.PlasticBrandProperty;
using Tnf.Dto;

namespace Thex.Application.Interfaces
{
    public interface IPlasticBrandPropertyAppService : IScaffoldPlasticBrandPropertyAppService
    {
        IListDto<PlasticBrandPropertyResultDto> GetAllByPropertyId(GetAllPlasticBrandPropertyDto request);
    }
}
