﻿using Thex.Dto;
using Tnf.Dto;
using Tnf.Dto;

namespace Thex.Application.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Thex.Domain.Entities;
    using Thex.Dto.ReservationItem;

    public interface IReservationItemAppService : IScaffoldReservationItemAppService
    {
        IListDto<ReservationItemDto> GetAllByPropertyAndReservation(RequestAllDto request, long reservationId);
        IListDto<AccommodationsByReservationIdResultDto> GetAllAccommodations(RequestAllDto request, long propertyId, long reservationId);
        void CancelReservationItem(long reservationItem, ReservationItemCancelDto dto);
        void ReactivateReservationItem(long reservationItemId);
        IListDto<GuestGuestRegistrationResultDto> GetAllGuestGuestRegistration(long propertyId, long reservationItemId, string language);
        void AssociateRoom(int propertyId, long reservationItemId, int roomTypeId,  int? roomId = null);
        void UpdateToCheckInReservationItem(long resertionItemId, int propertyId, DateTime checkInDate);
        void ChangePeriodAndRoomTypeAndRoom(int propertyId, long reservationItemId, ChangePeriodReservationItemDto dto);
        void RealeseReservationItem(long reservationItemId);
        void PatchReservationItemBudget(int propertyId, long reservationItemId, ReservationItemWithBudgetDto dto);
        List<GuestReservationItem> GetGuestReservationItemByReservationItemId(long reservationItemId);
        void AccommodationChange(AccommodationChangeDto reservationItemChange);
        void Checkout(int propertyId, long reservationItemId);
        EarlyCheckoutResponseDto IsEarlyCheckout(int propertyId, long reservationItemId);
        TemplateReservationSlipDto GenerateHtmlForSlipReservationAccommodation(int propertyId, long reservationId);
        void SendMailConfirmationReservation(List<string> emailsList, int propertyId, long reservationId);
        void ConfirmReservationItem(long reservationItemId);
        Task<ReservationItemDto> UpdateByEntrance(ReservationItemDto dto, bool isChild);
        void ReactivateNoShow(long reservationItemId);
    }
}
