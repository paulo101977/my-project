﻿using System.Threading.Tasks;
using Thex.Dto;
using Tnf.Dto;

namespace Thex.Application.Interfaces
{
    public interface IPaymentTypeAppService : IScaffoldPaymentTypeAppService
    {
        Task<IListDto<PaymentTypeDto>> GetAllForCreation(GetAllPaymentTypeDto request);
    }
}
