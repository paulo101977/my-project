﻿using System;
using System.Threading.Tasks;
using Thex.Dto;
using Tnf.Dto;

namespace Thex.Application.Interfaces
{
    public interface IPropertyMealPlanTypeRateAppService : IScaffoldPropertyMealPlanTypeRateAppService
    {
        PropertyMealPlanTypeRateHeaderDto GetById(Guid id);
        IListDto<PropertyMealPlanTypeRateDto> GetAll(GetAllPropertyMealPlanTypeRateDto dto);
        Task<PropertyMealPlanTypeRateHeaderDto> Create(PropertyMealPlanTypeRateHeaderDto dto);
        void ToggleActivation(Guid id);
    }
}
