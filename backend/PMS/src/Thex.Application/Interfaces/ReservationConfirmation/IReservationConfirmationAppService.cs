﻿
using Thex.Dto;
using System;
using Thex.Domain.Entities;

namespace Thex.Application.Interfaces
{
    public interface IReservationConfirmationAppService : IScaffoldReservationConfirmationAppService
    {
		ReservationConfirmation.Builder GetBuilder(ReservationConfirmationDto reservationConfirmationDto, ReservationDto reservationDto, DateTime firstCheckinDate, long? plasticId);

        ReservationConfirmation.Builder GetBuilder(ReservationConfirmationDto reservationConfirmationDto, ReservationDto reservationDto, DateTime firstCheckinDate);
        void UpdateReservationEnsuresNoShow(ReservationConfirmation reservationConfirmation, bool ensuresNoShow);
    }
}
