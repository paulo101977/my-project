﻿
using Thex.Domain.Entities;
using Thex.Dto;

namespace Thex.Application.Interfaces
{
    public interface IChainMarketSegmentAppService : IScaffoldChainMarketSegmentAppService
    {
        ChainMarketSegment.Builder GetBuilder(MarketSegmentDto MarketSegmentDto, int chainId);
        ChainMarketSegment ToDomain(MarketSegmentDto MarketSegment, int chainId);
        void Create(MarketSegmentDto MarketSegmentDto, int chainId);
    }
}
