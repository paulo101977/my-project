﻿using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Thex.Common.Dto.InvoiceIntegration;
using Thex.Common.Enumerations;
using Thex.Dto;
using Thex.Dto.BillingAccountClosure;
using Thex.Dto.Integration;
using Tnf.Application.Services;


namespace Thex.Application.Interfaces
{
    public interface IIntegrationBrazilAppService : IApplicationService
    {
        Task<NotificationResponseDto> GenerateInvoice(List<BillingAccountClosureInvoiceDto> billingInvoiceList, BillingInvoiceTypeEnum invoiceType);
        Task<BillingInvoiceIntegrationResponseDto> UpdateStatus(InvoiceIntegrationResponseDto invoiceResponsetDto);
        Task<Stream> Download(SearchBillingInvoiceDto requestDto, int propertyId);
        Task<string> DownloadSetupUrl(string propertyId);
    }
}
