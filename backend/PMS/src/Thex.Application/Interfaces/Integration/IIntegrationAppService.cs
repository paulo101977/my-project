﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Thex.Common.Dto.InvoiceIntegration;
using Thex.Common.Enumerations;
using Thex.Dto;
using Thex.Dto.BillingAccountClosure;
using Thex.Dto.Integration;
using Tnf.Application.Services;

namespace Thex.Application.Interfaces
{
    public interface IIntegrationAppService : IApplicationService
    {
        Task<NotificationResponseDto> GenerateInvoice(List<BillingAccountClosureInvoiceDto> billingInvoiceList, BillingInvoiceTypeEnum invoiceType, BillingAccountItemCreditDto payment = null);
        Task<NotificationResponseDto> GenerateCreditNote(BillingInvoiceTypeEnum invoiceType, Guid billingInvoiceId);
        Task<BillingInvoiceIntegrationResponseDto> UpdateStatus(InvoiceIntegrationResponseDto invoiceResponsetDto);
        Task<NotificationResponseDto> ResendInvoice(List<BillingAccountClosureInvoiceDto> billingInvoiceList);
        Task<Stream> Download(SearchBillingInvoiceDto requestDto, int propertyId);
        Task<string> DownloadSetupUrl(string propertyId);
        Task<string> GetIntegratorIdById(Guid id);
    }
}
