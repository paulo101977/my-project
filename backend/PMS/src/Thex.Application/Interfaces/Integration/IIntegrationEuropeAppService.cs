﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.Common.Enumerations;
using Thex.Dto;
using Thex.Dto.BillingAccountClosure;
using Tnf.Application.Services;

namespace Thex.Application.Interfaces
{
    public interface IIntegrationEuropeAppService : IApplicationService
    {
        Task<NotificationResponseDto> GenerateCreditNote(BillingInvoiceTypeEnum invoiceType, Guid billingInvoiceId);
        Task<NotificationResponseDto> GenerateInvoice(List<BillingAccountClosureInvoiceDto> billingInvoiceList, BillingInvoiceTypeEnum invoiceType, BillingAccountItemCreditDto payment = null);
        Task<string> GetIntegratorIdById(Guid id);
    }
}
