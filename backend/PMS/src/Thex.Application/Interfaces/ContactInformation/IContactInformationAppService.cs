﻿
using Thex.Domain.Entities;
using Thex.Dto;
using System.Collections.Generic;
using System;

namespace Thex.Application.Interfaces
{
    public interface IContactInformationAppService
    {
        ContactInformation.Builder GetBuilder(ContactInformationDto contactInformationDto);
        ICollection<ContactInformation> ToDomain(ICollection<ContactInformationDto> contactInformations);
        string GetEmailContactInformationByPersonId(Guid guid);
    }
}
