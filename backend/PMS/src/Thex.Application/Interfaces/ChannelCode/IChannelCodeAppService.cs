﻿using System.Threading.Tasks;
using Thex.Dto;
using Tnf.Dto;

namespace Thex.Application.Interfaces
{
    public interface IChannelCodeAppService : IScaffoldChannelCodeAppService
    {
        Task<IListDto<ChannelCodeDto>> GetAllChannelsCodeDtoAsync();
        ChannelCodeDto GetDtoById(int id);
    }
}
