﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.Domain.Entities;
using Thex.Dto;
using Tnf.Dto;

namespace Thex.Application.Interfaces
{
    public interface IReservationBudgetAppService : IScaffoldReservationBudgetAppService
    {
        Task<ReservationItemBudgetHeaderDto> GetWithBudgetOffer(ReservationItemBudgetRequestDto requestDto, int propertyId);
        void UpdateReservationBudgetList(int propertyId, ReservationItem reservationItem, ReservationItemWithBudgetDto dto);
        ReservationBudget GetReservationBudgetEntity(ReservationBudgetDto reservationBudgetDto);
        Task UpdateByReservationItemId(int propertyId, long reservationItemId);
        Task UpdateByReservationId(int propertyId, long reservationId);
        void ValidateReservationBudgetDays(int totalDaysBudget, DateTime? CheckInDate, DateTime? CheckOutDate, DateTime EstimatedArrivalDate, DateTime EstimatedDepartureDate, int reservationItemStatusId);
        IList<ReservationBudget> GetAllByReservationItemId(long reservationItemId);
        void UpdateEntranceBudget(IList<ReservationBudgetDto> reservationBudgetDtoList, long reservationItemId, bool isSeparated, DateTime? systemDate);
        void DeleteRange(DateTime systemDate, long reservationItemId, bool isEarlyDayUse);
        void ValidateMigratedReservationBudget(ICollection<ReservationBudget> reservationBudgetList, ICollection<ReservationBudgetDto> reservationBudgetDtoList);
        void RemoveRangeNoShow(DateTime? systemDate, long reservationItemId);
    }
}
