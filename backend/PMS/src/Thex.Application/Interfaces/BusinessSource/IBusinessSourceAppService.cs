﻿namespace Thex.Application.Interfaces
{
    using System.Threading.Tasks;
    using Thex.Dto;
    using Tnf.Dto;

    public interface IBusinessSourceAppService : IScaffoldBusinessSourceAppService
    {
        void ToggleActivation(int id);

        IListDto<BusinessSourceDto> GetAllByPropertyId(GetAllBusinessSourceDto request, int propertyId);
    }
}
