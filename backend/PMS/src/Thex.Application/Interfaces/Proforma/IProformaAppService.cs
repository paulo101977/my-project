﻿using System.Threading.Tasks;
using Thex.Dto;

namespace Thex.Application.Interfaces
{
    public interface IProformaAppService
    {
        Task<NotificationResponseDto> CreateProforma(ProformaRequestDto requestDto);
    }
}
