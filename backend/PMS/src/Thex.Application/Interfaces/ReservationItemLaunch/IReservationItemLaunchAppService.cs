﻿using System.Collections.Generic;
using Thex.Dto;
using Tnf.Application.Services;

namespace Thex.Application.Interfaces
{
    public interface IReservationItemLaunchAppService : IApplicationService
    {
        void CreateToTourismTax(List<BillingAccountGuestReservationItemDto> billingAccountGuestReservationItemDtoList, TourismTaxDto tourismTaxDto);
        void UpdateToTourismTax(List<BillingAccountGuestReservationItemDto> billingAccountGuestReservationItemDtoList, TourismTaxDto tourismTaxDto);
    }
}
