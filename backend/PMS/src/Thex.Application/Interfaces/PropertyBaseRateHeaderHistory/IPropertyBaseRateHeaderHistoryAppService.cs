﻿using System;
using System.Threading.Tasks;
using Thex.Domain.Entities;
using Thex.Dto;

namespace Thex.Application.Interfaces
{
    public interface IPropertyBaseRateHeaderHistoryAppService : IScaffoldPropertyBaseRateHeaderHistoryAppService
    {
        PropertyBaseRateHeaderHistory.Builder Create(BaseRateConfigurationDto dto);
    }
}
