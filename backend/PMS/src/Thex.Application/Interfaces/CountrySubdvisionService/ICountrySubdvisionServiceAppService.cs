﻿using System;
using Thex.Dto;
using Tnf.Dto;

namespace Thex.Application.Interfaces
{
    public interface ICountrySubdvisionServiceAppService : IScaffoldCountrySubdvisionServiceAppService
    {
        IListDto<CountrySubdvisionServiceDto> GetAllByPropertyId(int propertyId);
    }
}
