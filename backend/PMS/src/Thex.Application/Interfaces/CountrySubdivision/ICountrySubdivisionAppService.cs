﻿using Thex.Dto;
using Tnf.Dto;

namespace Thex.Application.Interfaces
{
    public interface ICountrySubdivisionAppService : IScaffoldCountrySubdivisionAppService
    {
        IListDto<CountryDto> GetAllCountriesByLanguageIsoCode(GetAllCountryDto request);
        (int?, int?, int?) CreateCountrySubdivisionTranslateAndGetCityId(LocationDto location, string language);
    }
}
