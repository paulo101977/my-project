﻿using Thex.Dto.Reservation;
using Thex.Dto.Availability;
using Tnf.Application.Services;
using System.Collections.Generic;
using System;

namespace Thex.Application.Interfaces
{
    public interface IAvailabilityAppService : IApplicationService
    {
        AvailabilityDto GetAvailabilityRoomTypes(SearchAvailabilityDto requestDto, int propertyId, DateTime? start = null, DateTime? final = null);
        AvailabilityRoomsDto GetAvailabilityRooms(SearchAvailabilityDto requestDto, int propertyId, int roomTypeId);
    }
}
