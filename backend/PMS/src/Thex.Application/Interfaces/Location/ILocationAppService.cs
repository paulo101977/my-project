﻿
using Thex.Domain.Entities;
using Thex.Dto;
using System.Collections.Generic;

namespace Thex.Application.Interfaces
{
    using System;

    public interface ILocationAppService : IScaffoldLocationAppService
    {
        void CreateLocationList(ICollection<LocationDto> locationDtoList, Guid personId);
        LocationDto Get(DefaultIntRequestDto id);
        Location.Builder GetBuilder(LocationDto locationDto);
        Location.Builder GetBuilder(LocationDto locationDto, Guid personId);
        ICollection<Location> ToDomain(ICollection<LocationDto> locations);
        LocationDto Update(LocationDto location);
        void UpdateLocationList(IList<LocationDto> locationDtoList, string ownerId);
        Location GetLocationForUpdated(LocationDto locationDto, string personId);
        void CreateOrUpdateGuestRegistrationLocation(LocationDto locationDto, string ownerId);
        void CreateOrUpdatePersonHeaderLocation(Location location, Guid ownerId);
    }
}
