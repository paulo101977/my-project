﻿using System;
using System.Collections.Generic;
using Thex.Dto;
using Tnf.Application.Services;

namespace Thex.Application.Interfaces
{
    public interface ITourismTaxAppService : IApplicationService
    {
        TourismTaxDto GetByPropertyId();
        TourismTaxDto Create(TourismTaxDto dto);
        TourismTaxDto Update(TourismTaxDto dto);
        void ToggleActivation(Guid id);
        void Launch(List<TourismTaxLaunchRequestDto> requestDtoList);
        void Launch();
    }
}
