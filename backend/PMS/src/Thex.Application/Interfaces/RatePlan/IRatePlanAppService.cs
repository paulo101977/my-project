﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.Domain.Entities;
using Thex.Dto;
using Thex.Dto.RatePlan;
using Thex.Dto.RoomTypes;
using Tnf.Dto;

namespace Thex.Application.Interfaces
{
    public interface IRatePlanAppService : IScaffoldRatePlanAppService
    {
        HeaderRatePlanDto GetHeaderRatePlan(int propertyId);
        IListDto<RoomTypeDto> GetRoomTypesByPropertyId(GetAllRoomTypesDto request, int propertyId);
        IListDto<GetAllCompanyClientResultDto> GetAllByPropertyId(GetAllCompanyClientDto request, int propertyId);
        IListDto<BillingItemDto> GetAllBillingItemByPropertyId(int propertyId);
        void RatePlanCreate(RatePlanDto ratePlanDto);
        RatePlanDto GetRatePlanById(Guid id);
        IListDto<RatePlanDto> GetAllRatePlanSearch(GetAllRatePlanDto request, int propertyId);
        void RatePlanUpdate(RatePlanDto ratePlanDto);
        void ToggleAndSaveIsActive(Guid id);
        IListDto<PropertyCompanyClientCategoryDto> GetAllCompanyClientCategoryByPropertyId(GetAllPropertyCompanyClientCategoryDto request, int propertyId);
        Task<ListDto<PropertyBaseRateHeaderHistoryDto>> GetRatePlanHistoryById(Guid id);
        Task<ListDto<PropertyBaseRateHeaderHistoryDto>> GetRatePlanHistoryByDate(Guid ratePlanId, DateTime dateTimeHistory);
        Task<ListDto<PropertyBaseRateHeaderHistoryDto>> GetRatePlanHistoryByHistoryId(Guid id);
        Task<ListDto<RatePlanDto>> GetAllBaseRateByPeriodAndPropertyId(DateTime startDate, DateTime endDate, int propertyId);
    }     
}
