﻿using System.Collections.Generic;
using Thex.Domain.Entities;
using Thex.Dto;
using Thex.Dto.BillingItemCategory;
using Tnf.Dto;

namespace Thex.Application.Interfaces
{
    public interface IBillingItemCategoryAppService : IScaffoldBillingItemCategoryAppService
    {
        IListDto<BillingItemCategoryDto> GetAllGroupFixed();
        IListDto<BillingItemCategoryDto> GetItemsByGroupId(int groupId);
        IListDto<BillingItemCategoryDto> GetAllGroupByPropertyId(GetAllBillingItemCategoryDto request, int propertyId);
        IListDto<GetAllCategoriesForItemDto> GetAllCategoriesForItem(int propertyId);
        void ToggleActivation(int id);
        void DeleteBillingCategory(int id);

    }
}
