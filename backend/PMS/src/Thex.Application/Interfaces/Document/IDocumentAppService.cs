﻿
using Thex.Domain.Entities;
using Thex.Dto;
using System.Collections.Generic;

namespace Thex.Application.Interfaces
{
    using System;

    public interface IDocumentAppService
    {
        Document.Builder GetBuilder(DocumentDto documentDto);
        ICollection<Document> ToDomain(ICollection<DocumentDto> documents);
        void SaveDocumentList(ICollection<DocumentDto> documentListDto, Guid personId);
        ICollection<Document> getDocumentListFromDto(ICollection<DocumentDto> documentDtoList, Guid personId);
        void UpdateDocumentList(IList<DocumentDto> DocumentDtoList, string ownerId);
        void CreateOrUpdateGuestRegistrationDocument(DocumentDto documentDto, string ownerId);
        ICollection<DocumentDto> GetDocumentDtoListByPersonId(Guid owerId);
        void UpdateDocumentPersonHeader(DocumentDto dto, Guid ownerId);
    }
}
