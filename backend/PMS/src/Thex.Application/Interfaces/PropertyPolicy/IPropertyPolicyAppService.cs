﻿using System;
using Thex.Dto;
using Tnf.Dto;

namespace Thex.Application.Interfaces
{
    public interface IPropertyPolicyAppService : IScaffoldPropertyPolicyAppService
    {
        void ToggleAndSaveIsActive(Guid id);
        void Delete(Guid id);
        void PropertyPolicyCreate(PropertyPolicyDto propertyPolicy);
        void PropertyPolicyUpdate(PropertyPolicyDto propertyPolicy);
        PropertyPolicyDto GetPropertyPolicy(Guid guid);
        IListDto<PropertyPolicyDto> GetAllPropertyPolicy(int propertyId);
        IListDto<PolicyTypeDto> GetAllPolicyTypes();

    }
}
