﻿using System;
using System.Collections.Generic;
using Thex.Dto;
using Tnf.Dto;

namespace Thex.Application.Interfaces
{
    public interface ILevelRateAppService : IScaffoldLevelRateAppService
    {
        IListDto<LevelRateHeaderDto> GetAllDto();
        LevelRateResultDto GetById(Guid levelRateHeaderId);
        LevelRateResultRequestDto GetAllLevelsAvailable(LevelRateRequestDto request);
        void Remove(Guid id);
        void ToggleActive(Guid id);
        void Create(LevelRateResultDto dto);
        void Update(LevelRateResultDto dto, Guid id);
        IListDto<PropertyBaseRateByLevelRateResultDto> GetAllLevelRateForPropertyBaseRate(DateTime initialDate, DateTime endDate, Guid? currencyId);
    }
}
