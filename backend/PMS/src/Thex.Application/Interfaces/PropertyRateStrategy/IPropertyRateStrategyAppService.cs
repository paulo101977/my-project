﻿using System;
using System.Threading.Tasks;
using Thex.Dto;
using Tnf.Application.Services;
using Tnf.Dto;

namespace Thex.Application.Interfaces
{
    public interface IPropertyRateStrategyAppService : IApplicationService
    {
        IListDto<GetAllPropertyRateStrategyDto> GetAllByPropertyId();
        Task<PropertyRateStrategyDto> GetPropertyRateStrategyById(Guid propertyRateStrategyId);
        Task<PropertyRateStrategyDto> CreateAsync(PropertyRateStrategyDto propertyRateStrategyDto);
        Task<PropertyRateStrategyDto> UpdateAsync(Guid id, PropertyRateStrategyDto propertyRateStrategyDto);
        void ToggleAndSaveIsActive(Guid id);
    }
}
