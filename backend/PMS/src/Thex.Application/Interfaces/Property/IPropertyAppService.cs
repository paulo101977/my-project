﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.Dto;
using Tnf.Dto;

namespace Thex.Application.Interfaces
{
    public interface IPropertyAppService : IScaffoldPropertyAppService
    {
        PropertyDto GetProperty(DefaultIntRequestDto id);
        void CreateProperty(PropertyDto brand);
        void UpdateProperty(int id, PropertyDto propertyDto);
        void DeleteProperty(int id);
        IListDto<PropertyDto> GetAllProperty(IRequestAllDto request, bool central = false);
        PropertyDto GetByPropertyId(int propertyId);
        GetPropertySiba GetPropertySiba();
        Task<PropertyDto> SendPhoto(PropertyDto request);
    }
}
