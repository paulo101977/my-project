﻿using Thex.Dto;
using Thex.Dto.RoomStatus;
using Tnf.Application.Services;
using Tnf.Dto;

namespace Thex.Application.Interfaces.RoomStatus
{
    public interface IRoomStatusAppService : IApplicationService
    {
        ListDto<RoomStatusDto> GetAll(GetAllRoomStatusDto getAllRoomStatusDto, int propertyId);
    }
}
