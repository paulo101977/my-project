﻿using System;
using System.Threading.Tasks;
using Thex.Dto;
using Thex.Dto.RoomTypes;
using Tnf.Dto;

namespace Thex.Application.Interfaces
{
    public interface IRoomTypeAppService : IScaffoldRoomTypeAppService
    {
        IListDto<RoomTypeDto> GetRoomTypesByPropertyId(GetAllRoomTypesDto request, int propertyId);
        void ToggleActivation(int id);
        IListDto<RoomTypeOverbookingDto> GetAllRoomTypesByPropertyAndPeriodDto(GetAllRoomTypesByPropertyAndPeriodDto request, int propertyId);

        RoomTypeDto CreateRoomType(RoomTypeDto roomTypeDto);
        RoomTypeDto UpdateRoomType(int id, RoomTypeDto roomTypeDto);
        RoomTypeDto GetRoomTypeByRoomId(int roomId);
        RoomTypeDto GetRoomTypeById(int roomTypeId);
        void DeleteRoomTypeAsync(int id);

    }
}
