﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.Dto;
using Thex.Dto.PropertyParameter;
using Tnf.Dto;

namespace Thex.Application.Interfaces
{
    public interface IPropertyParameterAppService : IScaffoldPropertyParameterAppService
    {
        PropertyParameterDividedByGroupDto GetPropertyParameterByPropertyId(int propertyId);
        void ToggleAndSaveIsActive(Guid id);
        void UpdateParameter(PropertyParameterDto parameter);
        Task<List<PropertyParameterDto>> GetChildrenAgeGroupList(int propertyId);
        IListDto<PropertyParameterDto> GetChildrenAgeGroupForRoomTypeList(int propertyId);
        PropertyParameterDto GetSystemDate(int propertyId);
    }
}
