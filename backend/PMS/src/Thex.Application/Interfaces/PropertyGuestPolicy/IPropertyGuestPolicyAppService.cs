﻿using System;
using System.Collections.Generic;
using Thex.Dto;

namespace Thex.Application.Interfaces
{
    public interface IPropertyGuestPolicyAppService
    {
        void Create(PropertyGuestPolicyDto propertyGuestPolicy);
        List<PropertyGuestPolicyDto> GetAll();
        PropertyGuestPolicyDto GetById(Guid id);
        void Update(PropertyGuestPolicyDto propertyGuestPolicyDto);
        void Delete(Guid id);
    }
}
