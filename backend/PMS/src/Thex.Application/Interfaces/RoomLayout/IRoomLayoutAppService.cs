﻿using Thex.Dto;
using System;
using Tnf.Dto;
using Tnf.Dto;

namespace Thex.Application.Interfaces
{
    public interface IRoomLayoutAppService : IScaffoldRoomLayoutAppService
    {
        IListDto<RoomLayoutDto> GetAllLayoutsByRoomTypeId(DefaultIntRequestDto id);
    }
}
