﻿using System;
using System.Threading.Tasks;
using Thex.Domain.Entities;
using Thex.Dto;
using Tnf.Dto;

namespace Thex.Application.Interfaces
{
    public interface IPropertyPremiseHeaderAppService : IScaffoldPropertyPremiseHeaderAppService
    {
        IListDto<PropertyPremiseHeaderListDto> GetAllHeader(GetAllPropertyPremiseHeaderDto dto);
        PropertyPremiseHeaderListDto GetById(Guid id);
        PropertyPremiseHeader Create(PropertyPremiseHeaderDto dto);
        void ToggleActivation(Guid id);
    }
}
