﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.Dto;
using Thex.Dto.Report;
using Thex.Dto.Reservation;
using Thex.Dto.ReservationReport;
using Thex.Dto.Room;
using Thex.Dto.SearchReservationReportDto;
using Tnf.Dto;

namespace Thex.Application.Interfaces.ReservationReport
{
    public interface IReservationReportAppService
    {
        ListDto<SearchReservationReportResultReturnDto> GetAllByFilters(SearchReservationReportDto request, int propertyId);
        ListDto<SearchMeanPlanControlReportResultDto> GetAllMeanPlanByFilters(SearchReservationReportDto request, int propertyId);
        PostMeanPlanControlReportDto PostMeanPlan(PostMeanPlanControlReportDto request);
        ListDto<SearchGuestSaleReportResultDto> GetAllGuestSaleByFilters(int propertyId, bool guestSale,
            bool sparseSale, bool masterGroupSale, bool companySale, bool eventSale, bool pendingSale, bool positiveSale, bool canceledSale,
            bool noShowSale, double? sale, bool aheadSale);

        ListSearchIssueNotesReportResultDto GetAllIssuedNotes(int propertyId, SearchIssuedNotesReportDto requestDto);
        SearchReservationCheckReportResultList GetAllCheckByFilters(SearchReservationReportDto request, int propertyId);
        ListDto<SearchReservationUhReportResultReturnDto> GetAllUhSituationByFilters(GetAllSituationRoomsDto request, int propertyId);
        ListSearchBordereauReportResultDto GetBordereau(SearchBordereauReportDto requestDto, int propertyId);
        IListDto<ReportHousekeepingStatusDisagreementReturnDto> GetAllHousekeepingStatusDisagreement(SearchReportHousekeepingStatusDisagreementDto request, int propertyId);
        List<PostHousekeepingRoomDisagreementDto> PostHousekeepingRoomDisagreement(List<PostHousekeepingRoomDisagreementDto> request);
        ReportReservationsMadeReturnListDto GetReservationsMade(SearchReservationsMadeReportDto requestDto, int propertyId);
        Task<ReportReturnDto> GetGraficReport(int propertyId);
        ListDto<ReservationsInAdvanceReturnDto> GetAllReservationsInAdvance(SearchReservationsInAdvanceDto request, int propertyId);
        Task<PropertyDashboardDto> GetDashboardInformationsAsync();
    }
}
