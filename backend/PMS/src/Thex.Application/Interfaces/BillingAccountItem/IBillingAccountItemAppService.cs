﻿using System;
using System.Collections.Generic;
using Thex.Domain.Entities;
using Thex.Dto;
using Thex.Dto.BillingAccountItem;

namespace Thex.Application.Interfaces
{
    public interface IBillingAccountItemAppService : IScaffoldBillingAccountItemAppService
    {
        BillingAccountItemDebitDto CreateDebit(BillingAccountItemDebitDto dto, bool validateServiceIntegration = true);
        BillingAccountItemCreditDto CreateCredit(BillingAccountItemCreditDto dto);
        BillingAccountItemReversalParentDto CreateReversal(BillingAccountItemReversalParentDto dto);
        BillingAccountItemTransferSourceDestinationDto CreateTransfer(BillingAccountItemTransferSourceDestinationDto dto);
        void CreateTransferAccountsToAccount(Guid billingAccountDestination, List<Guid> billingAccountIdList);
        List<BillingAccountItem> GetAllByBillingAccountId(Guid billingAccountId);
        BillingAccountItemUndoTransferSourceDestinationDto UndoTransfer(BillingAccountItemUndoTransferSourceDestinationDto dto);
        void CreateTransferAccountsToAccount(Guid billingAccountDestination, List<Guid> billingAccountIdList, List<Guid> billingAccountItemsIdList);
        void CreateBillingAccountItemDiscountList(IList<BillingAccountItemDiscountDto> dto);
        BillingAccountItemPartialDto CreatePartial(BillingAccountItemPartialDto dto);
        BillingAccountItemUndoPartialDto UndoPartial(BillingAccountItemUndoPartialDto dto);
        BillingAccountItemDebitDto CreateDebitIntegration(BillingAccountItemDebitDto dto);
        List<BillingAccountItem> GetAllForSetInvoiceIdByBillingAccountId(Guid billingAccountId);
        BillingAccountItemReversalParentDto CreateReversalIntegration(BillingAccountItemReversalParentDto dto);
        IList<BillingAccountItemCreditAmountDto> GetAllBillingAccountItemCreditAmountByInvoiceId(Guid billingInvoiceId);
        void CreateAllDailyDebit(List<BillingAccountItemDebitDto> debitDtoList);
        List<BillingAccountItem> CreateAllTaxTourismDebit(List<BillingAccountItemTourismTaxDto> tourismTaxDtoList);
        BillingAccountItemIntegrationDto CreateDebitAndCreditWithInvoice(BillingAccountItemIntegrationDto dto);
    }
}
