﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.Dto;
using Tnf.Dto;

namespace Thex.Application.Interfaces
{
    public interface IChannelAppService : IScaffoldChannelAppService
    {
        Task<IListDto<ChannelDto>> GetAllChannelsDtoByTenantIdAsync(GetAllChannelDto requestDto);
        ChannelDto GetDtoById(Guid id);

        void DeleteChannel(Guid id);
        ChannelDto CreateChannel(ChannelDto channelDto);
        void ToggleAndSaveIsActive(Guid id);
        ChannelDto UpdateChannel(Guid id, ChannelDto channelDto);
        IListDto<ChannelDto> SearchChannel(string search);
    }
}
