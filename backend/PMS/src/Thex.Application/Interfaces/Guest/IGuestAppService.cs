﻿using System;
using System.Collections.Generic;
using Thex.Dto;
using Thex.Dto.Person;

namespace Thex.Application.Interfaces
{
    public interface IGuestAppService : IScaffoldGuestAppService
    {
        long CreateGuestIfNotExistAndReturnGuest(Guid personId);

        SearchResultDto SearchBasedOnFilter(SearchPersonsDto request);
        List<GetAllForeingGuest> GetAllForeingGuestByFilters(DateTime startDate, DateTime endDate);
    }
}
