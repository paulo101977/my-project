﻿using System;
using System.Collections.Generic;
using Thex.Dto;
using Thex.Dto.PropertyCurrencyExchange;
using Tnf.Dto;

namespace Thex.Application.Interfaces
{
    public interface IPropertyCurrencyExchangeAppService : IScaffoldPropertyCurrencyExchangeAppService
    {
        IListDto<QuotationCurrentDto> GetAllQuotationByPeriod(int propertyId, DateTime? startDate, Guid currencyId, DateTime? endDate);
        QuotationCurrentDto GetQuotationByCurrencyId(int propertyId, Guid currencyId);
        void UpdateQuotationFuture(List<PropertyCurrencyExchangeDto> quotationList);
        void InsertQuotationByPeriod(InsertQuotationByPeriodDto quotationByPeriod);
        List<QuotationCurrentDto> GetCurrentQuotationList();
        void PatchCurrentQuotation(QuotationCurrentDto quotationCurrentDto);
    }
}
