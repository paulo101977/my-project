﻿using Thex.Dto;
using Tnf.Dto;

namespace Thex.Application.Interfaces
{
    public interface IReasonCategoryAppService : IScaffoldReasonCategoryAppService
    {
        IListDto<ReasonCategoryGetAllDto> GetAllReasonCategory();
    }
}
