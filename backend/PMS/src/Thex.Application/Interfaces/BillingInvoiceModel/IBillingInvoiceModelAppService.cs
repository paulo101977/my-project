﻿using Thex.Dto;
using Tnf.Dto;

namespace Thex.Application.Interfaces
{
    public interface IBillingInvoiceModelAppService : IScaffoldBillingInvoiceModelAppService
    {
        IListDto<BillingInvoiceModelDto> GetAllVisible();
    }
}
