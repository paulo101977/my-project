﻿using Thex.Dto.Person;
using Tnf.Dto;
using System;

namespace Thex.Application.Interfaces
{
    public interface IPersonAppService : IScaffoldPersonAppService
    {
        IListDto<SearchPersonsResultDto> GetPersonsBasedOnFilter(SearchPersonsDto request);
        Guid CreateOrUpdateHeaderPersonAndGetId(HeaderPersonDto personDto);
        HeaderPersonDto GetHeaderByBillingAccountId(Guid billingAccountId);
        HeaderPersonDto GetHeaderByBillingInvoiceId(Guid billingInvoiceId);
    }
}
