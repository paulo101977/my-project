﻿using Thex.Dto;
using Tnf.Application.Services;
using System.Threading.Tasks;


namespace Thex.Application.Interfaces
{
    public interface IBillingAccountClosureBrazilAppService : IApplicationService
    {
        Task<BillingAccountClosureResultDto> Create(BillingAccountClosureDto dto);
        Task<BillingAccountClosureResultDto> CreatePartial(BillingAccountClosurePartial dto);
    }
}
