﻿
using Thex.Domain.Entities;
using Thex.Dto;
using System.Collections.Generic;
using Tnf.Dto;

namespace Thex.Application.Interfaces
{
    public interface IEmployeeAppService : IScaffoldEmployeeAppService
    {
        Employee.Builder GetBuilder(EmployeeDto employeeDto);
        ICollection<Employee> ToDomain(ICollection<EmployeeDto> employees);
    }
}
