﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a Crudzilla tool.
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using Tnf.Application.Services;
using Thex.Dto;
using System.Threading.Tasks;
using Tnf.Dto;

namespace Thex.Application.Interfaces
{
    public interface IScaffoldCountryCurrencyAppService : IApplicationService
    {
        Task<CountryCurrencyDto> Create(CountryCurrencyDto dto);
        Task Delete(Guid id);
        Task<CountryCurrencyDto> Get(DefaultGuidRequestDto id);
        Task<IListDto<CountryCurrencyDto>> GetAll(GetAllCountryCurrencyDto request);
        Task<CountryCurrencyDto> Update(Guid id, CountryCurrencyDto dto);
    }
}
