﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a Crudzilla tool.
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using Tnf.Application.Services;
using Thex.Dto;
using System.Threading.Tasks;
using Tnf.Dto;

namespace Thex.Application.Interfaces
{
    public interface IScaffoldTenantAppService : IApplicationService
    {
        Task<TenantDto> Create(TenantDto dto);
        Task Delete(Guid id);
        Task<TenantDto> Get(DefaultGuidRequestDto id);
        Task<IListDto<TenantDto>> GetAll(GetAllTenantDto request);
        Task<TenantDto> Update(Guid id, TenantDto dto);
    }
}
