﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Thex.Dto;
using Tnf.Application.Services;
using Tnf.Dto;

namespace Thex.Application.Interfaces
{
    public interface IScaffoldGuestRegistrationDocumentAppService : IApplicationService
    {
        Task<GuestRegistrationDocumentDto> Create(GuestRegistrationDocumentDto dto);
        Task Delete(Guid id);
        Task<GuestRegistrationDocumentDto> Get(DefaultGuidRequestDto id);
        Task<IListDto<GuestRegistrationDocumentDto>> GetAll(GetAllGuestRegistrationDocumentDto request);
        Task<GuestRegistrationDocumentDto> Update(Guid id, GuestRegistrationDocumentDto dto);
    }
}
