﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a Crudzilla tool.
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using Tnf.Application.Services;
using Thex.Dto;
using System.Threading.Tasks;
using Tnf.Dto;

namespace Thex.Application.Interfaces
{
    public interface IScaffoldPersonInformationTypeAppService : IApplicationService
    {
        Task<PersonInformationTypeDto> Create(PersonInformationTypeDto dto);
        Task Delete(int id);
        Task<PersonInformationTypeDto> Get(DefaultIntRequestDto id);
        Task<IListDto<PersonInformationTypeDto>> GetAll(GetAllPersonInformationTypeDto request);
        Task<PersonInformationTypeDto> Update(int id, PersonInformationTypeDto dto);
    }
}
