﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a Crudzilla tool.
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using Tnf.Application.Services;
using Thex.Dto;
using System.Threading.Tasks;
using Tnf.Dto;

namespace Thex.Application.Interfaces
{
    public interface IScaffoldPropertyBusinessPartnerAppService : IApplicationService
    {
        Task<PropertyBusinessPartnerDto> Create(PropertyBusinessPartnerDto dto);
        Task Delete(Guid id);
        Task<PropertyBusinessPartnerDto> Get(DefaultGuidRequestDto id);
        Task<IListDto<PropertyBusinessPartnerDto>> GetAll(GetAllPropertyBusinessPartnerDto request);
        Task<PropertyBusinessPartnerDto> Update(Guid id, PropertyBusinessPartnerDto dto);
    }
}
