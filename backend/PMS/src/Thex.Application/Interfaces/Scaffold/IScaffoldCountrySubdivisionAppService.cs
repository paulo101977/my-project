﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a Crudzilla tool.
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using Tnf.Application.Services;
using Thex.Dto;
using System.Threading.Tasks;
using Tnf.Dto;

namespace Thex.Application.Interfaces
{
    public interface IScaffoldCountrySubdivisionAppService : IApplicationService
    {
        Task<CountrySubdivisionDto> Create(CountrySubdivisionDto dto);
        Task Delete(int id);
        Task<CountrySubdivisionDto> Get(DefaultIntRequestDto id);
        Task<IListDto<CountrySubdivisionDto>> GetAll(GetAllCountrySubdivisionDto request);
        Task<CountrySubdivisionDto> Update(int id, CountrySubdivisionDto dto);
    }
}
