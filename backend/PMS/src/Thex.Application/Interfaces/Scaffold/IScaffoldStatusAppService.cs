﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a Crudzilla tool.
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using Tnf.Application.Services;
using Thex.Dto;
using System.Threading.Tasks;
using Tnf.Dto;

namespace Thex.Application.Interfaces
{
    public interface IScaffoldStatusAppService : IApplicationService
    {
        Task<StatusDto> Create(StatusDto dto);
        Task Delete(int id);
        Task<StatusDto> Get(DefaultIntRequestDto id);
        Task<IListDto<StatusDto>> GetAll(GetAllStatusDto request);
        Task<StatusDto> Update(int id, StatusDto dto);
    }
}
