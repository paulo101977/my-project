﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Thex.Dto;
using Tnf.Application.Services;
using Tnf.Dto;

namespace Thex.Application.Interfaces
{
    public interface IScaffoldCompanyClientChannelAppService : IApplicationService
    {
        Task<CompanyClientChannelDto> Create(CompanyClientChannelDto dto);
        Task Delete(Guid id);
        Task<CompanyClientChannelDto> Get(DefaultIntRequestDto id);
        Task<IListDto<CompanyClientChannelDto>> GetAll(GetAllCompanyClientChannelDto request);
        Task<CompanyClientChannelDto> Update(Guid id, CompanyClientChannelDto dto);
    }
}
