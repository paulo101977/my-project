﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a Crudzilla tool.
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using Tnf.Application.Services;
using Thex.Dto;
using System.Threading.Tasks;
using Tnf.Dto;

namespace Thex.Application.Interfaces
{
    public interface IScaffoldPaymentTypeAppService : IApplicationService
    {
        Task<PaymentTypeDto> Create(PaymentTypeDto dto);
        Task Delete(int id);
        Task<PaymentTypeDto> Get(DefaultIntRequestDto id);
        Task<IListDto<PaymentTypeDto>> GetAll(GetAllPaymentTypeDto request);
        Task<PaymentTypeDto> Update(int id, PaymentTypeDto dto);
    }
}
