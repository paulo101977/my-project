﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a Crudzilla tool.
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using Tnf.Application.Services;
using Thex.Dto;
using System.Threading.Tasks;
using Tnf.Dto;

namespace Thex.Application.Interfaces
{
    public interface IScaffoldPlasticAppService : IApplicationService
    {
        Task<PlasticDto> Create(PlasticDto dto);
        Task Delete(long id);
        Task<PlasticDto> Get(DefaultLongRequestDto id);
        Task<IListDto<PlasticDto>> GetAll(GetAllPlasticDto request);
        Task<PlasticDto> Update(long id, PlasticDto dto);
    }
}
