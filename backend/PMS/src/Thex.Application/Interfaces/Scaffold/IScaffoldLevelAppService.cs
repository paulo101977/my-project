﻿
using System;
using Tnf.Application.Services;
using System.Threading.Tasks;
using Tnf.Dto;
using Thex.Dto;
using System.Collections.Generic;

namespace Thex.Application.Interfaces
{
    public interface IScaffoldLevelAppService : IApplicationService
    {
        Task<LevelDto> Create(LevelDto dto);
        Task Delete(Guid id);
        Task<LevelDto> Get(DefaultIntRequestDto id);
        Task<LevelDto> Update(Guid id, LevelDto dto);       
    }
}
