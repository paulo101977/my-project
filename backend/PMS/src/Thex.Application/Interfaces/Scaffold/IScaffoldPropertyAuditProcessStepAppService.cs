﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a Crudzilla tool.
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using Tnf.Application.Services;
using Thex.Dto;
using System.Threading.Tasks;
using Tnf.Dto;

namespace Thex.Application.Interfaces
{
    public interface IScaffoldPropertyAuditProcessStepAppService : IApplicationService
    {
        Task<PropertyAuditProcessStepDto> Create(PropertyAuditProcessStepDto dto);
        Task Delete(Guid id);
        Task<PropertyAuditProcessStepDto> Get(DefaultGuidRequestDto id);
        Task<IListDto<PropertyAuditProcessStepDto>> GetAll(GetAllPropertyAuditProcessStepDto request);
        Task<PropertyAuditProcessStepDto> Update(Guid id, PropertyAuditProcessStepDto dto);
    }
}
