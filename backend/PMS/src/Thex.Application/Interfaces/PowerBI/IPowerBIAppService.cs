﻿using System.Threading.Tasks;
using Thex.Dto.PowerBI;

namespace Thex.Application.Interfaces
{
    public interface IPowerBIAppService
    {
        Task<PowerBiMenuDto> GetAllAsync();
    }
}
