﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.Dto;

namespace Thex.Application.Interfaces
{
    public interface IBillingInvoiceAppService : IScaffoldBillingInvoiceAppService
    {
        BillingInvoiceDto CreateBillingInvoice(BillingInvoiceDto dto);
        Task<RpsDto> GetRpsByInvoiceId(int propertyId, Guid invoiceId, string languageIsoCode);
        Task<BillingInvoiceDto> CreateCreditNoteAndUpdateLastNumber(List<BillingAccountItemCreditAmountDto> billingAccountItemDtoList);
    }
}
