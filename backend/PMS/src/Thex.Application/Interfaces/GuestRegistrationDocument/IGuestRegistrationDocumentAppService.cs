﻿using System;
using System.Collections.Generic;
using System.Text;
using Thex.Dto;

namespace Thex.Application.Interfaces
{
    public interface IGuestRegistrationDocumentAppService : IScaffoldGuestRegistrationDocumentAppService
    {
        ICollection<GuestRegistrationDocumentDto> GetAllByGuestRegistrationId(Guid guestRegistrationId);
        void CreateOrUpdateGuestRegistrationDocument(ICollection<GuestRegistrationDocumentDto> documentDtoList, Guid guestRegistrationId);
    }
}
