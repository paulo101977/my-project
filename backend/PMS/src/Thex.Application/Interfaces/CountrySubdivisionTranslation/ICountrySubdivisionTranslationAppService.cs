﻿using Thex.Dto;
using Tnf.Dto;

namespace Thex.Application.Interfaces
{
    public interface ICountrySubdivisionTranslationAppService : IScaffoldCountrySubdivisionTranslationAppService
    {
        IListDto<CountrySubdivisionTranslationDto> GetAllNationalities(GetAllCountrySubdivisionTranslationDto request);
        CountrySubdivisionTranslationDto GetNationality(int Nationality);
    }
}
