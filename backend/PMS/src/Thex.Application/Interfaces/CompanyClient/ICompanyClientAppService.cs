﻿namespace Thex.Application.Interfaces
{
    using System;
    using Thex.Dto;
    using Tnf.Dto;
    using System.Collections.Generic;
    using Thex.Dto.Person;
    using System.Threading.Tasks;

    public interface ICompanyClientAppService : IScaffoldCompanyClientAppService
    {
        IListDto<GetAllCompanyClientResultDto> GetAllByPropertyId(GetAllCompanyClientDto request, int propertyId);

        IListDto<GetAllCompanyClientSearchResultDto> GetAllBasedOnFilter(GetAllCompanyClientSearchDto request, int propertyId);

        IListDto<GetAllCompanyClientContactInformationSearchResultDto> GetAllContactPersonByCompanyClientId(GetAllCompanyClientContactInformationSearchDto request);

        void ToggleActivation(string id);

        CompanyClientDto CreateBySparseBillingAccount(CompanyClientDto companyClientDto);

        void SaveLocationList(CompanyClientDto companyClientDto, Guid ownerId);

        IListDto<SearchPersonsResultDto> GetAllBasedOnFilterWithLocation(GetAllCompanyClientSearchDto request, int propertyId);

        IListDto<CompanyClientDto> GetAcquirerCompanyClient(GetAllCompanyClientDto request);
        bool CheckAvailableChannelAssociation(CompanyClientChannelDto dto);
        Task AssociateClientListWithTributsWithheld(List<CompanyClientWithTributeWihheldDto> dtoList);
        Task<List<GetAllCompanyClientResultDto>> GetAllAssociatedWithTributWithheld();
        Task DeleteAssociationClientListWithTributsWithheld(Guid companyClientId);
    }
}
