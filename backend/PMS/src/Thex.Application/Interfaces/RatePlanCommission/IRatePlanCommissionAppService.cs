﻿using System.Collections.Generic;
using Thex.Dto;

namespace Thex.Application.Interfaces
{
    public interface IRatePlanCommissionAppService : IScaffoldRatePlanCommissionAppService
    {
        void RatePlanCommissionCreate(List<RatePlanCommissionDto> ratePlanCommissions);
    }
}
