﻿using System;
using System.Threading.Tasks;
using Thex.Dto;
using Thex.Dto.ExternalRegistration;
using Tnf.Application.Services;
using Tnf.Dto;

namespace Thex.Application.Interfaces
{
    public interface IExternalRegistrationAppService : IApplicationService
    {
        Task<string> CreateAsync(Guid reservationUid, string email);
        Task<ExternalRoomListHeaderDto> GetRoomListAsync();
        Task<IListDto<ExternalGuestRegistrationDto>> GetRegistrationListAsync(Guid reservationItemUid);
        Task<ExternalGuestRegistrationDto> GetRegistrationAsync(long guestReservationItemId);
        Task<ExternalGuestRegistrationDto> UpdateAsync(long guestReservationItemId, ExternalGuestRegistrationDto dto);
    }
}
