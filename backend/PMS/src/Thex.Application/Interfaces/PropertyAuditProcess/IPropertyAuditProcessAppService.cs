﻿using Thex.Dto;
using Thex.Dto.PropertyAuditProcessStep;

namespace Thex.Application.Interfaces
{
    public interface IPropertyAuditProcessAppService : IScaffoldPropertyAuditProcessAppService
    {
        PropertyAuditProcessStepDto Execute(int propertyId);
        PropertyAuditProcessStepNumber GetAuditStepByPropertyId(int propertyId);
        PropertyAuditProcessTimeDto GetTimeForAuditByPropertyId(int propertyId);
    }
}
