﻿using System;
using Thex.Dto;
using Tnf.Dto;

namespace Thex.Application.Interfaces
{
    public interface IPropertyCompanyClientCategoryAppService : IScaffoldPropertyCompanyClientCategoryAppService
    {
        IListDto<PropertyCompanyClientCategoryDto> GetAllByPropertyId(GetAllPropertyCompanyClientCategoryDto request, int propertyId);
        PropertyCompanyClientCategoryDto GetPropertyCompanyClientCategoryById(DefaultGuidRequestDto id);
        void ToggleAndSaveIsActive(Guid id);
        new void Delete(Guid id);
    }
}
