﻿
using Thex.Domain.Entities;
using Thex.Dto;
using System.Collections.Generic;
using Tnf.Dto;

namespace Thex.Application.Interfaces
{
    public interface IPersonInformationAppService : IScaffoldPersonInformationAppService
    {
        PersonInformation.Builder GetBuilder(PersonInformationDto personInformationDto);
        ICollection<PersonInformation> ToDomain(ICollection<PersonInformationDto> personInformations);

    }
}
