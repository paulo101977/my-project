﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.Dto;
using Tnf.Dto;

namespace Thex.Application.Interfaces
{
    public interface ILevelAppService : IScaffoldLevelAppService
    {
        IListDto<LevelDto> GetAllDto(GetAllLevelDto requestDto);
        void DeleteLevel(Guid id);
        LevelDto GetDtoById(Guid id);
        LevelDto CreateLevel(LevelDto levelDto);
        void ToggleAndSaveIsActive(Guid id);
        LevelDto UpdateLevel(LevelDto levelDto);
    }
}
