﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.Domain.Entities;
using Thex.Dto;

namespace Thex.Application.Interfaces
{
    public interface IGuestEntranceAppService : IScaffoldReservationBudgetAppService
    {
        Task<ReservationItemBudgetHeaderDto> GetWithBudgetOffer(ReservationItemBudgetRequestDto requestDto, int propertyId);
        Task ConfirmEntrance(ReservationItemGuestDto requestDto);

        Task<GuestRegistrationDto> CreateGuest(GuestRegistrationDto dto);
        Task<GuestRegistrationDto> UpdateGuest(Guid id, GuestRegistrationDto dto);
    }
}
