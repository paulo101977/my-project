﻿using System;
using System.Threading.Tasks;
using Thex.Dto;


namespace Thex.Application.Interfaces
{
    public interface IBillingInvoicePropertyPortugalAppService : IScaffoldBillingInvoicePropertyAppService
    {
        new void Create(BillingInvoicePropertyDto billingInvoicePropertyDto);
        BillingInvoicePropertyDto GetById(Guid id);
        void UpdateBillingInvoiceProperty(BillingInvoicePropertyDto billingInvoicePropertyDto);
        Task<byte[]> GetBillingInvoicePdf(Guid id);
    }
}
