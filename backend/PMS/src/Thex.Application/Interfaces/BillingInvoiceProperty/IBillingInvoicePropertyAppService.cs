﻿using System;
using System.Threading.Tasks;
using Thex.Dto;
using Tnf.Dto;


namespace Thex.Application.Interfaces
{
    public interface IBillingInvoicePropertyAppService : IScaffoldBillingInvoicePropertyAppService
    {
        new void Create(BillingInvoicePropertyDto billingInvoicePropertyDto);
        IListDto<BillingInvoicePropertyDto> GetAllByPropertyId(int propertyId, GetAllBillingInvoicePropertyDto request);
        BillingInvoicePropertyDto GetById(Guid id);
        void UpdateBillingInvoiceProperty(BillingInvoicePropertyDto billingInvoicePropertyDto);
        Task CancelBillingInvoiceAsync(int propertyId, Guid billlingInvoiceId, int reasonId);
        Task<byte[]> GetBillingInvoicePdf(Guid id);
        IListDto<BillingInvoicePropertyDto> GetAllWithoutReference();
        BillingAccountWithAccountsDto GetInvoiceDetails(Guid billingInvoiceId, Guid billingAccountId);
        Task<byte[]> GetProformaPdf(long reservationId, long reservationItemId);
    }
}
