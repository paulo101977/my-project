﻿using System;
using System.Collections.Generic;
using Thex.Domain.Entities;
using Thex.Dto;
using Thex.Dto.BillingAccount;
using Thex.Dto.Person;
using Tnf.Dto;

namespace Thex.Application.Interfaces
{
    public interface IBillingAccountAppService : IScaffoldBillingAccountAppService
    {
        IListDto<SearchBillingAccountResultDto> GetAllByFilters(SearchBillingAccountDto request, int propertyId);
        BillingAccountDto CreateSparceAccount(BillingAccountDto dto);
        BillingAccountHeaderDto BillingAccountHeader(Guid billingAccountId);
        IListDto<BillingAccountSidebarDto> BillingAccountSidebar(Guid billingAccountId);
        BillingAccountWithAccountsDto GetWithAccountEntries(DefaultGuidRequestDto id, bool withInvoices = true);
        BillingAccountWithAccountsDto GetBillingAccountStatement(DefaultGuidRequestDto id);
        BillingAccountDto AddBillingAccount(AddBillingAccountDto dto, BillingAccount copyBillingAccount = null, Guid groupKey = new Guid(), bool usePersonHeaderId = false);
        BillingAccountTotalForClosureDto GetBillingAccountsForClosure(Guid id, bool allSimiliarAccounts);
        IListDto<BillingAccountWithAccountsDto> GetAllWithAccountEntriesList(List<Guid> ids);
        BillingAccountWithCompleteAccountDto GetAllWithAccountEntriesComplete(Guid id);
        IListDto<SearchBillingAccountResultDto> GetAllBillingAccountsByUhOrSparseAccount(Guid id, int propertyId);
        IListDto<SearchBillingAccountResultDto> GetAllByFiltersForTransferSearch(SearchBillingAccountDto request, int propertyId);
        void ReopenBillingAccount(int propertyId, Guid id, int reasonId);
        void CheckIfExistIsMainBillingAccount(long reservationId, long reservationItemId, long? guestReservationItem, Guid? companyClient, int billingAccountTypeId);
        BillingAccountDto GetByInvoiceId(Guid invoiceId);
        BillingAccountWithAccountsDto GetUniqueBillingAccountWithEntries(DefaultGuidRequestDto id, Guid invoiceId, bool withInvoices = true);
        Guid CreateAndSetNewMainAccount(Guid groupKey);
        BillingAccount ChangeMainAccount(BillingAccount newBillingAccountMain);
        HeaderPersonDto AssociatePersonHeader(HeaderPersonDto personDto);
        HeaderPersonDto GetHeaderByBillingAccountId(Guid billingAccountId);
        HeaderPersonDto GetHeaderByBillingInvoiceId(Guid billingInvoiceId);
        BillingAccountDto AddBillingAccountCreditNote(AddBillingAccountDto dto, BillingAccount copyBillingAccount = null);
        List<InvoiceDto> GetInvoicesAccount(DefaultGuidRequestDto id);
        void CheckIfExistIsMainBillingAccountByGuestReservationItemIds(List<long?> guestReservationItemIds);
    }
}
