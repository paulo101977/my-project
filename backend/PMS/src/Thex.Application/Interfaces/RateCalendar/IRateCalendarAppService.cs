﻿using Thex.Dto.Reservation;
using Thex.Dto.Availability;
using Tnf.Application.Services;
using System.Collections.Generic;
using Tnf.Dto;
using System.Threading.Tasks;
using Thex.Dto;

namespace Thex.Application.Interfaces
{
    public interface IRateCalendarAppService : IApplicationService
    {
        Task<ListDto<RateCalendarDto>> GetRateCalendar(SearchRateCalendarDto requestDto, int propertyId);
        Task<PropertyBaseRateConfigurationParametersDto> GetCalendarDataParameters(int propertyId);
    }
}
