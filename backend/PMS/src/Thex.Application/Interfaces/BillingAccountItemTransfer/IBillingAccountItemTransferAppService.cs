﻿using System.Collections.Generic;
using Thex.Dto;

namespace Thex.Application.Interfaces
{
    public interface IBillingAccountItemTransferAppService : IScaffoldBillingAccountItemTransferAppService
    {
        void CreateRange(List<BillingAccountItemTransferDto> transferListDto);
    }
}
