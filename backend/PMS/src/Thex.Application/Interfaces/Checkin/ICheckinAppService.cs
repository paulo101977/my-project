﻿using Thex.Domain.Entities;
using Thex.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace Thex.Application.Interfaces
{
    public interface ICheckinAppService
    {
        Reservation GetReservationForChekin(long reservationId);
        void CreateAccountInCompany(Reservation reservation, int propertyId);
        void CreateAccountGuest(GuestReservationItem item, long reservationId, int propertyId, int businessSourceId, int marketSegmentId);
        void CreateAccountGuestFirst(GuestReservationItem item, long reservationId, int propertyId, int businessSourceId, int marketSegmentId);
        List<BillingAccount> GetAllForGuestReservationItemId(List<long> guestReservationItemId, long reservationId);
        void CreateBillingAccountForGroup(long reservationId, Guid? companyClientId, GuestReservationItem guestReservationItem, int propertyId, int businessSourceId, int marketSegmentId, string groupName);
        void UpdateToCheckInGuestReservationItem(GuestReservationItem guestReservationItem, DateTime checkinDate);
        bool AnyGroupAccount(long reservationId);
        bool AnyCompanyAccountAccomodation(long reservationItemId);
        void CreateBillingAccountForCompany(long reservationItemId, Reservation reservation, int propertyId);
        ReservationDto GetBusinessSourceAndMarketSegmentByReservationId(long id);
        void Cancel(long guestReservationItemId);
    }
}
