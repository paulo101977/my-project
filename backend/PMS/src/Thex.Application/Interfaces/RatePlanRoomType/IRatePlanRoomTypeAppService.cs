﻿using System.Collections.Generic;
using Thex.Dto;

namespace Thex.Application.Interfaces
{
    public interface IRatePlanRoomTypeAppService : IScaffoldRatePlanRoomTypeAppService
    {
        void RatePlanRoomTypeCreate(List<RatePlanRoomTypeDto> ratePlanRoomTypes);
    }
}
