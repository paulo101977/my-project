﻿using System;
using System.Collections.Generic;
using Thex.Domain.Entities;
using Thex.Dto;
using Thex.Dto.Housekeeping;
using Tnf.Dto;

namespace Thex.Application.Interfaces
{
    public interface IHousekeepingStatusPropertyAppService : IScaffoldHousekeepingStatusPropertyAppService
    {
        IListDto<HousekeepingStatusPropertyDto> GetAll(GetAllHousekeepingStatusPropertyDto request, int propertyId);
        HousekeepingStatusPropertyDto Get(Guid guid);
        void ToggleAndSaveIsActive(Guid id);
        void HousekeepingStatusPropertyCreate(HousekeepingStatusPropertyDto housekeepingStatusProperty);
        //void Update(HousekeepingStatusPropertyDto housekeepingStatusProperty);
        void HousekeepingStatusPropertyDelete(Guid id);
        void Update(HousekeepingStatusPropertyDto housekeepingStatusProperty);

        IListDto<GetAllHousekeepingAlterStatusDto> GetAllHousekeepingAlterStatusRoom(GetAllHousekeepingAlterStatusRoomDto request, int propertyId);
        IListDto<HousekeepingStatusPropertyDto> GetAllHousekeepingStatusCreateBySystem(int propertyId);
        void UpdateRoomForHousekeeppingStatus(int roomId, Guid housekeepingStatusPropertyId);
        void UpdateManyRoomForHousekeeppingStatus(List<int> roomIds, Guid housekeepingStatusPropertyId);
        Guid GetHousekeepingStatusIdDirty();
        Guid GetHousekeepingStatusIdMaintenance();
    }
}
