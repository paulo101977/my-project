﻿
using Thex.Domain.Entities;
using Thex.Dto;

namespace Thex.Application.Interfaces
{
    public interface IChainBusinessSourceAppService
    {
        ChainBusinessSource.Builder GetBuilder(BusinessSourceDto businessSourceDto, int chainId);
        ChainBusinessSource ToDomain(BusinessSourceDto businessSource, int chainId);
        void Create(BusinessSourceDto businessSourceDto, int chainId);
      
    }
}
