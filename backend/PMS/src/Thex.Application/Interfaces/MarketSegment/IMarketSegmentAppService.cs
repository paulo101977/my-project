﻿namespace Thex.Application.Interfaces
{
    using System.Threading.Tasks;
    using Thex.Dto;
    using Tnf.Dto;

    public interface IMarketSegmentAppService : IScaffoldMarketSegmentAppService
    {
        void ToggleActivation(int id);
        IListDto<MarketSegmentDto> GetAllByPropertyId(GetAllMarketSegmentDto request, int propertyId);
        MarketSegmentDto GetById(int id);
    }
}
