﻿using System.Threading.Tasks;
using Thex.Common.Enumerations;
using Thex.Dto;

namespace Thex.Application.Interfaces.RateProposal
{
    public interface IRateProposalAppService
    {        
        Task<ReservationItemBudgetHeaderDto> GetAllhBudgetOfferWithRateProposal(ReservationItemBudgetRequestDto requestDto, int propertyId);       
    }
}
