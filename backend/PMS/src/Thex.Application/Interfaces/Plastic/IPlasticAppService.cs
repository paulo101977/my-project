﻿
using Thex.Domain.Entities;
using Thex.Dto;

namespace Thex.Application.Interfaces
{
    public interface IPlasticAppService : IScaffoldPlasticAppService
    {
        Plastic.Builder GetBuilder(PlasticDto plasticDto);
    }
}
