﻿using Thex.Dto.Reservation;
using Thex.Dto;
using Tnf.Dto;
namespace Thex.Application.Interfaces
{
    using Thex.Dto.ReservationCompanyClient;
    using Thex.Dto.ReservationGuest;
    using Thex.Dto.Reservation;
    using System.Threading.Tasks;

    public interface IReservationAppService : IScaffoldReservationAppService
    {
        ReservationDto GetReservation(DefaultLongRequestDto id);
        ReservationDto CreateReservation(ReservationDto reservation);
        Task<ReservationDto> UpdateReservationAsync(long id, ReservationDto reservation);
        void DeleteReservation(long id);

        ReservationDto GetWithReservationItemsAndRoomsAndRoomTypes(DefaultLongRequestDto id);
        IListDto<SearchReservationResultDto> GetAllByFilters(SearchReservationDto request, int propertyId);
        IListDto<SearchReservationResultDto> GetAllByFiltersV2(SearchReservationDto request, int propertyId);
        SearchReservationResultDto GetReservationDetail(SearchReservationDto request);
        IListDto<ReservationDto> GetReservationsByPropertyId(RequestAllDto request, int propertyId);
        ReservationDto GetByReservationItemId(DefaultLongRequestDto id);
        void CancelReservation(long reservationId, ReservationCancelDto dto);
        ReservationHeaderResultDto GetReservationHeaderByReservationItemId(long reservationItemId);
        void ReservationSaveNote(long reservationItemId, ReservationSaveNotesDto dto);
        void ReactivateReservation(long reservationId);
        IListDto<ReservationCompanyClientResultDto> GetReservationCompanyClient(GetAllReservationCompanyClientDto request, int propertyId);
        IListDto<ReservationGuestResultDto> GetReservationGuest(GetAllReservationGuestDto request, int propertyId);
        void ConfirmReservation(long reservationId);
        ReservationHeaderResultDto GetReservationHeader(long reservationId);
        NewReservationCodeDto GetNewReservationCode();
    }
}
