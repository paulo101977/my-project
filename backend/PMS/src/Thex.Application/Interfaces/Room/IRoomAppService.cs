﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.Dto;
using Thex.Dto.Base;
using Thex.Dto.Housekeeping;
using Thex.Dto.Room;
using Thex.Dto.Rooms;
using Tnf.Dto;

namespace Thex.Application.Interfaces
{
    public interface IRoomAppService : IScaffoldRoomAppService
    {
        IListDto<RoomDto> GetRoomsByPropertyId(GetAllRoomsDto request, int propertyId);
        IListDto<RoomDto> GetChildRoomsByParentRoomId(GetAllRoomsDto request, int parentRoomId);
        IListDto<RoomDto> GetParentRoomsByPropertyId(GetAllRoomsDto request, int propertyId);
        IListDto<RoomDto> GetRoomsWithoutChildrenByPropertyId(GetAllRoomsDto request, int propertyId);
        IListDto<RoomDto> GetRoomsWithoutParentAndChildrenByPropertyId(GetAllRoomsDto request, int propertyId);
        void ToggleActivation(int id);
        IListDto<RoomDto> GetAllRoomsByPropertyAndPeriod(GetAllRoomsByPropertyAndPeriodDto request, int propertyId);
        IListDto<RoomDto> GetAllAvailableByPeriodOfRoomType(PeriodDto request, int propertyId, int roomTypeId, int? reservationItemId, bool isCheckin);
        RoomDto CreateRoom(RoomDto roomDto);
        RoomDto UpdateRoom(int id, RoomDto roomDto);
        void DeleteRoom(int id);
        RoomDto GetRoom(DefaultIntRequestDto id);
        List<int> GetAllOccupedRoomsByPropertyAndPeriod(DateTime initialDate, DateTime endDate, int propertyId);
        IListDto<GetAllHousekeepingAlterStatusDto> GetAllRoomWithHousekeepingStatus(int propertyId);
        IListDto<RoomDto> GetAllStatusCheckinRooms(GetAllRoomsByPropertyAndPeriodDto request, int propertyId);
        Task<BudgetCurrentOrFutureDto> GetCurrentOrFutureBudgetByReservationItem(int propertyId, int roomTypeId, DateTime initialDate, DateTime finalDate, long reservationItemId);
        bool RoomIsAvailable(int propertyId, int roomId, long reservationItemId, DateTime systemDate);
        Task<RoomsAvailableDto> GetTotalAndAvailableRoomsByPropertyAsync();
    }
}
