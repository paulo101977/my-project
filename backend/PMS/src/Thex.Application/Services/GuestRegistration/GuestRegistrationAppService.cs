﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Thex.Application.Adapters;
using Thex.Application.Interfaces;
using Thex.Common;
using Thex.Common.Enumerations;
using Thex.Common.Extensions;
using Thex.Kernel;
using Thex.Domain.Entities;
using Thex.Domain.Interfaces.Repositories;
using Thex.Domain.Rules;
using Thex.Domain.Services.Interfaces;
using Thex.Dto;
using Thex.Dto.GuestRegistration;
using Thex.Infra.ReadInterfaces;
using Tnf.Domain.Services;
using Tnf.Dto;
using Tnf.Notifications;
using Tnf.Repositories.Uow;
using Tnf.Repositories;
using Thex.Domain.Interfaces.Repositories.ReadDapper;

namespace Thex.Application.Services
{
    public class GuestRegistrationAppService : ScaffoldGuestRegistrationAppService, IGuestRegistrationAppService
    {
        private readonly IGuestRegistrationReadRepository _guestRegistrationReadRepository;

        protected readonly IDomainService<Person> _personDomainService;
        protected readonly IPersonAdapter _personAdapter;
        protected readonly IContactInformationRepository _contactInformationRepository;
        protected readonly IContactInformationAppService _contactInformationAppService;
        protected readonly ICountrySubdivisionAppService _countrySubdivisionAppService;
        protected readonly ILocationAppService _locationAppService;
        protected readonly IDocumentAppService _documentAppService;
        protected readonly IDomainService<Guest> _guestDomainService;
        protected readonly IGuestReservationItemDomainService _guestReservationItemDomainService;
        protected readonly IPersonRepository _personRepository;
        protected readonly IPersonInformationAppService _personInformationAppService;
        protected readonly IPersonInformationRepository _personInformationRepository;
        protected readonly IPersonReadRepository _personReadRepository;
        protected readonly ICountrySubdivisionTranslationAppService _countrySubdivisionTranslationAppService;
        protected readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IApplicationUser _applicationUser;
        private readonly IPropertyParameterReadRepository _propertyParameterReadRepository;
        private readonly IDocumentAdapter _documentAdapter;
        private readonly IGuestRegistrationDocumentAppService _guestRegistrationDocumentAppService;
        private readonly IRepository<Guest> _guestRepository;
        private readonly IGuestReservationItemRepository _guestReservationItemRepository;
        private readonly IPropertyGuestPolicyReadRepository _propertyGuestPolicyReadRepository;
        protected readonly IGuestReservationItemDapperReadRepository _guestReservationItemDapperReadRepository;

        public GuestRegistrationAppService(
            IUnitOfWorkManager unitOfWorkManager,
            IGuestRegistrationAdapter guestRegistrationAdapter,
            IDomainService<GuestRegistration> guestRegistrationDomainService,
            IGuestRegistrationReadRepository guestRegistrationReadRepository,
            IDomainService<Person> personDomainService,
            IPersonAdapter personAdapter,
            IContactInformationRepository contactInformationRepository,
            IContactInformationAppService contactInformationAppService,
            ICountrySubdivisionAppService countrySubdivisionAppService,
            ILocationAppService locationAppService,
            IDocumentAppService documentAppService,
            IDomainService<Guest> guestDomainService,
            IGuestReservationItemDomainService guestReservationItemDomainService,
            IPersonRepository personRepository,
            IPersonInformationAppService personInformationAppService,
            IGuestRegistrationDocumentAppService guestRegistrationDocumentAppService,
            IPersonInformationRepository personInformationRepository,
            IPersonReadRepository personReadRepository,
            IApplicationUser applicationUser,
            IPropertyParameterReadRepository propertyParameterReadRepository,
            ICountrySubdivisionTranslationAppService countrySubdivisionTranslationAppService,
            IDocumentAdapter documentAdapter,
            INotificationHandler notificationHandler,
            IRepository<Guest> guestRepository,
            IGuestReservationItemRepository guestReservationItemRepository,
            IPropertyGuestPolicyReadRepository propertyGuestPolicyReadRepository,
            IGuestReservationItemDapperReadRepository guestReservationItemDapperReadRepository)
            : base(guestRegistrationAdapter, guestRegistrationDomainService, notificationHandler)
        {
            _unitOfWorkManager = unitOfWorkManager;
            _guestRegistrationReadRepository = guestRegistrationReadRepository;
            _personDomainService = personDomainService;
            _personAdapter = personAdapter;
            _contactInformationRepository = contactInformationRepository;
            _contactInformationAppService = contactInformationAppService;
            _countrySubdivisionAppService = countrySubdivisionAppService;
            _locationAppService = locationAppService;
            _documentAppService = documentAppService;
            _guestDomainService = guestDomainService;
            _guestReservationItemDomainService = guestReservationItemDomainService;
            _personRepository = personRepository;
            _personInformationAppService = personInformationAppService;
            _personInformationRepository = personInformationRepository;
            _personReadRepository = personReadRepository;
            _countrySubdivisionTranslationAppService = countrySubdivisionTranslationAppService;
            _propertyParameterReadRepository = propertyParameterReadRepository;
            _applicationUser = applicationUser;
            _documentAdapter = documentAdapter;
            _guestRegistrationDocumentAppService = guestRegistrationDocumentAppService;
            _guestRepository = guestRepository;
            _guestReservationItemRepository = guestReservationItemRepository;
            _propertyGuestPolicyReadRepository = propertyGuestPolicyReadRepository;
            _guestReservationItemDapperReadRepository = guestReservationItemDapperReadRepository;
        }

        public override async Task<GuestRegistrationDto> Update(Guid id, GuestRegistrationDto dto)
        {
            return await Update(id, dto, true);
        }

        public async Task<GuestRegistrationDto> Update(Guid id, GuestRegistrationDto dto, bool hasReservation)
        {
            ValidateDtoAndId(dto, id, nameof(dto), nameof(id));
            if (Notification.HasNotification())
                return GuestRegistrationDto.NullInstance;

            using (var uow = _unitOfWorkManager.Begin())
            {
                dto.Id = id;

                if (hasReservation)
                    dto.IsChild = _guestRegistrationReadRepository.IsChild(dto.GuestReservationItemId);
                else if (dto.Person != null)
                    dto.Person.Id = dto.PersonId;

                if (dto.IsChild || dto.IsLegallyIncompetent)
                {
                    ValidateChildAndIncapable(dto);
                }
                else
                {
                    ValidateDocument(dto);

                    if (Notification.HasNotification())
                        return GuestRegistrationDto.NullInstance;

                    ValidateBirthDate(dto);

                    if (Notification.HasNotification())
                        return GuestRegistrationDto.NullInstance;

                    ValidateContact(dto);

                    if (Notification.HasNotification())
                        return GuestRegistrationDto.NullInstance;
                }

                dto.SetFirstAndLastNameBasedOnFullName();
                SetPersonAndGuestIdBasedOnDocument(dto);

                if (Notification.HasNotification())
                    return GuestRegistrationDto.NullInstance;

                if (ValidateIdsGuestRegistration(dto, hasReservation))
                    NotifyIdIsMissing();

                SetLocationAndCreateLocation(dto.Location);

                if (Notification.HasNotification())
                    return GuestRegistrationDto.NullInstance;

                ValidateIfGuestRegistrationEqualsGuestReservationItem(dto);

                if (Notification.HasErrorNotification())
                    return GuestRegistrationDto.NullInstance;

                if (dto.Location != null)
                    _locationAppService.CreateOrUpdateGuestRegistrationLocation(dto.Location, id.ToString());

                if (Notification.HasNotification())
                    return GuestRegistrationDto.NullInstance;

                await CreateOrUpdatePersonGuestRegistration(dto).ConfigureAwait(false);

                if (Notification.HasNotification())
                    return GuestRegistrationDto.NullInstance;

                await UpdateGuestRegistration(dto).ConfigureAwait(false);

                if (Notification.HasNotification())
                    return GuestRegistrationDto.NullInstance;

                CreateOrUpdateGuestRegistrationDocument(dto, id);

                if (Notification.HasNotification())
                    return GuestRegistrationDto.NullInstance;

                if (hasReservation)
                {
                    _guestReservationItemDomainService.UpdatebasedOnGuestRegistration(dto, GetNationality(dto));

                    if (Notification.HasNotification())
                        return GuestRegistrationDto.NullInstance;
                }

                await uow.CompleteAsync();
            }

            return dto;
        }

        private CountrySubdivisionTranslationDto GetNationality(GuestRegistrationDto dto)
        {
            return _countrySubdivisionTranslationAppService.GetNationality(dto.Nationality ?? 0);
        }

        private void SetLocationAndCreateLocation(LocationDto locationDto)
        {
            if (locationDto != null)
            {
                locationDto.LocationCategoryId = (int)LocationCategoryEnum.Residential;

                var countrySubdivision = this._countrySubdivisionAppService.CreateCountrySubdivisionTranslateAndGetCityId(locationDto, locationDto.BrowserLanguage);
                locationDto.CityId = countrySubdivision.Item1;
                locationDto.StateId = countrySubdivision.Item2;
                locationDto.CountryId = countrySubdivision.Item3;

                locationDto.Id = 0;
            }
        }

        public async Task<GuestRegistrationDto> GetById(Guid Id)
        {
            var guestRegistrationDto = await Get(new DefaultGuidRequestDto(Id));

            if (guestRegistrationDto.BirthDate != null)
                guestRegistrationDto.IsChild = IsChild(guestRegistrationDto.BirthDate);

            return guestRegistrationDto;
        }

        public override async Task<GuestRegistrationDto> Create(GuestRegistrationDto dto)
        {
            return await Create(dto, true);
        }

        private bool IsChild(DateTime? birthDate)
        {
            var age = Convert.ToInt16(birthDate.GetAge());
            var maxKidAge = Convert.ToInt16(_propertyParameterReadRepository.GetPropertyParameterMaxChildrenAge(Convert.ToInt32(_applicationUser.PropertyId)).PropertyParameterValue);

            return age < maxKidAge;
        }

        public async Task<GuestRegistrationDto> Create(GuestRegistrationDto dto, bool hasReservation)
        {
            ValidateDto<GuestRegistrationDto>(dto, nameof(dto));

            if (Notification.HasNotification())
                return GuestRegistrationDto.NullInstance;

            using (var uow = _unitOfWorkManager.Begin())
            {
                if (hasReservation)
                    dto.IsChild = _guestRegistrationReadRepository.IsChild(dto.GuestReservationItemId);

                if (dto.IsChild || dto.IsLegallyIncompetent)
                {
                    ValidateChildAndIncapable(dto);

                    if (Notification.HasNotification())
                        return GuestRegistrationDto.NullInstance;
                }
                else
                {
                    ValidateDocument(dto);

                    if (Notification.HasNotification())
                        return GuestRegistrationDto.NullInstance;

                    ValidateBirthDate(dto);

                    if (Notification.HasNotification())
                        return GuestRegistrationDto.NullInstance;

                    ValidateContact(dto);

                    if (Notification.HasNotification())
                        return GuestRegistrationDto.NullInstance;
                }


                dto.SetFirstAndLastNameBasedOnFullName();

                SetPersonAndGuestIdBasedOnDocument(dto);

                if (dto.Location != null)
                    dto.Location.LocationCategoryId = (int)LocationCategoryEnum.Residential;

                ValidateIfGuestRegistrationEqualsGuestReservationItem(dto);

                if (Notification.HasErrorNotification())
                    return GuestRegistrationDto.NullInstance;

                await CreateOrUpdatePersonGuestRegistration(dto);

                if (Notification.HasNotification())
                    return GuestRegistrationDto.NullInstance;

                if (ValidateIdsGuestRegistration(dto, hasReservation))
                    NotifyIdIsMissing();

                CreateGuestRegistration(dto);

                if (Notification.HasErrorNotification())
                    return GuestRegistrationDto.NullInstance;

                CreateOrUpdateGuestRegistrationDocument(dto, dto.Id);

                if (Notification.HasErrorNotification())
                    return GuestRegistrationDto.NullInstance;

                if (hasReservation)
                {
                    _guestReservationItemDomainService.UpdatebasedOnGuestRegistration(dto, GetNationality(dto));

                    if (Notification.HasErrorNotification())
                        return GuestRegistrationDto.NullInstance;
                }

                await uow.CompleteAsync();
            }

            return dto;
        }

        private void SetPersonAndGuestIdBasedOnDocument(GuestRegistrationDto dto)
        {
            if (dto.Document != null && !string.IsNullOrEmpty(dto.Document.DocumentInformation) && dto.Document.DocumentTypeId != 0)
            {
                var personAndGuestId = _personReadRepository.GetPersonIdAndGuestIdByDocument(dto.Document.DocumentTypeId, dto.Document.DocumentInformation);

                if (personAndGuestId.HasValue)
                {
                    dto.PersonId = personAndGuestId.Value.Key == Guid.Empty ? dto.PersonId : personAndGuestId.Value.Key;
                    dto.GuestId = personAndGuestId.Value.Value == 0 ? dto.GuestId : personAndGuestId.Value.Value;
                }
            }
        }

        private async Task UpdateGuestRegistration(GuestRegistrationDto dto)
        {
            SetDocumentInformationAndDocumentTypeOnGuestRegistration(dto);

            var guestRegistrationBuilder = GuestRegistrationAdapter.Map(dto);
            await GuestRegistrationDomainService.UpdateAsync(guestRegistrationBuilder);
        }

        private async Task CreateOrUpdatePersonGuestRegistration(GuestRegistrationDto dto)
        {
            if (dto.PersonId == Guid.Empty)
            {
                var personType = GetPersonType(dto);
                await CreatePerson(dto, personType);
            }
            else
            {
                var personType = GetPersonType(dto);
                var person = GetPerson(dto, personType);

                if (Notification.HasErrorNotification())
                    return;

                _personRepository.UpdateWithDocumentLocationContactInformation(person);
            }
        }

        private static bool ValidateIdsGuestRegistration(GuestRegistrationDto dto, bool hasReservation = true)
        {
            return (hasReservation && dto.GuestReservationItemId == 0) || (dto.Document != null && dto.Document.DocumentTypeId == 0);
        }

        private PersonTypeEnum GetPersonType(GuestRegistrationDto dto)
        {
            return PersonTypeEnum.Contact; //dto.Document != null; // && _documentTypeReadRepository.IsLegal(dto.Document.DocumentTypeId) ? PersonTypeEnum.Legal : PersonTypeEnum.Natural;
        }

        private Person GetPerson(GuestRegistrationDto dto, PersonTypeEnum personType)
        {
            PersonDto personDto = new PersonDto
            {
                Id = dto.PersonId,
                FirstName = dto.FirstName,
                LastName = dto.LastName,
                PersonType = personType.ToString(),
                CountrySubdivisionId = dto.Nationality,
                DateOfBirth = dto.BirthDate,
                ReceiveOffers = (dto.Person != null ? dto.Person.ReceiveOffers : false),
                SharePersonData = (dto.Person != null ? dto.Person.SharePersonData : false),
                AllowContact = (dto.Person != null ? dto.Person.AllowContact : false)
            };

            SetLocationAndCreateLocation(dto.Location);

            if (Notification.HasNotification())
                return null;

            var contactInformationList = GetContactInformationListFromDto(
                personId: dto.PersonId,
                email: dto.Email,
                homePage: null,
                phoneNumber: dto.PhoneNumber,
                cellPhoneNumber: dto.MobilePhoneNumber);

            var personInformationList = GetPersonInformationListFromDto(
                personId: dto.PersonId,
                socialName: dto.SocialName,
                healthInsurance: dto.HealthInsurance);

            var personClientBuilder = new Person.Builder(Notification);
            if (!dto.IsChild)
            {
                var documentList = _documentAppService.getDocumentListFromDto(new List<DocumentDto> { dto.Document }, dto.PersonId);
                personClientBuilder = this._personAdapter.FullMap(personDto)
                    .WithContactInformationList(contactInformationList)
                    .WithPersonInformationList(personInformationList)
                    .WithDocumentList(documentList);
            }
            else
            {
                personClientBuilder = this._personAdapter.FullMap(personDto)
                    .WithContactInformationList(contactInformationList)
                    .WithPersonInformationList(personInformationList);
            }

            if (dto.Location != null)
                personClientBuilder = personClientBuilder.WithLocationList(new List<Location> { _locationAppService.GetLocationForUpdated(dto.Location, dto.PersonId.ToString()) });

            var personClient = personClientBuilder.Build();

            SaveGuest(dto, personDto.Id);

            if (Notification.HasErrorNotification())
                return null;

            return personClient;
        }

        private void CreateGuestRegistration(GuestRegistrationDto dto)
        {
            SetDocumentInformationAndDocumentTypeOnGuestRegistration(dto);

            var guestRegistrationBuilder = GuestRegistrationAdapter.Map(dto);

            var guestRegistrationId = GuestRegistrationDomainService.InsertAndSaveChanges(guestRegistrationBuilder).Id;

            if (Notification.HasErrorNotification())
                return;

            ////criar documento (Não será mais salvo na tabela de documentos e sim na GuestRegistration 
            //if (dto.Document != null && !string.IsNullOrEmpty(dto.Document.DocumentInformation) && dto.Document.DocumentTypeId != 0)
            //    _documentAppService.SaveDocumentList(new List<DocumentDto> { dto.Document }, guestRegistrationId);

            if (Notification.HasErrorNotification())
                return;

            //criar location para a GuestRegistration
            if (dto.Location != null)
                SaveLocation(dto.Location, guestRegistrationId);

            if (Notification.HasErrorNotification())
                return;

            dto.Id = guestRegistrationId;
        }

        private async Task CreatePerson(GuestRegistrationDto dto, PersonTypeEnum personType)
        {
            var personId = await this.CreatePersonAndGetId(dto, personType);

            if (Notification.HasErrorNotification())
                return;

            //criar documento 
            if (dto.Document != null && !string.IsNullOrEmpty(dto.Document.DocumentInformation) && dto.Document.DocumentTypeId != 0)
                _documentAppService.SaveDocumentList(new List<DocumentDto> { dto.Document }, personId);

            if (Notification.HasErrorNotification())
                return;

            //criar contatos
            var contactInformationList = GetContactInformationListFromDto(personId, dto.Email, null, dto.PhoneNumber, dto.MobilePhoneNumber);

            if (Notification.HasErrorNotification())
                return;

            SaveContactInformationList(contactInformationList, false);

            if (Notification.HasErrorNotification())
                return;

            //criar dados de person
            var personInformationList = GetPersonInformationListFromDto(
                personId: personId,
                socialName: dto.SocialName,
                healthInsurance: dto.HealthInsurance);

            SavePersonInformationList(personInformationList);

            if (Notification.HasErrorNotification())
                return;

            //criar location para a GuestRegistration
            if (dto.Location != null)
                SaveLocation(dto.Location, personId);

            if (Notification.HasErrorNotification())
                return;

            SaveGuest(dto, personId);

            if (Notification.HasErrorNotification())
                return;

        }

        private void SavePersonInformationList(IList<PersonInformation> personInformationList)
        {
            if (personInformationList.Any())
            {
                this._personInformationRepository.AddRange(personInformationList);
            }
        }

        private void SaveGuest(GuestRegistrationDto dto, Guid personId)
        {
            dto.PersonId = personId;

            if (dto.Id == Guid.Empty)
            {
                var guestBuilder = new Guest.Builder(Notification)
                    .WithPersonId(personId);

                var guest = _personRepository.CreateGuestIfNotExist(guestBuilder.Build());
                dto.GuestId = guest.Id;
            }
            else
            {
                var guestReservationItem = _guestReservationItemRepository.GetByGuestRegistrationId(dto.Id);
                var guestReservationItemBuilder = new GuestReservationItem.Builder(Notification, guestReservationItem)
                    .WithGuestDocument(dto.Document.DocumentInformation)
                    .WithGuestDocumentTypeId(dto.Document.DocumentTypeId);
                _guestReservationItemRepository.Update(guestReservationItemBuilder.Build());

                var guest = _guestRepository.Get(new DefaultLongRequestDto(guestReservationItem.GuestId.Value));
                var guestBuilder = new Guest.Builder(Notification, guest)
                    .WithPersonId(personId);
                _guestRepository.Update(guestBuilder.Build());

                dto.GuestId = guest.Id;
            }
        }

        private void SaveLocation(LocationDto locationDto, Guid personId)
        {
            var locationDtoList = new List<LocationDto>();

            SetLocationAndCreateLocation(locationDto);
            //int cityId = this._countrySubdivisionAppService.CreateCountrySubdivisionTranslateAndGetCityId(locationDto, locationDto.BrowserLanguage);
            //locationDto.CityId = cityId;
            //locationDto.LocationCategoryId = (int)LocationCategoryEnum.Commercial;

            if (!Notification.HasNotification())
            {
                locationDtoList.Add(locationDto);

                this._locationAppService.CreateLocationList(locationDtoList, personId);
            }
        }

        private void SaveContactInformationList(IList<ContactInformation> contactInformationList, bool update)
        {
            if (contactInformationList.Any())
            {
                this._contactInformationRepository.AddRangeAndSaveChanges(contactInformationList);
            }
        }

        private async Task<Guid> CreatePersonAndGetId(GuestRegistrationDto dto, PersonTypeEnum personType)
        {
            PersonDto personDto = new PersonDto()
            {
                FirstName = dto.FirstName,
                LastName = dto.LastName,
                PersonType = personType.ToString(),
                CountrySubdivisionId = dto.Nationality,
                DateOfBirth = dto.BirthDate,
                ReceiveOffers = (dto.Person != null ? dto.Person.ReceiveOffers : false),
                SharePersonData = (dto.Person != null ? dto.Person.SharePersonData : false),
                AllowContact = (dto.Person != null ? dto.Person.AllowContact : false),
            };

            var personBuilder = _personAdapter.FullMap(personDto);

            Guid personId = (await _personDomainService.InsertAndSaveChangesAsync(personBuilder)).Id;

            return personId;
        }

        private IList<PersonInformation> GetPersonInformationListFromDto(Guid personId, string socialName = "", string healthInsurance = "")
        {
            var personInformationList = new List<PersonInformation>();

            if (!String.IsNullOrEmpty(socialName))
                personInformationList.Add(getPersonInformationDto(PersonInformationTypeEnum.SocialName, socialName, personId).Build());

            if (!String.IsNullOrEmpty(healthInsurance))
                personInformationList.Add(getPersonInformationDto(PersonInformationTypeEnum.HealthInsurance, healthInsurance, personId).Build());

            return personInformationList;
        }

        private void SetDocumentInformationAndDocumentTypeOnGuestRegistration(GuestRegistrationDto dto)
        {
            if (dto.Document != null && dto.Document.DocumentTypeId != 0 && !string.IsNullOrWhiteSpace(dto.Document.DocumentInformation) && !dto.IsChild)
            {
                dto.DocumentTypeId = dto.Document.DocumentTypeId;
                dto.DocumentInformation = dto.Document.DocumentInformation;
            }
        }

        private PersonInformation.Builder getPersonInformationDto(PersonInformationTypeEnum personInformationTypeEnum, string personInformation, Guid personId)
        {
            var personInformationDto = new PersonInformationDto();
            personInformationDto.OwnerId = personId;
            personInformationDto.Information = personInformation;
            personInformationDto.PersonInformationTypeId = (int)personInformationTypeEnum;

            return _personInformationAppService.GetBuilder(personInformationDto);
        }

        private IList<ContactInformation> GetContactInformationListFromDto(Guid personId, string email = "", string homePage = "",
            string phoneNumber = "", string cellPhoneNumber = "")
        {
            List<ContactInformation> contactInformationList = new List<ContactInformation>();

            if (!String.IsNullOrEmpty(email))
            {
                contactInformationList.Add(getContactInformationDto(ContactInformationTypeEnum.Email, email, personId).Build());
            }

            if (!String.IsNullOrEmpty(homePage))
            {
                contactInformationList.Add(getContactInformationDto(ContactInformationTypeEnum.Website, homePage, personId).Build());
            }

            if (!String.IsNullOrEmpty(phoneNumber))
            {
                contactInformationList.Add(getContactInformationDto(ContactInformationTypeEnum.PhoneNumber, phoneNumber, personId).Build());
            }

            if (!String.IsNullOrEmpty(cellPhoneNumber))
            {
                contactInformationList.Add(getContactInformationDto(ContactInformationTypeEnum.CellPhoneNumber, cellPhoneNumber, personId).Build());
            }

            return contactInformationList;
        }

        private ContactInformation.Builder getContactInformationDto(ContactInformationTypeEnum contactInformationTypeEnum, string contactInformation, Guid personId)
        {
            ContactInformationDto contactInformationDto = new ContactInformationDto();
            contactInformationDto.OwnerId = personId;
            contactInformationDto.Information = contactInformation;
            contactInformationDto.ContactInformationTypeId = (int)contactInformationTypeEnum;

            return _contactInformationAppService.GetBuilder(contactInformationDto);
        }

        public GuestRegistrationResultDto GetGuestRegistrationByGuestReservationItem(long propertyId, long guestReservationItemId, string languageIsoCode)
        {
            if (propertyId <= 0)
            {
                NotifyIdIsMissing();
                return new GuestRegistrationResultDto();
            }

            if (guestReservationItemId <= 0)
            {
                NotifyIdIsMissing();
                return new GuestRegistrationResultDto();
            }

            if (String.IsNullOrEmpty(languageIsoCode))
            {
                NotifyIdIsMissing();
                return new GuestRegistrationResultDto();
            }

            var resultDto = _guestRegistrationReadRepository.GetGuestRegistrationByGuestReservationItemId(propertyId, guestReservationItemId, languageIsoCode);
            if (resultDto == null || resultDto.Id == Guid.Empty) resultDto = _guestRegistrationReadRepository.GetLastGuestRegistrationByGuestReservationItemId(propertyId, guestReservationItemId, languageIsoCode);
            if (resultDto == null || resultDto.Id == Guid.Empty) resultDto = _guestRegistrationReadRepository.GetGuestRegistrationFilledByGuestReservationAndPerson(propertyId, guestReservationItemId, languageIsoCode);

            var guestReservationItem = _guestReservationItemDomainService.GetReservationItemWithCountrySubdivision(guestReservationItemId, languageIsoCode);

            if (guestReservationItem != null)
            {
                resultDto.FirstName = resultDto.FirstName;
                resultDto.FullName = resultDto.FullName;
                resultDto.Email = resultDto.Email;
                resultDto.Document = guestReservationItem.GuestDocument ?? resultDto.Document;
                resultDto.DocumentTypeId = guestReservationItem.GuestDocumentTypeId ?? resultDto.DocumentTypeId;
                resultDto.SocialName = resultDto.SocialName;
                resultDto.Nationality = resultDto.Nationality;
                resultDto.GuestTypeId = resultDto.GuestTypeId;
                resultDto.GuestTypeName = resultDto.GuestTypeName;
            }


            if(guestReservationItem != null && !guestReservationItem.GuestId.HasValue)
            {
                var externalGuest = _guestReservationItemDapperReadRepository.GetExternalGuestByGuestReservationItemId(guestReservationItemId).ConfigureAwait(false).GetAwaiter().GetResult();

                if (externalGuest != null)
                {
                    resultDto.BirthDate = externalGuest.BirthDate;
                    resultDto.Nationality = externalGuest.NationalityId;
                    resultDto.Location = new Dto.LocationDto
                    {
                        Latitude = externalGuest.Latitude,
                        Longitude = externalGuest.Longitude,
                        StreetName = externalGuest.StreetName,
                        StreetNumber = externalGuest.StreetNumber,
                        AdditionalAddressDetails = externalGuest.AdditionalAddressDetails,
                        PostalCode = externalGuest.PostalCode,
                        CountryCode = externalGuest.CountryCode,
                        LocationCategoryId = externalGuest.LocationCategoryId ?? 0,
                        Neighborhood = externalGuest.Neighborhood,
                        CityId = externalGuest.CityId,
                        Subdivision = externalGuest.CityName,
                        Division = externalGuest.StateName,
                        Country = externalGuest.CountryName,
                        AddressTranslation = new Dto.CountrySubdivisionTranslation.AddressTranslationDto
                        {
                            CityId = externalGuest.CityId ?? 0,
                            CityName = externalGuest.CityName,
                            StateId = externalGuest.StateId ?? 0,
                            StateName = externalGuest.StateName,
                            CountryId = externalGuest.CountryId ?? 0,
                            CountryName = externalGuest.CountryName,
                            LanguageIsoCode = externalGuest.LanguageIsoCode,
                            TwoLetterIsoCode = externalGuest.StateTwoLetterIsoCode,
                            CountryTwoLetterIsoCode = externalGuest.CountryTwoLetterIsoCode
                        }
                    };
                }
                    
            }

            var propertyGuestPolicy = _propertyGuestPolicyReadRepository.GetAll();

            var guestPolicyList = propertyGuestPolicy.GroupBy(
                p => p.CountryCode,
                p => p.Description,
                (key, d) => new GuestPolicyList { CountryCode = key, DescriptionList = d.ToList() })
                .OrderByDescending(x => x.CountryCode == _applicationUser.PropertyCountryCode).ToList();

            var guestPolicyPrint = new GuestPolicyPrint
            {
                GuestPolicyList = guestPolicyList
            };

            resultDto.GuestPolicyPrint = guestPolicyPrint;

            return resultDto;
        }

        public GuestRegistrationResultDto GetGuestRegistrationBasedOnFilter(SearchGuestRegistrationsDto request)
        {
            int minimumCharacters = 2;

            if (request == null)
                NotifyNullParameter();
            else if (string.IsNullOrEmpty(request.SearchData))
                NotifyRequired("SearchData");
            else if (request.SearchData.Length < minimumCharacters)
                NotifyMinimumCharacters(minimumCharacters);

            if (Notification.HasNotification())
                return null;

            return _guestRegistrationReadRepository.GetGuestRegistrationBasedOnFilter(request);
        }

        public IListDto<GuestRegistrationResultDto> GetAllGuestRegistrationBasedOnFilter(SearchGuestRegistrationsDto request)
        {
            int minimumCharacters = 2;

            if (request == null)
                NotifyNullParameter();
            else if (string.IsNullOrEmpty(request.SearchData))
                NotifyRequired("SearchData");
            else if (request.SearchData.Length < minimumCharacters)
                NotifyMinimumCharacters(minimumCharacters);

            if (Notification.HasNotification())
                return new ListDto<GuestRegistrationResultDto>();

            return _guestRegistrationReadRepository.GetAllGuestRegistrationBasedOnFilter(request);
        }

        public IListDto<GuestRegistrationResultDto> GetAllGuestRegistrationByReservationItemId(long propertyId, long reservationItemId, string languageIsoCode)
        {
            if (propertyId <= 0)
            {
                NotifyIdIsMissing();
                return new ListDto<GuestRegistrationResultDto>();
            }

            if (reservationItemId <= 0)
            {
                NotifyIdIsMissing();
                return new ListDto<GuestRegistrationResultDto>();
            }

            if (String.IsNullOrEmpty(languageIsoCode))
            {
                NotifyIdIsMissing();
                return new ListDto<GuestRegistrationResultDto>();
            }

            return _guestRegistrationReadRepository.GetAllGuestRegistrationByReservationItemId(propertyId, reservationItemId, languageIsoCode);
        }

        public void ValidateChildAndIncapable(GuestRegistrationDto dto)
        {
            //CHECKIN> se o hospede for menor ou incapaz ele deve possuir um responsável e esse responsável tem q ter um  documento.
            if ((dto.IsLegallyIncompetent || !GenericDomainRules.IsAdult(dto.BirthDate.Value)) &&
                dto.ResponsibleGuestRegistrationId.HasValue &&
                !_guestRegistrationReadRepository.GuestRegistrationHasDocument(dto.ResponsibleGuestRegistrationId.Value))
            {
                Notification.Raise(Notification
                    .DefaultBuilder
                    .WithMessage(AppConsts.LocalizationSourceName, GuestRegistrationEnum.Error.GuestRegistrationLocationRequiredCpfOrRg)
                    .Build());
            }
        }

        public void ValidateIfGuestRegistrationEqualsGuestReservationItem(GuestRegistrationDto dto)
        {
            //checa se o tipo de convidado cadastrado na reserva é o mesmo na hora do checkin
            //Por exemplo: Foi cadastrado um adulto na reserva, verififcar se no preenchimento
            // da ficha do hósspede, está sendo cadastrado um adulto e não uma criança.
            var age = GetAge(dto.BirthDate.Value);
            var parameterProperty = _propertyParameterReadRepository.GetPropertyParameterMaxChildrenAge(int.Parse(_applicationUser.PropertyId));

            if (dto.IsChild)
            {
                if (age > int.Parse(parameterProperty.PropertyParameterValue))
                {
                    Notification.Raise(Notification
                  .DefaultBuilder
                  .WithMessage(AppConsts.LocalizationSourceName, GuestRegistrationEnum.Error.GuestRegistrationMustBeChild)
                  .Build());
                }
            }
            else
            {
                if (age < int.Parse(parameterProperty.PropertyParameterValue))
                {
                    Notification.Raise(Notification
                  .DefaultBuilder
                  .WithMessage(AppConsts.LocalizationSourceName, GuestRegistrationEnum.Error.GuestRegistrationMustBeAdult)
                  .Build());
                }
            }
        }

        private int GetAge(DateTime birthDate)
        {
            var birthDateZone = birthDate.ToZonedDateTimeLoggedUser(_applicationUser);
            var currentDateZone = DateTime.Now.ToZonedDateTimeLoggedUser(_applicationUser);
            var age = currentDateZone.Year - birthDateZone.Year;
            if (currentDateZone.Date < birthDateZone.AddYears(age).Date)
                age--;

            return age;
        }

        public void ValidateDocument(GuestRegistrationDto dto)
        {
            if (dto.Document == null || (dto.Document != null && string.IsNullOrEmpty(dto.Document.DocumentInformation)))
            {
                Notification.Raise(Notification
                    .DefaultBuilder
                    .WithMessage(AppConsts.LocalizationSourceName, GuestRegistrationEnum.Error.GuestRegistrationDocumentRequired)
                    .Build());
            }
            else
            {
                var documentBuilder = _documentAppService.GetBuilder(dto.Document);
                documentBuilder.Build();
            }
        }

        private void ValidateBirthDate(GuestRegistrationDto dto)
        {
            if (dto.BirthDate == null)
            {
                Notification.Raise(Notification
                    .DefaultBuilder
                    .WithMessage(AppConsts.LocalizationSourceName, GuestRegistration.EntityError.GuestRegistrationInvalidBirthDate)
                    .Build());
            }
        }

        private void ValidateContact(GuestRegistrationDto dto)
        {
            if (string.IsNullOrEmpty(dto.Email) && string.IsNullOrEmpty(dto.PhoneNumber)
                && string.IsNullOrEmpty(dto.MobilePhoneNumber))
            {
                Notification.Raise(Notification
                    .DefaultBuilder
                    .WithMessage(AppConsts.LocalizationSourceName, GuestRegistrationEnum.Error.GuestRegistrationContact)
                    .Build());
            }
        }

        public IListDto<GuestRegistrationResultDto> GetAllResponsibles(long propertyId, long reservationId)
        {
            return _guestRegistrationReadRepository.GetAllResponsibles(propertyId, reservationId);
        }

        public IListDto<GuestRegistrationResultDto> GetAllResponsiblesFromReservation(long reservationId)
        {
            return _guestRegistrationReadRepository.GetAllResponsibleFromReservation(reservationId);
        }

        private void CreateOrUpdateGuestRegistrationDocument(GuestRegistrationDto dto, Guid id)
        {
            if (dto.GuestRegistrationDocumentList.Any(grd => grd.DocumentTypeId == dto.DocumentTypeId || (dto.Document != null && dto.Document.DocumentTypeId == grd.DocumentTypeId)))
            {
                Notification.Raise(Notification.DefaultBuilder
                  .WithMessage(AppConsts.LocalizationSourceName, GuestRegistrationDocument.EntityError.GuestRegistrationDocumentEqualToMainDocumentGuestRegistration)
                  .WithDetailedMessage(AppConsts.LocalizationSourceName, GuestRegistrationDocument.EntityError.GuestRegistrationDocumentEqualToMainDocumentGuestRegistration)
                  .Build());

                return;
            }

            _guestRegistrationDocumentAppService.CreateOrUpdateGuestRegistrationDocument(dto.GuestRegistrationDocumentList, id);
        }

        public void UpdateNationalityOfGuestRegistration(GuestRegistration entity, int nationalityId)
             => GuestRegistrationDomainService.UpdateAsync(GuestRegistrationAdapter.Map(entity, nationalityId));

                 
    }
}