﻿using System;
using Thex.Application.Adapters;
using Thex.Domain.Entities;
using Thex.Application.Interfaces;
using Tnf.Domain.Services;
using Tnf.Notifications;

namespace Thex.Application.Services
{
    public class PropertyContactPersonAppService : ScaffoldPropertyContactPersonAppService, IPropertyContactPersonAppService
    {
        public PropertyContactPersonAppService(
            IPropertyContactPersonAdapter propertyContactPersonAdapter,
            IDomainService<PropertyContactPerson> propertyContactPersonDomainService,
            INotificationHandler notificationHandler)
            : base(propertyContactPersonAdapter, propertyContactPersonDomainService, notificationHandler)
        {
        }
    }
}
