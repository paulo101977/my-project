﻿using System;
using Thex.Application.Adapters;
using Thex.Domain.Entities;
using Thex.Application.Interfaces;
using Tnf.Domain.Services;
using Thex.Domain.Interfaces.Repositories;
using Thex.Dto.Rooms;
using Thex.Common;
using Thex.Dto;
using Thex.Infra.ReadInterfaces;
using System.Collections.Generic;
using System.Linq;
using Thex.Dto.HousekeepingStatusProperty;
using Tnf.Dto;
using Newtonsoft;
using Newtonsoft.Json;

using Thex.Common.Enumerations;
using Tnf.Notifications;
using Thex.Common.Extensions;
using Tnf.Repositories.Uow;
using Thex.Common.Interfaces.Observables;
using Microsoft.Extensions.DependencyInjection;
using Thex.Inventory.Domain.Services;
using Thex.Kernel;
using Thex.Common.Dto.Observable;

namespace Thex.Application.Services
{
    public class RoomBlockingAppService : ScaffoldRoomBlockingAppService, IRoomBlockingAppService
    {
        private readonly IRoomAppService _roomAppService;
        private readonly IRoomBlockingRepository _roomBlockingRepository;
        private readonly IRoomBlockingReadRepository _roomBlockingReadRepository;
        private readonly IReasonReadRepository _reasonReadRepository;
        private readonly IHousekeepingStatusPropertyAppService _housekeepingStatusPropertyAppService;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IApplicationUser _applicationUser;
        private readonly IRoomTypeRepository _roomTypeRepository;
        private readonly IPropertyParameterReadRepository _propertyParameterReadRepository;
        private readonly IRoomRepository _roomRepository;

        private IList<ICreateRoomBlockingObservable> _createRoomBlockingObservableList;
        private IList<IDeleteRoomBlockingObservable> _deleteRoomBlockingObservableList;

        public RoomBlockingAppService(
            IUnitOfWorkManager unitOfWorkManager,
            IRoomBlockingAdapter roomBlockingAdapter,
            IDomainService<RoomBlocking> roomBlockingDomainService,
            IRoomBlockingRepository roomBlockingRepository,
            IRoomBlockingReadRepository roomBlockingReadRepository,
            IRoomAppService roomAppService,
            IReasonReadRepository reasonReadRepository,
            IHousekeepingStatusPropertyAppService housekeepingStatusPropertyAppService,
            INotificationHandler notificationHandler,
            IRoomTypeRepository roomTypeRepository,
            IServiceProvider serviceProvider,
            IApplicationUser applicationUser,
            IPropertyParameterReadRepository propertyParameterReadRepository,
            IRoomRepository roomRepository)
            : base(roomBlockingAdapter, roomBlockingDomainService, notificationHandler)
        {
            _unitOfWorkManager = unitOfWorkManager;
            _roomAppService = roomAppService;
            _roomBlockingRepository = roomBlockingRepository;
            _roomBlockingReadRepository = roomBlockingReadRepository;
            _reasonReadRepository = reasonReadRepository;
            _housekeepingStatusPropertyAppService = housekeepingStatusPropertyAppService;
            _roomTypeRepository = roomTypeRepository;
            _applicationUser = applicationUser;
            _propertyParameterReadRepository = propertyParameterReadRepository;
            _roomRepository = roomRepository;

            SetObservables(serviceProvider);
        }

        private void SetObservables(IServiceProvider serviceProvider)
        {
            _createRoomBlockingObservableList = new List<ICreateRoomBlockingObservable>();
            var serviceForCreateRoomBlockingObservable = serviceProvider.GetServices<ICreateRoomBlockingObservable>().First(o => o.GetType() == typeof(RoomTypeInventoryDomainService));
            _createRoomBlockingObservableList.Add(serviceForCreateRoomBlockingObservable);

            _deleteRoomBlockingObservableList = new List<IDeleteRoomBlockingObservable>();
            var serviceForDeleteRoomBlockingObservable = serviceProvider.GetServices<IDeleteRoomBlockingObservable>().First(o => o.GetType() == typeof(RoomTypeInventoryDomainService));
            _deleteRoomBlockingObservableList.Add(serviceForDeleteRoomBlockingObservable);
        }

        public List<RoomWithReservationsDto> BlockingRoom(RoomBlockingDto roomBlocking)
        {
            using (var uow = _unitOfWorkManager.Begin())
            {
                var roomList = new List<int>();
                roomList.Add(roomBlocking.RoomId);
                var roomWithReservations = _roomBlockingReadRepository.GetReservationByRoomIds(roomList, roomBlocking.BlockingStartDate, roomBlocking.BlockingEndDate);

                if (roomWithReservations.Count() > 0)
                {
                    var json = JsonConvert.SerializeObject(roomWithReservations);

                    Notification.Raise(Notification.DefaultBuilder
                      .WithMessage(AppConsts.LocalizationSourceName, RoomBlocking.EntityError.Commom)
                      .WithMessageFormat(json)
                      .WithForbiddenStatus()
                     .Build());
                }

                if (Notification.HasNotification()) return null;

                var roomsBlockingForPeriod = _roomBlockingReadRepository.GetAllBlockedInPeriod(roomBlocking.BlockingStartDate, roomBlocking.BlockingEndDate, roomList).Select(x => x.RoomId);

                if (roomsBlockingForPeriod.Contains(roomBlocking.RoomId))
                {
                    Notification.Raise(Notification.DefaultBuilder
                          .WithMessage(AppConsts.LocalizationSourceName, RoomBlocking.EntityError.RoomBusyForPeriod)
                          .WithDetailedMessage(AppConsts.LocalizationSourceName, RoomBlocking.EntityError.RoomBusyForPeriod)
                          .Build());
                }
                if (Notification.HasNotification()) return null;
                var builder = base.RoomBlockingAdapter.Map(roomBlocking);
                _roomBlockingRepository.BlockingRoom(builder.Build());

                var housekeeppingStatusId = _housekeepingStatusPropertyAppService.GetHousekeepingStatusIdDirty();
                _housekeepingStatusPropertyAppService.UpdateManyRoomForHousekeeppingStatus(new List<int> { roomBlocking.RoomId }, housekeeppingStatusId);

                RiseActionsAfterCreateRoomBlocking(new List<RoomBlockingDto> { roomBlocking });

                if (Notification.HasNotification()) return null;

                uow.Complete();
            }
            
            return null;
        }

        public void UnlockRoomForAvailability(Guid id)
        {
            var roomBlocking = RoomBlockingDomainService.Get(new DefaultGuidRequestDto { Id = id });

            if (roomBlocking != null)
            {
                using (var uow = _unitOfWorkManager.Begin())
                {
                    var roomBlockingList = new List<RoomBlocking> { roomBlocking };
                    var roomIdList = new List<int> { roomBlockingList.Select(r => r.RoomId).FirstOrDefault() };

                    RoomBlockingDomainService.Delete(e => e.Id == id);

                    RiseActionsAfterDeleteRoomBlocking(roomIdList, roomBlockingList);

                    uow.Complete();
                }
            }
        }

        public void BlockingRoomList(List<RoomBlockingDto> roomBlockingList)
        {
            using (var uow = _unitOfWorkManager.Begin())
            {
                if (roomBlockingList.Count > 0 && roomBlockingList != null)
                {
                    var roomWithReservations = _roomBlockingReadRepository.GetReservationByRoomIds(roomBlockingList.Select(x => x.RoomId).ToList(), roomBlockingList.FirstOrDefault().BlockingStartDate, roomBlockingList.FirstOrDefault().BlockingEndDate);

                    if (roomWithReservations != null && roomWithReservations.Count > 0)
                    {
                        Notification.Raise(Notification.DefaultBuilder
                        .WithMessage(AppConsts.LocalizationSourceName, RoomBlocking.EntityError.RoomBlockingExistsReservationForPeriod)
                        .WithDetailedMessage(AppConsts.LocalizationSourceName, RoomBlocking.EntityError.RoomBlockingExistsReservationForPeriod)
                        .Build());
                    }
                }

                if (Notification.HasNotification()) return;

                var roomsBlockingForPeriod = _roomBlockingReadRepository.GetAllBlockedInPeriod(roomBlockingList.FirstOrDefault().BlockingStartDate, roomBlockingList.FirstOrDefault().BlockingEndDate, roomBlockingList.Select(x => x.RoomId).ToList()).Select(x => x.RoomId);
                foreach (var roomBlocking in roomBlockingList)
                {
                    if (roomsBlockingForPeriod.Contains(roomBlocking.RoomId))
                    {
                        Notification.Raise(Notification.DefaultBuilder
                              .WithMessage(AppConsts.LocalizationSourceName, RoomBlocking.EntityError.RoomBlockingIsBlockedForPeriod)
                              .WithDetailedMessage(AppConsts.LocalizationSourceName, RoomBlocking.EntityError.RoomBlockingIsBlockedForPeriod)
                              .Build());
                        break;
                    }
                    var builder = base.RoomBlockingAdapter.Map(roomBlocking);
                    _roomBlockingRepository.BlockingRoom(builder.Build());

                    var housekeeppingStatusId = _housekeepingStatusPropertyAppService.GetHousekeepingStatusIdDirty();

                    _housekeepingStatusPropertyAppService.UpdateManyRoomForHousekeeppingStatus(new List<int> { roomBlocking.RoomId }, housekeeppingStatusId);
                }
                if (Notification.HasNotification()) return;

                RiseActionsAfterCreateRoomBlocking(roomBlockingList);

                uow.Complete();
            }
        }

        public void UnlockRoomList(List<int> roomIds, int propertyId)
        {
            var systemDate = _propertyParameterReadRepository.GetSystemDate(propertyId).GetValueOrDefault();

            if (systemDate == null)
            {
                Notification.Raise(Notification.DefaultBuilder
                .WithMessage(AppConsts.LocalizationSourceName, PropertyAuditProcess.EntityError.PropertyAuditProcessInvalidPropertySystemDate)
                .WithDetailedMessage(AppConsts.LocalizationSourceName, PropertyAuditProcess.EntityError.PropertyAuditProcessInvalidPropertySystemDate)
                .Build());
            }

            if (Notification.HasNotification()) return;

            using (var uow = _unitOfWorkManager.Begin())
            {
                var getAllRoomBlockedList = _roomBlockingReadRepository.GetAllRoomBlockingForUnlock(systemDate, systemDate, propertyId);//.Select(x => new { x.RoomId, x.Id, x.BlockingStartDate, x.BlockingEndDate });

                if (getAllRoomBlockedList != null && getAllRoomBlockedList.Count() > 0)
                {
                    var roomsForBlock = getAllRoomBlockedList.Where(x => roomIds.Contains(x.RoomId));
                    if (roomsForBlock != null && roomsForBlock.Count() > 0)
                    {
                        foreach (var room in roomsForBlock)
                        {
                            _roomBlockingRepository.UnlockingRoom(room.Id);
                        }
                    }

                    var housekeeppingStatusId = _housekeepingStatusPropertyAppService.GetHousekeepingStatusIdDirty();

                    _housekeepingStatusPropertyAppService.UpdateManyRoomForHousekeeppingStatus(roomIds, housekeeppingStatusId);

                    RiseActionsAfterDeleteRoomBlocking(roomIds, getAllRoomBlockedList);
                }

                uow.Complete();
            }
        }

        public IEnumerable<int> UnlockAndGetRoomListByBlockingEndDate(int propertyId, DateTime blockingEndDate)
        {
            var roomBlockingList = _roomBlockingReadRepository.GetAllRoomBlockingForUnlockByBlockingEndDate(blockingEndDate, propertyId);

            if (roomBlockingList != null && roomBlockingList.Count() > 0)
            {
                _roomBlockingRepository.UnlockingRoom(roomBlockingList);

                RiseActionsAfterDeleteRoomBlocking(roomBlockingList.Select(r => r.RoomId), roomBlockingList);
            }
            return roomBlockingList.Select(r => r.RoomId);
        }

        public IEnumerable<int> GetRoomBlockingListByBlockingStartDate(int propertyId, DateTime blockingStartDate)
        {
            return _roomBlockingReadRepository.GetAllRoomBlockingForBlockingByBlockingEndDate(blockingStartDate, propertyId).Select(r => r.RoomId);
        }

        private void RiseActionsAfterCreateRoomBlocking(IEnumerable<RoomBlockingDto> roomBlockingList)
        {
            var roomIdList = roomBlockingList.Select(r => r.RoomId);
            var roomList = _roomRepository.GetAllByIdList(roomIdList);

            foreach (var observable in _createRoomBlockingObservableList)
            {
                var dtoObservable = new CreateRoomBlockingObservableDto
                {
                    InitialDate = roomBlockingList.FirstOrDefault().BlockingStartDate,
                    EndDate = roomBlockingList.FirstOrDefault().BlockingEndDate,
                };

                var roomTypeIdList = roomList.Select(r => r.RoomTypeId).Distinct();
                foreach (var roomTypeId in roomTypeIdList)
                {
                    var roomQuantityByRoomType = roomList.Where(r => r.RoomTypeId == roomTypeId).Count();
                    dtoObservable.RoomTypeIdWithRoomQuantityDictionary.Add(new KeyValuePair<int, int>(roomTypeId, roomQuantityByRoomType));
                }

                observable.ExecuteActionAfterCreateRoomBlocking(dtoObservable);
            }
        }

        private void RiseActionsAfterDeleteRoomBlocking(IEnumerable<int> roomIdList, IEnumerable<RoomBlocking> roomBlockingList)
        {
            var roomList = _roomRepository.GetAllByIdList(roomIdList);

            foreach (var observable in _deleteRoomBlockingObservableList)
            {
                var dtoObservable = new DeleteRoomBlockingObservableDto
                {
                    InitialDate = roomBlockingList.FirstOrDefault().BlockingStartDate,
                    EndDate = roomBlockingList.FirstOrDefault().BlockingEndDate,
                };

                var roomTypeIdList = roomList.Select(r => r.RoomTypeId).Distinct();
                foreach (var roomTypeId in roomTypeIdList)
                {
                    var roomQuantityByRoomType = roomList.Where(r => r.RoomTypeId == roomTypeId).Count();
                    dtoObservable.RoomTypeIdWithRoomQuantityDictionary.Add(new KeyValuePair<int, int>(roomTypeId, roomQuantityByRoomType));
                }

                observable.ExecuteActionAfterDeleteRoomBlocking(dtoObservable);
            }
        }
    }
}
