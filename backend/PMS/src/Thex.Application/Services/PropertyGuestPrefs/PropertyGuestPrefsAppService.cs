﻿using System;
using Thex.Application.Adapters;
using Thex.Domain.Entities;
using Thex.Application.Interfaces;
using Tnf.Domain.Services;
using Tnf.Notifications;

namespace Thex.Application.Services
{
    public class PropertyGuestPrefsAppService : ScaffoldPropertyGuestPrefsAppService, IPropertyGuestPrefsAppService
    {
        public PropertyGuestPrefsAppService(
            IPropertyGuestPrefsAdapter propertyGuestPrefsAdapter,
            IDomainService<PropertyGuestPrefs> propertyGuestPrefsDomainService,
            INotificationHandler notificationHandler)
            : base(propertyGuestPrefsAdapter, propertyGuestPrefsDomainService, notificationHandler)
        {
        }
    }
}
