﻿using System;
using Thex.Application.Adapters;
using Thex.Domain.Entities;
using Thex.Application.Interfaces;
using Tnf.Domain.Services;

using System.Collections.Generic;

using Thex.Dto;
using Tnf.Dto;
using Tnf.Notifications;

namespace Thex.Application.Services
{
    public class EmployeeAppService : ScaffoldEmployeeAppService, IEmployeeAppService
    {
        public EmployeeAppService(
            IEmployeeAdapter employeeAdapter,
            IDomainService<Employee> employeeDomainService,
            INotificationHandler notificationHandler)
            : base(employeeAdapter, employeeDomainService, notificationHandler)
        {
        }

        public EmployeeDto Get(DefaultIntRequestDto id)
        {
            if (id.Id < 0)
            {
                NotifyIdIsMissing();
                return EmployeeDto.NullInstance;
            }

            Employee employee = EmployeeDomainService.Get(id);
            return employee.MapTo<EmployeeDto>();
        }


        public ICollection<Employee> ToDomain(ICollection<EmployeeDto> employees)
        {
            var employeeList = new HashSet<Employee>();
            if (employees != null)
            {
                foreach (var employeeDto in employees)
                {
                    var employeeBuilder = this.GetBuilder(employeeDto);
                    var employee = employeeBuilder.Build();
                    employeeList.Add(employee);
                }
            }
            return employeeList;
        }

        public Employee.Builder GetBuilder(EmployeeDto employeeDto)
        {
            var builder = new Employee.Builder(Notification);

            builder
                .WithId(employeeDto.Id)
                .WithPersonId(employeeDto.PersonId)
                .WithOccupationId(employeeDto.OccupationId);

            return builder;
        }

        public new EmployeeDto Create(EmployeeDto employee)
        {
            if (employee == null)
            {
                NotifyNullParameter();
                return EmployeeDto.NullInstance;
            }

            employee.Id = EmployeeDomainService.InsertAndSaveChanges(GetBuilder(employee)).Id;

            return employee;
        }

        public EmployeeDto Update(EmployeeDto employee)
        {
            if (employee == null)
            {
                NotifyNullParameter();
                return EmployeeDto.NullInstance;
            }

            if (employee.Id <= 0)
                NotifyIdIsMissing();

            if (Notification.HasNotification())
                return EmployeeDto.NullInstance;

            EmployeeDomainService.Update(GetBuilder(employee));

            return employee;
        }

        public new void Delete(int id)
        {
            if (id < 0)
            {
                NotifyIdIsMissing();
                return;
            }

            var employee = EmployeeDomainService.Get(new DefaultIntRequestDto(id));
            if (employee == null)
            {
                NotifyWhenEntityNotExist("Employee");
                return;
            }

            EmployeeDomainService.Delete(employee);
        }
    }
}
