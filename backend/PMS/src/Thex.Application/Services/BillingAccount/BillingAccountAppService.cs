﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Thex.Application.Adapters;
using Thex.Application.Interfaces;
using Thex.Common;
using Thex.Common.Enumerations;
using Thex.Domain.Entities;
using Thex.Domain.Interfaces.Repositories;
using Thex.Domain.Services.Interfaces;
using Thex.Dto;
using Thex.Dto.BillingAccount;
using Thex.Dto.Person;
using Thex.Infra.ReadInterfaces;
using Thex.Kernel;
using Tnf.Dto;
using Tnf.Localization;
using Tnf.Notifications;
using Tnf.Repositories.Uow;

namespace Thex.Application.Services
{
    public class BillingAccountAppService : ScaffoldBillingAccountAppService, IBillingAccountAppService
    {
        private readonly IBillingAccountDomainService _billingAccountDomainService;
        private readonly ICompanyClientAppService _companyClientAppService;

        private readonly IBillingAccountReadRepository _billingAccountReadRepository;
        private readonly IBillingAccountItemReadRepository _billingAccountItemReadRepository;
        private readonly IReservationReadRepository _reservationReadRepository;
        private readonly IReasonReadRepository _reasonReadRepository;
        private readonly IBillingInvoiceReadRepository _billingInvoiceReadRepository;
        private readonly ILocalizationManager _localizationManager;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IBillingAccountRepository _billingAccountRepository;
        private readonly IApplicationUser _applicationUser;
        private readonly IGuestReservationItemRepository _guestReservationItemRepository;
        private readonly IBillingAccountItemAdapter _billingAccountItemAdapter;
        private readonly IPersonAppService _personAppService;
        private readonly IBillingItemReadRepository _billingItemReadRepository;
        private readonly IPropertyParameterReadRepository _PropertyParameterReadRepository;
        private readonly ICurrencyReadRepository _currencyReadRepository;

        public BillingAccountAppService(
            IUnitOfWorkManager unitOfWorkManager,
            IBillingAccountAdapter BillingAccountAdapter,
            IBillingAccountDomainService billingAccountDomainService,
            ICompanyClientAppService companyClientAppService,
            IBillingAccountReadRepository billingAccountReadRepository,
            IBillingAccountItemReadRepository billingAccountItemReadRepository,
            IReservationReadRepository reservationReadRepository,
            IReasonReadRepository reasonReadRepository,
            IBillingInvoiceReadRepository billingInvoiceReadRepository,
            ILocalizationManager localizationManager,
            INotificationHandler notificationHandler,
            IBillingAccountRepository billingAccountRepository,
            IApplicationUser applicationUser,
            IGuestReservationItemRepository guestReservationItemRepository,
            IBillingAccountItemAdapter billingAccountItemAdapter,
            IPersonAppService personAppService,
            IBillingItemReadRepository billingItemReadRepository,
            IPropertyParameterReadRepository propertyParameterReadRepository,
            ICurrencyReadRepository currencyReadRepository
            )
            : base(BillingAccountAdapter, billingAccountDomainService, notificationHandler)
        {
            _unitOfWorkManager = unitOfWorkManager;
            _billingAccountDomainService = billingAccountDomainService;
            _companyClientAppService = companyClientAppService;
            _billingAccountReadRepository = billingAccountReadRepository;
            _billingAccountItemReadRepository = billingAccountItemReadRepository;
            _reservationReadRepository = reservationReadRepository;
            _reasonReadRepository = reasonReadRepository;
            _billingInvoiceReadRepository = billingInvoiceReadRepository;
            _localizationManager = localizationManager;
            _billingAccountRepository = billingAccountRepository;
            _applicationUser = applicationUser;
            _guestReservationItemRepository = guestReservationItemRepository;
            _billingAccountItemAdapter = billingAccountItemAdapter;
            _personAppService = personAppService;
            _billingItemReadRepository = billingItemReadRepository;
            _PropertyParameterReadRepository = propertyParameterReadRepository;
            _currencyReadRepository = currencyReadRepository;
        }

        public override async Task<BillingAccountDto> Create(BillingAccountDto dto)
        {
            using (var uow = _unitOfWorkManager.Begin())
            {
                if (!dto.CreateMainAccountOfCompanyInReservationItem.HasValue ||
                    (dto.CreateMainAccountOfCompanyInReservationItem.HasValue && !dto.CreateMainAccountOfCompanyInReservationItem.GetValueOrDefault()))
                {
                    CheckIfExistIsMainBillingAccount(dto.ReservationId.Value, dto.ReservationItemId.GetValueOrDefault(), dto.GuestReservationItemId, dto.CompanyClientId, dto.BillingAccountTypeId);
                }

                if (Notification.HasNotification())
                    return BillingAccountDto.NullInstance;

                ValidateDto<BillingAccountDto>(dto, nameof(dto));

                var reservation = _reservationReadRepository.GetReservationForCheckin(dto.ReservationId.Value);

                if (string.IsNullOrEmpty(dto.BillingAccountName) && dto.GuestReservationItemId != null)
                {
                    var guestName = _guestReservationItemRepository.GetById(dto.GuestReservationItemId.GetValueOrDefault()).GuestName;
                    dto.BillingAccountName = !string.IsNullOrWhiteSpace(guestName) ? guestName :
                         _localizationManager.GetString(AppConsts.LocalizationSourceName, (BillingAccount.EntityError.BillingAccountPreAccount).ToString());
                }
                else if (string.IsNullOrEmpty(dto.BillingAccountName) && reservation.CompanyClientId != null)
                    dto.BillingAccountName = reservation.CompanyClient.TradeName;

                dto.BusinessSourceId = reservation.BusinessSourceId.Value;
                dto.MarketSegmentId = reservation.MarketSegmentId.Value;
                dto.IsMainAccount = true;
                dto.Blocked = false;
                dto.StartDate = DateTime.UtcNow.ToZonedDateTimeLoggedUser();

                if (Notification.HasNotification())
                    return BillingAccountDto.NullInstance;

                var billingAccountBuilder = BillingAccountAdapter.MapWithGroupKey(dto);

                dto.Id = (await BillingAccountDomainService.InsertAndSaveChangesAsync(billingAccountBuilder)).Id;

                if (Notification.HasNotification())
                    return BillingAccountDto.NullInstance;

                await uow.CompleteAsync();
            }

            return dto;
        }

        public IListDto<SearchBillingAccountResultDto> GetAllByFilters(SearchBillingAccountDto request, int propertyId)
        {
            return _billingAccountReadRepository.GetAllByFiltersV2(request, propertyId);
        }

        public BillingAccountDto CreateSparceAccount(BillingAccountDto dto)
        {
            using (var uow = _unitOfWorkManager.Begin())
            {
                ValidateDto<BillingAccountDto>(dto, nameof(dto));

                if (Notification.HasNotification())
                    return BillingAccountDto.NullInstance;

                CompanyClientDto companyClient = null;

                if (!dto.CompanyClientId.HasValue)
                    companyClient = _companyClientAppService.CreateBySparseBillingAccount(dto.CompanyClient);
                else
                    companyClient = new CompanyClientDto
                    {
                        Id = dto.CompanyClientId.ToString(),
                        TradeName = dto.BillingAccountName
                    };

                if (Notification.HasNotification())
                    return BillingAccountDto.NullInstance;

                dto.CompanyClientId = Guid.Parse(companyClient.Id);
                dto.BillingAccountName = companyClient.TradeName;
                dto.IsMainAccount = true;
                dto.Blocked = false;
                dto.StartDate = DateTime.UtcNow.ToZonedDateTimeLoggedUser();

                var billingAccountBuilder = BillingAccountAdapter.MapWithGroupKey(dto);

                dto.Id = BillingAccountDomainService.InsertAndSaveChanges(billingAccountBuilder).Id;

                if (dto.CompanyClient != null && dto.CompanyClient.LocationList != null)
                    _companyClientAppService.SaveLocationList(dto.CompanyClient, dto.Id);

                uow.Complete();
            }

            return dto;
        }

        public BillingAccountHeaderDto BillingAccountHeader(Guid billingAccountId)
        {
            return _billingAccountReadRepository.BillingAccountHeaderV2(billingAccountId);
        }

        public IListDto<BillingAccountSidebarDto> BillingAccountSidebar(Guid billingAccountId)
        {
            return _billingAccountReadRepository.BillingAccountSidebarV2(billingAccountId);
        }

        #region Billing Account Entries

        public BillingAccountWithAccountsDto GetUniqueBillingAccountWithEntries(DefaultGuidRequestDto id, Guid invoiceId, bool withInvoices = true)
        {
            var billingAccountWithEntries = _billingAccountReadRepository.GetById(id);

            if (billingAccountWithEntries == null)
                return null;

            var groupKeys = _billingAccountReadRepository.GetBillingAccountIdsByGroupId(billingAccountWithEntries.GroupKey);

            var billingAccounts = _billingAccountReadRepository.GetAllByBillingAccountByIds(new List<Guid> { billingAccountWithEntries.Id });

            AccountEntriesViewProcess(billingAccountWithEntries, billingAccounts, invoiceId);

            if (withInvoices && billingAccounts != null && billingAccounts.Count() > 0)
            {
                var invoices = _billingInvoiceReadRepository.GetAllByBillingAccountIdList(billingAccountWithEntries.BillingAccounts.Select(e => e.BillingAccountId).ToList());

                foreach (var accounts in billingAccountWithEntries.BillingAccounts)
                {
                    accounts.Invoices = invoices.Where(e => e.BillingAccountId == accounts.BillingAccountId).ToList();
                }
            }

            return billingAccountWithEntries;
        }

        public List<InvoiceDto> GetInvoicesAccount(DefaultGuidRequestDto id)
        {
            return _billingInvoiceReadRepository.GetAllByBillingAccountId(id.Id);
        }

        public BillingAccountWithAccountsDto GetWithAccountEntries(DefaultGuidRequestDto id, bool withInvoices = true)
        {
            var billingAccountWithEntries = _billingAccountReadRepository.GetById(id);

            if (billingAccountWithEntries == null)
                return null;

            var groupKeys = _billingAccountReadRepository.GetBillingAccountIdsByGroupId(billingAccountWithEntries.GroupKey);

            var billingAccounts = _billingAccountReadRepository.GetAllByBillingAccountGroupKey(groupKeys);

            AccountEntriesViewProcess(billingAccountWithEntries, billingAccounts);

            if (withInvoices && billingAccounts != null && billingAccounts.Count() > 0)
            {
                var invoices = _billingInvoiceReadRepository.GetAllByBillingAccountIdList(billingAccountWithEntries.BillingAccounts.Select(e => e.BillingAccountId).ToList());

                foreach (var accounts in billingAccountWithEntries.BillingAccounts)
                {
                    accounts.Invoices = invoices.Where(e => e.BillingAccountId == accounts.BillingAccountId).ToList();
                }
            }

            ValidateAndInsertPropertyCurrencySymbol(billingAccountWithEntries);            

            return billingAccountWithEntries;
        }

        private void ValidateAndInsertPropertyCurrencySymbol(BillingAccountWithAccountsDto billingAccountWithEntries)
        {
            var propertyCurrencyId = GetDefaultCurrencyIdByProperty(int.Parse(_applicationUser.PropertyId));

            var currency = new CurrencyDto();

            if (propertyCurrencyId != Guid.Empty)
                currency = _currencyReadRepository.GetNameCurrencyById(propertyCurrencyId);

            if (billingAccountWithEntries?.BillingAccounts != null && currency != null && !string.IsNullOrWhiteSpace(currency.Symbol))
            {
                billingAccountWithEntries.CurrencySymbol = currency.Symbol;
                foreach (var billingAccount in billingAccountWithEntries.BillingAccounts)
                {
                    if (billingAccount.BillingAccountItems != null && billingAccount.BillingAccountItems.Count() > 0)
                    {
                        foreach (var BillingAccountItem in billingAccount.BillingAccountItems)
                        {
                            BillingAccountItem.CurrencySymbol = currency.Symbol;
                            if (BillingAccountItem.BillingAccountChildrenItems != null && BillingAccountItem.BillingAccountChildrenItems.Count() > 0)
                            {
                                foreach (var BillingAccountChild in BillingAccountItem.BillingAccountChildrenItems)
                                    BillingAccountChild.CurrencySymbol = currency.Symbol;
                            }
                        }
                    }
                }
            }
        }

        public IListDto<BillingAccountWithAccountsDto> GetAllWithAccountEntriesList(List<Guid> ids)
        {
            var result = new List<BillingAccountWithAccountsDto>();
            var allBillingAccounts = _billingAccountReadRepository.GetAllByBillingAccountByIds(ids);
            var billingAccountWithEntriesList = _billingAccountReadRepository.GetAllByIds(ids);

            if (allBillingAccounts == null || allBillingAccounts.Count() == 0)
                return null;

            var newBillingAccountWithEntriesList = new List<BillingAccountWithAccountsDto>();

            foreach (var newItem in billingAccountWithEntriesList)
            {
                if (!newBillingAccountWithEntriesList.Any(e => e.GroupKey == newItem.GroupKey))
                    newBillingAccountWithEntriesList.Add(newItem);
            }

            foreach (var billingAccountWithEntries in newBillingAccountWithEntriesList)
            {
                var billingAccounts = allBillingAccounts.Where(e => e.GroupKey == billingAccountWithEntries.GroupKey).ToList();

                AccountEntriesViewProcess(billingAccountWithEntries, billingAccounts);

                result.Add(billingAccountWithEntries);
            }

            return new ListDto<BillingAccountWithAccountsDto>
            {
                HasNext = false,
                Items = result,
                //Total = result.Count
            };
        }

        public BillingAccountWithCompleteAccountDto GetAllWithAccountEntriesComplete(Guid id)
        {
            var billingAccountWithEntriesList = _billingAccountReadRepository.GetBillingAccountsIdsForEntries(id);

            if (billingAccountWithEntriesList.Count() == 0)
                return null;

            var result = new List<BillingAccountWithAccountsDto>();
            var allBillingAccounts = _billingAccountReadRepository.GetAllByBillingAccountByIds(billingAccountWithEntriesList.Select(e => e.Id).ToList());

            if (allBillingAccounts.Count() == 0)
                return null;

            var newBillingAccountWithEntriesList = new List<BillingAccountWithAccountsDto>();

            foreach (var newItem in billingAccountWithEntriesList)
            {
                if (!newBillingAccountWithEntriesList.Any(e => e.GroupKey == newItem.GroupKey))
                    newBillingAccountWithEntriesList.Add(newItem);
            }

            foreach (var billingAccountWithEntries in newBillingAccountWithEntriesList)
            {
                var billingAccounts = allBillingAccounts.Where(e => e.GroupKey == billingAccountWithEntries.GroupKey).ToList();

                AccountEntriesViewProcess(billingAccountWithEntries, billingAccounts);

                billingAccountWithEntries.AccountTypeName = _localizationManager.GetString(AppConsts.LocalizationSourceName, ((BillingAccountTypeEnum)billingAccountWithEntries.AccountTypeId).ToString());
                billingAccountWithEntries.StatusName = _localizationManager.GetString(AppConsts.LocalizationSourceName, ((AccountBillingStatusEnum)billingAccountWithEntries.StatusId).ToString());

                result.Add(billingAccountWithEntries);
            }
            return ReturnAccountEntriesComplete(billingAccountWithEntriesList, result);
        }

        private BillingAccountWithCompleteAccountDto ReturnAccountEntriesComplete(List<BillingAccountWithAccountsDto> billingAccountWithEntriesList, List<BillingAccountWithAccountsDto> result)
        {
            if (result == null)
                return null;

            var firstBillingAccountWithEntries = billingAccountWithEntriesList.FirstOrDefault();

            var total = result.Sum(e => e.Total);

            //with reservation
            if (firstBillingAccountWithEntries != null && firstBillingAccountWithEntries.ReservationItemArrivalDate.HasValue)
            {
                var periodOfStay = firstBillingAccountWithEntries.ReservationItemArrivalDate.Value.NumberOfNights(firstBillingAccountWithEntries.ReservationItemDepartureDate.Value);

                return new BillingAccountWithCompleteAccountDto
                {
                    BillingAccountWithAccounts = result
                                        .OrderByDescending(e => e.IsHolder)
                                        .ThenByDescending(e => e.AccountTypeId == (int)BillingAccountTypeEnum.Guest)
                                        .ThenByDescending(e => e.AccountTypeId == (int)BillingAccountTypeEnum.Sparse)
                                        .ThenByDescending(e => e.AccountTypeId == (int)BillingAccountTypeEnum.Company)
                                        .ThenByDescending(e => e.AccountTypeId == (int)BillingAccountTypeEnum.GroupAccount).ToList(),
                    Id = Guid.Empty,
                    AverageSpendPerDay = _billingAccountDomainService.CalculateAverageSpendPerDay(total, periodOfStay),
                    Total = total,
                    PeriodOfStay = periodOfStay
                };
            }
            //sparce account
            else
            {
                return new BillingAccountWithCompleteAccountDto
                {
                    BillingAccountWithAccounts = result
                                        .OrderByDescending(e => e.IsHolder)
                                        .ThenByDescending(e => e.AccountTypeId == (int)BillingAccountTypeEnum.Guest)
                                        .ThenByDescending(e => e.AccountTypeId == (int)BillingAccountTypeEnum.Sparse)
                                        .ThenByDescending(e => e.AccountTypeId == (int)BillingAccountTypeEnum.Company)
                                        .ThenByDescending(e => e.AccountTypeId == (int)BillingAccountTypeEnum.GroupAccount).ToList(),
                    Id = Guid.Empty,
                    AverageSpendPerDay = total,
                    Total = total,
                    PeriodOfStay = 0
                };
            }
        }

        private void AccountEntriesViewProcess(BillingAccountWithAccountsDto billingAccountEntries, List<BillingAccount> billingAccounts, Guid? billingInvoiceId = null)
        {
            var accounts = new List<BillingAccountWithEntriesDto>();

            //agrupar por conta
            foreach (var billingAccount in billingAccounts)
            {
                var account = new BillingAccountWithEntriesDto
                {
                    BillingAccountId = billingAccount.Id,
                    BillingAccountItemName = billingAccount.BillingAccountName,
                    IsBlocked = billingAccount.Blocked,
                    BillingAccountItems = new List<BillingAccountItemsEntriesDto>(),
                    StatusId = billingAccount.StatusId,
                    StartDate = billingAccount.StartDate,
                    StatusName = _localizationManager.GetString(AppConsts.LocalizationSourceName, ((AccountBillingStatusEnum)billingAccount.StatusId).ToString())

                };

                var billingAccountItems = billingAccount.BillingAccountItemList.ToList();

                if (billingInvoiceId.HasValue)
                    billingAccountItems = billingAccountItems.Where(e => e.BillingInvoiceId == billingInvoiceId.Value).ToList();

                var billingAccountItemsParents = billingAccountItems
                    .Where(e => e.BillingAccountItemParent == null
                    || e.BillingAccountItemTypeId == (int)BillingAccountItemTypeEnum.BillingAccountItemCreditNote).ToList();

                foreach (var billingAccountItem in billingAccountItemsParents)
                {
                    var billingAccountItemsEntriesDto = new BillingAccountItemsEntriesDto();

                    if (billingAccountItem.WasReversed)
                    {
                        AccountEntriesReversalViewProcess(ref billingAccountItemsEntriesDto, billingAccountItem, billingAccountItems);
                    }
                    else if (billingAccountItem.BillingAccountItemTypeId == (int)BillingAccountItemTypeEnum.BillingAccountItemTypeDebit
                        || billingAccountItem.BillingAccountItemTypeIdLastSource == (int)BillingAccountItemTypeEnum.BillingAccountItemTypeDebit)
                    {
                        AccountEntriesDebitViewProcess(ref billingAccountItemsEntriesDto, billingAccountItem, billingAccountItems);
                    }
                    else if (billingAccountItem.BillingAccountItemTypeId == (int)BillingAccountItemTypeEnum.BillingAccountItemCreditNote
                        || billingAccountItem.BillingAccountItemTypeId == (int)BillingAccountItemTypeEnum.BillingAccountItemTypeCredit
                        || billingAccountItem.BillingAccountItemTypeIdLastSource == (int)BillingAccountItemTypeEnum.BillingAccountItemTypeCredit)
                    {
                        AccountEntriesCreditViewProcess(ref billingAccountItemsEntriesDto, billingAccountItem, billingAccountItems);
                    }

                    //totalizador
                    account.Total = account.Total + billingAccountItemsEntriesDto.Total;
                    billingAccountEntries.Total = billingAccountEntries.Total + billingAccountItemsEntriesDto.Total;

                    account.BillingAccountItems.Add(billingAccountItemsEntriesDto);
                }

                account.BillingAccountItems = account.BillingAccountItems.OrderByDescending(e => e.BillingAccountItemDate).ToList();

                accounts.Add(account);
            }

            if (billingAccountEntries.ArrivalDate.HasValue && billingAccountEntries.DepartureDate.HasValue)
                billingAccountEntries.PeriodOfStay = _billingAccountDomainService.CalculatePeriodOfStay(billingAccountEntries.ArrivalDate.Value, billingAccountEntries.DepartureDate.Value);

            billingAccountEntries.AverageSpendPerDay = _billingAccountDomainService.CalculateAverageSpendPerDay(billingAccountEntries.Total, billingAccountEntries.PeriodOfStay);

            billingAccountEntries.BillingAccounts = accounts.OrderBy(e => e.StartDate).ToList();
        }

        //OBS: qd for estorno exibe uma linha zerada e não exibe o 'pai/origem' do estorno 
        //     cria-se uma linha de estorno e dentro dela exibe os lançamentos originados pelo estorno
        private void AccountEntriesReversalViewProcess(ref BillingAccountItemsEntriesDto billingAccountItemsEntriesDto, BillingAccountItem parentBillingAccount, List<BillingAccountItem> billingAccountItems)
        {
            //obter a linha PAI de estorno 
            var reversalChild = billingAccountItems.Where(e => e.BillingAccountItemParentId == parentBillingAccount.Id && e.BillingAccountItemTypeId == (int)BillingAccountItemTypeEnum.BillingAccountItemTypeReversal).FirstOrDefault();

            //monta a linha PAI de EXIBIÇÃO
            billingAccountItemsEntriesDto = GetBillingAccountItemReversalLineDto(reversalChild);
            billingAccountItemsEntriesDto.BillingAccountChildrenItems = new List<BillingAccountItemsEntriesDto>();

            #region Children
            //cria linha filha para o PAI
            billingAccountItemsEntriesDto.BillingAccountChildrenItems.Add(GetBillingAccountItemsCreditOrDebitLineDto(reversalChild));

            //cria linhas filhas caso existam taxas
            var originTaxesIds = billingAccountItems.Where(e => e.BillingAccountItemParentId == parentBillingAccount.Id && e.WasReversed).Select(e => e.Id).ToList();
            if (originTaxesIds.Any())
            {
                var reversalTaxChildren = billingAccountItems.Where(e => e.BillingAccountItemParentId.HasValue && originTaxesIds.Contains(e.BillingAccountItemParentId.Value)).ToList();

                foreach (var reversalTaxChild in reversalTaxChildren)
                {
                    billingAccountItemsEntriesDto.BillingAccountChildrenItems.Add(GetBillingAccountItemsCreditOrDebitLineDto(reversalTaxChild));
                }
            }
            #endregion
        }

        //OBS: qd for débito exibe uma linha com o total calculado de débitos com suas taxas
        //     é exibido os filhos detalhadamente
        private void AccountEntriesDebitViewProcess(ref BillingAccountItemsEntriesDto billingAccountItemsEntriesDto, BillingAccountItem parentBillingAccount, List<BillingAccountItem> billingAccountItems)
        {
            //filhos exceto os estornados e divididos
            var children = billingAccountItems.Where(e =>
                        e.BillingAccountItemParentId == parentBillingAccount.Id &&
                        !e.WasReversed)
                        .OrderBy(x => x.BillingAccountItemDate).ToList();

            var totalWithoutRate = Math.Round(parentBillingAccount.Amount, 2);
            var percentageRate = children.Count > 0 && totalWithoutRate != 0 ? Math.Round(children.Sum(e => e.Amount) / totalWithoutRate, 2) : 0;
            var total = children.Count > 0 ? children.Sum(e => e.Amount) + parentBillingAccount.Amount : parentBillingAccount.Amount;

            //monta a linha PAI de EXIBIÇÃO
            billingAccountItemsEntriesDto = GetBillingAccountItemDebitLineDto(parentBillingAccount, total);
            billingAccountItemsEntriesDto.TotalWithoutRate = totalWithoutRate;
            billingAccountItemsEntriesDto.PercentageRate = percentageRate;
            billingAccountItemsEntriesDto.IsProduct = parentBillingAccount.BillingItem.BillingItemTypeId == (int)BillingItemTypeEnum.PointOfSale;
            billingAccountItemsEntriesDto.BillingAccountChildrenItems = new List<BillingAccountItemsEntriesDto>();
            billingAccountItemsEntriesDto.BillingInvoiceNumber = parentBillingAccount.BillingInvoice != null ? parentBillingAccount.BillingInvoice.ExternalNumber : null;
            billingAccountItemsEntriesDto.BillingInvoiceSeries = parentBillingAccount.BillingInvoice != null ? parentBillingAccount.BillingInvoice.ExternalSeries : null;

            #region Children

            //cria linha filha para o PAI
            if (parentBillingAccount.IntegrationDetail is null)
                billingAccountItemsEntriesDto.BillingAccountChildrenItems.Add(GetBillingAccountItemsCreditOrDebitLineDto(parentBillingAccount));

            //cria linhas filha
            foreach (var child in children)
            {
                billingAccountItemsEntriesDto.BillingAccountChildrenItems.Add(GetBillingAccountItemsCreditOrDebitLineDto(child));
            }

            // cria a linha filha dos itens enviados pelo lançador
            if (parentBillingAccount.IntegrationDetail != null)
            {
                var detailsList = JsonConvert.DeserializeObject<List<BillingAccountItemIntegrationDetailDto>>(parentBillingAccount.IntegrationDetail);
                foreach (var detail in detailsList)
                {
                    var launcherDetail = GetBillingAccountItemsCreditOrDebitLineDto(_billingAccountItemAdapter.MapChild(parentBillingAccount)
                                                                                                              .WithId(parentBillingAccount.Id)
                                                                                                              .Build());

                    launcherDetail.BillingItemCategoryName = detail.GroupDescription;
                    launcherDetail.Total = detail.Amount.Replace(".", ",").TryParseToDecimal() * -1;
                    launcherDetail.Total =
                        parentBillingAccount.BillingItem.BillingItemTypeId == (int)BillingItemTypeEnum.Service ?
                           parentBillingAccount.Amount : detail.Amount.Replace(".", ",").TryParseToDecimal() * -1;
                    launcherDetail.TotalWithoutRate = detail.Amount.Replace(".", ",").TryParseToDecimal() * -1;
                    launcherDetail.BillingItemName = detail.Description;

                    billingAccountItemsEntriesDto.BillingAccountChildrenItems.Add(launcherDetail);
                }
            }

            #endregion
        }

        //OBS: qd for crédito exibe apenas uma linha
        private void AccountEntriesCreditViewProcess(ref BillingAccountItemsEntriesDto billingAccountItemsEntriesDto, BillingAccountItem parentBillingAccount, List<BillingAccountItem> billingAccountItems)
        {
            var discounts = billingAccountItems.Where(e => e.BillingAccountItemParentId == parentBillingAccount.Id && !e.WasReversed).ToList();

            if (discounts.Count > 0)
            {
                var total = discounts.Count > 0 ? discounts.Sum(e => e.Amount) + parentBillingAccount.Amount : parentBillingAccount.Amount;

                billingAccountItemsEntriesDto = GetBillingAccountItemDebitLineDto(parentBillingAccount, total);
                billingAccountItemsEntriesDto.TotalWithoutRate = total;
                billingAccountItemsEntriesDto.PercentageRate = 0;
                billingAccountItemsEntriesDto.BillingInvoiceNumber = parentBillingAccount.BillingInvoice != null ? parentBillingAccount.BillingInvoice.ExternalNumber : null;
                billingAccountItemsEntriesDto.BillingInvoiceSeries = parentBillingAccount.BillingInvoice != null ? parentBillingAccount.BillingInvoice.ExternalSeries : null;
                billingAccountItemsEntriesDto.BillingAccountChildrenItems = new List<BillingAccountItemsEntriesDto>();

                billingAccountItemsEntriesDto.BillingAccountChildrenItems.Add(GetBillingAccountItemsCreditOrDebitLineDto(parentBillingAccount));

                foreach (var discount in discounts)
                {
                    billingAccountItemsEntriesDto.BillingAccountChildrenItems.Add(GetBillingAccountItemsCreditOrDebitLineDto(discount));
                }
            }
            else billingAccountItemsEntriesDto = GetBillingAccountItemsCreditOrDebitLineDto(parentBillingAccount);
        }

        private BillingAccountItemsEntriesDto GetBillingAccountItemsCreditOrDebitLineDto(BillingAccountItem billingAccountItem)
        {
            var itemType = billingAccountItem.BillingAccountItemTypeIdLastSource.HasValue ? billingAccountItem.BillingAccountItemTypeIdLastSource.Value : billingAccountItem.BillingAccountItemTypeId;

            var billingAccountItemLine = new BillingAccountItemsEntriesDto
            {
                BillingAccountItemId = billingAccountItem.Id,
                Total = billingAccountItem.Amount,
                TotalWithoutRate = billingAccountItem.Amount,
                BillingAccountItemTypeName = billingAccountItem.Amount >= 0 ? _localizationManager.GetString(AppConsts.LocalizationSourceName, BillingAccountItemTypeEnum.BillingAccountItemTypeCredit.ToString()) : _localizationManager.GetString(AppConsts.LocalizationSourceName, BillingAccountItemTypeEnum.BillingAccountItemTypeDebit.ToString()),
                BillingAccountItemTypeId = billingAccountItem.Amount >= 0 ? (int)BillingAccountItemTypeEnum.BillingAccountItemTypeCredit : (int)BillingAccountItemTypeEnum.BillingAccountItemTypeDebit,
                BillingAccountItemDate = billingAccountItem.BillingAccountItemDate,
                DateParameter = billingAccountItem.DateParameter,
                PartialBillingAccountItemId = billingAccountItem.PartialParentBillingAccountItemId,
                BillingItemId = billingAccountItem.BillingItemId.HasValue ? billingAccountItem.BillingItemId.Value : 0,
                ReasonName = billingAccountItem.BillingAccountItemReason != null ? billingAccountItem.BillingAccountItemReason.ReasonName : null,
                Discount = itemType == (int)BillingAccountItemTypeEnum.BillingAccountItemTypeDiscount ? billingAccountItem.Amount : 0,
                BillingInvoiceId = billingAccountItem.BillingInvoiceId,
                BillingInvoiceNumber = billingAccountItem.BillingInvoice != null ? billingAccountItem.BillingInvoice.ExternalNumber : null,
                BillingInvoiceSeries = billingAccountItem.BillingInvoice != null ? billingAccountItem.BillingInvoice.ExternalSeries : null
            };

            if (itemType == (int)BillingAccountItemTypeEnum.BillingAccountItemTypeDiscount)
            {
                billingAccountItemLine.BillingItemCategoryName = _localizationManager.GetString(AppConsts.LocalizationSourceName, BillingAccountItemTypeEnum.BillingAccountItemTypeDiscount.ToString());
                billingAccountItemLine.BillingItemName = billingAccountItem.PercentualAmount != null && billingAccountItem.PercentualAmount > 0 ? billingAccountItem.PercentualAmount.ToString() : null;
            }
            else if (billingAccountItem.BillingItem != null && billingAccountItem.BillingItem.PaymentType != null)
                billingAccountItemLine.BillingItemCategoryName = billingAccountItem.BillingItem.BillingItemName;
            else
            {
                billingAccountItemLine.BillingItemCategoryName = billingAccountItem.BillingItem?.BillingItemCategory?.CategoryName;
                billingAccountItemLine.BillingItemName = billingAccountItem.BillingItem?.BillingItemName;
            }

            if (billingAccountItemLine.BillingAccountItemTypeId == (int)BillingAccountItemTypeEnum.BillingAccountItemTypeCredit ||
               billingAccountItemLine.BillingAccountItemTypeId == (int)BillingAccountItemTypeEnum.BillingAccountItemTypeDebit)
                billingAccountItemLine.PlasticBrandFormatted = _billingItemReadRepository.GetPlasticBrandFormattedByBillingItemId(billingAccountItemLine.BillingItemId);

            return billingAccountItemLine;
        }

        private BillingAccountItemsEntriesDto GetBillingAccountItemReversalLineDto(BillingAccountItem billingAccountItem)
        {
            var billingAccountItemLine = new BillingAccountItemsEntriesDto
            {
                BillingAccountItemId = billingAccountItem.Id,
                BillingItemName = billingAccountItem.BillingItem.BillingItemName,
                Total = 0,
                TotalWithoutRate = 0,
                PercentageRate = 0,
                BillingAccountItemTypeName = _localizationManager.GetString(AppConsts.LocalizationSourceName, BillingAccountItemTypeEnum.BillingAccountItemTypeReversal.ToString()),
                BillingAccountItemTypeId = (int)BillingAccountItemTypeEnum.BillingAccountItemTypeReversal,
                BillingItemCategoryName = billingAccountItem.BillingItem.BillingItemCategory != null ? billingAccountItem.BillingItem.BillingItemCategory.CategoryName : "",
                BillingAccountItemDate = billingAccountItem.BillingAccountItemDate,
                DateParameter = billingAccountItem.DateParameter,
                PartialBillingAccountItemId = billingAccountItem.PartialParentBillingAccountItemId,
                BillingInvoiceId = billingAccountItem.BillingInvoiceId
            };

            if (billingAccountItem.BillingItem.PaymentType != null)
                billingAccountItemLine.BillingItemCategoryName = _localizationManager.GetString(AppConsts.LocalizationSourceName, ((PaymentTypeEnum)billingAccountItem.BillingItem.PaymentType.Id).ToString());
            else if (billingAccountItem.BillingItem != null && billingAccountItem.BillingItem.BillingItemCategory != null)
                billingAccountItemLine.BillingItemCategoryName = billingAccountItem.BillingItem.BillingItemCategory.CategoryName;

            return billingAccountItemLine;
        }

        private BillingAccountItemsEntriesDto GetBillingAccountItemDebitLineDto(BillingAccountItem billingAccountItem, decimal total)
        {
            return new BillingAccountItemsEntriesDto
            {
                BillingAccountItemId = billingAccountItem.Id,
                BillingItemName = billingAccountItem.BillingItem.BillingItemName,
                Total = total,
                BillingAccountItemTypeName = _localizationManager.GetString(AppConsts.LocalizationSourceName, BillingAccountItemTypeEnum.BillingAccountItemTypeDebit.ToString()),
                BillingAccountItemTypeId = (int)BillingAccountItemTypeEnum.BillingAccountItemTypeDebit,
                BillingItemCategoryName = billingAccountItem.BillingItem.BillingItemCategory != null ? billingAccountItem.BillingItem.BillingItemCategory.CategoryName : "",
                BillingAccountItemDate = billingAccountItem.BillingAccountItemDate,
                DateParameter = billingAccountItem.DateParameter,
                PartialBillingAccountItemId = billingAccountItem.PartialParentBillingAccountItemId,
                BillingItemId = billingAccountItem.BillingItemId.HasValue ? billingAccountItem.BillingItemId.Value : 0,
                BillingInvoiceId = billingAccountItem.BillingInvoiceId
            };
        }

        #endregion

        public BillingAccountWithAccountsDto GetBillingAccountStatement(DefaultGuidRequestDto id)
        {
            var billingAccountWithEntries = _billingAccountReadRepository.GetById(id);

            if (billingAccountWithEntries == null)
                return null;

            var groupKeys = _billingAccountReadRepository.GetBillingAccountIdsByGroupId(billingAccountWithEntries.GroupKey);

            var billingAccounts = _billingAccountReadRepository.GetAllByBillingAccountGroupKey(groupKeys);

            AccountEntriesViewProcess(billingAccountWithEntries, billingAccounts);

            ValidateAndInsertPropertyCurrencySymbol(billingAccountWithEntries);

            return billingAccountWithEntries;
        }

        public BillingAccountDto AddBillingAccount(AddBillingAccountDto dto, BillingAccount copyBillingAccount = null, Guid groupKey = new Guid(), bool usePersonHeaderId = false)
        {
            if (copyBillingAccount == null)
            {
                if (dto == null || dto.BillingAccountId == Guid.Empty)
                    NotifyParameterInvalid();

                if (string.IsNullOrEmpty(dto.BillingAccountName))
                {
                    Notification.Raise(Notification.DefaultBuilder
                   .WithMessage(AppConsts.LocalizationSourceName, BillingAccount.EntityError.BillingAccountMustHaveBillingAccountName)
                   .WithDetailedMessage(AppConsts.LocalizationSourceName, BillingAccount.EntityError.BillingAccountMustHaveBillingAccountName)
                   .Build());
                    return BillingAccountDto.NullInstance;
                }
            }

            var originalBillingAccount = copyBillingAccount ?? BillingAccountDomainService.Get(new DefaultGuidRequestDto(dto.BillingAccountId));

            var personHeaderId = default(Guid?);

            if (usePersonHeaderId)
            {
                var headerPersonDto = _personAppService.GetHeaderByBillingAccountId(originalBillingAccount.Id);

                if (headerPersonDto != null)
                {
                    personHeaderId = CreatePersonHeader(headerPersonDto);

                    if (Notification.HasNotification())
                        return null;
                }
            }

            var billingAccountBuilder = BillingAccountAdapter.AddBillingAccountMap(originalBillingAccount, dto, groupKey, personHeaderId);

            var result = new BillingAccountDto { BillingAccountName = dto.BillingAccountName };
            result.Id = BillingAccountDomainService.InsertAndSaveChanges(billingAccountBuilder).Id;

            return result;
        }

        private Guid? CreatePersonHeader(HeaderPersonDto headerPersonDto)
        {
            Guid? personHeaderId;
            headerPersonDto.Id = Guid.Empty;
            headerPersonDto.BillingAccountId = Guid.Empty;
            headerPersonDto.LocationList.ToList().ForEach(location =>
            {
                location.Id = 0;
                location.BrowserLanguage = _applicationUser.PreferredLanguage;
            });

            personHeaderId = AssociatePersonHeader(headerPersonDto).Id;
            return personHeaderId;
        }

        public BillingAccountDto AddBillingAccountCreditNote(AddBillingAccountDto dto, BillingAccount copyBillingAccount = null)
        {
            if (copyBillingAccount == null)
            {
                if (dto == null || dto.BillingAccountId == Guid.Empty)
                    NotifyParameterInvalid();

                if (string.IsNullOrEmpty(dto.BillingAccountName))
                {
                    Notification.Raise(Notification.DefaultBuilder
                   .WithMessage(AppConsts.LocalizationSourceName, BillingAccount.EntityError.BillingAccountMustHaveBillingAccountName)
                   .WithDetailedMessage(AppConsts.LocalizationSourceName, BillingAccount.EntityError.BillingAccountMustHaveBillingAccountName)
                   .Build());
                    return BillingAccountDto.NullInstance;
                }
            }

            var originalBillingAccount = copyBillingAccount ?? BillingAccountDomainService.Get(new DefaultGuidRequestDto(dto.BillingAccountId));

            var billingAccountBuilder = BillingAccountAdapter.AddBillingAccountCreditNoteMap(originalBillingAccount, dto);

            var result = new BillingAccountDto { BillingAccountName = dto.BillingAccountName };
            result.Id = BillingAccountDomainService.InsertAndSaveChanges(billingAccountBuilder).Id;

            return result;
        }

        public override async Task Delete(Guid id)
        {
            if (!ValidateId(id)) return;

            if (Notification.HasNotification())
                return;

            IsClosedOrBlockedAccount(id);

            if (Notification.HasNotification())
                return;

            //conta principal n pode ser excluida
            if (_billingAccountReadRepository.IsMainAccount(id))
            {
                Notification.Raise(Notification.DefaultBuilder
                .WithMessage(AppConsts.LocalizationSourceName, BillingAccountEnum.Error.MainBillingAccountCanNotBeDeleted)
                .WithDetailedMessage(AppConsts.LocalizationSourceName, BillingAccountEnum.Error.MainBillingAccountCanNotBeDeleted)
                .Build());
                return;
            }

            //conta só pode ser excluida caso nao tenha lançamentos
            if (_billingAccountItemReadRepository.BillingAccountHasBillingAccountItems(id))
            {
                Notification.Raise(Notification.DefaultBuilder
                .WithMessage(AppConsts.LocalizationSourceName, BillingAccountEnum.Error.BillingAccountWithBillingAccountItemsCanNotBeDeleted)
                .WithDetailedMessage(AppConsts.LocalizationSourceName, BillingAccountEnum.Error.BillingAccountWithBillingAccountItemsCanNotBeDeleted)
                .Build());
                return;
            }

            await BillingAccountDomainService.DeleteAsync(w => w.Id == id);

        }

        private void IsClosedOrBlockedAccount(Guid billingAccountId)
        {
            if (_billingAccountReadRepository.IsClosedOrBlockedAccount(billingAccountId))
            {
                Notification.Raise(Notification.DefaultBuilder
                .WithMessage(AppConsts.LocalizationSourceName, BillingAccountEnum.Error.BillingAccountWithBillingAccountItemsCanNotBeClosedOrBlocked)
                .WithDetailedMessage(AppConsts.LocalizationSourceName, BillingAccountEnum.Error.BillingAccountWithBillingAccountItemsCanNotBeClosedOrBlocked)
                .Build());
            }
        }

        public BillingAccountTotalForClosureDto GetBillingAccountsForClosure(Guid id, bool allSimiliarAccounts)
        {
            if (id == Guid.Empty)
                NotifyIdIsMissing();

            var result = _billingAccountReadRepository.GetBillingAccountsForClosure(id, allSimiliarAccounts);

            return result;
        }

        public IListDto<SearchBillingAccountResultDto> GetAllBillingAccountsByUhOrSparseAccount(Guid id, int propertyId)
        {
            if (id == Guid.Empty)
                NotifyIdIsMissing();

            return _billingAccountReadRepository.GetAllBillingAccountsByUhOrSparseAccount(id, propertyId);
        }

        public IListDto<SearchBillingAccountResultDto> GetAllByFiltersForTransferSearch(SearchBillingAccountDto request, int propertyId)
        {
            int minimumCharacters = 2;

            if (propertyId <= 0)
            {
                NotifyIdIsMissing();
                return null;
            }

            if (string.IsNullOrEmpty(request.FreeTextSearch))
                NotifyRequired("FreeTextSearch");
            else if (request.FreeTextSearch.Length < minimumCharacters)
                NotifyMinimumCharacters(minimumCharacters);

            if (!request.BaseAccountIgnored.HasValue)
            {
                NotifyParameterInvalid();
                return null;
            }

            return _billingAccountReadRepository.GetAllByFiltersV2(request, propertyId);
        }

        public void ReopenBillingAccount(int propertyId, Guid id, int reasonId)
        {
            ValidatePropertyAndBillingAccountId(propertyId, id);

            if (Notification.HasNotification()) return;

            ValidateIfBillingAccountIsOpen(id);
            ValidateIfReasonIsToReopenBillingAccount(reasonId);

            if (Notification.HasNotification()) return;

            var allAccountsAreClosed = _billingAccountReadRepository.AllSimiliarBillingAccountsAreClosed(id);

            _billingAccountDomainService.Reopen(propertyId, id, reasonId, allAccountsAreClosed);
        }

        private void ValidatePropertyAndBillingAccountId(int propertyId, Guid id)
        {
            if (id == Guid.Empty || propertyId <= 0)
                NotifyIdIsMissing();
        }

        private void ValidateIfBillingAccountIsOpen(Guid id)
        {
            if (_billingAccountReadRepository.IsOpen(id))
                Notification.Raise(Notification.DefaultBuilder
                    .WithMessage(AppConsts.LocalizationSourceName, BillingAccountEnum.Error.CanNotReopenAnBillingAccountAlreadyOpen)
                    .Build());
        }

        private void ValidateIfReasonIsToReopenBillingAccount(int reasonId)
        {
            if (!_reasonReadRepository.CheckIfReasonIsToReopenBillingAccount(reasonId))
            {
                Notification.Raise(Notification.DefaultBuilder
                .WithMessage(AppConsts.LocalizationSourceName, BillingAccountItemEnum.Error.BillingAccountItemInvalidReasonId)
                .WithDetailedMessage(AppConsts.LocalizationSourceName, BillingAccountItemEnum.Error.BillingAccountItemInvalidReasonId)
                .Build());
            }
        }

        public void CheckIfExistIsMainBillingAccount(long reservationId, long reservationItemId, long? guestReservationItem, Guid? companyClient, int billingAccountTypeId)
        {
            if (_billingAccountReadRepository.CheckIfExistIsMainBillingAccount(reservationId, reservationItemId, guestReservationItem.HasValue ? guestReservationItem.Value : (long?)null, companyClient.HasValue ? companyClient.Value : (Guid?)null, billingAccountTypeId))
            {
                Notification.Raise(Notification.DefaultBuilder
                .WithMessage(AppConsts.LocalizationSourceName, BillingAccount.EntityError.BillingAccountHasAlreadyCreated)
                .WithDetailedMessage(AppConsts.LocalizationSourceName, BillingAccount.EntityError.BillingAccountHasAlreadyCreated)
                .Build());
            }
        }

        public BillingAccountDto GetByInvoiceId(Guid invoiceId)
        {
            if (invoiceId == Guid.Empty)
                NotifyIdIsMissing();

            var result = _billingAccountReadRepository.GetByInvoiceId(invoiceId);

            return result;
        }

        public Guid CreateAndSetNewMainAccount(Guid groupKey)
        {
            var oldBillingAccountMain = SetIsMainAccountToFalseByGroupKey(groupKey);

            if (oldBillingAccountMain == null || (oldBillingAccountMain != null && oldBillingAccountMain.Id == Guid.Empty))
            {
                Notification.Raise(Notification
                    .DefaultBuilder
                    .WithMessage(AppConsts.LocalizationSourceName, PropertyParameter.EntityError.GroupKeyInvalid)
                    .WithMessageFormat(groupKey)
                    .Build());
                return groupKey;
            }

            var addBillingAccountDto = new AddBillingAccountDto
            {
                BillingAccountId = oldBillingAccountMain.Id,
                BillingAccountName = oldBillingAccountMain.BillingAccountName,
                CreatePrincipalAccount = true
            };
            return AddBillingAccount(addBillingAccountDto, null, groupKey).Id;
        }

        private BillingAccount SetIsMainAccountToFalseByGroupKey(Guid groupKey)
        {
            var billingAccountMain = _billingAccountRepository.GetById(groupKey);

            if (billingAccountMain == null)
            {
                var billingAccountOnlyWithGroupKey = new BillingAccount();
                billingAccountOnlyWithGroupKey.SetGroupKey(groupKey);
                return billingAccountOnlyWithGroupKey;
            }

            billingAccountMain.SetIsMainAccount(false);
            return billingAccountMain;
        }

        public BillingAccount ChangeMainAccount(BillingAccount newBillingAccountMain)
        {
            var mainAccount = SetIsMainAccountToFalseByGroupKey(newBillingAccountMain.GroupKey);

            if (mainAccount == null || (mainAccount != null && mainAccount.Id == Guid.Empty))
                return newBillingAccountMain;

            newBillingAccountMain.SetIsMainAccount(true);
            return newBillingAccountMain;
        }

        public HeaderPersonDto AssociatePersonHeader(HeaderPersonDto personDto)
        {
            if (personDto.Id != Guid.Empty)
            {
                VerifyPersonHeaderAlreadyAssociateWithOtherAccount(personDto.Id, personDto.BillingAccountId ?? Guid.Empty);

                if (Notification.HasNotification()) return null;
            }

            using (var uow = _unitOfWorkManager.Begin())
            {
                personDto.Id = _personAppService.CreateOrUpdateHeaderPersonAndGetId(personDto);

                if (personDto.BillingAccountId.HasValue && personDto.BillingAccountId.Value != Guid.Empty)
                {
                    var billingAccount = _billingAccountReadRepository.GetById(personDto.BillingAccountId.GetValueOrDefault());
                    billingAccount.SetPersonHeaderId(personDto.Id);
                    _billingAccountRepository.Update(billingAccount);
                }    

                uow.Complete();

                return personDto;
            }
        }

        private void VerifyPersonHeaderAlreadyAssociateWithOtherAccount(Guid personHeaderId, Guid billingAccountId)
        {
            if (personHeaderId != Guid.Empty)
            {
                var existingBillingAccountId = _billingAccountReadRepository.GetIdByPersonHeaderId(personHeaderId);

                if (existingBillingAccountId != Guid.Empty && existingBillingAccountId != billingAccountId)
                    Notification.Raise(Notification.DefaultBuilder
                       .WithMessage(AppConsts.LocalizationSourceName, BillingAccountEnum.Error.PersonHeaderAlreadyAssociateWithOtherAccount)
                       .WithDetailedMessage(AppConsts.LocalizationSourceName, BillingAccountEnum.Error.PersonHeaderAlreadyAssociateWithOtherAccount)
                       .Build());
            }
        }

        public HeaderPersonDto GetHeaderByBillingAccountId(Guid billingAccountId)
        {
            return _personAppService.GetHeaderByBillingAccountId(billingAccountId);
        }

        public HeaderPersonDto GetHeaderByBillingInvoiceId(Guid billingInvoiceId)
        {
            return _personAppService.GetHeaderByBillingInvoiceId(billingInvoiceId);
        }

        private Guid GetDefaultCurrencyIdByProperty(int propertyId)
        {
            var propertyCurrency = _PropertyParameterReadRepository.GetPropertyParameterForPropertyIdAndApplicationParameterId(propertyId, (int)ApplicationParameterEnum.ParameterDefaultCurrency);
            return Guid.Parse(propertyCurrency.PropertyParameterValue.Trim());
        }

        public void CheckIfExistIsMainBillingAccountByGuestReservationItemIds(List<long?> guestReservationItemIds)
        {
            if (_billingAccountReadRepository.CheckIfExistIsMainBillingAccountByGuestReservationItemIds(guestReservationItemIds))
            {
                Notification.Raise(Notification.DefaultBuilder
                .WithMessage(AppConsts.LocalizationSourceName, BillingAccount.EntityError.BillingAccountExistToGuest)
                .WithDetailedMessage(AppConsts.LocalizationSourceName, BillingAccount.EntityError.BillingAccountExistToGuest)
                .Build());
            }
        }
    }
}