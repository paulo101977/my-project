﻿using System;
using Thex.Application.Adapters;
using Thex.Domain.Entities;
using Thex.Application.Interfaces;
using Tnf.Domain.Services;
using Tnf.Notifications;

namespace Thex.Application.Services
{
    public class BusinessPartnerTypeAppService : ScaffoldBusinessPartnerTypeAppService, IBusinessPartnerTypeAppService
    {
        public BusinessPartnerTypeAppService(
            IBusinessPartnerTypeAdapter businessPartnerTypeAdapter,
            IDomainService<BusinessPartnerType> businessPartnerTypeDomainService,
            INotificationHandler notificationHandler)
            : base(businessPartnerTypeAdapter, businessPartnerTypeDomainService, notificationHandler)
        {
        }
    }
}
