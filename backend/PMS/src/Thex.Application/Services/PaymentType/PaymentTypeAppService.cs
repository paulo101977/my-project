﻿using System;
using Thex.Application.Adapters;
using Thex.Domain.Entities;
using Thex.Application.Interfaces;
using Tnf.Domain.Services;
using System.Threading.Tasks;
using Thex.Dto;
using Tnf.Dto;
using Thex.Common.Enumerations;
using System.Linq;
using Tnf.Localization;
using Thex.Common;
using Tnf.Notifications;

namespace Thex.Application.Services
{
    public class PaymentTypeAppService : ScaffoldPaymentTypeAppService, IPaymentTypeAppService
    {
        private readonly ILocalizationManager _localizationManager;

        public PaymentTypeAppService(
            IPaymentTypeAdapter paymentTypeAdapter,
            IDomainService<PaymentType> paymentTypeDomainService,
            ILocalizationManager localizationManager,
            INotificationHandler notificationHandler)
            : base(paymentTypeAdapter, paymentTypeDomainService, notificationHandler)
        {
            _localizationManager = localizationManager;
        }

        public virtual async Task<IListDto<PaymentTypeDto>> GetAllForCreation(GetAllPaymentTypeDto request)
        {
            ValidateRequestAllDto(request, nameof(request));

            if (Notification.HasNotification())
                return null;

            var ids = (new int[] {
                (int)PaymentTypeEnum.Debit,
                (int)PaymentTypeEnum.CreditCard,
                (int)PaymentTypeEnum.TobeBilled,
                (int)PaymentTypeEnum.Check,
                (int)PaymentTypeEnum.Money,
                (int)PaymentTypeEnum.Deposit
            }).ToList();

            var response = await PaymentTypeDomainService.GetAllAsync<PaymentTypeDto>(request, d => ids.Contains(d.Id)).ConfigureAwait(false);

            foreach (var item in response.Items)
                item.PaymentTypeName = _localizationManager.GetString(AppConsts.LocalizationSourceName, ((PaymentTypeEnum)item.Id).ToString());

            return response;
        }
    }
}
