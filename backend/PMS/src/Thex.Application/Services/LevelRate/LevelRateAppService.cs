﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Thex.Application.Adapters;
using Thex.Application.Interfaces;
using Thex.Common;
using Thex.Common.Enumerations;
using Thex.Domain.Entities;
using Thex.Domain.Interfaces.Repositories;
using Thex.Dto;
using Thex.Infra.ReadInterfaces;
using Thex.Kernel;
using Tnf.Domain.Services;
using Tnf.Dto;
using Tnf.Notifications;
using Tnf.Repositories.Uow;

namespace Thex.Application.Services
{
    public class LevelRateAppService : ScaffoldLevelRateAppService, ILevelRateAppService
    {
        private readonly ILevelRateHeaderReadRepository _levelRateHeaderReadRepository;
        private readonly IApplicationUser _applicationUser;
        private readonly IPropertyMealPlanTypeRateReadRepository _propertyMealPlanTypeRateReadRepository;
        private readonly ILevelRateHeaderRepository _levelRateHeaderRepository;
        private readonly ILevelRateHeaderAdapter _levelRateHeaderAdapter;
        private readonly IDomainService<LevelRateHeader> _levelRateHeaderDomainService;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly ILevelRateRepository _levelRateRepository;
        private readonly IRoomTypeReadRepository _roomTypeReadRepository;
        private readonly IPropertyMealPlanTypeReadRepository _propertyMealPlanTypeReadRepository;

        public LevelRateAppService(
            ILevelRateAdapter levelRateAdapter,
            ILevelRateHeaderAdapter levelRateHeaderAdapter,
            IApplicationUser applicationUser,
            ILevelRateHeaderReadRepository levelRateHeaderReadRepository,
            ILevelRateHeaderRepository levelRateHeaderRepository,
            ILevelRateRepository levelRateRepository,
            IPropertyMealPlanTypeRateReadRepository propertyMealPlanTypeRateReadRepository,
            IPropertyMealPlanTypeReadRepository propertyMealPlanTypeReadRepository,
            IRoomTypeReadRepository roomTypeReadRepository,
            IDomainService<LevelRate> levelRateDomainService,
            IDomainService<LevelRateHeader> levelRateHeaderDomainService,
            IUnitOfWorkManager unitOfWorkManager,
            INotificationHandler notificationHandler)
                : base(levelRateAdapter, levelRateDomainService, notificationHandler)
        {
            _levelRateHeaderReadRepository = levelRateHeaderReadRepository;
            _applicationUser = applicationUser;
            _propertyMealPlanTypeRateReadRepository = propertyMealPlanTypeRateReadRepository;
            _levelRateHeaderRepository = levelRateHeaderRepository;
            _levelRateHeaderAdapter = levelRateHeaderAdapter;
            _levelRateHeaderDomainService = levelRateHeaderDomainService;
            _unitOfWorkManager = unitOfWorkManager;
            _levelRateRepository = levelRateRepository;
            _roomTypeReadRepository = roomTypeReadRepository;
            _propertyMealPlanTypeReadRepository = propertyMealPlanTypeReadRepository;
        }

        public void Create(LevelRateResultDto dto)
        {
            ValidateCreateLevelRate(dto);

            if (Notification.HasNotification()) return;

            using (var uow = _unitOfWorkManager.Begin())
            {
                var levelRateHeaderBuilder = _levelRateHeaderAdapter.Map(dto.LevelRateHeader);

                var levelRateHeader = _levelRateHeaderDomainService.InsertAndSaveChanges(levelRateHeaderBuilder);

                InsertLevelRates(dto, levelRateHeader.Id);

                if (Notification.HasNotification()) return;

                uow.Complete();
            }
        }

        public void Update(LevelRateResultDto dto, Guid id)
        {
            dto.LevelRateHeader.Id = id;

            var levelRateHeaderEntity = _levelRateHeaderReadRepository.GetById(id);

            ValidateUpdateLevelRate(dto, levelRateHeaderEntity);

            if (Notification.HasNotification()) return;

            using (var uow = _unitOfWorkManager.Begin())
            {
                var levelRateHeaderBuilder = _levelRateHeaderAdapter.Map(levelRateHeaderEntity, dto.LevelRateHeader);

                var levelRateHeader = _levelRateHeaderDomainService.Update(levelRateHeaderBuilder);

                UpdateLevelRates(dto, levelRateHeader.Id);

                if (Notification.HasNotification()) return;

                uow.Complete();
            }
        }

        public IListDto<LevelRateHeaderDto> GetAllDto()
        {
            var levelRateList = _levelRateHeaderReadRepository.GetAllDto(_applicationUser.TenantId);

            return new ListDto<LevelRateHeaderDto>
            {
                Items = levelRateList.ToList(),
                HasNext = false
            };
        }

        public LevelRateResultDto GetById(Guid levelRateHeaderId)
            => _levelRateHeaderReadRepository.GetDtoById(levelRateHeaderId);

        public LevelRateResultRequestDto GetAllLevelsAvailable(LevelRateRequestDto request)
        {
            ValidateDates(request.InitialDate, request.EndDate);

            if (Notification.HasNotification()) return null;

            var rangeDates = GenerateRangeDate(request.InitialDate, request.EndDate);
            var propertyId = int.Parse(_applicationUser.PropertyId);
            var levelsDto = _levelRateHeaderReadRepository.GetAllLevelsAvailable(request, propertyId);

            return new LevelRateResultRequestDto
            {
                LevelList = levelsDto,
                HasMealPlanTypes = _propertyMealPlanTypeRateReadRepository.ExistsForAllPeriod(request.CurrencyId, propertyId, rangeDates)
        };
        }

        public void ToggleActive(Guid id)
        {
            var levelRate = _levelRateHeaderReadRepository.GetById(id);
            if (levelRate == null)
            {
                NotifyNullParameter();
                return;
            }

            ValidatePermission(levelRate.TenantId, _applicationUser.TenantId);

            if (Notification.HasNotification()) return;

            if (!levelRate.IsActive && !LevelIsAvailable(levelRate.InitialDate, levelRate.EndDate, levelRate.CurrencyId, levelRate.LevelId, id))
            {
                Notification.Raise(Notification.DefaultBuilder
               .WithMessage(AppConsts.LocalizationSourceName, LevelRateHeader.EntityError.AlreadyExistsActiveLevelRate)
               .WithDetailedMessage(AppConsts.LocalizationSourceName, LevelRateHeader.EntityError.AlreadyExistsActiveLevelRate)
               .Build());

                return;
            }

            _levelRateHeaderRepository.ToggleIsActive(levelRate.Id);
        }

        public void Remove(Guid id)
        {
            var levelRateHeader = _levelRateHeaderReadRepository.GetById(id);
            if (levelRateHeader == null)
            {
                NotifyNullParameter();
                return;
            }

            ValidatePermission(levelRateHeader.TenantId, _applicationUser.TenantId);

            if (Notification.HasNotification()) return;

            if (_levelRateHeaderReadRepository.LevelRateAlreadyUsed(levelRateHeader.Id))
            {
                Notification.Raise(Notification.DefaultBuilder
                   .WithMessage(AppConsts.LocalizationSourceName, LevelRateHeader.EntityError.LevelRateHeaderCanNotBeRemovedBecauseAlreadyUsed)
                   .WithDetailedMessage(AppConsts.LocalizationSourceName, LevelRateHeader.EntityError.LevelRateHeaderCanNotBeRemovedBecauseAlreadyUsed)
                   .Build());

                return;
            }

            _levelRateHeaderRepository.RemoveAndSaveChanges(levelRateHeader);
        }

        public IListDto<PropertyBaseRateByLevelRateResultDto> GetAllLevelRateForPropertyBaseRate(DateTime initialDate, DateTime endDate, Guid? currencyId)
        {
            ValidateDates(initialDate, endDate);

            if (Notification.HasNotification()) return null;

            var propertyBaseRateByLevelRateResultList = _levelRateHeaderReadRepository.GetAllDtoForPropertyBaseRate(initialDate.Date, endDate.Date, currencyId);

            return new ListDto<PropertyBaseRateByLevelRateResultDto>
            {
                HasNext = false,
                Items = propertyBaseRateByLevelRateResultList.ToList()
            };
        }

        private bool LevelIsAvailable(DateTime initialDate, DateTime endDate, Guid currencyId, Guid levelId, Guid levelHeaderId)
                    => _levelRateHeaderReadRepository.LevelIsAvailable(initialDate, endDate, currencyId, levelId, levelHeaderId, int.Parse(_applicationUser.PropertyId));

        private void InsertLevelRates(LevelRateResultDto dto, Guid levelRateHeaderId)
        {
            var levelRateList = new List<LevelRate>();
            if(dto.LevelRateHeader.PropertyMealPlanTypeRate.HasValue && dto.LevelRateHeader.PropertyMealPlanTypeRate.Value)
            {
                dto.LevelRateMealPlanTypeList = new List<LevelRateMealPlanTypeDto>();
                var mealPlanTypeRateList = _propertyMealPlanTypeRateReadRepository.GetAllEntity(new GetAllPropertyMealPlanTypeRateDto { StartDate = dto.LevelRateHeader.InitialDate, EndDate = dto.LevelRateHeader.EndDate, CurrencyId = dto.LevelRateHeader.CurrencyId });

                dto.LevelRateMealPlanTypeList.Add(new LevelRateMealPlanTypeDto
                {
                    MealPlanTypeId = (int)MealPlanTypeEnum.None,
                    IsMealPlanTypeDefault = true
                });

                foreach (var mealPlanTypeId in mealPlanTypeRateList.Select(mpt => mpt.MealPlanTypeId).Distinct())
                {
                    if(!dto.LevelRateMealPlanTypeList.Any(lrmpt => lrmpt.MealPlanTypeId == mealPlanTypeId))
                    {
                        dto.LevelRateMealPlanTypeList.Add(new LevelRateMealPlanTypeDto
                        {
                            MealPlanTypeId = mealPlanTypeId,
                            IsMealPlanTypeDefault = false
                        });
                    }
                }
            }

            foreach (var levelRateMealPlanType in dto.LevelRateMealPlanTypeList)
            {
                var mealPlanTypeDefault = dto.LevelRateMealPlanTypeList.FirstOrDefault(lrmpt => lrmpt.IsMealPlanTypeDefault);

                foreach (var levelRateDto in dto.LevelRateList)
                {
                    levelRateDto.CurrencyId = dto.LevelRateHeader.CurrencyId;
                    levelRateDto.LevelRateHeaderId = levelRateHeaderId;
                    levelRateDto.MealPlanTypeId = levelRateMealPlanType.MealPlanTypeId;
                    levelRateDto.MealPlanTypeDefault = mealPlanTypeDefault.MealPlanTypeId;
                    levelRateDto.AdultMealPlanAmount = levelRateMealPlanType.AdultMealPlanAmount;
                    levelRateDto.Child1MealPlanAmount = levelRateMealPlanType.Child1MealPlanAmount;
                    levelRateDto.Child2MealPlanAmount = levelRateMealPlanType.Child2MealPlanAmount;
                    levelRateDto.Child3MealPlanAmount = levelRateMealPlanType.Child3MealPlanAmount;

                    levelRateList.Add(LevelRateAdapter.Map(levelRateDto).Build());
                }
            }

            if (Notification.HasNotification()) return;

            _levelRateRepository.AddRangeAndSaveChanges(levelRateList);
        }

        private void UpdateLevelRates(LevelRateResultDto dto, Guid levelRateHeaderId)
        {
            _levelRateRepository.RemoveRangeByLevelRateHeaderIdAndSaveChanges(levelRateHeaderId);

            InsertLevelRates(dto, levelRateHeaderId);
        }

        private void ValidateDates(DateTime initialDate, DateTime endDate)
        {
            if (endDate.Date < initialDate.Date)
            {
                Notification.Raise(Notification.DefaultBuilder
                 .WithMessage(AppConsts.LocalizationSourceName, LevelRateHeader.EntityError.EndDateCanNotBeLessThanInitialDate)
                 .WithDetailedMessage(AppConsts.LocalizationSourceName, LevelRateHeader.EntityError.EndDateCanNotBeLessThanInitialDate)
                 .Build());
            }
        }

        private void ValidateLevelIsAvailable(LevelRateHeaderDto dto)
        {
            if (!_levelRateHeaderReadRepository.LevelIsAvailable(dto.InitialDate, dto.EndDate, dto.CurrencyId,
                                                dto.LevelId, dto.Id, int.Parse(_applicationUser.PropertyId)))
            {
                Notification.Raise(Notification.DefaultBuilder
                 .WithMessage(AppConsts.LocalizationSourceName, LevelRateHeader.EntityError.AlreadyExistsActiveLevelRate)
                 .WithDetailedMessage(AppConsts.LocalizationSourceName, LevelRateHeader.EntityError.AlreadyExistsActiveLevelRate)
                 .Build());
            }
        }

        private void ValidateIfCanUpdate(Guid levelRateHeaderId)
        {
            if (_levelRateHeaderReadRepository.LevelRateAlreadyUsed(levelRateHeaderId))
            {
                Notification.Raise(Notification.DefaultBuilder
                  .WithMessage(AppConsts.LocalizationSourceName, LevelRateHeader.EntityError.LevelRateHeaderCanNotBeUpdatedBecauseAlreadyUsed)
                  .WithDetailedMessage(AppConsts.LocalizationSourceName, LevelRateHeader.EntityError.LevelRateHeaderCanNotBeUpdatedBecauseAlreadyUsed)
                  .Build());
            }
        }

        private void ValidateCreateLevelRate(LevelRateResultDto dto)
        {
            ValidateDto<LevelRateResultDto>(dto, nameof(dto));

            if (Notification.HasNotification()) return;

            if (dto.LevelRateList == null || !dto.LevelRateList.Any() || (dto.LevelRateHeader.PropertyMealPlanTypeRate.HasValue && !dto.LevelRateHeader.PropertyMealPlanTypeRate.Value && (dto.LevelRateMealPlanTypeList == null || !dto.LevelRateMealPlanTypeList.Any())))
            {
                NotifyNullParameter();
                return;
            }


            if (Notification.HasNotification()) return;

            ValidateDates(dto.LevelRateHeader.InitialDate, dto.LevelRateHeader.EndDate);

            if (Notification.HasNotification()) return;

            ValidateLevelIsAvailable(dto.LevelRateHeader);

            if (Notification.HasNotification()) return;

            ValidateRoomTypeList(dto.LevelRateList);

            if (Notification.HasNotification()) return;

            if (!dto.LevelRateHeader.PropertyMealPlanTypeRate.HasValue || !dto.LevelRateHeader.PropertyMealPlanTypeRate.Value)
                ValidateMealPlanTypeList(dto.LevelRateMealPlanTypeList);
            else
                ValidatePropertyMealPlanTypeRate(dto);
        }

        private void ValidateUpdateLevelRate(LevelRateResultDto dto, LevelRateHeader entity)
        {
            ValidateDto<LevelRateResultDto>(dto, nameof(dto));

            if (Notification.HasNotification()) return;

            if (dto.LevelRateList == null || !dto.LevelRateList.Any() || (dto.LevelRateHeader.PropertyMealPlanTypeRate.HasValue && !dto.LevelRateHeader.PropertyMealPlanTypeRate.Value && (dto.LevelRateMealPlanTypeList == null || !dto.LevelRateMealPlanTypeList.Any())))
            {
                NotifyNullParameter();
                return;
            }

            if (Notification.HasNotification()) return;

            ValidateDates(dto.LevelRateHeader.InitialDate, dto.LevelRateHeader.EndDate);

            if (Notification.HasNotification()) return;

            if (entity == null)
            {
                NotifyNullParameter();
                return;
            }

            ValidatePermission(entity.TenantId, _applicationUser.TenantId);

            if (Notification.HasNotification()) return;

            ValidateIfCanUpdate(entity.Id);

            if (Notification.HasNotification()) return;

            ValidateLevelIsAvailable(dto.LevelRateHeader);

            if (Notification.HasNotification()) return;

            ValidateRoomTypeList(dto.LevelRateList);

            if (Notification.HasNotification()) return;

            if (!dto.LevelRateHeader.PropertyMealPlanTypeRate.HasValue || !dto.LevelRateHeader.PropertyMealPlanTypeRate.Value)
                ValidateMealPlanTypeList(dto.LevelRateMealPlanTypeList);
            else
                ValidatePropertyMealPlanTypeRate(dto);
        }

        private void ValidateRoomTypeList(ICollection<LevelRateDto> levelRateList)
        {
            var roomTypeList = _roomTypeReadRepository.GetAllRoomTypesByPropertyIdAsync(int.Parse(_applicationUser.PropertyId)).Result;

            ValidateAdultAmount(levelRateList, roomTypeList);
        }

        private void ValidateAdultAmount(ICollection<LevelRateDto> levelRateList, IListDto<RoomTypeDto> roomTypeList)
        {
            foreach (var roomType in roomTypeList.Items)
            {
                var levelRateListByRoomType = levelRateList.Where(lr => lr.RoomTypeId == roomType.Id);
                for (int i = 1; i <= (roomType.AdultCapacity > 5 ? 5 : roomType.AdultCapacity); i++)
                {
                    if (levelRateListByRoomType.Any(lr => lr[$"Pax{i}Amount"] == null)|| (roomType.AdultCapacity > 5 && levelRateListByRoomType.Any(lr => lr.PaxAdditionalAmount == null)))
                    {
                        Notification.Raise(Notification.DefaultBuilder
                         .WithMessage(AppConsts.LocalizationSourceName, LevelRate.EntityError.LevelRateAdultAmountEmpty)
                         .WithDetailedMessage(AppConsts.LocalizationSourceName, LevelRate.EntityError.LevelRateAdultAmountEmpty)
                         .Build());

                        return;
                    }

                    if (levelRateListByRoomType.Any(lr => (decimal?)lr[$"Pax{i}Amount"] < roomType.MinimumRate || (decimal?)lr[$"Pax{i}Amount"] > roomType.MaximumRate))
                    {
                        Notification.Raise(Notification.DefaultBuilder
                         .WithMessage(AppConsts.LocalizationSourceName, LevelRate.EntityError.LevelRateAdultAmountInvalid)
                         .WithDetailedMessage(AppConsts.LocalizationSourceName, LevelRate.EntityError.LevelRateAdultAmountInvalid)
                         .Build());

                        return;

                    }
                }
            }
        }

        private void ValidateChildAmount(ICollection<LevelRateDto> levelRateList, IListDto<RoomTypeDto> roomTypeList)
        {
            foreach (var roomType in roomTypeList.Items)
            {
                var levelRateListByRoomType = levelRateList.Where(lr => lr.RoomTypeId == roomType.Id);
                for (int i = 1; i <= roomType.ChildCapacity; i++)
                {
                    if (levelRateListByRoomType.Any(lr => lr[$"Child{i}Amount"] == null))
                    {
                        Notification.Raise(Notification.DefaultBuilder
                         .WithMessage(AppConsts.LocalizationSourceName, LevelRate.EntityError.LevelRateChildAmountEmpty)
                         .WithDetailedMessage(AppConsts.LocalizationSourceName, LevelRate.EntityError.LevelRateChildAmountEmpty)
                         .Build());

                        return;
                    }
                }
            }
        }

        private void ValidateMealPlanTypeList(ICollection<LevelRateMealPlanTypeDto> levelRateMealPlanTypeList)
        {
            var mealPlanTypeIdList = _propertyMealPlanTypeReadRepository.GetAllMealPlanTypeIdListByPropertyId(int.Parse(_applicationUser.PropertyId));

            foreach (var mealPlanTypeId in mealPlanTypeIdList)
            {
                var levelRateListByMealPlanType = levelRateMealPlanTypeList.Where(lr => lr.MealPlanTypeId == mealPlanTypeId);
                if (levelRateListByMealPlanType.Any(lr => !lr.AdultMealPlanAmount.HasValue))
                {
                    Notification.Raise(Notification.DefaultBuilder
                                         .WithMessage(AppConsts.LocalizationSourceName, LevelRate.EntityError.LevelRateMealPlanTypeEmpty)
                                         .WithDetailedMessage(AppConsts.LocalizationSourceName, LevelRate.EntityError.LevelRateMealPlanTypeEmpty)
                                         .Build());

                    return;
                }
            }
        }

        private void ValidatePropertyMealPlanTypeRate(LevelRateResultDto dto)
        {
            var rangeDates = GenerateRangeDate(dto.LevelRateHeader.InitialDate, dto.LevelRateHeader.EndDate);
            var existsPropertyMealPlanTypeRate = _propertyMealPlanTypeRateReadRepository.ExistsForAllPeriod(dto.LevelRateHeader.CurrencyId, int.Parse(_applicationUser.PropertyId), rangeDates); 
            if(!existsPropertyMealPlanTypeRate)
            {
                Notification.Raise(Notification.DefaultBuilder
                                         .WithMessage(AppConsts.LocalizationSourceName, PropertyMealPlanTypeRate.EntityError.NotExistsPropertyMealPlanTypeRateForAllPeriod)
                                         .WithDetailedMessage(AppConsts.LocalizationSourceName, PropertyMealPlanTypeRate.EntityError.NotExistsPropertyMealPlanTypeRateForAllPeriod)
                                         .Build());

                
            }
        }
    }
}
