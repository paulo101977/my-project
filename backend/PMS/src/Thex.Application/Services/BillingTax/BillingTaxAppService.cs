﻿using System;
using Thex.Application.Adapters;
using Thex.Domain.Entities;
using Thex.Application.Interfaces;
using Tnf.Domain.Services;
using Tnf.Notifications;

namespace Thex.Application.Services
{
    public class BillingTaxAppService : ScaffoldBillingTaxAppService, IBillingTaxAppService
    {
        public BillingTaxAppService(
            IBillingTaxAdapter billingTaxAdapter,
            IDomainService<BillingTax> billingTaxDomainService,
            INotificationHandler notificationHandler)
            : base(billingTaxAdapter, billingTaxDomainService, notificationHandler)
        {
        }
    }
}
