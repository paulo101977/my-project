﻿using System;
using Thex.Application.Adapters;
using Thex.Domain.Entities;
using Thex.Application.Interfaces;
using Tnf.Domain.Services;
using Tnf.Notifications;

namespace Thex.Application.Services
{
    public class AuditStepTypeAppService : ScaffoldAuditStepTypeAppService, IAuditStepTypeAppService
    {
        public AuditStepTypeAppService(
            IAuditStepTypeAdapter auditStepTypeAdapter,
            IDomainService<AuditStepType> auditStepTypeDomainService,
            INotificationHandler notificationHandler)
            : base(auditStepTypeAdapter, auditStepTypeDomainService, notificationHandler)
        {
        }
    }
}
