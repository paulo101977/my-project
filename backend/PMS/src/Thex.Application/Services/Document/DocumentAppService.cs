﻿using System;
using Thex.Application.Adapters;
using Thex.Domain.Entities;
using Thex.Application.Interfaces;
using Tnf.Domain.Services;

namespace Thex.Application.Services
{
    using System.Collections.Generic;
    using System.Linq;
    
    using Thex.Domain.Interfaces.Repositories;
    using Thex.Dto;
    using Thex.Infra.ReadInterfaces;
    using Tnf;
    using Tnf.Notifications;
    using Thex.Storage;


    public class DocumentAppService : ApplicationServiceBase, IDocumentAppService
    {
        protected readonly IDocumentRepository _documentRepository;
        protected readonly IDocumentReadRepository _documentReadRepository;
        protected readonly IAzureStorage _documentStorage;

        public DocumentAppService(
            IDocumentRepository documentRepository,
            IDocumentReadRepository documentReadRepository,
            IAzureStorage documentStorage,
            INotificationHandler notificationHandler)
               : base(notificationHandler)
        {
            _documentRepository = Check.NotNull(documentRepository, nameof(documentRepository));
            _documentReadRepository = documentReadRepository;
            _documentStorage = documentStorage;
        }

        public Document.Builder GetBuilder(DocumentDto documentDto)
        {
            var builder = new Document.Builder(Notification);

            builder
                .WithOwnerId(documentDto.OwnerId)
                .WithDocumentInformation(documentDto.DocumentInformation)
                .WithDocumentTypeId(documentDto.DocumentTypeId);

            return builder;
        }

        public void SaveDocumentList(ICollection<DocumentDto> documentListDto, Guid personId)
        {
            List<Document> documentList = new List<Document>();
            foreach (var documentDto in documentListDto)
            {
                documentDto.OwnerId = personId;
                documentList.Add(this.GetBuilder(documentDto).Build());
            }
            _documentRepository.AddRange(documentList);
        }

        public ICollection<Document> ToDomain(ICollection<DocumentDto> documents)
        {
            var documentList = new List<Document>();
            if (documents != null)
            {
                foreach (var documentDto in documents)
                {
                    var documentBuilder = this.GetBuilder(documentDto);
                    var document = documentBuilder.Build();
                    documentList.Add(document);
                }
            }
            return documentList;
        }

        public ICollection<Document> getDocumentListFromDto(ICollection<DocumentDto> documentDtoList, Guid personId)
        {
            IList<Document> documentList = new List<Document>();

            foreach (var documentDto in documentDtoList)
            {
                documentDto.OwnerId = personId;
                Document document = GetBuilder(documentDto).Build();
                documentList.Add(document);
            }

            return documentList;
        }

        public void UpdateDocumentList(IList<DocumentDto> DocumentDtoList, string ownerId)
        {
            var documentList = getDocumentListFromDto(DocumentDtoList, Guid.Parse(ownerId));

            _documentRepository.UpdateDocumentList(documentList.ToList(), Guid.Parse(ownerId));
        }

        public void CreateOrUpdateGuestRegistrationDocument(DocumentDto documentDto, string ownerId)
        {
            var document = GetDocumentForUpdated(documentDto, ownerId);

            if (Notification.HasNotification())
                return;

            _documentRepository.CreateOrUpdateDocumentGuestRegistration(document, Guid.Parse(ownerId));
        }

        public Document GetDocumentForUpdated(DocumentDto documentDto, string personId)
        {
            documentDto.OwnerId = Guid.Parse(personId);

            return GetBuilder(documentDto).Build();
        }

        public ICollection<DocumentDto> GetDocumentDtoListByPersonId(Guid owerId)
        {
            return _documentReadRepository.GetDocumentDtoListByPersonId(owerId);
        }

        public void SendDocumentToStorage(Guid pPropertyGuid, string pNameFolder, string pNameFile, Byte[] pValue)
        {
            _documentStorage.SendAsync(pPropertyGuid, pNameFolder, pNameFile, pValue);
        }

        public void UpdateDocumentPersonHeader(DocumentDto dto, Guid ownerId)
        {
            var documentExisting = _documentReadRepository.GetByOwnerId(ownerId);

            if (documentExisting != null)
            {
                _documentRepository.RemoveDocument(documentExisting);
                SaveDocumentList(new List<DocumentDto> { dto }, ownerId);
            }
        }
    }
}
