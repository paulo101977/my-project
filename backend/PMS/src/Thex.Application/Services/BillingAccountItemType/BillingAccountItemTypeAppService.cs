﻿using System;
using Thex.Application.Adapters;
using Thex.Domain.Entities;
using Thex.Application.Interfaces;
using Tnf.Domain.Services;
using Tnf.Notifications;

namespace Thex.Application.Services
{
    public class BillingAccountItemTypeAppService : ScaffoldBillingAccountItemTypeAppService, IBillingAccountItemTypeAppService
    {
        public BillingAccountItemTypeAppService(
            IBillingAccountItemTypeAdapter billingAccountItemTypeAdapter,
            IDomainService<BillingAccountItemType> billingAccountItemTypeDomainService,
            INotificationHandler notificationHandler)
            : base(billingAccountItemTypeAdapter, billingAccountItemTypeDomainService, notificationHandler)
        {
        }
    }
}
