﻿using System;
using Thex.Application.Adapters;
using Thex.Domain.Entities;
using Thex.Application.Interfaces;
using Tnf.Domain.Services;
using Tnf.Notifications;

namespace Thex.Application.Services
{
    public class LocationCategoryAppService : ScaffoldLocationCategoryAppService, ILocationCategoryAppService
    {
        public LocationCategoryAppService(
            ILocationCategoryAdapter locationCategoryAdapter,
            IDomainService<LocationCategory> locationCategoryDomainService,
            INotificationHandler notificationHandler)
            : base(locationCategoryAdapter, locationCategoryDomainService, notificationHandler)
        {
        }
    }
}
