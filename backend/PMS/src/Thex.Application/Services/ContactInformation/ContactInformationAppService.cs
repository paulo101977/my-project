﻿using System;
using Thex.Application.Adapters;
using Thex.Domain.Entities;
using Thex.Application.Interfaces;

using Thex.Dto;
using System.Collections.Generic;
using Thex.Domain.Interfaces.Repositories;
using Tnf.Notifications;

namespace Thex.Application.Services
{
    public class ContactInformationAppService : ApplicationServiceBase, IContactInformationAppService
    {
        private readonly IContactInformationRepository _contactInformationRepository;
        public ContactInformationAppService(IContactInformationRepository contactInformationRepository,
            INotificationHandler notificationHandler)
               : base(notificationHandler)
        {
            _contactInformationRepository = contactInformationRepository;
        }

        public ContactInformation.Builder GetBuilder(ContactInformationDto contactInformationDto)
        {
            var builder = new ContactInformation.Builder(Notification);

            builder
                .WithOwnerId(contactInformationDto.OwnerId)
                .WithContactInformationTypeId(contactInformationDto.ContactInformationTypeId)
                .WithInformation(contactInformationDto.Information);
            return builder;
        }

        public ICollection<ContactInformation> ToDomain(ICollection<ContactInformationDto> contactInformations)
        {
            var contactInformationList = new List<ContactInformation>();
            if (contactInformations != null)
            {
                foreach (var contactInformationDto in contactInformations)
                {
                    var contactInformationBuilder = this.GetBuilder(contactInformationDto);
                    var contactInformation = contactInformationBuilder.Build();
                    contactInformationList.Add(contactInformation);
                }
            }
            return contactInformationList;
        }

        public string GetEmailContactInformationByPersonId(Guid guid)
        {
          return  _contactInformationRepository.GetEmailContactInformationByPersonId(guid);
        }
    }
}
