﻿using System;
using Thex.Application.Adapters;
using Thex.Domain.Entities;
using Thex.Application.Interfaces;
using Tnf.Domain.Services;
using Tnf.Notifications;

namespace Thex.Application.Services
{
    public class CompanyContactPersonAppService : ScaffoldCompanyContactPersonAppService, ICompanyContactPersonAppService
    {
        public CompanyContactPersonAppService(
            ICompanyContactPersonAdapter companyContactPersonAdapter,
            IDomainService<CompanyContactPerson> companyContactPersonDomainService,
            INotificationHandler notificationHandler)
            : base(companyContactPersonAdapter, companyContactPersonDomainService, notificationHandler)
        {
        }
    }
}
