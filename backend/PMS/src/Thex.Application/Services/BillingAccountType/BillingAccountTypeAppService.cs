﻿using System;
using Thex.Application.Adapters;
using Thex.Domain.Entities;
using Thex.Application.Interfaces;
using Tnf.Domain.Services;
using Tnf.Notifications;

namespace Thex.Application.Services
{
    public class BillingAccountTypeAppService : ScaffoldBillingAccountTypeAppService, IBillingAccountTypeAppService
    {
        public BillingAccountTypeAppService(
            IBillingAccountTypeAdapter billingAccountTypeAdapter,
            IDomainService<BillingAccountType> billingAccountTypeDomainService,
            INotificationHandler notificationHandler)
            : base(billingAccountTypeAdapter, billingAccountTypeDomainService, notificationHandler)
        {
        }
    }
}
