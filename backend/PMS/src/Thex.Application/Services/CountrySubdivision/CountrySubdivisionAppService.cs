﻿using System;
using Thex.Application.Adapters;
using Thex.Domain.Entities;
using Thex.Application.Interfaces;
using Tnf.Domain.Services;
using Thex.Infra.ReadInterfaces;
using Thex.Dto;
using Tnf.Dto;
using Thex.Maps.Geocode.ReadInterfaces;
using System.Linq;

using Thex.Maps.Geocode.Enumerations;
using System.Collections.Generic;
using Thex.Maps.Geocode.Entities.GoogleAddress;
using System.Text;
using Thex.Common;

namespace Thex.Application.Services
{
    using Thex.Common.Enumerations;

    using Tnf.Localization;
    using Tnf.Notifications;

    public class CountrySubdivisionAppService : ScaffoldCountrySubdivisionAppService, ICountrySubdivisionAppService
    {
        private readonly ICountrySubdivisionTranslationReadRepository _countrySubdivisionTranslationReadRepository;
        private readonly IAddressMapsGeocodeReadRepository _addressMapsGeocodeReadRepository;
        private readonly IDomainService<CountrySubdivisionTranslation> _countrySubdivisionTranslationDomainService;
        private readonly ILocalizationManager _localizationManager;

        public CountrySubdivisionAppService(
            ICountrySubdivisionAdapter countrySubdivisionAdapter,
            IDomainService<CountrySubdivision> countrySubdivisionDomainService,
            IDomainService<CountrySubdivisionTranslation> countrySubdivisionTranslatorDomainService,
            ICountrySubdivisionTranslationReadRepository countrySubdivisionTranslationReadRepository,
            IAddressMapsGeocodeReadRepository addressMapsGeocodeReadRepository,
            ILocalizationManager localizationManager,
            INotificationHandler notificationHandler)
            : base(countrySubdivisionAdapter, countrySubdivisionDomainService, notificationHandler)
        {
            _countrySubdivisionTranslationReadRepository = countrySubdivisionTranslationReadRepository;
            _addressMapsGeocodeReadRepository = addressMapsGeocodeReadRepository;
            _countrySubdivisionTranslationDomainService = countrySubdivisionTranslatorDomainService;
            _localizationManager = localizationManager;
        }

        public virtual IListDto<CountryDto> GetAllCountriesByLanguageIsoCode(GetAllCountryDto request)
        {
            ValidateRequestAllDto(request, nameof(request));

            if (string.IsNullOrEmpty(request.LanguageIsoCode))
                NotifyNullParameter();

            if (Notification.HasNotification())
                return null;

            return _countrySubdivisionTranslationReadRepository.GetAllCountriesByLanguageIsoCode(request);
        }

        public virtual (int?, int?, int?) CreateCountrySubdivisionTranslateAndGetCityId(LocationDto location, string language)
        {
            //validar country code obrigatorio
            if (string.IsNullOrEmpty(location.CountryCode))
            {
                Notification.Raise(Notification
                    .DefaultBuilder
                    .WithMessage(AppConsts.LocalizationSourceName, CountrySubdivisionTranslation.EntityError.CountrySubdivisionTranslationMustHaveCountryCode)
                    .Build());
                return (null,null,null);
            }

            language = language.ToLower();
            var locationCountryCode = location.CountryCode.ToLower();
            var searchLanguages = new List<string>
                {
                    { "en-us" },
                    { "pt-br" }
                };

            //se o countrycode == br então linguagem principal = brasil e secondLanguage vira a estados unidos
            if (locationCountryCode == "br")
                searchLanguages.Reverse();

            var address = ParseAddress(location);

            //validar se está dentro das linguagens permitidas do google 
            if (!_addressMapsGeocodeReadRepository.GetValidLanguages().Any(l => l.ToLower() == language))
            {
                Notification.Raise(Notification
                    .DefaultBuilder
                    .WithMessage(AppConsts.LocalizationSourceName, CountrySubdivisionTranslation.EntityError.CountrySubdivisionTranslationInvalidLanguage)
                    .Build());
                return (null, null, null);
            }

            //passar a região tb
            var task = _addressMapsGeocodeReadRepository.GetMapsGeocodeResultByAddress(address, language);

            var result = task.Result;

            if (location.CountryCode.Equals("BR") && result == null)
            {
                Notification.Raise(Notification
                    .DefaultBuilder
                    .WithMessage(AppConsts.LocalizationSourceName, CountrySubdivisionTranslation.EntityError.CountrySubdivisionTranslationGeocodeApiReturnError)
                    .Build());
                return (null, null, null);
            }

            if (location.CountryCode.Equals("BR") && result.Count() == 0) 
            {
                Notification.Raise(Notification
                    .DefaultBuilder
                    .WithMessage(AppConsts.LocalizationSourceName, CountrySubdivisionTranslation.EntityError.CountrySubdivisionTranslationGeocodeApiReturnZeroResults)
                    .Build());
                return (null, null, null);
            }

            //varrer cada resultado e pegar a melhor escolha
            var geocodeUserSearch = result.FirstOrDefault();
            var geocodeUserPlaceIdSearch = geocodeUserSearch?.PlaceId;

            int? countryId = null;
            int? stateId = null;
            int? cityId = null;

            //tradução principal            
            if (!string.IsNullOrWhiteSpace(geocodeUserPlaceIdSearch))
            {
                var principalGeoCodeResult = searchLanguages.First() == language ? geocodeUserSearch : _addressMapsGeocodeReadRepository.GetMapsGeocodeResultByPlaceId(geocodeUserPlaceIdSearch, searchLanguages.First()).Result;
                var principalGeoCodeIds = CreateCountrySubdivisionDefaultLanguage(principalGeoCodeResult, searchLanguages.First(), locationCountryCode);

                if (Notification.HasNotification())
                    return (null, null, null);

                countryId = principalGeoCodeIds.Where(exp => exp.Key == "country").FirstOrDefault().Value;
                stateId = principalGeoCodeIds.Where(exp => exp.Key == "state").FirstOrDefault().Value;
                cityId = principalGeoCodeIds.Where(exp => exp.Key == "city").FirstOrDefault().Value;


                //tradução da 'property'
                var secondGeoCodeResult = searchLanguages.Last() == language ? geocodeUserSearch : _addressMapsGeocodeReadRepository.GetMapsGeocodeResultByPlaceId(geocodeUserPlaceIdSearch, searchLanguages.Last()).Result;
                CreateCountrySubdivisionTranslate(secondGeoCodeResult, searchLanguages.Last(), locationCountryCode, countryId ?? 0, stateId ?? 0, cityId ?? 0);

                //tradução do usuário somente se linguagem passada NÃO está entre uma das principais
                if (!searchLanguages.Contains(language))
                    CreateCountrySubdivisionTranslate(geocodeUserSearch, language, locationCountryCode, countryId ?? 0, stateId ?? 0, cityId ?? 0);
            }        

            return (cityId, stateId, countryId);
        }

        private string ParseAddress(LocationDto location)
        {
            var completeAddress = new StringBuilder();

            completeAddress.Append($"{location.StreetName}, {location.StreetNumber}");

            if (!string.IsNullOrEmpty(location.Neighborhood))
                completeAddress.Append($" - {location.Neighborhood}");

            if (!string.IsNullOrEmpty(location.Subdivision))
                completeAddress.Append($", {location.Subdivision}");

            if (!string.IsNullOrEmpty(location.Division))
                completeAddress.Append($" - {location.Division}");

            completeAddress.Append($", {location.PostalCode}");

            if (!string.IsNullOrEmpty(location.Country))
                completeAddress.Append($", {location.Country}");

            return completeAddress.ToString();
        }

        private void CreateCountrySubdivisionTranslate(GoogleAddress googleAddress, string language, string countryIsoCode, int countryId, int stateId, int cityId)
        {
            var levelsIds = new Dictionary<string, int>();

            language = language.ToLower();

            var components = googleAddress.Components;

            #region Get Address Component Levels

            var countryConfig = _addressMapsGeocodeReadRepository.GetAddressComponentConfigByCountryTwoLetterIsoCode(countryIsoCode, components);

            var country = components.FirstOrDefault(exp => exp.Types.Any(d => d == countryConfig.TypeLevel));
            var state = components.FirstOrDefault(exp => exp.Types.Any(d => d == countryConfig.ChildrenLevel.TypeLevel));
            var city = components.FirstOrDefault(exp => exp.Types.Any(d => d == countryConfig.ChildrenLevel.ChildrenLevel?.TypeLevel));

            #endregion

            #region Country 

            if (country != null)
            {
                var countryDbResults = _countrySubdivisionTranslationReadRepository.GetCountryWithTranslations(country.ShortName);
                CountrySubdivisionLanguageProcess(levelsIds, language, country, countryDbResults, 1, "country");
            }

            #endregion

            #region State

            if (state != null)
            {
                var stateDbResults = _countrySubdivisionTranslationReadRepository.GetStateWithTranslations(state.LongName, levelsIds.First(exp => exp.Key == "country").Value);

                if(stateDbResults.Count == 0)
                    stateDbResults = _countrySubdivisionTranslationReadRepository.GetStateWithTranslationsByCode(state.ShortName, levelsIds.First(exp => exp.Key == "country").Value);

                if (countryId.Equals(1) && stateDbResults.Count == 0)
                {                    
                        Notification.Raise(Notification
                            .DefaultBuilder
                            .WithMessage(AppConsts.LocalizationSourceName, CountrySubdivisionTranslation.EntityError.CountrySubdivisionTranslationStateNameNotEqualGeocodeStateShortName)
                            .Build());
                }

                CountrySubdivisionLanguageProcess(levelsIds, language, state, stateDbResults, 2, "state");
            }

            #endregion

            #region City

            if (city != null)
            {
                var cityDbResults = _countrySubdivisionTranslationReadRepository.GetCityWithTranslations(city.LongName, levelsIds.FirstOrDefault(exp => exp.Key == "state").Value);
                CountrySubdivisionLanguageProcess(levelsIds, language, city, cityDbResults, 3, "city");
            }

            #endregion

        }

        private Dictionary<string, int> CreateCountrySubdivisionDefaultLanguage(GoogleAddress googleAddress, string language, string countryIsoCode)
        {
            var levelsIds = new Dictionary<string, int>();

            var components = googleAddress.Components;

            #region Get Address Component Levels

            //preparado para efetuar de forma genérica
            var countryConfig = _addressMapsGeocodeReadRepository.GetAddressComponentConfigByCountryTwoLetterIsoCode(countryIsoCode, components);
                    
            var country = components.FirstOrDefault(exp => exp.Types.Any(d => d == countryConfig.TypeLevel));
            var state = components.FirstOrDefault(exp => exp.Types.Any(d => d == countryConfig.ChildrenLevel.TypeLevel));
            var city = components.FirstOrDefault(exp => exp.Types.Any(d => d == countryConfig.ChildrenLevel.ChildrenLevel?.TypeLevel));

            #endregion

            //validar se o countryIsoCode é diferente do resultado!
            if (countryIsoCode.ToLower() != country.ShortName.ToLower())
            {
                Notification.Raise(Notification
                    .DefaultBuilder
                    .WithMessage(AppConsts.LocalizationSourceName, CountrySubdivisionTranslation.EntityError.CountrySubdivisionTranslationCountryCodeNotEqualGeocodeCountryShortName)
                    .Build());
                return null;
            }

            #region Country 

            if (country != null)
            {
                var countryDbResults = _countrySubdivisionTranslationReadRepository.GetCountryWithTranslations(country.ShortName);
                CountrySubdivisionProcess(levelsIds, language, country, countryDbResults, 1, "country");
            }

            #endregion

            #region State

            if (state != null)
            {
                var stateDbResults = _countrySubdivisionTranslationReadRepository.GetStateWithTranslations(state.LongName, levelsIds.First(exp => exp.Key == "country").Value);
                CountrySubdivisionProcess(levelsIds, language, state, stateDbResults, 2, "state", countryIsoCode);
            }

            #endregion

            #region City

            if (city != null)
            {
                var cityDbResults = _countrySubdivisionTranslationReadRepository.GetCityWithTranslations(city.LongName, levelsIds.First(exp => exp.Key == "state").Value);
                CountrySubdivisionProcess(levelsIds, language, city, cityDbResults, 3, "city", countryIsoCode);
            }

            #endregion



            return levelsIds;
        }

        private void CountrySubdivisionLanguageProcess(Dictionary<string, int> levelsIds, string language, GoogleAddressComponent component, List<BaseCountrySubdvisionTranslationDto> countryDbResults, int countrySubdivisionType, string countrySubdivisionLevel)
        {
            var countryDbResult = countryDbResults.FirstOrDefault();
            if (countryDbResult != null)
            {
                if (!countryDbResults.Any(exp => exp.Name.ToLower() == component.LongName.ToLower() && exp.LanguageIsoCode.ToLower() == language))
                    CreateCountrySubdivisionTranslation(language, countryDbResult.Id, component.LongName);

                SetCountrySubdivisionLevelsIds(levelsIds, countrySubdivisionLevel, countryDbResult.Id);
            }
        }

        private void CountrySubdivisionProcess(Dictionary<string, int> levelsIds, string language, GoogleAddressComponent component, List<BaseCountrySubdvisionTranslationDto> countryDbResults, int countrySubdivisionType, string countrySubdivisionLevel, string countryTwoLetterIsoCode = null)
        {
            if (countryDbResults == null || countryDbResults.Count == 0)
                CreateCountrySubdivision(levelsIds, language, component, countrySubdivisionLevel, countrySubdivisionType, countryTwoLetterIsoCode);
            else
            {
                var countryDbResult = countryDbResults.FirstOrDefault();
                if (countryDbResult != null)
                {

                    if (!countryDbResults.Any(exp => exp.Name.ToLower() == component.LongName.ToLower() && exp.LanguageIsoCode.ToLower() == language))
                        CreateCountrySubdivisionTranslation(language, countryDbResult.Id, component.LongName);

                    SetCountrySubdivisionLevelsIds(levelsIds, countrySubdivisionLevel, countryDbResult.Id);
                }
            }
        }

        private void CreateCountrySubdivision(Dictionary<string, int> levelsIds, string language, GoogleAddressComponent component, string countrySubdivisionLevel, int countrySubdivisionType, string countryTwoLetterIsoCode = null)
        {
            var countryBuilder = GetCountrySubdivisionBuilder(levelsIds, language, component, countrySubdivisionLevel, countrySubdivisionType, countryTwoLetterIsoCode);

            var countrySubdivisionId = CountrySubdivisionDomainService.InsertAndSaveChanges(countryBuilder).Id;

            CreateCountrySubdivisionTranslation(language, countrySubdivisionId, component.LongName);

            SetCountrySubdivisionLevelsIds(levelsIds, countrySubdivisionLevel, countrySubdivisionId);
        }

        private void SetCountrySubdivisionLevelsIds(Dictionary<string, int> levelsIds, string countrySubdivisionLevel, int countrySubdivisionId)
        {
            levelsIds.Add(countrySubdivisionLevel, countrySubdivisionId);
        }

        private CountrySubdivision.Builder GetCountrySubdivisionBuilder(Dictionary<string, int> ret, string language, GoogleAddressComponent component, string countrySubdivisionLevel, int countrySubdivisionType, string countryTwoLetterIsoCode = null)
        {
            var countryBuilder = new CountrySubdivision.Builder(Notification);

            countryBuilder
                .WithCountryTwoLetterIsoCode(countryTwoLetterIsoCode != null ? countryTwoLetterIsoCode.ToUpper() : component.ShortName.ToUpper())
                .WithSubdivisionTypeId(countrySubdivisionType);

            switch (countrySubdivisionLevel)
            {
                case "country":
                    countryBuilder.WithTwoLetterIsoCode(component.ShortName.ToUpper());
                    break;
                case "state":
                    countryBuilder.WithParentSubdivisionId(ret.First(x => x.Key == "country").Value);
                    //fix states brazil
                    if(language.ToLower() == "pt-br")
                        countryBuilder.WithTwoLetterIsoCode(component.ShortName.ToUpper());
                    break;
                case "city":
                    countryBuilder.WithParentSubdivisionId(ret.First(x => x.Key == "state").Value);
                    break;
            }

            return countryBuilder;
        }

        private void CreateCountrySubdivisionTranslation(string language, int countrySubdivisionId, string longName)
        {
            CountrySubdivisionTranslation.Builder countryTranslationBuilder = GetCountrySubdivisionTranslationBuilder(language, countrySubdivisionId, longName);

            _countrySubdivisionTranslationDomainService.InsertAndSaveChanges(countryTranslationBuilder);
        }

        private CountrySubdivisionTranslation.Builder GetCountrySubdivisionTranslationBuilder(string language, int countrySubdivisionId, string longName)
        {
            var countryTranslationBuilder = new CountrySubdivisionTranslation.Builder(Notification);

            countryTranslationBuilder
                .WithCountrySubdivisionId(countrySubdivisionId)
                .WithLanguageIsoCode(language)
                .WithName(longName);
            return countryTranslationBuilder;
        }
    }
}
