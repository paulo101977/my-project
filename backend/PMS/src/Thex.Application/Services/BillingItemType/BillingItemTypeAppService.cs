﻿using System;
using Thex.Application.Adapters;
using Thex.Domain.Entities;
using Thex.Application.Interfaces;
using Tnf.Domain.Services;
using Tnf.Notifications;

namespace Thex.Application.Services
{
    public class BillingItemTypeAppService : ScaffoldBillingItemTypeAppService, IBillingItemTypeAppService
    {
        public BillingItemTypeAppService(
            IBillingItemTypeAdapter billingItemTypeAdapter,
            IDomainService<BillingItemType> billingItemTypeDomainService,
            INotificationHandler notificationHandler)
            : base(billingItemTypeAdapter, billingItemTypeDomainService, notificationHandler)
        {
        }
    }
}
