﻿using System;
using Thex.Application.Adapters;
using Thex.Domain.Entities;
using Thex.Application.Interfaces;
using Tnf.Domain.Services;

using Thex.Dto;
using Thex.Dto.BillingItem;
using Thex.Common.Enumerations;

using Thex.Infra.ReadInterfaces;
using System.Collections.Generic;
using Tnf.Dto;
using System.Threading.Tasks;
using Thex.Domain.Interfaces.Repositories;
using System.Linq;
using Thex.Common;
using System.Linq.Expressions;
using Tnf.Localization;
using Tnf.Notifications;
using Tnf.Repositories.Uow;
using Thex.Kernel;

namespace Thex.Application.Services
{
    public class BillingItemAppService : ScaffoldBillingItemAppService, IBillingItemAppService
    {
        private readonly IBillingTaxAppService _billingTaxAppService;
        private readonly IBillingItemReadRepository _billingItemReadRepository;
        private readonly IBillingItemRepository _billingItemRepository;
        private readonly IBillingTaxRepository _billingTaxRepository;
        private readonly IBillingTaxAdapter _billingTaxAdapter;
        private readonly IBillingItemPaymentConditionAdapter _billingItemPaymentCondition;
        private readonly IDomainService<BillingItemPaymentCondition> _billingItemPaymentConditionDomainService;
        private readonly IPaymentTypeAppService _paymentTypeAppService;
        private readonly ILocalizationManager _localizationManager;
        private readonly IApplicationUser _applicationUser;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly ITourismTaxReadRepository _tourismTaxReadRepository;
        private readonly IPropertyParameterReadRepository _propertyParameterReadRepository;

        public BillingItemAppService(
           IUnitOfWorkManager unitOfWorkManager,
           IBillingItemAdapter billingItemAdapter,
           IDomainService<BillingItem> billingItemDomainService,
           IBillingTaxAppService billingTaxAppService,
           IBillingItemReadRepository billingItemReadRepository,
           IBillingItemRepository billingItemRepository,
           IBillingTaxRepository billingTaxRepository,
           IBillingTaxAdapter billingTaxAdapter,
           IBillingItemPaymentConditionAdapter billingItemPaymentCondition,
           IDomainService<BillingItemPaymentCondition> billingItemPaymentConditionDomainService,
           IPaymentTypeAppService paymentTypeAppService,
           IPropertyParameterReadRepository propertyParameterReadRepository,
           ILocalizationManager localizationManager,
           IApplicationUser applicationUser,
           INotificationHandler notificationHandler,
           ITourismTaxReadRepository tourismTaxReadRepository)
           : base(billingItemAdapter, billingItemDomainService, notificationHandler)
        {
            _unitOfWorkManager = unitOfWorkManager;
            _billingTaxAppService = billingTaxAppService;
            _billingItemReadRepository = billingItemReadRepository;
            _billingItemRepository = billingItemRepository;
            _billingTaxRepository = billingTaxRepository;
            _billingTaxAdapter = billingTaxAdapter;
            _billingItemPaymentCondition = billingItemPaymentCondition;
            _billingItemPaymentConditionDomainService = billingItemPaymentConditionDomainService;
            _paymentTypeAppService = paymentTypeAppService;
            _localizationManager = localizationManager;
            _applicationUser = applicationUser;
            _tourismTaxReadRepository = tourismTaxReadRepository;
            _propertyParameterReadRepository = propertyParameterReadRepository;
        }

        #region BillingItem

        public void Delete(int id, long propertyId)
        {
            using (var uow = _unitOfWorkManager.Begin())
            {
                var response = GetBillingItem(id, propertyId);

                if (response != null)
                {
                    foreach (var tax in response.ServiceAndTaxList)
                    {
                        _billingTaxRepository.Delete(tax.Id.Value);
                    }
                    _billingItemRepository.DeleteBillingItemService(id);
                }

                uow.Complete();
            }
        }

        public void CreateServiceAssociateTax(BillingItemServiceDto dto)
        {
            using (var uow = _unitOfWorkManager.Begin())
            {
                ValidateActiveForService(dto.ServiceAndTaxList);

                if (dto.PropertyId == 0)
                    NotifyParameterInvalid();

                if (Notification.HasNotification())
                    return;

                if (!string.IsNullOrWhiteSpace(dto.IntegrationCode))
                    ValidateIntegrationCode(dto.IntegrationCode, dto.Id);

                if (Notification.HasNotification()) return;

                var billingService = new BillingItemDto()
                {
                    BillingItemName = dto.Name,
                    IsActive = dto.IsActive,
                    BillingItemTypeId = (int)BillingItemTypeEnum.Service,
                    BillingItemCategoryId = dto.BillingItemCategoryId,
                    PropertyId = dto.PropertyId,
                    IntegrationCode = dto.IntegrationCode
                };

                var billingItemId = CreateBillingItemAndGetId(billingService);

                if (Notification.HasNotification())
                    return;

                if (dto.ServiceAndTaxList.Count() > 1)
                    ValidateTaxEqualsForServiceWithTax(dto.ServiceAndTaxList);

                if (Notification.HasNotification())
                    return;

                foreach (var tax in dto.ServiceAndTaxList)
                {
                    var taxBilling = new BillingTaxDto()
                    {
                        Id = billingItemId > 0 ? Guid.NewGuid() : Guid.Empty,
                        IsActive = tax.IsActive,
                        BeginDate = tax.BeginDate,
                        EndDate = tax.EndDate,
                        TaxPercentage = tax.TaxPercentage,
                        BillingItemId = billingItemId,
                        BillingItemTaxId = tax.BillingItemTaxId.Value
                    };

                    BillingTax billingTax = _billingTaxAdapter.Map(taxBilling).Build();
                    _billingTaxRepository.Create(billingTax);
                }

                uow.Complete();
            }
        }

        public void ValidateTaxEqualsForServiceWithTax(List<BillingItemServiceAndTaxAssociateDto> dto)
        {
            bool erro = false;
            foreach (var tax in dto)
            {
                if (erro) break;
                if (tax.BillingItemTaxId.HasValue)
                {
                    if (!dto.Any(x => x.BillingItemTaxId == tax.BillingItemTaxId && x.Identify != tax.Identify))
                        continue;

                    foreach (var item in dto.Where(x => x.BillingItemTaxId == tax.BillingItemTaxId.Value && x.Identify != tax.Identify))
                    {

                        if (item.BeginDate.Date <= (tax.EndDate.HasValue ? tax.EndDate.Value.Date : DateTime.MaxValue) &&
                            (item.EndDate.HasValue ? item.EndDate.Value.Date : DateTime.MaxValue) >= tax.BeginDate.Date)
                        {
                            ErrorValidateTaxEquals();
                            erro = true;
                            break;
                        }
                        else continue;
                    }
                }
            }

        }

        private void ValidateActiveForTax(List<BillingItemServiceAndTaxAssociateDto> dto)
        {
            if (dto != null && dto.Count() > 0)
            {
                foreach (var tax in dto)
                {
                    if (_billingItemReadRepository.ValidateActiveForTax(tax.BillingItemServiceId.Value) && tax.IsActive)
                    {
                        Notification.Raise(Notification
                           .DefaultBuilder
                           .WithMessage(AppConsts.LocalizationSourceName, BillingItem.EntityError.BillingItemTaxDisable)
                           .Build());
                        break;
                    }
                }
            }
        }
        private void ValidateActiveForService(List<BillingItemServiceAndTaxAssociateDto> dto)
        {
            if (dto != null && dto.Count() > 0)
            {
                foreach (var tax in dto)
                {
                    if (_billingItemReadRepository.ValidateActiveForTax(tax.BillingItemTaxId.Value) && tax.IsActive)
                    {
                        Notification.Raise(Notification
                           .DefaultBuilder
                           .WithMessage(AppConsts.LocalizationSourceName, BillingItem.EntityError.BillingItemTaxDisable)
                           .Build());
                        break;
                    }
                }
            }
        }
        public void ValidateTaxEqualsForTaxWithService(List<BillingItemServiceAndTaxAssociateDto> dto)
        {
            bool erro = false;
            foreach (var tax in dto)
            {
                if (erro) break;
                if (tax.BillingItemServiceId.HasValue)
                {
                    if (!dto.Any(x => x.BillingItemServiceId == tax.BillingItemServiceId && x.Identify != tax.Identify))
                        continue;

                    foreach (var item in dto.Where(x => x.BillingItemServiceId == tax.BillingItemServiceId.Value && x.Identify != tax.Identify))
                    {
                        if (item.BeginDate.Date <= (tax.EndDate.HasValue ? tax.EndDate.Value.Date : DateTime.MaxValue) &&
                            (item.EndDate.HasValue ? item.EndDate.Value.Date : DateTime.MaxValue) >= tax.BeginDate.Date)
                        {
                            ErrorValidateTaxEquals();
                            erro = true;
                            break;
                        }
                        else continue;
                    }
                }
            }
        }
        public void ErrorValidateTaxEquals()
        {
            Notification.Raise(Notification
                   .DefaultBuilder
                   .WithMessage(AppConsts.LocalizationSourceName, BillingItem.EntityError.BillingItemAddNotPosibleWithTaxAndEffectiveDate)
                   .Build());
        }

        public int CreateBillingItemAndGetId(BillingItemDto billingItemDto)
        {
            ValidateDto<BillingItemDto>(billingItemDto, nameof(billingItemDto));

            var billingItemBuilder = BillingItemAdapter.Map(billingItemDto);

            return billingItemDto.Id = BillingItemDomainService.InsertAndSaveChanges(billingItemBuilder).Id;

        }

        public void CreateTaxAssociateService(BillingItemTaxDto dto)
        {
            using (var uow = _unitOfWorkManager.Begin())
            {
                ValidateActiveForTax(dto.ServiceAndTaxList);

                if (dto.PropertyId == 0)
                    NotifyParameterInvalid();

                if (Notification.HasNotification())
                    return;

                if (!string.IsNullOrWhiteSpace(dto.IntegrationCode))
                    ValidateIntegrationCode(dto.IntegrationCode, dto.Id);

                if (Notification.HasNotification()) return;

                var billingItemTax = new BillingItemDto()
                {
                    BillingItemName = dto.Name,
                    IsActive = dto.IsActive,
                    BillingItemTypeId = (int)BillingItemTypeEnum.Tax,
                    BillingItemCategoryId = dto.BillingItemCategoryId,
                    PropertyId = dto.PropertyId,
                    IntegrationCode = dto.IntegrationCode
                };

                var billingItemTaxId = CreateBillingItemAndGetId(billingItemTax);


                if (dto.ServiceAndTaxList.Count() > 1)
                    ValidateTaxEqualsForTaxWithService(dto.ServiceAndTaxList);

                if (Notification.HasNotification())
                    return;

                foreach (var service in dto.ServiceAndTaxList)
                {
                    var taxBilling = new BillingTaxDto()
                    {
                        Id = Guid.NewGuid(),
                        IsActive = service.IsActive,
                        BeginDate = service.BeginDate,
                        EndDate = service.EndDate,
                        TaxPercentage = service.TaxPercentage,
                        BillingItemId = service.BillingItemServiceId.Value,
                        BillingItemTaxId = billingItemTaxId,
                    };
                    _billingTaxAppService.Create(taxBilling).ConfigureAwait(false).GetAwaiter().GetResult();
                }

                uow.Complete();
            }
        }


        public BillingItemServiceDto GetBillingItem(long id, long propertyId)
        {
            return _billingItemReadRepository.GetBillingItem(id, propertyId);
        }

        public void UpdateBillingItemServiceWithTax(BillingItemServiceDto dto)
        {
            using (var uow = _unitOfWorkManager.Begin())
            {
                ValidateActiveForService(dto.ServiceAndTaxList);

                if (Notification.HasNotification())
                    return;

                if(!string.IsNullOrWhiteSpace(dto.IntegrationCode))
                    ValidateIntegrationCode(dto.IntegrationCode, dto.Id);

                if (Notification.HasNotification()) return;

                var billingItemServiceDto = new BillingItemDto()
                {
                    BillingItemName = dto.Name,
                    BillingItemCategoryId = dto.BillingItemCategoryId,
                    IsActive = dto.IsActive,
                    BillingItemTypeId = (int)BillingItemTypeEnum.Service,
                    PropertyId = dto.PropertyId,
                    Id = dto.Id,
                    IntegrationCode = dto.IntegrationCode
                };

                var billingItemExisting = _billingItemReadRepository.GetById(billingItemServiceDto.Id);

                if (billingItemExisting.IsActive && !billingItemServiceDto.IsActive)
                    ToggleActivation(billingItemServiceDto.Id);

                if (Notification.HasNotification())
                    return;

                BillingItem service = base.BillingItemAdapter.Map(billingItemServiceDto)
                    .Build();

                _billingItemRepository.Update(service);


                var billingTaxIds = _billingItemReadRepository.GetBillingTaxByBillingItemId(dto.Id);

                if (dto.ServiceAndTaxList.Count() > 0)
                    ValidateTaxEqualsForServiceWithTax(dto.ServiceAndTaxList);

                if (Notification.HasNotification())
                    return;

                foreach (var billingItem in dto.ServiceAndTaxList)
                {
                    var billingTax = new BillingTaxDto()
                    {
                        BeginDate = billingItem.BeginDate,
                        EndDate = billingItem.EndDate,
                        IsActive = billingItem.IsActive,
                        TaxPercentage = billingItem.TaxPercentage,
                        BillingItemId = billingItem.BillingItemServiceId.HasValue ? billingItem.BillingItemServiceId.Value : dto.Id,
                        BillingItemTaxId = billingItem.BillingItemTaxId.Value,
                        Id = billingItem.Id.HasValue ? billingItem.Id.Value : Guid.Empty
                    };

                    BillingTax tax = _billingTaxAdapter.Map(billingTax).Build();

                    if (billingItem.Id.Value != Guid.Empty && billingItem.Id != null)
                        _billingTaxRepository.Update(tax);

                    else
                        _billingTaxRepository.Create(tax);
                }


                foreach (var id in billingTaxIds)
                {
                    if (!dto.ServiceAndTaxList.Any(x => x.Id == id))
                        _billingTaxRepository.Delete(id);
                }


                uow.Complete();
            }

        }

        public void UpdateBillingItemTaxWithService(BillingItemServiceDto dto)
        {
            using (var uow = _unitOfWorkManager.Begin())
            {
                ValidateActiveForTax(dto.ServiceAndTaxList);

                if (Notification.HasNotification())
                    return;

                if (!string.IsNullOrWhiteSpace(dto.IntegrationCode))
                    ValidateIntegrationCode(dto.IntegrationCode, dto.Id);

                if (Notification.HasNotification()) return;

                var billingItemService = new BillingItemDto()
                {
                    BillingItemName = dto.Name,
                    BillingItemCategoryId = dto.BillingItemCategoryId,
                    IsActive = dto.IsActive,
                    BillingItemTypeId = (int)BillingItemTypeEnum.Tax,
                    Id = dto.Id,
                    PropertyId = dto.PropertyId,
                    IntegrationCode = dto.IntegrationCode
                };

                BillingItem service = base.BillingItemAdapter.Map(billingItemService)
                    .Build();

                _billingItemRepository.Update(service);

                var billingTaxIds = _billingItemReadRepository.GetBillingTaxByBillingItemId(dto.Id);

                if (dto.ServiceAndTaxList.Count() > 1)
                    ValidateTaxEqualsForTaxWithService(dto.ServiceAndTaxList);

                if (Notification.HasNotification())
                    return;


                foreach (var billingItem in dto.ServiceAndTaxList)
                {
                    var billingTax = new BillingTaxDto()
                    {
                        BeginDate = billingItem.BeginDate,
                        EndDate = billingItem.EndDate,
                        IsActive = billingItem.IsActive,
                        TaxPercentage = billingItem.TaxPercentage,
                        BillingItemId = billingItem.BillingItemServiceId.Value,
                        BillingItemTaxId = billingItem.BillingItemTaxId.HasValue ? billingItem.BillingItemTaxId.Value : dto.Id,
                        Id = billingItem.Id.HasValue ? billingItem.Id.Value : Guid.Empty
                    };

                    BillingTax tax = _billingTaxAdapter.Map(billingTax).Build();

                    if (billingItem.Id.Value != Guid.Empty && billingItem.Id != null)
                        _billingTaxRepository.Update(tax);

                    else
                        _billingTaxRepository.Create(tax);
                }


                foreach (var id in billingTaxIds)
                {
                    if (!dto.ServiceAndTaxList.Any(x => x.Id == id))
                        _billingTaxRepository.Delete(id);
                }

                uow.Complete();
            }
        }

        public IListDto<GetAllBillingItemServiceDto> GetAllBillingItemService(long propertyId, bool? isActive)
        {
            return _billingItemReadRepository.GetAllBillingItemService(propertyId, isActive);
        }

        public IListDto<GetAllBillingItemServiceDto> GetAllBillingItemServiceByFilters(SearchBillingItemDto requestDto, long propertyId)
        {
            return _billingItemReadRepository.GetAllBillingItemServiceByFilters(requestDto, propertyId);
        }

        public IListDto<GetAllBillingItemTaxDto> GetAllBillingItemTax(long propertyId)
        {
            return _billingItemReadRepository.GetAllBillingItemTax(propertyId);
        }

        public IListDto<GetAllBillingItemForAssociateTaxDto> GetAllBillingItemForAssociateTax(long propertyId)
        {
            return _billingItemReadRepository.GetAllBillingItemForAssociateTax(propertyId);
        }

        public IListDto<GetAllBillingItemForAssociateServiceDto> GetAllBillingItemForAssociateService(long propertyId)
        {
            return _billingItemReadRepository.GetAllBillingItemForAssociateService(propertyId);
        }

        public void ToggleActivation(int id)
        {
            using (var uow = _unitOfWorkManager.Begin())
            {
                var billingItem = _billingItemReadRepository.GetById(id);

                VerifyTourismTax(billingItem);

                if (Notification.HasNotification())
                    return;

                _billingItemRepository.ToggleAndSaveIsActive(billingItem);

                var taxs = _billingItemReadRepository.GetBillingTaxForBiilingItem(id);

                if (taxs != null)
                {
                    foreach (var tax in taxs)
                    {
                        ToggleActivationForTax(tax.Id.Value);
                    }
                }

                uow.Complete();
            }
        }

        public void ToggleActivationForTax(Guid id)
        {
            _billingTaxRepository.ToggleAndSaveIsActive(id);
        }


        #endregion


        private BillingItemPaymentTypeDto InsertPropertyPaymentType(BillingItemPaymentTypeDto billingItemPaymentTypeDto)
        {
            BillingItemPaymentTypeDto result = null;
            using (var uow = _unitOfWorkManager.Begin())
            {
                if (billingItemPaymentTypeDto.PaymentTypeId == (int)PaymentTypeEnum.Debit ||
                   billingItemPaymentTypeDto.PaymentTypeId == (int)PaymentTypeEnum.CreditCard)
                {
                    var billingItemsToInsert = new List<BillingItem>();

                    foreach (var billingItem in billingItemPaymentTypeDto.BillingItemPaymentTypeDetailList)
                    {
                        BillingItem.Builder builder = CreateCreditOrDebitBillingItemPaymentTypeBuilder(billingItemPaymentTypeDto, billingItem);

                        var billingItemEntity = builder.Build();

                        if (Notification.HasNotification())
                            return null;

                        billingItemsToInsert.Add(billingItemEntity);
                    }

                    if (billingItemsToInsert.Count() > 0)
                        _billingItemRepository.AddRange(billingItemsToInsert);

                    result = GetByPaymentTypeIdAndAcquirerId(billingItemPaymentTypeDto.PropertyId, billingItemPaymentTypeDto.PaymentTypeId, billingItemPaymentTypeDto.AcquirerId.Value);
                }
                else
                {
                    BillingItem.Builder builder = CreateOthersBillingItemPaymentTypeBuilder(billingItemPaymentTypeDto);

                    if (Notification.HasNotification())
                        return null;

                    BillingItemDomainService.InsertAndSaveChanges(builder);

                    result = GetByPaymentTypeId(billingItemPaymentTypeDto.PropertyId, billingItemPaymentTypeDto.PaymentTypeId);
                }

                uow.Complete();
            }

            return result;
        }

        private BillingItemPaymentTypeDto UpdatePaymentType(BillingItemPaymentTypeDto billingItemPaymentTypeDto)
        {
            BillingItemPaymentTypeDto result = null;
            using (var uow = _unitOfWorkManager.Begin())
            {
                if (billingItemPaymentTypeDto.PaymentTypeId == (int)PaymentTypeEnum.Debit ||
               billingItemPaymentTypeDto.PaymentTypeId == (int)PaymentTypeEnum.CreditCard)
                {
                    RemoveOldBillingItems(billingItemPaymentTypeDto);

                    foreach (var billingItem in billingItemPaymentTypeDto.BillingItemPaymentTypeDetailList)
                    {
                        BillingItem.Builder builder = CreateCreditOrDebitBillingItemPaymentTypeBuilder(billingItemPaymentTypeDto, billingItem);

                        if (Notification.HasNotification())
                            return null;

                        _billingItemRepository.UpdatePropertyPaymentType(builder.Build());
                    }

                    result = GetByPaymentTypeIdAndAcquirerId(billingItemPaymentTypeDto.PropertyId, billingItemPaymentTypeDto.PaymentTypeId, billingItemPaymentTypeDto.AcquirerId.Value);
                }
                else
                {
                    BillingItem.Builder builder = CreateOthersBillingItemPaymentTypeBuilder(billingItemPaymentTypeDto);

                    if (Notification.HasNotification())
                        return null;

                    _billingItemRepository.UpdatePropertyPaymentType(builder.Build());

                    result = GetByPaymentTypeId(billingItemPaymentTypeDto.PropertyId, billingItemPaymentTypeDto.PaymentTypeId);
                }
                uow.Complete();
            }

            return result;


        }

        private void RemoveOldBillingItems(BillingItemPaymentTypeDto billingItemPaymentTypeDto)
        {
            using (var uow = _unitOfWorkManager.Begin())
            {
                Expression<Func<BillingItem, bool>> exp;

                if (billingItemPaymentTypeDto.BillingItemPaymentTypeDetailList != null && billingItemPaymentTypeDto.BillingItemPaymentTypeDetailList.Count() > 0)
                {
                    var billingItemsIds = billingItemPaymentTypeDto.BillingItemPaymentTypeDetailList.Select(d => d.Id).ToList();

                    exp = (d => d.PropertyId == billingItemPaymentTypeDto.PropertyId &&
                                d.PaymentTypeId == billingItemPaymentTypeDto.PaymentTypeId &&
                                d.AcquirerId == billingItemPaymentTypeDto.AcquirerId &&
                                !billingItemsIds.Contains(d.Id));
                }
                else
                {
                    exp = (d => d.PropertyId == billingItemPaymentTypeDto.PropertyId &&
                                d.PaymentTypeId == billingItemPaymentTypeDto.PaymentTypeId &&
                                d.AcquirerId == billingItemPaymentTypeDto.AcquirerId);
                }

                var billingItems = _billingItemReadRepository.GetAllByExpression(exp);

                DeletePaymentTypesWithChildren(billingItems);

                uow.Complete();
            }

        }

        private BillingItem.Builder CreateCreditOrDebitBillingItemPaymentTypeBuilder(BillingItemPaymentTypeDto billingItemPaymentTypeDto, BillingItemPaymentTypeDetailDto billingItem)
        {
            var billingItemCondition = new List<BillingItemPaymentCondition>();

            if (billingItem.BillingItemPaymentTypeDetailConditionList != null &&
               billingItem.BillingItemPaymentTypeDetailConditionList.Count > 0)
            {
                foreach (var condition in billingItem.BillingItemPaymentTypeDetailConditionList)
                {
                    var billingItemServiceCondition = new BillingItemPaymentConditionDto()
                    {
                        Id = condition.Id == Guid.Empty ? Guid.NewGuid() : condition.Id,
                        CommissionPct = condition.CommissionPct,
                        InstallmentNumber = condition.InstallmentNumber,
                        PaymentDeadline = condition.PaymentDeadline,
                        BillingItemId = condition.BillingItemPaymentTypeDetailId
                    };

                    var builderCondition = _billingItemPaymentCondition.Map(billingItemServiceCondition);

                    billingItemCondition.Add(builderCondition.Build());
                }
            }

            var billingItemDto = new BillingItemDto()
            {
                Id = billingItem.Id,
                IsActive = !billingItemPaymentTypeDto.IsActive ? billingItemPaymentTypeDto.IsActive : billingItem.IsActive,
                BillingItemTypeId = (int)BillingItemTypeEnum.PaymentType,
                Description = billingItemPaymentTypeDto.Description,
                BillingItemName = string.IsNullOrEmpty(billingItemPaymentTypeDto.Description) ? billingItem.PlasticBrandPropertyName : billingItemPaymentTypeDto.Description,
                PropertyId = billingItemPaymentTypeDto.PropertyId,
                PaymentTypeId = billingItemPaymentTypeDto.PaymentTypeId,
                PlasticBrandPropertyId = billingItem.PlasticBrandPropertyId,
                AcquirerId = billingItemPaymentTypeDto.AcquirerId,
                MaximumInstallmentsQuantity = billingItem.MaximumInstallmentsQuantity,
                IntegrationCode = billingItemPaymentTypeDto.IntegrationCode
            };

            var builder = base.BillingItemAdapter.MapWithChildren(billingItemDto, billingItemCondition);
            return builder;
        }

        private BillingItem.Builder CreateOthersBillingItemPaymentTypeBuilder(BillingItemPaymentTypeDto billingItemPaymentTypeDto)
        {
            var billingItemDto = new BillingItemDto()
            {
                Id = billingItemPaymentTypeDto.Id,
                IsActive = billingItemPaymentTypeDto.IsActive,
                BillingItemTypeId = (int)BillingItemTypeEnum.PaymentType,
                Description = billingItemPaymentTypeDto.Description,
                BillingItemName = billingItemPaymentTypeDto.Description,
                PropertyId = billingItemPaymentTypeDto.PropertyId,
                PaymentTypeId = billingItemPaymentTypeDto.PaymentTypeId,
                IntegrationCode = billingItemPaymentTypeDto.IntegrationCode
            };

            var builder = base.BillingItemAdapter.Map(billingItemDto);
            return builder;
        }

        public void ValidateBillingItemPaymentType(BillingItemPaymentTypeDto billingItemPaymentTypeDto)
        {
            switch (billingItemPaymentTypeDto.PaymentTypeId)
            {
                case (int)PaymentTypeEnum.Money:
                case (int)PaymentTypeEnum.Check:
                case (int)PaymentTypeEnum.TobeBilled:
                case (int)PaymentTypeEnum.Deposit:

                    if (billingItemPaymentTypeDto.BillingItemPaymentTypeDetailList != null &&
                        billingItemPaymentTypeDto.BillingItemPaymentTypeDetailList.Count > 0)
                    {
                        Notification.Raise(Notification.DefaultBuilder
                        .WithMessage(AppConsts.LocalizationSourceName, BillingItemEnum.Error.InvalidPropertyPaymentTypeParameters)
                        .WithDetailedMessage(AppConsts.LocalizationSourceName, BillingItemEnum.Error.InvalidPropertyPaymentTypeParameters)
                        .Build());
                        return;
                    }

                    break;

                case (int)PaymentTypeEnum.Debit:

                    foreach (var billingItemPaymentTypeDetail in billingItemPaymentTypeDto.BillingItemPaymentTypeDetailList)
                    {
                        if (billingItemPaymentTypeDetail.BillingItemPaymentTypeDetailConditionList == null)
                        {
                            Notification.Raise(Notification.DefaultBuilder
                            .WithMessage(AppConsts.LocalizationSourceName, BillingItemEnum.Error.InvalidPropertyPaymentTypeParameters)
                            .WithDetailedMessage(AppConsts.LocalizationSourceName, BillingItemEnum.Error.InvalidPropertyPaymentTypeParameters)
                            .Build());
                            return;
                        }

                        if (billingItemPaymentTypeDetail.BillingItemPaymentTypeDetailConditionList != null && billingItemPaymentTypeDetail.BillingItemPaymentTypeDetailConditionList.Count != 1)
                        {
                            Notification.Raise(Notification.DefaultBuilder
                            .WithMessage(AppConsts.LocalizationSourceName, BillingItemEnum.Error.InvalidPropertyPaymentTypeParameters)
                            .WithDetailedMessage(AppConsts.LocalizationSourceName, BillingItemEnum.Error.InvalidPropertyPaymentTypeParameters)
                            .Build());
                            return;
                        }
                    }
                    break;
                case (int)PaymentTypeEnum.CreditCard:

                    foreach (var billingItemPaymentTypeDetail in billingItemPaymentTypeDto.BillingItemPaymentTypeDetailList)
                    {
                        if (billingItemPaymentTypeDetail.BillingItemPaymentTypeDetailConditionList == null)
                        {
                            Notification.Raise(Notification.DefaultBuilder
                            .WithMessage(AppConsts.LocalizationSourceName, BillingItemEnum.Error.InvalidPropertyPaymentTypeParameters)
                            .WithDetailedMessage(AppConsts.LocalizationSourceName, BillingItemEnum.Error.InvalidPropertyPaymentTypeParameters)
                            .Build());
                            return;
                        }

                        if (billingItemPaymentTypeDetail.BillingItemPaymentTypeDetailConditionList != null &&
                            billingItemPaymentTypeDetail.BillingItemPaymentTypeDetailConditionList.Count != billingItemPaymentTypeDetail.MaximumInstallmentsQuantity)
                        {
                            Notification.Raise(Notification.DefaultBuilder
                            .WithMessage(AppConsts.LocalizationSourceName, BillingItemEnum.Error.InvalidPropertyPaymentTypeMaximumInstallmentsQuantity)
                            .WithDetailedMessage(AppConsts.LocalizationSourceName, BillingItemEnum.Error.InvalidPropertyPaymentTypeMaximumInstallmentsQuantity)
                            .Build());
                            return;
                        }
                    }
                    break;
                default:
                    Notification.Raise(Notification.DefaultBuilder
                    .WithMessage(AppConsts.LocalizationSourceName, BillingItemEnum.Error.InvalidPropertyPaymentTypeParameters)
                    .WithDetailedMessage(AppConsts.LocalizationSourceName, BillingItemEnum.Error.InvalidPropertyPaymentTypeParameters)
                    .Build());
                    break;
            }
        }

        public void ValidateDuplicatesPaymentType(BillingItemPaymentTypeDto billingItemPaymentTypeDto, bool isUpdate)
        {
            switch (billingItemPaymentTypeDto.PaymentTypeId)
            {
                case (int)PaymentTypeEnum.Money:
                case (int)PaymentTypeEnum.Check:
                case (int)PaymentTypeEnum.TobeBilled:
                case (int)PaymentTypeEnum.Deposit:

                    if (!isUpdate)
                    {
                        var anyBillingItem = _billingItemReadRepository.AnyBillingItemByExpression(d => d.PropertyId == billingItemPaymentTypeDto.PropertyId && d.PaymentTypeId == billingItemPaymentTypeDto.PaymentTypeId);

                        if (anyBillingItem)
                        {
                            Notification.Raise(Notification.DefaultBuilder
                            .WithMessage(AppConsts.LocalizationSourceName, BillingItemEnum.Error.DuplicateBillingItemEnumPaymentType)
                            .WithDetailedMessage(AppConsts.LocalizationSourceName, BillingItemEnum.Error.DuplicateBillingItemEnumPaymentType)
                            .Build());
                        }
                    }

                    break;

                case (int)PaymentTypeEnum.Debit:
                case (int)PaymentTypeEnum.CreditCard:

                    if (!isUpdate)
                    {
                        foreach (var detail in billingItemPaymentTypeDto.BillingItemPaymentTypeDetailList)
                        {
                            var anyBillingItemCredOrDebit = _billingItemReadRepository.AnyBillingItemByExpression(d =>
                                    d.PropertyId == billingItemPaymentTypeDto.PropertyId &&
                                    d.PaymentTypeId == billingItemPaymentTypeDto.PaymentTypeId &&
                                    d.PlasticBrandPropertyId == detail.PlasticBrandPropertyId &&
                                    d.AcquirerId == billingItemPaymentTypeDto.AcquirerId);

                            if (anyBillingItemCredOrDebit)
                            {
                                Notification.Raise(Notification.DefaultBuilder
                                .WithMessage(AppConsts.LocalizationSourceName, BillingItemEnum.Error.DuplicateBillingItemEnumPaymentType)
                                .WithDetailedMessage(AppConsts.LocalizationSourceName, BillingItemEnum.Error.DuplicateBillingItemEnumPaymentType)
                                .Build());
                            }
                        }
                    }

                    var groupBillingItem =
                        from b in billingItemPaymentTypeDto.BillingItemPaymentTypeDetailList
                        group b by b.PlasticBrandPropertyId into pbp
                        select new
                        {
                            pbp.Key,
                            Total = pbp.Count()
                        };

                    if (groupBillingItem.Any(e => e.Total > 1))
                    {
                        Notification.Raise(Notification.DefaultBuilder
                        .WithMessage(AppConsts.LocalizationSourceName, BillingItemEnum.Error.DuplicateBillingItemEnumPaymentType)
                        .WithDetailedMessage(AppConsts.LocalizationSourceName, BillingItemEnum.Error.DuplicateBillingItemEnumPaymentType)
                        .Build());
                    }

                    break;
                default:
                    Notification.Raise(Notification.DefaultBuilder
                    .WithMessage(AppConsts.LocalizationSourceName, BillingItemEnum.Error.InvalidPropertyPaymentTypeParameters)
                    .WithDetailedMessage(AppConsts.LocalizationSourceName, BillingItemEnum.Error.InvalidPropertyPaymentTypeParameters)
                    .Build());
                    break;
            }
        }


        public BillingItemPaymentTypeDto CreatePropertyPaymentType(BillingItemPaymentTypeDto billingItemPaymentTypeDto)
        {
            if (billingItemPaymentTypeDto == null)
            {
                NotifyNullParameter();
                return BillingItemPaymentTypeDto.NullInstance;
            }

            if (!string.IsNullOrWhiteSpace(billingItemPaymentTypeDto.IntegrationCode))
                ValidateIntegrationCode(billingItemPaymentTypeDto.IntegrationCode, billingItemPaymentTypeDto.Id);

            if (Notification.HasNotification()) return BillingItemPaymentTypeDto.NullInstance;

            BillingItemPaymentTypeDto result = null;

            using (var uow = _unitOfWorkManager.Begin())
            {
                ValidateBillingItemPaymentType(billingItemPaymentTypeDto);

                if (Notification.HasNotification())
                    result = BillingItemPaymentTypeDto.NullInstance;

                ValidateDuplicatesPaymentType(billingItemPaymentTypeDto, false);


                if (Notification.HasNotification())
                    result = BillingItemPaymentTypeDto.NullInstance;

                result = InsertPropertyPaymentType(billingItemPaymentTypeDto);

                if (Notification.HasNotification())
                    result = BillingItemPaymentTypeDto.NullInstance;

                uow.Complete();
            }

            return result;
        }

        public BillingItemPaymentTypeDto UpdatePropertyPaymentType(BillingItemPaymentTypeDto billingItemPaymentTypeDto)
        {
            if (billingItemPaymentTypeDto == null)
            {
                NotifyNullParameter();
                return BillingItemPaymentTypeDto.NullInstance;
            }

            if(!string.IsNullOrWhiteSpace(billingItemPaymentTypeDto.IntegrationCode))
                ValidateIntegrationCode(billingItemPaymentTypeDto.IntegrationCode, billingItemPaymentTypeDto.Id);

            if (Notification.HasNotification()) return BillingItemPaymentTypeDto.NullInstance;

            BillingItemPaymentTypeDto result = null;

            using (var uow = _unitOfWorkManager.Begin())
            {
                ValidateBillingItemPaymentType(billingItemPaymentTypeDto);

                if (Notification.HasNotification())
                    result = BillingItemPaymentTypeDto.NullInstance;

                ValidateDuplicatesPaymentType(billingItemPaymentTypeDto, true);

                if (Notification.HasNotification())
                    result = BillingItemPaymentTypeDto.NullInstance;

                result = UpdatePaymentType(billingItemPaymentTypeDto);

                if (Notification.HasNotification())
                    result = BillingItemPaymentTypeDto.NullInstance;


                uow.Complete();
            }

            return result;


        }

        public BillingItemPaymentTypeDto GetByPaymentTypeIdAndAcquirerId(int propertyId, int paymentTypeId, Guid acquirerId)
        {
            if (propertyId <= 0)
            {
                NotifyIdIsMissing();
                return BillingItemPaymentTypeDto.NullInstance;
            }

            var validPaymentTypes = new int[]
            {
                (int)PaymentTypeEnum.Debit,
                (int)PaymentTypeEnum.CreditCard
            };

            if (!validPaymentTypes.Any(e => e == paymentTypeId))
            {
                Notification.Raise(Notification.DefaultBuilder
                    .WithMessage(AppConsts.LocalizationSourceName, BillingItemEnum.Error.InvalidPropertyPaymentTypeParameters)
                    .WithDetailedMessage(AppConsts.LocalizationSourceName, BillingItemEnum.Error.InvalidPropertyPaymentTypeParameters)
                    .Build());
                return null;
            }


            return _billingItemReadRepository.GetByPaymentTypeIdAndAcquirerId(propertyId, paymentTypeId, acquirerId);
        }

        public BillingItemPaymentTypeDto GetByPaymentTypeId(int propertyId, int paymentTypeId)
        {
            if (propertyId <= 0)
            {
                NotifyIdIsMissing();
                return BillingItemPaymentTypeDto.NullInstance;
            }

            var validPaymentTypes = new int[]
            {
                (int)PaymentTypeEnum.Money,
                (int)PaymentTypeEnum.Check,
                (int)PaymentTypeEnum.TobeBilled,
                (int)PaymentTypeEnum.Deposit
            };

            if (!validPaymentTypes.Any(e => e == paymentTypeId))
            {
                Notification.Raise(Notification.DefaultBuilder
                    .WithMessage(AppConsts.LocalizationSourceName, BillingItemEnum.Error.InvalidPropertyPaymentTypeParameters)
                    .WithDetailedMessage(AppConsts.LocalizationSourceName, BillingItemEnum.Error.InvalidPropertyPaymentTypeParameters)
                    .Build());
                return null;
            }

            return _billingItemReadRepository.GetByPaymentTypeId(propertyId, paymentTypeId);
        }


        public IListDto<BillingItemPaymentTypeDto> GetAllPaymentTypeIdByPropertyId(int propertyId)
        {
            if (propertyId <= 0)
            {
                NotifyIdIsMissing();
                return null;
            }

            return _billingItemReadRepository.GetAllPaymentTypeIdByPropertyId(propertyId);
        }

        public void DeletePaymentType(int propertyId, int paymentTypeId, Guid acquirerId)
        {
            Expression<Func<BillingItem, bool>> exp;

            var othersPaymentTypes = new int[]
            {
                (int)PaymentTypeEnum.Money,
                (int)PaymentTypeEnum.Check,
                (int)PaymentTypeEnum.TobeBilled,
                (int)PaymentTypeEnum.Deposit
            };
            using (var uow = _unitOfWorkManager.Begin())
            {
                if (acquirerId == Guid.Empty && othersPaymentTypes.Contains(paymentTypeId))
                    exp = (d => d.PropertyId == propertyId && d.PaymentTypeId == paymentTypeId);
                else
                    exp = (d => d.PropertyId == propertyId && d.PaymentTypeId == paymentTypeId && d.AcquirerId == acquirerId);

                var billingItems = _billingItemReadRepository.GetAllByExpression(exp);

                DeletePaymentTypesWithChildren(billingItems);

                uow.Complete();
            }

        }

        private void DeletePaymentTypesWithChildren(List<BillingItem> billingItems)
        {

            foreach (var billingItem in billingItems)
            {

                var conditions = _billingItemReadRepository.GetAllBillingItemPaymentConditionByBillingItemId(billingItem.Id);

                foreach (var condition in conditions)
                    _billingItemPaymentConditionDomainService.Delete(condition);

                _billingItemRepository.DeletePaymentType(billingItem.Id);
            }
        }

        public void ToggleActivationPaymentType(int propertyId, int paymentTypeId, Guid acquirerId, bool activate)
        {
            using (var uow = _unitOfWorkManager.Begin())
            {
                Expression<Func<BillingItem, bool>> exp;

                if (acquirerId == Guid.Empty)
                    exp = (d => d.PropertyId == propertyId && d.PaymentTypeId == paymentTypeId);
                else
                    exp = (d => d.PropertyId == propertyId && d.PaymentTypeId == paymentTypeId && d.AcquirerId == acquirerId);

                var billingItems = _billingItemReadRepository.GetAllByExpression(exp);

                foreach (var billingItem in billingItems)
                {
                    _billingItemRepository.ToggleActivationPaymentType(billingItem, activate);
                }

                uow.Complete();
            }
        }

        public IListDto<BillingItemServiceLancamentoDto> GetAllServiceWithAssociateTaxForDebitRealease(int propertyId, GetAllBillingItemServiceForRealase request)
        {
            var propertyDate = _propertyParameterReadRepository.GetSystemDate(int.Parse(_applicationUser.PropertyId));
            var services = _billingItemReadRepository.GetAllServiceWithAssociateTaxForDebitRealease(propertyId, request, propertyDate.GetValueOrDefault());

            if (services != null && services.Items.Count() > 0)
            {
                var taxForServices = _billingItemReadRepository.GetTaxesForServices(services.Items.Select(x => x.Id).ToList(), propertyDate);

                if (taxForServices != null && taxForServices.Count() > 0)
                {
                    foreach (var service in services.Items)
                    {
                        service.ServiceAndTaxList = taxForServices.Where(x => x.BillingItemServiceId == service.Id).Select(a => new BillingItemServiceAndTaxAssociateDto()
                        {
                            Name = a.Name,
                            TaxPercentage = a.TaxPercentage
                        }).ToList();

                    }
                }
            }

            return services;
        }


        public IListDto<BillingItemPlasticBrandCompanyClientDto> GetPlasticBrandWithCompanyClientByPaymentType(int paymentTypeId)
        {
            return _billingItemReadRepository.GetPlasticBrandWithCompanyClientByPaymentType(paymentTypeId);
        }

        public IListDto<PaymentTypeWithBillingItemDto> GetPaymentTypes(GetAllPaymentTypeDto request)
        {
            if (request.PropertyId <= 0)
            {
                NotifyIdIsMissing();
                return null;
            }

            var paymentTypes = _paymentTypeAppService.GetAllForCreation(request).ConfigureAwait(false).GetAwaiter().GetResult();
            var paymentTypesWithBillingItem = new int[] { (int)PaymentTypeEnum.Check, (int)PaymentTypeEnum.Money, (int)PaymentTypeEnum.Deposit, (int)PaymentTypeEnum.TobeBilled }.ToList();


            var billingItems = _billingItemReadRepository.GetAllByExpression(e => e.PropertyId == request.PropertyId && e.PaymentTypeId.HasValue && paymentTypesWithBillingItem.Contains(e.PaymentTypeId.Value) && e.IsActive);
            var paymentTypeWithBillingItemDto = new ListDto<PaymentTypeWithBillingItemDto>
            {
                HasNext = false,
                //Total = paymentTypes.Items.Count()
            };

            foreach (var item in paymentTypes.Items)
            {
                if (new int[] { (int)PaymentTypeEnum.CreditCard, (int)PaymentTypeEnum.Debit }.Contains(item.Id) &&
                    !_billingItemReadRepository.AnyBillingItemByExpression(e => e.PaymentTypeId == item.Id && e.PropertyId == request.PropertyId))
                    continue;

                var billingItem = billingItems.FirstOrDefault(e => e.PaymentTypeId.HasValue && e.PaymentTypeId.Value == item.Id);

                var result = new PaymentTypeWithBillingItemDto
                {
                    Id = item.Id,
                    BillingItemId = billingItem != null ? billingItem.Id : (int?)null,
                    PaymentTypeName = billingItem != null ? billingItem.BillingItemName : _localizationManager.GetString(AppConsts.LocalizationSourceName, ((PaymentTypeEnum)item.Id).ToString())
                };

                if (paymentTypesWithBillingItem.Contains(result.Id) && !result.BillingItemId.HasValue)
                    continue;

                paymentTypeWithBillingItemDto.Items.Add(result);
            }

            return paymentTypeWithBillingItemDto;
        }

        public IListDto<BillingItemWithParametersDocumentsDto> GetAllServiceWithParameters(int propertyId)
        {
            return _billingItemReadRepository.GetAllServiceWithParameters(propertyId);
        }

        public void ValidateIntegrationCode(string integrationCode, int billingItemId)
        {
            if (!_billingItemReadRepository.IntegrationCodeIsAvailable(integrationCode, billingItemId, int.Parse(_applicationUser.PropertyId)))
            {
                Notification.Raise(Notification.DefaultBuilder
                           .WithMessage(AppConsts.LocalizationSourceName, BillingItem.EntityError.BillingItemIntegrationCodeUnavailable)
                           .WithDetailedMessage(AppConsts.LocalizationSourceName, BillingItem.EntityError.BillingItemIntegrationCodeUnavailable)
                           .Build());
            }
        }

        private void VerifyTourismTax(BillingItem billingItem)
        {
            var tourismTaxId = _tourismTaxReadRepository.GetIdByBillintItemId(billingItem.Id);
            if (tourismTaxId != null && tourismTaxId != Guid.Empty)
            {
                Notification.Raise(Notification.DefaultBuilder
                .WithMessage(AppConsts.LocalizationSourceName, BillingItem.EntityError.BillingItemHasTourismTax)
                .WithDetailedMessage(AppConsts.LocalizationSourceName, BillingItem.EntityError.BillingItemHasTourismTax)
                .Build());
                return;
            }
        }

        public List<BillingItemDto> GetAllBillingItemService()
        {
            return _billingItemReadRepository.GetAllByTypeId((int)BillingItemTypeEnum.Service);
        }
    }
}
