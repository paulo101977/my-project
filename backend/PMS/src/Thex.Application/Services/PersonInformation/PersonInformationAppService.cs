﻿using System;
using Thex.Application.Adapters;
using Thex.Domain.Entities;
using Thex.Application.Interfaces;
using Tnf.Domain.Services;
using Thex.Dto;
using System.Collections.Generic;

using Tnf.Dto;

using Tnf.Notifications;

namespace Thex.Application.Services
{
    public class PersonInformationAppService : ScaffoldPersonInformationAppService, IPersonInformationAppService
    {
        public PersonInformationAppService(
            IPersonInformationAdapter personInformationAdapter,
            IDomainService<PersonInformation> personInformationDomainService,
            INotificationHandler notificationHandler)
            : base(personInformationAdapter, personInformationDomainService, notificationHandler)
        {
        }
        public new PersonInformationDto Get(DefaultIntRequestDto id)
        {
            if (id.Id < 0)
            {
                NotifyIdIsMissing();
                return PersonInformationDto.NullInstance;
            }

            PersonInformation location = PersonInformationDomainService.Get(id);
            return location.MapTo<PersonInformationDto>();
        }


        public ICollection<PersonInformation> ToDomain(ICollection<PersonInformationDto> personInformations)
        {
            var personInformationList = new HashSet<PersonInformation>();
            if (personInformations != null)
            {
                foreach (var personInformationDto in personInformations)
                {
                    var personInformationBuilder = this.GetBuilder(personInformationDto);
                    var personInformation = personInformationBuilder.Build();
                    personInformationList.Add(personInformation);
                }
            }
            return personInformationList;
        }

        /// <summary>
        /// Creates a <see cref="PersonInformationBuilder"/> passing a dto with parents
        /// </summary>
        /// <param name="PersonInformationDto">An instance of the <see cref="PersonInformationDto"/> class</param>
        /// <returns>This <see cref="PersonInformationBuilder"/> instance</returns>
        public PersonInformation.Builder GetBuilder(PersonInformationDto personInformationDto)
        {
            var builder = new PersonInformation.Builder(Notification);

            builder
                .WithOwnerId(personInformationDto.OwnerId)
                .WithPersonInformationTypeId(personInformationDto.PersonInformationTypeId)
                .WithInformation(personInformationDto.Information);

            return builder;
        }

        public new PersonInformationDto Create(PersonInformationDto location)
        {
            if (location == null)
            {
                NotifyNullParameter();
                return PersonInformationDto.NullInstance;
            }

            location.Id = PersonInformationDomainService.InsertAndSaveChanges(GetBuilder(location)).Id;

            return location;
        }

        public PersonInformationDto Update(PersonInformationDto location)
        {
            if (location == null)
                NotifyNullParameter();
            else if (location.Id <= 0)
                NotifyIdIsMissing();

            if (Notification.HasNotification())
                return PersonInformationDto.NullInstance;

            PersonInformationDomainService.Update(GetBuilder(location));

            return location;
        }

        public new void Delete(int id)
        {
            if (id < 0)
            {
                NotifyIdIsMissing();
                return;
            }

            var location = PersonInformationDomainService.Get(new DefaultIntRequestDto(id));
            if (location == null)
            {
                NotifyWhenEntityNotExist("PersonInformation");
                return;
            }

            PersonInformationDomainService.Delete(location);
        }
    }
}
