﻿using Thex.Application.Interfaces;
using Thex.Common.Enumerations;

using Thex.Domain.Entities;
using Thex.Domain.Interfaces.Repositories;
using Thex.Infra.ReadInterfaces;
using Thex.Dto;
using System;
using System.Collections.Generic;
using Tnf.Notifications;
using Thex.Kernel;
using Thex.Domain.Interfaces.Services;

namespace Thex.Application.Services
{
    public class CheckinAppService : ApplicationServiceBase, ICheckinAppService
    {
        private readonly IReservationReadRepository _reservationReadRepository;
        private readonly IBillingAccountAppService _billingAccountAppService;      
        private readonly IBillingAccountReadRepository _billingAccountReadRepository;
        private readonly IGuestReservationItemReadRepository _guestReservationItemReadRepositoryitory;
        private readonly IPropertyParameterReadRepository _propertyParameterReadRepository;
        private readonly IApplicationUser _applicationUser;
        private readonly ICheckinDomainService _checkinDomainService;

        public CheckinAppService(
            IBillingAccountAppService billingAccountAppService,
            IBillingAccountReadRepository billingAccountReadRepository,
            IReservationReadRepository reservationReadRepository,
            IGuestReservationItemReadRepository guestReservationItemReadRepositoryitory,
            INotificationHandler notificationHandler,
            IPropertyParameterReadRepository propertyParameterReadRepository,
            IApplicationUser applicationUser,
            ICheckinDomainService checkinDomainService)
            :base(notificationHandler)
        {
            _billingAccountAppService = billingAccountAppService;
            _billingAccountReadRepository = billingAccountReadRepository;
            _reservationReadRepository = reservationReadRepository;
            _guestReservationItemReadRepositoryitory = guestReservationItemReadRepositoryitory;
            _propertyParameterReadRepository = propertyParameterReadRepository;
            _applicationUser = applicationUser;
            _checkinDomainService = checkinDomainService;
        }

        public void CreateAccountGuest(GuestReservationItem item, long reservationId, int propertyId, int businessSourceId, int marketSegmentId)
        {
            var account = new BillingAccountDto()
            {
                ReservationItemId = item.ReservationItemId,
                BillingAccountName = item.GuestName,
                BillingAccountTypeId = (int)BillingAccountTypeEnum.Guest,
                StatusId = (int)AccountBillingStatusEnum.Opened,
                ReservationId = reservationId,
                GuestReservationItemId = item.Id,
                StartDate = DateTime.UtcNow.ToZonedDateTimeLoggedUser(),
                PropertyId = propertyId,
                BusinessSourceId = businessSourceId,
                MarketSegmentId = marketSegmentId
            };

            CreateAccount(account);
        }

        public void CreateAccountGuestFirst(GuestReservationItem item, long reservationId, int propertyId, int businessSourceId, int marketSegmentId)
        {
            var account = new BillingAccountDto()
            {
                ReservationItemId = item.ReservationItemId,
                BillingAccountName = item.GuestName,
                BillingAccountTypeId = (int)BillingAccountTypeEnum.Company,
                StatusId = (int)AccountBillingStatusEnum.Opened,
                ReservationId = reservationId,
                GuestReservationItemId = item.Id,
                StartDate = DateTime.UtcNow.ToZonedDateTimeLoggedUser(),
                PropertyId = propertyId,
                BusinessSourceId = businessSourceId,
                MarketSegmentId = marketSegmentId
            };


            CreateAccount(account);
        }

        public void CreateBillingAccountForGroup(long reservationId, Guid? companyClientId, GuestReservationItem guestReservationItem, int propertyId, int businessSourceId, int marketSegmentId, string groupName)
        {
            var account = new BillingAccountDto()
            {
                BillingAccountTypeId = (int)BillingAccountTypeEnum.GroupAccount,
                StatusId = (int)AccountBillingStatusEnum.Opened,
                ReservationId = reservationId,
                CompanyClientId = companyClientId.HasValue ? companyClientId : (Guid?)null,
                GuestReservationItemId = guestReservationItem != null ? guestReservationItem.Id : (long?)null,
                //ReservationItemId = guestReservationItem != null ? guestReservationItem.ReservationItemId : (long?)null,
                StartDate = DateTime.UtcNow.ToZonedDateTimeLoggedUser(),
                PropertyId = propertyId,
                BusinessSourceId = businessSourceId,
                MarketSegmentId = marketSegmentId,
                BillingAccountName = groupName
            };

            CreateAccount(account);
        }

        public void CreateBillingAccountForCompany(long reservationItemId, Reservation reservation, int propertyId)
        {
            var account = new BillingAccountDto()
            {
                BillingAccountTypeId = (int)BillingAccountTypeEnum.Company,
                BillingAccountName = reservation.CompanyClient.TradeName,
                StatusId = (int)AccountBillingStatusEnum.Opened,
                ReservationId = reservation.Id,
                ReservationItemId = reservationItemId,
                CompanyClientId = reservation.CompanyClientId,
                StartDate = DateTime.UtcNow.ToZonedDateTimeLoggedUser(),
                PropertyId = propertyId,
                BusinessSourceId = reservation.BusinessSourceId.Value,
                MarketSegmentId = reservation.MarketSegmentId.Value,
                CreateMainAccountOfCompanyInReservationItem = true
            };

            CreateAccount(account);
        }


        public void CreateAccountInCompany(Reservation reservation, int propertyId)
        {
            var account = new BillingAccountDto()
            {
                BillingAccountTypeId = (int)BillingAccountTypeEnum.Company,
                BillingAccountName = reservation.CompanyClient.TradeName,
                StatusId = (int)AccountBillingStatusEnum.Opened,
                ReservationId = reservation.Id,
                CompanyClientId = reservation.CompanyClientId,
                StartDate = DateTime.UtcNow.ToZonedDateTimeLoggedUser(),
                PropertyId = propertyId,
                BusinessSourceId = reservation.BusinessSourceId.Value,
                MarketSegmentId = reservation.MarketSegmentId.Value
            };

            CreateAccount(account);
        }

        private void CreateAccount(BillingAccountDto account)
        {
            var responseBilling = _billingAccountAppService.Create(account).GetAwaiter().GetResult();

            if (Notification.HasNotification())
                return;
        }

        public List<BillingAccount> GetAllForGuestReservationItemId(List<long> listGuestReservation, long reservationId)
        {
            return _billingAccountReadRepository.GetAllForGuestReservationItemId(listGuestReservation, reservationId);
        }

        public Reservation GetReservationForChekin(long reservationId)
        {           
            return _reservationReadRepository.GetReservationForCheckin(reservationId);
        }

        public void UpdateToCheckInGuestReservationItem(GuestReservationItem guestReservationItem, DateTime checkinDate)
        {
            var builder = new GuestReservationItem.Builder(Notification, guestReservationItem);

            builder
                .WithCheckInDate(checkinDate)
                .WithCheckInHour(checkinDate)
                .WithGuestStatusId((int)ReservationStatus.Checkin);

            var newGuestReservationItem = builder.Build();

            if (Notification.HasNotification())
                return;

            _guestReservationItemReadRepositoryitory.Update(newGuestReservationItem);
        }

        public bool AnyGroupAccount(long reservationId)
        {
            return _billingAccountReadRepository.AnyGroupAccount(reservationId);
        }

        public bool AnyCompanyAccountAccomodation(long reservationItemId)
        {
            return _billingAccountReadRepository.AnyCompanyAccountAccomodation(reservationItemId);
        }


        public List<BillingAccount> GetForGuestReservationItemId(long id, long reservationId)
        {
            return _billingAccountReadRepository.GetAllForGuestReservationItemId(new List<long> { id }, reservationId);

        }

        public ReservationDto GetBusinessSourceAndMarketSegmentByReservationId(long id)
        {
            return _reservationReadRepository.GetBusinessSourceAndMarketSegmentByReservationId(id);
        }

        public void Cancel(long reservationItemId)
        {
            var propertyId = int.Parse(_applicationUser.PropertyId);

            var systemDate = _propertyParameterReadRepository.GetSystemDate(propertyId);

            _checkinDomainService.Cancel(reservationItemId, systemDate.Value);
        }
    }
}
