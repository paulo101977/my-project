﻿using System;
using Thex.Application.Adapters;
using Thex.Domain.Entities;
using Thex.Application.Interfaces;
using Tnf.Domain.Services;
using Tnf.Notifications;

namespace Thex.Application.Services
{
    public class PropertyTypeAppService : ScaffoldPropertyTypeAppService, IPropertyTypeAppService
    {
        public PropertyTypeAppService(
            IPropertyTypeAdapter propertyTypeAdapter,
            IDomainService<PropertyType> propertyTypeDomainService,
            INotificationHandler notificationHandler)
            : base(propertyTypeAdapter, propertyTypeDomainService, notificationHandler)
        {
        }
    }
}
