﻿using System;
using Thex.Application.Adapters;
using Thex.Domain.Entities;
using Thex.Application.Interfaces;
using Tnf.Domain.Services;
using Thex.Dto;

using Tnf.Notifications;
using Tnf.Repositories;

namespace Thex.Application.Services
{
    public class ChainMarketSegmentAppService : ScaffoldChainMarketSegmentAppService, IChainMarketSegmentAppService
    {
        protected readonly IRepository<ChainMarketSegment> _chainMarketSegmentRepository;

        public ChainMarketSegmentAppService(
            IChainMarketSegmentAdapter chainMarketSegmentAdapter,
            IDomainService<ChainMarketSegment> chainMarketSegmentDomainService,
            IRepository<ChainMarketSegment> chainMarketSegmentRepository,
            INotificationHandler notificationHandler
            )
            : base(chainMarketSegmentAdapter, chainMarketSegmentDomainService, notificationHandler)
        {
            _chainMarketSegmentRepository = chainMarketSegmentRepository;
        }

         public void Create(MarketSegmentDto MarketSegmentDto, int chainId)
        {
            var chainMarketSegment = ToDomain(MarketSegmentDto, chainId);
            if (chainMarketSegment != null)
            {
                _chainMarketSegmentRepository.InsertAndSaveChanges(chainMarketSegment);
            }
        }

        public ChainMarketSegment ToDomain(MarketSegmentDto MarketSegment, int chainId)
        {
            var chainMarketSegmentBuilder = this.GetBuilder(MarketSegment, chainId);
            return chainMarketSegmentBuilder.Build();

        }

        public ChainMarketSegment.Builder GetBuilder(MarketSegmentDto MarketSegmentDto, int chainId)
        {
            var builder = new ChainMarketSegment.Builder(Notification);

            builder
                .WithChainId(chainId)
                .WithMarketSegmentId(MarketSegmentDto.Id)
                .WithIsActive(MarketSegmentDto.IsActive);

            return builder;
        }
    }
}
