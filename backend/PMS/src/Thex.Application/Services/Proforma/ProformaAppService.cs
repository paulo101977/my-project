﻿using System;
using System.Threading.Tasks;
using Thex.Application.Interfaces;
using Thex.Dto;
using Tnf.Notifications;
using Thex.Common.Enumerations;
using Microsoft.Extensions.Configuration;
using System.Net.Http;
using Polly;
using Newtonsoft.Json;
using Thex.Dto.Integration;
using System.Linq;
using Thex.Common;
using Microsoft.AspNetCore.Http;
using System.Text;
using System.Collections.Generic;
using Thex.Dto.Proforma;
using Thex.Domain.Entities;
using Thex.Kernel;
using Thex.Domain.Interfaces.Integration;
using Thex.Infra.ReadInterfaces;
using Thex.Application.Adapters;
using Thex.Domain.Interfaces.Repositories;

namespace Thex.Application.Services
{
    public class ProformaAppService : ApplicationServiceBase, IProformaAppService
    {
        private readonly string _apiKey = "228d48ba12e8f26e3ea55f423f88836d1f354b43";

        private readonly INotificationHandler _notificationHandler;
        private readonly IConfiguration _configuration;
        private readonly IInvoiceIntegration _invoiceIntegration;
        private readonly IApplicationUser _applicationUser;
        private readonly IInvoiceIntegrationClient _invoiceIntegrationClient;
        private readonly IReservationBudgetReadRepository _reservationBudgetReadRepository;
        private readonly IPropertyParameterReadRepository _propertyParameterReadRepository;
        private readonly IBillingItemReadRepository _billingItemReadRepository;
        private readonly IReservationReadRepository _reservationReadRepository;
        private readonly IBillingAccountAdapter _billingAccountAdapter;
        private readonly IReservationItemReadRepository _reservationItemReadRepository;
        private readonly ITourismTaxReadRepository _tourismTaxReadRepository;
        private readonly IReservationItemRepository _reservationItemRepository;
        private readonly IPropertyReadRepository _propertyReadRepository;

        public ProformaAppService(
            INotificationHandler notificationHandler, 
            IConfiguration configuration,
            IApplicationUser applicationUser,
            IInvoiceIntegrationClient invoiceIntegrationClient,
            IReservationBudgetReadRepository reservationBudgetReadRepository,
            IPropertyParameterReadRepository propertyParameterReadRepository,
            IBillingItemReadRepository billingItemReadRepository,
            IReservationReadRepository reservationReadRepository,
            IBillingAccountAdapter billingAccountAdapter,
            IReservationItemReadRepository reservationItemReadRepository,
            ITourismTaxReadRepository tourismTaxReadRepository,
            IReservationItemRepository reservationItemRepository,
            IPropertyReadRepository propertyReadRepository,
            IInvoiceIntegration invoiceIntegration) : base(notificationHandler)
        {
            _notificationHandler = notificationHandler;
            _configuration = configuration;
            _invoiceIntegration = invoiceIntegration;
            _applicationUser = applicationUser;
            _invoiceIntegrationClient = invoiceIntegrationClient;
            _reservationBudgetReadRepository = reservationBudgetReadRepository;
            _propertyParameterReadRepository = propertyParameterReadRepository;
            _billingItemReadRepository = billingItemReadRepository;
            _reservationReadRepository = reservationReadRepository;
            _billingAccountAdapter = billingAccountAdapter;
            _reservationItemReadRepository = reservationItemReadRepository;
            _tourismTaxReadRepository = tourismTaxReadRepository;
            _reservationItemRepository = reservationItemRepository;
            _propertyReadRepository = propertyReadRepository;
        }

        public async Task<NotificationResponseDto> CreateProforma(ProformaRequestDto requestDto)
        {
            var taxRuleEndpoint = new Uri(string.Concat(_configuration.GetValue<string>("TaxRuleEndpoint"), $"/api/invoice/proforma"));

            try
            {
                var json = await ProccessProformaList(requestDto);

                if (_notificationHandler.HasNotification()) return NotificationResponseDto.NullInstance;

                HttpResponseMessage responseMessage = await _invoiceIntegration.PostRequest(json, taxRuleEndpoint);

                if (responseMessage.IsSuccessStatusCode)
                {
                    var message = await responseMessage.Content.ReadAsStringAsync();

                    if (!string.IsNullOrEmpty(message))
                    {
                        var proformaResponse = JsonConvert.DeserializeObject<ProfromaResponseDto>(message);

                        if (proformaResponse != null && proformaResponse.ResponseDetails != null && !string.IsNullOrWhiteSpace(proformaResponse.ResponseDetails.Id))
                            _reservationItemRepository.UpdateIntegrationId(proformaResponse.ResponseDetails.Id, requestDto);

                        if (_notificationHandler.HasNotification()) return NotificationResponseDto.NullInstance;
                    }
                }
                else
                {
                    if (_notificationHandler.HasNotification()) return NotificationResponseDto.NullInstance;

                    var message = await responseMessage.Content.ReadAsStringAsync();

                    if (!string.IsNullOrEmpty(message))
                    {
                        var taxRuleNotification = JsonConvert.DeserializeObject<InvoiceIntegrationNotification>(message);

                        if (taxRuleNotification != null)
                        {
                            foreach (var notify in taxRuleNotification.Details)
                                return new NotificationResponseDto(notify.DetailedMessage, notify.DetailedMessage, new Guid());

                        }
                        else
                            _notificationHandler.Raise(_notificationHandler
                                                .DefaultBuilder
                                                .WithMessage(AppConsts.LocalizationSourceName, BillingAccountItemEnum.Error.BillingAccountInvoiceNFeioComunicationError)
                                                .Build());
                    }
                    else
                        GenerateProformaError(string.Empty);
                }
            }
            catch (Exception ex)
            {
                GenerateProformaError(ex.Message);
            }

            return NotificationResponseDto.NullInstance;
        }

        private void GenerateProformaError(string message)
        {            
            _notificationHandler.Raise(_notificationHandler
                            .DefaultBuilder
                            .WithMessage(AppConsts.LocalizationSourceName, BillingAccountItemEnum.Error.BillingAccountInvoiceNFeioComunicationError)
                            .WithDetailedMessage(message)
                            .Build());       
        }

        private async Task<string> ProccessProformaList(ProformaRequestDto requestDto)
        {
            if (!requestDto.ReservationId.HasValue && !requestDto.ReservationItemId.HasValue)
            {
                NotifyIdIsMissing();
                return string.Empty;
            }

            var reservationBudgeList = new List<ReservationBudget>();
            var reservation = new Reservation();

            if (requestDto.ReservationId.HasValue)
            {
                reservationBudgeList = _reservationBudgetReadRepository.GetAllByReservationId(requestDto.ReservationId.Value);
                reservation = _reservationReadRepository.GetById(requestDto.ReservationId.Value);
            }
            else
            {
                reservationBudgeList = _reservationBudgetReadRepository.GetAllByReservationItemId(requestDto.ReservationItemId.Value);
                reservation = _reservationReadRepository.GetByReservationItemId(new DefaultLongRequestDto(requestDto.ReservationItemId.Value));
            }

            Guid? companyClientId = null;

            if (reservation != null && reservation.CompanyClientId.HasValue)
                companyClientId = reservation.CompanyClientId.Value;

            if (!companyClientId.HasValue)
            {
                _notificationHandler.Raise(_notificationHandler
                            .DefaultBuilder
                            .WithMessage(AppConsts.LocalizationSourceName, Reservation.EntityError.ReservationMustHaveCompanyClient)
                            .Build());
                return string.Empty;
            }

            var nifDocumentTypeIdList = new List<int>() { (int)DocumentTypeEnum.NaturalNIF, (int)DocumentTypeEnum.LegalNIF };

            if (!reservationBudgeList.Any())
            {
                _notificationHandler.Raise(_notificationHandler
                            .DefaultBuilder
                            .WithMessage(AppConsts.LocalizationSourceName, ReservationBudget.EntityError.ThereIsNoReservationBudget)
                            .Build());
                return string.Empty;
            }

            var ProformaIntegrationItemlist = new List<ProformaIntegrationItemDto>();

            var dailyService = _propertyParameterReadRepository.GetPropertyParameterForPropertyIdAndApplicationParameterId(_applicationUser.PropertyId.TryParseToInt32(), (int)ApplicationParameterEnum.ParameterDaily);

            int serviceId = 0;

            if (dailyService != null)
                int.TryParse(dailyService.PropertyParameterValue, out serviceId);

            if (dailyService == null || serviceId == default(int))
            {
                _notificationHandler.Raise(_notificationHandler
                           .DefaultBuilder
                           .WithMessage(AppConsts.LocalizationSourceName, PropertyParameter.EntityError.PropertyParameterMustHaveDailyBillingItem)
                           .Build());
                return string.Empty;
            }

            var service = _billingItemReadRepository.GetById(serviceId);

            ProformaIntegrationItemlist.AddRange( 
                reservationBudgeList
                .GroupBy(x => new { x.BudgetDay, x.ManualRate})
                .Select(gp => new ProformaIntegrationItemDto
                {
                    Name = service.BillingItemName,
                    Description = service.Description,
                    UnitPrice = gp.Average(x => x.ManualRate),
                    Quantity = gp.Count(),
                    ServiceId = serviceId
                }));

            if (requestDto.LaunchTourismTax)
            {
                var tourismTaxDto = _tourismTaxReadRepository.GetByPropertyId();
                if (tourismTaxDto != null && tourismTaxDto.IsActive)
                {
                    var adultsPerDate = _reservationItemReadRepository.GetNumberOfAdultGuestAndBudgetDayByReservationId(requestDto);

                    var total = tourismTaxDto.IsTotalOfNights || tourismTaxDto.NumberOfNights > adultsPerDate.Count() ? 
                            adultsPerDate.Sum(x => x.Value) * tourismTaxDto.Amount :
                            adultsPerDate.Where(x => x.Key < adultsPerDate.OrderBy(y => y.Key).FirstOrDefault().Key
                            .AddDays(tourismTaxDto.NumberOfNights)).Sum(x => x.Value) * tourismTaxDto.Amount;

                    var tourismTaxService = _billingItemReadRepository.GetById(tourismTaxDto.BillingItemId);

                    ProformaIntegrationItemlist.Add(new ProformaIntegrationItemDto()
                    {
                        Name = tourismTaxService.BillingItemName,
                        Description = tourismTaxService.Description,
                        UnitPrice = total,
                        Quantity = 1,
                        ServiceId = tourismTaxDto.BillingItemId
                    });
                }
            }

            var client = _invoiceIntegrationClient.GetProformaBorrowerInformation(companyClientId.Value).Result
                                                .OrderByDescending(x => nifDocumentTypeIdList.Contains(x.DocumentTypeId))
                                                .FirstOrDefault();

            ProformaClientDto proformaClient;

            if (nifDocumentTypeIdList.Contains(client.DocumentTypeId))
            {
                proformaClient = new ProformaClientDto()
                {
                    Code = client.Id,
                    Name = client.Name,
                    FiscalId = client.Document
                };
            }
            else
            {
                proformaClient = new ProformaClientDto()
                {
                    Code = client.Id,
                    Name = client.Name
                };
            }

            var reservationCode = requestDto.ReservationId.HasValue ? reservation.ReservationCode : reservation.ReservationItemList.FirstOrDefault().ReservationItemCode;

            var proformaIntegrationDto = new ProformaDto()
            {
                Date = DateTime.UtcNow.ToZonedDateTimeLoggedUser(_applicationUser),
                DueDate = DateTime.UtcNow.ToZonedDateTimeLoggedUser(_applicationUser),
                Client = proformaClient,
                Items = ProformaIntegrationItemlist,
                PropertyUUId = _propertyReadRepository.GetUIdById(int.Parse(_applicationUser.PropertyId)),
                Observations = requestDto.ReservationId.HasValue ? $"Código da Reserva: {reservationCode} / Total de Acomodações: {reservation.ReservationItemList.Count}"
                    : $"Código da Reserva: {reservationCode}"
            };

            GeneratePartnertIntegrationDetails(ref proformaIntegrationDto);

            if (string.IsNullOrWhiteSpace(proformaIntegrationDto.Client.Name))
            {
                _notificationHandler.Raise(_notificationHandler
                            .DefaultBuilder
                            .WithMessage(AppConsts.LocalizationSourceName, CompanyClient.EntityError.CompanyClientMustHaveName)
                            .Build());
                return string.Empty;
            }

            if (_notificationHandler.HasNotification())
                return await Task.FromResult(string.Empty);

            var json = await Task.FromResult(JsonConvert.SerializeObject(proformaIntegrationDto));
            return json; 
        }

        private void GeneratePartnertIntegrationDetails(ref ProformaDto proformaDto)
        {
            var propertyId = int.Parse(_applicationUser.PropertyId);

            var propertyWithDetails = _propertyParameterReadRepository.GetDtoByIdAsync(propertyId).Result;
            if (propertyWithDetails == null || propertyWithDetails.IntegrationPartnerPropertyList == null || propertyWithDetails.IntegrationPartnerPropertyList.Count == 0)
                _notificationHandler.Raise(_notificationHandler
                                       .DefaultBuilder
                                       .WithMessage(AppConsts.LocalizationSourceName, BillingAccountItemEnum.Error.BillingAccountInvoiceNFeioComunicationError)
                                       .Build());

            var integrationProperty = propertyWithDetails.IntegrationPartnerPropertyList.Where(x => x.PropertyId == propertyId &&
                                                                                                    x.IntegrationPartnerType == (int)IntegrationPartnerTypeEnum.InvoiceXpress).FirstOrDefault();
            if (integrationProperty == null)
                _notificationHandler.Raise(_notificationHandler
                                   .DefaultBuilder
                                   .WithMessage(AppConsts.LocalizationSourceName, BillingAccountItemEnum.Error.BillingAccountInvoiceNFeioComunicationError)
                                   .Build());

            proformaDto.TokenClient = integrationProperty.IntegrationNumber.Trim();
            proformaDto.IntegrationCode = integrationProperty.IntegrationCode.Trim();
        }
    }
}
