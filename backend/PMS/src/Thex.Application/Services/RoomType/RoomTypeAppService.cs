﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Thex.Application.Adapters;
using Thex.Application.Interfaces;
using Thex.Common;
using Thex.Common.Dto.Observable;
using Thex.Common.Enumerations;
using Thex.Common.Interfaces.Observables;
using Thex.Domain.Entities;
using Thex.Domain.Interfaces.Repositories;
using Thex.Domain.Services.Interfaces;
using Thex.Dto;
using Thex.Dto.Rooms;
using Thex.Dto.RoomTypes;
using Thex.EntityFrameworkCore.Utils;
using Thex.Infra.ReadInterfaces;
using Thex.Infra.Repositories.Read;
using Thex.Inventory.Domain.Repositories.Interfaces;
using Thex.Inventory.Domain.Services;
using Thex.Kernel;
using Tnf.Dto;
using Tnf.Notifications;
using Tnf.Repositories.Uow;
using IRoomTypeInventoryRepository = Thex.Inventory.Domain.Repositories.Interfaces.IRoomTypeInventoryRepository;


namespace Thex.Application.Services
{
    public class RoomTypeAppService : ScaffoldRoomTypeAppService, IRoomTypeAppService
    {
        private readonly IRoomTypeRepository _roomTypeRepository;
        private readonly IRoomTypeReadRepository _roomTypeReadRepository;
        private readonly IRoomTypeDomainService _roomTypeDomainService;
        private readonly EntityFrameworkCore.ReadInterfaces.Repositories.IRoomReadRepository _roomReadRepository;
        private readonly IApplicationUser _applicationUser;
        protected readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly PropertyParameterReadRepository _propertyParameterReadRepository;
        private readonly IRoomTypeInventoryRepository _roomTypeInventoryRepository;
        private readonly IReservationItemReadRepository _reservationItemReadRepository;
        private readonly IList<ICreateRoomTypeObservable> _createRoomTypeObservableList;
        private readonly IDeleteRoomTypeInventoryRepository _deleteRoomTypeInventoryRepository;
        private readonly IIgnoreAuditedEntity ignoreAuditedEntity;

        public RoomTypeAppService(
            IUnitOfWorkManager unitOfWorkManager,
            IRoomTypeAdapter roomTypeAdapter,
            IRoomTypeDomainService roomTypeDomainService,
            IRoomTypeReadRepository roomTypeReadRepository,
            IRoomTypeRepository roomTypeRepository,
            EntityFrameworkCore.ReadInterfaces.Repositories.IRoomReadRepository roomReadRepository,
            IApplicationUser applicationUser,
            INotificationHandler notificationHandler,
            PropertyParameterReadRepository propertyParameterReadRepository,
            IRoomTypeInventoryRepository roomTypeInventoryRepository,
            IReservationItemReadRepository reservationItemReadRepository,
            IServiceProvider serviceProvider,
            IDeleteRoomTypeInventoryRepository deleteRoomTypeInventoryRepository,
            IIgnoreAuditedEntity _ignoreAuditedEntity)
            : base(roomTypeAdapter, roomTypeDomainService, notificationHandler)
        {
            _unitOfWorkManager = unitOfWorkManager;
            _applicationUser = applicationUser;
            _roomTypeReadRepository = roomTypeReadRepository;
            _roomTypeRepository = roomTypeRepository;
            _roomTypeDomainService = roomTypeDomainService;
            _roomReadRepository = roomReadRepository;
            _propertyParameterReadRepository = propertyParameterReadRepository;
            _roomTypeInventoryRepository = roomTypeInventoryRepository;
            _reservationItemReadRepository = reservationItemReadRepository;
            _deleteRoomTypeInventoryRepository = deleteRoomTypeInventoryRepository;
            ignoreAuditedEntity = _ignoreAuditedEntity;

            _createRoomTypeObservableList = new List<ICreateRoomTypeObservable>();

            var roomTypeInventoryDomainService = serviceProvider.GetServices<ICreateRoomTypeObservable>().First(o => o.GetType() == typeof(RoomTypeInventoryDomainService));
            _createRoomTypeObservableList.Add(roomTypeInventoryDomainService);
        }

        private void ValidationsRoomTypes(RoomTypeDto roomTypeDto)
        {
            ValidateMaximumRateLargerMinimumRate(roomTypeDto.MaximumRate, roomTypeDto.MinimumRate);

            if (Notification.HasNotification()) return;

            ValidateNegativeRate(roomTypeDto.MaximumRate);

            if (Notification.HasNotification()) return;

            ValidateChildQuantityRange(roomTypeDto);

            if (Notification.HasNotification()) return;

            if (!string.IsNullOrWhiteSpace(roomTypeDto.DistributionCode))
                ValidateDistributionCode(roomTypeDto);
        }

        public RoomTypeDto CreateRoomType(RoomTypeDto roomTypeDto)
        {
            using (var uow = _unitOfWorkManager.Begin())
            {
                if (roomTypeDto == null)
                {
                    NotifyNullParameter();
                    return RoomTypeDto.NullInstance;
                }

                ValidateDto(roomTypeDto, nameof(roomTypeDto));

                if (Notification.HasNotification())
                    return RoomTypeDto.NullInstance;

                ValidationsRoomTypes(roomTypeDto);

                if (Notification.HasNotification()) return null;

                var roomTypeOrder = 0;
                if (roomTypeDto.Order == 0)
                {
                    var listDto = _roomTypeReadRepository.GetAllRoomTypesByPropertyIdAsync(int.Parse(_applicationUser.PropertyId)).Result.Items;

                    if (listDto.Any())
                        roomTypeOrder = listDto.Max(x => x.Order) + 1;
                    else
                        roomTypeOrder = 1;
                }

                RoomType.Builder builder = new RoomType.Builder(Notification, _roomTypeRepository);

                builder
                    .WithId(roomTypeDto.Id)
                    .WithPropertyId(roomTypeDto.PropertyId)
                    .WithOrder(roomTypeDto.Order > 0 ? roomTypeDto.Order : roomTypeOrder)
                    .WithName(roomTypeDto.Name)
                    .WithAbbreviation(roomTypeDto.Abbreviation)
                    .WithAdultCapacity(roomTypeDto.AdultCapacity)
                    .WithChildCapacity(roomTypeDto.ChildCapacity)
                    .WithIsActive(true)
                    .WithFreeChildQuantity1(roomTypeDto.FreeChildQuantity1)
                    .WithFreeChildQuantity2(roomTypeDto.FreeChildQuantity2)
                    .WithFreeChildQuantity3(roomTypeDto.FreeChildQuantity3)
                    .WithDistributionCode(roomTypeDto.DistributionCode)
                    .WithMaximumRate(roomTypeDto.MaximumRate)
                    .WithMinimumRate(roomTypeDto.MinimumRate);


                foreach (var bedTypeDto in roomTypeDto.RoomTypeBedTypeList)
                {
                    var roomTypeBedTypeBuilder = new RoomTypeBedType.Builder(Notification);

                    roomTypeBedTypeBuilder
                        .WithBedTypeId(bedTypeDto.BedTypeId)
                        .WithCount(bedTypeDto.Count)
                        .WithOptionalLimit(bedTypeDto.OptionalLimit);

                    builder.WithBedType(roomTypeBedTypeBuilder);
                }

                roomTypeDto.Id = RoomTypeDomainService.InsertAndSaveChanges(builder).Id;

                var systemDate = _propertyParameterReadRepository.GetSystemDate(Convert.ToInt32(_applicationUser.PropertyId));

                if (systemDate == null)
                {
                    Notification.Raise(Notification.DefaultBuilder
                    .WithMessage(AppConsts.LocalizationSourceName, PropertyAuditProcess.EntityError.PropertyAuditProcessInvalidPropertySystemDate)
                    .WithDetailedMessage(AppConsts.LocalizationSourceName, PropertyAuditProcess.EntityError.PropertyAuditProcessInvalidPropertySystemDate)
                    .Build());
                }

                if (Notification.HasNotification()) return null;

                foreach (var observable in _createRoomTypeObservableList)
                {
                    var dtoObservable = new CreateRoomTypeObservableDto
                    {
                        InitialDate = systemDate.Value,
                        RoomTypeId = roomTypeDto.Id,
                        PropertyId = Convert.ToInt32(_applicationUser.PropertyId)
                    };

                    observable.ExecuteActionAfterCreateRoomType(dtoObservable);

                    if (Notification.HasNotification()) return null;
                }

                uow.Complete();
            }

            return roomTypeDto;
        }

        private void ValidateMaximumRateLargerMinimumRate(decimal maximumRate, decimal minimumRate)
        {
            if (minimumRate > maximumRate)
            {
                Notification.Raise(Notification.DefaultBuilder
                   .WithMessage(AppConsts.LocalizationSourceName, RoomType.EntityError.RoomTypeMinimumRateLargerMaximumRate)
                   .WithDetailedMessage(AppConsts.LocalizationSourceName, RoomType.EntityError.RoomTypeMinimumRateLargerMaximumRate)
                   .Build());

            }
        }

        private void ValidateChildQuantityRange(RoomTypeDto roomTypeDto)
        {
            if (roomTypeDto.FreeChildQuantity1 > roomTypeDto.ChildCapacity || roomTypeDto.FreeChildQuantity2 > roomTypeDto.ChildCapacity
                || roomTypeDto.FreeChildQuantity3 > roomTypeDto.ChildCapacity)
            {
                Notification.Raise(Notification.DefaultBuilder
                   .WithMessage(AppConsts.LocalizationSourceName, RoomType.EntityError.RoomTypeCountFreeChildrenLargerCapacity)
                   .WithDetailedMessage(AppConsts.LocalizationSourceName, RoomType.EntityError.RoomTypeCountFreeChildrenLargerCapacity)
                   .Build());
            }
        }

        private void ValidateDistributionCode(RoomTypeDto roomTypeDto)
        {
            var distributionCodeAvailable = _roomTypeReadRepository.DistributionCodeAvailable(roomTypeDto.DistributionCode, int.Parse(_applicationUser.PropertyId), roomTypeDto.Id);
            if (!distributionCodeAvailable)
            {
                Notification.Raise(Notification.DefaultBuilder
                  .WithMessage(AppConsts.LocalizationSourceName, RoomType.EntityError.AlreadyExistsRoomTypeWithSameDistributionCode)
                  .WithDetailedMessage(AppConsts.LocalizationSourceName, RoomType.EntityError.AlreadyExistsRoomTypeWithSameDistributionCode)
                  .Build());
            }
        }

        public RoomTypeDto Get(DefaultIntRequestDto id)
        {
            if (id.Id <= 0)
            {
                NotifyIdIsMissing();
                return RoomTypeDto.NullInstance;
            }

            RoomType roomType = RoomTypeDomainService.Get(id);

            return roomType.MapTo<RoomTypeDto>();
        }

        public IListDto<RoomTypeDto> GetRoomTypesByPropertyId(GetAllRoomTypesDto request, int propertyId)
        {
            if (propertyId < 0)
            {
                NotifyIdIsMissing();
                return new ListDto<RoomTypeDto>();
            }

            var roomTypes = _roomTypeReadRepository.GetRoomTypesByPropertyId(request, propertyId);
            return roomTypes;
        }
        private void ValidateNegativeRate(decimal rate)
        {
            if (rate < 0)
            {
                Notification.Raise(Notification.DefaultBuilder
                .WithMessage(AppConsts.LocalizationSourceName, RoomType.EntityError.RoomTypeRateInvalid)
                .WithDetailedMessage(AppConsts.LocalizationSourceName, RoomType.EntityError.RoomTypeRateInvalid)
                .Build());
            }
        }

        public void ToggleActivation(int id)
        {
            using (var uow = _unitOfWorkManager.Begin())
            {
                if (id <= 0)
                {
                    NotifyIdIsMissing();
                    return;
                }

                var isActive = _roomTypeDomainService.ToggleActivation(id);

                if (!isActive)
                {
                    ValidateIfRoomTypeIsInCheckIn(id);

                    if (Notification.HasNotification())
                        return;
                }

                uow.Complete();
            }
        }

        private void ValidateIfRoomTypeIsInCheckIn(int id)
        {
            if (_roomTypeReadRepository.ValidateIfRoomTypeIsInCheckIn(id))
            {
                Notification.Raise(Notification.DefaultBuilder
               .WithMessage(AppConsts.LocalizationSourceName, RoomType.EntityError.RoomTypeIsInCheckIn)
               .WithDetailedMessage(AppConsts.LocalizationSourceName, RoomType.EntityError.RoomTypeIsInCheckIn)
               .Build());
            }
        }

        public RoomTypeDto UpdateRoomType(int id, RoomTypeDto roomTypeDto)
        {
            if (roomTypeDto == null)
            {
                NotifyNullParameter();
                return RoomTypeDto.NullInstance;
            }
            if (id <= 0)
                NotifyIdIsMissing();

            if (Notification.HasNotification())
                return RoomTypeDto.NullInstance;

            ValidationsRoomTypes(roomTypeDto);

            if (Notification.HasNotification()) return null;

            roomTypeDto.Id = id;

            RoomType.Builder builder = new RoomType.Builder(Notification, _roomTypeRepository);

            builder
                .WithId(id)
                .WithPropertyId(roomTypeDto.PropertyId)
                .WithOrder(roomTypeDto.Order)
                .WithName(roomTypeDto.Name)
                .WithAbbreviation(roomTypeDto.Abbreviation)
                .WithAdultCapacity(roomTypeDto.AdultCapacity)
                .WithChildCapacity(roomTypeDto.ChildCapacity)
                .WithIsActive(roomTypeDto.IsActive)
                .WithFreeChildQuantity1(roomTypeDto.FreeChildQuantity1)
                .WithFreeChildQuantity2(roomTypeDto.FreeChildQuantity2)
                .WithFreeChildQuantity3(roomTypeDto.FreeChildQuantity3)
                .WithDistributionCode(roomTypeDto.DistributionCode)
                .WithMaximumRate(roomTypeDto.MaximumRate)
                .WithMinimumRate(roomTypeDto.MinimumRate)
                .ClearBedTypes();

            foreach (var bedTypeDto in roomTypeDto.RoomTypeBedTypeList)
            {
                var roomTypeBedTypeBuilder = new RoomTypeBedType.Builder(Notification);

                roomTypeBedTypeBuilder
                    .WithRoomTypeId(id)
                    .WithBedTypeId(bedTypeDto.BedType)
                    .WithCount(bedTypeDto.Count)
                    .WithOptionalLimit(bedTypeDto.OptionalLimit);

                builder.WithBedType(roomTypeBedTypeBuilder);
            }

            RoomTypeDomainService.Update(builder);

            return roomTypeDto;
        }

        public IListDto<RoomTypeOverbookingDto> GetAllRoomTypesByPropertyAndPeriodDto(GetAllRoomTypesByPropertyAndPeriodDto request, int propertyId)
        {
            //TODO: validar datas InitialDate e FinalDate
            if (propertyId < 0)
            {
                NotifyIdIsMissing();
                return new ListDto<RoomTypeOverbookingDto>();
            }

            if (request.InitialDate.Date > request.FinalDate.Date)
            {
                NotifyInvalidRangeDate();
                return new ListDto<RoomTypeOverbookingDto>();
            }

            propertyId = int.Parse(_applicationUser.PropertyId);
            var roomTypeOverbookingDtoList = _roomTypeReadRepository.GetAllRoomTypesByPropertyAndPeriodDto(request, propertyId);

            foreach (var roomTypeOverbookingDto in roomTypeOverbookingDtoList.Items)
            {
                var roomTypeInventoryList = _roomTypeInventoryRepository.GetAllByRoomTypeIdAndPeriod(roomTypeOverbookingDto.Id, request.InitialDate, request.FinalDate);

                roomTypeOverbookingDto.OverbookingLimit = roomTypeInventoryList.Any(r => r.Balance == 0);
                roomTypeOverbookingDto.Overbooking = roomTypeInventoryList.Any(r => r.Balance < 0);
            }
            // Para não mexer na query de disponibilidade de uh
            // Quando a reserva for day use, subtrai um dia da data
            // de início do período
            if (request.InitialDate.Date == request.FinalDate.Date)
                request.InitialDate = request.FinalDate.Subtract(TimeSpan.FromDays(1));


            if (request.ExpandRooms &&
                roomTypeOverbookingDtoList.Items != null &&
                roomTypeOverbookingDtoList.Items.Any())
            {
                foreach (var item in roomTypeOverbookingDtoList.Items)
                {
                    item.RoomList = _roomReadRepository.GetAllRoomsByPropertyAndPeriodWithoutPagination(
    new GetAllRoomsByPropertyAndPeriodDto { InitialDate = request.InitialDate, FinalDate = request.FinalDate, IsActive = true },
    propertyId, item.Id);
                }
            }

            if (request.ReservationItemId.HasValue)
            {
                var roomTypeReservationItem = _roomTypeReadRepository.GetRoomTypeByReservationItemId(request.ReservationItemId.Value);
                if (roomTypeReservationItem != null)
                {
                    if (!roomTypeOverbookingDtoList.Items.Any(rt => rt.Id == roomTypeReservationItem.Id))
                    {
                        roomTypeOverbookingDtoList.Items.Add(new RoomTypeOverbookingDto()
                        {
                            Id = roomTypeReservationItem.Id,
                            Name = roomTypeReservationItem.Name,
                            Abbreviation = roomTypeReservationItem.Abbreviation,
                            AdultCapacity = roomTypeReservationItem.AdultCapacity,
                            ChildCapacity = roomTypeReservationItem.ChildCapacity
                        });
                    }
                }
            }

            return roomTypeOverbookingDtoList;
        }

        public RoomTypeDto GetRoomTypeById(int roomTypeId)
        {
            return _roomTypeReadRepository.GetRoomTypeById(roomTypeId);
        }

        public RoomTypeDto GetRoomTypeByRoomId(int roomId)
        {
            return _roomTypeReadRepository.GetRoomTypeByRoomId(roomId);
        }

        public void DeleteRoomTypeAsync(int id)
        {
             
            ignoreAuditedEntity.SetEntityName(typeof(RoomTypeInventory).Name);
             
            using (var uow = _unitOfWorkManager.Begin())
            {
                if (id <= 0)
                {
                    NotifyIdIsMissing();
                    return;
                }
                
                _roomTypeRepository.RemoveRoomTypeBedTypePocoFromDatabaseWithRoomTypeId(id);

                if (Notification.HasNotification())
                    return;
                                              
                 _deleteRoomTypeInventoryRepository.Delete(id);

                if (Notification.HasNotification())
                    return;

          
                _roomTypeRepository.Delete(id);

                if (Notification.HasNotification())
                    return;
                 

                uow.Complete();
            }




        }
    }
}
