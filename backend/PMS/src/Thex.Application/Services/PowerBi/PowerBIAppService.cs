﻿using Microsoft.Extensions.Configuration;
using Microsoft.PowerBI.Api.V2;
using Microsoft.PowerBI.Api.V2.Models;
using Microsoft.Rest;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Thex.Application.Interfaces;
using Thex.Dto.PowerBI;
using Thex.Infra.ReadInterfaces;
using Thex.Kernel;
using Tnf.Notifications;

namespace Thex.Application.Services
{
    public class PowerBIAppService : ApplicationServiceBase, IPowerBIAppService
    {
        private readonly IApplicationUser _applicationUser;
        private readonly IConfiguration _configuration;
        private readonly IPropertyParameterReadRepository _propertyParameterReadRepository;
        private PowerBIClient PowerBIClient;

        public PowerBIAppService(
            IApplicationUser applicationUser,
            IConfiguration configuration,
            IPropertyParameterReadRepository propertyParameterReadRepository,
            INotificationHandler notification)
            : base(notification)
        {
            _applicationUser = applicationUser;
            _configuration = configuration;
            _propertyParameterReadRepository = propertyParameterReadRepository;
        }

        public async Task<PowerBiMenuDto> GetAllAsync()
        {
            var accessToken = await AuthenticateAsync();
            if (string.IsNullOrWhiteSpace(accessToken)) return null;

            var powerBiMenuDto = new PowerBiMenuDto() { PowerBiGroupDtoList = await GetGroupsAsync() };

            return powerBiMenuDto;
        }

        private async Task<ICollection<PowerBIReportDto>> GetReportsAsync(string groupId)
        {
            var reportDtoList = new List<PowerBIReportDto>();
            var reports = await PowerBIClient.Reports.GetReportsAsync(groupId);



            foreach (var report in reports.Value)
            {
                var embedToken = await PowerBIClient.Reports.GenerateTokenAsync(groupId, report.Id, new GenerateTokenRequest("View"));

                reportDtoList.Add(new PowerBIReportDto()
                {
                    PowerBIReportId = report.Id,
                    Name = report.Name,
                    TenantId = _applicationUser.TenantId,
                    EmbedUrl = AddPowerBIFilters(report.EmbedUrl),
                    EmbedConfigDto = new PowerBIEmbedConfigDto()
                    {
                        Id = embedToken.TokenId,
                        EmbedToken = embedToken
                    }
                });
            }

            return reportDtoList;
        }

        private async Task<ICollection<PowerBIDashboardDto>> GetDashboardsAsync(string groupId)
        {
            var dashboardDtoList = new List<PowerBIDashboardDto>();
            var dashboards = await PowerBIClient.Dashboards.GetDashboardsAsync(groupId);
            foreach (var dashboard in dashboards.Value)
            {
                var embedToken = await PowerBIClient.Dashboards.GenerateTokenAsync(groupId, dashboard.Id, new GenerateTokenRequest("View"));

                dashboardDtoList.Add(new PowerBIDashboardDto()
                {
                    PowerBIDashboardId = dashboard.Id,
                    Name = dashboard.DisplayName,
                    TenantId = _applicationUser != null ? _applicationUser.TenantId : Guid.Empty,
                    EmbedUrl = AddPowerBIFilters(dashboard.EmbedUrl),
                    EmbedConfigDto = new PowerBIEmbedConfigDto()
                    {
                        Id = embedToken.TokenId,
                        EmbedToken = embedToken
                    }
                });
            }

            return dashboardDtoList;
        }

        private async Task<ICollection<PowerBIGroupDto>> GetGroupsAsync()
        {
            var groupDtoList = new List<PowerBIGroupDto>();
            var groups = await PowerBIClient.Groups.GetGroupsAsync();
            var group = groups.Value.FirstOrDefault(g => g.Id.Equals(_configuration.GetSection("PowerBISettings").GetValue<string>("GroupId")));

            groupDtoList.Add(new PowerBIGroupDto()
            {
                GroupId = group.Id,
                GroupName = group.Name,
                PowerBIDashboardDtoList = await GetDashboardsAsync(group.Id),
                PowerBIReportDtoList = await GetReportsAsync(group.Id)
            });

            return groupDtoList;
        }

        private async Task<string> AuthenticateAsync()
        {
            var oauthEndpoint = new Uri(_configuration.GetSection("PowerBISettings").GetValue<string>("AuthorityUrl"));

            var client = new HttpClient();
            var result = await client.PostAsync(oauthEndpoint, new FormUrlEncodedContent(new[]
            {
                new KeyValuePair<string, string>("resource", _configuration.GetSection("PowerBISettings").GetValue<string>("ResourceUrl")),
                new KeyValuePair<string, string>("client_id", _configuration.GetSection("PowerBISettings").GetValue<string>("ClientId")),
                new KeyValuePair<string, string>("grant_type", "password"),
                new KeyValuePair<string, string>("username", _configuration.GetSection("PowerBISettings").GetValue<string>("Username")),
                new KeyValuePair<string, string>("password", _configuration.GetSection("PowerBISettings").GetValue<string>("Password")),
                new KeyValuePair<string, string>("scope", "openid"),
            }));

            var content = await result.Content.ReadAsStringAsync();
            var powerBIResultDto = JsonConvert.DeserializeObject<PowerBIResultDto>(content);

            if (powerBIResultDto == null)
            {
                NotifyNullParameter();
                return null;
            }

            var tokenCredentials = new TokenCredentials(powerBIResultDto.AccessToken, "Bearer");
            PowerBIClient = new PowerBIClient(new Uri(_configuration.GetSection("PowerBISettings").GetValue<string>("ApiUrl")), tokenCredentials);

            return powerBIResultDto.AccessToken;
        }

        private string AddPowerBIFilters(string embedUrl)
        {
            return $"{embedUrl}&filter=dProperty/TenantId eq '{_applicationUser.TenantId}'";
        }
    }
}
