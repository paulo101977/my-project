﻿using System;
using Thex.Application.Adapters;
using Thex.Domain.Entities;
using Thex.Application.Interfaces;
using Tnf.Domain.Services;
using Tnf.Notifications;

namespace Thex.Application.Services
{
    public class TenantAppService : ScaffoldTenantAppService, ITenantAppService
    {
        public TenantAppService(
            ITenantAdapter tenantAdapter,
            IDomainService<Tenant> tenantDomainService,
            INotificationHandler notificationHandler)
            : base(tenantAdapter, tenantDomainService, notificationHandler)
        {
        }
    }
}
