﻿using System;
using Thex.Application.Adapters;
using Thex.Domain.Entities;
using Thex.Application.Interfaces;
using Tnf.Domain.Services;
using Thex.Dto.PropertyCurrencyExchange;
using Tnf.Dto;
using Thex.Infra.ReadInterfaces;
using System.Collections.Generic;
using System.Linq;
using Thex.Common;
using Thex.Dto;
using Tnf.Notifications;
using Thex.Common.Enumerations;
using Thex.Kernel;
using Thex.Domain.Interfaces.Repositories;
using Tnf.Repositories.Uow;

namespace Thex.Application.Services
{
    public class PropertyCurrencyExchangeAppService : ScaffoldPropertyCurrencyExchangeAppService, IPropertyCurrencyExchangeAppService
    {
        private readonly IPropertyCurrencyExchangeReadRepository _propertyCurrencyExchangeReadRepository;
        private readonly IApplicationUser _applicationUser;
        private readonly IPropertyReadRepository _propertyReadRepository;
        private readonly ICurrencyReadRepository _currencyReadRepository;
        private readonly IPropertyCurrencyExchangeRepository _propertyCurrencyExchangeRepository;
        private readonly IUnitOfWorkManager _unitOfWorkManager;

        public PropertyCurrencyExchangeAppService(
            IPropertyCurrencyExchangeAdapter propertyCurrencyExchangeAdapter,
            IDomainService<PropertyCurrencyExchange> propertyCurrencyExchangeDomainService,
            IPropertyCurrencyExchangeReadRepository propertyCurrencyExchangeReadRepository,
            IApplicationUser applicationUser,
            IPropertyReadRepository propertyReadRepository,
            ICurrencyReadRepository currencyReadRepository,
            IPropertyCurrencyExchangeRepository propertyCurrencyExchangeRepository,
            IUnitOfWorkManager unitOfWorkManager,
            INotificationHandler notificationHandler)
            : base(propertyCurrencyExchangeAdapter, propertyCurrencyExchangeDomainService, notificationHandler)
        {
            _propertyCurrencyExchangeReadRepository = propertyCurrencyExchangeReadRepository;
            _applicationUser = applicationUser;
            _propertyReadRepository = propertyReadRepository;
            _currencyReadRepository = currencyReadRepository;
            _unitOfWorkManager = unitOfWorkManager;
            _propertyCurrencyExchangeRepository = propertyCurrencyExchangeRepository;
        }

        public IListDto<QuotationCurrentDto> GetAllQuotationByPeriod(int propertyId, DateTime? startDate, Guid currencyId, DateTime? endDate)
        {
            var propertyDefaultCurrency = Guid.Parse(_propertyReadRepository.GetPropertyDefaultCurrency(int.Parse(_applicationUser.PropertyId)));

            IsQuotationEqualsPropertyCurrency(new InsertQuotationByPeriodDto() { CurrencyId = currencyId }, propertyDefaultCurrency);

            if (Notification.HasNotification()) return null;

            return _propertyCurrencyExchangeReadRepository.GetAllQuotationByPeriod(propertyId, startDate, currencyId, endDate);
        }

        public QuotationCurrentDto GetQuotationByCurrencyId(int propertyId, Guid currencyId)
        {
            return _propertyCurrencyExchangeReadRepository.GetQuotationByCurrencyId(propertyId, currencyId);
        }        

        public void InsertQuotationByPeriod(InsertQuotationByPeriodDto quotationByPeriod)
        {            
            if (quotationByPeriod == null)
            NotifyNullParameter();

            if (Notification.HasNotification()) return;

            ValidateZeroValue(quotationByPeriod.PropertyExchangeRate);

            if (Notification.HasNotification()) return;

            var propertyDefaultCurrency = Guid.Parse(_propertyReadRepository.GetPropertyDefaultCurrency(int.Parse(_applicationUser.PropertyId)));

            IsQuotationEqualsPropertyCurrency(quotationByPeriod, propertyDefaultCurrency);

            if (Notification.HasNotification()) return;

            var dateList = Enumerable.Range(0, 1 + quotationByPeriod.EndDate.Subtract(quotationByPeriod.StartDate).Days)
                                            .Select(offset => quotationByPeriod.StartDate.AddDays(offset))
                                            .ToList();

            var dateTimeZone = DateTime.UtcNow.ToZonedDateTimeLoggedUser();

            ValidateDate(dateList, dateTimeZone);

            if (Notification.HasNotification()) return;

            var propertyId = int.Parse(_applicationUser.PropertyId);

            var propertyCurrencyExchangeDtoList = new List<PropertyCurrencyExchangeDto>();


            if (IsToDolarQuotation(propertyDefaultCurrency))
            {
                foreach (var date in dateList)
                {
                    propertyCurrencyExchangeDtoList.Add(new PropertyCurrencyExchangeDto()
                    {
                        CurrencyId = propertyDefaultCurrency,
                        ExchangeRate = 1,
                        PropertyCurrencyExchangeDate = date,
                        PropertyId = propertyId
                    });

                    var propertyExchangeRate = CalculateExchangeRateToDolar(quotationByPeriod.PropertyExchangeRate, 1);

                    propertyCurrencyExchangeDtoList.Add(new PropertyCurrencyExchangeDto()
                    {
                        CurrencyId = quotationByPeriod.CurrencyId,
                        ExchangeRate = propertyExchangeRate,
                        PropertyCurrencyExchangeDate = date,
                        PropertyId = propertyId
                    });   

                }

                _propertyCurrencyExchangeRepository.InsertAndSaveChanges(propertyCurrencyExchangeDtoList);
            }
            else if (IsToDolarQuotation(quotationByPeriod.CurrencyId))
            {
                var quotationCurrencyDtoList = _propertyCurrencyExchangeReadRepository
                                                    .GetPropertyQuotationByPropertyId(propertyId,
                                                        quotationByPeriod.StartDate, quotationByPeriod.EndDate, true)
                                                        .Where(x => x.Id != quotationByPeriod.CurrencyId).ToList();

                var propertyCurrencyExchangeRate = quotationCurrencyDtoList.FirstOrDefault(x => x.CurrencyId == propertyDefaultCurrency)?.ExchangeRate;

                quotationCurrencyDtoList = quotationCurrencyDtoList.Where(x => x.CurrencyId != propertyDefaultCurrency).ToList();

                if (quotationCurrencyDtoList.Any())
                {
                    if (propertyCurrencyExchangeRate == null)
                    {
                        Notification.Raise(Notification.DefaultBuilder
                        .WithMessage(AppConsts.LocalizationSourceName, PropertyCurrencyExchange.EntityError.PropertyCurrencyExchangeRateDoesNotExistForPeriod)
                        .WithDetailedMessage(AppConsts.LocalizationSourceName, PropertyCurrencyExchange.EntityError.PropertyCurrencyExchangeRateDoesNotExistForPeriod)
                        .Build()); return;
                    }

                    CalculateExchangeRateToCurrency(quotationCurrencyDtoList, propertyCurrencyExchangeRate.Value, quotationByPeriod.PropertyExchangeRate, propertyCurrencyExchangeDtoList);
                }                    

                foreach (var date in dateList)
                {
                    propertyCurrencyExchangeDtoList.Add(new PropertyCurrencyExchangeDto()
                    {
                        CurrencyId = quotationByPeriod.CurrencyId,
                        ExchangeRate = 1,
                        PropertyCurrencyExchangeDate = date,
                        PropertyId = propertyId
                    });

                    propertyCurrencyExchangeDtoList.Add(new PropertyCurrencyExchangeDto()
                    {
                        CurrencyId = propertyDefaultCurrency,
                        ExchangeRate = quotationByPeriod.PropertyExchangeRate,
                        PropertyCurrencyExchangeDate = date,
                        PropertyId = propertyId
                    });                  
                }

                _propertyCurrencyExchangeRepository.InsertAndSaveChanges(propertyCurrencyExchangeDtoList);
            }
            else
            {
                var quotationCurrencyDtoList = _propertyCurrencyExchangeReadRepository
                                                    .GetPropertyQuotationByPropertyId(int.Parse(_applicationUser.PropertyId),
                                                        quotationByPeriod.StartDate, quotationByPeriod.EndDate);

                foreach (var date in dateList)
                {
                    var propertyCurrencyExchangeRate = quotationCurrencyDtoList?.FirstOrDefault(p => p.CurrentDate.Date == date.Date)?.ExchangeRate;

                    if (propertyCurrencyExchangeRate == null)
                    {
                        Notification.Raise(Notification.DefaultBuilder
                        .WithMessage(AppConsts.LocalizationSourceName, PropertyCurrencyExchange.EntityError.PropertyCurrencyExchangeRateDoesNotExistForPeriod)
                        .WithDetailedMessage(AppConsts.LocalizationSourceName, PropertyCurrencyExchange.EntityError.PropertyCurrencyExchangeRateDoesNotExistForPeriod)
                        .Build()); return;
                    }

                    var propertyExchangeRate = CalculateExchangeRateToDolar(quotationByPeriod.PropertyExchangeRate, propertyCurrencyExchangeRate);

                    var propertyCurrencyExchangeDto = new PropertyCurrencyExchangeDto
                    {
                        CurrencyId = quotationByPeriod.CurrencyId,
                        ExchangeRate = propertyExchangeRate,
                        PropertyCurrencyExchangeDate = date,
                        PropertyId = propertyId
                    };

                    propertyCurrencyExchangeDtoList.Add(propertyCurrencyExchangeDto);
                }

                _propertyCurrencyExchangeRepository.InsertAndSaveChanges(propertyCurrencyExchangeDtoList);
            }
        }

        private void CalculateExchangeRateToCurrency(List<QuotationCurrentDto> quotationCurrencyDtoList, decimal oldPropertyExchangeRate, decimal newPropertyExchangeRate, List<PropertyCurrencyExchangeDto> propertyCurrencyExchangeDtoList)
        {
            quotationCurrencyDtoList.ForEach(quotation =>            
                propertyCurrencyExchangeDtoList.Add(new PropertyCurrencyExchangeDto()
                {
                    CurrencyId = quotation.CurrencyId,
                    ExchangeRate = quotation.PropertyExchangeRate.HasValue ?  1 / (oldPropertyExchangeRate / quotation.PropertyExchangeRate.Value) * 
                                                                                                             newPropertyExchangeRate : 
                                                                              1 / (oldPropertyExchangeRate / quotation.ExchangeRate.Value) * 
                                                                                                             newPropertyExchangeRate,
                    PropertyCurrencyExchangeDate = quotation.CurrentDate,
                    PropertyId = quotation.PropertyId
                })
            );           
        }

        private void ValidateDate(List<DateTime> dateList, DateTime dateTimeZone)
        {
            if (dateList.Any(d => d.Date < dateTimeZone.Date))
            {
                Notification.Raise(Notification.DefaultBuilder
                    .WithMessage(AppConsts.LocalizationSourceName, PropertyCurrencyExchange.EntityError.PropertyCurrencyExchangeInvalidPropertyCurrencyExchangeDate)
                    .WithDetailedMessage(AppConsts.LocalizationSourceName, PropertyCurrencyExchange.EntityError.PropertyCurrencyExchangeInvalidPropertyCurrencyExchangeDate)
                    .Build()); return;
            }
        }

        private decimal CalculateExchangeRateToDolar(decimal propertyExchangeRate, decimal? propertyCurrencyExchangeRate)
        {
            return propertyCurrencyExchangeRate.Value / propertyExchangeRate;
        }

        private bool IsToDolarQuotation(Guid currencyId)
        {
            return _currencyReadRepository.IsToDolarQuotation(currencyId);
        }

        private void IsQuotationEqualsPropertyCurrency(InsertQuotationByPeriodDto quotationByPeriod, Guid propertyDefaultCurrency)
        {
            if(quotationByPeriod.CurrencyId == propertyDefaultCurrency)
            {
                Notification.Raise(Notification.DefaultBuilder
                         .WithMessage(AppConsts.LocalizationSourceName, PropertyCurrencyExchange.EntityError.PropertyCurrencyExchangeIsEqualToCurrencyQuotation)
                         .WithDetailedMessage(AppConsts.LocalizationSourceName, PropertyCurrencyExchange.EntityError.PropertyCurrencyExchangeIsEqualToCurrencyQuotation)
                         .Build());
            }
        }

        public void UpdateQuotationFuture(List<PropertyCurrencyExchangeDto> quotationList)
        {
            if (quotationList == null || quotationList.Count == 0)
                NotifyNullQuotationList();

            var vLst = quotationList.Where(e => e != null);

            var propertyCurrencyList = _propertyCurrencyExchangeReadRepository.GetAllByIds(vLst.Select(x => x.Id).ToList()).Where(e => e != null);

            foreach (var cotation in vLst)
            {
                if (cotation.Id == null || cotation.Id == Guid.Empty)
                {
                    var builderCurrencyChange = new PropertyCurrencyExchange.Builder(Notification);

                    builderCurrencyChange.WithCurrencyId(cotation.CurrencyId)
                    .WithExchangeRate(cotation.ExchangeRate)
                    .WithPropertyCurrencyExchangeDate(cotation.PropertyCurrencyExchangeDate)
                    .WithPropertyId(cotation.PropertyId);

                    var currencyId = PropertyCurrencyExchangeDomainService.InsertAndSaveChanges(builderCurrencyChange).Id;
                    if (Notification.HasNotification()) break;
                }
                else
                {
                    ValidateDtoAndId(cotation, cotation.Id, nameof(cotation), nameof(cotation.Id));

                    if (Notification.HasNotification()) break;


                    var propertyCurrencyOld = propertyCurrencyList.Where(x => x.Id.Equals(cotation.Id)).FirstOrDefault();

                    if (Notification.HasNotification()) break;

                    if (propertyCurrencyOld.PropertyCurrencyExchangeDate.Date < DateTime.UtcNow.ToZonedDateTimeLoggedUser())
                        ValidateIsDateIsFuture();

                    if (Notification.HasNotification()) break;

                    var builder = new PropertyCurrencyExchange.Builder(Notification);
                    builder.WithCurrencyId(propertyCurrencyOld.CurrencyId)
                    .WithId(cotation.Id)
                    .WithExchangeRate(cotation.ExchangeRate)
                    .WithPropertyCurrencyExchangeDate(propertyCurrencyOld.PropertyCurrencyExchangeDate)
                    .WithPropertyId(propertyCurrencyOld.PropertyId);


                    PropertyCurrencyExchangeDomainService.Update(builder);

                    if (Notification.HasNotification()) break;

                }
            }
        }

        private void NotifyNullQuotationList()
        {
            Notification.Raise(Notification.DefaultBuilder
                         .WithMessage(AppConsts.LocalizationSourceName, PropertyCurrencyExchange.EntityError.PropertyCurrencyExchangeQuotationListIsNull)
                         .WithDetailedMessage(AppConsts.LocalizationSourceName, PropertyCurrencyExchange.EntityError.PropertyCurrencyExchangeQuotationListIsNull)
                         .Build());

            return;
        }

        private void ValidateIsDateIsFuture()
        {
            Notification.Raise(Notification.DefaultBuilder
                          .WithMessage(AppConsts.LocalizationSourceName, PropertyCurrencyExchange.EntityError.PropertyCurrencyExchangeIsExistQuotationForPeriod)
                          .WithDetailedMessage(AppConsts.LocalizationSourceName, PropertyCurrencyExchange.EntityError.PropertyCurrencyExchangeIsExistQuotationForPeriod)
                          .Build());
        }

        private void ValidateRangeDate(DateTime startDate, DateTime? endDate)
        {
            var nowDateProperty = DateTime.UtcNow.ToZonedDateTimeLoggedUser().Date;

            if (startDate.Date < nowDateProperty || endDate.Value.Date < nowDateProperty)
                NotifyInvalidRangeDate();


            if (Notification.HasNotification()) return;

            if (startDate.Date > endDate.Value.Date)
                NotifyInvalidRangeDate();

        }

        private void IsQuotationByPeriodList(InsertQuotationByPeriodDto quotationByPeriod)
        {
            if (_propertyCurrencyExchangeReadRepository.IsQuotationByPeriodList(quotationByPeriod.StartDate, quotationByPeriod.EndDate, quotationByPeriod.CurrencyId, quotationByPeriod.PropertyId))
            {
                _propertyCurrencyExchangeReadRepository.DeleteByPeriodList(quotationByPeriod);
            };
        }

        public List<QuotationCurrentDto> GetCurrentQuotationList()
        {
            int propertyId = 0;
            int.TryParse(_applicationUser.PropertyId, out propertyId);

            return _propertyCurrencyExchangeReadRepository.GetCurrentQuotationList(propertyId, null);
        }

        public void PatchCurrentQuotation(QuotationCurrentDto quotationCurrentDto)
        {
            if (quotationCurrentDto == null)
                NotifyNullParameter();

            if (Notification.HasNotification()) return;

            ValidateZeroValue(quotationCurrentDto.PropertyExchangeRate);

            if (Notification.HasNotification()) return;

            var propertyDefaultCurrency = Guid.Parse(_propertyReadRepository.GetPropertyDefaultCurrency(int.Parse(_applicationUser.PropertyId)));

            IsQuotationEqualsPropertyCurrency(new InsertQuotationByPeriodDto() { CurrencyId = quotationCurrentDto.CurrencyId }, propertyDefaultCurrency);

            if (Notification.HasNotification()) return;

            var dateTimeZone = DateTime.UtcNow.ToZonedDateTimeLoggedUser();

            ValidatePatchDate(quotationCurrentDto.CurrentDate, dateTimeZone);

            if (Notification.HasNotification()) return;

            var propertyId = int.Parse(_applicationUser.PropertyId);

            if (IsToDolarQuotation(quotationCurrentDto.CurrencyId))
            {
                var propertyCurrencyExchangeDtoList = new List<PropertyCurrencyExchangeDto>();

                var quotationCurrencyDtoList = _propertyCurrencyExchangeReadRepository
                                                    .GetPropertyQuotationByPropertyId(propertyId,
                                                        quotationCurrentDto.CurrentDate, quotationCurrentDto.CurrentDate, true)
                                                        .Where(x => x.CurrencyId != quotationCurrentDto.CurrencyId).ToList();

                var oldPropertyCurrencyExchange = quotationCurrencyDtoList.FirstOrDefault(x => x.CurrencyId == propertyDefaultCurrency)?.ExchangeRate;                

                if (!oldPropertyCurrencyExchange.HasValue)
                {
                    Notification.Raise(Notification.DefaultBuilder
                        .WithMessage(AppConsts.LocalizationSourceName, PropertyCurrencyExchange.EntityError.PropertyCurrencyExchangeRateDoesNotExistForDate)
                        .WithDetailedMessage(AppConsts.LocalizationSourceName, PropertyCurrencyExchange.EntityError.PropertyCurrencyExchangeRateDoesNotExistForDate)
                        .Build()); return;
                }

                CaculateCurrencies(quotationCurrencyDtoList, oldPropertyCurrencyExchange.Value, quotationCurrentDto.PropertyExchangeRate.Value, propertyDefaultCurrency);

                if (quotationCurrencyDtoList.Any())
                    _propertyCurrencyExchangeRepository.UpdateCurrenciesAndSaveChanges(quotationCurrencyDtoList);
            }
            else
            {
                var quotationCurrencyDtoList = _propertyCurrencyExchangeReadRepository
                                                    .GetPropertyQuotationByPropertyId(int.Parse(_applicationUser.PropertyId),
                                                        quotationCurrentDto.CurrentDate, quotationCurrentDto.CurrentDate);
                
                var propertyCurrencyExchangeRate = quotationCurrencyDtoList?.FirstOrDefault(p => p.CurrentDate.Date == quotationCurrentDto.CurrentDate.Date)?.ExchangeRate;

                if (propertyCurrencyExchangeRate == null)
                {
                    Notification.Raise(Notification.DefaultBuilder
                    .WithMessage(AppConsts.LocalizationSourceName, PropertyCurrencyExchange.EntityError.PropertyCurrencyExchangeRateDoesNotExistForPeriod)
                    .WithDetailedMessage(AppConsts.LocalizationSourceName, PropertyCurrencyExchange.EntityError.PropertyCurrencyExchangeRateDoesNotExistForPeriod)
                    .Build()); return;
                }

                var propertyExchangeRate = CalculateExchangeRateToDolar(quotationCurrentDto.PropertyExchangeRate.Value, propertyCurrencyExchangeRate);

                var propertyCurrencyExchangeDto = new PropertyCurrencyExchangeDto
                {
                    Id = quotationCurrentDto.Id,
                    CurrencyId = quotationCurrentDto.CurrencyId,
                    ExchangeRate = propertyExchangeRate,
                    PropertyCurrencyExchangeDate = quotationCurrentDto.CurrentDate,
                    PropertyId = propertyId
                };

                _propertyCurrencyExchangeRepository.UpdateAndSaveChanges(propertyCurrencyExchangeDto);
            }
        }

        private void ValidateZeroValue(decimal? propertyExchangeRate)
        {
            if(!propertyExchangeRate.HasValue || propertyExchangeRate.Value == 0)
            {
                Notification.Raise(Notification.DefaultBuilder
                   .WithMessage(AppConsts.LocalizationSourceName, PropertyCurrencyExchange.EntityError.PropertyCurrencyExchangeRateMustHaveValue)
                   .WithDetailedMessage(AppConsts.LocalizationSourceName, PropertyCurrencyExchange.EntityError.PropertyCurrencyExchangeRateMustHaveValue)
                   .Build()); return;
            }
        }

        private void CaculateCurrencies(List<QuotationCurrentDto> quotationCurrencyDtoList, decimal oldPropertyExchangeRate, decimal newPropertyExchangeRate, Guid propertyCurrencyId)
        {
            quotationCurrencyDtoList.ForEach(currency => currency.ExchangeRate = currency.CurrencyId == propertyCurrencyId ? newPropertyExchangeRate :
                                                         1 / (oldPropertyExchangeRate / currency.ExchangeRate.Value) * newPropertyExchangeRate);
        }

        private void ValidatePatchDate(DateTime currentDate, DateTime dateTimeZone)
        {
            if (currentDate.Date < dateTimeZone.Date)
            {
                Notification.Raise(Notification.DefaultBuilder
                    .WithMessage(AppConsts.LocalizationSourceName, PropertyCurrencyExchange.EntityError.PropertyCurrencyExchangeInvalidDate)
                    .WithDetailedMessage(AppConsts.LocalizationSourceName, PropertyCurrencyExchange.EntityError.PropertyCurrencyExchangeInvalidDate)
                    .Build()); return;
            }
        }
    }
}
