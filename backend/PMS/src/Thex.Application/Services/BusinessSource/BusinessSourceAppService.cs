﻿using Thex.Application.Adapters;
using Thex.Domain.Entities;
using Thex.Application.Interfaces;
using Thex.Dto;
using System.Threading.Tasks;

using Thex.Domain.Services.Interfaces;

namespace Thex.Application.Services
{
    using Thex.Common;
    using Thex.Infra.ReadInterfaces;
    using Thex.Kernel;
    using Tnf.Dto;
    using Tnf.Notifications;

    public class BusinessSourceAppService : ScaffoldBusinessSourceAppService, IBusinessSourceAppService
    {
        protected IBusinessSourceDomainService _businessSourceDomainService;
        protected IBusinessSourceReadRepository _businessSourceReadRepository;
        protected readonly IApplicationUser _applicationUser;

        public BusinessSourceAppService(
            IBusinessSourceAdapter businessSourceAdapter,
            IBusinessSourceDomainService businessSourceDomainService,
            IBusinessSourceReadRepository businessSourceReadRepository,
            IChainReadRepository chainReadRepository,
            IChainBusinessSourceAppService chainBusinessSourceAppService,
            IApplicationUser applicationUser,
            INotificationHandler notificationHandler)
            : base(businessSourceAdapter, businessSourceDomainService, chainReadRepository, chainBusinessSourceAppService, applicationUser, businessSourceReadRepository, notificationHandler)
        {
            _businessSourceDomainService = businessSourceDomainService;
            _businessSourceReadRepository = businessSourceReadRepository;
            _applicationUser = applicationUser;
        }

        public IListDto<BusinessSourceDto> GetAllByPropertyId(GetAllBusinessSourceDto request, int propertyId)
        {
            var nullResponseInstance = new ListDto<BusinessSourceDto>();
            if (propertyId <= 0)
            {
                NotifyIdIsMissing();
                return nullResponseInstance;
            }

            int chainId = _chainReadRepository.GetChainByPropertyId(propertyId).Id;
            return  _businessSourceReadRepository.GetAllByChainId(request, chainId);
        }

        public void ToggleActivation(int id)
        {
            if (id <= 0)
            {
                NotifyIdIsMissing();
            }

            _businessSourceDomainService.ToggleActivation(id);
        }


        public override async Task Delete(int id)
        {
            if (id < 0)
            {
                NotifyIdIsMissing();
                return;
            }

            if (_businessSourceReadRepository.ValidateBusinessSourceWithReservation(id, int.Parse(_applicationUser.PropertyId)))
            {
                Notification.Raise(Notification.DefaultBuilder
               .WithMessage(AppConsts.LocalizationSourceName, BusinessSource.EntityError.BusinessSourceCannotDeleteWithReservation)
               .WithDetailedMessage(AppConsts.LocalizationSourceName, BusinessSource.EntityError.BusinessSourceCannotDeleteWithReservation)
               .Build());

                return;
            }

            await _businessSourceDomainService.DeleteAsync(e => e.Id == id);
        }
    }
}
