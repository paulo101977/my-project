﻿using System;
using Thex.Application.Adapters;
using Thex.Domain.Entities;
using Thex.Application.Interfaces;
using Tnf.Domain.Services;
using Tnf.Notifications;

namespace Thex.Application.Services
{
    public class ApplicationParameterAppService : ScaffoldApplicationParameterAppService, IApplicationParameterAppService
    {
        public ApplicationParameterAppService(
            IApplicationParameterAdapter applicationParameterAdapter,
            IDomainService<ApplicationParameter> applicationParameterDomainService,
            INotificationHandler notificationHandler)
            : base(applicationParameterAdapter, applicationParameterDomainService, notificationHandler)
        {
        }
    }
}
