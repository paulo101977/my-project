﻿using System;
using Thex.Application.Adapters;
using Thex.Domain.Entities;
using Thex.Application.Interfaces;
using Tnf.Domain.Services;
using Thex.Infra.ReadInterfaces;
using Thex.Dto;
using Tnf.Dto;
using Tnf.Notifications;
using Thex.Kernel;

namespace Thex.Application.Services
{
    public class CountrySubdivisionTranslationAppService : ScaffoldCountrySubdivisionTranslationAppService, ICountrySubdivisionTranslationAppService
    {

        protected readonly IApplicationUser _applicationUser;

        public CountrySubdivisionTranslationAppService(
            ICountrySubdivisionTranslationAdapter countrySubdivisionTranslationAdapter,
            IDomainService<CountrySubdivisionTranslation> countrySubdivisionTranslationDomainService,
            ICountrySubdivisionTranslationReadRepository countrySubdivisionTranslationReadRepository,
            IApplicationUser applicationUser,
            INotificationHandler notificationHandler)
            : base(countrySubdivisionTranslationAdapter, countrySubdivisionTranslationDomainService, countrySubdivisionTranslationReadRepository, notificationHandler)
        {
             _applicationUser = applicationUser;
        }

        public IListDto<CountrySubdivisionTranslationDto> GetAllNationalities(GetAllCountrySubdivisionTranslationDto request)
        {
            ValidateRequestAllDto(request, nameof(request));

            if (Notification.HasNotification())
                return null;

            request.Language = _applicationUser.PreferredLanguage;

            var response = _countrySubdivisionTranslationReadRepository.GetNationalities(request);
            return response;
        }

        public CountrySubdivisionTranslationDto GetNationality(int Nationality)
        {
            return Nationality > 0 ? _countrySubdivisionTranslationReadRepository.GetNationalityByIdAndProperty(Nationality, _applicationUser.PropertyId.TryParseToInt32()) : null;
        }
    }
}
