﻿using System;
using Thex.Application.Adapters;
using Thex.Domain.Entities;
using Thex.Application.Interfaces;
using Tnf.Domain.Services;
using Thex.Dto;

using System.Collections.Generic;


namespace Thex.Application.Services
{

    using Thex.Domain.Interfaces.Repositories;
    using Tnf.Notifications;

    public class LocationAppService : ScaffoldLocationAppService, ILocationAppService
    {
        private readonly ILocationRepository _locationRepository;
        private readonly ICountrySubdivisionAppService _countrySubdivisionAppService;

        public LocationAppService(
            ILocationRepository locationRepository,
            ILocationAdapter locationAdapter,
            IDomainService<Location> locationDomainService,
            INotificationHandler notificationHandler,
            ICountrySubdivisionAppService countrySubdivisionAppService)
            : base(locationAdapter, locationDomainService, notificationHandler)
        {
            _locationRepository = locationRepository;
            _countrySubdivisionAppService = countrySubdivisionAppService;
        }

        public LocationDto Get(DefaultIntRequestDto id)
        {
            if (id.Id < 0)
            {
                NotifyIdIsMissing();
                return LocationDto.NullInstance;
            }

            Location location = LocationDomainService.Get(id);
            return location.MapTo<LocationDto>();
        }

        public ICollection<Location> ToDomain(ICollection<LocationDto> locations)
        {
            var locationList = new HashSet<Location>();
            if (locations != null)
            {
                foreach (var locationDto in locations)
                {
                    var locationBuilder = this.GetBuilder(locationDto);
                    var location = locationBuilder.Build();
                    locationList.Add(location);
                }
            }
            return locationList;
        }

        /// <summary>
        /// Creates a <see cref="LocationBuilder"/> passing a dto with parents
        /// </summary>
        /// <param name="LocationDto">An instance of the <see cref="LocationDto"/> class</param>
        /// <returns>This <see cref="LocationBuilder"/> instance</returns>
        public Location.Builder GetBuilder(LocationDto locationDto)
        {
            var builder = new Location.Builder(Notification);

            builder
                .WithId(locationDto.Id)
                .WithOwnerId(locationDto.OwnerId)
                .WithLatitude(locationDto.Latitude)
                .WithLongitude(locationDto.Longitude)
                .WithStreetName(locationDto.StreetName)
                .WithStreetNumber(locationDto.StreetNumber)
                .WithCountryCode(locationDto.CountryCode)
                .WithAdditionalAddressDetails(locationDto.AdditionalAddressDetails);

            return builder;
        }

        public Location.Builder GetBuilder(LocationDto locationDto, Guid personId)
        {
            var builder = new Location.Builder(Notification);

            builder
                .WithId(locationDto.Id)
                .WithOwnerId(personId)
                .WithLatitude(locationDto.Latitude)
                .WithLongitude(locationDto.Longitude)
                .WithStreetName(locationDto.StreetName)
                .WithStreetNumber(locationDto.StreetNumber)
                .WithCountryCode(locationDto.CountryCode)
                .WithAdditionalAddressDetails(locationDto.AdditionalAddressDetails);

            return builder;
        }

        public new LocationDto Create(LocationDto location)
        {
            if (location == null)
            {
                NotifyNullParameter();
                return LocationDto.NullInstance;
            }

            location.Id = LocationDomainService.InsertAndSaveChanges(GetBuilder(location)).Id;

            return location;
        }

        public void CreateLocationList(ICollection<LocationDto> locationDtoList, Guid personId)
        {
            IList<Location> locationList = new List<Location>();

            foreach (var locationDto in locationDtoList)
            {
                var countrySubdivision = _countrySubdivisionAppService.CreateCountrySubdivisionTranslateAndGetCityId(locationDto, locationDto.BrowserLanguage);
                locationDto.CityId = countrySubdivision.Item1;
                locationDto.StateId = countrySubdivision.Item2;
                locationDto.CountryId = countrySubdivision.Item3;

                Location location = GetBuilder(locationDto, personId)
                    .WithLocationCategoryId(locationDto.LocationCategoryId)
                    .WithNeighborhood(locationDto.Neighborhood)
                    .WithPostalCode(locationDto.PostalCode)
                    .WithCityId(locationDto.CityId)
                    .WithCountryId(locationDto.CountryId)
                    .WithStateId(locationDto.StateId)
                    .WithCountryCode(locationDto.CountryCode).Build();

                locationList.Add(location);
            }

            if (Notification.HasNotification()) return;

            _locationRepository.AddRange(locationList);
        }

        public LocationDto Update(LocationDto location)
        {
            if (location == null)
            {
                NotifyNullParameter();
                return LocationDto.NullInstance;
            }

            if (location.Id <= 0)
                NotifyIdIsMissing();

            if (Notification.HasNotification())
                return LocationDto.NullInstance;

            LocationDomainService.Update(GetBuilder(location));

            return location;
        }

        public new void Delete(int id)
        {
            if (id < 0)
            {
                NotifyIdIsMissing();
                return;
            }

            var location = LocationDomainService.Get(new DefaultIntRequestDto(id));
            if (location == null)
            {
                NotifyWhenEntityNotExist("Location");
                return;
            }

            LocationDomainService.Delete(location);
        }

        public void UpdateLocationList(IList<LocationDto> locationDtoList, string ownerId)
        {
            var locationList = new List<Location>();

            foreach (var locationDto in locationDtoList)
                locationList.Add(GetLocationForUpdated(locationDto, ownerId));

            if (Notification.HasNotification() || locationList.Count == 0)
                return;

            _locationRepository.UpdateLocationList(locationList, Guid.Parse(ownerId));
        }

        public Location GetLocationForUpdated(LocationDto locationDto, string personId)
        {
            Location location = GetBuilder(locationDto, Guid.Parse(personId))
                .WithLocationCategoryId(locationDto.LocationCategoryId)
                .WithNeighborhood(locationDto.Neighborhood)
                .WithPostalCode(locationDto.PostalCode)
                .WithCityId(locationDto.CityId)
                .WithCountryId(locationDto.CountryId)
                .WithStateId(locationDto.StateId)
                .WithStreetName(locationDto.StreetName)
                .WithStreetNumber(locationDto.StreetNumber)
                .WithAdditionalAddressDetails(locationDto.AdditionalAddressDetails)
                .WithCountryCode(locationDto.CountryCode).Build();

            return location;
        }

        public void CreateOrUpdateGuestRegistrationLocation(LocationDto locationDto, string ownerId)
        {
            var location = GetLocationForUpdated(locationDto, ownerId);

            if (Notification.HasNotification())
                return;

            _locationRepository.CreateOrUpdate(location, Guid.Parse(ownerId));
        }

        public void CreateOrUpdatePersonHeaderLocation(Location location, Guid ownerId)
            => _locationRepository.CreateOrUpdate(location, ownerId);
    }
}