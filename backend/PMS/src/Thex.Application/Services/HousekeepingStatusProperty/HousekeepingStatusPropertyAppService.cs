﻿using System;
using Thex.Application.Adapters;
using Thex.Domain.Entities;
using Thex.Application.Interfaces;
using Tnf.Domain.Services;
using Thex.Dto;
using Tnf.Dto;
using Thex.Infra.ReadInterfaces;
using Thex.Domain.Interfaces.Repositories;
using Thex.Dto.Housekeeping;
using Thex.Common.Enumerations;
using System.Collections.Generic;
using Tnf.Notifications;
using Tnf.Repositories.Uow;
using Thex.Common;

namespace Thex.Application.Services
{
    public class HousekeepingStatusPropertyAppService : ScaffoldHousekeepingStatusPropertyAppService, IHousekeepingStatusPropertyAppService
    {
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IHousekeepingStatusPropertyReadRepository _housekeepingStatusReadProperty;
        private readonly IHousekeepingStatusPropertyRepository _housekeepingStatusPropertyRepository;
        private readonly IPropertyParameterReadRepository _propertyParameterReadRepository;
        IDomainService<RoomBlocking> _roomBlockingDomainService;
        IDomainService<Room> _roomDomainService;

        public HousekeepingStatusPropertyAppService(
            IUnitOfWorkManager unitOfWorkManager,
            IHousekeepingStatusPropertyAdapter housekeepingStatusPropertyAdapter,
            IDomainService<HousekeepingStatusProperty> housekeepingStatusPropertyDomainService,
            IHousekeepingStatusPropertyReadRepository housekeepingStatusReadProperty,
            IHousekeepingStatusPropertyRepository housekeepingStatusPropertyRepository,
            IDomainService<RoomBlocking> roomBlockingDomainService,
            IDomainService<Room> roomDomainService,
            INotificationHandler notificationHandler,
            IPropertyParameterReadRepository propertyParameterReadRepository)
            : base(housekeepingStatusPropertyAdapter, housekeepingStatusPropertyDomainService, notificationHandler)
        {
            _unitOfWorkManager = unitOfWorkManager;
            _housekeepingStatusReadProperty = housekeepingStatusReadProperty;
            _housekeepingStatusPropertyRepository = housekeepingStatusPropertyRepository;
            _roomBlockingDomainService = roomBlockingDomainService;
            _roomDomainService = roomDomainService;
            _propertyParameterReadRepository = propertyParameterReadRepository;
        }

        public HousekeepingStatusPropertyDto Get(Guid guid)
        {
            return _housekeepingStatusReadProperty.Get(guid);
        }

        public IListDto<HousekeepingStatusPropertyDto> GetAll(GetAllHousekeepingStatusPropertyDto request, int propertyId)
        {
            return _housekeepingStatusReadProperty.GetAll(request, propertyId);
        }

        public void ToggleAndSaveIsActive(Guid id)
        {
            using (var uow = _unitOfWorkManager.Begin())
            {
                _housekeepingStatusPropertyRepository.ToggleAndSaveIsActive(id);
                uow.Complete();
            }
        }

        public new void Update(HousekeepingStatusPropertyDto housekeepingStatusProperty)
        {
            using (var uow = _unitOfWorkManager.Begin())
            {
                var builder = HousekeepingStatusPropertyAdapter.Map(housekeepingStatusProperty);
                _housekeepingStatusPropertyRepository.Update(builder.Build());

                uow.Complete();
            }
        }

        public void HousekeepingStatusPropertyCreate(HousekeepingStatusPropertyDto housekeepingStatusProperty)
        {
            using (var uow = _unitOfWorkManager.Begin())
            {
                var builder = HousekeepingStatusPropertyAdapter.Map(housekeepingStatusProperty);
                _housekeepingStatusPropertyRepository.HousekeepingStatusPropertyCreate(builder.Build());

                uow.Complete();
            }
        }

        public void HousekeepingStatusPropertyDelete(Guid id)
        {
            using (var uow = _unitOfWorkManager.Begin())
            {
                _housekeepingStatusPropertyRepository.HousekeepingStatusPropertyDelete(id);
                uow.Complete();
            }
        }

        public IListDto<GetAllHousekeepingAlterStatusDto> GetAllHousekeepingAlterStatusRoom(GetAllHousekeepingAlterStatusRoomDto request, int propertyId)
        {
            DateTime? systemDate = _propertyParameterReadRepository.GetSystemDate(propertyId);

            if (systemDate == null)
            {
                Notification.Raise(Notification.DefaultBuilder
                .WithMessage(AppConsts.LocalizationSourceName, PropertyAuditProcess.EntityError.PropertyAuditProcessInvalidPropertySystemDate)
                .WithDetailedMessage(AppConsts.LocalizationSourceName, PropertyAuditProcess.EntityError.PropertyAuditProcessInvalidPropertySystemDate)
                .Build());
            }

            if (Notification.HasNotification()) return null;

            return _housekeepingStatusReadProperty.GetAllHousekeepingAlterStatusRoom(request, propertyId, systemDate.GetValueOrDefault());
        }

        public IListDto<HousekeepingStatusPropertyDto> GetAllHousekeepingStatusCreateBySystem(int propertyId)
        {
            return _housekeepingStatusReadProperty.GetAllHousekeepingStatusCreateBySystem(propertyId);
        }

        public void UpdateRoomForHousekeeppingStatus(int roomId, Guid housekeepingStatusPropertyId)
        {
            using (var uow = _unitOfWorkManager.Begin())
            {
                _housekeepingStatusPropertyRepository.UpdateRoomForHousekeeppingStatus(roomId, housekeepingStatusPropertyId);
                uow.Complete();
            }

        }

        public void UpdateManyRoomForHousekeeppingStatus(List<int> roomIds, Guid housekeepingStatusPropertyId)
        {
            using (var uow = _unitOfWorkManager.Begin())
            {
                _housekeepingStatusPropertyRepository.UpdateManyRoomForHousekeeppingStatus(roomIds, housekeepingStatusPropertyId);
                uow.Complete();
            }
        }

        public Guid GetHousekeepingStatusIdDirty()
        {
            return _housekeepingStatusReadProperty.GetHousekeepingStatusIdDirty();
        }

        public Guid GetHousekeepingStatusIdMaintenance()
        {
            return _housekeepingStatusReadProperty.GetHousekeepingStatusIdMaintenance();
        }

    }
}
