﻿using System;
using Thex.Application.Adapters;
using Thex.Domain.Entities;
using Thex.Application.Interfaces;
using Tnf.Domain.Services;
using Tnf.Notifications;

namespace Thex.Application.Services
{
    public class RateTypeAppService : ScaffoldRateTypeAppService, IRateTypeAppService
    {
        public RateTypeAppService(
            IRateTypeAdapter rateTypeAdapter,
            IDomainService<RateType> rateTypeDomainService,
            INotificationHandler notificationHandler)
                : base(rateTypeAdapter, rateTypeDomainService, notificationHandler)
        {
        }
    }
}
