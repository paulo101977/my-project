﻿using System;
using Thex.Application.Adapters;
using Thex.Domain.Entities;
using Thex.Application.Interfaces;
using Tnf.Domain.Services;
using Tnf.Notifications;

namespace Thex.Application.Services
{
    public class ParameterTypeAppService : ScaffoldParameterTypeAppService, IParameterTypeAppService
    {
        public ParameterTypeAppService(
            IParameterTypeAdapter parameterTypeAdapter,
            IDomainService<ParameterType> parameterTypeDomainService,
            INotificationHandler notificationHandler)
            : base(parameterTypeAdapter, parameterTypeDomainService, notificationHandler)
        {
        }
    }
}
