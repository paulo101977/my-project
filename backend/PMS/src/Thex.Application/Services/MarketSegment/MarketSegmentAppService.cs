﻿using Thex.Application.Adapters;
using Thex.Application.Interfaces;

namespace Thex.Application.Services
{
    using Thex.Infra.ReadInterfaces;
    using Thex.Dto;
    using Tnf.Dto;
    using System.Threading.Tasks;

    using Thex.Domain.Services.Interfaces;
    using Tnf.Notifications;
    using Thex.Kernel;
    using Thex.Common;
    using Thex.Domain.Entities;

    public class MarketSegmentAppService : ScaffoldMarketSegmentAppService, IMarketSegmentAppService
    {
        protected readonly IMarketSegmentReadRepository _marketSegmentReadRepository;
        protected readonly IChainReadRepository _chainReadRepository;
        protected readonly IChainMarketSegmentAppService _chainMarketSegmentAppService;
        protected readonly IApplicationUser _applicationUser;

        public MarketSegmentAppService(
            IMarketSegmentReadRepository marketSegmentReadRepository,
            IChainReadRepository chainReadRepository,
            IMarketSegmentAdapter marketSegmentAdapter,
            IMarketSegmentDomainService marketSegmentDomainService,
            IChainMarketSegmentAppService chainMarketSegmentAppService,
            IApplicationUser applicationUser,
            INotificationHandler notificationHandler)
            : base(marketSegmentAdapter, marketSegmentDomainService, notificationHandler, chainReadRepository, marketSegmentReadRepository, applicationUser)
        {
            _marketSegmentReadRepository = marketSegmentReadRepository;
            _chainReadRepository = chainReadRepository;
            _chainMarketSegmentAppService = chainMarketSegmentAppService;
            _applicationUser = applicationUser;
        }

        public IListDto<MarketSegmentDto> GetAllByPropertyId(GetAllMarketSegmentDto request, int propertyId)
        {
            var nullResponseInstance = new ListDto<MarketSegmentDto>();
            if (propertyId <= 0)
            {
                NotifyIdIsMissing();
                return nullResponseInstance;
            }

            var chain = _chainReadRepository.GetChainByPropertyId(propertyId);

            if(chain == null)
            {
                NotifyNullParameter();
                return nullResponseInstance;
            }

            return _marketSegmentReadRepository.GetAllByChainId(request, chain.Id);
        }

        public override async Task<MarketSegmentDto> Create(MarketSegmentDto dto)
        {
            ValidateDto<MarketSegmentDto>(dto, nameof(dto));

            if (Notification.HasNotification())
                return MarketSegmentDto.NullInstance;

            if (dto.PropertyId == 0)
            {
                NotifyNullParameter();
                return MarketSegmentDto.NullInstance;
            }

            ValidateIfDescriptionAlreadyExists(dto);

            if (Notification.HasNotification())
                return MarketSegmentDto.NullInstance;

            var marketSegmentBuilder = MarketSegmentAdapter.Map(dto);

            int chainId = _chainReadRepository.GetChainByPropertyId(dto.PropertyId).Id;

            var segment = MarketSegmentDomainService.InsertAndSaveChanges(marketSegmentBuilder);

            dto.Id = segment.Id;

            _chainMarketSegmentAppService.Create(dto, chainId);

            return dto;
        }       

        public void ToggleActivation(int id)
        {
            if (id <= 0)
            {
                NotifyIdIsMissing();
            }

            (MarketSegmentDomainService as IMarketSegmentDomainService).ToggleActivation(id);
        }

        public MarketSegmentDto GetById(int id)
        {
            return _marketSegmentReadRepository.GetById(id);
        }
             
        public override async Task Delete(int id)
        {
            if (id < 0)
            {
                NotifyIdIsMissing();
                return;
            }

            if(_marketSegmentReadRepository.ValidateMarketSegmentWithReservation(id, int.Parse(_applicationUser.PropertyId)))
            {
                Notification.Raise(Notification.DefaultBuilder
               .WithMessage(AppConsts.LocalizationSourceName, MarketSegment.EntityError.MarketSegmentCannotDeleteWithReservation)
               .WithDetailedMessage(AppConsts.LocalizationSourceName, MarketSegment.EntityError.MarketSegmentCannotDeleteWithReservation)
               .Build());

                return;
            }

            await (MarketSegmentDomainService as IMarketSegmentDomainService).DeleteAsync(e => e.Id == id);
        }
    }
}
