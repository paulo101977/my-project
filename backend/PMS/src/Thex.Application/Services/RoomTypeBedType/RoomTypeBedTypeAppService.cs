﻿using System;
using Thex.Application.Adapters;
using Thex.Domain.Entities;
using Thex.Application.Interfaces;
using Tnf.Domain.Services;
using Tnf.Notifications;

namespace Thex.Application.Services
{
    public class RoomTypeBedTypeAppService : ScaffoldRoomTypeBedTypeAppService, IRoomTypeBedTypeAppService
    {
        public RoomTypeBedTypeAppService(
            IRoomTypeBedTypeAdapter roomTypeBedTypeAdapter,
            IDomainService<RoomTypeBedType> roomTypeBedTypeDomainService,
            INotificationHandler notificationHandler)
            : base(roomTypeBedTypeAdapter, roomTypeBedTypeDomainService, notificationHandler)
        {
        }
    }
}
