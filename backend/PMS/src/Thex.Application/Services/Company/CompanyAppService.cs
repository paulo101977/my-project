﻿using System;
using Thex.Application.Adapters;
using Thex.Domain.Entities;
using Thex.Application.Interfaces;
using Tnf.Domain.Services;
using Thex.Dto;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;

using Thex.Common.Enumerations;
using Tnf.Notifications;

namespace Thex.Application.Services
{
    public class CompanyAppService : ScaffoldCompanyAppService, ICompanyAppService
    {
        private readonly IDomainService<Person> _personDomainService;
        public CompanyAppService(
            ICompanyAdapter companyAdapter,
            IDomainService<Company> companyDomainService,
            IDomainService<Person> personDomainService,
            INotificationHandler notificationHandler)
            : base(companyAdapter, companyDomainService, notificationHandler)
        {
            _personDomainService = personDomainService;
        }

        #region CreateOrUpdateCompanyAndRelationShips

        private async Task<CompanyDto> CreateOrUpdateCompanyAndRelationShips(CompanyDto companyDto, IList<Guid> peopleIds = null, bool update = false)
        {
            peopleIds = peopleIds ?? new List<Guid>();

            if (companyDto != null && companyDto.Person != null)
            {
                CompanyDto parentCompany = null;
                if (companyDto.ParentCompany != null && companyDto.ParentCompany.Person != null)
                {
                    parentCompany = await CreateOrUpdateCompanyAndRelationShips(companyDto.ParentCompany, peopleIds, update).ConfigureAwait(false);
                    if (Notification.HasNotification())
                        return companyDto;
                }

                // Casos onde duas ou mais companies tenham a mesma person
                var refPersonId = peopleIds.FirstOrDefault(w => w != Guid.Empty && w == companyDto.Person.Id);
                if (refPersonId == Guid.Empty)
                {
                    // TODO: Construtor de PersonBuilder pode receber uma instancia de Person podendo ser usado para atualização da entidade
                    // Ex:
                    //      PersonBuilder personBuilder = new PersonBuilder(_personDomainService.Get(new DefaultGuidRequestDto(companyDto.Person.Id)));
                    //
                    // nesse caso presuponho que nao haverá atualização de apenas uma propriedade da entidade, mas de todos os seus campos
                    var personBuilder = new Person.Builder(Notification);

                    personBuilder
                        .WithId(companyDto.Person.Id)
                        .WithFirstName(companyDto.Person.FirstName)
                        .WithLastName(companyDto.Person.LastName)
                        .WithPersonType(PersonTypeEnum.Legal);

                    if (!update)
                        companyDto.Person.Id = (await _personDomainService.InsertAndSaveChangesAsync(personBuilder)).Id;
                    else
                        await _personDomainService.UpdateAsync(personBuilder).ConfigureAwait(false);

                    peopleIds.Add(companyDto.Person.Id);
                }

                if (Notification.HasNotification())
                    return companyDto;

                var builder = new Company.Builder(Notification);

                builder
                    .WithShortName(companyDto.ShortName)
                    .WithTradeName(companyDto.TradeName)
                    .WithId(companyDto.Id)
                    .WithParentCompanyId(parentCompany?.Id)
                    .WithPersonId(companyDto.Person.Id);

                if (!update)
                    companyDto.Id = (await CompanyDomainService.InsertAndSaveChangesAsync(builder)).Id;
                else
                    await CompanyDomainService.UpdateAsync(builder).ConfigureAwait(false);
            }

            return companyDto;
        }

        #endregion
        public override async Task<CompanyDto> Create(CompanyDto dto)
        {
            if (dto == null)
            { 
                NotifyNullParameter();
                return CompanyDto.NullInstance;
            }

            ValidateDto(dto, nameof(dto));

            if (Notification.HasNotification())
                return CompanyDto.NullInstance;

            dto = await CreateOrUpdateCompanyAndRelationShips(dto, update: false).ConfigureAwait(false);

            return dto;
        }

        public override async Task<CompanyDto> Update(int id, CompanyDto dto)
        {
            if (dto == null)
            { 
                NotifyNullParameter();
                return CompanyDto.NullInstance;
            }

            if (dto.Id <= 0)
            {
                NotifyIdIsMissing();
                return CompanyDto.NullInstance;
            }

            ValidateDtoAndId(dto, id, nameof(dto), nameof(id));
            dto.Id = id;

            if (Notification.HasNotification())
                return CompanyDto.NullInstance;

            dto = await CreateOrUpdateCompanyAndRelationShips(dto, update: true).ConfigureAwait(false); ;

            return dto;
        }
    }
}
