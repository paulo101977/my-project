﻿using System;
using System.Collections.Generic;
using Thex.Application.Adapters;
using Thex.Application.Interfaces;
using Thex.Domain.Interfaces.Repositories;
using Thex.Dto;
using Thex.Infra.ReadInterfaces;
using Thex.Kernel;
using Tnf.Notifications;

namespace Thex.Application.Services
{
    public class PropertyGuestPolicyAppService : ApplicationServiceBase, IPropertyGuestPolicyAppService
    {
        private readonly IPropertyGuestPolicyAdapter _propertyPolicyAdapter;
        private readonly IPropertyGuestPolicyRepository _propertyGuestPolicyRepository;
        private readonly IPropertyGuestPolicyReadRepository _propertyGuestPolicyReadRepository;
        private readonly IApplicationUser _applicationUser;

        public PropertyGuestPolicyAppService(INotificationHandler notificationHandler, 
                                             IPropertyGuestPolicyAdapter propertyPolicyAdapter,
                                             IPropertyGuestPolicyRepository propertyGuestPolicyRepository,
                                             IPropertyGuestPolicyReadRepository propertyGuestPolicyReadRepository,
                                             IApplicationUser applicationUser) :
        base(notificationHandler)
        {
            _propertyPolicyAdapter = propertyPolicyAdapter;
            _propertyGuestPolicyRepository = propertyGuestPolicyRepository;
            _propertyGuestPolicyReadRepository = propertyGuestPolicyReadRepository;
            _applicationUser = applicationUser;
        }

        public void Create(PropertyGuestPolicyDto propertyGuestPolicy)
        {
            ValidateDto(propertyGuestPolicy, nameof(propertyGuestPolicy));

            if (Notification.HasNotification()) return;

            propertyGuestPolicy.PropertyId = int.Parse(_applicationUser.PropertyId);
            propertyGuestPolicy.PolicyTypeId = (int)Common.Enumerations.PolicyType.GuestPolicies;

            var builder = _propertyPolicyAdapter.Map(propertyGuestPolicy);
            _propertyGuestPolicyRepository.Create(builder.Build());
        }

        public PropertyGuestPolicyDto GetById(Guid id)
            => _propertyGuestPolicyReadRepository.GetById(id);

        public List<PropertyGuestPolicyDto> GetAll()
            => _propertyGuestPolicyReadRepository.GetAll();

        public void Update(PropertyGuestPolicyDto propertyGuestPolicy)
        {
            ValidateDto(propertyGuestPolicy, nameof(propertyGuestPolicy));

            if (Notification.HasNotification()) return;

            propertyGuestPolicy.PropertyId = int.Parse(_applicationUser.PropertyId);
            propertyGuestPolicy.PolicyTypeId = (int)Common.Enumerations.PolicyType.GuestPolicies;

            var builder = _propertyPolicyAdapter.Map(propertyGuestPolicy);
            _propertyGuestPolicyRepository.Update(builder.Build());
        }

        public void Delete(Guid id)
            => _propertyGuestPolicyRepository.Delete(id);

    }
}
