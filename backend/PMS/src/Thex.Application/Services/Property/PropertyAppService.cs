using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Polly;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Thex.Application.Adapters;
using Thex.Application.Interfaces;
using Thex.Common;
using Thex.Common.Enumerations;
using Thex.Domain.Entities;
using Thex.Domain.Interfaces.Repositories;
using Thex.Domain.Interfaces.Services;
using Thex.Dto;
using Thex.Infra.ReadInterfaces;
using Thex.Kernel;
using Thex.Storage;
using Tnf.Domain.Services;
using Tnf.Dto;
using Tnf.Notifications;
using Tnf.Repositories.Uow;

namespace Thex.Application.Services
{
    public class PropertyAppService : ScaffoldPropertyAppService, IPropertyAppService
    {
        private readonly IBrandAppService _brandAppService;
        private readonly ICompanyAppService _companyAppService;
        private readonly ILocationAppService _locationAppService;
        private readonly IPropertyReadRepository _propertyReadRepository;
        private readonly IContactInformationDomainService _contactInformationDomainService;
        private readonly IDomainService<Person> _personDomainService;
        private readonly ICompanyRepository _companyRepository;
        private readonly ICompanyReadRepository _companyReadRepository;
        private readonly IDocumentAppService _documentAppService;
        private readonly ICountrySubdivisionAppService _countrySubdivisionAppService;
        private readonly IContactInformationReadRepository _contactInformationReadRepository;
        private readonly IPersonReadRepository _personReadRepository;
        private readonly IPersonRepository _personRepository;
        private readonly IConfiguration _configuration;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IApplicationUser _applicationUser;
        private readonly IAzureStorage _azureStorage;
        private readonly IUnitOfWorkManager _unitOfWork;

        public PropertyAppService(
            IPropertyAdapter propertyAdapter,
            IDomainService<Property> propertyDomainService,
            IBrandAppService brandAppService,
            ICompanyAppService companyAppService,
            ILocationAppService locationAppService,
            IPropertyReadRepository propertyReadRepository,
            IContactInformationDomainService contactInformationDomainService,
            IDomainService<Person> personDomainService,
            IDocumentAppService documentAppService,
            ICountrySubdivisionAppService countrySubdivisionAppService,
            IContactInformationReadRepository contactInformationReadRepository,
            IPersonReadRepository personReadRepository,
            IPersonRepository personRepository,
            ICompanyRepository companyRepository,
            ICompanyReadRepository companyReadRepository,
            INotificationHandler notificationHandler,
            IConfiguration configuration,
            IHttpContextAccessor httpContextAccessor,
            IApplicationUser applicationUser,
            IAzureStorage azureStorage,
            IUnitOfWorkManager unitOfWork)
            : base(propertyAdapter, propertyDomainService, notificationHandler)
        {
            _brandAppService = brandAppService;
            _companyAppService = companyAppService;
            _locationAppService = locationAppService;
            _propertyReadRepository = propertyReadRepository;
            _contactInformationDomainService = contactInformationDomainService;
            _personDomainService = personDomainService;
            _documentAppService = documentAppService;
            _countrySubdivisionAppService = countrySubdivisionAppService;
            _contactInformationReadRepository = contactInformationReadRepository;
            _personReadRepository = personReadRepository;
            _personRepository = personRepository;
            _companyRepository = companyRepository;
            _companyReadRepository = companyReadRepository;
            _configuration = configuration;
            _httpContextAccessor = httpContextAccessor;
            _applicationUser = applicationUser;
            _azureStorage = azureStorage;
            _unitOfWork = unitOfWork;
        }

        public PropertyDto GetProperty(DefaultIntRequestDto id)
        {
            if (id.Id < 0)
            {
                NotifyIdIsMissing();
                return PropertyDto.NullInstance;
            }

            var property = PropertyDomainService.Get(id);
            var propertyDto = property?.MapTo<PropertyDto>();

            if (property != null && propertyDto != null)
            {
                propertyDto?.SetTotalOfRooms(_propertyReadRepository.GetTotalOfRooms(property.Id));
            }

            return propertyDto;
        }

        private void CreateOrUpdatePerson(PropertyDto propertyDto, int? countrySubdivision)
        {
            var personBuilder = new Person.Builder(Notification);

            var person = _personReadRepository.GetById(propertyDto.PropertyUId);

            if (person == null)
            {
                personBuilder.WithId(propertyDto.PropertyUId)
                    .WithPersonType(PersonTypeEnum.Legal)
                    .WithFirstName(propertyDto.Name)
                    .WithFullName(null)
                    .WithLastName(null)
                    .WithCountrySubdivisionId(countrySubdivision);

                _personDomainService.InsertAndSaveChanges(personBuilder);
            }
            else if (person != null && person.CountrySubdivisionId != countrySubdivision || person.FirstName != propertyDto.Name)
            {
                personBuilder.WithId(propertyDto.PropertyUId)
                    .WithFirstName(propertyDto.Name)
                    .WithCountrySubdivisionId(countrySubdivision)
                    .WithFullName(person.FullName)
                    .WithLastName(person.LastName)
                    .WithPersonType(person.PersonType);

                _personRepository.UpdateWithDocumentLocationContactInformation(personBuilder.Build());
            }
        }

        private void UpdateCompany(PropertyDto propertyDto)
        {

            var companyDto = new CompanyDto()
            {
                Id = propertyDto.CompanyId,
                TradeName = propertyDto.TradeName,
                ShortName = propertyDto.ShortName
            };

            var builder = new CompanyAdapter(Notification).Map(companyDto);

            var company = builder.Build();

            if (Notification.HasNotification()) return;

            _companyRepository.CompanyUpdate(company);
        }

        private void CreateOrUpdateContactProperty(PropertyDto propertyDto)
        {

            ICollection<ContactInformation.Builder> contactInformationList = new HashSet<ContactInformation.Builder>();

            if (!string.IsNullOrWhiteSpace(propertyDto.ContactPhone))
            {
                // Creating contact information phone
                var contactInformationPhoneBuilder = new ContactInformation.Builder(Notification);
                contactInformationPhoneBuilder
                                .WithOwnerId(propertyDto.PropertyUId)
                                .WithContactInformationTypeId((int)ContactInformationTypeEnum.PhoneNumber)
                                .WithInformation(propertyDto.ContactPhone);

                contactInformationList.Add(contactInformationPhoneBuilder);
            }

            if (!string.IsNullOrWhiteSpace(propertyDto.ContactEmail))
            {
                // Creating contact information  email
                var contactInformationEmailBuilder = new ContactInformation.Builder(Notification);
                contactInformationEmailBuilder
                                .WithOwnerId(propertyDto.PropertyUId)
                                .WithContactInformationTypeId((int)ContactInformationTypeEnum.Email)
                                .WithInformation(propertyDto.ContactEmail);

                contactInformationList.Add(contactInformationEmailBuilder);

            }

            if (!string.IsNullOrWhiteSpace(propertyDto.ContactWebSite))
            {
                // Creating contact information website
                var contactInformationBuilder = new ContactInformation.Builder(Notification);
                contactInformationBuilder
                                .WithOwnerId(propertyDto.PropertyUId)
                                .WithContactInformationTypeId((int)ContactInformationTypeEnum.Website)
                                .WithInformation(propertyDto.ContactWebSite);

                contactInformationList.Add(contactInformationBuilder);
            }

            _contactInformationDomainService.ResetContactInformationsProperty(propertyDto.PropertyUId);
            if (contactInformationList != null && contactInformationList.Count > 0)
            {
                var contactInformationsByOwnerId = _contactInformationReadRepository.GetAllByOwnerId(propertyDto.PropertyUId);
                foreach (var contactBuilder in contactInformationList)
                {
                    var contact = contactBuilder.Build();
                    _contactInformationDomainService.InsertAndSaveChanges(contactBuilder);

                    if (Notification.HasNotification()) break;
                }
            }
        }

        private void CreateOrUpdateLocation(PropertyDto propertyDto)
        {
            if (propertyDto != null)
            {
                foreach (var locationDto in propertyDto.LocationList)
                {
                    var countrySubdivision = this._countrySubdivisionAppService.CreateCountrySubdivisionTranslateAndGetCityId(locationDto, "pt-BR");
                    locationDto.CityId = countrySubdivision.Item1;
                    locationDto.StateId = countrySubdivision.Item2;
                    locationDto.CountryId = countrySubdivision.Item3;


                    _locationAppService.CreateOrUpdateGuestRegistrationLocation(locationDto, propertyDto.PropertyUId.ToString());
                    if (Notification.HasNotification()) break;

                }
                CreateOrUpdatePerson(propertyDto, propertyDto.LocationList.FirstOrDefault().CityId);
            }
        }

        public void CreateProperty(PropertyDto propertyDto)
        {
            if (propertyDto == null)
                NotifyNullParameter();

            if (Notification.HasNotification()) return;

            ValidateDto<PropertyDto>(propertyDto, nameof(propertyDto));

            if (Notification.HasNotification()) return;

            var builder = PropertyAdapter.Map(propertyDto);

            if (Notification.HasNotification()) return;

            propertyDto.Id = PropertyDomainService.InsertAndSaveChanges(builder).Id;

            var property = builder.Build();
            propertyDto.PropertyUId = property.PropertyUId;

            if (Notification.HasNotification()) return;

            CreateOrUpdateContactProperty(propertyDto);

            if (Notification.HasNotification()) return;

            CreateOrUpdateLocation(propertyDto);

            if (Notification.HasNotification()) return;

            CreateOrUpdateDocumentCompany(propertyDto);


        }

        public void CreateOrUpdateDocumentCompany(PropertyDto propertyDto)
        {
            var documentList = new List<DocumentDto>();

            if (propertyDto.CNPJ != null)
            {
                var documentDto = new DocumentDto()
                {
                    DocumentInformation = propertyDto.CNPJ,
                    DocumentTypeId = (int)DocumentTypeEnum.BrLegalPersonCNPJ
                };

                documentList.Add(documentDto);

            }
            if (propertyDto.InscEst != null)
            {
                var documentDto = new DocumentDto()
                {
                    DocumentInformation = propertyDto.InscEst,
                    DocumentTypeId = (int)DocumentTypeEnum.BrLegalPersonInscEst
                };

                documentList.Add(documentDto);

            }
            if (propertyDto.InscMun != null)
            {
                var documentDto = new DocumentDto()
                {
                    DocumentInformation = propertyDto.InscMun,
                    DocumentTypeId = (int)DocumentTypeEnum.BrLegalPersonInscMun
                };

                documentList.Add(documentDto);
            }

            if (documentList != null && documentList.Count > 0)
            {

                var company = _companyReadRepository.GetCompanyById(propertyDto.CompanyId);
                foreach (var document in documentList)
                {
                    _documentAppService.CreateOrUpdateGuestRegistrationDocument(document, company.PersonId.ToString());
                    if (Notification.HasNotification()) return;
                }

            }
        }

        public void UpdateProperty(int id, PropertyDto propertyDto)
        {
            if (propertyDto == null)
                NotifyNullParameter();

            if (Notification.HasNotification()) return;

            ValidateDto<PropertyDto>(propertyDto, nameof(propertyDto));

            if (Notification.HasNotification()) return;

            var builder = PropertyAdapter.Map(propertyDto);

            if (Notification.HasNotification()) return;

            PropertyDomainService.Update(builder);

            if (Notification.HasNotification()) return;

            CreateOrUpdateLocation(propertyDto);

            if (Notification.HasNotification()) return;

            if (Notification.HasNotification()) return;

            CreateOrUpdateContactProperty(propertyDto);

            if (Notification.HasNotification()) return;

            UpdateCompany(propertyDto);

        }

        public void DeleteProperty(int id)
        {
            if (id < 0)
            {
                NotifyIdIsMissing();
                return;
            }

            PropertyDomainService.Delete(e => e.Id == id);
        }

        public IListDto<PropertyDto> GetAllProperty(IRequestAllDto request, bool central = false)
            => this._propertyReadRepository.GetAll(request, central);

        public PropertyDto GetByPropertyId(int propertyId)
        {
            return _propertyReadRepository.GetByPropertyId(propertyId);
        }

        public GetPropertySiba GetPropertySiba()
        {
            var documentList = GetGlobalDocumentTypes().Result;

            return _propertyReadRepository.GetByPropertyId(documentList.ToList().FindAll(x => x.Name == "NIF")
                                                                       .Select(d => d.Id).ToList());
        }

        private async Task<IList<DocumentTypeDto>> GetGlobalDocumentTypes()
        {
            var documentList = new ListDto<DocumentTypeDto>();

            using (var httpClient = new HttpClient())
            {
                var supportEndpoint = new Uri(string.Concat(_configuration.GetValue<string>("SupportEndpoint"), $"/api/documenttypes/globaltypes"));

                var token = _httpContextAccessor.HttpContext.Request.Headers.FirstOrDefault(x => x.Key.Equals("Authorization")).Value.ToString();
                httpClient.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", token);

                var responseMessage = await Policy
                                             .HandleResult<HttpResponseMessage>(message => !message.IsSuccessStatusCode)
                                             .WaitAndRetryAsync(3, i => TimeSpan.FromSeconds(15), (result, timeSpan, retryCount, context) =>
                                             {
                                                 Console.Write($"Request failed with {result.Result.StatusCode}. Waiting {timeSpan} before next retry. Retry attempt {retryCount}");
                                             })
                                             .ExecuteAsync(async () => await httpClient.GetAsync(supportEndpoint));

                if (responseMessage.IsSuccessStatusCode)
                    documentList = JsonConvert.DeserializeObject<ListDto<DocumentTypeDto>>(await responseMessage.Content.ReadAsStringAsync());
                else
                    Notification.Raise(Notification
                                .DefaultBuilder
                                .WithMessage(AppConsts.LocalizationSourceName, CommonsEnum.Error.NullOrEmptyObject)
                                .Build());
            }

            return documentList.Items;
        }

        public async Task<PropertyDto> SendPhoto(PropertyDto request)
        {
            var _bytes = Convert.FromBase64String(request.Photo);
            var url = await _azureStorage.SendAsync(_applicationUser.UserUid, _applicationUser.PropertyId, _applicationUser.PropertyId, _bytes);

            if (Notification.HasNotification())
                return null;

            using (var uow = _unitOfWork.Begin())
            {
                var propertyDto = _propertyReadRepository.GetByPropertyId(int.Parse(_applicationUser.PropertyId));
                if (propertyDto == null)
                {
                    NotifyNullParameter();
                    return null;
                }

                var builder = PropertyAdapter.MapPhotoUrl(propertyDto, url);

                if (Notification.HasNotification())
                    return null;

                PropertyDomainService.Update(builder);

                if (Notification.HasNotification())
                    return null;

                await uow.CompleteAsync();
            }

            return new PropertyDto() { Photo = url };
        }
    }
}
