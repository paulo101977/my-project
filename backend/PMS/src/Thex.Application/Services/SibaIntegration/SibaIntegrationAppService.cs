﻿using System.Threading.Tasks;
using Thex.Application.Interfaces;
using Thex.Domain.Interfaces.Repositories;
using Thex.Dto;
using Thex.Infra.ReadInterfaces;
using Thex.Kernel;
using Polly;
using System;
using System.Net.Http;
using Microsoft.Extensions.Configuration;
using System.Text;
using Newtonsoft.Json;
using Thex.Domain.Entities;
using System.Collections.Generic;
using Thex.Dto.SibaIntegration;
using Tnf.Notifications;
using Thex.Common;

namespace Thex.Application.Services
{
    public class SibaIntegrationAppService : ApplicationServiceBase, ISibaIntegrationAppService
    {
        private readonly IApplicationUser _applicationUser;
        private readonly ISibaIntegrationRepository _sibaIntegrationRepository;
        private readonly ISibaIntegrationReadRepository _sibaIntegrationReadRepository;
        private readonly IConfiguration _configuration;
        private readonly IGuestAppService _guestAppService;
        private readonly IPropertyAppService _propertyAppService;
        private readonly IPropertyReadRepository _propertyReadRepository;
        private readonly INotificationHandler _notificationHandler;

        public SibaIntegrationAppService(
            IApplicationUser applicationUser,
            ISibaIntegrationRepository sibaIntegrationRepository,
            ISibaIntegrationReadRepository sibaIntegrationReadRepository,
            IConfiguration configuration,
            IGuestAppService guestAppService,
            IPropertyAppService propertyAppService,
            IPropertyReadRepository propertyReadRepository,
            INotificationHandler notificationHandler)
            : base (notificationHandler)
        {
            _applicationUser = applicationUser;
            _sibaIntegrationRepository = sibaIntegrationRepository;
            _sibaIntegrationReadRepository = sibaIntegrationReadRepository;
            _configuration = configuration;
            _guestAppService = guestAppService;
            _propertyAppService = propertyAppService;
            _propertyReadRepository = propertyReadRepository;
            _notificationHandler = notificationHandler;
        }

        public async Task Send(SibaIntegrationRequestDto dto)
        {
            var propertyUId = _propertyReadRepository.GetUIdById(int.Parse(_applicationUser.PropertyId));

            var guestList = _guestAppService.GetAllForeingGuestByFilters(dto.StartDate, dto.EndDate);
            VerifyGuestList(guestList);
            if (Notification.HasNotification())
                return;

            var propertyInfo = _propertyAppService.GetPropertySiba();
            VerifyPropertyInformationError(propertyInfo);
            if (Notification.HasNotification())
                return;

            var sibaIntegration = _sibaIntegrationReadRepository.GetByPropertyId(propertyUId);
            VerifySibaInformationError(sibaIntegration);
            if (Notification.HasNotification())
                return;


            using (var httpClient = new HttpClient())
            {
                var sibaEndpoint = new Uri(string.Concat(_configuration.GetValue<string>("SibaEndpoint"), $"/api/sibaintegration"));

                var responseMessage = await Policy
                                             .HandleResult<HttpResponseMessage>(message => !message.IsSuccessStatusCode)
                                             .WaitAndRetryAsync(0, i => TimeSpan.FromSeconds(15), (result, timeSpan, retryCount, context) =>
                                             {
                                                 Console.Write($"Request failed with {result.Result.StatusCode}. Waiting {timeSpan} before next retry. Retry attempt {retryCount}");
                                             })
                                             .ExecuteAsync(async () => await httpClient.PostAsync(sibaEndpoint, new StringContent(await ProccessRequestSibaIntegration(sibaIntegration, guestList, propertyInfo), Encoding.UTF8, "application/json")));

                if (responseMessage.IsSuccessStatusCode)
                {
                    var message = await responseMessage.Content.ReadAsStringAsync();

                    if (!string.IsNullOrEmpty(message))
                    {
                        var sibaResponse = JsonConvert.DeserializeObject<SibaIntegrationResponseDto>(message);

                        if (sibaResponse == null)
                            RaiseIntegrationDefaultError();
                        else if (sibaResponse != null && sibaResponse.Code == 0)
                            IncrementIntegrationFileNumber(sibaIntegration);
                        else if (!string.IsNullOrEmpty(sibaResponse.Description))
                        {
                            var notificationResponseDto = new NotificationResponseDto(sibaResponse.Description, sibaResponse.Description);
                            var notify = JsonConvert.DeserializeObject<NotificationEvent>(JsonConvert.SerializeObject(notificationResponseDto));
                            _notificationHandler.Raise(notify);
                        }
                        else
                            RaiseIntegrationDefaultError();
                    }  
                }
                else
                    RaiseIntegrationDefaultError();
            }       
        }

        private void RaiseIntegrationDefaultError()
        {
            _notificationHandler.Raise(_notificationHandler
                                        .DefaultBuilder
                                        .WithMessage(AppConsts.LocalizationSourceName, SibaIntegration.Error.SibaIntegrationComunicationError)
                                        .Build());
        }   
        
        private void VerifyGuestList(List<GetAllForeingGuest> guestList)
        {
            if (guestList == null || (guestList != null && guestList.Count == 0))
                _notificationHandler.Raise(_notificationHandler
                                        .DefaultBuilder
                                        .WithMessage(AppConsts.LocalizationSourceName, SibaIntegration.Error.WithoutForeignGuestToSentToSiba)
                                        .Build());
        }

        private void VerifyPropertyInformationError(GetPropertySiba getPropertySiba)
        {
            if (getPropertySiba == null)
                _notificationHandler.Raise(_notificationHandler
                                        .DefaultBuilder
                                        .WithMessage(AppConsts.LocalizationSourceName, SibaIntegration.Error.PropertyInformationError)
                                        .Build());
        }

        private void VerifySibaInformationError(SibaIntegration sibaIntegration)
        {
            if (sibaIntegration == null)
                _notificationHandler.Raise(_notificationHandler
                                        .DefaultBuilder
                                        .WithMessage(AppConsts.LocalizationSourceName, SibaIntegration.Error.SibaInformationError)
                                        .Build());
        }

        private async Task<string> ProccessRequestSibaIntegration(SibaIntegration sibaIntegration, List<GetAllForeingGuest> guestList, GetPropertySiba getPropertySiba)
        {
            var sibaIntegrationPostDto = new SibaIntegrationPostDto
            {
                Property = getPropertySiba,
                GuestList = guestList,
                SibaIntegrationInfo = new SibaIntegrationInfoDto
                {
                    IntegrationDate = DateTime.UtcNow.ToZonedDateTimeLoggedUser().Date,
                    IntegrationFileNumber = sibaIntegration.IntegrationFileNumber,
                    AccessKey = getPropertySiba.IntegrationCode
                }
            };

            var json = await Task.FromResult(JsonConvert.SerializeObject(sibaIntegrationPostDto));

            return json;
        }

        private void IncrementIntegrationFileNumber(Domain.Entities.SibaIntegration sibaIntegration)
        {
            sibaIntegration.IncrementIntegrationFileNumber();
            _sibaIntegrationRepository.Update(sibaIntegration);
        }
    }
}
