﻿using System;
using Thex.Application.Adapters;
using Thex.Domain.Entities;
using Thex.Application.Interfaces;
using Tnf.Domain.Services;
using Tnf.Dto;
using Thex.Dto.Person;
using Thex.Infra.ReadInterfaces;

using Thex.Dto;
using Tnf.Notifications;
using Thex.Common.Enumerations;
using Tnf.Repositories.Uow;
using System.Collections.Generic;
using Thex.Domain.Interfaces.Repositories;
using System.Linq;
using System.Globalization;
using Thex.Common;

namespace Thex.Application.Services
{
    public class PersonAppService : ScaffoldPersonAppService, IPersonAppService
    {
        private readonly IPersonReadRepository _personReadRepository;
        private readonly ILocationAppService _locationAppService;
        private readonly IDocumentAppService _documentAppService;
        private readonly IEmployeeAppService _employeeAppService;
        private readonly IContactInformationAppService _contactInformationAppService;
        private readonly IPersonInformationAppService _personInformationAppService;
        private readonly ICountrySubdivisionAppService _countrySubdivisionAppService;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IPersonRepository _personRepository;
        private readonly IDocumentReadRepository _documentReadRepository;
        private readonly ILocationReadRepository _locationReadRepository;
        private readonly IContactInformationRepository _contactInformationRepository;

        public PersonAppService(
            IPersonAdapter personAdapter,
            IDomainService<Person> personDomainService,
            IPersonReadRepository personReadRepository,
            IDocumentAppService documentAppService,
            ILocationAppService locationAppService,
            IEmployeeAppService employeeAppService,
            IContactInformationAppService contactInformationAppService,
            IPersonInformationAppService personInformationAppService,
            INotificationHandler notificationHandler,
            ICountrySubdivisionAppService countrySubdivisionAppService,
            IUnitOfWorkManager unitOfWorkManager,
            IPersonRepository personRepository,
            IDocumentReadRepository documentReadRepository,
            ILocationReadRepository locationReadRepository,
            IContactInformationRepository contactInformationRepository
            )
            : base(personAdapter, personDomainService, notificationHandler)
        {
            _personReadRepository = personReadRepository;
            _documentAppService = documentAppService;
            _locationAppService = locationAppService;
            _employeeAppService = employeeAppService;
            _contactInformationAppService = contactInformationAppService;
            _personInformationAppService = personInformationAppService;
            _countrySubdivisionAppService = countrySubdivisionAppService;
            _unitOfWorkManager = unitOfWorkManager;
            _personRepository = personRepository;
            _documentReadRepository = documentReadRepository;
            _locationReadRepository = locationReadRepository;
            _contactInformationRepository = contactInformationRepository;
        }

        public Person.Builder GetBuilder(PersonDto personDto)
        {
            var documentList = _documentAppService.ToDomain(personDto.DocumentList);
            var locationList = _locationAppService.ToDomain(personDto.LocationList);
            var employeeList = _employeeAppService.ToDomain(personDto.EmployeeList);
            var contactInformationList = _contactInformationAppService.ToDomain(personDto.ContactInformationList);
            var personInformationList = _personInformationAppService.ToDomain(personDto.PersonInformationList);

            var builder = new Person.Builder(Notification);

            builder
                .WithPersonType(personDto.PersonType)
                .WithFirstName(personDto.FirstName)
                .WithLastName(personDto.LastName)
                .WithDateOfBirth(personDto.DateOfBirth)
                .WithDocumentList(documentList)
                .WithLocationList(locationList)
                .WithEmployeeList(employeeList)
                .WithContactInformationList(contactInformationList)
                .WithPersonInformationList(personInformationList);

            return builder;
        }

        public IListDto<SearchPersonsResultDto> GetPersonsBasedOnFilter(SearchPersonsDto request)
        {
            int minimumCharacters = 2;

            if (request == null)
                NotifyNullParameter();
            else if (string.IsNullOrEmpty(request.SearchData))
                NotifyRequired("SearchData");
            else if (request.SearchData.Length < minimumCharacters)
                NotifyMinimumCharacters(minimumCharacters);

            if (Notification.HasNotification())
                return new ListDto<SearchPersonsResultDto>();
            
            return _personReadRepository.GetPersonsBasedOnFilter(request);
        }

        public Guid CreateOrUpdateHeaderPersonAndGetId(HeaderPersonDto personDto)
        {
            var personId = Guid.Empty;

            if (personDto.Id == null || personDto.Id == Guid.Empty)
            {
                personId = CreateHeaderPersonAndGetId(personDto, PersonTypeEnum.Header);
            }
            else
            {
                var personOld = PersonDomainService.Get(new DefaultGuidRequestDto { Id = personDto.Id });

                if (personOld.PersonTypeValue == PersonTypeEnum.Natural || personOld.PersonTypeValue == PersonTypeEnum.Legal)
                {
                    personId = CreateHeaderPersonAndGetId(personDto, PersonTypeEnum.Header);
                }
                else if (personOld.PersonTypeValue == PersonTypeEnum.Header)
                {
                    personId = personDto.Id;
                    UpdateHeaderPersonAndGetId(personDto);
                }
            }

            return personId;
        }

        private Guid CreateHeaderPersonAndGetId(HeaderPersonDto personDto, PersonTypeEnum personType)
        {
            personDto.Id = Guid.Empty;
            personDto.PersonType = ((char)personType).ToString();
            var personBuilder = PersonAdapter.Map(personDto);
            var personId = PersonDomainService.InsertAndSaveChanges(personBuilder).Id;

            if (personDto.LocationList != null && personDto.LocationList.Any())
            {
                personDto.LocationList.ToList().ForEach(l =>
                {
                    if (l.LocationCategoryId == default(int))
                        l.LocationCategoryId = (int)LocationCategoryEnum.Correspondence;
                });

                _locationAppService.CreateLocationList(personDto.LocationList, personId);
            }

            _documentAppService.SaveDocumentList(personDto.DocumentList, personId);

            var contactInformationList = GetContactInformationListFromDto(
                            personId: personId,
                            email: personDto.Email,
                            phoneNumber: personDto.PhoneNumber);

            if (contactInformationList.Any())
                _contactInformationRepository.AddRangeAndSaveChanges(contactInformationList);

            return personId;
        }

        private void UpdateHeaderPersonAndGetId(HeaderPersonDto personDto)
        {
            var locationList = GetLocationListForUpdatedPersonHeader(personDto);
            if (locationList != null && locationList.Any())
                _locationAppService.CreateOrUpdatePersonHeaderLocation(locationList.FirstOrDefault(), personDto.Id);

            personDto.PersonType = ((char)PersonTypeEnum.Header).ToString();

            if (personDto.DocumentList != null && personDto.DocumentList.Any())
                _documentAppService.UpdateDocumentPersonHeader(personDto.DocumentList.FirstOrDefault(), personDto.Id);

            var contactInformationList = GetContactInformationListFromDto(
                            personId: personDto.Id,
                            email: personDto.Email,
                            phoneNumber: personDto.PhoneNumber);

            Person personEntity = PersonAdapter.Map(personDto)
                .WithContactInformationList(contactInformationList)
                .Build();

            _personRepository.UpdatePersonHeaderWithContactInformation(personEntity);
        }

        private IList<Location> GetLocationListForUpdatedPersonHeader(PersonDto personDto)
        {
            IList<Location> locationList = new List<Location>();

            foreach (var locationDto in personDto.LocationList)
            {
                var countrySubdivision = this._countrySubdivisionAppService.CreateCountrySubdivisionTranslateAndGetCityId(locationDto, locationDto.BrowserLanguage);
                locationDto.CityId = countrySubdivision.Item1;
                locationDto.StateId = countrySubdivision.Item2;
                locationDto.CountryId = countrySubdivision.Item3;
                locationDto.Id = 0;
                locationDto.LocationCategoryId = locationDto.LocationCategoryId == default(int) ? (int)LocationCategoryEnum.Correspondence : locationDto.LocationCategoryId;

                locationList.Add(_locationAppService.GetBuilder(locationDto, personDto.Id)
                    .WithLocationCategoryId(locationDto.LocationCategoryId)
                    .WithNeighborhood(locationDto.Neighborhood)
                    .WithPostalCode(locationDto.PostalCode)
                    .WithCityId(locationDto.CityId)
                    .WithCountryId(locationDto.CountryId)
                    .WithStateId(locationDto.StateId)
                    .WithCountryCode(locationDto.CountryCode)
                    .Build());
            }

            return locationList;
        }

        private IList<ContactInformation> GetContactInformationListFromDto(Guid personId, string email, string phoneNumber)
        {
            List<ContactInformation> contactInformationList = new List<ContactInformation>();

            if (!string.IsNullOrEmpty(email))
            {
                contactInformationList.Add(GetContactInformationDto(ContactInformationTypeEnum.Email, email, personId).Build());
            }

            if (!string.IsNullOrEmpty(phoneNumber))
            {
                contactInformationList.Add(GetContactInformationDto(ContactInformationTypeEnum.PhoneNumber, phoneNumber, personId).Build());
            }

            return contactInformationList;
        }

        private ContactInformation.Builder GetContactInformationDto(ContactInformationTypeEnum contactInformationTypeEnum, string contactInformation, Guid personId)
        {
            ContactInformationDto contactInformationDto = new ContactInformationDto
            {
                OwnerId = personId,
                Information = contactInformation
            };

            switch (contactInformationTypeEnum)
            {
                case ContactInformationTypeEnum.Email:
                    contactInformationDto.ContactInformationTypeId = (int)ContactInformationTypeEnum.Email;
                    break;
                case ContactInformationTypeEnum.PhoneNumber:
                    contactInformationDto.ContactInformationTypeId = (int)ContactInformationTypeEnum.PhoneNumber;
                    break;
            }
            return _contactInformationAppService.GetBuilder(contactInformationDto);
        }

        public HeaderPersonDto GetHeaderByBillingAccountId(Guid billingAccountId)
        {
            var personHeaderDto = _personReadRepository.GetHeaderByBillingAccountId(billingAccountId);

            if (personHeaderDto == null)
                return HeaderPersonDto.NullInstance;

            SetDocumentAndLocationList(personHeaderDto);

            return personHeaderDto;
        }

        public HeaderPersonDto GetHeaderByBillingInvoiceId(Guid billingInvoiceId)
        {
            var personHeaderDto = _personReadRepository.GetHeaderByBillingInvoiceId(billingInvoiceId);

            if (personHeaderDto == null)
                return HeaderPersonDto.NullInstance;

            SetDocumentAndLocationList(personHeaderDto);

            return personHeaderDto;
        }

        private void SetDocumentAndLocationList(HeaderPersonDto personHeaderDto)
        {
            personHeaderDto.DocumentList = _documentReadRepository.GetDocumentDtoListByPersonId(personHeaderDto.Id);

            personHeaderDto.LocationList = _locationReadRepository.GetLocationDtoListByPersonId(personHeaderDto.Id, CultureInfo.CurrentCulture.Name);

            if(personHeaderDto.LocationList == null || personHeaderDto.LocationList.Count == 0)
                personHeaderDto.LocationList = _locationReadRepository.GetLocationDtoListByPersonId(personHeaderDto.Id, AppConsts.DEFAULT_CULTURE_INFO_NAME);
        }
    }
}
