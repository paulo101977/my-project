﻿using System;
using Thex.Application.Adapters;
using Thex.Domain.Entities;
using Thex.Application.Interfaces;
using Tnf.Domain.Services;

using Thex.Dto;
using Tnf.Notifications;

namespace Thex.Application.Services
{
    public class PlasticAppService : ScaffoldPlasticAppService, IPlasticAppService
    {
        public PlasticAppService(
            IPlasticAdapter plasticAdapter,
            IDomainService<Plastic> plasticDomainService,
            INotificationHandler notificationHandler)
            : base(plasticAdapter, plasticDomainService, notificationHandler)
        {
        }

        public Plastic.Builder GetBuilder(PlasticDto plasticDto)
        {
            var builder = new Plastic.Builder(Notification);

            builder
                .WithHolder(plasticDto.Holder)
                .WithPlasticNumber("n/d")
                .WithExpirationDate("n/d")
                .WithPlasticBrandId(plasticDto.PlasticBrandId)
                .WithCsc(0);

            return builder;
        }
    }
}
