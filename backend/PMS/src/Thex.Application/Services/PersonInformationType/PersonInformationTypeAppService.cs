﻿using System;
using Thex.Application.Adapters;
using Thex.Domain.Entities;
using Thex.Application.Interfaces;
using Tnf.Domain.Services;
using Tnf.Notifications;

namespace Thex.Application.Services
{
    public class PersonInformationTypeAppService : ScaffoldPersonInformationTypeAppService, IPersonInformationTypeAppService
    {
        public PersonInformationTypeAppService(
            IPersonInformationTypeAdapter personInformationTypeAdapter,
            IDomainService<PersonInformationType> personInformationTypeDomainService,
            INotificationHandler notificationHandler)
            : base(personInformationTypeAdapter, personInformationTypeDomainService, notificationHandler)
        {
        }
    }
}
