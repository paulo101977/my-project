﻿using System;
using Thex.Application.Adapters;
using Thex.Domain.Entities;
using Thex.Application.Interfaces;
using Tnf.Domain.Services;
using Thex.Infra.ReadInterfaces;
using Thex.Dto;
using Tnf.Dto;
using Tnf.Dto;
using System.Threading.Tasks;
using Tnf.Notifications;
using Thex.Kernel;
using Thex.Common;

namespace Thex.Application.Services
{
    public class PropertyCompanyClientCategoryAppService : ScaffoldPropertyCompanyClientCategoryAppService, IPropertyCompanyClientCategoryAppService
    {

        private readonly IPropertyCompanyClientCategoryReadRepository _propertyCompanyClientCategoryReadRepository;
        private readonly IPropertyCompanyClientCategoryRepository _propertyCompanyClientCategoryRepository;

        public PropertyCompanyClientCategoryAppService(
            IPropertyCompanyClientCategoryAdapter propertyCompanyClientCategoryAdapter,
            IPropertyCompanyClientCategoryReadRepository propertyCompanyClientCategoryReadRepository,
            IPropertyCompanyClientCategoryRepository propertyCompanyClientCategoryRepository,
        IDomainService<PropertyCompanyClientCategory> propertyCompanyClientCategoryDomainService,
        IApplicationUser applicationUser,
            INotificationHandler notificationHandler)
            : base(propertyCompanyClientCategoryAdapter, propertyCompanyClientCategoryDomainService, propertyCompanyClientCategoryReadRepository,
                  applicationUser, notificationHandler)
        {
            _propertyCompanyClientCategoryReadRepository = propertyCompanyClientCategoryReadRepository;
            _propertyCompanyClientCategoryRepository = propertyCompanyClientCategoryRepository;
        }

        public IListDto<PropertyCompanyClientCategoryDto> GetAllByPropertyId(GetAllPropertyCompanyClientCategoryDto request, int propertyId)
        {
            return _propertyCompanyClientCategoryReadRepository.GetAllByPropertyId(request, propertyId);
        }

        public PropertyCompanyClientCategoryDto GetPropertyCompanyClientCategoryById(DefaultGuidRequestDto id)
        {
            var category = PropertyCompanyClientCategoryDomainService.Get(id);

            return new PropertyCompanyClientCategoryDto()
            {
                Id = category.Id,
                IsActive = category.IsActive,
                PropertyCompanyClientCategoryName = category.PropertyCompanyClientCategoryName,
                PropertyId = category.PropertyId

            };
        }

        public void ToggleAndSaveIsActive(Guid guid)
        {
            _propertyCompanyClientCategoryRepository.ToggleAndSaveIsActive(guid);
        }


        public new void Delete(Guid id)
        {
            if (_propertyCompanyClientCategoryReadRepository.HasClient(id))
            {
                Notification.Raise(Notification.DefaultBuilder
                  .WithMessage(AppConsts.LocalizationSourceName, PropertyCompanyClientCategory.EntityError.PropertyCompanyClientCategoryContainsClient)
                  .WithDetailedMessage(AppConsts.LocalizationSourceName, PropertyCompanyClientCategory.EntityError.PropertyCompanyClientCategoryContainsClient)
                  .Build());               
            }
            else 
                _propertyCompanyClientCategoryRepository.Delete(id);
        }


    }
}
