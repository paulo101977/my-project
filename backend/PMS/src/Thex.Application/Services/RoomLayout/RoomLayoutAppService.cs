﻿using System;
using Thex.Application.Adapters;
using Thex.Domain.Entities;
using Thex.Application.Interfaces;
using Thex.Dto;
using Tnf.Dto;
using Tnf.Dto;


using Thex.Domain.Interfaces.Services;
using Tnf.Notifications;

namespace Thex.Application.Services
{
    public class RoomLayoutAppService : ScaffoldRoomLayoutAppService, IRoomLayoutAppService
    {
        private readonly IRoomLayoutDomainService _roomLayoutDomainService;

        public RoomLayoutAppService(
            IRoomLayoutAdapter roomLayoutAdapter,
            IRoomLayoutDomainService roomLayoutDomainService,
            INotificationHandler notificationHandler)
            : base(roomLayoutAdapter, roomLayoutDomainService, notificationHandler)
        {
            _roomLayoutDomainService = roomLayoutDomainService;
        }

        public RoomLayoutDto Get(DefaultGuidRequestDto id)
        {
            if (id.Id == null)
            {
                NotifyIdIsMissing();
                return RoomLayoutDto.NullInstance;
            }

            RoomLayout roomLayout =RoomLayoutDomainService.Get(id);

            return roomLayout.MapTo<RoomLayoutDto>();
        }

        public IListDto<RoomLayoutDto> GetAllLayoutsByRoomTypeId(DefaultIntRequestDto id)
        {
            if (id.Id <= 0)
            {
                NotifyIdIsMissing();
                return null;
            }

            var roomLayoutDtos = new ListDto<RoomLayoutDto>();
            var roomLayouts = _roomLayoutDomainService.GetAllLayoutsByRoomTypeId(id);
            foreach (var roomLayout in roomLayouts)
            {
                var roomLayoutDto = roomLayout.MapTo<RoomLayoutDto>();
                roomLayoutDtos.Items.Add(roomLayoutDto);
            };
            
            return roomLayoutDtos;
        }

        public new RoomLayoutDto Create(RoomLayoutDto roomLayoutDto)
        {
            if (roomLayoutDto == null)
            {
                NotifyNullParameter();
                return RoomLayoutDto.NullInstance;
            }

            RoomLayout.Builder builder = new RoomLayout.Builder(Notification);

            builder
                .WithId(roomLayoutDto.Id)
                .WithQuantitySingle(roomLayoutDto.QuantitySingle)
                .WithQuantityDouble(roomLayoutDto.QuantityDouble);

            roomLayoutDto.Id = RoomLayoutDomainService.InsertAndSaveChanges(builder).Id;
            return roomLayoutDto;
        }
    }
}
