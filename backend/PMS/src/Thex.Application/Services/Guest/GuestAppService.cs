﻿using System;
using Thex.Application.Adapters;
using Thex.Domain.Entities;
using Thex.Application.Interfaces;
using Tnf.Domain.Services;
using Thex.Dto;

using Tnf.Dto;
using Thex.Domain.Interfaces.Repositories;
using Tnf.Notifications;
using Thex.Dto.Person;
using Thex.Dto.GuestRegistration;
using System.Collections.Generic;
using Tnf.Localization;
using Thex.Common.Enumerations;
using Thex.Common;
using Thex.Infra.ReadInterfaces;
using System.Linq;

namespace Thex.Application.Services
{
    public class GuestAppService : ScaffoldGuestAppService, IGuestAppService
    {
        private readonly IPersonRepository _personRepository; 
        private readonly IPersonAppService _personAppService;
        private readonly IGuestRegistrationAppService _guestRegistrationAppService;
        private readonly IGuestReadRepository _guestReadRepository;
        protected readonly ILocalizationManager _localizationManager;

        public GuestAppService(
            IGuestAdapter guestAdapter,
            IDomainService<Guest> guestDomainService,
            IPersonRepository personRepository,
            IPersonAppService personAppService,
            ILocalizationManager localizationManager,
            IGuestRegistrationAppService guestRegistrationAppService,
            IGuestReadRepository guestReadRepository,
            INotificationHandler notificationHandler)
            : base(guestAdapter, guestDomainService, notificationHandler)
        {
            _personRepository = personRepository;
            _personAppService = personAppService;
            _localizationManager = localizationManager;
            _guestRegistrationAppService = guestRegistrationAppService;
            _guestReadRepository = guestReadRepository;
        }

        public long CreateGuestIfNotExistAndReturnGuest(Guid personId)
        {
            var guestDto = new GuestDto() { PersonId = personId };

            ValidateDto<GuestDto>(guestDto, nameof(guestDto));

            var guestBuilder = GuestAdapter.Map(guestDto);

            var guest = _personRepository.CreateGuestIfNotExist(guestBuilder.Build());

            return guest.Id;
            
        }

        public SearchResultDto SearchBasedOnFilter(SearchPersonsDto request)
        {
            var result = new SearchResultDto {  SearchResultCategory = new List<SearchResultCategoryDto>() };

            var personResult = _personAppService.GetPersonsBasedOnFilter(request);
            var guestRegistrationResult = _guestRegistrationAppService.GetAllGuestRegistrationBasedOnFilter(new SearchGuestRegistrationsDto
            {
                Page = request.Page,
                PageSize = request.PageSize,
                ByEmail = request.ByEmail,
                DocumentTypeId = request.DocumentTypeId,
                SearchData = request.SearchData
            });

            SearchResultCategoryDto personResultDto = FillPersonResultDto(personResult);
            SearchResultCategoryDto guestRegistrationResultDto = FillGuestRegistrationDto(guestRegistrationResult);

            result.SearchResultCategory.Add(personResultDto);
            result.SearchResultCategory.Add(guestRegistrationResultDto);

            return result;
        }

        private SearchResultCategoryDto FillGuestRegistrationDto(IListDto<GuestRegistrationResultDto> guestRegistrationResult)
        {
            var guestRegistrationResultDto = new SearchResultCategoryDto
            {
                SearchCategoryName = _localizationManager.GetString(AppConsts.LocalizationSourceName, PersonEnum.Common.PersonAndGuestRegistrationSearchByGuest.ToString()),
                SearchPersonAndGuest = new List<SearchPersonAndGuestDto>()
            };

            foreach (var item in guestRegistrationResult.Items)
            {
                guestRegistrationResultDto.SearchPersonAndGuest.Add(new SearchPersonAndGuestDto
                {
                    Document = item.Document,
                    DocumentType = item.DocumentTypeId != 0 ? _localizationManager.GetString(AppConsts.LocalizationSourceName, ((DocumentTypeEnum)item.DocumentTypeId).ToString()) : null,
                    DocumentTypeId = item.DocumentTypeId,
                    GuestId = item.GuestId,
                    Name = item.FullName,
                    PersonId = Guid.Parse(item.PersonId)
                });
            }

            return guestRegistrationResultDto;
        }

        private SearchResultCategoryDto FillPersonResultDto(IListDto<SearchPersonsResultDto> personResult)
        {
            var personResultDto = new SearchResultCategoryDto
            {
                SearchCategoryName = _localizationManager.GetString(AppConsts.LocalizationSourceName, PersonEnum.Common.PersonAndGuestRegistrationSearchByGlobalPerson.ToString()),
                SearchPersonAndGuest = new List<SearchPersonAndGuestDto>()
            };

            foreach (var item in personResult.Items)
            {
                personResultDto.SearchPersonAndGuest.Add(new SearchPersonAndGuestDto
                {
                    Document = item.Document,
                    DocumentType = item.DocumentTypeId != 0 ? _localizationManager.GetString(AppConsts.LocalizationSourceName, ((DocumentTypeEnum)item.DocumentTypeId).ToString()) : null,
                    DocumentTypeId = item.DocumentTypeId,
                    GuestId = item.GuestId,
                    Name = item.Name,
                    PersonId = item.Id
                });
            }

            return personResultDto;
        }

        public List<GetAllForeingGuest> GetAllForeingGuestByFilters(DateTime startDate, DateTime endDate)
        {
            var listStatus = new List<int>() { (int)ReservationStatus.Checkin, (int)ReservationStatus.Checkout, (int)ReservationStatus.Pending };

            return _guestReadRepository.GetAllForeingGuestByFilters(startDate, endDate, listStatus);
        }
    }
}
