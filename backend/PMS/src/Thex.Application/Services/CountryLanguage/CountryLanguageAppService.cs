﻿using System;
using Thex.Application.Adapters;
using Thex.Domain.Entities;
using Thex.Application.Interfaces;
using Tnf.Domain.Services;
using Tnf.Notifications;

namespace Thex.Application.Services
{
    public class CountryLanguageAppService : ScaffoldCountryLanguageAppService, ICountryLanguageAppService
    {
        public CountryLanguageAppService(
            ICountryLanguageAdapter countryLanguageAdapter,
            IDomainService<CountryLanguage> countryLanguageDomainService,
            INotificationHandler notificationHandler)
            : base(countryLanguageAdapter, countryLanguageDomainService, notificationHandler)
        {
        }
    }
}
