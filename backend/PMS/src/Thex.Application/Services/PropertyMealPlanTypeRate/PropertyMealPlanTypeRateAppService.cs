﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Thex.Application.Adapters;
using Thex.Application.Interfaces;
using Thex.Common;
using Thex.Common.Enumerations;
using Thex.Domain.Entities;
using Thex.Domain.Interfaces.Repositories;
using Thex.Dto;
using Thex.Infra.ReadInterfaces;
using Thex.Kernel;
using Tnf.Domain.Services;
using Tnf.Dto;
using Tnf.Notifications;
using Tnf.Repositories.Uow;

namespace Thex.Application.Services
{
    public class PropertyMealPlanTypeRateAppService : ScaffoldPropertyMealPlanTypeRateAppService, IPropertyMealPlanTypeRateAppService
    {
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        public readonly IApplicationUser _applicationUser;
        private readonly IPropertyMealPlanTypeRateReadRepository _mealplanRateReadRepository;
        private readonly IPropertyMealPlanTypeRateRepository _mealplaRateRepository;
        private readonly IPropertyBaseRateReadRepository _propertyBaseRateReadRepository;
        private readonly IPropertyBaseRateRepository _propertyBaseRateRepository;
        private readonly IPropertyBaseRateAdapter _propertyBaseRateAdapter;
        private readonly IPropertyMealPlanTypeRateAdapter _propertyMealPlanTypeRateAdapter;
        private readonly IPropertyMealPlanTypeRateHeaderAdapter _propertyMealPlanTypeRateHeaderAdapter;
        private readonly IPropertyMealPlanTypeRateHistoryAdapter _propertyMealPlanTypeRateHistoryAdapter;

        private readonly IDomainService<PropertyMealPlanTypeRateHeader> _propertyMealPlanTypeRateHeaderDomainService;
        private readonly IDomainService<PropertyMealPlanTypeRateHistory> _propertyMealPlanTypeRateHistoryDomainService;

        public PropertyMealPlanTypeRateAppService(
            IUnitOfWorkManager unitOfWorkManager,
            IApplicationUser applicationUser,
            IPropertyMealPlanTypeRateReadRepository mealplanRateReadRepository,
            IPropertyMealPlanTypeRateRepository mealplanRateRepository,
            IPropertyBaseRateReadRepository propertyBaseRateReadRepository,
            IPropertyBaseRateRepository propertyBaseRateRepository,
            IPropertyMealPlanTypeRateHeaderAdapter propertyMealPlanTypeRateHeaderAdapter,
            IPropertyMealPlanTypeRateHistoryAdapter propertyMealPlanTypeRateHistoryAdapter,
            IPropertyMealPlanTypeRateAdapter propertyMealPlanTypeRateAdapter,
            IPropertyBaseRateAdapter propertyBaseRateAdapter,
            IDomainService<PropertyMealPlanTypeRate> propertyMealPlanTypeRateDomainService,
            IDomainService<PropertyMealPlanTypeRateHeader> propertyMealPlanTypeRateHeaderDomainService,
            IDomainService<PropertyMealPlanTypeRateHistory> propertyMealPlanTypeRateHistoryDomainService,
            INotificationHandler notificationHandler)
                : base(propertyMealPlanTypeRateAdapter, propertyMealPlanTypeRateDomainService, notificationHandler)
        {

            _unitOfWorkManager = unitOfWorkManager;
            _applicationUser = applicationUser;
            _mealplanRateReadRepository = mealplanRateReadRepository;
            _mealplaRateRepository = mealplanRateRepository;
            _propertyBaseRateReadRepository = propertyBaseRateReadRepository;
            _propertyBaseRateRepository = propertyBaseRateRepository;

            _propertyBaseRateAdapter = propertyBaseRateAdapter;
            _propertyMealPlanTypeRateAdapter = propertyMealPlanTypeRateAdapter;
            _propertyMealPlanTypeRateHeaderAdapter = propertyMealPlanTypeRateHeaderAdapter;
            _propertyMealPlanTypeRateHistoryAdapter = propertyMealPlanTypeRateHistoryAdapter;

            _propertyMealPlanTypeRateHeaderDomainService = propertyMealPlanTypeRateHeaderDomainService;
            _propertyMealPlanTypeRateHistoryDomainService = propertyMealPlanTypeRateHistoryDomainService;
        }

        public virtual PropertyMealPlanTypeRateHeaderDto GetById(Guid id)
        {
            var result = _mealplanRateReadRepository.GetById(id);

            if (Notification.HasNotification())
                return null;

            return result;
        }

        public virtual IListDto<PropertyMealPlanTypeRateDto> GetAll(GetAllPropertyMealPlanTypeRateDto dto)
        {
            var result = _mealplanRateReadRepository.GetAll(dto);

            if (Notification.HasNotification())
                return null;

            return result;
        }

        public void ToggleActivation(Guid id)
        {
            using (var uow = _unitOfWorkManager.Begin())
            {
                _mealplaRateRepository.ToggleActive(id);

                uow.Complete();
            }
        }

        public async virtual Task<PropertyMealPlanTypeRateHeaderDto> Create(PropertyMealPlanTypeRateHeaderDto dto)
        {
            ValidateDto<PropertyMealPlanTypeRateHeaderDto>(dto, nameof(dto));
            // TODO: Get days of week from Dto
            var days = new List<int> { 0, 1, 2, 3, 4, 5, 6 };

            if (Notification.HasNotification())
                return null;

            using (var uow = _unitOfWorkManager.Begin())
            {
                dto.Id = Guid.NewGuid();
                dto.PropertyId = int.Parse(_applicationUser.PropertyId);
                dto.TenantId = _applicationUser.TenantId;

                ValidatePropertyMealPlanTypeRateDtoList(dto, days);

                if (Notification.HasNotification())
                    return null;

                if (!dto.PropertyMealPlanTypeRateHistoryList.Any(x => x.MealPlanTypeId.Equals((int)MealPlanTypeEnum.None)))
                {
                    dto.PropertyMealPlanTypeRateHistoryList.Add(new PropertyMealPlanTypeRateHistoryDto
                    {
                        MealPlanTypeId = (int)MealPlanTypeEnum.None,
                        MealPlanTypeDefault = false,
                    });
                }
                    
                var headerDto = _propertyMealPlanTypeRateHeaderAdapter.MapWithHistory(dto);
                var result = await _propertyMealPlanTypeRateHeaderDomainService.InsertAndSaveChangesAsync(headerDto);

                if (Notification.HasNotification())
                    return null;

                var itemsToInsert = new List<PropertyMealPlanTypeRate>();
                var itemsToUpdate = new List<PropertyMealPlanTypeRate>();
                BuildItems(dto, ref itemsToInsert, ref itemsToUpdate, days);

                _mealplaRateRepository.CreateOrUpdate(itemsToInsert, itemsToUpdate);

                if (Notification.HasNotification())
                    return null;

                UpdatePropertyBaseRate(dto);

                if (Notification.HasNotification())
                    return null;

                uow.Complete();
            }

            return dto;
        }

        private void ValidatePropertyMealPlanTypeRateDtoList(PropertyMealPlanTypeRateHeaderDto dto, List<int> days)
        {
           

            if (dto.PropertyMealPlanTypeRateHistoryList == null || !dto.PropertyMealPlanTypeRateHistoryList.Any() || days == null || !days.Any())
            {
                NotifyNullParameter();
                return;
            }

            if(dto.PropertyMealPlanTypeRateHistoryList.Any(pmptr => pmptr.AdultMealPlanAmount == null))
            {
                Notification.Raise(Notification.DefaultBuilder
                  .WithMessage(AppConsts.LocalizationSourceName, PropertyMealPlanTypeRate.EntityError.PropertyMealPlanTypeRateMissedValues)
                  .WithDetailedMessage(AppConsts.LocalizationSourceName, PropertyMealPlanTypeRate.EntityError.PropertyMealPlanTypeRateMissedValues)
                  .Build());

                return;
            }

            if (!dto.PropertyMealPlanTypeRateHistoryList.Any(pmptr => pmptr.MealPlanTypeDefault.HasValue && pmptr.MealPlanTypeDefault.Value))
            {
                Notification.Raise(Notification.DefaultBuilder
                  .WithMessage(AppConsts.LocalizationSourceName, PropertyMealPlanTypeRate.EntityError.PropertyMealPlanTypeRateMissedDefault)
                  .WithDetailedMessage(AppConsts.LocalizationSourceName, PropertyMealPlanTypeRate.EntityError.PropertyMealPlanTypeRateMissedDefault)
                  .Build());

                return;
            }
        }

        private void BuildItems(PropertyMealPlanTypeRateHeaderDto request, ref List<PropertyMealPlanTypeRate> itemsToInsert, ref List<PropertyMealPlanTypeRate> itemsToUpdate, List<int> days)
        {
            var itemRequest = new GetAllPropertyMealPlanTypeRateDto() { CurrencyId = request.CurrencyId };
            var propertyMealPlanTypeRateList = _mealplanRateReadRepository.GetAllEntity(itemRequest);

            for (DateTime currDate = request.InitialDate.Date; currDate <= request.FinalDate.Date; currDate = currDate.AddDays(1))
            {
                if (!days.Contains((int)currDate.DayOfWeek))
                    continue;

                foreach (var item in request.PropertyMealPlanTypeRateHistoryList)
                {
                    var dto = new PropertyMealPlanTypeRateDto()
                    {
                        CurrencyId = request.CurrencyId,
                        Date = currDate,
                        PropertyMealPlanTypeRateHeaderId = request.Id,
                        IsActive = true,
                        AdultMealPlanAmount = item.AdultMealPlanAmount,
                        Child1MealPlanAmount = item.Child1MealPlanAmount,
                        Child2MealPlanAmount = item.Child2MealPlanAmount,
                        Child3MealPlanAmount = item.Child3MealPlanAmount,
                        MealPlanTypeId = item.MealPlanTypeId,
                        MealPlanTypeDefault = item.MealPlanTypeDefault,
                        Sunday = days.Contains(0),
                        Monday = days.Contains(1),
                        Tuesday = days.Contains(2),
                        Wednesday = days.Contains(3),
                        Thursday = days.Contains(4),
                        Friday = days.Contains(5),
                        Saturday = days.Contains(6),
                    };

                    var entity = propertyMealPlanTypeRateList.FirstOrDefault(x => x.Date.Date == currDate.Date && x.MealPlanTypeId == item.MealPlanTypeId);
                    if (entity == null)
                    {
                        dto.Id = Guid.NewGuid();
                        dto.PropertyId = request.PropertyId;
                        dto.TenantId = request.TenantId;

                        var _item = _propertyMealPlanTypeRateAdapter.Map(dto).Build();
                        _item.SetAudit(_applicationUser);

                        itemsToInsert.Add(_item);
                    }
                    else
                        itemsToUpdate.AddIfNotContains(_propertyMealPlanTypeRateAdapter.Map(entity, dto).Build());
                }
            };
        }

        private GetAllPropertyMealPlanTypeRateDto CreateCheckDto(PropertyMealPlanTypeRateHeaderDto dto)
        {
            return new GetAllPropertyMealPlanTypeRateDto()
            {
                CurrencyId = dto.CurrencyId,
                StartDate = dto.InitialDate,
                EndDate = dto.FinalDate,
                DateList = Enumerable.Range(0, (dto.FinalDate - dto.InitialDate).Days + 1).Select(d => dto.InitialDate.AddDays(d)).ToList(),
                MealPlanTypeList = dto.PropertyMealPlanTypeRateHistoryList.Select(x => x.MealPlanTypeId).ToList(),
                PropertyId = int.Parse(_applicationUser.PropertyId)
            };
        }

        private void UpdatePropertyBaseRate(PropertyMealPlanTypeRateHeaderDto dto)
        {
            var propertyBaseRateListToUpdate = new List<PropertyBaseRate>();
            var propertyBaseRateList = _propertyBaseRateReadRepository.GetAllWithPropertyMealPlanTypeRate(dto, int.Parse(_applicationUser.PropertyId));
            if (propertyBaseRateList == null || !propertyBaseRateList.Any()) return;

            var mealPlanTypeDefaultId = dto.PropertyMealPlanTypeRateHistoryList.FirstOrDefault(pmptrh => pmptrh.MealPlanTypeDefault.HasValue && pmptrh.MealPlanTypeDefault.Value)?.MealPlanTypeId;

            foreach (var propertyBaseRate in propertyBaseRateList)
            {
                var propertyBaseRateHistoryDto = dto.PropertyMealPlanTypeRateHistoryList.FirstOrDefault(pmptrh => pmptrh.MealPlanTypeId == propertyBaseRate.MealPlanTypeId);
                if (propertyBaseRateHistoryDto == null) continue;

                var entity = _propertyBaseRateAdapter.MapUpdateMeaPlanTypes(propertyBaseRate, propertyBaseRateHistoryDto, mealPlanTypeDefaultId.GetValueOrDefault()).Build();
                entity.SetAudit(_applicationUser);
                propertyBaseRateListToUpdate.Add(entity);
            }

            _propertyBaseRateRepository.CreateAndUpdate(new List<PropertyBaseRate>(), propertyBaseRateListToUpdate);
        }
    }
}
