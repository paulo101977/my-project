﻿ using System;
using Thex.Application.Interfaces;
using Thex.Domain.Interfaces.Repositories;
using Thex.Dto;
using Thex.Infra.ReadInterfaces;
using Thex.Kernel;
using Tnf.Notifications;
using System.Linq;
using Tnf.Repositories.Uow;
using Thex.Application.Adapters;
using System.Collections.Generic;
using Thex.Common.Enumerations;
using Thex.Dto.PropertyCurrencyExchange;
using Thex.Dto.BillingAccount;
using Thex.Domain.Entities;
using Thex.Common;
using Thex.Common.Helpers;

namespace Thex.Application.Services
{
    public class TourismTaxAppService : ApplicationServiceBase, ITourismTaxAppService
    {
        private readonly IApplicationUser _applicationUser;
        private readonly ITourismTaxReadRepository _tourismTaxReadRepository;
        private readonly ITourismTaxRepository _tourismTaxRepository;
        private readonly IPropertyGuestTypeRepository _propertyGuestTypeRepository;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly ITourismTaxAdapter _tourismTaxAdapter;
        private readonly IPropertyGuestTypeReadRepository _propertyGuestTypeReadRepository;
        private readonly IBillingAccountReadRepository _billingAccountReadRepository;
        private readonly IBillingAccountItemAppService _billingAccountItemAppService;
        private readonly IPropertyParameterReadRepository _propertyParameterReadRepository;
        private readonly IPropertyCurrencyExchangeReadRepository _propertyCurrencyExchangeReadRepository;
        private readonly IReservationBudgetReadRepository _reservationBudgetReadRepository;
        private readonly IBillingAccountItemReadRepository _billingAccountItemReadRepository;
        private readonly IGuestReservationItemReadRepository _guestReservationItemReadRepository;
        private readonly IReservationItemLaunchAppService _reservationItemLaunchAppService;
        private readonly IBillingAccountItemTransferAppService _billingAccountItemTransferAppService;

        public TourismTaxAppService(
            INotificationHandler notificationHandler,
            IApplicationUser applicationUser,
            ITourismTaxReadRepository tourismTaxReadRepository,
            ITourismTaxRepository tourismTaxRepository,
            IPropertyGuestTypeRepository propertyGuestTypeRepository,
            IUnitOfWorkManager unitOfWorkManager,
            ITourismTaxAdapter tourismTaxAdapter,
            IPropertyGuestTypeReadRepository propertyGuestTypeReadRepository,
            IBillingAccountReadRepository billingAccountReadRepository,
            IBillingAccountItemAppService billingAccountItemAppService,
            IPropertyParameterReadRepository propertyParameterReadRepository,
            IPropertyCurrencyExchangeReadRepository propertyCurrencyExchangeReadRepository,
            IReservationBudgetReadRepository reservationBudgetReadRepository,
            IBillingAccountItemReadRepository billingAccountItemReadRepository,
            IGuestReservationItemReadRepository guestReservationItemReadRepository,
            IReservationItemLaunchAppService reservationItemLaunchAppService,
            IBillingAccountItemTransferAppService billingAccountItemTransferAppService)
            : base(notificationHandler)
        {
            _applicationUser = applicationUser;
            _tourismTaxReadRepository = tourismTaxReadRepository;
            _propertyGuestTypeRepository = propertyGuestTypeRepository;
            _unitOfWorkManager = unitOfWorkManager;
            _tourismTaxAdapter = tourismTaxAdapter;
            _propertyGuestTypeReadRepository = propertyGuestTypeReadRepository;
            _tourismTaxRepository = tourismTaxRepository;
            _billingAccountReadRepository = billingAccountReadRepository;
            _billingAccountItemAppService = billingAccountItemAppService;
            _propertyParameterReadRepository = propertyParameterReadRepository;
            _propertyCurrencyExchangeReadRepository = propertyCurrencyExchangeReadRepository;
            _reservationBudgetReadRepository = reservationBudgetReadRepository;
            _billingAccountItemReadRepository = billingAccountItemReadRepository;
            _guestReservationItemReadRepository = guestReservationItemReadRepository;
            _reservationItemLaunchAppService = reservationItemLaunchAppService;
            _billingAccountItemTransferAppService = billingAccountItemTransferAppService;
        }

        public TourismTaxDto GetByPropertyId()
        {
            ValidateCountry();

            if (Notification.HasNotification())
                return TourismTaxDto.NullInstance;

            var tourismTaxDto = _tourismTaxReadRepository.GetByPropertyId();

            if (tourismTaxDto == null)
                return TourismTaxDto.NullInstance;

            var propertyId = int.Parse(_applicationUser.PropertyId);

            tourismTaxDto.PropertyGuestTypeList = _propertyGuestTypeReadRepository.GetAllByPropertyId(propertyId, true);

            return tourismTaxDto;
        }

        public TourismTaxDto Create(TourismTaxDto dto)
        {
            ValidateDto(dto, nameof(dto));

            if (Notification.HasNotification())
                return TourismTaxDto.NullInstance;

            ValidateCountry();

            if (Notification.HasNotification())
                return TourismTaxDto.NullInstance;

            var propertyId = int.Parse(_applicationUser.PropertyId);

            using (var uow = _unitOfWorkManager.Begin())
            {
                var entity = _tourismTaxAdapter.MapToCreate(dto, propertyId).Build();

                if (Notification.HasNotification())
                    return TourismTaxDto.NullInstance;

                dto.Id = _tourismTaxRepository.Insert(entity).Id;

                _propertyGuestTypeRepository.UpdateIsExemptOfTourismTax(dto.GetExemptGuestTypeIdList(), true);

                uow.Complete();
            }

            return dto;
        }

        public TourismTaxDto Update(TourismTaxDto dto)
        {
            ValidateDto(dto, nameof(dto));

            if (Notification.HasNotification())
                return TourismTaxDto.NullInstance;

            ValidateCountry();

            if (Notification.HasNotification())
                return TourismTaxDto.NullInstance;

            var propertyId = int.Parse(_applicationUser.PropertyId);

            var entity = _tourismTaxAdapter.MapToUpdate(_tourismTaxRepository.Get(dto.Id), dto).Build();

            if (Notification.HasNotification())
                return TourismTaxDto.NullInstance;

            ValidateTenantId(entity.TenantId);

            if (Notification.HasNotification())
                return TourismTaxDto.NullInstance;

            using (var uow = _unitOfWorkManager.Begin())
            {
                _tourismTaxRepository.Update(entity);

                _propertyGuestTypeRepository.UpdateIsExemptOfTourismTax(dto.GetExemptGuestTypeIdList(), true);

                var notExemptGuestTypeIdList = _propertyGuestTypeReadRepository.GetAllByPropertyId(propertyId)
                    .Where(p => !dto.GetExemptGuestTypeIdList().Contains(p.Id)).Select(p => p.Id).ToList();

                _propertyGuestTypeRepository.UpdateIsExemptOfTourismTax(notExemptGuestTypeIdList, false);

                uow.Complete();
            }

            return dto;
        }

        public void ToggleActivation(Guid id)
        {
            ValidateCountry();

            if (Notification.HasNotification()) return;

            _tourismTaxRepository.ToggleActivation(id);
        }

        public void Launch(List<TourismTaxLaunchRequestDto> requestDtoList)
        {
            ValidateCountry();

            if (Notification.HasNotification()) return;

            var tourismTaxDto = GetByPropertyId();

            if (tourismTaxDto == null || (tourismTaxDto != null && !tourismTaxDto.IsActive))
                return;

            TourismTaxLaunchSearchDto dtoSearch = MapDtoSearch(requestDtoList, tourismTaxDto);

            var billingAccountGuestReservationItemDtoList = _billingAccountReadRepository.GetAllAccountGuestReservationItemDtoByFilters(dtoSearch);

            if (TotalAccountIsDifferentOfTotalGuests(requestDtoList, billingAccountGuestReservationItemDtoList))
                AddBillintAccountIsNotMain(dtoSearch, billingAccountGuestReservationItemDtoList);

            if (billingAccountGuestReservationItemDtoList.Any())
            {
                var systemDate = _propertyParameterReadRepository.GetSystemDate(int.Parse(_applicationUser.PropertyId)).Value;
                var propertyCurrency = _propertyParameterReadRepository.GetPropertyParameterForPropertyIdAndApplicationParameterId(int.Parse(_applicationUser.PropertyId), 4)?.PropertyParameterValue;
                var currentQuotationList = _propertyCurrencyExchangeReadRepository.GetCurrentQuotationList(int.Parse(_applicationUser.PropertyId), null);
                var reservationBudgetList = _reservationBudgetReadRepository.GetAllByReservationItemId(billingAccountGuestReservationItemDtoList.FirstOrDefault().ReservationItemId);
                var billingAccountItemDtoList = new List<BillingAccountItemTourismTaxDto>();

                var billingAccountItemTourismTaxDtoList = _billingAccountItemReadRepository.GetAllAccountItemTourismTaxByAccountIdList(billingAccountGuestReservationItemDtoList
                    .Select(b => b.BillingAccountId).ToList());

                if (!tourismTaxDto.IsTotalOfNights)
                    billingAccountGuestReservationItemDtoList = FilterListByQuantityOfTourismTaxLaunched(tourismTaxDto, billingAccountGuestReservationItemDtoList);

                foreach (var dto in billingAccountGuestReservationItemDtoList)
                {
                    var numberOfNightsToBilling = tourismTaxDto.NumberOfNights;

                    for (var checkinDate = dto.CheckinDate.Date; checkinDate.Date < dto.EstimatedDepartureDate.Date && (tourismTaxDto.IsTotalOfNights || (!tourismTaxDto.IsTotalOfNights 
                        && numberOfNightsToBilling > 0)); checkinDate = checkinDate.AddDays(1))
                    {
                        var tourismTaxWasReleased = billingAccountItemTourismTaxDtoList.Where(b => b.BillingAccountItemDate.Date == checkinDate.Date
                            && b.IsTourismTax.HasValue && b.IsTourismTax.Value && b.BillingAccountId == dto.BillingAccountId).Any();

                        if (!tourismTaxWasReleased)
                        {
                            var reservationBudgetCurrent = reservationBudgetList.FirstOrDefault(r => r.BudgetDay.Date == checkinDate.Date);

                            if (reservationBudgetCurrent != null)
                            {
                                var currencyExchangeResultDto = GetCurrencyAndCotationList(reservationBudgetCurrent.CurrencyId, propertyCurrency, currentQuotationList);

                                if (currencyExchangeResultDto != null)
                                    billingAccountItemDtoList.Add(MapBillintAccountItem(tourismTaxDto, dto.BillingAccountId, checkinDate, reservationBudgetCurrent, currencyExchangeResultDto, systemDate));
                            }
                        }
                        numberOfNightsToBilling--;
                    }
                }

                using (var uow = _unitOfWorkManager.Begin())
                {
                    if (billingAccountItemDtoList.Any())
                        _billingAccountItemAppService.CreateAllTaxTourismDebit(billingAccountItemDtoList);

                    var dtoToCreateList = billingAccountGuestReservationItemDtoList
                        .Where(b => billingAccountItemDtoList.Select(bai => bai.BillingAccountId).Contains(b.BillingAccountId)).ToList();

                    if (dtoToCreateList.Any())
                        _reservationItemLaunchAppService.CreateToTourismTax(dtoToCreateList, tourismTaxDto);

                    uow.Complete();
                }
            }
        }

        public void Launch()
        {
            var requestListDto = new List<TourismTaxLaunchRequestDto>();

            var guestReservationItemIdList = _guestReservationItemReadRepository.GetAllIdByStatus((int)ReservationStatus.Checkin)
                    .Select(g => (long?)g).ToList();

            foreach (var guestReservationItemId in guestReservationItemIdList)
                requestListDto.Add(new TourismTaxLaunchRequestDto { GuestReservationItemId = guestReservationItemId });

            ValidateCountry();

            if (Notification.HasNotification()) return;

            var tourismTaxDto = GetByPropertyId();

            if (tourismTaxDto == null || (tourismTaxDto != null && !tourismTaxDto.IsActive))
                return;

            TourismTaxLaunchSearchDto dtoSearch = MapDtoSearch(requestListDto, tourismTaxDto);

            var billingAccountGuestReservationItemDtoList = _billingAccountReadRepository.GetAllAccountGuestReservationItemDtoByFilters(dtoSearch);

            if (TotalAccountIsDifferentOfTotalGuests(requestListDto, billingAccountGuestReservationItemDtoList))
                AddBillintAccountIsNotMain(dtoSearch, billingAccountGuestReservationItemDtoList);

            billingAccountGuestReservationItemDtoList = billingAccountGuestReservationItemDtoList.OrderByDescending(b => b.IsMainAccount).DistinctBy(b => b.GuestReservationItemId).ToList();

            if (billingAccountGuestReservationItemDtoList.Any())
            {
                var propertyCurrency = _propertyParameterReadRepository.GetPropertyParameterForPropertyIdAndApplicationParameterId(int.Parse(_applicationUser.PropertyId), 4)?.PropertyParameterValue;
                var currentQuotationList = _propertyCurrencyExchangeReadRepository.GetCurrentQuotationList(int.Parse(_applicationUser.PropertyId), null);
                var reservationBudgetList = _reservationBudgetReadRepository.GetAllByReservationItemId(billingAccountGuestReservationItemDtoList.FirstOrDefault().ReservationItemId);
                var billingAccountItemDtoList = new List<BillingAccountItemTourismTaxDto>();

                var billingAccountItemTourismTaxDtoList = _billingAccountItemReadRepository.GetAllAccountItemTourismTaxByAccountIdList(billingAccountGuestReservationItemDtoList
                    .Select(b => b.BillingAccountId).ToList());

                if (!tourismTaxDto.IsTotalOfNights)
                    billingAccountGuestReservationItemDtoList = FilterListByQuantityOfTourismTaxLaunched(tourismTaxDto, billingAccountGuestReservationItemDtoList);

                var billingAccountGroupList = new List<BillingAccount>();

                if (EnumHelper.GetEnumValue<CountryIsoCodeEnum>(_applicationUser.PropertyCountryCode, true) == CountryIsoCodeEnum.Europe)
                {
                    var reservationIdList = billingAccountGuestReservationItemDtoList
                    .Where(b => b.ReservationUnifyAccounts)
                    .Select(b => b.ReservationId)
                    .Distinct()
                    .ToList();

                    billingAccountGroupList = _billingAccountReadRepository.GetAllByFilters(reservationIdList,
                        BillingAccountTypeEnum.GroupAccount, AccountBillingStatusEnum.Opened);
                }

                var billingAccountIdOfReservationItemAndGroupDictionary = new Dictionary<Guid, BillingAccount>();
                var billingAccountItemOfGroupList = new List<BillingAccountItemTourismTaxDto>();

                foreach (var dto in billingAccountGuestReservationItemDtoList)
                {
                    var systemDate = _propertyParameterReadRepository.GetSystemDate(int.Parse(_applicationUser.PropertyId)).Value;

                    var tourismTaxWasReleased = billingAccountItemTourismTaxDtoList.Any(b => b.BillingAccountItemDate.Date == systemDate.Date
                                && b.IsTourismTax.HasValue && b.IsTourismTax.Value && b.BillingAccountId == dto.BillingAccountId);

                    if (!tourismTaxWasReleased)
                    {
                        var reservationBudgetCurrent = reservationBudgetList.FirstOrDefault(r => r.BudgetDay.Date == systemDate.Date);

                        if (reservationBudgetCurrent != null)
                        {
                            var currencyExchangeResultDto = GetCurrencyAndCotationList(reservationBudgetCurrent.CurrencyId, propertyCurrency, currentQuotationList);

                            if (currencyExchangeResultDto != null)
                            {
                                billingAccountItemDtoList.Add(MapBillintAccountItem(tourismTaxDto, dto.BillingAccountId, systemDate.Date, reservationBudgetCurrent, currencyExchangeResultDto, systemDate));

                                if (dto.ReservationUnifyAccounts)
                                {
                                    var billingAccountGroup = billingAccountGroupList.FirstOrDefault(b => b.ReservationId == dto.ReservationId);
                                    if (billingAccountGroup != null)
                                    {
                                        billingAccountIdOfReservationItemAndGroupDictionary.Add(dto.BillingAccountId, billingAccountGroup);
                                        billingAccountItemOfGroupList.Add(MapBillintAccountItem(tourismTaxDto, billingAccountGroup.Id, systemDate.Date, reservationBudgetCurrent, currencyExchangeResultDto, systemDate, dto.BillingAccountId));
                                    }
                                }
                            }
                        }
                    }
                }

                var billingAccountItemList = new List<BillingAccountItem>();

                if (billingAccountItemOfGroupList.Any())
                    billingAccountItemList = _billingAccountItemAppService.CreateAllTaxTourismDebit(GetAllBillingAccountsWithoutGroup(billingAccountItemDtoList.Concat(billingAccountItemOfGroupList).ToList(),
                        billingAccountIdOfReservationItemAndGroupDictionary));
                else
                    billingAccountItemList = _billingAccountItemAppService.CreateAllTaxTourismDebit(billingAccountItemDtoList);

                RedirectItens(billingAccountIdOfReservationItemAndGroupDictionary, billingAccountItemList);

                UpdateTourismTaxInReservationItem(tourismTaxDto, billingAccountGuestReservationItemDtoList, billingAccountItemDtoList);
            }
        }

        private List<BillingAccountItemTourismTaxDto> GetAllBillingAccountsWithoutGroup(List<BillingAccountItemTourismTaxDto> billingAccountItemDtoList, Dictionary<Guid, BillingAccount> billingAccountIdOfReservationItemAndGroupDictionary)
        {
            return billingAccountItemDtoList
                                .Where(b => !billingAccountIdOfReservationItemAndGroupDictionary.Keys.Contains(b.BillingAccountId))
                                .ToList();
        }

        private void RedirectItens(Dictionary<Guid, BillingAccount> billingAccountIdOfReservationItemAndGroupDictionary, List<BillingAccountItem> billingAccountItemList)
        {
            var transferDtoList = new List<BillingAccountItemTransferDto>();

            SetTransferList(billingAccountItemList, billingAccountIdOfReservationItemAndGroupDictionary, transferDtoList);

            _billingAccountItemTransferAppService.CreateRange(transferDtoList);
        }

        private void SetTransferList(List<BillingAccountItem> billingAccountItemList, Dictionary<Guid, BillingAccount> billingAccountIdOfReservationItemAndGroupDictionary, List<BillingAccountItemTransferDto> transferDtoList)
        {
            foreach (var item in billingAccountIdOfReservationItemAndGroupDictionary)
            {
                var billingAccountIdOfReservationItem = item.Key;
                var billingAccountIdOfGroup = item.Value.Id;
                var billingAccountItem = billingAccountItemList.FirstOrDefault(b => b.BillingAccountId == billingAccountIdOfGroup) == null ? null
                        : billingAccountItemList.FirstOrDefault(b => b.BillingAccountId == billingAccountIdOfGroup);

                transferDtoList.Add(new BillingAccountItemTransferDto()
                {
                    TransferDate = DateTime.UtcNow.ToZonedDateTimeLoggedUser(),
                    BillingAccountItemId = billingAccountItem == null ? Guid.Empty : billingAccountItem.Id,
                    BillingAccountIdSource = billingAccountIdOfReservationItem,
                    BillingAccountIdDestination = billingAccountIdOfGroup
                });

                billingAccountItemList.Remove(billingAccountItem);
            }   
        }

        private void UpdateTourismTaxInReservationItem(TourismTaxDto tourismTaxDto, List<BillingAccountGuestReservationItemDto> billingAccountGuestReservationItemDtoList, List<BillingAccountItemTourismTaxDto> billingAccountItemDtoList)
        {
            var dtoToUpdateList = billingAccountGuestReservationItemDtoList
                                    .Where(b => billingAccountItemDtoList.Select(bai => bai.BillingAccountId).Contains(b.BillingAccountId)).ToList();

            if (dtoToUpdateList.Any())
                _reservationItemLaunchAppService.UpdateToTourismTax(dtoToUpdateList, tourismTaxDto);
        }

        private void ValidateTenantId(Guid tenantId)
        {
            if (tenantId != _applicationUser.TenantId)
                Notification.Raise(Notification.DefaultBuilder
                .WithMessage(AppConsts.LocalizationSourceName, Tenant.EntityError.TenantUserInvalid)
                .WithDetailedMessage(AppConsts.LocalizationSourceName, Tenant.EntityError.TenantUserInvalid)
                .Build());
        }

        private void ValidateCountry()
        {
            if (EnumHelper.GetEnumValue<CountryIsoCodeEnum>(_applicationUser.PropertyCountryCode, true) != CountryIsoCodeEnum.Europe)
                Notification.Raise(Notification.DefaultBuilder
                          .WithMessage(AppConsts.LocalizationSourceName, TourismTax.EntityError.TourismTaxCountryIsNotRegistered)
                          .WithDetailedMessage(AppConsts.LocalizationSourceName, TourismTax.EntityError.TourismTaxCountryIsNotRegistered)
                          .Build());
        }

        private bool TotalAccountIsDifferentOfTotalGuests(List<TourismTaxLaunchRequestDto> requestListDto, List<BillingAccountGuestReservationItemDto> billingAccountTourismTaxDtoList)
        {
            return billingAccountTourismTaxDtoList.Count() < requestListDto.Count;
        }

        private void AddBillintAccountIsNotMain(TourismTaxLaunchSearchDto dtoSearch, List<BillingAccountGuestReservationItemDto> billingAccountGuestReservationItemDtoList)
        {
            dtoSearch.IsMainAccount = false;

            var isNotMainDtoList = _billingAccountReadRepository.GetAllAccountGuestReservationItemDtoByFilters(dtoSearch);

            if (isNotMainDtoList.Any())
                billingAccountGuestReservationItemDtoList.AddRange(isNotMainDtoList);
        }

        private BillingAccountItemTourismTaxDto MapBillintAccountItem(TourismTaxDto tourismTaxDto, Guid billingAccountId, DateTime date, ReservationBudget reservationBudgetCurrent, 
            CurrencyExchangeResultDto currencyExchangeResultDto, DateTime systemDate, Guid? billingAccountIdLastSource = null)
        {
            return new BillingAccountItemTourismTaxDto
            {
                PropertyId = int.Parse(_applicationUser.PropertyId),
                Amount = tourismTaxDto.Amount,
                CurrencyId = reservationBudgetCurrent == null ? (Guid?)null : reservationBudgetCurrent.CurrencyId,
                BillingItemId = tourismTaxDto.BillingItemId,
                CurrencyExchangeReferenceId = currencyExchangeResultDto.CurrencyExchangeReferenceId,
                CurrencyExchangeReferenceSecId = currencyExchangeResultDto.CurrencyExchangeReferenceSecId,
                DateParameter = systemDate,
                BillingAccountItemDate = date,
                BillingAccountId = billingAccountId,
                BillingAccountIdLastSource = billingAccountIdLastSource
            };
        }

        private TourismTaxLaunchSearchDto MapDtoSearch(List<TourismTaxLaunchRequestDto> requestListDto, TourismTaxDto tourismTaxDto)
        {
            return new TourismTaxLaunchSearchDto
            {
                GuestReservationItemIdList = requestListDto.Select(r => r.GuestReservationItemId).ToList(),
                IsChild = false,
                IsMainAccount = true,
                ReservationItemStatus = (int)ReservationStatus.Checkin,
                StatusAccount = (int)AccountBillingStatusEnum.Opened,
                PropertyGuestTypeIdList = tourismTaxDto.GetNotExemptGuestTypeIdList(),
                NumberOfNights = tourismTaxDto?.NumberOfNights
            };
        }

        private CurrencyExchangeResultDto GetCurrencyAndCotationList(Guid currencyId, string propertyCurrency, List<QuotationCurrentDto> currentQuotationList)
        {
            var propertyCurrencyId = propertyCurrency == null ? Guid.Empty : Guid.Parse(propertyCurrency);

            var dto = new CurrencyExchangeResultDto();
            dto.CurrencyExchangeReferenceId = GetCurrencyExchangeReference(propertyCurrencyId, currentQuotationList, currencyId);
            dto.CurrencyExchangeReferenceSecId = GetCurrencyExchangeReferenceSec(propertyCurrencyId, currentQuotationList, dto.CurrencyExchangeReferenceId);

            return dto;
        }

        private Guid? GetCurrencyExchangeReference(Guid propertyCurrencyId, List<QuotationCurrentDto> currentQuotationList, Guid currencyId)
        {
            return propertyCurrencyId == Guid.Empty || currencyId == propertyCurrencyId ? null :
                                              currentQuotationList.FirstOrDefault(c => c.CurrencyId.Equals(currencyId))?.Id;
        }

        private Guid? GetCurrencyExchangeReferenceSec(Guid propertyCurrencyId, List<QuotationCurrentDto> currentQuotationList, Guid? currencyExchangeReferenceId)
        {
            return currencyExchangeReferenceId == null ? null :
                                                 currentQuotationList.FirstOrDefault(c => c.CurrencyId.Equals(propertyCurrencyId))?.Id;
        }

        private int GetDaysOfreservationItem(BillingAccountGuestReservationItemDto item)
        {
            var period = item.EstimatedDepartureDate.Date - item.CheckinDate.Date;
            return period.Days <= 0 ? 1 : period.Days;
        }

        private List<BillingAccountGuestReservationItemDto> FilterListByQuantityOfTourismTaxLaunched(TourismTaxDto tourismTaxDto, List<BillingAccountGuestReservationItemDto> billingAccountGuestReservationItemDtoList)
        {
            billingAccountGuestReservationItemDtoList = billingAccountGuestReservationItemDtoList
                .Where(b => !b.QuantityOfTourismTaxLaunched.HasValue
                    || (b.QuantityOfTourismTaxLaunched.HasValue && b.QuantityOfTourismTaxLaunched.Value < tourismTaxDto.NumberOfNights)).ToList();
            return billingAccountGuestReservationItemDtoList;
        }
    }
}
