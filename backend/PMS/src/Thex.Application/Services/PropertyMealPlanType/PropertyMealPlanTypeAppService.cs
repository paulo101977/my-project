﻿using System;
using Thex.Application.Adapters;
using Thex.Domain.Entities;
using Thex.Application.Interfaces;
using Tnf.Domain.Services;
using Thex.Infra.ReadInterfaces;
using Thex.Dto;
using Tnf.Dto;
using System.Collections.Generic;
using Thex.Domain.Interfaces.Repositories;
using System.Linq;
using Tnf.Notifications;
using Thex.Common;
using Thex.Common.Enumerations;
using System.Threading.Tasks;
using Thex.Kernel;

namespace Thex.Application.Services
{
    public class PropertyMealPlanTypeAppService : ScaffoldPropertyMealPlanTypeAppService, IPropertyMealPlanTypeAppService
    {
        private readonly IPropertyMealPlanTypeReadRepository _propertyMealPlanTypeReadRepository;
        private readonly IPropertyMealPlanTypeRepository _propertyMealPlanTypeRepository;
        private readonly IApplicationUser _applicationUser;

        public PropertyMealPlanTypeAppService(
            IPropertyMealPlanTypeAdapter propertyMealPlanTypeAdapter,
            IApplicationUser applicationUser,
            IDomainService<PropertyMealPlanType> propertyMealPlanTypeDomainService,
            IPropertyMealPlanTypeReadRepository propertyMealPlanTypeReadRepository,
            IPropertyMealPlanTypeRepository propertyMealPlanTypeRepository,
            INotificationHandler notificationHandler)
            : base(propertyMealPlanTypeAdapter, propertyMealPlanTypeDomainService, notificationHandler)
        {
            _propertyMealPlanTypeReadRepository = propertyMealPlanTypeReadRepository;
            _propertyMealPlanTypeRepository = propertyMealPlanTypeRepository;
            _applicationUser = applicationUser;
        }

        public IListDto<PropertyMealPlanTypeDto> GetAllPropertyMealPlanTypeByProperty(int propertyId, bool all = false)
        {
            return _propertyMealPlanTypeReadRepository.GetAllPropertyMealPlanTypeByPropertyWithApplicationMealPlanType(propertyId, all);
        }

        public async Task<IListDto<PropertyMealPlanTypeDto>> GetAllPropertyMealPlanTypeByProperty(bool all = false)
        {            
            return await _propertyMealPlanTypeReadRepository.GetAllPropertyMealPlanTypeAsync(int.Parse(_applicationUser.PropertyId), all);
        }

        public void PropertyMealPlanTypeUpdate(IList<PropertyMealPlanTypeDto> propertyMealPlanTypeList)
        {
            if (propertyMealPlanTypeList != null && propertyMealPlanTypeList.Count > 0)
            {
                foreach (var propertyMealPlanType in propertyMealPlanTypeList)
                {
                    var builder = PropertyMealPlanTypeAdapter.Map(propertyMealPlanType);

                    if (Notification.HasNotification()) break;

                    if (propertyMealPlanType.Id == null || propertyMealPlanType.Id == Guid.Empty)
                        PropertyMealPlanTypeDomainService.InsertAndSaveChanges(builder);
                    else
                        _propertyMealPlanTypeRepository.PropertyMealPlanTypeUpdate(builder.Build());
                }
            }
        }

        public void ToggleActivation(Guid id)
        {
            if (id == Guid.Empty)
            {
                NotifyNullParameter();
                return;
            }

            var propertyMealPlanType = _propertyMealPlanTypeRepository.GetById(id);
            if (propertyMealPlanType == null)
            {
                Notification.Raise(Notification
                .DefaultBuilder
                .WithMessage(AppConsts.LocalizationSourceName, PropertyMealPlanTypeEnum.Error.PropertyMealPlanTypeDoesNotExist)
                .Build());
                return;
            }

            if(propertyMealPlanType.IsActive.HasValue && propertyMealPlanType.IsActive.Value)
                ValidateIfExistsPropertyMealPlanTypeActive(id);

            if (Notification.HasNotification()) return;

            _propertyMealPlanTypeRepository.ToggleAndSaveIsActive(propertyMealPlanType);
        }

        private void ValidateIfExistsPropertyMealPlanTypeActive(Guid id)
        {
           if(!_propertyMealPlanTypeReadRepository.ExistsPropertyMealPlanTypeActive(id))
            {
                Notification.Raise(Notification
               .DefaultBuilder
               .WithMessage(AppConsts.LocalizationSourceName, PropertyMealPlanType.EntityError.CanNotDeactiveAllMealPlanTypes)
               .Build());
            }
        }
    }
}
