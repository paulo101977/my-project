﻿using Thex.Application.Adapters;
using Thex.Application.Interfaces;
using Thex.Domain.Entities;
using Thex.Dto;
using Thex.Dto.ReservationItem;
using Thex.Infra.ReadInterfaces;
using Tnf.Domain.Services;
using Tnf.Dto;


namespace Thex.Application.Services
{
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Web;
    using Thex.Common;
    using Thex.Common.Dto.Observable;
    using Thex.Common.Emails.AwsSes;
    using Thex.Common.Enumerations;
    using Thex.Common.Interfaces.Observables;
    using Thex.Common.Message;
    using Thex.Domain.Interfaces.Repositories;
    using Thex.Domain.Interfaces.Services;
    using Thex.Inventory.Domain.Services;
    using Thex.Kernel;
    using Thex.Maps.Geocode;
    using Tnf.Localization;
    using Tnf.Notifications;
    using Tnf.Repositories;
    using Tnf.Repositories.Uow;

    public class ReservationItemAppService : ScaffoldReservationItemAppService, IReservationItemAppService
    {
        protected readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IReservationItemReadRepository _reservationItemReadRepository;
        private readonly IReservationItemDomainService _reservationItemDomainService;
        private readonly IRepository<ReservationItem> _repository;
        private readonly IApplicationUser _applicationUser;
        private readonly IReservationItemStatusDomainService _reservationItemStatusDomainService;
        private readonly EntityFrameworkCore.ReadInterfaces.Repositories.IRoomReadRepository _roomReadRepository;
        private readonly IPropertyReadRepository _propertyReadRepository;
        private readonly IHousekeepingStatusPropertyAppService _housekeepingStatusAppService;
        private readonly IReservationBudgetAppService _reservationBudgetAppService;
        private readonly Domain.Interfaces.Repositories.IGuestReservationItemReadRepository _guestReservationItemReadRepository;
        private readonly Domain.Interfaces.Repositories.IGuestReservationItemRepository _guestReservationItemRepository;
        private readonly IReservationConfirmationAppService _reservationConfirmationAppService;
        private readonly IPropertyParameterReadRepository _propertyParameterReadRepository;
        private readonly IConfiguration _configuration;
        private readonly IBillingAccountReadRepository _billingAccountReadRepository;
        private readonly ILocalizationManager _localizationManager;
        private readonly ILocationReadRepository _locationReadRepository;
        private readonly IRoomRepository _roomRepository;
        private readonly IHousekeepingStatusPropertyAppService _housekeepingStatusPropertyAppService;
        private readonly IHousekeepingStatusPropertyReadRepository _housekeepingStatusPropertyReadRepository;
        private readonly IHousekeepingStatusPropertyRepository _housekeepingStatusPropertyRepository;
        private readonly IBillingAccountRepository _billingAccountRepository;
        private readonly IGuestReservationItemRepository _guestItemRepository;
        private readonly ICurrencyReadRepository _currencyReadRepository;
        private readonly IRoomOccupiedDomainService _RoomOccupiedDomainService;
        protected readonly IRoomOccupiedAdapter _RoomOccupiedAdapter;
        private readonly IRoomOccupiedReadRepository _roomOccupiedReadRepository;
        private readonly IReservationBudgetRepository _reservationBudgetRepository;
        private IList<IReservationItemChangeStatusObservable> _reservationChangeItemStatusObservableList;
        private IList<IReservationItemChangeRoomTypeOrPeriodObservable> _reservationItemChangeRoomTypeOrPeriodObservableList;
        private readonly IReservationItemRepository _reservationItemRepository;

        public ReservationItemAppService(
            IUnitOfWorkManager unitOfWorkManager,
            IPropertyParameterReadRepository propertyParameterReadRepository,
            IReservationItemAdapter reservationItemAdapter,
            IReservationItemDomainService reservationItemDomainService,
            IDomainService<ReservationItem> reservationItemAppDomainService,
            IReservationItemReadRepository reservationItemReadRepository,
            IRepository<ReservationItem> repository,
            IReservationItemStatusDomainService reservationItemStatusDomainService,
            EntityFrameworkCore.ReadInterfaces.Repositories.IRoomReadRepository roomReadRepository,
            IHousekeepingStatusPropertyAppService housekeepingStatusAppService,
            IApplicationUser applicationUser,
            IReservationBudgetAppService reservationBudgetAppService,
            ILocationReadRepository locationReadRepository,
            IConfiguration configuration,
            IReservationConfirmationAppService reservationConfirmationAppService,
            Domain.Interfaces.Repositories.IGuestReservationItemReadRepository guestReservationItemReadRepository,
            Domain.Interfaces.Repositories.IGuestReservationItemRepository guestReservationItemRepository,
             IPropertyReadRepository propertyReadRepository,
             IBillingAccountReadRepository billingAccountReadRepository,
             ILocalizationManager localizationManager,
             INotificationHandler notificationHandler,
             IRoomRepository roomRepository,
             IHousekeepingStatusPropertyAppService housekeepingStatusPropertyAppService,
             IHousekeepingStatusPropertyReadRepository housekeepingStatusPropertyReadRepository,
             IHousekeepingStatusPropertyRepository housekeepingStatusPropertyRepository,
             IBillingAccountRepository billingAccountRepository,
             IGuestReservationItemRepository guestItemRepository,
             ICurrencyReadRepository currencyReadRepository,
             IServiceProvider serviceProvider,
             IRoomOccupiedDomainService RoomOccupiedDomainService,
             IRoomOccupiedAdapter RoomOccupiedAdapter,
             IReservationBudgetRepository reservationBudgetRepository,
             IRoomOccupiedReadRepository roomOccupiedReadRepository,
             IReservationItemRepository reservationItemRepository)
            : base(reservationItemAdapter, reservationItemAppDomainService, notificationHandler, RoomOccupiedAdapter)
        {
            _unitOfWorkManager = unitOfWorkManager;
            _reservationItemReadRepository = reservationItemReadRepository;
            _reservationItemDomainService = reservationItemDomainService;
            _repository = repository;
            _applicationUser = applicationUser;
            _reservationItemStatusDomainService = reservationItemStatusDomainService;
            _roomReadRepository = roomReadRepository;
            _housekeepingStatusAppService = housekeepingStatusAppService;
            _reservationBudgetAppService = reservationBudgetAppService;
            _reservationConfirmationAppService = reservationConfirmationAppService;
            _guestReservationItemReadRepository = guestReservationItemReadRepository;
            _guestReservationItemRepository = guestReservationItemRepository;
            _propertyParameterReadRepository = propertyParameterReadRepository;
            _propertyReadRepository = propertyReadRepository;
            _configuration = configuration;
            _billingAccountReadRepository = billingAccountReadRepository;
            _localizationManager = localizationManager;
            _locationReadRepository = locationReadRepository;
            _roomRepository = roomRepository;
            _housekeepingStatusPropertyAppService = housekeepingStatusPropertyAppService;
            _housekeepingStatusPropertyReadRepository = housekeepingStatusPropertyReadRepository;
            _housekeepingStatusPropertyRepository = housekeepingStatusPropertyRepository;
            _billingAccountRepository = billingAccountRepository;
            _guestItemRepository = guestItemRepository;
            _currencyReadRepository = currencyReadRepository;
            _RoomOccupiedDomainService = RoomOccupiedDomainService;
            _roomOccupiedReadRepository = roomOccupiedReadRepository;
            _reservationBudgetRepository = reservationBudgetRepository;
            _reservationItemRepository = reservationItemRepository;
            SetObservables(serviceProvider);
        }

        private void SetObservables(IServiceProvider serviceProvider)
        {
            _reservationChangeItemStatusObservableList = new List<IReservationItemChangeStatusObservable>();
            var serviceForReservationItemChangeStatusObservable = serviceProvider.GetServices<IReservationItemChangeStatusObservable>().First(o => o.GetType() == typeof(RoomTypeInventoryDomainService));
            _reservationChangeItemStatusObservableList.Add(serviceForReservationItemChangeStatusObservable);

            _reservationItemChangeRoomTypeOrPeriodObservableList = new List<IReservationItemChangeRoomTypeOrPeriodObservable>();
            var serviceForReservationItemChangeRoomTypeOrPeriodObservable = serviceProvider.GetServices<IReservationItemChangeRoomTypeOrPeriodObservable>().First(o => o.GetType() == typeof(RoomTypeInventoryDomainService));
            _reservationItemChangeRoomTypeOrPeriodObservableList.Add(serviceForReservationItemChangeRoomTypeOrPeriodObservable);
        }

        public IListDto<ReservationItemDto> GetAllByPropertyAndReservation(RequestAllDto request, long reservationId)
        {
            if (reservationId <= 0)
            {
                NotifyIdIsMissing();
                return null;
            }

            return _reservationItemReadRepository.GetAllByPropertyAndReservation(request, reservationId);
        }

        public IListDto<AccommodationsByReservationIdResultDto> GetAllAccommodations(RequestAllDto request, long propertyId, long reservationId)
        {
            if (propertyId <= 0)
            {
                NotifyIdIsMissing();
                return null;
            }

            if (reservationId <= 0)
            {
                NotifyIdIsMissing();
                return null;
            }

            return _reservationItemReadRepository.GetAllAccommodations(request, propertyId, reservationId);
        }

        public void CancelReservationItem(long reservationItemId, ReservationItemCancelDto dto)
        {
            using (var uow = _unitOfWorkManager.Begin())
            {
                var reservationItem = _repository.Get(new DefaultLongRequestDto(reservationItemId));

                var billingAccountValidation = _billingAccountReadRepository.AnyOpenAccountAndHaveBillingAccountItemByReservationItemId(reservationItemId);

                if (billingAccountValidation.Item2)
                {
                    Notification.Raise(Notification.DefaultBuilder
                     .WithMessage(AppConsts.LocalizationSourceName, ReservationItem.EntityError.ReservationItemHaveAnyOpenedBillingAccountWithBillingAccountItem)
                     .WithDetailedMessage(AppConsts.LocalizationSourceName, ReservationItem.EntityError.ReservationItemHaveAnyOpenedBillingAccountWithBillingAccountItem)
                     .Build());
                    return;
                }
                else
                {
                    if (billingAccountValidation.Item1)
                    {
                        var billingAccountList = _billingAccountReadRepository.GetAllOpenedByReservationItemId(reservationItemId);
                        billingAccountList.ForEach(billingAccount => _billingAccountRepository.CloseBillingAccount(billingAccount.Id));
                    }

                    this._reservationItemDomainService.CancelReservationItem(reservationItem, dto.ReasonId, dto.CancellationDescription);

                    RiseActionsAfterReservationItemChangeStatus(reservationItem, (ReservationStatus)reservationItem.ReservationItemStatusId);
                }

                uow.Complete();
            }
        }

        public void ReactivateReservationItem(long reservationItemId)
        {
            using (var uow = _unitOfWorkManager.Begin())
            {
                var request = new DefaultLongRequestDto();
                request.Id = reservationItemId;
                request.Expand = "Reservation.ReservationConfirmationList";

                var reservationItem = ReservationItemDomainService.Get(request);

                var newStatus = _reservationItemDomainService.GetStatusByReservationConfirmation(reservationItem.Reservation.ReservationConfirmationList.FirstOrDefault());

                if (reservationItem.ReservationItemStatusId != (int)ReservationStatus.Canceled ||
                    !_reservationItemStatusDomainService.CanChangeStatus(reservationItem, newStatus))
                {
                    Notification.Raise(Notification.DefaultBuilder
                            .WithMessage(AppConsts.LocalizationSourceName, ReservationItemEnum.Error.CouldNotChangeStatus)
                            .WithDetailedMessage(AppConsts.LocalizationSourceName, ReservationItemEnum.Error.CouldNotChangeStatus)
                            .Build());
                    return;
                }

                if (reservationItem.RoomId.HasValue)
                    _reservationItemDomainService.CleanRoomIdIfIsNotAvailable(reservationItem);

                var oldReservationItem = _reservationItemStatusDomainService.GetNewInstanceWithStatusId(reservationItem);
                var builder = new ReservationItem.Builder(Notification, reservationItem, _roomReadRepository, oldReservationItem, reservationItem.Reservation.PropertyId);

                builder.WithReservationItemStatus(newStatus);

                _reservationItemDomainService.Update(builder);

                if (newStatus == ReservationStatus.ToConfirm)
                    _guestReservationItemRepository.UpdateAllGuestToConfirm(request.Id);
                else if (newStatus == ReservationStatus.Confirmed)
                    _guestReservationItemRepository.UpdateAllGuestToConfirmed(request.Id);

                RiseActionsAfterReservationItemChangeStatus(reservationItem, (ReservationStatus)reservationItem.ReservationItemStatusId);

                uow.Complete();
            }
        }

        public void RealeseReservationItem(long reservationItemId)
        {
            using (var uow = _unitOfWorkManager.Begin())
            {
                var request = new DefaultLongRequestDto();
                request.Id = reservationItemId;
                request.Expand = "Reservation";

                var reservationItem = ReservationItemDomainService.Get(request);
                var reservationItemStatusIdOld = reservationItem.ReservationItemStatusId;

                var newStatus = ReservationStatus.Pending;

                if (reservationItem.ReservationItemStatusId == (int)ReservationStatus.Pending ||
                    !_reservationItemStatusDomainService.CanChangeStatus(reservationItem, newStatus))
                {
                    Notification.Raise(Notification.DefaultBuilder
                            .WithMessage(AppConsts.LocalizationSourceName, ReservationItemEnum.Error.CouldNotChangeStatus)
                            .WithDetailedMessage(AppConsts.LocalizationSourceName, ReservationItemEnum.Error.CouldNotChangeStatus)
                            .Build());
                    return;
                }
                _reservationItemDomainService.UpdateStatusReservationItem(reservationItemId, newStatus);

                var roomOfReservationItem = _roomRepository.GetByReservationItemId(reservationItemId);

                if (roomOfReservationItem != null)
                {
                    _housekeepingStatusPropertyAppService.UpdateRoomForHousekeeppingStatus(roomOfReservationItem.Id, _housekeepingStatusPropertyAppService.GetHousekeepingStatusIdDirty());
                }

                if (Notification.HasNotification()) return;

                _guestReservationItemRepository.UpdateAllGuestsToPending(reservationItemId);

                if (Notification.HasNotification()) return;

                ReleaseUH(reservationItem);

                var systemDate = _propertyParameterReadRepository.GetSystemDate(int.Parse(_applicationUser.PropertyId)).Value;

                var isEarlyDayUse = _reservationItemReadRepository.IsEarlyDayUse(systemDate, reservationItemId);

                _reservationBudgetAppService.DeleteRange(systemDate, reservationItemId, isEarlyDayUse);

                if (Notification.HasNotification()) return;

                RiseActionsAfterReservationItemChangeStatus(reservationItem, (ReservationStatus)reservationItem.ReservationItemStatusId, (ReservationStatus)reservationItemStatusIdOld);

                if (reservationItem.RoomId.HasValue)
                    _RoomOccupiedDomainService.DeleteWithRoomId(reservationItem.RoomId.Value);

                uow.Complete();
            }
        }

        private void ReleaseUH(ReservationItem reservationItem)
        {
            var HousekeepingId = _housekeepingStatusAppService.GetHousekeepingStatusIdDirty();
            _housekeepingStatusAppService.UpdateRoomForHousekeeppingStatus(reservationItem.RoomId.Value, HousekeepingId);
        }

        public IListDto<GuestGuestRegistrationResultDto> GetAllGuestGuestRegistration(long propertyId, long reservationItemId, string language)
        {
            if (propertyId <= 0 || reservationItemId <= 0)
            {
                NotifyIdIsMissing();
                return null;
            }

            return _reservationItemReadRepository.GetAllGuestGuestRegistration(propertyId, reservationItemId, language);
        }

        public void AssociateRoom(int propertyId, long reservationItemId, int roomTypeId, int? roomId = null)
        {
            using (var uow = _unitOfWorkManager.Begin())
            {
                if (propertyId <= 0 || reservationItemId <= 0 || roomTypeId <= 0 || (roomId.HasValue && roomId.Value == 0))
                {
                    NotifyIdIsMissing();
                    return;
                }

                var request = new DefaultLongRequestDto(reservationItemId);

                var reservationItem = ReservationItemDomainService.Get(request);
                var oldRoomTypeId = reservationItem.ReceivedRoomTypeId;

                if (reservationItem.RoomId.HasValue && reservationItem.RoomId.Value == roomId)
                    return;

                if (!_reservationItemStatusDomainService.CanChangeReservationItem(reservationItem.ReservationItemStatusId))
                {
                    Notification.Raise(Notification.DefaultBuilder.WithMessage(AppConsts.LocalizationSourceName, ReservationItemEnum.Error.ReservationItemCanNotBeChanged).Build());
                    return;
                }

                var oldReservationItem = _reservationItemStatusDomainService.GetNewInstanceWithStatusId(reservationItem);

                var builder = new ReservationItem.Builder(Notification, reservationItem, _roomReadRepository, oldReservationItem, propertyId);

                builder.WithRoomId(roomId);
                builder.WithReceivedRoomTypeId(roomTypeId);

                _reservationItemDomainService.Update(builder);

                if (Notification.HasNotification())
                {
                    _reservationItemReadRepository.Detach();
                    return;
                }

                if (ChangeRoomType(oldRoomTypeId, roomTypeId))
                    RiseActionsAfterReservationItemChangeRoomTypeInAssociateRoom(reservationItem, oldRoomTypeId);

                uow.Complete();
            }
        }

        private bool ChangeRoomType(int roomTypeIdOld, int roomTypeIdNew)
        {
            return roomTypeIdOld != roomTypeIdNew;
        }

        public void UpdateToCheckInReservationItem(long reservationItemId, int propertyId, DateTime checkInDate)
        {
            var request = new DefaultLongRequestDto();
            request.Id = reservationItemId;

            var reservationItem = ReservationItemDomainService.Get(request);

            //var reservationItem = _repository.Get(reservationItemId);

            var oldReservationItem = _reservationItemStatusDomainService.GetNewInstanceWithStatusIdAndRoomId(reservationItem);

            var builder = new ReservationItem.Builder(Notification, reservationItem, _roomReadRepository, oldReservationItem, propertyId);

            builder.WithReservationItemStatus(ReservationStatus.Checkin);

            builder.WithCheckInDate(checkInDate);

            builder.WithCheckInHour(DateTime.UtcNow.ToZonedDateTimeLoggedUser());

            var reservationItemDomain = builder.Build();

            if (Notification.HasNotification())
            {
                _reservationItemReadRepository.Detach();
                return;
            }

            ReservationItemDomainService.Update(builder);

            var systemDate = checkInDate;

            if (reservationItemDomain.EstimatedArrivalDate.Date > reservationItemDomain.CheckInDate.Value.Date)
            {
                var lastReservationBudget = _reservationBudgetRepository.GetLastReservationBudget(reservationItem);
                var reservationBudgetList = new List<ReservationBudget>();
                TimeSpan earlyDays = reservationItemDomain.EstimatedArrivalDate.Date - reservationItemDomain.CheckInDate.Value.Date;
                for (var i = 0; i < earlyDays.Days; i++)
                {
                    reservationBudgetList.Add(
                        _reservationItemDomainService.CreateReservationBudgetToReservationItemListBySystemDate(reservationItemDomain, lastReservationBudget, checkInDate,
                                                            GetReservationItemCommercialBudgetDto(reservationItemDomain, propertyId)));
                    
                    checkInDate = checkInDate.AddDays(1);
                }

                _reservationBudgetRepository.AddReservationBudgetRange(reservationBudgetList);
            }

            if (reservationItem.EstimatedArrivalDate.Date != systemDate.Date)
                RiseActionsAfterReservationItemChangePeriodInUpdateToCheckin(reservationItem, systemDate);
        }

        private ReservationItemCommercialBudgetDto GetReservationItemCommercialBudgetDto(ReservationItem reservationItem, int propertyId)
        {
            var reservationItemBudgetRequest = new ReservationItemBudgetRequestDto
            {
                ReservationItemId = reservationItem.Id,
                RoomTypeId = reservationItem.RequestedRoomTypeId,
                CurrencyId = reservationItem.CurrencyId,
                MealPlanTypeId = reservationItem.MealPlanTypeId,
                InitialDate = reservationItem.CheckInDate.Value,
                FinalDate = reservationItem.CheckInDate.Value,
                RatePlanId = reservationItem.RatePlanId
            };

            var reservationItemBudgetHeaderDto = _reservationBudgetAppService.GetWithBudgetOffer(reservationItemBudgetRequest, propertyId).Result;
            ReservationItemCommercialBudgetDto reservationItemCommercialBudgetDto;

            //Só vai ter CommercialBudgetList de um dia(que está sendo iterado)
            if (reservationItem.RatePlanId == null)
                reservationItemCommercialBudgetDto = reservationItemBudgetHeaderDto.CommercialBudgetList.Where(r => r.MealPlanTypeId == reservationItem.MealPlanTypeId && r.RatePlanId == null).FirstOrDefault();
            else
                reservationItemCommercialBudgetDto = reservationItemBudgetHeaderDto.CommercialBudgetList.Where(r => r.MealPlanTypeId == reservationItem.MealPlanTypeId && r.RatePlanId == reservationItem.RatePlanId).FirstOrDefault();

            return reservationItemCommercialBudgetDto;
        }

        public void ChangePeriodAndRoomTypeAndRoom(int propertyId, long reservationItemId, ChangePeriodReservationItemDto dto)
        {
            ValidateChangePeriodReservationItemDto(propertyId, reservationItemId, dto);

            if (Notification.HasNotification())
                return;

            var request = new DefaultLongRequestDto();
            request.Id = reservationItemId;

            var reservationItem = ReservationItemDomainService.Get(request);

            var oldReservationItem = _reservationItemStatusDomainService.GetNewInstanceWithStatusAndAndRoomAndRoomTypeIdAndEstimatedDates(reservationItem);

            if (!_reservationItemStatusDomainService.CanChangeReservationItem(reservationItem.ReservationItemStatusId))
            {
                Notification.Raise(Notification.DefaultBuilder.WithMessage(AppConsts.LocalizationSourceName, ReservationItemEnum.Error.ReservationItemCanNotBeChanged).Build());
                return;
            }

            using (var uow = _unitOfWorkManager.Begin())
            {
                var builder = new ReservationItem.Builder(Notification, reservationItem, _roomReadRepository, oldReservationItem, propertyId);

                if (oldReservationItem.ReservationItemStatusId == (int)ReservationStatus.Checkin)
                {
                    if ((reservationItem.CheckInDate ?? reservationItem.EstimatedArrivalDate).Date != Convert.ToDateTime(dto.InitialDate).Date ||
                        (reservationItem.CheckOutDate ?? reservationItem.EstimatedDepartureDate).Date != Convert.ToDateTime(dto.FinalDate).Date)
                    {
                        Notification.Raise(Notification.DefaultBuilder.WithMessage(AppConsts.LocalizationSourceName, ReservationItemEnum.Error.ReservationItemWithStatusCheckinCanNotBeChanged).Build());
                        return;
                    }

                    if (!dto.RoomId.HasValue)
                    {
                        Notification.Raise(Notification.DefaultBuilder.WithMessage(AppConsts.LocalizationSourceName, ReservationItemEnum.Error.ReservationItemRoomCanNotBeChanged).Build());
                        return;
                    }

                    builder.WithEstimatedArrivalDate(Convert.ToDateTime(dto.InitialDate));
                    builder.WithEstimatedDepartureDate(Convert.ToDateTime(dto.FinalDate));
                    builder.WithCheckInDate(reservationItem.CheckInDate);
                    builder.WithCheckOutDate(null);
                    builder.WithRoomId(dto.RoomId);
                    builder.WithReceivedRoomTypeId(dto.RoomTypeId);
                }
                else if (oldReservationItem.ReservationItemStatusId == (int)ReservationStatus.ToConfirm ||
                    oldReservationItem.ReservationItemStatusId == (int)ReservationStatus.Confirmed)
                {
                    builder.WithEstimatedArrivalDate(Convert.ToDateTime(dto.InitialDate));
                    builder.WithEstimatedDepartureDate(Convert.ToDateTime(dto.FinalDate));
                    builder.WithCheckInDate(null);
                    builder.WithCheckOutDate(null);
                    builder.WithRoomId(dto.RoomId);
                    builder.WithReceivedRoomTypeId(dto.RoomTypeId);

                    RiseActionsAfterReservationItemChangeRoomTypeOrPeriodInAvailabilityGrid(oldReservationItem, dto);
                }

                _reservationItemDomainService.Update(builder);

                if (Notification.HasNotification())
                {
                    _reservationItemReadRepository.Detach();
                    return;
                }

                uow.Complete();
            }
        }

        private void ValidateChangePeriodReservationItemDto(int propertyId, long reservationItemId, ChangePeriodReservationItemDto requestDto)
        {
            if (propertyId <= 0 || reservationItemId <= 0)
                NotifyIdIsMissing();

            var nowDateProperty = DateTime.UtcNow.ToZonedDateTimeLoggedUser().Date;

            DateTime dateTimeInitial;
            if (!DateTime.TryParse(requestDto.InitialDate, out dateTimeInitial) && dateTimeInitial.Date < nowDateProperty)
                Notification.Raise(Notification.DefaultBuilder.WithMessage(AppConsts.LocalizationSourceName, CommonsEnum.Error.ParameterInvalid).Build());

            DateTime dateTimeFinal;
            if (!DateTime.TryParse(requestDto.FinalDate, out dateTimeFinal) && dateTimeFinal.Date < nowDateProperty)
                Notification.Raise(Notification.DefaultBuilder.WithMessage(AppConsts.LocalizationSourceName, CommonsEnum.Error.ParameterInvalid).Build());
        }


        public void PatchReservationItemBudget(int propertyId, long reservationItemId, ReservationItemWithBudgetDto dto)
        {
            using (var uow = _unitOfWorkManager.Begin())
            {
                var request = new DefaultLongRequestDto();
                request.Id = reservationItemId;

                var propertyIdLoggedUser = _applicationUser.PropertyId.TryParseToInt32();
                var reservationItem = ReservationItemDomainService.Get(request);
                var reservationItemParent = _reservationItemReadRepository.GetReservationItemParent(reservationItem.ReservationId, reservationItem.Id);

                ValidateReservationBudget(dto.ReservationBudgetDtoList, reservationItemParent?.CurrencyId ?? null);

                if (Notification.HasNotification()) return;

                _reservationBudgetAppService.ValidateReservationBudgetDays(dto.ReservationBudgetDtoList.Count, reservationItem.CheckInDate, reservationItem.CheckOutDate, reservationItem.EstimatedArrivalDate, reservationItem.EstimatedDepartureDate, reservationItem.ReservationItemStatusId);

                if (Notification.HasNotification())
                    return;

                var oldReservationItem = _reservationItemStatusDomainService.GetNewInstanceWithStatusIdAndRoomId(reservationItem);
                var reservationBudgetList = _reservationBudgetAppService.GetAllByReservationItemId(reservationItem.Id);
                var paramEditMigratedReservation = _propertyParameterReadRepository.GetPropertyParameterForPropertyIdAndApplicationParameterId(int.Parse(_applicationUser.PropertyId), (int)ApplicationParameterEnum.ParameterAllowEditMigratedReservation);

                if (reservationItem.IsMigrated && (paramEditMigratedReservation != null && !bool.Parse(paramEditMigratedReservation.PropertyParameterValue)))
                    _reservationBudgetAppService.ValidateMigratedReservationBudget(reservationBudgetList, dto.ReservationBudgetDtoList);

                if (Notification.HasNotification()) return;

                var builder = new ReservationItem.Builder(Notification, reservationItem, _roomReadRepository, oldReservationItem, propertyIdLoggedUser);

                builder.WithMealPlanTypeId(dto.MealPlanTypeId);
                builder.WithCurrencyId(dto.CurrencyId);
                builder.WithRatePlanId(dto.RatePlanId);
                builder.WithGratuityTypeId(dto.GratuityTypeId);

                var reservationItemDomain = builder.Build();

                _reservationBudgetAppService.UpdateReservationBudgetList(propertyIdLoggedUser, reservationItemDomain, dto);

                ReservationItemDomainService.Update(builder);

                if (Notification.HasNotification())
                {
                    _reservationItemReadRepository.Detach();
                    return;
                }

                uow.Complete();
            }


        }

        public List<GuestReservationItem> GetGuestReservationItemByReservationItemId(long reservationItemId)
        {
            return _reservationItemReadRepository.GetGuestReservationItemByReservationItemId(reservationItemId);
        }

        public void AccommodationChange(AccommodationChangeDto reservationItemChange)
        {
            using (var uow = _unitOfWorkManager.Begin())
            {
                var request = new DefaultLongRequestDto();
                request.Id = reservationItemChange.ReservationItemId;

                var reservationItem = ReservationItemDomainService.Get(request);
                var oldRoomTypeId = reservationItem.ReceivedRoomTypeId;

                if (Notification.HasNotification())
                    return;

                var oldReservationItem = _reservationItemStatusDomainService.GetNewInstanceWithStatusIdAndRoomId(reservationItem);

                //Trocar status da UH para sujo
                var housekeepingStatusPropertyIdDirty = _housekeepingStatusPropertyReadRepository.GetHousekeepingStatusIdDirty();
                var oldRoomId = oldReservationItem.RoomId;
                _housekeepingStatusPropertyRepository.UpdateRoomForHousekeeppingStatus(oldRoomId.GetValueOrDefault(), housekeepingStatusPropertyIdDirty);

                //Atualizar quarto da acomodação
                var builder = new ReservationItem.Builder(Notification, reservationItem, _roomReadRepository, oldReservationItem, reservationItemChange.PropertyId);

                builder.WithReasonId(reservationItemChange.ReasonId);
                builder.WithReceivedRoomTypeId(reservationItemChange.RoomTypeId);
                builder.WithRoomId(reservationItemChange.RoomId);

                var reservationItemDomain = builder.Build();

                ReservationItemDomainService.Update(builder);

                if (Notification.HasNotification())
                {
                    _reservationItemReadRepository.Detach();
                    return;
                }

                if (oldRoomId.HasValue)
                    _RoomOccupiedDomainService.DeleteWithRoomId(oldRoomId.Value);

                if (reservationItem.RoomId.HasValue)
                    if (!_roomOccupiedReadRepository.RoomIdUsed(reservationItem.RoomId.Value, reservationItem.ReservationId))
                    {
                        try
                        {
                            _RoomOccupiedDomainService.Insert(RoomOccupiedAdapter.Map(reservationItem.RoomId.Value, reservationItem.ReservationId).Build());
                        }
                        catch (Exception ex)
                        {
                            Notification.Raise(Notification.DefaultBuilder.WithMessage(AppConsts.LocalizationSourceName, ReservationItemEnum.Error.RoomIsNotAvaiable)
                                                                          .WithDetailedMessage(ex.Message)
                                                                          .Build());
                            return;
                        }
                    }

                RiseActionsAfterReservationItemChangeRoomTypeInAccommodationChange(reservationItem, oldRoomTypeId);

                uow.Complete();
            }
        }

        public void Checkout(int propertyId, long reservationItemId)
        {
            using (var uow = _unitOfWorkManager.Begin())
            {
                var request = new DefaultLongRequestDto();
                request.Id = reservationItemId;
                request.Expand = "Reservation";

                var reservationItem = ReservationItemDomainService.Get(request);
                var reservationItemStatusIdOld = reservationItem.ReservationItemStatusId;

                var newStatus = ReservationStatus.Checkout;

                if (!_reservationItemStatusDomainService.CanChangeStatus(reservationItem, newStatus))
                {
                    Notification.Raise(Notification.DefaultBuilder
                            .WithMessage(AppConsts.LocalizationSourceName, ReservationItemEnum.Error.CouldNotChangeStatus)
                            .WithDetailedMessage(AppConsts.LocalizationSourceName, ReservationItemEnum.Error.CouldNotChangeStatus)
                            .Build());
                    return;
                }

                if (_billingAccountReadRepository.AnyOpenAccountByReservationItemId(reservationItemId))
                {
                    Notification.Raise(Notification.DefaultBuilder
                            .WithMessage(AppConsts.LocalizationSourceName, ReservationItemEnum.Error.InvalidCheckoutReservationItemHasOpenBillingAccount)
                            .WithDetailedMessage(AppConsts.LocalizationSourceName, ReservationItemEnum.Error.InvalidCheckoutReservationItemHasOpenBillingAccount)
                            .Build());
                    return;
                }

                var roomOfReservationItem = _roomRepository.GetByReservationItemId(reservationItemId);

                if (roomOfReservationItem != null)
                {
                    _housekeepingStatusPropertyAppService.UpdateRoomForHousekeeppingStatus(roomOfReservationItem.Id, _housekeepingStatusPropertyAppService.GetHousekeepingStatusIdDirty());
                }

                var oldReservationItem = _reservationItemStatusDomainService.GetNewInstanceWithStatusIdAndRoomId(reservationItem);
                var builder = new ReservationItem.Builder(Notification, reservationItem, _roomReadRepository, oldReservationItem, reservationItem.Reservation.PropertyId);

                builder.WithReservationItemStatus(newStatus);

                var systemDate = _propertyParameterReadRepository.GetSystemDate(reservationItem.Reservation.PropertyId).Value;

                var checkOutDate = GetCheckoutDate(systemDate);

                builder.WithCheckOutProcess(checkOutDate);

                _reservationItemDomainService.Update(builder);

                if (Notification.HasNotification()) return;

                _guestReservationItemRepository.UpdateAllGuestsToCheckout(reservationItemId, checkOutDate);

                if (Notification.HasNotification()) return;

                ReleaseUH(reservationItem);

                if (Notification.HasNotification()) return;

                var isEarlyDayUse = _reservationItemReadRepository.IsEarlyDayUse(checkOutDate, reservationItemId);

                _reservationBudgetAppService.DeleteRange(checkOutDate, reservationItemId, isEarlyDayUse);

                if (Notification.HasNotification()) return;

                RiseActionsAfterReservationItemChangeStatus(reservationItem, (ReservationStatus)reservationItem.ReservationItemStatusId, (ReservationStatus)reservationItemStatusIdOld);


                if (reservationItem.RoomId.HasValue)
                    _RoomOccupiedDomainService.DeleteWithRoomId(reservationItem.RoomId.Value);

                uow.Complete();
            }
        }

        private DateTime GetCheckoutDate(DateTime checkoutDate)
        {
            var currentDate = DateTime.UtcNow.ToZonedDateTimeLoggedUser();

            var newCheckoutDate = new DateTime(
                                checkoutDate.Year,
                                checkoutDate.Month,
                                checkoutDate.Day,
                                currentDate.Hour,
                                currentDate.Minute,
                                currentDate.Second);
            return newCheckoutDate;
        }

        public EarlyCheckoutResponseDto IsEarlyCheckout(int propertyId, long reservationItemId)
        {
            var reservationItem = _reservationItemReadRepository.GetByIdNoTracking(reservationItemId);

            if (reservationItem == null)
            {
                NotifyWhenEntityNotExist("ReservationItem");
                return null;
            }

            return new EarlyCheckoutResponseDto
            {
                IsEarlyCheckout = DateTime.UtcNow.ToZonedDateTimeLoggedUser() < (reservationItem.CheckOutDate ?? reservationItem.EstimatedDepartureDate).Date
            };
        }

        public TemplateReservationSlipDto GenerateHtmlForSlipReservationAccommodation(int propertyId, long reservationId)
        {
            var culture = CultureInfo.CurrentCulture.Name;
            string path = Path.Combine(Directory.GetCurrentDirectory(), "Resources", "TemplateSlipReservation.html");
            if (File.Exists(path))
            {
                string slipReservationText = File.ReadAllText(path);

                //LABELS DO E-MAIL
                slipReservationText = slipReservationText.Replace("[ConfirmationReservationLabel]", _localizationManager.GetString(AppConsts.LocalizationSourceName, EmailEnum.SlipReservationConfirmationReservationLabel.ToString()));
                slipReservationText = slipReservationText.Replace("[ReservationConfirmedSuccessfullyLabel]", _localizationManager.GetString(AppConsts.LocalizationSourceName, EmailEnum.SlipReservationReservationConfirmedSuccessfullyLabel.ToString()));
                slipReservationText = slipReservationText.Replace("[WelcomeLabel]", _localizationManager.GetString(AppConsts.LocalizationSourceName, EmailEnum.SlipReservationWelcomeLabel.ToString()));
                slipReservationText = slipReservationText.Replace("[ReservationCodeLabel]", _localizationManager.GetString(AppConsts.LocalizationSourceName, EmailEnum.SlipReservationReservationCodeLabel.ToString()));
                slipReservationText = slipReservationText.Replace("[CompanyNameLabel]", _localizationManager.GetString(AppConsts.LocalizationSourceName, EmailEnum.SlipReservationCompanyNameLabel.ToString()));
                slipReservationText = slipReservationText.Replace("[ContactNameLabel]", _localizationManager.GetString(AppConsts.LocalizationSourceName, EmailEnum.SlipReservationContactNameLabel.ToString()));
                slipReservationText = slipReservationText.Replace("[ContactPhoneLabel]", _localizationManager.GetString(AppConsts.LocalizationSourceName, EmailEnum.SlipReservationContactPhoneLabel.ToString()));
                slipReservationText = slipReservationText.Replace("[ContactEmailLabel]", _localizationManager.GetString(AppConsts.LocalizationSourceName, EmailEnum.SlipReservationContactEmailLabel.ToString()));
                slipReservationText = slipReservationText.Replace("[StayInformationLabel]", _localizationManager.GetString(AppConsts.LocalizationSourceName, EmailEnum.SlipReservationStayInformationLabel.ToString()));
                slipReservationText = slipReservationText.Replace("[AccommodationLabel]", _localizationManager.GetString(AppConsts.LocalizationSourceName, EmailEnum.SlipReservationAccommodationLabel.ToString()));
                slipReservationText = slipReservationText.Replace("[ReservationPaymentLabel]", _localizationManager.GetString(AppConsts.LocalizationSourceName, EmailEnum.SlipReservationReservationPaymentLabel.ToString()));
                slipReservationText = slipReservationText.Replace("[DescriptionLabel]", _localizationManager.GetString(AppConsts.LocalizationSourceName, EmailEnum.SlipReservationDescriptionLabel.ToString()));
                slipReservationText = slipReservationText.Replace("[ValueLabel]", _localizationManager.GetString(AppConsts.LocalizationSourceName, EmailEnum.SlipReservationValueLabel.ToString()));
                slipReservationText = slipReservationText.Replace("[PriceRoomNightLabel]", _localizationManager.GetString(AppConsts.LocalizationSourceName, EmailEnum.SlipReservationPriceRoomNightLabel.ToString()));
                slipReservationText = slipReservationText.Replace("[HotelPoliciesLabel]", _localizationManager.GetString(AppConsts.LocalizationSourceName, EmailEnum.SlipReservationHotelPoliciesLabel.ToString()));
                slipReservationText = slipReservationText.Replace("[PaymentTypeLabel]", _localizationManager.GetString(AppConsts.LocalizationSourceName, EmailEnum.EmailBodyPaymentType.ToString()));
               

                var slipInformations = _reservationItemReadRepository.GetSlipReservation(reservationId, propertyId);
                slipReservationText = slipReservationText.Replace("[countAccommodations]", slipInformations.ReservationItemList.Count().ToString());
                if (slipInformations != null)
                {
                    slipReservationText = slipReservationText.Replace("[reservationCode]", slipInformations.ReservationCode);
                    slipReservationText = slipReservationText.Replace("[companyName]", !string.IsNullOrWhiteSpace(slipInformations.CompanyClientName) ? slipInformations.CompanyClientName : "-");
                    slipReservationText = slipReservationText.Replace("[companyNameTitle]", slipInformations.CompanyClientName);
                    slipReservationText = slipReservationText.Replace("[contactName]", !string.IsNullOrWhiteSpace(slipInformations.ContactName) ? slipInformations.ContactName : "-");
                    slipReservationText = slipReservationText.Replace("[contactPhone]", !string.IsNullOrWhiteSpace(slipInformations.ContactPhone) ? slipInformations.ContactPhone : "-");
                    slipReservationText = slipReservationText.Replace("[contactEmail]", !string.IsNullOrWhiteSpace(slipInformations.ContactEmail) ? slipInformations.ContactEmail : "-");
                    slipReservationText = slipReservationText.Replace("[observationValue]", slipInformations.ExternalComments);

                    var firstItem = slipInformations.ReservationItemList.FirstOrDefault();
                    var checkin = firstItem != null ? firstItem.CheckInDate.Value : DateTime.MinValue;
                    var checkout = firstItem != null ? firstItem.CheckOutDate.Value : DateTime.MinValue;

                    var observationSb = new StringBuilder();
                    if (!string.IsNullOrWhiteSpace(slipInformations.ExternalComments))
                    {
                        observationSb = observationSb.Append($@"
                             <tr>
                                <td colspan=""12"" height=""60"" align=""left""
                                    style =""font-size: 20px; color: #0C9ABE;font-family: 'Nunito Sans', Helvetica, Arial;""
                                    valign=""bottom"">
                                    {_localizationManager.GetString(AppConsts.LocalizationSourceName, "ObservationLabel" )}
                                    <hr style = ""border: 1px solid #CCD1D2;margin:0;padding:0;"" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan=""12"" align= ""left""
                                    style=""font-size: 14px; color: #2D3434;font-family: 'Nunito Sans', Helvetica, Arial;"">
                                    {slipInformations.ExternalComments}
                                </td>
                            </tr>
                        ");
                    }
                    slipReservationText = slipReservationText.Replace("[Observations]", observationSb.ToString());

                    slipReservationText = slipReservationText.Replace("[checkin]", checkin.ToShortDateString());
                    slipReservationText = slipReservationText.Replace("[checkout]", checkout.ToShortDateString());

                    var paymentType = slipInformations.ReservationConfirmation?.PaymentType;

                    slipReservationText = slipReservationText.Replace("[PaymentTypeValue]", !string.IsNullOrWhiteSpace(paymentType) ? paymentType : "-");

                    if (slipInformations.ReservationItemList != null && slipInformations.ReservationItemList.Count() > 0)
                    {
                        var sb = new StringBuilder();
                        var countNight = DateTimeExtensions.NumberOfNights(checkin, checkout);
                        slipReservationText = slipReservationText.Replace("[NightsLabel]", _localizationManager.GetString(AppConsts.LocalizationSourceName, countNight > 1 ? EmailEnum.SlipReservationNightsLabel.ToString() : EmailEnum.SlipReservationNightLabel.ToString()));
                        slipReservationText = slipReservationText.Replace("[countNigth]", countNight.ToString());
                        foreach (var reservationItem in slipInformations.ReservationItemList)
                        {
                            //Detalhes da Acomodação
                            sb.AppendFormat(
                                                            @"<tr>" +
                                                                "<td>" +
                                                                "<td colspan = \"12\" align = \"left\" style = \"font-size:16px; color: #2D3434;font-family: 'Nunito Sans', Helvetica, Arial;\">" +
                                                                 reservationItem.ReservationItemCode + "<span><strong>" + "  " + reservationItem.RequestedRoomType.Name + "</strong></span>" +
                                                                "</td>" +
                                                            "</tr>"
                                );

                            if (reservationItem.GuestReservationItemForSlipList != null && reservationItem.GuestReservationItemForSlipList.Count() > 0)
                            {

                                foreach (var guestReservationItem in reservationItem.GuestReservationItemForSlipList.Where(x => x.IsMain))
                                {
                                    sb.AppendFormat(
                                        "<tr>" +
                                                "<td height =\"40\" colspan = \"4\" align = \"left\" " +
                                                "style =\"font-size:14px; color: #2D3434;font-family: 'Nunito Sans', Helvetica, Arial;\"" +
                                                "valign = \"top\">" +
                                                   guestReservationItem.GuestName +
                                        "</td>" +

                                      "<td colspan = \"2\" align = \"left\"" +
                                            "style =\"font - size:16px; color: #2D3434;font-family: 'Nunito Sans', Helvetica, Arial;\"" +
                                        "valign = \"top\">" +
                                        "<img align = \"left\" src = \"https://cdn.totvscmnet-cloud.net/img/thex/0.28.22/person.png\"" +
                                             "height = \"17\" alt = \"person\" border = \"0\">" +
                                            "<span>" + reservationItem.AdultCount.ToString() + "</span>" +
                                        "</td>" +

                                        "<td colspan = \"6\" align = \"left\"" +
                                            "style =\"font - size:16px; color: #2D3434;font-family: 'Nunito Sans', Helvetica, Arial;\"" +
                                        "valign = \"top\">" +
                                        "<img align = \"left\" src = \"https://cdn.totvscmnet-cloud.net/img/thex/0.28.22/child.png\"" +
                                             "height = \"17\" alt = \"person\" border = \"0\">" +
                                            "<span>" + reservationItem.ChildCount.ToString() + "</span>" +
                                        "</td>");

                                }
                            }
                        }
                        slipReservationText = slipReservationText.Replace("[reservationItem]", sb.ToString());

                        var propertyInformations = _propertyReadRepository.GetByPropertyId(propertyId);
                        if (propertyInformations != null)
                        {
                            var rateDaily = slipInformations.ReservationItemList.Average(x => x.RateDaily);
                            var propertyDefaultCurrency = _currencyReadRepository.GetPropertyCurrencyById(propertyId);
                            var rateDailyFormatted = string.Format("{0} {1:N}", !string.IsNullOrWhiteSpace(propertyDefaultCurrency?.Symbol) ? propertyDefaultCurrency.Symbol : "$", rateDaily);

                            slipReservationText = slipReservationText.Replace("[rateDaily]", rateDailyFormatted);

                            slipReservationText = slipReservationText.Replace("[propertyName]", propertyInformations.TradeName);
                            slipReservationText = slipReservationText.Replace("[companyName]", propertyInformations.TradeName);

                            var locations = _locationReadRepository.GetLocationDtoListByPersonId(propertyInformations.PropertyUId, string.IsNullOrWhiteSpace(culture) ? _applicationUser.PreferredCulture : culture);
                            var location = locations != null && locations.Any() ? locations.FirstOrDefault(x => x.OwnerId == propertyInformations.PropertyUId) : null;
                            string completeAddress = "";
                            if (location != null)
                            {
                                var completeAddressBuilder = new StringBuilder();
                                completeAddressBuilder.Append($"{location.StreetName} {location.StreetNumber}, {location.AdditionalAddressDetails}");

                                if (!string.IsNullOrEmpty(location.Neighborhood))
                                    completeAddressBuilder.Append($", {location.Neighborhood}");

                                if (!string.IsNullOrWhiteSpace(location.Division))
                                    completeAddressBuilder.Append($", {location.Division}");

                                if (!string.IsNullOrWhiteSpace(location.Country))
                                    completeAddressBuilder.Append($" - {location.Country}");

                                if (!string.IsNullOrWhiteSpace(location.PostalCode))
                                    completeAddressBuilder.Append($" - CEP: {location.PostalCode}");

                                completeAddress = completeAddressBuilder.ToString();
                            }

                            slipReservationText = slipReservationText.Replace("[propertyAddress]", location != null ? completeAddress : "-");
                            slipReservationText = slipReservationText.Replace("[propertyPhone]", !string.IsNullOrWhiteSpace(propertyInformations.ContactPhone) ? propertyInformations.ContactPhone : "-");
                            slipReservationText = slipReservationText.Replace("[propertyWebSite]", !string.IsNullOrWhiteSpace(propertyInformations.ContactWebSite) ? propertyInformations.ContactWebSite : "-");
                            slipReservationText = slipReservationText.Replace("[googleMaps]", MapsGeocodeConfig.UrlGoogleMaps + (location != null ? completeAddress : "-"));
                        }

                        var policy = new StringBuilder();
                        foreach (var propertyPolicy in slipInformations.PropertyPoliciesList)
                        {
                            var policyType = _localizationManager.GetString(AppConsts.LocalizationSourceName, ((PolicyType)propertyPolicy.PolicyTypeId).ToString());

                            policy.AppendFormat("<tr>" +
                                "<td colspan = \"12\" height = \"10\" align = \"left\" " +
                                    "style = \"font-size: 14px; color: #2D3434;font-family: 'Nunito Sans', Helvetica, Arial;\"" +
                                    "valign = \"top\" >" +
                                    "<strong>" + policyType + ": </strong>" +
                                    "<span style = \"display:block\">" + propertyPolicy.Description + "</span>" +
                                    "</td>" +
                                 "</tr>");

                        }
                        slipReservationText = slipReservationText.Replace("[propertyPolicy]", policy.ToString());

                    }

                }
                var slipReservation = HttpUtility.HtmlEncode(slipReservationText);
                return new TemplateReservationSlipDto() { SlipReservation = slipReservationText };

            }
            return null;
        }

        public void SendMailConfirmationReservation(List<string> emailsList, int propertyId, long reservationId)
        {
            var host = _configuration.GetValue<string>("AwsEmailHost");
            var port = Convert.ToInt32(_configuration.GetValue<string>("AwsEmailPort"));
            var userName = _configuration.GetValue<string>("AwsEmailUserName");
            var passsword = _configuration.GetValue<string>("AwsEmailPassword");

            var subject = _localizationManager.GetString(AppConsts.LocalizationSourceName, "ConfirmationReservation");
            var from = _configuration.GetValue<string>("EmailNoReply");

            var bodyEmail = GenerateHtmlForSlipReservationAccommodation(propertyId, reservationId);

            foreach (var email in emailsList)
            {
                var to = email;
                // Prepare Email Message
                var message = EmailMessageBuilder
                                .Init()
                                .AddSubject(subject)
                                .AddFrom(from)
                                .AddBody(bodyEmail.SlipReservation)
                                .AddTo(to)
                                .Build();

                // Send Email Message
                IAwsEmailSender sender = new AwsEmailSender(new AwsEmailSettings(host, port, userName, passsword));
                sender.Send(message, true);
            }


        }

        private void ValidateReservationBudget(ICollection<ReservationBudgetDto> reservationBudgetList, Guid? currencyId)
        {
            if (currencyId.HasValue && !reservationBudgetList.All(rb => rb.CurrencyId == currencyId))
            {
                Notification.Raise(Notification.DefaultBuilder
                         .WithMessage(AppConsts.LocalizationSourceName, Reservation.EntityError.ReservationMustHaveSameCurrencyForAllReservationItems)
                         .WithDetailedMessage(AppConsts.LocalizationSourceName, Reservation.EntityError.ReservationMustHaveSameCurrencyForAllReservationItems)
                         .Build());

                return;
            }

            if (reservationBudgetList.Select(rb => rb.CurrencyId).Distinct().Count() > 1)
                Notification.Raise(Notification.DefaultBuilder
                         .WithMessage(AppConsts.LocalizationSourceName, ReservationItem.EntityError.ReservationItemMustHaveSameCurrencyForAllReservationBudgets)
                         .WithDetailedMessage(AppConsts.LocalizationSourceName, ReservationItem.EntityError.ReservationItemMustHaveSameCurrencyForAllReservationBudgets)
                         .Build());
        }

        private void RiseActionsAfterReservationItemChangeStatus(ReservationItem reservationItem,
            ReservationStatus statusNew, ReservationStatus? statusOld = null)
        {
            foreach (var observable in _reservationChangeItemStatusObservableList)
            {
                var dtoObservable = new ReservationItemChangeStatusObservableDto
                {
                    RoomTypeId = reservationItem.ReceivedRoomTypeId,
                    ReservationItemStatusIdNew = statusNew,
                    ReservationItemStatusIdOld = statusOld,
                    InitialDate = reservationItem.CheckInDate ?? reservationItem.EstimatedArrivalDate,
                    EndDate = reservationItem.EstimatedDepartureDate
                };

                observable.ExecuteActionAfterChangeReservationItemStatus(dtoObservable);
            }
        }

        private void RiseActionsAfterReservationItemChangeRoomTypeInAssociateRoom(ReservationItem reservationItem, int oldRoomTypeId)
        {
            foreach (var observable in _reservationItemChangeRoomTypeOrPeriodObservableList)
            {
                var dtoObservable = new ReservationItemChangeRoomTypeOrPeriodObservableDto
                {
                    OldRoomTypeId = oldRoomTypeId,
                    NewRoomTypeId = reservationItem.ReceivedRoomTypeId,
                    OldInitialDate = reservationItem.CheckInDate ?? reservationItem.EstimatedArrivalDate,
                    OldEndDate = reservationItem.EstimatedDepartureDate
                };

                observable.ExecuteActionAfterReservationItemChangeRoomTypeOrPeriod(dtoObservable);
            }
        }

        private void RiseActionsAfterReservationItemChangePeriodInUpdateToCheckin(ReservationItem reservationItem, DateTime systemDate)
        {
            foreach (var observable in _reservationItemChangeRoomTypeOrPeriodObservableList)
            {
                var dtoObservable = new ReservationItemChangeRoomTypeOrPeriodObservableDto
                {
                    OldRoomTypeId = reservationItem.ReceivedRoomTypeId,
                    NewRoomTypeId = reservationItem.ReceivedRoomTypeId,
                    OldInitialDate = reservationItem.EstimatedArrivalDate,
                    OldEndDate = reservationItem.EstimatedDepartureDate,
                    NewInitialDate = systemDate,
                    NewEndDate = reservationItem.EstimatedDepartureDate
                };

                observable.ExecuteActionAfterReservationItemChangeRoomTypeOrPeriod(dtoObservable);
            }
        }

        private void RiseActionsAfterReservationItemChangeRoomTypeInAccommodationChange(ReservationItem reservationItem, int oldRoomTypeId)
        {
            foreach (var observable in _reservationItemChangeRoomTypeOrPeriodObservableList)
            {
                var dtoObservable = new ReservationItemChangeRoomTypeOrPeriodObservableDto
                {
                    OldRoomTypeId = oldRoomTypeId,
                    NewRoomTypeId = reservationItem.ReceivedRoomTypeId,
                    OldInitialDate = reservationItem.CheckInDate.Value,
                    OldEndDate = reservationItem.EstimatedDepartureDate
                };

                observable.ExecuteActionAfterReservationItemChangeRoomTypeOrPeriod(dtoObservable);
            }
        }

        private void RiseActionsAfterReservationItemChangeRoomTypeOrPeriodInAvailabilityGrid(ReservationItem existingReservationItem, ChangePeriodReservationItemDto dto)
        {
            foreach (var observable in _reservationItemChangeRoomTypeOrPeriodObservableList)
            {
                var dtoObservable = new ReservationItemChangeRoomTypeOrPeriodObservableDto
                {
                    OldRoomTypeId = existingReservationItem.ReceivedRoomTypeId,
                    OldInitialDate = existingReservationItem.EstimatedArrivalDate,
                    OldEndDate = existingReservationItem.EstimatedDepartureDate,
                    NewRoomTypeId = dto.RoomTypeId,
                    NewInitialDate = Convert.ToDateTime(dto.InitialDate),
                    NewEndDate = Convert.ToDateTime(dto.FinalDate)
                };
                observable.ExecuteActionAfterReservationItemChangeRoomTypeOrPeriod(dtoObservable);
            }
        }

        public void ConfirmReservationItem(long reservationItemId)
        {
            if (reservationItemId <= 0)
                NotifyIdIsMissing();

            if (Notification.HasNotification())
                return;

            _reservationItemDomainService.ConfirmReservationItem(reservationItemId);
        }

        public virtual async Task<ReservationItemDto> UpdateByEntrance(ReservationItemDto dto, bool isChild)
        {
            ValidateDto<ReservationItemDto>(dto, nameof(dto));

            if (Notification.HasNotification())
                return ReservationItemDto.NullInstance;

            var reservationBuilder = ReservationItemAdapter.MapEntrance(dto, isChild);

            await ReservationItemDomainService.UpdateAsync(reservationBuilder);

            return dto;
        }

        public void ReactivateNoShow(long reservationItemId)
        {
            using (var uow = _unitOfWorkManager.Begin())
            {
                var reservationItem = _reservationItemReadRepository.GetById(reservationItemId);
                var systemDate = _propertyParameterReadRepository.GetSystemDate(int.Parse(_applicationUser.PropertyId));
                var newStatus = ReservationStatus.ToConfirm;

                _reservationItemDomainService.ValidateReactivation(reservationItem, systemDate, newStatus);

                if (Notification.HasNotification()) return;

                if (reservationItem.RoomId.HasValue)
                    _reservationItemDomainService.CleanRoomIdIfIsNotAvailable(reservationItem);

                reservationItem = _reservationItemDomainService.ChangeEstimatedArrivalDate(reservationItem, systemDate);

                var oldReservationItem = _reservationItemStatusDomainService.GetNewInstanceWithStatusId(reservationItem);
                var builder = new ReservationItem.Builder(Notification, reservationItem, _roomReadRepository, oldReservationItem, int.Parse(_applicationUser.PropertyId));

                builder.WithReservationItemStatus(newStatus);

                _reservationItemRepository.Update(builder.Build());
               
                _guestReservationItemRepository.UpdateGuestsToConfirmAndEstimatedArrivalDate(reservationItemId, systemDate.Value.AddHours(reservationItem.EstimatedArrivalDate.Hour));                

                _reservationBudgetAppService.RemoveRangeNoShow(systemDate, reservationItemId);

                RiseActionsAfterReservationItemChangeStatus(reservationItem, (ReservationStatus)reservationItem.ReservationItemStatusId);

                uow.Complete();
            }
        }       
    }
}
