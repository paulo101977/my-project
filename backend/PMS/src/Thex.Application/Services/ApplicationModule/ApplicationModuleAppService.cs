﻿using System;
using Thex.Application.Adapters;
using Thex.Domain.Entities;
using Thex.Application.Interfaces;
using Tnf.Domain.Services;
using Tnf.Notifications;

namespace Thex.Application.Services
{
    public class ApplicationModuleAppService : ScaffoldApplicationModuleAppService, IApplicationModuleAppService
    {
        public ApplicationModuleAppService(
            IApplicationModuleAdapter applicationModuleAdapter,
            IDomainService<ApplicationModule> applicationModuleDomainService,
            INotificationHandler notificationHandler)
            : base(applicationModuleAdapter, applicationModuleDomainService, notificationHandler)
        {
        }
    }
}
