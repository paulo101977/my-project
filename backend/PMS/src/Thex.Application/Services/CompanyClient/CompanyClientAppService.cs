﻿using System;
using Thex.Application.Adapters;
using Thex.Domain.Entities;
using Thex.Application.Interfaces;
using Tnf.Domain.Services;
using Thex.Dto.Reservation;
using Thex.Dto;
using Tnf.Dto;

namespace Thex.Application.Services
{
    using Microsoft.AspNetCore.Http;
    using Microsoft.Extensions.Configuration;
    using Newtonsoft.Json;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net.Http;
    using System.Text;
    using System.Threading.Tasks;
    using Thex.Common;
    using Thex.Common.Enumerations;
    using Thex.Domain.Interfaces.Repositories;
    using Thex.Domain.Interfaces.Services;
    using Thex.Domain.Services.Interfaces;
    using Thex.Dto.Person;
    using Thex.Infra.ReadInterfaces;
    using Thex.Kernel;
    using Tnf.Dto;
    using Tnf.Notifications;
    using Tnf.Repositories.Uow;

    public class CompanyClientAppService : ScaffoldCompanyClientAppService, ICompanyClientAppService
    {
        private new readonly ICompanyClientReadRepository _companyClientReadRepository;

        private new readonly ICompanyReadRepository _companyReadRepository;

        private new readonly ICompanyClientRepository _companyClientRepository;

        private readonly IPersonReadRepository _personReadRepository;

        private readonly ILocationReadRepository _locationReadRepository;
        private readonly ICompanyClientChannelReadRepository _companyClientChannelReadRepository;
        private readonly IConfiguration _configuration;
        private readonly IApplicationUser _applicationUser;
        private readonly IPropertyReadRepository _propertyReadRepository;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public CompanyClientAppService(
           IUnitOfWorkManager unitOfWorkManager,
           IPersonRepository personRepository,
           ICompanyClientContactPersonDomainService companyClientContactPersonDomainService,
           ICompanyClientContactPersonRepository companyClientContactPersonRepository,
           IOccupationReadRepository occupationReadRepository,
           ICompanyClientAdapter companyClientAdapter,
           ILocationAppService locationAppService,
           ICountrySubdivisionAppService countrySubdivisionAppService,
           IPersonAdapter personAdapter,
           IDomainService<Person> personDomainService,
           ICompanyClientDomainService companyClientDomainService,
           IContactInformationAppService contactInformationAppService,
           IDomainService<ContactInformation> contactInformationDomainService,
           IDomainService<Occupation> occupationDomainService,
           IDocumentAppService documentAppService,
           IContactInformationRepository contactInformationRepository,
           ICompanyReadRepository companyReadRepository,
           ICompanyClientReadRepository companyClientReadRepository,
           ICompanyClientRepository companyClientRepository,
           IPersonReadRepository personReadRepository,
           IApplicationUser applicationUser,
           ILocationReadRepository locationReadRepository,
           ICompanyClientChannelRepository companyClientChannelRepository,
           ICompanyClientChannelReadRepository companyClientChannelReadRepository,
           ICompanyClientChannelAdapter companyClientChannelAdapter,
            INotificationHandler notificationHandler,
            IConfiguration configuration,
            IPropertyReadRepository propertyReadRepository,
            IHttpContextAccessor httpContextAccessor)
           : base(unitOfWorkManager, companyClientReadRepository, companyClientContactPersonDomainService, companyClientRepository, occupationReadRepository, companyClientDomainService, companyClientContactPersonRepository, personRepository, locationAppService, countrySubdivisionAppService, companyClientAdapter, personAdapter, personDomainService, contactInformationAppService,
                 contactInformationDomainService, occupationDomainService, documentAppService, contactInformationRepository,
               companyReadRepository, companyClientChannelRepository, companyClientChannelReadRepository, companyClientChannelAdapter, applicationUser, notificationHandler)
        {
            _companyClientReadRepository = companyClientReadRepository;
            _companyReadRepository = companyReadRepository;
            _companyClientRepository = companyClientRepository;
            _personReadRepository = personReadRepository;
            _locationReadRepository = locationReadRepository;
            _companyClientChannelReadRepository = companyClientChannelReadRepository;
            _configuration = configuration;
            _applicationUser = applicationUser;
            _propertyReadRepository = propertyReadRepository;
            _httpContextAccessor = httpContextAccessor;
        }

        public IListDto<GetAllCompanyClientResultDto> GetAllByPropertyId(GetAllCompanyClientDto request, int propertyId)
        {
            var nullResponseInstance = new ListDto<GetAllCompanyClientResultDto>();
            if (propertyId <= 0)
            {
                NotifyIdIsMissing();
                return nullResponseInstance;
            }

            int companyId = _companyReadRepository.GetCompanyIdByPropertyId(propertyId);

            return _companyClientReadRepository.GetAllByCompanyId(request, companyId);
        }

        public IListDto<GetAllCompanyClientSearchResultDto> GetAllBasedOnFilter(GetAllCompanyClientSearchDto request, int propertyId)
        {
            var nullResponseInstance = new ListDto<GetAllCompanyClientSearchResultDto>();
            int minimumCharacters = 2;

            if (propertyId <= 0)
            {
                NotifyIdIsMissing();
                return nullResponseInstance;
            }

            if (string.IsNullOrEmpty(request.SearchData))
                NotifyRequired("SearchData");
            else if (request.SearchData.Length < minimumCharacters)
                NotifyMinimumCharacters(minimumCharacters);

            if (Notification.HasNotification())
                return nullResponseInstance;

            int companyId = _companyReadRepository.GetCompanyIdByPropertyId(propertyId);

            if (companyId == 0)
                return nullResponseInstance;

            return _companyClientReadRepository.GetAllBasedOnFilter(request, companyId);
        }

        public IListDto<GetAllCompanyClientContactInformationSearchResultDto> GetAllContactPersonByCompanyClientId(GetAllCompanyClientContactInformationSearchDto request)
        {
            var nullResponseInstance = new ListDto<GetAllCompanyClientContactInformationSearchResultDto>();
            int minimumCharacters = 2;

            var propertyId = request.PropertyId;
            if (propertyId <= 0)
            {
                NotifyIdIsMissing();
                return nullResponseInstance;
            }

            Guid companyGuidId;
            if (!Guid.TryParse(request.CompanyClientId, out companyGuidId))
                return nullResponseInstance;

            if (string.IsNullOrEmpty(request.SearchData))
                NotifyRequired("SearchData");
            else if (request.SearchData.Length < minimumCharacters)
                NotifyMinimumCharacters(minimumCharacters);

            if (Notification.HasNotification())
                return nullResponseInstance;

            int companyId = _companyReadRepository.GetCompanyIdByPropertyId(propertyId);

            if (companyId == 0)
                return nullResponseInstance;

            return _companyClientReadRepository.GetAllContactPersonByCompanyClientId(request, companyId, companyGuidId);
        }

        public void ToggleActivation(string id)
        {
            Guid companyGuidId;
            if (!Guid.TryParse(id, out companyGuidId))
            {
                NotifyIdIsMissing();
                return;
            }

            _companyClientRepository.ToggleAndSaveIsActive(companyGuidId);
        }


        public CompanyClientDto CreateBySparseBillingAccount(CompanyClientDto companyClientDto)
        {
            ValidateDto<CompanyClientDto>(companyClientDto, nameof(companyClientDto));

            if (Notification.HasNotification())
                return CompanyClientDto.NullInstance;

            ValidateCompanyClientByNewSparseAccount(companyClientDto);

            if (Notification.HasNotification())
                return CompanyClientDto.NullInstance;

            if (companyClientDto.PropertyId <= 0)
            {
                NotifyIdIsMissing();
                return CompanyClientDto.NullInstance;
            }

            if (companyClientDto.TradeName.Length > 40)
            {
                var str = companyClientDto.TradeName.Substring(0, 13);
                var result = str.Substring(0, str.LastIndexOf(' '));
                companyClientDto.ShortName = "";
            }
            else
                companyClientDto.ShortName = companyClientDto.TradeName;

            Guid personId;
            int companyId = this._companyReadRepository.GetCompanyIdByPropertyId(companyClientDto.PropertyId);

            if (companyClientDto.PersonId == Guid.Empty)
            {

                var fullName = $"{companyClientDto.TradeName} {companyClientDto.ShortName}"; 

                personId = CreatePersonAndGetIdNotAsync(companyClientDto.TradeName, companyClientDto.ShortName, fullName.Trim(), (PersonTypeEnum)companyClientDto.PersonType);

                SaveLocationList(companyClientDto, personId);

                if (Notification.HasNotification())
                    return CompanyClientDto.NullInstance;

                SaveContactInformationList(GetContactInformationListFromDto(personId, companyClientDto.Email, null, companyClientDto.PhoneNumber, null));

                _documentAppService.SaveDocumentList(companyClientDto.DocumentList, personId);
            }
            else
                personId = companyClientDto.PersonId;

            var companyClientBuilder = _companyClientAdapter.Map(companyClientDto, personId, companyId);

            var newCompanyClientId = _companyClientDomainService.InsertAndSaveChanges(companyClientBuilder).Id;

            companyClientDto.Id = newCompanyClientId.ToString();

            return companyClientDto;
        }

        public IListDto<SearchPersonsResultDto> GetAllBasedOnFilterWithLocation(GetAllCompanyClientSearchDto request, int propertyId)
        {
            var nullResponseInstance = new ListDto<SearchPersonsResultDto>();
            int minimumCharacters = 2;

            if (propertyId <= 0)
            {
                NotifyIdIsMissing();
                return nullResponseInstance;
            }

            if (string.IsNullOrEmpty(request.SearchData))
                NotifyRequired("SearchData");
            else if (request.SearchData.Length < minimumCharacters)
                NotifyMinimumCharacters(minimumCharacters);

            if (string.IsNullOrEmpty(request.PersonType))
                request.PersonType = "N";

            if (Notification.HasNotification())
                return nullResponseInstance;

            int companyId = _companyReadRepository.GetCompanyIdByPropertyId(propertyId);

            if (companyId == 0)
                return nullResponseInstance;

            var requestPerson = new SearchPersonsDto
            {
                SearchData = request.SearchData,
                Page = request.Page.HasValue ? request.Page.Value : 1000,
                PageSize = request.PageSize.HasValue ? request.PageSize.Value : 1000,
                PersonType = request.PersonType,
                DocumentTypeId = request.DocumentTypeId
            };

            var result = _personReadRepository.GetPersonsBasedOnFilterWithCompanyClient(requestPerson, companyId);

            foreach (var item in result.Items)
            {
                item.LocationList = _locationReadRepository.GetLocationDtoListByPersonId(item.Id, request.Language).ToList();
            }

            return result;
        }


        public IListDto<CompanyClientDto> GetAcquirerCompanyClient(GetAllCompanyClientDto request)
        {
            if (request.PropertyId <= 0)
            {
                NotifyIdIsMissing();
                return null;
            }

            int companyId = _companyReadRepository.GetCompanyIdByPropertyId(request.PropertyId);

            return _companyClientReadRepository.GetAcquirerCompanyClient(request, companyId);
        }

        public bool CheckAvailableChannelAssociation(CompanyClientChannelDto dto)
        {
            var associationAvailable = !_companyClientChannelReadRepository.AssociationExists(dto.CompanyId, dto.ChannelId);
            if(!associationAvailable)
            {
                Notification.Raise(Notification
                  .DefaultBuilder
                  .WithMessage(AppConsts.LocalizationSourceName, CompanyClientChannel.EntityError.AlreadyExistsCompanyClientChannelWithSameCompanyId)
                  .Build());
                return false;
            }

            return true;
        }

        public async Task AssociateClientListWithTributsWithheld(List<CompanyClientWithTributeWihheldDto> dtoList)
        {
            var url = new Uri(string.Concat(_configuration.GetValue<string>("TaxRuleEndpoint"), $"/api/br/createimpostoretidolist"));

            var propertyUId = _propertyReadRepository.GetPropertyUId(int.Parse(_applicationUser.PropertyId));

            dtoList.ForEach(dto => {
                dto.PropertyUId = propertyUId;
                dto.OwnerId = dto.Id;
                dto.Id = Guid.Empty;
            });

            var json = JsonConvert.SerializeObject(dtoList);

            using (var httpClient = new HttpClient())
            {
                httpClient.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", GetCurrentToken());

                var responseMessage = await httpClient.PostAsync(url,
                                                new StringContent(json, Encoding.UTF8, "application/json"));

                if (!responseMessage.IsSuccessStatusCode)
                {
                    Notification.Raise(Notification.DefaultBuilder
                     .WithMessage(AppConsts.LocalizationSourceName, CompanyClient.EntityError.CompanyClientErrorIntegrationTribute)
                     .WithDetailedMessage(AppConsts.LocalizationSourceName, CompanyClient.EntityError.CompanyClientErrorIntegrationTribute)
                     .Build());
                }
            }
        }

        public async Task<List<GetAllCompanyClientResultDto>> GetAllAssociatedWithTributWithheld()
        {
            var propertyUId = _propertyReadRepository.GetPropertyUId(int.Parse(_applicationUser.PropertyId));

            var url = new Uri(string.Concat(_configuration.GetValue<string>("TaxRuleEndpoint"), 
                $"/api/br/getallclientwithimpostoretido/property/{propertyUId}"));

            using (var httpClient = new HttpClient())
            {
                httpClient.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", GetCurrentToken());

                var responseMessage = httpClient.GetAsync(url).Result;
                
                if (responseMessage.IsSuccessStatusCode)
                {
                    var json = await responseMessage.Content.ReadAsStringAsync();

                    var companyClientWithTributeWithheldDtoList = JsonConvert.DeserializeObject<ListDto<CompanyClientWithTributeWihheldDto>>(json);

                    var getAllCompanyClientResultDtoList = new List<GetAllCompanyClientResultDto>();

                    foreach (var dto in companyClientWithTributeWithheldDtoList.Items)
                    {
                        var getAllCompanyClientResultDto = _companyClientReadRepository.GetByIdWithDocument(dto.OwnerId);

                        if (getAllCompanyClientResultDto != null)
                            getAllCompanyClientResultDtoList.Add(getAllCompanyClientResultDto);
                    }

                    return getAllCompanyClientResultDtoList;
                }
                else
                {
                    Notification.Raise(Notification.DefaultBuilder
                        .WithMessage(AppConsts.LocalizationSourceName, CompanyClient.EntityError.CompanyClientErrorGetAllTributeWithheld)
                        .WithDetailedMessage(AppConsts.LocalizationSourceName, CompanyClient.EntityError.CompanyClientErrorGetAllTributeWithheld)
                        .Build());

                    return null;
                }
            }
        }

        public async Task DeleteAssociationClientListWithTributsWithheld(Guid companyClientId)
        {
            var propertyUId = _propertyReadRepository.GetPropertyUId(int.Parse(_applicationUser.PropertyId));

            var url = new Uri(string.Concat(_configuration.GetValue<string>("TaxRuleEndpoint"), $"/api/br/deleteassociationwithimpostoretido/owner/{companyClientId}/property/{propertyUId}"));

            using (var httpClient = new HttpClient())
            {
                httpClient.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", GetCurrentToken());

                var responseMessage = await httpClient.DeleteAsync(url);

                if (!responseMessage.IsSuccessStatusCode)
                {
                    Notification.Raise(Notification.DefaultBuilder
                     .WithMessage(AppConsts.LocalizationSourceName, CompanyClient.EntityError.CompanyClientErrorIntegrationTribute)
                     .WithDetailedMessage(AppConsts.LocalizationSourceName, CompanyClient.EntityError.CompanyClientErrorIntegrationTribute)
                     .Build());
                }
            }
        }

        private string GetCurrentToken()
        {
            return _httpContextAccessor.HttpContext.Request.Headers.FirstOrDefault(x => x.Key.Equals("Authorization")).Value.ToString();
        }
    }
}
