﻿using System;
using Thex.Application.Adapters;
using Thex.Domain.Entities;
using Thex.Application.Interfaces;
using Tnf.Domain.Services;
using Thex.Dto;
using Tnf.Dto;
using Thex.Infra.ReadInterfaces;
using Tnf.Notifications;

namespace Thex.Application.Services
{
    public class CountrySubdvisionServiceAppService : ScaffoldCountrySubdvisionServiceAppService, ICountrySubdvisionServiceAppService
    {
        private readonly ICountrySubdvisionServiceReadRepository _countrySubdvisionServiceReadRepoitory;
        public CountrySubdvisionServiceAppService(
            ICountrySubdvisionServiceAdapter countrySubdvisionServiceAdapter,
            IDomainService<CountrySubdvisionService> countrySubdvisionServiceDomainService,
            ICountrySubdvisionServiceReadRepository countrySubdvisionServiceReadRepoitory,
            INotificationHandler notificationHandler)
            : base(countrySubdvisionServiceAdapter, countrySubdvisionServiceDomainService, notificationHandler)
        {
            _countrySubdvisionServiceReadRepoitory = countrySubdvisionServiceReadRepoitory;
        }

        public IListDto<CountrySubdvisionServiceDto> GetAllByPropertyId(int propertyId)
        {
            return _countrySubdvisionServiceReadRepoitory.GetAllByPropertyId(propertyId);
        }
    }
}
