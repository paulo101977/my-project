﻿using System;
using Thex.Application.Adapters;
using Thex.Domain.Entities;
using Thex.Application.Interfaces;
using Tnf.Domain.Services;
using Thex.Infra.ReadInterfaces;
using Thex.Dto;
using System.Collections.Generic;
using Tnf.Dto;
using Thex.Domain.Interfaces.Repositories;
using Thex.Dto.PropertyParameter;
using System.Linq;
using Thex.Common.Enumerations;
using System.Text.RegularExpressions;
using Thex.Common;
using System.Threading.Tasks;
using Tnf.Notifications;
using Thex.Kernel;
using Tnf.Repositories.Uow;

namespace Thex.Application.Services
{
    public class PropertyParameterAppService : ScaffoldPropertyParameterAppService, IPropertyParameterAppService
    {
        private readonly IPropertyParameterReadRepository _propertyParameterReadRepository;
        private readonly IPropertyParameterRepository _propertyParameterRepository;
        private readonly ICurrencyReadRepository _currencyReadRepository;
        private readonly IApplicationUser _applicationUser;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IBillingAccountReadRepository _billingAccountReadRepository;
        private readonly IPropertyCurrencyExchangeRepository _propertyCurrencyExchangeRepository;

        public PropertyParameterAppService(
            IPropertyParameterAdapter propertyParameterAdapter,
            IDomainService<PropertyParameter> propertyParameterDomainService,
            IPropertyParameterReadRepository propertyParameterReadRepository,
            IPropertyParameterRepository propertyParameterRepository,
            ICurrencyReadRepository currencyReadRepository,
            IApplicationUser applicationUser,
            IUnitOfWorkManager unitOfWorkManager,
            INotificationHandler notificationHandler,
            IBillingAccountReadRepository billingAccountReadRepository,
            IPropertyCurrencyExchangeRepository propertyCurrencyExchangeRepository)
            : base(propertyParameterAdapter, propertyParameterDomainService, notificationHandler)
        {
            _propertyParameterReadRepository = propertyParameterReadRepository;
            _propertyParameterRepository = propertyParameterRepository;
            _currencyReadRepository = currencyReadRepository;
            _applicationUser = applicationUser;
            _unitOfWorkManager = unitOfWorkManager;
            _billingAccountReadRepository = billingAccountReadRepository;
            _propertyCurrencyExchangeRepository = propertyCurrencyExchangeRepository;
        }

        public PropertyParameterDividedByGroupDto GetPropertyParameterByPropertyId(int propertyId)
        {
            return _propertyParameterReadRepository.GetPropertyParameterForPropertyId(propertyId);
        }

        public void ToggleAndSaveIsActive(Guid id)
        {
            _propertyParameterRepository.ToggleAndSaveIsActive(id);
        }

        private void ValidateFormatHour(string hour)
        {

            Regex checktime = new Regex("^(?:0?[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$");
            if (!checktime.IsMatch(hour))

            {
                Notification.Raise(Notification.DefaultBuilder
                .WithMessage(AppConsts.LocalizationSourceName, PropertyParameter.EntityError.PropertyParameterHourFormatInvalid)
                .WithDetailedMessage(AppConsts.LocalizationSourceName, PropertyParameter.EntityError.PropertyParameterHourFormatInvalid)
                .Build());
                return;

            };
        }

        private void IsExisCurrencyId(Guid currencyId)
        {
            if (!_currencyReadRepository.IsExistCurrencyId(currencyId))
                NotifyParameterInvalid();

        }

        public void UpdateParameter(PropertyParameterDto parameter)
        {
            using (var uow = _unitOfWorkManager.Begin())
            {
                var loggedPropertyId  = Convert.ToInt32(_applicationUser.PropertyId);

                var propertyOld = _propertyParameterReadRepository.GetPropertyParameterByApplicationParameterId(parameter.ApplicationParameterId);

                if (propertyOld != null)
                {
                    if (propertyOld.PropertyId != loggedPropertyId)
                        NotifyParameterInvalid();

                    parameter.PropertyId = loggedPropertyId;

                    if (Notification.HasNotification()) return;

                    ValidateParameters(parameter, propertyOld);

                    if (Notification.HasNotification()) return;

                    if (parameter.ApplicationParameterId == (int)ApplicationParameterEnum.ParameterDefaultCurrency)
                        DeleteAllPropertyCurrencyQuotation();

                    var builder = PropertyParameterAdapter.Map(parameter);
                    _propertyParameterRepository.Update(builder.Build());
                }
                else
                {
                    parameter.PropertyId = loggedPropertyId;

                    var buildeSigle = PropertyParameterAdapter.Map(parameter);
                    _propertyParameterRepository.Insert(buildeSigle.Build());
                }

                uow.Complete();
            }
        }

        private void DeleteAllPropertyCurrencyQuotation()
        {
            _propertyCurrencyExchangeRepository.DeleteAllAndSaveChanges();
        }
       
        private void ValidateParameters(PropertyParameterDto propertyNew, PropertyParameterDto propertyOld)
        {
            if (!propertyNew.ApplicationParameterId.Equals(propertyOld.ApplicationParameterId) || !propertyNew.PropertyId.Equals(propertyOld.PropertyId))
            {
                NotifyParameterInvalid();
                return;
            }
            var propertyParametersByFeatureGroupList = _propertyParameterReadRepository.GetPropertyParametersByFeatureGroupId(propertyNew.PropertyId, propertyNew.FeatureGroupId);

            if (propertyParametersByFeatureGroupList == null)
                NotifyNullParameter();

            var parametersByFeatureGroupIdList = new List<PropertyParameterDto>();

            parametersByFeatureGroupIdList.AddRange(propertyParametersByFeatureGroupList.Where(x => x.ApplicationParameterId != propertyNew.ApplicationParameterId));

            switch (propertyNew.FeatureGroupId)
            {
                case (int)FeatureGroupEnum.GeneralParameters:
                    ValidateGeneralParameters(parametersByFeatureGroupIdList, propertyNew);
                    break;
                case (int)FeatureGroupEnum.AgeGroupChieldren:
                    if (propertyNew.IsActive.Value)
                    {
                        parametersByFeatureGroupIdList.Add(propertyNew);
                        AgeGroupChildren(parametersByFeatureGroupIdList);
                    }
                    break;

                default:
                    break;
            }

            if (_billingAccountReadRepository.AnyBillingAccountItemByPropertyId(propertyNew.PropertyId) && propertyNew.ApplicationParameterId == (int)ApplicationParameterEnum.ParameterDefaultCurrency)
            {
                Notification.Raise(Notification.DefaultBuilder
                .WithMessage(AppConsts.LocalizationSourceName, PropertyParameter.EntityError.PropertyParameterDoesNotChangeCurrencyIfExistsAnyBillingAccountItem)
                .WithDetailedMessage(AppConsts.LocalizationSourceName, PropertyParameter.EntityError.PropertyParameterDoesNotChangeCurrencyIfExistsAnyBillingAccountItem)
                .Build());
                return;
            }
        }


        private void AgeGroupChildren(List<PropertyParameterDto> AgeGroupChildrens)
        {
            if (AgeGroupChildrens == null || AgeGroupChildrens.Count() != 3)
                return;

            var children1 = AgeGroupChildrens.Where(x => x.ApplicationParameterId == (int)ApplicationParameterEnum.ParameterChildren_1).FirstOrDefault();
            var children2 = AgeGroupChildrens.Where(x => x.ApplicationParameterId == (int)ApplicationParameterEnum.ParameterChildren_2).FirstOrDefault();
            var children3 = AgeGroupChildrens.Where(x => x.ApplicationParameterId == (int)ApplicationParameterEnum.ParameterChildren_3).FirstOrDefault();

            if (Convert.ToInt16(children1.PropertyParameterValue) > Convert.ToInt16(children2.PropertyParameterValue) && children2 != null && children2.IsActive.Value || Convert.ToInt16(children1.PropertyParameterValue) > Convert.ToInt16(children3.PropertyParameterValue) && children3 != null && children3.IsActive.Value || Convert.ToInt16(children2.PropertyParameterValue) > Convert.ToInt16(children3.PropertyParameterValue) && children3 != null && children3.IsActive.Value)
            {
                Notification.Raise(Notification.DefaultBuilder
                .WithMessage(AppConsts.LocalizationSourceName, PropertyParameter.EntityError.PropertyParameterAgeRangeInvalidad)
                .WithDetailedMessage(AppConsts.LocalizationSourceName, PropertyParameter.EntityError.PropertyParameterAgeRangeInvalidad)
                .Build());
                return;
            }

            if (Convert.ToInt16(children1.PropertyParameterValue) < Convert.ToInt16(children1.PropertyParameterMinValue) || Convert.ToInt16(children1.PropertyParameterValue) > Convert.ToInt16(children1.PropertyParameterMaxValue))
            {
                NotifyParameterInvalid();
            }
            else if (Convert.ToInt16(children2.PropertyParameterValue) < Convert.ToInt16(children2.PropertyParameterMinValue) || Convert.ToInt16(children2.PropertyParameterValue) > Convert.ToInt16(children2.PropertyParameterMaxValue) && children2.IsActive.Value)
            {
                NotifyParameterInvalid();
            }
            else if (Convert.ToInt16(children3.PropertyParameterValue) < Convert.ToInt16(children3.PropertyParameterMinValue) || Convert.ToInt16(children3.PropertyParameterValue) > Convert.ToInt16(children3.PropertyParameterMaxValue) && children3.IsActive.Value)
            {
                NotifyParameterInvalid();
            }
        }

        private void ValidateGeneralParameters(List<PropertyParameterDto> generalparameters, PropertyParameterDto parameter)
        {
            PropertyParameterDto hourCheckin = null;
            PropertyParameterDto hourCheckout = null;

            if (parameter.ApplicationParameterId == (int)ApplicationParameterEnum.ParameterCheckInTime)
            {
                //Validar formato Hora Check in
                hourCheckin = parameter;
                hourCheckout = generalparameters.Where(x => x.ApplicationParameterId == (int)ApplicationParameterEnum.ParameterCheckOutTime).FirstOrDefault();

                if (hourCheckin == null) NotifyParameterInvalid();

                if (Notification.HasNotification()) return;

                ValidateFormatHour(hourCheckin.PropertyParameterValue);

                if (Notification.HasNotification()) return;
            }

            if (parameter.ApplicationParameterId == (int)ApplicationParameterEnum.ParameterCheckOutTime)
            {
                hourCheckout = parameter;
                hourCheckin = generalparameters.Where(x => x.ApplicationParameterId == (int)ApplicationParameterEnum.ParameterCheckInTime).FirstOrDefault();

                if (hourCheckout == null) NotifyParameterInvalid();

                if (Notification.HasNotification()) return;

                ValidateFormatHour(hourCheckout.PropertyParameterValue);

                if (Notification.HasNotification()) return;
            }

            if (hourCheckin != null && 
                hourCheckout != null && 
                string.IsNullOrEmpty(hourCheckin.PropertyParameterValue) &&
                string.IsNullOrEmpty(hourCheckout.PropertyParameterValue) &&
                Convert.ToDateTime(hourCheckout.PropertyParameterValue) > Convert.ToDateTime(hourCheckin.PropertyParameterValue))
            {
                Notification.Raise(Notification.DefaultBuilder
                .WithMessage(AppConsts.LocalizationSourceName, PropertyParameter.EntityError.PropertyParameterCheckoutLargerCheckin)
                .WithDetailedMessage(AppConsts.LocalizationSourceName, PropertyParameter.EntityError.PropertyParameterCheckoutLargerCheckin)
                .Build());
                return;
            }
        }

        public Task<List<PropertyParameterDto>> GetChildrenAgeGroupList(int propertyId)
        {
            return _propertyParameterReadRepository.GetChildrenAgeGroupList(propertyId);
        }

        public IListDto<PropertyParameterDto> GetChildrenAgeGroupForRoomTypeList(int propertyId)
        {
            return _propertyParameterReadRepository.GetChildrenAgeGroupForRoomTypeList(propertyId);

        }

        public PropertyParameterDto GetSystemDate(int propertyId)
        {
            var systemDate =  _propertyParameterReadRepository.GetSystemDate(propertyId);
            var propertyParameterDto = new PropertyParameterDto();
            if (systemDate == null)
            {
                propertyParameterDto.PropertyParameterValue = null;
            }
            else
            {
                propertyParameterDto.PropertyParameterValue = systemDate.GetValueOrDefault().ToString();
            }
            return propertyParameterDto;
        }
    }
}
