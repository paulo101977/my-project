﻿using System;
using Thex.Application.Adapters;
using Thex.Domain.Entities;
using Thex.Application.Interfaces;
using Tnf.Domain.Services;
using Tnf.Notifications;

namespace Thex.Application.Services
{
    public class BusinessPartnerAppService : ScaffoldBusinessPartnerAppService, IBusinessPartnerAppService
    {
        public BusinessPartnerAppService(
            IBusinessPartnerAdapter businessPartnerAdapter,
            IDomainService<BusinessPartner> businessPartnerDomainService,
            INotificationHandler notificationHandler)
            : base(businessPartnerAdapter, businessPartnerDomainService, notificationHandler)
        {
        }
    }
}
