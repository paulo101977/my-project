﻿using System;
using Thex.Application.Adapters;
using Thex.Domain.Entities;
using Thex.Application.Interfaces;
using Tnf.Domain.Services;
using System.Collections.Generic;
using Thex.Dto;
using Thex.Domain.Interfaces.Repositories;
using Tnf.Notifications;

namespace Thex.Application.Services
{
    public class RatePlanCommissionAppService : ScaffoldRatePlanCommissionAppService, IRatePlanCommissionAppService
    {
        private readonly IRatePlanCommissionRepository _ratePlanCommissionRepository;
        public RatePlanCommissionAppService(
            IRatePlanCommissionAdapter ratePlanCommissionAdapter,
            IDomainService<RatePlanCommission> ratePlanCommissionDomainService,
            IRatePlanCommissionRepository ratePlanCommissionRepository,
            INotificationHandler notificationHandler)
            : base(ratePlanCommissionAdapter, ratePlanCommissionDomainService, notificationHandler)
        {
            _ratePlanCommissionRepository = ratePlanCommissionRepository;
        }

        public void RatePlanCommissionCreate(List<RatePlanCommissionDto> ratePlanCommissions)
        {
            if (ratePlanCommissions != null && ratePlanCommissions.Count > 0)
            {
                var ratePlanCommissionList = new List<RatePlanCommission>();
                foreach (var ratePlanCommission in ratePlanCommissions)
                {
                    ValidateDto<RatePlanCommissionDto>(ratePlanCommission, nameof(ratePlanCommission));

                    if (Notification.HasNotification()) break;
                    var ratePlanCommissionBuilder = RatePlanCommissionAdapter.Map(ratePlanCommission);


                    if (Notification.HasNotification()) break;

                    var builder = ratePlanCommissionBuilder.Build();

                    if (Notification.HasNotification()) break;

                    ratePlanCommissionList.Add(builder);

                  
                }
                if (Notification.HasNotification()) return;

                _ratePlanCommissionRepository.RatePlanCommissionCreate(ratePlanCommissionList);

            }
        }
    }
}
