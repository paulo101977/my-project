﻿using System;
using Thex.Application.Adapters;
using Thex.Domain.Entities;
using Thex.Application.Interfaces;
using Tnf.Domain.Services;
using Tnf.Notifications;

namespace Thex.Application.Services
{
    public class StatusAppService : ScaffoldStatusAppService, IStatusAppService
    {
        public StatusAppService(
            IStatusAdapter statusAdapter,
            IDomainService<Status> statusDomainService,
            INotificationHandler notificationHandler)
            : base(statusAdapter, statusDomainService, notificationHandler)
        {
        }
    }
}
