﻿using System;
using Thex.Application.Adapters;
using Thex.Domain.Entities;
using Thex.Application.Interfaces;
using Tnf.Domain.Services;
using Thex.Dto;
using Thex.Domain.Interfaces.Repositories;
using System.Threading.Tasks;
using Tnf.Dto;
using Thex.Infra.ReadInterfaces;
using Tnf.Notifications;
using System.Linq;
using Thex.Kernel;
using Thex.Common.Enumerations;
using System.Collections.Generic;
using Thex.Domain.Services.Interfaces;
using Thex.Common;
using Tnf.Repositories.Uow;
using Newtonsoft.Json;

namespace Thex.Application.Services
{
    public class BillingInvoiceAppService : ScaffoldBillingInvoiceAppService, IBillingInvoiceAppService
    {
        private readonly IBillingInvoiceRepository _billingInvoiceRepository;
        private readonly IBillingAccountAppService _billingAccountAppService;
        private readonly IPropertyAppService _propertyAppService;
        private readonly IGuestRegistrationReadRepository _guestRegistrationReadRepository;
        private readonly ICompanyClientReadRepository _companyClientReadRepository;
        private readonly IApplicationUser _applicationUser;
        private readonly IBillingInvoicePropertyRepository _billingInvoicePropertyRepository;
        private readonly IBillingAccountItemAppService _billingAccountItemAppService;
        private readonly IPropertyParameterReadRepository _propertyParameterReadRepository;
        private readonly IBillingAccountItemDomainService _billingAccountItemDomainService;
        private readonly IBillingAccountItemAdapter _billingAccountItemAdapter;
        private readonly IBillingInvoiceAdapter _billingInvoiceAdapter;
        private readonly IBillingAccountItemRepository _billingAccountItemRepository;
        protected readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IBillingAccountRepository _billingAccountRepository;
        private readonly IPersonReadRepository _personReadRepository;
        private readonly IIntegrationAppService _integrationAppService;
        private readonly INotificationHandler _notificationHandler;
        private readonly IBillingInvoiceReadRepository _billingInvoiceReadRepository;

        public BillingInvoiceAppService(
            IBillingInvoiceAdapter billingInvoiceAdapter,
            IDomainService<BillingInvoice> billingInvoiceDomainService,
            IBillingInvoiceRepository billingInvoiceRepository,
            IBillingAccountAppService billingAccountAppService,
            IPropertyAppService propertyAppService,
            IGuestRegistrationReadRepository guestRegistrationReadRepository,
            ICompanyClientReadRepository companyClientReadRepository,
            INotificationHandler notificationHandler,
            IApplicationUser applicationUser,
            IBillingInvoicePropertyRepository billingInvoicePropertyRepository,
            IBillingAccountItemAppService billingAccountItemAppService,
            IPropertyParameterReadRepository propertyParameterReadRepository,
            IBillingAccountItemDomainService billingAccountItemDomainService,
            IBillingAccountItemAdapter billingAccountItemAdapter,
            IBillingAccountItemRepository billingAccountItemRepository,
            IUnitOfWorkManager unitOfWorkManager,
            IBillingAccountRepository billingAccountRepository,
            IPersonReadRepository personReadRepository,
            IIntegrationAppService integrationAppService,
            IBillingInvoiceReadRepository billingInvoiceReadRepository)
            : base(billingInvoiceAdapter, billingInvoiceDomainService, notificationHandler)
        {
            _billingInvoiceRepository = billingInvoiceRepository;
            _billingAccountAppService = billingAccountAppService;
            _propertyAppService = propertyAppService;
            _guestRegistrationReadRepository = guestRegistrationReadRepository;
            _companyClientReadRepository = companyClientReadRepository;
            _applicationUser = applicationUser;
            _billingInvoicePropertyRepository = billingInvoicePropertyRepository;
            _billingAccountItemAppService = billingAccountItemAppService;
            _propertyParameterReadRepository = propertyParameterReadRepository;
            _billingAccountItemDomainService = billingAccountItemDomainService;
            _billingAccountItemAdapter = billingAccountItemAdapter;
            _billingInvoiceAdapter = billingInvoiceAdapter;
            _billingAccountItemRepository = billingAccountItemRepository;
            _unitOfWorkManager = unitOfWorkManager;
            _billingAccountRepository = billingAccountRepository;
            _personReadRepository = personReadRepository;
            _integrationAppService = integrationAppService;
            _notificationHandler = notificationHandler;
            _billingInvoiceReadRepository = billingInvoiceReadRepository;
        }

        public virtual BillingInvoiceDto CreateBillingInvoice(BillingInvoiceDto dto)
        {
            ValidateDto<BillingInvoiceDto>(dto, nameof(dto));

            if (Notification.HasNotification())
                return BillingInvoiceDto.NullInstance;

            var billingInvoiceBuilder = BillingInvoiceAdapter.MapToCreditNote(dto);

            var builder = billingInvoiceBuilder.Build();

            _billingInvoiceRepository.Create(builder);

            return dto;
        }

        public async Task<RpsDto> GetRpsByInvoiceId(int propertyId, Guid invoiceId, string languageIsoCode)
        {
            if (invoiceId == Guid.Empty)
            {
                NotifyIdIsMissing();
                return null;
            }

            var result = new RpsDto() { ServiceRecipient = new RpsServiceRecipientDto() };
            var invoice = await Get(new DefaultGuidRequestDto(invoiceId) { Expand = "BillingInvoiceProperty" });

            var billingAccount = _billingAccountAppService.GetByInvoiceId(invoiceId);

            if (billingAccount.PersonHeaderId.HasValue)
            {
                result.ServiceRecipient.Company = _personReadRepository.GetHeaderRpsById(billingAccount.PersonHeaderId.Value);
            }
            else if (billingAccount.GuestReservationItemId.HasValue)
            {
                result.ServiceRecipient.Guest = _guestRegistrationReadRepository.GetGuestRegistrationByGuestReservationItemId(propertyId, billingAccount.GuestReservationItemId.Value, languageIsoCode);
            }
            else
            {
                result.ServiceRecipient.Company = _companyClientReadRepository.GetCompanyClientDtoById(billingAccount.CompanyClientId.Value);
            }

            result.Invoice = invoice;
            result.AccountEntries = _billingAccountAppService.GetUniqueBillingAccountWithEntries(new DefaultGuidRequestDto(billingAccount.Id), invoice.Id, false);
            result.ServiceProvider = _propertyAppService.GetByPropertyId(billingAccount.PropertyId);

            return result;
        }

        public async Task<BillingInvoiceDto> CreateCreditNoteAndUpdateLastNumber(List<BillingAccountItemCreditAmountDto> billingAccountItemDtoList)
        {
            using (var uow = _unitOfWorkManager.Begin())
            {
                var reasonId = billingAccountItemDtoList.Any() ? billingAccountItemDtoList.FirstOrDefault().ReasonId : default(int);

                VerifyReasonCreditNote(reasonId);

                if (Notification.HasNotification())
                    return BillingInvoiceDto.NullInstance;

                var billingInvoiceReferenceId = billingAccountItemDtoList.Select(b => b.BillingInvoiceId.Value).FirstOrDefault();
                var billingInvoiceReference = await _billingInvoiceRepository.GetById(billingInvoiceReferenceId);

                VerifyEmissionCreditNote(billingInvoiceReference);

                if (Notification.HasNotification())
                    return BillingInvoiceDto.NullInstance;

                VerifyCreditNoteHasPaymentType(billingAccountItemDtoList);

                if (Notification.HasNotification())
                    return BillingInvoiceDto.NullInstance;

                var newBillingAccount = CreateBillingAccountToCreditNote(billingInvoiceReference);

                if (Notification.HasNotification())
                    return BillingInvoiceDto.NullInstance;

                var systemDate = _propertyParameterReadRepository.GetSystemDate(int.Parse(_applicationUser.PropertyId)).Value;

                var billingAccountItemNewList = CreateBillingAccountItemListToNewAccount(billingAccountItemDtoList, newBillingAccount.Id, systemDate);

                CreateBillingAccountItemDebit(billingAccountItemDtoList, billingInvoiceReference, newBillingAccount, systemDate);

                if (Notification.HasNotification())
                    return BillingInvoiceDto.NullInstance;

                var billingInvoicePropertyChild = _billingInvoicePropertyRepository.GetChildByParentId(billingInvoiceReference.BillingInvoicePropertyId);

                _billingInvoicePropertyRepository.IncrementLastNumberAndUpdate(billingInvoicePropertyChild);

                if (Notification.HasNotification())
                    return BillingInvoiceDto.NullInstance;

                var issueTotalCreditNote = billingInvoiceReference.BillingAccountItemList.Sum(b => b.Amount * -1) == billingInvoiceReference.BillingAccountItemList.Sum(b => b.CreditAmount);

                var billingInvoiceDto = new BillingInvoiceDto
                {
                    Id = Guid.NewGuid(),
                    BillingInvoiceNumber = billingInvoicePropertyChild.LastNumber,
                    BillingInvoiceSeries = billingInvoicePropertyChild.BillingInvoicePropertySeries,
                    PropertyId = int.Parse(_applicationUser.PropertyId),
                    BillingAccountId = newBillingAccount.Id,
                    BillingInvoicePropertyId = billingInvoicePropertyChild.Id,
                    BillingInvoiceStatusId = (int)BillingInvoiceStatusEnum.NotSent,
                    EmissionDate = DateTime.UtcNow.ToZonedDateTimeLoggedUser(),
                    EmissionUserId = _applicationUser.UserUid,
                    BillingInvoiceTypeId = issueTotalCreditNote ? (int)BillingInvoiceTypeEnum.CreditNoteTotal :
                                                                  (int)BillingInvoiceTypeEnum.CreditNotePartial,
                    BillingInvoiceCancelReasonId = reasonId,
                    BillingInvoiceReferenceId = billingInvoiceReferenceId,
                    IntegratorId = billingInvoiceReference.IntegratorId
                };

                CreateBillingInvoice(billingInvoiceDto);

                if (Notification.HasNotification())
                    return BillingInvoiceDto.NullInstance;

                AssociateInvoiceToBillingAccountItemList(billingAccountItemNewList, billingInvoiceDto);

                if (issueTotalCreditNote)
                {
                    billingInvoiceReference.SetBillingInvoiceCreditId(billingInvoiceDto.Id);
                    _billingInvoiceRepository.Update(billingInvoiceReference);
                }

                CloseNewBillingAccount(newBillingAccount);

                uow.Complete();

                if (!Notification.HasNotification())
                {
                    var notificationDto = await _integrationAppService.GenerateCreditNote(issueTotalCreditNote ? BillingInvoiceTypeEnum.CreditNoteTotal :
                                                                                        BillingInvoiceTypeEnum.CreditNotePartial, billingInvoiceDto.Id).ConfigureAwait(false);
                    if (notificationDto != null)
                    {
                        var notify = JsonConvert.DeserializeObject<NotificationEvent>(JsonConvert.SerializeObject(notificationDto));
                        _notificationHandler.Raise(notify);
                    }
                }
                    
                return billingInvoiceDto;
            }
        }

        private void VerifyReasonCreditNote(int reasonId)
        {
            if (reasonId == default(int))
                Notification.Raise(Notification.DefaultBuilder
                .WithMessage(AppConsts.LocalizationSourceName, BillingInvoice.EntityError.BillingInvoiceCreditNoteHasReason)
                .WithDetailedMessage(AppConsts.LocalizationSourceName, BillingInvoice.EntityError.BillingInvoiceCreditNoteHasReason)
                .Build());
        }

        private void VerifyEmissionCreditNote(BillingInvoice billingInvoiceReference)
        {
            if (billingInvoiceReference.BillingInvoiceTypeId != (int)BillingInvoiceTypeEnum.Factura)
            {
                Notification.Raise(Notification.DefaultBuilder
                .WithMessage(AppConsts.LocalizationSourceName, BillingInvoice.EntityError.BillingInvoiceFacturaOnlyIssueCreditNote)
                .WithDetailedMessage(AppConsts.LocalizationSourceName, BillingInvoice.EntityError.BillingInvoiceFacturaOnlyIssueCreditNote)
                .Build());
                return;
            }


            if (FacturaAlreadyIssuedCreditNoteTotal(billingInvoiceReference))
            {
                Notification.Raise(Notification.DefaultBuilder
                 .WithMessage(AppConsts.LocalizationSourceName, BillingInvoice.EntityError.BillingInvoiceFacturaAlreadyIssuedCreditNote)
                 .WithDetailedMessage(AppConsts.LocalizationSourceName, BillingInvoice.EntityError.BillingInvoiceFacturaAlreadyIssuedCreditNote)
                 .Build());
                return;
            }
        }

        private bool FacturaAlreadyIssuedCreditNoteTotal(BillingInvoice billingInvoiceReference)
        {
            return billingInvoiceReference.BillingInvoiceCreditId.HasValue;
        }

        private void VerifyCreditNoteHasPaymentType(List<BillingAccountItemCreditAmountDto> billingAccountItemDtoList)
        {
            if (billingAccountItemDtoList.Any(b => b.BillingItemId == 0))
            {
                Notification.Raise(Notification.DefaultBuilder
                .WithMessage(AppConsts.LocalizationSourceName, BillingInvoice.EntityError.BillingInvoiceCreditNoteEmptyPaymentType)
                .WithDetailedMessage(AppConsts.LocalizationSourceName, BillingInvoice.EntityError.BillingInvoiceCreditNoteEmptyPaymentType)
                .Build());
            }
        }

        private BillingAccountDto CreateBillingAccountToCreditNote(BillingInvoice billingInvoiceReference)
        {
            var billingAccountReference = billingInvoiceReference.BillingAccount;

            var addBillingAccountDto = new AddBillingAccountDto
            {
                BillingAccountName = billingAccountReference.BillingAccountName + " - NC",
            };

            return _billingAccountAppService.AddBillingAccountCreditNote(addBillingAccountDto, billingAccountReference);
        }

        private List<BillingAccountItem> CreateBillingAccountItemListToNewAccount(IList<BillingAccountItemCreditAmountDto> billingAccountItemDtoList, Guid newBillingAccountId, DateTime systemDate)
        {
            var billingAccountItemNewList = new List<BillingAccountItem>();
            var billingAccountItemOriginalToUpdateList = new List<BillingAccountItem>();

            foreach (var billingAccountItemDto in billingAccountItemDtoList)
            {
                var billingAccountItemOriginal = _billingAccountItemRepository.GetById(billingAccountItemDto.Id);

                if (billingAccountItemDto.CreditAmount <= 0)
                    Notification.Raise(Notification.DefaultBuilder
                               .WithMessage(AppConsts.LocalizationSourceName, BillingInvoice.EntityError.BillingInvoiceCreditNoteValueCanNotBeLessThanOrEqualToZero)
                               .WithDetailedMessage(AppConsts.LocalizationSourceName, BillingInvoice.EntityError.BillingInvoiceCreditNoteValueCanNotBeLessThanOrEqualToZero)
                               .Build());

                if (Notification.HasNotification())
                    return null;

                var creditAmountOfBillingItemOriginal = billingAccountItemOriginal.CreditAmount.HasValue ? billingAccountItemOriginal.CreditAmount : 0;

                if (billingAccountItemDto.CreditAmount > ((billingAccountItemOriginal.Amount * -1) - creditAmountOfBillingItemOriginal))
                    Notification.Raise(Notification.DefaultBuilder
                               .WithMessage(AppConsts.LocalizationSourceName, BillingInvoice.EntityError.BillingInvoiceCreditNoteValueIsGreaterThanAllowed)
                               .WithDetailedMessage(AppConsts.LocalizationSourceName, BillingInvoice.EntityError.BillingInvoiceCreditNoteValueIsGreaterThanAllowed)
                               .Build());

                if (Notification.HasNotification())
                    return null;

                var billingAccountItemCreditNoteBuilder = _billingAccountItemAdapter.MapCreditNote(billingAccountItemOriginal, billingAccountItemDto.CreditAmount.Value, newBillingAccountId, systemDate);

                billingAccountItemNewList.Add(billingAccountItemCreditNoteBuilder.Build());

                billingAccountItemOriginal.SetCreditAmount(billingAccountItemDto.CreditAmount.Value);
                billingAccountItemOriginalToUpdateList.Add(billingAccountItemOriginal);
            }

            _billingAccountItemRepository.AddRange(billingAccountItemNewList);

            _billingAccountItemRepository.UpdateRange(billingAccountItemOriginalToUpdateList);

            return billingAccountItemNewList;
        }

        private void CreateBillingAccountItemDebit(List<BillingAccountItemCreditAmountDto> billingAccountItemDtoList, BillingInvoice billingInvoiceReference, BillingAccountDto newBillingAccount, DateTime systemDate)
        {
            var billingAccountItemDebitDto = new BillingAccountItemDebitDto
            {
                BillingAccountId = newBillingAccount.Id,
                BillingItemId = billingAccountItemDtoList.FirstOrDefault().BillingItemId,
                Amount = billingAccountItemDtoList.Sum(b => b.CreditAmount).GetValueOrDefault(),
                DateParameter = systemDate,
                CurrencyId = billingInvoiceReference.BillingAccountItemList.FirstOrDefault().CurrencyId,
                CurrencyExchangeReferenceId = billingInvoiceReference.BillingAccountItemList.FirstOrDefault()?.CurrencyExchangeReferenceId,
                CurrencyExchangeReferenceSecId = billingInvoiceReference.BillingAccountItemList.FirstOrDefault()?.CurrencyExchangeReferenceSecId,
            };

            var billingAccountItemBuilder = _billingAccountItemAdapter.MapDebit(billingAccountItemDebitDto);

            _billingAccountItemRepository.InsertAndSaveChanges(billingAccountItemBuilder.Build());
        }

        private void AssociateInvoiceToBillingAccountItemList(List<BillingAccountItem> billingAccountItemList, BillingInvoiceDto billingInvoiceDto)
        {
            foreach (var billingAccountItemId in billingAccountItemList.Select(b => b.Id))
            {
                var billingAccountItemEntity = _billingAccountItemRepository.GetById(billingAccountItemId);

                billingAccountItemEntity.SetBillingInvoice(billingInvoiceDto.Id);
                _billingAccountItemRepository.Update(billingAccountItemEntity);
            }
        }

        private void CloseNewBillingAccount(BillingAccountDto newBillingAccountDto)
        {
            var billingAccount = _billingAccountRepository.GetById(newBillingAccountDto.Id);
            billingAccount.SetStatusClosed();
            _billingAccountRepository.Update(billingAccount);
        }
    }
}
