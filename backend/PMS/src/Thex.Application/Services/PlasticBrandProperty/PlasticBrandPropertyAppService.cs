﻿using System;
using Thex.Application.Adapters;
using Thex.Domain.Entities;
using Thex.Application.Interfaces;
using Tnf.Domain.Services;
using System.Threading.Tasks;
using Tnf.Dto;
using Thex.Dto;
using Thex.Dto.PlasticBrandProperty;
using Thex.Infra.ReadInterfaces;
using Tnf.Notifications;

namespace Thex.Application.Services
{
    public class PlasticBrandPropertyAppService : ScaffoldPlasticBrandPropertyAppService, IPlasticBrandPropertyAppService
    {
        private readonly IPlasticBrandPropertyReadRepository _plasticBrandPropertyReadRepository;

        public PlasticBrandPropertyAppService(
            IPlasticBrandPropertyAdapter plasticBrandPropertyAdapter,
            IDomainService<PlasticBrandProperty> plasticBrandPropertyDomainService,
            IPlasticBrandPropertyReadRepository plasticBrandPropertyReadRepository,
            INotificationHandler notificationHandler)
            : base(plasticBrandPropertyAdapter, plasticBrandPropertyDomainService, notificationHandler)
        {
            _plasticBrandPropertyReadRepository = plasticBrandPropertyReadRepository;
        }

        public IListDto<PlasticBrandPropertyResultDto> GetAllByPropertyId(GetAllPlasticBrandPropertyDto request)
        {
            ValidateRequestAllDto(request, nameof(request));

            if (Notification.HasNotification())
                return null;

            return _plasticBrandPropertyReadRepository.GetAllByPropertyId(request);
        }
    }
}
