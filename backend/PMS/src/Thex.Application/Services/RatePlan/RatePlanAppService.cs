﻿using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Linq;
using System.Threading.Tasks;
using Thex.Application.Adapters;
using Thex.Application.Interfaces;
using Thex.Common;
using Thex.Common.Enumerations;
using Thex.Domain.Entities;
using Thex.Domain.Interfaces.Repositories;
using Thex.Dto;
using Thex.Dto.RatePlan;
using Thex.Dto.RoomTypes;
using Thex.Infra.ReadInterfaces;
using Thex.Kernel;
using Tnf.Domain.Services;
using Tnf.Dto;
using Tnf.Notifications;

namespace Thex.Application.Services
{
    public class RatePlanAppService : ScaffoldRatePlanAppService, IRatePlanAppService
    {
        private readonly ICurrencyReadRepository _currencyReadRepository;
        private readonly IAgreementTypeReadRepository _agreementTypeReadRepository;
        private readonly IPropertyMealPlanTypeReadRepository _propertyMealPlanTypeReadRepository;
        private readonly IRoomTypeAppService _roomTypeAppService;
        private readonly ICompanyClientAppService _companyClientAppService;
        private readonly IBillingItemReadRepository _billingItemReadRepository;
        private readonly IRatePlanRoomTypeAppService _ratePlanRoomTypeAppService;
        private readonly IRatePlanCompanyClientAppService _ratePlanCompanyClientAppService;
        private readonly IRatePlanCommissionAppService _ratePlanCommissionAppService;
        private readonly IRatePlanReadRepository _ratePlanReadRepository;
        private readonly IRatePlanRoomTypeAdapter _ratePlanRoomTypeAdapter;
        private readonly IRatePlanRepository _ratePlanRepository;
        private readonly IPropertyCompanyClientCategoryReadRepository _propertyCompanyClientCategoryReadRepository;
        private readonly IRateTypeReadRepository _rateTypeReadRepository;
        private readonly IMarketSegmentAppService _marketsegmentappservice;
        private readonly IPropertyBaseRateHistoryReadRepository _propertyBaseRateHistoryReadRepository;
        private readonly IApplicationUser _applicationUser;
        private readonly IPropertyParameterReadRepository _propertyParameterReadRepository;

        public RatePlanAppService(
            IRatePlanAdapter ratePlanAdapter,
            IDomainService<RatePlan> ratePlanDomainService,

            ICurrencyReadRepository currencyReadRepository,
            IAgreementTypeReadRepository agreementTypeReadRepository,
            IPropertyMealPlanTypeReadRepository propertyMealPlanTypeReadRepository,
            IRoomTypeAppService roomTypeAppService,
            ICompanyClientAppService companyClientAppService,
            IBillingItemReadRepository billingItemReadRepository,
            IRatePlanRoomTypeAppService ratePlanRoomTypeAppService,
            IRatePlanCompanyClientAppService ratePlanCompanyClientAppService,
            IRatePlanCommissionAppService ratePlanCommissionAppService,
            IApplicationUser applicationUser,
            IRatePlanReadRepository ratePlanReadRepository,
            IRatePlanRoomTypeAdapter ratePlanRoomTypeAdapter,
            IRatePlanRepository ratePlanRepository,
            IPropertyCompanyClientCategoryReadRepository propertyCompanyClientCategoryReadRepository,
            IRateTypeReadRepository rateTypeReadRepository,
            IMarketSegmentAppService marketsegmentappservice,
            IPropertyBaseRateHistoryReadRepository propertyBaseRateHistoryReadRepository,
            INotificationHandler notificationHandler,
            IPropertyParameterReadRepository propertyParameterReadRepository
            )
            : base(ratePlanAdapter, ratePlanDomainService, notificationHandler)
        {
            _currencyReadRepository = currencyReadRepository;
            _agreementTypeReadRepository = agreementTypeReadRepository;
            _propertyMealPlanTypeReadRepository = propertyMealPlanTypeReadRepository;
            _roomTypeAppService = roomTypeAppService;
            _companyClientAppService = companyClientAppService;
            _billingItemReadRepository = billingItemReadRepository;
            _ratePlanRoomTypeAppService = ratePlanRoomTypeAppService;
            _ratePlanCompanyClientAppService = ratePlanCompanyClientAppService;
            _ratePlanCommissionAppService = ratePlanCommissionAppService;
            _ratePlanReadRepository = ratePlanReadRepository;
            _ratePlanRoomTypeAdapter = ratePlanRoomTypeAdapter;
            _ratePlanRepository = ratePlanRepository;
            _propertyCompanyClientCategoryReadRepository = propertyCompanyClientCategoryReadRepository;
            _rateTypeReadRepository = rateTypeReadRepository;
            _marketsegmentappservice = marketsegmentappservice;
            _propertyBaseRateHistoryReadRepository = propertyBaseRateHistoryReadRepository;
            _applicationUser = applicationUser;
            _propertyParameterReadRepository = propertyParameterReadRepository;
        }

        public HeaderRatePlanDto GetHeaderRatePlan(int propertyId)
        {
            GetAllMarketSegmentDto vDto = new GetAllMarketSegmentDto();
            vDto.PropertyId = propertyId;

            var headerRatePlan = new HeaderRatePlanDto()
            {
                CurrencyList = _currencyReadRepository.GetAllCurrency(),
                AgreementTypeList = _agreementTypeReadRepository.GetAllAgreementType(),
                PropertyMealPlanTypeList = _propertyMealPlanTypeReadRepository.GetAllPropertyMealPlanTypeByProperty(propertyId),
                PropertyRateTypeList = _rateTypeReadRepository.GetAllRateType(),
                MarketSegmentList = _marketsegmentappservice.GetAllByPropertyId(vDto, propertyId)

            };

            return headerRatePlan;
        }

        public IListDto<RoomTypeDto> GetRoomTypesByPropertyId(GetAllRoomTypesDto request, int propertyId)
        {
            return _roomTypeAppService.GetRoomTypesByPropertyId(request, propertyId);
        }

        public IListDto<GetAllCompanyClientResultDto> GetAllByPropertyId(GetAllCompanyClientDto request, int propertyId)
        {
            return _companyClientAppService.GetAllByPropertyId(request, propertyId);
        }

        public IListDto<BillingItemDto> GetAllBillingItemByPropertyId(int propertyId)
        {
            var billingItemList = _billingItemReadRepository.GetAllServiceFilterTypeDailyByProperty(propertyId);

            return new ListDto<BillingItemDto>()
            {
                HasNext = false,
                Items = billingItemList,
                //Total = billingItemList.Count
            };
        }

        public void RatePlanCreate(RatePlanDto ratePlanDto)
        {
            ValidateDto<RatePlanDto>(ratePlanDto, nameof(ratePlanDto));

            if (Notification.HasNotification()) return;

            ValidateFields(ratePlanDto);

            if (Notification.HasNotification()) return;

            var ratePlanBuilder = RatePlanAdapter.Map(ratePlanDto);

            ValidateDate(ratePlanDto.StartDate, ratePlanDto.EndDate.Value);

            if (Notification.HasNotification()) return;

            ValidateStartDateGreaterCurrentDate(ratePlanDto.StartDate);

            if (Notification.HasNotification()) return;

            VerifyNameAgreementSamePeriod(ratePlanDto.StartDate, ratePlanDto.EndDate.Value, ratePlanDto.AgreementName, ratePlanDto.Id, ratePlanDto.PropertyId);

            if (Notification.HasNotification()) return;

            ValidateDistributionCode(ratePlanDto);

            if (Notification.HasNotification()) return;

            if (ratePlanDto.AgreementTypeId.Equals((int)AgreementTypeEnum.BusinessSale))
                ValidateClient(ratePlanDto.RatePlanCompanyClientList);

            if (Notification.HasNotification()) return;

            var ratePlanId = RatePlanDomainService.InsertAndSaveChanges(ratePlanBuilder).Id;

            ratePlanDto.Id = ratePlanId;

            if (Notification.HasNotification()) return;


            if (ratePlanDto.RateTypeId == (int)RatePlanTypeEnum.BaseRate)
            {
                if (ratePlanDto.RatePlanRoomTypeList != null && ratePlanDto.RatePlanRoomTypeList.Count() > 0)
                {
                    foreach (var ratePlanRoomType in ratePlanDto.RatePlanRoomTypeList)
                        ratePlanRoomType.RatePlanId = ratePlanId;

                    _ratePlanRoomTypeAppService.RatePlanRoomTypeCreate(ratePlanDto.RatePlanRoomTypeList.ToList());
                }
                else NotifyNullParameter();
            }

            if (Notification.HasNotification()) return;

            if (ratePlanDto.AgreementTypeId == (int)AgreementTypeEnum.BusinessSale)
            {

                if (ratePlanDto.RatePlanCompanyClientList != null && ratePlanDto.RatePlanCompanyClientList.Count() > 0)
                {
                    foreach (var ratePlanCompanyClient in ratePlanDto.RatePlanCompanyClientList)
                        ratePlanCompanyClient.RatePlanId = ratePlanId;

                    _ratePlanCompanyClientAppService.RatePlanCompanyClientCreate(ratePlanDto.RatePlanCompanyClientList.ToList());
                }
                else NotifyNullParameter();

                if (Notification.HasNotification()) return;

                if (ratePlanDto.RatePlanCommissionList != null && ratePlanDto.RatePlanCommissionList.Count() > 0)
                {
                    foreach (var ratePlanCommission in ratePlanDto.RatePlanCommissionList)
                        ratePlanCommission.RatePlanId = ratePlanId;

                    _ratePlanCommissionAppService.RatePlanCommissionCreate(ratePlanDto.RatePlanCommissionList.ToList());
                }
            }
        }

        private void ValidateClient(IList<RatePlanCompanyClientDto> ratePlanCompanyClientList)
        {
            if (ratePlanCompanyClientList.Count == 0)
                Notification.Raise(Notification.DefaultBuilder
                             .WithMessage(AppConsts.LocalizationSourceName, RatePlan.EntityError.RatePlanMustHaveCompanyClient)
                             .WithDetailedMessage(AppConsts.LocalizationSourceName, RatePlan.EntityError.RatePlanMustHaveCompanyClient)
                             .Build());
        }

        private void ValidateFields(RatePlanDto ratePlanDto)
        {
            if (ratePlanDto.AgreementTypeId == 0)
                Notification.Raise(Notification.DefaultBuilder
                             .WithMessage(AppConsts.LocalizationSourceName, RatePlan.EntityError.RatePlanMustHaveAgreementTypeId)
                             .WithDetailedMessage(AppConsts.LocalizationSourceName, RatePlan.EntityError.RatePlanMustHaveAgreementTypeId)
                             .Build());

            if (string.IsNullOrEmpty(ratePlanDto.AgreementName))
                Notification.Raise(Notification.DefaultBuilder
                             .WithMessage(AppConsts.LocalizationSourceName, RatePlan.EntityError.RatePlanMustHaveAgreementName)
                             .WithDetailedMessage(AppConsts.LocalizationSourceName, RatePlan.EntityError.RatePlanMustHaveAgreementName)
                             .Build());

            if (ratePlanDto.StartDate == DateTime.MinValue)
                Notification.Raise(Notification.DefaultBuilder
                             .WithMessage(AppConsts.LocalizationSourceName, RatePlan.EntityError.RatePlanInvalidStartDate)
                             .WithDetailedMessage(AppConsts.LocalizationSourceName, RatePlan.EntityError.RatePlanInvalidStartDate)
                             .Build());


            if (!ratePlanDto.EndDate.HasValue)
                Notification.Raise(Notification.DefaultBuilder
                             .WithMessage(AppConsts.LocalizationSourceName, RatePlan.EntityError.RatePlanInvalidEndDate)
                             .WithDetailedMessage(AppConsts.LocalizationSourceName, RatePlan.EntityError.RatePlanInvalidEndDate)
                             .Build());

            if (ratePlanDto.CurrencyId == Guid.Empty)
                Notification.Raise(Notification.DefaultBuilder
                             .WithMessage(AppConsts.LocalizationSourceName, RatePlan.EntityError.RatePlanMustHaveCurrencyId)
                             .WithDetailedMessage(AppConsts.LocalizationSourceName, RatePlan.EntityError.RatePlanMustHaveCurrencyId)
                             .Build());

            if (ratePlanDto.MealPlanTypeId == 0)
                Notification.Raise(Notification.DefaultBuilder
                             .WithMessage(AppConsts.LocalizationSourceName, RatePlan.EntityError.RatePlanMustHavePropertyMealPlanTypeId)
                             .WithDetailedMessage(AppConsts.LocalizationSourceName, RatePlan.EntityError.RatePlanMustHavePropertyMealPlanTypeId)
                             .Build());

            if(ratePlanDto.RateTypeId == (int)RatePlanTypeEnum.BaseRate)
            {
                if(ratePlanDto.RoundingTypeId.HasValue && !Enum.IsDefined(typeof(RoundingTypeEnum), ratePlanDto.RoundingTypeId.Value))
                {
                    Notification.Raise(Notification.DefaultBuilder
                            .WithMessage(AppConsts.LocalizationSourceName, RatePlan.EntityError.RatePlanRoundingTypeInvalid)
                            .WithDetailedMessage(AppConsts.LocalizationSourceName, RatePlan.EntityError.RatePlanRoundingTypeInvalid)
                            .Build());
                }
            }
        }

        public void RatePlanUpdate(RatePlanDto ratePlanDto)
        {
            ValidateDto<RatePlanDto>(ratePlanDto, nameof(ratePlanDto));

            if (ratePlanDto.MarketSegmentId == default(int))
                ratePlanDto.MarketSegmentId = null;

            if (Notification.HasNotification()) return;

            ValidateFields(ratePlanDto);

            if (Notification.HasNotification()) return;

            if (ratePlanDto.Id == null || ratePlanDto.Id == Guid.Empty)
                NotifyIdIsMissing();

            if (Notification.HasNotification()) return;

            if (!ratePlanDto.EndDate.HasValue)
                ratePlanDto.EndDate = (DateTime)SqlDateTime.MaxValue;

            ValidateDate(ratePlanDto.StartDate, ratePlanDto.EndDate.Value);

            if (Notification.HasNotification()) return;

            VerifyNameAgreementSamePeriod(ratePlanDto.StartDate, ratePlanDto.EndDate.Value, ratePlanDto.AgreementName, ratePlanDto.Id, ratePlanDto.PropertyId);

            if (Notification.HasNotification()) return;

            ValidateDistributionCode(ratePlanDto);

            if (Notification.HasNotification()) return;

            if (ratePlanDto.AgreementTypeId.Equals((int)AgreementTypeEnum.BusinessSale))
                ValidateClient(ratePlanDto.RatePlanCompanyClientList);

            if (Notification.HasNotification()) return;

            var ratePlanExisting = _ratePlanReadRepository.GetRatePlanById(ratePlanDto.Id);
            if (!string.IsNullOrWhiteSpace(ratePlanExisting.DistributionCode))
                ratePlanDto.DistributionCode = ratePlanExisting.DistributionCode;

            var ratePlanBuilder = RatePlanAdapter.Map(ratePlanDto);
            var ratePlanNew = ratePlanBuilder.Build();

            var roomTypes = MapRatePlanRoomType(ratePlanDto.RatePlanRoomTypeList.ToList());
            var companyClients = MapRatePlanCompanyClient(ratePlanDto.RatePlanCompanyClientList.ToList());
            var commissions = MapRatePlanComission(ratePlanDto.RatePlanCommissionList.ToList());

            if ((int)AgreementTypeEnum.DirectSelling == ratePlanDto.AgreementTypeId)
            {
                companyClients = null;
                commissions = null;
            }

            if (Notification.HasNotification()) return;

            if (ratePlanExisting != null)
                _ratePlanRepository.RatePlanUpdate(ratePlanExisting, ratePlanNew, roomTypes, companyClients, commissions);
        }

        private List<RatePlanRoomType> MapRatePlanRoomType(List<RatePlanRoomTypeDto> dto)
        {

            var result = new List<RatePlanRoomType>();
            foreach (var roomtype in dto)
            {
                var builder = new RatePlanRoomTypeAdapter(Notification).Map(roomtype);
                result.Add(builder.Build());
            }
            return result;
        }
        private List<RatePlanCompanyClient> MapRatePlanCompanyClient(List<RatePlanCompanyClientDto> dto)
        {

            var result = new List<RatePlanCompanyClient>();
            foreach (var companyClient in dto)
            {
                var builder = new RatePlanCompanyClientAdapter(Notification).Map(companyClient);
                result.Add(builder.Build());
            }
            return result;
        }
        private List<RatePlanCommission> MapRatePlanComission(List<RatePlanCommissionDto> dto)
        {

            var result = new List<RatePlanCommission>();
            foreach (var commission in dto)
            {
                var builder = new RatePlanCommissionAdapter(Notification).Map(commission);
                result.Add(builder.Build());
            }
            return result;
        }

        private void RatePlanRoomType(IList<RatePlanRoomTypeDto> ratePlanRoomTypes)
        {
            foreach (var ratePlanRoomType in ratePlanRoomTypes)
            {
                ValidateDto<RatePlanRoomTypeDto>(ratePlanRoomType, nameof(ratePlanRoomType));

                if (Notification.HasNotification()) break;

                var ratePlanRoomTypeBuilder = _ratePlanRoomTypeAdapter.Map(ratePlanRoomType);
            }
        }

        public RatePlanDto GetRatePlanById(Guid id)
        {
            return _ratePlanReadRepository.RatePlanDtoRatePlanId(id);
        }

        public IListDto<RatePlanDto> GetAllRatePlanSearch(GetAllRatePlanDto request, int propertyId)
        {
            return _ratePlanReadRepository.GetAllRatePlanSearch(request, propertyId);
        }

        public void ToggleAndSaveIsActive(Guid id)
        {
            _ratePlanRepository.ToggleAndSaveIsActive(id);
        }

        private void VerifyNameAgreementSamePeriod(DateTime startDate, DateTime endDate, string agreementName, Guid ratePlanId, int propertyId)
        {
            if (_ratePlanReadRepository.VerifyNameAgreementSamePeriod(startDate, endDate, agreementName, ratePlanId, propertyId))
            {
                Notification.Raise(Notification.DefaultBuilder
               .WithMessage(AppConsts.LocalizationSourceName, RatePlan.EntityError.RatePlanNotNameAgreementSamePeriod)
               .WithDetailedMessage(AppConsts.LocalizationSourceName, RatePlan.EntityError.RatePlanNotNameAgreementSamePeriod)
               .Build());
            }
        }

        private void ValidateDate(DateTime startDate, DateTime endDate)
        {
            if (startDate.Date > endDate.Date)
            {
                Notification.Raise(Notification.DefaultBuilder
                .WithMessage(AppConsts.LocalizationSourceName, RatePlan.EntityError.RatePlanNotDateStartGreaterThanDateEnd)
                .WithDetailedMessage(AppConsts.LocalizationSourceName, RatePlan.EntityError.RatePlanNotDateStartGreaterThanDateEnd)
                .Build());
            }
        }

        private void ValidateStartDateGreaterCurrentDate(DateTime startDate)
        {
            var systemDate = _propertyParameterReadRepository.GetSystemDate(int.Parse(_applicationUser.PropertyId));

            if (systemDate == null)
            {
                Notification.Raise(Notification.DefaultBuilder
                .WithMessage(AppConsts.LocalizationSourceName, PropertyAuditProcess.EntityError.PropertyAuditProcessInvalidPropertySystemDate)
                .WithDetailedMessage(AppConsts.LocalizationSourceName, PropertyAuditProcess.EntityError.PropertyAuditProcessInvalidPropertySystemDate)
                .Build());

                return;
            }

            if (startDate.Date < systemDate.Value.Date)
                Notification.Raise(Notification.DefaultBuilder
                                .WithMessage(AppConsts.LocalizationSourceName, RatePlan.EntityError.RatePlanStartDateNotMustBeLessCurrentDate)
                                .WithDetailedMessage(AppConsts.LocalizationSourceName, RatePlan.EntityError.RatePlanStartDateNotMustBeLessCurrentDate)
                                .Build());
        }

        private void ValidateDistributionCode(RatePlanDto ratePlanDto)
        {
            if(ratePlanDto.PublishOnline && string.IsNullOrWhiteSpace(ratePlanDto.DistributionCode))
            {
                Notification.Raise(Notification.DefaultBuilder
                 .WithMessage(AppConsts.LocalizationSourceName, RatePlan.EntityError.RatePlanMustHaveDistributionCode)
                 .WithDetailedMessage(AppConsts.LocalizationSourceName, RatePlan.EntityError.RatePlanMustHaveDistributionCode)
                 .Build());

                return;
            }

            if(!string.IsNullOrWhiteSpace(ratePlanDto.DistributionCode))
            {
                var distributionCodeAvailable = _ratePlanReadRepository.DistributionCodeAvailable(ratePlanDto.DistributionCode, int.Parse(_applicationUser.PropertyId), ratePlanDto.Id);
                if (!distributionCodeAvailable)
                {
                    Notification.Raise(Notification.DefaultBuilder
                      .WithMessage(AppConsts.LocalizationSourceName, RatePlan.EntityError.AlreadyExistsRatePlanWithSameDistributionCode)
                      .WithDetailedMessage(AppConsts.LocalizationSourceName, RatePlan.EntityError.AlreadyExistsRatePlanWithSameDistributionCode)
                      .Build());
                }
            }
        }

        public IListDto<PropertyCompanyClientCategoryDto> GetAllCompanyClientCategoryByPropertyId(GetAllPropertyCompanyClientCategoryDto request, int propertyId)
        {
            return _propertyCompanyClientCategoryReadRepository.GetAllByPropertyId(request, propertyId);
        }

        public async Task<ListDto<PropertyBaseRateHeaderHistoryDto>> GetRatePlanHistoryById(Guid id)
        {
            var resultList = new List<PropertyBaseRateHeaderHistoryDto>();

            var ratePlanList = await _ratePlanReadRepository.GetRatePlanHistoryById(id);

            foreach (var item in ratePlanList.Items)
            {
                var v = item.MapTo<PropertyBaseRateHeaderHistoryDto>();
                resultList.Add(v);
            }

            return new ListDto<PropertyBaseRateHeaderHistoryDto>
            {
                HasNext = false,
                Items = resultList
            };
        }

        public async Task<ListDto<PropertyBaseRateHeaderHistoryDto>> GetRatePlanHistoryByDate(Guid id, DateTime dateTimeHistory)
        {
            var resultList = new List<PropertyBaseRateHeaderHistoryDto>();

            var ratePlanListTask = _ratePlanReadRepository.GetRatePlanHistoryByDate(id, dateTimeHistory);

            var ratePlanList = await ratePlanListTask;

            foreach (var item in ratePlanList)
            {
                resultList.Add(item.MapTo<PropertyBaseRateHeaderHistoryDto>());
            }

            return new ListDto<PropertyBaseRateHeaderHistoryDto>
            {
                HasNext = false,
                Items = resultList
            };
        }

        public async Task<ListDto<PropertyBaseRateHeaderHistoryDto>> GetRatePlanHistoryByHistoryId(Guid id)
        {
            var resultList = new List<PropertyBaseRateHeaderHistoryDto>();
            var ratePlanList = await _ratePlanReadRepository.GetRatePlanHistoryByHistoryId(id);
            foreach (var item in ratePlanList.Items)
            {
                var v = item.MapTo<PropertyBaseRateHeaderHistoryDto>();
                resultList.Add(v);
            }
            return new ListDto<PropertyBaseRateHeaderHistoryDto>
            {
                HasNext = false,
                Items = resultList
            };
        }

        public async Task<ListDto<RatePlanDto>> GetAllBaseRateByPeriodAndPropertyId(DateTime startDate, DateTime endDate, int propertyId)
        {
            var resultList = new List<RatePlanDto>();

            var ratePlanList = await _ratePlanReadRepository.GetAllBaseRateByPeriodAndPropertyId(startDate, endDate, propertyId);

            foreach (var item in ratePlanList)
            {
                resultList.Add(item.MapTo<RatePlanDto>());
            }

            return new ListDto<RatePlanDto>
            {
                HasNext = false,
                Items = resultList
            };
        }
    }
}
