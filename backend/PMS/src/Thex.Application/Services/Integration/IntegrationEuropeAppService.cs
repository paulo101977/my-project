﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.Application.Interfaces;
using Thex.Common.Enumerations;
using Thex.Domain.Interfaces.Integration;
using Thex.Dto;
using Thex.Dto.BillingAccountClosure;
using Tnf.Notifications;

namespace Thex.Application.Services
{
    public class IntegrationEuropeAppService : ApplicationServiceBase, IIntegrationEuropeAppService
    {
        private readonly IInvoiceIntegrationEurope _invoiceIntegrationEurope;

        public IntegrationEuropeAppService(
            IInvoiceIntegrationEurope invoiceIntegrationEurope,
            INotificationHandler notificationHandler)
            : base(notificationHandler)
        {
            _invoiceIntegrationEurope = invoiceIntegrationEurope;
        }

        public async Task<NotificationResponseDto> GenerateCreditNote(BillingInvoiceTypeEnum invoiceType, Guid billingInvoiceId)
        {
            return await _invoiceIntegrationEurope.GenerateInvoice(new List<BillingAccountClosureInvoiceDto>
            { new BillingAccountClosureInvoiceDto()
                  {
                        InvoiceId = billingInvoiceId.ToString(),
                        Type = invoiceType
                  }
            }, invoiceType, null);
        }

        public async Task<NotificationResponseDto> GenerateInvoice(List<BillingAccountClosureInvoiceDto> billingInvoiceList, BillingInvoiceTypeEnum invoiceType, BillingAccountItemCreditDto payment = null)
        {
            return await _invoiceIntegrationEurope.GenerateInvoice(billingInvoiceList, invoiceType, payment);
        }
        public async Task<string> GetIntegratorIdById(Guid id)
        {
            return await _invoiceIntegrationEurope.GetIntegratorIdById(id);
        }
    }
}
