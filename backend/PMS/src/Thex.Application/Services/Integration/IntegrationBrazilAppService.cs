﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Polly;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Thex.Application.Interfaces;
using Thex.AspNetCore.Security;
using Thex.Common.Dto.InvoiceIntegration;
using Thex.Common.Enumerations;
using Thex.Common.Helpers;
using Thex.Domain.Interfaces.Factories;
using Thex.Domain.Interfaces.Integration;
using Thex.Dto;
using Thex.Dto.BillingAccountClosure;
using Thex.Dto.Integration;
using Thex.GenericLog;
using Thex.Infra.ReadInterfaces;
using Thex.Kernel;
using Tnf.Localization;
using Tnf.Notifications;
using Tnf.Repositories.Uow;

namespace Thex.Application.Services
{
    public class IntegrationBrazilAppService : ApplicationServiceBase, IIntegrationBrazilAppService
    {
        private readonly string _enotasIntegrationIndexName = "enotas-log";
        private readonly IInvoiceIntegrationBrazil _invoiceIntegrationBrazil;
        private readonly IConfiguration _configuration;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IPropertyParameterReadRepository _propertyParameter;
        private readonly ILocalizationManager _localizationManager;
        private readonly IApplicationUser _applicationUser;
        private readonly IGenericLogHandler _genericLog;
        private readonly INotificationHandler _notificationHandler;

        public IntegrationBrazilAppService(
            IUnitOfWorkManager unitOfWorkManager,
            IInvoiceIntegrationBrazil invoiceIntegrationBrazil,
            IConfiguration configuration,
            INotificationHandler notificationHandler,
            IHttpContextAccessor httpContextAccessor,
            IPropertyParameterReadRepository propertyParameter,
            IEmailFactory emailFactory,
            ILocalizationManager localizationManager,
            IApplicationUser applicationUser,
            IGenericLogHandler genericLog)
            : base(notificationHandler)
        {
            _unitOfWorkManager = unitOfWorkManager;
            _invoiceIntegrationBrazil = invoiceIntegrationBrazil;
            _configuration = configuration;
            _httpContextAccessor = httpContextAccessor;
            _propertyParameter = propertyParameter;
            _localizationManager = localizationManager;
            _applicationUser = applicationUser;
            _genericLog = genericLog;
            _notificationHandler = notificationHandler;
        }

        public async Task<NotificationResponseDto> GenerateInvoice(List<BillingAccountClosureInvoiceDto> billingInvoiceList, BillingInvoiceTypeEnum invoiceType)
        {
            return await _invoiceIntegrationBrazil.GenerateInvoice(billingInvoiceList, invoiceType);
        }

        private async Task SendInvoiceIntegratioResponse(BillingInvoiceIntegrationResponseDto response, InvoiceIntegrationResponseDto invoiceResponse)
        {
            await _invoiceIntegrationBrazil.SendInvoiceIntegratioResponse(response, invoiceResponse);
        }

        public async Task<BillingInvoiceIntegrationResponseDto> UpdateStatus(InvoiceIntegrationResponseDto invoiceResponsetDto)
        {
            BillingInvoiceIntegrationResponseDto ret = null;

            try
            {
                // ajuste das resposta do enotas
                if (!string.IsNullOrEmpty(invoiceResponsetDto.InternalId) && string.IsNullOrEmpty(invoiceResponsetDto.ExternalId))
                    invoiceResponsetDto.ExternalId = invoiceResponsetDto.InternalId;

                switch (EnumHelper.GetEnumValue<InvoiceIntegrationEnum>(invoiceResponsetDto.Status, true))
                {
                    case InvoiceIntegrationEnum.Authorized:
                        {
                            ret = await _invoiceIntegrationBrazil.UpdateWithSuccess(invoiceResponsetDto);
                            break;
                        }
                    case InvoiceIntegrationEnum.Denied:
                        {
                            ret = await _invoiceIntegrationBrazil.UpdateWithError(invoiceResponsetDto);
                            break;
                        }
                    case InvoiceIntegrationEnum.Canceled:
                        {
                            ret = await _invoiceIntegrationBrazil.UpdateWithCanceled(invoiceResponsetDto);
                            break;
                        }
                    case InvoiceIntegrationEnum.CancelDenied:
                        {
                            ret = await _invoiceIntegrationBrazil.UpdateWithError(invoiceResponsetDto);
                            break;
                        }
                    default:
                        {
                            NotifyNullParameter();
                            return BillingInvoiceIntegrationResponseDto.NullInstance;
                        }
                }

                await SendInvoiceIntegratioResponse(ret, invoiceResponsetDto);
            }
            finally
            {
                if (_genericLog.HasGenericLog())
                    _genericLog.Commit(_enotasIntegrationIndexName);
            }

            return ret;
        }

        public async Task<string> DownloadSetupUrl(string propertyId)
        {
            var details = await _invoiceIntegrationBrazil.GetIntegrationDetails(propertyId);

            if (_notificationHandler.HasNotification())
                return null;


            return await GetSetupFile($"{_configuration.GetValue<string>("BaseEndPoint")}/v{_configuration.GetValue<string>("VersaoProduct")}/empresas/" +
                                      $"{details.FirstOrDefault().Value}/sat/setup", details.FirstOrDefault().Key);
        }

        public async Task<Stream> Download(SearchBillingInvoiceDto requestDto, int propertyId)
        {
            if (_notificationHandler.HasNotification())
                return null;

            var invoiceList = await _invoiceIntegrationBrazil.GetAllInvoicesByFilters(requestDto, propertyId);

            if (requestDto.Xml)
                return await GetFiles(invoiceList, requestDto.Xml);

            if (requestDto.Pdf)
                return await GetFiles(invoiceList, requestDto.Pdf);

            return null;
        }

        private async Task<string> GetSetupFile(string url, string apiKey)
        {
            using (var httpClient = new HttpClient())
            {
                httpClient.DefaultRequestHeaders.Clear();
                httpClient.DefaultRequestHeaders.Add("Authorization", "Basic " + HashManager.GenerateMD5Decrypto(apiKey));
                httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                var response = await Policy
                                     .HandleResult<HttpResponseMessage>(message => !message.IsSuccessStatusCode)
                                     .WaitAndRetryAsync(1, i => TimeSpan.FromSeconds(60), (result, timeSpan, retryCount, context) => { })
                                     .ExecuteAsync(() => httpClient.GetAsync(url));


                if (response.IsSuccessStatusCode)
                {
                    return await response.Content.ReadAsStringAsync();
                }

            }

            return await Task.FromResult(string.Empty);
        }

        private async Task<Stream> GetFiles(List<InvoiceDto> invoiceList, bool xml = false, bool pdf = false)
        {
            var zipStream = new MemoryStream();

            using (var zip = new ZipArchive(zipStream, ZipArchiveMode.Create, true))
            {
                foreach (var invoice in invoiceList)
                {
                    HttpResponseMessage response = null;

                    if (xml)
                        response = await Policy
                                        .HandleResult<HttpResponseMessage>(message => !message.IsSuccessStatusCode)
                                        .WaitAndRetryAsync(1, i => TimeSpan.FromSeconds(60), (result, timeSpan, retryCount, context) => { })
                                        .ExecuteAsync(() => new HttpClient().GetAsync(invoice.IntegratorXml));
                    else if (pdf)
                        response = await Policy
                                        .HandleResult<HttpResponseMessage>(message => !message.IsSuccessStatusCode)
                                        .WaitAndRetryAsync(1, i => TimeSpan.FromSeconds(60), (result, timeSpan, retryCount, context) => { })
                                        .ExecuteAsync(() => new HttpClient().GetAsync(invoice.IntegratorLink));

                    if (response.IsSuccessStatusCode)
                    {
                        var stream = response.Content.ReadAsStreamAsync();

                        var entry = zip.CreateEntry(string.Concat(invoice.ExternalSeries, "-",
                                                                  invoice.ExternalNumber, xml ? ".xml" : pdf ? ".pdf" : ".xml"));
                        using (var entryStream = entry.Open())
                        {
                            stream.Result.CopyTo(entryStream);
                        }
                    }
                }
            }

            zipStream.Position = 0;
            return zipStream;
        }
    }
}