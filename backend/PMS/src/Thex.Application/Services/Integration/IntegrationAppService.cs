﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Thex.Application.Interfaces;
using Thex.Common.Dto.InvoiceIntegration;
using Thex.Common.Enumerations;
using Thex.Common.Helpers;
using Thex.Domain.Interfaces.Factories;
using Thex.Domain.Interfaces.Repositories;
using Thex.Dto;
using Thex.Dto.BillingAccountClosure;
using Thex.Dto.Integration;
using Thex.Infra.ReadInterfaces;
using Thex.Kernel;
using Tnf.Localization;
using Tnf.Notifications;
using Tnf.Repositories.Uow;

namespace Thex.Application.Services
{
    public class IntegrationAppService : ApplicationServiceBase, IIntegrationAppService
    {
        private readonly IConfiguration _configuration;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IPropertyParameterReadRepository _propertyParameter;
        private readonly ILocalizationManager _localizationManager;
        private readonly IEmailFactory _emailFactory;
        private readonly IApplicationUser _applicationUser;
        private readonly IIntegrationBrazilAppService _integrationBrazilAppService;
        private readonly IIntegrationEuropeAppService _integrationEuropeAppService;
        private readonly INotificationHandler _notificationHandler;
        private readonly IBillingInvoiceReadRepository _billingInvoiceReadRepository;
        private readonly IBillingAccountRepository _billingAccountRepository;

        public IntegrationAppService(
            IUnitOfWorkManager unitOfWorkManager,
            IConfiguration configuration,
            INotificationHandler notificationHandler,
            IHttpContextAccessor httpContextAccessor,
            IPropertyParameterReadRepository propertyParameter,
            IEmailFactory emailFactory,
            ILocalizationManager localizationManager,
            IApplicationUser applicationUser,
            IIntegrationBrazilAppService integrationBrazilAppService,
            IIntegrationEuropeAppService integrationEuropeAppService,
            IBillingInvoiceReadRepository billingInvoiceReadRepository,
            IBillingAccountRepository billingAccountRepository)
            : base(notificationHandler)
        {
            _unitOfWorkManager = unitOfWorkManager;
            _configuration = configuration;
            _httpContextAccessor = httpContextAccessor;
            _propertyParameter = propertyParameter;
            _localizationManager = localizationManager;
            _emailFactory = emailFactory;
            _applicationUser = applicationUser;
            _integrationBrazilAppService = integrationBrazilAppService;
            _integrationEuropeAppService = integrationEuropeAppService;
            _notificationHandler = notificationHandler;
            _billingInvoiceReadRepository = billingInvoiceReadRepository;
            _billingAccountRepository = billingAccountRepository;
        }

        public async Task<string> GetIntegratorIdById(Guid id)
        {
            switch (EnumHelper.GetEnumValue<CountryIsoCodeEnum>(_applicationUser.PropertyCountryCode, true))
            {
                case CountryIsoCodeEnum.Europe:
                    return await _integrationEuropeAppService.GetIntegratorIdById(id);
                case CountryIsoCodeEnum.Brasil:
                case CountryIsoCodeEnum.Latam:
                default:
                    NotifyInvalidLocation();
                    return null;
            }
        }

        public async Task<string> DownloadSetupUrl(string propertyId)
        {
            switch (EnumHelper.GetEnumValue<CountryIsoCodeEnum>(_applicationUser.PropertyCountryCode, true))
            {
                case CountryIsoCodeEnum.Brasil:
                    {
                        return await _integrationBrazilAppService.DownloadSetupUrl(propertyId);
                    }
                case CountryIsoCodeEnum.Europe:
                case CountryIsoCodeEnum.Latam:
                default:
                    NotifyInvalidLocation();
                    return null;
            }
        }

        public async Task<Stream> Download(SearchBillingInvoiceDto requestDto, int propertyId)
        {
            switch (EnumHelper.GetEnumValue<CountryIsoCodeEnum>(_applicationUser.PropertyCountryCode, true))
            {
                case CountryIsoCodeEnum.Brasil:
                    {
                        return await _integrationBrazilAppService.Download(requestDto, propertyId);
                    }
                case CountryIsoCodeEnum.Europe:
                    NotifyInvalidLocation();
                    return null;
                default:
                    NotifyEmptyFilter();
                    return null;
            }
        }

        public async Task<NotificationResponseDto> GenerateCreditNote(BillingInvoiceTypeEnum invoiceType, Guid billingInvoiceId)
        {
            NotificationResponseDto responseDto = null;

            switch (EnumHelper.GetEnumValue<CountryIsoCodeEnum>(_applicationUser.PropertyCountryCode, true))
            {
                case CountryIsoCodeEnum.Europe:
                    responseDto = await _integrationEuropeAppService.GenerateCreditNote(invoiceType, billingInvoiceId);
                    break;
                case CountryIsoCodeEnum.Brasil:
                default:
                    NotifyInvalidLocation();
                    break;
            }

            return responseDto;
        }

        public async Task<NotificationResponseDto> GenerateInvoice(List<BillingAccountClosureInvoiceDto> billingInvoiceList, BillingInvoiceTypeEnum invoiceType, BillingAccountItemCreditDto payment = null)
        {
            NotificationResponseDto responseDto = null;

            using (var uow = _unitOfWorkManager.Begin())
            {
                if (billingInvoiceList == null || billingInvoiceList.Count == 0)
                    NotifyParameterInvalid();

                switch (EnumHelper.GetEnumValue<CountryIsoCodeEnum>(_applicationUser.PropertyCountryCode, true))
                {
                    case CountryIsoCodeEnum.Brasil:
                        responseDto = await _integrationBrazilAppService.GenerateInvoice(billingInvoiceList, invoiceType);
                        break;
                    case CountryIsoCodeEnum.Europe:
                        responseDto = await _integrationEuropeAppService.GenerateInvoice(billingInvoiceList, invoiceType, payment);
                        break;
                    default:
                        NotifyNullParameter();
                        break;
                }

                uow.Complete();
            }

            return responseDto;
        }

        public async Task<NotificationResponseDto> ResendInvoice(List<BillingAccountClosureInvoiceDto> billingInvoiceList)
        {
            NotificationResponseDto responseDto = null;

            using (var uow = _unitOfWorkManager.Begin())
            {
                foreach (var resendInvoice in billingInvoiceList)
                {
                    var databaseInvoice = _billingInvoiceReadRepository.GetDtoById(Guid.Parse(resendInvoice.InvoiceId));

                    if (databaseInvoice == null)
                        return NotificationResponseDto.NullInstance;

                    responseDto = await GenerateInvoice(new List<BillingAccountClosureInvoiceDto>() { new BillingAccountClosureInvoiceDto()
                    {
                        InvoiceId = databaseInvoice.Id.ToString(),
                        Type =  (BillingInvoiceTypeEnum)databaseInvoice.BillingInvoiceTypeId
                    }},
                    (BillingInvoiceTypeEnum)databaseInvoice.BillingInvoiceTypeId, null);

                    if (responseDto != null)
                    {
                        var notify = JsonConvert.DeserializeObject<NotificationEvent>(JsonConvert.SerializeObject(responseDto));
                        _notificationHandler.Raise(notify);

                        return NotificationResponseDto.NullInstance;
                    }
                    else
                        _billingAccountRepository.CloseBillingAccount(databaseInvoice.BillingAccountId);
                }

                uow.Complete();
            }

            return responseDto;
        }

        public async Task<BillingInvoiceIntegrationResponseDto> UpdateStatus(InvoiceIntegrationResponseDto invoiceResponsetDto)
        {
            BillingInvoiceIntegrationResponseDto responseDto = null;

            using (var uow = _unitOfWorkManager.Begin())
            {
                responseDto = await _integrationBrazilAppService.UpdateStatus(invoiceResponsetDto);

                uow.Complete();
            }

            return responseDto;
        }
    }
}
