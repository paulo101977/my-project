﻿using System;
using Thex.Application.Adapters;
using Thex.Domain.Entities;
using Thex.Application.Interfaces;
using Tnf.Domain.Services;
using Thex.Infra.ReadInterfaces;
using System.Collections.Generic;
using Thex.Dto;
using Tnf.Dto;
using Thex.Dto.BillingItemCategory;
using Thex.Domain.Interfaces.Repositories;
using Tnf.Notifications;

namespace Thex.Application.Services
{
    public class BillingItemCategoryAppService : ScaffoldBillingItemCategoryAppService, IBillingItemCategoryAppService
    {
        private readonly IBillingItemCategoryReadRepository _billingItemCategoryRead;
        private readonly IDomainService<BillingItemCategory> _billingItemCategoryDomainService;
        
        private readonly IBillingItemCategoryRepository _billingItemCategoryRepository;
        public BillingItemCategoryAppService(
            IBillingItemCategoryAdapter billingItemCategoryAdapter,
            IBillingItemCategoryReadRepository billingItemCategoryRead,
            IDomainService<BillingItemCategory> billingItemCategoryDomainService,
            IBillingItemCategoryRepository billingItemCategoryRepository,
            INotificationHandler notificationHandler)
            : base(billingItemCategoryAdapter, billingItemCategoryDomainService, notificationHandler)
        {
            _billingItemCategoryRead = billingItemCategoryRead;
            _billingItemCategoryRepository = billingItemCategoryRepository;
        }

        public IListDto<BillingItemCategoryDto> GetAllGroupFixed()
        {
            return _billingItemCategoryRead.GetAllGroupFixed();
        }

        public IListDto<BillingItemCategoryDto> GetItemsByGroupId(int groupId)
        {
            return _billingItemCategoryRead.GetItemsByGroupId(groupId);
        }

        public IListDto<BillingItemCategoryDto> GetAllGroupByPropertyId(GetAllBillingItemCategoryDto request, int propertyId)
        {
            return _billingItemCategoryRead.GetAllGroupByPropertyId(request, propertyId);
        }

        public IListDto<GetAllCategoriesForItemDto> GetAllCategoriesForItem(int propertyId)
        {
            return _billingItemCategoryRead.GetAllCategoriesForItem(propertyId);
        }

        public void ToggleActivation(int id)
        {
            _billingItemCategoryRepository.ToggleAndSaveIsActive(id);
        }

        public void DeleteBillingCategory(int id)
        {
            _billingItemCategoryRepository.BillingItemCategoryDelete(id);
        }
    }
}
