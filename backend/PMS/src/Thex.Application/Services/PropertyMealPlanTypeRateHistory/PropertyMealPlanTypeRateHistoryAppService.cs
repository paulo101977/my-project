﻿using System;
using Thex.Application.Adapters;
using Thex.Domain.Entities;
using Thex.Application.Interfaces;
using Tnf.Domain.Services;
using Thex.Dto;
using System.Threading.Tasks;
using Thex.Infra.ReadInterfaces;
using Tnf.Dto;
using Tnf.Notifications;
using Thex.Kernel;
using Thex.Domain.Interfaces.Repositories;

namespace Thex.Application.Services
{
    public class PropertyMealPlanTypeRateHistoryAppService : ScaffoldPropertyMealPlanTypeRateHistoryAppService, IPropertyMealPlanTypeRateHistoryAppService
    {
        public PropertyMealPlanTypeRateHistoryAppService(
            IPropertyMealPlanTypeRateHistoryAdapter propertyMealPlanTypeRateHistoryAdapter,
            IDomainService<PropertyMealPlanTypeRateHistory> propertyMealPlanTypeRateHistoryDomainService,
            INotificationHandler notificationHandler)
                : base(propertyMealPlanTypeRateHistoryAdapter, propertyMealPlanTypeRateHistoryDomainService, notificationHandler)
        {
        }
    }
}
