﻿using System;
using System.Collections.Generic;
using System.Linq;
using Thex.Application.Adapters;
using Thex.Application.Interfaces;
using Thex.Common;
using Thex.Domain.Entities;
using Thex.Domain.Interfaces.Repositories;
using Thex.Dto;
using Thex.Infra.ReadInterfaces;
using Tnf.Domain.Services;
using Tnf.Notifications;

namespace Thex.Application.Services
{
    public class GuestRegistrationDocumentAppService : ScaffoldGuestRegistrationDocumentAppService, IGuestRegistrationDocumentAppService
    {
        private readonly IGuestRegistrationDocumentRepository _guestRegistrationDocumentRepository;
        private readonly IGuestRegistrationDocumentReadRepository _guestRegistrationDocumentReadRepository;

        public GuestRegistrationDocumentAppService
            (IGuestRegistrationDocumentAdapter guestRegistrationDocumentAdapter,
            IDomainService<GuestRegistrationDocument> guestRegistrationDocumentDomainService,
            IGuestRegistrationDocumentRepository guestRegistrationDocumentRepository,
            IGuestRegistrationDocumentReadRepository guestRegistrationDocumentReadRepository,
            INotificationHandler notificationHandler)
            : base(guestRegistrationDocumentAdapter,
                  guestRegistrationDocumentDomainService,
                  notificationHandler)
        {
            _guestRegistrationDocumentRepository = guestRegistrationDocumentRepository;
            _guestRegistrationDocumentReadRepository = guestRegistrationDocumentReadRepository;
        }

        public void CreateOrUpdateGuestRegistrationDocument(ICollection<GuestRegistrationDocumentDto> documentDtoList, Guid guestRegistrationId)
        {
            ValidateDocumentList(documentDtoList);

            if (Notification.HasNotification())
                return;

            var documentDtoListToInsert = new List<GuestRegistrationDocument>();
            var documentDtoListToUpdate = new List<GuestRegistrationDocument>();
            var guestRegistrationDocumentList = _guestRegistrationDocumentReadRepository.GetAllGuestRegistrationDocumentByGuestRegistrationId(guestRegistrationId);
            var documentListToRemove = guestRegistrationDocumentList
                                                    .Where(grdi => !documentDtoList
                                                    .Select(grd => grd.Id)
                                                    .Contains(grdi.Id))
                                                    .ToList();

            foreach (var documentDto in documentDtoList)
            {
                if (documentDto.Id == Guid.Empty)
                {
                    documentDto.GuestRegistrationId = guestRegistrationId;
                    var document = GuestRegistrationDocumentAdapter.Map(documentDto).Build();
                    documentDtoListToInsert.Add(document);
                }
                else
                {
                    var document = _guestRegistrationDocumentReadRepository.GetById(documentDto.Id);
                    documentDtoListToUpdate.Add(GuestRegistrationDocumentAdapter.Map(document, documentDto).Build());
                }
                    
            }

            if (Notification.HasNotification())
                return;

            if (documentDtoListToInsert.Any())
                _guestRegistrationDocumentRepository.AddRange(documentDtoListToInsert);

            if (documentDtoListToUpdate.Any())
                _guestRegistrationDocumentRepository.UpdateRange(documentDtoListToUpdate);

            if (documentListToRemove.Any())
                _guestRegistrationDocumentRepository.RemoveRange(documentListToRemove);
        }

        public ICollection<GuestRegistrationDocumentDto> GetAllByGuestRegistrationId(Guid guestRegistrationId)
            => _guestRegistrationDocumentReadRepository.GetAllDocumentsByGuestRegistrationId(guestRegistrationId);

        #region Private Methods
        private void ValidateDocumentList(ICollection<GuestRegistrationDocumentDto> documentDtoList)
        {
            if (documentDtoList
                .GroupBy(d => d.DocumentTypeId)
                .Any(d =>  d.Count() > 1))
            {
                Notification.Raise(Notification.DefaultBuilder
                   .WithMessage(AppConsts.LocalizationSourceName, GuestRegistrationDocument.EntityError.GuestRegistrationDocumentListHasDuplicates)
                   .WithDetailedMessage(AppConsts.LocalizationSourceName, GuestRegistrationDocument.EntityError.GuestRegistrationDocumentListHasDuplicates)
                   .Build());
            }
        }
        #endregion
    }
}
