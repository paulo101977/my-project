﻿using System;
using Thex.Application.Adapters;
using Thex.Domain.Entities;
using Thex.Application.Interfaces;
using Tnf.Domain.Services;
using Tnf.Notifications;

namespace Thex.Application.Services
{
    public class OccupationAppService : ScaffoldOccupationAppService, IOccupationAppService
    {
        public OccupationAppService(
            IOccupationAdapter occupationAdapter,
            IDomainService<Occupation> occupationDomainService,
            INotificationHandler notificationHandler)
            : base(occupationAdapter, occupationDomainService, notificationHandler)
        {
        }
    }
}
