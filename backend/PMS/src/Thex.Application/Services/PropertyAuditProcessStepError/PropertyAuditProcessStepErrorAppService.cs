﻿using System;
using Thex.Application.Adapters;
using Thex.Domain.Entities;
using Thex.Application.Interfaces;
using Tnf.Domain.Services;
using Tnf.Notifications;

namespace Thex.Application.Services
{
    public class PropertyAuditProcessStepErrorAppService : ScaffoldPropertyAuditProcessStepErrorAppService, IPropertyAuditProcessStepErrorAppService
    {
        public PropertyAuditProcessStepErrorAppService(
            IPropertyAuditProcessStepErrorAdapter propertyAuditProcessStepErrorAdapter,
            IDomainService<PropertyAuditProcessStepError> propertyAuditProcessStepErrorDomainService,
            INotificationHandler notificationHandler)
            : base(propertyAuditProcessStepErrorAdapter, propertyAuditProcessStepErrorDomainService, notificationHandler)
        {
        }
    }
}
