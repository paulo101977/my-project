﻿using System;
using Thex.Application.Adapters;
using Thex.Domain.Entities;
using Thex.Application.Interfaces;
using Tnf.Domain.Services;
using Thex.Dto;
using System.Collections.Generic;
using Thex.Common.Enumerations;
using Thex.Infra.ReadInterfaces;
using System.Linq;
using Tnf.Dto;

using Thex.Kernel;
using Thex.Common.Extensions;
using Thex.Dto.PropertyAuditProcessStep;
using Thex.Domain.Interfaces.Services;
using Thex.Common;
using Thex.EntityFrameworkCore.ReadInterfaces.Repositories;
using Tnf.Notifications;
using Thex.Infra.Repositories.Read;
using Tnf.Repositories.Uow;
using Thex.Common.Interfaces.Observables;
using Microsoft.Extensions.DependencyInjection;
using Thex.Inventory.Domain.Services;
using Thex.Common.Dto.Observable;
using Thex.Domain.Interfaces.Repositories;
using Thex.Dto.PropertyCurrencyExchange;
using Thex.Common.Helpers;

namespace Thex.Application.Services
{
    public class PropertyAuditProcessAppService : ScaffoldPropertyAuditProcessAppService, IPropertyAuditProcessAppService
    {
        private readonly IPropertyAuditProcessDomainService _propertyAuditProcessDomainService;
        private readonly PropertyParameterReadRepository _propertyParameterReadRepository;
        private readonly IPropertyAuditProcessStepReadRepository _propertyAuditProcessStepReadRepository;
        private readonly IReservationItemReadRepository _reservationItemReadRepository;
        private readonly IBillingAccountItemAppService _billingAccountItemAppService;
        private readonly IBillingAccountAppService _billingAccountAppService;
        private readonly IRoomBlockingAppService _roomBlockingAppService;
        private readonly IHousekeepingStatusPropertyAppService _housekeepingStatusPropertyAppService;
        private readonly IReservationItemDomainService _reservationItemDomainService;
        private readonly EntityFrameworkCore.ReadInterfaces.Repositories.IRoomReadRepository _roomReadRepository;
        private readonly IPropertyAuditProcessReadRepository _propertyAuditProcessReadRepository;
        private readonly IDomainService<PropertyAuditProcessStep> _propertyAuditProcessStepDomainService;
        private readonly IDomainService<PropertyAuditProcessStepError> _propertyAuditProcessStepErrorDomainService;
        private readonly IApplicationUser _applicationUser;
        private readonly IPropertyParameterDomainService _propertyParameterDomainService;
        private readonly IGuestReservationItemAppService _guestReservationItemAppService;
        private readonly IRoomTypeRepository _roomTypeRepository;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly ICompanyClientReadRepository _companyClientReadRepository;
        private readonly IPropertyCurrencyExchangeReadRepository _propertyCurrencyExchangeReadRepository;
        private readonly ITourismTaxAppService _tourismTaxAppService;
        private readonly IBillingAccountItemTransferAppService _billingAccountItemTransferAppService;

        private IList<IExecutePropertyAuditProcessObservable> _executePropertyAuditProcessObservableList;
        private IList<IReservationItemChangeStatusObservable> _reservationItemChangeStatusObservableList;
        private IList<IReservationItemChangeRoomTypeOrPeriodObservable> _reservationItemChangeRoomTypeOrPeriodObservableList;

        public PropertyAuditProcessAppService(
            IUnitOfWorkManager unitOfWorkManager,
            IPropertyAuditProcessAdapter propertyAuditProcessAdapter,
            IPropertyAuditProcessDomainService propertyAuditProcessDomainService,
            PropertyParameterReadRepository propertyParameterReadRepository,
            IPropertyAuditProcessStepReadRepository propertyAuditProcessStepReadRepository,
            IReservationItemReadRepository reservationItemReadRepository,
            IBillingAccountItemAppService billingAccountItemAppService,
            IBillingAccountAppService billingAccountAppService,
            IRoomBlockingAppService roomBlockingAppService,
            IHousekeepingStatusPropertyAppService housekeepingStatusPropertyAppService,
            IReservationItemDomainService reservationItemDomainService,
            EntityFrameworkCore.ReadInterfaces.Repositories.IRoomReadRepository roomReadRepository,
            IPropertyAuditProcessReadRepository propertyAuditProcessReadRepository,
            IPropertyParameterDomainService propertyParameterDomainService,
            IDomainService<PropertyAuditProcessStep> propertyAuditProcessStepDomainService,
            IApplicationUser applicationUser,
            IDomainService<PropertyAuditProcessStepError> propertyAuditProcessStepErrorDomainService,
            IGuestReservationItemAppService guestReservationItemAppService,
            IRoomTypeRepository roomTypeRepository,
            INotificationHandler notificationHandler,
            IServiceProvider serviceProvider,
            ICompanyClientReadRepository companyClientReadRepository,
            IPropertyCurrencyExchangeReadRepository propertyCurrencyExchangeReadRepository,
            ITourismTaxAppService tourismTaxAppService,
            IBillingAccountItemTransferAppService billingAccountItemTransferAppService)
            : base(propertyAuditProcessAdapter, propertyAuditProcessDomainService, notificationHandler)
        {
            _unitOfWorkManager = unitOfWorkManager;
            _propertyAuditProcessDomainService = propertyAuditProcessDomainService;
            _propertyParameterReadRepository = propertyParameterReadRepository;

            _propertyAuditProcessStepReadRepository = propertyAuditProcessStepReadRepository;
            _reservationItemReadRepository = reservationItemReadRepository;
            _billingAccountItemAppService = billingAccountItemAppService;
            _billingAccountAppService = billingAccountAppService;
            _roomBlockingAppService = roomBlockingAppService;
            _housekeepingStatusPropertyAppService = housekeepingStatusPropertyAppService;
            _reservationItemDomainService = reservationItemDomainService;
            _roomReadRepository = roomReadRepository;
            _propertyAuditProcessReadRepository = propertyAuditProcessReadRepository;
            _propertyParameterDomainService = propertyParameterDomainService;
            _propertyAuditProcessStepDomainService = propertyAuditProcessStepDomainService;
            _applicationUser = applicationUser;
            _propertyAuditProcessStepErrorDomainService = propertyAuditProcessStepErrorDomainService;
            _guestReservationItemAppService = guestReservationItemAppService;
            _roomTypeRepository = roomTypeRepository;
            _companyClientReadRepository = companyClientReadRepository;
            _propertyCurrencyExchangeReadRepository = propertyCurrencyExchangeReadRepository;
            _tourismTaxAppService = tourismTaxAppService;
            _billingAccountItemTransferAppService = billingAccountItemTransferAppService;

            SetObservables(serviceProvider);
        }

        private void SetObservables(IServiceProvider serviceProvider)
        {
            _executePropertyAuditProcessObservableList = new List<IExecutePropertyAuditProcessObservable>();
            var serviceForExecutePropertyAuditProcessObservable = serviceProvider.GetServices<IExecutePropertyAuditProcessObservable>().First(o => o.GetType() == typeof(RoomTypeInventoryDomainService));
            _executePropertyAuditProcessObservableList.Add(serviceForExecutePropertyAuditProcessObservable);

            _reservationItemChangeStatusObservableList = new List<IReservationItemChangeStatusObservable>();
            var serviceForReservationItemChangeStatusObservable = serviceProvider.GetServices<IReservationItemChangeStatusObservable>().First(o => o.GetType() == typeof(RoomTypeInventoryDomainService));
            _reservationItemChangeStatusObservableList.Add(serviceForReservationItemChangeStatusObservable);

            _reservationItemChangeRoomTypeOrPeriodObservableList = new List<IReservationItemChangeRoomTypeOrPeriodObservable>();
            var serviceForReservationItemChangeRoomTypeOrPeriodObservable = serviceProvider.GetServices<IReservationItemChangeRoomTypeOrPeriodObservable>().First(o => o.GetType() == typeof(RoomTypeInventoryDomainService));
            _reservationItemChangeRoomTypeOrPeriodObservableList.Add(serviceForReservationItemChangeRoomTypeOrPeriodObservable);
        }

        public PropertyAuditProcessStepDto Execute(int propertyId)
        {
            PropertyAuditProcessStepDto propertyAuditProcessStepDto = null;

            using (var uow = _unitOfWorkManager.Begin())
            {
                var auditParameters = GetAuditProcessParameters(propertyId);

                ValidatePropertyParameters(auditParameters);

                if (Notification.HasNotification()) return null;

                var reservationItemListForLaunchDaily = _reservationItemReadRepository.GetReservationItemListWithoutDailyByCheckinDate(propertyId, auditParameters.SystemDate.GetValueOrDefault());

                // Lança apenas diária se não pode fazer auditoria
                if (!GetTimeForAuditByPropertyId(propertyId).PropertyAuditProcessIsAvailable)
                {
                    LaunchDaily(reservationItemListForLaunchDaily, auditParameters);

                    uow.Complete();

                    return new PropertyAuditProcessStepDto();
                }

                // Erros
                var propertyAuditProcessStepErrorList = new List<PropertyAuditProcessStepError>();

                // Número de registros alteradas
                int numberOfRowsChanged = 0;

                // Pega o PropertyAuditProcess da data do sistema
                var propertyAuditProcess = _propertyAuditProcessReadRepository.GetPropertyAuditProcessCurrentByPropertyId(propertyId);

                //Passo 01 (Permanência na UH Inesperada)
                if (propertyAuditProcess == null)
                {
                    //validar reservas chekcin c a data igual a data do sistema
                    //a lista de reservas vai criar uma linha na tabela ReservationBudget com a data do sistema igual ao ultimo registro
                    //a reservationitem va ter a data de departuredate + 1
                    // na guestreservationitem também mudar a data 

                    List<ReservationItem> reservationItemList;
                    (reservationItemList, numberOfRowsChanged) = _reservationItemDomainService.UpdateEstimatedDepartureDateFromReservationItemListBySystemDate(propertyId, auditParameters.SystemDate.GetValueOrDefault().AddDays(1));

                    if (numberOfRowsChanged > 0)
                    {
                        var lstReservationItemDepartureDate = _reservationItemDomainService
                            .GetAllEstimatedDepartureDateFromReservationItemListBySystemDate(propertyId, auditParameters.SystemDate.GetValueOrDefault().AddDays(1));

                        var guestReservationItemList = new List<GuestReservationItem>();

                        if(lstReservationItemDepartureDate.Count() > 0)
                        {
                            var reservationItemIdList = lstReservationItemDepartureDate.Select(e => e.Id).ToList();
                            guestReservationItemList.AddRange(_guestReservationItemAppService.GetGuestReservationItemByReservationItemId(reservationItemIdList));
                        }

                        //create budget
                        if (lstReservationItemDepartureDate.Count() > 0)
                            _reservationItemDomainService.CreateReservationBudgetToReservationItemListBySystemDate(lstReservationItemDepartureDate, auditParameters.SystemDate.GetValueOrDefault());

                        if (reservationItemList.Any())
                            _reservationItemDomainService.BulkUpdate(reservationItemList);

                        //update guest reservation item
                        if (guestReservationItemList.Count() > 0)
                            _guestReservationItemAppService.UpdateGuestEstimatedDepartureDate(guestReservationItemList, auditParameters.SystemDate.GetValueOrDefault());

                        //update inventory
                        foreach (ReservationItem reservationItem in lstReservationItemDepartureDate)
                            RiseActionsAfterReservationItemChangeRoomTypeOrPeriod(reservationItem);
                    }
                }

                //Passo 02 (Lançar diária e Taxa de turismo(Portugal))
                if (propertyAuditProcess != null && propertyAuditProcess.AuditStepTypeId == (int)PropertyAuditProcessStepEnum.StepOne)
                {
                    LaunchDaily(reservationItemListForLaunchDaily, auditParameters);

                    if (Notification.HasNotification()) return null;

                    if (EnumHelper.GetEnumValue<CountryIsoCodeEnum>(_applicationUser.PropertyCountryCode, true) == CountryIsoCodeEnum.Europe)
                        _tourismTaxAppService.Launch();
                }
                    
                // Passo 03 (Mudar data do sistema)
                if (propertyAuditProcess != null && propertyAuditProcess.AuditStepTypeId == (int)PropertyAuditProcessStepEnum.StepTwo)
                {
                    DateTime newSystemDate = auditParameters.SystemDate.GetValueOrDefault().AddDays(1);
                    const int sytemDateApplicationParameterId = 10;
                    _propertyParameterDomainService.UpdatePropertyParameterValueByApplicationParameterId(propertyId, sytemDateApplicationParameterId, newSystemDate.ToString("yyyy-MM-dd"));
                    _propertyAuditProcessDomainService.UpdatePropertySystemDate(propertyAuditProcess, newSystemDate);
                }

                //Passo 04 (Atualizar No-show)
                if (propertyAuditProcess != null && propertyAuditProcess.AuditStepTypeId == (int)PropertyAuditProcessStepEnum.StepThree)
                {
                    var reservationItemNoShowList = _reservationItemDomainService.UpdateReservationItemListForNoShowBySystemDate(propertyId, auditParameters.SystemDate.GetValueOrDefault());

                    foreach (var reservationItem in reservationItemNoShowList)
                        RiseActionsAfterReservationItemChangeStatus(reservationItem, (ReservationStatus)reservationItem.ReservationItemStatusId);
                }

                //Passo 05
                if (propertyAuditProcess != null && propertyAuditProcess.AuditStepTypeId == (int)PropertyAuditProcessStepEnum.StepFour)
                {
                    //(Desbloquear UH)
                    var dateBeforeTheSystemDate = auditParameters.SystemDate.GetValueOrDefault().AddDays(-1);
                    var roomIdsForUnlock = _roomBlockingAppService.UnlockAndGetRoomListByBlockingEndDate(propertyId, dateBeforeTheSystemDate);
                    _housekeepingStatusPropertyAppService.UpdateManyRoomForHousekeeppingStatus(roomIdsForUnlock.ToList(), _housekeepingStatusPropertyAppService.GetHousekeepingStatusIdDirty());

                    //(Bloquear UH)
                    var roomIdsForBlocking = _roomBlockingAppService.GetRoomBlockingListByBlockingStartDate(propertyId, auditParameters.SystemDate.GetValueOrDefault());
                    _housekeepingStatusPropertyAppService.UpdateManyRoomForHousekeeppingStatus(roomIdsForBlocking.ToList(), _housekeepingStatusPropertyAppService.GetHousekeepingStatusIdDirty());

                    //(Mudar Status para sujo das UHs Ocupadas e Vagas*)
                    var roomIdListToChangeStatusForDirty = new List<int>();

                    var occupiedRoomIdList = _roomReadRepository.GetOccupiedRoomListByPropertyIdAndSystemDate(propertyId, auditParameters.SystemDate.GetValueOrDefault()).Select(r => r.Id);
                    roomIdListToChangeStatusForDirty.AddRange(occupiedRoomIdList);

                    var numberOfDaysToChangeRoomStatus = _propertyParameterReadRepository.GetNumberOfDaysToChangeRoomStatusByPropertyId(propertyId);
                    if (numberOfDaysToChangeRoomStatus != null)
                    {
                        var spareRoomIdList = _roomReadRepository.GetSpareRoomListByPropertyIdAndSystemDateForChangeHousekeepingStatus(propertyId, auditParameters.SystemDate.GetValueOrDefault(), numberOfDaysToChangeRoomStatus.GetValueOrDefault()).Select(r => r.Id);
                        roomIdListToChangeStatusForDirty.AddRange(spareRoomIdList);
                    }

                    _housekeepingStatusPropertyAppService.UpdateManyRoomForHousekeeppingStatus(roomIdListToChangeStatusForDirty, _housekeepingStatusPropertyAppService.GetHousekeepingStatusIdDirty());

                    RiseActionsAfterExecutePropertyAuditProcess();
                }

                var propertyAuditProcessStepCurrent = UpdatePropertyAuditProcessAndCreateStep(propertyId, auditParameters.SystemDate.GetValueOrDefault(), propertyAuditProcess, numberOfRowsChanged, auditParameters.PropertyTimeZone);

                _propertyAuditProcessStepReadRepository.SaveChanges();

                propertyAuditProcessStepDto = MapPropertyAuditProcessStepToDto(propertyAuditProcessStepCurrent, propertyAuditProcessStepErrorList);

                uow.Complete();
            }

            return propertyAuditProcessStepDto;
        }

        private AuditProcessParametersDto GetAuditProcessParameters(int propertyId)
        {
            var auditProcessParameters = new AuditProcessParametersDto
            {
                SystemDate = _propertyParameterReadRepository.GetSystemDate(propertyId),
                DailyBillingItemId = _propertyParameterReadRepository.GetDailyBillingItem(propertyId),
                DiffDailyBillingItemId = _propertyParameterReadRepository.GetDifferenceDailyBillingItem(propertyId),
                PropertyTimeZone = _propertyParameterReadRepository.GetTimeZoneByPropertyId(propertyId),
                PropertyId = propertyId
            };

            GetCurrencyAndCotationList(propertyId, auditProcessParameters);
            return auditProcessParameters;
        }

        private void GetCurrencyAndCotationList(int propertyId, AuditProcessParametersDto auditProcessParameters)
        {
            var propertyCurrency = _propertyParameterReadRepository.GetPropertyParameterForPropertyIdAndApplicationParameterId(propertyId, 4)?.PropertyParameterValue;

            auditProcessParameters.PropertyCurrencyId = propertyCurrency == null ? Guid.Empty : Guid.Parse(propertyCurrency);
            auditProcessParameters.CurrentQuotationList = _propertyCurrencyExchangeReadRepository.GetCurrentQuotationList(int.Parse(_applicationUser.PropertyId), null);
        }

        private void ValidatePropertyParameters(AuditProcessParametersDto auditProcessParameters)
        {
            if (auditProcessParameters.SystemDate == null)
            {
                Notification.Raise(Notification.DefaultBuilder
                .WithMessage(AppConsts.LocalizationSourceName, PropertyAuditProcess.EntityError.PropertyAuditProcessInvalidPropertySystemDate)
                .WithDetailedMessage(AppConsts.LocalizationSourceName, PropertyAuditProcess.EntityError.PropertyAuditProcessInvalidPropertySystemDate)
                .Build());
            }

            if (auditProcessParameters.DailyBillingItemId == null)
            {
                Notification.Raise(Notification.DefaultBuilder
                .WithMessage(AppConsts.LocalizationSourceName, PropertyParameter.EntityError.PropertyParameterMustHaveDailyBillingItem)
                .WithDetailedMessage(AppConsts.LocalizationSourceName, PropertyParameter.EntityError.PropertyParameterMustHaveDailyBillingItem)
                .Build());
            }

            if (auditProcessParameters.DiffDailyBillingItemId == null)
            {
                Notification.Raise(Notification.DefaultBuilder
                .WithMessage(AppConsts.LocalizationSourceName, PropertyParameter.EntityError.PropertyParameterMustHaveDiffDailyBillingItem)
                .WithDetailedMessage(AppConsts.LocalizationSourceName, PropertyParameter.EntityError.PropertyParameterMustHaveDiffDailyBillingItem)
                .Build());
            }

            if (String.IsNullOrEmpty(auditProcessParameters.PropertyTimeZone))
            {
                Notification.Raise(Notification.DefaultBuilder
                .WithMessage(AppConsts.LocalizationSourceName, PropertyParameter.EntityError.PropertyParameterMustHaveTimeZone)
                .WithDetailedMessage(AppConsts.LocalizationSourceName, PropertyParameter.EntityError.PropertyParameterMustHaveTimeZone)
                .Build());
            }
        }

        #region LaunchDaily

        private void LaunchDaily(IList<ReservationItem> reservationItemListForAuditProcess, AuditProcessParametersDto auditProcessParameters)
        {
            var billingAccountItemDebitDtoList = new List<BillingAccountItemDebitDto>();

            if (EnumHelper.GetEnumValue<CountryIsoCodeEnum>(_applicationUser.PropertyCountryCode, true) == CountryIsoCodeEnum.Europe)
            {
                var reservationIdForAuditProcessList = reservationItemListForAuditProcess
                .Select(r => r.Reservation)
                .DistinctBy(r => r.Id)
                .Where(r => FilterByLauchDailyOfReservationConfirmation(r.ReservationConfirmationList.FirstOrDefault()))
                .Select(r => r.Id);

                reservationItemListForAuditProcess = reservationItemListForAuditProcess.Where(r => reservationIdForAuditProcessList.Contains(r.ReservationId)).ToList();
            }

            var billingAccountReservationItemAndGroupDictionary = new Dictionary<BillingAccount, BillingAccount>();
            List<BillingAccountItemTransferDto> transferDtoList = new List<BillingAccountItemTransferDto>();

            foreach (var reservationItem in reservationItemListForAuditProcess)
            {
                var billingAccountOpen = GetBillingAccountOpen(auditProcessParameters.PropertyId, reservationItem);

                if (Notification.HasNotification()) return;

                BillingAccount billingAccountGroup = null;

                if (reservationItem.Reservation.UnifyAccounts
                    && EnumHelper.GetEnumValue<CountryIsoCodeEnum>(_applicationUser.PropertyCountryCode, true) == CountryIsoCodeEnum.Europe)
                    billingAccountGroup = reservationItem.Reservation.BillingAccountList
                        .FirstOrDefault(b => b.BillingAccountTypeId == (int)BillingAccountTypeEnum.GroupAccount &&
                            b.IsMainAccount &&
                            b.StatusId == (int)AccountBillingStatusEnum.Opened);

                if (billingAccountGroup != null)
                    billingAccountReservationItemAndGroupDictionary.Add(billingAccountOpen, billingAccountGroup);

                var reservationBudgetCurrent = reservationItem.ReservationBudgetList.Where(r => r.BudgetDay == auditProcessParameters.SystemDate.GetValueOrDefault()).FirstOrDefault();

                if (reservationBudgetCurrent != null)
                {
                    var currencyExchangeReferenceId = GetCurrencyExchangeReference(auditProcessParameters.PropertyCurrencyId, auditProcessParameters.CurrentQuotationList, reservationBudgetCurrent);

                    var currencyExchangeReferenceSecId = GetCurrencyExchangeReferenceSec(auditProcessParameters.PropertyCurrencyId, auditProcessParameters.CurrentQuotationList, currencyExchangeReferenceId);

                    CreateDailyDebitDto(auditProcessParameters, billingAccountItemDebitDtoList, billingAccountOpen, reservationBudgetCurrent, currencyExchangeReferenceId, currencyExchangeReferenceSecId, billingAccountGroup);

                    if (reservationBudgetCurrent.SeparatedRate.HasValue && reservationBudgetCurrent.SeparatedRate.Value > 0)
                        CreateDiffDaily(auditProcessParameters, billingAccountItemDebitDtoList, billingAccountOpen, reservationBudgetCurrent, currencyExchangeReferenceId, currencyExchangeReferenceSecId, billingAccountGroup);
                }
            }

            _billingAccountItemAppService.CreateAllDailyDebit(billingAccountItemDebitDtoList);

            RedirectItens(billingAccountItemDebitDtoList, billingAccountReservationItemAndGroupDictionary, transferDtoList);
        }

        private bool FilterByLauchDailyOfReservationConfirmation(ReservationConfirmation reservationConfirmation)
        {
            return !reservationConfirmation.LaunchDaily.HasValue || (reservationConfirmation.LaunchDaily.HasValue && reservationConfirmation.LaunchDaily.Value);
        }

        private Guid? GetCurrencyExchangeReferenceSec(Guid propertyCurrencyId, List<QuotationCurrentDto> currentQuotationList, Guid? currencyExchangeReferenceId)
        {
            return currencyExchangeReferenceId == null ? null :
                                                 currentQuotationList.FirstOrDefault(c => c.CurrencyId.Equals(propertyCurrencyId))?.Id;
        }

        private Guid? GetCurrencyExchangeReference(Guid propertyCurrencyId, List<QuotationCurrentDto> currentQuotationList, ReservationBudget reservationBudgetCurrent)
        {
            return propertyCurrencyId == Guid.Empty || reservationBudgetCurrent.CurrencyId == propertyCurrencyId ? null :
                                              currentQuotationList.FirstOrDefault(c => c.CurrencyId.Equals(reservationBudgetCurrent.CurrencyId))?.Id;
        }

        private void CreateDiffDaily(AuditProcessParametersDto auditProcessParameters, List<BillingAccountItemDebitDto> billingAccountItemDebitDtoList, BillingAccount billingAccountOpen,
            ReservationBudget reservationBudgetCurrent, Guid? currencyExchangeReferenceId, Guid? currencyExchangeReferenceSecId, BillingAccount billingAccountGroup = null)
        {
            billingAccountItemDebitDtoList.Add(new BillingAccountItemDebitDto
            {
                PropertyId = auditProcessParameters.PropertyId,
                Amount = reservationBudgetCurrent.SeparatedRate.Value,
                CurrencyId = reservationBudgetCurrent.CurrencyId,
                BillingItemId = auditProcessParameters.DiffDailyBillingItemId.GetValueOrDefault(),
                CurrencyExchangeReferenceId = currencyExchangeReferenceId,
                CurrencyExchangeReferenceSecId = currencyExchangeReferenceSecId,
                DateParameter = auditProcessParameters.SystemDate.GetValueOrDefault(),
                BillingAccountId = billingAccountGroup != null ? billingAccountGroup.Id : billingAccountOpen.Id,
                BillingAccountIdLastSource = billingAccountGroup != null ? billingAccountOpen.Id : (Guid?)null
            });
        }

        private void CreateDailyDebitDto(AuditProcessParametersDto auditProcessParameters, List<BillingAccountItemDebitDto> billingAccountItemDebitDtoList, BillingAccount billingAccountOpen,
            ReservationBudget reservationBudgetCurrent, Guid? currencyExchangeReferenceId, Guid? currencyExchangeReferenceSecId, BillingAccount billingAccountGroup = null)
        {
            billingAccountItemDebitDtoList.Add(new BillingAccountItemDebitDto
            {
                PropertyId = auditProcessParameters.PropertyId,
                Amount = reservationBudgetCurrent.ManualRate - (reservationBudgetCurrent.SeparatedRate ?? 0),
                CurrencyId = reservationBudgetCurrent.CurrencyId,
                BillingItemId = auditProcessParameters.DailyBillingItemId.GetValueOrDefault(),
                CurrencyExchangeReferenceId = currencyExchangeReferenceId,
                CurrencyExchangeReferenceSecId = currencyExchangeReferenceSecId,
                DateParameter = auditProcessParameters.SystemDate.GetValueOrDefault(),
                BillingAccountId = billingAccountGroup != null ? billingAccountGroup.Id : billingAccountOpen.Id,
                BillingAccountIdLastSource = billingAccountGroup != null ? billingAccountOpen.Id : (Guid?)null
            });
        }

        private BillingAccount GetBillingAccountOpen(int propertyId, ReservationItem reservationItem)
        {
            var billingAccountOpen = GetOpenBillingAccountForLaunchDaily(reservationItem);
            if (NoAccountIsOpen(billingAccountOpen))
            {
                billingAccountOpen = new BillingAccount();

                if (ReservationItemHasCompanyClientAndToBeBilled(reservationItem))
                    SetBillingAccountOpenWhenIsCompany(billingAccountOpen, reservationItem, propertyId);
                else
                    SetBillingAccountOpenWhenIsMainGuest(billingAccountOpen, reservationItem, propertyId);
            }
            else if (!billingAccountOpen.IsMainAccount)
            {
                _billingAccountAppService.ChangeMainAccount(billingAccountOpen);
            }

            return billingAccountOpen;
        }

        private void RedirectItens(List<BillingAccountItemDebitDto> billingAccountItemDebitDtoList, Dictionary<BillingAccount, BillingAccount> billingAccountReservationItemAndGroupDictionary, List<BillingAccountItemTransferDto> transferDtoList)
        {
            if (billingAccountReservationItemAndGroupDictionary.Any())
            {
                foreach (var item in billingAccountReservationItemAndGroupDictionary)
                {
                    var billingAccountItemListOfAccount = billingAccountItemDebitDtoList.Where(b => b.BillingAccountIdLastSource == item.Key.Id).ToList();
                    SetTransferList(billingAccountItemListOfAccount, transferDtoList, item.Key, item.Value);
                }

                if (transferDtoList.Any())
                    _billingAccountItemTransferAppService.CreateRange(transferDtoList);
            }
        }

        private void SetTransferList(List<BillingAccountItemDebitDto> billingAccountItemDebitDtoList, List<BillingAccountItemTransferDto> transferDtoList, BillingAccount billingAccountOpen, BillingAccount billingAccountGroup)
        {
            foreach (var billingAccountItem in billingAccountItemDebitDtoList)
            {
                transferDtoList.Add(new BillingAccountItemTransferDto()
                {
                    TransferDate = DateTime.UtcNow.ToZonedDateTimeLoggedUser(),
                    BillingAccountItemId = billingAccountItem.Id,
                    BillingAccountIdSource = billingAccountOpen.Id,
                    BillingAccountIdDestination = billingAccountGroup.Id
                });
            }
        }

        private BillingAccount GetOpenBillingAccountForLaunchDaily(ReservationItem reservationItem)
        {
            BillingAccount billingAccount = null;

            if (ReservationItemHasCompanyClientAndToBeBilled(reservationItem)
                &&
                (EnumHelper.GetEnumValue<CountryIsoCodeEnum>(_applicationUser.PropertyCountryCode, true) != CountryIsoCodeEnum.Europe
                || IsEuropeAndDoesNotUnifyAccounts(reservationItem)))
            {
                var billingAccountMainOpen = reservationItem.BillingAccountList
                    .FirstOrDefault(b => b.BillingAccountTypeId == (int)BillingAccountTypeEnum.Company &&
                        b.IsMainAccount &&
                        b.StatusId == (int)AccountBillingStatusEnum.Opened);

                if (billingAccountMainOpen == null)
                    billingAccount = reservationItem.BillingAccountList
                        .FirstOrDefault(b => b.BillingAccountTypeId == (int)BillingAccountTypeEnum.Company &&
                            b.StatusId == (int)AccountBillingStatusEnum.Opened);
                else
                    billingAccount = billingAccountMainOpen;
            }
            else
            {
                var billingAccountMainOpen = reservationItem.GuestReservationItemList.FirstOrDefault(g => g.IsMain).BillingAccountList
                       .FirstOrDefault(b => b.IsMainAccount && b.StatusId == (int)AccountBillingStatusEnum.Opened);

                if (billingAccountMainOpen == null)
                    billingAccount = reservationItem.GuestReservationItemList.Where(g => g.IsMain).FirstOrDefault().BillingAccountList
                        .FirstOrDefault(b => b.StatusId == (int)AccountBillingStatusEnum.Opened);
                else
                    billingAccount = billingAccountMainOpen;
            }

            return billingAccount;
        }

        private bool ReservationItemHasCompanyClient(ReservationItem reservationItem)
        {
            return reservationItem.Reservation.CompanyClientId != null;
        }

        private bool ReservationItemHasCompanyClientAndToBeBilled(ReservationItem reservationItem)
        {
            return reservationItem.Reservation.CompanyClientId != null && reservationItem.Reservation.ReservationConfirmationList.FirstOrDefault().PaymentTypeId == (int)PaymentTypeEnum.TobeBilled;
        }

        private bool IsEuropeAndDoesNotUnifyAccounts(ReservationItem reservationItem)
        {
            return (ReservationItemHasCompanyClientAndToBeBilled(reservationItem)
                                && EnumHelper.GetEnumValue<CountryIsoCodeEnum>(_applicationUser.PropertyCountryCode, true) == CountryIsoCodeEnum.Europe
                                && !reservationItem.Reservation.UnifyAccounts);
        }

        private bool NoAccountIsOpen(BillingAccount billingAccountOpen)
        {
            return billingAccountOpen == null;
        }

        private void SetBillingAccountOpenWhenIsCompany(BillingAccount billingAccountOpen, ReservationItem reservationItem, int propertyId)
        {
            var companyClientHasAccount = reservationItem.BillingAccountList
            .Any(b => b.BillingAccountTypeId == (int)BillingAccountTypeEnum.Company);

            if (companyClientHasAccount)
            {
                var groupKeyOfNewBillingAccount = reservationItem.BillingAccountList
                    .FirstOrDefault(b => b.BillingAccountTypeId == (int)BillingAccountTypeEnum.Company &&
                        b.IsMainAccount).GroupKey;

                billingAccountOpen.Id = _billingAccountAppService.CreateAndSetNewMainAccount(groupKeyOfNewBillingAccount);
            }
            else
            {
                var companyClientId = reservationItem.Reservation.CompanyClientId;

                var billingAccountDto = new BillingAccountDto
                {
                    ReservationId = reservationItem.ReservationId,
                    ReservationItemId = reservationItem.Id,
                    CompanyClientId = companyClientId,
                    BillingAccountName = _companyClientReadRepository.GetTradeNameById(companyClientId.Value),
                    BillingAccountTypeId = (int)BillingAccountTypeEnum.Company,
                    StatusId = (int)AccountBillingStatusEnum.Opened,
                    PropertyId = propertyId
                };
                billingAccountOpen.Id = _billingAccountAppService.Create(billingAccountDto).Result.Id;
            }
        }

        private void SetBillingAccountOpenWhenIsMainGuest(BillingAccount billingAccountOpen, ReservationItem reservationItem, int propertyId)
        {
            var mainGuestHasAccount = reservationItem.GuestReservationItemList
            .FirstOrDefault(g => g.IsMain).BillingAccountList.Any();

            if (mainGuestHasAccount)
            {
                var groupKeyOfNewBillingAccount = reservationItem.GuestReservationItemList
                    .FirstOrDefault(g => g.IsMain).BillingAccountList.FirstOrDefault().GroupKey;

                billingAccountOpen.Id = _billingAccountAppService.CreateAndSetNewMainAccount(groupKeyOfNewBillingAccount);
            }
            else
            {
                var guest = reservationItem.GuestReservationItemList
                    .FirstOrDefault(g => g.IsMain);

                var billingAccountDto = new BillingAccountDto
                {
                    ReservationId = reservationItem.ReservationId,
                    ReservationItemId = reservationItem.Id,
                    GuestReservationItemId = guest.Id,
                    BillingAccountName = guest.GuestName,
                    BillingAccountTypeId = (int)BillingAccountTypeEnum.Guest,
                    StatusId = (int)AccountBillingStatusEnum.Opened,
                    PropertyId = propertyId
                };
                billingAccountOpen.Id = _billingAccountAppService.Create(billingAccountDto).Result.Id;
            }
        }

        #endregion

        private PropertyAuditProcessStep UpdatePropertyAuditProcessAndCreateStep(int propertyId, DateTime systemDate, PropertyAuditProcess propertyAuditProcess, int numberOfRowsChanged, string timeZone)
        {
            // Cria ou atualiza o processo de auditoria
            if (propertyAuditProcess == null)
            {
                var propertyAuditProcessBuilder = new PropertyAuditProcess.Builder(Notification)
                    .WithId(Guid.NewGuid())
                    .WithAuditStepTypeId((int)PropertyAuditProcessStepEnum.StepOne)
                    .WithStartDate(DateTime.UtcNow.ToZonedDateTime(timeZone))
                    .WithPropertyId(propertyId)
                    .WithPropertySystemDate(systemDate)
                    .WithTenantId(_applicationUser.TenantId);

                PropertyAuditProcessDomainService.InsertAndSaveChanges(propertyAuditProcessBuilder);

                propertyAuditProcess = propertyAuditProcessBuilder.Build();
            }
            else if (propertyAuditProcess.AuditStepTypeId == (int)PropertyAuditProcessStepEnum.StepFour)
            {
                _propertyAuditProcessDomainService.UpdateStepTypeIdAndEndDate(propertyAuditProcess, propertyAuditProcess.AuditStepTypeId + 1, DateTime.UtcNow.ToZonedDateTime(timeZone));
            }
            else
            {
                _propertyAuditProcessDomainService.UpdateStepTypeIdAndEndDate(propertyAuditProcess, propertyAuditProcess.AuditStepTypeId + 1);
            }

            // Cria um novo Step
            var propertyAuditProcessStepBuilder = new PropertyAuditProcessStep.Builder(Notification)
                .WithId(Guid.NewGuid())
                .WithAuditStepTypeId(propertyAuditProcess.AuditStepTypeId)
                .WithPropertyAuditProcessId(propertyAuditProcess.Id)
                .WithPropertyId(propertyId)
                .WithRowTotal(numberOfRowsChanged)
                .WithCurrentRow(numberOfRowsChanged)
                .WithStartDate(DateTime.UtcNow.ToZonedDateTimeLoggedUser())
                .WithTenantId(_applicationUser.TenantId);

            _propertyAuditProcessStepDomainService.InsertAndSaveChanges(propertyAuditProcessStepBuilder);
            var propertyAuditProcessStep = propertyAuditProcessStepBuilder.Build();

            // Atualiza o Passo nos Parâmetros da Property
            const int auditStepApplicationParameterId = 11;
            if (propertyAuditProcessStep.AuditStepTypeId == (int)PropertyAuditProcessStepEnum.StepFive)
            {
                _propertyParameterDomainService.UpdatePropertyParameterValueByApplicationParameterId(propertyId, auditStepApplicationParameterId, "0");
            }
            else
            {
                _propertyParameterDomainService.UpdatePropertyParameterValueByApplicationParameterId(propertyId, auditStepApplicationParameterId, propertyAuditProcess.AuditStepTypeId.ToString());
            }

            return propertyAuditProcessStep;
        }

        private PropertyAuditProcessStepDto MapPropertyAuditProcessStepToDto(PropertyAuditProcessStep entity, IList<PropertyAuditProcessStepError> propertyAuditProcessStepErrorList)
        {
            PropertyAuditProcessStepDto dto = new PropertyAuditProcessStepDto
            {
                Id = entity.Id,
                AuditStepTypeId = entity.AuditStepTypeId,
                PropertyAuditProcessId = entity.PropertyAuditProcessId,
                RowTotal = entity.RowTotal,
                CurrentRow = entity.CurrentRow,
                PropertyId = entity.PropertyId,
                StartDate = entity.StartDate,
                EndDate = entity.EndDate,
                TenantId = entity.TenantId
            };

            foreach (PropertyAuditProcessStepError errorEntity in propertyAuditProcessStepErrorList)
            {
                PropertyAuditProcessStepErrorDto errorDto = new PropertyAuditProcessStepErrorDto
                {
                    Id = errorEntity.Id,
                    PropertyAuditProcessStepId = errorEntity.PropertyAuditProcessStepId,
                    PropertyId = errorEntity.PropertyId,
                    TenantId = errorEntity.TenantId,
                    ReservationItemCode = errorEntity.ReservationItemCode,
                    ReservationItemStatusName = errorEntity.ReservationItemStatusName,
                    ReservationItemStatusId = errorEntity.ReservationItemStatusId,
                    EstimatedArrivalDate = errorEntity.EstimatedArrivalDate,
                    EstimatedDepartureDate = errorEntity.EstimatedDepartureDate,
                    RoomNumber = errorEntity.RoomNumber,
                    RoomTypeName = errorEntity.RoomTypeName,
                    GuestName = errorEntity.GuestName
                };
                dto.PropertyAuditProcessStepErrorList.Add(errorDto);
            }

            return dto;
        }

        public PropertyAuditProcessStepNumber GetAuditStepByPropertyId(int propertyId)
        {
            return _propertyParameterReadRepository.GetAuditStepByPropertyId(propertyId);
        }

        public PropertyAuditProcessTimeDto GetTimeForAuditByPropertyId(int propertyId)
        {
            PropertyAuditProcessTimeDto propertyAuditProcessTime = new PropertyAuditProcessTimeDto();
            DateTime today = DateTime.UtcNow.ToZonedDateTimeLoggedUser();
            DateTime? systemDate = _propertyParameterReadRepository.GetSystemDate(propertyId);
            if (systemDate != null && systemDate.GetValueOrDefault().Date < today.Date)
            {
                propertyAuditProcessTime.PropertyAuditProcessIsAvailable = true;
            }
            else if (systemDate != null && systemDate.GetValueOrDefault().Date == today.Date && _propertyAuditProcessReadRepository.GetPropertyAuditProcessCurrentByPropertyId(propertyId) != null)
            {
                propertyAuditProcessTime.PropertyAuditProcessIsAvailable = true;
            }
            else
            {
                DateTime tomorrow = today.AddDays(1).Date;
                propertyAuditProcessTime.Hour = (tomorrow - today).Hours;
                propertyAuditProcessTime.Minutes = (tomorrow - today).Minutes;
                propertyAuditProcessTime.PropertyAuditProcessIsAvailable = false;
            }

            if (systemDate != null)
            {
                propertyAuditProcessTime.CurrentSystemDate = systemDate.GetValueOrDefault();
                propertyAuditProcessTime.NextSystemDate = systemDate.GetValueOrDefault().AddDays(1);
            }

            return propertyAuditProcessTime;
        }

        public PropertyAuditProcessTimeDto GetSystemDateByPropertyId(int propertyId)
        {
            PropertyAuditProcessTimeDto propertyAuditProcessTime = new PropertyAuditProcessTimeDto();
            DateTime? systemDate = _propertyParameterReadRepository.GetSystemDate(propertyId);

            if (systemDate != null)
            {
                propertyAuditProcessTime.CurrentSystemDate = systemDate.GetValueOrDefault();
            }

            return propertyAuditProcessTime;
        }

        private void RiseActionsAfterExecutePropertyAuditProcess()
        {
            foreach (var observable in _executePropertyAuditProcessObservableList)
            {
                var dtoObservable = new ExecutePropertyAuditProcessObservableDto
                {
                    RoomTypeIdList = _roomTypeRepository.GetAllByPropertyId(Convert.ToInt32(_applicationUser.PropertyId)).Select(r => r.Id)
                };

                observable.ExecuteActionAfterExecutePropertyAuditProcess(dtoObservable);
            }
        }

        private void RiseActionsAfterReservationItemChangeStatus(ReservationItem reservationItem,
            ReservationStatus statusNew, ReservationStatus? statusOld = null)
        {
            foreach (var observable in _reservationItemChangeStatusObservableList)
            {
                var dtoObservable = new ReservationItemChangeStatusObservableDto
                {
                    RoomTypeId = reservationItem.ReceivedRoomTypeId,
                    ReservationItemStatusIdNew = statusNew,
                    ReservationItemStatusIdOld = statusOld,
                    InitialDate = reservationItem.EstimatedArrivalDate,
                    EndDate = reservationItem.EstimatedDepartureDate
                };

                observable.ExecuteActionAfterChangeReservationItemStatus(dtoObservable);
            }
        }

        private void RiseActionsAfterReservationItemChangeRoomTypeOrPeriod(ReservationItem reservationItem)
        {
            foreach (var observable in _reservationItemChangeRoomTypeOrPeriodObservableList)
            {
                var dtoObservable = new ReservationItemChangeRoomTypeOrPeriodObservableDto
                {
                    OldRoomTypeId = reservationItem.ReceivedRoomTypeId,
                    NewRoomTypeId = reservationItem.ReceivedRoomTypeId,
                    OldInitialDate = reservationItem.CheckInDate.Value,
                    OldEndDate = reservationItem.EstimatedDepartureDate.AddDays(-1),
                    NewInitialDate = reservationItem.CheckInDate.Value,
                    NewEndDate = reservationItem.EstimatedDepartureDate
                };

                observable.ExecuteActionAfterReservationItemChangeRoomTypeOrPeriod(dtoObservable);
            }
        }
    }

    public class AuditProcessParametersDto
    {
        public DateTime? SystemDate { get; set; }
        public int? DailyBillingItemId { get; set; }
        public int? DiffDailyBillingItemId { get; set; }
        public string PropertyTimeZone { get; set; }
        public Guid PropertyCurrencyId { get; set; }
        public List<QuotationCurrentDto> CurrentQuotationList { get; set; }
        public int PropertyId { get; set; }
    }
}
