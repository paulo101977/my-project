﻿using System;
using Thex.Application.Adapters;
using Thex.Domain.Entities;
using Thex.Application.Interfaces;
using Tnf.Domain.Services;
using Thex.Dto;
using Tnf.Dto;
using Thex.Common.Enumerations;
using Thex.Infra.ReadInterfaces;
using Thex.Dto.Reservation;
using Thex.Dto.Availability;
using Thex.Domain.Services.Interfaces;
using System.Linq;
using Thex.Common;
using Thex.Dto.Rooms;
using System.Collections.Generic;
using Thex.EntityFrameworkCore.ReadInterfaces.Repositories;
using Tnf.Notifications;
using Thex.Domain.Interfaces.Repositories;

namespace Thex.Application.Services
{
    public class AvailabilityAppService : ApplicationServiceBase, IAvailabilityAppService
    {
        protected readonly IRoomTypeAppService _roomTypeAppService;
        protected readonly IRoomTypeReadRepository _roomTypeReadRepository;
        protected readonly EntityFrameworkCore.ReadInterfaces.Repositories.IRoomReadRepository _roomReadRepository;
        protected readonly IRoomBlockingReadRepository _roomBlockingReadRepository;
        protected readonly IReservationItemReadRepository _reservationItemReadRepository;
        protected readonly IRoomTypeDomainService _roomTypeDomainService;
        protected readonly IRoomDomainService _roomDomainService;
        protected readonly IReservationDomainService _reservationDomainService;
        private readonly IRoomTypeInventoryRepository _roomTypeInventoryRepository;

        public AvailabilityAppService(
            IRoomTypeAppService roomTypeAppService,
            IRoomTypeReadRepository roomTypeReadRepository,
            EntityFrameworkCore.ReadInterfaces.Repositories.IRoomReadRepository roomReadRepository,
            IReservationItemReadRepository reservationItemReadRepository,
            IRoomTypeDomainService roomTypeDomainService,
            IRoomDomainService roomDomainService,
            IReservationDomainService reservationDomainService,
            IRoomBlockingReadRepository roomBlockingReadRepository,
            IRoomTypeInventoryRepository roomTypeInventoryRepository,
            INotificationHandler notificationHandler)
            :base(notificationHandler)
        {
            _roomTypeAppService = roomTypeAppService;
            _roomTypeReadRepository = roomTypeReadRepository;
            _roomReadRepository = roomReadRepository;
            _reservationItemReadRepository = reservationItemReadRepository;
            _roomTypeDomainService = roomTypeDomainService;
            _roomDomainService = roomDomainService;
            _reservationDomainService = reservationDomainService;
            _roomBlockingReadRepository = roomBlockingReadRepository;
            _roomTypeInventoryRepository = roomTypeInventoryRepository;
        }

        public AvailabilityDto GetAvailabilityRoomTypes(SearchAvailabilityDto requestDto, int propertyId, DateTime? start = null, DateTime? final = null)
        {
            if (requestDto != null && !start.HasValue && !final.HasValue)
                ValidateAvailability(requestDto, propertyId);

            if (Notification.HasNotification())
                return null;

            var startDate = start ?? Convert.ToDateTime(requestDto.InitialDate);
            var finalDate = final ?? Convert.ToDateTime(requestDto.InitialDate).AddDays(requestDto.Period);

            var parentRoomsWithTotals = _roomReadRepository.GetParentRoomsWithTotalChildrens(propertyId);
            var reservationItems = _reservationItemReadRepository.GetAvailabilityReservationsItemsByPeriodAndPropertyId(startDate, finalDate, propertyId, false);
            var roomTypesList = _roomTypeReadRepository.GetAvailabilityRoomTypeRow(propertyId);
            var roomBlockedList = _roomBlockingReadRepository.GetAllRoomsBlockedByPropertyId(propertyId, startDate.Date, finalDate.Date);

            var roomTypeInventoryList = _roomTypeInventoryRepository.GetAllByPeriod(startDate.AddDays(-1), finalDate.AddDays(1)).ToList();
            var availabilityList = _roomTypeDomainService.GetAvailabilityRoomTypeColumns(parentRoomsWithTotals, reservationItems, roomTypesList, roomBlockedList, roomTypeInventoryList, startDate.AddDays(-1), finalDate.AddDays(1));
            
            var footerList = _roomTypeDomainService.GetAvailabilityTotals(availabilityList, roomBlockedList, roomTypesList, startDate, finalDate);            

            _roomTypeDomainService.FillRolesInAvailabilityRoomTypeColumns(roomTypesList, availabilityList, startDate, finalDate);

            availabilityList = availabilityList.OrderBy(a => a.DateFormatted).ThenBy(a => a.RoomTypeId).ToList();

            return new AvailabilityDto()
            {
                RoomTypeList = roomTypesList,
                AvailabilityList = availabilityList,
                FooterList = footerList,
                RoomsBlocked = roomBlockedList
            };
        }


        private void ValidateAvailabilityWithRoomType(SearchAvailabilityDto requestDto, int propertyId, int roomTypeId)
        {
            if (roomTypeId <= 0)
                NotifyIdIsMissing();

            ValidateAvailability(requestDto, propertyId);
        }

        private void ValidateAvailability(SearchAvailabilityDto requestDto, int propertyId)
        {
            if (propertyId <= 0)
                NotifyIdIsMissing();

            var validPeriods = new int[] { 7, 15, 30, 31 }.ToList();

            if (!validPeriods.Contains(requestDto.Period))
                Notification.Raise(Notification.DefaultBuilder.WithMessage(AppConsts.LocalizationSourceName, CommonsEnum.Error.InvalidPeriodParameterAvailability).Build());

            DateTime dateTime;
            if (!DateTime.TryParse(requestDto.InitialDate, out dateTime))
                Notification.Raise(Notification.DefaultBuilder.WithMessage(AppConsts.LocalizationSourceName, CommonsEnum.Error.InvalidDateParameterAvailability).Build());
        }

        public AvailabilityRoomsDto GetAvailabilityRooms(SearchAvailabilityDto requestDto, int propertyId, int roomTypeId)
        {
            ValidateAvailabilityWithRoomType(requestDto, propertyId, roomTypeId);

            if (Notification.HasNotification())
                return null;

            var startDate = Convert.ToDateTime(requestDto.InitialDate);
            var finalDate = Convert.ToDateTime(requestDto.InitialDate).AddDays(requestDto.Period);
            
            var allPropertyRooms = _roomReadRepository.GetRoomsByPropertyId(new GetAllRoomsDto() { PageSize = 100000 }, propertyId);
            var reservationItems = _reservationItemReadRepository.GetAvailabilityReservationsItemsWithMainGuest(startDate, finalDate, propertyId, roomTypeId);

            var roomList = _roomDomainService.GetAvailabilityRoomRowDto(allPropertyRooms, roomTypeId);
            var reservationList = _reservationDomainService.GetAvailabilityReservationColumnsDto(allPropertyRooms, reservationItems);

            return new AvailabilityRoomsDto
            {
                RoomList = roomList,
                ReservationList = reservationList,
            };

        }
    }
}
