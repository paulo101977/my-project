﻿using System;
using Thex.Application.Adapters;
using Thex.Domain.Entities;
using Thex.Application.Interfaces;
using Tnf.Domain.Services;
using Tnf.Notifications;

namespace Thex.Application.Services
{
    public class FeatureGroupAppService : ScaffoldFeatureGroupAppService, IFeatureGroupAppService
    {
        public FeatureGroupAppService(
            IFeatureGroupAdapter featureGroupAdapter,
            IDomainService<FeatureGroup> featureGroupDomainService,
            INotificationHandler notificationHandler)
            : base(featureGroupAdapter, featureGroupDomainService, notificationHandler)
        {
        }
    }
}
