﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Thex.Application.Adapters;
using Thex.Application.Interfaces;
using Thex.Common;
using Thex.Common.Enumerations;
using Thex.Domain.Entities;
using Thex.Domain.Interfaces.Repositories;
using Thex.Domain.Services.Interfaces;
using Thex.Dto;
using Thex.Infra.ReadInterfaces;
using Thex.Kernel;
using Tnf.Dto;
using Tnf.Localization;
using Tnf.Notifications;

namespace Thex.Application.Services
{
    public class ReservationBudgetAppService : ScaffoldReservationBudgetAppService, IReservationBudgetAppService
    {
        protected readonly IPropertyBaseRateReadRepository _propertyBaseRateReadRepository;
        protected readonly IPropertyParameterReadRepository _propertyParameterReadRepository;
        protected readonly IRatePlanReadRepository _ratePlanReadRepository;
        protected readonly IReservationItemReadRepository _reservationItemReadRepository;
        protected readonly IReservationBudgetDomainService _reservationBudgetDomainService;
        protected readonly IReservationBudgetRepository _reservationBudgetRepository;
        protected readonly IReservationBudgetReadRepository _reservationBudgetReadRepository;
        protected readonly IReservationReadRepository _reservationReadRepository;
        protected readonly ILocalizationManager _localizationManager;
        protected readonly IPropertyMealPlanTypeAppService _propertyMealPlanTypeAppService;
        protected readonly IRoomTypeReadRepository _roomTypeReadRepository;
        protected readonly IPropertyRateStrategyReadRepository _propertyRateStrategyReadRepository;
        protected readonly IAvailabilityAppService _availabilityAppService;
        protected readonly IRoomTypeDomainService _roomTypeDomainService;
        protected readonly EntityFrameworkCore.ReadInterfaces.Repositories.IRoomReadRepository _roomReadRepository;
        protected readonly IRoomBlockingReadRepository _roomBlockingReadRepository;
        private readonly IRoomTypeInventoryRepository _roomTypeInventoryRepository;
        private readonly IApplicationUser _applicationUser;

        public ReservationBudgetAppService(
            IPropertyMealPlanTypeAppService propertyMealPlanTypeAppService,
            IReservationBudgetAdapter reservationBudgetAdapter,
            IReservationBudgetDomainService reservationBudgetDomainService,
            IPropertyBaseRateReadRepository propertyBaseRateReadRepository,
            IPropertyParameterReadRepository propertyParameterReadRepository,
            IRatePlanReadRepository ratePlanReadRepository,
            IReservationItemReadRepository reservationItemReadRepository,
            IReservationBudgetRepository reservationBudgetRepository,
            IReservationBudgetReadRepository reservationBudgetReadRepository,
            IReservationReadRepository reservationReadRepository,
            ILocalizationManager localizationManager,
            INotificationHandler notificationHandler,
            IRoomTypeReadRepository roomTypeReadRepository,
            IPropertyRateStrategyReadRepository propertyRateStrategyReadRepository,
            IAvailabilityAppService availabilityAppService,
            IRoomTypeDomainService roomTypeDomainService,
            IApplicationUser applicationUser,
            EntityFrameworkCore.ReadInterfaces.Repositories.IRoomReadRepository roomReadRepository,
            IRoomBlockingReadRepository roomBlockingReadRepository,
            IRoomTypeInventoryRepository roomTypeInventoryRepository)
            : base(reservationBudgetAdapter, reservationBudgetDomainService, notificationHandler)
        {
            _propertyMealPlanTypeAppService = propertyMealPlanTypeAppService;
            _propertyBaseRateReadRepository = propertyBaseRateReadRepository;
            _propertyParameterReadRepository = propertyParameterReadRepository;
            _ratePlanReadRepository = ratePlanReadRepository;
            _reservationItemReadRepository = reservationItemReadRepository;
            _reservationBudgetDomainService = reservationBudgetDomainService;
            _reservationBudgetRepository = reservationBudgetRepository;
            _reservationBudgetReadRepository = reservationBudgetReadRepository;
            _reservationReadRepository = reservationReadRepository;
            _localizationManager = localizationManager;
            _roomTypeReadRepository = roomTypeReadRepository;
            _propertyRateStrategyReadRepository = propertyRateStrategyReadRepository;
            _availabilityAppService = availabilityAppService;
            _roomTypeDomainService = roomTypeDomainService;
            _roomReadRepository = roomReadRepository;
            _roomBlockingReadRepository = roomBlockingReadRepository;
            _roomTypeInventoryRepository = roomTypeInventoryRepository;
            _applicationUser = applicationUser;
        }

        public IList<ReservationBudget> GetAllByReservationItemId(long reservationItemId)
        {
            return _reservationBudgetReadRepository.GetAllByReservationItemId(reservationItemId);
        }

        public async Task<ReservationItemBudgetHeaderDto> GetWithBudgetOffer(ReservationItemBudgetRequestDto requestDto, int propertyId)
        {
            var result = new ReservationItemBudgetHeaderDto()
            {
                CommercialBudgetList = new List<ReservationItemCommercialBudgetDto>()
            };

            ValidateReservationBudget(requestDto);

            if (Notification.HasNotification())
                return null;

            var childParameterList = await _propertyParameterReadRepository.GetChildrenAgeGroupList(propertyId);

            ReservationItemDto reservationItem = requestDto.ReservationItemId.HasValue ? _reservationItemReadRepository.GetByIdNoTracking(requestDto.ReservationItemId.Value) : null;

            if (reservationItem != null)
            {
                result.GratuityTypeId = reservationItem.GratuityTypeId;
                result.CurrencyId = reservationItem.CurrencyId;
                result.ReservationBudgetCurrencySymbol = reservationItem.Currency?.Symbol;
                result.RatePlanId = reservationItem.RatePlanId;
                result.MealPlanTypeId = reservationItem.MealPlanTypeId;
                result.ReservationItemId = reservationItem.Id;
                result.ReservationItemCode = reservationItem.ReservationItemCode;

                var reservationBudgetList = reservationItem.ReservationBudgetList.OrderBy(r => r.BudgetDay.Date).ToList();

                var reservationBudgetListDto = new List<ReservationBudgetDto>();

                foreach (var reservationBudget in reservationBudgetList)
                {
                    reservationBudget.ReservationBudgetUid = Guid.Empty;
                    reservationBudgetListDto.Add(reservationBudget);
                }

                result.ReservationBudgetDtoList = reservationBudgetListDto;

                if (!requestDto.CompanyClientId.HasValue)
                    requestDto.CompanyClientId = _reservationReadRepository.GetCompanyClientIdByReservationId(reservationItem.ReservationId);

                if (!requestDto.AdultCount.HasValue)
                    requestDto.AdultCount = reservationItem.GuestReservationItemList.Count(e => !e.IsChild);

                if (requestDto.ChildrenAge == null)
                    requestDto.ChildrenAge = reservationItem.GuestReservationItemList.Where(e => e.IsChild).Select(e => e.ChildAge.HasValue ? Convert.ToInt32(e.ChildAge.Value) : 0).ToList();
            }

            if (requestDto.RoomTypeIdList == null)
                requestDto.RoomTypeIdList = new List<int> { requestDto.RoomTypeId.Value };

            List<int?> newListRoomTypeId = new List<int?>(requestDto.RoomTypeIdList.Count);
            foreach (var i in requestDto.RoomTypeIdList)
                newListRoomTypeId.Add(i);

            var allMealPlanTypes = _propertyMealPlanTypeAppService.GetAllPropertyMealPlanTypeByProperty(propertyId);
            var allPropertyBaseRateTask = _propertyBaseRateReadRepository.GetAllByPeriod(propertyId, requestDto.RoomTypeIdList, requestDto.CurrencyId, requestDto.MealPlanTypeId, requestDto.InitialDate, requestDto.FinalDate, true);
            var allRatePlansTask = _ratePlanReadRepository.GetAllCompleteRatePlan(propertyId, requestDto.InitialDate, requestDto.FinalDate, requestDto.CompanyClientId, newListRoomTypeId);
            var allFixedRatesTask = _propertyBaseRateReadRepository.GetAllByPeriod(propertyId, requestDto.RoomTypeIdList, requestDto.CurrencyId, requestDto.MealPlanTypeId, requestDto.InitialDate, requestDto.FinalDate, true, true);
            var allRateStrategyTask = _propertyRateStrategyReadRepository.GetAllWithRatePlanByPropertyIdAsync(propertyId);
            var reservationItemsTask = _reservationItemReadRepository.GetAvailabilityReservationsItemsByPeriodAndPropertyIdAsync(requestDto.InitialDate, requestDto.FinalDate, propertyId, false);
            var roomTypesListTask = _roomTypeReadRepository.GetAvailabilityRoomTypeRowAsync(propertyId);

            await Task.WhenAll(allPropertyBaseRateTask, allRatePlansTask, allFixedRatesTask, allRateStrategyTask, reservationItemsTask, roomTypesListTask);
  
            var allPropertyBaseRate = allPropertyBaseRateTask.Result;
            var allRatePlans = allRatePlansTask.Result;
            var allFixedRates = allFixedRatesTask.Result;
            var allRateStrategy = allRateStrategyTask.Result;
            var reservationItems = reservationItemsTask.Result;
            var roomTypesList = roomTypesListTask.Result;
            var availabilityList = _roomTypeDomainService.GetAvailabilityRoomTypeColumns(reservationItems, roomTypesList);
            var roomTypesRowTotal = roomTypesList.Sum(e => e.Total);
            var ocupationsList = _roomTypeDomainService.GetAvailabilityTotalsAndPercentageByDate(availabilityList, requestDto.InitialDate, requestDto.FinalDate, roomTypesRowTotal).Value;

            var roomTypeInventoryList = _roomTypeInventoryRepository.GetAllByPeriod(requestDto.InitialDate.Date, requestDto.FinalDate.Date).ToList();

            //BASE RATE AND RATE PLAN
            await ProccessBaseRateWithRatePlan(requestDto, result, childParameterList, allPropertyBaseRate, allRatePlans, allMealPlanTypes.Items, requestDto.RoomTypeIdList, propertyId, allRateStrategy, ocupationsList, roomTypeInventoryList);

            //RATE PLAN WITH FIXED RATE TYPE
            await ProccessWithFixedRatePlan(requestDto, result, childParameterList, allFixedRates, allRatePlans, allMealPlanTypes.Items, requestDto.RoomTypeIdList, propertyId, roomTypeInventoryList);


            return result;
        }

        public void ValidateMigratedReservationBudget(ICollection<ReservationBudget> reservationBudgetList, ICollection<ReservationBudgetDto> reservationBudgetDtoList)
        {
            var notificationBudgetDayChange = 0;
            var notificationManualRateChange = 0;
            var notificationCurrencyChange = 0;

            if (reservationBudgetList.Count != reservationBudgetDtoList.Count)
                notificationBudgetDayChange++;

            if (reservationBudgetList.Count == reservationBudgetDtoList.Count)
            {
                foreach (var budgetDto in reservationBudgetDtoList)
                {
                    var budgetEntity = reservationBudgetList.FirstOrDefault(rb => rb.Id == budgetDto.Id || rb.BudgetDay == budgetDto.BudgetDay);
                    if(budgetEntity == null)
                        notificationBudgetDayChange++;
                    else
                    {
                        if (budgetEntity.ManualRate != budgetDto.ManualRate)
                            notificationManualRateChange++;

                        if (budgetEntity.BudgetDay != budgetDto.BudgetDay)
                            notificationBudgetDayChange++;

                        if (budgetEntity.CurrencyId != budgetDto.CurrencyId)
                            notificationCurrencyChange++;
                    }
                }
            }

            if (notificationBudgetDayChange > 0)
                Notification.Raise(Notification.DefaultBuilder
                        .WithMessage(AppConsts.LocalizationSourceName, ReservationBudget.EntityError.CanNotChangeBudgetDayFromMigratedReservation)
                        .WithDetailedMessage(AppConsts.LocalizationSourceName, ReservationBudget.EntityError.CanNotChangeBudgetDayFromMigratedReservation)
                        .Build());

            if (notificationManualRateChange > 0)
                Notification.Raise(Notification.DefaultBuilder
                        .WithMessage(AppConsts.LocalizationSourceName, ReservationBudget.EntityError.CanNotChangeManualRateFromMigratedReservation)
                        .WithDetailedMessage(AppConsts.LocalizationSourceName, ReservationBudget.EntityError.CanNotChangeManualRateFromMigratedReservation)
                        .Build());

            if (notificationCurrencyChange > 0)
                Notification.Raise(Notification.DefaultBuilder
                        .WithMessage(AppConsts.LocalizationSourceName, ReservationBudget.EntityError.CanNotChangeCurrencyIdFromMigratedReservation)
                        .WithDetailedMessage(AppConsts.LocalizationSourceName, ReservationBudget.EntityError.CanNotChangeCurrencyIdFromMigratedReservation)
                        .Build());

        }

        private async Task ProccessBaseRateWithRatePlan(ReservationItemBudgetRequestDto requestDto, ReservationItemBudgetHeaderDto result, List<PropertyParameterDto> childParameterList, List<PropertyBaseRate> allPropertyBaseRate, List<Domain.JoinMap.RatePlanBaseJoinMap> allRatePlans, IList<PropertyMealPlanTypeDto> allMealPlanTypes, List<int> roomTypeIdList, int propertyId, IList<GetAllPropertyRateStrategyWithRatePlanDto> allRateStrategy, List<Dto.Availability.AvailabilityFooterColumnDto> ocupationsList, IList<RoomTypeInventory> roomTypeInventoryList)
        {
            var order = await _roomTypeReadRepository.GetAllRoomTypesByPropertyIdAsync(propertyId);

            foreach (var roomTypeId in roomTypeIdList)
            {
                var distinctPropertyBaseRate = allPropertyBaseRate
                                                .Select(e => new
                                                {
                                                    e.MealPlanTypeId,
                                                    e.CurrencyId,
                                                    e.Currency.Symbol,
                                                    e.RoomType.Abbreviation,
                                                    e.RoomTypeId,
                                                    e.RatePlanId
                                                })
                                                .Where(e => e.RoomTypeId == roomTypeId)
                                                .Distinct()
                                                .ToList();

                foreach (var baseRateGroup in distinctPropertyBaseRate)
                {
                    var firstBaseRate = allPropertyBaseRate.Where(e => e.CurrencyId == baseRateGroup.CurrencyId && e.RoomTypeId == baseRateGroup.RoomTypeId).OrderBy(e => e.Date).FirstOrDefault();
                    var mealPlanType = allMealPlanTypes.FirstOrDefault(e => e.MealPlanTypeId == baseRateGroup.MealPlanTypeId);
                    var mealPlanTypeDefault = allMealPlanTypes.FirstOrDefault(e => e.MealPlanTypeId == firstBaseRate.MealPlanTypeDefault);

                    var commercialBudget = new ReservationItemCommercialBudgetDto
                    {
                        CurrencyId = baseRateGroup.CurrencyId,
                        MealPlanTypeId = baseRateGroup.MealPlanTypeId,
                        RoomTypeId = baseRateGroup.RoomTypeId,
                        Order = order.Items.FirstOrDefault(x => x.Id.Equals(roomTypeId)).Order,
                        CommercialBudgetDayList = new List<ReservationItemCommercialBudgetDayDto>(),
                        CommercialBudgetName = _localizationManager.GetString(AppConsts.LocalizationSourceName, PropertyBaseRateEnum.Info.PropertyBaseRateName.ToString()),
                        CurrencySymbol = baseRateGroup.Symbol,
                        RoomTypeAbbreviation = baseRateGroup.Abbreviation,
                        TotalBudget = 0,
                        MealPlanTypeDefaultId = mealPlanTypeDefault != null? mealPlanTypeDefault.MealPlanTypeId : 0,
                        IsMealPlanTypeDefault = mealPlanTypeDefault?.MealPlanTypeId == baseRateGroup.MealPlanTypeId,
                        MealPlanTypeDefaultName = mealPlanTypeDefault?.PropertyMealPlanTypeName,
                        MealPlanTypeName = mealPlanType?.PropertyMealPlanTypeName                       
                    };

                    foreach (var propertybaseRate in allPropertyBaseRate.Where(e => e.MealPlanTypeId == baseRateGroup.MealPlanTypeId && e.CurrencyId == baseRateGroup.CurrencyId && e.RoomTypeId == baseRateGroup.RoomTypeId).OrderBy(e => e.Date))
                    {
                        var baseRate = _reservationBudgetDomainService.CalculateBaseRate(requestDto.AdultCount ?? 0, requestDto.ChildrenAge, childParameterList, propertybaseRate, propertybaseRate.RoomType);
                        
                        var rateStrategyDayList = allRateStrategy.FirstOrDefault(x => x.StartDate.Date <= propertybaseRate?.Date.Date && x.EndDate >= propertybaseRate?.Date.Date &&
                                                                                     x.PropertyRateStrategyRoomTypeList.Contains(x.PropertyRateStrategyRoomTypeList.FirstOrDefault(y => y.RoomTypeId == roomTypeId)))
                                                                                     ?.PropertyRateStrategyRoomTypeList;

                        var ocupationDay = ocupationsList.FirstOrDefault(x => DateTime.Parse(x.Date).Date == propertybaseRate?.Date.Date);

                        var rateStrategyWithOcupationDay = rateStrategyDayList?.FirstOrDefault(x => x.MinOccupationPercentual <= ocupationDay?.Number && x.MaxOccupationPercentual >= ocupationDay?.Number);


                        var isDecreasedStrategy = rateStrategyWithOcupationDay == null ? false : rateStrategyWithOcupationDay.IsDecreased;
                        var percentualStrategyAmount = rateStrategyWithOcupationDay?.PercentualAmount;
                        var strategyIncrementAmount = rateStrategyWithOcupationDay?.IncrementAmount;

                        var baseRateAmount = baseRate.BaseRateAmount ?? 0;
                        var childRateAmount = baseRate.ChildRateAmount ?? 0;

                        var baseRateAmountWithStrategy = CalculateWithRateStrategyAmount(baseRateAmount, isDecreasedStrategy, percentualStrategyAmount, strategyIncrementAmount);
                        var childRateAmountWithStrategy = CalculateWithRateStrategyAmount(childRateAmount, isDecreasedStrategy, percentualStrategyAmount, strategyIncrementAmount);

                        var roomTypeInventory = roomTypeInventoryList.FirstOrDefault(r => r.Date.Date == propertybaseRate.Date.Date && r.RoomTypeId == roomTypeId);

                        var budgetDay = new ReservationItemCommercialBudgetDayDto
                        {
                            CurrencyId = propertybaseRate.CurrencyId,
                            MealPlanTypeId = propertybaseRate.MealPlanTypeId,
                            CurrencySymbol = propertybaseRate.Currency.Symbol,
                            Day = propertybaseRate.Date.Date,
                            BaseRateAmount = baseRateAmountWithStrategy,
                            ChildRateAmount = childRateAmountWithStrategy,
                            MealPlanAmount = baseRate.MealPlanAmount ?? 0,
                            ChildMealPlanAmount = baseRate.ChildMealPlanAmount ?? 0,
                            BaseRateHeaderHistoryId = propertybaseRate.PropertyBaseRateHeaderHistoryId,
                            Overbooking = roomTypeInventory == null ? false : roomTypeInventory.Balance < 0,
                            OverbookingLimit = roomTypeInventory == null ? false : roomTypeInventory.Balance == 0,
                            MinimumRate = propertybaseRate.RoomType.MinimumRate,
                            MaximumRate = propertybaseRate.RoomType.MaximumRate
                        };

                        commercialBudget.TotalBudget = commercialBudget.TotalBudget + budgetDay.Total;
                        commercialBudget.CommercialBudgetDayList.Add(budgetDay);
                    }

                    result.CommercialBudgetList.Add(commercialBudget);

                    var withoutBaseRateInPeriod = false;

                    //RATE PLANS
                    foreach (var ratePlan in allRatePlans.Where(e => e.RatePlan.RateTypeId == (int)RatePlanTypeEnum.BaseRate && e.RoomType.Id == roomTypeId && e.Currency.Id == baseRateGroup.CurrencyId))
                    {
                        var ratePlanCommercialBudget = new ReservationItemCommercialBudgetDto
                        {
                            CurrencyId = commercialBudget.CurrencyId,
                            RoomTypeId = commercialBudget.RoomTypeId,
                            Order = commercialBudget.Order,
                            AgreementTypeId = ratePlan.RatePlan.AgreementTypeId,
                            CommercialBudgetDayList = new List<ReservationItemCommercialBudgetDayDto>(),
                            CommercialBudgetName = ratePlan.RatePlan.AgreementName,
                            CurrencySymbol = commercialBudget.CurrencySymbol,
                            RoomTypeAbbreviation = commercialBudget.RoomTypeAbbreviation,
                            TotalBudget = 0,
                            RatePlanId = ratePlan.RatePlan.Id,
                            RateVariation = ratePlan.RatePlanRoomType.IsDecreased ? ratePlan.RatePlanRoomType.PercentualAmmount * -1 : ratePlan.RatePlanRoomType.PercentualAmmount,
                            MealPlanTypeId = commercialBudget.MealPlanTypeId,
                            MealPlanTypeName = commercialBudget.MealPlanTypeName,
                            MealPlanTypeDefaultId = commercialBudget.MealPlanTypeDefaultId,
                            IsMealPlanTypeDefault = commercialBudget.IsMealPlanTypeDefault,
                            MealPlanTypeDefaultName = commercialBudget.MealPlanTypeName                           
                        };

                        var dateRatePlan = requestDto.InitialDate.Date;
                        while (dateRatePlan <= requestDto.FinalDate.Date)
                        {
                            var propertyBaseRateBudgetDay = commercialBudget.CommercialBudgetDayList.FirstOrDefault(e => e.Day.Date == dateRatePlan.Date);                           

                            if (propertyBaseRateBudgetDay != null)
                            {
                                var isDecreased = ratePlan.RatePlanRoomType.IsDecreased;
                                var percentualAmmount = ratePlan.RatePlanRoomType.PercentualAmmount;

                                var budgetDay = new ReservationItemCommercialBudgetDayDto
                                {
                                    CurrencyId = baseRateGroup.CurrencyId,
                                    MealPlanTypeId = baseRateGroup.MealPlanTypeId,
                                    CurrencySymbol = baseRateGroup.Symbol,
                                    Day = dateRatePlan,
                                    RatePlanId = ratePlan.RatePlan.Id,

                                    BaseRateAmount = propertyBaseRateBudgetDay == null ? 0 : propertyBaseRateBudgetDay.BaseRateAmount,
                                    ChildRateAmount = propertyBaseRateBudgetDay == null ? 0 : propertyBaseRateBudgetDay.ChildRateAmount,
                                    MealPlanAmount = propertyBaseRateBudgetDay == null ? 0 : propertyBaseRateBudgetDay.MealPlanAmount,
                                    ChildMealPlanAmount = propertyBaseRateBudgetDay == null ? 0 : propertyBaseRateBudgetDay.ChildMealPlanAmount,                                   

                                    BaseRateAmountWithRatePlan = CalculateWithRatePlanAmount(propertyBaseRateBudgetDay.BaseRateAmount, isDecreased, percentualAmmount, ratePlan.RatePlan.RoundingTypeId),
                                    ChildRateAmountWithRatePlan = CalculateWithRatePlanAmount(propertyBaseRateBudgetDay.ChildRateAmount, isDecreased, percentualAmmount, ratePlan.RatePlan.RoundingTypeId),
                                    MealPlanAmountWithRatePlan = CalculateWithRatePlanAmount(propertyBaseRateBudgetDay.MealPlanAmount, isDecreased, percentualAmmount, ratePlan.RatePlan.RoundingTypeId),
                                    ChildMealPlanAmountWithRatePlan = CalculateWithRatePlanAmount(propertyBaseRateBudgetDay.ChildMealPlanAmount, isDecreased, percentualAmmount, ratePlan.RatePlan.RoundingTypeId),
                                    BaseRateHeaderHistoryId = propertyBaseRateBudgetDay.BaseRateHeaderHistoryId,
                                    Overbooking = propertyBaseRateBudgetDay.Overbooking,
                                    OverbookingLimit = propertyBaseRateBudgetDay.OverbookingLimit,

                                    MinimumRate = ratePlan.RoomType.MinimumRate,
                                    MaximumRate = ratePlan.RoomType.MaximumRate
                                };

                                ratePlanCommercialBudget.TotalBudget = ratePlanCommercialBudget.TotalBudget + budgetDay.Total;
                                ratePlanCommercialBudget.CommercialBudgetDayList.Add(budgetDay);

                            }
                            dateRatePlan = dateRatePlan.AddDays(1);
                        }

                        if (!withoutBaseRateInPeriod)
                            result.CommercialBudgetList.Add(ratePlanCommercialBudget);

                    }
                }
            }
        }

        private async Task ProccessWithFixedRatePlan(ReservationItemBudgetRequestDto requestDto, ReservationItemBudgetHeaderDto result, List<PropertyParameterDto> childParameterList, List<PropertyBaseRate> allPropertyFixedRate, List<Domain.JoinMap.RatePlanBaseJoinMap> allRatePlans, IList<PropertyMealPlanTypeDto> allMealPlanTypes, List<int> roomTypeIdList, int propertyId, IList<RoomTypeInventory> roomTypeInventoryList)
        {
            var order = await _roomTypeReadRepository.GetAllRoomTypesByPropertyIdAsync(propertyId);

            foreach (var roomTypeId in roomTypeIdList)
            {
                //varrer as tarifas bases
                //varrer os rateplans
                var distinctPropertyFixedRate = allPropertyFixedRate
                                                    .Select(e => new
                                                    {
                                                        e.MealPlanTypeId,
                                                        e.CurrencyId,
                                                        e.Currency.Symbol,
                                                        e.RoomType.Abbreviation,
                                                        e.RoomTypeId
                                                    })
                                                    .Where(e => e.RoomTypeId == roomTypeId)
                                                    .Distinct()
                                                    .ToList();

                //RATE PLANS
                foreach (var ratePlan in allRatePlans.Where(e => e.RatePlan.RateTypeId == (int)RatePlanTypeEnum.FlatRate))
                {
                    foreach (var baseRateGroup in distinctPropertyFixedRate)
                    {
                        var mealPlanType = allMealPlanTypes.FirstOrDefault(e => e.MealPlanTypeId == baseRateGroup.MealPlanTypeId);
                        var mealPlanTypeDefault = allMealPlanTypes.FirstOrDefault(e => e.MealPlanTypeId == ratePlan.RatePlan.MealPlanTypeId);

                        var commercialBudget = new ReservationItemCommercialBudgetDto
                        {
                            CurrencyId = baseRateGroup.CurrencyId,
                            MealPlanTypeId = baseRateGroup.MealPlanTypeId,
                            RoomTypeId = baseRateGroup.RoomTypeId,
                            Order = order.Items.FirstOrDefault(x => x.Id.Equals(roomTypeId)).Order,
                            RateVariation = 0,
                            CommercialBudgetDayList = new List<ReservationItemCommercialBudgetDayDto>(),
                            CommercialBudgetName = ratePlan.RatePlan.AgreementName,
                            CurrencySymbol = baseRateGroup.Symbol,
                            RoomTypeAbbreviation = baseRateGroup.Abbreviation,
                            TotalBudget = 0,
                            RatePlanId = ratePlan.RatePlan.Id,
                            MealPlanTypeDefaultId = mealPlanTypeDefault != null? mealPlanTypeDefault.MealPlanTypeId : default(int),
                            IsMealPlanTypeDefault = mealPlanTypeDefault?.MealPlanTypeId == baseRateGroup.MealPlanTypeId,
                            MealPlanTypeDefaultName = mealPlanTypeDefault?.PropertyMealPlanTypeName,
                            MealPlanTypeName = mealPlanType?.PropertyMealPlanTypeName
                        };

                        foreach (var propertyFixedRate in allPropertyFixedRate.Where(e => e.MealPlanTypeId == baseRateGroup.MealPlanTypeId && e.RatePlanId == ratePlan.RatePlan.Id && e.CurrencyId == baseRateGroup.CurrencyId && e.RoomTypeId == baseRateGroup.RoomTypeId).OrderBy(e => e.Date))
                        {
                            var baseRate = _reservationBudgetDomainService.CalculateBaseRate(requestDto.AdultCount ?? 0, requestDto.ChildrenAge, childParameterList, propertyFixedRate, propertyFixedRate.RoomType);

                            var roomTypeInventory = roomTypeInventoryList.FirstOrDefault(r => r.Date.Date == propertyFixedRate.Date.Date && r.RoomTypeId == roomTypeId);

                            var budgetDay = new ReservationItemCommercialBudgetDayDto
                            {
                                CurrencyId = propertyFixedRate.CurrencyId,
                                MealPlanTypeId = propertyFixedRate.MealPlanTypeId,
                                CurrencySymbol = propertyFixedRate.Currency.Symbol,
                                Day = propertyFixedRate.Date.Date,
                                BaseRateAmount = baseRate.BaseRateAmount ?? 0,
                                ChildRateAmount = baseRate.ChildRateAmount ?? 0,
                                MealPlanAmount = baseRate.MealPlanAmount ?? 0,
                                ChildMealPlanAmount = baseRate.ChildMealPlanAmount ?? 0,
                                BaseRateHeaderHistoryId = propertyFixedRate.PropertyBaseRateHeaderHistoryId,
                                MinimumRate = propertyFixedRate.RoomType.MinimumRate,
                                MaximumRate = propertyFixedRate.RoomType.MaximumRate,
                                Overbooking = roomTypeInventory == null ? false : roomTypeInventory.Balance < 0,
                                OverbookingLimit = roomTypeInventory == null ? false : roomTypeInventory.Balance == 0,
                            };

                            commercialBudget.TotalBudget = commercialBudget.TotalBudget + budgetDay.Total;
                            commercialBudget.CommercialBudgetDayList.Add(budgetDay);
                        }

                        if (commercialBudget.CommercialBudgetDayList.Count > 0)
                            result.CommercialBudgetList.Add(commercialBudget);
                    }
                }
            }
        }

        private decimal CalculateWithRatePlanAmount(decimal amount, bool isDecreased, decimal percentualAmmount, int? roundingTypeId)
        {
            return _reservationBudgetDomainService.CalculateWithRatePlanAmount(amount, isDecreased, percentualAmmount, roundingTypeId);
        }

        private decimal CalculateWithRateStrategyAmount(decimal strategyAmount, bool isDecreasedStrategy, decimal? percentualStrategyAmount, decimal? strategyIncrementAmount)
        {
            return _reservationBudgetDomainService.CalculateWithRateStrategyAmount(strategyAmount, isDecreasedStrategy, percentualStrategyAmount, strategyIncrementAmount);
        }

        private void ValidateReservationBudget(ReservationItemBudgetRequestDto requestDto)
        {
            //tipo de quarto
            if (requestDto.RoomTypeIdList != null && requestDto.RoomTypeIdList.Any(x => x.Equals(0)))
                NotifyParameterInvalid();

            if (requestDto.RoomTypeIdList == null && requestDto.RoomTypeId == 0)
                NotifyParameterInvalid();

            //periodo
            if (requestDto.InitialDate == DateTime.MinValue || requestDto.FinalDate == DateTime.MinValue)
                NotifyParameterInvalid();

            if (!requestDto.ReservationItemId.HasValue && !requestDto.AdultCount.HasValue)
                NotifyParameterInvalid();
        }

        public void UpdateReservationBudgetList(int propertyId, ReservationItem reservationItem, ReservationItemWithBudgetDto dto)
        {
            List<ReservationBudgetDto> reservationBudgetDtoList = dto.ReservationBudgetDtoList;
            DateTime? systemDate = _propertyParameterReadRepository.GetSystemDate(propertyId);

            if (!systemDate.HasValue)
                return;

            var reservationBudgetList = new List<ReservationBudget>();

            foreach (var reservationBudgetDto in reservationBudgetDtoList.Where(x => x.BudgetDay.Date >= systemDate.Value.Date))
            {
                reservationBudgetDto.CurrencyId = dto.CurrencyId.Value;
                reservationBudgetDto.ReservationItemId = reservationItem.Id;
                reservationBudgetList.Add(GetReservationBudgetEntity(reservationBudgetDto));
            }

            if (Notification.HasNotification() || reservationBudgetList.Count == 0)
                return;

            _reservationBudgetRepository.RemoveAndAddRange(reservationItem, reservationBudgetList, systemDate.Value.Date);
        }

        public ReservationBudget GetReservationBudgetEntity(ReservationBudgetDto reservationBudgetDto)
        {
            var reservationBudgetBuilder = ReservationBudgetAdapter.Map(reservationBudgetDto);

            return reservationBudgetBuilder.Build();
        }

        public async Task UpdateByReservationItemId(int propertyId, long reservationItemId)
        {
            //pegar todas os budgets das acomodações
            var allBudgets = _reservationBudgetReadRepository.GetAllByReservationItemId(reservationItemId);

            if (allBudgets == null || allBudgets.Count == 0)
                return;

            var childParameterList = await _propertyParameterReadRepository.GetChildrenAgeGroupList(propertyId);
            var reservationItem = _reservationItemReadRepository.GetByIdNoTracking(reservationItemId);

            if (reservationItem.GratuityTypeId.HasValue)
                return;

            var receivedRoomTypeIdList = new List<int>() { reservationItem.ReceivedRoomTypeId };

            var initialDate = (reservationItem.CheckInDate ?? reservationItem.EstimatedArrivalDate).Date;
            var finalDate = (reservationItem.CheckOutDate ?? reservationItem.EstimatedDepartureDate).Date;
            var baseRate = await _propertyBaseRateReadRepository.GetAllByPeriod(propertyId, receivedRoomTypeIdList,
                                                            reservationItem.CurrencyId, reservationItem.MealPlanTypeId,
                                                            initialDate,
                                                            finalDate);

            if (baseRate.Count == 0)
            {
                RaiseNotification(PropertyBaseRateEnum.Error.PropertyBaseRateWasNotFound, PropertyBaseRateEnum.Error.PropertyBaseRateWasNotFound);
                return;
            }

            var budgetList = new List<ReservationBudget>();
            foreach (var budget in allBudgets)
            {
                var baseRateDay = baseRate.FirstOrDefault(e => e.Date.Date == budget.BudgetDay);


                if (baseRateDay == null)
                {
                    RaiseNotification(PropertyBaseRateEnum.Error.PropertyBaseRateWasNotFound, PropertyBaseRateEnum.Error.PropertyBaseRateWasNotFound);
                    return;
                }

                var adultCount = reservationItem.GuestReservationItemList.Count(e => !e.IsChild);
                var childrenAge = reservationItem.GuestReservationItemList.Where(e => e.IsChild).Select(e => e.ChildAge.HasValue ? Convert.ToInt32(e.ChildAge.Value) : 0).ToList();
                var baseRateAmount = _reservationBudgetDomainService.CalculateBaseRate(adultCount, childrenAge, childParameterList, baseRateDay, baseRateDay.RoomType);

                var budgetDto = new ReservationBudgetDto
                {
                    Id = budget.Id,
                    CurrencyId = budget.CurrencyId,
                    CurrencySymbol = budget.CurrencySymbol,
                    Discount = 0,
                    MealPlanTypeId = budget.MealPlanTypeId,
                    RateVariation = 0,
                    ReservationBudgetUid = budget.ReservationBudgetUid,
                    ReservationItemId = budget.ReservationItemId,
                    ManualRate = baseRateAmount.Total,
                    SeparatedRate = budget.SeparatedRate,

                    BudgetDay = budget.BudgetDay,

                    BaseRate = baseRateAmount.BaseRateAmount ?? 0,
                    ChildRate = baseRateAmount.ChildRateAmount ?? 0,

                    MealPlanTypeBaseRate = baseRateAmount.MealPlanAmount,
                    ChildMealPlanTypeRate = baseRateAmount.ChildMealPlanAmount,

                    PropertyBaseRateHeaderHistoryId = baseRateDay.PropertyBaseRateHeaderHistoryId,

                    AgreementRate = baseRateAmount.Total,
                    ChildAgreementRate = 0,
                    ChildMealPlanTypeAgreementRate = 0,
                    MealPlanTypeAgreementRate = 0
                };

                budgetList.Add(ReservationBudgetAdapter.Map(budgetDto).Build());
            }

            if (Notification.HasNotification())
                return;

            _reservationBudgetRepository.UpdateRange(budgetList);
        }

        public async Task UpdateByReservationId(int propertyId, long reservationId)
        {
            //pegar todas os budgets de todas acomodações da reserva
            var allBudgets = _reservationBudgetReadRepository.GetAllByReservationId(reservationId);
            DateTime? systemDate = _propertyParameterReadRepository.GetSystemDate(propertyId);

            if (allBudgets == null || allBudgets.Count == 0 || !systemDate.HasValue)
                return;

            var childParameterList = await _propertyParameterReadRepository.GetChildrenAgeGroupList(propertyId);
            var distinctReservationItemId = allBudgets.Select(e => e.ReservationItemId).Distinct();
            var budgetList = new List<ReservationBudget>();

            var paramEditMigratedReservation = _propertyParameterReadRepository.GetPropertyParameterForPropertyIdAndApplicationParameterId(int.Parse(_applicationUser.PropertyId), (int)ApplicationParameterEnum.ParameterAllowEditMigratedReservation);

            foreach (var reservationItemId in distinctReservationItemId)
            {
                var reservationItem = _reservationItemReadRepository.GetByIdNoTracking(reservationItemId);

                if (reservationItem.GratuityTypeId.HasValue)
                    return;

                var receivedRoomTypeIdList = new List<int>() { reservationItem.ReceivedRoomTypeId };

                var initialDate = (reservationItem.CheckInDate ?? reservationItem.EstimatedArrivalDate).Date;
                var finalDate = (reservationItem.CheckOutDate ?? reservationItem.EstimatedDepartureDate).Date;
                var baseRate = await _propertyBaseRateReadRepository.GetAllByPeriod(propertyId, receivedRoomTypeIdList,
                                                                reservationItem.CurrencyId, reservationItem.MealPlanTypeId,
                                                                initialDate,
                                                                finalDate, true);

                if (baseRate.Count == 0)
                {
                    RaiseNotification(PropertyBaseRateEnum.Error.PropertyBaseRateWasNotFound, PropertyBaseRateEnum.Error.PropertyBaseRateWasNotFound);
                    return;
                }

                foreach (var budget in allBudgets.Where(e => e.ReservationItemId == reservationItemId && e.BudgetDay.Date >= systemDate.Value.Date).ToList())
                {
                    var baseRateDay = baseRate.FirstOrDefault(e => e.Date.Date == budget.BudgetDay);
                    var adultCount = reservationItem.GuestReservationItemList.Count(e => !e.IsChild);
                    var childrenAge = reservationItem.GuestReservationItemList.Where(e => e.IsChild).Select(e => e.ChildAge.HasValue ? Convert.ToInt32(e.ChildAge.Value) : 0).ToList();
                    var baseRateAmount = _reservationBudgetDomainService.CalculateBaseRate(adultCount, childrenAge, childParameterList, baseRateDay, baseRateDay.RoomType);

                    var budgetDto = new ReservationBudgetDto
                    {
                        Id = budget.Id,
                        CurrencyId = budget.CurrencyId,
                        CurrencySymbol = budget.CurrencySymbol,
                        Discount = 0,
                        MealPlanTypeId = budget.MealPlanTypeId,
                        RateVariation = 0,
                        ReservationBudgetUid = budget.ReservationBudgetUid,
                        ReservationItemId = budget.ReservationItemId,
                        ManualRate = baseRateAmount.Total,
                        SeparatedRate = budget.SeparatedRate,

                        BudgetDay = budget.BudgetDay,

                        BaseRate = baseRateAmount.BaseRateAmount ?? 0,
                        ChildRate = baseRateAmount.ChildRateAmount ?? 0,

                        MealPlanTypeBaseRate = baseRateAmount.MealPlanAmount,
                        ChildMealPlanTypeRate = baseRateAmount.ChildMealPlanAmount,

                        PropertyBaseRateHeaderHistoryId = baseRateDay.PropertyBaseRateHeaderHistoryId,

                        AgreementRate = baseRateAmount.Total,
                        ChildAgreementRate = 0,
                        ChildMealPlanTypeAgreementRate = 0,
                        MealPlanTypeAgreementRate = 0
                    };

                    budgetList.Add(ReservationBudgetAdapter.Map(budgetDto).Build());
                }

                if (Notification.HasNotification())
                    return;

                if (reservationItem.IsMigrated && (paramEditMigratedReservation != null && !bool.Parse(paramEditMigratedReservation.PropertyParameterValue)))
                    ValidateMigratedReservationBudget(budgetList, reservationItem.ReservationBudgetList);

                if (Notification.HasNotification())
                    return;

            }
            _reservationBudgetRepository.UpdateRange(budgetList);

        }

        private void RaiseNotification(Enum notificationEnumMessage, Enum notificationEnumDetailedMessage)
        {
            var notification = Notification
                .DefaultBuilder
                .WithMessage(AppConsts.LocalizationSourceName, notificationEnumMessage);

            Notification.Raise(notification.Build());
        }

        public void ValidateReservationBudgetDays(int totalDaysBudget, DateTime? CheckInDate, DateTime? CheckOutDate, DateTime EstimatedArrivalDate, DateTime EstimatedDepartureDate, int reservationItemStatusId)
        {
            if (reservationItemStatusId >= (int)ReservationStatus.Checkin) return;

            var initialDate = (CheckInDate ?? EstimatedArrivalDate).Date;
            var finalDate = (CheckOutDate ?? EstimatedDepartureDate).Date;

            if (initialDate == finalDate)
                finalDate = finalDate.AddDays(1);

            var totalDays = (finalDate - initialDate).TotalDays;

            if (totalDays != totalDaysBudget)
                Notification.Raise(Notification.DefaultBuilder.WithMessage(AppConsts.LocalizationSourceName, ReservationBudgetEnum.Error.ReservationBudgetDayWasNotFound).Build());
        }

        public void UpdateEntranceBudget(IList<ReservationBudgetDto> reservationBudgetDtoList, long reservationItemId,  bool isSeparated, DateTime? systemDate)
        {
            if (reservationBudgetDtoList != null)
            {
                var currentBudgets = GetAllByReservationItemId(reservationItemId).Where(x => x.BudgetDay.Date >= systemDate.Value.Date).ToList();
                var newBudgets = reservationBudgetDtoList.Where(x => x.BudgetDay.Date >= systemDate.Value.Date).ToList();

                var budgets = ReservationBudgetAdapter.Map(currentBudgets, newBudgets, reservationItemId, isSeparated).ToList();
                _reservationBudgetRepository.UpdateRange(budgets);
            }
        }

        public void DeleteRange(DateTime systemDate, long reservationItemId, bool isEarlyDayUse)
            => _reservationBudgetRepository.RemoveRange(systemDate, reservationItemId, isEarlyDayUse);

        public void RemoveRangeNoShow(DateTime? systemDate, long reservationItemId)
            => _reservationBudgetRepository.RemoveRangeNoShow(systemDate, reservationItemId);
    }
}
