﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Thex.Application.Adapters;
using Thex.Application.Interfaces;
using Thex.Common;
using Thex.Common.Enumerations;
using Thex.Domain.Entities;
using Thex.Domain.Interfaces.Repositories;
using Thex.Dto;
using Thex.Infra.ReadInterfaces;
using Thex.Kernel;
using Tnf.Domain.Services;
using Tnf.Dto;
using Tnf.Notifications;
using Tnf.Repositories.Uow;

namespace Thex.Application.Services
{
    public class PropertyBaseRateAppService : ScaffoldPropertyBaseRateAppService, IPropertyBaseRateAppService
    {
        private readonly ICurrencyReadRepository _currencyReadRepository;
        private readonly ICurrencyAppService _currencyAppService;
        private readonly IPropertyMealPlanTypeReadRepository _propertyMealPlanTypeReadRepository;
        private readonly IRoomTypeReadRepository _roomTypeReadRepository;
        private readonly IPropertyParameterReadRepository _propertyParameterReadRepository;
        private readonly IPropertyBaseRateReadRepository _propertyBaseRateReadRepository;
        private readonly IPropertyBaseRateRepository _propertyBaseRateRepository;
        private readonly IPropertyBaseRateHeaderHistoryAppService _propertyBaseRateHeaderHistoryAppService;
        private readonly IPropertyBaseRateHeaderHistoryReadRepository _propertyBaseRateHeaderHistoryReadRepository;
        private readonly ILevelRateHeaderReadRepository _levelRateHeaderReadRepository;
        private readonly IDomainService<PropertyBaseRateHeaderHistory> _propertyBaseRateHeaderHistoryDomainService;
        private readonly IApplicationUser _applicationUser;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IPropertyPremiseHeaderReadRepository _premiseHeaderReadRepository;
        private readonly IPropertyMealPlanTypeRateReadRepository _propertyMealPlanTypeRateReadRepository;

        public PropertyBaseRateAppService(
            IUnitOfWorkManager unitOfWorkManager,
            IApplicationUser applicationUser,
            IPropertyBaseRateAdapter propertyBaseRateAdapter,
            IDomainService<PropertyBaseRate> propertyBaseRateDomainService,
            IDomainService<PropertyBaseRateHeaderHistory> propertyBaseRateHeaderHistoryDomainService,
            ICurrencyReadRepository currencyReadRepository,
            IPropertyMealPlanTypeReadRepository propertyMealPlanTypeReadRepository,
            IRoomTypeReadRepository roomTypeReadRepository,
            IPropertyParameterReadRepository propertyParameterReadRepository,
            IPropertyBaseRateReadRepository propertyBaseRateReadRepository,
            IPropertyMealPlanTypeRateReadRepository propertyMealPlanTypeRateReadRepository,
            ILevelRateHeaderReadRepository levelRateHeaderReadRepository,
            ICurrencyAppService currencyAppService,
            IPropertyBaseRateRepository propertyBaseRateRepository,
            IPropertyBaseRateHeaderHistoryAppService propertyBaseRateHeaderHistoryAppService,
            IPropertyBaseRateHeaderHistoryReadRepository propertyBaseRateHeaderHistoryReadRepository,
            INotificationHandler notificationHandler,
            IPropertyPremiseHeaderReadRepository premiseHeaderReadRepository)
            : base(propertyBaseRateAdapter, propertyBaseRateDomainService, notificationHandler)
        {
            _applicationUser = applicationUser;
            _unitOfWorkManager = unitOfWorkManager;
            _currencyReadRepository = currencyReadRepository;
            _propertyMealPlanTypeReadRepository = propertyMealPlanTypeReadRepository;
            _roomTypeReadRepository = roomTypeReadRepository;
            _propertyParameterReadRepository = propertyParameterReadRepository;
            _propertyBaseRateReadRepository = propertyBaseRateReadRepository;
            _currencyAppService = currencyAppService;
            _propertyBaseRateRepository = propertyBaseRateRepository;
            _propertyBaseRateHeaderHistoryAppService = propertyBaseRateHeaderHistoryAppService;
            _propertyBaseRateHeaderHistoryReadRepository = propertyBaseRateHeaderHistoryReadRepository;
            _levelRateHeaderReadRepository = levelRateHeaderReadRepository;
            _propertyBaseRateHeaderHistoryDomainService = propertyBaseRateHeaderHistoryDomainService;
            _premiseHeaderReadRepository = premiseHeaderReadRepository;
            _propertyMealPlanTypeRateReadRepository = propertyMealPlanTypeRateReadRepository;
        }

        public async Task<PropertyBaseRateConfigurationParametersDto> GetConfigurationDataParameters(int propertyId)
        {
            var mealPlanTypeTask = await _propertyMealPlanTypeReadRepository.GetAllPropertyMealPlanTypeByPropertyAsync(propertyId);
            var currencyTask = await _currencyReadRepository.GetAllCurrencyAsync();
            var roomTypeTask = await _roomTypeReadRepository.GetAllRoomTypesByPropertyIdAsync(propertyId);
            var propertyParameterTask = await _propertyParameterReadRepository.GetChildrenAgeGroupList(propertyId);

            var result = new PropertyBaseRateConfigurationParametersDto
            {
                MealPlanTypeList = mealPlanTypeTask,
                CurrencyList = currencyTask,
                RoomTypeList = roomTypeTask,
                PropertyParameterList = propertyParameterTask
            };

            return result;
        }

        public async Task<List<PropertyBaseRateHeaderHistoryDto>> GetPropertyBaseRateHeaderHistory(int propertyId)
        {
            var result = await _propertyBaseRateHeaderHistoryReadRepository.GetAllPropertyBaseRateHeaderHistoryByPropertyId(propertyId);

            var propertyBaseRateHeaderHistoryDtos = new List<PropertyBaseRateHeaderHistoryDto>();

            foreach (var propertyBaseRateHeaderHistoryDto in result)
                propertyBaseRateHeaderHistoryDtos.Add(propertyBaseRateHeaderHistoryDto);

            return propertyBaseRateHeaderHistoryDtos;
        }

        public async Task Create(BaseRateConfigurationDto dto)
        {
            ValidatePeriod(dto);

            if (Notification.HasNotification())
                return;

            dto.PropertyBaseRateTypeId = (int)PropertyBaseRateTypeEnum.Amount;
            using (var uow = _unitOfWorkManager.Begin())
            {
                var result = await GeneratePropertyBaseRate(dto);

                if (Notification.HasNotification()) return;

                _propertyBaseRateRepository.InsertHeaderHistoryRangeAndSaveChanges(new List<PropertyBaseRateHeaderHistory>() { result.PropertyBaseRateHeaderHistory });

                _propertyBaseRateRepository.CreateAndUpdate(result.PropertyBaseRateListToAdd.ToList(), result.PropertyBaseRateListToUpdate.ToList());

                uow.Complete();
            }
        }

        public async Task CreateByPremise(BaseRateConfigurationDto dto)
        {
            ValidateDto<BaseRateConfigurationDto>(dto, nameof(dto));

            if (Notification.HasNotification()) return;

            dto.PropertyBaseRateTypeId = (int)PropertyBaseRateTypeEnum.Premise;

            var result = await GeneratePropertyBaseRate(dto);

            if (Notification.HasNotification()) return;

            using (var uow = _unitOfWorkManager.Begin())
            {
                _propertyBaseRateRepository.InsertHeaderHistoryRangeAndSaveChanges(new List<PropertyBaseRateHeaderHistory>() { result.PropertyBaseRateHeaderHistory });

                _propertyBaseRateRepository.CreateAndUpdate(result.PropertyBaseRateListToAdd.ToList(), result.PropertyBaseRateListToUpdate.ToList());

                uow.Complete();
            }
        }

        public async Task CreateByLevelRate(PropertyBaseLevelRateDto dto)
        {
            var levelRateHeaderList = _levelRateHeaderReadRepository.GetAllByLevelRateHeaderIdList(dto.LevelRateIdList);
            if (levelRateHeaderList == null || !levelRateHeaderList.Any())
            {
                NotifyNullParameter();
                return;
            }

            var rangeDate = GenerateRangeDate(dto.InitialDate, dto.EndDate);
            var baseRateConfigurationDtoList = new List<BaseRateConfigurationDto>();
            foreach (var levelRateHeader in levelRateHeaderList)
            {
                var days = rangeDate.Where(d => levelRateHeader.InitialDate.Date <= d.Date && levelRateHeader.EndDate.Date >= d.Date).OrderBy(d => d);

                var baseRateConfigurationDto = new BaseRateConfigurationDto();
                baseRateConfigurationDto.PropertyMealPlanTypeRate = levelRateHeader.PropertyMealPlanTypeRate ?? false;
                baseRateConfigurationDto.PropertyBaseRateTypeId = (int)PropertyBaseRateTypeEnum.Level;
                baseRateConfigurationDto.MealPlanTypeDefaultId = levelRateHeader.LevelRateList.Any() ? levelRateHeader.LevelRateList.FirstOrDefault().MealPlanTypeDefault : (int)MealPlanTypeEnum.None;
                baseRateConfigurationDto.CurrencyId = levelRateHeader.CurrencyId;
                baseRateConfigurationDto.DaysOfWeekDto = dto.DaysOfWeekDto;
                baseRateConfigurationDto.StartDate = days.FirstOrDefault();
                baseRateConfigurationDto.FinalDate = days.LastOrDefault();
                baseRateConfigurationDto.LevelId = levelRateHeader.LevelId;
                baseRateConfigurationDto.LevelRateId = levelRateHeader.Id;
                baseRateConfigurationDto.PropertyId = int.Parse(_applicationUser.PropertyId);

                InsertRoomTypeAmountList(baseRateConfigurationDto, levelRateHeader);

                InsertMealPlanAmountByLevelRate(baseRateConfigurationDto, levelRateHeader);

                baseRateConfigurationDtoList.Add(baseRateConfigurationDto);
            }

            await SaveAllChangesByLevelRate(baseRateConfigurationDtoList.DistinctBy(x => x.LevelRateId).ToList());
        }

        private void ValidatePeriod(BaseRateConfigurationDto dto)
        {
            if (dto.StartDate.Date > dto.FinalDate.Date)
                NotifyInvalidRangeDate();

            var totalDays = (dto.FinalDate.Date - dto.StartDate.Date).TotalDays;

            if ((dto.FinalDate.Date - dto.StartDate.Date).TotalDays > 1000)
            {
                object[] validationObj = { totalDays };
                RaiseNotification(PropertyBaseRateEnum.Error.PropertyBaseRateInvalidPeriod, PropertyBaseRateEnum.Error.PropertyBaseRateInvalidPeriod, validationObj);
            }
        }

        private List<DateTime> CreateRangeOfDates(BaseRateConfigurationDto dto)
        {
            var systemDate = _propertyParameterReadRepository.GetSystemDate(dto.PropertyId);

            //monto as datas que serão salvas
            var startDate = dto.StartDate.Date;
            var finalDate = dto.FinalDate.Date;
            var rangeDates = new List<DateTime>();

            while (startDate <= finalDate)
            {
                if (dto.DaysOfWeekDto.Contains((int)startDate.DayOfWeek))
                    rangeDates.Add(startDate);

                startDate = startDate.AddDays(1);
            }

            return rangeDates.Where(d => d >= systemDate).ToList();
        }

        public async Task ValidatePropertyBaseRate(BaseRateConfigurationDto dto, IListDto<RoomTypeDto> roomTypeList, BaseRateConfigurationMealPlanTypeDto baseRateConfigurationMealPlanTypeDto)
        {
            if (dto.PropertyId == 0 || (!dto.PropertyMealPlanTypeRate && dto.MealPlanTypeDefaultId == 0) || dto.CurrencyId == Guid.Empty)
                NotifyParameterInvalid();

            var childParameterList = await _propertyParameterReadRepository.GetChildrenAgeGroupList(dto.PropertyId);

            foreach (var baseRoomType in dto.BaseRateConfigurationRoomTypeList)
            {
                var roomType = roomTypeList.Items.FirstOrDefault(e => e.Id == baseRoomType.RoomTypeId);

                if (roomType == null)
                    NotifyNullParameter();

                ValidatePaxAmount(baseRoomType, roomType, roomType.MinimumRate, roomType.MaximumRate, baseRateConfigurationMealPlanTypeDto);
                //ValidateChildAmount(baseRoomType, roomType, childParameterList);
            }
        }

        private void ValidatePaxAmount(BaseRateConfigurationRoomTypeDto baseRoomType, RoomTypeDto roomType, decimal paxMinValue, decimal paxMaxValue, BaseRateConfigurationMealPlanTypeDto baseRateConfigurationMealPlanTypeDto)
        {
            var maxAdultCapacity = roomType.AdultCapacity > 6 ? 6 : roomType.AdultCapacity;

            for (int i = 1; i <= maxAdultCapacity; i++)
            {
                var isPaxAddittional = i > 5;
                var paxValue = !isPaxAddittional ? (decimal?)baseRoomType["Pax" + i + "Amount"] : (decimal?)baseRoomType["PaxAdditionalAmount"];


                if ((!paxValue.HasValue || (!isPaxAddittional && (paxValue.Value > paxMaxValue || paxValue < paxMinValue))) && paxMaxValue != 0)
                {
                    object[] validationObj = { i, roomType.Abbreviation, paxMinValue, paxMaxValue };
                    RaiseNotification(PropertyBaseRateEnum.Error.PropertyBaseRateHasInvalidMinOrMaxValue, PropertyBaseRateEnum.Error.PropertyBaseRateHasInvalidMinOrMaxValue, validationObj);
                }

                //ultimo registro
                if (i == maxAdultCapacity)
                {
                    //os acima não podem estar preenchidos
                    if (i == 5)
                    {
                        var paxValueValidate = (decimal?)baseRoomType["PaxAdditionalAmount"];
                        if (paxValueValidate.HasValue)
                            if (paxValueValidate.HasValue)
                            {
                                object[] validationObj = { 6, roomType.Abbreviation };
                                RaiseNotification(PropertyBaseRateEnum.Error.PropertyBaseRatePaxCanNotHaveAValue, PropertyBaseRateEnum.Error.PropertyBaseRatePaxCanNotHaveAValue, validationObj);
                            }
                    }
                    else
                    {
                        for (int j = i + 1; j <= 6; j++)
                        {
                            var paxValueValidate = j < 6 ? (decimal?)baseRoomType["Pax" + j + "Amount"] : (decimal?)baseRoomType["PaxAdditionalAmount"];

                            if (paxValueValidate.HasValue)
                            {
                                object[] validationObj = { j, roomType.Abbreviation };
                                RaiseNotification(PropertyBaseRateEnum.Error.PropertyBaseRatePaxCanNotHaveAValue, PropertyBaseRateEnum.Error.PropertyBaseRatePaxCanNotHaveAValue, validationObj);
                            }
                        }
                    }
                }
            }
        }

        private void RaiseNotification(Enum notificationEnumMessage, Enum notificationEnumDetailedMessage, params object[] values)
        {
            var notification = Notification
                .DefaultBuilder
                .WithMessage(AppConsts.LocalizationSourceName, notificationEnumMessage);

            if (values.Length > 0)
                notification.WithMessageFormat(values);

            Notification.Raise(notification.Build());
        }

        private List<BaseRateConfigurationMealPlanTypeDto> BaseRateConfigurationMealPlanTypeDtoDefault(List<BaseRateConfigurationMealPlanTypeDto> baseRateConfigurationMealPlanTypeDtos, int MealPlanTypeDefaultId)
        {
            if (baseRateConfigurationMealPlanTypeDtos == null)
                baseRateConfigurationMealPlanTypeDtos = new List<BaseRateConfigurationMealPlanTypeDto>();

            if (!baseRateConfigurationMealPlanTypeDtos.Any(x => x.MealPlanTypeId.Equals(MealPlanTypeDefaultId)))
                baseRateConfigurationMealPlanTypeDtos.Add(new BaseRateConfigurationMealPlanTypeDto()
                {
                    MealPlanTypeId = MealPlanTypeDefaultId
                });

            if (!baseRateConfigurationMealPlanTypeDtos.Any(x => x.MealPlanTypeId.Equals((int)MealPlanTypeEnum.None)))
                baseRateConfigurationMealPlanTypeDtos.Add(new BaseRateConfigurationMealPlanTypeDto()
                {
                    MealPlanTypeId = (int)MealPlanTypeEnum.None
                });

            return baseRateConfigurationMealPlanTypeDtos;
        }

        private void ValidatePropertyMealPlanTypeRate(DateTime startDate, DateTime finalDate, Guid currencyId)
        {
            if (!_propertyMealPlanTypeRateReadRepository.ExistsForAllPeriod(currencyId, int.Parse(_applicationUser.PropertyId), GenerateRangeDate(startDate.Date, finalDate.Date)))
            {
                Notification.Raise(Notification.DefaultBuilder
                                         .WithMessage(AppConsts.LocalizationSourceName, PropertyMealPlanTypeRate.EntityError.NotExistsPropertyMealPlanTypeRateForAllPeriod)
                                         .WithDetailedMessage(AppConsts.LocalizationSourceName, PropertyMealPlanTypeRate.EntityError.NotExistsPropertyMealPlanTypeRateForAllPeriod)
                                         .Build());
            }
        }

        private void ValidatePremiseDto(PremiseConfigurationDto dto)
        {
            var systemDate = _propertyParameterReadRepository.GetSystemDate(int.Parse(_applicationUser.PropertyId));

            if (systemDate == null)
            {
                Notification.Raise(Notification.DefaultBuilder
                    .WithMessage(AppConsts.LocalizationSourceName, PropertyAuditProcess.EntityError.PropertyAuditProcessInvalidPropertySystemDate)
                    .WithDetailedMessage(AppConsts.LocalizationSourceName, PropertyAuditProcess.EntityError.PropertyAuditProcessInvalidPropertySystemDate)
                    .Build());
                return;
            }

            if (dto.Start.Date < systemDate.Value.Date || dto.End.Date < systemDate.Value.Date)
                NotifyParameterInvalid();

            if (dto.Start.Date > dto.End.Date)
                NotifyInvalidRangeDate();

            var totalDays = (dto.End.Date - dto.Start.Date).TotalDays;

            if ((dto.End.Date - dto.Start.Date).TotalDays > 1000)
            {
                object[] validationObj = { totalDays };
                RaiseNotification(PropertyBaseRateEnum.Error.PropertyBaseRateInvalidPeriod, PropertyBaseRateEnum.Error.PropertyBaseRateInvalidPeriod, validationObj);
            }

            if (dto.PremiseValue < 0)
            {
                Notification.Raise(Notification.DefaultBuilder
                   .WithMessage(AppConsts.LocalizationSourceName, PremiseConfigurationDto.EntityError.PremisseValueInvalid)
                   .WithDetailedMessage(AppConsts.LocalizationSourceName, PremiseConfigurationDto.EntityError.PremisseValueInvalid)
                   .Build());

            }
        }

        private async Task CreateBaseRateList(
                 BaseRateConfigurationDto dto,
                 IListDto<RoomTypeDto> roomTypeList,
                 BaseRateConfigurationMealPlanTypeDto baseRateConfigurationMealPlanTypeDto,
                 List<DateTime> rangeDateList,
                 List<int> roomTypeIdList,
                 CurrencyDto currency,
                 Guid headerHistoryId,
                 List<PropertyBaseRate> propertyBaseRateListToAdd,
                 List<PropertyBaseRate> propertyBaseRateListToUpdate,
                 List<PropertyMealPlanTypeRate> propertyMealPlanTypeRateList)
        {
            await ValidatePropertyBaseRate(dto, roomTypeList, baseRateConfigurationMealPlanTypeDto);

            if (Notification.HasNotification())
                return;

            var propertyBaseRate = await _propertyBaseRateReadRepository.GetAll(baseRateConfigurationMealPlanTypeDto.MealPlanTypeId, dto.CurrencyId, rangeDateList, roomTypeIdList, dto.RatePlanId);

            foreach (var date in rangeDateList)
            {
                if (dto.PropertyMealPlanTypeRate)
                    GetPropertyMealPlanTypeRate(dto, baseRateConfigurationMealPlanTypeDto, propertyMealPlanTypeRateList, date);
                
                foreach (var propertyBaseRateRoomType in dto.BaseRateConfigurationRoomTypeList)
                {
                    var propertyBaseRateToUpdate = propertyBaseRate.SingleOrDefault(e => e.RoomTypeId == propertyBaseRateRoomType.RoomTypeId && e.Date.Date == date);

                    if (propertyBaseRateToUpdate == null)
                        propertyBaseRateListToAdd.Add(PropertyBaseRateAdapter
                            .MapBaseRateConfigurationDto(propertyBaseRateRoomType, baseRateConfigurationMealPlanTypeDto, dto, date, currency, headerHistoryId)
                            .SetTenant(_applicationUser)
                            .Build());
                    else
                        propertyBaseRateListToUpdate.Add(PropertyBaseRateAdapter
                            .MapBaseRateConfigurationDto(propertyBaseRateToUpdate, baseRateConfigurationMealPlanTypeDto, propertyBaseRateRoomType, dto, date, currency, headerHistoryId)
                            .SetTenant(_applicationUser)
                            .Build());
                }
            }

        }

        private async Task<PropertyBaseRateToCreate> GeneratePropertyBaseRate(BaseRateConfigurationDto dto)
        {
            ValidateDto<BaseRateConfigurationDto>(dto, nameof(dto));

            var rangeDateList = CreateRangeOfDates(dto);
            if (rangeDateList == null || !rangeDateList.Any())
            {
                Notification.Raise(Notification
                .DefaultBuilder
                .WithMessage(AppConsts.LocalizationSourceName, PropertyBaseRate.EntityError.NotExistsWeekDaysForPropertyBaseRate)
                .Build());

                return null;
            }

            var propertyMealPlanTypeRateList = new List<PropertyMealPlanTypeRate>();
            if (dto.PropertyMealPlanTypeRate)
            {
                ValidatePropertyMealPlanTypeRate(dto.StartDate, dto.FinalDate, dto.CurrencyId);

                if (Notification.HasNotification()) return null;

                propertyMealPlanTypeRateList = _propertyMealPlanTypeRateReadRepository.GetAllEntity(new GetAllPropertyMealPlanTypeRateDto { CurrencyId = dto.CurrencyId, StartDate = rangeDateList.OrderBy(d => d).FirstOrDefault(), EndDate = rangeDateList.OrderBy(d => d).LastOrDefault() });
                InsertMealPlanAmountByMealPlanTypeRate(dto, propertyMealPlanTypeRateList);
            }

            // adiciona o tipo none e checa o tipo de meanPlanTypeId Default está preenchido, se não estiver preenche
            dto.BaseRateConfigurationMealPlanTypeList = BaseRateConfigurationMealPlanTypeDtoDefault(dto.BaseRateConfigurationMealPlanTypeList, dto.MealPlanTypeDefaultId);

            var roomTypeIdList = dto.BaseRateConfigurationRoomTypeList.Select(e => e.RoomTypeId).ToList();
            var headerHistory = _propertyBaseRateHeaderHistoryAppService.Create(dto).Build();

            if (Notification.HasNotification())
                return null;

            var currency = await _currencyAppService.Get(new DefaultGuidRequestDto(dto.CurrencyId, new RequestDto()));
            var roomTypeList = await _roomTypeReadRepository.GetAllRoomTypesByPropertyIdAsync(dto.PropertyId);

            List<Task> tasks = new List<Task>();

            var propertyBaseRateListToAdd = new List<PropertyBaseRate>();
            var propertyBaseRateListToUpdate = new List<PropertyBaseRate>();

            // Atualizando o mealplantypedefault de todos os propertybaserates neste periodo no banco
            var propertyBaseRateMealPlanListToUpdate = await _propertyBaseRateRepository.GetByFiltersAsync(dto.PropertyId, roomTypeIdList, dto.CurrencyId, dto.StartDate, dto.FinalDate, dto.MealPlanTypeDefaultId);

            foreach (var baseRateConfigurationMealPlanTypeDto in dto.BaseRateConfigurationMealPlanTypeList)
            {
                tasks.Add(CreateBaseRateList(
                 dto,
                 roomTypeList,
                 baseRateConfigurationMealPlanTypeDto,
                 rangeDateList,
                 roomTypeIdList,
                 currency,
                 headerHistory.Id,
                 propertyBaseRateListToAdd,
                 propertyBaseRateListToUpdate,
                 propertyMealPlanTypeRateList));
            }

            await Task.WhenAll(tasks);


            if (Notification.HasNotification())
                return null;

            return new PropertyBaseRateToCreate
            {
                PropertyBaseRateHeaderHistory = headerHistory,
                PropertyBaseRateListToAdd = propertyBaseRateListToAdd,
                PropertyBaseRateListToUpdate = propertyBaseRateListToUpdate,
                PropertyBaseRateMealPlanListToUpdate = propertyBaseRateMealPlanListToUpdate
            };
        }

        private async Task SaveAllChangesByLevelRate(ICollection<BaseRateConfigurationDto> baseRateConfigurationDtoList)
        {
            var baseRateHeaderHistoryList = new List<PropertyBaseRateHeaderHistory>();
            var propertyBaseRateToAdd = Enumerable.Empty<PropertyBaseRate>();
            var propertyBaseRateToUpdate = Enumerable.Empty<PropertyBaseRate>();
            var propertyBaseRateMealPlanListToUpdate = Enumerable.Empty<PropertyBaseRate>().AsQueryable();
            foreach (var baseRateConfigurationDto in baseRateConfigurationDtoList)
            {
                var result = await GeneratePropertyBaseRate(baseRateConfigurationDto);
                if (Notification.HasNotification()) return;

                baseRateHeaderHistoryList.Add(result.PropertyBaseRateHeaderHistory);
                propertyBaseRateToAdd = propertyBaseRateToAdd.Concat(result.PropertyBaseRateListToAdd);
                propertyBaseRateToUpdate = propertyBaseRateToUpdate.Concat(result.PropertyBaseRateListToUpdate);
                propertyBaseRateMealPlanListToUpdate = propertyBaseRateMealPlanListToUpdate.Concat(result.PropertyBaseRateMealPlanListToUpdate);
            }

            propertyBaseRateToAdd = propertyBaseRateToAdd.OrderBy(x => x.Date);

            using (var uow = _unitOfWorkManager.Begin())
            {
                _propertyBaseRateRepository.InsertHeaderHistoryRangeAndSaveChanges(baseRateHeaderHistoryList);

                _propertyBaseRateRepository.CreateAndUpdate(propertyBaseRateToAdd.ToList(), propertyBaseRateToUpdate.ToList());

                await uow.CompleteAsync();
            }
        }

        private void InsertMealPlanAmountByLevelRate(BaseRateConfigurationDto dto, LevelRateHeader levelRateHeader)
        {
            dto.BaseRateConfigurationMealPlanTypeList = levelRateHeader.LevelRateList.GroupBy(glrh => new
            {
                glrh.AdultMealPlanAmount,
                glrh.Child1MealPlanAmount,
                glrh.Child2MealPlanAmount,
                glrh.Child3MealPlanAmount,
                glrh.MealPlanTypeId
            })
          .Select(lr => new BaseRateConfigurationMealPlanTypeDto
          {
              AdultMealPlanAmount = lr.Key.AdultMealPlanAmount,
              Child1MealPlanAmount = lr.Key.Child1MealPlanAmount,
              Child2MealPlanAmount = lr.Key.Child2MealPlanAmount,
              Child3MealPlanAmount = lr.Key.Child3MealPlanAmount,
              MealPlanTypeId = lr.Key.MealPlanTypeId
          }).ToList();
        }

        private void InsertMealPlanAmountByMealPlanTypeRate(BaseRateConfigurationDto dto, List<PropertyMealPlanTypeRate> mealPlanTypeRateList)
        {
            dto.BaseRateConfigurationMealPlanTypeList = mealPlanTypeRateList.GroupBy(pmptr => new
            {
                pmptr.AdultMealPlanAmount,
                pmptr.Child1MealPlanAmount,
                pmptr.Child2MealPlanAmount,
                pmptr.Child3MealPlanAmount,
                pmptr.MealPlanTypeId,
                pmptr.MealPlanTypeDefault
            }).Select(pmptr => new BaseRateConfigurationMealPlanTypeDto
            {
                AdultMealPlanAmount = pmptr.Key.AdultMealPlanAmount,
                Child1MealPlanAmount = pmptr.Key.Child1MealPlanAmount,
                Child2MealPlanAmount = pmptr.Key.Child2MealPlanAmount,
                Child3MealPlanAmount = pmptr.Key.Child3MealPlanAmount,
                MealPlanTypeId = pmptr.Key.MealPlanTypeId
            }).ToList();

            var mealPlanTypeDefaultId = mealPlanTypeRateList.FirstOrDefault(pmptr => pmptr.MealPlanTypeDefault.HasValue && pmptr.MealPlanTypeDefault.Value)?.MealPlanTypeId;

            dto.MealPlanTypeDefaultId = mealPlanTypeDefaultId ?? default(int);
        }

        private void InsertRoomTypeAmountList(BaseRateConfigurationDto dto, LevelRateHeader levelRateHeader)
        {
            dto.BaseRateConfigurationRoomTypeList = levelRateHeader.LevelRateList.GroupBy(glrh => new
            {
                glrh.Child1Amount,
                glrh.Child2Amount,
                glrh.Child3Amount,
                glrh.Pax1Amount,
                glrh.Pax2Amount,
                glrh.Pax3Amount,
                glrh.Pax4Amount,
                glrh.Pax5Amount,
                glrh.RoomTypeId,
                glrh.PaxAdditionalAmount
            })
                   .Select(lr => new BaseRateConfigurationRoomTypeDto
                   {
                       Child1Amount = lr.Key.Child1Amount,
                       Child2Amount = lr.Key.Child2Amount,
                       Child3Amount = lr.Key.Child3Amount,
                       Pax1Amount = lr.Key.Pax1Amount,
                       Pax2Amount = lr.Key.Pax2Amount,
                       Pax3Amount = lr.Key.Pax3Amount,
                       Pax4Amount = lr.Key.Pax4Amount,
                       Pax5Amount = lr.Key.Pax5Amount,
                       RoomTypeId = lr.Key.RoomTypeId,
                       PaxAdditionalAmount = lr.Key.PaxAdditionalAmount
                   }).ToList();
        }

        private void GetPropertyMealPlanTypeRate(BaseRateConfigurationDto baseDto, BaseRateConfigurationMealPlanTypeDto dto, List<PropertyMealPlanTypeRate> mealPlanTypeRateList, DateTime date)
        {
            var mealPlanTypeRate = mealPlanTypeRateList.FirstOrDefault(mptr => mptr.MealPlanTypeId == dto.MealPlanTypeId && mptr.Date.Date == date.Date);
            if (mealPlanTypeRate == null)
                return;

            var mealPlanTypeDefaultId = mealPlanTypeRateList.FirstOrDefault(mptr => mptr.Date.Date == date.Date && mptr.MealPlanTypeDefault.HasValue && mptr.MealPlanTypeDefault.Value)?.MealPlanTypeId;

            baseDto.MealPlanTypeDefaultId = mealPlanTypeDefaultId ?? default(int);
            dto.MealPlanTypeId = mealPlanTypeRate.MealPlanTypeId;
            dto.AdultMealPlanAmount = mealPlanTypeRate.AdultMealPlanAmount;
            dto.Child1MealPlanAmount = mealPlanTypeRate.Child1MealPlanAmount;
            dto.Child2MealPlanAmount = mealPlanTypeRate.Child2MealPlanAmount;
            dto.Child3MealPlanAmount = mealPlanTypeRate.Child3MealPlanAmount;
        }
    }
}
