﻿using System;
using System.Collections.Generic;
using System.Linq;
using Thex.Application.Interfaces;
using Thex.Domain.Entities;
using Thex.Domain.Interfaces.Repositories;
using Thex.Dto;
using Thex.Infra.ReadInterfaces;
using Thex.Kernel;
using Tnf.Notifications;

namespace Thex.Application.Services
{
    public class ReservationItemLaunchAppService : ApplicationServiceBase, IReservationItemLaunchAppService
    {
        private readonly IApplicationUser _applicationUser;
        private readonly IReservationItemLaunchReadRepository _reservationItemLaunchReadRepository;
        private readonly IReservationItemLaunchRepository _reservationItemLaunchRepository;

        public ReservationItemLaunchAppService(
            IApplicationUser applicationUser,
            INotificationHandler notificationHandler,
            IReservationItemLaunchReadRepository reservationItemLaunchReadRepositor,
            IReservationItemLaunchRepository reservationItemLaunchRepository
        )
            : base(notificationHandler)
        {
            _applicationUser = applicationUser;
            _reservationItemLaunchReadRepository = reservationItemLaunchReadRepositor;
            _reservationItemLaunchRepository = reservationItemLaunchRepository;
        }

        public void CreateToTourismTax(List<BillingAccountGuestReservationItemDto> billingAccountGuestReservationItemDtoList, TourismTaxDto tourismTaxDto)
        {
            if (billingAccountGuestReservationItemDtoList == null || (billingAccountGuestReservationItemDtoList != null && !billingAccountGuestReservationItemDtoList.Any()))
                return;

            var existingReservationItemLaunchList = _reservationItemLaunchReadRepository
                   .GetAllByGuestReservationItemIdList(billingAccountGuestReservationItemDtoList.Select(g => (long?)g.GuestReservationItemId).ToList());

            var guestReservationItemWithoutLaunchList = billingAccountGuestReservationItemDtoList.Where(b
                => !(existingReservationItemLaunchList.Select(r => r.GuestReservationItemId).Contains(b.GuestReservationItemId)));

            var newReservationItemLaunchList = new List<ReservationItemLaunchDto>();

            foreach (var guestReservationItemWithoutLaunch in guestReservationItemWithoutLaunchList)
            {
                var daysOfReservationItem = GetDaysOfReservationItem(guestReservationItemWithoutLaunch, tourismTaxDto);

                var reservationItemLaunchDto = new ReservationItemLaunchDto
                {
                    ReservationItemId = guestReservationItemWithoutLaunch.ReservationItemId,
                    GuestReservationItemId = guestReservationItemWithoutLaunch.GuestReservationItemId,
                    QuantityOfTourismTaxLaunched = daysOfReservationItem
                };

                newReservationItemLaunchList.Add(reservationItemLaunchDto);
            }

            var entityList = MapToEntityList(newReservationItemLaunchList);

            _reservationItemLaunchRepository.InsertList(entityList);
        }

        public void UpdateToTourismTax(List<BillingAccountGuestReservationItemDto> billingAccountGuestReservationItemDtoList, TourismTaxDto tourismTaxDto)
        {
            if (billingAccountGuestReservationItemDtoList == null || (billingAccountGuestReservationItemDtoList != null && !billingAccountGuestReservationItemDtoList.Any()))
                return;

            var existingReservationItemLaunchList = _reservationItemLaunchReadRepository
                      .GetAllByGuestReservationItemIdList(billingAccountGuestReservationItemDtoList.Select(g => (long?)g.GuestReservationItemId).ToList());

            existingReservationItemLaunchList.ForEach(r => r.IncrementQuantityOfTourismTaxLaunched(tourismTaxDto.NumberOfNights, tourismTaxDto.IsTotalOfNights));

            var guestReservationItemWithoutLaunchList = billingAccountGuestReservationItemDtoList.Where(b
                => !(existingReservationItemLaunchList.Select(r => r.GuestReservationItemId).Contains(b.GuestReservationItemId)));

            var newReservationItemLaunchList = new List<ReservationItemLaunchDto>();
            const int initialQuantityOfTourismTaxLaunched = 1;

            foreach (var guestReservationItemWithoutLaunch in guestReservationItemWithoutLaunchList)
            {
                var reservationItemLaunchDto = new ReservationItemLaunchDto
                {
                    ReservationItemId = guestReservationItemWithoutLaunch.ReservationItemId,
                    GuestReservationItemId = guestReservationItemWithoutLaunch.GuestReservationItemId,
                    QuantityOfTourismTaxLaunched = initialQuantityOfTourismTaxLaunched
                };

                newReservationItemLaunchList.Add(reservationItemLaunchDto);
            }

            _reservationItemLaunchRepository.UpdateList(existingReservationItemLaunchList);
            _reservationItemLaunchRepository.InsertList(MapToEntityList(newReservationItemLaunchList));
        }

        private List<ReservationItemLaunch> MapToEntityList(List<ReservationItemLaunchDto> dtoList)
        {
            List<ReservationItemLaunch> entityList = new List<ReservationItemLaunch>();

            foreach (var dto in dtoList)
            {
                var entity = new ReservationItemLaunch.Builder(Notification)
                .WithId(Guid.NewGuid())
                .WithReservationItemId(dto.ReservationItemId)
                .WithGuestReservationItemId(dto.GuestReservationItemId)
                .WithQuantityOfTourismTaxLaunched(dto.QuantityOfTourismTaxLaunched)
                .SetTenant(_applicationUser)
                .Build();

                entityList.Add(entity);
            }

            return entityList;
        }

        private int GetDaysOfReservationItem(BillingAccountGuestReservationItemDto item, TourismTaxDto tourismTaxDto)
        {
            var period = item.EstimatedDepartureDate.Date - item.CheckinDate.Date;
            var days = period.Days <= 0 ? 1 : period.Days;
            return !tourismTaxDto.IsTotalOfNights && days > tourismTaxDto.NumberOfNights ? tourismTaxDto.NumberOfNights : period.Days;
        }
    }
}
