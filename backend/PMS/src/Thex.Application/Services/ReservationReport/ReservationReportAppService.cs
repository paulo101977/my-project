﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Thex.Application.Interfaces.ReservationReport;
using Thex.Common;
using Thex.Common.Enumerations;
using Thex.Kernel;
using Thex.Domain.Entities;
using Thex.Domain.Interfaces.Repositories;
using Thex.Dto.Report;
using Thex.Dto.Reservation;
using Thex.Dto.ReservationReport;
using Thex.Dto.Room;
using Thex.Dto.SearchReservationReportDto;
using Thex.Infra.ReadInterfaces;
using Thex.Infra.Repositories.Read;
using Tnf.Dto;
using Tnf.Notifications;
using Thex.Dto;
using Tnf.Localization;

namespace Thex.Application.Services.ReservationReport
{
    public class ReservationReportAppService : ApplicationServiceBase, IReservationReportAppService
    {
        private readonly IReservationReportReadRepository _reservationReportReadRepository;
        private readonly IReservationReportRepository _reservationReportRepository;
        private readonly IApplicationUser _applicationUser;
        private readonly PropertyParameterReadRepository _propertyParameterReadRepository;
        private readonly IGuestReservationItemReadRepository _guestReservationItemReadRepository;
        private readonly ILocalizationManager _localizationManager;
        private readonly IReservationItemReadRepository _reservationItemReadRepository;
        private readonly IPropertyReadRepository _propertyReadRepository;

        public ReservationReportAppService(
           IReservationReportReadRepository reservationReportReadRepository,
           IGuestReservationItemReadRepository guestReservationItemReadRepository,
           IReservationReportRepository reservationReportRepository,
           IReservationItemReadRepository reservationItemReadRepository,
           IPropertyReadRepository propertyReadRepository,
           IApplicationUser applicationUser,
           INotificationHandler notificationHandler,
           ILocalizationManager localizationManager,
           PropertyParameterReadRepository propertyParameterReadRepository)
           : base(notificationHandler)
        {
            _reservationReportReadRepository = reservationReportReadRepository;
            _reservationReportRepository = reservationReportRepository;
            _applicationUser = applicationUser;
            _propertyParameterReadRepository = propertyParameterReadRepository;
            _guestReservationItemReadRepository = guestReservationItemReadRepository;
            _localizationManager = localizationManager;
            _reservationItemReadRepository = reservationItemReadRepository;
            _propertyReadRepository = propertyReadRepository;
        }


        public ListDto<SearchReservationReportResultReturnDto> GetAllByFilters(SearchReservationReportDto request, int propertyId)
        {
            bool vFlag = false;
            var nullResponseInstance = new ListDto<SearchReservationReportResultReturnDto>();

            foreach (var propertyInfo in request.GetType()
                                .GetProperties(
                                        BindingFlags.Public
                                        | BindingFlags.Instance))
            {
                if (propertyInfo.GetValue(request, null) != null)
                {
                    if (propertyInfo.Name != "Page" &&
                       propertyInfo.Name != "PageSize")
                    {
                        vFlag = true;
                    }
                }
            }

            if (!vFlag)
            {
                NotifyEmptyFilter();
                return nullResponseInstance;
            }

            if (propertyId <= 0)
            {
                NotifyIdIsMissing();
                return nullResponseInstance;
            }

            if (!request.HasValidOrderBy())
            {
                this.NotifyInvalidOrderBy();
                return nullResponseInstance;
            }

            var status = new List<ReservationStatus>
            {
                ReservationStatus.Checkin
            };

            DateTime? systemDate = _propertyParameterReadRepository.GetSystemDateAsync(propertyId).Result;

            if (systemDate == null)
            {
                Notification.Raise(Notification.DefaultBuilder
                .WithMessage(AppConsts.LocalizationSourceName, PropertyAuditProcess.EntityError.PropertyAuditProcessInvalidPropertySystemDate)
                .WithDetailedMessage(AppConsts.LocalizationSourceName, PropertyAuditProcess.EntityError.PropertyAuditProcessInvalidPropertySystemDate)
                .Build());

                return null;
            }

            var searchReservationResultDto = _reservationReportReadRepository.GetAllByFilters(request, propertyId, status, systemDate.Value);

            var propertyParameter = _propertyParameterReadRepository.GetPropertyParameterByApplicationParameterId((int)ApplicationParameterEnum.ParameterCheckInTime);

            DateTime.TryParse(propertyParameter.PropertyParameterValue, out var checkInTime);

            var searchReservationResultReturnDto = new ListDto<SearchReservationReportResultReturnDto>();

            var searchReservationResultReturnList = new List<SearchReservationReportResultReturnDto>();

            foreach (var searchReservationResult in searchReservationResultDto.Items)
            {
                var searchReservationResultReturn = new SearchReservationReportResultReturnDto()
                {
                    Id = searchReservationResult.Id,
                    GuestName = searchReservationResult.GuestName,
                    GuestTypeId = searchReservationResult.GuestTypeId,
                    GuestTypeName = searchReservationResult.GuestTypeName,
                    ReservationCode = searchReservationResult.ReservationCode,
                    UH = searchReservationResult.RoomNumber.ToString(),
                    TypeUH = searchReservationResult.ReceivedRoomTypeAbbreviation,
                    AdultsAndChildren = searchReservationResult.AdultsAndChildren,
                    Adult = searchReservationResult.Adult,
                    Child = searchReservationResult.Child,
                    Client = searchReservationResult.Client,
                    MealPlanTypeAmount = searchReservationResult.MealPlanTypeAmount,
                    MealPlanType = searchReservationResult.IsBudget ?
                            DateTime.UtcNow.ToZonedDateTimeLoggedUser().Hour < checkInTime.Hour ?
                            searchReservationResult.IsBudgetLastDay ? searchReservationResult.MealPlanLastDay :
                            _localizationManager.GetString(AppConsts.LocalizationSourceName,
                            (MealPlanTypeEnum.None).ToString()) : 
                            searchReservationResult.MealPlan :
                            searchReservationResult.MealPlanLastDay,
                    DailyAmount = searchReservationResult.DailyValue,
                    ArrivalDate = searchReservationResult.ArrivalDate,
                    DepartureDate = searchReservationResult.DepartureDate,
                    IsInternalUsage = searchReservationResult.IsInternalUsage,
                    IsCourtesy = searchReservationResult.IsCourtesy,
                    MealPlanTypeId = searchReservationResult.MealPlanTypeId,
                    CurrencySymbol = searchReservationResult.CurrencySymbol
                };

                searchReservationResultReturnList.Add(searchReservationResultReturn);
            }

            searchReservationResultReturnDto.Items = searchReservationResultReturnList;

            return searchReservationResultReturnDto;
        }

        public SearchReservationCheckReportResultList GetAllCheckByFilters(SearchReservationReportDto request, int propertyId)
        {
            ValidateDate(request.DateInitial, request.DateFinal);

            if (Notification.HasNotification()) return null;

            bool vFlag = false;
            var nullResponseInstance = new SearchReservationCheckReportResultList();

            foreach (var propertyInfo in request.GetType()
                                .GetProperties(
                                        BindingFlags.Public
                                        | BindingFlags.Instance))
            {
                if (propertyInfo.GetValue(request, null) != null)
                {
                    if (propertyInfo.Name != "Page" &&
                       propertyInfo.Name != "PageSize")
                    {
                        vFlag = true;
                    }
                }
            }

            if (propertyId <= 0)
            {
                NotifyIdIsMissing();
                return nullResponseInstance;
            }

            if (!request.HasValidOrderBy())
            {
                this.NotifyInvalidOrderBy();
                return nullResponseInstance;
            }

            var listStatus = new List<ReservationStatus>();
            var listAllStatus = new List<ReservationStatus>();
            var listEstimateStatus = new List<ReservationStatus>();
            var listEffectiveStatus = new List<ReservationStatus>();          

            if (!request.IsCheckIn)
            {
                listAllStatus = new List<ReservationStatus>()
                {
                    ReservationStatus.ToConfirm,
                    ReservationStatus.Confirmed,
                    ReservationStatus.Checkin,
                    ReservationStatus.Checkout,
                    ReservationStatus.Pending
                };

                listEstimateStatus = new List<ReservationStatus>()
                {
                    ReservationStatus.ToConfirm,
                    ReservationStatus.Confirmed,
                    ReservationStatus.Checkin
                };

                listEffectiveStatus = new List<ReservationStatus>()
                {
                    ReservationStatus.Checkout,
                    ReservationStatus.Pending
                };
            }
            else
            {
                listAllStatus = new List<ReservationStatus>()
                {
                     ReservationStatus.ToConfirm,
                     ReservationStatus.Confirmed,
                     ReservationStatus.Checkin
                };

                listEstimateStatus = new List<ReservationStatus>()
                {
                    ReservationStatus.ToConfirm,
                    ReservationStatus.Confirmed
                };

                listEffectiveStatus = new List<ReservationStatus>()
                {
                     ReservationStatus.Checkin
                };
            }

            DateTime? systemDate = _propertyParameterReadRepository.GetSystemDateAsync(propertyId).Result;

            if (systemDate == null)
            {
                Notification.Raise(Notification.DefaultBuilder
                .WithMessage(AppConsts.LocalizationSourceName, PropertyAuditProcess.EntityError.PropertyAuditProcessInvalidPropertySystemDate)
                .WithDetailedMessage(AppConsts.LocalizationSourceName, PropertyAuditProcess.EntityError.PropertyAuditProcessInvalidPropertySystemDate)
                .Build());

                return null;
            }

            var searchReservationResultDto = (ListReservationDto<SearchReservationReportResultDto>)_reservationReportReadRepository.GetAllCheckByFilters(request, propertyId, listAllStatus, systemDate.Value);
                       
            switch (request.CheckReport)
            {
                case CheckReportEnum.All:
                    listStatus = listAllStatus;
                    break;
                case CheckReportEnum.Estimates:
                    listStatus = listEstimateStatus;
                    break;
                case CheckReportEnum.Effectives:
                    listStatus = listEffectiveStatus;
                    break;
                default:
                    break;
            }            

            var totalReservationCheckReport = new TotalReservationCheckReport()
            {
                TotalChecksInPeriod = searchReservationResultDto.Items.Count(x => x.IsMain),
                TotalGuests = searchReservationResultDto.Items.Count(),
                TotalEstimatedReservations = searchReservationResultDto.Items.Count(x => listEstimateStatus.Contains(
                    (ReservationStatus)Enum.Parse(typeof(ReservationStatus), x.ReservationItemStatusId.ToString())) && x.IsMain),
                TotalEstimatedGuests = searchReservationResultDto.Items.Count(x => listEstimateStatus.Contains(
                    (ReservationStatus)Enum.Parse(typeof(ReservationStatus), x.ReservationItemStatusId.ToString()))),
                TotalReservationsMade = searchReservationResultDto.Items.Count(x => listEffectiveStatus.Contains(
                   (ReservationStatus)Enum.Parse(typeof(ReservationStatus), x.ReservationItemStatusId.ToString())) && x.IsMain),
                TotalGuestsMade = searchReservationResultDto.Items.Count(x => listEffectiveStatus.Contains(
                    (ReservationStatus)Enum.Parse(typeof(ReservationStatus), x.ReservationItemStatusId.ToString())))
            };            

            searchReservationResultDto.Items = searchReservationResultDto.Items.Where(x => listStatus.Contains(
                (ReservationStatus)Enum.Parse(typeof(ReservationStatus), x.ReservationItemStatusId.ToString()))).ToList();

            var searchReservationResultReturnList = new List<SearchReservationCheckReportResultReturnDto>();
            var searchReservationResultReturnDto = new ListReservationDto<SearchReservationCheckReportResultReturnDto>()
            {
                TotalReservations = searchReservationResultDto.Items.Count(x => x.IsMain),
                TotalGuests = searchReservationResultDto.Items.Count()
            };

            foreach (var searchReservationResult in searchReservationResultDto.Items)
            {
                var searchReservationResultReturn = new SearchReservationCheckReportResultReturnDto()
                {
                    Id = searchReservationResult.Id,
                    GuestName = searchReservationResult.GuestName,
                    GuestTypeName = searchReservationResult.GuestTypeName,
                    ReservationCode = searchReservationResult.ReservationCode,
                    UH = searchReservationResult?.RoomNumber,
                    TypeUH = searchReservationResult.ReceivedRoomTypeAbbreviation,
                    AdultsAndChildren = searchReservationResult.AdultsAndChildren,
                    Client = searchReservationResult.Client,
                    MealPlan = searchReservationResult.MealPlan,
                    MealPlanType = searchReservationResult.MealPlanType,
                    ArrivalDate = searchReservationResult.ArrivalDate,
                    DepartureDate = searchReservationResult.DepartureDate,
                    Status = searchReservationResult.ReservationItemStatusName,
                    EnsuresNoShow = searchReservationResult.EnsuresNoShow,
                    UserName = searchReservationResult.UserName,
                    AgreementTypeId = searchReservationResult.AgreementTypeId,
                    AgreementName = searchReservationResult.AgreementName,
                    Deposit = searchReservationResult.Deposit,
                    CurrencySymbol = searchReservationResult.CurrencySymbol,
                    InternalComments = searchReservationResult.InternalComments,
                    ExternalComments = searchReservationResult.ExternalComments,
                    PartnerComments = searchReservationResult.PartnerComments
                };

                searchReservationResultReturnList.AddIfNotContains(searchReservationResultReturn);
            }

            searchReservationResultReturnDto.Items = searchReservationResultReturnList;

            var searchReservationCheckReportResultList = new SearchReservationCheckReportResultList
            {
                ListReservationCheckReportDto = searchReservationResultReturnDto,
                TotalReservationCheckReport = totalReservationCheckReport
            };            

            return searchReservationCheckReportResultList;
        }

        private void ValidateDate(DateTime? dateInitial, DateTime? dateFinal)
        {
            if (dateInitial == null || dateInitial == DateTime.MinValue || dateFinal == null || dateFinal == DateTime.MinValue)
            {
                Notification.Raise(Notification.DefaultBuilder
             .WithMessage(AppConsts.LocalizationSourceName, Reservation.EntityError.ReservationReportsMustHaveDateInitialAndDateFinal)
             .WithDetailedMessage(AppConsts.LocalizationSourceName, Reservation.EntityError.ReservationReportsMustHaveDateInitialAndDateFinal)
             .Build());
            }
        }

        public ListDto<SearchGuestSaleReportResultDto> GetAllGuestSaleByFilters(int propertyId, bool guestSale,
            bool sparseSale, bool masterGroupSale, bool companySale, bool eventSale, bool pendingSale, bool positiveSale, bool canceledSale,
            bool noShowSale, double? sale, bool aheadSale)
        {
            return _reservationReportReadRepository.GetAllGuestSaleByFilters(propertyId, guestSale, sparseSale, masterGroupSale,
                companySale, eventSale, pendingSale, positiveSale, canceledSale, noShowSale, sale, aheadSale);
        }

        public ListDto<SearchMeanPlanControlReportResultDto> GetAllMeanPlanByFilters(SearchReservationReportDto request, int propertyId)
        {
            DateTime? systemDate = _propertyParameterReadRepository.GetSystemDateAsync(propertyId).Result;

            if (systemDate == null)
            {
                Notification.Raise(Notification.DefaultBuilder
                .WithMessage(AppConsts.LocalizationSourceName, PropertyAuditProcess.EntityError.PropertyAuditProcessInvalidPropertySystemDate)
                .WithDetailedMessage(AppConsts.LocalizationSourceName, PropertyAuditProcess.EntityError.PropertyAuditProcessInvalidPropertySystemDate)
                .Build());

                return null;
            }

            var propertyParameter = _propertyParameterReadRepository.GetPropertyParameterByApplicationParameterId((int)ApplicationParameterEnum.ParameterCheckInTime);

            DateTime.TryParse(propertyParameter.PropertyParameterValue, out var checkInTime);

            return _reservationReportReadRepository.GetAllMeanPlanByFilters(request, propertyId, systemDate.Value, checkInTime.Hour);
        }

        public PostMeanPlanControlReportDto PostMeanPlan(PostMeanPlanControlReportDto request)
        {
            var propertyId = int.Parse(_applicationUser.PropertyId);

            DateTime? systemDate = _propertyParameterReadRepository.GetSystemDateAsync(propertyId).Result;

            if (systemDate == null)
            {
                Notification.Raise(Notification.DefaultBuilder
                .WithMessage(AppConsts.LocalizationSourceName, PropertyAuditProcess.EntityError.PropertyAuditProcessInvalidPropertySystemDate)
                .WithDetailedMessage(AppConsts.LocalizationSourceName, PropertyAuditProcess.EntityError.PropertyAuditProcessInvalidPropertySystemDate)
                .Build());

                return null;
            }
            return _reservationReportRepository.PostMeanPlan(request, _applicationUser.TenantId, systemDate);
        }

        public ListSearchIssueNotesReportResultDto GetAllIssuedNotes(int propertyId, SearchIssuedNotesReportDto requestDto)
        {
            var issueNotes = _reservationReportReadRepository.GetAllIssuedNotes(propertyId, requestDto);
            var listIssuedNotesPerSeries = new ListDto<ListSearchIssuedNotesReportResultDtoPerSeries>();
            var listIssuedNotesProduct = new ListDto<ListSearchIssuedNotesReportResultDtoProduct>();
            var listIssuedNotesService = new ListDto<ListSearchIssuedNotesReportResultDtoService>();

            var productTotals = issueNotes.Items.Where(n => n.BillingInvoiceTypeId == (int)BillingInvoiceTypeEnum.NFCE).ToList();
            var productStatus = productTotals.Select(x => x.StatusId).Distinct().ToList();
            var serviceTotals = issueNotes.Items.Where(n => n.BillingInvoiceTypeId == (int)BillingInvoiceTypeEnum.NFSE).ToList();
            var serviceStatus = serviceTotals.Select(x => x.StatusId).Distinct().ToList();
            var netStatus = new List<int> { (int)BillingInvoiceStatusEnum.Canceled, (int)BillingInvoiceStatusEnum.Error };

            AddProductIssuedNotes(listIssuedNotesProduct, productTotals, productStatus, new ListDto<ListSearchIssuedNotesReportResultDtoPerStatus>(), netStatus);
            AddServiceIssuedNotes(listIssuedNotesService, serviceTotals, serviceStatus, new ListDto<ListSearchIssuedNotesReportResultDtoPerStatus>(), netStatus);

            listIssuedNotesPerSeries.Items.Add(new ListSearchIssuedNotesReportResultDtoPerSeries()
            {
                ListIssuedNotesProduct = listIssuedNotesProduct,
                ListIssuedNotesService = listIssuedNotesService
            });

            return new ListSearchIssueNotesReportResultDto
            {
                ListIssuedNotes = issueNotes,
                ListIssuedNotesPerSeries = listIssuedNotesPerSeries
            };
        }       

        private void AddProductIssuedNotes(ListDto<ListSearchIssuedNotesReportResultDtoProduct> listIssuedNotesProduct, List<SearchIssuedNotesReportResultDto> productTotals, List<int> productStatus, ListDto<ListSearchIssuedNotesReportResultDtoPerStatus> listIssuedNotesPerStatus, List<int> netStatus)
        {
            productStatus.ForEach(status =>
            {
                listIssuedNotesPerStatus.Items.Add(new ListSearchIssuedNotesReportResultDtoPerStatus()
                {
                    StatusId = status,
                    StatusName = string.IsNullOrEmpty(_localizationManager.GetString(AppConsts.LocalizationSourceName,
                                                     ((BillingInvoiceStatusEnum)status).ToString())) ? "" :
                                                     _localizationManager.GetString(AppConsts.LocalizationSourceName,
                                                     ((BillingInvoiceStatusEnum)status).ToString()),
                    Quantity = productTotals.Count(x => x.StatusId == status),
                    Service = productTotals.Where(x => x.StatusId == status).Sum(x => x.Service),
                    Rate = productTotals.Where(x => x.StatusId == status).Sum(x => x.Rate),
                    Iss = productTotals.Where(x => x.StatusId == status).Sum(x => x.Iss),
                    TypeOfPayment = productTotals.Where(x => x.StatusId == status).Sum(x => x.TypeOfPayment),
                    PointOfSale = productTotals.Where(x => x.StatusId == status).Sum(x => x.PointOfSale),
                    Total = productTotals.Where(x => x.StatusId == status).Sum(x => x.Total)
                });
            });

            listIssuedNotesProduct.Items.Add(new ListSearchIssuedNotesReportResultDtoProduct()
            {
                ListIssuedNotesPerStatus = listIssuedNotesPerStatus,
                GrossIssuedNotes = new GrossIssuedNotes()
                {
                    Quantity = productTotals.Count(),
                    Service = productTotals.Sum(x => x.Service),
                    Rate = productTotals.Sum(x => x.Rate),
                    Iss = productTotals.Sum(x => x.Iss),
                    TypeOfPayment = productTotals.Sum(x => x.TypeOfPayment),
                    PointOfSale = productTotals.Sum(x => x.PointOfSale),
                    Total = productTotals.Sum(x => x.Total)
                },
                NetIssuedNotes = new NetIssuedNotes()
                {
                    Quantity = productTotals.Count() - productTotals.Count(x => netStatus.Contains(x.StatusId)),
                    Service = productTotals.Sum(x => x.Service) - productTotals.Where(x => netStatus.Contains(x.StatusId)).Sum(x => x.Service),
                    Rate = productTotals.Sum(x => x.Rate) - productTotals.Where(x => netStatus.Contains(x.StatusId)).Sum(x => x.Rate),
                    Iss = productTotals.Sum(x => x.Iss) - productTotals.Where(x => netStatus.Contains(x.StatusId)).Sum(x => x.Iss),
                    TypeOfPayment = productTotals.Sum(x => x.TypeOfPayment) - productTotals.Where(x => netStatus.Contains(x.StatusId)).Sum(x => x.TypeOfPayment),
                    PointOfSale = productTotals.Sum(x => x.PointOfSale) - productTotals.Where(x => netStatus.Contains(x.StatusId)).Sum(x => x.PointOfSale),
                    Total = productTotals.Sum(x => x.Total) - productTotals.Where(x => netStatus.Contains(x.StatusId)).Sum(x => x.Total)
                }
            });
        }

        private void AddServiceIssuedNotes(ListDto<ListSearchIssuedNotesReportResultDtoService> listIssuedNotesService, List<SearchIssuedNotesReportResultDto> serviceTotals, List<int> serviceStatus, ListDto<ListSearchIssuedNotesReportResultDtoPerStatus> listIssuedNotesPerStatus, List<int> netStatus)
        {
            serviceStatus.ForEach(status =>
            {
                listIssuedNotesPerStatus.Items.Add(new ListSearchIssuedNotesReportResultDtoPerStatus()
                {
                    StatusId = status,
                    StatusName = string.IsNullOrEmpty(_localizationManager.GetString(AppConsts.LocalizationSourceName,
                                                     ((BillingInvoiceStatusEnum)status).ToString())) ? "" :
                                                     _localizationManager.GetString(AppConsts.LocalizationSourceName,
                                                     ((BillingInvoiceStatusEnum)status).ToString()),
                    Quantity = serviceTotals.Count(x => x.StatusId == status),
                    Service = serviceTotals.Where(x => x.StatusId == status).Sum(x => x.Service),
                    Rate = serviceTotals.Where(x => x.StatusId == status).Sum(x => x.Rate),
                    Iss = serviceTotals.Where(x => x.StatusId == status).Sum(x => x.Iss),
                    TypeOfPayment = serviceTotals.Where(x => x.StatusId == status).Sum(x => x.TypeOfPayment),
                    PointOfSale = serviceTotals.Where(x => x.StatusId == status).Sum(x => x.PointOfSale),
                    Total = serviceTotals.Where(x => x.StatusId == status).Sum(x => x.Total)
                });
            });

            listIssuedNotesService.Items.Add(new ListSearchIssuedNotesReportResultDtoService()
            {
                ListIssuedNotesPerStatus = listIssuedNotesPerStatus,
                GrossIssuedNotes = new GrossIssuedNotes()
                {
                    Quantity = serviceTotals.Count(),
                    Service = serviceTotals.Sum(x => x.Service),
                    Rate = serviceTotals.Sum(x => x.Rate),
                    Iss = serviceTotals.Sum(x => x.Iss),
                    TypeOfPayment = serviceTotals.Sum(x => x.TypeOfPayment),
                    PointOfSale = serviceTotals.Sum(x => x.PointOfSale),
                    Total = serviceTotals.Sum(x => x.Total)
                },
                NetIssuedNotes = new NetIssuedNotes()
                {
                    Quantity = serviceTotals.Count() - serviceTotals.Count(x => netStatus.Contains(x.StatusId)),
                    Service = serviceTotals.Sum(x => x.Service) - serviceTotals.Where(x => netStatus.Contains(x.StatusId)).Sum(x => x.Service),
                    Rate = serviceTotals.Sum(x => x.Rate) - serviceTotals.Where(x => netStatus.Contains(x.StatusId)).Sum(x => x.Rate),
                    Iss = serviceTotals.Sum(x => x.Iss) - serviceTotals.Where(x => netStatus.Contains(x.StatusId)).Sum(x => x.Iss),
                    TypeOfPayment = serviceTotals.Sum(x => x.TypeOfPayment) - serviceTotals.Where(x => netStatus.Contains(x.StatusId)).Sum(x => x.TypeOfPayment),
                    PointOfSale = serviceTotals.Sum(x => x.PointOfSale) - serviceTotals.Where(x => netStatus.Contains(x.StatusId)).Sum(x => x.PointOfSale),
                    Total = serviceTotals.Sum(x => x.Total) - serviceTotals.Where(x => netStatus.Contains(x.StatusId)).Sum(x => x.Total)
                }
            });
        }

        public ListDto<SearchReservationUhReportResultReturnDto> GetAllUhSituationByFilters(GetAllSituationRoomsDto request, int propertyId)
        {
            return _reservationReportReadRepository.GetRoomsByPropertyId(request, propertyId);
        }

        private async Task<ReportUhSituationReturnDto> GetTotalUhSituation(int propertyId)
        {
            return await _reservationReportReadRepository.GetUhSituation(propertyId);

        }

        private async Task<ReportHousekeepingStatusReturnDto> GetTotalHousekeepingStatus(int propertyId)
        {
            return await _reservationReportReadRepository.GetHousekeepingStatus(propertyId);
        }

        private async Task<ReportReservationStatusReturnDto> GetTotalReservationStatus(int propertyId, DateTime? systemDate)
        {
            if (systemDate == null)
            {
                Notification.Raise(Notification.DefaultBuilder
                .WithMessage(AppConsts.LocalizationSourceName, PropertyAuditProcess.EntityError.PropertyAuditProcessInvalidPropertySystemDate)
                .WithDetailedMessage(AppConsts.LocalizationSourceName, PropertyAuditProcess.EntityError.PropertyAuditProcessInvalidPropertySystemDate)
                .Build());

                return null;
            }
            else
                return await _reservationReportReadRepository.GetReservationStatus(propertyId, systemDate);
        }

        private async Task<ReportCheckStatusReturnDto> GetTotalCheckStatus(int propertyId, DateTime? systemDate)
        {
            if (systemDate == null)
            {
                Notification.Raise(Notification.DefaultBuilder
                .WithMessage(AppConsts.LocalizationSourceName, PropertyAuditProcess.EntityError.PropertyAuditProcessInvalidPropertySystemDate)
                .WithDetailedMessage(AppConsts.LocalizationSourceName, PropertyAuditProcess.EntityError.PropertyAuditProcessInvalidPropertySystemDate)
                .Build());

                return null;
            }
            else
                return await _reservationReportReadRepository.GetCheckStatus(propertyId, systemDate);
        }

        public ListSearchBordereauReportResultDto GetBordereau(SearchBordereauReportDto requestDto, int propertyId)
        {            
            if (requestDto.Synthesise)
            {
                return new ListSearchBordereauReportResultDto()
                {
                    ListBordereau = _reservationReportReadRepository.GetBordereau(requestDto, propertyId)
                };
            }
            else
            {
                var bordereauList = _reservationReportReadRepository.GetBordereau(requestDto, propertyId);
                var ListDtoPerGroup = new ListDto<ListSearchBordereauReportResultDtoPerGroup>();
                foreach (var billingItem in bordereauList.Items.Select(x => x.BillingItemName).Distinct())
                {
                    var bordereauListPerBillingItem = bordereauList.Items.Where(b => b.BillingItemName == billingItem);
                    ListDtoPerGroup.Items.Add(new ListSearchBordereauReportResultDtoPerGroup()
                    {
                        BordereauPerGroup = bordereauListPerBillingItem.ToListDto(false),
                        BillingItemName = billingItem,
                        CurrencySymbol = bordereauList.Items.FirstOrDefault().CurrencySymbol,
                        GrossAmount = bordereauListPerBillingItem.Where(b => b.BillingAccountItemTypeId != 3).Sum(b => b.Amount),
                        ReversedAmount = bordereauListPerBillingItem.Where(b => b.BillingAccountItemTypeId == 3).Sum(b => b.Amount),
                        NetAmount = bordereauListPerBillingItem.Sum(b => b.Amount)
                    });
                }               

                return new ListSearchBordereauReportResultDto()
                {
                    ListBordereau = bordereauList,
                    ListBordereauPerGroup = ListDtoPerGroup
                };
            }
        }

        public IListDto<ReportHousekeepingStatusDisagreementReturnDto> GetAllHousekeepingStatusDisagreement(SearchReportHousekeepingStatusDisagreementDto request, int propertyId)
        {

            DateTime? systemDate = _propertyParameterReadRepository.GetSystemDate(propertyId);

            if (systemDate == null)
            {
                Notification.Raise(Notification.DefaultBuilder
                .WithMessage(AppConsts.LocalizationSourceName, PropertyAuditProcess.EntityError.PropertyAuditProcessInvalidPropertySystemDate)
                .WithDetailedMessage(AppConsts.LocalizationSourceName, PropertyAuditProcess.EntityError.PropertyAuditProcessInvalidPropertySystemDate)
                .Build());

                return null;
            }
            else if (request.Date.HasValue && request.Date > systemDate)
            {
                Notification.Raise(Notification.DefaultBuilder
               .WithMessage(AppConsts.LocalizationSourceName, Reservation.EntityError.ReportDateError)
               .WithDetailedMessage(AppConsts.LocalizationSourceName, Reservation.EntityError.ReportDateError)
               .Build());

                return null;
            }
            else
                return _reservationReportReadRepository.GetAllHousekeepingStatusDisagreement(request, propertyId, systemDate);
        }

        public List<PostHousekeepingRoomDisagreementDto> PostHousekeepingRoomDisagreement(List<PostHousekeepingRoomDisagreementDto> request)
        {
            var nullResponseInstance = new List<PostHousekeepingRoomDisagreementDto>();

            if (request == null || request.Exists(x => x.PropertyId <= 0))
            {
                NotifyIdIsMissing();
                return nullResponseInstance;
            }

            var housekeepingRoomDisagreementDto = new List<PostHousekeepingRoomDisagreementDto>();


            DateTime? systemDate = _propertyParameterReadRepository.GetSystemDate(request.FirstOrDefault().PropertyId);

            if (systemDate == null)
            {
                Notification.Raise(Notification.DefaultBuilder
                .WithMessage(AppConsts.LocalizationSourceName, PropertyAuditProcess.EntityError.PropertyAuditProcessInvalidPropertySystemDate)
                .WithDetailedMessage(AppConsts.LocalizationSourceName, PropertyAuditProcess.EntityError.PropertyAuditProcessInvalidPropertySystemDate)
                .Build());

                return null;
            }
            else
            {
                foreach (var data in request)
                {
                    if(data.Date.Date != systemDate.Value.Date && !data.HousekeepingStatusId.HasValue)
                    {
                        Notification.Raise(Notification.DefaultBuilder
                        .WithMessage(AppConsts.LocalizationSourceName, Reservation.EntityError.ReportDateInvalid)
                        .WithDetailedMessage(AppConsts.LocalizationSourceName, Reservation.EntityError.ReportDateInvalid)
                        .Build());

                        return null;
                    }
                    else
                    {
                        var result = _reservationReportRepository.PostHousekeepingRoomDisagreement(data, systemDate);
                        housekeepingRoomDisagreementDto.Add(result);
                    }                  
                }

                return housekeepingRoomDisagreementDto;
            }           
        }

        public ReportReservationsMadeReturnListDto GetReservationsMade(SearchReservationsMadeReportDto requestDto, int propertyId)
        {
            ValidateDate(requestDto);

            if (Notification.HasNotification()) return null;

            var reservationsMadeList = _reservationReportReadRepository.GetReservationsMade(requestDto, propertyId);

            var reportReservationsMadeTotalizerList = new List<ReportReservationsMadeTotalizerReturnDto>();

            var totalizer = reservationsMadeList.Items.DistinctBy(x => new { x.ReservationStatusId, x.ReservationCode })
                                                .GroupBy(x =>  x.ReservationStatusId)
                                                .Select(gp => new ReportReservationsMadeTotalizerReturnDto
                                                {
                                                    ReservationStatus = _localizationManager.GetString(AppConsts.LocalizationSourceName,
                                                                    ((ReservationStatus)gp.Key).ToString()),                                                                  
                                                    Total = gp.Count()                        
                                                }).ToList();

            var result = new ReportReservationsMadeReturnListDto()
            {
                ReportReservationsMadeReturnDtoList = reservationsMadeList,
                ReportReservationsMadeTotalizerReturnDtoList = totalizer
            };

            return result;
        }       

        public async Task<ReportReturnDto> GetGraficReport(int propertyId)
        {
            DateTime? systemDate = await _propertyParameterReadRepository.GetSystemDateAsync(propertyId);

            var totalUhSituationTask = GetTotalUhSituation(propertyId);
            var totalHousekeepingStatusTask = GetTotalHousekeepingStatus(propertyId);
            var totalReservationStatusTask = GetTotalReservationStatus(propertyId, systemDate);
            var totalCheckStatusTask = GetTotalCheckStatus(propertyId, systemDate);

            await Task.WhenAll(totalUhSituationTask, totalHousekeepingStatusTask, totalReservationStatusTask, totalCheckStatusTask);

            var totalUhSituation = totalUhSituationTask.Result;
            var totalHousekeepingStatus = totalHousekeepingStatusTask.Result;
            var totalReservationStatus = totalReservationStatusTask.Result;
            var totalCheckStatus = totalCheckStatusTask.Result;           

            var response = new ReportReturnDto()
            {
                ReportUhSituationReturnDto = totalUhSituation,
                ReportHousekeepingStatusReturnDto = totalHousekeepingStatus,
                ReportCheckStatusReturnDto = totalCheckStatus,
                ReportReservationStatusReturnDto = totalReservationStatus
            };

            return response;
        }

        public ListDto<ReservationsInAdvanceReturnDto> GetAllReservationsInAdvance(SearchReservationsInAdvanceDto request, int propertyId)
        {
            return _reservationReportReadRepository.GetAllReservationsInAdvance(request, propertyId);
        }

        public async Task<PropertyDashboardDto> GetDashboardInformationsAsync()
        {
            var systemDate = (_propertyParameterReadRepository.GetSystemDate(int.Parse(_applicationUser.PropertyId)) ?? default(DateTime)).Date;

            var totalUhSituationTask = GetTotalUhSituation(int.Parse(_applicationUser.PropertyId));
            var guestsTask = _guestReservationItemReadRepository.GetAllForPropertyDashboardAsync(systemDate, int.Parse(_applicationUser.PropertyId));
            var reservationsTask = _reservationItemReadRepository.GetReservationsForDashboardAsync(systemDate, int.Parse(_applicationUser.PropertyId));
            var propertyInformationsTask = _propertyReadRepository.GetInformationsForDashboardAsync(int.Parse(_applicationUser.PropertyId));

            await Task.WhenAll(totalUhSituationTask, guestsTask, reservationsTask, propertyInformationsTask);

            var dashboardInformations = new PropertyDashboardDto();
            var totalRooms = totalUhSituationTask.Result.Blocked + totalUhSituationTask.Result.Vague + totalUhSituationTask.Result.Busy;
            dashboardInformations.Overview = new PropertyDashboardOverviewDto
            {
                PropertyName = propertyInformationsTask.Result.PropertyName,
                ChainName = propertyInformationsTask.Result.ChainName,
                TotalCheckIn = reservationsTask.Result.Count(r => r.CheckInDate?.Date == systemDate),
                TotalCheckOut = reservationsTask.Result.Count(r => r.CheckOutDate?.Date == systemDate),
                TotalWalkIn = reservationsTask.Result.Count(r => r.WalkIn && r.CheckInDate?.Date == systemDate),
                TotalDayUse = reservationsTask.Result.Count(r => r.EstimatedArrivalDate.Date == systemDate && r.EstimatedDepartureDate.Date == systemDate),
                TotalAvailableRoom = totalUhSituationTask.Result.Vague,
                TotalOccupiedRoom = totalUhSituationTask.Result.Busy,
                OccupationPercentage = totalRooms > 0 ? decimal.Round(Decimal.Divide(totalUhSituationTask.Result.Busy, (totalRooms)) * 100, 2) : 0.0m
            };

            dashboardInformations.CheckInList = guestsTask.Result.Where(g => g.GuestStatusId == (int)ReservationStatus.Confirmed || g.GuestStatusId == (int)ReservationStatus.ToConfirm).ToList();
            dashboardInformations.CheckOutList = guestsTask.Result.Where(g => g.GuestStatusId == (int)ReservationStatus.Checkin).ToList();

            return dashboardInformations;
        }

        private void ValidateDate(SearchReservationsMadeReportDto requestDto)
        {
            if (!requestDto.InitialDate.HasValue || !requestDto.EndDate.HasValue)
            {
                Notification.Raise(Notification.DefaultBuilder
                       .WithMessage(AppConsts.LocalizationSourceName, Reservation.EntityError.DateMustHaveValue)
                       .WithDetailedMessage(AppConsts.LocalizationSourceName, Reservation.EntityError.DateMustHaveValue)
                       .Build());

                return;
            }
            else if (requestDto.InitialDate.Value.Date > requestDto.EndDate.Value.Date)
            {
                Notification.Raise(Notification.DefaultBuilder
                    .WithMessage(AppConsts.LocalizationSourceName, Reservation.EntityError.InvalidDate)
                    .WithDetailedMessage(AppConsts.LocalizationSourceName, Reservation.EntityError.InvalidDate)
                    .Build());

                return;
            }
        }
    }
}
