﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Thex.Application.Interfaces;
using Thex.Application.Interfaces.RateProposal;
using Thex.Dto;
using Thex.Infra.ReadInterfaces;

namespace Thex.Application.Services.RateProposal
{
    public class RateProposalAppService : IRateProposalAppService
    {
        protected readonly IReservationBudgetAppService _reservationBudgetAppService;
        protected readonly IRoomTypeReadRepository _roomTypeReadRepository;

        public RateProposalAppService(IRoomTypeReadRepository roomTypeReadRepository,
                                      IReservationBudgetAppService reservationBudgetAppService)
        {
            _reservationBudgetAppService = reservationBudgetAppService;
            _roomTypeReadRepository = roomTypeReadRepository;
        }

        public async Task<ReservationItemBudgetHeaderDto> GetAllhBudgetOfferWithRateProposal(ReservationItemBudgetRequestDto requestDto, int propertyId)
        {
        
            requestDto.RoomTypeIdList = await _roomTypeReadRepository.GetAllRoomTypeIdsByPropertyIdAsync(propertyId, requestDto.AdultCount, requestDto.ChildrenAge);

            var response = await _reservationBudgetAppService.GetWithBudgetOffer(requestDto, propertyId);

            if (requestDto.TotalBudgetDescending)
                response.CommercialBudgetList = response.CommercialBudgetList.OrderBy(x => x.Order).ThenByDescending(x => x.AgreementTypeId).ThenBy(x => x.MealPlanTypeId).ThenByDescending(x => x.TotalBudget).ToList();
            else
                response.CommercialBudgetList = response.CommercialBudgetList.OrderBy(x => x.Order).ThenByDescending(x => x.AgreementTypeId).ThenBy(x => x.MealPlanTypeId).ThenBy(x => x.TotalBudget).ToList();


            return response;
        }
    }
}
