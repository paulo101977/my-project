﻿using System;
using Thex.Application.Adapters;
using Thex.Domain.Entities;
using Thex.Application.Interfaces;
using Tnf.Domain.Services;
using Thex.Dto;
using Tnf.Dto;
using Thex.Common.Enumerations;
using Thex.Infra.ReadInterfaces;
using Thex.Dto.Reservation;
using Thex.Dto.Availability;
using Thex.Domain.Services.Interfaces;
using System.Linq;
using Thex.Common;
using Thex.Dto.Rooms;
using System.Collections.Generic;
using System.Threading.Tasks;
using Tnf.Notifications;
using Thex.Dto.RatePlan;
using Tnf.Localization;

namespace Thex.Application.Services
{
    public class RateCalendarAppService : ApplicationServiceBase, IRateCalendarAppService
    {
        protected readonly IAvailabilityAppService _availabilityAppService;
        protected readonly IRoomTypeDomainService _roomTypeDomainService;
        protected readonly IPropertyBaseRateReadRepository _propertyBaseRateReadRepository;
        protected readonly IPropertyParameterReadRepository _propertyParameterReadRepository;
        protected readonly IRatePlanRoomTypeReadRepository _ratePlanRoomTypeReadRepository;
        private readonly ICurrencyReadRepository _currencyReadRepository;
        private readonly IRoomTypeReadRepository _roomTypeReadRepository;
        private readonly IPropertyMealPlanTypeReadRepository _propertyMealPlanTypeReadRepository;
        private readonly IReservationBudgetDomainService _reservationBudgetDomainService;
        private readonly IRatePlanReadRepository _ratePlanReadRepository;
        private readonly ILocalizationManager _localizationManager;

        public RateCalendarAppService(
            IAvailabilityAppService availabilityAppService,
            IRoomTypeDomainService roomTypeDomainService,
            IPropertyBaseRateReadRepository propertyBaseRateReadRepository,
            IPropertyParameterReadRepository propertyParameterReadRepository,
            IRatePlanRoomTypeReadRepository ratePlanRoomTypeReadRepository,
            ICurrencyReadRepository currencyReadRepository,
            IRoomTypeReadRepository roomTypeReadRepository,
            IPropertyMealPlanTypeReadRepository propertyMealPlanTypeReadRepository,
            IReservationBudgetDomainService reservationBudgetDomainService,
            IRatePlanReadRepository ratePlanReadRepository,
            ILocalizationManager localizationManager,
            INotificationHandler notificationHandler)
            : base(notificationHandler)
        {
            _availabilityAppService = availabilityAppService;
            _roomTypeDomainService = roomTypeDomainService;
            _propertyBaseRateReadRepository = propertyBaseRateReadRepository;
            _propertyParameterReadRepository = propertyParameterReadRepository;
            _ratePlanRoomTypeReadRepository = ratePlanRoomTypeReadRepository;
            _currencyReadRepository = currencyReadRepository;
            _roomTypeReadRepository = roomTypeReadRepository;
            _propertyMealPlanTypeReadRepository = propertyMealPlanTypeReadRepository;
            _reservationBudgetDomainService = reservationBudgetDomainService;
            _ratePlanReadRepository = ratePlanReadRepository;
            _localizationManager = localizationManager;
        }

        public async Task<PropertyBaseRateConfigurationParametersDto> GetCalendarDataParameters(int propertyId)
        {
            var currencyTask = await _currencyReadRepository.GetAllCurrencyAsync();
            var roomTypeTask = await _roomTypeReadRepository.GetAllRoomTypesByPropertyIdAsync(propertyId);
            var mealPlanTypeTask = await _propertyMealPlanTypeReadRepository.GetAllPropertyMealPlanTypeByPropertyAsync(propertyId);

            var result = new PropertyBaseRateConfigurationParametersDto
            {
                CurrencyList = currencyTask,
                RoomTypeList = roomTypeTask,
                MealPlanTypeList = mealPlanTypeTask
            };

            return result;
        }

        public async Task<ListDto<RateCalendarDto>> GetRateCalendar(SearchRateCalendarDto requestDto, int propertyId)
        {
            var childParameterList = await _propertyParameterReadRepository.GetChildrenAgeGroupList(propertyId);
            var roomType = _roomTypeDomainService.Get(new DefaultIntRequestDto(requestDto.RoomTypeId), false, false);

            ValidateRateCalendarDto(requestDto, propertyId, childParameterList, roomType);

            if (Notification.HasNotification())
                return null;

            var resultList = new List<RateCalendarDto>();
            RatePlanWithRatePlanRoomTypeDto ratePlanRoomType = null;
            var propertyFixedRateList = new List<PropertyBaseRate>();
            bool isBaseRateType = false;

            var roomTypeIdList = new List<int>() { requestDto.RoomTypeId };

            var baseRateListTask = _propertyBaseRateReadRepository.GetAllByPeriod(propertyId, roomTypeIdList, requestDto.CurrencyId, requestDto.MealPlanTypeId, requestDto.InitialDate, requestDto.FinalDate, null, null, null, true);

            var baseRateList = await baseRateListTask;

            if (requestDto.RatePlanId.HasValue)
            {
                isBaseRateType = _ratePlanReadRepository.IsBaseRateType(requestDto.RatePlanId.Value);

                if(isBaseRateType)
                    ratePlanRoomType = await _ratePlanRoomTypeReadRepository.GetByRatePlanIdAndRoomTypeId(requestDto.RoomTypeId, requestDto.RatePlanId.Value);
                else
                    propertyFixedRateList = await _propertyBaseRateReadRepository.GetAllByPeriod(propertyId, roomTypeIdList, requestDto.CurrencyId, requestDto.MealPlanTypeId, requestDto.InitialDate, requestDto.FinalDate, null, true, requestDto.RatePlanId.Value);
            }

            var availability = _availabilityAppService.GetAvailabilityRoomTypes(null, propertyId, requestDto.InitialDate.Date, requestDto.FinalDate.Date);
            var availabilityPercentageList = availability.FooterList.FirstOrDefault(e => e.Id == (int)AvailabilityGridEnum.AvailabilityGridPercentage);
            var roomTypeTotal = availability.RoomTypeList.Where(e => e.Id == requestDto.RoomTypeId).FirstOrDefault();
            var availabilityList = availability.AvailabilityList.Where(e => e.RoomTypeId == requestDto.RoomTypeId);

            var date = requestDto.InitialDate.Date;
            while (date <= requestDto.FinalDate.Date)
            {
                var dateString = date.ToString("dd/MM/yyyy");
                var rateCalendarRow = new RateCalendarDto
                {
                    Date = dateString,
                    DateFormatted = date.ToString("s"),
                };

                var baseRate = baseRateList.FirstOrDefault(e => e.Date == date);

                if (baseRate != null)
                {
                    var baseRateResult = _reservationBudgetDomainService.CalculateBaseRate(requestDto.AdultCount, requestDto.ChildrenAge, childParameterList, baseRate, roomType);
                    rateCalendarRow.BaseRateAmount = baseRateResult.Total;
                    rateCalendarRow.LevelCode = baseRate.LevelEntity != null ? baseRate.LevelEntity.Description : _localizationManager.GetString(AppConsts.LocalizationSourceName, (LevelEnum.Basevalue).ToString());
                    rateCalendarRow.IsLevel = baseRate.LevelEntity != null;
                }

                if (ratePlanRoomType != null && baseRate != null && rateCalendarRow.BaseRateAmount.HasValue && isBaseRateType)
                    rateCalendarRow.BaseRateWithRatePlanAmount = _reservationBudgetDomainService.CalculateWithRatePlanAmount(rateCalendarRow.BaseRateAmount.Value, ratePlanRoomType.IsDecreased, ratePlanRoomType.PercentualAmmount, ratePlanRoomType.RoundingTypeId);

                if (propertyFixedRateList != null && propertyFixedRateList.Count > 0 && !isBaseRateType)
                {
                    var propertyFixedRate = propertyFixedRateList.FirstOrDefault(e => e.Date.Date == date.Date);

                    if (propertyFixedRate != null)
                    {
                        var propertyFixedRateResult = _reservationBudgetDomainService.CalculateBaseRate(requestDto.AdultCount, requestDto.ChildrenAge, childParameterList, propertyFixedRate, roomType);

                        rateCalendarRow.BaseRateWithRatePlanAmount = propertyFixedRateResult.Total;
                    }
                }

                var availabilityOccupation = availabilityPercentageList.Values.Where(e => e.Date == date.ToString("s")).FirstOrDefault();
                var availabilityDay = availabilityList.Where(e => e.Date == dateString).FirstOrDefault();

                rateCalendarRow.RoomTotal = roomTypeTotal != null ? roomTypeTotal.Total : 0;
                rateCalendarRow.OccupiedRooms = availabilityDay != null && roomTypeTotal != null ? availabilityDay.AvailableRooms > 0 ? roomTypeTotal.Total - availabilityDay.AvailableRooms : roomTypeTotal.Total + (availabilityDay.AvailableRooms * (-1)) : 0;
                rateCalendarRow.Occupation = availabilityOccupation != null ? availabilityOccupation.Number : 0;
                rateCalendarRow.CurrencyId = baseRate?.CurrencyId;
                rateCalendarRow.CurrencySymbol = baseRate?.CurrencySymbol;

                resultList.Add(rateCalendarRow);
                date = date.AddDays(1);
            }

            return new ListDto<RateCalendarDto>
            {
                HasNext = false,
                Items = resultList
            };
        }

        private void ValidateRateCalendarDto(SearchRateCalendarDto requestDto, int propertyId, List<PropertyParameterDto> childParameterList, RoomType roomType)
        {
            //idade das crianças deve coincidir com o parametro
            var maxChildAge = childParameterList.Where(e => e.IsActive.HasValue && e.IsActive.Value).Select(e => Convert.ToInt32(e.PropertyParameterValue)).Max();

            if (propertyId == 0)
                NotifyParameterInvalid();

            if (requestDto.ChildrenAge != null && requestDto.ChildrenAge.Any(e => e > maxChildAge))
                RaiseNotification(RateCalendarEnum.Error.RateCalendarInvalidChildAge, RateCalendarEnum.Error.RateCalendarInvalidChildAge);

            //data inicial e final
            if (requestDto.FinalDate.Date < requestDto.InitialDate.Date || (requestDto.FinalDate.Date - requestDto.InitialDate.Date).TotalDays > 62)
                RaiseNotification(RateCalendarEnum.Error.RateCalendarInvalidPeriod, RateCalendarEnum.Error.RateCalendarInvalidPeriod);

            //roomtypeid é obrigatorio e deve pertencer a property
            if (requestDto.RoomTypeId == 0 || roomType == null || roomType.PropertyId != propertyId)
                RaiseNotification(RateCalendarEnum.Error.RateCalendarInvalidRoomType, RateCalendarEnum.Error.RateCalendarInvalidRoomType);

            //moeda é obrigatório
            if (requestDto.CurrencyId == Guid.Empty)
                RaiseNotification(RateCalendarEnum.Error.RateCalendarInvalidCurrency, RateCalendarEnum.Error.RateCalendarInvalidCurrency);

            //tipo de pensão é obrigatório
            if (requestDto.MealPlanTypeId == 0)
                RaiseNotification(RateCalendarEnum.Error.RateCalendarInvalidPropertyMealPlanType, RateCalendarEnum.Error.RateCalendarInvalidPropertyMealPlanType);

            //obrigatorio 
            //qtd de adultos e crianças n pode ser maior que da roomtype enviada
            //qtd de adultos é obrigatoria sempre
            //qtd de adultos pode ser zero se a qtd de crianças é maior que 0
            if (requestDto.AdultCount > roomType.AdultCapacity ||
                (requestDto.ChildrenAge != null && requestDto.ChildrenAge?.Count() > roomType.ChildCapacity) ||
                (requestDto.AdultCount == 0 && (requestDto.ChildrenAge != null || requestDto.ChildrenAge?.Count() == 0)))
                RaiseNotification(RateCalendarEnum.Error.RateCalendarInvalidAdultCapacityOrChildCapacityPropertyMealPlanType, RateCalendarEnum.Error.RateCalendarInvalidAdultCapacityOrChildCapacityPropertyMealPlanType);
        }

        //private decimal CalculateBaseRateWithRatePlanAmount(decimal baseRateAmount, RatePlanRoomTypeDto ratePlanRoomType)
        //{
        //    if (ratePlanRoomType == null)
        //        return baseRateAmount;

        //    var percentage = (ratePlanRoomType.IsDecreased ? 
        //                            ratePlanRoomType.PercentualAmmount * -1 : 
        //                            ratePlanRoomType.PercentualAmmount) / 100;
        //    //20 + (20 * (10 * -1) / 100)
        //    return baseRateAmount + (baseRateAmount * percentage);
        //}

        //private decimal CalculateBaseRate(int adultCount, List<int> childrenAgeList, List<PropertyParameterDto> childParameterList, PropertyBaseRate baseRate, RoomType roomType)
        //{
        //    decimal baseRateSum;
        //    decimal paxChildrenSum = 0;
        //    var paxAdultValue = adultCount == 0 ? 0 : adultCount <= 5 ? (decimal?)baseRate["Pax" + adultCount + "Amount"] : (decimal?)baseRate["PaxAdditionalAmount"];
        //    var roomTypeCapacity = new Dictionary<int, int>();

        //    var childrenCount = childrenAgeList == null ? 0 : childrenAgeList.Count();

        //    if (childrenCount >= 1 && childrenCount <= 3)
        //    {
        //        for (int i = 0; i < childrenCount; i++)
        //        {
        //            var childAge = childrenAgeList[i];

        //            var childPos = 0;
        //            var keepGoing = true;
        //            //pode estar desativado 1 parametro de child
        //            childParameterList
        //                .Where(e => e.IsActive.HasValue && e.IsActive.Value)
        //                .Select(e => Convert.ToInt32(e.PropertyParameterValue))
        //                .OrderBy(e => e)
        //                .ToList()
        //                .ForEach(delegate (int value)
        //                {
        //                    if(keepGoing)
        //                    { 
        //                        childPos++;

        //                        if (childAge <= value)
        //                            keepGoing = false;
        //                    }
                            
        //                });

        //            //var paxChildValue = (decimal?)baseRate["Child" + (childPos == 0 ? 1 : childPos) + "Amount"];

        //            //while (paxChildValue == null)
        //            //{
        //            //    childPos--;

        //            //    if (childPos < 1)
        //            //        paxChildValue = 0;
        //            //    else
        //            //        paxChildValue = (decimal?)baseRate["Child" + childPos + "Amount"];
        //            //}

        //            decimal? paxChildValue = (decimal?)baseRate["Child" + (childPos == 0 ? 1 : childPos) + "Amount"] ?? 0;

        //            if (childPos == 3 && roomType.FreeChildQuantity3 > 0)
        //                paxChildValue = CalculatePaxChildValueBasedOnFreeChildQuantity(roomType, roomTypeCapacity, childPos, paxChildValue);

        //            if (childPos == 2 && roomType.FreeChildQuantity2 > 0)
        //                paxChildValue = CalculatePaxChildValueBasedOnFreeChildQuantity(roomType, roomTypeCapacity, childPos, paxChildValue);

        //            if ((childPos == 0 || childPos == 1)  && roomType.FreeChildQuantity2 > 0)
        //                paxChildValue = CalculatePaxChildValueBasedOnFreeChildQuantity(roomType, roomTypeCapacity, childPos, paxChildValue);


        //            paxChildrenSum = paxChildrenSum + paxChildValue.Value;
        //        }
        //    }

        //    baseRateSum = paxAdultValue.Value + paxChildrenSum;
        //    return baseRateSum;
        //}

        //private static decimal? CalculatePaxChildValueBasedOnFreeChildQuantity(RoomType roomType, Dictionary<int, int> roomTypeCapacity, int childPos, decimal? paxChildValue)
        //{
        //    var checkRoomTypeCapacity = roomTypeCapacity.Where(e => e.Key == childPos).Count();

        //    if (checkRoomTypeCapacity == 0 || checkRoomTypeCapacity < roomType.FreeChildQuantity3)
        //    {
        //        roomTypeCapacity.Add(childPos, 1);
        //        paxChildValue = 0;
        //    }

        //    return paxChildValue;
        //}

        private void RaiseNotification(Enum notificationEnumMessage, Enum notificationEnumDetailedMessage)
        {
            var notification = Notification
                .DefaultBuilder
                .WithMessage(AppConsts.LocalizationSourceName, notificationEnumMessage);

            Notification.Raise(notification.Build());
        }

    }
}
