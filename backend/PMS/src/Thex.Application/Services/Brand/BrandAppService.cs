﻿using System;
using Thex.Application.Adapters;
using Thex.Domain.Entities;
using Thex.Application.Interfaces;
using Tnf.Domain.Services;
using Thex.Dto;
using System.Threading.Tasks;

using Tnf.Dto;
using Tnf.Notifications;

namespace Thex.Application.Services
{
    public class BrandAppService : ScaffoldBrandAppService, IBrandAppService
    {
        private readonly IDomainService<Chain> _chainDomainService;
        public BrandAppService(
            IBrandAdapter brandAdapter,
            IDomainService<Brand> brandDomainService,
            IDomainService<Chain> chainDomainService,
            INotificationHandler notificationHandler)
            : base(brandAdapter, brandDomainService, notificationHandler)
        {
            _chainDomainService = chainDomainService;
        }

        #region Get Builder

        public Brand.Builder GetBuilder(BrandDto brandDto)
        {
            var builder = new Brand.Builder(Notification);

            builder
                .WithName(brandDto.Name)
                .WithId(brandDto.Id)
                .WithChainId(brandDto.Chain.Id);

            return builder;
        }

        public Chain.Builder GetChainBuilder(BrandDto brandDto)
        {
            Chain.Builder builder = new Chain.Builder(Notification);

            builder
                .WithName(brandDto.Chain.Name)
                .WithId(brandDto.Chain.Id);

            return builder;
        }

        #endregion

        public override async Task<BrandDto> Create(BrandDto dto)
        {
            if (dto == null || dto.Chain == null)
            {
                NotifyNullParameter();
                return BrandDto.NullInstance;
            }

            ValidateDto(dto, nameof(dto));

            if (Notification.HasNotification())
                return BrandDto.NullInstance;

            dto.Chain.Id = (await _chainDomainService.InsertAndSaveChangesAsync(GetChainBuilder(dto))).Id;

            dto.Id = (await BrandDomainService.InsertAndSaveChangesAsync(GetBuilder(dto))).Id;

            return dto;
        }

        public override async Task<BrandDto> Update(int id, BrandDto dto)
        {
            if (dto == null)
                NotifyNullParameter();

            if (dto.Id <= 0 || (dto.Chain == null || dto.Chain.Id <= 0))
                NotifyIdIsMissing();

            ValidateDtoAndId(dto, id, nameof(dto), nameof(id));

            if (Notification.HasNotification())
                return BrandDto.NullInstance;

            dto.Id = id;

            await _chainDomainService.UpdateAsync(GetChainBuilder(dto)).ConfigureAwait(false);
            await BrandDomainService.UpdateAsync(GetBuilder(dto)).ConfigureAwait(false);

            return dto;
        }

        public override async Task Delete(int id)
        {
            if (id < 0)
                NotifyIdIsMissing();

            if (!ValidateId(id)) return;

            var brand = await BrandDomainService.GetAsync(new DefaultIntRequestDto(id)).ConfigureAwait(false);
            if (brand == null)
                NotifyWhenEntityNotExist("Brand");

            if (!Notification.HasNotification())
                await BrandDomainService.DeleteAsync(w => w.Id == id);
        }
    }
}
