﻿using System;
using Thex.Application.Adapters;
using Thex.Domain.Entities;
using Thex.Application.Interfaces;
using Tnf.Domain.Services;
using Thex.Dto;
using System.Threading.Tasks;
using Tnf.Dto;
using Thex.Domain.Services.Interfaces;
using Thex.Common;
using Thex.Common.Enumerations;
using System.Linq;
using Thex.Dto.GuestReservationItem;
using System.Collections.Generic;
using Thex.Infra.ReadInterfaces;
using Thex.Domain.Interfaces.Repositories;
using Tnf.Notifications;
using Tnf.Localization;
using Thex.Kernel;
using Tnf.Repositories.Uow;
using Thex.Domain.Interfaces.Services;

namespace Thex.Application.Services
{
    public class GuestReservationItemAppService : ScaffoldGuestReservationItemAppService, IGuestReservationItemAppService
    {
        private readonly IDomainService<PropertyGuestType> _propertyGuestTypeDomainService;
        private readonly IDomainService<ReservationItem> _reservationItemDomainService;
        private readonly IGuestReservationItemDomainService _guestReservationItemDomainService;
        private readonly ICheckinAppService _checkinAppService;
        private readonly IReservationItemAppService _reservationItemAppservice;
        private readonly IGuestAppService _guestAppService;
        private readonly IContactInformationAppService _contactInformationAppService;
        private readonly IDocumentReadRepository _documentReadRepository;
        private readonly IPersonReadRepository _personReadRepository;
        private readonly IPropertyParameterReadRepository _propertyParameterReadRepository;
        private readonly IGuestReservationItemReadRepository _guestReservationItemReadRepository;
        private readonly IGuestReservationItemRepository _guestReservationItemRepository;
        private readonly ILocalizationManager _localizationManager;
        private readonly IApplicationUser _applicationUser;
        private readonly IBillingAccountItemAppService _billingAccountItemAppService;
        private readonly IReservationItemReadRepository _reservationItemReadRepository;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IHousekeepingStatusPropertyAppService _housekeepingStatusPropertyAppService;
        private readonly IRoomRepository _roomRepository;
        private readonly ICountrySubdivisionTranslationAppService _countrySubdivisionTranslationAppService;
        private readonly IReservationItemStatusDomainService _reservationItemStatusDomainService;
        private readonly EntityFrameworkCore.ReadInterfaces.Repositories.IRoomReadRepository _roomReadRepository;
        private readonly IRoomOccupiedDomainService _roomOccupiedDomainService;
        private readonly IRoomOccupiedReadRepository _roomOccupiedReadRepository;
        private readonly IGuestRegistrationReadRepository _guestRegistrationReadRepository;
        private readonly ICountrySubdivisionTranslationReadRepository _countrySubdivisionTranslationReadRepository;
        private readonly IGuestRegistrationAppService _guestRegistrationAppService;
        private readonly ICheckinDomainService _checkinDomainService;
        private readonly IBillingAccountReadRepository _billingAccountReadRepository;

        public GuestReservationItemAppService(
            IUnitOfWorkManager unitOfWorkManager,
            IGuestReservationItemAdapter guestReservationItemAdapter,
            IGuestReservationItemDomainService guestReservationItemDomainService,
            IDomainService<PropertyGuestType> propertyGuestTypeDomainService,
            IDomainService<ReservationItem> reservationItemDomainService,
            IReservationItemAppService reservationItemAppservice,
            ICheckinAppService checkinAppService,
            IGuestReservationItemReadRepository guestReservationItemReadRepository,
            IGuestAppService guestAppService,
            IContactInformationAppService contactInformationAppService,
            IDocumentReadRepository documentReadRepository,
            IPersonReadRepository personReadRepository,
            IPropertyParameterReadRepository propertyParameterReadRepository,
            IGuestReservationItemRepository guestReservationItemRepository,
            INotificationHandler notificationHandler,
            IApplicationUser applicationUser,
            IBillingAccountItemAppService billingAccountItemAppService,
            IReservationItemReadRepository reservationItemReadRepository,
            ILocalizationManager localizationManager,
            IHousekeepingStatusPropertyAppService housekeepingStatusPropertyAppService,
            IRoomRepository roomRepository,
            ICountrySubdivisionTranslationAppService countrySubdivisionTranslationAppService,
            IReservationItemStatusDomainService reservationItemStatusDomainService,
            EntityFrameworkCore.ReadInterfaces.Repositories.IRoomReadRepository roomReadRepository,
            IRoomOccupiedDomainService RoomOccupiedDomainService,
            IRoomOccupiedAdapter RoomOccupiedAdapter,
            IRoomOccupiedReadRepository roomOccupiedReadRepository,
            IGuestRegistrationReadRepository guestRegistrationReadRepository,
            ICountrySubdivisionTranslationReadRepository countrySubdivisionTranslationReadRepository,
            IGuestRegistrationAppService guestRegistrationAppService,
            ICheckinDomainService checkinDomainService,
            IBillingAccountReadRepository billingAccountReadRepository)
            : base(guestReservationItemAdapter, guestReservationItemDomainService, notificationHandler, RoomOccupiedAdapter)
        {
            _propertyGuestTypeDomainService = propertyGuestTypeDomainService;
            _reservationItemDomainService = reservationItemDomainService;
            _guestReservationItemDomainService = guestReservationItemDomainService;
            _reservationItemAppservice = reservationItemAppservice;
            _checkinAppService = checkinAppService;
            _guestAppService = guestAppService;
            _contactInformationAppService = contactInformationAppService;
            _documentReadRepository = documentReadRepository;
            _personReadRepository = personReadRepository;
            _propertyParameterReadRepository = propertyParameterReadRepository;
            _guestReservationItemReadRepository = guestReservationItemReadRepository;
            _guestReservationItemRepository = guestReservationItemRepository;
            _localizationManager = localizationManager;
            _applicationUser = applicationUser;
            _billingAccountItemAppService = billingAccountItemAppService;
            _reservationItemReadRepository = reservationItemReadRepository;
            _unitOfWorkManager = unitOfWorkManager;
            _housekeepingStatusPropertyAppService = housekeepingStatusPropertyAppService;
            _roomRepository = roomRepository;
            _countrySubdivisionTranslationAppService = countrySubdivisionTranslationAppService;
            _reservationItemStatusDomainService = reservationItemStatusDomainService;
            _roomReadRepository = roomReadRepository;
            _roomOccupiedDomainService = RoomOccupiedDomainService;
            _roomOccupiedReadRepository = roomOccupiedReadRepository;
            _guestRegistrationReadRepository = guestRegistrationReadRepository;
            _countrySubdivisionTranslationReadRepository = countrySubdivisionTranslationReadRepository;
            _guestRegistrationAppService = guestRegistrationAppService;
            _checkinDomainService = checkinDomainService;
            _billingAccountReadRepository = billingAccountReadRepository;
        }

        public Task<GuestReservationItemDto> Get(DefaultLongRequestDto id)
        {
            if (id.Id < 0)
                NotifyIdIsMissing();

            return base.Get(id);
        }

        public override async Task<GuestReservationItemDto> Create(GuestReservationItemDto dto)
        {
            using (var uow = _unitOfWorkManager.Begin())
            {
                if (dto == null)
                {
                    NotifyNullParameter();
                    return GuestReservationItemDto.NullInstance;
                }
                ValidateDto<GuestReservationItemDto>(dto, nameof(dto));

                if (Notification.HasNotification())
                    return GuestReservationItemDto.NullInstance;

                var reservationItemDto = await GetReservationItemDtoByGuestReservationItem(dto);

                _guestReservationItemDomainService.CheckSumOfPctDailyIs100(dto, reservationItemDto);

                if (Notification.HasNotification())
                    return GuestReservationItemDto.NullInstance;

                var childAgeParameter = _propertyParameterReadRepository.GetPropertyParameterMaxChildrenAge(dto.PropertyId);

                var builder = GetGuestReservationItemBuilder(dto, reservationItemDto, childAgeParameter);

                dto.Id = (await GuestReservationItemDomainService.InsertAndSaveChangesAsync(builder)).Id;


                await uow.CompleteAsync();
            }

            return dto;
        }

        public override async Task<GuestReservationItemDto> Update(long id, GuestReservationItemDto dto)
        {
            using (var uow = _unitOfWorkManager.Begin())
            {

                if (dto == null)
                {
                    NotifyNullParameter();
                    return GuestReservationItemDto.NullInstance;
                }

                ValidateDtoAndId(dto, id, nameof(dto), nameof(id));

                if (Notification.HasNotification())
                    return GuestReservationItemDto.NullInstance;

                dto.Id = id;

                var reservationItemDto = await GetReservationItemDtoByGuestReservationItem(dto);

                _guestReservationItemDomainService.CheckSumOfPctDailyIs100(dto, reservationItemDto);

                if (Notification.HasNotification())
                    return GuestReservationItemDto.NullInstance;

                var childAgeParameter = _propertyParameterReadRepository.GetPropertyParameterMaxChildrenAge(dto.PropertyId);

                var builder = GetGuestReservationItemBuilder(dto, reservationItemDto, childAgeParameter, true);

                GuestReservationItemDomainService.Update(builder);

                await uow.CompleteAsync();
            }

            return dto;
        }


        public new void Delete(long id)
        {
            using (var uow = _unitOfWorkManager.Begin())
            {
                if (id < 0)
                    NotifyIdIsMissing();

                if (!ValidateId(id)) return;

                if (!Notification.HasNotification())
                {
                    var guestReservationItem = _guestReservationItemDomainService.Get(new DefaultLongRequestDto(id));
                    if (guestReservationItem == null)
                    {
                        NotifyWhenEntityNotExist("Guest Reservation Item");
                        return;
                    }

                    if (!_guestReservationItemDomainService
                        .HasValidReservationItemStatusToModifyOrDelete(guestReservationItem))
                    {
                        Notification.Raise(Notification.DefaultBuilder
                            .WithMessage(AppConsts.LocalizationSourceName, GuestReservationItem.EntityError.GuestReservationItemHasInvalidReservationItemStatusToAssociate)
                            .WithDetailedMessage(AppConsts.LocalizationSourceName, GuestReservationItem.EntityError.GuestReservationItemHasInvalidReservationItemStatusToAssociate)
                            .Build());
                        return;
                    }

                    _guestReservationItemDomainService.Delete(guestReservationItem);
                }

                uow.Complete();
            }
        }

        private async Task<ReservationItemDto> GetReservationItemDtoByGuestReservationItem(GuestReservationItemDto dto)
        {
            var requestReservationItem = new DefaultLongRequestDto(dto.ReservationItemId)
            {
                Expand = "GuestReservationItemList"
            };

            var reservationItem = await _reservationItemDomainService.GetAsync(requestReservationItem).ConfigureAwait(false);
            var reservationItemDto = reservationItem.MapTo<ReservationItemDto>();

            if (reservationItemDto != null)
                reservationItemDto.ReservationItemStatusFormatted = _localizationManager.GetString(AppConsts.LocalizationSourceName, ((ReservationStatus)reservationItemDto.ReservationItemStatusId).ToString());

            return reservationItemDto;
        }

        #region Get Builder


        public GuestReservationItem.Builder GetGuestReservationItemFromReservationBuilder(GuestReservationItemDto guestReservationItemDto, ReservationItemDto reservationItemDto, PropertyParameterDto childAgeParameter, bool update = false)
        {
            var status = new int[] { (int)ReservationStatus.ToConfirm, (int)ReservationStatus.Confirmed }.ToList();

            GuestReservationItem.Builder builder = null;
            var childMaxAge = !string.IsNullOrEmpty(childAgeParameter.PropertyParameterValue) ? Convert.ToInt32(childAgeParameter.PropertyParameterValue) : (int?)null;

            _guestReservationItemDomainService.CheckEstimatedDateWithInvalidPeriodBasedOnReservationDto(guestReservationItemDto, reservationItemDto, update);

            builder = new GuestReservationItem.Builder(Notification, childMaxAge);
            if (update && guestReservationItemDto.Id > 0)
            {
                var id = new DefaultLongRequestDto(guestReservationItemDto.Id);
                var guestReservationItem = GuestReservationItemDomainService.Get(id);


                builder
                    .WithReservationItemId(guestReservationItem.ReservationItemId)
                    .WithPreferredName(guestReservationItem.PreferredName)
                    .WithGuestEmail(guestReservationItem.GuestEmail)
                    .WithGuestDocumentTypeId(guestReservationItem.GuestDocumentTypeId)
                    .WithGuestDocument(guestReservationItem.GuestDocument)
                    .WithGuestCitizenship(guestReservationItem.GuestCitizenship)
                    .WithGuestLanguage(guestReservationItem.GuestLanguage)
                    .WithCheckInDate(guestReservationItem.CheckInDate)
                    .WithCheckOutDate(guestReservationItem.CheckOutDate)
                    .WithIsIncognito(guestReservationItem.IsIncognito)
                    .WithPctDailyRate(guestReservationItem.PctDailyRate)
                    .WithGuestTypeId(guestReservationItem.GuestTypeId)
                    .WithGuestStatusId(status.Contains(reservationItemDto.ReservationItemStatusId) ? reservationItemDto.ReservationItemStatusId : guestReservationItem.GuestStatusId)
                    .WithEstimatedArrivalDate(guestReservationItem.EstimatedArrivalDate)
                    .WithEstimatedDepartureDate(guestReservationItem.EstimatedDepartureDate)
                    .WithGuestId(guestReservationItemDto.GuestId)
                    .WithGuestRegistrationId(guestReservationItem.GuestRegistrationId);

            }
            else
            {
                guestReservationItemDto.SetGuestStatusDefaultIfNotExist();
                guestReservationItemDto.SetGuestTypeDefaultIfNotExist();
                guestReservationItemDto.SetPctDailyRate100IfIsMain();

                builder
                        .WithReservationItemId(guestReservationItemDto.ReservationItemId)
                        .WithPreferredName(guestReservationItemDto.PreferredName)
                        .WithGuestEmail(guestReservationItemDto.GuestEmail)
                        .WithGuestDocumentTypeId(guestReservationItemDto.GuestDocumentTypeId)
                        .WithGuestDocument(guestReservationItemDto.GuestDocument)
                        .WithGuestCitizenship(guestReservationItemDto.GuestCitizenship)
                        .WithGuestLanguage(guestReservationItemDto.GuestLanguage)
                        .WithCheckInDate(guestReservationItemDto.CheckInDate)
                        .WithCheckOutDate(guestReservationItemDto.CheckOutDate)
                        .WithIsIncognito(guestReservationItemDto.IsIncognito)
                        .WithPctDailyRate(guestReservationItemDto.PctDailyRate)
                        .WithGuestTypeId(guestReservationItemDto.GuestTypeId)
                        .WithGuestStatusId(status.Contains(reservationItemDto.ReservationItemStatusId) ? reservationItemDto.ReservationItemStatusId : guestReservationItemDto.GuestStatusId)
                        .WithEstimatedArrivalDate(reservationItemDto.EstimatedArrivalDate)
                        .WithEstimatedDepartureDate(reservationItemDto.EstimatedDepartureDate)
                        .WithGuestId(guestReservationItemDto.GuestId);
            }


            builder
                .WithIsChild(guestReservationItemDto.IsChild)
                .WithIsMain(guestReservationItemDto.IsMain)
                .WithGuestId(guestReservationItemDto.GuestId)
                .WithGuestName(guestReservationItemDto.GuestName)
                .WithChildAge(guestReservationItemDto.ChildAge)
                .WithIsGratuity(guestReservationItemDto.IsGratuity)
                .WithIsSeparated(guestReservationItemDto.IsSeparated)
                .WithPaxPosition(guestReservationItemDto.PaxPosition)
                .WithId(guestReservationItemDto.Id)
                .WithPropertyId(Convert.ToInt32(_applicationUser.PropertyId));


            return builder;
        }

        public GuestReservationItem.Builder GetGuestReservationItemBuilder(GuestReservationItemDto guestReservationItemDto, ReservationItemDto reservationItemDto, PropertyParameterDto childAgeParameter, bool update = false)
        {
            GuestReservationItem.Builder builder = null;

            _guestReservationItemDomainService.CheckEstimatedDateWithInvalidPeriodBasedOnReservationDto(guestReservationItemDto, reservationItemDto, update);

            if (Notification.HasNotification())
                return null;

            guestReservationItemDto.SetGuestStatusDefaultIfNotExist();
            guestReservationItemDto.SetGuestTypeDefaultIfNotExist();

            var childMaxAge = !string.IsNullOrEmpty(childAgeParameter.PropertyParameterValue) ? Convert.ToInt32(childAgeParameter.PropertyParameterValue) : (int?)null;

            if (update)
            {
                var id = new DefaultLongRequestDto(guestReservationItemDto.Id);
                var guestReservationItem = GuestReservationItemDomainService.Get(id);
                builder = new GuestReservationItem.Builder(Notification, guestReservationItem, childMaxAge);
            }
            else
            {
                builder = new GuestReservationItem.Builder(Notification, childMaxAge);
            }

            builder
                .WithId(guestReservationItemDto.Id)
                .WithReservationItemId(guestReservationItemDto.ReservationItemId)
                .WithGuestId(guestReservationItemDto.GuestId)
                .WithGuestName(guestReservationItemDto.GuestName)
                .WithPreferredName(guestReservationItemDto.PreferredName)
                .WithGuestEmail(guestReservationItemDto.GuestEmail)
                .WithGuestDocumentTypeId(guestReservationItemDto.GuestDocumentTypeId)
                .WithGuestDocument(guestReservationItemDto.GuestDocument)
                .WithGuestCitizenship(guestReservationItemDto.GuestCitizenship)
                .WithGuestLanguage(guestReservationItemDto.GuestLanguage)
                .WithCheckInDate(guestReservationItemDto.CheckInDate)
                .WithCheckOutDate(guestReservationItemDto.CheckOutDate)
                .WithChildAge(guestReservationItemDto.ChildAge)
                .WithIsGratuity(guestReservationItemDto.IsGratuity)
                .WithIsSeparated(guestReservationItemDto.IsSeparated)
                .WithPaxPosition(guestReservationItemDto.PaxPosition)
                .WithIsIncognito(guestReservationItemDto.IsIncognito)
                .WithIsMain(guestReservationItemDto.IsMain)
                .WithPctDailyRate(guestReservationItemDto.PctDailyRate)
                .WithGuestTypeId(guestReservationItemDto.GuestTypeId)
                .WithGuestStatusId(guestReservationItemDto.GuestStatusId)
                .WithEstimatedArrivalDate(reservationItemDto.EstimatedArrivalDate)
                .WithEstimatedDepartureDate(reservationItemDto.EstimatedDepartureDate)
                .WithGuestRegistrationId(guestReservationItemDto.GuestRegistrationId);

            return builder;
        }

        #endregion

        public void Associate(long id, GuestReservationItemDto dto)
        {
            ValidateDtoAndId(dto, id, nameof(dto), nameof(id));

            if (Notification.HasNotification())
                return;

            using (var uow = _unitOfWorkManager.Begin())
            {

                dto.Id = id;

                ValidatePeriod(dto);

                if (Notification.HasNotification()) return;

                if (dto.PersonId.HasValue)
                {
                    dto.GuestId = _guestAppService.CreateGuestIfNotExistAndReturnGuest(dto.PersonId.Value);

                    var guestEmail = _contactInformationAppService.GetEmailContactInformationByPersonId(dto.PersonId.Value);
                    dto.GuestEmail = !String.IsNullOrEmpty(guestEmail) ? guestEmail : null;

                    var documents = _personReadRepository.GetDocumentsByPersonId(dto.PersonId.Value);

                    if (documents != null)
                    {
                        dto.GuestDocument = documents.FirstOrDefault().Document;
                        dto.GuestDocumentTypeId = documents.FirstOrDefault().DocumentTypeId;
                    }
                }

                var childAgeParameter = _propertyParameterReadRepository.GetPropertyParameterMaxChildrenAge(dto.PropertyId);

                int? childMaxAge;

                if (childAgeParameter != null && childAgeParameter.PropertyParameterValue != null)
                {
                    childMaxAge = Convert.ToInt32(childAgeParameter.PropertyParameterValue);
                }
                else
                {
                    childMaxAge = null;
                }

                if (dto.GuestId.HasValue)
                {
                    var guestRegistration = _guestRegistrationReadRepository.GetByGuestId(dto.GuestId.Value);
                    var nationalityId = _countrySubdivisionTranslationReadRepository.GetIdByCountryName(dto.GuestCitizenship);

                    if (guestRegistration.Id != null)
                        dto.GuestRegistrationId = guestRegistration.Id;

                    _guestRegistrationAppService.UpdateNationalityOfGuestRegistration(guestRegistration, nationalityId);
                }

                _guestReservationItemDomainService.Associate(dto, childMaxAge);

                uow.Complete();
            }
        }

        private void ValidatePeriod(GuestReservationItemDto dto)
        {
            var reservationItem = _reservationItemReadRepository.GetDtoById(dto.ReservationItemId);

            if (dto.EstimatedArrivalDate.Date < reservationItem.EstimatedArrivalDate.Date || dto.EstimatedDepartureDate.Date > reservationItem.EstimatedDepartureDate.Date)
            {
                Notification.Raise(Notification
                                 .DefaultBuilder
                                 .WithMessage(AppConsts.LocalizationSourceName, GuestReservationItem.EntityError.GuestReservationItemDateIsOutOfBound)
                                 .WithMessageFormat(AppConsts.LocalizationSourceName, GuestReservationItem.EntityError.GuestReservationItemDateIsOutOfBound)
                                 .Build());
            }
        }

        public void Disassociate(long id)
        {
            if (id < 0)
                NotifyIdIsMissing();

            if (!ValidateId(id)) return;

            if (Notification.HasNotification())
                return;

            using (var uow = _unitOfWorkManager.Begin())
            {
                var guestReservationItem = _guestReservationItemDomainService.Get(new DefaultLongRequestDto(id));
                if (guestReservationItem == null)
                {
                    NotifyWhenEntityNotExist("Guest Reservation Item");
                    return;
                }
                _guestReservationItemDomainService.Disassociate(id);

                uow.Complete();
            }
        }

        public void ConfirmCheckin(long reservationId, List<GuestReservationIdsDto> dto, int propertyId)
        {
            using (var uow = _unitOfWorkManager.Begin())
            {
                if (dto == null || dto.Count == 0)
                    NotifyParameterInvalid();

                var listGuestReservationItem = _guestReservationItemDomainService.GetListGuestReservationPerReservationItemIdForCheckin(dto.FirstOrDefault().GuestReservationItemId);

                var reservationItem = _reservationItemReadRepository.GetDtoById(listGuestReservationItem.FirstOrDefault().ReservationItemId);

                var roomIsUsed = false;
                if (reservationItem.RoomId.HasValue)
                    roomIsUsed = _roomOccupiedReadRepository.RoomIdUsed(reservationItem.RoomId.Value, reservationId);


                if (!roomIsUsed)
                    if (!reservationItem.RoomId.HasValue || (reservationItem.RoomId.HasValue && !_roomReadRepository.IsAvailableExceptReservationItemId(propertyId, reservationItem.RoomId.Value, reservationId)))
                    {
                        Notification.Raise(Notification.DefaultBuilder.WithMessage(AppConsts.LocalizationSourceName, ReservationItemEnum.Error.RoomIsNotAvaiable).Build());
                        return;
                    }

                if (!_reservationItemStatusDomainService.CanChangeReservationItem(reservationItem.ReservationItemStatusId))
                {
                    Notification.Raise(Notification.DefaultBuilder.WithMessage(AppConsts.LocalizationSourceName, ReservationItemEnum.Error.ReservationItemCanNotBeChanged).Build());
                    return;
                }

                if (_guestReservationItemDomainService.AnyGuestReservationItemWithStatusEqualCheckin(dto.Select(e => e.GuestReservationItemId).ToList()))
                    NotifyParameterInvalid();

                if (Notification.HasNotification())
                    return;

                bool err = false;

                var systemDate = _propertyParameterReadRepository.GetSystemDate(propertyId).Value;
                var vCheckInDate = GetCheckinDate(systemDate);


                foreach (var guestReservationItem in listGuestReservationItem)
                {
                    if (!guestReservationItem.GuestRegistrationId.HasValue)
                    {
                        Notification.Raise(Notification
                                  .DefaultBuilder
                                  .WithMessage(AppConsts.LocalizationSourceName, GuestReservationItem.EntityError.GuestNotSave)
                                  .WithMessageFormat(guestReservationItem.GuestName)
                                  .Build());

                        err = true;
                    }
                }

                var roomOfReservationItem = _roomRepository.GetByGuestReservationItemId(dto.FirstOrDefault().GuestReservationItemId);

                if (roomOfReservationItem == null)
                {
                    Notification.Raise(Notification
                                 .DefaultBuilder
                                 .WithMessage(AppConsts.LocalizationSourceName, GuestReservationItem.EntityError.GuestReservatiomItemWithoutRoom)
                                 .Build());
                    err = true;
                }
                //Verificar se existe conta para os hóspedes da acomodação,
                //caso não exista,
                //o sistema deve criar de forma automática uma conta para cada hóspede da acomodação.
                if (err) return;
                else
                {
                    if (!roomIsUsed)
                    {
                        try
                        {
                            _roomOccupiedDomainService.Insert(RoomOccupiedAdapter.Map(reservationItem.RoomId.Value, reservationId).Build());
                        }
                        catch (Exception ex)
                        {
                            Notification.Raise(Notification.DefaultBuilder.WithMessage(AppConsts.LocalizationSourceName, ReservationItemEnum.Error.RoomIsNotAvaiable)
                                                                          .WithDetailedMessage(ex.Message)
                                                                          .Build());
                            return;
                        }
                    }


                    var billingAccounts = _checkinAppService.GetAllForGuestReservationItemId(listGuestReservationItem.Select(x => x.Id).ToList(), reservationId);
                    var firstGuestReservationItem = listGuestReservationItem.FirstOrDefault(e => !e.IsChild);
                    var reservation = _checkinAppService.GetReservationForChekin(reservationId);

                    VerifyAndCreateCompanyOrGroupBillingAccount(reservationId, propertyId, firstGuestReservationItem, reservation);

                    if (Notification.HasNotification())
                        return;

                    _checkinDomainService.UpdateToCheckInGuestReservationItem(listGuestReservationItem, vCheckInDate);                    

                    var guestReservationItemIds = listGuestReservationItem.Select(g => (long?)g.Id).ToList();

                   var existingBillingAccounts = _billingAccountReadRepository.GetAllExistingBillingAccountsByGuestReservationItemIds(guestReservationItemIds);

                    _checkinDomainService.CreateGuestAccounts(listGuestReservationItem, reservationId, propertyId, reservation.BusinessSourceId.Value, reservation.MarketSegmentId.Value, existingBillingAccounts);

                    //Mudar status da reservar para Check-in
                    if (firstGuestReservationItem != null && firstGuestReservationItem.ReservationItem.ReservationItemStatusId < (int)ReservationStatus.Checkin)
                        _reservationItemAppservice.UpdateToCheckInReservationItem(firstGuestReservationItem.ReservationItemId, propertyId, vCheckInDate);


                    if (Notification.HasNotification())
                        return;
                }

                uow.Complete();
            }
        }


        private DateTime GetCheckinDate(DateTime checkinDate)
        {
            var currentDate = DateTime.UtcNow.ToZonedDateTimeLoggedUser();

            var newCheckinDate = new DateTime(
                                checkinDate.Year,
                                checkinDate.Month,
                                checkinDate.Day,
                                currentDate.Hour,
                                currentDate.Minute,
                                currentDate.Second);
            return newCheckinDate;
        }       

        public void VerifyAndCreateCompanyOrGroupBillingAccount(long reservationId, int propertyId, GuestReservationItem firstGuestReservationItem, Reservation reservation)
        {
            //Verificar se a conta em grupo já não foi criada
            if (reservation != null && reservation.UnifyAccounts && !_checkinAppService.AnyGroupAccount(reservationId))
            {
                //ou conta em grupo para company
                if (reservation.CompanyClientId.HasValue)
                    _checkinAppService.CreateBillingAccountForGroup(reservationId, reservation.CompanyClientId, null, propertyId, reservation.BusinessSourceId.Value, reservation.MarketSegmentId.Value, reservation.GroupName);
                //ou conta em grupo pro primeiro guest
                else
                    _checkinAppService.CreateBillingAccountForGroup(reservationId, null, firstGuestReservationItem, propertyId, reservation.BusinessSourceId.Value, reservation.MarketSegmentId.Value, reservation.GroupName);
            }

            //Cria uma conta empresa para a UH
            if (reservation.CompanyClientId.HasValue && !_checkinAppService.AnyCompanyAccountAccomodation(firstGuestReservationItem.ReservationItemId))
                _checkinAppService.CreateBillingAccountForCompany(firstGuestReservationItem.ReservationItemId, reservation, propertyId);
        }

        public IListDto<GuestReservationItemDto> guestReservationItensByRoomId(long roomId)
        {
            return _guestReservationItemReadRepository.GetGuestReservationItensByRoomId(roomId);
        }


        public void Checkout(int propertyId, long id)
        {
            using (var uow = _unitOfWorkManager.Begin())
            {
                if (id < 0)
                    NotifyIdIsMissing();

                if (!ValidateId(id)) return;

                if (Notification.HasNotification())
                    return;

                var checkOutDate = _propertyParameterReadRepository.GetSystemDate(propertyId).Value;

                _guestReservationItemRepository.UpdateGuestToCheckout(id, checkOutDate);

                uow.Complete();
            }

        }

        public IList<GuestReservationItem> GetGuestReservationItemByReservationItemId(long reservationItemId)
        {
            return _reservationItemReadRepository.GetGuestReservationItemByReservationItemId(reservationItemId);
        }

        public void UpdateGuestEstimatedDepartureDate(long guestReservationItemId, DateTime estimatedDepartureDate)
        {
            _guestReservationItemRepository.UpdateGuestEstimatedDepartureDate(guestReservationItemId, estimatedDepartureDate);
        }

        public void UpdateGuestEstimatedArrivalAndDepartureDate(long guestReservationItemId, DateTime estimatedArrivalDate, DateTime estimatedDepartureDate)
        {
            _guestReservationItemRepository.UpdateGuestEstimatedArrivalAndDepartureDate(guestReservationItemId, estimatedArrivalDate, estimatedDepartureDate);
        }


        public void ConfirmEntranceCheckin(GuestReservationItem guestReservationItem, long reservationId, int propertyId, DateTime? systemDate)
        {
            using (var uow = _unitOfWorkManager.Begin())
            {
                Validar(guestReservationItem);

                if (Notification.HasNotification())
                    return;

                var vCheckInDate = systemDate ?? _propertyParameterReadRepository.GetSystemDate(propertyId).Value;

                var reservation = _checkinAppService.GetBusinessSourceAndMarketSegmentByReservationId(reservationId);

                _checkinAppService.UpdateToCheckInGuestReservationItem(guestReservationItem, vCheckInDate);

                _checkinAppService.CreateAccountGuest(guestReservationItem, reservationId, propertyId, reservation.BusinessSourceId ?? 0, reservation.MarketSegmentId ?? 0);

                uow.Complete();
            }
        }

        public void Validar(GuestReservationItem guestReservationItem)
        {
            if (!guestReservationItem.GuestRegistrationId.HasValue)
            {
                Notification.Raise(Notification
                          .DefaultBuilder
                          .WithMessage(AppConsts.LocalizationSourceName, GuestReservationItem.EntityError.GuestNotSave)
                          .WithMessageFormat(guestReservationItem.GuestName)
                          .Build());
            }
        }


        public virtual async Task<GuestReservationItem> CreateByEntrance(GuestRegistrationDto guestRegistrationDto, ReservationItemDto reservationItem, GuestReservationItemEntranceDto dto)
        {
            ValidateDto<GuestRegistrationDto>(guestRegistrationDto, nameof(guestRegistrationDto));

            if (Notification.HasNotification())
                return null;

            ValidateDto<ReservationItemDto>(reservationItem, nameof(reservationItem));

            if (Notification.HasNotification())
                return null;

            dto.SystemDate = dto.SystemDate ?? _propertyParameterReadRepository.GetSystemDate(_applicationUser.PropertyId.TryParseToInt32());
            ValidateSystemDateNull(dto.SystemDate);

            if (Notification.HasNotification())
                return null;

            var guestReservationItemBuilder = GuestReservationItemAdapter.Map(guestRegistrationDto, reservationItem, GetNationality(guestRegistrationDto), dto);

            return await GuestReservationItemDomainService.InsertAndSaveChangesAsync(guestReservationItemBuilder);

        }

        private void ValidateSystemDateNull(DateTime? systemDate)
        {
            if (systemDate == null)
            {
                Notification.Raise(Notification.DefaultBuilder
                .WithMessage(AppConsts.LocalizationSourceName, PropertyAuditProcess.EntityError.PropertyAuditProcessInvalidPropertySystemDate)
                .WithDetailedMessage(AppConsts.LocalizationSourceName, PropertyAuditProcess.EntityError.PropertyAuditProcessInvalidPropertySystemDate)
                .Build());
            }
        }

        private CountrySubdivisionTranslationDto GetNationality(GuestRegistrationDto dto)
        {
            return _countrySubdivisionTranslationAppService.GetNationality(dto.Nationality ?? 0);
        }

        public void UpdateGuestEstimatedDepartureDate(List<GuestReservationItem> guestReservationItemList, DateTime systemDate)
        {
            _guestReservationItemRepository.UpdateGuestEstimatedDepartureDate(guestReservationItemList, systemDate);
        }

        public IList<GuestReservationItem> GetGuestReservationItemByReservationItemId(List<long> reservationItemIdList)
        {
            return _reservationItemReadRepository.GetGuestReservationItemByReservationItemIdList(reservationItemIdList);
        }
    }
}
