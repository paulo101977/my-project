﻿using System;
using Thex.Application.Adapters;
using Thex.Domain.Entities;
using Thex.Application.Interfaces;
using Tnf.Domain.Services;
using System.Collections.Generic;
using Thex.Dto;

using Thex.Domain.Interfaces.Repositories;
using Tnf.Notifications;

namespace Thex.Application.Services
{
    public class RatePlanRoomTypeAppService : ScaffoldRatePlanRoomTypeAppService, IRatePlanRoomTypeAppService
    {
        private readonly IRatePlanRoomTypeRepository _ratePlanRoomTypeRepository;
        public RatePlanRoomTypeAppService(
            IRatePlanRoomTypeAdapter ratePlanRoomTypeAdapter,
            IDomainService<RatePlanRoomType> ratePlanRoomTypeDomainService,
            IRatePlanRoomTypeRepository ratePlanRoomTypeRepository,
            INotificationHandler notificationHandler)
            : base(ratePlanRoomTypeAdapter, ratePlanRoomTypeDomainService, notificationHandler)
        {
            _ratePlanRoomTypeRepository = ratePlanRoomTypeRepository;
        }

        public void RatePlanRoomTypeCreate(List<RatePlanRoomTypeDto> ratePlanRoomTypes)
        {
            if (ratePlanRoomTypes != null && ratePlanRoomTypes.Count > 0)
            {
                var ratePlanRoomTypeList = new List<RatePlanRoomType>();
                foreach (var ratePlanRoomType in ratePlanRoomTypes)
                {
                    ValidateDto<RatePlanRoomTypeDto>(ratePlanRoomType, nameof(ratePlanRoomType));

                    if (Notification.HasNotification()) break;

                    var ratePlanRoomTypeBuilder = RatePlanRoomTypeAdapter.Map(ratePlanRoomType);

                    if (Notification.HasNotification()) break;

                    ratePlanRoomTypeList.Add(ratePlanRoomTypeBuilder.Build());

                    if (Notification.HasNotification()) break;


                }

                if (Notification.HasNotification()) return;

                _ratePlanRoomTypeRepository.AddRange(ratePlanRoomTypeList);

            }
        }
    }
}
