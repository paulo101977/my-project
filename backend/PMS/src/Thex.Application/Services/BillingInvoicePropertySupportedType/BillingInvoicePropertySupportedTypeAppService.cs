﻿using System;
using Thex.Application.Adapters;
using Thex.Domain.Entities;
using Thex.Application.Interfaces;
using Tnf.Domain.Services;
using Thex.Dto;
using System.Collections.Generic;
using Tnf.Notifications;
using Thex.Kernel;
using Thex.Common.Helpers;
using Thex.Common.Enumerations;
using Thex.Common;

namespace Thex.Application.Services
{
    public class BillingInvoicePropertySupportedTypeAppService : ScaffoldBillingInvoicePropertySupportedTypeAppService, IBillingInvoicePropertySupportedTypeAppService
    {
        private readonly IApplicationUser _applicationUser;
        private readonly IBillingInvoicePropertySupportedTypeBrazilAppService _billingInvoicePropertySupportedTypeBrazilAppService;
        private readonly IBillingInvoicePropertySupportedTypeEuropeAppService _billingInvoicePropertySupportedTypeEuropeAppService;
        private readonly INotificationHandler _notificationHandler;

        public BillingInvoicePropertySupportedTypeAppService(
            IApplicationUser applicationUser,
            IBillingInvoicePropertySupportedTypeAdapter billingInvoicePropertySupportedTypeAdapter,
            IDomainService<BillingInvoicePropertySupportedType> billingInvoicePropertySupportedTypeDomainService,
            INotificationHandler notificationHandler,
            IBillingInvoicePropertySupportedTypeBrazilAppService billingInvoicePropertySupportedTypeBrazilAppService,
            IBillingInvoicePropertySupportedTypeEuropeAppService billingInvoicePropertySupportedTypeEuropeAppService)
            : base(billingInvoicePropertySupportedTypeAdapter, billingInvoicePropertySupportedTypeDomainService, notificationHandler)
        {
            _applicationUser = applicationUser;
            _billingInvoicePropertySupportedTypeBrazilAppService = billingInvoicePropertySupportedTypeBrazilAppService;
            _billingInvoicePropertySupportedTypeEuropeAppService = billingInvoicePropertySupportedTypeEuropeAppService;
            _notificationHandler = notificationHandler;
        }

        public void BillingInvoicePropertySupportedTypeCreate(List<BillingInvoicePropertySupportedTypeDto> BillingInvoicePropertySupportedTypes, Guid billingInvoicePropertyId, Guid childBillingInvoicePropertyId = new Guid())
        {
            switch (EnumHelper.GetEnumValue<CountryIsoCodeEnum>(_applicationUser.PropertyCountryCode, true))
            {
                case CountryIsoCodeEnum.Brasil:
                    {
                        _billingInvoicePropertySupportedTypeBrazilAppService.BillingInvoicePropertySupportedTypeCreate(BillingInvoicePropertySupportedTypes, billingInvoicePropertyId, int.Parse(_applicationUser.PropertyId));
                        break;
                    }
                case CountryIsoCodeEnum.Europe:
                    {
                        _billingInvoicePropertySupportedTypeEuropeAppService.CreateRange(BillingInvoicePropertySupportedTypes, billingInvoicePropertyId, childBillingInvoicePropertyId);
                        break;
                    }
                case CountryIsoCodeEnum.Latam:
                default:
                    {
                        // TODO: Criar a mensagem de erro corretamente
                        _notificationHandler.Raise(_notificationHandler.DefaultBuilder
                                                                 .WithMessage(AppConsts.LocalizationSourceName, BillingInvoiceEnum.Error.BillingInvoiceInvalidAccountClosed)
                                                                 .WithDetailedMessage(string.Concat(_applicationUser.PropertyCountryCode, AppConsts.LocalizationSourceName, BillingInvoiceEnum.Error.BillingInvoiceInvalidAccountClosed))
                                                                 .Build());

                        break;
                    }
            }
        }

    }
}
