﻿using System;
using System.Collections.Generic;
using System.Linq;
using Thex.Application.Adapters;
using Thex.Application.Interfaces;
using Thex.Common.Enumerations;
using Thex.Domain.Entities;
using Thex.Domain.Interfaces.Repositories;
using Thex.Dto;
using Thex.Infra.ReadInterfaces;
using Thex.Kernel;
using Tnf.Domain.Services;
using Tnf.Notifications;

namespace Thex.Application.Services
{
    public class BillingInvoicePropertySupportedTypeEuropeAppService : ScaffoldBillingInvoicePropertySupportedTypeAppService, IBillingInvoicePropertySupportedTypeEuropeAppService
    {
        private readonly IBillingItemReadRepository _billingItemReadRepository;
        private readonly ICountrySubdvisionServiceReadRepository _countrySubdvisionServiceReadRepoitory;
        private readonly IBillingInvoicePropertySupportedTypeRepository _billingInvoicePropertySupportedTypeRepository;
        private readonly IApplicationUser _applicationUser;

        public BillingInvoicePropertySupportedTypeEuropeAppService(
            IBillingInvoicePropertySupportedTypeAdapter billingInvoicePropertySupportedTypeAdapter,
            IDomainService<BillingInvoicePropertySupportedType> billingInvoicePropertySupportedTypeDomainService,
            IBillingItemReadRepository billingItemReadRepository,
            ICountrySubdvisionServiceReadRepository countrySubdvisionServiceReadRepoitory,
            INotificationHandler notificationHandler,
            IBillingInvoicePropertySupportedTypeRepository billingInvoicePropertySupportedTypeRepository,
            IApplicationUser applicationUser)
            : base(billingInvoicePropertySupportedTypeAdapter, billingInvoicePropertySupportedTypeDomainService, notificationHandler)
        {
            _billingItemReadRepository = billingItemReadRepository;
            _countrySubdvisionServiceReadRepoitory = countrySubdvisionServiceReadRepoitory;
            _billingInvoicePropertySupportedTypeRepository = billingInvoicePropertySupportedTypeRepository;
            _applicationUser = applicationUser;
        }

        public void CreateRange(List<BillingInvoicePropertySupportedTypeDto> BillingInvoicePropertySupportedTypes, Guid billingInvoicePropertyId, Guid childBillingInvoicePropertyId)
        {
            if (BillingInvoicePropertySupportedTypes != null && BillingInvoicePropertySupportedTypes.Count > 0)
            {
                var billingInvoicePropertySupportedTypeList = new List<BillingInvoicePropertySupportedType>();

                var billingItemsForDocuments = _billingItemReadRepository.GetAllServiceWithParameters(int.Parse(_applicationUser.PropertyId)).Items;

                foreach (var billingInvoicePropertySupportedTypeDto in BillingInvoicePropertySupportedTypes)
                {
                    billingInvoicePropertySupportedTypeDto.BillingInvoicePropertyId = billingInvoicePropertyId;
                    ValidateDto<BillingInvoicePropertySupportedTypeDto>(billingInvoicePropertySupportedTypeDto, nameof(BillingInvoicePropertySupportedType));

                    if (!billingItemsForDocuments.Where(x => x.BillingItemId == billingInvoicePropertySupportedTypeDto.BillingItemId).Any())
                        NotifyParameterInvalid();

                    if (Notification.HasNotification()) break;

                    var billingInvoicePropertySupportedTypeBuilder = BillingInvoicePropertySupportedTypeAdapter.Map(billingInvoicePropertySupportedTypeDto);

                    billingInvoicePropertySupportedTypeList.Add(billingInvoicePropertySupportedTypeBuilder.Build());

                    var childBillingInvoicePropertySupportedTypeDto = new BillingInvoicePropertySupportedTypeDto
                    {
                        Id = Guid.NewGuid(),
                        BillingItemTypeId = billingInvoicePropertySupportedTypeDto.BillingItemTypeId,
                        BillingItemId = billingInvoicePropertySupportedTypeDto.BillingItemId,
                        BillingInvoicePropertyId = childBillingInvoicePropertyId
                    };

                    var childBillingInvoicePropertySupportedTypeBuilder = BillingInvoicePropertySupportedTypeAdapter.Map(childBillingInvoicePropertySupportedTypeDto);
                    billingInvoicePropertySupportedTypeList.Add(childBillingInvoicePropertySupportedTypeBuilder.Build());

                    if (Notification.HasNotification()) break;
                }

                _billingInvoicePropertySupportedTypeRepository.CreateRange(billingInvoicePropertySupportedTypeList);
            }
        }
    }
}
