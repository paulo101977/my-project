﻿using System;
using System.Collections.Generic;
using System.Linq;
using Thex.Application.Adapters;
using Thex.Application.Interfaces;
using Thex.Common.Enumerations;
using Thex.Domain.Entities;
using Thex.Dto;
using Thex.Infra.ReadInterfaces;
using Tnf.Domain.Services;
using Tnf.Notifications;

namespace Thex.Application.Services
{
    public class BillingInvoicePropertySupportedTypeBrazilAppService : ScaffoldBillingInvoicePropertySupportedTypeAppService, IBillingInvoicePropertySupportedTypeBrazilAppService
    {
        private readonly IBillingItemReadRepository _billingItemReadRepository;
        private readonly ICountrySubdvisionServiceReadRepository _countrySubdvisionServiceReadRepoitory;

        public BillingInvoicePropertySupportedTypeBrazilAppService(
            IBillingInvoicePropertySupportedTypeAdapter billingInvoicePropertySupportedTypeAdapter,
            IDomainService<BillingInvoicePropertySupportedType> billingInvoicePropertySupportedTypeDomainService,
            IBillingItemReadRepository billingItemReadRepository,
            ICountrySubdvisionServiceReadRepository countrySubdvisionServiceReadRepoitory,
            INotificationHandler notificationHandler)
            : base(billingInvoicePropertySupportedTypeAdapter, billingInvoicePropertySupportedTypeDomainService, notificationHandler)
        {
            _billingItemReadRepository = billingItemReadRepository;
            _countrySubdvisionServiceReadRepoitory = countrySubdvisionServiceReadRepoitory;
        }

        public void BillingInvoicePropertySupportedTypeCreate(List<BillingInvoicePropertySupportedTypeDto> BillingInvoicePropertySupportedTypes, Guid billingInvoicePropertyId, int propertyId)
        {
            if (BillingInvoicePropertySupportedTypes != null && BillingInvoicePropertySupportedTypes.Count > 0)
            {
                var BillingInvoicePropertySupportedTypeList = new List<BillingInvoicePropertySupportedType>();

                var billingItemsForDocuments = _billingItemReadRepository.GetAllServiceWithParameters(propertyId).Items;
                var billingItemsInformationsServices = billingItemsForDocuments.Where(x => x.BillingItemTypeId == (int)BillingItemTypeEnum.Service).ToList();
                var billingItemsInformationsPdv = billingItemsForDocuments.Where(x => x.BillingItemTypeId == (int)BillingItemTypeEnum.PointOfSale).ToList();

                var countrySubdvionServiceInformations = _countrySubdvisionServiceReadRepoitory.GetAllByPropertyId(propertyId).Items;


                foreach (var billingInvoicePropertySupportedType in BillingInvoicePropertySupportedTypes)
                {
                    billingInvoicePropertySupportedType.BillingInvoicePropertyId = billingInvoicePropertyId;
                    ValidateDto<BillingInvoicePropertySupportedTypeDto>(billingInvoicePropertySupportedType, nameof(BillingInvoicePropertySupportedType));

                    if (!billingItemsForDocuments.Where(x => x.BillingItemId == billingInvoicePropertySupportedType.BillingItemId).Any())
                        NotifyParameterInvalid();

                    //Verificar se billingItemsInformationsPdv billingItemsInformationsServices
                    if (billingItemsInformationsPdv.Any(e => e.BillingItemId == billingInvoicePropertySupportedType.BillingItemId) && billingInvoicePropertySupportedType.CountrySubdvisionServiceId.HasValue)
                        NotifyParameterInvalid();

                    if (billingItemsInformationsServices.Any(e => e.BillingItemId == billingInvoicePropertySupportedType.BillingItemId) && !billingInvoicePropertySupportedType.CountrySubdvisionServiceId.HasValue)
                        NotifyParameterInvalid();

                    if (Notification.HasNotification()) break;

                    if (billingInvoicePropertySupportedType.CountrySubdvisionServiceId != null && !countrySubdvionServiceInformations.Where(x => x.Id == billingInvoicePropertySupportedType.CountrySubdvisionServiceId).Any())
                        NotifyParameterInvalid();

                    var BillingInvoicePropertySupportedTypeBuilder = BillingInvoicePropertySupportedTypeAdapter.Map(billingInvoicePropertySupportedType);

                    BillingInvoicePropertySupportedTypeDomainService.InsertAndSaveChanges(BillingInvoicePropertySupportedTypeBuilder);

                    if (Notification.HasNotification()) break;

                }
            }
        }
    }
}
