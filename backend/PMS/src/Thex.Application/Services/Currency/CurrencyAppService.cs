﻿using System;
using Thex.Application.Adapters;
using Thex.Domain.Entities;
using Thex.Application.Interfaces;
using Tnf.Domain.Services;
using System.Threading.Tasks;
using Thex.Dto;
using Tnf.Dto;
using Thex.Infra.ReadInterfaces;
using Tnf.Notifications;

namespace Thex.Application.Services
{
    public class CurrencyAppService : ScaffoldCurrencyAppService, ICurrencyAppService
    {
        private readonly ICurrencyReadRepository _currencyReadRepository;

        public CurrencyAppService(
            ICurrencyAdapter currencyAdapter,
            IDomainService<Currency> currencyDomainService,
            ICurrencyReadRepository currencyReadRepository,
            INotificationHandler notificationHandler)
            : base(currencyAdapter, currencyDomainService, notificationHandler)
        {
            _currencyReadRepository = currencyReadRepository;
        }

        public new async Task<IListDto<CurrencyDto>> GetAll(GetAllCurrencyDto request = null)
        {
            return await _currencyReadRepository.GetAllCurrencyAsync(request);
        }
    }
}
