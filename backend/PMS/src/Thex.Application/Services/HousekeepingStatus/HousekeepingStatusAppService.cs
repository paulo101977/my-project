﻿using System;
using Thex.Application.Adapters;
using Thex.Domain.Entities;
using Thex.Application.Interfaces;
using Tnf.Domain.Services;
using Tnf.Notifications;

namespace Thex.Application.Services
{
    public class HousekeepingStatusAppService : ScaffoldHousekeepingStatusAppService, IHousekeepingStatusAppService
    {
        public HousekeepingStatusAppService(
            IHousekeepingStatusAdapter housekeepingStatusAdapter,
            IDomainService<HousekeepingStatus> housekeepingStatusDomainService,
            INotificationHandler notificationHandler)
            : base(housekeepingStatusAdapter, housekeepingStatusDomainService, notificationHandler)
        {
        }
    }
}
