﻿using System;
using Thex.Application.Adapters;
using Thex.Domain.Entities;
using Thex.Application.Interfaces;
using Tnf.Domain.Services;
using Tnf.Notifications;

namespace Thex.Application.Services
{
    public class BedTypeAppService : ScaffoldBedTypeAppService, IBedTypeAppService
    {
        public BedTypeAppService(
            IBedTypeAdapter bedTypeAdapter,
            IDomainService<BedType> bedTypeDomainService,
            INotificationHandler notificationHandler)
            : base(bedTypeAdapter, bedTypeDomainService, notificationHandler)
        {
        }
    }
}
