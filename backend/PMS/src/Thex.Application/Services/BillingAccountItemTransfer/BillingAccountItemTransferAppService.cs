﻿using System;
using Thex.Application.Adapters;
using Thex.Domain.Entities;
using Thex.Application.Interfaces;
using Tnf.Domain.Services;
using Thex.Dto;
using System.Collections.Generic;
using Thex.EntityFrameworkCore.Repositories;
using Tnf.Notifications;

namespace Thex.Application.Services
{
    public class BillingAccountItemTransferAppService : ScaffoldBillingAccountItemTransferAppService, IBillingAccountItemTransferAppService
    {
        private readonly BillingAccountItemTransferRepository _billingAccountItemTransferRepository;

        public BillingAccountItemTransferAppService(
            IBillingAccountItemTransferAdapter billingAccountItemTransferAdapter,
            IDomainService<BillingAccountItemTransfer> billingAccountItemTransferDomainService,
            BillingAccountItemTransferRepository billingAccountItemTransferRepository,
            INotificationHandler notificationHandler)
            : base(billingAccountItemTransferAdapter, billingAccountItemTransferDomainService, notificationHandler)
        {
            _billingAccountItemTransferRepository = billingAccountItemTransferRepository;
        }

        public virtual void CreateRange(List<BillingAccountItemTransferDto> transferListDto)
        {
            var transferList = new List<BillingAccountItemTransfer>();

            foreach (var transferItem in transferListDto)
            {
                ValidateDto<BillingAccountItemTransferDto>(transferItem, nameof(transferItem));

                var billingAccountItemTransferBuilder = BillingAccountItemTransferAdapter.Map(transferItem);

                transferList.Add(billingAccountItemTransferBuilder.Build());
            }

            if (Notification.HasNotification())
                return;

            _billingAccountItemTransferRepository.AddRange(transferList);
        }
    }
}
