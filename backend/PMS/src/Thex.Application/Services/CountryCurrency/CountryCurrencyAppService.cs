﻿using System;
using Thex.Application.Adapters;
using Thex.Domain.Entities;
using Thex.Application.Interfaces;
using Tnf.Domain.Services;
using Tnf.Notifications;

namespace Thex.Application.Services
{
    public class CountryCurrencyAppService : ScaffoldCountryCurrencyAppService, ICountryCurrencyAppService
    {
        public CountryCurrencyAppService(
            ICountryCurrencyAdapter countryCurrencyAdapter,
            IDomainService<CountryCurrency> countryCurrencyDomainService,
            INotificationHandler notificationHandler)
            : base(countryCurrencyAdapter, countryCurrencyDomainService, notificationHandler)
        {
        }
    }
}
