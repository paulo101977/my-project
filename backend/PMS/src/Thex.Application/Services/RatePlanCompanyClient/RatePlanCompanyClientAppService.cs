﻿using System;
using Thex.Application.Adapters;
using Thex.Domain.Entities;
using Thex.Application.Interfaces;
using Tnf.Domain.Services;
using System.Collections.Generic;
using Thex.Dto;
using Thex.Domain.Interfaces.Repositories;
using Tnf.Notifications;

namespace Thex.Application.Services
{
    public class RatePlanCompanyClientAppService : ScaffoldRatePlanCompanyClientAppService, IRatePlanCompanyClientAppService
    {
        private readonly IRatePlanCompanyClientRepository _ratePlanCompanyClientRepository;

        public RatePlanCompanyClientAppService(
            IRatePlanCompanyClientAdapter ratePlanCompanyClientAdapter,
            IDomainService<RatePlanCompanyClient> ratePlanCompanyClientDomainService,
           IRatePlanCompanyClientRepository ratePlanCompanyClientRepository,
            INotificationHandler notificationHandler)
            : base(ratePlanCompanyClientAdapter, ratePlanCompanyClientDomainService, notificationHandler)
        {
            _ratePlanCompanyClientRepository = ratePlanCompanyClientRepository;
        }

        public void RatePlanCompanyClientCreate(List<RatePlanCompanyClientDto> ratePlanCompanyClients)
        {
            if (ratePlanCompanyClients != null && ratePlanCompanyClients.Count > 0)
            {
                var ratePlanCompanyClientList = new List<RatePlanCompanyClient>();
                foreach (var ratePlanCompanyClient in ratePlanCompanyClients)
                {
                    ValidateDto<RatePlanCompanyClientDto>(ratePlanCompanyClient, nameof(ratePlanCompanyClient));

                    if (Notification.HasNotification()) break;

                    var ratePlanCompanyClientBuilder = RatePlanCompanyClientAdapter.Map(ratePlanCompanyClient);

                    if (Notification.HasNotification()) break;

                    ratePlanCompanyClientList.Add(ratePlanCompanyClientBuilder.Build());

                    if (Notification.HasNotification()) break;
                }
                if (Notification.HasNotification()) return;

                _ratePlanCompanyClientRepository.RatePlanCompanyClientCreate(ratePlanCompanyClientList);

            }
        }
    }
}
