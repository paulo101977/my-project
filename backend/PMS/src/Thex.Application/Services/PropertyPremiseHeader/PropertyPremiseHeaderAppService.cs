﻿using System;
using Thex.Application.Adapters;
using Thex.Application.Interfaces;
using Thex.Common;
using Thex.Domain.Entities;
using Thex.Domain.Interfaces.Repositories;
using Thex.Dto;
using Thex.Infra.ReadInterfaces;
using Thex.Kernel;
using Tnf.Domain.Services;
using Tnf.Dto;
using Tnf.Notifications;
using Tnf.Repositories.Uow;

namespace Thex.Application.Services
{
    public class PropertyPremiseHeaderAppService : ScaffoldPropertyPremiseHeaderAppService, IPropertyPremiseHeaderAppService
    {
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        public readonly IApplicationUser _applicationUser;
        private readonly IPropertyPremiseHeaderReadRepository _premiseHaderReadRepository;
        private readonly IPropertyPremiseHeaderRepository _premiseHaderRepository;

        public PropertyPremiseHeaderAppService(
            IUnitOfWorkManager unitOfWorkManager,
            IApplicationUser applicationUser,
            IPropertyPremiseHeaderReadRepository premiseHeaderReadRepository,
            IPropertyPremiseHeaderRepository premiseHeaderRepository,
            IPropertyPremiseHeaderRepository propertyPremiseHeaderRepository,
            IPropertyPremiseHeaderAdapter propertyPremiseHeaderAdapter,
            IDomainService<PropertyPremiseHeader> propertyPremiseHeaderDomainService,
            IPropertyPremiseHeaderReadRepository propertyPremiseHeaderReadRepository,
            IPropertyPremiseAdapter propertyPremiseAdapter,
            INotificationHandler notificationHandler)
                : base(propertyPremiseHeaderAdapter, propertyPremiseHeaderDomainService, propertyPremiseHeaderRepository, propertyPremiseHeaderReadRepository, propertyPremiseAdapter, unitOfWorkManager, notificationHandler)
        {
            _unitOfWorkManager = unitOfWorkManager;
            _applicationUser = applicationUser;
            _premiseHaderReadRepository = premiseHeaderReadRepository;
            _premiseHaderRepository = premiseHeaderRepository;
        }

        public virtual PropertyPremiseHeaderListDto GetById(Guid id)
        {
            var result = _premiseHaderReadRepository.GetDtoById(id);

            if (Notification.HasNotification())
                return null;

            return result;
        }

        public virtual IListDto<PropertyPremiseHeaderListDto> GetAllHeader(GetAllPropertyPremiseHeaderDto dto)
        {
            ValidateDates(dto);

            if (Notification.HasNotification()) return null;

            if ((dto.StartDate.HasValue && !dto.FinalDate.HasValue) || (!dto.StartDate.HasValue && dto.FinalDate.HasValue))
                return new ListDto<PropertyPremiseHeaderListDto>();

            var result = _premiseHaderReadRepository.GetAll(dto);

            if (Notification.HasNotification())
                return null;

            return result;
        }

        private void ValidateDates(GetAllPropertyPremiseHeaderDto dto)
        {
            if (dto.StartDate.HasValue && dto.FinalDate.HasValue && dto.StartDate.Value.Date > dto.FinalDate.Value.Date)
            {
                Notification.Raise(Notification.DefaultBuilder
                 .WithMessage(AppConsts.LocalizationSourceName, PropertyPremiseHeader.EntityError.EndDateCanNotBeLessThanInitialDate)
                 .WithDetailedMessage(AppConsts.LocalizationSourceName, PropertyPremiseHeader.EntityError.EndDateCanNotBeLessThanInitialDate)
                 .Build());
            }
        }

        public new virtual PropertyPremiseHeader Create(PropertyPremiseHeaderDto dto)
        {
            PropertyPremiseHeader result = null;
            ValidateDto<PropertyPremiseHeaderDto>(dto, nameof(dto));

            if (Notification.HasNotification())
                return null;

            dto.Id = Guid.NewGuid();
            dto.PropertyId = int.Parse(_applicationUser.PropertyId);
            dto.TenantId = _applicationUser.TenantId;
            dto.IsActive = true;

            if (_premiseHaderReadRepository.CheckPremiseHeaderConstraints(dto))
            {
                Notification.Raise(Notification.DefaultBuilder
                 .WithMessage(AppConsts.LocalizationSourceName, PropertyPremiseHeader.EntityError.PropertyPremiseHeaderConstraintError)
                 .WithDetailedMessage(AppConsts.LocalizationSourceName, PropertyPremiseHeader.EntityError.PropertyPremiseHeaderConstraintError)
                 .Build());

                if (Notification.HasNotification())
                    return null;
            }

            var propertyPremiseHeader = PropertyPremiseHeaderAdapter.MapWithPremise(dto);

            result = PropertyPremiseHeaderDomainService.InsertAndSaveChangesAsync(propertyPremiseHeader).Result;

            if (Notification.HasNotification())
                return null;

            return result;
        }

        public void ToggleActivation(Guid id)
        {
            using (var uow = _unitOfWorkManager.Begin())
            {
                _premiseHaderRepository.ToggleActive(id);

                uow.Complete();
            }
        }
    }
}
