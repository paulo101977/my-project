﻿using System;
using Thex.Application.Adapters;
using Thex.Domain.Entities;
using Thex.Application.Interfaces;
using Tnf.Domain.Services;
using Tnf.Notifications;

namespace Thex.Application.Services
{
    public class MealPlanTypeAppService : ScaffoldMealPlanTypeAppService, IMealPlanTypeAppService
    {
        public MealPlanTypeAppService(
            IMealPlanTypeAdapter mealPlanTypeAdapter,
            IDomainService<MealPlanType> mealPlanTypeDomainService,
            INotificationHandler notificationHandler)
            : base(mealPlanTypeAdapter, mealPlanTypeDomainService, notificationHandler)
        {
        }
    }
}
