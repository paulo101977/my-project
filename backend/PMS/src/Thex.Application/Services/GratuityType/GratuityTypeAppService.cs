﻿using System;
using Thex.Application.Adapters;
using Thex.Domain.Entities;
using Thex.Application.Interfaces;
using Tnf.Domain.Services;
using Tnf.Notifications;

namespace Thex.Application.Services
{
    public class GratuityTypeAppService : ScaffoldGratuityTypeAppService, IGratuityTypeAppService
    {
        public GratuityTypeAppService(
            IGratuityTypeAdapter gratuityTypeAdapter,
            IDomainService<GratuityType> gratuityTypeDomainService,
            INotificationHandler notificationHandler)
            : base(gratuityTypeAdapter, gratuityTypeDomainService, notificationHandler)
        {
        }
    }
}
