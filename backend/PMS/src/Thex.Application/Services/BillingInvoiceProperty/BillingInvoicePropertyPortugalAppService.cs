﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Thex.Application.Adapters;
using Thex.Application.Interfaces;
using Thex.Common;
using Thex.Common.Enumerations;
using Thex.Domain.Entities;
using Thex.Domain.Interfaces.Integration;
using Thex.Domain.Interfaces.Repositories;
using Thex.Dto;
using Thex.Infra.ReadInterfaces;
using Thex.Infra.Repositories.Read;
using Thex.Kernel;
using Tnf.Domain.Services;
using Tnf.Notifications;
using Tnf.Repositories.Uow;

namespace Thex.Application.Services
{
    public class BillingInvoicePropertyPortugalAppService : ScaffoldBillingInvoicePropertyAppService, IBillingInvoicePropertyPortugalAppService
    {
        private readonly IBillingInvoicePropertyReadRepository _billingInvoicePropertyReadRepository;
        private readonly IBillingInvoicePropertyRepository _billingInvoicePropertyRepository;
        private readonly IBillingInvoicePropertySupportedTypeAppService _billingInvoicePropertySupportedTypeAppService;
        private readonly IBillingInvoicePropertySupportedTypeRepository _billingInvoicePropertySupportedTypeRepository;
        private readonly IBillingInvoicePropertySupportedTypeReadRepository _billingInvoicePropertySupportedTypeReadRepository;
        private readonly IBillingItemReadRepository _billingItemReadRepository;
        private readonly ICountrySubdvisionServiceReadRepository _countrySubdvisionServiceReadRepoitory;
        private readonly IInvoiceIntegrationEurope _nFEIntegration;
        private readonly IReasonReadRepository _reasonReadRepository;
        private readonly IBillingInvoiceRepository _billingInvoiceRepository;
        private readonly PropertyParameterReadRepository _propertyParameterReadRepository;
        private readonly IApplicationUser _applicationUser;
        private readonly IBillingAccountRepository _billingAccountRepository;
        private readonly IBillingAccountItemRepository _billingAccountItemRepository;
        private readonly IBillingInvoiceReadRepository _billingInvoiceReadRepository;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IBillingInvoiceModelReadRepository _billingInvoiceModelReadRepository;
        private readonly IIntegrationAppService _integrationAppService;

        public BillingInvoicePropertyPortugalAppService(
            IUnitOfWorkManager unitOfWorkManager,
            IBillingInvoicePropertyAdapter billingInvoicePropertyAdapter,
            IDomainService<BillingInvoiceProperty> billingInvoicePropertyDomainService,
            IBillingInvoicePropertyReadRepository billingInvoicePropertyReadRepository,
            IBillingInvoicePropertySupportedTypeAppService billingInvoicePropertySupportedTypeAppService,
            IBillingInvoicePropertySupportedTypeRepository billingInvoicePropertySupportedTypeRepository,
            IBillingInvoicePropertyRepository billingInvoicePropertyRepository,
            IBillingInvoicePropertySupportedTypeReadRepository billingInvoicePropertySupportedTypeReadRepository,
            IBillingItemReadRepository billingItemReadRepository,
            ICountrySubdvisionServiceReadRepository countrySubdvisionServiceReadRepoitory,
            INotificationHandler notificationHandler,
            IInvoiceIntegrationEurope nFEIntegration,
            IReasonReadRepository reasonReadRepository,
            IBillingInvoiceRepository billingInvoiceRepository,
            IBillingInvoiceReadRepository billingInvoiceReadRepository,
            PropertyParameterReadRepository propertyParameterReadRepository,
            IApplicationUser applicationUser,
            IBillingAccountRepository billingAccountRepository,
            IBillingAccountItemRepository billingAccountItemRepository,
            IBillingInvoiceModelReadRepository billingInvoiceModelReadRepository)
            : base(billingInvoicePropertyAdapter, billingInvoicePropertyDomainService, notificationHandler)
        {
            _unitOfWorkManager = unitOfWorkManager;
            _billingInvoicePropertyReadRepository = billingInvoicePropertyReadRepository;
            _billingInvoicePropertySupportedTypeAppService = billingInvoicePropertySupportedTypeAppService;
            _billingInvoicePropertySupportedTypeRepository = billingInvoicePropertySupportedTypeRepository;
            _billingInvoicePropertyRepository = billingInvoicePropertyRepository;
            _billingInvoicePropertySupportedTypeReadRepository = billingInvoicePropertySupportedTypeReadRepository;
            _billingItemReadRepository = billingItemReadRepository;
            _countrySubdvisionServiceReadRepoitory = countrySubdvisionServiceReadRepoitory;
            _nFEIntegration = nFEIntegration;
            _reasonReadRepository = reasonReadRepository;
            _billingInvoiceRepository = billingInvoiceRepository;
            _propertyParameterReadRepository = propertyParameterReadRepository;
            _applicationUser = applicationUser;
            _billingAccountRepository = billingAccountRepository;
            _billingAccountItemRepository = billingAccountItemRepository;
            _billingInvoiceReadRepository = billingInvoiceReadRepository;
            _billingInvoiceModelReadRepository = billingInvoiceModelReadRepository;
        }

        public new void Create(BillingInvoicePropertyDto dto)
        {
            if (dto == null)
            {
                NotifyNullParameter();
                return;
            }

            ValidateBillingInvoiceSeries(dto.BillingInvoicePropertySeries, dto.PropertyId);

            if (Notification.HasNotification())
                return;

            ValidateChildBillingInvoice(dto);

            if (Notification.HasNotification())
                return;

            var billingInvoicePropertyBuilder = BillingInvoicePropertyAdapter.Map(dto);

            var parentBillingInvoiceProperty = BillingInvoicePropertyDomainService.InsertAndSaveChanges(billingInvoicePropertyBuilder);

            var childBillingInvoicePropertyId = CreateChildBillingInvoiceProperty(dto, parentBillingInvoiceProperty);

            if (Notification.HasNotification())
                return;

            ValidateForUnlinkService(dto.Id, childBillingInvoicePropertyId, dto.BillingInvoicePropertySupportedTypeList);

            if (Notification.HasNotification())
                return;

            _billingInvoicePropertySupportedTypeAppService.BillingInvoicePropertySupportedTypeCreate(dto.BillingInvoicePropertySupportedTypeList, parentBillingInvoiceProperty.Id, childBillingInvoicePropertyId);
        }

        private Guid CreateChildBillingInvoiceProperty(BillingInvoicePropertyDto dto, BillingInvoiceProperty parentBillingInvoiceProperty)
        {
            var childDto = dto;
            childDto.Id = Guid.NewGuid();
            childDto.BillingInvoicePropertySeries = dto.ChildBillingInvoicePropertySeries;
            childDto.LastNumber = dto.ChildLastNumber;
            childDto.BillingInvoicePropertyReferenceId = parentBillingInvoiceProperty.Id;
            childDto.Description = string.Empty;

            childDto.BillingInvoiceModelId = _billingInvoiceModelReadRepository.GetIdByBillingInvoiceTypeId((int)BillingInvoiceTypeEnum.CreditNoteTotal);

            var childBillingInvoicePropertyBuilder = BillingInvoicePropertyAdapter.Map(childDto);
            BillingInvoicePropertyDomainService.InsertAndSaveChanges(childBillingInvoicePropertyBuilder);

            return childDto.Id;
        }

        private void ValidateBillingInvoiceSeries(string billingInvoicePropertySeries, int propertyId)
        {
            if (billingInvoicePropertySeries.Length > 10)
            {
                Notification.Raise(Notification
                .DefaultBuilder
                .WithMessage(AppConsts.LocalizationSourceName, BillingInvoicePropertyEnum.Error.BillingInvoicePropertySeriesLength)
                .Build());
                return;
            }

            if (_billingInvoicePropertyRepository.CompanyHasSerieByPropertyId(propertyId, billingInvoicePropertySeries))
            {
                Notification.Raise(Notification
                .DefaultBuilder
                .WithMessage(AppConsts.LocalizationSourceName, BillingInvoicePropertyEnum.Error.BillingInvoicePropertySeriesAlreadyExistsInCompany)
                .Build());
            }
        }

        private void ValidateChildBillingInvoice(BillingInvoicePropertyDto dto)
        {
            //TODO Verificar quantidade de números permitidos para a série
            if (string.IsNullOrEmpty(dto.ChildBillingInvoicePropertySeries) || string.IsNullOrEmpty(dto.ChildLastNumber))
            {
                Notification.Raise(Notification
                .DefaultBuilder
                .WithMessage(AppConsts.LocalizationSourceName, BillingInvoicePropertyEnum.Error.BillingInvoicePropertySeriesLength)
                .Build());
                return;
            }
        }

        private void ValidateForUnlinkService(Guid billingInvoicePropertyId, Guid childBillingInvoicePropertyId, List<BillingInvoicePropertySupportedTypeDto> billingItemList)
        {
            var billingItemIdList = new List<int>();
            if (billingItemList != null && billingItemList.Count() > 0)
            {
                billingItemIdList = billingItemList.Select(e => e.BillingItemId).ToList();

                var propertySupportedWithCodeServiceList = _billingInvoicePropertySupportedTypeReadRepository.GetBillingPropertyListWithoutCodeService(billingInvoicePropertyId, childBillingInvoicePropertyId, billingItemIdList).Items;

                foreach (var propertySupportedCodeService in propertySupportedWithCodeServiceList)
                {
                    _billingInvoicePropertySupportedTypeAppService.Delete(propertySupportedCodeService.Id);
                    _billingInvoicePropertyRepository.SaveChanges();
                }
            }
        }

        public BillingInvoicePropertyDto GetById(Guid id)
        {
            return _billingInvoicePropertyReadRepository.GetById(id);
        }

        public async Task<byte[]> GetBillingInvoicePdf(Guid id)
        {
            return await _billingInvoiceReadRepository.GeneratePDFBillingInvoice(id);
        }

        public void UpdateBillingInvoiceProperty(BillingInvoicePropertyDto parentBillingInvoicePropertyDto)
        {
            var parentBillingInvoicePropertyOld = _billingInvoicePropertyReadRepository.GetById(parentBillingInvoicePropertyDto.Id);

            if (parentBillingInvoicePropertyOld != null)
            {
                parentBillingInvoicePropertyOld.EmailAlert = parentBillingInvoicePropertyDto.EmailAlert;
                parentBillingInvoicePropertyOld.IsIntegrated = parentBillingInvoicePropertyDto.IsIntegrated;
                parentBillingInvoicePropertyOld.Description = parentBillingInvoicePropertyDto.Description;
                parentBillingInvoicePropertyOld.AllowsCancel = parentBillingInvoicePropertyDto.AllowsCancel;
                parentBillingInvoicePropertyOld.DaysForCancel = parentBillingInvoicePropertyDto.DaysForCancel;

                var builder = BillingInvoicePropertyAdapter.Map(parentBillingInvoicePropertyOld);

                if (Notification.HasNotification()) return;

                _billingInvoicePropertyRepository.UpdateBillingInvoiceProperty(builder.Build());

                if (Notification.HasNotification()) return;

                _billingInvoicePropertyRepository.SaveChanges();

                var childBillingInvoiceOld = _billingInvoicePropertyReadRepository.GetChildByParentId(parentBillingInvoicePropertyOld.Id);

                if (childBillingInvoiceOld != null)
                {
                    UpdateChildBillingInvoiceProperty(childBillingInvoiceOld, parentBillingInvoicePropertyDto);

                    if (Notification.HasNotification()) return;

                    _billingInvoicePropertyRepository.SaveChanges();
                }

                if (parentBillingInvoicePropertyDto.BillingInvoicePropertySupportedTypeList != null && parentBillingInvoicePropertyDto.BillingInvoicePropertySupportedTypeList.Count > 0)
                {
                    ValidateForUnlinkService(parentBillingInvoicePropertyDto.Id, childBillingInvoiceOld.Id, parentBillingInvoicePropertyDto.BillingInvoicePropertySupportedTypeList);

                    if (Notification.HasNotification())
                        return;

                    var propertySupportedTypeForCreateList = new List<BillingInvoicePropertySupportedTypeDto>();

                    parentBillingInvoicePropertyDto.BillingInvoicePropertySupportedTypeList.ForEach(b => b.BillingItemTypeId = (int)BillingItemTypeEnum.Service);

                    foreach (var propertySupportedDto in parentBillingInvoicePropertyDto.BillingInvoicePropertySupportedTypeList.DistinctBy(x => x.BillingItemId))
                    {
                        if (!parentBillingInvoicePropertyOld.BillingInvoicePropertySupportedTypeList.Select(b => b.BillingItemId).Contains(propertySupportedDto.BillingItemId))
                            propertySupportedTypeForCreateList.Add(propertySupportedDto);
                    }

                    if (propertySupportedTypeForCreateList != null && propertySupportedTypeForCreateList.Count() > 0)
                        _billingInvoicePropertySupportedTypeAppService.BillingInvoicePropertySupportedTypeCreate(propertySupportedTypeForCreateList, parentBillingInvoicePropertyDto.Id, childBillingInvoiceOld.Id);
                }
            }
        }

        private void UpdateChildBillingInvoiceProperty(BillingInvoicePropertyDto childBillingInvoiceOld, BillingInvoicePropertyDto parentBillingInvoicePropertyOld)
        {
            childBillingInvoiceOld.EmailAlert = parentBillingInvoicePropertyOld.EmailAlert;
            childBillingInvoiceOld.IsIntegrated = parentBillingInvoicePropertyOld.IsIntegrated;
            childBillingInvoiceOld.AllowsCancel = parentBillingInvoicePropertyOld.AllowsCancel;
            childBillingInvoiceOld.DaysForCancel = parentBillingInvoicePropertyOld.DaysForCancel;

            var childBuilder = BillingInvoicePropertyAdapter.Map(childBillingInvoiceOld);

            _billingInvoicePropertyRepository.UpdateBillingInvoiceProperty(childBuilder.Build());
        }

        private void ValidateReasonOfBillingInvoiceCancel(int reasonId)
        {
            if (!_reasonReadRepository.CheckIfReasonIsBillingInvoiceCancel(reasonId))
            {
                Notification.Raise(Notification
                .DefaultBuilder
                .WithMessage(AppConsts.LocalizationSourceName, BillingInvoiceEnum.Error.BillingInvoiceInvalidReason)
                .Build());
            }
        }

        private void ValidateBillingAccountOfInvoice(Guid billingInvoiceId)
        {
            if (BillingAccountIsClosed(billingInvoiceId))
            {
                Notification.Raise(Notification
                .DefaultBuilder
                .WithMessage(AppConsts.LocalizationSourceName, BillingInvoiceEnum.Error.BillingInvoiceInvalidAccountClosed)
                .Build());
            }
        }

        private bool BillingAccountIsClosed(Guid billingInvoiceId)
        {
            var billingAccount = _billingAccountRepository.GetBillingAccountByInvoiceId(billingInvoiceId);
            return billingAccount.StatusId == (int)AccountBillingStatusEnum.Closed;
        }
    }
}
