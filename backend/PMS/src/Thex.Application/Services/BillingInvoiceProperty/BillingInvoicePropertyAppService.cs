﻿using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Polly;
using System;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Thex.Application.Adapters;
using Thex.Application.Interfaces;
using Thex.Common;
using Thex.Common.Enumerations;
using Thex.Common.Helpers;
using Thex.Domain.Entities;
using Thex.Domain.Interfaces.Repositories;
using Thex.Dto;
using Thex.Infra.ReadInterfaces;
using Thex.Kernel;
using Tnf.Domain.Services;
using Tnf.Dto;
using Tnf.Notifications;
using Tnf.Repositories.Uow;

namespace Thex.Application.Services
{
    public class BillingInvoicePropertyAppService : ScaffoldBillingInvoicePropertyAppService, IBillingInvoicePropertyAppService
    {
        private readonly IBillingInvoicePropertyReadRepository _billingInvoicePropertyReadRepository;
        private readonly IBillingInvoicePropertyRepository _billingInvoicePropertyRepository;
        private readonly IBillingInvoicePropertySupportedTypeAppService _billingInvoicePropertySupportedTypeAppService;
        private readonly IBillingInvoicePropertySupportedTypeRepository _billingInvoicePropertySupportedTypeRepository;
        private readonly IBillingInvoicePropertySupportedTypeReadRepository _billingInvoicePropertySupportedTypeReadRepository;
        private readonly IBillingItemReadRepository _billingItemReadRepository;
        private readonly ICountrySubdvisionServiceReadRepository _countrySubdvisionServiceReadRepoitory;
        private readonly IReasonReadRepository _reasonReadRepository;
        private readonly IBillingInvoiceRepository _billingInvoiceRepository;
        private readonly IPropertyParameterReadRepository _propertyParameterReadRepository;
        private readonly IApplicationUser _applicationUser;
        private readonly IBillingAccountRepository _billingAccountRepository;
        private readonly IBillingAccountItemRepository _billingAccountItemRepository;
        private readonly IBillingInvoiceReadRepository _billingInvoiceReadRepository;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IBillingInvoicePropertyBrazilAppService _billingInvoicePropertyBrazilAppService;
        private readonly IBillingInvoicePropertyPortugalAppService _billingInvoicePropertyPortugalAppService;
        private readonly IBillingAccountAppService _billingAccountAppService;
        private readonly INotificationHandler _notificationHandler;
        private readonly IConfiguration _configuration;
        private readonly IIntegrationAppService _integrationAppService;
        private readonly IReservationItemReadRepository _reservationItemReadRepository;

        public BillingInvoicePropertyAppService(
            IUnitOfWorkManager unitOfWorkManager,
            IBillingInvoicePropertyAdapter billingInvoicePropertyAdapter,
            IDomainService<BillingInvoiceProperty> billingInvoicePropertyDomainService,
            IBillingInvoicePropertyReadRepository billingInvoicePropertyReadRepository,
            IBillingInvoicePropertySupportedTypeAppService billingInvoicePropertySupportedTypeAppService,
            IBillingInvoicePropertySupportedTypeRepository billingInvoicePropertySupportedTypeRepository,
            IBillingInvoicePropertyRepository billingInvoicePropertyRepository,
            IBillingInvoicePropertySupportedTypeReadRepository billingInvoicePropertySupportedTypeReadRepository,
            IBillingItemReadRepository billingItemReadRepository,
            ICountrySubdvisionServiceReadRepository countrySubdvisionServiceReadRepoitory,
            INotificationHandler notificationHandler,
            IReasonReadRepository reasonReadRepository,
            IBillingInvoiceRepository billingInvoiceRepository,
            IBillingInvoiceReadRepository billingInvoiceReadRepository,
            IPropertyParameterReadRepository propertyParameterReadRepository,
            IApplicationUser applicationUser,
            IBillingAccountRepository billingAccountRepository,
            IBillingAccountItemRepository billingAccountItemRepository,
            IBillingAccountAppService billingAccountAppService,
            IBillingInvoicePropertyBrazilAppService billingInvoicePropertyBrazilAppService,
            IBillingInvoicePropertyPortugalAppService billingInvoicePropertyPortugalAppService,
            IConfiguration configuration,
            IIntegrationAppService integrationAppService,
            IReservationItemReadRepository reservationItemReadRepository)
            : base(billingInvoicePropertyAdapter, billingInvoicePropertyDomainService, notificationHandler)
        {
            _unitOfWorkManager = unitOfWorkManager;
            _billingInvoicePropertyReadRepository = billingInvoicePropertyReadRepository;
            _billingInvoicePropertySupportedTypeAppService = billingInvoicePropertySupportedTypeAppService;
            _billingInvoicePropertySupportedTypeRepository = billingInvoicePropertySupportedTypeRepository;
            _billingInvoicePropertyRepository = billingInvoicePropertyRepository;
            _billingInvoicePropertySupportedTypeReadRepository = billingInvoicePropertySupportedTypeReadRepository;
            _billingItemReadRepository = billingItemReadRepository;
            _countrySubdvisionServiceReadRepoitory = countrySubdvisionServiceReadRepoitory;
            _reasonReadRepository = reasonReadRepository;
            _billingInvoiceRepository = billingInvoiceRepository;
            _propertyParameterReadRepository = propertyParameterReadRepository;
            _applicationUser = applicationUser;
            _billingAccountRepository = billingAccountRepository;
            _billingAccountItemRepository = billingAccountItemRepository;
            _billingInvoiceReadRepository = billingInvoiceReadRepository;
            _billingInvoicePropertyBrazilAppService = billingInvoicePropertyBrazilAppService;
            _billingInvoicePropertyPortugalAppService = billingInvoicePropertyPortugalAppService;
            _billingAccountAppService = billingAccountAppService;
            _notificationHandler = notificationHandler;
            _configuration = configuration;
            _integrationAppService = integrationAppService;
            _reservationItemReadRepository = reservationItemReadRepository;
        }

        public new void Create(BillingInvoicePropertyDto dto)
        {
            if (dto == null)
            {
                NotifyNullParameter();
                return;
            }

            dto.PropertyId = int.Parse(_applicationUser.PropertyId);
            dto.BillingInvoicePropertySupportedTypeList.ForEach(b => b.PropertyId = int.Parse(_applicationUser.PropertyId));

            using (var uow = _unitOfWorkManager.Begin())
            {
                var propertyCountryCode = _applicationUser.PropertyCountryCode;

                switch (EnumHelper.GetEnumValue<CountryIsoCodeEnum>(_applicationUser.PropertyCountryCode, true))
                {
                    case CountryIsoCodeEnum.Brasil:
                        {
                            _billingInvoicePropertyBrazilAppService.Create(dto);
                            break;
                        }
                    case CountryIsoCodeEnum.Europe:
                        {
                            _billingInvoicePropertyPortugalAppService.Create(dto);
                            break;
                        }
                    case CountryIsoCodeEnum.Latam:
                    default:
                        {
                            // TODO: Criar a mensagem de erro corretamente
                            _notificationHandler.Raise(_notificationHandler.DefaultBuilder
                                                                     .WithMessage(AppConsts.LocalizationSourceName, BillingInvoiceEnum.Error.BillingInvoiceInvalidAccountClosed)
                                                                     .WithDetailedMessage(string.Concat(_applicationUser.PropertyCountryCode, AppConsts.LocalizationSourceName, BillingInvoiceEnum.Error.BillingInvoiceInvalidAccountClosed))
                                                                     .Build());

                            break;
                        }
                }

                uow.Complete();
            }
        }

        public IListDto<BillingInvoicePropertyDto> GetAllByPropertyId(int propertyId, GetAllBillingInvoicePropertyDto request)
        {
            return _billingInvoicePropertyReadRepository.GetAllByPropertyId(propertyId, request);
        }

        public IListDto<BillingInvoicePropertyDto> GetAllWithoutReference()
        {
            var propertyId = int.Parse(_applicationUser.PropertyId);
            return _billingInvoicePropertyReadRepository.GetAllWithoutReferenceByPropertyId(propertyId);
        }

        public BillingInvoicePropertyDto GetById(Guid id)
        {
            return _billingInvoicePropertyReadRepository.GetById(id);
        }

        public async Task<byte[]> GetBillingInvoicePdf(Guid id)
        {
            switch (EnumHelper.GetEnumValue<CountryIsoCodeEnum>(_applicationUser.PropertyCountryCode, true))
            {
                case CountryIsoCodeEnum.Brasil:
                    {
                        return await _billingInvoiceReadRepository.GeneratePDFBillingInvoice(id);
                    }
                case CountryIsoCodeEnum.Europe:
                    {
                        var integratorId = _billingInvoiceReadRepository.GetIntegratorIdById(id);

                        if (string.IsNullOrEmpty(integratorId) || string.IsNullOrWhiteSpace(integratorId))
                            integratorId = await _integrationAppService.GetIntegratorIdById(id);

                        if (!string.IsNullOrEmpty(integratorId) && !string.IsNullOrWhiteSpace(integratorId))
                        {
                            var billingInvoiceWithSameIntegratorIdList = _billingInvoiceReadRepository.GetAllByIntegratorId(integratorId);

                            if (billingInvoiceWithSameIntegratorIdList.Any(b => b.BillingInvoiceTypeId == (int)BillingInvoiceTypeEnum.Factura)
                                &&
                                (billingInvoiceWithSameIntegratorIdList.Any(b => b.BillingInvoiceTypeId == (int)BillingInvoiceTypeEnum.CreditNotePartial) ||
                                billingInvoiceWithSameIntegratorIdList.Any(b => b.BillingInvoiceTypeId == (int)BillingInvoiceTypeEnum.CreditNoteTotal)))
                            {
                                _notificationHandler.Raise(_notificationHandler.DefaultBuilder
                                                                    .WithMessage(AppConsts.LocalizationSourceName, BillingInvoice.EntityError.BillingInvoiceDoesNotIssued)
                                                                    .WithDetailedMessage(string.Concat(_applicationUser.PropertyCountryCode, AppConsts.LocalizationSourceName, BillingInvoice.EntityError.BillingInvoiceDoesNotIssued))
                                                                    .Build());
                                return null;
                            }

                            using (var httpClient = new HttpClient())
                            {
                                var response = Policy.HandleResult<HttpResponseMessage>(message => !message.IsSuccessStatusCode ||
                                                                                                    (message.Content.ReadAsByteArrayAsync().Result).Length == 1)
                                      .WaitAndRetry(5, retryAttempt => TimeSpan.FromSeconds(Math.Pow(2, retryAttempt)),
                                      (exception, timeSpan, retryCount, context) =>
                                      {
                                          _notificationHandler.Raise(_notificationHandler.DefaultBuilder
                                                                              .WithMessage(AppConsts.LocalizationSourceName, BillingInvoice.EntityError.BillingInvoiceWithInvalidParterPDFGenerator)
                                                                              .WithDetailedMessage(string.Concat(_applicationUser.PropertyCountryCode, AppConsts.LocalizationSourceName, BillingInvoice.EntityError.BillingInvoiceWithInvalidParterPDFGenerator))
                                                                              .Build());
                                      })
                                      .Execute(() => httpClient.GetAsync(GetInvoiceDowloadUrl(integratorId)).Result);

                                if (response.IsSuccessStatusCode)
                                {
                                    var billingInvoiceOutput = JsonConvert.DeserializeObject<BillingInvoiceXpressOutputDto>(await response.Content.ReadAsStringAsync());

                                    if (billingInvoiceOutput != null && billingInvoiceOutput.Output != null)
                                    {
                                        var downloadResult = Policy.HandleResult<HttpResponseMessage>(message => !message.IsSuccessStatusCode)
                                          .WaitAndRetry(5, retryAttempt => TimeSpan.FromSeconds(Math.Pow(2, retryAttempt)),
                                          (exception, timeSpan, retryCount, context) =>
                                          {
                                              _notificationHandler.Raise(_notificationHandler.DefaultBuilder
                                                                                  .WithMessage(AppConsts.LocalizationSourceName, BillingInvoice.EntityError.BillingInvoiceWithInvalidParterPDFGenerator)
                                                                                  .WithDetailedMessage(string.Concat(_applicationUser.PropertyCountryCode, AppConsts.LocalizationSourceName, BillingInvoice.EntityError.BillingInvoiceWithInvalidParterPDFGenerator))
                                                                                  .Build());
                                          })
                                      .Execute(() => httpClient.GetAsync(billingInvoiceOutput.Output.PdfUrl).Result);

                                        if (!downloadResult.IsSuccessStatusCode)
                                            return null;

                                        var byteArray = await downloadResult.Content.ReadAsByteArrayAsync();
                                        return byteArray;
                                    }
                                    else
                                        return null;
                                }
                                else
                                    return null;
                            }
                        }
                        else
                            return null;
                    }
                case CountryIsoCodeEnum.Latam:
                default:
                    {
                        _notificationHandler.Raise(_notificationHandler.DefaultBuilder
                                                                    .WithMessage(AppConsts.LocalizationSourceName, BillingInvoiceEnum.Error.BillingInvoiceInvalidAccountClosed)
                                                                    .WithDetailedMessage(string.Concat(_applicationUser.PropertyCountryCode, AppConsts.LocalizationSourceName, BillingInvoiceEnum.Error.BillingInvoiceInvalidAccountClosed))
                                                                    .Build());

                        return null;
                    }
            }
        }


        public async Task<byte[]> GetProformaPdf(long reservationId, long reservationItemId)
        {
            switch (EnumHelper.GetEnumValue<CountryIsoCodeEnum>(_applicationUser.PropertyCountryCode, true))
            {
                case CountryIsoCodeEnum.Europe:
                    {
                        string integrationId = string.Empty;

                        if (reservationId == default(int))
                            integrationId = _reservationItemReadRepository.GetIntegrationIdById(reservationItemId);
                        else
                            integrationId = _reservationItemReadRepository.GetIntegrationIdByReservationId(reservationId);

                        if (!string.IsNullOrEmpty(integrationId) && !string.IsNullOrWhiteSpace(integrationId))
                        {
                            using (var httpClient = new HttpClient())
                            {
                                var response = Policy.HandleResult<HttpResponseMessage>(message => !message.IsSuccessStatusCode ||
                                                                                                    (message.Content.ReadAsByteArrayAsync().Result).Length == 1)
                                      .WaitAndRetry(5, retryAttempt => TimeSpan.FromSeconds(Math.Pow(2, retryAttempt)),
                                      (exception, timeSpan, retryCount, context) =>
                                      {
                                          _notificationHandler.Raise(_notificationHandler.DefaultBuilder
                                                                              .WithMessage(AppConsts.LocalizationSourceName, BillingInvoice.EntityError.BillingInvoiceWithInvalidParterPDFGenerator)
                                                                              .WithDetailedMessage(string.Concat(_applicationUser.PropertyCountryCode, AppConsts.LocalizationSourceName, BillingInvoice.EntityError.BillingInvoiceWithInvalidParterPDFGenerator))
                                                                              .Build());
                                      })
                                      .Execute(() => httpClient.GetAsync(GetInvoiceDowloadUrl(integrationId)).Result);

                                if (response.IsSuccessStatusCode)
                                {
                                    var billingInvoiceOutput = JsonConvert.DeserializeObject<BillingInvoiceXpressOutputDto>(await response.Content.ReadAsStringAsync());

                                    if (billingInvoiceOutput != null && billingInvoiceOutput.Output != null)
                                    {
                                        var downloadResult = Policy.HandleResult<HttpResponseMessage>(message => !message.IsSuccessStatusCode)
                                          .WaitAndRetry(5, retryAttempt => TimeSpan.FromSeconds(Math.Pow(2, retryAttempt)),
                                          (exception, timeSpan, retryCount, context) =>
                                          {
                                              _notificationHandler.Raise(_notificationHandler.DefaultBuilder
                                                                                  .WithMessage(AppConsts.LocalizationSourceName, BillingInvoice.EntityError.BillingInvoiceWithInvalidParterPDFGenerator)
                                                                                  .WithDetailedMessage(string.Concat(_applicationUser.PropertyCountryCode, AppConsts.LocalizationSourceName, BillingInvoice.EntityError.BillingInvoiceWithInvalidParterPDFGenerator))
                                                                                  .Build());
                                          })
                                      .Execute(() => httpClient.GetAsync(billingInvoiceOutput.Output.PdfUrl).Result);

                                        if (!downloadResult.IsSuccessStatusCode)
                                            return null;

                                        var byteArray = await downloadResult.Content.ReadAsByteArrayAsync();
                                        return byteArray;
                                    }
                                    else
                                        return null;
                                }
                                else
                                    return null;
                            }
                        }
                        else
                            return null;
                    }
                case CountryIsoCodeEnum.Brasil:
                case CountryIsoCodeEnum.Latam:
                default:
                    {
                        _notificationHandler.Raise(_notificationHandler.DefaultBuilder
                                                                    .WithMessage(AppConsts.LocalizationSourceName, BillingInvoiceEnum.Error.BillingInvoiceInvalidAccountClosed)
                                                                    .WithDetailedMessage(string.Concat(_applicationUser.PropertyCountryCode, AppConsts.LocalizationSourceName, BillingInvoiceEnum.Error.BillingInvoiceInvalidAccountClosed))
                                                                    .Build());

                        return null;
                    }
            }
        }

        private string GetJsonWithEndpointToDownload(HttpClient httpClient, string integratorId)
        {
            var response = httpClient.GetAsync(GetInvoiceDowloadUrl(integratorId)).Result;

            if (!response.IsSuccessStatusCode)
                return null;

            return response.Content.ReadAsStringAsync().Result;
        }

        private string GetInvoiceDowloadUrl(string integratorId)
        {
            var propertyWithDetails = _propertyParameterReadRepository.GetDtoByIdAsync(_applicationUser.PropertyId.TryParseToInt32()).Result;
            if (propertyWithDetails == null || propertyWithDetails.IntegrationPartnerPropertyList == null || propertyWithDetails.IntegrationPartnerPropertyList.Count == 0)
                _notificationHandler.Raise(_notificationHandler
                                       .DefaultBuilder
                                       .WithMessage(AppConsts.LocalizationSourceName, BillingAccountItemEnum.Error.BillingAccountInvoiceNFeioComunicationError)
                                       .Build());

            var integrationProperty = propertyWithDetails.IntegrationPartnerPropertyList.Where(x => x.PropertyId == _applicationUser.PropertyId.TryParseToInt32() &&
                                                                                                    x.IntegrationPartnerType == (int)IntegrationPartnerTypeEnum.InvoiceXpress).FirstOrDefault();
            if (integrationProperty == null)
                _notificationHandler.Raise(_notificationHandler
                                   .DefaultBuilder
                                   .WithMessage(AppConsts.LocalizationSourceName, BillingAccountItemEnum.Error.BillingAccountInvoiceNFeioComunicationError)
                                   .Build());

            switch (EnumHelper.GetEnumValue<EnvironmentEnum>(_configuration.GetValue<string>("CloudEnvironment"), true))
            {
                case EnvironmentEnum.Production:
                    break;
                case EnvironmentEnum.Development:
                case EnvironmentEnum.BugFix:
                case EnvironmentEnum.Test:
                case EnvironmentEnum.UAT:
                case EnvironmentEnum.Staging:
                default:
                    {
                        integrationProperty.IntegrationCode = _configuration.GetValue<string>("InvoiceXpressIntegrationCode");
                        integrationProperty.IntegrationNumber = _configuration.GetValue<string>("InvoiceXpressTokenClient");
                        break;
                    }
            }

            var url = $"{_configuration.GetValue<string>("InvoiceXpressBaseEndpoint")}";

            return string.Concat(string.Format(url, integrationProperty.IntegrationNumber.Trim()), string.Format("/api/pdf/{0}.json?second_copy=false", integratorId), $"&api_key={integrationProperty.IntegrationCode.Trim()}");
        }

        public void UpdateBillingInvoiceProperty(BillingInvoicePropertyDto billingInvoicePropertyDto)
        {
            if (billingInvoicePropertyDto == null)
                NotifyNullParameter();

            if (Notification.HasNotification()) return;

            if (billingInvoicePropertyDto.Id == null || billingInvoicePropertyDto.Id == Guid.Empty)
                NotifyIdIsMissing();

            if (Notification.HasNotification()) return;

            billingInvoicePropertyDto.PropertyId = int.Parse(_applicationUser.PropertyId);
            billingInvoicePropertyDto.BillingInvoicePropertySupportedTypeList.ForEach(b => b.PropertyId = int.Parse(_applicationUser.PropertyId));

            using (var uow = _unitOfWorkManager.Begin())
            {
                switch (EnumHelper.GetEnumValue<CountryIsoCodeEnum>(_applicationUser.PropertyCountryCode, true))
                {
                    case CountryIsoCodeEnum.Brasil:
                        {
                            _billingInvoicePropertyBrazilAppService.UpdateBillingInvoiceProperty(billingInvoicePropertyDto);
                            break;
                        }
                    case CountryIsoCodeEnum.Europe:
                        {
                            _billingInvoicePropertyPortugalAppService.UpdateBillingInvoiceProperty(billingInvoicePropertyDto);
                            break;
                        }
                    case CountryIsoCodeEnum.Latam:
                    default:
                        {
                            // TODO: Criar a mensagem de erro corretamente
                            _notificationHandler.Raise(_notificationHandler.DefaultBuilder
                                                                     .WithMessage(AppConsts.LocalizationSourceName, BillingInvoiceEnum.Error.BillingInvoiceInvalidAccountClosed)
                                                                     .WithDetailedMessage(string.Concat(_applicationUser.PropertyCountryCode, AppConsts.LocalizationSourceName, BillingInvoiceEnum.Error.BillingInvoiceInvalidAccountClosed))
                                                                     .Build());

                            break;
                        }
                }

                uow.Complete();
            }
        }

        public async Task CancelBillingInvoiceAsync(int propertyId, Guid billingInvoiceId, int reasonId)
        {
            using (var uow = _unitOfWorkManager.Begin())
            {
                var propertyCountryCode = _applicationUser.PropertyCountryCode;

                switch (EnumHelper.GetEnumValue<CountryIsoCodeEnum>(_applicationUser.PropertyCountryCode, true))
                {
                    case CountryIsoCodeEnum.Brasil:
                        {
                            await _billingInvoicePropertyBrazilAppService.CancelBillingInvoiceAsync(propertyId, billingInvoiceId, reasonId);
                            break;
                        }
                    case CountryIsoCodeEnum.Europe:
                    case CountryIsoCodeEnum.Latam:
                    default:
                        {
                            // TODO: Criar a mensagem de erro corretamente
                            _notificationHandler.Raise(_notificationHandler.DefaultBuilder
                                                                     .WithMessage(AppConsts.LocalizationSourceName, BillingInvoiceEnum.Error.BillingInvoiceInvalidAccountClosed)
                                                                     .WithDetailedMessage(string.Concat(_applicationUser.PropertyCountryCode, AppConsts.LocalizationSourceName, BillingInvoiceEnum.Error.BillingInvoiceInvalidAccountClosed))
                                                                     .Build());

                            break;
                        }
                }

                uow.Complete();
            }
        }

        private void ValidateReasonOfBillingInvoiceCancel(int reasonId)
        {
            if (!_reasonReadRepository.CheckIfReasonIsBillingInvoiceCancel(reasonId))
            {
                Notification.Raise(Notification
                .DefaultBuilder
                .WithMessage(AppConsts.LocalizationSourceName, BillingInvoiceEnum.Error.BillingInvoiceInvalidReason)
                .Build());
            }
        }

        private void ValidateBillingAccountOfInvoice(Guid billingInvoiceId)
        {
            if (BillingAccountIsClosed(billingInvoiceId))
            {
                Notification.Raise(Notification
                .DefaultBuilder
                .WithMessage(AppConsts.LocalizationSourceName, BillingInvoiceEnum.Error.BillingInvoiceInvalidAccountClosed)
                .Build());
            }
        }

        private bool BillingAccountIsClosed(Guid billingInvoiceId)
        {
            var billingAccount = _billingAccountRepository.GetBillingAccountByInvoiceId(billingInvoiceId);
            return billingAccount.StatusId == (int)AccountBillingStatusEnum.Closed;
        }

        public BillingAccountWithAccountsDto GetInvoiceDetails(Guid billingInvoiceId, Guid billingAccountId)
        {
            if (!AnyValidInvoiceAndAccountId(billingInvoiceId, billingAccountId))
            {
                NotifyNullParameter();
                return null;
            }

            var billingAccountEntries = _billingAccountAppService.GetUniqueBillingAccountWithEntries(new DefaultGuidRequestDto(billingAccountId), billingInvoiceId, true);

            if (billingAccountEntries == null)
                return null;

            billingAccountEntries.HeaderPerson = _billingAccountAppService.GetHeaderByBillingInvoiceId(billingInvoiceId);

            return billingAccountEntries;
        }

        private bool AnyValidInvoiceAndAccountId(Guid billingInvoiceId, Guid billingAccountId)
        {
            return new[] { billingInvoiceId, billingAccountId }.Any(e => e != Guid.Empty);
        }
    }
}
