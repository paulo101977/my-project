﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Thex.Application.Adapters;
using Thex.Application.Interfaces;
using Thex.Common;
using Thex.Common.Enumerations;
using Thex.Domain.Entities;
using Thex.Domain.Interfaces.Integration;
using Thex.Domain.Interfaces.Repositories;
using Thex.Dto;
using Thex.Infra.ReadInterfaces;
using Thex.Infra.Repositories.Read;
using Thex.Kernel;
using Tnf.Domain.Services;
using Tnf.Dto;
using Tnf.Notifications;
using Tnf.Repositories.Uow;

namespace Thex.Application.Services
{
    public class BillingInvoicePropertyBrazilAppService : ScaffoldBillingInvoicePropertyAppService, IBillingInvoicePropertyBrazilAppService
    {
        private readonly IBillingInvoicePropertyReadRepository _billingInvoicePropertyReadRepository;
        private readonly IBillingInvoicePropertyRepository _billingInvoicePropertyRepository;
        private readonly IBillingInvoicePropertySupportedTypeAppService _billingInvoicePropertySupportedTypeAppService;
        private readonly IBillingInvoicePropertySupportedTypeRepository _billingInvoicePropertySupportedTypeRepository;
        private readonly IBillingInvoicePropertySupportedTypeReadRepository _billingInvoicePropertySupportedTypeReadRepository;
        private readonly IBillingItemReadRepository _billingItemReadRepository;
        private readonly ICountrySubdvisionServiceReadRepository _countrySubdvisionServiceReadRepoitory;
        private readonly IInvoiceIntegrationBrazil _nFEIntegration;
        private readonly IReasonReadRepository _reasonReadRepository;
        private readonly IBillingInvoiceRepository _billingInvoiceRepository;
        private readonly PropertyParameterReadRepository _propertyParameterReadRepository;
        private readonly IApplicationUser _applicationUser;
        private readonly IBillingAccountRepository _billingAccountRepository;
        private readonly IBillingAccountItemRepository _billingAccountItemRepository;
        private readonly IBillingInvoiceReadRepository _billingInvoiceReadRepository;
        private readonly IUnitOfWorkManager _unitOfWorkManager;

        public BillingInvoicePropertyBrazilAppService(
            IUnitOfWorkManager unitOfWorkManager,
            IBillingInvoicePropertyAdapter billingInvoicePropertyAdapter,
            IDomainService<BillingInvoiceProperty> billingInvoicePropertyDomainService,
            IBillingInvoicePropertyReadRepository billingInvoicePropertyReadRepository,
            IBillingInvoicePropertySupportedTypeAppService billingInvoicePropertySupportedTypeAppService,
            IBillingInvoicePropertySupportedTypeRepository billingInvoicePropertySupportedTypeRepository,
            IBillingInvoicePropertyRepository billingInvoicePropertyRepository,
            IBillingInvoicePropertySupportedTypeReadRepository billingInvoicePropertySupportedTypeReadRepository,
            IBillingItemReadRepository billingItemReadRepository,
            ICountrySubdvisionServiceReadRepository countrySubdvisionServiceReadRepoitory,
            INotificationHandler notificationHandler,
            IInvoiceIntegrationBrazil nFEIntegration,
            IReasonReadRepository reasonReadRepository,
            IBillingInvoiceRepository billingInvoiceRepository,
            IBillingInvoiceReadRepository billingInvoiceReadRepository,
            PropertyParameterReadRepository propertyParameterReadRepository,
            IApplicationUser applicationUser,
            IBillingAccountRepository billingAccountRepository,
            IBillingAccountItemRepository billingAccountItemRepository
           )
            : base(billingInvoicePropertyAdapter, billingInvoicePropertyDomainService, notificationHandler)
        {
            _unitOfWorkManager = unitOfWorkManager;
            _billingInvoicePropertyReadRepository = billingInvoicePropertyReadRepository;
            _billingInvoicePropertySupportedTypeAppService = billingInvoicePropertySupportedTypeAppService;
            _billingInvoicePropertySupportedTypeRepository = billingInvoicePropertySupportedTypeRepository;
            _billingInvoicePropertyRepository = billingInvoicePropertyRepository;
            _billingInvoicePropertySupportedTypeReadRepository = billingInvoicePropertySupportedTypeReadRepository;
            _billingItemReadRepository = billingItemReadRepository;
            _countrySubdvisionServiceReadRepoitory = countrySubdvisionServiceReadRepoitory;
            _nFEIntegration = nFEIntegration;
            _reasonReadRepository = reasonReadRepository;
            _billingInvoiceRepository = billingInvoiceRepository;
            _propertyParameterReadRepository = propertyParameterReadRepository;
            _applicationUser = applicationUser;
            _billingAccountRepository = billingAccountRepository;
            _billingAccountItemRepository = billingAccountItemRepository;
            _billingInvoiceReadRepository = billingInvoiceReadRepository;
        }

        public async new void Create(BillingInvoicePropertyDto dto)
        {
            ValidateBillingInvoiceSeries(dto.BillingInvoicePropertySeries, dto.PropertyId);

            if (Notification.HasNotification())
                return;

            var billingInvoicePropertyBuilder = BillingInvoicePropertyAdapter.Map(dto);

            var billingInvoiceProperty = BillingInvoicePropertyDomainService.InsertAndSaveChanges(billingInvoicePropertyBuilder);

            _billingInvoicePropertyRepository.SaveChanges();

            dto.Id = billingInvoiceProperty.Id;

            //Desvincular codigo de serviço
            var countryServiceList = dto.BillingInvoicePropertySupportedTypeList.Where(x => x.CountrySubdvisionServiceId != null && x.CountrySubdvisionServiceId != Guid.Empty).Select(x => x.CountrySubdvisionServiceId.Value).Distinct().ToList();

            if (countryServiceList != null)
                ValidateForUnlinkService(dto.PropertyId, dto.Id, countryServiceList, dto.BillingInvoicePropertySupportedTypeList);

            if (Notification.HasNotification())
                return;

            _billingInvoicePropertySupportedTypeAppService.BillingInvoicePropertySupportedTypeCreate(dto.BillingInvoicePropertySupportedTypeList, dto.Id);
        }

        private void ValidateBillingInvoiceSeries(string billingInvoicePropertySeries, int propertyId)
        {
            if (billingInvoicePropertySeries.Length > 5)
            {
                Notification.Raise(Notification
                .DefaultBuilder
                .WithMessage(AppConsts.LocalizationSourceName, BillingInvoicePropertyEnum.Error.BillingInvoicePropertySeriesLength)
                .Build());
                return;
            }

            if (_billingInvoicePropertyRepository.CompanyHasSerieByPropertyId(propertyId, billingInvoicePropertySeries))
            {
                Notification.Raise(Notification
                .DefaultBuilder
                .WithMessage(AppConsts.LocalizationSourceName, BillingInvoicePropertyEnum.Error.BillingInvoicePropertySeriesAlreadyExistsInCompany)
                .Build());
            }
        }


        private void ValidateForUnlinkService(int propertyId, Guid billingInvoicePropertyId, List<Guid> countrySubdvisionServiceIdList, List<BillingInvoicePropertySupportedTypeDto> billingItemList)
        {
            var billingItemIdList = new List<int>();
            if (billingItemList != null && billingItemList.Count() > 0)
            {
                billingItemIdList = billingItemList.Select(e => e.BillingItemId).ToList();
                //Verificar se existe um billingItem com o codigo de serviço selecionado na tabela BillingInvoicePropertySupportedType
                // Recuperar todos os o modelos que possuem os códigos de serviços selecionados
                var propertySupportedWithCodeServiceList = _billingInvoicePropertySupportedTypeReadRepository.VerifyExistingBillingPropertyWithCodeService(propertyId, billingInvoicePropertyId, countrySubdvisionServiceIdList, billingItemIdList).Items;

                if (propertySupportedWithCodeServiceList != null && propertySupportedWithCodeServiceList.Count > 0)
                {
                    foreach (var propertySupportedCodeService in propertySupportedWithCodeServiceList)
                    {
                        _billingInvoicePropertySupportedTypeAppService.Delete(propertySupportedCodeService.Id);
                        _billingInvoicePropertyRepository.SaveChanges();
                    }
                }

            }
        }

        public IListDto<BillingInvoicePropertyDto> GetAllByPropertyId(int propertyId, GetAllBillingInvoicePropertyDto request)
        {
            return _billingInvoicePropertyReadRepository.GetAllByPropertyId(propertyId, request);
        }

        public IListDto<BillingInvoicePropertyDto> GetAllWithoutReference()
        {
            var propertyId = int.Parse(_applicationUser.PropertyId);
            return _billingInvoicePropertyReadRepository.GetAllWithoutReferenceByPropertyId(propertyId);
        }

        public BillingInvoicePropertyDto GetById(Guid id)
        {
            return _billingInvoicePropertyReadRepository.GetById(id);
        }

        public async Task<byte[]> GetBillingInvoicePdf(Guid id)
        {
            return await _billingInvoiceReadRepository.GeneratePDFBillingInvoice(id);
        }

        public void UpdateBillingInvoiceProperty(BillingInvoicePropertyDto billingInvoicePropertyDto)
        {
            var billingInvoicePropertyOld = _billingInvoicePropertyReadRepository.GetById(billingInvoicePropertyDto.Id);

            if (billingInvoicePropertyOld != null)
            {
                billingInvoicePropertyOld.EmailAlert = billingInvoicePropertyDto.EmailAlert;
                billingInvoicePropertyOld.IsIntegrated = billingInvoicePropertyDto.IsIntegrated;
                billingInvoicePropertyOld.Description = billingInvoicePropertyDto.Description;
                billingInvoicePropertyOld.AllowsCancel = billingInvoicePropertyDto.AllowsCancel;
                billingInvoicePropertyOld.DaysForCancel = billingInvoicePropertyDto.DaysForCancel;

                var builder = BillingInvoicePropertyAdapter.Map(billingInvoicePropertyOld);

                if (Notification.HasNotification()) return;

                _billingInvoicePropertyRepository.UpdateBillingInvoiceProperty(builder.Build());

                if (Notification.HasNotification()) return;

                if (billingInvoicePropertyDto.BillingInvoicePropertySupportedTypeList != null && billingInvoicePropertyDto.BillingInvoicePropertySupportedTypeList.Count > 0)
                {
                    var billingItemsInformations = _billingItemReadRepository.GetAllServiceWithParameters(billingInvoicePropertyDto.PropertyId).Items;
                    var billingItemsInformationsServices = billingItemsInformations.Where(x => x.BillingItemTypeId == (int)BillingItemTypeEnum.Service).ToList();
                    var billingItemsInformationsPdv = billingItemsInformations.Where(x => x.BillingItemTypeId == (int)BillingItemTypeEnum.PointOfSale).ToList();
                        
                    var countrySubdvionServiceInformations = _countrySubdvisionServiceReadRepoitory.GetAllByPropertyId(billingInvoicePropertyDto.PropertyId).Items;

                    //Desvincular codigo de serviço
                    var countryServiceList = billingInvoicePropertyDto.BillingInvoicePropertySupportedTypeList.Where(x => x.CountrySubdvisionServiceId != null && x.CountrySubdvisionServiceId != Guid.Empty).Select(x => x.CountrySubdvisionServiceId.Value).Distinct().ToList();
                    if (countryServiceList != null)
                        ValidateForUnlinkService(billingInvoicePropertyDto.PropertyId, billingInvoicePropertyDto.Id, countryServiceList, billingInvoicePropertyDto.BillingInvoicePropertySupportedTypeList);

                    var propertySupportedTypeForCreateList = new List<BillingInvoicePropertySupportedTypeDto>();

                    foreach (var propertySupportedNew in billingInvoicePropertyDto.BillingInvoicePropertySupportedTypeList)
                    {
                        //Verificar se existe o Item mencionado e se é do tipo Serviço ou PDV
                        if (!billingItemsInformations.Where(x => x.BillingItemId == propertySupportedNew.BillingItemId).Any())
                            NotifyParameterInvalid();

                        //Verificar se billingItemsInformationsPdv billingItemsInformationsServices
                        if (billingItemsInformationsPdv.Any(e => e.BillingItemId == propertySupportedNew.BillingItemId) && propertySupportedNew.CountrySubdvisionServiceId.HasValue)
                            NotifyParameterInvalid();

                        if (billingItemsInformationsServices.Any(e => e.BillingItemId == propertySupportedNew.BillingItemId) && !propertySupportedNew.CountrySubdvisionServiceId.HasValue)
                            NotifyParameterInvalid();

                        if (Notification.HasNotification()) break;

                        //Verificar se o Codigo de serviço é valido
                        if (propertySupportedNew.CountrySubdvisionServiceId != null && !countrySubdvionServiceInformations.Where(x => x.Id == propertySupportedNew.CountrySubdvisionServiceId).Any())
                            NotifyParameterInvalid();

                        if (Notification.HasNotification()) break;

                        var propertySupportedOld = billingInvoicePropertyOld.BillingInvoicePropertySupportedTypeList.Where(x => x.BillingItemId == propertySupportedNew.BillingItemId).FirstOrDefault();

                        if (propertySupportedOld != null)
                        {
                            propertySupportedOld.CountrySubdvisionServiceId = propertySupportedNew.CountrySubdvisionServiceId;

                            _billingInvoicePropertySupportedTypeAppService.Update(propertySupportedOld.Id, propertySupportedOld);
                            _billingInvoicePropertyRepository.SaveChanges();
                        }
                        else if (propertySupportedOld == null)
                        {
                            propertySupportedTypeForCreateList.Add(propertySupportedNew);
                        }
                    }

                    if (propertySupportedTypeForCreateList != null && propertySupportedTypeForCreateList.Count() > 0)
                        _billingInvoicePropertySupportedTypeAppService.BillingInvoicePropertySupportedTypeCreate(propertySupportedTypeForCreateList, billingInvoicePropertyDto.Id);
                }
            }
        }

        public async Task CancelBillingInvoiceAsync(int propertyId, Guid billingInvoiceId, int reasonId)
        {
            ValidateReasonOfBillingInvoiceCancel(reasonId);
            if(Notification.HasNotification())
                return;

            var billingInvoice = await _billingInvoiceRepository.GetById(billingInvoiceId);

            ValidateBillingAccountOfInvoice(billingInvoiceId);
            if (Notification.HasNotification())
                return;

            var cancelInvoice = await _nFEIntegration.CancelInvoice(billingInvoiceId, billingInvoice.BillingInvoiceTypeId.Value);

            if (cancelInvoice)
            {
               billingInvoice.SetIntegrationCancelPending(_applicationUser.UserUid, reasonId);
               await _billingInvoiceRepository.SaveChanges();
            }
        }

        private void ValidateReasonOfBillingInvoiceCancel(int reasonId)
        {
            if (!_reasonReadRepository.CheckIfReasonIsBillingInvoiceCancel(reasonId))
            {
                Notification.Raise(Notification
                .DefaultBuilder
                .WithMessage(AppConsts.LocalizationSourceName, BillingInvoiceEnum.Error.BillingInvoiceInvalidReason)
                .Build());
            }
        }

        private void ValidateEmissionDateOfBillingInvoiceCancel(int propertyId, BillingInvoice billingInvoice)
        {
            if (billingInvoice.EmissionDate.Month != DateTime.UtcNow.ToZonedDateTimeLoggedUser(_applicationUser).Month)
            {
                Notification.Raise(Notification
                .DefaultBuilder
                .WithMessage(AppConsts.LocalizationSourceName, BillingInvoiceEnum.Error.BillingInvoiceInvalidEmissionDateOfCancel)
                .Build());
            }
        }

        private void ValidateBillingAccountOfInvoice(Guid billingInvoiceId)
        {
            if (BillingAccountIsClosed(billingInvoiceId))
            {
                Notification.Raise(Notification
                .DefaultBuilder
                .WithMessage(AppConsts.LocalizationSourceName, BillingInvoiceEnum.Error.BillingInvoiceInvalidAccountClosed)
                .Build());
            }
        }

        private bool BillingAccountIsClosed(Guid billingInvoiceId)
        {
            var billingAccount = _billingAccountRepository.GetBillingAccountByInvoiceId(billingInvoiceId);
            return billingAccount.StatusId == (int) AccountBillingStatusEnum.Closed;
        }
    }
}
