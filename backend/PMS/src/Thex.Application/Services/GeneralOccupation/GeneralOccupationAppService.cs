﻿using System;
using Thex.Application.Adapters;
using Thex.Domain.Entities;
using Thex.Application.Interfaces;
using Tnf.Domain.Services;
using Thex.Dto;
using System.Threading.Tasks;
using Tnf.Dto;
using Tnf.Localization;
using Thex.Common;
using Thex.Common.Enumerations;
using Tnf.Notifications;

namespace Thex.Application.Services
{
    public class GeneralOccupationAppService : ScaffoldGeneralOccupationAppService, IGeneralOccupationAppService
    {
        private readonly ILocalizationManager _localizationManager;

        public GeneralOccupationAppService(
            IGeneralOccupationAdapter generalOccupationAdapter,
            IDomainService<GeneralOccupation> generalOccupationDomainService,
            ILocalizationManager localizationManager,
            INotificationHandler notificationHandler)
            : base(generalOccupationAdapter, generalOccupationDomainService, notificationHandler)
        {
            _localizationManager = localizationManager;
        }

        public override async Task<IListDto<GeneralOccupationDto>> GetAll(GetAllGeneralOccupationDto request)
        {
            ValidateRequestAllDto(request, nameof(request));

            if (Notification.HasNotification())
                return null;

            var response = await GeneralOccupationDomainService.GetAllAsync<GeneralOccupationDto>(request).ConfigureAwait(false);

            if (response.Items != null)
                foreach (var generalOccupations in response.Items)
                    generalOccupations.Occupation = _localizationManager.GetString(AppConsts.LocalizationSourceName, ((GeneralOccupationsEnum)generalOccupations.Id).ToString());

            return response;
        }
    }
}
