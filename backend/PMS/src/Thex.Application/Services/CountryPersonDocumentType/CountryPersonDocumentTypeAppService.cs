﻿using System;
using Thex.Application.Adapters;
using Thex.Domain.Entities;
using Thex.Application.Interfaces;
using Tnf.Domain.Services;
using Tnf.Notifications;

namespace Thex.Application.Services
{
    public class CountryPersonDocumentTypeAppService : ScaffoldCountryPersonDocumentTypeAppService, ICountryPersonDocumentTypeAppService
    {
        public CountryPersonDocumentTypeAppService(
            ICountryPersonDocumentTypeAdapter countryPersonDocumentTypeAdapter,
            IDomainService<CountryPersonDocumentType> countryPersonDocumentTypeDomainService,
            INotificationHandler notificationHandler)
            : base(countryPersonDocumentTypeAdapter, countryPersonDocumentTypeDomainService, notificationHandler)
        {
        }
    }
}
