﻿using Thex.Application.Adapters;
using Thex.Domain.Entities;
using Thex.Application.Interfaces;
using Tnf.Domain.Services;
using Tnf.Notifications;
using Thex.Dto;
using Thex.Infra.ReadInterfaces;
using Thex.Kernel;
using System.Linq;
using Thex.Common.Enumerations;
using Tnf.Dto;
using Thex.Common.Helpers;
using Thex.Common;

namespace Thex.Application.Services
{
    public class BillingInvoiceModelAppService : ScaffoldBillingInvoiceModelAppService, IBillingInvoiceModelAppService
    {
        private readonly IBillingInvoiceModelReadRepository _billingInvoiceModelReadRepository;
        private readonly IApplicationUser _applicationUser;
        private readonly INotificationHandler _notificationHandler;

        public BillingInvoiceModelAppService(
            IBillingInvoiceModelAdapter billingInvoiceModelAdapter,
            IDomainService<BillingInvoiceModel> billingInvoiceModelDomainService,
            IBillingInvoiceModelReadRepository billingInvoiceModelReadRepository,
            IApplicationUser applicationUser,
            INotificationHandler notificationHandler)
            : base(billingInvoiceModelAdapter, billingInvoiceModelDomainService, notificationHandler)
        {
            _billingInvoiceModelReadRepository = billingInvoiceModelReadRepository;
            _applicationUser = applicationUser;
            _notificationHandler = notificationHandler;
        }

        public IListDto<BillingInvoiceModelDto> GetAllVisible()
        {
            var billingInvoiceModelList = _billingInvoiceModelReadRepository.GetAllVisible();

            switch (EnumHelper.GetEnumValue<CountryIsoCodeEnum>(_applicationUser.PropertyCountryCode, true))
            {
                case CountryIsoCodeEnum.Brasil:
                    {
                        billingInvoiceModelList.Items = billingInvoiceModelList.Items.Where(b => b.BillingInvoiceTypeId == (int)BillingInvoiceTypeEnum.NFCE ||
                                                                                                 b.BillingInvoiceTypeId == (int)BillingInvoiceTypeEnum.NFE ||
                                                                                                 b.BillingInvoiceTypeId == (int)BillingInvoiceTypeEnum.NFSE ||
                                                                                                 b.BillingInvoiceTypeId == (int)BillingInvoiceTypeEnum.SAT).ToList();
                        break;
                    }
                case CountryIsoCodeEnum.Europe:
                    {
                        billingInvoiceModelList.Items = billingInvoiceModelList.Items.Where(b => b.BillingInvoiceTypeId == (int)BillingInvoiceTypeEnum.Factura).ToList();
                        break;
                    }
                case CountryIsoCodeEnum.Latam:
                default:
                    {
                        // TODO: Criar a mensagem de erro corretamente
                        _notificationHandler.Raise(_notificationHandler.DefaultBuilder
                                                                 .WithMessage(AppConsts.LocalizationSourceName, BillingInvoiceEnum.Error.BillingInvoiceInvalidAccountClosed)
                                                                 .WithDetailedMessage(string.Concat(_applicationUser.PropertyCountryCode, AppConsts.LocalizationSourceName, BillingInvoiceEnum.Error.BillingInvoiceInvalidAccountClosed))
                                                                 .Build());

                        break;
                    }
            }

            return billingInvoiceModelList;
        }
    }
}
