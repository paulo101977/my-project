﻿using System;
using Thex.Application.Adapters;
using Thex.Domain.Entities;
using Thex.Application.Interfaces;
using Tnf.Domain.Services;
using Tnf.Notifications;

namespace Thex.Application.Services
{
    public class PropertyBusinessPartnerAppService : ScaffoldPropertyBusinessPartnerAppService, IPropertyBusinessPartnerAppService
    {
        public PropertyBusinessPartnerAppService(
            IPropertyBusinessPartnerAdapter propertyBusinessPartnerAdapter,
            IDomainService<PropertyBusinessPartner> propertyBusinessPartnerDomainService,
            INotificationHandler notificationHandler)
            : base(propertyBusinessPartnerAdapter, propertyBusinessPartnerDomainService, notificationHandler)
        {
        }
    }
}
