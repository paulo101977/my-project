﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Thex.Application.Adapters;
using Thex.Application.Interfaces;
using Thex.Common;
using Thex.Common.Dto.Observable;
using Thex.Common.Interfaces.Observables;
using Thex.Domain.Entities;
using Thex.Domain.Interfaces.Repositories;
using Thex.Domain.Interfaces.Services;
using Thex.Domain.Services.Interfaces;
using Thex.Dto;
using Thex.Dto.Base;
using Thex.Dto.Housekeeping;
using Thex.Dto.Room;
using Thex.Dto.Rooms;
using Thex.EntityFrameworkCore.Utils;
using Thex.Infra.ReadInterfaces;
using Thex.Inventory.Domain.Services;
using Thex.Kernel;
using Tnf.Dto;
using Tnf.Notifications;
using Tnf.Repositories.Uow;

namespace Thex.Application.Services
{
    public class RoomAppService : ScaffoldRoomAppService, IRoomAppService
    {
        private readonly EntityFrameworkCore.ReadInterfaces.Repositories.IRoomReadRepository _roomReadRepository;
        private readonly IRoomDomainService _roomDomainService;
        private readonly IRoomTypeDomainService _roomTypeDomainService;
        private readonly IHousekeepingStatusPropertyReadRepository _housekeepingStatusPropertyReadRepository;
        private readonly IPropertyBaseRateReadRepository _propertyBaseRateReadRepository;
        private readonly IReservationItemDomainService _reservationItemDomainService;
        private readonly IGuestReservationItemAppService _guestReservationItemAppService;
        private readonly IReservationItemReadRepository _reservationItemReadRepository;
        private readonly IReservationBudgetDomainService _reservationBudgetDomainService;
        private readonly IPropertyParameterReadRepository _propertyParameterReadRepository;
        private readonly IRoomBlockingReadRepository _roomBlockingReadRepository;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IApplicationUser _applicationUser;
        private readonly IPropertyContractReadRepository _propertyContractReadRepository;
        private readonly IRoomRepository _roomRepository;
        private readonly IIgnoreAuditedEntity _ignoreAuditedEntity;
        private IList<ICreateRoomObservable> _createRoomObservableList;
        private IList<IRoomChangeRoomTypeObservable> _roomChangeRoomTypeObservableList;
        private IList<IDeleteRoomObservable> _deleteRoomObservableList;

        public RoomAppService(
            IRoomAdapter roomAdapter,
            EntityFrameworkCore.ReadInterfaces.Repositories.IRoomReadRepository roomReadRepository,
            IRoomDomainService roomDomainService,
            IHousekeepingStatusPropertyReadRepository housekeepingStatusPropertyReadRepository,
            IPropertyBaseRateReadRepository propertyBaseRateReadRepository,
            IReservationItemDomainService reservationItemDomainService,
            IGuestReservationItemAppService guestReservationItemAppService,
            IReservationItemReadRepository reservationItemReadRepository,
            IRoomTypeDomainService roomTypeDomainService,
            IReservationBudgetDomainService reservationBudgetDomainService,
            IPropertyParameterReadRepository propertyParameterReadRepository,
            INotificationHandler notificationHandler,
            IRoomBlockingReadRepository roomBlockingReadRepository,
            IServiceProvider serviceProvider,
            IUnitOfWorkManager unitOfWorkManager,
            IPropertyContractReadRepository propertyContractReadRepository,
            IIgnoreAuditedEntity ignoreAuditedEntity,
            IRoomRepository roomRepository,
            IApplicationUser applicationUser)
            : base(roomAdapter, roomDomainService, notificationHandler)
        {
            _roomReadRepository = roomReadRepository;
            _roomDomainService = roomDomainService;
            _housekeepingStatusPropertyReadRepository = housekeepingStatusPropertyReadRepository;
            _propertyBaseRateReadRepository = propertyBaseRateReadRepository;
            _reservationItemDomainService = reservationItemDomainService;
            _guestReservationItemAppService = guestReservationItemAppService;
            _reservationItemReadRepository = reservationItemReadRepository;
            _roomTypeDomainService = roomTypeDomainService;
            _reservationBudgetDomainService = reservationBudgetDomainService;
            _propertyParameterReadRepository = propertyParameterReadRepository;
            _roomBlockingReadRepository = roomBlockingReadRepository;
            _unitOfWorkManager = unitOfWorkManager;
            _applicationUser = applicationUser;
            _propertyContractReadRepository = propertyContractReadRepository;
            _roomRepository = roomRepository;
            _ignoreAuditedEntity = ignoreAuditedEntity;

            SetObservables(serviceProvider);
        }

        private void SetObservables(IServiceProvider serviceProvider)
        {
            _createRoomObservableList = new List<ICreateRoomObservable>();
            var serviceForCreateRoomObservable = serviceProvider.GetServices<ICreateRoomObservable>().First(o => o.GetType() == typeof(RoomTypeInventoryDomainService));
            _createRoomObservableList.Add(serviceForCreateRoomObservable);

            _roomChangeRoomTypeObservableList = new List<IRoomChangeRoomTypeObservable>();
            var serviceForRoomChangeRoomTypeObservable = serviceProvider.GetServices<IRoomChangeRoomTypeObservable>().First(o => o.GetType() == typeof(RoomTypeInventoryDomainService));
            _roomChangeRoomTypeObservableList.Add(serviceForRoomChangeRoomTypeObservable);

            _deleteRoomObservableList = new List<IDeleteRoomObservable>();
            var serviceForDeleteRoomObservable = serviceProvider.GetServices<IDeleteRoomObservable>().First(o => o.GetType() == typeof(RoomTypeInventoryDomainService));
            _deleteRoomObservableList.Add(serviceForDeleteRoomObservable);
        }

        public RoomDto CreateRoom(RoomDto roomDto)
        {
            using (var uow = _unitOfWorkManager.Begin())
            {
                if (roomDto == null)
                {
                    NotifyNullParameter();
                    return RoomDto.NullInstance;
                }
                ValidateDto(roomDto, nameof(roomDto));

                if (Notification.HasNotification())
                    return RoomDto.NullInstance;

                Room.Builder builder = new Room.Builder(Notification);

                var housekeepingStatusIdDirty = _housekeepingStatusPropertyReadRepository.GetHousekeepingStatusIdDirty();

                if (Notification.HasNotification())
                    return RoomDto.NullInstance;

                builder
                    .WithId(roomDto.Id)
                    .WithParentRoomId(roomDto.ParentRoomId)
                    .WithPropertyId(roomDto.PropertyId)
                    .WithRoomTypeId(roomDto.RoomTypeId)
                    .WithBuilding(roomDto.Building)
                    .WithWing(roomDto.Wing)
                    .WithFloor(roomDto.Floor)
                    .WithRoomNumber(roomDto.RoomNumber)
                    .WithRemarks(roomDto.Remarks)
                    .WithIsActive(true)
                    .WithHousekeepingStatusLastModificationTime(DateTime.UtcNow.ToZonedDateTimeLoggedUser())
                    .WithHousekeepingStatusPropertyId(housekeepingStatusIdDirty);

                ValidatePropertyContract();

                if (Notification.HasNotification())
                    return RoomDto.NullInstance;

                roomDto.Id = _roomDomainService.Create(builder, roomDto.ChildRoomIdList);

                RiseActionsAfterCreateRoom(roomDto.RoomTypeId);
                if (Notification.HasNotification()) return null;

                uow.Complete();
            }

            return roomDto;
        }

        public void DeleteRoom(int id)
        {
            _ignoreAuditedEntity.SetEntityName(typeof(Room).Name);

            try
            {
                using (var uow = _unitOfWorkManager.Begin())
                {
                    if (id < 0)
                        NotifyIdIsMissing();

                    if (!ValidateId(id)) return;

                    _roomDomainService.Delete(e => e.Id == id);

                    RiseActionsAfterDeleteRom(_roomReadRepository.GetRoomTypeIdByRoomId(id) ?? default(int));
                    if (Notification.HasNotification()) return;

                    uow.Complete();
                }
            }
            catch (DbUpdateException ex)
            {
                Notification.Raise(Notification.DefaultBuilder
                   .WithMessage(AppConsts.LocalizationSourceName, Room.EntityError.RoomCanNotBeDeleted)
                   .WithDetailedMessage(AppConsts.LocalizationSourceName, Room.EntityError.RoomCanNotBeDeleted)
                   .Build());
            }
        }

        public RoomDto GetRoom(DefaultIntRequestDto id)
        {
            if (id.Id < 0)
                NotifyIdIsMissing();

            if (!ValidateRequestDto(id) || !ValidateId<int>(id.Id)) return null;

            if (Notification.HasNotification())
                return RoomDto.NullInstance;

            Room room = _roomDomainService.Get(id);

            var roomDto = room.MapTo<RoomDto>();

            var childrenIds = _roomDomainService.GetChildrenIds(id.Id);

            if (childrenIds.Any())
            {
                roomDto.ChildRoomIdList = childrenIds;
            }

            return roomDto;
        }

        public IListDto<RoomDto> GetRoomsByPropertyId(GetAllRoomsDto request, int propertyId)
        {
            if (propertyId < 0)
                NotifyIdIsMissing();

            ValidateRequestAllDto(request, nameof(request));

            if (Notification.HasNotification())
                return null;

            var rooms = _roomReadRepository.GetRoomsByPropertyId(request, propertyId);
            return rooms;
        }

        public IListDto<RoomDto> GetChildRoomsByParentRoomId(GetAllRoomsDto request, int parentRoomId)
        {
            if (parentRoomId < 0)
                NotifyIdIsMissing();

            ValidateRequestAllDto(request, nameof(request));

            if (Notification.HasNotification())
                return null;

            IListDto<RoomDto> rooms = _roomReadRepository.GetChildRoomsByParentRoomId(request, parentRoomId);
            return rooms;
        }

        public IListDto<RoomDto> GetParentRoomsByPropertyId(GetAllRoomsDto request, int propertyId)
        {
            if (propertyId < 0)
                NotifyIdIsMissing();

            ValidateRequestAllDto(request, nameof(request));

            if (Notification.HasNotification())
                return null;

            var rooms = _roomReadRepository.GetParentRoomsByPropertyId(request, propertyId);
            return rooms;
        }

        public IListDto<RoomDto> GetRoomsWithoutChildrenByPropertyId(GetAllRoomsDto request, int propertyId)
        {
            if (propertyId < 0)
                NotifyIdIsMissing();

            ValidateRequestAllDto(request, nameof(request));

            if (Notification.HasNotification())
                return null;

            var rooms = _roomReadRepository.GetRoomsWithoutChildrenByPropertyId(request, propertyId);
            return rooms;
        }

        public IListDto<RoomDto> GetRoomsWithoutParentAndChildrenByPropertyId(GetAllRoomsDto request, int propertyId)
        {
            if (propertyId < 0)
                NotifyIdIsMissing();

            ValidateRequestAllDto(request, nameof(request));

            if (Notification.HasNotification())
                return null;

            var rooms = _roomReadRepository.GetRoomsWithoutParentAndChildrenByPropertyId(request, propertyId);
            return rooms;
        }

        public RoomDto UpdateRoom(int id, RoomDto roomDto)
        {
            using (var uow = _unitOfWorkManager.Begin())
            {
                if (roomDto == null || !roomDto.HousekeepingStatusPropertyId.HasValue)
                {
                    NotifyNullParameter();
                    return RoomDto.NullInstance;
                }
                if (roomDto.Id <= 0)
                    NotifyIdIsMissing();

                ValidateDtoAndId(roomDto, id, nameof(roomDto), nameof(id));

                if (Notification.HasNotification())
                    return RoomDto.NullInstance;

                ValidateRoomTypeCouldNotBeChange(roomDto.Id, roomDto.RoomTypeId);

                if (Notification.HasNotification())
                    return RoomDto.NullInstance;

                Room.Builder builder = new Room.Builder(Notification);

                builder
                    .WithId(roomDto.Id)
                    .WithParentRoomId(roomDto.ParentRoomId)
                    .WithPropertyId(roomDto.PropertyId)
                    .WithRoomTypeId(roomDto.RoomTypeId)
                    .WithBuilding(roomDto.Building)
                    .WithWing(roomDto.Wing)
                    .WithFloor(roomDto.Floor)
                    .WithRoomNumber(roomDto.RoomNumber)
                    .WithRemarks(roomDto.Remarks)
                    .WithIsActive(roomDto.IsActive)
                    .WithHousekeepingStatusLastModificationTime(DateTime.UtcNow.ToZonedDateTimeLoggedUser())
                    .WithHousekeepingStatusPropertyId(roomDto.HousekeepingStatusPropertyId.Value);

                var roomTypeIdOld = _roomDomainService.Get(new DefaultIntRequestDto { Id = roomDto.Id }).RoomTypeId;

                _roomDomainService.Update(builder, roomDto.ChildRoomIdList);

                if (ChangeRoomType(roomTypeIdOld, roomDto.RoomTypeId) && builder.Build().IsActive)
                {
                    RiseActionsAfterRoomChangeRoomType(roomTypeIdOld, roomDto.RoomTypeId);
                    if (Notification.HasNotification()) return null;
                }

                uow.Complete();
            }

            return roomDto;
        }

        private void ValidateRoomTypeCouldNotBeChange(int roomId, int roomTypeId)
        {
            if (_roomReadRepository.RoomTypeCouldNotBeChanged(roomId, roomTypeId))
            {
                Notification.Raise(Notification.DefaultBuilder
               .WithMessage(AppConsts.LocalizationSourceName, Room.EntityError.RoomTypeCouldNotBeChange)
               .WithDetailedMessage(AppConsts.LocalizationSourceName, Room.EntityError.RoomTypeCouldNotBeChange)
               .Build());
            }
        }

        private bool ChangeRoomType(int roomTypeIdOld, int roomTypeIdNew)
        {
            return roomTypeIdOld != roomTypeIdNew;
        }

        public void ToggleActivation(int id)
        {
            using (var uow = _unitOfWorkManager.Begin())
            {
                if (id < 0)
                {
                    NotifyIdIsMissing();
                    return;
                }

                var isActive = _roomDomainService.ToggleActivation(id);

                if (Notification.HasNotification())
                    return;

                if (!isActive)
                {
                    ValidateIfRoomIsInCheckIn(id);

                    if (Notification.HasNotification())
                        return;

                    CleanAllRoomIdInReservationItem(id);
                }

                if (isActive)
                    RiseActionsAfterCreateRoom(_roomDomainService.Get(new DefaultIntRequestDto { Id = id }).RoomTypeId);
                else
                    RiseActionsAfterDeleteRom(_roomDomainService.Get(new DefaultIntRequestDto { Id = id }).RoomTypeId);

                if (Notification.HasNotification()) return;

                uow.Complete();
            }
        }

        private void CleanAllRoomIdInReservationItem(int id)
        {
            _roomRepository.CleanAllRoomIdInReservationItem(id);
        }

        private void ValidateIfRoomIsInCheckIn(int id)
        {
            if (_roomReadRepository.ValidateIfRoomIsInCheckIn(id))
            {
                Notification.Raise(Notification.DefaultBuilder
               .WithMessage(AppConsts.LocalizationSourceName, Room.EntityError.RoomIsInCheckIn)
               .WithDetailedMessage(AppConsts.LocalizationSourceName, Room.EntityError.RoomIsInCheckIn)
               .Build());
            }
        }

        public IListDto<RoomDto> GetAllRoomsByPropertyAndPeriod(GetAllRoomsByPropertyAndPeriodDto request, int propertyId)
        {
            if (request.InitialDate > request.FinalDate)
                NotifyInvalidRangeDate();

            if (propertyId < 0)
                NotifyIdIsMissing();

            ValidateRequestAllDto(request, nameof(request));

            if (Notification.HasNotification())
                return null;

            IListDto<RoomDto> rooms = _roomReadRepository.GetAllRoomsByPropertyAndPeriod(request, propertyId);
            return rooms;
        }

        public IListDto<RoomDto> GetAllAvailableByPeriodOfRoomType(PeriodDto request, int propertyId, int roomTypeId, int? reservationItemId, bool isCheckin)
        {
            var systemDate = _propertyParameterReadRepository.GetSystemDate(propertyId);

            ValidateSystemDate(systemDate);

            if (Notification.HasNotification()) return null;

            request.InitialDate = systemDate.GetValueOrDefault()
                .AddHours(request.InitialDate.Hour)
                .AddMinutes(request.InitialDate.Minute)
                .AddSeconds(request.InitialDate.Second);

            if (request.InitialDate.Date > request.FinalDate.Date)
                NotifyInvalidRangeDate();

            if (roomTypeId <= 0 || propertyId <= 0)
                NotifyIdIsMissing();

            ValidateRequestAllDto(request, nameof(request));

            if (Notification.HasNotification())
                return null;

            var isAvailable = true;

            if (isCheckin && reservationItemId.HasValue)
            {
                var roomId = _reservationItemReadRepository.GetById(reservationItemId.Value).RoomId;
                if (roomId.HasValue)
                    isAvailable = RoomIsAvailable(propertyId, roomId.Value, reservationItemId.Value, systemDate.GetValueOrDefault());
            }

            return _roomReadRepository.GetAllAvailableByPeriodOfRoomType(request, propertyId, roomTypeId, reservationItemId, isCheckin, isAvailable);
        }

        private void ValidateSystemDate(DateTime? systemDate)
        {
            if (systemDate == null)
            {
                Notification.Raise(Notification.DefaultBuilder
                .WithMessage(AppConsts.LocalizationSourceName, PropertyAuditProcess.EntityError.PropertyAuditProcessInvalidPropertySystemDate)
                .WithDetailedMessage(AppConsts.LocalizationSourceName, PropertyAuditProcess.EntityError.PropertyAuditProcessInvalidPropertySystemDate)
                .Build());
            }
        }

        public List<int> GetAllOccupedRoomsByPropertyAndPeriod(DateTime initialDate, DateTime endDate, int propertyId)
        {
            return _roomReadRepository.GetAllOccupedRoomsByPropertyAndPeriod(initialDate, endDate, propertyId);
        }

        public IListDto<GetAllHousekeepingAlterStatusDto> GetAllRoomWithHousekeepingStatus(int propertyId)
        {
            return _roomReadRepository.GetAllRoomWithHousekeepingStatus(propertyId);
        }

        public IListDto<RoomDto> GetAllStatusCheckinRooms(GetAllRoomsByPropertyAndPeriodDto request, int propertyId)
        {
            return _roomReadRepository.GetAllStatusCheckinRooms(request, propertyId);
        }

        public async Task<BudgetCurrentOrFutureDto> GetCurrentOrFutureBudgetByReservationItem(int propertyId, int roomTypeId, DateTime initialDate, DateTime finalDate, long reservationItemId)
        {
            var roomTypeIdList = new List<int>() { roomTypeId };

            var baseRateListTask = _propertyBaseRateReadRepository.GetAllByPeriod(propertyId, roomTypeIdList, null, null, initialDate, finalDate);
            var childParameterList = await _propertyParameterReadRepository.GetChildrenAgeGroupList(propertyId);

            var roomType = _roomTypeDomainService.Get(new DefaultIntRequestDto(roomTypeId), false, false);


            var baseRateList = await baseRateListTask;

            var ChildrenAge = _reservationItemReadRepository.GetGuestReservationItemByReservationItemId(reservationItemId).Where(x => x.IsChild).Select(x => (int)x.ChildAge).ToList();

            DefaultLongRequestDto request = null;
            request.Id = reservationItemId;
            var reservationItem = _reservationItemDomainService.Get(request);

            var date = initialDate.Date;
            decimal currentBudget = 0;

            while (date <= finalDate.Date)
            {
                var baseRate = baseRateList.FirstOrDefault(e => e.Date == date);

                if (baseRate != null)
                {
                    var baseRateResult = _reservationBudgetDomainService.CalculateBaseRate(reservationItem.AdultCount, ChildrenAge, childParameterList, baseRate, roomType);

                    currentBudget += baseRateResult.Total;
                }

            }
            return new BudgetCurrentOrFutureDto
            {
                Id = reservationItemId,
                Budget = currentBudget

            };

        }

        public bool RoomIsAvailable(int propertyId, int roomId, long reservationItemId, DateTime systemDate)
        {
            var roomBlockedList = _roomBlockingReadRepository.GetAllBlockedInPeriod(systemDate, systemDate, roomId).Select(x => x.RoomId).ToList();
            var roomIsAvailable = _roomReadRepository.IsAvailableExceptReservationItemId(propertyId, roomId, reservationItemId);

            return !roomBlockedList.Any(e => e == roomId) && roomIsAvailable;
        }


        public virtual async Task<RoomsAvailableDto> GetTotalAndAvailableRoomsByPropertyAsync()
        {
            var totalUsedRoomsTask = _roomReadRepository.CountByPropertyIdAsync(int.Parse(_applicationUser.PropertyId));
            var totalRoomsTask = _propertyContractReadRepository.GetTotalRoomsByPropertyIdAsync(int.Parse(_applicationUser.PropertyId));

            await Task.WhenAll(totalUsedRoomsTask, totalRoomsTask);

            return new RoomsAvailableDto
            {
                Rooms = totalUsedRoomsTask.Result,
                TotalRooms = totalRoomsTask.Result ?? default(int)
            };
        }

        private void RiseActionsAfterCreateRoom(int roomTypeId)
        {
            foreach (var observable in _createRoomObservableList)
            {
                var dtoObservable = new CreateRoomObservableDto
                {
                    InitialDate = _propertyParameterReadRepository.GetSystemDate(Convert.ToInt32(_applicationUser.PropertyId)).Value,
                    RoomTypeId = roomTypeId,
                    PropertyId = Convert.ToInt32(_applicationUser.PropertyId)
                };

                observable.ExecuteActionAfterCreateRoom(dtoObservable);
            }
        }

        private void RiseActionsAfterRoomChangeRoomType(int roomTypeIdOld, int roomTypeIdNew)
        {
            foreach (var observable in _roomChangeRoomTypeObservableList)
            {
                var dtoObservable = new RoomChangeRoomTypeObservableDto
                {
                    InitialDate = _propertyParameterReadRepository.GetSystemDate(Convert.ToInt32(_applicationUser.PropertyId)).Value,
                    RoomTypeIdOld = roomTypeIdOld,
                    RoomTypeIdNew = roomTypeIdNew,
                    PropertyId = Convert.ToInt32(_applicationUser.PropertyId)
                };

                observable.ExecuteActionAfterRoomChangeRoomType(dtoObservable);
            }
        }

        private void RiseActionsAfterDeleteRom(int roomTypeId)
        {
            foreach (var observable in _deleteRoomObservableList)
            {
                var dtoObservable = new DeleteRoomObservableDto
                {
                    InitialDate = _propertyParameterReadRepository.GetSystemDate(int.Parse(_applicationUser.PropertyId)).Value,
                    RoomTypeId = roomTypeId,
                    PropertyId = int.Parse(_applicationUser.PropertyId)
                };

                observable.ExecuteActionAfterDeleteRoom(dtoObservable);
            }
        }

        private void ValidatePropertyContract()
        {
            var propertyId = int.Parse(_applicationUser.PropertyId);

            var propertyContract = _propertyContractReadRepository.GetPropertyContractByPropertyId(propertyId);

            var rooms = _roomReadRepository.GetRoomsByPropertyId(new GetAllRoomsDto(), propertyId);

            if (propertyContract == null)
            {
                Notification.Raise(Notification
               .DefaultBuilder
               .WithMessage(AppConsts.LocalizationSourceName, PropertyContract.EntityError.PropertyMustHaveContract)
               .Build());
            }
            else if (rooms.Items.Count >= propertyContract.MaxUh)
            {
                Notification.Raise(Notification
                .DefaultBuilder
                .WithMessage(AppConsts.LocalizationSourceName, Room.EntityError.RoomOutOfQuantity)
                .Build());
            }
        }
    }
}
