﻿using System;
using Thex.Application.Adapters;
using Thex.Domain.Entities;
using Thex.Application.Interfaces;
using Tnf.Domain.Services;
using Tnf.Notifications;

namespace Thex.Application.Services
{
    public class ChainAppService : ScaffoldChainAppService, IChainAppService
    {
        public ChainAppService(
            IChainAdapter chainAdapter,
            IDomainService<Chain> chainDomainService,
            INotificationHandler notificationHandler)
            : base(chainAdapter, chainDomainService, notificationHandler)
        {
        }
    }
}
