﻿using System;
using Thex.Application.Adapters;
using Thex.Domain.Entities;
using Thex.Application.Interfaces;
using Tnf.Domain.Services;
using Tnf.Notifications;

namespace Thex.Application.Services
{
    public class PlasticBrandAppService : ScaffoldPlasticBrandAppService, IPlasticBrandAppService
    {
        public PlasticBrandAppService(
            IPlasticBrandAdapter plasticBrandAdapter,
            IDomainService<PlasticBrand> plasticBrandDomainService,
            INotificationHandler notificationHandler)
            : base(plasticBrandAdapter, plasticBrandDomainService, notificationHandler)
        {
        }
    }
}
