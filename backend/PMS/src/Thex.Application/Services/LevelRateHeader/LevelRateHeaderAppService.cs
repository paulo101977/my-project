﻿using System;
using Thex.Application.Adapters;
using Thex.Domain.Entities;
using Thex.Application.Interfaces;
using Tnf.Notifications;
using Tnf.Domain.Services;

namespace Thex.Application.Services
{
    public class LevelRateHeaderAppService : ScaffoldLevelRateHeaderAppService, ILevelRateHeaderAppService
    {
        public LevelRateHeaderAppService(
            ILevelRateHeaderAdapter levelRateHeaderAdapter,
            IDomainService<LevelRateHeader> levelRateHeaderDomainService,
            INotificationHandler notificationHandler)
                : base(levelRateHeaderAdapter, levelRateHeaderDomainService, notificationHandler)
        {
        }
    }
}
