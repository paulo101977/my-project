﻿using Thex.Application.Interfaces.RoomStatus;
using Thex.Dto;
using Thex.Dto.RoomStatus;
using Thex.Infra.ReadInterfaces;
using Tnf.Dto;
using Tnf.Notifications;

namespace Thex.Application.Services.RoomStatus
{
    public class RoomStatusAppService : ApplicationServiceBase, IRoomStatusAppService
    {
        private readonly IRoomStatusReadRepository _roomStatusReadRepository;

        public RoomStatusAppService(
            IRoomStatusReadRepository roomStatusReadRepository,
            INotificationHandler notificationHandler)
            : base(notificationHandler)
        {
            _roomStatusReadRepository = roomStatusReadRepository;
        }

        public ListDto<RoomStatusDto> GetAll(GetAllRoomStatusDto getAllRoomStatusDto, int propertyId)
        {
            return _roomStatusReadRepository.GetAll(getAllRoomStatusDto, propertyId);
        }
    }
}
