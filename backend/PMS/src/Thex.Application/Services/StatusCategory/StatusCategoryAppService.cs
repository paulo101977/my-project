﻿using System;
using Thex.Application.Adapters;
using Thex.Domain.Entities;
using Thex.Application.Interfaces;
using Tnf.Domain.Services;
using Tnf.Notifications;

namespace Thex.Application.Services
{
    public class StatusCategoryAppService : ScaffoldStatusCategoryAppService, IStatusCategoryAppService
    {
        public StatusCategoryAppService(
            IStatusCategoryAdapter statusCategoryAdapter,
            IDomainService<StatusCategory> statusCategoryDomainService,
            INotificationHandler notificationHandler)
            : base(statusCategoryAdapter, statusCategoryDomainService, notificationHandler)
        {
        }
    }
}
