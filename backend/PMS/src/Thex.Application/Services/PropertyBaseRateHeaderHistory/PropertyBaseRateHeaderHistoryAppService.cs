﻿using System;
using Thex.Application.Adapters;
using Thex.Domain.Entities;
using Thex.Application.Interfaces;
using Tnf.Domain.Services;
using Tnf.Notifications;
using Thex.Dto;
using System.Threading.Tasks;
using System.Collections.Generic;
using Thex.Infra.ReadInterfaces;

namespace Thex.Application.Services
{
    public class PropertyBaseRateHeaderHistoryAppService : ScaffoldPropertyBaseRateHeaderHistoryAppService, IPropertyBaseRateHeaderHistoryAppService
    {
        ICurrencyReadRepository _currencyReadRepository;
        public PropertyBaseRateHeaderHistoryAppService(
            IPropertyBaseRateHeaderHistoryAdapter propertyBaseRateHeaderHistoryAdapter,
            ICurrencyReadRepository currencyReadRepository,
            IDomainService<PropertyBaseRateHeaderHistory> propertyBaseRateHeaderHistoryDomainService,
            INotificationHandler notificationHandler)
                : base(propertyBaseRateHeaderHistoryAdapter, propertyBaseRateHeaderHistoryDomainService, notificationHandler)
        {
            _currencyReadRepository = currencyReadRepository;
        }

        public PropertyBaseRateHeaderHistory.Builder Create(BaseRateConfigurationDto dto)
        {
            ValidateDto<PropertyBaseRateHeaderHistoryDto>(dto, nameof(dto));

            if (Notification.HasNotification())
                return null;

            var propertyBaseRateHeaderHistoryBuilder = PropertyBaseRateHeaderHistoryAdapter.MapWithHistory(CreatePropertyBaseRateHeaderHistoryDto(dto));

            return propertyBaseRateHeaderHistoryBuilder;
        }

        private PropertyBaseRateHeaderHistoryDto CreatePropertyBaseRateHeaderHistoryDto(BaseRateConfigurationDto dto)
        {
            var headerHistoryDto = new PropertyBaseRateHeaderHistoryDto
            {
                CurrencyId = dto.CurrencyId,
                InitialDate = dto.StartDate,
                FinalDate = dto.FinalDate,
                MealPlanTypeDefaultId = dto.MealPlanTypeDefaultId,
                PropertyId = dto.PropertyId,
                RatePlanId = dto.RatePlanId,
                Sunday = dto.DaysOfWeekDto.Contains((int)DayOfWeek.Sunday),
                Monday = dto.DaysOfWeekDto.Contains((int)DayOfWeek.Monday),
                Tuesday = dto.DaysOfWeekDto.Contains((int)DayOfWeek.Tuesday),
                Wednesday = dto.DaysOfWeekDto.Contains((int)DayOfWeek.Wednesday),
                Thursday = dto.DaysOfWeekDto.Contains((int)DayOfWeek.Thursday),
                Friday = dto.DaysOfWeekDto.Contains((int)DayOfWeek.Friday),
                Saturday = dto.DaysOfWeekDto.Contains((int)DayOfWeek.Saturday),
                PropertyBaseRateHistoryList = new List<PropertyBaseRateHistoryDto>()
            };

            foreach (var baseRateRoomType in dto.BaseRateConfigurationRoomTypeList)
            {
                foreach (var baseMealPlanType in dto.BaseRateConfigurationMealPlanTypeList)
                {
                    var propertyBaseRateHistoryDto = CreatePropertyBaseRateHistoryDto(dto, baseRateRoomType, baseMealPlanType);

                    headerHistoryDto.PropertyBaseRateHistoryList.Add(propertyBaseRateHistoryDto);
                }
            }

            return headerHistoryDto;
        }

        private PropertyBaseRateHistoryDto CreatePropertyBaseRateHistoryDto(BaseRateConfigurationDto dto, BaseRateConfigurationRoomTypeDto baseRateRoomType, BaseRateConfigurationMealPlanTypeDto baseMealPlanType)
        {
            return new PropertyBaseRateHistoryDto
            {
                AdultMealPlanAmount = baseMealPlanType.AdultMealPlanAmount,
                Child1MealPlanAmount = baseMealPlanType.Child1MealPlanAmount,
                Child2MealPlanAmount = baseMealPlanType.Child2MealPlanAmount,
                Child3MealPlanAmount = baseMealPlanType.Child3MealPlanAmount,
                MealPlanTypeId = baseMealPlanType.MealPlanTypeId,
                Level = baseRateRoomType.Level,
                Pax1Amount = baseRateRoomType.Pax1Amount,
                Pax2Amount = baseRateRoomType.Pax2Amount,
                Pax3Amount = baseRateRoomType.Pax3Amount,
                Pax4Amount = baseRateRoomType.Pax4Amount,
                Pax5Amount = baseRateRoomType.Pax5Amount,
                PaxAdditionalAmount = baseRateRoomType.PaxAdditionalAmount,
                Child1Amount = baseRateRoomType.Child1Amount,
                Child2Amount = baseRateRoomType.Child2Amount,
                Child3Amount = baseRateRoomType.Child3Amount,
                MealPlanTypeDefault = dto.MealPlanTypeDefaultId,
                RoomTypeId = baseRateRoomType.RoomTypeId,
                CurrencySymbol = "",
                CurrencyId = dto.CurrencyId,
                PropertyId = dto.PropertyId,
                RatePlanId = dto.RatePlanId,
                PropertyBaseRateTypeId = dto.PropertyBaseRateTypeId,
                LevelId = dto.LevelId,
                LevelRateId = dto.LevelRateId,
                CurrencyName = _currencyReadRepository.GetNameCurrencyById(dto.CurrencyId).CurrencyName
            };
        }
    }
}
