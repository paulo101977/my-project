﻿using System;
using Thex.Application.Adapters;
using Thex.Domain.Entities;
using Thex.Application.Interfaces;
using Tnf.Domain.Services;
using Tnf.Notifications;

namespace Thex.Application.Services
{
    public class AgreementTypeAppService : ScaffoldAgreementTypeAppService, IAgreementTypeAppService
    {
        public AgreementTypeAppService(
            IAgreementTypeAdapter agreementTypeAdapter,
            IDomainService<AgreementType> agreementTypeDomainService,
            INotificationHandler notificationHandler)
            : base(agreementTypeAdapter, agreementTypeDomainService, notificationHandler)
        {
        }
    }
}
