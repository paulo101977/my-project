﻿using System;
using Thex.Application.Adapters;
using Thex.Domain.Entities;
using Thex.Application.Interfaces;
using Tnf.Domain.Services;
using Tnf.Notifications;

namespace Thex.Application.Services
{
    public class CurrencyExchangeAppService : ScaffoldCurrencyExchangeAppService, ICurrencyExchangeAppService
    {
        public CurrencyExchangeAppService(
            ICurrencyExchangeAdapter currencyExchangeAdapter,
            IDomainService<CurrencyExchange> currencyExchangeDomainService,
            INotificationHandler notificationHandler)
            : base(currencyExchangeAdapter, currencyExchangeDomainService, notificationHandler)
        {
        }
    }
}
