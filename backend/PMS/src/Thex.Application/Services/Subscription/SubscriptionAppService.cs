﻿using System;
using Thex.Application.Adapters;
using Thex.Domain.Entities;
using Thex.Application.Interfaces;
using Tnf.Domain.Services;
using Tnf.Notifications;

namespace Thex.Application.Services
{
    public class SubscriptionAppService : ScaffoldSubscriptionAppService, ISubscriptionAppService
    {
        public SubscriptionAppService(
            ISubscriptionAdapter subscriptionAdapter,
            IDomainService<Subscription> subscriptionDomainService,
            INotificationHandler notificationHandler)
            : base(subscriptionAdapter, subscriptionDomainService, notificationHandler)
        {
        }
    }
}
