﻿using System;
using Thex.Application.Adapters;
using Thex.Domain.Entities;
using Thex.Application.Interfaces;
using Tnf.Domain.Services;
using Tnf.Notifications;

namespace Thex.Application.Services
{
    public class CompanyClientContactPersonAppService : ScaffoldCompanyClientContactPersonAppService, ICompanyClientContactPersonAppService
    {
        public CompanyClientContactPersonAppService(
            ICompanyClientContactPersonAdapter companyClientContactPersonAdapter,
            IDomainService<CompanyClientContactPerson> companyClientContactPersonDomainService,
            INotificationHandler notificationHandler)
            : base(companyClientContactPersonAdapter, companyClientContactPersonDomainService, notificationHandler)
        {
        }
    }
}
