﻿using System;
using System.Collections.Generic;
using System.Linq;
using Thex.Application.Adapters;
using Thex.Application.Interfaces;
using Thex.Common;
using Thex.Common.Enumerations;
using Thex.Domain.Entities;
using Thex.Domain.Extensions;
using Thex.Domain.Helpers.ReservationCode;
using Thex.Domain.Rules;
using Thex.Domain.Services.Interfaces;
using Thex.Dto;
using Thex.Infra.ReadInterfaces;
using Tnf.Domain.Services;
using Tnf.Dto;

namespace Thex.Application.Services
{
    using Microsoft.Extensions.DependencyInjection;
    using System.Reflection;
    using Thex.Common.Dto.Observable;
    using Thex.Domain.Interfaces.Repositories;
    using Thex.Common.Interfaces.Observables;
    using Thex.Domain.Interfaces.Services;
    using Thex.Dto.Reservation;
    using Thex.Dto.ReservationCompanyClient;
    using Thex.Dto.ReservationGuest;
    using Thex.EntityFrameworkCore.ReadInterfaces.Repositories;
    using Thex.Inventory.Domain.Services;
    using Thex.Kernel;
    using Tnf.Localization;
    using Tnf.Notifications;
    using Tnf.Repositories.Uow;
    using System.Threading.Tasks;
    using Thex.Common.Helpers;

    public class ReservationAppService : ScaffoldReservationAppService, IReservationAppService
    {
        protected readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IGuestReservationItemAppService _guestReservationItemAppService;
        private readonly ICompanyClientReadRepository _companyClientReadRepository;
        private readonly ICompanyClientContactPersonReadRepository _companyClientContactPersonReadRepository;
        private readonly IReservationReadRepository _reservationReadRepository;
        private readonly EntityFrameworkCore.ReadInterfaces.Repositories.IRoomReadRepository _roomReadRepository;
        private readonly ICountrySubdivisionTranslationReadRepository _countrySubdivisionTranslationReadRepository;
        private readonly IReservationDomainService _reservationDomainService;
        private readonly IReservationConfirmationAppService _reservationConfirmationAppService;
        private readonly IDomainService<ReservationConfirmation> _reservationConfirmationDomainService;
        private readonly IPlasticAppService _plasticAppService;
        private readonly IDomainService<Plastic> _plasticDomainService;
        private readonly IPersonReadRepository _personReadRepository;
        private readonly IGuestAppService _guestAppService;
        private readonly IContactInformationAppService _contactInformationAppService;
        private readonly IDocumentReadRepository _documentReadRepository;
        private readonly IPropertyParameterReadRepository _propertyParameterReadRepository;
        private readonly IPropertyCurrencyExchangeReadRepository _propertyCurrencyExchangeReadRepository;
        private readonly IReservationBudgetAppService _reservationBudgetAppService;
        private readonly ILocalizationManager _localizationManager;
        private readonly IApplicationUser _applicationUser;
        private readonly IReservationItemStatusDomainService _reservationItemStatusDomainService;
        private readonly IRoomBlockingReadRepository _roomBlockingReadRepository;
        private readonly IReservationItemDomainService _reservationItemDomainService;
        private readonly IReservationItemReadRepository _reservationItemReadRepository;
        private readonly IRoomAppService _roomAppService;
        private readonly IBillingAccountReadRepository _billingAccountReadRepository;
        private readonly IGuestReservationItemRepository _guestItemRepository;
        private readonly IBillingAccountRepository _billingAccountRepository;
        private readonly IRoomTypeReadRepository _roomTypeReadRepository;
        private readonly IBillingAccountAppService _billingAccountAppService;
        private IList<IReservationItemChangeStatusObservable> _reservationItemChangeStatusObservableList;
        private IList<IReservationItemChangeRoomTypeOrPeriodObservable> _reservationItemChangeRoomTypeOrPeriodObservableList;
        private DateTime? _systemDate;
        private readonly IExternalRegistrationDomainService _externalRegistrationDomainService;

        public ReservationAppService(
            IUnitOfWorkManager unitOfWorkManager,
            IReservationAdapter reservationAdapter,
            IReservationDomainService reservationDomainService,
            IGuestReservationItemAppService guestReservationItemBuilderAppService,
            IReservationReadRepository reservationReadRepository,
            ICountrySubdivisionTranslationReadRepository countrySubdivisionTranslationReadRepository,
            EntityFrameworkCore.ReadInterfaces.Repositories.IRoomReadRepository roomReadRepository,
            ICompanyClientReadRepository companyClientReadRepository,
            ICompanyClientContactPersonReadRepository companyClientContactPersonReadRepository,
            IReservationConfirmationAppService reservationConfirmationAppService,
            IDomainService<ReservationConfirmation> reservationConfirmationDomainService,
            IDomainService<Plastic> plasticDomainService,
            IPlasticAppService plasticAppService,
            IPersonReadRepository personReadRepository,
            IGuestAppService guestAppService,
            IContactInformationAppService contactInformationAppService,
            IDocumentReadRepository documentReadRepository,
            IPropertyParameterReadRepository propertyParameterReadRepository,
            IPropertyCurrencyExchangeReadRepository propertyCurrencyExchangeReadRepository,
            IReservationBudgetAppService reservationBudgetAppService,
            ILocalizationManager localizationManager,
            IApplicationUser applicationUser,
            INotificationHandler notificationHandler,
            IReservationItemStatusDomainService reservationItemStatusDomainService,
            IRoomBlockingReadRepository roomBlockingReadRepository,
            IReservationItemDomainService reservationItemDomainService,
            IReservationItemReadRepository reservationItemReadRepository,
            IRoomAppService roomAppService,
            IBillingAccountReadRepository billingAccountReadRepository,
            IGuestReservationItemRepository guestItemRepository,
            IBillingAccountRepository billingAccountRepository,
            IRoomTypeReadRepository roomTypeReadRepository,
            IBillingAccountAppService billingAccountAppService,
            IExternalRegistrationDomainService externalRegistrationDomainService,
            IServiceProvider serviceProvider)
            : base(reservationAdapter, reservationDomainService, notificationHandler)
        {
            _unitOfWorkManager = unitOfWorkManager;
            _guestReservationItemAppService = guestReservationItemBuilderAppService;
            _reservationReadRepository = reservationReadRepository;
            _countrySubdivisionTranslationReadRepository = countrySubdivisionTranslationReadRepository;
            _roomReadRepository = roomReadRepository;
            _companyClientReadRepository = companyClientReadRepository;
            _companyClientContactPersonReadRepository = companyClientContactPersonReadRepository;
            _reservationDomainService = reservationDomainService;
            _reservationConfirmationAppService = reservationConfirmationAppService;
            _reservationConfirmationDomainService = reservationConfirmationDomainService;
            _plasticAppService = plasticAppService;
            _plasticDomainService = plasticDomainService;
            _personReadRepository = personReadRepository;
            _guestAppService = guestAppService;
            _contactInformationAppService = contactInformationAppService;
            _documentReadRepository = documentReadRepository;
            _propertyParameterReadRepository = propertyParameterReadRepository;
            _propertyCurrencyExchangeReadRepository = propertyCurrencyExchangeReadRepository;
            _reservationBudgetAppService = reservationBudgetAppService;
            _localizationManager = localizationManager;
            _applicationUser = applicationUser;
            _reservationItemStatusDomainService = reservationItemStatusDomainService;
            _roomBlockingReadRepository = roomBlockingReadRepository;
            _reservationItemDomainService = reservationItemDomainService;
            _reservationItemReadRepository = reservationItemReadRepository;
            _roomAppService = roomAppService;
            _billingAccountReadRepository = billingAccountReadRepository;
            _guestItemRepository = guestItemRepository;
            _billingAccountRepository = billingAccountRepository;
            _roomTypeReadRepository = roomTypeReadRepository;
            _billingAccountAppService = billingAccountAppService;
            _externalRegistrationDomainService = externalRegistrationDomainService;

            SetObservables(serviceProvider);
        }

        private void SetObservables(IServiceProvider serviceProvider)
        {
            _reservationItemChangeStatusObservableList = new List<IReservationItemChangeStatusObservable>();
            var serviceForReservationItemChangeStatusObservable = serviceProvider.GetServices<IReservationItemChangeStatusObservable>().First(o => o.GetType() == typeof(RoomTypeInventoryDomainService));
            _reservationItemChangeStatusObservableList.Add(serviceForReservationItemChangeStatusObservable);

            _reservationItemChangeRoomTypeOrPeriodObservableList = new List<IReservationItemChangeRoomTypeOrPeriodObservable>();
            var serviceForReservationItemChangeRoomTypeOrPeriodObservable = serviceProvider.GetServices<IReservationItemChangeRoomTypeOrPeriodObservable>().First(o => o.GetType() == typeof(RoomTypeInventoryDomainService));
            _reservationItemChangeRoomTypeOrPeriodObservableList.Add(serviceForReservationItemChangeRoomTypeOrPeriodObservable);
        }

        public ReservationDto GetReservation(DefaultLongRequestDto id)
        {
            if (id.Id < 0)
                NotifyIdIsMissing();

            if (Notification.HasNotification())
                return ReservationDto.NullInstance;

            Reservation reservation = ReservationDomainService.Get(id);

            ReservationDto reservationDto = reservation.MapTo<ReservationDto>();
            if (reservation.CompanyClientId != null)
            {
                reservationDto.CompanyClient = _companyClientReadRepository.GetCompanyClientDtoByReservationId(id.Id);
                if (reservation.CompanyClientContactPersonId != null)
                {
                    reservationDto.CompanyClientContactPerson = this._companyClientContactPersonReadRepository
                        .GetContactByReservationId(id.Id);
                }
            }

            if (reservationDto.ReservationConfirmation != null && reservationDto.ReservationConfirmation.PaymentTypeId == (int)PaymentTypeEnum.TobeBilled)
            {
                reservationDto.ReservationConfirmation.BillingClientName = _companyClientReadRepository.GetCompanyClientTradeNameById(reservationDto.ReservationConfirmation.BillingClientId ?? Guid.Empty);
                reservationDto.ReservationConfirmation.PaymentType = _localizationManager.GetString(AppConsts.LocalizationSourceName, ((PaymentTypeEnum)reservationDto.ReservationConfirmation.PaymentTypeId).ToString());
            }

            foreach (var reservationItem in reservationDto.ReservationItemList)
            {
                if (reservationItem.GratuityTypeId.HasValue)
                    reservationItem.CommercialBudgetName = _localizationManager.GetString(AppConsts.LocalizationSourceName, ((GratuityTypeEnum)reservationItem.GratuityTypeId.Value).ToString());
                else if (reservationItem.RatePlanId.HasValue)
                    reservationItem.CommercialBudgetName = reservationItem.RatePlan.AgreementName;
                else
                    reservationItem.CommercialBudgetName = _localizationManager.GetString(AppConsts.LocalizationSourceName, PropertyBaseRateEnum.Info.PropertyBaseRateName.ToString());

                if (reservationItem != null)
                    reservationItem.ReservationItemStatusFormatted = _localizationManager.GetString(AppConsts.LocalizationSourceName, ((ReservationStatus)reservationItem.ReservationItemStatusId).ToString());


                if (reservationItem.ExternalReservationNumber.ToString().ToUpper().Equals(reservationItem.ReservationItemUid.ToString().ToUpper()))
                    reservationItem.ExternalReservationNumber = null;
            }
            return reservationDto;
        }

        public ReservationDto CreateReservation(ReservationDto reservationDto)
        {
            using (var uow = _unitOfWorkManager.Begin())
            {
                if (reservationDto == null || reservationDto.ReservationConfirmation == null)
                {
                    NotifyNullParameter();
                    return ReservationDto.NullInstance;
                }

                ValidatePeriodReservation(reservationDto);

                DateTime? systemDate = _propertyParameterReadRepository.GetSystemDate(int.Parse(_applicationUser.PropertyId));

                if (systemDate == null)
                {
                    Notification.Raise(Notification.DefaultBuilder
                    .WithMessage(AppConsts.LocalizationSourceName, PropertyAuditProcess.EntityError.PropertyAuditProcessInvalidPropertySystemDate)
                    .WithDetailedMessage(AppConsts.LocalizationSourceName, PropertyAuditProcess.EntityError.PropertyAuditProcessInvalidPropertySystemDate)
                    .Build());
                    return ReservationDto.NullInstance;
                }

                if (reservationDto.ReservationItemList.Any(x => x.EstimatedArrivalDate.Date < systemDate.Value.Date ||
                                                              x.EstimatedDepartureDate.Date < systemDate.Value.Date))
                {
                    Notification.Raise(Notification.DefaultBuilder
                    .WithMessage(AppConsts.LocalizationSourceName, Reservation.EntityError.ReservationDateInvalidSystemDate)
                    .WithDetailedMessage(AppConsts.LocalizationSourceName, Reservation.EntityError.ReservationDateInvalidSystemDate)
                    .Build());
                    return ReservationDto.NullInstance;
                }

                if (reservationDto.ReservationItemList.Any(r => r.AdultCount > _roomTypeReadRepository.GetRoomTypeById(r.ReceivedRoomTypeId)?.AdultCapacity))
                {
                    Notification.Raise(Notification.DefaultBuilder
                    .WithMessage(AppConsts.LocalizationSourceName, ReservationItem.EntityError.ReservationItemOverAdultCapacityOfRoomType)
                    .WithDetailedMessage(AppConsts.LocalizationSourceName, ReservationItem.EntityError.ReservationItemOverAdultCapacityOfRoomType)
                    .Build());
                    return ReservationDto.NullInstance;
                }

                if (reservationDto.ReservationItemList.Any(r => r.ChildCount > _roomTypeReadRepository.GetRoomTypeById(r.ReceivedRoomTypeId)?.ChildCapacity))
                {
                    Notification.Raise(Notification.DefaultBuilder
                    .WithMessage(AppConsts.LocalizationSourceName, ReservationItem.EntityError.ReservationItemOverChildCapacityOfRoomType)
                    .WithDetailedMessage(AppConsts.LocalizationSourceName, ReservationItem.EntityError.ReservationItemOverChildCapacityOfRoomType)
                    .Build());
                    return ReservationDto.NullInstance;
                }

                ValidateMainGuest(reservationDto.ReservationItemList.ToList());

                if (Notification.HasNotification()) return ReservationDto.NullInstance;

                ValidateReservationBudget(reservationDto.ReservationItemList);

                if (Notification.HasNotification()) return ReservationDto.NullInstance;

                ValidateBillingClientId(reservationDto);

                if (Notification.HasNotification()) return ReservationDto.NullInstance;

                long? plasticId = null;

                ResetCheckinAndCheckoutDates(reservationDto);

                CheckIfReservationHasGratuity(reservationDto);

                var reservationItemList = new List<ReservationItemDto>();

                foreach (var reservationItem in reservationDto.ReservationItemList)
                {
                    reservationItem.WalkIn = reservationDto.WalkIn;
                    reservationItemList.Add(reservationItem);
                }

                reservationDto.ReservationItemList = reservationItemList;

                reservationDto.ReservationItemList.Where(ri => ri.GratuityTypeId.Equals(0)).ToList().ForEach(reservationItem => reservationItem.GratuityTypeId = null);

                var builder = GetReservationBuilder(reservationDto, reservationDto.ReservationConfirmation);

                if (Notification.HasNotification())
                    return ReservationDto.NullInstance;

                var reservation = ReservationDomainService.InsertAndSaveChanges(builder);
                reservationDto.Id = reservation.Id;
                reservationDto.ReservationUid = reservation.ReservationUid;

                if (reservationDto.ReservationConfirmation != null)
                {
                    reservationDto.ReservationConfirmation.ReservationId = reservationDto.Id;

                    if (reservationDto.ReservationConfirmation.Plastic != null)
                    {
                        plasticId = _plasticDomainService.InsertAndSaveChanges(_plasticAppService.GetBuilder(reservationDto.ReservationConfirmation.Plastic)).Id;
                    }

                    if (reservationDto.ReservationConfirmation.Deadline.HasValue &&
                        reservationDto.ReservationConfirmation.Deadline.Value.Date < systemDate.Value.Date)
                    {
                        Notification.Raise(Notification.DefaultBuilder
                       .WithMessage(AppConsts.LocalizationSourceName, Reservation.EntityError.ReservationInvalidDeadline)
                       .WithDetailedMessage(AppConsts.LocalizationSourceName, Reservation.EntityError.ReservationInvalidDeadline)
                       .Build());
                        return ReservationDto.NullInstance;
                    }

                    var firstDateToValidateDeadline = reservationDto.ReservationItemList.Select(c => (c.CheckInDate ?? c.EstimatedArrivalDate).Date).Min();

                    if (reservationDto.ReservationConfirmation.Deadline.HasValue &&
                        reservationDto.ReservationConfirmation.Deadline.Value.Date > firstDateToValidateDeadline.Date)
                    {
                        Notification.Raise(Notification.DefaultBuilder
                       .WithMessage(AppConsts.LocalizationSourceName, Reservation.EntityError.ReservationDeadlineCouldNotBeGreaterThanDateofEntry)
                       .WithDetailedMessage(AppConsts.LocalizationSourceName, Reservation.EntityError.ReservationDeadlineCouldNotBeGreaterThanDateofEntry)
                       .Build());
                        return ReservationDto.NullInstance;
                    }

                    if (reservationDto.ReservationConfirmation.LaunchDaily.HasValue
                        && !reservationDto.ReservationConfirmation.LaunchDaily.Value
                        && EnumHelper.GetEnumValue<CountryIsoCodeEnum>(_applicationUser.PropertyCountryCode, true) != CountryIsoCodeEnum.Europe)
                    {
                        Notification.Raise(Notification.DefaultBuilder
                       .WithMessage(AppConsts.LocalizationSourceName, Reservation.EntityError.FlagLaunchDailyMustBeChecked)
                       .WithDetailedMessage(AppConsts.LocalizationSourceName, Reservation.EntityError.FlagLaunchDailyMustBeChecked)
                       .Build());
                        return ReservationDto.NullInstance;
                    }

                    _reservationConfirmationDomainService.InsertAndSaveChanges(_reservationConfirmationAppService.GetBuilder(reservationDto.ReservationConfirmation, reservationDto, firstDateToValidateDeadline, plasticId));
                }

                reservationDto.ReservationItemList = _reservationItemReadRepository.GetAllByPropertyAndReservation(new RequestAllDto(), reservationDto.Id).Items;

                foreach (var reservationItemDto in reservationDto.ReservationItemList)
                    RiseActionsAfterReservationItemChangeStatus(reservationItemDto, reservationItemDto.ReservationItemStatus);

                uow.Complete();
            }

            return reservationDto;
        }

        private void ValidateMainGuest(List<ReservationItemDto> reservationItemList)
        {
            reservationItemList.ForEach(ri =>
            {
                if (ri.GuestReservationItemList.All(x => !x.IsMain))
                {
                    Notification.Raise(Notification.DefaultBuilder
                                 .WithMessage(AppConsts.LocalizationSourceName, GuestReservationItem.EntityError.GuestReservationItemMustHaveMain)
                                 .WithDetailedMessage(AppConsts.LocalizationSourceName, GuestReservationItem.EntityError.GuestReservationItemMustHaveMain)
                                 .Build()); return;
                }
            });
        }

        private void ValidateBillingClientId(ReservationDto reservationDto)
        {
            if (reservationDto.ReservationConfirmation.PaymentTypeId == (int)PaymentTypeEnum.TobeBilled &&
               (!reservationDto.ReservationConfirmation.BillingClientId.HasValue || reservationDto.ReservationConfirmation.BillingClientId == Guid.Empty))
            {
                Notification.Raise(Notification.DefaultBuilder
                                  .WithMessage(AppConsts.LocalizationSourceName, ReservationConfirmation.EntityError.ReservationConfirmationHasInvalidBillingClientId)
                                  .WithDetailedMessage(AppConsts.LocalizationSourceName, ReservationConfirmation.EntityError.ReservationConfirmationHasInvalidBillingClientId)
                                  .Build());
            }
        }

        private void CheckIfReservationHasGratuity(ReservationDto reservationDto)
        {
            if ((reservationDto.ReservationConfirmation.PaymentTypeId == (int)PaymentTypeEnum.Courtesy ||
                reservationDto.ReservationConfirmation.PaymentTypeId == (int)PaymentTypeEnum.InternalUsage))
                ResetAmountsFromReservationBudgetList(reservationDto.ReservationItemList.ToList());
            else
                ResetAmountsFromReservationBudgetList(reservationDto.ReservationItemList.Where(x => x.GratuityTypeId.HasValue).ToList());

        }

        private void ResetAmountsFromReservationBudgetList(List<ReservationItemDto> reservationItemDtos)
        {
            if (reservationItemDtos != null && reservationItemDtos.Any())
            {
                foreach (var reservItem in reservationItemDtos)
                {
                    if (reservItem.ReservationBudgetList != null && reservItem.ReservationBudgetList.Any())
                    {
                        foreach (var reservBud in reservItem.ReservationBudgetList)
                        {
                            reservBud.BaseRate = 0;
                            reservBud.ManualRate = 0;
                            reservBud.AgreementRate = 0;
                            reservBud.Discount = 0;
                        }
                    }
                }
            }
        }

        public void ResetCheckinAndCheckoutDates(ReservationDto reservationDto)
        {
            foreach (var reservationItem in reservationDto.ReservationItemList)
            {
                reservationItem.CheckInDate = null;
                reservationItem.CheckOutDate = null;

                foreach (var guestReservationItem in reservationItem.GuestReservationItemList)
                {
                    guestReservationItem.CheckInDate = null;
                    guestReservationItem.CheckOutDate = null;
                }
            }
        }

        public async Task<ReservationDto> UpdateReservationAsync(long id, ReservationDto reservationDto)
        {
            using (var uow = _unitOfWorkManager.Begin())
            {
                if (reservationDto == null || reservationDto.ReservationConfirmation == null || reservationDto.ReservationConfirmation.Id == null)
                {
                    NotifyNullParameter();
                    return ReservationDto.NullInstance;
                }

                ValidatePeriodReservation(reservationDto);

                DateTime? systemDate = _propertyParameterReadRepository.GetSystemDate(int.Parse(_applicationUser.PropertyId));
                _systemDate = systemDate;

                if (systemDate == null)
                {
                    Notification.Raise(Notification.DefaultBuilder
                    .WithMessage(AppConsts.LocalizationSourceName, PropertyAuditProcess.EntityError.PropertyAuditProcessInvalidPropertySystemDate)
                    .WithDetailedMessage(AppConsts.LocalizationSourceName, PropertyAuditProcess.EntityError.PropertyAuditProcessInvalidPropertySystemDate)
                    .Build());


                    return ReservationDto.NullInstance;
                }

                ValidateReservationBudget(reservationDto.ReservationItemList);

                if (Notification.HasNotification()) return ReservationDto.NullInstance;

                ValidateBillingClientId(reservationDto);

                if (Notification.HasNotification()) return ReservationDto.NullInstance;

                var existingReservation = _reservationDomainService.GetExistingReservation(id);

                ValidateRemovedReservationItem(reservationDto, existingReservation);

                if (Notification.HasNotification()) return ReservationDto.NullInstance;

                var newReservationItemList = reservationDto.ReservationItemList.Where(r => r.Id == 0);
                var reservationItemChangedRoomTypeOrPeriodDictionary = GetReservationItemChangedRoomTypeOrPeriodDictionary(existingReservation, reservationDto);
                var paramEditMigratedReservation = _propertyParameterReadRepository.GetPropertyParameterForPropertyIdAndApplicationParameterId(int.Parse(_applicationUser.PropertyId), (int)ApplicationParameterEnum.ParameterAllowEditMigratedReservation);

                if (existingReservation.ReservationItemList.Any(ri => ri.IsMigrated) && (paramEditMigratedReservation != null && !bool.Parse(paramEditMigratedReservation.PropertyParameterValue)))
                    ValidateMigratedReservation(existingReservation, reservationDto);

                if (Notification.HasNotification()) return ReservationDto.NullInstance;

                reservationDto.ReservationItemList.Where(ri => ri.GratuityTypeId.Equals(0)).ToList().ForEach(reservationItem => reservationItem.GratuityTypeId = null);

                ValidateGuestPeriod(reservationDto, existingReservation);

                if (Notification.HasNotification()) return ReservationDto.NullInstance;

                var builder = GetReservationBuilder(reservationDto, reservationDto.ReservationConfirmation, true, id, existingReservation);

                if (Notification.HasNotification())
                    return ReservationDto.NullInstance;

                Plastic plastic = null;
                if (reservationDto.ReservationConfirmation.Plastic != null)
                    plastic = _plasticAppService.GetBuilder(reservationDto.ReservationConfirmation.Plastic).Build();


                if (reservationDto.ReservationConfirmation.Deadline.HasValue &&
                        reservationDto.ReservationConfirmation.Deadline.Value.Date < systemDate.Value.Date)
                {
                    Notification.Raise(Notification.DefaultBuilder
                   .WithMessage(AppConsts.LocalizationSourceName, Reservation.EntityError.ReservationInvalidDeadline)
                   .WithDetailedMessage(AppConsts.LocalizationSourceName, Reservation.EntityError.ReservationInvalidDeadline)
                   .Build());
                    return ReservationDto.NullInstance;
                }

                var firstDateToValidateDeadline = reservationDto.ReservationItemList.Select(c => (c.CheckInDate ?? c.EstimatedArrivalDate).Date).Min();

                var reservationConfirmation = _reservationConfirmationAppService.GetBuilder(reservationDto.ReservationConfirmation, reservationDto, firstDateToValidateDeadline).Build();

                if (Notification.HasNotification())
                    return ReservationDto.NullInstance;

                _reservationDomainService.Update(builder.Build(), existingReservation, reservationConfirmation, plastic, systemDate.Value.Date);

                foreach (var reservationItem in reservationDto.ReservationItemList)
                {
                    foreach (var guestReservationItem in reservationItem.GuestReservationItemList)
                        _guestReservationItemAppService.UpdateGuestEstimatedArrivalAndDepartureDate(guestReservationItem.Id, guestReservationItem.EstimatedArrivalDate, guestReservationItem.EstimatedDepartureDate);
                }

                foreach (var reservationItemChangedRoomTypeOrDate in reservationItemChangedRoomTypeOrPeriodDictionary)
                    RiseActionsAfterReservationItemChangeRoomTypeOrPeriod(reservationItemChangedRoomTypeOrDate.Key, reservationItemChangedRoomTypeOrDate.Value);

                foreach (var newReservationItemDto in newReservationItemList)
                    RiseActionsAfterReservationItemChangeStatus(newReservationItemDto, newReservationItemDto.ReservationItemStatus);

                if (reservationDto.ReservationItemList.Count > 0)
                    await _externalRegistrationDomainService.AddNewGuestsIfNecessaryAsync(existingReservation.ReservationUid);

                uow.Complete();
            }


            return reservationDto;
        }

        private void ValidateRemovedReservationItem(ReservationDto reservationDto, Reservation existingReservation)
        {
            var existingReservationDto = existingReservation.MapTo<ReservationDto>();
            var existingGuestReservationItemDto = new List<GuestReservationItemDto>();
            var guestReservationItemDto = new List<GuestReservationItemDto>();

            existingReservationDto.ReservationItemList.ToList().ForEach(eri => existingGuestReservationItemDto.AddRange(eri.GuestReservationItemList));
            reservationDto.ReservationItemList.ToList().ForEach(ri => guestReservationItemDto.AddRange(ri.GuestReservationItemList));

            var removedReservationItemList = existingGuestReservationItemDto.Select(x => new { x.Id, x.IsMain })
                                             .Except(guestReservationItemDto.Select(x => new { x.Id, x.IsMain }))
                                             .ToList();
           
            if (removedReservationItemList.Any(x => x.IsMain))
            {
                Notification.Raise(Notification.DefaultBuilder
                    .WithMessage(AppConsts.LocalizationSourceName, GuestReservationItem.EntityError.GuestReservationItemIsMain)
                    .WithDetailedMessage(AppConsts.LocalizationSourceName, GuestReservationItem.EntityError.GuestReservationItemIsMain)
                    .Build());

                return;
            }

            if(removedReservationItemList.Any())
                _billingAccountAppService.CheckIfExistIsMainBillingAccountByGuestReservationItemIds(removedReservationItemList.Select(x => (long?)x.Id).ToList());            
        }       

        private void ValidateReservationBudget(ICollection<ReservationItemDto> reservationItemList)
        {
            if(reservationItemList.Select(ri => ri.CurrencyId).Distinct().Count() > 1)
            {
                Notification.Raise(Notification.DefaultBuilder
                         .WithMessage(AppConsts.LocalizationSourceName, Reservation.EntityError.ReservationMustHaveSameCurrencyForAllReservationItems)
                         .WithDetailedMessage(AppConsts.LocalizationSourceName, Reservation.EntityError.ReservationMustHaveSameCurrencyForAllReservationItems)
                         .Build());

                return;
            }

            var reservationBudgetList = Enumerable.Empty<ReservationBudgetDto>();
            foreach (var reservItemDto in reservationItemList)
                reservationBudgetList = reservationBudgetList.Concat(reservItemDto.ReservationBudgetList.AsEnumerable());

            if(reservationBudgetList.Select(rb => rb.CurrencyId).Distinct().Count() > 1)
                Notification.Raise(Notification.DefaultBuilder
                         .WithMessage(AppConsts.LocalizationSourceName, ReservationItem.EntityError.ReservationItemMustHaveSameCurrencyForAllReservationBudgets)
                         .WithDetailedMessage(AppConsts.LocalizationSourceName, ReservationItem.EntityError.ReservationItemMustHaveSameCurrencyForAllReservationBudgets)
                         .Build());
        }

        private void ValidateMigratedReservationItem(ReservationItem reservationItem, ReservationItemDto dto)
        {
            if (reservationItem.GratuityTypeId != dto.GratuityTypeId)
                Notification.Raise(Notification.DefaultBuilder
                          .WithMessage(AppConsts.LocalizationSourceName, ReservationItem.EntityError.CanNotChangeGratuityTypeIdFromMigratedReservation)
                          .WithDetailedMessage(AppConsts.LocalizationSourceName, ReservationItem.EntityError.CanNotChangeGratuityTypeIdFromMigratedReservation)
                          .Build());

            if (reservationItem.ChildCount != dto.ChildCount)
                Notification.Raise(Notification.DefaultBuilder
                          .WithMessage(AppConsts.LocalizationSourceName, ReservationItem.EntityError.CanNotChangeChildCountFromMigratedReservation)
                          .WithDetailedMessage(AppConsts.LocalizationSourceName, ReservationItem.EntityError.CanNotChangeChildCountFromMigratedReservation)
                          .Build());

            if (reservationItem.AdultCount != dto.AdultCount)
                Notification.Raise(Notification.DefaultBuilder
                          .WithMessage(AppConsts.LocalizationSourceName, ReservationItem.EntityError.CanNotChangeAdultCountFromMigratedReservation)
                          .WithDetailedMessage(AppConsts.LocalizationSourceName, ReservationItem.EntityError.CanNotChangeAdultCountFromMigratedReservation)
                          .Build());

            if (reservationItem.RequestedRoomTypeId != dto.RequestedRoomTypeId)
                Notification.Raise(Notification.DefaultBuilder
                          .WithMessage(AppConsts.LocalizationSourceName, ReservationItem.EntityError.CanNotChangeRequestedRoomTypeIdFromMigratedReservation)
                          .WithDetailedMessage(AppConsts.LocalizationSourceName, ReservationItem.EntityError.CanNotChangeRequestedRoomTypeIdFromMigratedReservation)
                          .Build());

            if (reservationItem.EstimatedArrivalDate.Date != dto.EstimatedArrivalDate.Date)
                Notification.Raise(Notification.DefaultBuilder
                          .WithMessage(AppConsts.LocalizationSourceName, ReservationItem.EntityError.CanNotChangeEstimatedArrivalDateFromMigratedReservation)
                          .WithDetailedMessage(AppConsts.LocalizationSourceName, ReservationItem.EntityError.CanNotChangeEstimatedArrivalDateFromMigratedReservation)
                          .Build());

            if (reservationItem.EstimatedDepartureDate.Date != dto.EstimatedDepartureDate.Date)
                Notification.Raise(Notification.DefaultBuilder
                          .WithMessage(AppConsts.LocalizationSourceName, ReservationItem.EntityError.CanNotChangeEstimatedDepartureDateFromMigratedReservation)
                          .WithDetailedMessage(AppConsts.LocalizationSourceName, ReservationItem.EntityError.CanNotChangeEstimatedDepartureDateFromMigratedReservation)
                          .Build());

            _reservationBudgetAppService.ValidateMigratedReservationBudget(reservationItem.ReservationBudgetList, dto.ReservationBudgetList);
        }

        private void ValidateMigratedReservation(Reservation existingReservation, ReservationDto reservationDto)
        {
            var existingReservationItem = existingReservation.ReservationItemList.FirstOrDefault();
            var reservationItemDto = reservationDto.ReservationItemList.FirstOrDefault();
            if (existingReservationItem == null || reservationItemDto == null)
            {
                NotifyNullParameter();
                return;
            }

            ValidateMigratedReservationItem(existingReservationItem, reservationItemDto);
        }

        private void ValidateGuestPeriod(ReservationDto reservationDto, Reservation existingReservation)
        {
            var dictionaryReservationItemOld = new Dictionary<long, (DateTime, DateTime)>();
            var dictionaryGuestReservationItemOld = new Dictionary<long, (DateTime, DateTime)>();

            // Pegando as informações da reserva antiga para as validações de datas do guest
            existingReservation.ReservationItemList.ToList().ForEach(
                ri => { dictionaryReservationItemOld.Add(ri.Id, (ri.EstimatedArrivalDate, ri.EstimatedDepartureDate));
                    ri.GuestReservationItemList.ToList().ForEach(
                       gri => dictionaryGuestReservationItemOld.Add(gri.Id, (gri.EstimatedArrivalDate, gri.EstimatedDepartureDate))
                    );
                }
            );

            // ForEach para validar se a data do guest será alterada conforme a data da reserva, ou não, pois cada guest pode ter o seu próprio período 
            // Se nenhuma dessas condições forem verdadeiras, não será alterado a data do guest, mesmo que a data da reserva seja alterada
            foreach (var reservationItem in reservationDto.ReservationItemList)
            {
                foreach(var guestReservationItem in reservationItem.GuestReservationItemList.Where(g => !g.Id.Equals(0)))
                {
                    // Se a data do guest antigo não for igual ao da reserva antiga, iremos a outras regras, se for igual, a data do guest será alterada                    
                    if (!(dictionaryReservationItemOld.FirstOrDefault(x => x.Key == reservationItem.Id).Value.Item1.Date == 
                        dictionaryGuestReservationItemOld.FirstOrDefault(x => x.Key == guestReservationItem.Id).Value.Item1.Date &&
                        dictionaryReservationItemOld.FirstOrDefault(x => x.Key == reservationItem.Id).Value.Item2.Date ==
                        dictionaryGuestReservationItemOld.FirstOrDefault(x => x.Key == guestReservationItem.Id).Value.Item2.Date))
                    {
                        // Se ao editar a data da reserva, caso a data de entrada ficar maior que a data de saída do hospede,
                        // ou a data de saída ficar menor que a data de entrada do hospede,
                        // será emitido uma mensagem de erro, dizendo que o data de algum hospede ficará fora dos limites com a data da reserva.
                        if (reservationItem.EstimatedArrivalDate.Date > dictionaryGuestReservationItemOld.FirstOrDefault(x => x.Key == guestReservationItem.Id).Value.Item2.Date ||
                            reservationItem.EstimatedDepartureDate.Date < dictionaryGuestReservationItemOld.FirstOrDefault(x => x.Key == guestReservationItem.Id).Value.Item1.Date)
                        {
                            Notification.Raise(Notification.DefaultBuilder
                            .WithMessage(AppConsts.LocalizationSourceName, ReservationItem.EntityError.ReservationItemDateIsOutOfBoundWithGuestDate)
                            .WithDetailedMessage(AppConsts.LocalizationSourceName, ReservationItem.EntityError.ReservationItemDateIsOutOfBoundWithGuestDate)
                            .Build());
                            return;
                        }

                        // Se a data do guest for diferente da data da reserva, ao editar a data da reserva e a data de entrada da reserva 
                        // for maior que a data de entrada do guest, atualizo a data de entrada do guest
                        if (reservationItem.EstimatedArrivalDate.Date > dictionaryGuestReservationItemOld.FirstOrDefault(x => x.Key == guestReservationItem.Id).Value.Item1.Date)
                            guestReservationItem.EstimatedArrivalDate = reservationItem.EstimatedArrivalDate;
                        else
                            guestReservationItem.EstimatedArrivalDate = dictionaryGuestReservationItemOld.FirstOrDefault(x => x.Key == guestReservationItem.Id).Value.Item1;

                        // Se a data do guest for diferente da data da reserva, ao editar a data da reserva e a data de saida 
                        // for menor que a data de saida do guest, atualizo a data de saida do guest.
                        if (reservationItem.EstimatedDepartureDate.Date < dictionaryGuestReservationItemOld.FirstOrDefault(x => x.Key == guestReservationItem.Id).Value.Item2.Date)
                            guestReservationItem.EstimatedDepartureDate = reservationItem.EstimatedDepartureDate;
                        else
                            guestReservationItem.EstimatedDepartureDate = dictionaryGuestReservationItemOld.FirstOrDefault(x => x.Key == guestReservationItem.Id).Value.Item2;

                        // Se a reserva já estiver em check-in, ao alterar a data de saída da reserva, 
                        // será alterada a data de saída dos hóspedes também, para ter a mesma data de saída da reserva
                        if (reservationItem.ReservationItemStatusId == (int)ReservationStatus.Checkin &&
                           reservationItem.EstimatedDepartureDate != dictionaryReservationItemOld.FirstOrDefault(x => x.Key == reservationItem.Id).Value.Item2.Date)
                            guestReservationItem.EstimatedDepartureDate = reservationItem.EstimatedDepartureDate;
                    }
                    else
                    {
                        guestReservationItem.EstimatedArrivalDate = reservationItem.EstimatedArrivalDate;
                        guestReservationItem.EstimatedDepartureDate = reservationItem.EstimatedDepartureDate;
                    }  
                } 
            }
        }

        private IDictionary<ReservationItem, ReservationItemDto> GetReservationItemChangedRoomTypeOrPeriodDictionary(Reservation existingReservation, ReservationDto reservationDto)
        {
            var reservationItemChangedRoomTypeOrPeriodDictionary = new Dictionary<ReservationItem, ReservationItemDto>();

            var reservationItemList = reservationDto.ReservationItemList
                .Where(r => (ReservationStatus)r.ReservationItemStatusId == ReservationStatus.ToConfirm ||
                (ReservationStatus)r.ReservationItemStatusId == ReservationStatus.Confirmed ||
                (ReservationStatus)r.ReservationItemStatusId == ReservationStatus.Checkin);

            foreach (var reservationItemDto in reservationItemList)
            {
                var existingReservationItem = existingReservation.ReservationItemList.Where(r => r.Id == reservationItemDto.Id).FirstOrDefault();

                if (existingReservationItem != null)
                {
                    var arrivalDateExistent = existingReservationItem.CheckInDate.HasValue ? existingReservationItem.CheckInDate.Value.Date : existingReservationItem.EstimatedArrivalDate.Date;

                    if (existingReservationItem != null &&
                        (existingReservationItem.ReceivedRoomTypeId != reservationItemDto.ReceivedRoomTypeId ||
                            arrivalDateExistent != reservationItemDto.EstimatedArrivalDate.Date ||
                            existingReservationItem.EstimatedDepartureDate.Date != reservationItemDto.EstimatedDepartureDate.Date)
                        )
                    {
                        var oldReservationItem = _reservationItemDomainService.GetNewInstanceWithReceivedRoomTypeIdAndDates(existingReservationItem);

                        reservationItemChangedRoomTypeOrPeriodDictionary.Add(oldReservationItem, reservationItemDto);
                    }
                }      
            }

            return reservationItemChangedRoomTypeOrPeriodDictionary;
        }

        private bool ReservationItemChangedRoomTypeAndDate(IDictionary<ReservationItem, ReservationItemDto> reservationItemChangedDateDictionary, ReservationItem existingReservationItem, ReservationItemDto reservationItemDto)
        {
            return reservationItemChangedDateDictionary.Keys.Select(reservationItem => reservationItem.Id).Contains(existingReservationItem.Id) &&
                    reservationItemChangedDateDictionary.Values.Select(dto => dto.Id).Contains(reservationItemDto.Id);
        }

        public void DeleteReservation(long id)
        {
            throw new System.NotImplementedException();
        }

        public ReservationDto GetWithReservationItemsAndRoomsAndRoomTypes(DefaultLongRequestDto id)
        {
            if (id.Id < 0)
                NotifyIdIsMissing();

            if (Notification.HasNotification())
                return ReservationDto.NullInstance;

            Reservation reservation = _reservationReadRepository.GetWithReservationItemsAndRoomsAndRoomTypes(id);
            return reservation.MapTo<ReservationDto>();
        }

        private Reservation.Builder GetReservationBuilder(ReservationDto reservationDto, ReservationConfirmationDto reservationConfirmationDto, bool update = false, long id = 0, Reservation existingReservation = null)
        {
            IList<ReservationItem> reservationItemList = new List<ReservationItem>();

            if (reservationDto.ReservationItemList.ContainsElement())
            {
                if ((reservationDto.Id == 0 || !update) && 
                    (string.IsNullOrEmpty(reservationDto.ReservationCode) || reservationDto.ReservationCode == "001"))
                {
                    ReservationCode guid = ReservationCodeGeneratorHelper.GenerateTimeBasedShortGuid();
                    reservationDto.ReservationCode = guid.ToString();
                }

                _reservationDomainService.ValidateDuplicatesRoomsInReservationDto(reservationDto);

                if (Notification.HasNotification())
                    return null;

                MapReservationItemListDtoToReservationItemListEntity(reservationDto.ReservationItemList, reservationItemList, reservationDto, existingReservation);


                var builder = new Reservation.Builder(Notification);

                builder
                    .WithId(id)
                    .WithReservationUid(reservationDto.ReservationUid)
                    .WithReservationCode(reservationDto.ReservationCode)
                    .WithReservationVendorCode(reservationDto.ReservationVendorCode)
                    .WithPropertyId(Convert.ToInt32(_applicationUser.PropertyId))
                    .WithContactName(reservationDto.ContactName)
                    .WithContactEmail(reservationDto.ContactEmail)
                    .WithContactPhone(reservationDto.ContactPhone)
                    .WithPartnerComments(existingReservation != null ? existingReservation.PartnerComments : null)
                    .WithInternalComments(reservationDto.InternalComments)
                    .WithExternalComments(reservationDto.ExternalComments)
                    .WithGroupName(reservationDto.GroupName)
                    .WithUnifyAccounts(reservationDto.UnifyAccounts)
                    .WithReservationItemList(reservationItemList)
                    .WithCompanyClientId(reservationDto.CompanyClientId, reservationConfirmationDto.BillingClientId)
                    .WithCompanyClientContactPersonId(reservationDto.CompanyClientContactPersonId)
                    .WithBusinessSourceId(reservationDto.BusinessSourceId)
                    .WithMarketSegmentId(reservationDto.MarketSegmentId);

                return builder;
            }
            return null;

        }

        private void MapReservationItemListDtoToReservationItemListEntity(ICollection<ReservationItemDto> reservationItemDtoList, IList<ReservationItem> reservationItemList, ReservationDto reservationDto, Reservation existingReservation = null)
        {
            var childAgeParameter = _propertyParameterReadRepository.GetPropertyParameterMaxChildrenAge(reservationDto.PropertyId);

            foreach (var reservationItemDto in reservationItemDtoList)
            {
                ReservationItem existingReservationItem = existingReservation != null ? existingReservation.ReservationItemList.FirstOrDefault(exp => exp.Id == reservationItemDto.Id) : null;

                if (existingReservationItem != null)
                {
                    if (!_reservationItemStatusDomainService.CanChangeStatus((ReservationStatus)existingReservationItem.ReservationItemStatusId, (ReservationStatus)reservationItemDto.ReservationItemStatusId))
                    {
                        Notification.Raise(Notification.DefaultBuilder.WithMessage(AppConsts.LocalizationSourceName, ReservationItemEnum.Error.CouldNotChangeStatus).Build());
                    }
                }

                if (Notification.HasNotification())
                    return;

                _reservationBudgetAppService.ValidateReservationBudgetDays(reservationItemDto.ReservationBudgetList.Count, reservationItemDto.CheckInDate, reservationItemDto.CheckOutDate, reservationItemDto.EstimatedArrivalDate, reservationItemDto.EstimatedDepartureDate, reservationItemDto.ReservationItemStatusId);

                if (Notification.HasNotification())
                    return;

                IList<ReservationBudget> reservationBudgetList = new List<ReservationBudget>();
                IList<GuestReservationItem> guestReservationItemList = new List<GuestReservationItem>();

                if (reservationItemDto.ReservationBudgetList.ContainsElement())
                {
                    MapReservationBudgetListDtoToReservationBudgetListEntity(reservationItemDto.ReservationBudgetList, reservationBudgetList, existingReservationItem != null);
                }

                if (reservationItemDto.GuestReservationItemList.ContainsElement())
                {
                    MapGuestReservationItemListDtoToGuestReservationItemListEntity(reservationItemDto.GuestReservationItemList, guestReservationItemList, reservationItemDto, childAgeParameter);
                }

                ReservationItem reservationItem = new ReservationItem.Builder(Notification, _roomReadRepository, existingReservationItem, reservationDto.PropertyId, true)
                    .WithId(reservationItemDto.Id)
                    .WithReservationItemUid(reservationItemDto.ReservationItemUid)
                    .WithEstimatedArrivalDate(reservationItemDto.EstimatedArrivalDate)
                    .WithCheckInDate(existingReservationItem?.CheckInDate)
                    .WithEstimatedDepartureDate(reservationItemDto.EstimatedDepartureDate)
                    .WithCheckOutDate(existingReservationItem?.CheckOutDate)
                    .WithReservationItemCode(reservationDto.ReservationCode + "-" + reservationItemDto.ReservationItemCode.Split("-").LastOrDefault())
                    .WithRequestedRoomTypeId(reservationItemDto.RequestedRoomTypeId)
                    .WithReceivedRoomTypeId(reservationItemDto.ReceivedRoomTypeId)
                    .WithRoomId(reservationItemDto.RoomId)
                    .WithAdultCount(reservationItemDto.AdultCount)
                    .WithChildCount(reservationItemDto.ChildCount)
                    .WithReservationItemStatus(reservationItemDto.ReservationItemStatus, reservationDto.ReservationConfirmation)
                    .WithRoomLayoutId(reservationItemDto.RoomLayoutId)
                    .WithExtraBedCount(reservationItemDto.ExtraBedCount)
                    .WithExtraCribCount(reservationItemDto.ExtraCribCount)
                    .WithReservationBudgetList(reservationBudgetList)
                    .WithGuestReservationItemList(guestReservationItemList)
                    .WithCurrencyId(reservationItemDto.CurrencyId)
                    .WithRatePlanId(reservationItemDto.RatePlanId)
                    .WithGratuityTypeId(reservationItemDto.GratuityTypeId)
                    .WithMealPlanTypeId(reservationItemDto.MealPlanTypeId)
                    .WithWalkIn(reservationItemDto.WalkIn)
                    .WithIsMigrated(existingReservationItem != null ? existingReservationItem.IsMigrated : false)
                    .WithExternalReservationNumber(existingReservationItem != null && existingReservationItem.IsMigrated ? existingReservationItem.ExternalReservationNumber : reservationItemDto.ExternalReservationNumber)
                    .WithPartnerReservationNumber(existingReservationItem != null && existingReservationItem.IsMigrated ? existingReservationItem.PartnerReservationNumber : reservationItemDto.PartnerReservationNumber)
                    .Build();

                reservationItemList.Add(reservationItem);
            }
        }

        private void MapReservationBudgetListDtoToReservationBudgetListEntity(
            ICollection<ReservationBudgetDto> reservationBudgetDtoList,
            ICollection<ReservationBudget> reservationBudgetList,
            bool isUpdate)
        {
            if (isUpdate)
            {
                _systemDate = _systemDate ?? _propertyParameterReadRepository.GetSystemDate(int.Parse(_applicationUser.PropertyId));

                reservationBudgetDtoList = reservationBudgetDtoList.Where(e => e.BudgetDay.Date >= _systemDate.Value.Date).ToList();
            }

            foreach (var reservationBudgetDto in reservationBudgetDtoList)
            {
                var reservationBudget = _reservationBudgetAppService.GetReservationBudgetEntity(reservationBudgetDto);
                //var reservationBudgetBuilder = ReservationBudgetAdapter.Map(reservationBudgetDto);

                //ReservationBudget reservationBudget = new ReservationBudgetBuilder(Notification)
                //    .WithId(reservationBudgetDto.Id)
                //    .WithReservationBudgetUid(reservationBudgetDto.ReservationBudgetUid)
                //    .WithReservationItemId(reservationBudgetDto.ReservationItemId)
                //    .WithBudgetDay(reservationBudgetDto.BudgetDay)
                //    .WithManualRate(reservationBudgetDto.ManualRate)
                //    .WithCurrencyId(CurrencyId)
                //    .Build();

                reservationBudgetList.Add(reservationBudget);
            }
        }

        private void MapGuestReservationItemListDtoToGuestReservationItemListEntity(
            ICollection<GuestReservationItemDto> guestReservationItemDtoList,
            ICollection<GuestReservationItem> guestReservationItemList,
            ReservationItemDto reservationItemDto,
            PropertyParameterDto childAgeParameter
            )
        {
            var sumPctDailyRate = guestReservationItemDtoList.Sum(d => d.PctDailyRate);
            if (GuestReservationItemDomainRules.SumOfListOfPctDailyRateGreaterThanAllowed(sumPctDailyRate))
            {
                Notification.Raise(Notification
                    .DefaultBuilder
                    .WithMessage(AppConsts.LocalizationSourceName, GuestReservationItem.EntityError.GuestReservationItemSumOfListOfPctDailyRateGreaterThanAllowed)
                    .Build());
            }

            var maxIsMain = guestReservationItemDtoList.Count(d => d.IsMain);
            var maxTotalOfIsMain = 1;

            if (maxIsMain > maxTotalOfIsMain)
            {
                Notification.Raise(Notification
                    .DefaultBuilder
                    .WithMessage(AppConsts.LocalizationSourceName, CommonsEnum.Error.GuestReservationItemTotalOfListOfIsMainGreaterThanAllowed)
                    .Build());
            }

            var adultCount = reservationItemDto.AdultCount;
            var adultCountGuests = guestReservationItemDtoList.Where(exp => !exp.IsChild).Count();

            if (adultCount != adultCountGuests)
            {
                Notification.Raise(Notification
                    .DefaultBuilder
                    .WithMessage(AppConsts.LocalizationSourceName, GuestReservationItem.EntityError.GuestReservationItemTotalAdultsInvalid)
                    .Build());
            }

            var childCount = reservationItemDto.ChildCount;
            var childCountGuests = guestReservationItemDtoList.Where(exp => exp.IsChild).Count();

            if (childCount != childCountGuests)
            {
                Notification.Raise(Notification
                    .DefaultBuilder
                    .WithMessage(AppConsts.LocalizationSourceName, GuestReservationItem.EntityError.GuestReservationItemTotalChildsInvalid)
                    .Build());
            }


            if (!Notification.HasNotification())
                foreach (var guestReservationItemDto in guestReservationItemDtoList)
                {
                    var isUpdate = reservationItemDto.Id > 0;

                    if (guestReservationItemDto.GuestId.HasValue && guestReservationItemDto.GuestId.Value == 0)
                    {
                        guestReservationItemDto.GuestId = null;
                    }
                    if (guestReservationItemDto.PersonId.HasValue)
                    {
                        guestReservationItemDto.GuestId = _guestAppService.CreateGuestIfNotExistAndReturnGuest(guestReservationItemDto.PersonId.Value);

                        var guestEmail = _contactInformationAppService.GetEmailContactInformationByPersonId(guestReservationItemDto.PersonId.Value);
                        guestReservationItemDto.GuestEmail = !String.IsNullOrEmpty(guestEmail) ? guestEmail : null;

                        var documents = _personReadRepository.GetDocumentsByPersonId(guestReservationItemDto.PersonId.Value);

                        if (documents != null)
                        {
                            guestReservationItemDto.GuestDocument = documents.Any(x => x.IsDocumentMain) ? documents.Where(x => x.IsDocumentMain).FirstOrDefault().Document : documents.FirstOrDefault().Document;

                            guestReservationItemDto.GuestDocumentTypeId = documents.Any(x => x.IsDocumentMain) ? documents.Where(x => x.IsDocumentMain).FirstOrDefault().DocumentTypeId : documents.FirstOrDefault().DocumentTypeId;
                        }
                    }

                    var guestReservationItemBuilder = _guestReservationItemAppService.GetGuestReservationItemFromReservationBuilder(guestReservationItemDto, reservationItemDto, childAgeParameter, isUpdate);

                    if (guestReservationItemBuilder == null)
                        guestReservationItemList = null;
                    else
                    {
                        var guestReservationItem = guestReservationItemBuilder.Build();

                        if (!Notification.HasNotification())
                            guestReservationItemList.Add(guestReservationItem);
                        else
                            guestReservationItemList = null;
                    }
                }
        }

        public IListDto<SearchReservationResultDto> GetAllByFilters(SearchReservationDto request, int propertyId)
        {
            bool vFlag = false;
            var nullResponseInstance = new ListDto<SearchReservationResultDto>();

            foreach (var propertyInfo in request.GetType()
                                .GetProperties(
                                        BindingFlags.Public
                                        | BindingFlags.Instance))
            {
                if (propertyInfo.GetValue(request, null) != null)
                {
                    if (propertyInfo.Name != "Page" &&
                       propertyInfo.Name != "PageSize")
                    {
                        vFlag = true;
                    }
                }
            }

            if (!vFlag)
            {
                NotifyEmptyFilter();
                return nullResponseInstance;
            }

            if (propertyId <= 0)
            {
                NotifyIdIsMissing();
                return nullResponseInstance;
            }

            if (!request.HasValidOrderBy())
            {
                this.NotifyInvalidOrderBy();
                return nullResponseInstance;
            }

            return _reservationReadRepository.GetAllByFilters(request, propertyId);
        }

        public IListDto<SearchReservationResultDto> GetAllByFiltersV2(SearchReservationDto request, int propertyId)
        {
            bool vFlag = false;
            var nullResponseInstance = new ListDto<SearchReservationResultDto>();

            foreach (var propertyInfo in request.GetType()
                                .GetProperties(
                                        BindingFlags.Public
                                        | BindingFlags.Instance))
            {
                if (propertyInfo.GetValue(request, null) != null)
                {
                    if (propertyInfo.Name != "Page" &&
                       propertyInfo.Name != "PageSize")
                    {
                        vFlag = true;
                    }
                }
            }

            if (!vFlag)
            {
                NotifyEmptyFilter();
                return nullResponseInstance;
            }

            if (propertyId <= 0)
            {
                NotifyIdIsMissing();
                return nullResponseInstance;
            }

            if (!request.HasValidOrderBy())
            {
                this.NotifyInvalidOrderBy();
                return nullResponseInstance;
            }

            return _reservationReadRepository.GetAllByFiltersV2(request, propertyId);
        }

        public IListDto<ReservationDto> GetReservationsByPropertyId(RequestAllDto request, int propertyId)
        {
            if (propertyId <= 0)
            {
                this.NotifyIdIsMissing();
                return new ListDto<ReservationDto>();
            }

            return this._reservationReadRepository.GetReservationsByPropertyId(request, propertyId);
        }

        public SearchReservationResultDto GetReservationDetail(SearchReservationDto request)
        {
            if (request.ReservationId <= 0)
                NotifyIdIsMissing();

            if (Notification.HasNotification())
                return SearchReservationResultDto.NullInstance;

            return _reservationReadRepository.GetReservationDetail(request, int.Parse(_applicationUser.PropertyId));
        }

        public ReservationDto GetByReservationItemId(DefaultLongRequestDto id)
        {
            if (id.Id <= 0)
                NotifyIdIsMissing();

            if (Notification.HasNotification())
                return ReservationDto.NullInstance;

            Reservation reservation = _reservationReadRepository.GetByReservationItemId(id);

            var reservationDto = reservation.MapTo<ReservationDto>();

            if (reservationDto != null)
                foreach (var reservationsItems in reservationDto.ReservationItemList.SelectMany(list => list.GuestReservationItemList))
                {
                    if (reservationsItems.Guest != null && reservationsItems.Guest.Person != null)
                        foreach (var location in reservationsItems.Guest.Person.LocationList)
                            location.AddressTranslation = _countrySubdivisionTranslationReadRepository.GetAddressTranslationByCityId(location.CityId, "pt-BR");
                }

            return reservationDto;
        }

        public void CancelReservation(long reservationId, ReservationCancelDto dto)
        {
            using (var uow = _unitOfWorkManager.Begin())
            {
                var reservationItemDtoList = _reservationItemReadRepository.GetAllDtoByReservationId(reservationId).ToList();

                var billingAccountValidation = new Dictionary<long, (bool, bool)>();

                reservationItemDtoList.ForEach(reservationItem => billingAccountValidation.Add(reservationItem.Id, _billingAccountReadRepository.AnyOpenAccountAndHaveBillingAccountItemByReservationItemId(reservationItem.Id)));

                if (billingAccountValidation.Any(b => b.Value.Item2))
                {
                    Notification.Raise(Notification.DefaultBuilder
                    .WithMessage(AppConsts.LocalizationSourceName, ReservationItem.EntityError.ReservationItemHaveAnyOpenedBillingAccountWithBillingAccountItem)
                    .WithDetailedMessage(AppConsts.LocalizationSourceName, ReservationItem.EntityError.ReservationItemHaveAnyOpenedBillingAccountWithBillingAccountItem)
                    .Build());
                    return;
                }
                else
                {
                    if (billingAccountValidation.Any(b => b.Value.Item1))
                    {
                        billingAccountValidation.ToList().ForEach(billingAccount =>
                        {
                            if (billingAccount.Value.Item1)
                            {
                                var billingAccountList = _billingAccountReadRepository.GetAllOpenedByReservationItemId(billingAccount.Key);
                                billingAccountList.ForEach(billingAccounts => _billingAccountRepository.CloseBillingAccount(billingAccounts.Id));
                            }
                        });
                    }

                    this._reservationDomainService.CancelReservation(reservationId, dto.ReasonId, dto.CancellationDescription);

                    foreach (var reservationItemDto in reservationItemDtoList)
                        RiseActionsAfterReservationItemChangeStatus(reservationItemDto, ReservationStatus.Canceled);
                }

                uow.Complete();            
            }
        }

        public ReservationHeaderResultDto GetReservationHeaderByReservationItemId(long reservationItemId)
        {
            if (reservationItemId <= 0)
                NotifyIdIsMissing();

            if (Notification.HasNotification())
                return null;

            var result = _reservationReadRepository.GetReservationHeaderByReservationItemId(reservationItemId);

            if (result != null)
            {
                DateTime? systemDate = _propertyParameterReadRepository.GetSystemDate(result.PropertyId);

                if (systemDate == null)
                {
                    Notification.Raise(Notification.DefaultBuilder
                    .WithMessage(AppConsts.LocalizationSourceName, PropertyAuditProcess.EntityError.PropertyAuditProcessInvalidPropertySystemDate)
                    .WithDetailedMessage(AppConsts.LocalizationSourceName, PropertyAuditProcess.EntityError.PropertyAuditProcessInvalidPropertySystemDate)
                    .Build());
                    return null;
                }                

                result.ReservationItemBudgetHeaderDto = _reservationBudgetAppService.GetWithBudgetOffer(new ReservationItemBudgetRequestDto
                {
                    AdultCount = result.AdultCount,
                    ChildrenAge = result.ChildrenAge,
                    CurrencyId = result.CurrencyId,                    
                    CompanyClientId = result.CompanyClientId,
                    InitialDate = result.ArrivalDate.Date,
                    FinalDate = result.ArrivalDate.Date == result.DepartureDate.Date ? result.DepartureDate.Date : result.DepartureDate.AddDays(-1).Date,
                    MealPlanTypeId = result.MealPlanTypeId,
                    RatePlanId = result.RatePlanId,
                    ReservationItemId = result.ReservationItemId,
                    RoomTypeIdList = new List<int>() { result.ReceivedRoomTypeId }
                }, result.PropertyId).ConfigureAwait(false).GetAwaiter().GetResult();
            }


            return result;

        }

        public void ReservationSaveNote(long reservationId, ReservationSaveNotesDto dto)
        {
            if (reservationId <= 0)
                NotifyIdIsMissing();

            if (Notification.HasNotification())
                return;

            var reservation = _reservationDomainService.GetExistingReservation(reservationId);

            if (reservation != null)
            {
                var reservationBuilder = new Reservation.Builder(Notification, reservation);

                if (reservationBuilder != null)
                {
                    reservationBuilder
                        .WithExternalComments(dto.ExternalComments)
                        .WithInternalComments(dto.InternalComments);


                    if (!_reservationDomainService.CanModifyReservation(reservationId))
                    {
                        Notification.Raise(Notification
                         .DefaultBuilder
                         .WithMessage(AppConsts.LocalizationSourceName, Reservation.EntityError.ReservationNotModify)
                         .Build());
                        return;
                    }

                    _reservationDomainService.Update(reservationBuilder);
                }
            }

        }

        public void ReactivateReservation(long reservationId)
        {
            using (var uow = _unitOfWorkManager.Begin())
            {

                if (reservationId <= 0)
                    NotifyIdIsMissing();

                if (Notification.HasNotification())
                    return;

                _reservationDomainService.ReactivateReservation(reservationId);

                var reservationItemDtoList = _reservationItemReadRepository.GetAllDtoByReservationId(reservationId);

                foreach (var reservationItemDto in reservationItemDtoList)
                    RiseActionsAfterReservationItemChangeStatus(reservationItemDto, reservationItemDto.ReservationItemStatus);

                uow.Complete();
            }
        }

        public IListDto<ReservationCompanyClientResultDto> GetReservationCompanyClient(GetAllReservationCompanyClientDto request, int propertyId)
        {
            if (propertyId <= 0)
                NotifyIdIsMissing();

            if (Notification.HasNotification())
                return null;

            return _reservationReadRepository.GetReservationCompanyClient(request, propertyId);
        }

        public IListDto<ReservationGuestResultDto> GetReservationGuest(GetAllReservationGuestDto request, int propertyId)
        {
            if (propertyId <= 0)
                NotifyIdIsMissing();

            if (Notification.HasNotification())
                return null;

            return _reservationReadRepository.GetReservationGuest(request, propertyId);
        }

        public void ConfirmReservation(long reservationId)
        {
            if (reservationId <= 0)
                NotifyIdIsMissing();

            if (Notification.HasNotification())
                return;
 
            _reservationDomainService.ConfirmReservation(reservationId);
        }

        public ReservationHeaderResultDto GetReservationHeader(long reservationId)
        {
            if (reservationId <= 0)
                NotifyIdIsMissing();

            if (Notification.HasNotification())
                return null;

            return _reservationReadRepository.GetReservationHeader(reservationId);
        }

        private void RiseActionsAfterReservationItemChangeStatus(ReservationItemDto reservationItemDto, 
            ReservationStatus statusNew, ReservationStatus? statusOld = null)
        {
            foreach (var observable in _reservationItemChangeStatusObservableList)
            {
                var dtoObservable = new ReservationItemChangeStatusObservableDto
                {
                    RoomTypeId = reservationItemDto.ReceivedRoomTypeId,
                    ReservationItemStatusIdNew = statusNew,
                    ReservationItemStatusIdOld = statusOld,
                    InitialDate = reservationItemDto.EstimatedArrivalDate,
                    EndDate = reservationItemDto.EstimatedDepartureDate
                };

                observable.ExecuteActionAfterChangeReservationItemStatus(dtoObservable);
            }
        }

        private void RiseActionsAfterReservationItemChangeRoomTypeOrPeriod(ReservationItem existingReservationItem, ReservationItemDto reservationItemDto)
        {
            foreach (var observable in _reservationItemChangeRoomTypeOrPeriodObservableList)
            {
                var dtoObservable = new ReservationItemChangeRoomTypeOrPeriodObservableDto
                {
                    OldRoomTypeId = existingReservationItem.ReceivedRoomTypeId,
                    OldInitialDate = existingReservationItem.CheckInDate ?? existingReservationItem.EstimatedArrivalDate,
                    OldEndDate = existingReservationItem.EstimatedDepartureDate,
                    NewRoomTypeId = reservationItemDto.ReceivedRoomTypeId,
                    NewInitialDate = reservationItemDto.EstimatedArrivalDate,
                    NewEndDate = reservationItemDto.EstimatedDepartureDate,
                };

                observable.ExecuteActionAfterReservationItemChangeRoomTypeOrPeriod(dtoObservable);
            }
        }

        private void ValidatePeriodReservation(ReservationDto dto)
        {
            if (dto.ReservationItemList.Any(ri => ri.EstimatedArrivalDate > ri.EstimatedDepartureDate))
            {
                Notification.Raise(Notification.DefaultBuilder
                     .WithMessage(AppConsts.LocalizationSourceName, ReservationItem.EntityError.ReservationItemEstimatedArrivalDateMustBeGreaterEstimatedDepartureDate)
                     .WithDetailedMessage(AppConsts.LocalizationSourceName, ReservationItem.EntityError.ReservationItemEstimatedArrivalDateMustBeGreaterEstimatedDepartureDate)
                     .Build());
            }
        }

        public NewReservationCodeDto GetNewReservationCode()
            => new NewReservationCodeDto(ReservationCodeGeneratorHelper.GenerateTimeBasedShortGuid().ToString());
    }
}
