﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Thex.Application.Adapters;
using Thex.Application.Interfaces;
using Thex.Common;
using Thex.Common.Enumerations;
using Thex.Domain.Entities;
using Thex.Domain.Interfaces.Repositories;
using Thex.Domain.Services.Interfaces;
using Thex.Dto;
using Thex.Infra.ReadInterfaces;
using Thex.Kernel;
using Tnf.Dto;
using Tnf.Notifications;
using Tnf.Repositories.Uow;

namespace Thex.Application.Services
{
    public class GuestEntranceAppService : ScaffoldReservationBudgetAppService, IGuestEntranceAppService
    {
        protected readonly IReservationBudgetAppService _reservationBudgetAppService;
        protected readonly IGuestRegistrationAppService _guestRegistrationAppService;
        protected readonly IGuestReservationItemAppService _guestReservationItemAppService;
        protected readonly IGuestAppService _guestAppService;
        protected readonly IPersonAppService _personAppService;
        protected readonly IReservationAppService _reservationAppService;
        protected readonly IReservationItemAppService _reservationItemAppService;
        protected readonly IRoomAppService _roomAppService;
        protected readonly IRoomTypeAppService _roomTypeAppService;
        protected readonly IReservationBudgetRepository _reservationBudgetRepository;
        protected readonly IReservationBudgetAdapter _reservationBudgetAdapter;
        protected readonly IPropertyParameterReadRepository _propertyParameterReadRepository;
        protected readonly IApplicationUser _applicationUser;
        protected readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IReservationItemReadRepository _reservationItemReadRepository;

        public GuestEntranceAppService(
            IReservationBudgetAppService reservationBudgetAppService,
            IGuestRegistrationAppService guestRegistrationAppService,
            IReservationItemAppService reservationItemAppService,
            IGuestReservationItemAppService guestReservationItemAppService,
            IGuestAppService guestAppService,
            IPersonAppService personAppService,
            IReservationAppService reservationAppService,
            IReservationBudgetAdapter reservationBudgetAdapter,
            IReservationBudgetDomainService reservationBudgetDomainService,
            IRoomAppService roomAppService,
            IRoomTypeAppService roomTypeAppService,
            IReservationBudgetRepository reservationBudgetRepository,
            IPropertyParameterReadRepository propertyParameterReadRepository,
            IReservationItemReadRepository reservationItemReadRepository,
            IApplicationUser applicationUser,
            IUnitOfWorkManager unitOfWorkManager,
            INotificationHandler notificationHandler)
            : base(reservationBudgetAdapter, reservationBudgetDomainService, notificationHandler)
        {
            _reservationBudgetAppService = reservationBudgetAppService;
            _reservationItemAppService = reservationItemAppService;
            _guestRegistrationAppService = guestRegistrationAppService;
            _guestReservationItemAppService = guestReservationItemAppService;
            _guestAppService = guestAppService;
            _personAppService = personAppService;
            _reservationAppService = reservationAppService;
            _roomAppService = roomAppService;
            _roomTypeAppService = roomTypeAppService;
            _reservationBudgetAdapter = reservationBudgetAdapter;
            _reservationBudgetRepository = reservationBudgetRepository;
            _propertyParameterReadRepository = propertyParameterReadRepository;
            _applicationUser = applicationUser;
            _unitOfWorkManager = unitOfWorkManager;
            _reservationItemReadRepository = reservationItemReadRepository;
        }

        public async Task<GuestRegistrationDto> CreateGuest(GuestRegistrationDto dto)
        {
            ValidateDto<GuestRegistrationDto>(dto, nameof(dto));

            if (Notification.HasNotification())
                return GuestRegistrationDto.NullInstance;

            return await _guestRegistrationAppService.Create(dto, false);
        }

        public async Task<GuestRegistrationDto> UpdateGuest(Guid id, GuestRegistrationDto dto)
        {
            ValidateDto<GuestRegistrationDto>(dto, nameof(dto));

            if (Notification.HasNotification())
                return GuestRegistrationDto.NullInstance;

            return await _guestRegistrationAppService.Update(id, dto, false);
        }

        public async Task<ReservationItemBudgetHeaderDto> GetWithBudgetOffer(ReservationItemBudgetRequestDto requestDto, int propertyId)
        {
            ValidateDto<ReservationItemBudgetHeaderDto>(requestDto, nameof(requestDto));

            if (Notification.HasNotification())
                return ReservationItemBudgetHeaderDto.NullInstance;

            var systemDate = _propertyParameterReadRepository.GetSystemDate(propertyId);
            ValidateSystemDateNull(systemDate);

            if (Notification.HasNotification())
                return ReservationItemBudgetHeaderDto.NullInstance;

            var ret = await _reservationBudgetAppService.GetWithBudgetOffer(requestDto, propertyId);

            if (Notification.HasNotification())
                return ReservationItemBudgetHeaderDto.NullInstance;

            if(ret.MealPlanTypeId.HasValue && ret.CurrencyId.HasValue)
            {
                var commercialBudgetDayByDay = new List<ReservationItemCommercialBudgetDayDto>();
                var budgetWithoutMealPlanType = ret.CommercialBudgetList.FirstOrDefault(e => e.MealPlanTypeId == (int)MealPlanTypeEnum.None && e.CurrencyId == ret.CurrencyId.Value);
                var defaultBudget = ret.CommercialBudgetList.FirstOrDefault(cb => cb.MealPlanTypeId == ret.MealPlanTypeId.Value && (ret.CurrencyId.HasValue && cb.CurrencyId == ret.CurrencyId.Value) && cb.RatePlanId == requestDto.RatePlanId);
                if(defaultBudget == null)
                    defaultBudget = ret.CommercialBudgetList.FirstOrDefault(cb => cb.MealPlanTypeId == ret.MealPlanTypeId.Value && (ret.CurrencyId.HasValue && cb.CurrencyId == ret.CurrencyId.Value));

                foreach (var reservationBudgetDay in ret.ReservationBudgetDtoList.Select(e => new { e.BudgetDay, e.MealPlanTypeId, e.CurrencyId}))
                {
                    var budget = ret.CommercialBudgetList.FirstOrDefault(e => e.MealPlanTypeId == reservationBudgetDay.MealPlanTypeId && e.CurrencyId == reservationBudgetDay.CurrencyId && e.RatePlanId == defaultBudget.RatePlanId);
                    var budgetDay = budget.CommercialBudgetDayList.FirstOrDefault(e => e.Day == reservationBudgetDay.BudgetDay);
                    var day = budgetDay ?? budgetWithoutMealPlanType.CommercialBudgetDayList.FirstOrDefault(e => e.Day == reservationBudgetDay.BudgetDay);

                    if (day != null)
                        commercialBudgetDayByDay.Add(day);
                }

                defaultBudget.CommercialBudgetDayList = commercialBudgetDayByDay;

                ret.CommercialBudgetList = new List<ReservationItemCommercialBudgetDto> { defaultBudget };
            }

            var totalReservationBudget = ret.ReservationBudgetDtoList.Where(e => e.BudgetDay < systemDate.Value.Date).Sum(e => e.ManualRate);

            foreach (var item in ret.CommercialBudgetList)
            {
                item.CommercialBudgetDayList = item.CommercialBudgetDayList.Where(x => x.Day.Date >= systemDate.Value.Date).ToList();
                item.TotalBudget = item.CommercialBudgetDayList.Sum(x => x.Total) + totalReservationBudget;
            }

            return ret;
        }

        public async Task ConfirmEntrance(ReservationItemGuestDto requestDto)
        {
            ValidateDto<ReservationItemGuestDto>(requestDto, nameof(requestDto));

            if (Notification.HasNotification())
                return;

            var propertyId = _applicationUser.PropertyId.TryParseToInt32();
            var systemDate = _propertyParameterReadRepository.GetSystemDate(propertyId);
            ValidateSystemDateNull(systemDate);

            if (Notification.HasNotification())
                return;

            using (var uow = _unitOfWorkManager.Begin())
            {
                var reservationItemDto = await _reservationItemAppService.Get(new DefaultLongRequestDto(requestDto.ReservationItemId));

                ValidateReservationItem(reservationItemDto);

                if (Notification.HasNotification())
                    return;

                var reservationItemParent = _reservationItemReadRepository.GetReservationItemParent(reservationItemDto.ReservationId, reservationItemDto.Id);

                ValidateReservationBudget(requestDto.reservationItemBudgetHeaderDto.ReservationBudgetDtoList, reservationItemParent?.CurrencyId ?? null);

                if (Notification.HasNotification()) return;

                var guestRegistrationDto = await _guestRegistrationAppService.GetById(requestDto.GuestRegistrationId);

                if (Notification.HasNotification())
                    return;

                ValidateCapacity(guestRegistrationDto.IsChild, reservationItemDto, reservationItemDto.RoomId ?? 0);

                if (Notification.HasNotification())
                    return;

                var guestEntranceDto = new GuestReservationItemEntranceDto()
                {
                    IsGratuity = requestDto.reservationItemBudgetHeaderDto == null,
                    IsSeparated = (requestDto.reservationItemBudgetHeaderDto != null && requestDto.reservationItemBudgetHeaderDto.IsSeparated),
                    PaxPosition = Convert.ToInt16(!guestRegistrationDto.IsChild ? (reservationItemDto.AdultCount + 1) : (reservationItemDto.ChildCount + 1))
                };

                var guestReservationItem = await _guestReservationItemAppService.CreateByEntrance(guestRegistrationDto, reservationItemDto, guestEntranceDto);

                if (Notification.HasNotification())
                    return;

                if (requestDto.reservationItemBudgetHeaderDto != null)
                {
                    _reservationBudgetAppService.UpdateEntranceBudget(
                        requestDto.reservationItemBudgetHeaderDto.ReservationBudgetDtoList,
                        requestDto.ReservationItemId,
                        requestDto.reservationItemBudgetHeaderDto.IsSeparated,
                        systemDate
                    );

                    if (Notification.HasNotification())
                        return;
                }

                await _reservationItemAppService.UpdateByEntrance(reservationItemDto, guestRegistrationDto.IsChild);

                if (Notification.HasNotification())
                    return;

                _guestReservationItemAppService.ConfirmEntranceCheckin(guestReservationItem, reservationItemDto.ReservationId, propertyId, systemDate);

                if (Notification.HasNotification())
                    return;

                uow.Complete();
            }
        }

        private void ValidateReservationBudget(ICollection<ReservationBudgetDto> reservationBudgetList, Guid? currencyId)
        {
            if (currencyId.HasValue && !reservationBudgetList.All(rb => rb.CurrencyId == currencyId))
            {
                Notification.Raise(Notification.DefaultBuilder
                         .WithMessage(AppConsts.LocalizationSourceName, Reservation.EntityError.ReservationMustHaveSameCurrencyForAllReservationItems)
                         .WithDetailedMessage(AppConsts.LocalizationSourceName, Reservation.EntityError.ReservationMustHaveSameCurrencyForAllReservationItems)
                         .Build());

                return;
            }

            if (reservationBudgetList.Select(rb => rb.CurrencyId).Distinct().Count() > 1)
                Notification.Raise(Notification.DefaultBuilder
                         .WithMessage(AppConsts.LocalizationSourceName, ReservationItem.EntityError.ReservationItemMustHaveSameCurrencyForAllReservationBudgets)
                         .WithDetailedMessage(AppConsts.LocalizationSourceName, ReservationItem.EntityError.ReservationItemMustHaveSameCurrencyForAllReservationBudgets)
                         .Build());
        }

        private void ValidateCapacity(bool isChild, ReservationItemDto reservationItemDto, int roomId)
        {
            var roomTypeDto = _roomTypeAppService.GetRoomTypeByRoomId(roomId);

            if (isChild && reservationItemDto.ChildCount >= roomTypeDto.ChildCapacity)
            {
                Notification.Raise(Notification.DefaultBuilder
                .WithMessage(AppConsts.LocalizationSourceName, ReservationItem.EntityError.ReservationItemOverChildCapacityOfRoomType)
                .WithDetailedMessage(AppConsts.LocalizationSourceName, ReservationItem.EntityError.ReservationItemOverChildCapacityOfRoomType)
                .Build());
            }

            if (!isChild && reservationItemDto.AdultCount >= roomTypeDto.AdultCapacity)
            {
                Notification.Raise(Notification.DefaultBuilder
                .WithMessage(AppConsts.LocalizationSourceName, ReservationItem.EntityError.ReservationItemOverAdultCapacityOfRoomType)
                .WithDetailedMessage(AppConsts.LocalizationSourceName, ReservationItem.EntityError.ReservationItemOverAdultCapacityOfRoomType)
                .Build());
            }

        }

        private void ValidateReservationItem(ReservationItemDto reservationItemDto)
        {
            if (reservationItemDto.ReservationItemStatusId != (int)ReservationStatus.Checkin)
            {
                Notification.Raise(Notification.DefaultBuilder
                .WithMessage(AppConsts.LocalizationSourceName, ReservationItem.EntityError.EntranceReservationNotInCheckIn)
                .WithDetailedMessage(AppConsts.LocalizationSourceName, ReservationItem.EntityError.EntranceReservationNotInCheckIn)
                .Build());
            }
        }

        private void ValidateSystemDateNull(DateTime? systemDate)
        {
            if (systemDate == null)
            {
                Notification.Raise(Notification.DefaultBuilder
                .WithMessage(AppConsts.LocalizationSourceName, PropertyAuditProcess.EntityError.PropertyAuditProcessInvalidPropertySystemDate)
                .WithDetailedMessage(AppConsts.LocalizationSourceName, PropertyAuditProcess.EntityError.PropertyAuditProcessInvalidPropertySystemDate)
                .Build());
            }
        }
    }
}
