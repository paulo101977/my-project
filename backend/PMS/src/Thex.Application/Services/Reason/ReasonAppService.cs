﻿using System;
using Thex.Application.Adapters;
using Thex.Domain.Entities;
using Thex.Application.Interfaces;
using Tnf.Domain.Services;
using Thex.Dto;
using Tnf.Dto;
using Thex.Common.Enumerations;
using Thex.Infra.ReadInterfaces;
using Thex.Domain.Interfaces.Repositories;
using Tnf.Notifications;

namespace Thex.Application.Services
{
    public class ReasonAppService : ScaffoldReasonAppService, IReasonAppService
    {
        protected readonly IReasonReadRepository _reasonReadRepository;
        protected readonly IReasonRepository _reasonRepository;
        protected readonly IChainReadRepository _chainReadRepository;

        public ReasonAppService(
            IReasonRepository reasonRepository,
            IReasonReadRepository reasonReadRepository,
            IChainReadRepository chainReadRepository,
            IReasonAdapter reasonAdapter,
            IDomainService<Reason> reasonDomainService,
            INotificationHandler notificationHandler)
            : base(reasonAdapter, reasonDomainService, notificationHandler)
        {
            _reasonReadRepository = reasonReadRepository;
            _chainReadRepository = chainReadRepository;
            _reasonRepository = reasonRepository;


        }

        public IListDto<ReasonDto> GetAllByPropertyId(GetAllReasonDto request, int propertyId)
        {
            return _reasonReadRepository.GetAllByPropertyId(request, propertyId);
        }

        public IListDto<ReasonDto> GetAllByChainIdAndReasonCategory(GetAllReasonDto request)
        {
            var nullResponseInstance = new ListDto<ReasonDto>();
            int propertyId = request.PropertyId;
            ReasonCategoryEnum reasonCategoryId = request.ReasonCategoryId.Value;
            if (propertyId <= 0 || !Enum.IsDefined(typeof(ReasonCategoryEnum), reasonCategoryId))
            {
                NotifyParameterInvalid();
                return nullResponseInstance;
            }

            int chainId = _chainReadRepository.GetChainByPropertyId(propertyId).Id;
            return _reasonReadRepository.GetAllByChainIdAndReasonCategory(request, chainId, reasonCategoryId);
        }

        public void CreateReason(ReasonDto reasonDto)
        {
            if (reasonDto.PropertyId == 0)
                NotifyRequired("PropertyId");

            if (Notification.HasNotification())
                return;

            reasonDto.ChainId = _chainReadRepository.GetChainByPropertyId(reasonDto.PropertyId).Id;
            var builder = ReasonAdapter.Map(reasonDto);

            if (Notification.HasNotification())
                return;

            ReasonDomainService.InsertAndSaveChanges(builder);
        }

        public ReasonDto GetReasonByReasonId(int reasonId)
        {
            var response = _reasonReadRepository.GetReasonByReasonId(reasonId);
            return response;
        }

        public void ToggleAndSaveIsActive(int id)
        {
            _reasonRepository.ToggleAndSaveIsActive(id);
        }

        public void Update(ReasonDto reasonDto)
        {
            if (reasonDto.PropertyId == 0)
                NotifyRequired("PropertyId");

            if (Notification.HasNotification())
                return;

            reasonDto.ChainId = _chainReadRepository.GetChainByPropertyId(reasonDto.PropertyId).Id;

            var reason = ReasonAdapter.Map(reasonDto).Build();
            _reasonRepository.Update(reason);
        }

        public new void Delete(int id)
        {
            _reasonRepository.Delete(id);
        }

        public IListDto<ReasonDto> GetAllByPropertyIdAndReasonCategoryId(GetAllReasonDto request, int propertyId, int reasonCategoryId)
        {
            return _reasonReadRepository.GetAllByPropertyIdAndReasonCategoryId(request, propertyId, reasonCategoryId);
        }
     
    }
}
