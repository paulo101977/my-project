﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Thex.Application.Interfaces;
using Thex.Common;
using Thex.Common.Enumerations;
using Thex.Kernel;
using Thex.Domain.Services.Interfaces;
using Thex.Dto;
using Tnf.Dto;
using Tnf.Notifications;
using Tnf.Repositories.Uow;
using Thex.Dto.ExternalRegistration;
using Thex.AspNetCore.Security.Interfaces;
using Microsoft.Extensions.Configuration;
using Thex.Domain.Interfaces.Repositories.ReadDapper;
using Tnf.Localization;
using Thex.Common.Emails.AwsSes;
using Thex.Common.Message;
using Thex.Domain.Interfaces.Factories;
using Thex.Dto.Email;
using System.Globalization;
using Microsoft.AspNetCore.Http;

namespace Thex.Application.Services
{
    public class ExternalRegistrationAppService : ApplicationServiceBase, IExternalRegistrationAppService
    {
        protected readonly IUnitOfWorkManager _unitOfWorkManager;
        protected readonly IApplicationUser _applicationUser;
        protected readonly INotificationHandler _notificationHandler;
        protected readonly IExternalRegistrationDomainService _externalRegistrationDomainService;
        protected readonly ITokenManager _tokenManager;
        protected readonly IConfiguration _configuration;
        protected readonly IReservationDapperReadRepository _reservationDapperReadRepository;
        protected readonly IReservationItemDapperReadRepository _reservationItemDapperReadRepository;
        protected readonly IGuestReservationItemDapperReadRepository _guestReservationItemDapperReadRepository;
        protected readonly ICountrySubdivisionAppService _countrySubdivisionAppService;
        protected readonly ILocalizationManager _localizationManager;
        protected readonly IEmailFactory _emailFactory;
        protected readonly IDocumentAppService _documentAppService;
        protected readonly IHttpContextAccessor _httpContextAccessor;
        protected readonly string _frontUrl;

        protected readonly string _host;
        protected readonly int _port;
        protected readonly string _userName;
        protected readonly string _passsword;

        public ExternalRegistrationAppService(
            IReservationDapperReadRepository reservationDapperReadRepository,
            IReservationItemDapperReadRepository reservationItemDapperReadRepository,
            IExternalRegistrationDomainService externalRegistrationDomainService,
            IGuestReservationItemDapperReadRepository guestReservationItemDapperReadRepository,
            ICountrySubdivisionAppService countrySubdivisionAppService,
            IDocumentAppService documentAppService,
            IEmailFactory emailFactory,
            ILocalizationManager localizationManager,
            ITokenManager tokenManager,
            IUnitOfWorkManager unitOfWorkManager,
            INotificationHandler notificationHandler,
            IApplicationUser applicationUser,
            IConfiguration configuration,
            IHttpContextAccessor httpContextAccessor)
            : base(notificationHandler)
        {
            _reservationDapperReadRepository = reservationDapperReadRepository;
            _reservationItemDapperReadRepository = reservationItemDapperReadRepository;
            _externalRegistrationDomainService = externalRegistrationDomainService;
            _guestReservationItemDapperReadRepository = guestReservationItemDapperReadRepository;
            _localizationManager = localizationManager;
            _tokenManager = tokenManager;
            _unitOfWorkManager = unitOfWorkManager;
            _applicationUser = applicationUser;
            _notificationHandler = notificationHandler;
            _configuration = configuration;
            _httpContextAccessor = httpContextAccessor;
            _emailFactory = emailFactory;
            _documentAppService = documentAppService;
            _countrySubdivisionAppService = countrySubdivisionAppService;

            _frontUrl = _configuration.GetValue<string>("UrlFront");

            _host = _configuration.GetValue<string>("AwsEmailHost");
            _port = Convert.ToInt32(_configuration.GetValue<string>("AwsEmailPort"));
            _userName = _configuration.GetValue<string>("AwsEmailUserName");
            _passsword = _configuration.GetValue<string>("AwsEmailPassword");
        }
        
        public async Task<string> CreateAsync(Guid reservationUid, string email)
        {
            if (!await _reservationDapperReadRepository.AnyByReservationUidAsync(reservationUid))
            {
                NotifyParameterInvalid();
                return null;
            }

            var externalLink = string.Empty;
            using (var uow = _unitOfWorkManager.Begin())
            {
                externalLink = await GenerateExternalLink(reservationUid, email);

                await _externalRegistrationDomainService.CreateAsync(reservationUid, email, externalLink);

                if (Notification.HasNotification())
                    return null;
                
                SendMail(email, externalLink);

                await uow.CompleteAsync();
            }

            return externalLink;
        }

        public async Task<ExternalRoomListHeaderDto> GetRoomListAsync()
        {
            var reservationUid = _httpContextAccessor.HttpContext.GetReservationUid();

            var header = await _reservationItemDapperReadRepository.GetHeaderByReservationUid(reservationUid);
            var list = await _reservationItemDapperReadRepository.GetExternalListByReservationUid(reservationUid);

            header.ExternalRoomListDto = list;

            return header;
        }

        public async Task<IListDto<ExternalGuestRegistrationDto>> GetRegistrationListAsync(Guid reservationItemUid)
        {
            var tasks = new List<Task>();

            var taskGuestRegistration = _guestReservationItemDapperReadRepository.GetGuestRegistrationListByReservationItemUid(reservationItemUid);
            var taskExternalGuestRegistration = _guestReservationItemDapperReadRepository.GetExternalGuestListByReservationItemUid(reservationItemUid);

            await Task.WhenAll(tasks);

            var guestRegistrationList = taskGuestRegistration.Result;
            var externalGuestRegistrationList = taskExternalGuestRegistration.Result;

            return ProcessRegistrationList(externalGuestRegistrationList, guestRegistrationList);
        }

        public async Task<ExternalGuestRegistrationDto> GetRegistrationAsync(long guestReservationItemId)
        {
            var tasks = new List<Task>();

            var taskGuestRegistration = _guestReservationItemDapperReadRepository.GetGuestRegistrationByGuestReservationItemId(guestReservationItemId);
            var taskExternalGuestRegistration = _guestReservationItemDapperReadRepository.GetExternalGuestByGuestReservationItemId(guestReservationItemId);

            await Task.WhenAll(tasks);

            var externalGuestRegistration = taskExternalGuestRegistration.Result;
            var guestRegistration = taskGuestRegistration.Result;

            return ProcessRegistration(externalGuestRegistration, guestRegistration);
        }

        public async Task<ExternalGuestRegistrationDto> UpdateAsync(long guestReservationItemId, ExternalGuestRegistrationDto dto)
        {
            var reservationUid = _httpContextAccessor.HttpContext.GetReservationUid();

            if (!await _reservationDapperReadRepository.AnyByGuestReservationItemIdAsync(reservationUid, guestReservationItemId))
            {
                NotifyParameterInvalid();
                return null;
            }

            using (var uow = _unitOfWorkManager.Begin())
            {
                dto.GuestReservationItemId = guestReservationItemId;

                dto.SetFirstAndLastNameBasedOnFullName();

                ValidateDocument(dto);

                if (Notification.HasNotification())
                    return null;
                
                SetLocationAndCreateLocation(dto);

                await _externalRegistrationDomainService.UpdateExternalGuestAsync(dto);

                await uow.CompleteAsync();
            }

            return dto;
        }

        private async Task<string> GenerateExternalLink(Guid reservationUid, string email)
        {
            var departureDate = await _reservationItemDapperReadRepository.GetMaxDepartureDateByReservationUidAsync(reservationUid);

            departureDate = departureDate.Date.AddHours(23).AddMinutes(59).AddSeconds(59);

            var expirationTime = Convert.ToInt32(DateTime.UtcNow.Subtract(departureDate).TotalSeconds);

            if (expirationTime < 0)
                expirationTime = expirationTime * -1;

            return _tokenManager.CreateRoomListExternalLink(_frontUrl, email, expirationTime, reservationUid, Guid.Empty);
        }

        private void SendMail(string email, string link)
        {
            var emailDto = new EmailExternalAccessDto()
            {
                Title = _localizationManager.GetString(AppConsts.LocalizationSourceName, EmailEnum.EmailSubjectExternalAccessReservation.ToString()),
                Body = _localizationManager.GetString(AppConsts.LocalizationSourceName, EmailEnum.EmailBodyExternalAccessReservation.ToString()),
                ExternalLink = link
            };

            var message = EmailMessageBuilder
                            .Init()
                            .AddSubject(emailDto.Title)
                            .AddFrom(_configuration.GetValue<string>("EmailNoReply"))
                            .AddBody(_emailFactory.GenerateEmail(EmailTemplateEnum.EmailExternalAccessReservation, emailDto))
                            .AddTo(email)
                            .Build();

            IAwsEmailSender sender = new AwsEmailSender(new AwsEmailSettings(_host, _port, _userName, _passsword), _configuration);

            sender.Send(message, true);
        }

        private IListDto<ExternalGuestRegistrationDto> ProcessRegistrationList(List<ExternalGuestRegistrationDto> externalGuestRegistrationList, List<GuestRegistrationDapperDto> guestRegistrationList)
        {
            var resultList = new List<ExternalGuestRegistrationDto>();

            foreach (var externalGuestRegistration in externalGuestRegistrationList)
            {
                var guestRegistration = guestRegistrationList.FirstOrDefault(e => e.GuestReservationItemId == externalGuestRegistration.GuestReservationItemId);

                if (guestRegistration != null)
                {
                    var externalGuestDb = guestRegistration.ToExternalGuestRegistrationDto(externalGuestRegistration.Id, externalGuestRegistration.ExternalRegistrationEmail);
                    resultList.Add(externalGuestDb);
                }
                else
                {
                    externalGuestRegistration.IsEditable = true;
                    resultList.Add(externalGuestRegistration);
                }
            }

            return new ListDto<ExternalGuestRegistrationDto>
            {
                HasNext = false,
                Items = resultList
            };
        }

        private ExternalGuestRegistrationDto ProcessRegistration(ExternalGuestRegistrationDto externalGuestRegistration, GuestRegistrationDapperDto guestRegistration)
        {
            if (guestRegistration != null)
                return guestRegistration.ToExternalGuestRegistrationDto(externalGuestRegistration.Id, externalGuestRegistration.ExternalRegistrationEmail);
            else
                externalGuestRegistration.IsEditable = true;

            return externalGuestRegistration;
        }

        private void SetLocationAndCreateLocation(ExternalGuestRegistrationDto dto)
        {
            if (!string.IsNullOrEmpty(dto.StreetName) && !string.IsNullOrEmpty(dto.StreetNumber) && !string.IsNullOrEmpty(dto.CountryCode))
            {
                var countrySubdivision = _countrySubdivisionAppService.CreateCountrySubdivisionTranslateAndGetCityId(dto.GetLocationDto(), CultureInfo.CurrentCulture.Name);

                dto.CityId = countrySubdivision.Item1;
                dto.StateId = countrySubdivision.Item2;
                dto.CountryId = countrySubdivision.Item3;
            }
        }

        private void ValidateDocument(ExternalGuestRegistrationDto dto)
        {
            if (!string.IsNullOrEmpty(dto.DocumentInformation) && dto.DocumentTypeId.HasValue)
            {
                var documentBuilder = _documentAppService.GetBuilder(new DocumentDto
                {
                    DocumentInformation = dto.DocumentInformation,
                    DocumentTypeId = dto.DocumentTypeId.Value
                });

                documentBuilder.Build();
            }
        }
    }
}