﻿using System;
using Thex.Application.Adapters;
using Thex.Domain.Entities;
using Thex.Application.Interfaces;
using Tnf.Domain.Services;
using Tnf.Notifications;
using System.Threading.Tasks;
using Thex.Dto;
using Tnf.Dto;
using Thex.Infra.ReadInterfaces;

namespace Thex.Application.Services
{
    public class ChannelCodeAppService : ScaffoldChannelCodeAppService, IChannelCodeAppService
    {
        private readonly IChannelCodeReadRepository _channelCodeReadRepository;

        public ChannelCodeAppService(
            IChannelCodeAdapter channelCodeAdapter,
            IChannelCodeReadRepository channelCodeReadRepository,
            IDomainService<ChannelCode> channelCodeDomainService,
            INotificationHandler notificationHandler)
                : base(channelCodeAdapter, channelCodeDomainService, notificationHandler)
        {
            _channelCodeReadRepository = channelCodeReadRepository;
        }

        public async Task<IListDto<ChannelCodeDto>> GetAllChannelsCodeDtoAsync()
        {
            return await _channelCodeReadRepository.GetAllChannelsCodeDtoAsync();
        }

        public ChannelCodeDto GetDtoById(int id)
        {
            return _channelCodeReadRepository.GetDtoById(id);
        }
    }
}
