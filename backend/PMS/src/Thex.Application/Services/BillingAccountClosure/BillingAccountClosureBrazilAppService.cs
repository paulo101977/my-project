﻿using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Thex.Application.Interfaces;
using Thex.Common;
using Thex.Common.Enumerations;
using Thex.Common.Helpers;
using Thex.Domain.Entities;
using Thex.Domain.Interfaces.Repositories;
using Thex.Dto;
using Thex.Dto.BillingAccountClosure;
using Thex.Infra.ReadInterfaces;
using Thex.Kernel;
using Tnf.Localization;
using Tnf.Notifications;
using Tnf.Repositories;
using Tnf.Repositories.Uow;

namespace Thex.Application.Services
{
    public class BillingAccountClosureBrazilAppService : ApplicationServiceBase, IBillingAccountClosureBrazilAppService
    {
        private readonly string _deniedMessage = "Negada - 539";
        private readonly IBillingAccountAppService _billingAccountAppService;
        private readonly IBillingAccountItemAppService _billingAccountItemAppService;
        private readonly IBillingItemReadRepository _billingItemReadRepository;
        private readonly IBillingAccountRepository _billingAccountRepository;
        private readonly IBillingAccountReadRepository _billingAccountReadRepository;
        private readonly IBillingInvoicePropertySupportedTypeReadRepository _billingInvoicePropertySupportedTypeReadRepository;
        private readonly IBillingInvoiceAppService _billingInvoiceAppService;
        private readonly IBillingInvoicePropertyRepository _billingInvoicePropertyRepository;
        private readonly IBillingAccountItemReadRepository _billingAccountItemReadRepository;
        private readonly ILocalizationManager _localizationManager;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IIntegrationAppService _integrationAppService;
        private readonly IReservationItemReadRepository _reservationItemReadRepository;
        private readonly IRepository<ReservationItem> _reservationItemRepository;
        private readonly IReservationRepository _reservationRepository;
        private readonly IGuestReservationItemReadRepository _guestReservationItemReadRepository;
        private readonly IGuestReservationItemRepository _guestReservationItemRepository;
        private readonly IGuestRegistrationReadRepository _guestRegistrationReadRepository;
        private readonly IApplicationUser _applicationUser;
        private readonly ICompanyClientReadRepository _companyClientReadRepository;
        private readonly IPersonReadRepository _personReadRepository;
        private readonly INotificationHandler _notificationHandler;
        private readonly IPropertyParameterReadRepository _propertyParameterReadRepository;
        private readonly IBillingInvoiceModelReadRepository _billingInvoiceModelReadRepository;

        public BillingAccountClosureBrazilAppService(
            IUnitOfWorkManager unitOfWorkManager,
            IBillingAccountAppService billingAccountAppService,
            IBillingAccountItemAppService billingAccountItemAppService,
            IBillingItemReadRepository billingItemReadRepository,
            IBillingAccountRepository billingAccountRepository,
            IBillingAccountReadRepository billingAccountReadRepository,
            IBillingInvoicePropertySupportedTypeReadRepository billingInvoicePropertySupportedTypeReadRepository,
            IBillingInvoiceAppService billingInvoiceAppService,
            IReservationItemReadRepository reservationItemReadRepository,
            IRepository<ReservationItem> reservationItemRepository,
            IGuestReservationItemReadRepository guestReservationItemReadRepository,
            IGuestReservationItemRepository guestReservationItemRepository,
            IGuestRegistrationReadRepository guestRegistrationReadRepository,
            IReservationRepository reservationRepository,
            IBillingInvoicePropertyRepository billingInvoicePropertyRepository,
            IBillingAccountItemReadRepository billingAccountItemReadRepository,
            ILocalizationManager localizationManager,
            IApplicationUser applicationUser,
            ICompanyClientReadRepository companyClientReadRepository,
            IPersonReadRepository personReadRepository,
            INotificationHandler notificationHandler,
            IIntegrationAppService integrationAppService,
            IPropertyParameterReadRepository propertyParameterReadRepository,
            IBillingInvoiceModelReadRepository billingInvoiceModelReadRepository)
               : base(notificationHandler)
        {
            _unitOfWorkManager = unitOfWorkManager;
            _billingAccountAppService = billingAccountAppService;
            _billingAccountItemAppService = billingAccountItemAppService;
            _billingItemReadRepository = billingItemReadRepository;
            _billingAccountRepository = billingAccountRepository;
            _billingAccountReadRepository = billingAccountReadRepository;
            _billingInvoicePropertySupportedTypeReadRepository = billingInvoicePropertySupportedTypeReadRepository;
            _billingInvoiceAppService = billingInvoiceAppService;
            _billingInvoicePropertyRepository = billingInvoicePropertyRepository;
            _billingAccountItemReadRepository = billingAccountItemReadRepository;
            _localizationManager = localizationManager;
            _integrationAppService = integrationAppService;
            _reservationItemReadRepository = reservationItemReadRepository;
            _reservationItemRepository = reservationItemRepository;
            _reservationRepository = reservationRepository;
            _guestReservationItemReadRepository = guestReservationItemReadRepository;
            _guestReservationItemRepository = guestReservationItemRepository;
            _guestRegistrationReadRepository = guestRegistrationReadRepository;
            _applicationUser = applicationUser;
            _companyClientReadRepository = companyClientReadRepository;
            _personReadRepository = personReadRepository;
            _notificationHandler = notificationHandler;
            _propertyParameterReadRepository = propertyParameterReadRepository;
            _billingInvoiceModelReadRepository = billingInvoiceModelReadRepository;
        }

        public virtual async Task<BillingAccountClosureResultDto> Create(BillingAccountClosureDto dto)
        {
            NotificationResponseDto notificationDto = null;
            BillingAccountClosureResultDto result;

            using (var uow = _unitOfWorkManager.Begin())
            {
                ValidateDto<BillingAccountClosureDto>(dto, nameof(dto));

                if (Notification.HasNotification())
                    return null;

                //não pode ter conta bloqueada 
                //nao pode ter conta já encerrada
                if (_billingAccountReadRepository.AnyClosedOrBlockedAccount(dto.BillingAccountList.ToList()))
                {
                    Notification.Raise(Notification.DefaultBuilder
                    .WithMessage(AppConsts.LocalizationSourceName, BillingAccountEnum.Error.BillingAccountWithBillingAccountItemsCanNotBeClosedOrBlocked)
                    .WithDetailedMessage(AppConsts.LocalizationSourceName, BillingAccountEnum.Error.BillingAccountWithBillingAccountItemsCanNotBeClosedOrBlocked)
                    .Build());
                }

                if (Notification.HasNotification())
                    return null;

                //processar
                if (dto.OwnerDestination != null && dto.OwnerDestination != Guid.Empty)
                    result = AccountClosureProccessWithTransfer(dto);
                else if (dto.Credit == null)
                    result = CloseBillingAccount(dto.BillingAccountList.First(), dto.PropertyId);
                else
                    result = AccountClosureProccess(dto);

                if (Notification.HasNotification())
                    return null;

                if (result.InvoiceList.Any())
                {
                    ValidateOwnerAccount(dto.BillingAccountList.First());

                    if (Notification.HasNotification())
                        return null;
                }

                await uow.CompleteAsync();
            }

            if (result.InvoiceList.Any())
            {
                if (result.InvoiceList.Any(x => x.Type == BillingInvoiceTypeEnum.NFSE))
                    notificationDto = await _integrationAppService.GenerateInvoice(result.InvoiceList, BillingInvoiceTypeEnum.NFSE, dto.Credit)
                                                                  .ConfigureAwait(false);

                if (result.InvoiceList.Any(x => x.Type == BillingInvoiceTypeEnum.NFCE || x.Type == BillingInvoiceTypeEnum.SAT))
                    notificationDto = await _integrationAppService.GenerateInvoice(result.InvoiceList, result.InvoiceList.FirstOrDefault().Type, dto.Credit)
                                                                  .ConfigureAwait(false);
            }

            if (notificationDto != null)
            {
                if (notificationDto.DetailedMessage.Contains(_deniedMessage))
                {
                    _billingInvoicePropertyRepository.IncrementLastNumberAndUpdate(_billingInvoicePropertyRepository.Get(new DefaultGuidRequestDto(notificationDto.BillingInvoicePropertyId)));
                }

                var notify = JsonConvert.DeserializeObject<NotificationEvent>(JsonConvert.SerializeObject(notificationDto));
                _notificationHandler.Raise(notify);
            }

            return result;
        }

        public virtual async Task<BillingAccountClosureResultDto> CreatePartial(BillingAccountClosurePartial dto)
        {
            BillingAccountClosureResultDto result = null;

            ValidateDto<BillingAccountClosurePartial>(dto, nameof(dto));

            if (Notification.HasNotification())
                return null;

            using (var uow = _unitOfWorkManager.Begin())
            {
                //validar se dentro dos itens nao tem uma taxa selecionada
                if (_billingAccountItemReadRepository.AnyReversalOrTaxByBillingAccountItemIdList(dto.BillingAccountItemList.ToList()))
                {
                    NotifyParameterInvalid();
                    return null;
                }

                //não pode ter conta bloqueada 
                //nao pode ter conta já encerrada
                if (_billingAccountReadRepository.AnyClosedOrBlockedAccount(dto.BillingAccountList.ToList()))
                {
                    Notification.Raise(Notification.DefaultBuilder
                    .WithMessage(AppConsts.LocalizationSourceName, BillingAccountEnum.Error.BillingAccountWithBillingAccountItemsCanNotBeClosedOrBlocked)
                    .WithDetailedMessage(AppConsts.LocalizationSourceName, BillingAccountEnum.Error.BillingAccountWithBillingAccountItemsCanNotBeClosedOrBlocked)
                    .Build());
                }

                result = AccountClosureProccessWithTransfer(dto);

                await uow.CompleteAsync();
            }

            if (result.InvoiceList.Any())
            {
                ValidateOwnerAccount(dto.BillingAccountList.First());

                if (Notification.HasNotification())
                    return null;

                if (result.InvoiceList.Any(x => x.Type == BillingInvoiceTypeEnum.NFSE))
                    await _integrationAppService.GenerateInvoice(result.InvoiceList, BillingInvoiceTypeEnum.NFSE, dto.Credit).ConfigureAwait(false);

                if (result.InvoiceList.Any(x => x.Type == BillingInvoiceTypeEnum.NFCE))
                    await _integrationAppService.GenerateInvoice(result.InvoiceList, BillingInvoiceTypeEnum.NFCE, dto.Credit).ConfigureAwait(false);
            }

            return result;
        }


        private BillingAccountClosureResultDto AccountClosureProccessWithTransfer(BillingAccountClosurePartial dto)
        {
            var billingAccountBase = _billingAccountReadRepository.GetBillingAccountById(dto.BillingAccountList.First());

            var newBillingAccount = _billingAccountAppService.AddBillingAccount(new AddBillingAccountDto { CreatePrincipalAccount = false, BillingAccountName = dto.BillingAccountName }, billingAccountBase);

            _billingAccountItemAppService.CreateTransferAccountsToAccount(newBillingAccount.Id, dto.BillingAccountList.ToList(), dto.BillingAccountItemList.ToList());

            //async
            //lançar crédito para a nova conta
            dto.Credit.BillingAccountId = newBillingAccount.Id;
            _billingAccountItemAppService.CreateCredit(dto.Credit);

            if (Notification.HasNotification())
                return null;

            _billingAccountRepository.SaveChanges();

            return CloseBillingAccount(newBillingAccount.Id, dto.PropertyId);
        }

        private BillingAccountClosureResultDto AccountClosureProccess(BillingAccountClosureDto dto)
        {
            var billingAccountId = dto.BillingAccountList.First();
            var credit = dto.Credit;
            var propertyId = dto.PropertyId;

            //async
            //lançar crédito
            credit.BillingAccountId = billingAccountId;

            _billingAccountItemAppService.CreateCredit(credit);

            if (Notification.HasNotification())
                return null;

            _billingAccountRepository.SaveChanges();

            return CloseBillingAccount(billingAccountId, propertyId);
        }

        private void ValidateOwnerAccount(Guid billingAccountId)
        {
            var billingAccount = _billingAccountReadRepository.GetBillingAccountById(billingAccountId);
            if (billingAccount == null)
            {
                NotifyNullParameter();
                return;
            }

            if (billingAccount.GuestReservationItemId.HasValue)
                ValidateGuestRegistration(billingAccount.GuestReservationItemId.Value);
            else
                ValidateCompanyClient(billingAccount.CompanyClientId.Value);
        }

        private void ValidateGuestRegistration(long guestReservationItemId)
        {
            var guestRegistration = _guestRegistrationReadRepository.GetGuestRegistrationByGuestReservationItemId(
                   long.Parse(_applicationUser.PropertyId), guestReservationItemId, _applicationUser.PreferredLanguage);

            if (guestRegistration.ResponsibleGuestRegistrationId.HasValue)
                guestRegistration = _guestRegistrationReadRepository.GetById(guestRegistration.ResponsibleGuestRegistrationId.Value);

            // Email
            if (string.IsNullOrWhiteSpace(guestRegistration.Email))
            {
                Notification.Raise(Notification.DefaultBuilder
                    .WithMessage(AppConsts.LocalizationSourceName, BillingAccountEnum.Error.OwnerMustHaveEmail)
                    .WithDetailedMessage(AppConsts.LocalizationSourceName, BillingAccountEnum.Error.OwnerMustHaveEmail)
                    .Build());

                return;
            }

            // Documento
            if (string.IsNullOrWhiteSpace(guestRegistration.Document))
            {
                Notification.Raise(Notification.DefaultBuilder
                   .WithMessage(AppConsts.LocalizationSourceName, BillingAccountEnum.Error.OwnerMustHaveDocument)
                   .WithDetailedMessage(AppConsts.LocalizationSourceName, BillingAccountEnum.Error.OwnerMustHaveDocument)
                   .Build());

                return;
            }

            // Endereço
            if (guestRegistration.Location == null || string.IsNullOrWhiteSpace(guestRegistration.Location.StreetName))
            {
                Notification.Raise(Notification.DefaultBuilder
                 .WithMessage(AppConsts.LocalizationSourceName, BillingAccountEnum.Error.OwnerMustHaveLocation)
                 .WithDetailedMessage(AppConsts.LocalizationSourceName, BillingAccountEnum.Error.OwnerMustHaveLocation)
                 .Build());

                return;
            }

            // Nationality
            if (!guestRegistration.Nationality.HasValue)
            {
                Notification.Raise(Notification.DefaultBuilder
                 .WithMessage(AppConsts.LocalizationSourceName, BillingAccountEnum.Error.OwnerMustHaveNationality)
                 .WithDetailedMessage(AppConsts.LocalizationSourceName, BillingAccountEnum.Error.OwnerMustHaveNationality)
                 .Build());

                return;
            }
        }

        private void ValidateCompanyClient(Guid companyClientId)
        {
            var companyClient = _companyClientReadRepository.GetCompanyClientDtoById(companyClientId);
            var person = _personReadRepository.GetCompletePersonById(companyClient.PersonId);

            // Email
            if (!person.ContactInformationList.Any(c => c.ContactInformationTypeId == (int)ContactInformationTypeEnum.Email))
            {
                Notification.Raise(Notification.DefaultBuilder
                                      .WithMessage(AppConsts.LocalizationSourceName, BillingAccountEnum.Error.OwnerMustHaveEmail)
                                      .WithDetailedMessage(AppConsts.LocalizationSourceName, BillingAccountEnum.Error.OwnerMustHaveEmail)
                                      .Build());

                return;
            }

            // Documento
            if (!person.DocumentList.Any())
            {
                Notification.Raise(Notification.DefaultBuilder
                                      .WithMessage(AppConsts.LocalizationSourceName, BillingAccountEnum.Error.OwnerMustHaveDocument)
                                      .WithDetailedMessage(AppConsts.LocalizationSourceName, BillingAccountEnum.Error.OwnerMustHaveDocument)
                                      .Build());

                return;
            }

            // Endereço
            if (!person.LocationList.Any())
            {
                Notification.Raise(Notification.DefaultBuilder
                                      .WithMessage(AppConsts.LocalizationSourceName, BillingAccountEnum.Error.OwnerMustHaveLocation)
                                      .WithDetailedMessage(AppConsts.LocalizationSourceName, BillingAccountEnum.Error.OwnerMustHaveLocation)
                                      .Build());

                return;
            }
        }

        private BillingAccountClosureResultDto AccountClosureProccessWithTransfer(BillingAccountClosureDto dto)
        {
            ValidateAccountClosureProccessWithTransfer(dto);

            if (Notification.HasNotification())
                return null;

            BillingAccount billingAccountBase = GetBillingAccountBase(dto);

            //criar nova conta com base nessa
            var newBillingAccount = _billingAccountAppService.AddBillingAccount(
                new AddBillingAccountDto
                {
                    CreatePrincipalAccount = false,
                    BillingAccountName = string.IsNullOrEmpty(dto.BillingAccountName) ? _localizationManager.GetString(AppConsts.LocalizationSourceName, BillingAccountEnum.Default.BillingAccountDefaultNameConsolidated.ToString()) : dto.BillingAccountName
                },
                billingAccountBase);

            //transferir tudo pra conta destino
            _billingAccountItemAppService.CreateTransferAccountsToAccount(newBillingAccount.Id, dto.BillingAccountList.ToList());

            //async
            //lançar crédito para a nova conta
            if (dto.Credit != null)
            {
                dto.Credit.BillingAccountId = newBillingAccount.Id;
                _billingAccountItemAppService.CreateCredit(dto.Credit);
            }

            //async
            //encerrar as conta da lista
            //encerrar a nova conta caso o saldo seja zero (a soma de todos billing items deve ser igual ou menor a do crédito)
            //voltar cada conta e seu saldo
            //var newAccountBalance = Math.Round(accountBalance, 2, MidpointRounding.AwayFromZero) + dto.Credit.Amount;

            _billingAccountRepository.CloseAllBillingAccounts(dto.BillingAccountList.ToList());

            return CloseBillingAccount(newBillingAccount.Id, dto.PropertyId);
        }

        private void ValidateAccountClosureProccessWithTransfer(BillingAccountClosureDto dto)
        {
            //voltar notificação
            if (!dto.OwnerDestination.HasValue || !dto.IsCompany.HasValue)
                NotifyParameterInvalid();

            //nao pode ter uma conta em grupo
            if (_billingAccountReadRepository.AnyGroupAccount(dto.BillingAccountList.ToList()))
                NotifyParameterInvalid();
        }

        private BillingAccount GetBillingAccountBase(BillingAccountClosureDto dto)
        {
            var billingAccountBase = _billingAccountReadRepository.GetBillingAccountById(dto.BillingAccountList.First());

            if (billingAccountBase.BillingAccountTypeId != (int)BillingAccountTypeEnum.Sparse)
                billingAccountBase = _billingAccountReadRepository.GetBillingAccountByOwnerIdAndReservationId(dto.OwnerDestination.Value, dto.IsCompany.Value, dto.ReservationId.Value, dto.ReservatiomItemId);
            return billingAccountBase;
        }

        public List<BillingAccountClosureInvoiceDto> CloseBillingAccountServices(ref Dictionary<string, string> countrySubdService,
                                                                           List<BillingInvoicePropertySupportedType> supportedTypes,
                                                                           List<BillingAccountItem> accountItems, Guid billingAccountId, int propertyId)
        {
            foreach (var supportedType in supportedTypes.Where(b => b.CountrySubdvisionServiceId != null))
            {
                if (supportedType.BillingInvoiceProperty.IsIntegrated)
                {
                    var billingAccountItemsBySupportedTypes = accountItems.Where(e => e.BillingItem.BillingItemTypeId == supportedType.BillingItemTypeId && e.BillingItemId == supportedType.BillingItemId).ToList();

                    if (billingAccountItemsBySupportedTypes.Any())
                    {
                        if (!countrySubdService.Keys.Contains(supportedType.CountrySubdvisionServiceId.Value.ToString()))
                        {
                            var billingInvoice = CreateBillingInvoiceAndUpdateLastNumber(billingAccountId, propertyId, supportedType.BillingInvoiceProperty, BillingInvoiceTypeEnum.NFSE);

                            countrySubdService.Add(supportedType.CountrySubdvisionServiceId.Value.ToString(), billingInvoice.Id.ToString());
                        }

                        //criar invoice e atualizar 
                        foreach (var billingAccountItem in billingAccountItemsBySupportedTypes)
                        {
                            var billingInvoiceId =
                                countrySubdService[countrySubdService.Keys.Where(b => b == supportedType.CountrySubdvisionServiceId.Value.ToString()).FirstOrDefault()];

                            billingAccountItem.SetBillingInvoice(Guid.Parse(billingInvoiceId));

                            var billingAccountItemTaxList = accountItems.Where(e => e.BillingAccountItemParentId == billingAccountItem.Id &&
                                                                                    e.BillingItem.BillingItemTypeId == (int)BillingItemTypeEnum.Tax).ToList();

                            foreach (var billingAccountItemTax in billingAccountItemTaxList)
                                billingAccountItemTax.SetBillingInvoice(Guid.Parse(billingInvoiceId));
                        }
                    }
                }
            }

            var billingAccountList = new List<BillingAccountClosureInvoiceDto>();

            foreach (var service in countrySubdService)
            {
                billingAccountList.Add(new BillingAccountClosureInvoiceDto()
                {
                    Type = BillingInvoiceTypeEnum.NFSE,
                    InvoiceId = service.Value
                });
            }

            return billingAccountList;
        }

        public List<BillingAccountClosureInvoiceDto> CloseBillingAccountProducts(ref Dictionary<string, string> countrySubdService,
                                                                                 List<BillingInvoicePropertySupportedType> supportedTypes,
                                                                                 List<BillingAccountItem> accountItems, Guid billingAccountId, int propertyId)
        {
            var billingAccountList = new List<BillingAccountClosureInvoiceDto>();

            foreach (var supportedType in supportedTypes.Where(b => b.BillingItemTypeId == (int)BillingItemTypeEnum.PointOfSale))
            {
                if (supportedType.BillingInvoiceProperty.IsIntegrated)
                {
                    var billingAccountItemsBySupportedTypes = accountItems.Where(e => e.BillingItem.BillingItemTypeId == supportedType.BillingItemTypeId && e.BillingItemId == supportedType.BillingItemId).ToList();

                    if (billingAccountItemsBySupportedTypes.Any())
                    {
                        var billingInvoice = CreateBillingInvoiceAndUpdateLastNumber(billingAccountId, propertyId, supportedType.BillingInvoiceProperty, (BillingInvoiceTypeEnum)_billingInvoiceModelReadRepository.GetInvoiceTypeByModel(supportedType.BillingInvoiceProperty.BillingInvoiceModelId));

                        //criar invoice e atualizar 
                        foreach (var billingAccountItem in billingAccountItemsBySupportedTypes)
                        {
                            billingAccountItem.SetBillingInvoice(billingInvoice.Id);

                            var billingAccountItemTaxList = accountItems.Where(e => e.BillingAccountItemParentId == billingAccountItem.Id &&
                                                                                    e.BillingItem.BillingItemTypeId == (int)BillingItemTypeEnum.Tax).ToList();

                            foreach (var billingAccountItemTax in billingAccountItemTaxList)
                                billingAccountItemTax.SetBillingInvoice(billingInvoice.Id);
                        }

                        billingAccountList.Add(new BillingAccountClosureInvoiceDto()
                        {
                            Type = (BillingInvoiceTypeEnum)_billingInvoiceModelReadRepository.GetInvoiceTypeByModel(supportedType.BillingInvoiceProperty.BillingInvoiceModelId),
                            InvoiceId = billingInvoice.Id.ToString()
                        });
                    }
                }
            }

            return billingAccountList;
        }

        private BillingAccountClosureResultDto CloseBillingAccount(Guid billingAccountId, int propertyId)
        {
            List<BillingAccountClosureInvoiceDto> invoicesList = new List<BillingAccountClosureInvoiceDto>();

            //obter tipos suportados
            var supportedTypes = _billingInvoicePropertySupportedTypeReadRepository.GetAllByPropertyId(propertyId);

            var billingAccountItemForSetInvoiceIdList = _billingAccountItemAppService.GetAllForSetInvoiceIdByBillingAccountId(billingAccountId);

            Dictionary<string, string> countrySubdService = new Dictionary<string, string>();

            ValidateBillingAccountItemWithoutBillingInvoicePropertySupportedType(supportedTypes, billingAccountItemForSetInvoiceIdList);

            if (Notification.HasNotification())
                return null;

            if (billingAccountItemForSetInvoiceIdList.Any() && _billingAccountRepository.GetTotalAmountByBillingAccountId(billingAccountId) == 0)
            {
                // Recupera os invices para serviços e produtos
                invoicesList.AddRange(CloseBillingAccountServices(ref countrySubdService, supportedTypes, billingAccountItemForSetInvoiceIdList, billingAccountId, propertyId));
                invoicesList.AddRange(CloseBillingAccountProducts(ref countrySubdService, supportedTypes, billingAccountItemForSetInvoiceIdList, billingAccountId, propertyId));

                _billingAccountRepository.SaveChanges();
            }

            _billingAccountRepository.CloseBillingAccount(billingAccountId);
            _billingAccountRepository.SaveChanges();

            var systemDate = _propertyParameterReadRepository.GetSystemDate(int.Parse(_applicationUser.PropertyId)).Value;

            ChangeReservationItemStatus(billingAccountId, systemDate);
            ChangeGuestReservationStatus(billingAccountId, systemDate);

            return new
             BillingAccountClosureResultDto
            {
                Id = billingAccountId,
                AccountBalance = _billingAccountRepository.GetTotalAmountByBillingAccountId(billingAccountId),
                InvoiceList = invoicesList
            };
        }

        private BillingInvoiceDto CreateBillingInvoiceAndUpdateLastNumber(Guid billingAccountId, int propertyId,
                                                                          BillingInvoiceProperty billingInvoiceProperty, BillingInvoiceTypeEnum invoiceType,
                                                                          int attempt = 1)
        {
            if (attempt == 4)
                throw new Exception();

            if (attempt > 1)
                billingInvoiceProperty = _billingInvoicePropertyRepository.Get(new DefaultGuidRequestDto(billingInvoiceProperty.Id));

            try
            {
                _billingInvoicePropertyRepository.IncrementLastNumberAndUpdate(billingInvoiceProperty);

                var billingInvoiceDto = new BillingInvoiceDto
                {
                    Id = Guid.NewGuid(),
                    BillingInvoiceNumber = billingInvoiceProperty.LastNumber,
                    BillingInvoiceSeries = billingInvoiceProperty.BillingInvoicePropertySeries,
                    PropertyId = propertyId,
                    BillingAccountId = billingAccountId,
                    BillingInvoicePropertyId = billingInvoiceProperty.Id,
                    BillingInvoiceStatusId = (int)BillingInvoiceStatusEnum.NotSent,
                    EmissionDate = DateTime.UtcNow.ToZonedDateTimeLoggedUser(),
                    BillingInvoiceTypeId = (int)invoiceType
                };

                _billingInvoiceAppService.CreateBillingInvoice(billingInvoiceDto);

                return billingInvoiceDto;
            }
            catch (DbUpdateException ex)
            {
                if (ex.InnerException.Message.Contains("UK_BillingInvoice"))
                {
                    attempt++;
                    return CreateBillingInvoiceAndUpdateLastNumber(billingAccountId, propertyId, billingInvoiceProperty, invoiceType, attempt);
                }
                else
                    throw;
            }
        }

        private void SetInvoiceInBillingItemWithoutCountrySubdvisionServiceId(IEnumerable<BillingAccountItem> billingAccountItemForSetInvoiceIdList, List<BillingItem> billingItemList, Guid billingInvoiceId)
        {
            var billingAccountItemList = billingAccountItemForSetInvoiceIdList.Where(b => !b.BillingInvoiceId.HasValue && billingItemList.Contains(b.BillingItem));

            foreach (var billingAccountItemWithoutInvoice in billingAccountItemList)
            {
                billingAccountItemWithoutInvoice.SetBillingInvoice(billingInvoiceId);

                var billingAccountItemTaxList = billingAccountItemForSetInvoiceIdList.Where(e => e.BillingAccountItemParentId == billingAccountItemWithoutInvoice.Id && e.BillingItem.BillingItemTypeId == (int)BillingItemTypeEnum.Tax).ToList();

                foreach (var billingAccountItemTax in billingAccountItemTaxList)
                    billingAccountItemTax.SetBillingInvoice(billingInvoiceId);
            }
        }

        private void ValidateBillingAccountItemWithoutBillingInvoicePropertySupportedType(List<BillingInvoicePropertySupportedType> supportedTypes, List<BillingAccountItem> billingAccountItems)
        {
            var billingItemListCount = billingAccountItems.Select(e => e.BillingItem).Where(e => e.BillingItemTypeId == (int)BillingItemTypeEnum.Service).Select(e => e.Id).Distinct().ToList();

            if (billingItemListCount.Count > 0)
            {
                var billingItemIdSupportedIdList = supportedTypes.Select(e => e.BillingItemId).Distinct().ToList();

                if (billingItemListCount.Count(e => !billingItemIdSupportedIdList.Contains(e)) > 0)
                {
                    Notification.Raise(Notification.DefaultBuilder
                        .WithMessage(AppConsts.LocalizationSourceName, BillingItemEnum.Error.BillingAccountItemWithoutBillingInvoicePropertySupportedType)
                        .WithDetailedMessage(AppConsts.LocalizationSourceName, BillingItemEnum.Error.BillingAccountItemWithoutBillingInvoicePropertySupportedType)
                        .Build());
                }
            }
        }

        private void ChangeReservationItemStatus(Guid billingAccountId, DateTime systemDate)
        {
            var allAccountsAreClosed = _billingAccountReadRepository.CheckAllBillingAccountsAreClosedByReservationItemId(billingAccountId);
            if (allAccountsAreClosed)
            {
                var reservationItem = _reservationItemReadRepository.GetByBillingAccountdId(billingAccountId);
                if (reservationItem != null && reservationItem.ReservationItemStatusId == (int)ReservationStatus.Pending)
                    _reservationRepository.ChangeStatusReservationItem(reservationItem.Id, (int)ReservationStatus.Checkout, systemDate);
            }
        }

        private void ChangeGuestReservationStatus(Guid billingAccountId, DateTime systemDate)
        {
            var guestReservationItem = _guestReservationItemReadRepository.GetGuestReservationItemByBilligAccountId(billingAccountId);
            if (guestReservationItem != null && guestReservationItem.GuestStatusId == (int)ReservationStatus.Pending)
                _guestReservationItemRepository.ChangeGuestReservationItemStatus(guestReservationItem.Id, ReservationStatus.Checkout, systemDate);
        }
    }
}
