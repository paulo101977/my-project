﻿using System;
using System.Threading.Tasks;
using Thex.Application.Adapters;
using Thex.Application.Interfaces;
using Thex.Common;
using Thex.Common.Enumerations;
using Thex.Domain.Entities;
using Thex.Domain.Interfaces.Services;
using Thex.Dto;
using Thex.Infra.ReadInterfaces;
using Thex.Kernel;
using Tnf.Dto;
using Tnf.Notifications;

namespace Thex.Application.Services
{
    public class ChannelAppService : ScaffoldChannelAppService, IChannelAppService
    {
        private readonly IChannelReadRepository _channelReadRepository;
        private readonly IChannelDomainService _channelDomainService;
        private readonly IApplicationUser _applicationUser;


        public ChannelAppService(
            IChannelAdapter channelAdapter,
            IChannelDomainService channelDomainService,
            IChannelReadRepository channelReadRepository,
            IApplicationUser applicationUser,
            INotificationHandler notificationHandler)
            : base(channelAdapter, channelDomainService, notificationHandler)
        {
            _applicationUser = applicationUser;
            _channelReadRepository = channelReadRepository;
            _channelDomainService = channelDomainService;
        }

        private void ValidationsChannel(ChannelDto channelDto)
        {

            ValidateNull(channelDto);

            if (Notification.HasNotification()) return;

            ValidatePermition(channelDto.Id);

            if (Notification.HasNotification()) return;


        }

        private void ValidatePermition(Guid id)
        {
            Channel channel = _channelReadRepository.GetById(id);

            if (_applicationUser.TenantId != channel.TenantId)
            {
                Notification.Raise(Notification.DefaultBuilder
                    .WithMessage(AppConsts.LocalizationSourceName, Channel.EntityError.ChannelMustHavePermition)
                    .WithDetailedMessage(AppConsts.LocalizationSourceName, Channel.EntityError.ChannelMustHavePermition)
                    .Build());
            }
        }

        private void ValidateNull(ChannelDto channel)
        {
            if (channel == null)
                Notification.Raise(Notification
                    .DefaultBuilder
                    .WithMessage(AppConsts.LocalizationSourceName, CommonsEnum.Error.NullOrEmptyObject)
                    .Build());
        }

        private void ValidateBusinessSource(int businessSourceId)
        {
            if (businessSourceId == default(int))
            {
                Notification.Raise(Notification
                    .DefaultBuilder
                    .WithMessage(AppConsts.LocalizationSourceName, Channel.EntityError.ChannelMustHaveBusinessSource)
                    .WithDetailedMessage(AppConsts.LocalizationSourceName, Channel.EntityError.ChannelMustHaveBusinessSource)
                    .Build());
            }
        }

        private void ValidateMarketSegment(int marketSegmentId)
        {
            if (marketSegmentId == default(int))
            {
                Notification.Raise(Notification
                    .DefaultBuilder
                    .WithMessage(AppConsts.LocalizationSourceName, Channel.EntityError.ChannelMustHaveMarketSegment)
                    .WithDetailedMessage(AppConsts.LocalizationSourceName, Channel.EntityError.ChannelMustHaveMarketSegment)
                    .Build());
            }
        }

        public ChannelDto CreateChannel(ChannelDto channelDto)
        {
            ValidateDto(channelDto, nameof(channelDto));

            if (Notification.HasNotification())
                return ChannelDto.NullInstance;

            ValidateBusinessSource(channelDto.BusinessSourceId);

            if (Notification.HasNotification()) return null;

            ValidateMarketSegment(channelDto.MarketSegmentId);

            if (Notification.HasNotification()) return null;

            Channel.Builder builder = new Channel.Builder(Notification);

            builder
                .WithId(Guid.NewGuid())
                .WithChannelCodeId(channelDto.ChannelCodeId)
                .WithDescription(channelDto.Description)
                .WithBusinessSourceId(channelDto.BusinessSourceId)
                .WithMarketSegmentId(channelDto.MarketSegmentId)
                .WithDistributionAmount(channelDto.DistributionAmount)
                .WithIsActive(channelDto.IsActive);

            if (Notification.HasNotification()) return null;

            channelDto.Id = _channelDomainService.InsertAndSaveChanges(builder).Id;

            return channelDto;
        }

        public void DeleteChannel(Guid id)
        {
            var channel = _channelReadRepository.GetDtoById(id);
            ValidationsChannel(channel);
            Channel.Builder builder = new Channel.Builder(Notification);
            builder
               .WithId(channel.Id)
               .WithChannelCodeId(channel.ChannelCodeId)
               .WithDescription(channel.Description)
               .WithBusinessSourceId(channel.BusinessSourceId)
               .WithDistributionAmount(channel.DistributionAmount)
               .WithIsActive(channel.IsActive);

            _channelDomainService.DeleteChannel(builder);
        }

        public async Task<IListDto<ChannelDto>> GetAllChannelsDtoByTenantIdAsync(GetAllChannelDto requestDto)
        {
            var result = await _channelReadRepository.GetAllChannelsDtoByTenantIdAsync(requestDto, _applicationUser.TenantId);
            return result;
        }

        public ChannelDto GetDtoById(Guid id)
        {
            var channel = _channelReadRepository.GetDtoById(id);
            ValidationsChannel(channel);
            return channel;
        }

        public void ToggleAndSaveIsActive(Guid id)
        {
            var channel = _channelReadRepository.GetDtoById(id);
            ValidationsChannel(channel);
            _channelDomainService.ToggleAndSaveIsActive(id);
        }

        public ChannelDto UpdateChannel(Guid id, ChannelDto channelDto)
        {
            ValidateDto(channelDto, nameof(channelDto));
            if (Notification.HasNotification())
                return ChannelDto.NullInstance;

            ValidationsChannel(channelDto);

            if (Notification.HasNotification()) return null;

            ValidateBusinessSource(channelDto.BusinessSourceId);

            if (Notification.HasNotification()) return null;

            ValidateMarketSegment(channelDto.MarketSegmentId);

            if (Notification.HasNotification()) return null;

            Channel.Builder builder = new Channel.Builder(Notification);
            channelDto.Id = id;

            builder
                .WithId(channelDto.Id)
                .WithChannelCodeId(channelDto.ChannelCodeId)
                .WithDescription(channelDto.Description)
                .WithMarketSegmentId(channelDto.MarketSegmentId)
                .WithBusinessSourceId(channelDto.BusinessSourceId)
                .WithDistributionAmount(channelDto.DistributionAmount)
                .WithIsActive(channelDto.IsActive);

            _channelDomainService.Update(builder);
            return channelDto;
        }

        public IListDto<ChannelDto> SearchChannel(string search)
        {
            var channelList = _channelReadRepository.SearchChannels(search);
            return channelList;
        }
    }
}
