﻿using System;
using Thex.Application.Adapters;
using Thex.Domain.Entities;
using Thex.Application.Interfaces;
using Tnf.Domain.Services;
using Tnf.Notifications;

namespace Thex.Application.Services
{
    public class BillingItemPaymentConditionAppService : ScaffoldBillingItemPaymentConditionAppService, IBillingItemPaymentConditionAppService
    {
        public BillingItemPaymentConditionAppService(
            IBillingItemPaymentConditionAdapter billingItemPaymentConditionAdapter,
            IDomainService<BillingItemPaymentCondition> billingItemPaymentConditionDomainService,
            INotificationHandler notificationHandler)
            : base(billingItemPaymentConditionAdapter, billingItemPaymentConditionDomainService, notificationHandler)
        {
        }
    }
}
