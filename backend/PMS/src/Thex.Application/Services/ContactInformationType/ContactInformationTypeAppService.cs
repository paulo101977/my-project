﻿using System;
using Thex.Application.Adapters;
using Thex.Domain.Entities;
using Thex.Application.Interfaces;
using Tnf.Domain.Services;
using Tnf.Notifications;

namespace Thex.Application.Services
{
    public class ContactInformationTypeAppService : ScaffoldContactInformationTypeAppService, IContactInformationTypeAppService
    {
        public ContactInformationTypeAppService(
            IContactInformationTypeAdapter contactInformationTypeAdapter,
            IDomainService<ContactInformationType> contactInformationTypeDomainService,
            INotificationHandler notificationHandler)
            : base(contactInformationTypeAdapter, contactInformationTypeDomainService, notificationHandler)
        {
        }
    }
}
