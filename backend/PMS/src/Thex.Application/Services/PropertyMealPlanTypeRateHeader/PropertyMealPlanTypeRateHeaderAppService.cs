﻿using System;
using Thex.Application.Adapters;
using Thex.Domain.Entities;
using Thex.Application.Interfaces;
using Tnf.Domain.Services;
using Thex.Dto;
using System.Threading.Tasks;
using Thex.Infra.ReadInterfaces;
using Tnf.Dto;
using Tnf.Notifications;
using Thex.Kernel;
using Thex.Domain.Interfaces.Repositories;

namespace Thex.Application.Services
{
    public class PropertyMealPlanTypeRateHeaderAppService : ScaffoldPropertyMealPlanTypeRateHeaderAppService, IPropertyMealPlanTypeRateHeaderAppService
    {
        public PropertyMealPlanTypeRateHeaderAppService(
            IPropertyMealPlanTypeRateHeaderAdapter propertyMealPlanTypeRateHeaderAdapter,
            IDomainService<PropertyMealPlanTypeRateHeader> propertyMealPlanTypeRateHeaderDomainService,
            INotificationHandler notificationHandler)
                : base(propertyMealPlanTypeRateHeaderAdapter, propertyMealPlanTypeRateHeaderDomainService, notificationHandler)
        {
        }
    }
}
