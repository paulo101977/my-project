﻿using System;
using System.Collections.Generic;
using System.Linq;
using Thex.Application.Adapters;
using Thex.Application.Interfaces;
using Thex.Common;
using Thex.Common.Enumerations;
using Thex.Domain.Entities;
using Thex.Domain.Interfaces.Services;
using Thex.Dto;
using Thex.Infra.ReadInterfaces;
using Thex.Kernel;
using Tnf.Dto;
using Tnf.Notifications;
using static Thex.Domain.Entities.Level;
using static Thex.Dto.LevelDto;

namespace Thex.Application.Services
{
    public class LevelAppService : ScaffoldLevelAppService, ILevelAppService
    {
        private readonly ILevelReadRepository _levelReadRepository;
        private readonly ILevelDomainService _levelDomainService;
        private readonly IApplicationUser _applicationUser;


        public LevelAppService(
            ILevelAdapter levelAdapter,
            ILevelDomainService levelDomainService,
            ILevelReadRepository levelReadRepository,
            IApplicationUser applicationUser,
            INotificationHandler notificationHandler)
            : base(levelAdapter, levelDomainService, notificationHandler)
        {
            _applicationUser = applicationUser;
            _levelReadRepository = levelReadRepository;
            _levelDomainService = levelDomainService;
        }

        public LevelDto CreateLevel(LevelDto levelDto)
        {

            var documentListToUpdate = new List<Level>();

            ValidateLevel(levelDto, Method.Insert);

            if (Notification.HasNotification()) return null;

            var listLevelDto = GeneratorOrder(levelDto, Method.Insert);

            var document = LevelAdapter.Map(levelDto).Build();

            if (Notification.HasNotification()) return null;

            _levelDomainService.Insert(document);

            if (listLevelDto.Any())
            {
                var listLevel = _levelReadRepository.GetAll(int.Parse(_applicationUser.PropertyId));

                foreach (var documentDto in listLevelDto)
                {
                    var levelItem = listLevel.FirstOrDefault(x => x.Id == documentDto.Id);

                    documentListToUpdate.Add(LevelAdapter.Map(levelItem, documentDto).Build());
                }

                _levelDomainService.UpdateRange(documentListToUpdate);

            }

            return levelDto;
        }

        public LevelDto UpdateLevel(LevelDto levelDto)
        {
            var documentListToUpdate = new List<Level>();

            ValidateLevel(levelDto, Method.Update);

            if (Notification.HasNotification()) return null;

            var listLevelDto = GeneratorOrder(levelDto, Method.Update);

            var listLevel = _levelReadRepository.GetAll(int.Parse(_applicationUser.PropertyId));

            foreach (var documentDto in listLevelDto)
            {
                var levelItem = listLevel.FirstOrDefault(x => x.Id == documentDto.Id);

                documentListToUpdate.Add(LevelAdapter.Map(levelItem, documentDto).Build());
            }

            if (Notification.HasNotification()) return null;

            _levelDomainService.UpdateRange(documentListToUpdate);

            return levelDto;
        }

        public void DeleteLevel(Guid id)
        {
            var level = _levelReadRepository.GetDtoById(int.Parse(_applicationUser.PropertyId), id);

            ValidateNull(level);

            if (Notification.HasNotification()) return;

            ValidatePermition(level.Id.Value);

            if (Notification.HasNotification()) return;

            if(_levelReadRepository.LevelAlreadyUsed(id))
            { 
                Notification.Raise(Notification.DefaultBuilder
                  .WithMessage(AppConsts.LocalizationSourceName, Level.EntityError.LevelCanNotBeRemovedBecauseItIsBeingUsed)
                  .WithDetailedMessage(AppConsts.LocalizationSourceName, Level.EntityError.LevelCanNotBeRemovedBecauseItIsBeingUsed)
                  .Build());

                return;
            }

            Level.Builder builder = new Level.Builder(Notification);
            builder
              .WithId(level.Id.Value)
              .WithLevelCode(level.LevelCode)
              .WithDescription(level.Description)
              .WithOrder(level.Order)
              .WithPropertyId(int.Parse(_applicationUser.PropertyId))
              .WithIsActive(level.IsActive);

            _levelDomainService.DeleteLevel(builder);

        }

        public void ToggleAndSaveIsActive(Guid id)
        {
            var level = _levelReadRepository.GetDtoById(int.Parse(_applicationUser.PropertyId), id);

            ValidateNull(level);

            if (Notification.HasNotification()) return;

            ValidatePermition(level.Id.Value);

            if (Notification.HasNotification()) return;

            _levelDomainService.ToggleAndSaveIsActive(id);
        }

        public IListDto<LevelDto> GetAllDto(GetAllLevelDto requestDto)
        {
            return _levelReadRepository.GetAllDto(int.Parse(_applicationUser.PropertyId), requestDto);
        }

        public LevelDto GetDtoById(Guid id)
        {
            return _levelReadRepository.GetDtoById(int.Parse(_applicationUser.PropertyId), id);
        }

        public List<LevelDto> GeneratorOrder(LevelDto levelDto, Method method)
        {
            GetAllLevelDto getAllLevelDto = new GetAllLevelDto { All = true };

            var listLevel = GetAllDto(getAllLevelDto).Items.ToList();

            var list = new List<LevelDto>();

            if (Method.Update.Equals(method))
                GeneratorOrderUpdate(listLevel, levelDto, out list);
            else
                GeneratorOrderInclusion(listLevel, levelDto, out list);


            return list;
        }

        private void ValidatePermition(Guid id)
        {
            var level = _levelReadRepository.GetById(int.Parse(_applicationUser.PropertyId), id);

            if (level != null)
            {
                if (_applicationUser.TenantId != level.TenantId)
                {
                    Notification.Raise(Notification.DefaultBuilder
                        .WithMessage(AppConsts.LocalizationSourceName, Level.EntityError.LevelMustHavePermition)
                        .WithDetailedMessage(AppConsts.LocalizationSourceName, Level.EntityError.LevelMustHavePermition)
                        .Build());
                }
            }
        }

        private void ValidateLevel(LevelDto levelDto, Method method)
        {
            LevelDto levelReturnGetId = null;
            LevelDto returnGetLevelCode = null;

            ValidateDto(levelDto, nameof(levelDto));

            if (Notification.HasNotification()) return;

            ValidateNull(levelDto);

            if (Notification.HasNotification()) return;

            if (levelDto.PropertyId == 0)
                levelDto.PropertyId = int.Parse(_applicationUser.PropertyId);

            if (levelDto.Id.HasValue)
            {
                ValidatePermition(levelDto.Id.Value);
                levelReturnGetId = _levelReadRepository.GetDtoById(int.Parse(_applicationUser.PropertyId), levelDto.Id.Value);
            }

            if (levelDto.LevelCode != default(int))
                returnGetLevelCode = _levelReadRepository.GetDtoByLevelCode(int.Parse(_applicationUser.PropertyId), levelDto.LevelCode);


            if (Method.Insert.Equals(method))
            {
                if (levelReturnGetId != null)
                {
                    Notification.Raise(Notification.DefaultBuilder
                        .WithMessage(AppConsts.LocalizationSourceName, Level.EntityError.IdDouble)
                        .WithDetailedMessage(AppConsts.LocalizationSourceName, Level.EntityError.IdDouble)
                        .Build());
                }

                if (returnGetLevelCode != null)
                {
                    Notification.Raise(Notification.DefaultBuilder
                        .WithMessage(AppConsts.LocalizationSourceName, Level.EntityError.LevelCodeDuplicate)
                        .WithDetailedMessage(AppConsts.LocalizationSourceName, Level.EntityError.LevelCodeDuplicate)
                        .Build());
                }
            }
            else if (Method.Update.Equals(method))
            {
                if (!levelDto.Id.HasValue)
                {
                    Notification.Raise(Notification.DefaultBuilder
                     .WithMessage(AppConsts.LocalizationSourceName, Level.EntityError.IdNotInformed)
                     .WithDetailedMessage(AppConsts.LocalizationSourceName, Level.EntityError.IdNotInformed)
                     .Build());

                }

                if (levelReturnGetId == null)
                {
                    Notification.Raise(Notification.DefaultBuilder
                     .WithMessage(AppConsts.LocalizationSourceName, Level.EntityError.NoExistentLevelForUpdating)
                     .WithDetailedMessage(AppConsts.LocalizationSourceName, Level.EntityError.NoExistentLevelForUpdating)
                     .Build());
                }
                else if (levelReturnGetId.Id != levelDto.Id.Value)
                {
                    Notification.Raise(Notification.DefaultBuilder
                        .WithMessage(AppConsts.LocalizationSourceName, Level.EntityError.IdNoExistentForLevelCode)
                        .WithDetailedMessage(AppConsts.LocalizationSourceName, Level.EntityError.IdNoExistentForLevelCode)
                        .Build());
                }

                returnGetLevelCode = _levelReadRepository.GetDtoByLevelCode(int.Parse(_applicationUser.PropertyId), levelDto.LevelCode);

                if (returnGetLevelCode != null)
                {
                    if (returnGetLevelCode.Id.Value != levelDto.Id.Value)
                    {
                        Notification.Raise(Notification.DefaultBuilder
                                .WithMessage(AppConsts.LocalizationSourceName, Level.EntityError.LevelCodeDuplicate)
                                .WithDetailedMessage(AppConsts.LocalizationSourceName, Level.EntityError.LevelCodeDuplicate)
                                .Build());
                    }
                }
            }
        }

        private void ValidateNull(LevelDto level)
        {
            if (level == null)
                Notification.Raise(Notification
                    .DefaultBuilder
                    .WithMessage(AppConsts.LocalizationSourceName, CommonsEnum.Error.NullOrEmptyObject)
                    .Build());
        }

        private List<LevelDto> GeneratorOrderInclusion(List<LevelDto> listLevel, LevelDto levelDto, out List<LevelDto> listToReturn)
        {
            listToReturn = new List<LevelDto>();
            var dtoItemToListReturn = false;

            if (listLevel.Any(x => x.Order == levelDto.Order))
            {
                foreach (var item in listLevel.Where(x => x.Order >= levelDto.Order).OrderBy(c => c.Order))
                {
                    if (!dtoItemToListReturn)
                    {
                        if (item.Order == levelDto.Order)
                        {
                            item.Order = item.Order + 1;
                            listToReturn.Add(item);

                            dtoItemToListReturn = true;
                        }
                    }
                    else
                    {
                        var dtoItemExist = listToReturn.Where(x => x.Id.Value == item.Id.Value);

                        if (!dtoItemExist.Any())
                        {
                            if (listToReturn.Any(x => x.Order == item.Order))
                            {
                                item.Order = item.Order + 1;
                                listToReturn.Add(item);
                            }
                            else
                                break;

                        }
                    }
                }
            }

            return listToReturn;
        }

        private List<LevelDto> GeneratorOrderUpdate(List<LevelDto> listLevel, LevelDto levelDto, out List<LevelDto> listToReturn)
        {
            listToReturn = new List<LevelDto>();
            var dtoItemToListReturn = false;

            var dtoItem = listLevel.Where(x => x.Id == levelDto.Id && x.Order == levelDto.Order);

            if (dtoItem.Any())
            {
                listToReturn.Add(levelDto);
                return listToReturn;
            }

            foreach (var item in listLevel.OrderBy(c => c.Order))
            {
                if (!dtoItemToListReturn)
                {
                    if (item.Id.Value == levelDto.Id.Value)
                    {
                        listToReturn.Add(levelDto);
                        dtoItemToListReturn = true;
                        continue;
                    }

                    if (item.Order == levelDto.Order)
                    {
                        item.Order = item.Order + 1;
                        listToReturn.Add(levelDto);
                        listToReturn.Add(item);
                        dtoItemToListReturn = true;
                    }
                }
                else
                {
                    var dtoItemExist = listToReturn.Where(x => x.Id.Value == item.Id.Value);

                    if (!dtoItemExist.Any())
                    {
                        if (listToReturn.Any(x => x.Order == item.Order))
                        {
                            item.Order = item.Order + 1;
                            listToReturn.Add(item);
                        }
                        else
                            break;

                    }
                }
            }

            return listToReturn;
        }

    }
}
