﻿using System;
using Thex.Application.Adapters;
using Thex.Domain.Entities;
using Thex.Application.Interfaces;
using Tnf.Domain.Services;
using Tnf.Notifications;

namespace Thex.Application.Services
{
    public class PolicyTypeAppService : ScaffoldPolicyTypeAppService, IPolicyTypeAppService
    {
        public PolicyTypeAppService(
            IPolicyTypeAdapter policyTypeAdapter,
            IDomainService<PolicyType> policyTypeDomainService,
            INotificationHandler notificationHandler)
            : base(policyTypeAdapter, policyTypeDomainService, notificationHandler)
        {
        }
    }
}
