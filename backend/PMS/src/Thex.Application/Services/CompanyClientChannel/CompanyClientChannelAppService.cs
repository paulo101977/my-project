﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Thex.Application.Adapters;
using Thex.Application.Interfaces;
using Thex.Domain.Entities;
using Thex.Infra.ReadInterfaces;
using Tnf.Domain.Services;
using Tnf.Notifications;

namespace Thex.Application.Services
{
    public class CompanyClientChannelAppService : ScaffoldCompanyClientChannelAppService, ICompanyClientChannelAppService
    {
        public CompanyClientChannelAppService(
           ICompanyClientChannelAdapter companyClientChannelAdapter,
           IDomainService<CompanyClientChannel> companyClientChannelDomainService,
           ICompanyClientChannelReadRepository companyClientChannelReadRepository,
           INotificationHandler notificationHandler)
           : base(companyClientChannelAdapter, companyClientChannelDomainService, notificationHandler)
        {
           
        }
    }
}
