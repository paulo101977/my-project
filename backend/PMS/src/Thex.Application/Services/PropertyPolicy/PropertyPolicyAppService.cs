﻿using System;
using Thex.Application.Adapters;
using Thex.Domain.Entities;
using Thex.Application.Interfaces;
using Tnf.Domain.Services;
using Thex.Dto;
using Thex.Infra.ReadInterfaces;
using Thex.Domain.Interfaces.Repositories;
using Tnf.Dto;
using Tnf.Notifications;

namespace Thex.Application.Services
{
    public class PropertyPolicyAppService : ScaffoldPropertyPolicyAppService, IPropertyPolicyAppService
    {

        private readonly IPropertyPolicyReadRepository _propertyPolicyReadRepository;
        private readonly IPropertyPolicyRepository _propertyPolicyRepository;
        private readonly IPolicyTypesReadRepository _propertyTypeReadRepository;

        public PropertyPolicyAppService(
            IPropertyPolicyAdapter propertyPolicyAdapter,
            IPropertyPolicyReadRepository propertyPolicyReadRepository,
            IPropertyPolicyRepository propertyPolicyRepository,
            IPolicyTypesReadRepository propertyTypeReadRepository,
            IDomainService<PropertyPolicy> propertyPolicyDomainService,
            INotificationHandler notificationHandler)
            : base(propertyPolicyAdapter, propertyPolicyDomainService, notificationHandler)
        {
            _propertyPolicyReadRepository = propertyPolicyReadRepository;
            _propertyPolicyRepository = propertyPolicyRepository;
            _propertyTypeReadRepository = propertyTypeReadRepository;
        }

        public IListDto<PropertyPolicyDto> GetAllPropertyPolicy(int propertyId)
        {
            return _propertyPolicyReadRepository.GetAllPropertyPolicyByPropertyId(propertyId);
        }

        public PropertyPolicyDto GetPropertyPolicy(Guid guid)
        {
          return  _propertyPolicyReadRepository.GetPropertyPolicy(guid);
        }

        public void PropertyPolicyCreate(PropertyPolicyDto propertyPolicy)
        {
            var builder = PropertyPolicyAdapter.Map(propertyPolicy);
            _propertyPolicyRepository.PropertyPolicyCreate(builder.Build());
        }

        public void Delete(Guid id)
        {
           // _propertyPolicyRepository.PropertyPolicyDelete(id);
            _propertyPolicyRepository.Delete(id);
        }

        public void PropertyPolicyUpdate(PropertyPolicyDto propertyPolicy)
        {
            var builder = PropertyPolicyAdapter.Map(propertyPolicy);
            _propertyPolicyRepository.PropertyPolicyUpdate(builder.Build());
        }

        public void ToggleAndSaveIsActive(Guid guid)
        {
            _propertyPolicyRepository.ToggleAndSaveIsActive(guid);
        }

        public IListDto<PolicyTypeDto> GetAllPolicyTypes()
        {
            return _propertyTypeReadRepository.GetAllPolicyTypes();
        }
    }
}
