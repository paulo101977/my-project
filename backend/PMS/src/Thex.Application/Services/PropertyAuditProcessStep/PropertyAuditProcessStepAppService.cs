﻿using System;
using Thex.Application.Adapters;
using Thex.Domain.Entities;
using Thex.Application.Interfaces;
using Tnf.Domain.Services;
using Tnf.Notifications;

namespace Thex.Application.Services
{
    public class PropertyAuditProcessStepAppService : ScaffoldPropertyAuditProcessStepAppService, IPropertyAuditProcessStepAppService
    {
        public PropertyAuditProcessStepAppService(
            IPropertyAuditProcessStepAdapter propertyAuditProcessStepAdapter,
            IDomainService<PropertyAuditProcessStep> propertyAuditProcessStepDomainService,
            INotificationHandler notificationHandler)
            : base(propertyAuditProcessStepAdapter, propertyAuditProcessStepDomainService, notificationHandler)
        {
        }
    }
}
