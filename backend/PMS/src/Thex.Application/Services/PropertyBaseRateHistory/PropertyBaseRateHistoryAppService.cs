﻿using System;
using Thex.Application.Adapters;
using Thex.Domain.Entities;
using Thex.Application.Interfaces;
using Tnf.Domain.Services;
using Tnf.Notifications;

namespace Thex.Application.Services
{
    public class PropertyBaseRateHistoryAppService : ScaffoldPropertyBaseRateHistoryAppService, IPropertyBaseRateHistoryAppService
    {
        public PropertyBaseRateHistoryAppService(
            IPropertyBaseRateHistoryAdapter propertyBaseRateHistoryAdapter,
            IDomainService<PropertyBaseRateHistory> propertyBaseRateHistoryDomainService,
            INotificationHandler notificationHandler)
                : base(propertyBaseRateHistoryAdapter, propertyBaseRateHistoryDomainService, notificationHandler)
        {
        }
    }
}
