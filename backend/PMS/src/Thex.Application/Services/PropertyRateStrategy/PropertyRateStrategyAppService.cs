﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Thex.Application.Interfaces;
using Thex.Common;
using Thex.Common.Enumerations;
using Thex.Domain.Entities;
using Thex.Domain.Interfaces.Repositories;
using Thex.Domain.Interfaces.Services;
using Thex.Dto;
using Thex.Dto.RoomTypes;
using Thex.Infra.ReadInterfaces;
using Thex.Kernel;
using Tnf.Dto;
using Tnf.Notifications;
using Tnf.Repositories.Uow;

namespace Thex.Application.Services
{
    public class PropertyRateStrategyAppService : ApplicationServiceBase, IPropertyRateStrategyAppService
    {
        private readonly IPropertyRateStrategyRepository _propertyRateStrategyRepository;
        private readonly IPropertyParameterReadRepository _propertyParameterReadRepository;
        private readonly IApplicationUser _applicationUser;
        private readonly IRatePlanAppService _ratePlanAppService;
        private readonly IPropertyRateStrategyDomainService _propertyRateStrategyDomainService;
        private readonly IPropertyRateStrategyReadRepository _propertyRateStrategyReadRepository;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IRoomTypeAppService _roomTypeAppService;

        public PropertyRateStrategyAppService(
           INotificationHandler notificationHandler,
           IPropertyRateStrategyRepository propertyRateStrategyRepository,
           IPropertyParameterReadRepository propertyParameterReadRepository,
           IApplicationUser applicationUser,
           IRatePlanAppService ratePlanAppService,
           IPropertyRateStrategyDomainService propertyRateStrategyDomainService,
           IPropertyRateStrategyReadRepository propertyRateStrategyReadRepository,
           IUnitOfWorkManager unitOfWorkManager,
           IRoomTypeAppService roomTypeAppService)
           : base(notificationHandler)
        {
            _propertyRateStrategyRepository = propertyRateStrategyRepository;
            _propertyParameterReadRepository = propertyParameterReadRepository;
            _applicationUser = applicationUser;
            _ratePlanAppService = ratePlanAppService;
            _propertyRateStrategyDomainService = propertyRateStrategyDomainService;
            _propertyRateStrategyReadRepository = propertyRateStrategyReadRepository;
            _unitOfWorkManager = unitOfWorkManager;
            _roomTypeAppService = roomTypeAppService;
        }

        public IListDto<GetAllPropertyRateStrategyDto> GetAllByPropertyId()
        {
            int propertyId = int.Parse(_applicationUser.PropertyId);
            return _propertyRateStrategyReadRepository.GetAllByPropertyId(propertyId);
        }

        public async Task<PropertyRateStrategyDto> CreateAsync(PropertyRateStrategyDto propertyRateStrategyDto)
        {
            using (var uow = _unitOfWorkManager.Begin())
            {
                if (propertyRateStrategyDto == null)
                {
                    NotifyNullParameter();
                    return PropertyRateStrategyDto.NullInstance;
                }

                int propertyId = int.Parse(_applicationUser.PropertyId);

                var builder = await GetPropertyRateStrategyBuilder(propertyRateStrategyDto, Guid.NewGuid(), propertyId);

                if (Notification.HasNotification())
                    return PropertyRateStrategyDto.NullInstance;

                propertyRateStrategyDto.Id = _propertyRateStrategyRepository.Create(builder.Build()).Id;

                uow.Complete();
            }
            return propertyRateStrategyDto;
        }

        public async Task<PropertyRateStrategyDto> UpdateAsync(Guid id, PropertyRateStrategyDto propertyRateStrategyDto)
        {
            using (var uow = _unitOfWorkManager.Begin())
            {
                if (propertyRateStrategyDto == null)
                {
                    NotifyNullParameter();
                    return PropertyRateStrategyDto.NullInstance;
                }

                int propertyId = int.Parse(_applicationUser.PropertyId);

                var existingPropertyRateStrategy = _propertyRateStrategyRepository.GetById(id);

                var builder = await GetPropertyRateStrategyBuilder(propertyRateStrategyDto, id, propertyId, true);

                if (Notification.HasNotification())
                    return PropertyRateStrategyDto.NullInstance;

                propertyRateStrategyDto.Id = _propertyRateStrategyRepository.Update(builder.Build(), existingPropertyRateStrategy).Id;

                uow.Complete();
            }
            return propertyRateStrategyDto;
        }

        private async Task<bool> InvalidRangeOccupation(DateTime startDate, DateTime endDate, decimal minOccupation, decimal maxOccupation, int roomTypeId, Guid propertyRateStrategyId)
        {
            int propertyId = int.Parse(_applicationUser.PropertyId);

            var result = await _propertyRateStrategyReadRepository.GetAllWithRatePlanByPropertyIdAsync(propertyId);
         
            result.Remove(result.FirstOrDefault(x => x.Id.Equals(propertyRateStrategyId)));

            var invalidOccupation =
                 result.Any(x =>
                    x.PropertyRateStrategyRoomTypeList.Any(y =>
                        (
                            ((minOccupation >= y.MinOccupationPercentual && minOccupation <= y.MaxOccupationPercentual) ||
                            (maxOccupation >= y.MinOccupationPercentual && maxOccupation <= y.MaxOccupationPercentual)) &&
                            y.RoomTypeId.Equals(roomTypeId)
                        )
                    )) &&
                 result.Any(x =>
                    (
                        (x.StartDate.Date >= startDate.Date && x.StartDate.Date <= endDate.Date) ||
                        (x.EndDate.Date >= startDate.Date && x.EndDate.Date <= endDate.Date)
                    ));

            return invalidOccupation;
        }

        private async Task<PropertyRateStrategy.Builder> GetPropertyRateStrategyBuilder(PropertyRateStrategyDto propertyRateStrategyDto, Guid id, int propertyId, bool update = false)
        {
            DateTime? systemDate = _propertyParameterReadRepository.GetSystemDate(propertyId);

            if (systemDate == null)
            {
                Notification.Raise(Notification.DefaultBuilder
                .WithMessage(AppConsts.LocalizationSourceName, PropertyAuditProcess.EntityError.PropertyAuditProcessInvalidPropertySystemDate)
                .WithDetailedMessage(AppConsts.LocalizationSourceName, PropertyAuditProcess.EntityError.PropertyAuditProcessInvalidPropertySystemDate)
                .Build());
                return null;
            }

            if (_propertyRateStrategyDomainService.PeriodIsInvalid(propertyRateStrategyDto.StartDate, propertyRateStrategyDto.EndDate))
            {
                Notification.Raise(Notification.DefaultBuilder
                .WithMessage(AppConsts.LocalizationSourceName, PropertyRateStrategyEnum.Error.InvalidPeriod)
                .WithDetailedMessage(AppConsts.LocalizationSourceName, PropertyRateStrategyEnum.Error.InvalidPeriod)
                .Build());
                return null;
            }

            var propertyRateStrategyBuilder = new PropertyRateStrategy.Builder(Notification);

            propertyRateStrategyBuilder
                .WithId(id)
                .WithPropertyId(propertyId)
                .WithIsActive(propertyRateStrategyDto.IsActive)
                .WithName(propertyRateStrategyDto.Name)
                .WithStartDate(propertyRateStrategyDto.StartDate)
                .WithEndDate(propertyRateStrategyDto.EndDate);        

            //Tipos de UH

            IList<RoomTypeDto> propertyRoomTypeList = null;

            if (propertyRateStrategyDto.PropertyRateStrategyRoomTypeList.Any(r => r.RoomTypeId == 0))
            {
                if (propertyRateStrategyDto.PropertyRateStrategyRoomTypeList.Count(r => r.RoomTypeId == 0) != propertyRateStrategyDto.PropertyRateStrategyRoomTypeList.Count())
                {
                    Notification.Raise(Notification.DefaultBuilder
                    .WithMessage(AppConsts.LocalizationSourceName, PropertyRateStrategyEnum.Error.InvalidPropertyRateStrategyRoomTypes)
                    .WithDetailedMessage(AppConsts.LocalizationSourceName, PropertyRateStrategyEnum.Error.InvalidPropertyRateStrategyRoomTypes)
                    .Build());
                    return null;
                }
                propertyRoomTypeList = _roomTypeAppService.GetRoomTypesByPropertyId(new GetAllRoomTypesDto(), propertyId).Items;
            }

            var propertyRateStrategyRoomTypeList = new List<PropertyRateStrategyRoomType>();

            foreach (var propertyRateStrategyRoomTypeDto in propertyRateStrategyDto.PropertyRateStrategyRoomTypeList)
            {
                if (propertyRateStrategyRoomTypeDto.RoomTypeId == 0)
                {
                    foreach (var roomType in propertyRoomTypeList)
                    {
                        var propertyRateStrategyRoomTypeBuilder = (await GetPropertyRateStrategyRoomTypeBuilder(propertyRateStrategyRoomTypeDto, propertyRateStrategyDto, id, roomType));

                        if (Notification.HasNotification())
                            return null;

                        propertyRateStrategyRoomTypeList.Add(propertyRateStrategyRoomTypeBuilder.Build());
                    }
                }
                else
                {
                    var propertyRateStrategyRoomTypeBuilder = (await GetPropertyRateStrategyRoomTypeBuilder(propertyRateStrategyRoomTypeDto, propertyRateStrategyDto, id));

                    if (Notification.HasNotification())
                        return null;

                    propertyRateStrategyRoomTypeList.Add(propertyRateStrategyRoomTypeBuilder.Build());
                }
            }

            propertyRateStrategyBuilder.WithPropertyRateStrategyRoomTypeList(propertyRateStrategyRoomTypeList);

            return propertyRateStrategyBuilder;
        }

        private PropertyRateStrategyRatePlan.Builder GetPropertyRateStrategyRatePlanBuilder(RatePlanDto dto)
        {
            var builder = new PropertyRateStrategyRatePlan.Builder(Notification);

            builder
                .WithId(Guid.Empty)
                .WithRatePlanId(dto.Id);

            return builder;
        }

        private PropertyRateStrategyRatePlan.Builder GetPropertyRateStrategyRatePlanBuilder(PropertyRateStrategyRatePlanDto dto)
        {
            var builder = new PropertyRateStrategyRatePlan.Builder(Notification);

            builder
                .WithId(dto.Id == Guid.Empty ? Guid.NewGuid() : dto.Id)
                .WithPropertyRateStrategyId(dto.PropertyRateStrategyId)
                .WithRatePlanId(dto.RatePlanId);

            return builder;
        }

        private async Task<PropertyRateStrategyRoomType.Builder> GetPropertyRateStrategyRoomTypeBuilder(PropertyRateStrategyRoomTypeDto propertyRateStrategyRoomTypeDto, 
            PropertyRateStrategyDto propertyRateStrategyDto, Guid propertyRateStrategyId, RoomTypeDto roomType = null)
        {
            var builder = new PropertyRateStrategyRoomType.Builder(Notification);

            if (propertyRateStrategyRoomTypeDto.MinOccupationPercentual < 0 || propertyRateStrategyRoomTypeDto.MaxOccupationPercentual < 0)
            {
                Notification.Raise(Notification.DefaultBuilder
               .WithMessage(AppConsts.LocalizationSourceName, PropertyRateStrategyEnum.Error.InvalidMinOccupation)
               .WithDetailedMessage(AppConsts.LocalizationSourceName, PropertyRateStrategyEnum.Error.InvalidMinOccupation)
               .Build());
                return null;
            }

            if (propertyRateStrategyRoomTypeDto.MinOccupationPercentual > 100 || propertyRateStrategyRoomTypeDto.MaxOccupationPercentual > 100)
            {
                Notification.Raise(Notification.DefaultBuilder
               .WithMessage(AppConsts.LocalizationSourceName, PropertyRateStrategyEnum.Error.InvalidMaxOccupation)
               .WithDetailedMessage(AppConsts.LocalizationSourceName, PropertyRateStrategyEnum.Error.InvalidMaxOccupation)
               .Build());
                return null;
            }

            if (propertyRateStrategyRoomTypeDto.MinOccupationPercentual > propertyRateStrategyRoomTypeDto.MaxOccupationPercentual)
            {
                Notification.Raise(Notification.DefaultBuilder
               .WithMessage(AppConsts.LocalizationSourceName, PropertyRateStrategyEnum.Error.InvalidOccupation)
               .WithDetailedMessage(AppConsts.LocalizationSourceName, PropertyRateStrategyEnum.Error.InvalidOccupation)
               .Build());
                return null;
            }

            if (propertyRateStrategyRoomTypeDto.PercentualAmount < 0 || propertyRateStrategyRoomTypeDto.IncrementAmount < 0)
            {
                Notification.Raise(Notification.DefaultBuilder
               .WithMessage(AppConsts.LocalizationSourceName, PropertyRateStrategyEnum.Error.InvalidAmount)
               .WithDetailedMessage(AppConsts.LocalizationSourceName, PropertyRateStrategyEnum.Error.InvalidAmount)
               .Build());
                return null;
            }

            if (propertyRateStrategyRoomTypeDto.PercentualAmount != null && propertyRateStrategyRoomTypeDto.IncrementAmount != null)
            {
                Notification.Raise(Notification.DefaultBuilder
               .WithMessage(AppConsts.LocalizationSourceName, PropertyRateStrategyEnum.Error.InvalidAmountNotNull)
               .WithDetailedMessage(AppConsts.LocalizationSourceName, PropertyRateStrategyEnum.Error.InvalidAmountNotNull)
               .Build());
                return null;
            }

            if (propertyRateStrategyRoomTypeDto.PercentualAmount == null && propertyRateStrategyRoomTypeDto.IncrementAmount == null)
            {
                Notification.Raise(Notification.DefaultBuilder
               .WithMessage(AppConsts.LocalizationSourceName, PropertyRateStrategyEnum.Error.InvalidAmountNull)
               .WithDetailedMessage(AppConsts.LocalizationSourceName, PropertyRateStrategyEnum.Error.InvalidAmountNull)
               .Build());
                return null;
            }

            _propertyRateStrategyDomainService.ValidateDuplicateRoomTypeAndOccupationInDto(propertyRateStrategyRoomTypeDto, propertyRateStrategyDto.PropertyRateStrategyRoomTypeList);

            if (Notification.HasNotification())
                return null;

            if (await InvalidRangeOccupation(propertyRateStrategyDto.StartDate, propertyRateStrategyDto.EndDate, propertyRateStrategyRoomTypeDto.MinOccupationPercentual,
               propertyRateStrategyRoomTypeDto.MaxOccupationPercentual, roomType == null ? propertyRateStrategyRoomTypeDto.RoomTypeId : roomType.Id, propertyRateStrategyId))
            {
                Notification.Raise(Notification.DefaultBuilder
               .WithMessage(AppConsts.LocalizationSourceName, PropertyRateStrategyEnum.Error.InvalidRangeOccupation)
               .WithDetailedMessage(AppConsts.LocalizationSourceName, PropertyRateStrategyEnum.Error.InvalidRangeOccupation)
               .Build());
                return null;
            }

            builder
                .WithId(propertyRateStrategyRoomTypeDto.Id == Guid.Empty ? Guid.NewGuid() : propertyRateStrategyRoomTypeDto.Id)
                .WithPropertyRateStrategyId(propertyRateStrategyRoomTypeDto.PropertyRateStrategyId)
                .WithRoomTypeId(roomType == null ? propertyRateStrategyRoomTypeDto.RoomTypeId : roomType.Id)
                .WithMinOccupationPercentual(propertyRateStrategyRoomTypeDto.MinOccupationPercentual)
                .WithMaxOccupationPercentual(propertyRateStrategyRoomTypeDto.MaxOccupationPercentual)
                .WithIsDecreased(propertyRateStrategyRoomTypeDto.IsDecreased)
                .WithPercentualAmount(propertyRateStrategyRoomTypeDto.PercentualAmount)
                .WithIncrementAmount(propertyRateStrategyRoomTypeDto.IncrementAmount);

            return builder;
        }

        public void ToggleAndSaveIsActive(Guid id)
        {
            _propertyRateStrategyRepository.ToggleAndSaveIsActive(id);
        }

        public async Task<PropertyRateStrategyDto> GetPropertyRateStrategyById(Guid propertyRateStrategyId)
        {
           return await _propertyRateStrategyReadRepository.GetPropertyRateStrategyById(propertyRateStrategyId);
        }
    }
}
