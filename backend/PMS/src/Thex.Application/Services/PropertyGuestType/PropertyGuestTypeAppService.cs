﻿using System;
using Thex.Application.Adapters;
using Thex.Domain.Entities;
using Thex.Application.Interfaces;
using Tnf.Domain.Services;
using Thex.Dto;
using System.Threading.Tasks;
using Thex.Infra.ReadInterfaces;
using Tnf.Dto;
using Tnf.Notifications;
using Thex.Kernel;
using Thex.Domain.Interfaces.Repositories;

namespace Thex.Application.Services
{
    public class PropertyGuestTypeAppService : ScaffoldPropertyGuestTypeAppService, IPropertyGuestTypeAppService
    {
        private readonly IApplicationUser _applicationUser;
        private readonly IPropertyGuestTypeReadRepository _propertyGuestTypeReadRepository;
        private readonly IPropertyGuestTypeRepository _propertyGuestTypeRepository;

        public PropertyGuestTypeAppService(
            IPropertyGuestTypeAdapter propertyGuestTypeAdapter,
            IDomainService<PropertyGuestType> propertyGuestTypeDomainService, 
            IPropertyGuestTypeReadRepository propertyGuestTypeReadRepository,
            IPropertyGuestTypeRepository propertyGuestTypeRepository,
            IApplicationUser applicationUser,
            INotificationHandler notificationHandler)
            : base(propertyGuestTypeAdapter, propertyGuestTypeDomainService, notificationHandler)
        {
            _applicationUser = applicationUser;
            _propertyGuestTypeReadRepository = propertyGuestTypeReadRepository;
            _propertyGuestTypeRepository = propertyGuestTypeRepository;
        }

        public virtual async Task<IListDto<PropertyGuestTypeDto>> GetAllByPropertyId(GetAllPropertyGuestTypeDto request, int propertyId)
        {
            ValidateRequestAllDto(request, nameof(request));

            if (Notification.HasNotification())
                return null;

            var response = await PropertyGuestTypeDomainService.GetAllAsync<PropertyGuestTypeDto>(request, exp =>exp.Property.Id == propertyId).ConfigureAwait(false);


            foreach (var item in response.Items)
            {
                item.IsIncognito = item.IsIncognito ?? false;
                item.IsActive = item.IsActive ?? false;
            }

            return response;
        }

        public override  Task<PropertyGuestTypeDto> Create(PropertyGuestTypeDto dto)
        {
            if (dto != null)
            {
                dto.PropertyId = Convert.ToInt32(_applicationUser.PropertyId);
            }

            return base.Create(dto);
        }

        public override async Task<PropertyGuestTypeDto> Update(int id, PropertyGuestTypeDto dto)
        {
            if (dto != null)
            {
                dto.PropertyId = Convert.ToInt32(_applicationUser.PropertyId);
            }

            if (!_propertyGuestTypeReadRepository.Any(id, Convert.ToInt32(_applicationUser.PropertyId)))
            {
                NotifyParameterInvalid();
                return null;
            }

            return await base.Update(id, dto);
        }

        public override async Task<PropertyGuestTypeDto> Get(DefaultIntRequestDto id)
        {
            var propertyGuestType = await base.Get(id);

            if (propertyGuestType != null)
            {
                if (propertyGuestType.PropertyId != Convert.ToInt32(_applicationUser.PropertyId))
                {
                    NotifyParameterInvalid();
                    return null;
                }

                propertyGuestType.IsIncognito = propertyGuestType.IsIncognito ?? false;
                propertyGuestType.IsActive = propertyGuestType.IsActive ?? false;
            }

            return propertyGuestType;
        }

        public override async Task Delete(int id)
        {
            if (!_propertyGuestTypeReadRepository.Any(id, Convert.ToInt32(_applicationUser.PropertyId)))
            {
                NotifyParameterInvalid();
                return;
            }

            await base.Delete(id);
        }

        public void ToggleActivation(int id)
        {
            if (id <= 0)
            {
                NotifyIdIsMissing();
                return;
            }

            if (!_propertyGuestTypeReadRepository.Any(id, Convert.ToInt32(_applicationUser.PropertyId)))
            {
                NotifyParameterInvalid();
                return;
            }

            _propertyGuestTypeRepository.ToggleAndSaveIsActive(id);
        }
    }
}
