﻿using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using Thex.Application.Adapters;
using Thex.Application.Interfaces;
using Thex.Common;
using Thex.Common.Enumerations;
using Thex.Common.Helpers;
using Thex.Domain.Entities;
using Thex.Domain.Interfaces.Repositories;
using Thex.Domain.Interfaces.Services;
using Thex.Domain.Services.Interfaces;
using Thex.Dto;
using Thex.Dto.BillingAccountItem;
using Thex.Dto.BillingItem;
using Thex.Infra.ReadInterfaces;
using Thex.Kernel;
using Tnf.Domain.Services;
using Tnf.Notifications;
using Tnf.Repositories.Uow;

namespace Thex.Application.Services
{
    public class BillingAccountItemAppService : ScaffoldBillingAccountItemAppService, IBillingAccountItemAppService
    {
        private readonly IBillingItemReadRepository _billingItemReadRepository;
        private readonly IBillingAccountItemDomainService _billingAccountItemDomainService;
        private readonly IBillingAccountItemRepository _billingAccountItemRepository;
        private readonly IBillingAccountItemReadRepository _billingAccountItemReadRepository;
        private readonly IBillingAccountDomainService _billingAccountDomainService;
        private readonly IReasonReadRepository _reasonReadRepository;
        private readonly IBillingAccountItemTransferAppService _billingAccountItemTransferAppService;
        private readonly IBillingAccountReadRepository _billingAccountReadRepository;
        private readonly IReservationConfirmationReadRepository _reservationConfirmationReadRepository;
        private readonly IPropertyCurrencyExchangeReadRepository _propertyCurrencyExchangeReadRepository;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IPropertyReadRepository _propertyReadRepository;
        private readonly IRatePlanReadRepository _ratePlanReadRepository;
        private readonly IRatePlanDomainService _ratePlanDomainService;
        private readonly IPropertyParameterReadRepository _PropertyParameter;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly ServicesConfiguration _servicesConfiguration;
        private readonly IApplicationUser _applicationUser;
        private readonly string _urlMostSelected = "/api/userpreference/mostselected";
        private readonly IBillingInvoicePropertySupportedTypeReadRepository _billingInvoicePropertySupportedTypeReadRepository;
        private readonly IBillingInvoicePropertyRepository _billingInvoicePropertyRepository;
        private readonly IBillingInvoiceAdapter _billingInvoiceAdapter;
        private readonly IBillingInvoiceRepository _billingInvoiceRepository;

        public BillingAccountItemAppService(
            IUnitOfWorkManager unitOfWorkManager,
            IBillingAccountItemAdapter billingAccountItemAdapter,
            IDomainService<BillingAccountItem> billingAccountItemDomainService,
            IBillingItemReadRepository billingItemReadRepository,
            IBillingAccountItemDomainService billingAccountItemBaseDomainService,
            IBillingAccountItemRepository billingAccountItemRepository,
            IBillingAccountItemReadRepository billingAccountItemReadRepository,
            IBillingAccountDomainService billingAccountDomainService,
            IReasonReadRepository reasonReadRepository,
            IBillingAccountItemTransferAppService billingAccountItemTransferAppService,
            IBillingAccountReadRepository billingAccountReadRepository,
            IReservationConfirmationReadRepository reservationConfirmationReadRepository,
            IPropertyCurrencyExchangeReadRepository propertyCurrencyExchangeReadRepository,
            IHttpContextAccessor httpContextAccessor,
            IPropertyReadRepository propertyReadRepository,
            IRatePlanReadRepository ratePlanReadRepository,
            IRatePlanDomainService ratePlanDomainService,
            IPropertyParameterReadRepository propertyParameterReadRepository,
            INotificationHandler notificationHandler,
            IApplicationUser applicationUser,
            ServicesConfiguration serviceConfiguration,
            IBillingInvoicePropertySupportedTypeReadRepository billingInvoicePropertySupportedTypeReadRepository,
            IBillingInvoicePropertyRepository billingInvoicePropertyRepository,
            IBillingInvoiceAdapter billingInvoiceAdapter,
            IBillingInvoiceRepository billingInvoiceRepository
            )
            : base(billingAccountItemAdapter, billingAccountItemDomainService, notificationHandler)
        {
            _unitOfWorkManager = unitOfWorkManager;
            _billingItemReadRepository = billingItemReadRepository;
            _billingAccountItemDomainService = billingAccountItemBaseDomainService;
            _billingAccountItemRepository = billingAccountItemRepository;
            _billingAccountItemReadRepository = billingAccountItemReadRepository;
            _billingAccountDomainService = billingAccountDomainService;
            _reasonReadRepository = reasonReadRepository;
            _billingAccountItemTransferAppService = billingAccountItemTransferAppService;
            _billingAccountReadRepository = billingAccountReadRepository;
            _reservationConfirmationReadRepository = reservationConfirmationReadRepository;
            _propertyCurrencyExchangeReadRepository = propertyCurrencyExchangeReadRepository;
            _httpContextAccessor = httpContextAccessor;
            _propertyReadRepository = propertyReadRepository;
            _ratePlanReadRepository = ratePlanReadRepository;
            _ratePlanDomainService = ratePlanDomainService;
            _PropertyParameter = propertyParameterReadRepository;
            _servicesConfiguration = serviceConfiguration;
            _applicationUser = applicationUser;
            _billingInvoicePropertySupportedTypeReadRepository = billingInvoicePropertySupportedTypeReadRepository;
            _billingInvoicePropertyRepository = billingInvoicePropertyRepository;
            _billingInvoiceAdapter = billingInvoiceAdapter;
            _billingInvoiceRepository = billingInvoiceRepository;
        }

        public virtual BillingAccountItemDebitDto CreateDebit(BillingAccountItemDebitDto dto, bool ignoreFiltersForIntegration = false)
        {
            using (var uow = _unitOfWorkManager.Begin())
            {
                ValidateDto<BillingAccountItemDebitDto>(dto, nameof(dto));

                if (Notification.HasNotification()) return null;

                ValidateDebit(dto, ignoreFiltersForIntegration);

                if (Notification.HasNotification()) return null;

                ValidateDailyDebit(dto);

                MapAndCreateDebit(dto);

                if (Notification.HasNotification()) return null;
               
                CreateTaxBasedOnDebit(dto);

                if (Notification.HasNotification()) return null;             

                CreateMostSelectedDebitAsync(dto);

                uow.Complete();
            }

            return dto;
        }

        private void ValidateDailyDebit(BillingAccountItemDebitDto dto)
        {
            var dailyBillingItemId = _PropertyParameter.GetDailyBillingItem(int.Parse(_applicationUser.PropertyId));

            if (dailyBillingItemId.HasValue && dailyBillingItemId.Value == dto.BillingItemId)
                dto.Daily = true;
        }

        private void CreateMostSelectedDebitAsync(BillingAccountItemDebitDto dto)
        {
            if (dto.BillingItemId > 0 && dto.DetailList != null && dto.DetailList.Count > 0)
            {
                System.Threading.Tasks.Task.Run(() =>
                {
                    try
                    {
                        var _token = _httpContextAccessor.HttpContext.Request.Headers.FirstOrDefault(x => x.Key.Equals("Authorization")).Value.ToString();
                        var _complete = false;
                        var _body = new Preference.Dto.MostSelectedDto()
                        {
                            UserId = _applicationUser.UserUid,
                            Type = (int)MostSelectedTypeEnum.MostSelectedService
                        };

                        dto.DetailList.ForEach(item =>
                        {
                            _body.Details = new List<Preference.Dto.MostSelectedDetailsDto>()
                            {
                                    new Preference.Dto.MostSelectedDetailsDto()
                                    {
                                        BillingItemId = dto.BillingItemId.ToString(),
                                        Description = item.Description,
                                        ExternalId = item.ExternalId,
                                        Quantity = 1,
                                        GroupDescription = item.GroupDescription,
                                        Code = item.Code
                                    }
                            };
                        });

                        HttpClientHelper.SendRequest<bool>(string.Concat(_servicesConfiguration.PreferenceAPI, _urlMostSelected),
                                                           string.Empty, JsonConvert.SerializeObject(_body), ref _complete, _token);
                    }
                    catch
                    {
                    }
                });
            }
        }

        public virtual BillingAccountItemCreditDto CreateCredit(BillingAccountItemCreditDto dto)
        {
            using (var uow = _unitOfWorkManager.Begin())
            {
                ValidateDto<BillingAccountItemCreditDto>(dto, nameof(dto));

                if (Notification.HasNotification()) return null;

                ValidateCredit(dto);

                if (Notification.HasNotification()) return null;

                MapAndCreateCredit(dto);

                if (Notification.HasNotification()) return null;

                CreateMostSelectedCreditAsync(dto);

                uow.Complete();
            }

            return dto;
        }

        private void CreateMostSelectedCreditAsync(BillingAccountItemCreditDto dto)
        {
            if (dto.BillingItemId > 0 && dto.DetailList != null && dto.DetailList.Count > 0)
            {
                System.Threading.Tasks.Task.Run(() =>
                {
                    try
                    {
                        var _token = _httpContextAccessor.HttpContext.Request.Headers.FirstOrDefault(x => x.Key.Equals("Authorization")).Value.ToString();
                        var _complete = false;
                        var _body = new Preference.Dto.MostSelectedDto()
                        {
                            UserId = _applicationUser.UserUid,
                            Type = (int)MostSelectedTypeEnum.MostSelectedCredit
                        };

                        dto.DetailList.ForEach(item =>
                        {
                            _body.Details = new List<Preference.Dto.MostSelectedDetailsDto>()
                        {
                            new Preference.Dto.MostSelectedDetailsDto()
                            {
                                BillingItemId = dto.BillingItemId.ToString(),
                                Description = item.Description,
                                ExternalId = item.ExternalId,
                                Quantity = 1,
                                GroupDescription = item.GroupDescription,
                                Code = item.Code
                            }
                        };
                        });

                        HttpClientHelper.SendRequest<bool>(string.Concat(_servicesConfiguration.PreferenceAPI, _urlMostSelected),
                                                                         string.Empty, JsonConvert.SerializeObject(_body), ref _complete, _token);
                    }
                    catch
                    {
                    }
                });
            }
        }

        private decimal? GetExchangeRateById(Guid id)
                    => _propertyCurrencyExchangeReadRepository.GetExchangeRateById(id);

        public BillingAccountItemReversalParentDto CreateReversal(BillingAccountItemReversalParentDto dto)
        {
            using (var uow = _unitOfWorkManager.Begin())
            {
                Guid billinAccountId;
                ValidateDto<BillingAccountItemReversalParentDto>(dto, nameof(dto));

                if (Notification.HasNotification()) return null;

                ValidateBillingAccountsAlreadyReversed(dto);
                ValidateDuplicates(dto);

                if (Notification.HasNotification()) return null;

                IsClosedOrBlockedAccount(dto.BillingAccountItems.FirstOrDefault().BillingAccountId);

                if (Notification.HasNotification()) return null;

                HasLinkedInvoice(dto.BillingAccountItems.FirstOrDefault().OriginalBillingAccountItemId);

                if (Notification.HasNotification()) return null;

                foreach (var item in dto.BillingAccountItems)
                {

                    CheckIfReasonIsReversal(item.ReasonId);

                    if (Notification.HasNotification()) return null;

                    var billingAccountItemOriginalList = _billingAccountItemReadRepository.GetAllWithChildrenByBillingAccountId(new DefaultGuidRequestDto(item.OriginalBillingAccountItemId));

                    if (billingAccountItemOriginalList == null || billingAccountItemOriginalList.Count == 0)
                    {
                        NotifyParameterInvalid();
                        return null;
                    }

                    billinAccountId = item.BillingAccountId;
                    var originalBillingAccountItem = billingAccountItemOriginalList.FirstOrDefault(e => e.Id == item.OriginalBillingAccountItemId);

                    CanNotBeATax(originalBillingAccountItem);

                    if (Notification.HasNotification())
                        return null;

                    var billingAccountItemList = MapReversal(item, billingAccountItemOriginalList);

                    if (Notification.HasNotification())
                        return null;

                    UpdateReversalBillingAccountItems(item, billingAccountItemOriginalList, billingAccountItemList, item.ReasonId, item.Observation);
                }

                uow.Complete();
            }

            return dto;
        }

        public virtual BillingAccountItemTransferSourceDestinationDto CreateTransfer(BillingAccountItemTransferSourceDestinationDto dto)
        {
            using (var uow = _unitOfWorkManager.Begin())
            {
                ValidateDto<BillingAccountItemTransferSourceDestinationDto>(dto, nameof(dto));

                if (Notification.HasNotification())
                    return null;

                var billingAccountItemsIds = GetBillingAccountItemsIds(dto.BillingAccountItemTransferList);

                ValidateTransfer(dto, billingAccountItemsIds);

                if (Notification.HasNotification())
                    return null;

                var billingAccountItems = _billingAccountItemReadRepository.GetAllWithChildrenExceptReversedItems(billingAccountItemsIds);

                if (Notification.HasNotification())
                    return null;

                _billingAccountItemRepository.TransferRange(dto.BillingAccountItemIdDestination, billingAccountItems);

                if (Notification.HasNotification())
                    return null;

                CreateBillingAccountItemTransferLines(dto.BillingAccountItemIdDestination, billingAccountItems);

                if (Notification.HasNotification())
                    return null;

                uow.Complete();
            }

            return dto;
        }

        public virtual BillingAccountItemUndoTransferSourceDestinationDto UndoTransfer(BillingAccountItemUndoTransferSourceDestinationDto dto)
        {
            using (var uow = _unitOfWorkManager.Begin())
            {
                ValidateDto<BillingAccountItemUndoTransferSourceDestinationDto>(dto, nameof(dto));

                if (Notification.HasNotification())
                    return null;

                if (dto.BillingAccountItemTransferList == null || dto.BillingAccountItemTransferList.Count() == 0)
                {
                    NotifyParameterInvalid();
                    return null;
                }

                var billingAccountItemsIds = GetBillingAccountItemsIds(dto.BillingAccountItemTransferList);
                var billingAccountItems = _billingAccountItemReadRepository.GetAllWithChildrenExceptReversedItems(billingAccountItemsIds);
                var billingAccountItemLastSourceIdList = GetBillingAccountItemIdList(billingAccountItems);

                ValidateUndoTransfer(billingAccountItemLastSourceIdList, billingAccountItemsIds);

                if (Notification.HasNotification())
                    return null;

                foreach (var billingAccountItemIdDestination in billingAccountItemLastSourceIdList)
                {
                    if (Notification.HasNotification())
                        return null;

                    _billingAccountItemRepository.TransferRange(billingAccountItemIdDestination, billingAccountItems);

                    if (Notification.HasNotification())
                        return null;

                    CreateBillingAccountItemTransferLines(billingAccountItemIdDestination, billingAccountItems);

                    if (Notification.HasNotification())
                        return null;
                }

                uow.Complete();
            }

            return dto;
        }

        private List<Guid> GetBillingAccountItemIdList(List<BillingAccountItem> billingAccountItems)
        {
            return billingAccountItems
                                                        .Where(e => e.BillingAccountIdLastSource.HasValue)
                                                        .Select(e => e.BillingAccountIdLastSource.Value)
                                                        .Distinct()
                                                        .ToList();
        }

        private void ValidateUndoTransfer(List<Guid> billingAccountIdList, List<Guid> billingAccountItemsIdList)
        {
            if (
                billingAccountItemsIdList == null ||
                billingAccountItemsIdList.Count() == 0 ||
                billingAccountIdList == null ||
                billingAccountIdList.Count() == 0)
            {
                NotifyParameterInvalid();
                return;
            }

            if (_billingAccountItemReadRepository.AnyReversalOrTaxByBillingAccountItemIdList(billingAccountItemsIdList))
            {
                NotifyParameterInvalid();
                return;
            }

            AnyClosedOrBlockedAccount(billingAccountIdList);

            HasLinkedInvoice(billingAccountItemsIdList);
        }


        public void CreateTransferAccountsToAccount(Guid billingAccountDestination, List<Guid> billingAccountIdList, List<Guid> billingAccountItemsIdList)
        {
            using (var uow = _unitOfWorkManager.Begin())
            {
                var billingAccountItems = _billingAccountItemReadRepository.GetAllWithChildrenExceptReversedItemsByBillingAccountIdListAndItemIdList(billingAccountIdList, billingAccountItemsIdList);

                if (Notification.HasNotification())
                    return;

                _billingAccountItemRepository.TransferRange(billingAccountDestination, billingAccountItems);

                if (Notification.HasNotification())
                    return;

                CreateBillingAccountItemTransferLines(billingAccountDestination, billingAccountItems);

                if (Notification.HasNotification())
                    return;

                uow.Complete();
            }
        }

        public void CreateTransferAccountsToAccount(Guid billingAccountDestination, List<Guid> billingAccountIds)
        {
            using (var uow = _unitOfWorkManager.Begin())
            {
                var billingAccountItems = _billingAccountItemReadRepository.GetAllWithChildrenExceptReversedItemsByBillingAccountIdList(billingAccountIds);

                if (Notification.HasNotification())
                    return;

                _billingAccountItemRepository.TransferRange(billingAccountDestination, billingAccountItems);

                if (Notification.HasNotification())
                    return;

                CreateBillingAccountItemTransferLines(billingAccountDestination, billingAccountItems);

                if (Notification.HasNotification())
                    return;

                uow.Complete();
            }
        }

        private void ValidateTransfer(BillingAccountItemTransferSourceDestinationDto dto, List<Guid> billingAccountItemsIds)
        {
            if (
                dto.BillingAccountItemIdDestination == Guid.Empty ||
                dto.BillingAccountItemTransferList == null ||
                dto.BillingAccountItemTransferList.Count() == 0)
            {
                NotifyParameterInvalid();
                return;
            }

            if (_billingAccountItemReadRepository.AnyReversalOrTaxByBillingAccountItemIdList(billingAccountItemsIds))
            {
                NotifyParameterInvalid();
                return;
            }

            var billingAccounts = _billingAccountItemReadRepository.GetAllBillingAccountIdListByBillingAccountItemIdList(billingAccountItemsIds);
            billingAccounts.Add(dto.BillingAccountItemIdDestination);

            AnyClosedOrBlockedAccount(billingAccounts);

            HasLinkedInvoice(billingAccountItemsIds);
        }

        private List<Guid> GetBillingAccountItemsIds(List<BillingAccountItemTransferChildDto> dto)
        {
            return dto.Distinct().Select(e => e.BillingAccountItemId).ToList();
        }

        private void CreateBillingAccountItemTransferLines(Guid billingAccountItemIdDestination, List<BillingAccountItem> billingAccountItems)
        {
            var transferListDto = new List<BillingAccountItemTransferDto>();

            foreach (var transferItem in billingAccountItems)
            {
                var originalAccountId = transferItem.BillingAccountIdLastSource.Value;

                transferListDto.Add(new BillingAccountItemTransferDto
                {
                    TransferDate = DateTime.UtcNow.ToZonedDateTimeLoggedUser(),
                    BillingAccountIdDestination = billingAccountItemIdDestination,
                    BillingAccountIdSource = originalAccountId,
                    BillingAccountItemId = transferItem.Id
                });
            }

            _billingAccountItemTransferAppService.CreateRange(transferListDto);
        }

        private void ValidateBillingAccountsAlreadyReversed(BillingAccountItemReversalParentDto dto)
        {
            var originalBillingAccountItemList = dto.BillingAccountItems.Select(e => e.OriginalBillingAccountItemId).ToList();

            if (_billingAccountItemReadRepository.BillingAccountItemsAlreadyReversed(originalBillingAccountItemList))
            {
                Notification.Raise(Notification.DefaultBuilder
                .WithMessage(AppConsts.LocalizationSourceName, BillingAccountItemEnum.Error.BillingAccountItemsAlreadyReversed)
                .WithDetailedMessage(AppConsts.LocalizationSourceName, BillingAccountItemEnum.Error.BillingAccountItemsAlreadyReversed)
                .Build());
            }
        }

        private void ValidateDuplicates(BillingAccountItemReversalParentDto dto)
        {
            var groupBillingItem =
                               from b in dto.BillingAccountItems
                               group b by b.OriginalBillingAccountItemId into pbp
                               select new
                               {
                                   pbp.Key,
                                   Total = pbp.Count()
                               };

            if (groupBillingItem.Any(e => e.Total > 1))
            {
                Notification.Raise(Notification.DefaultBuilder
                .WithMessage(AppConsts.LocalizationSourceName, BillingAccountItemEnum.Error.DuplicateBillingAccountEnumReversal)
                .WithDetailedMessage(AppConsts.LocalizationSourceName, BillingAccountItemEnum.Error.DuplicateBillingAccountEnumReversal)
                .Build());
            }
        }

        private void UpdateReversalBillingAccountItems(BillingAccountItemReversalDto dto, List<BillingAccountItem> originalBillingAccountItemList, List<BillingAccountItem> billingAccountItemList, int reasonId, string observation = "")
        {
            if (billingAccountItemList.Count > 0)
            {
                _billingAccountItemRepository.AddRange(billingAccountItemList);
                UpdateOriginalBillingAccountItemToReversed(originalBillingAccountItemList, reasonId, observation);
            }
        }

        private void CanNotBeATax(BillingAccountItem billingAccountItem)
        {
            if (_billingAccountItemDomainService.CheckIfBillingItemIsTax(billingAccountItem))
            {
                Notification.Raise(Notification.DefaultBuilder
                .WithMessage(AppConsts.LocalizationSourceName, BillingAccountItemEnum.Error.BillingAccountItemCanNotBeATax)
                .WithDetailedMessage(AppConsts.LocalizationSourceName, BillingAccountItemEnum.Error.BillingAccountItemCanNotBeATax)
                .Build());
            }
        }

        private void CheckIfReasonIsReversal(int reasonId)
        {
            if (!_reasonReadRepository.CheckIfReasonIsReversal(reasonId))
            {
                Notification.Raise(Notification.DefaultBuilder
                .WithMessage(AppConsts.LocalizationSourceName, BillingAccountItemEnum.Error.BillingAccountItemInvalidReasonId)
                .WithDetailedMessage(AppConsts.LocalizationSourceName, BillingAccountItemEnum.Error.BillingAccountItemInvalidReasonId)
                .Build());
            }
        }

        private void CheckIfReasonIsDiscount(int reasonId)
        {
            if (!_reasonReadRepository.CheckIfReasonIsDiscount(reasonId))
            {
                Notification.Raise(Notification.DefaultBuilder
                .WithMessage(AppConsts.LocalizationSourceName, BillingAccountItemEnum.Error.BillingAccountItemInvalidReasonId)
                .WithDetailedMessage(AppConsts.LocalizationSourceName, BillingAccountItemEnum.Error.BillingAccountItemInvalidReasonId)
                .Build());
            }
        }

        private void UpdateOriginalBillingAccountItemToReversed(List<BillingAccountItem> originalBillingAccountItemList, int reasonId, string observation = "")
        {
            foreach (var originalBillingAccountItem in originalBillingAccountItemList)
            {
                _billingAccountItemDomainService.UpdateBillingAccountItemToReversed(originalBillingAccountItem, reasonId, observation);
            }
        }

        private List<BillingAccountItem> MapReversal(BillingAccountItemReversalDto dto, List<BillingAccountItem> billingAccountItemOriginalList)
        {
            var billingAccountItemList = new List<BillingAccountItem>();
            var dateParameter = GetSystemDate(dto.PropertyId);

            foreach (var billingAccountItemOriginal in billingAccountItemOriginalList)
            {
                var builder = BillingAccountItemAdapter.MapReserval(billingAccountItemOriginal, billingAccountItemOriginal.Id, dto.ReasonId, dateParameter.Value);

                var billingAccountItem = builder.Build();

                billingAccountItemList.Add(billingAccountItem);
            }

            return billingAccountItemList;
        }

        private void MapAndCreateCredit(BillingAccountItemCreditDto dto)
        {
            dto.DateParameter = GetSystemDate(dto.PropertyId);

            CalculateCreditAmountBasedOnExchangeRate(dto);

            if (!dto.CurrencyId.HasValue)
                dto.CurrencyId = GetDefaultCurrencyIdByProperty(dto.PropertyId);

            var billingAccountItemBuilder = BillingAccountItemAdapter.MapCredit(dto);

            dto.Id = BillingAccountItemDomainService.InsertAndSaveChanges(billingAccountItemBuilder).Id;

        }

        private void MapAndCreateDebit(BillingAccountItemDebitDto dto)
        {
            dto.DateParameter = GetSystemDate(dto.PropertyId);

            CalculateDebitAmountBasedOnExchangeRate(dto);

            if (!dto.CurrencyId.HasValue)
                dto.CurrencyId = GetDefaultCurrencyIdByProperty(dto.PropertyId);

            var billingAccountItemBuilder = BillingAccountItemAdapter.MapDebit(dto);

            dto.Id = BillingAccountItemDomainService.InsertAndSaveChanges(billingAccountItemBuilder).Id;
        }

        private void MapAndCreateDebitWithInvoice(BillingAccountItemIntegrationDto dto)
        {
            dto.DateParameter = GetSystemDate(dto.PropertyId);

            if (!dto.CurrencyId.HasValue)
                dto.CurrencyId = GetDefaultCurrencyIdByProperty(dto.PropertyId);

            var billingAccountItemBuilder = BillingAccountItemAdapter.MapDebitWithInvoice(dto);

            dto.Id = BillingAccountItemDomainService.InsertAndSaveChanges(billingAccountItemBuilder).Id;
        }

        private void MapAndCreateCreditWithInvoice(BillingAccountItemIntegrationDto dto)
        {
            dto.DateParameter = GetSystemDate(dto.PropertyId);

            if (!dto.CurrencyId.HasValue)
                dto.CurrencyId = GetDefaultCurrencyIdByProperty(dto.PropertyId);

            var billingAccountItemBuilder = BillingAccountItemAdapter.MapCreditWithInvoice(dto);

            dto.Id = BillingAccountItemDomainService.InsertAndSaveChanges(billingAccountItemBuilder).Id;
        }

        private void CalculateDebitAmountBasedOnExchangeRate(BillingAccountItemDebitDto dto)
        {
            if (dto.CurrencyExchangeReferenceId.HasValue && dto.CurrencyExchangeReferenceSecId.HasValue)
            {
                var firstExchangeRate = GetExchangeRateById(dto.CurrencyExchangeReferenceId.Value).Value;
                var secondExchangeRate = GetExchangeRateById(dto.CurrencyExchangeReferenceSecId.Value).Value;

                dto.CalculateAmountByExchangeRate(firstExchangeRate, secondExchangeRate);

                if (dto.DetailList != null && dto.DetailList.Count > 0)
                    foreach (var detail in dto.DetailList)
                        detail.CalculateAmountByExchangeRate(firstExchangeRate, secondExchangeRate);
            }
        }

        private void CalculateCreditAmountBasedOnExchangeRate(BillingAccountItemCreditDto dto)
        {
            if (dto.CurrencyExchangeReferenceId.HasValue && dto.CurrencyExchangeReferenceSecId.HasValue)
            {
                var firstExchangeRate = GetExchangeRateById(dto.CurrencyExchangeReferenceId.Value).Value;
                var secondExchangeRate = GetExchangeRateById(dto.CurrencyExchangeReferenceSecId.Value).Value;

                dto.CalculateAmountByExchangeRate(firstExchangeRate, secondExchangeRate);

                if (dto.DetailList != null && dto.DetailList.Count > 0)
                    foreach (var detail in dto.DetailList)
                        detail.CalculateAmountByExchangeRate(firstExchangeRate, secondExchangeRate);
            }
        }

        private Guid GetDefaultCurrencyIdByProperty(int propertyId)
        {
            var propertyCurrency = _PropertyParameter.GetPropertyParameterForPropertyIdAndApplicationParameterId(propertyId, (int)ApplicationParameterEnum.ParameterDefaultCurrency);
            return Guid.Parse(propertyCurrency.PropertyParameterValue.Trim());
        }

        private BillingAccountItemDebitDto CreateTaxBasedOnDebitDto(BillingAccountItemDebitDto dto, BillingItemServiceAndTaxAssociateDto tax)
        {
            return new BillingAccountItemDebitDto
            {
                BillingAccountId = dto.BillingAccountId,
                BillingAccountIdLastSource = dto.BillingAccountIdLastSource,
                BillingItemId = tax.BillingItemTaxId.Value,
                Amount = _billingAccountItemDomainService.CalculateTax(dto.Amount, tax.TaxPercentage),
                PropertyId = dto.PropertyId,
                CurrencyId = dto.CurrencyId,
                CurrencyExchangeReferenceId = dto.CurrencyExchangeReferenceId,
                CurrencyExchangeReferenceSecId = dto.CurrencyExchangeReferenceSecId,
                DateParameter = dto.DateParameter
            };
        }

        private void CreateTaxBasedOnDebit(BillingAccountItemDebitDto dto)
        {
            var billingAccountItemList = new List<BillingAccountItem>();
            var ratePlanOfReservationItem = _ratePlanReadRepository.GetByBillingAccountId(dto.BillingAccountId);

            if (ratePlanOfReservationItem == null || _ratePlanDomainService.AlllowsCreatTax(ratePlanOfReservationItem))
            {
                var propertyDate = _PropertyParameter.GetSystemDate(int.Parse(_applicationUser.PropertyId));

                var taxes = _billingItemReadRepository.GetTaxesForServices(new List<int> { dto.BillingItemId }, propertyDate);

                foreach (var tax in taxes)
                {
                    var taxDto = CreateTaxBasedOnDebitDto(dto, tax);
                    var builder = BillingAccountItemAdapter.MapDebitTax(taxDto, dto.Id);
                    billingAccountItemList.Add(builder.Build());
                }
            }

            if (Notification.HasNotification())
                return;

            if (billingAccountItemList.Count > 0)
                _billingAccountItemRepository.AddRange(billingAccountItemList);
        }

        private void CreateTaxBasedOnDebitForDiscount(BillingAccountItemDebitDto dto)
        {
            var billingAccountItemList = new List<BillingAccountItem>();
            var ratePlanOfReservationItem = _ratePlanReadRepository.GetByBillingAccountId(dto.BillingAccountId);

            if (ratePlanOfReservationItem == null || _ratePlanDomainService.AlllowsCreatTax(ratePlanOfReservationItem))
            {
                var taxes = _billingItemReadRepository.GetTaxesForServices(new List<int> { dto.BillingItemId });

                foreach (var tax in taxes)
                {
                    var taxDto = CreateTaxBasedOnDebitDto(dto, tax);
                    var builder = BillingAccountItemAdapter.MapDebitTaxForDiscount(taxDto, dto.Id);
                    billingAccountItemList.Add(builder.Build());
                }
            }

            if (Notification.HasNotification())
                return;

            if (billingAccountItemList.Count > 0)
                _billingAccountItemRepository.AddRange(billingAccountItemList);
        }

        private void ValidateDebit(BillingAccountItemDebitDto dto, bool ignoreFiltersForIntegration)
        {
            if (!_billingItemReadRepository.AnyBillingItemByBillingItemType(dto.BillingItemId, (int)BillingItemTypeEnum.Service, ignoreFiltersForIntegration) &&
                !_billingItemReadRepository.AnyBillingItemByBillingItemType(dto.BillingItemId, (int)BillingItemTypeEnum.PointOfSale, ignoreFiltersForIntegration))
            {
                Notification.Raise(Notification.DefaultBuilder
                .WithMessage(AppConsts.LocalizationSourceName, BillingAccountItemEnum.Error.BillingAccountItemInvalidBillingItemId)
                .WithDetailedMessage(AppConsts.LocalizationSourceName, BillingAccountItemEnum.Error.BillingAccountItemInvalidBillingItemId)
                .Build());
            }

            if (!ignoreFiltersForIntegration && !_billingItemReadRepository.IsServiceAndExistSupportedType(dto.BillingItemId))
            {
                Notification.Raise(Notification.DefaultBuilder
                .WithMessage(AppConsts.LocalizationSourceName, BillingItemEnum.Error.BillingAccountItemWithoutBillingInvoicePropertySupportedType)
                .WithDetailedMessage(AppConsts.LocalizationSourceName, BillingItemEnum.Error.BillingAccountItemWithoutBillingInvoicePropertySupportedType)
                .Build());
            }

            ValidateNegativeAmount(dto.Amount);
            IsClosedOrBlockedAccount(dto.BillingAccountId);
        }

        private void ValidateDebit(BillingAccountItemIntegrationDto dto, bool ignoreFiltersForIntegration)
        {
            if (!_billingItemReadRepository.AnyBillingItemByBillingItemType(dto.BillingItemId, (int)BillingItemTypeEnum.Service, ignoreFiltersForIntegration) &&
                !_billingItemReadRepository.AnyBillingItemByBillingItemType(dto.BillingItemId, (int)BillingItemTypeEnum.PointOfSale, ignoreFiltersForIntegration))
            {
                Notification.Raise(Notification.DefaultBuilder
                .WithMessage(AppConsts.LocalizationSourceName, BillingAccountItemEnum.Error.BillingAccountItemInvalidBillingItemId)
                .WithDetailedMessage(AppConsts.LocalizationSourceName, BillingAccountItemEnum.Error.BillingAccountItemInvalidBillingItemId)
                .Build());
            }

            if (!ignoreFiltersForIntegration && !_billingItemReadRepository.IsServiceAndExistSupportedType(dto.BillingItemId))
            {
                Notification.Raise(Notification.DefaultBuilder
                .WithMessage(AppConsts.LocalizationSourceName, BillingItemEnum.Error.BillingAccountItemWithoutBillingInvoicePropertySupportedType)
                .WithDetailedMessage(AppConsts.LocalizationSourceName, BillingItemEnum.Error.BillingAccountItemWithoutBillingInvoicePropertySupportedType)
                .Build());
            }

            ValidateNegativeAmount(dto.Amount);
            IsClosedOrBlockedAccount(dto.BillingAccountId);
        }

        private void ValidateCredit(BillingAccountItemIntegrationDto dto)
        {
            if (!_billingItemReadRepository.AnyBillingItemByBillingItemType(dto.PaymentId.TryParseToInt32(), (int)BillingItemTypeEnum.PaymentType))
            {
                Notification.Raise(Notification.DefaultBuilder
                .WithMessage(AppConsts.LocalizationSourceName, BillingAccountItemEnum.Error.BillingAccountItemInvalidBillingItemId)
                .WithDetailedMessage(AppConsts.LocalizationSourceName, BillingAccountItemEnum.Error.BillingAccountItemInvalidBillingItemId)
                .Build());
            }

            //se for a faturar verifica se a reserva permite a faturar
            if (_billingItemReadRepository.CheckPaymentTypeByBillingItemId(dto.PaymentId.TryParseToInt32(), (int)PaymentTypeEnum.TobeBilled))
            {
                if (!_reservationConfirmationReadRepository.AnyToBeBilledByBillingAccountId(dto.BillingAccountId))
                    Notification.Raise(Notification.DefaultBuilder
                    .WithMessage(AppConsts.LocalizationSourceName, BillingAccountItemEnum.Error.ReservationInvalidPaymentType)
                    .WithDetailedMessage(AppConsts.LocalizationSourceName, BillingAccountItemEnum.Error.ReservationInvalidPaymentType)
                    .Build());
            }

            ValidateNegativeAmount(dto.Amount);
            IsClosedOrBlockedAccount(dto.BillingAccountId);
        }
        private void ValidateCredit(BillingAccountItemCreditDto dto)
        {
            if (!_billingItemReadRepository.AnyBillingItemByBillingItemType(dto.BillingItemId, (int)BillingItemTypeEnum.PaymentType))
            {
                Notification.Raise(Notification.DefaultBuilder
                .WithMessage(AppConsts.LocalizationSourceName, BillingAccountItemEnum.Error.BillingAccountItemInvalidBillingItemId)
                .WithDetailedMessage(AppConsts.LocalizationSourceName, BillingAccountItemEnum.Error.BillingAccountItemInvalidBillingItemId)
                .Build());
            }

            //se for a faturar verifica se a reserva permite a faturar
            if (_billingItemReadRepository.CheckPaymentTypeByBillingItemId(dto.BillingItemId, (int)PaymentTypeEnum.TobeBilled))
            {
                if (!_reservationConfirmationReadRepository.AnyToBeBilledByBillingAccountId(dto.BillingAccountId))
                    Notification.Raise(Notification.DefaultBuilder
                    .WithMessage(AppConsts.LocalizationSourceName, BillingAccountItemEnum.Error.ReservationInvalidPaymentType)
                    .WithDetailedMessage(AppConsts.LocalizationSourceName, BillingAccountItemEnum.Error.ReservationInvalidPaymentType)
                    .Build());
            }

            //se for credito n pode passar o maximo de parcelas do billing item
            if (dto.InstallmentsQuantity.HasValue && _billingItemReadRepository.CheckIfExceedingMaximumInstallmentsQuantity(dto.BillingItemId, dto.InstallmentsQuantity.Value))
            {
                Notification.Raise(Notification.DefaultBuilder
                .WithMessage(AppConsts.LocalizationSourceName, BillingAccountItemEnum.Error.BillingAccountItemExceedingMaximumInstallmentsQuantity)
                .WithDetailedMessage(AppConsts.LocalizationSourceName, BillingAccountItemEnum.Error.BillingAccountItemExceedingMaximumInstallmentsQuantity)
                .Build());
            }

            ValidateNegativeAmount(dto.Amount);
            IsClosedOrBlockedAccount(dto.BillingAccountId);
        }

        private void ValidateNegativeAmount(decimal amount)
        {
            if (amount <= 0)
            {
                Notification.Raise(Notification.DefaultBuilder
                .WithMessage(AppConsts.LocalizationSourceName, BillingAccountItemEnum.Error.BillingAccountItemInvalidAmount)
                .WithDetailedMessage(AppConsts.LocalizationSourceName, BillingAccountItemEnum.Error.BillingAccountItemInvalidAmount)
                .Build());
            }
        }

        private void IsClosedOrBlockedAccount(Guid billingAccountId)
        {
            if (_billingAccountReadRepository.IsClosedOrBlockedAccount(billingAccountId))
            {
                Notification.Raise(Notification.DefaultBuilder
                .WithMessage(AppConsts.LocalizationSourceName, BillingAccountEnum.Error.BillingAccountWithBillingAccountItemsCanNotBeClosedOrBlocked)
                .WithDetailedMessage(AppConsts.LocalizationSourceName, BillingAccountEnum.Error.BillingAccountWithBillingAccountItemsCanNotBeClosedOrBlocked)
                .Build());
            }
        }

        private void AnyClosedOrBlockedAccount(List<Guid> billingAccountIdList)
        {
            if (_billingAccountReadRepository.AnyClosedOrBlockedAccount(billingAccountIdList))
            {
                Notification.Raise(Notification.DefaultBuilder
                .WithMessage(AppConsts.LocalizationSourceName, BillingAccountEnum.Error.BillingAccountWithBillingAccountItemsCanNotBeClosedOrBlocked)
                .WithDetailedMessage(AppConsts.LocalizationSourceName, BillingAccountEnum.Error.BillingAccountWithBillingAccountItemsCanNotBeClosedOrBlocked)
                .Build());
            }
        }

        public List<BillingAccountItem> GetAllByBillingAccountId(Guid billingAccountId)
        {
            if (billingAccountId == Guid.Empty)
                return null;

            return _billingAccountItemReadRepository.GetAllByBillingAccountId(billingAccountId);
        }

        private void ValidateIfExistTax(IList<BillingAccountItemDiscountDto> dto)
        {
            var billingAccountList = dto.Select(x => x.BillingAccountItemId).ToList();

            if (_billingAccountItemReadRepository.AnyTaxByBillingAccountItemIdList(billingAccountList))
            {
                Notification.Raise(Notification.DefaultBuilder
                .WithMessage(AppConsts.LocalizationSourceName, BillingAccountItemEnum.Error.BillingAccountItemInvalidBillingItemId)
                .WithDetailedMessage(AppConsts.LocalizationSourceName, BillingAccountItemEnum.Error.BillingAccountItemInvalidBillingItemId)
                .Build());
            }
        }

        public void CreateBillingAccountItemDiscountList(IList<BillingAccountItemDiscountDto> dto)
        {
            using (var uow = _unitOfWorkManager.Begin())
            {
                ValidateIfExistTax(dto);

                if (Notification.HasNotification()) return;

                var billingAccountItemWithAmount = _billingAccountItemReadRepository.GetBillingAccountItemsById(dto.Select(x => x.BillingAccountItemId).ToList());

                if (dto != null && dto.Count() > 0)
                {
                    var billingAccountItems = new List<BillingAccountItem>();
                    // decimal amountOriginal = 0;
                    foreach (var discountItemDto in dto)
                    {
                        ValidateNegativeAmount(discountItemDto.AmountDiscount);

                        if (Notification.HasNotification()) break;

                        CheckPercentage(discountItemDto.Percentual);

                        if (Notification.HasNotification()) break;

                        CheckIfReasonIsDiscount(discountItemDto.ReasonId);

                        if (Notification.HasNotification()) break;

                        IsClosedOrBlockedAccount(discountItemDto.BillingAccountId);

                        if (Notification.HasNotification()) break;

                        HasLinkedInvoice(discountItemDto.BillingAccountItemId);

                        if (Notification.HasNotification()) break;

                        if (!discountItemDto.IsPercentual)
                            ValidateCreditAmount(discountItemDto.BillingAccountItemId, discountItemDto.AmountDiscount);

                        if (Notification.HasNotification()) break;

                        var billingAccountItem = billingAccountItemWithAmount.Where(exp => exp.Id == discountItemDto.BillingAccountItemId).FirstOrDefault();

                        discountItemDto.CurrencyId = billingAccountItem.CurrencyId;
                        discountItemDto.CurrencyExchangeReferenceId = billingAccountItem.CurrencyExchangeReferenceId;
                        discountItemDto.CurrencyExchangeReferenceSecId = billingAccountItem.CurrencyExchangeReferenceSecId;

                        if (discountItemDto.IsPercentual)
                        {
                            ValidateNegativeAmount(discountItemDto.Percentual);

                            if (Notification.HasNotification()) break;

                            if (discountItemDto.Percentual >= 100)
                            {
                                Notification.Raise(Notification.DefaultBuilder
                                               .WithMessage(AppConsts.LocalizationSourceName, BillingAccountItemEnum.Error.BillingAccountItemInvalidAmount)
                                               .WithDetailedMessage(AppConsts.LocalizationSourceName, BillingAccountItemEnum.Error.BillingAccountItemInvalidAmount)
                                               .Build());
                            }

                            if (Notification.HasNotification()) break;

                            discountItemDto.AmountDiscount = billingAccountItem.Amount * -1;
                            discountItemDto.AmountDiscount = _billingAccountItemDomainService.CalculateTax(discountItemDto.AmountDiscount, discountItemDto.Percentual);
                        }

                        BillingAccountItem.Builder builder;

                        if (discountItemDto.IsCredit) builder = BillingAccountItemAdapter.MapDiscountForCredit(discountItemDto);
                        else
                        {
                            discountItemDto.DateParameter = GetSystemDate(discountItemDto.PropertyId);
                            builder = BillingAccountItemAdapter.MapDiscountForDebit(discountItemDto);

                            if (Notification.HasNotification()) break;

                            CreateBillingAccountItemDiscountWithTaxList(discountItemDto);

                        }
                        if (Notification.HasNotification()) break;
                        billingAccountItems.Add(builder.Build());

                    }

                    _billingAccountItemRepository.AddRange(billingAccountItems);
                }

                uow.Complete();
            }
        }

        private void ValidateCreditAmount(Guid billingAccountItemId, decimal amountDiscount)
        {
            var billingAccountItem = _billingAccountItemReadRepository.GetBillingAccountItemsById(new List<Guid>() { billingAccountItemId }).FirstOrDefault();

            var amount = billingAccountItem.Amount < 0 ? billingAccountItem.Amount * -1 : billingAccountItem.Amount;

            if (amountDiscount > amount)
            {
                Notification.Raise(Notification.DefaultBuilder
                .WithMessage(AppConsts.LocalizationSourceName, BillingAccountItem.EntityError.BillingAccountItemInvalidDiscountAmount)
                .WithDetailedMessage(AppConsts.LocalizationSourceName, BillingAccountItem.EntityError.BillingAccountItemInvalidDiscountAmount)
                .Build());
            }
        }

        private void CheckPercentage(decimal percentage)
        {
            if (percentage > 100)
            {
                Notification.Raise(Notification.DefaultBuilder
                .WithMessage(AppConsts.LocalizationSourceName, BillingAccountItemEnum.Error.BillingAccountItemInvalidAmount)
                .WithDetailedMessage(AppConsts.LocalizationSourceName, BillingAccountItemEnum.Error.BillingAccountItemInvalidAmount)
                .Build());
            }
        }

        private void CreateBillingAccountItemDiscountWithTaxList(BillingAccountItemDiscountDto dto)
        {
            var systemDateParameter = GetSystemDate(dto.PropertyId);
            dto.DateParameter = systemDateParameter;

            var discountWithtax = new BillingAccountItemDebitDto()
            {
                Amount = dto.AmountDiscount,
                BillingItemId = dto.BillingItemId,
                BillingAccountId = dto.BillingAccountId,
                PropertyId = dto.PropertyId,
                Id = dto.BillingAccountItemId,
                CurrencyId = dto.CurrencyId,
                CurrencyExchangeReferenceId = dto.CurrencyExchangeReferenceId,
                CurrencyExchangeReferenceSecId = dto.CurrencyExchangeReferenceSecId,
                DateParameter = systemDateParameter
            };
            CreateTaxBasedOnDebitForDiscount(discountWithtax);
        }

        public BillingAccountItemPartialDto CreatePartial(BillingAccountItemPartialDto dto)
        {
            using (var uow = _unitOfWorkManager.Begin())
            {
                if (dto.Id == Guid.Empty)
                    NotifyIdIsMissing();             

                IsClosedOrBlockedAccount(dto.BillingAccountId);

                HasLinkedInvoice(dto.BillingAccountItemId);

                BillingAccountItemCanNotBeChildren(new List<Guid> { dto.BillingAccountItemId });

                if (Notification.HasNotification())
                    return null;

                var originalBillingAccountItem = _billingAccountItemReadRepository.GetByIdWithBillingItem(dto.BillingAccountItemId);

                if (originalBillingAccountItem == null)
                {
                    NotifyWhenEntityNotExist("BillingAccount");
                    return null;
                }

                if (originalBillingAccountItem.BillingItem.BillingItemTypeId == (int)BillingItemTypeEnum.PointOfSale)
                {
                    Notification.Raise(Notification.DefaultBuilder
                        .WithMessage(AppConsts.LocalizationSourceName, BillingAccountItem.EntityError.BillingItemPOSCanNotBePartial)
                        .WithDetailedMessage(AppConsts.LocalizationSourceName, BillingAccountItem.EntityError.BillingItemPOSCanNotBePartial)
                        .Build());

                    return null;
                }

                VerifyBillingAccountItemTypePartial(originalBillingAccountItem);

                if (Notification.HasNotification())
                    return null;

                if (dto.Value.HasValue)
                {
                    ValidateNegativeAmount(dto.Value.Value);

                    if (dto.Value >= (originalBillingAccountItem.Amount > 0 ? originalBillingAccountItem.Amount : (originalBillingAccountItem.Amount * -1)))
                        NotifyParameterInvalid();
                }

                if (Notification.HasNotification())
                    return null;

                var childrenBillingAccountItem = _billingAccountItemReadRepository.GetAllChildrenByBillingAccountParentId(dto.BillingAccountItemId);
                var listToInclude = new List<BillingAccountItem>();

                if (dto.Parts.HasValue)
                {
                    ProccessPartialWithParts(dto, originalBillingAccountItem, childrenBillingAccountItem, listToInclude);
                }
                else if (dto.Value.HasValue)
                {
                    ProccessPartialWithValue(dto, originalBillingAccountItem, childrenBillingAccountItem, listToInclude);
                }

                uow.Complete();
            }

            return dto;
        }    

        private void VerifyBillingAccountItemTypePartial(BillingAccountItem originalBillingAccountItem)
        {
            if (originalBillingAccountItem.BillingAccountItemTypeId == (int)BillingAccountItemTypeEnum.BillingAccountItemTypePartial || originalBillingAccountItem.PartialParentBillingAccountItemId != null)
                Notification.Raise(Notification.DefaultBuilder
                .WithMessage(AppConsts.LocalizationSourceName, BillingAccountItem.EntityError.BillingAccountItemDoesNotCreatePartial)
                .WithDetailedMessage(AppConsts.LocalizationSourceName, BillingAccountItem.EntityError.BillingAccountItemDoesNotCreatePartial)
                .Build());
        }

        private void ProccessPartialWithValue(BillingAccountItemPartialDto dto, BillingAccountItem originalBillingAccountItem, List<BillingAccountItem> childrenBillingAccountItem, List<BillingAccountItem> listToInclude)
        {
            var dtoValue = originalBillingAccountItem.Amount > 0 ? dto.Value.Value : dto.Value.Value * -1;
            var proportionalChildPercentage = dto.Value.Value * 100 / (originalBillingAccountItem.Amount > 0 ? originalBillingAccountItem.Amount : originalBillingAccountItem.Amount * -1);

            //SETAR O DIVIDIDO
            var billingAccountItemPart = BillingAccountItemAdapter.MapPartial(originalBillingAccountItem, dtoValue, originalBillingAccountItem.Id).Build();

            listToInclude.Add(billingAccountItemPart);

            //espelho de taxas e filhos para cada PARTE de forma proporcional
            if (childrenBillingAccountItem.Any())
                foreach (var childBillingAccountItem in childrenBillingAccountItem)
                {
                    var proportionalValueChild = childBillingAccountItem.Amount * proportionalChildPercentage / 100;

                    var billingAccountItemPartChild = BillingAccountItemAdapter.MapPartialChild(childBillingAccountItem, billingAccountItemPart.Id, proportionalValueChild, originalBillingAccountItem.Id).Build();

                    listToInclude.Add(billingAccountItemPartChild);
                }

            _billingAccountItemRepository.AddRange(listToInclude);

            //SETAR O ORIGINAL
            originalBillingAccountItem.SetToPartial(originalBillingAccountItem.Amount - dtoValue, originalBillingAccountItem.Id);

            //valores filhos na proporção inversa do valor obtido
            if (childrenBillingAccountItem.Any())
                foreach (var childBillingAccountItem in childrenBillingAccountItem)
                {
                    var proportionalValueChild =
                            childBillingAccountItem.Amount * (100 - proportionalChildPercentage) / 100;

                    childBillingAccountItem.SetToPartial(proportionalValueChild, originalBillingAccountItem.Id);
                }
        }

        private void ProccessPartialWithParts(BillingAccountItemPartialDto dto, BillingAccountItem originalBillingAccountItem, List<BillingAccountItem> childrenBillingAccountItem, List<BillingAccountItem> listToInclude)
        {
            var proportionalValue = (originalBillingAccountItem.Amount / dto.Parts.Value).TruncateTwoDecimalPlaces();

            //SETAR OS DIVIDIDOS (fazer os OUTROS SETS)
            for (int i = 1; i < dto.Parts.Value; i++)
            {
                var billingAccountItemPart = BillingAccountItemAdapter.MapPartial(originalBillingAccountItem, proportionalValue, originalBillingAccountItem.Id).Build();

                listToInclude.Add(billingAccountItemPart);

                //espelho de taxas e filhos para cada PARTE de forma proporcional
                if (childrenBillingAccountItem.Any())
                    foreach (var childBillingAccountItem in childrenBillingAccountItem)
                    {
                        var proportionalValueChild = (childBillingAccountItem.Amount / dto.Parts.Value).TruncateTwoDecimalPlaces();

                        var billingAccountItemPartChild = BillingAccountItemAdapter.MapPartialChild(childBillingAccountItem, billingAccountItemPart.Id, proportionalValueChild, originalBillingAccountItem.Id).Build();

                        listToInclude.Add(billingAccountItemPartChild);
                    }
            }
            _billingAccountItemRepository.AddRange(listToInclude);



            //SETAR O ORIGINAL
            var originalProportionalValue = ((originalBillingAccountItem.Amount - proportionalValue * dto.Parts.Value) + proportionalValue).TruncateTwoDecimalPlaces();

            originalBillingAccountItem.SetToPartial(originalProportionalValue, originalBillingAccountItem.Id);

            //setar o valor proporcional certo para cada filho
            if (childrenBillingAccountItem.Any())
                foreach (var childBillingAccountItem in childrenBillingAccountItem)
                {
                    var proportionalValueChild = (childBillingAccountItem.Amount / dto.Parts.Value).TruncateTwoDecimalPlaces();

                    var originalProportionalValueChild = ((childBillingAccountItem.Amount - proportionalValueChild * dto.Parts.Value) + proportionalValueChild).TruncateTwoDecimalPlaces();

                    childBillingAccountItem.SetToPartial(originalProportionalValueChild, originalBillingAccountItem.Id);
                }
        }

        public BillingAccountItemUndoPartialDto UndoPartial(BillingAccountItemUndoPartialDto dto)
        {
            using (var uow = _unitOfWorkManager.Begin())
            {
                if (dto.PropertyId <= 0 || dto.BillingAccountItemId.Count() <= 1)
                    NotifyParameterInvalid();

                var billingAccountItemIdList = dto.BillingAccountItemId.ToList();

                HasLinkedInvoice(billingAccountItemIdList);

                BillingAccountItemCanNotBeChildren(billingAccountItemIdList);

                if (Notification.HasNotification())
                    return null;

                var billingAccountItemList = _billingAccountItemReadRepository.GetAllWithChildren(billingAccountItemIdList);

                var partialParentBillingAccountItemId = billingAccountItemList.FirstOrDefault().PartialParentBillingAccountItemId;

                var partialParentBillingAccountItemList = new List<BillingAccountItem>();

                if (partialParentBillingAccountItemId.HasValue)
                    partialParentBillingAccountItemList = _billingAccountItemReadRepository.GetAllByPartialParentBillingAccountItemId(partialParentBillingAccountItemId.Value);

                _billingAccountItemDomainService.ValidateUndoPartial(billingAccountItemList, partialParentBillingAccountItemList);

                if (Notification.HasNotification()) return null;

                var originalItems = billingAccountItemList.Where(e => e.OriginalAmount != e.Amount /*&& e.IsOriginal.HasValue && e.IsOriginal.Value*/).ToList();
                var originalItemsIdList = new List<Guid>();

                //não existe o lançamento original - obtém os primeiros para serem considerados como originais temporariamente
                if (originalItems.Count() == 0)
                    originalItems = billingAccountItemList.Where(e => e.Id == billingAccountItemIdList.FirstOrDefault() || e.BillingAccountItemParentId == billingAccountItemIdList.FirstOrDefault()).ToList();

                originalItemsIdList = originalItems.Select(e => e.Id).ToList();
                billingAccountItemList = billingAccountItemList.Where(e => !originalItemsIdList.Contains(e.Id)).ToList();                

                //verificar se eles são do mesmo pai que foi dividido
                _billingAccountItemDomainService.UndoPartial(
                    originalItems
                        .OrderByDescending(e => e.BillingAccountItemParent != null)
                        .ThenByDescending(e => e.BillingAccountItemParent)
                        .ToList(), billingAccountItemList);

                uow.Complete();
            }

            return dto;
        }

        private void BillingAccountItemCanNotBeChildren(List<Guid> billingAccountItemIdList)
        {
            //somente registros que são pais devem ser enviados no DTO
            if (_billingAccountItemReadRepository.AnyChildrenItem(billingAccountItemIdList))
            {
                //notificação
                Notification.Raise(Notification.DefaultBuilder
                .WithMessage(AppConsts.LocalizationSourceName, BillingAccountItemEnum.Error.BillingAccountItemsCanNotBeChildren)
                .WithDetailedMessage(AppConsts.LocalizationSourceName, BillingAccountItemEnum.Error.BillingAccountItemsCanNotBeChildren)
                .Build());
            }
        }        

        private void HasLinkedInvoice(Guid billingAccountId)
        {
            if (_billingAccountItemReadRepository.HasLinkedInvoice(billingAccountId))
            {
                Notification.Raise(Notification.DefaultBuilder
                .WithMessage(AppConsts.LocalizationSourceName, BillingAccountItemEnum.Error.BillingAccountItemsCantHasAnInvoiceLinked)
                .WithDetailedMessage(AppConsts.LocalizationSourceName, BillingAccountItemEnum.Error.BillingAccountItemsCantHasAnInvoiceLinked)
                .Build());
            }
        }

        private void HasLinkedInvoice(List<Guid> billingAccountIdList)
        {
            if (_billingAccountItemReadRepository.HasLinkedInvoice(billingAccountIdList))
            {
                Notification.Raise(Notification.DefaultBuilder
                .WithMessage(AppConsts.LocalizationSourceName, BillingAccountItemEnum.Error.BillingAccountItemCantHasAnInvoiceLinked)
                .WithDetailedMessage(AppConsts.LocalizationSourceName, BillingAccountItemEnum.Error.BillingAccountItemCantHasAnInvoiceLinked)
                .Build());
            }
        }

        public virtual BillingAccountItemDebitDto CreateDebitIntegration(BillingAccountItemDebitDto dto)
        {
            ValidateDto<BillingAccountItemDebitDto>(dto, nameof(dto));

            if (Notification.HasNotification()) return null;

            if (dto.TenantId == Guid.Empty || !_propertyReadRepository.IsValidTenantIdByPropertyId(dto.PropertyId, dto.TenantId))
                NotifyParameterInvalid();

            if (Notification.HasNotification()) return null;

            if (!_billingItemReadRepository.AnyBillingItemByBillingItemType(dto.BillingItemId, (int)BillingItemTypeEnum.PointOfSale, true))
            {
                Notification.Raise(Notification.DefaultBuilder
                .WithMessage(AppConsts.LocalizationSourceName, BillingAccountItemEnum.Error.BillingAccountItemInvalidBillingItemId)
                .WithDetailedMessage(AppConsts.LocalizationSourceName, BillingAccountItemEnum.Error.BillingAccountItemInvalidBillingItemId)
                .Build());
                return null;
            }

            return CreateDebit(dto, true);
        }

        public virtual BillingAccountItemIntegrationDto CreateDebitAndCreditWithInvoice(BillingAccountItemIntegrationDto dto, bool ignoreFiltersForIntegration = false)
        {
            using (var uow = _unitOfWorkManager.Begin())
            {
                ValidateDto<BillingAccountItemDebitDto>(dto, nameof(dto));

                if (Notification.HasNotification()) return null;

                ValidateDebit(dto, ignoreFiltersForIntegration);

                if (Notification.HasNotification()) return null;

                var invoice = CreateBillingInvoice(dto, dto.BillingInvoiceTypeId.TryParseToInt32());

                if (invoice != null && invoice.Id != null && invoice.Id != Guid.Empty)
                    dto.BillingInvoiceId = invoice.Id;

                if (Notification.HasNotification()) return null;

                MapAndCreateDebitWithInvoice(dto);

                if (Notification.HasNotification()) return null;

                dto.ParentId = dto.Id;

                ValidateCredit(dto);

                if (Notification.HasNotification()) return null;

                MapAndCreateCreditWithInvoice(dto);

                if (Notification.HasNotification()) return null;

                uow.Complete();
            }

            return dto;
        }

        public virtual BillingAccountItemIntegrationDto CreateDebitAndCreditWithInvoice(BillingAccountItemIntegrationDto dto)
        {
            ValidateDto<BillingAccountItemIntegrationDto>(dto, nameof(dto));

            if (Notification.HasNotification()) return null;

            if (dto.TenantId == Guid.Empty || dto.BillingInvoiceTypeId == null || !_propertyReadRepository.IsValidTenantIdByPropertyId(dto.PropertyId, dto.TenantId))
                NotifyParameterInvalid();

            if (Notification.HasNotification()) return null;

            if (!_billingItemReadRepository.AnyBillingItemByBillingItemType(dto.BillingItemId, (int)BillingItemTypeEnum.PointOfSale, true))
            {
                Notification.Raise(Notification.DefaultBuilder
                .WithMessage(AppConsts.LocalizationSourceName, BillingAccountItemEnum.Error.BillingAccountItemInvalidBillingItemId)
                .WithDetailedMessage(AppConsts.LocalizationSourceName, BillingAccountItemEnum.Error.BillingAccountItemInvalidBillingItemId)
                .Build());
                return null;
            }

            return CreateDebitAndCreditWithInvoice(dto, true);
        }

        private BillingInvoiceDto CreateBillingInvoice(BillingAccountItemIntegrationDto dto, int invoiceType)
        {
            BillingInvoiceDto result = null;

            switch (EnumHelper.GetEnumValue<CountryIsoCodeEnum>(_propertyReadRepository.GetPropertyCountryTwoLetterIsoCode(dto.PropertyId), true))
            {
                case CountryIsoCodeEnum.Brasil:
                case CountryIsoCodeEnum.Europe:
                    {
                        var supportedTypeList = _billingInvoicePropertySupportedTypeReadRepository.GetByType(invoiceType, dto.PropertyId);

                        if (supportedTypeList.IsNullOrEmpty())
                        {
                            Notification.Raise(Notification.DefaultBuilder
                                  .WithMessage(AppConsts.LocalizationSourceName, BillingItemEnum.Error.BillingAccountItemWithoutBillingInvoicePropertySupportedType)
                                  .WithDetailedMessage(AppConsts.LocalizationSourceName, BillingItemEnum.Error.BillingAccountItemWithoutBillingInvoicePropertySupportedType)
                                  .Build());

                            return BillingInvoiceDto.NullInstance;
                        }

                        var billingInvoice = CreateBillingInvoiceAndUpdateLastNumber(dto, supportedTypeList.Select(s => s.BillingInvoiceProperty).FirstOrDefault(),
                                                                                    (BillingInvoiceTypeEnum)invoiceType);
                        return billingInvoice.MapTo<BillingInvoiceDto>();
                    }
                case CountryIsoCodeEnum.Latam:
                default:
                    {
                        Notification.Raise(Notification.DefaultBuilder
                                                       .WithMessage(AppConsts.LocalizationSourceName, BillingInvoice.EntityError.BillingInvoiceOutOfBoundIntegratorId)
                                                       .WithDetailedMessage(string.Concat(_applicationUser.PropertyCountryCode, AppConsts.LocalizationSourceName, BillingInvoice.EntityError.BillingInvoiceOutOfBoundIntegratorId))
                                                       .Build());

                        break;
                    }
            }

            return result;
        }

        private BillingInvoiceDto CreateBillingInvoiceAndUpdateLastNumber(BillingAccountItemIntegrationDto integrationDto, BillingInvoiceProperty billingInvoiceProperty, BillingInvoiceTypeEnum invoiceType)
        {
            billingInvoiceProperty = _billingInvoicePropertyRepository.Get(new DefaultGuidRequestDto(billingInvoiceProperty.Id));

            try
            {
                _billingInvoicePropertyRepository.IncrementLastNumberAndUpdate(billingInvoiceProperty);

                var billingInvoiceDto = new BillingInvoiceDto
                {
                    Id = Guid.NewGuid(),
                    BillingInvoiceNumber = billingInvoiceProperty.LastNumber,
                    BillingInvoiceSeries = billingInvoiceProperty.BillingInvoicePropertySeries,
                    PropertyId = integrationDto.PropertyId,
                    BillingAccountId = integrationDto.BillingAccountId,
                    BillingInvoicePropertyId = billingInvoiceProperty.Id,
                    BillingInvoiceStatusId = (int)BillingInvoiceStatusEnum.Issued,
                    EmissionDate = DateTime.UtcNow.ToZonedDateTimeLoggedUser(),
                    BillingInvoiceTypeId = (int)invoiceType,
                    ExternalNumber = integrationDto.InvoiceNumber,
                    ExternalSeries = integrationDto.InvoiceSeries,
                    ExternalEmissionDate = integrationDto.InvoiceEmissionDate,
                    IntegratorId = integrationDto.IntegratorId
                };

                var billingInvoiceBuilder = _billingInvoiceAdapter.Map(billingInvoiceDto).Build();

                if (Notification.HasNotification()) return BillingInvoiceDto.NullInstance;

                _billingInvoiceRepository.Create(billingInvoiceBuilder);

                if (Notification.HasNotification()) return BillingInvoiceDto.NullInstance;

                return billingInvoiceDto;
            }
            catch (DbUpdateException ex)
            {
                if (ex.InnerException.Message.Contains("UK_BillingInvoice"))
                {
                    return CreateBillingInvoiceAndUpdateLastNumber(integrationDto, billingInvoiceProperty, invoiceType);
                }
                else
                    throw;
            }
        }

        public List<BillingAccountItem> GetAllForSetInvoiceIdByBillingAccountId(Guid billingAccountId)
        {
            if (billingAccountId == Guid.Empty)
                return null;

            return _billingAccountItemReadRepository.GetAllForSetInvoiceIdByBillingAccountId(billingAccountId);
        }

        private DateTime? GetSystemDate(int propertyId)
        {
            return _PropertyParameter.GetSystemDate(propertyId);
        }

        public BillingAccountItemReversalParentDto CreateReversalIntegration(BillingAccountItemReversalParentDto dto)
        {
            if (!dto.TenantId.HasValue || dto.TenantId.HasValue && dto.TenantId == Guid.Empty || dto.BillingAccountItems == null || dto.BillingAccountItems.Count == 0)
                NotifyIdIsMissing();

            var result = CreateReversal(dto);
            return result;
        }

        public IList<BillingAccountItemCreditAmountDto> GetAllBillingAccountItemCreditAmountByInvoiceId(Guid billingInvoiceId)
        {
            return _billingAccountItemReadRepository.GetBillingAccountItemCreditNoteLiByInvoiceId(billingInvoiceId);
        }

        public virtual void CreateAllDailyDebit(List<BillingAccountItemDebitDto> debitDtoList)
        {
            if (debitDtoList == null || debitDtoList.Count == 0)
                return;

            using (var uow = _unitOfWorkManager.Begin())
            {
                var billingAccountItemList = new List<BillingAccountItem>();

                foreach (var debitDto in debitDtoList)
                {
                    MapDailyDebit(debitDto, billingAccountItemList);

                    if (Notification.HasNotification()) return;

                    MapTaxBasedOnDailyDebit(debitDto, billingAccountItemList);

                    if (Notification.HasNotification()) return;
                }

                _billingAccountItemRepository.CreateBulk(billingAccountItemList);

                uow.Complete();
            }
        }

        public List<BillingAccountItem> CreateAllTaxTourismDebit(List<BillingAccountItemTourismTaxDto> debitTourismTaxDtoList)
        {
            if (debitTourismTaxDtoList == null || (debitTourismTaxDtoList != null && !debitTourismTaxDtoList.Any()))
                return null;

            using (var uow = _unitOfWorkManager.Begin())
            {
                var billingAccountItemList = new List<BillingAccountItem>();

                var taxeList = _billingItemReadRepository.GetTaxesForServices(debitTourismTaxDtoList.Select(b => b.BillingItemId).ToList());
                var ratePlanList = _ratePlanReadRepository.GetAllByBillingAccountIdList(debitTourismTaxDtoList.Select(b => b.BillingAccountId).ToList());

                foreach (var debitDto in debitTourismTaxDtoList)
                {
                    MapTourismTaxDebit(debitDto, billingAccountItemList);

                    if (Notification.HasNotification()) return null;

                    var billintItemTaxeList = taxeList.Where(t => t.BillingItemServiceId == debitDto.BillingItemId).ToList();
                    var ratePlan = ratePlanList.Where(t => t.BillingAccountId == debitDto.BillingAccountId).FirstOrDefault();

                    MapTaxBasedOnTourismTaxDebit(debitDto, billingAccountItemList, billintItemTaxeList, ratePlan);

                    if (Notification.HasNotification()) return null;
                }

                _billingAccountItemRepository.CreateBulk(billingAccountItemList);

                uow.Complete();

                return billingAccountItemList;
            }
        }

        private void MapDailyDebit(BillingAccountItemDebitDto dto, List<BillingAccountItem> billingAccountItemList)
        {
            CalculateDebitAmountBasedOnExchangeRate(dto);

            if (!dto.CurrencyId.HasValue)
                dto.CurrencyId = GetDefaultCurrencyIdByProperty(dto.PropertyId);

            var builder = BillingAccountItemAdapter
                    .MapDailyDebit(dto)
                    .SetTenant(_applicationUser);

            var billingAccountItem = builder.Build();

            if (Notification.HasNotification()) return;

            dto.Id = billingAccountItem.Id;

            billingAccountItemList.Add(billingAccountItem);
        }

        private void MapTaxBasedOnDailyDebit(BillingAccountItemDebitDto dto, List<BillingAccountItem> billingAccountItemList)
        {
            var ratePlanOfReservationItem = _ratePlanReadRepository.GetByBillingAccountId(dto.BillingAccountId);

            if (ratePlanOfReservationItem == null || _ratePlanDomainService.AlllowsCreatTax(ratePlanOfReservationItem))
            {
                var taxes = _billingItemReadRepository.GetTaxesForServices(new List<int> { dto.BillingItemId });

                foreach (var tax in taxes)
                {
                    var taxDto = CreateTaxBasedOnDebitDto(dto, tax);
                    var builder = BillingAccountItemAdapter
                        .MapDebitTax(taxDto, dto.Id)
                        .SetTenant(_applicationUser);

                    var billingAccountItem = builder.Build();

                    if (Notification.HasNotification()) return;

                    billingAccountItemList.Add(billingAccountItem);
                }
            }
        }

        private void MapTourismTaxDebit(BillingAccountItemTourismTaxDto dto, List<BillingAccountItem> billingAccountItemList)
        {
            CalculateDebitAmountBasedOnExchangeRate(dto);

            if (!dto.CurrencyId.HasValue)
                dto.CurrencyId = GetDefaultCurrencyIdByProperty(dto.PropertyId);

            var builder = BillingAccountItemAdapter
                    .MapTaxTourismDebit(dto)
                    .SetTenant(_applicationUser);

            var billingAccountItem = builder.Build();

            if (Notification.HasNotification()) return;

            dto.Id = billingAccountItem.Id;

            billingAccountItemList.Add(billingAccountItem);
        }

        private void MapTaxBasedOnTourismTaxDebit(BillingAccountItemTourismTaxDto dto, List<BillingAccountItem> billingAccountItemList, List<BillingItemServiceAndTaxAssociateDto> taxeList, RatePlanBillingAccountDto ratePlanDto)
        {
            if (ratePlanDto == null || _ratePlanDomainService.AlllowsCreatTax(ratePlanDto))
            {
                foreach (var tax in taxeList)
                {
                    var taxDto = CreateTaxBasedOnDebitDto(dto, tax);
                    var builder = BillingAccountItemAdapter
                        .MapTaxTourismDebitTax(taxDto, dto.Id, dto.BillingAccountItemDate)
                        .SetTenant(_applicationUser);

                    var billingAccountItem = builder.Build();

                    if (Notification.HasNotification()) return;

                    billingAccountItemList.Add(billingAccountItem);
                }
            }
        }

    }
}


