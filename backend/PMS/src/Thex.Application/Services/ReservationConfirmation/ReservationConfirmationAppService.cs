﻿using System;
using Thex.Application.Adapters;
using Thex.Domain.Entities;
using Thex.Application.Interfaces;
using Tnf.Domain.Services;

using Thex.Dto;
using Tnf.Notifications;
using Thex.Kernel;
using Thex.Common.Helpers;
using Thex.Common.Enumerations;

namespace Thex.Application.Services
{
    public class ReservationConfirmationAppService : ScaffoldReservationConfirmationAppService, IReservationConfirmationAppService
    {
        private readonly IApplicationUser _applicationUser;

        public ReservationConfirmationAppService(
            IReservationConfirmationAdapter reservationConfirmationAdapter,
            IDomainService<ReservationConfirmation> reservationConfirmationDomainService,
            INotificationHandler notificationHandler,
            IApplicationUser applicationUser)
            : base(reservationConfirmationAdapter, reservationConfirmationDomainService, notificationHandler)
        {
            _applicationUser = applicationUser;
        }
	
        public ReservationConfirmation.Builder GetBuilder(ReservationConfirmationDto reservationConfirmationDto, ReservationDto reservationDto, DateTime firstDateToValidateDeadline, long? plasticId)
        {
            var builder = new ReservationConfirmation.Builder(Notification, firstDateToValidateDeadline);

            SetLaunchDailyInDto(reservationConfirmationDto);

            builder
                .WithId(reservationConfirmationDto.Id)
                .WithPaymentTypeId(reservationConfirmationDto.PaymentTypeId)
                .WithBillingClientId(reservationConfirmationDto.BillingClientId, reservationDto.CompanyClientId, reservationConfirmationDto.PaymentTypeId)
                .WithEnsuresNoShow(reservationConfirmationDto.EnsuresNoShow)
                .WithValue(reservationConfirmationDto.Value)
                .WithDeadline(reservationConfirmationDto.Deadline)
                .WithReservationId(reservationConfirmationDto.ReservationId)
                .WithReservationIsConfirmed(reservationConfirmationDto.IsConfirmed)
                .WithLaunchDaily(reservationConfirmationDto.LaunchDaily);

            if (plasticId.HasValue)
                builder.WithPlasticId(plasticId);

            return builder;
        }

        public ReservationConfirmation.Builder GetBuilder(ReservationConfirmationDto reservationConfirmationDto, ReservationDto reservationDto, DateTime firstDateToValidateDeadline)
        {
            var builder = new ReservationConfirmation.Builder(Notification, firstDateToValidateDeadline);

            SetLaunchDailyInDto(reservationConfirmationDto);

            builder
                .WithId(reservationConfirmationDto.Id)
                .WithPaymentTypeId(reservationConfirmationDto.PaymentTypeId)
                .WithBillingClientId(reservationConfirmationDto.BillingClientId, reservationDto.CompanyClientId, reservationConfirmationDto.PaymentTypeId)
                .WithEnsuresNoShow(reservationConfirmationDto.EnsuresNoShow)
                .WithValue(reservationConfirmationDto.Value)
                .WithDeadline(reservationConfirmationDto.Deadline)
                .WithReservationId(reservationConfirmationDto.ReservationId)
                .WithReservationIsConfirmed(reservationConfirmationDto.IsConfirmed)
                .WithLaunchDaily(reservationConfirmationDto.LaunchDaily);

            return builder;
        }

        private void SetLaunchDailyInDto(ReservationConfirmationDto reservationConfirmationDto)
        {
            if (EnumHelper.GetEnumValue<CountryIsoCodeEnum>(_applicationUser.PropertyCountryCode, true) == CountryIsoCodeEnum.Europe)
            {
                if (!reservationConfirmationDto.IsConfirmed.HasValue
                    ||
                    (reservationConfirmationDto.IsConfirmed.HasValue && !reservationConfirmationDto.IsConfirmed.Value)
                    )
                    reservationConfirmationDto.LaunchDaily = null;
            }
            else
                reservationConfirmationDto.LaunchDaily = null;
        }

        public void UpdateReservationEnsuresNoShow(ReservationConfirmation reservationConfirmation, bool ensuresNoShow)
        {
            var reservationConfirmationBuilder = new ReservationConfirmation.Builder(Notification, reservationConfirmation);
            reservationConfirmationBuilder
                        .WithEnsuresNoShow(ensuresNoShow);
            ReservationConfirmationDomainService.Update(reservationConfirmationBuilder);
        }
    }
}
