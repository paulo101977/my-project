﻿using Thex.Domain.Entities;
using Thex.Application.Interfaces;


namespace Thex.Application.Services
{
    using System;
    using Thex.Domain.Interfaces.Repositories;
    using Thex.Dto;
    using Tnf;
    using Tnf.Notifications;

    public class ChainBusinessSourceAppService : ApplicationServiceBase, IChainBusinessSourceAppService
    {
        protected readonly IChainBusinessSourceRepository _chainBusinessSourceRepository;

        public ChainBusinessSourceAppService(IChainBusinessSourceRepository chainBusinessSourceRepository,
            INotificationHandler notificationHandler)
               : base(notificationHandler)
        {
            _chainBusinessSourceRepository = Check.NotNull(chainBusinessSourceRepository, nameof(chainBusinessSourceRepository));
        }

        public void Create(BusinessSourceDto businessSourceDto, int chainId)
        {
            _chainBusinessSourceRepository.Create(ToDomain(businessSourceDto, chainId));
        }

        public ChainBusinessSource ToDomain(BusinessSourceDto businessSource, int chainId)
        {
            var chainBusinessSourceBuilder = this.GetBuilder(businessSource, chainId);
            return chainBusinessSourceBuilder.Build();

        }

        public ChainBusinessSource.Builder GetBuilder(BusinessSourceDto businessSourceDto, int chainId)
        {
            var builder = new ChainBusinessSource.Builder(Notification);

            builder
                .WithId(Guid.NewGuid())
                .WithChainId(chainId)
                .WithBusinessSourceId(businessSourceDto.Id)
                .WithIsActive(businessSourceDto.IsActive);

            return builder;
        }
    }
}
