﻿
using System;
using Tnf;
using Tnf.Dto;
using Tnf.Domain.Services;
using Tnf.Application.Services;
using Tnf.Notifications;
using System.Threading.Tasks;
using Thex.Dto;
using Thex.Domain.Entities;
using Thex.Application.Adapters;
using Thex.Application.Interfaces;
using Thex.Domain;

namespace Thex.Application.Services
{
    public abstract class ScaffoldLevelAppService : ApplicationServiceBase, IScaffoldLevelAppService
    {
        protected readonly ILevelAdapter LevelAdapter;
        protected readonly IDomainService<Level> LevelDomainService;

        public ScaffoldLevelAppService(
            ILevelAdapter levelAdapter,
            IDomainService<Level> levelDomainService,
            INotificationHandler notificationHandler)
               : base(notificationHandler)
        {
            LevelAdapter = Check.NotNull(levelAdapter, nameof(levelAdapter));
            LevelDomainService = Check.NotNull(levelDomainService, nameof(levelDomainService));

            levelDomainService.EntityName = EntityNames.Level;
        }

        public virtual async Task<LevelDto> Create(LevelDto dto)
        {
            ValidateDto<LevelDto>(dto, nameof(dto));

            if (Notification.HasNotification())
                return LevelDto.NullInstance;

            var levelBuilder = LevelAdapter.Map(dto);

            dto.Id = (await LevelDomainService.InsertAndSaveChangesAsync(levelBuilder)).Id;
            return dto;
        }

        public virtual async Task Delete(Guid id)
        {
            if (!ValidateId(id)) return;;

            if (!Notification.HasNotification())
                await LevelDomainService.DeleteAsync(w => w.Id == id);
        }

        public virtual async Task<LevelDto> Get(DefaultIntRequestDto id)
        {
            if (!ValidateRequestDto(id) || !ValidateId<int>(id.Id)) return null;

            if (Notification.HasNotification())
                return LevelDto.NullInstance;

            var entity = await LevelDomainService.GetAsync(id).ConfigureAwait(false);
            return entity.MapTo<LevelDto>();
        }
        
        public virtual async Task<LevelDto> Update(Guid id, LevelDto dto)
        {
            ValidateDtoAndId(dto, id, nameof(dto), nameof(id));

            if (Notification.HasNotification())
                return LevelDto.NullInstance;

            dto.Id = id;

            var levelBuilder = LevelAdapter.Map(dto);

            await LevelDomainService.UpdateAsync(levelBuilder);
            return dto;
        }
    }
}
