﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a Crudzilla tool.
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using Tnf;

using Tnf.Dto;
using Tnf.Dto;
using Tnf.Domain.Services;
using Tnf.Application.Services;
using System.Threading.Tasks;
using Thex.Dto;
using Thex.Domain.Entities;
using Thex.Application.Adapters;
using Thex.Application.Interfaces;
using Thex.Domain;
using Tnf.Notifications;

namespace Thex.Application.Services
{
    public abstract class ScaffoldPersonInformationTypeAppService : ApplicationServiceBase, IScaffoldPersonInformationTypeAppService
    {
        protected readonly IPersonInformationTypeAdapter PersonInformationTypeAdapter;
        protected readonly IDomainService<PersonInformationType> PersonInformationTypeDomainService;

        public ScaffoldPersonInformationTypeAppService(
            IPersonInformationTypeAdapter personInformationTypeAdapter,
            IDomainService<PersonInformationType> personInformationTypeDomainService,
            INotificationHandler notificationHandler)
               : base(notificationHandler)
        {
            PersonInformationTypeAdapter = Check.NotNull(personInformationTypeAdapter, nameof(personInformationTypeAdapter));
            PersonInformationTypeDomainService = Check.NotNull(personInformationTypeDomainService, nameof(personInformationTypeDomainService));

            PersonInformationTypeDomainService.EntityName = EntityNames.PersonInformationType;
        }

        public virtual async Task<PersonInformationTypeDto> Create(PersonInformationTypeDto dto)
        {
            ValidateDto<PersonInformationTypeDto>(dto, nameof(dto));

            if (Notification.HasNotification())
                return PersonInformationTypeDto.NullInstance;

            var personInformationTypeBuilder = PersonInformationTypeAdapter.Map(dto);

            dto.Id = (await PersonInformationTypeDomainService.InsertAndSaveChangesAsync(personInformationTypeBuilder)).Id;
            return dto;
        }

        public virtual async Task Delete(int id)
        {
            if (!ValidateId(id)) return;

            if (!Notification.HasNotification())
                await PersonInformationTypeDomainService.DeleteAsync(w => w.Id == id);
        }

        public virtual async Task<PersonInformationTypeDto> Get(DefaultIntRequestDto id)
        {
            if (!ValidateRequestDto(id) || !ValidateId<int>(id.Id)) return null;

            if (Notification.HasNotification())
                return PersonInformationTypeDto.NullInstance;

            var entity = await PersonInformationTypeDomainService.GetAsync(id).ConfigureAwait(false);
            return entity.MapTo<PersonInformationTypeDto>();
        }

        public virtual async Task<IListDto<PersonInformationTypeDto>> GetAll(GetAllPersonInformationTypeDto request)
        {
            ValidateRequestAllDto(request, nameof(request));

            if (Notification.HasNotification())
                return null;

            var response = await PersonInformationTypeDomainService.GetAllAsync<PersonInformationTypeDto>(request).ConfigureAwait(false);
            return response;
        }

        public virtual async Task<PersonInformationTypeDto> Update(int id, PersonInformationTypeDto dto)
        {
            ValidateDtoAndId(dto, id, nameof(dto), nameof(id));

            if (Notification.HasNotification())
                return PersonInformationTypeDto.NullInstance;

            dto.Id = id;

            var personInformationTypeBuilder = PersonInformationTypeAdapter.Map(dto);

            await PersonInformationTypeDomainService.UpdateAsync(personInformationTypeBuilder);
            return dto;
        }
    }
}
