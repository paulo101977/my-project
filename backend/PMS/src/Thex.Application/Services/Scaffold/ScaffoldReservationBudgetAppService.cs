﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a Crudzilla tool.
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using Tnf;

using Tnf.Dto;
using Tnf.Dto;
using Tnf.Domain.Services;
using Tnf.Application.Services;
using System.Threading.Tasks;
using Thex.Dto;
using Thex.Domain.Entities;
using Thex.Application.Adapters;
using Thex.Application.Interfaces;
using Thex.Domain;
using Tnf.Notifications;

namespace Thex.Application.Services
{
    public abstract class ScaffoldReservationBudgetAppService : ApplicationServiceBase, IScaffoldReservationBudgetAppService
    {
        protected readonly IReservationBudgetAdapter ReservationBudgetAdapter;
        protected readonly IDomainService<ReservationBudget> ReservationBudgetDomainService;

        public ScaffoldReservationBudgetAppService(
            IReservationBudgetAdapter reservationBudgetAdapter,
            IDomainService<ReservationBudget> reservationBudgetDomainService,
            INotificationHandler notificationHandler)
               : base(notificationHandler)
        {
            ReservationBudgetAdapter = Check.NotNull(reservationBudgetAdapter, nameof(reservationBudgetAdapter));
            ReservationBudgetDomainService = Check.NotNull(reservationBudgetDomainService, nameof(reservationBudgetDomainService));

            ReservationBudgetDomainService.EntityName = EntityNames.ReservationBudget;
        }

        public virtual async Task<ReservationBudgetDto> Create(ReservationBudgetDto dto)
        {
            ValidateDto<ReservationBudgetDto>(dto, nameof(dto));

            if (Notification.HasNotification())
                return ReservationBudgetDto.NullInstance;

            var reservationBudgetBuilder = ReservationBudgetAdapter.Map(dto);

            dto.Id = (await ReservationBudgetDomainService.InsertAndSaveChangesAsync(reservationBudgetBuilder)).Id;
            return dto;
        }

        public virtual async Task Delete(long id)
        {
            if (!ValidateId(id)) return;

            if (!Notification.HasNotification())
                await ReservationBudgetDomainService.DeleteAsync(w => w.Id == id);
        }

        public virtual async Task<ReservationBudgetDto> Get(DefaultLongRequestDto id)
        {
            if (!ValidateRequestDto(id) || !ValidateId<long>(id.Id)) return null;

            if (Notification.HasNotification())
                return ReservationBudgetDto.NullInstance;

            var entity = await ReservationBudgetDomainService.GetAsync(id).ConfigureAwait(false);
            return entity.MapTo<ReservationBudgetDto>();
        }

        public virtual async Task<IListDto<ReservationBudgetDto>> GetAll(GetAllReservationBudgetDto request)
        {
            ValidateRequestAllDto(request, nameof(request));

            if (Notification.HasNotification())
                return null;

            var response = await ReservationBudgetDomainService.GetAllAsync<ReservationBudgetDto>(request).ConfigureAwait(false);
            return response;
        }

        public virtual async Task<ReservationBudgetDto> Update(long id, ReservationBudgetDto dto)
        {
            ValidateDtoAndId(dto, id, nameof(dto), nameof(id));

            if (Notification.HasNotification())
                return ReservationBudgetDto.NullInstance;

            dto.Id = id;

            var reservationBudgetBuilder = ReservationBudgetAdapter.Map(dto);

            await ReservationBudgetDomainService.UpdateAsync(reservationBudgetBuilder);
            return dto;
        }
    }
}
