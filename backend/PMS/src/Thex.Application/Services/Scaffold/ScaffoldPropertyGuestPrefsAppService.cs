﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a Crudzilla tool.
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using Tnf;

using Tnf.Dto;
using Tnf.Dto;
using Tnf.Domain.Services;
using Tnf.Application.Services;
using System.Threading.Tasks;
using Thex.Dto;
using Thex.Domain.Entities;
using Thex.Application.Adapters;
using Thex.Application.Interfaces;
using Thex.Domain;
using Tnf.Notifications;

namespace Thex.Application.Services
{
    public abstract class ScaffoldPropertyGuestPrefsAppService : ApplicationServiceBase, IScaffoldPropertyGuestPrefsAppService
    {
        protected readonly IPropertyGuestPrefsAdapter PropertyGuestPrefsAdapter;
        protected readonly IDomainService<PropertyGuestPrefs> PropertyGuestPrefsDomainService;

        public ScaffoldPropertyGuestPrefsAppService(
            IPropertyGuestPrefsAdapter propertyGuestPrefsAdapter,
            IDomainService<PropertyGuestPrefs> propertyGuestPrefsDomainService,
            INotificationHandler notificationHandler)
               : base(notificationHandler)
        {
            PropertyGuestPrefsAdapter = Check.NotNull(propertyGuestPrefsAdapter, nameof(propertyGuestPrefsAdapter));
            PropertyGuestPrefsDomainService = Check.NotNull(propertyGuestPrefsDomainService, nameof(propertyGuestPrefsDomainService));

            PropertyGuestPrefsDomainService.EntityName = EntityNames.PropertyGuestPrefs;
        }

        public virtual async Task<PropertyGuestPrefsDto> Create(PropertyGuestPrefsDto dto)
        {
            ValidateDto<PropertyGuestPrefsDto>(dto, nameof(dto));

            if (Notification.HasNotification())
                return PropertyGuestPrefsDto.NullInstance;

            var propertyGuestPrefsBuilder = PropertyGuestPrefsAdapter.Map(dto);

            dto.Id = (await PropertyGuestPrefsDomainService.InsertAndSaveChangesAsync(propertyGuestPrefsBuilder)).Id;
            return dto;
        }

        public virtual async Task Delete(int id)
        {
            if (!ValidateId(id)) return;

            if (!Notification.HasNotification())
                await PropertyGuestPrefsDomainService.DeleteAsync(w => w.Id == id);
        }

        public virtual async Task<PropertyGuestPrefsDto> Get(DefaultIntRequestDto id)
        {
            if (!ValidateRequestDto(id) || !ValidateId<int>(id.Id)) return null;

            if (Notification.HasNotification())
                return PropertyGuestPrefsDto.NullInstance;

            var entity = await PropertyGuestPrefsDomainService.GetAsync(id).ConfigureAwait(false);
            return entity.MapTo<PropertyGuestPrefsDto>();
        }

        public virtual async Task<IListDto<PropertyGuestPrefsDto>> GetAll(GetAllPropertyGuestPrefsDto request)
        {
            ValidateRequestAllDto(request, nameof(request));

            if (Notification.HasNotification())
                return null;

            var response = await PropertyGuestPrefsDomainService.GetAllAsync<PropertyGuestPrefsDto>(request).ConfigureAwait(false);
            return response;
        }

        public virtual async Task<PropertyGuestPrefsDto> Update(int id, PropertyGuestPrefsDto dto)
        {
            ValidateDtoAndId(dto, id, nameof(dto), nameof(id));

            if (Notification.HasNotification())
                return PropertyGuestPrefsDto.NullInstance;

            dto.Id = id;

            var propertyGuestPrefsBuilder = PropertyGuestPrefsAdapter.Map(dto);

            await PropertyGuestPrefsDomainService.UpdateAsync(propertyGuestPrefsBuilder);
            return dto;
        }
    }
}
