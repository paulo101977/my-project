﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a Crudzilla tool.
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using Tnf;

using Tnf.Dto;
using Tnf.Dto;
using Tnf.Domain.Services;
using Tnf.Application.Services;
using System.Threading.Tasks;
using Thex.Dto;
using Thex.Domain.Entities;
using Thex.Application.Adapters;
using Thex.Application.Interfaces;
using Thex.Domain;
using Tnf.Notifications;
using Thex.Infra.ReadInterfaces;
using Thex.Kernel;
using Thex.Common;
using Thex.Common.Enumerations;

namespace Thex.Application.Services
{
    public abstract class ScaffoldPropertyCompanyClientCategoryAppService : ApplicationServiceBase, IScaffoldPropertyCompanyClientCategoryAppService
    {
        protected readonly IPropertyCompanyClientCategoryAdapter PropertyCompanyClientCategoryAdapter;
        protected readonly IDomainService<PropertyCompanyClientCategory> PropertyCompanyClientCategoryDomainService;
        private readonly IPropertyCompanyClientCategoryReadRepository PropertyCompanyClientCategoryReadRepository;

        public IApplicationUser ApplicationUser { get; }

        public ScaffoldPropertyCompanyClientCategoryAppService(
            IPropertyCompanyClientCategoryAdapter propertyCompanyClientCategoryAdapter,
            IDomainService<PropertyCompanyClientCategory> propertyCompanyClientCategoryDomainService,
            IPropertyCompanyClientCategoryReadRepository propertyCompanyClientCategoryReadRepository,
            IApplicationUser applicationUser,
            INotificationHandler notificationHandler)
               : base(notificationHandler)
        {
            PropertyCompanyClientCategoryAdapter = Check.NotNull(propertyCompanyClientCategoryAdapter, nameof(propertyCompanyClientCategoryAdapter));
            PropertyCompanyClientCategoryDomainService = Check.NotNull(propertyCompanyClientCategoryDomainService, nameof(propertyCompanyClientCategoryDomainService));
            PropertyCompanyClientCategoryReadRepository = propertyCompanyClientCategoryReadRepository;
            PropertyCompanyClientCategoryDomainService.EntityName = EntityNames.PropertyCompanyClientCategory;
            ApplicationUser = applicationUser;
        }

        public virtual async Task<PropertyCompanyClientCategoryDto> Create(PropertyCompanyClientCategoryDto dto)
        {
            ValidateDto<PropertyCompanyClientCategoryDto>(dto, nameof(dto));

            if (Notification.HasNotification())
                return PropertyCompanyClientCategoryDto.NullInstance;

            ValidatePropertyCompanyClientCategory(dto);

            if (Notification.HasNotification())
                return PropertyCompanyClientCategoryDto.NullInstance;

            var propertyCompanyClientCategoryBuilder = PropertyCompanyClientCategoryAdapter.Map(dto);

            dto.Id = (await PropertyCompanyClientCategoryDomainService.InsertAndSaveChangesAsync(propertyCompanyClientCategoryBuilder)).Id;
            return dto;
        }

        private void ValidatePropertyCompanyClientCategory(PropertyCompanyClientCategoryDto dto)
        {
            if (string.IsNullOrWhiteSpace(dto.PropertyCompanyClientCategoryName))
            {
                NotifyNullParameter();
                return;
            }

            var exists = PropertyCompanyClientCategoryReadRepository.Exists(dto.PropertyCompanyClientCategoryName, int.Parse(ApplicationUser.PropertyId), dto.Id);
            if(exists)
            {
                Notification.Raise(Notification.DefaultBuilder
                    .WithMessage(AppConsts.LocalizationSourceName, PropertyCompanyClientCategory.EntityError.AlreadyExistsPropertyCompanyClientCategoryWithSamePropertyCompanyClientCategoryName)
                    .WithDetailedMessage(AppConsts.LocalizationSourceName, PropertyCompanyClientCategory.EntityError.AlreadyExistsPropertyCompanyClientCategoryWithSamePropertyCompanyClientCategoryName)
                    .Build());
            }
        }

        public virtual async Task Delete(Guid id)
        {
            if (!ValidateId(id)) return;

            if (!Notification.HasNotification())
                await PropertyCompanyClientCategoryDomainService.DeleteAsync(w => w.Id == id);
        }

        public virtual async Task<PropertyCompanyClientCategoryDto> Get(DefaultGuidRequestDto id)
        {
            if (!ValidateRequestDto(id) || !ValidateId<Guid>(id.Id)) return null;

            if (Notification.HasNotification())
                return PropertyCompanyClientCategoryDto.NullInstance;

            var entity = await PropertyCompanyClientCategoryDomainService.GetAsync(id).ConfigureAwait(false);
            return entity.MapTo<PropertyCompanyClientCategoryDto>();
        }

        public virtual async Task<IListDto<PropertyCompanyClientCategoryDto>> GetAll(GetAllPropertyCompanyClientCategoryDto request)
        {
            ValidateRequestAllDto(request, nameof(request));

            if (Notification.HasNotification())
                return null;

            var response = await PropertyCompanyClientCategoryDomainService.GetAllAsync<PropertyCompanyClientCategoryDto>(request).ConfigureAwait(false);
            return response;
        }

        public virtual async Task<PropertyCompanyClientCategoryDto> Update(Guid id, PropertyCompanyClientCategoryDto dto)
        {
            ValidateDtoAndId(dto, id, nameof(dto), nameof(id));

            if (Notification.HasNotification())
                return PropertyCompanyClientCategoryDto.NullInstance;

            ValidatePropertyCompanyClientCategory(dto);

            if (Notification.HasNotification())
                return PropertyCompanyClientCategoryDto.NullInstance;

            dto.Id = id;

            var propertyCompanyClientCategoryBuilder = PropertyCompanyClientCategoryAdapter.Map(dto);

            await PropertyCompanyClientCategoryDomainService.UpdateAsync(propertyCompanyClientCategoryBuilder);
            return dto;
        }
    }
}
