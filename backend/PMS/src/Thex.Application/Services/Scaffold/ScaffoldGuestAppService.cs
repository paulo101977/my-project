﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a Crudzilla tool.
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using Tnf;

using Tnf.Dto;
using Tnf.Dto;
using Tnf.Domain.Services;
using Tnf.Application.Services;
using System.Threading.Tasks;
using Thex.Dto;
using Thex.Domain.Entities;
using Thex.Application.Adapters;
using Thex.Application.Interfaces;
using Thex.Domain;
using Tnf.Notifications;

namespace Thex.Application.Services
{
    public abstract class ScaffoldGuestAppService : ApplicationServiceBase, IScaffoldGuestAppService
    {
        protected readonly IGuestAdapter GuestAdapter;
        protected readonly IDomainService<Guest> GuestDomainService;

        public ScaffoldGuestAppService(
            IGuestAdapter guestAdapter,
            IDomainService<Guest> guestDomainService,
            INotificationHandler notificationHandler)
               : base(notificationHandler)
        {
            GuestAdapter = Check.NotNull(guestAdapter, nameof(guestAdapter));
            GuestDomainService = Check.NotNull(guestDomainService, nameof(guestDomainService));

            GuestDomainService.EntityName = EntityNames.Guest;
        }

        public virtual async Task<GuestDto> Create(GuestDto dto)
        {
            ValidateDto<GuestDto>(dto, nameof(dto));

            if (Notification.HasNotification())
                return GuestDto.NullInstance;

            var guestBuilder = GuestAdapter.Map(dto);

            dto.Id = (await GuestDomainService.InsertAndSaveChangesAsync(guestBuilder)).Id;
            return dto;
        }

        public virtual async Task Delete(long id)
        {
            if (!ValidateId(id)) return;

            if (!Notification.HasNotification())
                await GuestDomainService.DeleteAsync(w => w.Id == id);
        }

        public virtual async Task<GuestDto> Get(DefaultLongRequestDto id)
        {
            if (!ValidateRequestDto(id) || !ValidateId<long>(id.Id)) return null;

            if (Notification.HasNotification())
                return GuestDto.NullInstance;

            var entity = await GuestDomainService.GetAsync(id).ConfigureAwait(false);
            return entity.MapTo<GuestDto>();
        }

        public virtual async Task<IListDto<GuestDto>> GetAll(GetAllGuestDto request)
        {
            ValidateRequestAllDto(request, nameof(request));

            if (Notification.HasNotification())
                return null;;

            var response = await GuestDomainService.GetAllAsync<GuestDto>(request).ConfigureAwait(false);
            return response;
        }

        public virtual async Task<GuestDto> Update(long id, GuestDto dto)
        {
            ValidateDtoAndId(dto, id, nameof(dto), nameof(id));

            if (Notification.HasNotification())
                return GuestDto.NullInstance;

            dto.Id = id;

            var guestBuilder = GuestAdapter.Map(dto);

            await GuestDomainService.UpdateAsync(guestBuilder);
            return dto;
        }
    }
}
