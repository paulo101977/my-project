﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Thex.Application.Adapters;
using Thex.Application.Interfaces;
using Thex.Domain;
using Thex.Domain.Entities;
using Thex.Dto;
using Tnf;
using Tnf.Domain.Services;
using Tnf.Dto;
using Tnf.Notifications;

namespace Thex.Application.Services
{
    public abstract class ScaffoldGuestRegistrationDocumentAppService : ApplicationServiceBase, IScaffoldGuestRegistrationDocumentAppService
    {
        protected readonly IGuestRegistrationDocumentAdapter GuestRegistrationDocumentAdapter;
        protected readonly IDomainService<GuestRegistrationDocument> GuestRegistrationDocumentDomainService;

        public ScaffoldGuestRegistrationDocumentAppService(
            IGuestRegistrationDocumentAdapter guestRegistrationDocumentAdapter,
            IDomainService<GuestRegistrationDocument> guestRegistrationDocumentDomainService,
            INotificationHandler notificationHandler)
               : base(notificationHandler)
        {
            GuestRegistrationDocumentAdapter = Check.NotNull(guestRegistrationDocumentAdapter, nameof(guestRegistrationDocumentAdapter));
            GuestRegistrationDocumentDomainService = Check.NotNull(guestRegistrationDocumentDomainService, nameof(guestRegistrationDocumentDomainService));
            GuestRegistrationDocumentDomainService.EntityName = EntityNames.GuestRegistrationDocument;
        }

        public virtual async Task<GuestRegistrationDocumentDto> Create(GuestRegistrationDocumentDto dto)
        {
            ValidateDto<GuestRegistrationDocumentDto>(dto, nameof(dto));

            if (Notification.HasNotification())
                return GuestRegistrationDocumentDto.NullInstance;

            var channelBuilder = GuestRegistrationDocumentAdapter.Map(dto);

            dto.Id = (await GuestRegistrationDocumentDomainService.InsertAndSaveChangesAsync(channelBuilder)).Id;
            return dto;
        }

        public virtual async Task Delete(Guid id)
        {
            if (!ValidateId(id)) return; ;

            if (!Notification.HasNotification())
                await GuestRegistrationDocumentDomainService.DeleteAsync(w => w.Id == id);
        }

        public virtual async Task<GuestRegistrationDocumentDto> Get(DefaultGuidRequestDto id)
        {
            if (!ValidateRequestDto(id) || !ValidateId<Guid>(id.Id)) return null;

            if (Notification.HasNotification())
                return GuestRegistrationDocumentDto.NullInstance;

            var entity = await GuestRegistrationDocumentDomainService.GetAsync(id).ConfigureAwait(false);
            return entity.MapTo<GuestRegistrationDocumentDto>();
        }

        public virtual async Task<IListDto<GuestRegistrationDocumentDto>> GetAll(GetAllGuestRegistrationDocumentDto request)
        {
            ValidateRequestAllDto(request, nameof(request));

            if (Notification.HasNotification())
                return null;

            var response = await GuestRegistrationDocumentDomainService.GetAllAsync<GuestRegistrationDocumentDto>(request).ConfigureAwait(false);
            return response;
        }

        public virtual async Task<GuestRegistrationDocumentDto> Update(Guid id, GuestRegistrationDocumentDto dto)
        {
            ValidateDtoAndId(dto, id, nameof(dto), nameof(id));

            if (Notification.HasNotification())
                return GuestRegistrationDocumentDto.NullInstance;

            dto.Id = id;

            var channelBuilder = GuestRegistrationDocumentAdapter.Map(dto);

            await GuestRegistrationDocumentDomainService.UpdateAsync(channelBuilder);
            return dto;
        }
    }
}
