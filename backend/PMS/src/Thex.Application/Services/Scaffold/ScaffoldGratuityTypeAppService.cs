﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a Crudzilla tool.
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using Tnf;

using Tnf.Dto;
using Tnf.Dto;
using Tnf.Domain.Services;
using Tnf.Application.Services;
using System.Threading.Tasks;
using Thex.Dto;
using Thex.Domain.Entities;
using Thex.Application.Adapters;
using Thex.Application.Interfaces;
using Thex.Domain;
using Tnf.Notifications;

namespace Thex.Application.Services
{
    public abstract class ScaffoldGratuityTypeAppService : ApplicationServiceBase, IScaffoldGratuityTypeAppService
    {
        protected readonly IGratuityTypeAdapter GratuityTypeAdapter;
        protected readonly IDomainService<GratuityType> GratuityTypeDomainService;

        public ScaffoldGratuityTypeAppService(
            IGratuityTypeAdapter gratuityTypeAdapter,
            IDomainService<GratuityType> gratuityTypeDomainService,
            INotificationHandler notificationHandler)
               : base(notificationHandler)
        {
            GratuityTypeAdapter = Check.NotNull(gratuityTypeAdapter, nameof(gratuityTypeAdapter));
            GratuityTypeDomainService = Check.NotNull(gratuityTypeDomainService, nameof(gratuityTypeDomainService));

            GratuityTypeDomainService.EntityName = EntityNames.GratuityType;
        }

        public virtual async Task<GratuityTypeDto> Create(GratuityTypeDto dto)
        {
            ValidateDto<GratuityTypeDto>(dto, nameof(dto));

            if (Notification.HasNotification())
                return GratuityTypeDto.NullInstance;

            var gratuityTypeBuilder = GratuityTypeAdapter.Map(dto);

            dto.Id = (await GratuityTypeDomainService.InsertAndSaveChangesAsync(gratuityTypeBuilder)).Id;
            return dto;
        }

        public virtual async Task Delete(int id)
        {
            if (!ValidateId(id)) return;

            if (!Notification.HasNotification())
                await GratuityTypeDomainService.DeleteAsync(w => w.Id == id);
        }

        public virtual async Task<GratuityTypeDto> Get(DefaultIntRequestDto id)
        {
            if (!ValidateRequestDto(id) || !ValidateId<int>(id.Id)) return null;

            if (Notification.HasNotification())
                return GratuityTypeDto.NullInstance;

            var entity = await GratuityTypeDomainService.GetAsync(id).ConfigureAwait(false);
            return entity.MapTo<GratuityTypeDto>();
        }

        public virtual async Task<IListDto<GratuityTypeDto>> GetAll(GetAllGratuityTypeDto request)
        {
            ValidateRequestAllDto(request, nameof(request));

            if (Notification.HasNotification())
                return null;

            var response = await GratuityTypeDomainService.GetAllAsync<GratuityTypeDto>(request).ConfigureAwait(false);
            return response;
        }

        public virtual async Task<GratuityTypeDto> Update(int id, GratuityTypeDto dto)
        {
            ValidateDtoAndId(dto, id, nameof(dto), nameof(id));

            if (Notification.HasNotification())
                return GratuityTypeDto.NullInstance;

            dto.Id = id;

            var gratuityTypeBuilder = GratuityTypeAdapter.Map(dto);

            await GratuityTypeDomainService.UpdateAsync(gratuityTypeBuilder);
            return dto;
        }
    }
}
