﻿using System;
using System.Threading.Tasks;
using Thex.Application.Adapters;
using Thex.Application.Interfaces;
using Thex.Domain.Entities;
using Thex.Dto;
using Tnf;
using Tnf.Domain.Services;
using Tnf.Dto;
using Tnf.Notifications;

namespace Thex.Application.Services
{
    public abstract class ScaffoldCompanyClientChannelAppService : ApplicationServiceBase, IScaffoldCompanyClientChannelAppService
    {
        public ICompanyClientChannelAdapter CompanyClientChannelAdapter { get; }
        public IDomainService<CompanyClientChannel> CompanyClientChannelDomainService { get; }

        public ScaffoldCompanyClientChannelAppService(
           ICompanyClientChannelAdapter companyClientChannelAdapter,
           IDomainService<CompanyClientChannel> companyClientChannelDomainService,
           INotificationHandler notificationHandler)
              : base(notificationHandler)
        {
            CompanyClientChannelAdapter = Check.NotNull(companyClientChannelAdapter, nameof(companyClientChannelAdapter));
            CompanyClientChannelDomainService = Check.NotNull(companyClientChannelDomainService, nameof(companyClientChannelDomainService));
        }


        public virtual async Task<CompanyClientChannelDto> Create(CompanyClientChannelDto dto)
        {
            ValidateDto<CompanyClientChannelDto>(dto, nameof(dto));

            if (Notification.HasNotification())
                return CompanyClientChannelDto.NullInstance;

            var companyClientChannelBuilder = CompanyClientChannelAdapter.Map(dto);

            dto.Id = (await CompanyClientChannelDomainService.InsertAndSaveChangesAsync(companyClientChannelBuilder)).Id;
            return dto;
        }

        public virtual async Task Delete(Guid id)
        {
            if (!ValidateId(id)) return; ;

            if (!Notification.HasNotification())
                await CompanyClientChannelDomainService.DeleteAsync(w => w.Id == id);
        }

        public virtual async Task<CompanyClientChannelDto> Get(DefaultIntRequestDto id)
        {
            if (!ValidateRequestDto(id) || !ValidateId<int>(id.Id)) return null;

            if (Notification.HasNotification())
                return CompanyClientChannelDto.NullInstance;

            var entity = await CompanyClientChannelDomainService.GetAsync(id).ConfigureAwait(false);
            return entity.MapTo<CompanyClientChannelDto>();
        }

        public virtual async Task<IListDto<CompanyClientChannelDto>> GetAll(GetAllCompanyClientChannelDto request)
        {
            ValidateRequestAllDto(request, nameof(request));

            if (Notification.HasNotification())
                return null;

            var response = await CompanyClientChannelDomainService.GetAllAsync<CompanyClientChannelDto>(request).ConfigureAwait(false);
            return response;
        }

        public virtual async Task<CompanyClientChannelDto> Update(Guid id, CompanyClientChannelDto dto)
        {
            ValidateDtoAndId(dto, id, nameof(dto), nameof(id));

            if (Notification.HasNotification())
                return CompanyClientChannelDto.NullInstance;

            dto.Id = id;

            var companyClientChannelBuilder = CompanyClientChannelAdapter.Map(dto);

            await CompanyClientChannelDomainService.UpdateAsync(companyClientChannelBuilder);
            return dto;
        }
    }
}
