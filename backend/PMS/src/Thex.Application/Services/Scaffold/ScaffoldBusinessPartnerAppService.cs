﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a Crudzilla tool.
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using Tnf;

using Tnf.Dto;
using Tnf.Dto;
using Tnf.Domain.Services;
using Tnf.Application.Services;
using System.Threading.Tasks;
using Thex.Dto;
using Thex.Domain.Entities;
using Thex.Application.Adapters;
using Thex.Application.Interfaces;
using Thex.Domain;
using Tnf.Notifications;

namespace Thex.Application.Services
{
    public abstract class ScaffoldBusinessPartnerAppService : ApplicationServiceBase, IScaffoldBusinessPartnerAppService
    {
        protected readonly IBusinessPartnerAdapter BusinessPartnerAdapter;
        protected readonly IDomainService<BusinessPartner> BusinessPartnerDomainService;

        public ScaffoldBusinessPartnerAppService(
            IBusinessPartnerAdapter businessPartnerAdapter,
            IDomainService<BusinessPartner> businessPartnerDomainService,
            INotificationHandler notificationHandler)
               : base(notificationHandler)
        {
            BusinessPartnerAdapter = Check.NotNull(businessPartnerAdapter, nameof(businessPartnerAdapter));
            BusinessPartnerDomainService = Check.NotNull(businessPartnerDomainService, nameof(businessPartnerDomainService));

            BusinessPartnerDomainService.EntityName = EntityNames.BusinessPartner;
        }

        public virtual async Task<BusinessPartnerDto> Create(BusinessPartnerDto dto)
        {
            ValidateDto<BusinessPartnerDto>(dto, nameof(dto));

            if (Notification.HasNotification())
                return BusinessPartnerDto.NullInstance;

            var businessPartnerBuilder = BusinessPartnerAdapter.Map(dto);

            dto.Id = (await BusinessPartnerDomainService.InsertAndSaveChangesAsync(businessPartnerBuilder)).Id;
            return dto;
        }

        public virtual async Task Delete(Guid id)
        {
            if (!ValidateId(id)) return;

            if (!Notification.HasNotification())
                await BusinessPartnerDomainService.DeleteAsync(w => w.Id == id);
        }

        public virtual async Task<BusinessPartnerDto> Get(DefaultGuidRequestDto id)
        {
            if (!ValidateRequestDto(id) || !ValidateId<Guid>(id.Id)) return null;

            if (Notification.HasNotification())
                return BusinessPartnerDto.NullInstance;

            var entity = await BusinessPartnerDomainService.GetAsync(id).ConfigureAwait(false);
            return entity.MapTo<BusinessPartnerDto>();
        }

        public virtual async Task<IListDto<BusinessPartnerDto>> GetAll(GetAllBusinessPartnerDto request)
        {
            ValidateRequestAllDto(request, nameof(request));

            if (Notification.HasNotification())
                return null;

            var response = await BusinessPartnerDomainService.GetAllAsync<BusinessPartnerDto>(request).ConfigureAwait(false);
            return response;
        }

        public virtual async Task<BusinessPartnerDto> Update(Guid id, BusinessPartnerDto dto)
        {
            ValidateDtoAndId(dto, id, nameof(dto), nameof(id));

            if (Notification.HasNotification())
                return BusinessPartnerDto.NullInstance;

            dto.Id = id;

            var businessPartnerBuilder = BusinessPartnerAdapter.Map(dto);

            await BusinessPartnerDomainService.UpdateAsync(businessPartnerBuilder);
            return dto;
        }
    }
}
