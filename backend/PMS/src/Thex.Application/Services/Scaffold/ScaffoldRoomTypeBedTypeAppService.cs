﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a Crudzilla tool.
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using Tnf;

using Tnf.Dto;
using Tnf.Dto;
using Tnf.Domain.Services;
using Tnf.Application.Services;
using System.Threading.Tasks;
using Thex.Dto;
using Thex.Domain.Entities;
using Thex.Application.Adapters;
using Thex.Application.Interfaces;
using Thex.Domain;
using Tnf.Notifications;

namespace Thex.Application.Services
{
    public abstract class ScaffoldRoomTypeBedTypeAppService : ApplicationServiceBase, IScaffoldRoomTypeBedTypeAppService
    {
        protected readonly IRoomTypeBedTypeAdapter RoomTypeBedTypeAdapter;
        protected readonly IDomainService<RoomTypeBedType> RoomTypeBedTypeDomainService;

        public ScaffoldRoomTypeBedTypeAppService(
            IRoomTypeBedTypeAdapter roomTypeBedTypeAdapter,
            IDomainService<RoomTypeBedType> roomTypeBedTypeDomainService,
            INotificationHandler notificationHandler)
               : base(notificationHandler)
        {
            RoomTypeBedTypeAdapter = Check.NotNull(roomTypeBedTypeAdapter, nameof(roomTypeBedTypeAdapter));
            RoomTypeBedTypeDomainService = Check.NotNull(roomTypeBedTypeDomainService, nameof(roomTypeBedTypeDomainService));

            RoomTypeBedTypeDomainService.EntityName = EntityNames.RoomTypeBedType;
        }

        public virtual async Task<RoomTypeBedTypeDto> Create(RoomTypeBedTypeDto dto)
        {
            ValidateDto<RoomTypeBedTypeDto>(dto, nameof(dto));

            if (Notification.HasNotification())
                return RoomTypeBedTypeDto.NullInstance;

            var roomTypeBedTypeBuilder = RoomTypeBedTypeAdapter.Map(dto);

            dto.Id = (await RoomTypeBedTypeDomainService.InsertAndSaveChangesAsync(roomTypeBedTypeBuilder)).Id;
            return dto;
        }

        public virtual async Task Delete(int id)
        {
            if (!ValidateId(id)) return;

            if (!Notification.HasNotification())
                await RoomTypeBedTypeDomainService.DeleteAsync(w => w.Id == id);
        }

        public virtual async Task<RoomTypeBedTypeDto> Get(DefaultIntRequestDto id)
        {
            if (!ValidateRequestDto(id) || !ValidateId<int>(id.Id)) return null;

            if (Notification.HasNotification())
                return RoomTypeBedTypeDto.NullInstance;

            var entity = await RoomTypeBedTypeDomainService.GetAsync(id).ConfigureAwait(false);
            return entity.MapTo<RoomTypeBedTypeDto>();
        }

        public virtual async Task<IListDto<RoomTypeBedTypeDto>> GetAll(GetAllRoomTypeBedTypeDto request)
        {
            ValidateRequestAllDto(request, nameof(request));

            if (Notification.HasNotification())
                return null;

            var response = await RoomTypeBedTypeDomainService.GetAllAsync<RoomTypeBedTypeDto>(request).ConfigureAwait(false);
            return response;
        }

        public virtual async Task<RoomTypeBedTypeDto> Update(int id, RoomTypeBedTypeDto dto)
        {
            ValidateDtoAndId(dto, id, nameof(dto), nameof(id));

            if (Notification.HasNotification())
                return RoomTypeBedTypeDto.NullInstance;

            dto.Id = id;

            var roomTypeBedTypeBuilder = RoomTypeBedTypeAdapter.Map(dto);

            await RoomTypeBedTypeDomainService.UpdateAsync(roomTypeBedTypeBuilder);
            return dto;
        }
    }
}
