﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a Crudzilla tool.
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using Tnf;

using Tnf.Dto;
using Tnf.Dto;
using Tnf.Domain.Services;
using Tnf.Application.Services;
using System.Threading.Tasks;
using Thex.Dto;
using Thex.Domain.Entities;
using Thex.Application.Adapters;
using Thex.Application.Interfaces;
using Thex.Domain;
using Tnf.Notifications;

namespace Thex.Application.Services
{
    public abstract class ScaffoldPropertyParameterAppService : ApplicationServiceBase, IScaffoldPropertyParameterAppService
    {
        protected readonly IPropertyParameterAdapter PropertyParameterAdapter;
        protected readonly IDomainService<PropertyParameter> PropertyParameterDomainService;

        public ScaffoldPropertyParameterAppService(
            IPropertyParameterAdapter propertyParameterAdapter,
            IDomainService<PropertyParameter> propertyParameterDomainService,
            INotificationHandler notificationHandler)
               : base(notificationHandler)
        {
            PropertyParameterAdapter = Check.NotNull(propertyParameterAdapter, nameof(propertyParameterAdapter));
            PropertyParameterDomainService = Check.NotNull(propertyParameterDomainService, nameof(propertyParameterDomainService));

            PropertyParameterDomainService.EntityName = EntityNames.PropertyParameter;
        }

        public virtual async Task<PropertyParameterDto> Create(PropertyParameterDto dto)
        {
            ValidateDto<PropertyParameterDto>(dto, nameof(dto));

            if (Notification.HasNotification())
                return PropertyParameterDto.NullInstance;

            var propertyParameterBuilder = PropertyParameterAdapter.Map(dto);

            dto.Id = (await PropertyParameterDomainService.InsertAndSaveChangesAsync(propertyParameterBuilder)).Id;
            return dto;
        }

        public virtual async Task Delete(Guid id)
        {
            if (!ValidateId(id)) return;

            if (!Notification.HasNotification())
                await PropertyParameterDomainService.DeleteAsync(w => w.Id == id);
        }

        public virtual async Task<PropertyParameterDto> Get(DefaultGuidRequestDto id)
        {
            if (!ValidateRequestDto(id) || !ValidateId<Guid>(id.Id)) return null;

            if (Notification.HasNotification())
                return PropertyParameterDto.NullInstance;

            var entity = await PropertyParameterDomainService.GetAsync(id).ConfigureAwait(false);
            return entity.MapTo<PropertyParameterDto>();
        }

        public virtual async Task<IListDto<PropertyParameterDto>> GetAll(GetAllPropertyParameterDto request)
        {
            ValidateRequestAllDto(request, nameof(request));

            if (Notification.HasNotification())
                return null;

            var response = await PropertyParameterDomainService.GetAllAsync<PropertyParameterDto>(request).ConfigureAwait(false);
            return response;
        }

        public virtual async Task<PropertyParameterDto> Update(Guid id, PropertyParameterDto dto)
        {
            ValidateDtoAndId(dto, id, nameof(dto), nameof(id));

            if (Notification.HasNotification())
                return PropertyParameterDto.NullInstance;

            dto.Id = id;

            var propertyParameterBuilder = PropertyParameterAdapter.Map(dto);

            await PropertyParameterDomainService.UpdateAsync(propertyParameterBuilder);
            return dto;
        }
    }
}
