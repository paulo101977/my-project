﻿using System;
using Thex.Application.Adapters;
using Thex.Domain.Entities;
using Thex.Application.Interfaces;
using Tnf.Domain.Services;
using Thex.Dto;
using Tnf.Dto;
using Thex.Infra.ReadInterfaces;
using Tnf.Notifications;

namespace Thex.Application.Services
{
    public class ReasonCategoryAppService : ScaffoldReasonCategoryAppService, IReasonCategoryAppService
    {
        private readonly IReasonCategoryReadRepository _reasonCategoryReadRepository;
        public ReasonCategoryAppService(
            IReasonCategoryAdapter reasonCategoryAdapter,
            IDomainService<ReasonCategory> reasonCategoryDomainService,
            IReasonCategoryReadRepository reasonCategoryReadRepository,
            INotificationHandler notificationHandler)
            : base(reasonCategoryAdapter, reasonCategoryDomainService, notificationHandler)
        {
            _reasonCategoryReadRepository = reasonCategoryReadRepository;
        }

        public IListDto<ReasonCategoryGetAllDto> GetAllReasonCategory()
        {
            return _reasonCategoryReadRepository.GetAllReasonCategory();
        }
    }
}
