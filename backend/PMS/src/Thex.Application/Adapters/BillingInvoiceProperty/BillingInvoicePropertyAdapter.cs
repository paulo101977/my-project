﻿using Thex.Domain.Entities;
using Thex.Dto;

using Tnf.Notifications;

namespace Thex.Application.Adapters
{
    public class BillingInvoicePropertyAdapter : ScaffoldBillingInvoicePropertyAdapter, IBillingInvoicePropertyAdapter
    {
        public BillingInvoicePropertyAdapter(INotificationHandler notificationHandler)
            : base(notificationHandler)
        {
        }

        public override BillingInvoiceProperty.Builder Map(BillingInvoiceProperty entity, BillingInvoicePropertyDto dto)
        {
            var builder = base.Map(entity, dto);
            return builder;
        }

        public override BillingInvoiceProperty.Builder Map(BillingInvoicePropertyDto dto)
        {
            var builder = base.Map(dto);
            return builder;
        }
    }
}
