﻿using Tnf.Dependency;

namespace Thex.Application.Adapters
{
    public interface ITenantAdapter : IScaffoldTenantAdapter, ITransientDependency
    {
    }
}
