﻿using Thex.Domain.Entities;
using Thex.Dto;

using Tnf.Notifications;

namespace Thex.Application.Adapters
{
    public class TenantAdapter : ScaffoldTenantAdapter, ITenantAdapter
    {
        public TenantAdapter(INotificationHandler notificationHandler)
            : base(notificationHandler)
        {
        }

        public override Tenant.Builder Map(Tenant entity, TenantDto dto)
        {
            var builder = base.Map(entity, dto);
            return builder;
        }

        public override Tenant.Builder Map(TenantDto dto)
        {
            var builder = base.Map(dto);
            return builder;
        }
    }
}
