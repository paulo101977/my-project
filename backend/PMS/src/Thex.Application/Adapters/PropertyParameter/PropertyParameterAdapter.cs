﻿using Thex.Domain.Entities;
using Thex.Dto;

using Tnf.Notifications;

namespace Thex.Application.Adapters
{
    public class PropertyParameterAdapter : ScaffoldPropertyParameterAdapter, IPropertyParameterAdapter
    {
        public PropertyParameterAdapter(INotificationHandler notificationHandler)
            : base(notificationHandler)
        {
        }

        public override PropertyParameter.Builder Map(PropertyParameter entity, PropertyParameterDto dto)
        {
            var builder = base.Map(entity, dto);
            return builder;
        }

        public override PropertyParameter.Builder Map(PropertyParameterDto dto)
        {
            var builder = base.Map(dto);
            return builder;
        }
    }
}
