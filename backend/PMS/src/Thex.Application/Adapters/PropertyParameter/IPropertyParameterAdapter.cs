﻿using Tnf.Dependency;

namespace Thex.Application.Adapters
{
    public interface IPropertyParameterAdapter : IScaffoldPropertyParameterAdapter, ITransientDependency
    {
    }
}
