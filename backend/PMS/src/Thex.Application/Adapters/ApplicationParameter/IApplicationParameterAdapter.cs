﻿using Tnf.Dependency;

namespace Thex.Application.Adapters
{
    public interface IApplicationParameterAdapter : IScaffoldApplicationParameterAdapter, ITransientDependency
    {
    }
}
