﻿using Thex.Domain.Entities;
using Thex.Dto;

using Tnf.Notifications;

namespace Thex.Application.Adapters
{
    public class ApplicationParameterAdapter : ScaffoldApplicationParameterAdapter, IApplicationParameterAdapter
    {
        public ApplicationParameterAdapter(INotificationHandler notificationHandler)
            : base(notificationHandler)
        {
        }

        public override ApplicationParameter.Builder Map(ApplicationParameter entity, ApplicationParameterDto dto)
        {
            var builder = base.Map(entity, dto);
            return builder;
        }

        public override ApplicationParameter.Builder Map(ApplicationParameterDto dto)
        {
            var builder = base.Map(dto);
            return builder;
        }
    }
}
