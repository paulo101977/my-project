﻿using Tnf.Dependency;

namespace Thex.Application.Adapters
{
    public interface IBusinessPartnerAdapter : IScaffoldBusinessPartnerAdapter, ITransientDependency
    {
    }
}
