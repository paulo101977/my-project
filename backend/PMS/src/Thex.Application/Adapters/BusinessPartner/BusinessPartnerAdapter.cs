﻿using Thex.Domain.Entities;
using Thex.Dto;

using Tnf.Notifications;

namespace Thex.Application.Adapters
{
    public class BusinessPartnerAdapter : ScaffoldBusinessPartnerAdapter, IBusinessPartnerAdapter
    {
        public BusinessPartnerAdapter(INotificationHandler notificationHandler)
            : base(notificationHandler)
        {
        }

        public override BusinessPartner.Builder Map(BusinessPartner entity, BusinessPartnerDto dto)
        {
            var builder = base.Map(entity, dto);
            return builder;
        }

        public override BusinessPartner.Builder Map(BusinessPartnerDto dto)
        {
            var builder = base.Map(dto);
            return builder;
        }
    }
}
