﻿using Thex.Domain.Entities;
using Thex.Dto;

using Tnf.Notifications;

namespace Thex.Application.Adapters
{
    public class LocationAdapter : ScaffoldLocationAdapter, ILocationAdapter
    {
        public LocationAdapter(INotificationHandler notificationHandler)
            : base(notificationHandler)
        {
        }

        public override Location.Builder Map(Location entity, LocationDto dto)
        {
            var builder = base.Map(entity, dto);
            return builder;
        }

        public override Location.Builder Map(LocationDto dto)
        {
            var builder = base.Map(dto);
            return builder;
        }
    }
}
