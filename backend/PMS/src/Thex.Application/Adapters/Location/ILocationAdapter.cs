﻿using Tnf.Dependency;

namespace Thex.Application.Adapters
{
    public interface ILocationAdapter : IScaffoldLocationAdapter, ITransientDependency
    {
    }
}
