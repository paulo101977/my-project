﻿using Tnf.Dependency;

namespace Thex.Application.Adapters
{
    public interface IBillingInvoiceAdapter : IScaffoldBillingInvoiceAdapter, ITransientDependency
    {
    }
}
