﻿using Thex.Domain.Entities;
using Thex.Dto;

using Tnf.Notifications;

namespace Thex.Application.Adapters
{
    public class BillingInvoiceAdapter : ScaffoldBillingInvoiceAdapter, IBillingInvoiceAdapter
    {
        public BillingInvoiceAdapter(INotificationHandler notificationHandler)
            : base(notificationHandler)
        {
        }

        public override BillingInvoice.Builder Map(BillingInvoice entity, BillingInvoiceDto dto)
        {
            var builder = base.Map(entity, dto);
            return builder;
        }

        public override BillingInvoice.Builder Map(BillingInvoiceDto dto)
        {
            var builder = base.Map(dto);
            return builder;
        }
    }
}
