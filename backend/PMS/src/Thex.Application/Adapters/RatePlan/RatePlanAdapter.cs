﻿using Thex.Domain.Entities;
using Thex.Dto;

using Tnf.Notifications;

namespace Thex.Application.Adapters
{
    public class RatePlanAdapter : ScaffoldRatePlanAdapter, IRatePlanAdapter
    {
        public RatePlanAdapter(INotificationHandler notificationHandler)
            : base(notificationHandler)
        {
        }

        public override RatePlan.Builder Map(RatePlan entity, RatePlanDto dto)
        {
            var builder = base.Map(entity, dto);
            return builder;
        }

        public override RatePlan.Builder Map(RatePlanDto dto)
        {
            var builder = base.Map(dto);
            return builder;
        }
    }
}
