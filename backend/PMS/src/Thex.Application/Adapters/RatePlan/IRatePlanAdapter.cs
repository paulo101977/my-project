﻿using Tnf.Dependency;

namespace Thex.Application.Adapters
{
    public interface IRatePlanAdapter : IScaffoldRatePlanAdapter, ITransientDependency
    {
    }
}
