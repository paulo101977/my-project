﻿using Thex.Domain.Entities;
using Thex.Dto;
using Tnf.Notifications;

namespace Thex.Application.Adapters
{
    public class PropertyBaseRateHistoryAdapter : ScaffoldPropertyBaseRateHistoryAdapter, IPropertyBaseRateHistoryAdapter
    {
        public PropertyBaseRateHistoryAdapter(INotificationHandler notificationHandler)
            : base(notificationHandler)
        {
        }

        public override PropertyBaseRateHistory.Builder Map(PropertyBaseRateHistory entity, PropertyBaseRateHistoryDto dto)
        {
            var builder = base.Map(entity, dto);
            return builder;
        }

        public override PropertyBaseRateHistory.Builder Map(PropertyBaseRateHistoryDto dto)
        {
            var builder = base.Map(dto);
            return builder;
        }
    }
}
