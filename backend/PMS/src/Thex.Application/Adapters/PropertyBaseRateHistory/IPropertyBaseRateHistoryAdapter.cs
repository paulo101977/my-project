﻿using Tnf.Dependency;

namespace Thex.Application.Adapters
{
    public interface IPropertyBaseRateHistoryAdapter : IScaffoldPropertyBaseRateHistoryAdapter, ITransientDependency
    {
    }
}
