﻿using Tnf.Dependency;

namespace Thex.Application.Adapters
{
    public interface IPropertyBusinessPartnerAdapter : IScaffoldPropertyBusinessPartnerAdapter, ITransientDependency
    {
    }
}
