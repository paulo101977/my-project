﻿using Thex.Domain.Entities;
using Thex.Dto;

using Tnf.Notifications;

namespace Thex.Application.Adapters
{
    public class PropertyBusinessPartnerAdapter : ScaffoldPropertyBusinessPartnerAdapter, IPropertyBusinessPartnerAdapter
    {
        public PropertyBusinessPartnerAdapter(INotificationHandler notificationHandler)
            : base(notificationHandler)
        {
        }

        public override PropertyBusinessPartner.Builder Map(PropertyBusinessPartner entity, PropertyBusinessPartnerDto dto)
        {
            var builder = base.Map(entity, dto);
            return builder;
        }

        public override PropertyBusinessPartner.Builder Map(PropertyBusinessPartnerDto dto)
        {
            var builder = base.Map(dto);
            return builder;
        }
    }
}
