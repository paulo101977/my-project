﻿using Tnf.Dependency;

namespace Thex.Application.Adapters
{
    public interface ILocationCategoryAdapter : IScaffoldLocationCategoryAdapter, ITransientDependency
    {
    }
}
