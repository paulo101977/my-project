﻿using Thex.Domain.Entities;
using Thex.Dto;

using Tnf.Notifications;

namespace Thex.Application.Adapters
{
    public class LocationCategoryAdapter : ScaffoldLocationCategoryAdapter, ILocationCategoryAdapter
    {
        public LocationCategoryAdapter(INotificationHandler notificationHandler)
            : base(notificationHandler)
        {
        }

        public override LocationCategory.Builder Map(LocationCategory entity, LocationCategoryDto dto)
        {
            var builder = base.Map(entity, dto);
            return builder;
        }

        public override LocationCategory.Builder Map(LocationCategoryDto dto)
        {
            var builder = base.Map(dto);
            return builder;
        }
    }
}
