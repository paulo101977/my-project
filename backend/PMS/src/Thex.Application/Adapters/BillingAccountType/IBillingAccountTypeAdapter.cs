﻿using Tnf.Dependency;

namespace Thex.Application.Adapters
{
    public interface IBillingAccountTypeAdapter : IScaffoldBillingAccountTypeAdapter, ITransientDependency
    {
    }
}
