﻿using Thex.Domain.Entities;
using Thex.Dto;

using Tnf.Notifications;

namespace Thex.Application.Adapters
{
    public class BillingAccountTypeAdapter : ScaffoldBillingAccountTypeAdapter, IBillingAccountTypeAdapter
    {
        public BillingAccountTypeAdapter(INotificationHandler notificationHandler)
            : base(notificationHandler)
        {
        }

        public override BillingAccountType.Builder Map(BillingAccountType entity, BillingAccountTypeDto dto)
        {
            var builder = base.Map(entity, dto);
            return builder;
        }

        public override BillingAccountType.Builder Map(BillingAccountTypeDto dto)
        {
            var builder = base.Map(dto);
            return builder;
        }
    }
}
