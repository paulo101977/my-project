﻿using Tnf.Dependency;

namespace Thex.Application.Adapters
{
    public interface IRoomOccupiedAdapter : IScaffoldRoomOccupiedAdapter, ITransientDependency
    {
    }
}
