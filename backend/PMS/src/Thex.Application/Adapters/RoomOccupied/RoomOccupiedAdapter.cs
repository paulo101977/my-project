﻿using Thex.Domain.Entities;
using Thex.Dto;
using Thex.Kernel;
using Tnf.Notifications;

namespace Thex.Application.Adapters
{
    public class RoomOccupiedAdapter : ScaffoldRoomOccupiedAdapter, IRoomOccupiedAdapter
    {
        public RoomOccupiedAdapter(INotificationHandler notificationHandler, IApplicationUser applicationUser)
            : base(notificationHandler, applicationUser)
        {
        }

        public override RoomOccupied.Builder Map(int room, long reservationId)
            => base.Map(room, reservationId);
    }
}
