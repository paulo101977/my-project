﻿using Tnf.Dependency;

namespace Thex.Application.Adapters
{
    public interface IOccupationAdapter : IScaffoldOccupationAdapter, ITransientDependency
    {
    }
}
