﻿using Thex.Domain.Entities;
using Thex.Dto;

using Tnf.Notifications;

namespace Thex.Application.Adapters
{
    public class OccupationAdapter : ScaffoldOccupationAdapter, IOccupationAdapter
    {
        public OccupationAdapter(INotificationHandler notificationHandler)
            : base(notificationHandler)
        {
        }

        public override Occupation.Builder Map(Occupation entity, OccupationDto dto)
        {
            var builder = base.Map(entity, dto);
            return builder;
        }

        public override Occupation.Builder Map(OccupationDto dto)
        {
            var builder = base.Map(dto);
            return builder;
        }
    }
}
