﻿using Thex.Domain.Entities;
using Thex.Dto;

using Tnf.Notifications;

namespace Thex.Application.Adapters
{
    public class BedTypeAdapter : ScaffoldBedTypeAdapter, IBedTypeAdapter
    {
        public BedTypeAdapter(INotificationHandler notificationHandler)
            : base(notificationHandler)
        {
        }

        public override BedType.Builder Map(BedType entity, BedTypeDto dto)
        {
            var builder = base.Map(entity, dto);
            return builder;
        }

        public override BedType.Builder Map(BedTypeDto dto)
        {
            var builder = base.Map(dto);
            return builder;
        }
    }
}
