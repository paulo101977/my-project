﻿using Tnf.Dependency;

namespace Thex.Application.Adapters
{
    public interface IBedTypeAdapter : IScaffoldBedTypeAdapter, ITransientDependency
    {
    }
}
