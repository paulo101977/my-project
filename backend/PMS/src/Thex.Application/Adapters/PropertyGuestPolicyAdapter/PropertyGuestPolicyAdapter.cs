﻿using Thex.Domain.Entities;
using Thex.Dto;
using Tnf;
using Tnf.Notifications;

namespace Thex.Application.Adapters
{
    public class PropertyGuestPolicyAdapter : IPropertyGuestPolicyAdapter
    {
        public INotificationHandler NotificationHandler { get; }

        public PropertyGuestPolicyAdapter(INotificationHandler notificationHandler)
        {
            NotificationHandler = notificationHandler;
        }

        public virtual PropertyPolicy.Builder Map(PropertyPolicy entity, PropertyGuestPolicyDto dto)
        {
            Check.NotNull(entity, nameof(entity));
            Check.NotNull(dto, nameof(dto));

            var builder = new PropertyPolicy.Builder(NotificationHandler, entity)
                .WithId(dto.Id)
                .WithPropertyId(dto.PropertyId)
                .WithCountryId(dto.CountryId)
                .WithPolicyTypeId(dto.PolicyTypeId)
                .WithDescription(dto.Description)
                .WithIsActive(dto.IsActive);

            return builder;
        }

        public virtual PropertyPolicy.Builder Map(PropertyGuestPolicyDto dto)
        {
            Check.NotNull(dto, nameof(dto));

            var builder = new PropertyPolicy.Builder(NotificationHandler)
                .WithId(dto.Id)
                .WithPropertyId(dto.PropertyId)
                .WithCountryId(dto.CountryId)
                .WithPolicyTypeId(dto.PolicyTypeId)
                .WithDescription(dto.Description)
                .WithIsActive(dto.IsActive);

            return builder;
        }
    }
}
