﻿using Thex.Domain.Entities;
using Thex.Dto;
using Tnf.Dependency;

namespace Thex.Application.Adapters
{
    public interface IPropertyGuestPolicyAdapter : ITransientDependency
    {
        PropertyPolicy.Builder Map(PropertyPolicy entity, PropertyGuestPolicyDto dto);
        PropertyPolicy.Builder Map(PropertyGuestPolicyDto dto);
    }
}
