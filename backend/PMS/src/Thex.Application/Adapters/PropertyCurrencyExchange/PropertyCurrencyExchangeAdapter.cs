﻿using Thex.Domain.Entities;
using Thex.Dto;

using Tnf.Notifications;

namespace Thex.Application.Adapters
{
    public class PropertyCurrencyExchangeAdapter : ScaffoldPropertyCurrencyExchangeAdapter, IPropertyCurrencyExchangeAdapter
    {
        public PropertyCurrencyExchangeAdapter(INotificationHandler notificationHandler)
            : base(notificationHandler)
        {
        }

        public override PropertyCurrencyExchange.Builder Map(PropertyCurrencyExchange entity, PropertyCurrencyExchangeDto dto)
        {
            var builder = base.Map(entity, dto);
            return builder;
        }

        public override PropertyCurrencyExchange.Builder Map(PropertyCurrencyExchangeDto dto)
        {
            var builder = base.Map(dto);
            return builder;
        }
    }
}
