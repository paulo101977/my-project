﻿using Tnf.Dependency;

namespace Thex.Application.Adapters
{
    public interface IPropertyCurrencyExchangeAdapter : IScaffoldPropertyCurrencyExchangeAdapter, ITransientDependency
    {
    }
}
