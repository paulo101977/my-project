﻿using Thex.Domain.Entities;
using Thex.Dto;

using Tnf.Notifications;

namespace Thex.Application.Adapters
{
    public class GratuityTypeAdapter : ScaffoldGratuityTypeAdapter, IGratuityTypeAdapter
    {
        public GratuityTypeAdapter(INotificationHandler notificationHandler)
            : base(notificationHandler)
        {
        }

        public override GratuityType.Builder Map(GratuityType entity, GratuityTypeDto dto)
        {
            var builder = base.Map(entity, dto);
            return builder;
        }

        public override GratuityType.Builder Map(GratuityTypeDto dto)
        {
            var builder = base.Map(dto);
            return builder;
        }
    }
}
