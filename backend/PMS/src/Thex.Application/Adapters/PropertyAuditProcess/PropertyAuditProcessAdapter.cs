﻿using Thex.Domain.Entities;
using Thex.Dto;

using Tnf.Notifications;

namespace Thex.Application.Adapters
{
    public class PropertyAuditProcessAdapter : ScaffoldPropertyAuditProcessAdapter, IPropertyAuditProcessAdapter
    {
        public PropertyAuditProcessAdapter(INotificationHandler notificationHandler)
            : base(notificationHandler)
        {
        }

        public override PropertyAuditProcess.Builder Map(PropertyAuditProcess entity, PropertyAuditProcessDto dto)
        {
            var builder = base.Map(entity, dto);
            return builder;
        }

        public override PropertyAuditProcess.Builder Map(PropertyAuditProcessDto dto)
        {
            var builder = base.Map(dto);
            return builder;
        }
    }
}
