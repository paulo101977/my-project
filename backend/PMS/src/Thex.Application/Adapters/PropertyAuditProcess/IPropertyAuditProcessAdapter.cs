﻿using Tnf.Dependency;

namespace Thex.Application.Adapters
{
    public interface IPropertyAuditProcessAdapter : IScaffoldPropertyAuditProcessAdapter, ITransientDependency
    {
    }
}
