﻿using Newtonsoft.Json;
using System;
using Thex.Common.Enumerations;
using Thex.Domain.Entities;
using Thex.Dto;
using Thex.Dto.BillingAccountItem;
using Tnf.Notifications;

namespace Thex.Application.Adapters
{
    public class BillingAccountItemAdapter : ScaffoldBillingAccountItemAdapter, IBillingAccountItemAdapter
    {
        public BillingAccountItemAdapter(INotificationHandler notificationHandler)
            : base(notificationHandler)
        {
        }

        public override BillingAccountItem.Builder Map(BillingAccountItem entity, BillingAccountItemDto dto)
        {
            var builder = base.Map(entity, dto);
            return builder;
        }

        public override BillingAccountItem.Builder Map(BillingAccountItemDto dto)
        {
            var builder = base.Map(dto);
            return builder;
        }

        public BillingAccountItem.Builder MapCredit(BillingAccountItemCreditDto dto)
        {
            var builder = new BillingAccountItem.Builder(NotificationHandler)
                .WithId(Guid.NewGuid())
                .WithBillingAccountId(dto.BillingAccountId)
                .WithBillingItemId(dto.BillingItemId)
                .WithBillingAccountItemTypeId((int)BillingAccountItemTypeEnum.BillingAccountItemTypeCredit)
                .WithAmount(dto.Amount)
                .WithOriginalAmount(dto.Amount)
                .WithCheckNumber(dto.CheckNumber)
                .WithNsu(dto.NSU)
                .WithBillingAccountItemDate(dto.DateParameter.Value)
                .WithWasReversed(false)
                .WithInstallmentsQuantity(dto.InstallmentsQuantity)
                .WithCurrencyId(dto.CurrencyId.Value)
                .WithCurrencyExchangeReferenceId(dto.CurrencyExchangeReferenceId)
                .WithCurrencyExchangeReferenceSecId(dto.CurrencyExchangeReferenceSecId)
                .WithDateParameter(dto.DateParameter)
                .WithDaily(false)
                .WithIsTourismTax(false);


            return builder;
        }

        public BillingAccountItem.Builder MapDebit(BillingAccountItemDebitDto dto)
        {
            var builder = new BillingAccountItem.Builder(NotificationHandler)
                .WithId(Guid.NewGuid())
                .WithBillingAccountId(dto.BillingAccountId)
                .WithBillingItemId(dto.BillingItemId)
                .WithBillingAccountItemTypeId((int)BillingAccountItemTypeEnum.BillingAccountItemTypeDebit)
                .WithAmount(dto.Amount * (-1))
                .WithOriginalAmount(dto.Amount * (-1))
                .WithBillingAccountItemDate(dto.DateParameter.Value)
                .WithWasReversed(false)
                .WithCurrencyId(dto.CurrencyId.Value)
                .WithCurrencyExchangeReferenceId(dto.CurrencyExchangeReferenceId)
                .WithCurrencyExchangeReferenceSecId(dto.CurrencyExchangeReferenceSecId)
                .WithDateParameter(dto.DateParameter)
                .WithDaily(dto.Daily)
                .WithIsTourismTax(false);


            if (dto.DetailList != null && dto.DetailList.Count > 0)
                builder.WithIntegrationDetail(JsonConvert.SerializeObject(dto.DetailList));

            return builder;
        }

        public BillingAccountItem.Builder MapDebitWithInvoice(BillingAccountItemIntegrationDto dto)
        {
            var builder = new BillingAccountItem.Builder(NotificationHandler)
                .WithId(Guid.NewGuid())
                .WithBillingAccountId(dto.BillingAccountId)
                .WithBillingItemId(dto.BillingItemId)
                .WithBillingAccountItemTypeId((int)BillingAccountItemTypeEnum.BillingAccountItemTypeDebit)
                .WithAmount(dto.Amount * (-1))
                .WithOriginalAmount(dto.Amount * (-1))
                .WithBillingAccountItemDate(dto.DateParameter.Value)
                .WithWasReversed(false)
                .WithCurrencyId(dto.CurrencyId.Value)
                .WithCurrencyExchangeReferenceId(dto.CurrencyExchangeReferenceId)
                .WithCurrencyExchangeReferenceSecId(dto.CurrencyExchangeReferenceSecId)
                .WithDateParameter(dto.DateParameter)
                .WithDaily(false)
                .WithBillingInvoiceId(dto.BillingInvoiceId)
                .WithIsTourismTax(false);

            if (dto.DetailList != null && dto.DetailList.Count > 0)
                builder.WithIntegrationDetail(JsonConvert.SerializeObject(dto.DetailList));

            return builder;
        }

        public BillingAccountItem.Builder MapCreditWithInvoice(BillingAccountItemIntegrationDto dto)
        {
            var builder = new BillingAccountItem.Builder(NotificationHandler)
                .WithId(Guid.NewGuid())
                .WithBillingAccountId(dto.BillingAccountId)
                .WithBillingItemId(dto.BillingItemId)
                .WithBillingAccountItemTypeId((int)BillingAccountItemTypeEnum.BillingAccountItemTypeCredit)
                .WithAmount(dto.Amount)
                .WithOriginalAmount(dto.Amount)
                .WithBillingAccountItemDate(dto.DateParameter.Value)
                .WithWasReversed(false)
                .WithCurrencyId(dto.CurrencyId.Value)
                .WithCurrencyExchangeReferenceId(dto.CurrencyExchangeReferenceId)
                .WithCurrencyExchangeReferenceSecId(dto.CurrencyExchangeReferenceSecId)
                .WithDateParameter(dto.DateParameter)
                .WithDaily(false)
                .WithBillingInvoiceId(dto.BillingInvoiceId)
                .WithIsTourismTax(false)
                .WithBillingAccountItemParentId(dto.ParentId);

            return builder;
        }

        public BillingAccountItem.Builder MapDiscountForCredit(BillingAccountItemDiscountDto dto)
        {
            var builder = new BillingAccountItem.Builder(NotificationHandler)
                .WithId(Guid.NewGuid())
                .WithBillingAccountId(dto.BillingAccountId)
                .WithBillingItemId(dto.BillingItemId)
                .WithBillingAccountItemTypeId((int)BillingAccountItemTypeEnum.BillingAccountItemTypeDiscount)
                .WithAmount(dto.AmountDiscount * (-1))
                .WithOriginalAmount(dto.AmountDiscount * (-1))
                .WithBillingAccountItemDate(dto.DateParameter.Value)
                .WithPercentualAmount(dto.Percentual)
                .WithBillingAccountItemParentId(dto.BillingAccountItemId)
                .WithWasReversed(false)
                .WithBillingAccountItemReasonId(dto.ReasonId)
                .WithBillingAccountItemObservation(dto.BillingAccountItemObservation)
                .WithCurrencyId(dto.CurrencyId.Value)
                .WithCurrencyExchangeReferenceId(dto.CurrencyExchangeReferenceId)
                .WithCurrencyExchangeReferenceSecId(dto.CurrencyExchangeReferenceSecId)
                .WithDateParameter(dto.DateParameter)
                .WithDaily(false)
                .WithIsTourismTax(false);

            return builder;
        }

        public BillingAccountItem.Builder MapDiscountForDebit(BillingAccountItemDiscountDto dto)
        {
            var builder = new BillingAccountItem.Builder(NotificationHandler)
                .WithId(Guid.NewGuid())
                .WithBillingAccountId(dto.BillingAccountId)
                .WithBillingItemId(dto.BillingItemId)
                .WithBillingAccountItemTypeId((int)BillingAccountItemTypeEnum.BillingAccountItemTypeDiscount)
                .WithAmount(dto.AmountDiscount)
                .WithOriginalAmount(dto.AmountDiscount)
                .WithBillingAccountItemDate(dto.DateParameter.Value)
                .WithPercentualAmount(dto.Percentual)
                .WithBillingAccountItemParentId(dto.BillingAccountItemId)
                .WithWasReversed(false)
                .WithBillingAccountItemReasonId(dto.ReasonId)
                .WithBillingAccountItemObservation(dto.BillingAccountItemObservation)
                .WithCurrencyId(dto.CurrencyId.Value)
                .WithCurrencyExchangeReferenceId(dto.CurrencyExchangeReferenceId)
                .WithCurrencyExchangeReferenceSecId(dto.CurrencyExchangeReferenceSecId)
                .WithDateParameter(dto.DateParameter)
                .WithDaily(false)
                .WithIsTourismTax(false);

            return builder;
        }

        public BillingAccountItem.Builder MapDebitTax(BillingAccountItemDebitDto dto, Guid parentId)
        {
            var builder = new BillingAccountItem.Builder(NotificationHandler)
                .WithId(Guid.NewGuid())
                .WithBillingAccountId(dto.BillingAccountId)
                .WithBillingAccountIdLastSource(dto.BillingAccountIdLastSource)
                .WithBillingItemId(dto.BillingItemId)
                .WithBillingAccountItemTypeId((int)BillingAccountItemTypeEnum.BillingAccountItemTypeDebit)
                .WithAmount(dto.Amount * (-1))
                .WithOriginalAmount(dto.Amount * (-1))
                .WithBillingAccountItemDate(dto.DateParameter.Value)
                .WithBillingAccountItemParentId(parentId)
                .WithWasReversed(false)
                .WithCurrencyId(dto.CurrencyId.Value)
                .WithCurrencyExchangeReferenceId(dto.CurrencyExchangeReferenceId)
                .WithCurrencyExchangeReferenceSecId(dto.CurrencyExchangeReferenceSecId)
                .WithDateParameter(dto.DateParameter)
                .WithDaily(false)
                .WithIsTourismTax(false);

            return builder;
        }
        public BillingAccountItem.Builder MapDebitTaxForDiscount(BillingAccountItemDebitDto dto, Guid parentId)
        {
            var builder = new BillingAccountItem.Builder(NotificationHandler)
                .WithId(Guid.NewGuid())
                .WithBillingAccountId(dto.BillingAccountId)
                .WithBillingItemId(dto.BillingItemId)
                .WithBillingAccountItemTypeId((int)BillingAccountItemTypeEnum.BillingAccountItemTypeDiscount)
                .WithAmount(dto.Amount)
                .WithOriginalAmount(dto.Amount)
                .WithBillingAccountItemDate(dto.DateParameter.Value)
                .WithBillingAccountItemParentId(parentId)
                .WithWasReversed(false)
                .WithCurrencyId(dto.CurrencyId.Value)
                .WithCurrencyExchangeReferenceId(dto.CurrencyExchangeReferenceId)
                .WithCurrencyExchangeReferenceSecId(dto.CurrencyExchangeReferenceSecId)
                .WithDateParameter(dto.DateParameter)
                .WithDaily(false)
                .WithIsTourismTax(false);

            return builder;
        }

        public BillingAccountItem.Builder MapReserval(BillingAccountItem billingAccountItem, Guid parentId, int ReasonId, DateTime dateParameter)
        {
            var builder = new BillingAccountItem.Builder(NotificationHandler)
                .WithId(Guid.NewGuid())
                .WithBillingAccountId(billingAccountItem.BillingAccountId)
                .WithBillingItemId(billingAccountItem.BillingItemId)
                .WithBillingAccountItemTypeId((int)BillingAccountItemTypeEnum.BillingAccountItemTypeReversal)
                .WithAmount(billingAccountItem.Amount * (-1))
                .WithOriginalAmount(billingAccountItem.Amount * (-1))
                .WithBillingAccountItemDate(dateParameter)
                .WithBillingAccountItemParentId(parentId)
                .WithWasReversed(false)
                .WithCurrencyId(billingAccountItem.CurrencyId)
                .WithCurrencyExchangeReferenceId(billingAccountItem.CurrencyExchangeReferenceId)
                .WithCurrencyExchangeReferenceSecId(billingAccountItem.CurrencyExchangeReferenceSecId)
                .WithDateParameter(dateParameter)
                .WithDaily(false)
                .WithIsTourismTax(false);

            return builder;
        }

        public BillingAccountItem.Builder MapPartial(BillingAccountItem originalBillingAccountItem, decimal amount, Guid partialParentBillingAccountItemId)
        {
            var builder = new BillingAccountItem.Builder(NotificationHandler)
                .WithId(Guid.NewGuid())
                .WithBillingAccountId(originalBillingAccountItem.BillingAccountId)
                .WithBillingItemId(originalBillingAccountItem.BillingItemId)
                .WithBillingAccountItemTypeId((int)BillingAccountItemTypeEnum.BillingAccountItemTypePartial)
                .WithBillingAccountItemTypeIdLastSource(originalBillingAccountItem.BillingAccountItemTypeId)
                .WithAmount(amount)
                .WithOriginalAmount(amount)
                .WithBillingAccountItemDate(originalBillingAccountItem.BillingAccountItemDate)
                .WithWasReversed(false)
                .WithPartialParentBillingAccountItemId(partialParentBillingAccountItemId)
                .WithCurrencyId(originalBillingAccountItem.CurrencyId)
                .WithCurrencyExchangeReferenceId(originalBillingAccountItem.CurrencyExchangeReferenceId)
                .WithCurrencyExchangeReferenceSecId(originalBillingAccountItem.CurrencyExchangeReferenceSecId)
                .WithDateParameter(originalBillingAccountItem.DateParameter)
                .WithDaily(false)
                .WithIsTourismTax(false);

            return builder;
        }


        public BillingAccountItem.Builder MapPartialChild(BillingAccountItem originalBillingAccountItem, Guid parentId, decimal amount, Guid partialParentBillingAccountItemId)
        {
            var builder = new BillingAccountItem.Builder(NotificationHandler)
                .WithId(Guid.NewGuid())
                .WithBillingAccountId(originalBillingAccountItem.BillingAccountId)
                .WithBillingItemId(originalBillingAccountItem.BillingItemId)
                .WithBillingAccountItemTypeId((int)BillingAccountItemTypeEnum.BillingAccountItemTypePartial)
                .WithBillingAccountItemTypeIdLastSource(originalBillingAccountItem.BillingAccountItemTypeId)
                .WithAmount(amount)
                .WithOriginalAmount(amount)
                .WithBillingAccountItemDate(originalBillingAccountItem.BillingAccountItemDate)
                .WithBillingAccountItemParentId(parentId)
                .WithWasReversed(false)
                .WithPartialParentBillingAccountItemId(partialParentBillingAccountItemId)
                .WithCurrencyId(originalBillingAccountItem.CurrencyId)
                .WithCurrencyExchangeReferenceId(originalBillingAccountItem.CurrencyExchangeReferenceId)
                .WithCurrencyExchangeReferenceSecId(originalBillingAccountItem.CurrencyExchangeReferenceSecId)
                .WithDateParameter(originalBillingAccountItem.DateParameter)
                .WithDaily(false)
                .WithIsTourismTax(false);

            return builder;
        }

        public BillingAccountItem.Builder MapDailyDebit(BillingAccountItemDebitDto dto)
        {
            var builder = new BillingAccountItem.Builder(NotificationHandler)
                .WithId(Guid.NewGuid())
                .WithBillingAccountId(dto.BillingAccountId)
                .WithBillingAccountIdLastSource(dto.BillingAccountIdLastSource)
                .WithBillingItemId(dto.BillingItemId)
                .WithBillingAccountItemTypeId((int)BillingAccountItemTypeEnum.BillingAccountItemTypeDebit)
                .WithAmount(dto.Amount * (-1))
                .WithOriginalAmount(dto.Amount * (-1))
                .WithBillingAccountItemDate(dto.DateParameter.Value)
                .WithWasReversed(false)
                .WithCurrencyId(dto.CurrencyId.Value)
                .WithCurrencyExchangeReferenceId(dto.CurrencyExchangeReferenceId)
                .WithCurrencyExchangeReferenceSecId(dto.CurrencyExchangeReferenceSecId)
                .WithDateParameter(dto.DateParameter)
                .WithDaily(true)
                .WithIsTourismTax(false);

            if (dto.DetailList != null && dto.DetailList.Count > 0)
                builder.WithIntegrationDetail(JsonConvert.SerializeObject(dto.DetailList));

            return builder;
        }

        public BillingAccountItem.Builder MapDailyDebitTax(BillingAccountItemDebitDto dto, Guid parentId)
        {
            var builder = new BillingAccountItem.Builder(NotificationHandler)
                .WithId(Guid.NewGuid())
                .WithBillingAccountId(dto.BillingAccountId)
                .WithBillingItemId(dto.BillingItemId)
                .WithBillingAccountItemTypeId((int)BillingAccountItemTypeEnum.BillingAccountItemTypeDebit)
                .WithAmount(dto.Amount * (-1))
                .WithOriginalAmount(dto.Amount * (-1))
                .WithBillingAccountItemDate(dto.DateParameter.Value)
                .WithBillingAccountItemParentId(parentId)
                .WithWasReversed(false)
                .WithCurrencyId(dto.CurrencyId.Value)
                .WithCurrencyExchangeReferenceId(dto.CurrencyExchangeReferenceId)
                .WithCurrencyExchangeReferenceSecId(dto.CurrencyExchangeReferenceSecId)
                .WithDateParameter(dto.DateParameter)
                .WithDaily(true)
                .WithIsTourismTax(false);

            return builder;
        }

        public BillingAccountItem.Builder MapChild(BillingAccountItem billingAccountItem)
        {
            var builder = new BillingAccountItem.Builder(NotificationHandler)
                .WithId(Guid.NewGuid())
                .WithBillingAccountId(billingAccountItem.BillingAccountId)
                .WithBillingItemId(billingAccountItem.BillingItemId)
                .WithBillingAccountItemTypeId((int)BillingAccountItemTypeEnum.BillingAccountItemTypePartial)
                .WithBillingAccountItemTypeIdLastSource(billingAccountItem.BillingAccountItemTypeId)
                .WithAmount(billingAccountItem.Amount)
                .WithOriginalAmount(billingAccountItem.Amount)
                .WithBillingAccountItemDate(billingAccountItem.BillingAccountItemDate)
                .WithWasReversed(false)
                .WithCurrencyId(billingAccountItem.CurrencyId)
                .WithCurrencyExchangeReferenceId(billingAccountItem.CurrencyExchangeReferenceId)
                .WithCurrencyExchangeReferenceSecId(billingAccountItem.CurrencyExchangeReferenceSecId)
                .WithDateParameter(billingAccountItem.DateParameter)
                .WithDaily(false)
                .WithIsTourismTax(false);

            return builder;
        }

        public virtual BillingAccountItem.Builder MapCreditNote(BillingAccountItem billingAccountItemOriginal, decimal creditAmount, Guid billingAccountId, DateTime dateParameter)
        {
            var nowDateProperty = DateTime.UtcNow.ToZonedDateTimeLoggedUser().Date;

            var builder = new BillingAccountItem.Builder(NotificationHandler)
                .WithBillingAccountId(billingAccountId)
                .WithBillingAccountItemParentId(billingAccountItemOriginal.Id)
                .WithBillingItemId(billingAccountItemOriginal.BillingItemId)
                .WithBillingAccountItemTypeId((int)BillingAccountItemTypeEnum.BillingAccountItemCreditNote)
                .WithBillingAccountItemComments(billingAccountItemOriginal.BillingAccountItemComments)
                .WithAmount(creditAmount)
                .WithCurrencyId(billingAccountItemOriginal.CurrencyId)
                .WithCurrencyExchangeReferenceId(billingAccountItemOriginal.CurrencyExchangeReferenceId)
                .WithCurrencySymbol(billingAccountItemOriginal.CurrencySymbol)
                .WithOriginalAmount(creditAmount)
                .WithPercentualAmount(billingAccountItemOriginal.PercentualAmount)
                .WithCheckNumber(billingAccountItemOriginal.CheckNumber)
                .WithNsu(billingAccountItemOriginal.Nsu)
                .WithBillingAccountItemDate(nowDateProperty)
                .WithBillingAccountItemReasonId(billingAccountItemOriginal.BillingAccountItemReasonId)
                .WithBillingAccountItemObservation(billingAccountItemOriginal.BillingAccountItemObservation)
                .WithOrder(billingAccountItemOriginal.Order)
                .WithInstallmentsQuantity(billingAccountItemOriginal.InstallmentsQuantity)
                .WithServiceDescription(billingAccountItemOriginal.ServiceDescription)
                .WithIsOriginal(billingAccountItemOriginal.IsOriginal)
                .WithIntegrationDetail(billingAccountItemOriginal.IntegrationDetail)
                .WithIntegrationPaymentId(billingAccountItemOriginal.IntegrationPaymentId)
                .WithDateParameter(dateParameter)
                .WithDaily(false)
                .WithIsTourismTax(false);

            return builder;
        }

        public BillingAccountItem.Builder MapTaxTourismDebit(BillingAccountItemTourismTaxDto dto)
        {
            var builder = new BillingAccountItem.Builder(NotificationHandler)
                .WithId(Guid.NewGuid())
                .WithBillingAccountId(dto.BillingAccountId)
                .WithBillingAccountIdLastSource(dto.BillingAccountIdLastSource)
                .WithBillingItemId(dto.BillingItemId)
                .WithBillingAccountItemTypeId((int)BillingAccountItemTypeEnum.BillingAccountItemTypeDebit)
                .WithAmount(dto.Amount * (-1))
                .WithOriginalAmount(dto.Amount * (-1))
                .WithBillingAccountItemDate(dto.BillingAccountItemDate)
                .WithWasReversed(false)
                .WithCurrencyId(dto.CurrencyId.Value)
                .WithCurrencyExchangeReferenceId(dto.CurrencyExchangeReferenceId)
                .WithCurrencyExchangeReferenceSecId(dto.CurrencyExchangeReferenceSecId)
                .WithDateParameter(dto.BillingAccountItemDate)
                .WithDaily(false)
                .WithIsTourismTax(true);

            if (dto.DetailList != null && dto.DetailList.Count > 0)
                builder.WithIntegrationDetail(JsonConvert.SerializeObject(dto.DetailList));

            return builder;
        }

        public BillingAccountItem.Builder MapTaxTourismDebitTax(BillingAccountItemDebitDto dto, Guid parentId, DateTime billingAccountItemDate)
        {
            var builder = new BillingAccountItem.Builder(NotificationHandler)
                .WithId(Guid.NewGuid())
                .WithBillingAccountId(dto.BillingAccountId)
                .WithBillingAccountIdLastSource(dto.BillingAccountIdLastSource)
                .WithBillingItemId(dto.BillingItemId)
                .WithBillingAccountItemTypeId((int)BillingAccountItemTypeEnum.BillingAccountItemTypeDebit)
                .WithAmount(dto.Amount * (-1))
                .WithOriginalAmount(dto.Amount * (-1))
                .WithBillingAccountItemDate(billingAccountItemDate)
                .WithBillingAccountItemParentId(parentId)
                .WithWasReversed(false)
                .WithCurrencyId(dto.CurrencyId.Value)
                .WithCurrencyExchangeReferenceId(dto.CurrencyExchangeReferenceId)
                .WithCurrencyExchangeReferenceSecId(dto.CurrencyExchangeReferenceSecId)
                .WithDateParameter(dto.DateParameter)
                .WithDaily(false)
                .WithIsTourismTax(true);

            return builder;
        }
    }
}
