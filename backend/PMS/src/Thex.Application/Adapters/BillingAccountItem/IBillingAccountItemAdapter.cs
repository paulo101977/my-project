﻿using System;

using Thex.Domain.Entities;
using Thex.Dto;
using Thex.Dto.BillingAccountItem;
using Tnf.Dependency;

namespace Thex.Application.Adapters
{
    public interface IBillingAccountItemAdapter : IScaffoldBillingAccountItemAdapter, ITransientDependency
    {
        BillingAccountItem.Builder MapCredit(BillingAccountItemCreditDto dto);
        BillingAccountItem.Builder MapDebit(BillingAccountItemDebitDto dto);
        BillingAccountItem.Builder MapDebitTax(BillingAccountItemDebitDto dto, Guid parentId);
        BillingAccountItem.Builder MapDebitTaxForDiscount(BillingAccountItemDebitDto dto, Guid parentId);
        BillingAccountItem.Builder MapReserval(BillingAccountItem dto, Guid parentId, int ReasonId, DateTime dateParameter);
        BillingAccountItem.Builder MapDiscountForDebit(BillingAccountItemDiscountDto dto);
        BillingAccountItem.Builder MapDiscountForCredit(BillingAccountItemDiscountDto dto);
        BillingAccountItem.Builder MapPartial(BillingAccountItem originalBillingAccountItem, decimal amount, Guid partialParentBillingAccountItemId);
        BillingAccountItem.Builder MapPartialChild(BillingAccountItem originalBillingAccountItem, Guid parentId, decimal amount, Guid partialParentBillingAccountItemId);
        BillingAccountItem.Builder MapDailyDebit(BillingAccountItemDebitDto dto);
        BillingAccountItem.Builder MapDailyDebitTax(BillingAccountItemDebitDto dto, Guid parentId);
        BillingAccountItem.Builder MapChild(BillingAccountItem originalBillingAccountItem);
        BillingAccountItem.Builder MapCreditNote(BillingAccountItem billingAccountItemOriginal, decimal creditAmount, Guid billingAccountId, DateTime dateParameter);
        BillingAccountItem.Builder MapTaxTourismDebit(BillingAccountItemTourismTaxDto dto);
        BillingAccountItem.Builder MapTaxTourismDebitTax(BillingAccountItemDebitDto dto, Guid parentId, DateTime billingAccountItemDate);
        BillingAccountItem.Builder MapDebitWithInvoice(BillingAccountItemIntegrationDto dto);
        BillingAccountItem.Builder MapCreditWithInvoice(BillingAccountItemIntegrationDto dto);

    }
}
