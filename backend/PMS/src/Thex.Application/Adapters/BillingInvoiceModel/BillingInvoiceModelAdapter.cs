﻿using Thex.Domain.Entities;
using Thex.Dto;

using Tnf.Notifications;

namespace Thex.Application.Adapters
{
    public class BillingInvoiceModelAdapter : ScaffoldBillingInvoiceModelAdapter, IBillingInvoiceModelAdapter
    {
        public BillingInvoiceModelAdapter(INotificationHandler notificationHandler)
            : base(notificationHandler)
        {
        }

        public override BillingInvoiceModel.Builder Map(BillingInvoiceModel entity, BillingInvoiceModelDto dto)
        {
            var builder = base.Map(entity, dto);
            return builder;
        }

        public override BillingInvoiceModel.Builder Map(BillingInvoiceModelDto dto)
        {
            var builder = base.Map(dto);
            return builder;
        }
    }
}
