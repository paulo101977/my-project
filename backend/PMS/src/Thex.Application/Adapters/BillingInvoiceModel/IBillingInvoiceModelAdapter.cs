﻿using Tnf.Dependency;

namespace Thex.Application.Adapters
{
    public interface IBillingInvoiceModelAdapter : IScaffoldBillingInvoiceModelAdapter, ITransientDependency
    {
    }
}
