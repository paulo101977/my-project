﻿using Thex.Domain.Entities;
using Thex.Dto;

using Tnf.Notifications;

namespace Thex.Application.Adapters
{
    public class ReservationItemAdapter : ScaffoldReservationItemAdapter, IReservationItemAdapter
    {
        public ReservationItemAdapter(INotificationHandler notificationHandler)
            : base(notificationHandler)
        {
        }

        public override ReservationItem.Builder Map(ReservationItem entity, ReservationItemDto dto)
        {
            var builder = base.Map(entity, dto);
            return builder;
        }

        public override ReservationItem.Builder Map(ReservationItemDto dto)
        {
            var builder = base.Map(dto);
            return builder;
        }

        public ReservationItem.Builder MapEntrance(ReservationItemDto dto, bool isChild)
        {
            if (isChild)
                dto.ChildCount++;
            else
                dto.AdultCount++;

            if (string.IsNullOrEmpty(dto.ExternalReservationNumber))
                dto.ExternalReservationNumber = dto.ReservationItemUid.ToString().ToUpper();

            var builder = base.Map(dto);
            return builder;
        }
    }
}
