﻿using Thex.Domain.Entities;
using Thex.Dto;
using Tnf.Dependency;

namespace Thex.Application.Adapters
{
    public interface IReservationItemAdapter : IScaffoldReservationItemAdapter, ITransientDependency
    {
        ReservationItem.Builder MapEntrance(ReservationItemDto dto, bool isChild);
    }
}
