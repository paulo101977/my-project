﻿using Tnf.Dependency;

namespace Thex.Application.Adapters
{
    public interface IBillingItemPaymentConditionAdapter : IScaffoldBillingItemPaymentConditionAdapter, ITransientDependency
    {
    }
}
