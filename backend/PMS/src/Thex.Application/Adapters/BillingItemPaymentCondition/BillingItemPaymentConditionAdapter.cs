﻿using Thex.Domain.Entities;
using Thex.Dto;

using Tnf.Notifications;

namespace Thex.Application.Adapters
{
    public class BillingItemPaymentConditionAdapter : ScaffoldBillingItemPaymentConditionAdapter, IBillingItemPaymentConditionAdapter
    {
        public BillingItemPaymentConditionAdapter(INotificationHandler notificationHandler)
            : base(notificationHandler)
        {
        }

        public override BillingItemPaymentCondition.Builder Map(BillingItemPaymentCondition entity, BillingItemPaymentConditionDto dto)
        {
            var builder = base.Map(entity, dto);
            return builder;
        }

        public override BillingItemPaymentCondition.Builder Map(BillingItemPaymentConditionDto dto)
        {
            var builder = base.Map(dto);
            return builder;
        }
    }
}
