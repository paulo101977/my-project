﻿using Thex.Domain.Entities;
using Thex.Dto;

using Tnf.Notifications;

namespace Thex.Application.Adapters
{
    public class MealPlanTypeAdapter : ScaffoldMealPlanTypeAdapter, IMealPlanTypeAdapter
    {
        public MealPlanTypeAdapter(INotificationHandler notificationHandler)
            : base(notificationHandler)
        {
        }

        public override MealPlanType.Builder Map(MealPlanType entity, MealPlanTypeDto dto)
        {
            var builder = base.Map(entity, dto);
            return builder;
        }

        public override MealPlanType.Builder Map(MealPlanTypeDto dto)
        {
            var builder = base.Map(dto);
            return builder;
        }
    }
}
