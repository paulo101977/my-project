﻿using Tnf.Dependency;

namespace Thex.Application.Adapters
{
    public interface IMealPlanTypeAdapter : IScaffoldMealPlanTypeAdapter, ITransientDependency
    {
    }
}
