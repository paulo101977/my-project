﻿using System;
using Thex.Domain.Entities;
using Thex.Dto;
using Thex.Kernel;
using Tnf;
using Tnf.Notifications;

namespace Thex.Application.Adapters
{
    public abstract class ScaffoldLevelAdapter : IScaffoldLevelAdapter
    {
        public INotificationHandler NotificationHandler { get; }

        private readonly IApplicationUser _applicationUser;

        public ScaffoldLevelAdapter(INotificationHandler notificationHandler, IApplicationUser applicationUser)
        {
            NotificationHandler = notificationHandler;
            _applicationUser = applicationUser;
        }

        public virtual Level.Builder Map(Level entity, LevelDto dto)
        {
            Check.NotNull(entity, nameof(entity));
            Check.NotNull(dto, nameof(dto));

            var builder = new Level.Builder(NotificationHandler, entity)
                .WithId(entity.Id)
                .WithLevelCode(dto.LevelCode)
                .WithDescription(dto.Description)
                .WithOrder(dto.Order)
                .WithPropertyId(entity.PropertyId)
                .WithIsActive(dto.IsActive);

            return builder;
        }

        public virtual Level.Builder Map(LevelDto dto)
        {
            Check.NotNull(dto, nameof(dto));

            var builder = new Level.Builder(NotificationHandler)
                .WithId(Guid.NewGuid())
                .WithLevelCode(dto.LevelCode)
                .WithDescription(dto.Description)
                .WithOrder(dto.Order)
                .WithPropertyId(int.Parse(_applicationUser.PropertyId))
                .WithIsActive(dto.IsActive);

            return builder;
        }
    }
}
