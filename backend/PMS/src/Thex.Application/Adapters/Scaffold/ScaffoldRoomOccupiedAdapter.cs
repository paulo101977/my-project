﻿using System;
using Thex.Domain.Entities;
using Thex.Dto;
using Thex.Kernel;
using Tnf;
using Tnf.Notifications;

namespace Thex.Application.Adapters
{
    public abstract class ScaffoldRoomOccupiedAdapter : IScaffoldRoomOccupiedAdapter
    {
        public INotificationHandler NotificationHandler { get; }

        private readonly IApplicationUser _applicationUser;

        public ScaffoldRoomOccupiedAdapter(INotificationHandler notificationHandler, IApplicationUser applicationUser)
        {
            NotificationHandler = notificationHandler;
            _applicationUser = applicationUser;
        }

        public virtual RoomOccupied.Builder Map(int roomId, long reservationId)
        {
             var builder = new RoomOccupied.Builder(NotificationHandler)
                .WithId(Guid.NewGuid())
                .WithRoomId(roomId)
                .WithPropertyId(int.Parse(_applicationUser.PropertyId))
                .WithReservationId(reservationId);

            return builder;            
        }
            
        
    }
}
