﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a Crudzilla tool.
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using Thex.Domain.Entities;

using Thex.Dto;
using Tnf;
using Tnf.Notifications;

namespace Thex.Application.Adapters
{
    public abstract class ScaffoldPropertyMealPlanTypeAdapter : IScaffoldPropertyMealPlanTypeAdapter
    {
        public INotificationHandler NotificationHandler { get; }

        public ScaffoldPropertyMealPlanTypeAdapter(INotificationHandler notificationHandler)
        {
            NotificationHandler = notificationHandler;
        }

        public virtual PropertyMealPlanType.Builder Map(PropertyMealPlanType entity, PropertyMealPlanTypeDto dto)
        {
            Check.NotNull(entity, nameof(entity));
            Check.NotNull(dto, nameof(dto));

            var builder = new PropertyMealPlanType.Builder(NotificationHandler, entity)
                .WithId(dto.Id)
                .WithPropertyMealPlanTypeCode(dto.PropertyMealPlanTypeCode)
                .WithPropertyMealPlanTypeName(dto.PropertyMealPlanTypeName)
                .WithPropertyId(dto.PropertyId)
                .WithMealPlanTypeId(dto.MealPlanTypeId)
                .WithIsActive(dto.IsActive);

            return builder;
        }

        public virtual PropertyMealPlanType.Builder Map(PropertyMealPlanTypeDto dto)
        {
            Check.NotNull(dto, nameof(dto));

            var builder = new PropertyMealPlanType.Builder(NotificationHandler)
                .WithId(dto.Id)
                .WithPropertyMealPlanTypeCode(dto.PropertyMealPlanTypeCode)
                .WithPropertyMealPlanTypeName(dto.PropertyMealPlanTypeName)
                .WithPropertyId(dto.PropertyId)
                .WithMealPlanTypeId(dto.MealPlanTypeId)
                .WithIsActive(dto.IsActive);

            return builder;
        }
    }
}
