﻿
using Thex.Domain.Entities;
using Thex.Dto;

namespace Thex.Application.Adapters
{
    public interface IScaffoldRoomOccupiedAdapter
    {
        RoomOccupied.Builder Map(int roomId, long reservationId);
    }
}
