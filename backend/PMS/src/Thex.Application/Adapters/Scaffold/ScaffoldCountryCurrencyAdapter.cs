﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a Crudzilla tool.
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using Thex.Domain.Entities;

using Thex.Dto;
using Tnf;
using Tnf.Notifications;

namespace Thex.Application.Adapters
{
    public abstract class ScaffoldCountryCurrencyAdapter : IScaffoldCountryCurrencyAdapter
    {
        public INotificationHandler NotificationHandler { get; }

        public ScaffoldCountryCurrencyAdapter(INotificationHandler notificationHandler)
        {
            NotificationHandler = notificationHandler;
        }

        public virtual CountryCurrency.Builder Map(CountryCurrency entity, CountryCurrencyDto dto)
        {
            Check.NotNull(entity, nameof(entity));
            Check.NotNull(dto, nameof(dto));

            var builder = new CountryCurrency.Builder(NotificationHandler, entity)
                .WithId(dto.Id)
                .WithCountrySubdivisionId(dto.CountrySubdivisionId)
                .WithCurrencyId(dto.CurrencyId);

            return builder;
        }

        public virtual CountryCurrency.Builder Map(CountryCurrencyDto dto)
        {
            Check.NotNull(dto, nameof(dto));

            var builder = new CountryCurrency.Builder(NotificationHandler)
                .WithId(dto.Id)
                .WithCountrySubdivisionId(dto.CountrySubdivisionId)
                .WithCurrencyId(dto.CurrencyId);

            return builder;
        }
    }
}
