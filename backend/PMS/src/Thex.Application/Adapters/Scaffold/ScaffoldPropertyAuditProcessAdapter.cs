﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a Crudzilla tool.
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using Thex.Domain.Entities;

using Thex.Dto;
using Tnf;
using Tnf.Notifications;

namespace Thex.Application.Adapters
{
    public abstract class ScaffoldPropertyAuditProcessAdapter : IScaffoldPropertyAuditProcessAdapter
    {
        public INotificationHandler NotificationHandler { get; }

        public ScaffoldPropertyAuditProcessAdapter(INotificationHandler notificationHandler)
        {
            NotificationHandler = notificationHandler;
        }

        public virtual PropertyAuditProcess.Builder Map(PropertyAuditProcess entity, PropertyAuditProcessDto dto)
        {
            Check.NotNull(entity, nameof(entity));
            Check.NotNull(dto, nameof(dto));

            var builder = new PropertyAuditProcess.Builder(NotificationHandler, entity)
                .WithId(dto.Id)
                .WithPropertySystemDate(dto.PropertySystemDate)
                .WithStartDate(dto.StartDate)
                .WithEndDate(dto.EndDate)
                .WithPropertyId(dto.PropertyId)
                .WithAuditStepTypeId(dto.AuditStepTypeId)
                .WithTenantId(dto.TenantId);

            return builder;
        }

        public virtual PropertyAuditProcess.Builder Map(PropertyAuditProcessDto dto)
        {
            Check.NotNull(dto, nameof(dto));

            var builder = new PropertyAuditProcess.Builder(NotificationHandler)
                .WithId(dto.Id)
                .WithPropertySystemDate(dto.PropertySystemDate)
                .WithStartDate(dto.StartDate)
                .WithEndDate(dto.EndDate)
                .WithPropertyId(dto.PropertyId)
                .WithAuditStepTypeId(dto.AuditStepTypeId)
                .WithTenantId(dto.TenantId);

            return builder;
        }
    }
}
