﻿using Thex.Domain.Entities;
using Thex.Dto;
using Tnf;
using Tnf.Notifications;

namespace Thex.Application.Adapters
{
    public abstract class ScaffoldCompanyClientChannelAdapter : IScaffoldCompanyClientChannelAdapter
    {
        public INotificationHandler NotificationHandler { get; }

        public ScaffoldCompanyClientChannelAdapter(INotificationHandler notificationHandler)
        {
            NotificationHandler = notificationHandler;
        }

        public virtual CompanyClientChannel.Builder Map(CompanyClientChannel entity, CompanyClientChannelDto dto)
        {
            Check.NotNull(entity, nameof(entity));
            Check.NotNull(dto, nameof(dto));

            var builder = new CompanyClientChannel.Builder(NotificationHandler, entity)
                .WithId(dto.Id)
                .WithChannelId(dto.ChannelId)
                .WithCompanyClientId(dto.CompanyClientId)
                .WithCompanyId(dto.CompanyId)
                .WithIsActive(dto.IsActive);

            return builder;
        }

        public virtual CompanyClientChannel.Builder Map(CompanyClientChannelDto dto)
        {
            Check.NotNull(dto, nameof(dto));

            var builder = new CompanyClientChannel.Builder(NotificationHandler)
                .WithId(dto.Id)
                .WithChannelId(dto.ChannelId)
                .WithCompanyClientId(dto.CompanyClientId)
                .WithCompanyId(dto.CompanyId)
                .WithIsActive(dto.IsActive);

            return builder;
        }
    }
}
