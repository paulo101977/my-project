﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a Crudzilla tool.
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using Thex.Domain.Entities;

using Thex.Dto;
using Tnf;
using Tnf.Notifications;

namespace Thex.Application.Adapters
{
    public abstract class ScaffoldPropertyGuestPrefsAdapter : IScaffoldPropertyGuestPrefsAdapter
    {
        public INotificationHandler NotificationHandler { get; }

        public ScaffoldPropertyGuestPrefsAdapter(INotificationHandler notificationHandler)
        {
            NotificationHandler = notificationHandler;
        }

        public virtual PropertyGuestPrefs.Builder Map(PropertyGuestPrefs entity, PropertyGuestPrefsDto dto)
        {
            Check.NotNull(entity, nameof(entity));
            Check.NotNull(dto, nameof(dto));

            var builder = new PropertyGuestPrefs.Builder(NotificationHandler, entity)
                .WithId(dto.Id)
                .WithGuestId(dto.GuestId)
                .WithPropertyId(dto.PropertyId)
                .WithPreferredRoomId(dto.PreferredRoomId)
                .WithLastRoomId(dto.LastRoomId);

            return builder;
        }

        public virtual PropertyGuestPrefs.Builder Map(PropertyGuestPrefsDto dto)
        {
            Check.NotNull(dto, nameof(dto));

            var builder = new PropertyGuestPrefs.Builder(NotificationHandler)
                .WithId(dto.Id)
                .WithGuestId(dto.GuestId)
                .WithPropertyId(dto.PropertyId)
                .WithPreferredRoomId(dto.PreferredRoomId)
                .WithLastRoomId(dto.LastRoomId);

            return builder;
        }
    }
}
