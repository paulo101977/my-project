﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a Crudzilla tool.
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using Thex.Domain.Entities;

using Thex.Dto;
using Tnf;
using Tnf.Notifications;

namespace Thex.Application.Adapters
{
    public abstract class ScaffoldReasonAdapter : IScaffoldReasonAdapter
    {
        public INotificationHandler NotificationHandler { get; }

        public ScaffoldReasonAdapter(INotificationHandler notificationHandler)
        {
            NotificationHandler = notificationHandler;
        }

        public virtual Reason.Builder Map(Reason entity, ReasonDto dto)
        {
            Check.NotNull(entity, nameof(entity));
            Check.NotNull(dto, nameof(dto));

            var builder = new Reason.Builder(NotificationHandler, entity)
                .WithId(dto.Id)
                .WithReasonName(dto.ReasonName)
                .WithReasonCategoryId(dto.ReasonCategoryId)
                .WithIsActive(dto.IsActive)
                .WithChainId(dto.ChainId);

            return builder;
        }

        public virtual Reason.Builder Map(ReasonDto dto)
        {
            Check.NotNull(dto, nameof(dto));

            var builder = new Reason.Builder(NotificationHandler)
                .WithId(dto.Id)
                .WithReasonName(dto.ReasonName)
                .WithReasonCategoryId(dto.ReasonCategoryId)
                .WithIsActive(dto.IsActive)
                .WithChainId(dto.ChainId);

            return builder;
        }
    }
}
