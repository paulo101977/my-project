﻿using System;
using System.Collections.Generic;
using System.Text;
using Thex.Domain.Entities;
using Thex.Dto;
using Tnf;
using Tnf.Notifications;

namespace Thex.Application.Adapters
{
    public abstract class ScaffoldGuestRegistrationDocumentAdapter : IScaffoldGuestRegistrationDocumentAdapter
    {
        public INotificationHandler NotificationHandler { get; }

        public ScaffoldGuestRegistrationDocumentAdapter(INotificationHandler notificationHandler)
        {
            NotificationHandler = notificationHandler;
        }

        public virtual GuestRegistrationDocument.Builder Map(GuestRegistrationDocument entity, GuestRegistrationDocumentDto dto)
        {
            Check.NotNull(entity, nameof(entity));
            Check.NotNull(dto, nameof(dto));

            var builder = new GuestRegistrationDocument.Builder(NotificationHandler, entity)
                .WithId(entity.Id)
                .WithDocumentInformation(dto.DocumentInformation)
                .WithDocumentTypeId(dto.DocumentTypeId)
                .WithGuestRegistrationId(entity.GuestRegistrationId);


            return builder;
        }

        public virtual GuestRegistrationDocument.Builder Map(GuestRegistrationDocumentDto dto)
        {
            Check.NotNull(dto, nameof(dto));

            var builder = new GuestRegistrationDocument.Builder(NotificationHandler)
                .WithId(Guid.NewGuid())
                .WithDocumentInformation(dto.DocumentInformation)
                .WithDocumentTypeId(dto.DocumentTypeId)
                .WithGuestRegistrationId(dto.GuestRegistrationId);

            return builder;
        }
    }
}
