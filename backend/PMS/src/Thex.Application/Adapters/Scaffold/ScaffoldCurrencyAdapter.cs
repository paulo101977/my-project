﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a Crudzilla tool.
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using Thex.Domain.Entities;
using Thex.Dto;
using Tnf;
using Tnf.Notifications;

namespace Thex.Application.Adapters
{
    public abstract class ScaffoldCurrencyAdapter : IScaffoldCurrencyAdapter
    {
        public INotificationHandler NotificationHandler { get; }

        public ScaffoldCurrencyAdapter(INotificationHandler notificationHandler)
        {
            NotificationHandler = notificationHandler;
        }

        public virtual Currency.Builder Map(Currency entity, CurrencyDto dto)
        {
            Check.NotNull(entity, nameof(entity));
            Check.NotNull(dto, nameof(dto));

            var builder = Currency.Create(NotificationHandler, entity)
                .WithId(dto.Id)
                //.WithCountrySubdivisionId(dto.CountrySubdivisionId)
                .WithTwoLetterIsoCode(dto.TwoLetterIsoCode)
                .WithCurrencyName(dto.CurrencyName)
                .WithAlphabeticCode(dto.AlphabeticCode)
                .WithNumnericCode(dto.NumnericCode)
                .WithMinorUnit(dto.MinorUnit)
                .WithSymbol(dto.Symbol)
                .WithExchangeRate(dto.ExchangeRate);

            return builder;
        }

        public virtual Currency.Builder Map(CurrencyDto dto)
        {
            Check.NotNull(dto, nameof(dto));

            var builder = Currency.Create(NotificationHandler)
                .WithId(dto.Id)
                //.WithCountrySubdivisionId(dto.CountrySubdivisionId)
                .WithTwoLetterIsoCode(dto.TwoLetterIsoCode)
                .WithCurrencyName(dto.CurrencyName)
                .WithAlphabeticCode(dto.AlphabeticCode)
                .WithNumnericCode(dto.NumnericCode)
                .WithMinorUnit(dto.MinorUnit)
                .WithSymbol(dto.Symbol)
                .WithExchangeRate(dto.ExchangeRate);

            return builder;
        }
    }
}
