﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a Crudzilla tool.
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using Thex.Domain.Entities;

using Thex.Dto;
using Tnf;
using Tnf.Notifications;

namespace Thex.Application.Adapters
{
    public abstract class ScaffoldApplicationModuleAdapter : IScaffoldApplicationModuleAdapter
    {
        public INotificationHandler NotificationHandler { get; }

        public ScaffoldApplicationModuleAdapter(INotificationHandler notificationHandler)
        {
            NotificationHandler = notificationHandler;
        }

        public virtual ApplicationModule.Builder Map(ApplicationModule entity, ApplicationModuleDto dto)
        {
            Check.NotNull(entity, nameof(entity));
            Check.NotNull(dto, nameof(dto));

            var builder = new ApplicationModule.Builder(NotificationHandler, entity)
                .WithId(dto.Id)
                .WithApplicationModuleName(dto.ApplicationModuleName)
                .WithApplicationModuleDescription(dto.ApplicationModuleDescription);

            return builder;
        }

        public virtual ApplicationModule.Builder Map(ApplicationModuleDto dto)
        {
            Check.NotNull(dto, nameof(dto));

            var builder = new ApplicationModule.Builder(NotificationHandler)
                .WithId(dto.Id)
                .WithApplicationModuleName(dto.ApplicationModuleName)
                .WithApplicationModuleDescription(dto.ApplicationModuleDescription);

            return builder;
        }
    }
}
