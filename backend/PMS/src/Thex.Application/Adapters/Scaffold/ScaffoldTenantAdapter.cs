﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a Crudzilla tool.
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using Thex.Domain.Entities;

using Thex.Dto;
using Tnf;
using Tnf.Notifications;

namespace Thex.Application.Adapters
{
    public abstract class ScaffoldTenantAdapter : IScaffoldTenantAdapter
    {
        public INotificationHandler NotificationHandler { get; }

        public ScaffoldTenantAdapter(INotificationHandler notificationHandler)
        {
            NotificationHandler = notificationHandler;
        }

        public virtual Tenant.Builder Map(Tenant entity, TenantDto dto)
        {
            Check.NotNull(entity, nameof(entity));
            Check.NotNull(dto, nameof(dto));

            var builder = new Tenant.Builder(NotificationHandler, entity)
                .WithId(dto.Id)
                .WithChainId(dto.ChainId)
                .WithPropertyId(dto.PropertyId)
                .WithTenantName(dto.TenantName)
                .WithParentId(dto.ParentId)
                .WithIsActive(dto.IsActive);

            return builder;
        }

        public virtual Tenant.Builder Map(TenantDto dto)
        {
            Check.NotNull(dto, nameof(dto));

            var builder = new Tenant.Builder(NotificationHandler)
                .WithId(dto.Id)
                .WithChainId(dto.ChainId)
                .WithPropertyId(dto.PropertyId)
                .WithTenantName(dto.TenantName)
                .WithParentId(dto.ParentId)
                .WithIsActive(dto.IsActive);

            return builder;
        }
    }
}
