﻿using Thex.Domain.Entities;
using Thex.Dto;
using Tnf;
using Tnf.Notifications;

namespace Thex.Application.Adapters.Scaffold
{
    public class ScaffoldPropertyRateStrategyAdapter : IScaffoldPropertyRateStrategyAdapter
    {
        public INotificationHandler NotificationHandler { get; }

        public ScaffoldPropertyRateStrategyAdapter(INotificationHandler notificationHandler)
        {
            NotificationHandler = notificationHandler;
        }

        public virtual PropertyRateStrategy.Builder Map(PropertyRateStrategy entity, PropertyRateStrategyDto dto)
        {
            Check.NotNull(entity, nameof(entity));
            Check.NotNull(dto, nameof(dto));

            var builder = new PropertyRateStrategy.Builder(NotificationHandler, entity)
                .WithId(entity.Id)
                .WithIsActive(dto.IsActive)
                .WithName(dto.Name)
                .WithStartDate(dto.StartDate)
                .WithEndDate(dto.EndDate);

            return builder;
        }

        public virtual PropertyRateStrategy.Builder Map(PropertyRateStrategyDto dto)
        {
            Check.NotNull(dto, nameof(dto));

            var builder = new PropertyRateStrategy.Builder(NotificationHandler)
                .WithId(dto.Id)
                .WithIsActive(dto.IsActive)
                .WithName(dto.Name)
                .WithStartDate(dto.StartDate)
                .WithEndDate(dto.EndDate);

            return builder;
        }
    }
}
