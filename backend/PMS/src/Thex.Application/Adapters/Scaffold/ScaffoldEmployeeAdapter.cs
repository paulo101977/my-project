﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a Crudzilla tool.
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using Thex.Domain.Entities;

using Thex.Dto;
using Tnf;
using Tnf.Notifications;

namespace Thex.Application.Adapters
{
    public abstract class ScaffoldEmployeeAdapter : IScaffoldEmployeeAdapter
    {
        public INotificationHandler NotificationHandler { get; }

        public ScaffoldEmployeeAdapter(INotificationHandler notificationHandler)
        {
            NotificationHandler = notificationHandler;
        }

        public virtual Employee.Builder Map(Employee entity, EmployeeDto dto)
        {
            Check.NotNull(entity, nameof(entity));
            Check.NotNull(dto, nameof(dto));

            var builder = new Employee.Builder(NotificationHandler, entity)
                .WithId(dto.Id)
                .WithPersonId(dto.PersonId)
                .WithOccupationId(dto.OccupationId);

            return builder;
        }

        public virtual Employee.Builder Map(EmployeeDto dto)
        {
            Check.NotNull(dto, nameof(dto));

            var builder = new Employee.Builder(NotificationHandler)
                .WithId(dto.Id)
                .WithPersonId(dto.PersonId)
                .WithOccupationId(dto.OccupationId);

            return builder;
        }
    }
}
