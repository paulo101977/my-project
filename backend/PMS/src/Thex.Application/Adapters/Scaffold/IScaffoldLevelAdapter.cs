﻿
using Thex.Domain.Entities;
using Thex.Dto;

namespace Thex.Application.Adapters
{
    public interface IScaffoldLevelAdapter
    {
        Level.Builder Map(Level entity, LevelDto dto);
        Level.Builder Map(LevelDto dto);
    }
}
