﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a Crudzilla tool.
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using Thex.Domain.Entities;

using Thex.Dto;
using Tnf;
using Tnf.Notifications;

namespace Thex.Application.Adapters
{
    public abstract class ScaffoldGuestAdapter : IScaffoldGuestAdapter
    {
        public INotificationHandler NotificationHandler { get; }

        public ScaffoldGuestAdapter(INotificationHandler notificationHandler)
        {
            NotificationHandler = notificationHandler;
        }

        public virtual Guest.Builder Map(Guest entity, GuestDto dto)
        {
            Check.NotNull(entity, nameof(entity));
            Check.NotNull(dto, nameof(dto));

            var builder = new Guest.Builder(NotificationHandler, entity)
                .WithId(dto.Id)
                .WithPersonId(dto.PersonId);

            return builder;
        }

        public virtual Guest.Builder Map(GuestDto dto)
        {
            Check.NotNull(dto, nameof(dto));

            var builder = new Guest.Builder(NotificationHandler)
                .WithId(dto.Id)
                .WithPersonId(dto.PersonId);

            return builder;
        }
    }
}
