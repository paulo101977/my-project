﻿using Thex.Domain.Entities;
using Thex.Dto;

namespace Thex.Application.Adapters
{
    public interface IScaffoldCompanyClientChannelAdapter
    {
        CompanyClientChannel.Builder Map(CompanyClientChannel entity, CompanyClientChannelDto dto);
        CompanyClientChannel.Builder Map(CompanyClientChannelDto dto);
    }
}
