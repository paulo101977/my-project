﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a Crudzilla tool.
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using Thex.Domain.Entities;

using Thex.Dto;
using Tnf;
using Tnf.Notifications;

namespace Thex.Application.Adapters
{
    public abstract class ScaffoldRoomTypeBedTypeAdapter : IScaffoldRoomTypeBedTypeAdapter
    {
        public INotificationHandler NotificationHandler { get; }

        public ScaffoldRoomTypeBedTypeAdapter(INotificationHandler notificationHandler)
        {
            NotificationHandler = notificationHandler;
        }

        public virtual RoomTypeBedType.Builder Map(RoomTypeBedType entity, RoomTypeBedTypeDto dto)
        {
            Check.NotNull(entity, nameof(entity));
            Check.NotNull(dto, nameof(dto));

            var builder = new RoomTypeBedType.Builder(NotificationHandler, entity)
                .WithId(dto.Id)
                .WithRoomTypeId(dto.RoomTypeId)
                .WithBedTypeId(dto.BedTypeId)
                .WithCount(dto.Count)
                .WithOptionalLimit(dto.OptionalLimit);

            return builder;
        }

        public virtual RoomTypeBedType.Builder Map(RoomTypeBedTypeDto dto)
        {
            Check.NotNull(dto, nameof(dto));

            var builder = new RoomTypeBedType.Builder(NotificationHandler)
                .WithId(dto.Id)
                .WithRoomTypeId(dto.RoomTypeId)
                .WithBedTypeId(dto.BedTypeId)
                .WithCount(dto.Count)
                .WithOptionalLimit(dto.OptionalLimit);

            return builder;
        }
    }
}
