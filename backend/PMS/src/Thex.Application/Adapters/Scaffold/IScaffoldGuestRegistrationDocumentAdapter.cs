﻿using System;
using System.Collections.Generic;
using System.Text;
using Thex.Domain.Entities;
using Thex.Dto;

namespace Thex.Application.Adapters
{
    public interface IScaffoldGuestRegistrationDocumentAdapter
    {
        GuestRegistrationDocument.Builder Map(GuestRegistrationDocument entity, GuestRegistrationDocumentDto dto);
        GuestRegistrationDocument.Builder Map(GuestRegistrationDocumentDto dto);
    }
}
