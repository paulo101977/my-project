﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a Crudzilla tool.
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using Thex.Domain.Entities;
using Thex.Dto;


namespace Thex.Application.Adapters
{
    public interface IScaffoldCountryCurrencyAdapter
    {
        CountryCurrency.Builder Map(CountryCurrency entity, CountryCurrencyDto dto);
        CountryCurrency.Builder Map(CountryCurrencyDto dto);
    }
}
