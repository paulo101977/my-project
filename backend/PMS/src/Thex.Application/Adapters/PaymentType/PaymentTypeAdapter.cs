﻿using Thex.Domain.Entities;
using Thex.Dto;

using Tnf.Notifications;

namespace Thex.Application.Adapters
{
    public class PaymentTypeAdapter : ScaffoldPaymentTypeAdapter, IPaymentTypeAdapter
    {
        public PaymentTypeAdapter(INotificationHandler notificationHandler)
            : base(notificationHandler)
        {
        }

        public override PaymentType.Builder Map(PaymentType entity, PaymentTypeDto dto)
        {
            var builder = base.Map(entity, dto);
            return builder;
        }

        public override PaymentType.Builder Map(PaymentTypeDto dto)
        {
            var builder = base.Map(dto);
            return builder;
        }
    }
}
