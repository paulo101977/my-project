﻿using Tnf.Dependency;

namespace Thex.Application.Adapters
{
    public interface IPaymentTypeAdapter : IScaffoldPaymentTypeAdapter, ITransientDependency
    {
    }
}
