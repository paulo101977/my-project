﻿using Thex.Domain.Entities;
using Thex.Dto;

using Tnf.Notifications;

namespace Thex.Application.Adapters
{
    public class BrandAdapter : ScaffoldBrandAdapter, IBrandAdapter
    {
        public BrandAdapter(INotificationHandler notificationHandler)
            : base(notificationHandler)
        {
        }

        public override Brand.Builder Map(Brand entity, BrandDto dto)
        {
            var builder = base.Map(entity, dto);
            return builder;
        }

        public override Brand.Builder Map(BrandDto dto)
        {
            var builder = base.Map(dto);
            return builder;
        }
    }
}
