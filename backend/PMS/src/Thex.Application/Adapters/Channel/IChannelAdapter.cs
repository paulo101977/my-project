﻿using Tnf.Dependency;

namespace Thex.Application.Adapters
{
    public interface IChannelAdapter : IScaffoldChannelAdapter, ITransientDependency
    {
    }
}
