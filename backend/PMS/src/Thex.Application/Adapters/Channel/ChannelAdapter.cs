﻿using Thex.Domain.Entities;
using Thex.Dto;
using Tnf.Notifications;

namespace Thex.Application.Adapters
{
    public class ChannelAdapter : ScaffoldChannelAdapter, IChannelAdapter
    {
        public ChannelAdapter(INotificationHandler notificationHandler)
            : base(notificationHandler)
        {
        }

        public override Channel.Builder Map(Channel entity, ChannelDto dto)
        {
            var builder = base.Map(entity, dto);
            return builder;
        }

        public override Channel.Builder Map(ChannelDto dto)
        {
            var builder = base.Map(dto);
            return builder;
        }
    }
}
