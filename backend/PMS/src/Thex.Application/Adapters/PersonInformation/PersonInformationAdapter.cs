﻿using Thex.Domain.Entities;
using Thex.Dto;

using Tnf.Notifications;

namespace Thex.Application.Adapters
{
    public class PersonInformationAdapter : ScaffoldPersonInformationAdapter, IPersonInformationAdapter
    {
        public PersonInformationAdapter(INotificationHandler notificationHandler)
            : base(notificationHandler)
        {
        }

        public override PersonInformation.Builder Map(PersonInformation entity, PersonInformationDto dto)
        {
            var builder = base.Map(entity, dto);
            return builder;
        }

        public override PersonInformation.Builder Map(PersonInformationDto dto)
        {
            var builder = base.Map(dto);
            return builder;
        }
    }
}
