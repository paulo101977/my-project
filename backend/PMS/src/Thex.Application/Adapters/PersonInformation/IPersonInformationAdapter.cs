﻿using Tnf.Dependency;

namespace Thex.Application.Adapters
{
    public interface IPersonInformationAdapter : IScaffoldPersonInformationAdapter, ITransientDependency
    {
    }
}
