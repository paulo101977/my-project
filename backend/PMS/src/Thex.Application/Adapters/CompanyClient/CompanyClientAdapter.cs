﻿using Thex.Domain.Entities;
using Thex.Dto;

using Tnf.Notifications;
using System;
using Tnf;

namespace Thex.Application.Adapters
{
    public class CompanyClientAdapter : ScaffoldCompanyClientAdapter, ICompanyClientAdapter
    {
        public CompanyClientAdapter(INotificationHandler notificationHandler)
            : base(notificationHandler)
        {
        }

        public override CompanyClient.Builder Map(CompanyClient entity, CompanyClientDto dto)
        {
            var builder = base.Map(entity, dto);
            return builder;
        }

        public override CompanyClient.Builder Map(CompanyClientDto dto)
        {
            var builder = base.Map(dto);
            return builder;
        }

        public virtual CompanyClient.Builder Map(CompanyClientDto dto, Guid personId, int companyId)
        {
            Check.NotNull(dto, nameof(dto));

            if (!Guid.TryParse(dto.Id, out Guid guid))
                guid = Guid.NewGuid();

            var builder = new CompanyClient.Builder(NotificationHandler)
                .WithId(guid)
                .WithCompanyId(companyId)
                .WithPersonId(personId)
                .WithShortName(dto.ShortName)
                .WithTradeName(dto.TradeName)
                .WithIsActive(dto.IsActive)
                .WithCountrySubdivisionId(dto.CountrySubdivisionId)
                .WithPersonType(dto.PersonType.ToString())
                .WithPropertyCompanyClientCategoryId(dto.PropertyCompanyClientCategoryId.HasValue ? dto.PropertyCompanyClientCategoryId.Value : (Guid?)null)
                .WithIsAcquirer(dto.IsAcquirer)
                .WithExternalCode(dto.ExternalCode)
                .WithDateOfBirth(dto.DateOfBirth);
                
                

            return builder;
        }
    }
}
