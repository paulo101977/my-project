﻿using System;
using Thex.Domain.Entities;
using Thex.Dto;
using Tnf.Dependency;

namespace Thex.Application.Adapters
{
    public interface ICompanyClientAdapter : IScaffoldCompanyClientAdapter, ITransientDependency
    {
        CompanyClient.Builder Map(CompanyClientDto dto, Guid personId, int companyId);
    }
}
