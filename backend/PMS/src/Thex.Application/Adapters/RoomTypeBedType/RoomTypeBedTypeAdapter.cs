﻿using Thex.Domain.Entities;
using Thex.Dto;

using Tnf.Notifications;

namespace Thex.Application.Adapters
{
    public class RoomTypeBedTypeAdapter : ScaffoldRoomTypeBedTypeAdapter, IRoomTypeBedTypeAdapter
    {
        public RoomTypeBedTypeAdapter(INotificationHandler notificationHandler)
            : base(notificationHandler)
        {
        }

        public override RoomTypeBedType.Builder Map(RoomTypeBedType entity, RoomTypeBedTypeDto dto)
        {
            var builder = base.Map(entity, dto);
            return builder;
        }

        public override RoomTypeBedType.Builder Map(RoomTypeBedTypeDto dto)
        {
            var builder = base.Map(dto);
            return builder;
        }
    }
}
