﻿using Tnf.Dependency;

namespace Thex.Application.Adapters
{
    public interface IRoomTypeBedTypeAdapter : IScaffoldRoomTypeBedTypeAdapter, ITransientDependency
    {
    }
}
