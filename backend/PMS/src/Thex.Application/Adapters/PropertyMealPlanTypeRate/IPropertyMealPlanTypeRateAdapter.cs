﻿using Tnf.Dependency;

namespace Thex.Application.Adapters
{
    public interface IPropertyMealPlanTypeRateAdapter : IScaffoldPropertyMealPlanTypeRateAdapter, ITransientDependency
    {
    }
}
