﻿using Thex.Domain.Entities;
using Thex.Dto;
using Tnf.Notifications;

namespace Thex.Application.Adapters
{
    public class PropertyMealPlanTypeRateAdapter : ScaffoldPropertyMealPlanTypeRateAdapter, IPropertyMealPlanTypeRateAdapter
    {
        public PropertyMealPlanTypeRateAdapter(INotificationHandler notificationHandler)
            : base(notificationHandler)
        {
        }

        public override PropertyMealPlanTypeRate.Builder Map(PropertyMealPlanTypeRate entity, PropertyMealPlanTypeRateDto dto)
        {
            var builder = base.Map(entity, dto);
            return builder;
        }

        public override PropertyMealPlanTypeRate.Builder Map(PropertyMealPlanTypeRateDto dto)
        {
            var builder = base.Map(dto);
            return builder;
        }
    }
}
