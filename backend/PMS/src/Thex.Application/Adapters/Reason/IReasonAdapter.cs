﻿using Tnf.Dependency;

namespace Thex.Application.Adapters
{
    public interface IReasonAdapter : IScaffoldReasonAdapter, ITransientDependency
    {
    }
}
