﻿using Thex.Domain.Entities;
using Thex.Dto;

using Tnf.Notifications;

namespace Thex.Application.Adapters
{
    public class ReasonAdapter : ScaffoldReasonAdapter, IReasonAdapter
    {
        public ReasonAdapter(INotificationHandler notificationHandler)
            : base(notificationHandler)
        {
        }

        public override Reason.Builder Map(Reason entity, ReasonDto dto)
        {
            var builder = base.Map(entity, dto);
            return builder;
        }

        public override Reason.Builder Map(ReasonDto dto)
        {
            var builder = base.Map(dto);
            return builder;
        }
    }
}
