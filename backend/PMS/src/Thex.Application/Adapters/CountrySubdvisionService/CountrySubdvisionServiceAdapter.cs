﻿using Thex.Domain.Entities;
using Thex.Dto;

using Tnf.Notifications;

namespace Thex.Application.Adapters
{
    public class CountrySubdvisionServiceAdapter : ScaffoldCountrySubdvisionServiceAdapter, ICountrySubdvisionServiceAdapter
    {
        public CountrySubdvisionServiceAdapter(INotificationHandler notificationHandler)
            : base(notificationHandler)
        {
        }

        public override CountrySubdvisionService.Builder Map(CountrySubdvisionService entity, CountrySubdvisionServiceDto dto)
        {
            var builder = base.Map(entity, dto);
            return builder;
        }

        public override CountrySubdvisionService.Builder Map(CountrySubdvisionServiceDto dto)
        {
            var builder = base.Map(dto);
            return builder;
        }
    }
}
