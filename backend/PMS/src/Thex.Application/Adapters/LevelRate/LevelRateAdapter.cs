﻿using Thex.Domain.Entities;
using Thex.Dto;
using Thex.Kernel;
using Tnf.Notifications;

namespace Thex.Application.Adapters
{
    public class LevelRateAdapter : ScaffoldLevelRateAdapter, ILevelRateAdapter
    {
        public LevelRateAdapter(
            INotificationHandler notificationHandler,
            IApplicationUser applicationUser)
            : base(notificationHandler, applicationUser)
        {
        }

        public override LevelRate.Builder Map(LevelRate entity, LevelRateDto dto)
        {
            var builder = base.Map(entity, dto);
            return builder;
        }

        public override LevelRate.Builder Map(LevelRateDto dto)
        {
            var builder = base.Map(dto);
            return builder;
        }
    }
}
