﻿using Tnf.Dependency;

namespace Thex.Application.Adapters
{
    public interface ILevelRateAdapter : IScaffoldLevelRateAdapter, ITransientDependency
    {
    }
}
