﻿using Tnf.Dependency;

namespace Thex.Application.Adapters
{
    public interface IFeatureGroupAdapter : IScaffoldFeatureGroupAdapter, ITransientDependency
    {
    }
}
