﻿using Thex.Domain.Entities;
using Thex.Dto;

using Tnf.Notifications;

namespace Thex.Application.Adapters
{
    public class FeatureGroupAdapter : ScaffoldFeatureGroupAdapter, IFeatureGroupAdapter
    {
        public FeatureGroupAdapter(INotificationHandler notificationHandler)
            : base(notificationHandler)
        {
        }

        public override FeatureGroup.Builder Map(FeatureGroup entity, FeatureGroupDto dto)
        {
            var builder = base.Map(entity, dto);
            return builder;
        }

        public override FeatureGroup.Builder Map(FeatureGroupDto dto)
        {
            var builder = base.Map(dto);
            return builder;
        }
    }
}
