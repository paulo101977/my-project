﻿using System;
using System.Collections.Generic;
using Thex.Domain.Entities;
using Thex.Dto;
using Tnf;
using Tnf.Notifications;

namespace Thex.Application.Adapters
{
    public class PropertyBaseRateHeaderHistoryAdapter : ScaffoldPropertyBaseRateHeaderHistoryAdapter, IPropertyBaseRateHeaderHistoryAdapter
    {
        private readonly PropertyBaseRateHistoryAdapter _propertyBaseRateHistoryAdapter;

        public PropertyBaseRateHeaderHistoryAdapter(
            PropertyBaseRateHistoryAdapter propertyBaseRateHistoryAdapter,
            INotificationHandler notificationHandler)
            : base(notificationHandler)
        {
            _propertyBaseRateHistoryAdapter = propertyBaseRateHistoryAdapter;
        }

        public override PropertyBaseRateHeaderHistory.Builder Map(PropertyBaseRateHeaderHistory entity, PropertyBaseRateHeaderHistoryDto dto)
        {
            var builder = base.Map(entity, dto);
            return builder;
        }

        public override PropertyBaseRateHeaderHistory.Builder Map(PropertyBaseRateHeaderHistoryDto dto)
        {
            var builder = base.Map(dto);
            return builder;
        }

        public PropertyBaseRateHeaderHistory.Builder MapWithHistory(PropertyBaseRateHeaderHistoryDto dto)
        {
            Check.NotNull(dto, nameof(dto));

            var historyList = new List<PropertyBaseRateHistory>();

            foreach (var propertyBaseRateHistoryDto in dto.PropertyBaseRateHistoryList)
                historyList.Add(_propertyBaseRateHistoryAdapter.Map(propertyBaseRateHistoryDto).Build());

            var builder = new PropertyBaseRateHeaderHistory.Builder(NotificationHandler)
                .WithId(Guid.NewGuid())
                .WithPropertyId(dto.PropertyId)
                .WithInitialDate(dto.InitialDate)
                .WithFinalDate(dto.FinalDate)
                .WithCurrencyId(dto.CurrencyId)
                .WithMealPlanTypeDefaultId(dto.MealPlanTypeDefaultId)
                .WithRatePlanId(dto.RatePlanId)
                .WithSunday(dto.Sunday)
                .WithMonday(dto.Monday)
                .WithTuesday(dto.Tuesday)
                .WithWednesday(dto.Wednesday)
                .WithThursday(dto.Thursday)
                .WithLevelId(dto.LevelId)
                .WithLevelRateId(dto.LevelRateId)
                .WithFriday(dto.Friday)
                .WithSaturday(dto.Saturday)
                .WithPropertyBaseRateHistoryList(historyList);

            return builder;
        }

    }
}
