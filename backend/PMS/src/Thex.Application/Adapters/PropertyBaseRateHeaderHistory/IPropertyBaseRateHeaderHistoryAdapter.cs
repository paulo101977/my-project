﻿using System.Collections.Generic;
using Thex.Domain.Entities;
using Thex.Dto;
using Tnf.Dependency;

namespace Thex.Application.Adapters
{
    public interface IPropertyBaseRateHeaderHistoryAdapter : IScaffoldPropertyBaseRateHeaderHistoryAdapter, ITransientDependency
    {
        PropertyBaseRateHeaderHistory.Builder MapWithHistory(PropertyBaseRateHeaderHistoryDto dto);
    }
}
