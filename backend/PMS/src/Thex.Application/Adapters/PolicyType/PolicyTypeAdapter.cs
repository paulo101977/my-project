﻿using Thex.Domain.Entities;
using Thex.Dto;

using Tnf.Notifications;

namespace Thex.Application.Adapters
{
    public class PolicyTypeAdapter : ScaffoldPolicyTypeAdapter, IPolicyTypeAdapter
    {
        public PolicyTypeAdapter(INotificationHandler notificationHandler)
            : base(notificationHandler)
        {
        }

        public override PolicyType.Builder Map(PolicyType entity, PolicyTypeDto dto)
        {
            var builder = base.Map(entity, dto);
            return builder;
        }

        public override PolicyType.Builder Map(PolicyTypeDto dto)
        {
            var builder = base.Map(dto);
            return builder;
        }
    }
}
