﻿using Tnf.Dependency;

namespace Thex.Application.Adapters
{
    public interface IPolicyTypeAdapter : IScaffoldPolicyTypeAdapter, ITransientDependency
    {
    }
}
