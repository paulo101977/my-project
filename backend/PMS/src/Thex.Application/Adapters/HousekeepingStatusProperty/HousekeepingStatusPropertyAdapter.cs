﻿using Thex.Domain.Entities;
using Thex.Dto;

using Tnf.Notifications;

namespace Thex.Application.Adapters
{
    public class HousekeepingStatusPropertyAdapter : ScaffoldHousekeepingStatusPropertyAdapter, IHousekeepingStatusPropertyAdapter
    {
        public HousekeepingStatusPropertyAdapter(INotificationHandler notificationHandler)
            : base(notificationHandler)
        {
        }

        public override HousekeepingStatusProperty.Builder Map(HousekeepingStatusProperty entity, HousekeepingStatusPropertyDto dto)
        {
            var builder = base.Map(entity, dto);
            return builder;
        }

        public override HousekeepingStatusProperty.Builder Map(HousekeepingStatusPropertyDto dto)
        {
            var builder = base.Map(dto);
            return builder;
        }
    }
}
