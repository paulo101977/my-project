﻿using Tnf.Dependency;

namespace Thex.Application.Adapters
{
    public interface IHousekeepingStatusPropertyAdapter : IScaffoldHousekeepingStatusPropertyAdapter, ITransientDependency
    {
    }
}
