﻿using Thex.Domain.Entities;
using Thex.Dto;

using Tnf.Notifications;

namespace Thex.Application.Adapters
{
    public class AuditStepTypeAdapter : ScaffoldAuditStepTypeAdapter, IAuditStepTypeAdapter
    {
        public AuditStepTypeAdapter(INotificationHandler notificationHandler)
            : base(notificationHandler)
        {
        }

        public override AuditStepType.Builder Map(AuditStepType entity, AuditStepTypeDto dto)
        {
            var builder = base.Map(entity, dto);
            return builder;
        }

        public override AuditStepType.Builder Map(AuditStepTypeDto dto)
        {
            var builder = base.Map(dto);
            return builder;
        }
    }
}
