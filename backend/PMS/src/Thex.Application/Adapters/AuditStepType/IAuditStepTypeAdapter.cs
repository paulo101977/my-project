﻿using Tnf.Dependency;

namespace Thex.Application.Adapters
{
    public interface IAuditStepTypeAdapter : IScaffoldAuditStepTypeAdapter, ITransientDependency
    {
    }
}
