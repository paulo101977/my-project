﻿using Tnf.Dependency;

namespace Thex.Application.Adapters
{
    public interface IRoomLayoutAdapter : IScaffoldRoomLayoutAdapter, ITransientDependency
    {
    }
}
