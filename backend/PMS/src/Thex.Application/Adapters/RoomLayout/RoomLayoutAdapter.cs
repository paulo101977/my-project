﻿using Thex.Domain.Entities;
using Thex.Dto;

using Tnf.Notifications;

namespace Thex.Application.Adapters
{
    public class RoomLayoutAdapter : ScaffoldRoomLayoutAdapter, IRoomLayoutAdapter
    {
        public RoomLayoutAdapter(INotificationHandler notificationHandler)
            : base(notificationHandler)
        {
        }

        public override RoomLayout.Builder Map(RoomLayout entity, RoomLayoutDto dto)
        {
            var builder = base.Map(entity, dto);
            return builder;
        }

        public override RoomLayout.Builder Map(RoomLayoutDto dto)
        {
            var builder = base.Map(dto);
            return builder;
        }
    }
}
