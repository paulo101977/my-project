﻿using Thex.Domain.Entities;
using Thex.Dto;

using Tnf.Notifications;

namespace Thex.Application.Adapters
{
    public class BusinessSourceAdapter : ScaffoldBusinessSourceAdapter, IBusinessSourceAdapter
    {
        public BusinessSourceAdapter(INotificationHandler notificationHandler)
            : base(notificationHandler)
        {
        }

        public override BusinessSource.Builder Map(BusinessSource entity, BusinessSourceDto dto)
        {
            var builder = base.Map(entity, dto);
            return builder;
        }

        public override BusinessSource.Builder Map(BusinessSourceDto dto)
        {
            var builder = base.Map(dto);
            return builder;
        }
    }
}
