﻿using Tnf.Dependency;

namespace Thex.Application.Adapters
{
    public interface IBusinessSourceAdapter : IScaffoldBusinessSourceAdapter, ITransientDependency
    {
    }
}
