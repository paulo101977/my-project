﻿using Thex.Domain.Entities;
using Thex.Dto;

using Tnf.Notifications;

namespace Thex.Application.Adapters
{
    public class CountryLanguageAdapter : ScaffoldCountryLanguageAdapter, ICountryLanguageAdapter
    {
        public CountryLanguageAdapter(INotificationHandler notificationHandler)
            : base(notificationHandler)
        {
        }

        public override CountryLanguage.Builder Map(CountryLanguage entity, CountryLanguageDto dto)
        {
            var builder = base.Map(entity, dto);
            return builder;
        }

        public override CountryLanguage.Builder Map(CountryLanguageDto dto)
        {
            var builder = base.Map(dto);
            return builder;
        }
    }
}
