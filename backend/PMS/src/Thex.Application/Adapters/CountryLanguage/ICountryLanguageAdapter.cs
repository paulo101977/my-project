﻿using Tnf.Dependency;

namespace Thex.Application.Adapters
{
    public interface ICountryLanguageAdapter : IScaffoldCountryLanguageAdapter, ITransientDependency
    {
    }
}
