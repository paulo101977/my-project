﻿using Tnf.Dependency;

namespace Thex.Application.Adapters
{
    public interface IRoomAdapter : IScaffoldRoomAdapter, ITransientDependency
    {
    }
}
