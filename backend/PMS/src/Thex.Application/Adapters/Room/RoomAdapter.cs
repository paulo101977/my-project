﻿using Thex.Domain.Entities;
using Thex.Dto;

using Tnf.Notifications;

namespace Thex.Application.Adapters
{
    public class RoomAdapter : ScaffoldRoomAdapter, IRoomAdapter
    {
        public RoomAdapter(INotificationHandler notificationHandler)
            : base(notificationHandler)
        {
        }

        public override Room.Builder Map(Room entity, RoomDto dto)
        {
            var builder = base.Map(entity, dto);
            return builder;
        }

        public override Room.Builder Map(RoomDto dto)
        {
            var builder = base.Map(dto);
            return builder;
        }
    }
}
