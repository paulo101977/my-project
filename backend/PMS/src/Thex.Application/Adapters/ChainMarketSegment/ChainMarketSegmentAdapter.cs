﻿using Thex.Domain.Entities;
using Thex.Dto;

using Tnf.Notifications;

namespace Thex.Application.Adapters
{
    public class ChainMarketSegmentAdapter : ScaffoldChainMarketSegmentAdapter, IChainMarketSegmentAdapter
    {
        public ChainMarketSegmentAdapter(INotificationHandler notificationHandler)
            : base(notificationHandler)
        {
        }

        public override ChainMarketSegment.Builder Map(ChainMarketSegment entity, ChainMarketSegmentDto dto)
        {
            var builder = base.Map(entity, dto);
            return builder;
        }

        public override ChainMarketSegment.Builder Map(ChainMarketSegmentDto dto)
        {
            var builder = base.Map(dto);
            return builder;
        }
    }
}
