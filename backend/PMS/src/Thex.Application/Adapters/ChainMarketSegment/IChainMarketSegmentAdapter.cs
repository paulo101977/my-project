﻿using Tnf.Dependency;

namespace Thex.Application.Adapters
{
    public interface IChainMarketSegmentAdapter : IScaffoldChainMarketSegmentAdapter, ITransientDependency
    {
    }
}
