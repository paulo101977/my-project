﻿using Tnf.Dependency;

namespace Thex.Application.Adapters
{
    public interface IRoomTypeAdapter : IScaffoldRoomTypeAdapter, ITransientDependency
    {
    }
}
