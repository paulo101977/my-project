﻿using Thex.Domain.Entities;
using Thex.Dto;

using Tnf.Notifications;

namespace Thex.Application.Adapters
{
    public class RoomTypeAdapter : ScaffoldRoomTypeAdapter, IRoomTypeAdapter
    {
        public RoomTypeAdapter(INotificationHandler notificationHandler)
            : base(notificationHandler)
        {
        }

        public override RoomType.Builder Map(RoomType entity, RoomTypeDto dto)
        {
            var builder = base.Map(entity, dto);
            return builder;
        }

        public override RoomType.Builder Map(RoomTypeDto dto)
        {
            var builder = base.Map(dto);
            return builder;
        }
    }
}
