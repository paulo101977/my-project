﻿using System.Collections.Generic;

using Thex.Domain.Entities;
using Thex.Dto;
using Tnf.Dependency;

namespace Thex.Application.Adapters
{
    public interface IBillingItemAdapter : IScaffoldBillingItemAdapter, ITransientDependency
    {
        BillingItem.Builder MapWithChildren(BillingItemDto dto, List<BillingItemPaymentCondition> children);
        BillingItem.Builder MapWithChildren(BillingItem entity, BillingItemDto dto, List<BillingItemPaymentCondition> children);
    }
}
