﻿using Thex.Domain.Entities;
using Thex.Dto;

using Tnf.Notifications;
using System.Collections.Generic;

namespace Thex.Application.Adapters
{
    public class BillingItemAdapter : ScaffoldBillingItemAdapter, IBillingItemAdapter
    {
        public BillingItemAdapter(INotificationHandler notificationHandler)
            : base(notificationHandler)
        {
        }

        public override BillingItem.Builder Map(BillingItem entity, BillingItemDto dto)
        {
            var builder = base.Map(entity, dto);
            return builder;
        }

        public override BillingItem.Builder Map(BillingItemDto dto)
        {
            var builder = base.Map(dto);
            return builder;
        }

        public BillingItem.Builder MapWithChildren(BillingItemDto dto, List<BillingItemPaymentCondition> children)
        {
            var builder = base.Map(dto);

            builder.WithBillingItemPaymentConditionList(children);

            return builder;
        }

        public BillingItem.Builder MapWithChildren(BillingItem entity, BillingItemDto dto, List<BillingItemPaymentCondition> children)
        {
            var builder = base.Map(dto);

            builder.WithBillingItemPaymentConditionList(children);

            return builder;
        }
    }
}
