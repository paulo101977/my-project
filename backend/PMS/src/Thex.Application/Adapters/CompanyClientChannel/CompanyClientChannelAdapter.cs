﻿using Thex.Domain.Entities;
using Thex.Dto;
using Tnf.Notifications;

namespace Thex.Application.Adapters
{
    public class CompanyClientChannelAdapter : ScaffoldCompanyClientChannelAdapter, ICompanyClientChannelAdapter
    {
        public CompanyClientChannelAdapter(INotificationHandler notificationHandler)
           : base(notificationHandler)
        {
        }

        public override CompanyClientChannel.Builder Map(CompanyClientChannel entity, CompanyClientChannelDto dto)
        {
            var builder = base.Map(entity, dto);
            return builder;
        }

        public override CompanyClientChannel.Builder Map(CompanyClientChannelDto dto)
        {
            var builder = base.Map(dto);
            return builder;
        }
    }
}
