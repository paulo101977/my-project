﻿using Tnf.Dependency;

namespace Thex.Application.Adapters
{
    public interface ICompanyClientChannelAdapter : IScaffoldCompanyClientChannelAdapter, ITransientDependency
    {
    }
}
