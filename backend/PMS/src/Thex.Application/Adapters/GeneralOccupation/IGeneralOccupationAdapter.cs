﻿using Tnf.Dependency;

namespace Thex.Application.Adapters
{
    public interface IGeneralOccupationAdapter : IScaffoldGeneralOccupationAdapter, ITransientDependency
    {
    }
}
