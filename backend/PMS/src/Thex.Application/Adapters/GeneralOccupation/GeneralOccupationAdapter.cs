﻿using Thex.Domain.Entities;
using Thex.Dto;

using Tnf.Notifications;

namespace Thex.Application.Adapters
{
    public class GeneralOccupationAdapter : ScaffoldGeneralOccupationAdapter, IGeneralOccupationAdapter
    {
        public GeneralOccupationAdapter(INotificationHandler notificationHandler)
            : base(notificationHandler)
        {
        }

        public override GeneralOccupation.Builder Map(GeneralOccupation entity, GeneralOccupationDto dto)
        {
            var builder = base.Map(entity, dto);
            return builder;
        }

        public override GeneralOccupation.Builder Map(GeneralOccupationDto dto)
        {
            var builder = base.Map(dto);
            return builder;
        }
    }
}
