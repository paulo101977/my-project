﻿using Thex.Domain.Entities;
using Thex.Dto;

using Tnf.Notifications;

namespace Thex.Application.Adapters
{
    public class CountryPersonDocumentTypeAdapter : ScaffoldCountryPersonDocumentTypeAdapter, ICountryPersonDocumentTypeAdapter
    {
        public CountryPersonDocumentTypeAdapter(INotificationHandler notificationHandler)
            : base(notificationHandler)
        {
        }

        public override CountryPersonDocumentType.Builder Map(CountryPersonDocumentType entity, CountryPersonDocumentTypeDto dto)
        {
            var builder = base.Map(entity, dto);
            return builder;
        }

        public override CountryPersonDocumentType.Builder Map(CountryPersonDocumentTypeDto dto)
        {
            var builder = base.Map(dto);
            return builder;
        }
    }
}
