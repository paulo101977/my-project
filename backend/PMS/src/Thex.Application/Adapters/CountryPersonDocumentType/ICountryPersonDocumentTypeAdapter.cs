﻿using Tnf.Dependency;

namespace Thex.Application.Adapters
{
    public interface ICountryPersonDocumentTypeAdapter : IScaffoldCountryPersonDocumentTypeAdapter, ITransientDependency
    {
    }
}
