﻿using Thex.Domain.Entities;
using Thex.Dto;

using Tnf.Notifications;

namespace Thex.Application.Adapters
{
    public class ApplicationModuleAdapter : ScaffoldApplicationModuleAdapter, IApplicationModuleAdapter
    {
        public ApplicationModuleAdapter(INotificationHandler notificationHandler)
            : base(notificationHandler)
        {
        }

        public override ApplicationModule.Builder Map(ApplicationModule entity, ApplicationModuleDto dto)
        {
            var builder = base.Map(entity, dto);
            return builder;
        }

        public override ApplicationModule.Builder Map(ApplicationModuleDto dto)
        {
            var builder = base.Map(dto);
            return builder;
        }
    }
}
