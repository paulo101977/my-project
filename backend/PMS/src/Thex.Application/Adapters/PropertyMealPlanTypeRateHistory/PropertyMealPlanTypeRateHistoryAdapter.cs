﻿using Thex.Domain.Entities;
using Thex.Dto;
using Tnf.Notifications;

namespace Thex.Application.Adapters
{
    public class PropertyMealPlanTypeRateHistoryAdapter : ScaffoldPropertyMealPlanTypeRateHistoryAdapter, IPropertyMealPlanTypeRateHistoryAdapter
    {
        public PropertyMealPlanTypeRateHistoryAdapter(INotificationHandler notificationHandler)
            : base(notificationHandler)
        {
        }

        public override PropertyMealPlanTypeRateHistory.Builder Map(PropertyMealPlanTypeRateHistory entity, PropertyMealPlanTypeRateHistoryDto dto)
        {
            var builder = base.Map(entity, dto);
            return builder;
        }

        public override PropertyMealPlanTypeRateHistory.Builder Map(PropertyMealPlanTypeRateHistoryDto dto)
        {
            var builder = base.Map(dto);
            return builder;
        }
    }
}
