﻿using Thex.Domain.Entities;
using Thex.Dto;

using Tnf.Notifications;

namespace Thex.Application.Adapters
{
    public class PropertyContactPersonAdapter : ScaffoldPropertyContactPersonAdapter, IPropertyContactPersonAdapter
    {
        public PropertyContactPersonAdapter(INotificationHandler notificationHandler)
            : base(notificationHandler)
        {
        }

        public override PropertyContactPerson.Builder Map(PropertyContactPerson entity, PropertyContactPersonDto dto)
        {
            var builder = base.Map(entity, dto);
            return builder;
        }

        public override PropertyContactPerson.Builder Map(PropertyContactPersonDto dto)
        {
            var builder = base.Map(dto);
            return builder;
        }
    }
}
