﻿using Tnf.Dependency;

namespace Thex.Application.Adapters
{
    public interface IPropertyContactPersonAdapter : IScaffoldPropertyContactPersonAdapter, ITransientDependency
    {
    }
}
