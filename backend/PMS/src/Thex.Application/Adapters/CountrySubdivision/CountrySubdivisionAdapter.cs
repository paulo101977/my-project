﻿using Thex.Domain.Entities;
using Thex.Dto;
using Tnf.Notifications;

namespace Thex.Application.Adapters
{
    public class CountrySubdivisionAdapter : ScaffoldCountrySubdivisionAdapter, ICountrySubdivisionAdapter
    {
        public CountrySubdivisionAdapter(INotificationHandler notificationHandler)
            : base(notificationHandler)
        {
        }

        public override CountrySubdivision.Builder Map(CountrySubdivision entity, CountrySubdivisionDto dto)
        {
            var builder = base.Map(entity, dto);
            return builder;
        }

        public override CountrySubdivision.Builder Map(CountrySubdivisionDto dto)
        {
            var builder = base.Map(dto);
            return builder;
        }
    }
}
