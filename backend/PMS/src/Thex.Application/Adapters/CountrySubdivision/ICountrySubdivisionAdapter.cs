﻿using Tnf.Dependency;

namespace Thex.Application.Adapters
{
    public interface ICountrySubdivisionAdapter : IScaffoldCountrySubdivisionAdapter, ITransientDependency
    {
    }
}
