﻿using Thex.Domain.Entities;
using Thex.Dto;

using Tnf.Notifications;

namespace Thex.Application.Adapters
{
    public class PropertyAuditProcessStepErrorAdapter : ScaffoldPropertyAuditProcessStepErrorAdapter, IPropertyAuditProcessStepErrorAdapter
    {
        public PropertyAuditProcessStepErrorAdapter(INotificationHandler notificationHandler)
            : base(notificationHandler)
        {
        }

        public override PropertyAuditProcessStepError.Builder Map(PropertyAuditProcessStepError entity, PropertyAuditProcessStepErrorDto dto)
        {
            var builder = base.Map(entity, dto);
            return builder;
        }

        public override PropertyAuditProcessStepError.Builder Map(PropertyAuditProcessStepErrorDto dto)
        {
            var builder = base.Map(dto);
            return builder;
        }
    }
}
