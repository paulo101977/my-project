﻿using Tnf.Dependency;

namespace Thex.Application.Adapters
{
    public interface IPropertyAuditProcessStepErrorAdapter : IScaffoldPropertyAuditProcessStepErrorAdapter, ITransientDependency
    {
    }
}
