﻿using Thex.Domain.Entities;
using Thex.Dto;

using Tnf.Notifications;

namespace Thex.Application.Adapters
{
    public class StatusAdapter : ScaffoldStatusAdapter, IStatusAdapter
    {
        public StatusAdapter(INotificationHandler notificationHandler)
            : base(notificationHandler)
        {
        }

        public override Status.Builder Map(Status entity, StatusDto dto)
        {
            var builder = base.Map(entity, dto);
            return builder;
        }

        public override Status.Builder Map(StatusDto dto)
        {
            var builder = base.Map(dto);
            return builder;
        }
    }
}
