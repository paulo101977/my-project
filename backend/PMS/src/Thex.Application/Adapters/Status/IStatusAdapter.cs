﻿using Tnf.Dependency;

namespace Thex.Application.Adapters
{
    public interface IStatusAdapter : IScaffoldStatusAdapter, ITransientDependency
    {
    }
}
