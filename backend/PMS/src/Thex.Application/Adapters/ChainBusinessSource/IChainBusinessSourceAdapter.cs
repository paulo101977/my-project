﻿using Tnf.Dependency;

namespace Thex.Application.Adapters
{
    public interface IChainBusinessSourceAdapter : IScaffoldChainBusinessSourceAdapter, ITransientDependency
    {
    }
}
