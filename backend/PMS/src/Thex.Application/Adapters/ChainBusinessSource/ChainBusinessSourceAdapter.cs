﻿using Thex.Domain.Entities;
using Thex.Dto;

using Tnf.Notifications;

namespace Thex.Application.Adapters
{
    public class ChainBusinessSourceAdapter : ScaffoldChainBusinessSourceAdapter, IChainBusinessSourceAdapter
    {
        public ChainBusinessSourceAdapter(INotificationHandler notificationHandler)
            : base(notificationHandler)
        {
        }

        public override ChainBusinessSource.Builder Map(ChainBusinessSource entity, ChainBusinessSourceDto dto)
        {
            var builder = base.Map(entity, dto);
            return builder;
        }

        public override ChainBusinessSource.Builder Map(ChainBusinessSourceDto dto)
        {
            var builder = base.Map(dto);
            return builder;
        }
    }
}
