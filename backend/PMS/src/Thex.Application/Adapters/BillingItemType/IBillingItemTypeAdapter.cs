﻿using Tnf.Dependency;

namespace Thex.Application.Adapters
{
    public interface IBillingItemTypeAdapter : IScaffoldBillingItemTypeAdapter, ITransientDependency
    {
    }
}
