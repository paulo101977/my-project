﻿using Thex.Domain.Entities;
using Thex.Dto;

using Tnf.Notifications;

namespace Thex.Application.Adapters
{
    public class BillingItemTypeAdapter : ScaffoldBillingItemTypeAdapter, IBillingItemTypeAdapter
    {
        public BillingItemTypeAdapter(INotificationHandler notificationHandler)
            : base(notificationHandler)
        {
        }

        public override BillingItemType.Builder Map(BillingItemType entity, BillingItemTypeDto dto)
        {
            var builder = base.Map(entity, dto);
            return builder;
        }

        public override BillingItemType.Builder Map(BillingItemTypeDto dto)
        {
            var builder = base.Map(dto);
            return builder;
        }
    }
}
