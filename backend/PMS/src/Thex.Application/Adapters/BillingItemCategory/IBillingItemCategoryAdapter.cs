﻿using Tnf.Dependency;

namespace Thex.Application.Adapters
{
    public interface IBillingItemCategoryAdapter : IScaffoldBillingItemCategoryAdapter, ITransientDependency
    {
    }
}
