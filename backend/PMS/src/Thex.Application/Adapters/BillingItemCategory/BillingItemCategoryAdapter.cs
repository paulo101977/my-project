﻿using Thex.Domain.Entities;
using Thex.Dto;

using Tnf.Notifications;

namespace Thex.Application.Adapters
{
    public class BillingItemCategoryAdapter : ScaffoldBillingItemCategoryAdapter, IBillingItemCategoryAdapter
    {
        public BillingItemCategoryAdapter(INotificationHandler notificationHandler)
            : base(notificationHandler)
        {
        }

        public override BillingItemCategory.Builder Map(BillingItemCategory entity, BillingItemCategoryDto dto)
        {
            var builder = base.Map(entity, dto);
            return builder;
        }

        public override BillingItemCategory.Builder Map(BillingItemCategoryDto dto)
        {
            var builder = base.Map(dto);
            return builder;
        }
    }
}
