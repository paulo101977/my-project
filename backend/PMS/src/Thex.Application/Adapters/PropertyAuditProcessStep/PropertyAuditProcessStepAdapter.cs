﻿using Thex.Domain.Entities;
using Thex.Dto;

using Tnf.Notifications;

namespace Thex.Application.Adapters
{
    public class PropertyAuditProcessStepAdapter : ScaffoldPropertyAuditProcessStepAdapter, IPropertyAuditProcessStepAdapter
    {
        public PropertyAuditProcessStepAdapter(INotificationHandler notificationHandler)
            : base(notificationHandler)
        {
        }

        public override PropertyAuditProcessStep.Builder Map(PropertyAuditProcessStep entity, PropertyAuditProcessStepDto dto)
        {
            var builder = base.Map(entity, dto);
            return builder;
        }

        public override PropertyAuditProcessStep.Builder Map(PropertyAuditProcessStepDto dto)
        {
            var builder = base.Map(dto);
            return builder;
        }
    }
}
