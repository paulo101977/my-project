﻿using Tnf.Dependency;

namespace Thex.Application.Adapters
{
    public interface IPropertyAuditProcessStepAdapter : IScaffoldPropertyAuditProcessStepAdapter, ITransientDependency
    {
    }
}
