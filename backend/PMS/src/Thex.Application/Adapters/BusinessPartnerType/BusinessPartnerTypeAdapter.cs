﻿using Thex.Domain.Entities;
using Thex.Dto;

using Tnf.Notifications;

namespace Thex.Application.Adapters
{
    public class BusinessPartnerTypeAdapter : ScaffoldBusinessPartnerTypeAdapter, IBusinessPartnerTypeAdapter
    {
        public BusinessPartnerTypeAdapter(INotificationHandler notificationHandler)
            : base(notificationHandler)
        {
        }

        public override BusinessPartnerType.Builder Map(BusinessPartnerType entity, BusinessPartnerTypeDto dto)
        {
            var builder = base.Map(entity, dto);
            return builder;
        }

        public override BusinessPartnerType.Builder Map(BusinessPartnerTypeDto dto)
        {
            var builder = base.Map(dto);
            return builder;
        }
    }
}
