﻿using Thex.Domain.Entities;
using Thex.Dto;

using Tnf.Notifications;

namespace Thex.Application.Adapters
{
    public class BillingTaxAdapter : ScaffoldBillingTaxAdapter, IBillingTaxAdapter
    {
        public BillingTaxAdapter(INotificationHandler notificationHandler)
            : base(notificationHandler)
        {
        }

        public override BillingTax.Builder Map(BillingTax entity, BillingTaxDto dto)
        {
            var builder = base.Map(entity, dto);
            return builder;
        }

        public override BillingTax.Builder Map(BillingTaxDto dto)
        {
            var builder = base.Map(dto);
            return builder;
        }
    }
}
