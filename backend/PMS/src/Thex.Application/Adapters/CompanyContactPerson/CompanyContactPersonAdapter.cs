﻿using Thex.Domain.Entities;
using Thex.Dto;

using Tnf.Notifications;

namespace Thex.Application.Adapters
{
    public class CompanyContactPersonAdapter : ScaffoldCompanyContactPersonAdapter, ICompanyContactPersonAdapter
    {
        public CompanyContactPersonAdapter(INotificationHandler notificationHandler)
            : base(notificationHandler)
        {
        }

        public override CompanyContactPerson.Builder Map(CompanyContactPerson entity, CompanyContactPersonDto dto)
        {
            var builder = base.Map(entity, dto);
            return builder;
        }

        public override CompanyContactPerson.Builder Map(CompanyContactPersonDto dto)
        {
            var builder = base.Map(dto);
            return builder;
        }
    }
}
