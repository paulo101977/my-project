﻿using Tnf.Dependency;

namespace Thex.Application.Adapters
{
    public interface ICompanyContactPersonAdapter : IScaffoldCompanyContactPersonAdapter, ITransientDependency
    {
    }
}
