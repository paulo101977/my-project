﻿using Thex.Domain.Entities;
using Thex.Dto;

using Tnf.Notifications;

namespace Thex.Application.Adapters
{
    public class PropertyAdapter : ScaffoldPropertyAdapter, IPropertyAdapter
    {
        public PropertyAdapter(INotificationHandler notificationHandler)
            : base(notificationHandler)
        {
        }

        public override Property.Builder Map(Property entity, PropertyDto dto)
        {
            var builder = base.Map(entity, dto);
            return builder;
        }

        public override Property.Builder Map(PropertyDto dto)
        {
            var builder = base.Map(dto);
            return builder;
        }

        public override Property.Builder MapPhotoUrl(PropertyDto dto, string photoUrl)
        {
            var builder = base.MapPhotoUrl(dto, photoUrl);
            return builder;
        }
    }
}
