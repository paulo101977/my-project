﻿using Thex.Application.Adapters.Scaffold;
using Thex.Domain.Entities;
using Tnf.Notifications;
using Thex.Dto;

namespace Thex.Application.Adapters
{
    public class PropertyRateStrategyAdapter : ScaffoldPropertyRateStrategyAdapter, IPropertyRateStrategyAdapter
    {
        public PropertyRateStrategyAdapter(INotificationHandler notificationHandler)
            : base(notificationHandler)
        {
        }

        public override PropertyRateStrategy.Builder Map(PropertyRateStrategy entity, PropertyRateStrategyDto dto)
        {
            var builder = base.Map(entity, dto);
            return builder;
        }

        public override PropertyRateStrategy.Builder Map(PropertyRateStrategyDto dto)
        {
            var builder = base.Map(dto);
            return builder;
        }
    }
}
