﻿using Tnf.Dependency;

namespace Thex.Application.Adapters
{
    public interface IPropertyRateStrategyAdapter : IScaffoldPropertyRateStrategyAdapter, ITransientDependency
    {
    }
}
