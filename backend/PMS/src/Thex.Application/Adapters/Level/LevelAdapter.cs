﻿using Thex.Domain.Entities;
using Thex.Dto;
using Thex.Kernel;
using Tnf.Notifications;

namespace Thex.Application.Adapters
{
    public class LevelAdapter : ScaffoldLevelAdapter, ILevelAdapter
    {
        public LevelAdapter(INotificationHandler notificationHandler, IApplicationUser applicationUser)
            : base(notificationHandler, applicationUser)
        {
        }

        public override Level.Builder Map(Level entity, LevelDto dto)
        {
            var builder = base.Map(entity, dto);
            return builder;
        }

        public override Level.Builder Map(LevelDto dto)
        {
            var builder = base.Map(dto);
            return builder;
        }
    }
}
