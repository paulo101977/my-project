﻿using Tnf.Dependency;

namespace Thex.Application.Adapters
{
    public interface ILevelAdapter : IScaffoldLevelAdapter, ITransientDependency
    {
    }
}
