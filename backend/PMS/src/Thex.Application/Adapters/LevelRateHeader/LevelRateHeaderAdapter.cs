﻿using Thex.Domain.Entities;
using Thex.Dto;
using Thex.Kernel;
using Tnf.Notifications;

namespace Thex.Application.Adapters
{
    public class LevelRateHeaderAdapter : ScaffoldLevelRateHeaderAdapter, ILevelRateHeaderAdapter
    {
        public LevelRateHeaderAdapter(
            INotificationHandler notificationHandler,
            IApplicationUser applicationUser)
            : base(notificationHandler, applicationUser)
        {
        }

        public override LevelRateHeader.Builder Map(LevelRateHeader entity, LevelRateHeaderDto dto)
        {
            var builder = base.Map(entity, dto);
            return builder;
        }

        public override LevelRateHeader.Builder Map(LevelRateHeaderDto dto)
        {
            var builder = base.Map(dto);
            return builder;
        }
    }
}
