﻿using Tnf.Dependency;

namespace Thex.Application.Adapters
{
    public interface ILevelRateHeaderAdapter : IScaffoldLevelRateHeaderAdapter, ITransientDependency
    {
    }
}
