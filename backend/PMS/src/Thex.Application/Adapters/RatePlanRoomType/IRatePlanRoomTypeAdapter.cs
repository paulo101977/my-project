﻿using Tnf.Dependency;

namespace Thex.Application.Adapters
{
    public interface IRatePlanRoomTypeAdapter : IScaffoldRatePlanRoomTypeAdapter, ITransientDependency
    {
    }
}
