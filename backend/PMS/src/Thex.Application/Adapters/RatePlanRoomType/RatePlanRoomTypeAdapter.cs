﻿using Thex.Domain.Entities;
using Thex.Dto;

using Tnf.Notifications;

namespace Thex.Application.Adapters
{
    public class RatePlanRoomTypeAdapter : ScaffoldRatePlanRoomTypeAdapter, IRatePlanRoomTypeAdapter
    {
        public RatePlanRoomTypeAdapter(INotificationHandler notificationHandler)
            : base(notificationHandler)
        {
        }

        public override RatePlanRoomType.Builder Map(RatePlanRoomType entity, RatePlanRoomTypeDto dto)
        {
            var builder = base.Map(entity, dto);
            return builder;
        }

        public override RatePlanRoomType.Builder Map(RatePlanRoomTypeDto dto)
        {
            var builder = base.Map(dto);
            return builder;
        }
        public override RatePlanRoomType.Builder MapUpdate(RatePlanRoomTypeDto dto)
        {
            var builder = base.MapUpdate(dto);
            return builder;
        }
    }
}
