﻿
using System;
using Thex.Domain.Entities;
using Thex.Dto;
using Tnf.Dependency;

namespace Thex.Application.Adapters
{
    public interface IBillingAccountAdapter : IScaffoldBillingAccountAdapter, ITransientDependency
    {
        BillingAccount.Builder AddBillingAccountMap(BillingAccount originalBillingAccount, AddBillingAccountDto dto, Guid groupKeyParam = new Guid(), Guid? personHeaderId = null);
        BillingAccount.Builder MapWithGroupKey(BillingAccount entity, BillingAccountDto dto);
        BillingAccount.Builder MapWithGroupKey(BillingAccountDto dto);
        BillingAccount.Builder AddBillingAccountCreditNoteMap(BillingAccount originalBillingAccount, AddBillingAccountDto dto);
    }
}
