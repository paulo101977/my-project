﻿using Thex.Domain.Entities;
using Thex.Dto;

using Tnf.Notifications;
using System;
using Thex.Common.Enumerations;
using Tnf;

namespace Thex.Application.Adapters
{
    public class BillingAccountAdapter : ScaffoldBillingAccountAdapter, IBillingAccountAdapter
    {
        public BillingAccountAdapter(
            INotificationHandler notificationHandler)
            : base(notificationHandler)
        {
        }

        public override BillingAccount.Builder Map(BillingAccount entity, BillingAccountDto dto)
        {
            var builder = base.Map(entity, dto);
            return builder;
        }

        public override BillingAccount.Builder Map(BillingAccountDto dto)
        {
            var builder = base.Map(dto);
            return builder;
        }


        public virtual BillingAccount.Builder MapWithGroupKey(BillingAccount entity, BillingAccountDto dto)
        {
            Check.NotNull(entity, nameof(entity));
            Check.NotNull(dto, nameof(dto));

            var id = Guid.NewGuid();

            var builder = new BillingAccount.Builder(NotificationHandler, entity)
                .WithId(id)
                .WithCompanyClientId(dto.CompanyClientId)
                .WithReservationId(dto.ReservationId)
                .WithReservationItemId(dto.ReservationItemId)
                .WithGuestReservationItemId(dto.GuestReservationItemId)
                .WithBillingAccountTypeId(dto.BillingAccountTypeId)
                .WithStartDate(dto.StartDate)
                .WithEndDate(dto.EndDate)
                .WithStatusId(dto.StatusId)
                .WithIsMainAccount(dto.IsMainAccount)
                .WithMarketSegmentId(dto.MarketSegmentId)
                .WithBusinessSourceId(dto.BusinessSourceId)
                .WithPropertyId(dto.PropertyId)
                .WithGroupKey(id)
                .WithBlocked(dto.Blocked)
                .WithIsBillingAccountName(dto.BillingAccountName)
                .WithReopeningReasonId(dto.ReopeningReasonId)
                .WithReopeningDate(dto.ReopeningDate);

            return builder;
        }

        public virtual BillingAccount.Builder MapWithGroupKey(BillingAccountDto dto)
        {
            Check.NotNull(dto, nameof(dto));

            var id = Guid.NewGuid();

            var builder = new BillingAccount.Builder(NotificationHandler)
                .WithId(id)
                .WithCompanyClientId(dto.CompanyClientId)
                .WithReservationId(dto.ReservationId)
                .WithReservationItemId(dto.ReservationItemId)
                .WithGuestReservationItemId(dto.GuestReservationItemId)
                .WithBillingAccountTypeId(dto.BillingAccountTypeId)
                .WithStartDate(dto.StartDate)
                .WithEndDate(dto.EndDate)
                .WithStatusId(dto.StatusId)
                .WithIsMainAccount(dto.IsMainAccount)
                .WithMarketSegmentId(dto.MarketSegmentId)
                .WithBusinessSourceId(dto.BusinessSourceId)
                .WithPropertyId(dto.PropertyId)
                .WithGroupKey(id)
                .WithBlocked(dto.Blocked)
                .WithIsBillingAccountName(dto.BillingAccountName);

            return builder;
        }

        public BillingAccount.Builder AddBillingAccountMap(BillingAccount originalBillingAccount, AddBillingAccountDto dto, Guid groupKeyParam = new Guid(), Guid? personHeaderId = null)
        {
            Check.NotNull(originalBillingAccount, nameof(originalBillingAccount));

            var id = Guid.NewGuid();
            var groupKey = groupKeyParam == Guid.Empty ? id : groupKeyParam;

            var builder = new BillingAccount.Builder(NotificationHandler)
                .WithId(id)
                .WithCompanyClientId(originalBillingAccount.CompanyClientId)
                .WithReservationId(originalBillingAccount.ReservationId)
                .WithReservationItemId(originalBillingAccount.ReservationItemId)
                .WithGuestReservationItemId(originalBillingAccount.GuestReservationItemId)
                .WithBillingAccountTypeId(originalBillingAccount.BillingAccountTypeId)
                .WithStartDate(DateTime.UtcNow.ToZonedDateTimeLoggedUser())
                .WithStatusId((int)AccountBillingStatusEnum.Opened)
                .WithMarketSegmentId(originalBillingAccount.MarketSegmentId)
                .WithBusinessSourceId(originalBillingAccount.BusinessSourceId)
                .WithPropertyId(originalBillingAccount.PropertyId)
                .WithBlocked(false)
                .WithIsBillingAccountName(dto.BillingAccountName)
                .WithPersonHeaderId(personHeaderId);

            if (dto.CreatePrincipalAccount.HasValue && dto.CreatePrincipalAccount.Value)
                builder
                    .WithIsMainAccount(true)
                    .WithGroupKey(groupKey);
            else
                builder
                    .WithGuestReservationItemId(originalBillingAccount.GuestReservationItemId)
                    .WithIsMainAccount(false)
                    .WithGroupKey(originalBillingAccount.GroupKey);

            return builder;
        }

        public BillingAccount.Builder AddBillingAccountCreditNoteMap(BillingAccount originalBillingAccount, AddBillingAccountDto dto)
        {
            Check.NotNull(originalBillingAccount, nameof(originalBillingAccount));

            var id = Guid.NewGuid();

            var builder = new BillingAccount.Builder(NotificationHandler)
                .WithId(id)
                .WithCompanyClientId(originalBillingAccount.CompanyClientId)
                .WithReservationId(originalBillingAccount.ReservationId)
                .WithReservationItemId(originalBillingAccount.ReservationItemId)
                .WithGuestReservationItemId(originalBillingAccount.GuestReservationItemId)
                .WithBillingAccountTypeId(originalBillingAccount.BillingAccountTypeId)
                .WithStartDate(DateTime.UtcNow.ToZonedDateTimeLoggedUser().Date)
                .WithStatusId((int)AccountBillingStatusEnum.Opened)
                .WithMarketSegmentId(originalBillingAccount.MarketSegmentId)
                .WithBusinessSourceId(originalBillingAccount.BusinessSourceId)
                .WithPropertyId(originalBillingAccount.PropertyId)
                .WithBlocked(false)
                .WithIsBillingAccountName(dto.BillingAccountName)
                .WithIsMainAccount(false)
                .WithGroupKey(originalBillingAccount.GroupKey);

            return builder;
        }
    }
}
