﻿using Tnf.Dependency;

namespace Thex.Application.Adapters
{
    public interface IPersonInformationTypeAdapter : IScaffoldPersonInformationTypeAdapter, ITransientDependency
    {
    }
}
