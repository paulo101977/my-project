﻿using Thex.Domain.Entities;
using Thex.Dto;

using Tnf.Notifications;

namespace Thex.Application.Adapters
{
    public class PersonInformationTypeAdapter : ScaffoldPersonInformationTypeAdapter, IPersonInformationTypeAdapter
    {
        public PersonInformationTypeAdapter(INotificationHandler notificationHandler)
            : base(notificationHandler)
        {
        }

        public override PersonInformationType.Builder Map(PersonInformationType entity, PersonInformationTypeDto dto)
        {
            var builder = base.Map(entity, dto);
            return builder;
        }

        public override PersonInformationType.Builder Map(PersonInformationTypeDto dto)
        {
            var builder = base.Map(dto);
            return builder;
        }
    }
}
