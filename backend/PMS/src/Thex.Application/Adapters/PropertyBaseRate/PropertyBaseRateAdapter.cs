﻿using System;
using Thex.Common.Enumerations;
using Thex.Domain.Entities;
using Thex.Dto;
using Tnf;
using Tnf.Notifications;

namespace Thex.Application.Adapters
{
    public class PropertyBaseRateAdapter : ScaffoldPropertyBaseRateAdapter, IPropertyBaseRateAdapter
    {
        public PropertyBaseRateAdapter(INotificationHandler notificationHandler)
            : base(notificationHandler)
        {
        }

        public override PropertyBaseRate.Builder Map(PropertyBaseRate entity, PropertyBaseRateDto dto)
        {
            var builder = base.Map(entity, dto);
            return builder;
        }

        public override PropertyBaseRate.Builder Map(PropertyBaseRateDto dto)
        {
            var builder = base.Map(dto);
            return builder;
        }

        public virtual PropertyBaseRate.Builder MapBaseRateConfigurationDto(BaseRateConfigurationRoomTypeDto dto, BaseRateConfigurationMealPlanTypeDto dtoMealPlanType, BaseRateConfigurationDto baseDto, DateTime date, CurrencyDto currency, Guid propertyBaseRateHeaderHistoryId)
        {
            Check.NotNull(dto, nameof(dto));

            var builder = new PropertyBaseRate.Builder(NotificationHandler)
                .WithId(Guid.NewGuid())
                .WithPropertyId(baseDto.PropertyId)
                .WithDate(date)
                .WithMealPlanTypeDefault(baseDto.MealPlanTypeDefaultId)
                .WithRoomTypeId(dto.RoomTypeId)
                .WithPax1Amount(dto.Pax1Amount)
                .WithPax2Amount(dto.Pax2Amount)
                .WithPax3Amount(dto.Pax3Amount)
                .WithPax4Amount(dto.Pax4Amount)
                .WithPax5Amount(dto.Pax5Amount)
                .WithPaxAdditionalAmount(dto.PaxAdditionalAmount)
                .WithChild1Amount(dto.Child1Amount)
                .WithChild2Amount(dto.Child2Amount)
                .WithChild3Amount(dto.Child3Amount)
                .WithCurrencyId(currency.Id)
                .WithCurrencySymbol(currency.Symbol)
                .WithMealPlanTypeId(dtoMealPlanType.MealPlanTypeId)
                .WithAdultMealPlanAmount(dtoMealPlanType.AdultMealPlanAmount)
                .WithChild1MealPlanAmount(dtoMealPlanType.Child1MealPlanAmount)
                .WithChild2MealPlanAmount(dtoMealPlanType.Child2MealPlanAmount)
                .WithChild3MealPlanAmount(dtoMealPlanType.Child3MealPlanAmount)
                .WithRatePlanId(baseDto.RatePlanId)
                .WithPropertyBaseRateTypeId(baseDto.PropertyBaseRateTypeId)
                .WithPropertyMealPlanTypeRate(baseDto.PropertyMealPlanTypeRate)
                .WithPropertyBaseRateHeaderHistoryId(propertyBaseRateHeaderHistoryId)
                .WithLevel(dto.Level);

            GenerateBuilderByPropertyBaseRateTypeId(baseDto, builder);

            return builder;
        }


        public virtual PropertyBaseRate.Builder MapBaseRateConfigurationDto(PropertyBaseRate entity, BaseRateConfigurationMealPlanTypeDto dtoMealPlanType, BaseRateConfigurationRoomTypeDto dto, BaseRateConfigurationDto baseDto, DateTime date, CurrencyDto currency, Guid propertyBaseRateHeaderHistoryId)
        {
            Check.NotNull(entity, nameof(entity));
            Check.NotNull(dto, nameof(dto));

            var builder = new PropertyBaseRate.Builder(NotificationHandler, entity)
                .WithId(entity.Id)
                .WithPropertyId(baseDto.PropertyId)
                .WithDate(date)
                .WithMealPlanTypeDefault(baseDto.MealPlanTypeDefaultId)
                .WithRoomTypeId(dto.RoomTypeId)
                .WithPax1Amount(dto.Pax1Amount)
                .WithPax2Amount(dto.Pax2Amount)
                .WithPax3Amount(dto.Pax3Amount)
                .WithPax4Amount(dto.Pax4Amount)
                .WithPax5Amount(dto.Pax5Amount)
                .WithPaxAdditionalAmount(dto.PaxAdditionalAmount)
                .WithChild1Amount(dto.Child1Amount)
                .WithChild2Amount(dto.Child2Amount)
                .WithChild3Amount(dto.Child3Amount)
                .WithCurrencyId(currency.Id)
                .WithCurrencySymbol(currency.Symbol)
                .WithMealPlanTypeId(dtoMealPlanType.MealPlanTypeId)
                .WithAdultMealPlanAmount(dtoMealPlanType.AdultMealPlanAmount.HasValue ? dtoMealPlanType.AdultMealPlanAmount : entity.AdultMealPlanAmount)
                .WithChild1MealPlanAmount(dtoMealPlanType.AdultMealPlanAmount.HasValue ? dtoMealPlanType.Child1MealPlanAmount : entity.Child1MealPlanAmount)
                .WithChild2MealPlanAmount(dtoMealPlanType.AdultMealPlanAmount.HasValue ? dtoMealPlanType.Child2MealPlanAmount : entity.Child2MealPlanAmount)
                .WithChild3MealPlanAmount(dtoMealPlanType.AdultMealPlanAmount.HasValue ? dtoMealPlanType.Child3MealPlanAmount : entity.Child3MealPlanAmount)
                .WithRatePlanId(baseDto.RatePlanId)
                .WithPropertyBaseRateTypeId(baseDto.PropertyBaseRateTypeId)
                .WithPropertyMealPlanTypeRate(baseDto.PropertyMealPlanTypeRate)
                .WithPropertyBaseRateHeaderHistoryId(propertyBaseRateHeaderHistoryId)
                .WithLevel(dto.Level);

            GenerateBuilderByPropertyBaseRateTypeId(baseDto, builder);

            return builder;
        }

        private PropertyBaseRate.Builder GenerateBuilderByPropertyBaseRateTypeId(BaseRateConfigurationDto baseDto, PropertyBaseRate.Builder builder)
        {
            if (baseDto.PropertyBaseRateTypeId == (int)PropertyBaseRateTypeEnum.Level)
            {
                builder
                    .WithLevelId(baseDto.LevelId)
                    .WithLevelRateId(baseDto.LevelRateId);
            }

            return builder;
        }

        public PropertyBaseRate.Builder MapBaseRateConfigurationWithPremiseDto(PropertyBaseRate entity, PremiseValueDto premiseDto)
        {
            Check.NotNull(entity, nameof(entity));

            var builder = new PropertyBaseRate.Builder(NotificationHandler, entity)

                .WithPax1Amount(premiseDto.Pax1)
                .WithPax2Amount(premiseDto.Pax2)
                .WithPax3Amount(premiseDto.Pax3)
                .WithPax4Amount(premiseDto.Pax4)
                .WithPax5Amount(premiseDto.Pax5)
                .WithPaxAdditionalAmount(premiseDto.PaxAdditional)
                .WithChild1Amount(premiseDto.Children1)
                .WithChild2Amount(premiseDto.Children2)
                .WithChild3Amount(premiseDto.Children3);

            return builder;
        }

        public PropertyBaseRate.Builder MapUpdateMeaPlanTypes(PropertyBaseRate entity, PropertyMealPlanTypeRateHistoryDto dto, int mealPlanTypeDefaultId)
        {
            Check.NotNull(entity, nameof(entity));
            Check.NotNull(dto, nameof(dto));

            var builder = new PropertyBaseRate.Builder(NotificationHandler, entity)
                .WithId(entity.Id)
                .WithPropertyId(entity.PropertyId)
                .WithDate(entity.Date)
                .WithMealPlanTypeDefault(mealPlanTypeDefaultId)
                .WithRoomTypeId(entity.RoomTypeId)
                .WithPax1Amount(entity.Pax1Amount)
                .WithPax2Amount(entity.Pax2Amount)
                .WithPax3Amount(entity.Pax3Amount)
                .WithPax4Amount(entity.Pax4Amount)
                .WithPax5Amount(entity.Pax5Amount)
                .WithPaxAdditionalAmount(entity.PaxAdditionalAmount)
                .WithChild1Amount(entity.Child1Amount)
                .WithChild2Amount(entity.Child2Amount)
                .WithChild3Amount(entity.Child3Amount)
                .WithCurrencyId(entity.CurrencyId)
                .WithCurrencySymbol(entity.CurrencySymbol)
                .WithMealPlanTypeId(dto.MealPlanTypeId)
                .WithAdultMealPlanAmount(dto.AdultMealPlanAmount)
                .WithChild1MealPlanAmount(dto.Child1MealPlanAmount)
                .WithChild2MealPlanAmount(dto.Child2MealPlanAmount)
                .WithChild3MealPlanAmount(dto.Child3MealPlanAmount)
                .WithRatePlanId(entity.RatePlanId)
                .WithPropertyBaseRateTypeId(entity.PropertyBaseRateTypeId)
                .WithPropertyBaseRateHeaderHistoryId(entity.PropertyBaseRateHeaderHistoryId)
                .WithLevelId(entity.LevelId)
                .WithLevelRateId(entity.LevelRateId)
                .WithLevel(entity.Level);

            return builder;
        }
    }
}
