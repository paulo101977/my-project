﻿using System;

using Thex.Domain.Entities;
using Thex.Dto;
using Tnf.Dependency;

namespace Thex.Application.Adapters
{
    public interface IPropertyBaseRateAdapter : IScaffoldPropertyBaseRateAdapter, ITransientDependency
    {
        PropertyBaseRate.Builder MapBaseRateConfigurationDto(BaseRateConfigurationRoomTypeDto dto, BaseRateConfigurationMealPlanTypeDto dtoMealPlanType, BaseRateConfigurationDto baseDto, DateTime date, CurrencyDto currency, Guid propertyBaseRateHeaderHistoryId);
        PropertyBaseRate.Builder MapBaseRateConfigurationDto(PropertyBaseRate entity, BaseRateConfigurationMealPlanTypeDto dtoMealPlanType, BaseRateConfigurationRoomTypeDto dto, BaseRateConfigurationDto baseDto, DateTime date, CurrencyDto currency, Guid propertyBaseRateHeaderHistoryId);
        PropertyBaseRate.Builder MapUpdateMeaPlanTypes(PropertyBaseRate entity, PropertyMealPlanTypeRateHistoryDto dto, int mealPlanTypeDefaultId);
        PropertyBaseRate.Builder MapBaseRateConfigurationWithPremiseDto(PropertyBaseRate entity, PremiseValueDto premiseDto);
    }
}
