﻿using System;
using Thex.Domain.Entities;
using Thex.Dto;
using Tnf.Dependency;

namespace Thex.Application.Adapters
{
    public interface IGuestReservationItemAdapter : IScaffoldGuestReservationItemAdapter, ITransientDependency
    {
        GuestReservationItem.Builder Map(GuestRegistrationDto guestRegistrationDto, ReservationItemDto reservationItemDto, CountrySubdivisionTranslationDto countrySubdivisionTranslation, GuestReservationItemEntranceDto dto);
    }
}
