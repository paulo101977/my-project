﻿using System;
using Thex.Common.Enumerations;
using Thex.Domain.Entities;
using Thex.Dto;
using Thex.Kernel;
using Tnf.Notifications;

namespace Thex.Application.Adapters
{
    public class GuestReservationItemAdapter : ScaffoldGuestReservationItemAdapter, IGuestReservationItemAdapter
    {
        private readonly IApplicationUser _applicationUser;

        public GuestReservationItemAdapter(
            INotificationHandler notificationHandler,
            IApplicationUser applicationUser)
            : base(notificationHandler)
        {
            _applicationUser = applicationUser;
        }

        public override GuestReservationItem.Builder Map(GuestReservationItem entity, GuestReservationItemDto dto)
        {
            var builder = base.Map(entity, dto);
            return builder;
        }

        public override GuestReservationItem.Builder Map(GuestReservationItemDto dto)
        {
            var builder = base.Map(dto);
            return builder;
        }

        public GuestReservationItem.Builder Map(GuestRegistrationDto guestRegistrationDto, ReservationItemDto reservationItemDto, CountrySubdivisionTranslationDto countrySubdivisionTranslation, GuestReservationItemEntranceDto dto)
        {
            var builder = base.Map(new GuestReservationItemDto()
            {
                ReservationItemId = reservationItemDto.Id,
                GuestId = guestRegistrationDto.GuestId,
                GuestName = guestRegistrationDto.FullName,
                GuestEmail = guestRegistrationDto.Email,
                GuestDocumentTypeId = guestRegistrationDto.DocumentTypeId,
                GuestDocument = guestRegistrationDto.DocumentInformation,
                EstimatedArrivalDate = dto.SystemDate.Value,
                CheckInDate = dto.SystemDate.Value,
                EstimatedDepartureDate = reservationItemDto.EstimatedDepartureDate,
                GuestStatusId = (int)ReservationStatus.Checkin,
                GuestTypeId = guestRegistrationDto.GuestTypeId ?? 0,
                PreferredName = guestRegistrationDto.FullName,
                CurrencyId = reservationItemDto.CurrencyId.Value,
                GuestRegistrationId = guestRegistrationDto.Id,
                PersonId = guestRegistrationDto.PersonId,
                PropertyId = int.Parse(_applicationUser.PropertyId),
                IsChild = guestRegistrationDto.IsChild,
                IsSeparated = dto.IsSeparated,
                IsGratuity = dto.IsGratuity,
                PaxPosition = dto.PaxPosition
            });

            if (guestRegistrationDto.Nationality != null)
            {
                builder.WithGuestCitizenship(countrySubdivisionTranslation.Nationality);
                builder.WithGuestLanguage(countrySubdivisionTranslation.LanguageIsoCode);
            }

            if (guestRegistrationDto.BirthDate.HasValue && guestRegistrationDto.IsChild)
                builder.WithChildAge(Convert.ToInt16(guestRegistrationDto.BirthDate.GetAge()));
            else
                builder.WithChildAge(null);

            return builder;
        }
    }
}
