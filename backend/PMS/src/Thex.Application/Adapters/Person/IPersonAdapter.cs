﻿
using Thex.Domain.Entities;
using Thex.Dto;
using Tnf.Dependency;

namespace Thex.Application.Adapters
{
    public interface IPersonAdapter : IScaffoldPersonAdapter, ITransientDependency
    {
        Person.Builder FullMap(PersonDto dto);
    }
}
