﻿
using Thex.Domain.Entities;
using Thex.Dto;

using Tnf.Notifications;
using Tnf;

namespace Thex.Application.Adapters
{
    public class PersonAdapter : ScaffoldPersonAdapter, IPersonAdapter
    {
        public PersonAdapter(INotificationHandler notificationHandler)
            : base(notificationHandler)
        {
        }

        public override Person.Builder Map(Person entity, PersonDto dto)
        {
            var builder = base.Map(entity, dto);
            return builder;
        }

        public override Person.Builder Map(PersonDto dto)
        {
            var builder = base.Map(dto);
            return builder;
        }

        public Person.Builder FullMap(PersonDto dto)
        {
            Check.NotNull(dto, nameof(dto));

            var builder = new Person.Builder(NotificationHandler)
                .WithId(dto.Id)
                .WithFirstName(dto.FirstName)
                .WithLastName(dto.LastName)
                .WithPersonType(dto.PersonType)
                .WithDateOfBirth(dto.DateOfBirth)
                .WithCountrySubdivisionId(dto.CountrySubdivisionId)
                .WithAllowContact(dto.AllowContact)
                .WithReceiveOffers(dto.ReceiveOffers)
                .WithSharePersonData(dto.SharePersonData);

            return builder;
        }
    }
}
