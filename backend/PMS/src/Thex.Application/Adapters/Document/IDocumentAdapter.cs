﻿using Tnf.Dependency;

namespace Thex.Application.Adapters
{
    public interface IDocumentAdapter : IScaffoldDocumentAdapter, ITransientDependency
    {
    }
}
