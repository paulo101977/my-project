﻿using Thex.Domain.Entities;
using Thex.Dto;

using Tnf.Notifications;

namespace Thex.Application.Adapters
{
    public class DocumentAdapter : ScaffoldDocumentAdapter, IDocumentAdapter
    {
        public DocumentAdapter(INotificationHandler notificationHandler)
            : base(notificationHandler)
        {
        }

        public override Document.Builder Map(Document entity, DocumentDto dto)
        {
            var builder = base.Map(entity, dto);
            return builder;
        }

        public override Document.Builder Map(DocumentDto dto)
        {
            var builder = base.Map(dto);
            return builder;
        }
    }
}
