﻿using Thex.Domain.Entities;
using Thex.Dto;
using Tnf.Notifications;

namespace Thex.Application.Adapters
{
    public class GuestRegistrationDocumentAdapter : ScaffoldGuestRegistrationDocumentAdapter, IGuestRegistrationDocumentAdapter
    {
        public GuestRegistrationDocumentAdapter(INotificationHandler notificationHandler)
            : base(notificationHandler)
        {
        }

        public override GuestRegistrationDocument.Builder Map(GuestRegistrationDocument entity, GuestRegistrationDocumentDto dto)
        {
            var builder = base.Map(entity, dto);
            return builder;
        }

        public override GuestRegistrationDocument.Builder Map(GuestRegistrationDocumentDto dto)
        {
            var builder = base.Map(dto);
            return builder;
        }
    }
}
