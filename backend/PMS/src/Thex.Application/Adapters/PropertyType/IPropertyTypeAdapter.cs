﻿using Tnf.Dependency;

namespace Thex.Application.Adapters
{
    public interface IPropertyTypeAdapter : IScaffoldPropertyTypeAdapter, ITransientDependency
    {
    }
}
