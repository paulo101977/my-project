﻿using Thex.Domain.Entities;
using Thex.Dto;

using Tnf.Notifications;

namespace Thex.Application.Adapters
{
    public class PropertyTypeAdapter : ScaffoldPropertyTypeAdapter, IPropertyTypeAdapter
    {
        public PropertyTypeAdapter(INotificationHandler notificationHandler)
            : base(notificationHandler)
        {
        }

        public override PropertyType.Builder Map(PropertyType entity, PropertyTypeDto dto)
        {
            var builder = base.Map(entity, dto);
            return builder;
        }

        public override PropertyType.Builder Map(PropertyTypeDto dto)
        {
            var builder = base.Map(dto);
            return builder;
        }
    }
}
