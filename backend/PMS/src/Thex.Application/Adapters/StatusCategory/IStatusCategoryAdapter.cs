﻿using Tnf.Dependency;

namespace Thex.Application.Adapters
{
    public interface IStatusCategoryAdapter : IScaffoldStatusCategoryAdapter, ITransientDependency
    {
    }
}
