﻿using Thex.Domain.Entities;
using Thex.Dto;

using Tnf.Notifications;

namespace Thex.Application.Adapters
{
    public class StatusCategoryAdapter : ScaffoldStatusCategoryAdapter, IStatusCategoryAdapter
    {
        public StatusCategoryAdapter(INotificationHandler notificationHandler)
            : base(notificationHandler)
        {
        }

        public override StatusCategory.Builder Map(StatusCategory entity, StatusCategoryDto dto)
        {
            var builder = base.Map(entity, dto);
            return builder;
        }

        public override StatusCategory.Builder Map(StatusCategoryDto dto)
        {
            var builder = base.Map(dto);
            return builder;
        }
    }
}
