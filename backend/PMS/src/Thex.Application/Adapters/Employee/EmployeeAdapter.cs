﻿using Thex.Domain.Entities;
using Thex.Dto;

using Tnf.Notifications;

namespace Thex.Application.Adapters
{
    public class EmployeeAdapter : ScaffoldEmployeeAdapter, IEmployeeAdapter
    {
        public EmployeeAdapter(INotificationHandler notificationHandler)
            : base(notificationHandler)
        {
        }

        public override Employee.Builder Map(Employee entity, EmployeeDto dto)
        {
            var builder = base.Map(entity, dto);
            return builder;
        }

        public override Employee.Builder Map(EmployeeDto dto)
        {
            var builder = base.Map(dto);
            return builder;
        }
    }
}
