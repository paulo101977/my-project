﻿using Tnf.Dependency;

namespace Thex.Application.Adapters
{
    public interface IRoomBlockingAdapter : IScaffoldRoomBlockingAdapter, ITransientDependency
    {
    }
}
