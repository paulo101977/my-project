﻿using Thex.Domain.Entities;
using Thex.Dto;

using Tnf.Notifications;

namespace Thex.Application.Adapters
{
    public class RoomBlockingAdapter : ScaffoldRoomBlockingAdapter, IRoomBlockingAdapter
    {
        public RoomBlockingAdapter(INotificationHandler notificationHandler)
            : base(notificationHandler)
        {
        }

        public override RoomBlocking.Builder Map(RoomBlocking entity, RoomBlockingDto dto)
        {
            var builder = base.Map(entity, dto);
            return builder;
        }

        public override RoomBlocking.Builder Map(RoomBlockingDto dto)
        {
            var builder = base.Map(dto);
            return builder;
        }
    }
}
