﻿using Tnf.Dependency;

namespace Thex.Application.Adapters
{
    public interface ISubscriptionAdapter : IScaffoldSubscriptionAdapter, ITransientDependency
    {
    }
}
