﻿using Thex.Domain.Entities;
using Thex.Dto;

using Tnf.Notifications;

namespace Thex.Application.Adapters
{
    public class SubscriptionAdapter : ScaffoldSubscriptionAdapter, ISubscriptionAdapter
    {
        public SubscriptionAdapter(INotificationHandler notificationHandler)
            : base(notificationHandler)
        {
        }

        public override Subscription.Builder Map(Subscription entity, SubscriptionDto dto)
        {
            var builder = base.Map(entity, dto);
            return builder;
        }

        public override Subscription.Builder Map(SubscriptionDto dto)
        {
            var builder = base.Map(dto);
            return builder;
        }
    }
}
