﻿using Tnf.Dependency;

namespace Thex.Application.Adapters
{
    public interface IBillingInvoicePropertySupportedTypeAdapter : IScaffoldBillingInvoicePropertySupportedTypeAdapter, ITransientDependency
    {
    }
}
