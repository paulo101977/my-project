﻿using Thex.Domain.Entities;
using Thex.Dto;
using Thex.Kernel;
using Tnf.Notifications;

namespace Thex.Application.Adapters
{
    public class BillingInvoicePropertySupportedTypeAdapter : ScaffoldBillingInvoicePropertySupportedTypeAdapter, IBillingInvoicePropertySupportedTypeAdapter
    {
        public BillingInvoicePropertySupportedTypeAdapter(
            INotificationHandler notificationHandler,
            IApplicationUser applicationUser)
            : base(notificationHandler, applicationUser)
        {
        }

        public override BillingInvoicePropertySupportedType.Builder Map(BillingInvoicePropertySupportedType entity, BillingInvoicePropertySupportedTypeDto dto)
        {
            var builder = base.Map(entity, dto);
            return builder;
        }

        public override BillingInvoicePropertySupportedType.Builder Map(BillingInvoicePropertySupportedTypeDto dto)
        {
            var builder = base.Map(dto);
            return builder;
        }
    }
}
