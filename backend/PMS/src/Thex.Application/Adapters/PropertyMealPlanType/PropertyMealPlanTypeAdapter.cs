﻿using Thex.Domain.Entities;
using Thex.Dto;

using Tnf.Notifications;

namespace Thex.Application.Adapters
{
    public class PropertyMealPlanTypeAdapter : ScaffoldPropertyMealPlanTypeAdapter, IPropertyMealPlanTypeAdapter
    {
        public PropertyMealPlanTypeAdapter(INotificationHandler notificationHandler)
            : base(notificationHandler)
        {
        }

        public override PropertyMealPlanType.Builder Map(PropertyMealPlanType entity, PropertyMealPlanTypeDto dto)
        {
            var builder = base.Map(entity, dto);
            return builder;
        }

        public override PropertyMealPlanType.Builder Map(PropertyMealPlanTypeDto dto)
        {
            var builder = base.Map(dto);
            return builder;
        }
    }
}
