﻿using Tnf.Dependency;

namespace Thex.Application.Adapters
{
    public interface IPropertyMealPlanTypeAdapter : IScaffoldPropertyMealPlanTypeAdapter, ITransientDependency
    {
    }
}
