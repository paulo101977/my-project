﻿using Thex.Domain.Entities;
using Thex.Dto;

using Tnf.Notifications;

namespace Thex.Application.Adapters
{
    public class MarketSegmentAdapter : ScaffoldMarketSegmentAdapter, IMarketSegmentAdapter
    {
        public MarketSegmentAdapter(INotificationHandler notificationHandler)
            : base(notificationHandler)
        {
        }

        public override MarketSegment.Builder Map(MarketSegment entity, MarketSegmentDto dto)
        {
            var builder = base.Map(entity, dto);
            return builder;
        }

        public override MarketSegment.Builder Map(MarketSegmentDto dto)
        {
            var builder = base.Map(dto);
            return builder;
        }
    }
}
