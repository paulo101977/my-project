﻿using Tnf.Dependency;

namespace Thex.Application.Adapters
{
    public interface ICountryCurrencyAdapter : IScaffoldCountryCurrencyAdapter, ITransientDependency
    {
    }
}
