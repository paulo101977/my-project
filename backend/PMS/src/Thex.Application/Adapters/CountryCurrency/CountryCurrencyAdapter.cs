﻿using Thex.Domain.Entities;
using Thex.Dto;

using Tnf.Notifications;

namespace Thex.Application.Adapters
{
    public class CountryCurrencyAdapter : ScaffoldCountryCurrencyAdapter, ICountryCurrencyAdapter
    {
        public CountryCurrencyAdapter(INotificationHandler notificationHandler)
            : base(notificationHandler)
        {
        }

        public override CountryCurrency.Builder Map(CountryCurrency entity, CountryCurrencyDto dto)
        {
            var builder = base.Map(entity, dto);
            return builder;
        }

        public override CountryCurrency.Builder Map(CountryCurrencyDto dto)
        {
            var builder = base.Map(dto);
            return builder;
        }
    }
}
