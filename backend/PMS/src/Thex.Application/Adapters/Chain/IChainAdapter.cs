﻿using Tnf.Dependency;

namespace Thex.Application.Adapters
{
    public interface IChainAdapter : IScaffoldChainAdapter, ITransientDependency
    {
    }
}
