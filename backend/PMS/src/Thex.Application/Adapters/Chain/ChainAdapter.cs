﻿using Thex.Domain.Entities;
using Thex.Dto;

using Tnf.Notifications;

namespace Thex.Application.Adapters
{
    public class ChainAdapter : ScaffoldChainAdapter, IChainAdapter
    {
        public ChainAdapter(INotificationHandler notificationHandler)
            : base(notificationHandler)
        {
        }

        public override Chain.Builder Map(Chain entity, ChainDto dto)
        {
            var builder = base.Map(entity, dto);
            return builder;
        }

        public override Chain.Builder Map(ChainDto dto)
        {
            var builder = base.Map(dto);
            return builder;
        }
    }
}
