﻿using Tnf.Dependency;

namespace Thex.Application.Adapters
{
    public interface IBillingAccountItemTypeAdapter : IScaffoldBillingAccountItemTypeAdapter, ITransientDependency
    {
    }
}
