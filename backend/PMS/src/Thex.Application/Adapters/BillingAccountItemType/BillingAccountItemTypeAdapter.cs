﻿using Thex.Domain.Entities;
using Thex.Dto;

using Tnf.Notifications;

namespace Thex.Application.Adapters
{
    public class BillingAccountItemTypeAdapter : ScaffoldBillingAccountItemTypeAdapter, IBillingAccountItemTypeAdapter
    {
        public BillingAccountItemTypeAdapter(INotificationHandler notificationHandler)
            : base(notificationHandler)
        {
        }

        public override BillingAccountItemType.Builder Map(BillingAccountItemType entity, BillingAccountItemTypeDto dto)
        {
            var builder = base.Map(entity, dto);
            return builder;
        }

        public override BillingAccountItemType.Builder Map(BillingAccountItemTypeDto dto)
        {
            var builder = base.Map(dto);
            return builder;
        }
    }
}
