﻿using Tnf.Dependency;

namespace Thex.Application.Adapters
{
    public interface IReservationConfirmationAdapter : IScaffoldReservationConfirmationAdapter, ITransientDependency
    {
    }
}
