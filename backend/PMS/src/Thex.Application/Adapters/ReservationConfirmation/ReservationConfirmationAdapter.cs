﻿using Thex.Domain.Entities;
using Thex.Dto;

using Tnf.Notifications;

namespace Thex.Application.Adapters
{
    public class ReservationConfirmationAdapter : ScaffoldReservationConfirmationAdapter, IReservationConfirmationAdapter
    {
        public ReservationConfirmationAdapter(INotificationHandler notificationHandler)
            : base(notificationHandler)
        {
        }

        public override ReservationConfirmation.Builder Map(ReservationConfirmation entity, ReservationConfirmationDto dto)
        {
            var builder = base.Map(entity, dto);
            return builder;
        }

        public override ReservationConfirmation.Builder Map(ReservationConfirmationDto dto)
        {
            var builder = base.Map(dto);
            return builder;
        }
    }
}
