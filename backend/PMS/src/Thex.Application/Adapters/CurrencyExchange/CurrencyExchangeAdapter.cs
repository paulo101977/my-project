﻿using Thex.Domain.Entities;
using Thex.Dto;

using Tnf.Notifications;

namespace Thex.Application.Adapters
{
    public class CurrencyExchangeAdapter : ScaffoldCurrencyExchangeAdapter, ICurrencyExchangeAdapter
    {
        public CurrencyExchangeAdapter(INotificationHandler notificationHandler)
            : base(notificationHandler)
        {
        }

        public override CurrencyExchange.Builder Map(CurrencyExchange entity, CurrencyExchangeDto dto)
        {
            var builder = base.Map(entity, dto);
            return builder;
        }

        public override CurrencyExchange.Builder Map(CurrencyExchangeDto dto)
        {
            var builder = base.Map(dto);
            return builder;
        }
    }
}
