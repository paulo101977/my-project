﻿using Tnf.Dependency;

namespace Thex.Application.Adapters
{
    public interface ICurrencyExchangeAdapter : IScaffoldCurrencyExchangeAdapter, ITransientDependency
    {
    }
}
