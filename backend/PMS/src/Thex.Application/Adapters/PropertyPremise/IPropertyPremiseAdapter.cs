﻿using Tnf.Dependency;

namespace Thex.Application.Adapters
{
    public interface IPropertyPremiseAdapter : IScaffoldPropertyPremiseAdapter, ITransientDependency
    {
    }
}
