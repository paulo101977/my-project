﻿using Thex.Domain.Entities;
using Thex.Dto;
using Tnf.Notifications;

namespace Thex.Application.Adapters
{
    public class PropertyPremiseAdapter : ScaffoldPropertyPremiseAdapter, IPropertyPremiseAdapter
    {
        public PropertyPremiseAdapter(INotificationHandler notificationHandler)
            : base(notificationHandler)
        {
        }

        public override PropertyPremise.Builder Map(PropertyPremise entity, PropertyPremiseDto dto)
        {
            var builder = base.Map(entity, dto);
            return builder;
        }

        public override PropertyPremise.Builder Map(PropertyPremiseDto dto)
        {
            var builder = base.Map(dto);
            return builder;
        }
    }
}
