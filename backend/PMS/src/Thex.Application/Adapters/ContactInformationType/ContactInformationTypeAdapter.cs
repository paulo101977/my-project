﻿using Thex.Domain.Entities;
using Thex.Dto;

using Tnf.Notifications;

namespace Thex.Application.Adapters
{
    public class ContactInformationTypeAdapter : ScaffoldContactInformationTypeAdapter, IContactInformationTypeAdapter
    {
        public ContactInformationTypeAdapter(INotificationHandler notificationHandler)
            : base(notificationHandler)
        {
        }

        public override ContactInformationType.Builder Map(ContactInformationType entity, ContactInformationTypeDto dto)
        {
            var builder = base.Map(entity, dto);
            return builder;
        }

        public override ContactInformationType.Builder Map(ContactInformationTypeDto dto)
        {
            var builder = base.Map(dto);
            return builder;
        }
    }
}
