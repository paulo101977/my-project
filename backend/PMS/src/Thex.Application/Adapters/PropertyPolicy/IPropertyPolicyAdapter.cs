﻿using Tnf.Dependency;

namespace Thex.Application.Adapters
{
    public interface IPropertyPolicyAdapter : IScaffoldPropertyPolicyAdapter, ITransientDependency
    {
    }
}
