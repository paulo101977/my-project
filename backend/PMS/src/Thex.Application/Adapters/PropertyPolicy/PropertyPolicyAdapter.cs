﻿using Thex.Domain.Entities;
using Thex.Dto;

using Tnf.Notifications;

namespace Thex.Application.Adapters
{
    public class PropertyPolicyAdapter : ScaffoldPropertyPolicyAdapter, IPropertyPolicyAdapter
    {
        public PropertyPolicyAdapter(INotificationHandler notificationHandler)
            : base(notificationHandler)
        {
        }

        public override PropertyPolicy.Builder Map(PropertyPolicy entity, PropertyPolicyDto dto)
        {
            var builder = base.Map(entity, dto);
            return builder;
        }

        public override PropertyPolicy.Builder Map(PropertyPolicyDto dto)
        {
            var builder = base.Map(dto);
            return builder;
        }
    }
}
