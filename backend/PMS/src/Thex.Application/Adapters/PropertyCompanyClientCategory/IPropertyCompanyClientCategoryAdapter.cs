﻿using Tnf.Dependency;

namespace Thex.Application.Adapters
{
    public interface IPropertyCompanyClientCategoryAdapter : IScaffoldPropertyCompanyClientCategoryAdapter, ITransientDependency
    {
    }
}
