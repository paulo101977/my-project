﻿using Thex.Domain.Entities;
using Thex.Dto;

using Tnf.Notifications;

namespace Thex.Application.Adapters
{
    public class PropertyCompanyClientCategoryAdapter : ScaffoldPropertyCompanyClientCategoryAdapter, IPropertyCompanyClientCategoryAdapter
    {
        public PropertyCompanyClientCategoryAdapter(INotificationHandler notificationHandler)
            : base(notificationHandler)
        {
        }

        public override PropertyCompanyClientCategory.Builder Map(PropertyCompanyClientCategory entity, PropertyCompanyClientCategoryDto dto)
        {
            var builder = base.Map(entity, dto);
            return builder;
        }

        public override PropertyCompanyClientCategory.Builder Map(PropertyCompanyClientCategoryDto dto)
        {
            var builder = base.Map(dto);
            return builder;
        }
    }
}
