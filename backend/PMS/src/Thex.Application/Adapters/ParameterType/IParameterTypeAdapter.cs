﻿using Tnf.Dependency;

namespace Thex.Application.Adapters
{
    public interface IParameterTypeAdapter : IScaffoldParameterTypeAdapter, ITransientDependency
    {
    }
}
