﻿using Thex.Domain.Entities;
using Thex.Dto;

using Tnf.Notifications;

namespace Thex.Application.Adapters
{
    public class ParameterTypeAdapter : ScaffoldParameterTypeAdapter, IParameterTypeAdapter
    {
        public ParameterTypeAdapter(INotificationHandler notificationHandler)
            : base(notificationHandler)
        {
        }

        public override ParameterType.Builder Map(ParameterType entity, ParameterTypeDto dto)
        {
            var builder = base.Map(entity, dto);
            return builder;
        }

        public override ParameterType.Builder Map(ParameterTypeDto dto)
        {
            var builder = base.Map(dto);
            return builder;
        }
    }
}
