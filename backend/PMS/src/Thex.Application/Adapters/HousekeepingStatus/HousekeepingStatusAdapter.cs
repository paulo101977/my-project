﻿using Thex.Domain.Entities;
using Thex.Dto;

using Tnf.Notifications;

namespace Thex.Application.Adapters
{
    public class HousekeepingStatusAdapter : ScaffoldHousekeepingStatusAdapter, IHousekeepingStatusAdapter
    {
        public HousekeepingStatusAdapter(INotificationHandler notificationHandler)
            : base(notificationHandler)
        {
        }

        public override HousekeepingStatus.Builder Map(HousekeepingStatus entity, HousekeepingStatusDto dto)
        {
            var builder = base.Map(entity, dto);
            return builder;
        }

        public override HousekeepingStatus.Builder Map(HousekeepingStatusDto dto)
        {
            var builder = base.Map(dto);
            return builder;
        }
    }
}
