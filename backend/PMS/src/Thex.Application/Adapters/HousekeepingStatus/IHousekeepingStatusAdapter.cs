﻿using Tnf.Dependency;

namespace Thex.Application.Adapters
{
    public interface IHousekeepingStatusAdapter : IScaffoldHousekeepingStatusAdapter, ITransientDependency
    {
    }
}
