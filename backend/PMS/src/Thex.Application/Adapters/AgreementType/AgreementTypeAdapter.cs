﻿using Thex.Domain.Entities;
using Thex.Dto;

using Tnf.Notifications;

namespace Thex.Application.Adapters
{
    public class AgreementTypeAdapter : ScaffoldAgreementTypeAdapter, IAgreementTypeAdapter
    {
        public AgreementTypeAdapter(INotificationHandler notificationHandler)
            : base(notificationHandler)
        {
        }

        public override AgreementType.Builder Map(AgreementType entity, AgreementTypeDto dto)
        {
            var builder = base.Map(entity, dto);
            return builder;
        }

        public override AgreementType.Builder Map(AgreementTypeDto dto)
        {
            var builder = base.Map(dto);
            return builder;
        }
    }
}
