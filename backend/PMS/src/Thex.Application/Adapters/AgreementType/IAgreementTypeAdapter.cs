﻿using Tnf.Dependency;

namespace Thex.Application.Adapters
{
    public interface IAgreementTypeAdapter : IScaffoldAgreementTypeAdapter, ITransientDependency
    {
    }
}
