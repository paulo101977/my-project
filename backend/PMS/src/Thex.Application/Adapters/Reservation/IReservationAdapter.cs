﻿using Tnf.Dependency;

namespace Thex.Application.Adapters
{
    public interface IReservationAdapter : IScaffoldReservationAdapter, ITransientDependency
    {
    }
}
