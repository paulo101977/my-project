﻿using Thex.Domain.Entities;
using Thex.Dto;

using Tnf.Notifications;

namespace Thex.Application.Adapters
{
    public class ReservationAdapter : ScaffoldReservationAdapter, IReservationAdapter
    {
        public ReservationAdapter(INotificationHandler notificationHandler)
            : base(notificationHandler)
        {
        }

        public override Reservation.Builder Map(Reservation entity, ReservationDto dto)
        {
            var builder = base.Map(entity, dto);
            return builder;
        }

        public override Reservation.Builder Map(ReservationDto dto)
        {
            var builder = base.Map(dto);
            return builder;
        }
    }
}
