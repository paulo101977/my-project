﻿using Thex.Domain.Entities;
using Thex.Dto;

using Tnf.Notifications;

namespace Thex.Application.Adapters
{
    public class PlasticAdapter : ScaffoldPlasticAdapter, IPlasticAdapter
    {
        public PlasticAdapter(INotificationHandler notificationHandler)
            : base(notificationHandler)
        {
        }

        public override Plastic.Builder Map(Plastic entity, PlasticDto dto)
        {
            var builder = base.Map(entity, dto);
            return builder;
        }

        public override Plastic.Builder Map(PlasticDto dto)
        {
            var builder = base.Map(dto);
            return builder;
        }
    }
}
