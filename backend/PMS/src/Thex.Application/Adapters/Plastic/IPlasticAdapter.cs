﻿using Tnf.Dependency;

namespace Thex.Application.Adapters
{
    public interface IPlasticAdapter : IScaffoldPlasticAdapter, ITransientDependency
    {
    }
}
