﻿using Thex.Domain.Entities;
using Thex.Dto;
using Tnf.Dependency;

namespace Thex.Application.Adapters
{
    public interface IPropertyMealPlanTypeRateHeaderAdapter : IScaffoldPropertyMealPlanTypeRateHeaderAdapter, ITransientDependency
    {
        PropertyMealPlanTypeRateHeader.Builder MapWithHistory(PropertyMealPlanTypeRateHeaderDto dto);
    }
}
