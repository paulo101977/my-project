﻿using System;
using System.Collections.Generic;
using Thex.Domain.Entities;
using Thex.Dto;
using Tnf;
using Tnf.Notifications;

namespace Thex.Application.Adapters
{
    public class PropertyMealPlanTypeRateHeaderAdapter : ScaffoldPropertyMealPlanTypeRateHeaderAdapter, IPropertyMealPlanTypeRateHeaderAdapter
    {
        private readonly PropertyMealPlanTypeRateHistoryAdapter _propertyMelPlanTypeRateHistoryAdapter;

        public PropertyMealPlanTypeRateHeaderAdapter(
            INotificationHandler notificationHandler,
            PropertyMealPlanTypeRateHistoryAdapter propertyMelPlanTypeRateHistoryAdapter
        )
            : base(notificationHandler)
        {
            _propertyMelPlanTypeRateHistoryAdapter = propertyMelPlanTypeRateHistoryAdapter;
        }

        public override PropertyMealPlanTypeRateHeader.Builder Map(PropertyMealPlanTypeRateHeader entity, PropertyMealPlanTypeRateHeaderDto dto)
        {
            var builder = base.Map(entity, dto);
            return builder;
        }

        public override PropertyMealPlanTypeRateHeader.Builder Map(PropertyMealPlanTypeRateHeaderDto dto)
        {
            var builder = base.Map(dto);
            return builder;
        }

        public PropertyMealPlanTypeRateHeader.Builder MapWithHistory(PropertyMealPlanTypeRateHeaderDto dto)
        {
            Check.NotNull(dto, nameof(dto));

            var historyList = new List<PropertyMealPlanTypeRateHistory>();

            foreach (var prop in dto.PropertyMealPlanTypeRateHistoryList)
            {
                prop.Id = Guid.NewGuid();
                prop.PropertyId = dto.PropertyId;
                prop.TenantId = dto.TenantId;

                historyList.Add(_propertyMelPlanTypeRateHistoryAdapter.Map(prop).Build());
            }

            var builder = base.Map(dto)
                .WithHistoryList(historyList);

            return builder;
        }
    }
}
