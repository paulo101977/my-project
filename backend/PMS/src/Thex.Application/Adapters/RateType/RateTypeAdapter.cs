﻿using Thex.Domain.Entities;
using Thex.Dto;
using Tnf.Notifications;

namespace Thex.Application.Adapters
{
    public class RateTypeAdapter : ScaffoldRateTypeAdapter, IRateTypeAdapter
    {
        public RateTypeAdapter(INotificationHandler notificationHandler)
            : base(notificationHandler)
        {
        }

        public override RateType.Builder Map(RateType entity, RateTypeDto dto)
        {
            var builder = base.Map(entity, dto);
            return builder;
        }

        public override RateType.Builder Map(RateTypeDto dto)
        {
            var builder = base.Map(dto);
            return builder;
        }
    }
}
