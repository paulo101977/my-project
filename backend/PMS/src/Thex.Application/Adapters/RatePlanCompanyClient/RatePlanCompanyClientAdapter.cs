﻿using Thex.Domain.Entities;
using Thex.Dto;

using Tnf.Notifications;

namespace Thex.Application.Adapters
{
    public class RatePlanCompanyClientAdapter : ScaffoldRatePlanCompanyClientAdapter, IRatePlanCompanyClientAdapter
    {
        public RatePlanCompanyClientAdapter(INotificationHandler notificationHandler)
            : base(notificationHandler)
        {
        }

        public override RatePlanCompanyClient.Builder Map(RatePlanCompanyClient entity, RatePlanCompanyClientDto dto)
        {
            var builder = base.Map(entity, dto);
            return builder;
        }

        public override RatePlanCompanyClient.Builder Map(RatePlanCompanyClientDto dto)
        {
            var builder = base.Map(dto);
            return builder;
        }
    }
}
