﻿using Tnf.Dependency;

namespace Thex.Application.Adapters
{
    public interface IRatePlanCompanyClientAdapter : IScaffoldRatePlanCompanyClientAdapter, ITransientDependency
    {
    }
}
