﻿using Tnf.Dependency;

namespace Thex.Application.Adapters
{
    public interface IPropertyGuestTypeAdapter : IScaffoldPropertyGuestTypeAdapter, ITransientDependency
    {
    }
}
