﻿using Thex.Domain.Entities;
using Thex.Dto;

using Tnf.Notifications;

namespace Thex.Application.Adapters
{
    public class PropertyGuestTypeAdapter : ScaffoldPropertyGuestTypeAdapter, IPropertyGuestTypeAdapter
    {
        public PropertyGuestTypeAdapter(INotificationHandler notificationHandler)
            : base(notificationHandler)
        {
        }

        public override PropertyGuestType.Builder Map(PropertyGuestType entity, PropertyGuestTypeDto dto)
        {
            var builder = base.Map(entity, dto);
            return builder;
        }

        public override PropertyGuestType.Builder Map(PropertyGuestTypeDto dto)
        {
            var builder = base.Map(dto);
            return builder;
        }
    }
}
