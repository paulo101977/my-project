﻿using Tnf.Dependency;

namespace Thex.Application.Adapters
{
    public interface IPlasticBrandPropertyAdapter : IScaffoldPlasticBrandPropertyAdapter, ITransientDependency
    {
    }
}
