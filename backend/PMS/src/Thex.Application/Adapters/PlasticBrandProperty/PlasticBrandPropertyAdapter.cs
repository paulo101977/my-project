﻿using Thex.Domain.Entities;
using Thex.Dto;

using Tnf.Notifications;

namespace Thex.Application.Adapters
{
    public class PlasticBrandPropertyAdapter : ScaffoldPlasticBrandPropertyAdapter, IPlasticBrandPropertyAdapter
    {
        public PlasticBrandPropertyAdapter(INotificationHandler notificationHandler)
            : base(notificationHandler)
        {
        }

        public override PlasticBrandProperty.Builder Map(PlasticBrandProperty entity, PlasticBrandPropertyDto dto)
        {
            var builder = base.Map(entity, dto);
            return builder;
        }

        public override PlasticBrandProperty.Builder Map(PlasticBrandPropertyDto dto)
        {
            var builder = base.Map(dto);
            return builder;
        }
    }
}
