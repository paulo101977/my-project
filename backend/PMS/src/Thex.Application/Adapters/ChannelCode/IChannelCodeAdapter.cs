﻿using Tnf.Dependency;

namespace Thex.Application.Adapters
{
    public interface IChannelCodeAdapter : IScaffoldChannelCodeAdapter, ITransientDependency
    {
    }
}
