﻿using Thex.Domain.Entities;
using Thex.Dto;
using Tnf.Notifications;

namespace Thex.Application.Adapters
{
    public class ChannelCodeAdapter : ScaffoldChannelCodeAdapter, IChannelCodeAdapter
    {
        public ChannelCodeAdapter(INotificationHandler notificationHandler)
            : base(notificationHandler)
        {
        }

        public override ChannelCode.Builder Map(ChannelCode entity, ChannelCodeDto dto)
        {
            var builder = base.Map(entity, dto);
            return builder;
        }

        public override ChannelCode.Builder Map(ChannelCodeDto dto)
        {
            var builder = base.Map(dto);
            return builder;
        }
    }
}
