﻿using Tnf.Dependency;

namespace Thex.Application.Adapters
{
    public interface ICompanyClientContactPersonAdapter : IScaffoldCompanyClientContactPersonAdapter, ITransientDependency
    {
    }
}
