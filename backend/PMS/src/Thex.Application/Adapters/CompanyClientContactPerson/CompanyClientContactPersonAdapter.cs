﻿using Thex.Domain.Entities;
using Thex.Dto;

using Tnf.Notifications;

namespace Thex.Application.Adapters
{
    public class CompanyClientContactPersonAdapter : ScaffoldCompanyClientContactPersonAdapter, ICompanyClientContactPersonAdapter
    {
        public CompanyClientContactPersonAdapter(INotificationHandler notificationHandler)
            : base(notificationHandler)
        {
        }

        public override CompanyClientContactPerson.Builder Map(CompanyClientContactPerson entity, CompanyClientContactPersonDto dto)
        {
            var builder = base.Map(entity, dto);
            return builder;
        }

        public override CompanyClientContactPerson.Builder Map(CompanyClientContactPersonDto dto)
        {
            var builder = base.Map(dto);
            return builder;
        }
    }
}
