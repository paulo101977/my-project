﻿using Thex.Domain.Entities;
using Thex.Dto;
using Tnf.Notifications;

namespace Thex.Application.Adapters
{
    public class CurrencyAdapter : ScaffoldCurrencyAdapter, ICurrencyAdapter
    {
        public CurrencyAdapter(INotificationHandler notificationHandler)
            : base(notificationHandler)
        {
        }

        public override Currency.Builder Map(Currency entity, CurrencyDto dto)
        {
            var builder = base.Map(entity, dto);
            return builder;
        }

        public override Currency.Builder Map(CurrencyDto dto)
        {
            var builder = base.Map(dto);
            return builder;
        }
    }
}
