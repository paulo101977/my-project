﻿using Tnf.Dependency;

namespace Thex.Application.Adapters
{
    public interface ICurrencyAdapter : IScaffoldCurrencyAdapter, ITransientDependency
    {
    }
}
