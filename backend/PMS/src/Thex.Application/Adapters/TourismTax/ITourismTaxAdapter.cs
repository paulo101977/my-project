﻿using Thex.Domain.Entities;
using Thex.Dto;

namespace Thex.Application.Adapters
{
    public interface ITourismTaxAdapter
    {
        TourismTax.Builder MapToCreate(TourismTaxDto dto, int propertyId);
        TourismTax.Builder MapToUpdate(TourismTax entity, TourismTaxDto dto);
    }
}
