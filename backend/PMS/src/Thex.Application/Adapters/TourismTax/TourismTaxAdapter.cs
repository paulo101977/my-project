﻿using System;
using Thex.Domain.Entities;
using Thex.Dto;
using Tnf;
using Tnf.Notifications;

namespace Thex.Application.Adapters
{
    public class TourismTaxAdapter : ITourismTaxAdapter
    {
        public INotificationHandler NotificationHandler { get; }

        public TourismTaxAdapter(
            INotificationHandler notificationHandler)
        {
            NotificationHandler = notificationHandler;
        }

        public TourismTax.Builder MapToCreate(TourismTaxDto dto, int propertyId)
        {
            Check.NotNull(dto, nameof(dto));

            var builder = new TourismTax.Builder(NotificationHandler)
                    .WithId(Guid.NewGuid())
                    .WithBillingItemId(dto.BillingItemId)
                    .WithIsActive(true)
                    .WithAmount(dto.Amount)
                    .WithIsIntegratedFiscalDocument(true)
                    .WithIsTotalOfNights(dto.IsTotalOfNights)
                    .WithNumberOfNights(dto.NumberOfNights)
                    .WithPropertyId(propertyId)
                    .WithLaunchType(dto.LaunchType);

            return builder;
        }

        public TourismTax.Builder MapToUpdate(TourismTax entity, TourismTaxDto dto)
        {
            Check.NotNull(entity, nameof(entity));
            Check.NotNull(dto, nameof(dto));

            var builder = new TourismTax.Builder(NotificationHandler, entity)
                    .WithBillingItemId(dto.BillingItemId)
                    .WithAmount(dto.Amount)
                    .WithIsTotalOfNights(dto.IsTotalOfNights)
                    .WithNumberOfNights(dto.NumberOfNights)
                    .WithLaunchType(dto.LaunchType);

            return builder;
        }
    }
}
