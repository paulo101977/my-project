﻿using Thex.Domain.Entities;
using Thex.Dto;

using Tnf.Notifications;

namespace Thex.Application.Adapters
{
    public class ReasonCategoryAdapter : ScaffoldReasonCategoryAdapter, IReasonCategoryAdapter
    {
        public ReasonCategoryAdapter(INotificationHandler notificationHandler)
            : base(notificationHandler)
        {
        }

        public override ReasonCategory.Builder Map(ReasonCategory entity, ReasonCategoryDto dto)
        {
            var builder = base.Map(entity, dto);
            return builder;
        }

        public override ReasonCategory.Builder Map(ReasonCategoryDto dto)
        {
            var builder = base.Map(dto);
            return builder;
        }
    }
}
