﻿using Tnf.Dependency;

namespace Thex.Application.Adapters
{
    public interface IReasonCategoryAdapter : IScaffoldReasonCategoryAdapter, ITransientDependency
    {
    }
}
