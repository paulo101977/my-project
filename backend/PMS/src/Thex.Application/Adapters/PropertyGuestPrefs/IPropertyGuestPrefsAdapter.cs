﻿using Tnf.Dependency;

namespace Thex.Application.Adapters
{
    public interface IPropertyGuestPrefsAdapter : IScaffoldPropertyGuestPrefsAdapter, ITransientDependency
    {
    }
}
