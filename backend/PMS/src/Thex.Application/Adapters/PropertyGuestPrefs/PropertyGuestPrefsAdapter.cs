﻿using Thex.Domain.Entities;
using Thex.Dto;

using Tnf.Notifications;

namespace Thex.Application.Adapters
{
    public class PropertyGuestPrefsAdapter : ScaffoldPropertyGuestPrefsAdapter, IPropertyGuestPrefsAdapter
    {
        public PropertyGuestPrefsAdapter(INotificationHandler notificationHandler)
            : base(notificationHandler)
        {
        }

        public override PropertyGuestPrefs.Builder Map(PropertyGuestPrefs entity, PropertyGuestPrefsDto dto)
        {
            var builder = base.Map(entity, dto);
            return builder;
        }

        public override PropertyGuestPrefs.Builder Map(PropertyGuestPrefsDto dto)
        {
            var builder = base.Map(dto);
            return builder;
        }
    }
}
