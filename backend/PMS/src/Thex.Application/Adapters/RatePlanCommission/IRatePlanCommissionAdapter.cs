﻿using Tnf.Dependency;

namespace Thex.Application.Adapters
{
    public interface IRatePlanCommissionAdapter : IScaffoldRatePlanCommissionAdapter, ITransientDependency
    {
    }
}
