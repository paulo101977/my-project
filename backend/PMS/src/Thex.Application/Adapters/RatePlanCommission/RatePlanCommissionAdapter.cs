﻿using Thex.Domain.Entities;
using Thex.Dto;

using Tnf.Notifications;

namespace Thex.Application.Adapters
{
    public class RatePlanCommissionAdapter : ScaffoldRatePlanCommissionAdapter, IRatePlanCommissionAdapter
    {
        public RatePlanCommissionAdapter(INotificationHandler notificationHandler)
            : base(notificationHandler)
        {
        }

        public override RatePlanCommission.Builder Map(RatePlanCommission entity, RatePlanCommissionDto dto)
        {
            var builder = base.Map(entity, dto);
            return builder;
        }

        public override RatePlanCommission.Builder Map(RatePlanCommissionDto dto)
        {
            var builder = base.Map(dto);
            return builder;
        }
    }
}
