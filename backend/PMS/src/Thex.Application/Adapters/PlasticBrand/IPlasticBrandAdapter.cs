﻿using Tnf.Dependency;

namespace Thex.Application.Adapters
{
    public interface IPlasticBrandAdapter : IScaffoldPlasticBrandAdapter, ITransientDependency
    {
    }
}
