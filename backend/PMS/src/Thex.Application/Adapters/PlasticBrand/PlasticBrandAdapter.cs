﻿using Thex.Domain.Entities;
using Thex.Dto;

using Tnf.Notifications;

namespace Thex.Application.Adapters
{
    public class PlasticBrandAdapter : ScaffoldPlasticBrandAdapter, IPlasticBrandAdapter
    {
        public PlasticBrandAdapter(INotificationHandler notificationHandler)
            : base(notificationHandler)
        {
        }

        public override PlasticBrand.Builder Map(PlasticBrand entity, PlasticBrandDto dto)
        {
            var builder = base.Map(entity, dto);
            return builder;
        }

        public override PlasticBrand.Builder Map(PlasticBrandDto dto)
        {
            var builder = base.Map(dto);
            return builder;
        }
    }
}
