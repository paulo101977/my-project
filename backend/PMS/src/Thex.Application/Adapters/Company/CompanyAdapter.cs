﻿using Thex.Domain.Entities;
using Thex.Dto;

using Tnf.Notifications;

namespace Thex.Application.Adapters
{
    public class CompanyAdapter : ScaffoldCompanyAdapter, ICompanyAdapter
    {
        public CompanyAdapter(INotificationHandler notificationHandler)
            : base(notificationHandler)
        {
        }

        public override Company.Builder Map(Company entity, CompanyDto dto)
        {
            var builder = base.Map(entity, dto);
            return builder;
        }

        public override Company.Builder Map(CompanyDto dto)
        {
            var builder = base.Map(dto);
            return builder;
        }
    }
}
