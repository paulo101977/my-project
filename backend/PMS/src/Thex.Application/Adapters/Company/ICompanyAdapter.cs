﻿using Tnf.Dependency;

namespace Thex.Application.Adapters
{
    public interface ICompanyAdapter : IScaffoldCompanyAdapter, ITransientDependency
    {
    }
}
