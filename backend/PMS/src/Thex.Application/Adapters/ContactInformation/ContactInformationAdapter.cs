﻿using Thex.Domain.Entities;
using Thex.Dto;

using Tnf.Notifications;

namespace Thex.Application.Adapters
{
    public class ContactInformationAdapter : ScaffoldContactInformationAdapter, IContactInformationAdapter
    {
        public ContactInformationAdapter(INotificationHandler notificationHandler)
            : base(notificationHandler)
        {
        }

        public override ContactInformation.Builder Map(ContactInformation entity, ContactInformationDto dto)
        {
            var builder = base.Map(entity, dto);
            return builder;
        }

        public override ContactInformation.Builder Map(ContactInformationDto dto)
        {
            var builder = base.Map(dto);
            return builder;
        }
    }
}
