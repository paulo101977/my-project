﻿using Tnf.Dependency;

namespace Thex.Application.Adapters
{
    public interface IContactInformationAdapter : IScaffoldContactInformationAdapter, ITransientDependency
    {
    }
}
