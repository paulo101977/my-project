﻿using Thex.Domain.Entities;
using Thex.Dto;
using Tnf.Dependency;

namespace Thex.Application.Adapters
{
    public interface IPropertyPremiseHeaderAdapter : IScaffoldPropertyPremiseHeaderAdapter, ITransientDependency
    {
        PropertyPremiseHeader.Builder MapWithPremise(PropertyPremiseHeaderDto dto);
    }
}
