﻿using Thex.Domain.Entities;
using Tnf;
using Thex.Dto;
using Tnf.Notifications;
using System.Collections.Generic;
using System;

namespace Thex.Application.Adapters
{
    public class PropertyPremiseHeaderAdapter : ScaffoldPropertyPremiseHeaderAdapter, IPropertyPremiseHeaderAdapter
    {
        private readonly PropertyPremiseAdapter _propertyPremiseAdapter;

        public PropertyPremiseHeaderAdapter(
            PropertyPremiseAdapter propertyPremiseAdapter,
            INotificationHandler notificationHandler
        )
            : base(notificationHandler)
        {
            _propertyPremiseAdapter = propertyPremiseAdapter;
        }

        public override PropertyPremiseHeader.Builder Map(PropertyPremiseHeader entity, PropertyPremiseHeaderDto dto)
        {
            var builder = base.Map(entity, dto);
            return builder;
        }

        public override PropertyPremiseHeader.Builder Map(PropertyPremiseHeaderDto dto)
        {
            var builder = base.Map(dto);
            return builder;
        }

        public PropertyPremiseHeader.Builder MapWithPremise(PropertyPremiseHeaderDto dto)
        {
            Check.NotNull(dto, nameof(dto));

            var premiseList = new List<PropertyPremise>();

            foreach (var prop in dto.PropertyPremiseList)
            {
                prop.Id = Guid.NewGuid();
                prop.PropertyId = dto.PropertyId;
                prop.TenantId = dto.TenantId;

                premiseList.Add(_propertyPremiseAdapter.Map(prop).Build());
            }

            var builder = new PropertyPremiseHeader.Builder(NotificationHandler)
                .WithId(dto.Id)
                .WithPropertyId(dto.PropertyId)
                .WithInitialDate(dto.InitialDate)
                .WithFinalDate(dto.FinalDate)
                .WithCurrencyId(dto.CurrencyId)
                .WithTenantId(dto.TenantId)
                .WithIsActive(true)
                .WithName(dto.Name)
                .WithPaxReference(dto.PaxReference)
                .WithRoomTypeReferenceId(dto.RoomTypeReferenceId)
                .WithPropertyPremiseList(premiseList);

            return builder;
        }
    }
}
