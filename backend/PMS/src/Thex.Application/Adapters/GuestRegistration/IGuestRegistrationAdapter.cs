﻿using Tnf.Dependency;

namespace Thex.Application.Adapters
{
    public interface IGuestRegistrationAdapter : IScaffoldGuestRegistrationAdapter, ITransientDependency
    {
    }
}
