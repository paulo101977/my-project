﻿using Thex.Domain.Entities;
using Thex.Dto;
using Thex.Kernel;
using Tnf.Notifications;

namespace Thex.Application.Adapters
{
    public class GuestRegistrationAdapter : ScaffoldGuestRegistrationAdapter, IGuestRegistrationAdapter
    {
        public GuestRegistrationAdapter(INotificationHandler notificationHandler, IApplicationUser applicationUser)
            : base(notificationHandler, applicationUser)
        {
        }

        public override GuestRegistration.Builder Map(GuestRegistration entity, GuestRegistrationDto dto)
        {
            var builder = base.Map(entity, dto);
            return builder;
        }

        public override GuestRegistration.Builder Map(GuestRegistrationDto dto)
        {
            if (dto.GuestTypeId == 0)
                dto.GuestTypeId = null;

            var builder = base.Map(dto);
            return builder;
        }
    }
}
