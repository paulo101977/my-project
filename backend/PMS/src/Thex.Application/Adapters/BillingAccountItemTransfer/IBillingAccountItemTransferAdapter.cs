﻿using Tnf.Dependency;

namespace Thex.Application.Adapters
{
    public interface IBillingAccountItemTransferAdapter : IScaffoldBillingAccountItemTransferAdapter, ITransientDependency
    {
    }
}
