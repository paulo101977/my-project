﻿using Thex.Domain.Entities;
using Thex.Dto;

using Tnf.Notifications;

namespace Thex.Application.Adapters
{
    public class BillingAccountItemTransferAdapter : ScaffoldBillingAccountItemTransferAdapter, IBillingAccountItemTransferAdapter
    {
        public BillingAccountItemTransferAdapter(INotificationHandler notificationHandler)
            : base(notificationHandler)
        {
        }

        public override BillingAccountItemTransfer.Builder Map(BillingAccountItemTransfer entity, BillingAccountItemTransferDto dto)
        {
            var builder = base.Map(entity, dto);
            return builder;
        }

        public override BillingAccountItemTransfer.Builder Map(BillingAccountItemTransferDto dto)
        {
            var builder = base.Map(dto);
            return builder;
        }
    }
}
