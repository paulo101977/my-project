﻿using Thex.Domain.Entities;
using Thex.Dto;

using Tnf.Notifications;

namespace Thex.Application.Adapters
{
    public class CountrySubdivisionTranslationAdapter : ScaffoldCountrySubdivisionTranslationAdapter, ICountrySubdivisionTranslationAdapter
    {
        public CountrySubdivisionTranslationAdapter(INotificationHandler notificationHandler)
            : base(notificationHandler)
        {
        }

        public override CountrySubdivisionTranslation.Builder Map(CountrySubdivisionTranslation entity, CountrySubdivisionTranslationDto dto)
        {
            var builder = base.Map(entity, dto);
            return builder;
        }

        public override CountrySubdivisionTranslation.Builder Map(CountrySubdivisionTranslationDto dto)
        {
            var builder = base.Map(dto);
            return builder;
        }
    }
}
