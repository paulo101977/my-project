﻿using Thex.Domain.Entities;
using Thex.Dto;

using Tnf.Notifications;

namespace Thex.Application.Adapters
{
    public class GuestAdapter : ScaffoldGuestAdapter, IGuestAdapter
    {
        public GuestAdapter(INotificationHandler notificationHandler)
            : base(notificationHandler)
        {
        }

        public override Guest.Builder Map(Guest entity, GuestDto dto)
        {
            var builder = base.Map(entity, dto);
            return builder;
        }

        public override Guest.Builder Map(GuestDto dto)
        {
            var builder = base.Map(dto);
            return builder;
        }
    }
}
