﻿using Tnf.Dependency;

namespace Thex.Application.Adapters
{
    public interface IGuestAdapter : IScaffoldGuestAdapter, ITransientDependency
    {
    }
}
