﻿using System.Collections.Generic;
using Thex.Domain.Entities;
using Thex.Dto;
using Tnf.Dependency;

namespace Thex.Application.Adapters
{
    public interface IReservationBudgetAdapter : IScaffoldReservationBudgetAdapter, ITransientDependency
    {
        IList<ReservationBudget> Map(IList<ReservationBudgetDto> dto);
        IList<ReservationBudget> Map(IList<ReservationBudget> currentBudgetList, IList<ReservationBudgetDto> newBudgetList, long reservationItemId, bool isSeparated);
    }
}
