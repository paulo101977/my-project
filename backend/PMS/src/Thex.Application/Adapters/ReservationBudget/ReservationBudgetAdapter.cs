﻿using System.Collections.Generic;
using System.Linq;
using Thex.Domain.Entities;
using Thex.Dto;

using Tnf.Notifications;

namespace Thex.Application.Adapters
{
    public class ReservationBudgetAdapter : ScaffoldReservationBudgetAdapter, IReservationBudgetAdapter
    {
        public ReservationBudgetAdapter(INotificationHandler notificationHandler)
            : base(notificationHandler)
        {
        }

        public override ReservationBudget.Builder Map(ReservationBudget entity, ReservationBudgetDto dto)
        {
            var builder = base.Map(entity, dto);
            return builder;
        }

        public override ReservationBudget.Builder Map(ReservationBudgetDto dto)
        {
            var builder = base.Map(dto);
            return builder;
        }

        public IList<ReservationBudget> Map(IList<ReservationBudgetDto> dto)
        {
            var builder = new List<ReservationBudget>();

            foreach (var item in dto)
                builder.Add(base.Map(item).Build());

            return builder;
        }

        public IList<ReservationBudget> Map(IList<ReservationBudget> currentBudgetList, IList<ReservationBudgetDto> newBudgetList, long reservationItemId, bool isSeparated)
        {
            var budgets = new List<ReservationBudget>();

            foreach (var item in newBudgetList)
            {
                var oldItem = currentBudgetList.FirstOrDefault(x => x.Id == item.Id);
                item.ReservationItemId = reservationItemId;

                if (isSeparated)
                {
                    item.SeparatedRate = item.ManualRate - oldItem.ManualRate;
                    //TODO: item.ManualRate = oldItem.ManualRate;
                }

                if (oldItem != null)
                    budgets.Add(Map(oldItem, item).Build());
                else
                    budgets.Add(Map(item).Build());
            }

            return budgets;
        }
    }
}
