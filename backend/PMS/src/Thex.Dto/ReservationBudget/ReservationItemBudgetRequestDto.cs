﻿using System;
using System.Collections.Generic;
using Tnf.Dto;

namespace Thex.Dto
{
    public class ReservationItemBudgetRequestDto : BaseDto
    {
        public long Id { get; set; }
        
        public DateTime InitialDate { get; set; }
        
        public DateTime FinalDate { get; set; }

        public DateTime? SystemDate { get; set; }

        public int? RoomTypeId { get; set; }

        public List<int> RoomTypeIdList { get; set; }

        public int? RoomTypeIdSort { get; set; }

        public int? AdultCount { get; set; }

        public List<int> ChildrenAge { get; set; }

        public long? ReservationItemId { get; set; }

        public Guid? CurrencyId { get; set; }

        public int? MealPlanTypeId { get; set; }

        public Guid? RatePlanId { get; set; }

        public Guid? CompanyClientId { get; set; }

        public bool TotalBudgetDescending { get; set; }
    }
}
