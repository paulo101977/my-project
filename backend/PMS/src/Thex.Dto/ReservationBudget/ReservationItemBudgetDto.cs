﻿using System;
using System.Collections.Generic;
using Tnf.Dto;

namespace Thex.Dto
{
    public class ReservationItemBudgetHeaderDto : BaseDto
    {
        public static ReservationItemBudgetHeaderDto NullInstance = null;

        public long Id { get; set; }
        public long? ReservationItemId { get; set; }
        public string ReservationItemCode { get; set; }
        public int? MealPlanTypeId { get; set; }
        public Guid? RatePlanId { get; set; }
        public Guid? CurrencyId { get; set; }
        public string ReservationBudgetCurrencySymbol { get; set; }
        public int? GratuityTypeId { get; set; }
        public bool IsSeparated { get; set; }
        public List<ReservationItemCommercialBudgetDto> CommercialBudgetList { get; set; }

        public List<ReservationBudgetDto> ReservationBudgetDtoList { get; set; }
    }

    public class ReservationItemCommercialBudgetDto : BaseDto
    {
        public long Id { get; set; }
        public Guid CurrencyId { get; set; }
        public int RoomTypeId { get; set; }
        public int Order { get; set; }
        public int MealPlanTypeId { get; set; }
        public Guid? RatePlanId { get; set; }
        public Guid? CompanyClientId { get; set; }

        public int? AgreementTypeId { get; set; }        

        public string RoomTypeAbbreviation { get; set; }
        public string MealPlanTypeName { get; set; }

        public string CommercialBudgetName { get; set; }
        public string CurrencySymbol { get; set; }

        public decimal RateVariation { get; set; }
        public decimal TotalBudget { get; set; }

        public int MealPlanTypeDefaultId { get; set; }
        public string MealPlanTypeDefaultName { get; set; }
        public bool IsMealPlanTypeDefault { get; set; }       

        public List<ReservationItemCommercialBudgetDayDto> CommercialBudgetDayList { get; set; }
    }
    
    public class ReservationItemCommercialBudgetDayDto : BaseDto
    {
        public long Id { get; set; }
        
        public DateTime Day { get; set; }

        public decimal BaseRateAmount { get; set; }
        public decimal ChildRateAmount { get; set; }
        public decimal MealPlanAmount { get; set; }
        public decimal ChildMealPlanAmount { get; set; }       

        public decimal? BaseRateAmountWithRatePlan { get; set; }
        public decimal? ChildRateAmountWithRatePlan { get; set; }
        public decimal? MealPlanAmountWithRatePlan { get; set; }
        public decimal? ChildMealPlanAmountWithRatePlan { get; set; }

        public string CurrencySymbol { get; set; }
        public Guid CurrencyId { get; set; }
        public int MealPlanTypeId { get; set; }
        public Guid? RatePlanId { get; set; }

        public Guid? BaseRateHeaderHistoryId { get; set; }

        public bool Overbooking { get; set; }
        public bool OverbookingLimit { get; set; }



        public decimal MinimumRate { get; set; }
        public decimal MaximumRate { get; set; }

        public bool RateLimitExceeded { get; set; }

        public decimal Total
        {
            get { 
                decimal _total = 0;

                if(!RatePlanId.HasValue)
                {
                    _total += BaseRateAmount;
                    _total += ChildRateAmount;
                    _total += MealPlanAmount;
                    _total += ChildMealPlanAmount;                    
                }
                else
                {
                    _total += BaseRateAmountWithRatePlan ?? 0;
                    _total += ChildRateAmountWithRatePlan ?? 0;
                    _total += MealPlanAmountWithRatePlan ?? 0;
                    _total += ChildMealPlanAmountWithRatePlan ?? 0;
                }

                if (_total < MinimumRate)
                {
                    _total = MinimumRate;
                    RateLimitExceeded = true;
                }                  
                else if (_total > MaximumRate && MaximumRate != 0)
                {
                    _total = MaximumRate;
                    RateLimitExceeded = true;
                }                  

                return _total;
            }
        }


    }

    public class ReservationItemCommercialBudgetValuesDto
    {
        public decimal? BaseRateAmount { get; set; }
        public decimal? ChildRateAmount { get; set; }
        public decimal? MealPlanAmount { get; set; }
        public decimal? ChildMealPlanAmount { get; set; }

        public decimal Total
        {
            get
            {
                decimal _total = 0;

                _total += BaseRateAmount ?? 0;
                _total += ChildRateAmount ?? 0;
                _total += MealPlanAmount ?? 0;
                _total += ChildMealPlanAmount ?? 0;
              
                return _total;
            }
        }

    }

}
