﻿namespace Thex.Dto.PropertyAuditProcessStep
{
    public class PropertyAuditProcessStepNumber
    {
        public int StepNumber { get; set; }
    }
}
