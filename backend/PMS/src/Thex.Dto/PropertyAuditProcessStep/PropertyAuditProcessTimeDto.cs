﻿using System;

namespace Thex.Dto.PropertyAuditProcessStep
{
    public class PropertyAuditProcessTimeDto
    {
        public int Hour { get; set; }
        public int Minutes { get; set; }
        public bool PropertyAuditProcessIsAvailable { get; set; }
        public DateTime CurrentSystemDate { get; set; }
        public DateTime NextSystemDate { get; set; }
    }
}
