﻿using System.Collections.Generic;

namespace Thex.Dto
{
    public partial class GetAllPropertyRateStrategyDto
    {
        public List<string> RoomTypeAbbreviationList { get; set; }

        public GetAllPropertyRateStrategyDto()
        {
            RoomTypeAbbreviationList = new List<string>();
        }
    }
}
