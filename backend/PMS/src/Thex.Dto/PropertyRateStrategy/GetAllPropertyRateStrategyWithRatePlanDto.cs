﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Thex.Dto
{
    public partial class GetAllPropertyRateStrategyWithRatePlanDto
    {
        public List<RoomTypeDto> RoomTypeList { get; set; }      
        public List<PropertyRateStrategyRoomTypeDto> PropertyRateStrategyRoomTypeList { get; set; }

        public GetAllPropertyRateStrategyWithRatePlanDto()
        {
            RoomTypeList = new List<RoomTypeDto>();
            PropertyRateStrategyRoomTypeList = new List<PropertyRateStrategyRoomTypeDto>();
        }
    }
}
