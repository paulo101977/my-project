﻿using Tnf.Dto;

namespace Thex.Dto
{
    public class PaymentTypeWithBillingItemDto : BaseDto
    {
        public int Id { get; set; }
        public string PaymentTypeName { get; set; }
        public int? BillingItemId { get; set; }
    }
}
