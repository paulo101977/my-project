﻿using Tnf.Dto;

namespace Thex.Dto
{
    public partial class GetAllCountryDto : RequestAllDto
    {
        public string LanguageIsoCode { get; set; }
    }
}
