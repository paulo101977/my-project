﻿
namespace Thex.Dto.CountrySubdivisionTranslation
{
    public class AddressTranslationDto
    {
        public int CityId { get; set; }
        public string CityName { get; set; }
        public int StateId { get; set; }
        public string StateName { get; set; }
        public int CountryId { get; set; }
        public string CountryName { get; set; }
        public string LanguageIsoCode { get; set; }
        public string TwoLetterIsoCode { get; set; }
        public string CountryTwoLetterIsoCode { get; set; }
    }
}
