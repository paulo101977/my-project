﻿using Tnf.Dto;

namespace Thex.Dto
{
    public class BaseCountrySubdvisionTranslationDto : BaseDto
    {
        public int Id { get; set; }
        public virtual int TranslationId { get; set; }
        public virtual string TwoLetterIsoCode { get; set; }
        public virtual string CountryTwoLetterIsoCode { get; set; }
        public virtual string Name { get; set; }
        public virtual string LanguageIsoCode { get; set; }

    }
}
