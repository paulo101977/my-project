﻿using System;
using System.Collections.Generic;
using Tnf.Dto;

namespace Thex.Dto.Availability
{
    public class AvailabilityDto : BaseDto
    {
        public string Id => Guid.NewGuid().ToString();

        public List<AvailabilityRoomTypeRowDto> RoomTypeList { get; set; }
        public List<AvailabilityRoomTypeColumnDto> AvailabilityList { get; set; }
        public List<AvailabilityFooterRowDto> FooterList { get; set; }
        public List<RoomBlockingDto> RoomsBlocked { get; set; }
    }
    
    public class AvailabilityRoomTypeRowDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Total { get; set; }
        public int Order { get; set; }
    }

    public class AvailabilityRoomTypeColumnDto
    {
        public long RoomTypeId { get; set; }
        public DateTime Day { get; set; }
        public string Date { get; set; }
        public int AvailableRooms { get; set; }
        public int BlockedQuantity { get; set; }
        public int Rooms { get; set; }
        public int RoomTotal { get; set; }
        public decimal Occupation { get; set; }

        public int AvailableRoomsWithoutBlocked { get; set; }

        public string DateFormatted { get; set; }
    }

    public class AvailabilityFooterRowDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Total { get; set; }
        public List<AvailabilityFooterColumnDto> Values { get; set; }
    }

    public class AvailabilityFooterColumnDto
    {
        public string Date { get; set; }
        public decimal Number { get; set; }

        public string DateFormatted { get; set; }
    }

}
