﻿using System;
using System.Collections.Generic;
using System.Text;
using Tnf.Dto;

namespace Thex.Dto.Availability
{
    public class AvailabilityRoomsDto : BaseDto
    {
        public string Id => Guid.NewGuid().ToString();

        public List<AvailabilityRoomRowDto> RoomList { get; set; }
        public List<AvailabilityReservationColumnsDto> ReservationList { get; set; }
    }

    public class AvailabilityRoomRowDto
    {
        public int Id { get; set; }
        public int RoomId { get; set; }
        public string Name { get; set; }
        public int[] Beds { get; set; }
    }

    public class AvailabilityReservationColumnsDto
    {
        public long Id { get; set; }
        public int? RoomId { get; set; }
        public long? ReservationId { get; set; }
        public long? RoomTypeId { get; set; }
        public string Start { get; set; }
        public string End { get; set; }
        public string GuestName { get; set; }
        public int AdultCount { get; set; }
        public int ChildCount { get; set; }
        public int Status { get; set; }
        public bool isConjugated { get; set; }
        public string ReservationCode { get; set; }
        public string RoomNumber { get; set; }
        public string RoomTypeName { get; set; }
        public long ReservationItemId { get; set; }
        public int ReservationItemStatusId { get; set; }


        public string StartFormatted { get; set; }
        public string EndFormatted { get; set; }
    }

}
