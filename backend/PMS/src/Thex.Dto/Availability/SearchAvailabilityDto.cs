//  <copyright file="SearchAvailabilityDto.cs" company="TOTVS">
//  Copyright (c) TOTVS. All rights reserved.
//  </copyright>
namespace Thex.Dto.Reservation
{
    public class SearchAvailabilityDto
    {
        public string InitialDate { get; set; }

        public int Period { get; set; }
    }
}