﻿using System;
using Thex.Common.Enumerations;
using Tnf.Dto;

namespace Thex.Dto.RatePlan
{
    public class RatePlanWithRatePlanRoomTypeDto : BaseDto
    {
        public Guid Id { get; set; }

        public Guid RatePlanId { get; set; }
        public int RoomTypeId { get; set; }
        public decimal PercentualAmmount { get; set; }
        public bool IsDecreased { get; set; }
        public int RateTypeId { get; set; }
        public int? RoundingTypeId { get; set; }

        public bool IsBaseRateType()
        {
            return RateTypeId == (int)RatePlanTypeEnum.BaseRate;
        }
    }
}
