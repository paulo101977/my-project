﻿namespace Thex.Dto
{
    public partial class RatePlanDto
    {
        public string AgreementTypeName { get; set; }        
        public string PropertyMealPlanTypeName { get; set; }
        public string CurrencyName { get; set; }
    }
}
