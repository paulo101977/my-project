﻿using System;

namespace Thex.Dto
{
    public partial class GetAllRatePlanDto
    {
        public string SearchData { get; set; }
        public Guid? CompanyClientId { get; set; }
        public int? RoomTypeId { get; set; }
        public Guid? CurrencyId { get; set; }
        public DateTime? CreationTime { get; set; }
    }
}
