﻿using System;
using System.Collections.Generic;
using System.Text;
using Thex.Dto;
using Tnf.Dto;

namespace Thex.Dto.RatePlan
{
    public class HeaderRatePlanDto : BaseDto
    {
        public HeaderRatePlanDto()
        {
            CurrencyList = new ListDto<CurrencyDto>();
            AgreementTypeList = new ListDto<AgreementTypeDto>();
            PropertyMealPlanTypeList = new ListDto<PropertyMealPlanTypeDto>();
            PropertyRateTypeList = new ListDto<RateTypeDto>();
        }
        public string Id => Guid.NewGuid().ToString();
        public ListDto<CurrencyDto> CurrencyList { get; set; }
        public ListDto<AgreementTypeDto> AgreementTypeList { get; set; }
        public ListDto<PropertyMealPlanTypeDto> PropertyMealPlanTypeList { get; set; }
        public ListDto<RateTypeDto> PropertyRateTypeList { get; set; }
        public IListDto<MarketSegmentDto> MarketSegmentList { get; set; }
    }
}
