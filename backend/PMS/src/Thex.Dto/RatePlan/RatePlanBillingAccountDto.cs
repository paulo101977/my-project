﻿using System;

namespace Thex.Dto
{
    public class RatePlanBillingAccountDto
    {
        public Guid RatePlanId { get; set; }
        public bool RateNet { get; set; }
        public Guid BillingAccountId { get; set; }
    }
}
