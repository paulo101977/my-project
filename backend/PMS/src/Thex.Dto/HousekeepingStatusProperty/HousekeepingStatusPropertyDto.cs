﻿namespace Thex.Dto
{
    public partial class HousekeepingStatusPropertyDto
    {
        public string HousekeepingStatusName { get; set; }
        public bool? IsActive { get; set; }
        public bool VisibleBtnIsActive { get; set; }
        
       
    }
}
