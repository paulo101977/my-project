﻿using Newtonsoft.Json;
using System;
using Tnf.Dto;

namespace Thex.Dto.HousekeepingStatusProperty
{
    public class RoomWithReservationsDto : BaseDto
    {
        public long Id { get; set; }
        public int RoomId { get; set; }
        public string ReservationCode { get; set; }
        public DateTime EstimatedArrivalDate { get; set; }
        public DateTime EstimatedDepartureDate { get; set; }

    }
}
