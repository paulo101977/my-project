﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Thex.Dto.HousekeepingStatusProperty
{
    public class HousekeppingPropertyForAlterStatusDto
    {
        public Guid HousekeppingStatusPropertyId { get; set; }
        public string HousekeppingStatusName { get; set; }
    }
}
