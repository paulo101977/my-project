﻿using System;
using Tnf.Dto;

namespace Thex.Dto.SearchReservationReportDto
{
    public class SearchReservationReportResultDto : BaseDto
    {
        public long Id { get; set; }

        public string GuestName { get; set; }

        public int GuestTypeId { get; set; }

        public string GuestTypeName { get; set; }

        public long ReservationId { get; set; }

        public string ReservationCode { get; set; }

        public string RoomNumber { get; set; }

        public string RequestedRoomTypeAbbreviation { get; set; }

        public string AdultsAndChildren { get; set; }      

        public int? Adult { get; set; }

        public int? Child { get; set; }

        public string Client { get; set; }

        public decimal MealPlanTypeAmount { get; set; }

        public string MealPlan { get; set; }

        public string MealPlanLastDay { get; set; }

        public bool IsBudget { get; set; }

        public bool IsBudgetLastDay { get; set; }

        public string MealPlanType { get; set; }

        public int? MealPlanTypeId { get; set; }

        public decimal DailyValue { get; set; }

        public DateTime ArrivalDate { get; set; }

        public DateTime DepartureDate { get; set; }

        public bool IsInternalUsage { get; set; }

        public bool IsCourtesy { get; set; }

        public bool IsAdult { get; set; }

        public bool IsChild { get; set; }

        public bool IsMain { get; set; }

        public bool IsCompanion { get; set; }

        public bool EnsuresNoShow { get; set; }

        public decimal? Deposit { get; set; }

        public int? AgreementTypeId { get; set; }

        public string AgreementName { get; set; }

        public string UserName { get; set; }


        public string ReservationItemCode { get; set; }

        public long ReservationItemId { get; set; }

        public int ReservationItemStatusId { get; set; }

        public int GuestReservationItemStatusId { get; set; }

        public long GuestReservationItemId { get; set; }

        public int RequestedRoomTypeId { get; set; }

        public string RequestedRoomTypeName { get; set; }

        public DateTime? CancellationDate { get; set; }

        public int ReceivedRoomTypeId { get; set; }

        public string ReceivedRoomTypeName { get; set; }

        public string ReceivedRoomTypeAbbreviation { get; set; }

        public long? GuestId { get; set; }

        public bool GuestIsIncognito { get; set; }

        public string GuestLanguage { get; set; }

        public string GroupName { get; set; }

        public DateTime CreationTime { get; set; }

        public string ReservationBy { get; set; }

        public DateTime? Deadline { get; set; }

        public string GuestDocument { get; set; }

        public double Total { get; set; }

        public Guid BillingAccountId { get; set; }

        public string ReservationItemStatusName { get; set; }

        public string GuestReservationItemStatusName { get; set; }

        public Guid TenantId { get; set; }

        public string CurrencySymbol { get; set; }

        public string CompanyClientName { get; set; }

        public DateTime? CheckInDate { get; set; }

        public DateTime? CheckOutDate { get; set; }

        public DateTime? EstimatedArrivalDateGuest { get; set; }

        public DateTime? EstimatedDepartureDateGuest { get; set; }

        public string InternalComments { get; set; }

        public string ExternalComments { get; set; }

        public string PartnerComments { get; set; }
    }
}
