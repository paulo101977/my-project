﻿using Newtonsoft.Json;

namespace Thex.Dto.Proforma
{
    public class ProformaIntegrationItemDto
    {
        public int ServiceId { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("unit_price")]
        public decimal UnitPrice { get; set; }

        [JsonProperty("quantity")]
        public int Quantity { get; set; }
    }
}
