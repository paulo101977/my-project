﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Thex.Dto.Proforma
{
    public class ProformaDto
    {
        public string TokenClient { get; set; }

        public string IntegrationCode { get; set; }

        public Guid PropertyUUId { get; set; }

        [JsonProperty("date")]
        public DateTime Date { get; set; }

        [JsonProperty("due_date")]
        public DateTime DueDate { get; set; }

        [JsonProperty("tax_exemption")]
        public string TaxExemption { get; set; }

        [JsonProperty("observations")]
        public string Observations { get; set; }

        [JsonProperty("client")]
        public ProformaClientDto Client { get; set; }

        [JsonProperty("items")]
        public List<ProformaIntegrationItemDto> Items { get; set; }
    }

    public class ProformaClientDto
    {
        [JsonProperty("code")]
        public string Code { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("fiscal_id")]
        public string FiscalId { get; set; }
    }
}
