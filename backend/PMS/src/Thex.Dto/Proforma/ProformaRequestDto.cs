﻿namespace Thex.Dto
{
    public class ProformaRequestDto
    {
        public long? ReservationItemId { get; set; }
        public long? ReservationId { get; set; }
        public bool LaunchTourismTax { get; set; }
    }
}
