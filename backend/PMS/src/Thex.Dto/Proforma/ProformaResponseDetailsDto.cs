﻿using Newtonsoft.Json;

namespace Thex.Dto.Proforma
{
    public class ProformaResponseDetailsDto
    {
        [JsonProperty("id")]
        public string Id { get; set; }
    }
}
