﻿using Newtonsoft.Json;

namespace Thex.Dto.Proforma
{
    public class ProfromaResponseDto
    {
        [JsonProperty("proforma")]
        public ProformaResponseDetailsDto ResponseDetails { get; set; }
    }
}
