﻿using System;
using System.Collections.Generic;
using Tnf.Dto;

namespace Thex.Dto
{
    public class EntranceItemBudgetRequestDto : ReservationItemBudgetRequestDto
    {
        public DateTime SystemDate { get; set; }
    }
}
