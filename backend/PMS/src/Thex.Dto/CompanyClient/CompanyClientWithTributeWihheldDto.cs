﻿using System;
using Tnf.Dto;

namespace Thex.Dto
{
    public class CompanyClientWithTributeWihheldDto : BaseDto
    {
        public Guid Id { get; set; }
        public Guid OwnerId { get; set; }
        public Guid PropertyUId { get; set; }
    }
}
