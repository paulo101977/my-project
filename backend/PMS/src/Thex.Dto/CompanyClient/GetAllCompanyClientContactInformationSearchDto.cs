﻿using System;
using Tnf.Dto;

namespace Thex.Dto
{
    public partial class GetAllCompanyClientContactInformationSearchDto : RequestAllDto
    {
        public int PropertyId { get; set; }
        public string CompanyClientId { get; set; }
        public string SearchData { get; set; }
    }
}
