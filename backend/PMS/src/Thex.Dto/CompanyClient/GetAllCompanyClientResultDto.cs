﻿// //  <copyright file="GetAllCompanyClientResultDto.cs" company="TOTVS">
// //  Copyright (c) TOTVS. All rights reserved.
// //  </copyright>
namespace Thex.Dto
{
    using System;

    using Tnf.Dto;

    public class GetAllCompanyClientResultDto : BaseDto
    {
        public Guid Id { get; set; }
        public Boolean IsActive;

        public string Type;

        public string Name;

        public string Document;

        public string Email;

        public string Phone;

        public string HomePage;
        public Guid ClientCategoryId { get; set; }
        public string ClientCategoryName { get; set; }
        public string Address { get; set; }

        public string ExternalCode { get; set; }
    }
}