﻿using System;

namespace Thex.Dto
{
    public partial class GetAllCompanyClientDto
    {
        public int PropertyId { get; set; }
        public string SearchData { get; set; }
        public Guid? PropertyCompanyClientCategoryId { get; set; }
    }
}
