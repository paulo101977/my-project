﻿using System;
using Tnf.Dto;

namespace Thex.Dto
{
    public partial class GetAllCompanyClientSearchResultDto : BaseDto
    {
        public Guid Id { get; set; }
        public string Document;
        public string DocumentType;
        public int DocumentTypeId;
        public string ShortName;
        public string TradeName;
    }
}
