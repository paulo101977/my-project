﻿using System;
using Tnf.Dto;

namespace Thex.Dto
{
    public partial class GetAllCompanyClientContactInformationSearchResultDto : BaseDto
    {
        public Guid Id { get; set; }
        public string Name;
        public string Email;
        public string PhoneNumber;
    }
}
