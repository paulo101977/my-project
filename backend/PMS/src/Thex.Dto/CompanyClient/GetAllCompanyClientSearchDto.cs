﻿using Tnf.Dto;

namespace Thex.Dto
{
    public partial class GetAllCompanyClientSearchDto : RequestAllDto
    {
        public int PropertyId { get; set; }
        public string SearchData { get; set; }
        public string Language { get; set; }
        public string PersonType { get; set; }
        public int DocumentTypeId { get; set; }
    }
}
