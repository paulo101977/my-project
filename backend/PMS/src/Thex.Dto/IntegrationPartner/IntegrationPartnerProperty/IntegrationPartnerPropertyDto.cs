﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Thex.Dto.IntegrationPartner.IntegrationPartnerProperty
{
    public class IntegrationPartnerPropertyDto
    {
        public Guid Id { get; set; }
        public int PropertyId { get; set; }
        public bool IsActive { get; set; }
        public string IntegrationCode { get; set; }
        public string TokenClient { get; set; }
        public int PartnerId { get; set; }
        public string PartnerName { get; set; }
        public int? IntegrationPartnerType { get; set; }
        public string IntegrationNumber { get; set; }
    }
}
