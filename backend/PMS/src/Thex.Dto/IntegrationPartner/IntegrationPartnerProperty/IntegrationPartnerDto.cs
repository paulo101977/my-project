﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Thex.Dto.IntegrationPartner.IntegrationPartnerProperty
{
    public partial class IntegrationPartnerDto
    {
        public static IntegrationPartnerDto NullInstance = null;

        public int IntegrationPartnerId { get; set; }
        public string TokenClient { get; set; }
        public string TokenApplication { get; set; }
        public string IntegrationPartnerName { get; set; }
        public int IntegrationPartnerType { get; set; }
        public bool IsActive { get; set; }
    }
}
