﻿using System;
using System.Collections.Generic;

namespace Thex.Dto
{
    public partial class PropertyMealPlanTypeRateHeaderSelectDto
    {
        public static PropertyMealPlanTypeRateHeaderSelectDto NullInstance = null;

        public PropertyMealPlanTypeRateHeaderSelectDto()
        {
            PropertyMealPlanTypeRateList = new HashSet<PropertyMealPlanTypeRateDto>();
            PropertyMealPlanTypeRateHistoryList = new HashSet<PropertyMealPlanTypeRateHistoryDto>();
        }

        public Guid Id { get; set; }
        public int PropertyId { get; set; }

        public DateTime InitialDate { get; set; }
        public DateTime FinalDate { get; set; }
        public Guid CurrencyId { get; set; }
        public Guid TenantId { get; set; }

        public virtual CurrencyDto Currency { get; set; }
        public virtual ICollection<PropertyMealPlanTypeRateDto> PropertyMealPlanTypeRateList { get; set; }
        public virtual ICollection<PropertyMealPlanTypeRateHistoryDto> PropertyMealPlanTypeRateHistoryList { get; set; }
    }
}
