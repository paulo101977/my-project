﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Thex.Dto.PowerBI
{
    public class PowerBiMenuDto
    {
        public string AccessToken { get; set; }
        public ICollection<PowerBIGroupDto> PowerBiGroupDtoList { get; set; }
    }
}
