﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Thex.Dto.PowerBI
{
    public class PowerBIGroupDto
    {
        public string GroupId { get; set; }
        public string GroupName { get; set; }
        public ICollection<PowerBIDashboardDto> PowerBIDashboardDtoList { get; set; }
        public ICollection<PowerBIReportDto> PowerBIReportDtoList { get; set; }
    }
}
