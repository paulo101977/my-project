﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Thex.Dto.PowerBI
{
    public class PowerBIDashboardDto
    {
        public string PowerBIDashboardId { get; set; }
        public string Name { get; set; }
        public string EmbedUrl { get; set; }
        public Guid TenantId { get; set; }
        public PowerBIEmbedConfigDto EmbedConfigDto { get; set; }
    }
}
