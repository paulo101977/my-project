﻿using System;

namespace Thex.Dto.PowerBI
{
    public class PowerBIReportDto
    {
        public string PowerBIReportId { get; set; }
        public string Name { get; set; }
        public string EmbedUrl { get; set; }
        public Guid TenantId { get; set; }
        public string WebUrl { get; set; }
        public PowerBIEmbedConfigDto EmbedConfigDto { get; set; }
    }
}
