﻿namespace Thex.Dto.ReservationGuest
{
    using System;
    using Tnf.Dto;

    public class GetAllReservationGuestDto : RequestAllDto
    {
        public string SearchData { get; set; }
    }
}