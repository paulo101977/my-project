//  <copyright file="ReservationGuestResultDto.cs" company="TOTVS">
//  Copyright (c) TOTVS. All rights reserved.
//  </copyright>
namespace Thex.Dto.ReservationGuest
{
    using System;
    using Tnf.Dto;

    public class ReservationGuestResultDto : BaseDto
    {
        public long Id { get; set; }
        public long ReservationId { get; set; }
        public long ReservationItemId { get; set; }
        public long GuestReservationItemId { get; set; }
        public long? GuestId { get; set; }
        public string GuestName { get; set; }
        public string PersonId { get; set; }
        public string ReservationCode { get; set; }
        public string ReservationItemCode { get; set; }
        
        public DateTime ArrivalDate { get; set; }
        
        public DateTime DepartureDate { get; set; }
        public int ReservationItemStatus { get; set; }
        public string ReservationItemStatusName { get; set; }
        public Guid? BillingAccountId { get; set; }
        public Guid TenantId { get; set; }
    }
}