﻿namespace Thex.Dto
{
    public partial class TenantDto
    {
        public string ChainName { get; set; }
        public string PropertyName { get; set; }
    }
}
