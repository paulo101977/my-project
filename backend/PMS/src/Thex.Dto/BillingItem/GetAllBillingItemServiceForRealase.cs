﻿using System;
using System.Collections.Generic;
using System.Text;
using Tnf.Dto;

namespace Thex.Dto.BillingItem
{
    public partial class  GetAllBillingItemServiceForRealase : RequestAllDto
    {
        public string SearchData { get; set; }
        public int? GroupId { get; set; }
        public int? BillingItemGroupId { get; set; }
    }
}
