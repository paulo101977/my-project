﻿using System;
using System.Collections.Generic;
using System.Text;
using Tnf.Dto;

namespace Thex.Dto.BillingItem
{
    public class SearchBillingItemDto : RequestAllDto
    {
        public string Param { get; set; }
    }
}
