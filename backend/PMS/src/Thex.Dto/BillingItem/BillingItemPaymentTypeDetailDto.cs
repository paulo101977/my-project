﻿using System;
using System.Collections.Generic;
using System.Text;
using Tnf.Dto;

namespace Thex.Dto.BillingItem
{
  public  class BillingItemPaymentTypeDetailDto : BaseDto
    {
        public int Id { get; set; }

        public static BillingItemPaymentTypeDetailDto NullInstance = null;

        public BillingItemPaymentTypeDetailDto()
        {
            BillingItemPaymentTypeDetailConditionList = new HashSet<BillingItemAcquirerPaymentTypeDetailConditionDto>();
        }

        public int BillingItemPaymentTypeId { get; set; }
        public Guid PlasticBrandPropertyId { get; set; }
        public short MaximumInstallmentsQuantity { get; set; }
        public bool IsActive { get; set; }

        public virtual ICollection<BillingItemAcquirerPaymentTypeDetailConditionDto> BillingItemPaymentTypeDetailConditionList { get; set; }

        public string PlasticBrandPropertyName { get; set; }
        public int? PlasticBrandId { get; set; }
    }
}
