﻿using System;
using System.Collections.Generic;
using System.Text;
using Tnf.Dto;

namespace Thex.Dto.BillingItem
{
  public  class BillingItemPaymentTypeDto : BaseDto
    {
        public int Id { get; set; }
        public static BillingItemPaymentTypeDto NullInstance = null;

        public BillingItemPaymentTypeDto()
        {
            BillingItemPaymentTypeDetailList = new HashSet<BillingItemPaymentTypeDetailDto>();
        }

        public int PropertyId { get; set; }
        public int PaymentTypeId { get; set; }
        public Guid? AcquirerId { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
        public string IntegrationCode { get; set; }

        public virtual CompanyClientDto Acquirer { get; set; }
        public virtual ICollection<BillingItemPaymentTypeDetailDto> BillingItemPaymentTypeDetailList { get; set; }

        public string PaymentTypeName { get; set; }
        public string AcquirerName { get; set; }
    }
}
