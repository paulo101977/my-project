﻿using System;
using Tnf.Dto;

namespace Thex.Dto
{
    public class BillingItemPlasticBrandCompanyClientDto : BaseDto
    {
        public int Id { get; set; }
        public int AcquirerId { get; set; }
        public Guid PlasticBrandPropertyId { get; set; }
        public int PlasticBrandId { get; set; }
        public string PlasticBrandName { get; set; }
        public string AcquirerName { get; set; }
        public string IconName { get; set; }

        public int? MaximumInstallmentsQuantity { get; set; }


        public string PlasticBrandFormatted => PlasticBrandName + " (" + AcquirerName + ")";
    }
}
