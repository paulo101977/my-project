﻿using System;
using System.Collections.Generic;
using System.Text;
using Tnf.Dto;

namespace Thex.Dto.BillingItem
{
    public class GetAllBillingItemServiceDto : BaseDto
    {

        public GetAllBillingItemServiceDto()
        {
            this.ServiceAndTaxList = new List<BillingItemServiceAndTaxAssociateDto>();
        }

        public long Id { get; set; }
        public bool IsActive { get; set; }
        public string BillingItemName { get; set; }
        public string BillingItemCategoryName { get; set; }
        public int BillingItemCategoryId { get; set; }
        public string BillingItemCategoryNameIconName { get; set; }
        public string IconName { get; set; }
        public int? StandardCategoryId { get; set; }
        public List<BillingItemServiceAndTaxAssociateDto> ServiceAndTaxList { get; set; } = new List<BillingItemServiceAndTaxAssociateDto>();
    }
}
