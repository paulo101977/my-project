﻿using System;
using System.Collections.Generic;
using System.Text;
using Tnf.Dto;

namespace Thex.Dto.BillingItem
{
    public class GetAllBillingItemTaxDto : BaseDto
    {
        public long Id { get; set; }
        public bool IsActive { get; set; }
        public string BillingItemName { get; set; }
        public string BillingItemIconName { get; set; }
        public string BillingItemCategoryName { get; set; }
    }
}
