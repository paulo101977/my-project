﻿using System;

namespace Thex.Dto.BillingItem
{
   public class BillingItemServiceAndTaxAssociateDto
    {
        public BillingItemServiceAndTaxAssociateDto()
        {
            Identify = Guid.NewGuid();
        }

        public Guid? Identify { get; set; }
        public Guid? Id { get; set; }
        public int? BillingItemTaxId { get; set; }
        public int? BillingItemServiceId { get; set; }
        public string Name { get; set; }
        public bool IsActive { get; set; }
        public decimal TaxPercentage { get; set; }
        public DateTime BeginDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string CategoryName { get; set; }
    }
}
