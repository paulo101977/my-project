﻿using System;
using System.Collections.Generic;
using System.Text;
using Tnf.Dto;

namespace Thex.Dto.BillingItem
{
    public class BillingItemWithParametersDocumentsDto : BaseDto
    {
        public int Id { get; set; }
        public int BillingItemId { get; set; }
        public int BillingItemTypeId { get; set; }
        public string BillingItemName { get; set; }
        public string BillingInvoiceModelName { get; set; }
        public string BillingInvoicePropertySeries { get; set; }
        public string Code { get; set; }
    }
}
