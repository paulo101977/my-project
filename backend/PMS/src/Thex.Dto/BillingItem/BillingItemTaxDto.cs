﻿using System;
using System.Collections.Generic;
using System.Text;
using Tnf.Dto;

namespace Thex.Dto.BillingItem
{
  public  class BillingItemTaxDto : BaseDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool IsActive { get; set; }
        public int BillingItemCategoryId { get; set; }
        public int PropertyId { get; set; }
        public string IntegrationCode { get; set; }
        public List<BillingItemServiceAndTaxAssociateDto> ServiceAndTaxList { get; set; }

        public BillingItemTaxDto()
        {
            this.ServiceAndTaxList = new List<BillingItemServiceAndTaxAssociateDto>();
        }
    }
}
