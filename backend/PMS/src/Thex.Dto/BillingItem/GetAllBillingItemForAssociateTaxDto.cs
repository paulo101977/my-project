﻿using System;
using System.Collections.Generic;
using System.Text;
using Tnf.Dto;

namespace Thex.Dto.BillingItem
{
  public  class GetAllBillingItemForAssociateTaxDto : BaseDto
    {
        public long Id { get; set; }
        public string BillingItemName { get; set; }
        public string BillingItemCategoryName { get; set; }
        
    }
}
