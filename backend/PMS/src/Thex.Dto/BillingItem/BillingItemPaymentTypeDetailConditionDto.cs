﻿
using System;
using System.Collections.Generic;
using System.Text;
using Tnf.Dto;

namespace Thex.Dto.BillingItem
{
  public  class BillingItemAcquirerPaymentTypeDetailConditionDto : BaseDto
  {
        public Guid Id { get; set; }
        public static BillingItemAcquirerPaymentTypeDetailConditionDto NullInstance = null;

        public int BillingItemPaymentTypeDetailId { get; set; }
        public short InstallmentNumber { get; set; }
        public decimal CommissionPct { get; set; }
        public short PaymentDeadline { get; set; }
  }
}
