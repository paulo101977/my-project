﻿using System;
using System.Collections.Generic;

using System.Linq;
using System.Text;
using Tnf.Dto;

namespace Thex.Dto.BillingItem
{
    public class BillingItemServiceLancamentoDto : BaseDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool IsActive { get; set; }
        public int BillingItemCategoryId { get; set; }
        public string BillingItemIconName { get; set; }
        public int? StandardCategoryId { get; set; }
        public string billingItemCategoryName { get; set; }
        public List<BillingItemServiceAndTaxAssociateDto> ServiceAndTaxList { get; set; }
    }
}
