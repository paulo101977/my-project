﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Thex.Dto
{
    public class PropertyBaseRateByLevelRateResultDto
    {
        public Guid LevelId { get; set; }
        public string LevelName { get; set; }
        public ICollection<LevelRateCurrencyDto> LevelRateCurrencyList { get; set; }
        
    }
}
