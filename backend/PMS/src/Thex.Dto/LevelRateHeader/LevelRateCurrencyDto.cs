﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Thex.Dto
{
    public class LevelRateCurrencyDto
    {
        public Guid CurrencyId { get; set; }
        public string CurrencyName { get; set; }
        public ICollection<LevelRateResultDto> LevelRateList { get; set; }
        public ICollection<DateTime> DateListOutsideRateList { get; set; }
    }
}
