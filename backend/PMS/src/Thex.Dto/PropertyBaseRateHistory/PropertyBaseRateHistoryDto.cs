﻿namespace Thex.Dto
{
    public partial class PropertyBaseRateHistoryDto
    {
        public string CurrencyName { get; set; }
        
        public string MealPlanTypeName { get; set; }
        public string RoomTypeName { get; set; }
        public string CreatorUserName { get; set; }
    }
}
