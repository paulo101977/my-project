﻿using Tnf.Dto;

namespace Thex.Dto
{
    public class ReasonCategoryGetAllDto : BaseDto
    {
        public int Id { get; set; }
        public string ReasonCategoryName { get; set; }
    }
}
