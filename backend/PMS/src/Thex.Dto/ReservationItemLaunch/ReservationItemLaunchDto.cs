﻿using System;

namespace Thex.Dto
{
    public class ReservationItemLaunchDto
    {
        public Guid Id { get; set; }
        public int? QuantityOfTourismTaxLaunched { get; set; }
        public long ReservationItemId { get; set; }
        public long? GuestReservationItemId { get; set; }
    }
}
