﻿using System;
using System.Collections.Generic;
using Tnf.Dto;

namespace Thex.Dto
{
    public class PropertyBaseRateConfigurationParametersDto : BaseDto
    {
        public int Id { get; set; }
        public IListDto<CurrencyDto> CurrencyList { get; set; }
        public IListDto<PropertyMealPlanTypeDto> MealPlanTypeList { get; set; }
        public IListDto<RoomTypeDto> RoomTypeList { get; set; }
        public List<PropertyParameterDto> PropertyParameterList { get; set; }
    }
}
