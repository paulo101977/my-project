﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Thex.Dto
{
    public class PropertyBaseLevelRateDto
    {
        public ICollection<Guid> LevelRateIdList { get; set; }
        public DateTime InitialDate { get; set; }
        public DateTime EndDate { get; set; }
        public List<int> DaysOfWeekDto { get; set; }
    }
}
