﻿using System;
using System.Collections.Generic;
using Tnf.Dto;

namespace Thex.Dto
{
    public class BaseRateConfigurationDto : BaseDto
    {
        public int Id { get; set; }
        public int MealPlanTypeDefaultId { get; set; }
        public Guid CurrencyId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime FinalDate { get; set; }
        public List<BaseRateConfigurationRoomTypeDto> BaseRateConfigurationRoomTypeList { get; set; }
        public List<BaseRateConfigurationMealPlanTypeDto> BaseRateConfigurationMealPlanTypeList { get; set; }
        public List<int> DaysOfWeekDto { get; set; }
        public int PropertyId { get; set; }
        public Guid? RatePlanId { get; set; }
        public Guid? LevelId { get; set; }
        public Guid? LevelRateId { get; set; }
        public int PropertyBaseRateTypeId { get; set; }
        public bool PropertyMealPlanTypeRate { get; set; }
    }

    public class BaseRateConfigurationRoomTypeDto
    {
        public int RoomTypeId { get; set; }
        public decimal? Pax1Amount { get; set; }
        public decimal? Pax2Amount { get; set; }
        public decimal? Pax3Amount { get; set; }
        public decimal? Pax4Amount { get; set; }
        public decimal? Pax5Amount { get; set; }
        public decimal? PaxAdditionalAmount { get; set; }
        public decimal? Child1Amount { get; set; }
        public decimal? Child2Amount { get; set; }
        public decimal? Child3Amount { get; set; }
        public int Level { get; set; }

        public object this[string propertyName]
        {
            get { return this.GetType().GetProperty(propertyName).GetValue(this, null); }
            set { this.GetType().GetProperty(propertyName).SetValue(this, value, null); }
        }
    }

    public class BaseRateConfigurationMealPlanTypeDto
    {
        public decimal? AdultMealPlanAmount { get; set; }
        public decimal? Child1MealPlanAmount { get; set; }
        public decimal? Child2MealPlanAmount { get; set; }
        public decimal? Child3MealPlanAmount { get; set; }
        public int MealPlanTypeId { get; set; }

        public object this[string propertyName]
        {
            get { return this.GetType().GetProperty(propertyName).GetValue(this, null); }
            set { this.GetType().GetProperty(propertyName).SetValue(this, value, null); }
        }
    }
}
