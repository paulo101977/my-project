﻿using System;
using Tnf.Dto;

namespace Thex.Dto.BillingAccountItem
{
    public class BillingAccountItemDiscountDto : BaseDto
    {
        public Guid Id { get; set; }
        public Guid BillingAccountId { get; set; }
        public Guid BillingAccountItemId { get; set; }
        public int   BillingItemId { get; set; }
        public bool IsPercentual { get; set; }
        public decimal Percentual { get; set; }
        public int ReasonId { get; set; }
        public string BillingAccountItemObservation { get; set; }
        public decimal AmountDiscount { get; set; }
        public bool IsCredit { get; set; }
        public int PropertyId { get; set; }
        public Guid? CurrencyId { get; set; }
        public Guid? CurrencyExchangeReferenceId { get; set; }
        public Guid? CurrencyExchangeReferenceSecId { get; set; }
        public DateTime? DateParameter { get; set; }
    }
}
