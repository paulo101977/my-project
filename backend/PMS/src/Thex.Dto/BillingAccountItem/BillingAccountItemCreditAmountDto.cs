﻿using System;
using System.Globalization;

namespace Thex.Dto
{
    public class BillingAccountItemCreditAmountDto
    {
        public Guid Id { get; set; }
        public decimal DifferenceAmountAndCreditNote { get; set; }
        public decimal? CreditAmount { get; set; }
        public string BillingItemName { get; set; }
        public Guid? BillingInvoiceId { get; set; }
        public DateTime BillingAccountItemDate { get; set; }
        public string CurrencySymbol { get; set; }
        public int BillingItemId { get; set; }
        public int ReasonId { get; set; }
        public decimal DiscountAmount { get; set; }
        public Guid? BillingAccountItemParentId { get; set; }
        public Guid? BillingAccountId { get; set; }

        public string DifferenceAmountAndCreditNoteFormatted
        {
            get
            {
                string resultFormatted;

                if (DifferenceAmountAndCreditNote < 0)
                    resultFormatted = $"- {CurrencySymbol} {(DifferenceAmountAndCreditNote * (-1)).ToString("N", CultureInfo.CurrentCulture)}";
                else
                    resultFormatted = $"{CurrencySymbol} {DifferenceAmountAndCreditNote.ToString("N", CultureInfo.CurrentCulture)}";

                return resultFormatted;
            }
        }
    }
}
