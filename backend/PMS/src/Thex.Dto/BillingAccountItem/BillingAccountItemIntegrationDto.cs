﻿using System;
using System.Collections.Generic;

namespace Thex.Dto
{
    public class BillingAccountItemIntegrationDto : BillingAccountItemBaseDto
    {
        public BillingAccountItemIntegrationDto()
        {
            DetailList = new List<DebiIntegrationtDetail>();
        }

        public Guid Id { get; set; }
        public int PropertyId { get; set; }
        public Guid BillingAccountId { get; set; }
        public int BillingItemId { get; set; }
        public Guid ProductId { get; set; }
        public int BillingItemCategoryId { get; set; }
        public Guid TenantId { get; set; }
        public DateTime? DateParameter { get; set; }
        public List<DebiIntegrationtDetail> DetailList { get; set; }
        public int? BillingInvoiceTypeId { get; set; }
        public int? PaymentId { get; set; }
        public Guid? BillingInvoiceId { get; set; }
        public Guid? ParentId { get; set; }
        public string InvoiceNumber { get; set; }
        public string InvoiceSeries { get; set; }
        public DateTime InvoiceEmissionDate { get; set; }
        public string IntegratorId { get; set; }
    }

    public class DebiIntegrationtDetail : BillingAccountItemBaseDto
    {
        /// <summary>
        /// Id do produto para lançamentos de POS
        /// BillingItemId para lançamentos de serviço
        /// BillingItemTypeId para lançamentos de crédito
        /// </summary>
        public string ExternalId { get; set; }
        public string Description { get; set; }
        public string GroupDescription { get; set; }
        public string Code { get; set; }
        public decimal Quantity { get; set; }
        public int Type { get; set; }
    }
}
