﻿using System;
using System.Collections.Generic;
using Tnf.Dto;

namespace Thex.Dto
{
    public class BillingAccountItemTransferSourceDestinationDto : BaseDto
    {
        public Guid Id { get; set; }
        public Guid BillingAccountItemIdDestination { get; set; }

        public List<BillingAccountItemTransferChildDto> BillingAccountItemTransferList { get; set; }
    }
}
