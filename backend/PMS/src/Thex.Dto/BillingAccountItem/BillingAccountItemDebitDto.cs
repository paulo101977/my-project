﻿using System;
using System.Collections.Generic;
using Tnf.Dto;

namespace Thex.Dto
{
    public class BillingAccountItemDebitDto : BillingAccountItemBaseDto
    {
        public BillingAccountItemDebitDto()
        {
            DetailList = new List<DebitDetailIntegration>();
        }

        public Guid Id { get; set; }
        public int PropertyId { get; set; }
        public Guid BillingAccountId { get; set; }
        public Guid? BillingAccountIdLastSource { get; set; }
        public int BillingItemId { get; set; }
        public Guid ProductId { get; set; }
        public int BillingItemCategoryId { get; set; }
        public Guid TenantId { get; set; }
        public DateTime? DateParameter { get; set; }
        public List<DebitDetailIntegration> DetailList { get; set; }
        public bool Daily { get; set; }
    }

    public class DebitDetailIntegration : BillingAccountItemBaseDto
    {
        /// <summary>
        /// Id do produto para lançamentos de POS
        /// BillingItemId para lançamentos de serviço
        /// BillingItemTypeId para lançamentos de crédito
        /// </summary>
        public string ExternalId { get; set; }
        public string Description { get; set; }
        public string GroupDescription { get; set; }
        public string Code { get; set; }
        public decimal Quantity { get; set; }
        public int Type { get; set; }
    }

    public class BillingAccountItemTourismTaxDto : BillingAccountItemDebitDto
    {
        public DateTime BillingAccountItemDate { get; set; }
        public bool? IsTourismTax { get; set; }
    }
}
