﻿using System;
using Tnf.Dto;

namespace Thex.Dto
{
    public class BillingAccountItemPartialDto : BaseDto
    {
        public Guid Id { get; set; }
        public Guid BillingAccountId { get; set; }
        public Guid BillingAccountItemId { get; set; }
        public decimal? Value { get; set; }
        public int? Parts { get; set; }
    }
}
