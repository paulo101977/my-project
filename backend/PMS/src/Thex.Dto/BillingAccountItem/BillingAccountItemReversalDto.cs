﻿using System;
using System.Collections.Generic;
using Tnf.Dto;

namespace Thex.Dto
{
    public class BillingAccountItemReversalParentDto : BaseDto
    {
        public Guid Id { get; set; }
        public List<BillingAccountItemReversalDto> BillingAccountItems { get; set; }
        public Guid? TenantId { get; set; }
    }

    public class BillingAccountItemReversalDto
    {
        public Guid BillingAccountId { get; set; }
        public Guid OriginalBillingAccountItemId { get; set; }
        public int ReasonId { get; set; }
        public string Observation { get; set; }
        public int PropertyId { get; set; }
    }
}
