﻿using System;
using System.Collections.Generic;
using Tnf.Dto;

namespace Thex.Dto
{
    public class BillingAccountItemCreditDto : BillingAccountItemBaseDto
    {
        public Guid Id { get; set; }
        public int PropertyId { get; set; }
        public Guid BillingAccountId { get; set; }
        public int BillingItemId { get; set; }
        public Guid ProductId { get; set; }
        public int BillingItemCategoryId { get; set; }
        public string NSU { get; set; }
        public string CheckNumber { get; set; }
        public int? InstallmentsQuantity { get; set; }
        public DateTime? DateParameter { get; set; }
        public List<CreditDetailIntegration> DetailList { get; set; }
    }

    public class CreditDetailIntegration : BillingAccountItemBaseDto
    {
        /// <summary>
        /// Id do produto para lançamentos de POS
        /// BillingItemId para lançamentos de serviço
        /// BillingItemTypeId para lançamentos de crédito
        /// </summary>
        public string ExternalId { get; set; }
        public string Description { get; set; }
        public string GroupDescription { get; set; }
        public string Code { get; set; }
        public decimal Quantity { get; set; }
        public string BillingItemId { get; set; }
        public int Type { get; set; }
    }
}
