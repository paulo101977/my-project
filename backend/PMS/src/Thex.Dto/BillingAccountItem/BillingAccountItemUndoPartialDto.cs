﻿using System;
using Tnf.Dto;

namespace Thex.Dto
{
    public class BillingAccountItemUndoPartialDto : BaseDto
    {
        public Guid Id { get; set; }
        public int PropertyId { get; set; }
        public Guid[] BillingAccountItemId { get; set; }
    }
}
