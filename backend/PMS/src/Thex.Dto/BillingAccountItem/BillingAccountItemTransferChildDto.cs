﻿using System;
using System.Collections.Generic;
using Tnf.Dto;

namespace Thex.Dto
{
    public class BillingAccountItemTransferChildDto
    {
        public Guid BillingAccountItemId { get; set; }
    }
}
