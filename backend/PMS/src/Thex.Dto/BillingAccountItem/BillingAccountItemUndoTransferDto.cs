﻿using System;
using System.Collections.Generic;
using Tnf.Dto;

namespace Thex.Dto
{
    public class BillingAccountItemUndoTransferSourceDestinationDto : BaseDto
    {
        public Guid Id { get; set; }
        public List<BillingAccountItemTransferChildDto> BillingAccountItemTransferList { get; set; }
    }
}
