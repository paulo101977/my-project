﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Thex.Dto
{
    public class BillingAccountItemIntegrationDetailDto
    {
        public string Code { get; set; }
        public string Quantity { get; set; }
        public string Description { get; set; }
        public string GroupDescription { get; set; }
        public string Amount { get; set; }

        public BillingAccountItemIntegrationDetailDto()
        {
            Code = "";
            Quantity = "";
            Description = "";
            Amount = "";
        }
    }
}
