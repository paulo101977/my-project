﻿using System;
using System.Collections.Generic;
using Tnf.Dto;

namespace Thex.Dto
{
    public class BillingAccountItemCreditNoteParentDto : BaseDto
    {
        public List<BillingAccountItemCreditNoteDto> BillingAccountItems { get; set; }
    }

    public class BillingAccountItemCreditNoteDto
    {
        public Guid BillingAccountId { get; set; }
        public Guid OriginalBillingAccountItemId { get; set; }
    }
}
