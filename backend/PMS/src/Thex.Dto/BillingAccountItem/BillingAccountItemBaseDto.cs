﻿using System;
using System.Collections.Generic;
using Tnf.Dto;

namespace Thex.Dto
{
    public class BillingAccountItemBaseDto : BaseDto
    {
        public decimal Amount { get; set; }

        public Guid? CurrencyId { get; set; }
        public Guid? CurrencyExchangeReferenceId { get; set; }
        public Guid? CurrencyExchangeReferenceSecId { get; set; }

        public void CalculateAmountByExchangeRate(decimal exchangeRateFirst, decimal exchangeRateSec)
        {
            this.Amount = Math.Round(Amount / exchangeRateFirst * exchangeRateSec, 2);
        }
    }
}
