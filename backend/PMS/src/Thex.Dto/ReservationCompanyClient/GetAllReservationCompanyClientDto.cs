﻿namespace Thex.Dto.ReservationCompanyClient
{
    using System;
    using Tnf.Dto;

    public class GetAllReservationCompanyClientDto : RequestAllDto
    {
        public string SearchData { get; set; }
    }
}