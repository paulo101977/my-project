//  <copyright file="ReservationCompanyClientResultDto.cs" company="TOTVS">
//  Copyright (c) TOTVS. All rights reserved.
//  </copyright>
namespace Thex.Dto.ReservationCompanyClient
{
    using System;
    using Tnf.Dto;

    public class ReservationCompanyClientResultDto : BaseDto
    {
        public long Id { get; set; }
        public long ReservationId { get; set; }      
        public long ReservationItemId { get; set; }
        public string PersonId { get; set; }
        public string ReservationCode { get; set; }
        public string ReservationItemCode { get; set; }
        public string CompannyClientId { get; set; }
        public string CompanyClientName { get; set; }
        public Guid BillingAccountId { get; set; }
        public Guid TenantId { get; set; }
    }
}