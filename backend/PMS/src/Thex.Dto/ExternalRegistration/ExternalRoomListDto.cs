﻿using System;
using System.Collections.Generic;
using Tnf.Dto;

namespace Thex.Dto.ExternalRegistration
{
    public class ExternalRoomListDto : BaseDto
    {
        public Guid ReservationUid { get; set; }
        public Guid ReservationItemUid { get; set; }
        public long StatusId { get; set; }
        public string RoomNumber { get; set; }
        public string RoomTypeName { get; set; }
        public string RoomTypeAbbreviation { get; set; }
        public DateTime? CheckinDate { get; set; }
        public DateTime? CheckoutDate { get; set; }
        public DateTime EstimatedArrivalDate { get; set; }
        public DateTime EstimatedDepartureDate { get; set; }
        public int AdultCount { get; set; }
        public int ChildCount { get; set; }
        public bool IsValid { get; set; }
        public string ReservationItemCode { get; set; }
        public string ReservationItemStatus { get; set; }

        public string GuestName { get; set; }
        public string ExternalName { get; set; }
        
        public List<string> SearchableNames { get; set; }
    }
}
