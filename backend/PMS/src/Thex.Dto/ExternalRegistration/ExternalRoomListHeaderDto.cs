﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Thex.Common.Enumerations;
using Tnf.Dto;

namespace Thex.Dto.ExternalRegistration
{
    public class ExternalRoomListHeaderDto : BaseDto
    {
        public int Adultcount { get; set; }
        public int Childcount { get; set; }
        public string ReservationCode { get; set; }
        public DateTime MaxDate { get; set; }
        public DateTime MinDate { get; set; }
        public string CountryCode { get; set; }
        public string StreetNumber { get; set; }
        public string StreetName { get; set; }
        public string CityName { get; set; }
        public string PostalCode { get; set; }
        public string CountryName { get; set; }
        public string CountryTwoLetterIsoCode { get; set; }
        public string State { get; set; }
        public string Neighborhood { get; set; }
        public string AdditionalAddressDetails { get; set; }
        public int? CityId { get; set; }
        public int? StateId { get; set; }
        public int? CountryId { get; set; }

        public decimal? Latitude { get; set; }
        public decimal? Longitude { get; set; }
        public string LanguageIsoCode { get; set; }
        public string Name { get; set; }

        public IListDto<ExternalRoomListDto> ExternalRoomListDto { get; set; }

    }
}
