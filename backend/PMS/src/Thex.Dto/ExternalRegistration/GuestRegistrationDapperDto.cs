﻿using System;
using Tnf.Dto;

namespace Thex.Dto.ExternalRegistration
{
    public class GuestRegistrationDapperDto : BaseDto
    {
        public Guid Id { get; set; }
        public long GuestReservationItemId { get; set; }
        public long ReservationItemId { get; internal set; }
        public long ReservationId { get; internal set; }
        public Guid ReservationItemUid { get; set; }

        public string FirstName { get; internal set; }
        public string LastName { get; internal set; }
        public string FullName { get; internal set; }
        public string SocialName { get; internal set; }
        public string Email { get; internal set; }
        public DateTime? BirthDate { get; internal set; }
        public string Gender { get; internal set; }
        public int? OccupationId { get; internal set; }
        public string PhoneNumber { get; internal set; }
        public string MobilePhoneNumber { get; internal set; }
        public int? NationalityId { get; internal set; }
        public int? PurposeOfTrip { get; internal set; }
        public int? ArrivingBy { get; internal set; }
        public int? ArrivingFrom { get; internal set; }
        public string ArrivingFromText { get; internal set; }
        public int? NextDestination { get; internal set; }
        public string NextDestinationText { get; internal set; }
        public string AdditionalInformation { get; internal set; }
        public string HealthInsurance { get; internal set; }
        public int PropertyId { get; internal set; }
        public string DocumentInformation { get; internal set; }
        public int? DocumentTypeId { get; internal set; }
        public string DocumentTypeName { get; internal set; }
        public int? LocationCategoryId { get; internal set; }
        public decimal? Latitude { get; internal set; }
        public decimal? Longitude { get; internal set; }
        public string StreetName { get; internal set; }
        public string StreetNumber { get; internal set; }
        public string AdditionalAddressDetails { get; internal set; }
        public string Neighborhood { get; internal set; }
        public int? CityId { get; internal set; }
        public int? StateId { get; internal set; }
        public int? CountryId { get; internal set; }
        public string PostalCode { get; internal set; }
        public string CountryCode { get; internal set; }
        public string CityName { get; internal set; }
        public string StateName { get; internal set; }
        public string CountryName { get; internal set; }
        public string CountryTwoLetterIsoCode { get; internal set; }
        public string State { get; internal set; }
        public int ReservationItemStatusId { get; internal set; }
        public int? GuestTypeId { get; internal set; }
        public int IsChild { get; internal set; }
        public int IsMain { get; internal set; }
        public int IsIncognito { get; internal set; }
        public string Nationality { get; internal set; }
        public string ReservationCode { get; internal set; }
        public Guid GuestRegistrationId { get; internal set; }
        public Guid? ResponsibleGuestRegistrationId { get; internal set; }
        public int? GuestId { get; internal set; }

        public string LanguageIsoCode { get; internal set; }

        public ExternalGuestRegistrationDto ToExternalGuestRegistrationDto(Guid id, string externalRegistrationEmail)
        {
            return new ExternalGuestRegistrationDto
            {
                Id = id,
                AdditionalAddressDetails = AdditionalAddressDetails,
                AdditionalInformation = AdditionalInformation,
                ArrivingBy = ArrivingBy,
                ArrivingFrom = ArrivingFrom,
                ArrivingFromText = ArrivingFromText,
                BirthDate = BirthDate,
                CityId = CityId,
                CountryCode = CountryCode,
                CountryId = CountryId,
                DocumentInformation = DocumentInformation,
                DocumentTypeId = DocumentTypeId,
                Email = Email,
                ExternalRegistrationEmail = externalRegistrationEmail,
                FirstName = FirstName,
                FullName = FullName,
                Gender = Gender,
                GuestReservationItemId = GuestReservationItemId,
                HealthInsurance = HealthInsurance,
                IsEditable = false,
                LastName = LastName,
                Latitude = Latitude,
                LocationCategoryId = LocationCategoryId,
                Longitude = Longitude,
                MobilePhoneNumber = MobilePhoneNumber,
                NationalityId = NationalityId,
                Neighborhood = Neighborhood,
                NextDestination = NextDestination,
                NextDestinationText = NextDestinationText,
                OccupationId = OccupationId,
                PhoneNumber = PhoneNumber,
                PostalCode = PostalCode,
                PurposeOfTrip = PurposeOfTrip,
                SocialName = SocialName,
                StateId = StateId,
                StreetName = StreetName,
                StreetNumber = StreetNumber,
                CityName = CityName,
                CountryName = CountryName,
                StateName = StateName,
                CountryTwoLetterIsoCode = CountryCode,
                StateTwoLetterIsoCode = CountryTwoLetterIsoCode,
                LanguageIsoCode = LanguageIsoCode,
                ReservationItemUid = ReservationItemUid
            };
        }
    }
}
