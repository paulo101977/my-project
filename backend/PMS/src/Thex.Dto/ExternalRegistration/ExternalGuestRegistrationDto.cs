﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Thex.Common.Enumerations;
using Tnf.Dto;

namespace Thex.Dto.ExternalRegistration
{
    public class ExternalGuestRegistrationDto : BaseDto
    {
        public Guid Id { get; set; }
        public long GuestReservationItemId { get; set; }
        public Guid ReservationItemUid { get; set; }
        public bool IsChild { get; set; }
        public string ExternalRegistrationEmail { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FullName { get; set; }
        public string SocialName { get; set; }
        public string Email { get; set; }
        public DateTime? BirthDate { get; set; }
        public string Gender { get; set; }
        public int? OccupationId { get; set; }
        public string PhoneNumber { get; set; }
        public string MobilePhoneNumber { get; set; }
        public int? NationalityId { get; set; }
        public int? PurposeOfTrip { get; set; }
        public int? ArrivingBy { get; set; }
        public int? ArrivingFrom { get; set; }
        public string ArrivingFromText { get; set; }
        public int? NextDestination { get; set; }
        public string NextDestinationText { get; set; }
        public string AdditionalInformation { get; set; }
        public string HealthInsurance { get; set; }
        public string DocumentInformation { get; set; }
        public int? DocumentTypeId { get; set; }
        public int? LocationCategoryId { get; set; }
        public decimal? Latitude { get; set; }
        public decimal? Longitude { get; set; }
        public string StreetName { get; set; }
        public string StreetNumber { get; set; }
        public string AdditionalAddressDetails { get; set; }
        public string Neighborhood { get; set; }
        public int? CityId { get; set; }
        public int? StateId { get; set; }
        public int? CountryId { get; set; }
        public string PostalCode { get; set; }
        public string CountryCode { get; set; }

        public string Subdivision { get; set; }
        public string Division { get; set; }
        public string Country { get; set; }

        public string CityName { get; set; }
        public string StateName { get; set; }
        public string CountryName { get; set; }
        public string LanguageIsoCode { get; set; }

        public string StateTwoLetterIsoCode { get; set; }
        public string CountryTwoLetterIsoCode { get; set; }


        public bool IsEditable { get; set; }


        public string GetFullName()
        {
            return $"{FirstName} {LastName}";
        }

        public void SetFirstAndLastNameBasedOnFullName()
        {
            if (!string.IsNullOrEmpty(FullName))
            {
                var separator = " ";

                var FullNameList = FullName.Split(separator).ToList();

                FirstName = FullNameList.First();

                if (FullNameList.Count() > 1)
                {
                    FullNameList.RemoveAt(0);

                    LastName = FullNameList.JoinAsString(separator);
                }
            }
        }

        public LocationDto GetLocationDto()
        {
            LocationCategoryId = (int)LocationCategoryEnum.Residential;

            return new LocationDto
               {
                   CountryCode = CountryCode,
                   LocationCategoryId = LocationCategoryId.Value,
                   AdditionalAddressDetails = AdditionalAddressDetails,
                   BrowserLanguage = CultureInfo.CurrentCulture.Name,
                   CityId = CityId,
                   StreetName = StreetName,
                   StreetNumber = StreetNumber,
                   Neighborhood = Neighborhood,
                   PostalCode = PostalCode,
                   Subdivision = Subdivision,
                   Division = Division,
                   Country = Country
               };
        }
    }
}
