﻿using System;
using System.Collections.Generic;
using System.Text;
using Tnf.Dto;

namespace Thex.Dto.BillingItemCategory
{
    public class GetAllCategoriesForItemDto : BaseDto
    {
        public int Id { get; set; }
        public string BillingItemCategoryName { get; set; }
    }
}
