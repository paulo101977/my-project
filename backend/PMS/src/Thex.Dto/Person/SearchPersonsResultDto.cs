﻿// //  <copyright file="SearchPersonsResultDto.cs" company="TOTVS">
// //  Copyright (c) TOTVS. All rights reserved.
// //  </copyright>
namespace Thex.Dto.Person
{
    using Thex.Dto.Person;
    using System;
    using System.Collections.Generic;

    using Tnf.Dto;
    public class SearchPersonsResultDto : BaseDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Document { get; set; }
        public int DocumentTypeId { get; set; }
        public bool IsDocumentMain { get; set; }
        public string DocumentType { get; set; }
        public long? GuestId { get; set; }
        public List<SearchPersonsInformationResultDto> PersonInformation { get; set; }
        public List<SearchPersonsContactInformationResultDto> ContactInformation { get; set; }
        public List<LocationDto> LocationList { get; set; }

        public Guid? CompanyClientId { get; set; }
    }


}