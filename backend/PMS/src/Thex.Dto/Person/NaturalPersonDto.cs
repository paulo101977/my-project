﻿//  <copyright file="NaturalPerson.cs" company="TOTVS">
//  Copyright (c) TOTVS. All rights reserved.
//  </copyright>

namespace Thex.Dto.Person
{
    using Thex.Common.Enumerations;
    using Thex.Dto;

    public class NaturalPersonDto : PersonDto
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="NaturalPersonDto"/> class.
        /// </summary>
        public NaturalPersonDto() : base()
        {
            this._personType = PersonTypeEnum.Natural;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="NaturalPersonDto"/> class.
        /// </summary>
        /// <param name="firstName">First name</param>
        /// <param name="lastName">Second name</param>
        public NaturalPersonDto(string firstName, string lastName) : this()
        {
            this.FirstName = firstName;

            this.LastName = lastName;
        }
    }
}