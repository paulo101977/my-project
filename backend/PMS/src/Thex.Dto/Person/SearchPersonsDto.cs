﻿// //  <copyright file="GetAllRoomTypesDto.cs" company="TOTVS">
// //  Copyright (c) TOTVS. All rights reserved.
// //  </copyright>

namespace Thex.Dto.Person
{
    using Thex.Dto.Common;

    public class SearchPersonsDto : PagingDto
    {
        public string SearchData { get; set; }

        public int DocumentTypeId { get; set; }

        public bool ByEmail { get; set; }

        public string PersonType { get; set; }
    }
}