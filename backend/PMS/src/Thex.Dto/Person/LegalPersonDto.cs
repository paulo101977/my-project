﻿//  <copyright file="LegalPerson.cs" company="TOTVS">
//  Copyright (c) TOTVS. All rights reserved.
//  </copyright>

using Thex.Common.Enumerations;
using Thex.Dto;

namespace Thex.Dto.Person
{
    public class LegalPersonDto : PersonDto
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LegalPersonDto"/> class.
        /// </summary>
        public LegalPersonDto() : base()
        {
            _personType = PersonTypeEnum.Legal;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="LegalPersonDto"/> class.
        /// </summary>
        /// <param name="tradeName">Trade name</param>
        /// <param name="shortName">Short name</param>
        public LegalPersonDto(string tradeName, string shortName) : this()
        {
            FirstName = tradeName;
            LastName = shortName;
        }
    }
}