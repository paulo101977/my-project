﻿namespace Thex.Dto.Person
{
    public class SearchPersonsContactInformationResultDto
    {
        public int Type { get; set; }
        public string TypeValue { get; set; }
        public string Value { get; set; }
    }
}
