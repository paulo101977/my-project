﻿using Thex.Common.Enumerations;
using Thex.Dto.Person;
using Thex.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace Thex.Dto.Person
{
    public class ContactPersonDto : PersonDto
    {
        public ContactPersonDto() : base()
        {
            this._personType = PersonTypeEnum.Contact;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="NaturalPersonDto"/> class.
        /// </summary>
        /// <param name="firstName">First name</param>
        /// <param name="lastName">Second name</param>
        public ContactPersonDto(string firstName, string lastName) : this()
        {
            this.FirstName = firstName;
            this.LastName = lastName;
        }
    }
}
