﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Thex.Dto.Person
{
    public class SearchPersonsInformationResultDto
    {
        public int Type { get; set; }
        public string TypeValue { get; set; }
        public string Value { get; set; }
    }
}
