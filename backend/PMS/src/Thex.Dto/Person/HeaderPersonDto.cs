﻿using System;
using Thex.Common.Enumerations;

namespace Thex.Dto.Person
{
    public class HeaderPersonDto : PersonDto
    {
        public static HeaderPersonDto NullInstance = null;

        public HeaderPersonDto()
        {
            _personType = PersonTypeEnum.Header;
        }

        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public Guid? BillingAccountId { get; set; }
        public Guid? BillingInvoiceId { get; set; }
    }
}
