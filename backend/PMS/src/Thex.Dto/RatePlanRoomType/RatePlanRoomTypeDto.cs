﻿namespace Thex.Dto
{
    public partial class RatePlanRoomTypeDto
    {
       public string RoomTypeName { get; set; }
       public string Abbreviation { get; set; }
    }
}
