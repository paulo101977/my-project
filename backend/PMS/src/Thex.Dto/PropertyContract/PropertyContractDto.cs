﻿using System;
using Tnf.Dto;

namespace Thex.Dto
{
    public class PropertyContractDto : BaseDto
    {
        public Guid Id { get; set; }
        public int PropertyId { get; set; }
        public int MaxUh { get; set; }
    }
}
