﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Thex.Dto.Integration
{
    public class InvoiceIntegrationNotification
    {
        public string Code { get; set; }
        public string Message { get; set; }
        public string DetailedMessage { get; set; }

        public List<InvoiceIntegrationNotification> Details { get; set; }
    }
}
