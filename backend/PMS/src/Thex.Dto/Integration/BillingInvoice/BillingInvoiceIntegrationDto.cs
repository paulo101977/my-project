﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Thex.Dto.Integration
{
    public class BillingInvoiceBaseIntegrationDto
    {
        public string Service { get; set; }
    }

    public class BillingInvoiceSuccessIntegrationDto : BillingInvoiceBaseIntegrationDto
    {
        public BillingInvoiceSuccessIntegrationDetailDto Message { get; set; }
    }

    public class BillingInvoiceErrorIntegrationDto : BillingInvoiceBaseIntegrationDto
    {
        public BillingInvoiceErrorIntegrationDetailDto Message { get; set; }
    }

    public class BillingInvoiceSuccessIntegrationDetailDto
    {
        public string InvoiceId { get; set; }
        public string LinkPdf { get; set; }
        public string LinkXml { get; set; }
        public string Number { get; set; }
        public int RpsNumber { get; set; }
        public string RpsSerialNumber { get; set; }
        public Guid TenantId { get; set; }
    }

    public class BillingInvoiceErrorIntegrationDetailDto
    {
        public string Code { get; set; }
        public string InvoiceId { get; set; }
        public string Message { get; set; }
        public string Detail { get; set; }
        public Guid TenantId { get; set; }
    }
}
