﻿using Tnf.Dto;

namespace Thex.Dto.Integration
{
    public class BillingInvoiceIntegrationResponseDto : BaseDto
    {
        public static BillingInvoiceIntegrationResponseDto NullInstance = null;
        public int Id { get; set; }
        public bool Success { get; set; }
        public string EmailAlert { get; set; }
        public int Type { get; set; }
    }
}
