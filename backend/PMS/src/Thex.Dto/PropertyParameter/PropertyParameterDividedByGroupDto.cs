﻿using System;
using System.Collections.Generic;
using System.Text;
using Tnf.Dto;

namespace Thex.Dto.PropertyParameter
{
    public class PropertyParameterDividedByGroupDto : BaseDto
    {
        public Guid Id { get; set; }

        public PropertyParameterDividedByGroupDto()
        {
            propertyParameters = new List<AllParameters>();
        }
        public List<AllParameters> propertyParameters { get; set; }
    }

    public class AllParameters
    {
        public int FeatureGroupId { get; set; }
        public string FeatureGroupName { get; set; }
        public List<PropertyParameterDto> Parameters { get; set; }
    }



}
