﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Thex.Dto.PropertyParameter
{
    public class GeneralParametersDto : PropertyParameterDto
    {
        public List<CurrencyDto> Currencies { get; set; }
    }
}
