﻿using System;
using System.Collections.Generic;
using Tnf.Dto;

namespace Thex.Dto
{
    public partial class PropertyParameterDto
    {

        public string ParameterTypeName { get; set; }
        public string ParameterDescription { get; set; }
        public int FeatureGroupId { get; set; }
        public bool? CanInactive { get; set; }
     
    }
}
