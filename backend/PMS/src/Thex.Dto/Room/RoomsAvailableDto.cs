﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Thex.Dto
{
    public class RoomsAvailableDto
    {
        public int Rooms { get; set; }
        public int TotalRooms { get; set; }
    }
}
