﻿namespace Thex.Dto
{
    public partial class RoomDto
    {
        public string HousekeepingStatusName { get; set; }
        public string RoomTypeAbbreviation { get; set; }
    }
}
