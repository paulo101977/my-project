﻿// //  <copyright file="GetAllRoomTypesDto.cs" company="TOTVS">
// //  Copyright (c) TOTVS. All rights reserved.
// //  </copyright>

namespace Thex.Dto.Rooms
{
    using Tnf.Dto;

    public class GetAllRoomsDto : RequestAllDto
    {
        /// <summary>
        /// Gets or sets nameOrAbbreviationOrOrder
        /// </summary>
        public string SearchData { get; set; }
    }
}