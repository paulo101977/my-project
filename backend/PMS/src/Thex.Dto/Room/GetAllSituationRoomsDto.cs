﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Thex.Dto.Room
{
    public class GetAllSituationRoomsDto
    {
        public int? UhSituationId { get; set; }

        public int? HousekeepingStatusId { get; set; }

        public int? TypeUHId { get; set; }

        public string ClientName { get; set; }

        public string GroupName { get; set; }

        public bool OnlyVipGuests { get; set; }
    }
}
