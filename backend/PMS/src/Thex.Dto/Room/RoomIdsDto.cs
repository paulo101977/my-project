﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Thex.Dto.Room
{
    public class RoomIdsDto
    {
        public List<int> RoomIdsList { get; set; }
    }
}
