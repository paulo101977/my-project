﻿namespace Thex.Dto.Rooms
{
    using System;
    using Tnf.Dto;

    public class GetAllRoomsByPropertyAndPeriodDto : RequestAllDto
    {
        /// <summary>
        /// Gets or sets InitialDate
        /// </summary>
        
        public DateTime InitialDate { get; set; }

        /// <summary>
        /// Gets or sets FinalDate
        /// </summary>
        
        public DateTime FinalDate { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this Room is active or not
        /// </summary>
        public bool IsActive { get; set; }

        public string SearchData { get; set; }

    }
}