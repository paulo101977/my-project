﻿using System;
using System.Collections.Generic;
using System.Text;
using Tnf.Dto;

namespace Thex.Dto.Room
{
    public class BudgetCurrentOrFutureDto : BaseDto
    {
        public long Id { get; set; }
        public decimal Budget { get; set; }
    }
}
