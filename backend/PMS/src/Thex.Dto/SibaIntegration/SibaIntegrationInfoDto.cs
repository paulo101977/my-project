﻿using System;

namespace Thex.Dto.SibaIntegration
{
    public class SibaIntegrationInfoDto
    {
        public int IntegrationFileNumber { get; set; }
        public DateTime IntegrationDate { get; set; }
        public string AccessKey { get; set; }
    }
}
