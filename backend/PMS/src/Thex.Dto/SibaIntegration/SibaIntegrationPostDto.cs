﻿using System.Collections.Generic;

namespace Thex.Dto.SibaIntegration
{
    public class SibaIntegrationPostDto
    {
        public GetPropertySiba Property { get; set; }
        public List<GetAllForeingGuest> GuestList { get; set; }
        public SibaIntegrationInfoDto SibaIntegrationInfo { get; set; }
    }
}
