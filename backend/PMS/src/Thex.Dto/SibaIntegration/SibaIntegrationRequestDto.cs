﻿using System;

namespace Thex.Dto
{
    public class SibaIntegrationRequestDto
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }
}
