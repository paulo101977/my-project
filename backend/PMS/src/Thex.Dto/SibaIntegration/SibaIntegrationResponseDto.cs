﻿namespace Thex.Dto
{
    public class SibaIntegrationResponseDto
    {
        public int Code { get; set; }
        public string Description { get; set; }
    }
}
