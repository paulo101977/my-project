﻿using System;

namespace Thex.Dto
{
    public class SibaIntegrationDto
    {
        public Guid Id { get; private set; }
        public int PropertyId { get; private set; }
        public int IntegrationFileNumber { get; private set; }
    }
}
