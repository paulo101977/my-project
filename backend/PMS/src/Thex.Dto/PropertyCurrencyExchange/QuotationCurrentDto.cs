﻿using System;
using System.Collections.Generic;
using Tnf.Dto;

namespace Thex.Dto.PropertyCurrencyExchange
{
   public class QuotationCurrentDto : BaseDto
    {
        public Guid Id { get; set; }
        public Guid CurrencyId { get; set; }
        public string CurrencyName { get; set; }
        public DateTime CurrentDate { get; set; }
        public decimal? ExchangeRate { get; set; }
        public decimal? PropertyExchangeRate { get; set; }
        public int Origin { get; set; }
        public int PropertyId { get; set; }
        public string CurrencySymbol { get; set; }
        public DateTime CreationTime { get; set; }
        public Guid CreatorUserId { get; set; }
    }

    public class ExchangeRateDto : BaseDto
    {
        public List<QuotationCurrentDto> ExchangeRateList { get; set; }
    }
}
