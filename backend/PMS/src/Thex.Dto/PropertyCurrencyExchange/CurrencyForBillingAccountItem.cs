﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Thex.Dto.PropertyCurrencyExchange
{
    public class CurrencyForBillingAccountItemDto
    {
        public Guid CurrencyId { get; set; }
        public Guid? CurrencyExchangeReferenceId { get; set; }
    }
}
