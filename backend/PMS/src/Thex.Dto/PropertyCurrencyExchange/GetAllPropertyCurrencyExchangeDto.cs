﻿using System;

namespace Thex.Dto
{
    public partial class GetAllPropertyCurrencyExchangeDto
    {
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public Guid CurrencyId { get; set; }
    }
}
