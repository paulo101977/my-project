﻿using System;

namespace Thex.Dto.PropertyCurrencyExchange
{
    public class InsertQuotationByPeriodDto
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public Guid CurrencyId { get; set; }
        public decimal PropertyExchangeRate { get; set; }
        public int PropertyId { get; set; }
        public Guid? PropertyCurrencyExchangeId { get; set; }
    }
}
