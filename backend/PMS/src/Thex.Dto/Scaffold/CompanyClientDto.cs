﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a Crudzilla tool.
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using Tnf.Dto;

namespace Thex.Dto
{
	using System.Collections.Generic;
	using System;

    public partial class CompanyClientDto : BaseDto
	{
        public string Id { get; set; }

        public static CompanyClientDto NullInstance = null;


		public CompanyClientDto()
		{
			this.DocumentList = new List<DocumentDto>();
			this.LocationList = new List<LocationDto>();
			CompanyClientContactPersonList = new HashSet<CompanyClientContactPersonDto>();
            CompanyClientChannelList = new List<CompanyClientChannelDto>();
		}

		public int CompanyId { get; set; }
		public Guid PersonId { get; set; }
		//public string PersonId { get; set; }
		public int PropertyId { get; set; }
		private char _personType;
		public char PersonType
		{
			get
			{
				return this._personType.ToString().ToUpper()[0];
			}
			set
			{
				this._personType = value;
			}
		}
		public string ShortName { get; set; }
		public string TradeName { get; set; }
		public string Email { get; set; }
		public string HomePage { get; set; }
		public string PhoneNumber { get; set; }
		public string CellPhoneNumber { get; set; }
		public bool IsActive { get; set; }
        public bool IsAcquirer { get; set; }
        public string ExternalCode { get; set; }

        public DateTime? DateOfBirth { get; set; }
        public int CountrySubdivisionId { get; set; }
        public Guid? PropertyCompanyClientCategoryId { get; set; }
		public ICollection<DocumentDto> DocumentList { get; set; }
		public ICollection<LocationDto> LocationList { get; set; }
        public ICollection<CompanyClientChannelDto> CompanyClientChannelList { get; set; }
        public ICollection<CustomerStationDto> CustomerStationList { get; set; }
		public virtual ICollection<CompanyClientContactPersonDto> CompanyClientContactPersonList { get; set; }

	}
}
