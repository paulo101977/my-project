﻿
using System;
using Tnf.Dto;

namespace Thex.Dto
{
    public partial class LevelDto : BaseDto
    {
        public static LevelDto NullInstance = null;

        public Guid? Id { get; set; }
        public int LevelCode { get; set; }
        public string Description { get; set; }
        public int Order { get; set; }
        public bool IsActive { get; set; }
        public int PropertyId { get; set; }


        public enum Method
        {
            Insert,
            Update,
            Delete,
            ToogleAndSave,
            Get
        }

    }
}
