﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a Crudzilla tool.
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using Tnf.Dto;

namespace Thex.Dto
{
    public partial class BusinessPartnerDto : BaseDto
    {
        public Guid Id { get; set; }
        public static BusinessPartnerDto NullInstance = null;

        public BusinessPartnerDto()
        {
            PropertyBusinessPartnerList = new HashSet<PropertyBusinessPartnerDto>();
        }

        public string BusinessPartnerName { get; set; }
        public string Description { get; set; }
        public string Url { get; set; }
        public int CountrySubdivisionId { get; set; }
        public int BusinessPartnerTypeId { get; set; }

        public virtual BusinessPartnerTypeDto BusinessPartnerType { get; set; }
        public virtual CountrySubdivisionDto CountrySubdivision { get; set; }
        public virtual ICollection<PropertyBusinessPartnerDto> PropertyBusinessPartnerList { get; set; }
    }
}
