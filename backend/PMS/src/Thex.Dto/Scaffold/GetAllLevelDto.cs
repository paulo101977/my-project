﻿ 
using Tnf.Dto;

namespace Thex.Dto
{
    public partial class GetAllLevelDto : RequestAllDto
    {
        public string SearchData { get; set; }
        public bool All { get; set; }
    }
}
