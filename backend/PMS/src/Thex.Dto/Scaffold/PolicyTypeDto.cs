﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a Crudzilla tool.
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using Tnf.Dto;

namespace Thex.Dto
{
    public partial class PolicyTypeDto : BaseDto
    {
        public int Id { get; set; }
        public static PolicyTypeDto NullInstance = null;

        //public PolicyTypeDto()
        //{
        //    PropertyPolicyList = new HashSet<PropertyPolicyDto>();
        //}

        public string PolicyTypeName { get; set; }

      //  public virtual ICollection<PropertyPolicyDto> PropertyPolicyList { get; set; }
    }
}
