﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a Crudzilla tool.
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using Tnf.Dto;

namespace Thex.Dto
{
    public partial class PropertyCompanyClientCategoryDto : BaseDto
    {
        public Guid Id { get; set; }
        public static PropertyCompanyClientCategoryDto NullInstance = null;

        //public PropertyCompanyClientCategoryDto()
        //{
        //    CompanyClientList = new HashSet<CompanyClientDto>();
        //}

        public int PropertyId { get; set; }
        public string PropertyCompanyClientCategoryName { get; set; }
        public bool IsActive { get; set; }

        //public virtual PropertyDto Property { get; set; }
        //public virtual ICollection<CompanyClientDto> CompanyClientList { get; set; }
    }
}
