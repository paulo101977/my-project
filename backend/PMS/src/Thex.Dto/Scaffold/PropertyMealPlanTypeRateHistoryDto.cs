﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a Crudzilla tool.
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using Tnf.Dto;

namespace Thex.Dto
{
    public partial class PropertyMealPlanTypeRateHistoryDto : BaseDto
    {
        public static PropertyMealPlanTypeRateHistoryDto NullInstance = null;

        public Guid Id { get; set; }
        public int PropertyId { get; set; }
        public int MealPlanTypeId { get; set; }
        public bool? MealPlanTypeDefault { get; set; }
        public decimal? AdultMealPlanAmount { get; set; }
        public decimal? Child1MealPlanAmount { get; set; }
        public decimal? Child2MealPlanAmount { get; set; }
        public decimal? Child3MealPlanAmount { get; set; }
        public Guid TenantId { get; set; }
        public Guid? PropertyMealPlanTypeRateHeaderId { get; set; }
        public bool Sunday { get; set; }
        public bool Monday { get; set; }
        public bool Tuesday { get; set; }
        public bool Wednesday { get; set; }
        public bool Thursday { get; set; }
        public bool Friday { get; set; }
        public bool Saturday { get; set; }
        public List<int> DaysOfWeekDto { get; set; }

        public virtual MealPlanTypeDto MealPlanType { get; set; }
        public virtual PropertyMealPlanTypeRateHeaderDto PropertyMealPlanTypeRateHeader { get; set; }
    }
}
