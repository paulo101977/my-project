﻿using System;
using Tnf.Dto;

namespace Thex.Dto
{
    public class CompanyClientChannelDto : BaseDto
    {
        public static CompanyClientChannelDto NullInstance = null;

        public Guid Id { get; set; }
        public Guid ChannelId { get; set; }
        public Guid CompanyClientId { get; set; }
        public bool IsActive { get; set; }
        public string CompanyId { get; set; }
        public string ChannelCodeDescription { get; set; }
        public int ChannelCodeId { get; set; }
        public string ChannelCodeName { get; set; }
        public string ChannelDescription { get; set; }
        public int BusinessSourceId { get; set; }
        public string BusinessSourceDescription { get; set; }
    }
}
