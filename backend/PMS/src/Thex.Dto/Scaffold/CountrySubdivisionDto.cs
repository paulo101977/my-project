﻿using System;
using Tnf.Dto;

namespace Thex.Dto
{
    public partial class CountrySubdivisionDto : BaseDto
    {
        public int Id { get; set; }

        public static CountrySubdivisionDto NullInstance = null;

        public string TwoLetterIsoCode { get; set; }
        public string ThreeLetterIsoCode { get; set; }
        public string CountryTwoLetterIsoCode { get; set; }
        public string CountryThreeLetterIsoCode { get; set; }
        public string ExternalCode { get; set; }
        public int? ParentSubdivisionId { get; set; }
        public int SubdivisionTypeId { get; set; }
        public string OfficialNatural { get; set; }
        public string OfficialLegal { get; set; }
    }
}
