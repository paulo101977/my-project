﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a Crudzilla tool.
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;

namespace Thex.Dto
{
    public partial class PropertyRateStrategyRoomTypeDto
    {
        public Guid Id { get; set; }
        public static PropertyRateStrategyRoomTypeDto NullInstance = null;

        public Guid PropertyRateStrategyId { get; set; }
        public int RoomTypeId { get; set; }
        public string RoomTypeName { get; set; }
        public decimal MinOccupationPercentual { get; set; }
        public decimal MaxOccupationPercentual { get; set; }
        public bool IsDecreased { get; set; }
        public decimal? PercentualAmount { get; set; }
        public decimal? IncrementAmount { get; set; }      
    }
}
