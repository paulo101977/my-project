﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a Crudzilla tool.
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using Tnf.Dto;

namespace Thex.Dto
{
    public partial class BillingAccountItemTypeDto : BaseDto
    {
        public int Id { get; set; }

        public static BillingAccountItemTypeDto NullInstance = null;

        public BillingAccountItemTypeDto()
        {
            BillingAccountItemList = new HashSet<BillingAccountItemDto>();
        }

        public string BillingAccountItemTypeName { get; set; }

        public virtual ICollection<BillingAccountItemDto> BillingAccountItemList { get; set; }
    }
}
