﻿using System;

namespace Thex.Dto
{
    public partial class GetAllPropertyRateStrategyWithRatePlanDto
    {
        public Guid Id { get; set; }
        public bool IsActive { get; set; }
        public string Name { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }        
    }
}
