﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a Crudzilla tool.
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using Tnf.Dto;

namespace Thex.Dto
{
    public partial class BedTypeDto : BaseDto
    {
        public int Id { get; set; }

        public static BedTypeDto NullInstance = null;

        public BedTypeDto()
        {
            RoomTypeBedTypeList = new HashSet<RoomTypeBedTypeDto>();
        }

        public string Name { get; set; }
        public byte Capactity { get; set; }

        public virtual ICollection<RoomTypeBedTypeDto> RoomTypeBedTypeList { get; set; }
    }
}
