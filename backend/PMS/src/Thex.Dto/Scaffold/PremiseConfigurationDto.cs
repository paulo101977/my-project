﻿using System;
using System.Collections.Generic;
using System.Text;
using Tnf.Dto;

namespace Thex.Dto
{
    public partial class PremiseConfigurationDto : BaseDto
    {
        public Guid premiseId { get; set; }
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
        public Decimal PremiseValue { get; set; }

        public enum EntityError
        {
           PremisseValueInvalid
        }
    }

    public class PremiseValueDto
    {
        public Decimal Pax1 { get; set; }
        public Decimal Pax2 { get; set; }
        public Decimal Pax3 { get; set; }
        public Decimal Pax4 { get; set; }
        public Decimal Pax5 { get; set; }        
        public Decimal PaxAdditional { get; set; }
        public Decimal Children1 { get; set; }
        public Decimal Children2 { get; set; }
        public Decimal Children3 { get; set; }
    }

    public enum Pax
    {
        Pax1 = 1,
        Pax2 = 2,
        Pax3 = 3,
        Pax4 = 4,
        Pax5 = 5     
    }
}
