﻿using System;
using System.Collections.Generic;
using System.Text;
using Tnf.Dto;

namespace Thex.Dto
{
    public partial class GuestRegistrationDocumentDto : BaseDto
    {
        public static GuestRegistrationDocumentDto NullInstance = null;

        public Guid Id { get; set; }
        public int DocumentTypeId { get; set; }
        public Guid GuestRegistrationId { get; set; }
        public string DocumentInformation { get; set; }
        public string DocumentTypeName { get; set; }
    }
}
