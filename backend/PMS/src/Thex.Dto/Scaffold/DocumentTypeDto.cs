﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a Crudzilla tool.
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using Tnf.Dto;

namespace Thex.Dto
{
    public partial class DocumentTypeDto : BaseDto
    {
        public int Id { get; set; }
        public static DocumentTypeDto NullInstance = null;

        public DocumentTypeDto()
        {
        }

        public DocumentTypeDto(int id)
        {
            Id = id;
        }

        public string PersonType { get; set; }
        public int? CountrySubdivisionId { get; set; }
        public string Name { get; set; }
        public string StringFormatMask { get; set; }
        public string RegexValidationExpression { get; set; }
        public bool IsMandatory { get; set; }
    }
}
