﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a Crudzilla tool.
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using Tnf.Dto;

namespace Thex.Dto
{
    public partial class SubscriptionDto : BaseDto
    {
        public int Id { get; set; }
        public static SubscriptionDto NullInstance = null;

        public int AccountId { get; set; }
        public int RoomCountLimit { get; set; }
        public bool IsActive { get; set; }
    }
}
