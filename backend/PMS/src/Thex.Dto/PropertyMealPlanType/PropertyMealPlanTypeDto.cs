﻿namespace Thex.Dto
{
    public partial class PropertyMealPlanTypeDto
    {
        public string MealPlanTypeName { get; set; }
        public bool? IsFirstView { get; set; }
    }
}
