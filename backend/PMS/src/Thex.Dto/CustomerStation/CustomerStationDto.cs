﻿using System;

namespace Thex.Dto
{
    public class CustomerStationDto
    {
        public Guid Id { get; set; }
        public string CustomerStationName { get; set; }
        public string SocialReason { get; set; }
        public Guid? CustomerStationCategoryId { get; set; }
        public string CustomerStationCategoryName { get; set; }
        public bool IsActive { get; set; }
        public Guid CompanyClientId { get; set; }
        public string CompanyClientName { get; set; }
        public string HomePage { get; set; }
    }
}
