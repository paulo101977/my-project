﻿using System.Collections.Generic;

namespace Thex.Dto
{
    public class GuestPolicyList
    {
        public string CountryCode { get; set; }
        public List<string> DescriptionList { get; set; }
    }
}
