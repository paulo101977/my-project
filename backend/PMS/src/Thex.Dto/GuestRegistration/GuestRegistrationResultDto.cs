﻿namespace Thex.Dto.GuestRegistration
{
    using Thex.Common.Extensions;
    using Thex.Dto;
    using System;

    using Tnf.Dto;
    using System.Collections.Generic;

    public class GuestRegistrationResultDto : BaseDto
    {
        public Guid Id { get; set; }
        public string Document { get; set; }
        public int DocumentTypeId { get; set; }
        public long? GuestId { get; set; }
        public string FirstName { get; set; }
        public string SocialName { get; set; }
        public string LastName { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
        public DateTime? BirthDate { get; set; }
        public string PersonId { get; set; }
        public int Age
        {
            get
            {
                return BirthDate.GetAge();
            }
        }
        public string Gender { get; set; }
        public int? OccupationId { get; set; }
        public int? GuestTypeId { get; set; }
        public string GuestTypeName { get; set; }
        public string PhoneNumber { get; set; }
        public string MobilePhoneNumber { get; set; }
        public int? Nationality { get; set; }
        public int? PurposeOfTrip { get; set; }
        public int? ArrivingBy { get; set; }
        public int? ArrivingFrom { get; set; }
        public string ArrivingFromText { get; set; }
        public int? NextDestination { get; set; }
        public string NextDestinationText { get; set; }
        public string AdditionalInformation { get; set; }
        public string HealthInsurance { get; set; }
        public Guid? ResponsibleGuestRegistrationId { get; set; }
        public bool IsLegallyIncompetent { get; set; }

        public virtual LocationDto Location { get; set; }
        public virtual PersonDto Person { get; set; }

        public long GuestReservationItemId { get; set; }

        public int PropertyId { get; set; }
        public Guid TenantId { get; set; }
        public DateTime? CheckinDate { get; set; }

        public bool IsChild { get; set; }
        public short? ChildAge { get; set; }
        public bool IsMain { get; set; }

        public GuestPolicyPrint GuestPolicyPrint { get; set; }

        public ICollection<GuestRegistrationDocumentDto> GuestRegistrationDocumentList { get; set; }

        public GuestRegistrationResultDto()
        {
            this.GuestRegistrationDocumentList = new HashSet<GuestRegistrationDocumentDto>();
        }
    }

}