﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Thex.Dto
{
    public partial class GuestRegistrationDto
    {
        public bool IsChild { get; set; }
        public void SetFirstAndLastNameBasedOnFullName()
        {
            if (!string.IsNullOrEmpty(FullName))
            {
                var separator = " ";

                var FullNameList = FullName.Split(separator).ToList();

                FirstName = FullNameList.First();

                if (FullNameList.Count() > 1)
                {
                    FullNameList.RemoveAt(0);

                    LastName = FullNameList.JoinAsString(separator);
                }
            }
        }
    }
}
