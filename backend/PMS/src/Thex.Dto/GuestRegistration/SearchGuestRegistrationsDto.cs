﻿// //  <copyright file="GetAllRoomTypesDto.cs" company="TOTVS">
// //  Copyright (c) TOTVS. All rights reserved.
// //  </copyright>

namespace Thex.Dto.GuestRegistration
{
    using System;
    using Thex.Dto.Common;

    public class SearchGuestRegistrationsDto : PagingDto
    {
        public string SearchData { get; set; }
        public int DocumentTypeId { get; set; }
        public int PropertyId { get; set; }
        public Guid TenantId { get; set; }
        public bool ByEmail { get; set; }
        public string LanguageIsoCode { get; set; }
    }
}