﻿namespace Thex.Dto
{
    public partial class BillingInvoicePropertyDto
    {
        public string BillingInvoiceModeName { get; set; }
        public string ChildBillingInvoicePropertySeries { get; set; }
        public int ChildBillingInvoiceModelId { get; set; }
        public string ChildLastNumber { get; set; }
        public string ChildIntegrationId { get; set; }
    }
}
