﻿using System;
using System.Collections.Generic;
using System.Text;
using Tnf.Dto;

namespace Thex.Dto.BillingAccount
{
   public class SearchBillingAccountDto : RequestAllDto
    {
        public string ReservationCode { get; set; }
        public string AccountTypeName { get; set; }
        public string BillingAccountName { get; set; }
        public string RoomTypeName { get; set; }
        public string FreeTextSearch { get; set; }
        public int? AccountStatus { get; set; } 

        public bool? OpenedAccounts { get; set; }
        public bool? ClosedAccounts { get; set; }
        public bool? PendingAccounts { get; set; }

        public Guid? BaseAccountIgnored { get; set; }

        public List<Guid> BillingAccountIgnoredList { get; set; }
    }
}
