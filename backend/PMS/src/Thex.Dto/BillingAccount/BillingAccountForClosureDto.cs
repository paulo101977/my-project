﻿using System;
using System.Collections.Generic;
using Tnf.Dto;

namespace Thex.Dto
{
    public class BillingAccountTotalForClosureDto : BaseDto
    {
        public Guid Id { get; set; }
        public decimal Total { get; set; }
        public List<BillingAccountForClosureDto> BillingAccountForClosureList { get; set; }
        public List<OwnerForClosureDto> OwnerList { get; set; }
    }
    public class OwnerForClosureDto
    {
        public Guid OwnerId { get; set; }
        public string OwnerName { get; set; }
        public bool IsCompany {get;set;}
    }

    public class BillingAccountForClosureDto
    {
        public Guid Id { get; set; }
        public long? ReservationId { get; set; }
        public long? ReservatiomItemId { get; set; }
        public long? GuestReservatiomItemId { get; set; }
        public long? RoomId { get; set; }
        public string RoomNumber { get; set; }
        public int BillingAccountTypeId { get; set; }
        public string BillingAccountItemName { get; set; }
        public bool IsHolder { get; set; }
        public int StatusId { get; set; }
        public decimal Total { get; set; }
        public bool IsCompany { get; set; }
        public string OwnerName { get; set; }
        public Guid OwnerId { get; set; }
    }
}