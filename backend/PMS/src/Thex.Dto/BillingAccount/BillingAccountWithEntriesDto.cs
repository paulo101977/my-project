﻿using System;
using System.Collections.Generic;
using Thex.Dto.Person;
using Tnf.Dto;

namespace Thex.Dto
{
    public class BillingAccountWithCompleteAccountDto : BaseDto
    {
        public Guid Id { get; set; }
        public decimal Total { get; set; }
        public int PeriodOfStay { get; set; }
        public decimal AverageSpendPerDay { get; set; }

        public List<BillingAccountWithAccountsDto> BillingAccountWithAccounts { get; set; }
    }

    public class BillingAccountWithAccountsDto : BaseDto
    {
        public Guid Id { get; set; }
        public Guid MainBillingAccountId { get; set; }
        public bool? IsHolder { get; set; }
        public string HolderName { get; set; }
        public DateTime? ArrivalDate { get; set; }
        public DateTime? DepartureDate { get; set; }
        public string AccountTypeName { get; set; }
        public int AccountTypeId { get; set; }
        public string StatusName { get; set; }
        public int StatusId { get; set; }
        public decimal Total { get; set; }
        public Guid GroupKey { get; set; }
        public int PeriodOfStay { get; set; }
        public decimal AverageSpendPerDay { get; set; }
        public string ReservationItemCode { get; set; }
        public long? ReservationId { get; set; }
        public string ReservationCode { get; set; }
        public string RoomNumber { get; set; }
        public string RoomTypeName { get; set; }
        public string CurrencySymbol { get; set; }

        public Guid? BillingClientId { get; set; }
        public string BillingClientName { get; set; }

        public DateTime? ReservationItemArrivalDate { get; set; }
        public DateTime? ReservationItemDepartureDate { get; set; }
        
        public List<BillingAccountWithEntriesDto> BillingAccounts { get; set; }

        public HeaderPersonDto HeaderPerson { get; set; }
    }

    public class BillingAccountWithEntriesDto
    {
        public Guid BillingAccountId { get; set; }
        public string BillingAccountItemName { get; set; }
        public bool IsBlocked { get; set; }
        public decimal Total { get; set; }
        public int StatusId { get; set; }

        public DateTime StartDate { get; set; }
        public string StatusName { get; set; }

        public List<InvoiceDto> Invoices { get; set; }
        public List<BillingAccountItemsEntriesDto> BillingAccountItems { get; set; }
    }

    public class BillingAccountItemsEntriesDto
    {
        public Guid BillingAccountItemId { get; set; }

        public DateTime BillingAccountItemDate { get; set; }
        public decimal? Discount { get; set; }
        public decimal Total { get; set; }
        public decimal TotalWithoutRate { get; set; }
        public decimal PercentageRate { get; set; }
        public string BillingItemName { get; set; }
        public string BillingItemCategoryName { get; set; }
        public string BillingAccountItemTypeName { get; set; }
        public string PlasticBrandFormatted { get; set; }
        public int BillingAccountItemTypeId { get; set; }
        public int BillingItemId { get; set; }
        public string ReasonName { get; set; }
        public bool IsProduct { get; set; }
        public string BillingInvoiceNumber { get; set; }
        public string BillingInvoiceSeries { get; set; }

        public DateTime? DateParameter { get; set; }

        public Guid? BillingInvoiceId { get; set; }

        public Guid? PartialBillingAccountItemId { get; set; }

        public string CurrencySymbol { get; set; }

        public List<BillingAccountItemsEntriesDto> BillingAccountChildrenItems { get; set; }
    }

    public class InvoiceDto
    {

        public Guid Id { get; set; }
        public string BillingInvoiceNumber { get; set; }
        public string BillingInvoiceSeries { get; set; }
        public DateTime EmissionDate { get; set; }
        public DateTime? CancelDate { get; set; }
        public Guid? EmissionUserId { get; set; }
        public Guid? CancelUserId { get; set; }
        public string ExternalRps { get; set; }
        public string ExternalNumber { get; set; }
        public string ExternalSeries { get; set; }
        public DateTime? ExternalEmissionDate { get; set; }
        public Guid BillingAccountId { get; set; }
        public string AditionalDetails { get; set; }
        public Guid BillingInvoicePropertyId { get; set; }
        public int? BillingInvoiceCancelReasonId { get; set; }
        public string IntegratorLink { get; set; }
        public string IntegratorXml { get; set; }
        public string IntegratorId { get; set; }
        public int BillingInvoiceStatusId { get; set; }
        public string ExternalCodeNumber { get; set; }
        public DateTime? ErrorDate { get; set; }
        public int BillingInvoiceTypeId { get; set; }
        public string BillingInvoiceTypeName { get; set; }
    }
}