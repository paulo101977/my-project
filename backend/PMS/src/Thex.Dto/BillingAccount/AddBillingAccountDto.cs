﻿using System;

namespace Thex.Dto
{
    public class AddBillingAccountDto
    {
        public Guid BillingAccountId { get; set; }
        public string BillingAccountName { get; set; }

        public bool? CreatePrincipalAccount { get; set; }

    }
}
