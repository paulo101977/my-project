﻿using System;
using Tnf.Dto;

namespace Thex.Dto.BillingAccount
{
    public class BillingAccountSidebarDto : BaseDto
    {
        public Guid Id { get; set; }
        public static BillingAccountSidebarDto NullInstance = null;

        public Guid billingAccountId { get; set; }
        public string holderName { get; set; }
        public bool isHolder { get; set; }
        public DateTime? arrivalDate { get; set; }
        public DateTime? departureDate { get; set; }
        public decimal price { get; set; }
        public int accountTypeId { get; set; }
        public bool isClosed { get; set; }
        public long ReservatiomItemId { get; set; }
        public long? ReservationId { get; set; }
        public Guid GroupKey { get; set; }

        public DateTime? EndDate { get; set; }
        public int? GuestReservationItemStatusId { get; set; }
        public int BillingInvoicePendingStatusId { get; set; }
        public int BillingInvoiceErrorStatusId { get; set; }
        public int BillingInvoiceIssuedStatusId { get; set; }
        public string GuestReservationItemStatusName { get; set; }

        public Guid? CurrencyId { get; set; }
        public string CurrencySymbol { get; set; }
    }
}
