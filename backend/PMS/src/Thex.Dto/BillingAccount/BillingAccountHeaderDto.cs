﻿using System;
using System.Collections.Generic;
using System.Text;
using Tnf.Dto;

namespace Thex.Dto.BillingAccount
{
    public partial class BillingAccountHeaderDto : BaseDto
    {
        public Guid Id { get; set; }
        public static BillingAccountHeaderDto NullInstance = null;
        public string reservationItemCode { get; set; }
        public string internalComments { get; set; }
        public string externalComments { get; set; }
        public string PartnerComments { get; set; }
        public int quantityComments { get; set; }
        public decimal priceDayDaily { get; set; }
        public decimal priceTotal { get; set; }
        public string statusName { get; set; }
        public string reservationCode { get; set; }
        public int reservationItemStatusId { get; set; }
        public string roomNumber { get; set; }
        public long? ReservationItemId { get; set; }
        public Guid GroupKey { get; set; }
        public int BillingAccountTypeId { get; set; }
        public bool AllClosed { get; set; }
        public int BillingAccountStatusId { get; set; }
        public Guid? CurrencyId { get; set; }
        public string CurrencySymbol { get; set; }
    }
}
