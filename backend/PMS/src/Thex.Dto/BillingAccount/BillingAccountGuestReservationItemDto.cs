﻿using System;

namespace Thex.Dto
{
    public class BillingAccountGuestReservationItemDto
    {
        public Guid BillingAccountId { get; set; }
        public long ReservationId { get; set; }
        public long ReservationItemId { get; set; }
        public long GuestReservationItemId { get; set; }
        public DateTime CheckinDate { get; set; }
        public DateTime EstimatedDepartureDate { get; set; }
        public bool IsMainAccount { get; set; }
        public int? QuantityOfTourismTaxLaunched { get; set; }
        public bool ReservationUnifyAccounts { get; set; }
    }
}
