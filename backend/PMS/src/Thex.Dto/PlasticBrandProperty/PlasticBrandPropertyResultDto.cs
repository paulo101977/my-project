//  <copyright file="PlasticBrandPropertyResultDto.cs" company="TOTVS">
//  Copyright (c) TOTVS. All rights reserved.
//  </copyright>
namespace Thex.Dto.PlasticBrandProperty
{
    using System;
    using Tnf.Dto;

    public class PlasticBrandPropertyResultDto : BaseDto
    {
        public Guid Id { get; set; }
        public int PlasticBrandId { get; set; }

        public string PlasticBrandName { get; set; }
    }
}