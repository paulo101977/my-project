﻿using System;

namespace Thex.Dto.Report
{
    public class SearchBordereauReportResultDto
    {
        public Guid? TenantId { get; set; }
        public int? ProperyId { get; set; }
        public string AccountName { get; set; }
        public string BillingAccountName { get; set; }
        public string UH { get; set; }
        public string ReservationItemCode { get; set; }
        public string FullName { get; set; }
        public string GuestName { get; set; }
        public string BillingItemName { get; set; }
        public decimal? Amount { get; set; }
        public decimal? PercentualAmount { get; set; }
        public string CheckNumber { get; set; }
        public string NSU { get; set; }
        public DateTime? BillingAccountItemDate { get; set; }
        public string Name { get; set; }
        public int? PaymentTypeId { get; set; }
        public string PaymentTypeName { get; set; }
        public int BillingAccountItemTypeId { get; set; }
        public string AcquirerName { get; set; }
        public string ReasonName { get; set; }
        public string Observation { get; set; }
        public string CategoryName { get; set; }
        public string Rps { get; set; }
        public string CurrencySymbol { get; set; }

        public string AmountFormatted
        {
            get
            {
                return Amount.HasValue ? $"{CurrencySymbol} {string.Format("{0:0.00}", Amount)}" : string.Empty;
            }
        }

    }
}
