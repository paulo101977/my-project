﻿using System;
using System.Collections.Generic;
using System.Text;
using Thex.Dto.Report;

namespace Thex.Dto.ReservationReport
{
    public class ReportHousekeepingStatusReturnDto
    {
        public List<ReportHousekeepingStatusResolveDto> reportHousekeepingStatusResolveDtos;
    }
}
