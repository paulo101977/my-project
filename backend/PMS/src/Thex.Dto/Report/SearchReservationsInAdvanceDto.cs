﻿using System;

namespace Thex.Dto.Report
{
    public class SearchReservationsInAdvanceDto
    {
        public DateTime? InitialDate { get; set; }
        public DateTime? EndDate { get; set; }
    }
}
