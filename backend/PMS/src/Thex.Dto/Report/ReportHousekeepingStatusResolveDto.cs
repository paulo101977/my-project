﻿using System;
using System.Collections.Generic;
using System.Text;
using Thex.Common.Enumerations;

namespace Thex.Dto.Report
{
    public class ReportHousekeepingStatusResolveDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Total { get; set; }
        public string Color { get; set; }
    }
}
