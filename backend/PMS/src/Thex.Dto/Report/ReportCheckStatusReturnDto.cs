﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Thex.Dto.Report
{
    public class ReportCheckStatusReturnDto
    {
        public CheckInReturnDto CheckInReturn { get; set; }
        public CheckOutReturnDto CheckOutReturn { get; set; }
        public WalkInReturnDto WalkInReturn { get; set; }
    }
}
