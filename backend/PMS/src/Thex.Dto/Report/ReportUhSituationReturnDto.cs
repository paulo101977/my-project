﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Thex.Dto.ReservationReport
{
    public class ReportUhSituationReturnDto
    {
        public int Vague { get; set; }
        public int Busy { get; set; }
        public int Blocked { get; set; }
    }
}
