﻿namespace Thex.Dto.Report
{
    public class ReportReservationsMadeTotalizerReturnDto
    {
        public string ReservationStatus { get; set; }
        public int Total { get; set; }
    }
}
