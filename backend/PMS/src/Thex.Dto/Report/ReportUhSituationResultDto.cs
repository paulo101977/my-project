﻿using Thex.Common.Enumerations;

namespace Thex.Dto.ReservationReport
{
    public class ReportUhSituationResultDto
    {
        public RoomAvailabilityEnum RoomAvailability { get; set; }
    }
}
