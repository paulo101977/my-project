﻿using System;

namespace Thex.Dto.Report
{
    public class SearchReservationWithDepositsDto
    {
        public DateTime? InitialDate { get; set; }
        public DateTime? EndDate { get; set; }
    }
}
