﻿using System;
using System.Collections.Generic;
using System.Text;
using Thex.Common.Enumerations;

namespace Thex.Dto.ReservationReport
{
    public class ReportHousekeepingStatusResultDto
    {
        public int HousekeepingStatusId { get; set; }
        public string HousekeepingStatusName { get; set; }
        public string Color { get; set; }
        public int? RoomId { get; set; }
    }
}
