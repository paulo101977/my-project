﻿using System;
using System.Collections.Generic;
using System.Text;
using Thex.Common.Enumerations;

namespace Thex.Dto.Report
{
    public class SearchBordereauReportDto
    {
        public DateTime? InitialDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string AccountName { get; set; }
        public bool IssuePayment { get; set; }
        public string Name { get; set; }
        public bool WasReversed { get; set; }
        public bool Synthesise { get; set; }
    }
}
