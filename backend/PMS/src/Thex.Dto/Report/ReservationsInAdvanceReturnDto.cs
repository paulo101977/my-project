﻿using System;

namespace Thex.Dto.Report
{
    public class ReservationsInAdvanceReturnDto
    {
        public string ReservationCode { get; set; }
        public string Uh{ get; set; }
        public string Abbreviation { get; set; }
        public string PaymentTypeName { get; set; }
        public decimal Amount { get; set; }
        public string BillingAccountName { get; set; }
        public string GuestName { get; set; }
        public string StatusName { get; set; }
        public DateTime ArrivalDate { get; set; }
        public DateTime DepartureDate { get; set; }
        public DateTime BillingAccountItemDate { get; set; }
        public string CurrencySymbol { get; set; }

        public string AmountFormatted
        {
            get
            {
                return $"{CurrencySymbol} {string.Format("{0:0.00}", Amount)}"; 
            }
        }
    }
}
