﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Thex.Dto
{
    public class PropertyDashboardOverviewDto
    {
        public string PropertyName { get; set; }
        public string ChainName { get; set; }
        public int TotalCheckIn { get; set; }
        public int TotalCheckOut { get; set; }
        public int TotalWalkIn { get; set; }
        public int TotalDayUse { get; set; }
        public decimal OccupationPercentage { get; set; }
        public int TotalOccupiedRoom { get; set; }
        public int TotalAvailableRoom { get; set; }
    }
}
