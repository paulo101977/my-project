﻿using System.Collections.Generic;
using Thex.Dto.Report;

namespace Thex.Dto.ReservationReport
{
    public class ReportReservationStatusReturnDto
    {
        public ReportReservationListDto ToConfirm { get; set; }
        public ReportReservationListDto Confirmed { get; set; }
        public ReportReservationListDto Checkin { get; set; }
        public ReportReservationListDto Checkout { get; set; }
        public ReportReservationListDto Pending { get; set; }
        public ReportReservationListDto NoShow { get; set; }
        public ReportReservationListDto Canceled { get; set; }
        public ReportReservationListDto WaitList { get; set; }
        public ReportReservationListDto ReservationProposal { get; set; }
    }
}
