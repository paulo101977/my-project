﻿using Tnf.Dto;

namespace Thex.Dto.Report
{
    public class ListSearchBordereauReportResultDto
    {
        public ListDto<SearchBordereauReportResultDto> ListBordereau { get; set; }
        public ListDto<ListSearchBordereauReportResultDtoPerGroup> ListBordereauPerGroup { get; set; }
    }

    public class ListSearchBordereauReportResultDtoPerGroup
    {
        public IListDto<SearchBordereauReportResultDto> BordereauPerGroup { get; set; }
        public string BillingItemName { get; set; }
        public string CurrencySymbol { get; set; }
        public decimal? GrossAmount { get; set; }
        public decimal? ReversedAmount { get; set; }
        public decimal? NetAmount { get; set; }

        public string GrossAmountFormatted
        {
            get
            {
                return GrossAmount.HasValue ? $"{CurrencySymbol} {string.Format("{0:0.00}", GrossAmount)}" : string.Empty;
            }
        }

        public string ReversedAmountFormatted
        {
            get
            {
                return ReversedAmount.HasValue ? $"{CurrencySymbol} {string.Format("{0:0.00}", ReversedAmount)}" : string.Empty;
            }
        }

        public string NetAmountFormatted
        {
            get
            {
                return NetAmount.HasValue ? $"{CurrencySymbol} {string.Format("{0:0.00}", NetAmount)}" : string.Empty;
            }
        }
    }
}
