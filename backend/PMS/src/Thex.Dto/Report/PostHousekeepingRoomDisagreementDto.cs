﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Thex.Dto.Report
{
    public class PostHousekeepingRoomDisagreementDto
    {
        public Guid? HousekeepingRoomDisagreementId { get; set; }
        public int PropertyId { get; set; }
        public Guid? HousekeepingRoomId { get; set; }
        public int? HousekeepingRoomDisagreementStatusId { get; set; }
        public int? DisagreementAdultCount { get; set; }
        public int? BagCount { get; set; }
        public int? HousekeepingRoomInspectionId { get; set; }
        public int? HousekeepingStatusId { get; set; }
        public string Observation { get; set; }
        public Guid LoggedUserUid { get; set; }
        public int RoomId { get; set; }
        public DateTime Date { get; set; }
    }
}
