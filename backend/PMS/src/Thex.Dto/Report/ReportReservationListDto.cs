﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Thex.Dto.Report
{
    public class ReportReservationListDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Total { get; set; }
    }
}
