﻿using System;
using Thex.Common.Enumerations;
using Tnf.Dto;

namespace Thex.Dto.Report
{
    public class SearchReportHousekeepingStatusDisagreementDto : RequestAllDto
    {
        public string SearchData { get; set; }
        public int? HousekeepingPropertyStatusId { get; set; }
        public int? StatusRoomId { get; set; }
        public int? RoomTypeId { get; set; }
        public DateTime? Date { get; set; }
    }
}
