﻿using System;
using Thex.Common.Enumerations;

namespace Thex.Dto.Report
{
    public class ReportCheckStatusResultDto
    {
        public DateTime? EstimatedArrivalDate { get; set; }
        public DateTime? CheckInDate { get; set; }
        public DateTime? EstimatedDepartureDate { get; set; }
        public DateTime? CheckOutDate { get; set; }       
        public bool WalkIn { get; set; }
        public ReservationStatus ReservationStatus { get; set; }
    }
}
