﻿using System;

namespace Thex.Dto.Report
{
    public class ReservationWithDepositsReturnDto
    {
        public string ReservationCode { get; set; }
        public string Uh{ get; set; }
        public string Abbreviation { get; set; }
        public decimal Amount { get; set; }
        public string BillingAccountName { get; set; }
        public string GuestName { get; set; }
        public string StatusName { get; set; }
        public DateTime ArrivalDate { get; set; }
        public DateTime DepartureDate { get; set; }
    }
}
