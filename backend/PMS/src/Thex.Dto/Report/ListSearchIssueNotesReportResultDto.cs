﻿using Thex.Dto.Reservation;
using Tnf.Dto;

namespace Thex.Dto.Report
{
    public class ListSearchIssueNotesReportResultDto
    {
        public ListDto<SearchIssuedNotesReportResultDto> ListIssuedNotes { get; set; }
        public ListDto<ListSearchIssuedNotesReportResultDtoPerSeries> ListIssuedNotesPerSeries { get; set; }
    }

    public class ListSearchIssuedNotesReportResultDtoPerSeries
    {
        public ListDto<ListSearchIssuedNotesReportResultDtoProduct> ListIssuedNotesProduct { get; set; }
        public ListDto<ListSearchIssuedNotesReportResultDtoService> ListIssuedNotesService { get; set; }
    }

    public class ListSearchIssuedNotesReportResultDtoProduct
    {
        public ListDto<ListSearchIssuedNotesReportResultDtoPerStatus> ListIssuedNotesPerStatus { get; set; }
        public GrossIssuedNotes GrossIssuedNotes { get; set; }
        public NetIssuedNotes NetIssuedNotes { get; set; }
    }

    public class ListSearchIssuedNotesReportResultDtoService
    {
        public ListDto<ListSearchIssuedNotesReportResultDtoPerStatus> ListIssuedNotesPerStatus { get; set; }
        public GrossIssuedNotes GrossIssuedNotes { get; set; }
        public NetIssuedNotes NetIssuedNotes { get; set; }
    }

    public class ListSearchIssuedNotesReportResultDtoPerStatus
    {
        public int StatusId { get; set; }
        public string StatusName { get; set; }
        public int Quantity { get; set; }
        public double Service { get; set; }
        public double Rate { get; set; }
        public double Iss { get; set; }
        public double TypeOfPayment { get; set; }
        public double PointOfSale { get; set; }
        public double Total { get; set; }
    }

    public class GrossIssuedNotes
    {
        public int Quantity { get; set; }
        public double Service { get; set; }
        public double Rate { get; set; }
        public double Iss { get; set; }
        public double TypeOfPayment { get; set; }
        public double PointOfSale { get; set; }
        public double Total { get; set; }
    }

    public class NetIssuedNotes
    {
        public int Quantity { get; set; }
        public double Service { get; set; }
        public double Rate { get; set; }
        public double Iss { get; set; }
        public double TypeOfPayment { get; set; }
        public double PointOfSale { get; set; }
        public double Total { get; set; }
    }
}
