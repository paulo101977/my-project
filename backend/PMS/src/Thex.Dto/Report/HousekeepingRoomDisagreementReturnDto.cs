﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Thex.Dto.Report
{
    public class HousekeepingRoomDisagreementReturnDto
    {
        public Guid? HousekeepingRoomDisagreementId { get; set; }
        public Guid? HousekeepingRoomId { get; set; }
        public int? RoomId { get; set; }
        public DateTime? CreationTime { get; set; }
        public int? HousekeepingRoomDisagreementStatusId { get; set; }
        public string HousekeepingRoomDisagreementStatus { get; set; }
        public int? AdultCount { get; set; }
        public int? BagCount { get; set; }
        public string Bag { get; set; }
        public string Observation { get; set; }
        public Guid? LoggedUserUid { get; set; }
        public int? HousekeepingRoomInspectionId { get; set; }
        public string HousekeepingRoomInspection { get; set; }
    }
}
