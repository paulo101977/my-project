﻿using Thex.Common.Enumerations;

namespace Thex.Dto.ReservationReport
{
    public class ReportReservationStatusResultDto
    {       
        public ReservationStatus ReservationStatus { get; set; }        
    }
}
