﻿using System;
using System.Collections.Generic;
using System.Text;
using Thex.Dto.ReservationReport;
using Tnf.Dto;

namespace Thex.Dto
{
    public class PropertyDashboardDto
    {
        public PropertyDashboardOverviewDto Overview { get; set; }
        public List<PropertyDashboardGuestDto> CheckInList { get; set; }
        public List<PropertyDashboardGuestDto> CheckOutList { get; set; }
    }
}
