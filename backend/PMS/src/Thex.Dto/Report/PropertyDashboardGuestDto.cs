﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Thex.Dto
{
    public class PropertyDashboardGuestDto
    {
        public long GuestReservationItemId { get; set; }
        public string GuestName { get; set; }
        public string GuestDocumentTypeName { get; set; }
        public string GuestDocument { get; set; }
        public long ReservationItemId { get; set; }
        public string ReservationItemCode { get; set; }
        public string RoomType { get; set; }
        public int GuestStatusId { get; set; }
        public bool IsMigrated { get; set; }
        public bool IsGroupReservation { get; set; }
        public Guid BillingAccountId { get; set; }
    }
}
