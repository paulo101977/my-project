﻿using System.Collections.Generic;
using Thex.Dto.Reservation;
using Tnf.Dto;

namespace Thex.Dto.Report
{
    public class ReportReservationsMadeReturnListDto
    {
        public ListDto<ReportReservationsMadeReturnDto> ReportReservationsMadeReturnDtoList { get; set; }
        public List<ReportReservationsMadeTotalizerReturnDto> ReportReservationsMadeTotalizerReturnDtoList { get; set; }
    }
}
