﻿using Thex.Dto.Report;

namespace Thex.Dto.ReservationReport
{
    public class ReportReturnDto
    {
        public ReportUhSituationReturnDto ReportUhSituationReturnDto { get; set; }
        public ReportHousekeepingStatusReturnDto ReportHousekeepingStatusReturnDto { get; set; }
        public ReportCheckStatusReturnDto ReportCheckStatusReturnDto { get; set; }
        public ReportReservationStatusReturnDto ReportReservationStatusReturnDto { get; set; }
    }
}
