﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Thex.Dto.Report
{
    public class CheckOutReturnDto
    {
        public int Estimated { get; set; }
        public int Accomplished { get; set; }
        public int DayUse { get; set; }
    }
}
