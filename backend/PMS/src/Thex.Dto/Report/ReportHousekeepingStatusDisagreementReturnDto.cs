﻿using System;
using Tnf.Dto;

namespace Thex.Dto.Report
{
    public class ReportHousekeepingStatusDisagreementReturnDto
    {
        public int Id { get; set; }
        public int HousekeepingStatusId { get; set; }
        public Guid HousekeepingStatusPropertyId { get; set; }
        public string Color { get; set; }
        public string HousekeepingStatusName { get; set; }
        public int RoomId { get; set; }
        public string RoomNumber { get; set; }
        public int StatusRoomId { get; set; }
        public string StatusRoom { get; set; }
        public int RoomTypeId { get; set; }
        public string RoomTypeName { get; set; }
        public DateTime? CheckInDate { get; set; }
        public DateTime? CheckOutDate { get; set; }
        public double TotalDays { get; set; }
        public int QuantityDouble { get; set; }
        public int QuantitySingle { get; set; }
        public int ExtraCribCount { get; set; }
        public int ExtraBadCount { get; set; }
        public int AdultCount { get; set; }
        public int ChildCount { get; set; }
        public int ReservationItemStatusId { get; set; }
        public DateTime? HousekeepingStatusLastModificationTime { get; set; }
        public bool Blocked { get; set; }
        public string Wing { get; set; }
        public string Floor { get; set; }
        public string Building { get; set; }
        public Guid? HousekeepingRoomDisagreementId { get; set; }
        public Guid? HousekeepingRoomId { get; set; }
        public int? HousekeepingRoomDisagreementStatusId { get; set; }
        public string HousekeepingRoomDisagreementStatus { get; set; }
        public int? DisagreementAdultCount { get; set; }
        public int? BagCount { get; set; }
        public string Bag { get; set; }
        public string Observation { get; set; }
        public Guid? LoggedUserUid { get; set; }
        public int? DisagreementRoomId { get; set; }
        public int? HousekeepingRoomInspectionId { get; set; }
        public string HousekeepingRoomInspection { get; set; }
    }
}
