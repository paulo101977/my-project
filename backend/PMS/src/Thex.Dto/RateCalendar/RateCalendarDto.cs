﻿using System;
using System.Collections.Generic;
using System.Text;
using Tnf.Dto;

namespace Thex.Dto.Availability
{
    public class RateCalendarDto : BaseDto
    {
        public int Id { get; set; }
        public decimal? BaseRateAmount { get; set; }
        public decimal? BaseRateWithRatePlanAmount { get; set; }

        public string Date { get; set; }
        public string DateFormatted { get; set; }

        public decimal Occupation { get; set; }
        public int RoomTotal { get; set; }
        public int OccupiedRooms { get; set; }

        public Guid? CurrencyId { get; set; }
        public string CurrencySymbol { get; set; }

        public string LevelCode { get; set; }
        public bool IsLevel { get; set; }
    }
}
