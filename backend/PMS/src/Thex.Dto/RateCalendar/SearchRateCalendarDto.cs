//  <copyright file="SearchRateCalendarDto.cs" company="TOTVS">
//  Copyright (c) TOTVS. All rights reserved.
//  </copyright>
using System;
using System.Collections.Generic;

namespace Thex.Dto.Reservation
{
    public class SearchRateCalendarDto
    {
        public DateTime InitialDate { get; set; }

        public DateTime FinalDate { get; set; }

        public int AdultCount { get; set; }

        public List<int> ChildrenAge { get; set; }

        public int RoomTypeId { get; set; }

        public Guid CurrencyId { get; set; }

        public int MealPlanTypeId { get; set; }

        public Guid? RatePlanId { get; set; }
    }
}
