﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Thex.Dto.RoomType
{
    public class RoomTypeOverbookingperDateSqlDto
    {
        public RoomTypeOverbookingperDateSqlDto()
        {
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Abbreviation { get; set; }
        public int AdultCapacity { get; set; }
        public int ChildCapacity { get; set; }
        public int? Qtd { get; set; }
        public int? Total { get; set; }
        public bool Overbooking => Qtd.HasValue && Total.HasValue && Total.Value - Qtd.Value < 0;
        public int TotalConjugatedRoom { get; set; }
        public DateTime? InitialDate { get; set; }
        public DateTime? FinalDate { get; set; }
    }
}
