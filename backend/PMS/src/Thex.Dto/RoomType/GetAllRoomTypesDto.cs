﻿// //  <copyright file="GetAllRoomTypesDto.cs" company="TOTVS">
// //  Copyright (c) TOTVS. All rights reserved.
// //  </copyright>
namespace Thex.Dto.RoomTypes
{
    using Tnf.Dto;

    public class GetAllRoomTypesDto : RequestAllDto
    {
        /// <summary>
        /// Gets or sets nameOrAbbreviationOrOrder
        /// </summary>
        public string SearchData { get; set; }
        public bool All { get; set; }
    }
}