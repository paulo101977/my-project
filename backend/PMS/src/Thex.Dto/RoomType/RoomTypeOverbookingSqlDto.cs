﻿// //  <copyright file="RoomTypeDto.cs" company="TOTVS">
// //  Copyright (c) TOTVS. All rights reserved.
// //  </copyright>
using System;

namespace Thex.Dto.RoomTypes
{
    public class RoomTypeOverbookingSqlDto
    {
        public RoomTypeOverbookingSqlDto()
        {
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Abbreviation { get; set; }
        public int AdultCapacity { get; set; }
        public int ChildCapacity { get; set; }
        public int? Qtd { get; set; }
        public int? Total { get; set; }
        public bool Overbooking => Qtd.HasValue && Total.HasValue && Total.Value - Qtd.Value < 0;
        public int TotalConjugatedRoom { get; set; }     
    }
}