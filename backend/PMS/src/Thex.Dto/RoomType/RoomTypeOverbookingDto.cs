﻿// //  <copyright file="RoomTypeDto.cs" company="TOTVS">
// //  Copyright (c) TOTVS. All rights reserved.
// //  </copyright>
namespace Thex.Dto.RoomTypes
{
    using System.Collections.Generic;

    using Tnf.Dto;
    public class RoomTypeOverbookingDto : BaseDto
    {
        public RoomTypeOverbookingDto()
        {
        }
        public int Id { get; set; }
        public string Name { get; set; }
        public string Abbreviation { get; set; }
        public bool Overbooking { get; set; }
        public bool OverbookingLimit { get; set; }
        public int AdultCapacity { get; set; }
        public int ChildCapacity { get; set; }
        public int TotalConjugatedRooms { get; set; }
        public bool HasConjugatedRooms => TotalConjugatedRooms > 0;
        public List<RoomDto> RoomList { get; set; }
        public List<RoomTypeBedTypeDto> RoomTypeBedTypeList { get; set; }
    }
}