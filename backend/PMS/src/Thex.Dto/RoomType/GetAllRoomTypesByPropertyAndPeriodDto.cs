﻿namespace Thex.Dto.RoomTypes
{
    using System;
    using Tnf.Dto;

    public class GetAllRoomTypesByPropertyAndPeriodDto : RequestAllDto
    {
        
        public DateTime InitialDate { get; set; }

        
        public DateTime FinalDate { get; set; }

        public bool IsActive { get; set; }

        public bool ExpandRooms { get; set; } = true;

        public byte? ChildCapacity { get; set; }

        public byte? AdultCapacity { get; set; }

        public int?  ReservationItemId { get; set; }
    }
}