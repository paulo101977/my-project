﻿using System;

namespace Thex.Dto
{
    public partial class PropertyMealPlanTypeRateSelectDto
    {
        public static PropertyMealPlanTypeRateSelectDto NullInstance = null;

        public Guid Id { get; set; }
        public int PropertyId { get; set; }
        public DateTime Date { get; set; }
        public bool? MealPlanTypeDefault { get; set; }
        public int MealPlanTypeId { get; set; }
        public decimal? AdultMealPlanAmount { get; set; }
        public decimal? Child1MealPlanAmount { get; set; }
        public decimal? Child2MealPlanAmount { get; set; }
        public decimal? Child3MealPlanAmount { get; set; }
        public Guid CurrencyId { get; set; }
        public Guid TenantId { get; set; }
        public Guid? PropertyMealPlanTypeRateHeaderId { get; set; }
        public bool Sunday { get; set; }
        public bool Monday { get; set; }
        public bool Tuesday { get; set; }
        public bool Wednesday { get; set; }
        public bool Thursday { get; set; }
        public bool Friday { get; set; }
        public bool Saturday { get; set; }
        public bool IsActive { get; set; }

        public virtual CurrencyDto Currency { get; internal set; }
        public virtual MealPlanTypeDto MealPlanType { get; internal set; }
        public virtual PropertyMealPlanTypeRateHeaderDto PropertyMealPlanTypeRateHeader { get; set; }
    }
}
