﻿using Tnf.Dto;

namespace Thex.Dto.RoomStatus
{
    public class GetAllRoomStatusDto : RequestAllDto
    {
        public int? HouseKeepingStatusId { get; set; }
    }
}
