﻿using System;
using Thex.Common.Enumerations;
using Tnf.Dto;

namespace Thex.Dto
{
    public class RoomStatusDto : BaseDto
    {
        public int RoomId { get; set; }
        public string RoomNumber { get; set; }
        public RoomAvailabilityEnum RoomStatusId { get; set; }
        public string RoomStatusName { get; set; }
        public Guid? HousekeepingStatusPropertyId { get; set; }
        public string HousekeepingStatusPropertyName { get; set; }
        public string HousekeepingStatusPropertyColor { get; set; }
        public int HousekeepingStatusId { get; set; }
        public bool IsActive { get; set; }
        public string RoomTypeName { get; set; }
        public string RoomTypeAbbreviation { get; set; }
        
        public long ReservationItemId { get; set; }
        public int AdultCount { get; set; }
        public int ChildCount { get; set; }
        public DateTime? CheckInDate { get; set; }
        public DateTime? CheckOutDate { get; set; }
        public int ReservationItemStatusId { get; set; }
        public string ReservationItemStatusName { get; set; }   
    }
}
