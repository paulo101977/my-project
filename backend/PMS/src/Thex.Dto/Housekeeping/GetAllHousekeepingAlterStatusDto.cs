﻿using System;
using Tnf.Dto;

namespace Thex.Dto.Housekeeping
{
    public class GetAllHousekeepingAlterStatusDto : BaseDto
    {
        public int Id { get; set; }
        public int HousekeepingStatusId { get; set; }
        public Guid HousekeepingStatusPropertyId { get; set; }
        public string Color { get; set; }
        public string HousekeepingStatusName { get; set; }
        public string RoomNumber { get; set; }
        public string StatusRoom { get; set; }
        public string RoomTypeName { get; set; }
        public DateTime? CheckInDate { get; set; }
        public DateTime? CheckOutDate { get; set; }
        public int QuantityDouble { get; set; }
        public int QuantitySingle { get; set; }
        public int ExtraCribCount { get; set; }
        public int ExtraBadCount { get; set; }
        public int AdultCount { get; set; }
        public int ChildCount { get; set; }
        public int ReservationItemStatusId { get; set; }
        public DateTime? HousekeepingStatusLastModificationTime { get; set; }
        public bool Blocked { get; set; }
      


    }
}
