﻿using System;
using System.Collections.Generic;
using System.Text;
using Thex.Common.Enumerations;
using Tnf.Dto;

namespace Thex.Dto.Housekeeping
{
    public class GetAllHousekeepingAlterStatusRoomDto : RequestAllDto
    {
        public string SearchData { get; set; }
        public HousekeepingStatusEnum? HousekeepingPropertyStatusId { get; set; }
    }
}
