﻿using System;
using Tnf.Dto;

namespace Thex.Dto
{
    public class ReservationItemGuestDto : BaseDto
    {
        public ReservationItemBudgetHeaderDto reservationItemBudgetHeaderDto { get; set; }

        public Guid GuestRegistrationId { get; set; }
        public long ReservationItemId { get; set; }
        public bool? IsGratuity { get; set; }
        public bool? IsSeparated { get; set; }
        public short? PaxPosition { get; set; }
    }

    public class GuestReservationItemEntranceDto : BaseDto
    {
        public DateTime? SystemDate { get; set; }
        public bool? IsGratuity { get; set; }
        public bool? IsSeparated { get; set; }
        public short? PaxPosition { get; set; }
    }
}