using Tnf.Dto;

namespace Thex.Dto.Reservation
{
    public class NewReservationCodeDto : BaseDto
    {
        public NewReservationCodeDto(string reservationCode)
        {
            ReservationCode = reservationCode;
        }

        public string ReservationCode { get; private set; }
    }
}