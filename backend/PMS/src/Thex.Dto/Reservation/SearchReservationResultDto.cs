//  <copyright file="SearchReservationResultDto.cs" company="TOTVS">
//  Copyright (c) TOTVS. All rights reserved.
//  </copyright>
namespace Thex.Dto.Reservation
{
    using System;
    using Tnf.Dto;

    public class SearchReservationResultDto : BaseDto
    {
        public static SearchReservationResultDto NullInstance = null;
        public long Id { get; set; }

        public string ReservationCode { get; set; }

        public string ReservationItemCode { get; set; }

        public long ReservationItemId { get; set; }

        public int ReservationItemStatusId { get; set; }

        public int GuestReservationItemStatusId { get; set; }

        public long GuestReservationItemId { get; set; }

        public DateTime ArrivalDate { get; set; }

        public DateTime DepartureDate { get; set; }

        public DateTime? CancellationDate { get; set; }

        public string RoomNumber { get; set; }

        public int RequestedRoomTypeId { get; set; }

        public string RequestedRoomTypeName { get; set; }

        public string RequestedRoomTypeAbbreviation { get; set; }

        public int ReceivedRoomTypeId { get; set; }

        public string ReceivedRoomTypeName { get; set; }

        public string ReceivedRoomTypeAbbreviation { get; set; }

        public long? GuestId { get; set; }

        public bool GuestIsIncognito { get; set; }

        public string GuestName { get; set; }

        public string GuestEmail { get; set; }

        public string GuestPhoneNumber { get; set; }

        public string GuestTypeName { get; set; }

        public string GuestLanguage { get; set; }

        public string Client { get; set; }

        public string GroupName { get; set; }

        public DateTime CreationTime { get; set; }

        public string ReservationBy { get; set; }


        public DateTime? Deadline { get; set; }

        public string GuestDocument { get; set; }

        public double Total { get; set; }

        public double TotalARR { get; set; }

        public Guid BillingAccountId { get; set; }

        public int BillingAccountTypeId { get; set; }

        public string BillingAccountTypeName { get; set; }

        public string ReservationItemStatusName { get; set; }

        public string GuestReservationItemStatusName { get; set; }

        public Guid TenantId { get; set; }

        public string CurrencySymbol { get; set; }

        public string CompanyClientName { get; set; }

        public Guid CompanyClientId { get; set; }

        public int MealPlanTypeId { get; set; }

        public int MarketSegmentId { get; set; }
        public string MarketSegmentName { get; set; }

        public int BusinessSourceId { get; set; }
        public string BusinessSourceName { get; set; }

        public int GuestTypeId { get; set; }

        public Guid RatePlanId { get; set; }
        public string RatePlanName { get; set; }

        public int AdultsCount { get; set; }

        public int ChildCount { get; set; }

        public DateTime? CheckInDate { get; set; }

        public DateTime? CheckOutDate { get; set; }
        public DateTime? EstimatedArrivalDateGuest { get; set; }
        public DateTime? EstimatedDepartureDateGuest { get; set; }

        public string PartnerReservationNumber { get; set; }
        public bool IsMigrated { get; set; }
        public bool IsMain { get; set; }
        public string Room { get; set; }

        public int PaymentTypeId { get; set; }
        public string PaymentTypeName { get; set; }

        public long TotalReservations { get; set; }
        public long TotalGuests { get; set; }

        public string InternalComments { get; set; }
        public string ExternalComments { get; set; }
        public string PartnerComments { get; set; }

        public int RoomId { get; set; }
    }
}