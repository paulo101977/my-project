﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Thex.Dto.Reservation
{
    public class SearchMeanPlanControlReportResultDto
    {
        public string GuestMealPlanControlId { get; set; }

        public int GuestReservationItemId { get; set; }

        public string UH { get; set; }

        public string MealPlanTypeName { get; set; }

        public int? MealPlanTypeId { get; set; }

        public int? PropertyGuestTypeId { get; set; }

        public string FullName { get; set; }

        public string GuestTypeName { get; set; }

        public string Type { get; set; }

        public bool IsChild { get; set; }

        public bool Coffee { get; set; }

        public bool Lunch { get; set; }

        public bool Dinner { get; set; }
    }
}
