﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Thex.Dto.Reservation
{
    public class SearchReservationUhReportResultReturnDto
    {
        public long Id { get; set; }

        public string UH { get; set; }      

        public int? UhSituationId { get; set; }

        public string UhSituation { get; set; }

        public int? TypeUHId { get; set; }

        public string TypeUH { get; set; }

        public int? HousekeepingStatusId { get; set; }

        public string HousekeepingStatus { get; set; }

        public string Wing { get; set; }

        public string Floor { get; set; }

        public string AdultsAndChildren { get; set; }

        public string GuestName { get; set; }

        public string GuestNameValidation { get; set; }

        public DateTime? ArrivalDate { get; set; }

        public DateTime? DepartureDate { get; set; }

        public string Company { get; set; }

        public int? MealPlanTypeId { get; set; }

        public string MealPlan { get; set; }

        public string Observation { get; set; }
        
        public int? ReservationItemStatusId { get; set; }

        public string Client { get; set; }

        public string Group { get; set; }

        public bool IsVIP { get; set; }
    }
}
