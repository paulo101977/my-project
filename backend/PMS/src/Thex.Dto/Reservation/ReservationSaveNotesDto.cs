using System;
using System.Collections.Generic;
using System.Text;
using Tnf.Dto;

namespace Thex.Dto.Reservation
{
   public class ReservationSaveNotesDto : BaseDto
    {
        public long Id { get; set; }
        public string InternalComments { get; set; }
        public string ExternalComments { get; set; }
    }
}
