//  <copyright file="ReservationHeaderResultDto.cs" company="TOTVS">
//  Copyright (c) TOTVS. All rights reserved.
//  </copyright>
namespace Thex.Dto.Reservation
{
    using System;
    using Tnf.Dto;
    using System.Collections.Generic;

    public class ReservationHeaderResultDto : BaseDto
    {
        public long Id { get; set; }
        public string ReservationCode { get; set; }
        public long ReservationItemId { get; set; }


        public string RoomLayoutId { get; set; }
        public byte QuantitySingle { get; set; }
        public byte QuantityDouble { get; set; }


        public int RequestedRoomTypeId { get; set; }
        public string RequestedRoomTypeName { get; set; }
        public string RequestedRoomTypeAbbreviation { get; set; }

        public int ReceivedRoomTypeId { get; set; }
        public string ReceivedRoomTypeName { get; set; }
        public string ReceivedRoomTypeAbbreviation { get; set; }

        public int? RoomId { get; set; }
        public string RoomNumber { get; set; }

        public DateTime ArrivalDate { get; set; }
        public DateTime DepartureDate { get; set; }

        public int AdultCount { get; set; }
        public int ChildCount { get; set; }

        public int ReservationItemStatusId { get; set; }
        public string ReservationItemStatusName { get; set; }

        public int PaymentTypeId { get; set; }
        public string PaymentType { get; set; }

        public int NumberObservations { get; set; }
        public string InternalComments { get; set; }
        public string ExternalComments { get; set; }
        public string PartnerComments { get; set; }

        public string Color { get; set; }
        public Guid?  HousekeepingStatusPropertyId { get; set; }
        public string HousekeepingStatusName { get; set; }


        public long Accomodations { get; set; }
        public decimal ReservationBudget { get; set; }


        public List<int> ChildrenAge { get; set; }
        public int PropertyId { get; set; }

        public Guid? CurrencyId { get; set; }
        public string CurrencySymbol { get; set; }
        public Guid? CompanyClientId { get; set; }
        public Guid? RatePlanId { get; set; }
        public int? MealPlanTypeId { get; set; }

        public ReservationItemBudgetHeaderDto ReservationItemBudgetHeaderDto { get; set; }
        
        public Guid TenantId { get; set; }
    }
}