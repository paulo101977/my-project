﻿
using System;

namespace Thex.Dto.Reservation
{
    public class SearchIssuedNotesReportDto
    {
        public DateTime? InitialDate { get; set; }
        public DateTime? EndDate { get; set; }
        public bool? FilterOnlyCancelledNotes { get; set; }
        public int? NumberInitialNote { get; set; }
        public int? NumberEndNote { get; set; }
        public string ReservationNumber { get; set; }
        public string CompanyClientName { get; set; }
    }
}
