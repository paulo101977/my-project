//  <copyright file="SearchReservationDto.cs" company="TOTVS">
//  Copyright (c) TOTVS. All rights reserved.
//  </copyright>
namespace Thex.Dto.Reservation
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Tnf.Dto;

    public class SearchReservationDto : RequestAllDto
    {
        public string FreeTextSearch { get; set; }

        public long? ReservationId { get; set; }
        public int? ReservationItemStatus { get; set; }
        public int? MealPlanType { get; set; }
        public int? MarketSegment { get; set; }
        public int? BusinessSource { get; set; }
        public Guid? RatePlan { get; set; }
        public int? RoomType { get; set; }
        public Guid? CompanyClientId { get; set; }
        public string CompanyClientName { get; set; }
        public int? Adults { get; set; }
        public int? Children { get; set; }
        public int? GuestType { get; set; }
        public string PartnerReservationNumber { get; set; }
        public bool IsMigrated { get; set; }
        public bool IsMain { get; set; }
        public string Room { get; set; }

        public DateTime? ArrivalInitial { get; set; }
        public DateTime? ArrivalFinal { get; set; }


        public DateTime? DepartureInitial { get; set; }
        public DateTime? DepartureFinal { get; set; }

        public DateTime? PassingByInitial { get; set; }
        public DateTime? PassingByFinal { get; set; }

        public string GroupName { get; set; }

        public DateTime? CreationInitial { get; set; }

        public DateTime? CreationFinal { get; set; }

        public DateTime? CancellationInitial { get; set; }

        public DateTime? CancellationFinal { get; set; }

        public DateTime? DeadlineInitial { get; set; }

        public DateTime? DeadlineFinal { get; set; }


        public bool? CheckInToday { get; set; }

        public bool? CheckOutToday { get; set; }

        public bool? Deadline { get; set; }

        public string ClientName { get; set; }

        public bool IsPreCheckIn { get; set; }

        public bool IsGuestVision { get; set; }

        /// <summary>
        /// The valid order by.
        /// </summary>
        private string[] ValidOrderBy => new[]
                                             {
                                                 "Id", "ReservationCode", "ReservationItemId",
                                                 "ReservationItemStatusId", "EstimatedArrivalDate",
                                                 "EstimatedDepartureDate", "CheckInDate", "CheckOutDate",
                                                 "DateOfCancellation", "RoomNumber", "RequestedRoomTypeId",
                                                 "RequestedRoomTypeName", "RequestedRoomTypeAbbreviation",
                                                 "ReceivedRoomTypeId", "ReceivedRoomTypeName",
                                                 "ReceivedRoomTypeAbbreviation", "GuestId", "GuestIsIncognito",
                                                 "GuestName", "GuestLanguage", "Client", "GroupName", "CreationTime",
                                                 "ReservationBy", "Deadline", "GuestDocument", "Total"
                                             };

        public bool HasValidOrderBy()
        {
            var validOrder = false;

            if(!string.IsNullOrEmpty(Order))
            {
                var orderSplit = Order.Split(',');

                foreach (var orderValue in orderSplit)
                {
                    var orderValidate = "";

                    if (orderValue.Contains("-"))
                        orderValidate = orderValue.Replace("-", "");
                    else
                        orderValidate = orderValue;

                    validOrder = ValidOrderBy.Any(exp => exp.ToLower() == orderValidate.ToLower());
                }

                return validOrder;
            }

            return true;
        }
    }
}