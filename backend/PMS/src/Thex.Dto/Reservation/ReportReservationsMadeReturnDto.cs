﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Thex.Dto.Reservation
{
    public class ReportReservationsMadeReturnDto
    {
        public string GuestName { get; set; }
        public int GuestTypeId { get; set; }
        public string Uh { get; set; }
        public string Abbreviation { get; set; }
        public DateTime? CheckinDate { get; set; }
        public DateTime? CheckoutDate { get; set; }
        public string AdultsAndChildren { get; set; }
        public string ReservationCode { get; set; }
        public string StatusName { get; set; }
        public string MealPlan { get; set; }        
        public string TradeName { get; set; }
        public string UserName { get; set; }
        public int ReservationStatusId { get; set; }
    }
}
