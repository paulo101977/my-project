﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Thex.Dto.Reservation
{
    public class PostMeanPlanControlReportDto 
    {
        public string GuestMealPlanControlId { get; set; }

        public int GuestReservationItemId { get; set; }

        public bool Coffee { get; set; }

        public bool Lunch { get; set; }

        public bool Dinner { get; set; }

        public DateTime GuestMealPlanControlDate { get; set; }

        public int PropertyId { get; set; }
   
    }
}
