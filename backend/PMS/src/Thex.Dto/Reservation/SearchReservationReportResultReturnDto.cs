﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace Thex.Dto.Reservation
{
    public class SearchReservationReportResultReturnDto
    {
        public long Id { get; set; }

        public string UH { get; set; }

        public string TypeUH { get; set; }

        public string GuestName { get; set; }

        public int GuestTypeId { get; set; }

        public string GuestTypeName { get; set; }        

        public string ReservationCode { get; set; }       

        public string AdultsAndChildren { get; set; }

        public int? Adult { get; set; }

        public int? Child { get; set; }

        public string Client { get; set; }

        public decimal? MealPlanTypeAmount { get; set; }

        public string MealPlan { get; set; }

        public string MealPlanType { get; set; }

        public int? MealPlanTypeId { get; set; }

        public decimal DailyAmount { get; set; }

        public DateTime ArrivalDate { get; set; }

        public DateTime DepartureDate { get; set; }

        public bool IsInternalUsage { get; set; }

        public bool IsCourtesy { get; set; }

        public string CurrencySymbol { get; set; }

        public string DailyAmountFormatted
        {
            get
            {
                return $"{CurrencySymbol} {string.Format("{0:0.00}", DailyAmount)}";
            }
        }

    }
}
