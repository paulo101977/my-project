﻿// //  <copyright file="ReservationCancelDto.cs" company="TOTVS">
// //  Copyright (c) TOTVS. All rights reserved.
// //  </copyright>
namespace Thex.Dto.Reservation
{
    using Tnf.Dto;

    public class ReservationCancelDto : BaseDto
    {
        public int Id { get; set; }
        public int ReasonId { get; set; }
        public string CancellationDescription { get; set; }
    }
}