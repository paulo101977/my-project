﻿using System;

namespace Thex.Dto.Reservation
{
    public class SearchReservationCheckReportResultList
    {
        public ListReservationDto<SearchReservationCheckReportResultReturnDto> ListReservationCheckReportDto { get; set; }
        public TotalReservationCheckReport TotalReservationCheckReport { get; set; }
    }
    
    public class SearchReservationCheckReportResultReturnDto
    {
        public long Id { get; set; }

        public string GuestName { get; set; }

        public string GuestTypeName { get; set; }

        public string UH { get; set; }

        public string TypeUH { get; set; }

        public DateTime ArrivalDate { get; set; }

        public string Status { get; set; }

        public DateTime DepartureDate { get; set; }

        public string AdultsAndChildren { get; set; }

        public string ReservationCode { get; set; }

        public string MealPlan { get; set; }

        public string MealPlanType { get; set; }

        public bool EnsuresNoShow { get; set; }

        public string Client { get; set; }

        public decimal? Deposit { get; set; }

        public int? AgreementTypeId { get; set; }

        public string AgreementName { get; set; }

        public string UserName { get; set; }

        public string CurrencySymbol { get; set; }

        public string DepositFormatted
        {
            get
            {
                return $"{CurrencySymbol} {string.Format("{0:0.00}", Deposit)}";
            }
        }

        public string InternalComments { get; set; }

        public string ExternalComments { get; set; }

        public string PartnerComments { get; set; }
    }

    public class TotalReservationCheckReport
    {
        public int TotalChecksInPeriod { get; set; }
        public int TotalGuests { get; set; }
        public int TotalEstimatedReservations { get; set; }
        public int TotalEstimatedGuests { get; set; }
        public int TotalReservationsMade { get; set; }
        public int TotalGuestsMade { get; set; }
    }
}
