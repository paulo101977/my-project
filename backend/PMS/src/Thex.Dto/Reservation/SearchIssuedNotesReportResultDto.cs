﻿

using System;

namespace Thex.Dto.Reservation
{
    public class SearchIssuedNotesReportResultDto
    {
        public Guid BillingAccountId { get; set; }
        public string UH { get; set; }
        public string RecipientsName { get; set; }
        public string ReservationNumber { get; set; }
        public string AccountName { get; set; }
        public string Type { get; set; }
        public string ExternalNumber { get; set; }
        public string ExternalSeries { get; set; }
        public string Rps { get; set; }      
        public DateTime? EmissionDate { get; set; }
        public DateTime? CancelDate { get; set; }
        public string Status { get; set; }     
        public decimal? Deposit { get; set; }
        public decimal? Card { get; set; }
        public decimal? Invoice { get; set; }
        public decimal? Check { get; set; }
        public decimal? Cash { get; set; }
        public decimal? OthersReceipts { get; set; }
        public double Restaurant { get; set; }
        public double Others { get; set; }
        public double Bars { get; set; }
        public double Daily { get; set; }
        public double Events { get; set; }
        public double Banquet { get; set; }
        public double Laundry { get; set; }
        public double Telecommunications { get; set; }
        public double Iss { get; set; }
        public double ServiceRate { get; set; }
        public double Total { get; set; }
        public double Service { get; set; }
        public double Rate { get; set; }
        public double TypeOfPayment { get; set; }
        public double PointOfSale { get; set; }
        public int StatusId { get; set; }
        public int BillingInvoiceTypeId { get; set; }
        public string CurrencySymbol { get; set; }

        public string DepositFormatted
        {
            get
            {
                return $"{CurrencySymbol} {string.Format("{0:0.00}", Deposit)}";
            }
        }

        public string CardFormatted
        {
            get
            {
                return $"{CurrencySymbol} {string.Format("{0:0.00}", Card)}";
            }
        }

        public string InvoiceFormatted
        {
            get
            {
                return $"{CurrencySymbol} {string.Format("{0:0.00}", Invoice)}";
            }
        }

        public string CheckFormatted
        {
            get
            {
                return $"{CurrencySymbol} {string.Format("{0:0.00}", Check)}";
            }
        }

        public string CashFormatted
        {
            get
            {
                return $"{CurrencySymbol} {string.Format("{0:0.00}", Cash)}";
            }
        }

        public string OthersReceiptsFormatted
        {
            get
            {
                return $"{CurrencySymbol} {string.Format("{0:0.00}", OthersReceipts)}";
            }
        }

        public string RestaurantFormatted
        {
            get
            {
                return $"{CurrencySymbol} {string.Format("{0:0.00}", Restaurant)}";
            }
        }

        public string OthersFormatted
        {
            get
            {
                return $"{CurrencySymbol} {string.Format("{0:0.00}", Others)}";
            }
        }

        public string BarsFormatted
        {
            get
            {
                return $"{CurrencySymbol} {string.Format("{0:0.00}", Bars)}";
            }
        }

        public string DailyFormatted
        {
            get
            {
                return $"{CurrencySymbol} {string.Format("{0:0.00}", Daily)}";
            }
        }

        public string EventsFormatted
        {
            get
            {
                return $"{CurrencySymbol} {string.Format("{0:0.00}", Events)}";
            }
        }

        public string BanquetFormatted
        {
            get
            {
                return $"{CurrencySymbol} {string.Format("{0:0.00}", Banquet)}";
            }
        }

        public string LaundryFormatted
        {
            get
            {
                return $"{CurrencySymbol} {string.Format("{0:0.00}", Laundry)}";
            }
        }

        public string TelecommunicationsFormatted
        {
            get
            {
                return $"{CurrencySymbol} {string.Format("{0:0.00}", Telecommunications)}";
            }
        }

        public string IssFormatted
        {
            get
            {
                return $"{CurrencySymbol} {string.Format("{0:0.00}", Iss)}";
            }
        }

        public string ServiceRateFormatted
        {
            get
            {
                return $"{CurrencySymbol} {string.Format("{0:0.00}", ServiceRate)}";
            }
        }

        public string TotalFormatted
        {
            get
            {
                return $"{CurrencySymbol} {string.Format("{0:0.00}", Total)}";
            }
        }

        public string ServiceFormatted
        {
            get
            {
                return $"{CurrencySymbol} {string.Format("{0:0.00}", Service)}";
            }
        }

        public string RateFormatted
        {
            get
            {
                return $"{CurrencySymbol} {string.Format("{0:0.00}", Rate)}";
            }
        }

        public string TypeOfPaymentFormatted
        {
            get
            {
                return $"{CurrencySymbol} {string.Format("{0:0.00}", TypeOfPayment)}";
            }
        }

        public string PointOfSaleFormatted
        {
            get
            {
                return $"{CurrencySymbol} {string.Format("{0:0.00}", PointOfSale)}";
            }
        }
    }
}
