﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Thex.Dto.Reservation
{
    public class SearchGuestSaleReportResultDto
    {
        public int PropertyId { get; set; }

        public string RoomNumber { get; set; }

        public string NumReserva { get; set; }

        public string NomeConta { get; set; }

        public string NomeHotel { get; set; }

        public Guid TenantId { get; set; }

        public string TipoConta { get; set; }

        public double Restaurante { get; set; }

        public double Outros { get; set; }

        public double Bares { get; set; }

        public double Diaria { get; set; }

        public double Eventos { get; set; }

        public double Banquetes { get; set; }

        public double Lavandeira { get; set; }

        public double Telecomunicacoes { get; set; }

        public double Iss { get; set; }

        public double TaxadeServico { get; set; }

        public double Creditos { get; set; }

        public double Total { get; set; }

        public string CurrencySymbol { get; set; }

        public string RestauranteFormatted
        {
            get
            {
                return $"{CurrencySymbol} {string.Format("{0:0.00}", Restaurante)}";
            }
        }

        public string OutrosFormatted
        {
            get
            {
                return $"{CurrencySymbol} {string.Format("{0:0.00}", Outros)}";
            }
        }

        public string BaresFormatted
        {
            get
            {
                return $"{CurrencySymbol} {string.Format("{0:0.00}", Bares)}";
            }
        }

        public string DiariaFormatted
        {
            get
            {
                return $"{CurrencySymbol} {string.Format("{0:0.00}", Diaria)}";
            }
        }

        public string EventosFormatted
        {
            get
            {
                return $"{CurrencySymbol} {string.Format("{0:0.00}", Eventos)}";
            }
        }

        public string BanquetesFormatted
        {
            get
            {
                return $"{CurrencySymbol} {string.Format("{0:0.00}", Banquetes)}";
            }
        }

        public string LavandeiraFormatted
        {
            get
            {
                return $"{CurrencySymbol} {string.Format("{0:0.00}", Lavandeira)}";
            }
        }

        public string TelecomunicacoesFormatted
        {
            get
            {
                return $"{CurrencySymbol} {string.Format("{0:0.00}", Telecomunicacoes)}";
            }
        }

        public string IssFormatted
        {
            get
            {
                return $"{CurrencySymbol} {string.Format("{0:0.00}", Iss)}";
            }
        }

        public string TaxadeServicoFormatted
        {
            get
            {
                return $"{CurrencySymbol} {string.Format("{0:0.00}", TaxadeServico)}";
            }
        }

        public string CreditosFormatted
        {
            get
            {
                return $"{CurrencySymbol} {string.Format("{0:0.00}", Creditos)}";
            }
        }

        public string TotalFormatted
        {
            get
            {
                return $"{CurrencySymbol} {string.Format("{0:0.00}", Total)}";
            }
        }

    }
}
