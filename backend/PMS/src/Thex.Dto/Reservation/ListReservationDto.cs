﻿using System.Collections.Generic;
using Tnf.Dto;

namespace Thex.Dto
{
    public partial class ListReservationDto<TDto> : IListDto<TDto>
    {
        public long TotalGuests { get; set; }
        public long TotalReservations { get; set; }

        public bool HasNext { get; set; }
        public IList<TDto> Items { get; set; }
    }
}
