﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Thex.Dto.Reservation
{
    public class SearchReservationsMadeReportDto
    {
        public DateTime? InitialDate { get; set; }
        public DateTime? EndDate { get; set; }
        public bool ToConfirm { get; set; }
        public bool Confirmed { get; set; }
        public bool Checkin { get; set; }
        public bool Checkout { get; set; }
        public bool Pending { get; set; }
        public bool NoShow { get; set; }
        public bool Canceled { get; set; }
        public bool WaitList { get; set; }
        public bool ReservationProposal { get; set; }
        public bool PassingBy { get; set; }
    }
}
