﻿
namespace Thex.Dto.Email
{
    public class EmailExternalAccessDto : EmailBaseDto
    {
        public string ExternalLink { get; set; }
    }
}
