﻿
namespace Thex.Dto.Email
{
    public class EmailUserInvitationDto : EmailBaseDto
    {
        public string UrlInvitation { get; set; }
    }
}
