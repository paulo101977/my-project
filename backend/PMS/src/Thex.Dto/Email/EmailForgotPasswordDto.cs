﻿
namespace Thex.Dto.Email
{
    public class EmailForgotPasswordDto : EmailBaseDto
    {
        public string UrlForgotPassword { get; set; }
    }
}
