﻿
namespace Thex.Dto.Email
{
    public class EmailIntegrationDto : EmailBaseDto
    {
        public string ErrorMessage { get; set; }
    }
}
