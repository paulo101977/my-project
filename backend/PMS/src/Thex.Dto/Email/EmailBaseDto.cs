﻿namespace Thex.Dto.Email
{
    public class EmailBaseDto
    {
        public string Title { get; set; }
        public string Body { get; set; }
    }
}
