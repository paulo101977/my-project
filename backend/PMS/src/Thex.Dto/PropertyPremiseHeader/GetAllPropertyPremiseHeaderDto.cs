﻿using System;
using Tnf.Dto;

namespace Thex.Dto
{
    public partial class GetAllPropertyPremiseHeaderDto : IRequestAllDto
    {
        public DateTime? StartDate { get; set; }
        public DateTime? FinalDate { get; set; }
        public Guid? CurrencyId { get; set; }
    }
}
