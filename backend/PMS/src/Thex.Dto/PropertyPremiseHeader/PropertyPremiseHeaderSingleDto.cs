﻿using System;
using System.Collections.Generic;

namespace Thex.Dto
{
    public partial class PropertyPremiseHeaderSingleDto
    {
        public static PropertyPremiseHeaderSingleDto NullInstance = null;

        public PropertyPremiseHeaderSingleDto()
        {
            PropertyPremiseList = new HashSet<PropertyPremiseDto>();
        }

        public Guid Id { get; set; }
        public int PropertyId { get; set; }
        public DateTime InitialDate { get; set; }
        public DateTime FinalDate { get; set; }
        public Guid CurrencyId { get; set; }
        public Guid TenantId { get; set; }
        public bool IsActive { get; set; }
        public string Name { get; set; }
        public int PaxReference { get; set; }
        public int RoomTypeReferenceId { get; set; }

        public virtual CurrencyDto Currency { get; set; }
        public virtual PropertyDto Property { get; set; }
        public virtual RoomTypeDto RoomTypeReference { get; set; }

        public virtual ICollection<PropertyPremiseDto> PropertyPremiseList { get; set; }
    }
}
