﻿namespace Thex.Dto
{
    public class BillingInvoiceXpressUrlDto
    {
        public string PdfUrl { get; set; }
    }
}
