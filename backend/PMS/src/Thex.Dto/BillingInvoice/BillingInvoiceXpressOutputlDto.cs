﻿namespace Thex.Dto
{
    public class BillingInvoiceXpressOutputDto
    {
        public BillingInvoiceXpressUrlDto Output { get; set; }

        public bool IsInvalid()
        {
            if (Output == null || 
                (Output != null && (string.IsNullOrEmpty(Output.PdfUrl) || (string.IsNullOrWhiteSpace(Output.PdfUrl)))))
                return true;
            else
                return false;
        }
    }
}
