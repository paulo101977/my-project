﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Thex.Dto.BillingInvoice
{
    public class InvoiceResponseDto
    {
        public static InvoiceResponseDto NullInstance = null;

        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonProperty("linkDanfe")]
        public string UrlPdf { get; set; }

        [JsonProperty("linkDownloadXml")]
        public string UrlXml { get; set; }

        [JsonProperty("id")]
        public string ExternalId { get; set; }

        [JsonProperty("externalId")]
        public string Id { get; set; }

        [JsonProperty("paymentId")]
        public string PaymentId { get; set; }

        [JsonProperty("dataCriacao")]
        public DateTime EmissionDate { get; set; }

        [JsonProperty("numero")]
        public string InvoiceNumber { get; set; }

        [JsonProperty("serie")]
        public string Series { get; set; }
    }
}
