﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Thex.Dto
{
    public class SearchBillingInvoiceDto
    {
        public DateTime? InitialDate { get; set; }
        public DateTime? FinalDate { get; set; }
        public int? InitialNumber { get; set; }
        public int? FinalNumber { get; set; }
        public bool Xml { get; set; }
        public bool Pdf { get; set; }
    }
}
