﻿using System;
using Thex.Dto.GuestRegistration;
using Tnf.Dto;

namespace Thex.Dto
{
    public class RpsDto : BaseDto
    {
        public Guid Id { get; set; }
        public BillingInvoiceDto Invoice { get; set; }
        public BillingAccountWithAccountsDto AccountEntries { get; set; }

        public PropertyDto ServiceProvider { get; set; }
        public RpsServiceRecipientDto ServiceRecipient { get; set; }
    }

    public class RpsServiceRecipientDto
    {
        public CompanyClientDto Company { get; set; }
        public GuestRegistrationResultDto Guest { get; set; }
    }
}
