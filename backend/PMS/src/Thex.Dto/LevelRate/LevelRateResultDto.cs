﻿using System;
using System.Collections.Generic;
using System.Text;
using Tnf.Dto;

namespace Thex.Dto
{
    public class LevelRateResultDto : BaseDto
    {
        public LevelRateHeaderDto LevelRateHeader { get; set; }
        public ICollection<LevelRateDto> LevelRateList { get; set; }
        public ICollection<LevelRateMealPlanTypeDto> LevelRateMealPlanTypeList {get;set;}

        public LevelRateResultDto()
        {
            this.LevelRateList = new HashSet<LevelRateDto>();
            this.LevelRateMealPlanTypeList = new HashSet<LevelRateMealPlanTypeDto>();
        }
    }
}
