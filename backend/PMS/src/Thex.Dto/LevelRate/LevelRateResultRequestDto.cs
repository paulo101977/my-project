﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Thex.Dto
{
    public class LevelRateResultRequestDto
    {
        public ICollection<LevelDto> LevelList { get; set; }
        public bool HasMealPlanTypes { get; set; }

        public LevelRateResultRequestDto()
        {
            this.LevelList = new HashSet<LevelDto>();
        }
    }
}
