﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Thex.Dto
{
    public class LevelRateRequestDto
    {
        public DateTime InitialDate { get; set; }
        public DateTime EndDate { get; set; }
        public Guid CurrencyId { get; set; }
    }
}
