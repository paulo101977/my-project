﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Thex.Dto
{
    public class LevelRateMealPlanTypeDto
    {
        public Guid? Id { get; set; }
        public bool IsMealPlanTypeDefault { get; set; }
        public string MealPlanTypeName { get; set; }
        public int MealPlanTypeId { get; set; }
        public decimal? AdultMealPlanAmount { get; set; }
        public decimal? Child1MealPlanAmount { get; set; }
        public decimal? Child2MealPlanAmount { get; set; }
        public decimal? Child3MealPlanAmount { get; set; }
    }
}
