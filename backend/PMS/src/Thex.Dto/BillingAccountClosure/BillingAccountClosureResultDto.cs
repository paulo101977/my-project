﻿using System;
using System.Collections.Generic;
using Thex.Dto.BillingAccountClosure;
using Tnf.Dto;

namespace Thex.Dto
{
    public class BillingAccountClosureResultDto : BaseDto
    {
        public static BillingAccountClosureResultDto NullInstance = null;

        public BillingAccountClosureResultDto()
        {
            this.InvoiceList = new List<BillingAccountClosureInvoiceDto>();
        }

        public Guid Id { get; set; }
        public decimal AccountBalance { get; set; }
        public List<BillingAccountClosureInvoiceDto> InvoiceList { get; set; }
        public NotificationResponseDto NotificationResponse { get; set; }
    }

    public class NotificationResponseDto
    {
        public NotificationResponseDto(string message, string detail, Guid billingInvoicePropertyId)
        {
            Message = message;
            DetailedMessage = detail;
            BillingInvoicePropertyId = billingInvoicePropertyId;
        }

        public NotificationResponseDto(string message, string detail)
        {
            Message = message;
            DetailedMessage = detail;
        }

        public static NotificationResponseDto NullInstance = null;
        public Guid BillingInvoicePropertyId { get; set; }
        public string Message { get; set; }
        public string DetailedMessage { get; set; }
    }
}




