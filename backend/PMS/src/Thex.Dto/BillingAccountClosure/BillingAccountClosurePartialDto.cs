﻿using System;
using Tnf.Dto;

namespace Thex.Dto
{
    public class BillingAccountClosurePartial : BaseDto
    {
        public Guid Id { get; set; }
        public Guid[] BillingAccountItemList { get; set; }
        public Guid[] BillingAccountList { get; set; }

        public string BillingAccountName { get; set; }
        public int PropertyId { get; set; }

        public BillingAccountItemCreditDto Credit { get; set; }
    }

}
