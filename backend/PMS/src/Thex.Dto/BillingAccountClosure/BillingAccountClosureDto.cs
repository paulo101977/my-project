﻿using System;
using Tnf.Dto;

namespace Thex.Dto
{
    public class BillingAccountClosureDto : BaseDto
    {
        public Guid Id { get; set; }
        public Guid[] BillingAccountList { get; set; }
        public Guid? OwnerDestination { get; set; }
        public bool? IsCompany { get; set; }
        public int PropertyId { get; set; }
        public int? ReservationId { get; set; }
        public long? ReservatiomItemId { get; set; }
        public string BillingAccountName { get; set; }
        public BillingAccountItemCreditDto Credit { get; set; }
    }
}
