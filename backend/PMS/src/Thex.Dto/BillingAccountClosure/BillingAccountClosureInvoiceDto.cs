﻿using System;
using System.Collections.Generic;
using System.Text;
using Thex.Common.Enumerations;
using Tnf.Dto;

namespace Thex.Dto.BillingAccountClosure
{
    public class BillingAccountClosureInvoiceDto : BaseDto
    {
        public string InvoiceId { get; set; }
        public BillingInvoiceTypeEnum Type { get; set; }
    }
}
