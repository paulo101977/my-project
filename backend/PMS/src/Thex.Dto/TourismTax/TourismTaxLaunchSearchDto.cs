﻿using System.Collections.Generic;

namespace Thex.Dto
{
    public class TourismTaxLaunchSearchDto
    {
        public List<long?> GuestReservationItemIdList { get; set; }
        public bool IsChild { get; set; }
        public bool IsMainAccount { get; set; }
        public int StatusAccount { get; set; }
        public int ReservationItemStatus { get; set; }
        public List<int?> PropertyGuestTypeIdList { get; set; }
        public int? NumberOfNights { get; set; }
    }
}
