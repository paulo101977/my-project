﻿using System;
using Tnf.Dto;

namespace Thex.Dto
{
    public class PropertyGuestPolicyDto : BaseDto
    {
        public Guid Id { get; set; }
        public static PropertyGuestPolicyDto NullInstance = null;

        public int? CountryId { get; set; }
        public string CountryCode { get; set; }
        public int PropertyId { get; set; }
        public int PolicyTypeId { get; set; }
        public string Description { get; set; }
        public bool? IsActive { get; set; }
    }
}
