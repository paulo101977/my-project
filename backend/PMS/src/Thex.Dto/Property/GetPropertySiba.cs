﻿namespace Thex.Dto
{
    public class GetPropertySiba
    {
        public long Code { get; set; }
        public int Number { get; set; }
        public string IntegrationCode { get; set; }
        public string Name { get; set; }
        public string NameAbbreviation { get; set; }
        public string Address { get; set; }
        public string Location { get; set; }
        public int PostalCode { get; set; }
        public int PostalZone { get; set; }
        public long PhoneNumber { get; set; }
        public string ContactName { get; set; }
        public string ContactEmail { get; set; }
    }
}
