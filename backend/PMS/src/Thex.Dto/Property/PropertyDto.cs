﻿namespace Thex.Dto
{
    public partial class PropertyDto
    {
        public string CNPJ { get; set; }
        public string InscEst { get; set; }
        public string InscMun { get; set; }
    }
}
