﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Thex.Dto
{
    public class PropertyInformationForDashboardDto
    {
        public int PropertyId { get; set; }
        public string PropertyName { get; set; }
        public string ChainName { get; set; }
    }
}
