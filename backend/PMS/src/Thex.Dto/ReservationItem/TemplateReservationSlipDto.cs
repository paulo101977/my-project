﻿using System;
using System.Collections.Generic;
using System.Text;
using Tnf.Dto;

namespace Thex.Dto.ReservationItem
{
  public  class TemplateReservationSlipDto : BaseDto
    {
        public int Id { get; set; }
        public string SlipReservation { get; set; }
    }
}
