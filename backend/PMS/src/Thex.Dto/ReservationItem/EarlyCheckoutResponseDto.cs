﻿using Tnf.Dto;

namespace Thex.Dto
{
    public partial class EarlyCheckoutResponseDto : BaseDto
    {
        public int Id { get; set; }
        public bool IsEarlyCheckout { get; set; }
    }
}
