﻿namespace Thex.Dto.ReservationItem
{
    using System;
    using System.Collections.Generic;

    using Tnf.Dto;

    public class AccommodationsByReservationIdResultDto : BaseDto
    {
        public long Id { get; set; }
        public long ReservationId { get; set; }
        public long RoomTypeId { get; set; }
        public long StatusId { get; set; }
        public string RoomNumber { get; set; }
        public string RoomTypeName { get; set; }
        public string RoomTypeAbbreviation { get; set; }
        
        public DateTime? CheckIn { get; set; }
        
        public DateTime? CheckOut { get; set; }
        
        public DateTime EstimatedArrivalDate { get; set; }
        
        public DateTime EstimatedDepartureDate { get; set; }
        public int AdultCount { get; set; }
        public int ChildCount { get; set; }
        public List<string> SearchableNames { get; set; }
        public bool IsValid { get; set; }
        public string ReservationContactName { get; set; }
        public string ReservationItemCode { get; set; }
        public string ReservationItemStatus { get; set; }

        public int RequestedRoomTypeId { get; set; }
        public string RequestedRoomTypeName { get; set; }
        public string RequestedRoomTypeAbbreviation { get; set; }

        public Guid? CurrencyId { get; set; }
        public int? MealPlanTypeId { get; set; }
        public int? GratuityTypeId { get; set; }
        public Guid? RatePlanId { get; set; }
        public Guid? CompanyClientId { get; set; }
        public string CompanyClientName { get; set; }
        public bool IsMigrated { get; set; }
    }
}