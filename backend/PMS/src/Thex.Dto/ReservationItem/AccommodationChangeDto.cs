﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Thex.Dto.ReservationItem
{
    public class AccommodationChangeDto
    {
        public long ReservationItemId { get; set; }
        public int ReasonId { get; set; }
        public int RoomId { get; set; }
        public int RoomTypeId { get; set; }
        public int PropertyId { get; set; }
    }
}
