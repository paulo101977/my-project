﻿// //  <copyright file="ReservationItemCancelDto.cs" company="TOTVS">
// //  Copyright (c) TOTVS. All rights reserved.
// //  </copyright>
namespace Thex.Dto.ReservationItem
{
    using Tnf.Dto;

    public class ReservationItemCancelDto : BaseDto
    {
        public int Id { get; set; }
        public int ReasonId { get; set; }
        public string CancellationDescription { get; set; }
    }
}