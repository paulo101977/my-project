﻿using System;
using System.Collections.Generic;

namespace Thex.Dto.ReservationItem
{
    public class ReservationItemSlipReservationDto
    {
        public string ReservationCode { get; set; }
        public string CompanyClientName { get; set; }
        public string ContactName { get; set; }
        public string ContactEmail { get; set; }
        public string ContactPhone { get; set; }
        public int CountNight { get; set; }
        public string ExternalComments { get; set; }

        public DateTime Checkin { get; set; }
        
        public DateTime Checkout { get; set; }
        public IList<PropertyPolicyDto> PropertyPoliciesList { get; set; }
        public IList<ReservationItemDto> ReservationItemList { get; set; }
        public ReservationConfirmationDto ReservationConfirmation { get; set; }
        

    }
}
