﻿using System;
using System.Collections.Generic;

namespace Thex.Dto
{
    public class ReservationItemWithBudgetDto
    {
        public int? MealPlanTypeId { get; set; }
        public Guid? RatePlanId { get; set; }
        public Guid? CurrencyId { get; set; }
        public int? GratuityTypeId { get; set; }

        public List<ReservationBudgetDto> ReservationBudgetDtoList { get; set; }
    }
}
