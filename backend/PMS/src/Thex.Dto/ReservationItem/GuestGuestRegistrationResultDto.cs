﻿namespace Thex.Dto.ReservationItem
{
    using System;
    using System.Collections.Generic;
    using Tnf.Dto;

    public class GuestGuestRegistrationResultDto : BaseDto
    {
        public long Id { get; set; }
        public string BusinessSourceName { get; set; }
        public long ReservationItemId { get; set; }
        public string ReservationItemCode { get; set; }
        public int ReservationItemStatusId { get; set; }
        public long GuestReservationItemId { get; set; }
        public string GuestReservationItemName { get; set; }
        public Guid? PersonId { get; set; }
        public string DocumentInformation { get; set; }
        public string DocumentTypeName { get; set; }
        public Guid? GuestRegistrationId { get; set; }
        public int GuestStatusId { get; set; }
        public bool IsLegallyIncompetent { get; set; }
        
        public DateTime? GuestRegistrationDocumentBirthDate { get; set; }
        public string Division { get; set; }
        public string Subdivision { get; set; }
        public string Country { get; set; }
        public bool? IsValid { get; set; }
        public bool IsChild { get; set; }
        public short? ChildAge { get; set; }
        public bool IsMain { get; set; }
    }
}