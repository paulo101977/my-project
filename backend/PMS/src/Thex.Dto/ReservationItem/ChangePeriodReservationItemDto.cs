using Tnf.Dto;

namespace Thex.Dto
{
    public class ChangePeriodReservationItemDto : BaseDto
    {
        public int Id { get; set; }
        public string InitialDate { get; set; }
        public string FinalDate { get; set; }
        public int RoomTypeId { get; set; }
        public int? RoomId { get; set; }
    }
}
