﻿using System;

namespace Thex.Dto
{
    public class ReservationItemDetailsDto
    {
        public Guid ReservationItemUid { get; set; }
        public string ReservationCode { get; set; }
        private string _partnerReservationNumber;

        public string PartnerReservationNumber
        {
            get
            {
                return _partnerReservationNumber == ReservationItemUid.ToString() ? String.Empty : _partnerReservationNumber;
            }
            set
            {
                _partnerReservationNumber = value;
            }
        }
    }
}
