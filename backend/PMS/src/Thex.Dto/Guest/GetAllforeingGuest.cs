﻿using System;

namespace Thex.Dto
{
    public class GetAllForeingGuest : IEquatable<GetAllForeingGuest>
    {
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string Nationality { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string BirthPlace { get; set; }
        public string DocumentInformation { get; set; }
        public string TypeDocumentIdentification { get; set; }
        public string DocumentIssuingCountry { get; set; }
        public DateTime ArrivalDate { get; set; }
        public DateTime? DepartureDate { get; set; }
        public string CountryResidenceOrigin { get; set; }
        public string Location { get; set; }

        public bool Equals(GetAllForeingGuest other)
        {
            if (DocumentInformation == other.DocumentInformation && TypeDocumentIdentification == other.TypeDocumentIdentification)
                return true;

            return false;
        }

        public override int GetHashCode()
        {
            int hashDocumentInformation = DocumentInformation == null ? 0 : DocumentInformation.GetHashCode();
            int hashTypeDocumentIdentification = TypeDocumentIdentification.GetHashCode();

            return hashDocumentInformation ^ hashTypeDocumentIdentification;
        }
    }
}
