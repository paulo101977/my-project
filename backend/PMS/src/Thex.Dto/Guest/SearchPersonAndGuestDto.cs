﻿using System;
using System.Collections.Generic;

namespace Thex.Dto
{
    public class SearchResultDto
    {
        public List<SearchResultCategoryDto> SearchResultCategory { get; set; }
    }

    public class SearchResultCategoryDto
    {
        public string SearchCategoryName { get; set; }
        public List<SearchPersonAndGuestDto> SearchPersonAndGuest { get; set; }
    }

    public class SearchPersonAndGuestDto
    {
        public Guid PersonId { get; set; }
        public string Name { get; set; }
        public string Document { get; set; }
        public int DocumentTypeId { get; set; }
        public string DocumentType { get; set; }
        public long? GuestId { get; set; }
    }

}
