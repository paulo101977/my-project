﻿namespace Thex.Dto.Base
{
    using System;
    using Tnf.Dto;

    public class PeriodDto : RequestAllDto
    {
        public DateTime InitialDate { get; set; }
        public DateTime FinalDate { get; set; }
    }
}