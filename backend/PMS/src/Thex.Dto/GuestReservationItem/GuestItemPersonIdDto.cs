using System;
using System.Collections.Generic;
using System.Text;

namespace Thex.Dto.GuestReservationItem
{
    public class GuestItemPersonIdDto
    {
        public Guid PersonId { get; set; }
        public Guid GuestRegistrationId { get; set; }
        public int GuestId { get; set; }
    }
}
