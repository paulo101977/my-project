using System;

namespace Thex.Dto.GuestReservationItem
{
    public class GuestReservationItemWithCountrySubdivisionDto
    {


        public string GuestName { get; set; }
        public string FullName { get; set; }
        public string GuestEmail { get; set; }
        public string GuestDocument { get; set; }
        public int? GuestDocumentTypeId { get; set; }
        public int GuestTypeId { get; set; }
        public string GuestTypeName { get; set; }
        public string PreferredName { get; set; }
        public int CountrySubdivisionId { get; set; }
        public long? GuestId { get; set; }
    }
}
