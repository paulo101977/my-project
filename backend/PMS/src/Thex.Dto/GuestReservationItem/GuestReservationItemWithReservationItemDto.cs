using System;

namespace Thex.Dto.GuestReservationItem
{
    public class GuestReservationItemWithReservationItemDto
    {
        //roomid
        //reservationitemcode
        //reservation code
        //initialdate
        //finaldate

        public int RoomId { get; set; }
        public string ReservationItemCode { get; set; }
        public string ReservationCode { get; set; }
        public DateTime InitialDate { get; set; }
        public DateTime FinalDate { get; set; }
    }
}
