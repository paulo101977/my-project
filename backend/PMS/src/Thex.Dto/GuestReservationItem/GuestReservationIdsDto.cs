using System;
using System.Collections.Generic;
using System.Text;
using Tnf.Dto;

namespace Thex.Dto.GuestReservationItem
{
    public class GuestReservationIdsDto : BaseDto
    {
        public long Id { get; set; }
        public long GuestReservationItemId { get; set; }
    }
}
