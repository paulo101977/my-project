﻿//  <copyright file="SearchDto.cs" company="TOTVS">
//  Copyright (c) TOTVS. All rights reserved.
//  </copyright>
namespace Thex.Dto.Common
{
    using System.Collections.Generic;

    using Tnf.Dto;

    public class PagingDto
    {

        public const int PAGE_SIZE_MAX_VALUE_THEX = 1000;

        private int _page;
        public int Page
        {
            get => _page <= 0 ? 1 : _page;
            set => _page = value;
        }

        private int _pageSize;
        public int PageSize
        {
            get
            {
                if (_pageSize == 0)
                    PageSize = 0;

                return _pageSize;
            }
            set
            {
                _pageSize = value;
                var defaultValue = RequestAllDtoConfiguration.PageSizeDefaultValue;
                var maxValue = RequestAllDtoConfiguration.PageSizeMaxValue;

                if (defaultValue == 0)
                    defaultValue = 10;

                if (maxValue == 0)
                    maxValue = 100;

                if (_pageSize <= 0)
                    _pageSize = defaultValue.HasValue ? defaultValue.Value : PAGE_SIZE_MAX_VALUE_THEX;

                if (_pageSize > maxValue)
                    _pageSize = maxValue.HasValue ? maxValue.Value : PAGE_SIZE_MAX_VALUE_THEX;
            }
        }
    }
}