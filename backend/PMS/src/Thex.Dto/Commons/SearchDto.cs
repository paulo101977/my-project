﻿//  <copyright file="SearchDto.cs" company="TOTVS">
//  Copyright (c) TOTVS. All rights reserved.
//  </copyright>
namespace Thex.Dto.Common
{
    using System.Collections.Generic;

    using Tnf.Dto;

    public class SearchDto: PagingDto
    {
        public List<SearchOrderDto> OrderList { get; set; }
    }
}