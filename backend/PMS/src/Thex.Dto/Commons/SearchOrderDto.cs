﻿//  <copyright file="SearchOrderDto.cs" company="TOTVS">
//  Copyright (c) TOTVS. All rights reserved.
//  </copyright>
namespace Thex.Dto.Common
{
    public class SearchOrderDto
    {
        /// <summary>
        /// Gets or sets the field.
        /// </summary>
        public string Field { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether descending.
        /// </summary>
        public bool Descending { get; set; }
    }
}