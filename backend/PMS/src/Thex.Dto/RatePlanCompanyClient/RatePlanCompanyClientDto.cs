﻿using System;

namespace Thex.Dto
{
    public partial class RatePlanCompanyClientDto
    {
        public Boolean IsActive { get; set; }

        public string Type { get; set; }

        public string Name { get; set; }

        public string Document { get; set; }

        public string Email { get; set; }

        public string Phone { get; set; }

        public string HomePage { get; set; }

        public Guid ClientCategoryId { get; set; }

        public string ClientCategoryName { get; set; }
        public string Address { get; set; }

    }
}
