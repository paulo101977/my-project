﻿using System;

namespace Thex.Dto
{
    public class CurrencyExchangeResultDto
    {
        public Guid? CurrencyExchangeReferenceId { get; set; }
        public Guid? CurrencyExchangeReferenceSecId { get; set; }
    }
}
