﻿using Thex.Domain;
using Thex.Infra.Context;
using Thex.Infra.SqlServer.Context;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;
using Microsoft.Extensions.DependencyInjection;
using Thex.Common;

namespace Thex.Infra.SqlServer
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddSqlServerDependency(this IServiceCollection services)
        {
            services
                .AddInfraDependency()
                .AddTnfDbContext<ThexContext, SqlServerCrudDbContext>((config) =>
                {
                    if (config.ExistingConnection != null)
                        config.DbContextOptions.UseSqlServer(config.ExistingConnection, db => db.UseRowNumberForPaging());
                    else
                        config.DbContextOptions.UseSqlServer(config.ConnectionString, db => db.UseRowNumberForPaging());
                });

            return services;
        }
    }
}
