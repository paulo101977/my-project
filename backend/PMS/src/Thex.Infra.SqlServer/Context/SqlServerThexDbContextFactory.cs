﻿using Thex.Infra.Context;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using System.IO;
using Tnf.Runtime.Session;
using Thex.Kernel;
using Thex.GenericLog;
using System;
using System.Diagnostics;
using Thex.Common;

namespace Thex.Infra.SqlServer.Context
{
    public class SqlServerThexDbContextFactory : IDesignTimeDbContextFactory<SqlServerCrudDbContext>
    {
        private IApplicationUser _applicationUser;
        private IConfiguration _configuration;
        private IGenericLogHandler _genericLog;
        private IServiceProvider _serviceProvider;

        public SqlServerThexDbContextFactory(IApplicationUser applicationUser,
                                             IConfiguration configuration,
                                             IGenericLogHandler genericLog,
                                             IServiceProvider serviceProvider)
            :base()
        {
            _applicationUser = applicationUser;
            _configuration = configuration;
            _genericLog = genericLog;
            _serviceProvider = serviceProvider;
        }

        public SqlServerCrudDbContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<ThexContext>();

            var configuration = new ConfigurationBuilder()
                                    .SetBasePath(Directory.GetCurrentDirectory())
                                    .AddJsonFile($"appsettings.Development.json", false)
                                    .Build();

            var databaseConfiguration = new DatabaseConfiguration(configuration);

            var connectionString = databaseConfiguration.ConnectionString;
            builder.UseSqlServer(connectionString, db => db.UseRowNumberForPaging());

            return new SqlServerCrudDbContext(_applicationUser, _configuration, builder.Options, NullTnfSession.Instance, _genericLog, _serviceProvider);
        }
    }
}
