﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using Thex.GenericLog;
using Thex.Infra.Context;
using Thex.Kernel;
using Tnf.Runtime.Session;

namespace Thex.Infra.SqlServer.Context
{
    public class SqlServerCrudDbContext : ThexContext
    {
        public SqlServerCrudDbContext(IApplicationUser applicationUser, 
                                      IConfiguration configuration, 
                                      DbContextOptions<ThexContext> options, 
                                      ITnfSession session,
                                      IGenericLogHandler genericLog,
                                      IServiceProvider serviceProvider)
            : base(applicationUser, configuration, options, session, genericLog, serviceProvider)
        {
        }
    }
}