﻿
using System;
using Tnf.Builder;
using Tnf.Notifications;

namespace Thex.Domain.Entities
{
    public partial class SibaIntegration
    {
        public class Builder : Builder<SibaIntegration>
        {
            public Builder(INotificationHandler handler) : base(handler)
            {
            }

            public Builder(INotificationHandler handler, SibaIntegration instance) : base(handler, instance)
            {
            }

            public virtual Builder WithId(Guid id)
            {
                Instance.Id = id;
                return this;
            }

            public virtual Builder WithPropertyUId(Guid propertyUId)
            {
                Instance.PropertyUId = propertyUId;
                return this;
            }

            public virtual Builder WithIntegrationFileNumber(int integrationFileNumber)
            {
                Instance.IntegrationFileNumber = integrationFileNumber;
                return this;
            }
        }
    }
}
