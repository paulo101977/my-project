﻿using System;
using Thex.Common;
using Tnf.Builder;
using Tnf.Notifications;
using Tnf.Specifications;

namespace Thex.Domain.Entities
{
    public partial class CompanyClientChannel
    {
        public class Builder : Builder<CompanyClientChannel>
        {

            public Builder(INotificationHandler handler) : base(handler)
            {
            }

            public Builder(INotificationHandler handler, CompanyClientChannel instance) : base(handler, instance)
            {
            }

            public virtual Builder WithId(Guid id)
            {
                Instance.Id = id;
                return this;
            }
            public virtual Builder WithChannelId(Guid channelId)
            {
                Instance.ChannelId = channelId;
                return this;
            }
            public virtual Builder WithCompanyClientId(Guid companyClientId)
            {
                Instance.CompanyClientId = companyClientId;
                return this;
            }
            public virtual Builder WithIsActive(bool isActive)
            {
                Instance.IsActive = isActive;
                return this;
            }

            public virtual Builder WithCompanyId(string companyId)
            {
                Instance.CompanyId = companyId;
                return this;
            }

            protected override void Specifications()
            {
                AddSpecification(new ExpressionSpecification<CompanyClientChannel>(
                    AppConsts.LocalizationSourceName,
                    CompanyClientChannel.EntityError.CompanyClientChannelMustHaveChannelId,
                    w => w.ChannelId != Guid.Empty));

                AddSpecification(new ExpressionSpecification<CompanyClientChannel>(
                    AppConsts.LocalizationSourceName,
                    CompanyClientChannel.EntityError.CompanyClientChannelMustHaveCompanyClientId,
                    w => w.CompanyClientId != Guid.Empty));

                AddSpecification(new ExpressionSpecification<CompanyClientChannel>(
                   AppConsts.LocalizationSourceName,
                   CompanyClientChannel.EntityError.CompanyClientChannelMustHaveCompanyId,
                   w =>!string.IsNullOrWhiteSpace(w.CompanyId)));
            }
        }
    }
}
