﻿using Thex.Common;
using Tnf.Builder;
using Tnf.Notifications;
using Tnf.Specifications;

namespace Thex.Domain.Entities
{
    public partial class IntegrationPartner
    {
        public class Builder : Builder<IntegrationPartner>
        {
            public Builder(INotificationHandler handler) : base(handler)
            {
            }

            public Builder(INotificationHandler handler, IntegrationPartner instance) : base(handler, instance)
            {
            }

            public virtual Builder WithId(int id)
            {
                Instance.Id = id;
                return this;
            }
            public virtual Builder WithTokenClient(string tokenClient)
            {
                Instance.TokenClient = tokenClient;
                return this;
            }

            public virtual Builder WithTokenApplication(string tokenApplication)
            {
                Instance.TokenApplication = tokenApplication;
                return this;
            }

            public virtual Builder WithIntegrationPartnerName(string integrationPartnerName)
            {
                Instance.IntegrationPartnerName = integrationPartnerName;
                return this;
            }

            public virtual Builder WithIntegrationPartnerType(int integrationPartnerType)
            {
                Instance.IntegrationPartnerType = integrationPartnerType;
                return this;
            }

            public virtual Builder WithIsActive(bool isActive)
            {
                Instance.IsActive = isActive;
                return this;
            }

            internal void EntitySpecifications()
            {
                AddSpecification(new ExpressionSpecification<IntegrationPartner>(
                    AppConsts.LocalizationSourceName,
                    IntegrationPartner.EntityError.IntegrationPartnerMustHaveIntegrationPartnerName,
                    w => !string.IsNullOrWhiteSpace(w.IntegrationPartnerName)));

                AddSpecification(new ExpressionSpecification<IntegrationPartner>(
                    AppConsts.LocalizationSourceName,
                    IntegrationPartner.EntityError.IntegrationPartnerMustHaveIntegrationPartnerType,
                    w => w.IntegrationPartnerType != 0));
            }
        }
    }
}
