﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a Crudzilla tool.
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using Tnf.Builder;
using Tnf.Notifications;
using Tnf.Specifications;


using Thex.Dto;
using System.Linq;
using Thex.Common.Enumerations;
using Thex.Common;
using Thex.EntityFrameworkCore.ReadInterfaces.Repositories;
using Thex.Domain.Specifications.Property;

namespace Thex.Domain.Entities
{
    public partial class ReservationItem
    {
        public class Builder : Builder<ReservationItem>
        {
            IRoomReadRepository _roomReadRepository;
            ReservationItem _oldReservationItem;
            int _propertyId;
            private readonly bool _isReservation;

            protected virtual void NotifyInvalidConstructor()
            {
                Notification.Raise(Notification
                    .DefaultBuilder
                    .WithMessage(AppConsts.LocalizationSourceName, CommonsEnum.Error.InvalidConstructor)
                    .Build());
            }

            public Builder(INotificationHandler handler, IRoomReadRepository roomReadRepository, ReservationItem oldReservationItem, int propertyId, bool isReservation = false) : base(handler)
            {
                _roomReadRepository = roomReadRepository;
                _oldReservationItem = oldReservationItem;
                _propertyId = propertyId;
                _isReservation = isReservation;
            }

            public Builder(INotificationHandler handler, ReservationItem instance, IRoomReadRepository roomReadRepository, ReservationItem oldReservationItem, int propertyId) : base(handler, instance)
            {
                _roomReadRepository = roomReadRepository;
                _oldReservationItem = oldReservationItem;
                _propertyId = propertyId;
            }

            public Builder(INotificationHandler handler, int propertyId) : base(handler)
            {
                _propertyId = propertyId;
            }

            public Builder(INotificationHandler handler) : base(handler)
            {
            }

            public Builder(INotificationHandler handler, ReservationItem instance) : base(handler, instance)
            {
                NotifyInvalidConstructor();
            }

            public virtual Builder WithId(long id)
            {
                Instance.Id = id;
                return this;
            }
            public virtual Builder WithReservationItemUid(Guid reservationItemUid)
            {
                Instance.ReservationItemUid = reservationItemUid == Guid.Empty ? Guid.NewGuid() : reservationItemUid;
                return this;
            }
            public virtual Builder WithReservationId(long reservationId)
            {
                Instance.ReservationId = reservationId;
                return this;
            }
            public virtual Builder WithReasonId(int? reasonId)
            {
                Instance.ReasonId = reasonId;
                return this;
            }
            public virtual Builder WithEstimatedArrivalDate(DateTime estimatedArrivalDate)
            {
                Instance.EstimatedArrivalDate = estimatedArrivalDate;
                return this;
            }
            public virtual Builder WithCheckInDate(DateTime? checkInDate)
            {
                Instance.CheckInDate = checkInDate;
                return this;
            }
            public virtual Builder WithEstimatedDepartureDate(DateTime estimatedDepartureDate)
            {
                Instance.EstimatedDepartureDate = estimatedDepartureDate;
                return this;
            }
            public virtual Builder WithCheckOutDate(DateTime? checkOutDate)
            {
                Instance.CheckOutDate = checkOutDate;
                return this;
            }
            public virtual Builder WithCancellationDate(DateTime? cancellationDate)
            {
                Instance.CancellationDate = cancellationDate;
                return this;
            }
            public virtual Builder WithCancellationDescription(string cancellationDescription)
            {
                Instance.CancellationDescription = cancellationDescription;
                return this;
            }
            public virtual Builder WithReservationItemCode(string reservationItemCode)
            {
                Instance.ReservationItemCode = reservationItemCode;
                return this;
            }
            public virtual Builder WithRequestedRoomTypeId(int requestedRoomTypeId)
            {
                Instance.RequestedRoomTypeId = requestedRoomTypeId;
                return this;
            }
            public virtual Builder WithReceivedRoomTypeId(int receivedRoomTypeId)
            {
                Instance.ReceivedRoomTypeId = receivedRoomTypeId;
                return this;
            }
            public virtual Builder WithRoomId(int? roomId)
            {
                Instance.RoomId = roomId;
                return this;
            }
            public virtual Builder WithAdultCount(byte adultCount)
            {
                Instance.AdultCount = adultCount;
                return this;
            }
            public virtual Builder WithChildCount(byte childCount)
            {
                Instance.ChildCount = childCount;
                return this;
            }
            public virtual Builder WithReservationItemStatus(ReservationStatus reservationItemStatus, ReservationConfirmationDto reservationConfirmation = null)
            {
                var paymentTypes = new int[] {
                (int)PaymentTypeEnum.Deposit,
                (int)PaymentTypeEnum.CreditCard
            };

                if (reservationConfirmation != null &&
                    reservationItemStatus == (int)ReservationStatus.ToConfirm &&
                    ((paymentTypes.Contains((int)reservationConfirmation.PaymentTypeId) && reservationConfirmation.IsConfirmed.HasValue && reservationConfirmation.IsConfirmed.Value) ||
                    (!reservationConfirmation.Deadline.HasValue)))
                    Instance.ReservationItemStatusId = (int)ReservationStatus.Confirmed;
                else
                    Instance.ReservationItemStatusId = (int)reservationItemStatus;

                return this;
            }
            public virtual Builder WithRoomLayoutId(Guid roomLayoutId)
            {
                Instance.RoomLayoutId = roomLayoutId;
                return this;
            }
            public virtual Builder WithExtraBedCount(byte extraBedCount)
            {
                Instance.ExtraBedCount = extraBedCount;
                return this;
            }
            public virtual Builder WithExtraCribCount(byte extraCribCount)
            {
                Instance.ExtraCribCount = extraCribCount;
                return this;
            }

            public virtual Builder WithReservationBudgetList(IList<ReservationBudget> reservationBudgetList)
            {
                Instance.ReservationBudgetList = reservationBudgetList;
                return this;
            }

            public virtual Builder WithGuestReservationItemList(IList<GuestReservationItem> guestReservationItemList)
            {
                List<GuestReservationItem> aux = new List<GuestReservationItem>();

                foreach (var x in guestReservationItemList)
                {
                    x.GuestStatusId = Instance.ReservationItemStatusId;
                    aux.Add(x);
                }

                Instance.GuestReservationItemList = aux;
                return this;
            }

            public virtual Builder WithRatePlanId(Guid? ratePlanId)
            {
                Instance.RatePlanId = ratePlanId;
                return this;
            }

            public virtual Builder WithCurrencyId(Guid? currencyId)
            {
                Instance.CurrencyId = currencyId;
                return this;
            }

            public virtual Builder WithMealPlanTypeId(int? mealPlanTypeId)
            {
                Instance.MealPlanTypeId = mealPlanTypeId;
                return this;
            }
            public virtual Builder WithGratuityTypeId(int? gratuityTypeId)
            {
                Instance.GratuityTypeId = gratuityTypeId;
                return this;
            }

            public virtual Builder WithCheckInHour(DateTime? checkInHour)
            {
                Instance.CheckInHour = checkInHour;
                return this;
            }
            public virtual Builder WithCheckOutHour(DateTime? checkOutHour)
            {
                Instance.CheckOutHour = checkOutHour;
                return this;
            }
            public virtual Builder WithWalkIn(bool WalkIn)
            {
                Instance.WalkIn = WalkIn;
                return this;
            }
            public virtual Builder WithExternalReservationNumber(string externalReservationNumber)
            {
                Instance.ExternalReservationNumber = string.IsNullOrWhiteSpace(externalReservationNumber)
                    ? Instance.ReservationItemUid.ToString() : externalReservationNumber;
                return this;
            }
            public virtual Builder WithPartnerReservationNumber(string partnerReservationNumber)
            {
                Instance.PartnerReservationNumber = partnerReservationNumber;
                return this;
            }
            public virtual Builder WithIsMigrated(bool isMigrated)
            {
                Instance.IsMigrated = isMigrated;
                return this;
            }

            public virtual Builder WithCheckOutProcess(DateTime checkOutDate)
            {
                Instance.SetToCheckout(checkOutDate);
                return this;
            }
            protected override void Specifications()
            {
                //AddSpecification(new ExpressionSpecification<ReservationItem>(
                //	AppConsts.LocalizationSourceName, 
                //	ReservationItem.EntityError.ReservationItemMustHaveReservationId, 
                //	w => w.ReservationId != default(long)));

                AddSpecification(new ExpressionSpecification<ReservationItem>(
                    AppConsts.LocalizationSourceName,
                    ReservationItem.EntityError.ReservationItemInvalidEstimatedArrivalDate,
                    w => w.EstimatedArrivalDate >= new DateTime(1753, 1, 1) && w.EstimatedArrivalDate <= DateTime.MaxValue));

                AddSpecification(new ExpressionSpecification<ReservationItem>(
                    AppConsts.LocalizationSourceName,
                    ReservationItem.EntityError.ReservationItemInvalidCheckInDate,
                    w => w.CheckInDate == null || w.CheckInDate >= new DateTime(1753, 1, 1) && w.CheckInDate <= DateTime.MaxValue));

                AddSpecification(new ExpressionSpecification<ReservationItem>(
                    AppConsts.LocalizationSourceName,
                    ReservationItem.EntityError.ReservationItemInvalidEstimatedDepartureDate,
                    w => w.EstimatedDepartureDate >= new DateTime(1753, 1, 1) && w.EstimatedDepartureDate <= DateTime.MaxValue));

                AddSpecification(new ExpressionSpecification<ReservationItem>(
                    AppConsts.LocalizationSourceName,
                    ReservationItem.EntityError.ReservationItemInvalidCheckOutDate,
                    w => w.CheckOutDate == null || w.CheckOutDate >= new DateTime(1753, 1, 1) && w.CheckOutDate <= DateTime.MaxValue));

                AddSpecification(new ExpressionSpecification<ReservationItem>(
                    AppConsts.LocalizationSourceName,
                    ReservationItem.EntityError.ReservationItemInvalidCancellationDate,
                    w => w.CancellationDate == null || w.CancellationDate >= new DateTime(1753, 1, 1) && w.CancellationDate <= DateTime.MaxValue));

                AddSpecification(new ExpressionSpecification<ReservationItem>(
                    AppConsts.LocalizationSourceName,
                    ReservationItem.EntityError.ReservationItemOutOfBoundCancellationDescription,
                    w => string.IsNullOrWhiteSpace(w.CancellationDescription) || w.CancellationDescription.Length > 0 && w.CancellationDescription.Length <= 1000));

                AddSpecification(new ExpressionSpecification<ReservationItem>(
                    AppConsts.LocalizationSourceName,
                    ReservationItem.EntityError.ReservationItemMustHaveReservationItemCode,
                    w => !string.IsNullOrWhiteSpace(w.ReservationItemCode)));

                AddSpecification(new ExpressionSpecification<ReservationItem>(
                    AppConsts.LocalizationSourceName,
                    ReservationItem.EntityError.ReservationItemOutOfBoundReservationItemCode,
                    w => string.IsNullOrWhiteSpace(w.ReservationItemCode) || w.ReservationItemCode.Length > 0 && w.ReservationItemCode.Length <= 50));

                AddSpecification(new ExpressionSpecification<ReservationItem>(
                    AppConsts.LocalizationSourceName,
                    ReservationItem.EntityError.ReservationItemMustHaveRequestedRoomTypeId,
                    w => w.RequestedRoomTypeId != default(int)));

                AddSpecification(new ExpressionSpecification<ReservationItem>(
                    AppConsts.LocalizationSourceName,
                    ReservationItem.EntityError.ReservationItemMustHaveReceivedRoomTypeId,
                    w => w.ReceivedRoomTypeId != default(int)));

                //AddSpecification(new ExpressionSpecification<ReservationItem>(
                //	AppConsts.LocalizationSourceName, 
                //	ReservationItem.EntityError.ReservationItemMustHaveReservationItemStatusId, 
                //	w => w.ReservationItemStatusId != default(int)));

                AddSpecification(new ExpressionSpecification<ReservationItem>(
                    AppConsts.LocalizationSourceName,
                    ReservationItem.EntityError.ReservationItemMustHaveRoomLayoutId,
                    w => w.RoomLayoutId != default(Guid)));


                var validadeIfRoomIsAvailable = false;

                //DISPONIBILIDADE DO QUARTO

                //1 - (inclusão) quando RoomId não estiver nulo e for inserção
                if (Instance.RoomId.HasValue && _oldReservationItem == null)
                    validadeIfRoomIsAvailable = true;

                //2 - (edição:reativar) quando RoomId não estiver nulo, 
                //    quando o status do reservationitem anterior for cancelamento 
                //    quando o status do reservationitem anterior for diferente do atual
                if (Instance.RoomId.HasValue && _oldReservationItem != null &&
                    _oldReservationItem.ReservationItemStatusId == (int)ReservationStatus.Canceled &&
                    _oldReservationItem.ReservationItemStatusId != Instance.ReservationItemStatusId)
                    validadeIfRoomIsAvailable = true;

                //3 - (edição) quando ele não estiver nulo, for edição e o quarto for diferente do quarto anterior
                if (Instance.RoomId.HasValue && _oldReservationItem != null &&
                    (!_oldReservationItem.RoomId.HasValue || Instance.RoomId.Value != _oldReservationItem.RoomId.Value))
                    validadeIfRoomIsAvailable = true;
                
                if (_oldReservationItem != null && _oldReservationItem.ReservationItemStatusId == (int)ReservationStatus.Checkin && _isReservation)
                {
                    int changes = default(int);
                    if (Instance.EstimatedArrivalDate.Date != (_oldReservationItem.CheckInDate ?? _oldReservationItem.EstimatedArrivalDate).Date)
                        changes++;
                       
                    if (Instance.CheckInDate.HasValue && Instance.CheckInDate.Value.Date != (_oldReservationItem.CheckInDate ?? _oldReservationItem.EstimatedArrivalDate).Date)
                        changes++;

                    if (Instance.ReceivedRoomTypeId != _oldReservationItem.ReceivedRoomTypeId)
                        changes++;

                    if (Instance.RequestedRoomTypeId != _oldReservationItem.RequestedRoomTypeId)
                        changes++;

                    if (Instance.RoomId != _oldReservationItem.RoomId)
                        changes++;                  

                    AddSpecification(new ExpressionSpecification<ReservationItem>(
                        AppConsts.LocalizationSourceName,
                        ReservationItem.EntityError.ReservationItemCanUpdateOnlyEstimatedDepartureOrLayoutOrExtraInformation, w => changes == default(int)));

                    var guestReservationItemIds = _oldReservationItem.GuestReservationItemList.Where(gri => gri.GuestStatusId == (int)ReservationStatus.Checkin).Select(gri => gri.Id);
                    var guestReservationItemDtoIds = Instance.GuestReservationItemList.Where(gri => gri.Id != default(long)).Select(gri => gri.Id);

                    AddSpecification(new ExpressionSpecification<ReservationItem>(
                        AppConsts.LocalizationSourceName,
                        GuestReservationItem.EntityError.CanNotRemoveGuestReservationItemWithStatusInCheckin,
                        w => guestReservationItemDtoIds.Intersect(guestReservationItemIds).Count() == guestReservationItemIds.Count()));
                }
            }
        }
    }
}
