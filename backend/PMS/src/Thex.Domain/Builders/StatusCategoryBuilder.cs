﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a Crudzilla tool.
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using Thex.Common;
using Tnf.Builder;
using Tnf.Notifications;
using Tnf.Specifications;



namespace Thex.Domain.Entities
{
    public partial class StatusCategory
    {
    public class Builder : Builder<StatusCategory>
    {
		public Builder(INotificationHandler handler) : base(handler)
		{
		}

		public Builder(INotificationHandler handler, StatusCategory instance) : base(handler, instance)
		{
		}
		
        public virtual Builder WithId(int id)
        {
            Instance.Id = id;
            return this;
        }
        public virtual Builder WithName(string name)
        {
            Instance.Name = name;
            return this;
        }

        protected override void Specifications()
        {
            AddSpecification(new ExpressionSpecification<StatusCategory>(
				AppConsts.LocalizationSourceName, 
				StatusCategory.EntityError.StatusCategoryMustHaveName, 
				w => !string.IsNullOrWhiteSpace(w.Name)));

            AddSpecification(new ExpressionSpecification<StatusCategory>(
				AppConsts.LocalizationSourceName, 
				StatusCategory.EntityError.StatusCategoryOutOfBoundName, 
				w => string.IsNullOrWhiteSpace(w.Name) || w.Name.Length > 0 && w.Name.Length <= 40));

        }
    }
}
}
