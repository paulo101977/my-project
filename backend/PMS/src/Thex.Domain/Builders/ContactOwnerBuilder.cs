﻿using System;
using Tnf.Builder;
using Tnf.Notifications;

namespace Thex.Domain.Entities
{
    public partial class ContactOwner
    {
        public class Builder : Builder<ContactOwner>
        {
            public Builder(INotificationHandler handler) : base(handler)
            {
            }

            public Builder(INotificationHandler handler, ContactOwner instance) : base(handler, instance)
            {
            }

            public virtual Builder WithId(Guid id)
            {
                Instance.Id = id;
                return this;
            }
            public virtual Builder WithPersonId(Guid personId)
            {
                Instance.PersonId = personId;
                return this;
            }

            public virtual Builder WithPerson(Person person)
            {
                Instance.Person = person;
                return this;
            }
            public virtual Builder WithOwnerId(Guid ownerId)
            {
                Instance.OwnerId = ownerId;
                return this;
            }
            public virtual Builder WithOccupationId(int? occupationId)
            {
                Instance.OccupationId = occupationId;
                return this;
            }

            public Builder WithOccupation(Occupation occupation)
            {
                Instance.Occupation = occupation;
                return this;
            }

            protected override void Specifications()
            {
                
            }
        }
    }
}
