﻿using System;
using Thex.Common;
using Tnf.Builder;
using Tnf.Notifications;
using Tnf.Specifications;

namespace Thex.Domain.Entities
{
    public partial class Level
    {
        public class Builder : Builder<Level>
        {

            public Builder(INotificationHandler handler) : base(handler)
            {
            }

            public Builder(INotificationHandler handler, Level instance) : base(handler, instance)
            {
            }

            public virtual Builder WithId(Guid id)
            {
                Instance.Id = id;
                return this;
            }

            public virtual Builder WithLevelCode(int levelCode)
            {
                Instance.LevelCode = levelCode;
                return this;
            }


            public virtual Builder WithPropertyId(int propertyId)
            {
                Instance.PropertyId = propertyId;
                return this;
            }

            public virtual Builder WithDescription(string description)
            {
                Instance.Description = description;
                return this;
            }

            public virtual Builder WithOrder(int order)
            {
                Instance.Order = order;
                return this;
            }
            public virtual Builder WithIsActive(bool isActive)
            {
                Instance.IsActive = isActive;
                return this;
            }
            public virtual Builder WithTenantId(Guid tenantId)
            {
                Instance.TenantId = tenantId;
                return this;
            }

            protected override void Specifications()
            {
                 
                AddSpecification(new ExpressionSpecification<Level>(
                    AppConsts.LocalizationSourceName,
                    Level.EntityError.LevelCodeRequired,
                    w => w.LevelCode != default(int)));

                AddSpecification(new ExpressionSpecification<Level>(
                    AppConsts.LocalizationSourceName,
                    Level.EntityError.DescriptionRequired,
                    w => !string.IsNullOrWhiteSpace(w.Description)));

                AddSpecification(new ExpressionSpecification<Level>(
                    AppConsts.LocalizationSourceName,
                    Level.EntityError.OrderRequired,
                    w => w.Order >= 1 ));

                AddSpecification(new ExpressionSpecification<Level>(
                  AppConsts.LocalizationSourceName,
                  Level.EntityError.DescriptionItemOutOfBound,
                  w => w.Description.Length > 0 && w.Description.Length <= 60));

            }
        }
    }
}
