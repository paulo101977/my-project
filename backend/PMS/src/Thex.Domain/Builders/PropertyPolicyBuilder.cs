﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a Crudzilla tool.
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using Thex.Common;
using Tnf.Builder;
using Tnf.Notifications;
using Tnf.Specifications;



namespace Thex.Domain.Entities
{
    public partial class PropertyPolicy
    {
    public class Builder : Builder<PropertyPolicy>
    {
		public Builder(INotificationHandler handler) : base(handler)
		{
		}

		public Builder(INotificationHandler handler, PropertyPolicy instance) : base(handler, instance)
		{
		}
		
        public virtual Builder WithId(Guid id)
        {
            Instance.Id = id;
            return this;
        }
        public virtual Builder WithPropertyId(int propertyId)
        {
            Instance.PropertyId = propertyId;
            return this;
        }
            public virtual Builder WithCountryId(int? countryId)
            {
                Instance.CountryId = countryId;
                return this;
            }
            public virtual Builder WithPolicyTypeId(int policyTypeId)
        {
            Instance.PolicyTypeId = policyTypeId;
            return this;
        }
        public virtual Builder WithDescription(string description)
        {
            Instance.Description = description;
            return this;
        }
        public virtual Builder WithIsActive(bool? isActive)
        {
            Instance.IsActive = isActive;
            return this;
        }

        protected override void Specifications()
        {
            AddSpecification(new ExpressionSpecification<PropertyPolicy>(
				AppConsts.LocalizationSourceName, 
				PropertyPolicy.EntityError.PropertyPolicyMustHavePropertyId, 
				w => w.PropertyId != default(int)));

            AddSpecification(new ExpressionSpecification<PropertyPolicy>(
				AppConsts.LocalizationSourceName, 
				PropertyPolicy.EntityError.PropertyPolicyMustHavePolicyTypeId, 
				w => w.PolicyTypeId != default(int)));

            AddSpecification(new ExpressionSpecification<PropertyPolicy>(
				AppConsts.LocalizationSourceName, 
				PropertyPolicy.EntityError.PropertyPolicyOutOfBoundDescription, 
				w => string.IsNullOrWhiteSpace(w.Description) || w.Description.Length > 0 && w.Description.Length <= 4000));

        }
    }
}
}

