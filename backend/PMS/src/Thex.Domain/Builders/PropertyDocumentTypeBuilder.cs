﻿using System;
using Tnf.Builder;
using Tnf.Notifications;

namespace Thex.Domain.Entities
{
    public partial class PropertyDocumentType
    {
        public class Builder : Builder<PropertyDocumentType>
        {

            public Builder(INotificationHandler handler) : base(handler)
            {
            }

            public Builder(INotificationHandler handler, PropertyDocumentType instance) : base(handler, instance)
            {
            }

            public virtual Builder WithId(Guid id)
            {
                Instance.Id = id;
                return this;
            }

            public virtual Builder WithPropertyId(int propertyId)
            {
                Instance.PropertyId = propertyId;
                return this;
            }


            public virtual Builder WithDocumentTypeId(int documentTypeId)
            {
                Instance.DocumentTypeId = documentTypeId;
                return this;
            } 

            protected override void Specifications()
            {
                
            }
        }
    }
}
