﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a Crudzilla tool.
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using Thex.Common;
using Tnf.Builder;
using Tnf.Notifications;
using Tnf.Specifications;



namespace Thex.Domain.Entities
{
    public partial class ReasonCategory
    {
    public class Builder : Builder<ReasonCategory>
    {
		public Builder(INotificationHandler handler) : base(handler)
		{
		}

		public Builder(INotificationHandler handler, ReasonCategory instance) : base(handler, instance)
		{
		}
		
        public virtual Builder WithId(int id)
        {
            Instance.Id = id;
            return this;
        }
        public virtual Builder WithReasonType(string reasonType)
        {
            Instance.ReasonType = reasonType;
            return this;
        }

        protected override void Specifications()
        {
            AddSpecification(new ExpressionSpecification<ReasonCategory>(
				AppConsts.LocalizationSourceName, 
				ReasonCategory.EntityError.ReasonCategoryMustHaveReasonType, 
				w => !string.IsNullOrWhiteSpace(w.ReasonType)));

            AddSpecification(new ExpressionSpecification<ReasonCategory>(
				AppConsts.LocalizationSourceName, 
				ReasonCategory.EntityError.ReasonCategoryOutOfBoundReasonType, 
				w => string.IsNullOrWhiteSpace(w.ReasonType) || w.ReasonType.Length > 0 && w.ReasonType.Length <= 100));

        }
    }
}
}
