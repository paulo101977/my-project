﻿using System;
using System.Collections.Generic;
using System.Text;
using Thex.Kernel;
using Tnf.Builder;
using Tnf.Notifications;

namespace Thex.Domain.Entities
{
    public partial class ReservationItemLaunch
    {
        public class Builder : Builder<ReservationItemLaunch>
        {
            public Builder(INotificationHandler handler) : base(handler)
            {
            }

            public Builder(INotificationHandler handler, ReservationItemLaunch instance) : base(handler, instance)
            {
            }

            public virtual Builder WithId(Guid id)
            {
                Instance.Id = id;
                return this;
            }

            public virtual Builder WithQuantityOfTourismTaxLaunched(int? quantityOfTourismTaxLaunched)
            {
                Instance.QuantityOfTourismTaxLaunched = quantityOfTourismTaxLaunched;
                return this;
            }

            public virtual Builder WithReservationItemId(long reservationItemId)
            {
                Instance.ReservationItemId = reservationItemId;
                return this;
            }

            public virtual Builder WithGuestReservationItemId(long? guestReservationItemId)
            {
                Instance.GuestReservationItemId = guestReservationItemId;
                return this;
            }

            public virtual Builder SetTenant(IApplicationUser applicationUser)
            {
                Instance.SetTenant(applicationUser);
                return this;
            }
        }
    }
}
