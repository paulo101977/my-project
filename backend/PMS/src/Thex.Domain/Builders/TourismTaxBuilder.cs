﻿using System;
using Thex.Common;
using Thex.Common.Enumerations;
using Tnf.Builder;
using Tnf.Notifications;
using Tnf.Specifications;

namespace Thex.Domain.Entities
{
    public partial class TourismTax
    {
        public class Builder : Builder<TourismTax>
        {
            public Builder(INotificationHandler handler) : base(handler)
            {
            }

            public Builder(INotificationHandler handler, TourismTax instance) : base(handler, instance)
            {
            }


            public virtual Builder WithId(Guid id)
            {
                Instance.Id = id;
                return this;
            }
            public virtual Builder WithPropertyId(int propertyId)
            {
                Instance.PropertyId = propertyId;
                return this;
            }
            public virtual Builder WithIsActive(bool isActive)
            {
                Instance.IsActive = isActive;
                return this;
            }
            public virtual Builder WithBillingItemId(int billingItemId)
            {
                Instance.BillingItemId = billingItemId;
                return this;
            }
            public virtual Builder WithAmount(decimal amount)
            {
                Instance.Amount = amount;
                return this;
            }
            public virtual Builder WithNumberOfNights(int numberOfNights)
            {
                Instance.NumberOfNights = numberOfNights;
                return this;
            }
            public virtual Builder WithIsTotalOfNights(bool isTotalOfNights)
            {
                Instance.IsTotalOfNights = isTotalOfNights;
                return this;
            }
            public virtual Builder WithIsIntegratedFiscalDocument(bool isIntegratedFiscalDocument)
            {
                Instance.IsIntegratedFiscalDocument = isIntegratedFiscalDocument;
                return this;
            }
            public virtual Builder WithLaunchType(int? launchType)
            {
                Instance.LaunchType = (TourismTaxLaunchTypeEnum?)launchType;
                return this;
            }

            protected override void Specifications()
            {
                AddSpecification(new ExpressionSpecification<TourismTax>(
                    AppConsts.LocalizationSourceName,
                    TourismTax.EntityError.TourismTaxMustHavePropertyId,
                    w => w.PropertyId != default(int)));

                AddSpecification(new ExpressionSpecification<TourismTax>(
                    AppConsts.LocalizationSourceName,
                    TourismTax.EntityError.TourismTaxMustHaveBillingItemId,
                    w => w.BillingItemId != default(int)));

                AddSpecification(new ExpressionSpecification<TourismTax>(
                    AppConsts.LocalizationSourceName,
                    TourismTax.EntityError.TourismTaxMustHaveLaunchType,
                    w => w.LaunchType != default(int)));
            }

        }
    }
}
