﻿using System;
using System.Collections.Generic;
using System.Text;
using Thex.Common;
using Tnf.Builder;
using Tnf.Notifications;
using Tnf.Specifications;

namespace Thex.Domain.Entities
{
    public partial class LevelRate
    {
        public class Builder: Builder<LevelRate>
        {
            public Builder(INotificationHandler notificationHandler) : base(notificationHandler)
            {

            }

            public Builder(INotificationHandler notificationHandler, LevelRate instance) : base(notificationHandler)
            {

            }

            public virtual Builder WithId(Guid id)
            {
                Instance.Id = id;
                return this;
            }
            public virtual Builder WithPropertyId(int propertyId)
            {
                Instance.PropertyId = propertyId;
                return this;
            }
            public virtual Builder WithMealPlanTypeDefault(int mealPlanTypeDefault)
            {
                Instance.MealPlanTypeDefault = mealPlanTypeDefault;
                return this;
            }
            public virtual Builder WithMealPlanTypeId(int mealPlanTypeId)
            {
                Instance.MealPlanTypeId = mealPlanTypeId;
                return this;
            }
            public virtual Builder WithRoomTypeId(int roomTypeId)
            {
                Instance.RoomTypeId = roomTypeId;
                return this;
            }
            public virtual Builder WithPax1Amount(decimal? pax1Amount)
            {
                Instance.Pax1Amount = pax1Amount;
                return this;
            }
            public virtual Builder WithPax2Amount(decimal? pax2Amount)
            {
                Instance.Pax2Amount = pax2Amount;
                return this;
            }
            public virtual Builder WithPax3Amount(decimal? pax3Amount)
            {
                Instance.Pax3Amount = pax3Amount;
                return this;
            }
            public virtual Builder WithPax4Amount(decimal? pax4Amount)
            {
                Instance.Pax4Amount = pax4Amount;
                return this;
            }
            public virtual Builder WithPax5Amount(decimal? pax5Amount)
            {
                Instance.Pax5Amount = pax5Amount;
                return this;
            }
            public virtual Builder WithPax6Amount(decimal? pax6Amount)
            {
                Instance.Pax6Amount = pax6Amount;
                return this;
            }
            public virtual Builder WithPax7Amount(decimal? pax7Amount)
            {
                Instance.Pax7Amount = pax7Amount;
                return this;
            }
            public virtual Builder WithPax8Amount(decimal? pax8Amount)
            {
                Instance.Pax8Amount = pax8Amount;
                return this;
            }
            public virtual Builder WithPax9Amount(decimal? pax9Amount)
            {
                Instance.Pax9Amount = pax9Amount;
                return this;
            }
            public virtual Builder WithPax10Amount(decimal? pax10Amount)
            {
                Instance.Pax10Amount = pax10Amount;
                return this;
            }
            public virtual Builder WithPax11Amount(decimal? pax11Amount)
            {
                Instance.Pax11Amount = pax11Amount;
                return this;
            }
            public virtual Builder WithPax12Amount(decimal? pax12Amount)
            {
                Instance.Pax12Amount = pax12Amount;
                return this;
            }
            public virtual Builder WithPax13Amount(decimal? pax13Amount)
            {
                Instance.Pax13Amount = pax13Amount;
                return this;
            }
            public virtual Builder WithPax14Amount(decimal? pax14Amount)
            {
                Instance.Pax14Amount = pax14Amount;
                return this;
            }
            public virtual Builder WithPax15Amount(decimal? pax15Amount)
            {
                Instance.Pax15Amount = pax15Amount;
                return this;
            }
            public virtual Builder WithPax16Amount(decimal? pax16Amount)
            {
                Instance.Pax16Amount = pax16Amount;
                return this;
            }
            public virtual Builder WithPax17Amount(decimal? pax17Amount)
            {
                Instance.Pax17Amount = pax17Amount;
                return this;
            }
            public virtual Builder WithPax18Amount(decimal? pax18Amount)
            {
                Instance.Pax18Amount = pax18Amount;
                return this;
            }
            public virtual Builder WithPax19Amount(decimal? pax19Amount)
            {
                Instance.Pax19Amount = pax19Amount;
                return this;
            }
            public virtual Builder WithPax20Amount(decimal? pax20Amount)
            {
                Instance.Pax20Amount = pax20Amount;
                return this;
            }
            public virtual Builder WithPaxAdditionalAmount(decimal? paxAdditionalAmount)
            {
                Instance.PaxAdditionalAmount = paxAdditionalAmount;
                return this;
            }
            public virtual Builder WithChild1Amount(decimal? child1Amount)
            {
                Instance.Child1Amount = child1Amount;
                return this;
            }
            public virtual Builder WithChild2Amount(decimal? child2Amount)
            {
                Instance.Child2Amount = child2Amount;
                return this;
            }
            public virtual Builder WithChild3Amount(decimal? child3Amount)
            {
                Instance.Child3Amount = child3Amount;
                return this;
            }
            public virtual Builder WithAdultMealPlanAmount(decimal? adultMealPlanAmount)
            {
                Instance.AdultMealPlanAmount = adultMealPlanAmount;
                return this;
            }
            public virtual Builder WithChild1MealPlanAmount(decimal? child1MealPlanAmount)
            {
                Instance.Child1MealPlanAmount = child1MealPlanAmount;
                return this;
            }
            public virtual Builder WithChild2MealPlanAmount(decimal? child2MealPlanAmount)
            {
                Instance.Child2MealPlanAmount = child2MealPlanAmount;
                return this;
            }
            public virtual Builder WithChild3MealPlanAmount(decimal? child3MealPlanAmount)
            {
                Instance.Child3MealPlanAmount = child3MealPlanAmount;
                return this;
            }
            public virtual Builder WithCurrencyId(Guid currencyId)
            {
                Instance.CurrencyId = currencyId;
                return this;
            }
            public virtual Builder WithCurrencySymbol(string currencySymbol)
            {
                Instance.CurrencySymbol = currencySymbol;
                return this;
            }
            public virtual Builder WithLevelRateHeaderId(Guid? levelRateHeaderId)
            {
                Instance.LevelRateHeaderId = levelRateHeaderId;
                return this;
            }
            public virtual Builder WithTenantId(Guid tenantId)
            {
                Instance.TenantId = tenantId;
                return this;
            }

            internal void EntitySpecifications()
            {
                AddSpecification(new ExpressionSpecification<LevelRate>(
                    AppConsts.LocalizationSourceName,
                    LevelRate.EntityError.LevelRateOutOfBoundCurrencySymbol,
                    w => string.IsNullOrWhiteSpace(w.CurrencySymbol) || w.CurrencySymbol.Length > 0 && w.CurrencySymbol.Length <= 10));

            }
        }
    }
}
