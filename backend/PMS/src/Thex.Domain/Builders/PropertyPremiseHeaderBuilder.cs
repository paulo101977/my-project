﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a Crudzilla tool.
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using Thex.Common;
using Tnf.Builder;
using Tnf.Notifications;
using Tnf.Specifications;

namespace Thex.Domain.Entities
{
    public partial class PropertyPremiseHeader
    {
        public class Builder : Builder<PropertyPremiseHeader>
        {
            public Builder(INotificationHandler handler) : base(handler)
            {
            }

            public Builder(INotificationHandler handler, PropertyPremiseHeader instance) : base(handler, instance)
            {
            }

            public virtual Builder WithId(Guid id)
            {
                Instance.Id = id;
                return this;
            }
            public virtual Builder WithPropertyId(int propertyId)
            {
                Instance.PropertyId = propertyId;
                return this;
            }
            public virtual Builder WithInitialDate(DateTime initialDate)
            {
                Instance.InitialDate = initialDate;
                return this;
            }
            public virtual Builder WithFinalDate(DateTime finalDate)
            {
                Instance.FinalDate = finalDate;
                return this;
            }
            public virtual Builder WithCurrencyId(Guid currencyId)
            {
                Instance.CurrencyId = currencyId;
                return this;
            }
            public virtual Builder WithTenantId(Guid tenantId)
            {
                Instance.TenantId = tenantId;
                return this;
            }
            public virtual Builder WithIsActive(bool isActive)
            {
                Instance.IsActive = isActive;
                return this;
            }
            public virtual Builder WithName(string name)
            {
                Instance.Name = name;
                return this;
            }
            public virtual Builder WithPaxReference(int paxReference)
            {
                Instance.PaxReference = paxReference;
                return this;
            }
            public virtual Builder WithRoomTypeReferenceId(int roomTypeReferenceId)
            {
                Instance.RoomTypeReferenceId = roomTypeReferenceId;
                return this;
            }

            public virtual Builder WithPropertyPremiseList(List<PropertyPremise> propertyPremiseList)
            {
                Instance.PropertyPremiseList = propertyPremiseList;
                return this;
            }

            internal void EntitySpecifications()
            {
                AddSpecification(new ExpressionSpecification<PropertyPremiseHeader>(
                    AppConsts.LocalizationSourceName,
                    PropertyPremiseHeader.EntityError.PropertyPremiseHeaderInvalidInitialDate,
                    w => w.InitialDate >= new DateTime(1753, 1, 1) && w.InitialDate <= DateTime.MaxValue));

                AddSpecification(new ExpressionSpecification<PropertyPremiseHeader>(
                    AppConsts.LocalizationSourceName,
                    PropertyPremiseHeader.EntityError.PropertyPremiseHeaderInvalidFinalDate,
                    w => w.FinalDate >= new DateTime(1753, 1, 1) && w.FinalDate <= DateTime.MaxValue));

                AddSpecification(new ExpressionSpecification<PropertyPremiseHeader>(
                    AppConsts.LocalizationSourceName,
                    PropertyPremiseHeader.EntityError.PropertyPremiseHeaderMustHaveName,
                    w => !string.IsNullOrWhiteSpace(w.Name)));

                AddSpecification(new ExpressionSpecification<PropertyPremiseHeader>(
                    AppConsts.LocalizationSourceName,
                    PropertyPremiseHeader.EntityError.PropertyPremiseHeaderOutOfBoundName,
                    w => string.IsNullOrWhiteSpace(w.Name) || w.Name.Length > 0 && w.Name.Length <= 200));

            }
        }
    }
}
