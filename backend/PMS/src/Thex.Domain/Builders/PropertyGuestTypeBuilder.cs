﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a Crudzilla tool.
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using Thex.Common;
using Tnf.Builder;
using Tnf.Notifications;
using Tnf.Specifications;



namespace Thex.Domain.Entities
{
    public partial class PropertyGuestType
    {
        public class Builder : Builder<PropertyGuestType>
        {
            public Builder(INotificationHandler handler) : base(handler)
            {
            }

            public Builder(INotificationHandler handler, PropertyGuestType instance) : base(handler, instance)
            {
            }

            public virtual Builder WithId(int id)
            {
                Instance.Id = id;
                return this;
            }
            public virtual Builder WithPropertyId(int propertyId)
            {
                Instance.PropertyId = propertyId;
                return this;
            }
            public virtual Builder WithGuestTypeName(string guestTypeName)
            {
                Instance.GuestTypeName = guestTypeName;
                return this;
            }
            public virtual Builder WithIsVip(bool isVip)
            {
                Instance.IsVip = isVip;
                return this;
            }

            public virtual Builder WithCode(string code)
            {
                Instance.Code = code;
                return this;
            }
            public virtual Builder WithIsIncognito(bool? isIncognito)
            {
                Instance.IsIncognito = isIncognito;
                return this;
            }
            public virtual Builder WithIsActive(bool? isActive)
            {
                Instance.IsActive = isActive;
                return this;
            }
            public virtual Builder WithIsExemptOfTourismTax(bool? isExemptOfTourismTax)
            {
                Instance.IsExemptOfTourismTax = isExemptOfTourismTax;
                return this;
            }

            protected override void Specifications()
            {
                AddSpecification(new ExpressionSpecification<PropertyGuestType>(
                    AppConsts.LocalizationSourceName,
                    PropertyGuestType.EntityError.PropertyGuestTypeMustHavePropertyId,
                    w => w.PropertyId != default(int)));

                AddSpecification(new ExpressionSpecification<PropertyGuestType>(
                    AppConsts.LocalizationSourceName,
                    PropertyGuestType.EntityError.PropertyGuestTypeMustHaveGuestTypeName,
                    w => !string.IsNullOrWhiteSpace(w.GuestTypeName)));

                AddSpecification(new ExpressionSpecification<PropertyGuestType>(
                    AppConsts.LocalizationSourceName,
                    PropertyGuestType.EntityError.PropertyGuestTypeOutOfBoundGuestTypeName,
                    w => string.IsNullOrWhiteSpace(w.GuestTypeName) || w.GuestTypeName.Length > 0 && w.GuestTypeName.Length <= 100));

            }
        }
    }
}
