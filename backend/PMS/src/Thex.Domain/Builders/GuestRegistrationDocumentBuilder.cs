﻿using System;
using System.Collections.Generic;
using System.Text;
using Thex.Common;
using Thex.Domain.Factories;
using Tnf.Builder;
using Tnf.Notifications;
using Tnf.Specifications;

namespace Thex.Domain.Entities
{
    public partial class GuestRegistrationDocument
    {
        public class Builder : Builder<GuestRegistrationDocument>
        {

            public Builder(INotificationHandler handler) : base(handler)
            {
            }

            public Builder(INotificationHandler handler, GuestRegistrationDocument instance) : base(handler, instance)
            {
                Instance = instance;
            }

            public virtual Builder WithId(Guid id)
            {
                Instance.Id = id;
                return this;
            }
            public virtual Builder WithDocumentTypeId(int documentTypeId)
            {
                Instance.DocumentTypeId = documentTypeId;
                return this;
            }
            public virtual Builder WithDocumentInformation(string documentInformation)
            {
                Instance.DocumentInformation = documentInformation;
                return this;
            }
            public virtual Builder WithGuestRegistrationId(Guid guestRegistrationId)
            {
                Instance.GuestRegistrationId = guestRegistrationId;
                return this;
            }

            protected override void Specifications()
            {
                AddSpecification(new ExpressionSpecification<GuestRegistrationDocument>(
                   AppConsts.LocalizationSourceName,
                   GuestRegistrationDocument.EntityError.GuestRegistrationDocumentMustHaveDocumentTypeId,
                   w => w.DocumentTypeId != default(int)));

                AddSpecification(new ExpressionSpecification<GuestRegistrationDocument>(
                    AppConsts.LocalizationSourceName,
                    GuestRegistrationDocument.EntityError.GuestRegistrationDocumentMustHaveDocumentInformation,
                    w => !string.IsNullOrWhiteSpace(w.DocumentInformation)));

                AddSpecification(new ExpressionSpecification<GuestRegistrationDocument>(
                    AppConsts.LocalizationSourceName,
                    GuestRegistrationDocument.EntityError.GuestRegistrationDocumentOutOfBoundDocumentInformation,
                    w => string.IsNullOrWhiteSpace(w.DocumentInformation) || w.DocumentInformation.Length > 0 && w.DocumentInformation.Length <= 20));

                AddSpecification(new ExpressionSpecification<GuestRegistrationDocument>(
                  AppConsts.LocalizationSourceName,
                  GuestRegistrationDocument.EntityError.GuestRegistrationDocumentMustHaveGuestRegistrationId,
                  w => w.GuestRegistrationId != Guid.Empty));

                if (!string.IsNullOrWhiteSpace(Instance.DocumentInformation))
                {
                    var documentExpression = new DocumentsValidatorFactory<GuestRegistrationDocument>();
                    AddSpecification(documentExpression.ValidateDocument(AppConsts.LocalizationSourceName, Instance.DocumentTypeId, Instance.DocumentInformation));

                }
            }
        }
    }
}
