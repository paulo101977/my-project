﻿using System;
using Tnf.Builder;
using Tnf.Notifications;

namespace Thex.Domain.Entities
{
    public partial class RoomOccupied
    {
        public class Builder : Builder<RoomOccupied>
        {

            public Builder(INotificationHandler handler) : base(handler)
            {
            }

            public Builder(INotificationHandler handler, RoomOccupied instance) : base(handler, instance)
            {
            }

            public virtual Builder WithId(Guid id)
            {
                Instance.Id = id;
                return this;
            }

            public virtual Builder WithRoomId(int roomId)
            {
                Instance.RoomId = roomId;
                return this;
            }


            public virtual Builder WithPropertyId(int propertyId)
            {
                Instance.PropertyId = propertyId;
                return this;
            }

            public virtual Builder WithReservationId(long reservationId)
            {
                Instance.ReservationId = reservationId;
                return this;
            }


            public virtual Builder WithTenantId(Guid tenantId)
            {
                Instance.TenantId = tenantId;
                return this;
            }

            protected override void Specifications()
            {
                
            }
        }
    }
}
