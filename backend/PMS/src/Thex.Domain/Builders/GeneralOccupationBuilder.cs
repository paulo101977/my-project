﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a Crudzilla tool.
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using Thex.Common;
using Tnf.Builder;
using Tnf.Notifications;
using Tnf.Specifications;



namespace Thex.Domain.Entities
{
    public partial class GeneralOccupation
    {
    public class Builder : Builder<GeneralOccupation>
    {
		public Builder(INotificationHandler handler) : base(handler)
		{
		}

		public Builder(INotificationHandler handler, GeneralOccupation instance) : base(handler, instance)
		{
		}
		
        public virtual Builder WithId(int id)
        {
            Instance.Id = id;
            return this;
        }
        public virtual Builder WithOccupation(string occupation)
        {
            Instance.Occupation = occupation;
            return this;
        }

        protected override void Specifications()
        {
            AddSpecification(new ExpressionSpecification<GeneralOccupation>(
				AppConsts.LocalizationSourceName, 
				GeneralOccupation.EntityError.GeneralOccupationMustHaveOccupation, 
				w => !string.IsNullOrWhiteSpace(w.Occupation)));

            AddSpecification(new ExpressionSpecification<GeneralOccupation>(
				AppConsts.LocalizationSourceName, 
				GeneralOccupation.EntityError.GeneralOccupationOutOfBoundOccupation, 
				w => string.IsNullOrWhiteSpace(w.Occupation) || w.Occupation.Length > 0 && w.Occupation.Length <= 100));

        }
    }
}
}
