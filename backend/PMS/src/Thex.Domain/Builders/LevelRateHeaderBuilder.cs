﻿using System;
using Thex.Common;
using Tnf.Builder;
using Tnf.Notifications;
using Tnf.Specifications;

namespace Thex.Domain.Entities
{
    public partial class LevelRateHeader
    {
        public class Builder : Builder<LevelRateHeader>
        {
            public Builder(INotificationHandler notificationHandler) : base(notificationHandler)
            {

            }

            public Builder(INotificationHandler notificationHandler, LevelRateHeader instance) : base(notificationHandler)
            {

            }

            public virtual Builder WithId(Guid id)
            {
                Instance.Id = id;
                return this;
            }
            public virtual Builder WithRateName(string rateName)
            {
                Instance.RateName = rateName;
                return this;
            }
            public virtual Builder WithInitialDate(DateTime initialDate)
            {
                Instance.InitialDate = initialDate;
                return this;
            }
            public virtual Builder WithEndDate(DateTime endDate)
            {
                Instance.EndDate = endDate;
                return this;
            }
            public virtual Builder WithLevelId(Guid levelId)
            {
                Instance.LevelId = levelId;
                return this;
            }
            public virtual Builder WithPropertyMealPlanTypeRate(bool? propertyMealPlanTypeRate)
            {
                Instance.PropertyMealPlanTypeRate = propertyMealPlanTypeRate;
                return this;
            }
            public virtual Builder WithTenantId(Guid tenantId)
            {
                Instance.TenantId = tenantId;
                return this;
            }
            public virtual Builder WithCurrencyId(Guid currencyId)
            {
                Instance.CurrencyId = currencyId;
                return this;
            }
            public virtual Builder WithIsActive(bool isActive)
            {
                Instance.IsActive = isActive;
                return this;
            }

            internal void EntitySpecifications()
            {
                AddSpecification(new ExpressionSpecification<LevelRateHeader>(
                    AppConsts.LocalizationSourceName,
                    LevelRateHeader.EntityError.LevelRateHeaderMustHaveRateName,
                    w => !string.IsNullOrWhiteSpace(w.RateName)));

                AddSpecification(new ExpressionSpecification<LevelRateHeader>(
                    AppConsts.LocalizationSourceName,
                    LevelRateHeader.EntityError.LevelRateHeaderOutOfBoundRateName,
                    w => string.IsNullOrWhiteSpace(w.RateName) || w.RateName.Length > 0 && w.RateName.Length <= 100));

                AddSpecification(new ExpressionSpecification<LevelRateHeader>(
                    AppConsts.LocalizationSourceName,
                    LevelRateHeader.EntityError.LevelRateHeaderInvalidInitialDate,
                    w => w.InitialDate >= new DateTime(1753, 1, 1) && w.InitialDate <= DateTime.MaxValue));

                AddSpecification(new ExpressionSpecification<LevelRateHeader>(
                    AppConsts.LocalizationSourceName,
                    LevelRateHeader.EntityError.LevelRateHeaderInvalidEndDate,
                    w => w.EndDate >= new DateTime(1753, 1, 1) && w.EndDate <= DateTime.MaxValue));

            }
        }
    }
}
