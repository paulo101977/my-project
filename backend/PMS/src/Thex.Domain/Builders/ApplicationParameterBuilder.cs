﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a Crudzilla tool.
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using Thex.Common;
using Tnf.Builder;
using Tnf.Notifications;
using Tnf.Specifications;



namespace Thex.Domain.Entities
{
    public partial class ApplicationParameter
    {
    public class Builder : Builder<ApplicationParameter>
    {
		public Builder(INotificationHandler handler) : base(handler)
		{
		}

		public Builder(INotificationHandler handler, ApplicationParameter instance) : base(handler, instance)
		{
		}
		
        public virtual Builder WithId(int id)
        {
            Instance.Id = id;
            return this;
        }
        public virtual Builder WithApplicationModuleId(int applicationModuleId)
        {
            Instance.ApplicationModuleId = applicationModuleId;
            return this;
        }
        public virtual Builder WithFeatureGroupId(int featureGroupId)
        {
            Instance.FeatureGroupId = featureGroupId;
            return this;
        }
        public virtual Builder WithParameterName(string parameterName)
        {
            Instance.ParameterName = parameterName;
            return this;
        }
        public virtual Builder WithParameterDescription(string parameterDescription)
        {
            Instance.ParameterDescription = parameterDescription;
            return this;
        }
        public virtual Builder WithParameterDefaultValue(string parameterDefaultValue)
        {
            Instance.ParameterDefaultValue = parameterDefaultValue;
            return this;
        }
        public virtual Builder WithParameterTypeId(int parameterTypeId)
        {
            Instance.ParameterTypeId = parameterTypeId;
            return this;
        }
        public virtual Builder WithParameterComponentType(string parameterComponentType)
        {
            Instance.ParameterComponentType = parameterComponentType;
            return this;
        }
        public virtual Builder WithParameterMinValue(string parameterMinValue)
        {
            Instance.ParameterMinValue = parameterMinValue;
            return this;
        }
        public virtual Builder WithParameterMaxValue(string parameterMaxValue)
        {
            Instance.ParameterMaxValue = parameterMaxValue;
            return this;
        }
        public virtual Builder WithParameterPossibleValues(string parameterPossibleValues)
        {
            Instance.ParameterPossibleValues = parameterPossibleValues;
            return this;
        }
        public virtual Builder WithIsNullable(bool isNullable)
        {
            Instance.IsNullable = isNullable;
            return this;
        }

        protected override void Specifications()
        {
            AddSpecification(new ExpressionSpecification<ApplicationParameter>(
				AppConsts.LocalizationSourceName, 
				ApplicationParameter.EntityError.ApplicationParameterMustHaveApplicationModuleId, 
				w => w.ApplicationModuleId != default(int)));

            AddSpecification(new ExpressionSpecification<ApplicationParameter>(
				AppConsts.LocalizationSourceName, 
				ApplicationParameter.EntityError.ApplicationParameterMustHaveFeatureGroupId, 
				w => w.FeatureGroupId != default(int)));

            AddSpecification(new ExpressionSpecification<ApplicationParameter>(
				AppConsts.LocalizationSourceName, 
				ApplicationParameter.EntityError.ApplicationParameterMustHaveParameterName, 
				w => !string.IsNullOrWhiteSpace(w.ParameterName)));

            AddSpecification(new ExpressionSpecification<ApplicationParameter>(
				AppConsts.LocalizationSourceName, 
				ApplicationParameter.EntityError.ApplicationParameterOutOfBoundParameterName, 
				w => string.IsNullOrWhiteSpace(w.ParameterName) || w.ParameterName.Length > 0 && w.ParameterName.Length <= 100));

            AddSpecification(new ExpressionSpecification<ApplicationParameter>(
				AppConsts.LocalizationSourceName, 
				ApplicationParameter.EntityError.ApplicationParameterOutOfBoundParameterDescription, 
				w => string.IsNullOrWhiteSpace(w.ParameterDescription) || w.ParameterDescription.Length > 0 && w.ParameterDescription.Length <= 500));

            AddSpecification(new ExpressionSpecification<ApplicationParameter>(
				AppConsts.LocalizationSourceName, 
				ApplicationParameter.EntityError.ApplicationParameterMustHaveParameterTypeId, 
				w => w.ParameterTypeId != default(int)));

            AddSpecification(new ExpressionSpecification<ApplicationParameter>(
				AppConsts.LocalizationSourceName, 
				ApplicationParameter.EntityError.ApplicationParameterOutOfBoundParameterComponentType, 
				w => string.IsNullOrWhiteSpace(w.ParameterComponentType) || w.ParameterComponentType.Length > 0 && w.ParameterComponentType.Length <= 100));

            AddSpecification(new ExpressionSpecification<ApplicationParameter>(
				AppConsts.LocalizationSourceName, 
				ApplicationParameter.EntityError.ApplicationParameterOutOfBoundParameterMinValue, 
				w => string.IsNullOrWhiteSpace(w.ParameterMinValue) || w.ParameterMinValue.Length > 0 && w.ParameterMinValue.Length <= 100));

            AddSpecification(new ExpressionSpecification<ApplicationParameter>(
				AppConsts.LocalizationSourceName, 
				ApplicationParameter.EntityError.ApplicationParameterOutOfBoundParameterMaxValue, 
				w => string.IsNullOrWhiteSpace(w.ParameterMaxValue) || w.ParameterMaxValue.Length > 0 && w.ParameterMaxValue.Length <= 100));

        }
    }
}
}
