﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a Crudzilla tool.
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using Thex.Common;
using Tnf.Builder;
using Tnf.Notifications;
using Tnf.Specifications;



namespace Thex.Domain.Entities
{
    public partial class BillingItem
    {
    public class Builder : Builder<BillingItem>
    {
		public Builder(INotificationHandler handler) : base(handler)
		{
		}

		public Builder(INotificationHandler handler, BillingItem instance) : base(handler, instance)
		{
		}
		
        public virtual Builder WithId(int id)
        {
            Instance.Id = id;
            return this;
        }
        public virtual Builder WithBillingItemTypeId(int billingItemTypeId)
        {
            Instance.BillingItemTypeId = billingItemTypeId;
            return this;
        }
        public virtual Builder WithBillingItemName(string billingItemName)
        {
            Instance.BillingItemName = billingItemName;
            return this;
        }
        public virtual Builder WithBillingItemCategoryId(int? billingItemCategoryId)
        {
            Instance.BillingItemCategoryId = billingItemCategoryId;
            return this;
        }
        public virtual Builder WithDescription(string description)
        {
            Instance.Description = description;
            return this;
        }
        public virtual Builder WithPropertyId(int? propertyId)
        {
            Instance.PropertyId = propertyId;
            return this;
        }
        public virtual Builder WithPaymentTypeId(int? paymentTypeId)
        {
            Instance.PaymentTypeId = paymentTypeId;
            return this;
        }
        public virtual Builder WithPlasticBrandPropertyId(Guid? plasticBrandPropertyId)
        {
            Instance.PlasticBrandPropertyId = plasticBrandPropertyId;
            return this;
        }
        public virtual Builder WithAcquirerId(Guid? acquirerId)
        {
            Instance.AcquirerId = acquirerId;
            return this;
        }
        public virtual Builder WithMaximumInstallmentsQuantity(short? maximumInstallmentsQuantity)
        {
            Instance.MaximumInstallmentsQuantity = maximumInstallmentsQuantity;
            return this;
        }
        public virtual Builder WithIntegrationCode(string integrationCode)
        {
            Instance.IntegrationCode = integrationCode;
            return this;
        }
        public virtual Builder WithIsActive(bool isActive)
        {
            Instance.IsActive = isActive;
            return this;
        }

        public Builder WithBillingItemPaymentConditionList(List<BillingItemPaymentCondition> billingItemPaymentConditionList)
        {
            Instance.BillingItemPaymentConditionList = billingItemPaymentConditionList;
            return this;
        }

        protected override void Specifications()
        {
            AddSpecification(new ExpressionSpecification<BillingItem>(
				AppConsts.LocalizationSourceName, 
				BillingItem.EntityError.BillingItemMustHaveBillingItemTypeId, 
				w => w.BillingItemTypeId != default(int)));

            //        AddSpecification(new ExpressionSpecification<BillingItem>(
            //AppConsts.LocalizationSourceName, 
            //BillingItem.EntityError.BillingItemMustHaveBillingItemName, 
            //w => !string.IsNullOrWhiteSpace(w.BillingItemName)));

            AddSpecification(new ExpressionSpecification<BillingItem>(
                AppConsts.LocalizationSourceName,
                BillingItem.EntityError.BillingItemOutOfBoundBillingItemName,
                w => string.IsNullOrWhiteSpace(w.BillingItemName) || w.BillingItemName.Length > 0 && w.BillingItemName.Length <= 100));

            AddSpecification(new ExpressionSpecification<BillingItem>(
				AppConsts.LocalizationSourceName, 
				BillingItem.EntityError.BillingItemOutOfBoundDescription, 
				w => string.IsNullOrWhiteSpace(w.Description) || w.Description.Length > 0 && w.Description.Length <= 200));

            AddSpecification(new ExpressionSpecification<BillingItem>(
				AppConsts.LocalizationSourceName, 
				BillingItem.EntityError.BillingItemOutOfBoundIntegrationCode, 
				w => string.IsNullOrWhiteSpace(w.IntegrationCode) || w.IntegrationCode.Length > 0 && w.IntegrationCode.Length <= 100));

        }

    }
}
}
