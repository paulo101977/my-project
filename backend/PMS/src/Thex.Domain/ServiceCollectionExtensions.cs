﻿using Microsoft.Extensions.DependencyInjection;
using Thex.Common;
using Thex.Domain.Factories;
using Thex.Domain.Interfaces.Factories;
using Thex.Domain.Interfaces.Repositories;
using Thex.Domain.Interfaces.Repositories.ReadDapper;
using Thex.Domain.Interfaces.Services;
using Thex.Domain.Services;
using Thex.Domain.Services.Interfaces;

namespace Thex.Domain
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddDomainDependency(this IServiceCollection services)
        {
            // Adiciona as dependencias para utilização dos serviços de crud generico do Tnf
            services
                .AddTnfDomain()
                .AddCommonDependency();

            // Para habilitar as convenções do Tnf para Injeção de dependência (ITransientDependency, IScopedDependency, ISingletonDependency)
             services.AddTnfDefaultConventionalRegistrations();

            // Registro dos serviços
            services.AddTransient<IReservationItemStatusFactory, ReservationItemStatusFactory>();
            services.AddTransient<IEmailFactory, EmailFactory>();

            services.AddTransient<IBillingAccountDomainService, BillingAccountDomainService>();
            services.AddTransient<IBillingAccountItemDomainService, BillingAccountItemDomainService>();
            services.AddTransient<IBusinessSourceDomainService, BusinessSourceDomainService>();
            services.AddTransient<ICompanyClientContactPersonDomainService, CompanyClientContactPersonDomainService>();
            services.AddTransient<ICompanyClientDomainService, CompanyClientDomainService>();
            services.AddTransient<IGuestReservationItemDomainService, GuestReservationItemDomainService>();
            services.AddTransient<IMarketSegmentDomainService, MarketSegmentDomainService>();
            services.AddTransient<IPropertyAuditProcessDomainService, PropertyAuditProcessDomainService>();
            services.AddTransient<IPropertyParameterDomainService, PropertyParameterDomainService>();
            services.AddTransient<IRatePlanDomainService, RatePlanDomainService>();
            services.AddTransient<IReservationBudgetDomainService, ReservationBudgetDomainService>();
            services.AddTransient<IReservationDomainService, ReservationDomainService>();
            services.AddTransient<IReservationItemDomainService, ReservationItemDomainService>();
            services.AddTransient<IReservationItemStatusDomainService, ReservationItemStatusDomainService>();
            services.AddTransient<IRoomDomainService, RoomDomainService>();
            services.AddTransient<IRoomLayoutDomainService, RoomLayoutDomainService>();
            services.AddTransient<IRoomTypeDomainService, RoomTypeDomainService>();
            services.AddTransient<IPropertyRateStrategyDomainService, PropertyRateStrategyDomainService>();
            services.AddTransient<ILevelDomainService,LevelDomainService>();
            services.AddTransient<IRoomOccupiedDomainService, RoomOccupiedDomainService>();
            services.AddTransient<IPropertyDocumentTypeDomainService, PropertyDocumentTypeDomainService>();
            services.AddTransient<ICheckinDomainService, CheckinDomainService>();
            services.AddTransient<IExternalRegistrationDomainService, ExternalRegistrationDomainService>();



            return services;
        }
    }
}
