﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.Common.Enumerations;
using Thex.Domain.Entities;
using Thex.Dto.Integration;

namespace Thex.Domain.Interfaces.Integration
{
    public interface IInvoiceIntegrationClient
    {
        Task<List<IntegrationClientDto>> GetBorrowerInformation(BillingAccount billingAccount, BillingInvoiceTypeEnum invoiceType);
        Task<List<IntegrationClientDto>> GetProformaBorrowerInformation(Guid companyClientId);
    }
}
