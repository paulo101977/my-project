﻿using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace Thex.Domain.Interfaces.Integration
{
    public interface IInvoiceIntegration
    {
        Task<HttpResponseMessage> PostRequest(string json, Uri taxRuleEndpoint);
    }
}
