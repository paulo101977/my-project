﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.Common.Enumerations;
using Thex.Dto;
using Thex.Dto.BillingAccountClosure;

namespace Thex.Domain.Interfaces.Integration
{
    public interface IInvoiceIntegrationEurope
    {
        Task<NotificationResponseDto> GenerateInvoice(List<BillingAccountClosureInvoiceDto> billingInvoiceList, 
                                                      BillingInvoiceTypeEnum invoiceType, 
                                                      BillingAccountItemCreditDto payment = null);

        Task<string> GetIntegratorIdById(Guid id);
    }
}
