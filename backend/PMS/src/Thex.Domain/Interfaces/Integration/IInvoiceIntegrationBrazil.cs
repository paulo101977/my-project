﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.Common.Dto.InvoiceIntegration;
using Thex.Common.Enumerations;
using Thex.Dto;
using Thex.Dto.BillingAccountClosure;
using Thex.Dto.Integration;

namespace Thex.Domain.Interfaces.Integration
{
    public interface IInvoiceIntegrationBrazil
    {
        Task<NotificationResponseDto> GenerateInvoice(List<BillingAccountClosureInvoiceDto> billingInvoiceList, BillingInvoiceTypeEnum invoiceType);
        Task<BillingInvoiceIntegrationResponseDto> UpdateWithSuccess(InvoiceIntegrationResponseDto dto);
        Task<BillingInvoiceIntegrationResponseDto> UpdateWithError(InvoiceIntegrationResponseDto dto);
        Task<BillingInvoiceIntegrationResponseDto> UpdateWithCanceled(InvoiceIntegrationResponseDto dto);
        Task<bool> CancelInvoice(Guid billingInvoiceId, int billingInvoiceTypeId);
        Task SendInvoiceIntegratioResponse(BillingInvoiceIntegrationResponseDto response, InvoiceIntegrationResponseDto invoiceResponse);
        Task<List<InvoiceDto>> GetAllInvoicesByFilters(SearchBillingInvoiceDto requestDto, int propertyId);
        Task<Dictionary<string, string>> GetIntegrationDetails(string propertyId);
    }
}
