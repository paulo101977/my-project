﻿// //  <copyright file="IReservationItemStatusFactory.cs" company="TOTVS">
// //  Copyright (c) TOTVS. All rights reserved.
// //  </copyright>
namespace Thex.Domain.Interfaces.Factories
{
    using System;
    using System.Collections.Generic;

    using Thex.Common.Enumerations;

    using Tnf.Domain.Services;
    using Tnf.Application.Services;

    public interface IReservationItemStatusFactory : IApplicationService
    {
        IList<ReservationStatus> GetAllowedStatus(ReservationStatus status);
    }
}