﻿using Thex.Common.Enumerations;
using Thex.Dto.Email;

namespace Thex.Domain.Interfaces.Factories
{
    public interface IEmailFactory
    {
        string GenerateEmail(EmailTemplateEnum emailTemplate, EmailBaseDto emailDto);
    }
}
