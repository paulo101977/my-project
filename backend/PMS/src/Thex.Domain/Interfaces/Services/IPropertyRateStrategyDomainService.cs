﻿using System;
using System.Collections.Generic;
using Thex.Domain.Entities;
using Thex.Dto;
using Tnf.Domain.Services;

namespace Thex.Domain.Interfaces.Services
{
    public interface IPropertyRateStrategyDomainService : IDomainService<PropertyRateStrategy>
    {
        bool PeriodIsInvalid(DateTime startDate, DateTime endDate);
        void ValidateDuplicateRoomTypeAndOccupationInDto(PropertyRateStrategyRoomTypeDto dto, ICollection<PropertyRateStrategyRoomTypeDto> dtoList);
    }
}
