﻿using Thex.Domain.Entities;
using Thex.Dto;
using Tnf.Domain.Services;

namespace Thex.Domain.Interfaces.Services
{
    public interface IRatePlanDomainService : IDomainService<RatePlan>
    {
        bool AlllowsCreatTax(RatePlan ratePlan);
        bool AlllowsCreatTax(RatePlanBillingAccountDto dto);
    }
}
