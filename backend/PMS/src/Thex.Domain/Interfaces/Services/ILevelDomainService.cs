﻿using System;
using System.Collections.Generic;
using System.Text;
using Thex.Domain.Entities;
using Tnf.Domain.Services;

namespace Thex.Domain.Interfaces.Services
{
    public interface ILevelDomainService : IDomainService<Level>
    {
        void DeleteLevel(Level.Builder levelBuilder);
        void Create(Level.Builder levelBuilder);
        void ToggleAndSaveIsActive(Guid id);
        void Update(Level.Builder levelBuilder);
        void UpdateRange(ICollection<Level> levelList);
        void Insert(Level document);
    }
}
