﻿// //  <copyright file="ICompanyClientContactPersonDomainService.cs" company="TOTVS">
// //  Copyright (c) TOTVS. All rights reserved.
// //  </copyright>
namespace Thex.Domain.Interfaces.Services
{
    using System;
    using System.Collections.Generic;

    using Thex.Domain.Entities;

    using Tnf.Domain.Services;

    public interface ICompanyClientContactPersonDomainService : IDomainService<CompanyClientContactPerson>
    {
        void UpdateRange(IList<CompanyClientContactPerson>  companyClientContactPersonList, Guid companyClientId);
    }
}