﻿using System.Collections.Generic;
using Thex.Domain.Entities;
using Tnf.Domain.Services;
using Thex.Dto;

namespace Thex.Domain.Interfaces.Services
{
    public interface IRoomLayoutDomainService : IDomainService<RoomLayout>
    {
        List<RoomLayout> GetAllLayoutsByRoomTypeId(DefaultIntRequestDto id);
    }
}