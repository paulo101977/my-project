﻿using System;
using System.Collections.Generic;
using System.Text;
using Thex.Domain.Entities;
using Tnf.Domain.Services;

namespace Thex.Domain.Interfaces.Services
{
    public interface IRoomOccupiedDomainService : IDomainService<RoomOccupied>
    {
        void Insert(RoomOccupied room);   
        void DeleteWithRoomId(int roomId);    
    }
}
