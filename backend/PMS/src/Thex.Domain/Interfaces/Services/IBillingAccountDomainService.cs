﻿using System;
using Thex.Domain.Entities;
using Tnf.Domain.Services;

namespace Thex.Domain.Services.Interfaces
{
    public interface IBillingAccountDomainService : IDomainService<BillingAccount>
    {
        decimal CalculateAverageSpendPerDay(decimal total, int periodOfStay);
        void Reopen(int propertyId, Guid id, int reasonId, bool allAccountsAreClosed);
        int CalculatePeriodOfStay(DateTime startDate, DateTime finalDate);
    }
}