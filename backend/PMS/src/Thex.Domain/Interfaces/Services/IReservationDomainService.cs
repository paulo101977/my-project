﻿//  <copyright file="IReservationDomainService.cs" company="TOTVS">
//  Copyright (c) TOTVS. All rights reserved.
//  </copyright>
namespace Thex.Domain.Services.Interfaces
{
    using System;
    using System.Collections.Generic;
    using Thex.Domain.Entities;
    using Thex.Dto;
    using Thex.Dto.Availability;
    using Thex.Dto.Reservation;
    using Tnf.Domain.Services;
    using Tnf.Dto;

    public interface IReservationDomainService : IDomainService<Reservation>
    {
        Reservation Update(Reservation entity, Reservation existingReservation, ReservationConfirmation reservationConfirmation, Plastic plastic, DateTime systemDate);
        void CancelReservation(long reservationId, int reasonId, string cancellationDescription);
        void ReactivateReservation(long reservationId);
        Reservation GetExistingReservation(long reservationId);
        bool CanModifyReservation(long reservationId);
        List<AvailabilityReservationColumnsDto> GetAvailabilityReservationColumnsDto(IListDto<RoomDto> allPropertyRooms, List<ReservationItem> reservationItems);
        void ConfirmReservation(long reservationId);
        void ValidateDuplicatesRoomsInReservationDto(ReservationDto reservationDto);
    }
}