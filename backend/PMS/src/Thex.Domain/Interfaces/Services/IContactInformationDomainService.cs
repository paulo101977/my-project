﻿using System;
using Thex.Domain.Entities;
using Tnf.Domain.Services;

namespace Thex.Domain.Interfaces.Services
{
    public interface IContactInformationDomainService : IDomainService<ContactInformation>
    {
        void ResetContactInformationsProperty(Guid guid);
    }
}
