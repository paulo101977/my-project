﻿//  <copyright file="IRoomTypeDomainService.cs" company="TOTVS">
//  Copyright (c) TOTVS. All rights reserved.
//  </copyright>

using Thex.Dto;

namespace Thex.Domain.Services.Interfaces
{
    using Thex.Domain.Entities;
    using System;
    using System.Collections.Generic;
    using Tnf.Domain.Services;
    using Thex.Dto.GuestReservationItem;
    using System.Linq;

    public interface IGuestReservationItemDomainService : IDomainService<GuestReservationItem>
    {
        bool HasValidReservationItemStatusToModifyOrDelete(GuestReservationItem guestReservationItem, ReservationItem reservationItem = null);
        void CheckSumOfPctDailyIs100(GuestReservationItemDto dto, ReservationItemDto reservationItemDto);
        void CheckEstimatedDateWithInvalidPeriodBasedOnReservationDto(GuestReservationItemDto guestReservationItemDto, ReservationItemDto reservationItemDto, bool update);
        void Associate(GuestReservationItemDto dto, int? childAgeParameter);
        void Disassociate(long id);
        void UpdatebasedOnGuestRegistration(GuestRegistrationDto guestRegistrationDto, CountrySubdivisionTranslationDto nationality);
        List<GuestReservationItem> GetListGuestReservationForCheckin(List<long> ids);
        bool AnyGuestReservationItemWithStatusEqualCheckin(List<long> guestReservationIds);
        GuestReservationItem GetGuestReservationForCheckin(long id);
        List<GuestReservationItem> GetListGuestReservationPerReservationItemIdForCheckin(long id);
        GuestReservationItemWithCountrySubdivisionDto GetReservationItemWithCountrySubdivision(long guestReservationItemId, string languageIsoCode);
    }
}