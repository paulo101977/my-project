﻿using Thex.Domain.Entities;
using Tnf.Domain.Services;

namespace Thex.Domain.Services.Interfaces
{
    public interface IBusinessSourceDomainService : IDomainService<BusinessSource>
    {
        void ToggleActivation(int id);
    }
}