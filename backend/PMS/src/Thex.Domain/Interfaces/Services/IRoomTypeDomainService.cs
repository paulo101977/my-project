﻿//  <copyright file="IRoomTypeDomainService.cs" company="TOTVS">
//  Copyright (c) TOTVS. All rights reserved.
//  </copyright>
namespace Thex.Domain.Services.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Thex.Domain.Entities;
    using Thex.Dto;
    using Thex.Dto.Availability;
    using Thex.Dto.Room;
    using Tnf.Domain.Services;

    public interface IRoomTypeDomainService : IDomainService<RoomType>
    {
        bool ToggleActivation(int id);
        bool HasReservationForwards(int id);
        List<AvailabilityRoomTypeColumnDto> GetAvailabilityRoomTypeColumns(List<ParentRoomsWithTotalChildrens> parentRooms, List<ReservationItem> reservationItems, List<AvailabilityRoomTypeRowDto> roomTypesRow, List<RoomBlockingDto> roomBlockings, List<RoomTypeInventory> roomTypeInventoryList, DateTime startDateParam, DateTime endDateParam);
        List<AvailabilityRoomTypeColumnDto> GetAvailabilityRoomTypeColumns(List<ReservationItem> reservationItems, List<AvailabilityRoomTypeRowDto> roomTypesRow);
        List<AvailabilityFooterRowDto> GetAvailabilityTotals(List<AvailabilityRoomTypeColumnDto> availabilityColumns,List<RoomBlockingDto> roomBlockings, List<AvailabilityRoomTypeRowDto> roomTypesRow, DateTime startDate, DateTime finalDate);
        void FillRolesInAvailabilityRoomTypeColumns(List<AvailabilityRoomTypeRowDto> roomTypeAvailability, List<AvailabilityRoomTypeColumnDto> availabilityList, DateTime startDate, DateTime finalDate);
        decimal CalculateAvailabilityPercentage(int total, int availabilityDay, int blockingDay);
        int RoomBlockingCount(List<RoomBlockingDto> roomBlockings, DateTime date);
        decimal CalculateOverbookingPercentage(int totalRooms, int occupiedRooms);
        KeyValuePair<List<AvailabilityFooterColumnDto>, List<AvailabilityFooterColumnDto>> GetAvailabilityTotalsAndPercentageByDate(List<AvailabilityRoomTypeColumnDto> availabilityColumns, DateTime startDate, DateTime finalDate, int total);
     }
}