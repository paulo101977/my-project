﻿using System;
using Thex.Common.Enumerations;
using Thex.Domain.Entities;
using Tnf.Domain.Services;

namespace Thex.Domain.Interfaces.Services
{
    public interface IPropertyAuditProcessDomainService : IDomainService<PropertyAuditProcess>
    {
        void UpdateStepTypeIdAndEndDate(PropertyAuditProcess propertyAuditProcess, int auditStepTypeId, DateTime? endDate = null);
        void UpdatePropertySystemDate(PropertyAuditProcess propertyAuditProcess, DateTime propertySystemDate);
    }
}
