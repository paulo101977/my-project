﻿using System;
using System.Threading.Tasks;
using Thex.Domain.Entities;
using Thex.Dto.ExternalRegistration;
using Tnf.Domain.Services;

namespace Thex.Domain.Services.Interfaces
{
    public interface IExternalRegistrationDomainService : IDomainService<ExternalRegistration>
    {
        Task CreateAsync(Guid reservationUid, string email, string link);
        Task AddNewGuestsIfNecessaryAsync(Guid reservationUid);
        Task UpdateExternalGuestAsync(ExternalGuestRegistrationDto dto);
    }
}