﻿//  <copyright file="IReservationBudgetDomainService.cs" company="TOTVS">
//  Copyright (c) TOTVS. All rights reserved.
//  </copyright>

using Thex.Dto;

namespace Thex.Domain.Services.Interfaces
{
    using System.Collections.Generic;
    using Thex.Domain.Entities;
    using Tnf.Domain.Services;

    public interface IReservationBudgetDomainService : IDomainService<ReservationBudget>
    {
        decimal CalculateWithRatePlanAmount(decimal amount, bool isDecreased, decimal percentualAmmount, int? roundingType);
        decimal CalculateWithRateStrategyAmount(decimal strategyAmount, bool isDecreasedStrategy, decimal? percentualStrategyAmount, decimal? strategyIncrementAmount);
        ReservationItemCommercialBudgetValuesDto CalculateBaseRate(int adultCount, List<int> childrenAgeList, List<PropertyParameterDto> childParameterList, PropertyBaseRate baseRate, RoomType roomType);
        (decimal?, decimal?) CalculatePaxChildValueBasedOnFreeChildQuantity(RoomType roomType, Dictionary<int, int> roomTypeCapacity, int childPos, decimal? paxChildValue, decimal? mealPlanChildValue);
        decimal RoundByRoundingTypeId(decimal total, int? roundingTypeId);
    }
}