﻿using Thex.Domain.Entities;
using Tnf.Domain.Services;

namespace Thex.Domain.Services.Interfaces
{
    public interface IMarketSegmentDomainService : IDomainService<MarketSegment>
    {
        void ToggleActivation(int id);
    }
}