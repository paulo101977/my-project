﻿using System;
using System.Collections.Generic;
using System.Text;
using Thex.Domain.Entities;
using Tnf.Domain.Services;

namespace Thex.Domain.Interfaces.Services
{
    public interface IPropertyDocumentTypeDomainService : IDomainService<PropertyDocumentType>
    {
        void Insert(PropertyDocumentType.Builder propertyDocumentType);
        void Delete(PropertyDocumentType.Builder propertyDocumentType);
    }
}
