﻿using System;
using Thex.Domain.Entities;
using Tnf.Domain.Services;

namespace Thex.Domain.Interfaces.Services
{
    public interface IPropertyParameterDomainService : IDomainService<PropertyParameter>
    {
        void UpdatePropertyParameterValueByApplicationParameterId(int propertyId, int applicationParameterId, string propertyParameterValue);
    }
}
