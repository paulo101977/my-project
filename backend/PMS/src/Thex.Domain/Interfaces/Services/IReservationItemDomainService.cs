﻿// //  <copyright file="IReservationItemDomainService.cs" company="TOTVS">
// //  Copyright (c) TOTVS. All rights reserved.
// //  </copyright>
using Tnf.Domain.Services;

namespace Thex.Domain.Interfaces.Services
{
    using System;
    using System.Collections.Generic;
    using Thex.Common.Enumerations;
    using Thex.Domain.Entities;
    using Thex.Dto;

    public interface IReservationItemDomainService : IDomainService<ReservationItem>
    {
        void CancelReservationItem(ReservationItem reservationItem, int reasonId, string cancellationDescription);

        void CancelReservationItemList(
            List<ReservationItem> reservationItemList,
            int reasonId,
            string cancellationDescription);

        void UpdateStatusReservationItem(long reservationItemId);
        void UpdateStatusReservationItem(long reservationItemId, ReservationStatus reservationEnum);
        IList<ReservationItem> UpdateReservationItemListForNoShowBySystemDate(int propertyId, DateTime systemDate);

        IList<ReservationItem> GetAllEstimatedDepartureDateFromReservationItemListBySystemDate(int propertyId, DateTime systemDate);
        ReservationBudget CreateReservationBudgetToReservationItemListBySystemDate(ReservationItem reservationItem, ReservationBudget lastReservationBudget, DateTime systemDate, ReservationItemCommercialBudgetDto reservationItemCommercialBudgetDto);

        void UpdateDepartureDateReservationItem(long reservationItemId, DateTime date);
        void RemoveRoom(long reservationItemId);

        ReservationItem GetNewInstanceWithReceivedRoomTypeIdAndDates(ReservationItem reservationItem);
        ReservationStatus GetStatusByReservationConfirmation(ReservationConfirmation reservationConfirmation);

        ReservationItem CleanRoomIdIfIsNotAvailable(ReservationItem reservationItem);

        void ConfirmReservationItem(long reservationItemId);

        void CreateReservationBudgetToReservationItemListBySystemDate(IList<ReservationItem> reservationItemList, DateTime systemDate);

        (List<ReservationItem>, int) UpdateEstimatedDepartureDateFromReservationItemListBySystemDate(int propertyId, DateTime estimatedDepartureDate);
        void BulkUpdate(List<ReservationItem> reservationItemList);
        ReservationItem ChangeEstimatedArrivalDate(ReservationItem reservationItem, DateTime? systemDate);
        void ValidateReactivation(ReservationItem reservationItem, DateTime? systemDate, ReservationStatus newStatus);
    }
}