﻿// //  <copyright file="IReservationItemStatusDomainService.cs" company="TOTVS">
// //  Copyright (c) TOTVS. All rights reserved.
// //  </copyright>
namespace Thex.Domain.Interfaces.Services
{
    using System;
    using Thex.Common.Enumerations;
    using Thex.Domain.Entities;

    using Tnf.Application.Services;

    public interface IReservationItemStatusDomainService : IApplicationService
    {
        bool CanChangeStatus(ReservationStatus currentStatus, ReservationStatus newStatus);

        bool CanChangeStatus(ReservationItem reservationItem, ReservationStatus newStatus);

        ReservationItem GetNewInstanceWithStatusId(ReservationItem reservationItem);

        ReservationItem GetNewInstanceWithStatusIdAndRoomId(ReservationItem reservationItem);

        ReservationItem GetNewInstanceWithStatusAndAndRoomAndRoomTypeIdAndEstimatedDates(ReservationItem reservationItem);

        bool CanChangeReservationItem(int reservationItemStatusId);
    }
}