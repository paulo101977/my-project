﻿using System.Collections.Generic;
using Thex.Domain.Entities;
using Tnf.Domain.Services;

namespace Thex.Domain.Services.Interfaces
{
    public interface IBillingAccountItemDomainService : IDomainService<BillingAccountItem>
    {
        decimal CalculateTax(decimal amount, decimal taxPercentage);
        void UpdateBillingAccountItemToReversed(BillingAccountItem billingAccountItem, int reasonId, string observation = "");
        void UpdateBillingAccountItemToCreditNote(BillingAccountItem billingAccountItem);
        bool CheckIfBillingItemIsTax(BillingAccountItem billingAccountItem);
        decimal CalculateAmountWithPercentage(decimal amount, decimal taxPercentage);
        void UndoPartial(List<BillingAccountItem> originalBillingAccountItemList, List<BillingAccountItem> billingAccountItemList);
        void ValidateUndoPartial(List<BillingAccountItem> billingAccountItemList, List<BillingAccountItem> partialParentBillingAccountItemList);
    }
}