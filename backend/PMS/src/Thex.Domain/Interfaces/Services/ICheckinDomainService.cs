﻿using System;
using System.Collections.Generic;
using Thex.Domain.Entities;

namespace Thex.Domain.Interfaces.Services
{
    public interface ICheckinDomainService
    {
        void CreateGuestAccounts(List<GuestReservationItem> guestReservationItemList, long reservationId, int propertyId, int businessSourceId, int marketSegmentId, List<BillingAccount> existingMainBillingAccounts);
        void UpdateToCheckInGuestReservationItem(List<GuestReservationItem> guestReservationItemList, DateTime checkinDate);
        void Cancel(long reservationItemId, DateTime systemDate);
    }
}
