﻿using System;
using System.Collections.Generic;
using System.Text;
using Thex.Domain.Entities;
using Tnf.Domain.Services;

namespace Thex.Domain.Interfaces.Services
{
    public interface IChannelDomainService : IDomainService<Channel>
    {
        void DeleteChannel(Channel.Builder channelBuilder);
        void Create(Channel.Builder channelBuilder);
        void ToggleAndSaveIsActive(Guid id);
        void Update(Channel.Builder channelBuilder);
    }
}
