﻿//  <copyright file="IRoomDomainService.cs" company="TOTVS">
//  Copyright (c) TOTVS. All rights reserved.
//  </copyright>
namespace Thex.Domain.Services.Interfaces
{
    using System.Collections.Generic;
    
    using Thex.Domain.Entities;
    using Thex.Dto;
    using Thex.Dto.Availability;
    using Tnf.Domain.Services;
    using Tnf.Dto;

    public interface IRoomDomainService : IDomainService<Room>
    {
        bool ToggleActivation(int id);
        bool HasReservationForwards(int id);
        int Create(Room.Builder roomBuilder, IList<int> childRoomIdList);
        void Update(Room.Builder roomBuilder, IList<int> childRoomIdList);
        IList<int> GetChildrenIds(int id);
        List<AvailabilityRoomRowDto> GetAvailabilityRoomRowDto(IListDto<RoomDto> allPropertyRooms, int roomTypeId);
    }
}