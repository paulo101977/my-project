﻿using Thex.Domain.Entities;
using Tnf.Domain.Services;

namespace Thex.Domain.Services.Interfaces
{
    using System;
    using System.Collections.Generic;

    using Thex.Dto;

    public interface ICompanyClientDomainService : IDomainService<CompanyClient>
    {
        void LocationListQuantityIsValid(ICollection<LocationDto> locationList);
        void LocationListTypesIsValid(ICollection<LocationDto> locationList, char personType);
        void ValidateBasicDtoProperties(CompanyClientDto companyClient);
        void DocumentListIsValid(CompanyClientDto companyClient, string isoCode);
        void ValidateBasicDtoPropertiesByNewSparseAccount(CompanyClientDto companyClient);
    }
}