﻿using System;
using System.Collections.Generic;
using System.Text;
using Thex.Domain.Entities;
using Tnf.Repositories;

namespace Thex.Domain.Interfaces.Repositories
{
    public interface IRatePlanRoomTypeRepository : IRepository<RatePlanRoomType>
    {

        void AddRange(List<RatePlanRoomType> ratePlanRoomTypes);
        void RatePlanRoomTypeUpdate(RatePlanRoomType ratePlanRoomType);

    }
}
