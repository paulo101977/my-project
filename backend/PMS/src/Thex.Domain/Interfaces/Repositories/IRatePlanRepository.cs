﻿using System;
using System.Collections.Generic;
using System.Text;
using Thex.Domain.Entities;
using Tnf.Repositories;

namespace Thex.Domain.Interfaces.Repositories
{
    public interface IRatePlanRepository : IRepository<RatePlan>
    {
        void ToggleAndSaveIsActive(Guid id);
        void RatePlanCreate(RatePlan ratePlan);
        void RatePlanUpdate(RatePlan ratePlanOld, RatePlan ratePlanNew, List<RatePlanRoomType> ratePlanRoomTypeList, List<RatePlanCompanyClient> ratePlanCompanyClientList, List<RatePlanCommission> ratePlanCommissionList) ;
        void Delete(Guid id);
    }
}
