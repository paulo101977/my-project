﻿//  <copyright file="IRoomTypeRepository.cs" company="TOTVS">
//  Copyright (c) TOTVS. All rights reserved.
//  </copyright>
namespace Thex.Domain.Interfaces.Repositories
{
    using System.Collections.Generic;
    using Thex.Domain.Entities;
    using Tnf.Repositories;

    public interface IRoomTypeRepository : IRepository<RoomType>
    {
        bool ToggleAndSaveIsActive(int id);
        List<RoomType> GetAllByPropertyId(int propertyId);
        void RemoveRoomTypeBedTypePocoFromDatabaseWithRoomTypeId(int roomTypeId);
        void Delete(int id);

    }
}