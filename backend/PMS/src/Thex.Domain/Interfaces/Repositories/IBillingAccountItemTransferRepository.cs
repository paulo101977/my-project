﻿// //  <copyright file="IBillingAccountItemTransferRepository.cs" company="TOTVS">
// //  Copyright (c) TOTVS. All rights reserved.
// //  </copyright>
namespace Thex.Domain.Interfaces.Repositories
{
    using System;
    using System.Collections.Generic;

    using Thex.Domain.Entities;

    using Tnf.Repositories;

    public interface IBillingAccountItemTransferRepository : IRepository<BillingAccountItemTransfer>
    {
        void AddRange(IList<BillingAccountItemTransfer> billingAccountItemTransferList);
    }
}