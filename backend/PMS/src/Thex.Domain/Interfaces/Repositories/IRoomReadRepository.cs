﻿//  <copyright file="IRoomReadRepository.cs" company="TOTVS">
//  Copyright (c) TOTVS. All rights reserved.
//  </copyright>
namespace Thex.EntityFrameworkCore.ReadInterfaces.Repositories
{
    using System.Collections.Generic;
    
    using Thex.Dto.Rooms;
    using Tnf.Dto;
    using System;
    using Thex.Dto.Base;
    using Thex.Dto;
    using Thex.Dto.Room;
    using Thex.Dto.Housekeeping;
    using Thex.Domain.Entities;
    using Tnf.Repositories;
    using System.Threading.Tasks;

    public interface IRoomReadRepository : IRepository
    {
        IListDto<RoomDto> GetRoomsByPropertyId(GetAllRoomsDto request, int propertyId);
        IListDto<RoomDto> GetChildRoomsByParentRoomId(GetAllRoomsDto request, int parentRoomId);
        IListDto<RoomDto> GetParentRoomsByPropertyId(GetAllRoomsDto request, int propertyId);
        IListDto<RoomDto> GetRoomsWithoutChildrenByPropertyId(GetAllRoomsDto request, int propertyId);
        IListDto<RoomDto> GetRoomsWithoutParentAndChildrenByPropertyId(GetAllRoomsDto request, int propertyId);
        IListDto<RoomDto> GetAllRoomsByPropertyAndPeriod(GetAllRoomsByPropertyAndPeriodDto request, int propertyId, int? roomTypeId = null);
        IListDto<RoomDto> GetAllAvailableByPeriodOfRoomType(PeriodDto request, int propertyId, int roomTypeId, int? reservationItemId, bool isCheckin, bool available);
        List<RoomDto> GetAllRoomsByPropertyAndPeriodWithoutPagination(GetAllRoomsByPropertyAndPeriodDto request, int propertyId, int? roomTypeId = null, bool isCheckIn = false);
        bool RoomIsAvailableByPeriodOfRoomType(PeriodDto request, int propertyId, int roomTypeId, int roomId);
        List<ParentRoomsWithTotalChildrens> GetParentRoomsWithTotalChildrens(int propertyId);
        bool AnyRoomTypeIdForRoomId(int roomTypeId, int roomId);
        IListDto<GetAllHousekeepingAlterStatusDto> GetAllHousekeepingAlterStatusRoom(GetAllHousekeepingAlterStatusRoomDto request, int propertyId, DateTime systemDate);
        List<int>  GetAllOccupedRoomsByPropertyAndPeriod(DateTime initialDate,DateTime endDate, int propertyId);
        IListDto<GetAllHousekeepingAlterStatusDto> GetAllRoomWithHousekeepingStatus(int propertyId);
        IListDto<RoomDto> GetAllStatusCheckinRooms(GetAllRoomsByPropertyAndPeriodDto request, int propertyId);
        IList<Room> GetOccupiedRoomListByPropertyIdAndSystemDate(int propertyId, DateTime systemDate);
        IList<Room> GetSpareRoomListByPropertyIdAndSystemDateForChangeHousekeepingStatus(int propertyId, DateTime systemDate, int numberOfDaysToChangeRoomStatus);
        bool IsAvailableExceptReservationItemId(int propertyId, int roomId, long reservationItemId);
        bool ValidateIfRoomIsInCheckIn(int roomId);
        bool RoomTypeCouldNotBeChanged(int roomId, int roomTypeId);
        Task<int> CountByPropertyIdAsync(int propertyId);
        int? GetRoomTypeIdByRoomId(int id);
    }
}