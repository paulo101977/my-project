﻿using System;
using System.Collections.Generic;
using System.Text;
using Thex.Domain.Entities;

namespace Thex.Domain.Interfaces.Repositories
{
    public interface IBillingTaxRepository
    {
        void ToggleAndSaveIsActive(Guid id);
        void Update(BillingTax billingTax);
        void Delete(Guid id);
        void Create(BillingTax billingTax);
    }
}
