﻿using System.Collections.Generic;
using Thex.Domain.Entities;

namespace Thex.Domain.Interfaces.Repositories
{
    public interface IReservationItemLaunchRepository
    {
        void InsertList(List<ReservationItemLaunch> entityList);
        void UpdateList(List<ReservationItemLaunch> entityList);
    }
}
