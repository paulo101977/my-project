﻿//  <copyright file="IReservationRepository.cs" company="TOTVS">
//  Copyright (c) TOTVS. All rights reserved.
//  </copyright>
namespace Thex.Domain.Interfaces.Repositories
{
    using System;
    using System.Collections.Generic;

    using Thex.Domain.Entities;
    using Tnf.Repositories;

    public interface IReservationRepository : IRepository<Reservation>
    {
        Reservation Update(Reservation reservation, Reservation existingReservation, ReservationConfirmation reservationConfirmation, Plastic plastic, DateTime systemDate);
        Reservation GetExistingReservation(long reservationId);
        bool CheckStatusAllItensForModification(long reservationId);
        IList<Reservation> GetReservationListWithConfirmedOrToConfirmStatus(int propertyId);
        void ChangeStatusReservationItem(long reservationItemId, int status, DateTime systemDate);
        List<ReservationItem> GetReservationItemListByEstimatedDepartureDateAndPropertyId(DateTime estimatedDepartureDate, int propertyId);
        void RemoveRoom(long reservationItemId);
    }
}