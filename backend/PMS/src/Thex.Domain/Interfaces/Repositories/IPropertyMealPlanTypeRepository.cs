﻿using System;
using System.Collections.Generic;
using System.Text;
using Thex.Domain.Entities;

namespace Thex.Domain.Interfaces.Repositories
{
    public interface IPropertyMealPlanTypeRepository
    {
        PropertyMealPlanType GetById(Guid id);
        void PropertyMealPlanTypeUpdate(PropertyMealPlanType propertyMealPlanType);
        void ToggleAndSaveIsActive(PropertyMealPlanType id);
    }
}
