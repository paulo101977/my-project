﻿using System;
using System.Collections.Generic;
using System.Text;
using Thex.Domain.Entities;
using Tnf.Dependency;
using Tnf.Repositories;

namespace Thex.Domain.Interfaces.Repositories
{
    public interface IPropertyDocumentTypeRepository : IRepository<PropertyDocumentType>, IRepository, ITransientDependency, ISupportsExplicitLoading<RoomOccupied>
    {
        void Insert(PropertyDocumentType propertyDocumentType);
        void DeleteRoomOccupiedWithRoomId(PropertyDocumentType propertyDocumentType);   
    }
}
