﻿using System;
using System.Collections.Generic;
using System.Text;
using Thex.Domain.Entities;

namespace Thex.Domain.Interfaces.Repositories
{
    public interface ILevelRateRepository
    {
        void AddRangeAndSaveChanges(ICollection<LevelRate> levelRateList);
        void RemoveRangeByLevelRateHeaderIdAndSaveChanges(Guid levelRateHeaderId);
    }
}
