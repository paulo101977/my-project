﻿// //  <copyright file="IPersonRepository.cs" company="TOTVS">
// //  Copyright (c) TOTVS. All rights reserved.
// //  </copyright>
namespace Thex.Domain.Interfaces.Repositories
{
    using System;
    using System.Collections.Generic;

    using Thex.Domain.Entities;

    using Tnf.Repositories;

    public interface IPersonRepository : IRepository<Person>
    {
        void AddRange(IList<Person> personList);

        void UpdateWithDocumentLocationContactInformation(Person person);

        Guest CreateGuestIfNotExist(Guest guest);

        void SaveChanges();

        void UpdatePersonHeaderWithContactInformation(Person person);
    }
}