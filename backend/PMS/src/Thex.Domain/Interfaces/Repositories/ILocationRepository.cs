﻿// //  <copyright file="ILocationRepository.cs" company="TOTVS">
// //  Copyright (c) TOTVS. All rights reserved.
// //  </copyright>
namespace Thex.Domain.Interfaces.Repositories
{
    using System;
    using System.Collections.Generic;

    using Thex.Domain.Entities;

    public interface ILocationRepository
    {
        void AddRange(IList<Location> locationList);
        void UpdateLocationList(IList<Location> locationLis, Guid ownerId);
        void CreateOrUpdate(Location location, Guid ownerId);
    }
}