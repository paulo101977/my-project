﻿using Thex.Domain.Entities;
using Tnf.Repositories;

namespace Thex.Domain.Interfaces.Repositories
{
    public interface ISibaIntegrationRepository : IRepository<SibaIntegration>
    {
    }
}
