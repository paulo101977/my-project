﻿// //  <copyright file="IBillingAccountRepository.cs" company="TOTVS">
// //  Copyright (c) TOTVS. All rights reserved.
// //  </copyright>
namespace Thex.Domain.Interfaces.Repositories
{
    using System;
    using System.Collections.Generic;

    using Thex.Domain.Entities;
    using Thex.Dto;
    using Tnf.Repositories;

    public interface IBillingAccountRepository : IRepository<BillingAccount>
    {
        BillingAccount GetById(Guid id);
        IList<BillingAccount> GetListByGroupKey(Guid groupKey);
        void CloseBillingAccount(Guid billingAccountId);
        void CloseAllBillingAccounts(List<Guid> billingAccountIdList);
        decimal GetTotalAmountByBillingAccountId(Guid billingAccountId);
        void SaveChanges();
        void DetachBillingInvoice(BillingInvoice billingInvoice);
        void DetachBillingAccount(BillingAccount billingAccount);
        BillingAccount GetBillingAccountByInvoiceId(Guid invoiceId);
        BillingAccount GetWithPersonHeaderById(Guid id);
        void CreateBillingAccounts(List<BillingAccount> billingAccountList);
        void UpdateBillingAccounts(List<BillingAccount> billingAccountList);
        void DeleteRange(List<Guid> idList);
    }
}