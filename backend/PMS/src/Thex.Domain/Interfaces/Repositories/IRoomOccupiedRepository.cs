﻿using System;
using System.Collections.Generic;
using System.Text;
using Thex.Domain.Entities;
using Tnf.Dependency;
using Tnf.Repositories;

namespace Thex.Domain.Interfaces.Repositories
{
    public interface IRoomOccupiedRepository : IRepository<RoomOccupied>, IRepository, ITransientDependency, ISupportsExplicitLoading<RoomOccupied>
    {
        void InsertRoomOccupied(RoomOccupied RoomOccupied);
        void DeleteRoomOccupiedWithRoomId(int RoomOccupied);   
    }
}
