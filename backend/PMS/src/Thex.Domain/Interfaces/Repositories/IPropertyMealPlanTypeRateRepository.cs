﻿using System;
using System.Collections.Generic;
using System.Text;
using Thex.Domain.Entities;

namespace Thex.Domain.Interfaces.Repositories
{
    public interface IPropertyMealPlanTypeRateRepository
    {
        void CreateOrUpdate(List<PropertyMealPlanTypeRate> addList, List<PropertyMealPlanTypeRate> updateList);
        void ToggleActive(Guid id);
    }
}
