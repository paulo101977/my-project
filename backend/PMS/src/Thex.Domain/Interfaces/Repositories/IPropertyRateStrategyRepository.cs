﻿using System;
using Thex.Domain.Entities;
using Tnf.Repositories;

namespace Thex.Domain.Interfaces.Repositories
{
    public interface IPropertyRateStrategyRepository : IRepository<PropertyRateStrategy>
    {
        PropertyRateStrategy Create(PropertyRateStrategy propertyRateStrategy);
        PropertyRateStrategy Update(PropertyRateStrategy propertyRateStrategy, PropertyRateStrategy existingPropertyRateStrategy);
        PropertyRateStrategy GetById(Guid id);
        void ToggleAndSaveIsActive(Guid id);
    }
}
