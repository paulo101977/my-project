﻿//  <copyright file="IReservationBudgetRepository.cs" company="TOTVS">
//  Copyright (c) TOTVS. All rights reserved.
//  </copyright>
namespace Thex.Domain.Interfaces.Repositories
{
    using System;
    using System.Collections.Generic;

    using Thex.Domain.Entities;
    using Tnf.Repositories;

    public interface IReservationBudgetRepository : IRepository<ReservationBudget>
    {
        void AddRange(List<ReservationBudget> reservationBudget);
        void RemoveAll(ReservationItem reservationItem, List<ReservationBudget> reservationBudget, bool completeChanges, DateTime? systemDate = null);
        void RemoveAndAddRange(ReservationItem reservationItem, List<ReservationBudget> reservationBudget, DateTime? systemDate = null);
        void RemoveAndAddRange(long reservationItemId, List<ReservationBudget> reservationBudget, DateTime? systemDate = null);
        void RemoveAndAddRangeByReservationItemId(long reservationItemId, List<ReservationBudget> reservationBudget, DateTime? systemDate);
        void UpdateRange(List<ReservationBudget> reservationBudget);
        ReservationBudget GetLastReservationBudget(ReservationItem reservationItem);
        void AddReservationBudgetRange(ICollection<ReservationBudget> reservationBudgetList);
        void RemoveRange(DateTime systemDate, long reservationItemId, bool isEarlyDayUse);
        void BulkInsert(List<ReservationBudget> reservationBudgetList);
        void DeleteRange(List<long> idList);
        void RemoveRangeNoShow(DateTime? systemDate, long reservationItemId);
    }
}
