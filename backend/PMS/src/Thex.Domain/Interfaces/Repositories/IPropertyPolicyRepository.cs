﻿using System;
using System.Collections.Generic;
using System.Text;
using Thex.Domain.Entities;

namespace Thex.Domain.Interfaces.Repositories
{
    public interface IPropertyPolicyRepository
    {
        void ToggleAndSaveIsActive(Guid id);
        void PropertyPolicyDelete(Guid id);
        void PropertyPolicyCreate(PropertyPolicy propertyPolicy);
        void PropertyPolicyUpdate(PropertyPolicy propertyPolicy);
        void Delete(Guid id);
    }
}
