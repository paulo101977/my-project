﻿using System;
using Thex.Domain.Entities;

namespace Thex.Domain.Interfaces.Repositories
{
    public interface IPropertyGuestPolicyRepository
    {
        void Create(PropertyPolicy propertyPolicy);       
        void Update(PropertyPolicy propertyPolicy);
        void Delete(Guid id);
    }
}
