﻿using System;
using System.Collections.Generic;
using System.Text;
using Thex.Domain.Entities;
using Tnf.Repositories;

namespace Thex.Domain.Interfaces.Repositories
{
    public interface IChannelRepository : IRepository<Channel>
    {
        void DeleteChannel(Channel channel);
        void Create(Channel billingTax);
        void ToggleAndSaveIsActive(Guid id);
    }
}
