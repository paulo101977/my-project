﻿using System;
using System.Collections.Generic;
using System.Text;
using Thex.Domain.Entities;

namespace Thex.Domain.Interfaces.Repositories
{
    public interface IRoomBlockingRepository
    {
        void BlockingRoom(RoomBlocking roomBlocking);
        void UnlockingRoom(Guid roomBlocking);
        void UnlockingRoom(List<RoomBlocking> roomBlockingList);
    }
}
