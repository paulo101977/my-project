﻿using System;
using System.Collections.Generic;
using System.Text;
using Thex.Domain.Entities;
using Tnf.Repositories;

namespace Thex.Domain.Interfaces.Repositories
{
    public  interface IRatePlanCompanyClientRepository : IRepository<RatePlanCompanyClient>
    {
        void RatePlanCompanyClientCreate(List<RatePlanCompanyClient> ratePlanCompanyClients);
    }
}
