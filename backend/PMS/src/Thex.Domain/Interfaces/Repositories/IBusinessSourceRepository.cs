﻿//  <copyright file="IRoomRepository.cs" company="TOTVS">
//  Copyright (c) TOTVS. All rights reserved.
//  </copyright>
namespace Thex.Domain.Interfaces.Repositories
{
    using System.Collections.Generic;

    using Thex.Domain.Entities;
    using Tnf.Repositories;
    public interface IBusinessSourceRepository : IRepository<BusinessSource>
    {
        void ToggleAndSaveIsActive(int id);
    }
}