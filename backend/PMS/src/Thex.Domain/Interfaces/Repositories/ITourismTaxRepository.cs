﻿using System;
using Thex.Domain.Entities;
using Tnf.Repositories;

namespace Thex.Domain.Interfaces.Repositories
{
    public interface ITourismTaxRepository : IRepository<TourismTax>
    {
        void ToggleActivation(Guid id);
        TourismTax Get(Guid id);
    }
}
