﻿// //  <copyright file="IBillingAccountItemRepository.cs" company="TOTVS">
// //  Copyright (c) TOTVS. All rights reserved.
// //  </copyright>
namespace Thex.Domain.Interfaces.Repositories
{
    using System.Collections.Generic;

    using Thex.Domain.Entities;

    using Tnf.Repositories;

    public interface IBillingInvoicePropertySupportedTypeRepository : IRepository<BillingInvoicePropertySupportedType>
    {
        void CreateRange(List<BillingInvoicePropertySupportedType> entityList);
    }
}