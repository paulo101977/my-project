﻿using System;
using System.Collections.Generic;
using System.Text;
using Thex.Domain.Entities;

namespace Thex.Domain.Interfaces.Repositories
{
    public  interface IHousekeepingStatusPropertyRepository
    {
        void ToggleAndSaveIsActive(Guid id);      
        void HousekeepingStatusPropertyCreate(HousekeepingStatusProperty housekeepingStatusProperty);
        new void Update(HousekeepingStatusProperty housekeepingStatusProperty);
        void HousekeepingStatusPropertyDelete(Guid id);
        void UpdateRoomForHousekeeppingStatus(int roomId, Guid housekeepingStatusPropertyId);
        void UpdateManyRoomForHousekeeppingStatus(List<int> roomIds, Guid housekeepingStatusPropertyId);
    }
}
