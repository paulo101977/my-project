﻿using System;
using System.Collections.Generic;
using Thex.Domain.Entities;

namespace Thex.Domain.Interfaces.Repositories
{
    public interface IRoomTypeInventoryRepository
    {
        void CreateList(IList<RoomTypeInventory> roomTypeInventoryList);
        void UpdateList(IList<RoomTypeInventory> roomTypeInventoryList);
        IEnumerable<RoomTypeInventory> GetAllByRoomTypeIdAndDate(int roomTypeId, DateTime date);
        IEnumerable<RoomTypeInventory> GetAllByPropertyIdAndDate(int propertyId, DateTime date);
        IEnumerable<RoomTypeInventory> GetAllByRoomTypeIdAndPeriod(int roomTypeId, DateTime initialDate, DateTime endDate);
        IEnumerable<RoomTypeInventory> GetAllByRoomTypeIdListAndPeriod(IEnumerable<int> roomTypeIdList, DateTime initialDate, DateTime endDate);
        RoomTypeInventory GetLastByRoomTypeId(int roomTypeId);
        IEnumerable<RoomTypeInventory> GetAllByPeriod(DateTime initialDate, DateTime endDate);
    }
}
