﻿// //  <copyright file="IBillingAccountItemRepository.cs" company="TOTVS">
// //  Copyright (c) TOTVS. All rights reserved.
// //  </copyright>
namespace Thex.Domain.Interfaces.Repositories
{
    using System;
    using System.Collections.Generic;

    using Thex.Domain.Entities;

    using Tnf.Repositories;

    public interface IBillingAccountItemRepository : IRepository<BillingAccountItem>
    {
        void AddRange(List<BillingAccountItem> billingAccountItems);
        void TransferRange(Guid billingAccountDestination, List<BillingAccountItem> billingAccountItems);
        IList<BillingAccountItem> GetListByInvoiceId(Guid invoiceId);
        BillingAccountItem GetById(Guid id);
        void UpdateRange(List<BillingAccountItem> billingAccountItem);
        void CreateBulk(List<BillingAccountItem> billingAccountItemList);
        void UpdateBulk(List<BillingAccountItem> billingAccountItemList);
    }
}