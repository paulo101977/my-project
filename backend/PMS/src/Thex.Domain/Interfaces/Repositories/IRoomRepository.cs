﻿//  <copyright file="IRoomRepository.cs" company="TOTVS">
//  Copyright (c) TOTVS. All rights reserved.
//  </copyright>
namespace Thex.Domain.Interfaces.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Thex.Domain.Entities;
    using Thex.Domain.JoinMap;
    using Tnf.Repositories;

    public interface IRoomRepository : IRepository<Room>
    {
        bool ToggleAndSaveIsActive(int id);

        void UpdateChildRoomList(IList<int> ids, int roomId);
        void RemoveParentIdOfChildren(IList<int> ids);
        IList<int> GetChildrenIdsOfParentExcept(int id, IList<int> exceptIds);
        IList<int> GetChildrenIds(int id);
        void UpdateRoomForHousekeeppingStatus(int roomId, Guid housekeepingStatusPropertyId);
        void UpdateManyRoomForHousekeeppingStatus(List<int> roomIds, Guid housekeepingStatusPropertyId);
        void Delete(int id);
        Room GetByReservationItemId(long reservationItemId);
        Room GetByGuestReservationItemId(long reservationItemId);
        IQueryable<RoomReservationItemJoinMap> GetAllOccupiedRoomList();
        IQueryable<RoomBlockingJoinMap> GetAllBlockingRoomList(int propertyId);
        IList<Room> GetAllByIdList(IEnumerable<int> roomIdList);
        void UpdateAndSaveChanges(Room room);
        void CleanAllRoomIdInReservationItem(int roomId);
    }
}