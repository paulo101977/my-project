﻿using System;
using System.Collections.Generic;
using Thex.Domain.Entities;
using Tnf.Repositories;

namespace Thex.Domain.Interfaces.Repositories
{
    public interface ICompanyClientChannelRepository : IRepository<CompanyClientChannel>
    {
        void ToogleIsActive(Guid id);
        void UpdateRangeAndSaveChanges(List<CompanyClientChannel> companyClientChannelList);
        void InsertRangeAndSavechanges(List<CompanyClientChannel> companyClientChannelList);
        void RemoveRangeAndSavechanges(List<CompanyClientChannel> companyClientChannelList);
    }
}
