﻿using System;
using System.Collections.Generic;
using System.Text;
using Thex.Domain.Entities;
using Tnf.Repositories;

namespace Thex.Domain.Interfaces.Repositories
{
    public interface IGuestRegistrationDocumentRepository : IRepository<GuestRegistrationDocument>
    {
        void AddRange(ICollection<GuestRegistrationDocument> guestRegistrationDocumentList);
        void UpdateRange(ICollection<GuestRegistrationDocument> guestRegistrationDocumentList);
        void RemoveRange(ICollection<GuestRegistrationDocument> guestRegistrationDocumentList);
    }
}
