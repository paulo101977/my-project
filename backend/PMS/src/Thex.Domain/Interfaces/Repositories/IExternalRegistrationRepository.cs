﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.Domain.Entities;
using Tnf.Repositories;

namespace Thex.Domain.Interfaces.Repositories
{
    public interface IExternalRegistrationRepository : IRepository<ExternalRegistration>
    {
        Task InsertItemsAndSaveChangesAsync(List<ExternalGuestRegistration> externalRegistrationList);
        Task UpdateExternalGuestAndSaveChanges(ExternalGuestRegistration externalGuestRegistration);
        Task<ExternalGuestRegistration> GetByGuestReservationItemIdAsync(long guestReservationItemId);
        Task<ExternalRegistration> GetByReservationUidAsync(Guid reservationUid);
    }
}