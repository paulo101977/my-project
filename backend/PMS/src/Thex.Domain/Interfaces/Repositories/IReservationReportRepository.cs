﻿using System;
using System.Collections.Generic;
using System.Text;
using Thex.Dto.Report;
using Thex.Dto.Reservation;

namespace Thex.Domain.Interfaces.Repositories
{
    public interface IReservationReportRepository
    {
        PostMeanPlanControlReportDto PostMeanPlan(PostMeanPlanControlReportDto request, Guid tenantId, DateTime? systemDate);
        PostHousekeepingRoomDisagreementDto PostHousekeepingRoomDisagreement(PostHousekeepingRoomDisagreementDto request, DateTime? systemDate);
    }
}
