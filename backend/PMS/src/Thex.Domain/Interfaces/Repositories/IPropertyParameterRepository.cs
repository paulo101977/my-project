﻿using System;
using Thex.Domain.Entities;
using Tnf.Repositories;

namespace Thex.Domain.Interfaces.Repositories
{
    public interface IPropertyParameterRepository : IRepository<PropertyParameter>
    {
        new void Update(PropertyParameter propertyParameterDto);
        void ToggleAndSaveIsActive(Guid id);
    }
}
