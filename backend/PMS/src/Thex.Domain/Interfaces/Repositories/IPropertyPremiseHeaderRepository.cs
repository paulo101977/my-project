﻿using System;
using System.Collections.Generic;
using System.Text;
using Thex.Domain.Entities;

namespace Thex.Domain.Interfaces.Repositories
{
    public interface IPropertyPremiseHeaderRepository
    {
        void ToggleActive(Guid id);
        void UpdatePropertyPremiseRange(ICollection<PropertyPremise> propertyPremiseList);
        void AddPropertyPremiseRange(ICollection<PropertyPremise> propertyPremiseList);
    }
}
