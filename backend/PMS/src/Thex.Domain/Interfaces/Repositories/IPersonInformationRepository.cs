// //  <copyright file="IPersonInformationRepository.cs" company="TOTVS">
// //  Copyright (c) TOTVS. All rights reserved.
// //  </copyright>
namespace Thex.Domain.Interfaces.Repositories
{
    using System.Collections.Generic;

    using Thex.Domain.Entities;

    using Tnf.Repositories;

    public interface IPersonInformationRepository : IRepository<PersonInformation>
    {
        void AddRange(IList<PersonInformation> personInformationList);
    }
}