﻿// //  <copyright file="IDocumentRepository.cs" company="TOTVS">
// //  Copyright (c) TOTVS. All rights reserved.
// //  </copyright>
namespace Thex.Domain.Interfaces.Repositories
{
    using System;
    using System.Collections.Generic;

    using Thex.Domain.Entities;
    using Thex.Dto;

    public interface IDocumentRepository
    {
        void AddRange(IList<Document> documentList);
        void UpdateDocumentList(IList<Document> documentList, Guid ownerId);
        void CreateOrUpdateDocumentGuestRegistration(Document document, Guid ownerId);
        void RemoveDocument(Document existingDocument);
    }
}