﻿// //  <copyright file="ICompanyClientRepository.cs" company="TOTVS">
// //  Copyright (c) TOTVS. All rights reserved.
// //  </copyright>
namespace Thex.Domain.Interfaces.Repositories
{
    using System;
    using System.Collections.Generic;

    using Thex.Domain.Entities;

    using Tnf.Repositories;

    public interface IContactInformationRepository : IRepository<ContactInformation>
    {
        void AddRangeAndSaveChanges(IList<ContactInformation> companyClientList);
        string GetEmailContactInformationByPersonId(Guid guid);
        void ResetContactInformationsProperty(Guid guid);
    }
}