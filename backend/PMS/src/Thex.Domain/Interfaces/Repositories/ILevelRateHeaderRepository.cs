﻿using System;
using System.Collections.Generic;
using System.Text;
using Thex.Domain.Entities;

namespace Thex.Domain.Interfaces.Repositories
{
    public interface ILevelRateHeaderRepository
    {
        void ToggleIsActive(Guid id);
        void RemoveAndSaveChanges(LevelRateHeader levelRateHeader);
    }
}
