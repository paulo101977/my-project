﻿using System;
using System.Threading.Tasks;

namespace Thex.Domain.Interfaces.Repositories.ReadDapper
{
    public interface IReservationDapperReadRepository
    {
        Task<bool> AnyByReservationUidAsync(Guid reservationUid);
        Task<bool> AnyByGuestReservationItemIdAsync(Guid reservationUid, long guestReservationItemId);
    }
}