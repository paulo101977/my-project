﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.Dto.ExternalRegistration;

namespace Thex.Domain.Interfaces.Repositories.ReadDapper
{
    public interface IGuestReservationItemDapperReadRepository
    {
        Task<List<long>> GetIdListByReservationUidAsync(Guid reservationUid);
        Task<ExternalGuestRegistrationDto> GetExternalGuestByGuestReservationItemId(long guestReservationItemId);
        Task<GuestRegistrationDapperDto> GetGuestRegistrationByGuestReservationItemId(long guestReservationItemId);
        Task<List<ExternalGuestRegistrationDto>> GetExternalGuestListByReservationItemUid(Guid reservationItemUid);
        Task<List<GuestRegistrationDapperDto>> GetGuestRegistrationListByReservationItemUid(Guid reservationItemUid);
    }
}