﻿using System;
using System.Threading.Tasks;
using Thex.Dto.ExternalRegistration;
using Tnf.Dto;

namespace Thex.Domain.Interfaces.Repositories.ReadDapper
{
    public interface IReservationItemDapperReadRepository
    {
        Task<DateTime> GetMaxDepartureDateByReservationUidAsync(Guid reservationUid);
        Task<ExternalRoomListHeaderDto> GetHeaderByReservationUid(Guid reservationUid);
        Task<IListDto<ExternalRoomListDto>> GetExternalListByReservationUid(Guid reservationUid);
    }
}