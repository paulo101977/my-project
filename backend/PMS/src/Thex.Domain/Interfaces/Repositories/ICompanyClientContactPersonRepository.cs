﻿// //  <copyright file="ICompanyClientContactPersonRepository.cs" company="TOTVS">
// //  Copyright (c) TOTVS. All rights reserved.
// //  </copyright>
namespace Thex.Domain.Interfaces.Repositories
{
    using System;
    using System.Collections.Generic;

    using Thex.Domain.Entities;

    using Tnf.Repositories;

    public interface ICompanyClientContactPersonRepository : IRepository<CompanyClientContactPerson>
    {
        void AddRange(IList<CompanyClientContactPerson> companyClientContactPersonList);

        void RemoveContactInformationListFromPersonContactByClientId(Guid clientId);
        void RemoveRangeFromCompanyClientContactPerson(IList<CompanyClientContactPerson> companyClientContactPersonList);
    }
}