﻿using System;
using System.Collections.Generic;
using System.Text;
using Thex.Domain.Entities;
using Tnf.Repositories;

namespace Thex.Domain.Interfaces.Repositories
{
    public interface IBillingItemRepository : IRepository<BillingItem>
    {
        new void Update(BillingItem billingItem);
        void ToggleAndSaveIsActive(int id);
        void Delete(int id);
        BillingItem UpdatePropertyPaymentType(BillingItem billingItem);
        void AddRange(List<BillingItem> billingItems);
        void ToggleActivationPaymentType(BillingItem billingItem, bool activate);
        void DeletePaymentType(int id);
        void DeleteBillingItemService(int id);
        void ToggleAndSaveIsActive(BillingItem billingItem);
    }
}
