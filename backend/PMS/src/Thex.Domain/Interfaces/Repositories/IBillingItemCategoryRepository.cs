﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Thex.Domain.Interfaces.Repositories
{
    public interface IBillingItemCategoryRepository
    {
        void ToggleAndSaveIsActive(int id);
        void BillingItemCategoryDelete(int id);
    }
}
