﻿using System;
using System.Collections.Generic;
using System.Text;
using Thex.Domain.Entities;
using Tnf.Dependency;
using Tnf.Repositories;

namespace Thex.Domain.Interfaces.Repositories
{
    public interface ILevelRepository : IRepository<Level>, IRepository, ITransientDependency, ISupportsExplicitLoading<Level>
    {
        void DeleteLevel(Level level);
        void Create(Level level);         
        void ToggleAndSaveIsActive(Guid id);
        void UpdateRange(ICollection<Level> listLevel);
    }
}
