﻿using System;
using System.Collections.Generic;

namespace Thex.Domain.Interfaces.Repositories.Read
{
    public interface IReservationBudgetReadRepository
    {
        List<long> GetAllIdByFilters(long reservationItemId, DateTime endDate);
    }
}
