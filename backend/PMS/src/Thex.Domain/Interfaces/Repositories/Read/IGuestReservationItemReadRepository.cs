﻿//  <copyright file="IGuestReservationItemReadRepository.cs" company="TOTVS">
//  Copyright (c) TOTVS. All rights reserved.
//  </copyright>
namespace Thex.Domain.Interfaces.Repositories
{
    using System;
    using System.Collections.Generic;

    using Domain.Entities;
    using Thex.Dto;
    using Tnf.Repositories;
    using Tnf.Dto;
    using Thex.Dto.GuestReservationItem;
    using System.Threading.Tasks;

    public interface IGuestReservationItemReadRepository : IRepository<GuestReservationItem>
    {
        int GetSumOfPctDailyRateItemsByReservationIdExceptId(long reservationItemId, long guestReservationItemId);
        List<GuestReservationItem> GetListGuestReservationForCheckin(List<long> ids);
        bool AnyGuestReservationItemWithStatusEqualCheckin(List<long> ids);
        ListDto<GuestReservationItemDto> GetGuestReservationItensByRoomId(long roomId);
        GuestReservationItem GetGuestReservationItemByBilligAccountId(Guid billingAccountId);
        List<GuestReservationItem> GetListGuestReservationPerReservationItemIdForCheckin(long id);
        List<long> GetAllIdByStatus(int guestStatusId);
        GuestReservationItemWithCountrySubdivisionDto GetReservationItemWithCountrySubdivision(long guestReservationItemId, string languageIsoCode);
        List<GuestReservationItem> GetAllByReservationItemId(long id);
        Task<List<PropertyDashboardGuestDto>> GetAllForPropertyDashboardAsync(DateTime systemDate, int propertyId);
    }
}
