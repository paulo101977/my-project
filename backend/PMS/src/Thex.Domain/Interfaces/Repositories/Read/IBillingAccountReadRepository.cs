﻿using System;
using System.Collections.Generic;
using Thex.Common.Enumerations;
using Thex.Domain.Entities;

namespace Thex.Domain.Interfaces.Repositories.Read
{
    public interface IBillingAccountReadRepository
    {
        BillingAccount GetByFilters(long reservationId, BillingAccountTypeEnum billingAccountTypeId);
        List<Guid> GetAllIdByFilters(List<long> guestReservationItemId, BillingAccountTypeEnum billingAccountTypeId);
        Guid GetIdByFilters(long reservationItemId, BillingAccountTypeEnum billingAccountTypeId);
    }
}
