﻿using System;
using System.Collections.Generic;

namespace Thex.Domain.Interfaces.Repositories.Read
{
    public interface IBillingAccountItemReadRepository
    {
        List<Guid> GetAllIdByBillingAccountIdList(List<Guid> billingAccountIdList);
        bool AnyByBillingAccountId(Guid billingAccountId);
    }
}
