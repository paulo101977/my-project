using System.Collections.Generic;
using Thex.Domain.Entities;
using Thex.Dto;

namespace Thex.Domain.Interfaces.Repositories.Read
{
    public interface IReservationItemReadRepository
    {
        IList<ReservationItem> GetAllByReservationId(long reservationId);
        ReservationItem GetByIdWithGuestReservationItemList(long id);
    }
}
