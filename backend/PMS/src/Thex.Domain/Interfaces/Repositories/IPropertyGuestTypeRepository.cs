﻿//  <copyright file="IPropertyGuestType.cs" company="TOTVS">
//  Copyright (c) TOTVS. All rights reserved.
//  </copyright>
namespace Thex.Domain.Interfaces.Repositories
{
    using System.Collections.Generic;
    using Thex.Domain.Entities;
    using Thex.Dto;
    using Tnf.Repositories;

    public interface IPropertyGuestTypeRepository : IRepository<PropertyGuestType>
    {
        void ToggleAndSaveIsActive(int id);
        void UpdateIsExemptOfTourismTax(List<int> idList, bool isExemptOfTourismTax);
    }
}