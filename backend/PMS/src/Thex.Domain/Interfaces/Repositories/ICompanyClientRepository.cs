﻿//  <copyright file="IRoomTypeRepository.cs" company="TOTVS">
//  Copyright (c) TOTVS. All rights reserved.
//  </copyright>
namespace Thex.Domain.Interfaces.Repositories
{
    using Thex.Domain.Entities;
    using System;
    using Tnf.Repositories;

    public interface ICompanyClientRepository : IRepository<CompanyClient>
    {
        void ToggleAndSaveIsActive(Guid id);

        new void Update(CompanyClient companyClient);
    }
}