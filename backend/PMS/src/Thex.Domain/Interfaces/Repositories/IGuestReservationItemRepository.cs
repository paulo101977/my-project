﻿//  <copyright file="IGuestReservationItemRepository.cs" company="TOTVS">
//  Copyright (c) TOTVS. All rights reserved.
//  </copyright>
namespace Thex.Domain.Interfaces.Repositories
{
    using System;
    using System.Collections.Generic;

    using Domain.Entities;
    using Thex.Dto;
    using Tnf.Repositories;
    using Tnf.Dto;
    using Thex.Common.Enumerations;

    public interface IGuestReservationItemRepository : IRepository<GuestReservationItem>
    {
        void UpdateAllGuestsToCheckout(long reservationItemId, DateTime pCheckOutDate);        
        void UpdateAllGuestsToCanceled(long guestReservationItemId);
        void UpdateAllGuestToConfirm(long reservationItemId);
        void UpdateAllGuestsToPending(long reservationItemId);
        void UpdateAllGuestsToNoShow(long reservationItemId);
        void UpdateGuestToCheckout(long id, DateTime checkOutDate);
        void UpdateAllGuestToConfirmed(long reservationItemId);
        GuestReservationItem GetById(long id);
        void ChangeGuestReservationItemStatus(long id, ReservationStatus reservationStatus, DateTime? systemDate = null);
        void UpdateGuestEstimatedDepartureDate(long guestReservationItemId, DateTime estimatedDepartureDate);
        void UpdateGuestEstimatedArrivalAndDepartureDate(long guestReservationItemId, DateTime estimatedArrivalDate, DateTime estimatedDepartureDate);
        void UpdateGuestEstimatedDepartureDate(List<GuestReservationItem> guestReservationItemList, DateTime systemDate);
        void UpdateAllGuestsToNoShow(List<long> reservationItemIdList);
        GuestReservationItem GetByGuestRegistrationId(Guid guestRegistrationId);
        void UpdateGuestsToCheckIn(List<long> ids);
        void UpdateRange(List<GuestReservationItem> guestReservationItemList);
        void UpdateAndSaveChanges(GuestReservationItem guestReservationItem);
        void UpdateGuestsToConfirmAndEstimatedArrivalDate(long reservationItemId, DateTime estimatedArrivalDate);
    }
}
