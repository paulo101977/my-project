﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Thex.Domain.Entities;
using Thex.Dto;

namespace Thex.Domain.Interfaces.Repositories
{
    public interface IPropertyBaseRateRepository
    {
        void CreateAndUpdate(List<PropertyBaseRate> PropertyBaseRateToAddList, List<PropertyBaseRate> PropertyBaseRateToUpdateList);
        Task<IQueryable<PropertyBaseRate>> GetByFiltersAsync(int propertyId, List<int> roomTypeIdList, Guid currencyId, DateTime startDate, DateTime finalDate, int mealPlanTypeDefaultId);
        void InsertHeaderHistoryRangeAndSaveChanges(ICollection<PropertyBaseRateHeaderHistory> baseRateHeaderHistoryList);
        void UpdatePropertyBaseRateMealPlanRange(IQueryable<PropertyBaseRate> propertyBaseRateList);
    }
}
