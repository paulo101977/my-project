﻿using System;
using System.Collections.Generic;
using System.Text;
using Thex.Domain.Entities;
using Tnf.Repositories;

namespace Thex.Domain.Interfaces.Repositories
{
   public interface IRatePlanCommissionRepository : IRepository<RatePlanCommission>
    {
        void RatePlanCommissionCreate(List<RatePlanCommission>  ratePlanCommissions);
    }
}
