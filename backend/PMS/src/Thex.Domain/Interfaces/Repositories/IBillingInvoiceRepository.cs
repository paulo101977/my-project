﻿// //  <copyright file="IBillingInvoiceRepository.cs" company="TOTVS">
// //  Copyright (c) TOTVS. All rights reserved.
// //  </copyright>
namespace Thex.Domain.Interfaces.Repositories
{
    using System;
    using System.Threading.Tasks;
    using Thex.Common.Enumerations;
    using Thex.Domain.Entities;
    using Tnf.Repositories;

    public interface IBillingInvoiceRepository : IRepository<BillingInvoice>
    {
        void DetachBillingInvoice(BillingInvoice billingInvoice);
        void Create(BillingInvoice billingInvoice);
        Task<BillingInvoice> GetById(Guid billingInvoiceId);
        Task SaveChanges();
        Task RemoveBillingInvoiceIdOfBillingAccountList(Guid billingInvoiceId);
        BillingInvoice GetByIdReference(Guid billingInvoiceReferenceId);
    }
}