﻿using System.Collections.Generic;
using Thex.Dto;
using Thex.Dto.PropertyCurrencyExchange;

namespace Thex.Domain.Interfaces.Repositories
{
    public interface IPropertyCurrencyExchangeRepository
    {
        void InsertAndSaveChanges(List<PropertyCurrencyExchangeDto> propertyCurrencyExchanges);
        void UpdateAndSaveChanges(PropertyCurrencyExchangeDto propertyCurrencyExchange);        
        void UpdateCurrenciesAndSaveChanges(List<QuotationCurrentDto> quotationCurrencyDtoList);
        void DeleteAllAndSaveChanges();
    }
}
