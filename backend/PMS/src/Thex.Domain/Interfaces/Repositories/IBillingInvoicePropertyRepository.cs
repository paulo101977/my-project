﻿// //  <copyright file="IBillingAccountItemRepository.cs" company="TOTVS">
// //  Copyright (c) TOTVS. All rights reserved.
// //  </copyright>
namespace Thex.Domain.Interfaces.Repositories
{
    using System;
    using System.Collections.Generic;

    using Thex.Domain.Entities;

    using Tnf.Repositories;

    public interface IBillingInvoicePropertyRepository : IRepository<BillingInvoiceProperty>
    {
        void IncrementLastNumberAndUpdate(BillingInvoiceProperty billingInvoiceProperty);
        void UpdateBillingInvoiceProperty(BillingInvoiceProperty billingInvoiceProperty);
        void SaveChanges();
        bool CompanyHasSerieByPropertyId(int propertyId, string billingInvoicePropertySeries);
        BillingInvoiceProperty GetChildByParentId(Guid parentId);
    }
}