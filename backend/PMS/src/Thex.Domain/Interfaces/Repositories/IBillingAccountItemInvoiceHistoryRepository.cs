﻿// //  <copyright file="IBillingAccountItemInvoiceHistoryRepository.cs" company="TOTVS">
// //  Copyright (c) TOTVS. All rights reserved.
// //  </copyright>
namespace Thex.Domain.Interfaces.Repositories
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Thex.Domain.Entities;

    using Tnf.Repositories;

    public interface IBillingAccountItemInvoiceHistoryRepository : IRepository<BillingAccountItemInvoiceHistory>
    {
        Task AddRangeAsync(List<BillingAccountItemInvoiceHistory> billingAccountItemInvoiceHistoryList);
    }
}