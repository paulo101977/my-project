﻿using System;
using System.Collections.Generic;
using System.Text;
using Thex.Domain.Entities;

namespace Thex.Domain.Interfaces.Repositories
{
    public interface IReasonRepository
    {
        void Delete(int id);
        void ToggleAndSaveIsActive(int id);
        void Update(Reason reason);
    }
}
