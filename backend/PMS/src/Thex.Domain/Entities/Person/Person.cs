﻿using Thex.Common.Enumerations;
using Thex.Common.Extensions;

namespace Thex.Domain.Entities
{
    public partial class Person
    {

        //private PersonTypeEnum _personType;
        //public PersonTypeEnum PersonTypeValue
        //{
        //    get
        //    {
        //        _personType = (PersonTypeEnum)Convert.ToChar(PersonTypeValue);
        //        return _personType;
        //    }
        //    internal set
        //    {
        //        _personType = value;
        //        PersonType = ((char)value).ToString();
        //    }
        //}

        //public string PersonType
        //{
        //    get;
        //    internal set;
        //}

        private PersonTypeEnum _personType;
        public PersonTypeEnum PersonTypeValue
        {
            get => _personType;
            internal set => _personType = value;
        }

        public string PersonType
        {
            get => ((char)_personType).ToString();
            internal set => _personType = value.ToEnum<PersonTypeEnum>();
        }
    }
}
