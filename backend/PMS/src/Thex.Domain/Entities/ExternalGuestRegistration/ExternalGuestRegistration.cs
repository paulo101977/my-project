﻿using System;
using Thex.Common.Enumerations;
using Tnf.Builder;
using Tnf.Notifications;

namespace Thex.Domain.Entities
{
    public class ExternalGuestRegistration : ThexMultiTenantFullAuditedEntity, IEntityGuid
    {
        public virtual Guid Id { get; set; }
        public virtual long GuestReservationItemId { get; internal set; }
        public virtual string ExternalRegistrationEmail { get; internal set; }
        
        public string FirstName { get; internal set; }
        public string LastName { get; internal set; }
        public string FullName { get; internal set; }
        public string SocialName { get; internal set; }
        public string Email { get; internal set; }
        public DateTime? BirthDate { get; internal set; }
        public string Gender { get; internal set; }
        public int? OccupationId { get; internal set; }
        public string PhoneNumber { get; internal set; }
        public string MobilePhoneNumber { get; internal set; }
        public int? NationalityId { get; internal set; }
        public int? PurposeOfTrip { get; internal set; }
        public int? ArrivingBy { get; internal set; }
        public int? ArrivingFrom { get; internal set; }
        public string ArrivingFromText { get; internal set; }
        public int? NextDestination { get; internal set; }
        public string NextDestinationText { get; internal set; }
        public string AdditionalInformation { get; internal set; }
        public string HealthInsurance { get; internal set; }
        public string DocumentInformation { get; internal set; }
        public int? DocumentTypeId { get; internal set; }
        public int? LocationCategoryId { get; internal set; }
        public decimal? Latitude { get; internal set; }
        public decimal? Longitude { get; internal set; }
        public string StreetName { get; internal set; }
        public string StreetNumber { get; internal set; }
        public string AdditionalAddressDetails { get; internal set; }
        public string Neighborhood { get; internal set; }
        public int? CityId { get; internal set; }
        public int? StateId { get; internal set; }
        public int? CountryId { get; internal set; }
        public string PostalCode { get; internal set; }
        public string CountryCode { get; internal set; }

        public ExternalGuestRegistration(long guestReservationItemId)
        {
            Id = Guid.NewGuid();
            GuestReservationItemId = guestReservationItemId;
            LocationCategoryId = (int)LocationCategoryEnum.Residential;
        }

        public void ChangeGuestRegistration(string firstName, string lastName, string fullName, string socialName, string email,
                                        DateTime? birthDate, string gender, int? occupationId, string phoneNumber, string mobilePhoneNumber,
                                        int? nationalityId, int? purposeOfTrip, int? arrivingBy, int? arrivingFrom, string arrivingFromText,
                                        int? nextDestination, string nextDestinationText, string additionalInformation, string healthInsurance,
                                        int? documentTypeId, string documentInformation,
                                        decimal? latitude, decimal? longitude, string streetName, string streetNumber, string additionalAddressDetails,
                                        string neighborhood, int? cityId, int? stateId, int? countryId, string postalCode, string countryCode)
        {
            LocationCategoryId = (int)LocationCategoryEnum.Residential;
            FirstName = firstName;
            LastName = lastName;
            FullName = fullName;
            SocialName = socialName;
            Email = email;
            BirthDate = birthDate;
            Gender = gender;
            OccupationId = occupationId;
            PhoneNumber = phoneNumber;
            MobilePhoneNumber = mobilePhoneNumber;
            NationalityId = nationalityId;
            PurposeOfTrip = purposeOfTrip;
            ArrivingBy = arrivingBy;
            ArrivingFrom = arrivingFrom;
            ArrivingFromText = arrivingFromText;
            NextDestination = nextDestination;
            AdditionalInformation = additionalInformation;
            DocumentTypeId = documentTypeId;
            DocumentInformation = documentInformation;
            HealthInsurance = healthInsurance;
            Latitude = latitude;
            Longitude = longitude;
            StreetName = streetName;
            StreetNumber = streetNumber;
            AdditionalAddressDetails = additionalAddressDetails;
            Neighborhood = neighborhood;
            CityId = cityId;
            StateId = stateId;
            CountryId = countryId;
            PostalCode = postalCode;
            CountryCode = countryCode;
        }
    }
}
