﻿using System;
using Thex.Common.Extensions;
using Thex.Kernel;

namespace Thex.Domain.Entities
{
    [Serializable]
    public abstract class ThexFullAuditedEntity// : Tnf.Repositories.Entities.ISoftDelete
    {
        public virtual bool IsDeleted { get; set; }

        public virtual DateTime CreationTime { get; set; }

        public virtual Guid? CreatorUserId { get; set; }

        public virtual DateTime? LastModificationTime { get; set; }

        public virtual Guid? LastModifierUserId { get; set; }

        public virtual DateTime? DeletionTime { get; set; }

        public virtual Guid? DeleterUserId { get; set; }

        public void SetAudit(IApplicationUser applicationUser)
        {
            this.CreationTime = DateTime.UtcNow.ToZonedDateTimeLoggedUser(applicationUser);
            this.CreatorUserId = applicationUser.UserUid;
            this.LastModificationTime = DateTime.UtcNow.ToZonedDateTimeLoggedUser(applicationUser);
            this.LastModifierUserId = applicationUser.UserUid;
        }
    }
}
