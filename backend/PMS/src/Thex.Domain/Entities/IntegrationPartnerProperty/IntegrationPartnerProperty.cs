﻿using System;
using Tnf.Notifications;

namespace Thex.Domain.Entities
{
    public partial class IntegrationPartnerProperty : IEntityGuid
    {
        public Guid Id { get; set; }

        public int PropertyId { get; internal set; }
        public bool IsActive { get; internal set; }
        public string IntegrationCode { get; internal set; }
        public int PartnerId { get; internal set; }
        public int? IntegrationPartnerId { get; internal set; }
        public string IntegrationNumber { get; internal set; }

        public virtual Property Property { get; internal set; }

        public enum EntityError
        {
            IntegrationPartnerPropertyMustHavePropertyId,
            IntegrationPartnerPropertyMustHaveIntegrationCode,
            IntegrationPartnerPropertyMustHavePartnerId,
            ExistsDuplicateIntegrationPartnerProperties,
            AlreadyExistsIntegrationPartnerPropertyWithSameIntegrationCode
        }

        public static Builder Create(INotificationHandler handler)
            => new Builder(handler);

        public static Builder Create(INotificationHandler handler, IntegrationPartnerProperty instance)
            => new Builder(handler, instance);
    }
}
