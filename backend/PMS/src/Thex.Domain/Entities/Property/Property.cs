﻿using Thex.Common.Enumerations;
using Thex.Common.Extensions;

namespace Thex.Domain.Entities
{
    public partial class Property
    {
        private PropertyTypeEnum _propertyTypeId;
        public PropertyTypeEnum PropertyTypeIdValue
        {
            get
            {
                _propertyTypeId = ((PropertyTypeEnum)PropertyTypeIdValue);
                return _propertyTypeId;
            }
            internal set
            {
                _propertyTypeId = value;
                PropertyTypeId = ((int)value);
            }
        }

        public int PropertyTypeId
        {
            get;
            internal set;
        }

    }
}
