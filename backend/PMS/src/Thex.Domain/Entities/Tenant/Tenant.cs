﻿namespace Thex.Domain.Entities
{
    public partial class Tenant
    {
        public int? ChainId { get; internal set; }
        public int? PropertyId { get; internal set; }
        public int? BrandId { get; internal set; }

        public virtual Chain Chain { get; internal set; }
        public virtual Property Property { get; internal set; }
        public virtual Brand Brand { get; internal set; }
    }
}
