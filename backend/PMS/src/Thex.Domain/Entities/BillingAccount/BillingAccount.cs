﻿using System;
using Thex.Common.Enumerations;

namespace Thex.Domain.Entities
{
    public partial class BillingAccount
    {
        public void Reopen(int reasonId)
        {
            ReopeningDate = DateTime.UtcNow.ToZonedDateTimeLoggedUser();
            ReopeningReasonId = reasonId;
            EndDate = null;
            StatusId = (int)AccountBillingStatusEnum.Opened;
            Blocked = false;
        }

        public void SetIsMainAccount(bool isMainAccount)
        {
            IsMainAccount = isMainAccount;
        }

        public void SetPersonHeaderId(Guid? personHeaderId)
        {
            PersonHeaderId = personHeaderId;
        }

        public void SetStatusClosed()
        {
            StatusId = (int)AccountBillingStatusEnum.Closed;
            EndDate = DateTime.UtcNow.ToZonedDateTimeLoggedUser();
        }

        public void SetGroupKey(Guid groupKey)
        {
            GroupKey = groupKey;
        }
    }
}
