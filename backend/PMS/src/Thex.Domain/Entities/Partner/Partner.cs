﻿using System.Collections.Generic;

namespace Thex.Domain.Entities
{
    public class Partner : IEntityInt
    {
        public int Id { get; set; }
        public string PartnerName { get; internal set; }

        public Partner()
        {
        }
    }
}
