﻿using System;
using Thex.Kernel;

namespace Thex.Domain.Entities
{
    [Serializable]
    public abstract class ThexMultiTenantFullAuditedEntity : ThexFullAuditedEntity
    {
        public Guid TenantId { get; set; }

        public Tenant Tenant { get; set; }

        public void SetTenant(IApplicationUser applicationUser)
        {
            base.SetAudit(applicationUser);
            this.TenantId = applicationUser.TenantId;
        }
    }
}
