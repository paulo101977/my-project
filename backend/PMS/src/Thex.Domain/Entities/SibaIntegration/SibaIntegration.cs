﻿using System;
using Tnf.Notifications;

namespace Thex.Domain.Entities
{
    public partial class SibaIntegration : IEntityGuid
    {
        public Guid Id { get; set; }
        public Guid PropertyUId { get; internal set; }
        public int IntegrationFileNumber { get; internal set; }

        public static Builder Create(INotificationHandler handler)
            => new Builder(handler);

        public static Builder Create(INotificationHandler handler, SibaIntegration instance)
            => new Builder(handler, instance);

        public void IncrementIntegrationFileNumber()
        {
            IntegrationFileNumber++;
        }

        public enum Error
        {
            SibaIntegrationComunicationError,
            WithoutForeignGuestToSentToSiba,
            SibaInformationError,
            PropertyInformationError
        }
    }
}
