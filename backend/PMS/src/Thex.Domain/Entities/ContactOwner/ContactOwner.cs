﻿using System;
using Tnf.Notifications;

namespace Thex.Domain.Entities
{

    public partial class ContactOwner : IEntityGuid
    {
        public Guid Id { get; set; }
        public Guid PersonId { get; internal set; }
        public Guid OwnerId { get; internal set; }
        public int? OccupationId { get; internal set; }

        public virtual Occupation Occupation { get; internal set; }
        public virtual Person Person { get; internal set; }

        public enum EntityError
        {
           
        }

        public static Builder Create(INotificationHandler handler)
            => new Builder(handler);

        public static Builder Create(INotificationHandler handler, ContactOwner instance)
            => new Builder(handler, instance);
    }
    
}
