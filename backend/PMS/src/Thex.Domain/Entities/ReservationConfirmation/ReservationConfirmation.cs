﻿namespace Thex.Domain.Entities
{
    public partial class ReservationConfirmation
    {
        public bool? ReservationItemIsConfirmed { get; set; }
    }
}
