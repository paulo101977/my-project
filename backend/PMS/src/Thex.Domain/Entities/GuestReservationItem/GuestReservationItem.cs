﻿using System;
using Thex.Common.Enumerations;
using Thex.Common.Extensions;

namespace Thex.Domain.Entities
{
    public partial class GuestReservationItem
    {
        public void SetToCheckout(DateTime checkoutDate, bool? setTime = false)
        {
            GuestStatusId = (int)ReservationStatus.Checkout;

            var currentDate = DateTime.UtcNow.ToZonedDateTimeLoggedUser();

            CheckOutDate = (setTime.HasValue && setTime.Value) ?
                                GetDateWithTime(checkoutDate, currentDate) :
                                checkoutDate;

            SetCheckoutHour(currentDate);
        }

        private void SetCheckoutHour(DateTime currentDate)
        {
            var checkoutDate = CheckOutDate.Value;

            CheckOutHour = GetDateWithTime(checkoutDate, currentDate);
        }

        private DateTime GetDateWithTime(DateTime checkoutDate, DateTime currentDate)
            => new DateTime(checkoutDate.Year,
                            checkoutDate.Month,
                            checkoutDate.Day,
                            currentDate.Hour,
                            currentDate.Minute,
                            currentDate.Second);
        public void SetToCanceled()
        {
            GuestStatusId = (int)ReservationStatus.Canceled;
        }

        public void SetToConfirm()
        {
            GuestStatusId = (int)ReservationStatus.ToConfirm;
        }

        public void SetToNoShow()
        {
            GuestStatusId = (int)ReservationStatus.NoShow;
        }

        public void SetToPending()
        {
            GuestStatusId = (int)ReservationStatus.Pending;
        }

        public void SetToConfirmed()
        {
            GuestStatusId = (int)ReservationStatus.Confirmed;
        }

        public void CancelCheckin()
        {
            SetToConfirm();
            CheckInDate = null;
            CheckInHour = null;
            GuestRegistrationId = null;
            GuestId = null;
            GuestEmail = null;
            GuestDocumentTypeId = null;
            GuestDocument = null;
        }

        public void ChangeGuestInformation(string guestEmail, string guestDocument, int? guestDocumentTypeId, string guestName, string preferredName)
        {
            GuestEmail = guestEmail;
            GuestDocument = guestDocument;
            GuestDocumentTypeId = guestDocumentTypeId;
            GuestName = guestName;
            PreferredName = preferredName;
        }

        public enum Error
        {
            GuestReservationBudgetChanged = 1,
        }

        public bool CanChangeByExternalRegistration()
            => GuestStatusId != (int)ReservationStatus.Confirmed && GuestStatusId != (int)ReservationStatus.ToConfirm;
    }
}
