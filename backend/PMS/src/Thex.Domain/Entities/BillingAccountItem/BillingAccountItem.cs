﻿using System;
using Thex.Common.Enumerations;

namespace Thex.Domain.Entities
{
    public partial class BillingAccountItem
    {
        public void SetToReversed()
        {
            WasReversed = true;
        }

        public void SetReason(int reasonId)
        {
            BillingAccountItemReasonId = reasonId;
        }

        public void SetObservation(string observation)
        {
            BillingAccountItemObservation = observation;
        }

        public void SetBillingInvoice(Guid billingInvoiceId)
        {
            BillingInvoiceId = billingInvoiceId;
        }

        public void RemoveBillingInvoice(string timeZone)
        {
            LastModificationTime = DateTime.UtcNow.ToZonedDateTime(timeZone);
            BillingInvoiceId = null;
        }

        public void SetToPartial(decimal amount, Guid partialParentBillingAccountItemId)
        {
            BillingAccountItemTypeIdLastSource = BillingAccountItemTypeId != (int)BillingAccountItemTypeEnum.BillingAccountItemTypePartial 
                                    ? BillingAccountItemTypeId : BillingAccountItemTypeIdLastSource;
            BillingAccountItemTypeId = (int)BillingAccountItemTypeEnum.BillingAccountItemTypePartial;
            PartialParentBillingAccountItemId = partialParentBillingAccountItemId;
            Amount = amount;
            IsOriginal = true;
        }

        public void SetTypeIdAndTypeIdLastSource(int billingAccountItemTypeId, int? billingAccountItemTypeIdLastSource)
        {
            BillingAccountItemTypeId = billingAccountItemTypeId;
            BillingAccountItemTypeIdLastSource = billingAccountItemTypeIdLastSource;
        }

        public void SetAccountIdLastSourceAndAccountId(Guid billingAccountId, Guid billingAccountIdLastSource)
        {
            BillingAccountIdLastSource = billingAccountId;
            BillingAccountId = billingAccountIdLastSource;
        }

        public void SetCreditAmount(decimal creditAmount)
        {
            if (CreditAmount == null)
                CreditAmount = creditAmount;
            else
                CreditAmount += creditAmount;
        }

        public void SetPaymentId(string paymentId)
        {
            IntegrationPaymentId = paymentId;
        }
    }
}
