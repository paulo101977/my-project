﻿using Tnf.Notifications;

namespace Thex.Domain.Entities
{
    public partial class IntegrationPartner : IEntityInt
    {
        public int Id { get; set; }
        public string TokenClient { get; internal set; }
        public string TokenApplication { get; internal set; }
        public string IntegrationPartnerName { get; internal set; }
        public int IntegrationPartnerType { get; internal set; }
        public bool IsActive { get; internal set; }

        public enum EntityError
        {
            IntegrationPartnerMustHaveIntegrationPartnerName,
            IntegrationPartnerMustHaveIntegrationPartnerType,
            IntegrationPartnerTypeNotFound
        }

        public static Builder Create(INotificationHandler handler)
           => new Builder(handler);

        public static Builder Create(INotificationHandler handler, IntegrationPartner instance)
            => new Builder(handler, instance);
    }
}