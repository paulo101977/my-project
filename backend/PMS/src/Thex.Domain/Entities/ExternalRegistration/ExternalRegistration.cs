﻿using System;
using System.Collections.Generic;
using Tnf.Builder;
using Tnf.Notifications;

namespace Thex.Domain.Entities
{
    public class ExternalRegistration : ThexMultiTenantFullAuditedEntity, IEntityGuid
    {
        public virtual Guid Id { get; set; }
        public virtual string Email { get; internal set; }
        public virtual string Link { get; internal set; }
        public virtual Guid ReservationUid { get; internal set; }

        public List<ExternalGuestRegistration> ExternalGuestRegistrationList { get; private set; }

        public ExternalRegistration(Guid reservationUid, string email, string link)
        {
            Id = Guid.NewGuid();
            ReservationUid = reservationUid;
            Email = email;
            Link = link;
        }

        public void AddExternalGuestRegistrationList(List<long> guestReservationItemIdList)
        {
            if (guestReservationItemIdList == null)
                return;

            ExternalGuestRegistrationList = new List<ExternalGuestRegistration>();

            foreach (var guestReservationItemId in guestReservationItemIdList)
                ExternalGuestRegistrationList.Add(new ExternalGuestRegistration(guestReservationItemId));
        }
    }
}
