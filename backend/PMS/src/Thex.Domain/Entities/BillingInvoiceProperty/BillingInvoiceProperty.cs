﻿using System;

namespace Thex.Domain.Entities
{
    public partial class BillingInvoiceProperty
    {
        public void IncrementLastNumber()
        {
            LastNumber = (Convert.ToInt32(LastNumber) + 1).ToString();
        }
    }
}
