﻿using System;

namespace Thex.Domain.Entities
{
    public partial class Room
    {
        public DateTime? HousekeepingStatusLastModificationTime { get; set; }
    }
}
