﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Thex.Domain.Entities
{
    public class PropertyBaseRateToCreate
    {
        public PropertyBaseRateHeaderHistory PropertyBaseRateHeaderHistory { get; set; }
        public ICollection<PropertyBaseRate> PropertyBaseRateListToAdd { get; set; }
        public ICollection<PropertyBaseRate> PropertyBaseRateListToUpdate { get; set; }
        public IQueryable<PropertyBaseRate> PropertyBaseRateMealPlanListToUpdate { get; set; }
    }
}
