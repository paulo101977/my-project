﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a Crudzilla tool.
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------



using Tnf.Notifications;

namespace Thex.Domain.Entities
{
    public partial class PropertyGuestPrefs: ThexMultiTenantFullAuditedEntity, IEntityInt
    {
        public int Id { get; set; }
        public long GuestId { get; internal set; }
        public int PropertyId { get; internal set; }
        public int? PreferredRoomId { get; internal set; }
        public int? LastRoomId { get; internal set; }

        public virtual Guest Guest { get; internal set; }
        public virtual Room LastRoom { get; internal set; }
        public virtual Room PreferredRoom { get; internal set; }
        public virtual Property Property { get; internal set; }

        public enum EntityError
        {
            PropertyGuestPrefsMustHaveGuestId,
            PropertyGuestPrefsMustHavePropertyId
        }

        public static Builder Create(INotificationHandler handler)
            => new Builder(handler);

        public static Builder Create(INotificationHandler handler, PropertyGuestPrefs instance)
            => new Builder(handler, instance);
    }
}
