﻿using System;
using Tnf.Notifications;

namespace Thex.Domain.Entities
{
    public partial class CompanyClientChannel : IEntityGuid
    {
        public Guid Id { get; set; }
        public Guid ChannelId { get; internal set; }
        public Guid CompanyClientId { get; internal set; }
        public bool IsActive { get; internal set; }
        public string CompanyId { get; internal set; }

        public Channel Channel { get; internal set; }
        public CompanyClient CompanyClient { get; internal set; }
      
        public enum EntityError
        {
            CompanyClientChannelMustHaveChannelId,
            CompanyClientChannelMustHaveCompanyClientId,
            CompanyClientChannelMustHaveCompanyId,
            CompanyClientChannelNotMustHaveAssociationsDuplicates,
            AlreadyExistsCompanyClientChannelWithSameCompanyId,
            CompanyClientExternalCodeUnavailable
        }

        public static Builder Create(INotificationHandler handler)
           => new Builder(handler);

        public static Builder Create(INotificationHandler handler, CompanyClientChannel instance)
            => new Builder(handler, instance);
    }
}
