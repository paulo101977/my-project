﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a Crudzilla tool.
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using Tnf.Notifications;

namespace Thex.Domain.Entities
{
    public partial class Occupation : IEntityInt
    {
        public Occupation()
        {
            CompanyClientContactPersonList = new HashSet<CompanyClientContactPerson>();            
            CompanyContactPersonList = new HashSet<CompanyContactPerson>();
            EmployeeList = new HashSet<Employee>();
            PropertyContactPersonList = new HashSet<PropertyContactPerson>();
            ContactOwnerList = new HashSet<ContactOwner>();
        }

        public int Id { get; set; }
        public string Name { get; internal set; }
        public int CompanyId { get; internal set; }

        public virtual Company Company { get; internal set; }
        public virtual ICollection<CompanyClientContactPerson> CompanyClientContactPersonList { get; internal set; }
        public virtual ICollection<CompanyContactPerson> CompanyContactPersonList { get; internal set; }
        public virtual ICollection<Employee> EmployeeList { get; internal set; }
        public virtual ICollection<PropertyContactPerson> PropertyContactPersonList { get; internal set; }
        public virtual ICollection<ContactOwner> ContactOwnerList { get; internal set; }

        public enum EntityError
        {
            OccupationMustHaveName,
            OccupationOutOfBoundName,
            OccupationMustHaveCompanyId
        }

        public static Builder Create(INotificationHandler handler)
            => new Builder(handler);

        public static Builder Create(INotificationHandler handler, Occupation instance)
            => new Builder(handler, instance);
    }
}
