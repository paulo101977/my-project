﻿using System;
using Tnf.Notifications;

namespace Thex.Domain.Entities
{
    public partial class PropertyContract : ThexFullAuditedEntity, IEntityGuid
    {
        public Guid Id { get; set; }
        public int PropertyId { get; internal set; }
        public int MaxUh { get; internal set; }

        public virtual Property Property { get; internal set; }

        public enum EntityError
        {
            PropertyContractMustHavePropertyId,
            PropertyContractMustHaveMaxUh,
            PropertyMustHaveContract
        }

        public static Builder Create(INotificationHandler handler)
            => new Builder(handler);

        public static Builder Create(INotificationHandler handler, PropertyContract instance)
            => new Builder(handler, instance);
    }
}
