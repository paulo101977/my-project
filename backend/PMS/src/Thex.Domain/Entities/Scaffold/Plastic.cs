﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a Crudzilla tool.
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using Tnf.Notifications;

namespace Thex.Domain.Entities
{
    public partial class Plastic : IEntityLong
    {
        public Plastic()
        {
            ReservationConfirmationList = new HashSet<ReservationConfirmation>();
        }
        public long Id { get; set; }
        public Guid? PersonId { get; internal set; }
        public string Holder { get; internal set; }
        public string PlasticNumber { get; internal set; }
        public string ExpirationDate { get; internal set; }
        public short Csc { get; internal set; }
        public int PlasticBrandId { get; internal set; }

        public virtual PlasticBrand PlasticBrand { get; internal set; }
        public virtual ICollection<ReservationConfirmation> ReservationConfirmationList { get; internal set; }

        public enum EntityError
        {
            PlasticMustHaveHolder,
            PlasticOutOfBoundHolder,
            PlasticMustHavePlasticNumber,
            PlasticOutOfBoundPlasticNumber,
            PlasticMustHaveExpirationDate,
            PlasticOutOfBoundExpirationDate,
            PlasticMustHavePlasticBrandId
        }

        public static Builder Create(INotificationHandler handler)
            => new Builder(handler);

        public static Builder Create(INotificationHandler handler, Plastic instance)
            => new Builder(handler, instance);
    }
}
