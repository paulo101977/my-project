﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a Crudzilla tool.
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using Tnf.Notifications;

namespace Thex.Domain.Entities
{
    public partial class CurrencyExchange : ThexFullAuditedEntity, IEntityGuid
    {
        public Guid Id { get; set; }
        public DateTime CurrencyExchangeDate { get; internal set; }
        public Guid CurrencyId { get; internal set; }
        public decimal ExchangeRate { get; internal set; }

        public virtual Currency Currency { get; internal set; }

        public enum EntityError
        {
            CurrencyExchangeInvalidCurrencyExchangeDate,
            CurrencyExchangeMustHaveCurrencyId
        }

        public static Builder Create(INotificationHandler handler)
            => new Builder(handler);

        public static Builder Create(INotificationHandler handler, CurrencyExchange instance)
            => new Builder(handler, instance);
    }
}
