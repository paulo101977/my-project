﻿using System;
using System.Collections.Generic;
using Tnf.Notifications;

namespace Thex.Domain.Entities
{
    public partial class Currency : ThexFullAuditedEntity, IEntityGuid
    {
        public Currency()
        {
            BillingAccountItemList = new HashSet<BillingAccountItem>();
            CountryCurrencyList = new HashSet<CountryCurrency>();
            CurrencyExchangeList = new HashSet<CurrencyExchange>();
            PropertyBaseRateList = new HashSet<PropertyBaseRate>();
            PropertyCurrencyExchangeList = new HashSet<PropertyCurrencyExchange>();
            RatePlanList = new HashSet<RatePlan>();
            ReservationBudgetList = new HashSet<ReservationBudget>();
            ReservationItemList = new HashSet<ReservationItem>();
            PropertyBaseRateHistoryList = new HashSet<PropertyBaseRateHistory>();
            PropertyBaseRateHeaderHistoryList = new HashSet<PropertyBaseRateHeaderHistory>();
        }

        public Guid Id { get; set; }

        public int CountrySubdivisionId { get; internal set; }
        public string TwoLetterIsoCode { get; internal set; }
        public string CurrencyName { get; internal set; }
        public string AlphabeticCode { get; internal set; }
        public string NumnericCode { get; internal set; }
        public int MinorUnit { get; internal set; }
        public string Symbol { get; internal set; }
        public decimal ExchangeRate { get; internal set; }

        public virtual CountrySubdivision CountrySubdivision { get; internal set; }
        public virtual ICollection<BillingAccountItem> BillingAccountItemList { get; internal set; }
        public virtual ICollection<CountryCurrency> CountryCurrencyList { get; internal set; }
        public virtual ICollection<CurrencyExchange> CurrencyExchangeList { get; internal set; }
        public virtual ICollection<PropertyBaseRate> PropertyBaseRateList { get; internal set; }
        public virtual ICollection<PropertyCurrencyExchange> PropertyCurrencyExchangeList { get; internal set; }
        public virtual ICollection<RatePlan> RatePlanList { get; internal set; }
        public virtual ICollection<ReservationBudget> ReservationBudgetList { get; internal set; }
        public virtual ICollection<ReservationItem> ReservationItemList { get; internal set; }
        public virtual ICollection<PropertyBaseRateHistory> PropertyBaseRateHistoryList { get; internal set; }
        public virtual ICollection<PropertyBaseRateHeaderHistory> PropertyBaseRateHeaderHistoryList { get; internal set; }
        public virtual ICollection<LevelRateHeader> LevelRateHeaderList { get; set; }

        public enum EntityError
        {
            CurrencyMustHaveCountrySubdivisionId,
            CurrencyOutOfBoundTwoLetterIsoCode,
            CurrencyMustHaveCurrencyName,
            CurrencyOutOfBoundCurrencyName,
            CurrencyMustHaveAlphabeticCode,
            CurrencyOutOfBoundAlphabeticCode,
            CurrencyMustHaveNumnericCode,
            CurrencyOutOfBoundNumnericCode,
            CurrencyMustHaveSymbol,
            CurrencyOutOfBoundSymbol
        }

        public static Builder Create(INotificationHandler handler)
            => new Builder(handler);

        public static Builder Create(INotificationHandler handler, Currency instance)
            => new Builder(handler, instance);
    }
}
