﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a Crudzilla tool.
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using Tnf.Notifications;

namespace Thex.Domain.Entities
{
    public partial class PropertyCurrencyExchange : ThexMultiTenantFullAuditedEntity, IEntityGuid
    {
        public Guid Id { get; set; }
        public int PropertyId { get; internal set; }
        public DateTime PropertyCurrencyExchangeDate { get; internal set; }
        public Guid CurrencyId { get; internal set; }
        public decimal ExchangeRate { get; internal set; }

        public virtual Currency Currency { get; internal set; }
        public virtual Property Property { get; internal set; }

        public enum EntityError
        {
            PropertyCurrencyExchangeMustHavePropertyId,
            PropertyCurrencyExchangeInvalidPropertyCurrencyExchangeDate,
            PropertyCurrencyExchangeMustHaveCurrencyId,
            PropertyCurrencyExchangeIsExistQuotationForPeriod,
            PropertyCurrencyExchangeIsOnlyDateFuture,
            PropertyCurrencyExchangeIsEqualToCurrencyQuotation,
            PropertyCurrencyExchangeQuotationListIsNull,
            PropertyCurrencyExchangeRateDoesNotExistForPeriod,
            PropertyCurrencyExchangeInvalidDate,
            PropertyCurrencyExchangeRateDoesNotExistForDate,
            PropertyCurrencyExchangeRateMustHaveValue
        }

        public static Builder Create(INotificationHandler handler)
            => new Builder(handler);

        public static Builder Create(INotificationHandler handler, PropertyCurrencyExchange instance)
            => new Builder(handler, instance);
    }
}
