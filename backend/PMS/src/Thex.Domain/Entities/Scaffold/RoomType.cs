﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a Crudzilla tool.
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using Tnf.Notifications;

namespace Thex.Domain.Entities
{
    public partial class RoomType : IEntityInt
    {
        public RoomType()
        {
            PropertyBaseRateHistoryList = new HashSet<PropertyBaseRateHistory>();
            PropertyBaseRateList = new HashSet<PropertyBaseRate>();
            //RatePlanList = new HashSet<RatePlan>();
            RatePlanRoomTypeList = new HashSet<RatePlanRoomType>();
            ReservationItemReceivedRoomTypeList = new HashSet<ReservationItem>();
            ReservationItemRequestedRoomTypeList = new HashSet<ReservationItem>();
            RoomList = new HashSet<Room>();
            RoomTypeBedTypeList = new HashSet<RoomTypeBedType>();
            RoomTypeInventoryList = new HashSet<RoomTypeInventory>();
            PropertyRateStrategyRoomTypeList = new HashSet<PropertyRateStrategyRoomType>();
        }

        public int Id { get; set; }
        public int PropertyId { get; internal set; }
        public int Order { get; internal set; }
        public string Name { get; internal set; }
        public string Abbreviation { get; internal set; }
        public int AdultCapacity { get; internal set; }
        public int ChildCapacity { get; internal set; }
        public int FreeChildQuantity1 { get; internal set; }
        public int FreeChildQuantity2 { get; internal set; }
        public int FreeChildQuantity3 { get; internal set; }
        public bool IsActive { get; internal set; }
        public decimal MaximumRate { get; internal set; }
        public decimal MinimumRate { get; internal set; }
        public string DistributionCode { get; internal set; }


        public virtual Property Property { get; internal set; }
        public virtual ICollection<PropertyBaseRateHistory> PropertyBaseRateHistoryList { get; internal set; }
        public virtual ICollection<PropertyBaseRate> PropertyBaseRateList { get; internal set; }
        //public virtual ICollection<RatePlan> RatePlanList { get; internal set; }
        public virtual ICollection<RatePlanRoomType> RatePlanRoomTypeList { get; internal set; }
        public virtual ICollection<ReservationItem> ReservationItemReceivedRoomTypeList { get; internal set; }
        public virtual ICollection<ReservationItem> ReservationItemRequestedRoomTypeList { get; internal set; }
        public virtual ICollection<Room> RoomList { get; internal set; }
        public virtual ICollection<RoomTypeBedType> RoomTypeBedTypeList { get; internal set; }
        public virtual ICollection<RoomTypeInventory> RoomTypeInventoryList { get; internal set; }
        public virtual ICollection<PropertyRateStrategyRoomType> PropertyRateStrategyRoomTypeList { get; internal set; }

        public enum EntityError
        {
            RoomTypeMustHavePropertyId,
            RoomTypeMustHaveName,
            RoomTypeOutOfBoundName,
            RoomTypeMustHaveAbbreviation,
            RoomTypeOutOfBoundAbbreviation,
            RoomTypeMustHaveOrder,
            RoomTypeMustHaveAdultCapacity,
            RoomTypeHasReserveForward,
            RoomTypeShouldNotDuplicateAbreviation,
            RoomTypeMustHaveBedTypes,
            RoomTypeMinimumRateLargerMaximumRate,
            RoomTypeRateInvalid,
            RoomTypeCountFreeChildrenLargerCapacity,
            AlreadyExistsRoomTypeWithSameDistributionCode,
            RoomTypeIsInCheckIn,
            RoomTypeErrorRemove
        }

        public static Builder Create(INotificationHandler handler)
            => new Builder(handler);

        public static Builder Create(INotificationHandler handler, RoomType instance)
            => new Builder(handler, instance);
    }
}
