﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a Crudzilla tool.
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using Tnf.Notifications;

namespace Thex.Domain.Entities
{
    public partial class PlasticBrand : IEntityInt
    {
        public PlasticBrand()
        {
            PlasticList = new HashSet<Plastic>();
            PlasticBrandPropertyList = new HashSet<PlasticBrandProperty>();
        }

        public int Id { get; set; }

        public string PlasticBrandName { get; internal set; }

        public string IconName { get; internal set; }

        public Guid? PlasticBrandUid { get; set; }

        public virtual ICollection<Plastic> PlasticList { get; internal set; }
        public virtual ICollection<PlasticBrandProperty> PlasticBrandPropertyList { get; internal set; }

        public enum EntityError
        {
            PlasticBrandMustHavePlasticBrandName,
            PlasticBrandOutOfBoundPlasticBrandName
        }

        public static Builder Create(INotificationHandler handler)
            => new Builder(handler);

        public static Builder Create(INotificationHandler handler, PlasticBrand instance)
            => new Builder(handler, instance);
    }
}
