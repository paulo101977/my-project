﻿using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Text;
using Tnf.Notifications;

namespace Thex.Domain.Entities
{
    public partial class GuestRegistrationDocument : ThexFullAuditedEntity, IEntityGuid
    {
        public Guid Id { get; set; }
        public int DocumentTypeId { get; internal set; }
        public Guid GuestRegistrationId { get; internal set; }
        public string DocumentInformation { get; internal set; }

        public virtual DocumentType DocumentType { get; internal set; }
        public virtual GuestRegistration GuestRegistration { get; internal set; }

        public enum EntityError
        {
            GuestRegistrationDocumentMustHaveGuestRegistrationId,
            GuestRegistrationDocumentMustHaveDocumentTypeId,
            GuestRegistrationDocumentMustHaveDocumentInformation,
            GuestRegistrationDocumentOutOfBoundDocumentInformation,
            GuestRegistrationDocumentInvalid,
            GuestRegistrationDocumentCpfInvalid,
            GuestRegistrationDocumentCnpjInvalid,
            GuestRegistrationDocumentListHasDuplicates,
            GuestRegistrationDocumentEqualToMainDocumentGuestRegistration
        }

        public static Builder Create(INotificationHandler handler)
            => new Builder(handler);

        public static Builder Create(INotificationHandler handler, GuestRegistrationDocument instance)
            => new Builder(handler, instance);
    }
}
