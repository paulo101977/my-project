﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a Crudzilla tool.
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using Tnf.Notifications;

namespace Thex.Domain.Entities
{
    public partial class RatePlanRoomType : ThexMultiTenantFullAuditedEntity, IEntityGuid
    {
        public Guid Id { get; set; }
        public Guid RatePlanId { get; internal set; }
        public int RoomTypeId { get; internal set; }
        public decimal PercentualAmmount { get; internal set; }
        public bool IsDecreased { get; internal set; }
        //public bool PublishOnline { get; internal set; }

        public virtual RatePlan RatePlan { get; internal set; }
        public virtual RoomType RoomType { get; internal set; }

        public enum EntityError
        {
            RatePlanRoomTypeMustHaveRatePlanId,
            RatePlanRoomTypeMustHaveRoomTypeId,
            RatePlanRoomTypeNotNegative
        }

        public static Builder Create(INotificationHandler handler)
            => new Builder(handler);

        public static Builder Create(INotificationHandler handler, RatePlanRoomType instance)
            => new Builder(handler, instance);
    }
}
