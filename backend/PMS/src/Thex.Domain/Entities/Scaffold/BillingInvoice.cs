﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a Crudzilla tool.
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using Tnf.Notifications;

namespace Thex.Domain.Entities
{
    public partial class BillingInvoice : ThexMultiTenantFullAuditedEntity, IEntityGuid
    {
        public BillingInvoice()
        {
            BillingAccountItemList = new HashSet<BillingAccountItem>();
            BillingAccountItemInvoiceHistoryList = new HashSet<BillingAccountItemInvoiceHistory>();
        }
        public Guid Id { get; set; }
        public string BillingInvoiceNumber { get; internal set; }
        public string BillingInvoiceSeries { get; internal set; }
        public DateTime EmissionDate { get; internal set; }
        public DateTime? CancelDate { get; internal set; }
        public Guid? EmissionUserId { get; internal set; }
        public Guid? CancelUserId { get; internal set; }
        public int PropertyId { get; internal set; }
        public string ExternalRps { get; internal set; }
        public string ExternalNumber { get; internal set; }
        public string ExternalSeries { get; internal set; }
        public DateTime? ExternalEmissionDate { get; internal set; }
        public Guid BillingAccountId { get; internal set; }
        public string AditionalDetails { get; internal set; }
        public Guid BillingInvoicePropertyId { get; internal set; }
        public int? BillingInvoiceCancelReasonId { get; internal set; }

        public string IntegratorLink { get; internal set; }
        public string IntegratorXml { get; internal set; }
        public string IntegratorId { get; internal set; }
        public int BillingInvoiceStatusId { get; internal set; }

        public string ExternalCodeNumber { get; internal set; }
        public DateTime? ErrorDate { get; internal set; }
        public Guid? BillingInvoiceReferenceId { get; internal set; }
        public Guid? PersonHeaderId { get; internal set; }
        public int? BillingInvoiceTypeId { get; internal set; }
        public string IntegrationCode { get; internal set; }
        public Guid? BillingInvoiceCreditId { get; internal set; }

        public virtual BillingAccount BillingAccount { get; internal set; }
        public virtual Reason BillingInvoiceCancelReason { get; internal set; }
        public virtual BillingInvoiceProperty BillingInvoiceProperty { get; internal set; }
        public virtual Property Property { get; internal set; }
        public virtual Status BillingInvoiceStatus { get; internal set; }
        public virtual Person PersonHeader { get; internal set; }
        public virtual BillingInvoice BillingInvoiceReference { get; internal set; }
        public virtual BillingInvoiceType BillingInvoiceType { get; internal set; }
        public virtual ICollection<BillingInvoice> ChildBillingInvoiceList { get; internal set; }
        public virtual ICollection<BillingInvoice> CreditBillingInvoiceList { get; internal set; }
        public virtual BillingInvoice BillingInvoiceCredit { get; internal set; }

        public virtual ICollection<BillingAccountItem> BillingAccountItemList { get; internal set; }
        public virtual ICollection<BillingAccountItemInvoiceHistory> BillingAccountItemInvoiceHistoryList { get; internal set; }

        public enum EntityError
        {
            BillingInvoiceMustHaveBillingInvoiceNumber,
            BillingInvoiceOutOfBoundBillingInvoiceNumber,
            BillingInvoiceMustHaveBillingInvoiceSeries,
            BillingInvoiceOutOfBoundBillingInvoiceSeries,
            BillingInvoiceInvalidEmissionDate,
            BillingInvoiceInvalidCancelDate,
            BillingInvoiceMustHavePropertyId,
            BillingInvoiceOutOfBoundExternalNumber,
            BillingInvoiceOutOfBoundExternalSeries,
            BillingInvoiceInvalidExternalEmissionDate,
            BillingInvoiceMustHaveBillingAccountId,
            BillingInvoiceOutOfBoundAditionalDetails,
            BillingInvoiceMustHaveBillingInvoicePropertyId,
            BillingInvoiceOutOfBoundIntegratorLink,
            BillingInvoiceOutOfBoundIntegratorXml,
            BillingInvoiceOutOfBoundIntegratorId,
            BillingInvoiceFacturaOnlyIssueCreditNote,
            BillingInvoiceFacturaAlreadyIssuedCreditNote,
            BillingInvoiceCreditNoteValueIsGreaterThanAllowed,
            BillingInvoiceCreditNoteValueCanNotBeLessThanOrEqualToZero,
            BillingInvoiceCreditNoteEmptyPaymentType,
            BillingInvoiceCreditNoteHasReason,
            BillingInvoiceDoesNotIssued,
            BillingInvoiceMustHaveClientName,
            BillingInvoiceWithInvalidParterPDFGenerator
        }

        public enum EntityMessage
        {
            BillingInvoiceIntegrationResponse,
            BillingInvoiceIntegrationInvalidIntegrationResponse
        }

        public static Builder Create(INotificationHandler handler)
            => new Builder(handler);

        public static Builder Create(INotificationHandler handler, BillingInvoice instance)
            => new Builder(handler, instance);
    }
}
