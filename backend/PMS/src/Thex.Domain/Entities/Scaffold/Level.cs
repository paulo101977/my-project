﻿using System;
using System.Collections.Generic;
using Tnf.Notifications;

namespace Thex.Domain.Entities
{
    public partial class Level : ThexMultiTenantFullAuditedEntity, IEntityGuid
    {
        public Guid Id { get; set; }
        public int LevelCode { get; internal set; }
        public string Description { get; internal set; }
        public int Order { get; internal set; }
        public bool IsActive { get; internal set; }
        public int PropertyId { get; internal set; }

        public virtual Property Property { get; internal set; }

        public virtual ICollection<LevelRateHeader> LevelRateHeaderList { get; internal set; }
        public virtual ICollection<PropertyBaseRate> PropertyBaseRateList { get; internal set; }

        public Level()
        {
            this.LevelRateHeaderList = new HashSet<LevelRateHeader>();
        }

        public enum EntityError
        {
            LevelCodeDuplicate,
            LevelMustHavePermition,
            NoExistentLevelForUpdating,
            IdNoExistentForLevelCode,
            LevelCodeRequired,
            DescriptionRequired,
            OrderRequired,
            IdNotInformed,
            IdDouble,
            LevelCodeItemOutOfBound,
            DescriptionItemOutOfBound,
            LevelCanNotBeRemovedBecauseItIsBeingUsed
        }

        public static Builder Create(INotificationHandler handler)
       => new Builder(handler);

        public static Builder Create(INotificationHandler handler, Level instance)
            => new Builder(handler, instance);

    }
}
