﻿
using System;
using Tnf.Notifications;

namespace Thex.Domain.Entities
{
    public partial class PropertyDocumentType : IEntityGuid
    {
        public Guid Id { get; set; }
        public int PropertyId { get; internal set; }
        public int DocumentTypeId { get; internal set; }

       public virtual Property Property { get; internal set; }

        public virtual DocumentType DocumentType { get; internal set; }


        public static Builder Create(INotificationHandler handler)
            => new Builder(handler);

        public static Builder Create(INotificationHandler handler, PropertyDocumentType instance)
            => new Builder(handler, instance);
    }
}