﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a Crudzilla tool.
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using Tnf.Notifications;

namespace Thex.Domain.Entities
{
    public partial class RoomLayout : IEntityGuid
    {
        public RoomLayout()
        {
            ReservationItemList = new HashSet<ReservationItem>();
        }

        public Guid Id { get; set; }
        public byte QuantitySingle { get; internal set; }
        public byte QuantityDouble { get; internal set; }

        public virtual ICollection<ReservationItem> ReservationItemList { get; internal set; }

        public static Builder Create(INotificationHandler handler)
            => new Builder(handler);

        public static Builder Create(INotificationHandler handler, RoomLayout instance)
            => new Builder(handler, instance);
    }
}
