﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a Crudzilla tool.
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using Tnf.Notifications;

namespace Thex.Domain.Entities
{
    public partial class PropertyParameter : ThexMultiTenantFullAuditedEntity, IEntityGuid
    {
        public Guid Id { get; set; }
        public int PropertyId { get; internal set; }
        public int ApplicationParameterId { get; internal set; }
        public string PropertyParameterValue { get; internal set; }
        public string PropertyParameterMinValue { get; internal set; }
        public string PropertyParameterMaxValue { get; internal set; }
        public string PropertyParameterPossibleValues { get; internal set; }
        public bool IsActive { get; internal set; }

        public virtual ApplicationParameter ApplicationParameter { get; internal set; }
        public virtual Property Property { get; internal set; }

        public enum EntityError
        {
            PropertyParameterMustHavePropertyId,
            PropertyParameterMustHaveApplicationParameterId,
            PropertyParameterOutOfBoundPropertyParameterMinValue,
            PropertyParameterOutOfBoundPropertyParameterMaxValue,
            PropertyParameterHourFormatInvalid,
            PropertyParameterCheckoutLargerCheckin,
            PropertyParameterAgeRangeInvalidad,
            PropertyParameterIsNotDeactivateChildren_2,
            PropertyParameterMustHaveDailyBillingItem,
            PropertyParameterMustHaveTimeZone,
            PropertyParameterDoesNotChangeCurrencyIfExistsAnyBillingAccountItem,
            PropertyParameterMustHaveDiffDailyBillingItem,
            GroupKeyInvalid
        }

        public static Builder Create(INotificationHandler handler)
            => new Builder(handler);

        public static Builder Create(INotificationHandler handler, PropertyParameter instance)
            => new Builder(handler, instance);
    }
}
