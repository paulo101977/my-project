﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a Crudzilla tool.
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using Tnf.Notifications;

namespace Thex.Domain.Entities
{
    public partial class PropertyMealPlanType : ThexMultiTenantFullAuditedEntity, IEntityGuid
    {
        public PropertyMealPlanType()
        {
        }
        public Guid Id { get; set; }
        public string PropertyMealPlanTypeCode { get; internal set; }
        public string PropertyMealPlanTypeName { get; internal set; }
        public int PropertyId { get; internal set; }
        public int MealPlanTypeId { get; internal set; }
        public bool? IsActive { get; internal set; }

        public virtual MealPlanType MealPlanType { get; internal set; }
        public virtual Property Property { get; internal set; }

        public enum EntityError
        {
            PropertyMealPlanTypeOutOfBoundPropertyMealPlanTypeCode,
            PropertyMealPlanTypeOutOfBoundPropertyMealPlanTypeName,
            PropertyMealPlanTypeMustHavePropertyId,
            PropertyMealPlanTypeMustHaveMealPlanTypeId,
            CanNotDeactiveAllMealPlanTypes
        }

        public static Builder Create(INotificationHandler handler)
            => new Builder(handler);

        public static Builder Create(INotificationHandler handler, PropertyMealPlanType instance)
            => new Builder(handler, instance);
    }
}
