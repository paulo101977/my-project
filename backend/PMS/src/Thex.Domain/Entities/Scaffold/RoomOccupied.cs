﻿using System;
using System.Collections.Generic;
using Tnf.Notifications;

namespace Thex.Domain.Entities
{
    public partial class RoomOccupied : ThexMultiTenantFullAuditedEntity, IEntityGuid
    {
        public Guid Id { get; set; }
        public int RoomId { get; internal set; }
        public long ReservationId { get; internal set; }
        public int PropertyId { get; internal set; }


        public virtual Reservation Reservation { get; internal set; }

        public virtual Property Property { get; internal set; }

        public virtual Room Room { get; internal set; }

        public static Builder Create(INotificationHandler handler)
       => new Builder(handler);

        public static Builder Create(INotificationHandler handler, RoomOccupied instance)
            => new Builder(handler, instance);

    }
}
