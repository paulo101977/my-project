﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a Crudzilla tool.
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using Tnf.Notifications;


namespace Thex.Domain.Entities
{
    public partial class BusinessPartnerType : IEntityInt
    {
        public BusinessPartnerType()
        {
            BusinessPartnerList = new HashSet<BusinessPartner>();
        }

        public int Id { get; set; }
        public string BusinessPartnerTypeName { get; internal set; }
        public string Description { get; internal set; }

        public virtual ICollection<BusinessPartner> BusinessPartnerList { get; internal set; }

        public enum EntityError
        {
            BusinessPartnerTypeMustHaveBusinessPartnerTypeName,
            BusinessPartnerTypeOutOfBoundBusinessPartnerTypeName,
            BusinessPartnerTypeMustHaveDescription,
            BusinessPartnerTypeOutOfBoundDescription
        }

        public static Builder Create(INotificationHandler handler)
            => new Builder(handler);

        public static Builder Create(INotificationHandler handler, BusinessPartnerType instance)
            => new Builder(handler, instance);
    }
}
