﻿using System;
using Tnf.Notifications;

namespace Thex.Domain.Entities
{
    public partial class ReservationItemLaunch : ThexMultiTenantFullAuditedEntity, IEntityGuid
    {
        public Guid Id { get; set; }
        public int? QuantityOfTourismTaxLaunched { get; internal set; }
        public long ReservationItemId { get; internal set; }
        public long? GuestReservationItemId { get; internal set; }

        public virtual ReservationItem ReservationItem { get; internal set; }
        public virtual GuestReservationItem GuestReservationItem { get; internal set; }

        
        public enum EntityError
        {
            ReservationItemLaunchMustHaveReservationItemId
        }

        public static Builder Create(INotificationHandler handler)
            => new Builder(handler);

        public static Builder Create(INotificationHandler handler, ReservationItemLaunch instance)
            => new Builder(handler, instance);

        public void IncrementQuantityOfTourismTaxLaunched(int numberOfNigthsAllowedInTourismTax, bool isTotalOfNights)
        {
            if (QuantityOfTourismTaxLaunched < numberOfNigthsAllowedInTourismTax && !isTotalOfNights)
                QuantityOfTourismTaxLaunched++;
        }
    }
}
