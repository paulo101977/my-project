﻿using System;
using Thex.Common.Enumerations;

namespace Thex.Domain.Entities
{
    public partial class ReservationItem
    {
        public void SetToCheckout(DateTime checkoutDate, bool? setTime = false)
        {
            ReservationItemStatusId = (int)ReservationStatus.Checkout;

            var currentDate = DateTime.UtcNow.ToZonedDateTimeLoggedUser();

            CheckOutDate = (setTime.HasValue && setTime.Value) ? 
                                GetDateWithTime(checkoutDate, currentDate) :
                                checkoutDate;

            SetCheckoutHour(currentDate);
        }

        private void SetCheckoutHour(DateTime currentDate)
        {
            var checkoutDate = CheckOutDate.Value;

            CheckOutHour = GetDateWithTime(checkoutDate, currentDate);
        }

        private DateTime GetDateWithTime(DateTime checkoutDate, DateTime currentDate)
            => new DateTime(checkoutDate.Year,
                            checkoutDate.Month,
                            checkoutDate.Day,
                            currentDate.Hour,
                            currentDate.Minute,
                            currentDate.Second);

        public void CancelCheckin()
        {
            ReservationItemStatusId = (int)ReservationStatus.ToConfirm;
            CheckInDate = null;
            CheckInHour = null;
        }
    }
}
