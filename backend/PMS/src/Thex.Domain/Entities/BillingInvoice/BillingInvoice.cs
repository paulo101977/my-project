﻿using System;
using Thex.Common.Dto.InvoiceIntegration;
using Thex.Common.Enumerations;

namespace Thex.Domain.Entities
{
    public partial class BillingInvoice
    {
        public void SetIntegrationError(string detail, string codeNumber)
        {
            BillingInvoiceStatusId = (int)BillingInvoiceStatusEnum.Error;
            AditionalDetails = detail;
            ErrorDate = DateTime.UtcNow.ToZonedDateTimeLoggedUser();
            ExternalCodeNumber = codeNumber;
        }

        public void SetIntegrationSuccess(InvoiceIntegrationResponseDto dto, string integrationId)
        {
            BillingInvoiceStatusId = (int)BillingInvoiceStatusEnum.Issued;
            IntegratorXml = dto.UrlXml;
            IntegratorLink = dto.UrlPdf;
            ExternalRps = dto.RpsNumber;
            ExternalNumber = !string.IsNullOrEmpty(dto.Number) ? dto.Number : dto.InvoiceNumber;
            ExternalSeries = dto.RpsSerial;
            ExternalEmissionDate = DateTime.Parse(dto.EmissionDate);
            EmissionDate = DateTime.Parse(dto.EmissionDate);
            IntegratorId = integrationId;
        }

        public void SetIntegrationPending(Guid billingInvoiceId, Guid emissionUserId)
        {
            Id = billingInvoiceId;
            EmissionDate = DateTime.UtcNow.ToZonedDateTimeLoggedUser();
            EmissionUserId = emissionUserId;
            BillingInvoiceStatusId = (int)BillingInvoiceStatusEnum.Pending;
        }

        public void SetIntegrationCancelPending(Guid cancelUserId, int billingInvoiceCancelReasonId)
        {
            CancelUserId = cancelUserId;
            BillingInvoiceCancelReasonId = billingInvoiceCancelReasonId;
            BillingInvoiceStatusId = (int)BillingInvoiceStatusEnum.Canceling;
        }

        public void SetIntegrationCancelSuccess(string timeZoneName)
        {
            CancelDate = DateTime.UtcNow.ToZonedDateTime(timeZoneName);
            BillingInvoiceStatusId = (int)BillingInvoiceStatusEnum.Canceled;
        }

        public void SetBillingInvoiceCreditId(Guid? billingInvoiceCreditId)
        {
            BillingInvoiceCreditId = billingInvoiceCreditId;
        }
    }
}
