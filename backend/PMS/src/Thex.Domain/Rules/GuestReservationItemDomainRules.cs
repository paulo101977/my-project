﻿using System;
using System.Collections.Generic;
using System.Linq;
using Thex.Domain.Entities;
using Thex.Dto;

namespace Thex.Domain.Rules
{
    public static class GuestReservationItemDomainRules
    {
        public static bool SumOfListOfPctDailyRateGreaterThanAllowed(List<GuestReservationItemDto> guestReservationItemDtoList)
        {
            var sumPctDailyRate = guestReservationItemDtoList.Sum(d => d.PctDailyRate);
            var maxSumPctDailyRate = 100;

            return sumPctDailyRate > maxSumPctDailyRate;
        }
        public static bool SumOfListOfPctDailyRateGreaterThanAllowed(int sumPctDailyRate)
        {
            var maxSumPctDailyRate = 100;

            return sumPctDailyRate > maxSumPctDailyRate;
        }
        public static bool SumOfListOfPctDailyRateGreaterThanAllowed(int pctDailyRate, int sumPctDailyRate)
        {
            var maxSumPctDailyRate = 100;

            return (pctDailyRate+ sumPctDailyRate) > maxSumPctDailyRate;
        }

        public static bool TotalOfListOfMainGreaterThanAllowed(List<GuestReservationItem> guestReservationItemList)
        {
            var maxIsMain = guestReservationItemList.Count(d => d.IsMain);
            var maxTotalOfIsMain = 1;

            return maxIsMain > maxTotalOfIsMain;
        }

        public static bool EstimatedDateWithInvalidPeriodBasedOnReservation(ReservationItem reservationItem, GuestReservationItem guestReservationItem)
        {
            if ((guestReservationItem.EstimatedArrivalDate == DateTime.MinValue && guestReservationItem.EstimatedDepartureDate == DateTime.MinValue) || 
                (guestReservationItem.EstimatedArrivalDate >= reservationItem.EstimatedArrivalDate && guestReservationItem.EstimatedDepartureDate <= reservationItem.EstimatedDepartureDate))
                return false;

            return false;
        }

        public static bool EstimatedDateWithInvalidPeriodBasedOnReservationDto(ReservationItemDto reservationItem, GuestReservationItemDto guestReservationItem)
        {
            if ((guestReservationItem.EstimatedArrivalDate == DateTime.MinValue && guestReservationItem.EstimatedDepartureDate == DateTime.MinValue) || 
                (guestReservationItem.EstimatedArrivalDate >= reservationItem.EstimatedArrivalDate && guestReservationItem.EstimatedDepartureDate <= reservationItem.EstimatedDepartureDate))
                return false;

            return false;
        }
    }
}
