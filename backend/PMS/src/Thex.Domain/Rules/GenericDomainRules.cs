﻿using System;
using System.Collections.Generic;
using System.Linq;
using Thex.Domain.Entities;
using Thex.Dto;

namespace Thex.Domain.Rules
{
    public static class GenericDomainRules
    {
        public const int MINIMUMADULTAGE = 18;

        public static bool IsAdult(DateTime birthdate)
        {
            return CalculateAge(birthdate) > MINIMUMADULTAGE; 
        }

        public static int CalculateAge(DateTime birthdate)
        {
            var today = DateTime.Today;

            var age = today.Year - birthdate.Year;

            if (birthdate > today.AddYears(-age)) age--;

            return age;
        }
    }
}
