﻿//  <copyright file="RoomLayoutDomainService.cs" company="TOTVS">
//  Copyright (c) TOTVS. All rights reserved.
//  </copyright>
namespace Thex.Domain.Services
{
    using Thex.Common.Enumerations;
    using Thex.Domain.Entities;
    using Thex.Domain.Interfaces.Services;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Tnf.Domain.Services;
    using Tnf.Repositories;
    using Tnf.Notifications;
    using Thex.Dto;

    public class RoomLayoutDomainService : DomainService<RoomLayout>, IRoomLayoutDomainService
    {
        private class Layout
        {
            int _singleBeds = 0;
            int _doubleBeds = 0;
            public Layout(int totalReverseBeds, int doubleBeds)
            {
                _doubleBeds = doubleBeds;
                _singleBeds = 2 * totalReverseBeds - _doubleBeds * 2;
            }
            public int DoubleBeds { get => _doubleBeds; }
            public int SingleBeds { get => _singleBeds; }
        }

        private readonly IRepository<RoomLayout> _repository;
        private readonly IDomainService<RoomType> _roomTypeDomainService;

        public RoomLayoutDomainService(
            IRepository<RoomLayout> repository, 
            IDomainService<RoomType> roomTypeDomainService,
            INotificationHandler notificationHandler)
            : base(repository, notificationHandler)
        {
            _repository = repository;
            _roomTypeDomainService = roomTypeDomainService;
        }

        private static IList<Layout> GetPossibleRoomLayouts(int totalReverseBeds)
        {
            IList<Layout> result = new List<Layout>();

            if (totalReverseBeds > 0)
            {
                int totalSingleBeds = totalReverseBeds * 2;

                int maxDouble = totalReverseBeds;
                for (int d = 0; d <= maxDouble; d++)
                {
                    var layout = new Layout(totalReverseBeds, d);
                    result.Add(layout);
                }
            }

            return result;
        }

        private RoomLayout GetRoomLayout(int totalSingle, int totalDouble)
        {
            RoomLayout roomLayout =
                _repository.GetAll()
                .Where(lt => lt.QuantitySingle == totalSingle && lt.QuantityDouble == totalDouble)
                .FirstOrDefault();

            if (roomLayout == null)
            {
                roomLayout = new RoomLayout();
                roomLayout.Id = Guid.NewGuid();
                roomLayout.QuantitySingle = (byte)totalSingle;
                roomLayout.QuantityDouble = (byte)totalDouble;
                _repository.Insert(roomLayout);
            }

            return roomLayout;
        }

        public List<RoomLayout> GetAllLayoutsByRoomTypeId(DefaultIntRequestDto id)
        {

            id.Expand = "roomTypeBedTypeList";
            RoomType roomtype = _roomTypeDomainService.Get(id);

            int quantitySingle = 0;
            int quantityDouble = 0;
            int quantityReversible = 0;
            foreach (var roomTypeBedType in roomtype.RoomTypeBedTypeList)
            {
                BedTypeEnum bedTypeEnum = (BedTypeEnum)roomTypeBedType.BedTypeId;
                switch (bedTypeEnum)
                {
                    case BedTypeEnum.Double:
                    {
                        quantityDouble += roomTypeBedType.Count;
                        break;
                    }
                    case BedTypeEnum.Single:
                    {
                        quantitySingle += roomTypeBedType.Count;
                        break;
                    }
                    case BedTypeEnum.Reversible:
                    {
                        quantityReversible += roomTypeBedType.Count;
                        break;
                    }
                }
            }


            IList<Layout> layouts = GetPossibleRoomLayouts(quantityReversible);
            List<RoomLayout> roomLayouts = new List<RoomLayout>();

            int totalSingle = quantitySingle;
            int totalDouble = quantityDouble;

            if ((layouts.Count == 0) && (totalSingle > 0 || totalDouble > 0))
            {
                RoomLayout roomLayout = GetRoomLayout(totalSingle, totalDouble);
                roomLayouts.Add(roomLayout);
            }


            foreach (var layout in layouts)
            {
                totalSingle = quantitySingle + layout.SingleBeds;
                totalDouble = quantityDouble + layout.DoubleBeds;

                if (totalSingle > 0 || totalDouble > 0)
                {
                    RoomLayout roomLayout = GetRoomLayout(totalSingle, totalDouble);
                    roomLayouts.Add(roomLayout);
                }
            }

            return roomLayouts;
        }
    }
}