﻿//  <copyright file="BillingAccountDomainService.cs" company="TOTVS">
//  Copyright (c) TOTVS. All rights reserved.
//  </copyright>
namespace Thex.Domain.Services
{

    using Thex.Common;
    using Thex.Domain.Entities;
    using Thex.Domain.Interfaces.Repositories;
    using Thex.Domain.Services.Interfaces;
    using Tnf.Domain.Services;
    using Tnf.Notifications;
    using System;
    using Thex.Common.Enumerations;
    using Thex.Domain.Interfaces.Services;
    using Tnf.Repositories;
    using Thex.Dto;

    public class BillingAccountDomainService : DomainService<BillingAccount>, IBillingAccountDomainService
    {
        private readonly IBillingAccountRepository _repository;
        private readonly IRepository<ReservationItem> _reservationItemRepository;
        private readonly IReservationItemStatusDomainService _reservationItemStatusDomainService;
        private readonly IGuestReservationItemReadRepository _guestReservationItemReadRepository;
        private readonly IGuestReservationItemRepository _guestReservationItemRepository;

        public BillingAccountDomainService(IBillingAccountRepository repository,
            IReservationItemStatusDomainService reservationItemStatusDomainService,
            IRepository<ReservationItem> reservationItemRepository,
            IGuestReservationItemReadRepository guestReservationItemReadRepository,
            IGuestReservationItemRepository guestReservationItemRepository,
            INotificationHandler notificationHandler)
            : base(repository, notificationHandler)
        {
            _repository = repository;
            _reservationItemStatusDomainService = reservationItemStatusDomainService;
            _reservationItemRepository = reservationItemRepository;
            _guestReservationItemReadRepository = guestReservationItemReadRepository;
            _guestReservationItemRepository = guestReservationItemRepository;
        }

        public decimal CalculateAverageSpendPerDay(decimal total, int periodOfStay)
        {
            if (periodOfStay == 0 || total == 0)
                return total;

            return total / periodOfStay;
        }

        public int CalculatePeriodOfStay(DateTime startDate, DateTime finalDate)
        {
            TimeSpan ts = finalDate - startDate;
            int totalDays = (int)Math.Ceiling(ts.TotalDays);
            if (ts.TotalDays < 1 && ts.TotalDays >= 0)
                totalDays = 1;
            else
                totalDays = (int)(ts.TotalDays);

            return totalDays;
        }

        public void Reopen(int propertyId, Guid id, int reasonId, bool allAccountsAreClosed)
        {
            var billingAccount = _repository.Get(new DefaultGuidRequestDto(id));

            if(billingAccount == null)
                Notification.Raise(Notification
                    .DefaultBuilder
                    .WithMessage(AppConsts.LocalizationSourceName, CommonsEnum.Error.NullOrEmptyObject)
                    .Build());

            billingAccount.Reopen(reasonId);

            if(allAccountsAreClosed)
            {
                if (billingAccount.ReservationItemId.HasValue)
                    ChangeReservationItemStatus(billingAccount.ReservationItemId.Value);

                if (billingAccount.GuestReservationItemId.HasValue)
                    ChangeGuestReservationStatus(billingAccount.GuestReservationItemId.Value);
            }

            _repository.Update(billingAccount);
        }

        private void ChangeReservationItemStatus(long reservationItemId)
        {
            var reservationItem = _reservationItemRepository.Get(new DefaultLongRequestDto(reservationItemId));
            if (reservationItem != null && reservationItem.ReservationItemStatusId == (int)ReservationStatus.Checkout)
            {
                var newStatus = ReservationStatus.Pending;
                if (!_reservationItemStatusDomainService.CanChangeStatus(reservationItem, newStatus))
                {
                    Notification.Raise(Notification.DefaultBuilder
                            .WithMessage(AppConsts.LocalizationSourceName, ReservationItemEnum.Error.CouldNotChangeStatus)
                            .WithDetailedMessage(AppConsts.LocalizationSourceName, ReservationItemEnum.Error.CouldNotChangeStatus)
                            .Build());
                    return;
                }

                reservationItem.ReservationItemStatusId = (int)newStatus;

                _reservationItemRepository.Update(reservationItem);
            }
        }

        private void ChangeGuestReservationStatus(long guestReservationItemId)
        {
            var guestReservationItem = _guestReservationItemReadRepository.Get(new DefaultLongRequestDto(guestReservationItemId));
            if(guestReservationItem != null && guestReservationItem.GuestStatusId == (int)ReservationStatus.Checkout)
                _guestReservationItemRepository.ChangeGuestReservationItemStatus(guestReservationItem.Id, ReservationStatus.Pending);
        }
    }
}