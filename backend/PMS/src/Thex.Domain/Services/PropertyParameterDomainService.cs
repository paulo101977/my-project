﻿using System.Linq;
using Thex.Domain.Entities;
using Thex.Domain.Interfaces.Services;
using Tnf.Domain.Services;
using Tnf.Notifications;
using Tnf.Repositories;

namespace Thex.Domain.Services
{
    public class PropertyParameterDomainService: DomainService<PropertyParameter>, IPropertyParameterDomainService
    {
        private readonly IRepository<PropertyParameter> _repository;

        public PropertyParameterDomainService(IRepository<PropertyParameter> repository,
            INotificationHandler notificationHandler)
            : base(repository, notificationHandler)
        {
            _repository = repository;
        }

        public void UpdatePropertyParameterValueByApplicationParameterId(int propertyId, int applicationParameterId, string propertyParameterValue)
        {
            var propertyParameter = _repository.GetAll().Where(r => r.ApplicationParameterId == applicationParameterId && r.PropertyId == propertyId).FirstOrDefault();
            propertyParameter.PropertyParameterValue = propertyParameterValue;
            _repository.Update(propertyParameter);
        }
    }
}
