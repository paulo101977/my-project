﻿using Thex.Domain.Entities;
using Thex.Domain.Interfaces.Services;
using Thex.Dto;
using Tnf.Domain.Services;
using Tnf.Notifications;
using Tnf.Repositories;

namespace Thex.Domain.Services
{
    public class RatePlanDomainService : DomainService<RatePlan>, IRatePlanDomainService
    {
        public RatePlanDomainService(IRepository<RatePlan> repository,
            INotificationHandler notificationHandler) : base(repository, notificationHandler)
        {
        }

        public bool AlllowsCreatTax(RatePlan ratePlan)
        {
            return ratePlan != null && !ratePlan.RateNet;
        }

        public bool AlllowsCreatTax(RatePlanBillingAccountDto dto)
        {
            return dto != null && !dto.RateNet;
        }
    }
}
