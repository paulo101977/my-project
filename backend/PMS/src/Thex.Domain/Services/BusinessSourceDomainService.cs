﻿//  <copyright file="BusinessSourceDomainService.cs" company="TOTVS">
//  Copyright (c) TOTVS. All rights reserved.
//  </copyright>
namespace Thex.Domain.Services
{
    
    using Thex.Domain.Entities;
    using Thex.Domain.Interfaces.Repositories;
    using Thex.Domain.Services.Interfaces;
    using Thex.Dto;
    using Tnf.Domain.Services;
    using Tnf.Notifications;

    public class BusinessSourceDomainService : DomainService<BusinessSource>, IBusinessSourceDomainService
    {
        private readonly IBusinessSourceRepository _repository;

        public BusinessSourceDomainService(IBusinessSourceRepository repository,
            INotificationHandler notificationHandler)
            : base(repository, notificationHandler)
        {
            _repository = repository;
        }

        public void ToggleActivation(int id)
        {
            BusinessSource businessSource = Repository.Get(new DefaultIntRequestDto(id));

            _repository.ToggleAndSaveIsActive(id);
        }

        public void Delete(int id)
        {
            _repository.Delete(e => e.Id == id);
        }
    }
}