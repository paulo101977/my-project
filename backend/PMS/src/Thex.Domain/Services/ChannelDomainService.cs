﻿using System;
using Thex.Domain.Entities;
using Thex.Domain.Interfaces.Repositories;
using Thex.Domain.Interfaces.Services;
using Thex.Dto;
using Thex.Kernel;
using Tnf.Domain.Services;
using Tnf.Notifications;

namespace Thex.Domain.Services
{
    public class ChannelDomainService : DomainService<Channel>, IChannelDomainService
    {
        private readonly IChannelRepository _repository;
        private readonly IApplicationUser _applicationUser;

        public ChannelDomainService(IChannelRepository repository,
           IApplicationUser applicationUser,
          INotificationHandler notificationHandler)
          : base(repository, notificationHandler)
        {
            _repository = repository;
            _applicationUser = applicationUser;
        }


        public void Create(Channel.Builder channelBuilder)
        {
            var channel = channelBuilder.Build();
            _repository.Create(channel);
        }

        public void DeleteChannel(Channel.Builder channelBuilder)
        {
            var channel = channelBuilder.Build();
            channel.IsDeleted = true;
            channel.DeletionTime = DateTime.UtcNow.ToZonedDateTimeLoggedUser();
            channel.DeleterUserId = _applicationUser.UserUid;
            _repository.DeleteChannel(channel);
        }

        public void ToggleAndSaveIsActive(Guid id)
        {
            _repository.ToggleAndSaveIsActive(id);

        }

        public void Update(Channel.Builder channelBuilder)
        {
            var channel = channelBuilder.Build();
            _repository.Update(channel);
        }
    }
}
