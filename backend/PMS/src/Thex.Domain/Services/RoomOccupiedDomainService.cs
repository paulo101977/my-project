﻿using System;
using System.Collections.Generic;
using Thex.Domain.Entities;
using Thex.Domain.Interfaces.Repositories;
using Thex.Domain.Interfaces.Services;
using Thex.Dto;
using Thex.Kernel;
using Tnf.Domain.Services;
using Tnf.Notifications;

namespace Thex.Domain.Services
{
    public class RoomOccupiedDomainService : DomainService<RoomOccupied>, IRoomOccupiedDomainService
    {
        private readonly IRoomOccupiedRepository _repository;
        private readonly IApplicationUser _applicationUser;

        public RoomOccupiedDomainService(IRoomOccupiedRepository repository,
           IApplicationUser applicationUser,
          INotificationHandler notificationHandler)
          : base(repository, notificationHandler)
        {
            _repository = repository;
            _applicationUser = applicationUser;
        }

        public void Insert(RoomOccupied roomItem)
            => _repository.InsertRoomOccupied(roomItem);


        public void DeleteWithRoomId(int roomId)
           => _repository.DeleteRoomOccupiedWithRoomId(roomId);
    }
}
