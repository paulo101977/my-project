﻿using System;
using Thex.Domain.Entities;
using Thex.Domain.Interfaces.Services;
using Tnf.Domain.Services;
using Tnf.Notifications;
using Tnf.Repositories;

namespace Thex.Domain.Services
{
    public class PropertyAuditProcessDomainService : DomainService<PropertyAuditProcess>, IPropertyAuditProcessDomainService
    {
        private readonly IRepository<PropertyAuditProcess> _repository;

        public PropertyAuditProcessDomainService(IRepository<PropertyAuditProcess> repository,
            INotificationHandler notificationHandler) : base(repository, notificationHandler)
        {
            _repository = repository;
        }

        public void UpdateStepTypeIdAndEndDate(PropertyAuditProcess propertyAuditProcess, int auditStepTypeId, DateTime? endDate = null)
        {
            propertyAuditProcess.AuditStepTypeId = auditStepTypeId;
            propertyAuditProcess.EndDate = endDate;
            _repository.Update(propertyAuditProcess);
        }

        public void UpdatePropertySystemDate(PropertyAuditProcess propertyAuditProcess, DateTime propertySystemDate)
        {
            propertyAuditProcess.PropertySystemDate = propertySystemDate;
            _repository.Update(propertyAuditProcess);
        }
    }
}
