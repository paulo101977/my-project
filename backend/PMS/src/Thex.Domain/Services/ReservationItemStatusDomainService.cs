﻿// //  <copyright file="ReservationItemStatusDomainService.cs" company="TOTVS">
// //  Copyright (c) TOTVS. All rights reserved.
// //  </copyright>
using Thex.Common.Enumerations;
using Thex.Domain.Interfaces.Services;

namespace Thex.Domain.Services
{
    using System.Collections.Generic;

    using Thex.Domain.Entities;
    using Thex.Domain.Interfaces.Factories;

    public class ReservationItemStatusDomainService : IReservationItemStatusDomainService
    {
        private IReservationItemStatusFactory _reservationItemStatusFactory;

        public ReservationItemStatusDomainService(IReservationItemStatusFactory reservationItemStatusFactory)
        {
            _reservationItemStatusFactory = reservationItemStatusFactory;
        }

        public bool CanChangeStatus(ReservationStatus currentStatus, ReservationStatus newStatus)
        {
            IList<ReservationStatus> allowedStatusList = _reservationItemStatusFactory.GetAllowedStatus(currentStatus);

            if (allowedStatusList.Contains(newStatus))
                return true;
            return false;
        }

        public bool CanChangeStatus(ReservationItem reservationItem, ReservationStatus newStatus)
        {
            IList<ReservationStatus> allowedStatusList = _reservationItemStatusFactory.GetAllowedStatus((ReservationStatus)reservationItem.ReservationItemStatusId);

            if (allowedStatusList.Contains(newStatus))
                return true;
            return false;
        }

        public bool CanChangeReservationItem(int reservationItemStatusId)
        {
            var status = (ReservationStatus)reservationItemStatusId;

            if (status == ReservationStatus.Canceled || status == ReservationStatus.NoShow
              || status == ReservationStatus.Checkout)
                return false;

            return true;
        }

        public ReservationItem GetNewInstanceWithStatusId(ReservationItem reservationItem)
        {
            return new ReservationItem()
            {
                ReservationItemStatusId = reservationItem.ReservationItemStatusId
            };
        }
        public ReservationItem GetNewInstanceWithStatusIdAndRoomId(ReservationItem reservationItem)
        {
            return new ReservationItem()
            {
                ReservationItemStatusId = reservationItem.ReservationItemStatusId,
                RoomId = reservationItem.RoomId
            };
        }
        public ReservationItem GetNewInstanceWithStatusAndAndRoomAndRoomTypeIdAndEstimatedDates(ReservationItem reservationItem)
        {
            return new ReservationItem()
            {
                ReservationItemStatusId = reservationItem.ReservationItemStatusId,
                RoomId = reservationItem.RoomId,
                ReceivedRoomTypeId = reservationItem.ReceivedRoomTypeId,
                EstimatedArrivalDate = reservationItem.EstimatedArrivalDate,
                EstimatedDepartureDate = reservationItem.EstimatedDepartureDate
            };
        }
    }
}