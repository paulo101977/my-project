﻿using System;
using System.Collections.Generic;
using System.Text;
using Thex.Domain.Entities;
using Thex.Domain.Interfaces.Repositories;
using Thex.Domain.Interfaces.Services;
using Thex.Kernel;
using Tnf.Domain.Services;
using Tnf.Notifications;

namespace Thex.Domain.Services
{
    public class ContactInformationDomainService : DomainService<ContactInformation>, IContactInformationDomainService
    {
        private readonly IContactInformationRepository _contactInformationRepository;
        private readonly IApplicationUser _applicationUser;

        public ContactInformationDomainService(IContactInformationRepository contactInformationRepository,
         IApplicationUser applicationUser,
        INotificationHandler notificationHandler)
        : base(contactInformationRepository, notificationHandler)
        {
            _contactInformationRepository = contactInformationRepository;
            _applicationUser = applicationUser;
        }

        public void ResetContactInformationsProperty(Guid guid)
        {
            _contactInformationRepository.ResetContactInformationsProperty(guid);
        }
    }
}
