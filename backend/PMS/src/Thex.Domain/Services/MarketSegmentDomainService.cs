﻿//  <copyright file="BusinessSourceDomainService.cs" company="TOTVS">
//  Copyright (c) TOTVS. All rights reserved.
//  </copyright>
namespace Thex.Domain.Services
{
    
    using Thex.Domain.Entities;
    using Thex.Domain.Interfaces.Repositories;
    using Thex.Domain.Services.Interfaces;
    using Tnf.Domain.Services;
    using Tnf.Notifications;
    using Thex.Dto;

    public class MarketSegmentDomainService : DomainService<MarketSegment>, IMarketSegmentDomainService
    {
        private readonly IMarketSegmentRepository _repository;

        public MarketSegmentDomainService(IMarketSegmentRepository repository,
            INotificationHandler notificationHandler) : base(repository, notificationHandler)
        {
            _repository = repository;
        }

        public void ToggleActivation(int id)
        {
            MarketSegment marketSegment = _repository.Get(new DefaultIntRequestDto(id));

            _repository.ToggleAndSaveIsActive(id);
        }

        public void Delete(int id)
        {
            _repository.Delete(e => e.Id == id);
        }
    }
}