﻿using System;
using System.Collections.Generic;
using System.Linq;
using Thex.Common;
using Thex.Common.Enumerations;
using Thex.Domain.Entities;
using Thex.Domain.Interfaces.Repositories;
using Thex.Domain.Interfaces.Repositories.Read;
using Thex.Domain.Interfaces.Services;
using Thex.Kernel;
using Tnf.Notifications;
using Tnf.Repositories.Uow;

namespace Thex.Domain.Services
{
    public class CheckinDomainService : ICheckinDomainService
    {
        private readonly INotificationHandler _notification;
        private readonly IGuestReservationItemRepository _guestReservationItemRepository;
        private readonly IBillingAccountRepository _billingAccountRepository;
        private readonly IApplicationUser _applicationUser;
        private readonly IGuestReservationItemReadRepository _guestReservationItemReadRepository;
        private readonly IReservationItemReadRepository _reservationItemReadRepository;
        private readonly IBillingAccountReadRepository _billingAccountReadRepository;
        private readonly IReservationBudgetReadRepository _reservationBudgetReadRepository;
        private readonly IReservationBudgetRepository _reservationBudgetRepository;
        private readonly IReservationItemRepository _reservationItemRepository;
        private readonly IBillingAccountItemReadRepository _billingAccountItemReadRepository;
        private readonly IUnitOfWorkManager _unitOfWorkManager;

        public CheckinDomainService(
            INotificationHandler Notification,
            IGuestReservationItemRepository guestReservationItemRepository,
            IBillingAccountRepository billingAccountRepository,
            IApplicationUser applicationUser,
            IGuestReservationItemReadRepository guestReservationItemReadRepository,
            IReservationItemReadRepository reservationItemReadRepository,
            IBillingAccountReadRepository billingAccountReadRepository,
            IReservationBudgetReadRepository reservationBudgetReadRepository,
            IReservationBudgetRepository reservationBudgetRepository,
            IReservationItemRepository reservationItemRepository,
            IBillingAccountItemReadRepository billingAccountItemReadRepository,
            IUnitOfWorkManager unitOfWorkManager)
        {
            _notification = Notification;
            _guestReservationItemRepository = guestReservationItemRepository;
            _billingAccountRepository = billingAccountRepository;
            _applicationUser = applicationUser;
            _guestReservationItemReadRepository = guestReservationItemReadRepository;

            _billingAccountReadRepository = billingAccountReadRepository;
            _reservationItemReadRepository = reservationItemReadRepository;
            _reservationBudgetReadRepository = reservationBudgetReadRepository;
            _reservationBudgetRepository = reservationBudgetRepository;
            _reservationItemRepository = reservationItemRepository;
            _billingAccountItemReadRepository = billingAccountItemReadRepository;
            _unitOfWorkManager = unitOfWorkManager;
        }

        public void CreateGuestAccounts(List<GuestReservationItem> guestReservationItemList, long reservationId, int propertyId, int businessSourceId, int marketSegmentId, List<BillingAccount> existingBillingAccounts)
        {
            var billingAccountList = new List<BillingAccount>();
            BillingAccount.Builder builder = null;

            guestReservationItemList.ForEach(item =>
                {
                    var id = Guid.NewGuid();

                    builder = new BillingAccount.Builder(_notification);

                    builder
                        .WithId(id)
                        .WithReservationItemId(item.ReservationItemId)
                        .WithIsBillingAccountName(item.GuestName)
                        .WithBillingAccountTypeId((int)BillingAccountTypeEnum.Guest)
                        .WithStatusId((int)AccountBillingStatusEnum.Opened)
                        .WithReservationId(reservationId)
                        .WithGuestReservationItemId(item.Id)
                        .WithStartDate(DateTime.UtcNow.ToZonedDateTimeLoggedUser())
                        .WithCreationTime(DateTime.UtcNow.ToZonedDateTimeLoggedUser())
                        .WithPropertyId(propertyId)
                        .WithBusinessSourceId(businessSourceId)
                        .WithMarketSegmentId(marketSegmentId)
                        .WithIsMainAccount(true)
                        .WithBlocked(false)
                        .WithGroupKey(id)
                        .WithCreatorUserId(_applicationUser.UserUid)
                        .WithTenantId(_applicationUser.TenantId);

                    billingAccountList.Add(builder.Build());

                }
            );         
          
            if (_notification.HasNotification()) return;

            _billingAccountRepository.CreateBillingAccounts(billingAccountList);

            if (existingBillingAccounts.Any())
                UpdateBillingAccountsGroupKey(billingAccountList, existingBillingAccounts);

        }       

        public void UpdateToCheckInGuestReservationItem(List<GuestReservationItem> guestReservationItemList, DateTime checkinDate)
        {
            GuestReservationItem.Builder builder = null;

            guestReservationItemList.ForEach(guestReservationItem =>
                {
                    builder = new GuestReservationItem.Builder(_notification, guestReservationItem);

                    builder
                        .WithCheckInDate(checkinDate)
                        .WithCheckInHour(checkinDate)
                        .WithGuestStatusId((int)ReservationStatus.Checkin);

                    builder.Build();                    
                }
            );

            if (_notification.HasNotification()) return;

            var guestReservationItemIds = guestReservationItemList.Select(g => g.Id).ToList();

            _guestReservationItemRepository.UpdateGuestsToCheckIn(guestReservationItemIds);
        }

        private void UpdateBillingAccountsGroupKey(List<BillingAccount> billingAccountList, List<BillingAccount> existingBillingAccounts)
        {
            existingBillingAccounts.ForEach(billingAccount =>
                {
                    var id = billingAccountList.FirstOrDefault(ba => ba.GuestReservationItemId == billingAccount.GuestReservationItemId)?.Id;
                    billingAccount.GroupKey = id ?? billingAccount.GroupKey;
                    billingAccount.IsMainAccount = false;
                }
            );

            _billingAccountRepository.UpdateBillingAccounts(existingBillingAccounts);
        }

        public void Cancel(long reservationItemId, DateTime systemDate)
        {
            var guestReservationItemList = _guestReservationItemReadRepository.GetAllByReservationItemId(reservationItemId);

            ValidateStatusInCancelCheckin(guestReservationItemList);

            if (_notification.HasNotification()) return;

            ValidateDateInCancelCheckin(guestReservationItemList, systemDate);

            if (_notification.HasNotification()) return;

            var reservationItem = _reservationItemReadRepository.GetByIdWithGuestReservationItemList(guestReservationItemList.FirstOrDefault().ReservationItemId);

            ValidateWalkinInCancelCheckin(reservationItem);

            if (_notification.HasNotification()) return;

            using (var uow = _unitOfWorkManager.Begin())
            {
                var billingAccountIdList = _billingAccountReadRepository.GetAllIdByFilters(guestReservationItemList.Select(g => g.Id).ToList(), BillingAccountTypeEnum.Guest);

                if (billingAccountIdList.Any())
                {
                    var billingAccountCompanyId = _billingAccountReadRepository.GetIdByFilters(reservationItem.Id, BillingAccountTypeEnum.Company);

                    if (billingAccountCompanyId != null && billingAccountCompanyId != Guid.Empty)
                        billingAccountIdList.Add(billingAccountCompanyId);

                    var billingAccountItemIdList = _billingAccountItemReadRepository.GetAllIdByBillingAccountIdList(billingAccountIdList);

                    if (billingAccountItemIdList.Any())
                    {
                        _notification.Raise(_notification.DefaultBuilder
                                                 .WithMessage(AppConsts.LocalizationSourceName, BillingAccount.EntityError.BillingAccountAlreadyItems)
                                                 .WithDetailedMessage(AppConsts.LocalizationSourceName, BillingAccount.EntityError.BillingAccountAlreadyItems)
                                                 .Build());
                        return;
                    }
                    else
                        _billingAccountRepository.DeleteRange(billingAccountIdList);
                }

                var billingAccountGroup = _billingAccountReadRepository.GetByFilters(reservationItem.ReservationId, BillingAccountTypeEnum.GroupAccount);

                if (billingAccountGroup != null)
                {
                    var reservationItemList = _reservationItemReadRepository.GetAllByReservationId(reservationItem.ReservationId);

                    if (GroupHasNotOtherReservationItemInCheckinOrCheckoutOrPending(reservationItem, reservationItemList))
                    {
                        if (_billingAccountItemReadRepository.AnyByBillingAccountId(billingAccountGroup.Id))
                        {
                            _notification.Raise(_notification.DefaultBuilder
                                                 .WithMessage(AppConsts.LocalizationSourceName, BillingAccount.EntityError.BillingAccountGroupAlreadyHasItems)
                                                 .WithDetailedMessage(AppConsts.LocalizationSourceName, BillingAccount.EntityError.BillingAccountGroupAlreadyHasItems)
                                                 .Build());
                            return;
                        }
                        else
                            _billingAccountRepository.DeleteRange(new List<Guid> { billingAccountGroup.Id });
                    }
                }

                guestReservationItemList.ForEach(g => g.CancelCheckin());
                reservationItem.CancelCheckin();

                var reservationBudgetIdList = _reservationBudgetReadRepository.GetAllIdByFilters(reservationItem.Id, reservationItem.EstimatedArrivalDate);
                if (reservationBudgetIdList.Any())
                    _reservationBudgetRepository.DeleteRange(reservationBudgetIdList);

                _reservationItemRepository.Update(reservationItem);
                _guestReservationItemRepository.UpdateRange(guestReservationItemList);

                uow.Complete();
            }
        }

        private static bool GroupHasNotOtherReservationItemInCheckinOrCheckoutOrPending(ReservationItem reservationItem, IList<ReservationItem> reservationItemList)
        {
            return !(reservationItemList.Count > 1 && reservationItemList.Any(r => r.Id != reservationItem.Id
                                    && (r.ReservationItemStatusId == (int)ReservationStatus.Checkin || r.ReservationItemStatusId == (int)ReservationStatus.Checkout
                                        || r.ReservationItemStatusId == (int)ReservationStatus.Pending)));
        }

        private void ValidateStatusInCancelCheckin(List<GuestReservationItem> guestReservationItemList)
        {
            if (guestReservationItemList.IsNullOrEmpty() || guestReservationItemList.Any(g => g.GuestStatusId != (int)ReservationStatus.Checkin))
                _notification.Raise(_notification.DefaultBuilder
                     .WithMessage(AppConsts.LocalizationSourceName, GuestReservationItem.EntityError.GuestStatusInvalidInCancelCheckin)
                     .WithDetailedMessage(AppConsts.LocalizationSourceName, GuestReservationItem.EntityError.GuestStatusInvalidInCancelCheckin)
                     .Build());
        }

        private void ValidateDateInCancelCheckin(List<GuestReservationItem> guestReservationItemList, DateTime systemDate)
        {
            if (guestReservationItemList.Any(g => g.CheckInDate == null || (g.CheckInDate.HasValue && g.CheckInDate.Value.Date != systemDate.Date)))
                _notification.Raise(_notification.DefaultBuilder
                     .WithMessage(AppConsts.LocalizationSourceName, GuestReservationItem.EntityError.GuestDateInvalidInCancelCheckin)
                     .WithDetailedMessage(AppConsts.LocalizationSourceName, GuestReservationItem.EntityError.GuestDateInvalidInCancelCheckin)
                     .Build());
        }

        private void ValidateWalkinInCancelCheckin(ReservationItem reservationItem)
        {
            if (reservationItem.WalkIn)
                _notification.Raise(_notification.DefaultBuilder
                     .WithMessage(AppConsts.LocalizationSourceName, ReservationItem.EntityError.ReservationItemCanNotBeWalkin)
                     .WithDetailedMessage(AppConsts.LocalizationSourceName, ReservationItem.EntityError.ReservationItemCanNotBeWalkin)
                     .Build());
        }
    }
}
