﻿using Thex.Common;
using Thex.Domain.Entities;
using Thex.Domain.Interfaces.Repositories;
using Thex.Domain.Services.Interfaces;
using Tnf.Domain.Services;
using Tnf.Notifications;
using System;
using Thex.Common.Enumerations;
using Thex.Domain.Interfaces.Repositories.ReadDapper;
using System.Threading.Tasks;
using Thex.Dto.ExternalRegistration;
using System.Collections.Generic;

namespace Thex.Domain.Services
{
    public class ExternalRegistrationDomainService : DomainService<ExternalRegistration>, IExternalRegistrationDomainService
    {
        protected readonly IExternalRegistrationRepository _externalRegistrationRepository;
        protected readonly IGuestReservationItemDapperReadRepository _guestReservationItemDapperReadRepository;
        protected readonly IGuestReservationItemRepository _guestReservationItemRepository;
        
        public ExternalRegistrationDomainService(
            IExternalRegistrationRepository externalRegistrationRepository,
            IGuestReservationItemDapperReadRepository guestReservationItemDapperReadRepository,
            IGuestReservationItemRepository guestReservationItemRepository,
            INotificationHandler notificationHandler)
            : base(externalRegistrationRepository, notificationHandler)
        {
            _externalRegistrationRepository = externalRegistrationRepository;
            _guestReservationItemRepository = guestReservationItemRepository;
            _guestReservationItemDapperReadRepository = guestReservationItemDapperReadRepository;
        }

        public async Task CreateAsync(Guid reservationUid, string email, string link)
        {
            var externalRegistration = new ExternalRegistration(reservationUid, email, link);

            await _externalRegistrationRepository.InsertAndSaveChangesAsync(externalRegistration);

            var guestReservationItemIdList = await _guestReservationItemDapperReadRepository.GetIdListByReservationUidAsync(reservationUid);

            externalRegistration.AddExternalGuestRegistrationList(guestReservationItemIdList);

            await _externalRegistrationRepository.InsertItemsAndSaveChangesAsync(externalRegistration.ExternalGuestRegistrationList);
        }

        public async Task AddNewGuestsIfNecessaryAsync(Guid reservationUid)
        {
            var externalRegistration = await _externalRegistrationRepository.GetByReservationUidAsync(reservationUid);

            if (externalRegistration == null)
                return;

            var guestReservationItemIdList = await _guestReservationItemDapperReadRepository.GetIdListByReservationUidAsync(reservationUid);

            if (guestReservationItemIdList == null)
                return;

            externalRegistration.AddExternalGuestRegistrationList(guestReservationItemIdList);

            await _externalRegistrationRepository.InsertItemsAndSaveChangesAsync(externalRegistration.ExternalGuestRegistrationList);
        }

        public async Task UpdateExternalGuestAsync(ExternalGuestRegistrationDto dto)
        {
            var externalGuest = await _externalRegistrationRepository.GetByGuestReservationItemIdAsync(dto.GuestReservationItemId);

            if(externalGuest == null)
            {
                Notification.Raise(Notification
                    .DefaultBuilder
                    .WithMessage(AppConsts.LocalizationSourceName, CommonsEnum.Error.ParameterInvalid)
                    .Build());
                return;
            }

            var guestReservationItem = _guestReservationItemRepository.GetById(externalGuest.GuestReservationItemId);

            if (guestReservationItem.CanChangeByExternalRegistration())
            {
                Notification.Raise(Notification
                    .DefaultBuilder
                    .WithMessage(AppConsts.LocalizationSourceName, GuestReservationItem.EntityError.GuestReservationItemStatusValidateByExternalRegistration)
                    .Build());
                return;
            }

            externalGuest.ChangeGuestRegistration(dto.FirstName, dto.LastName, dto.GetFullName(), dto.SocialName, dto.Email,
                                                    dto.BirthDate, dto.Gender, dto.OccupationId, dto.PhoneNumber, dto.MobilePhoneNumber,
                                                    dto.NationalityId, dto.PurposeOfTrip, dto.ArrivingBy, dto.ArrivingFrom, dto.ArrivingFromText,
                                                    dto.NextDestination, dto.NextDestinationText, dto.AdditionalInformation, dto.HealthInsurance,
                                                    dto.DocumentTypeId, dto.DocumentInformation, dto.Latitude, dto.Longitude, dto.StreetName, dto.StreetNumber,
                                                    dto.AdditionalAddressDetails, dto.Neighborhood, dto.CityId, dto.StateId, dto.CountryId,
                                                    dto.PostalCode, dto.CountryCode);

            await _externalRegistrationRepository.UpdateExternalGuestAndSaveChanges(externalGuest);

            dto.Id = externalGuest.Id;
            dto.GuestReservationItemId = externalGuest.GuestReservationItemId;
            dto.ExternalRegistrationEmail = externalGuest.ExternalRegistrationEmail;


            guestReservationItem.ChangeGuestInformation(dto.Email, dto.DocumentInformation, dto.DocumentTypeId, dto.GetFullName(), dto.GetFullName());

            _guestReservationItemRepository.UpdateAndSaveChanges(guestReservationItem);
        }
    }
}