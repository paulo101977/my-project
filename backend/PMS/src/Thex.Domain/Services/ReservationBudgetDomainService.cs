﻿//  <copyright file="ReservationBudgetDomainService.cs" company="TOTVS">
//  Copyright (c) TOTVS. All rights reserved.
//  </copyright>

using Thex.Dto;

namespace Thex.Domain.Services
{
    using Thex.Domain.Entities;
    using Thex.Domain.Services.Interfaces;
    using Tnf.Domain.Services;
    using System.Linq;
    using System.Collections.Generic;
    using System;
    using Tnf.Notifications;
    using Tnf.Repositories;
    using Thex.Common.Enumerations;

    public class ReservationBudgetDomainService : DomainService<ReservationBudget>, IReservationBudgetDomainService
    {
        public ReservationBudgetDomainService(IRepository<ReservationBudget> repository,
            INotificationHandler notificationHandler)
            : base(repository, notificationHandler)
        {
        }

        public decimal CalculateWithRatePlanAmount(decimal amount, bool isDecreased, decimal percentualAmmount, int? roundingTypeId)
        {
            if (amount == 0)
                return 0;

            var percentage = (isDecreased ?
                                    percentualAmmount * -1 :
                                    percentualAmmount) / 100;

            amount = amount + (amount * percentage);

            return RoundByRoundingTypeId(amount, roundingTypeId);
        }

        public decimal CalculateWithRateStrategyAmount(decimal strategyAmount, bool isDecreasedStrategy, decimal? percentualStrategyAmount, decimal? strategyIncrementAmount)
        {
            if (strategyAmount == 0)
                return 0;

            if (percentualStrategyAmount.HasValue)
            {
                if (isDecreasedStrategy)
                {
                    var value = 100 - percentualStrategyAmount.Value;
                    strategyAmount = strategyAmount * value / 100;
                }
                else
                {
                    var value = 100 + percentualStrategyAmount.Value;
                    strategyAmount = strategyAmount * value / 100;
                }
            }
            else if (strategyIncrementAmount.HasValue)
            {
                if (isDecreasedStrategy)
                    strategyAmount -= strategyIncrementAmount.Value;
                else
                    strategyAmount += strategyIncrementAmount.Value;
            }

            return strategyAmount;
        }

        public ReservationItemCommercialBudgetValuesDto CalculateBaseRate(int adultCount, List<int> childrenAgeList, List<PropertyParameterDto> childParameterList, PropertyBaseRate baseRate, RoomType roomType)
        {
            var result = new ReservationItemCommercialBudgetValuesDto();

            if (baseRate == null) 
                return result;

            result.BaseRateAmount = adultCount == 0 ? 0 : adultCount <= 5 ? (decimal?)baseRate["Pax" + adultCount + "Amount"] : (decimal)baseRate["Pax5Amount"] + ((adultCount - 5) * (decimal?)baseRate["PaxAdditionalAmount"]);
            result.MealPlanAmount = adultCount == 0 ? 0 : baseRate.AdultMealPlanAmount * adultCount;

            var roomTypeCapacity = new Dictionary<int, int>();

            var childrenCount = childrenAgeList == null ? 0 : childrenAgeList.Count();

            if (childrenCount >= 1 && childrenCount <= 3)
            {
                for (int i = 0; i < childrenCount; i++)
                {
                    var childAge = childrenAgeList[i];

                    var childPos = 0;
                    var keepGoing = true;
                    //pode estar desativado 1 parametro de child
                    childParameterList
                        .Where(e => e.IsActive.HasValue && e.IsActive.Value)
                        .Select(e => Convert.ToInt32(e.PropertyParameterValue))
                        .OrderBy(e => e)
                        .ToList()
                        .ForEach(delegate (int value)
                        {
                            if (keepGoing)
                            {
                                childPos++;

                                if (childAge <= value)
                                    keepGoing = false;
                            }

                        });

                    (decimal?, decimal?) childRate = (0, 0);

                    childRate.Item1 = (decimal?)baseRate["Child" + (childPos == 0 ? 1 : childPos) + "Amount"] ?? 0;
                    childRate.Item2 = (decimal?)baseRate["Child" + (childPos == 0 ? 1 : childPos) + "MealPlanAmount"] ?? 0;

                    if (childPos == 3 && roomType.FreeChildQuantity3 > 0)
                        childRate = CalculatePaxChildValueBasedOnFreeChildQuantity(roomType, roomTypeCapacity, childPos, childRate.Item1, childRate.Item2);

                    if (childPos == 2 && roomType.FreeChildQuantity2 > 0)
                        childRate = CalculatePaxChildValueBasedOnFreeChildQuantity(roomType, roomTypeCapacity, childPos, childRate.Item1, childRate.Item2);

                    if ((childPos == 0 || childPos == 1) && roomType.FreeChildQuantity2 > 0)
                        childRate = CalculatePaxChildValueBasedOnFreeChildQuantity(roomType, roomTypeCapacity, childPos, childRate.Item1, childRate.Item2);

                    result.ChildRateAmount = (result.ChildRateAmount ?? 0) + childRate.Item1;
                    result.ChildMealPlanAmount = (result.ChildMealPlanAmount ?? 0) + childRate.Item2;
                }
            }

            return result;
        }

        public (decimal?, decimal?) CalculatePaxChildValueBasedOnFreeChildQuantity(RoomType roomType, Dictionary<int, int> roomTypeCapacity, int childPos, decimal? paxChildValue, decimal? mealPlanChildValue)
        {
            var checkRoomTypeCapacity = roomTypeCapacity.Where(e => e.Key == childPos).Count();

            if (checkRoomTypeCapacity == 0 || checkRoomTypeCapacity < roomType.FreeChildQuantity3)
            {
                roomTypeCapacity.Add(childPos, 1);
                paxChildValue = 0;
                mealPlanChildValue = 0;
            }

            return (paxChildValue, mealPlanChildValue);
        }

        public decimal RoundByRoundingTypeId(decimal total, int? roundingTypeId)
        {
            if (!roundingTypeId.HasValue)
                return total;

            var roundingTypeEnum = (RoundingTypeEnum)roundingTypeId;
            switch (roundingTypeEnum)
            {
                case RoundingTypeEnum.NotRound:
                    return total;
                case RoundingTypeEnum.Default:
                    return Math.Round(total, 0, MidpointRounding.AwayFromZero);
                case RoundingTypeEnum.RoundUp:
                    return Math.Ceiling(total);
                case RoundingTypeEnum.RoundDown:
                    return Math.Floor(total);
                default:
                    return total;
            }
        }
    }
}