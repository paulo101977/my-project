﻿using System;
using System.Collections.Generic;
using Thex.Domain.Entities;
using Thex.Domain.Interfaces.Repositories;
using Thex.Domain.Interfaces.Services;
using Thex.Dto;
using Thex.Kernel;
using Tnf.Domain.Services;
using Tnf.Notifications;

namespace Thex.Domain.Services
{
    public class PropertyDocumentTypeDomainService : DomainService<PropertyDocumentType>, IPropertyDocumentTypeDomainService
    {
        private readonly IPropertyDocumentTypeRepository _repository;
        private readonly IApplicationUser _applicationUser;

        public PropertyDocumentTypeDomainService(IPropertyDocumentTypeRepository repository,
           IApplicationUser applicationUser,
          INotificationHandler notificationHandler)
          : base(repository, notificationHandler)
        {
            _repository = repository;
            _applicationUser = applicationUser;
        }

        public void Insert(PropertyDocumentType.Builder propertyDocumentType)
        {
            var pDocumentType = propertyDocumentType.Build();
            pDocumentType.PropertyId = int.Parse(_applicationUser.PropertyId);
            _repository.Insert(pDocumentType);
        }

        public void Delete(PropertyDocumentType.Builder propertyDocumentType)
        {
            var pDocumentType = propertyDocumentType.Build();
            _repository.Delete(pDocumentType);
        }

    }
}
