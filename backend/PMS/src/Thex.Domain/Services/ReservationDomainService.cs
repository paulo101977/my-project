﻿//  <copyright file="ReservationDomainService.cs" company="TOTVS">
//  Copyright (c) TOTVS. All rights reserved.
//  </copyright>
namespace Thex.Domain.Services
{
    using System.Linq;

    using Thex.Domain.Entities;
    using Thex.Domain.Interfaces.Repositories;
    using Thex.Domain.Services.Interfaces;
    using Thex.Domain.Interfaces.Services;

    using Tnf.Domain.Services;
    using Thex.Common.Enumerations;
    using Thex.EntityFrameworkCore.ReadInterfaces.Repositories;
    using System.Collections.Generic;
    using Thex.Dto.Availability;
    using Thex.Dto;
    using Thex.Dto.GuestReservationItem;
    using Thex.Common;
    using Tnf.Notifications;
    using Tnf.Dto;
    using System;

    public class ReservationDomainService : DomainService<Reservation>, IReservationDomainService
    {
        private readonly IReservationRepository _repository;
        protected readonly IDomainService<Reservation> _reservationAppDomainService;
        private readonly IReservationItemDomainService _reservationItemDomainService;
        private readonly IReservationItemStatusDomainService _reservationItemStatusDomainService;
        private readonly IRoomReadRepository _roomReadRepository;
        private readonly IGuestReservationItemRepository _guestItemRepository;
        private readonly IBillingAccountDomainService _billingAccountDomainService;

        public ReservationDomainService(IReservationRepository repository,
                                        IDomainService<Reservation> reservationAppDomainService,
                                        IReservationItemDomainService reservationItemDomainService,
                                        IReservationItemStatusDomainService reservationItemStatusDomainService,
                                        IRoomReadRepository roomReadRepository, 
                                        IGuestReservationItemRepository guestItemRepository,
                                        IBillingAccountDomainService billingAccountDomainService,
                                        INotificationHandler notificationHandler): base(repository, notificationHandler)
        {
            _repository = repository;
            _reservationAppDomainService = reservationAppDomainService;
            _reservationItemDomainService = reservationItemDomainService;
            _reservationItemStatusDomainService = reservationItemStatusDomainService;
            _roomReadRepository = roomReadRepository;
            _guestItemRepository = guestItemRepository;
            _billingAccountDomainService = billingAccountDomainService;
        }


        public Reservation GetExistingReservation(long reservationId)
        {
            return _repository.GetExistingReservation(reservationId);
        }

        public bool CanModifyReservation(long reservationId)
        {
  
            return _repository.CheckStatusAllItensForModification(reservationId);
        }


        public Reservation Update(Reservation entity, Reservation existingReservation, ReservationConfirmation reservationConfirmation, Plastic plastic, DateTime systemDate)
        {
            return _repository.Update(entity, existingReservation, reservationConfirmation, plastic, systemDate);
        }

        public void CancelReservation(long reservationId, int reasonId, string cancellationDescription)
        {
            var requestDto = new DefaultLongRequestDto();
            requestDto.Id = reservationId;
            requestDto.Expand = "reservationItemList";

            Reservation reservation = this._reservationAppDomainService.Get(requestDto);
            
            this._reservationItemDomainService.CancelReservationItemList(reservation.ReservationItemList.ToList(), reasonId, cancellationDescription);
        }

        public void ReactivateReservation(long reservationId)
        {
            var requestDto = new DefaultLongRequestDto();
            requestDto.Id = reservationId;
            requestDto.Expand = "reservationItemList,reservationConfirmationList";

            var reservation = this._reservationAppDomainService.Get(requestDto);

            if(reservation != null && reservation.ReservationItemList != null)
            {
                var newStatus = _reservationItemDomainService.GetStatusByReservationConfirmation(reservation.ReservationConfirmationList.FirstOrDefault());

                foreach (var reservationItem in reservation.ReservationItemList.Where(e => e.ReservationItemStatusId == (int)ReservationStatus.Canceled))
                {

                    var oldReservationItem = _reservationItemStatusDomainService.GetNewInstanceWithStatusId(reservationItem);


                    var builder = new ReservationItem.Builder(Notification, reservationItem, _roomReadRepository, oldReservationItem, reservation.PropertyId);

                    builder.WithReservationItemStatus(newStatus);

                    _reservationItemDomainService.Update(builder);

                    if (newStatus == ReservationStatus.ToConfirm)
                        _guestItemRepository.UpdateAllGuestToConfirm(reservationItem.Id);
                    else if (newStatus == ReservationStatus.Confirmed)
                        _guestItemRepository.UpdateAllGuestToConfirmed(reservationItem.Id);
                }
            }
        }

        public List<AvailabilityReservationColumnsDto> GetAvailabilityReservationColumnsDto(IListDto<RoomDto> allPropertyRooms, List<ReservationItem> reservationItems)
        {
            var result = new List<AvailabilityReservationColumnsDto>();
            var allPropertyRoomsList = allPropertyRooms.Items;

            foreach (var reservationItem in reservationItems)
            {
                var isConjugated = false;

                if (reservationItem.RoomId.HasValue)
                {
                    var childrenRooms = allPropertyRoomsList.Where(d => d.ParentRoomId.HasValue && d.ParentRoomId.Value == reservationItem.RoomId.Value).ToList();
                    
                    if(childrenRooms.Count > 0)
                    {
                        isConjugated = true;

                        DuplicateAvailabilityReservationColumnsDto(result, childrenRooms, reservationItem, isConjugated);
                    }
                    else
                        CreateAvailabilityReservationColumnsDto(result, reservationItem, isConjugated, allPropertyRoomsList);
                }
                else
                    CreateAvailabilityReservationColumnsDto(result, reservationItem, isConjugated, allPropertyRoomsList);
            }

            return result;
        }

        private void DuplicateAvailabilityReservationColumnsDto(List<AvailabilityReservationColumnsDto> result, List<RoomDto> childrenRooms, ReservationItem reservationItem, bool isConjugated)
        {
            foreach (var childrenRoom in childrenRooms)
            {
                result.Add(new AvailabilityReservationColumnsDto
                {
                    Id = reservationItem.Id,
                    RoomId = childrenRoom.Id,
                    ReservationId = reservationItem.ReservationId,
                    RoomTypeId = childrenRoom.RoomTypeId,
                    AdultCount = reservationItem.AdultCount,
                    ChildCount = reservationItem.ChildCount,
                    End = (reservationItem.CheckOutDate ?? reservationItem.EstimatedDepartureDate).ToString(),
                    GuestName = reservationItem.GuestReservationItemList.FirstOrDefault().GuestName,
                    isConjugated = isConjugated,
                    Start = (reservationItem.CheckInDate ?? reservationItem.EstimatedArrivalDate).ToString(),
                    Status = reservationItem.ReservationItemStatusId,
                    StartFormatted = (reservationItem.CheckInDate ?? reservationItem.EstimatedArrivalDate).ToString("s"),
                    EndFormatted = (reservationItem.CheckOutDate ?? reservationItem.EstimatedDepartureDate).ToString("s")
                });
            }
        }

        private void CreateAvailabilityReservationColumnsDto(List<AvailabilityReservationColumnsDto> result, ReservationItem reservationItem, bool isConjugated, IList<RoomDto> allPropertyRoomsList)
        {
            result.Add(new AvailabilityReservationColumnsDto
            {
                Id = reservationItem.Id,
                RoomId = reservationItem.RoomId,
                ReservationId = reservationItem.ReservationId,
                RoomTypeId = reservationItem.ReceivedRoomTypeId,
                AdultCount = reservationItem.AdultCount,
                ChildCount = reservationItem.ChildCount,
                End = (reservationItem.CheckOutDate ?? reservationItem.EstimatedDepartureDate).ToString(),
                GuestName = reservationItem.GuestReservationItemList.FirstOrDefault().GuestName,
                isConjugated = isConjugated,
                Start = (reservationItem.CheckInDate ?? reservationItem.EstimatedArrivalDate).ToString(),
                Status = reservationItem.ReservationItemStatusId,
                StartFormatted = (reservationItem.CheckInDate ?? reservationItem.EstimatedArrivalDate).ToString("s"),
                EndFormatted = (reservationItem.CheckOutDate ?? reservationItem.EstimatedDepartureDate).ToString("s"),
                RoomNumber = allPropertyRoomsList.Where( x=> x.Id == reservationItem.RoomId).FirstOrDefault().RoomNumber,
                ReservationCode = reservationItem.Reservation.ReservationCode,
                RoomTypeName = reservationItem.ReceivedRoomType.Abbreviation,
                ReservationItemId = reservationItem.Id,
                ReservationItemStatusId = reservationItem.ReservationItemStatusId
                
            });
        }

        public void ConfirmReservation(long reservationId)
        {
            var requestDto = new DefaultLongRequestDto();
            requestDto.Id = reservationId;
            requestDto.Expand = "reservationItemList";

            var reservation = this._reservationAppDomainService.Get(requestDto);

            if (reservation != null && reservation.ReservationItemList != null)
            {
                var newStatus = ReservationStatus.Confirmed;

                foreach (var reservationItem in reservation.ReservationItemList.Where(e => e.ReservationItemStatusId == (int)ReservationStatus.ToConfirm))
                {
                    var oldReservationItem = _reservationItemStatusDomainService.GetNewInstanceWithStatusId(reservationItem);

                    var builder = new ReservationItem.Builder(Notification, reservationItem, _roomReadRepository, oldReservationItem, reservation.PropertyId);

                    builder.WithReservationItemStatus(newStatus);

                    _reservationItemDomainService.Update(builder);
                }
            }
        }

        public void ValidateDuplicatesRoomsInReservationDto(ReservationDto reservationDto)
        {
            var guestReservationItemWithReservationItemList = new List<GuestReservationItemWithReservationItemDto>();

            foreach (var reservationItemDto in reservationDto.ReservationItemList)
            {
                foreach (var guestReservationItemDto in reservationItemDto.GuestReservationItemList)
                {
                    if(reservationItemDto.RoomId.HasValue)
                        guestReservationItemWithReservationItemList.Add(new GuestReservationItemWithReservationItemDto {
                            RoomId = reservationItemDto.RoomId.Value,
                            ReservationItemCode = reservationItemDto.ReservationItemCode,
                            ReservationCode = reservationDto.ReservationCode,
                            InitialDate = reservationItemDto.CheckInDate ?? reservationItemDto.EstimatedArrivalDate,
                            FinalDate = reservationItemDto.CheckOutDate ?? reservationItemDto.EstimatedDepartureDate,
                        });
                }
            }
                        
            foreach (var guestReservationItemWithReservationItem in guestReservationItemWithReservationItemList)
            {
                if (guestReservationItemWithReservationItemList.Any(e =>
                         e.RoomId == guestReservationItemWithReservationItem.RoomId &&
                         e.InitialDate.Date <= guestReservationItemWithReservationItem.FinalDate.Date &&
                         e.FinalDate.Date > guestReservationItemWithReservationItem.InitialDate.Date &&
                         e.ReservationItemCode != guestReservationItemWithReservationItem.ReservationItemCode
                    ))
                {
                    Notification.Raise(Notification
                        .DefaultBuilder
                        .WithMessage(AppConsts.LocalizationSourceName, ReservationItemEnum.Error.DuplicateRoomsInReservationItem)
                        .Build());

                    return;
                }
            }
        }        
    }
}