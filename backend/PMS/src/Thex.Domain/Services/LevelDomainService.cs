﻿using System;
using System.Collections.Generic;
using Thex.Domain.Entities;
using Thex.Domain.Interfaces.Repositories;
using Thex.Domain.Interfaces.Services;
using Thex.Dto;
using Thex.Kernel;
using Tnf.Domain.Services;
using Tnf.Notifications;

namespace Thex.Domain.Services
{
    public class LevelDomainService : DomainService<Level>, ILevelDomainService
    {
        private readonly ILevelRepository _repository;
        private readonly IApplicationUser _applicationUser;

        public LevelDomainService(ILevelRepository repository,
           IApplicationUser applicationUser,
          INotificationHandler notificationHandler)
          : base(repository, notificationHandler)
        {
            _repository = repository;
            _applicationUser = applicationUser;
        }


        public void Create(Level.Builder levelBuilder)
        {
            var level = levelBuilder.Build();
            level.PropertyId = int.Parse(_applicationUser.PropertyId);
            _repository.Create(level);
        }

        public void DeleteLevel(Level.Builder levelBuilder)
        {
            var level = levelBuilder.Build();
            level.IsDeleted = true;
            level.DeletionTime = DateTime.UtcNow.ToZonedDateTimeLoggedUser();
            level.DeleterUserId = _applicationUser.UserUid;
            _repository.DeleteLevel(level);
        }

        public void ToggleAndSaveIsActive(Guid id)
        {
            _repository.ToggleAndSaveIsActive(id);

        }

        public void Update(Level.Builder levelBuilder)
        {
            var level = levelBuilder.Build();
            _repository.Update(level);
        }

        

        public void UpdateRange(ICollection<Level> levelList)
            => _repository.UpdateRange(levelList);


        public void Insert(Level level)
            => _repository.Insert(level);
    }
}
