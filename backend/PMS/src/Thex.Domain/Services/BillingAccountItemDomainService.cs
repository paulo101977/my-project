﻿//  <copyright file="BillingItemAccountDomainService.cs" company="TOTVS">
//  Copyright (c) TOTVS. All rights reserved.
//  </copyright>
namespace Thex.Domain.Services
{

    using Thex.Domain.Entities;
    using Thex.Domain.Interfaces.Repositories;
    using Thex.Domain.Services.Interfaces;
    using System.Collections.Generic;
    using System.Linq;

    using Tnf.Domain.Services;
    using Tnf.Notifications;
    using Thex.Common.Enumerations;
    using Thex.Dto;
    using System;
    using Thex.Common;

    public class BillingAccountItemDomainService : DomainService<BillingAccountItem>, IBillingAccountItemDomainService
    {
        private readonly IBillingAccountItemRepository _repository;
        private readonly IBillingItemRepository _billingItemRepository;

        public BillingAccountItemDomainService(IBillingAccountItemRepository repository, IBillingItemRepository billingItemRepository,
            INotificationHandler notificationHandler)
            : base(repository, notificationHandler)
        {
            _repository = repository;
            _billingItemRepository = billingItemRepository;
        }

        public decimal CalculateTax(decimal amount, decimal taxPercentage)
        {
            return Math.Round((amount * (taxPercentage / 100)), 2);
        }

        public decimal CalculateAmountWithPercentage(decimal amount, decimal taxPercentage)
        {
            return amount - (taxPercentage / 100 * amount);
        }

        public void UpdateBillingAccountItemToReversed(BillingAccountItem billingAccountItem, int reasonId, string observation = "")
        {
            billingAccountItem.SetToReversed();
            billingAccountItem.SetReason(reasonId);

            if (!String.IsNullOrEmpty(observation))
                billingAccountItem.SetObservation(observation);

            _repository.Update(billingAccountItem);
        }

        public void UpdateBillingAccountItemToCreditNote(BillingAccountItem billingAccountItem)
        {
            billingAccountItem.SetToReversed();
            _repository.Update(billingAccountItem);
        }

        public bool CheckIfBillingItemIsTax(BillingAccountItem billingAccountItem)
        {
            if(!billingAccountItem.BillingItemId.HasValue)
                return false;

            if (billingAccountItem.BillingItem == null)
                billingAccountItem.BillingItem = _billingItemRepository.Get(new DefaultIntRequestDto(billingAccountItem.BillingItemId.Value));

            return billingAccountItem.BillingItem.BillingItemTypeId == (int)BillingItemTypeEnum.Tax;
        }

        public void UndoPartial(List<BillingAccountItem> originalBillingAccountItemList, List<BillingAccountItem> billingAccountItemList)
        {
            foreach (var originalItem in originalBillingAccountItemList)
            {
                var children = billingAccountItemList.Where(e => e.BillingItemId == originalItem.BillingItemId && e.Id != originalItem.Id);

                foreach (var child in children)
                {
                    originalItem.Amount += child.Amount;

                    _repository.Delete(child);
                }

                if(originalItem.Amount == originalItem.OriginalAmount)
                {
                    originalItem.BillingAccountItemTypeId = originalItem.BillingAccountItemTypeIdLastSource ?? originalItem.BillingAccountItemTypeId;
                    originalItem.BillingAccountItemTypeIdLastSource = null;
                    originalItem.PartialParentBillingAccountItemId = null;
                }
            }
        }

        public void ValidateUndoPartial(List<BillingAccountItem> billingAccountItemList, List<BillingAccountItem> partialParentBillingAccountItemList)
        {
            // cannot have an item that has been discounted
            if (billingAccountItemList.Any(e => e.BillingAccountItemTypeId == (int)BillingAccountItemTypeEnum.BillingAccountItemTypeDiscount))
            {
                //cannot have item that already has discount applied
                Notification.Raise(Notification.DefaultBuilder
                .WithMessage(AppConsts.LocalizationSourceName, BillingAccountItemEnum.Error.BillingAccountItemsCanNotBeDiscountItem)
                .WithDetailedMessage(AppConsts.LocalizationSourceName, BillingAccountItemEnum.Error.BillingAccountItemsCanNotBeDiscountItem)
                .Build()); return;
            }

            // cannot have an item that has been reversed
            if (billingAccountItemList.Any(e => e.WasReversed || e.BillingAccountItemTypeId == (int)BillingAccountItemTypeEnum.BillingAccountItemTypeReversal))
            {
                // cannot have item that has already been reversed
                Notification.Raise(Notification.DefaultBuilder
                .WithMessage(AppConsts.LocalizationSourceName, BillingAccountItemEnum.Error.BillingAccountItemsCanNotBeReversalItem)
                .WithDetailedMessage(AppConsts.LocalizationSourceName, BillingAccountItemEnum.Error.BillingAccountItemsCanNotBeReversalItem)
                .Build()); return;
            }

            // only items that have been split
            // and that belong to the same FATHER that was part of the same division cycle
            var billingAccountItemIdCount = billingAccountItemList
                .Where(e => e.PartialParentBillingAccountItemId.HasValue)
                .Select(e => e.PartialParentBillingAccountItemId)
                .Distinct()
                .Count();

            if (billingAccountItemIdCount != 1)
            {
                // notification
                // there are items that are from other divisions
                Notification.Raise(Notification.DefaultBuilder
                .WithMessage(AppConsts.LocalizationSourceName, BillingAccountItemEnum.Error.BillingAccountItemsMustBeFromTheSameDivision)
                .WithDetailedMessage(AppConsts.LocalizationSourceName, BillingAccountItemEnum.Error.BillingAccountItemsMustBeFromTheSameDivision)
                .Build()); return;
            }

            if (partialParentBillingAccountItemList.Any())
            {
                // only items that are in the same account
                var partialBillingAccountItemList = partialParentBillingAccountItemList
                                    .GroupBy(x => new { x.BillingAccountId })
                                    .Select(gp => new
                                    {
                                        gp.Key.BillingAccountId
                                    });

                if (partialBillingAccountItemList.Count() > 1)
                {
                    // notification
                    // there are items that are from other accounts
                    Notification.Raise(Notification.DefaultBuilder
                    .WithMessage(AppConsts.LocalizationSourceName, BillingAccountItemEnum.Error.BillingAccountItemsMustBeFromTheSameBillingAccount)
                    .WithDetailedMessage(AppConsts.LocalizationSourceName, BillingAccountItemEnum.Error.BillingAccountItemsMustBeFromTheSameBillingAccount)
                    .Build()); return;
                }
            }          
        }
    }
}