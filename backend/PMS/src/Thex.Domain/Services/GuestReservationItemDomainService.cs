﻿//  <copyright file="GuestReservationItemDomainService.cs" company="TOTVS">
//  Copyright (c) TOTVS. All rights reserved.
//  </copyright>

using Thex.Domain.Rules;
using Thex.Domain.Specifications.GuestReservationItem;
using Thex.Dto;

namespace Thex.Domain.Services
{
    using Thex.Common;
    using Thex.Common.Enumerations;
    using Thex.Domain.Entities;
    using Thex.Domain.Interfaces.Repositories;
    using Thex.Domain.Services.Interfaces;
    using Tnf.Domain.Services;
    using System.Linq;

    using Thex.Domain.Interfaces.Services;
    using System.Collections.Generic;
    using Thex.Common.Extensions;
    using System;
    using Tnf.Repositories;
    using Tnf.Notifications;
    using Thex.Dto.GuestReservationItem;

    public class GuestReservationItemDomainService : DomainService<GuestReservationItem>, IGuestReservationItemDomainService
    {
        private readonly IGuestReservationItemReadRepository _repository;
        private readonly IRepository<ReservationItem> _reservationItemRepository;
        private readonly IReservationItemStatusDomainService _reservationItemStatusDomainService;
        

        public GuestReservationItemDomainService(IGuestReservationItemReadRepository repository, 
            IRepository<ReservationItem> reservationItemRepository,
            IReservationItemStatusDomainService reservationItemStatusDomainService,
            INotificationHandler notificationHandler)
            : base(repository, notificationHandler)
        {
            _repository = repository;
            _reservationItemRepository = reservationItemRepository;
            _reservationItemStatusDomainService = reservationItemStatusDomainService;
        }
        
        public bool HasValidReservationItemStatusToModifyOrDelete(GuestReservationItem guestReservationItem, ReservationItem reservationItem = null)
        {
            if (reservationItem == null)
                reservationItem = _reservationItemRepository.Get(new DefaultLongRequestDto(guestReservationItem.ReservationItemId));

            var specification = new HasValidReservationItemStatusToModifyOrDeleteSpecification();
            return specification.IsSatisfiedBy(reservationItem);
        }
        

        public void Associate(GuestReservationItemDto dto, int? childAgeParameter)
        {
            var guestReservationItem = _repository.Get(new DefaultLongRequestDto(dto.Id));

            var builder = new GuestReservationItem.Builder(Notification, guestReservationItem, childAgeParameter);

            builder
                .WithGuestId(dto.GuestId)
                .WithGuestName(dto.GuestName)
                .WithPreferredName(dto.PreferredName)
                .WithGuestEmail(dto.GuestEmail)
                .WithGuestDocumentTypeId(dto.GuestDocumentTypeId)
                .WithGuestDocument(dto.GuestDocument)
                .WithGuestCitizenship(dto.GuestCitizenship)
                .WithGuestLanguage(dto.GuestLanguage)
                .WithIsIncognito(dto.IsIncognito)
                .WithGuestTypeId(dto.GuestTypeId)
                .WithPctDailyRate(dto.PctDailyRate)
                .WithEstimatedArrivalDate(dto.EstimatedArrivalDate)
                .WithEstimatedDepartureDate(dto.EstimatedDepartureDate)
                .WithChildAge(dto.ChildAge)
                .WithIsChild(guestReservationItem.IsChild)
                .WithGuestRegistrationId(dto.GuestRegistrationId);
                
            var newGuestReservationItem = builder.Build();

            if (Notification.HasNotification())
                return;



            var reservationItem = _reservationItemRepository.Get(new DefaultLongRequestDto(newGuestReservationItem.ReservationItemId));

            if (!this._reservationItemStatusDomainService
                .CanChangeReservationItem(reservationItem.ReservationItemStatusId))
            {
                RaiseNotification(GuestReservationItem.EntityError.ReservationItemStatusDoesNotAllowAssociateToGuest, GuestReservationItem.EntityError.ReservationItemStatusDoesNotAllowAssociateToGuest);
            }

            if (Notification.HasNotification())
                return;

            var sumPctDailyRateItems = _repository.GetSumOfPctDailyRateItemsByReservationIdExceptId(newGuestReservationItem.ReservationItemId, dto.Id);

            if (GuestReservationItemDomainRules.SumOfListOfPctDailyRateGreaterThanAllowed(sumPctDailyRateItems, newGuestReservationItem.PctDailyRate))
            {
                RaiseNotification(GuestReservationItem.EntityError.GuestReservationItemSumOfListOfPctDailyRateGreaterThanAllowed, GuestReservationItem.EntityError.GuestReservationItemSumOfListOfPctDailyRateGreaterThanAllowed);
            }

            if (GuestReservationItemDomainRules.EstimatedDateWithInvalidPeriodBasedOnReservation(reservationItem,newGuestReservationItem))
            {
                RaiseNotification(GuestReservationItem.EntityError.GuestReservationItemEstimatedDateWithInvalidPeriodBasedOnReservation, GuestReservationItem.EntityError.GuestReservationItemEstimatedDateWithInvalidPeriodBasedOnReservation);
            }
            
            if (!HasValidReservationItemStatusToModifyOrDelete(newGuestReservationItem, reservationItem))
            {
                RaiseNotification(GuestReservationItem.EntityError.GuestReservationItemHasInvalidReservationItemStatusToAssociate, GuestReservationItem.EntityError.GuestReservationItemHasInvalidReservationItemStatusToAssociate);
            }

            if (Notification.HasNotification())
                return;

            _repository.Update(newGuestReservationItem);
        }

        public void Disassociate(long id)
        {
            var guestReservationItem = _repository.Get(new DefaultLongRequestDto(id));

            var reservationItem = _reservationItemRepository.Get(new DefaultLongRequestDto(guestReservationItem.ReservationItemId));

            if (!this._reservationItemStatusDomainService
                    .CanChangeReservationItem(reservationItem.ReservationItemStatusId))
            {
                RaiseNotification(GuestReservationItem.EntityError.ReservationItemStatusDoesNotAllowDisassociateToGuest, GuestReservationItem.EntityError.ReservationItemStatusDoesNotAllowDisassociateToGuest);
                return;
            }

            var builder = new GuestReservationItem.Builder(Notification, guestReservationItem);

            builder
                .WithGuestId(null)
                .WithGuestName(null)
                .WithPreferredName(null)
                .WithGuestEmail(null)
                .WithGuestDocumentTypeId(null)
                .WithGuestDocument(null)
                .WithGuestCitizenship(null)
                .WithGuestLanguage(null)
                .WithChildAge(0);

            var newGuestReservationItem = builder.Build();

            if (!HasValidReservationItemStatusToModifyOrDelete(newGuestReservationItem))
            {
                RaiseNotification(GuestReservationItem.EntityError.GuestReservationItemHasInvalidReservationItemStatusToDissociate, GuestReservationItem.EntityError.GuestReservationItemHasInvalidReservationItemStatusToDissociate);
                return;
            }

            _repository.Update(newGuestReservationItem);
        }


        public void CheckSumOfPctDailyIs100(GuestReservationItemDto dto, ReservationItemDto reservationItemDto)
        {
            var sumOfPctDaily = reservationItemDto.GuestReservationItemList.Where(exp => exp.Id != dto.Id).Sum(exp => exp.PctDailyRate);

            if (sumOfPctDaily + dto.PctDailyRate != 100)
            {
                RaiseNotification(GuestReservationItem.EntityError.GuestReservationItemTotalChildsInvalid, GuestReservationItem.EntityError.GuestReservationItemTotalChildsInvalid);
            }
        }


        public void CheckEstimatedDateWithInvalidPeriodBasedOnReservationDto(GuestReservationItemDto guestReservationItemDto, ReservationItemDto reservationItemDto, bool update)
        {
            if (GuestReservationItemDomainRules.EstimatedDateWithInvalidPeriodBasedOnReservationDto(reservationItemDto, guestReservationItemDto))
            {
                RaiseNotification(GuestReservationItem.EntityError.GuestReservationItemEstimatedDateWithInvalidPeriodBasedOnReservation, GuestReservationItem.EntityError.GuestReservationItemEstimatedDateWithInvalidPeriodBasedOnReservation);
            }
        }

        private void RaiseNotification(GuestReservationItem.EntityError notificationEnumMessage, GuestReservationItem.EntityError notificationEnumDetailedMessage)
        {
            Notification.Raise(Notification
                .DefaultBuilder
                .WithMessage(AppConsts.LocalizationSourceName, notificationEnumMessage)
                .WithDetailedMessage(AppConsts.LocalizationSourceName, notificationEnumDetailedMessage)
                .Build());
        }


        public void UpdatebasedOnGuestRegistration(GuestRegistrationDto guestRegistrationDto, CountrySubdivisionTranslationDto nationality)
        {
            var guestReservationItem = _repository.Get(new DefaultLongRequestDto(guestRegistrationDto.GuestReservationItemId));

            var reservationItem = _reservationItemRepository.Get(new DefaultLongRequestDto(guestReservationItem.ReservationItemId));

            if (reservationItem.ReservationItemStatusId == (int)ReservationStatus.Canceled ||
                        reservationItem.ReservationItemStatusId == (int)ReservationStatus.Checkout)
            {
                Notification.Raise(Notification.DefaultBuilder.WithMessage(AppConsts.LocalizationSourceName, ReservationItemEnum.Error.ReservationItemCanNotBeChanged).Build());
                return;
            }

            var builder = new GuestReservationItem.Builder(Notification, guestReservationItem);

            builder
                .WithGuestId(guestRegistrationDto.GuestId)
                .WithGuestName(guestRegistrationDto.FullName)
                .WithPreferredName(guestRegistrationDto.FullName)
                .WithGuestEmail(guestRegistrationDto.Email)
                .WithGuestDocumentTypeId(guestRegistrationDto.Document.DocumentTypeId)
                .WithGuestDocument(guestRegistrationDto.Document.DocumentInformation)
                .WithGuestTypeId(guestRegistrationDto.GuestTypeId ?? default(int))
                .WithGuestCitizenship(nationality.Nationality)
                .WithGuestRegistrationId(guestRegistrationDto.Id);
                

            if(guestRegistrationDto.BirthDate.HasValue && guestReservationItem.IsChild)
                builder.WithChildAge(Convert.ToInt16(guestRegistrationDto.BirthDate.GetAge()));
            else
                builder.WithChildAge(null);

            if(guestReservationItem.GuestStatusId == (int)ReservationStatus.ToConfirm)
                builder.WithGuestStatusId((int)ReservationStatus.Confirmed);

            if(nationality != null)
            {
                builder.WithGuestCitizenship(nationality.Nationality);
                builder.WithGuestLanguage(nationality.LanguageIsoCode);
            }


            var newGuestReservationItem = builder.Build();

            if (Notification.HasNotification())
                return;

            _repository.Update(newGuestReservationItem);
        }

        public List<GuestReservationItem> GetListGuestReservationForCheckin(List<long> ids)
        {
            return _repository.GetListGuestReservationForCheckin(ids);
        }

        public void ValidateChildAge(int childAge, int maxAge)
        {
            if (childAge > maxAge)
                RaiseNotification(GuestReservationItem.EntityError.GuestReservationItemEstimatedDateWithInvalidPeriodBasedOnReservation, GuestReservationItem.EntityError.GuestReservationItemEstimatedDateWithInvalidPeriodBasedOnReservation);
        }

        public bool AnyGuestReservationItemWithStatusEqualCheckin(List<long> guestReservationIds)
        {
            return _repository.AnyGuestReservationItemWithStatusEqualCheckin(guestReservationIds);
        }

        public GuestReservationItem GetGuestReservationForCheckin(long id)
            => _repository.GetListGuestReservationForCheckin(new List<long> { id }).FirstOrDefault();

        public List<GuestReservationItem> GetListGuestReservationPerReservationItemIdForCheckin(long id)
        {
            return _repository.GetListGuestReservationPerReservationItemIdForCheckin(id);
        }

        public GuestReservationItemWithCountrySubdivisionDto GetReservationItemWithCountrySubdivision(long guestReservationItemId, string languageIsoCode)
        {
            return _repository.GetReservationItemWithCountrySubdivision(guestReservationItemId, languageIsoCode);
        }
    }
}