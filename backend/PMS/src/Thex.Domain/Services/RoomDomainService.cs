﻿//  <copyright file="RoomDomainService.cs" company="TOTVS">
//  Copyright (c) TOTVS. All rights reserved.
//  </copyright>
namespace Thex.Domain.Services
{

    using Thex.Common;
    using Thex.Domain.Entities;
    using Thex.Domain.Interfaces.Repositories;
    using Thex.Domain.Services.Interfaces;
    using System.Collections.Generic;
    using System.Linq;

    using Tnf.Domain.Services;
    using Tnf.Notifications;
    using Thex.Dto.Availability;
    using Thex.Dto;
    
    using System;
    using Thex.Common.Enumerations;
    using Tnf.Dto;

    public class RoomDomainService : DomainService<Room>, IRoomDomainService
    {
        private readonly IRoomRepository _repository;

        public RoomDomainService(IRoomRepository repository,
            INotificationHandler notificationHandler)
            : base(repository, notificationHandler)
        {
            _repository = repository;
        }

        public bool HasReservationForwards(int id)
        {
            return false;
        }

        public bool ToggleActivation(int id)
        {
            Room room = Repository.Get(new DefaultIntRequestDto(id));

            if (room.IsActive && HasReservationForwards(id))
            {
                RaiseRoomHasReservationForwadNotification();
                return false;
            }
            else
            {
                return _repository.ToggleAndSaveIsActive(id);
            } 
        }

        private void RaiseRoomHasReservationForwadNotification()
        {
            Notification.Raise(Notification.DefaultBuilder
                .WithMessage(AppConsts.LocalizationSourceName, Room.EntityError.RoomTypeHasReserveForward)
                .WithDetailedMessage(
                    AppConsts.LocalizationSourceName,
                    Room.EntityError.RoomTypeHasReserveForward)
                .Build());
        }

        public int Create(Room.Builder roomBuilder, IList<int> childRoomIdList)
        {
            try
            {
                var room = roomBuilder.Build();
                var roomId = _repository.InsertAndSaveChanges(room).Id;

                UpdateChildRoomList(childRoomIdList, roomId);

                return roomId;
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null && ex.InnerException.Message.Contains("UK_Room_PropertyId_RoomNumber"))
                {
                    var notifications = Notification.GetAll();
                    foreach (var notification in notifications)
                    {
                        Notification.Delete(notification);
                    }

                    Notification.Raise(Notification.DefaultBuilder
                          .WithMessage(AppConsts.LocalizationSourceName, RoomEnum.Error.RoomUKKeyError)
                          .WithDetailedMessage(
                              AppConsts.LocalizationSourceName,
                              RoomEnum.Error.RoomUKKeyError)
                          .Build());

                    return 0;
                }
                else
                    throw;
            }

        }

        public void Update(Room.Builder roomBuilder, IList<int> childRoomIdList)
        {
            try
            {
                var room = roomBuilder.Build();

                var childrenIds = _repository.GetChildrenIdsOfParentExcept(room.Id, childRoomIdList);

                _repository.RemoveParentIdOfChildren(childrenIds);

                UpdateChildRoomList(childRoomIdList, room.Id);

                _repository.UpdateAndSaveChanges(room);

            }
            catch (Exception ex)
            {
                if (ex.InnerException != null && ex.InnerException.Message.Contains("UK_Room_PropertyId_RoomNumber"))
                {
                    var notifications = Notification.GetAll();
                    foreach (var notification in notifications)
                    {
                        Notification.Delete(notification);
                    }

                    Notification.Raise(Notification.DefaultBuilder
                          .WithMessage(AppConsts.LocalizationSourceName, RoomEnum.Error.RoomUKKeyError)
                          .WithDetailedMessage(
                              AppConsts.LocalizationSourceName,
                              RoomEnum.Error.RoomUKKeyError)
                          .Build());
                }
                else
                    throw;
            }

        }

        private void UpdateChildRoomList(IList<int> childRoomIdList, int roomId)
        {
            if (childRoomIdList.Any()) this._repository.UpdateChildRoomList(childRoomIdList, roomId);
            else
            {
                var childrensForRemove = GetChildrenIds(roomId);
                _repository.RemoveParentIdOfChildren(childrensForRemove);

            };
        }

        public IList<int> GetChildrenIds(int id)
        {
            return _repository.GetChildrenIds(id);
        }

        public void Delete(int id)
        {

            _repository.Delete(id);
        }

        public List<AvailabilityRoomRowDto> GetAvailabilityRoomRowDto(IListDto<RoomDto> allPropertyRooms, int roomTypeId)
        {
            var result = new List<AvailabilityRoomRowDto>();
            var allPropertyRoomsList = allPropertyRooms.Items;
            var allPropertyRoomsByRoomTypeList = allPropertyRooms.Items.Where(i => i.RoomTypeId == roomTypeId && i.IsActive);
           
            foreach (var propertyRoom in allPropertyRoomsByRoomTypeList)
            {
                if (allPropertyRoomsList.Any(d => d.ParentRoomId.HasValue && d.ParentRoomId.Value == propertyRoom.Id))
                    continue;

                result.Add(new AvailabilityRoomRowDto
                {
                    Id = propertyRoom.RoomTypeId,
                    RoomId = propertyRoom.Id,
                    Beds = null,
                    Name = propertyRoom.RoomNumber
                });
            }

            return result.Distinct().OrderBy(x => x.Name.Length).ThenBy(x => x.Name).ToList();
        }
    }
}