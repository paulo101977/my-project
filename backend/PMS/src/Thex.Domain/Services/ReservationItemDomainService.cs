﻿//  <copyright file="ReservationDomainService.cs" company="TOTVS">
//  Copyright (c) TOTVS. All rights reserved.
//  </copyright>
namespace Thex.Domain.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Thex.Common;
    using Thex.Common.Enumerations;
    using Thex.Domain.Entities;
    using Thex.Domain.Interfaces.Repositories;
    using Thex.Domain.Interfaces.Services;
    using Thex.Dto;
    using Thex.Dto.Base;
    using Thex.Kernel;
    using Tnf.Domain.Services;
    using Tnf.Notifications;
    using Tnf.Repositories;

    public class ReservationItemDomainService : DomainService<ReservationItem>, IReservationItemDomainService
    {
        private readonly IReservationItemRepository _repository;
        private readonly IReservationRepository _reservationRepository;
        private readonly IGuestReservationItemRepository _guestItemRepository;
        private readonly IReservationItemStatusDomainService _reservationItemStatusDomainService;
        private readonly IReservationBudgetRepository _reservationBudgetRepository;
        private readonly IApplicationUser _applicationUser;
        private readonly EntityFrameworkCore.ReadInterfaces.Repositories.IRoomReadRepository _roomReadRepository;

        public ReservationItemDomainService(IReservationItemRepository repository,
                                            IReservationItemStatusDomainService reservationItemStatusDomainService,
                                            IReservationRepository reservationRepository,
            INotificationHandler notificationHandler, IGuestReservationItemRepository guestReservationItemRepository,
            IApplicationUser applicationUser, IReservationBudgetRepository reservationBudgetRepository,
            EntityFrameworkCore.ReadInterfaces.Repositories.IRoomReadRepository roomReadRepository)
            : base(repository, notificationHandler)
        {
            _repository = repository;
            _reservationRepository = reservationRepository;
            _reservationItemStatusDomainService = reservationItemStatusDomainService;
            _guestItemRepository = guestReservationItemRepository;
            _reservationBudgetRepository = reservationBudgetRepository;
            _applicationUser = applicationUser;
            _roomReadRepository = roomReadRepository;
        }

        public void CancelReservationItemList(List<ReservationItem> reservationItemList, int reasonId, string cancellationDescription)
        {
            foreach (var reservationItem in reservationItemList)
                if (!_reservationItemStatusDomainService.CanChangeStatus(reservationItem, ReservationStatus.Canceled))
                {
                    this.RaiseNotification();
                    return;
                }

            foreach (var reservationItemExisting in reservationItemList)
            {
                this.UpdateReservationItemForCancel(reasonId, cancellationDescription, reservationItemExisting);
                UpdateGuestReservationItemForCancel(reservationItemExisting);
            }
        }

        public void CancelReservationItem(ReservationItem reservationItemExisting, int reasonId, string cancellationDescription)
        {
            if (!this._reservationItemStatusDomainService.CanChangeStatus(
                reservationItemExisting,
                ReservationStatus.Canceled))
            {
                this.RaiseNotification();
                return;
            }

            UpdateReservationItemForCancel(reasonId, cancellationDescription, reservationItemExisting);
            UpdateGuestReservationItemForCancel(reservationItemExisting);
        }

        private void UpdateReservationItemForCancel(int reasonId, string cancellationDescription, ReservationItem reservationItemExisting)
        {
            reservationItemExisting.CancellationDate = DateTime.UtcNow.ToZonedDateTimeLoggedUser();
            reservationItemExisting.ReasonId = reasonId;
            reservationItemExisting.CancellationDescription = cancellationDescription;
            reservationItemExisting.ReservationItemStatusId = (int)ReservationStatus.Canceled;
            this._repository.Update(reservationItemExisting);
        }

        private void UpdateGuestReservationItemForCancel(ReservationItem reservationItemExisting)
        {
            _guestItemRepository.UpdateAllGuestsToCanceled(reservationItemExisting.Id);
        }

        private void RaiseNotification()
        {
            this.Notification.Raise(
                Notification.DefaultBuilder
                    .WithMessage(AppConsts.LocalizationSourceName, ReservationItemEnum.Error.CouldNotChangeStatus)
                    .WithDetailedMessage(AppConsts.LocalizationSourceName, ReservationItemEnum.Error.CouldNotChangeStatus)
                    .Build());
        }

        public void UpdateStatusReservationItem(long reservationItemId)
        {
            var reservationItem = _repository.Get(new DefaultLongRequestDto(reservationItemId));

            var builder = new ReservationItem.Builder(Notification, reservationItem);

            builder.WithReservationItemStatus(ReservationStatus.Checkin);

            var newReservationItem = builder.Build();

            _repository.Update(newReservationItem);
        }

        public void UpdateStatusReservationItem(long reservationItemId, ReservationStatus reservationEnum)
        {
            var reservationItem = _repository.Get(new DefaultLongRequestDto(reservationItemId));
            reservationItem.ReservationItemStatusId = (int)reservationEnum;

            _repository.Update(reservationItem);
        }

        public IList<ReservationItem> UpdateReservationItemListForNoShowBySystemDate(int propertyId, DateTime systemDate)
        {
            var reservationItemToUpdate = new List<ReservationItem>();

            IList<Reservation> reservationList = _reservationRepository.GetReservationListWithConfirmedOrToConfirmStatus(propertyId);
            IList<ReservationItem> reservationItemNoShowList = new List<ReservationItem>();
            foreach (var reservation in reservationList)
            {
                var reservationEnsuresNoShow = reservation.ReservationConfirmationList.Any(r => r.EnsuresNoShow);
                var reservationItemConfirmedOrToConfirmList = reservation.ReservationItemList.Where(r => r.ReservationItemStatusId == (int)ReservationStatus.ToConfirm
                    || r.ReservationItemStatusId == (int)ReservationStatus.Confirmed);

                foreach (ReservationItem reservationItem in reservationItemConfirmedOrToConfirmList)
                {
                    var reservationItemStatusIsNoShow = false;
                    var yestarday = systemDate.Yestarday().Date;

                    if (reservationItem.EstimatedArrivalDate.Date < yestarday
                        || (reservationItem.EstimatedArrivalDate.Date == yestarday && !reservationEnsuresNoShow))
                    {
                        reservationItemStatusIsNoShow = true;
                    }
                    else if (reservationItem.EstimatedArrivalDate.Date == yestarday && reservationEnsuresNoShow)
                    {
                        reservationItemStatusIsNoShow = false;
                    }

                    if (reservationItemStatusIsNoShow)
                    {
                        reservationItemNoShowList.Add(reservationItem);
                        reservationItem.ReservationItemStatusId = (int)ReservationStatus.NoShow;
                        reservationItem.LastModificationTime = DateTime.UtcNow.ToZonedDateTimeLoggedUser(_applicationUser);
                        reservationItem.LastModifierUserId = _applicationUser.UserUid;


                        reservationItemToUpdate.Add(reservationItem);
                    }
                }
            }

            if(reservationItemToUpdate.Any())
            {
                _guestItemRepository.UpdateAllGuestsToNoShow(reservationItemToUpdate.Select(e => e.Id).ToList());
                _repository.BulkUpdate(reservationItemToUpdate);
            }

            return reservationItemNoShowList;
        }

        public (List<ReservationItem>, int) UpdateEstimatedDepartureDateFromReservationItemListBySystemDate(int propertyId, DateTime estimatedDepartureDate)
        {
            List<ReservationItem> reservationItemList = _reservationRepository.GetReservationItemListByEstimatedDepartureDateAndPropertyId(estimatedDepartureDate, propertyId);
            int countOfReservationItemListThatHaveTheirEstimatedDepartureDateChanged = 0;
            foreach (ReservationItem reservationItem in reservationItemList)
            {
                countOfReservationItemListThatHaveTheirEstimatedDepartureDateChanged++;
                reservationItem.EstimatedDepartureDate = estimatedDepartureDate.AddHours(reservationItem.EstimatedDepartureDate.Hour).AddMinutes(reservationItem.EstimatedDepartureDate.Minute);
                reservationItem.LastModificationTime = DateTime.UtcNow.ToZonedDateTimeLoggedUser(_applicationUser);
                reservationItem.LastModifierUserId = _applicationUser.UserUid;
            }

            return (reservationItemList, countOfReservationItemListThatHaveTheirEstimatedDepartureDateChanged);
        }

        public void BulkUpdate(List<ReservationItem> reservationItemList)
        {
            _repository.BulkUpdate(reservationItemList);
        }

        public IList<ReservationItem> GetAllEstimatedDepartureDateFromReservationItemListBySystemDate(int propertyId, DateTime systemDate)
        {
            IList<ReservationItem> reservationItemList = _reservationRepository.GetReservationItemListByEstimatedDepartureDateAndPropertyId(systemDate, propertyId);
            return reservationItemList;
        }

        public ReservationBudget CreateReservationBudgetToReservationItemListBySystemDate(ReservationItem reservationItem, ReservationBudget lastReservationBudget, DateTime systemDate, ReservationItemCommercialBudgetDto reservationItemCommercialBudgetDto)
        {
            if (lastReservationBudget.BudgetDay.Date == systemDate.Date)
                return null;

                var aux = new ReservationBudget();

                aux.BillingAccountItemId = lastReservationBudget.BillingAccountItemId;
                aux.BudgetDay = systemDate;
                aux.CurrencyId = lastReservationBudget.CurrencyId;
                aux.CurrencySymbol = lastReservationBudget.CurrencySymbol;
                aux.MealPlanTypeId = lastReservationBudget.MealPlanTypeId;
                aux.ReservationItemId = lastReservationBudget.ReservationItemId;
                aux.ReservationBudgetUid = Guid.NewGuid();
                aux.TenantId = lastReservationBudget.TenantId;

                if (reservationItemCommercialBudgetDto == null)
                {
                    aux.AgreementRate = lastReservationBudget.AgreementRate;
                    aux.BaseRate = lastReservationBudget.BaseRate;
                    aux.ChildRate = lastReservationBudget.ChildRate;
                    aux.ChildAgreementRate = lastReservationBudget.ChildAgreementRate;
                    aux.ChildMealPlanTypeAgreementRate = lastReservationBudget.ChildMealPlanTypeAgreementRate;
                    aux.ChildMealPlanTypeRate = lastReservationBudget.ChildMealPlanTypeRate;
                    aux.Discount = lastReservationBudget.Discount;
                    aux.ManualRate = lastReservationBudget.ManualRate;
                    aux.MealPlanTypeAgreementRate = lastReservationBudget.MealPlanTypeAgreementRate;
                    aux.MealPlanTypeBaseRate = lastReservationBudget.MealPlanTypeBaseRate;
                    aux.RateVariation = lastReservationBudget.RateVariation;
                    aux.PropertyBaseRateHeaderHistoryId = lastReservationBudget.PropertyBaseRateHeaderHistoryId;
                }
                else
                {
                    // Só pode ter um item em CommercialBudgetDayList, pois estou criando um ReservationBudget para apenas um dia e também já vêm filtrado
                    //pelo tipo de pensão e acordo comercial.
                    var commercialBudgetDay = reservationItemCommercialBudgetDto.CommercialBudgetDayList.FirstOrDefault();

                    aux.AgreementRate = commercialBudgetDay.Total;
                    aux.BaseRate = commercialBudgetDay.BaseRateAmount;
                    aux.ChildRate = commercialBudgetDay.ChildRateAmount;
                    aux.ChildAgreementRate = commercialBudgetDay.ChildRateAmountWithRatePlan == null ? 0 : commercialBudgetDay.ChildRateAmountWithRatePlan.Value;
                    aux.ChildMealPlanTypeAgreementRate = commercialBudgetDay.ChildMealPlanAmountWithRatePlan == null ? 0 : commercialBudgetDay.ChildMealPlanAmountWithRatePlan.Value;
                    aux.ChildMealPlanTypeRate = commercialBudgetDay.ChildMealPlanAmount;
                    aux.Discount = 0;
                    aux.ManualRate = aux.AgreementRate;
                    aux.MealPlanTypeAgreementRate = commercialBudgetDay.MealPlanAmountWithRatePlan == null ? 0 : commercialBudgetDay.MealPlanAmountWithRatePlan.Value;
                    aux.MealPlanTypeBaseRate = commercialBudgetDay.MealPlanAmount;
                    aux.RateVariation = reservationItemCommercialBudgetDto.RateVariation;
                    aux.PropertyBaseRateHeaderHistoryId = commercialBudgetDay.BaseRateHeaderHistoryId;
                }

                aux.SetTenant(_applicationUser);

            return aux;
        }

        public void UpdateDepartureDateReservationItem(long reservationItemId, DateTime date)
        {
            var reservationItem = _repository.Get(new DefaultLongRequestDto(reservationItemId));
            reservationItem.EstimatedDepartureDate = date;

            _repository.Update(reservationItem);
        }

        public void RemoveRoom(long reservationItemId)
        {
            _reservationRepository.RemoveRoom(reservationItemId);

        }

        public ReservationItem GetNewInstanceWithReceivedRoomTypeIdAndDates(ReservationItem reservationItem)
        {
            return new ReservationItem()
            {
                ReceivedRoomTypeId = reservationItem.ReceivedRoomTypeId,
                EstimatedArrivalDate = reservationItem.EstimatedArrivalDate,
                EstimatedDepartureDate = reservationItem.EstimatedDepartureDate,
                CheckInDate = reservationItem.CheckInDate,
                CheckOutDate = reservationItem.CheckOutDate
            };
        }

        public ReservationStatus GetStatusByReservationConfirmation(ReservationConfirmation reservationConfirmation)
        {
            var paymentTypes = new int[] {
                (int)PaymentTypeEnum.Deposit,
                (int)PaymentTypeEnum.CreditCard
            };

            if (reservationConfirmation != null &&
                    ((paymentTypes.Contains(reservationConfirmation.PaymentTypeId) && reservationConfirmation.ReservationItemIsConfirmed.HasValue && reservationConfirmation.ReservationItemIsConfirmed.Value) ||
                    (!reservationConfirmation.Deadline.HasValue)))
                return ReservationStatus.Confirmed;
            else
                return ReservationStatus.ToConfirm;
        }

        public ReservationItem CleanRoomIdIfIsNotAvailable(ReservationItem reservationItem)
        {
            if (!ValidadeIfRoomIsAvailable(reservationItem.EstimatedArrivalDate, reservationItem.EstimatedDepartureDate,
                int.Parse(_applicationUser.PropertyId), reservationItem.ReceivedRoomTypeId, reservationItem.RoomId))
                reservationItem.RoomId = null;

            return reservationItem;
        }

        private bool ValidadeIfRoomIsAvailable(DateTime estimatedArrivalDate, DateTime estimatedDepartureDate, int propertyId, int receivedRoomTypeId, int? roomId)
        {
            return _roomReadRepository.RoomIsAvailableByPeriodOfRoomType(
               new PeriodDto { InitialDate = estimatedArrivalDate, FinalDate = estimatedDepartureDate },
               propertyId,
               receivedRoomTypeId,
               roomId.Value);
        }

        public void ConfirmReservationItem(long reservationItemId)
        {
            var reservationItem = _repository.Get(new DefaultLongRequestDto(reservationItemId));

            if (reservationItem != null)
            {
                var newStatus = (int)ReservationStatus.Confirmed;

                reservationItem.ReservationItemStatusId = newStatus;

                _repository.Update(reservationItem);

                _guestItemRepository.UpdateAllGuestToConfirmed(reservationItemId);
            }
        }

        public void CreateReservationBudgetToReservationItemListBySystemDate(IList<ReservationItem> reservationItemList, DateTime systemDate)
        {
            var reservationBudgetList = new List<ReservationBudget>();

            foreach (var reservationItem in reservationItemList)
            {
                ReservationBudget lastReservationBudget = _reservationBudgetRepository.GetLastReservationBudget(reservationItem);

                if (lastReservationBudget.BudgetDay.Date == systemDate.Date)
                    continue;

                ReservationBudget aux = new ReservationBudget();

                aux.BillingAccountItemId = lastReservationBudget.BillingAccountItemId;
                aux.BudgetDay = systemDate;
                aux.CurrencyId = lastReservationBudget.CurrencyId;
                aux.CurrencySymbol = lastReservationBudget.CurrencySymbol;

                aux.MealPlanTypeId = lastReservationBudget.MealPlanTypeId;

                aux.ReservationItemId = lastReservationBudget.ReservationItemId;
                aux.ReservationBudgetUid = Guid.NewGuid();
                aux.TenantId = lastReservationBudget.TenantId;

                aux.AgreementRate = lastReservationBudget.AgreementRate;
                aux.BaseRate = lastReservationBudget.BaseRate;
                aux.ChildRate = lastReservationBudget.ChildRate;
                aux.ChildAgreementRate = lastReservationBudget.ChildAgreementRate;
                aux.ChildMealPlanTypeAgreementRate = lastReservationBudget.ChildMealPlanTypeAgreementRate;
                aux.ChildMealPlanTypeRate = lastReservationBudget.ChildMealPlanTypeRate;
                aux.Discount = lastReservationBudget.Discount;
                aux.ManualRate = lastReservationBudget.ManualRate;
                aux.MealPlanTypeAgreementRate = lastReservationBudget.MealPlanTypeAgreementRate;
                aux.MealPlanTypeBaseRate = lastReservationBudget.MealPlanTypeBaseRate;
                aux.RateVariation = lastReservationBudget.RateVariation;
                aux.PropertyBaseRateHeaderHistoryId = lastReservationBudget.PropertyBaseRateHeaderHistoryId;

                aux.TenantId = _applicationUser.TenantId;
                aux.CreationTime = DateTime.UtcNow.ToZonedDateTimeLoggedUser(_applicationUser);
                aux.CreatorUserId = _applicationUser.UserUid;
                aux.LastModificationTime = DateTime.UtcNow.ToZonedDateTimeLoggedUser(_applicationUser);
                aux.LastModifierUserId = _applicationUser.UserUid;

                reservationBudgetList.Add(aux);
            }

            _reservationBudgetRepository.BulkInsert(reservationBudgetList);

        }

        public ReservationItem ChangeEstimatedArrivalDate(ReservationItem reservationItem, DateTime? systemDate)
        {
            if (systemDate.HasValue)
                reservationItem.EstimatedArrivalDate = systemDate.Value.AddHours(reservationItem.EstimatedArrivalDate.Hour);

            return reservationItem;
        }

        public void ValidateReactivation(ReservationItem reservationItem, DateTime? systemDate, ReservationStatus newStatus)
        {
            if (!(systemDate.HasValue && systemDate.Value.Date >= systemDate.Value.Date &&
                                   systemDate.Value.Date < reservationItem.EstimatedDepartureDate.Date))
            {
                Notification.Raise(Notification.DefaultBuilder
                       .WithMessage(AppConsts.LocalizationSourceName, ReservationItem.EntityError.ReservationItemInvalidPeriodNoShow)
                       .WithDetailedMessage(AppConsts.LocalizationSourceName, ReservationItem.EntityError.ReservationItemInvalidPeriodNoShow)
                       .Build());
                return;
            }

            if (reservationItem.ReservationItemStatusId != (int)ReservationStatus.NoShow ||
                  !_reservationItemStatusDomainService.CanChangeStatus(reservationItem, newStatus))
            {
                Notification.Raise(Notification.DefaultBuilder
                        .WithMessage(AppConsts.LocalizationSourceName, ReservationItemEnum.Error.CouldNotChangeStatus)
                        .WithDetailedMessage(AppConsts.LocalizationSourceName, ReservationItemEnum.Error.CouldNotChangeStatus)
                        .Build());
                return;
            }          
        }
    }
}