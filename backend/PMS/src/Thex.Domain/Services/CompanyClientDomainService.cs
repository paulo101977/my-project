﻿//  <copyright file="RoomDomainService.cs" company="TOTVS">
//  Copyright (c) TOTVS. All rights reserved.
//  </copyright>
namespace Thex.Domain.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Thex.Common;
    using Thex.Common.Enumerations;
    using Thex.Domain.Entities;
    using Thex.Domain.Interfaces.Repositories;
    using Thex.Domain.Services.Interfaces;

    using Thex.Dto;
    using Tnf.Domain.Services;
    using Tnf.Notifications;

    public class CompanyClientDomainService : DomainService<CompanyClient>, ICompanyClientDomainService
    {
        private readonly ICompanyClientRepository _repository;

        public CompanyClientDomainService(ICompanyClientRepository repository,
            INotificationHandler notificationHandler)
            : base(repository, notificationHandler)
        {
            _repository = repository;
        }

        public void LocationListQuantityIsValid(ICollection<LocationDto> locationList)
        {
            var maxLocations = 5;

            if (locationList.Count > maxLocations)
                RaiseNotification(CompanyClient.EntityError.CompanyClientLocationListGreaterThanAllowed);
        }

        public void LocationListTypesIsValid(ICollection<LocationDto> locationList, char personType)
        {
            var hasDuplicates = locationList
                .Select(exp => exp.LocationCategoryId)
                .GroupBy(exp => exp)
                .Any(exp => exp.Count() > 1);

            if (hasDuplicates)
            {
                RaiseNotification(CompanyClient.EntityError.CompanyClientLocationListHasDuplicates);
            }
            else
            {
                var hasCoomercialType = locationList.Any(l => l.LocationCategoryId == (int)LocationCategoryEnum.Commercial);

                if (!hasCoomercialType && personType == (char)PersonTypeEnum.Legal)
                {
                    RaiseNotification(CompanyClient.EntityError.CompanyClientMustHaveCommercialLocation);
                }
            }     
        }

        public void ValidateBasicDtoProperties(CompanyClientDto companyClient)
        {
            var validTypes = new List<PersonTypeEnum>
            {
                PersonTypeEnum.Natural,
                PersonTypeEnum.Legal
            };

            if (!validTypes.Contains((PersonTypeEnum)companyClient.PersonType))
                RaiseNotification(CompanyClient.EntityError.CompanyClientHasInvalidPersonType);

            if (string.IsNullOrEmpty(companyClient.Email))
                RaiseNotification(CompanyClient.EntityError.CompanyClientMustHaveEmail);          
        }


        public void ValidateBasicDtoPropertiesByNewSparseAccount(CompanyClientDto companyClient)
        {
            var validTypes = new List<PersonTypeEnum>
            {
                PersonTypeEnum.Natural,
                PersonTypeEnum.Legal
            };

            if (!validTypes.Contains((PersonTypeEnum)companyClient.PersonType))
                RaiseNotification(CompanyClient.EntityError.CompanyClientHasInvalidPersonType);

            if (string.IsNullOrEmpty(companyClient.Email))
                RaiseNotification(CompanyClient.EntityError.CompanyClientMustHaveEmail);
        }

        private void RaiseNotification(CompanyClient.EntityError notificationEnum)
        {
            Notification.Raise(Notification
                .DefaultBuilder
                .WithMessage(AppConsts.LocalizationSourceName, notificationEnum)
                .Build());
        }

        public void DocumentListIsValid(CompanyClientDto companyClient, string isoCode)
        {
            var documentDtoList = companyClient.DocumentList;

            if(isoCode.ToUpper().Equals("BR"))
            {
                if ((PersonTypeEnum)companyClient.PersonType == PersonTypeEnum.Natural && !PersonHasMainDocument(documentDtoList, DocumentTypeEnum.BrNaturalPersonCPF))
                    RaiseNotification(CompanyClient.EntityError.PersonNaturalDoesNotHaveMainDocument);
                else if ((PersonTypeEnum)companyClient.PersonType == PersonTypeEnum.Legal && !PersonHasMainDocument(documentDtoList, DocumentTypeEnum.BrLegalPersonCNPJ))
                    RaiseNotification(CompanyClient.EntityError.PersonLegalDoesNotHaveMainDocument);
            }
            else
            {
                if(documentDtoList == null || documentDtoList.Any(d => string.IsNullOrWhiteSpace(d.DocumentInformation)))
                {
                    if ((PersonTypeEnum)companyClient.PersonType == PersonTypeEnum.Natural)
                        RaiseNotification(CompanyClient.EntityError.PersonNaturalDoesNotHaveMainDocument);
                    else if ((PersonTypeEnum)companyClient.PersonType == PersonTypeEnum.Legal)
                        RaiseNotification(CompanyClient.EntityError.PersonLegalDoesNotHaveMainDocument);
                }
            }  
        }

        private bool PersonHasMainDocument(ICollection<DocumentDto> documentDtoList, DocumentTypeEnum t)
        {
            return documentDtoList.Any(d => d.DocumentTypeId == Convert.ToInt32(t));
        }
    }
}