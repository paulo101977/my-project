﻿using System;
using System.Collections.Generic;
using System.Linq;
using Thex.Common;
using Thex.Common.Enumerations;
using Thex.Domain.Entities;
using Thex.Domain.Interfaces.Repositories;
using Thex.Domain.Interfaces.Services;
using Thex.Dto;
using Tnf.Domain.Services;
using Tnf.Notifications;
using Tnf.Repositories;

namespace Thex.Domain.Services
{
    public class PropertyRateStrategyDomainService : DomainService<PropertyRateStrategy>, IPropertyRateStrategyDomainService
    {
        public PropertyRateStrategyDomainService(
            IRepository<PropertyRateStrategy> repository,
            INotificationHandler notificationHandler) : base(repository, notificationHandler)
        {
        }

        public bool PeriodIsInvalid(DateTime startDate, DateTime endDate)
        {
            return startDate > endDate;
        }

        public void ValidateDuplicateRoomTypeAndOccupationInDto(PropertyRateStrategyRoomTypeDto dto, ICollection<PropertyRateStrategyRoomTypeDto> dtoList)
        {
            var hasDuplicate = dtoList.Where(p => 
                (
                    (dto.MinOccupationPercentual >= p.MinOccupationPercentual && dto.MinOccupationPercentual <= p.MaxOccupationPercentual)
                    || 
                    (dto.MaxOccupationPercentual >= p.MinOccupationPercentual && dto.MaxOccupationPercentual <= p.MaxOccupationPercentual)
                )
                && p.RoomTypeId == dto.RoomTypeId).Count() > 1;

            if (hasDuplicate)
            {
                Notification.Raise(Notification.DefaultBuilder
                    .WithMessage(AppConsts.LocalizationSourceName, PropertyRateStrategyEnum.Error.DuplicateRoomTypeAndOccupation)
                    .WithDetailedMessage(AppConsts.LocalizationSourceName, PropertyRateStrategyEnum.Error.DuplicateRoomTypeAndOccupation)
                    .Build());
            }
        }       
    }
}
