﻿//  <copyright file="RoomTypeDomainService.cs" company="TOTVS">
//  Copyright (c) TOTVS. All rights reserved.
//  </copyright>
namespace Thex.Domain.Services
{
    using Thex.Common;
    using Thex.Domain.Entities;
    using Thex.Domain.Interfaces.Repositories;
    using Thex.Domain.Services.Interfaces;
    using System;
    using Tnf.Domain.Services;
    using Tnf.Notifications;
    using System.Collections.Generic;
    using Thex.Dto.Availability;
    using System.Linq;
    using Thex.Common.Enumerations;
    using Thex.Dto.Room;
    using Thex.Dto;
    using Tnf.Localization;
    using System.Threading.Tasks;

    public class RoomTypeDomainService : DomainService<RoomType>, IRoomTypeDomainService
    {
        private readonly IRoomTypeRepository _repository;
        private readonly ILocalizationManager _localizationManager;

        public RoomTypeDomainService(IRoomTypeRepository repository,
            INotificationHandler notificationHandler, 
            ILocalizationManager localizationManager)
            : base(repository, notificationHandler)
        {
            _repository = repository;
            _localizationManager = localizationManager;
        }

        public bool HasReservationForwards(int id)
        {
            return false;
        }

        public bool ToggleActivation(int id)
        {
            RoomType roomType = Repository.Get(new DefaultIntRequestDto(id));
            if (roomType.IsActive && HasReservationForwards(id))
            {
                RaiseRoomTypeHasReservationForwadNotification();
                return false;
            }                
            else
                return _repository.ToggleAndSaveIsActive(id);
        }

        private void RaiseRoomTypeHasReservationForwadNotification()
        {
            Notification.Raise(Notification.DefaultBuilder
                    .WithMessage(AppConsts.LocalizationSourceName, RoomType.EntityError.RoomTypeHasReserveForward)
                    .WithDetailedMessage(
                        AppConsts.LocalizationSourceName,
                        RoomType.EntityError.RoomTypeHasReserveForward)
                        .Build());
        }

        public List<AvailabilityRoomTypeColumnDto> GetAvailabilityRoomTypeColumns(List<ParentRoomsWithTotalChildrens> parentRooms, List<ReservationItem> reservationItems, List<AvailabilityRoomTypeRowDto> roomTypesRow, List<RoomBlockingDto> roomBlockings, List<RoomTypeInventory> roomTypeInventoryList, DateTime startDateParam, DateTime endDateParam)
        {
            var availabilityRoomTypeList = new List<AvailabilityRoomTypeColumnDto>();

            foreach (var inventory in roomTypeInventoryList)
            {
                var availabilityDay = new AvailabilityRoomTypeColumnDto
                {
                    Day = inventory.Date.Date,
                    Date = inventory.Date.ToString("dd/MM/yyyy"),
                    DateFormatted = inventory.Date.Date.ToString("s"),
                    RoomTypeId = inventory.RoomTypeId,
                    Rooms = inventory.Total - inventory.Balance,
                    AvailableRooms = inventory.Balance,
                    RoomTotal = inventory.Total
                };

                availabilityRoomTypeList.Add(availabilityDay);
            }

            foreach (var blockedRoom in roomBlockings)
            {
                var startDate = blockedRoom.BlockingStartDate;
                var finalDate = blockedRoom.BlockingEndDate;

                if (startDate > finalDate)
                    continue;

                while (startDate.Date <= finalDate.Date)
                {
                    var availabilityDayInList = availabilityRoomTypeList.FirstOrDefault(exp => exp.Date == startDate.ToString("dd/MM/yyyy") && exp.RoomTypeId == blockedRoom.RoomTypeId);

                    var conjugated = parentRooms.FirstOrDefault(exp => exp.Id == blockedRoom.RoomId);
                    var roomType = roomTypesRow.FirstOrDefault(exp => exp.Id == blockedRoom.RoomTypeId);

                    if (availabilityDayInList == null)
                    {
                        availabilityDayInList = new AvailabilityRoomTypeColumnDto
                        {
                            Day = startDate.Date,
                            Date = startDate.Date.ToString("dd/MM/yyyy"),
                            DateFormatted = startDate.Date.ToString("s"),
                            RoomTypeId = blockedRoom.RoomTypeId
                        };

                        availabilityRoomTypeList.Add(availabilityDayInList);
                    }

                    var roomTypeInventory = roomTypeInventoryList.FirstOrDefault(r => r.Date.Date == availabilityDayInList.Day && r.RoomTypeId == availabilityDayInList.RoomTypeId);

                    if (roomTypeInventory != null)
                    {
                        availabilityDayInList.Rooms = roomTypeInventory.Total - roomTypeInventory.Balance;
                        availabilityDayInList.AvailableRooms = roomTypeInventory.Balance;
                    }

                    startDate = startDate.AddDays(1);
                }
            }

            return availabilityRoomTypeList;
        }
        public int RoomBlockingCount(List<RoomBlockingDto> roomBlockings, DateTime date)
        {
            return roomBlockings.Where(x => x.BlockingStartDate.Date <= date && x.BlockingEndDate.Date >= date).Count();
        }

        private KeyValuePair<List<AvailabilityFooterColumnDto>, List<AvailabilityFooterColumnDto>> GetAllBlockingByDate(List<RoomBlockingDto> roomBlockings, DateTime startDate, DateTime finalDate)
        {
            var date = startDate;
            var blockingList = new List<AvailabilityFooterColumnDto>();
            while (date <= finalDate.Date)
            {
                var blockingDay = RoomBlockingCount(roomBlockings, date);
                var blockedResult = new AvailabilityFooterColumnDto();

                blockedResult.Number = blockingDay;
                blockedResult.Date = date.ToString("dd/MM/yyyy");
                blockedResult.DateFormatted = date.ToString("s");

                blockingList.Add(blockedResult);
                date = date.AddDays(1);
            }
            return new KeyValuePair<List<AvailabilityFooterColumnDto>, List<AvailabilityFooterColumnDto>>(null, blockingList);

        }

        private KeyValuePair<List<AvailabilityFooterColumnDto>, List<AvailabilityFooterColumnDto>> GetAvailabilityTotalsAndPercentageByDate(List<AvailabilityRoomTypeColumnDto> availabilityColumns, List<RoomBlockingDto> roomBlockings, DateTime startDate, DateTime finalDate, int total)
        {
            var totalList = new List<AvailabilityFooterColumnDto>();
            var percentageList = new List<AvailabilityFooterColumnDto>();           
            var date = startDate;            

            while (date <= finalDate.Date)
            {
                var availabilityDay = availabilityColumns.Where(exp => exp.Date == date.Date.ToString("dd/MM/yyyy")).Sum(exp => exp.Rooms);

                var blockingDay = roomBlockings.Count(exp => exp.BlockingStartDate.Date <= date.Date && exp.BlockingEndDate.Date >= date.Date);               

                var totalResult = new AvailabilityFooterColumnDto();
                var percentageResult = new AvailabilityFooterColumnDto();

                totalResult.Date = percentageResult.Date = date.ToString("dd/MM/yyyy");
                totalResult.DateFormatted = percentageResult.Date = date.ToString("s");

                totalResult.Number = total - availabilityDay;

                percentageResult.Number = CalculateOverbookingPercentage(total, (availabilityDay - blockingDay));               

                totalList.Add(totalResult);
                percentageList.Add(percentageResult);


                date = date.AddDays(1);
            }

            return new KeyValuePair<List<AvailabilityFooterColumnDto>, List<AvailabilityFooterColumnDto>>(totalList, percentageList);
        }

        public KeyValuePair<List<AvailabilityFooterColumnDto>, List<AvailabilityFooterColumnDto>> GetAvailabilityTotalsAndPercentageByDate(List<AvailabilityRoomTypeColumnDto> availabilityColumns, DateTime startDate, DateTime finalDate, int total)
        {
            var totalList = new List<AvailabilityFooterColumnDto>();
            var percentageList = new List<AvailabilityFooterColumnDto>();
            var date = startDate;

            while (date <= finalDate.Date)
            {
                var availabilityDay = availabilityColumns.Where(exp => exp.Date == date.Date.ToString("dd/MM/yyyy")).Sum(exp => exp.Rooms);               

                var totalResult = new AvailabilityFooterColumnDto();
                var percentageResult = new AvailabilityFooterColumnDto();

                totalResult.Date = percentageResult.Date = date.ToString("dd/MM/yyyy");
                totalResult.DateFormatted = percentageResult.Date = date.ToString("s");

                totalResult.Number = total - availabilityDay;

                percentageResult.Number = CalculateOverbookingPercentage(total, availabilityDay);

                if (totalResult.Number < 0)
                    totalResult.Number = 0;               

                totalList.Add(totalResult);
                percentageList.Add(percentageResult);


                date = date.AddDays(1);
            }

            return new KeyValuePair<List<AvailabilityFooterColumnDto>, List<AvailabilityFooterColumnDto>>(totalList, percentageList);
        }

        public decimal CalculateOverbookingPercentage(int totalRooms, int occupiedRooms)
        {
            if (totalRooms == 0)
                return 0;
            return Math.Round(100 * Convert.ToDecimal(occupiedRooms) / Convert.ToDecimal(totalRooms), 2);

        }

        public decimal CalculateAvailabilityPercentage(int total, int availabilityDay, int blockingDay)
        {
            if (total == 0)
                return 0;

            return Math.Truncate(100 * ((100 * Convert.ToDecimal(availabilityDay + blockingDay) / Convert.ToDecimal(total)) - 100) * -1) / 100;
        }

        public List<AvailabilityFooterRowDto> GetAvailabilityTotals(List<AvailabilityRoomTypeColumnDto> availabilityColumns, List<RoomBlockingDto> roomBlockings, List<AvailabilityRoomTypeRowDto> roomTypesRow, DateTime startDate, DateTime finalDate)
        {
            var availabilityFooterList = new List<AvailabilityFooterRowDto>();
            var roomTypesRowTotal = roomTypesRow.Sum(e => e.Total);
            var footerValues = GetAvailabilityTotalsAndPercentageByDate(availabilityColumns, roomBlockings, startDate, finalDate, roomTypesRowTotal);
            var footerValuesBlocked = GetAllBlockingByDate(roomBlockings, startDate, finalDate);


            var footerTotal = new AvailabilityFooterRowDto
            {
                Id = (int)AvailabilityGridEnum.AvailabilityGridTotal,
                Name = _localizationManager.GetString(AppConsts.LocalizationSourceName, AvailabilityGridEnum.AvailabilityGridTotal.ToString()),
                Total = roomTypesRowTotal,
                Values = footerValues.Key
            };

            var footerPercentage = new AvailabilityFooterRowDto
            {
                Id = (int)AvailabilityGridEnum.AvailabilityGridPercentage,
                Name = _localizationManager.GetString(AppConsts.LocalizationSourceName, AvailabilityGridEnum.AvailabilityGridPercentage.ToString()),
                Total = roomTypesRowTotal,
                Values = footerValues.Value
            };

            var footerBlocked = new AvailabilityFooterRowDto
            {
                Id = (int)AvailabilityGridEnum.AvailabilityGridBlocked,
                Name = _localizationManager.GetString(AppConsts.LocalizationSourceName, AvailabilityGridEnum.AvailabilityGridBlocked.ToString()),
                Values = footerValuesBlocked.Value
            };
            availabilityFooterList.Add(footerBlocked);
            availabilityFooterList.Add(footerPercentage);
            availabilityFooterList.Add(footerTotal);


            return availabilityFooterList;
        }

        public void FillRolesInAvailabilityRoomTypeColumns(List<AvailabilityRoomTypeRowDto> roomTypeAvailability, List<AvailabilityRoomTypeColumnDto> availabilityList, DateTime startDate, DateTime finalDate)
        {
            //preenche as datas que faltam para cada roomtype
            foreach (var roomType in roomTypeAvailability)
            {
                var date = startDate;
                while (date <= finalDate.Date)
                {
                    var dateString = date.ToString("dd/MM/yyyy");

                    if (availabilityList.Any(exp => exp.Date == dateString && exp.RoomTypeId == roomType.Id))
                    {
                        date = date.AddDays(1);
                        continue;
                    }

                    availabilityList.Add(new AvailabilityRoomTypeColumnDto
                    {
                        RoomTypeId = roomType.Id,
                        Date = dateString,
                        DateFormatted = date.ToString("s"),
                        AvailableRooms = roomType.Total,
                        Rooms = 0
                    });

                    date = date.AddDays(1);
                }
            }
        }

        public List<AvailabilityRoomTypeColumnDto> GetAvailabilityRoomTypeColumns(List<ReservationItem> reservationItems, List<AvailabilityRoomTypeRowDto> roomTypesRow)
        {
            var availabilityRoomTypeList = new List<AvailabilityRoomTypeColumnDto>();

            foreach (var reservationItem in reservationItems)
            {
                var startDate = reservationItem.CheckInDate ?? reservationItem.EstimatedArrivalDate;
                var finalDate = reservationItem.CheckOutDate ?? reservationItem.EstimatedDepartureDate;

                if (startDate > finalDate)
                    continue;

                while (startDate.Date <= finalDate.Date)
                {
                    var availabilityDayInList = availabilityRoomTypeList.FirstOrDefault(exp => exp.Date == startDate.ToString("dd/MM/yyyy") && exp.RoomTypeId == reservationItem.ReceivedRoomTypeId);
                    var availabilityDay = availabilityDayInList ?? new AvailabilityRoomTypeColumnDto();

                    availabilityDay.Day = startDate.Date;
                    availabilityDay.Date = startDate.Date.ToString("dd/MM/yyyy");
                    availabilityDay.DateFormatted = startDate.Date.ToString("s");
                    availabilityDay.RoomTypeId = reservationItem.ReceivedRoomTypeId;

                    if (startDate.Date == finalDate.Date)
                    {
                        startDate = startDate.AddDays(1);
                        continue;
                    }
                    else if (reservationItem.RoomId.HasValue)
                    {   
                        var roomType = roomTypesRow.FirstOrDefault(exp => exp.Id == reservationItem.ReceivedRoomTypeId);

                        availabilityDay.Rooms = availabilityDay.Rooms + 1;
                        availabilityDay.AvailableRooms = roomType == null ? 0 : roomType.Total - availabilityDay.Rooms;
                    }
                    else
                    {
                        var roomType = roomTypesRow.FirstOrDefault(exp => exp.Id == reservationItem.ReceivedRoomTypeId);

                        availabilityDay.Rooms = availabilityDay.Rooms + 1;
                        availabilityDay.AvailableRooms = roomType == null ? 0 : roomType.Total - availabilityDay.Rooms;
                    }

                    if (availabilityDayInList == null)
                        availabilityRoomTypeList.Add(availabilityDay);

                    startDate = startDate.AddDays(1);
                }
            }            

            return availabilityRoomTypeList;
        }
    }
}