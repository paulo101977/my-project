﻿// //  <copyright file="CompanyClientContactPersonDomainService.cs" company="TOTVS">
// //  Copyright (c) TOTVS. All rights reserved.
// //  </copyright>
namespace Thex.Domain.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Thex.Domain.Entities;
    using Thex.Domain.Interfaces.Repositories;
    using Thex.Domain.Interfaces.Services;

    using Tnf.Domain.Services;
    using Tnf.Notifications;

    public class CompanyClientContactPersonDomainService : DomainService<CompanyClientContactPerson>, ICompanyClientContactPersonDomainService
    {
        protected readonly ICompanyClientContactPersonRepository _companyClientContactPersonRepository;

        public CompanyClientContactPersonDomainService(ICompanyClientContactPersonRepository companyClientContactPersonRepository,
            INotificationHandler notificationHandler)
            : base(companyClientContactPersonRepository, notificationHandler)
        {
            _companyClientContactPersonRepository = companyClientContactPersonRepository;
        }

        public void UpdateRange(IList<CompanyClientContactPerson> companyClientContactPersonList, Guid companyClientId)
        {
            List<Guid> clientContactPersonIdList = new List<Guid>();
            foreach (var companyClientContactPerson in companyClientContactPersonList)
            {
                clientContactPersonIdList.Add(companyClientContactPerson.PersonId);
            }

            IList<CompanyClientContactPerson> companyClientContactPersonListToExcluded =
                this._companyClientContactPersonRepository.GetAllList()
                .Where(exp => !clientContactPersonIdList.Contains(exp.PersonId) && exp.CompanyClientId == companyClientId)
                .ToList();

            if (companyClientContactPersonListToExcluded.Any())
            {
                this._companyClientContactPersonRepository.RemoveRangeFromCompanyClientContactPerson(companyClientContactPersonListToExcluded);
            }

            foreach (var companyClientContactPerson in companyClientContactPersonList)
            {
                Update(companyClientContactPerson);
            }
            
        }

        public void Update(CompanyClientContactPerson companyClientContactPerson)
        {
            CompanyClientContactPerson companyClientContactPersonExisting = this._companyClientContactPersonRepository
                .GetAll().FirstOrDefault(c => c.PersonId == companyClientContactPerson.PersonId);

            companyClientContactPersonExisting.Occupation = companyClientContactPerson.Occupation;
            companyClientContactPersonExisting.OccupationId = companyClientContactPerson.Occupation.Id;
            companyClientContactPersonExisting.Person.FirstName = companyClientContactPerson.Person.FirstName;
            companyClientContactPersonExisting.Person.LastName = companyClientContactPerson.Person.LastName;
            companyClientContactPersonExisting.Person.FullName = companyClientContactPerson.Person.FullName;            
        }
    }
}