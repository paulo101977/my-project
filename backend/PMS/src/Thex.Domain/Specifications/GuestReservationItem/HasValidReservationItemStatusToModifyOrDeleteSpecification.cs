﻿//  <copyright file="HasValidReservationItemStatusToModifyOrDeleteSpecification.cs" company="TOTVS">
//  Copyright (c) TOTVS. All rights reserved.
//  </copyright>

namespace Thex.Domain.Specifications.GuestReservationItem
{
    using Thex.Common;
    using Thex.Common.Enumerations;
    using Thex.Domain.Entities;
    using System;
    using System.Linq;
    using System.Linq.Expressions;
    using Tnf.Specifications;

    public class HasValidReservationItemStatusToModifyOrDeleteSpecification : Specification<ReservationItem>
    {
        public override string LocalizationSource { get; protected set; } = AppConsts.LocalizationSourceName;

        public override Enum LocalizationKey { get; protected set; } = GuestReservationItemEnum.Error.GuestReservationItemModifyOrDeleteNotAllowed;

        public override Expression<Func<ReservationItem, bool>> ToExpression()
        {
            var validStatus = new int[]
            {
                (int)ReservationStatus.ToConfirm,
                (int)ReservationStatus.Confirmed
            };

            return b => validStatus.Contains(b.ReservationItemStatusId);
        }
    }
}
