﻿//  <copyright file="BrandMustHaveNameSpecification.cs" company="TOTVS">
//  Copyright (c) TOTVS. All rights reserved.
//  </copyright>

namespace Thex.Domain.Specifications.Property
{
    using Thex.Common;
    using Thex.Common.Enumerations;
    using Thex.Domain.Entities;
    using System;
    using System.Linq.Expressions;
    using Tnf.Specifications;

    public class PropertyMustHaveNameSpecification : Specification<Property>
    {
        public override string LocalizationSource { get; protected set; } = AppConsts.LocalizationSourceName;

        public override Enum LocalizationKey { get; protected set; } = PropertyEnum.Error.PropertyMustHaveName;

        public override Expression<Func<Property, bool>> ToExpression()
        {
            return b => !string.IsNullOrEmpty(b.Name);
        }
    }
}
