﻿//  <copyright file="BrandMustHaveNameSpecification.cs" company="TOTVS">
//  Copyright (c) TOTVS. All rights reserved.
//  </copyright>

namespace Thex.Domain.Specifications.Company
{
    using Thex.Common;
    using Thex.Common.Enumerations;
    using Thex.Domain.Entities;
    using System;
    using System.Linq.Expressions;
    using Tnf.Specifications;

    public class CompanyMustHaveShortNameSpecification : Specification<Company>
    {
        public override string LocalizationSource { get; protected set; } = AppConsts.LocalizationSourceName;

        public override Enum LocalizationKey { get; protected set; } = CompanyEnum.Error.CompanyMustHaveShortName;

        public override Expression<Func<Company, bool>> ToExpression()
        {
            return b => !string.IsNullOrEmpty(b.ShortName);
        }
    }
}
