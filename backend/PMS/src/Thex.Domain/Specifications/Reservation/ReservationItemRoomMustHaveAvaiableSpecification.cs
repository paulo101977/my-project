﻿//  <copyright file="BrandMustHaveNameSpecification.cs" company="TOTVS">
//  Copyright (c) TOTVS. All rights reserved.
//  </copyright>

namespace Thex.Domain.Specifications.Property
{
    using Thex.Common;
    using Thex.Common.Enumerations;
    using Thex.Domain.Entities;
    using Thex.EntityFrameworkCore.ReadInterfaces.Repositories;
    using Thex.Dto.Base;
    using System;
    using System.Linq.Expressions;
    using Tnf.Specifications;
    using System.Linq;

    public class ReservationItemRoomMustHaveAvaiableSpecification : Specification<ReservationItem>
    {
        private readonly IRoomReadRepository _roomReadRepository;
        private int _propertyId;

        public ReservationItemRoomMustHaveAvaiableSpecification(IRoomReadRepository roomReadRepository, int propertyId)
        {
            _roomReadRepository = roomReadRepository;
            _propertyId = propertyId;
        }

        public override string LocalizationSource { get; protected set; } = AppConsts.LocalizationSourceName;

        public override Enum LocalizationKey { get; protected set; } = ReservationItemEnum.Error.RoomIsNotAvaiable;

        public override Expression<Func<ReservationItem, bool>> ToExpression()
        {
            //return b => _roomReadRepository.GetAllAvailableByPeriodOfRoomType(new PeriodDto() { InitialDate = b.EstimatedArrivalDate, FinalDate = b.EstimatedDepartureDate }, propertyId, b.ReceivedRoomTypeId).Items.Select(r => r.Id).Contains(b.RoomId.Value);

            return b => _roomReadRepository.RoomIsAvailableByPeriodOfRoomType(
                new PeriodDto { InitialDate = b.EstimatedArrivalDate, FinalDate = b.EstimatedDepartureDate },
                _propertyId,
                b.ReceivedRoomTypeId,
                b.RoomId.Value);
        }
    }
}
