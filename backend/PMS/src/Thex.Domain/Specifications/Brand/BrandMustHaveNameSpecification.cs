﻿//  <copyright file="BrandMustHaveNameSpecification.cs" company="TOTVS">
//  Copyright (c) TOTVS. All rights reserved.
//  </copyright>

namespace Thex.Domain.Specifications.Brand
{
    using Thex.Common;
    using Thex.Common.Enumerations;
    using Thex.Domain.Entities;
    using System;
    using System.Linq.Expressions;
    using Tnf.Specifications;

    public class BrandMustHaveNameSpecification : Specification<Brand>
    {
        public override string LocalizationSource { get; protected set; } = AppConsts.LocalizationSourceName;

        public override Enum LocalizationKey { get; protected set; } = BrandEnum.Error.BrandMustHaveName;

        public override Expression<Func<Brand, bool>> ToExpression()
        {
            return b => !string.IsNullOrEmpty(b.Name);
        }
    }
}
