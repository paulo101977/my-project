﻿//  <copyright file="BrandMustHaveNameSpecification.cs" company="TOTVS">
//  Copyright (c) TOTVS. All rights reserved.
//  </copyright>

namespace Thex.Domain.Specifications.Person
{
    using Thex.Common;
    using Thex.Common.Enumerations;
    using Thex.Domain.Entities;
    using System;
    using System.Linq.Expressions;
    using Tnf.Specifications;

    public class PersonMustHaveNameSpecification : Specification<Person>
    {
        public override string LocalizationSource { get; protected set; } = AppConsts.LocalizationSourceName;

        public override Enum LocalizationKey { get; protected set; } = PersonEnum.Error.PersonMustHaveName;

        public override Expression<Func<Person, bool>> ToExpression()
        {
            return b => !string.IsNullOrEmpty(b.FirstName);
        }
    }
}
