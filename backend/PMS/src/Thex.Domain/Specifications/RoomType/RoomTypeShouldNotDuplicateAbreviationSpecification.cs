﻿namespace Thex.Domain.Specifications.RoomType
{
    using Thex.Common;
    using Thex.Domain.Entities;
    using System;
    using System.Linq.Expressions;
    using Tnf.Repositories;
    using Tnf.Specifications;


    public class RoomTypeShouldNotDuplicateAbreviationSpecification : Specification<RoomType>
    {
        public override string LocalizationSource { get; protected set; } = AppConsts.LocalizationSourceName;
        public override Enum LocalizationKey { get; protected set; } = RoomType.EntityError.RoomTypeShouldNotDuplicateAbreviation;

        private readonly IRepository<RoomType> _repository;
        private readonly int _roomTypeId;
        private readonly int _propertyId;
        private readonly string _abbreviation;

        public RoomTypeShouldNotDuplicateAbreviationSpecification(IRepository<RoomType> repository, int roomTypeId, int propertyId, string abbreviation)
        {
            _repository = repository;
            _roomTypeId = roomTypeId;
            _propertyId = propertyId;
            _abbreviation = abbreviation;
        }

        public override Expression<Func<RoomType, bool>> ToExpression()
            => ex => !(_repository.FirstOrDefault(w => w.Abbreviation == _abbreviation && w.PropertyId == _propertyId && w.Id != _roomTypeId) != null);
    }
}
