﻿namespace Thex.Domain.Specifications.RoomType
{
    using Thex.Common;
    using Thex.Domain.Entities;
    using System;
    using System.Linq.Expressions;
    using Tnf.Specifications;

    public class RoomTypeMustHaveAdultCapacitySpecification : Specification<RoomType>
    {
        public override string LocalizationSource { get; protected set; } = AppConsts.LocalizationSourceName;

        public override Enum LocalizationKey { get; protected set; } = RoomType.EntityError.RoomTypeMustHaveAdultCapacity;

        public override Expression<Func<RoomType, bool>> ToExpression()
        {
            return r => r.AdultCapacity > 0;
        }
    }
}
