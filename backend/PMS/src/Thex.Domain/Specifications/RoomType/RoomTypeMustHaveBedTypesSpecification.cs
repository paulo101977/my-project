﻿using Thex.Common;
using System;
using System.Linq.Expressions;
using Tnf.Specifications;


namespace Thex.Domain.Specifications.RoomType
{
    public class RoomTypeMustHaveBedTypesSpecification : Specification<Entities.RoomType>
    {
        public override string LocalizationSource { get; protected set; } = AppConsts.LocalizationSourceName;
        public override Enum LocalizationKey { get; protected set; } = Entities.RoomType.EntityError.RoomTypeMustHaveBedTypes;

        public override Expression<Func<Entities.RoomType, bool>> ToExpression()
        {
            return ex => ex.RoomTypeBedTypeList != null && ex.RoomTypeBedTypeList.Count > 0;
        }
    }
}
