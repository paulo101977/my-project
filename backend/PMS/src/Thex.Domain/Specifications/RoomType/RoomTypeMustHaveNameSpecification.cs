﻿// //  <copyright file="RoomTypeMustHaveChildCapacitySpecification.cs" company="TOTVS">
// //  Copyright (c) TOTVS. All rights reserved.
// //  </copyright>
namespace Thex.Domain.Specifications.Brands
{
    using Thex.Common;
    using Thex.Domain.Entities;
    using System;
    using System.Linq.Expressions;
    using Tnf.Specifications;

    public class RoomTypeMustHaveNameSpecification : Specification<RoomType>
    {
        public override string LocalizationSource { get; protected set; } = AppConsts.LocalizationSourceName;

        public override Enum LocalizationKey { get; protected set; } = RoomType.EntityError.RoomTypeMustHaveName;

        public override Expression<Func<RoomType, bool>> ToExpression()
        {
            return r => !string.IsNullOrEmpty(r.Name);
        }
    }
}