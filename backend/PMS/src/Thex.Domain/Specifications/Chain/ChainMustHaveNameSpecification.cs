﻿//  <copyright file="BrandMustHaveNameSpecification.cs" company="TOTVS">
//  Copyright (c) TOTVS. All rights reserved.
//  </copyright>

namespace Thex.Domain.Specifications.Chain
{
    using Thex.Common;
    using Thex.Common.Enumerations;
    using Thex.Domain.Entities;
    using System;
    using System.Linq.Expressions;
    using Tnf.Specifications;

    public class ChainMustHaveNameSpecification : Specification<Chain>
    {
        public override string LocalizationSource { get; protected set; } = AppConsts.LocalizationSourceName;

        public override Enum LocalizationKey { get; protected set; } = ChainEnum.Error.ChainMustHaveName;

        public override Expression<Func<Chain, bool>> ToExpression()
        {
            return b => !string.IsNullOrEmpty(b.Name);
        }
    }
}
