﻿using System;
using Thex.Common.Enumerations;
using Thex.Domain.Entities;
using Tnf.Specifications;

namespace Thex.Domain.Factories
{
    public class DocumentsValidatorFactory<T> where T : IEntity
    {
        public DocumentsValidatorFactory()
        {

        }

        private bool ValidateCNPJ(string document)
        {
            if (string.IsNullOrWhiteSpace(document))
                return false;

            document = document.Trim();
            document = document.Replace(".", "").Replace("-", "").Replace("/", "");

            if (document.Length != 14)
                return false;

            int[] multiplier1 = new[] { 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2 };
            int[] multiplier2 = new[] { 6, 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2 };
            int sum;
            int remainder;
            string digit;
            string tempCnpj;

            // Verifica os Patterns mais Comuns para CNPJ's Inválidos
            if (document.Equals("00000000000000") ||
                document.Equals("11111111111111") ||
                document.Equals("22222222222222") ||
                document.Equals("33333333333333") ||
                document.Equals("44444444444444") ||
                document.Equals("55555555555555") ||
                document.Equals("66666666666666") ||
                document.Equals("77777777777777") ||
                document.Equals("88888888888888") ||
                document.Equals("99999999999999"))
            {
                return false;
            }

            tempCnpj = document.Substring(0, 12);
            sum = 0;

            for (int i = 0; i < 12; i++)
                sum += int.Parse(tempCnpj[i].ToString()) * multiplier1[i];

            remainder = (sum % 11);

            if (remainder < 2)
                remainder = 0;
            else
                remainder = 11 - remainder;

            digit = remainder.ToString();
            tempCnpj = tempCnpj + digit;
            sum = 0;

            for (int i = 0; i < 13; i++)
                sum += int.Parse(tempCnpj[i].ToString()) * multiplier2[i];

            remainder = (sum % 11);

            if (remainder < 2)
                remainder = 0;
            else
                remainder = 11 - remainder;

            digit = digit + remainder.ToString();

            return document.EndsWith(digit);
        }

        private bool ValidateCPF(string document)
        {
            if (string.IsNullOrWhiteSpace(document))
                return false;

            int[] multiplier1 = new int[9] { 10, 9, 8, 7, 6, 5, 4, 3, 2 };
            int[] multiplier2 = new int[10] { 11, 10, 9, 8, 7, 6, 5, 4, 3, 2 };
            string tempCpf;
            string digit;
            int sum;
            int remainder;
            string cpf;

            cpf = document.Trim();
            cpf = cpf.Replace("-", "");
            cpf = cpf.Replace(".", "");

            if (cpf.Length != 11)            
                return false;
            
            
            if (cpf.Equals("00000000000") ||
                cpf.Equals("11111111111") ||
                cpf.Equals("22222222222") ||
                cpf.Equals("33333333333") ||
                cpf.Equals("44444444444") ||
                cpf.Equals("55555555555") ||
                cpf.Equals("66666666666") ||
                cpf.Equals("77777777777") ||
                cpf.Equals("88888888888") ||
                cpf.Equals("99999999999"))            
                return false;
            

            foreach (char c in cpf)
            {
                if (!char.IsNumber(c))                
                    return false;                
            }

            tempCpf = cpf.Substring(0, 9);
            sum = 0;

            for (int i = 0; i < 9; i++)
                sum += int.Parse(tempCpf[i].ToString()) * multiplier1[i];

            remainder = sum % 11;

            if (remainder < 2)
                remainder = 0;
            else
                remainder = 11 - remainder;

            digit = remainder.ToString();
            tempCpf = tempCpf + digit;
            sum = 0;

            for (int i = 0; i < 10; i++)
                sum += int.Parse(tempCpf[i].ToString()) * multiplier2[i];

            remainder = sum % 11;

            if (remainder < 2)
                remainder = 0;
            else
                remainder = 11 - remainder;

            digit = digit + remainder.ToString();

            return cpf.EndsWith(digit);
        }

        public ExpressionSpecification<T> ValidateDocument(string LocalizationSourceName, int documentTypeId, string document)
        {
            switch (documentTypeId)
            {
                case (int)DocumentTypeEnum.BrNaturalPersonCPF:
                    
                    return new ExpressionSpecification<T>(
                      LocalizationSourceName,
                      Document.EntityError.DocumentCpfInvalid,
                      w => ValidateCPF(document)
                      );

                case (int)DocumentTypeEnum.BrLegalPersonCNPJ:
                    return new ExpressionSpecification<T>(
                      LocalizationSourceName,
                      Document.EntityError.DocumentCnpjInvalid,
                      w => ValidateCNPJ(document));

            }

            return new ExpressionSpecification<T>(
                       LocalizationSourceName,
                       Document.EntityError.DocumentInvalid,
                       w => w != null);

        }
    }
}
