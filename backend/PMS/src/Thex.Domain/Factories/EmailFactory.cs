﻿using Microsoft.Extensions.Configuration;
using System.IO;
using Thex.Common;
using Thex.Common.Enumerations;
using Thex.Domain.Interfaces.Factories;
using Thex.Dto.Email;
using Tnf.Localization;

namespace Thex.Domain.Factories
{
    public class EmailFactory : IEmailFactory
    {
        private readonly ILocalizationManager _localizationManager;
        private readonly IConfiguration _configuration;

        public EmailFactory(ILocalizationManager localizationManager, IConfiguration configuration)
        {
            _localizationManager = localizationManager;
            _configuration = configuration;
        }

        public string GenerateEmail(EmailTemplateEnum emailTemplate, EmailBaseDto emailDto)
        {
            switch (emailTemplate)
            {
                case EmailTemplateEnum.EmailForgotPassword:
                    return GenerateEmailForgotPassword((EmailForgotPasswordDto)emailDto);
                case EmailTemplateEnum.EmailUserInvitation:
                    return GenerateEmailUserInvitation((EmailUserInvitationDto)emailDto);
                case EmailTemplateEnum.EmailIntegration:
                    return GenerateEmailIntegration((EmailIntegrationDto)emailDto);
                case EmailTemplateEnum.EmailExternalAccessReservation:
                    return GenerateExternalAccessReservation((EmailExternalAccessDto)emailDto);
                default:
                    return "";
            }
        }

        private string GenerateExternalAccessReservation(EmailExternalAccessDto dto)
        {
            var path = GetResourcePath("TemplateEmail.html");
            if (File.Exists(path))
            {
                string emailText = File.ReadAllText(path);
                emailText = ReplaceBaseParameters(dto, emailText);
                emailText = emailText.Replace("[EmailBody]", dto.Body);
                emailText = emailText.Replace("[EmailLink]", dto.ExternalLink);
                emailText = emailText.Replace("[DisplayLink]", "");
                emailText = emailText.Replace("[ClickHere]", _localizationManager.GetString(AppConsts.LocalizationSourceName, EmailEnum.ClickHere.ToString()));
                return emailText;
            }
            return "";
        }

        private string GenerateEmailForgotPassword(EmailForgotPasswordDto emailForgotPasswordDto)
        {
            var path = GetResourcePath("TemplateEmail.html");
            if (File.Exists(path))
            {
                string emailText = File.ReadAllText(path);
                emailText = ReplaceBaseParameters(emailForgotPasswordDto, emailText);
                emailText = emailText.Replace("[EmailBody]", emailForgotPasswordDto.Body);
                emailText = emailText.Replace("[EmailLink]", emailForgotPasswordDto.UrlForgotPassword);
                emailText = emailText.Replace("[DisplayLink]", "");
                emailText = emailText.Replace("[ClickHere]", _localizationManager.GetString(AppConsts.LocalizationSourceName, EmailEnum.ClickHere.ToString()));
                return emailText;
            }
            return "";
        }

        private string GenerateEmailUserInvitation(EmailUserInvitationDto emailUserInvitationDto)
        {
            var path = GetResourcePath("TemplateEmail.html");
            if (File.Exists(path))
            {
                string emailText = File.ReadAllText(path);
                emailText = ReplaceBaseParameters(emailUserInvitationDto, emailText);
                emailText = emailText.Replace("[EmailBody]", emailUserInvitationDto.Body);
                emailText = emailText.Replace("[EmailLink]", emailUserInvitationDto.UrlInvitation);
                emailText = emailText.Replace("[DisplayLink]", "");
                emailText = emailText.Replace("[ClickHere]", _localizationManager.GetString(AppConsts.LocalizationSourceName, EmailEnum.ClickHere.ToString()));
                return emailText;
            }
            return "";
        }

        private string GenerateEmailIntegration(EmailIntegrationDto emailIntegrationDto)
        {
            var path = GetResourcePath("TemplateEmail.html");
            if (File.Exists(path))
            {
                string emailText = File.ReadAllText(path);
                emailText = ReplaceBaseParameters(emailIntegrationDto, emailText);
                emailText = emailText.Replace("[EmailBody]", $"{emailIntegrationDto.Body}: {emailIntegrationDto.ErrorMessage}");
                emailText = emailText.Replace("[DisplayLink]", "display:none");
                return emailText;
            }
            return "";
        }

        private string ReplaceBaseParameters(EmailBaseDto emailBaseDto, string emailText)
        {
            var baseUrlImage = _configuration.GetValue<string>("BaseUrlImage");

            emailText = emailText.Replace("[EmailTitle]", emailBaseDto.Title);
            emailText = emailText.Replace("[FooterLogoTotvs]", $"https://cdn.totvscmnet-cloud.net/img/thex/0.28.22/Page%201.png");
            emailText = emailText.Replace("[MainLogoTotvs]", $"https://cdn.totvscmnet-cloud.net/img/thex/0.28.22/logo_totvs.png");
            //TODO: Fix Img
            //emailText = emailText.Replace("[FooterLogoTotvs]", $"{baseUrlImage}/0.28.22/Page%201.png");
            //emailText = emailText.Replace("[MainLogoTotvs]", $"{baseUrlImage}/0.28.22/logo_totvs.png");
            return emailText;
        }

        private string GetResourcePath(string templateName)
        {
            return Path.Combine(Directory.GetCurrentDirectory(), "Resources", templateName);
        }

    }
}
