﻿// //  <copyright file="ReservationItemStatusFactory.cs" company="TOTVS">
// //  Copyright (c) TOTVS. All rights reserved.
// //  </copyright>
namespace Thex.Domain.Factories
{
    using System.Collections.Generic;

    using Thex.Common.Enumerations;
    using Thex.Domain.Interfaces.Factories;

    public class ReservationItemStatusFactory : IReservationItemStatusFactory
    {
        public IList<ReservationStatus> GetAllowedStatus(ReservationStatus status)
        {
            IList<ReservationStatus> allowedStatusList = new List<ReservationStatus>();

            switch (status)
            {
                case ReservationStatus.WaitList:
                    allowedStatusList.Add(ReservationStatus.WaitList);
                    allowedStatusList.Add(ReservationStatus.ToConfirm);
                    allowedStatusList.Add(ReservationStatus.Confirmed);
                    break;
                case ReservationStatus.ReservationProposal:
                    allowedStatusList.Add(ReservationStatus.ReservationProposal);
                    allowedStatusList.Add(ReservationStatus.ToConfirm);
                    allowedStatusList.Add(ReservationStatus.Confirmed);
                    break;
                case ReservationStatus.Canceled:
                    allowedStatusList.Add(ReservationStatus.Canceled);
                    allowedStatusList.Add(ReservationStatus.ToConfirm);
                    allowedStatusList.Add(ReservationStatus.Confirmed);
                    allowedStatusList.Add(ReservationStatus.Pending);
                    break;
                case ReservationStatus.ToConfirm:
                    allowedStatusList.Add(ReservationStatus.ToConfirm);
                    allowedStatusList.Add(ReservationStatus.Canceled);
                    allowedStatusList.Add(ReservationStatus.Confirmed);
                    allowedStatusList.Add(ReservationStatus.Checkin);
                    break;
                case ReservationStatus.Confirmed:
                    allowedStatusList.Add(ReservationStatus.Confirmed);
                    allowedStatusList.Add(ReservationStatus.ToConfirm);
                    allowedStatusList.Add(ReservationStatus.Canceled);
                    allowedStatusList.Add(ReservationStatus.Checkin);
                    break;
                case ReservationStatus.Checkin:
                    allowedStatusList.Add(ReservationStatus.Checkin);
                    allowedStatusList.Add(ReservationStatus.Pending);
                    allowedStatusList.Add(ReservationStatus.Checkout);
                    break;
                case ReservationStatus.Checkout:
                    allowedStatusList.Add(ReservationStatus.Checkout);
                    allowedStatusList.Add(ReservationStatus.Pending);
                    break;
                case ReservationStatus.Pending:
                    allowedStatusList.Add(ReservationStatus.Pending);
                    allowedStatusList.Add(ReservationStatus.Checkout);
                    break;
                case ReservationStatus.NoShow:
                    allowedStatusList.Add(ReservationStatus.NoShow);
                    allowedStatusList.Add(ReservationStatus.ToConfirm);
                    break;
            }

            return allowedStatusList;
        }
    }
}