﻿using System;

namespace Thex.Domain.Helpers.ReservationCode
{
    public static class ReservationCodeGeneratorHelper
    {
        private static readonly Random Random;

        public static byte[] NodeBytes { get; set; }

        private static readonly object Lock = new object();

        private static DateTimeOffset _lastTimestampForNoDuplicatesGeneration = TimestampHelper.UtcNow();


        static ReservationCodeGeneratorHelper()
        {
            Random = new Random();
            NodeBytes = GenerateShortNodeBytes();
        }


        /// <summary>
        /// Generates a random value for the node.
        /// </summary>
        /// <returns></returns>
        public static byte[] GenerateShortNodeBytes()
        {
            var node = new byte[2];

            Random.NextBytes(node);
            return node;
        }


        public static ReservationCode GenerateTimeBasedShortGuid()
        {
            lock (Lock)
            {
                var ts = TimestampHelper.UtcNow();
                var nodeBytes = NodeBytes;

                if (ts <= _lastTimestampForNoDuplicatesGeneration)
                {
                    ts = TimestampHelper.UtcNow();
                }

                _lastTimestampForNoDuplicatesGeneration = ts;

                return GenerateTimeBasedShortGuid(ts, NodeBytes);
            }
        }

        public static ReservationCode GenerateTimeBasedShortGuid(DateTimeOffset dateTime, byte[] node)
        {
            if (node == null)
                throw new ArgumentNullException("node");

            if (node.Length != 2)
                throw new ArgumentOutOfRangeException("node", "The node must be 2 bytes.");
            ReservationCode reservationCode = new ReservationCode()
            {
                Year = dateTime.Year,
                Month = dateTime.Month,
                Day = dateTime.Day
            };

            long ticks = dateTime.Ticks - (new DateTime(reservationCode.Year, reservationCode.Month, 1, 0, 0, 0, 0)).Ticks;

            reservationCode.Timestamp = BitConverter.GetBytes(ticks);
            Array.Copy(node, 0, reservationCode.Node, 0, Math.Min(2, node.Length));

            return reservationCode;
        }
    }
}
