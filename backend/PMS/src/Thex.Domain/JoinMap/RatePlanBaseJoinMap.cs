﻿using Thex.Domain.Entities;

namespace Thex.Domain.JoinMap
{
    public class RatePlanBaseJoinMap
    {
        public RatePlan RatePlan { get; set; }
        public RatePlanRoomType RatePlanRoomType { get; set; }
        public RatePlanCompanyClient RatePlanCompanyClient { get; set; }
        public RatePlanCommission RatePlanCommission { get; set; }

        public CompanyClient CompanyClient { get; set; }
        public Currency Currency { get; set; }
        public RoomType RoomType { get; set; }
        public MealPlanType MealPlanType { get; set; }
    }
}
