﻿using System;
using Thex.Domain.Entities;

namespace Thex.Domain.JoinMap
{
    public class BillingAccountBaseJoinMap
    {
        public BillingAccount BillingAccount { get; set; }
        public Reservation Reservation { get; set; }
        public ReservationItem ReservationItem { get; set; }
        public CompanyClient CompanyClient { get; set; }
        public GuestReservationItem GuestReservationItem { get; set; }
        public Room Room { get; set; }
        public RoomType RoomType { get; set; }
        public Guest Guest { get; set; }
        public ReservationBudget ReservationBudget { get; set; }
        public ReservationConfirmation ReservationConfirmation { get; set; }
        public CompanyClient CompanyClientConfirmationReservation { get; set; }
        public Guid TenantId { get; set; }

    }
}
