﻿using System;
using Thex.Domain.Entities;

namespace Thex.Domain.JoinMap
{
    public class BillingAccountWithItemsMap : BillingAccountBaseJoinMap
    {
        public BillingAccountItem BillingAccountItem { get; set; }
        public Currency Currency { get; set; }
    }
}
