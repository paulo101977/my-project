﻿using Thex.Domain.Entities;

namespace Thex.Domain.JoinMap
{
    public class TenantPropertyParameterJoinMap
    {
        public Tenant Tenant { get; set; }
        public PropertyParameter PropertyParameter { get; set; }
    }
}
