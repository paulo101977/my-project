﻿using System;
using System.Collections.Generic;
using System.Text;
using Thex.Domain.Entities;

namespace Thex.Domain.JoinMap
{
    public class RoomBlockingJoinMap
    {
        public Room Room { get; set; }       
        public RoomBlocking RoomBlocking { get; set; }
    }
}
