﻿using System;
using Thex.Domain.Entities;

namespace Thex.Domain.JoinMap
{
    public class IntegrationBaseJoinMap
    {
        public BillingInvoice BillingInvoice { get; set; }
        public BillingInvoiceProperty BillingInvoiceProperty { get; set; }
        public BillingAccount BillingAccount { get; set; }
        public BillingAccountItem BillingAccountItem { get; set; }
        public BillingItem BillingItem { get; set; }
        public BillingInvoicePropertySupportedType BillingInvoicePropertySupportedType { get; set; }
        public CountrySubdvisionService CountrySubdvisionService { get; set; }
    }
}
