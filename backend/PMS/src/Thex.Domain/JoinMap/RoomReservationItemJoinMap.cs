﻿using System;
using System.Collections.Generic;
using System.Text;
using Thex.Domain.Entities;

namespace Thex.Domain.JoinMap
{
    public class RoomReservationItemJoinMap
    {
        public Room Room { get; set; }
        public ReservationItem ReservationItem { get; set; }
    }
}
