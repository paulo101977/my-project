﻿using System;

namespace Thex.Domain
{
    public class AppConsts
    {
        public const string LocalizationSourceName = "Thex";

        private const string ENVIRONMENT_VARIABLE = "ASPNETCORE_ENVIRONMENT";
        private const string DEV_ENVIRONMENT_VARIABLE = "DEVELOPMENT";

        public const string DefaultPreferredCulture = "en-us";
        public const string DefaultPreferredLanguage = "en-us";

        public static readonly Guid DefaultTenant = new Guid("23eb803c-726a-4c7c-b25b-2c22a56793d9");

        public static bool IsDevelopment()
        {
            var environmentName = Environment.GetEnvironmentVariable(ENVIRONMENT_VARIABLE);
            if (environmentName.ToUpperInvariant() == DEV_ENVIRONMENT_VARIABLE)
                return true;

            return false;
        }
    }
}
