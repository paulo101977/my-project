﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace Thex.Domain.Extensions
{
    public static class DocumentValidationExtension
    {
        public static bool IsCpf(this string cpf)

        {
            int[] multiplier1 = new int[9] { 10, 9, 8, 7, 6, 5, 4, 3, 2 };
            int[] multiplier2 = new int[10] { 11, 10, 9, 8, 7, 6, 5, 4, 3, 2 };

            string temporaryCpf;

            string digit;

            int checksum;

            int rest;

            cpf = cpf.Trim();

            string regex = "([0-9]{2}[\\.]?[0-9]{3}[\\.]?[0-9]{3}[\\/]?[0-9]{4}[-]?[0-9]{2})|([0-9]{3}[\\.]?[0-9]{3}[\\.]?[0-9]{3}[-]?[0-9]{2})";
            if (!Regex.IsMatch(cpf, regex, RegexOptions.IgnoreCase))
                return false;

            cpf = cpf.Replace(".", "").Replace("-", "");

            if (cpf.Length != 11)
                return false;

            switch (cpf)
            {
                case "11111111111":
                    return false;
                case "00000000000":
                    return false;
                case "2222222222":
                    return false;
                case "33333333333":
                    return false;
                case "44444444444":
                    return false;
                case "55555555555":
                    return false;
                case "66666666666":
                    return false;
                case "77777777777":
                    return false;
                case "88888888888":
                    return false;
                case "99999999999":
                    return false;
            }

            temporaryCpf = cpf.Substring(0, 9);

            checksum = 0;

            for (int i = 0; i < 9; i++)

                checksum += int.Parse(temporaryCpf[i].ToString()) * multiplier1[i];

            rest = checksum % 11;

            if (rest < 2)

                rest = 0;

            else

                rest = 11 - rest;

            digit = rest.ToString();

            temporaryCpf = temporaryCpf + digit;

            checksum = 0;

            for (int i = 0; i < 10; i++)

                checksum += int.Parse(temporaryCpf[i].ToString()) * multiplier2[i];

            rest = checksum % 11;

            if (rest < 2)

                rest = 0;

            else

                rest = 11 - rest;

            digit = digit + rest.ToString();

            return cpf.EndsWith(digit);

        }

        public static bool IsCnpj(string cnpj)
        {
            int[] multiplier1 = new int[12] { 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2 };
            int[] multiplier2 = new int[13] { 6, 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2 };
            int checksum;
            int rest;
            string digit;
            string temporaryCnpj;
            cnpj = cnpj.Trim();
            cnpj = cnpj.Replace(".", "").Replace("-", "").Replace("/", "");
            if (cnpj.Length != 14)
                return false;
            temporaryCnpj = cnpj.Substring(0, 12);
            checksum = 0;
            for (int i = 0; i < 12; i++)
                checksum += int.Parse(temporaryCnpj[i].ToString()) * multiplier1[i];
            rest = (checksum % 11);
            if (rest < 2)
                rest = 0;
            else
                rest = 11 - rest;
            digit = rest.ToString();
            temporaryCnpj = temporaryCnpj + digit;
            checksum = 0;
            for (int i = 0; i < 13; i++)
                checksum += int.Parse(temporaryCnpj[i].ToString()) * multiplier2[i];
            rest = (checksum % 11);
            if (rest < 2)
                rest = 0;
            else
                rest = 11 - rest;
            digit = digit + rest.ToString();
            return cnpj.EndsWith(digit);
        }
    }
}
