﻿//  <copyright file="ExpressionExtensions.cs" company="TOTVS">
//  Copyright (c) TOTVS. All rights reserved.
//  </copyright>
using System;

namespace Thex.Domain.Extensions
{
    using System.Linq;
    using System.Linq.Expressions;

    public static class ExpressionExtensions
    {
        public static Expression<Func<T, object>> GetPropertySelector<T>(string propertyName)
        {
            var arg = Expression.Parameter(typeof(T), "x");
            var body = Expression.Convert(Expression.Property(arg, propertyName), typeof(object));
            return Expression.Lambda<Func<T, object>>(body, arg);
        }
    }
}