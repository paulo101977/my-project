﻿using System;
using System.Collections.Generic;
using System.Text;
using Thex.Domain.Entities;
using Thex.Dto;
using System.Linq;
using Thex.EntityFrameworkCore.ReadInterfaces.Repositories;
using Tnf.App.Dto.Response;
using Tnf.App.EntityFrameworkCore.Repositories;
using Tnf.EntityFrameworkCore;
using Tnf.Localization;
using Thex.Common;
using Thex.Common.Enumerations;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Thex.EntityFrameworkCore.Repositories.Read
{
    public class PropertyMealPlanTypeReadRepository : AppEfCoreRepositoryBase<ThexContext, PropertyMealPlanType, Guid>, IPropertyMealPlanTypeReadRepository
    {


        public PropertyMealPlanTypeReadRepository(
            IDbContextProvider<ThexContext> dbContextProvider)
            : base(dbContextProvider)
        {

        }

        public ListDto<PropertyMealPlanTypeDto, Guid> GetAllPropertyMealPlanTypeByProperty(int propertyId)
        {
            var dbBaseQuery = GetAllPropertyMealPlanTypeByPropertyBaseQuery(propertyId);

            var resultDto = dbBaseQuery.ToList();

            foreach (var account in resultDto)
                account.MealPlanTypeName = LocalizationHelper.GetString(AppConsts.LocalizationSourceName, ((MealPlanTypeEnum)account.MealPlanTypeId));

            var result =
                new ListDto<PropertyMealPlanTypeDto, Guid>
                {
                    Items = resultDto,
                    HasNext = false,
                    Total = resultDto.Count()
                };

            return result;
        }

        public async Task<IListDto<PropertyMealPlanTypeDto, Guid>> GetAllPropertyMealPlanTypeByPropertyAsync(int propertyId)
        {
            var dbBaseQuery = GetAllPropertyMealPlanTypeByPropertyBaseQuery(propertyId);

            var resultDto = await dbBaseQuery.ToListAsync();

            foreach (var account in resultDto)
                account.MealPlanTypeName = LocalizationHelper.GetString(AppConsts.LocalizationSourceName, ((MealPlanTypeEnum)account.MealPlanTypeId));

            var result =
                new ListDto<PropertyMealPlanTypeDto, Guid>
                {
                    Items = resultDto,
                    HasNext = false,
                    Total = resultDto.Count()
                };

            return result;
        }

        private IQueryable<PropertyMealPlanTypeDto> GetAllPropertyMealPlanTypeByPropertyBaseQuery(int propertyId)
        {
            return (from mealPlanType in Context.MealPlanTypes
                    join propertyMealPlanType in Context.PropertyMealPlanTypes on mealPlanType.Id equals propertyMealPlanType.MealPlanTypeId
                    where propertyMealPlanType.PropertyId == propertyId && propertyMealPlanType.IsActive.Value
                    select new PropertyMealPlanTypeDto
                    {
                        Id = propertyMealPlanType.Id,
                        PropertyMealPlanTypeName = propertyMealPlanType != null ? propertyMealPlanType.PropertyMealPlanTypeName : mealPlanType.MealPlanTypeName,
                        PropertyMealPlanTypeCode = propertyMealPlanType != null ? propertyMealPlanType.PropertyMealPlanTypeCode : mealPlanType.MealPlanTypeCode,
                        MealPlanTypeId = propertyMealPlanType != null ? propertyMealPlanType.MealPlanTypeId : mealPlanType.Id,
                        PropertyId = propertyMealPlanType != null ? propertyMealPlanType.PropertyId : propertyId
                    });
        }
    }
}
