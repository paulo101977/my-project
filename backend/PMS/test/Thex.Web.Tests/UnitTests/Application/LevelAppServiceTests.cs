﻿using Shouldly;
using System;
using Tnf.AspNetCore.TestBase;
using Tnf.Notifications;
using Xunit;
using Microsoft.Extensions.DependencyInjection;
using Thex.Application.Adapters;
using Thex.Domain.Interfaces.Services;
using Thex.Infra.ReadInterfaces;
using Thex.Kernel;
using Thex.Application.Services;
using NSubstitute;
using Thex.Domain.Entities;
using Thex.Web.Tests.Mocks;
using System.Threading.Tasks;
using Tnf.Dto;
using Thex.Dto;
using Thex.Application.Interfaces;
using System.Linq;
using System.Collections.Generic;

namespace Thex.Web.Tests.UnitTests.Application
{
    public class LevelAppServiceTests : TnfAspNetCoreIntegratedTestBase<StartupUnitPropertyTest>
    {
        public INotificationHandler _notificationHandler;

        public LevelAppServiceTests()
        {
            _notificationHandler = new NotificationHandler(ServiceProvider);
        }

        [Fact]
        public void Should_Resolve_All()
        {
            TnfSession.ShouldNotBeNull();
            ServiceProvider.GetService<INotificationHandler>().ShouldNotBeNull();
            ServiceProvider.GetService<ILevelAdapter>().ShouldNotBeNull();
            ServiceProvider.GetService<ILevelDomainService>().ShouldNotBeNull();
            ServiceProvider.GetService<ILevelReadRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<IApplicationUser>().ShouldNotBeNull();
        }

        // Units Test Insert

        [Fact]
        public void Should_Create()
        {
            var levelReadRepository = Substitute.For<ILevelReadRepository>();
            var levelDomainService = Substitute.For<ILevelDomainService>();
            var levelAdapter = Substitute.For<ILevelAdapter>();
            var applicationUser = ServiceProvider.GetService<IApplicationUser>();


            var mock = new LevelMock(_notificationHandler);
            var levelDto = LevelMock.CreateLevelDto();
            var levelItem = LevelMock.GetListLevel().FirstOrDefault();


            levelReadRepository.GetById(default(int), Guid.Empty).ReturnsForAnyArgs(x =>
            {
                return LevelMock.GetCreateLevel();

            });


            levelReadRepository.GetDtoByLevelCode(default(int), default(int)).ReturnsForAnyArgs(x =>
             {
                 return null;
             });


            GetAllLevelDto requestDto = new GetAllLevelDto { All = true };



            levelReadRepository.GetAllDto(default(int), requestDto).ReturnsForAnyArgs(x =>
            {
                return LevelMock.GetDtoList();
            });

            levelDomainService.InsertAndSaveChanges(Level.Create(_notificationHandler)).ReturnsForAnyArgs(x =>
            {
                return LevelMock.Get();
            });


            levelAdapter.Map(levelDto).ReturnsForAnyArgs(x =>
            {
                return mock.Map(levelDto);

            });

            levelAdapter.Map(levelItem, levelDto).ReturnsForAnyArgs(x =>
            {
                return mock.Map(levelItem, levelDto);

            });

            levelReadRepository.GetAll(default(int)).ReturnsForAnyArgs(x =>
            {
                return LevelMock.GetListLevel();
            });


            var dto = new LevelAppService(levelAdapter, levelDomainService, levelReadRepository, applicationUser, _notificationHandler)
                                                    .CreateLevel(LevelMock.CreateLevelDto());

            Assert.False(_notificationHandler.HasNotification());
        }

        [Fact]
        public void Should_Create_Validate_Level()
        {
            var levelReadRepository = Substitute.For<ILevelReadRepository>();
            var levelDomainService = Substitute.For<ILevelDomainService>();
            var levelAdapter = Substitute.For<ILevelAdapter>();
            var applicationUser = ServiceProvider.GetService<IApplicationUser>();

            var mock = new LevelMock(_notificationHandler);
            var levelDto = LevelMock.CreateLevelDto();
            var levelItem = LevelMock.GetListLevel().FirstOrDefault();


            levelReadRepository.GetById(default(int), Guid.Empty).ReturnsForAnyArgs(x =>
            {
                return LevelMock.GetCreateLevel();
            });


            levelReadRepository.GetDtoByLevelCode(default(int), default(int)).ReturnsForAnyArgs(x =>
            {
                return null;
            });

            GetAllLevelDto requestDto = new GetAllLevelDto { All = true };

            levelReadRepository.GetAllDto(default(int), requestDto).ReturnsForAnyArgs(x =>
            {
                return LevelMock.GetDtoList();
            });

            levelDomainService.InsertAndSaveChanges(Level.Create(_notificationHandler)).ReturnsForAnyArgs(x =>
            {
                return LevelMock.Get();
            });

            levelAdapter.Map(levelDto).ReturnsForAnyArgs(x =>
            {
                return mock.Map(levelDto);

            });

            levelAdapter.Map(levelItem, levelDto).ReturnsForAnyArgs(x =>
            {
                return mock.Map(levelItem, levelDto);

            });


            levelReadRepository.GetDtoById(default(int), Guid.Empty).ReturnsForAnyArgs(x =>
            {
                return LevelMock.LevelDtoValidateLevel();

            });

            var dto = new LevelAppService(levelAdapter, levelDomainService, levelReadRepository, applicationUser, _notificationHandler)
                                                    .CreateLevel(LevelMock.LevelDtoValidateLevel());


            Assert.True(_notificationHandler.HasNotification());
        }

        [Fact]
        public void Should_Create_Erro_ValidadeteLevel_ValidateNull()
        {
            var levelReadRepository = Substitute.For<ILevelReadRepository>();
            var levelDomainService = Substitute.For<ILevelDomainService>();
            var levelAdapter = Substitute.For<ILevelAdapter>();
            var applicationUser = ServiceProvider.GetService<IApplicationUser>();



            var dto = new LevelAppService(levelAdapter, levelDomainService, levelReadRepository, applicationUser, _notificationHandler)
                                          .CreateLevel(LevelMock.LevelDtoNull());

            Assert.True(_notificationHandler.HasNotification());
        }

        [Fact]
        public void Should_Create_Erro_ValidadeteLevel_GetDtoByLevelCode_Null()
        {
            var levelReadRepository = Substitute.For<ILevelReadRepository>();
            var levelDomainService = Substitute.For<ILevelDomainService>();
            var levelAdapter = Substitute.For<ILevelAdapter>();
            var applicationUser = ServiceProvider.GetService<IApplicationUser>();

            levelReadRepository.GetById(default(int), Guid.Empty).ReturnsForAnyArgs(x =>
            {
                return null;
            });


            levelReadRepository.GetDtoByLevelCode(default(int), default(int)).ReturnsForAnyArgs(x =>
            {
                return LevelMock.GetDto(1, 1);
            });

            GetAllLevelDto requestDto = new GetAllLevelDto { All = true };

            levelReadRepository.GetAllDto(default(int), requestDto).ReturnsForAnyArgs(x =>
            {
                return LevelMock.GetDtoList();
            });

            levelDomainService.InsertAndSaveChanges(Level.Create(_notificationHandler)).ReturnsForAnyArgs(x =>
            {
                return LevelMock.Get();
            });

            var dto = new LevelAppService(levelAdapter, levelDomainService, levelReadRepository, applicationUser, _notificationHandler)
                                                .CreateLevel(LevelMock.GetDto(2, 2));

            Assert.True(_notificationHandler.HasNotification());
        }


        [Fact]
        public void Should_Create_Validate_Order_Include()
        {
            var levelReadRepository = Substitute.For<ILevelReadRepository>();
            var levelDomainService = Substitute.For<ILevelDomainService>();
            var levelAdapter = Substitute.For<ILevelAdapter>();
            var applicationUser = ServiceProvider.GetService<IApplicationUser>();

    
            GetAllLevelDto requestDto = new GetAllLevelDto { All = true };

            levelReadRepository.GetAllDto(default(int), requestDto).ReturnsForAnyArgs(x =>
            {
                return LevelMock.GetDtoList();
            });


            var orderResult = new LevelAppService(levelAdapter, levelDomainService, levelReadRepository, applicationUser, _notificationHandler)
                                                    .GeneratorOrder(LevelMock.GetDto(2, 1), LevelDto.Method.Insert);


            var result = orderResult.GroupBy(x => x.Order).Where(c => c.Count() > 1).Select(g => g.Key).ToList();

            Assert.True(result.Count == 0);

        }

        [Fact]
        public void Should_Validate_Order_Include()
        {
            var levelReadRepository = Substitute.For<ILevelReadRepository>();
            var levelDomainService = Substitute.For<ILevelDomainService>();
            var levelAdapter = Substitute.For<ILevelAdapter>();
            var applicationUser = ServiceProvider.GetService<IApplicationUser>();   


            GetAllLevelDto requestDto = new GetAllLevelDto { All = true };

            levelReadRepository.GetAllDto(default(int), requestDto).ReturnsForAnyArgs(x =>
            {
                return LevelMock.ListValidOrder();
            });


            var orderResult = new LevelAppService(levelAdapter, levelDomainService, levelReadRepository, applicationUser, _notificationHandler)
                                                    .GeneratorOrder(LevelMock.GetDto(2, 2), LevelDto.Method.Insert);

            var listValid = new List<KeyValuePair<int, int>>()
            {
                new KeyValuePair<int, int>(2, 3),
                new KeyValuePair<int, int>(3, 4),
                new KeyValuePair<int, int>(4, 5),
                new KeyValuePair<int, int>(5, 6),
                new KeyValuePair<int, int>(6, 7),
            };

            bool validItem = true;

            foreach (var resultItem in orderResult)
            {
                validItem = listValid.Where(x => x.Key == resultItem.LevelCode).Any(c => c.Value.Equals(resultItem.Order));

                if (!validItem)
                    break;
            }

            Assert.True(validItem);
        }


        // Units Test Update
        [Fact]
        public void Should_Update()
        {
            var levelReadRepository = Substitute.For<ILevelReadRepository>();
            var levelDomainService = Substitute.For<ILevelDomainService>();
            var levelAdapter = Substitute.For<ILevelAdapter>();
            var applicationUser = ServiceProvider.GetService<IApplicationUser>();


            var mock = new LevelMock(_notificationHandler);
            var levelDto = LevelMock.GetDto(2, 2);
            var levelItem = LevelMock.GetListLevel().FirstOrDefault();

            levelReadRepository.GetById(default(int), Guid.Empty).ReturnsForAnyArgs(x =>
            {
                return LevelMock.GetCreateLevel();
            });


            levelReadRepository.GetDtoByLevelCode(default(int), default(int)).ReturnsForAnyArgs(x =>
            {
                return LevelMock.GetDto(2, 2);
            });


            GetAllLevelDto requestDto = new GetAllLevelDto { All = true };

            levelReadRepository.GetAllDto(default(int), requestDto).ReturnsForAnyArgs(x =>
            {
                return LevelMock.GetDtoList();
            });


            levelAdapter.Map(levelDto).ReturnsForAnyArgs(x =>
            {
                return mock.Map(levelDto);

            });

            levelAdapter.Map(levelItem, levelDto).ReturnsForAnyArgs(x =>
            {
                return mock.Map(levelItem, levelDto);

            });

            levelReadRepository.GetAll(default(int)).ReturnsForAnyArgs(x =>
            {
                return LevelMock.GetListLevel();
            });


            levelReadRepository.GetDtoById(default(int), Guid.NewGuid()).ReturnsForAnyArgs(x =>
            {
                return levelDto;
            });

            levelReadRepository.GetDtoByLevelCode(default(int), default(int)).ReturnsForAnyArgs(x =>
            {
                return levelDto;
            });

            var dto = new LevelAppService(levelAdapter, levelDomainService, levelReadRepository, applicationUser, _notificationHandler)
                                                    .UpdateLevel(LevelMock.GetDto(2, 2));

            Assert.False(_notificationHandler.HasNotification());
        }

        [Fact]
        public void Should_Update_Validate_Level()
        {
            var levelReadRepository = Substitute.For<ILevelReadRepository>();
            var levelDomainService = Substitute.For<ILevelDomainService>();
            var levelAdapter = Substitute.For<ILevelAdapter>();
            var applicationUser = ServiceProvider.GetService<IApplicationUser>();


            var mock = new LevelMock(_notificationHandler);
            var levelDto = LevelMock.GetDto(2, 2);
            var levelItem = LevelMock.GetListLevel().FirstOrDefault();

            levelReadRepository.GetById(default(int), Guid.Empty).ReturnsForAnyArgs(x =>
            {
                return LevelMock.GetCreateLevel();
            });


            levelReadRepository.GetDtoByLevelCode(default(int), default(int)).ReturnsForAnyArgs(x =>
            {
                return levelDto;
            });


            levelReadRepository.GetDtoById(default(int), Guid.NewGuid()).ReturnsForAnyArgs(x =>
            {
                return levelDto;
            });

            levelReadRepository.GetDtoByLevelCode(default(int), default(int)).ReturnsForAnyArgs(x =>
            {
                return levelDto;
            });

            var dto = new LevelAppService(levelAdapter, levelDomainService, levelReadRepository, applicationUser, _notificationHandler)
                                                    .UpdateLevel(LevelMock.LevelDtoValidateLevel());

            Assert.True(_notificationHandler.HasNotification());
        }

        [Fact]
        public void Should_Update_Validate_Permition_Level_Null()
        {
            var levelReadRepository = Substitute.For<ILevelReadRepository>();
            var levelDomainService = Substitute.For<ILevelDomainService>();
            var levelAdapter = Substitute.For<ILevelAdapter>();
            var applicationUser = ServiceProvider.GetService<IApplicationUser>();

            levelReadRepository.GetById(default(int), Guid.Empty).ReturnsForAnyArgs(x =>
            {
                return null;
            });


            levelReadRepository.GetDtoByLevelCode(default(int), default(int)).ReturnsForAnyArgs(x =>
            {
                return null;
            });

            GetAllLevelDto requestDto = new GetAllLevelDto { All = true };

            levelReadRepository.GetAllDto(default(int), requestDto).ReturnsForAnyArgs(x =>
            {
                return LevelMock.GetDtoList();
            });

            levelDomainService.InsertAndSaveChanges(Level.Create(_notificationHandler)).ReturnsForAnyArgs(x =>
            {
                return LevelMock.Get();
            });

            var dto = new LevelAppService(levelAdapter, levelDomainService, levelReadRepository, applicationUser, _notificationHandler)
                                                    .UpdateLevel(LevelMock.GetDto(2, 2));


            Assert.True(_notificationHandler.HasNotification());
        }

        [Fact]
        public void Should_Update_Erro_Properties_Null()
        {
            var levelReadRepository = Substitute.For<ILevelReadRepository>();
            var levelDomainService = Substitute.For<ILevelDomainService>();
            var levelAdapter = Substitute.For<ILevelAdapter>();
            var applicationUser = ServiceProvider.GetService<IApplicationUser>();

            levelReadRepository.GetById(default(int), Guid.Empty).ReturnsForAnyArgs(x =>
            {
                return null;
            });


            levelReadRepository.GetDtoByLevelCode(default(int), default(int)).ReturnsForAnyArgs(x =>
            {
                return null;
            });

            GetAllLevelDto requestDto = new GetAllLevelDto { All = true };

            levelReadRepository.GetAllDto(default(int), requestDto).ReturnsForAnyArgs(x =>
            {
                return LevelMock.GetDtoList();
            });

            levelDomainService.InsertAndSaveChanges(Level.Create(_notificationHandler)).ReturnsForAnyArgs(x =>
            {
                return LevelMock.Get();
            });

            var dto = new LevelAppService(levelAdapter, levelDomainService, levelReadRepository, applicationUser, _notificationHandler)
                                          .UpdateLevel(LevelMock.LevelDtoValidNull());

            Assert.True(_notificationHandler.HasNotification());
        }

        [Fact]
        public void Should_Update_Erro_ValidadeteLevel_ValidateNull()
        {
            var levelReadRepository = Substitute.For<ILevelReadRepository>();
            var levelDomainService = Substitute.For<ILevelDomainService>();
            var levelAdapter = Substitute.For<ILevelAdapter>();
            var applicationUser = ServiceProvider.GetService<IApplicationUser>();

            levelReadRepository.GetById(default(int), Guid.Empty).ReturnsForAnyArgs(x =>
            {
                return null;
            });


            levelReadRepository.GetDtoByLevelCode(default(int), default(int)).ReturnsForAnyArgs(x =>
            {
                return null;
            });

            GetAllLevelDto requestDto = new GetAllLevelDto { All = true };

            levelReadRepository.GetAllDto(default(int), requestDto).ReturnsForAnyArgs(x =>
            {
                return LevelMock.GetDtoList();
            });

            levelDomainService.InsertAndSaveChanges(Level.Create(_notificationHandler)).ReturnsForAnyArgs(x =>
            {
                return LevelMock.Get();
            });

            var dto = new LevelAppService(levelAdapter, levelDomainService, levelReadRepository, applicationUser, _notificationHandler)
                                          .UpdateLevel(LevelMock.LevelDtoNull());

            Assert.True(_notificationHandler.HasNotification());
        }

        [Fact]
        public void Should_Update_ValidateLevel_Erro_Id()
        {
            var levelReadRepository = Substitute.For<ILevelReadRepository>();
            var levelDomainService = Substitute.For<ILevelDomainService>();
            var levelAdapter = Substitute.For<ILevelAdapter>();
            var applicationUser = ServiceProvider.GetService<IApplicationUser>();

            levelReadRepository.GetById(default(int), Guid.Empty).ReturnsForAnyArgs(x =>
            {
                return null;
            });


            levelReadRepository.GetDtoByLevelCode(default(int), default(int)).ReturnsForAnyArgs(x =>
            {
                return null;
            });

            GetAllLevelDto requestDto = new GetAllLevelDto { All = true };

            levelReadRepository.GetAllDto(default(int), requestDto).ReturnsForAnyArgs(x =>
            {
                return LevelMock.GetDtoList();
            });

            levelDomainService.InsertAndSaveChanges(Level.Create(_notificationHandler)).ReturnsForAnyArgs(x =>
            {
                return LevelMock.Get();
            });

            var dto = new LevelAppService(levelAdapter, levelDomainService, levelReadRepository, applicationUser, _notificationHandler)
                                          .UpdateLevel(LevelMock.CreateLevelDto());

            Assert.True(_notificationHandler.HasNotification());
        }

        [Fact]
        public void Should_Update_ValidateLevel_Valid_Id()
        {
            var levelReadRepository = Substitute.For<ILevelReadRepository>();
            var levelDomainService = Substitute.For<ILevelDomainService>();
            var levelAdapter = Substitute.For<ILevelAdapter>();
            var applicationUser = ServiceProvider.GetService<IApplicationUser>();

            levelReadRepository.GetById(default(int), Guid.Empty).ReturnsForAnyArgs(x =>
            {
                return null;
            });


            levelReadRepository.GetDtoByLevelCode(default(int), default(int)).ReturnsForAnyArgs(x =>
            {
                return LevelMock.GetDto(1, 2);
            });

            GetAllLevelDto requestDto = new GetAllLevelDto { All = true };

            levelReadRepository.GetAllDto(default(int), requestDto).ReturnsForAnyArgs(x =>
            {
                return LevelMock.GetDtoList();
            });

            levelDomainService.InsertAndSaveChanges(Level.Create(_notificationHandler)).ReturnsForAnyArgs(x =>
            {
                return LevelMock.Get();
            });

            var dto = new LevelAppService(levelAdapter, levelDomainService, levelReadRepository, applicationUser, _notificationHandler)
                                          .UpdateLevel(LevelMock.GetDtoIdValidUpdate());

            Assert.True(_notificationHandler.HasNotification());
        }

        [Fact]
        public void Should_Update_ValidateLevel_Valid_LevelCode()
        {
            var levelReadRepository = Substitute.For<ILevelReadRepository>();
            var levelDomainService = Substitute.For<ILevelDomainService>();
            var levelAdapter = Substitute.For<ILevelAdapter>();
            var applicationUser = ServiceProvider.GetService<IApplicationUser>();

            levelReadRepository.GetById(default(int), Guid.Empty).ReturnsForAnyArgs(x =>
            {
                return null;
            });


            levelReadRepository.GetDtoByLevelCode(default(int), default(int)).ReturnsForAnyArgs(x =>
            {
                return LevelMock.GetDto(1, 2);
            });

            GetAllLevelDto requestDto = new GetAllLevelDto { All = true };

            levelReadRepository.GetAllDto(default(int), requestDto).ReturnsForAnyArgs(x =>
            {
                return LevelMock.GetDtoList();
            });

            levelDomainService.InsertAndSaveChanges(Level.Create(_notificationHandler)).ReturnsForAnyArgs(x =>
            {
                return LevelMock.Get();
            });

            var dto = new LevelAppService(levelAdapter, levelDomainService, levelReadRepository, applicationUser, _notificationHandler)
                                          .UpdateLevel(LevelMock.GetDtoLevelCodeValidUpdate());

            Assert.True(_notificationHandler.HasNotification());
        }

        [Fact]
        public void Should_Create_Validate_Order_Update()
        {
            var levelReadRepository = Substitute.For<ILevelReadRepository>();
            var levelDomainService = Substitute.For<ILevelDomainService>();
            var levelAdapter = Substitute.For<ILevelAdapter>();
            var applicationUser = ServiceProvider.GetService<IApplicationUser>();


            GetAllLevelDto requestDto = new GetAllLevelDto { All = true };

            levelReadRepository.GetAllDto(default(int), requestDto).ReturnsForAnyArgs(x =>
            {
                return LevelMock.GetDtoList();
            });


            var orderResult = new LevelAppService(levelAdapter, levelDomainService, levelReadRepository, applicationUser, _notificationHandler)
                                                    .GeneratorOrder(LevelMock.GetDto(1, 3), LevelDto.Method.Update);


            var result = orderResult.GroupBy(x => x.Order).Where(c => c.Count() > 1).Select(g => g.Key).ToList();

            Assert.True(result.Count == 0);

        }

        [Fact]
        public void Should_Create_Validate_Order_Update_Item()
        {
            var levelReadRepository = Substitute.For<ILevelReadRepository>();
            var levelDomainService = Substitute.For<ILevelDomainService>();
            var levelAdapter = Substitute.For<ILevelAdapter>();
            var applicationUser = ServiceProvider.GetService<IApplicationUser>();

           
            GetAllLevelDto requestDto = new GetAllLevelDto { All = true };

            levelReadRepository.GetAllDto(default(int), requestDto).ReturnsForAnyArgs(x =>
            {
                return LevelMock.GetDtoList();
            });
            

            var orderResult = new LevelAppService(levelAdapter, levelDomainService, levelReadRepository, applicationUser, _notificationHandler)
                                                    .GeneratorOrder(LevelMock.GetDto(1, 1), LevelDto.Method.Update);


            var result = orderResult.GroupBy(x => x.Order).Where(c => c.Count() > 1).Select(g => g.Key).ToList();

            Assert.True(result.Count == 0);

        }

        [Fact]
        public void Should_Validate_Order_Update()
        {
            var levelReadRepository = Substitute.For<ILevelReadRepository>();
            var levelDomainService = Substitute.For<ILevelDomainService>();
            var levelAdapter = Substitute.For<ILevelAdapter>();
            var applicationUser = ServiceProvider.GetService<IApplicationUser>();

          

            GetAllLevelDto requestDto = new GetAllLevelDto { All = true };

            levelReadRepository.GetAllDto(default(int), requestDto).ReturnsForAnyArgs(x =>
            {
                return LevelMock.ListValidOrder();
            });


            var orderResult = new LevelAppService(levelAdapter, levelDomainService, levelReadRepository, applicationUser, _notificationHandler)
                                                    .GeneratorOrder(LevelMock.GetDto(3, 4), LevelDto.Method.Update);


            var listValid = new List<KeyValuePair<int, int>>()
            {
                new KeyValuePair<int, int>(3, 4),
                new KeyValuePair<int, int>(4, 5),
                new KeyValuePair<int, int>(5, 6),
                new KeyValuePair<int, int>(6, 7),
            };

            bool validItem = true;

            foreach (var resultItem in orderResult)
            {
                validItem = listValid.Where(x => x.Key == resultItem.LevelCode).Any(c => c.Value.Equals(resultItem.Order));

                if (!validItem)
                    break;
            }

            Assert.True(validItem);


        }

        // Units Test Delete

        [Fact]
        public void Should_Delete()
        {
            var levelReadRepository = Substitute.For<ILevelReadRepository>();
            var levelDomainService = Substitute.For<ILevelDomainService>();
            var levelAdapter = Substitute.For<ILevelAdapter>();
            var applicationUser = ServiceProvider.GetService<IApplicationUser>();

            levelDomainService.DeleteLevel(Level.Create(_notificationHandler));

            levelReadRepository.GetDtoById(default(int), Guid.Empty).ReturnsForAnyArgs(x =>
            {
                return LevelMock.GetDto(1, 1);
            });

            levelReadRepository.GetById(default(int), Guid.Empty).ReturnsForAnyArgs(x =>
            {
                return LevelMock.Get();
            });

            levelReadRepository.LevelAlreadyUsed(Guid.Empty).ReturnsForAnyArgs(x =>
            { return false; });

            new LevelAppService(levelAdapter, levelDomainService, levelReadRepository, applicationUser, _notificationHandler)
                                                   .DeleteLevel(Guid.NewGuid());

            Assert.False(_notificationHandler.HasNotification());
        }

        [Fact]
        public void Should_Not_Delete_Because_It_Is_Being_Used_()
        {
            var levelReadRepository = Substitute.For<ILevelReadRepository>();
            var levelDomainService = Substitute.For<ILevelDomainService>();
            var levelAdapter = Substitute.For<ILevelAdapter>();
            var applicationUser = ServiceProvider.GetService<IApplicationUser>();

            levelDomainService.DeleteLevel(Level.Create(_notificationHandler));

            levelReadRepository.GetDtoById(default(int), Guid.Empty).ReturnsForAnyArgs(x =>
            {
                return LevelMock.GetDto(1, 1);
            });

            levelReadRepository.GetById(default(int), Guid.Empty).ReturnsForAnyArgs(x =>
            {
                return LevelMock.Get();
            });

            levelReadRepository.LevelAlreadyUsed(Guid.Empty).ReturnsForAnyArgs(x =>
            { return true; });

            new LevelAppService(levelAdapter, levelDomainService, levelReadRepository, applicationUser, _notificationHandler)
                                                   .DeleteLevel(Guid.NewGuid());

            Assert.True(_notificationHandler.HasNotification());
        }

        [Fact]
        public void Should_Delete_Level_Null()
        {
            var levelReadRepository = Substitute.For<ILevelReadRepository>();
            var levelDomainService = Substitute.For<ILevelDomainService>();
            var levelAdapter = Substitute.For<ILevelAdapter>();
            var applicationUser = ServiceProvider.GetService<IApplicationUser>();

            levelDomainService.DeleteLevel(Level.Create(_notificationHandler));

            levelReadRepository.GetDtoById(default(int), Guid.Empty).ReturnsForAnyArgs(x =>
            {
                return null;
            });
             

            new LevelAppService(levelAdapter, levelDomainService, levelReadRepository, applicationUser, _notificationHandler)
                                                   .DeleteLevel(Guid.NewGuid());

            Assert.True(_notificationHandler.HasNotification());
        }


        [Fact]
        public void Should_Delete_ValidatePermission()
        {
            var levelReadRepository = Substitute.For<ILevelReadRepository>();
            var levelDomainService = Substitute.For<ILevelDomainService>();
            var levelAdapter = Substitute.For<ILevelAdapter>();
            var applicationUser = ServiceProvider.GetService<IApplicationUser>();

            levelDomainService.DeleteLevel(Level.Create(_notificationHandler));

            levelReadRepository.GetDtoById(default(int), Guid.Empty).ReturnsForAnyArgs(x =>
            {
                return LevelMock.GetDto(2, 2);
            });


            levelReadRepository.GetById(default(int), Guid.Empty).ReturnsForAnyArgs(x =>
            {
                return LevelMock.GetCreateLevel();
            });


            new LevelAppService(levelAdapter, levelDomainService, levelReadRepository, applicationUser, _notificationHandler)
                                                   .DeleteLevel(Guid.NewGuid());

            Assert.False(_notificationHandler.HasNotification());
        }

        [Fact]
        public void Should_Delete_ValidatePermission_TenantId()
        {
            var levelReadRepository = Substitute.For<ILevelReadRepository>();
            var levelDomainService = Substitute.For<ILevelDomainService>();
            var levelAdapter = Substitute.For<ILevelAdapter>();
            var applicationUser = ServiceProvider.GetService<IApplicationUser>();

            levelDomainService.DeleteLevel(Level.Create(_notificationHandler));

            levelReadRepository.GetDtoById(default(int), Guid.Empty).ReturnsForAnyArgs(x =>
            {
                return LevelMock.GetDto(1, 1);
            });


            levelReadRepository.GetById(default(int), Guid.Empty).ReturnsForAnyArgs(x =>
            {
                return LevelMock.GetValidTennantId();
            });


            new LevelAppService(levelAdapter, levelDomainService, levelReadRepository, applicationUser, _notificationHandler)
                                                   .DeleteLevel(Guid.NewGuid());

            Assert.True(_notificationHandler.HasNotification());
        }


        // Units Test ToggleAndSaveIsActive

        [Fact]
        public void Should_Toggle()
        {
            var levelReadRepository = Substitute.For<ILevelReadRepository>();
            var levelDomainService = Substitute.For<ILevelDomainService>();
            var levelAdapter = Substitute.For<ILevelAdapter>();
            var applicationUser = ServiceProvider.GetService<IApplicationUser>();

            levelDomainService.ToggleAndSaveIsActive(Guid.NewGuid());

            levelReadRepository.GetDtoById(default(int), Guid.Empty).ReturnsForAnyArgs(x =>
            {
                return LevelMock.GetDto(1, 1);
            });

            levelReadRepository.GetById(default(int), Guid.Empty).ReturnsForAnyArgs(x =>
            {
                return LevelMock.Get();
            });


            new LevelAppService(levelAdapter, levelDomainService, levelReadRepository, applicationUser, _notificationHandler)
                                                   .ToggleAndSaveIsActive(Guid.NewGuid());

            Assert.False(_notificationHandler.HasNotification());
        }

        [Fact]
        public void Should_Toggle_Level_Null()
        {
            var levelReadRepository = Substitute.For<ILevelReadRepository>();
            var levelDomainService = Substitute.For<ILevelDomainService>();
            var levelAdapter = Substitute.For<ILevelAdapter>();
            var applicationUser = ServiceProvider.GetService<IApplicationUser>();

            levelDomainService.ToggleAndSaveIsActive(Guid.NewGuid());

            levelReadRepository.GetDtoById(default(int), Guid.Empty).ReturnsForAnyArgs(x =>
            {
                return null;
            });
             

            new LevelAppService(levelAdapter, levelDomainService, levelReadRepository, applicationUser, _notificationHandler)
                                                   .ToggleAndSaveIsActive(Guid.NewGuid());

            Assert.True(_notificationHandler.HasNotification());
        }


        [Fact]
        public void Should_Toggle_ValidatePermission()
        {
            var levelReadRepository = Substitute.For<ILevelReadRepository>();
            var levelDomainService = Substitute.For<ILevelDomainService>();
            var levelAdapter = Substitute.For<ILevelAdapter>();
            var applicationUser = ServiceProvider.GetService<IApplicationUser>();

            levelDomainService.ToggleAndSaveIsActive(Guid.NewGuid());

            levelReadRepository.GetDtoById(default(int), Guid.Empty).ReturnsForAnyArgs(x =>
            {
                return LevelMock.GetDto(2, 2);
            });


            levelReadRepository.GetById(default(int), Guid.Empty).ReturnsForAnyArgs(x =>
            {
                return LevelMock.GetCreateLevel();
            });


            new LevelAppService(levelAdapter, levelDomainService, levelReadRepository, applicationUser, _notificationHandler)
                                                   .ToggleAndSaveIsActive(Guid.NewGuid());

            Assert.False(_notificationHandler.HasNotification());
        }

        [Fact]
        public void Should_Toggle_ValidatePermission_TenantId()
        {
            var levelReadRepository = Substitute.For<ILevelReadRepository>();
            var levelDomainService = Substitute.For<ILevelDomainService>();
            var levelAdapter = Substitute.For<ILevelAdapter>();
            var applicationUser = ServiceProvider.GetService<IApplicationUser>();

            levelDomainService.ToggleAndSaveIsActive(Guid.NewGuid());

            levelReadRepository.GetDtoById(default(int), Guid.Empty).ReturnsForAnyArgs(x =>
            {
                return LevelMock.GetDto(1, 1);
            });


            levelReadRepository.GetById(default(int), Guid.Empty).ReturnsForAnyArgs(x =>
            {
                return LevelMock.GetValidTennantId();
            });


            new LevelAppService(levelAdapter, levelDomainService, levelReadRepository, applicationUser, _notificationHandler)
                                                   .ToggleAndSaveIsActive(Guid.NewGuid());

            Assert.True(_notificationHandler.HasNotification());
        }

        // Units Test Get
        [Fact]
        public void GetDtoById()
        {
            var levelReadRepository = Substitute.For<ILevelReadRepository>();
            var levelDomainService = Substitute.For<ILevelDomainService>();
            var levelAdapter = Substitute.For<ILevelAdapter>();
            var applicationUser = ServiceProvider.GetService<IApplicationUser>();


            new LevelAppService(levelAdapter, levelDomainService, levelReadRepository, applicationUser, _notificationHandler)
                                                   .GetDtoById(Guid.NewGuid());

            Assert.False(_notificationHandler.HasNotification());
        }

    }
}
