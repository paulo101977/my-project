﻿using Tnf.AspNetCore.TestBase;
using Tnf.Notifications;
using Xunit;
using Microsoft.Extensions.DependencyInjection;
using Shouldly;
using Tnf.Repositories.Uow;
using Thex.Domain.Interfaces.Services;
using Tnf.Repositories;
using Thex.Kernel;
using Thex.Infra.ReadInterfaces;
using Thex.Application.Interfaces;
using Microsoft.Extensions.Configuration;
using Tnf.Localization;
using Thex.Domain.Interfaces.Repositories;
using Thex.Application.Adapters;
using Thex.Domain.Entities;
using NSubstitute;
using Tnf.Domain.Services;
using System;

namespace Thex.Web.Tests.UnitTests.Application
{
    public class ReservationItemAppServiceTest : TnfAspNetCoreIntegratedTestBase<StartupUnitPropertyTest>
    {
        private INotificationHandler _notificationHandler;
        private IUnitOfWorkManager _unitOfWorkManager;
        private IReservationItemReadRepository _reservationItemReadRepository;
        private IReservationItemDomainService _reservationItemDomainService;
        private IRepository<ReservationItem> _repository;
        private IApplicationUser _applicationUser;
        private IReservationItemStatusDomainService _reservationItemStatusDomainService;
        private EntityFrameworkCore.ReadInterfaces.Repositories.IRoomReadRepository _roomReadRepository;
        private IPropertyReadRepository _propertyReadRepository;
        private IHousekeepingStatusPropertyAppService _housekeepingStatusAppService;
        private IReservationBudgetAppService _reservationBudgetAppService;
        private IGuestReservationItemReadRepository _guestReservationItemReadRepository;
        private IGuestReservationItemRepository _guestReservationItemRepository;
        private IReservationConfirmationAppService _reservationConfirmationAppService;
        private IPropertyParameterReadRepository _propertyParameterReadRepository;
        private IConfiguration _configuration;
        private IBillingAccountReadRepository _billingAccountReadRepository;
        private ILocalizationManager _localizationManager;
        private ILocationReadRepository _locationReadRepository;
        private IRoomRepository _roomRepository;
        private IHousekeepingStatusPropertyAppService _housekeepingStatusPropertyAppService;
        private IHousekeepingStatusPropertyReadRepository _housekeepingStatusPropertyReadRepository;
        private IHousekeepingStatusPropertyRepository _housekeepingStatusPropertyRepository;
        private IBillingAccountRepository _billingAccountRepository;
        private IGuestReservationItemRepository _guestItemRepository;
        private ICurrencyReadRepository _currencyReadRepository;
        private IRoomOccupiedDomainService _RoomOccupiedDomainService;
        private IRoomOccupiedAdapter _RoomOccupiedAdapter;
        private IRoomOccupiedReadRepository _roomOccupiedReadRepository;
        private IReservationBudgetRepository _reservationBudgetRepository;     
        private IReservationItemRepository _reservationItemRepository;
        private IReservationItemAdapter _reservationItemAdapter;
        private IDomainService<ReservationItem> _reservationItemAppDomainService;
        private IServiceProvider _serviceProvider;
        private Inventory.Domain.Repositories.Interfaces.IPropertyParameterRepository _propertyParameterRepository;
        private Inventory.Domain.Repositories.Interfaces.IRoomTypeInventoryRepository _roomTypeInventoryRepository;


        public ReservationItemAppServiceTest()
        {
            _notificationHandler = new NotificationHandler(ServiceProvider);
            LoadDependencies();
        }

        private void LoadDependencies()
        {            
            _unitOfWorkManager = Substitute.For<IUnitOfWorkManager>();
            _reservationItemReadRepository = Substitute.For<IReservationItemReadRepository>();
            _reservationItemDomainService = Substitute.For<IReservationItemDomainService>();
            _repository = Substitute.For<IRepository<ReservationItem>>();
            _applicationUser = Substitute.For<IApplicationUser>();
            _reservationItemStatusDomainService = Substitute.For<IReservationItemStatusDomainService>();
            _roomReadRepository = Substitute.For<EntityFrameworkCore.ReadInterfaces.Repositories.IRoomReadRepository>();
            _housekeepingStatusAppService = Substitute.For<IHousekeepingStatusPropertyAppService>();
            _reservationBudgetAppService = Substitute.For<IReservationBudgetAppService>();
            _reservationConfirmationAppService = Substitute.For<IReservationConfirmationAppService>();
            _guestReservationItemReadRepository = Substitute.For<IGuestReservationItemReadRepository>();
            _guestReservationItemRepository = Substitute.For<IGuestReservationItemRepository>();
            _propertyParameterReadRepository = Substitute.For<IPropertyParameterReadRepository>();
            _propertyReadRepository = Substitute.For<IPropertyReadRepository>();
            _configuration = Substitute.For<IConfiguration>();
            _billingAccountReadRepository = Substitute.For<IBillingAccountReadRepository>();
            _localizationManager = Substitute.For<ILocalizationManager>();
            _locationReadRepository = Substitute.For<ILocationReadRepository>();
            _roomRepository = Substitute.For<IRoomRepository>();
            _housekeepingStatusPropertyAppService = Substitute.For<IHousekeepingStatusPropertyAppService>();
            _housekeepingStatusPropertyReadRepository = Substitute.For<IHousekeepingStatusPropertyReadRepository>();
            _housekeepingStatusPropertyRepository = Substitute.For<IHousekeepingStatusPropertyRepository>();
            _billingAccountRepository = Substitute.For<IBillingAccountRepository>();
            _guestItemRepository = Substitute.For<IGuestReservationItemRepository>();
            _currencyReadRepository = Substitute.For<ICurrencyReadRepository>();
            _RoomOccupiedDomainService = Substitute.For<IRoomOccupiedDomainService>();
            _RoomOccupiedAdapter = Substitute.For<IRoomOccupiedAdapter>();
            _roomOccupiedReadRepository = Substitute.For<IRoomOccupiedReadRepository>();
            _reservationBudgetRepository = Substitute.For<IReservationBudgetRepository>();        
            _reservationItemRepository = Substitute.For<IReservationItemRepository>();
            _reservationItemAdapter = Substitute.For<IReservationItemAdapter>();
            _reservationItemAppDomainService = Substitute.For<IDomainService<ReservationItem>>();
            _serviceProvider = Substitute.For<IServiceProvider>();
            _propertyParameterRepository = Substitute.For<Inventory.Domain.Repositories.Interfaces.IPropertyParameterRepository>();
            _roomTypeInventoryRepository = Substitute.For<Inventory.Domain.Repositories.Interfaces.IRoomTypeInventoryRepository>();
        }

        [Fact]
        public void Should_Resolve_All()
        {
            ServiceProvider.GetService<IUnitOfWorkManager>().ShouldNotBeNull();
            ServiceProvider.GetService<IReservationItemReadRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<IReservationItemDomainService>().ShouldNotBeNull();
            ServiceProvider.GetService<IRepository<ReservationItem>>().ShouldNotBeNull();
            ServiceProvider.GetService<IApplicationUser>().ShouldNotBeNull();
            ServiceProvider.GetService<IReservationItemStatusDomainService>().ShouldNotBeNull();
            ServiceProvider.GetService<EntityFrameworkCore.ReadInterfaces.Repositories.IRoomReadRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<IPropertyReadRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<IHousekeepingStatusPropertyAppService>().ShouldNotBeNull();
            ServiceProvider.GetService<IReservationBudgetAppService>().ShouldNotBeNull();
            ServiceProvider.GetService<IGuestReservationItemReadRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<IGuestReservationItemRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<IReservationConfirmationAppService>().ShouldNotBeNull();
            ServiceProvider.GetService<IPropertyParameterReadRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<IConfiguration>().ShouldNotBeNull();
            ServiceProvider.GetService<IBillingAccountReadRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<ILocalizationManager>().ShouldNotBeNull();
            ServiceProvider.GetService<ILocationReadRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<IRoomRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<IHousekeepingStatusPropertyAppService>().ShouldNotBeNull();
            ServiceProvider.GetService<IHousekeepingStatusPropertyReadRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<IHousekeepingStatusPropertyRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<IBillingAccountRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<IGuestReservationItemRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<IRoomOccupiedDomainService>().ShouldNotBeNull();
            ServiceProvider.GetService<IRoomOccupiedAdapter>().ShouldNotBeNull();
            ServiceProvider.GetService<IRoomOccupiedReadRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<IReservationBudgetRepository>().ShouldNotBeNull();      
            ServiceProvider.GetService<IReservationItemRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<IReservationItemAdapter>().ShouldNotBeNull();
            ServiceProvider.GetService<IDomainService<ReservationItem>>().ShouldNotBeNull();
            ServiceProvider.GetService<IServiceProvider>().ShouldNotBeNull();     
        }      
    }
}
