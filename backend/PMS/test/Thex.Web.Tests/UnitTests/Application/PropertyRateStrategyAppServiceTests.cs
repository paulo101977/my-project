﻿using NSubstitute;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thex.Application.Interfaces;
using Thex.Application.Services;
using Thex.Domain.Entities;
using Thex.Domain.Interfaces.Repositories;
using Thex.Domain.Interfaces.Services;
using Thex.Domain.Services;
using Thex.Dto;
using Thex.Infra.ReadInterfaces;
using Thex.Kernel;
using Thex.Web.Tests.Mocks;
using Thex.Web.Tests.Mocks.PropertyRateStrategy;
using Tnf.AspNetCore.TestBase;
using Tnf.Dto;
using Tnf.Notifications;
using Tnf.Repositories.Uow;
using Xunit;

namespace Thex.Web.Tests.UnitTests.Application
{
    public class PropertyRateStrategyAppServiceTests : TnfAspNetCoreIntegratedTestBase<StartupUnitPropertyTest>
    {
        private INotificationHandler _notificationHandler;

        public PropertyRateStrategyAppServiceTests()
        {
            _notificationHandler = new NotificationHandler(ServiceProvider);
        }

        #region Create
        [Fact]
        public async Task Should_Create()
        {
            var propertyRateStrategyRepository = Substitute.For<IPropertyRateStrategyRepository>();
            var propertyParameterReadRepository = Substitute.For<IPropertyParameterReadRepository>();
            var ratePlanAppService = Substitute.For<IRatePlanAppService>();
            var propertyRateStrategyDomainService = new PropertyRateStrategyDomainService(propertyRateStrategyRepository, _notificationHandler);
            var propertyRateStrategyReadRepository = Substitute.For<IPropertyRateStrategyReadRepository>();
            var unitOfWorkManager = Substitute.For<IUnitOfWorkManager>();
            var roomTypeAppService = Substitute.For<IRoomTypeAppService>();
            var propertyRateStrategyIdNew = Guid.NewGuid();

            propertyParameterReadRepository.GetSystemDate(1).ReturnsForAnyArgs(x =>
            {
                return new DateTime();
            });

            ratePlanAppService.GetAllBaseRateByPeriodAndPropertyId(new DateTime(), new DateTime(), 1).ReturnsForAnyArgs(x =>
            {
                return new ListDto<RatePlanDto>()
                {
                    Items = new List<RatePlanDto>
                    {
                        new RatePlanDto()
                    }
                };
            });

            propertyRateStrategyRepository.Create(new PropertyRateStrategy()).ReturnsForAnyArgs(x =>
            {
                return new PropertyRateStrategy { Id = propertyRateStrategyIdNew };
            });

            var propertyRateStrategyDto = new PropertyRateStrategyDto
            {
                IsActive = true,
                Name = "Estratégia 01",
                StartDate = new DateTime(2018, 10, 1),
                EndDate = new DateTime(2018, 10, 1),
                PropertyRateStrategyRoomTypeList = new List<PropertyRateStrategyRoomTypeDto>
                {
                    new PropertyRateStrategyRoomTypeDto
                    {
                        RoomTypeId = 1,
                        MinOccupationPercentual = 10,
                        MaxOccupationPercentual = 20,
                        IsDecreased = false,
                        PercentualAmount = 20
                    }
                }
            };

            propertyRateStrategyDto = await new PropertyRateStrategyAppService(_notificationHandler, propertyRateStrategyRepository, propertyParameterReadRepository, new ApplicationUserMock(),
                ratePlanAppService, propertyRateStrategyDomainService, propertyRateStrategyReadRepository, unitOfWorkManager, roomTypeAppService).CreateAsync(propertyRateStrategyDto);

            Assert.False(_notificationHandler.HasNotification());
            Assert.Equal(propertyRateStrategyIdNew, propertyRateStrategyDto.Id);
        }

        [Fact]
        public async Task Should_Does_Not_Create_When_SystemDate_Is_Null()
        {
            var propertyRateStrategyRepository = Substitute.For<IPropertyRateStrategyRepository>();
            var propertyParameterReadRepository = Substitute.For<IPropertyParameterReadRepository>();
            var ratePlanAppService = Substitute.For<IRatePlanAppService>();
            var propertyRateStrategyDomainService = new PropertyRateStrategyDomainService(propertyRateStrategyRepository, _notificationHandler);
            var propertyRateStrategyReadRepository = Substitute.For<IPropertyRateStrategyReadRepository>();
            var unitOfWorkManager = Substitute.For<IUnitOfWorkManager>();
            var roomTypeAppService = Substitute.For<IRoomTypeAppService>();
            var propertyRateStrategyIdNew = Guid.NewGuid();

            propertyParameterReadRepository.GetSystemDate(1).ReturnsForAnyArgs(x =>
            {
                return null;
            });

            ratePlanAppService.GetAllBaseRateByPeriodAndPropertyId(new DateTime(), new DateTime(), 1).ReturnsForAnyArgs(x =>
            {
                return new ListDto<RatePlanDto>()
                {
                    Items = new List<RatePlanDto>
                    {
                        new RatePlanDto()
                    }
                };
            });

            propertyRateStrategyRepository.Create(new PropertyRateStrategy()).ReturnsForAnyArgs(x =>
            {
                return new PropertyRateStrategy { Id = propertyRateStrategyIdNew };
            });

            var propertyRateStrategyDto = new PropertyRateStrategyDto
            {
                IsActive = true,
                Name = "Estratégia 01",
                StartDate = new DateTime(2018, 10, 1),
                EndDate = new DateTime(2018, 10, 1),
                PropertyRateStrategyRoomTypeList = new List<PropertyRateStrategyRoomTypeDto>
                {
                    new PropertyRateStrategyRoomTypeDto
                    {
                        RoomTypeId = 1,
                        MinOccupationPercentual = 10,
                        MaxOccupationPercentual = 20,
                        IsDecreased = false,
                        PercentualAmount = 20
                    }
                }
            };

            await new PropertyRateStrategyAppService(_notificationHandler, propertyRateStrategyRepository, propertyParameterReadRepository, new ApplicationUserMock(),
                ratePlanAppService, propertyRateStrategyDomainService, propertyRateStrategyReadRepository, unitOfWorkManager, roomTypeAppService).CreateAsync(propertyRateStrategyDto);

            Assert.True(_notificationHandler.HasNotification());
        }

        [Fact]
        public async Task Should_Does_Not_Create_When_Period_Is_Invalid()
        {
            var propertyRateStrategyRepository = Substitute.For<IPropertyRateStrategyRepository>();
            var propertyParameterReadRepository = Substitute.For<IPropertyParameterReadRepository>();
            var ratePlanAppService = Substitute.For<IRatePlanAppService>();
            var propertyRateStrategyDomainService = new PropertyRateStrategyDomainService(propertyRateStrategyRepository, _notificationHandler);
            var propertyRateStrategyReadRepository = Substitute.For<IPropertyRateStrategyReadRepository>();
            var unitOfWorkManager = Substitute.For<IUnitOfWorkManager>();
            var roomTypeAppService = Substitute.For<IRoomTypeAppService>();
            var propertyRateStrategyIdNew = Guid.NewGuid();

            propertyParameterReadRepository.GetSystemDate(1).ReturnsForAnyArgs(x =>
            {
                return new DateTime();
            });

            ratePlanAppService.GetAllBaseRateByPeriodAndPropertyId(new DateTime(), new DateTime(), 1).ReturnsForAnyArgs(x =>
            {
                return new ListDto<RatePlanDto>()
                {
                    Items = new List<RatePlanDto>
                    {
                        new RatePlanDto()
                    }
                };
            });

            propertyRateStrategyRepository.Create(new PropertyRateStrategy()).ReturnsForAnyArgs(x =>
            {
                return new PropertyRateStrategy { Id = propertyRateStrategyIdNew };
            });

            var propertyRateStrategyDto = new PropertyRateStrategyDto
            {
                IsActive = true,
                Name = "Estratégia 01",
                StartDate = new DateTime(2018, 10, 2),
                EndDate = new DateTime(2018, 10, 1),
                PropertyRateStrategyRoomTypeList = new List<PropertyRateStrategyRoomTypeDto>
                {
                    new PropertyRateStrategyRoomTypeDto
                    {
                        RoomTypeId = 1,
                        MinOccupationPercentual = 10,
                        MaxOccupationPercentual = 20,
                        IsDecreased = false,
                        PercentualAmount = 20
                    }
                }
            };

            await new PropertyRateStrategyAppService(_notificationHandler, propertyRateStrategyRepository, propertyParameterReadRepository, new ApplicationUserMock(),
                ratePlanAppService, propertyRateStrategyDomainService, propertyRateStrategyReadRepository, unitOfWorkManager, roomTypeAppService).CreateAsync(propertyRateStrategyDto);

            Assert.True(_notificationHandler.HasNotification());
        }

        [Fact]
        public async Task Should_Does_Not_Create_When_Period_Is_Overlapped()
        {
            var propertyRateStrategyRepository = Substitute.For<IPropertyRateStrategyRepository>();
            var propertyParameterReadRepository = Substitute.For<IPropertyParameterReadRepository>();
            var ratePlanAppService = Substitute.For<IRatePlanAppService>();
            var propertyRateStrategyDomainService = new PropertyRateStrategyDomainService(propertyRateStrategyRepository, _notificationHandler);
            var propertyRateStrategyReadRepository = Substitute.For<IPropertyRateStrategyReadRepository>();
            var unitOfWorkManager = Substitute.For<IUnitOfWorkManager>();
            var roomTypeAppService = Substitute.For<IRoomTypeAppService>();
            var propertyRateStrategyIdNew = Guid.NewGuid();

            propertyParameterReadRepository.GetSystemDate(1).ReturnsForAnyArgs(x =>
            {
                return new DateTime();
            });

            ratePlanAppService.GetAllBaseRateByPeriodAndPropertyId(new DateTime(), new DateTime(), 1).ReturnsForAnyArgs(x =>
            {
                return new ListDto<RatePlanDto>()
                {
                    Items = new List<RatePlanDto>
                    {
                        new RatePlanDto()
                    }
                };
            });

            propertyRateStrategyRepository.Create(new PropertyRateStrategy()).ReturnsForAnyArgs(x =>
            {
                return new PropertyRateStrategy { Id = propertyRateStrategyIdNew };
            });

            var propertyRateStrategyDto = new PropertyRateStrategyDto
            {
                IsActive = true,
                Name = "Estratégia 01",
                StartDate = new DateTime(2018, 10, 1),
                EndDate = new DateTime(2018, 10, 1),
                PropertyRateStrategyRoomTypeList = new List<PropertyRateStrategyRoomTypeDto>
                {
                    new PropertyRateStrategyRoomTypeDto
                    {
                        RoomTypeId = 1,
                        MinOccupationPercentual = 10,
                        MaxOccupationPercentual = 20,
                        IsDecreased = false,
                        PercentualAmount = 20
                    },
                    new PropertyRateStrategyRoomTypeDto
                    {
                        RoomTypeId = 1,
                        MinOccupationPercentual = 8,
                        MaxOccupationPercentual = 13,
                        IsDecreased = false,
                        PercentualAmount = 20
                    }
                }
            };

            await new PropertyRateStrategyAppService(_notificationHandler, propertyRateStrategyRepository, propertyParameterReadRepository, new ApplicationUserMock(),
                ratePlanAppService, propertyRateStrategyDomainService, propertyRateStrategyReadRepository, unitOfWorkManager, roomTypeAppService).CreateAsync(propertyRateStrategyDto);

            Assert.True(_notificationHandler.HasNotification());
        }

        [Fact]
        public async Task Should_Does_Not_Create_When_IncrementAmount_And_PercentualAmount_Is_Not_Null()
        {
            var propertyRateStrategyRepository = Substitute.For<IPropertyRateStrategyRepository>();
            var propertyParameterReadRepository = Substitute.For<IPropertyParameterReadRepository>();
            var ratePlanAppService = Substitute.For<IRatePlanAppService>();
            var propertyRateStrategyDomainService = new PropertyRateStrategyDomainService(propertyRateStrategyRepository, _notificationHandler);
            var propertyRateStrategyReadRepository = Substitute.For<IPropertyRateStrategyReadRepository>();
            var unitOfWorkManager = Substitute.For<IUnitOfWorkManager>();
            var roomTypeAppService = Substitute.For<IRoomTypeAppService>();
            var propertyRateStrategyIdNew = Guid.NewGuid();

            propertyParameterReadRepository.GetSystemDate(1).ReturnsForAnyArgs(x =>
            {
                return new DateTime();
            });

            ratePlanAppService.GetAllBaseRateByPeriodAndPropertyId(new DateTime(), new DateTime(), 1).ReturnsForAnyArgs(x =>
            {
                return new ListDto<RatePlanDto>()
                {
                    Items = new List<RatePlanDto>
                    {
                        new RatePlanDto()
                    }
                };
            });

            propertyRateStrategyRepository.Create(new PropertyRateStrategy()).ReturnsForAnyArgs(x =>
            {
                return new PropertyRateStrategy { Id = propertyRateStrategyIdNew };
            });

            var propertyRateStrategyDto = new PropertyRateStrategyDto
            {
                IsActive = true,
                Name = "Estratégia 01",
                StartDate = new DateTime(2018, 10, 1),
                EndDate = new DateTime(2018, 10, 1),
                PropertyRateStrategyRoomTypeList = new List<PropertyRateStrategyRoomTypeDto>
                {
                    new PropertyRateStrategyRoomTypeDto
                    {
                        RoomTypeId = 1,
                        MinOccupationPercentual = 10,
                        MaxOccupationPercentual = 20,
                        IsDecreased = false,
                        PercentualAmount = 20,
                        IncrementAmount = 10
                    }
                }
            };

            await new PropertyRateStrategyAppService(_notificationHandler, propertyRateStrategyRepository, propertyParameterReadRepository, new ApplicationUserMock(),
               ratePlanAppService, propertyRateStrategyDomainService, propertyRateStrategyReadRepository, unitOfWorkManager, roomTypeAppService).CreateAsync(propertyRateStrategyDto);

            Assert.True(_notificationHandler.HasNotification());
        }

        [Fact]
        public async Task Should_Does_Not_Create_When_IncrementAmount_And_PercentualAmount_Is_Null()
        {
            var propertyRateStrategyRepository = Substitute.For<IPropertyRateStrategyRepository>();
            var propertyParameterReadRepository = Substitute.For<IPropertyParameterReadRepository>();
            var ratePlanAppService = Substitute.For<IRatePlanAppService>();
            var propertyRateStrategyDomainService = new PropertyRateStrategyDomainService(propertyRateStrategyRepository, _notificationHandler);
            var propertyRateStrategyReadRepository = Substitute.For<IPropertyRateStrategyReadRepository>();
            var unitOfWorkManager = Substitute.For<IUnitOfWorkManager>();
            var roomTypeAppService = Substitute.For<IRoomTypeAppService>();
            var propertyRateStrategyIdNew = Guid.NewGuid();

            propertyParameterReadRepository.GetSystemDate(1).ReturnsForAnyArgs(x =>
            {
                return new DateTime();
            });

            ratePlanAppService.GetAllBaseRateByPeriodAndPropertyId(new DateTime(), new DateTime(), 1).ReturnsForAnyArgs(x =>
            {
                return new ListDto<RatePlanDto>()
                {
                    Items = new List<RatePlanDto>
                    {
                        new RatePlanDto()
                    }
                };
            });

            propertyRateStrategyRepository.Create(new PropertyRateStrategy()).ReturnsForAnyArgs(x =>
            {
                return new PropertyRateStrategy { Id = propertyRateStrategyIdNew };
            });

            var propertyRateStrategyDto = new PropertyRateStrategyDto
            {
                IsActive = true,
                Name = "Estratégia 01",
                StartDate = new DateTime(2018, 10, 1),
                EndDate = new DateTime(2018, 10, 1),
                PropertyRateStrategyRoomTypeList = new List<PropertyRateStrategyRoomTypeDto>
                {
                    new PropertyRateStrategyRoomTypeDto
                    {
                        RoomTypeId = 1,
                        MinOccupationPercentual = 10,
                        MaxOccupationPercentual = 20,
                        IsDecreased = false,
                        PercentualAmount = null,
                        IncrementAmount = null
                    }
                }
            };

            await new PropertyRateStrategyAppService(_notificationHandler, propertyRateStrategyRepository, propertyParameterReadRepository, new ApplicationUserMock(),
                ratePlanAppService, propertyRateStrategyDomainService, propertyRateStrategyReadRepository, unitOfWorkManager, roomTypeAppService).CreateAsync(propertyRateStrategyDto);

            Assert.True(_notificationHandler.HasNotification());
        }

        [Fact]
        public async Task Should_Does_Not_Create_When_MinOccupationPercentual_Is_Less_Than_Zero()
        {
            var propertyRateStrategyRepository = Substitute.For<IPropertyRateStrategyRepository>();
            var propertyParameterReadRepository = Substitute.For<IPropertyParameterReadRepository>();
            var ratePlanAppService = Substitute.For<IRatePlanAppService>();
            var propertyRateStrategyDomainService = new PropertyRateStrategyDomainService(propertyRateStrategyRepository, _notificationHandler);
            var propertyRateStrategyReadRepository = Substitute.For<IPropertyRateStrategyReadRepository>();
            var unitOfWorkManager = Substitute.For<IUnitOfWorkManager>();
            var roomTypeAppService = Substitute.For<IRoomTypeAppService>();
            var propertyRateStrategyIdNew = Guid.NewGuid();

            propertyParameterReadRepository.GetSystemDate(1).ReturnsForAnyArgs(x =>
            {
                return new DateTime();
            });

            ratePlanAppService.GetAllBaseRateByPeriodAndPropertyId(new DateTime(), new DateTime(), 1).ReturnsForAnyArgs(x =>
            {
                return new ListDto<RatePlanDto>()
                {
                    Items = new List<RatePlanDto>
                    {
                        new RatePlanDto()
                    }
                };
            });

            propertyRateStrategyRepository.Create(new PropertyRateStrategy()).ReturnsForAnyArgs(x =>
            {
                return new PropertyRateStrategy { Id = propertyRateStrategyIdNew };
            });

            var propertyRateStrategyDto = new PropertyRateStrategyDto
            {
                IsActive = true,
                Name = "Estratégia 01",
                StartDate = new DateTime(2018, 10, 1),
                EndDate = new DateTime(2018, 10, 1),
                PropertyRateStrategyRoomTypeList = new List<PropertyRateStrategyRoomTypeDto>
                {
                    new PropertyRateStrategyRoomTypeDto
                    {
                        RoomTypeId = 1,
                        MinOccupationPercentual = -1,
                        MaxOccupationPercentual = 20,
                        IsDecreased = false,
                        PercentualAmount = 10,
                    }
                }
            };

            await new PropertyRateStrategyAppService(_notificationHandler, propertyRateStrategyRepository, propertyParameterReadRepository, new ApplicationUserMock(),
                ratePlanAppService, propertyRateStrategyDomainService, propertyRateStrategyReadRepository, unitOfWorkManager, roomTypeAppService).CreateAsync(propertyRateStrategyDto);

            Assert.True(_notificationHandler.HasNotification());
        }

        [Fact]
        public async Task Should_Does_Not_Create_When_MaxOccupationPercentual_Is_Less_Than_Zero()
        {
            var propertyRateStrategyRepository = Substitute.For<IPropertyRateStrategyRepository>();
            var propertyParameterReadRepository = Substitute.For<IPropertyParameterReadRepository>();
            var ratePlanAppService = Substitute.For<IRatePlanAppService>();
            var propertyRateStrategyDomainService = new PropertyRateStrategyDomainService(propertyRateStrategyRepository, _notificationHandler);
            var propertyRateStrategyReadRepository = Substitute.For<IPropertyRateStrategyReadRepository>();
            var unitOfWorkManager = Substitute.For<IUnitOfWorkManager>();
            var roomTypeAppService = Substitute.For<IRoomTypeAppService>();
            var propertyRateStrategyIdNew = Guid.NewGuid();

            propertyParameterReadRepository.GetSystemDate(1).ReturnsForAnyArgs(x =>
            {
                return new DateTime();
            });

            ratePlanAppService.GetAllBaseRateByPeriodAndPropertyId(new DateTime(), new DateTime(), 1).ReturnsForAnyArgs(x =>
            {
                return new ListDto<RatePlanDto>()
                {
                    Items = new List<RatePlanDto>
                    {
                        new RatePlanDto()
                    }
                };
            });

            propertyRateStrategyRepository.Create(new PropertyRateStrategy()).ReturnsForAnyArgs(x =>
            {
                return new PropertyRateStrategy { Id = propertyRateStrategyIdNew };
            });

            var propertyRateStrategyDto = new PropertyRateStrategyDto
            {
                IsActive = true,
                Name = "Estratégia 01",
                StartDate = new DateTime(2018, 10, 1),
                EndDate = new DateTime(2018, 10, 1),
                PropertyRateStrategyRoomTypeList = new List<PropertyRateStrategyRoomTypeDto>
                {
                    new PropertyRateStrategyRoomTypeDto
                    {
                        RoomTypeId = 1,
                        MinOccupationPercentual = 2,
                        MaxOccupationPercentual = -1,
                        IsDecreased = false,
                        PercentualAmount = 10,
                    }
                }
            };

            await new PropertyRateStrategyAppService(_notificationHandler, propertyRateStrategyRepository, propertyParameterReadRepository, new ApplicationUserMock(),
                ratePlanAppService, propertyRateStrategyDomainService, propertyRateStrategyReadRepository, unitOfWorkManager, roomTypeAppService).CreateAsync(propertyRateStrategyDto);

            Assert.True(_notificationHandler.HasNotification());
        }

        [Fact]
        public async Task Should_Does_Not_Create_When_MinOccupationPercentual_Is_Bigger_Than_One_Hundred()
        {
            var propertyRateStrategyRepository = Substitute.For<IPropertyRateStrategyRepository>();
            var propertyParameterReadRepository = Substitute.For<IPropertyParameterReadRepository>();
            var ratePlanAppService = Substitute.For<IRatePlanAppService>();
            var propertyRateStrategyDomainService = new PropertyRateStrategyDomainService(propertyRateStrategyRepository, _notificationHandler);
            var propertyRateStrategyReadRepository = Substitute.For<IPropertyRateStrategyReadRepository>();
            var unitOfWorkManager = Substitute.For<IUnitOfWorkManager>();
            var roomTypeAppService = Substitute.For<IRoomTypeAppService>();
            var propertyRateStrategyIdNew = Guid.NewGuid();

            propertyParameterReadRepository.GetSystemDate(1).ReturnsForAnyArgs(x =>
            {
                return new DateTime();
            });

            ratePlanAppService.GetAllBaseRateByPeriodAndPropertyId(new DateTime(), new DateTime(), 1).ReturnsForAnyArgs(x =>
            {
                return new ListDto<RatePlanDto>()
                {
                    Items = new List<RatePlanDto>
                    {
                        new RatePlanDto()
                    }
                };
            });

            propertyRateStrategyRepository.Create(new PropertyRateStrategy()).ReturnsForAnyArgs(x =>
            {
                return new PropertyRateStrategy { Id = propertyRateStrategyIdNew };
            });

            var propertyRateStrategyDto = new PropertyRateStrategyDto
            {
                IsActive = true,
                Name = "Estratégia 01",
                StartDate = new DateTime(2018, 10, 1),
                EndDate = new DateTime(2018, 10, 1),
                PropertyRateStrategyRoomTypeList = new List<PropertyRateStrategyRoomTypeDto>
                {
                    new PropertyRateStrategyRoomTypeDto
                    {
                        RoomTypeId = 1,
                        MinOccupationPercentual = 101,
                        MaxOccupationPercentual = 10,
                        IsDecreased = false,
                        PercentualAmount = 10,
                    }
                }
            };

            await new PropertyRateStrategyAppService(_notificationHandler, propertyRateStrategyRepository, propertyParameterReadRepository, new ApplicationUserMock(),
                ratePlanAppService, propertyRateStrategyDomainService, propertyRateStrategyReadRepository, unitOfWorkManager, roomTypeAppService).CreateAsync(propertyRateStrategyDto);

            Assert.True(_notificationHandler.HasNotification());
        }

        [Fact]
        public async Task Should_Does_Not_Create_When_MaxOccupationPercentual_Is_Bigger_Than_One_Hundred()
        {
            var propertyRateStrategyRepository = Substitute.For<IPropertyRateStrategyRepository>();
            var propertyParameterReadRepository = Substitute.For<IPropertyParameterReadRepository>();
            var ratePlanAppService = Substitute.For<IRatePlanAppService>();
            var propertyRateStrategyDomainService = new PropertyRateStrategyDomainService(propertyRateStrategyRepository, _notificationHandler);
            var propertyRateStrategyReadRepository = Substitute.For<IPropertyRateStrategyReadRepository>();
            var unitOfWorkManager = Substitute.For<IUnitOfWorkManager>();
            var roomTypeAppService = Substitute.For<IRoomTypeAppService>();
            var propertyRateStrategyIdNew = Guid.NewGuid();

            propertyParameterReadRepository.GetSystemDate(1).ReturnsForAnyArgs(x =>
            {
                return new DateTime();
            });

            ratePlanAppService.GetAllBaseRateByPeriodAndPropertyId(new DateTime(), new DateTime(), 1).ReturnsForAnyArgs(x =>
            {
                return new ListDto<RatePlanDto>()
                {
                    Items = new List<RatePlanDto>
                    {
                        new RatePlanDto()
                    }
                };
            });

            propertyRateStrategyRepository.Create(new PropertyRateStrategy()).ReturnsForAnyArgs(x =>
            {
                return new PropertyRateStrategy { Id = propertyRateStrategyIdNew };
            });

            var propertyRateStrategyDto = new PropertyRateStrategyDto
            {
                IsActive = true,
                Name = "Estratégia 01",
                StartDate = new DateTime(2018, 10, 1),
                EndDate = new DateTime(2018, 10, 1),
                PropertyRateStrategyRoomTypeList = new List<PropertyRateStrategyRoomTypeDto>
                {
                    new PropertyRateStrategyRoomTypeDto
                    {
                        RoomTypeId = 1,
                        MinOccupationPercentual = 50,
                        MaxOccupationPercentual = 101,
                        IsDecreased = false,
                        PercentualAmount = 10,
                    }
                }
            };

            await new PropertyRateStrategyAppService(_notificationHandler, propertyRateStrategyRepository, propertyParameterReadRepository, new ApplicationUserMock(),
                ratePlanAppService, propertyRateStrategyDomainService, propertyRateStrategyReadRepository, unitOfWorkManager, roomTypeAppService).CreateAsync(propertyRateStrategyDto);

            Assert.True(_notificationHandler.HasNotification());
        }

        [Fact]
        public async Task Should_Does_Not_Create_When_PercentualAmount_Is_Less_Than_Zero()
        {
            var propertyRateStrategyRepository = Substitute.For<IPropertyRateStrategyRepository>();
            var propertyParameterReadRepository = Substitute.For<IPropertyParameterReadRepository>();
            var ratePlanAppService = Substitute.For<IRatePlanAppService>();
            var propertyRateStrategyDomainService = new PropertyRateStrategyDomainService(propertyRateStrategyRepository, _notificationHandler);
            var propertyRateStrategyReadRepository = Substitute.For<IPropertyRateStrategyReadRepository>();
            var unitOfWorkManager = Substitute.For<IUnitOfWorkManager>();
            var roomTypeAppService = Substitute.For<IRoomTypeAppService>();
            var propertyRateStrategyIdNew = Guid.NewGuid();

            propertyParameterReadRepository.GetSystemDate(1).ReturnsForAnyArgs(x =>
            {
                return new DateTime();
            });

            ratePlanAppService.GetAllBaseRateByPeriodAndPropertyId(new DateTime(), new DateTime(), 1).ReturnsForAnyArgs(x =>
            {
                return new ListDto<RatePlanDto>()
                {
                    Items = new List<RatePlanDto>
                    {
                        new RatePlanDto()
                    }
                };
            });

            propertyRateStrategyRepository.Create(new PropertyRateStrategy()).ReturnsForAnyArgs(x =>
            {
                return new PropertyRateStrategy { Id = propertyRateStrategyIdNew };
            });

            var propertyRateStrategyDto = new PropertyRateStrategyDto
            {
                IsActive = true,
                Name = "Estratégia 01",
                StartDate = new DateTime(2018, 10, 1),
                EndDate = new DateTime(2018, 10, 1),
                PropertyRateStrategyRoomTypeList = new List<PropertyRateStrategyRoomTypeDto>
                {
                    new PropertyRateStrategyRoomTypeDto
                    {
                        RoomTypeId = 1,
                        MinOccupationPercentual = 50,
                        MaxOccupationPercentual = 60,
                        IsDecreased = false,
                        PercentualAmount = -1,
                    }
                }
            };

            await new PropertyRateStrategyAppService(_notificationHandler, propertyRateStrategyRepository, propertyParameterReadRepository, new ApplicationUserMock(),
                ratePlanAppService, propertyRateStrategyDomainService, propertyRateStrategyReadRepository, unitOfWorkManager, roomTypeAppService).CreateAsync(propertyRateStrategyDto);

            Assert.True(_notificationHandler.HasNotification());
        }

        [Fact]
        public async Task Should_Does_Not_Create_When_MinOccupationPercentual_Is_Bigger_Than_MaxOccupationPercentual()
        {
            var propertyRateStrategyRepository = Substitute.For<IPropertyRateStrategyRepository>();
            var propertyParameterReadRepository = Substitute.For<IPropertyParameterReadRepository>();
            var ratePlanAppService = Substitute.For<IRatePlanAppService>();
            var propertyRateStrategyDomainService = new PropertyRateStrategyDomainService(propertyRateStrategyRepository, _notificationHandler);
            var propertyRateStrategyReadRepository = Substitute.For<IPropertyRateStrategyReadRepository>();
            var unitOfWorkManager = Substitute.For<IUnitOfWorkManager>();
            var roomTypeAppService = Substitute.For<IRoomTypeAppService>();
            var propertyRateStrategyIdNew = Guid.NewGuid();

            propertyParameterReadRepository.GetSystemDate(1).ReturnsForAnyArgs(x =>
            {
                return new DateTime();
            });

            ratePlanAppService.GetAllBaseRateByPeriodAndPropertyId(new DateTime(), new DateTime(), 1).ReturnsForAnyArgs(x =>
            {
                return new ListDto<RatePlanDto>()
                {
                    Items = new List<RatePlanDto>
                    {
                        new RatePlanDto()
                    }
                };
            });

            propertyRateStrategyRepository.Create(new PropertyRateStrategy()).ReturnsForAnyArgs(x =>
            {
                return new PropertyRateStrategy { Id = propertyRateStrategyIdNew };
            });

            var propertyRateStrategyDto = new PropertyRateStrategyDto
            {
                IsActive = true,
                Name = "Estratégia 01",
                StartDate = new DateTime(2018, 10, 1),
                EndDate = new DateTime(2018, 10, 1),
                PropertyRateStrategyRoomTypeList = new List<PropertyRateStrategyRoomTypeDto>
                {
                    new PropertyRateStrategyRoomTypeDto
                    {
                        RoomTypeId = 1,
                        MinOccupationPercentual = 50,
                        MaxOccupationPercentual = 10,
                        IsDecreased = false,
                        PercentualAmount = 90,
                    }
                }
            };

            await new PropertyRateStrategyAppService(_notificationHandler, propertyRateStrategyRepository, propertyParameterReadRepository, new ApplicationUserMock(),
                ratePlanAppService, propertyRateStrategyDomainService, propertyRateStrategyReadRepository, unitOfWorkManager, roomTypeAppService).CreateAsync(propertyRateStrategyDto);

            Assert.True(_notificationHandler.HasNotification());
        }

        [Fact]
        public async Task Should_Does_Not_Create_When_Invalidate_Range_Occupation()
        {
            var propertyRateStrategyRepository = Substitute.For<IPropertyRateStrategyRepository>();
            var propertyParameterReadRepository = Substitute.For<IPropertyParameterReadRepository>();
            var ratePlanAppService = Substitute.For<IRatePlanAppService>();
            var propertyRateStrategyDomainService = new PropertyRateStrategyDomainService(propertyRateStrategyRepository, _notificationHandler);
            var propertyRateStrategyReadRepository = Substitute.For<IPropertyRateStrategyReadRepository>();
            var unitOfWorkManager = Substitute.For<IUnitOfWorkManager>();
            var roomTypeAppService = Substitute.For<IRoomTypeAppService>();
            var propertyRateStrategyIdNew = Guid.NewGuid();

            propertyParameterReadRepository.GetSystemDate(1).ReturnsForAnyArgs(x =>
            {
                return new DateTime();
            });

            ratePlanAppService.GetAllBaseRateByPeriodAndPropertyId(new DateTime(), new DateTime(), 1).ReturnsForAnyArgs(x =>
            {
                return new ListDto<RatePlanDto>()
                {
                    Items = new List<RatePlanDto>
                    {
                        new RatePlanDto()
                    }
                };
            });

            propertyRateStrategyRepository.Create(new PropertyRateStrategy()).ReturnsForAnyArgs(x =>
            {
                return new PropertyRateStrategy { Id = propertyRateStrategyIdNew };
            });

            propertyRateStrategyReadRepository.GetAllWithRatePlanByPropertyIdAsync(1).ReturnsForAnyArgs(x =>
            {
                return new List<GetAllPropertyRateStrategyWithRatePlanDto>
                {
                    new GetAllPropertyRateStrategyWithRatePlanDto
                    {
                        StartDate = new DateTime(2018, 10, 1),
                        EndDate = new DateTime(2018, 10, 3),
                        PropertyRateStrategyRoomTypeList = new List<PropertyRateStrategyRoomTypeDto>
                        {
                            new PropertyRateStrategyRoomTypeDto
                            {
                                RoomTypeId = 1,
                                MinOccupationPercentual = 1,
                                MaxOccupationPercentual = 10,
                                IsDecreased = false,
                                PercentualAmount = 90
                            }
                        }
                    }
                };
            });

            var propertyRateStrategyDto = new PropertyRateStrategyDto
            {
                IsActive = true,
                Name = "Estratégia 01",
                StartDate = new DateTime(2018, 10, 1),
                EndDate = new DateTime(2018, 10, 3),
                PropertyRateStrategyRoomTypeList = new List<PropertyRateStrategyRoomTypeDto>
                {
                    new PropertyRateStrategyRoomTypeDto
                    {
                        RoomTypeId = 1,
                        MinOccupationPercentual = 1,
                        MaxOccupationPercentual = 10,
                        IsDecreased = false,
                        PercentualAmount = 100
                    }
                }
            };

            await new PropertyRateStrategyAppService(_notificationHandler, propertyRateStrategyRepository, propertyParameterReadRepository, new ApplicationUserMock(),
                ratePlanAppService, propertyRateStrategyDomainService, propertyRateStrategyReadRepository, unitOfWorkManager, roomTypeAppService).CreateAsync(propertyRateStrategyDto);

            Assert.True(_notificationHandler.HasNotification());
        }
        #endregion
        
        #region Update
        [Fact]
        public async Task Should_Update()
        {
            var propertyRateStrategyRepository = Substitute.For<IPropertyRateStrategyRepository>();
            var propertyParameterReadRepository = Substitute.For<IPropertyParameterReadRepository>();
            var ratePlanAppService = Substitute.For<IRatePlanAppService>();
            var propertyRateStrategyDomainService = new PropertyRateStrategyDomainService(propertyRateStrategyRepository, _notificationHandler);
            var propertyRateStrategyReadRepository = Substitute.For<IPropertyRateStrategyReadRepository>();
            var unitOfWorkManager = Substitute.For<IUnitOfWorkManager>();
            var roomTypeAppService = Substitute.For<IRoomTypeAppService>();
            var propertyRateStrategyIdNew = Guid.NewGuid();

            propertyRateStrategyRepository.GetById(Guid.NewGuid()).ReturnsForAnyArgs(x =>
            {
                return new PropertyRateStrategy();
            });

            propertyParameterReadRepository.GetSystemDate(1).ReturnsForAnyArgs(x =>
            {
                return new DateTime();
            });

            ratePlanAppService.GetAllBaseRateByPeriodAndPropertyId(new DateTime(), new DateTime(), 1).ReturnsForAnyArgs(x =>
            {
                return new ListDto<RatePlanDto>()
                {
                    Items = new List<RatePlanDto>
                    {
                        new RatePlanDto()
                    }
                };
            });

            propertyRateStrategyRepository.Update(new PropertyRateStrategy(), new PropertyRateStrategy()).ReturnsForAnyArgs(x =>
            {
                return new PropertyRateStrategy { Id = propertyRateStrategyIdNew };
            });

            var propertyRateStrategyDto = new PropertyRateStrategyDto
            {
                IsActive = true,
                Name = "Estratégia 01",
                StartDate = new DateTime(2018, 10, 1),
                EndDate = new DateTime(2018, 10, 1),
                PropertyRateStrategyRoomTypeList = new List<PropertyRateStrategyRoomTypeDto>
                {
                    new PropertyRateStrategyRoomTypeDto
                    {
                        RoomTypeId = 1,
                        MinOccupationPercentual = 10,
                        MaxOccupationPercentual = 20,
                        IsDecreased = false,
                        PercentualAmount = 20
                    }
                }
            };

            await new PropertyRateStrategyAppService(_notificationHandler, propertyRateStrategyRepository, propertyParameterReadRepository, new ApplicationUserMock(),
                ratePlanAppService, propertyRateStrategyDomainService, propertyRateStrategyReadRepository, unitOfWorkManager, roomTypeAppService).UpdateAsync(Guid.NewGuid(), propertyRateStrategyDto);

            Assert.False(_notificationHandler.HasNotification());
        }

        [Fact]
        public async Task Should_Does_Not_Update_When_SystemDate_Is_Null()
        {
            var propertyRateStrategyRepository = Substitute.For<IPropertyRateStrategyRepository>();
            var propertyParameterReadRepository = Substitute.For<IPropertyParameterReadRepository>();
            var ratePlanAppService = Substitute.For<IRatePlanAppService>();
            var propertyRateStrategyDomainService = new PropertyRateStrategyDomainService(propertyRateStrategyRepository, _notificationHandler);
            var propertyRateStrategyReadRepository = Substitute.For<IPropertyRateStrategyReadRepository>();
            var unitOfWorkManager = Substitute.For<IUnitOfWorkManager>();
            var roomTypeAppService = Substitute.For<IRoomTypeAppService>();
            var propertyRateStrategyIdNew = Guid.NewGuid();

            propertyRateStrategyRepository.GetById(Guid.NewGuid()).ReturnsForAnyArgs(x =>
            {
                return new PropertyRateStrategy();
            });

            propertyParameterReadRepository.GetSystemDate(1).ReturnsForAnyArgs(x =>
            {
                return null;
            });

            ratePlanAppService.GetAllBaseRateByPeriodAndPropertyId(new DateTime(), new DateTime(), 1).ReturnsForAnyArgs(x =>
            {
                return new ListDto<RatePlanDto>()
                {
                    Items = new List<RatePlanDto>
                    {
                        new RatePlanDto()
                    }
                };
            });

            propertyRateStrategyRepository.Update(new PropertyRateStrategy(), new PropertyRateStrategy()).ReturnsForAnyArgs(x =>
            {
                return new PropertyRateStrategy { Id = propertyRateStrategyIdNew };
            });

            var propertyRateStrategyDto = new PropertyRateStrategyDto
            {
                IsActive = true,
                Name = "Estratégia 01",
                StartDate = new DateTime(2018, 10, 1),
                EndDate = new DateTime(2018, 10, 1),
                PropertyRateStrategyRoomTypeList = new List<PropertyRateStrategyRoomTypeDto>
                {
                    new PropertyRateStrategyRoomTypeDto
                    {
                        RoomTypeId = 1,
                        MinOccupationPercentual = 10,
                        MaxOccupationPercentual = 20,
                        IsDecreased = false,
                        PercentualAmount = 20
                    }
                }
            };

            await new PropertyRateStrategyAppService(_notificationHandler, propertyRateStrategyRepository, propertyParameterReadRepository, new ApplicationUserMock(),
                ratePlanAppService, propertyRateStrategyDomainService, propertyRateStrategyReadRepository, unitOfWorkManager, roomTypeAppService).UpdateAsync(Guid.NewGuid(), propertyRateStrategyDto);

            Assert.True(_notificationHandler.HasNotification());
        }

        [Fact]
        public async Task Should_Does_Not_Update_When_Period_Is_Invalid()
        {
            var propertyRateStrategyRepository = Substitute.For<IPropertyRateStrategyRepository>();
            var propertyParameterReadRepository = Substitute.For<IPropertyParameterReadRepository>();
            var ratePlanAppService = Substitute.For<IRatePlanAppService>();
            var propertyRateStrategyDomainService = new PropertyRateStrategyDomainService(propertyRateStrategyRepository, _notificationHandler);
            var propertyRateStrategyReadRepository = Substitute.For<IPropertyRateStrategyReadRepository>();
            var unitOfWorkManager = Substitute.For<IUnitOfWorkManager>();
            var roomTypeAppService = Substitute.For<IRoomTypeAppService>();
            var propertyRateStrategyIdNew = Guid.NewGuid();

            propertyRateStrategyRepository.GetById(Guid.NewGuid()).ReturnsForAnyArgs(x =>
            {
                return new PropertyRateStrategy();
            });

            propertyParameterReadRepository.GetSystemDate(1).ReturnsForAnyArgs(x =>
            {
                return new DateTime();
            });

            ratePlanAppService.GetAllBaseRateByPeriodAndPropertyId(new DateTime(), new DateTime(), 1).ReturnsForAnyArgs(x =>
            {
                return new ListDto<RatePlanDto>()
                {
                    Items = new List<RatePlanDto>
                    {
                        new RatePlanDto()
                    }
                };
            });

            propertyRateStrategyRepository.Update(new PropertyRateStrategy(), new PropertyRateStrategy()).ReturnsForAnyArgs(x =>
            {
                return new PropertyRateStrategy { Id = propertyRateStrategyIdNew };
            });

            var propertyRateStrategyDto = new PropertyRateStrategyDto
            {
                IsActive = true,
                Name = "Estratégia 01",
                StartDate = new DateTime(2018, 10, 2),
                EndDate = new DateTime(2018, 10, 1),
                PropertyRateStrategyRoomTypeList = new List<PropertyRateStrategyRoomTypeDto>
                {
                    new PropertyRateStrategyRoomTypeDto
                    {
                        RoomTypeId = 1,
                        MinOccupationPercentual = 10,
                        MaxOccupationPercentual = 20,
                        IsDecreased = false,
                        PercentualAmount = 20
                    }
                }
            };

            await new PropertyRateStrategyAppService(_notificationHandler, propertyRateStrategyRepository, propertyParameterReadRepository, new ApplicationUserMock(),
                ratePlanAppService, propertyRateStrategyDomainService, propertyRateStrategyReadRepository, unitOfWorkManager, roomTypeAppService).UpdateAsync(Guid.NewGuid(), propertyRateStrategyDto);

            Assert.True(_notificationHandler.HasNotification());
        }

        [Fact]
        public async Task Should_Does_Not_Update_When_Period_Is_Overlapped()
        {
            var propertyRateStrategyRepository = Substitute.For<IPropertyRateStrategyRepository>();
            var propertyParameterReadRepository = Substitute.For<IPropertyParameterReadRepository>();
            var ratePlanAppService = Substitute.For<IRatePlanAppService>();
            var propertyRateStrategyDomainService = new PropertyRateStrategyDomainService(propertyRateStrategyRepository, _notificationHandler);
            var propertyRateStrategyReadRepository = Substitute.For<IPropertyRateStrategyReadRepository>();
            var unitOfWorkManager = Substitute.For<IUnitOfWorkManager>();
            var roomTypeAppService = Substitute.For<IRoomTypeAppService>();
            var propertyRateStrategyIdNew = Guid.NewGuid();

            propertyRateStrategyRepository.GetById(Guid.NewGuid()).ReturnsForAnyArgs(x =>
            {
                return new PropertyRateStrategy();
            });

            propertyParameterReadRepository.GetSystemDate(1).ReturnsForAnyArgs(x =>
            {
                return new DateTime();
            });

            ratePlanAppService.GetAllBaseRateByPeriodAndPropertyId(new DateTime(), new DateTime(), 1).ReturnsForAnyArgs(x =>
            {
                return new ListDto<RatePlanDto>()
                {
                    Items = new List<RatePlanDto>
                    {
                        new RatePlanDto()
                    }
                };
            });

            propertyRateStrategyRepository.Update(new PropertyRateStrategy(), new PropertyRateStrategy()).ReturnsForAnyArgs(x =>
            {
                return new PropertyRateStrategy { Id = propertyRateStrategyIdNew };
            });

            var propertyRateStrategyDto = new PropertyRateStrategyDto
            {
                IsActive = true,
                Name = "Estratégia 01",
                StartDate = new DateTime(2018, 10, 1),
                EndDate = new DateTime(2018, 10, 1),
                PropertyRateStrategyRoomTypeList = new List<PropertyRateStrategyRoomTypeDto>
                {
                    new PropertyRateStrategyRoomTypeDto
                    {
                        RoomTypeId = 1,
                        MinOccupationPercentual = 10,
                        MaxOccupationPercentual = 20,
                        IsDecreased = false,
                        PercentualAmount = 20
                    },
                    new PropertyRateStrategyRoomTypeDto
                    {
                        RoomTypeId = 1,
                        MinOccupationPercentual = 8,
                        MaxOccupationPercentual = 13,
                        IsDecreased = false,
                        PercentualAmount = 20
                    }
                }
            };

            await new PropertyRateStrategyAppService(_notificationHandler, propertyRateStrategyRepository, propertyParameterReadRepository, new ApplicationUserMock(),
                ratePlanAppService, propertyRateStrategyDomainService, propertyRateStrategyReadRepository, unitOfWorkManager, roomTypeAppService).UpdateAsync(Guid.NewGuid(), propertyRateStrategyDto);

            Assert.True(_notificationHandler.HasNotification());
        }

        [Fact]
        public async Task Should_Does_Not_Update_When_IncrementAmount_And_PercentualAmount_Is_Not_Null()
        {
            var propertyRateStrategyRepository = Substitute.For<IPropertyRateStrategyRepository>();
            var propertyParameterReadRepository = Substitute.For<IPropertyParameterReadRepository>();
            var ratePlanAppService = Substitute.For<IRatePlanAppService>();
            var propertyRateStrategyDomainService = new PropertyRateStrategyDomainService(propertyRateStrategyRepository, _notificationHandler);
            var propertyRateStrategyReadRepository = Substitute.For<IPropertyRateStrategyReadRepository>();
            var unitOfWorkManager = Substitute.For<IUnitOfWorkManager>();
            var roomTypeAppService = Substitute.For<IRoomTypeAppService>();
            var propertyRateStrategyIdNew = Guid.NewGuid();

            propertyParameterReadRepository.GetSystemDate(1).ReturnsForAnyArgs(x =>
            {
                return new DateTime();
            });

            ratePlanAppService.GetAllBaseRateByPeriodAndPropertyId(new DateTime(), new DateTime(), 1).ReturnsForAnyArgs(x =>
            {
                return new ListDto<RatePlanDto>()
                {
                    Items = new List<RatePlanDto>
                    {
                        new RatePlanDto()
                    }
                };
            });

            propertyRateStrategyRepository.Create(new PropertyRateStrategy()).ReturnsForAnyArgs(x =>
            {
                return new PropertyRateStrategy { Id = propertyRateStrategyIdNew };
            });

            var propertyRateStrategyDto = new PropertyRateStrategyDto
            {
                IsActive = true,
                Name = "Estratégia 01",
                StartDate = new DateTime(2018, 10, 1),
                EndDate = new DateTime(2018, 10, 1),
                PropertyRateStrategyRoomTypeList = new List<PropertyRateStrategyRoomTypeDto>
                {
                    new PropertyRateStrategyRoomTypeDto
                    {
                        RoomTypeId = 1,
                        MinOccupationPercentual = 10,
                        MaxOccupationPercentual = 20,
                        IsDecreased = false,
                        PercentualAmount = 20,
                        IncrementAmount = 10
                    }
                }
            };

            await new PropertyRateStrategyAppService(_notificationHandler, propertyRateStrategyRepository, propertyParameterReadRepository, new ApplicationUserMock(),
                ratePlanAppService, propertyRateStrategyDomainService, propertyRateStrategyReadRepository, unitOfWorkManager, roomTypeAppService)
                .UpdateAsync(Guid.NewGuid(), propertyRateStrategyDto);

            Assert.True(_notificationHandler.HasNotification());
        }

        [Fact]
        public async Task Should_Does_Not_Update_When_IncrementAmount_And_PercentualAmount_Is_Null()
        {
            var propertyRateStrategyRepository = Substitute.For<IPropertyRateStrategyRepository>();
            var propertyParameterReadRepository = Substitute.For<IPropertyParameterReadRepository>();
            var ratePlanAppService = Substitute.For<IRatePlanAppService>();
            var propertyRateStrategyDomainService = new PropertyRateStrategyDomainService(propertyRateStrategyRepository, _notificationHandler);
            var propertyRateStrategyReadRepository = Substitute.For<IPropertyRateStrategyReadRepository>();
            var unitOfWorkManager = Substitute.For<IUnitOfWorkManager>();
            var roomTypeAppService = Substitute.For<IRoomTypeAppService>();
            var propertyRateStrategyIdNew = Guid.NewGuid();

            propertyParameterReadRepository.GetSystemDate(1).ReturnsForAnyArgs(x =>
            {
                return new DateTime();
            });

            ratePlanAppService.GetAllBaseRateByPeriodAndPropertyId(new DateTime(), new DateTime(), 1).ReturnsForAnyArgs(x =>
            {
                return new ListDto<RatePlanDto>()
                {
                    Items = new List<RatePlanDto>
                    {
                        new RatePlanDto()
                    }
                };
            });

            propertyRateStrategyRepository.Create(new PropertyRateStrategy()).ReturnsForAnyArgs(x =>
            {
                return new PropertyRateStrategy { Id = propertyRateStrategyIdNew };
            });

            var propertyRateStrategyDto = new PropertyRateStrategyDto
            {
                IsActive = true,
                Name = "Estratégia 01",
                StartDate = new DateTime(2018, 10, 1),
                EndDate = new DateTime(2018, 10, 1),
                PropertyRateStrategyRoomTypeList = new List<PropertyRateStrategyRoomTypeDto>
                {
                    new PropertyRateStrategyRoomTypeDto
                    {
                        RoomTypeId = 1,
                        MinOccupationPercentual = 10,
                        MaxOccupationPercentual = 20,
                        IsDecreased = false,
                        PercentualAmount = null,
                        IncrementAmount = null
                    }
                }
            };

            await new PropertyRateStrategyAppService(_notificationHandler, propertyRateStrategyRepository, propertyParameterReadRepository, new ApplicationUserMock(),
                ratePlanAppService, propertyRateStrategyDomainService, propertyRateStrategyReadRepository, unitOfWorkManager, roomTypeAppService)
                .UpdateAsync(Guid.NewGuid(), propertyRateStrategyDto);

            Assert.True(_notificationHandler.HasNotification());
        }

        [Fact]
        public async Task Should_Does_Not_Update_When_MinOccupationPercentual_Is_Less_Than_Zero()
        {
            var propertyRateStrategyRepository = Substitute.For<IPropertyRateStrategyRepository>();
            var propertyParameterReadRepository = Substitute.For<IPropertyParameterReadRepository>();
            var ratePlanAppService = Substitute.For<IRatePlanAppService>();
            var propertyRateStrategyDomainService = new PropertyRateStrategyDomainService(propertyRateStrategyRepository, _notificationHandler);
            var propertyRateStrategyReadRepository = Substitute.For<IPropertyRateStrategyReadRepository>();
            var unitOfWorkManager = Substitute.For<IUnitOfWorkManager>();
            var roomTypeAppService = Substitute.For<IRoomTypeAppService>();
            var propertyRateStrategyIdNew = Guid.NewGuid();

            propertyParameterReadRepository.GetSystemDate(1).ReturnsForAnyArgs(x =>
            {
                return new DateTime();
            });

            ratePlanAppService.GetAllBaseRateByPeriodAndPropertyId(new DateTime(), new DateTime(), 1).ReturnsForAnyArgs(x =>
            {
                return new ListDto<RatePlanDto>()
                {
                    Items = new List<RatePlanDto>
                    {
                        new RatePlanDto()
                    }
                };
            });

            propertyRateStrategyRepository.Create(new PropertyRateStrategy()).ReturnsForAnyArgs(x =>
            {
                return new PropertyRateStrategy { Id = propertyRateStrategyIdNew };
            });

            var propertyRateStrategyDto = new PropertyRateStrategyDto
            {
                IsActive = true,
                Name = "Estratégia 01",
                StartDate = new DateTime(2018, 10, 1),
                EndDate = new DateTime(2018, 10, 1),
                PropertyRateStrategyRoomTypeList = new List<PropertyRateStrategyRoomTypeDto>
                {
                    new PropertyRateStrategyRoomTypeDto
                    {
                        RoomTypeId = 1,
                        MinOccupationPercentual = -1,
                        MaxOccupationPercentual = 20,
                        IsDecreased = false,
                        PercentualAmount = 10,
                    }
                }
            };

            await new PropertyRateStrategyAppService(_notificationHandler, propertyRateStrategyRepository, propertyParameterReadRepository, new ApplicationUserMock(),
                ratePlanAppService, propertyRateStrategyDomainService, propertyRateStrategyReadRepository, unitOfWorkManager, roomTypeAppService)
                .UpdateAsync(Guid.NewGuid(), propertyRateStrategyDto);

            Assert.True(_notificationHandler.HasNotification());
        }

        [Fact]
        public async Task Should_Does_Not_Update_When_MaxOccupationPercentual_Is_Less_Than_Zero()
        {
            var propertyRateStrategyRepository = Substitute.For<IPropertyRateStrategyRepository>();
            var propertyParameterReadRepository = Substitute.For<IPropertyParameterReadRepository>();
            var ratePlanAppService = Substitute.For<IRatePlanAppService>();
            var propertyRateStrategyDomainService = new PropertyRateStrategyDomainService(propertyRateStrategyRepository, _notificationHandler);
            var propertyRateStrategyReadRepository = Substitute.For<IPropertyRateStrategyReadRepository>();
            var unitOfWorkManager = Substitute.For<IUnitOfWorkManager>();
            var roomTypeAppService = Substitute.For<IRoomTypeAppService>();
            var propertyRateStrategyIdNew = Guid.NewGuid();

            propertyParameterReadRepository.GetSystemDate(1).ReturnsForAnyArgs(x =>
            {
                return new DateTime();
            });

            ratePlanAppService.GetAllBaseRateByPeriodAndPropertyId(new DateTime(), new DateTime(), 1).ReturnsForAnyArgs(x =>
            {
                return new ListDto<RatePlanDto>()
                {
                    Items = new List<RatePlanDto>
                    {
                        new RatePlanDto()
                    }
                };
            });

            propertyRateStrategyRepository.Create(new PropertyRateStrategy()).ReturnsForAnyArgs(x =>
            {
                return new PropertyRateStrategy { Id = propertyRateStrategyIdNew };
            });

            var propertyRateStrategyDto = new PropertyRateStrategyDto
            {
                IsActive = true,
                Name = "Estratégia 01",
                StartDate = new DateTime(2018, 10, 1),
                EndDate = new DateTime(2018, 10, 1),
                PropertyRateStrategyRoomTypeList = new List<PropertyRateStrategyRoomTypeDto>
                {
                    new PropertyRateStrategyRoomTypeDto
                    {
                        RoomTypeId = 1,
                        MinOccupationPercentual = 2,
                        MaxOccupationPercentual = -1,
                        IsDecreased = false,
                        PercentualAmount = 10,
                    }
                }
            };

            await new PropertyRateStrategyAppService(_notificationHandler, propertyRateStrategyRepository, propertyParameterReadRepository, new ApplicationUserMock(),
                ratePlanAppService, propertyRateStrategyDomainService, propertyRateStrategyReadRepository, unitOfWorkManager, roomTypeAppService)
                .UpdateAsync(Guid.NewGuid(), propertyRateStrategyDto);

            Assert.True(_notificationHandler.HasNotification());
        }

        [Fact]
        public async Task Should_Does_Not_Update_When_MinOccupationPercentual_Is_Bigger_Than_One_Hundred()
        {
            var propertyRateStrategyRepository = Substitute.For<IPropertyRateStrategyRepository>();
            var propertyParameterReadRepository = Substitute.For<IPropertyParameterReadRepository>();
            var ratePlanAppService = Substitute.For<IRatePlanAppService>();
            var propertyRateStrategyDomainService = new PropertyRateStrategyDomainService(propertyRateStrategyRepository, _notificationHandler);
            var propertyRateStrategyReadRepository = Substitute.For<IPropertyRateStrategyReadRepository>();
            var unitOfWorkManager = Substitute.For<IUnitOfWorkManager>();
            var roomTypeAppService = Substitute.For<IRoomTypeAppService>();
            var propertyRateStrategyIdNew = Guid.NewGuid();

            propertyParameterReadRepository.GetSystemDate(1).ReturnsForAnyArgs(x =>
            {
                return new DateTime();
            });

            ratePlanAppService.GetAllBaseRateByPeriodAndPropertyId(new DateTime(), new DateTime(), 1).ReturnsForAnyArgs(x =>
            {
                return new ListDto<RatePlanDto>()
                {
                    Items = new List<RatePlanDto>
                    {
                        new RatePlanDto()
                    }
                };
            });

            propertyRateStrategyRepository.Create(new PropertyRateStrategy()).ReturnsForAnyArgs(x =>
            {
                return new PropertyRateStrategy { Id = propertyRateStrategyIdNew };
            });

            var propertyRateStrategyDto = new PropertyRateStrategyDto
            {
                IsActive = true,
                Name = "Estratégia 01",
                StartDate = new DateTime(2018, 10, 1),
                EndDate = new DateTime(2018, 10, 1),
                PropertyRateStrategyRoomTypeList = new List<PropertyRateStrategyRoomTypeDto>
                {
                    new PropertyRateStrategyRoomTypeDto
                    {
                        RoomTypeId = 1,
                        MinOccupationPercentual = 101,
                        MaxOccupationPercentual = 10,
                        IsDecreased = false,
                        PercentualAmount = 10,
                    }
                }
            };

            await new PropertyRateStrategyAppService(_notificationHandler, propertyRateStrategyRepository, propertyParameterReadRepository, new ApplicationUserMock(),
                ratePlanAppService, propertyRateStrategyDomainService, propertyRateStrategyReadRepository, unitOfWorkManager, roomTypeAppService)
                .UpdateAsync(Guid.NewGuid(), propertyRateStrategyDto);

            Assert.True(_notificationHandler.HasNotification());
        }

        [Fact]
        public async Task Should_Does_Not_Update_When_MaxOccupationPercentual_Is_Bigger_Than_One_Hundred()
        {
            var propertyRateStrategyRepository = Substitute.For<IPropertyRateStrategyRepository>();
            var propertyParameterReadRepository = Substitute.For<IPropertyParameterReadRepository>();
            var ratePlanAppService = Substitute.For<IRatePlanAppService>();
            var propertyRateStrategyDomainService = new PropertyRateStrategyDomainService(propertyRateStrategyRepository, _notificationHandler);
            var propertyRateStrategyReadRepository = Substitute.For<IPropertyRateStrategyReadRepository>();
            var unitOfWorkManager = Substitute.For<IUnitOfWorkManager>();
            var roomTypeAppService = Substitute.For<IRoomTypeAppService>();
            var propertyRateStrategyIdNew = Guid.NewGuid();

            propertyParameterReadRepository.GetSystemDate(1).ReturnsForAnyArgs(x =>
            {
                return new DateTime();
            });

            ratePlanAppService.GetAllBaseRateByPeriodAndPropertyId(new DateTime(), new DateTime(), 1).ReturnsForAnyArgs(x =>
            {
                return new ListDto<RatePlanDto>()
                {
                    Items = new List<RatePlanDto>
                    {
                        new RatePlanDto()
                    }
                };
            });

            propertyRateStrategyRepository.Create(new PropertyRateStrategy()).ReturnsForAnyArgs(x =>
            {
                return new PropertyRateStrategy { Id = propertyRateStrategyIdNew };
            });

            var propertyRateStrategyDto = new PropertyRateStrategyDto
            {
                IsActive = true,
                Name = "Estratégia 01",
                StartDate = new DateTime(2018, 10, 1),
                EndDate = new DateTime(2018, 10, 1),
                PropertyRateStrategyRoomTypeList = new List<PropertyRateStrategyRoomTypeDto>
                {
                    new PropertyRateStrategyRoomTypeDto
                    {
                        RoomTypeId = 1,
                        MinOccupationPercentual = 50,
                        MaxOccupationPercentual = 101,
                        IsDecreased = false,
                        PercentualAmount = 10,
                    }
                }
            };

            await new PropertyRateStrategyAppService(_notificationHandler, propertyRateStrategyRepository, propertyParameterReadRepository, new ApplicationUserMock(),
                ratePlanAppService, propertyRateStrategyDomainService, propertyRateStrategyReadRepository, unitOfWorkManager, roomTypeAppService)
                .UpdateAsync(Guid.NewGuid(), propertyRateStrategyDto);

            Assert.True(_notificationHandler.HasNotification());
        }

        [Fact]
        public async Task Should_Does_Not_Update_When_PercentualAmount_Is_Less_Than_Zero()
        {
            var propertyRateStrategyRepository = Substitute.For<IPropertyRateStrategyRepository>();
            var propertyParameterReadRepository = Substitute.For<IPropertyParameterReadRepository>();
            var ratePlanAppService = Substitute.For<IRatePlanAppService>();
            var propertyRateStrategyDomainService = new PropertyRateStrategyDomainService(propertyRateStrategyRepository, _notificationHandler);
            var propertyRateStrategyReadRepository = Substitute.For<IPropertyRateStrategyReadRepository>();
            var unitOfWorkManager = Substitute.For<IUnitOfWorkManager>();
            var roomTypeAppService = Substitute.For<IRoomTypeAppService>();
            var propertyRateStrategyIdNew = Guid.NewGuid();

            propertyParameterReadRepository.GetSystemDate(1).ReturnsForAnyArgs(x =>
            {
                return new DateTime();
            });

            ratePlanAppService.GetAllBaseRateByPeriodAndPropertyId(new DateTime(), new DateTime(), 1).ReturnsForAnyArgs(x =>
            {
                return new ListDto<RatePlanDto>()
                {
                    Items = new List<RatePlanDto>
                    {
                        new RatePlanDto()
                    }
                };
            });

            propertyRateStrategyRepository.Create(new PropertyRateStrategy()).ReturnsForAnyArgs(x =>
            {
                return new PropertyRateStrategy { Id = propertyRateStrategyIdNew };
            });

            var propertyRateStrategyDto = new PropertyRateStrategyDto
            {
                IsActive = true,
                Name = "Estratégia 01",
                StartDate = new DateTime(2018, 10, 1),
                EndDate = new DateTime(2018, 10, 1),
                PropertyRateStrategyRoomTypeList = new List<PropertyRateStrategyRoomTypeDto>
                {
                    new PropertyRateStrategyRoomTypeDto
                    {
                        RoomTypeId = 1,
                        MinOccupationPercentual = 50,
                        MaxOccupationPercentual = 60,
                        IsDecreased = false,
                        PercentualAmount = -1,
                    }
                }
            };

            await new PropertyRateStrategyAppService(_notificationHandler, propertyRateStrategyRepository, propertyParameterReadRepository, new ApplicationUserMock(),
                ratePlanAppService, propertyRateStrategyDomainService, propertyRateStrategyReadRepository, unitOfWorkManager, roomTypeAppService)
                .UpdateAsync(Guid.NewGuid(), propertyRateStrategyDto);

            Assert.True(_notificationHandler.HasNotification());
        }

        [Fact]
        public async Task Should_Does_Not_Update_When_MinOccupationPercentual_Is_Bigger_Than_MaxOccupationPercentual()
        {
            var propertyRateStrategyRepository = Substitute.For<IPropertyRateStrategyRepository>();
            var propertyParameterReadRepository = Substitute.For<IPropertyParameterReadRepository>();
            var ratePlanAppService = Substitute.For<IRatePlanAppService>();
            var propertyRateStrategyDomainService = new PropertyRateStrategyDomainService(propertyRateStrategyRepository, _notificationHandler);
            var propertyRateStrategyReadRepository = Substitute.For<IPropertyRateStrategyReadRepository>();
            var unitOfWorkManager = Substitute.For<IUnitOfWorkManager>();
            var roomTypeAppService = Substitute.For<IRoomTypeAppService>();
            var propertyRateStrategyIdNew = Guid.NewGuid();

            propertyParameterReadRepository.GetSystemDate(1).ReturnsForAnyArgs(x =>
            {
                return new DateTime();
            });

            ratePlanAppService.GetAllBaseRateByPeriodAndPropertyId(new DateTime(), new DateTime(), 1).ReturnsForAnyArgs(x =>
            {
                return new ListDto<RatePlanDto>()
                {
                    Items = new List<RatePlanDto>
                    {
                        new RatePlanDto()
                    }
                };
            });

            propertyRateStrategyRepository.Create(new PropertyRateStrategy()).ReturnsForAnyArgs(x =>
            {
                return new PropertyRateStrategy { Id = propertyRateStrategyIdNew };
            });

            var propertyRateStrategyDto = new PropertyRateStrategyDto
            {
                IsActive = true,
                Name = "Estratégia 01",
                StartDate = new DateTime(2018, 10, 1),
                EndDate = new DateTime(2018, 10, 1),
                PropertyRateStrategyRoomTypeList = new List<PropertyRateStrategyRoomTypeDto>
                {
                    new PropertyRateStrategyRoomTypeDto
                    {
                        RoomTypeId = 1,
                        MinOccupationPercentual = 50,
                        MaxOccupationPercentual = 10,
                        IsDecreased = false,
                        PercentualAmount = 101,
                    }
                }
            };

            await new PropertyRateStrategyAppService(_notificationHandler, propertyRateStrategyRepository, propertyParameterReadRepository, new ApplicationUserMock(),
                ratePlanAppService, propertyRateStrategyDomainService, propertyRateStrategyReadRepository, unitOfWorkManager, roomTypeAppService)
                .UpdateAsync(Guid.NewGuid(), propertyRateStrategyDto);

            Assert.True(_notificationHandler.HasNotification());
        }

        [Fact]
        public async Task Should_Does_Not_Update_When_Invalidate_Range_Occupation()
        {
            var propertyRateStrategyRepository = Substitute.For<IPropertyRateStrategyRepository>();
            var propertyParameterReadRepository = Substitute.For<IPropertyParameterReadRepository>();
            var ratePlanAppService = Substitute.For<IRatePlanAppService>();
            var propertyRateStrategyDomainService = new PropertyRateStrategyDomainService(propertyRateStrategyRepository, _notificationHandler);
            var propertyRateStrategyReadRepository = Substitute.For<IPropertyRateStrategyReadRepository>();
            var unitOfWorkManager = Substitute.For<IUnitOfWorkManager>();
            var roomTypeAppService = Substitute.For<IRoomTypeAppService>();
            var propertyRateStrategyIdNew = Guid.NewGuid();

            propertyParameterReadRepository.GetSystemDate(1).ReturnsForAnyArgs(x =>
            {
                return new DateTime();
            });

            ratePlanAppService.GetAllBaseRateByPeriodAndPropertyId(new DateTime(), new DateTime(), 1).ReturnsForAnyArgs(x =>
            {
                return new ListDto<RatePlanDto>()
                {
                    Items = new List<RatePlanDto>
                    {
                        new RatePlanDto()
                    }
                };
            });

            propertyRateStrategyRepository.Create(new PropertyRateStrategy()).ReturnsForAnyArgs(x =>
            {
                return new PropertyRateStrategy { Id = propertyRateStrategyIdNew };
            });

            propertyRateStrategyReadRepository.GetAllWithRatePlanByPropertyIdAsync(1).ReturnsForAnyArgs(x =>
            {
                return new List<GetAllPropertyRateStrategyWithRatePlanDto>
                {
                    new GetAllPropertyRateStrategyWithRatePlanDto
                    {
                        StartDate = new DateTime(2018, 10, 1),
                        EndDate = new DateTime(2018, 10, 3),
                        PropertyRateStrategyRoomTypeList = new List<PropertyRateStrategyRoomTypeDto>
                        {
                            new PropertyRateStrategyRoomTypeDto
                            {
                                RoomTypeId = 1,
                                MinOccupationPercentual = 1,
                                MaxOccupationPercentual = 10,
                                IsDecreased = false,
                                PercentualAmount = 90
                            }
                        }
                    }
                };
            });

            var propertyRateStrategyDto = new PropertyRateStrategyDto
            {
                IsActive = true,
                Name = "Estratégia 01",
                StartDate = new DateTime(2018, 10, 1),
                EndDate = new DateTime(2018, 10, 3),
                PropertyRateStrategyRoomTypeList = new List<PropertyRateStrategyRoomTypeDto>
                {
                    new PropertyRateStrategyRoomTypeDto
                    {
                        RoomTypeId = 1,
                        MinOccupationPercentual = 1,
                        MaxOccupationPercentual = 10,
                        IsDecreased = false,
                        PercentualAmount = 100,
                    }
                }
            };

            await new PropertyRateStrategyAppService(_notificationHandler, propertyRateStrategyRepository, propertyParameterReadRepository, new ApplicationUserMock(),
                ratePlanAppService, propertyRateStrategyDomainService, propertyRateStrategyReadRepository, unitOfWorkManager, roomTypeAppService)
                .UpdateAsync(Guid.NewGuid(), propertyRateStrategyDto);

            Assert.True(_notificationHandler.HasNotification());
        }
        #endregion

        #region Get
        [Fact]
        public void Should_GetAllByProperty()
        {
            var propertyRateStrategyRepository = Substitute.For<IPropertyRateStrategyRepository>(); 
            var propertyParameterReadRepository = Substitute.For<IPropertyParameterReadRepository>(); 
            var ratePlanAppService = Substitute.For<IRatePlanAppService>(); 
            var propertyRateStrategyDomainService = Substitute.For<IPropertyRateStrategyDomainService>();
            var propertyRateStrategyReadRepository = Substitute.For<IPropertyRateStrategyReadRepository>();
            var unitOfWorkManager = Substitute.For<IUnitOfWorkManager>();
            var roomTypeAppService = Substitute.For<IRoomTypeAppService>();

            propertyRateStrategyReadRepository.GetAllByPropertyId(1).ReturnsForAnyArgs(x =>
            {
                return PropertyRateStrategyMock.GetAllDto();
            });

            new PropertyRateStrategyAppService(_notificationHandler, propertyRateStrategyRepository, propertyParameterReadRepository, 
                                               new ApplicationUserMock(), ratePlanAppService, propertyRateStrategyDomainService, 
                                               propertyRateStrategyReadRepository, unitOfWorkManager, roomTypeAppService)
                                               .GetAllByPropertyId();

            Assert.False(_notificationHandler.HasNotification());
        }

        [Fact]
        public async void Should_GetById()
        {
            var propertyRateStrategyRepository = Substitute.For<IPropertyRateStrategyRepository>();
            var propertyParameterReadRepository = Substitute.For<IPropertyParameterReadRepository>();
            var ratePlanAppService = Substitute.For<IRatePlanAppService>();
            var propertyRateStrategyDomainService = Substitute.For<IPropertyRateStrategyDomainService>();
            var propertyRateStrategyReadRepository = Substitute.For<IPropertyRateStrategyReadRepository>();
            var unitOfWorkManager = Substitute.For<IUnitOfWorkManager>();
            var roomTypeAppService = Substitute.For<IRoomTypeAppService>();

            propertyRateStrategyReadRepository.GetPropertyRateStrategyById(Guid.NewGuid()).ReturnsForAnyArgs(async x =>
            {
                return await PropertyRateStrategyMock.GetDto();
            });

            await new PropertyRateStrategyAppService(_notificationHandler, propertyRateStrategyRepository, propertyParameterReadRepository,
                                               new ApplicationUserMock(), ratePlanAppService, propertyRateStrategyDomainService,
                                               propertyRateStrategyReadRepository, unitOfWorkManager, roomTypeAppService)
                                               .GetPropertyRateStrategyById(Guid.NewGuid());

            Assert.False(_notificationHandler.HasNotification());
        }
        #endregion

    }
}
