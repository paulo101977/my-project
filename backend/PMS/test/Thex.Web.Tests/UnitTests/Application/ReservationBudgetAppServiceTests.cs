﻿using NSubstitute;
using System;
using System.Collections.Generic;
using Thex.Application.Adapters;
using Thex.Application.Interfaces;
using Thex.Application.Services;
using Thex.Domain.Entities;
using Thex.Domain.Interfaces.Repositories;
using Thex.Domain.JoinMap;
using Thex.Domain.Services;
using Thex.Domain.Services.Interfaces;
using Thex.Dto;
using Thex.Dto.Availability;
using Thex.Dto.RoomType;
using Thex.Dto.RoomTypes;
using Thex.Infra.ReadInterfaces;
using Thex.Kernel;
using Tnf.AspNetCore.TestBase;
using Tnf.Dto;
using Tnf.Localization;
using Tnf.Notifications;
using Tnf.Repositories;
using Xunit;

namespace Thex.Web.Tests.UnitTests.Application
{
    public class ReservationBudgetAppServiceTests : TnfAspNetCoreIntegratedTestBase<StartupUnitPropertyTest>
    {
        private INotificationHandler _notificationHandler;

        public ReservationBudgetAppServiceTests()
        {
            _notificationHandler = new NotificationHandler(ServiceProvider);
        }

        [Fact]
        public void Should_GetWithBudgetOffer_When_ReservationItem_Is_Null()
        {
            var repository = Substitute.For<IRepository<ReservationBudget>>();
            var reservationBudgetDomainService = new ReservationBudgetDomainService(repository, _notificationHandler);

            var propertyMealPlanTypeAppService = Substitute.For<IPropertyMealPlanTypeAppService>();
            var reservationBudgetAdapter = Substitute.For<IReservationBudgetAdapter>();
            var propertyBaseRateReadRepository = Substitute.For<IPropertyBaseRateReadRepository>();
            var propertyParameterReadRepository = Substitute.For<IPropertyParameterReadRepository>();
            var ratePlanReadRepository = Substitute.For<IRatePlanReadRepository>();
            var reservationItemReadRepository = Substitute.For<IReservationItemReadRepository>();
            var reservationBudgetRepository = Substitute.For<IReservationBudgetRepository>();
            var reservationBudgetReadRepository = Substitute.For<IReservationBudgetReadRepository>();
            var reservationReadRepository = Substitute.For<IReservationReadRepository>();
            var localizationManager = Substitute.For<ILocalizationManager>();
            var roomTypeReadRepository = Substitute.For<IRoomTypeReadRepository>();
            var propertyRateStrategyReadRepository = Substitute.For<IPropertyRateStrategyReadRepository>();
            var availabilityAppService = Substitute.For<IAvailabilityAppService>();
            var roomTypeInventoryRepository = Substitute.For<IRoomTypeInventoryRepository>();
            var roomTypeDomainService = Substitute.For<IRoomTypeDomainService>();
            var roomReadRepository = Substitute.For<EntityFrameworkCore.ReadInterfaces.Repositories.IRoomReadRepository>();
            var roomBlockingReadRepository = Substitute.For<IRoomBlockingReadRepository>();
            var applicationUser = Substitute.For<IApplicationUser>();


            var currencyId = Guid.NewGuid();
            var propertyMealPlanTypeId = Guid.NewGuid();
            var mealPlanTypeId = 1;
            var baseRateAmount = 100;
            var mealPlanAmount = 10;

            propertyParameterReadRepository.GetChildrenAgeGroupList(1).ReturnsForAnyArgs(x =>
            {
                return new List<PropertyParameterDto>()
                {
                    new PropertyParameterDto
                    {
                        IsActive = true,
                        PropertyParameterValue = "1",
                    }
                };
            });

            reservationItemReadRepository.GetByIdNoTracking(1).ReturnsForAnyArgs(x =>
            {
                return null;
            });

            propertyMealPlanTypeAppService.GetAllPropertyMealPlanTypeByProperty(1).ReturnsForAnyArgs(x =>
            {
                return new ListDto<PropertyMealPlanTypeDto>
                {
                    Items = new List<PropertyMealPlanTypeDto>
                    {
                        new PropertyMealPlanTypeDto
                        {
                            Id = propertyMealPlanTypeId
                        }
                    }
                };
            });


            var availabilityRoomTypeColumnDto = new List<AvailabilityRoomTypeColumnDto>()
            {
                new AvailabilityRoomTypeColumnDto()
                {
                    RoomTypeId = 1,
                    Day = new DateTime(2019,01,04),
                    Date = "2019-01-04",
                    AvailableRooms = 10,
                    BlockedQuantity = 0,
                    Rooms = 10,
                    RoomTotal = 10,
                    Occupation = 0
                }
            };

            roomTypeDomainService.GetAvailabilityTotalsAndPercentageByDate(availabilityRoomTypeColumnDto, new DateTime(), new DateTime(), 10).ReturnsForAnyArgs(x =>
            {
                var totalList = new List<AvailabilityFooterColumnDto>()
                {
                    new AvailabilityFooterColumnDto()
                    {
                        Date = "2019-01-04",
                        Number = 10
                    }
                };

                var percentageList = new List<AvailabilityFooterColumnDto>()
                {
                    new AvailabilityFooterColumnDto()
                    {
                        Date = "2019-01-04",
                        Number = 55
                    }
                };

                return new KeyValuePair<List<AvailabilityFooterColumnDto>, List<AvailabilityFooterColumnDto>>(totalList, percentageList);
            });

            propertyBaseRateReadRepository.GetAllByPeriod(1, null, null, null, new DateTime(), new DateTime()).ReturnsForAnyArgs(x =>
            {
                //Tarifa Base
                return new List<PropertyBaseRate>
                {
                    new PropertyBaseRate
                    {
                        Id = Guid.NewGuid(),
                        AdultMealPlanAmount = mealPlanAmount,
                        Pax1Amount = baseRateAmount,
                        PropertyId = 1,
                        MealPlanTypeId = mealPlanTypeId,
                        RoomTypeId = 1,
                        RoomType = new RoomType
                        {
                            Abbreviation = "STD",
                            MinimumRate = 100,
                            MaximumRate = 500
                        },
                        CurrencyId = currencyId,
                        Currency = new Currency
                        {
                            Symbol = "R$"
                        }
                    }
                };
            }, x =>
            {
                //Tarifa Fixa
                return new List<PropertyBaseRate>();
            });

            ratePlanReadRepository.GetAllCompleteRatePlan(1, new DateTime(), new DateTime(), null, null).ReturnsForAnyArgs(x =>
            {
                return new List<RatePlanBaseJoinMap>
                {

                };
            });

            propertyRateStrategyReadRepository.GetAllWithRatePlanByPropertyIdAsync(1).ReturnsForAnyArgs(x =>
            {
                return new List<GetAllPropertyRateStrategyWithRatePlanDto>
                {

                };
            });

            reservationItemReadRepository.GetAvailabilityReservationsItemsByPeriodAndPropertyIdAsync(new DateTime(), new DateTime(), 1, false).ReturnsForAnyArgs(x =>
            {
                return new List<ReservationItem>
                {
                    new ReservationItem
                    {
                        CheckInDate = new DateTime(2018, 12, 1),
                        CheckOutDate = new DateTime(2018, 12, 3),
                        ReceivedRoomTypeId = 1,
                        RoomId = 1
                    }
                };
            });

            roomTypeReadRepository.GetAvailabilityRoomTypeRowAsync(1).ReturnsForAnyArgs(x =>
            {
                return new List<AvailabilityRoomTypeRowDto>
                {
                    new AvailabilityRoomTypeRowDto
                    {
                        Id = 1
                    }
                };
            });

            roomTypeReadRepository.GetAllRoomTypesByPropertyIdAsync(1).ReturnsForAnyArgs(x =>
            {
                return new ListDto<RoomTypeDto>
                {
                    Items = new List<RoomTypeDto>()
                    {
                        new RoomTypeDto
                        {
                            Id = 1
                        }
                    }
                };
            });

            roomTypeReadRepository.GetRoomTypeOverbookingData(new DateTime(), new DateTime(), 1, null).ReturnsForAnyArgs(x =>
            {
                return new List<RoomTypeOverbookingSqlDto>();
            });

            roomTypeReadRepository.GetRoomTypeOverbookinperDateData(new DateTime(), new DateTime(), 1, null).ReturnsForAnyArgs(x =>
            {
                return new List<RoomTypeOverbookingperDateSqlDto>();
            });

            var reservationItemBudgetRequestDto = new ReservationItemBudgetRequestDto()
            {
                InitialDate = new DateTime(2018, 12, 1),
                FinalDate = new DateTime(2018, 12, 3),
                RoomTypeId = 1,
                AdultCount = 1,
                CurrencyId = currencyId,
                MealPlanTypeId = mealPlanTypeId
            };

            var reservationBudgetAppService =
                new ReservationBudgetAppService(propertyMealPlanTypeAppService, reservationBudgetAdapter, reservationBudgetDomainService, propertyBaseRateReadRepository,
                propertyParameterReadRepository, ratePlanReadRepository, reservationItemReadRepository, reservationBudgetRepository, reservationBudgetReadRepository,
                reservationReadRepository, localizationManager, _notificationHandler, roomTypeReadRepository, propertyRateStrategyReadRepository, availabilityAppService,
                roomTypeDomainService, applicationUser, roomReadRepository, roomBlockingReadRepository, roomTypeInventoryRepository);

            var result = reservationBudgetAppService.GetWithBudgetOffer(reservationItemBudgetRequestDto, 1).Result;

            Assert.Equal(baseRateAmount, result.CommercialBudgetList[0].CommercialBudgetDayList[0].BaseRateAmount);
            Assert.Equal(mealPlanAmount, result.CommercialBudgetList[0].CommercialBudgetDayList[0].MealPlanAmount);
            Assert.Equal(110, result.CommercialBudgetList[0].CommercialBudgetDayList[0].Total);
        }

        [Fact]
        public void Should_GetWithBudgetOffer_When_RateStrategy_Is_NotNull()
        {
            var repository = Substitute.For<IRepository<ReservationBudget>>();
            var reservationBudgetDomainService = new ReservationBudgetDomainService(repository, _notificationHandler);

            var propertyMealPlanTypeAppService = Substitute.For<IPropertyMealPlanTypeAppService>();
            var reservationBudgetAdapter = Substitute.For<IReservationBudgetAdapter>();
            var propertyBaseRateReadRepository = Substitute.For<IPropertyBaseRateReadRepository>();
            var propertyParameterReadRepository = Substitute.For<IPropertyParameterReadRepository>();
            var ratePlanReadRepository = Substitute.For<IRatePlanReadRepository>();
            var reservationItemReadRepository = Substitute.For<IReservationItemReadRepository>();
            var reservationBudgetRepository = Substitute.For<IReservationBudgetRepository>();
            var reservationBudgetReadRepository = Substitute.For<IReservationBudgetReadRepository>();
            var reservationReadRepository = Substitute.For<IReservationReadRepository>();
            var localizationManager = Substitute.For<ILocalizationManager>();
            var roomTypeReadRepository = Substitute.For<IRoomTypeReadRepository>();
            var propertyRateStrategyReadRepository = Substitute.For<IPropertyRateStrategyReadRepository>();
            var roomTypeInventoryRepository = Substitute.For<IRoomTypeInventoryRepository>();
            var availabilityAppService = Substitute.For<IAvailabilityAppService>();
            var roomTypeDomainService = Substitute.For<IRoomTypeDomainService>();
            var roomReadRepository = Substitute.For<EntityFrameworkCore.ReadInterfaces.Repositories.IRoomReadRepository>();
            var roomBlockingReadRepository = Substitute.For<IRoomBlockingReadRepository>();
            var applicationUser = Substitute.For<IApplicationUser>();

            var currencyId = Guid.NewGuid();
            var propertyMealPlanTypeId = Guid.NewGuid();
            var mealPlanTypeId = 1;
            var baseRateAmount = 100;
            var mealPlanAmount = 10;

            propertyParameterReadRepository.GetChildrenAgeGroupList(1).ReturnsForAnyArgs(x =>
            {
                return new List<PropertyParameterDto>()
                {
                    new PropertyParameterDto
                    {
                        IsActive = true,
                        PropertyParameterValue = "1",
                    }
                };
            });

            reservationItemReadRepository.GetByIdNoTracking(1).ReturnsForAnyArgs(x =>
            {
                return null;
            });

            propertyMealPlanTypeAppService.GetAllPropertyMealPlanTypeByProperty(1).ReturnsForAnyArgs(x =>
            {
                return new ListDto<PropertyMealPlanTypeDto>
                {
                    Items = new List<PropertyMealPlanTypeDto>
                    {
                        new PropertyMealPlanTypeDto
                        {
                            Id = propertyMealPlanTypeId
                        }
                    }
                };
            });

            propertyBaseRateReadRepository.GetAllByPeriod(1, null, null, null, new DateTime(), new DateTime()).ReturnsForAnyArgs(x =>
            {
                //Tarifa Base
                return new List<PropertyBaseRate>
                {
                    new PropertyBaseRate
                    {
                        Id = Guid.NewGuid(),
                        AdultMealPlanAmount = mealPlanAmount,
                        Pax1Amount = baseRateAmount,
                        PropertyId = 1,
                        MealPlanTypeId = mealPlanTypeId,
                        RoomTypeId = 1,
                        RoomType = new RoomType
                        {
                            Abbreviation = "STD",
                            MinimumRate = 100,
                            MaximumRate = 500
                        },
                        CurrencyId = currencyId,
                        Currency = new Currency
                        {
                            Symbol = "R$"
                        }
                    }
                };
            }, x =>
            {
                //Tarifa Fixa
                return new List<PropertyBaseRate>();
            });

            ratePlanReadRepository.GetAllCompleteRatePlan(1, new DateTime(), new DateTime(), null, null).ReturnsForAnyArgs(x =>
            {
                return new List<RatePlanBaseJoinMap>
                {

                };
            });

            propertyRateStrategyReadRepository.GetAllWithRatePlanByPropertyIdAsync(1).ReturnsForAnyArgs(x =>
            {
                return new List<GetAllPropertyRateStrategyWithRatePlanDto>
                {
                     new GetAllPropertyRateStrategyWithRatePlanDto
                     {
                        Id = Guid.Parse("E1FAED3D-1C6D-430B-9D28-D79D455AD0AD"),
                        IsActive = true,
                        Name = "Estratégia 01",
                        StartDate =  new DateTime(2018,11,25),
                        EndDate =  new DateTime(2019,01,05),
                        RoomTypeList = new List<RoomTypeDto>()
                        {
                            new RoomTypeDto()
                            {
                                 PropertyId = 1,
                                 Order = 1,
                                 Name = "Standard",
                                 Abbreviation = "STD",
                                 AdultCapacity = 2,
                                 ChildCapacity = 1,
                                 FreeChildQuantity1 = 0,
                                 FreeChildQuantity2 = 0,
                                 FreeChildQuantity3 = 0,
                                 IsActive = true,
                                 MaximumRate = 500,
                                 MinimumRate = 100,
                                 DistributionCode = "CD",
                                 AgeChildren1 = null,
                                 AgeChildren2 = null,
                                 AgeChildren3 = null
                            }
                        },
                        PropertyRateStrategyRoomTypeList = new List<PropertyRateStrategyRoomTypeDto>()
                        {
                            new PropertyRateStrategyRoomTypeDto()
                            {
                                Id = Guid.Parse("3318567A-8A3E-4CD7-8154-A43F28AC4A3F"),
                                PropertyRateStrategyId = Guid.Parse("E1FAED3D-1C6D-430B-9D28-D79D455AD0AD"),
                                RoomTypeId = 1,
                                MinOccupationPercentual = (decimal)50.1,
                                MaxOccupationPercentual = (decimal)55.0,
                                IsDecreased = false,
                                PercentualAmount = (decimal)10.00,
                                IncrementAmount = null
                            }
                        }
                    }
                };
            });

            reservationItemReadRepository.GetAvailabilityReservationsItemsByPeriodAndPropertyIdAsync(new DateTime(), new DateTime(), 1, false).ReturnsForAnyArgs(x =>
            {
                return new List<ReservationItem>
                {
                    new ReservationItem
                    {
                        CheckInDate = new DateTime(2018, 12, 1),
                        CheckOutDate = new DateTime(2018, 12, 3),
                        ReceivedRoomTypeId = 1,
                        RoomId = 1
                    }
                };
            });

            roomTypeReadRepository.GetAvailabilityRoomTypeRowAsync(1).ReturnsForAnyArgs(x =>
            {
                return new List<AvailabilityRoomTypeRowDto>()
                {
                    new AvailabilityRoomTypeRowDto()
                    {
                        Id = 1,
                        Name = "Standard",
                        Total = 10,
                        Order = 1
                    }
                };
            });

            var reservationItemList = new List<ReservationItem>
            {
                new ReservationItem
                {
                    CheckInDate = new DateTime(2018, 12, 1),
                    CheckOutDate = new DateTime(2018, 12, 3),
                    ReceivedRoomTypeId = 1,
                    RoomId = 1
                }
            };

            var availabilityRoomTypeRowDtoList = new List<AvailabilityRoomTypeRowDto>()
            {
                new AvailabilityRoomTypeRowDto()
                {
                    Id = 1,
                    Name = "Standard",
                    Total = 10,
                    Order = 1
                }
            };

            roomTypeDomainService.GetAvailabilityRoomTypeColumns(reservationItemList, availabilityRoomTypeRowDtoList).ReturnsForAnyArgs(x =>
            {
                return new List<AvailabilityRoomTypeColumnDto>()
                {
                    new AvailabilityRoomTypeColumnDto()
                    {
                        RoomTypeId = 1,
                        Day = new DateTime(2019,01,04),
                        Date = "2019-01-04",
                        AvailableRooms = 10,
                        BlockedQuantity = 0,
                        Rooms = 10,
                        RoomTotal = 10,
                        Occupation = 0
                    }
                };
            });

            var availabilityRoomTypeColumnDto = new List<AvailabilityRoomTypeColumnDto>()
            {
                new AvailabilityRoomTypeColumnDto()
                {
                    RoomTypeId = 1,
                    Day = new DateTime(2019,01,04),
                    Date = "2019-01-04",
                    AvailableRooms = 10,
                    BlockedQuantity = 0,
                    Rooms = 10,
                    RoomTotal = 10,
                    Occupation = 0
                }
            };

            roomTypeDomainService.GetAvailabilityTotalsAndPercentageByDate(availabilityRoomTypeColumnDto, new DateTime(), new DateTime(), 10).ReturnsForAnyArgs(x =>
            {
                var totalList = new List<AvailabilityFooterColumnDto>()
                {
                    new AvailabilityFooterColumnDto()
                    {
                        Date = "2019-01-04",
                        Number = 10
                    }
                };

                var percentageList = new List<AvailabilityFooterColumnDto>()
                {
                    new AvailabilityFooterColumnDto()
                    {
                        Date = "2019-01-04",
                        Number = 55
                    }
                };

                return new KeyValuePair<List<AvailabilityFooterColumnDto>, List<AvailabilityFooterColumnDto>>(totalList, percentageList);
            });

            roomTypeReadRepository.GetAllRoomTypesByPropertyIdAsync(1).ReturnsForAnyArgs(x =>
            {
                return new ListDto<RoomTypeDto>
                {
                    Items = new List<RoomTypeDto>()
                    {
                        new RoomTypeDto
                        {
                            Id = 1
                        }
                    }
                };
            });

            roomTypeReadRepository.GetRoomTypeOverbookingData(new DateTime(), new DateTime(), 1, null).ReturnsForAnyArgs(x =>
            {
                return new List<RoomTypeOverbookingSqlDto>();
            });

            roomTypeReadRepository.GetRoomTypeOverbookinperDateData(new DateTime(), new DateTime(), 1, null).ReturnsForAnyArgs(x =>
            {
                return new List<RoomTypeOverbookingperDateSqlDto>();
            });

            var reservationItemBudgetRequestDto = new ReservationItemBudgetRequestDto()
            {
                InitialDate = new DateTime(2018, 12, 1),
                FinalDate = new DateTime(2018, 12, 3),
                RoomTypeId = 1,
                AdultCount = 1,
                CurrencyId = currencyId,
                MealPlanTypeId = mealPlanTypeId
            };

            var reservationBudgetAppService = new ReservationBudgetAppService(propertyMealPlanTypeAppService, reservationBudgetAdapter, reservationBudgetDomainService, propertyBaseRateReadRepository,
                propertyParameterReadRepository, ratePlanReadRepository, reservationItemReadRepository, reservationBudgetRepository, reservationBudgetReadRepository,
                reservationReadRepository, localizationManager, _notificationHandler, roomTypeReadRepository, propertyRateStrategyReadRepository, availabilityAppService,
                roomTypeDomainService, applicationUser, roomReadRepository, roomBlockingReadRepository, roomTypeInventoryRepository);

            var result = reservationBudgetAppService.GetWithBudgetOffer(reservationItemBudgetRequestDto, 1).Result;

            Assert.Equal(baseRateAmount, result.CommercialBudgetList[0].CommercialBudgetDayList[0].BaseRateAmount);
            Assert.Equal(mealPlanAmount, result.CommercialBudgetList[0].CommercialBudgetDayList[0].MealPlanAmount);
            Assert.Equal(110, result.CommercialBudgetList[0].CommercialBudgetDayList[0].Total);
        }

        [Fact]
        public void Should_GetWithBudgetOffer_When_RateStrategy_Increased_10_Percent_And_Have_Commercial_Agreement()
        {
            var repository = Substitute.For<IRepository<ReservationBudget>>();
            var reservationBudgetDomainService = new ReservationBudgetDomainService(repository, _notificationHandler);

            var propertyMealPlanTypeAppService = Substitute.For<IPropertyMealPlanTypeAppService>();
            var reservationBudgetAdapter = Substitute.For<IReservationBudgetAdapter>();
            var propertyBaseRateReadRepository = Substitute.For<IPropertyBaseRateReadRepository>();
            var propertyParameterReadRepository = Substitute.For<IPropertyParameterReadRepository>();
            var ratePlanReadRepository = Substitute.For<IRatePlanReadRepository>();
            var reservationItemReadRepository = Substitute.For<IReservationItemReadRepository>();
            var reservationBudgetRepository = Substitute.For<IReservationBudgetRepository>();
            var reservationBudgetReadRepository = Substitute.For<IReservationBudgetReadRepository>();
            var reservationReadRepository = Substitute.For<IReservationReadRepository>();
            var localizationManager = Substitute.For<ILocalizationManager>();
            var roomTypeReadRepository = Substitute.For<IRoomTypeReadRepository>();
            var propertyRateStrategyReadRepository = Substitute.For<IPropertyRateStrategyReadRepository>();
            var roomTypeInventoryRepository = Substitute.For<IRoomTypeInventoryRepository>();
            var availabilityAppService = Substitute.For<IAvailabilityAppService>();
            var roomTypeDomainService = Substitute.For<IRoomTypeDomainService>();
            var roomReadRepository = Substitute.For<EntityFrameworkCore.ReadInterfaces.Repositories.IRoomReadRepository>();
            var roomBlockingReadRepository = Substitute.For<IRoomBlockingReadRepository>();
            var applicationUser = Substitute.For<IApplicationUser>();

            var currencyId = Guid.Parse("67838ACB-9525-4AEB-B0A6-127C1B986C48");
            var propertyMealPlanTypeId = Guid.NewGuid();
            var mealPlanTypeId = 1;
            var baseRateAmount = 100;
            var mealPlanAmount = 10;
            var strategyIncrementAmount = (decimal)10.00;

            propertyParameterReadRepository.GetChildrenAgeGroupList(1).ReturnsForAnyArgs(x =>
            {
                return new List<PropertyParameterDto>()
                {
                    new PropertyParameterDto
                    {
                        IsActive = true,
                        PropertyParameterValue = "1",
                    }
                };
            });

            reservationItemReadRepository.GetByIdNoTracking(1).ReturnsForAnyArgs(x =>
            {
                return null;
            });

            propertyMealPlanTypeAppService.GetAllPropertyMealPlanTypeByProperty(1).ReturnsForAnyArgs(x =>
            {
                return new ListDto<PropertyMealPlanTypeDto>
                {
                    Items = new List<PropertyMealPlanTypeDto>
                    {
                        new PropertyMealPlanTypeDto
                        {
                            Id = propertyMealPlanTypeId
                        }
                    }
                };
            });

            propertyBaseRateReadRepository.GetAllByPeriod(1, null, null, null, new DateTime(), new DateTime()).ReturnsForAnyArgs(x =>
            {
                //Tarifa Base
                return new List<PropertyBaseRate>
                {
                    new PropertyBaseRate
                    {
                        Id = Guid.NewGuid(),
                        AdultMealPlanAmount = mealPlanAmount,
                        Pax1Amount = baseRateAmount,
                        PropertyId = 1,
                        MealPlanTypeId = mealPlanTypeId,
                        RoomTypeId = 1,
                        Date = new DateTime(2019, 01, 07),
                        RoomType = new RoomType
                        {
                            Abbreviation = "STD",
                            MinimumRate = 100,
                            MaximumRate = 500
                        },
                        CurrencyId = currencyId,
                        Currency = new Currency
                        {
                            Symbol = "R$"
                        }
                    }
                };
            }, x =>
            {
                //Tarifa Fixa
                return new List<PropertyBaseRate>();
            });

            ratePlanReadRepository.GetAllCompleteRatePlan(1, new DateTime(), new DateTime(), null, null).ReturnsForAnyArgs(x =>
            {
                return new List<RatePlanBaseJoinMap>
                {
                    new RatePlanBaseJoinMap
                    {
                        RatePlan = new RatePlan()
                        {
                            Id = Guid.Parse("AF5C29B3-6A92-4BD6-94C1-16B8F4212CFD"),
                            AgreementTypeId = 1,
                            AgreementName = "Acordo",
                            StartDate = new DateTime(2019,01,01),
                            EndDate = new DateTime(2019,01,16),
                            PropertyId = 1,
                            MealPlanTypeId = 1,
                            IsActive = true,
                            CurrencyId = Guid.Parse("67838ACB-9525-4AEB-B0A6-127C1B986C48"),
                            CurrencySymbol = "RS",
                            RateNet = false,
                            RateTypeId = 1,
                            MarketSegmentId = null
                        },
                        RatePlanRoomType = new RatePlanRoomType()
                        {
                            Id = Guid.Parse("FB491E2C-AF00-4844-8BE7-0AA6581F41C7"),
                            RatePlanId = Guid.Parse("AF5C29B3-6A92-4BD6-94C1-16B8F4212CFD"),
                            RoomTypeId = 1,
                            PercentualAmmount = 10,
                            IsDecreased = true
                        },
                        RatePlanCompanyClient = new RatePlanCompanyClient()
                        {
                            Id = Guid.Parse("43337CFE-5645-4994-8AA6-D47D6E440248"),
                            CompanyClientId = Guid.Parse("6A27E16A-F227-4256-A62F-96E7B39D0658"),
                            RatePlanId = Guid.Parse("AF5C29B3-6A92-4BD6-94C1-16B8F4212CFD")
                        },
                        RatePlanCommission = new RatePlanCommission()
                        {
                            Id = Guid.Parse("524DD225-D186-49EB-9B6F-730775B11AC7"),
                            RatePlanId = Guid.Parse("AF5C29B3-6A92-4BD6-94C1-16B8F4212CFD"),
                            BillingItemId = 1,
                            CommissionPercentualAmount = 1
                        },
                        CompanyClient = new CompanyClient()
                        {
                            Id = Guid.Parse("6A27E16A-F227-4256-A62F-96E7B39D0658"),
                            CompanyId = 1,
                            PersonId = Guid.Parse("EEAE6861-E4F9-498E-8066-1E77F5DB2305"),
                            PersonType = "N",
                            ShortName = "Totvs Short 22",
                            TradeName = "Totvs Short 22",
                            DateOfBirth = null,
                            CountrySubdivisionId = 1,
                            PropertyCompanyClientCategoryId = Guid.Parse("68F1E09D-6510-49E6-B53A-DFA7FBB9A063"),
                            IsActive = true,
                            IsAcquirer = true
                        },
                        Currency = new Currency()
                        {
                            Id = Guid.Parse("67838ACB-9525-4AEB-B0A6-127C1B986C48"),
                            TwoLetterIsoCode = null,
                            CurrencyName = "BRL",
                            AlphabeticCode = "BRL",
                            NumnericCode = "2",
                            MinorUnit = 2,
                            Symbol = "R$",
                            ExchangeRate = 40
                        },
                        RoomType = new RoomType()
                        {
                            Id = 1,
                            PropertyId = 1,
                            Order = 1,
                            Name = "Standard",
                            Abbreviation = "STD",
                            AdultCapacity = 2,
                            ChildCapacity = 1,
                            FreeChildQuantity1 = 0,
                            FreeChildQuantity2 = 0,
                            FreeChildQuantity3 = 0,
                            IsActive = true,
                            MaximumRate = 500,
                            MinimumRate = 100,
                            DistributionCode = "CD"
                        },
                        MealPlanType = new MealPlanType()
                        {
                            Id = 1,
                            MealPlanTypeCode = "SAP",
                            MealPlanTypeName = "None"
                        }
                    }
                };
            });

            propertyRateStrategyReadRepository.GetAllWithRatePlanByPropertyIdAsync(1).ReturnsForAnyArgs(x =>
            {
                return new List<GetAllPropertyRateStrategyWithRatePlanDto>
                {
                     new GetAllPropertyRateStrategyWithRatePlanDto
                     {
                        Id = Guid.Parse("E1FAED3D-1C6D-430B-9D28-D79D455AD0AD"),
                        IsActive = true,
                        Name = "Estratégia 01",
                        StartDate =  new DateTime(2018,11,25),
                        EndDate =  new DateTime(2019,01,10),
                        RoomTypeList = new List<RoomTypeDto>()
                        {
                            new RoomTypeDto()
                            {
                                 PropertyId = 1,
                                 Order = 1,
                                 Name = "Standard",
                                 Abbreviation = "STD",
                                 AdultCapacity = 2,
                                 ChildCapacity = 1,
                                 FreeChildQuantity1 = 0,
                                 FreeChildQuantity2 = 0,
                                 FreeChildQuantity3 = 0,
                                 IsActive = true,
                                 MaximumRate = 500,
                                 MinimumRate = 100,
                                 DistributionCode = "CD",
                                 AgeChildren1 = null,
                                 AgeChildren2 = null,
                                 AgeChildren3 = null
                            }
                        },
                        PropertyRateStrategyRoomTypeList = new List<PropertyRateStrategyRoomTypeDto>()
                        {
                            new PropertyRateStrategyRoomTypeDto()
                            {
                                Id = Guid.Parse("3318567A-8A3E-4CD7-8154-A43F28AC4A3F"),
                                PropertyRateStrategyId = Guid.Parse("E1FAED3D-1C6D-430B-9D28-D79D455AD0AD"),
                                RoomTypeId = 1,
                                MinOccupationPercentual = (decimal)50.1,
                                MaxOccupationPercentual = (decimal)55.0,
                                IsDecreased = false,
                                PercentualAmount = (decimal)10.00,
                                IncrementAmount = null
                            }
                        }
                    }
                };
            });

            reservationItemReadRepository.GetAvailabilityReservationsItemsByPeriodAndPropertyIdAsync(new DateTime(), new DateTime(), 1, false).ReturnsForAnyArgs(x =>
            {
                return new List<ReservationItem>
                {
                    new ReservationItem
                    {
                        CheckInDate = new DateTime(2019, 01, 07),
                        CheckOutDate = new DateTime(2019, 01, 07),
                        ReceivedRoomTypeId = 1,
                        RoomId = 1
                    }
                };
            });

            roomTypeReadRepository.GetAvailabilityRoomTypeRowAsync(1).ReturnsForAnyArgs(x =>
            {
                return new List<AvailabilityRoomTypeRowDto>()
                {
                    new AvailabilityRoomTypeRowDto()
                    {
                        Id = 1,
                        Name = "Standard",
                        Total = 10,
                        Order = 1
                    }
                };
            });

            var reservationItemList = new List<ReservationItem>
            {
                new ReservationItem
                {
                    CheckInDate = new DateTime(2019, 01, 07),
                    CheckOutDate = new DateTime(2019, 01, 07),
                    ReceivedRoomTypeId = 1,
                    RoomId = 1
                }
            };

            var availabilityRoomTypeRowDtoList = new List<AvailabilityRoomTypeRowDto>()
            {
                new AvailabilityRoomTypeRowDto()
                {
                    Id = 1,
                    Name = "Standard",
                    Total = 10,
                    Order = 1
                }
            };

            roomTypeDomainService.GetAvailabilityRoomTypeColumns(reservationItemList, availabilityRoomTypeRowDtoList).ReturnsForAnyArgs(x =>
            {
                return new List<AvailabilityRoomTypeColumnDto>()
                {
                    new AvailabilityRoomTypeColumnDto()
                    {
                        RoomTypeId = 1,
                        Day = new DateTime(2019,01,07),
                        Date = "2019-01-07",
                        AvailableRooms = 10,
                        BlockedQuantity = 0,
                        Rooms = 10,
                        RoomTotal = 10,
                        Occupation = 0
                    }
                };
            });

            var availabilityRoomTypeColumnDto = new List<AvailabilityRoomTypeColumnDto>()
            {
                new AvailabilityRoomTypeColumnDto()
                {
                    RoomTypeId = 1,
                    Day = new DateTime(2019,01,07),
                    Date = "2019-01-07",
                    AvailableRooms = 10,
                    BlockedQuantity = 0,
                    Rooms = 10,
                    RoomTotal = 10,
                    Occupation = 0
                }
            };

            roomTypeDomainService.GetAvailabilityTotalsAndPercentageByDate(availabilityRoomTypeColumnDto, new DateTime(), new DateTime(), 10).ReturnsForAnyArgs(x =>
            {
                var totalList = new List<AvailabilityFooterColumnDto>()
                {
                    new AvailabilityFooterColumnDto()
                    {
                        Date = "2019-01-07",
                        Number = 10
                    }
                };

                var percentageList = new List<AvailabilityFooterColumnDto>()
                {
                    new AvailabilityFooterColumnDto()
                    {
                        Date = "2019-01-07",
                        Number = 55
                    }
                };

                return new KeyValuePair<List<AvailabilityFooterColumnDto>, List<AvailabilityFooterColumnDto>>(totalList, percentageList);
            });

            roomTypeReadRepository.GetAllRoomTypesByPropertyIdAsync(1).ReturnsForAnyArgs(x =>
            {
                return new ListDto<RoomTypeDto>
                {
                    Items = new List<RoomTypeDto>()
                    {
                        new RoomTypeDto
                        {
                            Id = 1
                        }
                    }
                };
            });

            roomTypeReadRepository.GetRoomTypeOverbookingData(new DateTime(), new DateTime(), 1, null).ReturnsForAnyArgs(x =>
            {
                return new List<RoomTypeOverbookingSqlDto>();
            });

            roomTypeReadRepository.GetRoomTypeOverbookinperDateData(new DateTime(), new DateTime(), 1, null).ReturnsForAnyArgs(x =>
            {
                return new List<RoomTypeOverbookingperDateSqlDto>();
            });

            var reservationItemBudgetRequestDto = new ReservationItemBudgetRequestDto()
            {
                InitialDate = new DateTime(2019, 01, 07),
                FinalDate = new DateTime(2019, 01, 07),
                RoomTypeId = 1,
                AdultCount = 1,
                CurrencyId = currencyId,
                MealPlanTypeId = mealPlanTypeId
            };

            var reservationBudgetAppService = new ReservationBudgetAppService(propertyMealPlanTypeAppService, reservationBudgetAdapter, reservationBudgetDomainService, propertyBaseRateReadRepository,
                propertyParameterReadRepository, ratePlanReadRepository, reservationItemReadRepository, reservationBudgetRepository, reservationBudgetReadRepository,
                reservationReadRepository, localizationManager, _notificationHandler, roomTypeReadRepository, propertyRateStrategyReadRepository, availabilityAppService,
                roomTypeDomainService, applicationUser, roomReadRepository, roomBlockingReadRepository, roomTypeInventoryRepository);

            var result = reservationBudgetAppService.GetWithBudgetOffer(reservationItemBudgetRequestDto, 1).Result;

            Assert.Equal(baseRateAmount + strategyIncrementAmount, result.CommercialBudgetList[0].CommercialBudgetDayList[0].BaseRateAmount);
            Assert.Equal(mealPlanAmount, result.CommercialBudgetList[0].CommercialBudgetDayList[0].MealPlanAmount);
            Assert.Equal(baseRateAmount + strategyIncrementAmount + mealPlanAmount, result.CommercialBudgetList[0].CommercialBudgetDayList[0].Total);
            Assert.Equal(108, result.CommercialBudgetList[1].CommercialBudgetDayList[0].Total);
        }

        [Fact]
        public void Should_GetWithBudgetOffer_When_RateStrategy_Decreased_10_Value_And_Have_Commercial_Agreement()
        {
            var repository = Substitute.For<IRepository<ReservationBudget>>();
            var reservationBudgetDomainService = new ReservationBudgetDomainService(repository, _notificationHandler);

            var propertyMealPlanTypeAppService = Substitute.For<IPropertyMealPlanTypeAppService>();
            var reservationBudgetAdapter = Substitute.For<IReservationBudgetAdapter>();
            var propertyBaseRateReadRepository = Substitute.For<IPropertyBaseRateReadRepository>();
            var propertyParameterReadRepository = Substitute.For<IPropertyParameterReadRepository>();
            var ratePlanReadRepository = Substitute.For<IRatePlanReadRepository>();
            var reservationItemReadRepository = Substitute.For<IReservationItemReadRepository>();
            var reservationBudgetRepository = Substitute.For<IReservationBudgetRepository>();
            var reservationBudgetReadRepository = Substitute.For<IReservationBudgetReadRepository>();
            var reservationReadRepository = Substitute.For<IReservationReadRepository>();
            var localizationManager = Substitute.For<ILocalizationManager>();
            var roomTypeReadRepository = Substitute.For<IRoomTypeReadRepository>();
            var propertyRateStrategyReadRepository = Substitute.For<IPropertyRateStrategyReadRepository>();
            var roomTypeInventoryRepository = Substitute.For<IRoomTypeInventoryRepository>();
            var availabilityAppService = Substitute.For<IAvailabilityAppService>();
            var roomTypeDomainService = Substitute.For<IRoomTypeDomainService>();
            var roomReadRepository = Substitute.For<EntityFrameworkCore.ReadInterfaces.Repositories.IRoomReadRepository>();
            var roomBlockingReadRepository = Substitute.For<IRoomBlockingReadRepository>();
            var applicationUser = Substitute.For<IApplicationUser>();

            var currencyId = Guid.Parse("67838ACB-9525-4AEB-B0A6-127C1B986C48");
            var propertyMealPlanTypeId = Guid.NewGuid();
            var mealPlanTypeId = 1;
            var baseRateAmount = 200;
            var mealPlanAmount = 10;
            var strategyIncrementAmount = (decimal)10.00;

            propertyParameterReadRepository.GetChildrenAgeGroupList(1).ReturnsForAnyArgs(x =>
            {
                return new List<PropertyParameterDto>()
                {
                    new PropertyParameterDto
                    {
                        IsActive = true,
                        PropertyParameterValue = "1",
                    }
                };
            });

            reservationItemReadRepository.GetByIdNoTracking(1).ReturnsForAnyArgs(x =>
            {
                return null;
            });

            propertyMealPlanTypeAppService.GetAllPropertyMealPlanTypeByProperty(1).ReturnsForAnyArgs(x =>
            {
                return new ListDto<PropertyMealPlanTypeDto>
                {
                    Items = new List<PropertyMealPlanTypeDto>
                    {
                        new PropertyMealPlanTypeDto
                        {
                            Id = propertyMealPlanTypeId
                        }
                    }
                };
            });

            propertyBaseRateReadRepository.GetAllByPeriod(1, null, null, null, new DateTime(), new DateTime()).ReturnsForAnyArgs(x =>
            {
                //Tarifa Base
                return new List<PropertyBaseRate>
                {
                    new PropertyBaseRate
                    {
                        Id = Guid.NewGuid(),
                        AdultMealPlanAmount = mealPlanAmount,
                        Pax1Amount = baseRateAmount,
                        PropertyId = 1,
                        MealPlanTypeId = mealPlanTypeId,
                        RoomTypeId = 1,
                        Date = new DateTime(2019, 01, 07),
                        RoomType = new RoomType
                        {
                            Abbreviation = "STD",
                            MinimumRate = 100,
                            MaximumRate = 500
                        },
                        CurrencyId = currencyId,
                        Currency = new Currency
                        {
                            Symbol = "R$"
                        }
                    }
                };
            }, x =>
            {
                //Tarifa Fixa
                return new List<PropertyBaseRate>();
            });

            ratePlanReadRepository.GetAllCompleteRatePlan(1, new DateTime(), new DateTime(), null, null).ReturnsForAnyArgs(x =>
            {
                return new List<RatePlanBaseJoinMap>
                {
                    new RatePlanBaseJoinMap
                    {
                        RatePlan = new RatePlan()
                        {
                            Id = Guid.Parse("AF5C29B3-6A92-4BD6-94C1-16B8F4212CFD"),
                            AgreementTypeId = 1,
                            AgreementName = "Acordo",
                            StartDate = new DateTime(2019,01,01),
                            EndDate = new DateTime(2019,01,16),
                            PropertyId = 1,
                            MealPlanTypeId = 1,
                            IsActive = true,
                            CurrencyId = Guid.Parse("67838ACB-9525-4AEB-B0A6-127C1B986C48"),
                            CurrencySymbol = "RS",
                            RateNet = false,
                            RateTypeId = 1,
                            MarketSegmentId = null
                        },
                        RatePlanRoomType = new RatePlanRoomType()
                        {
                            Id = Guid.Parse("FB491E2C-AF00-4844-8BE7-0AA6581F41C7"),
                            RatePlanId = Guid.Parse("AF5C29B3-6A92-4BD6-94C1-16B8F4212CFD"),
                            RoomTypeId = 1,
                            PercentualAmmount = 10,
                            IsDecreased = true
                        },
                        RatePlanCompanyClient = new RatePlanCompanyClient()
                        {
                            Id = Guid.Parse("43337CFE-5645-4994-8AA6-D47D6E440248"),
                            CompanyClientId = Guid.Parse("6A27E16A-F227-4256-A62F-96E7B39D0658"),
                            RatePlanId = Guid.Parse("AF5C29B3-6A92-4BD6-94C1-16B8F4212CFD")
                        },
                        RatePlanCommission = new RatePlanCommission()
                        {
                            Id = Guid.Parse("524DD225-D186-49EB-9B6F-730775B11AC7"),
                            RatePlanId = Guid.Parse("AF5C29B3-6A92-4BD6-94C1-16B8F4212CFD"),
                            BillingItemId = 1,
                            CommissionPercentualAmount = 1
                        },
                        CompanyClient = new CompanyClient()
                        {
                            Id = Guid.Parse("6A27E16A-F227-4256-A62F-96E7B39D0658"),
                            CompanyId = 1,
                            PersonId = Guid.Parse("EEAE6861-E4F9-498E-8066-1E77F5DB2305"),
                            PersonType = "N",
                            ShortName = "Totvs Short 22",
                            TradeName = "Totvs Short 22",
                            DateOfBirth = null,
                            CountrySubdivisionId = 1,
                            PropertyCompanyClientCategoryId = Guid.Parse("68F1E09D-6510-49E6-B53A-DFA7FBB9A063"),
                            IsActive = true,
                            IsAcquirer = true
                        },
                        Currency = new Currency()
                        {
                            Id = Guid.Parse("67838ACB-9525-4AEB-B0A6-127C1B986C48"),
                            TwoLetterIsoCode = null,
                            CurrencyName = "BRL",
                            AlphabeticCode = "BRL",
                            NumnericCode = "2",
                            MinorUnit = 2,
                            Symbol = "R$",
                            ExchangeRate = 40
                        },
                        RoomType = new RoomType()
                        {
                            Id = 1,
                            PropertyId = 1,
                            Order = 1,
                            Name = "Standard",
                            Abbreviation = "STD",
                            AdultCapacity = 2,
                            ChildCapacity = 1,
                            FreeChildQuantity1 = 0,
                            FreeChildQuantity2 = 0,
                            FreeChildQuantity3 = 0,
                            IsActive = true,
                            MaximumRate = 500,
                            MinimumRate = 100,
                            DistributionCode = "CD"
                        },
                        MealPlanType = new MealPlanType()
                        {
                            Id = 1,
                            MealPlanTypeCode = "SAP",
                            MealPlanTypeName = "None"
                        }
                    }
                };
            });

            propertyRateStrategyReadRepository.GetAllWithRatePlanByPropertyIdAsync(1).ReturnsForAnyArgs(x =>
            {
                return new List<GetAllPropertyRateStrategyWithRatePlanDto>
                {
                     new GetAllPropertyRateStrategyWithRatePlanDto
                     {
                        Id = Guid.Parse("E1FAED3D-1C6D-430B-9D28-D79D455AD0AD"),
                        IsActive = true,
                        Name = "Estratégia 01",
                        StartDate =  new DateTime(2018,11,25),
                        EndDate =  new DateTime(2019,01,10),
                        RoomTypeList = new List<RoomTypeDto>()
                        {
                            new RoomTypeDto()
                            {
                                 PropertyId = 1,
                                 Order = 1,
                                 Name = "Standard",
                                 Abbreviation = "STD",
                                 AdultCapacity = 2,
                                 ChildCapacity = 1,
                                 FreeChildQuantity1 = 0,
                                 FreeChildQuantity2 = 0,
                                 FreeChildQuantity3 = 0,
                                 IsActive = true,
                                 MaximumRate = 500,
                                 MinimumRate = 100,
                                 DistributionCode = "CD",
                                 AgeChildren1 = null,
                                 AgeChildren2 = null,
                                 AgeChildren3 = null
                            }
                        },
                        PropertyRateStrategyRoomTypeList = new List<PropertyRateStrategyRoomTypeDto>()
                        {
                            new PropertyRateStrategyRoomTypeDto()
                            {
                                Id = Guid.Parse("3318567A-8A3E-4CD7-8154-A43F28AC4A3F"),
                                PropertyRateStrategyId = Guid.Parse("E1FAED3D-1C6D-430B-9D28-D79D455AD0AD"),
                                RoomTypeId = 1,
                                MinOccupationPercentual = (decimal)50.1,
                                MaxOccupationPercentual = (decimal)55.0,
                                IsDecreased = true,
                                PercentualAmount = null,
                                IncrementAmount = strategyIncrementAmount
                            }
                        }
                    }
                };
            });

            reservationItemReadRepository.GetAvailabilityReservationsItemsByPeriodAndPropertyIdAsync(new DateTime(), new DateTime(), 1, false).ReturnsForAnyArgs(x =>
            {
                return new List<ReservationItem>
                {
                    new ReservationItem
                    {
                        CheckInDate = new DateTime(2019, 01, 07),
                        CheckOutDate = new DateTime(2019, 01, 07),
                        ReceivedRoomTypeId = 1,
                        RoomId = 1
                    }
                };
            });

            roomTypeReadRepository.GetAvailabilityRoomTypeRowAsync(1).ReturnsForAnyArgs(x =>
            {
                return new List<AvailabilityRoomTypeRowDto>()
                {
                    new AvailabilityRoomTypeRowDto()
                    {
                        Id = 1,
                        Name = "Standard",
                        Total = 10,
                        Order = 1
                    }
                };
            });

            var reservationItemList = new List<ReservationItem>
            {
                new ReservationItem
                {
                    CheckInDate = new DateTime(2019, 01, 07),
                    CheckOutDate = new DateTime(2019, 01, 07),
                    ReceivedRoomTypeId = 1,
                    RoomId = 1
                }
            };

            var availabilityRoomTypeRowDtoList = new List<AvailabilityRoomTypeRowDto>()
            {
                new AvailabilityRoomTypeRowDto()
                {
                    Id = 1,
                    Name = "Standard",
                    Total = 10,
                    Order = 1
                }
            };

            roomTypeDomainService.GetAvailabilityRoomTypeColumns(reservationItemList, availabilityRoomTypeRowDtoList).ReturnsForAnyArgs(x =>
            {
                return new List<AvailabilityRoomTypeColumnDto>()
                {
                    new AvailabilityRoomTypeColumnDto()
                    {
                        RoomTypeId = 1,
                        Day = new DateTime(2019,01,07),
                        Date = "2019-01-07",
                        AvailableRooms = 10,
                        BlockedQuantity = 0,
                        Rooms = 10,
                        RoomTotal = 10,
                        Occupation = 0
                    }
                };
            });

            var availabilityRoomTypeColumnDto = new List<AvailabilityRoomTypeColumnDto>()
            {
                new AvailabilityRoomTypeColumnDto()
                {
                    RoomTypeId = 1,
                    Day = new DateTime(2019,01,07),
                    Date = "2019-01-07",
                    AvailableRooms = 10,
                    BlockedQuantity = 0,
                    Rooms = 10,
                    RoomTotal = 10,
                    Occupation = 0
                }
            };

            roomTypeDomainService.GetAvailabilityTotalsAndPercentageByDate(availabilityRoomTypeColumnDto, new DateTime(), new DateTime(), 10).ReturnsForAnyArgs(x =>
            {
                var totalList = new List<AvailabilityFooterColumnDto>()
                {
                    new AvailabilityFooterColumnDto()
                    {
                        Date = "2019-01-07",
                        Number = 10
                    }
                };

                var percentageList = new List<AvailabilityFooterColumnDto>()
                {
                    new AvailabilityFooterColumnDto()
                    {
                        Date = "2019-01-07",
                        Number = 55
                    }
                };

                return new KeyValuePair<List<AvailabilityFooterColumnDto>, List<AvailabilityFooterColumnDto>>(totalList, percentageList);
            });

            roomTypeReadRepository.GetAllRoomTypesByPropertyIdAsync(1).ReturnsForAnyArgs(x =>
            {
                return new ListDto<RoomTypeDto>
                {
                    Items = new List<RoomTypeDto>()
                    {
                        new RoomTypeDto
                        {
                            Id = 1
                        }
                    }
                };
            });

            roomTypeReadRepository.GetRoomTypeOverbookingData(new DateTime(), new DateTime(), 1, null).ReturnsForAnyArgs(x =>
            {
                return new List<RoomTypeOverbookingSqlDto>();
            });

            roomTypeReadRepository.GetRoomTypeOverbookinperDateData(new DateTime(), new DateTime(), 1, null).ReturnsForAnyArgs(x =>
            {
                return new List<RoomTypeOverbookingperDateSqlDto>();
            });

            var reservationItemBudgetRequestDto = new ReservationItemBudgetRequestDto()
            {
                InitialDate = new DateTime(2019, 01, 07),
                FinalDate = new DateTime(2019, 01, 07),
                RoomTypeId = 1,
                AdultCount = 1,
                CurrencyId = currencyId,
                MealPlanTypeId = mealPlanTypeId
            };

            var reservationBudgetAppService = new ReservationBudgetAppService(propertyMealPlanTypeAppService, reservationBudgetAdapter, reservationBudgetDomainService, propertyBaseRateReadRepository,
                propertyParameterReadRepository, ratePlanReadRepository, reservationItemReadRepository, reservationBudgetRepository, reservationBudgetReadRepository,
                reservationReadRepository, localizationManager, _notificationHandler, roomTypeReadRepository, propertyRateStrategyReadRepository, availabilityAppService,
                roomTypeDomainService, applicationUser, roomReadRepository, roomBlockingReadRepository, roomTypeInventoryRepository);

            var result = reservationBudgetAppService.GetWithBudgetOffer(reservationItemBudgetRequestDto, 1).Result;

            Assert.Equal(baseRateAmount - strategyIncrementAmount, result.CommercialBudgetList[0].CommercialBudgetDayList[0].BaseRateAmount);
            Assert.Equal(mealPlanAmount, result.CommercialBudgetList[0].CommercialBudgetDayList[0].MealPlanAmount);
            Assert.Equal(200, result.CommercialBudgetList[0].CommercialBudgetDayList[0].Total);
            Assert.Equal(180, result.CommercialBudgetList[1].CommercialBudgetDayList[0].Total);
        }

        [Fact]
        public void Should_GetWithBudgetOffer_When_RateStrategy_Is_Not_Null_Value_And_Have_Commercial_Agreement_With_Flat_Rate()
        {
            var repository = Substitute.For<IRepository<ReservationBudget>>();
            var reservationBudgetDomainService = new ReservationBudgetDomainService(repository, _notificationHandler);

            var propertyMealPlanTypeAppService = Substitute.For<IPropertyMealPlanTypeAppService>();
            var reservationBudgetAdapter = Substitute.For<IReservationBudgetAdapter>();
            var propertyBaseRateReadRepository = Substitute.For<IPropertyBaseRateReadRepository>();
            var propertyParameterReadRepository = Substitute.For<IPropertyParameterReadRepository>();
            var ratePlanReadRepository = Substitute.For<IRatePlanReadRepository>();
            var reservationItemReadRepository = Substitute.For<IReservationItemReadRepository>();
            var reservationBudgetRepository = Substitute.For<IReservationBudgetRepository>();
            var reservationBudgetReadRepository = Substitute.For<IReservationBudgetReadRepository>();
            var reservationReadRepository = Substitute.For<IReservationReadRepository>();
            var localizationManager = Substitute.For<ILocalizationManager>();
            var roomTypeReadRepository = Substitute.For<IRoomTypeReadRepository>();
            var propertyRateStrategyReadRepository = Substitute.For<IPropertyRateStrategyReadRepository>();
            var roomTypeInventoryRepository = Substitute.For<IRoomTypeInventoryRepository>();
            var availabilityAppService = Substitute.For<IAvailabilityAppService>();
            var roomTypeDomainService = Substitute.For<IRoomTypeDomainService>();
            var roomReadRepository = Substitute.For<EntityFrameworkCore.ReadInterfaces.Repositories.IRoomReadRepository>();
            var roomBlockingReadRepository = Substitute.For<IRoomBlockingReadRepository>();
            var applicationUser = Substitute.For<IApplicationUser>();

            var currencyId = Guid.NewGuid();
            var propertyMealPlanTypeId = Guid.NewGuid();
            var mealPlanTypeId = 1;
            var baseRateAmount = 200;
            var mealPlanAmount = 10;
            var mealPlanTypeDefaultId = 1;
            var strategyIncrementAmount = (decimal)10.00;

            propertyParameterReadRepository.GetChildrenAgeGroupList(1).ReturnsForAnyArgs(x =>
            {
                return new List<PropertyParameterDto>()
                {
                    new PropertyParameterDto
                    {
                        IsActive = true,
                        PropertyParameterValue = "1",
                    }
                };
            });

            reservationItemReadRepository.GetByIdNoTracking(1).ReturnsForAnyArgs(x =>
            {
                return null;
            });

            propertyMealPlanTypeAppService.GetAllPropertyMealPlanTypeByProperty(1).ReturnsForAnyArgs(x =>
            {
                return new ListDto<PropertyMealPlanTypeDto>
                {
                    Items = new List<PropertyMealPlanTypeDto>
                    {
                        new PropertyMealPlanTypeDto
                        {
                            PropertyMealPlanTypeCode = "SAP",
                            PropertyMealPlanTypeName = "Sem Pensão",
                            PropertyId = 1,
                            MealPlanTypeId = 1,
                            IsActive = true
                        }
                    }
                };
            });

            propertyBaseRateReadRepository.GetAllByPeriod(1, null, null, null, new DateTime(), new DateTime()).ReturnsForAnyArgs(x =>
            {
                //Tarifa Base
                return new List<PropertyBaseRate>
                {
                    new PropertyBaseRate
                    {
                        Id = Guid.NewGuid(),
                        AdultMealPlanAmount = mealPlanAmount,
                        Pax1Amount = baseRateAmount,
                        PropertyId = 1,
                        MealPlanTypeId = mealPlanTypeId,
                        MealPlanTypeDefault = mealPlanTypeDefaultId,
                        RoomTypeId = 1,
                        Date = new DateTime(2019, 01, 07),
                        RoomType = new RoomType
                        {
                            Abbreviation = "STD",
                            MinimumRate = 100,
                            MaximumRate = 500
                        },
                        CurrencyId = currencyId,
                        Currency = new Currency
                        {
                            Symbol = "R$"
                        }
                    }
                };
            }, x =>
            {
                //Tarifa Fixa
                return new List<PropertyBaseRate>
                {
                    new PropertyBaseRate
                    {
                        Id = Guid.NewGuid(),
                        RatePlanId = Guid.Parse("AF5C29B3-6A92-4BD6-94C1-16B8F4212CFD"),
                        AdultMealPlanAmount = mealPlanAmount,
                        Pax1Amount = baseRateAmount,
                        PropertyId = 1,
                        MealPlanTypeId = mealPlanTypeId,
                        MealPlanTypeDefault = mealPlanTypeDefaultId,
                        RoomTypeId = 1,
                        Date = new DateTime(2019, 01, 07),
                        RoomType = new RoomType
                        {
                            Abbreviation = "STD",
                            MinimumRate = 100,
                            MaximumRate = 500
                        },
                        CurrencyId = currencyId,
                        Currency = new Currency
                        {
                            Symbol = "R$"
                        }
                    }
                };
            });

            ratePlanReadRepository.GetAllCompleteRatePlan(1, new DateTime(), new DateTime(), null, null).ReturnsForAnyArgs(x =>
            {
                return new List<RatePlanBaseJoinMap>
                {
                    new RatePlanBaseJoinMap
                    {
                        RatePlan = new RatePlan()
                        {
                            Id = Guid.Parse("AF5C29B3-6A92-4BD6-94C1-16B8F4212CFD"),
                            AgreementTypeId = 1,
                            AgreementName = "Acordo",
                            StartDate = new DateTime(2019,01,01),
                            EndDate = new DateTime(2019,01,16),
                            PropertyId = 1,
                            MealPlanTypeId = 1,
                            IsActive = true,
                            CurrencyId = Guid.Parse("67838ACB-9525-4AEB-B0A6-127C1B986C48"),
                            CurrencySymbol = "RS",
                            RateNet = false,
                            RateTypeId = 2,
                            MarketSegmentId = null
                        },
                        RatePlanRoomType = new RatePlanRoomType()
                        {
                            Id = Guid.Parse("FB491E2C-AF00-4844-8BE7-0AA6581F41C7"),
                            RatePlanId = Guid.Parse("AF5C29B3-6A92-4BD6-94C1-16B8F4212CFD"),
                            RoomTypeId = 1,
                            PercentualAmmount = 10,
                            IsDecreased = true
                        },
                        RatePlanCompanyClient = new RatePlanCompanyClient()
                        {
                            Id = Guid.Parse("43337CFE-5645-4994-8AA6-D47D6E440248"),
                            CompanyClientId = Guid.Parse("6A27E16A-F227-4256-A62F-96E7B39D0658"),
                            RatePlanId = Guid.Parse("AF5C29B3-6A92-4BD6-94C1-16B8F4212CFD")
                        },
                        RatePlanCommission = new RatePlanCommission()
                        {
                            Id = Guid.Parse("524DD225-D186-49EB-9B6F-730775B11AC7"),
                            RatePlanId = Guid.Parse("AF5C29B3-6A92-4BD6-94C1-16B8F4212CFD"),
                            BillingItemId = 1,
                            CommissionPercentualAmount = 1
                        },
                        CompanyClient = new CompanyClient()
                        {
                            Id = Guid.Parse("6A27E16A-F227-4256-A62F-96E7B39D0658"),
                            CompanyId = 1,
                            PersonId = Guid.Parse("EEAE6861-E4F9-498E-8066-1E77F5DB2305"),
                            PersonType = "N",
                            ShortName = "Totvs Short 22",
                            TradeName = "Totvs Short 22",
                            DateOfBirth = null,
                            CountrySubdivisionId = 1,
                            PropertyCompanyClientCategoryId = Guid.Parse("68F1E09D-6510-49E6-B53A-DFA7FBB9A063"),
                            IsActive = true,
                            IsAcquirer = true
                        },
                        Currency = new Currency()
                        {
                            Id = Guid.Parse("67838ACB-9525-4AEB-B0A6-127C1B986C48"),
                            TwoLetterIsoCode = null,
                            CurrencyName = "BRL",
                            AlphabeticCode = "BRL",
                            NumnericCode = "2",
                            MinorUnit = 2,
                            Symbol = "R$",
                            ExchangeRate = 40
                        },
                        RoomType = new RoomType()
                        {
                            Id = 1,
                            PropertyId = 1,
                            Order = 1,
                            Name = "Standard",
                            Abbreviation = "STD",
                            AdultCapacity = 2,
                            ChildCapacity = 1,
                            FreeChildQuantity1 = 0,
                            FreeChildQuantity2 = 0,
                            FreeChildQuantity3 = 0,
                            IsActive = true,
                            MaximumRate = 500,
                            MinimumRate = 100,
                            DistributionCode = "CD"
                        },
                        MealPlanType = new MealPlanType()
                        {
                            Id = 1,
                            MealPlanTypeCode = "SAP",
                            MealPlanTypeName = "None"
                        }
                    }
                };
            });

            propertyRateStrategyReadRepository.GetAllWithRatePlanByPropertyIdAsync(1).ReturnsForAnyArgs(x =>
            {
                return new List<GetAllPropertyRateStrategyWithRatePlanDto>
                {
                     new GetAllPropertyRateStrategyWithRatePlanDto
                     {
                        Id = Guid.Parse("E1FAED3D-1C6D-430B-9D28-D79D455AD0AD"),
                        IsActive = true,
                        Name = "Estratégia 01",
                        StartDate =  new DateTime(2018,11,25),
                        EndDate =  new DateTime(2019,01,10),
                        RoomTypeList = new List<RoomTypeDto>()
                        {
                            new RoomTypeDto()
                            {
                                 PropertyId = 1,
                                 Order = 1,
                                 Name = "Standard",
                                 Abbreviation = "STD",
                                 AdultCapacity = 2,
                                 ChildCapacity = 1,
                                 FreeChildQuantity1 = 0,
                                 FreeChildQuantity2 = 0,
                                 FreeChildQuantity3 = 0,
                                 IsActive = true,
                                 MaximumRate = 500,
                                 MinimumRate = 100,
                                 DistributionCode = "CD",
                                 AgeChildren1 = null,
                                 AgeChildren2 = null,
                                 AgeChildren3 = null
                            }
                        },
                        PropertyRateStrategyRoomTypeList = new List<PropertyRateStrategyRoomTypeDto>()
                        {
                            new PropertyRateStrategyRoomTypeDto()
                            {
                                Id = Guid.Parse("3318567A-8A3E-4CD7-8154-A43F28AC4A3F"),
                                PropertyRateStrategyId = Guid.Parse("E1FAED3D-1C6D-430B-9D28-D79D455AD0AD"),
                                RoomTypeId = 1,
                                MinOccupationPercentual = (decimal)50.1,
                                MaxOccupationPercentual = (decimal)55.0,
                                IsDecreased = true,
                                PercentualAmount = null,
                                IncrementAmount = (decimal)10.00
                            }
                        }
                    }
                };
            });

            reservationItemReadRepository.GetAvailabilityReservationsItemsByPeriodAndPropertyIdAsync(new DateTime(), new DateTime(), 1, false).ReturnsForAnyArgs(x =>
            {
                return new List<ReservationItem>
                {
                    new ReservationItem
                    {
                        CheckInDate = new DateTime(2019, 01, 07),
                        CheckOutDate = new DateTime(2019, 01, 07),
                        ReceivedRoomTypeId = 1,
                        RoomId = 1
                    }
                };
            });

            roomTypeReadRepository.GetAvailabilityRoomTypeRowAsync(1).ReturnsForAnyArgs(x =>
            {
                return new List<AvailabilityRoomTypeRowDto>()
                {
                    new AvailabilityRoomTypeRowDto()
                    {
                        Id = 1,
                        Name = "Standard",
                        Total = 10,
                        Order = 1
                    }
                };
            });

            var reservationItemList = new List<ReservationItem>
            {
                new ReservationItem
                {
                    CheckInDate = new DateTime(2019, 01, 07),
                    CheckOutDate = new DateTime(2019, 01, 07),
                    ReceivedRoomTypeId = 1,
                    RoomId = 1
                }
            };

            var availabilityRoomTypeRowDtoList = new List<AvailabilityRoomTypeRowDto>()
            {
                new AvailabilityRoomTypeRowDto()
                {
                    Id = 1,
                    Name = "Standard",
                    Total = 10,
                    Order = 1
                }
            };

            roomTypeDomainService.GetAvailabilityRoomTypeColumns(reservationItemList, availabilityRoomTypeRowDtoList).ReturnsForAnyArgs(x =>
            {
                return new List<AvailabilityRoomTypeColumnDto>()
                {
                    new AvailabilityRoomTypeColumnDto()
                    {
                        RoomTypeId = 1,
                        Day = new DateTime(2019,01,07),
                        Date = "2019-01-07",
                        AvailableRooms = 10,
                        BlockedQuantity = 0,
                        Rooms = 10,
                        RoomTotal = 10,
                        Occupation = 0
                    }
                };
            });

            var availabilityRoomTypeColumnDto = new List<AvailabilityRoomTypeColumnDto>()
            {
                new AvailabilityRoomTypeColumnDto()
                {
                    RoomTypeId = 1,
                    Day = new DateTime(2019,01,07),
                    Date = "2019-01-07",
                    AvailableRooms = 10,
                    BlockedQuantity = 0,
                    Rooms = 10,
                    RoomTotal = 10,
                    Occupation = 0
                }
            };

            roomTypeDomainService.GetAvailabilityTotalsAndPercentageByDate(availabilityRoomTypeColumnDto, new DateTime(), new DateTime(), 10).ReturnsForAnyArgs(x =>
            {
                var totalList = new List<AvailabilityFooterColumnDto>()
                {
                    new AvailabilityFooterColumnDto()
                    {
                        Date = "2019-01-07",
                        Number = 10
                    }
                };

                var percentageList = new List<AvailabilityFooterColumnDto>()
                {
                    new AvailabilityFooterColumnDto()
                    {
                        Date = "2019-01-07",
                        Number = 55
                    }
                };

                return new KeyValuePair<List<AvailabilityFooterColumnDto>, List<AvailabilityFooterColumnDto>>(totalList, percentageList);
            });

            roomTypeReadRepository.GetAllRoomTypesByPropertyIdAsync(1).ReturnsForAnyArgs(x =>
            {
                return new ListDto<RoomTypeDto>
                {
                    Items = new List<RoomTypeDto>()
                    {
                        new RoomTypeDto
                        {
                            Id = 1
                        }
                    }
                };
            });

            roomTypeReadRepository.GetRoomTypeOverbookingData(new DateTime(), new DateTime(), 1, null).ReturnsForAnyArgs(x =>
            {
                return new List<RoomTypeOverbookingSqlDto>();
            });

            roomTypeReadRepository.GetRoomTypeOverbookinperDateData(new DateTime(), new DateTime(), 1, null).ReturnsForAnyArgs(x =>
            {
                return new List<RoomTypeOverbookingperDateSqlDto>();
            });

            var reservationItemBudgetRequestDto = new ReservationItemBudgetRequestDto()
            {
                InitialDate = new DateTime(2019, 01, 07),
                FinalDate = new DateTime(2019, 01, 07),
                RoomTypeId = 1,
                AdultCount = 1,
                CurrencyId = currencyId,
                MealPlanTypeId = mealPlanTypeId
            };

            var reservationBudgetAppService = new ReservationBudgetAppService(propertyMealPlanTypeAppService, reservationBudgetAdapter, reservationBudgetDomainService, propertyBaseRateReadRepository,
                propertyParameterReadRepository, ratePlanReadRepository, reservationItemReadRepository, reservationBudgetRepository, reservationBudgetReadRepository,
                reservationReadRepository, localizationManager, _notificationHandler, roomTypeReadRepository, propertyRateStrategyReadRepository, availabilityAppService,
                roomTypeDomainService, applicationUser, roomReadRepository, roomBlockingReadRepository, roomTypeInventoryRepository);

            var result = reservationBudgetAppService.GetWithBudgetOffer(reservationItemBudgetRequestDto, 1).Result;

            Assert.Equal(baseRateAmount - strategyIncrementAmount, result.CommercialBudgetList[0].CommercialBudgetDayList[0].BaseRateAmount);
            Assert.Equal(mealPlanAmount, result.CommercialBudgetList[0].CommercialBudgetDayList[0].MealPlanAmount);
            Assert.Equal(200, result.CommercialBudgetList[0].CommercialBudgetDayList[0].Total);
            Assert.Equal(210, result.CommercialBudgetList[1].CommercialBudgetDayList[0].Total);
        }

        [Fact]
        public void Should_RemoveRangeNoShow()
        {
            var repository = Substitute.For<IRepository<ReservationBudget>>();
            var reservationBudgetDomainService = new ReservationBudgetDomainService(repository, _notificationHandler);

            var propertyMealPlanTypeAppService = Substitute.For<IPropertyMealPlanTypeAppService>();
            var reservationBudgetAdapter = Substitute.For<IReservationBudgetAdapter>();
            var propertyBaseRateReadRepository = Substitute.For<IPropertyBaseRateReadRepository>();
            var propertyParameterReadRepository = Substitute.For<IPropertyParameterReadRepository>();
            var ratePlanReadRepository = Substitute.For<IRatePlanReadRepository>();
            var reservationItemReadRepository = Substitute.For<IReservationItemReadRepository>();
            var reservationBudgetRepository = Substitute.For<IReservationBudgetRepository>();
            var reservationBudgetReadRepository = Substitute.For<IReservationBudgetReadRepository>();
            var reservationReadRepository = Substitute.For<IReservationReadRepository>();
            var localizationManager = Substitute.For<ILocalizationManager>();
            var roomTypeReadRepository = Substitute.For<IRoomTypeReadRepository>();
            var propertyRateStrategyReadRepository = Substitute.For<IPropertyRateStrategyReadRepository>();
            var roomTypeInventoryRepository = Substitute.For<IRoomTypeInventoryRepository>();
            var availabilityAppService = Substitute.For<IAvailabilityAppService>();
            var roomTypeDomainService = Substitute.For<IRoomTypeDomainService>();
            var roomReadRepository = Substitute.For<EntityFrameworkCore.ReadInterfaces.Repositories.IRoomReadRepository>();
            var roomBlockingReadRepository = Substitute.For<IRoomBlockingReadRepository>();
            var applicationUser = Substitute.For<IApplicationUser>();

            DateTime? systemDate = DateTime.Parse("2019-08-27");
            var reservationItemId = 1;

            var reservationBudgetAppService = new ReservationBudgetAppService(propertyMealPlanTypeAppService, reservationBudgetAdapter, reservationBudgetDomainService, propertyBaseRateReadRepository,
               propertyParameterReadRepository, ratePlanReadRepository, reservationItemReadRepository, reservationBudgetRepository, reservationBudgetReadRepository,
               reservationReadRepository, localizationManager, _notificationHandler, roomTypeReadRepository, propertyRateStrategyReadRepository, availabilityAppService,
               roomTypeDomainService, applicationUser, roomReadRepository, roomBlockingReadRepository, roomTypeInventoryRepository);

            reservationBudgetAppService.RemoveRangeNoShow(systemDate, reservationItemId);

            Assert.False(_notificationHandler.HasNotification());
        }
    }
}
