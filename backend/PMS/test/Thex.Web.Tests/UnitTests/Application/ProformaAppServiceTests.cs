﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using NSubstitute;
using Shouldly;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Thex.Application.Adapters;
using Thex.Application.Services;
using Thex.Common.Enumerations;
using Thex.Domain.Entities;
using Thex.Domain.Interfaces.Integration;
using Thex.Domain.Interfaces.Repositories;
using Thex.Dto;
using Thex.Dto.Integration;
using Thex.Dto.IntegrationPartner.IntegrationPartnerProperty;
using Thex.Infra.ReadInterfaces;
using Thex.Infra.Repositories.Integration;
using Thex.Kernel;
using Tnf.AspNetCore.TestBase;
using Tnf.Notifications;
using Xunit;

namespace Thex.Web.Tests.UnitTests.Application
{
    public class ProformaAppServiceTests : TnfAspNetCoreIntegratedTestBase<StartupUnitPropertyTest>
    {
        private INotificationHandler _notificationHandler;
        private IConfiguration _configuration;
        private IInvoiceIntegration _invoiceIntegration;
        private IApplicationUser _applicationUser;
        private IInvoiceIntegrationClient _invoiceIntegrationClient;
        private IReservationBudgetReadRepository _reservationBudgetReadRepository;
        private IPropertyParameterReadRepository _propertyParameterReadRepository;
        private IBillingItemReadRepository _billingItemReadRepository;
        private IReservationReadRepository _reservationReadRepository;
        private IBillingAccountAdapter _billingAccountAdapter;
        private IReservationItemReadRepository _reservationItemReadRepository;
        private ITourismTaxReadRepository _tourismTaxReadRepository;
        private IReservationItemRepository _reservationItemRepository;
        private IPropertyReadRepository _propertyReadRepository;

        public ProformaAppServiceTests()
        {
            _notificationHandler = new NotificationHandler(ServiceProvider);
            LoadDependencies();
        }

        private void LoadDependencies()
        {
            _configuration = Substitute.For<IConfiguration>();
            _applicationUser = ServiceProvider.GetService<IApplicationUser>();
            _invoiceIntegration = Substitute.For<IInvoiceIntegration>();
            _invoiceIntegrationClient = Substitute.For<IInvoiceIntegrationClient>();
            _reservationItemReadRepository = Substitute.For<IReservationItemReadRepository>();
            _propertyParameterReadRepository = Substitute.For<IPropertyParameterReadRepository>();
            _billingItemReadRepository = Substitute.For<IBillingItemReadRepository>();
            _reservationBudgetReadRepository = Substitute.For<IReservationBudgetReadRepository>();
            _reservationItemRepository = Substitute.For<IReservationItemRepository>();
            _billingAccountAdapter = Substitute.For<IBillingAccountAdapter>();
            _tourismTaxReadRepository = Substitute.For<ITourismTaxReadRepository>();
            _propertyReadRepository = Substitute.For<IPropertyReadRepository>();
            _reservationReadRepository = Substitute.For<IReservationReadRepository>();
        }

        [Fact]
        public void Should_Resolve_All()
        {
            TnfSession.ShouldNotBeNull();
            ServiceProvider.GetService<INotificationHandler>().ShouldNotBeNull();
            ServiceProvider.GetService<IApplicationUser>().ShouldNotBeNull();
            ServiceProvider.GetService<IInvoiceIntegration>().ShouldNotBeNull();
            ServiceProvider.GetService<IInvoiceIntegrationClient>().ShouldNotBeNull();
            ServiceProvider.GetService<IReservationItemReadRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<IPropertyParameterReadRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<IBillingItemReadRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<IReservationBudgetReadRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<IReservationItemRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<IBillingAccountAdapter>().ShouldNotBeNull();
            ServiceProvider.GetService<ITourismTaxReadRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<IPropertyReadRepository>().ShouldNotBeNull();
            ServiceProvider.GetService<IReservationReadRepository>().ShouldNotBeNull();
        }

        //TO DO: Create mock service provider
        //[Fact]
        public void Should_Issue_Proforma_When_Is_Reservation()
        {
            _configuration.GetValue<string>("TaxRuleEndpoint").ReturnsForAnyArgs(x =>
            {
                return "http://teste-api";
            });

            _invoiceIntegration.PostRequest(Arg.Any<string>(), Arg.Any<Uri>()).ReturnsForAnyArgs(x =>
            {
                return new HttpResponseMessage { StatusCode = HttpStatusCode.OK, Content = 
                    new StringContent(@"{'proforma': {'id': '123'}}") };
            });

            _reservationBudgetReadRepository.GetAllByReservationId(Arg.Any<long>()).Returns(x =>
            {
                return new List<ReservationBudget>
                {
                    new ReservationBudget
                    {
                        Id = 1,
                        BudgetDay = new DateTime(),
                        ManualRate = 100
                    }
                };
            });

            _reservationReadRepository.GetById(Arg.Any<long>()).ReturnsForAnyArgs(x =>
            {
                return new Reservation
                {
                    Id = 1,
                    CompanyClientId = Guid.NewGuid(),
                    ReservationCode = "code",
                    ReservationItemList = new List<ReservationItem>
                    {
                        new ReservationItem
                        {
                            ReservationItemCode = "code"
                        }
                    }
                };
            });

            _propertyParameterReadRepository.GetPropertyParameterForPropertyIdAndApplicationParameterId(Arg.Any<int>(), Arg.Any<int>()).ReturnsForAnyArgs(x =>
            {
                return new PropertyParameterDto
                {
                    PropertyParameterValue = "1"
                };
            });

            _billingItemReadRepository.GetById(Arg.Any<int>()).ReturnsForAnyArgs(x =>
            {
                return new BillingItem
                {
                    BillingItemName = string.Empty,
                    Description = string.Empty
                };
            });

            _invoiceIntegrationClient.GetProformaBorrowerInformation(Arg.Any<Guid>()).Returns(x =>
            {
                return new List<IntegrationClientDto>
                {
                    new IntegrationClientDto
                    {
                        Id = "id",
                        Name = "name",
                        DocumentTypeId = 21
                    }
                };
            });

            _propertyParameterReadRepository.GetDtoByIdAsync(Arg.Any<int>()).ReturnsForAnyArgs(x =>
            {
                return new PropertyDto
                {
                    IntegrationPartnerPropertyList = new List<IntegrationPartnerPropertyDto>
                    {
                        new IntegrationPartnerPropertyDto
                        {
                            PropertyId = 11,
                            IntegrationPartnerType = (int)IntegrationPartnerTypeEnum.InvoiceXpress,
                            IntegrationNumber = "IntegrationNumber",
                            IntegrationCode = "IntegrationCode"
                        }
                    }
                };
            });

            _propertyReadRepository.GetUIdById(Arg.Any<int>()).ReturnsForAnyArgs(x =>
            {
                return Guid.NewGuid();
            });

            var proformaRequestDto = new ProformaRequestDto()
            {
                ReservationId = 1
            };

            var proformaAppService = new ProformaAppService(_notificationHandler, _configuration, _applicationUser, _invoiceIntegrationClient,
                _reservationBudgetReadRepository, _propertyParameterReadRepository, _billingItemReadRepository, _reservationReadRepository,
                _billingAccountAdapter, _reservationItemReadRepository, _tourismTaxReadRepository, _reservationItemRepository, _propertyReadRepository, _invoiceIntegration)
                .CreateProforma(proformaRequestDto).Result;

            Assert.False(_notificationHandler.HasNotification());
            _reservationItemRepository.Received().UpdateIntegrationId("123", proformaRequestDto);
        }

        //TO DO: Create mock service provider
        //[Fact]
        public void Should_Issue_Proforma_When_Is_ReservationItem()
        {
            _configuration.GetValue<string>("TaxRuleEndpoint").ReturnsForAnyArgs(x =>
            {
                return "http://teste-api";
            });

            _invoiceIntegration.PostRequest(Arg.Any<string>(), Arg.Any<Uri>()).ReturnsForAnyArgs(x =>
            {
                return new HttpResponseMessage
                {
                    StatusCode = HttpStatusCode.OK,
                    Content =
                    new StringContent(@"{'proforma': {'id': '123'}}")
                };
            });

            _reservationBudgetReadRepository.GetAllByReservationItemId(Arg.Any<long>()).Returns(x =>
            {
                return new List<ReservationBudget>
                {
                    new ReservationBudget
                    {
                        Id = 1,
                        BudgetDay = new DateTime(),
                        ManualRate = 100
                    }
                };
            });

            _reservationReadRepository.GetByReservationItemId(Arg.Any<DefaultLongRequestDto>()).ReturnsForAnyArgs(x =>
            {
                return new Reservation
                {
                    Id = 1,
                    CompanyClientId = Guid.NewGuid(),
                    ReservationCode = "code",
                    ReservationItemList = new List<ReservationItem>
                    {
                        new ReservationItem
                        {
                            ReservationItemCode = "code"
                        }
                    }
                };
            });

            _propertyParameterReadRepository.GetPropertyParameterForPropertyIdAndApplicationParameterId(Arg.Any<int>(), Arg.Any<int>()).ReturnsForAnyArgs(x =>
            {
                return new PropertyParameterDto
                {
                    PropertyParameterValue = "1"
                };
            });

            _billingItemReadRepository.GetById(Arg.Any<int>()).ReturnsForAnyArgs(x =>
            {
                return new BillingItem
                {
                    BillingItemName = string.Empty,
                    Description = string.Empty
                };
            });

            _invoiceIntegrationClient.GetProformaBorrowerInformation(Arg.Any<Guid>()).Returns(x =>
            {
                return new List<IntegrationClientDto>
                {
                    new IntegrationClientDto
                    {
                        Id = "id",
                        Name = "name",
                        DocumentTypeId = 21
                    }
                };
            });

            _propertyParameterReadRepository.GetDtoByIdAsync(Arg.Any<int>()).ReturnsForAnyArgs(x =>
            {
                return new PropertyDto
                {
                    IntegrationPartnerPropertyList = new List<IntegrationPartnerPropertyDto>
                    {
                        new IntegrationPartnerPropertyDto
                        {
                            PropertyId = 11,
                            IntegrationPartnerType = (int)IntegrationPartnerTypeEnum.InvoiceXpress,
                            IntegrationNumber = "IntegrationNumber",
                            IntegrationCode = "IntegrationCode"
                        }
                    }
                };
            });

            _propertyReadRepository.GetUIdById(Arg.Any<int>()).ReturnsForAnyArgs(x =>
            {
                return Guid.NewGuid();
            });

            var proformaRequestDto = new ProformaRequestDto()
            {
                ReservationItemId = 1
            };

            var proformaAppService = new ProformaAppService(_notificationHandler, _configuration, _applicationUser, _invoiceIntegrationClient,
                _reservationBudgetReadRepository, _propertyParameterReadRepository, _billingItemReadRepository, _reservationReadRepository,
                _billingAccountAdapter, _reservationItemReadRepository, _tourismTaxReadRepository, _reservationItemRepository, _propertyReadRepository, _invoiceIntegration)
                .CreateProforma(proformaRequestDto).Result;

            Assert.False(_notificationHandler.HasNotification());
            _reservationItemRepository.Received().UpdateIntegrationId("123", proformaRequestDto);
        }

        //TO DO: Create mock service provider
        //[Fact]
        public void Should_Issue_Proforma_When_Has_Tourism_Tax()
        {
            _configuration.GetValue<string>("TaxRuleEndpoint").ReturnsForAnyArgs(x =>
            {
                return "http://teste-api";
            });

            _invoiceIntegration.PostRequest(Arg.Any<string>(), Arg.Any<Uri>()).ReturnsForAnyArgs(x =>
            {
                return new HttpResponseMessage
                {
                    StatusCode = HttpStatusCode.OK,
                    Content =
                    new StringContent(@"{'proforma': {'id': '123'}}")
                };
            });

            _reservationBudgetReadRepository.GetAllByReservationItemId(Arg.Any<long>()).Returns(x =>
            {
                return new List<ReservationBudget>
                {
                    new ReservationBudget
                    {
                        Id = 1,
                        BudgetDay = new DateTime(),
                        ManualRate = 100
                    }
                };
            });

            _reservationReadRepository.GetByReservationItemId(Arg.Any<DefaultLongRequestDto>()).ReturnsForAnyArgs(x =>
            {
                return new Reservation
                {
                    Id = 1,
                    CompanyClientId = Guid.NewGuid(),
                    ReservationCode = "code",
                    ReservationItemList = new List<ReservationItem>
                    {
                        new ReservationItem
                        {
                            ReservationItemCode = "code"
                        }
                    }
                };
            });

            _propertyParameterReadRepository.GetPropertyParameterForPropertyIdAndApplicationParameterId(Arg.Any<int>(), Arg.Any<int>()).ReturnsForAnyArgs(x =>
            {
                return new PropertyParameterDto
                {
                    PropertyParameterValue = "1"
                };
            });

            _billingItemReadRepository.GetById(Arg.Any<int>()).ReturnsForAnyArgs(x =>
            {
                return new BillingItem
                {
                    BillingItemName = string.Empty,
                    Description = string.Empty
                };
            });

            _invoiceIntegrationClient.GetProformaBorrowerInformation(Arg.Any<Guid>()).Returns(x =>
            {
                return new List<IntegrationClientDto>
                {
                    new IntegrationClientDto
                    {
                        Id = "id",
                        Name = "name",
                        DocumentTypeId = 21
                    }
                };
            });

            _propertyParameterReadRepository.GetDtoByIdAsync(Arg.Any<int>()).ReturnsForAnyArgs(x =>
            {
                return new PropertyDto
                {
                    IntegrationPartnerPropertyList = new List<IntegrationPartnerPropertyDto>
                    {
                        new IntegrationPartnerPropertyDto
                        {
                            PropertyId = 11,
                            IntegrationPartnerType = (int)IntegrationPartnerTypeEnum.InvoiceXpress,
                            IntegrationNumber = "IntegrationNumber",
                            IntegrationCode = "IntegrationCode"
                        }
                    }
                };
            });

            _propertyReadRepository.GetUIdById(Arg.Any<int>()).ReturnsForAnyArgs(x =>
            {
                return Guid.NewGuid();
            });

            _tourismTaxReadRepository.GetByPropertyId().ReturnsForAnyArgs(x =>
            {
                return new TourismTaxDto
                {
                    IsTotalOfNights = true,
                    IsActive = true,
                    Amount = 100,
                    BillingItemId = 1
                };
            });

            var proformaRequestDto = new ProformaRequestDto()
            {
                ReservationItemId = 1,
                LaunchTourismTax = true
            };

            var dictionary = new Dictionary<DateTime, int>
            {
                { new DateTime(), 1 }
            };

            _reservationItemReadRepository.GetNumberOfAdultGuestAndBudgetDayByReservationId(proformaRequestDto).Returns(x =>
            {
                return dictionary;
            });

            _billingItemReadRepository.GetById(Arg.Any<int>()).ReturnsForAnyArgs(x =>
            {
                return new BillingItem
                {
                    BillingItemName = "BillingItemName",
                    Description = "Description"
                };
            });

            var proformaAppService = new ProformaAppService(_notificationHandler, _configuration, _applicationUser, _invoiceIntegrationClient,
                _reservationBudgetReadRepository, _propertyParameterReadRepository, _billingItemReadRepository, _reservationReadRepository,
                _billingAccountAdapter, _reservationItemReadRepository, _tourismTaxReadRepository, _reservationItemRepository, _propertyReadRepository, _invoiceIntegration)
                .CreateProforma(proformaRequestDto).Result;

            Assert.False(_notificationHandler.HasNotification());
            _reservationItemRepository.Received().UpdateIntegrationId("123", proformaRequestDto);
        }

        [Fact]
        public void Should_Not_Issue_Proforma_When_CompanyClientId_Is_Null()
        {
            _configuration.GetValue<string>("TaxRuleEndpoint").ReturnsForAnyArgs(x =>
            {
                return "http://teste-api";
            });

            _invoiceIntegration.PostRequest(Arg.Any<string>(), Arg.Any<Uri>()).ReturnsForAnyArgs(x =>
            {
                return new HttpResponseMessage
                {
                    StatusCode = HttpStatusCode.OK,
                    Content =
                    new StringContent(@"{'proforma': {'id': '123'}}")
                };
            });

            _reservationBudgetReadRepository.GetAllByReservationItemId(Arg.Any<long>()).Returns(x =>
            {
                return new List<ReservationBudget>
                {
                    new ReservationBudget
                    {
                        Id = 1,
                        BudgetDay = new DateTime(),
                        ManualRate = 100
                    }
                };
            });

            _reservationReadRepository.GetByReservationItemId(Arg.Any<DefaultLongRequestDto>()).ReturnsForAnyArgs(x =>
            {
                return new Reservation
                {
                    Id = 1,
                    ReservationCode = "code",
                    ReservationItemList = new List<ReservationItem>
                    {
                        new ReservationItem
                        {
                            ReservationItemCode = "code"
                        }
                    }
                };
            });

            _propertyParameterReadRepository.GetPropertyParameterForPropertyIdAndApplicationParameterId(Arg.Any<int>(), Arg.Any<int>()).ReturnsForAnyArgs(x =>
            {
                return new PropertyParameterDto
                {
                    PropertyParameterValue = "1"
                };
            });

            _billingItemReadRepository.GetById(Arg.Any<int>()).ReturnsForAnyArgs(x =>
            {
                return new BillingItem
                {
                    BillingItemName = string.Empty,
                    Description = string.Empty
                };
            });

            _invoiceIntegrationClient.GetProformaBorrowerInformation(Arg.Any<Guid>()).Returns(x =>
            {
                return new List<IntegrationClientDto>
                {
                    new IntegrationClientDto
                    {
                        Id = "id",
                        Name = "name",
                        DocumentTypeId = 21
                    }
                };
            });

            _propertyParameterReadRepository.GetDtoByIdAsync(Arg.Any<int>()).ReturnsForAnyArgs(x =>
            {
                return new PropertyDto
                {
                    IntegrationPartnerPropertyList = new List<IntegrationPartnerPropertyDto>
                    {
                        new IntegrationPartnerPropertyDto
                        {
                            PropertyId = 11,
                            IntegrationPartnerType = (int)IntegrationPartnerTypeEnum.InvoiceXpress,
                            IntegrationNumber = "IntegrationNumber",
                            IntegrationCode = "IntegrationCode"
                        }
                    }
                };
            });

            _propertyReadRepository.GetUIdById(Arg.Any<int>()).ReturnsForAnyArgs(x =>
            {
                return Guid.NewGuid();
            });


            var proformaAppService = new ProformaAppService(_notificationHandler, _configuration, _applicationUser, _invoiceIntegrationClient,
                _reservationBudgetReadRepository, _propertyParameterReadRepository, _billingItemReadRepository, _reservationReadRepository,
                _billingAccountAdapter, _reservationItemReadRepository, _tourismTaxReadRepository, _reservationItemRepository, _propertyReadRepository, _invoiceIntegration)
                .CreateProforma(new ProformaRequestDto() { ReservationItemId = 1 }).Result;

            Assert.True(_notificationHandler.HasNotification());
            _reservationItemRepository.DidNotReceive().UpdateIntegrationId(Arg.Any<string>(), Arg.Any<ProformaRequestDto>());
        }

        [Fact]
        public void Should_Not_Issue_Proforma_When_Status_Code_Is_Not_Ok()
        {
            _configuration.GetValue<string>("TaxRuleEndpoint").ReturnsForAnyArgs(x =>
            {
                return "http://teste-api";
            });

            _invoiceIntegration.PostRequest(Arg.Any<string>(), Arg.Any<Uri>()).ReturnsForAnyArgs(x =>
            {
                return new HttpResponseMessage
                {
                    StatusCode = HttpStatusCode.InternalServerError,
                    Content =
                    new StringContent("")
                };
            });

            _reservationBudgetReadRepository.GetAllByReservationItemId(Arg.Any<long>()).Returns(x =>
            {
                return new List<ReservationBudget>
                {
                    new ReservationBudget
                    {
                        Id = 1,
                        BudgetDay = new DateTime(),
                        ManualRate = 100
                    }
                };
            });

            _reservationReadRepository.GetByReservationItemId(Arg.Any<DefaultLongRequestDto>()).ReturnsForAnyArgs(x =>
            {
                return new Reservation
                {
                    Id = 1,
                    ReservationCode = "code",
                    CompanyClientId = Guid.NewGuid(),
                    ReservationItemList = new List<ReservationItem>
                    {
                        new ReservationItem
                        {
                            ReservationItemCode = "code"
                        }
                    }
                };
            });

            _propertyParameterReadRepository.GetPropertyParameterForPropertyIdAndApplicationParameterId(Arg.Any<int>(), Arg.Any<int>()).ReturnsForAnyArgs(x =>
            {
                return new PropertyParameterDto
                {
                    PropertyParameterValue = "1"
                };
            });

            _billingItemReadRepository.GetById(Arg.Any<int>()).ReturnsForAnyArgs(x =>
            {
                return new BillingItem
                {
                    BillingItemName = string.Empty,
                    Description = string.Empty
                };
            });

            _invoiceIntegrationClient.GetProformaBorrowerInformation(Arg.Any<Guid>()).Returns(x =>
            {
                return new List<IntegrationClientDto>
                {
                    new IntegrationClientDto
                    {
                        Id = "id",
                        Name = "name",
                        DocumentTypeId = 21
                    }
                };
            });

            _propertyParameterReadRepository.GetDtoByIdAsync(Arg.Any<int>()).ReturnsForAnyArgs(x =>
            {
                return new PropertyDto
                {
                    IntegrationPartnerPropertyList = new List<IntegrationPartnerPropertyDto>
                    {
                        new IntegrationPartnerPropertyDto
                        {
                            PropertyId = 11,
                            IntegrationPartnerType = (int)IntegrationPartnerTypeEnum.InvoiceXpress,
                            IntegrationNumber = "IntegrationNumber",
                            IntegrationCode = "IntegrationCode"
                        }
                    }
                };
            });

            _propertyReadRepository.GetUIdById(Arg.Any<int>()).ReturnsForAnyArgs(x =>
            {
                return Guid.NewGuid();
            });


            var proformaAppService = new ProformaAppService(_notificationHandler, _configuration, _applicationUser, _invoiceIntegrationClient,
                _reservationBudgetReadRepository, _propertyParameterReadRepository, _billingItemReadRepository, _reservationReadRepository,
                _billingAccountAdapter, _reservationItemReadRepository, _tourismTaxReadRepository, _reservationItemRepository, _propertyReadRepository, _invoiceIntegration)
                .CreateProforma(new ProformaRequestDto() { ReservationItemId = 1 }).Result;

            _reservationItemRepository.DidNotReceive().UpdateIntegrationId(Arg.Any<string>(), Arg.Any<ProformaRequestDto>());
            Assert.True(_notificationHandler.HasNotification());
        }

    }
}
